
@echo off

if not exist dist.cnf (
    @echo .
    @echo .
    @echo . Configuration is required
    @echo .
    @echo . Use "cnf [configuration name]" first
    @echo .
) else (
    if "%1" == "" (
        ant -file build.xml
    ) else (
        ant -file build.xml -DreleaseName=%1
    )
)
