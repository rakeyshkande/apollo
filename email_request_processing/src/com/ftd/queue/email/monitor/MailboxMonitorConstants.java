package com.ftd.queue.email.monitor;

public abstract class MailboxMonitorConstants 
{
    public MailboxMonitorConstants()
    {
    }
    
    public static final String MAIL_SETTINGS_PROTOCOL = "protocol";
    public static final String MAIL_SETTINGS_HOST = "host";
    public static final String MAIL_SETTINGS_USER = "user";
    public static final String MAIL_SETTINGS_PASSWORD = "password";
    public static final String MAIL_SETTINGS_FOLDER_NAME = "folderName";
    public static final String MAIL_SETTINGS_FREQUENCY = "frequency";
    
    public static final String PROPERTY_FILE = "MailSort.xml";

    public static final String FROM_ADDR_POSTMASTER = "postmaster";
    public static final String FROM_ADDR_MAILER_DAEMON = "MAILER-DAEMON";

    public static final String QUEUE_SYSTEM_BBI_INDICATOR = "BBI";
    
    public static final String MAILBOX_MONITOR_ENTITY_TYPE = "MAILBOX_MONITOR";
    public static final String MAILBOX_MONITOR_ENTITY_ID = "MAILBOX_MONITOR";

    public static final String ATTACHMENT_INCLUDED 
        = "E-mail message included an attachment";
        
    public static final String ORDER_NUMBER_SEARCH_STRING = "Order Number:";
    
    public static final String INSERT_POINT_OF_CONTACT_MAILBOX_MONITOR 
        = "INSERT_POINT_OF_CONTACT_MAILBOX_MONITOR";
        
    public static final String EMAIL_REQUEST_IN_EMAIL_HEADER = "IN_EMAIL_HEADER";
    
  /* Retrieve Order Detail stored procedure and parameters */
    public static final String GET_ORDER_DETAILS_EXTERNAL 
        = "GET_ORDER_DETAILS_EXTERNAL";
    public static final String GET_ORDER_DETAILS_EXTERNAL_IN_EXTERNAL_ORDER_NUMBER 
        = "IN_EXTERNAL_ORDER_NUMBER";
        
    /* FIND_ORDER_NUMBER stored procedure and parameters */ 
    public static final String FIND_ORDER_NUMBER_EXTERNAL
        = "FIND_ORDER_NUMBER_EXTERNAL";
    public static final String FIND_ORDER_NUMBER_EXTERNAL_IN_ORDER_NUMBER = "IN_ORDER_NUMBER";
    public static final String FIND_ORDER_NUMBER_EXTERNAL_OUT_ORDER_DETAIL_ID
        = "OUT_ORDER_DETAIL_ID";
    public static final String FIND_ORDER_NUMBER_EXTERNAL_ORDER_OUT_ORDER_GUID 
        = "OUT_ORDER_GUID";
    public static final String FIND_ORDER_NUMBER_EXTERNAL_ORDER_OUT_EXTERNAL_ORDER_NUMBER 
        = "OUT_EXTERNAL_ORDER_NUMBER";
    public static final String FIND_ORDER_NUMBER_EXTERNAL_ORDER_OUT_MASTER_ORDER_NUMBER
        = "OUT_MASTER_ORDER_NUMBER";
    public static final String FIND_ORDER_NUMBER_EXTERNAL_ORDER_OUT_CUSTOMER_ID
        = "OUT_CUSTOMER_ID";
    public static final String FIND_ORDER_NUMBER_EXTERNAL_ORDER_OUT_ORDER_DISP_CODE
        = "OUT_ORDER_DISP_CODE";
    public static final String FIND_ORDER_NUMBER_EXTERNAL_ORDER_OUT_ORDER_INDICATOR
        = "OUT_ORDER_INDICATOR";
    
}