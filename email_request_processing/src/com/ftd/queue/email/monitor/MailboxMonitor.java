package com.ftd.queue.email.monitor;
import java.sql.Connection;
import java.util.Map;
import java.util.Properties;

import javax.mail.Folder;
import javax.mail.Session;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.ftd.eventhandling.events.EventHandler;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.queue.email.constants.EmailConstants;
import com.ftd.queue.email.util.DataSourceHelper;


/**
 * This class contains the logic to start up sorting of email requests from 
 * other sources.  It is a singleton.
 *
 * @author Charles Fox
 */
public class MailboxMonitor extends EventHandler
{
    
    private Logger logger = new Logger(
        "com.ftd.queue.email.monitor.MailboxMonitor");
        
    private static final String MAILBOX_PROTOCOL="pop3";
    
    private static final String MAILBOX_FOLDER="INBOX";
    
    // Secure Configuration Context
    public static final String SECURE_CONFIG_CONTEXT = "email_request_processing";

    public MailboxMonitor()
    {
    }
    
    /**
     * Main handler logic.  This method invokes MailboxMonitorBO to start 
     * sorting of email requests from other sources process.
     * @param n/a
     * @return List
     * @throws Throwable
     */ 
    public void invoke(Object payload) throws Throwable
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering invoke");
            logger.debug("Start Mailbox Monitor Process");
        }

        javax.mail.Store store=null;
        javax.mail.Folder folder=null;
        String mailboxName=null;
        Connection conn=null;
        String host=null;
        String user=null;
        String password=null;
        String excludeDomains = null;
        String defaultSourceCode = null;
        ConfigurationUtil cu = ConfigurationUtil.getInstance();

        try{
            MessageToken mt = (MessageToken)payload;

            if(logger.isDebugEnabled()){
                logger.debug("message:" + mt.getMessage());                
            }

            // Obtain the mailbox name  
            Document mailsettingsXML = 
                  (Document)(DOMUtil.getDocument((String)mt.getMessage()));

            Node node = DOMUtil.selectSingleNode(mailsettingsXML, "//" 
                + EmailConstants.MAILBOX_SETTINGS_NODE + "/" 
                + EmailConstants.MAILBOX_NAME_NODE);

            if(node == null) throw new Exception("The mailbox_name does not exist within the XML");
            
            if(node.getFirstChild() != null){
                mailboxName = node.getFirstChild().getNodeValue();
            }            
            
            logger.info("mailboxName:" + mailboxName);

            conn = DataSourceHelper.createDatabaseConnection();             
          
            MailboxMonitorDAO dao=new MailboxMonitorDAO(conn);
            Map mailboxProps=dao.getMailboxConfigProperties();
            
            if(StringUtils.isEmpty(mailboxName)){
                host = this.getMailboxProperty("MAIL SERVER", mailboxProps);                          
                excludeDomains = this.getMailboxProperty("EXCLUDE DOMAINS", mailboxProps);
                user = cu.getSecureProperty(SECURE_CONFIG_CONTEXT, "novator_mailbox_monitor_USERNAME");
                password = cu.getSecureProperty(SECURE_CONFIG_CONTEXT, "novator_mailbox_monitor_PASSWORD");     
            }else{
                host = this.getMailboxProperty(mailboxName + " MAIL SERVER", mailboxProps);
                excludeDomains = this.getMailboxProperty(mailboxName + " EXCLUDE DOMAINS", mailboxProps);
                user = cu.getSecureProperty(SECURE_CONFIG_CONTEXT, mailboxName + "_mailbox_monitor_USERNAME");                
                password = cu.getSecureProperty(SECURE_CONFIG_CONTEXT, mailboxName + "_mailbox_monitor_PASSWORD");     
            }
            if (host == null) throw new Exception("No mail server");
            if (user == null) throw new Exception("No user");
            if (password == null) throw new Exception("No password");
            if (excludeDomains == null)
               logger.warn("No Domains are being excluded from auto response");
            /* Get javax.mail.Store object that implements 
             * the specified protocol. (i.e. IMAP). 
             */

            Properties props = System.getProperties();
            
            Session session = Session.getDefaultInstance(props, null);
            
            // Get a message store, connect to the specified mail address.
            store = session.getStore(MAILBOX_PROTOCOL);
            store.connect(host, user, password);
            
            // Get the default folder
            javax.mail.Folder defFolder = store.getDefaultFolder();
            if (defFolder == null) throw new Exception("No default folder");
            
            /* Open the specified folder. */
            folder = defFolder.getFolder(MAILBOX_FOLDER);
            if (folder == null) throw new Exception(folder + " does not exist!!!");
            
            // Open the folder for read only
            folder.open(Folder.READ_WRITE);
            
            /* Call process method of MailboxMonitorBO to 
            * start sorting of email requests. */
            MailboxMonitorBO mbMonitorBO = new MailboxMonitorBO(conn);
            
            // Obtain the default source code for unattached emails
            defaultSourceCode = mbMonitorBO.getDefaultSourceCode(mailboxName, mailboxProps);

            try{
                mbMonitorBO.process(folder, user, excludeDomains, defaultSourceCode, mailboxName);
            }catch(Throwable excep){
                logger.error(excep); 
            }

        }catch(Throwable t){
            /* Send a system message if an exception occurred while obtaining 
             * the properties or the mailbox could not be opened.
             */ 
            try{    
                logger.error(t);
                if(conn == null || conn.isClosed()){
                    conn = DataSourceHelper.createDatabaseConnection();             
                }
                SystemMessengerVO systemError = new SystemMessengerVO();
                systemError.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);                
                systemError.setMessage(t.toString());    
                systemError.setRead(true);
                systemError.setSource(EmailConstants.SYS_MESS_SOURCE_MAILBOX);
                systemError.setType(EmailConstants.SYS_MESS_TYPE_ERROR);
                SystemMessenger systemMessenger = SystemMessenger.getInstance();
                systemMessenger.send(systemError,conn,false);            
            }catch(Exception exception){
                logger.error(exception);
            }   
        }finally
        {
            try
            {
               //close folder to delete messages.
               if(folder!=null){
                  folder.close(true);
                  logger.debug("folder open? "+folder.isOpen());
               }
               
               if(store!=null){
                  
                  store.close();
                  logger.debug("store closed");
               }
               
                /* close the database connection */
                if(conn!=null){
                    conn.close();
                } 
            }catch(Exception e){
                throw new Exception(e);
            }
        
            if(logger.isDebugEnabled()){
                logger.debug("Mailbox Monitor Process Completed");
            }
        } 
    }
    
  
    
    
    private String getMailboxProperty(String propName, Map mailboxProps)
    {
      if(mailboxProps.get(propName)!=null)
      {
        return (String)mailboxProps.get(propName);
      }else
      {
        return null;
      }
     
      
    }

//Test Code  Comment out method when done.
//  public static void main(String[] args)
//  {
//    try
//    {
//      MailboxMonitor mm = new MailboxMonitor();
//      mm.invoke(null);
//    }
//    catch(Throwable e)
//    {
//        e.printStackTrace();
//    }
//  }
    
}
