package com.ftd.queue.email.monitor;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.ftd.osp.utilities.MessageUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.queue.email.constants.EmailConstants;
import com.ftd.queue.email.vo.OrderDetailVO;

/**
 * This class contains database access methods used by 
 * Mailbox Monitor application.
 *
 * @author Charles Fox
 */
public class MailboxMonitorDAO 
{

    private Logger logger = new Logger(
        "com.ftd.queue.email.monitor.MailboxMonitorDAO");
    private Connection dbConnection = null;


    public MailboxMonitorDAO(Connection conn)
    {
        super();
        dbConnection = conn;
    }

  /**
     * retrieve high level order detail information
     * @param orderNumber - String
     * @return OrderDetailVO
     * @throws IOException, SAXException, SQLException, 
        ParserConfigurationException , Exception
     */
    public OrderDetailVO getOrderDetail(String orderNumber)
        throws IOException, SAXException, SQLException, 
        ParserConfigurationException , Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering getOrderDetail");
            logger.debug("orderNumber : " + orderNumber);
        }        
        
        DataRequest request = new DataRequest();
        MessageUtil messageUtil = MessageUtil.getInstance();
        String orderDetailID = "";
        OrderDetailVO ordDetail = new OrderDetailVO();
        try {
        
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(
            MailboxMonitorConstants.FIND_ORDER_NUMBER_EXTERNAL_IN_ORDER_NUMBER, orderNumber);

            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID(MailboxMonitorConstants.FIND_ORDER_NUMBER_EXTERNAL);

           /* execute the store prodcedure */
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            /* read store prodcedure output parameters to determine 
             * if the procedure executed successfully */            
            if(outputs.get(MailboxMonitorConstants.FIND_ORDER_NUMBER_EXTERNAL_OUT_ORDER_DETAIL_ID)!= null){
                orderDetailID = (String) outputs.get(
                    MailboxMonitorConstants.FIND_ORDER_NUMBER_EXTERNAL_OUT_ORDER_DETAIL_ID).toString();
                 logger.debug("order detail id="+orderDetailID);
                 ordDetail.setOrderDetailID(orderDetailID);
                ordDetail.setOrderGuid((String) outputs.get(
                    MailboxMonitorConstants.FIND_ORDER_NUMBER_EXTERNAL_ORDER_OUT_ORDER_GUID).toString());
                ordDetail.setExternalOrderNumber((String) outputs.get(
                    MailboxMonitorConstants.FIND_ORDER_NUMBER_EXTERNAL_ORDER_OUT_EXTERNAL_ORDER_NUMBER).toString());
                ordDetail.setMasterOrderNumber((String) outputs.get(
                    MailboxMonitorConstants.FIND_ORDER_NUMBER_EXTERNAL_ORDER_OUT_MASTER_ORDER_NUMBER).toString());
                ordDetail.setCustomerID((String) outputs.get(
                    MailboxMonitorConstants.FIND_ORDER_NUMBER_EXTERNAL_ORDER_OUT_CUSTOMER_ID).toString());  
                ordDetail.setOrderDisposition((String) outputs.get(
                    MailboxMonitorConstants.FIND_ORDER_NUMBER_EXTERNAL_ORDER_OUT_ORDER_DISP_CODE).toString()); 
                ordDetail.setSource((String) outputs.get(
                    MailboxMonitorConstants.FIND_ORDER_NUMBER_EXTERNAL_ORDER_OUT_ORDER_INDICATOR).toString());     
                 //retrieve additional order detail information
                populateOrderDetail(ordDetail);
             }
           
        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting findOrderDetailID");
            }  
        }
        return ordDetail;
    }
    /**
     * Retrieve Order Detail Value Object from database based 
     * on the order number. 
     * @param orderNumber - String
     * @return OrderDetailVO
     * @throws Exception
     */ 
    public void populateOrderDetail(OrderDetailVO ordDetail)  throws IOException, SAXException, SQLException, 
        ParserConfigurationException , Exception
     {
        if(logger.isDebugEnabled()){
            logger.debug("Entering findOrderDetail");
        } 
         DataRequest request = new DataRequest();
         MessageUtil messageUtil = MessageUtil.getInstance();
        try {
            if(ordDetail.getOrderDetailID()!=null){
                /* setup store procedure input parameters */
                HashMap inputParams = new HashMap();
                inputParams.put(EmailConstants.ORDER_DETAIL_IN_ORDER_NUMBER, 
                    ordDetail.getOrderDetailID());
                
                /* build DataRequest object */
                request.setConnection(dbConnection);
                request.reset();
                request.setInputParams(inputParams);
                request.setStatementID(
                    EmailConstants.QUEUE_RETRIEVE_ORDER_DETAIL);
    
               /* execute the store prodcedure */
                DataAccessUtil dau = DataAccessUtil.getInstance();
                CachedResultSet orderDetailRS = (CachedResultSet) dau.execute(request);
                /* read store prodcedure output parameters to determine 
                 * if the procedure executed successfully */
                while(orderDetailRS.next())      // get queue config context parms
                {
                    ordDetail.setCardMessage(orderDetailRS.getString(32));
                    ordDetail.setCardSignature(orderDetailRS.getString(33));
                    SimpleDateFormat sdfOutput = 
                           new SimpleDateFormat (EmailConstants.DELIVERY_DATE_FORMAT); 
                    String textDate = orderDetailRS.getString(17);
                    java.util.Date utilDate = sdfOutput.parse( textDate );
                    ordDetail.setDeliveryDate(utilDate);
                    ordDetail.setFillingFloristId(orderDetailRS.getString(25));
                    ordDetail.setInstitutionName(orderDetailRS.getString(16));
                    ordDetail.setCancelSentFlag(orderDetailRS.getString(56));
                    ordDetail.setMercuryFlag(orderDetailRS.getString(29));
                    ordDetail.setVendorFlag(orderDetailRS.getString(54));
                    ordDetail.setProductSku(orderDetailRS.getString(19));
                    ordDetail.setReceipientAddress(orderDetailRS.getString(9));
                    ordDetail.setReceipientCity(orderDetailRS.getString(11));
                    ordDetail.setReceipientFirstName(orderDetailRS.getString(7));
                    ordDetail.setReceipientLastName(orderDetailRS.getString(8));
                    ordDetail.setReceipientPhone(orderDetailRS.getString(52));
                    ordDetail.setReceipientState(orderDetailRS.getString(12));
                    ordDetail.setReceipientZip(orderDetailRS.getString(13));
                    ordDetail.setReceipientCountry(orderDetailRS.getString(14));
                    ordDetail.setRecipientLocationType(orderDetailRS.getString(15));
                    ordDetail.setTimeZone(orderDetailRS.getString(58));
                    ordDetail.setTrackingNumber(orderDetailRS.getString(59));
                    ordDetail.setRecipientID(orderDetailRS.getString(6));
                    ordDetail.setSubstitutionIndicator(orderDetailRS.getString(22));
                    ordDetail.setOccasion(orderDetailRS.getString(31));
                    ordDetail.setReleaseInfoIndicator(orderDetailRS.getString(34));
                    ordDetail.setSpecialInstructions(orderDetailRS.getString(35));
                    ordDetail.setQuantity(orderDetailRS.getString(41));
                    ordDetail.setColor1(orderDetailRS.getString(42));
                    ordDetail.setColor2(orderDetailRS.getString(43));
                    ordDetail.setSubstitutionIndicator(orderDetailRS.getString(44));
                    ordDetail.setSameDayGift(orderDetailRS.getString(46));
                    ordDetail.setShipMethod(orderDetailRS.getString(47));
                    ordDetail.setShipDate(orderDetailRS.getDate("ship_date"));
                    ordDetail.setSourceCode(orderDetailRS.getString("source_code"));
                    ordDetail.setLanguageId(orderDetailRS.getString("language_id"));
    
                }
              
            }
            else
            {
                String messageString = 
                    messageUtil.getMessageContent(
                    EmailConstants.UNABLE_RETRIEVE_AMAZON_ORDER);
                logger.error(messageString);
                throw new Exception(messageString);    
            }
        }finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting findOrderDetail");
            } 
        }
        
    }

    /**
     * Store Email Queue Value Object to database. 
     * @param EmailQueue - EmailQueueVO
     * @return n/a
     * @throws Exception
     */ 
    public String storeEmailQueue(EmailQueueVO EmailQueue) throws 
        IOException, SAXException, ParserConfigurationException, 
         SQLException, Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering storeEmailQueue");
        }
         String pointOfContactID = "";
         
         DataRequest request = new DataRequest();
         try {
         MessageUtil messageUtil = MessageUtil.getInstance();
        
            /* setup store procedure input parameters and parse out attributes*/
            HashMap inputParams = new HashMap();
            OrderDetailVO ordDetail = EmailQueue.getOrderDetailVO();
            if(ordDetail!=null&&ordDetail.getOrderDetailID()!=null){
                
                inputParams.put(EmailConstants.EMAIL_REQUEST_IN_CUSTOMER_ID, 
                    ordDetail.getCustomerID());
                inputParams.put(EmailConstants.EMAIL_REQUEST_IN_ORDER_GUID, 
                    ordDetail.getOrderGuid());
                inputParams.put(EmailConstants.EMAIL_REQUEST_IN_ORDER_DETAIL_ID,
                    ordDetail.getOrderDetailID());
                inputParams.put(EmailConstants.EMAIL_REQUEST_IN_MASTER_ORDER_NUMBER, 
                    ordDetail.getMasterOrderNumber());
                inputParams.put(EmailConstants.EMAIL_REQUEST_IN_ORDER_NUMBER,
                    ordDetail.getExternalOrderNumber());
            }    
            inputParams.put(EmailConstants.EMAIL_REQUEST_IN_DELIVERY_DATE_ONE, 
                null);
            inputParams.put(EmailConstants.EMAIL_REQUEST_IN_BILLING_FIRST_NAME, 
                null);
            inputParams.put(EmailConstants.EMAIL_REQUEST_IN_BILLING_LAST_NAME, 
                null);
            inputParams.put(EmailConstants.EMAIL_REQUEST_IN_DAY_PHONE, 
                null);
            inputParams.put(EmailConstants.EMAIL_REQUEST_IN_EVE_PHONE, 
                null);
            inputParams.put(EmailConstants.EMAIL_REQUEST_IN_SENDER_EMAIL_ADDRESS, 
                EmailQueue.getSenderEmail());
            inputParams.put(EmailConstants.EMAIL_REQUEST_IN_LETTER_TITLE, 
                null);
            inputParams.put(EmailConstants.EMAIL_REQUEST_IN_EMAIL_SUBJECT, 
                EmailQueue.getSubject());
            inputParams.put(EmailConstants.EMAIL_REQUEST_IN_BODY, 
                EmailQueue.getBody());
            inputParams.put(EmailConstants.EMAIL_REQUEST_IN_COMMENT_TYPE, 
                EmailConstants.POINT_OF_CONTACT_COMMENT_TYPE);
            inputParams.put(EmailConstants.EMAIL_REQUEST_IN_RECIPIENT_EMAIL_ADDRESS, 
                null);
            if(ordDetail!=null){
              inputParams.put(EmailConstants.EMAIL_REQUEST_IN_RECIPIENT_FIRST_NAME, 
                  (ordDetail.getReceipientFirstName() != null)?ordDetail.getReceipientFirstName():null);
              inputParams.put(EmailConstants.EMAIL_REQUEST_IN_RECIPIENT_LAST_NAME, 
                  (ordDetail.getReceipientLastName() != null)?ordDetail.getReceipientLastName():null);
              inputParams.put(EmailConstants.EMAIL_REQUEST_IN_RECIPIENT_ADDRESS, 
                  (ordDetail.getReceipientAddress() != null)?ordDetail.getReceipientAddress():null);
              inputParams.put(EmailConstants.EMAIL_REQUEST_IN_RECIPIENT_CITY, 
                  (ordDetail.getReceipientCity() != null)?ordDetail.getReceipientCity():null);
              inputParams.put(EmailConstants.EMAIL_REQUEST_IN_RECIPIENT_PHONE, 
                  (ordDetail.getReceipientPhone() != null)?ordDetail.getReceipientPhone():null);
              inputParams.put(EmailConstants.EMAIL_REQUEST_IN_RECIPIENT_ZIP,
                  (ordDetail.getReceipientZip() != null)?ordDetail.getReceipientZip():null);
              inputParams.put(EmailConstants.EMAIL_REQUEST_IN_RECIPIENT_STATE, 
                  (ordDetail.getReceipientState() != null)?ordDetail.getReceipientState():null);
              inputParams.put(EmailConstants.EMAIL_REQUEST_IN_RECIPIENT_COUNTRY, 
                  (ordDetail.getReceipientCountry() != null)?ordDetail.getReceipientCountry():null);
              inputParams.put(EmailConstants.EMAIL_REQUEST_IN_INSTITUTION, 
                  (ordDetail.getInstitutionName() != null)?ordDetail.getInstitutionName():null);
              inputParams.put(EmailConstants.EMAIL_REQUEST_IN_CARD_MESSAGE, 
                (ordDetail.getCardMessage() != null)?ordDetail.getCardMessage():null);
            }
            inputParams.put(EmailConstants.EMAIL_REQUEST_IN_DELIVERY_DATE_TWO, 
                null);
            
            inputParams.put(EmailConstants.EMAIL_REQUEST_IN_PRODUCT_SKU, 
                null);
            inputParams.put(EmailConstants.EMAIL_REQUEST_IN_CHANGE_CODES, 
                null);
            inputParams.put(EmailConstants.EMAIL_REQUEST_IN_SENT_RECEIVED_INDICATOR, 
               "I");
            inputParams.put(EmailConstants.EMAIL_REQUEST_IN_SEND_FLAG, 
                null);
            inputParams.put(EmailConstants.EMAIL_REQUEST_IN_PRINT_FLAG, 
                null);
            inputParams.put(EmailConstants.EMAIL_REQUEST_IN_POINT_OF_CONTACT_TYPE,
                EmailConstants.POINT_OF_CONTACT_TYPE_EMAIL);
            inputParams.put(EmailConstants.EMAIL_REQUEST_IN_COMMENT_TEXT,
                null);
            inputParams.put(MailboxMonitorConstants.EMAIL_REQUEST_IN_EMAIL_HEADER,
                EmailQueue.getEmailHeader());  
            inputParams.put(EmailConstants.EMAIL_REQUEST_IN_SOURCE_CODE, 
                EmailQueue.getDefaultSourceCode());
            inputParams.put("IN_CREATED_BY", "SYS");
            inputParams.put("IN_NEW_CARD_SIGNATURE", null);   
            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("QUEUE_INSERT_POINT_OF_CONTACT");

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            /* read store prodcedure output parameters to determine 
             * if the procedure executed successfully */
                String status = (String) outputs.get(EmailConstants.EMAIL_REQUEST_OUT_STATUS_PARAM);
                if(status.equals("N"))
                {
                    String message = (String) outputs.get(
                    EmailConstants.EMAIL_REQUEST_OUT_MESSAGE_PARAM);
                    String messageString = 
                        messageUtil.getMessageContent(
                        EmailConstants.UNABLE_ARCHIVE_MSG);
                    throw new Exception(messageString + " " +  message);
                }
                else
                {
                    pointOfContactID = (String) outputs.get(
                        EmailConstants.EMAIL_REQUEST_OUT_SEQUENCE_NUMBER);
                }
            
        }finally {
            // Since this DML involved CLOBs, cleanup any temporarily generated CLOBs.
            request.reset();
         
            if(logger.isDebugEnabled()){
               logger.debug("Exiting storeEmailQueue");
            } 
        }
        
        return pointOfContactID;
        
    }  
    
    
    
    
    
    
    /**
     * Check whether or not the sorting process is locked. If it is
     * locked return true. Otherwise, lock the process and return false.
     * @param boolean - lock status
     * @return n/a
     * @throws Exception
     */ 
    public boolean isLocked(int hashCode, String csr_id, String mailFolderURLName)
        throws IOException, SAXException, SQLException, 
            ParserConfigurationException , Exception{
        if(logger.isDebugEnabled()){
            logger.debug("Entering isLocked");
            logger.debug("hashCode : " + hashCode);
            logger.debug("csr_id : " + csr_id);
        }
      
         DataRequest request = new DataRequest();
         MessageUtil messageUtil = MessageUtil.getInstance();
        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(EmailConstants.LOCK_ORDER_IN_ENTITY_TYPE, 
                MailboxMonitorConstants.MAILBOX_MONITOR_ENTITY_TYPE);
            inputParams.put(EmailConstants.LOCK_ORDER_IN_ENTITY_ID, 
                mailFolderURLName);
            inputParams.put(EmailConstants.LOCK_ORDER_IN_SESSION_ID, 
                String.valueOf(hashCode));
            inputParams.put(EmailConstants.LOCK_ORDER_IN_CSR_ID, csr_id);

            // build DataRequest object
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID(EmailConstants.LOCK_ORDER);

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            String lockObtained=(String)outputs.get("OUT_LOCK_OBTAINED");
            logger.debug("lockObtained =["+lockObtained+"]");   
            return (lockObtained!=null&&lockObtained.trim().equalsIgnoreCase("Y"));
                
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting isLocked");
            } 
        }       
       
    }

    /**
     * Release the lock of sorting process.
     * @param n/a
     * @return n/a
     * @throws Exception
     */ 
    public void releaseLock(int hashCode, String csr_id, String folderURLName)
        throws IOException, SAXException, SQLException, 
        ParserConfigurationException , Exception
        {
        if(logger.isDebugEnabled()){
            logger.debug("Entering releaseLock");
           
            logger.debug("hashCode : " + hashCode);
            logger.debug("csr_id : " + csr_id);
        }
         DataRequest request = new DataRequest();
         MessageUtil messageUtil = MessageUtil.getInstance();
        
        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(EmailConstants.RELEASE_ORDER_IN_ENTITY_TYPE, 
                MailboxMonitorConstants.MAILBOX_MONITOR_ENTITY_TYPE);
            inputParams.put(EmailConstants.RELEASE_ORDER_IN_ENTITY_ID, 
                folderURLName);
            inputParams.put(EmailConstants.RELEASE_ORDER_IN_SESSION_ID, 
                String.valueOf(hashCode));
            inputParams.put(EmailConstants.RELEASE_ORDER_IN_CSR_ID, csr_id);
            
            // build DataRequest object
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID(EmailConstants.RELEASE_ORDER);

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            String status = (String) outputs.get(
                    EmailConstants.RELEASE_ORDER_STATUS_PARAM);
                if(status.equals("N"))
                {
                    String message = (String) outputs.get(
                        EmailConstants.RELEASE_ORDER_MESSAGE_PARAM);
                    String messageString = 
                        messageUtil.getMessageContent(
                        EmailConstants.UNABLE_RELEASE_ORDER);
                    throw new Exception(messageString + " " +  message);
                }
            
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting releaseLock");
            } 
        }       
    
    
    }
    
    
    
  public Map getMailboxConfigProperties() throws IOException, SAXException, SQLException, 
        ParserConfigurationException , Exception{  
    // build DataRequest object
            DataRequest request = new DataRequest();
            Map mailboxProps=new HashMap();
            request.reset();
            request.setConnection(dbConnection);
            HashMap inputParams = new HashMap();
            inputParams.put("CONTEXT_ID", "ins_novator_mailbox_monitor");
            request.setInputParams(inputParams);
            request.setStatementID("GET_EMAIL_QUEUE_CONFIG_DATA");

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();

           CachedResultSet rs= (CachedResultSet) dau.execute(request);
           while(rs.next())
           {
             mailboxProps.put(rs.getString("NAME"), rs.getString("VALUE"));
           }
           return mailboxProps;
  }
    
}
