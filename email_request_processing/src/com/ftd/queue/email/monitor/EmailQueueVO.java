package com.ftd.queue.email.monitor;
import com.ftd.queue.email.vo.OrderDetailVO;

public class EmailQueueVO 
{
    public EmailQueueVO()
    {
    }
    
    private String subject = "";
    private String body = "";
    private String senderEmail = "";
    private OrderDetailVO ordDetailVO;
    private String emailHeader = "";
    private String defaultSourceCode = "";
    
    public String getEmailHeader() {
        return emailHeader;
    }

    public void setEmailHeader(String newEmailHeader) {
        emailHeader = trim(newEmailHeader);
    } 
    
    public String getSubject() {
        return subject;
    }

    public void setSubject(String newSubject) {
        subject = trim(newSubject);
    }

    public String getBody() {
        return body;
    }

    public void setBody(String newBody) {
        body = trim(newBody);
    }

    public String getSenderEmail() {
        return senderEmail;
    }

    public void setSenderEmail(String newSenderEmail) {
        senderEmail = trim(newSenderEmail);
    }
    
    private String trim(String str)
    {
        return (str != null)?str.trim():str;
    }
    
    public OrderDetailVO getOrderDetailVO() {
        return ordDetailVO;
    }
    
    public void setOrderDetailVO(OrderDetailVO newOrdDetailVO) {
        ordDetailVO = newOrdDetailVO;
    }

    public void setDefaultSourceCode(String newDefaultSourceCode) {
        this.defaultSourceCode = trim(newDefaultSourceCode);
    }

    public String getDefaultSourceCode() {
        return defaultSourceCode;
    }
}
