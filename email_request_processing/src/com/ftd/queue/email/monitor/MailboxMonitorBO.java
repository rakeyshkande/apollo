package com.ftd.queue.email.monitor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;

import javax.activation.DataHandler;
import javax.mail.Address;
import javax.mail.Flags;
import javax.mail.Header;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.internet.ContentType;
import javax.mail.search.FlagTerm;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.messagegenerator.StockMessageGenerator;
import com.ftd.messagegenerator.dao.MessageGeneratorDAO;
import com.ftd.messagegenerator.vo.PointOfContactVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.PartnerVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.queue.email.constants.EmailConstants;
import com.ftd.queue.email.dao.EmailQueueDAO;
import com.ftd.queue.email.vo.AutoEmailVO;
import com.ftd.queue.email.vo.CompanyEmailVO;
import com.ftd.queue.email.vo.OrderDetailVO;
import com.ftd.queue.email.vo.QueueVO;

/**
 * This class contains all the logic to perform sorting of email requests from
 * other sources.
 *
 * @author Charles Fox
 */
public class MailboxMonitorBO {

    private Logger logger = new Logger(
            "com.ftd.queue.email.monitor.MailboxMonitorBO");

    private Connection dbConnection = null;

    private String folder = ""; // The mail box folder the monitor listens to.

    private String frequency = ""; // The frequency at which system checks for
                                    // new email.
    private static final String ORDER_NUMBER="Order Number:";
    private static final char lt = '<';
    private static final char gt = '>';
    private static final String WALMART_EMAIL_1 = "help@walmart.com";
    private static final String OOO_MSG_TYPE = "OOO";
    private static final int WALMART_MASTER_ORDER_MIN_LENGTH = 11;
    private static final int WALMART_MASTER_ORDER_MAX_LENGTH = 16;
    private static final int MAX_ORDER_LENGTH = 25;

    private ArrayList emailList = new ArrayList();
    private ArrayList walmartEmailAddresses = new ArrayList();
    private final static String _EMAIL_MAILBOX_USER = "_mailbox_monitor_USERNAME";

    public MailboxMonitorBO(Connection conn) {
        super();
        dbConnection = conn;
        walmartEmailAddresses.add(WALMART_EMAIL_1);
    }

    /**
     * This method controls the sorting of email requests from other sources
     * process.
     *
     * @param mailFolder -
     *            javax.mail.Folder
     * @param defaultSourceCode Default source code for unattached emails           
     * @return n/a
     * @throws Exception
     */
    public void process(javax.mail.Folder mailFolder, String mailboxUser, String excludeDomains, String defaultSourceCode, String mailboxName)
            throws MessagingException, IOException, SAXException, SQLException,
            ParserConfigurationException , TransformerException,
            Exception {

        if (logger.isDebugEnabled()) {
            logger.debug("Entering process");
        }
        try {
            MailboxMonitorDAO mbMonitorDAO = new MailboxMonitorDAO(dbConnection);
            String csr_id = ConfigurationUtil.getInstance().getProperty(
                    EmailConstants.ORDER_INFORMATION_CONFIG_FILE,
                    EmailConstants.ORDER_INFORMATION_CSR_ID);
            String foldURLName = mailFolder.getURLName().toString();

            // Check lock on the process.
            boolean locked = mbMonitorDAO.isLocked(this.hashCode(), csr_id,
                    foldURLName);

            if (locked) {
                logger.debug("able to obtain lock. Process messages...");
                try {
                    processUnreadMessages(mailFolder, mbMonitorDAO, csr_id, mailboxUser, excludeDomains, defaultSourceCode, mailboxName);
                } catch (Exception e) {
                    logger.error(e);
                }

                // Release lock.
                mbMonitorDAO.releaseLock(this.hashCode(), csr_id, foldURLName);
            }

        } finally {

            if (logger.isDebugEnabled()) {
                logger.debug("Exiting process");
            }
        }
    }

    /**
     * This method retrieves and processes all unread messages. It should be
     * called only when the process first starts up.
     *
     * @param mailFolder -
     *            javax.mail.Folder
     * @param defaultSourceCode Default source code for unattached emails           
     * @return n/a
     * @throws Exception
     */
    public void processUnreadMessages(javax.mail.Folder mailFolder,
            MailboxMonitorDAO mbMonitorDAO, String csrId, String mailboxUser,
            String excludeDomains, String defaultSourceCode,String mailboxName)
            throws MessagingException, IOException, SAXException, SQLException,
            ParserConfigurationException , TransformerException,
            Exception {

        if (logger.isDebugEnabled()) {
            logger.debug("Entering processUnreadMessages");
        }

        try {
            FlagTerm term = new FlagTerm(new Flags(Flags.Flag.SEEN), false);

            Message[] msgs = mailFolder.search(term);

            if (msgs != null && msgs.length > 0) {
                processMessage(msgs, mbMonitorDAO, csrId, mailboxUser, excludeDomains, defaultSourceCode, mailboxName);
            }

        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting processUnreadMessages");
            }
        }
    }

    /**
     * This method iterates through the given message array and processes the
     * individual message.
     *
     * @param mailFolder -
     *            javax.mail.Folder
     * @param defaultSourceCode Default source code for unattached emails           
     * @return n/a
     * @throws Exception
     */
    public void processMessage(Message[] messages,
            MailboxMonitorDAO mbMonitorDAO, String csrId, String mailboxUser,
            String excludeDomains, String defaultSourceCode, String mailboxName)
    {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering processMessage");
        }

        try {
            EmailQueueDAO emailDAO = new EmailQueueDAO(dbConnection);

            boolean hasError = false;
            for (int i = 0, n = messages.length; i < n; i++) {
                if (logger.isDebugEnabled()) {
                    logger.debug("Start Process Message " + i);
                }
                Message msg = messages[i];

                boolean processed = false;

                try {
                	if (logger.isDebugEnabled()) {
                         logger.debug(i+" Subject: " + msg.getSubject());
                	}

                      processMessage(msg, mbMonitorDAO, csrId, emailDAO, excludeDomains, defaultSourceCode, mailboxUser, mailboxName);

                } catch (MessagingException e) {
                    logger.error(e);
                } catch (IOException e) {
                    logger.error(e);
                } catch (SAXException e) {
                    logger.error(e);
                } catch (SQLException e) {
                    logger.error(e);
                } catch (ParserConfigurationException e) {
                    logger.error(e);
                } catch (TransformerException e) {
                    logger.error(e);
                } catch (Exception e) {
                    logger.error(e);
                }
                if (logger.isDebugEnabled()) {
                    logger.debug("End Process Message " + i);
                }
                if (!processed) {
                    hasError = true;
                }
                try{
                    msg.setFlag(Flags.Flag.DELETED, true);//delete the processed messages.
                }catch(Exception e)
                {
                    logger.error(e);
                }
            }

            //If any emails need to be sent out then send them.
            //This is done in batch to reduce the number of connections used.
            if(emailList != null && emailList.size() > 0)
            {
                StockMessageGenerator messageGenerator = new StockMessageGenerator();
                messageGenerator.processMessages(emailList);
            }

        } catch (Exception e) {
            logger.error(e);

        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting processMessage");
            }
        }
    }

    /**
     * @param messages
     * @param mbMonitorDAO
     * @param csrId
     * @param emailDAO
     * @param i
     * @param defaultSourceCode Default source code for unattached emails           
     * @throws MessagingException
     * @throws IOException
     * @throws SAXException
     * @throws SQLException
     * @throws ParserConfigurationException
     * @throws XSLException
     * @throws Exception
     * @throws TransformerException
     *             void
     */
    private void processMessage(Message message,
            MailboxMonitorDAO mbMonitorDAO, String csrId, EmailQueueDAO emailDAO,
            String excludeDomains, String defaultSourceCode, String mailboxUser,
            String mailboxName)
            throws MessagingException, IOException, SAXException, SQLException,
            ParserConfigurationException , Exception,
            TransformerException {
        // If the message is not processed by another processor, then:
        // Get message subject.
        EmailQueueVO emailQueue = new EmailQueueVO();
        OrderDetailVO ordDetail=new OrderDetailVO();
        setEmailQueueVOHeaderInfo(message, emailQueue);
        MessageGeneratorDAO msgGenDAO = new MessageGeneratorDAO(
                    dbConnection);
        String defaultSender=msgGenDAO
                    .loadSenderEmailAddress(null, ordDetail.getCompanyID(), null);
        setSenderEmail(message, emailQueue, defaultSender);
        emailQueue.setSubject(message.getSubject());
        logger.info("Email from " + emailQueue.getSenderEmail() + " / " +
        		emailQueue.getSubject());

        // Get message content.
        Object content = message.getContent();
        // If the content is multipart, call handleMultipart method to extract
        // message text body, and append �This message has an attachment, please
        // review the original email.� to the text body.
        // If the content is not multipart, call handlePart method to extract
        // message text body.
        if ((content instanceof Multipart)) {
        	if (logger.isDebugEnabled()) {
                logger.debug("Multipart");
        	}
            Multipart multipart = (Multipart) content;
            emailQueue.setBody(handleMultipart(multipart));
        } else {
        	if (logger.isDebugEnabled()) {
                logger.debug("Not Multipart");
        	}
            emailQueue.setBody(handlePart(message, false));
        }

        /*
         * Note: Attached and Unattached statuses will be based on whether or
         * not an order has been found for the message.
         */
        /* try to find order */

        /* extract order number from subject */
        String orderNumber = extractOrderNumber(emailQueue);

        /****************************************************************************************
         *retrieve sender's email @.  If it's a Walmart order and if the orderNumber is empty,
         *try to extract the order number from the email body again.
         ****************************************************************************************/
        //if orderNumber is not found in the subject
        if (StringUtils.isEmpty(orderNumber))
        {
          //set a flag to see if the email came from a valid walmart email address
          boolean isSenderWalmart = false;

          //retrieve sender's email @
          String emailAddress = emailQueue.getSenderEmail();

          //go thru valid walmart email address list.  check if the sender of current email is Walmart
          for (int i = 0; i < walmartEmailAddresses.size(); i++)
          {
            if (StringUtils.equalsIgnoreCase(emailAddress, walmartEmailAddresses.get(0).toString()))
            {
              isSenderWalmart = true;
              break;
            }
          }

          /* extract order number from body */
          if (isSenderWalmart)
          {
            orderNumber = extractOrderNumberFromBody(emailQueue);
          }
        }

        /* find order */
        if(StringUtils.isNotEmpty(orderNumber)){
            ordDetail.setExternalOrderNumber(orderNumber);
            ordDetail = mbMonitorDAO
                    .getOrderDetail(trim(orderNumber));

            emailQueue.setOrderDetailVO(ordDetail);
        }

        // Set default source code.
        emailQueue.setDefaultSourceCode(defaultSourceCode);
        
        /* store point of contact */
        String pointOfContact = mbMonitorDAO.storeEmailQueue(emailQueue);

        if (ordDetail!=null&&StringUtils.isEmpty(ordDetail.getOrderDetailID())) {
            ordDetail.setOrderDetailID(null);
        }
        /* store Queue */
        saveEmailQueue(emailQueue, ordDetail, pointOfContact, emailDAO, defaultSender, excludeDomains, msgGenDAO, mailboxUser, mailboxName);
        /* insert order comments */
        if (ordDetail!=null&&ordDetail.getOrderDetailID()!=null) {
            // -update order comments
            String comments = retrieveOrderComments("ot.queue");
            emailDAO.insertComments(comments, ordDetail, csrId);
        }

    }

    /**
     * @param emailQueue
     * @param ordDetail
     * @param pointOfContact
     * @throws IOException
     * @throws SAXException
     * @throws SQLException
     * @throws ParserConfigurationException
     * @throws XSLException
     * @throws Exception
     *             void
     */
    private void saveEmailQueue(EmailQueueVO emailQueue,
            OrderDetailVO ordDetail, String pointOfContact, EmailQueueDAO emailDAO, String defaultSender,
            String excludeDomains, MessageGeneratorDAO msgGenDAO, String mailboxUser, String mailboxName)
            throws IOException, SAXException, SQLException, ParserConfigurationException,
             Exception {
        QueueVO newQueue = new QueueVO();
        populateQueueVO(emailQueue, ordDetail, pointOfContact, newQueue, mailboxUser);
        if (excludeDomains == null)
        {
            excludeDomains = "none";
        }
        /* if sender is mailer-daemon or postmaster, set system to BBI */
        if (emailQueue.getSenderEmail().indexOf(
                MailboxMonitorConstants.FROM_ADDR_MAILER_DAEMON) > -1
                || emailQueue.getSenderEmail().indexOf(
                        MailboxMonitorConstants.FROM_ADDR_POSTMASTER) > -1) {

            newQueue
                    .setSystem(MailboxMonitorConstants.QUEUE_SYSTEM_BBI_INDICATOR);

        }
        else
        {
          //send auto response.
          //only send an auto response to senders that are not excluded domains
          //Example walmart.com or amazon.com

          String toEmails=emailQueue.getSenderEmail();
          if(StringUtils.isNotBlank(toEmails) && toEmails.indexOf(excludeDomains)==-1)
          {
               this.sendEmail(emailQueue, ordDetail, emailDAO, msgGenDAO, mailboxName);
          }

        }

        saveEmailQueue(newQueue,  emailDAO);
    }

    /**
     * @param emailQueue
     * @param ordDetail
     * @param pointOfContact
     * @param newQueue
     *            void
     * @throws Exception 
     */
	private void populateQueueVO(EmailQueueVO emailQueue,
			OrderDetailVO ordDetail, String pointOfContact, QueueVO newQueue,
			String mailboxUser) throws Exception {

		ConfigurationUtil configUtil;

		configUtil = ConfigurationUtil.getInstance();
		String commonString = "custserv";
		// Extracting the actual mailbox user name last index. For eg. mail
		// user name for mercent amazon orders is mercentamzncustserv_qa. In
		// this string
		// mercentamzn is the actual name and remaining sub string is common
		// for all mail users.
		int mailBoxUserNameLastIndex = mailboxUser.indexOf(commonString);

		if (mailBoxUserNameLastIndex > 0) {
			String mailBoxUserName = mailboxUser.substring(0,
					mailBoxUserNameLastIndex).toUpperCase();
			String queueTypeKey = mailBoxUserName + "_QUEUE_TYPE";
			String messageTypeKey = mailBoxUserName + "_MSG_TYPE";

			String queueType = configUtil.getFrpGlobalParm(
					"ins_novator_mailbox_monitor", queueTypeKey);
			String messageType = configUtil.getFrpGlobalParm(
					"ins_novator_mailbox_monitor", messageTypeKey);
			if (logger.isDebugEnabled()) {
			    logger.debug("Queue Type : "+queueType+" Message Type : "+messageType);
			}
			
			if (StringUtils.isNotEmpty(queueType)
					&& StringUtils.isNotEmpty(messageType)) {
				newQueue.setQueueType(queueType);
				newQueue.setEmail(emailQueue.getSenderEmail());
				newQueue.setOrderDetailVO(ordDetail);
				newQueue.setMercuryNumber(null);
				newQueue.setMessageType(messageType);
				newQueue.setPointOfContactID(pointOfContact);
				newQueue.setTimeStamp(new Timestamp(System.currentTimeMillis()));
			} else {
				newQueue.setQueueType("OT");
				newQueue.setEmail(emailQueue.getSenderEmail());
				newQueue.setOrderDetailVO(ordDetail);
				newQueue.setMercuryNumber(null);
				newQueue.setMessageType("OT");
				newQueue.setPointOfContactID(pointOfContact);
				newQueue.setTimeStamp(new Timestamp(System.currentTimeMillis()));
			}
		} else {
			if (logger.isDebugEnabled()) {
			    logger.debug("Sending to OT Queue");
			}
			newQueue.setQueueType("OT");
			newQueue.setEmail(emailQueue.getSenderEmail());
			newQueue.setOrderDetailVO(ordDetail);
			newQueue.setMercuryNumber(null);
			newQueue.setMessageType("OT");
			newQueue.setPointOfContactID(pointOfContact);
			newQueue.setTimeStamp(new Timestamp(System.currentTimeMillis()));
		}
	}

    /**
     * @param message
     * @param i
     * @param emailQueue
     * @throws MessagingException
     *             void
     */
    private void setSenderEmail(Message message, EmailQueueVO emailQueue, String defaultSender)
            throws MessagingException {
        String senderEmail = "";
        javax.mail.Address address = null;
        Address[] from = message.getFrom();

				int start = -1;
				int end = -1;
				String sender = null;
				for (int j = 0; j < from.length; j++) {
            if(from[j] != null && !from[j].toString().equalsIgnoreCase(defaultSender)){

							// Email address is stored within <>
							// Only obtain the actual email address
							sender = from[j].toString();
							start = sender.indexOf(lt);
							end = sender.indexOf(gt);
							if(start != -1 && end != -1 && (start + 1) < sender.length() && end < sender.length())
								sender = sender.substring(start + 1, end);

							if(logger.isDebugEnabled())
							{
								logger.debug("Sender email was:" + from[j].toString());
								logger.debug("Sender email converted to:" + sender);
							}

							if (senderEmail.equals("")) {
                  senderEmail = sender;
              } else {
                  senderEmail = senderEmail + ", " + sender;
              }
            }else
            {
              if(from[j] != null)
								logger.warn("From ["+from[j].toString()+"] is the same as System default sender ["+defaultSender+"]. Auto response will not be sent to this email address.");
							else
							  logger.warn("From email address is null!");
            }
        }

        emailQueue.setSenderEmail(senderEmail);
    }

    /**
     * @param messages
     * @param i
     * @param emailQueue
     * @throws MessagingException
     *             void
     */
    private void setEmailQueueVOHeaderInfo(Message message,
            EmailQueueVO emailQueue) throws MessagingException {
        Enumeration headers = message.getAllHeaders();
        String headerInfo = "";
        while (headers.hasMoreElements()) {
            Header header = (Header) headers.nextElement();
            if (!headerInfo.equals("")) {
                headerInfo = headerInfo + " " + header.getName() + ": "
                        + header.getValue();
            } else {
                headerInfo = headerInfo + header.getName() + ": "
                        + header.getValue();

            }

        }

        emailQueue.setEmailHeader(headerInfo);
    }

    /**
     * Based on com.ftd.osp.utilities.email.BuildOrderEmailHelper class:
     * HTMLend = <tr><td><br></td></tr>\n
     * Plain text end = \n.
     * The pattern of order number is: "Order Number:" + 15 spaces + external order number + either HTMLend or plain text end.
     * @param emailQueue
     * @return String
     */
    private String extractOrderNumber(EmailQueueVO emailQueue) {

        //order number
        String orderNumber = "";

        //first check if the body contains the order number
        String searchText = emailQueue.getSubject();
        int number = searchText.indexOf(ORDER_NUMBER);
        String orderNumberDelimiter = ")";

        //if the order number is not found, check the subject
        if (number < 0) {
            searchText = emailQueue.getBody();
            if (searchText != null){
            	number = searchText.indexOf(ORDER_NUMBER);
            }            
        }

        //parse out the order number
        if (number > 0) {
        	if (logger.isDebugEnabled()) {
                logger.debug("Order number string is found");
        	}
            String str = searchText.substring(number+ORDER_NUMBER.length()).trim();
            int newLine=str.indexOf(orderNumberDelimiter);//find the first \n.
            if (logger.isDebugEnabled()) {
            	logger.debug("first \\n: "+newLine);
            }

            //check to see where the order # string ends. this is determined by finding the relative
            //position of this delimeter in 'str'
            if (newLine < 0)
            {
              //define new delimeter
              orderNumberDelimiter = " ";
              //and find the position for this new delimeter.
              newLine=str.indexOf(orderNumberDelimiter);//find the first ' '

              //if the ' ' is not found either, set a default value for newLine
              if (newLine < 0)
              {
                //if the string's length < max_order_length, default to string's length
                if (str.length() < MAX_ORDER_LENGTH)
                  newLine = str.length();
                //else, default to max_order_length
                else
                  newLine = MAX_ORDER_LENGTH;
              }
            }

            //truncate and store the order number from position 0 to the value of newLine.
            String orderStr=str.substring(0, newLine);

            if (logger.isDebugEnabled()) {
            	logger.debug("order string: "+orderStr);
            }
            char[] orderCharacters = orderStr.toCharArray();

            for (int j = 0; j < orderCharacters.length; j++) {
                char character = orderCharacters[j];
                if(Character.isLetterOrDigit(character)){
                    orderNumber += character;
                }
            }

        }
        if (logger.isDebugEnabled()) {
            logger.debug("find order number: ["+orderNumber+"]");
        }
        return orderNumber;
    }

    private String trim(String str) {
        return (str != null) ? str.trim() : str;
    }

    public String retrieveOrderComments(String commentID) throws IOException,
            TransformerException, SAXException, ParserConfigurationException,
             Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering retrieveOrderComments");
            logger.debug("commentID : " + commentID);
        }
        String comment = "";
        try {
            comment = ConfigurationUtil.getInstance().getProperty(
                    EmailConstants.ORDER_INFORMATION_CONFIG_FILE, commentID);

        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting retrieveOrderComments");
            }
        }
        return comment;

    }

    /**
     * Call EmailQueueDAO to store Email Queue Value Object in Queue Database.
     *
     * @param emailQueueVO -
     *            QueueVO
     * @return n/a
     * @throws Exception
     */
    public void saveEmailQueue(QueueVO emailQueueVO,  EmailQueueDAO emailDAO) throws IOException,
            SAXException, SQLException, ParserConfigurationException,
             Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering saveEmailQueue");
        }

        try {

            emailDAO.store(emailQueueVO);

        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting saveEmailQueue");
            }
        }

    }

    /**
     * This method iterates through and process individual part of the give
     * multi part.
     *
     * @param mailFolder -
     *            javax.mail.Folder
     * @return n/a
     * @throws Exception
     */
    public String handleMultipart(Multipart multipart)
            throws MessagingException, IOException {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering handleMultipart");
        }

        String bodyPart = "";
        String indPart = "";
        try {
            for (int i = 0, n = multipart.getCount(); i < n; i++) {

                indPart = handlePart(multipart.getBodyPart(i), true);
                bodyPart = bodyPart + "\n" + indPart;
            }

        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting handleMultipart");
            }
        }
        return bodyPart;
    }

    /**
     * This method extracts the text body from message body part.
     *
     * @param mailFolder -
     *            javax.mail.Folder
     * @return n/a
     * @throws Exception
     */
    public String handlePart(Part part, boolean fromMultipart)
            throws MessagingException, IOException {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering handlePart");
        }

        String content = "";

        try {
            /*
             * Get the disposition of the part that describes how the part
             * should be presented to the user.
             */
            String disposition = part.getDisposition();

            /* Capture the content. */
            if (disposition == null || disposition.equalsIgnoreCase("inline")) { // When just body
                Object subpart = part.getContent();// get the content of the
                                                    // body.
                if (subpart instanceof Multipart) { // if it is a Multipart
                	if (logger.isDebugEnabled()) {
                        logger.debug("Subpart is Multipart");
                	}
                    content = this.handleMultipart((Multipart) subpart);
                } else {
                    // Check if plain
                    String contentTypeVal = part.getContentType();
                    if (logger.isDebugEnabled()) {
                        logger.debug("process content type: " + contentTypeVal + " " + fromMultipart);
                    }
                    if (contentTypeVal.equalsIgnoreCase("text")) {
                        content = captureContent(part);
                    } else {

                        ContentType contentType = new ContentType(contentTypeVal);


                        if (contentType.match("text/plain")) {
                            content = captureContent(part);

                        } else if (contentType.match("text/html")
                                && !fromMultipart) {// only retrieve text/html
                                                    // body when the //message
                                                    // is not multipart.
                            content = captureContent(part);
                        } else if (contentType.match("text/html")
                                && fromMultipart) {// only retrieve text/html
                                                    // body when the //message
                                                    // is not multipart.
                            /* do nothing */
                        } else {
                            // ignore any other types. Add comments
                            content = MailboxMonitorConstants.ATTACHMENT_INCLUDED;
                        }
                    }
                }
            } else {
                // discard attachments
                content = MailboxMonitorConstants.ATTACHMENT_INCLUDED;
                if (logger.isDebugEnabled()) {
                    logger.debug("Has disposition: " + disposition);
                }
            }
        } finally {

            if (logger.isDebugEnabled()) {
                logger.debug("Exiting handlePart");
            }
        }
        return content;
    }

    /**
     *
     * @param mailFolder -
     *            javax.mail.Folder
     * @return n/a
     * @throws Exception
     */
    private String captureContent(Part part) throws IOException,
            MessagingException {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering captureContent");
        }

        StringBuffer content = new StringBuffer();
        DataHandler dh = part.getDataHandler();

        // Read the input into a String.
        BufferedReader br = new BufferedReader(new InputStreamReader(dh.getInputStream()));
        try {
            char[] array = new char[8192];
            int n = 0;
            while ((n = br.read(array)) > 0)
            {
              content.append(array, 0, n);
            }
        } finally {

            br.close();

            if (logger.isDebugEnabled()) {
                logger.debug("Exiting captureContent");
            }
        }
        return content.toString();
    }

     /**
     * Look up Queue Database for queues with the same order external number.
     *
      * @param orderExternalNumber -
      *            String
      * @return QueueVO
      * @throws Exception
      */
    private void sendEmail(EmailQueueVO emailQueue, OrderDetailVO ordDetail, EmailQueueDAO emailDAO,
    		MessageGeneratorDAO msgGenDAO, String mailboxName)
           throws Exception
    {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering sendEmail");
            logger.debug("OrderDetailVO: " + ordDetail);
        }
        String mercentCompany = null;
        try
        {
        	// do not send automated email response if the customer has 
        	// enabled out of office response in his/her mailbox
        	if (hasEmailContainsOOO(emailQueue.getSubject(), emailQueue.getBody(), emailDAO.getKeyPhraseList(OOO_MSG_TYPE))) {
        		logger.warn("Recipient has set Out of Office. Automated email response will not be sent");
        		return;
        	}
            PartnerVO partnerVO = null;
            PointOfContactVO pocVO = new PointOfContactVO();
            AutoEmailVO autoEmail = new AutoEmailVO();
            String messageKey = null;
            String defaultSender = null;

            String orderDetailId = (ordDetail!= null ? ordDetail.getOrderDetailID() : "");
            String customerId = (ordDetail) != null ? ordDetail.getCustomerID() : "";
            autoEmail.setCompanyID(ordDetail.getCompanyID());

            // external order number instead?
            autoEmail.setOrderDetailID(orderDetailId);
            if(ordDetail!=null)
            {
               if(ordDetail.getDeliveryDate()!=null)
               {
                 SimpleDateFormat sdf=new SimpleDateFormat("MM/dd/yyyy");
                 autoEmail.setDeliveryDate(sdf.format(ordDetail.getDeliveryDate()));
               }
               autoEmail.setOrderNumber(ordDetail.getExternalOrderNumber());
               autoEmail.setTrackingNumber((ordDetail.getTrackingNumber()) != null ? ordDetail.getTrackingNumber() : "");
            }
            /* retrieve remaining attributes */
            CompanyEmailVO compEmailVO = emailDAO.getCompanyEmailInformation(ordDetail.getOrderDetailID(), null);
            autoEmail.setShipper(compEmailVO.getShipper());
            autoEmail.setCompanyName(compEmailVO.getCompanyName());
            autoEmail.setTrackingURL(compEmailVO.getTrackingURL());
            autoEmail.setPhoneNumber(compEmailVO.getPhoneNumber());

            /* Figure out the source code.  If it is on the order details, use it.
             * otherwise use the default source code from the partner
             */
            if (ordDetail != null && StringUtils.isNotEmpty(ordDetail.getSourceCode()))
            {
                autoEmail.setSourceCode(ordDetail.getSourceCode());
            }
            else
            {
                autoEmail.setSourceCode(emailQueue.getDefaultSourceCode());
            }
            if (logger.isDebugEnabled()) {
                logger.debug("source code = " + autoEmail.getSourceCode());
            }

            /* create e-mail XML */
            Document dataDocXML = (Document) DOMUtil.getDocument(autoEmail.toXML());

            pocVO.setDataDocument(dataDocXML);
            
            ConfigurationUtil cu = ConfigurationUtil.getInstance();
            String companyId = cu.getContentWithFilter(dbConnection, "EMAIL_REQUEST_PROCESSING", "COMPANY_ID", mailboxName, null);
            if (logger.isDebugEnabled()) {
                logger.debug("companyId: " + companyId);
            }
            
            if (companyId != null) {
            	pocVO.setCompanyId(companyId);
            } else {
                pocVO.setCompanyId(EmailConstants.ORDER_SOURCE_FTD);
            }

            /* Determine if the order source code is 
             * associated with a preferred partner.
             */ 
            if (ordDetail != null && StringUtils.isNotEmpty(ordDetail.getSourceCode())) {
                partnerVO = FTDCommonUtils.getPreferredPartnerBySource(ordDetail.getSourceCode());      
            }
            
            /* If the order detail source code is not associated with a 
             * preferred partner determine if the default source code 
             * is associated with a preferred partner.
             */ 
            if (partnerVO == null && StringUtils.isNotEmpty(emailQueue.getDefaultSourceCode())) {
                partnerVO = FTDCommonUtils.getPreferredPartnerBySource(emailQueue.getDefaultSourceCode());      
            }

            /* Set the messageKey/title.
             * Append the preferred partner name to the messageKey if a partner 
             * name exists so the associated preferred partner email is sent.
             */ 
            if (partnerVO != null && StringUtils.isNotEmpty(partnerVO.getPartnerName())) {
                messageKey = EmailConstants.EMAIL_MSG_QC_CO_QUEUE + "." + partnerVO.getPartnerName(); 

                if (logger.isDebugEnabled()) {
                    logger.debug("partnerName: " + partnerVO.getPartnerName());
                    logger.debug("updated messageKey: " + messageKey);
                }
            }else{
                messageKey = EmailConstants.EMAIL_MSG_QC_CO_QUEUE;
            }
           
            // There is no way to get the partner id except for using header detail. The order number may or may not have been populated.
            if(emailQueue != null && emailQueue.getEmailHeader() != null) {
            	if(emailQueue.getEmailHeader().indexOf("mercentamzncustserv") >= 0) {  
            		mercentCompany = new String("AMAZON");
	            	messageKey = EmailConstants.EMAIL_MSG_QC_CO_QUEUE.concat(".").concat("AMAZON");	            	
            	} else if(emailQueue.getEmailHeader().indexOf("mercentebaycustserv") >= 0) {  
            		mercentCompany = new String("EBAY");
	            	messageKey = EmailConstants.EMAIL_MSG_QC_CO_QUEUE.concat(".").concat("EBAY");	            	
            	}
        	}
			            
            pocVO.setLetterTitle(messageKey);


            pocVO.setPointOfContactType(EmailConstants.EMAIL_MESSAGE_TYPE);
            pocVO.setSentReceivedIndicator("O");

            pocVO.setExternalOrderNumber(ordDetail.getExternalOrderNumber());
            pocVO.setMasterOrderNumber(ordDetail.getMasterOrderNumber());
            pocVO.setOrderGuid(ordDetail.getOrderGuid());

            pocVO.setUpdatedBy("SYS");

            if(StringUtils.isNotEmpty(customerId)) {
                 pocVO.setCustomerId(new Long(customerId).longValue());
            }

            // reset order detail VO

            if(StringUtils.isNotEmpty(orderDetailId)) {
               pocVO.setOrderDetailId(new Long(orderDetailId).longValue());
            }

            pocVO.setRecipientEmailAddress(emailQueue.getSenderEmail());
            pocVO.setCommentType(EmailConstants.EMAIL_MESSAGE_TYPE);
            
            if (mercentCompany != null) {            	
            	defaultSender = getDefaultSenderEmail(mercentCompany); 
            } else if (partnerVO == null && (defaultSender == null || defaultSender.length() == 0)) {
                defaultSender = msgGenDAO.loadSenderEmailAddress(pocVO.getLetterTitle(), pocVO.getCompanyId(), pocVO.getOrderDetailId());                
            } else {
                defaultSender = msgGenDAO.loadSenderEmailAddress(pocVO.getLetterTitle(), pocVO.getCompanyId(), pocVO.getOrderDetailId(), partnerVO.getPartnerName());                
            }
           
            pocVO.setSenderEmailAddress(defaultSender);
            // populating email type for ET to find the template
            pocVO.setEmailType(EmailConstants.EMAIL_TYPE_AUTO_REPLY);
            
            //fetch ordervo to populate RecordAttributesXML attribute
          //Populate RecordAttributesXML attribute based on orderguid
            String orderGuid = ordDetail.getOrderGuid();            
            if(StringUtils.isNotEmpty(orderGuid)) {
            	StockMessageGenerator msgGenerator = new StockMessageGenerator(dbConnection);
            	String recordAttributesXML = msgGenerator.generateRecordAttributeXMLFromOrderGuid(orderGuid);
            	pocVO.setRecordAttributesXML(recordAttributesXML);
            } else {            	
            	if (logger.isDebugEnabled())
                {
                    logger.debug("Not populating RecordAttributesXML as there is no orderguid information");
                }
            }
            String sourceCode = ordDetail.getSourceCode();
            if(StringUtils.isNotEmpty(sourceCode)) {
            	pocVO.setSourceCode(sourceCode);
            } else {
            	pocVO.setSourceCode(emailQueue.getDefaultSourceCode());
            }                        
            
            emailList.add(pocVO);
        }
        finally
        {
             if (logger.isDebugEnabled())
             {
                 logger.debug("Exiting sendEmail");
             }
        }
    }

    /** Get the default sender email for the given partner name/ channel name
     * @param partnerName
     * @return
     */
    private String getDefaultSenderEmail(String partnerName) {  
		try {			
			StringBuffer fromEmailAddress = null;
			String domain = ConfigurationUtil.getInstance().getFrpGlobalParm("EMAIL_ADMIN_CONFIG", "MERCENT_CUSTSERV_EMAIL_DOMAIN");
			final String mercent = "MERCENT ";
			StringBuffer mercentConfigParam = new StringBuffer();			
			mercentConfigParam.append(mercent).append(partnerName).append(_EMAIL_MAILBOX_USER);
					
			if(logger.isDebugEnabled()) {
				logger.debug("mercentConfigParam: " + mercentConfigParam);
			}
					
			fromEmailAddress = new StringBuffer(ConfigurationUtil.getInstance().getSecureProperty("email_request_processing", mercentConfigParam.toString())).append("@").append(domain);
					
			if(logger.isDebugEnabled()) {
				logger.debug("From Email Address: " + fromEmailAddress.toString());
			}
			 return fromEmailAddress.toString();
		} catch (Exception e) {
			logger.error("Unable to get mercent Partner Address email : " + e.getMessage());			
		}  
		return null;
	}

	/**
     * This method checks to see if the order number follows a series of
     * phrases.  If it is found, it is returned, an empty string is returned if
     * no order number is found.
     *
     * @param emailQueue The email queue value object to search through.
     * @return A string representing the order number extracted from the body,
     * an empty string if none found.
     * @author Andy Liakas, 08/31/2006
     */
    private String extractOrderNumberFromBody(EmailQueueVO emailQueue) {

        /* Build array list of phrases to look for the order number after. */
        ArrayList searchStrings = new ArrayList();
        searchStrings.add("flower order");
        searchStrings.add("number is");
        searchStrings.add("order number is");
        searchStrings.add("order number");
        searchStrings.add("order");
        searchStrings.add("number");
        searchStrings.add("#");
        searchStrings.add("reference number");
        searchStrings.add("Shipment Confirmation");

        String orderNumber = StringUtils.EMPTY;

        /* Loop through search strings, looking for following order number. */
        for(int i = 0; i < searchStrings.size(); i++) {
            orderNumber = extractOrderNumberFromBody(emailQueue.getBody(), (String)searchStrings.get(i));

            /* If order number was found, concatenate a W and return it. */
            if(StringUtils.isNotEmpty(orderNumber)                &&
               orderNumber.length() >= WALMART_MASTER_ORDER_MIN_LENGTH   &&
               orderNumber.length() <= WALMART_MASTER_ORDER_MAX_LENGTH
              )
            {
                return 'W' + orderNumber;
            }
        }

        return orderNumber;
    }


    /**
     * This method searches for a search string within an e-mail body.  If
     * found, an order number is searched for following the search string.  The
     * order number is returned if found, an empty string is returned if it is
     * not found.
     *
     * @param body The e-mail body to search for the order number.
     * @param searchString The search string to look for the order number after.
     * @return A string representing the order number extracted from the body,
     * an empty string if none found.
     * @author Andy Liakas, 08/31/2006
     */
    private String extractOrderNumberFromBody(String body, String searchString) {

        /* Find index of instance of search string in e-mail body. */
        int start = body.toUpperCase().indexOf(searchString.toUpperCase());

        /* If search string not found, return null, else move index up
         * past search string.
         */
        if(start < 0) {
            return StringUtils.EMPTY;
        }
        else {
            start += searchString.length();
        }

        /* Remove leading and trailing whitespace.
         * Create order number buffer variable.
         */
        StringBuffer orderNumber = new StringBuffer();
        orderNumber.append(body.substring(start).trim());

        /* Remove any characters before the first digit or letter. */
        for(int i = 0; i < orderNumber.length();) {
            if(!Character.isDigit(orderNumber.charAt(i))) {
                orderNumber.deleteCharAt(i);
            }
            else {
                break;
            }
        }

        /* Cut down size of e-mail body we're working with by cutting off at
         * the first instance of whitespace or new line character.  If there is
         * no whitespace or new line character, work with the whole body (order
         * number is at end of body).
         */
        if (orderNumber.toString().indexOf('\n') >= 0) {
            orderNumber.delete(orderNumber.toString().indexOf('\n'), orderNumber.length());
        }
        if (orderNumber.toString().indexOf(' ') >= 0) {
            orderNumber.delete(orderNumber.toString().indexOf(' '), orderNumber.length());
        }

        /* Remove any characters after the last digit or letter. */
        for(int i = orderNumber.length() - 1; i >= 0; i--) {
            if(!Character.isDigit(orderNumber.charAt(i))) {
                orderNumber.deleteCharAt(i);
            }
            else {
                break;
            }
        }

        /* Check to make sure order number is numeric. */
        try {
            Long.parseLong(orderNumber.toString());
        }
        catch(NumberFormatException nfe) {
            return StringUtils.EMPTY;
        }

        return orderNumber.toString();
    }

    /**
     * This method obtains the default source code to be used for unattached
     * emails.
     *
     * @param mailboxName Mailbox name.
     * @param mailboxProps Mailbox properties.
     * @return String Default source code.
     */
    public String getDefaultSourceCode(String mailboxName, Map mailboxProps) {
        String partnerName = null;
        PartnerVO partnerVO = null;
        
        // If the mailbox name or properties is empty return null
        if(StringUtils.isEmpty(mailboxName) || mailboxProps == null){
            return null;
        }
        
        // Obtain the default partner name associated with the mailbox
        partnerName = (String)mailboxProps.get(mailboxName + " DEFAULT PARTNER NAME");

        // If the parter name is empty return null
        if(StringUtils.isEmpty(partnerName)){
            return null;
        }
        
        // Obtain the associated partner information
        partnerVO = FTDCommonUtils.getPreferredPartnerByName(partnerName);

        // If the partner exists return the default source code
        if(partnerVO != null){
            return partnerVO.getDefaultWebSourceCode();
        }

        return null;        
    }
    
    /**
     * Checks for the list of possible out of office strings present in the 
     * email subject/body.  
     * 
     * @param subject
     * @param body
     * @param oooList
     * @return
     */
    private boolean hasEmailContainsOOO(String subject, String body, List<String> oooList) {
    	
    	if (oooList != null && !oooList.isEmpty()) {
	    	for(String ooo : oooList) {
	    		if(body.toLowerCase().contains(ooo.toLowerCase()) || subject.toLowerCase().contains(ooo.toLowerCase())) {
	    			return true;
	    		}
	    	}
    	}
    	return false;
    }
}
