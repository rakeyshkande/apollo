package com.ftd.queue.email.cache.handler;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;

public class EmailQueuePriorityCacheHandler extends CacheHandlerBase 
{
  
  private Map queuePriorityMap = null;
  
  public EmailQueuePriorityCacheHandler()
  {
  }
  
  
   /**
     * load queue configuration data
     * 
     * @param Connection      - database connection
     * @return Object         - map of configuration information
     * @throws CacheException - cachecontroller error
     */
    public Object load(Connection conn) throws CacheException
    {   
    
        if(logger.isDebugEnabled()){
            logger.debug("Entering load");
            
        }
        Map cachedEmailQueueMap = new HashMap();
        try
        {
            // build DataRequest object
            DataRequest request = new DataRequest();
            request.reset();
            request.setConnection(conn);
            HashMap inputParams = new HashMap();
            inputParams.put("IN_QUEUE_INDICATOR", 
                "Email");
            request.setInputParams(inputParams);
            request.setStatementID("GET_TYPES_BY_QUEUE_INDICATOR");

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();

            CachedResultSet queueConfigContextRS = (CachedResultSet) dau.execute(request);
            while(queueConfigContextRS.next())      // get queue config context parms
            {
                cachedEmailQueueMap.put(queueConfigContextRS.getString("queue_type"),
                                   new Integer(queueConfigContextRS.getInt("priority")));
            }
        }
       catch(Exception e){
            logger.error(e);
            throw new CacheException(e.toString());
       }
       finally
        {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting load");
            }
        }
        return cachedEmailQueueMap;
    }
    
    
    /**
     * set cached object
     * 
     * @param Object  - cached object
     * @return void
     * @throws CacheException
     */
    public void setCachedObject(Object cachedObject) throws CacheException
    {
        queuePriorityMap = (Map) cachedObject;
        return;
    }

    
    
     public int getQueuePriority(String queueType)
     {
        if(this.queuePriorityMap!=null&&this.queuePriorityMap.get(queueType)!=null)
        {
          return ((Integer)this.queuePriorityMap.get(queueType)).intValue();
          
        }else
        {
          return 0;
        }
       
     }
  
}
