/**
* Value Object for inquiry details.
* 
* Class Name:   InquiryDetailVO
* Revision:     1.0
* Date:         February 24, 2005
* Author:       Charles Fox, Software Architects, Inc.
*/

package com.ftd.queue.email.vo;

import java.sql.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;

/**
 * This value object class contains data received from Novator.
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 */
 
public class InquiryDetailVO 
{

    private String recipientFirstName = "";
    private String recipientLastName = "";
    private String recipientAddress = "";
    private String recipientCity = "";
    private String recipientState = "";
    private String recipientZip = "";
    private String recipientCountry = "";
    private String recipientPhone = "";
    private Date newDeliveryDateOne;
    private Date newDeliveryDateTwo;
    private String cardMessage = "";
    private String cardSignature = "";
    private String otherComment = "";
    private String productSku = "";
    private String institution = "";
    private String requestSubType = "";
    
    public InquiryDetailVO()
    {
        super();
    }
    
    public String getRecipientFirstName() {
        return recipientFirstName;
    }
    
    public void setRecipientFirstName(String newRecipientFirstName) {
        recipientFirstName = trim(newRecipientFirstName);
    }
    
    public String getRecipientLastName() {
        return recipientLastName;
    }

    public void setRecipientLastName(String newRecipientLastName) {
        recipientLastName = trim(newRecipientLastName);
    }
    
    public String getRecipientAddress() {
        return recipientAddress;
    }
    
    public void setRecipientAddress(String newRecipientAddress) {
        recipientAddress = trim(newRecipientAddress);
    }
    
    public String getRecipientCity() {
        return recipientCity;
    }
    
    public void setRecipientCity(String newRecipientCity) {
        recipientCity = trim(newRecipientCity);
    }
    
    public String getRecipientState() {
        return recipientState;
    }
    
    public void setRecipientState(String newRecipientState) {
        recipientState = trim(newRecipientState);
    }
    
    public String getRecipientZip() {
        return recipientZip;
    }
    
    public void setRecipientZip(String newRecipientZip) {
        recipientZip = trim(newRecipientZip);
    }
    
    public String getRecipientCountry() {
        return recipientCountry;
    }
    
    public void setRecipientCountry(String newRecipientCountry) {
        recipientCountry = trim(newRecipientCountry);
    }
    
    public String getRecipientPhone() {
        return recipientPhone;
    }
    
    public void setRecipientPhone(String newRecipientPhone) {
        recipientPhone = trim(newRecipientPhone);
    }
    
    public Date getNewDeliveryDateOne() {
        return newDeliveryDateOne;
    }
    
    public void setNewDeliveryDateOne(Date newNewDeliveryDateOne) {
        newDeliveryDateOne = newNewDeliveryDateOne;
    }

    public Date getNewDeliveryDateTwo() {
        return newDeliveryDateTwo;
    }
    
    public void setNewDeliveryDateTwo(Date newNewDeliveryDateTwo) {
        newDeliveryDateTwo = newNewDeliveryDateTwo;
    }
    
    public String getCardMessage() {
        return cardMessage;
    }
    
    public void setCardMessage(String newCardMessage) {
        cardMessage = trim(newCardMessage);
    }
    
    public String getOtherComment() {
        return otherComment;
    }
    
    public void setOtherComment(String newOtherComment) {
        otherComment = trim(newOtherComment);
    }
    
    private String trim(String str)
    {
        return (str != null)?str.trim():str;
    }

    public String getCardSignature() {
        return cardSignature;
    }
    
    public void setCardSignature(String newCardSignature) {
        cardSignature = trim(newCardSignature);
    }
      
    public String getProductSku() {
        return productSku;
    }
    
    public void setProductSku(String newProductSku) {
        productSku = trim(newProductSku);
    }

    public String getInstitution() {
        return institution;
    }
    
    public void setInstitution(String newInstitution) {
        institution = trim(newInstitution);
    }

    public String getRequestSubType() {
        return requestSubType;
    }
    
    public void setRequestSubType(String newRequestSubType) {
        requestSubType = trim(newRequestSubType);
    } 
    
    
    
    public String toString(){
    
      StringBuffer sb=new StringBuffer();
      try
      {
        Map map = PropertyUtils.describe(this);
        Iterator it = map.keySet().iterator();

        while (it.hasNext()) {
            String key = (String) it.next();
            Object value = map.get(key);

            if (value != null) {
                if (!key.equalsIgnoreCase("class")) {
                    sb.append("[" + key + "]\t value =[");

                    if (value instanceof List) {
                        
                    } else{
                        sb.append(value.toString());
                    } 

                    sb.append("]");
                }
            }
        }
      }catch(Exception e)
      {
        
      }
      
      return sb.toString();
    
    }
  
}
