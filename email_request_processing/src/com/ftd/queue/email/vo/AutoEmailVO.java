package com.ftd.queue.email.vo;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import com.ftd.messagegenerator.vo.XMLInterface;
import com.ftd.osp.utilities.xml.DOMUtil;

public class AutoEmailVO 
{
    public AutoEmailVO()
    {
    }
    
    private String firstName = "";
    private String lastName = "";
    private String orderDetailID = "";
    private String companyID = "";
    private String companyName = "";
    private String trackingURL = "";
    private String trackingNumber = "";
    private String shipper = "";
    private String orderChange = "";
    private String deliveryDate="";
    private String phoneNumber = "";
    private String orderNumber="";
    private String sourceCode="";
    private String languageId = "";
    
      public void setPhoneNumber(String newPhoneNumber)
      {
        this.phoneNumber = newPhoneNumber;
      }
    
    
      public String getPhoneNumber()
      {
        return phoneNumber;
      }
      
      public void setDeliveryDate(String newDeliveryDate)
      {
        this.deliveryDate = newDeliveryDate;
      }
    
    
      public String getDeliveryDate()
      {
        return deliveryDate;
      }
      
      public void setOrderChange(String newOrderChange)
      {
        this.orderChange = newOrderChange;
      }
    
    
      public String getOrderChange()
      {
        return orderChange;
      }
      
      public void setShipper(String newShipper)
      {
        this.shipper = newShipper;
      }
    
    
      public String getShipper()
      {
        return shipper;
      }
      

    
      public void setTrackingNumber(String newTrackingNumber)
      {
        this.trackingNumber = newTrackingNumber;
      }
    
    
      public String getTrackingNumber()
      {
        return trackingNumber;
      }
      
      public void setTrackingURL(String newTrackingURL)
      {
        this.trackingURL = newTrackingURL;
      }
    
    
      public String getTrackingURL()
      {
        return trackingURL;
      }
      
      public void setCompanyName(String newCompanyName)
      {
        this.companyName = newCompanyName;
      }
    
    
      public String getCompanyName()
      {
        return companyName;
      }
      
      public void setCompanyID(String newCompanyID)
      {
        this.companyID = newCompanyID;
      }
    
    
      public String getCompanyID()
      {
        return companyID;
      }
    
      public void setOrderDetailID(String newOrderDetailID)
      {
        this.orderDetailID = newOrderDetailID;
      }
    
    
      public String getOrderDetailID()
      {
        return orderDetailID;
      }
    
      public void setLastName(String newLastName)
      {
        this.lastName = newLastName;
      }
    
    
      public String getLastName()
      {
        return lastName;
      }
    
      public void setFirstName(String newFirstName)
      {
        this.firstName = newFirstName;
      }
    
    
      public String getFirstName()
      {
        return firstName;
      }
  
      /**
        * This method uses the Reflection API to generate an XML string that will be
        * passed back to the calling module.
        * The XML string will contain all the fields within this VO, including the
        * variables as well as (a collection of) ValueObjects.
        *
        * @param  None
        * @return XML string
       **/
      public String toXML() throws Exception
      {
        StringBuffer sb = new StringBuffer();
        try
        {
          sb.append("<autoemail>");
          Field[] fields = this.getClass().getDeclaredFields();
    
          appendFields(sb, fields);
          sb.append("</autoemail>");
        }
    
        finally{}
    
        return sb.toString();
      }
    
      protected void appendFields(StringBuffer sb, Field[] fields) throws IllegalAccessException, ClassNotFoundException
      {
        for (int i = 0; i < fields.length; i++)
        {
          //if the field retrieved was a list of VO
          if(fields[i].getType().equals(Class.forName("java.util.List")))
          {
            List list = (List)fields[i].get(this);
            if(list != null)
            {
              for (int j = 0; j < list.size(); j++)
              {
                XMLInterface xmlInt = (XMLInterface)list.get(j);
                String sXmlVO = xmlInt.toXML();
                sb.append(sXmlVO);
              }
            }
          }
          else
          {
            //if the field retrieved was a VO
            if (fields[i].getType().toString().matches("(?i).*vo"))
            {
              XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
              String sXmlVO = xmlInt.toXML();
              sb.append(sXmlVO);
            }
            //if the field retrieved was a Calendar object
            else if (fields[i].getType().toString().matches("(?i).*calendar"))
            {
              Date date;
              String fDate = null;
              if (fields[i].get(this) != null)
              {
                date = (((GregorianCalendar)fields[i].get(this)).getTime());
                SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
                fDate = sdf.format(date).toString();
              }
              sb.append("<" + fields[i].getName() + ">");
              sb.append(fDate);
              sb.append("</" + fields[i].getName() + ">");
            }
            else
            {
              
              sb.append("<" + fields[i].getName() + ">");
              String value = "";
              if(fields[i].get(this)!=null){
                  value = fields[i].get(this).toString();    
                  value = DOMUtil.encodeChars(value);
              }
              else
              {
                  value = "";    
              }
                  
              sb.append(value);
              sb.append("</" + fields[i].getName() + ">");
            }
          }
        }
      }
     
  
        private String trim(String str)
        {
            return (str != null)?str.trim():str;
        } 


  public void setOrderNumber(String orderNumber)
  {
    this.orderNumber = orderNumber;
  }


  public String getOrderNumber()
  {
    return orderNumber;
  }

  public void setSourceCode(String sourceCode)
  {
      this.sourceCode = sourceCode;
  }
  
  public String getSourceCode()
  {
      return sourceCode;
  }
    
  public String getLanguageId() {
		return languageId;
  }

  public void setLanguageId(String languageId) {
		this.languageId = languageId;
  }

}