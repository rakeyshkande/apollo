package com.ftd.queue.email.vo;

public class CompanyEmailVO 
{
    public CompanyEmailVO()
    {
    }
    
    private String companyName = "";
    private String companyID = "";
    private String shipper = "";
    private String trackingURL = "";
    private String phoneNumber = "";
    
      public void setPhoneNumber(String newPhoneNumber)
      {
        this.phoneNumber = newPhoneNumber;
      }
    
    
      public String getPhoneNumber()
      {
        return phoneNumber;
      }

      public void setTrackingURL(String newTrackingURL)
      {
        this.trackingURL = newTrackingURL;
      }
    
    
      public String getTrackingURL()
      {
        return trackingURL;
      } 

      public void setShipper(String newShipper)
      {
        this.shipper = newShipper;
      }
    
    
      public String getShipper()
      {
        return shipper;
      } 

      public void setCompanyID(String newCompanyID)
      {
        this.companyID = newCompanyID;
      }
    
    
      public String getCompanyID()
      {
        return companyID;
      } 
    
      public void setCompanyName(String newCompanyName)
      {
        this.companyName = newCompanyName;
      }
    
    
      public String getCompanyName()
      {
        return companyName;
      } 
}