package com.ftd.queue.email.vo;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import com.ftd.messagegenerator.vo.XMLInterface;
import com.ftd.osp.utilities.xml.DOMUtil;


/**
 * PaymentVO
 * Stores payment information
 */
public class PaymentVO 
{
  private	String	    paymentType;

  public void setPaymentType(String paymentType)
  {
    this.paymentType = paymentType;
  }

  public String getPaymentType()
  {
    return paymentType;
  }

  /**
    * This method uses the Reflection API to generate an XML string that will be
    * passed back to the calling module.
    * The XML string will contain all the fields within this VO, including the
    * variables as well as (a collection of) ValueObjects.
    *
    * @param  None
    * @return XML string
   **/
  public String toXML() throws Exception
  {
    StringBuffer sb = new StringBuffer();
    try
    {
      sb.append("<PaymentVO>");
      Field[] fields = this.getClass().getDeclaredFields();
  
      appendFields(sb, fields);
      sb.append("</PaymentVO>");
    }
  
    finally{}
  
    return sb.toString();
  }
  
  protected void appendFields(StringBuffer sb, Field[] fields) throws IllegalAccessException, ClassNotFoundException
  {
    for (int i = 0; i < fields.length; i++)
    {
      //if the field retrieved was a list of VO
      if(fields[i].getType().equals(Class.forName("java.util.List")))
      {
        List list = (List)fields[i].get(this);
        if(list != null)
        {
          for (int j = 0; j < list.size(); j++)
          {
            XMLInterface xmlInt = (XMLInterface)list.get(j);
            String sXmlVO = xmlInt.toXML();
            sb.append(sXmlVO);
          }
        }
      }
      else
      {
        //if the field retrieved was a VO
        if (fields[i].getType().toString().matches("(?i).*vo"))
        {
          XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
          String sXmlVO = xmlInt.toXML();
          sb.append(sXmlVO);
        }
        //if the field retrieved was a Calendar object
        else if (fields[i].getType().toString().matches("(?i).*calendar"))
        {
          Date date;
          String fDate = null;
          if (fields[i].get(this) != null)
          {
            date = (((GregorianCalendar)fields[i].get(this)).getTime());
            SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
            fDate = sdf.format(date).toString();
          }
          sb.append("<" + fields[i].getName() + ">");
          sb.append(fDate);
          sb.append("</" + fields[i].getName() + ">");
        }
        else
        {
          
          sb.append("<" + fields[i].getName() + ">");
          String value = "";
          if(fields[i].get(this)!=null){
              value = fields[i].get(this).toString();    
              value = DOMUtil.encodeChars(value);
          }
          else
          {
              value = "";    
          }
              
          sb.append(value);
          sb.append("</" + fields[i].getName() + ">");
        }
      }
    }
  }
}
