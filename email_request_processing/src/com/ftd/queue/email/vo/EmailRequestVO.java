/**
* Value Object for e-mail requests.
* 
* Class Name:   EmailRequestVO
* Revision:     1.0
* Date:         February 24, 2005
* Author:       Charles Fox, Software Architects, Inc.
*/

package com.ftd.queue.email.vo;

import java.sql.Date;
import java.sql.Timestamp;

/**
 * Value Object that represents the attributes for a e-mail request received 
 * in XML format from Novator.  
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 */

public class EmailRequestVO 
{

    private String sequenceNumber = "";
    private String companyID = "";
    private String orderNumber = "";
    private String sourceCode = "";
    private String requestType = "";
    private Date deliveryDateOne;
    private String billingFirstName = "";
    private String billingLastName = "";
    private String dayPhone = "";
    private String evePhone = "";
    private String email = ""; 
    private String comment = "";
    private String subType = "";
    private String serverID = "";   
    private String serviceIdentifier = "";
    private InquiryDetailVO inquiryVO;
    private OrderDetailVO ordDetailVO;
    private Timestamp timeStamp;
    private Date deliveryDateTwo;
    
    private String pointOfContactId;
       
      /**
     * constructor
     * @param n/a
     * @return n/a
     * @throws none
     */
    public EmailRequestVO()
    {
        super();
    }
    
    public String getSequenceNumber() {
        return sequenceNumber;
    }
    
    public void setSequenceNumber(String newSequenceNumber) {
        sequenceNumber = trim(newSequenceNumber);
    }

    public String getCompanyID() {
        return companyID;
    }
    
    public void setCompanyID(String newCompanyID) {
        companyID = trim(newCompanyID);
    }
    
    public String getOrderNumber() {
        return orderNumber;
    }
    
    public void setOrderNumber(String newOrderNumber) {
        orderNumber = trim(newOrderNumber);
    }
    
    public String getSourceCode() {
        return sourceCode;
    }
    
    public void setSourceCode(String newSourceCode) {
        sourceCode = trim(newSourceCode);
    }

    public String getRequestType() {
        return requestType;
    }
    
    public void setRequestType(String newRequestType) {
        requestType = trim(newRequestType);
    }
    
    public Date getDeliveryDateOne() {
        return deliveryDateOne;
    }

    public void setDeliveryDateOne(Date newDeliveryDateOne) {
        deliveryDateOne = newDeliveryDateOne;
    }
    
    public String getBillingFirstName() {
        return billingFirstName;
    }
    
    public void setBillingFirstName(String newBillingFirstName) {
        billingFirstName = trim(newBillingFirstName);
    }
    
    public String getBillingLastName() {
        return billingLastName;
    }
    
    public void setBillingLastName(String newBillingLastName) {
        billingLastName = trim(newBillingLastName);
    }
    
    public String getDayPhone() {
        return dayPhone;
    }
    
    public void setDayPhone(String newDayPhone) {
        dayPhone = trim(newDayPhone);
    }  
    
    public String getEvePhone() {
        return evePhone;
    }

    public void setEvePhone(String newEvePhone) {
        evePhone = trim(newEvePhone);
    }  
    
    public String getEmail() {
        return email;
    }
    
    public void setEmail(String newEmail) {
        email = trim(newEmail);
    }

    public String getComment() {
        return comment;
    }
    
    public void setComment(String newComment) {
        comment = trim(newComment);
    }
    
    public String getSubType() {
        return subType;
    }
    
    public void setSubType(String newSubType) {
        subType = trim(newSubType);
    }

    public String getServerID() {
        return serverID;
    }
    
    public void setServerID(String newServerID) {
        serverID = trim(newServerID);
    }
    
    
    public String getServiceIdentifier() {
        return serviceIdentifier;
    }
    
    public void setServiceIdentifier(String newServiceIdentifier) {
        serviceIdentifier = trim(newServiceIdentifier);
    }
    
    public InquiryDetailVO getInquiryVO() {
        return inquiryVO;
    }
    
    public void setInquiryVO(InquiryDetailVO newInquiryVO) {
        inquiryVO = newInquiryVO;
    }

    public OrderDetailVO getOrderDetailVO() {
        return ordDetailVO;
    }
    
    public void setOrderDetailVO(OrderDetailVO newOrdDetailVO) {
        ordDetailVO = newOrdDetailVO;
    }
    
    public Timestamp getTimeStamp() {
        return timeStamp;
    }
    
    public void setTimeStamp(Timestamp newTimeStamp) {
        timeStamp = newTimeStamp;
    }

    public Date getDeliveryDateTwo() {
        return deliveryDateTwo;
    }
    
    public void setDeliveryDateTwo(Date newDeliveryDateTwo) {
        deliveryDateTwo = newDeliveryDateTwo;
    }     

    private String trim(String str)
    {
        return (str != null)?str.trim():str;
    }
    
    public String toString()
    {
      
      StringBuffer sbf=new StringBuffer();
      sbf.append("Order Number ="+this.orderNumber);
      sbf.append("request type ="+this.requestType);
      
      return sbf.toString();
      
    }


  public void setPointOfContactId(String pointOfContactId)
  {
    this.pointOfContactId = pointOfContactId;
  }


  public String getPointOfContactId()
  {
    return pointOfContactId;
  }
}