package com.ftd.queue.email.vo;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

/**
 * Value Object that represents the attributes for an entry in the ASK Event 
 * Handler Table
 * Mercury or Venus.  
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 */
public class ASKEventHandlerVO 
{
    private String fillingFloristId = "";
    private String orderDetailID = "";
    private String messageType = "";
    private String queueType="";
    private String pointOfContactId="";
    private String sendEmail="";
    private String mercuryMessageId="";
    private String askEventLogId="";
    private Date receivedDate;
    /*inbound ask/ans message flag | rej/can message flag | live ftd flag
        example:
        Y|N|Y
    */
    private String mercuryStatusFlags="";
    
    public ASKEventHandlerVO()
    {
    }
    
    public String getOrderDetailID() {
        return orderDetailID;
    }
    
    public void setOrderDetailID(String newOrderDetailID) {
        orderDetailID = trim(newOrderDetailID);
    } 
    
    public String getFillingFloristId() {
        return fillingFloristId;
    }

    public void setFillingFloristId(String newFillingFloristId) {
        fillingFloristId = trim(newFillingFloristId);
    }
    
    public String getMessageType() {
        return messageType;
    }
    
    public void setMessageType(String newMessageType) {
        messageType = trim(newMessageType);
    }

    private String trim(String str)
    {
        return (str != null)?str.trim():str;
    }


  public void setQueueType(String queueType)
  {
    this.queueType = queueType;
  }


  public String getQueueType()
  {
    return queueType;
  }


  public void setPointOfContactId(String pointOfContactId)
  {
    this.pointOfContactId = pointOfContactId;
  }


  public String getPointOfContactId()
  {
    return pointOfContactId;
  }


  public void setSendEmail(String sendEmail)
  {
    this.sendEmail = sendEmail;
  }


  public String getSendEmail()
  {
    return sendEmail;
  }


  public void setMercuryMessageId(String mercuryMessageId)
  {
    this.mercuryMessageId = mercuryMessageId;
  }


  public String getMercuryMessageId()
  {
    return mercuryMessageId;
  }


  public void setAskEventLogId(String askEventLogId)
  {
    this.askEventLogId = askEventLogId;
  }


  public String getAskEventLogId()
  {
    return askEventLogId;
  }


  public void setMercuryStatusFlags(String mercuryStatusFlags)
  {
    this.mercuryStatusFlags = mercuryStatusFlags;
  }


  public String getMercuryStatusFlags()
  {
    return mercuryStatusFlags;
  }
  
  
  /**
   * inbound ask/ans message flag | rej/can message flag | live ftd flag
   *  example:    Y|N|Y
   * @return 
   */
  public boolean isAnswered()
  {
     if(StringUtils.isNotEmpty(this.mercuryStatusFlags))
     {
       //the first flag.
      
       return this.mercuryStatusFlags.substring(0,1).equalsIgnoreCase("Y");
     }else
     {
       return false;
     }
    
  }
  
  
  /**
   * inbound ask/ans message flag | rej/can message flag | live ftd flag
   *  example:    Y|N|Y
   * @return 
   */
  public boolean isFTDRejectedOrCancelled()
  {
     if(StringUtils.isNotEmpty(this.mercuryStatusFlags))
     {
       //the first flag.
       if(this.mercuryStatusFlags.length()>2){
           
           return this.mercuryStatusFlags.substring(2,3).equalsIgnoreCase("Y");
       }else
       {
         return false;
       }
     }else
     {
       return false;
     }
    
  }
  
  /**
   * inbound ask/ans message flag | rej/can message flag | live ftd flag
   *  example:    Y|N|Y
   * @return 
   */
  public boolean orderHasLiveFTD()
  {
     if(StringUtils.isNotEmpty(this.mercuryStatusFlags))
     {
       //the first flag.
       if(this.mercuryStatusFlags.length()>4){
            
           return this.mercuryStatusFlags.substring(4).equalsIgnoreCase("Y");
       }else
       {
         return false;
       }
     }else
     {
       return false;
     }
    
  }
  
  
  public String toString()
  {
    
    return "ASK EVENT LOG ID ["+this.askEventLogId+"] mercury status flags ["+this.mercuryStatusFlags+"]";
  }


  public void setReceivedDate(Date receivedDate)
  {
    this.receivedDate = receivedDate;
  }


  public Date getReceivedDate()
  {
    return receivedDate;
  }
  
  
  
    
  
  
}