/**
* Value Object for order details.
* 
* Class Name:   InquiryDetailVO
* Revision:     1.0
* Date:         February 24, 2005
* Author:       Charles Fox, Software Architects, Inc.
*/

package com.ftd.queue.email.vo;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import com.ftd.queue.email.constants.EmailConstants;

/**
 * This value object class contains information about an order.
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 */
 
public class OrderDetailVO 
{

    private String source = "";
    private String externalOrderNumber = "";
    private String orderGuid = "";
    private String masterOrderNumber = "";
    private String orderDetailID = "";
    private String companyID = "";
    private String sourceCode = "";
    private Date orderDate;
    private Date deliveryDate;
    
    private String orderShipped = "";
    private String cancelSentFlag = "";
    private String fillingFloristId = "";
    private String trackingNumber = "";
    private String recipientLocationType = "";
    private String timeZone = "";
    private String cardMessage = "";
    private String cardSignature = "";
    private String institutionName = "";
    private String receipientFirstName = "";
    private String receipientLastName = "";
    private String orderDisposition = "";
    private String orderIndicator = "";
    private String customerID = "";
    
    private String recipientAddress = "";
    private String recipientCity = "";
    private String recipientPhone = "";
    private String recipientState = "";
    private String recipientZip = "";
    private String recipientCountry = "";
    private String productSku = "";
    private String vendorFlag = "";
    private String mercuryFlag = "";
    private boolean orderFTDM;

    private String recipientID = "";
    private String substitutionIndicator = "";
    private String occasion = "";
    private String releaseInfoIndicator = "";
    private String specialInstructions = "";
    private String quantity = "";
    
    private String color1 = "";
    private String color2 = "";
    private String sameDayGift = "";
    private String shipMethod = "";
    private Date shipDate;
    
    private String liveFTDFlag="";
    private String attemptedFTDFlag="";
    private String attemptedCANFlag="";
    private boolean ftpVendor;
    private String rejectedFlag="";
       
    private String ftdStatus="";
    
    private boolean personalized = false;
    private String languageId;

	public OrderDetailVO()
    {
        super();
    }

    public String getSource() {
        return source;
    }
    
    public void setSource(String newSource) {
        source = trim(newSource);
    }
    
    public String getExternalOrderNumber() {
        return externalOrderNumber;
    }
    
    public void setExternalOrderNumber(String newExternalOrderNumber) {
        externalOrderNumber = trim(newExternalOrderNumber);
    }

    public String getOrderGuid() {
        return orderGuid;
    }
    
    public void setOrderGuid(String newOrderGuid) {
        orderGuid = trim(newOrderGuid);
    }      
    
    public String getMasterOrderNumber() {
        return masterOrderNumber;
    }
    
    public void setMasterOrderNumber(String newMasterOrderNumber) {
        masterOrderNumber = trim(newMasterOrderNumber);
    }       
    
    public String getOrderDetailID() {
        return orderDetailID;
    }
    
    public void setOrderDetailID(String newOrderDetailID) {
        orderDetailID = trim(newOrderDetailID);
    }   

    public String getCompanyID() {
        return companyID;
    }
    
    public void setCompanyID(String newCompanyID) {
        companyID = trim(newCompanyID);
    }    
    
    public Date getOrderDate() {
        return orderDate;
    }
    
    public void setOrderDate(Date newOrderDate) {
        orderDate = newOrderDate;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }
    
    public void setDeliveryDate(Date newDeliveryDate) {
        deliveryDate = newDeliveryDate;
    }             
    
    public void setSourceCode(String newSourceCode) {
        this.sourceCode = trim(newSourceCode);
    }

    public String getSourceCode() {
        return sourceCode;
    }

    public String getOrderShipped() {
        return orderShipped;
    }
    
    public void setOrderShipped(String newOrderShipped) {
        orderShipped = trim(newOrderShipped);
    }     
    

    public String getTrackingNumber() {
        return trackingNumber;
    }
    
    public void setTrackingNumber(String newTrackingNumber) {
        trackingNumber = trim(newTrackingNumber);
    }     

    public String getRecipientLocationType() {
        return recipientLocationType;
    }
    
    public void setRecipientLocationType(String newRecipientLocationType) {
        recipientLocationType = trim(newRecipientLocationType);
    }       

    public String geTimeZone() {
        return timeZone;
    }
    
    public void setTimeZone(String newTimeZone) {
        timeZone = trim(newTimeZone);
    }      

    public String getFillingFloristId() {
        return fillingFloristId;
    }

    public void setFillingFloristId(String newFillingFloristId) {
        fillingFloristId = trim(newFillingFloristId);
    }

    public String getReceipientFirstName() {
        return receipientFirstName;
    }
    
    public void setReceipientFirstName(String newReceipientFirstName) {
        receipientFirstName = trim(newReceipientFirstName);
    }
    
    public String getReceipientLastName() {
        return receipientLastName;
    }

    public void setReceipientLastName(String newReceipientLastName) {
        receipientLastName = trim(newReceipientLastName);
    }


    public String getOrderDisposition() {
        return orderDisposition;
    }

    public void setOrderDisposition(String newOrderDisposition) {
        orderDisposition = trim(newOrderDisposition);
    }

    public String getReceipientAddress() {
        return recipientAddress;
    }

    public void setReceipientAddress(String newReceipientAddress) {
        recipientAddress = trim(newReceipientAddress);
    }
    
    public String getReceipientCity() {
        return recipientCity;
    }

    public void setReceipientCity(String newReceipientCity) {
        recipientCity = trim(newReceipientCity);
    }
    
    public String getReceipientPhone() {
        return recipientPhone;
    }

    public void setReceipientPhone(String newReceipientPhone) {
        recipientPhone = trim(newReceipientPhone);
    }

    public String getReceipientState() {
        return recipientState;
    }

    public void setReceipientState(String newReceipientState) {
        recipientState = trim(newReceipientState);
    }

    public String getReceipientZip() {
        return recipientZip;
    }

    public void setReceipientZip(String newReceipientZip) {
        recipientZip = trim(newReceipientZip);
    }

    public String getReceipientCountry() {
        return recipientCountry;
    }

    public void setReceipientCountry(String newReceipientCountry) {
        recipientCountry = trim(newReceipientCountry);
    }
    
    public String getProductSku() {
        return productSku;
    }
    
    public void setProductSku(String newProductSku) {
        productSku = trim(newProductSku);
    }
    
    public String getCardSignature() {
        return cardSignature;
    }


    public void setCardSignature(String newCardSignature) {
        cardSignature = trim(newCardSignature);
    }
    
    public String getCardMessage() {
        return cardMessage;
    }
    
    public void setCardMessage(String newCardMessage) {
        cardMessage = trim(newCardMessage);
    }

    public String getInstitutionName() {
        return institutionName;
    }
    
    public void setInstitutionName(String newInstitutionName) {
        institutionName = trim(newInstitutionName);
    }
    
    public String getOrderIndicator() {
        return orderIndicator;
    }
    
    public void setOrderIndicator(String newOrderIndicator) {
        orderIndicator = trim(newOrderIndicator);
    }    
    
    public String getCustomerID() {
        return customerID;
    }
    
    public void setCustomerID(String newCustomerID) {
        customerID = trim(newCustomerID);
    }       
    
    public String getVendorFlag() {
        return vendorFlag;
    }
    
    public void setVendorFlag(String newVendorFlag) {
        vendorFlag = trim(newVendorFlag);
    }    

    public String getMercuryFlag() {
        return mercuryFlag;
    }
    
    public void setMercuryFlag(String newMercuryFlag) {
        mercuryFlag = trim(newMercuryFlag);
    }  

    private String trim(String str)
    {
        return (str != null)?str.trim():str;
    }

    public Date getShipDate() {
        return shipDate;
    }
    
    public void setShipDate(Date newShipDate) {
        shipDate = newShipDate;
    }             
    
    public String getRecipientID() {
        return recipientID;
    }
    
    public void setRecipientID(String newRecipientID) {
        recipientID = trim(newRecipientID);
    }   
    
    public String getSubstitutionIndicator() {
        return substitutionIndicator;
    }
    
    public void setSubstitutionIndicator(String newSubstitutionIndicator) {
        substitutionIndicator = trim(newSubstitutionIndicator);
    }   

    public String getOccasion() {
        return occasion;
    }
    
    public void setOccasion(String newOccasion) {
        occasion = trim(newOccasion);
    }   

    public String getReleaseInfoIndicator() {
        return releaseInfoIndicator;
    }
    
    public void setReleaseInfoIndicator(String newReleaseInfoIndicator) {
        releaseInfoIndicator = trim(newReleaseInfoIndicator);
    }   

    public String getSpecialInstructions() {
        return specialInstructions;
    }
    
    public void setSpecialInstructions(String newSpecialInstructions) {
        specialInstructions = trim(newSpecialInstructions);
    }   

    public String getQuantity() {
        return quantity;
    }
    
    public void setQuantity(String newQuantity) {
        quantity = trim(newQuantity);
    } 

    public String getColor1() {
        return color1;
    }
    
    public void setColor1(String newColor1) {
        color1 = trim(newColor1);
    } 

    public String getColor2() {
        return color2;
    }
    
    public void setColor2(String newColor2) {
        color2 = trim(newColor2);
    } 

    public String getSameDayGift() {
        return sameDayGift;
    }
    
    public void setSameDayGift(String newSameDayGift) {
        sameDayGift = trim(newSameDayGift);
    }   

    public String getShipMethod() {
        return shipMethod;
    }
    
    public void setShipMethod(String newShipMethod) {
        shipMethod = trim(newShipMethod);
    }



    /**
     * Determine whether or not the order is FTDM.
     * 
     * @param n/a
     * @return boolean
     * @throws Exception
     */
    public boolean isOrderFTDM() throws Exception {
        
         return this.orderFTDM;
                   
    }
    
    
    
     /**
     * Determine whether or not the order is cancelled or rejected.
     * 
     * @param n/a
     * @return boolean
     * @throws Exception
     */
    public boolean isOrderCancelledOrRejected() throws Exception {
        return ((this.cancelSentFlag!=null&&this.cancelSentFlag.trim()
                .equalsIgnoreCase("Y"))||(this.rejectedFlag!=null&&this.rejectedFlag.trim().equalsIgnoreCase("Y")));
       

    }
    
    
    public boolean hasLiveFTD() throws Exception {
        if(this.liveFTDFlag!=null)
        {
          
          return this.liveFTDFlag.trim().equalsIgnoreCase("Y"); 
           
        }else
        {
           return false;
        }

    }
    
    
    
    
     /**
     * Determine whether or not the order is in scrub.
     * 
     * @param n/a
     * @return boolean
     * @throws Exception
     */
    public boolean isOrderInScrub() throws Exception {
        String status=this.orderDisposition;
        if(status!=null){
          return (status.trim().equalsIgnoreCase("In-Scrub")||status.equalsIgnoreCase("Pending")||status.equalsIgnoreCase("Removed"));
        }else
        {
          return false;
        }

    }



    /**
     * Determine whether or not the order is on hold.
     * 
     * @param n/a
     * @return boolean
     * @throws Exception
     */
    public boolean isOrderOnHold() throws Exception {
        
        String status=this.orderDisposition;
        if(status!=null){
          return (status.trim().equalsIgnoreCase(EmailConstants.ORDER_ON_HOLD));
        }else
        {
          return false;
        }
     
    }
    
    
    
    

    /**
     * Determine whether or not the order is a drop-ship.
     * 
     * @param n/a
     * @return boolean
     * @throws Exception
     */
    public boolean isOrderDropship() throws Exception {
        if (this.vendorFlag != null) {
            return (this.vendorFlag.trim()
                    .equals("Y"));
        } else {
            return false;
        }

    }
    
    
    
    
    /**
     * Determine whether or not the order is floral.
     * 
     * @param n/a
     * @return boolean
     * @throws Exception
     */
    public boolean isOrderFloral() throws Exception {

        if (this.vendorFlag != null) {
            return (this.vendorFlag.trim().equals("N")); 
        } else {
            return false;
        }

    }
    
     
    

    

    /**
     * Determine whether or not the drop-ship order is printed.
     * 
     * @param n/a
     * @return boolean
     * @throws Exception
     */
    public boolean isOrderPrintedOrShipped() throws Exception {
         String status=this.orderDisposition;
         return (status.equalsIgnoreCase(EmailConstants.ORDER_PRINTED)||status.equalsIgnoreCase("Shipped"));
    }
    
    
    
    /**
     * Determine whether or not a tracking number is available.
     * 
     * @param n/a
     * @return boolean
     * @throws Exception
     */
    public boolean isTrackingAvailable() throws Exception {

       return (this.trackingNumber!=null);
    }

    /**
     * Determine whether or not a florist is assigned to an order
     * 
     * @param n/a
     * @return boolean
     * @throws Exception
     */
    public boolean isFloristAssigned() throws Exception {

        return (this.fillingFloristId!=null);
    }



    /**
     * Determine whether or not the email request has passed order�s delivery
     * date.
     * 
     * @param n/a
     * @return boolean
     * @throws Exception
     */
    public boolean isPastDeliveryDate() throws Exception {
        Calendar cal = Calendar.getInstance(TimeZone.getDefault());

        if( this.deliveryDate!=null){
          return (cal.getTime().after(this.deliveryDate));
        }else
        {
          return false;
        }

    }


  public void setCancelSentFlag(String cancelSentFlag)
  {
    this.cancelSentFlag = cancelSentFlag;
  }


  public String getCancelSentFlag()
  {
    return cancelSentFlag;
  }


  public void setLiveFTDFlag(String liveFTDFlag)
  {
    this.liveFTDFlag = liveFTDFlag;
  }


  public String getLiveFTDFlag()
  {
    return liveFTDFlag;
  }


  public void setAttemptedFTDFlag(String attemptedFTDFlag)
  {
    this.attemptedFTDFlag = attemptedFTDFlag;
  }


  public String getAttemptedFTDFlag()
  {
    return attemptedFTDFlag;
  }


  public void setAttemptedCANFlag(String attemptedCANFlag)
  {
    this.attemptedCANFlag = attemptedCANFlag;
  }


  public String getAttemptedCANFlag()
  {
    return attemptedCANFlag;
  }
  
  
  public String toString()
  {
    
    return this.getClass().getName()+"\t external order number: "+this.externalOrderNumber+"\t order detail id: "+this.orderDetailID;
    
  }


 


  public boolean isAmazonOrder()
  {
    return this.source!=null&&this.source.equalsIgnoreCase("az_order_number");
    
  }

   public boolean isWalmartOrder()
  {
    return this.source!=null&&this.source.equalsIgnoreCase("walmart_order_number");
    
  }


  public void setFtpVendor(boolean ftpVendor)
  {
    this.ftpVendor = ftpVendor;
  }


  public boolean isFtpVendor()
  {
    return ftpVendor;
  }



  public boolean hasAttemptedFTD()
  {
    
    return this.attemptedFTDFlag!=null&&this.attemptedFTDFlag.trim().equalsIgnoreCase("Y");
  }

  public boolean hasAttemptedCAN()
  {
    
    return this.attemptedCANFlag!=null&&this.attemptedCANFlag.trim().equalsIgnoreCase("Y");
  }

  public void setRejectedFlag(String rejectedFlag)
  {
    this.rejectedFlag = rejectedFlag;
  }


  public String getRejectedFlag()
  {
    return rejectedFlag;
  }


  public void setFtdStatus(String ftdStatus)
  {
    this.ftdStatus = ftdStatus;
  }


  public String getFtdStatus()
  {
    return ftdStatus;
  }


  

  public void setOrderFTDM(boolean orderFTDM)
  {
    this.orderFTDM = orderFTDM;
  }

    public void setPersonalized(boolean personalized) {
        this.personalized = personalized;
    }

    public boolean isPersonalized() {
        return personalized;
    }

    public String getLanguageId() {
		return languageId;
	}

	public void setLanguageId(String languageId) {
		this.languageId = languageId;
	}

}
