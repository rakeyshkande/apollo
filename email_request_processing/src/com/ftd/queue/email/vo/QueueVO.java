/**
* Value Object for the queue table.
* 
* Class Name:   QueueVO
* Revision:     1.0
* Date:         February 24, 2005
* Author:       Charles Fox, Software Architects, Inc.
*/

package com.ftd.queue.email.vo;

import java.util.Date;

/**
 * This value object class contains data stored in the Queue Database 
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 */
 
public class QueueVO 
{

    private String messageID = "";
    private String queueType = "";
    private String pointOfContactID = "";
    private Date timeStamp;
    private String system = "";
    private String mercuryNumber = "";
    private String email = "";
    private String messageType = "";
    private int priority;
    private String tagged = "";
    private String attached = "";
    private OrderDetailVO orderDetailVO;
    private InquiryDetailVO inquiryVO;
  
    public QueueVO()
    {
        super();
    }
    
    public String getMessageID() {
        return messageID;
    }

    public void setMessageID(String newMessageID) {
        messageID = trim(newMessageID);
    }


    public String getQueueType() {
        return queueType;
    }

    public void setQueueType(String newQueueType) {
        queueType = trim(newQueueType);
    }
    
    public String getPointOfContactID() {
        return pointOfContactID;
    }

    public void setPointOfContactID(String newPointOfContactID) {
        pointOfContactID = trim(newPointOfContactID);
    }
    public Date getTimeStamp() {
        return timeStamp;
    }
    
    public void setTimeStamp(Date newTimeStamp) {
        timeStamp = newTimeStamp;
    }
    
    public String getSystem() {
        return system;
    }
    
    public void setSystem(String newSystem) {
        system = trim(newSystem);
    }
    
    public String getMercuryNumber() {
        return mercuryNumber;
    }
    
    public void setMercuryNumber(String newMercuryNumber) {
        mercuryNumber = trim(newMercuryNumber);
    }
    
    public String getEmail() {
        return email;
    }
    
    public void setEmail(String newEmail) {
        email = trim(newEmail);
    }

    public String getMessageType() {
        return messageType;
    }
    
    public void setMessageType(String newMessageType) {
        messageType = trim(newMessageType);
    }
    
    public int getPriority() {
        return priority;
    }
    
    public void setPriority(int newPriority) {
        priority = newPriority;
    }
    
    public String getTagged() {
        return tagged;
    }

    public void setTagged(String newTagged) {
        tagged = trim(newTagged);
    }
    
    public String getAttached() {
        return attached;
    }

    public void setAttached(String newAttached) {
        attached = trim(newAttached);
    }

    public OrderDetailVO getOrderDetailVO() {
        return orderDetailVO;
    }
    
    public void setOrderDetailVO(OrderDetailVO newOrderDetailVO) {
        orderDetailVO = newOrderDetailVO;
    }
    
    public InquiryDetailVO getInquiryVO() {
        return inquiryVO;
    }
    
    public void setInquiryVO(InquiryDetailVO newInquiryVO) {
        inquiryVO = newInquiryVO;
    }
    
    private String trim(String str)
    {
        return (str != null)?str.trim():str;
    }

}