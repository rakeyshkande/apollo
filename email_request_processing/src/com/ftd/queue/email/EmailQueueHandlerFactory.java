package com.ftd.queue.email;
import java.io.IOException;
import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.queue.email.constants.EmailConstants;
import com.ftd.queue.email.vo.EmailRequestVO;

/**
 * This class is will find the class information and instantiate an e-mail
 * queue handler
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 */
public class EmailQueueHandlerFactory 
{
  /**
   * Singleton instance of the object.
   */
  private static EmailQueueHandlerFactory INSTANCE;
    
    private Logger logger = 
        new Logger("com.ftd.queue.email.EmailQueueHandlerFactory");
   
  private static HashMap cache = new HashMap();
  
    public EmailQueueHandlerFactory()
    {
    }
    
    /**
     * Return the EmailQueueHandlerFactory instance.  It ensures that at a 
     * given time there is only a single instance.
     * @param n/a
     * @return EmailQueueHandlerFactory
     * @throws Exception
     */ 
    public static EmailQueueHandlerFactory getInstance(){
		if (INSTANCE == null) {
			INSTANCE = new EmailQueueHandlerFactory();
		}
		return INSTANCE;    
    }
    
  /**
   * 
   * @param requestType The type of email request. e.g. MO, CA, DI...
   * @param requestSubType The sub type of email request, it is used to distinguish Suggestions from Questions.
   * @return 
   * @throws javax.xml.transform.TransformerException
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws java.lang.ClassNotFoundException
   * @throws java.lang.InstantiationException
   * @throws java.lang.IllegalAccessException
   * @throws java.lang.Exception
   */
    public EmailQueueHandler getEmailQueueHandler (EmailRequestVO emailRequestVO) 
        throws TransformerException,IOException,SAXException,
        ParserConfigurationException,ClassNotFoundException,
        InstantiationException,IllegalAccessException,Exception{
        String requestType=emailRequestVO.getRequestType();
        String requestSubType=emailRequestVO.getSubType();
        if(logger.isDebugEnabled()){
            logger.debug("Entering getEmailQueueHandler");
            logger.debug("requestType : " + requestType);
        }
        
        EmailQueueHandler queueHandler=null;
        
        try{
            String className = null;
            //right now, Novator sends us suggestions emails as QC (question) with the subtype CO (comments), 
            //we need to convert those email requests from QC to SE.
            if(requestType.equalsIgnoreCase("QC"))
            {
              if(requestSubType!=null&&requestSubType.trim().equalsIgnoreCase("CO"))
              {
                requestType="SE";
                //reset email request value object type.
                emailRequestVO.setRequestType(requestType);
                logger.debug("new requestType : " + emailRequestVO.getRequestType());
              }
                            
            }
            className = ConfigurationUtil.getInstance().getProperty(
                EmailConstants.EMAIL_QUEUE_HANDLER_FILE, requestType);
               
           if(className==null||className.equals("")){
                /* request type not found, use default handler */
                className =  ConfigurationUtil.getInstance().getProperty(
                EmailConstants.EMAIL_QUEUE_HANDLER_FILE, EmailConstants.QUEUE_HANDLER_DEFAULT);
            }
            
            queueHandler = loadEmailQueueHandler(className);
            if(queueHandler==null)
            {
                throw new Exception(EmailConstants.UNABLE_TO_CREATE_QUEUE_HANDLER);       
            }
        }finally
        {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getEmailQueueHandler");
            }
        }     
        return queueHandler;
    }

    private EmailQueueHandler loadEmailQueueHandler (String className)
    throws ClassNotFoundException,InstantiationException,IllegalAccessException{
    
        if(logger.isDebugEnabled()){
            logger.debug("Entering loadEmailQueueHandler");
            logger.debug("className : " + className);
        }
    
        EmailQueueHandler handler=null;
        try{
            handler = (EmailQueueHandler) Class.forName(
						className).newInstance();
      
        }finally
        {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting loadEmailQueueHandler");
            }
        }     
        return handler;
    }
}


