package com.ftd.queue.email.constants;

/**
 * This class contains all of the constants used in the 
 * classes under com.ftd.queue.email
 * @author Charles Fox
 */
 
public interface EmailConstants 
{
    public static final String EMAIL_ = "QueueConfigHandler";

    /* Email Request Constants   */
    public static final String REQUEST_PARAMETER_EMAIL = "EmailRequest";
    public static final int RESPONSE_SUCCESS = 200;
    public static final int RESPONSE_ERROR = 600;
    
    public static final String PROPERTY_FILE="queue_config.xml";
    public static final String SERVICE_LOCATOR_CONFIG_FILE = "service-locator-config.xml";
    public static final String ERP_CONFIG_CONTEXT = "EMAIL_REQUEST_PROCESSING_CONFIG";

    //database connection:  data source name
    public static final String DATASOURCE_NAME = "CLEANDS";
    
    public static final String RESPONSE_SUCCESS_MSG = "SUCCESS";
    public static final String RESPONSE_DUPLICATE_MSG =
        "DUPLICATE SEQUENCE NUMBER";
    public static final String RESPONSE_ERROR_MSG = "ERROR";
    
    public static final int LEVEL_DEBUG = 0;
    public static final int LEVEL_PRODUCTION = 1;
    public static final String SYS_MESS_TYPE_ERROR = "ERROR";
    public static final String SYS_MESS_SOURCE = "Email Queue Automated Process";
    public static final String SYS_MESS_SOURCE_MAILBOX = "MAILBOX MONITOR";
    /* JMS Constants */
    public static final String JMS_EMAIL_STATUS = "NOVATOREMAIL";
    
    /* Novator E-mail XML constants */
    public static final String EMAIL_VALIDATION_XSD = "email-request.xsd";
    public static final String EMAIL_REQUEST_NODES = "/email-request";
    public static final String EMAIL_REQUEST_ORDER_INQUIRY_NODES 
        = "/email-request/order-inquiry";
    public static final String EMAIL_REQUEST_ORDER_INQUIRY_SUB_TYPES 
        = "/email-request/order-inquiry/request-sub-type";
    public static final String EMAIL_REQUEST_IDENTIFIER = "identifier";
    public static final String EMAIL_REQUEST_COMPANY_ID = "company-id";
    public static final String EMAIL_REQUEST_SERVER_ID = "server-id";
    public static final String EMAIL_REQUEST_SEQUENCE_NUMBER 
        = "sequence-number";
    public static final String EMAIL_REQUEST_TYPE = "type";
    public static final String EMAIL_REQUEST_BILLING_FIRSTNAME 
        = "billing-firstName";
    public static final String EMAIL_REQUEST_BILLING_LASTNAME 
        = "billing-lastName";
    public static final String EMAIL_REQUEST_BILLING_EMAIL 
        = "billing-email";
    public static final String EMAIL_REQUEST_BILLING_DAYPHONE 
        = "billing-dayPhone";
    public static final String EMAIL_REQUEST_BILLING_EVEPHONE 
        = "billing-evePhone";
    public static final String EMAIL_REQUEST_ORDER_NUMBER = "order-number";
    public static final String EMAIL_REQUEST_SOURCE_CODE = "source-code";
    public static final String EMAIL_REQUEST_DELIVERY_DATE1= "delivery-date1";
    public static final String EMAIL_REQUEST_DELIVERY_DATE2= "delivery-date2";
    public static final String EMAIL_REQUEST_SUB_TYPE = "sub-type";
    public static final String EMAIL_REQUEST_COMMENT_TYPE = "comment-type";
    public static final String EMAIL_REQUEST_COMMENT = "comment";
    public static final String EMAIL_REQUEST_ORDER_DETAIL_ID = "order-detail-id";
    public static final String EMAIL_REQUEST_ORDER_GUID = "order-guid";
    public static final String EMAIL_REQUEST_ORDER_EXT_NUMBER = "order-external-number";
    public static final String EMAIL_REQUEST_ORDER_MASTER_NUMBER = "order-master-number";
    public static final String EMAIL_REQUEST_CUSTOMER_ID = "customer-id";
    public static final String EMAIL_REQUEST_ORDER_DISP_CODE = "order-disp-code";
    public static final String EMAIL_REQUEST_ORDER_INDICATOR = "order-indicator";
    
    public static final String EMAIL_REQUEST_ORDER_INQUIRY = "order-inquriy";
    public static final String INQ_DETAIL_RECEIPIENT_FIRST_NAME 
        = "recipient-firstName";
    public static final String INQ_DETAIL_RECEIPIENT_LAST_NAME 
        = "recipient-lastName";
    public static final String INQ_INSTITUTION = "institution";
    public static final String INQ_DETAIL_RECEIPIENT_STREET 
        = "recipient-street";
    public static final String INQ_DETAIL_RECEIPIENT_CITY = "recipient-city";
    public static final String INQ_DETAIL_RECEIPIENT_STATE = "recipient-state";
    public static final String INQ_DETAIL_RECEIPIENT_ZIP = "recipient-zip";
    public static final String INQ_DETAIL_RECEIPIENT_COUNTRY 
        = "recipient-country";
    public static final String INQ_DETAIL_RECEIPIENT_PHONE = "recipient-phone";
    public static final String INQ_DETAIL_DELIVERY_DATE_ONE 
        = "new-delivery-date1";
    public static final String INQ_DETAIL_DELIVERY_DATE_TWO 
        = "new-delivery-date2";
    public static final String INQ_DETAIL_PRODUCT = "product";
    public static final String INQ_DETAIL_CARD_MESSAGE = "card-message";
    public static final String INQ_DETAIL_CARD_SIGNATURE = "card-signature";
    public static final String INQ_DETAIL_OTHER_COMMENTS = "other-comments";
    public static final String INQ_DETAIL_SUB_TYPE_ATTR_TYPE = "type";
    public static final String DELIVERY_DATE_FORMAT =  "yyyy/MM/dd";
    
    //Message Constants
    public static final String UNABLE_CREATE_DB_CONNECTION 
        = "UnableToCreateDBConnection";
    public static final String UNABLE_ARCHIVE_EMAIL_XML = 
    "UnableToArchiveEmailXML";
    public static final String UNABLE_ARCHIVE_MSG = "UnableToArchiveMsg";
    public static final String UNABLE_STORE_QUEUE = "UnableToStoreQueue";
    public static final String UNABLE_RETRIEVE_QUEUE = "UnableToRetrieveQueue";
    public static final String UNABLE_RETRIEVE_ORDER = "UnableToRetrieveOrder";
    public static final String UNABLE_CHECK_LOCK_ORDER 
        = "UnableToCheckLockOrder";
    public static final String UNABLE_LOCK_ORDER = "UnableToLockOrder";
    public static final String UNABLE_RELEASE_ORDER = "UnableToReleaseOrder";
    public static final String UNABLE_CREATE_AUTO_EMAIL 
        = "UnableToCreateAutoEmail";
    public static final String UNABLE_DISPATCH_JMS = "UnableToDispatchJMS";
    public static final String UNABLE_PROCESS_REQUEST_XML 
        = "UnableToProcessRequestXML";
    public static final String UNABLE_SET_TIME_STAMP = "UnableToSetTimeStamp"; 
    public static final String UNABLE_PROCESS_SUB_TYPES 
        = "UnableToProcessSubTypes"; 
    public static final String UNABLE_PROCESS_MESSAGE
        = "UnableToMessage";
    public static final String UNABLE_CREATE_EMAIL_REQUEST_VO
        = "UnableToCreateEmailRequestVO";
    public static final String UNABLE_TO_PROCESS_EMAIL_QUEUE
        = "UnableToProcessEmailQueue";
    public static final String UNABLE_TO_CREATE_QUEUE_HANDLER
        = "UnableToCreateQueueHandler";
    public static final String UNABLE_TO_LOAD_QUEUE
        = "UnableToLoadQueue";
    public static final String UNABLE_SEND_AUTO_EMAIL
        = "UnableToSendAutoEmail";
    public static final String UNABLE_PROCESS_PRODUCT_DISCREPANCY_EMAIL
        = "UnableToProcessProductDiscrepancyEmail";
    public static final String UNABLE_PROCESS_QUALITY_ISSUE_EMAIL
        = "UnableToProcessQualityIssueEmail";
    public static final String UNABLE_PROCESS_BILLING_DISCREPANCY_EMAIL
        = "UnableToProcessBillingDiscrepancyEmail";        
    public static final String UNABLE_PROCESS_QUESTION_EMAIL
        = "UnableToProcessQuestionEmail";     
    public static final String UNABLE_PROCESS_ORDER_ACKNOWLEDGEMENT
        = "UnableToProcessOrderAcknowledgement";   
    public static final String UNABLE_PROCESS_OTHER_ORDER
        = "UnableToProcessOtherOrder";
    public static final String UNABLE_PROCESS_DEFAULT
        = "UnableToProcessDefault";
    public static final String UNABLE_PROCESS_CANCEL_ORDER
        = "UnableToProcessCancelOrder";
    public static final String UNABLE_PROCESS_DELIVERY_INQUIRY
        = "UnableToProcessDeliveryInquiry";
    public static final String UNABLE_PROCESS_NON_DELIVERY_INQUIRY
        = "UnableToProcessNonDeliveryInquiry";
    public static final String UNABLE_PROCESS_MODIFY_ORDER
        = "UnableToProcessModifyOrder";
    public static final String UNABLE_INSERT_ORDER_COMMENTS
        = "UnableToInsertOrderComments";            
    public static final String UNABLE_CHECK_QUEUE
        = "UnableToCheckQueue";     
    public static final String UNABLE_REMOVE_QUEUE
        = "UnableToRemoveQueue"; 
    public static final String UNABLE_TO_RETRIEVE_ORDER_COMMENTS
        = "UnableToRetrieveOrderComments";         
    public static final String UNABLE_TO_RETRIEVE_HOLIDAY_DATES
        = "UnableToRetrieveHolidayDates";     
    public static final String UNABLE_TO_RETRIEVE_HOLIDAY_START_DATE
        = "UnableToRetrieveHolidayStartDate";    
    public static final String UNABLE_TO_RETRIEVE_HOLIDAY_END_DATE
        = "UnableToRetrieveHolidayEndDate";         
    public static final String UNABLE_STORE_ASK_MESSAGE
        = "UnableToStoreASKMessage";
    public static final String UNABLE_SEND_ASK_MESSAGE
        = "UnableToSendASKMessage";
    public static final String UNABLE_RETRIEVE_48_HOUR_QUEUES
        = "UnableToRetrieve48HourQueueList";
    public static final String UNABLE_RETRIEVE_24_HOUR_LIST
        = "UnableToRetrieve24HourMessageList";        
     public static final String UNABLE_PROCESS_DELIVERY_ASK_EVENT
        = "UnableToProcessDeliveryASKEvent";         
     public static final String UNABLE_FIND_ORDER_DETAIL_ID
        = "UnableToFindOrderDetailID";          
     public static final String UNABLE_RETRIEVE_FTD_ORDER
        = "UnableToRetrieveFTDOrder";   
     public static final String UNABLE_RETRIEVE_AMAZON_ORDER
        = "UnableToRetrieveAmazonOrder";
     public static final String UNABLE_RETRIEVE_WALMART_ORDER
        = "UnableToRetrieveWalmartOrder";
     public static final String UNABLE_LOAD_QUEUE_HANDLER
        = "UnableToLoadQueueHandler";
    public static final String UNABLE_LOAD_QUEUE_CONFIG_FILE
        = "UnableToLoadQueueConfigFile";
    public static final String UNABLE_RETRIEVE_ORDER_DETAIL
        = "UnableToRetrieveOrderDetail";  
    public static final String UNABLE_RELEASE_SEQUENCE_CHECK_LOCK
        = "UnableToReleaseSequenceCheckLock"; 
    public static final String UNABLE_LOCK_SEQUENCE_CHECK
        = "UnableToLockSequenceCheck";
    public static final String UNABLE_TO_CHECK_SEQUENCE_CHECK_LOCK
        = "UnableToCheckForSequenceCheckLock"; 
    public static final String UNABLE_TO_DELETE_MISSING_SEQUENCE
        = "UnableToDeleteMissingSequence"; 
    public static final String UNABLE_TO_STORE_MISSING_SEQUENCE
        = "UnableToStoreMissingSequence"; 
    public static final String UNABLE_TO_RETRIEVE_LAST_CHK_SEQUENCE
        = "UnableToRetrieveLastCheckedSequence"; 
    public static final String UNABLE_TO_RETRIEVE_ALL_CHK_SEQUENCES
        = "UnableToRetrieveAllCheckedSequences"; 
    public static final String UNABLE_TO_EXECUTE_SEQUENCE_CHECK_PROCESS
        = "UnableToExecuteSequenceCheckProcess"; 
        
    /* Stored Procedure Constants */
    /* Archive E-mail XML stored procedure and parameters */
    public static final String ARCHIVE_ORG_DATA = "INSERT_EMAIL_XML_ARCHIVE";
    public static final String ARCHIVE_ORG_DATA_IN_XML_SEQ
        = "IN_EMAIL_XML_SEQ";
    public static final String ARCHIVE_ORG_DATA_IN_XML_MESSAGE 
        = "IN_EMAIL_XML";
    public static final String ARCHIVE_ORG_DATA_OUT_STATUS_PARAM = "OUT_STATUS";
    public static final String ARCHIVE_ORG_DATA_OUT_MESSAGE_PARAM 
        = "OUT_MESSAGE";
    public static final String ARCHIVE_ORG_DATA_DUPLICATE_MSG = 
        "Duplicate Email Sequence Found";
  public static final String EMAIL_MESSAGE_TYPE           = "Email";
    /* QUEUE_ADD_EMAIL_REQUEST stored procedure and paramters */
    public static final String QUEUE_ADD_EMAIL_REQUEST = 
    "INSERT_POINT_OF_CONTACT2";
    public static final String EMAIL_REQUEST_IN_CUSTOMER_ID 
            = "IN_CUSTOMER_ID";
    public static final String EMAIL_REQUEST_IN_ORDER_GUID
            = "IN_ORDER_GUID";
    public static final String EMAIL_REQUEST_IN_ORDER_DETAIL_ID
            = "IN_ORDER_DETAIL_ID";
    public static final String EMAIL_REQUEST_IN_MASTER_ORDER_NUMBER
            = "IN_MASTER_ORDER_NUMBER";
    public static final String EMAIL_REQUEST_IN_ORDER_NUMBER 
        = "IN_EXTERNAL_ORDER_NUMBER";
    public static final String EMAIL_REQUEST_IN_DELIVERY_DATE_ONE 
        = "IN_DELIVERY_DATE";
    public static final String EMAIL_REQUEST_IN_SOURCE_CODE 
        = "IN_SOURCE_CODE";
    public static final String EMAIL_REQUEST_IN_BILLING_FIRST_NAME 
        = "IN_FIRST_NAME";
    public static final String EMAIL_REQUEST_IN_BILLING_LAST_NAME 
        = "IN_LAST_NAME";
    public static final String EMAIL_REQUEST_IN_DAY_PHONE = "IN_DAYTIME_PHONE_NUMBER";
    public static final String EMAIL_REQUEST_IN_EVE_PHONE = "IN_EVENING_PHONE_NUMBER";
    public static final String EMAIL_REQUEST_IN_SENDER_EMAIL_ADDRESS = "IN_SENDER_EMAIL_ADDRESS";
    public static final String EMAIL_REQUEST_IN_LETTER_TITLE = "IN_LETTER_TITLE";
    public static final String EMAIL_REQUEST_IN_EMAIL_SUBJECT = "IN_EMAIL_SUBJECT";
    public static final String EMAIL_REQUEST_IN_BODY = "IN_BODY";
    public static final String EMAIL_REQUEST_IN_COMMENT_TYPE 
        = "IN_COMMENT_TYPE";
    public static final String EMAIL_REQUEST_IN_RECIPIENT_EMAIL_ADDRESS = "IN_RECIPIENT_EMAIL_ADDRESS";
    public static final String EMAIL_REQUEST_IN_RECIPIENT_FIRST_NAME = 
    "IN_NEW_RECIP_FIRST_NAME";
    public static final String EMAIL_REQUEST_IN_RECIPIENT_LAST_NAME = 
    "IN_NEW_RECIP_LAST_NAME";
    public static final String EMAIL_REQUEST_IN_RECIPIENT_ADDRESS 
        = "IN_NEW_ADDRESS";
    public static final String EMAIL_REQUEST_IN_RECIPIENT_CITY 
        = "IN_NEW_CITY";
    public static final String EMAIL_REQUEST_IN_RECIPIENT_PHONE 
        = "IN_NEW_PHONE";
    public static final String EMAIL_REQUEST_IN_RECIPIENT_ZIP 
        = "IN_NEW_ZIP";
    public static final String EMAIL_REQUEST_IN_RECIPIENT_STATE 
        = "IN_NEW_STATE";
    public static final String EMAIL_REQUEST_IN_RECIPIENT_COUNTRY 
        = "IN_NEW_COUNTRY";
    public static final String EMAIL_REQUEST_IN_INSTITUTION = "IN_NEW_INSTITUTION";
    public static final String EMAIL_REQUEST_IN_DELIVERY_DATE_TWO 
        = "IN_NEW_DELIVERY_DATE";
    public static final String EMAIL_REQUEST_IN_CARD_MESSAGE 
        = "IN_NEW_CARD_MESSAGE";
    public static final String EMAIL_REQUEST_IN_CARD_SIGNATURE 
        = "IN_NEW_CARD_SIGNATURE";
    public static final String EMAIL_REQUEST_IN_PRODUCT_SKU = "IN_NEW_PRODUCT";
    public static final String EMAIL_REQUEST_IN_CHANGE_CODES = "IN_CHANGE_CODES";
    public static final String EMAIL_REQUEST_IN_SENT_RECEIVED_INDICATOR = "IN_SENT_RECEIVED_INDICATOR";
    public static final String EMAIL_REQUEST_IN_SEND_FLAG = "IN_SEND_FLAG";
    public static final String EMAIL_REQUEST_IN_PRINT_FLAG = "IN_PRINT_FLAG";
    public static final String EMAIL_REQUEST_IN_POINT_OF_CONTACT_TYPE = "IN_POINT_OF_CONTACT_TYPE";
    public static final String EMAIL_REQUEST_IN_COMMENT_TEXT = "IN_COMMENT_TEXT";
    public static final String EMAIL_REQUEST_OUT_SEQUENCE_NUMBER = "OUT_SEQUENCE_NUMBER";
    public static final String EMAIL_REQUEST_OUT_STATUS_PARAM = "OUT_STATUS";
    public static final String EMAIL_REQUEST_OUT_MESSAGE_PARAM = "OUT_MESSAGE";      
    
    public static final String POINT_OF_CONTACT_TYPE_EMAIL = "Email";
    
    /* parameters for stored prodcedure QUEUE_ADD_EMAIL_QUEUE */
    public static final String QUEUE_ADD_EMAIL_QUEUE = "INSERT_QUEUE_RECORD";
    public static final String ADD_EMAIL_QUEUE_IN_MESSAGE_ID = "IN_MESSAGE_ID";
    public static final String ADD_EMAIL_QUEUE_IN_QUEUE_TYPE = "IN_QUEUE_TYPE";
    public static final String ADD_EMAIL_QUEUE_IN_MESSAGE_TYPE 
        = "IN_MESSAGE_TYPE";
    public static final String ADD_EMAIL_QUEUE_IN_TIME_STAMP 
        = "IN_MESSAGE_TIMESTAMP";
    public static final String ADD_EMAIL_QUEUE_IN_SYSTEM = "IN_SYSTEM";
    public static final String ADD_EMAIL_QUEUE_IN_MERCURY_NUMBER 
        = "IN_MERCURY_NUMBER";
    public static final String ADD_EMAIL_QUEUE_IN_MASTER_ORDER_NUMBER 
        = "IN_MASTER_ORDER_NUMBER";    
    public static final String ADD_EMAIL_QUEUE_IN_ORDER_GUID 
        = "IN_ORDER_GUID";    
    public static final String ADD_EMAIL_QUEUE_IN_ORDER_DETAIL_ID 
        = "IN_ORDER_DETAIL_ID";
    public static final String ADD_EMAIL_QUEUE_IN_POINT_OF_CONTACT_ID
        = "IN_POINT_OF_CONTACT_ID"; 
    public static final String ADD_EMAIL_QUEUE_IN_DELIVERY_DATE
        = "IN_DELIVERY_DATE";
    public static final String ADD_EMAIL_QUEUE_IN_EMAIL = "IN_EMAIL_ADDRESS";
    public static final String ADD_EMAIL_QUEUE_IN_RECEIPIENT_LOCATION
        = "IN_RECEIPIENT_LOCATION";
    public static final String ADD_EMAIL_QUEUE_IN_EXTERNAL_ORDER_NUMBER 
        = "IN_EXTERNAL_ORDER_NUMBER";
    public static final String ADD_EMAIL_QUEUE_OUT_STATUS_PARAM = "OUT_STATUS";
    public static final String ADD_EMAIL_QUEUE_OUT_MESSAGE_PARAM = "OUT_ERROR_MESSAGE";
    public static final String POINT_OF_CONTACT_COMMENT_TYPE = "Email";

    public static final String QUEUE_OUT_STATUS_PARAM = "OUT_STATUS";
    public static final String QUEUE_OUT_MESSAGE_PARAM = "OUT_MESSAGE"; 

    /* QUEUE_GET_QUEUE stored procedure and parameters */
    public static final String QUEUE_GET_QUEUE = "GET_QUEUE_RECORD_BY_ORDER";
    /* parameters for stored prodcedure QUEUE_GET_QUEUE */
    public static final String GET_QUEUE_IN_ORDER_NUMBER = "IN_ORDER_NUMBER";
    public static final String GET_QUEUE_IN_REQUEST_TYPE = "IN_REQUEST_TYPE";
    
   
    
    /* Lock Order stored procedure and parameters */
    public static final String LOCK_ORDER = "UPDATE_CSR_LOCKED_ENTITIES";
    public static final String LOCK_ORDER_IN_ENTITY_TYPE = "IN_ENTITY_TYPE";
    public static final String LOCK_ORDER_IN_ENTITY_ID = "IN_ENTITY_ID";
    public static final String LOCK_ORDER_IN_SESSION_ID = "IN_SESSION_ID";
    public static final String LOCK_ORDER_IN_CSR_ID = "IN_CSR_ID";
    public static final String LOCK_ORDER_STATUS_PARAM = "OUT_STATUS";
    public static final String LOCK_ORDER_MESSAGE_PARAM = "OUT_MESSAGE";    
    
    /* Release Order stored procedure and parameters */
    public static final String RELEASE_ORDER = "DELETE_CSR_LOCKED_ENTITIES";
    public static final String RELEASE_ORDER_IN_ENTITY_TYPE = "IN_ENTITY_TYPE";
    public static final String RELEASE_ORDER_IN_ENTITY_ID = "IN_ENTITY_ID";
    public static final String RELEASE_ORDER_IN_SESSION_ID = "IN_SESSION_ID";
    public static final String RELEASE_ORDER_IN_CSR_ID = "IN_CSR_ID";
    public static final String RELEASE_ORDER_STATUS_PARAM = "OUT_STATUS";
    public static final String RELEASE_ORDER_MESSAGE_PARAM = "OUT_MESSAGE";  

    public static final String ORDER_DETAIL_ENTITY_TYPE = "ORDER_DETAILS";
    public static final String SEQUENCE_CHECK_ENTITY_TYPE = "SEQUENCE_CHECK";
    public static final String SEQUENCE_CHECK_ENTITY_ID = "SEQUENCE_CHECK";
    
    /* order queue entries stored procedure and parameters */
    public static final String QUEUE_GET_ALL_ORDER_QUEUE = 
    "QUEUE_GET_ALL_ORDER_QUEUE";
    public static final String GET_ALL_ORDER_IN_ORDER_NUMBER 
    = "IN_ORDER_NUMBER";
    public static final String GET_ALL_ORDER_OUT_ALL_ORDER_QUEUES 
    = "OUT_ALL_ORDER_QUEUES";
    public static final String GET_ALL_ORDER_OUT_STATUS 
    = "OUT_STATUS";
    public static final String GET_ALL_ORDER_OUT_OUT_MESSAGE 
    = "OUT_MESSAGE";
    
    /* FIND_ORDER_NUMBER stored procedure and parameters */
    public static final String PROC_FIND_ORDER_NUMBER = "FIND_ORDER_NUMBER";
    public static final String PROC_FON_IN_ORDER_NUMBER = "IN_ORDER_NUMBER";
    public static final String PROC_FON_IN_EMAIL_ADDRESS = "IN_EMAIL_ADDRESS";
    public static final String PROC_FON_IN_LAST_NAME = "IN_LAST_NAME";
    public static final String PROC_FON_IN_FIRST_NAME = "IN_FIRST_NAME";
    public static final String PROC_FON_OUT_ORDER_DETAIL_ID
        = "OUT_ORDER_DETAIL_ID";
    public static final String PROC_FON_ORDER_OUT_ORDER_GUID 
        = "OUT_ORDER_GUID";
    public static final String PROC_FON_ORDER_OUT_EXTERNAL_ORDER_NUMBER 
        = "OUT_EXTERNAL_ORDER_NUMBER";
    public static final String PROC_FON_ORDER_OUT_MASTER_ORDER_NUMBER
        = "OUT_MASTER_ORDER_NUMBER";
    public static final String PROC_FON_ORDER_OUT_CUSTOMER_ID
        = "OUT_CUSTOMER_ID";
    public static final String PROC_FON_ORDER_OUT_ORDER_DISP_CODE
        = "OUT_ORDER_DISP_CODE";
    public static final String PROC_FON_ORDER_OUT_ORDER_INDICATOR
        = "OUT_ORDER_INDICATOR";

  
  /* Retrieve Order Detail stored procedure and parameters */
    public static final String QUEUE_RETRIEVE_ORDER_DETAIL 
        = "GET_ORDER_DETAILS";
    public static final String ORDER_DETAIL_IN_ORDER_NUMBER = "IN_ORDER_DETAIL_ID";
    public static final String ORDER_DETAIL_OUT_RESULTS = "OUT_CURSOR";
   
   
    public static final String ORDER_DETAIL_IN_EMAIL = "IN_EMAIL";
    public static final String ORDER_DETAIL_OUT_COMPANY_ID_EMAIL 
        = "OUT_COMPANY_ID";
    public static final String ORDER_DETAIL_OUT_DELIVERY_DATE 
        = "OUT_DELIVERY_DATE";
    public static final String ORDER_DETAIL_OUT_EXTERNAL_ORDER_NUMBER 
        = "OUT_EXTERNAL_ORDER_NUMBER";
    public static final String ORDER_DETAIL_OUT_MASTER_ORDER_NUMBER 
        = "OUT_MASTER_ORDER_NUMBER";
    public static final String ORDER_DETAIL_OUT_ORDER_DATE = "OUT_ORDER_DATE";
    public static final String ORDER_DETAIL_OUT_ORDER_DETAIL_ID 
        = "OUT_ORDER_DETAIL_ID";
    public static final String ORDER_DETAIL_OUT_ORDER_GUID = "OUT_ORDER_GUID";
    public static final String ORDER_DETAIL_OUT_ORDER_ON_HOLD 
        = "OUT_ORDER_ON_HOLD";
    public static final String ORDER_DETAIL_OUT_ORDER_PRINTED 
        = "OUT_ORDER_PRINTED";
    public static final String ORDER_DETAIL_OUT_ORDER_SHIPPED 
        = "OUT_ORDER_SHIPPED";
    public static final String ORDER_DETAIL_OUT_CANCELLED 
        = "OUT_ORDER_CANCELLED";
    public static final String ORDER_DETAIL_OUT_ORDER_TYPE = "OUT_ORDER_TYPE";
    public static final String ORDER_DETAIL_OUT_RECIPIENT_LOCATION_TYPE 
        = "OUT_RECIPIENT_LOCATION_TYPE";
    public static final String ORDER_DETAIL_OUT_SOURCE = "OUT_SOURCE";
    public static final String ORDER_DETAIL_OUT_TIME_ZONE = "OUT_TIME_ZONE";
    public static final String ORDER_DETAIL_OUT_TRACKING_NUMBER 
        = "OUT_TRACKING_NUMBER";
    public static final String ORDER_DETAIL_OUT_FLORIST_ID
        = "OUT_FLORIST_ID";
    public static final String ORDER_DETAIL_OUT_CARD_SIGNATURE
        = "OUT_CARD_SIGNATURE";
    public static final String ORDER_DETAIL_OUT_CARD_MESSAGE
        = "OUT_CARD_MESSAGE";
    public static final String ORDER_DETAIL_OUT_INSTITUTION_NAME
        = "OUT_INSTITUTION_NAME";
    public static final String ORDER_DETAIL_OUT_RECEIPIENT_FIRST_NAME
        = "OUT_RECEIPIENT_FIRST_NAME";
    public static final String ORDER_DETAIL_OUT_RECEIPIENT_LAST_NAME
        = "OUT_RECEIPIENT_LAST_NAME";
    public static final String ORDER_DETAIL_OUT_RECEIPIENT_ADDRESS
        = "OUT_RECEIPIENT_ADDRESS";
    public static final String ORDER_DETAIL_OUT_RECEIPIENT_CITY
        = "OUT_RECEIPIENT_CITY";
    public static final String ORDER_DETAIL_OUT_RECEIPIENT_STATE
        = "OUT_RECEIPIENT_STATE";
    public static final String ORDER_DETAIL_OUT_RECEIPIENT_PHONE
        = "OUT_RECEIPIENT_PHONE";
    public static final String ORDER_DETAIL_OUT_PRODUCT_SKU
        = "OUT_PRODUCT_SKU";
    public static final String ORDER_DETAIL_OUT_ORDER_DISPOSITION
        = "OUT_DISPOSITION";   
        



    public static final String ORDER_DETAIL_OUT_STATUS_PARAM = "OUT_STATUS";
    public static final String ORDER_DETAIL_OUT_MESSAGE_PARAM = "OUT_MESSAGE"; 
    

    /*  Insert Comments Stored Procedure and parameters */
    public static final String INSERT_COMMENTS_PRODCEDURE 
        = "INSERT_COMMENTS";
    public static final String INSERT_COMMENTS_IN_CUSTOMER_ID 
    = "IN_CUSTOMER_ID";
    public static final String INSERT_COMMENTS_IN_ORDER_GUID = "IN_ORDER_GUID";
    public static final String INSERT_COMMENTS_IN_ORDER_DETAIL_ID 
    = "IN_ORDER_DETAIL_ID";
    public static final String INSERT_COMMENTS_IN_COMMENT_ORIGIN 
    = "IN_COMMENT_ORIGIN";
    public static final String INSERT_COMMENTS_IN_REASON= "IN_REASON";
    public static final String INSERT_COMMENTS_IN_DNIS_ID = "IN_DNIS_ID";
    public static final String INSERT_COMMENTS_IN_COMMENT_TEXT 
    = "IN_COMMENT_TEXT";
    public static final String INSERT_COMMENTS_IN_COMMENT_TYPE 
    = "IN_COMMENT_TYPE";
    public static final String INSERT_COMMENTS_IN_CSR_ID = "IN_CSR_ID";    
    public static final String INSERT_COMMENTS_OUT_STATUS_PARAM = "OUT_STATUS";
    public static final String INSERT_COMMENTS_OUT_MESSAGE_PARAM 
    = "OUT_MESSAGE";

    /* remove queue entries stored procedure and parameters */
    public static final String QUEUE_REMOVE_QUEUE = 
    "DELETE_QUEUE_RECORD";

    public static final String REMOVE_QUEUE_IN_MESSAGE_ID
    = "IN_MESSAGE_ID";
    public static final String REMOVE_QUEUE_OUT_STATUS 
    = "OUT_STATUS";
    public static final String REMOVE_QUEUE_OUT_OUT_MESSAGE 
    = "OUT_ERROR_MESSAGE";

    /* retrieve orders with no response in 24 or 48 hours 
     * stored procedure and parameters */
    public static final String GET_ASK_NO_RESPONSE = 
    "GET_ASK_NO_RESPONSE";
    public static final String GET_ASK_NO_RESPONSE_IN_HOURS = 
    "IN_HOURS";

    /* log ask event handler message 
     * stored procedure and parameters */
    public static final String LOG_ASK_EVENT_HANDLER_MESSAGE = 
    "INSERT_ASK_MESSAGE_EVENT_LOG";
    public static final String LOG_ASK_EVENT_HANDLER_IN_ORDER_DETAIL_ID 
    = "IN_ORDER_DETAIL_ID";
    public static final String LOG_ASK_EVENT_HANDLER_IN_FILLING_FLORIST_ID
    = "IN_FILLING_FLORIST_ID";
    public static final String LOG_ASK_EVENT_HANDLER_IN_MESSAGE_TIMESTAMP
    = "IN_MESSAGE_TIMESTAMP";
    public static final String LOG_ASK_EVENT_HANDLER_IN_MESSAGE_TYPE
    = "IN_SYSTEM_TYPE";
    public static final String LOG_ASK_EVENT_HANDLER_IN_RESPONSE_FLAG
    = "IN_RESPONSE_FLAG";
    public static final String LOG_ASK_EVENT_HANDLER_OUT_STATUS 
    = "OUT_STATUS";
    public static final String LOG_ASK_EVENT_HANDLER_OUT_MESSAGE 
    = "OUT_MESSAGE";
    public static final String LOG_ASK_EVENT_RESPONSE_FLAG = "N";
    /* get highest priority queue
     * stored procedure and parameters */
    public static final String GET_ORDR_HIGHEST_PRIORITY_QUE = 
        "GET_ORDR_HIGHEST_PRIORITY_QUE";
    public static final String GET_ORDR_HIGHEST_PRIORITY_QUE_IN_ORDER_DETAIL_ID 
        = "IN_ORDER_DETAIL_ID";
    public static final String GET_ORDR_HIGHEST_PRIORITY_QUE_IN_QUEUE_INDICATOR 
        = "IN_QUEUE_INDICATOR";
    public static final String EMAIL_QUEUE_INDICATOR = "Email";
    
    /* Order detail constants */
    public static final String ORDER_SOURCE_AMAZON = "az_order_number";
    public static final String ORDER_SOURCE_FTD = "FTD";
    public static final String ORDER_SOURCE_WALMART = "walmart_order_number";
    
    /* E-mail Request Receiver Message Driven Bean Constants */
    public static final String EMAIL_QUEUE_HANDLER_FILE = "email_queue_handler.xml";
    public static final String EMAIL_QUEUE_HANDLER_PRIORITY = "email_queue_handler_priority.xml";
    public static final String QUEUE_HANDLER_DEFAULT = "DEFAULT";
    public static final String HOLIDAY_DATE_FORMAT =  "MM/dd/yyyy";
    //public static final String HOLIDAY_START_DATE =  "MM/dd/yyyy";
    //public static final String HOLIDAY_END_DATE =  "MM/dd/yyyy";
                                             
    /* E-mail Queue Handler Constants     */
    public static final String ORDER_TYPE_FLORAL = "Floral";
    public static final String ORDER_TYPE_FTDM = "FTDM";
    public static final String ORDER_TYPE_DROP_SHIP = "Dropship";
    public static final String ORDER_IN_SCRUB = "In-Scrub";
    public static final String ORDER_ON_HOLD = "Held";
    public static final String ORDER_PRINTED = "Printed";
    public static final String ORDER_CANCELLED = "Y";
    public static final String ORDER_NOT_PRINTED = "N";
    public static final String ORDER_TAGGED = "Y";
    public static final String ORDER_UNTAGGED = "N";
    public static final String VENDOR_DROPSHIP = "Y";
    public static final String VENDOR_FLORAL = "N";
    public static final String MERCURY_FTDM = "N";
    
    public static final String ORDER_NOT_CANCELLED = "N";
    public static final String ORDER_TRACKING_AVAIL = "Y";
    public static final String ORDER_TRACKING_NOT_AVAIL = "N";
    public static final String ORDER_ATTACHED = "Y";
    public static final String ORDER_UNATTACHED = "N";
    
        
    public static final String SYSTEM_TYPE_VENUS = "Venus";
    public static final String SYSTEM_TYPE_MERCURY = "Merc";
    
    

    /* Order Detail Comments XML */
    public static final String ORDER_INFORMATION_CONFIG_FILE = "order_information.xml";
    public static final String ORDER_INFORMATION_CSR_ID = "csr-id";
    
    /* Order Comment Constants*/
    
    public static final String ORDER_COMMENTS_TYPE= "Order";
    public static final String EMAIL_QUEUE_CONFIG_HANDLER = "EmailQueueConfigHandler";  
   
    
    /* Lock Sequence Check stored procedure and parameters */
    public static final String SEQUENCE_LOCK = "SEQUENCE_LOCK";
    public static final String SEQUENCE_LOCK_ORDER_NUMBER = "IN_ORDER_NUMBER";
    public static final String SEQUENCE_LOCK_STATUS_PARAM = "OUT_STATUS";
    public static final String SEQUENCE_LOCK_MESSAGE_PARAM = "OUT_MESSAGE";   

    /* Release Sequence Check stored procedure and parameters */
    public static final String SEQUENCE_RELEASE_LOCK = "SEQUENCE_RELEASE_LOCK";
    public static final String SEQUENCE_RELEASE_ORDER_NUMBER = "IN_ORDER_NUMBER";
    public static final String SEQUENCE_RELEASE_LOCK_OUT_STATUS_PARAM = "OUT_STATUS";
    public static final String SEQUENCE_RELEASE_LOCK_OUT_MESSAGE_PARAM = "OUT_MESSAGE"; 
    
    /* Check for Sequence Check Lock stored procedure and parameters */
    public static final String SEQUENCE_CHECK_LOCK = "SEQUENCE_CHECK_LOCK";
    public static final String SEQUENCE_CHECK_LOCK_ORDER_NUMBER = "IN_ORDER_NUMBER";
    public static final String SEQUENCE_CHECK_LOCK_INDICATOR= "LOCK_INDICATOR";
    public static final String SEQUENCE_CHECK_LOCK_OUT_STATUS_PARAM = "OUT_STATUS";
    public static final String SEQUENCE_CHECK_LOCK_OUT_MESSAGE_PARAM = "OUT_MESSAGE"; 

    /* Delete Missing Sequence stored procedure and parameters */
    public static final String DELETE_MISSING_SEQUENCE 
        = "UPDATE_CHECKED_EMAIL_SEQ";
    public static final String DELETE_MISSING_SEQUENCE_IN_SEQ_NUMBER 
        = "IN_EMAIL_XML_SEQ";
    public static final String DELETE_MISSING_SEQUENCE_OUT_STATUS_PARAM 
        = "OUT_STATUS";
    public static final String DELETE_MISSING_SEQUENCE_OUT_MESSAGE_PARAM 
        = "OUT_MESSAGE";

    /* Store Missing Sequence stored procedure and parameters */
    public static final String STORE_MISSING_SEQUENCE 
        = "INSERT_EMAIL_XML_SEQ_ERRORS";
    public static final String STORE_MISSING_SEQUENCE_IN_SEQ_NUMBER 
        = "IN_ERROR_EMAIL_XML_SEQ";
    public static final String STORE_MISSING_SEQUENCE_IN_ERROR_EMAIL_XML 
        = "IN_ERROR_EMAIL_XML";
    public static final String STORE_MISSING_SEQUENCE_IN_ERROR_INDICATOR 
        = "IN_ERROR_INDICATOR";
    public static final String STORE_MISSING_SEQUENCE_IN_ERROR_DATE 
        = "IN_ERROR_DATE";
    public static final String STORE_MISSING_SEQUENCE_OUT_STATUS_PARAM 
        = "OUT_STATUS";
    public static final String STORE_MISSING_SEQUENCE_OUT_MESSAGE_PARAM 
        = "OUT_MESSAGE";
    public static final String ERROR_INDICATOR_DUPLICATE = "E";
    public static final String ERROR_INDICATOR_MISSING = "M";
    
    /* Retrieve Last Checked Sequence stored procedure and parameters */
    public static final String RETRIEVE_LAST_CHECKED_SEQUENCE 
        = "GET_MAX_EMAIL_SEQ";
    public static final String RETRIEVE_LAST_CHECKED_SEQUENCE_IN_PREFIX 
        = "IN_SEQ_PREFIX";
    public static final String RETRIEVE_LAST_CHECKED_SEQUENCE_OUT_SEQUENCE 
        = "MAX_EMAIL_SEQ";
    public static final String RETRIEVE_ALL_UNCHECKED_SEQUENCES 
        = "GET_ALL_UNCHECKED_EMAIL_SEQ";

  /* Retrieve Get Order by Guid stored procedure and parameters */
    public static final String QUEUE_GET_ORDER_BY_GUID 
        = "GET_ORDER_BY_GUID";
    public static final String ORDER_BY_GUID_IN_ORDER_GUID = "IN_ORDER_GUID";
    public static final String ORDER_BY_GUID_OUT_CURSOR = "OUT_CURSOR";

  /* Get queue type priority stored procedure and parameters */
    public static final String GET_QUEUE_TYPE_PRIORITY 
        = "GET_QUEUE_TYPE_PRIORITY";
    public static final String GET_QUEUE_TYPE_PRIORITY_IN_QUEUE_TYPE 
        = "IN_QUEUE_TYPE";
    public static final String GET_QUEUE_TYPE_PRIORITY_OUT_PRIORITY = "NUMBER";

    /*  Update Recipient Stored Procedure and parameters */
    public static final String UPDATE_RECIPIENT 
        = "UPDATE_RECIPIENT";
    public static final String UPDATE_RECIPIENT_IN_ORDER_DETAIL_ID
        = "IN_ORDER_DETAIL_ID";
    public static final String UPDATE_RECIPIENT_IN_FIRST_NAME
        = "IN_FIRST_NAME";
    public static final String UPDATE_RECIPIENT_IN_LAST_NAME
        = "IN_LAST_NAME";
    public static final String UPDATE_RECIPIENT_IN_BUSINESS_NAME
        = "IN_BUSINESS_NAME";
    public static final String UPDATE_RECIPIENT_IN_ADDRESS_1
        = "IN_ADDRESS_1";
    public static final String UPDATE_RECIPIENT_IN_ADDRESS_2
        = "IN_ADDRESS_2";
    public static final String UPDATE_RECIPIENT_IN_CITY
        = "IN_CITY";
    public static final String UPDATE_RECIPIENT_IN_STATE
        = "IN_STATE";
    public static final String UPDATE_RECIPIENT_IN_ZIP_CODE
        = "IN_ZIP_CODE";
    public static final String UPDATE_RECIPIENT_IN_COUNTY
        = "IN_COUNTY";
    public static final String UPDATE_RECIPIENT_IN_COUNTRY
        = "IN_COUNTRY";
    public static final String UPDATE_RECIPIENT_IN_ADDRESS_TYPE
        = "IN_ADDRESS_TYPE";
    public static final String UPDATE_RECIPIENT_IN_UPDATED_BY
        = "IN_UPDATED_BY";
    public static final String UPDATE_RECIPIENT_OUT_STATUS_PARAM = "OUT_STATUS";
    public static final String UPDATE_RECIPIENT_OUT_MESSAGE_PARAM 
    = "OUT_MESSAGE";

    /*  GET_ORDER_COMPANY_EMAIL Stored Procedure and parameters */
    public static final String GET_ORDER_COMPANY_EMAIL 
        = "GET_ORDER_COMPANY_EMAIL";
    public static final String GET_ORDER_COMPANY_EMAIL_IN_ORDER_DETAIL_ID
        = "IN_ORDER_DETAIL_ID";
    public static final String GET_ORDER_COMPANY_EMAIL_IN_COMPANY_ID
        = "IN_COMPANY_ID";

    /* Sequence Check constants */
    public static final String STARTING_SEQ_PREFIX = "Start";
    public static final String ENDING_SEQ_PREFIX = "End";
    public static final String CURRENT_SEQ_PREFIX = "Current";
    public static final String FIRST_SEQ_PREFIX = "First";
    public static final String SEQUENCE_SEPARATOR = "-";
    public static final String SEQ_CHECK_GLOBAL_PARAM_MISSING_SEQ 
        = "MAX_MISSING_SEQ";
    public static final String SEQ_CHECK_GLOBAL_PARAM_CONTEXT 
        ="NOVATOR_SEQUENCE_CHECKER";
    public static final String GET_MAXIMUM_MISSING_SEQUENCE_COUNT 
        ="GET_MAXIMUM_MISSING_SEQUENCE_COUNT";
    
    /* Get Email XML constants */
    public static final String GET_EMAIL_XML = "GET_EMAIL_XML";
    public static final String GET_EMAIL_XML_IN_SEQ_NUMBER = "IN_SEQ_NUMBER";
    public static final String GET_EMAIL_XML_OUT_CURSOR = "OUT_CURSOR";
    
    //config flie locations
    public static final String DAO_PROPERTY_FILE 
        = "order_locator_dao_config.xml";
    public static final String DAO_NODE = "//order-locator";
    public static final String DAO_TYPE = "type";
    
    /* Mercury Message Constants */
    public static final String MERCURY_MSG_TYPE_CAN = "CAN";
    public static final String MERCURY_MSG_TYPE_ASK = "ASK";
    
    public static final String MERCURY_MSG_COMMENTS_CAN 
        = "This order will be cancelled";
    public static final String MERCURY_MSG_COMMENTS_MO 
        = "This order is being modified";
    public static final String MERCURY_MSG_COMMENTS_DI 
        = "Please confirm delivery.  Thank you.";
        
    public static final String EMAIL_TYPE_MODIFY_ORDER = "MO";
    public static final String EMAIL_TYPE_DELIVERY_INQUIRY = "DI";
    
    /* Send e-mail constants */
    public static final String EMAIL_MSG_CX_AFTER = "cx.after";
    public static final String EMAIL_MSG_CX_AUTOCANCEL = "cx.autocancel";
    public static final String EMAIL_MSG_CX_AUTOCANCELREFUND = "cx.autocancelrefund";
    public static final String EMAIL_MSG_CX_CANCELLED = "cx.cancelled";
    public static final String EMAIL_MSG_CX_QUEUE = "cx.queue";
    public static final String EMAIL_MSG_DEFAULT_QUEUE = "default.queue";
    public static final String EMAIL_MSG_DI_AFTER = "di.after";
    
    
    public static final String EMAIL_MSG_DI_AFTER_HOLIDAY = "di.after.holiday";
    public static final String EMAIL_MSG_DI_BEFORE = "di.before";
    public static final String EMAIL_MSG_DI_INQUIRY = "di.inquiry";
    public static final String EMAIL_MSG_DI_QUEUE = "di.queue";
    public static final String EMAIL_MSG_DI_TRACK = "di.track";
    public static final String EMAIL_MSG_MO_AFTER = "mo.after";
    public static final String EMAIL_MSG_MO_CHANGED = "mo.changed";
    public static final String EMAIL_MSG_MO_QUEUE = "mo.queue";
    public static final String EMAIL_MSG_ND_QUEUE = "nd.queue";
    public static final String EMAIL_MSG_ND_QUEUE_HOLIDAY = "nd.queue.holiday";
    public static final String EMAIL_MSG_OA_FOUND = "oa.found";
    public static final String EMAIL_MSG_OA_QUEUE = "oa.queue";
    public static final String EMAIL_MSG_OP_QUEUE = "op.queue";
    public static final String EMAIL_MSG_QC_CO_QUEUE = "qc.co.queue";
    public static final String EMAIL_MSG_QC_QUEUE = "qc.queue";
    
    /* Sequence Check error messages */
    public static final String SEQUENCE_CHECK_MISSING_SEQ_LIST 
        = "The following sequence number(s) is/are missing: ";
    public static final String SEQUENCE_CHECK_MISSING_MAX_SEQ_LIST = 
        "Sequence Check found the number of sequence numbers not used exceeded the maximum to list individually: ";

    public static final String EMAIL_QUEUE_CONFIG_END_HOLIDAY = "END_HOLIDAY";
    public static final String EMAIL_QUEUE_CONFIG_START_HOLIDAY = "START_HOLIDAY";
    public static final String EMAIL_QUEUE_CONFIG_CONTEXT = "EMAIL_QUEUE_CONFIG";
    public static final String EMAIL_QUEUE_CONFIG_MAX_EMAIL_SIZE = "MAX_EMAIL_SIZE";

    public static final String EMAIL_SIZE_INVALID 
        = "Novator E-mail exceeds the maximum size allowed of ";

    /* User order cancel/refund constants */        
    public static final String USER_REFUND_DISP_CODE = "A10";
    public static final String USER_REFUND_USER_ID   = "CUSTOMER";
    public static final String USER_REFUND_STATUS    = "Unbilled";
    
    public static final String VALIDATE_EMAIL_XML = "VALIDATE_EMAIL_XML";
    
    /* Mailbox constants */
    public static final String MAILBOX_SETTINGS_NODE = "mailsettings";
    public static final String MAILBOX_NAME_NODE = "mailbox_name";
    
    public static final String EMAIL_TYPE_AUTO_REPLY = "AUTO_REPLY";
}