package com.ftd.queue.email.dao;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * This class is the Data access class used for determining if refunds
 * can automatically be processed for a user cancellation request.
 */
public class AutoRefundDAO  {
  private Connection dbConnection;
  private Logger logger = new Logger("com.ftd.queue.email.dao.AutoRefundDAO");

  public AutoRefundDAO(Connection conn) {
    super();
    dbConnection = conn;
  }   
  
  /**
   * Determine if order can be auto-refunded.
   * 
   * @param orderDetailId
   * @return true if auto refund is allowed for order
   */
  public boolean isAutoRefundAllowed(String orderDetailId)
      throws SAXException, ParserConfigurationException, IOException, 
          SQLException, Exception
  {  
      boolean retStat = false;
      
      if(logger.isDebugEnabled()) {
          logger.debug("Entering isAutoRefundAllowed");
      }
      DataRequest request = new DataRequest();

      try {
          // Setup store procedure input parameters 
          HashMap inputParams = new HashMap();
          inputParams.put("IN_ORDER_DETAIL_ID", orderDetailId);   

          // Build DataRequest object
          request.setConnection(dbConnection);
          request.reset();
          request.setInputParams(inputParams);
          request.setStatementID("IS_AUTO_REFUND_ALLOWED");

          // Get data
          DataAccessUtil dau = DataAccessUtil.getInstance();
          Map outputs = (Map) dau.execute(request);
          String procStatus = (String) outputs.get("OUT_STATUS");
          if(!procStatus.equals("Y")) {
            String errorMessage = (String) outputs.get("OUT_MESSAGE");
            logger.debug(errorMessage);
            retStat = false;
          } else {
            retStat = true;
          }
          
      } finally {
          if(logger.isDebugEnabled()){
             logger.debug("Exiting isAutoRefundAllowed");
          } 
      }
      return retStat;
  }
  
}