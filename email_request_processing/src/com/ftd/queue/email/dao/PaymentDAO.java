package com.ftd.queue.email.dao;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.queue.email.vo.PaymentVO;


/**
 * PaymentDAO
 * Obtains payment information
 */
public class PaymentDAO
{
  private Connection connection;
  private static Logger logger  = new Logger("com.ftd.queue.email.dao.PaymentDAO");

  /**
   * Constructor
   * @param connection Connection
   */
  public PaymentDAO(Connection connection)
  {
    this.connection = connection;
  }

  /**
   * Obtains payment information for a given order detail id.
   * @return List containing PaymentVO's
   * @throws java.lang.Exception
   */
  public List getPaymentInfo(String orderDetailId)
    throws Exception
  {
    if(logger.isDebugEnabled())
    {
      logger.debug("PaymentDAO.getPaymentInfo(String orderDetailId)");
      logger.debug("orderDetailId:" + orderDetailId);
    }
    
    ArrayList payments = new ArrayList();
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);

    dataRequest.setStatementID("GET_ORDER_PAYMENTS");
    dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);
    
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    CachedResultSet crs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

    PaymentVO pVO = null;
    while(crs.next())
    {
      pVO = new PaymentVO();
      pVO.setPaymentType(crs.getString("payment_type"));
      payments.add(pVO);
    }

    return payments;    
  }
}