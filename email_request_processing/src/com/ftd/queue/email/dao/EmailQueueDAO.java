/**
* DAO for email queue
* 
* Class Name:   EmailQueueDAO
* Revision:     1.0
* Date:         February 25, 2005
* Author:       Charles Fox, Software Architects, Inc.
*/

package com.ftd.queue.email.dao;

import java.io.IOException;
import java.io.Reader;
import java.io.StringWriter;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringUtils;
import org.xml.sax.SAXException;

import com.ftd.messagegenerator.vo.StockMessageVO;
import com.ftd.op.mercury.to.ASKMessageTO;
import com.ftd.op.mercury.to.CANMessageTO;
import com.ftd.op.venus.to.CancelOrderTO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.MessageUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.j2ee.JDBCConnectionUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.queue.email.constants.EmailConstants;
import com.ftd.queue.email.vo.CompanyEmailVO;
import com.ftd.queue.email.vo.EmailRequestVO;
import com.ftd.queue.email.vo.InquiryDetailVO;
import com.ftd.queue.email.vo.OrderDetailVO;
import com.ftd.queue.email.vo.QueueVO;

/**
 * This class is the Data access class for inserting and 
 * retrieving email queues in the Queue database schema.  
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 * @todo confirm stored procedure setup against actual stored procedures
 */

public class EmailQueueDAO 
{

    private Logger logger = new Logger("com.ftd.queue.email.dao.EmailQueueDAO");
    private String status;
    private String message;
    
    
    //database
    private Connection dbConnection;
    
    public EmailQueueDAO(Connection conn)
    {
        super();
        dbConnection = conn;
    }
    /**
     * Archive the original XML to the Queue database
     * @param emailRequestXMLString - XML request
     * @return n/a
     * @throws SQLException, Exception
     * @todo stored procedure needs to be completed
     */
    public boolean archive(String emailRequestXMLString, String seqNumber) 
        throws IOException, SAXException, ParserConfigurationException, 
         SQLException, Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering archive");
            //logger.debug("emailRequestXMLString : " + emailRequestXMLString);
            logger.debug("Sequence Number : " + seqNumber);
        }
         boolean duplicate = false;
         DataRequest request = new DataRequest();
         MessageUtil messageUtil = MessageUtil.getInstance();
        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(EmailConstants.ARCHIVE_ORG_DATA_IN_XML_SEQ, 
                seqNumber);
            inputParams.put(EmailConstants.ARCHIVE_ORG_DATA_IN_XML_MESSAGE, 
                emailRequestXMLString);

            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID(EmailConstants.ARCHIVE_ORG_DATA);

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            /* read store prodcedure output parameters to determine 
             * if the procedure executed successfully */
                status = (String) outputs.get(
                    EmailConstants.ARCHIVE_ORG_DATA_OUT_STATUS_PARAM);
                if(status.equals("N"))
                {
                
                    message = (String) outputs.get(
                    EmailConstants.ARCHIVE_ORG_DATA_OUT_MESSAGE_PARAM);
                    if(message.equals(EmailConstants.ARCHIVE_ORG_DATA_DUPLICATE_MSG))
                    {
                       duplicate = true; 
                    }
                    else
                    {
                        //check message
                        String messageString = 
                            messageUtil.getMessageContent(
                            EmailConstants.UNABLE_ARCHIVE_EMAIL_XML);
                        throw new Exception(messageString + " " +  message);
                    }
                }
      
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting archive");
            } 
        }
        return duplicate;
    }
    
    
    /**
     * Gets a request email in XML format using the 
     * clean.email_xml_archive_pkg.GET_EMAIL_XML procedure
     * 
     * @param seqNumber - the sequence number of the email XML record you wish to retreive
     * @return
     * @throws IOException
     * @throws SAXException
     * @throws ParserConfigurationException
     */
    public String getEmailRequestXML( String seqNumber) 
        throws IOException, SAXException, ParserConfigurationException 
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering getEmailRequestXML");
            //logger.debug("emailRequestXMLString : " + emailRequestXMLString);
            logger.debug("Sequence Number : " + seqNumber);
        }
        
        String emailRequestXML = "";
        /* setup store procedure input parameters */
        HashMap<String, String> inputParams = new HashMap<String, String>();
        inputParams.put(EmailConstants.GET_EMAIL_XML_IN_SEQ_NUMBER,seqNumber);
        DataRequest request = new DataRequest();
        /* build DataRequest object */
        request.setConnection(dbConnection);
        request.reset();
        request.setInputParams(inputParams);
        request.setStatementID(EmailConstants.GET_EMAIL_XML);
        DataAccessUtil dau = DataAccessUtil.getInstance();       
        request.setStatementID("GET_EMAIL_XML");
        
        try {
			// get data
        	Clob xmlClob = (Clob) dau.execute(request);
			/* read store prodcedure output parameters to determine 
			 * if the procedure executed successfully */
//		    Clob xmlClob = (Clob) outputs.get(
//		        EmailConstants.GET_EMAIL_XML_OUT_CURSOR);
		    Reader charStream = null;        
		    StringWriter sw=  new StringWriter();

		    if (xmlClob != null) {
		    	charStream = xmlClob.getCharacterStream();
		        int length;
		        while((length=charStream.read())!=-1) {
		          sw.write(length);
		        }
		    }
		    emailRequestXML=sw.toString();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}           
            
        if(logger.isDebugEnabled()){
           logger.debug("Exiting getEmailRequestXML");
        } 
        return emailRequestXML;
    }
    
    
    
    


   /**
     * Archive the original XML to the Queue database
     * @param emailRequestXMLString - XML request
     * @return String - Point of Contact ID
     * @throws SQLException, Exception
     * @todo stored proc not ready change name of stored proc
     */
    public String storeEmailRequest(EmailRequestVO emailRequest) throws 
        IOException, SAXException, ParserConfigurationException, 
         SQLException, Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering storeEmailRequest");
            logger.debug("emailRequest : " + emailRequest);
        }
         String pointOfContactID = null;
         
         DataRequest request = new DataRequest();
         try {
         MessageUtil messageUtil = MessageUtil.getInstance();
        
            InquiryDetailVO inqVO = emailRequest.getInquiryVO();
            OrderDetailVO ordDetailVO=emailRequest.getOrderDetailVO();
            /* setup store procedure input parameters and parse out attributes*/
            HashMap inputParams = new HashMap();
            if(ordDetailVO!=null&&StringUtils.isNotEmpty(ordDetailVO.getOrderDetailID())){
                logger.debug("order detail vo: "+ordDetailVO);
                inputParams.put("IN_CUSTOMER_ID", ordDetailVO.getCustomerID());
                inputParams.put("IN_ORDER_GUID", ordDetailVO.getOrderGuid());
                inputParams.put("IN_ORDER_DETAIL_ID", ordDetailVO.getOrderDetailID());
                inputParams.put("IN_MASTER_ORDER_NUMBER", ordDetailVO.getMasterOrderNumber());
                inputParams.put("IN_EXTERNAL_ORDER_NUMBER", ordDetailVO.getExternalOrderNumber());
            }else{
                inputParams.put("IN_CUSTOMER_ID", null);
                inputParams.put("IN_ORDER_GUID", null);
                inputParams.put("IN_ORDER_DETAIL_ID", null);
                inputParams.put("IN_MASTER_ORDER_NUMBER", null);
                inputParams.put("IN_EXTERNAL_ORDER_NUMBER", emailRequest.getOrderNumber());
            }
            inputParams.put("IN_SOURCE_CODE", emailRequest.getSourceCode());
            inputParams.put("IN_DELIVERY_DATE", emailRequest.getDeliveryDateOne());
            inputParams.put("IN_COMPANY_ID", emailRequest.getCompanyID());    
            inputParams.put("IN_FIRST_NAME", emailRequest.getBillingFirstName());                     
            inputParams.put("IN_LAST_NAME", emailRequest.getBillingLastName());   
            inputParams.put("IN_DAYTIME_PHONE_NUMBER", emailRequest.getDayPhone());
            inputParams.put("IN_EVENING_PHONE_NUMBER", emailRequest.getEvePhone());
            inputParams.put("IN_SENDER_EMAIL_ADDRESS", emailRequest.getEmail());
            inputParams.put("IN_LETTER_TITLE", null);
            inputParams.put("IN_EMAIL_SUBJECT", "Novator Email Request: "+emailRequest.getRequestType());
            if(inqVO!=null)
            {
              inputParams.put("IN_BODY", inqVO.getOtherComment());
            }else
            {
              inputParams.put("IN_BODY", null);
            }
            
            inputParams.put("IN_COMMENT_TYPE", emailRequest.getSubType());
            inputParams.put("IN_RECIPIENT_EMAIL_ADDRESS", null);
            inputParams.put("IN_NEW_RECIP_FIRST_NAME", inqVO.getRecipientFirstName());
            inputParams.put("IN_NEW_RECIP_LAST_NAME", inqVO.getRecipientLastName());
            inputParams.put("IN_NEW_ADDRESS", inqVO.getRecipientAddress());
            inputParams.put("IN_NEW_CITY", inqVO.getRecipientCity());
            inputParams.put("IN_NEW_PHONE", inqVO.getRecipientPhone());
            inputParams.put("IN_NEW_ZIP_CODE", inqVO.getRecipientZip());
            inputParams.put("IN_NEW_STATE", inqVO.getRecipientState());
            inputParams.put("IN_NEW_COUNTRY", inqVO.getRecipientCountry());
            inputParams.put("IN_NEW_INSTITUTION", inqVO.getInstitution());   
            inputParams.put("IN_NEW_DELIVERY_DATE", inqVO.getNewDeliveryDateOne());
            inputParams.put("IN_NEW_CARD_MESSAGE", inqVO.getCardMessage());
            inputParams.put("IN_NEW_CARD_SIGNATURE", inqVO.getCardSignature()); 
            inputParams.put("IN_EMAIL_HEADER", null); 
            inputParams.put("IN_NEW_PRODUCT", inqVO.getProductSku());
            inputParams.put("IN_CHANGE_CODES", null);
            inputParams.put("IN_SENT_RECEIVED_INDICATOR", "I");
            inputParams.put("IN_SEND_FLAG", null);
            inputParams.put("IN_PRINT_FLAG", null);
            inputParams.put("IN_POINT_OF_CONTACT_TYPE", "Email");
            inputParams.put("IN_COMMENT_TEXT", emailRequest.getComment());
            inputParams.put("IN_CREATED_BY", "SYS");
                                                 
               
            
            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("QUEUE_INSERT_POINT_OF_CONTACT");

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            /* read store prodcedure output parameters to determine 
             * if the procedure executed successfully */
                status = (String) outputs.get(EmailConstants.EMAIL_REQUEST_OUT_STATUS_PARAM);
                if(status.equals("N"))
                {
                    message = (String) outputs.get(
                    EmailConstants.EMAIL_REQUEST_OUT_MESSAGE_PARAM);
                    String messageString = 
                        messageUtil.getMessageContent(
                        EmailConstants.UNABLE_ARCHIVE_MSG);
                    throw new Exception(messageString + " " +  message);
                }
                else
                {
                    pointOfContactID =  outputs.get(
                        EmailConstants.EMAIL_REQUEST_OUT_SEQUENCE_NUMBER).toString();

                }
            
        }finally {
            // Since this DML involved CLOBs, cleanup any temporarily generated CLOBs.
            request.reset();
            
         logger.debug("pointOfContactID " + pointOfContactID);
            if(logger.isDebugEnabled()){
               logger.debug("Exiting storeEmailRequest");
            } 
        }
        
        return pointOfContactID;
    }  
    /**
     * Store email queue value object to the Queue database.
     * @param emailQueueVO - QueueVO instance
     * @return n/a
     * @throws SQLException, Exception
     */
    public void store(QueueVO emailQueueVO) throws 
        IOException, SAXException, SQLException, ParserConfigurationException, 
         Exception
    
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering store");
            logger.debug("emailQueueVO : " + emailQueueVO);
        }
         DataRequest request = new DataRequest();
         MessageUtil messageUtil = MessageUtil.getInstance();
        try {
    
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(EmailConstants.ADD_EMAIL_QUEUE_IN_QUEUE_TYPE, 
                emailQueueVO.getQueueType());
            inputParams.put(EmailConstants.ADD_EMAIL_QUEUE_IN_POINT_OF_CONTACT_ID,
                emailQueueVO.getPointOfContactID());
            inputParams.put(EmailConstants.ADD_EMAIL_QUEUE_IN_TIME_STAMP,
                emailQueueVO.getTimeStamp());
            inputParams.put(EmailConstants.ADD_EMAIL_QUEUE_IN_SYSTEM, 
                emailQueueVO.getSystem());
            inputParams.put(EmailConstants.ADD_EMAIL_QUEUE_IN_MERCURY_NUMBER, 
                emailQueueVO.getMercuryNumber());
            inputParams.put(EmailConstants.ADD_EMAIL_QUEUE_IN_MESSAGE_TYPE, 
                emailQueueVO.getMessageType());
            
            inputParams.put(EmailConstants.ADD_EMAIL_QUEUE_IN_MASTER_ORDER_NUMBER, 
                emailQueueVO.getOrderDetailVO().getMasterOrderNumber());
            inputParams.put(EmailConstants.ADD_EMAIL_QUEUE_IN_ORDER_GUID, 
                emailQueueVO.getOrderDetailVO().getOrderGuid());
            inputParams.put(EmailConstants.ADD_EMAIL_QUEUE_IN_ORDER_DETAIL_ID, 
                emailQueueVO.getOrderDetailVO().getOrderDetailID());
            inputParams.put(EmailConstants.ADD_EMAIL_QUEUE_IN_EMAIL, 
                emailQueueVO.getEmail());                
            inputParams.put(EmailConstants.ADD_EMAIL_QUEUE_IN_EXTERNAL_ORDER_NUMBER, 
                emailQueueVO.getOrderDetailVO().getExternalOrderNumber());

            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID(EmailConstants.QUEUE_ADD_EMAIL_QUEUE);

           /* execute the store prodcedure and retrieve output*/
            DataAccessUtil dau = DataAccessUtil.getInstance();

            Map outputs = (Map) dau.execute(request);
            /* read store prodcedure output parameters to determine 
             * if the procedure executed successfully */
                status = (String) outputs.get(
                    EmailConstants.ADD_EMAIL_QUEUE_OUT_STATUS_PARAM);
                if(status.equals("N"))
                {
                    message = (String) outputs.get(EmailConstants.ADD_EMAIL_QUEUE_OUT_MESSAGE_PARAM);
                    String messageString = 
                        messageUtil.getMessageContent(
                        EmailConstants.UNABLE_STORE_QUEUE);
                    throw new Exception(messageString + " " +  message);
                }
            
        }finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting store");
            } 
        }
    }

    /**
     * Create a new instance of QueueVO, 
     * Look up database based on given order external number and email address, 
     * Populate QueueVO with data retrieved from database.
     * 
     * @param emailRequestXMLString - XML request
     * @return QueueVO
     * @throws SQLException, Exception
     * @todo waiting to verify against stored procedure
     */    
    public QueueVO createEmailQueueVO(String orderNumber, String custEmail, 
            String requestType) 
        throws 
        IOException, SAXException, SQLException, ParserConfigurationException, 
         Exception{
        if(logger.isDebugEnabled()){
            logger.debug("Entering createEmailQueueVO");
            logger.debug("orderNumber : " + orderNumber);
            logger.debug("email : " + custEmail);
            logger.debug("requestType : " + requestType);
        }
        
         DataRequest request = new DataRequest();
         MessageUtil messageUtil = MessageUtil.getInstance();
         QueueVO queueVO = new QueueVO();
         CachedResultSet queueRS=null;
        try {

            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(
                EmailConstants.GET_QUEUE_IN_ORDER_NUMBER, orderNumber);
            inputParams.put(
                EmailConstants.GET_QUEUE_IN_REQUEST_TYPE, requestType);
            
            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID(EmailConstants.QUEUE_GET_QUEUE);

           /* execute the store prodcedure */
            DataAccessUtil dau = DataAccessUtil.getInstance();
            queueRS = (CachedResultSet) dau.execute(request);
            while(queueRS.next())      // get queue config context parms
            {  
                queueVO.setMessageID(queueRS.getString(1));

                String ordDetailID = queueRS.getString(8);
                String extOrderNumber = queueRS.getString(10);
                String masterOrderNumber = queueRS.getString(6);
                
                /* Populate QueueVO with stored procedure results  */
                queueVO.setEmail(queueRS.getString(12));
                queueVO.setMercuryNumber(queueRS.getString(5));

                queueVO.setMessageType(queueRS.getString(3));
                queueVO.setPointOfContactID(queueRS.getString(9));
                queueVO.setPriority(queueRS.getInt(11));
                
                queueVO.setQueueType(queueRS.getString(2));
                queueVO.setSystem(queueRS.getString(4));
               
                if(masterOrderNumber!=null){
                    queueVO.setAttached(EmailConstants.ORDER_ATTACHED);
                }
                else
                {
                    queueVO.setAttached(EmailConstants.ORDER_UNATTACHED);
                }

                if(queueRS.getInt(13) > 0)
                {
                    queueVO.setTagged(EmailConstants.ORDER_TAGGED);
                }
                else
                {
                    queueVO.setTagged(EmailConstants.ORDER_UNTAGGED);   
                }
            }

        }finally {
           // this.closeResultSet(queueRS);
            if(logger.isDebugEnabled()){
               logger.debug("Exiting createEmailQueueVO");
            } 
        }
        
        return queueVO;
    }

    /**
     * Look up queues in database based on given order external number.
     * Iterate through results to find the highest priority queue for the order
     * Return QueueVO with highest priority if there is one, otherwise 
     * return null.
     * @param orderDetailID
     * @return QueueVO
     * @throws SQLException, Exception
     * @todo need stored procedure
     */    
    public QueueVO lookupHighestPriorityExistingQueue
    (String orderDetailID) throws 
        IOException, SAXException, SQLException, ParserConfigurationException, 
         Exception{
        if(logger.isDebugEnabled()){
            logger.debug("Entering lookupHighestPriorityExistingQueue");
            logger.debug("orderDetailID : " + orderDetailID);
        }
          DataRequest request = new DataRequest();
         MessageUtil messageUtil = MessageUtil.getInstance();
         QueueVO queueVO = null;
         CachedResultSet queueRS=null;
        try {

            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(
                EmailConstants.GET_ORDR_HIGHEST_PRIORITY_QUE_IN_ORDER_DETAIL_ID, 
                orderDetailID);

            inputParams.put(
                EmailConstants.GET_ORDR_HIGHEST_PRIORITY_QUE_IN_QUEUE_INDICATOR, 
                EmailConstants.EMAIL_QUEUE_INDICATOR);

            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID(EmailConstants.GET_ORDR_HIGHEST_PRIORITY_QUE);

           /* execute the store prodcedure */
            DataAccessUtil dau = DataAccessUtil.getInstance();
            queueRS = (CachedResultSet) dau.execute(request);
             while(queueRS.next())      // get queue config context parms
            {
                /* Populate QueueVO with stored procedure results  */
                queueVO= new QueueVO();
                queueVO.setMercuryNumber(queueRS.getString("mercury_number"));
                queueVO.setMessageID(queueRS.getString("message_id"));
                queueVO.setMessageType(queueRS.getString("message_type"));
                queueVO.setPointOfContactID(queueRS.getString("point_of_contact_id"));
                queueVO.setPriority(queueRS.getInt("priority"));
                queueVO.setQueueType(queueRS.getString("queue_type"));
                queueVO.setSystem(queueRS.getString("system"));
                String masterOrderNumber = queueRS.getString("master_order_number");
                if(masterOrderNumber!=null){
                    queueVO.setAttached(EmailConstants.ORDER_ATTACHED);
                }
                else
                {
                    queueVO.setAttached(EmailConstants.ORDER_UNATTACHED);
                }

            }
               
        
        }finally {
           
            if(logger.isDebugEnabled()){
               logger.debug("Exiting lookupHighestPriorityExistingQueue");
            } 
        }

        
        return queueVO;
    }
    
    /**
     * Lock order
     * @param OrderVO
     * @return n/a
     * @throws SQLException, Exception
     * @todo stored procedure not completed
     */
    public boolean lockOrder(OrderDetailVO orderVO, String sessionId, String csr_id) 
      throws 
        IOException, SAXException, SQLException, ParserConfigurationException, 
         Exception{
        if(logger.isDebugEnabled()){
            logger.debug("Entering lockOrder");
            logger.debug("orderVO : " + orderVO);
            logger.debug("sessionId : " + sessionId);
            logger.debug("csr_id : " + csr_id);
        }
       
         DataRequest request = new DataRequest();
         MessageUtil messageUtil = MessageUtil.getInstance();
        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(EmailConstants.LOCK_ORDER_IN_ENTITY_TYPE, 
                "MODIFY_ORDER");
            inputParams.put(EmailConstants.LOCK_ORDER_IN_ENTITY_ID, 
                orderVO.getOrderDetailID());
            inputParams.put(EmailConstants.LOCK_ORDER_IN_SESSION_ID, 
                sessionId);
            inputParams.put(EmailConstants.LOCK_ORDER_IN_CSR_ID, csr_id);
            
            inputParams.put("IN_ORDER_LEVEL", null);
            logger.debug("lock inputParams="+inputParams);
            // build DataRequest object
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID(EmailConstants.LOCK_ORDER);

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
               String lockObtained=(String)outputs.get("OUT_LOCK_OBTAINED");
            logger.debug("lockObtained =["+lockObtained+"]");   
            return (lockObtained!=null&&lockObtained.trim().equalsIgnoreCase("Y"));
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting lockOrder");
            } 
        }       
      
    }
    
    
    
    /**
     * Lock order
     * @param OrderVO
     * @return n/a
     * @throws SQLException, Exception
     * @todo stored procedure not completed
     */
    public boolean isOrderLocked(OrderDetailVO orderVO, String sessionId, String csr_id) 
      throws 
        IOException, SAXException, SQLException, ParserConfigurationException, 
         Exception{
        if(logger.isDebugEnabled()){
            logger.debug("Entering isOrderLocked");
            logger.debug("orderVO : " + orderVO);
            logger.debug("sessionId : " + sessionId);
            logger.debug("csr_id : " + csr_id);
        }
       
         DataRequest request = new DataRequest();
         MessageUtil messageUtil = MessageUtil.getInstance();
        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(EmailConstants.LOCK_ORDER_IN_ENTITY_TYPE, 
                "MODIFY_ORDER");
            inputParams.put(EmailConstants.LOCK_ORDER_IN_ENTITY_ID, 
                orderVO.getOrderDetailID());
            inputParams.put(EmailConstants.LOCK_ORDER_IN_SESSION_ID, 
                sessionId);
            inputParams.put(EmailConstants.LOCK_ORDER_IN_CSR_ID, csr_id);
            
            inputParams.put("IN_ORDER_LEVEL", null);
            logger.debug("lock inputParams="+inputParams);
            // build DataRequest object
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("CHECK_CSR_LOCKED_ENTITIES");

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            String locked=(String)outputs.get("OUT_LOCKED_IND");
            String lockedCsr=(String)outputs.get("OUT_LOCKED_CSR_ID");
            logger.debug("locked =["+locked+"]");   
            boolean isLocked =(locked!=null&&locked.trim().equalsIgnoreCase("Y"));
            if(isLocked)
            {
              logger.warn("Order "+orderVO+"is locked by "+lockedCsr);
            }
            return isLocked;
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting isOrderLocked");
            } 
        }       
      
    }
    
    
    

    /**
     * remove queue
     * @param OrderVO
     * @return n/a
     * @throws SQLException, Exception
     * @todo stored procedure not completed
     */
    public void removeQueue(String messageID) throws 
        IOException, SAXException, SQLException, ParserConfigurationException, 
         Exception{
        if(logger.isDebugEnabled()){
            logger.debug("Entering removeQueue");
            logger.debug("MessageID : " + messageID);
        }
         DataRequest request = new DataRequest();
         MessageUtil messageUtil = MessageUtil.getInstance();
        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(
                EmailConstants.REMOVE_QUEUE_IN_MESSAGE_ID, messageID);
            inputParams.put(
                    "IN_CSR_ID", "SYS");
            
            
            // build DataRequest object
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID(EmailConstants.QUEUE_REMOVE_QUEUE);

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
                status = (String) outputs.get(
                    EmailConstants.REMOVE_QUEUE_OUT_STATUS);
                if(status.equals("N"))
                {
                    message = (String) outputs.get(
                        EmailConstants.REMOVE_QUEUE_OUT_OUT_MESSAGE);
                    String messageString = 
                        messageUtil.getMessageContent(
                        EmailConstants.UNABLE_REMOVE_QUEUE);
                    throw new Exception(messageString + " " +  message);
                }
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting removeQueue");
            } 
        }       
    }
    /**
     * Release order lock.
     * @param OrderVO
     * @param hashCode - int
     * @param csr_id - String 
     * @return n/a
     * @throws SQLException, Exception
     * @todo stored procedure not completed
     */
    public void releaseOrder(OrderDetailVO orderVO, String sessionId, String csr_id) 
        throws IOException, SAXException, SQLException, 
        ParserConfigurationException , Exception
        {
        if(logger.isDebugEnabled()){
            logger.debug("Entering releaseOrder");
            logger.debug("orderVO : " + orderVO);
            logger.debug("hashCode : " + sessionId);
            logger.debug("csr_id : " + csr_id);
        }
         DataRequest request = new DataRequest();
         MessageUtil messageUtil = MessageUtil.getInstance();
        
        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(EmailConstants.RELEASE_ORDER_IN_ENTITY_TYPE, 
               "MODIFY_ORDER");
            inputParams.put(EmailConstants.RELEASE_ORDER_IN_ENTITY_ID, 
                orderVO.getOrderDetailID());
            inputParams.put(EmailConstants.RELEASE_ORDER_IN_SESSION_ID, 
                sessionId);
            inputParams.put(EmailConstants.RELEASE_ORDER_IN_CSR_ID, csr_id);
            
            // build DataRequest object
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID(EmailConstants.RELEASE_ORDER);

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
                status = (String) outputs.get(
                    EmailConstants.RELEASE_ORDER_STATUS_PARAM);
                if(status.equals("N"))
                {
                    message = (String) outputs.get(
                        EmailConstants.RELEASE_ORDER_MESSAGE_PARAM);
                    String messageString = 
                        messageUtil.getMessageContent(
                        EmailConstants.UNABLE_RELEASE_ORDER);
                    throw new Exception(messageString + " " +  message);
                }
            
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting releaseOrder");
            } 
        }       
    }

  /**
     * Store email queue value object to the Queue database.
     * @param emailQueueVO - QueueVO instance
     * @return n/a
     * @throws SQLException, Exception
     * @todo find out what to pass into procedure
     */
    public void insertComments(String comments,  OrderDetailVO ordDetail, String csr_id) 
        throws IOException, SAXException, SQLException, 
        ParserConfigurationException , Exception
        {
        if(logger.isDebugEnabled()){
            logger.debug("Entering insertComments");
            logger.debug("comments : " + comments);
            logger.debug("ordDetail : " + ordDetail.getOrderDetailID());
        }
         DataRequest request = new DataRequest();
         MessageUtil messageUtil = MessageUtil.getInstance();
        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();

            inputParams.put(EmailConstants.INSERT_COMMENTS_IN_CUSTOMER_ID, 
                new Long(ordDetail.getCustomerID()));
            inputParams.put(EmailConstants.INSERT_COMMENTS_IN_ORDER_GUID, 
                ordDetail.getOrderGuid());
            inputParams.put(EmailConstants.INSERT_COMMENTS_IN_ORDER_DETAIL_ID, 
                new Long(ordDetail.getOrderDetailID()));
            inputParams.put(EmailConstants.INSERT_COMMENTS_IN_COMMENT_ORIGIN,
                "");
            inputParams.put(EmailConstants.INSERT_COMMENTS_IN_REASON,"");
            inputParams.put(EmailConstants.INSERT_COMMENTS_IN_DNIS_ID, 
                null);
            inputParams.put(EmailConstants.INSERT_COMMENTS_IN_COMMENT_TEXT, 
                comments);
            inputParams.put(EmailConstants.INSERT_COMMENTS_IN_COMMENT_TYPE,
                EmailConstants.ORDER_COMMENTS_TYPE);
            inputParams.put(EmailConstants.INSERT_COMMENTS_IN_CSR_ID,csr_id);
        
            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID(EmailConstants.INSERT_COMMENTS_PRODCEDURE);

           /* execute the store prodcedure and retrieve output*/
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            /* read store prodcedure output parameters to determine 
             * if the procedure executed successfully */
                status = (String) outputs.get(
                    EmailConstants.QUEUE_OUT_STATUS_PARAM);
                if(status.equals("N"))
                {
                    message = (String) outputs.get(
                        EmailConstants.QUEUE_OUT_MESSAGE_PARAM);
                    String messageString = 
                        messageUtil.getMessageContent(
                        EmailConstants.UNABLE_STORE_QUEUE);
                    throw new Exception(messageString + " " +  message);
                }
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting insertComments");
            } 
        }
    }

  /**
     * retrieve high level order detail information
     * @param orderNumber - String
     * @return OrderDetailVO
     * @throws IOException, SAXException, SQLException, 
        ParserConfigurationException , Exception
     */
    public OrderDetailVO locateOrderDetail(String orderNumber, String custEmail,
        String custLastName, String custFirstName)
        throws IOException, SAXException, SQLException, 
        ParserConfigurationException , Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering locateOrderDetail");
            logger.debug("orderNumber : " + orderNumber);
            logger.debug("custEmail : " + custEmail);
            logger.debug("custLastName : " + custLastName);
            logger.debug("custFirstName : " + custFirstName);
        }        
        
        DataRequest request = new DataRequest();
        MessageUtil messageUtil = MessageUtil.getInstance();
        String orderDetailID = "";
        OrderDetailVO ordDetail = new OrderDetailVO();
        try {
        
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(
            EmailConstants.PROC_FON_IN_ORDER_NUMBER, orderNumber);
            inputParams.put(
            EmailConstants.PROC_FON_IN_EMAIL_ADDRESS, custEmail);            
            inputParams.put(
            EmailConstants.PROC_FON_IN_LAST_NAME, custLastName);            
            inputParams.put(
            EmailConstants.PROC_FON_IN_FIRST_NAME, custFirstName);
            
            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID(EmailConstants.PROC_FIND_ORDER_NUMBER);

           /* execute the store prodcedure */
            DataAccessUtil dau = DataAccessUtil.getInstance();
            CachedResultSet outputs = (CachedResultSet) dau.execute(request);
            /* read store prodcedure output parameters to determine 
             * if the procedure executed successfully */  
             
             if(outputs.next()){
                if(outputs.getString("ORDER_DETAIL_ID")!= null){
                   // orderDetailID = (String) outputs.get(
                     //   EmailConstants.PROC_FON_OUT_ORDER_DETAIL_ID).toString();
                    ordDetail.setOrderDetailID(outputs.getString("order_detail_id"));
                    ordDetail.setOrderGuid(outputs.getString("order_guid"));
                    ordDetail.setExternalOrderNumber(outputs.getString("external_order_number"));
                    ordDetail.setMasterOrderNumber(outputs.getString("master_order_number"));
                    ordDetail.setCustomerID(outputs.getString("customer_id"));  
                    ordDetail.setOrderDisposition( outputs.getString("order_disp_code")); 
                    ordDetail.setSource( outputs.getString("order_indicator"));  
                    String ftpVendor=outputs.getString("ftp_vendor_flag");
                    logger.debug("FTP vendor flag="+ftpVendor);
                    ordDetail.setFtpVendor((ftpVendor!=null&&ftpVendor.trim().equalsIgnoreCase("Y")));
                    logger.debug("FTP vendor ? = "+ordDetail.isFtpVendor());
                    ordDetail.setSourceCode( outputs.getString("source_code"));  
                 }
             }
        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting findOrderDetailID");
            }  
        }
        return ordDetail;
    }
    
    public void setOrderDetailVO(String orderNumber,OrderDetailVO ordDetail) 
        throws IOException, SAXException, SQLException, 
        ParserConfigurationException , Exception
     {
        if(logger.isDebugEnabled()){
            logger.debug("Entering setOrderDetailVO");
            logger.debug("orderNumber : " + orderNumber);
            
        } 
         DataRequest request = new DataRequest();
         MessageUtil messageUtil = MessageUtil.getInstance();
         CachedResultSet orderDetailRS=null;
        try {
            if(orderNumber!=null){
                /* setup store procedure input parameters */
                HashMap inputParams = new HashMap();
                inputParams.put(EmailConstants.ORDER_DETAIL_IN_ORDER_NUMBER, 
                    orderNumber);
                
                /* build DataRequest object */
                request.setConnection(dbConnection);
                request.reset();
                request.setInputParams(inputParams);
                request.setStatementID(
                    EmailConstants.QUEUE_RETRIEVE_ORDER_DETAIL);
    
               /* execute the store prodcedure */
                DataAccessUtil dau = DataAccessUtil.getInstance();
                orderDetailRS = (CachedResultSet) dau.execute(request);
                /* read store prodcedure output parameters to determine 
                 * if the procedure executed successfully */
                if(orderDetailRS.next())      // get queue config context parms
                {
                    ordDetail.setCardMessage(orderDetailRS.getString("card_message"));
                    ordDetail.setCardSignature(orderDetailRS.getString("card_signature"));
                    SimpleDateFormat sdfOutput = 
                           new SimpleDateFormat ("MM/dd/yyyy"); 
                    String textDate = orderDetailRS.getString("delivery_date");
                    
                    if(textDate!=null){
                       try{
                        java.util.Date utilDate = sdfOutput.parse( textDate );
                        ordDetail.setDeliveryDate(utilDate);
                       }catch(ParseException pe)
                       {
                         logger.error("can not parse order delivery date "+textDate+" for order ["+orderNumber+"]", pe);
                       }
                    }
                    ordDetail.setFillingFloristId(orderDetailRS.getString("florist_id"));
                    ordDetail.setInstitutionName(orderDetailRS.getString("business_name"));
                    ordDetail.setMercuryFlag(orderDetailRS.getString("mercury_flag"));
                    logger.debug("mercury flag="+ordDetail.getMercuryFlag());
                    ordDetail.setVendorFlag(orderDetailRS.getString("vendor_flag"));
                    logger.debug("venus flag="+ordDetail.getVendorFlag());
                    String ftdm=orderDetailRS.getString("ftdm_indicator");
                    logger.debug("ftdm indicator="+ftdm);
                    ordDetail.setOrderFTDM(StringUtils.isNotBlank(ftdm)&&ftdm.trim().equalsIgnoreCase("Y"));
                    ordDetail.setProductSku(orderDetailRS.getString("product_id"));
                    ordDetail.setReceipientFirstName(orderDetailRS.getString("first_name"));
                    ordDetail.setReceipientLastName(orderDetailRS.getString("last_name"));
                    ordDetail.setReceipientAddress(orderDetailRS.getString("address_1"));
                    ordDetail.setReceipientCity(orderDetailRS.getString("city"));
                    ordDetail.setReceipientState(orderDetailRS.getString("state"));
                    ordDetail.setReceipientZip(orderDetailRS.getString("zip_code"));
                    ordDetail.setReceipientCountry(orderDetailRS.getString("country"));
                    ordDetail.setRecipientLocationType(orderDetailRS.getString("address_type"));
                    ordDetail.setReceipientPhone(orderDetailRS.getString("recipient_phone_number"));
                    
                    ordDetail.setTimeZone(orderDetailRS.getString("timezone"));
                    ordDetail.setTrackingNumber(orderDetailRS.getString("tracking_number"));
                    ordDetail.setRecipientID(orderDetailRS.getString("recipient_id"));
                    ordDetail.setSubstitutionIndicator(orderDetailRS.getString("substitution_indicator"));
                    ordDetail.setOccasion(orderDetailRS.getString("occasion"));
                    ordDetail.setReleaseInfoIndicator(orderDetailRS.getString("release_info_indicator"));
                    ordDetail.setSpecialInstructions(orderDetailRS.getString("special_instructions"));
                    ordDetail.setQuantity(orderDetailRS.getString("quantity"));
                    ordDetail.setColor1(orderDetailRS.getString("color_1"));
                    ordDetail.setColor2(orderDetailRS.getString("color_2"));
                    ordDetail.setSubstitutionIndicator(orderDetailRS.getString("substitution_indicator"));
                    ordDetail.setSameDayGift(orderDetailRS.getString("same_day_gift"));
                    ordDetail.setShipMethod(orderDetailRS.getString("ship_method"));
                    ordDetail.setShipDate(orderDetailRS.getDate("ship_date")); 
                    ordDetail.setLanguageId(orderDetailRS.getString("language_id"));
                    
                }
                
                this.setOrderDetailVOFlags(orderNumber, ordDetail);
              
            }
            else
            {
                String messageString = 
                    messageUtil.getMessageContent(
                    EmailConstants.UNABLE_RETRIEVE_AMAZON_ORDER);
                logger.error(messageString);
                throw new Exception(messageString);    
            }
        }finally {
           // this.closeResultSet(orderDetailRS);
            if(logger.isDebugEnabled()){
                logger.debug("Exiting findOrderDetail");
            } 
        }
        
      //  return retOrderDetailVO;
      }
      
      
      
      
      public void setOrderDetailVOIds(String orderNumber,OrderDetailVO ordDetail) 
        throws IOException, SAXException, SQLException, 
        ParserConfigurationException , Exception
     {
        if(logger.isDebugEnabled()){
            logger.debug("Entering setOrderDetailVOIds");
            logger.debug("orderNumber : " + orderNumber);
            
        } 
         DataRequest request = new DataRequest();
         MessageUtil messageUtil = MessageUtil.getInstance();
         CachedResultSet orderDetailRS=null;
        try {
            if(orderNumber!=null){
                /* setup store procedure input parameters */
                HashMap inputParams = new HashMap();
                inputParams.put(EmailConstants.ORDER_DETAIL_IN_ORDER_NUMBER, 
                    orderNumber);
                
                /* build DataRequest object */
                request.setConnection(dbConnection);
                request.reset();
                request.setInputParams(inputParams);
                request.setStatementID("GET_ORDER_CUSTOMER_INFO");
    
               /* execute the store prodcedure */
                DataAccessUtil dau = DataAccessUtil.getInstance();
                orderDetailRS = (CachedResultSet) dau.execute(request);
                /* read store prodcedure output parameters to determine 
                 * if the procedure executed successfully */
                if(orderDetailRS.next())      // get queue config context parms
                {
                    
                    ordDetail.setExternalOrderNumber(orderDetailRS.getString("external_order_number"));
                    ordDetail.setOrderGuid(orderDetailRS.getString("order_guid"));
                    ordDetail.setMasterOrderNumber(orderDetailRS.getString("master_order_number"));
                    ordDetail.setDeliveryDate(orderDetailRS.getTimestamp("delivery_date"));
                    ordDetail.setLanguageId(orderDetailRS.getString("language_id"));
    
                }
                
               
              
            }
            else
            {
                String messageString = 
                    messageUtil.getMessageContent(
                    EmailConstants.UNABLE_RETRIEVE_AMAZON_ORDER);
                logger.error(messageString);
                throw new Exception(messageString);    
            }
        }finally {
           // this.closeResultSet(orderDetailRS);
            if(logger.isDebugEnabled()){
                logger.debug("Exiting findOrderDetail");
            } 
        }
        
      //  return retOrderDetailVO;
      }
      
      
      
     
  /**
   * When the automated processes below are going through to look to see if there is a live mercury record they will be looking only at FTD records. 
   * They will not look at FTDC records unless the only record is an FTDC.
   * @param orderNumber
   * @param ordDetail
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws java.sql.SQLException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws java.lang.Exception
   */
     public void setOrderDetailVOFlags(String orderNumber,OrderDetailVO ordDetail) 
        throws IOException, SAXException, SQLException, 
        ParserConfigurationException , Exception
     {
        if(logger.isDebugEnabled()){
            logger.debug("Entering setOrderDetailVOFlags");
            logger.debug("orderNumber : " + orderNumber);
            
        } 
         DataRequest request = new DataRequest();
         MessageUtil messageUtil = MessageUtil.getInstance();
        
        try {
            if(orderNumber!=null){
                /* setup store procedure input parameters */
                HashMap inputParams = new HashMap();
                inputParams.put(EmailConstants.ORDER_DETAIL_IN_ORDER_NUMBER, 
                    orderNumber);
                    
                inputParams.put("IN_COMP_ORDER", "N");
                
                /* build DataRequest object */
                request.setConnection(dbConnection);
                request.reset();
                request.setInputParams(inputParams);
                request.setStatementID("GET_ORDER_MESSAGE_STATUS");
    
               /* execute the store prodcedure */
                DataAccessUtil dau = DataAccessUtil.getInstance();
                CachedResultSet result=(CachedResultSet)dau.execute(request);
                if(result.next()){
                  ordDetail.setAttemptedFTDFlag(result.getString("attempted_ftd"));
                  ordDetail.setLiveFTDFlag(result.getString("has_live_ftd"));
                  ordDetail.setCancelSentFlag(result.getString("cancel_sent"));
                  ordDetail.setRejectedFlag(result.getString("reject_sent"));
                  ordDetail.setFtdStatus(result.getString("ftd_status"));
                  ordDetail.setAttemptedCANFlag(result.getString("attempted_can"));
                }
               
              
            }
            else
            {
                String messageString = 
                    messageUtil.getMessageContent(
                    EmailConstants.UNABLE_RETRIEVE_AMAZON_ORDER);
                logger.error(messageString);
                throw new Exception(messageString);    
            }
        }finally {
           // this.closeResultSet(orderDetailRS);
            if(logger.isDebugEnabled()){
                logger.debug("Exiting findOrderDetail");
            } 
        }
        
      
      }
      
      
      
       private String getResultAsString(String key, Map resultMap)
     {
       
       Object obj=resultMap.get(key);
       if(obj!=null && obj instanceof String)
       {
         return (String)obj;
       }else
       {
         return null;
       }
       
     }
      
      

  /**
     * retrieve order information by using order GUID
     * @param ordDetail - OrderDetailVO
     * @return OrderDetailVO
     * @throws IOException, SAXException, SQLException, 
        ParserConfigurationException , Exception
     */
    public void setOrderDetailCompanyIdAndDateByGUID(OrderDetailVO ordDetail) 
        throws IOException, SAXException, SQLException, 
        ParserConfigurationException , Exception
     {
        if(logger.isDebugEnabled()){
            logger.debug("Entering findOrderDetailByGUID");
            logger.debug("Order Detail ID : " + ordDetail.getOrderDetailID());
            logger.debug("Order GUID : " + ordDetail.getOrderGuid());

        } 
         DataRequest request = new DataRequest();
         MessageUtil messageUtil = MessageUtil.getInstance();
         
         CachedResultSet orderDetailRS=null;
        try {
            if(ordDetail!=null){
                /* setup store procedure input parameters */
                HashMap inputParams = new HashMap();
                inputParams.put(EmailConstants.ORDER_BY_GUID_IN_ORDER_GUID, 
                    ordDetail.getOrderGuid());
                
                /* build DataRequest object */
                request.setConnection(dbConnection);
                request.reset();
                request.setInputParams(inputParams);
                request.setStatementID(
                    EmailConstants.QUEUE_GET_ORDER_BY_GUID);
    
               /* execute the store prodcedure */
                DataAccessUtil dau = DataAccessUtil.getInstance();
                orderDetailRS = (CachedResultSet) dau.execute(request);
                /* read store prodcedure output parameters to determine 
                 * if the procedure executed successfully */
                while(orderDetailRS.next())      // get queue config context parms
                {
                    ordDetail.setCompanyID(orderDetailRS.getString(4));
                    ordDetail.setOrderDate(orderDetailRS.getDate(7));
                }
              
            }
            else
            {
                String messageString = 
                    messageUtil.getMessageContent(
                    EmailConstants.UNABLE_RETRIEVE_AMAZON_ORDER);
                logger.error(messageString);
                throw new Exception(messageString);    
            }
        }finally {
           // this.closeResultSet(orderDetailRS);
            if(logger.isDebugEnabled()){
                logger.debug("Exiting findOrderDetail");
            } 
        }
        
      }    
      
      public int retrieveQueuePriority(String queueType) throws IOException, 
        SAXException, SQLException, ParserConfigurationException, 
         Exception
      {
        if(logger.isDebugEnabled()){
            logger.debug("Entering retrieveQueuePriority");
            logger.debug("Queue Type : " + queueType);
        }
         int queuePriority = 0;
         DataRequest request = new DataRequest();
         MessageUtil messageUtil = MessageUtil.getInstance();
        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(EmailConstants.GET_QUEUE_TYPE_PRIORITY_IN_QUEUE_TYPE, 
                queueType);

            // build DataRequest object
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID(EmailConstants.GET_QUEUE_TYPE_PRIORITY);

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            String priority = (String) dau.execute(request);         
            queuePriority = Integer.parseInt(priority);

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting retrieveQueuePriority");
            } 
        }       
        return queuePriority;  
      }
      
      
    /**
     * Update the order detail for an order
     * @param ordDetail - OrderDetailVO
     * @return n/a
     * @throws IOException, SAXException, ParserConfigurationException, 
         SQLException, Exception
     */
    public void modifyOrder(String ordDetailID, InquiryDetailVO inquiry) 
        throws IOException, SAXException, ParserConfigurationException, 
         SQLException, Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering modifyOrder");
            logger.debug("ordDetailID: " + ordDetailID);
            
        }
         updateRecipientInfo(ordDetailID, inquiry);
         //update card message / signature.
         this.updateOrderDetailInfo(ordDetailID, inquiry);
    }

  void updateRecipientInfo(String ordDetailID, InquiryDetailVO inquiry) throws SQLException , SAXException, javax.xml.transform.TransformerException, IOException, ParserConfigurationException, Exception
  {
    DataRequest request = new DataRequest();
    MessageUtil messageUtil = MessageUtil.getInstance();
    try
    {
      HashMap inputParams = new HashMap();
      inputParams.put(EmailConstants.UPDATE_RECIPIENT_IN_ORDER_DETAIL_ID, new Long(ordDetailID));
      inputParams.put(EmailConstants.UPDATE_RECIPIENT_IN_FIRST_NAME, inquiry.getRecipientFirstName());
      inputParams.put(EmailConstants.UPDATE_RECIPIENT_IN_LAST_NAME, inquiry.getRecipientLastName());
      inputParams.put(EmailConstants.UPDATE_RECIPIENT_IN_BUSINESS_NAME, inquiry.getInstitution());
      inputParams.put(EmailConstants.UPDATE_RECIPIENT_IN_ADDRESS_1, null);
      inputParams.put(EmailConstants.UPDATE_RECIPIENT_IN_ADDRESS_2, null);
      inputParams.put(EmailConstants.UPDATE_RECIPIENT_IN_CITY, null);
      inputParams.put(EmailConstants.UPDATE_RECIPIENT_IN_STATE, null);
      inputParams.put(EmailConstants.UPDATE_RECIPIENT_IN_ZIP_CODE, null);
      inputParams.put(EmailConstants.UPDATE_RECIPIENT_IN_COUNTY, null);
      inputParams.put(EmailConstants.UPDATE_RECIPIENT_IN_COUNTRY, null);
      inputParams.put(EmailConstants.UPDATE_RECIPIENT_IN_ADDRESS_TYPE, null);
      String csr_id = ConfigurationUtil.getInstance().getProperty(EmailConstants.ORDER_INFORMATION_CONFIG_FILE, EmailConstants.ORDER_INFORMATION_CSR_ID);
      inputParams.put(EmailConstants.UPDATE_RECIPIENT_IN_UPDATED_BY, csr_id);
      request.setConnection(dbConnection);
      request.reset();
      request.setInputParams(inputParams);
      request.setStatementID(EmailConstants.UPDATE_RECIPIENT);
      DataAccessUtil dau = DataAccessUtil.getInstance();
      Map outputs = (Map)dau.execute(request);
      status = (String)outputs.get(EmailConstants.UPDATE_RECIPIENT_OUT_STATUS_PARAM);
      if (status.equals("N"))
      {
        message = (String)outputs.get(EmailConstants.UPDATE_RECIPIENT_OUT_MESSAGE_PARAM);
        String messageString = messageUtil.getMessageContent(EmailConstants.UNABLE_ARCHIVE_EMAIL_XML);
        throw new Exception(messageString + " " + message);
      }
    }
    finally
    {
      if (logger.isDebugEnabled())
      {
        logger.debug("Exiting modifyOrder");
      }
    }

  }
  
  
  
  
  void updateOrderDetailInfo(String ordDetailID, InquiryDetailVO inquiry) throws SQLException , SAXException, javax.xml.transform.TransformerException, IOException, ParserConfigurationException, Exception
  {
    DataRequest request = new DataRequest();
    MessageUtil messageUtil = MessageUtil.getInstance();
    try
    {
      HashMap inputParams = new HashMap();
      inputParams.put(EmailConstants.UPDATE_RECIPIENT_IN_ORDER_DETAIL_ID, new Long(ordDetailID));
      inputParams.put("IN_CARD_MESSAGE", inquiry.getCardMessage());
      inputParams.put("IN_CARD_SIGNATURE", inquiry.getCardSignature());
      
      String csr_id = ConfigurationUtil.getInstance().getProperty(EmailConstants.ORDER_INFORMATION_CONFIG_FILE, EmailConstants.ORDER_INFORMATION_CSR_ID);
      inputParams.put("IN_UPDATED_BY", csr_id);
      request.setConnection(dbConnection);
      request.reset();
      request.setInputParams(inputParams);
      request.setStatementID("UPDATE_ORDER_DETAILS");
      DataAccessUtil dau = DataAccessUtil.getInstance();
      Map outputs = (Map)dau.execute(request);
      status = (String)outputs.get(EmailConstants.UPDATE_RECIPIENT_OUT_STATUS_PARAM);
      if (status.equals("N"))
      {
        message = (String)outputs.get(EmailConstants.UPDATE_RECIPIENT_OUT_MESSAGE_PARAM);
        String messageString = messageUtil.getMessageContent(EmailConstants.UNABLE_ARCHIVE_EMAIL_XML);
        throw new Exception(messageString + " " + message);
      }
    }
    finally
    {
      if (logger.isDebugEnabled())
      {
        logger.debug("Exiting modifyOrder");
      }
    }

  }
  
  
  
  
  

    /**
     * Look up values for sending a company e-mail for an order
     * @param orderDetailID
     * @return CompanyEmailVO
     * @throws IOException, SAXException, SQLException, 
     * ParserConfigurationException , Exception
     */    
    public CompanyEmailVO getCompanyEmailInformation
    (String orderDetailID, String companyId) throws 
        IOException, SAXException, SQLException, ParserConfigurationException, 
         Exception{
        if(logger.isDebugEnabled()){
            logger.debug("Entering getCompanyEmailInformation");
            logger.debug("orderDetailID : " + orderDetailID);
            logger.debug("companyId : " + companyId);
        }
          DataRequest request = new DataRequest();
         MessageUtil messageUtil = MessageUtil.getInstance();
         CompanyEmailVO companyEmailVO = new CompanyEmailVO();
         CachedResultSet companyRS=null;
        try {

            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(
                EmailConstants.GET_ORDER_COMPANY_EMAIL_IN_ORDER_DETAIL_ID, 
                orderDetailID);
            inputParams.put(EmailConstants.GET_ORDER_COMPANY_EMAIL_IN_COMPANY_ID,
            	companyId);

            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID(EmailConstants.GET_ORDER_COMPANY_EMAIL);

           /* execute the store prodcedure */
            DataAccessUtil dau = DataAccessUtil.getInstance();
            companyRS = (CachedResultSet) dau.execute(request);
             while(companyRS.next())      // get queue config context parms
            {
                companyEmailVO.setCompanyID(companyRS.getString(4));
                companyEmailVO.setCompanyName(companyRS.getString(5));
                companyEmailVO.setPhoneNumber(companyRS.getString(6));
                companyEmailVO.setShipper(companyRS.getString(2));
                companyEmailVO.setTrackingURL(companyRS.getString(3));
            }
               
        
        }finally {
            //this.closeResultSet(companyRS);
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getCompanyEmailInformation");
            } 
        }

        
        return companyEmailVO;
    }
    
    
    
    
    public CANMessageTO getMercuryCANMessage
    (String orderDetailID) throws 
        IOException, SAXException, SQLException, ParserConfigurationException, 
         Exception{
        if(logger.isDebugEnabled()){
            logger.debug("Entering getMercuryCANMessage");
            logger.debug("orderDetailID : " + orderDetailID);
        }
          DataRequest request = new DataRequest();
         CANMessageTO canTO=null;
         CachedResultSet messageRS=null;
        try {

            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_ORDER_DETAIL_ID", orderDetailID);
            inputParams.put("IN_MESSAGE_TYPE", "Mercury");
            inputParams.put("IN_COMP_ORDER", "N");//exclude COMP order.
            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("GET_MESG_DETAIL_FROM_LAST_FTD");

           /* execute the store prodcedure */
            DataAccessUtil dau = DataAccessUtil.getInstance();
            messageRS = (CachedResultSet) dau.execute(request);
             if(messageRS.next())      // get queue config context parms
            {
                canTO=new CANMessageTO();
                canTO.setMercuryId(messageRS.getString("ftd_message_id"));
           

           
            canTO.setFillingFlorist(messageRS.getString("filling_florist"));
            canTO.setSendingFlorist(messageRS.getString("sending_florist"));
            
            }
               
        
        }finally {
            //this.closeResultSet(messageRS);
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getMercuryCANMessage");
            } 
        }

        
        return canTO;
    }
    
    
    
    
    
    public CancelOrderTO getVenusCANMessage
    (String orderDetailID) throws 
        IOException, SAXException, SQLException, ParserConfigurationException, 
         Exception{
        if(logger.isDebugEnabled()){
            logger.debug("Entering getVenusCANMessage");
            logger.debug("orderDetailID : " + orderDetailID);
        }
          DataRequest request = new DataRequest();
         CancelOrderTO canTO=null;
         CachedResultSet messageRS=null;
        try {

            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_ORDER_DETAIL_ID", orderDetailID);
            inputParams.put("IN_MESSAGE_TYPE", "Venus");
            inputParams.put("IN_COMP_ORDER", "N");//exclude COMP order.
            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("GET_MESG_DETAIL_FROM_LAST_FTD");

           /* execute the store prodcedure */
            DataAccessUtil dau = DataAccessUtil.getInstance();
            messageRS = (CachedResultSet) dau.execute(request);
             if(messageRS.next())      // get queue config context parms
            {
                canTO=new CancelOrderTO();
                canTO.setVenusId(messageRS.getString("ftd_message_id"));
                
            
            }
               
        
        }finally {
            //this.closeResultSet(messageRS);
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getMercuryCANMessage");
            } 
        }

        
        return canTO;
    }
    
    
    
    
    
    public ASKMessageTO getMercuryASKMessage
    (String orderDetailID) throws 
        IOException, SAXException, SQLException, ParserConfigurationException, 
         Exception{
        if(logger.isDebugEnabled()){
            logger.debug("Entering getMercuryASKMessage");
            logger.debug("orderDetailID : " + orderDetailID);
        }
          DataRequest request = new DataRequest();
         ASKMessageTO askTO=null;
         CachedResultSet messageRS=null;
        try {

            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_ORDER_DETAIL_ID", orderDetailID);
            inputParams.put("IN_MESSAGE_TYPE", "Mercury");
            inputParams.put("IN_COMP_ORDER", "N");//exclude COMP order.
            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("GET_MESG_DETAIL_FROM_LAST_FTD");

           /* execute the store prodcedure */
            DataAccessUtil dau = DataAccessUtil.getInstance();
            messageRS = (CachedResultSet) dau.execute(request);
             if(messageRS.next())      // get queue config context parms
            {
                askTO=new ASKMessageTO();
                askTO.setMercuryId(messageRS.getString("ftd_message_id"));
     
                askTO.setFillingFlorist(messageRS.getString("filling_florist"));
                askTO.setSendingFlorist(messageRS.getString("sending_florist"));
            
            }
               
        
        }finally {
            //this.closeResultSet(messageRS);
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getMercuryCANMessage");
            } 
        }

        
        return askTO;
    }
    
    /**
     * This method returns a boolean value based on if the order is for a 
     * personalized product or not.
     */
    public boolean isPersonalizedOrder(String orderDetailID) throws 
            IOException, SAXException, SQLException, 
            ParserConfigurationException , Exception {
            
        if(logger.isDebugEnabled()) {
            logger.debug("Entering isPersonalizedOrder");
            logger.debug("orderDetailID : " + orderDetailID);
        }
        
        DataRequest request = new DataRequest();
        CachedResultSet rs = null;
        boolean isPersonalized = false;
        
        /* setup store procedure input parameters */
        HashMap inputParams = new HashMap();
        inputParams.put("IN_ORDER_DETAIL_ID", orderDetailID);
        
        /* build DataRequest object */
        request.setConnection(dbConnection);
        request.reset();
        request.setInputParams(inputParams);
        request.setStatementID("IS_PERSONALIZED_ORDER");

       /* execute the store prodcedure */
        DataAccessUtil dau = DataAccessUtil.getInstance();
        String isPersonalizedString = (String) dau.execute(request);

        if(isPersonalizedString != null && isPersonalizedString.equalsIgnoreCase("Y")) {
            isPersonalized = true;
        }
        else {
            isPersonalized = false;
        }

        if(logger.isDebugEnabled()) {
            logger.debug("isPersonalizedString : " + isPersonalizedString);
            logger.debug("isPersonalized : " + isPersonalized);
            logger.debug("Exiting isPersonalizedOrder");
        }
        
        return isPersonalized;
    }
    
    
    
    
    
    
    
    
    public void closeResultSet(ResultSet rs) throws SQLException
    {
      if(rs!=null)
            {
              try
              {
                 rs.close();
              }catch(Exception e)
              {
                logger.error(e);
              }
              
              if(rs.getStatement()!=null)
              {
                try
                {
                  rs.getStatement().close();
                }catch(Exception e)
                {
                   logger.error(e);
                }
              }
        }
            
    }
    
    public StockMessageVO getEmailType(String titleId, String originId) throws Exception
    {
      StockMessageVO msgObj = null;
      
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(this.dbConnection);
      dataRequest.setStatementID("GET_STOCK_EMAIL");
      dataRequest.addInputParam("IN_TITLE_ID", titleId);
      dataRequest.addInputParam("IN_ORIGIN_ID", originId);
      
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      
      CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
      
        
      if(rs.next())
      {
        msgObj = new StockMessageVO();
        msgObj.setMessageId(titleId);
        msgObj.setOriginId("");
        msgObj.setTitle(rs.getString("TITLE"));
        msgObj.setSubject(rs.getString("SUBJECT"));
        msgObj.setContent(rs.getClob("BODY").getSubString((long)1, (int)rs.getClob("BODY").length()));
      }
      
      return msgObj;    
    }


  public void setDbConnection(Connection dbConnection)
  {
    this.dbConnection = dbConnection;
  }


  public Connection getDbConnection()
  {
    return dbConnection;
  }


  /**
   * Sets the value of the delivery confirmation status
   *
   * @param orderDetailId
   * @param message type
   * @param comp order
   * @return CachedResultSet
   * @throws java.lang.Exception
   *
   */

  public void updateDeliveryConfirmationStatus(long orderDetailId, String newStatus, String updatedBy) throws Exception 
  {
    logger.debug("updateDeliveryConfirmationStatus");
    
    Map output = null;
    DataRequest dataRequest = new DataRequest();

    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_ORDER_DETAIL_ID", new Long(orderDetailId));
    inputParams.put("IN_STATUS", newStatus);
    inputParams.put("IN_CSR_ID", updatedBy);

    /* build DataRequest object */
    dataRequest.setConnection(this.dbConnection);
    dataRequest.setStatementID("UPDATE_DCON_STATUS");
    dataRequest.setInputParams(inputParams);

    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    output = (Map)dataAccessUtil.execute(dataRequest);

    /* read store prodcedure output parameters to determine
    * if the procedure executed successfully */
    String status = (String)output.get("OUT_STATUS");
    if (status != null && status.equalsIgnoreCase("N")) {
        String message = (String)output.get("OUT_MESSAGE");
        throw new Exception(message);
    }     
  }
  
  /**
   * gets the value of the delivery confirmation status
   *
   * @param orderDetailId
   * @return String with the delivery confimraiton status
   * @throws java.lang.Exception
   *
   */

  public String getDeliveryConfirmationStatus(long orderDetailId) throws Exception
  {
    logger.debug("getDeliveryConfirmationStatus for orderDetail " + orderDetailId);
    
    String dconStatus = null;
    DataRequest dataRequest = new DataRequest();

    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_ORDER_DETAIL_ID", orderDetailId);

    /* build DataRequest object */
    dataRequest.setConnection(this.dbConnection);
    dataRequest.setStatementID("GET_DCON_STATUS");
    dataRequest.setInputParams(inputParams);

    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    dconStatus = (String)dataAccessUtil.execute(dataRequest);
    
    return dconStatus;
  }
  
  /**
   * Retrieve all the configured key phrase list from the 
   * DB
   * 
   * @param msgType
   * @return
   * @throws Exception
   */
  public List<String> getKeyPhraseList(String msgType) throws Exception {
	  
	  List<String> oooList = null;
	  
	  DataRequest dataRequest = new DataRequest();
	  HashMap inputParams = new HashMap();
	  
	  inputParams.put("IN_MSG_TYPE", msgType);
	  
	  dataRequest.setConnection(this.dbConnection);
	  dataRequest.setStatementID("GET_KEY_PHRASE_LIST");
	  dataRequest.setInputParams(inputParams);
	  
	  DataAccessUtil dau = DataAccessUtil.getInstance();
	  CachedResultSet resultSet = (CachedResultSet) dau.execute(dataRequest);
	  
	  oooList = new ArrayList<String>();
	  
	  while (resultSet.next()) {
		  String ooo = resultSet.getString("KEY_PHRASE_TXT");
		  if(ooo != null && !ooo.isEmpty()) {
			  oooList.add(ooo);
		  }
	  }
	  
	  return oooList;
	  
	  
  }
}
