package com.ftd.queue.email.dao;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.ftd.osp.utilities.MessageUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.queue.email.constants.EmailConstants;
import com.ftd.queue.email.vo.ASKEventHandlerVO;

/**
 * This class retrieves and logs message transactions.
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 * @todo confirm stored procedure setup against actual stored procedures
 */
public class AskEventDAO 
{
    private Connection dbConnection;
    private Logger logger = new Logger("com.ftd.queue.email.dao.AskEventDAO");
    
    


    public AskEventDAO(Connection conn)
    {
        super();
        dbConnection = conn;
    }   

    /**
     * Call stored procedure to retrieve a list of order detail ids 
     * that have not received any response within 48 hours. 
     * @param n/a
     * @return List
     * @throws Exception
     */ 
    public Map getNoResponseMap(String queueType , String checkParm) throws 
        IOException, SAXException, ParserConfigurationException, 
         SQLException, Exception
    {
        List queueList = new ArrayList();
        Map queueMap=new HashMap();
        if(logger.isDebugEnabled()){
            logger.debug("Entering getQueueList ");
        }
        
        MessageUtil messageUtil = MessageUtil.getInstance();
        DataRequest request = new DataRequest();
        
        try
        {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_CHECK_PARM", checkParm);
            inputParams.put("IN_QUEUE_TYPE", queueType);
            // build DataRequest object
            request.reset();
            request.setConnection(dbConnection);
            request.setInputParams(inputParams);
            request.setStatementID(EmailConstants.GET_ASK_NO_RESPONSE);

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            HashMap results=(HashMap)dau.execute(request);
            CachedResultSet queueRS = (CachedResultSet) results.get("OUT_CURSOR");
            while(queueRS.next())     
            {
                ASKEventHandlerVO askVO = new ASKEventHandlerVO();
                askVO.setOrderDetailID(queueRS.getString("order_detail_id"));
                askVO.setFillingFloristId(queueRS.getString("filling_florist_id"));
                askVO.setMessageType(queueRS.getString("system_type"));
                askVO.setPointOfContactId(queueRS.getString("point_of_contact_id"));
                askVO.setSendEmail(queueRS.getString("sender_email_address"));
                askVO.setQueueType(queueRS.getString("queue_type"));
                askVO.setAskEventLogId(queueRS.getString("ask_message_event_log_id"));
                askVO.setMercuryStatusFlags(queueRS.getString("ask_ans_response_flags")); 
                askVO.setReceivedDate(queueRS.getTimestamp("message_timestamp"));
                queueList.add(askVO);
            }
            String priority=(String)results.get("OUT_QUEUE_PRIORITY");
            queueMap.put(priority, queueList);
        }finally
        {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getQueueList");
            }
        } 

        return queueMap;                
    }
    
    /**
     * Call stored procedure to retrieve a list of order detail ids, 
     * filling florist ids and messge types that have not received 
     * any responses within 24 hours.
     * @param n/a
     * @return List
     * @throws Exception
     */    
    public Map getMessageMap(String queueType) throws 
        IOException, SAXException, ParserConfigurationException, 
         SQLException, Exception
    {
        
        return this.getNoResponseMap(queueType, "FIRST_CHECK");
                   
    }
    
    
    
    
     public Map getQueueMap(String queueType) throws 
        IOException, SAXException, ParserConfigurationException, 
         SQLException, Exception
    {
        
        return this.getNoResponseMap(queueType, "SECOND_CHECK");
                   
    }
    
    
    
    
    
    
    private void populateASKEventHandlerVO(ASKEventHandlerVO askVO, CachedResultSet queueRS){
    
                
                askVO.setOrderDetailID(queueRS.getString("order_detail_id"));
                askVO.setFillingFloristId(queueRS.getString("filling_florist_id"));
                askVO.setMessageType(queueRS.getString("system_type"));
                askVO.setPointOfContactId(queueRS.getString("point_of_contact_id"));
                askVO.setSendEmail(queueRS.getString("sender_email_address"));
                askVO.setQueueType(queueRS.getString("queue_type"));
                askVO.setMercuryMessageId(queueRS.getString("mercury_id"));
    
    }
    
    

    /**
     * log a message transaction for an ASK message sent
     * @param n/a
     * @return n/a
     * @throws Exception
     */    
    public void storeMessageTran(ASKEventHandlerVO askVO) throws 
        IOException, SAXException, ParserConfigurationException, 
         SQLException, Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering storeMessageTran");
        }
        
        DataRequest request = new DataRequest();
        MessageUtil messageUtil = MessageUtil.getInstance();
        
        try {           
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(EmailConstants.LOG_ASK_EVENT_HANDLER_IN_ORDER_DETAIL_ID, 
                askVO.getOrderDetailID());
            inputParams.put(EmailConstants.LOG_ASK_EVENT_HANDLER_IN_FILLING_FLORIST_ID, 
                askVO.getFillingFloristId());
            inputParams.put(EmailConstants.LOG_ASK_EVENT_HANDLER_IN_MESSAGE_TIMESTAMP, 
                new Timestamp(System.currentTimeMillis()));
            inputParams.put(EmailConstants.LOG_ASK_EVENT_HANDLER_IN_MESSAGE_TYPE, 
                askVO.getMessageType());
            inputParams.put(EmailConstants.LOG_ASK_EVENT_HANDLER_IN_RESPONSE_FLAG, 
                EmailConstants.LOG_ASK_EVENT_RESPONSE_FLAG);
            inputParams.put("IN_SENDER_EMAIL_ADDRESS", 
                askVO.getSendEmail());
             inputParams.put("IN_POINT_OF_CONTACT_ID", 
                askVO.getPointOfContactId());
            inputParams.put("IN_QUEUE_TYPE", 
                askVO.getQueueType());
            inputParams.put("IN_MERCURY_ID", 
                askVO.getMercuryMessageId());
            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID(EmailConstants.LOG_ASK_EVENT_HANDLER_MESSAGE);

           /* execute the store prodcedure and retrieve output*/
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            /* read store prodcedure output parameters to determine 
             * if the procedure executed successfully */
                String status = (String) outputs.get(EmailConstants.LOG_ASK_EVENT_HANDLER_OUT_STATUS);
                if(status.equals("N"))
                {
                    String message = (String) outputs.get(EmailConstants.LOG_ASK_EVENT_HANDLER_OUT_MESSAGE);
                    String messageString = 
                        messageUtil.getMessageContent(
                        EmailConstants.UNABLE_STORE_ASK_MESSAGE);
                    throw new Exception(messageString + " " +  message);
                }
            
        }finally {
            //close connection
            if(logger.isDebugEnabled()){
               logger.debug("Exiting storeMessageTran");
            }
        }
        
    }
    
    
    
    
    
    /**
     * log a message transaction for an ASK message sent
     * @param n/a
     * @return n/a
     * @throws Exception
     */    
    public void updateASKEventLog(ASKEventHandlerVO askVO, int numberTimeOfCheck, String flag) throws 
        IOException, SAXException, ParserConfigurationException, 
         SQLException, Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering updateASKEventLog "+askVO + " numberTimeOfCheck = "+numberTimeOfCheck);
        }
        
        DataRequest request = new DataRequest();
        MessageUtil messageUtil = MessageUtil.getInstance();
        
        try {           
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_ASK_MESSAGE_EVENT_LOG_ID", 
                askVO.getAskEventLogId());
            inputParams.put(EmailConstants.LOG_ASK_EVENT_HANDLER_IN_RESPONSE_FLAG, 
               flag);
            if(numberTimeOfCheck==1){//24 hours
               logger.debug("process 24 hr");
              inputParams.put("IN_CHECKED_24HR_DATE", 
                  new Timestamp(System.currentTimeMillis()));
              inputParams.put("IN_CHECKED_48HR_DATE", null);
            }else //48 hours
            {
               logger.debug("process 48 hr");
               inputParams.put("IN_CHECKED_24HR_DATE", 
                  null);
               inputParams.put("IN_CHECKED_48HR_DATE", 
                  new Timestamp(System.currentTimeMillis()));
                  
                
            }
                       
            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("UPDATE_ASK_MESSAGE_EVENT_LOG");

           /* execute the store prodcedure and retrieve output*/
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            /* read store prodcedure output parameters to determine 
             * if the procedure executed successfully */
                String status = (String) outputs.get(EmailConstants.LOG_ASK_EVENT_HANDLER_OUT_STATUS);
                if(status.equals("N"))
                {
                    String message = (String) outputs.get(EmailConstants.LOG_ASK_EVENT_HANDLER_OUT_MESSAGE);
                    String messageString = 
                        messageUtil.getMessageContent(
                        EmailConstants.UNABLE_STORE_ASK_MESSAGE);
                    throw new Exception(messageString + " " +  message);
                }
            
        }finally {
            //close connection
            if(logger.isDebugEnabled()){
               logger.debug("Exiting updateASKEventLog "+askVO);
            }
        }
        
    }
    
    
}
