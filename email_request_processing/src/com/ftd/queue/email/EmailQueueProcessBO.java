package com.ftd.queue.email;

import java.io.IOException;
import java.sql.Connection;
import java.text.ParseException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.queue.email.constants.EmailConstants;
import com.ftd.queue.email.dao.EmailQueueDAO;
import com.ftd.queue.email.vo.EmailRequestVO;

/**
 * This class will:
 * 	Pass email request XML to XMLProcessor to create Email 
 *      Request Value Object, 
 * 	Ask EmailQueueHandlerFactory to create appropriate EmailQueueHandler 
 *      subclass to process Email Request Value Object.
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 */
public class EmailQueueProcessBO 
{
    private Logger logger = 
        new Logger("com.ftd.queue.email.EmailQueueProcessBO");
    private EmailConstants emailConstants;
    private Connection dbConnection;

    public EmailQueueProcessBO(Connection conn)
    {
        super();
        dbConnection = conn;
        
    }
    
    /**
     * Pass email request XML to XMLProcessor to create
     * Email Request Value Object 
     * @param String - emailRequestXMLString
     * @return EmailRequestVO
     * @throws Exception
     */ 
    private EmailRequestVO createEmailRequestXML(String emailRequestXMLSeq, EmailQueueDAO emailDAO) 
        throws SAXException ,IOException,ParseException,
        ParserConfigurationException, Exception
    {

        if(logger.isDebugEnabled()){
            logger.debug("Entering createEmailRequestXML "+emailRequestXMLSeq);
            //logger.debug("emailRequestXMLString : " + emailRequestXMLString);
        }
        
        EmailRequestVO emailRequest = new EmailRequestVO();
        try
        {
            String emailRequestXMLString=emailDAO.getEmailRequestXML(emailRequestXMLSeq);
            /* create Email Request object */
            XMLProcessor xmlProc = new XMLProcessor();
            emailRequest = xmlProc.createEmailRequestVO(emailRequestXMLString);
        
        }finally
        {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting createEmailRequestXML");
            }
        }        
        
        return emailRequest;
    }

    /**
     * Call EmailQueueHandlerFactory to instantiate the appropriate subclass
     * of EmailQueueHandler based on the type of 
     * EmailRequestVO (CX, MO, NO, QI) 
     * @param EmailRequestVO
     * @return EmailQueueHandler
     * @throws Exception
     */ 
    private EmailQueueHandler getEmailQueueHandler 
    (EmailRequestVO emailRequestVO)
        throws TransformerException,IOException,SAXException,
            ParserConfigurationException,ClassNotFoundException,
            InstantiationException,IllegalAccessException, Exception{

        if(logger.isDebugEnabled()){
            logger.debug("Entering getEmailQueueHandler");
            logger.debug("Create Handler : " + emailRequestVO.getRequestType());
        }
        EmailQueueHandler queueHandler;
        try{
            /* instantiate queue handler factory */
            EmailQueueHandlerFactory queueFactory 
                = EmailQueueHandlerFactory.getInstance();
            /* retrieve queue handler */
             queueHandler = 
                queueFactory.getEmailQueueHandler(emailRequestVO);
        
        
        }finally
        {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getEmailQueueHandler");
            }
        } 
        return queueHandler;
    }

    /**
     * This method will pass email request XML to XMLProcessor to create Email 
     * Request Value Object and ask EmailQueueHandlerFactory to create 
     * appropriate EmailQueueHandler subclass to process 
     * Email Request Value Object.
     * @param emailRequestXMLString - XML E-mail Request
     * @return n/a
     * @throws Exception
     */ 
    public void execute (String emailRequestXMLSeq) 
        throws TransformerException,IOException,SAXException,
            ParserConfigurationException,ClassNotFoundException,
            InstantiationException,IllegalAccessException,ParseException,
            Exception
            {
        if(logger.isDebugEnabled()){
            logger.debug("Entering execute: "+emailRequestXMLSeq);
            //logger.debug("emailRequestXMLString : " + emailRequestXMLString);
        }
        try
        {
            EmailQueueDAO emailDAO = new EmailQueueDAO(dbConnection);
            EmailRequestVO emailRequest= createEmailRequestXML(emailRequestXMLSeq, emailDAO);
            if(emailRequest!=null)
            {   
                /* Retrieve Queue Handler */
                EmailQueueHandler queueHandler= 
                    getEmailQueueHandler(emailRequest);
                if(queueHandler!= null)
                {
                    
                    
                     /* Process Queue Request */
                     queueHandler.process(emailRequest, emailDAO);                    
                }
   
            }
        }finally
        {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting execute");
            }
        }  
    }
}
