package com.ftd.queue.email.util;

import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author Anshu Gaind
 * @version $Id: XMLErrorHandler.java,v 1.2 2011/06/30 15:04:13 gsergeycvs Exp $
 */

class XMLErrorHandler extends DefaultHandler {
    private boolean valid = true;
    private SAXParseException parseException;

    //Receive notification of a recoverable error.
    public void error(SAXParseException se) {
        setError(false, se);
    }

    //Receive notification of a non-recoverable error.
    public void fatalError(SAXParseException se) {
        setError(false, se);
    }

    //Receive notification of a warning.
    public void warning(SAXParseException se) {
        setError(false, se);
    }


    public void setValid(boolean valid) {
        this.valid = valid;
    }

    private void setError(boolean valid, SAXParseException spe) {
        setValid(valid);
        setParseException(spe);
    }

    public boolean isValid() {
        return valid;
    }


    public void setParseException(SAXParseException parseException) {
        this.parseException = parseException;
    }


    public SAXParseException getParseException() {
        return parseException;
    }
}