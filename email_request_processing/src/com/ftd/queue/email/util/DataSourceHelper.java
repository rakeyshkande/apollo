package com.ftd.queue.email.util;
import java.io.IOException;
import java.sql.Connection;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.queue.email.constants.EmailConstants;

public class DataSourceHelper 
{
  public DataSourceHelper()
  {
  }
  
  /**
     * Create a connection to the database
     * @param n/a
     * @return Connection
     * @throws Exception
     */
    public static Connection createDatabaseConnection() throws IOException, 
        SAXException, ParserConfigurationException, TransformerException, 
         Exception
    {
       
        
        
            /* create a connection to the database */
           return DataSourceUtil.getInstance().
                getConnection(ConfigurationUtil.getInstance().getProperty(
                EmailConstants.PROPERTY_FILE,EmailConstants.DATASOURCE_NAME));
                  
                
        
    }
}
