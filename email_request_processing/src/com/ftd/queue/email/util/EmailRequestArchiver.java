package com.ftd.queue.email.util;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.ftd.osp.utilities.MessageUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.queue.email.constants.EmailConstants;
import com.ftd.queue.email.dao.EmailQueueDAO;


/**
 * This class takes the original XML received and calls the dao that stores 
 * it in the Queue Database.
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 */
public class EmailRequestArchiver 
{
    private Logger logger = 
        new Logger("com.ftd.queue.email.util.EmailRequestArchiver");
    private Connection dbConnection;
    private EmailConstants emailConstants;

    
    public EmailRequestArchiver(Connection conn)
    {
        super();
        dbConnection = conn;
    }
    
    /**
     * The method calls the DAO that archives the original 
     * XML from Novator to the Queue database
     * @param emailRequestXMLString - XML request
     * @return n/a
     * @throws Exception
     */    
    public boolean archive(String emailRequestXMLString, String seqNumber) 
        throws IOException, SAXException, ParserConfigurationException, 
         SQLException, Exception
        {
        if(logger.isDebugEnabled()){
            logger.debug("Entering archive");
            logger.debug("XML Request seqNumber: " + seqNumber);
        }
        boolean duplicate = false;
        MessageUtil messageUtil = MessageUtil.getInstance();
        try
        {
            /* create and call EmailQueueDAO */
            EmailQueueDAO emailDAO = new EmailQueueDAO(dbConnection);
            duplicate = emailDAO.archive(emailRequestXMLString,seqNumber);
           
        }finally
        {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting archive");
            }
        }
        
        return duplicate;
    }
    
}
