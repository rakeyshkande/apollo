package com.ftd.queue.email;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.messagegenerator.StockMessageGenerator;
import com.ftd.messagegenerator.dao.MessageGeneratorDAO;
import com.ftd.messagegenerator.vo.PointOfContactVO;
import com.ftd.messagegenerator.vo.StockMessageVO;
import com.ftd.op.common.to.ResultTO;
import com.ftd.op.mercury.to.ASKMessageTO;
import com.ftd.op.mercury.to.BaseMercuryMessageTO;
import com.ftd.op.mercury.to.CANMessageTO;
import com.ftd.op.venus.to.CancelOrderTO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.PaymentMethodHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.PartnerVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.queue.email.cache.handler.EmailQueueConfigHandler;
import com.ftd.queue.email.cache.handler.EmailQueuePriorityCacheHandler;
import com.ftd.queue.email.constants.EmailConstants;
import com.ftd.queue.email.dao.AskEventDAO;
import com.ftd.queue.email.dao.EmailQueueDAO;
import com.ftd.queue.email.dao.PaymentDAO;
import com.ftd.queue.email.util.MessagingServiceLocator;
import com.ftd.queue.email.vo.ASKEventHandlerVO;
import com.ftd.queue.email.vo.AutoEmailVO;
import com.ftd.queue.email.vo.CompanyEmailVO;
import com.ftd.queue.email.vo.EmailRequestVO;
import com.ftd.queue.email.vo.InquiryDetailVO;
import com.ftd.queue.email.vo.OrderDetailVO;
import com.ftd.queue.email.vo.PaymentVO;
import com.ftd.queue.email.vo.QueueVO;


/**
 * This class loads, sends mail for a set of auto email value objects and
 * records the time, type and recipient of auto response to database.
 *
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 */
public abstract class EmailQueueHandler implements IEmailQueueHandler {
    private Logger logger = new Logger("com.ftd.queue.email.EmailQueueHandler");

    public EmailQueueHandler() {
        super();
    }

    public boolean continueProcess(EmailRequestVO emailVO,
            EmailQueueDAO emailDAO) throws IOException, SAXException,
            SQLException, ParserConfigurationException , Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering checkExistingQueue");
            logger.debug("emailVO : " + emailVO);
        }

        boolean continueProcess = true;

        try {
            this.locateOrderDetailVOForRequest(emailVO, emailDAO);
            /* store e-mail request as point of contact */
            String pointOfContact = this.storePointOfContact(emailVO, emailDAO);
            emailVO.setPointOfContactId(pointOfContact);
            logger.debug("pointOfContact " + pointOfContact);
            if (isOrderFound(emailVO)) 
            {
                OrderDetailVO ordDetail = emailVO.getOrderDetailVO();
               
                
                
                
                    continueProcess = lookupUntaggedExistingQueue(emailVO, emailDAO);
                    if (continueProcess) {
                       
                        if (ordDetail.isOrderInScrub()) {
    
                            logger.debug(" Order is in Scrub, Pending or Removed.");
                            //NON-Delivery email requests need to send different auto response based on the holiday time.
                            //it will be handled in the NonDelvieryEmailQueueHandler.
                            if (!emailVO.getRequestType().equalsIgnoreCase("ND")) {
                                queueRequestUpdateCommentsSendAutoResponse(emailVO,
                                        emailDAO, ordDetail);
                                continueProcess = false;
                            }
                        }
                        
                        
                        else {
    
                            if (ordDetail.isFtpVendor()) {
                                logger.debug("FTP Vendor");
                                queueRequestUpdateCommentsSendAutoResponse(emailVO,
                                        emailDAO, ordDetail);
                                continueProcess = false;
                            } else {
                                boolean orderLocked = this.isOrderLocked(ordDetail,
                                        emailDAO);
                                if (orderLocked) {
                                    logger.debug("order is locked");
                                    queueRequestUpdateCommentsSendAutoResponse(
                                            emailVO, emailDAO, ordDetail);
                                    continueProcess = false;
                                }
                            }
                        }
                    }
                

            }
            else
            {
                logger.debug("order not found");
                this.storeUnattachedEmailQueue(emailVO, emailDAO);
                String messageKey=this.getMessageKey(emailVO.getRequestType());
                this.sendAutoResponse(messageKey, emailVO, emailDAO, true);
                continueProcess = false;

            }
        } finally {
            logger.debug("continueProcess " + continueProcess);
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting checkExistingQueue");
            }
        }
        return continueProcess;
    }

    /**
     * @param emailVO
     * @param emailDAO
     * @param ordDetail
     * @throws ParserConfigurationException
     * @throws XSLException
     * @throws SQLException
     * @throws Exception
     * @throws IOException
     * @throws SAXException
     * @throws TransformerException
     * void
     */
    protected void queueRequestUpdateCommentsSendAutoResponse(
            EmailRequestVO emailVO, EmailQueueDAO emailDAO,
            OrderDetailVO ordDetail) throws ParserConfigurationException,
             SQLException, Exception, IOException, SAXException,
            TransformerException {

        storeAttachedEmailQueue(emailVO, emailDAO);
        String requestType = emailVO.getRequestType();
        String commentKey = requestType.toLowerCase() + ".queue";
        String comments = retrieveOrderComments(commentKey);
        this.insertComments(comments, ordDetail, emailDAO);
        String messageKey = this.getMessageKey(requestType);
        this.sendAutoResponse(messageKey, emailVO, emailDAO, true);
    }

    private String getMessageKey(String requestType) {

        if (requestType.equalsIgnoreCase("QI")
                || requestType.equalsIgnoreCase("BD")
                || requestType.equalsIgnoreCase("PD")) {
            return "op.queue";
        } else if (requestType.equalsIgnoreCase("SE")) {
            return "qc.co.queue";
        } else {
            return requestType.toLowerCase() + ".queue";
        }

    }

    protected boolean isOrderLocked(OrderDetailVO ordDetail,
            EmailQueueDAO emailDAO) throws IOException, SAXException,
            SQLException, ParserConfigurationException , Exception {
        String sessionId = "" + this.hashCode() + new Date().getTime();
        String csrId = this.getSystemOperator();
        return emailDAO.isOrderLocked(ordDetail, sessionId, csrId);
    }

    /**
     * Check if new e-mail queue already exists and if it is a higher priority
     * 
     * @param orderExternalNumber -
     *            String
     * @param emailVO -
     *            CheckExistingQueue
     * @return boolean - true will indicate to continue processing the e-mail
     * @throws Exception
     * @todo - change process based on tech spec
     */
    boolean lookupUntaggedExistingQueue(EmailRequestVO emailVO,
            EmailQueueDAO emailDAO) throws SQLException,
            ParserConfigurationException, SAXException, Exception, IOException,
            TransformerException  {
        boolean continueProcess = true;
        logger.debug("lookupUntaggedExistingQueue");
        QueueVO highestQueue = this.lookupQueueVO(emailVO.getOrderDetailVO()
                .getOrderDetailID(), emailDAO);
        if (highestQueue != null && highestQueue.getPriority() != 0) {
            int emailPriority = retrievePriority(emailVO.getRequestType());
            int highPriority = highestQueue.getPriority();
            logger.debug("emailPriority=" + emailPriority + "\t highPriority="
                    + highPriority);
            //lower the priority number, higher the priority.
            if (emailPriority < highPriority) {
                if (!highestQueue.getTagged().equalsIgnoreCase("Y")) {
                    logger.debug("removeQueue=" + highestQueue.getMessageID());
                    this.removeQueue(highestQueue, emailDAO);
                }

            } else {
                sendAutoResponse("op.queue", emailVO, emailDAO, true);//don't need to queue.
                String comments = retrieveOrderComments("lower.priority");
                if(!this.isAutoResponseNeeded(emailVO.getOrderDetailVO()))
                {
                  comments=retrieveOrderComments("lower.priority.noauto.response");
                }
                this.insertComments(comments, emailVO.getOrderDetailVO(),
                        emailDAO);
                continueProcess = false;
            }
        } else {
            logger.debug("no highest queue found");

        }

        return continueProcess;

    }

    /**
     * This method: 1. creates new instance of QueueVO, 2.tries to find order
     * for the inbound request base on the order external number and email, 3.
     * populates QueueVO with data retrieved from Queue Database.
     * 
     * @param orderExternalNumber -
     *            String
     * @param email -
     *            String
     * @param requestType -
     *            String
     * @return QueueVO
     * @throws Exception
     */
    public QueueVO loadQueueVO(String orderExternalNumber, String email,
            String requestType, EmailQueueDAO emailDAO) throws IOException,
            SAXException, SQLException, ParserConfigurationException,
             Exception {

        if (logger.isDebugEnabled()) {
            logger.debug("Entering loadQueueVO");
            logger.debug("orderExternalNumber : " + orderExternalNumber);
            logger.debug("email : " + email);
            logger.debug("requestType : " + requestType);
        }

        try {

            return emailDAO.createEmailQueueVO(orderExternalNumber, email,
                    requestType);

        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting loadQueueVO");
            }
        }

    }

    /**
     * look up the order detail for the order
     * 
     * @param orderNumber -
     *            String
     * @param email -
     *            String
     * @return OrderDetailVO
     * @throws Exception
     */
    public void locateOrderDetailVOForRequest(EmailRequestVO emailVO,
            EmailQueueDAO emailDAO) throws IOException, SAXException,
            SQLException, ParserConfigurationException , Exception {

        if (logger.isDebugEnabled()) {
            logger.debug("Entering loadOrderDetailVO");
            logger.debug("emailVO : " + emailVO);
        }

        // OrderDetailVO = new OrderDetailVO();
        try {

            OrderDetailVO ordDetail = emailVO.getOrderDetailVO();
            /* find the order detail */

            ordDetail = emailDAO.locateOrderDetail(emailVO.getOrderNumber(),
                    emailVO.getEmail(), emailVO.getBillingLastName(), emailVO
                            .getBillingFirstName());

            emailVO.setOrderDetailVO(ordDetail);
            if (this.isOrderFound(emailVO)) {
                //set information associated with order detail vo.
                emailDAO.setOrderDetailVO(ordDetail.getOrderDetailID(),
                        ordDetail);

                /* retrieve company ID and order date */
                emailDAO.setOrderDetailCompanyIdAndDateByGUID(ordDetail);
            }
        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting loadOrderDetailVO");
            }
        }

    }

    /**
     * Determine whether or not an order can be found.
     * 
     * @param n/a
     * @return boolean
     * @throws Exception
     */
    public boolean isOrderFound(EmailRequestVO emailVO) throws Exception {
        logger.debug("entering isOrderFound ");
        return (emailVO.getOrderDetailVO() != null && StringUtils
                .isNotEmpty(emailVO.getOrderDetailVO().getOrderDetailID()));
    }

    /**
     * Determine whether or not the date falls during a holiday period
     * 
     * @param n/a
     * @return boolean
     * @throws Exception
     */
    public boolean isHolidayTime(Timestamp requestDate) throws ParseException,
            Exception {

        if (logger.isDebugEnabled()) {
            logger.debug("Entering isHolidayTime");
            logger.debug("requestDate : " + requestDate);
        }
        
        boolean holidayTime = false;
        if(requestDate!=null){
            try {
                EmailQueueConfigHandler emailQueueConfig = (EmailQueueConfigHandler) CacheManager
                        .getInstance().getHandler(
                                EmailConstants.EMAIL_QUEUE_CONFIG_HANDLER);
    
               
    
                java.util.Date startHoliday = emailQueueConfig.getStartHoliday();
                Date endHoliday = emailQueueConfig.getEndHoliday();
                long requestDateMS = requestDate.getTime();
                long startHolidayMS = startHoliday.getTime();
                long endHolidayMS = endHoliday.getTime();
                if(requestDateMS >= startHolidayMS && requestDateMS <= endHolidayMS) {
                    holidayTime = true;
                }
            } finally {
                if (logger.isDebugEnabled()) {
                    logger.debug("Exiting isHolidayTime");
                }
            }
        }
        return holidayTime;
    }

    /**
     * Determine whether or not the validation-able parts, for instance address,
     * city, state and product, have been changed.
     * 
     * @param n/a
     * @return n/a
     * @throws Exception
     * @todo receive verification on how this will work
     */
    public boolean isOrderValidatablePartChanged(EmailRequestVO emailRequest)
            throws Exception {
        boolean orderChanged = false;
        InquiryDetailVO inquiry = emailRequest.getInquiryVO();
        OrderDetailVO ordDetail = emailRequest.getOrderDetailVO();
        if (inquiry != null) {
            if (StringUtils.isNotBlank(inquiry.getInstitution())) {
                if (StringUtils.isNotBlank(ordDetail.getInstitutionName())) {
                    if (!ordDetail.getInstitutionName().equals(
                            inquiry.getInstitution())) {
                        orderChanged = true;
                    }
                } else {
                    orderChanged = true;
                }
            }
            
            if (StringUtils.isNotBlank(inquiry.getRecipientFirstName())) {
                if (!ordDetail.getReceipientFirstName().equals(
                        inquiry.getRecipientFirstName())) {
                    orderChanged = true;
                }
            } 
            
            if (StringUtils.isNotBlank(inquiry.getRecipientLastName())) {
                if (!ordDetail.getReceipientLastName().equals(
                        inquiry.getRecipientLastName())) {
                    orderChanged = true;
                }
            } 
            
            if (StringUtils.isNotBlank(inquiry.getCardMessage())) {
                if(StringUtils.isNotBlank(ordDetail.getCardMessage())){
                  if (!ordDetail.getCardMessage()
                          .equals(inquiry.getCardMessage())) {
                      orderChanged = true;
                  }
                }else
                {
                  orderChanged = true;
                }
            } 
            
            if (StringUtils.isNotBlank(inquiry.getCardSignature())) {
                if(StringUtils.isNotBlank(ordDetail.getCardSignature())){
                  if (!ordDetail.getCardSignature().equals(
                          inquiry.getCardSignature())) {
                      orderChanged = true;
                  }
                }else
                {
                  orderChanged = true;
                }
            }
        }

        return orderChanged;
    }

    /**
     * Compile a list of order attributes that were changed
     * 
     * @param n/a
     * @return n/a
     * @throws Exception
     * @todo receive verification on how this will work
     */
    public String orderPartChanged(EmailRequestVO emailRequest)
            throws Exception {
        String orderChanged = "";
        InquiryDetailVO inquiry = emailRequest.getInquiryVO();
        OrderDetailVO orderDetail = emailRequest.getOrderDetailVO();
        if (inquiry != null && orderDetail != null
                && !orderDetail.isOrderInScrub()) {
            if (StringUtils.isNotEmpty(inquiry.getInstitution())) {
                String institution = orderDetail.getInstitutionName();

                if (StringUtils.isEmpty(institution)
                        || !institution.equals(inquiry.getInstitution())) {
                    orderChanged = orderChanged + "\r\n Institution: "
                            + inquiry.getInstitution();
                }

            }

            if (StringUtils.isNotEmpty(inquiry.getRecipientFirstName())) {
                String firstName = orderDetail.getReceipientFirstName();
                if (StringUtils.isEmpty(firstName)
                        || !firstName.equals(inquiry.getRecipientFirstName())) {
                    orderChanged = orderChanged + " \r\n First Name: "
                            + inquiry.getRecipientFirstName();

                }
            }

            if (StringUtils.isNotEmpty(inquiry.getRecipientLastName())) {
                String lastName = orderDetail.getReceipientLastName();
                if (StringUtils.isEmpty(lastName)
                        || !lastName.equals(inquiry.getRecipientLastName())) {

                    orderChanged = orderChanged + " \r\n Last Name: "
                            + inquiry.getRecipientLastName();

                }
            }

            if (StringUtils.isNotEmpty(inquiry.getCardMessage())) {
                String cardMsg = orderDetail.getCardMessage();
                if (StringUtils.isEmpty(cardMsg)
                        || !cardMsg.equals(inquiry.getCardMessage())) {
                    orderChanged = orderChanged + " \r\n Card Message: "
                            + inquiry.getCardMessage();

                }
            }

            if (StringUtils.isNotEmpty(inquiry.getCardSignature())) {
                String cardSignature = orderDetail.getCardSignature();
                if (StringUtils.isEmpty(cardSignature)
                        || !cardSignature.equals(inquiry.getCardSignature())) {
                    orderChanged = orderChanged + " \r\n Card Signature: "
                            + inquiry.getCardSignature();

                }
            }

        }

        return orderChanged.trim();
    }

    /**
     * Checks for fields that cannot be automatically updated have been changed
     * 
     * @param n/a
     * @return n/a
     * @throws Exception
     * @todo receive verification on how this will work
     */
    public boolean isOrderChangedForQueue(EmailRequestVO emailRequest)
            throws Exception {
        boolean orderChanged = false;
        InquiryDetailVO inquiry = emailRequest.getInquiryVO();
        OrderDetailVO ordDetail = emailRequest.getOrderDetailVO();
        if (inquiry != null) {
            if (StringUtils.isNotBlank(inquiry.getRecipientAddress())) {
                if (StringUtils.isBlank(ordDetail.getReceipientAddress())||!ordDetail.getReceipientAddress().equals(
                        inquiry.getRecipientAddress())) {
                    orderChanged = true;
                }
            }
            if (StringUtils.isNotBlank(inquiry.getRecipientCity())) {
                if (StringUtils.isBlank(ordDetail.getReceipientCity())|| !ordDetail.getReceipientCity().equals(
                        inquiry.getRecipientCity())) {
                    orderChanged = true;
                }
            } 
            if (StringUtils.isNotBlank(inquiry.getRecipientState())) {
                if (StringUtils.isBlank(ordDetail.getReceipientState())||!ordDetail.getReceipientState().equals(
                        inquiry.getRecipientState())) {
                    orderChanged = true;
                }
            } 
            if (StringUtils.isNotBlank(inquiry.getRecipientZip())) {
                if (StringUtils.isBlank(ordDetail.getReceipientZip())||!ordDetail.getReceipientZip().equals(
                        inquiry.getRecipientZip())) {
                    orderChanged = true;
                }
            }
            if (StringUtils.isNotBlank(inquiry.getRecipientCountry())) {
                if (StringUtils.isBlank(ordDetail.getReceipientCountry())||!ordDetail.getReceipientCountry().equals(
                        inquiry.getRecipientCountry())) {
                    orderChanged = true;
                }
            } 
            
            if (StringUtils.isNotBlank(inquiry.getRecipientPhone())) {
                if (StringUtils.isBlank(ordDetail.getReceipientPhone())|| !ordDetail.getReceipientPhone().equals(
                        inquiry.getRecipientPhone())) {
                    orderChanged = true;
                }
            }
            
            if (inquiry.getNewDeliveryDateOne() != null) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String orderDelDate = sdf.format(ordDetail.getDeliveryDate());
                String newDelDate = sdf.format(inquiry.getNewDeliveryDateOne());
                if (!orderDelDate.equals(newDelDate)) {
                    orderChanged = true;
                }

            } 
            
            if (StringUtils.isNotBlank(inquiry.getProductSku())) {
                if (!ordDetail.getProductSku().equals(inquiry.getProductSku())) {
                    orderChanged = true;
                }
            }
        }
        return orderChanged;
    }

    /**
     * Send out auto response.
     * 
     * @param n/a
     * @return n/a
     * @throws Exception
     */
    public void sendAutoResponse(String messageKey, EmailRequestVO emailVO,
            EmailQueueDAO emailDAO, boolean queued) throws Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering sendAutoResponse");
            logger.debug("messageKey: " + messageKey);
        }

        try {

            OrderDetailVO ordDetail = emailVO.getOrderDetailVO();

            if (this.isAutoResponseNeeded(ordDetail)) {
                logger.debug("isAutoResponseNeeded");
                sendEmail(messageKey, emailVO, emailDAO);
            } else if (!queued) {
                this.storeEmailQueue(emailVO, emailDAO);
            }

        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting sendAutoResponse");
            }
        }
    }

    protected boolean isAutoResponseNeeded(OrderDetailVO ordDetail) {
        return (ordDetail != null && !ordDetail.isAmazonOrder()
                && !ordDetail.isWalmartOrder());
    }

    /**
     * Look up Queue Database for queues with the same order external number.
     * 
     * @param orderExternalNumber -
     *            String
     * @return QueueVO
     * @throws Exception
     */
    private void sendEmail(String messageKey, EmailRequestVO emailRequest,
            EmailQueueDAO emailDAO) throws Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering sendEmail");
            logger.debug("messageKey: " + messageKey);
        }

        try {
            String requestType = emailRequest.getRequestType();
            OrderDetailVO ordDetail = emailRequest.getOrderDetailVO();
            PointOfContactVO pocVO = new PointOfContactVO();
            AutoEmailVO autoEmail = new AutoEmailVO();
            PartnerVO partnerVO = null;
            String defaultSender = null;

            String orderDetailId = (ordDetail != null ? ordDetail
                    .getOrderDetailID() : "");
            String customerId = (ordDetail) != null ? ordDetail.getCustomerID()
                    : "";
            autoEmail.setCompanyID(emailRequest.getCompanyID());

            /* Determine if the order detail source code is 
             * associated with a preferred partner.
             */ 
            if (ordDetail != null && StringUtils.isNotEmpty(ordDetail.getSourceCode())) {
                partnerVO = FTDCommonUtils.getPreferredPartnerBySource(ordDetail.getSourceCode());      
            }
                       
            /* If the order detail source code is not associated with a 
             * preferred partner determine if the email request source code 
             * is associated with a preferred partner.
             */ 
            if (partnerVO == null && StringUtils.isNotEmpty(emailRequest.getSourceCode())) {
                partnerVO = FTDCommonUtils.getPreferredPartnerBySource(emailRequest.getSourceCode());      
            }

            /* Append the preferred partner name to the messageKey/title if a partner 
             * name exists so the associated preferred partner email is sent.
             */ 
            if (partnerVO != null && StringUtils.isNotEmpty(partnerVO.getPartnerName())) {
                messageKey = messageKey + "." + partnerVO.getPartnerName(); 

                if (logger.isDebugEnabled()) {
                    logger.debug("partnerName: " + partnerVO.getPartnerName());
                    logger.debug("updated messageKey: " + messageKey);
                }
            }

            // external order number instead?
            autoEmail.setOrderDetailID(orderDetailId);
            if (this.isOrderFound(emailRequest)) {
                if (ordDetail.getDeliveryDate() != null) {
                    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
                    autoEmail.setDeliveryDate(sdf.format(ordDetail
                            .getDeliveryDate()));
                }
                autoEmail.setOrderNumber(ordDetail.getExternalOrderNumber());
                autoEmail
                        .setTrackingNumber((ordDetail.getTrackingNumber()) != null ? ordDetail
                                .getTrackingNumber()
                                : "");
            }else
            {
              autoEmail.setOrderNumber(emailRequest.getOrderNumber());
            }
            /* retrieve remaining attributes */
            CompanyEmailVO compEmailVO = lookupCompanyEmailInformation(
                    orderDetailId, emailRequest.getCompanyID(), emailDAO);
            autoEmail.setShipper(compEmailVO.getShipper());
            autoEmail.setCompanyName(compEmailVO.getCompanyName());
            logger.debug("company name 1 =" + compEmailVO.getCompanyID());
            logger.debug("company name 2 =" + autoEmail.getCompanyName());
            autoEmail.setTrackingURL(compEmailVO.getTrackingURL());
            autoEmail.setPhoneNumber(compEmailVO.getPhoneNumber());

            autoEmail
                    .setFirstName((emailRequest.getBillingFirstName()) != null ? emailRequest
                            .getBillingFirstName()
                            : "");
            autoEmail
                    .setLastName((emailRequest.getBillingLastName()) != null ? emailRequest
                            .getBillingLastName()
                            : "");

            /* set order part changed if the request is MO - Modify Order */
            if (requestType != null && requestType.equalsIgnoreCase("MO")) {
                autoEmail.setOrderChange(orderPartChanged(emailRequest));
            }

            /* Figure out the source code.  If it is on the order details, use it.
             * otherwise use the default source code from the partner
             */
            if (ordDetail != null && StringUtils.isNotEmpty(ordDetail.getSourceCode()))
            {
                autoEmail.setSourceCode(ordDetail.getSourceCode());
            }
            else
            {
                autoEmail.setSourceCode(emailRequest.getSourceCode());
            }
            
            if (ordDetail != null) {
            	autoEmail.setLanguageId(ordDetail.getLanguageId());
            }

            /* create e-mail XML */
            Document dataDocXML = (Document) DOMUtil
                    .getDocument(autoEmail.toXML());
                    
            //Obtain order payments
            List payments = new PaymentDAO(emailDAO.getDbConnection()).getPaymentInfo(emailRequest.getOrderDetailVO().getOrderDetailID());
            ArrayList paymentMethods = new ArrayList();
            for(Iterator it = payments.iterator(); it.hasNext();)
            {
              paymentMethods.add(((PaymentVO)it.next()).getPaymentType());
            }
            
            //Set payment method text
            Element pmElement = dataDocXML.createElement("payment_method_text");
            String paymentMethodTxt = ((PaymentMethodHandler)CacheManager.getInstance().getHandler("CACHE_NAME_PAYMENT_METHOD")).getText(paymentMethods, null);
            pmElement.appendChild(dataDocXML.createTextNode(paymentMethodTxt));
            Node rootNode = DOMUtil.selectSingleNode(dataDocXML, "autoemail");
            rootNode.appendChild(pmElement);

            pocVO.setDataDocument(dataDocXML);
            if(StringUtils.isNotBlank(compEmailVO.getCompanyID())){
               pocVO.setCompanyId(compEmailVO.getCompanyID());
            }else{
              pocVO.setCompanyId(EmailConstants.ORDER_SOURCE_FTD);
            }
            pocVO.setLetterTitle(messageKey);
            pocVO.setPointOfContactType(EmailConstants.EMAIL_MESSAGE_TYPE);
            pocVO.setSentReceivedIndicator("O");

            pocVO.setExternalOrderNumber(emailRequest.getOrderDetailVO()
                    .getExternalOrderNumber());
            pocVO.setMasterOrderNumber(emailRequest.getOrderDetailVO()
                    .getMasterOrderNumber());
            pocVO.setOrderGuid(emailRequest.getOrderDetailVO().getOrderGuid());
            pocVO.setFirstName(emailRequest.getBillingFirstName());
            pocVO.setLastName(emailRequest.getBillingLastName());
            pocVO.setDaytimePhoneNumber(emailRequest.getDayPhone());
            pocVO.setUpdatedBy("SYS");

            if (StringUtils.isNotEmpty(customerId)) {
                pocVO.setCustomerId(new Long(customerId).longValue());
            }

            // reset order detail VO

            if (StringUtils.isNotEmpty(orderDetailId)) {
                pocVO.setOrderDetailId(new Long(orderDetailId).longValue());
            }

            pocVO
                    .setRecipientEmailAddress((emailRequest.getEmail()) != null ? emailRequest
                            .getEmail()
                            : "");
            pocVO.setCommentType(EmailConstants.EMAIL_MESSAGE_TYPE);
            MessageGeneratorDAO msgGenDAO = new MessageGeneratorDAO(emailDAO
                    .getDbConnection());

            /* Use the preferred partner reply email address as the sender 
             * email address if it exists.    
             */ 
            if(partnerVO != null){
                defaultSender = msgGenDAO.loadSenderEmailAddress(pocVO.getLetterTitle(), emailRequest.getCompanyID(), pocVO.getOrderDetailId(), partnerVO.getPartnerName());
            }else{
                defaultSender = msgGenDAO.loadSenderEmailAddress(pocVO.getLetterTitle(), emailRequest.getCompanyID(), pocVO.getOrderDetailId());
            }
            pocVO.setSenderEmailAddress(defaultSender);
            
            // populating email type for ET to find the template
            pocVO.setEmailType(EmailConstants.EMAIL_TYPE_AUTO_REPLY);
            
            StockMessageGenerator msgGenerator = new StockMessageGenerator();            
            
            //Populate RecordAttributesXML attribute based on orderguid
            String orderGuid = ordDetail.getOrderGuid();            
            if(StringUtils.isNotEmpty(orderGuid)) {
            	String recordAttributesXML = msgGenerator.generateRecordAttributeXMLFromOrderGuid(orderGuid);
            	pocVO.setRecordAttributesXML(recordAttributesXML);
            } else {            	
            	if (logger.isDebugEnabled())
                {
                    logger.debug("Not populating RecordAttributesXML as there is no orderguid information");
                }
            }
            String sourceCode = ordDetail.getSourceCode();
            if(StringUtils.isNotEmpty(sourceCode)) {
            	pocVO.setSourceCode(sourceCode);
            } else {
            	pocVO.setSourceCode(emailRequest.getSourceCode());
            }            
            
            msgGenerator.processMessage(pocVO);
        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting sendEmail");
            }
        }
    }
    
    
    /**
     * look up the order detail for the order
     * 
     * @param orderNumber -
     *            String
     * @param email -
     *            String
     * @return OrderDetailVO
     * @throws Exception
     */
    public CompanyEmailVO lookupCompanyEmailInformation(String orderDetailID, String companyId,
            EmailQueueDAO emailDAO) throws IOException, SAXException,
            SQLException, ParserConfigurationException , Exception {

        if (logger.isDebugEnabled()) {
            logger.debug("Entering lookupCompanyEmailInformation");
            logger.debug("orderDetailID : " + orderDetailID);
            logger.debug("companyId : " + companyId);
        }

        CompanyEmailVO compEmailVO = new CompanyEmailVO();
        try {

            compEmailVO = emailDAO.getCompanyEmailInformation(orderDetailID, companyId);
        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting lookupCompanyEmailInformation");
            }
        }
        return compEmailVO;
    }

    /**
     * Look up Queue Database for queues with the same order external number.
     * 
     * @param orderExternalNumber -
     *            String
     * @return QueueVO
     * @throws Exception
     */
    public QueueVO lookupQueueVO(String orderDetailId, EmailQueueDAO emailDAO)
            throws IOException, SAXException, SQLException,
            ParserConfigurationException , Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering lookupQueueVO");
            logger.debug("orderExternalNumber: " + orderDetailId);
        }

        QueueVO lookupQueue = new QueueVO();
        try {

            lookupQueue = emailDAO
                    .lookupHighestPriorityExistingQueue(orderDetailId);

        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting lookupQueueVO");
            }
        }
        return lookupQueue;
    }

    /**
     * Call EmailQueueDAO to store Email Queue Value Object in Queue Database.
     * 
     * @param emailQueueVO -
     *            QueueVO
     * @return n/a
     * @throws Exception
     */
    public void storeQueueVO(QueueVO emailQueueVO, EmailQueueDAO emailDAO)
            throws IOException, SAXException, SQLException,
            ParserConfigurationException , Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering storeQueueVO");
        }

        try {

            emailDAO.store(emailQueueVO);

        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting storeQueueVO");
            }
        }

    }

    /**
     * Call EmailQueueDAO to store Email Queue Value Object in Queue Database.
     * 
     * @param emailQueueVO -
     *            QueueVO
     * @return n/a
     * @throws Exception
     */
    public int retrievePriority(String queueType) throws IOException,
            SAXException, SQLException, ParserConfigurationException,
             Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering retrievePriority");
            logger.debug("Queue Type: " + queueType);
        }

        int requestPriority = 0;
        try {
            EmailQueuePriorityCacheHandler emailQueuePriorityHandler = (EmailQueuePriorityCacheHandler) CacheManager
                        .getInstance().getHandler("EmailQueuePriorityHandler");
            requestPriority = emailQueuePriorityHandler.getQueuePriority(queueType);

        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting retrievePriority");
            }
        }

        return requestPriority;
    }

    /**
     * Call EmailQueueDAO to store Email Queue Value Object in Queue Database.
     * 
     * @param emailQueueVO -
     *            QueueVO
     * @return n/a
     * @throws Exception
     */
    public String storePointOfContact(EmailRequestVO emailVO,
            EmailQueueDAO emailDAO) throws IOException, SAXException,
            SQLException, ParserConfigurationException , Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering storePointOfContact");
        }

        String pointOfContactID = null;
        try {

            pointOfContactID = emailDAO.storeEmailRequest(emailVO);

        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting storePointOfContact");
            }
        }

        return pointOfContactID;
    }

    /**
     * Calls the email queue DAO to remove a queue record from the database
     * 
     * @param emailQueueVO -
     *            QueueVO
     * @return n/a
     * @throws IOException,
     *             SAXException, SQLException, ParserConfigurationException,
     *              Exception
     */
    public void removeQueue(QueueVO emailQueueVO, EmailQueueDAO emailDAO)
            throws IOException, SAXException, SQLException,
            ParserConfigurationException , Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering removeQueue");
        }

        try {

            /* remove the queue from the database */
            emailDAO.removeQueue(emailQueueVO.getMessageID());

        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting removeQueue");
            }
        }
    }

    /**
     * Insert Comments for a queue order
     * 
     * @param comments -
     *            String
     * @param emailQueueVO -
     *            QueueVO
     * @return n/a
     * @throws Exception
     */
    public void insertComments(String comments, OrderDetailVO emailOrderVO,
            EmailQueueDAO emailDAO) throws IOException, SAXException,
            SQLException, ParserConfigurationException ,
            TransformerException, Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering insertComments");
            logger.debug("comments: " + comments);
            logger.debug("emailOrderVO: " + emailOrderVO);
        }

        try {
            /* retrieve the csr_id */
            String csr_id = ConfigurationUtil.getInstance().getProperty(
                    EmailConstants.ORDER_INFORMATION_CONFIG_FILE,
                    EmailConstants.ORDER_INFORMATION_CSR_ID);

            emailDAO.insertComments(comments, emailOrderVO, csr_id);

        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting insertComments");
            }
        }
    }

    public int convertStringToInt(String convert) throws Exception,
            NumberFormatException {

        int i;

        i = Integer.parseInt(convert.trim());

        return i;
    }

    /**
     * lock an order
     * 
     * @param orderDetail -
     *            OrderDetailVO
     * @return n/a
     * @throws Exception
     */
    public boolean lockOrder(String sessionId, EmailQueueDAO emailDAO,
            OrderDetailVO ordDetail) throws IOException, SAXException,
            SQLException, ParserConfigurationException , Exception {

        if (logger.isDebugEnabled()) {
            logger.debug("Entering lockOrder");
            logger.debug("hashCode : " + sessionId);
        }

        boolean orderLocked = false;
        try {
            /* lookup csr_id */
            String csr_id = ConfigurationUtil.getInstance().getProperty(
                    EmailConstants.ORDER_INFORMATION_CONFIG_FILE,
                    EmailConstants.ORDER_INFORMATION_CSR_ID);

            orderLocked = emailDAO.lockOrder(ordDetail, sessionId, csr_id);

        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting lockOrder");
            }
        }
        return orderLocked;
    }

    public String retrieveOrderComments(String commentID) throws IOException,
            TransformerException, SAXException, ParserConfigurationException,
             Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering retrieveOrderComments");
            logger.debug("commentID : " + commentID);
        }
        String comment = "";
        try {
            comment = ConfigurationUtil.getInstance().getProperty(
                    EmailConstants.ORDER_INFORMATION_CONFIG_FILE, commentID);

        } finally {
            if (logger.isDebugEnabled()) { 
                logger.debug("Exiting retrieveOrderComments");
            }
        }
        return comment;

    }

    /**
     * release an order
     * 
     * @param orderDetail -
     *            OrderDetailVO
     * @return n/a
     * @throws Exception
     */
    public void releaseOrder(String sessionId, EmailQueueDAO emailDAO,
            OrderDetailVO ordDetail) throws IOException, SAXException,
            SQLException, ParserConfigurationException , Exception {

        if (logger.isDebugEnabled()) {
            logger.debug("Entering releaseOrder");
            logger.debug("hashCode : " + sessionId);
        }

        try {
            /* lookup csr_id */
            String csr_id = ConfigurationUtil.getInstance().getProperty(
                    EmailConstants.ORDER_INFORMATION_CONFIG_FILE,
                    EmailConstants.ORDER_INFORMATION_CSR_ID);

            emailDAO.releaseOrder(ordDetail, sessionId, csr_id);
        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting releaseOrder");
            }
        }
    }

    /**
     * modify the order detail of an order
     * 
     * @param ordDetail -
     *            OrderDetailVO
     * @return n/a
     * @throws IOException,
     *             SAXException, SQLException, ParserConfigurationException,
     *              Exception
     */
    public void modifyOrderDetail(EmailRequestVO emailRequest,
            EmailQueueDAO emailDAO) throws IOException, SAXException,
            SQLException, ParserConfigurationException , Exception {
        OrderDetailVO ordDetail = emailRequest.getOrderDetailVO();
        if (logger.isDebugEnabled()) {
            logger.debug("Entering modifyOrderDetail");
            logger.debug("Order Detail ID " + ordDetail.getOrderDetailID());
        }

        try {

            /* remove the queue from the database */
            emailDAO.modifyOrder(ordDetail.getOrderDetailID(), emailRequest
                    .getInquiryVO());

        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting modifyOrderDetail");
            }
        }
    }

    public void logMessageTransaction(ASKEventHandlerVO askEvent,
            EmailQueueDAO emailDAO) throws IOException, SAXException,
            ParserConfigurationException , SQLException, Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering logMessageTransaction");
        }

        try {

            AskEventDAO askEventDAO = new AskEventDAO(emailDAO
                    .getDbConnection());
            askEventDAO.storeMessageTran(askEvent);
        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting logMessageTransaction");
            }
        }
    }

    /**
     * Retrieve message related data, such as type, message detail type,
     * messageOrderNumber and orderDetailId from HTTP request and create an ASK
     * Message TO. In addition, insert any information retrieved from the
     * request into the parameters HashMap
     * 
     * @param request -
     *            HttpServletRequest
     * @param paramters -
     *            HashMap
     * @return ASKMessageTO
     * @throws TransformerException
     * @throws IOException
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws XSLException
     */
    public ASKMessageTO createASKMessageTO(String emailType,
            EmailQueueDAO emailDAO, EmailRequestVO emailRequest)
            throws TransformerException, IOException, SAXException,
            ParserConfigurationException , SQLException, Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering createASKMessageTO");
        }

        ASKMessageTO askTO = null;
        OrderDetailVO ordDetail = emailRequest.getOrderDetailVO();
        try {

            askTO = emailDAO.getMercuryASKMessage(ordDetail.getOrderDetailID());
            if (askTO != null) {
                if (emailType.equals(EmailConstants.EMAIL_TYPE_MODIFY_ORDER)) {
                    askTO.setComments(EmailConstants.MERCURY_MSG_COMMENTS_MO
                            + "\t\n " + this.orderPartChanged(emailRequest));
                } else if (emailType
                        .equals(EmailConstants.EMAIL_TYPE_DELIVERY_INQUIRY)) {
                    askTO.setComments(EmailConstants.MERCURY_MSG_COMMENTS_DI);
                }

                askTO.setOperator(this.getSystemOperator());
                askTO.setMercuryStatus(BaseMercuryMessageTO.MERCURY_OPEN);
                askTO.setDirection(BaseMercuryMessageTO.OUTBOUND);
            }
        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting createASKMessageTO");
            }
        }

        return askTO;
    }

    public boolean sendVenusCANMessage(String reasonCode,
            EmailQueueDAO emailDAO, EmailRequestVO emailRequest)
            throws Exception {
        ResultTO result = new ResultTO();

        CancelOrderTO cancelTO = emailDAO.getVenusCANMessage(emailRequest
                .getOrderDetailVO().getOrderDetailID());

        if (cancelTO != null) {

            cancelTO.setComments(EmailConstants.MERCURY_MSG_COMMENTS_CAN);
            cancelTO.setCsr(this.getSystemOperator());
            cancelTO.setCancelReasonCode(reasonCode);
            result = MessagingServiceLocator.getInstance().getVenusAPI()
                    .sendCancel(cancelTO, emailDAO.getDbConnection());
            logger.debug("Venus Cancel transmistCancel ="+cancelTO.isTransmitCancel());
            if (!result.isSuccess()) {
                logger.error("Can't send Venus CAN Message. Error: "
                        + result.getErrorString());
            }
        }

        return result.isSuccess();
    }

    public String getSystemOperator() throws IOException, TransformerException,
            SAXException, ParserConfigurationException  {

        return ConfigurationUtil.getInstance().getProperty(
                EmailConstants.ORDER_INFORMATION_CONFIG_FILE,
                EmailConstants.ORDER_INFORMATION_CSR_ID);
    }

    /**
     * Retrieve message related data, such as type, message detail type,
     * messageOrderNumber and orderDetailId from HTTP request and create an CAN
     * Message TO. In addition, insert any information retrieved from the
     * request into the parameters HashMap
     * 
     * @return CANMessageTO
     * @throws TransformerException
     * @throws IOException
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws XSLException
     */
    public CANMessageTO createCANMessageTO(EmailQueueDAO emailDAO,
            EmailRequestVO emailRequest) throws TransformerException,
            IOException, SAXException, ParserConfigurationException,
             SQLException, Exception {
        CANMessageTO canTO = new CANMessageTO();

        if (logger.isDebugEnabled()) {
            logger.debug("Entering createCANMessageTO");
        }

        try {

            canTO = emailDAO.getMercuryCANMessage(emailRequest
                    .getOrderDetailVO().getOrderDetailID());

            canTO.setOperator(this.getSystemOperator());
            canTO.setComments(EmailConstants.MERCURY_MSG_COMMENTS_CAN);
            canTO.setMercuryStatus(BaseMercuryMessageTO.MERCURY_OPEN);

            canTO.setDirection(BaseMercuryMessageTO.OUTBOUND);

        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting createCANMessageTO");
            }
        }

        return canTO;
    }

    /**
     * Send CAN messages via Mercury Message API.
     * 
     * @param request -
     *            HttpServletRequest
     * @param connection            
     * @return String
     * @throws Exception????
     * @todo - code and determine exceptions
     */
    public boolean sendMercuryCancelMessage(CANMessageTO cancelMsg, Connection connection)
            throws TransformerException, IOException, SAXException,
            ParserConfigurationException , ParseException,
            Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering sendMercuryCancelMessage");
        }

        try {

            ResultTO result = MessagingServiceLocator.getInstance()
                    .getMercuryAPI().sendCANMessage(cancelMsg, connection);
            return result.isSuccess();

        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting sendMercuryCancelMessage");
            }
        }

    }

    /**
     * Send ASK messages via Mercury Message API.
     * 
     * @param request -
     *            HttpServletRequest
     * @param connection           
     * @return n/a
     * @throws Exception????
     */
    public ResultTO sendMercuryASKMessage(ASKMessageTO askMessage, Connection connection)
            throws TransformerException, IOException, SAXException,
            ParserConfigurationException , Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering sendMercuryASKMessage");
        }

        try {

            return MessagingServiceLocator.getInstance().getMercuryAPI()
                    .sendASKMessage(askMessage, connection);

        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting sendMercuryASKMessage");
            }
        }

    }

    protected void storeEmailQueue(EmailRequestVO emailVO,
            EmailQueueDAO emailDAO) throws ParserConfigurationException,
             SQLException, Exception, IOException, SAXException {
        if (this.isOrderFound(emailVO)) {
            logger.debug("order is found " + emailVO);
            this.storeAttachedEmailQueue(emailVO, emailDAO);
        } else {
            logger.debug("order is not found " + emailVO);
            this.storeUnattachedEmailQueue(emailVO, emailDAO);
        }
    }

    protected void storeUnattachedEmailQueue(EmailRequestVO emailVO,
            EmailQueueDAO emailDAO) throws ParserConfigurationException,
             SQLException, Exception, IOException, SAXException {
        QueueVO newQueue = new QueueVO();
        logger
                .debug("no order associated with e-mail request put order in untagged unattached queue: "
                        + emailVO.getRequestType());
        OrderDetailVO unattachedOrdDetail = new OrderDetailVO();
        unattachedOrdDetail.setMasterOrderNumber(null);
        unattachedOrdDetail.setOrderGuid(null);
        unattachedOrdDetail.setOrderDetailID(null);
        unattachedOrdDetail.setExternalOrderNumber(emailVO.getOrderNumber());
        newQueue.setTagged(EmailConstants.ORDER_UNTAGGED);
        newQueue.setQueueType(emailVO.getRequestType());
        newQueue.setAttached(EmailConstants.ORDER_UNATTACHED);
        newQueue.setEmail(emailVO.getEmail());
        newQueue.setOrderDetailVO(unattachedOrdDetail);
        newQueue.setMercuryNumber(null);
        newQueue.setMessageType(emailVO.getRequestType());
        newQueue.setPointOfContactID(emailVO.getPointOfContactId());
        newQueue.setSystem(null);
        newQueue.setTimeStamp(emailVO.getTimeStamp());
        this.storeQueueVO(newQueue, emailDAO);
    }

    protected void storeAttachedEmailQueue(EmailRequestVO emailVO,
            EmailQueueDAO emailDAO) throws ParserConfigurationException,
             SQLException, Exception, IOException, SAXException {
        logger
                .debug("order attached with e-mail request put order in untagged attached queue: "
                        + emailVO.getRequestType());
        QueueVO newQueue = new QueueVO();
        newQueue.setTagged(EmailConstants.ORDER_UNTAGGED);
        newQueue.setQueueType(emailVO.getRequestType());
        OrderDetailVO ordDetail = emailVO.getOrderDetailVO();
        ordDetail.setPersonalized(emailDAO.isPersonalizedOrder(ordDetail.getOrderDetailID()));        

        if(ordDetail.isPersonalized()) {
            newQueue.setSystem(null);
        } else if (ordDetail.isOrderDropship()) {
            newQueue.setSystem(EmailConstants.SYSTEM_TYPE_VENUS);
        } else {
            newQueue.setSystem(EmailConstants.SYSTEM_TYPE_MERCURY);
        }
        newQueue.setEmail(emailVO.getEmail());
        newQueue.setOrderDetailVO(ordDetail);
        newQueue.setMercuryNumber(null);
        newQueue.setMessageType(emailVO.getRequestType());
        newQueue.setPointOfContactID(emailVO.getPointOfContactId());
        newQueue.setTimeStamp(emailVO.getTimeStamp());
        this.storeQueueVO(newQueue, emailDAO);
    }
    

}
