package com.ftd.queue.email;
import com.ftd.queue.email.dao.EmailQueueDAO;
import com.ftd.queue.email.vo.EmailRequestVO;

public interface IEmailQueueHandler 
{
    public void process(EmailRequestVO emailVO, EmailQueueDAO emailDAO) throws Exception;
}