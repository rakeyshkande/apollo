package com.ftd.queue.email;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;

import javax.jms.JMSException;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.HttpServletResponseUtil;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.queue.email.cache.handler.EmailQueueConfigHandler;
import com.ftd.queue.email.constants.EmailConstants;
import com.ftd.queue.email.util.DataSourceHelper;
import com.ftd.queue.email.util.EmailRequestArchiver;

/**
 * This class is the main entry point of all XML Transmissions received 
 * from Novator. An Email Request will call this Servlet with an XML 
 * Transmission with a parameter named �Email�. 
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 */
public class EmailRequestGatherer extends HttpServlet 
{
    private static final String CONTENT_TYPE = 
        "text/html; charset=windows-1252";
    
    private Logger logger = 
        new Logger("com.ftd.queue.email.EmailRequestGatherer");
    private Logger duplicateSeqLogger = 
        new Logger("sequence.check.duplicates");
    
    
    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) 
        throws ServletException, IOException
    {
        PrintWriter out = response.getWriter();
        out.println("Email Request Gatherer");
        out.close();
    }

    /**
     * This method receives the XML Transmission, calls methods from 
     * supporting classes to create an email request value object by 
     * the data received from XML transmission. Then it archives the 
     * XML Transmission to the database, and passes it as a test 
     * message to JMS.
     * @param request - HttpServletRequest object
     * @param response - HttpServletResponse object
     * @return n/a
     * @throws ServletException, IOException
     * @todo need response URL
     * @todo put in logging for invalid XML
     * @todo put in sequence number check
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) 
        throws ServletException, IOException
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering Servlet EmailRequestGatherer");
        }
        String xmlRequest=null;
        Connection conn=null;
        int maxAllowed=20000;//default to 20000.
        try{
            /* create database connection */
            conn = DataSourceHelper.createDatabaseConnection();
            if(conn==null)
            {
              //System.out.println("connection is null");
            }
            /*retrieve xml from request */
            xmlRequest = request.getParameter(
            		EmailConstants.REQUEST_PARAMETER_EMAIL);
            logger.debug("XML Requst: "+xmlRequest);
            /* check the size of the e-mail against the maximum size allowed */
            Object emailQueue = (Object) 
                CacheManager.getInstance().getHandler(EmailConstants.EMAIL_QUEUE_CONFIG_HANDLER);
       
            EmailQueueConfigHandler emailQueueConfig =  (EmailQueueConfigHandler) emailQueue;
            String smax=emailQueueConfig.getMaxEmailSize();
            if(smax!=null&&smax.trim().length()>0){
                maxAllowed = Integer.parseInt(emailQueueConfig.getMaxEmailSize());
            }
            if(xmlRequest.length() <= maxAllowed)
            {
                /* create Email Request object */
                XMLProcessor xmlProc = new XMLProcessor();
                xmlProc.validateXML(xmlRequest);
    
                String sequenceNumber = xmlProc.retrieveSequenceNumber(xmlRequest);
                /* archive message */
                boolean duplicate = archiveEmailRequest(xmlRequest,sequenceNumber, conn);
                
                if(duplicate ==false){
                    /* pass message to JMS */
                    dispatchEmailRequest(sequenceNumber);
                    /* send success response */
                    HttpServletResponseUtil.sendError(response, EmailConstants.RESPONSE_SUCCESS, 
                        EmailConstants.RESPONSE_SUCCESS_MSG);
                }
                else
                {
                    /* log duplicate sequence number and corresponding XML */
                    duplicateSeqLogger.warn(
                        "The request received from Novator has a duplicate " + 
                        "sequence number of: " + sequenceNumber);
                    duplicateSeqLogger.warn("XML for Duplicate Sequence ID: " + 
                        sequenceNumber + " :" + xmlRequest);   
                    
                    /*notify Novator of duplicate sequence id */
                    HttpServletResponseUtil.sendError(response, EmailConstants.RESPONSE_SUCCESS, 
                        EmailConstants.RESPONSE_DUPLICATE_MSG);
                    
                }
            }
            else
            {
                throw new Throwable(EmailConstants.EMAIL_SIZE_INVALID + maxAllowed);
            }

            
        }catch(SAXException e){
            processError(response, xmlRequest, e, conn);
        }catch(IOException e){
        	processError(response, xmlRequest, e, conn);
        }catch(ParseException e){
        	processError(response, xmlRequest, e, conn);
        }catch(ParserConfigurationException e){
        	processError(response, xmlRequest, e, conn);
        }catch(SQLException e){
        	processError(response, xmlRequest, e, conn);
        }catch(NamingException e){
        	processError(response, xmlRequest, e, conn);
        }catch(JMSException e){
        	processError(response, xmlRequest, e, conn);
        }catch(Throwable e){
        	processError(response, xmlRequest, e, conn);
      }finally{
            try
            {
                /* close the database connection */
                if(conn!=null){
                    conn.close();
                } 
            }catch(Exception e){
                throw new ServletException(e);
            }
            
            if(logger.isDebugEnabled()){
                logger.debug("Exiting Servlet EmailRequestGatherer");
            }
        }
    }

    /**
	 * @param response
	 * @param xmlRequest
	 * @param e
	 * @throws IOException
	 */
	private void processError(HttpServletResponse response, String xmlRequest, Throwable e, Connection conn) throws IOException {
		
    logger.error(e);
    logError(e,xmlRequest, conn);
		
		/* send error response*/
        try
        {
            HttpServletResponseUtil.sendError(response, EmailConstants.RESPONSE_ERROR, 
                EmailConstants.RESPONSE_ERROR_MSG);
        } catch (Exception ex)
        {
            logger.error("Failed to send Error: " + ex.getMessage(), ex);
            throw new IOException("Failed to send Error: " + ex.getMessage());
        }
	}

	/**
     * Archive the original XML to the Queue database
     * @param xmlRequest - E-mail XML request
     * @return n/a
     * @throws Exception
     */
    public boolean archiveEmailRequest(String xmlRequest,String seqNumber, Connection conn) 
     throws IOException, SAXException, ParserConfigurationException, 
         SQLException, Exception  
    {    
        if(logger.isDebugEnabled()){
            logger.debug("Entering archiveEmailRequest");
            //logger.debug("XML Request : " + xmlRequest);
            logger.debug("Sequence Number : " + seqNumber);
        }
        
        
        boolean duplicate = false;
        try{   
            
                EmailRequestArchiver emailArchive = 
                    new EmailRequestArchiver(conn);
                duplicate = emailArchive.archive(xmlRequest,seqNumber);
            
            
        }finally
        {

            if(logger.isDebugEnabled()){
                logger.debug("Exiting archiveEmailRequest");
            }
        }
        return duplicate;
    }
    
    /**
     * send message through jms
     * @param xmlRequest - E-mail XML request
     * @return n/a
     * @throws Exception
     * @todo: set status for message token.  need from DBA
     */
   public void dispatchEmailRequest(String sequenceNumber) 
    throws NamingException, JMSException, Exception {

        if(logger.isDebugEnabled()){
            logger.debug("Entering dispatchEmailRequest "+sequenceNumber);
           
        }

        try{
         //   Dispatcher dispatcher = Dispatcher.getInstance();
            MessageToken messageToken = new MessageToken();
            messageToken.setStatus(EmailConstants.JMS_EMAIL_STATUS);    
            messageToken.setMessage(sequenceNumber);   
            messageToken.setJMSCorrelationID(sequenceNumber);
            dispatchEmailRequest(new InitialContext(), messageToken);
        
        }finally
        {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting dispatchEmailRequest");
            }
        }
    }

    
   
    /**
     * send jms message using the dispatch utility
     * @param InitialContext
     * @param MessageToken
     * @return n/a
     * @throws JMSException
     * @throws Exception
     */
   private void dispatchEmailRequest(InitialContext context, MessageToken token) 
    throws JMSException, Exception
   {
        if(logger.isDebugEnabled()){
            logger.debug("Entering dispatchEmailRequest");
            logger.debug("STATUS : " + (String)token.getStatus());
            logger.debug("DISPATCHING MESSAGE FROM E-mail GATHERER " + 
                (String)token.getMessage());
        }
        
        try{
            Dispatcher dispatcher = Dispatcher.getInstance();

            /* enlist the JMS transaction, with the other 
             * application transaction 
             */
            dispatcher.dispatchTextMessage(context, token); 

        }finally{
            if(logger.isDebugEnabled()){
               logger.debug("Exiting dispatchOrder");
            }
        }
    }
    
    private void logError(Throwable logException,String xmlRequest, Connection conn)
    {

       
         try{

            SystemMessengerVO systemError = new SystemMessengerVO();
            systemError.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
             
            if(xmlRequest!=null){
                systemError.setMessage(logException.toString() +
                    "\nOriginal XML Request " + xmlRequest);
            }
            else
            {
                systemError.setMessage(logException.toString());    
            }
            systemError.setRead(true);
            systemError.setSource(EmailConstants.SYS_MESS_SOURCE);
            systemError.setType(EmailConstants.SYS_MESS_TYPE_ERROR);
            SystemMessenger systemMessenger = SystemMessenger.getInstance();
            systemMessenger.send(systemError,conn,false);
      
            }catch(Exception exception){
                logger.error(exception);
            }   
        
        
    }
}
