package com.ftd.queue.email;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;

import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import com.ftd.osp.utilities.MessageUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.queue.email.constants.EmailConstants;
import com.ftd.queue.email.util.DataSourceHelper;

/**
 * This class is a JMS message listener and process messages asynchronously.
 * It will retrieve message information and forward it to the Email Queue
 * Process Business Object
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 */
public class EmailRequestReceiverMDBBean implements MessageDrivenBean, MessageListener 
{
    private MessageDrivenContext context;
    private static Logger logger;
    
    
    public void ejbCreate()
    {
    }

    private void processMessage(Message msg) throws Exception
    {
        
        if(logger.isDebugEnabled()){
            logger.debug("Entering processMessage");
            logger.debug("Message : " + msg.toString());
        }
        
        Connection conn=null;
        boolean success=false;
            try
            {
                /*Remove message token from message */
                MessageToken msgToken = getMessageToken(msg);
                /*Remove XML Request from message token */
                String xmlRequestSeq = getEmailRequestXMLSequenceNumber(msgToken);
                
                /* Call EmailQueueProcessBO to process message */
                conn = DataSourceHelper.createDatabaseConnection();
                EmailQueueProcessBO emailBO = new EmailQueueProcessBO(conn);
                
                emailBO.execute(xmlRequestSeq);
                success=true;
            }catch(ParserConfigurationException pceException){
                logger.error(pceException);
                logError(EmailConstants.UNABLE_PROCESS_MESSAGE,pceException, conn);
            }catch(SAXException saxException){
                logger.error(saxException);
                logError(EmailConstants.UNABLE_PROCESS_MESSAGE,saxException, conn);            
            }catch(SQLException sqlException){
                logger.error(sqlException);
                logError(EmailConstants.UNABLE_PROCESS_MESSAGE,sqlException, conn);
            }catch(TransformerException transException){
                logger.error(transException);
                logError(EmailConstants.UNABLE_PROCESS_MESSAGE,transException, conn);
            }catch(JMSException jmsException){
                logger.error(jmsException);
                logError(EmailConstants.UNABLE_PROCESS_MESSAGE,jmsException, conn);
            }catch(IOException ioException){
                logger.error(ioException);
                logError(EmailConstants.UNABLE_PROCESS_MESSAGE,ioException, conn);
            }catch(ClassNotFoundException cnfException){
                logger.error(cnfException);
                logError(EmailConstants.UNABLE_PROCESS_MESSAGE,cnfException, conn);
            }catch(InstantiationException instException){
                logger.error(instException);
                logError(EmailConstants.UNABLE_PROCESS_MESSAGE,instException, conn);
            }catch(IllegalAccessException iaException){
                logger.error(iaException);
                logError(EmailConstants.UNABLE_PROCESS_MESSAGE,iaException, conn);
            }catch(ParseException parseException){
                logger.error(parseException);
                logError(EmailConstants.UNABLE_PROCESS_MESSAGE,parseException, conn);
            }catch(Throwable e){
                logger.error(e);
                logError(EmailConstants.UNABLE_PROCESS_MESSAGE,e, conn);
                 
        }finally
        {
           
            if(conn!=null){
                conn.close();    
            }
        
            if(logger.isDebugEnabled()){
            logger.debug("Exiting onMessageof E-mail Request Receiver" + 
                     " Message Driven Bean");
            }
        }       
    }
   /**
     * Process JMS messages, extract Email Request XML from inbound messages, 
     * calls EmailQueueProcessorBO to process email requests.
     * @param msg - JMS Message
     * @return n/a
     * @throws n/a
     */
    public void onMessage(Message msg)
    {
        logger = 
        new Logger("com.ftd.queue.email.EmailRequestReceiverMDBBean");
        if(logger.isDebugEnabled()){
            logger.debug("Entering onMessage of E-mail Request Receiver" + 
                         " Message Driven Bean");
            logger.debug("Message : " + msg.toString());
        }
        
        try
        {
            /* process JMS Message */
            processMessage(msg);
            
        }catch(Exception e){
             logger.error(e);
        }finally
        {
           if(logger.isDebugEnabled()){
            logger.debug("Exiting onMessageof E-mail Request Receiver" + 
                     " Message Driven Bean");
            }
        }    

    }

    public void ejbRemove()
    {
    }

    public void setMessageDrivenContext(MessageDrivenContext ctx)
    {
        this.context = ctx;
    }

   /**
     * Extract the message token from the JMS message.
     * @param msg - JMS Message
     * @return com.ftd.osp.utilities.vo.MessageToken
     * @throws JMSException, Exception
     * @todo find purpose of this method
     */
    private MessageToken getMessageToken(Message msg) 
        throws JMSException, Exception
        {
        if(logger.isDebugEnabled()){
            logger.debug("Entering getMessageToken");
            logger.debug("Message : " + msg.toString());
        }
        
        MessageToken token = new MessageToken();
       
        try{
            TextMessage textMessage = (TextMessage)msg;
            String xmlRequest = textMessage.getText();
            token.setMessage(xmlRequest);
            
        
        }finally
        {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getMessageToken");
            }
        }
        
        return token;
    }

   /**
     * Extract the email request XML from the message token.
     * @param token - com.ftd.osp.utilities.vo.MessageToken
     * @return String - XML E-mail Request
     * @throws JMSException, Exception
     */
    private String getEmailRequestXMLSequenceNumber(MessageToken token) 
        throws Exception
        {

        if(logger.isDebugEnabled()){
            logger.debug("Entering getEmailRequestXMLSequenceNumber");
            logger.debug("MessageToken : " + token.toString());
        }
        
        String xmlRequestSeq=null;
        try
        {
            xmlRequestSeq = token.getMessage().toString();

        }finally
        {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getEmailRequestXML");
            }
        }
        
        return xmlRequestSeq;
        
    }
    
    
    private void logError(String message,Throwable excep, Connection con) throws IOException,
        SAXException,ParserConfigurationException,Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering logError");
        }
        MessageUtil messageUtil;
        SystemMessengerVO systemError = new SystemMessengerVO();
        systemError.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
        try
        {
            messageUtil = MessageUtil.getInstance();  
                String messageString=messageUtil.getMessageContent(message);
                systemError.setMessage(messageString+"\t Error: "+excep.toString());  
                systemError.setRead(true);
                systemError.setSource(EmailConstants.SYS_MESS_SOURCE);
                systemError.setType(EmailConstants.SYS_MESS_TYPE_ERROR);
                SystemMessenger systemMessenger = SystemMessenger.getInstance();
                systemMessenger.send(systemError,con,false);
            logger.error(messageString, excep);
        }finally
        {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting logError");
            }
        }    
    }
    
    private void closeConnection(Connection conn) throws SQLException, Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering closeConnection");
        }

        try
        {
            if(conn!=null){
                conn.close();
            }

        }finally
        {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting closeConnection");
            }
        }    
    }
}
