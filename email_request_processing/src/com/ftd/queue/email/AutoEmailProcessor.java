package com.ftd.queue.email;

import org.w3c.dom.Document;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.queue.email.constants.EmailConstants;
import com.ftd.queue.email.vo.QueueVO;

/**
 * This class loads, sends mail for a set of auto email value 
 * objects and records the time, type and recipient of 
 * auto response to database.
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 */
public class AutoEmailProcessor 
{
    private Logger logger = new Logger("com.ftd.queue.email.AutoEmailProcessor");
  /**
   * Singleton instance of the object.
   */
  private static AutoEmailProcessor INSTANCE;
  
  /**
   * Flag to keep track of if the singleton has been initialized
   */
  private static boolean SINGLETON_INITIALIZED=false;
  /**
   * Timestamp of the config file when read last
   */
  private long lastModified;
    
    private EmailConstants emailConstants;
    public AutoEmailProcessor()
    {
    }

    /**
     * Return the AutoEmailProcessor instance.  It ensures that at a 
     * given time there is only a single instance.
     * @param n/a
     * @return AutoEmailProcessor
     * @throws Exception
     */ 
	public static AutoEmailProcessor getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new AutoEmailProcessor();
		}
		return INSTANCE;
	}

  
    /**
     * create the XML document that will be sent in the auto response e-mail
     * @param QueueVO
     * @return org.w3c.dom.Document
     * @throws Exception
     * @todo find out XML format for e-mail content and write code
     */ 
     public Document createEmailContentXML(QueueVO queueVO){
        if(logger.isDebugEnabled()){
            logger.debug("Entering createEmailContentXML");
            logger.debug("queueVO : " + queueVO);
        }
        
        try
        {
            /*create XML Document */
            
        }finally
        {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting createEmailContentXML");
            } 
        }
        return null;
    }

    /**
     * Send an auto message out using information from the QueueVO
     * @param messageKey
     * @param QueueVO
     * @return n/a
     * @throws Exception
     * @todo find out how to use e-mail API and write code
     */ 
    public void sendAutoResponse(String messageKey, QueueVO queueVO)
        {
        if(logger.isDebugEnabled()){
            logger.debug("Entering sendAutoResponse");
            logger.debug("messageKey : " + messageKey);
            logger.debug("queueVO : " + queueVO);
        }
            
        try {
            QueueVO queue = new QueueVO();
            createEmailContentXML(queue);
            
            /* call e-mail API */
            
            
        }finally
        {
          if(logger.isDebugEnabled()){
            logger.debug("Exiting sendAutoResponse");
            }  
        }

    }
}