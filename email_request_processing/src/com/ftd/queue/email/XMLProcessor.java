package com.ftd.queue.email;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.LanguageUtility;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.SpecialCharacterMasterHandler;
import com.ftd.osp.utilities.j2ee.ResourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.queue.email.constants.EmailConstants;
import com.ftd.queue.email.vo.EmailRequestVO;
import com.ftd.queue.email.vo.InquiryDetailVO;
import com.ftd.queue.email.vo.OrderDetailVO;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;

import java.net.URL;

import java.sql.Timestamp;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


import org.apache.commons.lang.BooleanUtils;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


/**
 * This class will parse the XML transmission and create an
 * Email Request Value Object.
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 */
public class XMLProcessor 
{
    private Logger logger = new Logger("com.ftd.queue.email.XMLProcessor");
    SpecialCharacterMasterHandler spcmHandler = (SpecialCharacterMasterHandler)CacheManager.getInstance().getHandler("CACHE_NAME_CHARACTER_MAPPING");    
    Map spcmap = new HashMap();    
    Pattern p = Pattern.compile("[^a-zA-z0-9!,'$%()^:{}&|;/*~=`@ .#_-]+");
    Matcher m;
    
    public XMLProcessor()
    {
        super();        
    }

    private Node findNode(Element element,String node)
    {
        Node value=null;
        try{
            if((DOMUtil.selectNodes(element,node)).item(0).getFirstChild()!=null){
                 value= (DOMUtil.selectNodes(element,node)).item(0).getFirstChild();
                }
    
        } catch(Exception e) {
           logger.warn("Exception thrown in findNode.  Will continue by returning a null value."); 
        }
        return value;
    }
    
    private String getNodeValue(Element element,String node) throws DOMException, XPathExpressionException
    {
        return (DOMUtil.selectNodes(element,node)).item(0).getFirstChild().getNodeValue();
    }
    
    public void validateXML(String novatorXML) throws Exception {

        if(logger.isDebugEnabled()){
           logger.debug("Entering validateXML. XSD = "+EmailConstants.EMAIL_VALIDATION_XSD);
        } 
        
        //Check to see if validation has been turned off
        boolean bValidateXML = true;
        try {
            String validateXML = ConfigurationUtil.getInstance().getProperty(EmailConstants.PROPERTY_FILE,EmailConstants.VALIDATE_EMAIL_XML);
            bValidateXML = BooleanUtils.toBoolean(validateXML);
            
            if( !bValidateXML && logger.isDebugEnabled() ) {
                logger.debug("Validation has been turned off");
            }
        } catch (Exception e) {
            logger.error("Exception while trying to determine the configuration value of "+EmailConstants.VALIDATE_EMAIL_XML+".  Defaulting to true");
            bValidateXML = true;
        }
        
        if( bValidateXML ) {
            OutputStream errorMsg=null;
            URL schema = this.getClass().getClassLoader().getResource(EmailConstants.EMAIL_VALIDATION_XSD);
            
            if(schema==null)
            {
              throw new Exception ("com.ftd.queue.email.XMLProcessor: "+EmailConstants.EMAIL_VALIDATION_XSD+" is not defined");
            }
            try {
                String requestXML = "";
                if(novatorXML!=null){
                    if(!novatorXML.equals(""))
                    {
                        requestXML = novatorXML.trim();
                    }    
                    else
                    {
                        throw new Exception("XML not received");
                    }
                }
                else
                {
                    throw new Exception("XML not received");
                }
                
                InputSource is = new InputSource(new ByteArrayInputStream(requestXML.getBytes("UTF-8")));
                StreamSource ss = new StreamSource(new StringReader(requestXML));
                Schema schemaDoc = DOMUtil.getSchema(new StreamSource(
        													ResourceUtil
        														.getInstance()
        														.getResourceAsStream(EmailConstants.EMAIL_VALIDATION_XSD)));
                
                InputStream xml = new java.io.ByteArrayInputStream(requestXML.getBytes("UTF-8"));
                //TODO: fix this to validate the xml
                Document doc = DOMUtil.getDocument(xml);
                throw new Exception("Don't forget to validate the XML");

                //XSDBuilder builder = new XSDBuilder();
                //XMLSchema schemadoc = (XMLSchema)builder.build(schema);
//                DOMParser parser = new DOMParser();
//                parser.setValidationMode(XMLParser.SCHEMA_VALIDATION);
//                parser.setXMLSchema(schemadoc);
//                parser.showWarnings(true);
//                parser.setErrorStream(System.err);
//                parser.parse(xml);
//                Document doc = parser.getDocument();
            } catch(SAXException ex){
                //ex.printStackTrace();
                throw new Exception("Unexpected error while parsing " + schema + ": " + ex.getMessage());
            }finally {
                if(logger.isDebugEnabled()){
                    logger.debug("Exiting validateXML");
                }   
            }
        }
    }
    
    /** 
     * This method converts the email request XML string into an 
     * Email Request Object. It creates a java.io.StringReader, and utilizes 
     * DOMUtil to parse the StringReader into Email Request Value Object.   
     * Also a specified SAX ErrorHandler will be notified if parsing errors 
     * occur. It calls the populateEmailRequest(emailRequest) method, which 
     * sets a timestamp on the object. Then the Email Requests object is sent 
     * back to either the MessageReceiver or EmailRequestGatherer 
     * for further processing.
     * @param emailRequestXMLString - XML request
     * @return EmailRequestVO
     * @throws SAXException ,IOException,Exception
     */    
    public EmailRequestVO createEmailRequestVO(String emailRequestXMLString)
        throws SAXException ,IOException,ParseException,
        ParserConfigurationException, Exception {

        if(logger.isDebugEnabled()){
            logger.debug("Entering createEmailRequestVO");
            //logger.debug("XML Request : " + emailRequestXMLString);
        }
        /*character mapping code*/                
        if(spcmHandler != null)
        {
        spcmap = spcmHandler.getSpecialCharacterMap();
        }        
        /*end character mapping code*/        
        EmailRequestVO emailRequestVO = new EmailRequestVO();
        OrderDetailVO ordDetail = new OrderDetailVO();
        emailRequestVO.setOrderDetailVO(ordDetail);
        
         try
        {   
            /* Parse out EmailRequestVO information */ 
            Document emailXML = 
                (Document)(DOMUtil.getDocument(emailRequestXMLString));

            /* retrieve order inquirty items */
            NodeList nodesEmailRequest = DOMUtil.selectNodes(emailXML,EmailConstants.EMAIL_REQUEST_NODES);
            Element element = null;
            
            for (int i = 0; i < nodesEmailRequest.getLength(); i++) 
            {
                element = (Element)nodesEmailRequest.item(i);
                
                if(findNode(element,EmailConstants.EMAIL_REQUEST_IDENTIFIER)!=null){
                    emailRequestVO.setServiceIdentifier(
                    getNodeValue(element,EmailConstants.EMAIL_REQUEST_IDENTIFIER));    
                }

                if(findNode(element,EmailConstants.EMAIL_REQUEST_COMPANY_ID)!=null){
                    emailRequestVO.setCompanyID(
                    getNodeValue(element,EmailConstants.EMAIL_REQUEST_COMPANY_ID));    
                }

                if(findNode(element,EmailConstants.EMAIL_REQUEST_SERVER_ID)!=null){
                    emailRequestVO.setServerID(
                    getNodeValue(element,EmailConstants.EMAIL_REQUEST_SERVER_ID));    
                }

                if(findNode(element,EmailConstants.EMAIL_REQUEST_SEQUENCE_NUMBER)!=null){
                    emailRequestVO.setSequenceNumber(
                    getNodeValue(element,EmailConstants.EMAIL_REQUEST_SEQUENCE_NUMBER));    
                }

                if(findNode(element,EmailConstants.EMAIL_REQUEST_TYPE)!=null){
                    emailRequestVO.setRequestType(
                    getNodeValue(element,EmailConstants.EMAIL_REQUEST_TYPE));    
                }

                if(findNode(element,EmailConstants.EMAIL_REQUEST_BILLING_FIRSTNAME)!=null){
                    String billingFirstName = getNodeValue(element,EmailConstants.EMAIL_REQUEST_BILLING_FIRSTNAME);
                    m = p.matcher(billingFirstName);
                    if (m.find()) {billingFirstName = LanguageUtility.specialCharacterMapping(billingFirstName, spcmap);}
                	emailRequestVO.setBillingFirstName(billingFirstName);
                    //logger.debug("###XML Proc###bfn : "+billingFirstName +"##ER FName: "+emailRequestVO.getBillingFirstName());
                }

                if(findNode(element,EmailConstants.EMAIL_REQUEST_BILLING_LASTNAME)!=null){
                	String billingLastName = getNodeValue(element,EmailConstants.EMAIL_REQUEST_BILLING_LASTNAME);
                	m = p.matcher(billingLastName);
                    if (m.find()) {billingLastName = LanguageUtility.specialCharacterMapping(billingLastName, spcmap);}
                	emailRequestVO.setBillingLastName(billingLastName);
                	//logger.debug("###XML Proc###bln : "+billingLastName +"##ER LName: "+emailRequestVO.getBillingLastName());
                }

                if(findNode(element,EmailConstants.EMAIL_REQUEST_BILLING_EMAIL)!=null){
                    emailRequestVO.setEmail(
                    getNodeValue(element,EmailConstants.EMAIL_REQUEST_BILLING_EMAIL));    
                }              

                if(findNode(element,EmailConstants.EMAIL_REQUEST_BILLING_DAYPHONE)!=null){
                	String dayPhone = getNodeValue(element,EmailConstants.EMAIL_REQUEST_BILLING_DAYPHONE);
                	m = p.matcher(dayPhone);
                    if (m.find()) {dayPhone = LanguageUtility.specialCharacterMapping(dayPhone, spcmap);}
                	emailRequestVO.setDayPhone(dayPhone);    
                }   

                if(findNode(element,EmailConstants.EMAIL_REQUEST_BILLING_EVEPHONE)!=null){
                	String evePhone = getNodeValue(element,EmailConstants.EMAIL_REQUEST_BILLING_EVEPHONE);
                	m = p.matcher(evePhone);
                    if (m.find()) {evePhone = LanguageUtility.specialCharacterMapping(evePhone, spcmap);}
                	emailRequestVO.setEvePhone(evePhone);    
                }     

                if(findNode(element,EmailConstants.EMAIL_REQUEST_ORDER_NUMBER)!=null){
                	String orderNumber = getNodeValue(element,EmailConstants.EMAIL_REQUEST_ORDER_NUMBER);
                	m = p.matcher(orderNumber);
                    if (m.find()) {orderNumber = LanguageUtility.specialCharacterMapping(orderNumber, spcmap);}
                	emailRequestVO.setOrderNumber(orderNumber);    
                }     

                if(findNode(element,EmailConstants.EMAIL_REQUEST_DELIVERY_DATE1)!=null){
                    SimpleDateFormat sdfOutput = 
                       new SimpleDateFormat (EmailConstants.DELIVERY_DATE_FORMAT); 
                    String textDate = getNodeValue(element,EmailConstants.EMAIL_REQUEST_DELIVERY_DATE1);
                    java.util.Date utilDate = sdfOutput.parse( textDate );
                    java.sql.Date deliveryDate1 = 
                        new java.sql.Date(utilDate.getTime());
                    logger.debug("delivery date in request: "+textDate+" formatted delivery date: "+deliveryDate1);
                    emailRequestVO.setDeliveryDateOne(deliveryDate1);    
                }   

                if(findNode(element,EmailConstants.EMAIL_REQUEST_DELIVERY_DATE2)!=null){
                    SimpleDateFormat sdfOutput = 
                       new SimpleDateFormat (EmailConstants.DELIVERY_DATE_FORMAT); 
                    String textDate = getNodeValue(element,EmailConstants.EMAIL_REQUEST_DELIVERY_DATE2);
                    java.util.Date utilDate = sdfOutput.parse( textDate );
                    java.sql.Date deliveryDate1 = 
                        new java.sql.Date(utilDate.getTime());
                    emailRequestVO.setDeliveryDateTwo(deliveryDate1);    
                } 

                if(findNode(element,EmailConstants.EMAIL_REQUEST_SUB_TYPE)!=null){
                    emailRequestVO.setSubType(
                    getNodeValue(element,EmailConstants.EMAIL_REQUEST_SUB_TYPE));    
                }   
               
                if(findNode(element,EmailConstants.EMAIL_REQUEST_COMMENT)!=null){
                	String comment = getNodeValue(element,EmailConstants.EMAIL_REQUEST_COMMENT);
                	m = p.matcher(comment);
                    if (m.find()) {comment = LanguageUtility.specialCharacterMapping(comment, spcmap);}
                	emailRequestVO.setComment(comment);    
                }                 

                if(findNode(element,EmailConstants.EMAIL_REQUEST_ORDER_DETAIL_ID)!=null){
                	emailRequestVO.getOrderDetailVO().setOrderDetailID(
                    getNodeValue(element,EmailConstants.EMAIL_REQUEST_ORDER_DETAIL_ID));
                } 
                if(findNode(element,EmailConstants.EMAIL_REQUEST_ORDER_GUID)!=null){
                	emailRequestVO.getOrderDetailVO().setOrderGuid(
                    getNodeValue(element,EmailConstants.EMAIL_REQUEST_ORDER_GUID));
                } 
                if(findNode(element,EmailConstants.EMAIL_REQUEST_ORDER_EXT_NUMBER)!=null){
                	emailRequestVO.getOrderDetailVO().setExternalOrderNumber(
                    getNodeValue(element,EmailConstants.EMAIL_REQUEST_ORDER_EXT_NUMBER));
                } 
                if(findNode(element,EmailConstants.EMAIL_REQUEST_ORDER_MASTER_NUMBER)!=null){
                	emailRequestVO.getOrderDetailVO().setMasterOrderNumber(
                    getNodeValue(element,EmailConstants.EMAIL_REQUEST_ORDER_MASTER_NUMBER));
                } 
                if(findNode(element,EmailConstants.EMAIL_REQUEST_CUSTOMER_ID)!=null){
                	emailRequestVO.getOrderDetailVO().setCustomerID(
                    getNodeValue(element,EmailConstants.EMAIL_REQUEST_CUSTOMER_ID));
                } 
                if(findNode(element,EmailConstants.EMAIL_REQUEST_ORDER_DISP_CODE)!=null){
                    emailRequestVO.getOrderDetailVO().setOrderDisposition(
                    getNodeValue(element,EmailConstants.EMAIL_REQUEST_ORDER_DISP_CODE));    
                } 
                if(findNode(element,EmailConstants.EMAIL_REQUEST_ORDER_INDICATOR)!=null){
                    emailRequestVO.getOrderDetailVO().setOrderIndicator(
                    getNodeValue(element,EmailConstants.EMAIL_REQUEST_ORDER_INDICATOR));    
                } 
                if(findNode(element,EmailConstants.EMAIL_REQUEST_SOURCE_CODE)!=null){
                    emailRequestVO.setSourceCode(
                    getNodeValue(element,EmailConstants.EMAIL_REQUEST_SOURCE_CODE));    
                } 

                if((DOMUtil.selectNodes(element,EmailConstants.EMAIL_REQUEST_ORDER_INQUIRY)) != null)
                { 
                    NodeList nodesInquiryDetail =DOMUtil.selectNodes(emailXML, 
                        EmailConstants.EMAIL_REQUEST_ORDER_INQUIRY_NODES);
                    NodeList nlRequestSubTypes = DOMUtil.selectNodes(emailXML, 
                        EmailConstants.EMAIL_REQUEST_ORDER_INQUIRY_SUB_TYPES);
                    
                    InquiryDetailVO inqDetail = 
                        createInquiryDetailVO(nodesInquiryDetail);
                    String requestSubTypes = 
                        findRequestSubType(nlRequestSubTypes);
                    if(requestSubTypes!= null)
                    {
                       inqDetail.setRequestSubType(requestSubTypes); 
                    }
                    else
                    {
                        inqDetail.setRequestSubType("");    
                    }
                    emailRequestVO.setInquiryVO(inqDetail);
                }
                
            }
            
            /* set the timestamp for the object */
            populateEmailRequest(emailRequestVO);
        }finally
        {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting createEmailRequestVO");
            }
        }
        
        return emailRequestVO;
    }

   /**
     * This method parses the order-inquiry nodes and uses the information
     * to create and populate an InquiryDetailVO
     * @param inqDetailNodes - org.w3c.dom.NodeList
     * @return InquiryDetailVO
 * @throws XPathExpressionException 
 * @throws DOMException 
     * @throws Exception
     */
    private InquiryDetailVO createInquiryDetailVO(NodeList inqDetailNodes)
    throws ParseException, DOMException, XPathExpressionException, Exception{

        if(logger.isDebugEnabled()){
            logger.debug("Entering createInquiryDetailVO");
        } 
        
        InquiryDetailVO inqDetail = new InquiryDetailVO();
        try
        {
            Element element = null;
            
            for (int i = 0; i < inqDetailNodes.getLength(); i++) 
            {
                element = (Element)inqDetailNodes.item(i);
                
                if(findNode(element,EmailConstants.INQ_DETAIL_RECEIPIENT_FIRST_NAME)!=null){                
                	String recipientFirstName = getNodeValue(element,EmailConstants.INQ_DETAIL_RECEIPIENT_FIRST_NAME);
                	m = p.matcher(recipientFirstName);
                    if (m.find()) {recipientFirstName = LanguageUtility.specialCharacterMapping(recipientFirstName, spcmap);}
                    inqDetail.setRecipientFirstName(recipientFirstName);
                    //logger.debug("###XML Proc InquiryDetailVO###bfn : "+recipientFirstName +"##ER FName: "+inqDetail.getRecipientFirstName());
                }     
                
                if(findNode(element,EmailConstants.INQ_DETAIL_RECEIPIENT_LAST_NAME)!=null){
                	String recipientLastName = getNodeValue(element,EmailConstants.INQ_DETAIL_RECEIPIENT_LAST_NAME);
                	m = p.matcher(recipientLastName);
                    if (m.find()) {recipientLastName = LanguageUtility.specialCharacterMapping(recipientLastName, spcmap);}
                    inqDetail.setRecipientLastName(recipientLastName);
                    //logger.debug("###XML Proc InquiryDetailVO###bln : "+recipientLastName +"##ER FName: "+inqDetail.getRecipientLastName());
                }                 
                
                if(findNode(element,EmailConstants.INQ_INSTITUTION)!=null){
                	String institution = getNodeValue(element,EmailConstants.INQ_INSTITUTION);
                	m = p.matcher(institution);
                    if (m.find()) {institution = LanguageUtility.specialCharacterMapping(institution, spcmap);}
                	inqDetail.setInstitution(institution);    
                }    

                if(findNode(element,EmailConstants.INQ_DETAIL_RECEIPIENT_STREET)!=null){
                	String recipientAddress = getNodeValue(element,EmailConstants.INQ_DETAIL_RECEIPIENT_STREET);
                	m = p.matcher(recipientAddress);
                    if (m.find()) {recipientAddress = LanguageUtility.specialCharacterMapping(recipientAddress, spcmap);}
                	inqDetail.setRecipientAddress(recipientAddress);    
                }               
                
                if(findNode(element,EmailConstants.INQ_DETAIL_RECEIPIENT_CITY)!=null){
                	String recipientCity = getNodeValue(element,EmailConstants.INQ_DETAIL_RECEIPIENT_CITY);
                	m = p.matcher(recipientCity);
                    if (m.find()) {recipientCity = LanguageUtility.specialCharacterMapping(recipientCity, spcmap);}
                	inqDetail.setRecipientCity(recipientCity);    
                }  

                if(findNode(element,EmailConstants.INQ_DETAIL_RECEIPIENT_STATE)!=null){
                	String recipientState = getNodeValue(element,EmailConstants.INQ_DETAIL_RECEIPIENT_STATE);
                	m = p.matcher(recipientState);
                    if (m.find()) {recipientState = LanguageUtility.specialCharacterMapping(recipientState, spcmap);}
                	inqDetail.setRecipientState(recipientState);    
                }  

                if(findNode(element,EmailConstants.INQ_DETAIL_RECEIPIENT_ZIP)!=null){
                	String recipientZip = getNodeValue(element,EmailConstants.INQ_DETAIL_RECEIPIENT_ZIP);
                	m = p.matcher(recipientZip);
                    if (m.find()) {recipientZip = LanguageUtility.specialCharacterMapping(recipientZip, spcmap);}
                	inqDetail.setRecipientZip(recipientZip);    
                }              

                if(findNode(element,EmailConstants.INQ_DETAIL_RECEIPIENT_COUNTRY)!=null){
                	String recipientCountry = getNodeValue(element,EmailConstants.INQ_DETAIL_RECEIPIENT_COUNTRY);
                	m = p.matcher(recipientCountry);
                    if (m.find()) {recipientCountry = LanguageUtility.specialCharacterMapping(recipientCountry, spcmap);}
                	inqDetail.setRecipientCountry(recipientCountry);    
                } 

                if(findNode(element,EmailConstants.INQ_DETAIL_RECEIPIENT_PHONE)!=null){
                	String recipientPhone = getNodeValue(element,EmailConstants.INQ_DETAIL_RECEIPIENT_PHONE);
                	m = p.matcher(recipientPhone);
                    if (m.find()) {recipientPhone = LanguageUtility.specialCharacterMapping(recipientPhone, spcmap);}
                	inqDetail.setRecipientPhone(recipientPhone);    
                } 

                if(findNode(element,EmailConstants.INQ_DETAIL_DELIVERY_DATE_ONE)!=null){
                    SimpleDateFormat sdfOutput = 
                       new SimpleDateFormat (EmailConstants.DELIVERY_DATE_FORMAT); 
                    String textDate = getNodeValue(element,EmailConstants.INQ_DETAIL_DELIVERY_DATE_ONE);
                    java.util.Date utilDate = sdfOutput.parse( textDate );
                    java.sql.Date deliveryDate1 = 
                        new java.sql.Date(utilDate.getTime());
                    inqDetail.setNewDeliveryDateOne(deliveryDate1);    
                } 

                if(findNode(element,EmailConstants.INQ_DETAIL_DELIVERY_DATE_TWO)!=null){
                    SimpleDateFormat sdfOutput = 
                       new SimpleDateFormat (EmailConstants.DELIVERY_DATE_FORMAT); 
                    String textDate = getNodeValue(element,EmailConstants.INQ_DETAIL_DELIVERY_DATE_TWO);
                    java.util.Date utilDate = sdfOutput.parse( textDate );
                    java.sql.Date deliveryDate2 = 
                        new java.sql.Date(utilDate.getTime());
                    inqDetail.setNewDeliveryDateTwo(deliveryDate2);    
                } 

                if(findNode(element,EmailConstants.INQ_DETAIL_PRODUCT)!=null){
                    inqDetail.setProductSku(
                    getNodeValue(element,EmailConstants.INQ_DETAIL_PRODUCT));    
                } 

                if(findNode(element,EmailConstants.INQ_DETAIL_CARD_MESSAGE)!=null){
                	String cardMessage = getNodeValue(element,EmailConstants.INQ_DETAIL_CARD_MESSAGE);
                	m = p.matcher(cardMessage);
                    if (m.find()) {cardMessage = LanguageUtility.specialCharacterMapping(cardMessage, spcmap);}
                	inqDetail.setCardMessage(cardMessage);    
                } 

                if(findNode(element,EmailConstants.INQ_DETAIL_CARD_SIGNATURE)!=null){
                	String cardSignature = getNodeValue(element,EmailConstants.INQ_DETAIL_CARD_SIGNATURE);
                	m = p.matcher(cardSignature);
                    if (m.find()) {cardSignature = LanguageUtility.specialCharacterMapping(cardSignature, spcmap);}
                	inqDetail.setCardSignature(cardSignature);    
                } 

                if(findNode(element,EmailConstants.INQ_DETAIL_OTHER_COMMENTS)!=null){
                	String otherComment = getNodeValue(element,EmailConstants.INQ_DETAIL_OTHER_COMMENTS);
                	m = p.matcher(otherComment);
                    if (m.find()) {otherComment = LanguageUtility.specialCharacterMapping(otherComment, spcmap);}
                	inqDetail.setOtherComment(otherComment);    
                }             
                
            }
            
        }finally{
            if(logger.isDebugEnabled()){
               logger.debug("Exiting createInquiryDetailVO");
            }
        }
        
        return inqDetail;
        
    }
    
    
    /**
     * This method iterates through list of subTypes and places them in one
     * String value
     * @param subTypes - org.w3c.dom.node.NodeList
     * @return String - subtypes
     * @throws Exception
     */
    private String findRequestSubType(NodeList subTypes) throws Exception{

        if(logger.isDebugEnabled()){
            logger.debug("Entering findRequestSubType");
        } 
        
        String subTypeList=null;
      try{
            /* iterate through subTypes and add them to return value */
            for (int i = 0; i < subTypes.getLength(); i++) 
            {
                Element subType = (Element) subTypes.item(i);
                
                String newType = subType.getAttribute(
                    EmailConstants.INQ_DETAIL_SUB_TYPE_ATTR_TYPE);
                if(subTypeList!=null)
                {
                subTypeList = subTypeList + "," + newType;
                }
                else
                {
                    subTypeList = newType;    
                }
            
            }
       }finally{
            if(logger.isDebugEnabled()){
               logger.debug("Exiting findRequestSubType");
            }
        }
         return subTypeList;
    }
    /**
     * This method sets the timestamp to the object.
     * @param emailRequest - EmailRequestVO object
     * @return n/a
     * @throws Exception
     */
    private void populateEmailRequest(EmailRequestVO emailRequest) 
        throws Exception{

        if(logger.isDebugEnabled()){
            logger.debug("Entering populateEmailRequest");
        } 
        
        try
        {   
            Timestamp ts = new Timestamp(System.currentTimeMillis());
            emailRequest.setTimeStamp(ts);
          
        }finally{
            if(logger.isDebugEnabled()){
               logger.debug("Exiting populateEmailRequest");
            }
        }
    }

    public String retrieveSequenceNumber(String emailRequestXMLString)
        throws SAXException ,IOException,ParseException,
        ParserConfigurationException, Exception {

        if(logger.isDebugEnabled()){
            logger.debug("Entering retrieveSequenceNumber");
            //logger.debug("XML Request : " + emailRequestXMLString);
        }

         String sequenceNumber = null;
         try
        {   
            /* Parse out EmailRequestVO information */ 
            Document emailXML = 
                (Document)(DOMUtil.getDocument(emailRequestXMLString));

            /* retrieve order inquirty items */
            NodeList nodesEmailRequest = DOMUtil.selectNodes(emailXML,EmailConstants.EMAIL_REQUEST_NODES);
            Element element = null;
            
            for (int i = 0; i < nodesEmailRequest.getLength(); i++) 
            {
                element = (Element)nodesEmailRequest.item(i);
                
            
                if((DOMUtil.selectNodes(element, EmailConstants.EMAIL_REQUEST_SEQUENCE_NUMBER)).item(0).
                    getFirstChild() != null)
                { 
                    sequenceNumber=(DOMUtil.selectNodes(element, 
                        EmailConstants.EMAIL_REQUEST_SEQUENCE_NUMBER)).item(0).
                        getFirstChild().getNodeValue();
                        i = nodesEmailRequest.getLength() + 1;
                }

            
                
            }

        }finally
        {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting retrieveSequenceNumber");
            }
        }
        
        return sequenceNumber;
    }
    
}

