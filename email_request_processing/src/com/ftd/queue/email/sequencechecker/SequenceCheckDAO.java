package com.ftd.queue.email.sequencechecker;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.ftd.osp.utilities.MessageUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.queue.email.constants.EmailConstants;

/**
 * This class handles retrieving, inserting and deleting missing 
 * sequences from the database.
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 */
public class SequenceCheckDAO 
{

    
    public SequenceCheckDAO(Connection conn)
    {
        dbConnection = conn;
    }
    
    private Connection dbConnection;
    private Logger logger = new Logger(
        "com.ftd.queue.email.sequencechecker.SequenceCheckDAO");
    private String status;
    private String message;
    

   /**
     * Call stored procedure get_all_unchecked_sequence to return a
     * list of unchecked sequences.
     * @param n/a
     * @return n/a
     * @throws SQLException,Exception
     */
    public List getAllUncheckedSequences() throws 
        IOException, SAXException, ParserConfigurationException, 
         SQLException, Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering getAllUncheckedSequences");
        }

        List unCheckSeqs = new ArrayList();

         DataRequest request = new DataRequest();
         MessageUtil messageUtil = MessageUtil.getInstance();

        try {
    
            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setStatementID(
                EmailConstants.RETRIEVE_ALL_UNCHECKED_SEQUENCES);
            
            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            ResultSet rs = (ResultSet) dau.execute(request);
          //  String seq = null;
            while(rs.next())     
            {   
               // SequenceCheckVO seq = new SequenceCheckVO();
              // seq.setSequenceNumber(rs.getString(1));
                unCheckSeqs.add(rs.getString(1));
            }
        
        }finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getAllUncheckedSequences");
            } 
        }
        return unCheckSeqs;
    }

   /**
     * Return the last checked sequence number based on the prefix.
     * @param prefix - String
     * @return n/a
     * @throws SQLException,Exception
     */
    public String getLastCheckedSequences(String prefix) 
        throws IOException, SAXException, ParserConfigurationException, 
         SQLException, Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering getLastCheckedSequences");
            logger.debug("prefix " +  prefix);
        }
        String lastCheckSeq = null;
         DataRequest request = new DataRequest();
         MessageUtil messageUtil = MessageUtil.getInstance();

        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(
                EmailConstants.RETRIEVE_LAST_CHECKED_SEQUENCE_IN_PREFIX, 
                prefix);

            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID(
                EmailConstants.RETRIEVE_LAST_CHECKED_SEQUENCE);

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
           // Map outputs = (Map) dau.execute(request);
            lastCheckSeq= (String) dau.execute(request);
            /* read store prodcedure output parameters to determine 
             * if the procedure executed successfully */
            /* Check output parameter to determine if the order is locked */
           // if(outputs!=null){
             //   lastCheckSeq = (String) outputs.get(
               //     EmailConstants.RETRIEVE_LAST_CHECKED_SEQUENCE_OUT_SEQUENCE);
           // }
        }finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getLastCheckedSequences");
            } 
        }
        return lastCheckSeq;
    }

   /**
     * Insert the missing sequence number to email error log table.
     * @param missingSeq - String
     * @return n/a
     * @throws SQLException,Exception
     */
    public void storeMissingSequence(String missingSeq,String emailXML) 
        throws IOException, SAXException, ParserConfigurationException, 
         SQLException, Exception
        {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering storeMissingSequence");
            logger.debug("missingSeq " +  missingSeq);
        }
         DataRequest request = new DataRequest();
         MessageUtil messageUtil = MessageUtil.getInstance();

        try {
            java.util.Date utilDate = new Date(); 
            java.sql.Date errorDate = new java.sql.Date(utilDate.getTime());
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(EmailConstants.STORE_MISSING_SEQUENCE_IN_SEQ_NUMBER, 
                missingSeq);
            inputParams.put(EmailConstants.STORE_MISSING_SEQUENCE_IN_ERROR_EMAIL_XML, 
                "");
            inputParams.put(EmailConstants.STORE_MISSING_SEQUENCE_IN_ERROR_INDICATOR, 
                EmailConstants.ERROR_INDICATOR_MISSING);
            inputParams.put(EmailConstants.STORE_MISSING_SEQUENCE_IN_ERROR_DATE, 
                errorDate);
                
            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID(EmailConstants.STORE_MISSING_SEQUENCE);

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            /* read store prodcedure output parameters to determine 
             * if the procedure executed successfully */
            status = (String) outputs.get(
                EmailConstants.STORE_MISSING_SEQUENCE_OUT_STATUS_PARAM);
            if(status.equals("N"))
            {
                message = (String) outputs.get(
                EmailConstants.STORE_MISSING_SEQUENCE_OUT_MESSAGE_PARAM);
                String messageString = 
                    messageUtil.getMessageContent(
                    EmailConstants.UNABLE_TO_STORE_MISSING_SEQUENCE);
                throw new Exception(messageString + " " +  message);
            }
        }finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting storeMissingSequence");
            } 
        }

}




  public void updateSequence(String sequence) 
        throws IOException, SAXException, ParserConfigurationException, 
         SQLException, Exception
        {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering updateSequence "+sequence);
            
        }
         DataRequest request = new DataRequest();
         MessageUtil messageUtil = MessageUtil.getInstance();

        try {
            java.util.Date utilDate = new Date(); 
            java.sql.Date errorDate = new java.sql.Date(utilDate.getTime());
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_EMAIL_XML_SEQ", sequence);
                           
            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("UPDATE_CHECKED_EMAIL_SEQ");

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            /* read store prodcedure output parameters to determine 
             * if the procedure executed successfully */
            status = (String) outputs.get("OUT_STATUS");
            if(status.equals("N"))
            {
                message = (String) outputs.get(
                EmailConstants.STORE_MISSING_SEQUENCE_OUT_MESSAGE_PARAM);
                String messageString = 
                    messageUtil.getMessageContent(
                    EmailConstants.UNABLE_TO_STORE_MISSING_SEQUENCE);
                throw new Exception(messageString + " " +  message);
            }
        }finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting storeMissingSequence");
            } 
        }

}






   /**
     * Delete the missing sequence number from email error log table.
     * @param missingSeq - String
     * @return n/a
     * @throws SQLException,Exception
     */
    public void deleteMissingSequence(String missingSeq) throws 
        IOException, SAXException, ParserConfigurationException, 
         SQLException, Exception
        {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering deleteMissingSequence");
            logger.debug("missingSeq " +  missingSeq);
        }
         DataRequest request = new DataRequest();
         MessageUtil messageUtil = MessageUtil.getInstance();

        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(EmailConstants.DELETE_MISSING_SEQUENCE_IN_SEQ_NUMBER, 
                missingSeq);

            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID(EmailConstants.DELETE_MISSING_SEQUENCE);

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            /* read store prodcedure output parameters to determine 
             * if the procedure executed successfully */
            status = (String) outputs.get(
                EmailConstants.DELETE_MISSING_SEQUENCE_OUT_STATUS_PARAM);
            if(status.equals("N"))
            {
                message = (String) outputs.get(
                EmailConstants.DELETE_MISSING_SEQUENCE_OUT_MESSAGE_PARAM);
                String messageString = 
                    messageUtil.getMessageContent(
                    EmailConstants.UNABLE_TO_DELETE_MISSING_SEQUENCE);
                throw new Exception(messageString + " " +  message);
            }
        }finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting deleteMissingSequence");
            } 
        }
}

    /**
     * Release order lock.
     * @param OrderVO
     * @param hashCode - int
     * @param csr_id - String 
     * @return n/a
     * @throws SQLException, Exception
     * @todo stored procedure not completed
     */
    public void releaseSequenceCheckLock(String sessionId, String csr_id) 
        throws IOException, SAXException, ParserConfigurationException, 
         SQLException, Exception
        {
        if(logger.isDebugEnabled()){
            logger.debug("Entering releaseOrder");
          //  logger.debug("orderVO : " + orderVO);
            logger.debug("sessionId : " + sessionId);
            logger.debug("csr_id : " + csr_id);
        }
         DataRequest request = new DataRequest();
         MessageUtil messageUtil = MessageUtil.getInstance();
        
        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(EmailConstants.RELEASE_ORDER_IN_ENTITY_TYPE, 
                EmailConstants.SEQUENCE_CHECK_ENTITY_TYPE);
            inputParams.put(EmailConstants.RELEASE_ORDER_IN_ENTITY_ID, 
                EmailConstants.SEQUENCE_CHECK_ENTITY_ID);
            inputParams.put(EmailConstants.RELEASE_ORDER_IN_SESSION_ID, 
                sessionId);
            inputParams.put(EmailConstants.RELEASE_ORDER_IN_CSR_ID, csr_id);
            
            // build DataRequest object
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID(EmailConstants.RELEASE_ORDER);

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
                status = (String) outputs.get(EmailConstants.RELEASE_ORDER_STATUS_PARAM);
                if(status.equals("N"))
                {
                    message = (String) outputs.get(EmailConstants.RELEASE_ORDER_MESSAGE_PARAM);
                    String messageString = 
                        messageUtil.getMessageContent(
                        EmailConstants.UNABLE_RELEASE_ORDER);
                    throw new Exception(messageString + " " +  message);
                }
            
        }finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting releaseOrder");
            } 
        }       
    }

    /**
     * Lock order
     * @param OrderVO
     * @return n/a
     * @throws SQLException, Exception
     * @todo stored procedure not completed
     */
    public boolean lockSequenceCheck(String sessionId, String csr_id) 
        throws IOException, SAXException, ParserConfigurationException, 
         SQLException, Exception{
        if(logger.isDebugEnabled()){
            logger.debug("Entering lockOrder");
          //  logger.debug("orderVO : " + orderVO);
            logger.debug("sessionId : " + sessionId);
            logger.debug("csr_id : " + csr_id);
        }
        
        boolean checkLocked = false;
         DataRequest request = new DataRequest();
         MessageUtil messageUtil = MessageUtil.getInstance();
        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(EmailConstants.LOCK_ORDER_IN_ENTITY_TYPE, 
                EmailConstants.SEQUENCE_CHECK_ENTITY_TYPE);
            inputParams.put(EmailConstants.LOCK_ORDER_IN_ENTITY_ID, 
                EmailConstants.SEQUENCE_CHECK_ENTITY_ID);
            inputParams.put(EmailConstants.LOCK_ORDER_IN_SESSION_ID, 
                sessionId);
            inputParams.put(EmailConstants.LOCK_ORDER_IN_CSR_ID, csr_id);

            // build DataRequest object
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID(EmailConstants.LOCK_ORDER);

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
                status = (String) outputs.get(EmailConstants.LOCK_ORDER_STATUS_PARAM);
                if(status.equals("N"))
                {
                    message = (String) outputs.get(EmailConstants.LOCK_ORDER_MESSAGE_PARAM);
                    String messageString = 
                        messageUtil.getMessageContent(
                        EmailConstants.UNABLE_LOCK_ORDER);
                    throw new Exception(messageString + " " +  message);
                }
                else
                {
                    checkLocked=true;
                }
            
        }finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting lockOrder");
            } 
        }     
        return checkLocked;
    }

    public long retrieveMissingMaxSequence()
        throws IOException, SAXException, ParserConfigurationException, 
         SQLException, Exception{
       if(logger.isDebugEnabled()){
            logger.debug("Entering retrieveMissingMaxSequence");

        }
        long maximumMissingSeq=0;
        
        try{
            // build DataRequest object
            DataRequest request = new DataRequest();
            request.reset();
            request.setConnection(dbConnection);
            HashMap inputParams = new HashMap();//SEQUENCE_CHECKER
            inputParams.put("CONTEXT_ID", EmailConstants.SEQ_CHECK_GLOBAL_PARAM_CONTEXT);
            request.setInputParams(inputParams);
            request.setStatementID(EmailConstants.GET_MAXIMUM_MISSING_SEQUENCE_COUNT);

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();

            HashMap queueConfigMap = new HashMap();

            ResultSet queueConfigContextRS = (ResultSet) dau.execute(request);
            while(queueConfigContextRS.next())      // get queue config context parms
            {
                String name = queueConfigContextRS.getString(2);
                if(name.equals(EmailConstants.SEQ_CHECK_GLOBAL_PARAM_MISSING_SEQ)){
                    maximumMissingSeq = Long.parseLong((String) queueConfigContextRS.getObject(3));
                }
            }
        }finally{

            if(logger.isDebugEnabled()){
                logger.debug("Exiting load");
            }
        }
        return maximumMissingSeq;
        
        }
    
}
