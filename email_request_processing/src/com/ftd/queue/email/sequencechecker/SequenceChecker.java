package com.ftd.queue.email.sequencechecker;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import com.ftd.eventhandling.events.EventHandler;
import com.ftd.osp.utilities.MessageUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.exception.SystemMessengerException;
import com.ftd.queue.email.constants.EmailConstants;
import com.ftd.queue.email.util.DataSourceHelper;

public class SequenceChecker extends EventHandler
{
    public SequenceChecker()
    {
    }

    private Logger logger = new Logger(
        "com.ftd.queue.email.sequencechecker.SequenceChecker");

    
    private Connection dbConnection;
    
   /**
     * This method invokes the SeqeunceCheckBO to start the sequence check 
     * process.  It will only invoke this method of another instance of 
     * this class is not running (in this or any JVM).  It does this by check 
     * for the existence of a lock in the database.
     * @param n/a
     * @return n/a
     * @throws n/a
     */
    public void run() throws IOException, SAXException, ParserConfigurationException, 
         SQLException, SystemMessengerException, Exception{
        
        if(logger.isDebugEnabled()) {
            logger.debug("Entering run");
        }
        
        try{
            SequenceCheckBO sequenceBO = new SequenceCheckBO(dbConnection);
            sequenceBO.execute();
                    
        }finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting run");
            } 
        }     
    }


   /**
     * Starting point. It starts up the sequence checking thread.
     * @param payload - Object
     * @return n/a
     * @throws Throwable
     */
    public void invoke(Object payload) throws Throwable
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering invoke");
        }
        
        try
        {
            //create connection
            dbConnection = DataSourceHelper.createDatabaseConnection();
            //begin sequence checking
            run();  
            
        }catch(TransformerException e){
        	processError(e);          
        }catch(ParserConfigurationException e){
        	processError(e);
        }catch(SAXException e){
        	processError(e);
        }catch(IOException e){
        	processError(e);
        }catch(SQLException e){
            processError(e);   
        }catch(SystemMessengerException e){
            processError(e);
        }catch(Exception e){
        	processError(e);
        }finally
        {
            /* close database connection */
            if(dbConnection!=null){
                dbConnection.close();
            }
            if(logger.isDebugEnabled()){
               logger.debug("Exiting invoke");
            }
        }          
    }
    /**
     * @param e
     * @throws IOException
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws XSLException
     * @throws Throwable
     */
    private void processError(Throwable e) throws Throwable {
        MessageUtil messageUtil = MessageUtil.getInstance();
        String messageString=messageUtil.getMessageContent(
            EmailConstants.UNABLE_TO_EXECUTE_SEQUENCE_CHECK_PROCESS);
        logger.error(messageString, e);
        throw new Throwable(messageString + e);
    }


     
            
   

}
