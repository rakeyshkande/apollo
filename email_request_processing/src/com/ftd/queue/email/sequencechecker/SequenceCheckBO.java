package com.ftd.queue.email.sequencechecker;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.MessageUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.systemmessenger.exception.SystemMessengerException;
import com.ftd.queue.email.constants.EmailConstants;

/**
 * This object contains all the logic to perform the sequence checking
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 */
public class SequenceCheckBO 
{
    private Connection dbConnection;
     
    public SequenceCheckBO(Connection conn)
    {
        dbConnection = conn;
    }
    
    private Logger logger = new Logger(
        "com.ftd.queue.email.sequencechecker.SequenceCheckBO");
   
    
   
   
   /**
     * This method controls the process of the of Sequence check.  
     * This method checks for missing sequences and stores the missing 
     * sequences to the email_error_log table. Also, it generates alerts 
     * when the missing sequences are found. This process also checks if any 
     * messages that were previously flagged as missing have been received.  
     * If this happens the message will be deleted from the email_error_log 
     * table. The email request sequences are tracked for each request prefix. 
     * The request prefix consists of the alpha chars in the beginning of the 
     * sequence number.
     * The format for customer service emails will always be the machine number
     * of the page server at Novator and the sequence number which comes 
     * from a btr file on each of those servers.
     * Example of the format is: ftd20c-70231
     * Call getAllUncheckedSequences method of SequenceCheckDAO to retrieve all 
     * unchecked sequences from email archive table.
     * Call createPrefixMap method to create a map which key is the prefix 
     * and value is last checked sequence of this prefix.
     * Iterate through the unchecked list and check for missing sequences.
     * Call storeMissingSequence method of SequenceCheckDAO to store the 
     * missing sequences.
     * Call deleteMissingSequence method of SequenceCheckDAO to delete the 
     * previously missing sequences that are received.
     * Send alerts out.
     * @param inqDetailNodes - org.w3c.dom.NodeList
     * @return InquiryDetailVO
     * @throws XSLException,Exception
     */ 
  
  
    public void execute() 
        throws IOException, SAXException, ParserConfigurationException, 
         SQLException, SystemMessengerException, Exception  
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering execute");
        }

         MessageUtil messageUtil = MessageUtil.getInstance();



        SequenceCheckDAO sequenceDAO = new SequenceCheckDAO(dbConnection);
        try{
            
            /* retrieve csr_id */
            String csr_id = ConfigurationUtil.getInstance().getProperty(
                EmailConstants.ORDER_INFORMATION_CONFIG_FILE, 
                EmailConstants.ORDER_INFORMATION_CSR_ID);
            String sessionId="EMAIL_SEQ_"+this.hashCode()+"_"+new Date().getTime();
            if(sequenceDAO.lockSequenceCheck(sessionId,csr_id)){
                List unCheckedSequences = sequenceDAO.getAllUncheckedSequences();
                if(unCheckedSequences!=null){
                    Map sequenceMap = new HashMap();
                    long missingMaxSequence = sequenceDAO.retrieveMissingMaxSequence();
                    //createPrefixMap(unCheckedSequences);
                    Iterator unChecked = unCheckedSequences.iterator();
                    HashMap missingSeqMap=new HashMap();                 
              
                    while( unChecked.hasNext()) 
                    {
                        long startSeq=-1;
                        String sequence = (String) unChecked.next();
                        //update the check date to sysdate.
                        sequenceDAO.updateSequence(sequence);
                                                
                        logger.debug("sequence="+sequence);
                        String prefix = getPrefix(sequence);
                  
                        logger.debug("prefix="+prefix);
                        long sequenceNumber = Long.parseLong(getSequence(sequence));
                        logger.debug("current  sequence="+sequenceNumber);
                        //find start and end sequence number for the given prefix:
                        if(sequenceMap.get(prefix)!=null)
                        {
                          startSeq=((Long)sequenceMap.get(prefix)).longValue();
                        }else
                        {
                           String lastSeq=sequenceDAO.getLastCheckedSequences(prefix);
                           long lastSeqNum=new Long(getSequence(lastSeq)).longValue();
                           if(lastSeq.equals(sequence))
                           {
                             startSeq=lastSeqNum;
                           }else
                           {
                             startSeq=lastSeqNum+1;
                           }
                           
                  
                        }
                      
                        
                        logger.debug("start seq="+startSeq);
                        //long endSeq =  ((Long)sequenceMap.get(EmailConstants.ENDING_SEQ_PREFIX + prefix)).longValue();
                        
                        //if the current sequence number equals start sequence number, increment the start sequence number by 1
                        //add the new start sequence number to the map.
                        if(sequenceNumber==startSeq)
                        {
                          sequenceMap.put(prefix, new Long(startSeq+1));
                        }else if(sequenceNumber<startSeq)
                        {
                          //remove the sequence number from missing map.
                          this.removeSequenceFromMap(missingSeqMap, prefix, sequenceNumber);
                          //remove the sequence number from the email error table
                          sequenceDAO.deleteMissingSequence(sequence);
           
                        }else
                        {
                          
                           this.addMissingSeqToMap(missingSeqMap, prefix, startSeq, sequenceNumber, missingMaxSequence);
                           sequenceMap.put(prefix, new Long(sequenceNumber+1));
                        }
                        
                                          
                        
                    }
                    
                    if(!missingSeqMap.isEmpty())
                    {
                            sendAlert(missingSeqMap, sequenceDAO);
                    }
                    
                }  
                sequenceDAO.releaseSequenceCheckLock(sessionId,csr_id);
            }
        }finally{
            if(logger.isDebugEnabled()){
               logger.debug("Exiting execute");
            } 
        }
    }
    
    
    private void addMissingSeqToMap(HashMap missingSeqMap, String prefix, long startSeq, long endSeq, long maxMissing)
    {
        List missingList=new ArrayList();
        if(missingSeqMap.containsKey(prefix))
        {
          missingList=(List)missingSeqMap.get(prefix);
          
        }
        long diff=endSeq-startSeq;
        if(diff>maxMissing)
        {
          missingList.add(startSeq+" - "+prefix+"-"+endSeq);
          
        }else{
            for(long i=startSeq; i<endSeq; i++)
            {
              missingList.add(new Long(i+1));
            }
        }
        
        missingSeqMap.put(prefix, missingList);
    }
    
    
    
    private void removeSequenceFromMap(HashMap missingSeqMap, String prefix, long sequence)
    {
        if(missingSeqMap.containsKey(prefix))
        {
           List missingList=(List)missingSeqMap.get(prefix);
           missingList.remove(new Long(sequence));
          
        }
    }


   /**
     * Iterate through the unchecked list
     * Get the prefix.
     * Call getLastCheckSequence method of SequenceCheckDAO to retrieve the 
     * last sequence number of each prefix.
     * Return a Map which the key is the prefix and value is the last checked 
     * sequence number. If the prefix does not exist, use the current sequence 
     * number as the last checked sequence number.
     * @param inqDetailNodes - org.w3c.dom.NodeList
     * @return InquiryDetailVO
     * @throws XSLException,Exception
     */                                                 
   /* private Map createPrefixMap(List sequences) 
        throws IOException, SAXException, ParserConfigurationException, 
         SQLException, Exception  
        { 
        
        MessageUtil messageUtil = MessageUtil.getInstance();
        Map prefixMap= new HashMap();
        try{
            Iterator unChecked = sequences.iterator();
            
            String seq = null;
            List prefixes = new ArrayList();
            while( unChecked.hasNext() ) 
            {
                seq = (String) unChecked.next();   
                String lookupPrefix = getPrefix(seq);
                String seqNumber = getSequence(seq);
                if(prefixMap!=null)
                {
                    //check for current ending sequence
                    String endSeq = (String) prefixMap.get(EmailConstants.ENDING_SEQ_PREFIX + lookupPrefix);
                
                    if(endSeq!=null)
                    {
                        //replace if new one larger
                        long exist = Long.parseLong(endSeq);
                        long newSeq = Long.parseLong(seqNumber);
                        if(newSeq > exist)
                        {
                            prefixMap.put(EmailConstants.ENDING_SEQ_PREFIX + lookupPrefix, new Long(seqNumber));
                        }
                    }
                    else
                    {
                        //add new ending sequence
                        prefixMap.put(EmailConstants.ENDING_SEQ_PREFIX + lookupPrefix,new Long(seqNumber));
                    }
                }
                //check if prefix last sequence has been retrieved
               
                if(!prefixes.contains(lookupPrefix))
                {
                  prefixes.add(lookupPrefix);
                  String lastSeq = sequenceDAO.getLastCheckedSequences(lookupPrefix);
                    if(lastSeq!=null){
                        String lastSeqNum=getSequence(lastSeq);
                        Long startSeq=new Long(Long.parseLong(lastSeqNum)+1);
                        prefixMap.put(EmailConstants.STARTING_SEQ_PREFIX + lookupPrefix, startSeq);
                    }
                    else
                    {
                        prefixMap.put(EmailConstants.STARTING_SEQ_PREFIX + lookupPrefix, new Long(seqNumber));
                       
                    }
                }
                
              
            }
    
            }finally {
                if(logger.isDebugEnabled()){
                   logger.debug("Exiting execute");
                } 
            }
            return prefixMap;
    }*/
    

   /**
     * Create an alert for the missing sequence number.
     * Send the alert.
     * @param inqDetailNodes - org.w3c.dom.NodeList
     * @return InquiryDetailVO
     * @throws XSLException,Exception
     */
    private void sendAlert(Map missingSeqMap, SequenceCheckDAO sequenceDAO) throws SAXException,
        ParserConfigurationException, IOException, SQLException,
        SystemMessengerException, Exception
    {
        try{

            
            SystemMessengerVO systemError = new SystemMessengerVO();
            systemError.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
            SystemMessenger systemMessenger = SystemMessenger.getInstance();
            Iterator keyIt=missingSeqMap.keySet().iterator();
            while(keyIt.hasNext()){
                String prefix=(String)keyIt.next();
                List missingSeq=(List)missingSeqMap.get(prefix);
                
                Iterator missingSeqNumber = missingSeq.iterator();
                
                
                while( missingSeqNumber.hasNext() ) 
                {
                    Object sequence = missingSeqNumber.next();
                    //store to the error table.
                    String missing=prefix+"-"+sequence;
                    sequenceDAO.storeMissingSequence(missing, "");
                    //send message for every missing sequence number
                    systemError.setMessage("Novator Email Request Sequence Error. \n"+EmailConstants.SEQUENCE_CHECK_MISSING_SEQ_LIST + missing);            
                    systemError.setRead(true);
                    systemError.setSource(EmailConstants.SYS_MESS_SOURCE);
                    systemError.setType(EmailConstants.SYS_MESS_TYPE_ERROR);
                    
                    
                    systemMessenger.send(systemError,dbConnection,false);
                }
            
            } 
            
            
            
        }finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting execute");
            } 
        }
    }

    /* TBD */
    
    private String getPrefix(String sequenceNumber) throws Exception
    {
       
      
        
          int index = sequenceNumber.indexOf(EmailConstants.SEQUENCE_SEPARATOR);
            
          return sequenceNumber.substring(0,index);
          
    }
    
    private String getSequence(String sequenceNumber) throws Exception
    {
        String sequence = null;

        int index = sequenceNumber.indexOf(EmailConstants.SEQUENCE_SEPARATOR);
        sequence = sequenceNumber.substring(index + 1);
    
        return sequence;
    }
  
}
