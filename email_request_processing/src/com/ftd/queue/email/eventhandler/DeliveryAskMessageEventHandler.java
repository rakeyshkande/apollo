package com.ftd.queue.email.eventhandler;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import com.ftd.eventhandling.events.EventHandler;
import com.ftd.op.common.to.ResultTO;
import com.ftd.op.mercury.to.ASKMessageTO;
import com.ftd.op.mercury.to.BaseMercuryMessageTO;
import com.ftd.osp.utilities.MessageUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.systemmessenger.exception.SystemMessengerException;
import com.ftd.queue.email.constants.EmailConstants;
import com.ftd.queue.email.dao.AskEventDAO;
import com.ftd.queue.email.dao.EmailQueueDAO;
import com.ftd.queue.email.util.DataSourceHelper;
import com.ftd.queue.email.util.MessagingServiceLocator;
import com.ftd.queue.email.vo.ASKEventHandlerVO;
import com.ftd.queue.email.vo.OrderDetailVO;
import com.ftd.queue.email.vo.QueueVO;

/**
 * This class extends com.ftd.eventhandling.events.EventHandler and handles
 * the ASK event that required for Delivery Inquiry Email request.
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 */
public class DeliveryAskMessageEventHandler extends EventHandler
{   
    private Logger logger = 
        new Logger("com.ftd.queue.email.eventhandler.DeliveryAskMessageEventHandler");
    
    private int firstAsk=24;
    private int secondAsk=48;
    
    public DeliveryAskMessageEventHandler()
    {
      
    }  
    
    /**
     * Main handler logic. 
     * @param n/a
     * @return List
     * @throws Throwable
     */ 
    public void invoke(Object payload) throws Throwable
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering invoke DeliveryAskMessageEventHandler");
        }
       
        Connection dbConnection=null;
        try
        {  
            dbConnection = DataSourceHelper.createDatabaseConnection();
            
            this.processFirstAsk(dbConnection);
            this.processSecondAsk(dbConnection);        
        }catch(Exception e){
            MessageUtil messageUtil = MessageUtil.getInstance();
            String messageString=messageUtil.getMessageContent(
                EmailConstants.UNABLE_PROCESS_DELIVERY_ASK_EVENT);
            logger.error(messageString,e);
            this.sendSystemError(dbConnection, "DeliveryAskMessageEventHandler", messageString);
        }finally
        {
            /* close database connection */
            if(dbConnection!=null){
                dbConnection.close();
            }
            if(logger.isDebugEnabled()){
               logger.debug("Exiting invoke");
            }
        }    
    }

    /**
     * Call getQueueList method of AskEventDAO that returns a list
     * of order detail ids that need to be queued.
     * @param n/a
     * @return List
     * @throws Exception
     */ 
    public Map retrieveQueueMap(Connection dbConnection) throws 
        IOException, SAXException, ParserConfigurationException, 
         SQLException, Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering retrieveQueueMap");
        }
        
        
        try
        {
            AskEventDAO askEvent = new AskEventDAO(dbConnection);
            return  askEvent.getQueueMap("DI");  
            
        }finally
        {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting retrieveQueueMap");
            }
        }    
       
    }
    
    
    
     public void processFirstAsk(Connection dbConnection) throws 
        IOException, SAXException, ParserConfigurationException, 
         SQLException, Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering processFirstAsk");
        }
        
        
        try
        {
           Map messageMap=this.retrieveMessageMap(dbConnection);
          
           String priority=(String)messageMap.keySet().iterator().next();//should be only one
           logger.debug("DI priority="+priority);
           List messageList = (List)messageMap.get(priority);
           
           EmailQueueDAO emailQueueDAO = new EmailQueueDAO(dbConnection);
           AskEventDAO askDAO=new AskEventDAO(dbConnection);
           if(messageList!=null&&!messageList.isEmpty())
           {
             Iterator it=messageList.iterator();
             while(it.hasNext())
             {
               ASKEventHandlerVO askVO=(ASKEventHandlerVO)it.next();
               String flag="Y";
               if(!askVO.isAnswered())
               {
                  if(askVO.isFTDRejectedOrCancelled()||!askVO.orderHasLiveFTD())
                 {
                   
                   logger.debug("mercury status flag=["+askVO.getMercuryStatusFlags()+"]");
                   //set flag to Q, so the request will not get queued twice.
                   this.storeQueue(askVO, emailQueueDAO);
                   flag="Q";
                   
                 }else
                 {
                   //send message.
                   boolean sent=this.sendMessage(askVO, emailQueueDAO, dbConnection);
                   if(sent){
                        flag="N";
                   }else
                   {
                     //set flag to Q, so the request will not get queued twice.
                     this.storeQueue(askVO, emailQueueDAO);
                     flag="Q";
                   }
                 }
               }
               //update the responsed flag.
               askDAO.updateASKEventLog(askVO, 1, flag);
              
             }
           }
            
        }finally
        {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting processFirstAsk");
            }
        }    
       
    }
    
    
    
    public void processSecondAsk(Connection dbConnection) throws 
        IOException, SAXException, ParserConfigurationException, 
         SQLException, Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering processSecondAsk");
        }
        
        
        try
        {
           Map queueMap=retrieveQueueMap(dbConnection);
           String priority=(String)queueMap.keySet().iterator().next();//should be only one
           logger.debug("DI priority="+priority);
           List messageList = (List)queueMap.get(priority);
//           int priorityInt=Integer.parseInt(priority);
//           EmailQueueDAO emailQueueDAO = new EmailQueueDAO(dbConnection);
           AskEventDAO askDAO=new AskEventDAO(dbConnection);
           if(messageList!=null&&!messageList.isEmpty())
           {
             Iterator it=messageList.iterator();
             while(it.hasNext())
             {
               ASKEventHandlerVO askVO=(ASKEventHandlerVO)it.next();
               //update the responsed flag.
               askDAO.updateASKEventLog(askVO, 2, (askVO.isAnswered()?"Y":"N"));
               
               // JP Puzon 03-29-2006
               // Defect 929: Commented out the remaining workflow below for unanswered ASK messages.
//               if(!askVO.isAnswered()){
//                //check for higher priority untagged queue
//                QueueVO queueVO=emailQueueDAO.lookupHighestPriorityExistingQueue(askVO.getOrderDetailID());
//                
//                if(queueVO==null||queueVO.getPriority()>priorityInt){//lower than DI queue's priority.
//                  this.storeQueue(askVO, emailQueueDAO);
//                }
//              }                
             }
           }
            
        }finally
        {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting processSecondAsk");
            }
        }    
       
    }
    
    
    
    
    /**
     * Create and populate Delivery Inquiry queues
     * Call EmailQueueDAO to store the queues.
     * @param orderDetailIdList - List
     * @return List
     * @throws Exception
     * @todo fill in values for queue object
     */ 
     public void storeQueue(ASKEventHandlerVO askVO, EmailQueueDAO emailQueueDAO) 
        throws IOException, SAXException, SQLException, 
        ParserConfigurationException , Exception
     {
        if(logger.isDebugEnabled()){
            logger.debug("Entering storeQueue");
           
        }
                
        try
        {
       
               
                OrderDetailVO ordDetail = new OrderDetailVO();
                ordDetail.setOrderDetailID(askVO.getOrderDetailID());
                emailQueueDAO.setOrderDetailVOIds(ordDetail.getOrderDetailID(), ordDetail);
                ordDetail.setFillingFloristId(askVO.getFillingFloristId());
                QueueVO queue = new QueueVO();
                queue.setMessageType(askVO.getQueueType());
                logger.debug("set QueueType="+queue.getMessageType());
                queue.setEmail(askVO.getSendEmail());
                queue.setPointOfContactID(askVO.getPointOfContactId());
                queue.setQueueType(askVO.getQueueType());
                queue.setTimeStamp(askVO.getReceivedDate());  
                queue.setSystem(askVO.getMessageType());
                queue.setOrderDetailVO(ordDetail);
                emailQueueDAO.store(queue);
                      
        }finally
        {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting storeQueue");
            }
        }            
     }

    /**
     * Call getMessageList method of AskEventDAO that returns a list
     * of order detail ids, filling florist ids and message types 
     * that need to send ASK message to.
     * @param n/a
     * @return List
     * @throws Exception
     */ 
     public Map retrieveMessageMap(Connection dbConnection) throws 
        IOException, SAXException, ParserConfigurationException, 
         SQLException, Exception 
     {
        if(logger.isDebugEnabled()){
            logger.debug("Entering retrieveMessageMap");
        }
       
        
        try
        {
            AskEventDAO askEvent = new AskEventDAO(dbConnection);
            return  askEvent.getMessageMap("DI");
            
        }finally
        {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting retrieveMessageMap");
            }
        }    
        
        
     }

   /**
     * Create and populate ASK message TO
     * If the message type equals Mercury, call Mercury Message API
     * to send ASK to florists otherwise call Venus Message API to 
     * send ASK to Vendor.
     * @param n/a
     * @return List
     * @throws Exception
     * @todo fill in ASK messages and send
     */ 
    public boolean sendMessage(ASKEventHandlerVO askVO, EmailQueueDAO emailQueueDAO, Connection dbConnection) throws Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering sendMessage");
            
        }
        boolean sent=true;       
        try
        {
           
               
                 if(askVO.getMessageType().equalsIgnoreCase(EmailConstants.SYSTEM_TYPE_MERCURY))
                {
                  ASKMessageTO askTO=emailQueueDAO.getMercuryASKMessage(askVO.getOrderDetailID());
                  if(askTO!=null)
                  {
                      askTO.setOperator("SYS");
                      askTO.setMercuryStatus(BaseMercuryMessageTO.MERCURY_OPEN);
                      askTO.setDirection(BaseMercuryMessageTO.OUTBOUND);
                      askTO.setComments(EmailConstants.MERCURY_MSG_COMMENTS_DI);    
                      ResultTO result= MessagingServiceLocator.getInstance().getMercuryAPI().sendASKMessage(askTO, dbConnection);
                      if(!result.isSuccess())
                      {
                        sent=false;
                        this.sendSystemError(dbConnection, "Can't send Mercury ASK for order ["+askVO.getOrderDetailID()+"]. Error: "+result.getErrorString());
                      }
                  }else
                  {
                    logger.debug("could not create ASK message");
                    sent=false;
                  }
                }
            
            
        }finally
        {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting sendMessages");
            }
        }
        
        return sent;
    }

    
    
    
    
    private void sendSystemError(Connection dbConnection, String error) throws SAXException, ParserConfigurationException, IOException, SQLException, SystemMessengerException
    {
      sendSystemError(dbConnection, EmailConstants.SYS_MESS_SOURCE, error);
    }
    
    private void sendSystemError(Connection dbConnection, String source, String error) throws SAXException, ParserConfigurationException, IOException, SQLException, SystemMessengerException
    {
      if(logger.isDebugEnabled()){
               logger.debug("enter sendSystemError");
      } 
      try{

            
            SystemMessengerVO systemError = new SystemMessengerVO();
            systemError.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
            systemError.setMessage(error);
            systemError.setRead(true);
            systemError.setSource(source);
            systemError.setType(EmailConstants.SYS_MESS_TYPE_ERROR);
            SystemMessenger systemMessenger = SystemMessenger.getInstance();
            systemMessenger.send(systemError, dbConnection, false);           
            
            
        }finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting sendSystemError");
            } 
        }
      
    }    
    
    
    
    
   
    
    
}
