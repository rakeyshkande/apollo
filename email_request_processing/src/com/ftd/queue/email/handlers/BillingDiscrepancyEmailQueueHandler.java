package com.ftd.queue.email.handlers;

import java.io.IOException;
import java.sql.SQLException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.queue.email.EmailQueueHandler;
import com.ftd.queue.email.dao.EmailQueueDAO;
import com.ftd.queue.email.vo.EmailRequestVO;

/**
 * This class handles the processing for billing discrepancy e-mail requests
 * @todo check comments, error handling, and message handling
 * @author Charles Fox
 */
public class BillingDiscrepancyEmailQueueHandler extends EmailQueueHandler
{
    private Logger logger = 
        new Logger("com.ftd.queue.email.handlers.BillingDiscrepancyEmailQueueHandler");
    
    
    public BillingDiscrepancyEmailQueueHandler()
    {
    }

    /**
     * process the billing discrepancy request  
     * @param EmailRequestVO - emailVO
     * @return EmailRequestVO
     * @throws Exception
     * @todo update queue values
     * @todo value to send for AutoResponse E-mail
     */ 
    public void process(EmailRequestVO emailVO,EmailQueueDAO emailDAO)
        throws IOException, SAXException, SQLException, 
            ParserConfigurationException ,
            TransformerException,Exception
        {

        if(logger.isDebugEnabled()){
            logger.debug("Entering process Billing Discrepancy");
            logger.debug("emailVO : " + emailVO);
        }
        
        try{
        
            boolean process = this.continueProcess(emailVO, emailDAO);
            
             if(process){
                 /* send auto email */
                 this.queueRequestUpdateCommentsSendAutoResponse(emailVO, emailDAO, emailVO.getOrderDetailVO());
                
             }   
                     
    }finally
        {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting process Billing Discrepancy");
            }
        }                      
         
               
    }

  
}
