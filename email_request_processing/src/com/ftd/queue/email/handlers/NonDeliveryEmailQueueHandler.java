package com.ftd.queue.email.handlers;

import java.io.IOException;
import java.sql.SQLException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.queue.email.EmailQueueHandler;
import com.ftd.queue.email.constants.EmailConstants;
import com.ftd.queue.email.dao.EmailQueueDAO;
import com.ftd.queue.email.vo.EmailRequestVO;
import com.ftd.queue.email.vo.OrderDetailVO;

/**
 * This class handles the processing for non delivery e-mail requests
 * 
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 */
public class NonDeliveryEmailQueueHandler extends EmailQueueHandler {
    private Logger logger = new Logger(
            "com.ftd.queue.email.handlers.NonDeliveryEmailQueueHandler");

    public NonDeliveryEmailQueueHandler() {
    }

    /**
     * process the non delivery request
     * 
     * @param EmailRequestVO -
     *            emailVO
     * @return EmailRequestVO
     * @throws Exception
     * @todo update queue values
     * @todo value to send for AutoResponse E-mail
     */
    public void process(EmailRequestVO emailVO, EmailQueueDAO emailDAO)
            throws IOException, SAXException, SQLException,
            ParserConfigurationException , TransformerException,
            Exception {

        if (logger.isDebugEnabled()) {
            logger.debug("Entering process Non Delivery ");
            logger.debug("emailVO : " + emailVO);
        }

        try {

            boolean process = this.continueProcess(emailVO, emailDAO);
            String comments = "";
            if (process) {
                OrderDetailVO ordDetail = emailVO.getOrderDetailVO();
                /* put order in untagged product discrepancy queue */
                this.storeAttachedEmailQueue(emailVO, emailDAO);
                comments = retrieveOrderComments(EmailConstants.EMAIL_MSG_ND_QUEUE);
                this.insertComments(comments, ordDetail, emailDAO);

                if (this.isHolidayTime(emailVO.getTimeStamp())) {
                    this.sendAutoResponse(
                            EmailConstants.EMAIL_MSG_ND_QUEUE_HOLIDAY, emailVO,
                            emailDAO, true);
                } else {
                    this.sendAutoResponse(EmailConstants.EMAIL_MSG_ND_QUEUE,
                            emailVO, emailDAO, true);
                }

            }
        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting process");
            }
        }

       
    }
}
