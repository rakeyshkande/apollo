package com.ftd.queue.email.handlers;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Calendar;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import com.ftd.op.common.to.ResultTO;
import com.ftd.op.mercury.to.ASKMessageTO;
import com.ftd.op.venus.to.OrderDetailKeyTO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.queue.email.EmailQueueHandler;
import com.ftd.queue.email.constants.EmailConstants;
import com.ftd.queue.email.dao.EmailQueueDAO;
import com.ftd.queue.email.util.MessagingServiceLocator;
import com.ftd.queue.email.vo.EmailRequestVO;
import com.ftd.queue.email.vo.OrderDetailVO;

/**
 * This class handles the processing for modify order e-mail requests
 * 
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 */
public class ModifyOrderEmailQueueHandler extends EmailQueueHandler {

    private Logger logger = new Logger(
            "com.ftd.queue.email.handlers.ModifyOrderEmailQueueHandler");

    public ModifyOrderEmailQueueHandler() {
    }

    /**
     * process the modify order request
     * 
     * @param EmailRequestVO -
     *            emailVO
     * @return EmailRequestVO
     * @throws Exception
     * @todo Modify Order
     * @todo send ASK to florist
     * @todo update queue values
     * @todo value to send for AutoResponse E-mail
     * @todo update order
     * @todo send CAN to vendor
     * @todo send new order to vendor
     */
    public void process(EmailRequestVO emailVO, EmailQueueDAO emailDAO)
            throws IOException, SAXException, SQLException,
            ParserConfigurationException , TransformerException,
            Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering MO process");
            logger.debug("emailVO : " + emailVO.getOrderNumber());
        }

        try {

            boolean process = this.continueProcess(emailVO, emailDAO);
            String comments = "";
            if (process) {

                logger.debug("process MO request for " + emailVO);
                OrderDetailVO ordDetail = emailVO.getOrderDetailVO();
                if (ordDetail.isOrderOnHold()) {
                    logger.debug("order is on hold");
                    this.queueRequestUpdateCommentsSendAutoResponse(emailVO,
                            emailDAO, emailVO.getOrderDetailVO());
                } else {

                    processRequestForOrder(emailVO, emailDAO, ordDetail);
                }

            }
        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting MO process");
            }
        }

       
    }

    /**
     * @param emailVO
     * @param emailDAO
     * @param ordDetail
     * @throws IOException
     * @throws SAXException
     * @throws SQLException
     * @throws ParserConfigurationException
     * @throws XSLException
     * @throws Exception
     * @throws TransformerException
     *             void
     */
    private void processRequestForOrder(EmailRequestVO emailVO,
            EmailQueueDAO emailDAO, OrderDetailVO ordDetail)
            throws IOException, SAXException, SQLException,
            ParserConfigurationException , Exception,
            TransformerException {
        String comments = "";
        /**
         *If the Venus order on the communication screen was not created or verified or if the order was printed or shipped then 
         * Place the email in the �Untagged Modify Order� queue only if another untagged email isn�t in a higher priority queue � see rule 5.3 under Sorting of Novator customer related requests into the appropriate queues 
         * Send the auto response email (mo.queue) to the customer
         * Add a comment to the order comments screen that says: �Incoming modify order email request received.  Email has been queued.
         */
        
        if(ordDetail.isOrderDropship())
        {
          logger.debug("Dropship order");
          if(!ordDetail.hasLiveFTD()||ordDetail.isOrderPrintedOrShipped())
          {
            logger.debug("no attemped ftd or order is printed or shipped");
            this.queueRequestUpdateCommentsSendAutoResponse(emailVO, emailDAO, ordDetail);  
          }else
          {
            processModifyOrderRequest(emailVO, emailDAO);
          }
          
        }else if(ordDetail.isOrderFloral())
        {
          
            if (ordDetail.isPastDeliveryDate()) {
                logger.debug("past order delivery date");
                if(this.isAutoResponseNeeded(ordDetail)){
                    comments = retrieveOrderComments("mo.after.florist");
                }else
                {
                  comments = retrieveOrderComments("mo.noauto.response");
                }
                this.insertComments(comments, ordDetail, emailDAO);
                this.sendAutoResponse(EmailConstants.EMAIL_MSG_MO_AFTER, emailVO,
                        emailDAO, false);

            }else
            {
               if(ordDetail.hasLiveFTD())
               {
                 logger.debug("has liver FTD.");
                 if(ordDetail.isOrderFTDM())
                 {
                   logger.debug("order is FTDM.");
                   this.queueRequestUpdateCommentsSendAutoResponse(emailVO, emailDAO, ordDetail);
                   
                 }else
                 {
                   processModifyOrderRequest(emailVO, emailDAO);
                 }
                 
               }else
               {
                 logger.debug("order has no live FTD");
                 this.queueRequestUpdateCommentsSendAutoResponse(emailVO, emailDAO, ordDetail);
               }
              
            }
          
        }
               
       
    }

    void processModifyOrderRequest(EmailRequestVO emailVO,
            EmailQueueDAO emailDAO) throws  Exception {

        OrderDetailVO ordDetail = emailVO.getOrderDetailVO();

        if (!isOrderChangedForQueue(emailVO)
                && this.isOrderValidatablePartChanged(emailVO)) {
            logger.debug("isOrderChangedForQueue");
            String sessionId = "MO-" + this.hashCode() + "-"
                    + Calendar.getInstance().getTime().getTime();
            ResultTO result = new ResultTO();

            if (this.lockOrder(sessionId, emailDAO, ordDetail)) {
                this.modifyOrderDetail(emailVO, emailDAO);

                boolean msgSent = false;
                try{
                    if (ordDetail.isOrderDropship()) {
                        msgSent = this.sendVenusMessages(emailDAO, emailVO, ordDetail);
                    } else {
                        if (ordDetail.isOrderFloral()) {
                            ASKMessageTO askMessage = this.createASKMessageTO(
                                    EmailConstants.EMAIL_TYPE_MODIFY_ORDER,
                                    emailDAO, emailVO);
                            if (askMessage != null) {
                                result = this.sendMercuryASKMessage(askMessage, emailDAO.getDbConnection());
                                msgSent = result.isSuccess();
                            } else {
                                logger.debug("can not create an ASK TO");
                                this.queueRequestUpdateCommentsSendAutoResponse(
                                        emailVO, emailDAO, emailVO
                                                .getOrderDetailVO());
                            }
                        }
                    }
                }catch(Exception e)
                {
                  logger.error("cannot send Mercury/Venus message out.",e);
                  
                }finally
                {
                  this.releaseOrder(sessionId, emailDAO, ordDetail);
                }
                
                if (msgSent) {
                    this.sendAutoResponse(EmailConstants.EMAIL_MSG_MO_CHANGED,
                            emailVO, emailDAO, false);
                    String comments = retrieveOrderComments(EmailConstants.EMAIL_MSG_MO_CHANGED);
                    if(!this.isAutoResponseNeeded(ordDetail)){
                        retrieveOrderComments("mo.noauto.response");
                    }
                    this.insertComments(comments, ordDetail, emailDAO);
                } else {
                    logger.error("Messages can't be sent");
                    this.queueRequestUpdateCommentsSendAutoResponse(emailVO,
                            emailDAO, emailVO.getOrderDetailVO());
                }
            } else {
                logger.debug("Order is locked");
                this.queueRequestUpdateCommentsSendAutoResponse(emailVO,
                        emailDAO, emailVO.getOrderDetailVO());
            }
        } else {
            logger.debug("validatable part of order has not changed");
            this.queueRequestUpdateCommentsSendAutoResponse(emailVO, emailDAO,
                    emailVO.getOrderDetailVO());
        }

    }

    /**
     * Send CAN to vendor assigned to fulfill order Send new order to vendor
     * 
     * @param n/a
     * @return n/a
     * @throws Exception
     */
    public boolean sendVenusMessages(EmailQueueDAO emailDAO,
            EmailRequestVO emailVO, OrderDetailVO ordDetail) throws Exception {
        ResultTO result = new ResultTO();

        boolean canMsgSent = this.sendVenusCANMessage("OTH", emailDAO, emailVO);
        if (canMsgSent) {
            logger.debug("Venus CAN message sent. order detail id ["+ordDetail.getOrderDetailID()+"]");
            OrderDetailKeyTO orderDetailKeyTO = new OrderDetailKeyTO();
            orderDetailKeyTO.setOrderDetailId(ordDetail.getOrderDetailID());
            orderDetailKeyTO.setCsr(this.getSystemOperator());
            logger.debug("Start Send Venus FTD message. order detail ["+orderDetailKeyTO.getOrderDetailId()+"]");
            result = MessagingServiceLocator.getInstance().getVenusAPI()
                    .sendOrder(orderDetailKeyTO, emailDAO.getDbConnection());
        } else {
            logger.error("Venus CAN message can not be sent.");
            this.queueRequestUpdateCommentsSendAutoResponse(emailVO, emailDAO,
                    emailVO.getOrderDetailVO());
        }

        return result.isSuccess();
    }

}
