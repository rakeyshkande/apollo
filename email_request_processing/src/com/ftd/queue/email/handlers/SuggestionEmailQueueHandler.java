package com.ftd.queue.email.handlers;
import java.io.IOException;
import java.sql.SQLException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.queue.email.EmailQueueHandler;
import com.ftd.queue.email.dao.EmailQueueDAO;
import com.ftd.queue.email.vo.EmailRequestVO;

public class SuggestionEmailQueueHandler extends EmailQueueHandler
{
   private Logger logger = 
        new Logger("com.ftd.queue.email.handlers.SuggestionEmailQueueHandler");
  public SuggestionEmailQueueHandler()
  {
  }
  
  
  public void process(EmailRequestVO emailVO, EmailQueueDAO emailDAO)
        throws IOException, SAXException, SQLException, 
            ParserConfigurationException ,
            TransformerException,Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering SE process");
            logger.debug("emailVO : " + emailVO);
        }
        
        try{
        

            boolean process = this.continueProcess(emailVO, emailDAO);
            
             if(process){
                 /* send auto email */
                 this.queueRequestUpdateCommentsSendAutoResponse(emailVO, emailDAO, emailVO.getOrderDetailVO());
                
             }            
        }finally
        {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting process");
            }
        }                      
             
            
    }
}
