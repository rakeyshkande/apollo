package com.ftd.queue.email.handlers;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import com.ftd.ftdutilities.GeneralConstants;
import com.ftd.op.common.to.ResultTO;
import com.ftd.op.mercury.to.ASKMessageTO;
import com.ftd.osp.utilities.DeliveryInquiryUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.queue.email.EmailQueueHandler;
import com.ftd.queue.email.constants.EmailConstants;
import com.ftd.queue.email.dao.EmailQueueDAO;
import com.ftd.queue.email.vo.ASKEventHandlerVO;
import com.ftd.queue.email.vo.EmailRequestVO;
import com.ftd.queue.email.vo.OrderDetailVO;

/**
 * This class handles the processing for delivery inquiry e-mail requests
 * 
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 */
public class DeliveryInquiryEmailQueueHandler extends EmailQueueHandler {
    private Logger logger = new Logger(
            "com.ftd.queue.email.handlers.DeliveryInquiryEmailQueueHandler");

    public DeliveryInquiryEmailQueueHandler() {
    }

    /**
     * process the delivery inquiry request
     * 
     * @param EmailRequestVO -
     *            emailVO
     * @return EmailRequestVO
     * @throws Exception
     * @todo event handling framework code to send ASK message
     * @todo update queue values
     * @todo value to send for AutoResponse E-mail
     */
    public void process(EmailRequestVO emailVO, EmailQueueDAO emailDAO)
            throws IOException, SAXException, SQLException,
            ParserConfigurationException , TransformerException,
            ParseException, Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering DI process");
            logger.debug("emailVO : " + emailVO.getOrderNumber());
        }

        try {

            boolean process = this.continueProcess(emailVO, emailDAO);
            String comments = "";
            if (process) {
                OrderDetailVO ordDetail = emailVO.getOrderDetailVO();
                if (ordDetail.hasLiveFTD()) {
                    logger.debug("has live FTD");
                    if (ordDetail.isOrderFloral()) {
                        logger.debug(" order is floral");
                        
                        if (ordDetail.isOrderFTDM()) {
                            /* FTDM order */
                            logger.debug("FTDM order");
                            comments = retrieveOrderComments("di.queue.ftdm");
                            this.insertComments(comments, ordDetail, emailDAO);
                            this.storeAttachedEmailQueue(emailVO, emailDAO);
                            this.sendAutoResponse(
                                    EmailConstants.EMAIL_MSG_DI_QUEUE,
                                    emailVO, emailDAO, true);

                       }else{
                        
                            if (ordDetail.isPastDeliveryDate()) {
                                logger.debug("passed delivery date");
                                processDeliveryInquiryPastDeliveryDate(emailVO,
                                        ordDetail, emailDAO);
                            } else {
    
                                logger.debug("not past delivery date.");
                                /* order not past delivery date */
                                processDeliveryInquiryBeforeDeliveryDate(emailVO,
                                        ordDetail, emailDAO);
                            }
                       }
                    } else if (ordDetail.isOrderDropship()) {
                          
                          processRequestForDropshipOrder(emailDAO, emailVO, ordDetail);

                    } 
                } else {
                    logger.debug("no live FTD.");
                    this.queueRequestUpdateCommentsSendAutoResponse(emailVO,
                            emailDAO, ordDetail);
                }
            }

        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting process");
            }
        }

       
    }

  void processRequestForDropshipOrder( EmailQueueDAO emailDAO, EmailRequestVO emailVO, OrderDetailVO ordDetail) throws IOException, ParserConfigurationException, Exception, ParseException, SAXException, TransformerException, SQLException 
  {
     String comments="";
    if (ordDetail.isOrderPrintedOrShipped())
    {
      logger.debug("order is printed or shipped.");
      processOrderIsPrintedOrShipped(emailVO, ordDetail, emailDAO);
    }
    else
    {
      logger.debug("order is not printed or shipped.");
      if (ordDetail.isTrackingAvailable())
      {
        logger.debug("order is TrackingAvailable.");
        comments = retrieveOrderComments(EmailConstants.EMAIL_MSG_DI_TRACK);
        if(!this.isAutoResponseNeeded(ordDetail))
        {
          comments = retrieveOrderComments("di.noauto.response");
        }
        this.insertComments(comments, ordDetail, emailDAO);
        this.sendAutoResponse(EmailConstants.EMAIL_MSG_DI_TRACK, emailVO, emailDAO, false);
      }
      else
      {
        logger.debug("order is not TrackingAvailable.");
        comments = retrieveOrderComments("di.before.vendor");
        this.insertComments(comments, ordDetail, emailDAO);
        //this.storeAttachedEmailQueue(emailVO, emailDAO);
        this.sendAutoResponse(EmailConstants.EMAIL_MSG_DI_BEFORE, emailVO, emailDAO, true);
      }
    }
    
  }

    void processOrderIsPrintedOrShipped(EmailRequestVO emailVO,
            OrderDetailVO ordDetail, EmailQueueDAO emailDAO)
            throws ParseException, SAXException, TransformerException,
            ParserConfigurationException, Exception, SQLException,
             IOException {
        String comments = "";
        if (ordDetail.isTrackingAvailable()) {
            logger.debug("isTrackingAvailable");
            comments = retrieveOrderComments(EmailConstants.EMAIL_MSG_DI_TRACK);
            
            this.sendAutoResponse(EmailConstants.EMAIL_MSG_DI_TRACK, emailVO,
                    emailDAO, false);
            if(!isAutoResponseNeeded(ordDetail))
            {
              comments = retrieveOrderComments("di.noauto.response");
            }
            this.insertComments(comments, ordDetail, emailDAO);
        } else {
            this.storeAttachedEmailQueue(emailVO, emailDAO);
            if (this.isHolidayTime(emailVO.getTimeStamp())) {
                comments = retrieveOrderComments("di.after.holiday.vendor");
                this.insertComments(comments, ordDetail, emailDAO);

                this.sendAutoResponse(
                        EmailConstants.EMAIL_MSG_DI_AFTER_HOLIDAY, emailVO,
                        emailDAO, true);
            } else {
                comments = retrieveOrderComments("di.after.vendor");
                this.insertComments(comments, ordDetail, emailDAO);
                this.sendAutoResponse(EmailConstants.EMAIL_MSG_DI_AFTER,
                        emailVO, emailDAO, true);
            }
        }

    }

    void processDeliveryInquiryBeforeDeliveryDate(EmailRequestVO emailVO,
            OrderDetailVO ordDetail, EmailQueueDAO emailDAO)
            throws SAXException, TransformerException,
            ParserConfigurationException, Exception, SQLException,
             IOException {
            
              String comments = retrieveOrderComments("di.before.florist");
              if(!this.isAutoResponseNeeded(ordDetail))
              {
                comments = retrieveOrderComments("di.noauto.response");
              }
              this.insertComments(comments, ordDetail, emailDAO);
              this.sendAutoResponse(EmailConstants.EMAIL_MSG_DI_BEFORE,
                            emailVO, emailDAO, false);
    }

    void processDeliveryInquiryPastDeliveryDate(EmailRequestVO emailVO,
            OrderDetailVO ordDetail, EmailQueueDAO emailDAO)
            throws ParseException, SAXException, TransformerException,
            ParserConfigurationException, Exception, SQLException,
             IOException {
        String comments = "";
        String dconStatus = null;
        DeliveryInquiryUtil diu = null;
        logger.debug("has live ftd");
        
        dconStatus = emailDAO.getDeliveryConfirmationStatus(Long.parseLong(ordDetail.getOrderDetailID()));
        if (dconStatus == null)
        {
            dconStatus = "";
        }
        logger.debug("dconStatus is " + dconStatus );
        if (dconStatus == "" || dconStatus.equals(GeneralConstants.DCON_PENDING))
        {
          diu = new DeliveryInquiryUtil(emailDAO.getDbConnection());
          if (diu.sendASKMessage(ordDetail.getOrderDetailID())) {
	          ASKMessageTO askMessage = this.createASKMessageTO(
	                      EmailConstants.EMAIL_TYPE_DELIVERY_INQUIRY, emailDAO,
	                      emailVO);
	          if (askMessage != null) {
	            try{
	                  
	            ResultTO result = this.sendMercuryASKMessage(askMessage, emailDAO.getDbConnection());
	            if (result.isSuccess()) {
	              ASKEventHandlerVO askEvent = new ASKEventHandlerVO();
	              askEvent.setFillingFloristId(ordDetail.getFillingFloristId());
	              askEvent.setOrderDetailID(ordDetail.getOrderDetailID());
	              askEvent.setMessageType(EmailConstants.SYSTEM_TYPE_MERCURY);
	              askEvent.setPointOfContactId(emailVO.getPointOfContactId());
	              askEvent.setQueueType(emailVO.getRequestType());
	              askEvent.setSendEmail(emailVO.getEmail());
	              askEvent.setMercuryMessageId(result.getKey());
	              this.logMessageTransaction(askEvent, emailDAO);
	              if (this.isHolidayTime(emailVO.getTimeStamp())) {
	                  logger.debug("is holiday");
	                  comments = retrieveOrderComments("di.after.holiday.florist");
	                  this.insertComments(comments, ordDetail, emailDAO);
	                  this.sendAutoResponse(EmailConstants.EMAIL_MSG_DI_AFTER_HOLIDAY,
	                          emailVO, emailDAO, true);
	              } else {
	      
	                  comments = retrieveOrderComments("di.after.florist");
	                  this.insertComments(comments, ordDetail, emailDAO);
	                  this.sendAutoResponse(EmailConstants.EMAIL_MSG_DI_AFTER, emailVO,
	                          emailDAO, true);
	              }
	  
	              this.storeAttachedEmailQueue(emailVO, emailDAO);
	              
	              if (dconStatus.equals(GeneralConstants.DCON_PENDING))
	              {
	                  emailDAO.updateDeliveryConfirmationStatus(Long.parseLong(ordDetail.getOrderDetailID()),GeneralConstants.DCON_SENT, "DI_SYS");
	              }
	  
	            } else {
	              logger.error("Can't send Mercury ASK message. Error: "
	                  + result.getErrorString());
	                  this.queueRequestUpdateCommentsSendAutoResponse(emailVO,
	                              emailDAO, ordDetail);
	  
	            }
	          }catch(Exception e)
	          {
	            logger.error("Can't send Mercury ASK message. Error: ",e);
	            this.queueRequestUpdateCommentsSendAutoResponse(emailVO,
	                              emailDAO, ordDetail);
	          }
	        } else {
	          logger.error("Can't create Mercury ASK message. ");
	          this.queueRequestUpdateCommentsSendAutoResponse(emailVO,
	                          emailDAO, ordDetail);
	        }
        }
      }
    }
}
