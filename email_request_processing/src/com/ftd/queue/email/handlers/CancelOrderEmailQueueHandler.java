package com.ftd.queue.email.handlers;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Calendar;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.commons.lang.StringUtils;
import org.xml.sax.SAXException;

import com.ftd.customerordermanagement.bo.RefundAPIBO;
import com.ftd.customerordermanagement.bo.RefundBO;
import com.ftd.op.common.to.ResultTO;
import com.ftd.op.mercury.to.CANMessageTO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.EJBServiceLocator;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.queue.email.EmailQueueHandler;
import com.ftd.queue.email.constants.EmailConstants;
import com.ftd.queue.email.dao.AutoRefundDAO;
import com.ftd.queue.email.dao.EmailQueueDAO;
import com.ftd.queue.email.util.DataSourceHelper;
import com.ftd.queue.email.vo.EmailRequestVO;
import com.ftd.queue.email.vo.OrderDetailVO;
/**
 * This class handles the processing for cancel order e-mail requests
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 */
public class CancelOrderEmailQueueHandler extends EmailQueueHandler
{
    private Logger logger = 
        new Logger("com.ftd.queue.email.handlers.CancelOrderEmailQueueHandler");
    private static String initialContextStr = null;
    private static String ejbProviderUrl = null;
    
    
    public CancelOrderEmailQueueHandler()
    {
    }
    
    /**
     * process the cancel order request  
     * @param EmailRequestVO - emailVO
     * @return EmailRequestVO
     * @throws Exception
     * @todo send CAN to florist
     * @todo update queue values
     * @todo value to send for AutoResponse E-mail
     */ 
    public void process(EmailRequestVO emailVO, EmailQueueDAO emailDAO)
        throws IOException, SAXException, SQLException, 
            ParserConfigurationException ,TransformerException,
            Exception{
        if(logger.isDebugEnabled()){
            logger.debug("Entering cancel process");
            logger.debug("emailVO : " + emailVO.getOrderNumber());
        }
        boolean process = false;
        OrderDetailVO ordDetail = null;
        String sessionId = "CX-" + this.hashCode() + "-" + Calendar.getInstance().getTime().getTime();
        try{
            String comments="";
            
            process = this.continueProcess(emailVO, emailDAO);
            ordDetail = emailVO.getOrderDetailVO();  // This has to be after continueProcess call
            
            if(process){
                    logger.debug("process cancel request for "+emailVO);
                    
                    if (this.lockOrder(sessionId, emailDAO, ordDetail)) {
                        processCancelRequest(comments, emailVO, emailDAO);
                    } else {
                        logger.debug("Order is locked");
                        this.queueRequestUpdateCommentsSendAutoResponse(emailVO,
                        emailDAO, emailVO.getOrderDetailVO());
                    }
            }
        }finally
        {
            if (process) {
                this.releaseOrder(sessionId, emailDAO, ordDetail);
            }
            if(logger.isDebugEnabled()){
               logger.debug("Exiting process");
            }
        }                      
         
          
    }

    

  void processCancelRequest(String comments, EmailRequestVO emailVO, EmailQueueDAO emailDAO) throws IOException, TransformerException , SAXException, SQLException, Exception, java.text.ParseException, ParserConfigurationException
  {
    
    OrderDetailVO ordDetail=emailVO.getOrderDetailVO();
    if(ordDetail.isOrderFTDM())
    {
      logger.debug("order is FTDM");
        this.storeAttachedEmailQueue(emailVO, emailDAO);
        comments = retrieveOrderComments(EmailConstants.EMAIL_MSG_CX_QUEUE);
        this.insertComments(comments,ordDetail, emailDAO);
        this.sendAutoResponse(EmailConstants.EMAIL_MSG_CX_QUEUE, emailVO, emailDAO, true);
    }else{
    
          /*
           * If the FTD has not been created or verified then
           * Place the email in the �Untagged Cancel� queue
           * Send the auto response email (cx.queue) to customer
           * Add a comment to the order comments screen that says: �Incoming cancel email request received.  Email has been queued.�
           */
          if(!ordDetail.hasAttemptedFTD()){//FTD is not created
          
                  this.queueRequestUpdateCommentsSendAutoResponse(emailVO, emailDAO, ordDetail);
                  
          }else{  
                  String ftdStatus="";
                  if(StringUtils.isNotEmpty(ordDetail.getFtdStatus()))
                  {
                     ftdStatus=ordDetail.getFtdStatus();
                  }
                  
                  logger.debug("FTD status=["+ftdStatus+"]");
                 
                  /*
                   * 	If the order has already been cancelled, rejected, or in an ERR state.
                   *	The email will not be placed in the queue.  
                   *	Send the auto response email (cx.cancelled) to the customer
                   *	Add a comment to the order comments that says:  �Incoming cancel email request received.  Order already cancelled or rejected. Email not queued.�
                   */
                  if (ftdStatus.equalsIgnoreCase("ME")||ftdStatus.equalsIgnoreCase("MR"))//error state
                  {
                    logger.debug("order in ERR state");
                     processCancelledOrder(emailDAO, emailVO, ordDetail);
                    
                  } else if(ftdStatus.equalsIgnoreCase("MO"))//open, not verified.
                  {
                        this.queueRequestUpdateCommentsSendAutoResponse(emailVO, emailDAO, ordDetail);    
                   
                   }else{
                        if(ordDetail.isOrderCancelledOrRejected())
                        {
                          logger.debug("order has been cancelled or rejected or in ERR state");
                          processCancelledOrder(emailDAO, emailVO, ordDetail);
                        } else if(ordDetail.hasAttemptedCAN()) {
                          logger.debug("order already in process of being cancelled");
                          processCancelledOrder(emailDAO, emailVO, ordDetail);
                        }else{
                          // Cancel (and possibly auto-refund order)
                          processVendorFloralOrderCancelRequest(comments, emailVO, emailDAO);
                        }
            
                   }
                  
          }
    }
  }

  void processCancelledOrder( EmailQueueDAO emailDAO, EmailRequestVO emailVO, OrderDetailVO ordDetail) throws ParserConfigurationException, IOException, Exception, SAXException, TransformerException, SQLException 
  {
    this.sendAutoResponse(EmailConstants.EMAIL_MSG_CX_CANCELLED, emailVO, emailDAO, false);
    String comments = retrieveOrderComments(EmailConstants.EMAIL_MSG_CX_CANCELLED);
    if(!isAutoResponseNeeded(ordDetail))
    {
      comments = retrieveOrderComments("cx.noauto.response");
    }
    this.insertComments(comments, ordDetail, emailDAO);
    
  }

  void processVendorFloralOrderCancelRequest(String comments, EmailRequestVO emailVO, EmailQueueDAO emailDAO) throws IOException, TransformerException , SAXException, SQLException, Exception, java.text.ParseException, ParserConfigurationException
  {
    logger.debug("order is not FTDM");
    OrderDetailVO ordDetail=emailVO.getOrderDetailVO();
    boolean sendCAN = false;
    boolean messageSent = false;
    boolean queueIt = true;
    if (ordDetail.isOrderFloral())
    {
      logger.debug("order is floral");
      if (ordDetail.isPastDeliveryDate())
      {
        logger.debug("order past delivery date");
        comments = retrieveOrderComments("cx.after.florist");
        this.insertComments(comments, ordDetail, emailDAO);
        this.sendAutoResponse(EmailConstants.EMAIL_MSG_CX_AFTER, emailVO, emailDAO, false);
      }
      else
      {
        sendCAN = true;
      }
    }
    else
    {
      logger.debug("order is drop ship");
      if (ordDetail.isOrderPrintedOrShipped())
      {
        logger.debug("order is printed");
        comments = retrieveOrderComments("cx.after.vendor");
        this.insertComments(comments, ordDetail, emailDAO);
        this.sendAutoResponse(EmailConstants.EMAIL_MSG_CX_AFTER, emailVO, emailDAO, false);
      }
      else
      {
        sendCAN = true;
      }
    }
    if (sendCAN)
    {
      if (ordDetail.isOrderDropship())
      {
        messageSent = this.sendVenusCANMessage("CON", emailDAO, emailVO);
      }
      else
      {
        if (ordDetail.isOrderFloral())
        {
          CANMessageTO canMessage=this.createCANMessageTO(emailDAO, emailVO);
          if(canMessage!=null){
            messageSent = this.sendMercuryCancelMessage(canMessage, emailDAO.getDbConnection());
          }
        }
      }
      if (messageSent)
      {
        logger.debug("CAN message has been sent for order " + ordDetail.getOrderDetailID());
        String emailCommentKey = EmailConstants.EMAIL_MSG_CX_AUTOCANCEL;

        // If allowed, automatically process refund via COM EJB
        AutoRefundDAO ar = new AutoRefundDAO(emailDAO.getDbConnection());
        if (ar.isAutoRefundAllowed(ordDetail.getOrderDetailID())) {
          try {
			logger.info("call to postFullRemainingRefund...");
			postFullRemainingRefund(ordDetail.getOrderDetailID());
			logger.info("postFullRemainingRefund succeeded");
			// Note the two vars below were defaulted above and only changed here since success
			queueIt = false;  // No need to queue since refund was successful
			emailCommentKey = EmailConstants.EMAIL_MSG_CX_AUTOCANCELREFUND;

          } catch (Exception e) {
              logger.error("postFullRemainingRefund call failed: " + e);
          }
        } 
        comments = retrieveOrderComments(emailCommentKey);
        this.insertComments(comments, ordDetail, emailDAO);
        this.sendAutoResponse(emailCommentKey, emailVO, emailDAO, true);
      }
      else
      {
        
        this.sendAutoResponse(EmailConstants.EMAIL_MSG_CX_QUEUE, emailVO, emailDAO, true);
      }
      
      if (queueIt) {
        //Put order in �Untagged Cancel� queue so rep can refund
        storeAttachedEmailQueue(emailVO, emailDAO);
      }
    }
   
  }

  /** Method to issue a refund on the given order detail ID.
   * 
   * @param a_orderDetailId - the order detail ID for the order detail for which we want to issue a refund
   * @throws Exception - in case the refund request fails an exception will be thrown
   */
  private void postFullRemainingRefund(String a_orderDetailId) throws Exception
  {
	  Connection conn = DataSourceHelper.createDatabaseConnection();
	  RefundAPIBO refundAPI = new RefundAPIBO();
	  try {
		refundAPI.postFullRemainingRefund(a_orderDetailId, //order detail id
							    		EmailConstants.USER_REFUND_DISP_CODE, //disposition code
							    		null,	//responsible party
							    		EmailConstants.USER_REFUND_STATUS, //refund status
							    		EmailConstants.USER_REFUND_USER_ID, //userid
							    		conn); // database connection
	} finally {
		conn.close();
	}
  }
}
