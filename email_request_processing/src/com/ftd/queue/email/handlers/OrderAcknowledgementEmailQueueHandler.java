package com.ftd.queue.email.handlers;

import java.io.IOException;
import java.sql.SQLException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.queue.email.EmailQueueHandler;
import com.ftd.queue.email.constants.EmailConstants;
import com.ftd.queue.email.dao.EmailQueueDAO;
import com.ftd.queue.email.vo.EmailRequestVO;
import com.ftd.queue.email.vo.OrderDetailVO;

/**
 * This class handles the processing for order acknowledgement e-mail requests
 * 
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 */
public class OrderAcknowledgementEmailQueueHandler extends EmailQueueHandler {
    private Logger logger = new Logger(
            "com.ftd.queue.email.handlers.OrderAcknowledgementEmailQueueHandler");

    public OrderAcknowledgementEmailQueueHandler() {
    }

    /**
     * process the order acknowledgement request
     * 
     * @param EmailRequestVO -
     *            emailVO
     * @return EmailRequestVO
     * @throws Exception
     * @todo update queue values
     * @todo value to send for AutoResponse E-mail
     */
    public void process(EmailRequestVO emailVO, EmailQueueDAO emailDAO)
            throws IOException, SAXException, SQLException,
            ParserConfigurationException , TransformerException,
            Exception {

        if (logger.isDebugEnabled()) {
            logger
                    .debug("Entering process for OrderAcknowledgementEmailQueueHandler");
            logger.debug("emailVO : " + emailVO.getOrderNumber());
        }

        try {

            boolean process = this.continueProcess(emailVO, emailDAO);

            if (process) {
                /* send auto email */

                /* order not in scrub */
                logger.debug("order is not in scrub.");
                String comments = retrieveOrderComments(EmailConstants.EMAIL_MSG_OA_FOUND);
                OrderDetailVO ordDetailVO=emailVO.getOrderDetailVO();
                if(!this.isAutoResponseNeeded(ordDetailVO))
                {
                  comments = retrieveOrderComments("oa.noauto.response");
                }
                this.insertComments(comments,ordDetailVO ,
                        emailDAO);
                this.sendAutoResponse(EmailConstants.EMAIL_MSG_OA_FOUND,
                        emailVO, emailDAO, false);

            }
        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting process");
            }
        }

    }
}
