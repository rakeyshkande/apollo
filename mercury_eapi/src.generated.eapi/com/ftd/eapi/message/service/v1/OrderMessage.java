
package com.ftd.eapi.message.service.v1;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for orderMessage complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="orderMessage">
 *   &lt;complexContent>
 *     &lt;extension base="{http://eapi.ftd.com/message/service/v1/}erosMessage">
 *       &lt;sequence>
 *         &lt;element name="originalOrderSequenceNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="originalOrderReferenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cardMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="conventionMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="deliveryDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="deliveryDetails" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="firstChoiceProductCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="firstChoiceProductDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="occasionCode" type="{http://eapi.ftd.com/message/service/v1/}occasionCode" minOccurs="0"/>
 *         &lt;element name="price" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="recipientCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="recipientCityStateZip" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="recipientCountryCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="recipientName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="recipientPhoneNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="recipientPostalCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="recipientStateCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="recipientStreetAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="secondChoiceProductCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="secondChoiceProductDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="resend" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="specialInstructions" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="bypassSuspends" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="allowAutoForward" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="allowFillerForward" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="gotoForwardOnly" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="tagLine" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "orderMessage", propOrder = {
    "originalOrderSequenceNumber",
    "originalOrderReferenceNumber",
    "cardMessage",
    "conventionMessage",
    "deliveryDate",
    "deliveryDetails",
    "firstChoiceProductCode",
    "firstChoiceProductDescription",
    "occasionCode",
    "price",
    "recipientCity",
    "recipientCityStateZip",
    "recipientCountryCode",
    "recipientName",
    "recipientPhoneNumber",
    "recipientPostalCode",
    "recipientStateCode",
    "recipientStreetAddress",
    "secondChoiceProductCode",
    "secondChoiceProductDescription",
    "resend",
    "specialInstructions",
    "bypassSuspends",
    "allowAutoForward",
    "allowFillerForward",
    "gotoForwardOnly",
    "tagLine"
})
public class OrderMessage
    extends ErosMessage
{

    protected Integer originalOrderSequenceNumber;
    protected String originalOrderReferenceNumber;
    protected String cardMessage;
    protected String conventionMessage;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar deliveryDate;
    protected String deliveryDetails;
    protected String firstChoiceProductCode;
    protected String firstChoiceProductDescription;
    protected OccasionCode occasionCode;
    protected BigDecimal price;
    protected String recipientCity;
    protected String recipientCityStateZip;
    protected String recipientCountryCode;
    protected String recipientName;
    protected String recipientPhoneNumber;
    protected String recipientPostalCode;
    protected String recipientStateCode;
    protected String recipientStreetAddress;
    protected String secondChoiceProductCode;
    protected String secondChoiceProductDescription;
    protected Boolean resend;
    protected String specialInstructions;
    protected Boolean bypassSuspends;
    protected Boolean allowAutoForward;
    protected Boolean allowFillerForward;
    protected Boolean gotoForwardOnly;
    protected String tagLine;

    /**
     * Gets the value of the originalOrderSequenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOriginalOrderSequenceNumber() {
        return originalOrderSequenceNumber;
    }

    /**
     * Sets the value of the originalOrderSequenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOriginalOrderSequenceNumber(Integer value) {
        this.originalOrderSequenceNumber = value;
    }

    /**
     * Gets the value of the originalOrderReferenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginalOrderReferenceNumber() {
        return originalOrderReferenceNumber;
    }

    /**
     * Sets the value of the originalOrderReferenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginalOrderReferenceNumber(String value) {
        this.originalOrderReferenceNumber = value;
    }

    /**
     * Gets the value of the cardMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardMessage() {
        return cardMessage;
    }

    /**
     * Sets the value of the cardMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardMessage(String value) {
        this.cardMessage = value;
    }

    /**
     * Gets the value of the conventionMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConventionMessage() {
        return conventionMessage;
    }

    /**
     * Sets the value of the conventionMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConventionMessage(String value) {
        this.conventionMessage = value;
    }

    /**
     * Gets the value of the deliveryDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDeliveryDate() {
        return deliveryDate;
    }

    /**
     * Sets the value of the deliveryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDeliveryDate(XMLGregorianCalendar value) {
        this.deliveryDate = value;
    }

    /**
     * Gets the value of the deliveryDetails property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeliveryDetails() {
        return deliveryDetails;
    }

    /**
     * Sets the value of the deliveryDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeliveryDetails(String value) {
        this.deliveryDetails = value;
    }

    /**
     * Gets the value of the firstChoiceProductCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstChoiceProductCode() {
        return firstChoiceProductCode;
    }

    /**
     * Sets the value of the firstChoiceProductCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstChoiceProductCode(String value) {
        this.firstChoiceProductCode = value;
    }

    /**
     * Gets the value of the firstChoiceProductDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstChoiceProductDescription() {
        return firstChoiceProductDescription;
    }

    /**
     * Sets the value of the firstChoiceProductDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstChoiceProductDescription(String value) {
        this.firstChoiceProductDescription = value;
    }

    /**
     * Gets the value of the occasionCode property.
     * 
     * @return
     *     possible object is
     *     {@link OccasionCode }
     *     
     */
    public OccasionCode getOccasionCode() {
        return occasionCode;
    }

    /**
     * Sets the value of the occasionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link OccasionCode }
     *     
     */
    public void setOccasionCode(OccasionCode value) {
        this.occasionCode = value;
    }

    /**
     * Gets the value of the price property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * Sets the value of the price property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPrice(BigDecimal value) {
        this.price = value;
    }

    /**
     * Gets the value of the recipientCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecipientCity() {
        return recipientCity;
    }

    /**
     * Sets the value of the recipientCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecipientCity(String value) {
        this.recipientCity = value;
    }

    /**
     * Gets the value of the recipientCityStateZip property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecipientCityStateZip() {
        return recipientCityStateZip;
    }

    /**
     * Sets the value of the recipientCityStateZip property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecipientCityStateZip(String value) {
        this.recipientCityStateZip = value;
    }

    /**
     * Gets the value of the recipientCountryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecipientCountryCode() {
        return recipientCountryCode;
    }

    /**
     * Sets the value of the recipientCountryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecipientCountryCode(String value) {
        this.recipientCountryCode = value;
    }

    /**
     * Gets the value of the recipientName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecipientName() {
        return recipientName;
    }

    /**
     * Sets the value of the recipientName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecipientName(String value) {
        this.recipientName = value;
    }

    /**
     * Gets the value of the recipientPhoneNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecipientPhoneNumber() {
        return recipientPhoneNumber;
    }

    /**
     * Sets the value of the recipientPhoneNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecipientPhoneNumber(String value) {
        this.recipientPhoneNumber = value;
    }

    /**
     * Gets the value of the recipientPostalCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecipientPostalCode() {
        return recipientPostalCode;
    }

    /**
     * Sets the value of the recipientPostalCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecipientPostalCode(String value) {
        this.recipientPostalCode = value;
    }

    /**
     * Gets the value of the recipientStateCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecipientStateCode() {
        return recipientStateCode;
    }

    /**
     * Sets the value of the recipientStateCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecipientStateCode(String value) {
        this.recipientStateCode = value;
    }

    /**
     * Gets the value of the recipientStreetAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecipientStreetAddress() {
        return recipientStreetAddress;
    }

    /**
     * Sets the value of the recipientStreetAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecipientStreetAddress(String value) {
        this.recipientStreetAddress = value;
    }

    /**
     * Gets the value of the secondChoiceProductCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecondChoiceProductCode() {
        return secondChoiceProductCode;
    }

    /**
     * Sets the value of the secondChoiceProductCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecondChoiceProductCode(String value) {
        this.secondChoiceProductCode = value;
    }

    /**
     * Gets the value of the secondChoiceProductDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecondChoiceProductDescription() {
        return secondChoiceProductDescription;
    }

    /**
     * Sets the value of the secondChoiceProductDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecondChoiceProductDescription(String value) {
        this.secondChoiceProductDescription = value;
    }

    /**
     * Gets the value of the resend property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isResend() {
        return resend;
    }

    /**
     * Sets the value of the resend property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setResend(Boolean value) {
        this.resend = value;
    }

    /**
     * Gets the value of the specialInstructions property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpecialInstructions() {
        return specialInstructions;
    }

    /**
     * Sets the value of the specialInstructions property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpecialInstructions(String value) {
        this.specialInstructions = value;
    }

    /**
     * Gets the value of the bypassSuspends property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBypassSuspends() {
        return bypassSuspends;
    }

    /**
     * Sets the value of the bypassSuspends property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBypassSuspends(Boolean value) {
        this.bypassSuspends = value;
    }

    /**
     * Gets the value of the allowAutoForward property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowAutoForward() {
        return allowAutoForward;
    }

    /**
     * Sets the value of the allowAutoForward property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowAutoForward(Boolean value) {
        this.allowAutoForward = value;
    }

    /**
     * Gets the value of the allowFillerForward property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowFillerForward() {
        return allowFillerForward;
    }

    /**
     * Sets the value of the allowFillerForward property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowFillerForward(Boolean value) {
        this.allowFillerForward = value;
    }

    /**
     * Gets the value of the gotoForwardOnly property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isGotoForwardOnly() {
        return gotoForwardOnly;
    }

    /**
     * Sets the value of the gotoForwardOnly property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setGotoForwardOnly(Boolean value) {
        this.gotoForwardOnly = value;
    }

    /**
     * Gets the value of the tagLine property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTagLine() {
        return tagLine;
    }

    /**
     * Sets the value of the tagLine property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTagLine(String value) {
        this.tagLine = value;
    }

}
