
package com.ftd.eapi.message.service.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for messageDetailResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="messageDetailResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="singleMessageResponse" type="{http://eapi.ftd.com/message/service/v1/}singleMessageResponse"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "messageDetailResponse", propOrder = {
    "singleMessageResponse"
})
public class MessageDetailResponse {

    @XmlElement(required = true)
    protected SingleMessageResponse singleMessageResponse;

    /**
     * Gets the value of the singleMessageResponse property.
     * 
     * @return
     *     possible object is
     *     {@link SingleMessageResponse }
     *     
     */
    public SingleMessageResponse getSingleMessageResponse() {
        return singleMessageResponse;
    }

    /**
     * Sets the value of the singleMessageResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link SingleMessageResponse }
     *     
     */
    public void setSingleMessageResponse(SingleMessageResponse value) {
        this.singleMessageResponse = value;
    }

}
