
package com.ftd.eapi.message.service.v1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for occasionCode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="occasionCode">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="FUNERAL"/>
 *     &lt;enumeration value="GET_WELL"/>
 *     &lt;enumeration value="BIRTHDAY"/>
 *     &lt;enumeration value="BUSINESS"/>
 *     &lt;enumeration value="HOLIDAY"/>
 *     &lt;enumeration value="MATERNITY"/>
 *     &lt;enumeration value="ANNIVERSARY"/>
 *     &lt;enumeration value="OTHER"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "occasionCode")
@XmlEnum
public enum OccasionCode {

    FUNERAL,
    GET_WELL,
    BIRTHDAY,
    BUSINESS,
    HOLIDAY,
    MATERNITY,
    ANNIVERSARY,
    OTHER;

    public String value() {
        return name();
    }

    public static OccasionCode fromValue(String v) {
        return valueOf(v);
    }

}
