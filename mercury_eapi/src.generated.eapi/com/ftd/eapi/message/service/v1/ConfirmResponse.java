
package com.ftd.eapi.message.service.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for confirmResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="confirmResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="simpleResponse" type="{http://eapi.ftd.com/message/service/v1/}simpleResponse" maxOccurs="unbounded"/>
 *         &lt;element name="responseCode" type="{http://eapi.ftd.com/message/service/v1/}codeMessagePair" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "confirmResponse", propOrder = {
    "simpleResponse",
    "responseCode"
})
public class ConfirmResponse {

    @XmlElement(required = true)
    protected List<SimpleResponse> simpleResponse;
    protected CodeMessagePair responseCode;

    /**
     * Gets the value of the simpleResponse property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the simpleResponse property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSimpleResponse().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SimpleResponse }
     * 
     * 
     */
    public List<SimpleResponse> getSimpleResponse() {
        if (simpleResponse == null) {
            simpleResponse = new ArrayList<SimpleResponse>();
        }
        return this.simpleResponse;
    }

    /**
     * Gets the value of the responseCode property.
     * 
     * @return
     *     possible object is
     *     {@link CodeMessagePair }
     *     
     */
    public CodeMessagePair getResponseCode() {
        return responseCode;
    }

    /**
     * Sets the value of the responseCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeMessagePair }
     *     
     */
    public void setResponseCode(CodeMessagePair value) {
        this.responseCode = value;
    }

}
