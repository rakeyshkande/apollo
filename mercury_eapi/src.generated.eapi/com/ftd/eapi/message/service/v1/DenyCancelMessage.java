
package com.ftd.eapi.message.service.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for denyCancelMessage complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="denyCancelMessage">
 *   &lt;complexContent>
 *     &lt;extension base="{http://eapi.ftd.com/message/service/v1/}orderRelatedMessage">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "denyCancelMessage")
public class DenyCancelMessage
    extends OrderRelatedMessage
{


}
