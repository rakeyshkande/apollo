
package com.ftd.eapi.message.service.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for newMessageListResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="newMessageListResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="multiMessageNewListResponse" type="{http://eapi.ftd.com/message/service/v1/}multiMessageNewListResponse"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "newMessageListResponse", propOrder = {
    "multiMessageNewListResponse"
})
public class NewMessageListResponse {

    @XmlElement(required = true)
    protected MultiMessageNewListResponse multiMessageNewListResponse;

    /**
     * Gets the value of the multiMessageNewListResponse property.
     * 
     * @return
     *     possible object is
     *     {@link MultiMessageNewListResponse }
     *     
     */
    public MultiMessageNewListResponse getMultiMessageNewListResponse() {
        return multiMessageNewListResponse;
    }

    /**
     * Sets the value of the multiMessageNewListResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link MultiMessageNewListResponse }
     *     
     */
    public void setMultiMessageNewListResponse(MultiMessageNewListResponse value) {
        this.multiMessageNewListResponse = value;
    }

}
