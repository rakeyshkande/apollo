
package com.ftd.eapi.message.service.v1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for errorCode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="errorCode">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="INTERNAL_SERVER_ERROR"/>
 *     &lt;enumeration value="SECURITY_ERROR"/>
 *     &lt;enumeration value="INVOKE_POST_MESSAGE_ERROR"/>
 *     &lt;enumeration value="INVOKE_GET_NEW_MESSAGE_LIST_ERROR"/>
 *     &lt;enumeration value="INVOKE_GET_MESSAGE_DETAIL_MESSAGE_ID_ERROR"/>
 *     &lt;enumeration value="INVOKE_GET_MESSAGE_DETAIL_BY_REFERENCE_ERROR"/>
 *     &lt;enumeration value="INVOKE_GET_MESSAGE_LIST_ERROR"/>
 *     &lt;enumeration value="INVOKE_CONFIRM_MESSAGE_BY_REFERENCE_ERROR"/>
 *     &lt;enumeration value="REQUIRED_AUTHENTICATION_ERROR"/>
 *     &lt;enumeration value="REQUIRED_SECURITY_CONTEXT_ERROR"/>
 *     &lt;enumeration value="REQUIRED_MAIN_MEMBER_CODE_ERROR"/>
 *     &lt;enumeration value="REQUIRED_EROS_MESSAGE_ERROR"/>
 *     &lt;enumeration value="REQUIRED_MESSAGE_ID_ERROR"/>
 *     &lt;enumeration value="REQUIRED_REFERENCE_NUMBER_ERROR"/>
 *     &lt;enumeration value="REQUIRED_START_DATE_ERROR"/>
 *     &lt;enumeration value="REQUIRED_END_DATE_ERROR"/>
 *     &lt;enumeration value="REQUIRED_DIRECTION_ERROR"/>
 *     &lt;enumeration value="INVALID_MAIN_MEMBER_CODE"/>
 *     &lt;enumeration value="INVALID_SENDING_MEMBER_CODE"/>
 *     &lt;enumeration value="INVALID_RECEIVING_MEMBER_CODE"/>
 *     &lt;enumeration value="INVALID_FILLING_MEMBER_CODE"/>
 *     &lt;enumeration value="INVALID_REFERENCE_NUMBER"/>
 *     &lt;enumeration value="INVALID_OPERATOR"/>
 *     &lt;enumeration value="INVALID_END_DATE_ERROR"/>
 *     &lt;enumeration value="INVALID_SERVICE_TYPE"/>
 *     &lt;enumeration value="INVALID_RECIPIENT_NAME"/>
 *     &lt;enumeration value="INVALID_RECIPIENT_STREET_ADDRESS"/>
 *     &lt;enumeration value="INVALID_RECIPIENT_CITY"/>
 *     &lt;enumeration value="INVALID_RECIPIENT_STATE_CODE"/>
 *     &lt;enumeration value="INVALID_RECIPIENT_POSTAL_CODE"/>
 *     &lt;enumeration value="INVALID_RECIPIENT_CITYSTATEZIP"/>
 *     &lt;enumeration value="INVALID_RECIPIENT_COUNTRY_CODE"/>
 *     &lt;enumeration value="INVALID_RECIPIENT_PHONE_NUMBER"/>
 *     &lt;enumeration value="INVALID_FIRST_CHOICE_PRODUCT_CODE"/>
 *     &lt;enumeration value="INVALID_FIRST_CHOICE_PRODUCT_DESCRIPTION"/>
 *     &lt;enumeration value="INVALID_SECOND_CHOICE_PRODUCT_CODE"/>
 *     &lt;enumeration value="INVALID_SECOND_CHOICE_PRODUCT_DESCRIPTION"/>
 *     &lt;enumeration value="INVALID_DELIVERY_DATE"/>
 *     &lt;enumeration value="INVALID_DELIVERY_DATE_STRING"/>
 *     &lt;enumeration value="INVALID_DELIVERY_DATE_PAST"/>
 *     &lt;enumeration value="INVALID_DELIVERY_DATE_FUTURE"/>
 *     &lt;enumeration value="INVALID_PRICE"/>
 *     &lt;enumeration value="INVALID_PRICE_STRING"/>
 *     &lt;enumeration value="INVALID_CARD_MESSAGE"/>
 *     &lt;enumeration value="INVALID_SPECIAL_INSTRUCTIONS"/>
 *     &lt;enumeration value="INVALID_OCCASION_CODE"/>
 *     &lt;enumeration value="INVALID_ALLOW_AUTO_FORWARD"/>
 *     &lt;enumeration value="INVALID_ALLOW_FILLER_FORWARD"/>
 *     &lt;enumeration value="INVALID_BYPASS_SUSPENDS"/>
 *     &lt;enumeration value="INVALID_ORIGINAL_ORDER_SEQUENCE_NUMBER"/>
 *     &lt;enumeration value="INVALID_ORIGINAL_ORDER_REFERENCE_NUMBER"/>
 *     &lt;enumeration value="INVALID_CONVENTION_MESSAGE"/>
 *     &lt;enumeration value="INVALID_TAG_LINE"/>
 *     &lt;enumeration value="INVALID_RELATED_ORDER_SEQUENCE_NUMBER"/>
 *     &lt;enumeration value="INVALID_RELATED_ORDER_REFERENCE_NUMBER"/>
 *     &lt;enumeration value="INVALID_ORDER_DATE"/>
 *     &lt;enumeration value="INVALID_ORDER_DATE_STRING"/>
 *     &lt;enumeration value="INVALID_REASON_TEXT"/>
 *     &lt;enumeration value="INVALID_REQUEST_CODE"/>
 *     &lt;enumeration value="INVALID_FROM_DATE"/>
 *     &lt;enumeration value="INVALID_FROM_DATE_STRING"/>
 *     &lt;enumeration value="INVALID_TO_DATE"/>
 *     &lt;enumeration value="INVALID_TO_DATE_STRING"/>
 *     &lt;enumeration value="INVALID_COMBINED_REPORT_NUMBER"/>
 *     &lt;enumeration value="INVALID_MERCURY_ROF_NUMBER"/>
 *     &lt;enumeration value="INVALID_ORDER_AMOUNT"/>
 *     &lt;enumeration value="INVALID_ORDER_AMOUNT_STRING"/>
 *     &lt;enumeration value="INVALID_CORRECTED_AMOUNT"/>
 *     &lt;enumeration value="INVALID_CORRECTED_AMOUNT_STRING"/>
 *     &lt;enumeration value="INVALID_ADJUSTMENT_REASON"/>
 *     &lt;enumeration value="INVALID_ADJUSTMENT_REASON_STRING"/>
 *     &lt;enumeration value="INVALID_MESSAGE_TEXT"/>
 *     &lt;enumeration value="INVALID_NEW_FILLER_MEMBER_CODE"/>
 *     &lt;enumeration value="INVALID_NEW_FILLER_LISTING_CODE"/>
 *     &lt;enumeration value="INVALID_CALLING_MEMBER_CODE"/>
 *     &lt;enumeration value="INVALID_RELATED_ADMIN_SEQUENCE_NUMBER"/>
 *     &lt;enumeration value="INVALID_RELATED_ADMIN_REFERENCE_NUMBER"/>
 *     &lt;enumeration value="INVALID_ADMIN_DATE"/>
 *     &lt;enumeration value="INVALID_PRIORITY"/>
 *     &lt;enumeration value="BAD_COMBINATION"/>
 *     &lt;enumeration value="VALIDATION_ERROR"/>
 *     &lt;enumeration value="GENERIC_ERROR"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "errorCode")
@XmlEnum
public enum ErrorCode {

    INTERNAL_SERVER_ERROR,
    SECURITY_ERROR,
    INVOKE_POST_MESSAGE_ERROR,
    INVOKE_GET_NEW_MESSAGE_LIST_ERROR,
    INVOKE_GET_MESSAGE_DETAIL_MESSAGE_ID_ERROR,
    INVOKE_GET_MESSAGE_DETAIL_BY_REFERENCE_ERROR,
    INVOKE_GET_MESSAGE_LIST_ERROR,
    INVOKE_CONFIRM_MESSAGE_BY_REFERENCE_ERROR,
    REQUIRED_AUTHENTICATION_ERROR,
    REQUIRED_SECURITY_CONTEXT_ERROR,
    REQUIRED_MAIN_MEMBER_CODE_ERROR,
    REQUIRED_EROS_MESSAGE_ERROR,
    REQUIRED_MESSAGE_ID_ERROR,
    REQUIRED_REFERENCE_NUMBER_ERROR,
    REQUIRED_START_DATE_ERROR,
    REQUIRED_END_DATE_ERROR,
    REQUIRED_DIRECTION_ERROR,
    INVALID_MAIN_MEMBER_CODE,
    INVALID_SENDING_MEMBER_CODE,
    INVALID_RECEIVING_MEMBER_CODE,
    INVALID_FILLING_MEMBER_CODE,
    INVALID_REFERENCE_NUMBER,
    INVALID_OPERATOR,
    INVALID_END_DATE_ERROR,
    INVALID_SERVICE_TYPE,
    INVALID_RECIPIENT_NAME,
    INVALID_RECIPIENT_STREET_ADDRESS,
    INVALID_RECIPIENT_CITY,
    INVALID_RECIPIENT_STATE_CODE,
    INVALID_RECIPIENT_POSTAL_CODE,
    INVALID_RECIPIENT_CITYSTATEZIP,
    INVALID_RECIPIENT_COUNTRY_CODE,
    INVALID_RECIPIENT_PHONE_NUMBER,
    INVALID_FIRST_CHOICE_PRODUCT_CODE,
    INVALID_FIRST_CHOICE_PRODUCT_DESCRIPTION,
    INVALID_SECOND_CHOICE_PRODUCT_CODE,
    INVALID_SECOND_CHOICE_PRODUCT_DESCRIPTION,
    INVALID_DELIVERY_DATE,
    INVALID_DELIVERY_DATE_STRING,
    INVALID_DELIVERY_DATE_PAST,
    INVALID_DELIVERY_DATE_FUTURE,
    INVALID_PRICE,
    INVALID_PRICE_STRING,
    INVALID_CARD_MESSAGE,
    INVALID_SPECIAL_INSTRUCTIONS,
    INVALID_OCCASION_CODE,
    INVALID_ALLOW_AUTO_FORWARD,
    INVALID_ALLOW_FILLER_FORWARD,
    INVALID_BYPASS_SUSPENDS,
    INVALID_ORIGINAL_ORDER_SEQUENCE_NUMBER,
    INVALID_ORIGINAL_ORDER_REFERENCE_NUMBER,
    INVALID_CONVENTION_MESSAGE,
    INVALID_TAG_LINE,
    INVALID_RELATED_ORDER_SEQUENCE_NUMBER,
    INVALID_RELATED_ORDER_REFERENCE_NUMBER,
    INVALID_ORDER_DATE,
    INVALID_ORDER_DATE_STRING,
    INVALID_REASON_TEXT,
    INVALID_REQUEST_CODE,
    INVALID_FROM_DATE,
    INVALID_FROM_DATE_STRING,
    INVALID_TO_DATE,
    INVALID_TO_DATE_STRING,
    INVALID_COMBINED_REPORT_NUMBER,
    INVALID_MERCURY_ROF_NUMBER,
    INVALID_ORDER_AMOUNT,
    INVALID_ORDER_AMOUNT_STRING,
    INVALID_CORRECTED_AMOUNT,
    INVALID_CORRECTED_AMOUNT_STRING,
    INVALID_ADJUSTMENT_REASON,
    INVALID_ADJUSTMENT_REASON_STRING,
    INVALID_MESSAGE_TEXT,
    INVALID_NEW_FILLER_MEMBER_CODE,
    INVALID_NEW_FILLER_LISTING_CODE,
    INVALID_CALLING_MEMBER_CODE,
    INVALID_RELATED_ADMIN_SEQUENCE_NUMBER,
    INVALID_RELATED_ADMIN_REFERENCE_NUMBER,
    INVALID_ADMIN_DATE,
    INVALID_PRIORITY,
    BAD_COMBINATION,
    VALIDATION_ERROR,
    GENERIC_ERROR;

    public String value() {
        return name();
    }

    public static ErrorCode fromValue(String v) {
        return valueOf(v);
    }

}
