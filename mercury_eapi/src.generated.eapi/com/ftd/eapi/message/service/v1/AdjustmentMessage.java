
package com.ftd.eapi.message.service.v1;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for adjustmentMessage complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="adjustmentMessage">
 *   &lt;complexContent>
 *     &lt;extension base="{http://eapi.ftd.com/message/service/v1/}erosMessage">
 *       &lt;sequence>
 *         &lt;element name="adjustmentReason" type="{http://eapi.ftd.com/message/service/v1/}adjustmentReason" minOccurs="0"/>
 *         &lt;element name="adjustmentReasonString" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="combinedReportNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="correctedAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="correctedAmountString" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="deliveryDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="deliveryDateString" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fillingMemberCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mercuryRofNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="messageText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="orderAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="orderAmountString" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="recipientName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "adjustmentMessage", propOrder = {
    "adjustmentReason",
    "adjustmentReasonString",
    "combinedReportNumber",
    "correctedAmount",
    "correctedAmountString",
    "deliveryDate",
    "deliveryDateString",
    "fillingMemberCode",
    "mercuryRofNumber",
    "messageText",
    "orderAmount",
    "orderAmountString",
    "recipientName"
})
public class AdjustmentMessage
    extends ErosMessage
{

    protected AdjustmentReason adjustmentReason;
    protected String adjustmentReasonString;
    protected String combinedReportNumber;
    protected BigDecimal correctedAmount;
    protected String correctedAmountString;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar deliveryDate;
    protected String deliveryDateString;
    protected String fillingMemberCode;
    protected String mercuryRofNumber;
    protected String messageText;
    protected BigDecimal orderAmount;
    protected String orderAmountString;
    protected String recipientName;

    /**
     * Gets the value of the adjustmentReason property.
     * 
     * @return
     *     possible object is
     *     {@link AdjustmentReason }
     *     
     */
    public AdjustmentReason getAdjustmentReason() {
        return adjustmentReason;
    }

    /**
     * Sets the value of the adjustmentReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link AdjustmentReason }
     *     
     */
    public void setAdjustmentReason(AdjustmentReason value) {
        this.adjustmentReason = value;
    }

    /**
     * Gets the value of the adjustmentReasonString property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdjustmentReasonString() {
        return adjustmentReasonString;
    }

    /**
     * Sets the value of the adjustmentReasonString property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdjustmentReasonString(String value) {
        this.adjustmentReasonString = value;
    }

    /**
     * Gets the value of the combinedReportNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCombinedReportNumber() {
        return combinedReportNumber;
    }

    /**
     * Sets the value of the combinedReportNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCombinedReportNumber(String value) {
        this.combinedReportNumber = value;
    }

    /**
     * Gets the value of the correctedAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCorrectedAmount() {
        return correctedAmount;
    }

    /**
     * Sets the value of the correctedAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCorrectedAmount(BigDecimal value) {
        this.correctedAmount = value;
    }

    /**
     * Gets the value of the correctedAmountString property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrectedAmountString() {
        return correctedAmountString;
    }

    /**
     * Sets the value of the correctedAmountString property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrectedAmountString(String value) {
        this.correctedAmountString = value;
    }

    /**
     * Gets the value of the deliveryDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDeliveryDate() {
        return deliveryDate;
    }

    /**
     * Sets the value of the deliveryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDeliveryDate(XMLGregorianCalendar value) {
        this.deliveryDate = value;
    }

    /**
     * Gets the value of the deliveryDateString property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeliveryDateString() {
        return deliveryDateString;
    }

    /**
     * Sets the value of the deliveryDateString property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeliveryDateString(String value) {
        this.deliveryDateString = value;
    }

    /**
     * Gets the value of the fillingMemberCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFillingMemberCode() {
        return fillingMemberCode;
    }

    /**
     * Sets the value of the fillingMemberCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFillingMemberCode(String value) {
        this.fillingMemberCode = value;
    }

    /**
     * Gets the value of the mercuryRofNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMercuryRofNumber() {
        return mercuryRofNumber;
    }

    /**
     * Sets the value of the mercuryRofNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMercuryRofNumber(String value) {
        this.mercuryRofNumber = value;
    }

    /**
     * Gets the value of the messageText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageText() {
        return messageText;
    }

    /**
     * Sets the value of the messageText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageText(String value) {
        this.messageText = value;
    }

    /**
     * Gets the value of the orderAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOrderAmount() {
        return orderAmount;
    }

    /**
     * Sets the value of the orderAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOrderAmount(BigDecimal value) {
        this.orderAmount = value;
    }

    /**
     * Gets the value of the orderAmountString property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderAmountString() {
        return orderAmountString;
    }

    /**
     * Sets the value of the orderAmountString property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderAmountString(String value) {
        this.orderAmountString = value;
    }

    /**
     * Gets the value of the recipientName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecipientName() {
        return recipientName;
    }

    /**
     * Sets the value of the recipientName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecipientName(String value) {
        this.recipientName = value;
    }

}
