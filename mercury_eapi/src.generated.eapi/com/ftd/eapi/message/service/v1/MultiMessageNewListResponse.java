
package com.ftd.eapi.message.service.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for multiMessageNewListResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="multiMessageNewListResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="responseCode" type="{http://eapi.ftd.com/message/service/v1/}codeMessagePair" minOccurs="0"/>
 *         &lt;element name="newMessageList" type="{http://eapi.ftd.com/message/service/v1/}newMessageList" minOccurs="0"/>
 *         &lt;element name="hasMoreMessages" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "multiMessageNewListResponse", propOrder = {
    "responseCode",
    "newMessageList",
    "hasMoreMessages"
})
public class MultiMessageNewListResponse {

    protected CodeMessagePair responseCode;
    protected NewMessageList newMessageList;
    protected Boolean hasMoreMessages;

    /**
     * Gets the value of the responseCode property.
     * 
     * @return
     *     possible object is
     *     {@link CodeMessagePair }
     *     
     */
    public CodeMessagePair getResponseCode() {
        return responseCode;
    }

    /**
     * Sets the value of the responseCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeMessagePair }
     *     
     */
    public void setResponseCode(CodeMessagePair value) {
        this.responseCode = value;
    }

    /**
     * Gets the value of the newMessageList property.
     * 
     * @return
     *     possible object is
     *     {@link NewMessageList }
     *     
     */
    public NewMessageList getNewMessageList() {
        return newMessageList;
    }

    /**
     * Sets the value of the newMessageList property.
     * 
     * @param value
     *     allowed object is
     *     {@link NewMessageList }
     *     
     */
    public void setNewMessageList(NewMessageList value) {
        this.newMessageList = value;
    }

    /**
     * Gets the value of the hasMoreMessages property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isHasMoreMessages() {
        return hasMoreMessages;
    }

    /**
     * Sets the value of the hasMoreMessages property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHasMoreMessages(Boolean value) {
        this.hasMoreMessages = value;
    }

}
