
package com.ftd.eapi.message.service.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for forwardMessage complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="forwardMessage">
 *   &lt;complexContent>
 *     &lt;extension base="{http://eapi.ftd.com/message/service/v1/}orderRelatedMessage">
 *       &lt;sequence>
 *         &lt;element name="newFillerMemberCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="newFillerListingCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "forwardMessage", propOrder = {
    "newFillerMemberCode",
    "newFillerListingCode"
})
public class ForwardMessage
    extends OrderRelatedMessage
{

    protected String newFillerMemberCode;
    protected String newFillerListingCode;

    /**
     * Gets the value of the newFillerMemberCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewFillerMemberCode() {
        return newFillerMemberCode;
    }

    /**
     * Sets the value of the newFillerMemberCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewFillerMemberCode(String value) {
        this.newFillerMemberCode = value;
    }

    /**
     * Gets the value of the newFillerListingCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewFillerListingCode() {
        return newFillerListingCode;
    }

    /**
     * Sets the value of the newFillerListingCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewFillerListingCode(String value) {
        this.newFillerListingCode = value;
    }

}
