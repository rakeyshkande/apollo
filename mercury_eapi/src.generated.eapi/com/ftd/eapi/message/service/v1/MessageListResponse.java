
package com.ftd.eapi.message.service.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for messageListResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="messageListResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="multiMessageListResponse" type="{http://eapi.ftd.com/message/service/v1/}multiMessageListResponse"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "messageListResponse", propOrder = {
    "multiMessageListResponse"
})
public class MessageListResponse {

    @XmlElement(required = true)
    protected MultiMessageListResponse multiMessageListResponse;

    /**
     * Gets the value of the multiMessageListResponse property.
     * 
     * @return
     *     possible object is
     *     {@link MultiMessageListResponse }
     *     
     */
    public MultiMessageListResponse getMultiMessageListResponse() {
        return multiMessageListResponse;
    }

    /**
     * Sets the value of the multiMessageListResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link MultiMessageListResponse }
     *     
     */
    public void setMultiMessageListResponse(MultiMessageListResponse value) {
        this.multiMessageListResponse = value;
    }

}
