
package com.ftd.eapi.message.service.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for confirmMessageResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="confirmMessageResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="confirmResponse" type="{http://eapi.ftd.com/message/service/v1/}confirmResponse"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "confirmMessageResponse", propOrder = {
    "confirmResponse"
})
public class ConfirmMessageResponse {

    @XmlElement(required = true)
    protected ConfirmResponse confirmResponse;

    /**
     * Gets the value of the confirmResponse property.
     * 
     * @return
     *     possible object is
     *     {@link ConfirmResponse }
     *     
     */
    public ConfirmResponse getConfirmResponse() {
        return confirmResponse;
    }

    /**
     * Sets the value of the confirmResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link ConfirmResponse }
     *     
     */
    public void setConfirmResponse(ConfirmResponse value) {
        this.confirmResponse = value;
    }

}
