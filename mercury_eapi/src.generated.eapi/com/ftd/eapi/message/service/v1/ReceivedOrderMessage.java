
package com.ftd.eapi.message.service.v1;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for receivedOrderMessage complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="receivedOrderMessage">
 *   &lt;complexContent>
 *     &lt;extension base="{http://eapi.ftd.com/message/service/v1/}erosMessage">
 *       &lt;sequence>
 *         &lt;element name="callingMemberCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="deliveryDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="deliveryDateString" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="occasionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="price" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="priceString" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="recipientName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "receivedOrderMessage", propOrder = {
    "callingMemberCode",
    "deliveryDate",
    "deliveryDateString",
    "occasionCode",
    "price",
    "priceString",
    "recipientName"
})
public class ReceivedOrderMessage
    extends ErosMessage
{

    protected String callingMemberCode;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar deliveryDate;
    protected String deliveryDateString;
    protected String occasionCode;
    protected BigDecimal price;
    protected String priceString;
    protected String recipientName;

    /**
     * Gets the value of the callingMemberCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallingMemberCode() {
        return callingMemberCode;
    }

    /**
     * Sets the value of the callingMemberCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallingMemberCode(String value) {
        this.callingMemberCode = value;
    }

    /**
     * Gets the value of the deliveryDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDeliveryDate() {
        return deliveryDate;
    }

    /**
     * Sets the value of the deliveryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDeliveryDate(XMLGregorianCalendar value) {
        this.deliveryDate = value;
    }

    /**
     * Gets the value of the deliveryDateString property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeliveryDateString() {
        return deliveryDateString;
    }

    /**
     * Sets the value of the deliveryDateString property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeliveryDateString(String value) {
        this.deliveryDateString = value;
    }

    /**
     * Gets the value of the occasionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOccasionCode() {
        return occasionCode;
    }

    /**
     * Sets the value of the occasionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOccasionCode(String value) {
        this.occasionCode = value;
    }

    /**
     * Gets the value of the price property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * Sets the value of the price property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPrice(BigDecimal value) {
        this.price = value;
    }

    /**
     * Gets the value of the priceString property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriceString() {
        return priceString;
    }

    /**
     * Sets the value of the priceString property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriceString(String value) {
        this.priceString = value;
    }

    /**
     * Gets the value of the recipientName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecipientName() {
        return recipientName;
    }

    /**
     * Sets the value of the recipientName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecipientName(String value) {
        this.recipientName = value;
    }

}
