
package com.ftd.eapi.message.service.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for processedErosMessage complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="processedErosMessage">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="messageId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="messageReceivedDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="messageStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="messageType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="notification" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="referenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="adminSequenceNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="orderSequenceNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="relatedMessageId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="transmissionDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="transmissionId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="transmissionStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sender" type="{http://eapi.ftd.com/message/service/v1/}erosMember" minOccurs="0"/>
 *         &lt;element name="receiver" type="{http://eapi.ftd.com/message/service/v1/}erosMember" minOccurs="0"/>
 *         &lt;element name="newFiller" type="{http://eapi.ftd.com/message/service/v1/}erosMember" minOccurs="0"/>
 *         &lt;element name="erosMessage" type="{http://eapi.ftd.com/message/service/v1/}erosMessage" minOccurs="0"/>
 *         &lt;element name="warning" type="{http://eapi.ftd.com/message/service/v1/}codeMessagePair" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="error" type="{http://eapi.ftd.com/message/service/v1/}codeMessagePair" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "processedErosMessage", propOrder = {
    "messageId",
    "messageReceivedDate",
    "messageStatus",
    "messageType",
    "notification",
    "referenceNumber",
    "adminSequenceNumber",
    "orderSequenceNumber",
    "relatedMessageId",
    "transmissionDate",
    "transmissionId",
    "transmissionStatus",
    "sender",
    "receiver",
    "newFiller",
    "erosMessage",
    "warning",
    "error"
})
public class ProcessedErosMessage {

    protected Long messageId;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar messageReceivedDate;
    protected String messageStatus;
    protected String messageType;
    protected String notification;
    protected String referenceNumber;
    protected Integer adminSequenceNumber;
    protected Integer orderSequenceNumber;
    protected Long relatedMessageId;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar transmissionDate;
    protected Long transmissionId;
    protected String transmissionStatus;
    protected ErosMember sender;
    protected ErosMember receiver;
    protected ErosMember newFiller;
    protected ErosMessage erosMessage;
    @XmlElement(nillable = true)
    protected List<CodeMessagePair> warning;
    @XmlElement(nillable = true)
    protected List<CodeMessagePair> error;

    /**
     * Gets the value of the messageId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getMessageId() {
        return messageId;
    }

    /**
     * Sets the value of the messageId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setMessageId(Long value) {
        this.messageId = value;
    }

    /**
     * Gets the value of the messageReceivedDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getMessageReceivedDate() {
        return messageReceivedDate;
    }

    /**
     * Sets the value of the messageReceivedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setMessageReceivedDate(XMLGregorianCalendar value) {
        this.messageReceivedDate = value;
    }

    /**
     * Gets the value of the messageStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageStatus() {
        return messageStatus;
    }

    /**
     * Sets the value of the messageStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageStatus(String value) {
        this.messageStatus = value;
    }

    /**
     * Gets the value of the messageType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageType() {
        return messageType;
    }

    /**
     * Sets the value of the messageType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageType(String value) {
        this.messageType = value;
    }

    /**
     * Gets the value of the notification property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNotification() {
        return notification;
    }

    /**
     * Sets the value of the notification property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNotification(String value) {
        this.notification = value;
    }

    /**
     * Gets the value of the referenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenceNumber() {
        return referenceNumber;
    }

    /**
     * Sets the value of the referenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenceNumber(String value) {
        this.referenceNumber = value;
    }

    /**
     * Gets the value of the adminSequenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAdminSequenceNumber() {
        return adminSequenceNumber;
    }

    /**
     * Sets the value of the adminSequenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAdminSequenceNumber(Integer value) {
        this.adminSequenceNumber = value;
    }

    /**
     * Gets the value of the orderSequenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOrderSequenceNumber() {
        return orderSequenceNumber;
    }

    /**
     * Sets the value of the orderSequenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOrderSequenceNumber(Integer value) {
        this.orderSequenceNumber = value;
    }

    /**
     * Gets the value of the relatedMessageId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getRelatedMessageId() {
        return relatedMessageId;
    }

    /**
     * Sets the value of the relatedMessageId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setRelatedMessageId(Long value) {
        this.relatedMessageId = value;
    }

    /**
     * Gets the value of the transmissionDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTransmissionDate() {
        return transmissionDate;
    }

    /**
     * Sets the value of the transmissionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTransmissionDate(XMLGregorianCalendar value) {
        this.transmissionDate = value;
    }

    /**
     * Gets the value of the transmissionId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getTransmissionId() {
        return transmissionId;
    }

    /**
     * Sets the value of the transmissionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setTransmissionId(Long value) {
        this.transmissionId = value;
    }

    /**
     * Gets the value of the transmissionStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransmissionStatus() {
        return transmissionStatus;
    }

    /**
     * Sets the value of the transmissionStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransmissionStatus(String value) {
        this.transmissionStatus = value;
    }

    /**
     * Gets the value of the sender property.
     * 
     * @return
     *     possible object is
     *     {@link ErosMember }
     *     
     */
    public ErosMember getSender() {
        return sender;
    }

    /**
     * Sets the value of the sender property.
     * 
     * @param value
     *     allowed object is
     *     {@link ErosMember }
     *     
     */
    public void setSender(ErosMember value) {
        this.sender = value;
    }

    /**
     * Gets the value of the receiver property.
     * 
     * @return
     *     possible object is
     *     {@link ErosMember }
     *     
     */
    public ErosMember getReceiver() {
        return receiver;
    }

    /**
     * Sets the value of the receiver property.
     * 
     * @param value
     *     allowed object is
     *     {@link ErosMember }
     *     
     */
    public void setReceiver(ErosMember value) {
        this.receiver = value;
    }

    /**
     * Gets the value of the newFiller property.
     * 
     * @return
     *     possible object is
     *     {@link ErosMember }
     *     
     */
    public ErosMember getNewFiller() {
        return newFiller;
    }

    /**
     * Sets the value of the newFiller property.
     * 
     * @param value
     *     allowed object is
     *     {@link ErosMember }
     *     
     */
    public void setNewFiller(ErosMember value) {
        this.newFiller = value;
    }

    /**
     * Gets the value of the erosMessage property.
     * 
     * @return
     *     possible object is
     *     {@link ErosMessage }
     *     
     */
    public ErosMessage getErosMessage() {
        return erosMessage;
    }

    /**
     * Sets the value of the erosMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link ErosMessage }
     *     
     */
    public void setErosMessage(ErosMessage value) {
        this.erosMessage = value;
    }

    /**
     * Gets the value of the warning property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the warning property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWarning().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CodeMessagePair }
     * 
     * 
     */
    public List<CodeMessagePair> getWarning() {
        if (warning == null) {
            warning = new ArrayList<CodeMessagePair>();
        }
        return this.warning;
    }

    /**
     * Gets the value of the error property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the error property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getError().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CodeMessagePair }
     * 
     * 
     */
    public List<CodeMessagePair> getError() {
        if (error == null) {
            error = new ArrayList<CodeMessagePair>();
        }
        return this.error;
    }

}
