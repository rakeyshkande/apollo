
package com.ftd.eapi.message.service.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for multiMessageListResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="multiMessageListResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="responseCode" type="{http://eapi.ftd.com/message/service/v1/}codeMessagePair" minOccurs="0"/>
 *         &lt;element name="retrieveErosMessageList" type="{http://eapi.ftd.com/message/service/v1/}retrieveErosMessageList" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "multiMessageListResponse", propOrder = {
    "responseCode",
    "retrieveErosMessageList"
})
public class MultiMessageListResponse {

    protected CodeMessagePair responseCode;
    protected RetrieveErosMessageList retrieveErosMessageList;

    /**
     * Gets the value of the responseCode property.
     * 
     * @return
     *     possible object is
     *     {@link CodeMessagePair }
     *     
     */
    public CodeMessagePair getResponseCode() {
        return responseCode;
    }

    /**
     * Sets the value of the responseCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeMessagePair }
     *     
     */
    public void setResponseCode(CodeMessagePair value) {
        this.responseCode = value;
    }

    /**
     * Gets the value of the retrieveErosMessageList property.
     * 
     * @return
     *     possible object is
     *     {@link RetrieveErosMessageList }
     *     
     */
    public RetrieveErosMessageList getRetrieveErosMessageList() {
        return retrieveErosMessageList;
    }

    /**
     * Sets the value of the retrieveErosMessageList property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrieveErosMessageList }
     *     
     */
    public void setRetrieveErosMessageList(RetrieveErosMessageList value) {
        this.retrieveErosMessageList = value;
    }

}
