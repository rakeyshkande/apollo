
package com.ftd.eapi.message.service.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for singleMessageResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="singleMessageResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="responseCode" type="{http://eapi.ftd.com/message/service/v1/}codeMessagePair" minOccurs="0"/>
 *         &lt;element name="processedErosMessage" type="{http://eapi.ftd.com/message/service/v1/}processedErosMessage"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "singleMessageResponse", propOrder = {
    "responseCode",
    "processedErosMessage"
})
public class SingleMessageResponse {

    protected CodeMessagePair responseCode;
    @XmlElement(required = true)
    protected ProcessedErosMessage processedErosMessage;

    /**
     * Gets the value of the responseCode property.
     * 
     * @return
     *     possible object is
     *     {@link CodeMessagePair }
     *     
     */
    public CodeMessagePair getResponseCode() {
        return responseCode;
    }

    /**
     * Sets the value of the responseCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeMessagePair }
     *     
     */
    public void setResponseCode(CodeMessagePair value) {
        this.responseCode = value;
    }

    /**
     * Gets the value of the processedErosMessage property.
     * 
     * @return
     *     possible object is
     *     {@link ProcessedErosMessage }
     *     
     */
    public ProcessedErosMessage getProcessedErosMessage() {
        return processedErosMessage;
    }

    /**
     * Sets the value of the processedErosMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcessedErosMessage }
     *     
     */
    public void setProcessedErosMessage(ProcessedErosMessage value) {
        this.processedErosMessage = value;
    }

}
