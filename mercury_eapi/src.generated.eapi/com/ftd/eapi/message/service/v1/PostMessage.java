
package com.ftd.eapi.message.service.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for postMessage complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="postMessage">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="mainMemberCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="erosMessage" type="{http://eapi.ftd.com/message/service/v1/}erosMessage"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "postMessage", propOrder = {
    "mainMemberCode",
    "erosMessage"
})
public class PostMessage {

    @XmlElement(required = true)
    protected String mainMemberCode;
    @XmlElement(required = true)
    protected ErosMessage erosMessage;

    /**
     * Gets the value of the mainMemberCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMainMemberCode() {
        return mainMemberCode;
    }

    /**
     * Sets the value of the mainMemberCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMainMemberCode(String value) {
        this.mainMemberCode = value;
    }

    /**
     * Gets the value of the erosMessage property.
     * 
     * @return
     *     possible object is
     *     {@link ErosMessage }
     *     
     */
    public ErosMessage getErosMessage() {
        return erosMessage;
    }

    /**
     * Sets the value of the erosMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link ErosMessage }
     *     
     */
    public void setErosMessage(ErosMessage value) {
        this.erosMessage = value;
    }

}
