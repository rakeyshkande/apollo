
package com.ftd.eapi.message.service.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.ftd.eapi.message.service.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _PostMessage_QNAME = new QName("http://eapi.ftd.com/message/service/v1/", "postMessage");
    private final static QName _PostMessageResponse_QNAME = new QName("http://eapi.ftd.com/message/service/v1/", "postMessageResponse");
    private final static QName _GetMessageDetailByReference_QNAME = new QName("http://eapi.ftd.com/message/service/v1/", "getMessageDetailByReference");
    private final static QName _GetMessageList_QNAME = new QName("http://eapi.ftd.com/message/service/v1/", "getMessageList");
    private final static QName _GetMessageDetailByMessageID_QNAME = new QName("http://eapi.ftd.com/message/service/v1/", "getMessageDetailByMessageID");
    private final static QName _MessageDetailResponse_QNAME = new QName("http://eapi.ftd.com/message/service/v1/", "messageDetailResponse");
    private final static QName _ConfirmMessageByReference_QNAME = new QName("http://eapi.ftd.com/message/service/v1/", "confirmMessageByReference");
    private final static QName _GetNewMessageList_QNAME = new QName("http://eapi.ftd.com/message/service/v1/", "getNewMessageList");
    private final static QName _MessageListResponse_QNAME = new QName("http://eapi.ftd.com/message/service/v1/", "messageListResponse");
    private final static QName _EapiMessageServiceException_QNAME = new QName("http://eapi.ftd.com/message/service/v1/", "eapiMessageServiceException");
    private final static QName _MessageDetailByReferenceResponse_QNAME = new QName("http://eapi.ftd.com/message/service/v1/", "messageDetailByReferenceResponse");
    private final static QName _ConfirmMessageResponse_QNAME = new QName("http://eapi.ftd.com/message/service/v1/", "confirmMessageResponse");
    private final static QName _NewMessageListResponse_QNAME = new QName("http://eapi.ftd.com/message/service/v1/", "newMessageListResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.ftd.eapi.message.service.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link EapiMessageServiceFault }
     * 
     */
    public EapiMessageServiceFault createEapiMessageServiceFault() {
        return new EapiMessageServiceFault();
    }

    /**
     * Create an instance of {@link MessageDetailByReferenceResponse }
     * 
     */
    public MessageDetailByReferenceResponse createMessageDetailByReferenceResponse() {
        return new MessageDetailByReferenceResponse();
    }

    /**
     * Create an instance of {@link ConfirmMessageResponse }
     * 
     */
    public ConfirmMessageResponse createConfirmMessageResponse() {
        return new ConfirmMessageResponse();
    }

    /**
     * Create an instance of {@link NewMessageListResponse }
     * 
     */
    public NewMessageListResponse createNewMessageListResponse() {
        return new NewMessageListResponse();
    }

    /**
     * Create an instance of {@link GetMessageDetailByReference }
     * 
     */
    public GetMessageDetailByReference createGetMessageDetailByReference() {
        return new GetMessageDetailByReference();
    }

    /**
     * Create an instance of {@link PostMessage }
     * 
     */
    public PostMessage createPostMessage() {
        return new PostMessage();
    }

    /**
     * Create an instance of {@link PostMessageResponse }
     * 
     */
    public PostMessageResponse createPostMessageResponse() {
        return new PostMessageResponse();
    }

    /**
     * Create an instance of {@link GetMessageList }
     * 
     */
    public GetMessageList createGetMessageList() {
        return new GetMessageList();
    }

    /**
     * Create an instance of {@link GetMessageDetailByMessageID }
     * 
     */
    public GetMessageDetailByMessageID createGetMessageDetailByMessageID() {
        return new GetMessageDetailByMessageID();
    }

    /**
     * Create an instance of {@link MessageDetailResponse }
     * 
     */
    public MessageDetailResponse createMessageDetailResponse() {
        return new MessageDetailResponse();
    }

    /**
     * Create an instance of {@link ConfirmMessageByReference }
     * 
     */
    public ConfirmMessageByReference createConfirmMessageByReference() {
        return new ConfirmMessageByReference();
    }

    /**
     * Create an instance of {@link GetNewMessageList }
     * 
     */
    public GetNewMessageList createGetNewMessageList() {
        return new GetNewMessageList();
    }

    /**
     * Create an instance of {@link MessageListResponse }
     * 
     */
    public MessageListResponse createMessageListResponse() {
        return new MessageListResponse();
    }

    /**
     * Create an instance of {@link EapiException }
     * 
     */
    public EapiException createEapiException() {
        return new EapiException();
    }

    /**
     * Create an instance of {@link SuspendMessage }
     * 
     */
    public SuspendMessage createSuspendMessage() {
        return new SuspendMessage();
    }

    /**
     * Create an instance of {@link ConfirmCancelMessage }
     * 
     */
    public ConfirmCancelMessage createConfirmCancelMessage() {
        return new ConfirmCancelMessage();
    }

    /**
     * Create an instance of {@link ResumeMessage }
     * 
     */
    public ResumeMessage createResumeMessage() {
        return new ResumeMessage();
    }

    /**
     * Create an instance of {@link DenyCancelMessage }
     * 
     */
    public DenyCancelMessage createDenyCancelMessage() {
        return new DenyCancelMessage();
    }

    /**
     * Create an instance of {@link CancelAdjustmentMessage }
     * 
     */
    public CancelAdjustmentMessage createCancelAdjustmentMessage() {
        return new CancelAdjustmentMessage();
    }

    /**
     * Create an instance of {@link AdjustmentMessage }
     * 
     */
    public AdjustmentMessage createAdjustmentMessage() {
        return new AdjustmentMessage();
    }

    /**
     * Create an instance of {@link ReceivedOrderMessage }
     * 
     */
    public ReceivedOrderMessage createReceivedOrderMessage() {
        return new ReceivedOrderMessage();
    }

    /**
     * Create an instance of {@link MultiMessageListResponse }
     * 
     */
    public MultiMessageListResponse createMultiMessageListResponse() {
        return new MultiMessageListResponse();
    }

    /**
     * Create an instance of {@link OrderMessage }
     * 
     */
    public OrderMessage createOrderMessage() {
        return new OrderMessage();
    }

    /**
     * Create an instance of {@link RetrieveErosMessage }
     * 
     */
    public RetrieveErosMessage createRetrieveErosMessage() {
        return new RetrieveErosMessage();
    }

    /**
     * Create an instance of {@link SimpleResponse }
     * 
     */
    public SimpleResponse createSimpleResponse() {
        return new SimpleResponse();
    }

    /**
     * Create an instance of {@link AskMessage }
     * 
     */
    public AskMessage createAskMessage() {
        return new AskMessage();
    }

    /**
     * Create an instance of {@link RetrieveErosMessageList }
     * 
     */
    public RetrieveErosMessageList createRetrieveErosMessageList() {
        return new RetrieveErosMessageList();
    }

    /**
     * Create an instance of {@link MultiMessageNewListResponse }
     * 
     */
    public MultiMessageNewListResponse createMultiMessageNewListResponse() {
        return new MultiMessageNewListResponse();
    }

    /**
     * Create an instance of {@link CancelMarketplaceMessage }
     * 
     */
    public CancelMarketplaceMessage createCancelMarketplaceMessage() {
        return new CancelMarketplaceMessage();
    }

    /**
     * Create an instance of {@link SingleMessageResponse }
     * 
     */
    public SingleMessageResponse createSingleMessageResponse() {
        return new SingleMessageResponse();
    }

    /**
     * Create an instance of {@link AnswerMessage }
     * 
     */
    public AnswerMessage createAnswerMessage() {
        return new AnswerMessage();
    }

    /**
     * Create an instance of {@link ErosMember }
     * 
     */
    public ErosMember createErosMember() {
        return new ErosMember();
    }

    /**
     * Create an instance of {@link NewMessage }
     * 
     */
    public NewMessage createNewMessage() {
        return new NewMessage();
    }

    /**
     * Create an instance of {@link CodeMessagePair }
     * 
     */
    public CodeMessagePair createCodeMessagePair() {
        return new CodeMessagePair();
    }

    /**
     * Create an instance of {@link RejectOrderMessage }
     * 
     */
    public RejectOrderMessage createRejectOrderMessage() {
        return new RejectOrderMessage();
    }

    /**
     * Create an instance of {@link NewMessageList }
     * 
     */
    public NewMessageList createNewMessageList() {
        return new NewMessageList();
    }

    /**
     * Create an instance of {@link GeneralMessage }
     * 
     */
    public GeneralMessage createGeneralMessage() {
        return new GeneralMessage();
    }

    /**
     * Create an instance of {@link ConfirmResponse }
     * 
     */
    public ConfirmResponse createConfirmResponse() {
        return new ConfirmResponse();
    }

    /**
     * Create an instance of {@link CancelOrderMessage }
     * 
     */
    public CancelOrderMessage createCancelOrderMessage() {
        return new CancelOrderMessage();
    }

    /**
     * Create an instance of {@link ForwardMessage }
     * 
     */
    public ForwardMessage createForwardMessage() {
        return new ForwardMessage();
    }

    /**
     * Create an instance of {@link ProcessedErosMessage }
     * 
     */
    public ProcessedErosMessage createProcessedErosMessage() {
        return new ProcessedErosMessage();
    }

    /**
     * Create an instance of {@link CancelReceivedOrderMessage }
     * 
     */
    public CancelReceivedOrderMessage createCancelReceivedOrderMessage() {
        return new CancelReceivedOrderMessage();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PostMessage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://eapi.ftd.com/message/service/v1/", name = "postMessage")
    public JAXBElement<PostMessage> createPostMessage(PostMessage value) {
        return new JAXBElement<PostMessage>(_PostMessage_QNAME, PostMessage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PostMessageResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://eapi.ftd.com/message/service/v1/", name = "postMessageResponse")
    public JAXBElement<PostMessageResponse> createPostMessageResponse(PostMessageResponse value) {
        return new JAXBElement<PostMessageResponse>(_PostMessageResponse_QNAME, PostMessageResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMessageDetailByReference }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://eapi.ftd.com/message/service/v1/", name = "getMessageDetailByReference")
    public JAXBElement<GetMessageDetailByReference> createGetMessageDetailByReference(GetMessageDetailByReference value) {
        return new JAXBElement<GetMessageDetailByReference>(_GetMessageDetailByReference_QNAME, GetMessageDetailByReference.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMessageList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://eapi.ftd.com/message/service/v1/", name = "getMessageList")
    public JAXBElement<GetMessageList> createGetMessageList(GetMessageList value) {
        return new JAXBElement<GetMessageList>(_GetMessageList_QNAME, GetMessageList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMessageDetailByMessageID }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://eapi.ftd.com/message/service/v1/", name = "getMessageDetailByMessageID")
    public JAXBElement<GetMessageDetailByMessageID> createGetMessageDetailByMessageID(GetMessageDetailByMessageID value) {
        return new JAXBElement<GetMessageDetailByMessageID>(_GetMessageDetailByMessageID_QNAME, GetMessageDetailByMessageID.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MessageDetailResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://eapi.ftd.com/message/service/v1/", name = "messageDetailResponse")
    public JAXBElement<MessageDetailResponse> createMessageDetailResponse(MessageDetailResponse value) {
        return new JAXBElement<MessageDetailResponse>(_MessageDetailResponse_QNAME, MessageDetailResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConfirmMessageByReference }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://eapi.ftd.com/message/service/v1/", name = "confirmMessageByReference")
    public JAXBElement<ConfirmMessageByReference> createConfirmMessageByReference(ConfirmMessageByReference value) {
        return new JAXBElement<ConfirmMessageByReference>(_ConfirmMessageByReference_QNAME, ConfirmMessageByReference.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetNewMessageList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://eapi.ftd.com/message/service/v1/", name = "getNewMessageList")
    public JAXBElement<GetNewMessageList> createGetNewMessageList(GetNewMessageList value) {
        return new JAXBElement<GetNewMessageList>(_GetNewMessageList_QNAME, GetNewMessageList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MessageListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://eapi.ftd.com/message/service/v1/", name = "messageListResponse")
    public JAXBElement<MessageListResponse> createMessageListResponse(MessageListResponse value) {
        return new JAXBElement<MessageListResponse>(_MessageListResponse_QNAME, MessageListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EapiMessageServiceFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://eapi.ftd.com/message/service/v1/", name = "eapiMessageServiceException")
    public JAXBElement<EapiMessageServiceFault> createEapiMessageServiceException(EapiMessageServiceFault value) {
        return new JAXBElement<EapiMessageServiceFault>(_EapiMessageServiceException_QNAME, EapiMessageServiceFault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MessageDetailByReferenceResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://eapi.ftd.com/message/service/v1/", name = "messageDetailByReferenceResponse")
    public JAXBElement<MessageDetailByReferenceResponse> createMessageDetailByReferenceResponse(MessageDetailByReferenceResponse value) {
        return new JAXBElement<MessageDetailByReferenceResponse>(_MessageDetailByReferenceResponse_QNAME, MessageDetailByReferenceResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConfirmMessageResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://eapi.ftd.com/message/service/v1/", name = "confirmMessageResponse")
    public JAXBElement<ConfirmMessageResponse> createConfirmMessageResponse(ConfirmMessageResponse value) {
        return new JAXBElement<ConfirmMessageResponse>(_ConfirmMessageResponse_QNAME, ConfirmMessageResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NewMessageListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://eapi.ftd.com/message/service/v1/", name = "newMessageListResponse")
    public JAXBElement<NewMessageListResponse> createNewMessageListResponse(NewMessageListResponse value) {
        return new JAXBElement<NewMessageListResponse>(_NewMessageListResponse_QNAME, NewMessageListResponse.class, null, value);
    }

}
