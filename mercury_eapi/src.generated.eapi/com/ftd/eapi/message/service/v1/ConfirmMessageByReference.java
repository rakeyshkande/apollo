
package com.ftd.eapi.message.service.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for confirmMessageByReference complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="confirmMessageByReference">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="mainMemberCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="referenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "confirmMessageByReference", propOrder = {
    "mainMemberCode",
    "referenceNumber"
})
public class ConfirmMessageByReference {

    @XmlElement(required = true)
    protected String mainMemberCode;
    @XmlElement(nillable = true)
    protected List<String> referenceNumber;

    /**
     * Gets the value of the mainMemberCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMainMemberCode() {
        return mainMemberCode;
    }

    /**
     * Sets the value of the mainMemberCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMainMemberCode(String value) {
        this.mainMemberCode = value;
    }

    /**
     * Gets the value of the referenceNumber property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the referenceNumber property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReferenceNumber().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getReferenceNumber() {
        if (referenceNumber == null) {
            referenceNumber = new ArrayList<String>();
        }
        return this.referenceNumber;
    }

}
