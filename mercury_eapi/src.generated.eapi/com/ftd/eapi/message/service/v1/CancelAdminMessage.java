
package com.ftd.eapi.message.service.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for cancelAdminMessage complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cancelAdminMessage">
 *   &lt;complexContent>
 *     &lt;extension base="{http://eapi.ftd.com/message/service/v1/}erosMessage">
 *       &lt;sequence>
 *         &lt;element name="reasonText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="relatedAdminDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="relatedAdminReferenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="relatedAdminSequenceNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cancelAdminMessage", propOrder = {
    "reasonText",
    "relatedAdminDate",
    "relatedAdminReferenceNumber",
    "relatedAdminSequenceNumber"
})
@XmlSeeAlso({
    CancelAdjustmentMessage.class,
    CancelMarketplaceMessage.class,
    CancelReceivedOrderMessage.class
})
public abstract class CancelAdminMessage
    extends ErosMessage
{

    protected String reasonText;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar relatedAdminDate;
    protected String relatedAdminReferenceNumber;
    protected Integer relatedAdminSequenceNumber;

    /**
     * Gets the value of the reasonText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReasonText() {
        return reasonText;
    }

    /**
     * Sets the value of the reasonText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReasonText(String value) {
        this.reasonText = value;
    }

    /**
     * Gets the value of the relatedAdminDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRelatedAdminDate() {
        return relatedAdminDate;
    }

    /**
     * Sets the value of the relatedAdminDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRelatedAdminDate(XMLGregorianCalendar value) {
        this.relatedAdminDate = value;
    }

    /**
     * Gets the value of the relatedAdminReferenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelatedAdminReferenceNumber() {
        return relatedAdminReferenceNumber;
    }

    /**
     * Sets the value of the relatedAdminReferenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelatedAdminReferenceNumber(String value) {
        this.relatedAdminReferenceNumber = value;
    }

    /**
     * Gets the value of the relatedAdminSequenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRelatedAdminSequenceNumber() {
        return relatedAdminSequenceNumber;
    }

    /**
     * Sets the value of the relatedAdminSequenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRelatedAdminSequenceNumber(Integer value) {
        this.relatedAdminSequenceNumber = value;
    }

}
