
package com.ftd.eapi.message.service.v1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for adjustmentReason.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="adjustmentReason">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NO_RECORD"/>
 *     &lt;enumeration value="NON_DELIVERY"/>
 *     &lt;enumeration value="OVERCHARGED"/>
 *     &lt;enumeration value="UNDERCHARGED"/>
 *     &lt;enumeration value="DUPLICATE_CHARGE"/>
 *     &lt;enumeration value="PAID_DIRECT"/>
 *     &lt;enumeration value="ORDER_CANCELED"/>
 *     &lt;enumeration value="CHARGED_IN_ERROR"/>
 *     &lt;enumeration value="INCOMING_NOT_OUTGOING"/>
 *     &lt;enumeration value="UNSATISFACTORY"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "adjustmentReason")
@XmlEnum
public enum AdjustmentReason {

    NO_RECORD,
    NON_DELIVERY,
    OVERCHARGED,
    UNDERCHARGED,
    DUPLICATE_CHARGE,
    PAID_DIRECT,
    ORDER_CANCELED,
    CHARGED_IN_ERROR,
    INCOMING_NOT_OUTGOING,
    UNSATISFACTORY;

    public String value() {
        return name();
    }

    public static AdjustmentReason fromValue(String v) {
        return valueOf(v);
    }

}
