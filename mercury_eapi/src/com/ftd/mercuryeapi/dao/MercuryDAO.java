package com.ftd.mercuryeapi.dao;

/*
 * -------------------------------History------------------------------- Author
 * CreatedDate ModifiedDate Reason
 * --------------------------------------------------------------------- smotla Jun 17th
 * 2013 Jun 17th 2013 Apollo-EAPI Integration
 */
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.InitialContext;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.ftd.mercuryeapi.services.MercuryEapiException;
import com.ftd.mercuryeapi.util.MercuryEapiConstants;
import com.ftd.mercuryeapi.util.MercuryEapiUtil;
import com.ftd.mercuryeapi.viewobjects.MercuryEapiOpsVO;
import com.ftd.mercuryeapi.viewobjects.MercuryMessageVO;
import com.ftd.mercuryeapi.viewobjects.MercuryStageMessageVO;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.xml.DOMUtil;

/**
 * This class contains data access methods used within the Mercury Interface project.
 */
public class MercuryDAO {

  private Connection conn;
  private Logger logger;
  private static String LOGGER_CATEGORY = "com.ftd.mercuryeapi.dao.MercuryDAO";
  private static final String CLASS_NAME = "MercuryDAO";

  // custom parameters returned from database procedure
  private static final String STATUS_PARAM = "RegisterOutParameterStatus";
  private static final String MESSAGE_PARAM = "RegisterOutParameterMessage";
  private StringBuffer sb = null;

  /**
   * Constructor
   * 
   * @param Connection
   */
  public MercuryDAO(Connection conn) {
    this.conn = conn;
    logger = new Logger(LOGGER_CATEGORY);
  }

  /**
   * Clear the mercury eapi poll queue
   */
  public void clearEapiPollQueue() throws Exception {
    final String METHOD_NAME = ".clearEapiPollQueue()";
    if (conn == null) {
      sb = new StringBuffer();
      sb.append(CLASS_NAME).append(METHOD_NAME).append(" - DB connection is null");
      logger.error(sb.toString());
      throw new MercuryEapiException(sb.toString());
    }
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("CLEAR_EAPI_POLL_QUEUE");
    Map<String, String> paramMap = new HashMap<String, String>();
    dataRequest.setInputParams(paramMap);
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    String status = (String) dataAccessUtil.execute(dataRequest);
    if (status.equals("N")) {
      throw new Exception("Queue could not be cleared.");
    }
    if (logger.isDebugEnabled()) {
      logger.debug(CLASS_NAME + METHOD_NAME + " - Cleaned up MERCURY_POLL queue");
    }

  }

  public MercuryMessageVO getMercuryMessageByMessageNumber(String messageNumber) throws Exception {

    // declare and initialize variables
    final String METHOD_NAME = ".getMercuryMessageByMessageNumber()";
    boolean isEmpty = true;
    MercuryMessageVO vo = null;

    if (conn == null) {
      sb = new StringBuffer();
      sb.append(CLASS_NAME).append(METHOD_NAME).append(" - DB connection is null");
      logger.error(sb.toString());
      throw new MercuryEapiException(sb.toString());
    }
    vo = new MercuryMessageVO();

    DataRequest dataRequest = new DataRequest();

    /* setup stored procedure input parameters */
    HashMap<String, String> inputParams = new HashMap<String, String>();
    inputParams.put("IN_MERCURY_MESSAGE_NUMBER", messageNumber);

    dataRequest.setConnection(conn);
    dataRequest.setStatementID("GET_MERCURY_BY_MESSAGE_NUMBER");
    dataRequest.setInputParams(inputParams);

    /* execute the stored prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    CachedResultSet output = (CachedResultSet) dataAccessUtil.execute(dataRequest);
    while (output.next()) {
      isEmpty = false;
      vo.setMercuryID(output.getString("MERCURY_ID"));
      vo.setMercuryMessageNumber(output.getString("MERCURY_MESSAGE_NUMBER"));
      vo.setMercuryOrderNumber(output.getString("MERCURY_ORDER_NUMBER"));
      vo.setMercuryStatus(output.getString("MERCURY_STATUS"));
      vo.setMessageType(output.getString("MSG_TYPE"));
      vo.setOutboundID(output.getString("OUTBOUND_ID"));
      vo.setSendingFlorist(output.getString("SENDING_FLORIST"));
      vo.setFillingFlorist(output.getString("FILLING_FLORIST"));
      vo.setOrderDate(output.getDate("ORDER_DATE"));
      vo.setRecipient(output.getString("RECIPIENT"));
      vo.setAddress(output.getString("ADDRESS"));
      vo.setCityStateZip(output.getString("CITY_STATE_ZIP"));
      vo.setPhoneNumber(output.getString("PHONE_NUMBER"));
      vo.setDeliveryDate(output.getDate("DELIVERY_DATE"));
      vo.setDeliveryDateText(output.getString("DELIVERY_DATE_TEXT"));
      vo.setFirstChoice(output.getString("FIRST_CHOICE"));
      vo.setSecondChoice(output.getString("SECOND_CHOICE"));
      vo.setPrice(new Double(output.getDouble("PRICE")));
      vo.setCardMessage(output.getString("CARD_MESSAGE"));
      vo.setOccasion(output.getString("OCCASION"));
      vo.setSpecialInstructions(output.getString("SPECIAL_INSTRUCTIONS"));
      vo.setPriority(output.getString("PRIORITY"));
      vo.setOperator(output.getString("OPERATOR"));
      vo.setComments(output.getString("COMMENTS"));
      vo.setSakText(output.getString("SAK_TEXT"));
      vo.setCTSEQ(output.getInt("CTSEQ"));
      vo.setCRSEQ(output.getInt("CRSEQ"));
      vo.setTransmissionDate(output.getDate("TRANSMISSION_TIME"));
      vo.setReferenceNumber(output.getString("REFERENCE_NUMBER"));
      vo.setProductID(output.getString("PRODUCT_ID"));
      vo.setZipCode(output.getString("ZIP_CODE"));
      vo.setAskAnswerCode(output.getString("ASK_ANSWER_CODE"));
      vo.setSortValue(output.getString("SORT_VALUE"));
      vo.setRetrievalFlag(output.getString("RETRIEVAL_FLAG"));
      vo.setRetrievalToMessage(output.getString("TO_MESSAGE_NUMBER"));
      vo.setRetrievalFromMessage(output.getString("FROM_MESSAGE_NUMBER"));
      vo.setRetrievalFromDate(output.getString("FROM_MESSAGE_DATE"));
      vo.setRetrievalToDate(output.getString("TO_MESSAGE_DATE"));
      vo.setViewQueue(output.getString("VIEW_QUEUE"));
      vo.setSuffix(output.getString("SUFFIX"));
      vo.setDirection(output.getString("MESSAGE_DIRECTION"));
      vo.setOrderSequence(output.getString("ORDER_SEQ"));
      vo.setAdminSequence(output.getString("ADMIN_SEQ"));
      vo.setCombinedReportNumber(output.getString("COMBINED_REPORT_NUMBER"));
      vo.setRofNumber(output.getString("ROF_NUMBER"));
      vo.setOverUnderCharge(output.getDouble("OVER_UNDER_CHARGE"));
      vo.setADJReasonCode(output.getString("ADJ_REASON_CODE"));
    }

    if (isEmpty) {
      if (logger.isDebugEnabled()) {
        logger.debug(CLASS_NAME + METHOD_NAME
            + " - No record present in mercury table for mercury_message_number: " + messageNumber);
      }
      return null;
    }
    else {
      if (logger.isDebugEnabled()) {
        logger.debug(CLASS_NAME + METHOD_NAME
            + " - Fetched message from mercury table for mercury_message_number: " + messageNumber);
      }
      return vo;
    }

  }

  /**
   * Update a row in the Mercury Message table.
   * 
   * @param MercuryMessageVO
   */
  @SuppressWarnings("unchecked")
  public void updateMercuryMessageEapi(MercuryMessageVO vo) throws Exception {

    // declare and initialize the variables
    final String METHOD_NAME = ".updateMercuryMessageEapi()";
    DataRequest dataRequest = new DataRequest();

    if (conn == null) {
      sb = new StringBuffer();
      sb.append(CLASS_NAME).append(METHOD_NAME).append(" - DB connection is null");
      logger.error(sb.toString());
      throw new MercuryEapiException(sb.toString());
    }

    dataRequest.setConnection(conn);

    if (logger.isDebugEnabled()) {
      logger.debug(CLASS_NAME + METHOD_NAME
          + " - Updating Mercury Message with EAPI's response for MercuryId=" + vo.getMercuryID());
    }

    // mare sure all messages can fit in the DB
    vo = MercuryEapiUtil.truncateMercuryMessageVOFields(vo);

    dataRequest.setStatementID("UPDATE_MERCURY_EAPI");
    Map paramMap = new HashMap();

    paramMap.put("IN_MERCURY_ID", vo.getMercuryID());
    paramMap.put("IN_MERCURY_MESSAGE_NUMBER", vo.getMercuryMessageNumber());
    paramMap.put("IN_MERCURY_STATUS", vo.getMercuryStatus());
    paramMap.put("IN_SAK_TEXT", vo.getSakText());
    paramMap.put("IN_CTSEQ", new Long(vo.getCTSEQ()));
    paramMap.put("IN_CRSEQ", new Long(vo.getCRSEQ()));
    paramMap.put("IN_TRANSMISSION_TIME", MercuryEapiUtil.getTimestamp(vo.getTransmissionDate()));
    paramMap.put("IN_MERCURY_ORDER_NUMBER", vo.getMercuryOrderNumber());
    paramMap.put("IN_VIEW_QUEUE", vo.getViewQueue());
    paramMap.put("IN_SUFFIX", vo.getSuffix());
    paramMap.put("IN_ORDER_SEQ", vo.getOrderSequence());
    paramMap.put("IN_ADMIN_SEQ", vo.getAdminSequence());
    paramMap.put("IN_OUTBOUND_ID", vo.getOutboundID());
    paramMap.put("IN_COMMENTS", vo.getComments());

    dataRequest.setInputParams(paramMap);
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    Map<String, Object> outputs = (Map<String, Object>) dataAccessUtil.execute(dataRequest);

    String status = (String) outputs.get(MercuryEapiConstants.STATUS_PARAM);
    if (status.equals("N")) {
      String message = (String) outputs.get(MercuryEapiConstants.MESSAGE_PARAM);
      throw new MercuryEapiException(message);
    }

    if (logger.isDebugEnabled()) {
      logger.debug(CLASS_NAME + METHOD_NAME + " - Update successful for MercuryId: "
          + vo.getMercuryID());
    }
  }

  /**
   * insert a row in the Mercury Message table.
   * 
   * @param MercuryMessageVO
   */
  public void storeMercuryMessage(MercuryMessageVO vo) throws Exception {

    // declare and initialize the variables
    final String METHOD_NAME = ".updateMercuryMessageEapi()";
    DataRequest dataRequest = new DataRequest();

    if (conn == null) {
      sb = new StringBuffer();
      sb.append(CLASS_NAME).append(METHOD_NAME).append(" - DB connection is null");
      logger.error(sb.toString());
      throw new MercuryEapiException(sb.toString());
    }

    dataRequest.setConnection(conn);

    // mare sure all messages can fit in the DB
    vo = MercuryEapiUtil.truncateMercuryMessageVOFields(vo);

    Map paramMap = new HashMap();

    paramMap.put("IN_MERCURY_ID", vo.getMercuryID());
    paramMap.put("IN_MERCURY_MESSAGE_NUMBER", vo.getMercuryMessageNumber());
    paramMap.put("IN_MERCURY_ORDER_NUMBER", vo.getMercuryOrderNumber());
    paramMap.put("IN_MERCURY_STATUS", vo.getMercuryStatus());
    paramMap.put("IN_MSG_TYPE", vo.getMessageType());
    paramMap.put("IN_OUTBOUND_ID", vo.getOutboundID());
    paramMap.put("IN_SENDING_FLORIST", vo.getSendingFlorist());
    paramMap.put("IN_FILLING_FLORIST", vo.getFillingFlorist());
    paramMap.put("IN_ORDER_DATE", MercuryEapiUtil.getTimestamp(vo.getOrderDate()));
    paramMap.put("IN_RECIPIENT", vo.getRecipient());
    paramMap.put("IN_ADDRESS", vo.getAddress());
    paramMap.put("IN_CITY_STATE_ZIP", vo.getCityStateZip());
    paramMap.put("IN_PHONE_NUMBER", vo.getPhoneNumber());
    paramMap.put("IN_DELIVERY_DATE", MercuryEapiUtil.getTimestamp(vo.getDeliveryDate()));
    paramMap.put("IN_DELIVERY_DATE_TEXT", vo.getDeliveryDateText());
    paramMap.put("IN_FIRST_CHOICE", vo.getFirstChoice());
    paramMap.put("IN_SECOND_CHOICE", vo.getSecondChoice());
    paramMap.put("IN_CARD_MESSAGE", vo.getCardMessage());
    paramMap.put("IN_OCCASION", vo.getOccasion());
    paramMap.put("IN_SPECIAL_INSTRUCTIONS", vo.getSpecialInstructions());
    paramMap.put("IN_PRIORITY", vo.getPriority());
    paramMap.put("IN_OPERATOR", vo.getOperator());
    paramMap.put("IN_COMMENTS", vo.getComments());
    paramMap.put("IN_SAK_TEXT", vo.getSakText());
    paramMap.put("IN_CTSEQ", new Long(vo.getCTSEQ()));
    paramMap.put("IN_CRSEQ", new Long(vo.getCRSEQ()));
    paramMap.put("IN_TRANSMISSION_TIME", MercuryEapiUtil.getTimestamp(vo.getTransmissionDate()));
    paramMap.put("IN_REFERENCE_NUMBER", vo.getReferenceNumber());
    paramMap.put("IN_PRODUCT_ID", vo.getProductID());
    paramMap.put("IN_ZIP_CODE", vo.getZipCode());
    paramMap.put("IN_ASK_ANSWER_CODE", vo.getAskAnswerCode());
    paramMap.put("IN_SORT_VALUE", vo.getSortValue());
    paramMap.put("IN_RETRIEVAL_FLAG", vo.getRetrievalFlag());
    paramMap.put("IN_COMBINED_REPORT_NUMBER", vo.getCombinedReportNumber());
    paramMap.put("IN_ROF_NUMBER", vo.getRofNumber());
    paramMap.put("IN_ADJ_REASON_CODE", vo.getADJReasonCode());
    paramMap.put("IN_OVER_UNDER_CHARGE", Double.toString(vo.getOverUnderCharge()));
    paramMap.put("IN_FROM_MESSAGE_NUMBER", vo.getRetrievalFromMessage());
    paramMap.put("IN_FROM_MESSAGE_DATE", vo.getRetrievalFromDate());
    paramMap.put("IN_TO_MESSAGE_NUMBER", vo.getRetrievalToMessage());
    paramMap.put("IN_TO_MESSAGE_DATE", vo.getRetrievalToDate());
    paramMap.put("IN_PRICE", MercuryEapiUtil.doubleToString(vo.getPrice()));
    paramMap.put("IN_VIEW_QUEUE", vo.getViewQueue());
    paramMap.put("IN_MESSAGE_DIRECTION", vo.getDirection());
    paramMap.put("IN_COMP_ORDER", new String());
    paramMap.put("IN_REQUIRE_CONFIRMATION", new String("N"));
    paramMap.put("IN_SUFFIX", vo.getSuffix());
    paramMap.put("IN_ORDER_SEQ", vo.getOrderSequence());
    paramMap.put("IN_ADMIN_SEQ", vo.getAdminSequence());
    paramMap.put("IN_OLD_PRICE", new String());
    paramMap.put("IN_ORIG_COMPLAINT_COMM_TYPE_ID", null);
    paramMap.put("IN_NOTI_COMPLAINT_COMM_TYPE_ID", null);

    dataRequest.setStatementID("INSERT_MERCURY");

    dataRequest.setInputParams(paramMap);
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);

    String status = (String) outputs.get(STATUS_PARAM);
    if (status.equals("N")) {
      String message = (String) outputs.get(MESSAGE_PARAM);
      throw new Exception(message);
    }
    if (logger.isDebugEnabled()) {
      logger.debug(CLASS_NAME + METHOD_NAME
          + " - Successfully stored new mercury message with mercury message number: "
          + vo.getMercuryMessageNumber());
    }

  }

  /**
   * insert a Mercury Inbound JMS message into the OJMS.MERCURY_INBOUND table.
   * 
   * @param mercuryID
   */
  public void insertMercuryInboundMDB(String mercuryID) throws Exception {

    // declare and initialize the variable
    final String METHOD_NAME = ".insertMercuryInboundMDB()";

    if (conn == null) {
      sb = new StringBuffer();
      sb.append(CLASS_NAME).append(METHOD_NAME).append(" - DB connection is null");
      logger.error(sb.toString());
      throw new MercuryEapiException(sb.toString());
    }

    logger.info(CLASS_NAME + METHOD_NAME
        + " - Sending Mercury Message to MERCURY INBOUND QUEUE. MercuryId=" + mercuryID);
    // Send JMS message
    InitialContext initialContext = new InitialContext();
    MessageToken token = new MessageToken();

    token.setMessage(mercuryID);
    token.setJMSCorrelationID((String) token.getMessage());
    token.setStatus("MERCURYINBOUND");

    Dispatcher dispatcher = Dispatcher.getInstance();
    dispatcher.dispatchTextMessage(initialContext, token);
    if (logger.isDebugEnabled()) {
      logger.debug(CLASS_NAME + METHOD_NAME
          + " - Mercury message sent to MERCURY INBOUND QUEUE. MercuryId=" + mercuryID);
    }

  }

  /**
   * insert a row in the Mercury Message table.
   * 
   * @param MercuryMessageVO
   */
  public void updateSuspend(String floristID, String suspendInd, String susID, String resID)
      throws Exception {

    // declare and initialize the variables
    final String METHOD_NAME = ".updateSuspend()";

    if (conn == null) {
      sb = new StringBuffer();
      sb.append(CLASS_NAME).append(METHOD_NAME).append(" - DB connection is null");
      logger.error(sb.toString());
      throw new MercuryEapiException(sb.toString());
    }

    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("UPDATE_FLORIST_GOTO_SUSPEND");

    Map paramMap = new HashMap();
    paramMap.put("IN_FLORIST_ID", floristID);
    paramMap.put("IN_SUSPEND_INDICATOR", suspendInd);
    paramMap.put("IN_GOTO_FLORIST_SUS_MERC_ID", susID);
    paramMap.put("IN_GOTO_FLORIST_RES_MERC_ID", resID);

    dataRequest.setInputParams(paramMap);
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);

    String status = (String) outputs.get(STATUS_PARAM);
    if (status.equals("N")) {
      String message = (String) outputs.get(MESSAGE_PARAM);
      throw new Exception(message);
    }

    if (logger.isDebugEnabled()) {
      logger.debug(CLASS_NAME + METHOD_NAME + " - Updated the suspend flag for florist ID: "
          + floristID);
    }
  }

  /**
   * This method is used to store the mercury new messages to the mercury eapi staging
   * table
   * 
   * @param mercuryStageMessageVO
   * @throws Exception
   */
  public String storeMercuryEapiStagingMessage(MercuryStageMessageVO mercuryStageMessageVO)
      throws Exception {

    final String METHOD_NAME = ".storeMercuryEapiStagingMessage()";

    String status = null;

    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(conn);

    // mare sure all messages can fit in the DB
    mercuryStageMessageVO = MercuryEapiUtil.truncateMercuryStageMessageFields(mercuryStageMessageVO);

    Map paramMap = new HashMap();

    paramMap.put("IN_MESSAGE_ID", mercuryStageMessageVO.getMessageId().toString());
    paramMap.put("IN_REFERENCE_NUMBER", mercuryStageMessageVO.getReferenceNumber());
    paramMap.put("IN_MESSAGE_RECEIVED_DATE",
        MercuryEapiUtil.getTimestamp(mercuryStageMessageVO.getMessageReceivedDate()));
    paramMap.put("IN_MESSAGE_TYPE", mercuryStageMessageVO.getMessageType());
    paramMap.put("IN_STATUS", mercuryStageMessageVO.getStatus());
    paramMap.put("IN_MAIN_MEMBER_CODE", mercuryStageMessageVO.getMainMemberCode());
    paramMap.put("IN_CREATED_ON", MercuryEapiUtil.getTimestamp(mercuryStageMessageVO.getCreatedOn()));
    paramMap.put("IN_UPDATED_ON", MercuryEapiUtil.getTimestamp(mercuryStageMessageVO.getUpdatedOn()));
    paramMap.put("IN_CREATED_BY", mercuryStageMessageVO.getCreatedBy());
    paramMap.put("IN_UPDATED_BY", mercuryStageMessageVO.getUpdatedBy());

    dataRequest.setStatementID("INSERT_MERCURYEAPI_NEW_MESSAGE");

    dataRequest.setInputParams(paramMap);
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);

    status = (String) outputs.get(STATUS_PARAM);
    if (status.equals("N")) {
      String message = (String) outputs.get(MESSAGE_PARAM);
      logger.error("The mercury new message having the message id:: "
          + mercuryStageMessageVO.getMessageId() + " got the following error while inserting: "
          + message);
    }

    return status;
  }

  /**
   * This method is used to update the status of the mercury new messages to the mercury
   * eapi staging table
   * 
   * @param mercuryStageMessageVO
   * @throws Exception
   */
  public String updateMercuryEapiStagingMessage(MercuryStageMessageVO mercuryStageMessageVO)
      throws Exception {

    // declare and initialize the variables
    final String METHOD_NAME = ".updateMercuryEapiStagingMessage()";
    String status = null;

    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(conn);

    // mare sure all messages can fit in the DB
    mercuryStageMessageVO = MercuryEapiUtil
        .truncateMercuryStageMessageFieldsForUpdate(mercuryStageMessageVO);

    Map paramMap = new HashMap();

    paramMap.put("IN_MESSAGE_ID", mercuryStageMessageVO.getMessageId().toString());
    paramMap.put("IN_STATUS", mercuryStageMessageVO.getStatus());
    paramMap.put("IN_REASON", mercuryStageMessageVO.getReason());
    paramMap.put("IN_UPDATED_ON", MercuryEapiUtil.getTimestamp(new Date()));
    paramMap.put("IN_UPDATED_BY", mercuryStageMessageVO.getUpdatedBy());

    dataRequest.setStatementID("UPDATE_MERCURYEAPI_NEW_MESSAGE");

    dataRequest.setInputParams(paramMap);
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);

    status = (String) outputs.get(STATUS_PARAM);
    if (status.equals("N")) {
      String message = (String) outputs.get(MESSAGE_PARAM);
      logger.error("The mercury new message having the reference number : "
          + mercuryStageMessageVO.getReferenceNumber() + " got the following error while updating: "
          + message);
    }

    return status;
  }

  /**
   * Get a Mercury Message based on reference number
   * 
   * @param String
   *          referenceNumber
   * @return a MercuryMessageVO populated with values from the database
   * @throws Exception
   */
  public MercuryStageMessageVO getMercuryPolledMessageToBeProcessed(String messageId) throws Exception {

    // declare and initialize the variables
    final String METHOD_NAME = ".getMercuryPolledMessageToBeProcessed()";
    MercuryStageMessageVO mercuryStageMessageVO = null;

    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(conn);

    if (logger.isDebugEnabled()) {
      logger.debug(CLASS_NAME + METHOD_NAME + " - Fetch " + messageId
          + " from staging table");
    }

    dataRequest.setStatementID("GET_MERCURYEAPI_STAGE_MESSAGE");
    Map paramMap = new HashMap();
    paramMap.put("IN_MESSAGE_ID", messageId);
    paramMap.put("IN_STATUS", MercuryEapiConstants.CONFIRMED);
    dataRequest.setInputParams(paramMap);
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    Map<String, Object> outputs = (Map<String, Object>) dataAccessUtil.execute(dataRequest);

    String status = (String) outputs.get(STATUS_PARAM);

    if (MercuryEapiConstants.N_STRING.equalsIgnoreCase(status)) {
      String message = (String) outputs.get(MESSAGE_PARAM);
      throw new Exception(message);
    }

    CachedResultSet rs = (CachedResultSet) outputs.get("RegisterOutParameter1");

    if (rs.next()) {
    	mercuryStageMessageVO = new MercuryStageMessageVO();
    	mercuryStageMessageVO.setMessageId(Long.parseLong(rs.getString("message_id")));
    	mercuryStageMessageVO.setReferenceNumber(rs.getString("reference_number"));
    	mercuryStageMessageVO.setMessageReceivedDate(rs.getDate("message_received_date"));
    	mercuryStageMessageVO.setMessageType(rs.getString("message_type"));
    	mercuryStageMessageVO.setMainMemberCode(rs.getString("main_member_code"));
    	mercuryStageMessageVO.setStatus(rs.getString("status"));
    	mercuryStageMessageVO.setCreatedOn(rs.getDate("created_on"));
    	mercuryStageMessageVO.setUpdatedOn(rs.getDate("updated_on"));
    	mercuryStageMessageVO.setCreatedBy(rs.getString("created_by"));
    	mercuryStageMessageVO.setUpdatedBy(rs.getString("updated_by"));
    }

    return mercuryStageMessageVO;
  }

  /**
   * Get a list of Mercury Messages based on mainMemberCode that are in am 'MQ' status
   * 
   * @param String
   *          suffix
   * @return a list of MercuryMessageVO populate with values from the database
   * @throws Exception
   */
  public HashMap<String, String> getMercuryStageMessagesToBeConfirmed(String mainMemberCode, String status)
      throws Exception {

    // declare and initialize the variables
    final String METHOD_NAME = ".getMercuryStageMessagesToBeConfirmed()";
    HashMap<String, String> messageMap = new HashMap<String, String>();

    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(conn);

    dataRequest.setStatementID("GET_INBOUND_EAPI_RCVD_MESSAGES");
    Map paramMap = new HashMap();
    paramMap.put("IN_MAIN_MEMBER_CODE", mainMemberCode);
    paramMap.put("IN_STATUS", status);
    dataRequest.setInputParams(paramMap);
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

    while (rs.next()) {
      messageMap.put(rs.getString("reference_number"), rs.getString("message_id"));
    }

    if (logger.isDebugEnabled()) {
      logger.debug(CLASS_NAME + METHOD_NAME + " - Fetched " + messageMap.size()
          + " from EAPI Staging table which are supposed to be confirmed.");
    }

    return messageMap;
  }

  /**
   * Retrieve a record from the MERCURY_EAPI_OPS table.
   * 
   * @param mainMemberCode
   */
  @SuppressWarnings("unchecked")
  public MercuryEapiOpsVO getMercuryEapiOps(String mainMemberCode) throws Exception {

    // declare and initialize the variables
    final String METHOD_NAME = ".getMercuryEapiOps()";
    MercuryEapiOpsVO mercuryEapiOpsVO = null;

    if (conn == null) {
        sb = new StringBuffer();
        sb.append(CLASS_NAME).append(METHOD_NAME).append(" - DB connection is null");
        logger.error(sb.toString());
        throw new MercuryEapiException(sb.toString());
    }

    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("GET_MERCURY_EAPI_OPS");
    Map paramMap = new HashMap();
    paramMap.put("IN_MAIN_MEMBER_CODE", mainMemberCode);

    dataRequest.setInputParams(paramMap);
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

    if (rs.next()) {
    	mercuryEapiOpsVO = new MercuryEapiOpsVO();
    	mercuryEapiOpsVO.setMainMemberCode(mainMemberCode);
    	if (rs.getString("is_available").equalsIgnoreCase("Y")) {
    		mercuryEapiOpsVO.setAvailable(true);
    	} else {
    		mercuryEapiOpsVO.setAvailable(false);
    	}
    	if (rs.getString("in_use").equalsIgnoreCase("Y")) {
    		mercuryEapiOpsVO.setInUse(true);
    	} else {
    		mercuryEapiOpsVO.setInUse(false);
    	}
    	mercuryEapiOpsVO.setCreatedBy(rs.getString("created_by"));
    	mercuryEapiOpsVO.setCreatedOn(rs.getDate("created_on"));
    	mercuryEapiOpsVO.setUpdatedBy(rs.getString("updated_by"));
    	mercuryEapiOpsVO.setUpdatedOn(rs.getDate("updated_on"));
    }
    
    return mercuryEapiOpsVO;
  }


  /**
   * Update the MERCURY_EAPI_OPS table.
   * 
   * @param mercuryEapOpsVO
   */
  @SuppressWarnings("unchecked")
  public void updateMercuryEapiOps(MercuryEapiOpsVO mercuryEapiOpsVO) throws Exception {

    // declare and initialize the variables
    final String METHOD_NAME = ".updateMercuryEapiOps()";

    if (conn == null) {
        sb = new StringBuffer();
        sb.append(CLASS_NAME).append(METHOD_NAME).append(" - DB connection is null");
        logger.error(sb.toString());
        throw new MercuryEapiException(sb.toString());
    }

    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("UPDATE_MERCURY_EAPI_OPS");
    Map paramMap = new HashMap();
    paramMap.put("IN_MAIN_MEMBER_CODE", mercuryEapiOpsVO.getMainMemberCode());
    paramMap.put("IN_IS_AVAILABLE", mercuryEapiOpsVO.isAvailable()? "Y" : "N");
    paramMap.put("IN_IN_USE", mercuryEapiOpsVO.isInUse() ? "Y" : "N");
    paramMap.put("IN_UPDATED_BY", mercuryEapiOpsVO.getUpdatedBy());

    dataRequest.setInputParams(paramMap);
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    Map<String, Object> outputs = (Map<String, Object>) dataAccessUtil.execute(dataRequest);

    String status = (String) outputs.get(STATUS_PARAM);
    if (status.equals("N")) {
        String message = (String) outputs.get(MESSAGE_PARAM);
        throw new Exception(message);
    }

  }


  /**
   * Returns the available main member codes which are available
   * 
   * @param String queueName
   * @param String isAvaString
   * @param String inUseString
   * @return
   * @throws Exception
   */
  public List<String> getAvailableMainMemberCode(String isAvaString,
      String inUseString) throws Exception {
    String suffix = null;

    final String METHOD_NAME = ".getAvailableMainMemberCode()";
    List<String> mainMemberCodes = null;
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(conn);

    Map paramMap = new HashMap();
    paramMap.put("IN_AVAILABLE", isAvaString);
    paramMap.put("IN_IN_USE", inUseString);

    dataRequest.setStatementID("EAPI_GET_MAIN_MEMBER_CODES");

    dataRequest.setInputParams(paramMap);
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    logger.info(CLASS_NAME + METHOD_NAME
        + " - Fetch available main member codes for POLLING process");

    Map<String, Object> outputs = (Map<String, Object>) dataAccessUtil.execute(dataRequest);
    String status = (String) outputs.get(STATUS_PARAM);
    if (status.equals("N")) {
      String message = (String) outputs.get(MESSAGE_PARAM);
      throw new Exception(message);
    }

    CachedResultSet rs = (CachedResultSet) outputs.get("RegisterOutParameter1");
    mainMemberCodes = new ArrayList<String>();
    while (rs.next()) {
      mainMemberCodes.add((String) rs.getObject(1));
    }

    return mainMemberCodes;
  }

  /**
   * Get mercury details for a given mercury Id
   * 
   * @param String mercuryId
   */
  @SuppressWarnings("unchecked")
  public MercuryMessageVO getMercuryMessageById(String mercuryId)
      throws Exception {

    final String METHOD_NAME = ".getMercuryMessageById()";
    MercuryMessageVO vo = null;
    DataRequest dataRequest = new DataRequest();
    if (conn == null) {
      sb = new StringBuffer();
      sb.append(CLASS_NAME).append(METHOD_NAME).append(" - DB connection is null");
      logger.error(sb.toString());
      throw new MercuryEapiException(sb.toString());
    }

    dataRequest.setConnection(conn);

    dataRequest.setStatementID("GET_MERCURY_BY_ID_EAPI");
    Map paramMap = new HashMap();
    paramMap.put("IN_MERCURY_ID", mercuryId);
    dataRequest.setInputParams(paramMap);
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    if (logger.isDebugEnabled()) {
      logger.debug(CLASS_NAME + METHOD_NAME + " - Fetch message details for Mercury ID: " + mercuryId);
    }

    CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

    // if something was found
    if (rs.next()) {
        vo = new MercuryMessageVO();
        vo.setMercuryID(rs.getString("mercury_id"));
        vo.setMercuryMessageNumber(rs.getString("mercury_message_number"));
        vo.setMercuryOrderNumber(rs.getString("mercury_order_number"));
        vo.setMercuryStatus(rs.getString("mercury_status"));
        vo.setMessageType(rs.getString("msg_type"));
        vo.setOutboundID(rs.getString("outbound_id"));
        vo.setSendingFlorist(rs.getString("sending_florist"));
        vo.setFillingFlorist(rs.getString("filling_florist"));
        vo.setOrderDate(rs.getDate("order_date"));
        vo.setRecipient(rs.getString("recipient"));
        vo.setAddress(rs.getString("address"));
        vo.setCityStateZip(rs.getString("city_state_zip"));
        vo.setPhoneNumber(rs.getString("phone_number"));
        vo.setDeliveryDate(rs.getDate("delivery_date"));
        vo.setDeliveryDateText(rs.getString("delivery_date_text"));
        vo.setFirstChoice(rs.getString("first_choice"));
        vo.setSecondChoice(rs.getString("second_choice"));
        vo.setPrice(MercuryEapiUtil.getDoubleAmount(rs.getString("price")));
        vo.setCardMessage(rs.getString("card_message"));
        vo.setOccasion(rs.getString("occasion"));
        vo.setSpecialInstructions(rs.getString("special_instructions"));
        vo.setPriority(rs.getString("priority"));
        vo.setOperator(rs.getString("operator"));
        vo.setComments(rs.getString("comments"));
        vo.setReferenceNumber(rs.getString("reference_number"));
        vo.setProductID(rs.getString("product_id"));
        vo.setZipCode(rs.getString("zip_code"));
        vo.setAskAnswerCode(rs.getString("ask_answer_code"));
        vo.setSortValue(rs.getString("sort_value"));
        vo.setCombinedReportNumber(rs.getString("combined_report_number"));
        vo.setRofNumber(rs.getString("rof_number"));
        vo.setADJReasonCode(rs.getString("adj_reason_code"));
        vo.setOverUnderCharge(MercuryEapiUtil.getAmount(rs.getString("over_under_charge")));
        vo.setGotoFloristFlag(rs.getString("super_florist_flag"));
        vo.setFloristAllowsForwards(rs.getString("allow_message_forwarding_flag"));
        vo.setFloristSuspendStatus(rs.getString("suspend_type"));
    }
    if (logger.isDebugEnabled()) {
      logger.debug(CLASS_NAME + METHOD_NAME + " - Fetched message details for Mercury ID: "
          + mercuryId);
    }
    return vo;
  }
  
  /**
   * Returns the main member codes present in MERCURY_EAPI_OPS table.
   * Ops_Admin module calls this method to get the main membercode  info.
   * 
   * @param String
   *          mainMemberCode
   * @return MercuryEapiOpsVO
   */
  public MercuryEapiOpsVO getMercuryEapiOpsByMainMemberCode(String mainMemberCode) throws Exception {
    
    final String METHOD_NAME = ".getMercuryEapiOpsByMainMemberCode()";
    MercuryEapiOpsVO vo = null;
    DataRequest dataRequest = new DataRequest();
    
    if (conn == null) {
      sb = new StringBuffer();
      sb.append(CLASS_NAME).append(METHOD_NAME).append(" - DB connection is null");
      logger.error(sb.toString());
      throw new MercuryEapiException(sb.toString());
    }
    
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("VIEW_MERCURY_EAPI_OPS");
    Map paramMap = new HashMap();
    paramMap.put("IN_MAIN_MEMBER_CODE", mainMemberCode);
    dataRequest.setInputParams(paramMap);
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    Document xmlDoc = (Document) dataAccessUtil.execute(dataRequest);

    List list = new ArrayList();

    String xpath = "MAINMEMBERCODES/MAINMEMBERCODE";
    NodeList nl = DOMUtil.selectNodes(xmlDoc, xpath);
    if (nl.getLength() > 0) {
        Element node = (Element) nl.item(0);

        vo = new MercuryEapiOpsVO();
        vo.setMainMemberCode(node.getAttribute("main_member_code"));
        vo.setInUse(MercuryEapiUtil.getBoolean(node.getAttribute("in_use")));
        vo.setAvailable(MercuryEapiUtil.getBoolean(node.getAttribute("is_available")));

    }
    return vo;
  }
  
  /**
   * Updates the MercuryEapiOps row for the given main member code.
   * Ops_Admin module calls this method to update the main membercode  info.
   * @param MercuryEapiOpsVO mercuryEapiOpsVO
   * @param String oldMainMemberCode
   */
  public void updateMercuryEapiOps(String oldMainMemberCode, String newMainMemberCode, String inUse, String available, String updatedBy) throws Exception
  {
    
    final String METHOD_NAME = ".updateMercuryEapiOps()";
    DataRequest dataRequest = new DataRequest();
    
    if (conn == null) {
      sb = new StringBuffer();
      sb.append(CLASS_NAME).append(METHOD_NAME).append(" - DB connection is null");
      logger.error(sb.toString());
      throw new MercuryEapiException(sb.toString());
    }
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("UPDATE_MERCURY_EAPI_OPS_ADMIN");
    Map paramMap = new HashMap();
    paramMap.put("IN_OLD_MAIN_MEMBER_CODE",oldMainMemberCode);
    paramMap.put("IN_NEW_MAIN_MEMBER_CODE",newMainMemberCode);
    paramMap.put("IN_IN_USE",inUse);
    paramMap.put("IN_IS_AVAILABLE",available);
    paramMap.put("IN_UPDATED_ON",MercuryEapiUtil.getTimestamp(new Date()));
    paramMap.put("IN_UPDATED_BY", updatedBy);
    dataRequest.setInputParams(paramMap);
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);
    
    String status = (String) outputs.get(STATUS_PARAM);
    if(status.equals("N"))
    {
      String message = (String) outputs.get(MESSAGE_PARAM);
      throw new Exception(message);
    }

  }
  
  /**
   * Inserts new main member code into the MercuryEapiOps table.
   * Ops_Admin module calls this method to insert a new main membercode  info.
   * 
   * @param MercuryEapiOpsVO mercuryEapiOpsVO
   */
  public void insertMercuryEapiOps(MercuryEapiOpsVO vo) throws Exception
  {
    
    final String METHOD_NAME = ".insertMercuryEapiOps()";
    DataRequest dataRequest = new DataRequest();
    
    if (conn == null) {
      sb = new StringBuffer();
      sb.append(CLASS_NAME).append(METHOD_NAME).append(" - DB connection is null");
      logger.error(sb.toString());
      throw new MercuryEapiException(sb.toString());
    }
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("INSERT_MERCURY_EAPI_OPS_ADMIN");
    Map paramMap = new HashMap();
    paramMap.put("IN_MAIN_MEMBER_CODE",vo.getMainMemberCode());
    if (vo.isInUse()) {
      paramMap.put("IN_IN_USE",MercuryEapiConstants.Y_STRING);
    }
    else {
      paramMap.put("IN_IN_USE",MercuryEapiConstants.N_STRING);
    }
    if (vo.isAvailable()) {
      paramMap.put("IN_IS_AVAILABLE",MercuryEapiConstants.Y_STRING);
    }
    else {
      paramMap.put("IN_IS_AVAILABLE",MercuryEapiConstants.N_STRING);
    }
    paramMap.put("IN_CREATED_BY",vo.getCreatedBy());
    paramMap.put("IN_UPDATED_BY",vo.getUpdatedBy());
    dataRequest.setInputParams(paramMap);
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);
    
    String status = (String) outputs.get(STATUS_PARAM);
    if(status.equals("N"))
    {
      String message = (String) outputs.get(MESSAGE_PARAM);
      throw new Exception(message);
    }

  }


}
