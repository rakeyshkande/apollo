package com.ftd.mercuryeapi.viewobjects;
import java.util.Date;

public class MercuryEapiOpsVO 
{
  private String mainMemberCode;
  private boolean inUse;
  private boolean available;
  private Date createdOn;
  private Date updatedOn;
  private String createdBy;
  private String updatedBy;
  
  public MercuryEapiOpsVO()
  {
  }

  public boolean isInUse()
  {
    return inUse;
  }

  public void setInUse(boolean newInUse)
  {
    inUse = newInUse;
  }

  public boolean isAvailable()
  {
    return available;
  }

  public void setAvailable(boolean available)
  {
    this.available = available;
  }

  public void setMainMemberCode(String mainMemberCode) {
    this.mainMemberCode = mainMemberCode;
  }

  public String getMainMemberCode() {
    return mainMemberCode;
  }

  public void setCreatedOn(Date createdOn) {
    this.createdOn = createdOn;
  }

  public Date getCreatedOn() {
    return createdOn;
  }

  public void setUpdatedOn(Date updatedOn) {
    this.updatedOn = updatedOn;
  }

  public Date getUpdatedOn() {
    return updatedOn;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setUpdatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
  }

  public String getUpdatedBy() {
    return updatedBy;
  }
}