package com.ftd.mercuryeapi.viewobjects;

import java.util.Date;

public class MercuryMessageVO {

  private String MercuryID;
  private String MercuryMessageNumber;
  private String MercuryOrderNumber;
  private String MercuryStatus;
  private String MessageType;
  private String OutboundID;
  private String SendingFlorist;
  private String FillingFlorist;
  private Date OrderDate;
  private String Recipient;
  private String Address;
  private String CityStateZip;
  private String PhoneNumber;
  private Date DeliveryDate;
  private String FirstChoice;
  private String SecondChoice;
  private Double Price;
  private String CardMessage;
  private String Occasion;
  private String SpecialInstructions;
  private String Priority;
  private String Operator;
  private String Comments;
  private String SakText;
  private int CTSEQ;
  private int CRSEQ;
  private Date TransmissionDate;
  private String ReferenceNumber;
  private String ProductID;
  private String ZipCode;
  private String AskAnswerCode;
  private String DeliveryDateText;
  private String SortValue;
  private String RetrievalFlag;
  private String fromMessageNumber;
  private String toMessageNumber;
  private Date fromMessageDate;
  private Date toMessageDate;
  private String CombinedReportNumber;
  private String RofNumber;
  private double OverUnderCharge;
  private String ADJReasonCode;
  private String retrievalFromDate;
  private String retrievalFromMessage;
  private String retrievalToDate;
  private String retrievalToMessage;
  private String viewQueue;

  // new for phase III
  private String direction;
  private String suffix;
  private String orderSequence;
  private String adminSequence;
  private String orderReference;
  private String adminReference;
  private String correlationReference;
  private String service;
  private String deliveryDetails;
  private String tagLine;
  private String recipientCity;
  private String recipientState;
  private String recipientCountryCode;
  private String firstChoiceCode;
  private String firstChoiceDesc;
  private String secondChoiceCode;
  private String secondChoiceDesc;
  private String conventionMessage;
  
  private String gotoFloristFlag;
  private String floristSuspendStatus;
  private String floristAllowsForwards;

  public MercuryMessageVO() {
  }

  public String toString() {
    return "MercuryMessageVO:.:" + "Mercury Id.:" + this.MercuryID + "," + "\n"
        + "MercuryMessageNumber..:" + this.MercuryMessageNumber + "," + "\n"
        + "MercuryMessageNumber..:" + this.MercuryMessageNumber + "," + "\n"
        + "MercuryOrderNumber..:" + this.MercuryOrderNumber + "," + "\n" + "MercuryStatus..:"
        + this.MercuryStatus + "," + "\n" + "MessageType..:" + this.MessageType + "," + "\n"
        + "OutboundID..:" + this.OutboundID + "," + "\n" + "SendingFlorist..:"
        + this.SendingFlorist + "," + "\n" + "FillingFlorist..:" + this.FillingFlorist + "," + "\n"
        + "OrderDate..:" + this.OrderDate + "," + "\n" + "Recipient..:" + this.Recipient + ","
        + "\n" + "Address..:" + this.Address + "," + "\n" + "CityStateZip..:" + this.CityStateZip
        + "," + "\n" + "PhoneNumber..:" + this.PhoneNumber + "," + "\n" + "DeliveryDate..:"
        + this.DeliveryDate + "," + "\n" + "FirstChoice..:" + this.FirstChoice + "," + "\n"
        + "SecondChoice..:" + this.SecondChoice + "," + "\n" + "Price..:" + this.Price + "," + "\n"
        + "CardMessage..:" + this.CardMessage + "," + "\n" + "Occasion..:" + this.Occasion + ","
        + "\n" + "SpecialInstructions..:" + this.SpecialInstructions + "," + "\n" + "Priority..:"
        + this.Priority + "," + "\n" + "Operator..:" + this.Operator + "," + "\n" + "Comments..:"
        + this.Comments + "," + "\n" + "SakText..:" + this.SakText + "," + "\n" + "CTSEQ..:"
        + this.CTSEQ + "," + "\n" + "CRSEQ..:" + this.CRSEQ + "," + "\n" + "TransmissionDate..:"
        + this.TransmissionDate + "," + "\n" + "ReferenceNumber..:" + this.ReferenceNumber + ","
        + "\n" + "ProductID..:" + this.ProductID + "," + "\n" + "ZipCode..:" + this.ZipCode + ","
        + "\n" + "AskAnswerCode..:" + this.AskAnswerCode + "," + "\n" + "DeliveryDateText..:"
        + this.DeliveryDateText + "," + "\n" + "SortValue..:" + this.SortValue + "," + "\n"
        + "RetrievalFlag..:" + this.RetrievalFlag + "," + "\n" + "fromMessageNumber..:"
        + this.fromMessageNumber + "," + "\n" + "toMessageNumber..:" + this.toMessageNumber + ","
        + "\n" + "fromMessageDate..:" + this.fromMessageDate + "," + "\n" + "toMessageDate..:"
        + this.toMessageDate + "," + "\n" + "CombinedReportNumber..:" + this.CombinedReportNumber
        + "," + "\n" + "RofNumber..:" + this.RofNumber + "," + "\n" + "OverUnderCharge..:"
        + this.OverUnderCharge + "," + "\n" + "ADJReasonCode..:" + this.ADJReasonCode + "," + "\n"
        + "retrievalFromDate..:" + this.retrievalFromDate + "," + "\n" + "retrievalFromMessage..:"
        + this.retrievalFromMessage + "," + "\n" + "retrievalToDate..:" + this.retrievalToDate
        + "," + "\n" + "retrievalToMessage..:" + this.retrievalToMessage + "," + "\n"
        + "viewQueue..:" + this.viewQueue + "," + "\n" + "direction..:" + this.direction + ","
        + "\n" + "suffix..:" + this.suffix + "," + "\n" + "orderSequence..:" + this.orderSequence
        + "," + "\n" + "adminSequence..:" + this.adminSequence;

  }

  public String getViewQueue() {
    return viewQueue;
  }

  public void setViewQueue(String value) {
    viewQueue = value;
  }

  public String getRetrievalToMessage() {
    return retrievalToMessage;
  }

  public void setRetrievalToMessage(String value) {
    retrievalToMessage = value;
  }

  public String getRetrievalToDate() {
    return retrievalToDate;
  }

  public void setRetrievalToDate(String value) {
    retrievalToDate = value;
  }

  public String getRetrievalFromMessage() {
    return retrievalFromMessage;
  }

  public void setRetrievalFromMessage(String value) {
    retrievalFromMessage = value;
  }

  public String getRetrievalFromDate() {
    return retrievalFromDate;
  }

  public void setRetrievalFromDate(String value) {
    retrievalFromDate = value;
  }

  public double getOverUnderCharge() {
    return OverUnderCharge;
  }

  public void setOverUnderCharge(double value) {
    OverUnderCharge = value;
  }

  public String getADJReasonCode() {
    return ADJReasonCode;
  }

  public void setADJReasonCode(String value) {
    ADJReasonCode = value;
  }

  public String getRofNumber() {
    return RofNumber;
  }

  public void setRofNumber(String value) {
    RofNumber = value;
  }

  public String getCombinedReportNumber() {
    return CombinedReportNumber;
  }

  public void setCombinedReportNumber(String value) {
    CombinedReportNumber = value;
  }

  public String getMercuryID() {
    return MercuryID;
  }

  public void setMercuryID(String newMercuryID) {
    MercuryID = newMercuryID;
  }

  public String getMercuryMessageNumber() {
    return MercuryMessageNumber;
  }

  public void setMercuryMessageNumber(String newMercuryMessageNumber) {
    MercuryMessageNumber = newMercuryMessageNumber;
  }

  public String getMercuryOrderNumber() {
    return MercuryOrderNumber;
  }

  public void setMercuryOrderNumber(String newMercuryOrderNumber) {
    MercuryOrderNumber = newMercuryOrderNumber;
  }

  public String getMercuryStatus() {
    return MercuryStatus;
  }

  public void setMercuryStatus(String newMercuryStatus) {
    MercuryStatus = newMercuryStatus;
  }

  public String getMessageType() {
    return MessageType;
  }

  public void setMessageType(String newMessageType) {
    MessageType = newMessageType;
  }

  public String getOutboundID() {
    return OutboundID;
  }

  public void setOutboundID(String newOutboundID) {
    OutboundID = newOutboundID;
  }

  public String getSendingFlorist() {
    return SendingFlorist;
  }

  public void setSendingFlorist(String newSendingFlorist) {
    SendingFlorist = newSendingFlorist;
  }

  public String getFillingFlorist() {
    return FillingFlorist;
  }

  public void setFillingFlorist(String newFillingFlorist) {
    FillingFlorist = newFillingFlorist;
  }

  public Date getOrderDate() {
    return OrderDate;
  }

  public void setOrderDate(Date newOrderDate) {
    OrderDate = newOrderDate;
  }

  public String getRecipient() {
    return Recipient;
  }

  public void setRecipient(String newRecipient) {
    Recipient = newRecipient;
  }

  public String getAddress() {
    return Address;
  }

  public void setAddress(String newAddress) {
    Address = newAddress;
  }

  public String getCityStateZip() {
    return CityStateZip;
  }

  public void setCityStateZip(String newCityStateZip) {
    CityStateZip = newCityStateZip;
  }

  public String getPhoneNumber() {
    return PhoneNumber;
  }

  public void setPhoneNumber(String newPhoneNumber) {
    PhoneNumber = newPhoneNumber;
  }

  public Date getDeliveryDate() {
    return DeliveryDate;
  }

  public void setDeliveryDate(Date newDeliveryDate) {
    DeliveryDate = newDeliveryDate;
  }

  public String getFirstChoice() {
    return FirstChoice;
  }

  public void setFirstChoice(String newFirstChoice) {
    FirstChoice = newFirstChoice;
  }

  public String getSecondChoice() {
    return SecondChoice;
  }

  public void setSecondChoice(String newSecondChoice) {
    SecondChoice = newSecondChoice;
  }

  public Double getPrice() {
    return Price;
  }

  public void setPrice(Double newPrice) {
    Price = newPrice;
  }

  public String getCardMessage() {
    return CardMessage;
  }

  public void setCardMessage(String newCardMessage) {
    CardMessage = newCardMessage;
  }

  public String getOccasion() {
    return Occasion;
  }

  public void setOccasion(String newOccasion) {
    Occasion = newOccasion;
  }

  public String getSpecialInstructions() {
    return SpecialInstructions;
  }

  public void setSpecialInstructions(String newSpecialInstructions) {
    SpecialInstructions = newSpecialInstructions;
  }

  public String getPriority() {
    return Priority;
  }

  public void setPriority(String newPriority) {
    Priority = newPriority;
  }

  public String getOperator() {
    return Operator;
  }

  public void setOperator(String newOperator) {
    Operator = newOperator;
  }

  public String getComments() {
    return Comments;
  }

  public void setComments(String newComments) {
    Comments = newComments;
  }

  public String getSakText() {
    return SakText;
  }

  public void setSakText(String newSakText) {
    SakText = newSakText;
  }

  public int getCTSEQ() {
    return CTSEQ;
  }

  public void setCTSEQ(int newCTSEQ) {
    CTSEQ = newCTSEQ;
  }

  public int getCRSEQ() {
    return CRSEQ;
  }

  public void setCRSEQ(int newCRSEQ) {
    CRSEQ = newCRSEQ;
  }

  public Date getTransmissionDate() {
    return TransmissionDate;
  }

  public void setTransmissionDate(Date newTransmissionDate) {
    TransmissionDate = newTransmissionDate;
  }

  public String getReferenceNumber() {
    return ReferenceNumber;
  }

  public void setReferenceNumber(String newReferenceNumber) {
    ReferenceNumber = newReferenceNumber;
  }

  public String getProductID() {
    return ProductID;
  }

  public void setProductID(String newProductID) {
    ProductID = newProductID;
  }

  public String getZipCode() {
    return ZipCode;
  }

  public void setZipCode(String newZipCode) {
    ZipCode = newZipCode;
  }

  public String getAskAnswerCode() {
    return AskAnswerCode;
  }

  public void setAskAnswerCode(String value) {
    AskAnswerCode = value;
  }

  public String getDeliveryDateText() {
    return DeliveryDateText;
  }

  public void setDeliveryDateText(String value) {
    DeliveryDateText = value;
  }

  public String getSortValue() {
    return SortValue;
  }

  public void setSortValue(String value) {
    SortValue = value;
  }

  public String getRetrievalFlag() {
    return RetrievalFlag;
  }

  public void setRetrievalFlag(String value) {
    RetrievalFlag = value;
  }

  public Object clone() {
    return this;
  }

  public void setDirection(String direction) {
    this.direction = direction;
  }

  public String getDirection() {
    return direction;
  }

  public void setSuffix(String suffix) {
    this.suffix = suffix;
  }

  public String getSuffix() {
    return suffix;
  }

  public void setOrderSequence(String orderSequence) {
    this.orderSequence = orderSequence;
  }

  public String getOrderSequence() {
    return orderSequence;
  }

  public void setAdminSequence(String adminSequence) {
    this.adminSequence = adminSequence;
  }

  public String getAdminSequence() {
    return adminSequence;
  }

  public void setOrderReference(String orderReference) {
    this.orderReference = orderReference;
  }

  public String getOrderReference() {
    return orderReference;
  }

  public void setAdminReference(String adminReference) {
    this.adminReference = adminReference;
  }

  public String getAdminReference() {
    return adminReference;
  }

  public void setRecipientCountryCode(String recipientCountryCode) {
    this.recipientCountryCode = recipientCountryCode;
  }

  public String getRecipientCountryCode() {
    return recipientCountryCode;
  }

  public void setCorrelationReference(String correlationReference) {
    this.correlationReference = correlationReference;
  }

  public String getCorrelationReference() {
    return correlationReference;
  }

  public void setService(String service) {
    this.service = service;
  }

  public String getService() {
    return service;
  }

  public void setDeliveryDetails(String deliveryDetails) {
    this.deliveryDetails = deliveryDetails;
  }

  public String getDeliveryDetails() {
    return deliveryDetails;
  }

  public void setTagLine(String tagLine) {
    this.tagLine = tagLine;
  }

  public String getTagLine() {
    return tagLine;
  }

  public void setRecipientCity(String recipientCity) {
    this.recipientCity = recipientCity;
  }

  public String getRecipientCity() {
    return recipientCity;
  }

  public void setRecipientState(String recipientState) {
    this.recipientState = recipientState;
  }

  public String getRecipientState() {
    return recipientState;
  }

  public void setFirstChoiceCode(String firstChoiceCode) {
    this.firstChoiceCode = firstChoiceCode;
  }

  public String getFirstChoiceCode() {
    return firstChoiceCode;
  }

  public void setFirstChoiceDesc(String firstChoiceDesc) {
    this.firstChoiceDesc = firstChoiceDesc;
  }

  public String getFirstChoiceDesc() {
    return firstChoiceDesc;
  }

  public void setSecondChoiceCode(String secondChoiceCode) {
    this.secondChoiceCode = secondChoiceCode;
  }

  public String getSecondChoiceCode() {
    return secondChoiceCode;
  }

  public void setSecondChoiceDesc(String secondChoiceDesc) {
    this.secondChoiceDesc = secondChoiceDesc;
  }

  public String getSecondChoiceDesc() {
    return secondChoiceDesc;
  }

  public void setConventionMessage(String conventionMessage) {
    this.conventionMessage = conventionMessage;
  }

  public String getConventionMessage() {
    return conventionMessage;
  }

public void setGotoFloristFlag(String gotoFloristFlag) {
	this.gotoFloristFlag = gotoFloristFlag;
}

public String getGotoFloristFlag() {
	return gotoFloristFlag;
}

public void setFloristSuspendStatus(String floristSuspendStatus) {
	this.floristSuspendStatus = floristSuspendStatus;
}

public String getFloristSuspendStatus() {
	return floristSuspendStatus;
}

public void setFloristAllowsForwards(String floristAllowsForwards) {
	this.floristAllowsForwards = floristAllowsForwards;
}

public String getFloristAllowsForwards() {
	return floristAllowsForwards;
}

}