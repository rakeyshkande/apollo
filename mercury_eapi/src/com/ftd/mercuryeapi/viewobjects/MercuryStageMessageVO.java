package com.ftd.mercuryeapi.viewobjects;

import java.util.Date;

public class MercuryStageMessageVO {

  // declare variables
  private String messageType;
  private Long messageId;
  private String referenceNumber;
  private Date messageReceivedDate;
  private String status;
  private String mainMemberCode;
  private String reason;
  private String createdBy;
  private String updatedBy;
  private Date createdOn;
  private Date updatedOn;

  // getters and setter methods
  public String getMessageType() {
    return messageType;
  }

  public void setMessageType(String messageType) {
    this.messageType = messageType;
  }

  public Long getMessageId() {
    return messageId;
  }

  public void setMessageId(Long messageId) {
    this.messageId = messageId;
  }

  public String getReferenceNumber() {
    return referenceNumber;
  }

  public void setReferenceNumber(String referenceNumber) {
    this.referenceNumber = referenceNumber;
  }

  public Date getMessageReceivedDate() {
    return messageReceivedDate;
  }

  public void setMessageReceivedDate(Date messageReceivedDate) {
    this.messageReceivedDate = messageReceivedDate;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getMainMemberCode() {
    return mainMemberCode;
  }

  public void setMainMemberCode(String mainMemberCode) {
    this.mainMemberCode = mainMemberCode;
  }

  public String getReason() {
    return reason;
  }

  public void setReason(String reason) {
    this.reason = reason;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public String getUpdatedBy() {
    return updatedBy;
  }

  public void setUpdatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
  }

  public Date getCreatedOn() {
    return createdOn;
  }

  public void setCreatedOn(Date createdOn) {
    this.createdOn = createdOn;
  }

  public Date getUpdatedOn() {
    return updatedOn;
  }

  public void setUpdatedOn(Date updatedOn) {
    this.updatedOn = updatedOn;
  }

}
