package com.ftd.mercuryeapi.viewobjects;

import java.util.Calendar;

public class ErosTimeVO {

  private Calendar erosDownStartTime;
  private Calendar erosDownEndTime;
  private Calendar erosAlertTime;
  private boolean isErosDown;

  public ErosTimeVO() {
  }

  public void setErosAlertTime(Calendar value) {
    erosAlertTime = value;
  }

  public Calendar getErosAlertTime() {
    return erosAlertTime;
  }

  public void setErosDownEndTime(Calendar value) {
    erosDownEndTime = value;
  }

  public Calendar getErosDownEndTime() {
    return erosDownEndTime;
  }

  public void setErosDownStartTime(Calendar value) {
    erosDownStartTime = value;
  }

  public Calendar getErosDownStartTime() {
    return erosDownStartTime;
  }

public void setErosDown(boolean isErosDown) {
	this.isErosDown = isErosDown;
}

public boolean isErosDown() {
	return isErosDown;
}

}