package com.ftd.mercuryeapi.services;

/*
 * Copyright 2013 by United Online, Inc., 3113 Woodcreek Drive, Downers Grove, IL, 60515,
 * U.S.A. All rights reserved. This software is the confidential and proprietary
 * information of United Online, Inc. ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with United Online.
 */
/*
 * ##################$History Card$############################################### ####
 * Latest chages description should be on the top of the history card list###
 * ############################################################################### Created
 * Date Updated Date Author Change Description ============ ============ ======
 * =================== 07/09/2013 smotla initial creation
 */

/**
 * @author smotla this exception class is used to thrown by mercury eapi application as
 *         and when there is a fatal,error or unexpected issues while performing a task
 */
public class MercuryEapiException extends Exception {

  /**
   * This is used in serialization
   */
  private static final long serialVersionUID = 1L;

  /**
   * default constructor and calls super class default constructor
   */
  public MercuryEapiException() {
    super();
  }

  /**
   * this builds the customer service exception with supplied customised message and
   * exception
   * 
   * @param message
   * @param cause
   */
  public MercuryEapiException(String message, Throwable cause) {
    super(message, cause);
  }

  /**
   * This builds the customer service exception using supplied customised error message
   * 
   * @param message
   */
  public MercuryEapiException(String message) {
    super(message);
  }

  /**
   * this method is used to build the validation failure exception using the cause
   * 
   * @param cause
   */
  public MercuryEapiException(Throwable cause) {
    super(cause);
  }

}
