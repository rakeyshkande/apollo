package com.ftd.mercuryeapi.services;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.naming.InitialContext;
import javax.xml.soap.SOAPFault;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.soap.SOAPFaultException;

import com.ftd.eapi.message.service.v1.CodeMessagePair;
import com.ftd.eapi.message.service.v1.ConfirmResponse;
import com.ftd.eapi.message.service.v1.EAPIMessageService;
import com.ftd.eapi.message.service.v1.EAPIMessageServiceException;
import com.ftd.eapi.message.service.v1.EapiException;
import com.ftd.eapi.message.service.v1.EapiMessageServiceFault;
import com.ftd.eapi.message.service.v1.ErosMessage;
import com.ftd.eapi.message.service.v1.ErrorCode;
import com.ftd.eapi.message.service.v1.MultiMessageNewListResponse;
import com.ftd.eapi.message.service.v1.NewMessage;
import com.ftd.eapi.message.service.v1.NewMessageList;
import com.ftd.eapi.message.service.v1.ProcessedErosMessage;
import com.ftd.eapi.message.service.v1.SimpleResponse;
import com.ftd.eapi.message.service.v1.SingleMessageResponse;
import com.ftd.mercuryeapi.dao.MercuryDAO;
import com.ftd.mercuryeapi.util.MercuryEapiConstants;
import com.ftd.mercuryeapi.util.MercuryEapiObjectMapper;
import com.ftd.mercuryeapi.util.MercuryEapiUtil;
import com.ftd.mercuryeapi.validators.MercuryMessageValidator;
import com.ftd.mercuryeapi.viewobjects.ErosTimeVO;
import com.ftd.mercuryeapi.viewobjects.MercuryEapiOpsVO;
import com.ftd.mercuryeapi.viewobjects.MercuryMessageVO;
import com.ftd.mercuryeapi.viewobjects.MercuryStageMessageVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;

/**
 * This object contains methods to handle the mercury messages
 *
 * @TODO - do not store message into MERCURYINBOUND Queue for GEN and REJ messages
 * @author smotla
 */
public class MercuryEapiMessageMgmt {

  private Logger logger;

  private static String LOGGER_CATEGORY = "com.ftd.mercuryeapi.services.MercuryEapiMessageMgmt";
  private static final String CLASS_NAME = "MercuryEapiMessageMgmt";
  private int counter = 0;
  private boolean sendInboundMessages = true;

  public MercuryEapiMessageMgmt() {
    logger = new Logger(LOGGER_CATEGORY);
  }

  /**
   * This method is used to handle the post messages
   *
   * @param postMessages
   * @throws EAPIMessageServiceException
   * @throws Exception
   */
  public void processPOSTMessages(String mercuryId) throws MercuryEapiException {

	Long startPostTime = System.currentTimeMillis();

    // declare and initialize the variables
    final String METHOD_NAME = ".processPOSTMessages()";
    StringBuilder errorMessage = null;
    MercuryMessageVO mercuryMessage = null;

    // exit the process if EAPI is not enabled to process mercury orders
    if (!MercuryEapiUtil.isEAPIEnabled()) {
      logger.info(CLASS_NAME + METHOD_NAME + " EAPI is not enabled. Process will exit");
      return;
    }

    // exit the process if EAPI is down
    try {
        ErosTimeVO erosTimeVO = MercuryEapiUtil.isEROSDown();
        if (erosTimeVO.isErosDown()) {
            logger.info(CLASS_NAME + METHOD_NAME + " Current time is in EAPI's downtime.");
            long jmsDelay = calculateJmsDelay(erosTimeVO);
            logger.info("jmsDelay: " + jmsDelay);
            if (jmsDelay < 0) {
            	jmsDelay = 900;
            }
            this.sendPostQueueMessage(null, mercuryId, String.valueOf(jmsDelay));
            return;
        }
    } catch (Exception e) {
    	logger.error(e);
    }

    // get the mercury messages with status MO
    mercuryMessage = this.getMercuryMessageToBeProcessed(mercuryId);

    if (mercuryMessage == null) {
        // log the message if the Mercury record is not retrieved
        errorMessage = new StringBuilder();
        errorMessage.append(CLASS_NAME).append(METHOD_NAME)
            .append(" - No record returned from Mercury table. Process will exit.");
        logger.info(errorMessage.toString());
        return;
    }

    // send the mercury messages to the EAPI
    this.sendPOSTMessageToEAPI(mercuryMessage);

    logger.info("Total time to process " + mercuryId + " is "
            + (System.currentTimeMillis() - startPostTime));

  }

  /**
   * Send the mercury message to the EAPI
   *
   * @param mainMemberCode
   * @param mercuryMessageList
   */
  private void sendPOSTMessageToEAPI(MercuryMessageVO mercuryMessage)
      throws MercuryEapiException {

    final String METHOD_NAME = ".sendPOSTMessageToEAPI(mercuryMessageVO)";
    StringBuilder errorMessage = null;
    String mainMemberCode = null;

    try {

        // Step 1: build the EROS message from Mercury Message VO and get the main
        // member code
    	MercuryEapiObjectMapper objectMapper = new MercuryEapiObjectMapper();
        ErosMessage erosMessage = objectMapper.buildErosMessage(mercuryMessage);
        mainMemberCode = erosMessage.getSendingMemberCode();
        if (logger.isDebugEnabled()) {
            logger.debug("mainMemberCode: " + mainMemberCode);
        }

        // Step 2: get EAPIWS instance for POST_PROCESS
        WebServiceClientFactory wscFactory = new WebServiceClientFactory();
        EAPIMessageService eapiWSService = wscFactory.getEAPIMessageService(
            mainMemberCode, MercuryEapiConstants.POST_PROCESS);

        logger.info(CLASS_NAME + METHOD_NAME + "*** Request Message for Post Process  ***"
            + MercuryEapiUtil.postProcessRequestMessage(mercuryMessage));

        logger.info("\nErosMessage: " + erosMessage.getOperator() +
        		" " + erosMessage.getExternalReferenceNumber() +
        		" " + erosMessage.getSendingMemberCode() +
        		" " + erosMessage.getReceivingMemberCode());

        Long startTimepostMessage = System.currentTimeMillis();
        SingleMessageResponse singleMessageResponse = eapiWSService.postMessage(mainMemberCode,
            erosMessage);
        logger.info("**** Total time taken to get the response for postMessage: "
            + (System.currentTimeMillis() - startTimepostMessage));

        // Step 4: process EAPI's response received for the posted request
        this.processPOSTMessageResponse(singleMessageResponse, mercuryMessage);

    }
    catch (EAPIMessageServiceException eapiException) {
    	logger.error(eapiException);
        errorMessage = new StringBuilder();
        errorMessage.append("EAPIMessageServiceException Occured: ");
        EapiMessageServiceFault eapiFault = eapiException.getFaultInfo();
        errorMessage.append(eapiFault.getMessage())
            .append(". ").append(eapiException.getMessage());
        logger.error(errorMessage.toString());
        List<EapiException> exceptionList = eapiFault.getExceptions();
        String exceptionCode = null;
        String exceptionMessage = null;
        for (int i=0; i<exceptionList.size(); i++) {
        	EapiException ee = exceptionList.get(i);
        	ErrorCode eCode = ee.getCode();
        	exceptionCode = eCode.name();
        	exceptionMessage = ee.getMessage();
        	logger.error(exceptionCode);
        	logger.error(exceptionMessage);
        }
        if (exceptionCode != null && !exceptionCode.equalsIgnoreCase("INTERNAL_SERVER_ERROR")
            && !exceptionCode.equalsIgnoreCase("SECURITY_ERROR")) {
        	logger.info("Set MERCURY_STATUS to ME");
        	mercuryMessage.setMercuryStatus("ME");
        	mercuryMessage.setMercuryOrderNumber(mercuryMessage.getMercuryID());
        	mercuryMessage.setMercuryMessageNumber(mercuryMessage.getMercuryID());
        	mercuryMessage.setComments("[" + exceptionCode + "] " + exceptionMessage);
        	mercuryMessage.setTransmissionDate(new Date());
        	sendInboundMessages = true;
        	this.updateMercury(mercuryMessage);
        } else {
            this.handlePostException(mercuryMessage.getMercuryID(), mainMemberCode);
        }
    } catch (MercuryEapiException mee) {
        errorMessage = new StringBuilder();
        errorMessage.append("MercuryEapiException Occured: ");
        errorMessage.append(mee);
        logger.error(errorMessage.toString());
        logger.error(mee);
        this.handlePostException(mercuryMessage.getMercuryID(), mainMemberCode);
    } catch (Exception e) {
        errorMessage = new StringBuilder();
        errorMessage.append("Exception Occured: ");
        errorMessage.append(e);
        logger.error(errorMessage.toString());
        logger.error(e);
        this.handlePostException(mercuryMessage.getMercuryID(), mainMemberCode);
    }
  }

  /**
   * This method is used to get the mercury record from Mercury.mercury table
   *
   * @param mercuryId
   * @return
   */
  private MercuryMessageVO getMercuryMessageToBeProcessed(String mercuryId)
      throws MercuryEapiException {

    // declare and initialize the variables
    final String METHOD_NAME = ".getMercuryMessageToBeProcessed(mercuryId)";
    MercuryMessageVO message = null;
    Connection conn = null;
    StringBuilder errorMessage = null;

    try {

      // get the connection
      conn = MercuryEapiUtil.getConnection();

      // throw the exception if connection is null
      if (conn == null) {
        errorMessage = new StringBuilder();
        errorMessage.append(CLASS_NAME).append(METHOD_NAME).append(" DB Connection is null. ");
        throw new MercuryEapiException(errorMessage.toString());
      }

      MercuryDAO mercuryDAO = new MercuryDAO(conn);

      // get the mercury messages with the status MO
      message = mercuryDAO.getMercuryMessageById(mercuryId);

    }
    catch (Exception e) {

      errorMessage = new StringBuilder();
      errorMessage
          .append(CLASS_NAME)
          .append(METHOD_NAME)
          .append("- Exception Occured while reading the mercury record to be sent to EAPI.");
      logger.error(errorMessage.toString());
      logger.error(e);
      throw new MercuryEapiException(e);

    }
    finally {

      // close the connection
      if (conn != null) {
        try {
          conn.close();
        }
        catch (SQLException e) {

          errorMessage = new StringBuilder();
          errorMessage
              .append(CLASS_NAME)
              .append(METHOD_NAME)
              .append(
                  "- SQL Exception is caught and unable to close the existing connection after reading the mercury post messages.");
          logger.error(errorMessage.toString());
          throw new MercuryEapiException(e);

        }
      }
    }
    return message;
  }

  /**
   * This method gets the new messages from EAPI to be processed by Apollo
   *
   * @param suffix
   * @throws Exception
   */
  public void getNewMessagesFromEapi(String mainMemberCode) throws Exception {

    // declare and initialize the variables
    final String METHOD_NAME = ".getNewMessagesFromEapi(mainMemberCode)";
    StringBuilder errorMessage = null;
    Connection conn = null;
    boolean moreRecordsToProcess = false;

    if (logger.isDebugEnabled()) {
        logger.debug(CLASS_NAME + METHOD_NAME + " - mainMemberCode: " + mainMemberCode);
    }

    // validate mainMemberCode
    MercuryMessageValidator mercuryMessageValidator = new MercuryMessageValidator();
    mercuryMessageValidator.validateMainMemberCode(mainMemberCode);

    // return if eapi is not enabled
    if (!MercuryEapiUtil.isEAPIEnabled()) {
      logger.info(CLASS_NAME + METHOD_NAME + " EAPI is not enabled. Process will exit.");
      return;
    }

    // exit the process if EAPI is down
    try {
        ErosTimeVO erosTimeVO = MercuryEapiUtil.isEROSDown();
        if (erosTimeVO.isErosDown()) {
            logger.info(CLASS_NAME + METHOD_NAME + " Current time is in EAPI's downtime.");
            this.sendPollQueueMessage(mainMemberCode, false, null);
            return;
        }
    } catch (Exception e) {
    	logger.error(e);
    }

    MercuryEapiOpsVO mercuryEapiOpsVO = null;
    MercuryDAO mercuryDAO = null;

    try {

      // Get the connection
      conn = MercuryEapiUtil.getConnection();

      // throw exception if the connection is null
      if (conn == null) {
        errorMessage = new StringBuilder();
        errorMessage.append(CLASS_NAME).append(METHOD_NAME).append(" DB Connection is null. ");
        logger.error(errorMessage.toString());
        throw new MercuryEapiException(errorMessage.toString());
      }

      // get MercuryDAO instance
      mercuryDAO = new MercuryDAO(conn);

      mercuryEapiOpsVO = mercuryDAO.getMercuryEapiOps(mainMemberCode);
      if (mercuryEapiOpsVO == null) {
    	  String stopMessage = "No MERCURY_EAPI_OPS record for " + mainMemberCode;
    	  logger.error(stopMessage);
      } else if (!mercuryEapiOpsVO.isAvailable()) {
    	  String stopMessage = mainMemberCode + " is not available in MERCURY_EAPI_OPS. Stopping process.";
    	  logger.error(stopMessage);
      } else if (mercuryEapiOpsVO.isInUse()) {
    	  String stopMessage = mainMemberCode + " is in use. Stopping process.";
    	  logger.error(stopMessage);
      } else {

          mercuryEapiOpsVO.setInUse(true);
          mercuryEapiOpsVO.setUpdatedBy("POLL_MDB");
          mercuryDAO.updateMercuryEapiOps(mercuryEapiOpsVO);

          // Get the eapi WS instance
          WebServiceClientFactory wscFactory = new WebServiceClientFactory();
          EAPIMessageService eapiWSService = wscFactory.getEAPIMessageService(
              mainMemberCode, MercuryEapiConstants.POLLING_PROCESS);

          // First send confirm messages to the eapi for records having status
          // RECEIVED. If previous confirmation is failed then this would help in
          // re-confirming it to eapi before getting the new messages from eapi
          this.sendConfirmMessageToEapi(eapiWSService, mainMemberCode, mercuryDAO);

          // Get the new messages
          int messageLimit = MercuryEapiUtil.getPollMessageLimit();

          logger.info(CLASS_NAME + METHOD_NAME
              + " - Poll EAPI to get the new messages for mainmembercode: " + mainMemberCode
              + ", message limit: " + messageLimit);

          MultiMessageNewListResponse multiMessageNewListResponse = eapiWSService.getNewMessageList(
              mainMemberCode, messageLimit);

          NewMessageList newMessageList = multiMessageNewListResponse.getNewMessageList();

          // Return if there are no messages to store
          if (newMessageList == null || newMessageList.getNewMessage() == null
              || newMessageList.getNewMessage().size() <= 0) {

              errorMessage = new StringBuilder();
              errorMessage.append(CLASS_NAME).append(METHOD_NAME).append(" - There are no new messages.");
              logger.info(errorMessage.toString());
              moreRecordsToProcess = false;
          } else {
              logger.info(CLASS_NAME + METHOD_NAME + " - Total number of messages retrieved from EAPI:"
                  + newMessageList.getNewMessage().size());

              // Store the new message metadata into the staging table
              HashMap<String, String> messageMap = this.storeNewMercuryMessages(newMessageList.getNewMessage(),
                  mainMemberCode, mercuryDAO);

              if (messageMap == null || messageMap.size() <= 0) {
                  logger.info(CLASS_NAME + METHOD_NAME + " - Message list is empty.");
                  moreRecordsToProcess = false;
              } else  {

                  // Send confirmation to eapi - it requires reference number list
                  if (logger.isDebugEnabled()) {
                      logger.debug(CLASS_NAME
                          + METHOD_NAME
                          + " - Sending confirmation for the messages stored in the staging table for mainMemberCode: "
                          + mainMemberCode);
                  }

                  List<String> refNumList = new ArrayList<String>();
                  Iterator it = messageMap.keySet().iterator();
                  while (it.hasNext()) {
                  	  String key = (String) it.next();
                  	  String value = messageMap.get(key);
                  	  refNumList.add(key);
                  }
                  ConfirmResponse confirmResponse = eapiWSService.confirmMessageByReference(mainMemberCode,
                      refNumList);

                  // Process confirm response
                  this.processConfirmNewMessageResponse(confirmResponse, mercuryDAO, messageMap);

                  // call recursively if there are more new messages to fetch
                  if (multiMessageNewListResponse.isHasMoreMessages()) {
        	          moreRecordsToProcess = true;
                  }
              }
          }

          mercuryEapiOpsVO.setInUse(false);
          mercuryEapiOpsVO.setUpdatedBy("POLL_MDB");
          mercuryDAO.updateMercuryEapiOps(mercuryEapiOpsVO);

          this.sendPollQueueMessage(mainMemberCode, moreRecordsToProcess, null);

      }

    } catch (EAPIMessageServiceException eapiException) {

        errorMessage = new StringBuilder();
        errorMessage.append(mainMemberCode + " - EAPIMessageServiceException is caught: ")
            .append(eapiException.getMessage())
            .append("\n" + eapiException);
        EapiMessageServiceFault eapiFault = eapiException.getFaultInfo();
        errorMessage.append("\n" + eapiFault.getReasonCode() + " " + eapiFault.getMessage());
        logger.error(errorMessage.toString());
        logger.error(eapiException);
        this.handlePollException(mercuryEapiOpsVO, mercuryDAO, errorMessage.toString());

    } catch (SOAPFaultException soapException) {

        errorMessage = new StringBuilder();
        errorMessage.append(mainMemberCode + " - SOAPFaultException is caught: ")
            .append(soapException.getMessage());
        SOAPFault soapFault = soapException.getFault();
        errorMessage.append("\n" + soapFault.getFaultCode() + " " + soapFault.getValue());
        logger.error(errorMessage.toString() + "\n" +
            soapException.toString() + "\n" +
            soapException.getLocalizedMessage() + "\n" + soapException);
        logger.error(soapException.getFault().getFaultString());
        logger.error(soapException);
        this.handlePollException(mercuryEapiOpsVO, mercuryDAO, errorMessage.toString());

    } catch (MercuryEapiException mee) {

        errorMessage = new StringBuilder();
        errorMessage.append(mainMemberCode + " - MercuryEapiException is caught: ")
            .append(mee.getMessage()+ " "+ mee.toString())
            .append("\n" + mee);
        logger.error(errorMessage.toString());
        logger.error(mee);
        this.handlePollException(mercuryEapiOpsVO, mercuryDAO, errorMessage.toString());

    } catch (Exception e) {

        errorMessage = new StringBuilder();
        errorMessage.append(mainMemberCode + " - Exception is caught: ")
            .append(e.getMessage() + " " + e.toString())
            .append("\n" + e);
        logger.error(errorMessage.toString());
        logger.error(e);
        this.handlePollException(mercuryEapiOpsVO, mercuryDAO, errorMessage.toString());

    } finally {

      // close the connection
      if (conn != null) {
        try {
          conn.close();
        }
        catch (SQLException e) {

          errorMessage = new StringBuilder();
          errorMessage
              .append(CLASS_NAME)
              .append(METHOD_NAME)
              .append(
                  "- SQL Exception is caught and unable to close the existing connection after getting the new messages from EAPI.");
          logger.error(errorMessage.toString());
          throw new MercuryEapiException(e);
        }
      }
    }

    return;
  }

  /**
   * This method is used to store the new messages
   *
   * @param multiMessageNewListResponse
   */
  private HashMap<String, String> storeNewMercuryMessages(List<NewMessage> messageListFromEapi,
      String mainMemberCode, MercuryDAO mercuryDAO) throws MercuryEapiException {

    final String METHOD_NAME = ".storeNewMercuryMessages()";
    StringBuilder errorMessage = null;

    HashMap<String, String> messageMap = new HashMap<String, String>();

    try {

      for (NewMessage newMessage : messageListFromEapi) {
        MercuryStageMessageVO mercuryStageMessageVO = null;

        try {
          // build the mercury new message object
        	MercuryEapiObjectMapper objectMapper = new MercuryEapiObjectMapper();
        	mercuryStageMessageVO = objectMapper.buildMercuryStageMessageVO(newMessage,
              mainMemberCode);

          if (mercuryStageMessageVO == null) {

            errorMessage = new StringBuilder();
            errorMessage.append(CLASS_NAME).append(METHOD_NAME)
                .append(" :Mercury New Message VO is null.");
            logger.error(errorMessage.toString());
            MercuryEapiUtil.sendSystemMessageWrapper(errorMessage.toString());
            continue;
          }

          // set the status and suffix
          mercuryStageMessageVO.setMainMemberCode(mainMemberCode);
          mercuryStageMessageVO.setStatus(MercuryEapiConstants.RECEIVED);

          // store the mercury new message into the Mercury Eapi Staging Table
          String status = mercuryDAO.storeMercuryEapiStagingMessage(mercuryStageMessageVO);

          // Reference numbers are added to list, because we need to send
          // confirmation to Eapi that we received the messages with the
          // reference numbers send the confirmation messages to the only
          // successful records
          if (MercuryEapiConstants.Y_STRING.equalsIgnoreCase(status)) {
            messageMap.put(newMessage.getReferenceNumber(), Long.toString(newMessage.getMessageId()));
            logger.info(CLASS_NAME + METHOD_NAME
                    + " - Successfully stored the new message in the staging table"
                    + "\nMessage Id: " + mercuryStageMessageVO.getMessageId()
                    + "  Reference Number: " + mercuryStageMessageVO.getReferenceNumber()
                    + "  Message Type: " + mercuryStageMessageVO.getMessageType());
          }
          else {

            errorMessage = new StringBuilder();
            errorMessage.append(CLASS_NAME).append(METHOD_NAME)
                .append(" - Unable to store the new message with message id :")
                .append(newMessage.getMessageId());
            logger.error(errorMessage.toString());
            MercuryEapiUtil.sendSystemMessageWrapper(errorMessage.toString());
            continue;

          }
        }
        catch (Exception e) {
          errorMessage = new StringBuilder();
          errorMessage
              .append(CLASS_NAME)
              .append(METHOD_NAME)
              .append(
                  " -Exception is caught.Problem while stroring the new message with message id :.")
              .append(newMessage.getMessageId());
          logger.error(errorMessage.toString());
          logger.error(e);
          MercuryEapiUtil.sendSystemMessageWrapper(errorMessage.toString());
          continue;
        }
      }
    }
    catch (Exception e) {
      errorMessage = new StringBuilder();
      errorMessage.append(CLASS_NAME).append(METHOD_NAME)
          .append(" -Exception is caught.Problem while stroring the new messages.");
      logger.error(errorMessage.toString());
      throw new MercuryEapiException(errorMessage.toString());
    }

    return messageMap;

  }

  /**
   * This method sends the confirmation message to eapi for records left with RECEIVED
   * status in Mercury Eapi Msgs Staging Table
   */
  private void sendConfirmMessageToEapi(EAPIMessageService eapiWSService, String mainMemberCode,
      MercuryDAO mercuryDAO) throws MercuryEapiException {

    // initialise the variable to hold the method name to use for logging
    final String METHOD_NAME = ".sendConfirmMessageToEapi()";

    // initialise the variables
    StringBuilder errorMessage = null;

    try {

      // Step 1: get the mercury new messages from staging table with the status RECEIVED
      HashMap<String, String> messageMap = mercuryDAO.getMercuryStageMessagesToBeConfirmed(mainMemberCode,
          MercuryEapiConstants.RECEIVED);

      if (messageMap == null || messageMap.size() <= 0) {
        return;
      } else {
    	  logger.info("Fetched " + messageMap.size() + " messages that need to be confirmed and processed");
      }

      List<String> messageListToBeConfirmed = new ArrayList();
      Iterator it = messageMap.keySet().iterator();
      while (it.hasNext()) {
      	  String key = (String) it.next();
      	  String value = messageMap.get(key);
      	  messageListToBeConfirmed.add(key);
      }
      
      // Step 2. send the confirm message to the eapi
      ConfirmResponse confirmResponse = eapiWSService.confirmMessageByReference(mainMemberCode,
          messageListToBeConfirmed);
      if (logger.isDebugEnabled()) {
        logger.debug(CLASS_NAME + METHOD_NAME
            + " - Sent confirmation message to EAPI for mainMemberCode: " + mainMemberCode);
      }
      // Step 3. process confirm message response
      this.processConfirmNewMessageResponse(confirmResponse, mercuryDAO, messageMap);
    }
    catch (EAPIMessageServiceException e) {
      errorMessage = new StringBuilder();
      errorMessage.append(CLASS_NAME).append(METHOD_NAME)
          .append(" :EAPIMessageServiceException is caught.");
      logger.error(errorMessage.toString());
      throw new MercuryEapiException(e.getFaultInfo().getMessage());
    }
    catch (SOAPFaultException soapException) {
      errorMessage = new StringBuilder();
      errorMessage
          .append(CLASS_NAME)
          .append(METHOD_NAME)
          .append(
              "EAPI was unable to process the request as the confirm message contains errors. 'Error Message: ")
          .append(soapException.getMessage()).append(". Process will be stopped.");
      logger.error(errorMessage.toString());
      throw new MercuryEapiException(soapException);
    }
    catch (Exception e) {
      errorMessage = new StringBuilder();
      errorMessage
          .append(CLASS_NAME)
          .append(METHOD_NAME)
          .append(
              " - Exception caught. Problem while confirming the received new messages to EAPI.");
      logger.error(errorMessage.toString());
      throw new MercuryEapiException(e);
    }

  }

  /**
   * This method is used to process the POST Message response
   *
   * @param singleMessageResponse
   * @param mercuryMessageVO
   */
  private void processPOSTMessageResponse(SingleMessageResponse singleMessageResponse,
      MercuryMessageVO mercuryMessageVO) throws MercuryEapiException {

    // initialize the variable to hold the method name to use for logging
    final String METHOD_NAME = ".processPOSTMessageResponse()";
    StringBuffer sb = null;

    if (logger.isDebugEnabled()) {
      logger.debug(CLASS_NAME + METHOD_NAME
          + " POST message response processing started for mercury ID: "
          + mercuryMessageVO.getMercuryID());
    }

    // initialize the required variables
    ProcessedErosMessage processedErosMessage;
    CodeMessagePair responseCode;

    if (singleMessageResponse != null) {

      // get the response and response message from the response object
      responseCode = singleMessageResponse.getResponseCode();
      processedErosMessage = singleMessageResponse.getProcessedErosMessage();
      logger.info("responseCode: " + responseCode.getCode() + " " + responseCode.getMessage());

      // check the response code and process the message accordingly
      if (processedErosMessage != null) {

        logger.info(CLASS_NAME
              + METHOD_NAME
              + "*** Response Message for Post Process  ***"
              + MercuryEapiUtil.postProcessResponseMessage(processedErosMessage,
                  mercuryMessageVO.getMercuryID()));

        if (MercuryEapiConstants.VERIFIED.equalsIgnoreCase(responseCode.getCode())) {
          // handle success response
          this.handlePostMessgeSuccessResponse(processedErosMessage, mercuryMessageVO);
        }
        else if (MercuryEapiConstants.REJECTED.equalsIgnoreCase(responseCode.getCode())) {
          // handle rejected response
          this.handlePostMessgeFailureResponse(processedErosMessage, mercuryMessageVO);
        }

        if (logger.isDebugEnabled()) {
          logger.debug(METHOD_NAME + " POST message response processing is done for mercury id: "
              + mercuryMessageVO.getMercuryID());
        }
      }
      else {
        // if the response message is null then update the mercury status to MO
        // so that the mercury message will be retried again
        sb = new StringBuffer();
        sb.append(CLASS_NAME).append(METHOD_NAME).append(" EAPI's response for mercury ID: ")
            .append(mercuryMessageVO.getMercuryID())
            .append(" is null. Changing mercury status to MO in order to retry again");
        logger.info(sb.toString());
        mercuryMessageVO.setSakText(sb.toString());
        mercuryMessageVO.setTransmissionDate(new java.util.Date());
        // reset the status to MO
        mercuryMessageVO.setMercuryStatus(MercuryEapiConstants.MERCURY_OPEN);
        // do not send Mercury Inbound Message for this request
        sendInboundMessages = false;

        logger.info(METHOD_NAME
            + " EAPI's response is null. So updating the mercury status to MO for mercury ID:"
            + mercuryMessageVO.getMercuryID());

        // update the message in the mercury table
        this.updateMercury(mercuryMessageVO);

        // Send system message
        MercuryEapiUtil.sendSystemMessageWrapper(sb.toString());
      }
    }
    else {
      // if the response object itself is null then update the mercury status to MO
      // so that the mercury message will be retried again
      sb = new StringBuffer();
      sb.append(CLASS_NAME).append(METHOD_NAME).append(" EAPI's response for mercury ID: ")
          .append(mercuryMessageVO.getMercuryID())
          .append(" is null. Changing mercury status to MO in order to retry again");

      logger.info(sb.toString());

      mercuryMessageVO.setSakText(sb.toString());
      mercuryMessageVO.setTransmissionDate(new java.util.Date());
      // reset the status to MO
      mercuryMessageVO.setMercuryStatus(MercuryEapiConstants.MERCURY_OPEN);
      // do not send Mercury Inbound Message for this request
      sendInboundMessages = false;

      logger.info(METHOD_NAME
          + " EAPI's response is null. So updating the mercury status to MO for mercury ID:"
          + mercuryMessageVO.getMercuryID());

      // update the message in the mercury table
      this.updateMercury(mercuryMessageVO);

      // Send system message
      MercuryEapiUtil.sendSystemMessageWrapper(sb.toString());
    }
  }

  /**
   * This method is used to process the success Post Message Response
   *
   * @param processedMessage
   * @param mercuryMessageVO
   */
  private void handlePostMessgeFailureResponse(ProcessedErosMessage processedErosMessage,
      MercuryMessageVO mercuryMessageVO) throws MercuryEapiException {

    // declare and initialize the variables
    final String METHOD_NAME = ".handlePostMessgeFailureResponse()";
    List<CodeMessagePair> errorMessages;
    StringBuffer errorMessageSB = null;
    String mercuryMessageNumber;
    String mercuryOrderNumber;

    if (logger.isDebugEnabled()) {
      logger.debug(CLASS_NAME + METHOD_NAME
          + " - handling post message failure response started for mercury ID: "
          + mercuryMessageVO.getMercuryID());
    }

    if (processedErosMessage == null) {
      errorMessageSB = new StringBuffer();
      errorMessageSB.append(CLASS_NAME).append(METHOD_NAME)
          .append(" - Response message from EAPI is null.");
      logger.error(errorMessageSB.toString());
      return;
    }

    errorMessages = processedErosMessage.getError();
    if (errorMessages != null) {
      errorMessageSB = new StringBuffer();
      for (CodeMessagePair errorMessage : errorMessages) {
        errorMessageSB.append(errorMessage.getMessage()).append("\n");
      }
    }
    mercuryMessageVO.setViewQueue(MercuryEapiConstants.VIEW_QUEUE_NO);

    String sakText = "REJECTED ";
    SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd yy hh:mma");
    Date transmissionDate = new Date();
    sakText = sakText + sdf.format(transmissionDate).toUpperCase();

    if (MercuryEapiConstants.FTD_MESSAGE.equalsIgnoreCase(mercuryMessageVO.getMessageType())) {
        // for FTD message, form the mercury order number using
        // reference number and order sequence number from EAPI's response
    	String orderSequence = String.format("%04d", processedErosMessage.getOrderSequenceNumber().intValue());
        mercuryMessageVO.setOrderSequence(orderSequence);
        mercuryMessageNumber = mercuryOrderNumber = processedErosMessage.getReferenceNumber() + "-"
            + orderSequence;
        mercuryMessageVO.setMercuryOrderNumber(mercuryOrderNumber);
        mercuryMessageVO.setMercuryMessageNumber(mercuryMessageNumber);
        sakText += " ORDER # " + mercuryOrderNumber;
      }
      else {
        // for other order related messages, form the mercury message number using
        // reference number and admin sequence number from EAPI's response
        String adminSequence = String.format("%04d", processedErosMessage.getAdminSequenceNumber().intValue());
        mercuryMessageVO.setAdminSequence(adminSequence);
        mercuryMessageNumber = processedErosMessage.getReferenceNumber() + "-"
            + adminSequence;
        mercuryMessageVO.setMercuryMessageNumber(mercuryMessageNumber);
        sakText += " ADMIN # " + mercuryMessageNumber;
      }
    sakText += " " + processedErosMessage.getMessageId();
    sakText += "\n" + errorMessageSB.toString();
    mercuryMessageVO.setSakText(sakText);
    mercuryMessageVO.setTransmissionDate(new java.util.Date());
    mercuryMessageVO.setMercuryStatus(MercuryEapiConstants.MERCURY_REJECT);

    // log the failure response
    logger.error(errorMessageSB.toString());

    if (logger.isDebugEnabled()) {
      logger.debug(CLASS_NAME + METHOD_NAME + " - Updating mercury status to MR for mercury id: "
          + mercuryMessageVO.getMercuryID());
    }
    // update mercury message
    this.updateMercury(mercuryMessageVO);

  }

  /**
   * This method is used to process the success Post Message Response
   *
   * @param processedMessage
   * @param mercuryMessageVO
   */
  private void handlePostMessgeSuccessResponse(ProcessedErosMessage processedMessage,
      MercuryMessageVO mercuryMessageVO) throws MercuryEapiException {

    // declare and initialize the variables
    final String METHOD_NAME = ".handlePostMessgeSuccessResponse()";
    String mercuryMessageNumber;
    String mercuryOrderNumber;
    StringBuilder errorMessage = null;

    if (logger.isDebugEnabled()) {
      logger.debug(CLASS_NAME + METHOD_NAME
          + " - handling success post message response for Mercury Id: "
          + mercuryMessageVO.getMercuryID());
    }

    if (processedMessage != null
        && processedMessage.getMessageId() != null
        && processedMessage.getReferenceNumber() != null
        && (processedMessage.getOrderSequenceNumber() != null || processedMessage
            .getAdminSequenceNumber() != null)) {
      // construct mercury message vo object with the success response
      mercuryMessageVO.setMercuryStatus(MercuryEapiConstants.MERCURY_CLOSED);
      mercuryMessageVO.setSuffix(mercuryMessageVO.getSuffix());
      mercuryMessageVO.setOrderReference(processedMessage.getReferenceNumber());
      mercuryMessageVO.setViewQueue(MercuryEapiConstants.VIEW_QUEUE_NO);

      if (logger.isDebugEnabled()) {
        logger.debug(CLASS_NAME + METHOD_NAME + "EAPI's response for mercuryId: "
            + mercuryMessageVO.getMercuryID() + ". Reference number: "
            + processedMessage.getReferenceNumber());
      }

      String sakText = "VERIFIED ";
      SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd yy hh:mma");
      Date transmissionDate = new Date();
      sakText = sakText + sdf.format(transmissionDate).toUpperCase();

      if (MercuryEapiConstants.FTD_MESSAGE.equalsIgnoreCase(mercuryMessageVO.getMessageType())) {
        // for FTD message, form the mercury order number using
        // reference number and order sequence number from EAPI's response
    	String orderSequence = String.format("%04d", processedMessage.getOrderSequenceNumber().intValue());
        mercuryMessageVO.setOrderSequence(orderSequence);
        mercuryMessageNumber = mercuryOrderNumber = processedMessage.getReferenceNumber() + "-"
            + orderSequence;
        mercuryMessageVO.setMercuryOrderNumber(mercuryOrderNumber);
        mercuryMessageVO.setMercuryMessageNumber(mercuryMessageNumber);
        sakText += " ORDER # " + mercuryOrderNumber;
      }
      else {
        // for other order related messages, form the mercury message number using
        // reference number and admin sequence number from EAPI's response
      	String adminSequence = String.format("%04d", processedMessage.getAdminSequenceNumber().intValue());
        mercuryMessageVO.setAdminSequence(adminSequence);
        mercuryMessageNumber = processedMessage.getReferenceNumber() + "-"
            + adminSequence;
        mercuryMessageVO.setMercuryMessageNumber(mercuryMessageNumber);
        sakText += " ADMIN # " + mercuryMessageNumber;
      }
      sakText += " " + processedMessage.getMessageId();
      mercuryMessageVO.setSakText(sakText);
    }
    else {
      errorMessage = new StringBuilder();
      errorMessage.append(CLASS_NAME).append(METHOD_NAME)
          .append(" - EAPI's Response for mercury ID: ").append(mercuryMessageVO.getMercuryID())
          .append(" is null. Changing the mercury status to MO in order re-send again.");
      logger.info(errorMessage.toString());
      // set the mercury status to MO if the response is null
      mercuryMessageVO.setMercuryStatus(MercuryEapiConstants.MERCURY_OPEN);
    }
    mercuryMessageVO.setTransmissionDate(new java.util.Date());

    if (logger.isDebugEnabled()) {
      logger.debug(CLASS_NAME + METHOD_NAME
          + " - Updating mercury table with EAPI's response for mercury ID: "
          + mercuryMessageVO.getMercuryID());
    }
    // update the mercury info
    this.updateMercury(mercuryMessageVO);
  }

  /**
   * This method is used to process the polled data
   *
   * @param suffix
   * @throws MercuryEapiException
   */
  public void processPolledMessageDetails(String messageId) throws MercuryEapiException {

	Long startPollTime = System.currentTimeMillis();

    // declare and initialize the variables
    final String METHOD_NAME = ".processPolledMessageDetails()";
    StringBuilder errorMessage = null;
    Connection conn = null;
    MercuryStageMessageVO mercuryStageMessageVO = null;
    MercuryDAO mercuryDAO = null;
    String mainMemberCode = null;

    // exit the process if EAPI is not enabled to process mercury orders
    if (!MercuryEapiUtil.isEAPIEnabled()) {
      logger.info(CLASS_NAME + METHOD_NAME + " EAPI is not enabled. Process will exit");
      return;
    }

    // exit the process if EAPI is down
    try {
        ErosTimeVO erosTimeVO = MercuryEapiUtil.isEROSDown();
        if (erosTimeVO.isErosDown()) {
            logger.info(CLASS_NAME + METHOD_NAME + " Current time is in EAPI's downtime.");
            long jmsDelay = calculateJmsDelay(erosTimeVO);
            logger.info("jmsDelay: " + jmsDelay);
            if (jmsDelay < 0) {
            	jmsDelay = 900;
            }
            this.sendPollDetailQueueMessage(null, messageId, String.valueOf(jmsDelay));
            return;
        }
    } catch (Exception e) {
    	logger.error(e);
    }

    try {
        // Step 1: get the connection
        conn = MercuryEapiUtil.getConnection();

        // Step 2. throw exception if the connection is null
        if (conn == null) {
            errorMessage = new StringBuilder();
            errorMessage.append(CLASS_NAME).append(METHOD_NAME).append(" DB Connection is null. ");
            logger.error(errorMessage.toString());
            throw new MercuryEapiException(errorMessage.toString());
        }

        // Step 3: get the mercuryDAO instance
        mercuryDAO = new MercuryDAO(conn);

        // Step 4: Get the mercury messages from the staging table with the status CONFIRMED
        mercuryStageMessageVO = mercuryDAO.getMercuryPolledMessageToBeProcessed(messageId);

        // Step 5: return if there are no polled messages
        if (mercuryStageMessageVO == null) {
            errorMessage = new StringBuilder();
            errorMessage.append(CLASS_NAME).append(METHOD_NAME)
                .append(" - Record not found in MERCURY_EAPI_MSGS_STAGING.");
            logger.error(errorMessage.toString());
            return;
        }

        // Step 6: process the new message

        // get main member code
        mainMemberCode = mercuryStageMessageVO.getMainMemberCode();

        // Step 6.1: get the eapi WS instance
        WebServiceClientFactory wscFactory = new WebServiceClientFactory();
        EAPIMessageService eapiWSService = wscFactory.getEAPIMessageService(
            mainMemberCode, MercuryEapiConstants.POLLED_PROCESS);

        // Step 6.2: get the message details by message id
        if (logger.isDebugEnabled()) {
            logger.debug(CLASS_NAME + METHOD_NAME
                + " - Sending request to EAPI to get the message details for : "
                + mercuryStageMessageVO.getMessageId() + " " + mainMemberCode);

        }
        Long startTimeGetDetails = System.currentTimeMillis();
        SingleMessageResponse singleMessageResponse = eapiWSService.getMessageDetailByMessageID(
            mainMemberCode, mercuryStageMessageVO.getMessageId());
        logger.info("Time taken for getMessageDetailByMessageID: "
                + (System.currentTimeMillis() - startTimeGetDetails));

        // Step 6.3: process the single message
        this.processSingleMessageResponse(singleMessageResponse, mercuryDAO, mercuryStageMessageVO);

        logger.info("Total time to process " + messageId + " is: "
                + (System.currentTimeMillis() - startPollTime));

    } catch (EAPIMessageServiceException eapiException) {
    	logger.error(eapiException);
        errorMessage = new StringBuilder();
        errorMessage.append("Message Id: ")
            .append(messageId).append(" ")
            .append(eapiException.getMessage());
        logger.error("EAPIMessageServiceException caught: " + errorMessage.toString());

        this.handlePollDetailException(errorMessage.toString(), mercuryDAO, messageId, mainMemberCode);

    } catch (SOAPFaultException soapException) {
        logger.error(soapException);
        errorMessage = new StringBuilder();
        errorMessage.append("Message Id: ")
            .append(messageId).append(" ")
            .append(soapException.getMessage());
        logger.error(errorMessage.toString());

        this.handlePollDetailException(errorMessage.toString(), mercuryDAO, messageId, mainMemberCode);

    } catch (WebServiceException wsException) {
        logger.error(wsException);
        errorMessage = new StringBuilder();
        errorMessage.append("Message Id: ")
            .append(messageId).append(" ")
            .append(wsException.getMessage());
        logger.error(errorMessage.toString());

        this.handlePollDetailException(errorMessage.toString(), mercuryDAO, messageId, mainMemberCode);

    } catch (MercuryEapiException mee) {
        logger.error(mee);
        errorMessage = new StringBuilder();
        errorMessage.append("Message Id: ")
            .append(messageId).append(" ")
            .append(mee.getMessage());
        logger.error(errorMessage.toString());

        this.handlePollDetailException(errorMessage.toString(), mercuryDAO, messageId, mainMemberCode);

    } catch (Exception e) {
        logger.error(e);
        errorMessage = new StringBuilder();
        errorMessage.append("Message Id: ")
            .append(messageId).append(" ")
            .append(e.getMessage());
        logger.error(errorMessage.toString());

        this.handlePollDetailException(errorMessage.toString(), mercuryDAO, messageId, mainMemberCode);

    } finally {

        // close the connection
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {

                errorMessage = new StringBuilder();
                errorMessage
                    .append(CLASS_NAME)
                    .append(METHOD_NAME)
                    .append(
                        "- SQL Exception is caught and unable to close the existing connection after processing the polled messages.");
                logger.error(errorMessage.toString());
                throw new MercuryEapiException(e);

            }
        }
    }
  }

  /**
   * This method is used to process single message response received from
   * getMessageDetailsByMessageId call
   *
   * @param singleMessageResponse
   * @param mercuryDAO
   * @param messageType
   * @throws Exception
   */
  private void processSingleMessageResponse(SingleMessageResponse singleMessageResponse,
      MercuryDAO mercuryDAO, MercuryStageMessageVO mercuryStageMessageVO)
      throws MercuryEapiException {

    // declare and initialize the variables
    final String METHOD_NAME = ".processSingleMessageResponse(singleMessageResponse,mercuryDAO,messageType)";
    StringBuilder errorMessage = null;
    MercuryMessageVO mercuryMessageVO = null;

    try {
        ProcessedErosMessage processedErosMessage;
        CodeMessagePair codeMessagePair;

        if (singleMessageResponse != null) {

            codeMessagePair = singleMessageResponse.getResponseCode();
            processedErosMessage = singleMessageResponse.getProcessedErosMessage();

            if (processedErosMessage != null) {

                logger.info(CLASS_NAME + METHOD_NAME
                    + "*** Response Message for getMessageDetailsByMessageId Process  ***"
                    + MercuryEapiUtil.getMessageDetailsResponse(processedErosMessage,
                		mercuryStageMessageVO.getMessageId(), mercuryStageMessageVO.getMainMemberCode()));

                if (MercuryEapiConstants.VERIFIED.equalsIgnoreCase(codeMessagePair.getCode())) {

                    if (logger.isDebugEnabled()) {
                        logger.debug(CLASS_NAME + METHOD_NAME + " - Building Mercury Message VO");
                    }
                    // build mercury message
        	        MercuryEapiObjectMapper objectMapper = new MercuryEapiObjectMapper();
                    mercuryMessageVO = objectMapper.buildMercuryMessageVO(processedErosMessage,
                		mercuryStageMessageVO.getMessageType());

                    if (logger.isDebugEnabled()) {
                        logger.debug(CLASS_NAME + METHOD_NAME
                            + " - Fetching reference number from Mercury table for mercury order number: "
                            + mercuryMessageVO.getMercuryOrderNumber());
                    }

                    // get the reference number of the root message using mercury order number
                    MercuryMessageVO oldMercuryMessageVO = mercuryDAO
                        .getMercuryMessageByMessageNumber(mercuryMessageVO.getMercuryOrderNumber());

                    if (oldMercuryMessageVO != null) {
                        mercuryMessageVO.setReferenceNumber(oldMercuryMessageVO.getReferenceNumber());
                        mercuryMessageVO.setSuffix(oldMercuryMessageVO.getSuffix());
                    }

                    // handle sending florist for CON message
                    if (MercuryEapiConstants.CONFIRM_CANCEL_MESSAGE.equalsIgnoreCase(mercuryMessageVO.getMessageType())) {
                        if (logger.isDebugEnabled()) {
                            logger.debug(CLASS_NAME + METHOD_NAME + " - Sending Florist = "
                                + mercuryMessageVO.getSendingFlorist());
                            logger.debug(CLASS_NAME + METHOD_NAME + " - Reference Number = "
                                + mercuryMessageVO.getReferenceNumber());
                        }

                        if (mercuryMessageVO.getSendingFlorist().indexOf(MercuryEapiConstants.EROS_SYSTEM_CODES) >= 0) {
                            logger.info(CLASS_NAME + METHOD_NAME + "Modifying auto-generated CON message");

                            // look up the original filling florist from the ftd record
                            String originalFlorist = oldMercuryMessageVO.getFillingFlorist();

                            // make the CON look like it came from the florist instead of Mercury
                            mercuryMessageVO.setSendingFlorist(originalFlorist);
                        }
                    }

                    if (logger.isDebugEnabled()) {
                        logger.debug(CLASS_NAME + METHOD_NAME + " - Storing message in mercury table");
                    }

                    // store mercury message
                    mercuryDAO.storeMercuryMessage(mercuryMessageVO);
                    // Q4SP16-13 : GEN and CON messages are no longer needed to be queued to Customer Service:
                    if (MercuryEapiConstants.GEN_MESSAGE
                        .equalsIgnoreCase(mercuryMessageVO.getMessageType()) || MercuryEapiConstants.CONFIRM_CANCEL_MESSAGE.equalsIgnoreCase(mercuryMessageVO.getMessageType())) {
                        String comments = mercuryMessageVO.getComments().toUpperCase();
                        String newComments = comments.replaceAll("\\s", "");
                        if(MercuryEapiConstants.GEN_MESSAGE.equalsIgnoreCase(mercuryMessageVO.getMessageType()))
                        {
	                        if (mercuryMessageVO.getComments() != null
	                            && newComments.indexOf(MercuryEapiConstants.SUSPEND_TEXT) >= 0) {

	                            logger.info("Updating the suspend status for " + mercuryMessageVO.getSendingFlorist());
	                            // update suspend flag
	                            mercuryDAO.updateSuspend(mercuryMessageVO.getSendingFlorist(), "Y",
	                                mercuryMessageVO.getMercuryMessageNumber(), null);
	                            //sendInboundMessages = false;
	                        } else if (mercuryMessageVO.getComments() != null
	                            && newComments.indexOf(MercuryEapiConstants.RESUME_TEXT) >= 0) {

	                            logger.info("Updating the resume status for " + mercuryMessageVO.getSendingFlorist());
	                            // update resume flag
	                            mercuryDAO.updateSuspend(mercuryMessageVO.getSendingFlorist(), "N",
	                                null, mercuryMessageVO.getMercuryMessageNumber());
	                            //sendInboundMessages = false;
	                        }
                        }

                        sendInboundMessages = false;
                    }

                    if (logger.isDebugEnabled()) {
                        logger.debug(CLASS_NAME + METHOD_NAME
                            + " - Inserting into MercuryInboundMDB for mercury ID: "
                            + mercuryMessageVO.getMercuryID());
                    }

                    // insert the mercury id into MercuryInbound Queue
                    if (sendInboundMessages) {
                    	if (MercuryEapiUtil.isMarsEnabled()) {
                            if (logger.isDebugEnabled()) {
                            	logger.debug("Mars is enabled. Routing the request to MARS_MERCURY queue");
                    		}

                            logger.info("Mars is enabled. Routing the request to MARS_MERCURY queue");

                            try {
                            	this.sendToMarsMercuryQueue(null, mercuryMessageVO.getMercuryID());
                            	sendInboundMessages = true;
                            }
                            catch(Exception e) {
                            	logger.error("Exception caught when trying to send the message to MARS_MERCURY queue. So routing the request to MERCURY_INBOUND queue", e);
                            	mercuryDAO.insertMercuryInboundMDB(mercuryMessageVO.getMercuryID());
                            	sendInboundMessages = true;
                            }
                        }
                    	else {
                    		if (logger.isDebugEnabled()) {
                            	logger.debug("Mars is disabled. Routing the request to MERCURY_INBOUND queue");
                    		}
                    		logger.info("Mars is disabled. Routing the request to MERCURY_INBOUND queue");
                    		mercuryDAO.insertMercuryInboundMDB(mercuryMessageVO.getMercuryID());
                            sendInboundMessages = true;
                    	}
                    }

                    // update the message status from staging table
                    mercuryStageMessageVO.setStatus(MercuryEapiConstants.PROCESSED);
                    logger.info("Updating the status in staging table for " + mercuryStageMessageVO.getReferenceNumber() +
                            " / " + mercuryStageMessageVO.getMessageId() + " / " + mercuryStageMessageVO.getStatus());
                    // update the message in the staging table
                    mercuryDAO.updateMercuryEapiStagingMessage(mercuryStageMessageVO);
                } else {
                    errorMessage = new StringBuilder();
                    errorMessage.append("Response code for " + mercuryStageMessageVO.getMessageId() + " is not verified: ")
                        .append(codeMessagePair.getCode() + " Message: " + codeMessagePair.getMessage())
                        .append(". Message was not processed.");
                    logger.error(errorMessage.toString());
                    MercuryEapiUtil.sendSystemMessageWrapper(errorMessage.toString());
                    return;
                }
            } else {
                errorMessage = new StringBuilder();
                errorMessage.append("Response for " + mercuryStageMessageVO.getMessageId() + " is null.")
                    .append(" Code: " + codeMessagePair.getCode() + " Message: " + codeMessagePair.getMessage())
                    .append(". Message was not processed.");
                logger.error(errorMessage.toString());
                MercuryEapiUtil.sendSystemMessageWrapper(errorMessage.toString());
                return;
            }
        } else {
            errorMessage = new StringBuilder();
            errorMessage.append("Response for " + mercuryStageMessageVO.getMessageId() + " is null.")
                .append(". Message was not processed.");
            logger.error(errorMessage.toString());
            MercuryEapiUtil.sendSystemMessageWrapper(errorMessage.toString());
            return;
        }

        if (logger.isDebugEnabled()) {
            logger.debug(CLASS_NAME + METHOD_NAME
                + " - Finished processing the response message from EAPI for message id: "
                + mercuryStageMessageVO.getMessageId());
        }
    } catch (Exception e) {
        errorMessage = new StringBuilder();
        errorMessage.append(CLASS_NAME).append(METHOD_NAME)
            .append(" - Unable to store the mercury message with mercury order number:")
            .append(mercuryMessageVO.getMercuryOrderNumber());
        logger.error(errorMessage.toString());
        throw new MercuryEapiException(e);
    }
  }

  /**
   * This method is used to handle the various exceptions which were caught while
   * processing a POLL getNewMessagesFromEapi
   *
   * @param mercuryId
   */
  private void handlePollException(MercuryEapiOpsVO mercuryEapiOpsVO,
		  MercuryDAO mercuryDAO, String errorMessage) {
	  String delayTime = null;
      try {
          ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
          delayTime = configUtil.getFrpGlobalParm(MercuryEapiConstants.MERCURY_CONFIG_CONTEXT,
              MercuryEapiConstants.DEFAULT_EXCEPTION_DELAY);
      } catch (Exception e) {
	      logger.error("Could not retrieve delayTime, defaulting to 300");
          delayTime = "300";
      }
      if (delayTime == null) {
	      delayTime = "300";
      }
      String mainMemberCode = mercuryEapiOpsVO.getMainMemberCode();
      logger.info("Requeueing message " + mainMemberCode + " with a delay time of " + delayTime);
      this.sendPollQueueMessage(mainMemberCode, false, delayTime);

      logger.info("Removing cached thread instance");
      WebServiceClientFactory wscFactory = new WebServiceClientFactory();
      wscFactory.removeThreadInstance(mainMemberCode);

      try {
          mercuryEapiOpsVO.setInUse(false);
          mercuryEapiOpsVO.setUpdatedBy("POLL_MDB");
          mercuryDAO.updateMercuryEapiOps(mercuryEapiOpsVO);
      } catch (Exception e) {
    	  logger.error(e);
      }

      MercuryEapiUtil.sendSystemMessageWrapper(errorMessage);

  }

  /**
   * This method is used to handle the EAPIMessageServiceException
   *
   * @param e
   */
  private void handlePollDetailException(String errorMessage, MercuryDAO mercuryDAO,
		  String messageId, String mainMemberCode) {

	  try {
	      MercuryEapiObjectMapper objectMapper = new MercuryEapiObjectMapper();
          MercuryStageMessageVO mercuryStageMessageVO = objectMapper.buildMercuryStageMessageVO();
          mercuryStageMessageVO.setReason(errorMessage);
          mercuryStageMessageVO.setMessageId(Long.parseLong(messageId));

          // update the mercury message
          logger.info("Updating the staging table for " + messageId);
          mercuryDAO.updateMercuryEapiStagingMessage(mercuryStageMessageVO);

          MercuryEapiUtil.sendSystemMessageWrapper(errorMessage);

    	  String delayTime = null;
          try {
              ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
              delayTime = configUtil.getFrpGlobalParm(MercuryEapiConstants.MERCURY_CONFIG_CONTEXT,
                  MercuryEapiConstants.DEFAULT_EXCEPTION_DELAY);
          } catch (Exception e) {
    	      logger.error("Could not retrieve delayTime, defaulting to 30");
              delayTime = "300";
          }
          if (delayTime == null) {
    	      delayTime = "300";
          }
          logger.info("Requeueing message " + messageId + " with a delay time of " + delayTime);

          this.sendPollDetailQueueMessage(null, messageId, delayTime);

          logger.info("Removing cached thread instance");
          WebServiceClientFactory wscFactory = new WebServiceClientFactory();
          wscFactory.removeThreadInstance(mainMemberCode);

	  } catch (Exception e) {
		  logger.error(e);
	  }


  }

  /**
   * This method is used to handle the various exceptions which were caught while sending
   * POST message to EAPI
   *
   * @param mercuryId
   */
  private void handlePostException(String mercuryId, String mainMemberCode) {
	  String delayTime = null;
      try {
          ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
          delayTime = configUtil.getFrpGlobalParm(MercuryEapiConstants.MERCURY_CONFIG_CONTEXT,
              MercuryEapiConstants.DEFAULT_EXCEPTION_DELAY);
      } catch (Exception e) {
	      logger.error("Could not retrieve delayTime, defaulting to 30");
          delayTime = "300";
      }
      if (delayTime == null) {
	      delayTime = "300";
      }
      logger.info("Requeueing message " + mercuryId + " with a delay time of " + delayTime);
      this.sendPostQueueMessage(null, mercuryId, delayTime);

      logger.info("Removing cached thread instance");
      WebServiceClientFactory wscFactory = new WebServiceClientFactory();
      wscFactory.removeThreadInstance(mainMemberCode);
  }

  /**
   * This method is used to process the response to the confirmNewMessageByReference Call
   *
   * @param confirmResponse
   */
  private void processConfirmNewMessageResponse(ConfirmResponse confirmResponse,
      MercuryDAO mercuryDAO, HashMap<String, String> messageMap) throws MercuryEapiException {

    // declare and initialize the variables
    final String METHOD_NAME = ".processConfirmNewMessageResponse(confirmResponse,mercuryDAO)";
    StringBuilder errorMessage = null;

    if (confirmResponse == null || confirmResponse.getResponseCode() == null) {

      errorMessage = new StringBuilder();
      errorMessage.append(CLASS_NAME).append(METHOD_NAME)
          .append(" - No response is receieved from Eapi for confirming new messages.");
      logger.error(errorMessage.toString());
      throw new MercuryEapiException(errorMessage.toString());

    }

    CodeMessagePair codeMessagePair = confirmResponse.getResponseCode();

    if (codeMessagePair == null) {
      // return if the confirm message response is null. The process will retry to confirm
      // the messages again.
      errorMessage = new StringBuilder();
      errorMessage.append(CLASS_NAME).append(METHOD_NAME)
          .append(" - CodeMessagePair is null. EAPI's response is null.");
      logger.error(errorMessage.toString());
      throw new MercuryEapiException(errorMessage.toString());

    }
    if (logger.isDebugEnabled()) {
      logger.debug(CLASS_NAME + METHOD_NAME
          + " - Started processing the confirm message response from EAPI: "
          + codeMessagePair.getCode() + " " + codeMessagePair.getMessage());
    }
    if (codeMessagePair.getCode() != null
        && MercuryEapiConstants.VERIFIED.equalsIgnoreCase(codeMessagePair.getCode())) {

      List<SimpleResponse> listOfMessagesVerifiedByEapi = confirmResponse.getSimpleResponse();

      if (listOfMessagesVerifiedByEapi == null || listOfMessagesVerifiedByEapi.size() <= 0) {
        // return if the confirm message response is null. The process will retry to
        // confirm the messages again.
        errorMessage = new StringBuilder();
        errorMessage.append(CLASS_NAME).append(METHOD_NAME)
            .append(" -There are no confirmed message responses from EAPI..");
        logger.error(errorMessage.toString());
        throw new MercuryEapiException(errorMessage.toString());

      }
      else {
        // process each confirm message and update the staging table
        for (SimpleResponse simpleResponse : listOfMessagesVerifiedByEapi) {
        	this.updateMercuryStageMessage(simpleResponse, mercuryDAO, messageMap);
        }
      }

    }
    else if (MercuryEapiConstants.REJECTED.equalsIgnoreCase(codeMessagePair.getCode())) {

      errorMessage = new StringBuilder();
      errorMessage.append(CLASS_NAME).append(METHOD_NAME)
          .append(" - Rejected Response is received from Eapi for confirm message call.");
      logger.error(errorMessage.toString());

    }
    logger.debug(CLASS_NAME + METHOD_NAME
        + " - Finished processing the confirm message response from EAPI");
  }

  /**
   * This method is used update the mercury new message
   *
   * @param confirmResponse
   */
  private void updateMercuryStageMessage(SimpleResponse simpleResponse, MercuryDAO mercuryDAO,
		  HashMap<String, String> messageMap) throws MercuryEapiException {

    // declare and initialize the variable to hold the method name to use for logging
    final String METHOD_NAME = ".updateMercuryStageMessage()";
    StringBuilder errorMessage = null;
    MercuryStageMessageVO mercuryStageMessageVO = null;

    try {

      // Step 1. build the Mercury New Message VO Object
  	  MercuryEapiObjectMapper objectMapper = new MercuryEapiObjectMapper();
      mercuryStageMessageVO = objectMapper.buildMercuryStageMessageVO(simpleResponse);
      String referenceNumber = mercuryStageMessageVO.getReferenceNumber();
      String messageId = messageMap.get(referenceNumber);
      mercuryStageMessageVO.setMessageId(Long.parseLong(messageId));

      // Step 2: update the status of mercury new message in staging table
      logger.info("Updating the status in staging table for " + referenceNumber +
            " / " + messageId + " / " + mercuryStageMessageVO.getStatus());
      String status = mercuryDAO.updateMercuryEapiStagingMessage(mercuryStageMessageVO);

      // Step 3. send system message if the status is N
      if (MercuryEapiConstants.N_STRING.equalsIgnoreCase(status)) {

        errorMessage = new StringBuilder();
        errorMessage.append(CLASS_NAME).append(METHOD_NAME)
            .append(" - Unable to update the status of the mercury new message with message id:")
            .append(mercuryStageMessageVO.getMessageId());
        logger.error(errorMessage.toString());
        throw new MercuryEapiException(errorMessage.toString());

      } else {
          String delayTime = "3";
          try {
              ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
              delayTime = configUtil.getFrpGlobalParm(MercuryEapiConstants.MERCURY_CONFIG_CONTEXT,
                  MercuryEapiConstants.DEFAULT_POLL_PROCESS_DELAY);
          } catch (Exception e) {
        	  logger.error("Could not retrieve delayTime, defaulting to 3");
              delayTime = "3";
          }
          if (delayTime == null) {
        	  delayTime = "3";
          }
    	  this.sendPollDetailQueueMessage(null, messageId, delayTime);
    	  logger.info("Poll Process MDB created for " + messageId);
      }
    }
    catch (Exception e) {
      errorMessage = new StringBuilder();
      errorMessage.append(CLASS_NAME).append(METHOD_NAME)
          .append(" -Unable to update the status of the mercury new message with message id:")
          .append(mercuryStageMessageVO.getMessageId());
      logger.error(errorMessage.toString());
      logger.error(e);
      throw new MercuryEapiException(e);
    }

  }

  /**
   * This method updates the order info based on the EAPI response
   *
   * @param mercuryMessageVO
   */
  private void updateMercury(MercuryMessageVO mercuryMessageVO) throws MercuryEapiException {

    // declare and initialize the variables
    final String METHOD_NAME = ".updateMercury(mercuryMessageVO)";
    Connection conn = null;
    MercuryDAO dao = null;
    StringBuilder errorMessage = null;

    try {

      // get the connection
      conn = MercuryEapiUtil.getConnection();

      // throw exception if the connection is null
      if (conn == null) {

        errorMessage = new StringBuilder();
        errorMessage.append(CLASS_NAME).append(METHOD_NAME).append(" DB Connection is null. ");
        logger.error(errorMessage.toString());
        throw new MercuryEapiException(errorMessage.toString());

      }

      // get the MercuryDAO object
      dao = new MercuryDAO(conn);

      // update mercury message
      dao.updateMercuryMessageEapi(mercuryMessageVO);

      // insert data into HP table (this is how the HP knows that something needs to be
      // done with message)
      if (sendInboundMessages) {
        dao.insertMercuryInboundMDB(mercuryMessageVO.getMercuryID());
      }
      sendInboundMessages = true;
    }
    catch (Exception e) {
      counter++;
      if (counter == 3) {
        logger.error(e);
        errorMessage = new StringBuilder();
        errorMessage.append(CLASS_NAME).append(METHOD_NAME)
            .append(" -Unable to update the mercury message with mercury message number:")
            .append(mercuryMessageVO.getMercuryMessageNumber());
        counter = 0;
        throw new MercuryEapiException(errorMessage.toString());
      }

      // retry
      if (logger.isDebugEnabled()) {
        logger.debug(CLASS_NAME + METHOD_NAME + " - Retrying Update for mercury id: "
            + mercuryMessageVO.getMercuryID() + ". Retry count: " + counter);
      }
      updateMercury(mercuryMessageVO);

    }
    finally {

      // close the connection
      if (conn != null) {
        try {
          conn.close();
        }
        catch (SQLException e) {

          logger.error(e);
          errorMessage = new StringBuilder();
          errorMessage
              .append(CLASS_NAME)
              .append(METHOD_NAME)
              .append(
                  "- SQL Exception is caught and unable to close the existing connection after processing the polled messages.");
          throw new MercuryEapiException(errorMessage.toString());

        }
      }
    }
  }

  /**
   * This method is used to send the message to MERCURY EAPI POLL Queue
   *
   * @param suffix
   * @param delayTime
   */
  public void sendPollQueueMessage(String memberCode, boolean moreRecordsToProcess, String delayTime) {

    try {

      InitialContext context = new InitialContext();
      if (delayTime == null) {
          if (moreRecordsToProcess) {
    	      delayTime = "0";
          } else {
              try {
                  ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
                  delayTime = configUtil.getFrpGlobalParm(MercuryEapiConstants.MERCURY_CONFIG_CONTEXT,
                      MercuryEapiConstants.DEFAULT_POLL_DELAY);
              } catch (Exception e) {
    	          logger.error("Could not retrieve delayTime, defaulting to 30");
                  delayTime = "30";
              }
              if (delayTime == null) {
    	          delayTime = "30";
              }
          }
          if (delayTime == null) {
	          delayTime = "30";
          }
      }

	  MessageToken token = new MessageToken();
	  token.setMessage(memberCode);
	  token.setProperty(MercuryEapiConstants.JMS_DELAY_PROPERTY, delayTime, "int");
	  MercuryEapiUtil.sendJMSMessage(context, token, MercuryEapiConstants.MERCURY_EAPI_POLL_QUEUE);
	} catch (Exception e) {
      logger.error("Failed to Send Poll Queue JMS message: " + e);
    }
  }

  /**
   * This method is used to send the message to MERCURY EAPI POLL Queue
   *
   * @param suffix
   * @param delayTime
   */
  public void sendPostQueueMessage(InitialContext context, String mercuryId, String delayTime) {
    try {

      if (context == null) {
        context = new InitialContext();
      }
      MessageToken token = new MessageToken();
      token.setMessage(mercuryId);
      token.setProperty(MercuryEapiConstants.JMS_DELAY_PROPERTY, delayTime, "int");
      MercuryEapiUtil.sendJMSMessage(context, token, MercuryEapiConstants.MERCURY_EAPI_POST_PROCESS_QUEUE);

    }
    catch (Exception e) {
      logger.error("Failed to Send JMS message as part of scheduling: " + e);
    }
  }

  /**
   * This method is used to send the message to MERCURY EAPI POLL PROCESS Queue
   *
   * @param suffix
   * @param delayTime
   */
  public void sendPollDetailQueueMessage(InitialContext context, String messageId, String delayTime) {

    try {

      if (context == null) {
	      context = new InitialContext();
	  }
	  MessageToken token = new MessageToken();
	  token.setMessage(messageId);
	  token.setProperty(MercuryEapiConstants.JMS_DELAY_PROPERTY, delayTime, "int");
	  MercuryEapiUtil.sendJMSMessage(context, token, MercuryEapiConstants.MERCURY_EAPI_POLL_PROCESS_QUEUE);
	} catch (Exception e) {
      logger.error("Failed to Send JMS message as part of scheduling: " + e);
    }
  }

  private long calculateJmsDelay(ErosTimeVO erosTimeVO) {
	  long diff = 0;
      SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

      try {
  		  Date now = new Date();
  		  logger.info("now: " + sdf.format(now));

  		  Calendar downCal = erosTimeVO.getErosDownStartTime();
  		  Date downTime = new Date(downCal.getTimeInMillis());
  		  logger.info("downStartTime: " + sdf.format(downTime));

  		  if (now.after(downTime)) {
              Calendar startCal = erosTimeVO.getErosDownEndTime();
      		  Date startTime = new Date(startCal.getTimeInMillis());
      		  logger.info("downEndTime: " + sdf.format(startTime));
  			  diff = (startTime.getTime() - now.getTime()) / 1000;
  		  } else {
              Calendar startCal = erosTimeVO.getErosDownEndTime();
      		  Date startTime = new Date(startCal.getTimeInMillis());
      		  logger.info("downEndTime: " + sdf.format(startTime));
              if (now.before(startTime)) {
            	  diff = (startTime.getTime() - now.getTime()) / 1000;
              }
  		  }
      } catch (Exception e) {
          logger.error(e);
      }

	  return diff;
  }

  /**
   * This method is used to send the message to MERCURY EAPI POLL Queue
   *
   * @param suffix
   * @param delayTime
   */
  public void sendToMarsMercuryQueue(InitialContext context, String mercuryId) throws Exception {
	  if (context == null) {
	    context = new InitialContext();
	  }
	  MessageToken token = new MessageToken();
	  token.setMessage(mercuryId);
	  MercuryEapiUtil.sendJMSMessage(context, token, MercuryEapiConstants.MARS_MERCURY_QUEUE);
  }

}
