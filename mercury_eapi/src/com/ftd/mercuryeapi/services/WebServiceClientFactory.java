package com.ftd.mercuryeapi.services;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.ws.WebServiceException;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;

import com.ftd.eapi.message.service.v1.EAPIMessageService;
import com.ftd.eapi.message.service.v1.EAPIMessageService_Service;
import com.ftd.mercuryeapi.util.EAPILoggingInInterceptor;
import com.ftd.mercuryeapi.util.EAPILoggingOutInterceptor;
import com.ftd.mercuryeapi.util.EAPIServiceInterceptor;
import com.ftd.mercuryeapi.util.MercuryEapiConstants;
import com.ftd.mercuryeapi.util.MercuryEapiUtil;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * CXF implementation for getting EAPI's WS instance.
 * Uses custom interceptor to bind the username and password into soap header.
 *  
 * @author kdatchan
 *
 */
public class WebServiceClientFactory {
  
  private static final Logger logger = new Logger(WebServiceClientFactory.class.getName());
  protected EAPIServiceInterceptor eapiInterceptor;
  private static Map<String, EAPIMessageService> threadInstancesMap = new HashMap<String, EAPIMessageService>();
    
  public EAPIMessageService getEAPIMessageService(String mainMemberCode, String processType) throws CacheException, Exception 
  {
    StringBuffer errorMessage = null;
    EAPIMessageService eapiWS = null;
    String key = null;
    
    if (logger.isDebugEnabled()) {
      logger.debug("Getting the EAPI Web Service Intance");
    }
    
    try {

      String threadName = Thread.currentThread().getName();
      key = mainMemberCode + "-" + threadName;

      logger.debug("Key to get the web service instance: " + key);
      
      eapiInterceptor = new EAPIServiceInterceptor(mainMemberCode);
      
      if (threadInstancesMap.containsKey(key)) {
        logger.debug("Getting the cached instance.");
        eapiWS = threadInstancesMap.get(key);
      }
      else {

        if (logger.isDebugEnabled()) {
          logger.debug(" Creating the EAPI web service instance.");
        }
        
        String svsWsdlName = MercuryEapiUtil.getEapiWebServiceWsdl();
        String svsUrlString = MercuryEapiUtil.getEapiWebServiceUrl();

        Class class_v = this.getClass();
        ClassLoader classLoader = class_v.getClassLoader();
        URL svsWsdlLocation = classLoader.getResource(svsWsdlName);
       
        EAPIMessageService_Service ems = new EAPIMessageService_Service(svsWsdlLocation); 
        
        Iterator iter = ems.getPorts();
        while(iter.hasNext())
        {
          QName testQ = (QName)iter.next();
          ems.addPort(testQ, "http://schemas.xmlsoap.org/soap/", svsUrlString);
        }

        eapiWS = ems.getEAPIMessageServicePort();

        if (eapiWS == null) {
          errorMessage = new StringBuffer();
          errorMessage.append(" EAPI Web Service instance is null.");
          logger.error(errorMessage.toString());
          throw new MercuryEapiException(errorMessage.toString());

        }

        threadInstancesMap.put(key, eapiWS);
      }
      
      if (logger.isDebugEnabled()) {
        logger.debug("Size of the cache:"
            + threadInstancesMap.size());
      }
      
      Client cxfClient = ClientProxy.getClient(eapiWS);
      cxfClient.getOutInterceptors().add(eapiInterceptor);
      cxfClient.getInInterceptors().add(new EAPILoggingInInterceptor());
      cxfClient.getOutInterceptors().add(new EAPILoggingOutInterceptor());

      setTimeoutOnCxfClient(cxfClient);
      
      return eapiWS;
    }
    catch (MalformedURLException e1) {

      errorMessage = new StringBuffer();
      errorMessage.append(" Unable to create URL object for eapi wsdl url string.");
      logger.error(errorMessage.toString());
      throw new MercuryEapiException(e1);

    }
    catch (WebServiceException wsException) {

      errorMessage = new StringBuffer();
      errorMessage.append(" web service exception occured.");
      logger.error(errorMessage.toString());
      throw new MercuryEapiException(wsException);

    }
    catch (Exception e) {

      errorMessage = new StringBuffer();
      errorMessage.append(" Exception occured.");
      logger.error(errorMessage.toString());
      throw new MercuryEapiException(e);
    }
  }
    
    private void setTimeoutOnCxfClient(Client cxfClient) throws Exception {
      final HTTPConduit http = (HTTPConduit) cxfClient.getConduit();
      final HTTPClientPolicy httpClientPolicy = new HTTPClientPolicy();
      
      //set the timeout
      String timeout = MercuryEapiUtil.getCxfClientTimeout(); //default to 60s
      if(timeout != null ) {
        httpClientPolicy.setReceiveTimeout(Integer.parseInt(timeout));
        httpClientPolicy.setConnectionTimeout(Integer.parseInt(timeout));
      } 
      else {
        httpClientPolicy.setReceiveTimeout(MercuryEapiConstants.DEFAULT_CXF_CLIENT_TIMEOUT);
        httpClientPolicy.setConnectionTimeout(MercuryEapiConstants.DEFAULT_CXF_CLIENT_TIMEOUT);
      }
      httpClientPolicy.setAllowChunking(false);

      http.setClient(httpClientPolicy);
      
  }
    
    public void removeThreadInstance(String mainMemberCode) {
      String threadName = Thread.currentThread().getName();
      String key = mainMemberCode + "-" + threadName;

      if (logger.isDebugEnabled()) {
          logger.debug("key: " + key);
      }

      if (threadInstancesMap.containsKey(key)) {
          threadInstancesMap.remove(key);
          if (logger.isDebugEnabled()) {
              logger.debug("Key removed");
          }
      } else {
    	  if (logger.isDebugEnabled()) {
              logger.debug("Key does not exist in threadInstancesMap");
    	  }
      }
      if (logger.isDebugEnabled()) {
          logger.debug("Remaining keys");
      Iterator keyIterator = threadInstancesMap.keySet().iterator();
      while (keyIterator.hasNext()) {
          String tempKey = (String) keyIterator.next();
              logger.debug("\n    " + tempKey);
          }
      }
  }
}
