package com.ftd.mercuryeapi.validators;

/*
 * -------------------------------History------------------------------- Author
 * CreatedDate ModifiedDate Reason
 * --------------------------------------------------------------------- smotla Jun 17th
 * 2013 Jun 17th 2013 Apollo-EAPI Integration
 */

import com.ftd.mercuryeapi.services.MercuryEapiException;
import com.ftd.mercuryeapi.util.MercuryEapiConstants;
import com.ftd.mercuryeapi.util.MercuryEapiUtil;
import com.ftd.mercuryeapi.viewobjects.MercuryMessageVO;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * This object contains the validation rules used for outgoing messages from Apollo to
 * EAPI
 */
public class MercuryMessageValidator {

  private static String LOGGER_CATEGORY = "com.ftd.mercuryeapi.validators.MercuryMessageValidator";

  private static Logger logger = new Logger(LOGGER_CATEGORY);

  private static String CLASS_NAME = "MercuryMessageValidator";

  /**
   * Validate the passed in mercury message
   * 
   * @param mercuryVO
   *          message to validate
   * @return MercuryMessageValidatorResponse
   * @throws java.lang.Exception
   */
  public MercuryMessageValidatorResponse validate(MercuryMessageVO mercuryVO)
      throws Exception {

    MercuryMessageValidatorResponse response = null;

    // get the message type
    String msgType = mercuryVO.getMessageType();
    logger.debug(msgType);

    if (msgType == null) {
      logger.error(mercuryVO.getMercuryID() + MercuryEapiConstants.ERROR_MESSAGE_MSG_TYPE_NULL);
      throw new Exception(MercuryEapiConstants.LOG_MESSAGE_PART + mercuryVO.getMercuryID()
          + MercuryEapiConstants.LOG_MESSAGE_PART1 + MercuryEapiConstants.LOG_MESSAGE_PART1
          + MercuryEapiConstants.ERROR_MESSAGE_MSG_TYPE_NULL);
    }

    // Determine the mesage type and what validation should be done
    if (msgType.equals(MercuryEapiConstants.ANS_MESSAGE)) {
      response = validateANS(mercuryVO);
    }
    else if (msgType.equals(MercuryEapiConstants.ASK_MESSAGE)) {
      response = validateASK(mercuryVO);
    }
    else if (msgType.equals(MercuryEapiConstants.CAN_MESSAGE)) {
      response = validateCAN(mercuryVO);
    }
    else if (msgType.equals(MercuryEapiConstants.FTD_MESSAGE)) {
      response = validateFTD(mercuryVO);
    }
    else if (msgType.equals(MercuryEapiConstants.GEN_MESSAGE)) {
      response = validateGEN(mercuryVO);
    }
    else if (msgType.equals(MercuryEapiConstants.ADJ_MESSAGE)) {
      response = validateADJ(mercuryVO);
    }
    else if (msgType.equals(MercuryEapiConstants.REJ_MESSAGE)) {
      response = validateREJ(mercuryVO);
    }
    else if (msgType.equals(MercuryEapiConstants.RET_MESSAGE)) {
      response = validateRET(mercuryVO);
    }
    else {
      logger.debug(MercuryEapiConstants.LOG_MESSAGE_PART + mercuryVO.getMercuryID()
          + MercuryEapiConstants.LOG_MESSAGE_PART1
          + MercuryEapiConstants.ERROR_MESSAGE_UNKNOWN_MSG_TYPE + msgType);
      response = new MercuryMessageValidatorResponse();
      response.setValid(false);
      response.setMessage(MercuryEapiConstants.ERROR_MESSAGE_UNKNOWN_MSG_TYPE + msgType);
    }

    return response;

  }

  /**
   * This method validates the Outgoing ANS message
   * 
   * @param mercuryVO
   *          message to validate
   * @return MercuryMessageValidatorResponse
   */
  private MercuryMessageValidatorResponse validateANS(MercuryMessageVO mercuryVO) {

    MercuryMessageValidatorResponse response = new MercuryMessageValidatorResponse();
    response.setValid(true);

    // validate common fields
    response = validateCommonFields(response, mercuryVO);

    // validate price
    validatePrice(mercuryVO, response);

    return response;
  }

  /**
   * This method validates the Outgoing RETRIEVE Message
   * 
   * @param mercuryVO
   *          message to validate
   * @return MercuryMessageValidatorResponse
   */
  private MercuryMessageValidatorResponse validateRET(MercuryMessageVO mercuryVO) {

    MercuryMessageValidatorResponse response = new MercuryMessageValidatorResponse();
    response.setValid(true);

    if (response.isValid() && MercuryEapiUtil.isEmpty((mercuryVO.getRetrievalFromDate()))) {
      logger.debug(MercuryEapiConstants.LOG_MESSAGE_PART + mercuryVO.getMercuryID()
          + MercuryEapiConstants.LOG_MESSAGE_PART1
          + MercuryEapiConstants.ERROR_MESSAGE_MISSING_RETRIEVAL_FROM_DATE);
      response.setMessage(MercuryEapiConstants.ERROR_MESSAGE_MISSING_RETRIEVAL_FROM_DATE);
      response.setValid(false);
    }

    if (response.isValid() && MercuryEapiUtil.isEmpty((mercuryVO.getRetrievalToDate()))) {
      logger.debug(MercuryEapiConstants.LOG_MESSAGE_PART + mercuryVO.getMercuryID()
          + MercuryEapiConstants.LOG_MESSAGE_PART1
          + MercuryEapiConstants.ERROR_MESSAGE_MISSING_RETRIEVAL_TO_DATE);
      response.setMessage(MercuryEapiConstants.ERROR_MESSAGE_MISSING_RETRIEVAL_TO_DATE);
      response.setValid(false);
    }

    if (response.isValid() && !isValidMercuryOrderNumber((mercuryVO.getRetrievalFromMessage()))) {
      logger.debug(MercuryEapiConstants.LOG_MESSAGE_PART + mercuryVO.getMercuryID()
          + MercuryEapiConstants.LOG_MESSAGE_PART1
          + MercuryEapiConstants.ERROR_MESSAGE_MISSING_RETRIEVAL_FROM_NUMBER);
      response.setMessage(MercuryEapiConstants.ERROR_MESSAGE_MISSING_RETRIEVAL_FROM_NUMBER);
      response.setValid(false);
    }

    if (response.isValid() && !isValidMercuryOrderNumber((mercuryVO.getRetrievalToMessage()))) {
      logger.debug(MercuryEapiConstants.LOG_MESSAGE_PART + mercuryVO.getMercuryID()
          + MercuryEapiConstants.LOG_MESSAGE_PART1
          + MercuryEapiConstants.ERROR_MESSAGE_MISSING_RETRIEVAL_TO_NUMBER);
      response.setMessage(MercuryEapiConstants.ERROR_MESSAGE_MISSING_RETRIEVAL_TO_NUMBER);
      response.setValid(false);
    }

    return response;
  }

  /**
   * This method validates the Outgoing ASK message
   * 
   * @param mercuryVO
   *          message to validate
   * @return MercuryMessageValidatorResponse
   */
  private MercuryMessageValidatorResponse validateASK(MercuryMessageVO mercuryVO) {

    MercuryMessageValidatorResponse response = new MercuryMessageValidatorResponse();
    response.setValid(true);

    // validate common fields
    response = validateCommonFields(response, mercuryVO);

    // only validate price if Ask Answer code is P
    validatePrice(mercuryVO, response);

    return response;
  }

  /**
   * This method validates the Outgoing CANCEL message
   * 
   * @param mercuryVO
   *          message to validate
   * @return MercuryMessageValidatorResponse
   */
  private MercuryMessageValidatorResponse validateCAN(MercuryMessageVO mercuryVO) {

    MercuryMessageValidatorResponse response = new MercuryMessageValidatorResponse();
    response.setValid(true);

    // validate common fields
    response = validateCommonFields(response, mercuryVO);

    return response;

  }

  /**
   * This method validates the Outgoing FTD Order message
   * 
   * @param mercuryVO
   *          message to validate
   * @return MercuryMessageValidatorResponse
   */
  private MercuryMessageValidatorResponse validateFTD(MercuryMessageVO mercuryVO) {
    MercuryMessageValidatorResponse response = new MercuryMessageValidatorResponse();
    response.setValid(true);

    // validate order date
    validateOrderDate(mercuryVO, response);

    // validate delivery date text
    validateDeliveryDateText(mercuryVO, response);

    // validate operator
    validateOperator(mercuryVO, response);

    // validate filling florist code
    validateFillingFloristCode(mercuryVO, response);

    // validate recipient details
    validateRecipientDetails(mercuryVO, response);

    // validate product specific details
    validateProductDetails(mercuryVO, response);

    // validate priority
    validatePriority(mercuryVO, response);

    return response;
  }

  /**
   * This method validates the Outgoing GENERAL Message
   * 
   * @param mercuryVO
   *          message to validate
   * @return MercuryMessageValidatorResponse
   */
  private MercuryMessageValidatorResponse validateGEN(MercuryMessageVO mercuryVO) {
    MercuryMessageValidatorResponse response = new MercuryMessageValidatorResponse();
    response.setValid(true);

    // validate filling florist code
    validateFillingFloristCode(mercuryVO, response);

    if (response.isValid() && MercuryEapiUtil.isEmpty(mercuryVO.getComments())) {
      logger.debug(MercuryEapiConstants.LOG_MESSAGE_PART + mercuryVO.getMercuryID()
          + MercuryEapiConstants.LOG_MESSAGE_PART1
          + MercuryEapiConstants.ERROR_MESSAGE_MISSING_COMMENTS);
      response.setValid(false);
      response.setMessage(MercuryEapiConstants.ERROR_MESSAGE_MISSING_COMMENTS);
    }

    // validate operator
    validateOperator(mercuryVO, response);

    // validate priority
    validatePriority(mercuryVO, response);

    return response;
  }

  /**
   * This method validates the Outgoing REJECT message
   * 
   * @param mercuryVO
   *          message to validate
   * @return MercuryMessageValidatorResponse
   */
  private MercuryMessageValidatorResponse validateREJ(MercuryMessageVO mercuryVO) {
    MercuryMessageValidatorResponse response = new MercuryMessageValidatorResponse();
    response.setValid(true);

    // validate common fields
    response = validateCommonFields(response, mercuryVO);

    return response;
  }

  /**
   * This method validates the ADJUSTMENT Message
   * 
   * @param mercuryVO
   *          message to validate
   * @return MercuryMessageValidatorResponse
   */
  private MercuryMessageValidatorResponse validateADJ(MercuryMessageVO mercuryVO) {
    MercuryMessageValidatorResponse response = new MercuryMessageValidatorResponse();
    response.setValid(true);

    return response;
  }

  /**
   * Validate Mercury Order Number, Order Date, Deliery Date Text, Comments Operator
   * 
   * @param response
   * @param mercuryVO
   *          message to validate
   * @return MercuryMessageValidatorResponse
   */
  private MercuryMessageValidatorResponse validateCommonFields(
      MercuryMessageValidatorResponse response, MercuryMessageVO mercuryVO) {

    // validate mercury order number
    validateMercuryOrderNumber(mercuryVO, response);

    // validate order date
    validateOrderDate(mercuryVO, response);

    // validate delivery date text
    validateDeliveryDateText(mercuryVO, response);

    if (response.isValid() && MercuryEapiUtil.isEmpty(mercuryVO.getComments())) {
      logger.debug(MercuryEapiConstants.LOG_MESSAGE_PART + mercuryVO.getMercuryID()
          + MercuryEapiConstants.LOG_MESSAGE_PART1
          + MercuryEapiConstants.ERROR_MESSAGE_MISSING_REASON);
      response.setValid(false);
      response.setMessage(MercuryEapiConstants.ERROR_MESSAGE_MISSING_REASON);
    }

    // validate operator
    validateOperator(mercuryVO, response);

    return response;
  }

  /**
   * This method validates the mercury order number as per the format
   * 
   * @param value
   * @return boolean
   */
  public boolean isValidMercuryOrderNumber(String value) {
    // start assuming value is invalid
    boolean valid = false;

    // check if null
    if (value != null) {
      // first char must be letter
      if (Character.isLetter(value.charAt(0))) {
        // fifth char must be letter
        if (Character.isLetter(value.charAt(5))) {
          // must have dash in sixth position
          if (value.substring(6, 7).equals("-")) {
            try {
              Integer.parseInt(value.substring(1, 5));
              Integer.parseInt(value.substring(7, 11));

              // if we made it this far then value is valid
              valid = true;
            }
            catch (Exception e) {
              // not numeric
              logger.error(MercuryEapiConstants.ERROR_MESSAGE_INVALID_ORDER_NUMBER);
            }
          }// end must have dash
        }// end fifth char letter
      }// end first char letter
    }// if correct length

    return valid;
  }

  /**
   * This method validates the price with in the configured range
   * 
   * @param value
   * @return boolean
   */
  private boolean isValidPrice(Double value) {
    boolean valid = true;

    // null check
    if (value == null) {
      return false;
    }

    double dblValue = value.doubleValue();

    // check min
    if (dblValue < MercuryEapiConstants.VALID_PRICE_MIN) {
      valid = false;
    }

    // check max
    if (dblValue > MercuryEapiConstants.VALID_PRICE_MAX) {
      valid = false;
    }

    return valid;
  }

  /**
   * This method validates the filling florist code as per the standard format
   * 
   * @param value
   * @return boolean
   */
  private boolean isValidFillingFloristCode(String fillingFloristCode) {
    return isValidFloristCode(fillingFloristCode);
  }

  /**
   * This method validates the main member code as per the standard format
   * 
   * @param value
   * @return boolean
   */
  public boolean isValidMainMemberCode(String mainMemberCode) {

    return isValidFloristCode(mainMemberCode);

  }

  /**
   * This method validates the florist code as per the standard format
   * 
   * @param value
   * @return boolean
   */
  private boolean isValidFloristCode(String value) {

    // assume invalid to start
    boolean valid = false;

    // check length
    if (value.length() == 9) {
      // check for dash
      if (value.substring(2, 3).equals("-")) {
        // check for chars at last two positins
        if (Character.isLetter(value.charAt(7)) && Character.isLetter(value.charAt(8))) {
          try {
            Integer.parseInt(value.substring(0, 2));
            Integer.parseInt(value.substring(3, 7));

            // if we made this far then it is valid
            valid = true;
          }
          catch (Exception e) {
            // invalid numbers
            logger.error(MercuryEapiConstants.ERROR_MESSAGE_INVALID_FLORIST_CODE);
          }
        }// end char check
      }// end dash check
    }// end length check

    return valid;

  }

  /**
   * This method validates the price only when the askAnsCode is not null
   * 
   * @param mercuryVO
   * @param response
   */
  private void validatePrice(MercuryMessageVO mercuryVO,
      MercuryMessageValidatorResponse response) {
    // only validate price if Ask Answer code contains a value
    if (mercuryVO.getAskAnswerCode() != null && mercuryVO.getAskAnswerCode().length() > 0) {

      // validate price
      if (response.isValid() && !isValidPrice(mercuryVO.getPrice())) {
        logger.debug(MercuryEapiConstants.LOG_MESSAGE_PART + mercuryVO.getMercuryID()
            + MercuryEapiConstants.LOG_MESSAGE_PART1
            + MercuryEapiConstants.ERROR_MESSAGE_INVALID_PRICE);
        response.setMessage(MercuryEapiConstants.ERROR_MESSAGE_INVALID_PRICE);
        response.setValid(false);
      }
    }
  }

  /**
   * This method validates the price only when the askAnsCode is not null
   * 
   * @param mercuryVO
   * @param response
   */
  private void validateFillingFloristCode(MercuryMessageVO mercuryVO,
      MercuryMessageValidatorResponse response) {
    // only validate price if Ask Answer code contains a value
    if (response.isValid() && MercuryEapiUtil.isEmpty(mercuryVO.getFillingFlorist())) {
      logger.debug(MercuryEapiConstants.LOG_MESSAGE_PART + mercuryVO.getMercuryID()
          + MercuryEapiConstants.LOG_MESSAGE_PART1
          + MercuryEapiConstants.ERROR_MESSAGE_MISSING_FILLING_FLORIST_CODE);
      response.setValid(false);
      response.setMessage(MercuryEapiConstants.ERROR_MESSAGE_MISSING_FILLING_FLORIST_CODE);
    }
    else {
      if (!isValidFillingFloristCode(mercuryVO.getFillingFlorist())) {
        logger.debug(MercuryEapiConstants.LOG_MESSAGE_PART + mercuryVO.getMercuryID()
            + MercuryEapiConstants.LOG_MESSAGE_PART1
            + MercuryEapiConstants.ERROR_MESSAGE_INVALID_FILLING_FLORIST_NUMBER);
        response.setValid(false);
        response.setMessage(MercuryEapiConstants.ERROR_MESSAGE_INVALID_FILLING_FLORIST_NUMBER);
      }
    }
  }

  /**
   * This method validates the order date
   * 
   * @param mercuryVO
   * @param response
   */
  private void validateOrderDate(MercuryMessageVO mercuryVO,
      MercuryMessageValidatorResponse response) {
    if (response.isValid() && MercuryEapiUtil.isEmpty(mercuryVO.getOrderDate())) {
      logger.debug(MercuryEapiConstants.LOG_MESSAGE_PART + mercuryVO.getMercuryID()
          + MercuryEapiConstants.LOG_MESSAGE_PART1
          + MercuryEapiConstants.ERROR_MESSAGE_MISSING_ORDER_DATE);
      response.setValid(false);
      response.setMessage(MercuryEapiConstants.ERROR_MESSAGE_MISSING_ORDER_DATE);
    }
  }

  /**
   * This method validates the operator
   * 
   * @param mercuryVO
   * @param response
   */
  private void validateOperator(MercuryMessageVO mercuryVO,
      MercuryMessageValidatorResponse response) {
    if (response.isValid() && MercuryEapiUtil.isEmpty(mercuryVO.getOperator())) {
      logger.debug(MercuryEapiConstants.LOG_MESSAGE_PART + mercuryVO.getMercuryID()
          + MercuryEapiConstants.LOG_MESSAGE_PART1
          + MercuryEapiConstants.ERROR_MESSAGE_MISSING_OPERATOR);
      response.setValid(false);
      response.setMessage(MercuryEapiConstants.ERROR_MESSAGE_MISSING_OPERATOR);
    }
  }

  /**
   * This method validates the priority
   * 
   * @param mercuryVO
   * @param response
   */
  private void validatePriority(MercuryMessageVO mercuryVO,
      MercuryMessageValidatorResponse response) {
    if (response.isValid() && MercuryEapiUtil.isEmpty(mercuryVO.getPriority())) {
      logger.debug(MercuryEapiConstants.LOG_MESSAGE_PART + mercuryVO.getMercuryID()
          + MercuryEapiConstants.LOG_MESSAGE_PART1
          + MercuryEapiConstants.ERROR_MESSAGE_MISSING_PRIORITY);
      response.setValid(false);
      response.setMessage(MercuryEapiConstants.ERROR_MESSAGE_MISSING_PRIORITY);
    }
  }

  /**
   * This method validates the delivery date text
   * 
   * @param mercuryVO
   * @param response
   */
  private void validateDeliveryDateText(MercuryMessageVO mercuryVO,
      MercuryMessageValidatorResponse response) {

    if (response.isValid() && MercuryEapiUtil.isEmpty(mercuryVO.getDeliveryDateText())) {
      logger.debug(MercuryEapiConstants.LOG_MESSAGE_PART + mercuryVO.getMercuryID()
          + MercuryEapiConstants.LOG_MESSAGE_PART1
          + MercuryEapiConstants.ERROR_MESSAGE_MISSING_DELIVERY_DATE);
      response.setValid(false);
      response.setMessage(MercuryEapiConstants.ERROR_MESSAGE_MISSING_DELIVERY_DATE);
    }
  }

  /**
   * This method validates mercury order number
   * 
   * @param mercuryVO
   * @param response
   */
  private void validateMercuryOrderNumber(MercuryMessageVO mercuryVO,
      MercuryMessageValidatorResponse response) {

    if (response.isValid() && MercuryEapiUtil.isEmpty(mercuryVO.getMercuryOrderNumber())) {
      logger.debug(MercuryEapiConstants.LOG_MESSAGE_PART + mercuryVO.getMercuryID()
          + MercuryEapiConstants.LOG_MESSAGE_PART1
          + MercuryEapiConstants.ERROR_MESSAGE_MISSING_ORDER_NUMBER);
      response.setValid(false);
      response.setMessage(MercuryEapiConstants.ERROR_MESSAGE_MISSING_ORDER_NUMBER);
    }
    else {
      if (!isValidMercuryOrderNumber(mercuryVO.getMercuryOrderNumber())) {
        logger.debug(MercuryEapiConstants.LOG_MESSAGE_PART + mercuryVO.getMercuryID()
            + MercuryEapiConstants.LOG_MESSAGE_PART1
            + MercuryEapiConstants.ERROR_MESSAGE_INVALID_ORDER_NUMBER);
        response.setValid(false);
        response.setMessage(MercuryEapiConstants.ERROR_MESSAGE_INVALID_ORDER_NUMBER);
      }
    }
  }

  /**
   * This method validates the recipient details
   * 
   * @param mercuryVO
   * @param response
   */
  private void validateRecipientDetails(MercuryMessageVO mercuryVO,
      MercuryMessageValidatorResponse response) {

    if (response.isValid() && MercuryEapiUtil.isEmpty(mercuryVO.getRecipient())) {
      logger.debug(MercuryEapiConstants.LOG_MESSAGE_PART + mercuryVO.getMercuryID()
          + MercuryEapiConstants.LOG_MESSAGE_PART1
          + MercuryEapiConstants.ERROR_MESSAGE_MISSING_RECEIPIENT_NAME);
      response.setValid(false);
      response.setMessage(MercuryEapiConstants.ERROR_MESSAGE_MISSING_RECEIPIENT_NAME);
    }

    if (response.isValid() && MercuryEapiUtil.isEmpty(mercuryVO.getAddress())) {
      logger.debug(MercuryEapiConstants.LOG_MESSAGE_PART + mercuryVO.getMercuryID()
          + MercuryEapiConstants.LOG_MESSAGE_PART1
          + MercuryEapiConstants.ERROR_MESSAGE_MISSING_ADDRESS);
      response.setValid(false);
      response.setMessage(MercuryEapiConstants.ERROR_MESSAGE_MISSING_ADDRESS);
    }

    if (response.isValid() && MercuryEapiUtil.isEmpty(mercuryVO.getCityStateZip())) {
      logger.debug(MercuryEapiConstants.LOG_MESSAGE_PART + mercuryVO.getMercuryID()
          + MercuryEapiConstants.LOG_MESSAGE_PART1
          + MercuryEapiConstants.ERROR_MESSAGE_MISSING_CITY_STATE_ZIP);
      response.setValid(false);
      response.setMessage(MercuryEapiConstants.ERROR_MESSAGE_MISSING_CITY_STATE_ZIP);
    }

    if (response.isValid() && MercuryEapiUtil.isEmpty(mercuryVO.getPhoneNumber())) {
      logger.debug(MercuryEapiConstants.LOG_MESSAGE_PART + mercuryVO.getMercuryID()
          + MercuryEapiConstants.LOG_MESSAGE_PART1
          + MercuryEapiConstants.ERROR_MESSAGE_MISSING_PHONE_NUMBER);
      response.setValid(false);
      response.setMessage(MercuryEapiConstants.ERROR_MESSAGE_MISSING_PHONE_NUMBER);
    }
  }

  /**
   * This method validates the product specific details
   * 
   * @param mercuryVO
   * @param response
   */
  private void validateProductDetails(MercuryMessageVO mercuryVO,
      MercuryMessageValidatorResponse response) {

    if (response.isValid() && MercuryEapiUtil.isEmpty(mercuryVO.getFirstChoice())) {
      logger.debug(MercuryEapiConstants.LOG_MESSAGE_PART + mercuryVO.getMercuryID()
          + MercuryEapiConstants.LOG_MESSAGE_PART1
          + MercuryEapiConstants.ERROR_MESSAGE_MISSING_FIRST_CHOICE);
      response.setValid(false);
      response.setMessage(MercuryEapiConstants.ERROR_MESSAGE_MISSING_FIRST_CHOICE);
    }

    if (response.isValid() && MercuryEapiUtil.isEmpty(mercuryVO.getSecondChoice())) {
      logger.debug(MercuryEapiConstants.LOG_MESSAGE_PART + mercuryVO.getMercuryID()
          + MercuryEapiConstants.LOG_MESSAGE_PART1
          + MercuryEapiConstants.ERROR_MESSAGE_MISSING_SECOND_CHOICE);
      response.setValid(false);
      response.setMessage(MercuryEapiConstants.ERROR_MESSAGE_MISSING_SECOND_CHOICE);
    }

    if (response.isValid() && !isValidPrice(mercuryVO.getPrice())) {
      logger.debug(MercuryEapiConstants.LOG_MESSAGE_PART + mercuryVO.getMercuryID()
          + MercuryEapiConstants.LOG_MESSAGE_PART1
          + MercuryEapiConstants.ERROR_MESSAGE_INVALID_PRICE);
      response.setValid(false);
      response.setMessage(MercuryEapiConstants.ERROR_MESSAGE_INVALID_PRICE);
    }

    if (response.isValid() && MercuryEapiUtil.isEmpty(mercuryVO.getOccasion())) {
      logger.debug(MercuryEapiConstants.LOG_MESSAGE_PART + mercuryVO.getMercuryID()
          + MercuryEapiConstants.LOG_MESSAGE_PART1
          + MercuryEapiConstants.ERROR_MESSAGE_MISSING_OCCASSION);
      response.setValid(false);
      response.setMessage(MercuryEapiConstants.ERROR_MESSAGE_MISSING_OCCASSION);
    }
  }

  /**
   * This method is used to validate the main member code
   * 
   * @param mainMemberCode
   * @throws MercuryEapiException
   */
  public void validateMainMemberCode(String mainMemberCode) throws MercuryEapiException {

    final String METHOD_NAME = ".validateMainMemberCode()";
    StringBuilder errorMessage = null;
    if (mainMemberCode == null) {

      // log the message main member code is null
      errorMessage = new StringBuilder();
      errorMessage.append(CLASS_NAME).append(METHOD_NAME).append(" Main Member Code is null.");
      logger.error(errorMessage.toString());
      throw new MercuryEapiException(errorMessage.toString());

    }
    else if (!isValidMainMemberCode(mainMemberCode)) {

      // log the message main member code is invalid
      errorMessage = new StringBuilder();
      errorMessage.append(CLASS_NAME).append(METHOD_NAME).append(" Main Member Code is invalid.");
      logger.error(errorMessage.toString());
      throw new MercuryEapiException(errorMessage.toString());

    }
  }

  /**
   * This main method is for unit testing
   * 
   * @TODO move it to a separate test class
   * @param args
   */
  public void main(String[] args) {

    MercuryMessageVO mercuryVO = new MercuryMessageVO();
    mercuryVO.setMercuryID("FTD111");
    mercuryVO.setMessageType("AA");
    // MercuryMessageValidatorResponse response = new
    // MercuryMessageValidatorResponse();
    // me.validateCommonFields(response, mercuryVO);
    try {
    	MercuryMessageValidator mmv = new MercuryMessageValidator();
        mmv.validate(mercuryVO);
    }
    catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

}
