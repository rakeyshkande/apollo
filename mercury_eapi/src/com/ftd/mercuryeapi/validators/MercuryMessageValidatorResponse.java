package com.ftd.mercuryeapi.validators;

public class MercuryMessageValidatorResponse {

  private String message;
  private boolean valid;

  public MercuryMessageValidatorResponse() {
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String value) {
    message = value;
  }

  public boolean isValid() {
    return valid;
  }

  public void setValid(boolean value) {
    valid = value;
  }

}