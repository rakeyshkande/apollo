package com.ftd.mercuryeapi.test;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Pattern;

public class PropertyFileLoadTest {

	public static void main(String... strings) {

		//createPropertyFile();
		//readPropertyFile();
		//readSystemProperties();
		System.err.println(isValidSuffix("AAA"));
		System.err.println(isValidSuffix("A1"));
		System.err.println(isValidSuffix("11"));

	}

	/**
	 * This returns the array of files whose names matching with the provided
	 * pattern
	 * 
	 * @param root
	 * @param regex
	 * @return
	 */
	private static File[] listFilesMatching(File root, String regex) {
		if (!root.isDirectory()) {
			throw new IllegalArgumentException(root + " is no directory.");
		}
		final Pattern p = Pattern.compile(regex); // careful: could also throw
													// an exception!
		return root.listFiles(new FileFilter() {
			@Override
			public boolean accept(File file) {
				return p.matcher(file.getName()).matches();
			}
		});
	}

	private static void createPropertyFile() {
		Properties prop = new Properties();

		try {
			// set the properties value
			prop.setProperty("database", "localhost");
			prop.setProperty("dbuser", "mkyong");
			prop.setProperty("dbpassword", "password");

			// save properties to project root folder
			prop.store(new FileOutputStream("C:\\Apps\\config.properties"),
					null);

		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	private static void readPropertyFile() {
		Properties prop = new Properties();

		String pathToEAPIPasswordConfigFile = "user.dir";

		File pathToEAPIPasswordConfigDirectory = null;

		try {
			pathToEAPIPasswordConfigDirectory = new File(
					System.getProperty(pathToEAPIPasswordConfigFile));
			
			System.out.println("pathToEAPIPasswordConfigDirectory:: "+pathToEAPIPasswordConfigDirectory);

			File[] eapiPasswordConfigFiles = listFilesMatching(
					pathToEAPIPasswordConfigDirectory,
					"eapi-password.properties");

			// load a properties file
			prop.load(new FileInputStream(eapiPasswordConfigFiles[0]));

			// get the property value and print it out
			System.out.println(prop.getProperty("AH"));

		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	private static void readSystemProperties() {
		Properties properties = System.getProperties();
		Set<Object> sysPropertiesKeys = properties.keySet();
		for (Object key : sysPropertiesKeys) {
			System.out.println(key + " ="
					+ properties.getProperty((String) key));
		}
	}
	
	private static Boolean isValidSuffix(String suffix) {
		Boolean isSuffixValid = false;

		if (suffix.matches("[A-Z][A-Z]")) {
			isSuffixValid = true;
		}

		return isSuffixValid;
	}

}
