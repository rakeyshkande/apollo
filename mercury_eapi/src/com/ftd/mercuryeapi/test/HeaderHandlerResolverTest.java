package com.ftd.mercuryeapi.test;

import java.util.ArrayList;
import java.util.List;

import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.HandlerResolver;
import javax.xml.ws.handler.PortInfo;

/**
 * 
 * @author www.javadb.com
 */
public class HeaderHandlerResolverTest implements HandlerResolver {
	
	private String suffix;
	
	public HeaderHandlerResolverTest()
	{
		
	}
	
	public HeaderHandlerResolverTest(String suffix)
	{
		this.suffix = suffix;
	}

	public List<Handler> getHandlerChain(PortInfo portInfo) {

		List<Handler> handlerChain = new ArrayList<Handler>();
		HeaderHandlerTest hh = new HeaderHandlerTest(suffix);
		handlerChain.add(hh);

		return handlerChain;
	}

}
