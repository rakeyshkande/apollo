package com.ftd.mercuryeapi.test;

import java.util.HashMap;
import java.util.Map;


public class SuffixSingleTon {

	private static SuffixSingleTon suffixSingleTon;
	private static Map<String,String> suffixMap= new HashMap<String,String>();

	private SuffixSingleTon() {

	}

	/**
	 * Returns a reference of the EAPI web service
	 * 
	 * @return <code>EapiWebServicelocator<code>
	 */
	public static SuffixSingleTon getSuffixSingleTon() {

		if (suffixSingleTon == null) {

			suffixSingleTon = new SuffixSingleTon();

		}

		return suffixSingleTon;

	}

	public String getSuffixInstance(String suffix) {
		
		if(suffixMap.containsKey(suffix))
		{
			return suffixMap.get(suffix);
		} else
		{		
			suffixMap.put(suffix, suffix);
		}

		return suffixMap.get(suffix);
	}

}
