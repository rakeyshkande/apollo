package com.ftd.mercuryeapi.test;

import java.util.Set;

import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

/**
 * 
 * @author www.javadb.com
 */
public class HeaderHandlerTest implements SOAPHandler<SOAPMessageContext> {
	
	

	private static String SECURITY_URI = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
	private static String PREFIX = "wsse";
	private static String SECURITY_NAME = "Security";
	private static String USER_NAME_TOKEN = "UsernameToken";
	
	private static String USER_NAME = "Username";
	private static String PASSWORD = "Password";
	
	
	//TODO CHANGE THE hard coded values
	private static String USER_NAME_DATA = "908400AA17912";
	private static String PASSWORD_TYPE_VALUE = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText";
	private static String PASSWORD_DATA = "%ZdED53%";	
private String suffix;
	
	public HeaderHandlerTest()
	{
		
	}
	
	public HeaderHandlerTest(String suffix)
	{
		this.suffix = suffix;
	}

	public boolean handleMessage(SOAPMessageContext smc) {
		
		final String METHOD_NAME =".handleMessage()";

		Boolean outboundProperty = (Boolean) smc.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);

		if (outboundProperty.booleanValue()) {
			SOAPMessage message = smc.getMessage();
			try {
				SOAPEnvelope envelope = smc.getMessage().getSOAPPart().getEnvelope();
				SOAPHeader header = envelope.getHeader();
				if (header == null) {
					header = envelope.addHeader();
				}
			
				SOAPElement security = header.addChildElement(SECURITY_NAME,PREFIX,SECURITY_URI);
				SOAPElement usernameToken = security.addChildElement(USER_NAME_TOKEN, PREFIX);	
				
				//String userName = MercuryEapiUtil.getEAPIUserName(suffix);
				
				//logger.debug(CLASS_NAME+METHOD_NAME+"-"+userName);

				SOAPElement username = usernameToken.addChildElement(USER_NAME, PREFIX);
				username.addTextNode("908400AA17912");

				SOAPElement password = usernameToken.addChildElement(PASSWORD, PREFIX);
				password.setAttribute("Type",PASSWORD_TYPE_VALUE);
				String passwordString = null;
				//passwordString = MercuryEapiUtil.getEAPIPasswordFromSSHStore(suffix);
				passwordString ="test1234";
				
				password.addTextNode(passwordString);		
				message.writeTo(System.out);
				System.out.println(" ");
				
				
			} catch (Exception e) {
				e.printStackTrace();
			
			}
		} else {
			try {
				// This handler does nothing with the response from the Web
				// Service so
				// we just print out the SOAP message.
				SOAPMessage message = smc.getMessage();
				message.writeTo(System.out);
				System.out.println(" ");
				
			} catch (Exception ex) {
				
			}
		}
		return outboundProperty;

	}

	public Set getHeaders() {
		// throw new UnsupportedOperationException("Not supported yet.");
		return null;
	}

	public boolean handleFault(SOAPMessageContext context) {
		// throw new UnsupportedOperationException("Not supported yet.");
		return true;
	}

	public void close(MessageContext context) {
		// throw new UnsupportedOperationException("Not supported yet.");
	}
}
