package com.ftd.mercuryeapi.test;
/*
 * -------------------------------History-------------------------------
 * Author      	CreatedDate			ModifiedDate     	Reason
 * ---------------------------------------------------------------------
 * smotla		Jun 17th 2013		Jun 17th 2013		Apollo-EAPI Integration
 *      
 */
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;

import com.ftd.eapi.message.service.v1.DirectionType;
import com.ftd.eapi.message.service.v1.EAPIMessageService;
import com.ftd.eapi.message.service.v1.EAPIMessageServiceException;
import com.ftd.eapi.message.service.v1.EAPIMessageService_Service;
import com.ftd.eapi.message.service.v1.EapiException;
import com.ftd.eapi.message.service.v1.EapiMessageServiceFault;
import com.ftd.eapi.message.service.v1.ErosMessage;
import com.ftd.eapi.message.service.v1.MultiMessageListResponse;
import com.ftd.eapi.message.service.v1.MultiMessageNewListResponse;
import com.ftd.eapi.message.service.v1.ObjectFactory;
import com.ftd.eapi.message.service.v1.OccasionCode;
import com.ftd.eapi.message.service.v1.OrderMessage;
import com.ftd.eapi.message.service.v1.RetrieveErosMessage;
import com.ftd.eapi.message.service.v1.RetrieveErosMessageList;
import com.ftd.eapi.message.service.v1.Service;
import com.ftd.eapi.message.service.v1.SingleMessageResponse;

public class EAPIWSTest1_wsimport {

	public static void main(String... strings) {

		//String wsdlURL = "http://eapiqapp01v1.ftdi.com:8080/EAPIMessageServices-1.0/EAPIMessageService?wsdl";
		String wsdlURL = "https://eapiperf.mercurynetwork.com/EAPIMessageServices-1.0/EAPIMessageService?wsdl";
		//String wsdlURL = "http://eapiperfapp01v3.ftdi.com:8080/EAPIMessageServices-1.0/EAPIMessageService?wsdl";
		
		
		//String wsdlURL = "https://eapitest.mercurynetwork.com/EAPIMessageServices-1.0/EAPIMessageService?wsdl";
		URL eapiWSDLUrl = null;
		try {
			eapiWSDLUrl = new URL(wsdlURL);
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		EAPIMessageService_Service eapiWSSoapPort = new EAPIMessageService_Service(
				eapiWSDLUrl, new QName("http://eapi.ftd.com/message/service/v1/","EAPIMessageService"));

		HeaderHandlerResolverTest handlerResolver = new HeaderHandlerResolverTest("AA");
		eapiWSSoapPort.setHandlerResolver(handlerResolver);

		EAPIMessageService eapiWS = eapiWSSoapPort.getEAPIMessageServicePort();

		//getMessageDetailsByMessageId(eapiWS);
		//getMessageDetailsByReferenceNumber(eapiWS);
		getNewMessageList(eapiWS);
		//getMessageList(eapiWS, null);
		//postMessage(eapiWS, "FTD");
		//confirmMessageByReference(eapiWS);

	}

	private static void postMessage(EAPIMessageService eapiWS,
			String messageType) {
		try {

			ErosMessage erosMessage = null;

			if ("FTD".equalsIgnoreCase(messageType)) {
				erosMessage = buildErosMessage(messageType);
			}

			SingleMessageResponse smr = eapiWS.postMessage("90-0571AH",
					erosMessage);

			if (smr == null) {
				System.err.println("Response is null.");
			} else {

				System.err.println(smr.getResponseCode().getCode());
				System.err
						.println(smr.getProcessedErosMessage().getMessageId());
			}
		} catch (EAPIMessageServiceException e) {
			System.err.println("Exception is caught.");

			EapiMessageServiceFault eapiFault = e.getFaultInfo();
			System.err.println("Reason Code::." + eapiFault.getReasonCode());
			System.err.println("Reason Message::." + eapiFault.getMessage());
			List<EapiException> listOfExceptions = eapiFault.getExceptions();
			for (EapiException eapiException : listOfExceptions) {
				if (eapiException != null) {
					System.err.println("EAPI :: Error Code :: "
							+ eapiException.getCode());
					System.err.println("EAPI :: Error Code :: "
							+ eapiException.getMessage());
				}
			}
		}
	}

	private static ErosMessage buildErosMessage(String messageType) {
		ObjectFactory of = new ObjectFactory();
		OrderMessage orderMessage = of.createOrderMessage();
		orderMessage.setOperator("MI");
		orderMessage.setService(Service.FTD);
		orderMessage.setSendingMemberCode("90-0571AH");
		orderMessage.setReceivingMemberCode("90-0001EV");
		orderMessage.setBypassSuspends(false);
		orderMessage.setCardMessage("Happy Anniversary dude..!");
		orderMessage.setConventionMessage("EAPI to FTDM");
		orderMessage.setDeliveryDate(getXMLGregorianDate("31/07/2013"));
		orderMessage.setDeliveryDetails("RUSH");
		orderMessage.setFirstChoiceProductCode("ARR");
		orderMessage.setSecondChoiceProductCode("ARV");
		orderMessage.setFirstChoiceProductDescription("First choice flowers");
		orderMessage.setSecondChoiceProductDescription("Second choice flower");
		orderMessage.setOccasionCode(OccasionCode.ANNIVERSARY);
		BigDecimal bd = new BigDecimal(21);
		orderMessage.setPrice(bd);
		orderMessage.setRecipientCity("Downers Grove");
		orderMessage.setRecipientCityStateZip("");
		orderMessage.setRecipientCountryCode("U");
		orderMessage.setRecipientName("EAPI to Non-EAPI on 7th may");
		orderMessage.setRecipientPhoneNumber("2108277947");
		orderMessage.setRecipientPostalCode("aaaa");
		orderMessage.setRecipientStateCode("IL");
		orderMessage.setRecipientStreetAddress("23 Main St");
		orderMessage.setAllowAutoForward(false);
		orderMessage.setGotoForwardOnly(false);
		orderMessage.setAllowFillerForward(false);
		orderMessage.setTagLine("all iz well");
		orderMessage.setSpecialInstructions("e it pretty please.382");

		return orderMessage;
	}

	private static void confirmMessageByReference(EAPIMessageService eapiWS) {
		try {

			SingleMessageResponse smr = eapiWS.getMessageDetailByMessageID(
					"90-0571AH", 18287809L);

			System.err.println("after making call.");

			if (smr == null) {
				System.err.println("Response is null.");
			} else {

				System.err.println(smr.getResponseCode().getCode());
				System.err.println(smr.getResponseCode().getMessage());
			}
		} catch (EAPIMessageServiceException e) {

		}
	}

	private static void getMessageDetailsByMessageId(EAPIMessageService eapiWS) {
		try {

			SingleMessageResponse smr = eapiWS.getMessageDetailByMessageID(
					"90-0571AH", 18287809L);

			System.err.println("after making call.");

			if (smr == null) {
				System.err.println("Response is null.");
			} else {

				System.err.println(smr.getResponseCode().getCode());
				System.err.println(smr.getResponseCode().getMessage());
			}
		} catch (Exception e) {

		}
	}

	private static void getMessageDetailsByReferenceNumber(
			EAPIMessageService eapiWS) {
		try {

			SingleMessageResponse smr = eapiWS.getMessageDetailByReference(
					"90-0571AH", "S3377H");

			System.err.println("after making call.");

			if (smr == null) {
				System.err.println("Response is null.");
			} else {

				System.err.println(smr.getResponseCode().getCode());
				System.err.println(smr.getResponseCode().getMessage());
			}
		} catch (EAPIMessageServiceException e) {

		}
	}

	private static void getNewMessageList(EAPIMessageService eapiWS) {
		try {

			MultiMessageNewListResponse mnlr = eapiWS.getNewMessageList(
					"90-8400AA", 10);

			System.err.println("after making call.");

			if (mnlr == null) {
				System.err.println("Response is null.");
			} else {

				System.err.println(mnlr.getResponseCode().getCode());
				System.err.println(mnlr.getResponseCode().getMessage());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void getMessageList(EAPIMessageService eapiWS,
			Map<String, Object> inputMap) {
		try {

			MultiMessageListResponse mnlr = eapiWS.getMessageList("90-0571AH",
					getXMLGregorianDate("15/05/2013"),
					getXMLGregorianDate("17/05/2013"), DirectionType.BOTH);

			System.err.println("after making call.");

			if (mnlr == null) {
				System.err.println("Response is null.");
			} else {

				System.err.println(mnlr.getResponseCode().getCode());
				System.err.println(mnlr.getResponseCode().getMessage());
				System.err.println(mnlr.getRetrieveErosMessageList()
						.getRetrieveErosMessage().size());

				RetrieveErosMessageList retrieveErosMessageList = mnlr
						.getRetrieveErosMessageList();
				if (retrieveErosMessageList == null) {

				} else {
					if (retrieveErosMessageList.getRetrieveErosMessage() != null
							&& retrieveErosMessageList.getRetrieveErosMessage()
									.size() > 0) {
						for (RetrieveErosMessage erosMessage : retrieveErosMessageList
								.getRetrieveErosMessage()) {

							System.err.println(erosMessage.getMessageStatus());
							System.err
									.println(erosMessage.getReferenceNumber());
							System.err.println(erosMessage
									.getRelatedOrderReferenceNumber());

						}
					}

				}

			}
		} catch (EAPIMessageServiceException e) {

		}
	}

	private static XMLGregorianCalendar getXMLGregorianDate(String dateString) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date date = null;
		try {
			date = sdf.parse(dateString);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(date);
		XMLGregorianCalendar gcDate = null;
		try {
			gcDate = DatatypeFactory.newInstance().newXMLGregorianCalendarDate(
					2013, 07, 31, 0);
		} catch (DatatypeConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.err.println("gcDate::: " + gcDate);

		return gcDate;
	}

}
