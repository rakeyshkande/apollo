/**
 * 
 */
package com.ftd.mercuryeapi.util;

import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.message.Message;

public class EAPILoggingInInterceptor extends LoggingInInterceptor {
    
    com.ftd.osp.utilities.plugins.Logger logger = new com.ftd.osp.utilities.plugins.Logger(EAPILoggingInInterceptor.class.getName());

    /**
     * Mask the username and password value
     */ 
    @Override
    protected String transform(String originalLogString) {
      String maskedLogString = originalLogString.replaceAll("(?<=<Username>)(.*?)(?=</Username>)", "xxxxxx").replaceAll("(?<=<Password)(.*?)(?=</Password>)", ">xxxxxx");
        return maskedLogString;
    } 
    
    @Override
    public void handleMessage(Message message) throws Fault {
        if (writer != null || logger.isDebugEnabled()) {
            logging(message);
        }
    }
    
    @Override
    protected void log(String message) {
        message = transform(message);
        if (writer != null) {
            writer.println(message);
            // Flushing the writer to make sure the message is written 
            writer.flush();
        } else if (logger.isDebugEnabled()) {
            logger.info(message);
        }
    }
    


}
