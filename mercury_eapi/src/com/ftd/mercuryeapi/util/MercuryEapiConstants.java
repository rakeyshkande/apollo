package com.ftd.mercuryeapi.util;

public interface MercuryEapiConstants {

  // this is set to TRUE while running locally (testing\debuging\developing)
  public static final boolean DO_NOT_SEND_JMS_MESSAGES = true;

  // Mercury status's
  public static final String MERCURY_OPEN = "MO"; // Message waiting to be
  // processed
  public static final String MERCURY_QUEUED = "MQ"; // Message being processed
  public static final String MERCURY_CLOSED = "MC"; // Message processed
  public static final String MERCURY_ERROR = "ME"; // Message in error
  public static final String MERCURY_REJECT = "MR";

  // Message types
  public static final String ANS_MESSAGE = "ANS";
  public static final String ASK_MESSAGE = "ASK";
  public static final String CAN_MESSAGE = "CAN";
  public static final String FTD_MESSAGE = "FTD";
  public static final String GEN_MESSAGE = "GEN";
  public static final String RET_MESSAGE = "RET";
  public static final String REJ_MESSAGE = "REJ";
  public static final String ADJ_MESSAGE = "ADJ";
  public static final String FOR_MESSAGE = "FOR";
  public static final String SUS_MESSAGE = "SUS";
  public static final String RES_MESSAGE = "RES";

  public static final String CONFIRM_CANCEL_MESSAGE = "CON";
  public static final String DENY_CANCEL_MESSAGE = "DEN";

  // Config file name
  public static final String MERCURYEAPI_PROPERTY_FILE = "mercuryeapi_config.xml";
  public static final String MERCURY_CONFIG_CONTEXT = "MERCURY_INTERFACE_CONFIG";
  public static final String MERCURY_SECURE_CONFIG_CONTEXT = "MERCURY_EAPI";

  public static final String MERCURY_DATASOURCE_PROPERTY = "DATASOURCE";
  public static final String JMS_DELAY_PROPERTY = "JMS_OracleDelay";
  public static final String EAPI_DELAY = "JMS_OracleDelay";

  // retrival values
  public static final String MANUAL_RETRIEVAL = "M";
  public static final String AUTO_RETRIEVAL = "A";

  public static final String EFOS_DOWN_ERROR = "EFOS is currently not available";
  public static final String BOXX_BUSY_ERROR = "Your Signon Has Been Rejected By EFOS";

  public static final String SAK_REJECT_TEXT = "reject";

  public static final String SUSPEND_TEXT = "GOTOSUS";
  public static final String RESUME_TEXT = "GOTORES";

  public static final String EROS_SYSTEM_CODES = "00-0001 00-0002 00-0003";

  // added for EAPI project
  public static final String DATASOURCE = "DATASOURCE";
  public static final String MESSAGE_SOURCE = "MESSAGE_SOURCE";
  public static final String SERVICE_APOLLO = "APOLLO";
  public static final String SERVICE_FTD = "FTD";
  public static final String EAPI_WS_CONTEXT = "SERVICE";
  public static final String EAPI_WS_PASSWORD_FILE = "EAPI_WS_PASSWORD_FILE";
  public static final String USER_NAME = "_USER_NAME";
  public static final String EAPI_SERVICE = "EAPI_SERVICE_";
  public static final String PASSWORD = "_PASSWORD";
  public static final String IS_EAPI_ON = "IS_EAPI_ON";
  public static final String EAPI_WS_URL = "EAPI_WS_URL";
  public static final String EAPI_WS_WSDL = "EAPI_WS_WSDL";
  public static final String MERCURY_EAPI_SUFFIX = "MERCURY_EAPI_SUFFIX";
  public static final String EAPI_DOWN_TIME_CHECK_REQUIRED = "EAPI_DOWN_TIME_CHECK_REQUIRED";
  public static final String RECORDS_LIMIT_FOR_POLLED_PROCESS = "RECORDS_LIMIT_FOR_POLLED_PROCESS";
  public static final String RECORDS_LIMIT_FOR_POST_PROCESS = "RECORDS_LIMIT_FOR_POST_PROCESS";
  public static final String POLL_MESSAGE_LIMIT = "POLL_MESSAGE_LIMIT";
  public static final String STATUS_PARAM = "RegisterOutParameterStatus";
  public static final String MESSAGE_PARAM = "RegisterOutParameterMessage";
  public static final String POST_PROCESS = "POST";
  public static final String POLLING_PROCESS = "POLLING";
  public static final String POLLED_PROCESS = "POLLED";
  public static final String POST_PROCESS_QUEUE = "MERCURY_PROCESS_POST";
  public static final String POLLING_PROCESS_QUEUE = "MERCURY_POLL";
  public static final String POLLED_PROCESS_QUEUE = "MERCURY_PROCESS_POLL";
  
  public static final String MARS_MERCURY_QUEUE = "MARSMERCURY";
  public static final String DEFAULT_POLL_DELAY = "DEFAULT_POLL_DELAY";
  public static final String DEFAULT_EXCEPTION_DELAY = "DEFAULT_EXCEPTION_DELAY";
  public static final String DEFAULT_POLL_PROCESS_DELAY = "DEFAULT_POLL_PROCESS_DELAY";
  public static final String CXF_CLIENT_TIMEOUT = "EAPI_CXF_TIMEOUT";
  public static final int DEFAULT_CXF_CLIENT_TIMEOUT = 60000;

  // error types -EAPI project
  public static final String ERROR = "ERROR";

  // global parameters cache constants
  public static final String CACHE_NAME_GLOBAL_PARM = "CACHE_NAME_GLOBAL_PARM";

  // global parameters -EAPI Project - context
  public static final String LOAD_MEMBER_DATA = "LOAD_MEMBER_DATA";

  // global parameters -EAPI Project - names
  public static final String VIEW_QUEUE_SWITCH = "VIEW_QUEUE_SWITCH";

  // general constants
  public static final String Y_STRING = "Y";
  public static final String N_STRING = "N";
  public static final String DEFAULT_SENDING_FLORIST = "DEFAULT_SENDING_FLORIST";

  public static final String OCCASION = "OCCASION";
  public static final String[] ADJUSTMENT_REASON = { "NO_RECORD", "NON_DELIVERY", "OVERCHARGED",
      "UNDERCHARGED", "DUPLICATE_CHARGE", "PAID_DIRECT", "ORDER_CANCELED", "CHARGED_IN_ERROR",
      "INCOMING_NOT_OUTGOING", "UNSATISFACTORY" };

  // eapi response codes
  public static final String REJECTED = "REJECTED";
  public static final String RECEIVED = "RECEIVED";
  public static final String CONFIRM = "CONFIRM";
  public static final String INPROCESS = "INPROCESS";
  public static final String PROCESSED = "PROCESSED";
  public static final String CONFIRMED = "CONFIRMED";
  public static final String MERCURY_EAPI_POLL_QUEUE = "MERCURYEAPIPOLL";
  public static final String MERCURY_EAPI_POLL_PROCESS_QUEUE = "MERCURYEAPIPOLLPROCESS";
  public static final String MERCURY_EAPI_POST_PROCESS_QUEUE = "MERCURYEAPIPOSTPROCESS";
  public static final String EAPI_SYSTEM_REJECTED = "SYSTEM_REJECTED";

  // eapi - queue constants
  public static final String VERIFIED = "VERIFIED";

  // message direction constants
  public static final String INBOUND_DIRECTION = "INBOUND";
  public static final String OUTBOUND_DIRECTION = "OUTBOUND";

  // Error Message Constants
  public static final String[] ERROR_CODES = { "INTERNAL_SERVER_ERROR",
      "REQUIRED_SECURITY_CONTEXT_ERROR", "REQUIRED_AUTHENTICATION_ERROR", "SECURITY_ERROR",
      "Invalid Security Header", "Transaction rolled back",
      "Authentication Error, please contact FTD help desk." };
  public static final String INTERNAL_SERVER_ERROR = "Server is busy please try later or contact FTD help desk.";
  public static final String ERROR_MESSAGE_UNKNOWN_MSG_TYPE = "Unknown Message Type:";
  public static final String ERROR_MESSAGE_MSG_TYPE_NULL = "Message Type is null";
  public static final String ERROR_MESSAGE_MISSING_ORDER_NUMBER = "Missing Order Number";
  public static final String ERROR_MESSAGE_INVALID_ORDER_NUMBER = "Invalid Order Number";
  public static final String ERROR_MESSAGE_MISSING_ORDER_DATE = "Missing Order Date";
  public static final String ERROR_MESSAGE_MISSING_DELIVERY_DATE = "Missing Delivery Date";
  public static final String ERROR_MESSAGE_MISSING_COMMENTS = "Missing Comments";
  public static final String ERROR_MESSAGE_MISSING_REASON = "Missing Reason";
  public static final String ERROR_MESSAGE_MISSING_OPERATOR = "Missing Operator";
  public static final String ERROR_MESSAGE_INVALID_PRICE = "Invalid Price";
  public static final String ERROR_MESSAGE_MISSING_RETRIEVAL_FROM_DATE = "Missing Retrieval From Date";
  public static final String ERROR_MESSAGE_MISSING_RETRIEVAL_TO_DATE = "Missing Retrieval To Date";
  public static final String ERROR_MESSAGE_MISSING_RETRIEVAL_FROM_NUMBER = "Missing Retrieval From Number";
  public static final String ERROR_MESSAGE_MISSING_RETRIEVAL_TO_NUMBER = "Missing Retrieval To Number";
  public static final String ERROR_MESSAGE_MISSING_FILLING_FLORIST_CODE = "Missing filling florist code";
  public static final String ERROR_MESSAGE_INVALID_FILLING_FLORIST_NUMBER = "Invalid filling florist number";
  public static final String ERROR_MESSAGE_INVALID_FLORIST_CODE = "Invalid florist number";
  public static final String ERROR_MESSAGE_INVALID_MAIN_MEMBER_CODE = "Invalid main member code";
  public static final String ERROR_MESSAGE_MISSING_RECEIPIENT_NAME = "Missing recipient name";
  public static final String ERROR_MESSAGE_MISSING_ADDRESS = "Missing address";
  public static final String ERROR_MESSAGE_MISSING_CITY_STATE_ZIP = "Missing City/State/Zip";
  public static final String ERROR_MESSAGE_MISSING_PHONE_NUMBER = "Missing phone number";
  public static final String ERROR_MESSAGE_MISSING_FIRST_CHOICE = "Missing first choice";
  public static final String ERROR_MESSAGE_MISSING_SECOND_CHOICE = "Missing second choice";
  public static final String ERROR_MESSAGE_MISSING_OCCASSION = "Missing occasion";
  public static final String ERROR_MESSAGE_MISSING_PRIORITY = "Missing priority";
  public static final String REJECTED_CONFIRM_MESSAGE = "Unable to locate a \"confirmable\" transmission with a reference";

  // Added constants for logging
  public final String LOG_MESSAGE_PART = "The mercury message with mercury id ";
  public final String LOG_MESSAGE_PART1 = " has the following errors : ";

  public static final double VALID_PRICE_MIN = .01;
  public static final double VALID_PRICE_MAX = 99999.99;

  public static final String VIEW_QUEUE_NO = "N";

  // Product choice constants
  public static final String FIRST_CHOICE_CODE = "FIRST_CHOICE_CODE";
  public static final String SECOND_CHOICE_CODE = "SECOND_CHOICE_CODE";
  public static final String FIRST_CHOICE_DESC = "FIRST_CHOICE_DESC";
  public static final String SECOND_CHOICE_DESC = "SECOND_CHOICE_DESC";
  public static final String SERVICE = "FTD";
  public static final String EMPTY = "";

  // Default Customer Service Suffix (if null in MERCURY.MERCURY)
  public static final String DEFAULT_CUST_SERVE_SUFFIX = "DEFAULT_CUST_SERVE_SUFFIX";
  
  // MARS Lite Constants
  public static final String MARS_CONTEXT = "MARS_CONFIG";
  public static final String MARS_ENABLED = "IS_MARS_ENABLED";
  
}