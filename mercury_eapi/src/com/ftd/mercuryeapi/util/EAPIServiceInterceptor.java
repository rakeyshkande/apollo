package com.ftd.mercuryeapi.util;

import javax.xml.namespace.QName;

import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.binding.soap.interceptor.AbstractSoapInterceptor;
import org.apache.cxf.binding.soap.interceptor.SoapPreProtocolOutInterceptor;
import org.apache.cxf.headers.Header;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.phase.Phase;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;


public class EAPIServiceInterceptor extends AbstractSoapInterceptor{

private static final Logger logger = new Logger(EAPIServiceInterceptor.class.getName());
  
  private static final String XSD_WSSE = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
  private static final String XSD_WSSE_PASSWORD = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText";
  private String mainMemberCode;

  public EAPIServiceInterceptor(String mainMemberCode)
  {
    super(Phase.WRITE);
    this.mainMemberCode = mainMemberCode;
    addAfter(SoapPreProtocolOutInterceptor.class.getName());
  }

  public void handleMessage(SoapMessage message) throws Fault
  {
    try
    {

      Document doc = (Document) DOMUtil.getDocumentBuilder().newDocument();

      Element elementSecurity = doc.createElement("Security");
      elementSecurity.setAttribute("xmlns", XSD_WSSE);

      Element elementCredentials = doc.createElement("UsernameToken");
      Element elementUser = doc.createElement("Username");
      elementUser.setTextContent(MercuryEapiUtil.getEAPIUserName(mainMemberCode));
      Element elementPassword = doc.createElement("Password");
      elementPassword.setAttribute("Type", XSD_WSSE_PASSWORD);
      elementPassword.setTextContent(MercuryEapiUtil.getEAPIUserPassword(mainMemberCode));
      elementCredentials.appendChild(elementUser);
      elementCredentials.appendChild(elementPassword);

      elementSecurity.appendChild(elementCredentials);

      // Create Header object
      QName qnameCredentials = new QName("UsernameToken");
      Header header = new Header(qnameCredentials, elementSecurity);

      boolean hasSecurityHeader = false;
      for (Header hdr : message.getHeaders()) {
          if (hdr.getName().getLocalPart().equals("UsernameToken")) {
              hasSecurityHeader = true;
          }
      }
      
      if (!hasSecurityHeader) {
          message.getHeaders().add(header);
      }

    }
    catch (Exception e)
    {
    }
  }

}
