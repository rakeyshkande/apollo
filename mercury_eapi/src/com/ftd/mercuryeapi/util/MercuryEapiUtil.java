package com.ftd.mercuryeapi.util;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import javax.naming.InitialContext;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.ftd.eapi.message.service.v1.CodeMessagePair;
import com.ftd.eapi.message.service.v1.ErosMember;
import com.ftd.eapi.message.service.v1.ProcessedErosMessage;
import com.ftd.mercuryeapi.services.MercuryEapiException;
import com.ftd.mercuryeapi.viewobjects.ErosTimeVO;
import com.ftd.mercuryeapi.viewobjects.MercuryMessageVO;
import com.ftd.mercuryeapi.viewobjects.MercuryStageMessageVO;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.vo.MessageToken;

/**
 * This object contains common utilties used within project
 */
public class MercuryEapiUtil {

  private static Logger logger = new Logger(MercuryEapiUtil.class.getName());

  /**
   * This holds the name of the class to be used in logging and exception handling
   * messages
   */
  private static final String CLASS_NAME = "MercuryEapiUtil";

  public MercuryEapiUtil() {
  }

  /*
   * Get database connection
   */
  public static Connection getConnection() throws Exception {

    ConfigurationUtil config = ConfigurationUtil.getInstance();
    String dbConnection = config.getProperty(MercuryEapiConstants.MERCURYEAPI_PROPERTY_FILE,
        MercuryEapiConstants.DATASOURCE);

    // get DB connection
    return DataSourceUtil.getInstance().getConnection(dbConnection);
  }

  /**
   * Sends out a JMS message.
   * 
   * @param context
   *          Initial Context
   * @param messageToken
   *          JMS Message
   * @throws java.lang.Exception
   */
  public static void sendJMSMessage(InitialContext context, MessageToken messageToken, String queue)
      throws Exception {

    // make the correlation id the same as the message
    messageToken.setJMSCorrelationID((String) messageToken.getMessage());

    Dispatcher dispatcher = Dispatcher.getInstance();
    messageToken.setStatus(queue);
    dispatcher.dispatchTextMessage(context, messageToken);
  }

  /**
   * This procedure is used to convert java.util.Date to a timestamp.
   * 
   * @param java
   *          .util.Date
   * @returns java.sql.Date
   */
  public static java.sql.Timestamp getTimestamp(java.util.Date value) {
    java.sql.Timestamp sqlDate = null;
    if (value != null) {
      sqlDate = new java.sql.Timestamp(value.getTime());
    }
    return sqlDate;
  }

  /**
   * Checks if a field is empty (is empty string or null)
   * 
   * @param conn
   * @param message
   * @return
   * @throws java.lang.Exception
   */
  public static boolean isEmpty(String value) {
    return (value == null || value.length() == 0) ? true : false;
  }

  /**
   * Checks if a field is empty (is empty string or null)
   * 
   * @param conn
   * @param message
   * @return
   * @throws java.lang.Exception
   */
  public static boolean isEmpty(java.util.Date value) {
    return (value == null) ? true : false;
  }

  /**
   * Returns a transactional resource from the EJB container.
   * 
   * @param jndiName
   * @return
   * @throws javax.naming.NamingException
   */
  // public static Object lookupResource(String jndiName)
  // throws Exception
  // {
  // return InitialContextUtil.getInstance().lookupEnvironmentEntry(new
  // InitialContext(), jndiName, Object.class);
  // }

  /**
   * Truncate a string
   */
  public static String truncate(String value, int size) {
    if (value != null && value.length() > size) {
      value = value.substring(0, size);
    }

    return value;
  }

  /*
   * Checks the cache to determine if view queue flagging is turned on
   */
  public static boolean isViewQueueFlagOn() {

    GlobalParmHandler globalParamHandler = (GlobalParmHandler) CacheManager.getInstance()
        .getHandler("CACHE_NAME_GLOBAL_PARM");
    final String METHOD_NAME = ".isViewQueueFlagOn()";
    String viewQueueString = null;
    if (globalParamHandler != null) {
      viewQueueString = globalParamHandler.getFrpGlobalParm(MercuryEapiConstants.LOAD_MEMBER_DATA,
          MercuryEapiConstants.VIEW_QUEUE_SWITCH);
    }
    else {
      logger.error(CLASS_NAME + METHOD_NAME + "Unable to get the view queue ");
    }

    return viewQueueString == null || viewQueueString.equals("Y") ? true : false;

  }

  /**
   * This method is used for Date conversion
   * 
   * @param dateString
   * @return
   */
  public static XMLGregorianCalendar getXMLGregorianDate(Date date) {

    GregorianCalendar gc = new GregorianCalendar();
    gc.setTime(date);
    XMLGregorianCalendar gcDate = null;
    try {
      gcDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
    }
    catch (DatatypeConfigurationException e) {

      e.printStackTrace();
    }

    return gcDate;
  }

  /**
   * This method is used to convert the double to the big decimal
   * 
   * @param double1
   * @return
   */
  public static BigDecimal getBigDec(Double double1) {
    BigDecimal ret = null;

    if (double1 != null) {
      ret = new BigDecimal(double1.toString());
    }

    return ret;
  }

  /*
   * Converts XMLGregorianCalendar to java.util.Date in Java
   *    without the time component
   */
  public static Date toDate(XMLGregorianCalendar calendar) {
    if (calendar == null) {
      return null;
    }
    Date returnDate = calendar.toGregorianCalendar().getTime();
    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
    try {
        returnDate = sdf.parse(sdf.format(returnDate));
    } catch (Exception e) {
    	logger.error("Could not parse date" + e);
    }
    
    return returnDate;
  }

  /*
   * Converts XMLGregorianCalendar to java.util.Date in Java
   *     with the time component
   */
  public static Date toDateTime(XMLGregorianCalendar calendar) {
    if (calendar == null) {
      return null;
    }
    return calendar.toGregorianCalendar().getTime();
  }

  /**
   * Convert a database boolean to a java boolean
   */
  public static boolean getBoolean(String dbBool) {
    return (dbBool == null || dbBool.equalsIgnoreCase("N") ? false : true);
  }

  /**
   * Convert a java boolean to a DB boolean
   */
  public static String getBoolean(boolean bool) {
    return (bool ? "Y" : "N");
  }

  /**
   * This method is used to converts the price string into double
   * 
   * @param obj
   * @return
   */
  public static double getAmount(Object obj) {
    double value = 0.0;
    if (obj != null) {
      value = Double.parseDouble(obj.toString());
    }

    return value;
  }

  /**
   * This method is used to convert the String to Double
   * 
   * @param obj
   * @return
   */
  public static Double getDoubleAmount(Object obj) {
    Double value = null;
    if (obj != null) {
      value = new Double(obj.toString());
    }

    return value;
  }

  /**
   * This method is used to convert the doulbe to string
   * 
   * @param value
   * @return
   */
  public static String doubleToString(Double value) {
    String ret = null;

    if (value != null) {
      ret = value.toString();
    }

    return ret;
  }

  /**
   * This method is used to convert the object into primitive integer
   * 
   * @param obj
   * @return
   */
  public static int getInt(Object obj) {
    int value = 0;
    if (obj != null) {
      value = Integer.parseInt(obj.toString());
    }

    return value;
  }

  /**
   * Description: Takes in a string Date, checks for valid formatting nd converts it to a
   * Util date of yyyy-mm-dd.
   * 
   * @param String
   *          data string
   * @return java.sql.Date (returns null if invalid date)
   * @throws ParseException
   */
  public static java.util.Date formatStringToUtilDate(String strDate) throws ParseException {

    String inDateFormat = "";
    int dateLength = 0;
    int firstSep = 0;
    int lastSep = 0;

    java.util.Date returnDate = null;

    if ((strDate != null) && (!strDate.trim().equals(""))) {

      // set input date format
      dateLength = strDate.length();
      int firstTimeSep = strDate.indexOf(":");
      int firstDashSep = strDate.indexOf("-");
      if (dateLength > 20) {
        inDateFormat = "yyyy-MM-dd HH:mm:ss.S";
      }
      else if (firstTimeSep > 0) {
        inDateFormat = "yyyy-MM-dd hh:mm:ss";
      }
      else if (firstDashSep > 0) {
        int lastDashSep = strDate.lastIndexOf("-");
        // check how many in years
        if (strDate.length() - lastDashSep == 4) {// 4 digit year
          if (lastDashSep - firstDashSep == 3) {
            // abbreviated month
            inDateFormat = "dd-MMM-yyyy";
          }
          else {
            // non-abbreviated month
            inDateFormat = "dd-MMMMMM-yyyy";
          }
        }
        else {// 2 digit year
          if (lastDashSep - firstDashSep == 3) {
            // abbreviated month
            inDateFormat = "dd-MMM-yy";
          }
          else {
            // non-abbreviated month
            inDateFormat = "dd-MMMMMM-yy";
          }
        }
      }
      else {
        firstSep = strDate.indexOf("/");
        lastSep = strDate.lastIndexOf("/");

        switch (dateLength) {
          case 10:
            inDateFormat = "MM/dd/yyyy";
            break;
          case 9:
            if (firstSep == 1) {
              inDateFormat = "M/dd/yyyy";
            }
            else {
              inDateFormat = "MM/d/yyyy";
            }
            break;
          case 8:
            if (firstSep == 1) {
              inDateFormat = "M/d/yyyy";
            }
            else {
              inDateFormat = "MM/dd/yy";
            }
            break;
          case 7:
            if (firstSep == 1) {
              inDateFormat = "M/dd/yy";
            }
            else {
              inDateFormat = "MM/d/yy";
            }
            break;
          case 6:
            inDateFormat = "M/d/yy";
            break;
          default:
            break;
        }
      }
      SimpleDateFormat sdfInput = new SimpleDateFormat(inDateFormat);

      returnDate = sdfInput.parse(strDate);
    }

    return returnDate;
  }

  /**
   * Truncate a value. If the value is over the max allowed size a system message is sent
   * out.
   */
  public static String truncateField(String mercuryMessageID, String fieldName, String value,
      int maxLength) throws Exception {
    if (value != null && value.length() > maxLength) {

      String message = "Mercury Message field excedes database max.  Value truncated.  Mercury Message="
          + mercuryMessageID
          + ", Field="
          + fieldName
          + ", Size="
          + value.length()
          + ", Max size allowed=" + maxLength;

      value = value.substring(0, maxLength);
    }

    return value;
  }

  /**
   * This new method will split a string that could be up to 9,999 characters into three
   * strings of 4,000 characters max for database storage
   * 
   * @param String
   *          to split (9,999 characters max)
   * @return String array of 4,000 characters max each
   * @throws Exception
   */
  public static String[] splitString(String inString) throws Exception {
    String[] retString = new String[3];
    int maxLength = 4000;
    int inLength = inString.length();

    if (inLength <= maxLength) {
      retString[0] = inString.substring(0, inLength);
    }
    else {
      retString[0] = inString.substring(0, maxLength);
      if (inLength <= maxLength * 2) {
        retString[1] = inString.substring(maxLength, inLength);
      }
      else {
        retString[1] = inString.substring(maxLength, maxLength * 2);
        retString[2] = inString.substring(maxLength * 2, inLength);
      }
    }

    return retString;
  }

  /**
   * Truncate fields before inserting into DB. This is to insure the insert will be ok.
   */
  public static MercuryMessageVO truncateMercuryMessageVOFields(MercuryMessageVO vo)
      throws Exception {
    vo.setMercuryID(truncateField(vo.getMercuryID(), "IN_MERCURY_ID", vo.getMercuryID(), 20));
    vo.setMercuryStatus(truncateField(vo.getMercuryID(), "IN_MERCURY_STATUS",
        vo.getMercuryStatus(), 2));
    vo.setMessageType(truncateField(vo.getMercuryID(), "IN_MSG_TYPE", vo.getMessageType(), 3));
    vo.setOutboundID(truncateField(vo.getMercuryID(), "IN_OUTBOUND_ID", vo.getOutboundID(), 2));
    vo.setSendingFlorist(truncateField(vo.getMercuryID(), "IN_SENDING_FLORIST",
        vo.getSendingFlorist(), 2000));
    vo.setFillingFlorist(truncateField(vo.getMercuryID(), "IN_FILLING_FLORIST",
        vo.getFillingFlorist(), 2000));
    vo.setRecipient(truncateField(vo.getMercuryID(), "IN_RECIPIENT", vo.getRecipient(), 2000));
    vo.setAddress(truncateField(vo.getMercuryID(), "IN_ADDRESS", vo.getAddress(), 2000));
    vo.setCityStateZip(truncateField(vo.getMercuryID(), "IN_CITY_STATE_ZIP", vo.getCityStateZip(),
        2000));
    vo.setPhoneNumber(truncateField(vo.getMercuryID(), "IN_PHONE_NUMBER", vo.getPhoneNumber(), 63));
    vo.setDeliveryDateText(truncateField(vo.getMercuryID(), "IN_DELIVERY_DATE_TEXT",
        vo.getDeliveryDateText(), 63));
    vo.setFirstChoice(truncateField(vo.getMercuryID(), "IN_FIRST_CHOICE", vo.getFirstChoice(), 2000));
    vo.setSecondChoice(truncateField(vo.getMercuryID(), "IN_SECOND_CHOICE", vo.getSecondChoice(),
        2000));
    vo.setCardMessage(truncateField(vo.getMercuryID(), "IN_CARD_MESSAGE", vo.getCardMessage(), 4000));
    vo.setOccasion(truncateField(vo.getMercuryID(), "IN_OCCASION", vo.getOccasion(), 63));
    vo.setSpecialInstructions(truncateField(vo.getMercuryID(), "IN_SPECIAL_INSTRUCTIONS",
        vo.getSpecialInstructions(), 4000));
    vo.setPriority(truncateField(vo.getMercuryID(), "IN_PRIORITY", vo.getPriority(), 10));
    vo.setOperator(truncateField(vo.getMercuryID(), "IN_OPERATOR", vo.getOperator(), 2000));
    vo.setComments(truncateField(vo.getMercuryID(), "IN_COMMENTS", vo.getComments(), 4000));
    vo.setSakText(truncateField(vo.getMercuryID(), "IN_SAK_TEXT", vo.getSakText(), 600));
    vo.setReferenceNumber(truncateField(vo.getMercuryID(), "IN_REFERENCE_NUMBER",
        vo.getReferenceNumber(), 100));
    vo.setProductID(truncateField(vo.getMercuryID(), "IN_PRODUCT_ID", vo.getProductID(), 10));
    vo.setZipCode(truncateField(vo.getMercuryID(), "IN_ZIP_CODE", vo.getZipCode(), 10));
    vo.setAskAnswerCode(truncateField(vo.getMercuryID(), "IN_ASK_ANSWER_CODE",
        vo.getAskAnswerCode(), 1));
    vo.setSortValue(truncateField(vo.getMercuryID(), "IN_SORT_VALUE", vo.getSortValue(), 100));
    vo.setRetrievalFlag(truncateField(vo.getMercuryID(), "IN_RETRIEVAL_FLAG",
        vo.getRetrievalFlag(), 1));
    vo.setCombinedReportNumber(truncateField(vo.getMercuryID(), "IN_COMBINED_REPORT_NUMBER",
        vo.getCombinedReportNumber(), 63));
    vo.setRofNumber(truncateField(vo.getMercuryID(), "IN_ROF_NUMBER", vo.getRofNumber(), 63));
    vo.setADJReasonCode(truncateField(vo.getMercuryID(), "IN_ADJ_REASON_CODE",
        vo.getADJReasonCode(), 63));
    vo.setRetrievalFromMessage(truncateField(vo.getMercuryID(), "IN_FROM_MESSAGE_NUMBER",
        vo.getRetrievalFromMessage(), 63));
    vo.setRetrievalToMessage(truncateField(vo.getMercuryID(), "IN_TO_MESSAGE_NUMBER",
        vo.getRetrievalToMessage(), 63));

    return vo;
  }

  /**
   * Truncate fields before inserting into DB. This is to insure the insert will be ok.
   */
  public static MercuryStageMessageVO truncateMercuryStageMessageFields(
      MercuryStageMessageVO mercuryStageMessageVO) throws Exception {

	  mercuryStageMessageVO
        .setReferenceNumber(truncateField(mercuryStageMessageVO.getMessageId().toString(),
            "IN_REFERENCE_NUMBER", mercuryStageMessageVO.getReferenceNumber().toString(), 10));
	  mercuryStageMessageVO.setMessageType(truncateField(mercuryStageMessageVO.getMessageId().toString(),
        "IN_MESSAGE_TYPE", mercuryStageMessageVO.getMessageType().toString(), 3));
	  mercuryStageMessageVO.setMainMemberCode(truncateField(mercuryStageMessageVO.getMessageId()
        .toString(), "IN_MAIN_MEMBER_CODE", mercuryStageMessageVO.getMainMemberCode(), 9));
	  mercuryStageMessageVO.setCreatedBy(truncateField(mercuryStageMessageVO.getMessageId().toString(),
        "IN_CREATED_BY", mercuryStageMessageVO.getCreatedBy().toString(), 100));
	  mercuryStageMessageVO.setUpdatedBy(truncateField(mercuryStageMessageVO.getMessageId().toString(),
        "IN_UPDATED_BY", mercuryStageMessageVO.getUpdatedBy().toString(), 100));

    return mercuryStageMessageVO;
  }

  public static MercuryStageMessageVO truncateMercuryStageMessageFieldsForUpdate(
      MercuryStageMessageVO mercuryStageMessageVO) throws Exception {

    if (mercuryStageMessageVO.getReason() != null && mercuryStageMessageVO.getReason() != "") {
    	mercuryStageMessageVO.setReason(truncateField(mercuryStageMessageVO.getReason().toString(),
          "IN_REASON", mercuryStageMessageVO.getReason(), 1000));
    }
    return mercuryStageMessageVO;
  }

  /**
   * This method is used to get the EAPI username for mainMemberCode from global
   * parameters
   * 
   * @return
   * @throws Exception
   */
  public static String getEAPIUserName(String mainMemberCode) throws MercuryEapiException {

    final String METHOD_NAME = ".getEAPIUserName()";

    String userName = null;

    ConfigurationUtil configUtil;
    try {
      configUtil = ConfigurationUtil.getInstance();
      userName = configUtil.getSecureProperty(MercuryEapiConstants.MERCURY_SECURE_CONFIG_CONTEXT,
          mainMemberCode + MercuryEapiConstants.USER_NAME);
    }
    catch (IOException e) {
      logger.error(CLASS_NAME + METHOD_NAME + " IO Exception occured");
      throw new MercuryEapiException(e);
    }
    catch (SAXException e) {
      logger.error(CLASS_NAME + METHOD_NAME + " SAX Exception occured");
      throw new MercuryEapiException(e);
    }
    catch (ParserConfigurationException e) {
      logger.error(CLASS_NAME + METHOD_NAME + " ParserConfiguration Exception occured");
      throw new MercuryEapiException(e);
    }
    catch (Exception e) {
      logger.error(CLASS_NAME + METHOD_NAME + " Exception occured");
      throw new MercuryEapiException(e);
    }

    if (userName == null || userName.length() == 0 || userName.isEmpty()) {
      throw new MercuryEapiException(CLASS_NAME + METHOD_NAME + " :: User Name is null.");
    }

    return userName;

  }

  /**
   * This method is used to get the EAPI password for mainMemberCode from global parameters
   * 
   * @return
   * @throws Exception
   */
  public static String getEAPIUserPassword(String mainMemberCode) throws MercuryEapiException {

    final String METHOD_NAME = ".getEAPIUserPassword()";

    String eapiUserPassword = null;

    ConfigurationUtil configUtil;
    try {
      configUtil = ConfigurationUtil.getInstance();
      eapiUserPassword = configUtil.getSecureProperty(
          MercuryEapiConstants.MERCURY_SECURE_CONFIG_CONTEXT, mainMemberCode
              + MercuryEapiConstants.PASSWORD);
    }
    catch (IOException e) {
      logger.error(CLASS_NAME + METHOD_NAME + " IO Exception occured");
      throw new MercuryEapiException(e);
    }
    catch (SAXException e) {
      logger.error(CLASS_NAME + METHOD_NAME + " SAX Exception occured");
      throw new MercuryEapiException(e);
    }
    catch (ParserConfigurationException e) {
      logger.error(CLASS_NAME + METHOD_NAME + " ParserConfiguration Exception occured");
      throw new MercuryEapiException(e);
    }
    catch (Exception e) {
      logger.error(CLASS_NAME + METHOD_NAME + " Exception occured");
      throw new MercuryEapiException(e);
    }

    if (eapiUserPassword == null || eapiUserPassword.length() == 0 || eapiUserPassword.isEmpty()) {
      throw new MercuryEapiException(CLASS_NAME + METHOD_NAME + " :: User Name is null.");
    }

    return eapiUserPassword;

  }

  /**
   * This method is used to get the IS_EAPI_ON Flag from global parameters
   * 
   * @return
   * @throws Exception
   */
  public static Boolean isEAPIEnabled() throws MercuryEapiException {

    final String METHOD_NAME = ".isEAPIEnabled()";

    Boolean isEAPIEnabled = false;
    String eapiSwitch = null;

    ConfigurationUtil configUtil;
    try {
      configUtil = ConfigurationUtil.getInstance();
      eapiSwitch = configUtil.getFrpGlobalParm(MercuryEapiConstants.MERCURY_CONFIG_CONTEXT,
          MercuryEapiConstants.IS_EAPI_ON);
    }
    catch (IOException e) {
      throw new MercuryEapiException(e);
    }
    catch (SAXException e) {
      throw new MercuryEapiException(e);
    }
    catch (ParserConfigurationException e) {
      throw new MercuryEapiException(e);
    }
    catch (Exception e) {
      throw new MercuryEapiException(e);
    }

    if (eapiSwitch == null || eapiSwitch.length() == 0 || eapiSwitch.isEmpty()) {
      throw new MercuryEapiException(CLASS_NAME + METHOD_NAME + " :: Eapi Switch is null.");
    }

    if (eapiSwitch != null && MercuryEapiConstants.Y_STRING.equalsIgnoreCase(eapiSwitch)) {
      isEAPIEnabled = true;
    }

    return isEAPIEnabled;

  }

  /**
   * This method is created to reduce the code redundancy
   * 
   * @param message
   */
  public static void sendSystemMessageWrapper(String message) {

    final String METHOD_NAME = ".sendSystemMessageWrapper()";

    logger.error(message);
    Connection conn = null;
    
    try {
      //sendSystemMessage(message);
      ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
      String messageSource = configUtil.getProperty(MercuryEapiConstants.MERCURYEAPI_PROPERTY_FILE,
          MercuryEapiConstants.MESSAGE_SOURCE);

      String messageID = "";

      // build system vo
      SystemMessengerVO sysMessage = new SystemMessengerVO();
      sysMessage.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
      sysMessage.setSource(messageSource);
      sysMessage.setType(MercuryEapiConstants.ERROR);
      sysMessage.setMessage(message);

      SystemMessenger sysMessenger = SystemMessenger.getInstance();
      conn = getConnection();
      
      messageID = sysMessenger.send(sysMessage, conn);

      if (messageID == null) {
        String msg = "Error occured while attempting to send out a system message. Msg not sent: "
            + message;
        logger.error(msg);
      }
    } catch (Exception e) {
      logger.error(CLASS_NAME + METHOD_NAME + "Could not send system message:" + e);
    } finally {
        // close the connection
        try {
            conn.close();
        } catch (Exception e) {
        	logger.error(e);
        }
    }
    
  }

  /**
   * This method is used to get the EAPI's WS Url from global parameters
   * 
   * @return
   * @throws Exception
   */
  public static String getEapiWebServiceUrl() throws MercuryEapiException {

    final String METHOD_NAME = ".getEapiWebServiceUrl()";

    String eapiWsdlUrl = null;

    ConfigurationUtil configUtil;
    try {

      configUtil = ConfigurationUtil.getInstance();

      eapiWsdlUrl = configUtil.getFrpGlobalParm(MercuryEapiConstants.MERCURY_CONFIG_CONTEXT,
          MercuryEapiConstants.EAPI_WS_URL);
      logger.debug("****Eapi Wsdl Url From Global Param : " + eapiWsdlUrl);

    }
    catch (IOException e) {
      throw new MercuryEapiException(e);
    }
    catch (SAXException e) {
      throw new MercuryEapiException(e);
    }
    catch (ParserConfigurationException e) {
      throw new MercuryEapiException(e);
    }
    catch (Exception e) {
      throw new MercuryEapiException(e);
    }

    if (eapiWsdlUrl == null || eapiWsdlUrl.length() == 0 || eapiWsdlUrl.isEmpty()) {
      throw new MercuryEapiException(CLASS_NAME + METHOD_NAME + " :: EAPI Web Service URL is null.");
    }

    return eapiWsdlUrl;

  }
  
    /**
   * This method is used to get the EAPI's wsdl name from global parameters
   * 
   * @return
   * @throws Exception
   */
  public static String getEapiWebServiceWsdl() throws MercuryEapiException {

    final String METHOD_NAME = ".getEapiWebServiceWsdl()";

    String eapiWsdlName = null;

    ConfigurationUtil configUtil;
    try {

      configUtil = ConfigurationUtil.getInstance();

      eapiWsdlName = configUtil.getFrpGlobalParm(MercuryEapiConstants.MERCURY_CONFIG_CONTEXT,
          MercuryEapiConstants.EAPI_WS_WSDL);
      logger.debug("****Eapi Wsdl name From Global Param : " + eapiWsdlName);

    }
    catch (IOException e) {
      throw new MercuryEapiException(e);
    }
    catch (SAXException e) {
      throw new MercuryEapiException(e);
    }
    catch (ParserConfigurationException e) {
      throw new MercuryEapiException(e);
    }
    catch (Exception e) {
      throw new MercuryEapiException(e);
    }

    if (eapiWsdlName == null || eapiWsdlName.length() == 0 || eapiWsdlName.isEmpty()) {
      throw new MercuryEapiException(CLASS_NAME + METHOD_NAME + " :: EAPI Web Service WSDL Name is null.");
    }

    return eapiWsdlName;

  }

  
  /**
   * This method is used to get the CXF timeout from global parameters
   * 
   * @return
   * @throws Exception
   */
  public static String getCxfClientTimeout() throws MercuryEapiException {

    String timeout = null;

    ConfigurationUtil configUtil;
    try {

      configUtil = ConfigurationUtil.getInstance();

      timeout = configUtil.getFrpGlobalParm(MercuryEapiConstants.MERCURY_CONFIG_CONTEXT,
          MercuryEapiConstants.CXF_CLIENT_TIMEOUT);
      logger.debug("****CXF client timeout From Global Param : " + timeout);

    }
    catch (IOException e) {
      throw new MercuryEapiException(e);
    }
    catch (SAXException e) {
      throw new MercuryEapiException(e);
    }
    catch (ParserConfigurationException e) {
      throw new MercuryEapiException(e);
    }
    catch (Exception e) {
      throw new MercuryEapiException(e);
    }

    if (timeout == null || timeout.length() == 0 || timeout.isEmpty()) {
      return null;
    }

    return timeout;

  }
  /**
   * Splits the product choice code and product choice description from the product choice
   * variable.
   * 
   * @param key
   * @param productChoice
   * @return
   */
  public static void splitProductChoice(MercuryMessageVO mercuryMessageVO) {

    String[] product = null;

    if (mercuryMessageVO.getFirstChoice() != null) {
      product = mercuryMessageVO.getFirstChoice().split(" ", 2);
      if (product.length == 2) {
        mercuryMessageVO.setFirstChoiceDesc(product[1]);
      }
      mercuryMessageVO.setFirstChoiceCode(product[0]);

    }
    if (mercuryMessageVO.getSecondChoice() != null) {
      product = mercuryMessageVO.getSecondChoice().split(" ", 2);
      if (product.length == 2) {
        mercuryMessageVO.setSecondChoiceDesc(product[1]);
      }
      mercuryMessageVO.setSecondChoiceCode(product[0]);
    }
  }

  /**
   * Splits the recipient's city, state and zipcode from citystatezip variable.
   * 
   * @param cityStateZip
   * @param productChoice
   * @return
   */
  public static void splitCityStateZip(String cityStateZip, MercuryMessageVO vo) {
    int pos;
    String delimiter = ",";
    if (vo != null) {
      if (vo.getCityStateZip() != null) {
        pos = vo.getCityStateZip().lastIndexOf(delimiter);
        if (pos <= 0) {
        	vo.setRecipientCity(vo.getCityStateZip());
        	vo.setRecipientState("NA");
        } else {
            vo.setRecipientCity(cityStateZip.substring(0, pos));
            vo.setRecipientState(cityStateZip.substring(pos + 1, pos + 4).trim());
        }
    	if (vo.getRecipientCity().length() > 256) {
    		vo.setRecipientCity(vo.getRecipientCity().substring(0, 256));
    	}
    	if (vo.getRecipientState().length() > 2) {
    		vo.setRecipientState(vo.getRecipientState().substring(0, 2));
    	}
      }
    }

  }

  /**
   * An utility method which massages the message data.
   * 
   * @param message
   * @return MercuryMessageVO
   */
  public static MercuryMessageVO massageData(MercuryMessageVO message) {

    if (message.getService() == null) {
      message.setService(MercuryEapiConstants.SERVICE);
    }

    splitProductChoice(message);

    // trim the price ask-answer code
    if (message.getAskAnswerCode() != null) {
      message.setAskAnswerCode(message.getAskAnswerCode().trim());
    }

    if (message.getMercuryOrderNumber() != null) {
      String[] arr = message.getMercuryOrderNumber().split("-");
      message.setOrderReference(arr[0]);
      message.setOrderSequence(arr[1]);
    }

    // change BIRTHDAY_OCCASION to BIRTHDAY
    if (message.getOccasion() != null
        && message.getOccasion().contains(MercuryEapiConstants.OCCASION)) {
      message.setOccasion(message.getOccasion().replace(MercuryEapiConstants.OCCASION, "").trim());
      message.setOccasion(message.getOccasion().replace(" ", "_"));
    }

    // if second choice is null or empty spaces on an FTD message change it
    // to NONE
    if (message.getMessageType() != null
        && message.getMessageType().equals(MercuryEapiConstants.FTD_MESSAGE)) {
      if (message.getSecondChoice() == null || message.getSecondChoice().trim().length() == 0) {
        message.setSecondChoice(MercuryEapiConstants.EMPTY);
      }
    }// end type

    splitCityStateZip(message.getCityStateZip(), message);

    return message;
  }

  /**
   * An utility method which converts the null values present in the mercuryMessageVO to
   * empty value.
   * 
   * @param vo
   * @return MercuryMessageVO
   */
  public static MercuryMessageVO changeNullValues(MercuryMessageVO vo) {
    if (vo.getAddress() == null) {
      vo.setAddress(MercuryEapiConstants.EMPTY);
    }
    if (vo.getADJReasonCode() == null) {
      vo.setADJReasonCode(MercuryEapiConstants.EMPTY);
    }
    if (vo.getAskAnswerCode() == null) {
      vo.setAskAnswerCode(MercuryEapiConstants.EMPTY);
    }
    if (vo.getCardMessage() == null) {
      vo.setCardMessage(MercuryEapiConstants.EMPTY);
    }
    if (vo.getCityStateZip() == null) {
      vo.setCityStateZip(MercuryEapiConstants.EMPTY);
    }
    if (vo.getComments() == null) {
      vo.setComments(MercuryEapiConstants.EMPTY);
    }
    if (vo.getConventionMessage() == null) {
      vo.setConventionMessage(MercuryEapiConstants.EMPTY);
    }
    if (vo.getCorrelationReference() == null) {
      vo.setCorrelationReference(MercuryEapiConstants.EMPTY);
    }
    if (vo.getDeliveryDateText() == null) {
      vo.setDeliveryDateText(MercuryEapiConstants.EMPTY);
    }
    if (vo.getFillingFlorist() == null) {
      vo.setFillingFlorist(MercuryEapiConstants.EMPTY);
    }
    if (vo.getFirstChoice() == null) {
      vo.setFirstChoice(MercuryEapiConstants.EMPTY);
    }
    if (vo.getFirstChoiceCode() == null) {
      vo.setFirstChoiceCode(MercuryEapiConstants.EMPTY);
    }
    if (vo.getFirstChoiceDesc() == null) {
      vo.setFirstChoiceDesc(MercuryEapiConstants.EMPTY);
    }
    if (vo.getMercuryID() == null) {
      vo.setMercuryID(MercuryEapiConstants.EMPTY);
    }
    if (vo.getMercuryMessageNumber() == null) {
      vo.setMercuryMessageNumber(MercuryEapiConstants.EMPTY);
    }
    if (vo.getMercuryOrderNumber() == null) {
      vo.setMercuryOrderNumber(MercuryEapiConstants.EMPTY);
    }
    if (vo.getMercuryStatus() == null) {
      vo.setMercuryStatus(MercuryEapiConstants.EMPTY);
    }
    if (vo.getMessageType() == null) {
      vo.setMessageType(MercuryEapiConstants.EMPTY);
    }
    if (vo.getOccasion() == null) {
      vo.setOccasion(MercuryEapiConstants.EMPTY);
    }
    if (vo.getOperator() == null) {
      vo.setOperator(MercuryEapiConstants.EMPTY);
    }
    if (vo.getOrderReference() == null) {
      vo.setOrderReference(MercuryEapiConstants.EMPTY);
    }
    if (vo.getOrderSequence() == null) {
      vo.setOrderSequence(MercuryEapiConstants.EMPTY);
    }
    if (vo.getOutboundID() == null) {
      vo.setOutboundID(MercuryEapiConstants.EMPTY);
    }
    if (vo.getPhoneNumber() == null) {
      vo.setPhoneNumber(MercuryEapiConstants.EMPTY);
    }
    if (vo.getPriority() == null) {
      vo.setPriority(MercuryEapiConstants.EMPTY);
    }
    if (vo.getProductID() == null) {
      vo.setProductID(MercuryEapiConstants.EMPTY);
    }
    if (vo.getRecipient() == null) {
      vo.setRecipient(MercuryEapiConstants.EMPTY);
    }
    // if (vo.getRecipientCity() == null) {
    // vo.setRecipientCity("");
    // }
    // if (vo.getRecipientState() == null) {
    // vo.setRecipientState("");
    // }
    if (vo.getRecipientCountryCode() == null) {
      vo.setRecipientCountryCode(MercuryEapiConstants.EMPTY);
    }
    if (vo.getReferenceNumber() == null) {
      vo.setReferenceNumber(MercuryEapiConstants.EMPTY);
    }
    if (vo.getRofNumber() == null) {
      vo.setRofNumber(MercuryEapiConstants.EMPTY);
    }
    if (vo.getSecondChoice() == null) {
      vo.setSecondChoice(MercuryEapiConstants.EMPTY);
    }
    if (vo.getSecondChoiceCode() == null) {
      vo.setSecondChoiceCode(MercuryEapiConstants.EMPTY);
    }
    if (vo.getSecondChoiceDesc() == null) {
      vo.setSecondChoiceDesc(MercuryEapiConstants.EMPTY);
    }
    if (vo.getSendingFlorist() == null) {
      vo.setSendingFlorist(MercuryEapiConstants.EMPTY);
    }
    if (vo.getService() == null) {
      vo.setService(MercuryEapiConstants.SERVICE);
    }
    if (vo.getSpecialInstructions() == null) {
      vo.setSpecialInstructions(MercuryEapiConstants.EMPTY);
    }
    if (vo.getTagLine() == null) {
      vo.setTagLine(MercuryEapiConstants.EMPTY);
    }
    if (vo.getRecipientCity() == null) {
      vo.setRecipientCity(MercuryEapiConstants.EMPTY);
    }
    if (vo.getRecipientState() == null) {
      vo.setRecipientState(MercuryEapiConstants.EMPTY);
    }
    if (vo.getZipCode() == null) {
      vo.setZipCode(MercuryEapiConstants.EMPTY);
    }
    return vo;
  }

  /*
   * This method checks if the current time is within the EROS downtime.
   */
  public static ErosTimeVO isEROSDown() throws Exception {
    boolean erosDown = false;

    ErosTimeVO erosTimeVO = getEROSTimes();

    // get current time
    Calendar currentTime = Calendar.getInstance();

    // if the eros outage time goes past midnight (starts one night, ends the next
    // morning)
    if (erosTimeVO.getErosDownStartTime().after(erosTimeVO.getErosDownEndTime())) {
      // eros outtage goes past midnight

      // check if we are in the eros down time that started last night
      if (currentTime.before(erosTimeVO.getErosDownEndTime())) {
        erosDown = true;
      }

      // check if we are in the eros down time that starts tonight
      if (currentTime.after(erosTimeVO.getErosDownStartTime())) {
        erosDown = true;
      }

    }
    else {
      // eros outage starts and ends on same day

      // check if we are during outage
      if (currentTime.after(erosTimeVO.getErosDownStartTime())
          && currentTime.before(erosTimeVO.getErosDownEndTime())) {
        erosDown = true;
      }
    }
    erosTimeVO.setErosDown(erosDown);

    return erosTimeVO;
  }

  /*
   * This method gets the eros times from the config file
   */
  private static ErosTimeVO getEROSTimes() throws Exception {
    ErosTimeVO erosTimeVO = new ErosTimeVO();

    // Get efos times from config file
    ConfigurationUtil configUtil = ConfigurationUtil.getInstance();

    // get current day
    Calendar today = Calendar.getInstance();

    erosTimeVO.setErosAlertTime(getTimeFromString(today, configUtil.getFrpGlobalParm(
        MercuryEapiConstants.MERCURY_CONFIG_CONTEXT, "EFOS_ALERT_TIME")));

    // EFOS has different down times on Sunday
    if (today.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
        erosTimeVO.setErosDownStartTime(getTimeFromString(today, configUtil.getFrpGlobalParm(
            MercuryEapiConstants.MERCURY_CONFIG_CONTEXT, "EFOS_SUNDAY_DOWN_START_TIME")));
    } else {
        erosTimeVO.setErosDownStartTime(getTimeFromString(today, configUtil.getFrpGlobalParm(
            MercuryEapiConstants.MERCURY_CONFIG_CONTEXT, "EFOS_DOWN_START_TIME")));
    }
    
    // If we are after today's cutoff, get end time for tomorrow
    if (today.after(erosTimeVO.getErosDownStartTime())) {
    	today.add(Calendar.DATE, 1);
    }

    // EROS has different end time on Sunday and Monday
    String specialDays = configUtil.getFrpGlobalParm(MercuryEapiConstants.MERCURY_CONFIG_CONTEXT,
    		"EFOS_SPECIAL_END_TIME_DAYS").toUpperCase();
    String todayString = today.getDisplayName( Calendar.DAY_OF_WEEK ,Calendar.LONG, Locale.getDefault()).toUpperCase();
    if (specialDays.indexOf(todayString) >= 0) {
        erosTimeVO.setErosDownEndTime(getTimeFromString(today,configUtil.getFrpGlobalParm(
            MercuryEapiConstants.MERCURY_CONFIG_CONTEXT, "EFOS_SPECIAL_DOWN_END_TIME")));
    } else {
        erosTimeVO.setErosDownEndTime(getTimeFromString(today, configUtil.getFrpGlobalParm(
            MercuryEapiConstants.MERCURY_CONFIG_CONTEXT, "EFOS_DOWN_END_TIME")));
    }
    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
    logger.info("start: " + sdf.format(erosTimeVO.getErosDownStartTime().getTime()) +
    		" end: " + sdf.format(erosTimeVO.getErosDownEndTime().getTime()));

    return erosTimeVO;
  }

  /*
   * Returns a calender object for the given time string.
   * @param String Time in the format of HH:MM:SS (24 hour clock)
   * @return Calender *
   */
  private static Calendar getTimeFromString(Calendar cal, String timeString) throws Exception {

    // check for nulls
    if (timeString == null) {
      throw new Exception("Time string in property file is null.");
    }

    Calendar theTime = (Calendar) cal.clone();
    try {

      String hours = timeString.substring(0, 2);
      String minutes = timeString.substring(3, 5);

      theTime.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hours));
      theTime.set(Calendar.MINUTE, Integer.parseInt(minutes));
      theTime.set(Calendar.SECOND, 0);
    }
    catch (Throwable t) {
      throw new Exception("Time in config file is invalid.");
    }
    return theTime;

  }

  public static Integer getPollMessageLimit() throws MercuryEapiException {

    final String METHOD_NAME = ".getPollMessageLimit()";

    String recordLimit = null;

    ConfigurationUtil configUtil;
    try {
      configUtil = ConfigurationUtil.getInstance();
      recordLimit = configUtil.getFrpGlobalParm(MercuryEapiConstants.MERCURY_CONFIG_CONTEXT,
          MercuryEapiConstants.POLL_MESSAGE_LIMIT);
    }
    catch (IOException e) {
      logger.error(CLASS_NAME + METHOD_NAME + " IO Exception occured");
      throw new MercuryEapiException(e);
    }
    catch (SAXException e) {
      logger.error(CLASS_NAME + METHOD_NAME + " SAX Exception occured");
      throw new MercuryEapiException(e);
    }
    catch (ParserConfigurationException e) {
      logger.error(CLASS_NAME + METHOD_NAME + " ParserConfiguration Exception occured");
      throw new MercuryEapiException(e);
    }
    catch (Exception e) {
      logger.error(CLASS_NAME + METHOD_NAME + " Exception occured");
      throw new MercuryEapiException(e);
    }

    if (recordLimit == null || recordLimit.length() == 0 || recordLimit.isEmpty()) {
      throw new MercuryEapiException(CLASS_NAME + METHOD_NAME
          + " :: record Limit for mercury eapi for processing polled messages is null.");
    }

    return new Integer(recordLimit);

  }

  public static String postProcessRequestMessage(MercuryMessageVO mercuryMessageVO) {

    StringBuffer sb = new StringBuffer();
    sb.append("\nMercury ID: ").append(mercuryMessageVO.getMercuryID()).append("\n")
      .append("Outbound Id: ").append(mercuryMessageVO.getOutboundID()).append("\n")
      .append("Sending florist: ").append(mercuryMessageVO.getSendingFlorist()).append("\n")
      .append("Filling florist: ").append(mercuryMessageVO.getFillingFlorist()).append("\n");
    
      if (MercuryEapiConstants.FTD_MESSAGE.equalsIgnoreCase(mercuryMessageVO.getMessageType())) {
        
          sb.append("Recipient: ").append(mercuryMessageVO.getRecipient()).append("\n")
            .append("Address: ").append(mercuryMessageVO.getAddress()).append("\n")
            .append("City: ").append(mercuryMessageVO.getRecipientCity()).append("\n")
            .append("State: ").append(mercuryMessageVO.getRecipientState()).append("\n")
            .append("Zipcode: ").append(mercuryMessageVO.getZipCode()).append("\n")
            .append("Delivery date: ").append(mercuryMessageVO.getDeliveryDate().toString()).append("\n")
            .append("First choice code: ").append(mercuryMessageVO.getFirstChoiceCode()).append("\n")
            .append("First choice desc: ").append(mercuryMessageVO.getFirstChoiceDesc()).append("\n")
            .append("Occasion: ").append(mercuryMessageVO.getOccasion()).append("\n")
            .append("Card message: ").append(mercuryMessageVO.getCardMessage()).append("\n")
            .append("Price: ").append(mercuryMessageVO.getPrice()).append("\n")
            .append("Special Instructions: ").append(mercuryMessageVO.getSpecialInstructions()).append("\n")
            .append("Priority: ").append(mercuryMessageVO.getPriority());

      } else if (MercuryEapiConstants.ASK_MESSAGE.equalsIgnoreCase(mercuryMessageVO.getMessageType())
          || MercuryEapiConstants.ANS_MESSAGE.equalsIgnoreCase(mercuryMessageVO.getMessageType())
          || MercuryEapiConstants.CAN_MESSAGE.equalsIgnoreCase(mercuryMessageVO.getMessageType())) {
        
    	  sb.append("Mercury order number: ").append(mercuryMessageVO.getMercuryOrderNumber()).append("\n")
            .append("Reason: ").append(mercuryMessageVO.getComments()).append("\n")
            .append("Related order reference number: ").append(mercuryMessageVO.getOrderReference()).append("\n")
            .append("Related order sequence number: ").append(mercuryMessageVO.getOrderSequence());
    	  
          if (MercuryEapiConstants.ASK_MESSAGE.equalsIgnoreCase(mercuryMessageVO.getMessageType()) || 
        		  MercuryEapiConstants.ANS_MESSAGE.equalsIgnoreCase(mercuryMessageVO.getMessageType()) ) {
          
              sb.append("\nAsk Answer code: ").append(mercuryMessageVO.getAskAnswerCode()).append("\n")
                .append("Price: ").append(mercuryMessageVO.getPrice());
          }
      } else if (MercuryEapiConstants.ADJ_MESSAGE.equalsIgnoreCase(mercuryMessageVO.getMessageType())){
          sb.append("Mercury order number: ").append(mercuryMessageVO.getMercuryOrderNumber()).append("\n")
            .append("Adjustment code: ").append(mercuryMessageVO.getADJReasonCode()).append("\n")
            .append("Adjustment message: ").append(mercuryMessageVO.getComments()).append("\n")
            .append("Over under charge: ").append(mercuryMessageVO.getOverUnderCharge()).append("\n")
            .append("Combined report number: ").append(mercuryMessageVO.getCombinedReportNumber()).append("\n")
            .append("ROF number: ").append(mercuryMessageVO.getRofNumber()).append("\n")
            .append("Price: ").append(mercuryMessageVO.getPrice());
      }
    return sb.toString();
  }

  public static String postProcessResponseMessage(ProcessedErosMessage processedErosMessage,
      String mercuryId) {
    StringBuffer sb = new StringBuffer();

    sb.append("\nMercury ID: ").append(mercuryId)
        .append("\nReference Number: ").append(processedErosMessage.getReferenceNumber())
        .append("\nMessage status: ").append(processedErosMessage.getMessageStatus())
        .append("\nOrder Sequence number: ").append(processedErosMessage.getOrderSequenceNumber())
        .append("\nAdmin Sequence number: ").append(processedErosMessage.getAdminSequenceNumber())
        .append("\nMessage Id: ").append(processedErosMessage.getMessageId())
        .append("\nTransmission State: ").append(processedErosMessage.getTransmissionStatus())
        .append("\nTransmissionId: ").append(processedErosMessage.getTransmissionId())
        .append("\nError size: ").append(processedErosMessage.getError().size());
    
    if (processedErosMessage.getError().size() > 0) {
    	List errorList = processedErosMessage.getError();
    	for (int i=0; i<errorList.size(); i++) {
    		CodeMessagePair errorMessage = (CodeMessagePair) errorList.get(i);
    		sb.append("\n    ").append(errorMessage.getCode()).append(": ")
    		    .append(errorMessage.getMessage());
    	}
    }
    sb.append("\nWarning size: ").append(processedErosMessage.getWarning().size());
    if (processedErosMessage.getWarning().size() > 0) {
    	List warningList = processedErosMessage.getWarning();
    	for (int i=0; i<warningList.size(); i++) {
    		CodeMessagePair errorMessage = (CodeMessagePair) warningList.get(i);
    		sb.append("\n    ").append(errorMessage.getCode()).append(": ")
    		    .append(errorMessage.getMessage());
    	}
    }
    return sb.toString();

  }

  public static String getMessageDetailsResponse(ProcessedErosMessage processedErosMessage,
      Long messageId, String mainMemberCode) {
    StringBuffer sb = new StringBuffer();
    sb.append("\nMessage Id: ").append(messageId)
        .append("\nMessage Status: ").append(processedErosMessage.getMessageStatus())
        .append("\nMessage Type: ").append(processedErosMessage.getMessageType())
        .append("\nReference Number: ").append(processedErosMessage.getReferenceNumber())
        .append("\nAdmin Sequence Number: ").append(processedErosMessage.getAdminSequenceNumber())
        .append("\nTransmission Id: ").append(processedErosMessage.getTransmissionId())
        .append("\nTransmission Status: ").append(processedErosMessage.getTransmissionStatus());

    ErosMember fillingMember = processedErosMessage.getReceiver();
    if (fillingMember != null) {
        sb.append("\nFilling Florist: ").append(fillingMember.getMemberCode())
    		.append(": ").append(fillingMember.getBusinessName());
    }
    ErosMember sendingMember = processedErosMessage.getSender();
    if (sendingMember != null) {
        sb.append("\nSending Florist: ").append(sendingMember.getMemberCode())
    		.append(": ").append(sendingMember.getBusinessName());
    }

    return sb.toString();
  }
  
  
  /**
   * This method is used to get the IS_MARS_ENABLED Flag from global parameters
   * 
   * @return
   * @throws Exception
   */
  public static boolean isMarsEnabled() {

  	boolean isMarsEnabled = false;
    String flag = null;

    ConfigurationUtil configUtil;
    try {
      configUtil = ConfigurationUtil.getInstance();
      flag = configUtil.getFrpGlobalParm(MercuryEapiConstants.MARS_CONTEXT,
          MercuryEapiConstants.MARS_ENABLED);
    }
    catch (IOException e) {
      logger.error("Exception caught while trying to get the Mars enable/disable flag. Returning false so that request will be processed by old processing logic(i.e. before MARS)");
    }
    catch (SAXException e) {
      logger.error("Exception caught while trying to get the Mars enable/disable flag. Returning false so that request will be processed by old processing logic(i.e. before MARS)");
    }
    catch (ParserConfigurationException e) {
      logger.error("Exception caught while trying to get the Mars enable/disable flag. Returning false so that request will be processed by old processing logic(i.e. before MARS)");
    }
    catch (Exception e) {
      logger.error("Exception caught while trying to get the Mars enable/disable flag. Returning false so that request will be processed by old processing logic(i.e. before MARS)");
    }
    
    if (flag != null && MercuryEapiConstants.Y_STRING.equalsIgnoreCase(flag)) {
    	isMarsEnabled = true;
    }

    return isMarsEnabled;

  }

}