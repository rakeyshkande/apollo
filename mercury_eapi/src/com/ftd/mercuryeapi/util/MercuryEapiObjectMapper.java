package com.ftd.mercuryeapi.util;

/*
 * -------------------------------History------------------------------- Author
 * CreatedDate ModifiedDate Reason
 * --------------------------------------------------------------------- smotla Jun 17th
 * 2013 Jun 17th 2013 Apollo-EAPI Integration
 */
import java.text.SimpleDateFormat;
import java.util.Date;

import com.ftd.eapi.message.service.v1.AdjustmentMessage;
import com.ftd.eapi.message.service.v1.AdjustmentReason;
import com.ftd.eapi.message.service.v1.AnswerMessage;
import com.ftd.eapi.message.service.v1.AskMessage;
import com.ftd.eapi.message.service.v1.CancelOrderMessage;
import com.ftd.eapi.message.service.v1.CodeMessagePair;
import com.ftd.eapi.message.service.v1.ConfirmCancelMessage;
import com.ftd.eapi.message.service.v1.DenyCancelMessage;
import com.ftd.eapi.message.service.v1.ErosMessage;
import com.ftd.eapi.message.service.v1.ForwardMessage;
import com.ftd.eapi.message.service.v1.GeneralMessage;
import com.ftd.eapi.message.service.v1.NewMessage;
import com.ftd.eapi.message.service.v1.OccasionCode;
import com.ftd.eapi.message.service.v1.OrderMessage;
import com.ftd.eapi.message.service.v1.OrderRelatedMessage;
import com.ftd.eapi.message.service.v1.ProcessedErosMessage;
import com.ftd.eapi.message.service.v1.RejectOrderMessage;
import com.ftd.eapi.message.service.v1.RequestCode;
import com.ftd.eapi.message.service.v1.Service;
import com.ftd.eapi.message.service.v1.SimpleResponse;
import com.ftd.mercuryeapi.services.MercuryEapiException;
import com.ftd.mercuryeapi.viewobjects.MercuryMessageVO;
import com.ftd.mercuryeapi.viewobjects.MercuryStageMessageVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * This object contains methods for message conversion between Apollo and EAPI
 */
public class MercuryEapiObjectMapper {

  private static String LOGGER_CATEGORY = "com.ftd.mercuryeapi.util.MercuryEapiObjectMapper";
  private static Logger logger = new Logger(LOGGER_CATEGORY);
  private static final String CLASS_NAME = "MercuryEapiObjectMapper";

  /**
   * This method builds a BatchMessage for the given mercury message
   * 
   * @param MercuryMessageVO
   *          Mercury message
   */
  public ErosMessage buildErosMessage(MercuryMessageVO mercuryMessageVO)
      throws Exception {
    
    // declare and initialize the variable
    final String METHOD_NAME = ".buildErosMessage()";
    String msgType = mercuryMessageVO.getMessageType();

    if (msgType == null) {
      logger.info(CLASS_NAME+METHOD_NAME+" - Mercury Id: "+mercuryMessageVO.getMercuryID()
          + MercuryEapiConstants.ERROR_MESSAGE_MSG_TYPE_NULL);
      throw new Exception(MercuryEapiConstants.LOG_MESSAGE_PART + mercuryMessageVO.getMercuryID()
          + MercuryEapiConstants.LOG_MESSAGE_PART1 + MercuryEapiConstants.LOG_MESSAGE_PART1
          + MercuryEapiConstants.ERROR_MESSAGE_MSG_TYPE_NULL);
    }

    // massage MercuryVO
    mercuryMessageVO = MercuryEapiUtil.massageData(mercuryMessageVO);

    ErosMessage erosMessage = null;

    // construct the request based on the message type
    if (msgType.equals(MercuryEapiConstants.ANS_MESSAGE)) {
      erosMessage = getANSMessage(mercuryMessageVO);
    }
    else if (msgType.equals(MercuryEapiConstants.ASK_MESSAGE)) {
      erosMessage = getASKMessage(mercuryMessageVO);
    }
    else if (msgType.equals(MercuryEapiConstants.CAN_MESSAGE)) {
      erosMessage = getCANMessage(mercuryMessageVO);
    }
    else if (msgType.equals(MercuryEapiConstants.FTD_MESSAGE)) {
      erosMessage = getFTDMessage(mercuryMessageVO);
    }
    else if (msgType.equals(MercuryEapiConstants.GEN_MESSAGE)) {
      erosMessage = getGENMessage(mercuryMessageVO);
    }
    else if (msgType.equals(MercuryEapiConstants.REJ_MESSAGE)) {
      erosMessage = getREJMessage(mercuryMessageVO);
    }
    else if (msgType.equals(MercuryEapiConstants.ADJ_MESSAGE)) {
      erosMessage = getADJMessage(mercuryMessageVO);
    }
    else {
      logger.info(CLASS_NAME+METHOD_NAME+" - "+MercuryEapiConstants.LOG_MESSAGE_PART + ", Mercury Id: "+mercuryMessageVO.getMercuryID()
          + MercuryEapiConstants.LOG_MESSAGE_PART1
          + MercuryEapiConstants.ERROR_MESSAGE_UNKNOWN_MSG_TYPE + msgType);
      throw new Exception(MercuryEapiConstants.ERROR_MESSAGE_UNKNOWN_MSG_TYPE + msgType);

    }
    
    if (logger.isDebugEnabled()) {
      logger.debug(CLASS_NAME+METHOD_NAME+ " - Constructed Eros request message for mercury Id: "+mercuryMessageVO.getMercuryID());
    }

    return erosMessage;

  }

  /**
   * This method constructs FTD order message.
   * <p>
   * 1. Constructs all the common message attributes present in the Eros message
   * <p>
   * 2. Constructs the remaining attributes which are specific to FTD(FTD order) message.
   * 
   * @param mercuryVO
   * @param objFactory
   * @return
   */
  private ErosMessage getFTDMessage(MercuryMessageVO mercuryVO) {
    OrderMessage orderMessage = new OrderMessage();

    // construct all the common eros message attributes
    constructErosMessage(orderMessage, mercuryVO);

    orderMessage.setCardMessage(mercuryVO.getCardMessage());
    orderMessage.setConventionMessage(mercuryVO.getConventionMessage());
    orderMessage.setDeliveryDate(MercuryEapiUtil.getXMLGregorianDate(mercuryVO.getDeliveryDate()));
    orderMessage.setDeliveryDetails(mercuryVO.getDeliveryDetails());
    orderMessage.setFirstChoiceProductCode(mercuryVO.getFirstChoiceCode());
    orderMessage.setFirstChoiceProductDescription(mercuryVO.getFirstChoiceDesc());
    orderMessage.setOccasionCode(OccasionCode.fromValue(mercuryVO.getOccasion()));
    orderMessage.setPrice(MercuryEapiUtil.getBigDec(mercuryVO.getPrice()));
    orderMessage.setRecipientCityStateZip(mercuryVO.getCityStateZip());
    orderMessage.setRecipientName(mercuryVO.getRecipient());
    orderMessage.setRecipientPhoneNumber(mercuryVO.getPhoneNumber());
    orderMessage.setRecipientCity(mercuryVO.getRecipientCity());
    orderMessage.setRecipientStateCode(mercuryVO.getRecipientState());
    if (mercuryVO.getZipCode() == null) {
    	orderMessage.setRecipientPostalCode("N/A");
    } else {
        orderMessage.setRecipientPostalCode(mercuryVO.getZipCode());
    }
    orderMessage.setRecipientStreetAddress(mercuryVO.getAddress());
    orderMessage.setSecondChoiceProductCode(mercuryVO.getSecondChoiceCode());
    orderMessage.setSecondChoiceProductDescription(mercuryVO.getSecondChoiceDesc());
    orderMessage.setSpecialInstructions(mercuryVO.getSpecialInstructions());
    orderMessage.setTagLine(mercuryVO.getTagLine());
    orderMessage.setAllowAutoForward(false);
    if (mercuryVO.getGotoFloristFlag() != null && mercuryVO.getGotoFloristFlag().equals("Y")) {
    	if (mercuryVO.getFloristSuspendStatus() != null && mercuryVO.getFloristSuspendStatus().equals("G")) {
    		orderMessage.setBypassSuspends(false);
    	} else {
    		orderMessage.setBypassSuspends(true);
    	}
    	if (mercuryVO.getFloristAllowsForwards() != null && mercuryVO.getFloristAllowsForwards().equals("Y")) {
    	    orderMessage.setAllowFillerForward(true);
    	    orderMessage.setGotoForwardOnly(true);
    	} else {
    	    orderMessage.setAllowFillerForward(false);
    	    orderMessage.setGotoForwardOnly(false);
    	}
    } else {
    	orderMessage.setBypassSuspends(false);
    	if (mercuryVO.getFloristAllowsForwards() != null && mercuryVO.getFloristAllowsForwards().equals("Y")) {
    	    orderMessage.setAllowFillerForward(true);
    	    orderMessage.setGotoForwardOnly(false);
    	} else {
    	    orderMessage.setAllowFillerForward(false);
    	    orderMessage.setGotoForwardOnly(false);
    	}
    }
    if (logger.isDebugEnabled()) {
        logger.debug("\nGo-To florist:      " + mercuryVO.getGotoFloristFlag() + "\n" +
    		"Suspend Status:     " + mercuryVO.getFloristSuspendStatus() + "\n" +
    		"Allow Forwards:     " + mercuryVO.getFloristAllowsForwards() + "\n" +
    		"bypassSuspends:     " + orderMessage.isBypassSuspends() + "\n" +
    		"allowFillerForward: " + orderMessage.isAllowFillerForward() + "\n" +
    		"gotoForwardOnly:    " + orderMessage.isGotoForwardOnly());
    }
    // orderMessage.setRecipientCountryCode(mercuryVO.getRecipientCountryCode());
    // orderMessage.setResend(false);

    return orderMessage;
  }

  /**
   * This method converts the Mercury Message to Eros ANS message
   * <p>
   * 1. Constructs all the common message attributes present in the Eros message
   * <p>
   * 2. Constructs all the common order related message attributes.
   * <p>
   * 3. Constructs the remaining attributes which are specific to ANS(Answer) message.
   * 
   * @param mercuryVO
   * @param objFactory
   * @return
   */
  private ErosMessage getANSMessage(MercuryMessageVO mercuryVO) {
    AnswerMessage ansMessage = new AnswerMessage();

    // construct all the common eros message attributes
    constructErosMessage(ansMessage, mercuryVO);

    // construct all the common order related message attributes
    constructOrderRelatedMessage(ansMessage, mercuryVO);
    if (mercuryVO.getAskAnswerCode() != null) {
      ansMessage.setPrice(MercuryEapiUtil.getBigDec(mercuryVO.getPrice()));
      ansMessage.setPriceString(String.valueOf(mercuryVO.getPrice()));
      ansMessage.setRequestCode(RequestCode.fromValue(mercuryVO.getAskAnswerCode()));
    }
    ansMessage.setRecipientCountryCode(mercuryVO.getRecipientCountryCode());
    return ansMessage;
  }

  /**
   * This method converts the Mercury Message to the CAN(Cancel order) message
   * <p>
   * 1. Constructs all the common message attributes present in the Eros message
   * <p>
   * 2. Constructs all the common order related message attributes.
   * <p>
   * 3. Constructs the remaining attributes which are specific to CAN(Cancel Order)
   * message.
   * 
   * @param mercuryVO
   * @param objFactory
   * @return
   */
  private ErosMessage getCANMessage(MercuryMessageVO mercuryVO) {
    CancelOrderMessage cancelMessage = new CancelOrderMessage();

    // construct all the common eros message attributes
    constructErosMessage(cancelMessage, mercuryVO);

    // construct all the common order related message attributes
    constructOrderRelatedMessage(cancelMessage, mercuryVO);

    cancelMessage.setRecipientCountryCode(mercuryVO.getRecipientCountryCode());
    return cancelMessage;

  }

  /**
   * This method converts the Mercury Message to the ASK message
   * <p>
   * 1. Constructs all the common message attributes present in the Eros message
   * <p>
   * 2. Constructs all the common order related message attributes.
   * <p>
   * 3. Constructs the remaining attributes which are specific to ASK message.
   * 
   * @param mercuryVO
   * @param objFactory
   * @return
   */
  private ErosMessage getASKMessage(MercuryMessageVO mercuryVO) {
    AskMessage askMessage = new AskMessage();

    // construct all the common eros message attributes
    constructErosMessage(askMessage, mercuryVO);

    // construct all the common order related message attributes
    constructOrderRelatedMessage(askMessage, mercuryVO);

    if (mercuryVO.getAskAnswerCode() != null) {
      askMessage.setRequestCode(RequestCode.fromValue(mercuryVO.getAskAnswerCode()));
      askMessage.setPrice(MercuryEapiUtil.getBigDec(mercuryVO.getPrice()));
      askMessage.setPriceString(String.valueOf(mercuryVO.getPrice()));
    }
    return askMessage;
  }

  /**
   * This method converts the Mercury Message to the GEN message
   * <p>
   * 1. Constructs all the common message attributes present in the Eros message
   * <p>
   * 2. Constructs the remaining attributes which are specific to GEN(General) message.
   * 
   * @param mercuryVO
   * @param objFactory
   * @return
   */
  private ErosMessage getGENMessage(MercuryMessageVO mercuryVO) {
    GeneralMessage generalMessage = new GeneralMessage();

    // construct all the common eros message attributes
    constructErosMessage(generalMessage, mercuryVO);

    generalMessage.setPriority(mercuryVO.getPriority());
    generalMessage.setReceivingMemberCode(mercuryVO.getFillingFlorist());
    generalMessage.setSendingMemberCode(mercuryVO.getSendingFlorist());
    generalMessage.setSlimcast(false);

    return generalMessage;
  }

  /**
   * This method converts the Mercury Message to the REJ(Reject order) message
   * <p>
   * 1. Constructs all the common message attributes present in the Eros message
   * <p>
   * 2. Constructs all the common order related message attributes.
   * <p>
   * 3. Constructs the remaining attributes which are specific to REJ(Reject order)
   * message.
   * 
   * @param mercuryVO
   * @param objFactory
   * @return
   */
  private ErosMessage getREJMessage(MercuryMessageVO mercuryVO) {
    RejectOrderMessage rejectMessage = new RejectOrderMessage();

    // construct all the common eros message attributes
    constructErosMessage(rejectMessage, mercuryVO);

    // contruct the common order related message attributes
    constructOrderRelatedMessage(rejectMessage, mercuryVO);

    return rejectMessage;
  }

  /**
   * This method converts the Mercury Message to the ADJ(Adjustment) message
   * <p>
   * 1. Constructs all the common message attributes present in the Eros message
   * <p>
   * 2. Constructs the remaining attributes which are specific to GEN(General) message.
   * 
   * @param mercuryVO
   * @param objFactory
   * @return
   */
  private ErosMessage getADJMessage(MercuryMessageVO mercuryVO) {
    AdjustmentMessage adjustmentMessage = new AdjustmentMessage();

    // construct all the common eros message attributes
    constructErosMessage(adjustmentMessage, mercuryVO);

    // check this
    if (mercuryVO.getADJReasonCode() != null) {
      adjustmentMessage.setAdjustmentReason(AdjustmentReason
          .fromValue(MercuryEapiConstants.ADJUSTMENT_REASON[Integer.parseInt(mercuryVO
              .getADJReasonCode()) - 1]));
    }
    adjustmentMessage.setCombinedReportNumber(mercuryVO.getCombinedReportNumber());
    adjustmentMessage.setCorrectedAmount(MercuryEapiUtil.getBigDec(mercuryVO.getOverUnderCharge()));
    adjustmentMessage.setCorrectedAmountString(String.valueOf(mercuryVO.getOverUnderCharge()));
    adjustmentMessage.setDeliveryDate(MercuryEapiUtil.getXMLGregorianDate(mercuryVO
        .getDeliveryDate()));
    adjustmentMessage.setDeliveryDateString(mercuryVO.getDeliveryDateText());
    adjustmentMessage.setFillingMemberCode(mercuryVO.getFillingFlorist());
    adjustmentMessage.setMercuryRofNumber(mercuryVO.getRofNumber());
    adjustmentMessage.setMessageText(mercuryVO.getComments());
    adjustmentMessage.setOrderAmount(MercuryEapiUtil.getBigDec(mercuryVO.getPrice()));
    adjustmentMessage.setOrderAmountString(String.valueOf(mercuryVO.getPrice()));
    adjustmentMessage.setRecipientName(mercuryVO.getRecipient());
    return adjustmentMessage;
  }

  /**
   * This method constructs an high level Eros message based on the message type.
   * 
   * @param mercuryVO
   * @param objFactory
   * @return ErosMessage
   */
  private void constructErosMessage(ErosMessage erosMessage, MercuryMessageVO mercuryVO) {

    if (erosMessage != null) {
      erosMessage.setCorrelationReference(mercuryVO.getCorrelationReference());
      erosMessage.setExternalReferenceNumber(mercuryVO.getReferenceNumber());
      erosMessage.setOperator(mercuryVO.getOperator());
      erosMessage.setReceivingMemberCode(mercuryVO.getFillingFlorist());
      if (mercuryVO.getSendingFlorist() != null) {
    	String suffix = mercuryVO.getOutboundID();
    	if (suffix == null) {
    	    ConfigurationUtil configUtil;
    	    try {
    	      configUtil = ConfigurationUtil.getInstance();
    	      suffix = configUtil.getFrpGlobalParm(MercuryEapiConstants.MERCURY_CONFIG_CONTEXT,
    	          MercuryEapiConstants.DEFAULT_CUST_SERVE_SUFFIX);
    	      logger.debug("retrieved suffix " + suffix + " from Global Parms");
    	    }
    	    catch (Exception e) {
    	      suffix = "AA";
    	    }
    	    mercuryVO.setOutboundID(suffix);
    	}
        erosMessage.setSendingMemberCode(mercuryVO.getSendingFlorist() + suffix);
      }
      logger.debug("Sending Florist: " + erosMessage.getSendingMemberCode() + " " +
    		  mercuryVO.getSendingFlorist() + " " + mercuryVO.getOutboundID());
      erosMessage.setService(Service.fromValue(mercuryVO.getService()));
    }
  }

  /**
   * This method constructs an high level Order related message based on the message type.
   * 
   * @param mercuryVO
   * @param objFactory
   * @return OrderRelated
   */
  private void constructOrderRelatedMessage(OrderRelatedMessage orderRelatedMessage,
      MercuryMessageVO mercuryVO) {

    if (orderRelatedMessage != null) {
      orderRelatedMessage.setDeliveryDate(MercuryEapiUtil.getXMLGregorianDate(mercuryVO
          .getDeliveryDate()));
      orderRelatedMessage.setDeliveryDateString(mercuryVO.getDeliveryDateText());
      orderRelatedMessage
          .setOrderDate(MercuryEapiUtil.getXMLGregorianDate(mercuryVO.getOrderDate()));
      orderRelatedMessage.setOrderDateString(mercuryVO.getOrderDate().toString());
      orderRelatedMessage.setReasonText(mercuryVO.getComments());
      orderRelatedMessage.setRecipientAddress(mercuryVO.getAddress());
      orderRelatedMessage.setRecipientCityStateZip(mercuryVO.getCityStateZip());
      orderRelatedMessage.setRecipientName(mercuryVO.getRecipient());
      orderRelatedMessage.setRecipientPhone(mercuryVO.getPhoneNumber());
      orderRelatedMessage.setRecipientCity(mercuryVO.getRecipientCity());
      orderRelatedMessage.setRecipientStateCode(mercuryVO.getRecipientState());
      orderRelatedMessage.setRecipientPostalCode(mercuryVO.getZipCode());
      orderRelatedMessage.setRelatedOrderReferenceNumber(mercuryVO.getOrderReference());
      orderRelatedMessage.setRelatedOrderSequenceNumber(Integer.parseInt(mercuryVO
          .getOrderSequence()));
    }

  }

  /**
   * This method builds the Mercury MessageVO from EAPI ErosResponse
   * 
   * @param mercuryVO
   * @return
   */
  public MercuryMessageVO buildMercuryMessageVO(ProcessedErosMessage processedErosResponse,
      String messageType) throws MercuryEapiException {

    MercuryMessageVO mercuryMessageVO = null;

    // get the message type
    String msgType = processedErosResponse.getMessageType();
    logger.debug("Message Type :" + msgType);

    if (msgType == null) {
      logger.error(processedErosResponse.getMessageId()
          + MercuryEapiConstants.ERROR_MESSAGE_MSG_TYPE_NULL);
      throw new MercuryEapiException(MercuryEapiConstants.LOG_MESSAGE_PART
          + processedErosResponse.getMessageId() + MercuryEapiConstants.LOG_MESSAGE_PART1
          + MercuryEapiConstants.LOG_MESSAGE_PART1
          + MercuryEapiConstants.ERROR_MESSAGE_MSG_TYPE_NULL);
    }
    msgType = msgType.substring(0, 3);

    try {
      // determine message type
      if (MercuryEapiConstants.REJ_MESSAGE.equalsIgnoreCase(msgType)) {
        mercuryMessageVO = buildMercuryMessageVOFromRejectOrderMessage(
            (RejectOrderMessage) processedErosResponse.getErosMessage(), mercuryMessageVO);
      }
      else if (MercuryEapiConstants.ASK_MESSAGE.equalsIgnoreCase(msgType)) {
        mercuryMessageVO = buildMercuryMessageVOFromAskMessage(
            (AskMessage) processedErosResponse.getErosMessage(), mercuryMessageVO);
      }
      else if (MercuryEapiConstants.ANS_MESSAGE.equalsIgnoreCase(msgType)) {
          mercuryMessageVO = buildMercuryMessageVOFromAnswerMessage(
              (AnswerMessage) processedErosResponse.getErosMessage(), mercuryMessageVO);
      }
      else if (MercuryEapiConstants.CONFIRM_CANCEL_MESSAGE.equalsIgnoreCase(msgType)) {
        mercuryMessageVO = buildMercuryMessageVOFromConfirmCancelMessage(
            (ConfirmCancelMessage) processedErosResponse.getErosMessage(), mercuryMessageVO);
      }
      else if (MercuryEapiConstants.DENY_CANCEL_MESSAGE.equalsIgnoreCase(msgType)) {
        mercuryMessageVO = buildMercuryMessageVOFromDenyCancelMessage(
            (DenyCancelMessage) processedErosResponse.getErosMessage(), mercuryMessageVO);
      }
      else if (MercuryEapiConstants.FOR_MESSAGE.equalsIgnoreCase(msgType)) {
        mercuryMessageVO = buildMercuryMessageVOFromForwardMessage(
            (ForwardMessage) processedErosResponse.getErosMessage(), mercuryMessageVO);
      }
      else if (MercuryEapiConstants.GEN_MESSAGE.equalsIgnoreCase(msgType)) {
        mercuryMessageVO = buildMercuryMessageVOFromGeneralMessage(
            (GeneralMessage) processedErosResponse.getErosMessage(), mercuryMessageVO);
      }
      else if (MercuryEapiConstants.FTD_MESSAGE.equalsIgnoreCase(msgType)) {
          mercuryMessageVO = buildMercuryMessageVOFromOrderMessage(
              (OrderMessage) processedErosResponse.getErosMessage(), mercuryMessageVO);
          mercuryMessageVO.setMercuryOrderNumber(processedErosResponse.getReferenceNumber() + "-"
                  + String.format("%04d", processedErosResponse.getAdminSequenceNumber()));
      }
      else if (MercuryEapiConstants.CAN_MESSAGE.equalsIgnoreCase(msgType)) {
          mercuryMessageVO = buildMercuryMessageVOFromCancelMessage(
              (CancelOrderMessage) processedErosResponse.getErosMessage(), mercuryMessageVO);
      }
      else {
        logger.debug(MercuryEapiConstants.LOG_MESSAGE_PART + processedErosResponse.getMessageId()
            + MercuryEapiConstants.LOG_MESSAGE_PART1
            + MercuryEapiConstants.ERROR_MESSAGE_UNKNOWN_MSG_TYPE + msgType);
        throw new MercuryEapiException(MercuryEapiConstants.ERROR_MESSAGE_UNKNOWN_MSG_TYPE
            + msgType);
      }

      // if mercury id is empty, set the mercury id equal to the message #
      if (mercuryMessageVO.getMercuryID() == null || mercuryMessageVO.getMercuryID().length() == 0) {
        mercuryMessageVO.setMercuryID(getMercuryMsgNumFromErosResponse(processedErosResponse));
      }

      mercuryMessageVO.setMercuryStatus(MercuryEapiConstants.MERCURY_CLOSED);
      mercuryMessageVO.setDirection(MercuryEapiConstants.INBOUND_DIRECTION);

      String adminSequenceNumber = String.format("%04d", processedErosResponse.getAdminSequenceNumber());
      String orderSequenceNumber = null;
      if (processedErosResponse.getOrderSequenceNumber() != null) {
          orderSequenceNumber = String.format("%04d", processedErosResponse.getOrderSequenceNumber());
      }
      mercuryMessageVO.setMercuryMessageNumber(processedErosResponse.getReferenceNumber() + "-"
          + adminSequenceNumber);
      mercuryMessageVO.setAdminSequence(adminSequenceNumber);
      mercuryMessageVO.setOrderSequence(orderSequenceNumber);

      if (mercuryMessageVO.getDeliveryDate() != null) {
    	  SimpleDateFormat sdf = new SimpleDateFormat("MMM dd");
    	  mercuryMessageVO.setDeliveryDateText(sdf.format(mercuryMessageVO.getDeliveryDate()));
      }
      
      logger.info("\nMercury Message Number: " + mercuryMessageVO.getMercuryMessageNumber() +
    		  "\nMercury Order Number : " + mercuryMessageVO.getMercuryOrderNumber() +
    		  "\nComments : " + mercuryMessageVO.getComments() +
    		  "\nView Queue Flag : " + mercuryMessageVO.getViewQueue() +
    		  "\nASKP Code : " + mercuryMessageVO.getAskAnswerCode());
    }
    catch (Exception e) {
      throw new MercuryEapiException(e);
    }
    return mercuryMessageVO;
  }

  /**
   * This method is used to construct the mercury id
   * 
   * @param processedErosMessage
   * @return
   */
  private String getMercuryMsgNumFromErosResponse(ProcessedErosMessage processedErosMessage) {

    SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyy");
    String today = sdf.format(new Date());

    return processedErosMessage.getReferenceNumber() + "-"
        + String.format("%04d", processedErosMessage.getAdminSequenceNumber()) + "-" + today;
  }

  /**
   * This method builds the Mercury Message from RejectOrderMessage
   * 
   * @param rejectOrderMessage
   * @param mercuryMessageVO
   * @return
   */
  private MercuryMessageVO buildMercuryMessageVOFromRejectOrderMessage(
      RejectOrderMessage rejectOrderMessage, MercuryMessageVO mercuryMessageVO) throws Exception {

    mercuryMessageVO = new MercuryMessageVO();

    // set message type
    mercuryMessageVO.setMessageType(MercuryEapiConstants.REJ_MESSAGE);

    // set delivery date
    if (rejectOrderMessage.getDeliveryDate() != null) {
      mercuryMessageVO.setDeliveryDate(MercuryEapiUtil.toDate(rejectOrderMessage.getDeliveryDate()));
      mercuryMessageVO.setDeliveryDateText(rejectOrderMessage.getDeliveryDateString());
    }

    // set operator
    mercuryMessageVO.setOperator(rejectOrderMessage.getOperator());

    // set order date
    if (rejectOrderMessage.getOrderDate() != null) {
      mercuryMessageVO.setOrderDate(MercuryEapiUtil.toDate(rejectOrderMessage.getOrderDate()));
    }

    // set reason for rejecting the order
    mercuryMessageVO.setComments(rejectOrderMessage.getReasonText());

    // set recipient details
    mercuryMessageVO.setRecipient(rejectOrderMessage.getRecipientName());
    mercuryMessageVO.setAddress(rejectOrderMessage.getRecipientAddress());
    mercuryMessageVO.setCityStateZip(rejectOrderMessage.getRecipientCityStateZip());
    mercuryMessageVO.setPhoneNumber(rejectOrderMessage.getRecipientPhone());
    mercuryMessageVO.setZipCode(rejectOrderMessage.getRecipientPostalCode());

    // set mercury order number
    mercuryMessageVO.setMercuryOrderNumber(rejectOrderMessage.getRelatedOrderReferenceNumber()
        + "-" + String.format("%04d", rejectOrderMessage.getRelatedOrderSequenceNumber()));

    // set sending florist
    mercuryMessageVO.setSendingFlorist(rejectOrderMessage.getSendingMemberCode());

    // set filling florist code
    mercuryMessageVO.setFillingFlorist(rejectOrderMessage.getReceivingMemberCode());

    // set view queue flag
	mercuryMessageVO.setViewQueue(MercuryEapiConstants.N_STRING);
    if (MercuryEapiUtil.isViewQueueFlagOn()) {
        mercuryMessageVO.setViewQueue(MercuryEapiConstants.Y_STRING);
        if (rejectOrderMessage.getSendingMemberCode() != null
        		&& rejectOrderMessage.getSendingMemberCode().indexOf(
                MercuryEapiConstants.EROS_SYSTEM_CODES) >= 0) {
        	
        	logger.info("Automated Reject, not adding to View Queue");
            mercuryMessageVO.setViewQueue(MercuryEapiConstants.N_STRING);
        }
    }

    return mercuryMessageVO;

  }

  /**
   * This method is used to convert the EAPI WS ASK Message to the Mercury Message
   * 
   * @param askMessage
   * @param mercuryMessageVO
   * @return
   */
  private MercuryMessageVO buildMercuryMessageVOFromAskMessage(AskMessage askMessage,
      MercuryMessageVO mercuryMessageVO) {

    mercuryMessageVO = new MercuryMessageVO();

    // set message type
    mercuryMessageVO.setMessageType(MercuryEapiConstants.ASK_MESSAGE);

    // set delivery date
    if (askMessage.getDeliveryDate() != null) {
      mercuryMessageVO.setDeliveryDate(MercuryEapiUtil.toDate(askMessage.getDeliveryDate()));
      mercuryMessageVO.setDeliveryDateText(askMessage.getDeliveryDateString());
    }

    // set operator
    mercuryMessageVO.setOperator(askMessage.getOperator());

    // set order date
    if (askMessage.getOrderDate() != null) {
      mercuryMessageVO.setOrderDate(MercuryEapiUtil.toDate(askMessage.getOrderDate()));
    }

    // set reason for rejecting the order
    mercuryMessageVO.setComments(askMessage.getReasonText());

    // set recipient details
    mercuryMessageVO.setRecipient(askMessage.getRecipientName());
    mercuryMessageVO.setAddress(askMessage.getRecipientAddress());
    mercuryMessageVO.setCityStateZip(askMessage.getRecipientCityStateZip());
    mercuryMessageVO.setPhoneNumber(askMessage.getRecipientPhone());
    mercuryMessageVO.setZipCode(askMessage.getRecipientPostalCode());

    // set mercury order number
    mercuryMessageVO.setMercuryOrderNumber(askMessage.getRelatedOrderReferenceNumber() + "-"
        + String.format("%04d", askMessage.getRelatedOrderSequenceNumber()));

    // set sending florist code
    mercuryMessageVO.setSendingFlorist(askMessage.getSendingMemberCode());

    // set filling florist code
    mercuryMessageVO.setFillingFlorist(askMessage.getReceivingMemberCode());

    // set product details
    if (askMessage.getRequestCode() == null) {
      mercuryMessageVO.setPrice(null);
    }
    else {
    	mercuryMessageVO.setAskAnswerCode(askMessage.getRequestCode().value());
        if (askMessage.getPrice() != null) {
        	mercuryMessageVO.setPrice(askMessage.getPrice().doubleValue());
        }
    }

    // set view queue flag
    mercuryMessageVO.setViewQueue(MercuryEapiConstants.N_STRING);

    return mercuryMessageVO;

  }

  /**
   * This method is used to convert the EAPI WS ANSWER Message to the Mercury Message
   * before storing
   * 
   * @param answerMessage
   * @param mercuryMessageVO
   * @return
   */
  private MercuryMessageVO buildMercuryMessageVOFromAnswerMessage(
      AnswerMessage answerMessage, MercuryMessageVO mercuryMessageVO) {

    mercuryMessageVO = new MercuryMessageVO();

    // set message type
    mercuryMessageVO.setMessageType(MercuryEapiConstants.ANS_MESSAGE);

    // set delivery date
    if (answerMessage.getDeliveryDate() != null) {
      mercuryMessageVO.setDeliveryDate(MercuryEapiUtil.toDate(answerMessage.getDeliveryDate()));
      mercuryMessageVO.setDeliveryDateText(answerMessage.getDeliveryDateString());
    }

    // set operator
    mercuryMessageVO.setOperator(answerMessage.getOperator());

    // set order date
    // set delivery date
    if (answerMessage.getOrderDate() != null) {
      mercuryMessageVO.setOrderDate(MercuryEapiUtil.toDate(answerMessage.getOrderDate()));
    }

    // set reason for rejecting the order
    mercuryMessageVO.setComments(answerMessage.getReasonText());

    // set recipient details
    mercuryMessageVO.setRecipient(answerMessage.getRecipientName());
    mercuryMessageVO.setAddress(answerMessage.getRecipientAddress());
    mercuryMessageVO.setCityStateZip(answerMessage.getRecipientCityStateZip());
    mercuryMessageVO.setPhoneNumber(answerMessage.getRecipientPhone());
    mercuryMessageVO.setZipCode(answerMessage.getRecipientPostalCode());

    // set mercury order number
    mercuryMessageVO.setMercuryOrderNumber(answerMessage.getRelatedOrderReferenceNumber() + "-"
        + String.format("%04d", answerMessage.getRelatedOrderSequenceNumber()));

    // set sending florist code
    mercuryMessageVO.setSendingFlorist(answerMessage.getSendingMemberCode());

    // set filling florist code
    mercuryMessageVO.setFillingFlorist(answerMessage.getReceivingMemberCode());

    // set product details
    if (answerMessage.getRequestCode() == null) {
      mercuryMessageVO.setPrice(null);
    }
    else {

      mercuryMessageVO.setAskAnswerCode(answerMessage.getRequestCode().value());
      if (answerMessage.getPrice() != null) {
        mercuryMessageVO.setPrice(answerMessage.getPrice().doubleValue());
      }

    }

    // set view queue flag
    mercuryMessageVO.setViewQueue(MercuryEapiConstants.N_STRING);

    return mercuryMessageVO;

  }

  /**
   * This method is used to convert the EAPI WS GENERAL Message to the Mercury Message
   * before storing.
   * 
   * @param generalMessage
   * @param mercuryMessageVO
   * @return
   */
  private MercuryMessageVO buildMercuryMessageVOFromGeneralMessage(
      GeneralMessage generalMessage, MercuryMessageVO mercuryMessageVO) {

    mercuryMessageVO = new MercuryMessageVO();

    // set message type
    mercuryMessageVO.setMessageType(MercuryEapiConstants.GEN_MESSAGE);

    // set operator
    mercuryMessageVO.setOperator(generalMessage.getOperator());

    // set reason for rejecting the order
    mercuryMessageVO.setComments(generalMessage.getMessageText());

    // set sending florist code
    mercuryMessageVO.setSendingFlorist(generalMessage.getSendingMemberCode());

    // set filling florist code
    mercuryMessageVO.setFillingFlorist(generalMessage.getReceivingMemberCode());

    // set priority
    mercuryMessageVO.setPriority(generalMessage.getPriority());

    // set view queue flag
    mercuryMessageVO.setViewQueue(MercuryEapiConstants.N_STRING);

    return mercuryMessageVO;

  }

  /**
   * This method is used to convert the EAPI WS ASK Message to the Mercury Message
   * 
   * @param askMessage
   * @param mercuryMessageVO
   * @return
   */
  private MercuryMessageVO buildMercuryMessageVOFromConfirmCancelMessage(
      ConfirmCancelMessage confirmCancelMessage, MercuryMessageVO mercuryMessageVO) {

    mercuryMessageVO = new MercuryMessageVO();

    // set message type
    mercuryMessageVO.setMessageType(MercuryEapiConstants.CONFIRM_CANCEL_MESSAGE);

    // set delivery date
    if (confirmCancelMessage.getDeliveryDate() != null) {
      mercuryMessageVO.setDeliveryDate(MercuryEapiUtil.toDate(confirmCancelMessage
          .getDeliveryDate()));
      mercuryMessageVO.setDeliveryDateText(confirmCancelMessage.getDeliveryDateString());
    }

    // set operator
    mercuryMessageVO.setOperator(confirmCancelMessage.getOperator());

    // set order date
    if (confirmCancelMessage.getOrderDate() != null) {
      mercuryMessageVO.setOrderDate(MercuryEapiUtil.toDate(confirmCancelMessage.getOrderDate()));
    }

    // set reason for rejecting the order
    mercuryMessageVO.setComments(confirmCancelMessage.getReasonText());

    // set recipient details
    mercuryMessageVO.setRecipient(confirmCancelMessage.getRecipientName());
    mercuryMessageVO.setAddress(confirmCancelMessage.getRecipientAddress());
    mercuryMessageVO.setCityStateZip(confirmCancelMessage.getRecipientCityStateZip());
    mercuryMessageVO.setPhoneNumber(confirmCancelMessage.getRecipientPhone());
    mercuryMessageVO.setZipCode(confirmCancelMessage.getRecipientPostalCode());

    // set mercury order number
    mercuryMessageVO.setMercuryOrderNumber(confirmCancelMessage.getRelatedOrderReferenceNumber()
        + "-" + String.format("%04d", confirmCancelMessage.getRelatedOrderSequenceNumber()));

    // set sending florist code
    mercuryMessageVO.setSendingFlorist(confirmCancelMessage.getSendingMemberCode());

    // set filling florist code
    mercuryMessageVO.setFillingFlorist(confirmCancelMessage.getReceivingMemberCode());

    // set view queue flag
    mercuryMessageVO.setViewQueue(MercuryEapiConstants.N_STRING);

    return mercuryMessageVO;

  }

  /**
   * This method is used to convert the EAPI WS ASK Message to the Mercury Message
   * 
   * @param askMessage
   * @param mercuryMessageVO
   * @return
   */
  private MercuryMessageVO buildMercuryMessageVOFromDenyCancelMessage(
      DenyCancelMessage denyCancelMessage, MercuryMessageVO mercuryMessageVO) {

    mercuryMessageVO = new MercuryMessageVO();

    // set message type
    mercuryMessageVO.setMessageType(MercuryEapiConstants.DENY_CANCEL_MESSAGE);

    // set delivery date
    if (denyCancelMessage.getDeliveryDate() != null) {
      mercuryMessageVO.setDeliveryDate(MercuryEapiUtil.toDate(denyCancelMessage.getDeliveryDate()));
      mercuryMessageVO.setDeliveryDateText(denyCancelMessage.getDeliveryDateString());
    }

    // set operator
    mercuryMessageVO.setOperator(denyCancelMessage.getOperator());

    // set order date
    if (denyCancelMessage.getOrderDate() != null) {
      mercuryMessageVO.setOrderDate(MercuryEapiUtil.toDate(denyCancelMessage.getOrderDate()));
    }

    // set reason for rejecting the order
    mercuryMessageVO.setComments(denyCancelMessage.getReasonText());

    // set recipient details
    mercuryMessageVO.setRecipient(denyCancelMessage.getRecipientName());
    mercuryMessageVO.setAddress(denyCancelMessage.getRecipientAddress());
    mercuryMessageVO.setCityStateZip(denyCancelMessage.getRecipientCityStateZip());
    mercuryMessageVO.setPhoneNumber(denyCancelMessage.getRecipientPhone());
    mercuryMessageVO.setZipCode(denyCancelMessage.getRecipientPostalCode());

    // set mercury order number
    mercuryMessageVO.setMercuryOrderNumber(denyCancelMessage.getRelatedOrderReferenceNumber() + "-"
        + String.format("%04d", denyCancelMessage.getRelatedOrderSequenceNumber()));

    // set sending florist code
    mercuryMessageVO.setSendingFlorist(denyCancelMessage.getSendingMemberCode());

    // set filling florist code
    mercuryMessageVO.setFillingFlorist(denyCancelMessage.getReceivingMemberCode());

    // set view queue flag
    mercuryMessageVO.setViewQueue(MercuryEapiConstants.N_STRING);

    return mercuryMessageVO;

  }

  /**
   * This method is used to convert the EAPI WS FORWARD message to the mercury message
   * 
   * @param forwardMessage
   * @param mercuryMessageVO
   * @return
   */

  private MercuryMessageVO buildMercuryMessageVOFromForwardMessage(
      ForwardMessage forwardMessage, MercuryMessageVO mercuryMessageVO) throws Exception {

    mercuryMessageVO = new MercuryMessageVO();

    // set message type
    mercuryMessageVO.setMessageType(MercuryEapiConstants.FOR_MESSAGE);

    // set delivery date details
    if (forwardMessage.getDeliveryDate() != null) {
      mercuryMessageVO.setDeliveryDate(MercuryEapiUtil.toDate(forwardMessage.getDeliveryDate()));
      mercuryMessageVO.setDeliveryDateText(forwardMessage.getDeliveryDateString());
    }

    // set operator
    mercuryMessageVO.setOperator(forwardMessage.getOperator());

    // set order date
    if (forwardMessage.getOrderDate() != null) {
      mercuryMessageVO.setOrderDate(MercuryEapiUtil.toDate(forwardMessage.getOrderDate()));
    }

    // set reason for rejecting the order
    mercuryMessageVO.setComments(forwardMessage.getReasonText());

    // set recipient details
    mercuryMessageVO.setRecipient(forwardMessage.getRecipientName());
    mercuryMessageVO.setAddress(forwardMessage.getRecipientAddress());
    mercuryMessageVO.setCityStateZip(forwardMessage.getRecipientCityStateZip());
    mercuryMessageVO.setPhoneNumber(forwardMessage.getRecipientPhone());
    mercuryMessageVO.setZipCode(forwardMessage.getRecipientPostalCode());

    // set mercury order number
    mercuryMessageVO.setMercuryOrderNumber(forwardMessage.getRelatedOrderReferenceNumber() + "-"
        + String.format("%04d", forwardMessage.getRelatedOrderSequenceNumber()));

    // set sending florist code
    mercuryMessageVO.setSendingFlorist(forwardMessage.getSendingMemberCode());

    // set filling florist code
    mercuryMessageVO.setFillingFlorist(forwardMessage.getNewFillerMemberCode());

    // set view queue flag
	mercuryMessageVO.setViewQueue(MercuryEapiConstants.N_STRING);
    if (MercuryEapiUtil.isViewQueueFlagOn()) {
        mercuryMessageVO.setViewQueue(MercuryEapiConstants.Y_STRING);
        if (forwardMessage.getSendingMemberCode() != null
        		&& forwardMessage.getSendingMemberCode().indexOf(
                MercuryEapiConstants.EROS_SYSTEM_CODES) >= 0) {
        	
        	logger.info("Automated Forward, not adding to View Queue");
            mercuryMessageVO.setViewQueue(MercuryEapiConstants.N_STRING);
        }
    }

    return mercuryMessageVO;

  }

  /**
   * This method is used to convert the EAPI WS Order Message to the Mercury Message
   * before storing.
   * 
   * @param orderMessage
   * @param mercuryMessageVO
   * @return
   */
  private MercuryMessageVO buildMercuryMessageVOFromOrderMessage(
      OrderMessage orderMessage, MercuryMessageVO mercuryMessageVO) {

    mercuryMessageVO = new MercuryMessageVO();

    // set message type
    mercuryMessageVO.setMessageType(MercuryEapiConstants.FTD_MESSAGE);

    // set operator
    mercuryMessageVO.setOperator(orderMessage.getOperator());

    // set recipient details
    mercuryMessageVO.setRecipient(orderMessage.getRecipientName());
    mercuryMessageVO.setAddress(orderMessage.getRecipientStreetAddress());
    mercuryMessageVO.setCityStateZip(orderMessage.getRecipientCityStateZip());
    mercuryMessageVO.setPhoneNumber(orderMessage.getRecipientPhoneNumber());
    mercuryMessageVO.setZipCode(orderMessage.getRecipientPostalCode());

    // set sending florist code
    mercuryMessageVO.setSendingFlorist(orderMessage.getSendingMemberCode());

    // set filling florist code
    mercuryMessageVO.setFillingFlorist(orderMessage.getReceivingMemberCode());

    // set delivery date details
    if (orderMessage.getDeliveryDate() != null) {
      mercuryMessageVO.setDeliveryDate(MercuryEapiUtil.toDate(orderMessage.getDeliveryDate()));
      mercuryMessageVO.setDeliveryDateText(orderMessage.getDeliveryDetails());
    }

    // set view queue flag
    mercuryMessageVO.setViewQueue(MercuryEapiConstants.N_STRING);

    mercuryMessageVO.setCardMessage(orderMessage.getCardMessage());
    if (orderMessage.getPrice() != null) {
        mercuryMessageVO.setPrice(orderMessage.getPrice().doubleValue());
    }
    mercuryMessageVO.setFirstChoiceCode(orderMessage.getFirstChoiceProductCode());
    mercuryMessageVO.setFirstChoice(orderMessage.getFirstChoiceProductDescription());
    mercuryMessageVO.setSecondChoiceCode(orderMessage.getSecondChoiceProductCode());
    mercuryMessageVO.setSecondChoice(orderMessage.getSecondChoiceProductDescription());
    mercuryMessageVO.setSpecialInstructions(orderMessage.getSpecialInstructions());

    return mercuryMessageVO;

  }

  /**
   * This method is used to convert the EAPI WS CANCEL ORDER Message to the Mercury Message
   * before storing.
   * 
   * @param cancelOrderMessage
   * @param mercuryMessageVO
   * @return
   */
  private MercuryMessageVO buildMercuryMessageVOFromCancelMessage(
      CancelOrderMessage cancelMessage, MercuryMessageVO mercuryMessageVO) {

    mercuryMessageVO = new MercuryMessageVO();

    // set message type
    mercuryMessageVO.setMessageType(MercuryEapiConstants.CAN_MESSAGE);

    // set operator
    mercuryMessageVO.setOperator(cancelMessage.getOperator());

    // set reason for canceling the order
    mercuryMessageVO.setComments(cancelMessage.getReasonText());

    // set sending florist code
    mercuryMessageVO.setSendingFlorist(cancelMessage.getSendingMemberCode());

    // set filling florist code
    mercuryMessageVO.setFillingFlorist(cancelMessage.getReceivingMemberCode());

    // set view queue flag
    mercuryMessageVO.setViewQueue(MercuryEapiConstants.N_STRING);

    return mercuryMessageVO;

  }

  /**
   * This method is used to build the MercuryStageMessageVO Object which is used to create
   * or update a row in Mercury Eapi Staging Table
   * 
   * @param newMessage
   * @return
   */
  public MercuryStageMessageVO buildMercuryStageMessageVO(NewMessage newMessage,
      String pollingProcessId) {

    final String METHOD_NAME = ".buildMercuryStageMessageVO(newMessage,pollingProcessId)";

    // log the message and return
    if (logger.isDebugEnabled()) {
      logger.debug(CLASS_NAME + METHOD_NAME + " :build the Mercury New Message VO.");
    }

    MercuryStageMessageVO mercuryStageMessageVO = new MercuryStageMessageVO();
    mercuryStageMessageVO.setMessageId(newMessage.getMessageId());
    mercuryStageMessageVO.setReferenceNumber(newMessage.getReferenceNumber());
    mercuryStageMessageVO.setMessageType(newMessage.getMessageType());
    mercuryStageMessageVO.setMessageReceivedDate(MercuryEapiUtil.toDateTime(newMessage
        .getMessageReceivedDate()));
    mercuryStageMessageVO.setCreatedBy(pollingProcessId);
    mercuryStageMessageVO.setUpdatedBy(pollingProcessId);
    mercuryStageMessageVO.setCreatedOn(new Date());
    mercuryStageMessageVO.setUpdatedOn(new Date());

    return mercuryStageMessageVO;
  }

  /**
   * This method is used to build the MercuryStageMessageVO Object from SimpleResponse Eapi
   * Object which is used to create or update a row in Mercury Eapi Staging Table
   * 
   * @param newMessage
   * @return
   */
  public MercuryStageMessageVO buildMercuryStageMessageVO(SimpleResponse simpleResponse) {

    MercuryStageMessageVO mercuryStageMessageVO = new MercuryStageMessageVO();
    mercuryStageMessageVO.setReferenceNumber(simpleResponse.getKey());
    mercuryStageMessageVO.setReason(simpleResponse.getMessage());

    if (MercuryEapiConstants.VERIFIED.equalsIgnoreCase(simpleResponse.getCode())) {
    	mercuryStageMessageVO.setStatus(MercuryEapiConstants.CONFIRMED);
    }
    else if (MercuryEapiConstants.REJECTED.equalsIgnoreCase(simpleResponse.getCode())) {
      if (simpleResponse.getMessage().contains(MercuryEapiConstants.REJECTED_CONFIRM_MESSAGE)) {
    	  mercuryStageMessageVO.setStatus(MercuryEapiConstants.CONFIRMED);
      }
      else {
    	  mercuryStageMessageVO.setStatus(MercuryEapiConstants.REJECTED);
      }
    }
    else {
      // for EAPI's response code other than VERIFIED and REJECTED status will be set to RECEIVED 
    	mercuryStageMessageVO.setStatus(MercuryEapiConstants.RECEIVED);
    }
    mercuryStageMessageVO.setUpdatedBy(MercuryEapiConstants.SERVICE_APOLLO);
    mercuryStageMessageVO.setUpdatedOn(new Date());

    return mercuryStageMessageVO;
  }

  /**
   * This method is used to build the MercuryStageMessageVO Object which is used to create
   * or update a row in Mercury Eapi Staging Table
   * 
   * @param newMessage
   * @return
   */
  public MercuryStageMessageVO buildMercuryStageMessageVO() {

    MercuryStageMessageVO mercuryStageMessageVO = new MercuryStageMessageVO();
    mercuryStageMessageVO.setUpdatedBy(MercuryEapiConstants.SERVICE_APOLLO);
    mercuryStageMessageVO.setUpdatedOn(new Date());

    return mercuryStageMessageVO;
  }

  /**
   * This method is used to build the MercuryStageMessageVO Object from SimpleResponse Eapi
   * Object which is used to create or update a row in Mercury Eapi Staging Table
   * 
   * @param newMessage
   * @return
   */
  public MercuryStageMessageVO buildMercuryStageMessageVO(CodeMessagePair codeMessagePair) {

    MercuryStageMessageVO mercuryStageMessageVO = new MercuryStageMessageVO();
    mercuryStageMessageVO.setReason(codeMessagePair.getCode() + " : " + codeMessagePair.getMessage());
    mercuryStageMessageVO.setStatus(codeMessagePair.getCode());
    mercuryStageMessageVO.setUpdatedBy(MercuryEapiConstants.SERVICE_APOLLO);
    mercuryStageMessageVO.setUpdatedOn(new Date());

    return mercuryStageMessageVO;
  }
}