package com.ftd.mercuryeapi.mdb;

import javax.ejb.EJBException;
import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import com.ftd.mercuryeapi.services.MercuryEapiException;
import com.ftd.mercuryeapi.services.MercuryEapiMessageMgmt;
import com.ftd.mercuryeapi.util.MercuryEapiUtil;
import com.ftd.osp.utilities.plugins.Logger;

public class MercuryEapiPollMessageProcessorMDB implements MessageDrivenBean, MessageListener {

  MessageDrivenContext messageDrivenContext;

  private static Logger logger = new Logger(MercuryEapiPollMessageProcessorMDB.class.getName());

  @Override
  public void onMessage(Message message) {

    // declare and initialize the variables
    String messageId = null;

    try {

      // read the message
      TextMessage textMessage = (TextMessage) message;
      messageId = textMessage.getText();

      logger.info("Retrieved Message: " + textMessage.getText());
      
      // process polled message data
      MercuryEapiMessageMgmt mercuryEapiMgmt = new MercuryEapiMessageMgmt();
      mercuryEapiMgmt.processPolledMessageDetails(messageId);

      logger.info("Completed processing MDB");
    }
    catch (MercuryEapiException e) {

      logger.error(e);
      String errorMessage = "MercuryEapiException occured while processing "
          + messageId;
      logger.error(errorMessage);
      // send system message
      MercuryEapiUtil.sendSystemMessageWrapper(e.getMessage());

    }
    catch (Exception e) {

      logger.error(e);
      String errorMessage = "Exception occured while processing "
          + messageId;
      logger.error(errorMessage);
      // send system message
      MercuryEapiUtil.sendSystemMessageWrapper(e.getMessage());
    }

  }

  public void ejbRemove() throws EJBException {
    if (logger.isDebugEnabled()) {
      logger.debug(".ejbRemove()::" + "***In MercuryEapiMDB remove()***");
    }
  }

  public void ejbCreate() {
    if (logger.isDebugEnabled()) {
      logger.debug(".ejbCreate()::" + "***In MercuryEapiMDB create()***");
    }
  }

  @Override
  public void setMessageDrivenContext(MessageDrivenContext ctx) throws EJBException {
    if (logger.isDebugEnabled()) {
      logger.debug(".setMessageDrivenContext()::"
        + "***In MercuryEapiPollMessageProcessorMDB setMessageDrivenContext()**");
    }
    this.messageDrivenContext = ctx;
  }

}
