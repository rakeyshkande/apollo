package com.ftd.mercuryeapi.mdb;

import java.sql.Connection;
import java.util.List;

import javax.ejb.EJBException;
import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.naming.InitialContext;

import com.ftd.mercuryeapi.dao.MercuryDAO;
import com.ftd.mercuryeapi.services.MercuryEapiException;
import com.ftd.mercuryeapi.services.MercuryEapiMessageMgmt;
import com.ftd.mercuryeapi.util.MercuryEapiConstants;
import com.ftd.mercuryeapi.util.MercuryEapiUtil;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;

public class MercuryEapiPollMessageMDB implements MessageDrivenBean, MessageListener {

  MessageDrivenContext messageDrivenContext;

  private static Logger logger = new Logger(MercuryEapiPollMessageMDB.class.getName());

  // flag to indicate if we have sent out the initial JMS messages
  private static boolean initialMessagesSent;

  @SuppressWarnings("unused")
  @Override
  public void onMessage(Message message) {

    String mainMemberCode = null;
    MercuryEapiMessageMgmt mercuryEapiMgmt = null;

    try {

      // read the message
      TextMessage textMessage = (TextMessage) message;
      mainMemberCode = textMessage.getText();
      
      logger.info("Retrieved Message: " + textMessage.getText());

      // get new messages
      mercuryEapiMgmt = new MercuryEapiMessageMgmt();
      mercuryEapiMgmt.getNewMessagesFromEapi(mainMemberCode);

      logger.info("Completed processing MDB");
    } catch (Exception e) {

      logger.error(e);
      String errorMessage = "Exception occured while processing " + mainMemberCode;
      logger.error(errorMessage);
    }
  }

  @Override
  public void ejbRemove() throws EJBException {
    if (logger.isDebugEnabled()) {
      logger.debug(".ejbRemove()::" + "***In MercuryEapiMDB remove()***");
    }
  }

  public void ejbCreate() {
    if (logger.isDebugEnabled()) {
      logger.debug(".ejbCreate()::" + "***In MercuryEapiMDB create()***");
    }
  }

  /**
   * This message is invoked when the container is started
   * 
   * @param ctx
   */
  public void setMessageDrivenContext(MessageDrivenContext ctx) {
    
    this.messageDrivenContext = ctx;
    List<String> mainMemberCodes = null;

    // if the initial messages have not been then send them out..kick off the process
    if (!initialMessagesSent) {
      initialMessagesSent = true;

      if (logger.isDebugEnabled()) {
        logger.debug("Starting Mercury polling Process");
      }

      Connection conn = null;

      try {

        InitialContext initialContext = new InitialContext();

        // get DB connection
        conn = MercuryEapiUtil.getConnection();

        // create dao
        MercuryDAO dao = new MercuryDAO(conn);

        // remove any existing messages from MERCURY_POLL queue
        dao.clearEapiPollQueue();

        // get available main member code
        mainMemberCodes = dao.getAvailableMainMemberCode(MercuryEapiConstants.Y_STRING,
            MercuryEapiConstants.N_STRING);

        // send main member codes as jms messages to MERCURY_POLL queue
        if (mainMemberCodes != null) {
          if (mainMemberCodes.size() > 0) {
            for (String mainMemberCode : mainMemberCodes) {
              MessageToken token = new MessageToken();
              token.setMessage(mainMemberCode);
              token.setProperty(MercuryEapiConstants.JMS_DELAY_PROPERTY, "1", "int");
              logger.info("Sending JMS message: " + mainMemberCode +" to MERCURY_POLL queue");
              // send jms message
              MercuryEapiUtil.sendJMSMessage(initialContext, token,
                  MercuryEapiConstants.MERCURY_EAPI_POLL_QUEUE);
            }
          }
        }

      }
      catch (Throwable t) {
        String message = "Error starting mercury eapi.";
        logger.error(message);
        logger.error(t);
        try {
          // send system message
          MercuryEapiUtil.sendSystemMessageWrapper(message + " : " + t.toString());
        }
        catch (Exception e) {
          logger.error(e);
        }
      }
      finally {
        try {
          conn.close();
        }
        catch (Exception e) {
          // do nothing
        }// end try catch
      }// end finally
    }// end if messages sent
  }// end method

}
