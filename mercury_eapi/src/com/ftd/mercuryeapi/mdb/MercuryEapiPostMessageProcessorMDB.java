package com.ftd.mercuryeapi.mdb;

import javax.ejb.EJBException;
import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import com.ftd.mercuryeapi.services.MercuryEapiException;
import com.ftd.mercuryeapi.services.MercuryEapiMessageMgmt;
import com.ftd.mercuryeapi.util.MercuryEapiUtil;
import com.ftd.osp.utilities.plugins.Logger;

public class MercuryEapiPostMessageProcessorMDB implements MessageDrivenBean, MessageListener {

  private static Logger logger = new Logger(MercuryEapiPostMessageProcessorMDB.class.getName());

  MessageDrivenContext messageDrivenContext;

  public void onMessage(Message message) {

    String mercuryId = null;

    try {
      // read the message
      TextMessage textMessage = (TextMessage) message;
      mercuryId = textMessage.getText();

      logger.info("Retrieved Message: " + mercuryId);

      // process POST messages
      MercuryEapiMessageMgmt mercuryEapiMgmt = new MercuryEapiMessageMgmt();
      mercuryEapiMgmt.processPOSTMessages(mercuryId);

      logger.info("Completed processing MDB");

    }
    catch (MercuryEapiException e) {
      logger.error(e);
      String errorMessage = "Exception occured while processing the POST Message for " + mercuryId;
      logger.error(errorMessage + " :: " + e);
      MercuryEapiUtil.sendSystemMessageWrapper(e.getMessage());
    }
    catch (JMSException jmsException) {
      logger.error(jmsException);
      String errorMessage = "JMS Exception occured while processing the POST Message for " + mercuryId;
      logger.error(errorMessage + " :: " + jmsException);
      MercuryEapiUtil.sendSystemMessageWrapper(jmsException.getMessage());
    }
    catch (Exception e) {
      String errorMessage = "Exception occured while processing the POST Message for " + mercuryId;
      logger.error(errorMessage + " :: " + e);
      MercuryEapiUtil.sendSystemMessageWrapper(e.getMessage());
    }
  }

  public void ejbRemove() throws EJBException {
    logger.debug(".ejbRemove()::" + "***In MercuryEapiMDB remove()***");
  }

  public void ejbCreate() {
    logger.debug(".ejbCreate()::" + "***In MercuryEapiMDB create()***");
  }

  public void setMessageDrivenContext(MessageDrivenContext ctx) throws EJBException {
    logger.debug(".setMessageDrivenContext()::"
        + "***In MercuryEapiPostMessageProcessorMDB setMessageDrivenContext()**");
    this.messageDrivenContext = ctx;
  }
}
