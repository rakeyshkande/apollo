package com.ftd.taxadmin.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.csvreader.CsvReader;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;

/**
 * @author smeka
 *
 */
public class TaxAdminDAO {
	
	private Logger logger = new Logger(TaxAdminDAO.class.getName());
	
	// Static constants for input params
	private static final String IN_STATE = "IN_STATE";
	private static final String IN_COUNTYNAME = "IN_COUNTYNAME";
	private static final String IN_CITYNAME = "IN_CITYNAME";
	private static final String IN_ZIPCODE = "IN_ZIPCODE";
	private static final String IN_HSTFLAG = "IN_HSTFLAG";
	private static final String IN_STATERATE = "IN_STATERATE";
	private static final String IN_COUNTYRATE = "IN_COUNTYRATE";
	private static final String IN_CITYRATE = "IN_CITYRATE";
	private static final String IN_TOTALRATE = "IN_TOTALRATE";
	private static final String IN_DISTRATE = "IN_DISTRATE";
	private static final String IN_STGCRT = "IN_STGCRT";	
	private static final String IN_COUNTRY = "IN_COUNTRY";
	private static final String IN_FILE_DATA = "IN_FILE_DATA";
	
	// Static constants for output params & values
	public static final Object OUT_STATUS = "OUT_STATUS";
	public static final Object OUT_MESSAGE = "OUT_MESSAGE";
	public static final String NO = "N";
	
	
	//Static constants for statements
	private static final String SAVE_NEW_TAX_RATES_FILE = "SAVE_NEW_TAX_RATES_FILE";
	private static final String INSERT_TAX_RATES = "INSERT_TAX_RATES";

	
	/**
	 * @throws Exception
	 */
	public TaxAdminDAO() throws Exception {
		 
	}

	/** Delete all the tax rates
	 * @return
	 * @throws Exception
	 */
	public boolean deleteAllMasterTaxRates(DataRequest dataRequest) throws Exception {
		if(logger.isDebugEnabled()) {
			logger.debug("*********deleteAllMasterTaxRates*********");
		}
		
		try {			
			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			dataRequest.setStatementID("DELETE_TAX_RATES");
			
			@SuppressWarnings("unchecked")
			Map<String, Object> outputs = (Map<String, Object>) dataAccessUtil.execute(dataRequest);

			String status = (String) outputs.get(OUT_STATUS);
			
			if (NO.equalsIgnoreCase(status)) { 
				StringBuffer message = new StringBuffer("unable to delete master tax rates, Error caught: ").append(outputs.get(OUT_MESSAGE));
				throw new SQLException(message.toString());
			} 
			
			return true;
			
		} catch(Exception e) {
			throw new Exception(e.getMessage());
		} 		
	}

	/** Insert the new tax rate
	 * @param reader
	 * @param inputParams
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public Map<String, String> insertTaxRate(CsvReader reader, Map<String, String> inputParams, DataRequest dataRequest) throws Exception {
		if(logger.isDebugEnabled()) {
			logger.debug("*********insertTaxRate*********");
		}
		
		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
		
		inputParams.put(IN_COUNTRY, reader.get(5));
		inputParams.put(IN_STATE, reader.get(6));
		inputParams.put(IN_COUNTYNAME, reader.get(7));
		inputParams.put(IN_CITYNAME,reader.get(8));
		inputParams.put(IN_ZIPCODE, reader.get(9));
		inputParams.put(IN_HSTFLAG, reader.get(10));
		inputParams.put(IN_STATERATE, reader.get(11));
		inputParams.put(IN_COUNTYRATE, reader.get(12));
		inputParams.put(IN_CITYRATE, reader.get(13));
		inputParams.put(IN_DISTRATE, reader.get(14));
		inputParams.put(IN_TOTALRATE, reader.get(15));
		inputParams.put(IN_STGCRT, reader.get(17));
		
		
		dataRequest.setInputParams(inputParams);
		dataRequest.setStatementID(INSERT_TAX_RATES);
		
		return (Map<String, String>) dataAccessUtil.execute(dataRequest);		
	}
	
	
	/** Save the new tax rates file to tax_rates_archive
	 * @param taxRatesFileData
	 * @return
	 * @throws Exception
	 */
	public boolean saveNewTaxRatesFile(StringBuilder taxRatesFileData, DataRequest dataRequest) throws Exception {
		if(logger.isDebugEnabled()) {
			logger.debug("*********saveNewTaxRates()***********");
		} 
			
		try {
			 
			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			Map<String, String> inputParams = new HashMap<String, String>();		
						
			inputParams.put(IN_FILE_DATA,  taxRatesFileData.toString());
			dataRequest.setInputParams(inputParams);
			dataRequest.setStatementID(SAVE_NEW_TAX_RATES_FILE); 

			@SuppressWarnings("unchecked")
			Map<String, Object> outputs = (Map<String, Object>) dataAccessUtil.execute(dataRequest);
			String status = (String) outputs.get(OUT_STATUS);
			
			if (NO.equalsIgnoreCase(status)) { 
				StringBuffer message = new StringBuffer("unable to save the new tax rates, Error caught: ").append(outputs.get(OUT_MESSAGE));
				throw new SQLException(message.toString());
			}
			
			return true;
			
		} catch(Exception e) {
			throw new Exception(e.getMessage());
		}   finally { 
			taxRatesFileData = null;
		} 
	}

	/** Send the system message
	 * @param sysMessage
	 */
	public void sendSystemAlert(SystemMessengerVO sysMessage, Connection conn) {
		String messageID = null;		
		try {			
			logger.error("************sendSystemAlert() for message, " + sysMessage.getMessage());			
			SystemMessenger sysMessenger = SystemMessenger.getInstance(); 
			messageID = sysMessenger.send(sysMessage, conn, false);
			if (messageID == null) {
				String msg = "Error occured while attempting to send out a system message. Msg not sent: " + sysMessage.getMessage();
				logger.error(msg);
			}			
		} catch(Exception e) {
			logger.error("Error caught sending an alert, ", e);			
		}	
		
	}
}
