package com.ftd.taxadmin.mdb;

import java.sql.Connection;
import java.sql.SQLException;

import javax.ejb.EJBException;
import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.taxadmin.bo.TaxRatesProcessorBO;

/**
 * @author smeka
 * 
 */
@SuppressWarnings("serial")
public class TaxRatesProcessorMDB implements MessageDrivenBean, MessageListener { 
	
	private static Logger logger = new Logger(TaxRatesProcessorMDB.class.getName());
	
	public static final String LOAD_NEW_TAX_RATES = "loadTaxRates";	
	public static final String DOWNLOAD_TAX_RATES = "downloadTaxRates";
	
	// others
	private static final String DATA_SOURCE_NAME = "TAX_DS";
	protected MessageDrivenContext messageDrivenContext = null;

	@Override
	public void onMessage(Message msg) { 
		
		TextMessage textMessage = (TextMessage) msg;
		String payloadTxt = null;
		
		try {
			payloadTxt = textMessage.getText();
		} catch (JMSException e) {
			logger.error("Unable to identify the text message," , e);
			return;
		}
		
		if(logger.isDebugEnabled()) {
			logger.debug("request triggered for action on tax rates, " + payloadTxt);
		} 
		
		TaxRatesProcessorBO taxRatesProcessorBO = null;
		String requestType = payloadTxt;
		String fileName = null;
		DataRequest dataRequest = null;
		Connection conn = null;	
		
		try { 
			conn = getDatabaseConnection();
			
			if (conn == null) {
				throw new Exception("Unable to connect to database");
			}
			
			if(payloadTxt != null && payloadTxt.indexOf(TaxRatesProcessorBO.COMMA_SEPERATOR) > 0) {
				requestType = payloadTxt.split(TaxRatesProcessorBO.COMMA_SEPERATOR)[0];
				fileName = payloadTxt.split(TaxRatesProcessorBO.COMMA_SEPERATOR)[1];
			}          
            
            dataRequest = new DataRequest();
			dataRequest.setConnection(conn);
			taxRatesProcessorBO = new TaxRatesProcessorBO(dataRequest);
			
			if(LOAD_NEW_TAX_RATES.equalsIgnoreCase(requestType)) {				
				taxRatesProcessorBO.loadNewTaxRates(fileName);
			} else if(DOWNLOAD_TAX_RATES.equalsIgnoreCase(requestType)) {				
				taxRatesProcessorBO.downloadTaxRates();
			} else {
				logger.info("Invalid request to TaxRatesUpdateMDB, " + requestType);
			}
			
		}   catch (Throwable e) { 
			logger.info(e.toString());
			//messageDrivenContext.setRollbackOnly();
			throw new RuntimeException("Rollback detected, exception thrown to force rollback.");
		}
		finally {			
			try {
				if(dataRequest != null && dataRequest.getConnection() != null && !dataRequest.getConnection().isClosed()) {
					dataRequest.getConnection().close();
				}
			} catch (SQLException e) {
				logger.error("Error closing connection." + e.getMessage());
			}
		}
	}

	@Override
	public void ejbRemove() throws EJBException {}

	@Override
	public void setMessageDrivenContext(MessageDrivenContext messageDrivenContext) throws EJBException {
		this.messageDrivenContext = messageDrivenContext;
	}
	
	public void ejbCreate() {	}
	
	public Connection getDatabaseConnection() throws Exception {
		logger.debug("Entering 'getDatabaseConnection' with datasource " + DATA_SOURCE_NAME);
		String mappedDatasource = ConfigurationUtil.getInstance().getProperty("tax_admin_config.xml", DATA_SOURCE_NAME);
		logger.debug("Datasource " + DATA_SOURCE_NAME + " maps to " + mappedDatasource);
		return DataSourceUtil.getInstance().getConnection(mappedDatasource);
	}

	
}
