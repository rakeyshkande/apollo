

//***********************************************************************************
// Listbox.js  - Listbox functions
//
//	GetItemLocation	            ???
//	btnMoveRight_onClick		move selected items from one listbox to another
//	btnMoveLeft_onClick			move selected items from one listbox to another
//	btnMoveAllRight_onClick		moves all items from one listbox to another
//	btnMoveAllLeft_onClick		moves all items from one listbox to another
//	checkALL			checks to see if use selected all items in 
//					    passed listbox. If so, selects ALL option and 
//					    deselects all other options.
//  	checkALLWD                      makes sure that if ALL that nothing else is selected.
//***********************************************************************************

//**************************************************************************
//Function:		GetItemLocation(FromListText, ToList)
//Note:  		Modified so that the string comparison is NOT case sensitive.
//**************************************************************************

changeFlag = false;

function GetItemLocation (FromListText, ToList) {
var i; 

	if (ToList.options.length == 0) {
		i = 0
	}
	else {	
		for (var x=0; x < ToList.options.length; x++) {					
			if (FromListText.toUpperCase() < ToList.options[x].text.toUpperCase()) {
				i = x
				x = ToList.options.length
			}
			else {
				i = ToList.options.length
			}		
		}
	}	

return (i);
}

//**************************************************************************
//Function:		btnMoveRight_onclick(FromList, ToList)
//Desc:			Moves all selected items from the Selected listbox (FromList)
//				into the Available listbox (ToList)
//
//Author:		Chris Fagalde, Software Architects, Inc.
//
//--------------------------------------------------------------------------
//Changes:
//	5/30/2000	Chris Fagalde	Inital Creation
//**************************************************************************
function btnMoveRight_onclick(FromList, ToList) {
	var oOption = document.createElement("OPTION");
	var ItemLocation;

	  changeFlag = true;

		for (var i=0; i < FromList.options.length; i++) {		
			if(FromList.options[i].selected) {
				oOption = document.createElement("OPTION")
				ItemLocation = GetItemLocation(FromList.options(i).text, ToList)
				oOption.text = FromList.options[i].text
				oOption.value = FromList.options[i].value
				ToList.options.add(oOption,ItemLocation)
				FromList.options.remove(i)
				i--
			}
		}		
	}
	//**************************************************************************
	//Function:		btnMoveLeft_onclick(FromList, ToList)
	//
	//Desc:			Moves all selected items from the Available listbox (FromList)
	//					into the Selected listbox (ToList)
	//**************************************************************************
	function btnMoveLeft_onclick(FromList, ToList) {
	var oOption = document.createElement("OPTION");
	var ItemLocation;

	  changeFlag = true;

		for (var i=0; i < FromList.options.length; i++) {
			if(FromList.options[i].selected) {
				oOption = document.createElement("OPTION")
				ItemLocation = GetItemLocation(FromList.options(i).text, ToList)
				oOption.text = FromList.options[i].text
				oOption.value = FromList.options[i].value
				ToList.options.add(oOption,ItemLocation)
				FromList.options.remove(i)
				i--
			}
		}		
	}


	//**************************************************************************
	//Function:		btnMoveAllRight_onclick(FromList, ToList)
	//
	//Desc:			Moves all items from the Available listbox (FromList) into 
//					the Selected listbox (ToList)
	//**************************************************************************
	function btnMoveAllRight_onclick(FromList, ToList) {
	var oOption = document.createElement("OPTION");
	var ItemLocation;

	  changeFlag = true;

		for (var i=0; i < FromList.options.length; i++) {
			oOption = document.createElement("OPTION")
			ItemLocation = GetItemLocation(FromList.options(i).text, ToList)
			
			oOption.text = FromList.options[i].text
			oOption.value = FromList.options[i].value
			ToList.options.add(oOption,ItemLocation)
			FromList.options.remove(0)
			i--
		}		
	}

	//**************************************************************************
	//Function:		btnMoveAllLeft_onclick(FromList, ToList)
	//
	//Desc:			Moves all items from the Selected listbox (FromList) into 
//					the Available listbox (ToList)
	//**************************************************************************
	function btnMoveAllLeft_onclick(FromList, ToList) {
	var oOption = document.createElement("OPTION");	

	  changeFlag = true;
		
		for (var i=0; i<FromList.options.length; i++) {
			ItemLocation = GetItemLocation(FromList.options(i).text, ToList)
			oOption = document.createElement("OPTION")
			oOption.text = FromList.options[i].text
			oOption.value = FromList.options[i].value
			ToList.options.add(oOption,ItemLocation)
			FromList.options.remove(0)
			i--
		}
	}

/***************************************************************************
 *	checkALL(LISTBOX objMultiSel)
 *
 *	This method will check to make sure that if the person selected ALL that 
 *	nothing else is selected.  It will also Check to see if the user has selected 
 *	all of the items individually.  If this happens, it will then select ALL and
 *	unselect everything else.
 ***************************************************************************/
function checkALL(objMultiSel)
{
	var selectcount=0;
	// Make sure the rest are unselected if the all is selected.
	// Change the selection to ALL if they select all of the individual selections.
	
	if (objMultiSel.options[0].selected) 
	{
		for(var i=1; i < objMultiSel.length; i++) 
		{
			objMultiSel.options[i].selected = false;
		}
	}
	else 
	{
		for (var i=1; i<objMultiSel.length; i++) 
		{
			// find out how many options were selected.  If one of the options (other than all)
			// was not selected, we will end the count and continue processing.
			if (objMultiSel.options[i].selected) 
			{
				selectcount++;
			}
			else 
			{
				i = objMultiSel.length;
			}
		}
		if (selectcount == objMultiSel.length-1) 
		{
			// All of the individual values were selected change the selection to all
			objMultiSel.options[0].selected = true;
			
			for(var i=1; i < objMultiSel.length; i++) 
			{
				objMultiSel.options[i].selected = false;
			}
		}
	}
}

/***************************************************************************
 *	Function:		checkALLWD(LISTBOX objMultiSel )
 *	This method will check to make sure that if the person selected ALL that 
 *	nothing else is selected.  It will also Check to see if the user has 
 *	selected all of the items individually.  If this happens, it will then 
 *	select ALL and unselect everything else.  This Check all will also take into
 *	concideration the dashed line when counting selected values. This value 
 *	for the dashed line needs to be "---".  This function will uncheck the 
 *	dashed line if the dashed line was checked.  This function does NOT 
 *	handle any error messages. This function will account for multiple lines 
 *	of dashes, and it doesn't matter where the dash is located.
 ***************************************************************************/
function checkALLWD(objMultiSel)
{
	var selectcount=0;
	var dashcount=1; //this is set to one to handle the All case;
	
	// Make sure the rest are unselected if the all is selected.
	// Change the selection to ALL if they select all of the individual selections.
	
	if (objMultiSel.options[0].selected) 
	{
		for(var i=1; i < objMultiSel.length; i++) 
		{
			objMultiSel.options[i].selected = false;
		}
	}
	else 
	{
		for (var i=1; i<objMultiSel.length; i++) 
		{
			// find out how many options were selected.  If one of the options (other than all)
			// was not selected, we will end the count and continue processing.
			if (objMultiSel.options[i].value=="---")
			{
				dashcount++;
				objMultiSel.options[i].selected = false;
			}
			else
			{
				if (objMultiSel.options[i].selected) 
				{
					selectcount++;
				}
				else 
				{
					i = objMultiSel.length;
				}
			}
		}
		if (selectcount == objMultiSel.length-dashcount) 
		{
			alert(objMultiSel.length + "-" + dashcount + "=" + (objMultiSel.length-dashcount));
			// All of the individual values were selected change the selection to all
			objMultiSel.options[0].selected = true;
			
			for(var i=1; i < objMultiSel.length; i++) 
			{
				objMultiSel.options[i].selected = false;
			}
		}
	}
}

function Moveup(dbox) 
{
	var curSelected = 0;
	for(var i = 0; i < dbox.options.length; i++) 
	{
		if (dbox.options[i].selected && dbox.options[i] != "" && dbox.options[i] != dbox.options[0]) 
		{
			var tmpval = dbox.options[i].value;
			var tmpval2 = dbox.options[i].text;
			dbox.options[i].value = dbox.options[i - 1].value;
			dbox.options[i].text = dbox.options[i - 1].text
			dbox.options[i-1].value = tmpval;
			dbox.options[i-1].text = tmpval2;
			curSelected = i-1;
      		}
   	}
   	dbox.options.selectedIndex = curSelected;
}

function Movedown(ebox) 
{
	var curSelected = 0;
	for(var i = 0; i < ebox.options.length; i++) 
	{
		if (ebox.options[i].selected && ebox.options[i] != "" && ebox.options[i+1] != ebox.options[ebox.options.length]) 
		{
			var tmpval = ebox.options[i].value;
			var tmpval2 = ebox.options[i].text;
			ebox.options[i].value = ebox.options[i+1].value;
			ebox.options[i].text = ebox.options[i+1].text
			ebox.options[i+1].value = tmpval;
			ebox.options[i+1].text = tmpval2;
			curSelected = i+1;
      		}
   	}
   	ebox.options.selectedIndex = curSelected;
}


// Clears the selections in the Multi-Select List Box
function clearAllSelections(objMultiSel) {
  	for(var i = 0; i < objMultiSel.options.length; i++) {
      objMultiSel.options[i].selected = false;
    }
}


