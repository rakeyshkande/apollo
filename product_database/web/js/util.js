/*
 *  Dependencies: js/FormChek.js
 *                js/events.js
 */


/*
this variable is used in the Modify Order process in order
to make lock checking as generic as possible
also see the checkRecordLock() function in this file.
*/
var isRecordLocked = false;


function doMainMenuAction(){
  var url = '';
  url = "MainMenuAction.do" + getSecurityParams(true);
  performAction(url);
}

function performAction(url){
  document.forms[0].action = url;
  document.forms[0].submit();
}

/*
   Calls helper methods to limit navigation options.
*/
function setNavigationHandlers()
{
   document.oncontextmenu = contextMenuHandler;
   document.onkeydown = backKeyHandler;
}

/*
   Disables the right button mouse click.
*/
function contextMenuHandler()
{
   return false;
}

/*
   Closes the parent window.
*/
function closeParentWindow()
{
 if(window.parent != undefined && window.parent!=null)
   {
       top.close();
   }
}

/*
   Disables the various navigation keyboard commands.
*/
function backKeyHandler()
{
   // backspace
   if (window.event && window.event.keyCode == 8)
   {

			//when the control is on the page instead of an element like text box, we want to suppress the
			//back space.  This would prevent the users from hitting Alt+Backspace and would force them to
			//use the BACK button provided.
      var formElement = false;

			//get the form
			var testForm = document.forms[0];

			//if the page actually had a form, go in the loop.
			if (testForm != null)
			{
				for(i = 0; i < document.forms[0].elements.length; i++)
				{
					 if(document.forms[0].elements[i].name == document.activeElement.name)
					 {
							formElement = true;
							break;
					 }
				}
			}
			//else check if the activeElement is a Text or TextArea that is part of the body
			else if (document.activeElement.type == "text" || document.activeElement.type == "textarea")
			{
				formElement = true;
			}


      var searchBox = document.getElementById('numberEntry');
      if(searchBox != null){
        if(document.getElementById('numberEntry').id == document.activeElement.id){
          formElement = true;
        }
      }

      if(formElement == false || (document.activeElement.type != "text" && document.activeElement.type != "textarea" && document.activeElement.type != "password")){
         window.event.cancelBubble = true;
         window.event.returnValue = false;
         return false;
      }
   }

   // F5 and F11
   if (window.event.keyCode == 122 || window.event.keyCode == 116){
      window.event.keyCode = 0;
      event.returnValue = false;
      return false;
   }

   // Alt + Left Arrow
   if(window.event.altLeft){
      window.event.returnValue = false;
      return false;
   }

   // Alt + Right Arrow
   if(window.event.altKey){
      window.event.returnValue = false;
      return false;
   }
}

/*
  Catch all numeric keypresses and send them to the search box.
*/
function evaluateKeyPress()
{
  var a;
  var keypress = window.event.keyCode;

  if (keypress == 48||keypress==96)
    a = 0;
  if (keypress == 49||keypress==97)
    a = 1;
  if (keypress == 50||keypress==98)
    a = 2;
  if (keypress == 51||keypress==99)
    a = 3;
  if (keypress == 52||keypress==100)
    a = 4;
  if (keypress == 53||keypress==101)
    a = 5;
  if (keypress == 54||keypress==102)
    a = 6;
  if (keypress == 55||keypress==103)
    a = 7;
  if (keypress == 56||keypress==104)
    a = 8;
  if (keypress == 57||keypress==105)
    a = 9;

  if (a >= 0 && a <= 9)
  {
    document.getElementById("numberEntry").focus();
  }
}

function doEnterKeyAction(functionToCall) {
  if (event.keyCode == 13)
    functionToCall();
}


/*
   Returns the style object for the given element's field name.
*/
function getStyleObject(fieldName)
{
   if (document.getElementById && document.getElementById(fieldName))
      return document.getElementById(fieldName).style;
}

/*
   Limits input to digits only.
*/
function digitOnlyListener()
{
   var input = event.keyCode;
   if (input < 48 || input > 57){
       event.returnValue = false;
   }
}

/*
   The limitTextarea will squelch any characters that exceed the limit value included in the method call.
   textarea - the textarea object
   limit - the number of characters allowed
*/
function limitTextarea(textarea, limit)
{
   if (textarea.value.length > limit)
      textarea.value = textarea.value.substring(0, limit);
}

/*
   The following function changes the control's style sheet to 'errorField'.
   elements - an Array of control names on the form
*/
function setErrorFields(elements){
    var element;
    for (var i = 0; i < elements.length; i++){
        element = document.getElementById(elements[i]);
        if (element != null){
            element.className = "errorField";
        }
    }
}
function setErrorField(elementId){
    var element = document.getElementById(elementId);
    if (element != null){
        element.className = "errorField";
    }
}

/*
    The following functions change the cursor UI when an image triggers a 'mouseover' event.

    elements - an Array of control names on the form
*/
function addImageCursorListener(elements){
    var element;
    for (var i = 0; i < elements.length; i++){
        element = document.getElementById(elements[i]);
        if (element != null){
            //element.attachEvent("onmouseover", imageOver);
            //element.attachEvent("onmouseout", imageOut);
            addEvent(element, "onmouseover", imageOver, false);
            addEvent(element, "onmouseout", imageOut, false);
        }
    }
}
function imageOver(){
    document.forms[0].style.cursor = "hand";
}
function imageOut(){
    document.forms[0].style.cursor = "default";
}

/*
    The following functions are used to dis/enable fields.

    elements - an Array of control names on the form
    access - boolean value used to set the disabled property
*/
function disableFields(elements){
    setFieldsAccess(elements, true)
}
function enableFields(elements){
    setFieldsAccess(elements, false)
}
function setFieldsAccess(elements, access){
    var element;
    for (var i = 0; i < elements.length; i++){
        element = document.getElementById(elements[i]);
        if (element != null){
            setFieldAccess(element, access)
        }
    }
}
function setFieldAccess(element, access){
   element.disabled = access;
}

/*
   The following functions are used to show and hide any
   select boxes that would normally appear in front of a popup.
*/
function getAbsolutePos(element) {
   var r = { x: element.offsetLeft, y: element.offsetTop };
   if (element.offsetParent) {
      var tmp = getAbsolutePos(element.offsetParent);
      r.x += tmp.x;
      r.y += tmp.y;
   }
   return r;
}
function hideShowCovered(popup){
   function contains(a, b) {
      while (b.parentNode)
         if ((b = b.parentNode) == a)
            return true;
      return false;
   };

   var p = getAbsolutePos(popup);
   var EX1 = p.x;
   var EX2 = popup.offsetWidth + EX1;
   var EY1 = p.y;
   var EY2 = popup.offsetHeight + EY1;

   var tagsToHide = new Array("select");
   for (var k = 0; k < tagsToHide.length; k++) {
      var tags = document.getElementsByTagName(tagsToHide[k]);
      var tag = null;

      for (var i = 0; i < tags.length; i++) {
         tag = tags[i];
         if (!contains(this.div, tag)){
            p = getAbsolutePos(tag);
            var CX1 = p.x;
            var CX2 = tag.offsetWidth + CX1;
            var CY1 = p.y;
            var CY2 = tag.offsetHeight + CY1;

            if ( (CX1 > EX2) || (CX2 < EX1) || (CY1 > EY2) || (CY2 < EY1) ) {
               if (!tag.saveVisibility) {
                  tag.saveVisibility = tag.currentStyle["visibility"];
               }
               tag.style.visibility = tag.saveVisibility;
            } else {
               if (!tag.saveVisibility) {
                  tag.saveVisibility =  tag.currentStyle["visibility"];
               }
               tag.style.visibility = "hidden";
            }
         }
      }
   }
}

/*
  The following methods are used to display a wait message to the user
  while an action is being executed.  The following coding convention should
  be used for the wait div's table structure:

  waitDiv - id of the wait div
  waitMessage - id of the TD which will contain the message
  waitTD - id of the TD which will contain the dots (...)

  Parameters
  content - The div id containing the content to be hidden
  wait - The div id containing the wait message
  message - Optional, if provided the message to be displayed, otherwise, "Processing"
            will be the message
*/
function showWaitMessage(contentId, wait, message){
  window.scrollTo(0,0);
  var content = document.getElementById(contentId);
  var height = content.offsetHeight;
  var waitDiv = document.getElementById(wait + "Div").style;
  var waitMessage = document.getElementById(wait + "Message");
  _waitTD = document.getElementById(wait + "TD");

  content.style.display = "none";
  waitDiv.display = "block";
  waitDiv.height = height;
  waitMessage.innerHTML = (message) ? message : "Processing";
  clearWaitMessage();
  updateWaitMessage();
}

var _waitTD = "";
function clearWaitMessage(){
   _waitTD.innerHTML = "";
   setTimeout("clearWaitMessage()", 5000);
}
function updateWaitMessage(){
   setTimeout("updateWaitMessage()", 1000);
   setTimeout("showPeriod()", 1000);
}
function showPeriod(){
   _waitTD.innerHTML += "&nbsp;.";
}
function hideWaitMessage(contentId, wait){
   var content = document.getElementById(contentId);
   var waitDiv = document.getElementById(wait + "Div").style;
   content.style.display = "block";
   waitDiv.display = "none";
   clearTimeout("clearWaitMessage()");
   clearTimeout("updateWaitMessage()");
   clearTimeout("showPeriod()");
}

/*
   This is a helper function to return the two security parameters
   required by the security framework.

   areOnlyParams -   is used to change the url string (? vs. &) depending if these
                     are the only parameters in the url
*/
function getSecurityParams(areOnlyParams){
   return   ((areOnlyParams) ? "?" : "&") +
            "securitytoken=" + document.getElementById("securitytoken").value +
            "&context=" + document.getElementById("context").value +
            "&adminAction=" + document.getElementById("adminAction").value;
}
/*
  This function is used to clear all form fields on a page.
*/
function clearAllFields(form){

   for(var i = 0; i < form.elements.length; i++){
  if(form.elements[i].type == 'radio')
    form.elements[i].checked = false;
  if(form.elements[i].type == 'text')
    form.elements[i].value = "";
  if(form.elements[i].type == 'checkbox')
    form.elements[i].checked = false;
   }
}



/***********************************************************************************
 doAccountSearch()
 This function is called from the orderShoppingCrat.xsl
 which is part of the customerAccount / customerAccountDetail.xsl files
************************************************************************************/
function doAccountSearch(orderNumber, buyerId){
  var guid = document.getElementById('guid_' + orderNumber).value;
  var url = "customerOrderSearch.do" +
      "?action=customer_account_search" + "&order_number=" + orderNumber
         + "&order_guid=" + guid + "&customer_id=" + buyerId;
  performAction(url);
}



/***********************************************************************************
  doReturnEmailAddresses()
  Called from the customerAccount page.Used to display additional email addresses
  tied to a customer
************************************************************************************/
function doReturnEmailAddresses(){
  var url_source = "customerOrderSearch.do" + "?action=more_customer_emails" +
      "&customer_id=" + cust_id + getSecurityParams(false);
    var modal_dim = "dialogWidth:775px; dialogHeight:355px; center:yes; status=0; help:no; scroll:no";
  showModalDialogIFrame(url_source, "", modal_dim);
  //sourceCodeWindow = window.open("customerEmailAddresses.html","testPage","width=700,height=200,location=no,status=no,toolbar=no,top=130,left=40,titlebar=no,title=no");
}

/*
  The following functions are triggered by buttons which are found on both the Customer Account
  page and the Customer Shopping Cart page.
*/
function doAccountHistoryAction(){
  var url = "loadHistory.do?page_position=1" +
    "&history_type=Customer" + "&customer_id=" + document.csci.customer_id.value +
    getSecurityParams(false);

  performAction(url);
}



 function doHoldDetailAction(frompage,orderdetailid,externalOrderNumber){

  var url = "CustomerHoldAction.do" +
       "?action_type=loadcustomer&from_page=" + frompage  + "&order_detail_id=" + orderdetailid + "&external_order_number=" + externalOrderNumber + getSecurityParams(false);
  performAction(url);
 }


 function doHoldAction(frompage){

  var obj =document.getElementById("recipient_search");

  var flag = "";
  if(obj != null){
    flag = obj.value;
    }


  var url = "CustomerHoldAction.do" +
       "?action_type=loadcustomer&from_page=" + frompage  + "&recipient_search=" + flag + getSecurityParams(false);


  performAction(url);
}

/*
Retrieves the fields based on the fieldParms
array defined at the top of the shopping cart page.
*/
function retrievePopupParms(){
  var url = '';
  var ele;
  for(var i = 0; i < fieldParms.length; i++){
    ele = document.getElementById(fieldParms[i]);
    url += '&uc_' + ele.name + '=' + ele.value;
  }
 return url;
}


/*
  Retrieve Timer parms
  Used to attach them to a URL
*/
function getTimerParams(){
  var timerParms = '';
    timerParms += "&t_call_log_id=" + document.getElementById("t_call_log_id").value;
    timerParms += "&t_entity_history_id=" + document.getElementById("t_entity_history_id").value;
    timerParms += "&t_comment_origin_type=" + document.getElementById("t_comment_origin_type").value;

 return timerParms;

}

function Trim(value)
{
  return value.replace(/^\s+/,'').replace(/\s+$/,'');
}




/*
Remove any leading zeros from the search number
*/
function stripZeros(innum){
   var newTerm
   while (num.charAt(0) == "0") {
         newTerm = num.substring(1, num.length);
         num = newTerm;
   }
   if (num == "")
       num = "0";
       return num;
}


/* Validates the form using the supplied multi-dimensional array.
 * The array values should be as follows:
 *
 * 0 = name attribute of input
 * 1 = the error message to be displayed
 * 2 = the function name used to perform validation
 * 3 = any additional function params needed to perform validation
 *
 * @param matrix  The multi-dimensional array used to validate the form.
 */
var INPUT = 0;
var MESSAGE = 1;
var FUNCTION = 2;
var FUNCTION_PARAMS = 3;
var PREV_ERRORS = new Array();
function validateForm(matrix){
  validateFormResetPrevErrors();

  var errorInput, errorRow, errorCell, isValid = true;
  for (var i = 0; i < matrix.length; i++) {
    errorInput = document.getElementById(matrix[i][INPUT]);
    errorCell = document.getElementById(matrix[i][INPUT] + "_validation_message_cell");
    errorRow = document.getElementById(matrix[i][INPUT] + "_validation_message_row");

    // loop through array calling validation functions
    if ( !matrix[i][FUNCTION](matrix[i][INPUT], matrix[i][FUNCTION_PARAMS]) ) {
      PREV_ERRORS.push(matrix[i][INPUT]);
      errorInput.className = "errorField";
      if ( matrix[i][MESSAGE] != "" ) {
        errorCell.innerText = matrix[i][MESSAGE];
      }
      errorRow.style.display = "block";
      isValid = false;
    }
  }
  return isValid;
}

/* This method is to assist with Server side validation error messages.  The goal was to
 * reuse validateForm() so a generic pass-thru function was needed.
 *
 * @param input     the input name attribute
 * @param condition true or false
 */
function validateFormHelper(input, condition){
  return condition;
}

/* Resets any fields which were invalid when calling validateForm.  This is typically called
 * before calling validateForm() for the second time as to reset the error inputs.
 */
function validateFormResetPrevErrors(){
  for (var i = 0; i < PREV_ERRORS.length; i++) {
    // reset pink background
    errorInput = document.getElementById(PREV_ERRORS[i]);
    errorInput.className = "";

    // hide error message
    errorCell = document.getElementById(PREV_ERRORS[i] + "_validation_message_cell");
    errorRow = document.getElementById(PREV_ERRORS[i] + "_validation_message_row");
    errorRow.style.display = "none";
  }
  PREV_ERRORS = new Array();
}

/* Determines if the supplied input is a valid US or international
 * phone number.  The emptyOk default is false.  See FormChek.js.
 *
 * @param inputName The input name to validate.
 * @param emptyOk   true if empty values are valid, false if they are not
 * @return true if valid US or international phone number, otherwise false
 */
function validatePhone(inputName, emptyOk){
  emptyOk = (validatePhone.arguments.length == 1) ? false : emptyOk;
  var phone = document.getElementById(inputName);
  return checkUSPhone(phone, emptyOk) || checkInternationalPhone(phone, emptyOk);
}

/* Validates for valid US, Candian, or International zip codes.
 * Valid Formats:
 *    US - NNNNN{-NNNN}
 *    CA - ANA-NAN || ANA
 *    Inter - NNNNNNNNNNNN
 *
 * The emptyOk default is false.
 *
 * @param inputName The input name to validate.
 * @param emptyOk   true if empty values are valid, false if they are not
 * @return true if the zip code is a valid US, Candian, or international zip
 *         code, otherwise false
 */
function validateZipCode(inputName, countryList, emptyOk){
  emptyOk = (validateZipCode.arguments.length != 3) ? false : emptyOk;
  var zipCode = document.getElementById(inputName).value;
  var countryBox = document.getElementById(countryList);
  var selectedCountry = countryBox.options[countryBox.selectedIndex].value;
  var isDomestic = ( selectedCountry == 'US' || selectedCountry == 'CA' );

  var re = /^\d{5}([\-])?(\d{4})?$/;
  var can = /^\s*[a-ceghj-npr-tvxy]|[A-CEGHJ-NPR-TVXY]\d[a-z]|[A-Z](\s)?\d[a-z]|[A-Z]\d\s*$/i;
  var inter = /[a-z]|[A-Z]|[0-9](\s+)?/;

  if( isDomestic ) {
    return ( re.test(zipCode) || can.test(zipCode) );
  }
  return  inter.test(zipCode) || isWhitespace(zipCode);
}


/* Determines if any of the form's default values have changed.  Any tags that should
 * not be checked can use the attribute 'onchangeIgnore=true' to bypass the check.
 *
 * @return true if any of the form's default values have changed, otherwise false
 * @see hasInputChanged(obj)
 */
function hasFormChanged(){
  var hasFormBeenPreviouslyModified = document.getElementById("modify_order_updated");
  var hasChanged = ( hasFormBeenPreviouslyModified && hasFormBeenPreviouslyModified.value == "y" );

  if ( !hasChanged ) {
    // define tags to test
    var inputTypes = new Array("input", "select", "textarea");
    for (var k = 0; k < inputTypes.length && !hasChanged; k++){

      // loop through all tags of a specific type
      var inputs = document.getElementsByTagName(inputTypes[k]);
      for (var i = 0; i < inputs.length && !hasChanged; i++){
        // skip tags with attribute: 'onchangeIgnore=true'
        if ( !inputs[i].onchangeIgnore ){
 		      // unformat tags with attribute: 'onchangeUnformat=true'
       		if (inputs[i].onchangeUnformat)
       		{
						var bag = ",/.<>?;:\|[]{}~!@#$%^&*()-_=+`" + "\\\'";
						inputs[i].value = stripWhitespace(inputs[i].value);
						inputs[i].value = stripCharsInBag(inputs[i].value, bag);
       		}
      		hasChanged = hasInputChanged(inputs[i]);
        }
      }
    }
  }
  return hasChanged;
}

/* Determines if an input's default value has changed.
 *
 * @param input The object to test
 * @return true if the input's value is different from the default value, otherwise false
 */
function hasInputChanged(input){
  var inputType = input.type.toString();
  if ( inputType == "text" || inputType == "textarea" || inputType == "hidden" ) {
    if ( input.defaultValue != input.value ){
      return true;
    }
  }
  else if ( inputType.indexOf("select") > -1) {

    if (input.options.length > 1) {
      var defaultSelected, found = false;
      for ( var i = 0; i < input.options.length && !found; i++){
        if ( input.options[i].defaultSelected ) {
          defaultSelected = i;
          found = true;
        }
      }
      // no default was set so see if the first option is still selected
      if ( !found && !input[0].selected ) {
        return true;
      }
      // default was found, check to see if it matches the current selection
      else if ( found && defaultSelected != input.selectedIndex ) {
        return true;
      }
      else {
        return false;
      }
    }
    // select box contains no options, just return false
    else {
      return false;
    }
  }
  else if ( inputType == "checkbox" || inputType == "radio") {
    if (input.defaultChecked != input.checked) {
      return true;
    }
  }
  return false;
}


/**
 * Returns all the form elements in query string format.
 * ex.  ?input1=value1&input2=value2
 *
 * @param formObj The form object to gather name, value pairs for
 */
function getFormNameValueQueryString(formObj){
   var temp, toReturn = "";
   var inputs = formObj.elements;
   for (var i = 0; i < inputs.length; i++){
      temp = getNameValuePair(inputs[i]);
      if ( temp != "" ) {
         if ( i == 0 ) {
            toReturn += "?";
         }
         else {
            toReturn += "&";
         }
         toReturn += temp;
      }
   }
   return toReturn;
}

/**
 * Returns the input's name and value attributes delimited by the
 * equals sign (=).
 *
 * @param input The input object to get the name and value attributes of
 */
function getNameValuePair(input){
   var inputType = input.type.toString();
   if ( inputType == "text" || inputType == "textarea" || inputType == "hidden" ) {
      return input.name + "=" + input.value;
   }
   else if ( inputType.indexOf("select") > -1) {
      if ( input.selectedIndex > -1 )  return input.name + "=" + input[input.selectedIndex].value;
      else                             return "";
   }
   else if ( inputType == "checkbox" || inputType == "radio") {
      if ( input.checked ) return input.name + "=" + input.value;
      else                 return "";
   }
   else {
      return "";
   }
}

/*
 *  Show a modal dialog with an Iframe and put the url in the
 *  Iframe src.  This allows the url to go through an XSLT transform.
 */
function showModalDialogIFrame(url,vArguements,sFeatures)
{
  var iframeURL = "modalDialog.html";
  var imbeddedURL = new Object();
  imbeddedURL.dialogURL = url;

  return window.showModalDialog(iframeURL, imbeddedURL ,sFeatures);
}

function submitForm(action)
{
	//alert("ProductId : "+ document.getElementById("productId").value);	
    document.forms[0].action=action;    
    document.forms[0].submit();
}

function trimString(inString) {
  var outString;
  
  if( inString == undefined ) {
    outString = "";
  } else {
    outString = inString.replace( /^\s+/g, "" );// strip leading
    outString = outString.replace( /\s+$/g, "" );// strip trailing
  };
  
  return outString;
}

function validateTextarea(obj, maxlength) {
  
  if(obj.value.length > maxlength) {
    alert("Order Entry Keyword Search cannot be longer than " + maxlength + " characters.  Data has been truncated to " + maxlength + " characters.");
    obj.value = obj.value.substring(0, maxlength);
  }
}

// Checks for a blank or empty string
function isEmptyString(inString) {
  var tempString = trimString(inString);  
  if(tempString.length > 0){
     return false;
   }
   return true;
}  

// Returns the Text associated with an option in the passed in Option list
// Returns an empty string if no text found
function getOptionText(optionList, value) {
  
  for(var i=0; i < optionList.length; i++) {
    if(optionList.options[i].value == value) {
      return optionList.options[i].text;
    }
  }
  
  return '';
}

// Selects the option in the Option List based on the Value
function setSelectedOption(optionList, value) {
  for(var i=0; i < optionList.length; i++) {
    if(optionList.options[i].value == value) {
      optionList.options[i].selected = true;
    }
  }
}


// Enable/Disabled Simple HTML Tags such as Anchor Tags and Image Tags
// That utilize href or onclick attributes for processing
function disableBasicHtmlTag(obj, disable){
  if(disable){	
  
    if(obj.tagName.toUpperCase() == 'A') {
      var href = obj.getAttribute("href");
      if(href && href != "" && href != null){
         obj.setAttribute('href_bak', href);
      }
      obj.removeAttribute('href');
    }
	
    var onclick = obj.onclick;
    if(onclick && onclick != "" && onclick != null){
      obj.onclick_bak = onclick;
      obj.onclick = null;
    }

    obj.style.color="gray";
    obj.disabled=true;
  }
  else{    
    if(obj.tagName.toUpperCase() == 'A') {
      if(!isEmptyString(obj.getAttribute('href_bak'))) {
        obj.setAttribute('href', obj.getAttribute('href_bak'));
        obj.removeAttribute("href_bak");
      }
    }
    
    if( ! (obj.onclick_bak == undefined)) {
      obj.onclick = obj.onclick_bak;
      obj.onclick_bak = null;
    }
    
    obj.style.color="blue";
    obj.disabled=false;

  }
}
