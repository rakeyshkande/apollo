function parseXmlString(xmlString)
{
    var xmlDoc;
    // code for IE
    if (window.ActiveXObject)
    {
        xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
        xmlDoc.async=false;
        xmlDoc.loadXML(xmlString);
      }
    // code for Mozilla, etc.
    else if (document.implementation && document.implementation.createDocument)
    {
        var parser=new DOMParser();
        xmlDoc=parser.parseFromString(xmlString,"text/xml");
    }
    else
    {
        alert('Your browser cannot handle this script');
    }
    
    return xmlDoc;
}

function parseXmlFile(fileName)
{
    var xmlDoc;
    // code for IE
    if (window.ActiveXObject)
    {
        xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
        xmlDoc.async=false;
        xmlDoc.load(fileName);
      }
    // code for Mozilla, etc.
    else if (document.implementation && document.implementation.createDocument)
    {
        xmlDoc= document.implementation.createDocument("","",null);
        xmlDoc.load(fileName);
    }
    else
    {
        alert('Your browser cannot handle this script');
    }
    
    return xmlDoc;
}

function createXmlDocument(rootTagName, namespaceURL) { 
    if (!rootTagName) rootTagName = ""; 
    if (!namespaceURL) namespaceURL = ""; 
    if (document.implementation && document.implementation.createDocument) { 
        // This is the W3C standard way to do it 
        return document.implementation.createDocument(namespaceURL, rootTagName, null); 
    } 
    else 
    { 
        // This is the IE way to do it 
        // Create an empty document as an ActiveX object 
	// If there is no root element, this is all we have to do 
	var doc = new ActiveXObject("MSXML2.DOMDocument"); 
	// If there is a root tag, initialize the document 
	if (rootTagName) { 
            // Look for a namespace prefix 
            var prefix = ""; 
            var tagname = rootTagName; 
            var p = rootTagName.indexOf(':'); 
            if (p != -1) { 
                prefix = rootTagName.substring(0, p); 
                tagname = rootTagName.substring(p+1); 
            } 
            // If we have a namespace, we must have a namespace prefix 
            // If we don't have a namespace, we discard any prefix 
            if (namespaceURL) { 
                if (!prefix) prefix = "a0"; // What Firefox uses 
            } 
            else 
            {
                prefix = "";
            }
            // Create the root element (with optional namespace) as a 
            // string of text 
            var text = "<" + (prefix?(prefix+":"):"") +  tagname + 
            (namespaceURL ?(" xmlns:" + prefix + '="' + namespaceURL +'"') :"") + "/>"; 
            // And parse that text into the empty document 
            doc.loadXML(text); 
        }
        
        return doc; 
    }
}

function selectNodeText(xmlNode,tagName) {
    return xmlNode.getElementsByTagName(tagName)[0].firstChild.nodeValue;
}

function getXMLNodeSerialization(xmlNode) {
  var text = false;
  try {
    // Gecko-based browsers, Safari, Opera.
    var serializer = new XMLSerializer();
    text = serializer.serializeToString(xmlNode);
  }
  catch (e) {
    try {
      // Internet Explorer.
      text = xmlNode.xml;
    }
    catch (e) {}
  }
  return text;
}
