
// Global Fields that are always Enabled
var v_EnabledFields_Always = [
'Id=toggleTabsLink',
'Id=category',
'Name=btnUpdateProduct',
'Name=btnSpellCheck',
'Name=copyBtn',
'Name=productId',
'Id=pquadProductID',
'Name=personalizationCaseFlag',
'Name=allAlphaFlag'
];


// Default Constants
// Fields Enabled by Default on the Page (No Options Selected)
var v_EnabledFields_Default = [
'Id=addOnBalloonsFlag',
'Id=addOnBearsFlag',
'Id=addOnChocolateFlag',
'Id=addOnFuneralFlag',
'Id=addOnGreetingCardsFlag',
'Id=add_on_product_display_sequence_number',
'Id=add_on_product_max_quantity',
'Id=add_on_vase_product_display_sequence_number',
'Id=arrangementColorsArray',
'Id=arrangementSize',
'Id=availableComponentSkuArray',
'Id=boxId',
'Id=codifiedFlag',
'Id=colorSizeFlag',
'Id=companyList',
'Id=componentSkuArray',
'Id=corporateSiteFlag',
'Id=country',
'Id=defaultPrice1',
'Id=defaultPrice2',
'Id=defaultPrice3',
'Id=deluxeRecipe',
'Id=gbbPopoverFlag',
'Id=gbbTitle',
'Id=gbbNameOverrideFlag1',
'Id=gbbNameOverrideText1',
'Id=gbbPriceOverrideFlag1',
'Id=gbbPriceOverrideText1',
'Id=gbbNameOverrideFlag2',
'Id=gbbNameOverrideText2',
'Id=gbbPriceOverrideFlag2',
'Id=gbbPriceOverrideText2',
'Id=gbbNameOverrideFlag3',
'Id=gbbNameOverrideText3',
'Id=gbbPriceOverrideFlag3',
'Id=gbbPriceOverrideText3',
'Id=holdUntilAvailable',
'Id=deliveryDayFriday',
'Id=deliveryDayMonday',
'Id=deliveryDaySaturday',
'Id=deliveryDaySunday',
'Id=deliveryDayThursday',
'Id=deliveryDayTuesday',
'Id=deliveryDayWednesday',
'Id=deliveryType',
'Id=deluxePrice',
'Id=departmentCode',
'Id=dimWeight',
'Id=discountAllowedFlag',
'Id=dominantFlowers',
'Id=eGiftFlag',
'Id=exceptionCode',
'Id=exceptionMessage',
'Id=excludedColorsArray',
'Id=expressShippingOnly',
'Id=floristReferenceNumber',
'Id=generalComments',
'Id=itemComments',
'Id=keywordSearch',
'Id=keywords',
'Id=longDescription',
'Id=mercuryDescription',
'Id=mondayDeliveryFreshCuts',
'Id=novatorId',
'Id=novatorName',
'Id=novatorTagList',
'Id=personalizationLeadDays',
'Id=personalizationTemplate',
'Id=personalizationTemplateOrder',
'Id=ftdwestAccessoryId',
'Id=ftdwestTemplateMapping',
'Id=premiumPrice',
'Id=premiumRecipe',
'Id=productName',
'Id=productType',
'Id=recipe',
'Id=recipientSearch',
'Id=royaltyPercent',
'Id=searchPriority',
'Id=secondChoice',
'Id=sentToNovatorContent',
'Id=sentToNovatorProd',
'Id=sentToNovatorTest',
'Id=sentToNovatorUAT',
'Id=shipDayFriday',
'Id=shipDayMonday',
'Id=shipDaySaturday',
'Id=shipDaySunday',
'Id=shipDayThursday',
'Id=shipDayTuesday',
'Id=shipDayWednesday',
'Id=shipMethodFlorist',
'Id=shippingKey',
'Id=shippingSystem',
'Id=standardPrice',
'Id=status',
'Id=subCodeAddButton',
'Id=subCodeCommitButton',
'Id=subCodeRemoveButton',
'Id=supplyExpense',
'Id=taxFlag',
'Id=allowFreeShippingFlag',
'Id=morningDeliveryFlag',
'Id=unspscCode',
'Id=bulletDescription',
'Id=variablePriceMax',
'Id=variablePricingFlag',
'Id=vendorAddButton',
'Id=vendorListId',
'Id=weboeBlocked',
'Id=websites',
'Id=markerSelectBoxExcluded',
'Id=markerSelectBoxIncluded',
'Name=add_on_active_flag',
'Name=add_on_vase_active_flag',
'Name=customFlag',
'Name=over21Flag',
'Name=personalGreetingFlag',
'Name=premierCollectionFlag',
'Name=productSubType',
'Name=sendDeluxeRecipe',
'Name=sendPremiumRecipe',
'Name=sendStandardRecipe',
'Name=shipMethodCarrier',
'Name=shippingMethodList',
'Name=zoneJumpEligibleFlag',
'Id=startDateCalLink',
'Id=startDateClearLink',
'Id=endDateCalLink',
'Id=endDateClearLink',
'Id=removeVendorLink',
'Id=flipCheckboxesInTableRowLink',
'Id=ddExclusionLink',
'Id=supplyExpenseEffDateCalLink',
'Id=royaltyPercentEffDateCalLink',
'Id=colorsArrayButton',
'Id=showComponentArrowButton',
'Id=supplyExpenseEffDateCalClearLink',
'Id=royaltyPercentEffDateClearLink',
'Id=vendorAvailableCheckbox',
'Id=vendorCost',
'Id=vendorSKU',
'Id=azCatalogStatusStandard',
'Id=azProductNameStandard',
'Id=azProductDescriptionStandard',
'Id=azCatalogStatusDeluxe',
'Id=azProductNameDeluxe',
'Id=azProductDescriptionDeluxe',
'Id=azCatalogStatusPremium',
'Id=azProductNamePremium',
'Id=azProductDescriptionPremium',
'Id=azBulletPoint1',
'Id=azBulletPoint2',
'Id=azBulletPoint3',
'Id=azBulletPoint4',
'Id=azBulletPoint5',
'Id=azSearchTerm1',
'Id=azSearchTerm2',
'Id=azSearchTerm3',
'Id=azSearchTerm4',
'Id=azSearchTerm5',
'Id=azIntendedUse1',
'Id=azIntendedUse2',
'Id=azIntendedUse3',
'Id=azIntendedUse4',
'Id=azIntendedUse5',
'Id=azTargetAudience1',
'Id=azTargetAudience2',
'Id=azTargetAudience3',
'Id=azTargetAudience4',
'Id=azOtherAttribute1',
'Id=azOtherAttribute2',
'Id=azOtherAttribute3',
'Id=azOtherAttribute4',
'Id=azOtherAttribute5',
'Id=azSubjectMatter1',
'Id=azSubjectMatter2',
'Id=azSubjectMatter3',
'Id=azSubjectMatter4',
'Id=azSubjectMatter5',
'Id=azItemType',
'Name=copyBtn'
];

var v_FieldDefaultDisabledValues = [
{ id: 'duration', defaultValue: '', visibility: 'visible'}
];

var v_FieldDefaultEnabledValues = [
{ id: 'weboeBlocked', defaultValue: false, visibility: 'visible'},
{ id: 'taxFlag', defaultValue: false, visibility: 'visible'},
{ id: 'allowFreeShippingFlag', defaultValue: true, visibility: 'visible'}
];

// Free Shipping Constants
var v_EnabledFields_FREESHIP = [
'Id=productName',
'Id=novatorId',
'Id=novatorName',
'Id=departmentCode',
'Id=companyList',
'Id=longDescription',
'Id=standardPrice',
'Id=websites',
'Id=status',
'Id=discountAllowedFlag',
'Id=generalComments',
'Id=sentToNovatorProd',
'Id=sentToNovatorTest',
'Id=sentToNovatorUAT',
'Id=sentToNovatorContent',
'Id=duration'
];

// Wild card setting defaults text to blank and check box's to false. 
// Only list items that are not reset to defaults or require custom handling
var v_FieldDefaultDisabledValues_FREESHIP = [
{ id: 'weboeBlocked', defaultValue: true, visibility: 'visible'},
{ id: 'taxFlag', defaultValue: true, visibility: 'visible'},
{ id: 'deliveryType', defaultValue: 'D', visibility: 'visible'}, 
{ id: 'flipCheckboxesInTableRowLink', defaultValue: selectAll, visibility: 'visible'}, 
{ id: 'ddExclusionLink', defaultValue: selectAll, visibility: 'visible'}, 
{ id: 'excludedColorsArray', defaultValue: null, visibility: 'visible', script: function() {resetListBoxPair('arrangementColorsArray', 'excludedColorsArray');} },
{ id: 'availableComponentSkuArray', defaultValue: null, visibility: 'visible', script: function() {resetListBoxPair('componentSkuArray', 'availableComponentSkuArray');} }, 
{ id: 'personalizationTemplate', defaultValue: 'NONE', visibility: 'visible'}, 
{ id: 'vendorSKU', defaultValue:null, visibility: 'visible', script: function() {removeAllVendors();}},

// Wildcard Default for all other disabled items for Free Shipping is blank or false, no change to visibility
{ id: '*', defaultValue: '', visibility: null } 
];

var v_FieldDefaultEnabledValues_FREESHIP = [
{ id: 'duration', defaultValue: null, visibility: 'visible', script: function() {setDefaultEmptySelectValue('duration', '12');} }
];

// use a method in case there are more forms added, need to access by name
function getProductMaintDetailForm() {
  return document.forms[0];
}

// Used for Resetting the List Box Pairs (Moves all Items to the Left Box, and clears the selection)
function resetListBoxPair(ToListName, FromListName) {
  var ToList = $(ToListName);
  var FromList = $(FromListName);

  // Move all Items to the ToList
  btnMoveAllLeft_onclick(ToList, FromList);

  // Clear selections in the List Box Pair
  clearAllSelections(ToList);
  clearAllSelections(FromList);
}

// Set the default in a Select, only if the select value is blank
function setDefaultEmptySelectValue(selectName, value) {
  var element = $(selectName);

  var val = element.value;
  if(isEmptyString(val) == true) {
    setSelectedOption(element, value);  
  }
}


// Applies the passed in configuration to the Page
function setupFieldsForConfig(fieldNameArray, disabledFieldsValueArray, enabledFieldsValueArray) {
  var theForm = getProductMaintDetailForm();

  // Setup the Form Elements
  for ( var i = 0; i < theForm.elements.length; i++ ) {
    if(theForm.elements[i].type == "hidden" ||
       theForm.elements[i].name == 'productType' ||
       theForm.elements[i].name == 'productSubType' ) {
      continue; // Do not touch hidden fields, or type fields
    }

    setupFieldForConfig(theForm.elements[i], fieldNameArray, disabledFieldsValueArray, enabledFieldsValueArray);
  }
  
  // Setup Anchor Tags
  var anchorTags=document.getElementsByTagName('a');
	for(var i=0; i < anchorTags.length; i++) {
    if(isEmptyString(anchorTags[i].id) == false) {
      setupFieldForConfig(anchorTags[i], fieldNameArray, disabledFieldsValueArray, enabledFieldsValueArray);
    }
	}
  
  // Setup Image Tags
  var anchorTags=document.getElementsByTagName('img');
	for(var i=0; i < anchorTags.length; i++) {
    if(isEmptyString(anchorTags[i].id) == false) {
      setupFieldForConfig(anchorTags[i], fieldNameArray, disabledFieldsValueArray, enabledFieldsValueArray);
    }
	}  
}

// Configures the Element
function setupFieldForConfig(fieldElement, fieldNameArray, disabledFieldsValueArray, enabledFieldsValueArray) {
  var disabled = true;

  if(isEmptyString(fieldElement.id) == false 
     && (fieldNameArray.indexOf("Id=" + fieldElement.id) >= 0 || v_EnabledFields_Always.indexOf("Id=" + fieldElement.id) >= 0 )) {
      disabled = false;
  } else if(isEmptyString(fieldElement.name) == false 
            && (fieldNameArray.indexOf("Name=" + fieldElement.name) >= 0 || v_EnabledFields_Always.indexOf("Name=" + fieldElement.name) >= 0)) {
      disabled = false;
  }

  // check for default disabled values for add-ons and vases
  if (!disabled && fieldElement.name == 'add_on_active_flag') {
    if (arrayContains(disabledAddons, fieldElement.value)) {
      disabled = true;
    }
  } else if (!disabled && fieldElement.name == 'add_on_vase_active_flag') {
    if (arrayContains(disabledVases, fieldElement.value)) {
      disabled = true;
    }
  }
  
  var previousDisabled = fieldElement.disabled;
  
  if(fieldElement.tagName.toUpperCase() == 'A' || fieldElement.tagName.toUpperCase() == 'IMG') {
    disableBasicHtmlTag(fieldElement, disabled);  
  } else {
    fieldElement.disabled = disabled;
  }

  if(disabled) {
    setElementDefault(fieldElement, disabledFieldsValueArray);
  } else if(disabled == false && previousDisabled == true) {
    // Toggling from Disabled to Enabled, set the enabled defaults
    setElementDefault(fieldElement, enabledFieldsValueArray);
  }
}

// Applies the Default Value to the element if there is a default defined
function setElementDefault(element, elementDefaultValueArray) {
  for(var i = 0; i < elementDefaultValueArray.length; i++ ) {
    var defaultSettings = null;
    if(element.id==elementDefaultValueArray[i].id) {
      defaultSettings = elementDefaultValueArray[i];
    } else if(element.name == elementDefaultValueArray[i].name) {
      defaultSettings = elementDefaultValueArray[i];
    } else if(i == (elementDefaultValueArray.length - 1) && elementDefaultValueArray[i].id=='*' && (
      isEmptyString(element.id) == false || isEmptyString(element.name) == false)) {
      // use wild card default which is an optional item at the end of the array
      defaultSettings = elementDefaultValueArray[i]; // The last item in the list can be a wildcard
    }
    
    if(defaultSettings == null) {
      continue;
    }
    
    // Set the default for the Element
    if(! (defaultSettings.visibility === undefined) && defaultSettings.visibility != null) {
      element.style.visibility = defaultSettings.visibility;
    }
    
    // Invoke a Script if a script is defined
    if( ! (defaultSettings.script === undefined) && defaultSettings.script != null) {
      defaultSettings.script();
    } 
    
    if(! (defaultSettings.defaultValue === undefined) &&  defaultSettings.defaultValue != null) {
        if(element.type == 'checkbox') {
          if(defaultSettings.defaultValue == '') {
            element.checked = false; // Default blank to false for a checkbox
          } else {
            element.checked = defaultSettings.defaultValue;
          }
        } else if (element.type == 'text' || element.type == 'textarea') {
          element.value = defaultSettings.defaultValue;
        } else if (element.type == 'select-one') {
          setSelectedOption(element, defaultSettings.defaultValue);
          
          var val = element.value;
          if(val != defaultSettings.defaultValue && defaultSettings.defaultValue == '') {
            // There is no blank value in the select, select the first item in the list
            if(element.length > 0) {
              element.options[0].selected = true;
            }
          }
        } else if(element.type == 'select-multiple') {
          // Currently, only supports selecting a single item. 
          // Use script for more complex behavior
          clearAllSelections(element);
          setSelectedOption(element, defaultSettings.defaultValue);
        } else if (element.tagName.toUpperCase() == 'A' && defaultSettings.id != '*') { 
          element.innerHTML = defaultSettings.defaultValue; // Don't apply defaults to anchor tags since they have text or images in them
        }
    } 
    
    // If the Element has an onchange handler, call it as its value has 'changed'
    if( ! (element.onchange === undefined) && element.onchange != null) {
      element.onchange();
    }
    
    break;
  }
}

// Checks the Type and Subtype values and applies the Page Configuration to it
function setupFieldsForSubType() {
  var productType = document.getElementById("productType").value;
  var productSubType =  document.getElementById("productSubType").value;

  if(productType == 'SERVICES' && productSubType == 'FREESHIP') {
    setupFieldsForConfig(v_EnabledFields_FREESHIP, v_FieldDefaultDisabledValues_FREESHIP, v_FieldDefaultEnabledValues_FREESHIP);
  } else {
    setupFieldsForConfig(v_EnabledFields_Default, v_FieldDefaultDisabledValues, v_FieldDefaultEnabledValues);
  }
  
  // For Debugging
  // listFormEnabledFields();
}


// Called to process and display messages when product type value changes
function changeProductType() {
  var productTypeList = $("productType");
  var productSubTypeList = $("productSubType");
  var productType = productTypeList.value;
  var productSubType =  productSubTypeList.value;
         
  if(isEmptyString(currentProductType) == false
     && currentProductType != productType 
     && (currentProductType == 'SERVICES' || productType == 'SERVICES')) {
      var okToProceed = true;
     

      if(isEmptyString(productSubType) == true || currentProductType != 'SERVICES') {
        var newTypeText = getOptionText(productTypeList, productType);
        okToProceed = confirm('Are you sure you want to change this product to a Type: ' + newTypeText + ' product?');
      } else {
        var currentSubTypeText = getOptionText(productSubTypeList, productSubType);
        okToProceed = confirm('Are you sure you want to change this Sub-Type: ' + currentSubTypeText + ' product to another Type?');
      }
      
      if(!okToProceed) {
        //Revert Product Type Selection
        productTypeList.value=currentProductType;
        return;
      }
  }

  currentProductType = productType;     

  buildSubTypeDropdown(false); 
  changeAvailibility(); 
  chooseArrangementAvail(); 
  disableHoldUntilAvailable();
  //disableMorningDelivery4Floral();

}


// This is a helper function that just lists all the Fields that are enabled on the form
// usefull for building up test data and debugging
function listFormEnabledFields() {

  alert("Listing Form Enabled Fields");

  var theForm = getProductMaintDetailForm();
  
  var newwindow=window.open();
  newdocument=newwindow.document;

  for ( var i = 0; i < theForm.elements.length; i++ ) {
    if(theForm.elements[i].type == "hidden") {
      continue; // Do not touch hidden fields
    }
  
    if(theForm.elements[i].disabled == false) {
      if(isEmptyString(theForm.elements[i].id) == false) {
        newdocument.write("\nId=" + theForm.elements[i].id + ": Type=" + theForm.elements[i].type);
      } else if(isEmptyString(theForm.elements[i].name) == false) {
        newdocument.write("\nName=" + theForm.elements[i].name + ": Type=" + theForm.elements[i].type);
        
      }
    }
  }
  
  newdocument.close();
}


//****************************************************************************************
//Function:		btnMoveRightForMarker_onclick(FromList, ToList, ToTempList, FromTempList)
//
//Desc:			Moves all selected items from the Available listbox (FromList)
//				into the Selected listbox (ToList) & hidden listBox (ToTempList)
//				and also removes it from hidden listBox (FromTempList)
//              The two hidden list boxes act as clone(& backup) for the visible list boxes
//****************************************************************************************
function btnMoveRightForMarker_onclick(FromList, ToList, ToTempList, FromTempList) {
	var oOption = document.createElement("OPTION");
	var oOptionTemp = document.createElement("OPTION");
	var ItemLocation;
	var ItemLocationTemp;

	  changeFlag = true;

	  // Remove the selected items from the  hidden -'FromTempList'
	  // This is for 'FromTempList 'to be in sync with the 'FromList' 
	  for (var i=0; i < FromList.options.length; i++) {		
			if(FromList.options[i].selected) {
				for(var j=0; j < FromTempList.options.length; j++){
					if(FromList.options[i].value == FromTempList.options[j].value ){
						FromTempList.options.remove(j);
						j--;
						break;
					}
				}
				
			}
		}	
	  
	  for (var i=0; i < FromList.options.length; i++) {		
			if(FromList.options[i].selected) {
				oOption = document.createElement("OPTION")
				ItemLocation = GetItemLocation(FromList.options(i).text, ToList)
				oOption.text = FromList.options[i].text
				oOption.value = FromList.options[i].value
				ToList.options.add(oOption,ItemLocation)
				
                //Add it to 'ToTempList' along with 'ToList'
				// This is for 'ToTempList to be in sync with 'ToList' 
				oOptionTemp = document.createElement("OPTION")
				ItemLocationTemp = GetItemLocation(FromList.options(i).text, ToTempList)
				oOptionTemp.text = FromList.options[i].text
				oOptionTemp.value = FromList.options[i].value
				ToTempList.options.add(oOptionTemp,ItemLocationTemp)
				
				
				FromList.options.remove(i)
				i--
			}
		}		
		
	}
	
	//**************************************************************************
	//Function:		btnMoveLeftForMarker_onclick(FromList, ToList, FromTempList, ToTempList)
	//
	//Desc:			Moves all selected items from the Available listbox (FromList)
	//				into the Selected listbox (ToList) & hidden listbox (ToTempList)
	//				and also removes it from (FromList) * hidden listbox (FromTempList)
    //The two hidden list boxes act as clone(& backup) for the visible list boxes
	//**************************************************************************
	function btnMoveLeftForMarker_onclick(FromList, ToList, FromTempList, ToTempList) {
	var oOption = document.createElement("OPTION");
	var oOptionTemp = document.createElement("OPTION");
	var ItemLocation;
	var ItemLocationTemp;

	changeFlag = true;
	  
	  // Remove the selected items from the  'FromTempList'
	  // This is for 'FromTempList 'to be in sync with the 'FromList' 
	  for (var i=0; i < FromList.options.length; i++) {		
			if(FromList.options[i].selected) {
				for(var j=0; j < FromTempList.options.length; j++){
					if(FromList.options[i].value == FromTempList.options[j].value ){
						FromTempList.remove(j);
						j--;
						break;
					}
				}
			}
		}	
	  
		for (var i=0; i < FromList.options.length; i++) {
			if(FromList.options[i].selected) {
				oOption = document.createElement("OPTION")
				ItemLocation = GetItemLocation(FromList.options(i).text, ToList)
				oOption.text = FromList.options[i].text
				oOption.value = FromList.options[i].value
				ToList.options.add(oOption,ItemLocation)
				
				
				//Add it to 'ToTempList' along with 'ToList'
				// This is for 'ToTempList to be in sync with 'ToList' 
				oOptionTemp = document.createElement("OPTION")
				ItemLocationTemp = GetItemLocation(FromList.options(i).text, ToTempList)
				oOptionTemp.text = FromList.options[i].text
				oOptionTemp.value = FromList.options[i].value
				ToTempList.options.add(oOptionTemp,ItemLocationTemp)
				
				
				FromList.options.remove(i)
				i--
			}
		}		
	}

	
	
	
	//*********************************************************************************************
	// Function:		btnMoveRightForMarker_onclick(FromList, ToList, ToTempList, FromTempList)
	//
	// Desc:			Moves ALL items from the Available listbox (FromList)
    //					into the Selected listbox (ToList) & hidden listBox (ToTempList)
    //					and also removes it from hidden listBox (FromTempList)
    //	                The two hidden list boxes act as clone(& backup) for the visible list boxes
	//********************************************************************************************
	function btnMoveAllRightForMarker_onclick(FromList, ToList, ToTempList, FromTempList) {
		var oOption = document.createElement("OPTION");
		var oOptionTemp = document.createElement("OPTION");
		var ItemLocation;
		var ItemLocationTemp;

		  changeFlag = true;

		  // Remove the all items from the  hidden -'FromTempList'
		  // This is for 'FromTempList 'to be in sync with the 'FromList' 
		  		
		  for (var i=0; i < FromList.options.length; i++) {		
					for(var j=0; j < FromTempList.options.length; j++){
						if(FromList.options[i].value == FromTempList.options[j].value ){
							FromTempList.options.remove(j);
							j--;
							break;
						}
					}
					
		 }	
		  
		 for (var i=0; i < FromList.options.length; i++) {		
					oOption = document.createElement("OPTION")
					ItemLocation = GetItemLocation(FromList.options(i).text, ToList)
					oOption.text = FromList.options[i].text
					oOption.value = FromList.options[i].value
					ToList.options.add(oOption,ItemLocation)
					
	                //Add it to 'ToTempList' along with 'ToList'
					// This is for 'ToTempList to be in sync with 'ToList' 
					oOptionTemp = document.createElement("OPTION")
					ItemLocationTemp = GetItemLocation(FromList.options(i).text, ToTempList)
					oOptionTemp.text = FromList.options[i].text
					oOptionTemp.value = FromList.options[i].value
					ToTempList.options.add(oOptionTemp,ItemLocationTemp)
									
					FromList.options.remove(i)
					i--
		}		
			
	}
	
	
	
	//**********************************************************************************************
	// Function:		btnMoveAllLeftForMarker_onclick(FromList, ToList, FromTempList, ToTempList)
	//
	// Desc:			Moves ALL items from the Available listbox (FromList)
	//				    into the Selected listbox (ToList) & hidden listbox (ToTempList)
	//				    and also removes it from (FromList) * hidden listbox (FromTempList)
    //                  The two hidden list boxes act as clone(& backup) for the visible list boxes
	//*********************************************************************************************
	function btnMoveAllLeftForMarker_onclick(FromList, ToList, FromTempList, ToTempList) {
	var oOption = document.createElement("OPTION");
	var oOptionTemp = document.createElement("OPTION");
	var ItemLocation;
	var ItemLocationTemp;

	changeFlag = true;
	  
	    // Remove the selected items from the  'FromTempList'
	    // This is for 'FromTempList 'to be in sync with the 'FromList' 
		for (var i=0; i < FromList.options.length; i++) {		
				for(var j=0; j < FromTempList.options.length; j++){
					if(FromList.options[i].value == FromTempList.options[j].value ){
						FromTempList.remove(j);
						j--;
						break;
					}
				}
		}	
		
		for (var i=0; i < FromList.options.length; i++) {

				oOption = document.createElement("OPTION")
				ItemLocation = GetItemLocation(FromList.options(i).text, ToList)
				oOption.text = FromList.options[i].text
				oOption.value = FromList.options[i].value
				ToList.options.add(oOption,ItemLocation)
								
				//Add it to 'ToTempList' along with 'ToList'
				// This is for 'ToTempList to be in sync with 'ToList' 
				oOptionTemp = document.createElement("OPTION")
				ItemLocationTemp = GetItemLocation(FromList.options(i).text, ToTempList)
				oOptionTemp.text = FromList.options[i].text
				oOptionTemp.value = FromList.options[i].value
				ToTempList.options.add(oOptionTemp,ItemLocationTemp)
				
				FromList.options.remove(i)
				i--
		}		
		
	}

	
	
	// Based on the dropdown values selected from 'markerSelectBoxExcluded'
	// The listbox-FromList values are filtered
	function onselectMarkerTypeExcluded(FromList, OrigList) {

		 var selectBox = document.getElementById("markerSelectBoxExcluded");
		 var selectedValue = selectBox.options[selectBox.selectedIndex].value;	 
		 	 
		if(OrigList.options.length != FromList.options.length){
		
			for (var i=0; i < FromList.options.length; i++) {
				FromList.options.remove(i);
				i--;
			}
			var oOption = document.createElement("OPTION");
			var ItemLocation;
			for (var i=0; i < OrigList.options.length; i++) {
						oOption = document.createElement("OPTION")
						ItemLocation = GetItemLocation(OrigList.options(i).text, FromList)
						oOption.text = OrigList.options[i].text
						oOption.value = OrigList.options[i].value
						FromList.options.add(oOption,ItemLocation);
					
			}		
		}

		
		
		if(selectedValue == 'ALL'){
			
		}else if(selectedValue == 'color'){
			for (var i=0; i < FromList.options.length; i++) {	
				
				var val = String(FromList.options[i].value);
				if(!(val.substring(0,1) == 'C')){
					FromList.options.remove(i);
					i--;
				}
			}
		}else if(selectedValue == 'flower'){
			for (var i=0; i < FromList.options.length; i++) {
				var val = String(FromList.options[i].value);
				if(!(val.substring(0, 1) == 'F')){
					FromList.options.remove(i);
					i--;
				}
			}
			
		}else if(selectedValue == 'plant'){
			for (var i=0; i < FromList.options.length; i++) {
				var val = String(FromList.options[i].value);
				if(!(val.substring(0, 1) == 'P')){
					FromList.options.remove(i);
					i--;
				}
			}
			
		}else {
			for (var i=0; i < FromList.options.length; i++) {	
				var val = String(FromList.options[i].value);
				if(!(val.substring(0, 1) == 'G')){
					FromList.options.remove(i);
					i--;
				}
			}
			
		}	
		
	}


	//Based on the dropdown values selected from 'markerSelectBoxExcluded'
	//The listbox-FromList values are filtered
	function onselectMarkerTypeIncluded(FromList, OrigList) {

		 var selectBox = document.getElementById("markerSelectBoxIncluded");
		 var selectedValue = selectBox.options[selectBox.selectedIndex].value;	 
		 	 
		 
		if(OrigList.options.length >0){
			if(OrigList.options.length != FromList.options.length){
				
				for (var i=0; i < FromList.options.length; i++) {
					FromList.options.remove(i);
					i--;
				}
				var oOption = document.createElement("OPTION");
				var ItemLocation;
				for (var i=0; i < OrigList.options.length; i++) {
							oOption = document.createElement("OPTION")
							ItemLocation = GetItemLocation(OrigList.options(i).text, FromList)
							oOption.text = OrigList.options[i].text
							oOption.value = OrigList.options[i].value
							FromList.options.add(oOption,ItemLocation);
						
				}		
			}

			
			
			if(selectedValue == 'ALL'){
				
			}else if(selectedValue == 'color'){
				for (var i=0; i < FromList.options.length; i++) {	
					
					var val = String(FromList.options[i].value);
					if(!(val.substring(0,1) == 'C')){
						FromList.options.remove(i);
						i--;
					}
				}
			}else if(selectedValue == 'flower'){
				for (var i=0; i < FromList.options.length; i++) {
					var val = String(FromList.options[i].value);
					if(!(val.substring(0, 1) == 'F')){
						FromList.options.remove(i);
						i--;
					}
				}
				
			}else if(selectedValue == 'plant'){
				for (var i=0; i < FromList.options.length; i++) {
					var val = String(FromList.options[i].value);
					if(!(val.substring(0, 1) == 'P')){
						FromList.options.remove(i);
						i--;
					}
				}
				
			}else {
				for (var i=0; i < FromList.options.length; i++) {	
					var val = String(FromList.options[i].value);
					if(!(val.substring(0, 1) == 'G')){
						FromList.options.remove(i);
						i--;
					}
				}
				
			}	

			
		} 
		 
			
	}



