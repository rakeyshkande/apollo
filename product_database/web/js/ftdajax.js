/*
 *  Dependencies: js/prototype.js
 *                js/rico.js
 */

var FTD_AJAX = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || Rico.prototypeVersion < 1.3)
      throw("FTD Ajax Utility requires the Prototype JavaScript framework >= 1.3");
      
FTD_AJAX.ArrayExtensions = new Array();

if (Object.prototype.extend) {
   FTD_AJAX.ArrayExtensions[ Rico.ArrayExtensions.length ] = Object.prototype.extend;
}else{
  Object.prototype.extend = function(object) {
    return Object.extend.apply(this, [this, object]);
  }
  FTD_AJAX.ArrayExtensions[ Rico.ArrayExtensions.length ] = Object.prototype.extend;
}

if (Array.prototype.push) {
   FTD_AJAX.ArrayExtensions[ FTD_AJAX.ArrayExtensions.length ] = Array.prototype.push;
}

var FTD_AJAX_READY_STATE_UNINITIALIZED=0;
var FTD_AJAX_READY_STATE_LOADING=1;
var FTD_AJAX_READY_STATE_LOADED=2;
var FTD_AJAX_READY_STATE_INTERACTIVE=3;
var FTD_AJAX_READY_STATE_COMPLETE=4;

var FTD_AJAX_REQUEST_TYPE_GET='GET';
var FTD_AJAX_REQUEST_TYPE_POST='POST';

FTD_AJAX.Request = Class.create();

FTD_AJAX.Request.prototype = {
    initialize: function(url, requestType, callbackFunction, usePopup) {
        this.params                 = new Array();
        this.url                    = url;
        this.requestType            = requestType;
        this.callbackFunction       = callbackFunction;
        this.usePopup               = usePopup;
        this.requestObject          = this.getAjaxRequestObject();
        this.popupWindow            = null;
        
        if( usePopup == undefined ) {
            this.usePopup = false;
        } 
        
        this.addSecurityParams();
    },

    getAjaxRequestObject: function() {
        var xRequest=null;
        if (window.XMLHttpRequest) {
            xRequest=new XMLHttpRequest();	                //Mozilla, Firefox, Safari, etc
        }else if (typeof ActiveXObject != "undefined"){
            xRequest=new ActiveXObject("Microsoft.XMLHTTP");    //IE
        }
        return xRequest;
    },
    
    addParam: function(name,value) {
        if(!name || !value) {
            return;
        }
        
        this.params.push(name+'='+escape(value));
    },
    
    addSecurityParams: function() {
        if( securitytoken != undefined ) {
            this.addParam('securitytoken',securitytoken);
        }
        
        if( applicationcontext != undefined ) {
            this.addParam('applicationcontext',applicationcontext);
        }
        
        if( context != undefined ) {
            this.addParam('context',context);
        }
    },
    
    send: function() {
        if( this.requestObject ) {
            this.requestObject.open(this.requestType, this.url, true);
            this.requestObject.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        
            this.openPopup();
            
            var self = this;
            this.requestObject.onreadystatechange = function() {
                var ready=self.requestObject.readyState;
                var data=null;
                if (ready==FTD_AJAX_READY_STATE_COMPLETE){
                    self.closePopup(self);
                    data=self.requestObject.responseText;
                    
                    if( data.length > 0 ) {
                        if(data.substring(0,9) == "REDIRECT=" ) {
                            var pathname = window.location.pathname;
                            //alert("http://" + window.location.host + pathname.substring(0,pathname.lastIndexOf("/")) + "/" + data.substring(9));
                            window.location = "http://" + window.location.host + pathname.substring(0,pathname.lastIndexOf("/")) + "/" + data.substring(9);
                            return;
                        } else if(data.substring(0,5) == "<HTML" || data.substring(0,5) == "<html") {
                            window.location = "http://" + window.location.host + "/secadmin";
                            return;
                        }
                    }

                    //If html is returned then a request error occurred or the 
                    //securitytoken has expired.  Take the user back to the login page.
                    /*
                    if(data.length >= 5)
                      if(data.substring(0,5) == "<HTML" || data.substring(0,5) == "<html") {
                        window.location = "http://" + window.location.host + "/secadmin";
                        return;
                    }
                    */ 
                    self.callbackFunction(data);
                    return;
                } else if (ready == FTD_AJAX_READY_STATE_LOADING ) {
                    data="loading...";
                } else if (ready == FTD_AJAX_READY_STATE_LOADED ) {
                    data="loaded";
                } else if (ready == FTD_AJAX_READY_STATE_UNINITIALIZED ) {
                    data="initializing...";
                }
                
                self.updatePopup(self,data);
            }
            
            if( this.requestType == FTD_AJAX_REQUEST_TYPE_POST ) {
                this.requestObject.send(this.buildParamString());
            } else {    //Defaults to get with no parameters
                this.requestObject.send();
            }
        }
    },
    
    buildParamString: function() {
        var urlString = "";
        var idx;
        
        for( idx=0; idx<this.params.length; idx++ ) {
            if( idx==0 ) {
                urlString=this.params[idx];
            } else {
                urlString+="&"+this.params[idx];
            }
        }
        
        return urlString;
    },
    
    openPopup: function()
    {
        if( this.usePopup == true ) {
            this.popupWindow = window.open("","popupwindow","width=350,height=150");
            this.popupWindow.document.write("<html><head><title>Processing Request...</title></head><body><h2>Processing...</h2><p id=\"ftdAjaxPopupText\"><p/></body></html>");
        }
    },
    
    updatePopup: function(self,newText) {
        if( self.usePopup == true ) {
            self.popupWindow.document.getElementById('ftdAjaxPopupText').innerHTML=newText;
        }
    },
    
    closePopup: function(self) {
        if( self.usePopup == true ) {
            self.popupWindow.close();
        }
    }
}

