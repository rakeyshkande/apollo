/*
 *  Dependencies: js/prototype.js
 *                js/rico.js
 */

var FTD_DOM = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || Rico.prototypeVersion < 1.3)
      throw("Focus Utility requires the Prototype JavaScript framework >= 1.3");
      
FTD_DOM.ArrayExtensions = new Array();

if (Object.prototype.extend) {
   FTD_DOM.ArrayExtensions[ Rico.ArrayExtensions.length ] = Object.prototype.extend;
}else{
  Object.prototype.extend = function(object) {
    return Object.extend.apply(this, [this, object]);
  }
  FTD_DOM.ArrayExtensions[ Rico.ArrayExtensions.length ] = Object.prototype.extend;
}

if (Array.prototype.push) {
   FTD_DOM.ArrayExtensions[ FTD_DOM.ArrayExtensions.length ] = Array.prototype.push;
}

FTD_DOM.addEvent = function(elm, evType, fn, useCapture) {
    if( elm.addEventListener) {
        elm.addEventListener(evType, fn, useCapture);
        return true;
    } else if (elm.attachEvent) {
        var r = elm.attachEvent('on' + evType, fn);
        return r;
    } else {
        elm['on' + evType] = fn;
        return null;
    }
};

FTD_DOM.getStyle = function(element,styleProp)
{
    var value = element.style[styleProp.camelize()];
    if (!value) {
      if (document.defaultView && document.defaultView.getComputedStyle) {
        var css = document.defaultView.getComputedStyle(element, null);
        value = css ? css.getPropertyValue(styleProp) : null;
      } else if (element.currentStyle) {
        value = element.currentStyle[styleProp.camelize()];
      }
    }

    if (window.opera && ['left', 'top', 'right', 'bottom'].include(styleProp)) 
    {
        if (Element.getStyle(element, 'position') == 'static') { 
            value = 'auto';
        }
    }

    return value == 'auto' ? null : value;
};

FTD_DOM.findFirstInputFieldId = function(e) {
    if( e ) {
        var nodes = e.childNodes;
        for (var i = 0; i < nodes.length; i++) {
            if( (nodes[i].nodeName == 'INPUT' || nodes[i].nodeName == 'TEXTAREA' || nodes[i].nodeName=="SELECT") && 
                nodes[i].id && 
                !nodes[i].disabled &&
                nodes[i].type!='hidden') {
                return nodes[i].id;
            }
            if( nodes[i].hasChildNodes() ) {
                var results = FTD_DOM.findFirstInputFieldId(nodes[i]);
                if( results && results.length > 0 ) {
                    return results;
                }
            }
        }
    }
    
    return undefined;
}

FTD_DOM.findFirstAncestorOfType = function(e,nodeType) {
    if( e && nodeType ) {
        var parent = e.parentNode;
        if( parent ) {
            var nodename = parent.nodeName;
            if( nodename == nodeType.toUpperCase() ) {
                return parent;
            } else {
                return FTD_DOM.findFirstAncestorOfType(parent,nodeType);
            }
        } else {
            return undefined;
        }
    }
    
    return undefined;
}

FTD_DOM.getAllDecendents = function(parent, childArray) {
    if( parent && cNodeName && childArray && parent.hasChildNodes() ) {
        var nodes = parent.childNodes;
        childArray.push(nodes[i]);
        findDecendentsOfType(nodes[i], childArray);
    }
}

FTD_DOM.findDecendentsOfType = function(parent, cNodeName, cNodeType, childArray) {
    if( parent && cNodeName && childArray && parent.hasChildNodes() ) {
        var nodes = parent.childNodes;
        
        for (var i = 0; i < nodes.length; i++) {
            if( nodes[i].nodeName == cNodeName.toUpperCase() ) {
                if( cNodeType && cNodeType.length > 0 ) {
                    if( nodes[i].getAttribute('type') == cNodeType ) {
                        childArray.push(nodes[i]);
                    }
                } else {
                    childArray.push(nodes[i]);
                }
            }
            
            FTD_DOM.findDecendentsOfType(nodes[i], cNodeName, cNodeType, childArray);
        }
    }
}
      
FTD_DOM.FocusElement = Class.create();

FTD_DOM.FocusElement.prototype = {

    initialize: function(watchElementId, borderFocusColor, labelElementId, labelFocusColor) {
        this.watchElement           = $(watchElementId);
        this.borderFocusColor       = borderFocusColor;
        this.borderSize = 1;
        this.currentBorderBlurColor;
        if( !this.watchElement ) return;
        if( !this.borderFocusColor ) this.borderFocusColor = Green;
        
        if ( this.watchElement.type == "text" || this.watchElement.type == "textarea" || this.watchElement.tagName == "TEXTAREA") {
          this.watchElement.style.borderWidth = this.borderSize;
          this.watchElement.style.borderColor = "InactiveCaption";
          this.watchElement.style.borderStyle = "solid";
        } else if ( this.watchElement.type == "checkbox" ) {
          this.watchElement.style.borderWidth = this.borderSize;
          this.watchElement.style.borderColor = "white";
          this.watchElement.style.borderStyle = "solid";
        } 
        
        this._attachBehaviors();
        if( labelElementId ) {
            new FTD_DOM.FocusLabel( watchElementId, labelElementId, labelFocusColor );
        }
    },
   
    _attachBehaviors: function() {
        FTD_DOM.addEvent(this.watchElement, "focus", this.elementHasFocus.bindAsEventListener(this),false);
        FTD_DOM.addEvent(this.watchElement, "blur", this.elementLostFocus.bindAsEventListener(this),false);
    },

    elementHasFocus: function(e) {
        this.currentBorderBlurColor = FTD_DOM.getStyle(this.watchElement,'borderColor');
        //this.currentBorderBlurColor = "Background";
        if ( this.watchElement.type == "text" || this.watchElement.type == "textarea" || this.watchElement.tagName == "TEXTAREA") {
            this.watchElement.style.borderWidth = this.borderSize;
            this.watchElement.style.borderColor = this.borderFocusColor;
            this.watchElement.style.borderStyle = "solid";
        } else if (this.watchElement.type == "checkbox" ) {
            this.watchElement.style.borderWidth = this.borderSize;
            this.watchElement.style.borderColor = this.borderFocusColor;
            this.watchElement.style.borderStyle = "solid";
        } 
    },

    elementLostFocus: function(e) {
        if (this.watchElement.type == "text" || this.watchElement.type == "textarea" || this.watchElement.tagName == "TEXTAREA") {
            this.watchElement.style.borderWidth = this.borderSize;
//            this.watchElement.style.borderColor = "Background";
            this.watchElement.style.borderColor = this.currentBorderBlurColor;
            this.watchElement.style.borderStyle = "solid";
        } else if (this.watchElement.type == "checkbox" ) {
            this.watchElement.style.borderWidth = this.borderSize;
            this.watchElement.style.borderColor = "white";
            this.watchElement.style.borderStyle = "solid";
        } 
    }
};
      
FTD_DOM.FocusLabel = Class.create();

FTD_DOM.FocusLabel.prototype = {

    initialize: function(watchElementId, labelElementId, labelFocusColor) {
        this.watchElement           = $(watchElementId);
        this.labelElement           = $(labelElementId);
        this.labelFocusColor        = labelFocusColor;
        this.currentLabelBlurColor;
        if( !this.watchElement || !this.labelElement ) return;
        if( this.labelElement && !this.labelFocusColor ) this.labelFocusColor = Green;
        this._attachBehaviors();
    },
   
    _attachBehaviors: function() {
        FTD_DOM.addEvent(this.watchElement, "focus", this.elementHasFocus.bindAsEventListener(this),false);
        FTD_DOM.addEvent(this.watchElement, "blur", this.elementLostFocus.bindAsEventListener(this),false);
    },

    elementHasFocus: function(e) {
        this.currentLabelBlurColor = FTD_DOM.getStyle(this.labelElement,"color");
        //alert('currentLabelBlurColor: '+this.currentLabelBlurColor+'  labelFocusColor: '+this.labelFocusColor);
        this.labelElement.style.color = this.labelFocusColor;
    },

    elementLostFocus: function(e) {
        this.labelElement.style.color = this.currentLabelBlurColor;
    }
};

FTD_DOM.CharCountDown = Class.create();

FTD_DOM.CharCountDown.prototype = {

    initialize: function(textArea, countField, maxCount, label) {
      this.textArea             = $(textArea);
      this.countField           = $(countField);
      this.maxCount             = maxCount;
      this.label                = label;
      if( !label ) this.label   = 'Left: ';
      if( !textArea || !countField || !maxCount ) return;
      this._attachBehaviors();
   },
   
   _attachBehaviors: function() {
      this.textArea.onfocus     = this.textAreaHasFocus.bindAsEventListener(this);
      this.textArea.onblur      = this.textAreaLostFocus.bindAsEventListener(this);
      this.textArea.onkeyup     = this.textAreaHasChanged.bindAsEventListener(this);
   },

   textAreaHasFocus: function(e) {
        this.setCharCount();
        this.countField.style.visibility='visible';       
   },

   textAreaLostFocus: function(e) {
        this.countField.style.visibility='hidden'; 
   },

   textAreaHasChanged: function(e) {
        this.setCharCount();     
   },
   
   setCharCount: function() {
    var valueSize = this.textArea.value.length;
    this.countField.innerHTML= this.label+(this.maxCount-valueSize);
   }
};

FTD_DOM.InputElement = Class.create();

FTD_DOM.InputElement.prototype = {

   initialize: function(options) {
      this.setOptions(options);
  },

   setOptions: function(options) {
      this.options = {};
      Object.extend(this.options, options || {});
   },
   
   createNode: function() {
      var inputNode;

      //check for internet explorer
      if(document.all) {
        var nodeStr = '<input';
        
        for (var key in this.options) {
          var value = this.options[key];
          //alert('Key: '+key+"  Value: "+value);
          if( (value || value==0) && key!='extend' ) {
            //alert('Key: '+key+"  Value: "+value);
            nodeStr += ' ';
            nodeStr += key;
            nodeStr += '="';
            nodeStr += value;
            nodeStr += '"';
          }
        }
        nodeStr += '>';
        //alert(nodeStr);
        inputNode = document.createElement(nodeStr);
        
      } else {
        inputNode = document.createElement('input');
        
        for (var key in this.options) {
          var value = this.options[key];
          if( (value || value==0) && key!='extend' ) {
            //alert('Key: '+key+"  Value: "+value);
            inputNode.setAttribute(key,this.options[key]);
          }
        }
      }
      
      return inputNode;
   }
};