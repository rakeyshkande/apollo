<%
	String securitytoken = (String)request.getParameter("securitytoken");
%>
            <!-- Main Table -->
            <TABLE width="98%" border="0" cellpadding="2" cellspacing="2">
                <TR>
                    <TD width="15%" class="Header3"> Maintenance </TD>
                    <TD width="85%"> &nbsp; </TD>
                </TR>
                <TR>
					<TD> &nbsp; </TD>
                    <TD>
                        <a href="showProductMaintList.do?securitytoken=<%=securitytoken%>">
                            Product Maintenance
                        </a>                     
                    </TD>
                </TR>
                <TR>
					<TD> &nbsp; </TD>
                    <TD>
                        <a href="showUpdateNovatorProducts.do?securitytoken=<%=securitytoken%>">
                            Product Upload
                        </a>                     
                    </TD>
                </TR>
                <TR>
					<TD> &nbsp; </TD>
                    <TD>
                    	<a href="showImageFTP.do?securitytoken=<%=securitytoken%>">
                            FTP Image Upload
                        </a>
                    </TD>
                </TR>                
                <TR>
					<TD> &nbsp; </TD>
                    <TD>
                    	<a href="showOccasionMaintenance.do?securitytoken=<%=securitytoken%>">                        
                            Occasion Maintenance 
                        </a>
                    </TD>  
		</TR>
                <TR>
					<TD> &nbsp; </TD>
                    <TD>
                    	<a href="showUpsellMaintList.do?securitytoken=<%=securitytoken%>">                        
                            Upsell Maintenance 
                        </a>
                    </TD>  
		</TR>
                <TR>
					<TD> &nbsp; </TD>
                    <TD>
                    	<a href="showVendorProductOverride.do?securitytoken=<%=securitytoken%>">                        
                            Vendor Product Override 
                        </a>
                    </TD>  
		</TR>
                <TR>
					<TD> &nbsp; </TD>
                    <TD>
                    	<a href="showDeliveryZipMaintPage.do?securitytoken=<%=securitytoken%>">                        
                            Delivery Zip Maintenance 
                        </a>
                    </TD>  
		</TR>
                <TR>
                        <TD> <hr> </TD>
                        <TD> &nbsp; </TD>
                </TR>

                <TR>
                    <TD class="Header3"> Holiday Pricing </TD>
                    <TD> &nbsp; </TD>
                </TR>

                <TR>
					<TD> &nbsp; </TD>
                    <TD>
                    	<a href="showHolidayPricingActivate.do?securitytoken=<%=securitytoken%>">                        
                            Holiday Pricing Activate
                        </a>
                    </TD>  
                </TR>                   
                <TR>
					<TD> &nbsp; </TD>
                    <TD>
                    	<a href="showProductHolidayList.do?securitytoken=<%=securitytoken%>">                     
                            Display Products with Holiday Pricing
                        </a>
                    </TD>
                </TR>
                <TR>
                    <TD> <hr> </TD>
                    <TD> &nbsp; </TD>
                </TR>

                <TR>
                    <TD class="Header3"> Shipping Key </TD>
                    <TD> &nbsp; </TD>
                </TR>

                <TR>
                    <TD> &nbsp; </TD>
                    <TD>
                        <a href="showShippingKeyControl.do?securitytoken=<%=securitytoken%>">                        
                            Shipping Key Control Page
                        </a>
                    </TD>
                </TR>

                <TR>
                        <TD> <hr> </TD>
                        <TD> &nbsp; </TD>
                </TR>
                
            </TABLE>
