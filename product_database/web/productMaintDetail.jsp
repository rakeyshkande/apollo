<%@ page import = "org.apache.struts.action.ActionMessages"%>
<%@ page import = "com.ftd.osp.utilities.ConfigurationUtil"%>
<%@ page import = "com.ftd.pdb.common.ProductMaintConstants"%>
<%@ page import = "com.ftd.pdb.common.valobjs.VendorProductVO"%>
<%@ page import = "com.ftd.pdb.common.valobjs.ProductAddonVO"%>
<%@ page import = "com.ftd.pdb.common.valobjs.DeliveryOptionVO"%>
<%@ page import = "com.ftd.pdb.common.utilities.FTDUtil"%>
<%@ page import = "com.ftd.pdb.common.utilities.StrutsUtilities"%>
<%@ page import = "com.ftd.pdb.common.valobjs.*"%>
<%@ page import = "java.text.SimpleDateFormat"%>
<%@ page import = "java.util.List"%>
<%@ page import = "java.util.ArrayList"%>
<%@ page import = "org.apache.commons.lang.StringUtils"%>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html"%>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic"%>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean"%>

<bean:define id = "productForm" name = "productMaintDetailForm" type = "com.ftd.pdb.web.ProductMaintDetailForm" scope = "request"/>
<!-- List and combo options -->
<bean:define id = "categoriesOptions" name = "productMaintDetailForm" property = "categories" type = "java.util.ArrayList" scope = "request"/>

<bean:define id = "exceptionsOptions" name = "productMaintDetailForm" property = "exceptions" type = "java.util.ArrayList" scope = "request"/>

<bean:define id = "pricePointsOptions" name = "productMaintDetailForm" property = "pricePoints" type = "java.util.ArrayList" scope = "request"/>

<bean:define id = "shippingKeysOptions" name = "productMaintDetailForm" property = "shippingKeys" type = "java.util.ArrayList" scope = "request"/>

<bean:define id = "boxListOptions" name = "productMaintDetailForm" property = "boxList" type = "java.util.ArrayList" scope = "request"/>

<bean:define id = "recipientSearchListOptions" name = "productMaintDetailForm" property = "recipientSearchList" type = "java.util.ArrayList" scope = "request"/>

<bean:define id = "personalizationTemplateListOptions" name = "productMaintDetailForm" property = "personalizationTemplateList" type = "java.util.ArrayList" scope = "request"/>

<bean:define id = "personalizationTemplateOrderListOptions" name = "productMaintDetailForm" property = "personalizationTemplateOrderList" type = "java.util.ArrayList" scope = "request"/>

<bean:define id = "searchPriorityListOptions" name = "productMaintDetailForm" property = "searchPriorityList" type = "java.util.ArrayList" scope = "request"/>

<bean:define id = "companyMasterOptions" name = "productMaintDetailForm" property = "companyMasterList" type = "java.util.ArrayList" scope = "request"/>

<bean:define id = "typesOptions" name = "productMaintDetailForm" property = "types" type = "java.util.ArrayList" scope = "request"/>

<bean:define id = "currentSubTypesOptions" name = "productMaintDetailForm" property = "currentSubTypes" type = "java.util.ArrayList" scope = "request"/>

<bean:define id = "secondChoicesOptions" name = "productMaintDetailForm" property = "secondChoices" type = "java.util.ArrayList" scope = "request"/>

<bean:define id = "vendorListOptions" name = "productMaintDetailForm" property = "vendorList" type = "java.util.ArrayList" scope = "request"/>
<!-- product specific multi selections or options -->
<bean:define id = "excludedColorsListOptions" name = "productMaintDetailForm" property = "excludedColorsList" type = "java.util.ArrayList" scope = "request"/>

<!-- This i for the new temp collection -->
<bean:define id = "excludedColorsListStaticOptions" name = "productMaintDetailForm" property = "excludedColorsListStatic" type = "java.util.ArrayList" scope = "request"/>
<bean:define id = "includedColorsListStatic" name = "productMaintDetailForm" property = "includedColorsListStatic" type = "java.util.ArrayList" scope = "request"/>



<bean:define id = "includedColorsListOptions" name = "productMaintDetailForm" property = "includedColorsList" type = "java.util.ArrayList" scope = "request"/>

<bean:define id = "availableComponentSkuListOptions" name = "productMaintDetailForm" property = "availableComponentSkuList" type = "java.util.ArrayList" scope = "request"/>

<bean:define id = "includedComponentSkuListOptions" name = "productMaintDetailForm" property = "includedComponentSkuList" type = "java.util.ArrayList" scope = "request"/>

<bean:define id = "shippingAvailabilityListOptions" name = "productMaintDetailForm" property = "shippingAvailabilityList" type = "java.util.ArrayList" scope = "request"/>

<bean:define id = "shippingMethodsOptions" name = "productMaintDetailForm" property = "shippingMethods" type = "java.util.ArrayList" scope = "request"/>

<bean:define id = "currCountriesOptions" name = "productMaintDetailForm" property = "currCountries" type = "java.util.ArrayList" scope = "request"/>

<bean:define id = "vendorProductsListOptions" name = "productMaintDetailForm" property = "vendorProductsList" type = "java.util.ArrayList" scope = "request"/>

<bean:define id = "excludedDeliveryStatesOptions" name = "productMaintDetailForm" property = "excludedDeliveryStates" type = "java.util.ArrayList" scope = "request"/>

<bean:define id = "subCodeListOptions" name = "productMaintDetailForm" property = "subCodeList" type = "java.util.ArrayList" scope = "request"/>

<bean:define id = "serviceDurationOptions" name = "productMaintDetailForm" property = "durations" type = "java.util.ArrayList" scope = "request"/>

<% String optionalArgs = (String)productForm.get(ProductMaintConstants.MENU_BACK_OPTIONAL_ARGS);

   if (optionalArgs == null) {
    optionalArgs = "";
   }

   ConfigurationUtil rm = ConfigurationUtil.getInstance();
   String defaultTitleParm = rm.getFrpGlobalParmNoNull(ProductMaintConstants.PDB_CONFIG_CONTEXT, ProductMaintConstants.PDB_GBB_DEFAULT_TITLE);
   String imageURLPrefixParm = rm.getFrpGlobalParmNoNull(ProductMaintConstants.PDB_CONFIG_CONTEXT, ProductMaintConstants.PDB_IMAGE_URL_PREFIX);
   String imageURLTypeParm = rm.getFrpGlobalParmNoNull(ProductMaintConstants.PDB_CONFIG_CONTEXT, ProductMaintConstants.PDB_IMAGE_URL_TYPE);
   String imageURLStandardSuffix = rm.getFrpGlobalParmNoNull(ProductMaintConstants.PDB_CONFIG_CONTEXT, ProductMaintConstants.PDB_IMAGE_URL_STANDARD_SUFFIX);
   String imageURLDeluxeSuffix = rm.getFrpGlobalParmNoNull(ProductMaintConstants.PDB_CONFIG_CONTEXT, ProductMaintConstants.PDB_IMAGE_URL_DELUXE_SUFFIX);
   String imageURLPremiumSuffix = rm.getFrpGlobalParmNoNull(ProductMaintConstants.PDB_CONFIG_CONTEXT, ProductMaintConstants.PDB_IMAGE_URL_PREMIUM_SUFFIX);
   String floralLabelStandard = rm.getFrpGlobalParmNoNull(ProductMaintConstants.FTD_APPS_CONTEXT, ProductMaintConstants.PDB_FLORAL_LABEL_STANDARD);
   String floralLabelDeluxe = rm.getFrpGlobalParmNoNull(ProductMaintConstants.FTD_APPS_CONTEXT, ProductMaintConstants.PDB_FLORAL_LABEL_DELUXE);
   String floralLabelPremium = rm.getFrpGlobalParmNoNull(ProductMaintConstants.FTD_APPS_CONTEXT, ProductMaintConstants.PDB_FLORAL_LABEL_PREMIUM);

   java.util.List masterSkuList = (java.util.List)productForm.get(ProductMaintConstants.PRODUCT_MASTER_SKU_KEY);
   if (masterSkuList == null) {
     masterSkuList = new java.util.ArrayList();
   }
   /*boolean okForUnavailable = true;
   for (int x = 0; x < masterSkuList.size(); x++) {
     UpsellDetailVO upsellVO = (UpsellDetailVO)masterSkuList.get(x);
     if (upsellVO.getSequence() > 1 && upsellVO.getDefaultSkuFlag().equals("Y")) {
       okForUnavailable = false;
     }
   }*/
%>

<script type = "text/javascript" src = "js/util.js">
</script>

<script type = "text/javascript" src = "js/calendar2.js">
</script>

<script type = "text/javascript" src = "js/Tokenizer.js">
</script>

<script type = "text/javascript" src = "js/Listbox.js">
</script>

<script type = "text/javascript" src = "js/prototype.js">
</script>

<script type = "text/javascript" src = "js/rico.js">
</script>

<script type = "text/javascript" src = "js/ftddom.js">
</script>

<script type = "text/javascript" src = "js/ftdajax.js">
</script>

<script type = "text/javascript" src = "js/FormChek.js">
</script>

<script type = "text/javascript" src = "js/pngfix.js">
</script>

<!-- Do not move this section. It initializes variables shared between the jsp and the productMaintDetail.js file -->
<script language = "JavaScript" type = "text/javascript">
<%
    String selectAllText = "select all";
%>

selectAll = '<%=selectAllText%>';
deselectAll = 'deselect all';

</script>
 
<script type = "text/javascript" src = "js/productMaintDetail.js">
</script>

<script language = "JavaScript" type = "text/javascript">

 var currentProductType  = trimString('<%=productForm.get("productType")%>');
 if(currentProductType == 'NONE') {
  currentProductType = ''; // On Post Back for Err Validation, we could have NONE as the type. Clear to blank
 }
 
 var closePupUp = false;

 // varible for accordian design
 var onloads = new Array();

 // Good, Better, Best variables
 var productName = "";
 var standardPrice = "";
 var deluxePrice = "";
 var premiumPrice = "";
 var defaultTitle = "";
 var imageURL = "";
 var imageURLType = "";
 var imageURLStandard = "";
 var imageURLDeluxe = "";
 var imageURLPremium = "";
 var webProductId = "";
 var initProductAvailable = false;
 var DISALLOW_WEB_PRODUCT_ID_CHANGE_MSG = "Please make this entire product unavailable, feeding the change to the website. Then please return to make changes to the Novator ID and update the products availability."
 var disabledAddons = new Array();
 var disabledVases = new Array();
 
function changeName1() {
    if (document.getElementById("gbbNameOverrideFlag1").checked == true) {
        document.getElementById("position1Text").innerHTML = document.getElementById("gbbNameOverrideText1").value;
    } else {
        document.getElementById("position1Text").innerHTML = productName;
    }
}

function changePrice1() {
    if (document.getElementById("gbbPriceOverrideFlag1").checked == true) {
        document.getElementById("position1Price").innerHTML = formatPrice(document.getElementById("gbbPriceOverrideText1").value);
    } else {
        document.getElementById("position1Price").innerHTML = formatPrice(standardPrice);
    }
    document.getElementById("position1PriceLabel").innerHTML = formatPrice(standardPrice);
    document.getElementById("azPosition1PriceLabel").innerHTML = formatPrice(standardPrice);
}

function changeName2() {
    if (document.getElementById("gbbNameOverrideFlag2").checked == true) {
        document.getElementById("position2Text").innerHTML = document.getElementById("gbbNameOverrideText2").value;
    } else {
        document.getElementById("position2Text").innerHTML = productName;
    }
}

function changePrice2() {
    if (document.getElementById("gbbPriceOverrideFlag2").checked == true) {
        document.getElementById("position2Price").innerHTML = formatPrice(document.getElementById("gbbPriceOverrideText2").value);
    } else {
        document.getElementById("position2Price").innerHTML = formatPrice(deluxePrice);
    }
    document.getElementById("position2PriceLabel").innerHTML = formatPrice(deluxePrice);
    document.getElementById("azPosition2PriceLabel").innerHTML = formatPrice(deluxePrice);
}

function changeName3() {
    if (document.getElementById("gbbNameOverrideFlag3").checked == true) {
        document.getElementById("position3Text").innerHTML = document.getElementById("gbbNameOverrideText3").value;
    } else {
        document.getElementById("position3Text").innerHTML = productName;
    }
}

function changePrice3() {
    if (document.getElementById("gbbPriceOverrideFlag3").checked == true) {
        document.getElementById("position3Price").innerHTML = formatPrice(document.getElementById("gbbPriceOverrideText3").value);
    } else {
        document.getElementById("position3Price").innerHTML = formatPrice(premiumPrice);
    }
    document.getElementById("position3PriceLabel").innerHTML = formatPrice(premiumPrice);
    document.getElementById("azPosition3PriceLabel").innerHTML = formatPrice(premiumPrice);
}

function formatPrice(price) {
    var num = price.toString().replace(/\$|\,/g,'');
    if(isNaN(num))
      num = "0";
    sign = (num == (num = Math.abs(num)));
    num = Math.floor(num*100+0.50000000001);
    cents = num%100;
    num = Math.floor(num/100).toString();
    if(cents<10)
      cents = "0" + cents;
    for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
      num = num.substring(0,num.length-(4*i+3))+','+
    num.substring(num.length-(4*i+3));
    return '$' + (((sign)?'':'-') + num + '.' + cents);
}

 //***********************************************************************************************
 // function unload() - do NOT delete... security.jsp requires function definition
 //***********************************************************************************************
function unload(){}


 //***********************************************************************************************
 // function checkDateExceptions()
 //***********************************************************************************************
 function checkDateExceptions(clearOutValues) {
  if ((document.getElementById("exceptionCode").value == 'A') && (document.getElementById("exceptionStartDate").value != '') && (document.getElementById("exceptionEndDate").value != '')) {
   //***********
   //**START DATE
   //***********
   //retrieve the start date
   var startDate = document.getElementById("exceptionStartDate").value;

   //parse and store the start date
   var startDateTokens = startDate.tokenize("/", true, true);
   var startMonth = startDateTokens[0];
   var startDay = startDateTokens[1];
   var startYear = startDateTokens[2];

   //create a new start date as a date variable, using the user entered date
   var dStartDate = new Date();
   dStartDate.setMonth(startMonth - 1); //month is 0 based, so, subtract 1
   dStartDate.setYear(startYear);
   dStartDate.setDate(startDay);

   //get the time for the start date
   var startDateTime = dStartDate.getTime();

   //***********
   //**END DATE
   //***********
   //retrieve the end date
   var endDate = document.getElementById("exceptionEndDate").value;

   //parse and store the end date
   var endDateTokens = endDate.tokenize("/", true, true);
   var endMonth = endDateTokens[0];
   var endDay = endDateTokens[1];
   var endYear = endDateTokens[2];

   //create a new end date as a date variable, using the user entered date
   var dEndDate = new Date();
   dEndDate.setMonth(endMonth - 1); //month is 0 based, so, subtract 1
   dEndDate.setYear(endYear);
   dEndDate.setDate(endDay);

   //get the time for the end date
   var endDateTime = dEndDate.getTime();

   //***********
   //**TODAY DATE
   //***********
   //create todays date
   var dTodayDate = new Date();

   //get the time for the end date
   var todayDateTime = dTodayDate.getTime();

   if ((startDateTime <= todayDateTime) && (todayDateTime <= endDateTime) && (document.getElementById("status").checked == false)) {
    alert("Today is within the date exception period, therefore the product has been change from unavailable to available.");
    document.getElementById("status").checked = true;
   }
  } else if (clearOutValues == true) {
   document.getElementById("exceptionStartDate").value = "";
   document.getElementById("exceptionEndDate").value = "";
   document.getElementById("exceptionMessage").value = "";
  }
 }

 function toggleAvailability() {
  // this method changes all the sub code availability for vendor products
  var subCodes = document.getElementsByName('productSubCodeId');

  if (subCodes != null) {
   for (var i = 0; i < subCodes.length; i++) {
    onVendorAvailableChange(subCodes[i].value);
   }
  }
 }

 function toggleShipMethodCarrier() {
  var arrAvailable = document.getElementsByName("vendorAvailableCheckbox");
  var isAvailable = 0;

  for (var i = 0; i < arrAvailable.length; i++) {
   if (arrAvailable[i].checked == true) {
    isAvailable++;
   }
  }

  if ((document.getElementById("status").checked == true) && (isAvailable == 0) && (document.getElementById("shipMethodCarrier").checked == true)) {
   alert("The product is available and no vendors are available, therefore drop ship delivery has been turned off.");
   	document.getElementById("shipMethodCarrier").checked = false;
   	document.getElementById("morningDeliveryFlag").checked = false; 
   	document.getElementById("morningDeliveryFlag").disabled = true; 
  }

  if ((isAvailable > 0) && (document.getElementById("shipMethodCarrier").checked == false)) {
   var turnOn = confirm("Vendors are available, but drop ship delivery has been turned off.  Would you like to turn it on?");

   if (turnOn == true) {
    document.getElementById("shipMethodCarrier").checked = true;
    document.getElementById("morningDeliveryFlag").disabled = false; 
   } else {   	
   	document.getElementById("morningDeliveryFlag").checked = false;
	document.getElementById("morningDeliveryFlag").disabled = true; 
   }
  }
  
  if(document.getElementById("shipMethodCarrier").checked) {
  	document.getElementById("morningDeliveryFlag").disabled = false;
  }
 }
 
 // this method changes the sub code availability for a vendor products
 function onVendorAvailableChange(productId) {
  if (document.getElementsByName("productSubCodeId").length > 0) {
   var arrAvailable = document.getElementsByName("vendorAvailableCheckbox");
   var arrIds = document.getElementsByName("productSubcodeVendorId");
   var isAvailable = 0;

   for (var i = 0; i < arrAvailable.length; i++) {
    if (arrIds[i].value == productId) {
     if (arrAvailable[i].checked == true) {
      isAvailable++;
     }
    }
   }

   if (isAvailable > 0) {
    document.getElementById("productSubCodeAvailableCheckbox" + productId).checked = true;
    document.getElementById("productSubCodeAvailable" + productId).value = "true";
   } else {
    document.getElementById("productSubCodeAvailableCheckbox" + productId).checked = false;
    document.getElementById("productSubCodeAvailable" + productId).value = "false";
   }
  }
 }

 // removes all vendors
 function removeAllVendors() {
  var div = document.getElementById('div_vendor_context');
  div.innerHTML = "";
  toggleAvailability();
 }

 // removes a vendor
 function removeVendor(id) {
  removeVendorRow(id);
  toggleAvailability();
  document.getElementById("vendorListId").focus();
 }

 // removes a vendor row
 function removeVendorRow(id) {
  var vendorId = document.getElementById(id).vendorId;
  document.getElementById(id).outerHTML = "";
 }

 //changes overall availability based on type
 function changeAvailibility() {
  var productType = document.getElementById("productType").value;

  if (productType == "FLORAL") {
   document.getElementById("shippingSystem").selectedIndex = 0;
   document.getElementById("shippingSystem").disabled = true;
   document.getElementById("boxId").selectedIndex = 0;
   document.getElementById("boxId").disabled = true;
   document.getElementById("zoneJumpEligibleFlag").disabled = true;
   document.getElementById("zoneJumpEligibleFlag").checked = false;
   document.getElementById("supplyExpense").disabled = true;
   document.getElementById("royaltyPercent").disabled = true;
   document.getElementById("showSupplyExpenseCal").style.visibility = 'hidden';
   document.getElementById("showRoyaltyPercentCal").style.visibility = 'hidden';
   document.getElementById("showComponentArrows").style.visibility = 'hidden';
  } else if (productType == "NONE") {
   document.getElementById("shippingSystem").disabled = false;
   document.getElementById("boxId").disabled = false;
   document.getElementById("zoneJumpEligibleFlag").disabled = false;
   document.getElementById("supplyExpense").disabled = false;
   document.getElementById("royaltyPercent").disabled = false;
   document.getElementById("showSupplyExpenseCal").style.visibility = 'visible';
   document.getElementById("showRoyaltyPercentCal").style.visibility = 'visible';
   document.getElementById("showComponentArrows").style.visibility = 'visible';
  } else if (productType == "FRECUT" || productType == "SPEGFT") {
   document.getElementById("shippingSystem").disabled = false;
   document.getElementById("boxId").disabled = false;
   document.getElementById("zoneJumpEligibleFlag").disabled = false;
   document.getElementById("supplyExpense").disabled = false;
   document.getElementById("royaltyPercent").disabled = false;
   document.getElementById("showSupplyExpenseCal").style.visibility = 'visible';
   document.getElementById("showRoyaltyPercentCal").style.visibility = 'visible';
   document.getElementById("showComponentArrows").style.visibility = 'visible';
  } else if (productType == "SDG" || productType == "SDFC") {
   document.getElementById("shippingSystem").disabled = false;
   document.getElementById("boxId").disabled = false;
   document.getElementById("zoneJumpEligibleFlag").disabled = false;
   document.getElementById("supplyExpense").disabled = false;
   document.getElementById("royaltyPercent").disabled = false;
   document.getElementById("showSupplyExpenseCal").style.visibility = 'visible';
   document.getElementById("showRoyaltyPercentCal").style.visibility = 'visible';
   document.getElementById("showComponentArrows").style.visibility = 'visible';
  }
  
  /*if(productType == "SERVICES" || productType == "NONE" || productType == "FLORAL") {  	
  	document.getElementById("morningDeliveryFlag").checked = false; 
   	document.getElementById("morningDeliveryFlag").disabled = true;   
  } */ 
 }

 // toggles novator tags, they save novator ship method names
 function toggleNovatorTags(element1, element2) {
  var novatorTagList = document.getElementsByName("novatorTagList");
  novatorTagList[element1].checked = element2;
 }

 // closes the popup
 function closepopup() {
  if (closePupUp) {
   popuponclick();

   if (false == my_window.closed) {
    my_window.close();
   }
  }
 }

 // init method
 function init() {
  toggleAvailability();
  for (var i = 0; i < onloads.length; i++) {
   onloads[i]();
  }
  setVendorSKU();
  closepopup();
  getDefaultDisabledFields();
  selectMercuryChoice(document.forms[0].secondChoice, document.forms[0].mercurySecondChoice);
  chooseArrangementAvail();
  setNavigationHandlers();
  buildSubTypeDropdown(true);
  buildVendorDropdown(true);
  changeAvailibility();
  changeCountryList();
  setDropShipCosts();
  disableHoldUntilAvailable();

  if (document.getElementById("subcodeTable") != null && document.getElementById("subcodeTable").numRows == 0)
   document.getElementById("subcodeTable").style.visibility = "hidden";

  assignNextDayOnClick();
  document.getElementById('productName').focus();
  scroll(0, 0);

  productName = document.getElementById("productName").value;
  standardPrice = document.getElementById("standardPrice").value;
  if (standardPrice == "") standardPrice = "0";
  deluxePrice = document.getElementById("deluxePrice").value;
  if (deluxePrice == "") deluxePrice = "0";
  premiumPrice = document.getElementById("premiumPrice").value;
  if (premiumPrice == "") premiumPrice = "0";
  standardPrice = parseFloat(standardPrice).toFixed(2);
  deluxePrice = parseFloat(deluxePrice).toFixed(2);
  premiumPrice = parseFloat(premiumPrice).toFixed(2);
  if (deluxePrice < .01) {
      document.getElementById('defaultPrice2').disabled = true;
      document.getElementById('azCatalogStatusDeluxe').disabled = true;
      document.getElementById('azProductNameDeluxe').disabled = true;
      document.getElementById('azProductDescriptionDeluxe').disabled = true;
  }
  if (premiumPrice < .01) {
      document.getElementById('defaultPrice3').disabled = true;
      document.getElementById('azCatalogStatusPremium').disabled = true;
      document.getElementById('azProductNamePremium').disabled = true;
      document.getElementById('azProductDescriptionPremium').disabled = true;
  }
  checkCatalogStatusStandard();
  checkCatalogStatusDeluxe();
  checkCatalogStatusPremium();
  
  defaultTitle = "<%= defaultTitleParm %>";
  imageURL = "<%= imageURLPrefixParm %>";
  imageURLType = "<%= imageURLTypeParm %>";
  imageURLStandard = "<%= imageURLStandardSuffix %>";
  imageURLDeluxe = "<%= imageURLDeluxeSuffix %>";
  imageURLPremium = "<%= imageURLPremiumSuffix %>";

  document.getElementById("position1Text").innerHTML = productName;
  document.getElementById("position2Text").innerHTML = productName;
  document.getElementById("position3Text").innerHTML = productName;
  document.getElementById("position1PriceLabel").innerHTML = formatPrice(standardPrice);
  document.getElementById("position2PriceLabel").innerHTML = formatPrice(deluxePrice);
  document.getElementById("position3PriceLabel").innerHTML = formatPrice(premiumPrice);
  document.getElementById("azPosition1PriceLabel").innerHTML = formatPrice(standardPrice);
  document.getElementById("azPosition2PriceLabel").innerHTML = formatPrice(deluxePrice);
  document.getElementById("azPosition3PriceLabel").innerHTML = formatPrice(premiumPrice);
  document.getElementById("position1Price").innerHTML = formatPrice(standardPrice);
  document.getElementById("position2Price").innerHTML = formatPrice(deluxePrice);
  document.getElementById("position3Price").innerHTML = formatPrice(premiumPrice);
  if (document.getElementById("gbbTitle").value.length == 0) {
      document.getElementById("gbbTitle").value = defaultTitle;
  }
  document.getElementById("image1").src = imageURL + document.getElementById("novatorId").value + imageURLStandard + "." + imageURLType;
  document.getElementById("image2").src = imageURL + document.getElementById("novatorId").value + imageURLDeluxe + "." + imageURLType;
  document.getElementById("image3").src = imageURL + document.getElementById("novatorId").value + imageURLPremium + "." + imageURLType;
  changeName1();
  changeName2();
  changeName3();
  changePrice1();
  changePrice2();
  changePrice3();
  if (deluxePrice == "" || deluxePrice <= 0 || premiumPrice == "" || premiumPrice <= 0) {
      disableGBB();
  }
  
  standardLabel = "<%= floralLabelStandard %>";
  if (standardLabel.length > 0) {
      standardPriceLabel.innerHTML = standardLabel + " Price";
      recipeLabel.innerHTML = standardLabel + " Recipe";
      sendStandardRecipeLabel.innerHTML ="Send " + standardLabel + " Recipe";
  }
  deluxeLabel = "<%= floralLabelDeluxe %>";
  if (deluxeLabel.length > 0) {
      deluxePriceLabel.innerHTML = deluxeLabel + " Price";
      deluxeRecipeLabel.innerHTML = deluxeLabel + " Recipe";
      sendDeluxeRecipeLabel.innerHTML ="Send " + deluxeLabel + " Recipe";
  }
  premiumLabel = "<%= floralLabelPremium %>";
  if (premiumLabel.length > 0) {
      premiumPriceLabel.innerHTML = premiumLabel + " Price";
      premiumRecipeLabel.innerHTML = premiumLabel + " Recipe";
      sendPremiumRecipeLabel.innerHTML ="Send " + premiumLabel + " Recipe";
  }
  defaultPrice = document.getElementById('preferredPricePoint').value;
  if (defaultPrice == 3) {
      document.getElementById('defaultPrice3').checked = true;
  } else if (defaultPrice == 2) {
      document.getElementById('defaultPrice2').checked = true;
  } else {
      document.getElementById('defaultPrice1').checked = true;
  }
  
  initProductAvailable = document.getElementById("status").checked;
  webProductId = document.getElementById('novatorId').value;
  //setTimeout( 'pngFix()', 1000 );
  
  var errorSpanArray = document.getElementsByTagName("span");
  if(errorSpanArray != null && errorSpanArray.length > 0){
	  for(var i = 0; i < errorSpanArray.length; i++){
		  if(errorSpanArray[i].className == "Error" && 
				  (errorSpanArray[i].innerHTML == "Product Id is required." || errorSpanArray[i].innerHTML == "Product Id already exists.")){
			  var elems = document.getElementsByName("productId");
				for(var j = 0; j < elems.length; j++){
					if(elems[j].id == "productId"){
						elems[j].disabled = false;
					}
				}
		  }
	  }
  }
  <%String cp = (String)request.getAttribute(ProductMaintConstants.COPY_PRODUCT_COPY);%>
  var copyProd  = '<%=cp%>';
  var frmCopyProduct = '<%=productForm.get(ProductMaintConstants.COPY_PRODUCT)%>';
  var inst = '<%=productForm.get(ProductMaintConstants.PRODUCT_MAINT_INSTRUCTION_KEY)%>';
  //alert("copyProd : "+copyProd);
  //alert("frmCopyProduct : "+frmCopyProduct);
  if(copyProd != null && copyProd != "" && copyProd != "null"){
	  //alert("Entered if");
	  document.getElementById('copyProduct').value = copyProd;
	  
  }else{
	  copyProd = frmCopyProduct;
	  document.getElementById('copyProduct').value = frmCopyProduct;
  }
  //alert("inst -> copyProduct : "+ inst + " -> "+ copyProd);
  if(inst != null && inst != "" && copyProd != null && copyProd == 'COPY'){
	  
	  //document.getElementById('copyBtn').disabled = true;
	  var elems = document.getElementsByTagName("input");
	  for(var i = 0; i < elems.length; i++){
		if(elems[i].id == "productId"){
			elems[i].disabled = false;
			elems[i].value = "";
		}
		if(elems[i].id == "vendorCost" || elems[i].id == "standardPrice" 
			|| elems[i].id == "deluxePrice" || elems[i].id == "premiumPrice"
			|| elems[i].id == "variablePriceMax" || elems[i].id == "productSubCodePrice"){
			elems[i].value = "";
		}
		if(elems[i].id == "novatorId" || elems[i].id == "floristReferenceNumber"){
			elems[i].value = "";	
		}
	  }
	  document.getElementById("div_general_header").innerHTML = "Product Identification";
  /* }else if((inst != null && inst != "" && copyProd != null && copyProd == 'COPY')){
	  document.getElementById('copyProduct').value = ''; */
  }else if((inst == null || inst == '' || inst == 'null') && (copyProd != null && copyProd == 'COPY')){
	  //document.getElementById('copyBtn').disabled = false;
	  var elems = document.getElementsByTagName("input");
	  for(var i = 0; i < elems.length; i++){
		if(elems[i].id == "productId"){
			elems[i].disabled = false;
		}
		
	  }
  }
	

    if(document.getElementById("shipMethodCarrier").checked == false) {
    		document.getElementById("morningDeliveryFlag").disabled = true;
  	}
    disableValidShipDays();
    
    document.getElementById("markerSelectBoxExcluded").disabled = false;
    document.getElementById("markerSelectBoxIncluded").disabled = false;
    document.getElementById("bulletDescription").disabled = false;
    
 }

 function getDefaultDisabledFields() {
     myList = document.getElementsByTagName("input");
     for (i=0; i<myList.length; i++) {
         if (myList[i].name == 'add_on_active_flag' && myList[i].disabled) {
             disabledAddons.push(myList[i].value);
         } else if (myList[i].name == 'add_on_vase_active_flag' && myList[i].disabled) {
             disabledVases.push(myList[i].value);
         }

     }
 }

 function arrayContains(arr, value) {
     var i = arr.length;
     while (i--) {
         if (arr[i] === value) return true;
     }
     return false;
 }
function setVendorSKU()
{
	var elems = document.getElementsByTagName("input");
	var shipType = document.getElementById("shippingSystem").value;
	for(var i = 0; i < elems.length; i++)
	{
		if(shipType!=''&& shipType=='FTD WEST')
			{
			  if(elems[i].id == "vendorSKU" )
			  {
			    elems[i].value = document.getElementById("pquadProductID").value;
			    elems[i].readOnly = true;
			  }
			  if(elems[i].id == "vendorCost" )
			  {
				    elems[i].value = '0.0';
				    elems[i].readOnly = true;
			  }
			}
	
	} 
}
 function enableGBB() {
     document.getElementById("gbbPopoverFlag").disabled = false;
     document.getElementById("gbbTitle").disabled = false;
     document.getElementById("gbbNameOverrideFlag1").disabled = false;
     document.getElementById("gbbNameOverrideText1").disabled = false;
     document.getElementById("gbbPriceOverrideFlag1").disabled = false;
     document.getElementById("gbbPriceOverrideText1").disabled = false;
     document.getElementById("gbbNameOverrideFlag2").disabled = false;
     document.getElementById("gbbNameOverrideText2").disabled = false;
     document.getElementById("gbbPriceOverrideFlag2").disabled = false;
     document.getElementById("gbbPriceOverrideText2").disabled = false;
     document.getElementById("gbbNameOverrideFlag3").disabled = false;
     document.getElementById("gbbNameOverrideText3").disabled = false;
     document.getElementById("gbbPriceOverrideFlag3").disabled = false;
     document.getElementById("gbbPriceOverrideText3").disabled = false;
 }
 
 function disableGBB() {
     document.getElementById("gbbPopoverFlag").disabled = true;
     document.getElementById("gbbTitle").disabled = true;
     document.getElementById("gbbNameOverrideFlag1").disabled = true;
     document.getElementById("gbbNameOverrideText1").disabled = true;
     document.getElementById("gbbPriceOverrideFlag1").disabled = true;
     document.getElementById("gbbPriceOverrideText1").disabled = true;
     document.getElementById("gbbNameOverrideFlag2").disabled = true;
     document.getElementById("gbbNameOverrideText2").disabled = true;
     document.getElementById("gbbPriceOverrideFlag2").disabled = true;
     document.getElementById("gbbPriceOverrideText2").disabled = true;
     document.getElementById("gbbNameOverrideFlag3").disabled = true;
     document.getElementById("gbbNameOverrideText3").disabled = true;
     document.getElementById("gbbPriceOverrideFlag3").disabled = true;
     document.getElementById("gbbPriceOverrideText3").disabled = true;
 }

 function changeProductName() 
 {
	 var str;
	 str = document.getElementById("productName").value;
	 replaceSymboltoHtml(str,'productName');
     productName = document.getElementById("productName").value;
     changeName1();
     changeName2();
     changeName3();
 }
 
 function changeNovatorId() {
     // Trim the changed Novator Id
     var novatorId = trimString(document.getElementById("novatorId").value);
     document.getElementById("novatorId").value = novatorId;
	 webProductId = novatorId;
     if(allowWebProductIdChange()) {
        document.getElementById("image1").src = imageURL + document.getElementById("novatorId").value + imageURLStandard + "." + imageURLType;
        document.getElementById("image2").src = imageURL + document.getElementById("novatorId").value + imageURLDeluxe + "." + imageURLType;
        document.getElementById("image3").src = imageURL + document.getElementById("novatorId").value + imageURLPremium + "." + imageURLType;
        //setTimeout( 'pngFix()', 1000 );
     } else {
        alert(DISALLOW_WEB_PRODUCT_ID_CHANGE_MSG);
     }
 }
 
 function allowWebProductIdChange() {
     var latestWebProductId = document.getElementById("novatorId").value;
     var allowed = true;
    
     //Check if web product id / novator id has changed and product is availabe. If so, disallow the web product id change.
     if(latestWebProductId != webProductId && initProductAvailable == true) {
        allowed = false;
     }
     return allowed;
 }
 
 function changeStandardPrice() {
     standardPrice = document.getElementById("standardPrice").value;
     if (!isFloat(standardPrice) || standardPrice < .01) {
         standardPrice = "0.00";
     } else {
         standardPrice = parseFloat(standardPrice).toFixed(2);
     }
     document.getElementById("standardPrice").value = standardPrice;
     changePrice1();
     if (standardPrice <= 0 || deluxePrice <= 0 || premiumPrice <= 0) {
         disableGBB();
         if (document.getElementById("gbbPopoverFlag").checked) {
             document.getElementById("gbbPopoverFlag").checked = false;
             alert("Good, Better, Best Popover flag has been disabled");
         }
     } else {
         enableGBB();
     }
 }
 
 function changeDeluxePrice() {
     deluxePrice = document.getElementById("deluxePrice").value;
     if (!isFloat(deluxePrice) || deluxePrice < .01) {
         deluxePrice = "0.00";
         if (document.getElementById('defaultPrice2').checked == true) {
             document.getElementById('defaultPrice1').checked = true;
         }
         document.getElementById('defaultPrice2').disabled = true;
         document.getElementById('azCatalogStatusDeluxe').disabled = true;
         document.getElementById('azProductNameDeluxe').disabled = true;
         document.getElementById('azProductDescriptionDeluxe').disabled = true;
     } else {
         deluxePrice = parseFloat(deluxePrice).toFixed(2);
         document.getElementById('defaultPrice2').disabled = false;
         document.getElementById('azCatalogStatusDeluxe').disabled = false;
         document.getElementById('azProductNameDeluxe').disabled = false;
         document.getElementById('azProductDescriptionDeluxe').disabled = false;
     }
     document.getElementById("deluxePrice").value = deluxePrice;
     changePrice2();
     if (standardPrice <= 0 || deluxePrice <= 0 || premiumPrice <= 0) {
         disableGBB();
         if (document.getElementById("gbbPopoverFlag").checked) {
             document.getElementById("gbbPopoverFlag").checked = false;
             alert("Good, Better, Best Popover flag has been disabled");
         }
     } else {
         enableGBB();
     }
 }
 
 function changePremiumPrice() {
     premiumPrice = document.getElementById("premiumPrice").value;
     if (!isFloat(premiumPrice) || premiumPrice < .01) {
         premiumPrice = "0.00";
         if (document.getElementById('defaultPrice3').checked == true) {
             document.getElementById('defaultPrice1').checked = true;
         }
         document.getElementById('defaultPrice3').disabled = true;
         document.getElementById('azCatalogStatusPremium').disabled = true;
         document.getElementById('azProductNamePremium').disabled = true;
         document.getElementById('azProductDescriptionPremium').disabled = true;
     } else {
         premiumPrice = parseFloat(premiumPrice).toFixed(2);
         document.getElementById('defaultPrice3').disabled = false;
         document.getElementById('azCatalogStatusPremium').disabled = false;
         document.getElementById('azProductNamePremium').disabled = false;
         document.getElementById('azProductDescriptionPremium').disabled = false;
     }
     document.getElementById("premiumPrice").value = premiumPrice;
     changePrice3();
     if (standardPrice <= 0 || deluxePrice <= 0 || premiumPrice <= 0) {
         disableGBB();
         if (document.getElementById("gbbPopoverFlag").checked) {
             document.getElementById("gbbPopoverFlag").checked = false;
             alert("Good, Better, Best Popover flag has been disabled");
         }
     } else {
         enableGBB();
     }
 }
 
 function setDropShipCosts() {
     var supplyExpense = document.getElementById("supplyExpense").value;
     var supplyExpenseEffDate = document.getElementById("supplyExpenseEffDate").value;
     if (supplyExpenseEffDate == "") {
         document.getElementById("supplyExpense").value = "";
     } else {
         document.getElementById("supplyExpense").value = parseFloat(supplyExpense).toFixed(2); 
     }
     var royaltyPercent = document.getElementById("royaltyPercent").value;
     var royaltyPercentEffDate = document.getElementById("royaltyPercentEffDate").value;
     if (royaltyPercentEffDate == "") {
         document.getElementById("royaltyPercent").value = "";
     } else {
         document.getElementById("royaltyPercent").value = parseFloat(royaltyPercent).toFixed(0); 
     }
 }
 //add the onclick to the ND ship method
 function assignNextDayOnClick() {
  var shipMethods = document.getElementsByName("shippingMethodList");

  for (var i = 0; i < shipMethods.length; i++) {
   var shipMethod = shipMethods[i];

   if (shipMethod.value == "ND") {
    shipMethod.onclick = function() { onNextDayClick(); } ;

    if (shipMethod.checked == true) {
     document.getElementById("nextDayUpgrade").checked = true;
    } else {
     document.getElementById("nextDayUpgrade").checked = false;
    }
   }
  }
 }

 //if the ND ship method is clicked, click the nextDayUpgrade checkbox
 function onNextDayClick() {
  var shipMethods = document.getElementsByName("shippingMethodList");

  for (var i = 0; i < shipMethods.length; i++) {
   var shipMethod = shipMethods[i];

   if (shipMethod.value == "ND") {
    if (shipMethod.checked == true) {
     document.getElementById("nextDayUpgrade").checked = true;
    } else {
     document.getElementById("nextDayUpgrade").checked = false;
    }
   }
  }
 }

 //make sure drops downs are enabled so they go back to server
 function enableDisabledForSubmit() {
  for ( var i = 0; i < document.forms[0].elements.length; i++ ) {
    document.forms[0].elements[i].disabled = false;  
  }
 }

 // method do do anything you need to do before a submit
 function processInput(list1, list2, submitTo, hiddenSuperSetList) {
  // if Available option for a product is checked, check that inventory has been loaded for the product.
		

		if(hiddenSuperSetList.options.length != list1.options.length){
			
				for (var i=0; i < list1.options.length; i++) {
					list1.options.remove(i);
					i--;
				}
				var oOption;
				var ItemLocation;
				for (var i=0; i < hiddenSuperSetList.options.length; i++) {
							oOption = document.createElement("OPTION")
							ItemLocation = GetItemLocation(hiddenSuperSetList.options(i).text, list1)
							oOption.text = hiddenSuperSetList.options[i].text
							oOption.value = hiddenSuperSetList.options[i].value
							list1.options.add(oOption,ItemLocation);
						
				}		
		}
  
  
  	if ((document.getElementById("shipMethodCarrier").checked == true)) 
  	{
	var availCheckbox = document.getElementById('status');
        var typeId = document.getElementById("shippingSystem").value;
       
        if (availCheckbox.checked == true && typeId == 'SDS'){
		var hasInventory = <%=productForm.get(ProductMaintConstants.HAS_INVENTORY_RECORD)%>;
		if(hasInventory == false){
			alert("No inventory has been loaded for this product");
			availCheckbox.checked = false;
			return;
		}
   	}
   	} 
  // check that subcodes are committed.  Vendor data and subcode data must match.
  if (document.getElementById("subCodeAddButton").style.visibility == "hidden") {
   alert("Please commit the new subcode.");
   return;
  }

  var supplyExpense = document.getElementById("supplyExpense").value;
  var supplyExpenseEffDate = document.getElementById("supplyExpenseEffDate").value;
  var royaltyPercent = document.getElementById("royaltyPercent").value;
  var royaltyPercentEffDate = document.getElementById("royaltyPercentEffDate").value;
  var msg = "";
  
  
  if (supplyExpense == "" && supplyExpenseEffDate != "") {
      msg = msg + "Supplies Expense is required\n";
  } else {
      if (supplyExpenseEffDate == "" && supplyExpense != "") {
          msg = msg + "Supplies Expense Effective Date is required\n";
      }
  }
  if (supplyExpense != "" && (!isFloat(supplyExpense) || supplyExpense < 0)) {
      msg = msg + "Supplies Expense must be a positive number\n";
  }

  if (royaltyPercent == "" && royaltyPercentEffDate != "") {
      msg = msg + "Royalty Percent is required\n";
  } else {
      if (royaltyPercentEffDate == "" && royaltyPercent != "") {
          msg = msg + "Royalty Percent Effective Date is required\n";
      }
  }
  if (royaltyPercent != "" && (!isInteger(royaltyPercent) || royaltyPercent > 100)) {
      msg = msg + "The Royalty Percent must be a number between 0 and 100\n";
  }

  if (document.getElementById("gbbTitle").value == "" && document.getElementById("gbbPopoverFlag").checked) {
      msg = msg + "The Good, Better, Best pop-over must have something in the title field.\nIf you want to show a blank title, please enter a space and resubmit\n";
  }
  
  if (!allowWebProductIdChange()) {
      msg = msg + DISALLOW_WEB_PRODUCT_ID_CHANGE_MSG;
  }
  
  // amazon
  if (document.getElementById('azCatalogStatusStandard').checked) {
      if (document.getElementById('azProductNameStandard').value == "") {
          msg = msg + "Amazon Position 1 Product Name required\n";
      }
      if (document.getElementById('azProductDescriptionStandard').value == "") {
          msg = msg + "Amazon Position 1 Product Description required\n";
      }
  }
  if (document.getElementById('azCatalogStatusDeluxe').checked) {
      if (document.getElementById('azProductNameDeluxe').value == "") {
          msg = msg + "Amazon Position 2 Product Name required\n";
      }
      if (document.getElementById('azProductDescriptionDeluxe').value == "") {
          msg = msg + "Amazon Position 2 Product Description required\n";
      }
  }
  if (document.getElementById('azCatalogStatusPremium').checked) {
      if (document.getElementById('azProductNamePremium').value == "") {
          msg = msg + "Amazon Position 3 Product Name required\n";
      }
      if (document.getElementById('azProductDescriptionPremium').value == "") {
          msg = msg + "Amazon Position 3 Product Description required\n";
      }
  }
  if (document.getElementById('azCatalogStatusStandard').checked || document.getElementById('azCatalogStatusDeluxe').checked 
		  || document.getElementById('azCatalogStatusPremium').checked) {
      if (document.getElementById('azItemType').value == "") {
          msg = msg + "Amazon Item Type is required\n";
      }
  }

  if (msg != "" ) {
      alert(msg);
      return false;
  }

  toggleCheckboxToHidden("vendorAvailableCheckbox", "vendorAvailable");
  toggleShipMethodCarrier();
  checkDateExceptions(false);

  // color list
  for (i = 0; i < list1.length; i++) {
   list1.options[i].selected = true;
  }

  // component sku list
  for (i = 0; i < list2.length; i++) {
   list2.options[i].selected = true;
  }

  if (document.getElementById("gbbTitle").value == defaultTitle) {
      document.getElementById("gbbTitle").value = "";
  }

  if (document.getElementById('defaultPrice3').checked == true) {
      document.getElementById('preferredPricePoint').value = 3;
  } else if (document.getElementById('defaultPrice2').checked == true) {
      document.getElementById('preferredPricePoint').value = 2;
  } else {
      document.getElementById('preferredPricePoint').value = 1;
  }

  //make sure drops downs are enabled so they go back to server
  enableDisabledForSubmit();
  document.getElementById("toggleTabsLink").style.visibility = "hidden";
  popuponclick();
  submitForm(submitTo);
 }

 var lastSubCode = <%=productForm.get(ProductMaintConstants.LAST_SUBCODE_KEY)%>;

 // add a subcodes line
 function addSubCodeLine() {
  lastSubCode++;
  eval('document.all["subCode1Name' + lastSubCode + '"].style.display = ""');
 }

 // remove subcode line
 function removeSubCodeLine(index) {
  eval('document.all["subCode1Name' + index + '"].style.display = "none"');
  eval('document.all["subCodeName' + index + '"].innerHTML = ""');
 }

 // check spelling
 function checkSpelling(value, name) {
  value = escape(value).replace(/\+/g, "%2B");
  var url = "SpellCheckServlet?content=" + value + "&callerName=" + name;
  window.open(url, "Spell_Check", "toolbar=no,resizable=yes,scrollbars=yes,width=500,height=300");
 }

 function disableHoldUntilAvailable() {
  currentType = document.forms[0].productType.value;

  if (currentType == "<%=ProductMaintConstants.PRODUCT_TYPE_FLORAL%>") {
   document.forms[0].holdUntilAvailable.disabled = false;
  } else if (currentType != 'SERVICES') {
   if (document.forms[0].holdUntilAvailable.checked == true) {
    alert("Hold functionality for non-floral product types will be disabled and the hold will be removed.");
    document.forms[0].holdUntilAvailable.checked = false;
   }

   document.forms[0].holdUntilAvailable.disabled = true;
  }
 }
 
  
 /*function disableMorningDelivery4Floral() {
	currentProdType = document.forms[0].productType.value;	
 	if(currentProdType == "<%=ProductMaintConstants.PRODUCT_TYPE_FLORAL%>" || currentProdType == "SERVICES" || currentProdType == "NONE") {
 		document.forms[0].morningDeliveryFlag.checked = false;
   		document.forms[0].morningDeliveryFlag.disabled = true;
  	} else {
  		document.forms[0].morningDeliveryFlag.disabled = false;
  	}
}*/

 function chooseArrangementAvail() {
  var sameDayFreshCutsCode = "<%=ProductMaintConstants.PRODUCT_TYPE_SAME_DAY_FRESH_CUTS_CODE%>";
  var freshCutsCode = "<%=ProductMaintConstants.PRODUCT_TYPE_FRESHCUTS%>";
  var sameDayGiftCode = "<%=ProductMaintConstants.PRODUCT_TYPE_SAME_DAY_GIFT_CODE%>";
  if (document.all.productType.value == sameDayFreshCutsCode || document.all.productType.value == sameDayGiftCode) {
   document.all.addOnBalloonsFlag.checked = false;
   document.all.addOnBalloonsFlag.disabled = true;
   document.all.addOnBearsFlag.checked = false;
   document.all.addOnBearsFlag.disabled = true;
   document.all.addOnChocolateFlag.checked = false;
   document.all.addOnChocolateFlag.disabled = true;
  } else if (document.all.productType.value != 'SERVICES' ){
   document.all.secondChoice.disabled = false;
   document.all.holidaySecondChoice.disabled = false;
   document.all.addOnBalloonsFlag.disabled = false;
   document.all.addOnBearsFlag.disabled = false;
   document.all.addOnChocolateFlag.disabled = false;
  }
 }

 // build vendor dropdown //
 function buildVendorDropdown(populateOriginal) {	 
  var typeId = document.getElementById("shippingSystem").value;
  var element = document.getElementById("vendorListId");
  var selected = document.getElementById("vendorListId").value;
  element.options.length = 0;
  var productSubTypeHidden = document.getElementById("vendorListIdHidden");
  element.appendChild(new Option());

  for (var i = 0; i < productSubTypeHidden.length; i++) {
   if ((typeId == "") || (productSubTypeHidden.options[i].typeId == typeId)) {
    var option = productSubTypeHidden.options[i].cloneNode(true);

    if (populateOriginal && (selected == productSubTypeHidden.options[i].value)) {
     option.selected = true;
    }

    element.appendChild(option);
   }
  }
  return;
 }
 function disableValidShipDays(obj)
 {
	 var shipType = document.getElementById("shippingSystem").value;
	 var parent = FTD_DOM.findFirstAncestorOfType(document.getElementById("flipCheckboxesInTableRowLink"), 'tr');
	 if (parent)
	   {
	     var checkboxNodes = [];
	     FTD_DOM.findDecendentsOfType(parent, 'input', 'checkbox', checkboxNodes);
	   }
	 if(shipType =='FTD WEST'){
	    document.getElementById("flipCheckboxesInTableRowLink").onclick = function() { return false; } ;
	    for (var i = 0; i < checkboxNodes.length; i++) 
	     {
	    	 checkboxNodes[i].disabled=true;
	     }
	 }
	 else
	  {
		 document.getElementById("flipCheckboxesInTableRowLink").onclick = function() { flipCheckboxesInTableRow(this); return false; } ;
		 for (var i = 0; i < checkboxNodes.length; i++) 
	     {
		    checkboxNodes[i].disabled=false;
	     }
	  }
 }
 // build sub type dropdown //
 function buildSubTypeDropdown(populateOriginal) {
  var element = document.getElementById("productSubType");
  var selected = document.getElementById("productSubType").value;
  var typeId = document.getElementById("productType").value;
  element.options.length = 0;
  var productSubTypeHidden = document.getElementById("productSubTypeHidden");
  
  // Don't auto-add blank. There should be a blank already based on the 'None' setting
  // element.appendChild(new Option());

  for (var i = 0; i < productSubTypeHidden.length; i++) {
   if (productSubTypeHidden.options[i].typeId == typeId) {
    var option = productSubTypeHidden.options[i].cloneNode(true);

    if (populateOriginal && (selected == productSubTypeHidden.options[i].value)) {
     option.selected = true;
    }

    element.appendChild(option);
   }
  }
  
  // Auto-select first item if there is only 1 item in the list
  if(element.length == 1) {
    element.options[0].selected = true;
  }
  
  setupFieldsForSubType();
	checkForTemplateId();
  return;
 }

 // change country list //
 function changeCountryList() {
  deliveryTypeList = document.forms[0].deliveryType;
  countryList = document.forms[0].country;

  if (countryList.options != null) {
   countryList.options.length = 0;
  }

  <%
        java.util.List countries = (java.util.List)productForm.get(ProductMaintConstants.COUNTRIES_KEY);
    %>

  if (deliveryTypeList.options[deliveryTypeList.selectedIndex].value == "<%=ProductMaintConstants.DOMESTIC_CODE%>") {
   countryList.options[countryList.options.length] = new Option("<%=ProductMaintConstants.DOMESTIC_COUNTRY_DESC%>", "<%=ProductMaintConstants.DOMESTIC_COUNTRY_CODE%>");
  } else {
   <%
            for(int i = 0; i < countries.size(); i++)
            {
                String countryDesc = ((ValueVO)countries.get(i)).getDescription();
                String countryCode = ((ValueVO)countries.get(i)).getId();
                if(!countryCode.equals(ProductMaintConstants.DOMESTIC_COUNTRY_CODE))
                {
                    out.print("countryList.options[countryList.options.length] = new Option(\"" + countryDesc + "\",\"" + countryCode + "\");\r\n");
                }
            }
         %>
  }

  if (countryList.options[0] != null) {
   countryList.options[0].selected = true;
  }
 }

 function selectMercuryChoice(box1, box2) { box2.options.selectedIndex = box1.options.selectedIndex; }

 var floralType = "<%=ProductMaintConstants.PRODUCT_TYPE_FLORAL%>"
 var countLabel = "<br/>left: ";
 var borderFocusColor = "GoldenRod";
 var labelFocusColor = "DarkGoldenRod";

 <%
    String instruction = StringUtils.trimToNull((String)productForm.get("instruction"));
    String openAllTabsFlag = (String)productForm.get("openAllTabsFlag");
    ActionMessages errors = StrutsUtilities.getErrors(request);
    if ((errors.size() > 0)||(instruction != null))
    {
        openAllTabsFlag = "Y";
    }
    String tabBarClass = "tabTitleBarForcedOpen";
    if( openAllTabsFlag==null ) {
        openAllTabsFlag="N";
    }
    if( openAllTabsFlag.equalsIgnoreCase("N") ) {
        tabBarClass = "accordionTabTitleBar";
%>

 onloads.push(accord);

 function accord() { new Rico.Accordion('accordionDiv', {
  panelHeight: 255,
  onShowTab: scrollToFirstField
 }); }

 <%
    }
%>

 onloads.push(addFTDDOMEvents);

 function addFTDDOMEvents() {
  new FTD_DOM.CharCountDown('longDescription', 'longDescriptionCount', 1024, countLabel);
  new FTD_DOM.CharCountDown('dominantFlowers', 'dominantFlowersCount', 255, countLabel);
  new FTD_DOM.CharCountDown('exceptionMessage', 'exceptionMessageCounter', 280, countLabel);
  new FTD_DOM.CharCountDown('recipe', 'recipeCounter', 255, countLabel);
  new FTD_DOM.CharCountDown('deluxeRecipe', 'deluxeRecipeCounter', 255, countLabel);
  new FTD_DOM.CharCountDown('premiumRecipe', 'premiumRecipeCounter', 255, countLabel);
  new FTD_DOM.CharCountDown('generalComments', 'generalCommentsCounter', 1024, countLabel);

  new FTD_DOM.FocusElement('productName', borderFocusColor, 'productNameLabel', labelFocusColor);
  new FTD_DOM.FocusElement('novatorId', borderFocusColor, 'novatorIdLabel', labelFocusColor);
  new FTD_DOM.FocusElement('novatorName', borderFocusColor, 'novatorNameLabel', labelFocusColor);
  new FTD_DOM.FocusElement('category', borderFocusColor, 'categoryLabel', labelFocusColor);
  new FTD_DOM.FocusElement('companyList', borderFocusColor, 'companyListLabel', labelFocusColor);
  new FTD_DOM.FocusElement('productType', borderFocusColor, 'productTypeLabel', labelFocusColor);
  new FTD_DOM.FocusElement('productSubType', borderFocusColor, 'productSubTypeLabel', labelFocusColor);
  new FTD_DOM.FocusElement('longDescription', borderFocusColor, 'longDescriptionLabel', labelFocusColor);
  new FTD_DOM.FocusElement('excludedColorsArray', borderFocusColor, 'excludedColorsArrayLabel', labelFocusColor);
  new FTD_DOM.FocusElement('arrangementColorsArray', borderFocusColor, 'arrangementColorsArrayLabel', labelFocusColor);
  new FTD_DOM.FocusElement('availableComponentSkuArray', borderFocusColor, 'availableComponentSkuArrayLabel', labelFocusColor);
  new FTD_DOM.FocusElement('componentSkuArray', borderFocusColor, 'componentSkuArrayLabel', labelFocusColor);
  new FTD_DOM.FocusElement('colorSizeFlag', borderFocusColor, 'colorSizeFlagLabel', labelFocusColor);
  new FTD_DOM.FocusElement('arrangementSize', borderFocusColor, 'arrangementSizeLabel', labelFocusColor);
  new FTD_DOM.FocusElement('dominantFlowers', borderFocusColor, 'dominantFlowersLabel', labelFocusColor);
  new FTD_DOM.FocusElement('unspscCode', borderFocusColor, 'unspscCodeLabel', labelFocusColor);
  new FTD_DOM.FocusElement('bulletDescription', borderFocusColor, 'bulletDescriptionLabel', labelFocusColor);
  new FTD_DOM.FocusElement('markerSelectBoxExcluded', borderFocusColor, 'excludedColorsArrayTypeLabel', labelFocusColor);
  new FTD_DOM.FocusElement('markerSelectBoxIncluded', borderFocusColor, 'includedColorsArrayTypeLabel', labelFocusColor);
  new FTD_DOM.FocusElement('over21Flag', borderFocusColor, 'over21FlagLabel', labelFocusColor);
  new FTD_DOM.FocusElement('customFlag', borderFocusColor, 'customFlagLabel', labelFocusColor);
  new FTD_DOM.FocusElement('personalGreetingFlag', borderFocusColor, 'personalGreetingFlagLabel', labelFocusColor);
  new FTD_DOM.FocusElement('premierCollectionFlag', borderFocusColor, 'premierCollectionFlagLabel', labelFocusColor);
  new FTD_DOM.FocusElement('status', borderFocusColor, 'statusLabel', labelFocusColor);
  new FTD_DOM.FocusElement('weboeBlocked', borderFocusColor, 'weboeBlockedLabel', labelFocusColor);
  new FTD_DOM.FocusElement('corporateSiteFlag', borderFocusColor, 'corporateSiteFlagLabel', labelFocusColor);
  new FTD_DOM.FocusElement('eGiftFlag', borderFocusColor, 'eGiftFlagLabel', labelFocusColor);
  new FTD_DOM.FocusElement('exceptionCode', borderFocusColor, 'exceptionCodeLabel', labelFocusColor);
  new FTD_DOM.FocusElement('exceptionStartDate', borderFocusColor, 'exceptionStartDateLabel', labelFocusColor);
  new FTD_DOM.FocusElement('exceptionEndDate', borderFocusColor, 'exceptionEndDateLabel', labelFocusColor);
  new FTD_DOM.FocusElement('exceptionMessage', borderFocusColor, 'exceptionMessageLabel', labelFocusColor);
  new FTD_DOM.FocusElement('holdUntilAvailable', borderFocusColor, 'holdUntilAvailableLabel', labelFocusColor);
  new FTD_DOM.FocusElement('shipMethodFlorist', borderFocusColor, 'shipMethodFloristLabel', labelFocusColor);
  new FTD_DOM.FocusElement('deliveryType', borderFocusColor, 'deliveryTypeLabel', labelFocusColor);
  new FTD_DOM.FocusElement('country', borderFocusColor, 'countryLabel', labelFocusColor);
  new FTD_DOM.FocusElement('floristReferenceNumber', borderFocusColor, 'floristReferenceNumberLabel', labelFocusColor);
  new FTD_DOM.FocusElement('departmentCode', borderFocusColor, 'departmentCodeLabel', labelFocusColor);
  new FTD_DOM.FocusElement('codifiedFlag', borderFocusColor, 'codifiedFlagLabel', labelFocusColor);
  new FTD_DOM.FocusElement('mercuryDescription', borderFocusColor, 'mercuryDescriptionLabel', labelFocusColor);
  new FTD_DOM.FocusElement('recipe', borderFocusColor, 'recipeLabel', labelFocusColor);
  new FTD_DOM.FocusElement('sendStandardRecipe', borderFocusColor, 'sendStandardRecipeLabel', labelFocusColor);
  new FTD_DOM.FocusElement('deluxeRecipe', borderFocusColor, 'deluxeRecipeLabel', labelFocusColor);
  new FTD_DOM.FocusElement('sendDeluxeRecipe', borderFocusColor, 'sendDeluxeRecipeLabel', labelFocusColor);
  new FTD_DOM.FocusElement('premiumRecipe', borderFocusColor, 'premiumRecipeLabel', labelFocusColor);
  new FTD_DOM.FocusElement('sendPremiumRecipe', borderFocusColor, 'sendPremiumRecipeLabel', labelFocusColor);
  new FTD_DOM.FocusElement('itemComments', borderFocusColor, 'itemCommentsLabel', labelFocusColor);
  new FTD_DOM.FocusElement('secondChoice', borderFocusColor, 'secondChoiceLabel', labelFocusColor);
  new FTD_DOM.FocusElement('mercurySecondChoice', borderFocusColor, 'mercurySecondChoiceLabel', labelFocusColor);
  new FTD_DOM.FocusElement('shipMethodCarrier', borderFocusColor, 'shipMethodCarrierLabel', labelFocusColor);
  new FTD_DOM.FocusElement('shippingKey', borderFocusColor, 'shippingKeyLabel', labelFocusColor);
  new FTD_DOM.FocusElement('dimWeight', borderFocusColor, 'dimWeightLabel', labelFocusColor);
  new FTD_DOM.FocusElement('personalizationLeadDays', borderFocusColor, 'personalizationLeadDaysLabel', labelFocusColor);
  new FTD_DOM.FocusElement('zoneJumpEligibleFlag', borderFocusColor, 'zoneJumpEligibleFlagLabel', labelFocusColor);
  new FTD_DOM.FocusElement('shippingSystem', borderFocusColor, 'shippingSystemLabel', labelFocusColor);
  new FTD_DOM.FocusElement('boxId', borderFocusColor, 'boxId', labelFocusColor);
  new FTD_DOM.FocusElement('expressShippingOnly', borderFocusColor, 'expressShippingOnlyLabel', labelFocusColor);
  new FTD_DOM.FocusElement('mondayDeliveryFreshCuts', borderFocusColor, 'mondayDeliveryFreshCutsLabel', labelFocusColor);
  new FTD_DOM.FocusElement('standardPrice', borderFocusColor, 'standardPriceLabel', labelFocusColor);
  new FTD_DOM.FocusElement('deluxePrice', borderFocusColor, 'deluxePriceLabel', labelFocusColor);
  new FTD_DOM.FocusElement('premiumPrice', borderFocusColor, 'premiumPriceLabel', labelFocusColor);
  new FTD_DOM.FocusElement('variablePricingFlag', borderFocusColor, 'variablePricingFlagLabel', labelFocusColor);
  new FTD_DOM.FocusElement('variablePriceMax', borderFocusColor, 'variablePriceMaxLabel', labelFocusColor);
  new FTD_DOM.FocusElement('discountAllowedFlag', borderFocusColor, 'discountAllowedFlagLabel', labelFocusColor);
  new FTD_DOM.FocusElement('taxFlag', borderFocusColor, 'taxFlagLabel', labelFocusColor);
  new FTD_DOM.FocusElement('allowFreeShippingFlag', borderFocusColor, 'freeShippingLabel', labelFocusColor);
  new FTD_DOM.FocusElement('addOnBalloonsFlag', borderFocusColor, 'addOnBalloonsFlagLabel', labelFocusColor);
  new FTD_DOM.FocusElement('addOnBearsFlag', borderFocusColor, 'addOnBearsFlagLabel', labelFocusColor);
  new FTD_DOM.FocusElement('addOnGreetingCardsFlag', borderFocusColor, 'addOnGreetingCardsFlagLabel', labelFocusColor);
  new FTD_DOM.FocusElement('addOnFuneralFlag', borderFocusColor, 'addOnFuneralFlagLabel', labelFocusColor);
  new FTD_DOM.FocusElement('addOnChocolateFlag', borderFocusColor, 'addOnChocolateFlagLabel', labelFocusColor);
  new FTD_DOM.FocusElement('generalComments', borderFocusColor, 'generalCommentsLabel', labelFocusColor);
  new FTD_DOM.FocusElement('sentToNovatorProd', borderFocusColor, 'sentToNovatorProdLabel', labelFocusColor);
  new FTD_DOM.FocusElement('sentToNovatorTest', borderFocusColor, 'sentToNovatorTestLabel', labelFocusColor);
  new FTD_DOM.FocusElement('sentToNovatorUAT', borderFocusColor, 'sentToNovatorUATLabel', labelFocusColor);
  new FTD_DOM.FocusElement('sentToNovatorContent', borderFocusColor, 'sentToNovatorContentLabel', labelFocusColor);
  new FTD_DOM.FocusElement('personalizationTemplate', borderFocusColor, 'personalizationTemplateLabel', labelFocusColor);
  new FTD_DOM.FocusElement('personalizationTemplateOrder', borderFocusColor, 'personalizationTemplateOrderLabel', labelFocusColor);
  new FTD_DOM.FocusElement('personalizationCaseFlag', borderFocusColor, 'personalizationCaseFlagLabel', labelFocusColor);
  new FTD_DOM.FocusElement('allAlphaFlag', borderFocusColor, 'allAlphaFlagLabel', labelFocusColor);
  new FTD_DOM.FocusElement('ftdwestAccessoryId', borderFocusColor, 'ftdwestAccessoryIdLabel', labelFocusColor);
  new FTD_DOM.FocusElement('ftdwestTemplateMapping', borderFocusColor, 'ftdwestTemplateMappingLabel', labelFocusColor);
  new FTD_DOM.FocusElement('recipientSearch', borderFocusColor, 'recipientSearchLabel', labelFocusColor);
  new FTD_DOM.FocusElement('searchPriority', borderFocusColor, 'searchPriorityLabel', labelFocusColor);
  new FTD_DOM.FocusElement('keywordSearch', borderFocusColor, 'keywordSearchLabel', labelFocusColor);
  new FTD_DOM.FocusElement('keywords', borderFocusColor, 'keywordsLabel', labelFocusColor);
  new FTD_DOM.FocusElement('supplyExpense', borderFocusColor, 'supplyExpenseLabel', labelFocusColor);
  new FTD_DOM.FocusElement('supplyExpenseEffDate', borderFocusColor, 'supplyExpenseEffDateLabel', labelFocusColor);
  new FTD_DOM.FocusElement('royaltyPercent', borderFocusColor, 'royaltyPercentLabel', labelFocusColor);
  new FTD_DOM.FocusElement('royaltyPercentEffDate', borderFocusColor, 'royaltyPercentEffDateLabel', labelFocusColor);
  new FTD_DOM.FocusElement('gbbPopoverFlag', borderFocusColor, 'gbbPopoverFlagLabel', labelFocusColor);
  new FTD_DOM.FocusElement('gbbTitle', borderFocusColor, 'gbbTitleLabel', labelFocusColor);
  new FTD_DOM.FocusElement('gbbNameOverrideFlag1', borderFocusColor, 'gbbNameOverrideFlag1Label', labelFocusColor);
  new FTD_DOM.FocusElement('gbbNameOverrideText1', borderFocusColor, 'gbbNameOverrideText1Label', labelFocusColor);
  new FTD_DOM.FocusElement('gbbPriceOverrideFlag1', borderFocusColor, 'gbbPriceOverrideFlag1Label', labelFocusColor);
  new FTD_DOM.FocusElement('gbbPriceOverrideText1', borderFocusColor, 'gbbPriceOverrideText1Label', labelFocusColor);
  new FTD_DOM.FocusElement('gbbNameOverrideFlag2', borderFocusColor, 'gbbNameOverrideFlag2Label', labelFocusColor);
  new FTD_DOM.FocusElement('gbbNameOverrideText2', borderFocusColor, 'gbbNameOverrideText2Label', labelFocusColor);
  new FTD_DOM.FocusElement('gbbPriceOverrideFlag2', borderFocusColor, 'gbbPriceOverrideFlag2Label', labelFocusColor);
  new FTD_DOM.FocusElement('gbbPriceOverrideText2', borderFocusColor, 'gbbPriceOverrideText2Label', labelFocusColor);
  new FTD_DOM.FocusElement('gbbNameOverrideFlag3', borderFocusColor, 'gbbNameOverrideFlag3Label', labelFocusColor);
  new FTD_DOM.FocusElement('gbbNameOverrideText3', borderFocusColor, 'gbbNameOverrideText3Label', labelFocusColor);
  new FTD_DOM.FocusElement('gbbPriceOverrideFlag3', borderFocusColor, 'gbbPriceOverrideFlag3Label', labelFocusColor);
  new FTD_DOM.FocusElement('gbbPriceOverrideText3', borderFocusColor, 'gbbPriceOverrideText3Label', labelFocusColor);
  new FTD_DOM.FocusElement('websites', borderFocusColor, 'websitesLabel', labelFocusColor);
  new FTD_DOM.FocusElement('duration', borderFocusColor, 'durationLabel', labelFocusColor);
  new FTD_DOM.FocusElement('azCatalogStatusStandard', borderFocusColor, 'azCatalogStatusStandardLabel', labelFocusColor);
  new FTD_DOM.FocusElement('azProductNameStandard', borderFocusColor, 'azProductNameStandardLabel', labelFocusColor);
  new FTD_DOM.FocusElement('azProductDescriptionStandard', borderFocusColor, 'azProductDescriptionStandardLabel', labelFocusColor);
  new FTD_DOM.FocusElement('azCatalogStatusDeluxe', borderFocusColor, 'azCatalogStatusDeluxeLabel', labelFocusColor);
  new FTD_DOM.FocusElement('azProductNameDeluxe', borderFocusColor, 'azProductNameDeluxeLabel', labelFocusColor);
  new FTD_DOM.FocusElement('azProductDescriptionDeluxe', borderFocusColor, 'azProductDescriptionDeluxeLabel', labelFocusColor);
  new FTD_DOM.FocusElement('azCatalogStatusPremium', borderFocusColor, 'azCatalogStatusPremiumLabel', labelFocusColor);
  new FTD_DOM.FocusElement('azProductNamePremium', borderFocusColor, 'azProductNamePremiumLabel', labelFocusColor);
  new FTD_DOM.FocusElement('azProductDescriptionPremium', borderFocusColor, 'azProductDescriptionPremiumLabel', labelFocusColor);
  new FTD_DOM.FocusElement('azBulletPoint1', borderFocusColor, 'azBulletPoint1Label', labelFocusColor);
  new FTD_DOM.FocusElement('azBulletPoint2', borderFocusColor, 'azBulletPoint2Label', labelFocusColor);
  new FTD_DOM.FocusElement('azBulletPoint3', borderFocusColor, 'azBulletPoint3Label', labelFocusColor);
  new FTD_DOM.FocusElement('azBulletPoint4', borderFocusColor, 'azBulletPoint4Label', labelFocusColor);
  new FTD_DOM.FocusElement('azBulletPoint5', borderFocusColor, 'azBulletPoint5Label', labelFocusColor);
  new FTD_DOM.FocusElement('azSearchTerm1', borderFocusColor, 'azSearchTerm1Label', labelFocusColor);
  new FTD_DOM.FocusElement('azSearchTerm2', borderFocusColor, 'azSearchTerm2Label', labelFocusColor);
  new FTD_DOM.FocusElement('azSearchTerm3', borderFocusColor, 'azSearchTerm3Label', labelFocusColor);
  new FTD_DOM.FocusElement('azSearchTerm4', borderFocusColor, 'azSearchTerm4Label', labelFocusColor);
  new FTD_DOM.FocusElement('azSearchTerm5', borderFocusColor, 'azSearchTerm5Label', labelFocusColor);
  new FTD_DOM.FocusElement('azIntendedUse1', borderFocusColor, 'azIntendedUse1Label', labelFocusColor);
  new FTD_DOM.FocusElement('azIntendedUse2', borderFocusColor, 'azIntendedUse2Label', labelFocusColor);
  new FTD_DOM.FocusElement('azIntendedUse3', borderFocusColor, 'azIntendedUse3Label', labelFocusColor);
  new FTD_DOM.FocusElement('azIntendedUse4', borderFocusColor, 'azIntendedUse4Label', labelFocusColor);
  new FTD_DOM.FocusElement('azIntendedUse5', borderFocusColor, 'azIntendedUse5Label', labelFocusColor);
  new FTD_DOM.FocusElement('azTargetAudience1', borderFocusColor, 'azTargetAudience1Label', labelFocusColor);
  new FTD_DOM.FocusElement('azTargetAudience2', borderFocusColor, 'azTargetAudience2Label', labelFocusColor);
  new FTD_DOM.FocusElement('azTargetAudience3', borderFocusColor, 'azTargetAudience3Label', labelFocusColor);
  new FTD_DOM.FocusElement('azTargetAudience4', borderFocusColor, 'azTargetAudience4Label', labelFocusColor);
  new FTD_DOM.FocusElement('azOtherAttribute1', borderFocusColor, 'azOtherAttribute1Label', labelFocusColor);
  new FTD_DOM.FocusElement('azOtherAttribute2', borderFocusColor, 'azOtherAttribute2Label', labelFocusColor);
  new FTD_DOM.FocusElement('azOtherAttribute3', borderFocusColor, 'azOtherAttribute3Label', labelFocusColor);
  new FTD_DOM.FocusElement('azOtherAttribute4', borderFocusColor, 'azOtherAttribute4Label', labelFocusColor);
  new FTD_DOM.FocusElement('azOtherAttribute5', borderFocusColor, 'azOtherAttribute5Label', labelFocusColor);
  new FTD_DOM.FocusElement('azSubjectMatter1', borderFocusColor, 'azSubjectMatter1Label', labelFocusColor);
  new FTD_DOM.FocusElement('azSubjectMatter2', borderFocusColor, 'azSubjectMatter2Label', labelFocusColor);
  new FTD_DOM.FocusElement('azSubjectMatter3', borderFocusColor, 'azSubjectMatter3Label', labelFocusColor);
  new FTD_DOM.FocusElement('azSubjectMatter4', borderFocusColor, 'azSubjectMatter4Label', labelFocusColor);
  new FTD_DOM.FocusElement('azSubjectMatter5', borderFocusColor, 'azSubjectMatter5Label', labelFocusColor);
  new FTD_DOM.FocusElement('azItemType', borderFocusColor, 'azItemTypeLabel', labelFocusColor);
  new FTD_DOM.FocusElement('pquadProductID', borderFocusColor, 'pquadProductIDLabel', labelFocusColor);
  <%
    List shipMethodsList = (List)session.getAttribute(ProductMaintConstants.SHIPPING_METHODS_KEY);
    for( int didx=0; didx<shipMethodsList.size(); didx++ ) {
    %>

  new FTD_DOM.FocusElement('shipcheckbox<%=didx%>', borderFocusColor, 'shipcheckboxLabel<%=didx%>', labelFocusColor);

  <%
    }
    %>

  new FTD_DOM.FocusLabel('standardPriceRank', 'standardPriceLabel', labelFocusColor);
  new FTD_DOM.FocusLabel('deluxePriceRank', 'deluxePriceLabel', labelFocusColor);
  new FTD_DOM.FocusLabel('premiumPriceRank', 'premiumPriceLabel', labelFocusColor);

  new FTD_DOM.FocusLabel('vsdLink', 'validShipDaysLabel', labelFocusColor);
  new FTD_DOM.FocusElement('shipDaySunday', borderFocusColor, 'validShipDaysLabel', labelFocusColor);
  new FTD_DOM.FocusElement('shipDayMonday', borderFocusColor, 'validShipDaysLabel', labelFocusColor);
  new FTD_DOM.FocusElement('shipDayTuesday', borderFocusColor, 'validShipDaysLabel', labelFocusColor);
  new FTD_DOM.FocusElement('shipDayWednesday', borderFocusColor, 'validShipDaysLabel', labelFocusColor);
  new FTD_DOM.FocusElement('shipDayThursday', borderFocusColor, 'validShipDaysLabel', labelFocusColor);
  new FTD_DOM.FocusElement('shipDayFriday', borderFocusColor, 'validShipDaysLabel', labelFocusColor);
  new FTD_DOM.FocusElement('shipDaySaturday', borderFocusColor, 'validShipDaysLabel', labelFocusColor);

  <%
    java.util.List esl = (java.util.List)productForm.get(ProductMaintConstants.PDB_EXCLUDED_STATES_KEY );
    for(int idx = 0; idx < esl.size(); idx++) {
    %>

  new FTD_DOM.FocusLabel('ddExclusionLink<%=idx%>', 'ddExclusionLabel', labelFocusColor);
  new FTD_DOM.FocusElement('deliveryDaySunday<%=idx%>', borderFocusColor, 'ddExclusionLabel', labelFocusColor);
  new FTD_DOM.FocusElement('deliveryDayMonday<%=idx%>', borderFocusColor, 'ddExclusionLabel', labelFocusColor);
  new FTD_DOM.FocusElement('deliveryDayTuesday<%=idx%>', borderFocusColor, 'ddExclusionLabel', labelFocusColor);
  new FTD_DOM.FocusElement('deliveryDayWednesday<%=idx%>', borderFocusColor, 'ddExclusionLabel', labelFocusColor);
  new FTD_DOM.FocusElement('deliveryDayThursday<%=idx%>', borderFocusColor, 'ddExclusionLabel', labelFocusColor);
  new FTD_DOM.FocusElement('deliveryDayFriday<%=idx%>', borderFocusColor, 'ddExclusionLabel', labelFocusColor);
  new FTD_DOM.FocusElement('deliveryDaySaturday<%=idx%>', borderFocusColor, 'ddExclusionLabel', labelFocusColor);

  <%
    }
    %>

  <%
    java.util.List msl = (java.util.List)productForm.get(ProductMaintConstants.PRODUCT_MASTER_SKU_KEY );
    java.util.List scl = (java.util.List)productForm.get(ProductMaintConstants.SUB_CODE_LIST_KEY);
    if (msl.size() <= 0) {
        for(int idx = 0; idx < scl.size(); idx++) {
        %>

  new FTD_DOM.FocusElement('productSubCodeId<%=idx%>', borderFocusColor, 'subcodeLabel', labelFocusColor);
  new FTD_DOM.FocusElement('productSubCodeDescription<%=idx%>', borderFocusColor, 'subcodeDescriptionLabel', labelFocusColor);
  new FTD_DOM.FocusElement('productSubCodeReference<%=idx%>', borderFocusColor, 'subcodeReferenceLabel', labelFocusColor);
  new FTD_DOM.FocusElement('productSubCodePrice<%=idx%>', borderFocusColor, 'subcodePriceLabel', labelFocusColor);
  new FTD_DOM.FocusElement('productSubCodeDimWeight<%=idx%>', borderFocusColor, 'subcodeDimWtLabel', labelFocusColor);

  <%
        }
    }
    %>

  <%
    if (scl.size()>0) {
        for(int idx = 0; idx < scl.size(); idx++) {
    %>

  //              new FTD_DOM.FocusElement( 'subCodeVendorSku<%=idx%>', borderFocusColor, 'vendorSubcodeSkuLabel', labelFocusColor );
  //              new FTD_DOM.FocusElement( 'subCodeVendorPrice<%=idx%>', borderFocusColor, 'vendorSubcodeCostLabel', labelFocusColor );
  //              new FTD_DOM.FocusElement( 'subCodeVendorAvailable<%=idx%>', borderFocusColor, 'vendorSubcodeAvailLabel', labelFocusColor );
  <%
        }
    }
    else {
    }
    %>
 }

 function popuponclick() {
  my_window = window.open("", "mywindow", "width=350,height=150");
  my_window.document.write('<html><head><title>Status Window</title></head><body><H2>Processing...</H2><p><img src="images/example_please_wait.gif" alt="please wait"></p></body></html>');
 }
</script>
  
<script language = "JavaScript" type = "text/javascript">
 function scrollToFirstField(tab) {
  var fieldId = FTD_DOM.findFirstInputFieldId(tab.content);

  if (fieldId && fieldId.length > 0) {
   document.getElementById(fieldId).focus();
  }
 }

 function flipCheckboxesInTableRow(e) {
  var currentState = Trim(e.innerHTML);
  var parent = FTD_DOM.findFirstAncestorOfType(e, 'tr');

  if (parent) {
   var checkboxNodes = [];
   FTD_DOM.findDecendentsOfType(parent, 'input', 'checkbox', checkboxNodes);

   for (var i = 0; i < checkboxNodes.length; i++) {
    if (currentState == selectAll) {
     checkboxNodes[i].checked = true;
    } else {
     checkboxNodes[i].checked = false;
    }
   }

   if (currentState == selectAll) {
    e.innerHTML = deselectAll;
   } else {
    e.innerHTML = selectAll;
   }
  }
 }

 // move subcode row up
 function moveSubcodeRowUp(e) {
  //Since the image buttons are in their own table, transverse the DOM tree
  //up to the next level
  var imageRow = FTD_DOM.findFirstAncestorOfType(e, 'table');
  moveRowUp(imageRow);
 }

 // move subcode row up helper
 function moveRowUp(e) {
  if (e) {
   var currentRow = FTD_DOM.findFirstAncestorOfType(e, 'tr');

   if (currentRow) {
    var targetRow = currentRow.previousSibling;

    if (targetRow) {
     //Check to see if the target row is not the header row (TH vs TD)
     var testTD = targetRow.firstChild;

     if (testTD && testTD.nodeName == 'TD') {
      var parentTbody = FTD_DOM.findFirstAncestorOfType(currentRow, 'tbody');

      if (parentTbody) {
       parentTbody.insertBefore(currentRow, targetRow);
      }
     }
    }
   }
  }
 }

 // move subcode down
 function moveSubcodeRowDown(e) {
  //Since the image buttons are in their own table, transverse the DOM tree
  //up to the next level
  var imageRow = FTD_DOM.findFirstAncestorOfType(e, 'table');
  moveRowDown(imageRow);
 }

 // move subcode down helper
 function moveRowDown(e) {
  if (e) {
   var currentRow = FTD_DOM.findFirstAncestorOfType(e, 'tr');

   if (currentRow) {
    var parentTbody = FTD_DOM.findFirstAncestorOfType(currentRow, 'tbody');

    if (parentTbody) {
     //Check to see if the row is already on the bottom
     var targetRow = currentRow.nextSibling;

     if (targetRow) {
      parentTbody.appendChild(currentRow);
     }
    }
   }
  }
 }

 // add a vendor //
 function addVendor() {
  if (document.getElementById("subCodeAddButton").style.visibility == "hidden") {
   alert("Please commit the new subcode before adding a vendor.");
   return;
  }

  var vendors = document.getElementsByName("vendorId");
  var selected = document.getElementById("vendorListId").value;

  if (selected == "") {
   alert("Please select a vendor.");
  } else {
   var error;
	var existingVendorIds = {};
	var vendorsCount = 0;
   for (var i = 0; i < vendors.length; i++) {
    if (vendors[i].value == selected) {
     error = "This vendor has already been selected.";
    }
    	if(existingVendorIds[vendors[i].value.toLowerCase()] == undefined 
    	    || existingVendorIds[vendors[i].value.toLowerCase()] == null)
	    {
    		existingVendorIds[vendors[i].value.toLowerCase()] = vendors[i].value.toLowerCase();
    		vendorsCount++;
	    }
   }
	/* if(vendorsCount >= 6)
	{
		error= "Six vendors are already assigned to this product.\nPlease remove one (or more) before trying to add a new vendor.";
	} */
   if (error == null) { //NOT AN SDS PRODUCT
    var typeId = document.getElementById("shippingSystem").value;

    if (typeId != "SDS" && vendors.length > 0) {
     error = "This shipping system for this product only allows one vendor to be selected";
    }
   }

   if (error == null) {
    var subCodes = document.getElementsByName('productSubCodeId');

    if (subCodes.length > 0) {
     addProductVendor("Subcode");
    } else {
     addProductVendor("Product");
    }
   } else {
    alert(error);
   }
  }
 }

 var vendorCount = 0;

 // add product vendor
 function addProductVendor(columnName) {
  var vendorId = document.getElementById('vendorListId').value;
  var subcodes = document.getElementsByName('productSubCodeId');
  var hasSubcodes = false;

  if (subcodes.length > 0) {
   hasSubcodes = true;
  }

  if (vendorCount == 0) {
   vendorCount = 1;
  }

  vendorCount++;
  var div = document.getElementById('div_vendor_context');
  div.appendChild(document.createElement('br'));

  var outerTable = document.createElement('table');
  outerTable.style.borderColor = '#336699';
  outerTable.border = '3';
  outerTable.id = "vendor" + vendorId + "Table";
  var outerTbody = document.createElement('tbody');
  var outerTR = document.createElement('tr');
  var outerTD = document.createElement('td');

  var innerTable = document.createElement('table');
  innerTable.border = '1';
  innerTable.cellpadding = '2';
  innerTable.cellspacing = '0';

  var caption = document.createElement('caption');
  caption.style.backgroundColor = '#336699';
  caption.style.color = '#eeeeee';
  caption.style.fontWeight = 'bold';
  caption.style.textAlign = 'left';
  caption.style.paddingLeft = '5px';
  caption.appendChild(document.createTextNode(getSelectedVendorText() + " "));

  var a = document.createElement('a');
  a.href = "#";
  a.onclick = function() { removeVendor("vendor" + vendorId + "Table"); } ;
  a.style.color = '#eeeeee';
  a.appendChild(document.createTextNode("remove"));
  caption.appendChild(a);
  innerTable.appendChild(caption);

  var tbody = document.createElement('tbody');
  tbody.id = 'vendorTableId' + vendorCount;

  newTR = document.createElement('tr');
  var newTH = document.createElement('th');
  newTR.appendChild(newTH);
  newTH.className = 'LabelCenter'
  newTH.scope = 'col';
  newTH.appendChild(document.createTextNode(columnName));

  newTH = document.createElement('th');
  newTH.className = 'LabelCenter'
  newTH.scope = 'col';
  newTH.appendChild(document.createTextNode('Vendor SKU'));
  newTR.appendChild(newTH);

  newTH = document.createElement('th');
  newTH.className = 'LabelCenter'
  newTH.scope = 'col';
  newTH.appendChild(document.createTextNode('Cost'));
  newTR.appendChild(newTH);

  newTH = document.createElement('th');
  newTH.className = 'LabelCenter'
  newTH.scope = 'col';
  newTH.appendChild(document.createTextNode('Available'));
  newTR.appendChild(newTH);

  newTH = document.createElement('th');
  newTH.className = 'LabelCenter'
  newTH.scope = 'col';
  newTH.appendChild(document.createTextNode(''));
  newTR.appendChild(newTH);

  tbody.appendChild(newTR);
  var length = 0;

  if (hasSubcodes == true)
   numRows = subcodes.length;
  else
   numRows = 1;

  for (var i = 0; i < numRows; i++) {
   newTR = document.createElement('tr');
   tbody.appendChild(newTR);
   newTD = document.createElement('td');
   newTR.appendChild(newTD);
   newTD.align = 'center';
   var productCode = document.getElementById('productId').value;

   if (hasSubcodes == true)
    productCode = subcodes[i].value;

   newTD.appendChild(document.createTextNode(productCode));

   var hidden = document.createElement('input');
   hidden.name = 'vendorId';
   hidden.type = 'hidden';
   hidden.id = 'vendorId';
   hidden.value = vendorId;
   newTD.appendChild(hidden);
   var hidden = document.createElement('input');
   hidden.name = 'vendorName';
   hidden.type = 'hidden';
   hidden.id = 'vendorName';
   vendorList = document.getElementById('vendorListId');
   hidden.value = vendorList.options[vendorList.selectedIndex].vendorName
   newTD.appendChild(hidden);
   var hidden = document.createElement('input');
   hidden.name = 'productSubcodeVendorId';
   hidden.type = 'hidden';
   hidden.id = 'productSubcodeVendorId';
   hidden.name = 'productSubcodeVendorId';
   hidden.value = productCode;
   newTD.appendChild(hidden);

   newTD = document.createElement('td');
   newTR.appendChild(newTD);
   newTD.align = 'center';
   var newInput = document.createElement('input');
   newInput.name = 'vendorSKU';
   newInput.className = 'SubCode';
   newInput.type = 'text';
   newInput.id = 'vendorSKU';
   newInput.name = 'vendorSKU';
   newInput.size = '20';
   newInput.maxlength = '32';
   newTD.appendChild(newInput);
   var shipType = document.getElementById("shippingSystem").value;
   if(shipType =='FTD WEST'){
	   newInput.readOnly=true;
	   newInput.value=document.getElementById("pquadProductID").value;
   }
   newTD = document.createElement('td');
   newTR.appendChild(newTD);
   newTD.align = 'center';
   newInput = document.createElement('input');
   newInput.className = 'SubCode';
   newInput.type = 'text';
   newInput.id = 'vendorCost';
   newInput.name = 'vendorCost';
   newInput.size = '5';
   newInput.maxlength = '8';
   if(shipType =='FTD WEST'){
	   newInput.readOnly=true;
	   newInput.value='0.0';
   }
   newInput.title = '<%=ProductMaintConstants.PDB_PRODUCT_MAINT_TIP_MONEY%>';
   newTD.appendChild(newInput);
   newTD = document.createElement('td');
   newTR.align = 'center';

   var newCheckbox = document.createElement('input');
   newCheckbox.id = 'vendorAvailableCheckbox';
   newCheckbox.name = 'vendorAvailableCheckbox';
   newCheckbox.onclick = function() { toggleAvailability(); } ;
   newCheckbox.type = 'checkbox';
   newCheckbox.productCode = productCode;
   newCheckbox.vendorId = vendorId;
   newTD.appendChild(newCheckbox);
   var newHidden = document.createElement('input');
   newHidden.id = 'vendorAvailable';
   newHidden.name = 'vendorAvailable';
   newHidden.type = 'hidden';
   newTD.appendChild(newHidden);
   newTR.appendChild(newTD);
  }

  innerTable.appendChild(tbody);
  outerTD.appendChild(innerTable);
  outerTR.appendChild(outerTD);
  outerTbody.appendChild(outerTR);
  outerTable.appendChild(outerTbody);
  div.appendChild(outerTable);
 //document.getElementById('vendorSku'+vendorCount).focus();
 }

 // add selected vendor text //
 function getSelectedVendorText() {
  var vendorCombo = document.getElementById('vendorListId');
  var selectedIndex = vendorCombo.selectedIndex;

  if (selectedIndex > -1) {
   return vendorCombo.options[selectedIndex].text;
  }

  return "";
 }

 // add selected vendor value //
 function getSelectedVendorValue() {
  var vendorCombo = document.getElementById('vendorListId');
  var selectedIndex = vendorCombo.selectedIndex;

  if (selectedIndex > -1) {
   return vendorCombo.options[selectedIndex].value;
  }

  return "";
 }

 // remove sub code //
 function removeSubCode() {
  var productSubcodes = document.getElementsByName("productSubCodeId");
  var newSubcode = productSubcodes[productSubcodes.length - 1];
  var numSubcodes = productSubcodes.length;
  var table = newSubcode.parentNode.parentNode;
  var tr = newSubcode.parentNode;
  table.removeNode(tr);
  document.getElementById("subCodeAddButton").style.visibility = "visible";
  document.getElementById("subCodeCommitButton").style.visibility = "hidden";
  document.getElementById("subCodeRemoveButton").style.visibility = "hidden";

  if (numSubcodes == 1)
   document.getElementById("subcodeTable").style.visibility = "hidden";
 }

 var subCodeCount = 0;

 // commit subcude - we need to have a commit so we can not have people changing //
 // subcode info on the fly //
 function commitSubCode() {
  var productSubcodes = document.getElementsByName("productSubCodeId");
  var newSubcode = productSubcodes[productSubcodes.length - 1];

  if (newSubcode.value == "" || newSubcode.value.length < 4) {
   alert("Subcode ID length must be 4 characters.");
   return;
  }

  if (newSubcode.value == document.getElementById("productId").value) {
   alert("Subcode ID cannot be the same as Product ID.");
   return;
  }

  for (var i = 0; i < (productSubcodes.length - 1); i++) {
   if (productSubcodes[i].value == newSubcode.value) {
    alert("New Subcode ID must be unique.")
    return;
   }
  }

  //XMLHttpRequest - Validate with dataase call that subcode does not exist in the system
  var newRequest = new FTD_AJAX.Request('productMaintAjaxAction.do', FTD_AJAX_REQUEST_TYPE_POST, validateSubcodeCallback, false);
  newRequest.addParam('action_type', 'validate_subcode');
  newRequest.addParam('product_id', newSubcode.value);
  newRequest.send();
 //processing continues in validateSubcodeCallback
 }

 function validateSubcodeCallback(data) {
  if (data.length >= 7) {
   var result = data.substring(0, 7);

   if (result == 'success') {
    var productSubcodes = document.getElementsByName("productSubCodeId");
    var newSubcode = productSubcodes[productSubcodes.length - 1];
    var productSubCodeAvailableCheckbox = document.getElementsByName("productSubCodeAvailableCheckbox");
    productSubCodeAvailableCheckbox[productSubcodes.length - 1].id = "productSubCodeAvailableCheckbox" + newSubcode.value;
    var productSubCodeAvailable = document.getElementsByName("productSubCodeAvailable");
    productSubCodeAvailable[productSubcodes.length - 1].id = "productSubCodeAvailable" + newSubcode.value;
    newSubcode.readOnly = true;
    document.getElementById("subCodeAddButton").style.visibility = "visible";
    document.getElementById("subCodeCommitButton").style.visibility = "hidden";
    document.getElementById("subCodeRemoveButton").style.visibility = "hidden";
    alert("The subcode was committed.");
   } else
    alert(data);
  } else
   alert(data);
 }

 // add sub code //
 function addSubCode() {
  var productType = document.getElementById("productType").value;

  if (productType == "FLORAL") {
   alert("Florist products cannot have subcodes.");
   return;
  }
  else
  {
    alert("Subcodes have been retired. Please make product unavailable.");
    return;
  }

  var confirmRemove = confirm("Adding a subcode will remove all vendors.  Do you want to proceed?");

  if (confirmRemove) {
   removeAllVendors();

   if (document.getElementById("subcodeTable").style.visibility == "hidden")
    document.getElementById("subcodeTable").style.visibility = "visible";

   document.getElementById("subCodeAddButton").style.visibility = "hidden";
   document.getElementById("subCodeCommitButton").style.visibility = "visible";
   document.getElementById("subCodeRemoveButton").style.visibility = "visible";

   if (subCodeCount == 0) {
    var subCodes = document.getElementsByName('productSubCodeId');
    subCodeCount = subCodes.length;
   }

   subCodeCount++;
   var tbody = document.getElementById('subCodeTableId');
   var newRow = document.createElement('tr');
   var newColumn = document.createElement('td');
   newColumn.align = 'center';
   var content = document.createElement('input');
   content.id = 'productSubCodeId';
   content.name = 'productSubCodeId';
   content.type = 'text';
   content.className = 'SubCode';
   content.size = '3';
   content.maxlength = '10';
   newColumn.appendChild(content);
   newRow.appendChild(newColumn);

   newColumn = document.createElement('td');
   content = document.createElement('input');
   content.id = 'productSubCodeDescription' + subCodeCount;
   content.name = 'productSubCodeDescription';
   content.type = 'text';
   content.className = 'SubCode';
   content.size = '25';
   content.maxlength = '100';
   newColumn.appendChild(content);
   newRow.appendChild(newColumn);

   newColumn = document.createElement('td');
   content = document.createElement('input');
   content.id = 'productSubCodeReference' + subCodeCount;
   content.name = 'productSubCodeReference';
   content.type = 'text';
   content.className = 'SubCode';
   content.size = '25';
   content.maxlength = '40';
   newColumn.appendChild(content);
   newRow.appendChild(newColumn);

   newColumn = document.createElement('td');
   content = document.createElement('input');
   content.id = 'productSubCodePrice' + subCodeCount;
   content.name = 'productSubCodePrice';
   content.type = 'text';
   content.className = 'SubCode';
   content.size = '5';
   content.maxlength = '8';
   content.title = '<%=ProductMaintConstants.PDB_PRODUCT_MAINT_TIP_MONEY%>';
   newColumn.appendChild(content);
   newRow.appendChild(newColumn);

   newColumn = document.createElement('td');
   newColumn.align = 'center';
   content = document.createElement('input');
   content.id = 'productSubCodeDimWeight' + subCodeCount;
   content.name = 'productSubCodeDimWeight';
   content.type = 'text';
   content.className = 'SubCode';
   content.size = '2';
   content.maxlength = '10';
   newColumn.appendChild(content);
   newRow.appendChild(newColumn);

   newColumn = document.createElement('td');
   newColumn.align = 'center';
   content = document.createElement('input');
   content.id = 'productSubCodeAvailableCheckbox';
   content.name = 'productSubCodeAvailableCheckbox';
   content.type = 'checkbox';
   content.className = 'SubCode';
   content.disabled = true;
   newColumn.appendChild(content);
   content = document.createElement('input');
   content.id = 'productSubCodeAvailable';
   content.name = 'productSubCodeAvailable';
   content.type = 'hidden';
   content.className = 'SubCode';
   newColumn.appendChild(content);
   newRow.appendChild(newColumn);

   newColumn = document.createElement('td');
   newColumn.align = 'center';
   var arrowsTable = document.createElement('table');
   arrowsTable.border = '0';
   arrowsTable.cellpadding = '0';
   arrowsTable.cellspacing = '0';
   var arrowsBody = document.createElement('tbody');
   var arrowsNewColumn = document.createElement('tr');
   var arrowsNewRow = document.createElement('td');
   var arrowsContent = document.createElement('img');
   arrowsContent.src = './images/arrowUp.gif';
   arrowsContent.alt = 'arrow up';
   arrowsNewRow.appendChild(arrowsContent);
   arrowsContent = document.createElement('img');
   arrowsContent.src = './images/arrowDown.gif';
   arrowsContent.alt = 'arrow down';
   arrowsNewRow.appendChild(arrowsContent);
   arrowsNewColumn.appendChild(arrowsNewRow);
   arrowsBody.appendChild(arrowsNewColumn);
   arrowsTable.appendChild(arrowsBody);
   newColumn.appendChild(arrowsTable);
   newRow.appendChild(newColumn);
   tbody.appendChild(newRow);
  }
 }

 // Checkboxes don't submit if not checked.  To make the vendor[] equal lengths,
 // we hide the checkbox value in a hidden
 function toggleCheckboxToHidden(checkboxName, hiddenName) {
  var checkbox = document.getElementsByName(checkboxName);
  var hidden = document.getElementsByName(hiddenName);

  if (checkbox != null) {
   for (var i = 0; i < checkbox.length; i++) {
    if (checkbox[i].checked == true) {
     hidden[i].value = "true";
    } else if (checkbox[i].checked == false) {
     hidden[i].value = "false";
    }
   }
  }
 }

 // function to expand and close tabs //
 function toggleTabs() {
  //document.getElementById('openAllTabsFlag').value="N";
  var tabToggle = document.getElementById('openAllTabsFlag');

  if (tabToggle.value == "N") {
   tabToggle.value = "Y";
  } else {
   tabToggle.value = "N";
  }

  // color list
  var tempArray = document.getElementById("includedColorsStaticArray");
  for (i = 0; i < tempArray.length; i++) {
   tempArray.options[i].selected = true;
  }
  // component sku list
  var tempArray = document.getElementById("componentSkuArray");
  for (i = 0; i < tempArray.length; i++) {
   tempArray.options[i].selected = true;
  }

  toggleCheckboxToHidden("vendorAvailableCheckbox", "vendorAvailable");
  enableDisabledForSubmit();
  submitForm('reloadProductMaintDetail.do');
 //window.location='reloadProductMaintDetail.do';
 }

  function onStatusChanged() {
    var availCheckbox = document.getElementById('status');
    /*if (availCheckbox.checked == false && okForUnavailable == false) {
        availMsg = 'This sku is marked as the default sku on the product page.\n' +
            'Please change it in Upsell maintenance and then you can mark it\n' +
            'as an unavailable product on the Product details screen.';
        alert(availMsg);
        document.getElementById('status').checked = true;
        return;
    }*/
  }

  // Store Personalization Template Order Dropdown
  function buildPersonalizationTemplateOrder() {
    var dropdown = document.getElementById("personalizationTemplateOrder");
    //iterate through vendor nodes and set to null
    for( var i = dropdown.length-1; i >= 0; i-- ) {
        dropdown.options[i] = null;
    }

    var template = document.getElementById("personalizationTemplate");
    var newRequest = new FTD_AJAX.Request('productMaintAjaxAction.do', FTD_AJAX_REQUEST_TYPE_POST, buildPersonalizationTemplateOrderResponse, false);
    newRequest.addParam('action_type', 'get_template_order');
    newRequest.addParam('template_id', template.options[template.selectedIndex].value);
    newRequest.send();
  }

  // Store Personalization Template Order Dropdown
  function buildPersonalizationTemplateOrderResponse(data) {
    var order = data.split("_");

    var dropdown = document.getElementById("personalizationTemplateOrder");
    // Start at position 1 since the template id is in position 0
    for(var j = 0; j < order.length; j++)
    {
      dropdown.options[j] = new Option(order[j], order[j]);
    }
    dropdown.options[order.length - 1] = null;
  }

  
  function prepareForCopy(arrangementColorsArray, componentSkuArray, submitTo, includedColorsStaticArray){
	processInput(arrangementColorsArray, componentSkuArray, submitTo, includedColorsStaticArray);
  }
  
  function changeProductId(val){
	var prodId = val.toUpperCase();  
	document.getElementById("productId").value = prodId;
	var elems = document.getElementsByTagName("input");
	for(var i = 0; i < elems.length; i++){
		if(elems[i].id == "productSubcodeVendorId"){
			elems[i].value = "";
			elems[i].value = prodId;
		}	
	}
  }

  function checkCatalogStatusStandard() {
      //alert(document.getElementById("azCatalogStatusStandard").checked);
      if (document.getElementById("azCatalogStatusStandard").checked == true) {
          document.getElementById("azProductNameStandard").disabled = false;
          document.getElementById("azProductDescriptionStandard").disabled = false;
      } else {
          document.getElementById("azProductNameStandard").disabled = true;
          document.getElementById("azProductDescriptionStandard").disabled = true;
      }
  }

  function checkCatalogStatusDeluxe() {
      //alert(document.getElementById("azCatalogStatusDeluxe").checked);
      if (document.getElementById("azCatalogStatusDeluxe").checked == true) {
          document.getElementById("azProductNameDeluxe").disabled = false;
          document.getElementById("azProductDescriptionDeluxe").disabled = false;
      } else {
          document.getElementById("azProductNameDeluxe").disabled = true;
          document.getElementById("azProductDescriptionDeluxe").disabled = true;
      }
  }

  function checkCatalogStatusPremium() {
      //alert(document.getElementById("azCatalogStatusPremium").checked);
      if (document.getElementById("azCatalogStatusPremium").checked == true) {
          document.getElementById("azProductNamePremium").disabled = false;
          document.getElementById("azProductDescriptionPremium").disabled = false;
      } else {
          document.getElementById("azProductNamePremium").disabled = true;
          document.getElementById("azProductDescriptionPremium").disabled = true;
      }
  }
  
  function replaceSymboltoHtml(val,id) 
  {
	    var str = val;
	    var elementId=id;
	    var n = str.indexOf("\u00AE");
	    var n1= str.indexOf("\u2122");	
	    var n2= str.indexOf("\u002A");
	    var n3= str.indexOf("\u00E8");
	        if(n!=-1)
				{					
					str=str.replace(/\u00AE/g,"&reg;");   					
				}			
	        if(n1!=-1)
				{
					str=str.replace(/\u2122/g,"&#153;");											
				}	        
	        if(n2!=-1)
	        	{
	        	   str=str.replace(/\u002A/g,"&#042;");
	        	}	        
	        if(n3!=-1)
	        	{
	        		str=str.replace(/\u00E8/g,"&#232;");
	        	}
	       
	    document.getElementById(elementId).value =str;
	    
	}
  
  /* SP-127 
  Don't allow special characters in the Mercury Description field in PDB */
  
  function validate(val)
  {
	  var str=val;
	  var pattern=/[^a-zA-Z0-9\s\,\.]/g;
	  if(str!=null)
		{  
	  	    var res=str.match(pattern);
            if(res)		  
		  	{
		       alert("Please don't use any special characters in the Mercury Description");
		       str=str.replace(/[^a-zA-Z0-9\s\,\.]/g,"");
		 	}
            else{}
            document.getElementById("mercuryDescription").value =str;
		}
  }
  
</script>

<% //ConfigurationUtil rm = ConfigurationUtil.getInstance();
   Integer liveLen = new Integer(rm.getFrpGlobalParmNoNull(ProductMaintConstants.PDB_CONFIG_CONTEXT, ProductMaintConstants.PDB_NOVATOR_LIVE_KEY).length());
   Integer testLen = new Integer(rm.getFrpGlobalParmNoNull(ProductMaintConstants.PDB_CONFIG_CONTEXT, ProductMaintConstants.PDB_NOVATOR_TEST_KEY).length());
   Integer uatLen = new Integer(rm.getFrpGlobalParmNoNull(ProductMaintConstants.PDB_CONFIG_CONTEXT, ProductMaintConstants.PDB_NOVATOR_UAT_KEY).length());
   Integer contentLen = new Integer(rm.getFrpGlobalParmNoNull(ProductMaintConstants.PDB_CONFIG_CONTEXT, ProductMaintConstants.PDB_NOVATOR_CONTENT_KEY).length());

   String gbbTokenDisplay = rm.getProperty(ProductMaintConstants.PROPERTY_FILE, "GBB_TOKEN_DISPLAY");

   pageContext.setAttribute("liveLen", liveLen, PageContext.PAGE_SCOPE);
   pageContext.setAttribute("testLen", testLen, PageContext.PAGE_SCOPE);
   pageContext.setAttribute("uatLen", uatLen, PageContext.PAGE_SCOPE);
   pageContext.setAttribute("contentLen", contentLen, PageContext.PAGE_SCOPE);

   SimpleDateFormat formatter = new SimpleDateFormat(ProductMaintConstants.PDB_DATE_FORMAT);
   String labelClass = "Label";
   String errorClass = "Error";
   String lineBreak = "<br/>";
   String emptyString = "";
   String fieldStyleClass;
   String fieldLineBreak;

   String batchError = (String)productForm.get(ProductMaintConstants.BATCH_ERROR);
   String disableBatchMode = (String)productForm.get("disableBatchMode");

   if (disableBatchMode == null) {
    disableBatchMode = "false";
   }

   List edStatesList = (List)pageContext.findAttribute(ProductMaintConstants.PDB_EXCLUDED_STATES_KEY);

   String closePupUp = (String)productForm.get(ProductMaintConstants.CLOSE_POPUP);

   if (closePupUp != null && closePupUp.equals("Y")) { %>

    <script language = "JavaScript" type = "text/javascript">
     closePupUp = true;
    </script>

<% } %>

<html:form method = "POST" action = "/submitProductMaintDetail">
 <jsp:include page = "includes/security.jsp"/>

 <html:hidden property = "lastUpdateDate" styleId="lastUpdateDate"/>
 <!-- The following properties are no longer relevent -->
 <html:hidden property = "JCPenneyCategory" styleId="JCPenneyCategory"/>

 <html:hidden property = "shortDescription" styleId="shortDescription"/>

 <html:hidden property = "holidaySecondChoice" styleId="holidaySecondChoice"/>

 <html:hidden property = "mercuryHolidaySecondChoice" styleId="mercuryHolidaySecondChoice"/>

 <html:hidden property = "vendorCarrierData" styleId="vendorCarrierData"/>

 <html:hidden property = "twoDaySaturdayShipFreshCuts" styleId="twoDaySaturdayShipFreshCuts"/>
 <!-- The previous properties are no longer relevent -->
 <html:hidden property = "productId" styleId = "productId"/>
 <html:hidden property = "copyProduct" styleId="copyProduct"/>

 <html:hidden property = "openAllTabsFlag" styleId = "openAllTabsFlag"/>
 
 <html:hidden property="preferredPricePoint" styleId="preferredPricePoint"/>

 <html:errors property = "global"/>

 <table width = "98%" border = "0" cellpadding = "0" cellspacing = "2">
  <logic:present name = "instruction">
   <tr>
    <td class = "Instruction">
     <bean:write name = "instruction" ignore = "true" scope = "request"/>
    </td>
   </tr>
  </logic:present>

  <% if (masterSkuList.size() > 0) { %>

  <% for (int x = 0; x < masterSkuList.size(); x++) {
    UpsellDetailVO upsellVO = (UpsellDetailVO)masterSkuList.get(x);
    %>

     <tr>
      <td class = "Instruction">
       <bean:write name = "upsellinstruction" ignore = "true" scope = "request"/> <%= upsellVO.getDetailMasterID() %>, <%= upsellVO.getDetailMasterName() %>
      </td>
     </tr>

  <% } %>

  <% } %>
 </table>

 <div id = "accordionDiv" style = "border-top: 1px solid; margin-top: 6px">
  <div id = "div_general_panel">
   <div id = "div_general_header" class = "<%= tabBarClass %>">Product Identification (

    <bean:write name = "productMaintDetailForm" property = "productId" scope = "request"/>)
   </div>

   <div id = "div_general_context">
    <table width = "98%" border = "0" cellpadding = "0" cellspacing = "2">
     <tr>
      <td class = "Label" style = "width: 100px">Product ID

       <html:errors property = "productId"/>
      </td>

      <td>
       <html:text property = "productId" styleId = "productId" size = "10" maxlength = "10" onblur="changeProductId(this.value)"/>&nbsp;
	   <html:button property="copyBtn" value = "Copy" onclick="javascript:prepareForCopy(arrangementColorsArray, componentSkuArray, 'copyProductMaintDetail.do', includedColorsStaticArray )"/>
       <!--<bean:write name = "productMaintDetailForm" property = "productId" scope = "request"/>-->
      </td>

      <td align = "right" class = "Label">
       <label id = "productNameLabel" for = "productName">Product Name</label>

       <html:errors property = "productName"/>
      </td>

      <td>
       <html:text property = "productName" styleId = "productName" size = "30" maxlength = "25" onchange="changeProductName() "/>&nbsp;

       <html:button property = "btnSpellCheck" styleClass = "SpellCheckButton" value = "ABC" onclick = "checkSpelling(productName.value,'productName');"/>
      </td>
     </tr>

     <tr>
      <td class = "Label" title = "<%= ProductMaintConstants.PDB_PRODUCT_MAINT_TIP_NOVATOR_ID %>">
       <label id = "novatorIdLabel" for = "novatorId">Novator ID</label>

       <html:errors property = "novatorId"/>
      </td>

      <td title = "<%= ProductMaintConstants.PDB_PRODUCT_MAINT_TIP_NOVATOR_ID %>">
       <html:text styleId = "novatorId" property = "novatorId" size = "15" maxlength = "10" onchange="changeNovatorId()"/>
      </td>

      <td class = "Label" align = "right" valign = "top">
       <label id = "novatorNameLabel" for = "novatorName">Novator Name</label>

       <html:errors property = "novatorName"/>
      </td>

      <td>
       <html:text property = "novatorName" size = "30" maxlength = "100" styleId = "novatorName" onchange="replaceSymboltoHtml(this.value,'novatorName')"/>&nbsp;

       <html:button property = "btnSpellCheck" styleClass = "SpellCheckButton" value = "ABC" onclick = "checkSpelling(novatorName.value,'novatorName');"/>
      </td>
     </tr>
    <!-- PQuad Product ID changes -->
     <tr>
      <td class = "Label">
       <label id = "pquadProductIDLabel" for = "pquadProductID">PQuad Product ID</label><html:errors property = "pquadProductID"/>
      </td>
      <td>
       <html:text property = "pquadProductID" size = "30" maxlength = "32" styleId = "pquadProductID" onblur="setVendorSKU();" onchange="setVendorSKU();"/>
      </td>
      </tr>
    <!-- ------ -->
    
     <tr>
      <td class = "Label">
       <label id = "floristReferenceNumberLabel" for = "floristReferenceNumber">Florist Reference #</label>

       <html:errors property = "floristReferenceNumber"/>
      </td>

      <td>
       <html:text property = "floristReferenceNumber" size = "30" maxlength = "30" styleId = "floristReferenceNumber"/>
      </td>

      <td class = "Label" align = "right" valign = "top">
       <label id = "departmentCodeLabel" for = "departmentCode">Department Code</label>

       <html:errors property = "departmentCode"/>
      </td>

      <td>
       <html:text property = "departmentCode" size = "4" maxlength = "4" styleId = "departmentCode"/>
      </td>
     </tr>

     <tr>
      <td class = "Label" valign = "top">
       <label id = "categoryLabel" for = "category">Category</label>

       <html:errors property = "category"/>
      </td>

      <td>
       <html:select property = "category" styleId = "category">
        <html:options collection = "categories" property = "id" labelProperty = "description"/>
       </html:select>
      </td>

      <td class = "Label" align = "right" valign = "top">
       <label id = "mercuryDescriptionLabel" for = "mercuryDescription">Mercury Description</label>

       <html:errors property = "mercuryDescription"/>
      </td>

      <td>
       <html:text property = "mercuryDescription" size = "60" maxlength = "55" styleId = "mercuryDescription" onchange ="validate(this.value)"/>
      </td>
     </tr>

     <tr>
      <td class = "Label" valign = "top">
       <label id = "productTypeLabel" for = "productType">Type</label>

       <html:errors property = "productType"/>
      </td>

      <td>
       <html:select property = "productType" onchange = "javascript:changeProductType();" styleId = "productType">
        <html:options collection = "types" property = "id" labelProperty = "description"/>
       </html:select>
      </td>

      <td class = "Label" align = "right" valign = "top" rowspan = "3">
       <label id = "companyListLabel" for = "companyList">Company</label>

       <html:errors property = "companyList"/>
      </td>

      <td valign = "top" rowspan = "3">
       <html:select property = "companyList" multiple = "true" styleId = "companyList">
        <html:options collection = "companyMasterList" property = "id" labelProperty = "description"/>
       </html:select>
      </td>
     </tr>

     <tr>
      <td class = "Label" valign = "top">
       <label id = "productSubTypeLabel" for = "productSubType">Sub-Type</label>

       <html:errors property = "productSubType"/>
      </td>

      <td>
       <html:select property = "productSubType" onchange = "javascript:setupFieldsForSubType();" style = "width:150px" styleId="productSubType" >
        <html:options collection = "subTypes" property = "id" labelProperty = "description"/>
       </html:select>
       <!-- hidden version with all the options -->
       <select id = "productSubTypeHidden" style = "visibility:hidden" disabled = true>
        <logic:iterate id = "element" name = "subTypes" indexId = "i">
         <option value = "${subTypes[i].id}" typeId = "${subTypes[i].typeId}">${subTypes[i].description}</option>
        </logic:iterate>
       </select>
      </td>
     </tr>

     <tr title = "<%= ProductMaintConstants.PDB_PRODUCT_MAINT_TIP_LONG_DESCRIPTION %>">
      <td class = "Label" valign = "top">
       <label id = "longDescriptionLabel" for = "longDescription">Description</label>

       <html:errors property = "longDescription"/>

       <span id = "longDescriptionCount" class = "TextAreaCounter"/>
      </td>

      <td colspan = "3">
       <html:textarea styleId = "longDescription" property = "longDescription" rows = "6" cols = "75" onchange="replaceSymboltoHtml(this.value,'longDescription')"/>&nbsp;

       <html:button property = "btnSpellCheck" value = "ABC" style = "valign: top" onclick = "checkSpelling(longDescription.value,'longDescription');"/>
      </td>
     </tr>
	<tr title = "<%= ProductMaintConstants.PDB_PRODUCT_MAINT_BULLET_DESC_TIP_LONG_DESCRIPTION %>">
		<td class = "Label" valign = "top">
       <label id = "bulletDescriptionLabel" for = "bulletDescription">Bullet Description</label>
      </td>

      <td valign = "top">
       <html:textarea property = "bulletDescription"  styleId = "bulletDescription" rows = "6" cols = "75" />
       <html:button property = "btnSpellCheck" value = "ABC" style = "valign: top" onclick = "checkSpelling(bulletDescription.value,'bulletDescription');"/>
      </td>
	</tr>
     <tr>
      <td class = "Label" valign="top" title = "<%= ProductMaintConstants.PDB_PRODUCT_MAINT_TIP_WEBSITES %>">
       <label id = "websitesLabel" for = "websites">Websites</label>
       
      </td>

      <td colspan = "3">
       <html:textarea property = "websites" rows = "3" cols = "75" styleId = "websites"/>
      </td>
     </tr>
     <tr>
      <td colspan = "4"><html:errors property = "websites"/></td>
     </tr>
     
     
     <tr>
      <td class = "Label" valign = "top">
       <label id = "durationLabel" for = "duration">Services Duration</label>

        <html:errors property = "duration"/>
      </td>

      <td>
       <html:select property = "duration" styleId = "duration" >
        <html:options collection = "durations" property = "id" labelProperty = "description"/>
       </html:select>
      </td>
     </tr>
    </table>
   </div>
  </div>

  <div id = "div_detail_panel">
   <div id = "div_detail_header" class = "<%= tabBarClass %>">Product Properties
   </div>

   <div id = "div_detail_context">
    <table width = "98%" border = "0" cellpadding = "0" cellspacing = "2">
     <tr>
      <td class = "Label" valign = "top">Colors/Markers
            <%--  <html:errors property = "excludedColorsArray"/> --%>
      </td>
      <td class = "Label" colspan = "3" align = "left">
      <table width = "100%" border = "0" cellpadding = "0" cellspacing = "0">
      	<tr>
      	         <td class = "Label" align = "center">
      	         	          	<label id = "excludedColorsArrayTypeLabel" for = "markerSelectBoxExcluded">Type</label>
	         </td>
	         <td>&nbsp;</td>
	         <td class = "Label" align = "center">
	          	<label id = "includedColorsArrayTypeLabel" for = "markerSelectBoxIncluded">Type</label>
	         </td>
	         <td>&nbsp;</td>
    	     </tr>
        <tr>
	         <td width = "22%" align = "center">
		         <select id="markerSelectBoxExcluded"  onChange = "javascript:onselectMarkerTypeExcluded(excludedColorsArray, excludedColorsStaticArray);" >
					  <option selected value="ALL">ALL</option>
					  <option value="color">Color</option>
					  <option value="flower">Flower</option>
					  <option value="gift">Gift</option>
					  <option value="plant">Plant</option>
				</select>
	         </td>
         <td>&nbsp;</td>
          <td width = "22%" align = "center">
		         <select id="markerSelectBoxIncluded" onChange = "javascript:onselectMarkerTypeIncluded(arrangementColorsArray, includedColorsStaticArray);" >
					  <option selected value="ALL">ALL</option>
					  <option value="color">Color</option>
					  <option value="flower">Flower</option>
					  <option value="gift">Gift</option>
					  <option value="plant">Plant</option>
				</select>
	         </td>
         <td>&nbsp;</td>
        </tr>
        <tr>
         <td class = "Label" align = "center">
          <label id = "excludedColorsArrayLabel" for = "excludedColorsArray">Excluded Colors/Markers</label>
         </td>

         <td>&nbsp;</td>

         <td class = "Label" align = "center">
          <label id = "arrangementColorsArrayLabel" for = "arrangementColorsArray">Included Colors/Markers</label>
         </td>

         <td>&nbsp;</td>
        </tr>

        <tr>
         <td width = "22%" align = "center">
          <html:select property = "excludedColorsArray" size = "7" multiple = "true" styleClass = "PDBColorList" styleId = "excludedColorsArray">
           <html:options collection = "excludedColorsList" property = "id" labelProperty = "description"/>
          </html:select>
          <html:select style="display:none;" property = "excludedColorsStaticArray" size = "7" multiple = "true" styleClass = "PDBColorList" >
           <html:options collection = "excludedColorsListStatic" property = "id" labelProperty = "description"/>
          </html:select>
         </td>

         <td width = "5%" align = "center">
          <img id="colorsArrayButton" src = "./images/arrowSingleRight.gif" alt = "single right arrow" onClick = "javascript:btnMoveRightForMarker_onclick(excludedColorsArray, arrangementColorsArray, includedColorsStaticArray, excludedColorsStaticArray);"/>

          <br/>

          <img id="colorsArrayButton" src = "./images/arrowDoubleRight.gif" alt = "double right arrow" onClick = "btnMoveAllRightForMarker_onclick(excludedColorsArray, arrangementColorsArray, includedColorsStaticArray, excludedColorsStaticArray);"/>

          <br/>

          <img id="colorsArrayButton" src = "./images/arrowDoubleLeft.gif" alt = "double left arrow" onClick = "javascript:btnMoveAllLeftForMarker_onclick(arrangementColorsArray, excludedColorsArray, includedColorsStaticArray, excludedColorsStaticArray);"/>

          <br/>

          <img id="colorsArrayButton" src = "./images/arrowSingleLeft.gif" alt = "single left arrow" onClick = "javascript:btnMoveLeftForMarker_onclick(arrangementColorsArray, excludedColorsArray, includedColorsStaticArray, excludedColorsStaticArray);"/>
         </td>

         <td width = "22%" align = "center">
          <html:select property = "arrangementColorsArray" size = "7" multiple = "true" styleClass = "PDBColorList" styleId = "arrangementColorsArray">
           <html:options collection = "includedColorsList" property = "id" labelProperty = "description"/>
          </html:select>
	   <html:select style="display:none;" property = "includedColorsStaticArray" size = "7" multiple = "true" styleClass = "PDBColorList" >
           	<html:options collection = "includedColorsList" property = "id" labelProperty = "description"/>
          </html:select>

         </td>

         <td width = "5%" align = "center">
          <img src = "./images/arrowUp.gif" alt = "arrow up" onClick = "Moveup(arrangementColorsArray);"/>

          <br/>

          <img src = "./images/arrowDown.gif" alt = "arrow down" onClick = "Movedown(arrangementColorsArray);"/>
         </td>
        </tr>
       </table>
      </td>
     </tr>

     <tr>
      <td class = "Label" valign = "top">
       <label id = "colorSizeFlagLabel" for = "colorSizeFlag">Use colors</label>

       <html:errors property = "colorSizeFlag"/>
      </td>

      <td>
       <html:checkbox property = "colorSizeFlag" styleId = "colorSizeFlag"/>
      </td>

      <td class = "Label" valign = "top" align = "right" title = "<%= ProductMaintConstants.PDB_PRODUCT_MAINT_TIP_SIZE %>">
       <label id = "arrangementSizeLabel" for = "arrangementSize">Size</label>
      </td>

      <td title = "<%= ProductMaintConstants.PDB_PRODUCT_MAINT_TIP_SIZE %>">
       <html:text property = "arrangementSize" size = "25" maxlength = "25" styleId = "arrangementSize"/>
      </td>
     </tr>

     <tr title = "<%= ProductMaintConstants.PDB_PRODUCT_MAINT_TIP_DOMINANT_FLOWER %>">
      <td class = "Label" valign = "top" rowspan = "2">
       <label id = "dominantFlowersLabel" for = "dominantFlowers">Dominant Flowers</label>

       <span id = "dominantFlowersCount" class = "TextAreaCounter"/>
      </td>

      <td rowspan = "2" valign = "top">
       <html:textarea property = "dominantFlowers" rows = "3" cols = "35" styleId = "dominantFlowers"/>
      </td>

      <td class = "Label" align = "right" valign = "top">
       <label id = "unspscCodeLabel" for = "unspscCode">UNSPSC Code</label>
      </td>

      <td valign = "top">
       <html:text property = "unspscCode" size = "10" maxlength = "40" styleId = "unspscCode"/>
      </td>
     </tr>

     <tr>
      <td class = "Label" align = "right" valign = "top">
       <label id = "over21FlagLabel" for = "over21Flag">Over 21</label>
      </td>

      <td>
       <html:checkbox property = "over21Flag"/>
      </td>
     </tr>
     <tr>
      <td></td>
      <td></td>
      <td class = "Label" align = "right" valign = "top">
       <label id = "customFlagLabel" for = "customFlag">Custom</label>
      </td>

      <td>
       <html:checkbox property = "customFlag"/>
      </td>
     </tr>
     <tr>
      <td></td>
      <td></td>
      <td class = "Label" align = "right" valign = "top">
       <label id = "personalGreetingFlagLabel" for = "personalGreetingFlag">Personal Greeting</label>
      </td>

      <td>
       <html:checkbox property = "personalGreetingFlag"/>
      </td>
     </tr>    
     <tr>
      <td></td>
      <td></td>
      <td class = "Label" align = "right" valign = "top">
       <label id = "premierCollectionFlagLabel" for = "premierCollectionFlag">Premier Collection</label>
      </td>

      <td>
       <html:checkbox property = "premierCollectionFlag"/>
      </td>
     </tr>

    </table>
   </div>
  </div>

  <div id = "div_availability_panel">
   <div id = "div_availability_header" class = "<%= tabBarClass %>">Availability &amp; Exceptions
   </div>

   <div id = "div_availability_context">
    <a name="availability_div"/>
    <table width = "98%" border = "0" cellpadding = "2" cellspacing = "2">
     <tbody>
      <tr>
       <td colspan = "4" align = "center">
        <table align = "center" border = "0" cellpadding = "0" cellspacing = "0">
         <tr>
          <td class = "Label" width = "25%" title = "<%= ProductMaintConstants.PDB_PRODUCT_MAINT_TIP_AVAILABLE %>">
           <html:checkbox styleId = "status" property = "status" onclick="onStatusChanged();"/>

           <label id = "statusLabel" for = "status">Available</label>
          </td>

          <td class = "Label" width = "25%" title = "<%= ProductMaintConstants.PDB_PRODUCT_MAINT_TIP_WEBOE_BLOCKED %>">
           <html:checkbox styleId = "weboeBlocked" property = "weboeBlocked"/>

           <label id = "weboeBlockedLabel" for = "weboeBlocked">Block in JOE</label>
          </td>

          <td class = "Label" width = "25%" title = "<%= ProductMaintConstants.PDB_PRODUCT_MAINT_TIP_CORP_SITE %>">
           <html:checkbox styleId = "corporateSiteFlag" property = "corporateSiteFlag"/>

           <label id = "corporateSiteFlagLabel" for = "corporateSiteFlag">Corporate Site</label>
          </td>

          <td class = "Label" width = "25%">
           <html:checkbox styleId = "eGiftFlag" property = "eGiftFlag"/>

           <label id = "eGiftFlagLabel" for = "eGiftFlag">e-Gift</label>
          </td>
         </tr>
        </table>
       </td>
      </tr>

      <tr>
       <td>
        <table align = "center" style = "border-color: #336699;" border = "3">
         <tr>
          <td>
           <table border = "0" cellpadding = "0" cellspacing = "2">
            <caption style = "background-color:#336699; color:#eeeeee; font-weight:bold; text-align:left; padding-left: 5px;">Date Exceptions</caption>

            <tbody>
             <tr title = "<%= ProductMaintConstants.PDB_PRODUCT_MAINT_TIP_EXCEPTION_TYPE %>">
              <td class = "Label">
               <label id = "exceptionCodeLabel" for = "exceptionCode">Type</label>

               <html:errors property = "exceptionCode"/>
              </td>

              <td colspan = "3">
               <html:select property = "exceptionCode" styleId = "exceptionCode" onchange = "checkDateExceptions(true)">
                <html:options collection = "exceptions" property = "id" labelProperty = "description"/>
               </html:select>
              </td>
             </tr>

             <tr title = "<%= ProductMaintConstants.PDB_PRODUCT_MAINT_TIP_DATE_FORMAT %>">
              <td class = "Label" style = "vertical-align : middle">
               <label id = "exceptionStartDateLabel" for = "exceptionStartDate">Start Date</label>

               <html:errors property = "exceptionStartDate"/>
              </td>

              <td style = "vertical-align : middle">
               <html:text property = "exceptionStartDate" size = "10" styleId = "exceptionStartDate" disabled = "true"/>
              </td>

              <td style = "vertical-align : middle" width = "300px">
               <a id = "startDateCalLink"
               href = "javascript:startDateAddCal.popup();"><img src = "images/show-calendar.gif" align = "middle" border = "0" alt = "Click here to select the start date"></a>&nbsp;
               <a id = "startDateClearLink" href = "#availability_div" onclick = "document.getElementById('exceptionStartDate').value ='';">Clear</a>
              </td>
             </tr>

             <tr valign = "middle">
              <td class = "Label" style = "vertical-align : middle">
               <label id = "exceptionEndDateLabel" for = "exceptionEndDate">End Date</label>

               <html:errors property = "exceptionEndDate"/>
              </td>

              <td style = "vertical-align : middle; text-align: left">
               <html:text property = "exceptionEndDate" size = "10" styleId = "exceptionEndDate" onchange = "" disabled = "true"/>
              </td>

              <td style = "vertical-align : middle; text-align: left" width = "300px">
               <a id = "endDateCalLink"
               href = "javascript:endDateAddCal.popup();"><img src = "images/show-calendar.gif" align = "middle" border = "0" alt = "Click here to select the end date"></a>&nbsp;
               <a id = "endDateClearLink" href = "#availability_div" onclick = "document.getElementById('exceptionEndDate').value ='';">Clear</a>
              </td>
             </tr>

             <tr title = "<%= ProductMaintConstants.PDB_PRODUCT_MAINT_TIP_EXCEPTION_REASON %>">
              <td class = "Label" valign = "top">
               <label id = "exceptionMessageLabel" for = "exceptionMessage">Reason</label>

               <span id = "exceptionMessageCounter" class = "TextAreaCounter"/>
              </td>

              <td colspan = "3">
               <html:textarea property = "exceptionMessage" rows = "4" cols = "70" styleId = "exceptionMessage"/>
              </td>
             </tr>

             <tr title = "<%= ProductMaintConstants.PDB_PRODUCT_MAINT_TIP_HOLD_UNTIL_AVAILABLE %>">
              <td class = "Label">
               <label id = "holdUntilAvailableLabel" for = "holdUntilAvailable">Hold Until Available</label>
              </td>

              <td>
               <logic:present name = "productMaintDetailForm" property = "productType">
                <logic:equal name = "productMaintDetailForm" property = "productType" value = "<%= ProductMaintConstants.PRODUCT_TYPE_FLORAL %>">
                 <html:checkbox property = "holdUntilAvailable" styleId = "holdUntilAvailable"/>
                </logic:equal>

                <logic:notEqual name = "productMaintDetailForm" property = "productType" value = "<%= ProductMaintConstants.PRODUCT_TYPE_FLORAL %>">
                 <html:checkbox property = "holdUntilAvailable" disabled = "true" styleId = "holdUntilAvailable"/>
                </logic:notEqual>
               </logic:present>

               <logic:notPresent name = "productMaintDetailForm" property = "productType">
                <html:checkbox property = "holdUntilAvailable" disabled = "true" styleId = "holdUntilAvailable"/>
               </logic:notPresent>
              </td>
             </tr>
            </tbody>
           </table>
          </td>
         </tr>
        </table>
       </td>
      </tr>
     </tbody>
    </table>
   </div>
  </div>

  <div id = "div_florist_panel">
   <div id = "div_florist_header" class = "<%= tabBarClass %>">Florist Delivery
   </div>

   <div id = "div_florist_context">
    <table width = "98%" border = "0" cellpadding = "0" cellspacing = "2">
     <tr>
      <td class = "Label">
       <html:errors property = "shipMethod"/>

       <html:errors property = "shipMethodFlorist"/>
      </td>

      <td class = "Label">
       <html:checkbox property = "shipMethodFlorist" styleId = "shipMethodFlorist" title = "This value is controlled by the product 'Type' (Product Identification tab)"/>

       <label id = "shipMethodFloristLabel" for = "shipMethodFlorist">Florist Delivery Available</label>
      </td>
     </tr>

     <tr title = "<%= ProductMaintConstants.PDB_PRODUCT_MAINT_TIP_DELIVERY_TYPE %>">
      <td class = "Label" valign = "top" width = "150px">
       <label id = "deliveryTypeLabel" for = "deliveryType">Geographic Availability</label>

       <html:errors property = "deliveryType"/>
      </td>

      <td valign = "top">
       <table border = "0" cellpadding = "0" cellspacing = "0">
        <tr>
         <td>
          <html:select property = "deliveryType" onchange = "changeCountryList()" styleId = "deliveryType">
           <html:options collection = "shippingAvailabilityList" property = "id" labelProperty = "description"/>
          </html:select>
         </td>

         <td class = "LabelRight" valign = "top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

          <label id = "countryLabel" for = "country">Country</label>&nbsp;&nbsp;
         </td>

         <td valign = "top">
          <html:select property = "country" styleId = "country">
           <html:options collection = "currCountriesOptions" property = "id" labelProperty = "description"/>
          </html:select>
         </td>
        </tr>
       </table>
      </td>
     </tr>

     <tr>
      <td class = "Label" valign = "top">
       <label id = "codifiedFlagLabel" for = "codifiedFlag">Codification</label>&nbsp;&nbsp;
      </td>

      <td valign = "top">
       <html:text property = "codifiedFlag" size = "5" maxlength = "5" styleId = "codifiedFlag"/>
      </td>
     </tr>

     <tr>
      <td class = "Label" valign = "top">
       <label id = "itemCommentsLabel" for = "itemComments">Comments</label>

       <html:errors property = "itemComments"/>
      </td>

      <td>
       <html:text property = "itemComments" size = "60" maxlength = "60" styleId = "itemComments"/>
      </td>
     </tr>

     <tr>
      <td class = "Label" valign = "top">
       <label id = "secondChoiceLabel" for = "secondChoice">Second Choice</label>

       <html:errors property = "secondChoice"/>
      </td>

      <td>
       <html:select property = "secondChoice" onchange = "selectMercuryChoice(secondChoice, mercurySecondChoice);" styleId = "secondChoice">
        <html:options collection = "secondChoices" property = "productSecondChoiceID" labelProperty = "description"/>
       </html:select>
      </td>
     </tr>

     <tr>
      <td class = "Label" valign = "top">
       <label id = "mercurySecondChoiceLabel" for = "mercurySecondChoice">Mercury Second Choice</label>
      </td>

      <td>
       <html:select property = "mercurySecondChoice" disabled = "true" styleId = "mercurySecondChoice">
        <html:options collection = "secondChoices" property = "productSecondChoiceID" labelProperty = "mercuryDescription"/>
       </html:select>
      </td>
     </tr>

     <tr>
      <td class = "Label" valign = "top">
       <label id = "recipeLabel" for = "recipe">Standard Recipe</label>

       <span id = "recipeCounter" class = "TextAreaCounter"/>

       <html:errors property = "recipe"/>
      </td>

      <td>
       <table border = "0" cellpadding = "0" cellspacing = "0">
        <tr>
         <td>
          <html:textarea property = "recipe" styleId = "recipe" rows = "4" cols = "60"/>
         </td>

         <td class = "Label">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

          <html:checkbox property = "sendStandardRecipe" title = "Send the standard recipe on the Mercury order"/>

          <label id = "sendStandardRecipeLabel" for = "sendStandardRecipe">Send Standard Recipe</label>
         </td>
        </tr>
       </table>
      </td>
     </tr>

     <tr>
      <td class = "Label" valign = "top">
       <label id = "deluxeRecipeLabel" for = "deluxeRecipe">Deluxe Recipe</label>

       <span id = "deluxeRecipeCounter" class = "TextAreaCounter"/>

       <html:errors property = "deluxeRecipe"/>
      </td>

      <td>
       <table border = "0" cellpadding = "0" cellspacing = "0">
        <tr>
         <td>
          <html:textarea property = "deluxeRecipe" styleId = "deluxeRecipe" rows = "4" cols = "60"/>
         </td>

         <td class = "Label">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

          <html:checkbox property = "sendDeluxeRecipe" title = "Send the deluxe recipe on the Mercury order"/>

          <label id = "sendDeluxeRecipeLabel" for = "sendDeluxeRecipe">Send Deluxe Recipe</label>
         </td>
        </tr>
       </table>
      </td>
     </tr>

     <tr>
      <td class = "Label" valign = "top">
       <label id = "premiumRecipeLabel" for = "premiumRecipe">Premium Recipe</label>

       <span id = "premiumRecipeCounter" class = "TextAreaCounter"/>

       <html:errors property = "premiumRecipe"/>
      </td>

      <td>
       <table border = "0" cellpadding = "0" cellspacing = "0">
        <tr>
         <td>
          <html:textarea property = "premiumRecipe" styleId = "premiumRecipe" rows = "4" cols = "60"/>
         </td>

         <td class = "Label">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

          <html:checkbox property = "sendPremiumRecipe" title = "Send the premium recipe on the Mercury order"/>

          <label id = "sendPremiumRecipeLabel" for = "sendPremiumRecipe">Send Premium Recipe</label>
         </td>
        </tr>
       </table>
      </td>
     </tr>

    </table>
   </div>
  </div>

  <div id = "div_dropship_panel">
   <div id = "div_dropship_header" class = "<%= tabBarClass %>">Drop Ship Delivery
   </div>

   <div id = "div_dropship_context">
    <table width = "98%" border = "0" cellpadding = "0" cellspacing = "2">
     <tr>
      <td class = "Label" valign = "top">
       <html:errors property = "shipMethod"/>

       <html:errors property = "shipMethodCarrier"/>
      </td>

      <td colspan = "2" class = "Label">
       <html:checkbox property="shipMethodCarrier" styleId="shipMethodCarrier" title = "This value is controlled by the product 'Type' (Product Identification tab)" onclick = "toggleShipMethodCarrier()"/>

       <label id = "shipMethodCarrierLabel" for = "shipMethodCarrier">Drop Ship Delivery Available</label>
      </td>
     </tr>

     <tr>
      <td class = "Label" valign = "top">
       <label id = "shippingKeyLabel" for = "shippingKey">Shipping Key</label>

       <html:errors property = "shippingKey"/>
      </td>

      <td>
       <html:select property = "shippingKey" styleId = "shippingKey">
        <html:options collection = "shippingKeys" property = "id" labelProperty = "description"/>
       </html:select>
      </td>

      <td>
       <table border = "0" cellpadding = "0" cellspacing = "0">
        <tr>
         <td width = "20"/>

         <td class = "Label" align = "right">
          <label id = "dimWeightLabel" for = "dimWeight">Dim / Weight&nbsp;&nbsp;</label>

          <html:errors property = "dimWeight"/>
         </td>

         <td align = "left">
          <html:text property = "dimWeight" size = "10" maxlength = "10" styleId = "dimWeight"/>
         </td>

         <td width = "20"/>

         <td class = "Label" align = "right" title = "Which shipping system technology will be used?">
          <label id = "shippingSystemLabel" for = "shippingSystem">Shipping&nbsp;&nbsp;

          <br/>System&nbsp;&nbsp;</label>

          <html:errors property = "shippingSystem"/>
         </td>

         <td>
          <html:select property = "shippingSystem" styleId = "shippingSystem" onchange = "buildVendorDropdown(false);disableValidShipDays(this)">
           <html:option value = "NONE">&nbsp;</html:option>

           <logic:iterate id = "element" name = "shippingSystemsList" indexId = "i">
            <html:option value = "${shippingSystemsList[i].id}">${shippingSystemsList[i].description}</html:option>
           </logic:iterate>
          </html:select>
         </td>

         <td width = "20"/>

         <td class = "Label" align = "right">
          <label id = "personalizationLeadDaysLabel" for = "personalizationLeadDays">Shipping Lead Days&nbsp;&nbsp;

          <br/>for Personalizations&nbsp;&nbsp;</label>

          <html:errors property = "personalizationLeadDays"/>
         </td>

         <td align = "left">
          <html:text property = "personalizationLeadDays" size = "10" maxlength = "10" styleId = "personalizationLeadDays"/>
         </td>
        </tr>
       </table>
      </td>
     </tr>
     
     <tr>
       <td class = "Label" valign = "top" title = "Which shipping system technology will be used?">
          <label id = "boxIdLabel" for = "boxId">Box Type</label>

          <html:errors property = "boxId"/>
         </td>

         <td>
          <html:select property = "boxId" styleId = "boxId" >
           <html:option value = "NONE">&nbsp;</html:option>

           <logic:iterate id = "element" name = "boxList" indexId = "i">
            <html:option value = "${boxList[i].id}">${boxList[i].description}</html:option>
           </logic:iterate>
          </html:select>
         </td>
      <td>
       <table border = "0" cellpadding = "0" cellspacing = "0">
        <tr>
            <td>
                <html:checkbox property = "zoneJumpEligibleFlag" styleId="zoneJumpEligibleFlag"/>
            </td>
            <td class = "Label" align = "right" valign = "top">
                <label id = "zoneJumpEligibleFlagLabel" for = "zoneJumpEligibleFlag">Zone Jump Eligible</label>
            </td>
        </tr>
       </table>
      </td>
     </tr>

     <tr>
      <td class = "Label" valign = "top" rowspan = "2">Shipping Options

       <html:errors property = "deliveryOptionsError"/>
      </td>

      <td colspan = "3" align = "left">
       <table align = "left" border = "0">
        <% int delCount = 0;
        DeliveryOptionVO deliveryVO;
        String strChecked = "";
        String name = ""; %>

        <tr style = "valign:top">
         <logic:iterate id = "element" name = "shippingMethods" indexId = "i">
          <% deliveryVO = (DeliveryOptionVO)element;

          if (deliveryVO.isSelected())
           strChecked = "checked";
          else
           strChecked = "";

          name = "shipcheckbox" + i; %>

          <td>
           <input name = "shippingMethodList" type = "checkbox" value = "${shippingMethods[i].shippingMethodId}" count = "${i}" onclick = "toggleNovatorTags(this.count, this.checked);"
           <%=strChecked%>/>

           <label id = "shipcheckboxLabel" for = "shippingMethodList">${shippingMethods[i].shippingMethodDesc}</label>

           <input name = "novatorTagList" id = "novatorTagList" type = "checkbox" value = "${shippingMethods[i].novatorTag}" style = "visibility: hidden;"<%=strChecked%>/>
          </td>

          <% delCount++; %>
         </logic:iterate>
        </tr>

        <tr>
         <td title = "<%= ProductMaintConstants.PDB_PRODUCT_MAINT_TIP_EXPRESS_ONLY %>">
          <html:checkbox property = "expressShippingOnly" styleId = "expressShippingOnly"/>

          <label id = "expressShippingOnlyLabel" for = "expressShippingOnly">Express Shipping Only</label>
         </td>

         <td title = "<%= ProductMaintConstants.PDB_PRODUCT_MAINT_TIP_MONDAY_FRECUT %>">
          <html:checkbox property = "mondayDeliveryFreshCuts" styleId = "mondayDeliveryFreshCuts"/>

          <label id = "mondayDeliveryFreshCutsLabel" for = "mondayDeliveryFreshCuts">Allow Monday delivery</label>
         </td>

         <td>
          <input type = "checkbox" name = "nextDayUpgrade" id = "nextDayUpgrade" disabled/>

          <label id = "nextDayUpgradeLabel" for = "nextDayUpgrade">Next day upgrade to <strong>Priority Overnight</strong></label>
         </td>
        </tr>
       </table>
      </td>
     </tr>
     
    <tr> <td colspan="3" /> </tr>
	<tr> 
		<td class="Label" valign="top" rowspan="2">Morning Delivery <html:errors property="morningDeliveryError" /> </td>

		<td colspan="3" align="left"> 
			<table align="left" border="0">
				<tr>
					<td	title="<%=ProductMaintConstants.OE_PRODUCT_MASTER_MORNING_DELIVERY_FLAG%>"> 
							<html:checkbox property="morningDeliveryFlag" styleId="morningDeliveryFlag" disabled="true" />						
					</td>
				</tr>
			</table>
		</td>
	</tr>

     <tr>
      <td colspan = "3"/>
     </tr>

     <tr>
      <td width = "150px" class = "Label" valign = "top">
       <label id = "validShipDaysLabel">Valid Ship Days</label>

       <html:errors property = "validShipDaysError"/>
      </td>

      <td colspan = "3">
       <div>
        <table border = "0" cellpadding = "0" cellspacing = "0">
         <tr>
          <td width = "100px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>

          <td width = "70px">&nbsp;</td>

          <td class = "Label" align = "center" width = "50px">Sun</td>

          <td class = "Label" align = "center" width = "50px">Mon</td>

          <td class = "Label" align = "center" width = "50px">Tue</td>

          <td class = "Label" align = "center" width = "50px">Wed</td>

          <td class = "Label" align = "center" width = "50px">Thu</td>

          <td class = "Label" align = "center" width = "50px">Fri</td>

          <td class = "Label" align = "center" width = "50px">Sat</td>

          <td width = "25px">&nbsp;</td>
         </tr>

         <tr>
          <td width = "100px">&nbsp;</td>

          <td width = "70px">
           <a id='flipCheckboxesInTableRowLink' href = "" onclick = "flipCheckboxesInTableRow(this); return false;" id = "vsdLink"> <%= selectAllText %> </a>
          </td>

          <td>
           <div align = "center">
            <html:checkbox property = "shipDaySunday" styleId = "shipDaySunday"/>
           </div>
          </td>

          <td>
           <div align = "center">
            <html:checkbox property = "shipDayMonday" styleId = "shipDayMonday"/>
           </div>
          </td>

          <td>
           <div align = "center">
            <html:checkbox property = "shipDayTuesday" styleId = "shipDayTuesday"/>
           </div>
          </td>

          <td>
           <div align = "center">
            <html:checkbox property = "shipDayWednesday" styleId = "shipDayWednesday"/>
           </div>
          </td>

          <td>
           <div align = "center">
            <html:checkbox property = "shipDayThursday" styleId = "shipDayThursday"/>
           </div>
          </td>

          <td>
           <div align = "center">
            <html:checkbox property = "shipDayFriday" styleId = "shipDayFriday"/>
           </div>
          </td>

          <td>
           <div align = "center">
            <html:checkbox property = "shipDaySaturday" styleId = "shipDaySaturday"/>
           </div>
          </td>
         </tr>
        </table>
       </div>
      </td>
     </tr>

     <tr title = "Exclude delivery a specified state for the checked delivery day.">
      <td width = "150px" class = "Label" valign = "top">
       <label id = "ddExclusionLabel">Delivery Day Exclusion</label>
      </td>

      <td bgcolor = "#eeeeee" colspan = "3">
       <div style = "overflow: auto; height: 100px">
        <table border = "0" cellpadding = "0" cellspacing = "0">
         <tr>
          <td width = "100px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>

          <td width = "70px">&nbsp;</td>

          <td class = "Label" align = "center" width = "50px">Sun</td>

          <td class = "Label" align = "center" width = "50px">Mon</td>

          <td class = "Label" align = "center" width = "50px">Tue</td>

          <td class = "Label" align = "center" width = "50px">Wed</td>

          <td class = "Label" align = "center" width = "50px">Thu</td>

          <td class = "Label" align = "center" width = "50px">Fri</td>

          <td class = "Label" align = "center" width = "50px">Sat</td>

          <td>&nbsp;</td>
         </tr>

         <% String sBGColor;
         StateDeliveryExclusionVO sdeVO;

         for (int i = 0; i < edStatesList.size(); i++) {
          sdeVO = (StateDeliveryExclusionVO)edStatesList.get(i);

          if (i % 2 == 0)
           sBGColor = "#ffffff";
          else
           sBGColor = "#eeeeee"; %>

             <tr id = "excludeStateRow_" bgcolor = "<%= sBGColor %>">
              <td>
               <input type = "hidden" name = "excludedStateId" value = "<%= sdeVO.getExcludedState() %>"/>

               <input type = "hidden" name = "excludedStateName" value = "<%= sdeVO.getStateName() %>"/>
               <%= sdeVO.getStateName() %>
              </td>

              <td>
               <a id = "ddExclusionLink" href = "" onclick = "flipCheckboxesInTableRow(this); return false;"> <%= selectAllText %> </a>
              </td>

              <td>
               <div align = "center">
               <% if (sdeVO.isSunExcluded())
                 strChecked = "checked";
                else
                 strChecked = ""; %>

                   <input type = "checkbox" name = "deliveryDaySunday" id = "deliveryDaySunday" value = "<%= i %>" <%=strChecked%>/>
               </div>
              </td>

              <td>
               <div align = "center">
               <% if (sdeVO.isMonExcluded())
                 strChecked = "checked";
                else
                 strChecked = ""; %>

                   <input type = "checkbox" name = "deliveryDayMonday" id = "deliveryDayMonday" value = "<%= i %>" <%=strChecked%>/>
               </div>
              </td>

              <td>
               <div align = "center">
               <% if (sdeVO.isTueExcluded())
                 strChecked = "checked";
                else
                 strChecked = ""; %>

                   <input type = "checkbox" name = "deliveryDayTuesday" id = "deliveryDayTuesday" value = "<%= i %>" <%=strChecked%>/>
               </div>
              </td>

              <td>
               <div align = "center">
               <% if (sdeVO.isWedExcluded())
                 strChecked = "checked";
                else
                 strChecked = ""; %>

                   <input type = "checkbox" name = "deliveryDayWednesday" id = "deliveryDayWednesday" value = "<%= i %>" <%=strChecked%>/>
               </div>
              </td>

              <td>
               <div align = "center">
               <% if (sdeVO.isThuExcluded())
                 strChecked = "checked";
                else
                 strChecked = ""; %>

                   <input type = "checkbox" name = "deliveryDayThursday" id = "deliveryDayThursday" value = "<%= i %>" <%=strChecked%>/>
               </div>
              </td>

              <td>
               <div align = "center">
               <% if (sdeVO.isFriExcluded())
                 strChecked = "checked";
                else
                 strChecked = ""; %>

                   <input type = "checkbox" name = "deliveryDayFriday" id = "deliveryDayFriday" value = "<%= i %>" <%=strChecked%>/>
               </div>
              </td>

              <td>
               <div align = "center">
               <% if (sdeVO.isSatExcluded())
                 strChecked = "checked";
                else
                 strChecked = ""; %>

                   <input type = "checkbox" name = "deliveryDaySaturday" id = "deliveryDaySaturday" value = "<%= i %>" <%=strChecked%>/>
               </div>
              </td>

              <td>&nbsp;</td>
             </tr>

         <% } %>
        </table>
       </div>
      </td>
     </tr>
    </table>
   </div>
  </div>

  <div id = "div_price_panel">
   <div id = "div_price_header" class = "<%= tabBarClass %>">Prices &amp; Personalization
   </div>

   <div id = "div_price_context">
    <table width = "98%" border = "0" cellpadding = "2" cellspacing = "2">
     <tbody>
      <tr>
       <td colspan = "2" class = "Label" align = "left">
        <html:errors property = "colorSizeError"/>
       </td>
      </tr>

      <tr>
       <td>
        <table align = "center" style = "border-color: #336699;" border = "3">
         <tr>
          <td>
           <table border = "0" cellpadding = "0" cellspacing = "2">
            <caption style = "background-color:#336699; color:#eeeeee; font-weight:bold; text-align:left; padding-left: 5px;">Prices</caption>

            <tbody>
             <tr>
               <td colspan="2">&nbsp;</td>
               <td class="Label" width="75" align="center">Default</td>
               <td width="75">&nbsp;</td>
             </tr>
             <tr title = "<%= ProductMaintConstants.PDB_PRODUCT_MAINT_TIP_MONEY %>">
              <td class = "Label" valign = "top">
               <label id = "standardPriceLabel" for = "standardPrice">Standard Price</label>

               <html:errors property = "standardPrice"/>

               <html:errors property = "standardPriceRank"/>
              </td>

              <td>
               <html:text property = "standardPrice" size = "10" maxlength = "8" styleId = "standardPrice" onchange="changeStandardPrice()"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

               <input type = "hidden" id = "standardPriceRank" name = "standardPriceRank" value = "1"/>
              </td>
              <td align="center">
               <input type="radio" id="defaultPrice1" name="defaultPrice"/>
              </td>
             </tr>

             <tr title = "<%= ProductMaintConstants.PDB_PRODUCT_MAINT_TIP_MONEY %>">
              <td class = "Label" valign = "top">
               <label id = "deluxePriceLabel" for = "deluxePrice">Deluxe Price</label>

               <html:errors property = "deluxePrice"/>

               <html:errors property = "deluxePriceRank"/>
              </td>

              <td>
               <html:text property = "deluxePrice" size = "10" maxlength = "8" styleId = "deluxePrice" onchange="changeDeluxePrice()"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

               <input type = "hidden" id = "deluxePriceRank" name = "deluxePriceRank" value = "2"/>
              </td>
              <td align="center">
               <input type="radio" id="defaultPrice2" name="defaultPrice"/>
              </td>
             </tr>

             <tr title = "<%= ProductMaintConstants.PDB_PRODUCT_MAINT_TIP_MONEY %>">
              <td class = "Label" valign = "top">
               <label id = "premiumPriceLabel" for = "premiumPrice">Premium Price</label>

               <html:errors property = "premiumPrice"/>

               <html:errors property = "premiumPriceRank"/>
              </td>

              <td>
               <html:text property = "premiumPrice" size = "10" maxlength = "8" styleId = "premiumPrice" onchange="changePremiumPrice()"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

               <input type = "hidden" id = "premiumPriceRank" name = "premiumPriceRank" value = "3"/>
              </td>
              <td align="center">
               <input type="radio" id="defaultPrice3" name="defaultPrice"/>
              </td>
             </tr>

             <tr title = "<%=ProductMaintConstants.PDB_PRODUCT_MAINT_TIP_VARIABLE_PRICING%>  <%=ProductMaintConstants.PDB_PRODUCT_MAINT_TIP_MONEY%>">
              <td class = "Label" valign = "top">
               <label id = "variablePricingFlagLabel" for = "variablePricingFlag">Variable Pricing</label>

               <html:errors property = "variablePricingFlag"/>
              </td>

              <td colspan="3">
               <html:checkbox property = "variablePricingFlag" styleId = "variablePricingFlag"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span class = "Label">

               <label id = "variablePriceMaxLabel" for = "variablePriceMax">Maximum Price</label>

               </span>&nbsp;

               <html:text property = "variablePriceMax" size = "10" maxlength = "10" styleId = "variablePriceMax"/>
              </td>
             </tr>

             <tr>
              <td class = "Label" valign = "top" title = "<%=ProductMaintConstants.PDB_PRODUCT_MAINT_TIP_DISCOUNT_ALLOWED%>">
               <label id = "discountAllowedFlagLabel" for = "discountAllowedFlag">Discount Allowed</label>
              </td>

              <td valign = "top" title = "<%=ProductMaintConstants.PDB_PRODUCT_MAINT_TIP_DISCOUNT_ALLOWED%>">
               <html:checkbox property = "discountAllowedFlag" styleId = "discountAllowedFlag"/>
              </td>
             </tr>

             <tr>
              <td class = "Label" title = "<%=ProductMaintConstants.PDB_PRODUCT_MAINT_TIP_NO_TAX%>">
               <label id = "taxFlagLabel" for = "taxFlag">No Tax</label>
              </td>

              <td valign = "top" title = "<%=ProductMaintConstants.PDB_PRODUCT_MAINT_TIP_NO_TAX%>">
               <html:checkbox property = "taxFlag" styleId = "taxFlag"/>
              </td>
             </tr>

            <tr>
              <td class = "Label" valign = "top" title = "<%=ProductMaintConstants.PDB_PRODUCT_MAINT_TIP_FREE_SHIP_ELIGIBILITY%>">
               <label id = "freeShippingLabel" for = "allowFreeShippingFlag">Eligible for Free Shipping</label>
              </td>

              <td valign = "top" title = "<%=ProductMaintConstants.PDB_PRODUCT_MAINT_TIP_FREE_SHIP_ELIGIBILITY%>">
               <html:checkbox property = "allowFreeShippingFlag" styleId = "allowFreeShippingFlag"/>
              </td>
             </tr>
             
            </tbody>
           </table>
          </td>

          <td valign = "top">
           <table border = "0" cellpadding = "0" cellspacing = "2">
            <caption style = "background-color:#336699; color:#eeeeee; font-weight:bold; text-align:left; padding-left: 5px;">Personalization</caption>

            <tbody>
             <tr>
              <td class = "Label" valign = "top">
               <label id = "personalizationTemplateLabel" for = "personalizationTemplate">Template Name</label>

               <html:errors property = "personalizationTemplate"/>
              </td>

              <td>
               <html:select property = "personalizationTemplate" styleId = "personalizationTemplate" onchange="buildPersonalizationTemplateOrder();checkForTemplateId();" >
                <html:options collection = "personalizationTemplateList" property = "id" labelProperty = "description"/>
               </html:select>
              </td>
		      <td class = "Label" valign = "top">		       
		       <html:checkbox property = "personalizationCaseFlag"/>
		      </td>
		      
		      <td class = "Label" valign = "top">		       
		       <html:checkbox property = "allAlphaFlag"/>
		      </td>
		      
             </tr>
             <tr>
              <td class = "Label" valign = "top">
               <label id = "personalizationTemplateOrderLabel" for = "personalizationTemplateOrder">Template Order</label>

               <html:errors property = "personalizationTemplateOrder"/>
              </td>

              <td>
               <html:select property = "personalizationTemplateOrder" styleId = "personalizationTemplateOrder">
                <html:options collection = "personalizationTemplateOrderList" property = "id" labelProperty = "description"/>
               </html:select>
              </td>
             <td class = "Label" valign = "top">
               <label id = "personalizationCaseFlagLabel" for = "personalizationCaseFlag">All CAPs Required</label>
             </td>
             
             <td class = "Label" valign = "top">
               <label id = "allAlphaFlagLabel" for = "allAlphaFlag">All Alpha-characters required</label>
             </td>
             </tr>
             
             <tr>
              <td class = "Label" valign = "top">
               <label id = "ftdwestAccessoryIdLabel" for = "ftdwestAccessoryId">PQuad Accessory Template ID</label>

               <html:errors property = "ftdwestAccessoryId"/>
              </td>

              <td>
              <html:text property = "ftdwestAccessoryId" size = "20" maxlength = "20" styleId = "ftdwestAccessoryId"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;               
              </td>
             </tr>
             
             <tr>
              <td class = "Label" valign = "top">
               <label id = "ftdwestTemplateMappingLabel" for = "ftdwestTemplateMapping">PQuad Display Names</label>

               <html:errors property = "ftdwestTemplateMapping"/> 
              </td>

              <td>
              <html:text property = "ftdwestTemplateMapping" size = "30" maxlength = "100" styleId = "ftdwestTemplateMapping"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;               
              </td>
             </tr>
             <tr>
             <td colspan="2" class="Instruction" align="left">
             					Enter PQuad Accessory Id from PQuad. Enter Display Name(s) in order of Template Order.<br> Example: Template chosen for Product is Line 1 and Line 2. Enter Display names as seen in PQuad as First Name,Last Name.
             </td>
             </tr>
            </tbody>
           </table>
          </td>
         </tr>
        </table>
       </td>
      </tr>
     </tbody>
    </table>
   </div>
  </div>
  
  <div id = "div_add_on_and_vases_panel">
   <div id = "div_add_on_and_vases_header" class = "<%= tabBarClass %>">Add-ons &amp; Vases</div>

   <div id = "div_add_on_and_vases_context">
    <table width = "98%" border = "0" cellpadding = "2" cellspacing = "2">
     <tbody>
  	   <tr>
	      <td class = "Label" style = "vertical-align : middle">
               <html:errors property = "productAddons"/>
          </td>
	   </tr>
	   <tr>
	   <td>
       <table align = "center" style = "border-color: #336699;" border = "3">
           <tr>
		      <td class = "Instruction" colspan = "3">
			  ***Please assign either No Vase or one $0.00 vase as Display Order 1 if you assign any vase upgrades.***
			  </td>
		   </tr>
		   <tr>
            <td valign = "top">
           <table border = "0" cellpadding = "0" cellspacing = "2">
            <caption style = "background-color:#336699; color:#eeeeee; font-weight:bold; text-align:left; padding-left: 5px;">Options&nbsp;(WebOE&nbsp;Only)</caption>

            <tbody>
             <tr>
              <td>
               <html:checkbox styleId = "addOnBalloonsFlag" property = "addOnBalloonsFlag"/>
               <label id = "addOnBalloonsFlagLabel" for = "addOnBalloonsFlag" class = "Label">Balloon</label>
              </td>
             </tr>

             <tr>
              <td>
               <html:checkbox styleId = "addOnBearsFlag" property = "addOnBearsFlag"/>
               <label id = "addOnBearsFlagLabel" for = "addOnBearsFlag" class = "Label">Bear</label>
              </td>
             </tr>
             <tr>
              <td>
               <html:checkbox styleId = "addOnChocolateFlag" property = "addOnChocolateFlag"/>
               <label id = "addOnChocolateFlagLabel" for = "addOnChocolateFlag" class = "Label">Chocolate</label>
              </td>
             </tr>
            </tbody>
           </table>
		   </td>
          <td valign = "top">
           <table border = "0" cellpadding = "0" cellspacing = "2">
            <caption style = "background-color:#336699; color:#eeeeee; font-weight:bold; text-align:left; padding-left: 5px;">Add-ons</caption>
            <tbody>
             <tr>
			 <td>
 		     <div style="height : 120px; overflow : auto; ">
			 <table>
			 <tr>
			  <td align="center">
			    <span style = "font-weight:bold; text-align:center;">Assigned</span>
              </td>
			  <td align="center">
			    <span style = "font-weight:bold; text-align:center;">Add-on Name</span>
              </td>
			  <td align="center">
			    <span style = "font-weight:bold; text-align:center;">Display Order</span>
              </td>
			  <td align="center">
			    <span style = "font-weight:bold; text-align:center;">Max Quantity</span>
              </td>
             </tr>
               <% 
			     List productAddOnList = (ArrayList <ProductAddonVO>)pageContext.findAttribute(ProductMaintConstants.PRODUCT_ADD_ON_LIST);
                 for (int i = 0; i < productAddOnList.size(); i++) {
                %>
				 <tr>
				   <td align="center">
				        <input name = "<%= ProductMaintConstants.ADD_ON_ACTIVE_FLAG_KEY %>" class = "SubCode" type = "checkbox" <%= ((ProductAddonVO)productAddOnList.get(i)).getActiveFlag() == true ? " CHECKED " : "" %> <%= ((ProductAddonVO)productAddOnList.get(i)).getAddonAvailableFlag() == true ?  "" : " DISABLED " %> value = "<%= ((ProductAddonVO)productAddOnList.get(i)).getAddonId() %>"/>
				  </td>
				  <td>
				     <%= ((ProductAddonVO)productAddOnList.get(i)).getAddonId() %>&nbsp;-&nbsp;<%= ((ProductAddonVO)productAddOnList.get(i)).getAddonDescription() %>&nbsp;(<%= ((ProductAddonVO)productAddOnList.get(i)).getAddonPrice() %>)
				  
				  </td>
				  <td align="center">
				     <input name = "<%= ProductMaintConstants.ADD_ON_PRODUCT_DISPLAY_SEQUENCE_NUMBER_KEY %>" type = "text" class = "SubCode" id = "<%= ProductMaintConstants.ADD_ON_PRODUCT_DISPLAY_SEQUENCE_NUMBER_KEY %>" size = "4" maxlength = "4" value = "<%= ((ProductAddonVO)productAddOnList.get(i)).getDisplaySequenceNumber() == null ? "" : ((ProductAddonVO)productAddOnList.get(i)).getDisplaySequenceNumber() %>"/>				  </td>
				  <td align="center">
				  	 <input name = "<%= ProductMaintConstants.ADD_ON_PRODUCT_MAX_QUANTITY_KEY %>" type = "text" class = "SubCode" id = "<%= ProductMaintConstants.ADD_ON_PRODUCT_MAX_QUANTITY_KEY %>" size = "2" maxlength = "2" value = "<%= ((ProductAddonVO)productAddOnList.get(i)).getMaxQuantity() == null ? "" : ((ProductAddonVO)productAddOnList.get(i)).getMaxQuantity() %>"/>
                  </td>
				 </tr>
                <% } %> 
				</table>
                </div>	
				</td>
                </tr>				
        		</tbody>
            </table>
          </td>
		  
		  <td valign = "top">
           <table border = "0" cellpadding = "0" cellspacing = "2">
            <caption style = "background-color:#336699; color:#eeeeee; font-weight:bold; text-align:left; padding-left: 5px;">Vases</caption>

             <tbody>
             <tr>
			 <td>
 		     <div style="height : 120px; overflow : auto; ">
			 <table>	 
			  <td align="center">
			    <span style = "font-weight:bold; text-align:center;">Assigned</span>
              </td>
			  <td align="center">
			    <span style = "font-weight:bold; text-align:center;">Vase Name</span>
              </td>
			  <td align="center">
			    <span style = "font-weight:bold; text-align:center;">Display Order</span>
              </td>

             </tr>
			 <% 
			     List vaseAddOnList = (ArrayList <ProductAddonVO>)pageContext.findAttribute(ProductMaintConstants.PRODUCT_ADD_ON_VASE_LIST);
                 for (int i = 0; i < vaseAddOnList.size(); i++) {
                %>				 
				<tr>
				   <td align="center">
				        <input name = "<%= ProductMaintConstants.ADD_ON_VASE_ACTIVE_FLAG_KEY %>" class = "SubCode" type = "checkbox" <%= ((ProductAddonVO)vaseAddOnList.get(i)).getActiveFlag() == true ? " CHECKED " : "" %> <%= ((ProductAddonVO)vaseAddOnList.get(i)).getAddonAvailableFlag() == true ?  "" : " DISABLED " %>value = "<%= ((ProductAddonVO)vaseAddOnList.get(i)).getAddonId() %>"/>

				   </td>
				   
				   <td>
				     <%= ((ProductAddonVO)vaseAddOnList.get(i)).getAddonId() %>&nbsp;-&nbsp;<%= ((ProductAddonVO)vaseAddOnList.get(i)).getAddonDescription() %>&nbsp;(<%= ((ProductAddonVO)vaseAddOnList.get(i)).getAddonPrice() %>)
				  </td>
				  <td align="center">
				     <input name = "<%= ProductMaintConstants.ADD_ON_VASE_PRODUCT_DISPLAY_SEQUENCE_NUMBER_KEY %>" type = "text" class = "SubCode" id = "<%= ProductMaintConstants.ADD_ON_VASE_PRODUCT_DISPLAY_SEQUENCE_NUMBER_KEY %>" size = "4" maxlength = "4" value = "<%= ((ProductAddonVO)vaseAddOnList.get(i)).getDisplaySequenceNumber() == null ? "" : ((ProductAddonVO)vaseAddOnList.get(i)).getDisplaySequenceNumber() %>"/>
					<input name = "<%= ProductMaintConstants.ADD_ON_VASE_MAX_QUANTITY_KEY %>" type = "hidden" id = "<%= ProductMaintConstants.ADD_ON_VASE_MAX_QUANTITY_KEY %>" value = "<%= ((ProductAddonVO)vaseAddOnList.get(i)).getMaxQuantity() == null ? "" : ((ProductAddonVO)vaseAddOnList.get(i)).getMaxQuantity() %>"/>
				  </td>
				 </tr>
             <% }
			 %>
			 </table>
			 </div>
			 </td>
			 </tr>
            </tbody>
            </table>
          </td>
          </tr>
		  <tr>
              <td colspan = "3">
               <html:checkbox styleId = "addOnGreetingCardsFlag" property = "addOnGreetingCardsFlag"/>

               <label id = "addOnGreetingCardsFlagLabel" for = "addOnGreetingCardsFlag" class = "Label">Greeting Cards (applies to all order entry systems & website)</label>
              </td>
           </tr>

           <tr>
              <td colspan = "3">
               <html:checkbox styleId = "addOnFuneralFlag" property = "addOnFuneralFlag"/>

               <label id = "addOnFuneralFlagLabel" for = "addOnFuneralFlag" class = "Label">Funeral Banner (applies to all order entry systems; not website)</label>
              </td>
            </tr>
		  </table>
		  </td>
		  </tr>     
     </tbody>
    </table>
   </div>
  </div>

  <div id = "div_gbb_panel">
   <div id = "div_gbb_header" class = "<%= tabBarClass %>">Good, Better, Best
   </div>

   <div id = "div_gbb_context">
    <table width = "98%" border = "0" cellpadding = "2" cellspacing = "2">
     <tbody>

      <tr>
          <td width="50%" valign = "top">
              <table border = "0" cellpadding = "0" cellspacing = "2" width="100%">
                <tbody>
                  <tr>
                      <td class = "Label" width="35%">
                          Good, Better, Best Popover
                      </td>
                      <td width="5%">
                          <html:checkbox property = "gbbPopoverFlag" styleId = "gbbPopoverFlag"/>
                      </td>
                      <td width="60%">
                          &nbsp;
                      </td>
                  </tr>
                  <tr title="255 character max">
                      <td class = "Label" colspan="3" align="left">
                          <label id = "gbbTitleLabel" for = "gbbTitle">Title</label>
                          &nbsp;&nbsp;
                          <html:text property = "gbbTitle" size = "35" maxlength="255" styleId = "gbbTitle" onchange="replaceSymboltoHtml(this.value,'gbbTitle')"/>
                      </td>
                  </tr>

                  <tr>
                      <td class = "Label" colspan="3" align="left">
                          Position 1
                          &nbsp;&nbsp;
                          <label id="position1PriceLabel"/>
                      </td>
                  </tr>
                  <tr title = "<%=gbbTokenDisplay %>">
                      <td class = "Label" align="right">
                          Product Name Override
                      </td>
                      <td>
                          <html:checkbox property = "gbbNameOverrideFlag1" styleId="gbbNameOverrideFlag1"  onclick="changeName1()"/>
                      </td>
                      <td>
                          <html:text property="gbbNameOverrideText1" styleId="gbbNameOverrideText1"  size="35" maxlength="255" onchange="replaceSymboltoHtml(this.value,'gbbNameOverrideText1');changeName1()"/>
                      </td>
                  </tr>
                  <tr title = "<%=gbbTokenDisplay %>">
                      <td class = "Label" align="right">
                          Product Price Override
                      </td>
                      <td>
                          <html:checkbox property = "gbbPriceOverrideFlag1" styleId="gbbPriceOverrideFlag1" onclick="changePrice1()"/>
                      </td>
                      <td>
                          <html:text property="gbbPriceOverrideText1" size="35" maxlength="255" styleId="gbbPriceOverrideText1" onchange="changePrice1()"/>
                      </td>
                  </tr>

                  <tr>
                      <td class = "Label" colspan="3" align="left">
                          Position 2
                          &nbsp;&nbsp;
                          <label id="position2PriceLabel"/>
                      </td>
                  </tr>
                  <tr title = "<%=gbbTokenDisplay %>">
                      <td class = "Label" align="right">
                          Product Name Override
                      </td>
                      <td>
                          <html:checkbox property = "gbbNameOverrideFlag2" styleId="gbbNameOverrideFlag2" onclick="changeName2()"/>
                      </td>
                     <td>
                          <html:text property="gbbNameOverrideText2" styleId="gbbNameOverrideText2" size="35" maxlength="255" onchange="replaceSymboltoHtml(this.value,'gbbNameOverrideText2');changeName2()"/>
                      </td>
                  </tr>
                  <tr title = "<%=gbbTokenDisplay %>">
                      <td class = "Label" align="right">
                          Product Price Override
                      </td>
                      <td>
                          <html:checkbox property = "gbbPriceOverrideFlag2" styleId="gbbPriceOverrideFlag2" onclick="changePrice2()"/>
                      </td>
                      <td>
                          <html:text property="gbbPriceOverrideText2" styleId="gbbPriceOverrideText2" size="35" maxlength="255" onchange="changePrice2()"/>
                      </td>
                  </tr>

                  <tr>
                      <td class = "Label" colspan="3" align="left">
                          Position 3
                          &nbsp;&nbsp;
                          <label id="position3PriceLabel"/>
                      </td>
                  </tr>
                  <tr title = "<%=gbbTokenDisplay %>">
                      <td class = "Label" align="right">
                          Product Name Override
                      </td>
                      <td>
                          <html:checkbox property = "gbbNameOverrideFlag3" styleId="gbbNameOverrideFlag3" onclick="changeName3()"/>
                      </td>
                      <td>
                          <html:text property="gbbNameOverrideText3" styleId="gbbNameOverrideText3" size="35" maxlength="255" onchange="replaceSymboltoHtml(this.value,'gbbNameOverrideText3');changeName3()"/>
                      </td>
                  </tr>
                  <tr title = "<%=gbbTokenDisplay %>">
                      <td class = "Label" align="right">
                          Product Price Override
                      </td>
                      <td>
                          <html:checkbox property = "gbbPriceOverrideFlag3" styleId="gbbPriceOverrideFlag3" onclick="changePrice3()"/>
                      </td>
                      <td>
                          <html:text property="gbbPriceOverrideText3" styleId="gbbPriceOverrideText3" size="35" maxlength="255" onchange="changePrice3()"/>
                      </td>
                  </tr>

                </tbody>
              </table>
          </td>

          <td width="50%" valign = "top">
              <table border = "0" cellpadding = "0" cellspacing = "2" width="100%">
                <tbody>
                  <tr>
                      <td class = "Label" align = "center" width="33%">
                          Position 1
                      </td>

                      <td class = "Label" align = "center" width="33%">
                          Position 2
                      </td>

                      <td class = "Label" align = "center" width="33%">
                          Position 3
                      </td>

                  </tr>

                  <tr>
                      <td align="center">
                          <img name="image1" id="image1" src="" alt="Position 1" border="0" height="120" width="120" onerror='this.src="images/npi_a.jpg"'>
                      </td>
                      <td align="center">
                          <img name="image2" id="image2" src="" alt="Position 2" border="0" height="120" width="120" onerror='this.src="images/npi_a.jpg"'>
                      </td>
                      <td align="center">
                          <img name="image3" id="image3" src="" alt="Position 3" border="0" height="120" width="120" onerror='this.src="images/npi_a.jpg"'>
                      </td>
                  </tr>

                  <tr height="50">
                      <td align="center" valign="top">
                          <label id="position1Text"/>
                      </td>
                      <td align="center" valign="top">
                          <label id="position2Text"/>
                      </td>
                      <td align="center" valign="top">
                          <label id="position3Text"/>
                      </td>
                  </tr>

                  <tr>
                      <td align="center">
                          <label id="position1Price"/>
                      </td>
                      <td align="center">
                          <label id="position2Price"/>
                      </td>
                      <td align="center">
                          <label id="position3Price"/>
                      </td>
                  </tr>

                </tbody>
            </table>
        </td>

      </tr>
     </tbody>
    </table>
   </div>
  </div>

  <div id = "div_search_panel">
   <div id = "div_search_header" class = "<%= tabBarClass %>">Search
   </div>

   <div id = "div_search_context">
    <table width = "98%" border = "0" cellpadding = "0" cellspacing = "2">
     <tr>
      <td class = "Label" valign = "top">
       <label id = "recipientSearchLabel" for = "recipientSearch">Recipient Search</label>
      </td>

      <td colspan = "2">
       <html:select property = "recipientSearch" styleId = "recipientSearch" multiple = "true">
        <html:options collection = "recipientSearchList" property = "id" labelProperty = "description"/>
       </html:select>
      </td>
     </tr>

     <tr>
      <td class = "Label" valign = "top">
       <label id = "searchPriorityLabel" for = "searchPriority">Search Priority</label>
      </td>

      <td>
       <html:select property = "searchPriority" styleId = "searchPriority">
        <html:options collection = "searchPriorityList" property = "id" labelProperty = "description"/>
       </html:select>
      </td>

      <td>&nbsp;</td>
     </tr>

     <tr>
      <td class = "Label">
       <label id = "keywordSearchLabel" for = "keywordSearch">Keyword Search</label>
      </td>

      <td colspan = "2">
       <html:textarea property = "keywordSearch" rows = "3" cols = "60" styleId = "keywordSearch" onchange="replaceSymboltoHtml(this.value,'keywordSearch')"/>
      </td>
     </tr>
     <tr>
      <td class = "Label">
       <label id = "keywordsLabel" for = "keywords">Order Entry Keyword Search</label>
      </td>

      <td colspan = "2">
       <html:textarea property = "keywords" rows = "3" cols = "60" styleId = "keywords" onkeyup="replaceSymboltoHtml(this.value,'keywords');validateTextarea(this, 4000);"/>
      </td>
     </tr>
    </table>
   </div>
  </div>

  <div id = "div_subcodes_panel">
   <div id = "div_subcodes_header" class = "<%= tabBarClass %>">Subcodes &amp; Vendors
   </div>

   <div id = "div_subcodes_context" align = "left">
    <% String idCaption = "";
    List scList = (List)pageContext.findAttribute(ProductMaintConstants.SUB_CODE_LIST_KEY);
    //java.util.List subcodeList = (java.util.List)productForm.get(ProductMaintConstants.SUB_CODE_LIST_KEY);

    if (scList.size() > 0) {
     idCaption = "Subcode";
    } else {
     idCaption = "Product";
    } %>

    <div id = "div_subcodes_vendors" align = "left">
     <table>
      <tr>
       <td>&nbsp;</td>

       <td>&nbsp;</td>
      </tr>

      <tr>
       <td valign = "top" class = "Label" style = "width: 60px">Subcode</td>

       <td valign = "top">
        <input name = "subCodeAddButton" type = "button" id = "subCodeAddButton" value = "Add Subcode" onclick = "addSubCode(); return false;"/>

        <input name = "subCodeCommitButton" type = "button" id = "subCodeCommitButton" value = "Commit Subcode" onclick = "commitSubCode(); return false;" style = "visibility:hidden"/>

        <input name = "subCodeRemoveButton" type = "button" id = "subCodeRemoveButton" value = "Remove Subcode" onclick = "removeSubCode(); return false;" style = "visibility:hidden"/>

        &nbsp;

        <br/>

        <table align = "left" width = "98%" border = "0" cellpadding = "0" cellspacing = "2">
         <tr>
          <td colspan = "2" class = "Label" align = "left">
           <html:errors property = "subcode"/>
          </td>
         </tr>

         <% if (masterSkuList.size() <= 0) { %>

          <tr>
           <td colspan = "3" align = "left">
            <table border = "3" id = "subcodeTable" numRows = "<%= scList.size() %>" align = "left" style = "border-color: #336699;"
            title = "<%= ProductMaintConstants.PDB_PRODUCT_MAINT_TIP_SUBCODES %>">
             <tr>
              <td>
               <table border = "1" cellpadding = "0" cellspacing = "2">
                <caption style = "background-color:#336699; color:#eeeeee; font-weight:bold; text-align:left; padding-left: 5px;">Subcodes</caption>

                <tbody id = "subCodeTableId">
                 <tr>
                  <th class = "LabelCenter" scope = "col">
                   <label id = "subcodeLabel">Subcode</label>
                  </th>

                  <th class = "Label" scope = "col">
                   <label id = "subcodeDescriptionLabel">Description</label>
                  </th>

                  <th class = "Label" scope = "col">
                   <label id = "subcodeReferenceLabel">Reference</label>
                  </th>

                  <th class = "LabelCenter" scope = "col">
                   <label id = "subcodePriceLabel">Price</label>
                  </th>

                  <th class = "LabelCenter" scope = "col">
                   <label id = "subcodeDimWtLabel">Dim/Wt</label>
                  </th>

                  <th class = "LabelCenter" scope = "col">
                   <label id = "subCodeAvailableLabel">Available</label>
                  </th>

                  <th>&nbsp;</th>
                 </tr>

                <% int counter = 0;

                 for (int i = 0; i < scList.size(); i++) {
                  String divName1 = "subCode1Name" + counter;
                  String checked = "";
                  //                              if(((ProductSubCodeVO)scList.get(i)).isAvailable())
                  //                              {
                  //                                checked = "checked";
                  //                              }
                %>

                  <tr>
                   <td align = "left">
                    <input name = "productSubCodeId" type = "text" class = "SubCode" id = "productSubCodeId" size = "3" maxlength = "10"
                    value = "<%= ((ProductSubCodeVO)scList.get(i)).getProductSubCodeId() %>" readonly/>
                   </td>

                   <td>
                    <input name = "productSubCodeDescription" type = "text" class = "SubCode" id = "productSubCodeDescription" size = "25" maxlength = "100"
                    value = "<%= ((ProductSubCodeVO)scList.get(i)).getSubCodeDescription() %>"/>
                   </td>

                   <td>
                    <input name = "productSubCodeReference" type = "text" class = "SubCode" id = "productSubCodeReference" size = "25" maxlength = "40"
                    value = "<%= FTDUtil.translateNullToString(((ProductSubCodeVO)scList.get(i)).getSubCodeRefNumber()) %>"/>
                   </td>

                   <td align = "left">
                    <input name = "productSubCodePrice"
                    class = "SubCode"
                    type = "text"
                    id = "productSubCodePrice"
                    size = "5"
                    maxlength = "8"
                    value = "<%= FTDUtil.formatAmountToPattern(new Float(((ProductSubCodeVO)scList.get(i)).getSubCodePrice()), "#0.00") %>"
                    title = "<%= ProductMaintConstants.PDB_PRODUCT_MAINT_TIP_MONEY %>"/>
                   </td>

                   <td align = "left">
                    <input name = "productSubCodeDimWeight" class = "SubCode" type = "text" id = "productSubCodeDimWeight" size = "2" maxlength = "10"
                    value = "<%= FTDUtil.translateNullToString(((ProductSubCodeVO)scList.get(i)).getDimWeight()) %>"/>
                   </td>

                   <td align = "center">
                    <input name = "productSubCodeAvailableCheckbox" class = "SubCode" type = "checkbox"
                    id = "productSubCodeAvailableCheckbox<%= ((ProductSubCodeVO)scList.get(i)).getProductSubCodeId() %>" disabled/>

                    <input name = "productSubCodeAvailable" class = "SubCode" type = "hidden" id = "productSubCodeAvailable<%= ((ProductSubCodeVO)scList.get(i)).getProductSubCodeId() %>"/>
                   </td>

                   <td align = "left">
                    <table border = "1" cellpadding = "0" cellspacing = "0">
                     <tr>
                      <td>
                       <img src = "./images/arrowUp.gif" onclick = "moveSubcodeRowUp(this);" alt = "Move subcode up.  Order in this list will be the order displayed on the site."/>
                      </td>

                      <td>
                       <img src = "./images/arrowDown.gif" onclick = "moveSubcodeRowDown(this);" alt = "Move subcode down.  Order in this list will be the order displayed on the site."/>
                      </td>
                     </tr>
                    </table>
                   </td>
                  </tr>

                <% } %>
                <!-- end of for loop -->
                </tbody>
               </table>
              </td>
             </tr>
            </table>
           </td>
          </tr>

         <% } else { %>

          <tr>
           <td>Subcodes cannot be added to this product because it is part of an Upsell.</td>
          </tr>

         <% idCaption = "Product Id";
         } %>
        </table>
       </td>
      </tr>

      <tr>
       <td valign = "top" class = "Label" style = "width: 100px">Vendors</td>

       <td>
        <html:select property = "vendor" styleId = "vendorListId">
         <logic:iterate id = "element" name = "vendorList" indexId = "i">
          <option value = "${vendorList[i].vendorId}" typeId = "${vendorList[i].vendorType}" vendorName = "${vendorList[i].vendorName}">${vendorList[i].vendorId} -
          ${vendorList[i].vendorName}</option>
         </logic:iterate>
        </html:select>

        <input name = "vendorAddButton" type = "button" id = "vendorAddButton" value = "Add Vendor" onclick = "javascript: addVendor(); return false;"/>
        <!-- hidden version with all the options -->
        <select id = "vendorListIdHidden" style = "visibility:hidden" disabled = true>
         <logic:iterate id = "element" name = "vendorList" indexId = "i">
          <option value = "${vendorList[i].vendorId}" typeId = "${vendorList[i].vendorType}" vendorName = "${vendorList[i].vendorName}">${vendorList[i].vendorId} -
          ${vendorList[i].vendorName}</option>
         </logic:iterate>
        </select>

        <div id = "div_vendor_context">
         <html:errors property = "vendorId"/>

         <% List vpList = (List)pageContext.findAttribute("vendorProductsList");

         if (vpList.size() > 0) {
          VendorProductVO vpVO = (VendorProductVO)vpList.get(0);
          String vendorAvailable = "";

          if (vpVO.isAvailable()) {
           vendorAvailable = "CHECKED";
          }

          String oldVendorId = vpVO.getVendorId(); %>

          <br/>
          <%--start of all vendor tables--%>

          <table style = "border-color: #336699;" border = "3" id = "vendor<%= vpVO.getVendorId() %>Table" vendorId = "<%= vpVO.getVendorId() %>">
           <tr>
            <td>
             <table border = "1" cellpadding = "0" cellspacing = "2">
              <caption style = "background-color:#336699; color:#eeeeee; font-weight:bold; text-align:left; padding-left: 5px;">
               <%= vpVO.getVendorId() %>&nbsp;-&nbsp; <%= vpVO.getVendorName() %>
              <a id="removeVendorLink" href = "#" onclick = "removeVendor('vendor<%= vpVO.getVendorId() %>Table');" style = "color: #eeeeee">remove</a>
              </caption>

              <tbody id = "vendorTableId">
               <tr>
                <th class = "LabelCenter" scope = "col">
                 <%= idCaption %>
                </th>

                <th class = "LabelCenter" scope = "col">Vendor SKU</th>

                <th class = "LabelCenter" scope = "col">Cost</th>

                <th class = "LabelCenter" scope = "col">Available</th>
               </tr> <%-- a end of vendor header --%>

              <% for (int i = 0; i < vpList.size(); i++) {
                vpVO = (VendorProductVO)vpList.get(i);
                vendorAvailable = "";

                if (vpVO.isAvailable()) {
                 vendorAvailable = "CHECKED";
                }

                if (vpVO.getVendorId().equalsIgnoreCase(oldVendorId)) { %>

                 <tr>
                  <td align = "center">
                   <input type = "hidden" id = "productSubcodeVendorId" name = "productSubcodeVendorId" size = "3" maxlength = "10" value = "<%= vpVO.getProductSkuId() %>"/>

                   <input type = "hidden" id = "vendorName" name = "vendorName" value = "<%= vpVO.getVendorName() %>"/>

                   <input type = "hidden" id = "vendorId" name = "vendorId" value = "<%= vpVO.getVendorId() %>"/>
                   <%-- save removes for vendors that were originally there --%>
                   <%= vpVO.getProductSkuId() %>
                  </td>

                  <td align = "center">
                   <input type = "text" id = "vendorSKU" name = "vendorSKU" size = "20" maxlength = "32" value = "<%= vpVO.getVendorSku() %>"/>
                  </td>

                  <td align = "center">
                   <input type = "text" id = "vendorCost" name = "vendorCost" size = "5" maxlength = "8"
                   value = "<%= FTDUtil.formatAmountToPattern(new Float(vpVO.getVendorCost()), "#0.00") %>" title = "<%= ProductMaintConstants.PDB_PRODUCT_MAINT_TIP_MONEY %>"/>
                  </td>

                  <td align = "center">
                   <input productCode = "<%= vpVO.getProductSkuId() %>" vendorId = "<%= vpVO.getVendorId() %>" type = "checkbox" id = "vendorAvailableCheckbox"
                   name = "vendorAvailableCheckbox" onclick = "toggleAvailability(); "<%= vendorAvailable %>/>

                   <input type = "hidden" id = "vendorAvailable" name = "vendorAvailable" value = "<%= vpVO.isAvailable() %>"/>
                  </td>
                 </tr>

              <% } // end if (vpVO.getVendorId().equalsIgnoreCase(oldVendorId)
                else { %>
              </tbody>
             </table>
            </td>
           </tr>
          </table>

          <br/>

          <table style = "border-color: #336699;" border = "3" id = "vendor<%= vpVO.getVendorId() %>Table">
           <tr>
            <td>
             <table border = "1" cellpadding = "0" cellspacing = "2">
              <caption style = "background-color: #336699; color:#eeeeee; font-weight:bold; text-align:left; padding-left: 5px;">
               <%= vpVO.getVendorId() %>&nbsp;-&nbsp; <%= vpVO.getVendorName() %>
              <a id="removeVendorLink" href = "#" onclick = "removeVendor('vendor<%= vpVO.getVendorId() %>Table');" style = "color: #eeeeee">remove</a>
              </caption>

              <tbody id = "vendorTableId">
               <tr>
                <th class = "LabelCenter" scope = "col">
                 <%= idCaption %>
                </th>

                <th class = "LabelCenter" scope = "col">Vendor SKU</th>

                <th class = "LabelCenter" scope = "col">Cost</th>

                <th class = "LabelCenter" scope = "col">Available</th>
               </tr> <%-- a end of vendor header --%>

               <tr>
                <td align = "center">
                 <input type = "hidden" id = "productSubcodeVendorId" name = "productSubcodeVendorId" size = "3" maxlength = "10" value = "<%= vpVO.getProductSkuId() %>"/>

                 <input type = "hidden" id = "vendorName" name = "vendorName" value = "<%= vpVO.getVendorName() %>"/>

                 <input type = "hidden" id = "vendorId" name = "vendorId" value = "<%= vpVO.getVendorId() %>"/>
                 <%= vpVO.getProductSkuId() %>
                </td>

                <td align = "center">
                 <input type = "text" id = "vendorSKU" name = "vendorSKU" size = "20" maxlength = "20" value = "<%= vpVO.getVendorSku() %>"/>
                </td>

                <td align = "center">
                 <input type = "text" id = "vendorCost" name = "vendorCost" size = "5" maxlength = "8" value = "<%= vpVO.getVendorCost() %>"
                 title = "<%= ProductMaintConstants.PDB_PRODUCT_MAINT_TIP_MONEY %>"/>
                </td>

                <td align = "center">
                 <input productCode = "<%= vpVO.getProductSkuId() %>" vendorId = "<%= vpVO.getVendorId() %>" type = "checkbox" id = "vendorAvailableCheckbox" name = "vendorAvailableCheckbox"
                 onclick = "toggleAvailability();"<%= vendorAvailable %>/>

                 <input type = "hidden" id = "vendorAvailable" name = "vendorAvailable" value = "<%= vpVO.isAvailable() %>"/>
                </td>
               </tr>

              <% } // end of else

                oldVendorId = vpVO.getVendorId();
               }   // end of vpList for loop
              %>
              </tbody>
             </table>
            </td>
           </tr>
          </table>

         <% } //end of (vpList.size() > 0)
         %>
        </div>
       </td>
      </tr>
     </table>

     <br/>
    </div>
   </div>
  </div>

  <div id = "div_dscosts_panel">
   <div id = "div_dscosts_header" class = "<%= tabBarClass %>">Drop Ship Costs
   </div>

   <div id = "div_dscosts_context">
    <a name="dscosts"/>
    <table width = "98%" border = "0" cellpadding = "2" cellspacing = "2">
     <tbody>
      <tr>
       <td colspan = "2" class = "Label" align = "left">
        <html:errors property = "dropshipCostsError"/>
       </td>
      </tr>

      <tr>
       <td>
        <table align = "center" style = "border-color: #336699;" border = "3" width="90%">
         <tr>
          <td width="40%" valign = "top">
           <table border = "0" cellpadding = "0" cellspacing = "2" width="100%">
            <caption style = "background-color:#336699; color:#eeeeee; font-weight:bold; text-align:center; padding-left: 5px;">Costs</caption>

            <tbody>
             <tr title = "<%= ProductMaintConstants.PDB_PRODUCT_MAINT_TIP_MONEY %>">
              <td class = "Label" style = "vertical-align : middle" width="50%">
               <label id = "supplyExpenseLabel" for = "supplyExpense">Supplies Expense</label>
               <html:errors property = "supplyExpense"/>
              </td>
              <td style = "vertical-align : middle" width="30%">
               <html:text property = "supplyExpense" size = "10" styleId = "supplyExpense"/>
              </td>
              <td style = "vertical-align : middle" width="20%">
               &nbsp;
              </td>
             </tr>

             <tr title = "<%= ProductMaintConstants.PDB_PRODUCT_MAINT_TIP_DATE_FORMAT %>">
              <td class = "Label" style = "vertical-align : middle">
               <label id = "supplyExpenseEffDateLabel" for = "supplyExpenseEffDate">Supplies Expense Effective Date</label>

               <html:errors property = "supplyExpenseEffDate"/>
              </td>

              <td style = "vertical-align : middle">
               <html:text property = "supplyExpenseEffDate" size = "10" styleId = "supplyExpenseEffDate" disabled = "true"/>
              </td>
              <td style = "vertical-align : middle">
               <div id="showSupplyExpenseCal">
               <a id = "supplyExpenseEffDateCalLink"
               href = "javascript:supplyExpenseEffDateAddCal.popup();"><img src = "images/show-calendar.gif" align = "middle" border = "0" alt = "Click here to select the effective date"></a>&nbsp;
               <a id ="supplyExpenseEffDateCalClearLink" href = "#dscosts" onclick = "document.getElementById('supplyExpenseEffDate').value ='';">Clear</a>
               </div>
              </td>
             </tr>

             <tr title = "<%= ProductMaintConstants.PDB_PRODUCT_MAINT_TIP_PERCENT %>">
              <td class = "Label" style = "vertical-align : middle">
               <label id = "royaltyPercentLabel" for = "royaltyPercent">Royalty Percent</label>
               <html:errors property = "royaltyPercent"/>
              </td>
              <td style = "vertical-align : middle">
               <html:text property = "royaltyPercent" size = "10" styleId = "royaltyPercent"/>
              </td>
             </tr>

             <tr title = "<%= ProductMaintConstants.PDB_PRODUCT_MAINT_TIP_DATE_FORMAT %>">
              <td class = "Label" style = "vertical-align : middle">
               <label id = "royaltyPercentEffDateLabel" for = "royaltyPercentEffDate">Royalty Percent Effective Date</label>

               <html:errors property = "royaltyPercentEffDate"/>
              </td>

              <td style = "vertical-align : middle">
               <html:text property = "royaltyPercentEffDate" size = "10" styleId = "royaltyPercentEffDate" disabled = "true"/>
              </td>
              <td style = "vertical-align : middle">
               <div id="showRoyaltyPercentCal">
               <a id = "royaltyPercentEffDateCalLink"
               href = "javascript:royaltyPercentEffDateAddCal.popup();"><img src = "images/show-calendar.gif" align = "middle" border = "0" alt = "Click here to select the effective date"></a>&nbsp;
               <a id ="royaltyPercentEffDateClearLink" href = "#dscosts" onclick = "document.getElementById('royaltyPercentEffDate').value ='';">Clear</a>
               </div>
              </td>
             </tr>

            </tbody>

           </table>
          </td>

          <td width="60%" valign = "top">
           <table border = "0" cellpadding = "0" cellspacing = "2" width="100%">
            <caption style = "background-color:#336699; color:#eeeeee; font-weight:bold; text-align:center; padding-left: 5px;">Component SKU</caption>

            <tbody>
       <table width = "100%" border = "0" cellpadding = "0" cellspacing = "0">
        <tr>
         <td width="40%" class = "Label" align = "center">
          <label>Available Component SKUs</label>
         </td>

         <td width="20%">&nbsp;</td>

         <td width="40%" class = "Label" align = "center">
          <label>Included Component SKUs</label>
         </td>

         <td>&nbsp;</td>
        </tr>

        <tr>
         <td align = "center">
          <html:select property = "availableComponentSkuArray" size = "7" multiple = "true" styleClass = "PDBColorList" styleId = "availableComponentSkuArray">
           <html:options collection = "availableComponentSkuList" property = "id" labelProperty = "id"/>
          </html:select>
         </td>

         <td align = "center">
          <div id="showComponentArrows">
          <img id="showComponentArrowButton" src = "./images/arrowSingleRight.gif" alt = "single right arrow" onClick = "javascript:btnMoveRight_onclick(availableComponentSkuArray, componentSkuArray);"/>
          <br/>
          <img id="showComponentArrowButton" src = "./images/arrowDoubleRight.gif" alt = "double right arrow" onClick = "javascript:btnMoveAllRight_onclick(availableComponentSkuArray, componentSkuArray);"/>
          <br/>
          <img id="showComponentArrowButton" src = "./images/arrowDoubleLeft.gif" alt = "double left arrow" onClick = "javascript:btnMoveAllLeft_onclick(componentSkuArray, availableComponentSkuArray);"/>
          <br/>
          <img id="showComponentArrowButton" src = "./images/arrowSingleLeft.gif" alt = "single left arrow" onClick = "javascript:btnMoveLeft_onclick(componentSkuArray, availableComponentSkuArray);"/>
          </div>
         </td>

         <td align = "center">
          <html:select property = "componentSkuArray" size = "7" multiple = "true" styleClass = "PDBColorList" styleId = "componentSkuArray">
           <html:options collection = "includedComponentSkuList" property = "id" labelProperty = "id"/>
          </html:select>
         </td>

        </tr>

       </table>
            </tbody>
           </table>
          </td>

         </tr>
        </table>
       </td>
      </tr>
     </tbody>
    </table>
   </div>
  </div>

  <div id = "div_amazon_panel">
    <div id = "div_amazon_header" class = "<%= tabBarClass %>">Amazon
    </div>

    <div id = "div_amazon_context" align = "left">
      <html:errors property = "amazonList"/>
      <table width = "90%" border = "0" cellpadding = "0" cellspacing = "2">
        <tbody>
          <tr>
            <td class = "Instruction" colspan = "3">
              No HTML or special characters in Amazon Fields
            </td>
          </tr>
          <tr>
            <td class = "Label" align="left">
              Position 1
              &nbsp;&nbsp;
              <label id="azPosition1PriceLabel"/>
            </td>
            <td>&nbsp;</td>
            <td class="Label" align="left">
              Intended Use
            </td>
            <td>
              <html:text property = "azIntendedUse1" size = "60" maxlength = "50" styleId = "azIntendedUse1"/>&nbsp;
            </td>
          </tr>
          <tr>
            <td class = "Label" align="left" width="15%">
              &nbsp;&nbsp;
              Amazon Catalog
            </td>
            <td width="35%">
              <html:checkbox property = "azCatalogStatusStandard" onclick="checkCatalogStatusStandard()" styleId = "azCatalogStatusStandard"/>
            </td>
            <td class="Instruction" align="left" width="15%">
              (Max 5 terms and
            </td>
            <td width="35%">
              <html:text property = "azIntendedUse2" size = "60" maxlength = "50" styleId = "azIntendedUse2"/>&nbsp;
            </td>
          </tr>
          <tr>
            <td class = "Label" align="left">
              &nbsp;&nbsp;
              Product Name
            </td>
            <td>
              <html:text property = "azProductNameStandard" size = "60" maxlength = "500" styleId = "azProductNameStandard"/>&nbsp;
            </td>
            <td class="Instruction" align="left">
              50 characters per line)
            </td>
            <td>
              <html:text property = "azIntendedUse3" size = "60" maxlength = "50" styleId = "azIntendedUse3"/>&nbsp;
            </td>
          </tr>
          <tr>
            <td class = "Label" rowspan="2">
              <label id = "azProductDescriptionStandardLabel" for="azProductDescriptionStandard">Product Description</label>
            </td>
            <td rowspan = "2">
              <html:textarea property = "azProductDescriptionStandard" rows = "3" cols = "60" styleId = "azProductDescriptionStandard"/>
            </td>
            <td>&nbsp;</td>
            <td>
              <html:text property = "azIntendedUse4" size = "60" maxlength = "50" styleId = "azIntendedUse4"/>&nbsp;
            </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>
              <html:text property = "azIntendedUse5" size = "60" maxlength = "50" styleId = "azIntendedUse5"/>&nbsp;
            </td>
          </tr>

          <tr><td colspan="4">&nbsp;</td></tr>

          <tr>
            <td class = "Label" align="left">
              Position 2
              &nbsp;&nbsp;
              <label id="azPosition2PriceLabel"/>
            </td>
            <td>&nbsp;</td>
            <td class="Label" align="left">
              Target Audience
            </td>
            <td>
              <html:text property = "azTargetAudience1" size = "60" maxlength = "50" styleId = "azTargetAudience1"/>&nbsp;
            </td>
          </tr>
          <tr>
            <td class = "Label" align="left">
              &nbsp;&nbsp;
              Amazon Catalog
            </td>
            <td>
              <html:checkbox property = "azCatalogStatusDeluxe" onclick="checkCatalogStatusDeluxe()" styleId = "azCatalogStatusDeluxe"/>
            </td>
            <td class="Instruction" align="left">
              (Max 5 terms and
            </td>
            <td>
              <html:text property = "azTargetAudience2" size = "60" maxlength = "50" styleId = "azTargetAudience2"/>&nbsp;
            </td>
          </tr>
          <tr>
            <td class = "Label" align="left">
              &nbsp;&nbsp;
              Product Name
            </td>
            <td>
              <html:text property = "azProductNameDeluxe" size = "60" maxlength = "500" styleId = "azProductNameDeluxe"/>&nbsp;
            </td>
            <td class="Instruction" align="left">
              50 characters per line)
            </td>
            <td>
              <html:text property = "azTargetAudience3" size = "60" maxlength = "50" styleId = "azTargetAudience3"/>&nbsp;
            </td>
          </tr>
          <tr>
            <td class = "Label" rowspan="2">
              <label id = "azProductDescriptionDeluxeLabel" for="azProductDescriptionDeluxe">Product Description</label>
            </td>
            <td rowspan = "2">
              <html:textarea property = "azProductDescriptionDeluxe" rows = "3" cols = "60" styleId = "azProductDescriptionDeluxe"/>
            </td>
            <td>&nbsp;</td>
            <td>
              <html:text property = "azTargetAudience4" size = "60" maxlength = "50" styleId = "azTargetAudience4"/>&nbsp;
            </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>

          <tr><td colspan="4">&nbsp;</td></tr>

          <tr>
            <td class = "Label" align="left">
              Position 3
              &nbsp;&nbsp;
              <label id="azPosition3PriceLabel"/>
            </td>
            <td>&nbsp;</td>
            <td class="Label" align="left">
              Other Attributes
            </td>
            <td>
              <html:text property = "azOtherAttribute1" size = "60" maxlength = "50" styleId = "azOtherAttribute1"/>&nbsp;
            </td>
          </tr>
          <tr>
            <td class = "Label" align="left">
              &nbsp;&nbsp;
              Amazon Catalog
            </td>
            <td>
              <html:checkbox property = "azCatalogStatusPremium" onclick="checkCatalogStatusPremium()" styleId = "azCatalogStatusPremium"/>
            </td>
            <td class="Instruction" align="left">
              (Max 5 terms and
            </td>
            <td>
              <html:text property = "azOtherAttribute2" size = "60" maxlength = "50" styleId = "azOtherAttribute2"/>&nbsp;
            </td>
          </tr>
          <tr>
            <td class = "Label" align="left">
              &nbsp;&nbsp;
              Product Name
            </td>
            <td>
              <html:text property = "azProductNamePremium" size = "60" maxlength = "500" styleId = "azProductNamePremium"/>&nbsp;
            </td>
            <td class="Instruction" align="left">
              50 characters per line)
            </td>
            <td>
              <html:text property = "azOtherAttribute3" size = "60" maxlength = "50" styleId = "azOtherAttribute3"/>&nbsp;
            </td>
          </tr>
          <tr>
            <td class = "Label" rowspan="2">
              <label id = "azProductDescriptionPremiumLabel" for="azProductDescriptionPremium">Product Description</label>
            </td>
            <td rowspan = "2">
              <html:textarea property = "azProductDescriptionPremium" rows = "3" cols = "60" styleId = "azProductDescriptionPremium"/>
            </td>
            <td>&nbsp;</td>
            <td>
              <html:text property = "azOtherAttribute4" size = "60" maxlength = "50" styleId = "azOtherAttribute4"/>&nbsp;
            </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>
              <html:text property = "azOtherAttribute5" size = "60" maxlength = "50" styleId = "azOtherAttribute5"/>&nbsp;
            </td>
          </tr>

          <tr><td colspan="4">&nbsp;</td></tr>

          <tr>
            <td class="Label" align="left">
              Bullet Points
            </td>
            <td>
              <html:text property = "azBulletPoint1" size = "60" maxlength = "100" styleId = "azBulletPoint1"/>&nbsp;
            </td>
            <td class="Label" align="left">
              Subject Matter
            </td>
            <td>
              <html:text property = "azSubjectMatter1" size = "60" maxlength = "50" styleId = "azSubjectMatter1"/>&nbsp;
            </td>
          </tr>
          <tr>
            <td class="Instruction" align="left">
              (Max 5 terms and
            </td>
            <td>
              <html:text property = "azBulletPoint2" size = "60" maxlength = "100" styleId = "azBulletPoint2"/>&nbsp;
            </td>
            <td class="Instruction" align="left">
              (Max 5 terms and
            </td>
            <td>
              <html:text property = "azSubjectMatter2" size = "60" maxlength = "50" styleId = "azSubjectMatter2"/>&nbsp;
            </td>
          </tr>
          <tr>
            <td class="Instruction" align="left">
              100 characters per line)
            </td>
            <td>
              <html:text property = "azBulletPoint3" size = "60" maxlength = "100" styleId = "azBulletPoint3"/>&nbsp;
            </td>
            <td class="Instruction" align="left">
              50 characters per line)
            </td>
            <td>
              <html:text property = "azSubjectMatter3" size = "60" maxlength = "50" styleId = "azSubjectMatter3"/>&nbsp;
            </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>
              <html:text property = "azBulletPoint4" size = "60" maxlength = "100" styleId = "azBulletPoint4"/>&nbsp;
            </td>
            <td>&nbsp;</td>
            <td>
              <html:text property = "azSubjectMatter4" size = "60" maxlength = "50" styleId = "azSubjectMatter4"/>&nbsp;
            </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>
              <html:text property = "azBulletPoint5" size = "60" maxlength = "100" styleId = "azBulletPoint5"/>&nbsp;
            </td>
            <td>&nbsp;</td>
            <td>
              <html:text property = "azSubjectMatter5" size = "60" maxlength = "50" styleId = "azSubjectMatter5"/>&nbsp;
            </td>
          </tr>

          <tr><td colspan="4">&nbsp;</td></tr>

          <tr>
            <td class="Label" align="left">
              Search Terms
            </td>
            <td>
              <html:text property = "azSearchTerm1" size = "60" maxlength = "50" styleId = "azSearchTerm1"/>&nbsp;
            </td>
            <td class="Label" align="left">
              Item Type
            </td>
            <td>
              <html:text property = "azItemType" size = "60" maxlength = "50" styleId = "azItemType"/>&nbsp;
            </td>
          </tr>
          <tr>
            <td class="Instruction" align="left">
              (Max 5 terms and
            </td>
            <td>
              <html:text property = "azSearchTerm2" size = "60" maxlength = "50" styleId = "azSearchTerm2"/>&nbsp;
            </td>
          </tr>
          <tr>
            <td class="Instruction" align="left">
              50 characters per line)
            </td>
            <td>
              <html:text property = "azSearchTerm3" size = "60" maxlength = "50" styleId = "azSearchTerm3"/>&nbsp;
            </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>
              <html:text property = "azSearchTerm4" size = "60" maxlength = "50" styleId = "azSearchTerm4"/>&nbsp;
            </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>
              <html:text property = "azSearchTerm5" size = "60" maxlength = "50" styleId = "azSearchTerm5"/>&nbsp;
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
 </div> <!-- end accordionDiv -->

 <div id = "div_bottom_panel" align = "center">
  <table width = "98%" border = "0" cellpadding = "0" cellspacing = "2">
   <tr>
    <td colspan = "2">
     <hr/>
    </td>
   </tr>

   <tr>
    <td class = "Label" align = "left" valign = "top" title = "<%= ProductMaintConstants.PDB_PRODUCT_MAINT_TIP_COMMENT %>">
     <label id = "generalCommentsLabel" for = "generalComments">General Comments</label>

     <span id = "generalCommentsCounter" class = "TextAreaCounter"/>

     <html:errors property = "generalComments" />
    </td>

    <td align = "left" title = "<%= ProductMaintConstants.PDB_PRODUCT_MAINT_TIP_COMMENT %>">
     <html:textarea property = "generalComments" styleId = "generalComments" rows = "7" cols = "75" onchange="replaceSymboltoHtml(this.value,'generalComments')"/>
    </td>
   </tr>

   <tr title = "<%= ProductMaintConstants.PDB_PRODUCT_MAINT_TIP_NOVATOR_UPDATE %>">
    <td class = "Label" align = "left">Novator Update</td>

    <td>
     <table align = "left" cellpadding = "2" cellspacing = "0" border = "0">
      <tr>
       <td/>

       <logic:greaterThan value = "0" name = "liveLen">
        <td align = "left">
         <html:checkbox property = "sentToNovatorProd" styleId = "sentToNovatorProd"/>&nbsp;

         <label id = "sentToNovatorProdLabel" for = "sentToNovatorProd">Live</label>
        </td>
       </logic:greaterThan>

       <logic:greaterThan value = "0" name = "testLen">
        <td align = "left">
         <html:checkbox property = "sentToNovatorTest" styleId = "sentToNovatorTest"/>&nbsp;

         <label id = "sentToNovatorTestLabel" for = "sentToNovatorTest">Test</label>
        </td>
       </logic:greaterThan>

       <logic:greaterThan value = "0" name = "uatLen">
        <td align = "left">
         <html:checkbox property = "sentToNovatorUAT" styleId = "sentToNovatorUAT"/>&nbsp;

         <label id = "sentToNovatorUATLabel" for = "sentToNovatorUAT">UAT</label>
        </td>
       </logic:greaterThan>

       <logic:greaterThan value = "0" name = "contentLen">
        <td align = "left">
         <html:checkbox property = "sentToNovatorContent" styleId = "sentToNovatorContent"/>&nbsp;

         <label id = "sentToNovatorContentLabel" for = "sentToNovatorContent">Content</label>
        </td>
       </logic:greaterThan>

       <td>&nbsp;</td>
      </tr>
     </table>
    </td>
   </tr>

   <tr>
    <td colspan = "2">
     <hr/>
    </td>
   </tr>

   <tr>
    <td colspan = "2" align = "center">

     <html:button property = "btnUpdateProduct" value = "Submit" onclick = "javascript:processInput(arrangementColorsArray, componentSkuArray, 'submitProductMaintDetail.do', includedColorsStaticArray)"/>

     <% if (openAllTabsFlag.equals("N")) { %>&nbsp;&nbsp; <a href = "#" id = "toggleTabsLink" onclick = "toggleTabs();">open all tabs</a>

     <% } else { %>&nbsp;&nbsp; <a href = "#" id = "toggleTabsLink" onclick = "toggleTabs();">close tabs</a>

     <% } %>
    </td>
   </tr>
  </table>
 </div> <!-- end div_bottom_panel -->
</html:form>

<script language = "JavaScript" type = "text/javascript">
 var startDateAddCal = new calendar2(document.getElementById('exceptionStartDate'));

 startDateAddCal.year_scroll = true;
 startDateAddCal.time_comp = false;

 var endDateAddCal = new calendar2(document.getElementById('exceptionEndDate'));
 endDateAddCal.year_scroll = true;
 endDateAddCal.time_comp = false;

 var supplyExpenseEffDateAddCal = new calendar2(document.getElementById('supplyExpenseEffDate'));
 supplyExpenseEffDateAddCal.year_scroll = true;
 supplyExpenseEffDateAddCal.time_comp = false;

 var royaltyPercentEffDateAddCal = new calendar2(document.getElementById('royaltyPercentEffDate'));
 royaltyPercentEffDateAddCal.year_scroll = true;
 royaltyPercentEffDateAddCal.time_comp = false;
</script>

<script type="text/javascript">
	function checkForTemplateId()
	{      
	  var template = document.getElementById("personalizationTemplate");
	    var tempId;
	    for( var i = template.length-1; i >= 0; i-- )
	     {
	           if(template.options[i].selected)
	           {
	        	   tempId = template.options[i].value;
	           }

	     }
	    if(tempId =='NONE')
	     {	    	 
	    	document.getElementById("personalizationCaseFlag").checked = false;
	    	document.getElementById("personalizationCaseFlag").disabled = true;
	    	document.getElementById("allAlphaFlag").checked = false;
	    	document.getElementById("allAlphaFlag").disabled = true;
	     }
	    else
	     {	    	
	    	document.getElementById("personalizationCaseFlag").disabled=false;
	    	document.getElementById("allAlphaFlag").disabled = false;
	     }
	  }	
</script>