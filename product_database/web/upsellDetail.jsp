<%@ page import="com.ftd.osp.utilities.ConfigurationUtil"%>
<%@ page import="org.apache.commons.lang.StringUtils"%>
<%@ page import="com.ftd.pdb.common.ProductMaintConstants"%>
<%@ page import="com.ftd.pdb.common.valobjs.UpsellDetailVO"%>
<%@ page import = "com.ftd.pdb.common.utilities.FTDUtil"%>

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>

<link rel="stylesheet" type="text/css" href="css/ftd.css" />
<script type="text/javascript" src="js/util.js">
</script>
<script type = "text/javascript" src = "js/pngfix.js">
</script>
<script type="text/javascript" language="JavaScript">

<%
    
    ConfigurationUtil rm = ConfigurationUtil.getInstance();
    Integer liveLen = new Integer(rm.getFrpGlobalParmNoNull(ProductMaintConstants.PDB_CONFIG_CONTEXT, ProductMaintConstants.PDB_NOVATOR_LIVE_KEY).length());
    Integer testLen = new Integer(rm.getFrpGlobalParmNoNull(ProductMaintConstants.PDB_CONFIG_CONTEXT, ProductMaintConstants.PDB_NOVATOR_TEST_KEY).length());
    Integer uatLen = new Integer(rm.getFrpGlobalParmNoNull(ProductMaintConstants.PDB_CONFIG_CONTEXT, ProductMaintConstants.PDB_NOVATOR_UAT_KEY).length());
    Integer contentLen = new Integer(rm.getFrpGlobalParmNoNull(ProductMaintConstants.PDB_CONFIG_CONTEXT, ProductMaintConstants.PDB_NOVATOR_CONTENT_KEY).length());

    String defaultTitleParm = rm.getFrpGlobalParmNoNull(ProductMaintConstants.PDB_CONFIG_CONTEXT, ProductMaintConstants.PDB_GBB_DEFAULT_TITLE);
    String imageURLPrefixParm = rm.getFrpGlobalParmNoNull(ProductMaintConstants.PDB_CONFIG_CONTEXT, ProductMaintConstants.PDB_IMAGE_URL_PREFIX);
    String imageURLTypeParm = rm.getFrpGlobalParmNoNull(ProductMaintConstants.PDB_CONFIG_CONTEXT, ProductMaintConstants.PDB_IMAGE_URL_TYPE);
    String imageURLStandardSuffix = rm.getFrpGlobalParmNoNull(ProductMaintConstants.PDB_CONFIG_CONTEXT, ProductMaintConstants.PDB_IMAGE_URL_STANDARD_SUFFIX);
    String imageURLDeluxeSuffix = rm.getFrpGlobalParmNoNull(ProductMaintConstants.PDB_CONFIG_CONTEXT, ProductMaintConstants.PDB_IMAGE_URL_DELUXE_SUFFIX);
    String imageURLPremiumSuffix = rm.getFrpGlobalParmNoNull(ProductMaintConstants.PDB_CONFIG_CONTEXT, ProductMaintConstants.PDB_IMAGE_URL_PREMIUM_SUFFIX);
    
    String gbbTokenDisplay = rm.getProperty(ProductMaintConstants.PROPERTY_FILE, "GBB_TOKEN_DISPLAY");

    pageContext.setAttribute("liveLen", liveLen, PageContext.PAGE_SCOPE);
    pageContext.setAttribute("testLen", testLen, PageContext.PAGE_SCOPE);
    pageContext.setAttribute("uatLen", uatLen, PageContext.PAGE_SCOPE);
    pageContext.setAttribute("contentLen", contentLen, PageContext.PAGE_SCOPE);

    String goodLabel = "Label";
    String errorLabel = "error";

    String[] errorStrings = (String[])request.getAttribute(ProductMaintConstants.PDB_PRODUCT_UPSELL_ERROR_DESC_KEY );
    String[] assocIdError = (String[])request.getAttribute(ProductMaintConstants.PDB_PRODUCT_UPSELL_ASSOC_ID_ERROR_KEY );
    String[] assocNameError = (String[])request.getAttribute(ProductMaintConstants.PDB_PRODUCT_UPSELL_ASSOC_NAME_ERROR_KEY );
    String[] assocPriceError = (String[])request.getAttribute(ProductMaintConstants.PDB_PRODUCT_UPSELL_ASSOC_PRICE_ERROR_KEY );
    String[] assocTypeError = (String[])request.getAttribute(ProductMaintConstants.PDB_PRODUCT_UPSELL_ASSOC_TYPE_ERROR_KEY );
    String[] assocStatusError = (String[])request.getAttribute(ProductMaintConstants.PDB_PRODUCT_UPSELL_ASSOC_STATUS_ERROR_KEY );
%>

 // Good, Better, Best variables
 var productName1 = "";
 var productName2 = "";
 var productName3 = "";
 var standardPrice = "";
 var deluxePrice = "";
 var premiumPrice = "";
 var  defaultTitle = "<%= defaultTitleParm %>";
 var imageURL = "<%= imageURLPrefixParm %>";
 var imageURLType = "<%= imageURLTypeParm %>";
 var imageURLSuffix = "<%= imageURLStandardSuffix %>";

 var upsellId = new Array();
 var upsellName = new Array();
 var upsellPrice = new Array();
 var upsellImage = new Array();
 var upsellDefault = new Array();
 var upsellStatus = new Array();
 var productCount = 0;
 var foundDefaultSku = false;

<%
    java.util.List detailList = (java.util.List)request.getAttribute(ProductMaintConstants.SHOW_ASSOCIATE_LIST_KEY);
    for(int i = 0; i < detailList.size(); i++) {
        UpsellDetailVO upsellDetailVO = (UpsellDetailVO)detailList.get(i);
%>
        upsellId[productCount] = "<%= upsellDetailVO.getDetailID() %>";
        upsellName[productCount] = "<%= upsellDetailVO.getName().replaceAll("\"","&quot;") %>";
        upsellPrice[productCount] = "<%= upsellDetailVO.getPrice() %>";
        upsellImage[productCount] = imageURL + "<%= upsellDetailVO.getNovatorID() %>" + imageURLSuffix + "." + imageURLType;
        upsellDefault[productCount] = "<%= upsellDetailVO.getDefaultSkuFlag() %>";
        if (upsellDefault[productCount] == 'Y') foundDefaultSku = true;
        upsellStatus[productCount] = "<%= upsellDetailVO.isAvailable() %>";
        productCount += 1;
<%
    }
    
    //if (foundDefaultSku == false) upsellDefault[0] = "Y";
%>

function image_error(imageName) {
    //alert("image error: " + imageName + "\n" + document.getElementById(imageName).src);
    document.getElementById(imageName).src = "images/npi_a.jpg";
}

function init()
{
  if (foundDefaultSku == false) upsellDefault[0] = "Y";
  for (i=0; i<upsellId.length; i++) {
      if (upsellId[i] == document.getElementById("gbbUpsellDetailId1").value) {
          productName1 = upsellName[i];
          standardPrice = upsellPrice[i];
          document.getElementById("image1").src = upsellImage[i];
          if (document.getElementById("gbbUpsellDetailId1").options[0].value == "") {
              document.getElementById("gbbUpsellDetailId1").options[0] = null;
          }
      } else if (upsellId[i] == document.getElementById("gbbUpsellDetailId2").value) {
          productName2 = upsellName[i];
          deluxePrice = upsellPrice[i];
          document.getElementById("image2").src = upsellImage[i];
          if (document.getElementById("gbbUpsellDetailId2").options[0].value == "") {
              document.getElementById("gbbUpsellDetailId2").options[0] = null;
          }
      } else if (upsellId[i] == document.getElementById("gbbUpsellDetailId3").value) {
          productName3 = upsellName[i];
          premiumPrice = upsellPrice[i];
          document.getElementById("image3").src = upsellImage[i];
          if (document.getElementById("gbbUpsellDetailId3").options[0].value == "") {
              document.getElementById("gbbUpsellDetailId3").options[0] = null;
          }
      } else {
          if (document.getElementById("gbbUpsellDetailId1").value == "") {
              document.getElementById("gbbUpsellDetailId1").value = upsellId[i];
              productName1 = upsellName[i];
              standardPrice = upsellPrice[i];
              document.getElementById("image1").src = upsellImage[i];
              if (document.getElementById("gbbUpsellDetailId1").options[0].value == "") {
                  document.getElementById("gbbUpsellDetailId1").options[0] = null;
              }
          } else if (document.getElementById("gbbUpsellDetailId2").value == "") {
              document.getElementById("gbbUpsellDetailId2").value = upsellId[i];
              productName2 = upsellName[i];
              deluxePrice = upsellPrice[i];
              document.getElementById("image2").src = upsellImage[i];
              if (document.getElementById("gbbUpsellDetailId2").options[0].value == "") {
                  document.getElementById("gbbUpsellDetailId2").options[0] = null;
              }
          } else if (document.getElementById("gbbUpsellDetailId3").value == "") {
              document.getElementById("gbbUpsellDetailId3").value = upsellId[i];
              productName3 = upsellName[i];
              premiumPrice = upsellPrice[i];
              document.getElementById("image3").src = upsellImage[i];
              if (document.getElementById("gbbUpsellDetailId3").options[0].value == "") {
                  document.getElementById("gbbUpsellDetailId3").options[0] = null;
              }
          }
      }
      if (upsellDefault[i] == "Y") {
          document.getElementById("defaultSku" + i).checked = true;
      }
      if (i > 0 && upsellStatus[i] == "false") {
          document.getElementById("defaultSku" + i).disabled = true;
      }
  }
  
  if (standardPrice == "") standardPrice = "0";
  if (deluxePrice == "") deluxePrice = "0";
  if (premiumPrice == "") premiumPrice = "0";
  standardPrice = parseFloat(standardPrice).toFixed(2);
  deluxePrice = parseFloat(deluxePrice).toFixed(2);
  premiumPrice = parseFloat(premiumPrice).toFixed(2);
  
  document.getElementById("position1Text").innerHTML = productName1;
  document.getElementById("position2Text").innerHTML = productName2;
  document.getElementById("position3Text").innerHTML = productName3;
  document.getElementById("position1Price").innerHTML = formatPrice(standardPrice);
  document.getElementById("position2Price").innerHTML = formatPrice(deluxePrice);
  document.getElementById("position3Price").innerHTML = formatPrice(premiumPrice);
  if (document.getElementById("gbbTitle").value.length == 0) {
      document.getElementById("gbbTitle").value = defaultTitle;
  }
  changeName1();
  changeName2();
  changeName3();
  changePrice1();
  changePrice2();
  changePrice3();
  if (upsellId.length < 3) {
      disableGBB();
  }
  //setTimeout( 'pngFix()', 5000 );

}

function unload(){}

function changeName1() {
    if (document.getElementById("gbbNameOverrideFlag1").checked == true) {
        document.getElementById("position1Text").innerHTML = document.getElementById("gbbNameOverrideText1").value;
    } else {
        document.getElementById("position1Text").innerHTML = productName1;
    }
}

function changePrice1() {
    if (document.getElementById("gbbPriceOverrideFlag1").checked == true) {
        document.getElementById("position1Price").innerHTML = formatPrice(document.getElementById("gbbPriceOverrideText1").value);
    } else {
        document.getElementById("position1Price").innerHTML = formatPrice(standardPrice);
    }
}

function changeName2() {
    if (document.getElementById("gbbNameOverrideFlag2").checked == true) {
        document.getElementById("position2Text").innerHTML = document.getElementById("gbbNameOverrideText2").value;
    } else {
        document.getElementById("position2Text").innerHTML = productName2;
    }
}

function changePrice2() {
    if (document.getElementById("gbbPriceOverrideFlag2").checked == true) {
        document.getElementById("position2Price").innerHTML = formatPrice(document.getElementById("gbbPriceOverrideText2").value);
    } else {
        document.getElementById("position2Price").innerHTML = formatPrice(deluxePrice);
    }
}

function changeName3() {
    if (document.getElementById("gbbNameOverrideFlag3").checked == true) {
        document.getElementById("position3Text").innerHTML = document.getElementById("gbbNameOverrideText3").value;
    } else {
        document.getElementById("position3Text").innerHTML = productName3;
    }
}

function changePrice3() {
    if (document.getElementById("gbbPriceOverrideFlag3").checked == true) {
        document.getElementById("position3Price").innerHTML = formatPrice(document.getElementById("gbbPriceOverrideText3").value);
    } else {
        document.getElementById("position3Price").innerHTML = formatPrice(premiumPrice);
    }
}

function formatPrice(price) {
    var num = price.toString().replace(/\$|\,/g,'');
    if(isNaN(num))
      num = "0";
    sign = (num == (num = Math.abs(num)));
    num = Math.floor(num*100+0.50000000001);
    cents = num%100;
    num = Math.floor(num/100).toString();
    if(cents<10)
      cents = "0" + cents;
    for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
      num = num.substring(0,num.length-(4*i+3))+','+
    num.substring(num.length-(4*i+3));
    return '$' + (((sign)?'':'-') + num + '.' + cents);
}

function changePosition1() {
    newUpsellDetailId = document.getElementById("gbbUpsellDetailId1").value;
    if (newUpsellDetailId == "") {
        productName1 = "";
        standardPrice = "";
        document.getElementById("image1").src = "";
    } else {
        for (i=0; i<upsellId.length; i++) {
            if (upsellId[i] == newUpsellDetailId) {
                productName1 = upsellName[i];
                standardPrice = upsellPrice[i];
                document.getElementById("image1").src = upsellImage[i];
                break;
            }
        }
    }
    changeName1();
    changePrice1();
    //setTimeout( 'pngFix()', 1000 );
}

function changePosition2() {
    newUpsellDetailId = document.getElementById("gbbUpsellDetailId2").value;
    if (newUpsellDetailId == "") {
        productName2 = "";
        deluxePrice = "";
        document.getElementById("image2").src = "";
    } else {
        for (i=0; i<upsellId.length; i++) {
            if (upsellId[i] == newUpsellDetailId) {
                productName2 = upsellName[i];
                deluxePrice = upsellPrice[i];
                document.getElementById("image2").src = upsellImage[i];
                break;
            }
        }
    }
    changeName2();
    changePrice2();
    //setTimeout( 'pngFix()', 1000 );
}

function changePosition3() {
    newUpsellDetailId = document.getElementById("gbbUpsellDetailId3").value;
    if (newUpsellDetailId == "") {
        productName3 = "";
        premiumPrice = "";
        document.getElementById("image3").src = "";
    } else {
        for (i=0; i<upsellId.length; i++) {
            if (upsellId[i] == newUpsellDetailId) {
                productName3 = upsellName[i];
                premiumPrice = upsellPrice[i];
                document.getElementById("image3").src = upsellImage[i];
                break;
            }
        }
    }
    changeName3();
    changePrice3();
    //setTimeout( 'pngFix()', 2000 );
}

 function enableGBB() {
     document.getElementById("gbbPopoverFlag").disabled = false;
     document.getElementById("gbbTitle").disabled = false;
     document.getElementById("gbbUpsellDetailId1").disabled = false;
     document.getElementById("gbbNameOverrideFlag1").disabled = false;
     document.getElementById("gbbNameOverrideText1").disabled = false;
     document.getElementById("gbbPriceOverrideFlag1").disabled = false;
     document.getElementById("gbbPriceOverrideText1").disabled = false;
     document.getElementById("gbbUpsellDetailId2").disabled = false;
     document.getElementById("gbbNameOverrideFlag2").disabled = false;
     document.getElementById("gbbNameOverrideText2").disabled = false;
     document.getElementById("gbbPriceOverrideFlag2").disabled = false;
     document.getElementById("gbbPriceOverrideText2").disabled = false;
     document.getElementById("gbbUpsellDetailId3").disabled = false;
     document.getElementById("gbbNameOverrideFlag3").disabled = false;
     document.getElementById("gbbNameOverrideText3").disabled = false;
     document.getElementById("gbbPriceOverrideFlag3").disabled = false;
     document.getElementById("gbbPriceOverrideText3").disabled = false;
 }
 
 function disableGBB() {
     document.getElementById("gbbPopoverFlag").disabled = true;
     document.getElementById("gbbTitle").disabled = true;
     document.getElementById("gbbUpsellDetailId1").disabled = true;
     document.getElementById("gbbNameOverrideFlag1").disabled = true;
     document.getElementById("gbbNameOverrideText1").disabled = true;
     document.getElementById("gbbPriceOverrideFlag1").disabled = true;
     document.getElementById("gbbPriceOverrideText1").disabled = true;
     document.getElementById("gbbUpsellDetailId2").disabled = true;
     document.getElementById("gbbNameOverrideFlag2").disabled = true;
     document.getElementById("gbbNameOverrideText2").disabled = true;
     document.getElementById("gbbPriceOverrideFlag2").disabled = true;
     document.getElementById("gbbPriceOverrideText2").disabled = true;
     document.getElementById("gbbUpsellDetailId3").disabled = true;
     document.getElementById("gbbNameOverrideFlag3").disabled = true;
     document.getElementById("gbbNameOverrideText3").disabled = true;
     document.getElementById("gbbPriceOverrideFlag3").disabled = true;
     document.getElementById("gbbPriceOverrideText3").disabled = true;
 }

 function swamp(rowa,rowb){


	//get new row values - ROW A
	tempADetailID = document.forms[0].detailid[rowb].value;
	tempADetailname = document.forms[0].detailname[rowb].value;
	tempADetailtype = document.forms[0].detailtype[rowb].value;
	tempADetailprice = document.forms[0].detailprice[rowb].value;
	tempADetailavailable = document.forms[0].detailavailable[rowb].value;		
	tempANovatorID = document.forms[0].novatorID[rowb].value;			
	tempASentToNovatorProd = document.forms[0].sentToNovatorProd[rowb].value;				
	tempASentToNovatorContent = document.forms[0].sentToNovatorContent[rowb].value;				
	tempASentToNovatorUAT = document.forms[0].sentToNovatorUAT[rowb].value;				
	tempASentToNovatorTest = document.forms[0].sentToNovatorTest[rowb].value;
        tempADefaultSku = document.forms[0].defaultSku[rowb].checked;
	//get new row values - ROW B	
	tempBDetailID = document.forms[0].detailid[rowa].value;
	tempBDetailname = document.forms[0].detailname[rowa].value;
	tempBDetailtype = document.forms[0].detailtype[rowa].value;
	tempBDetailprice = document.forms[0].detailprice[rowa].value;
	tempBDetailavailable = document.forms[0].detailavailable[rowa].value;	
	tempBNovatorID = document.forms[0].novatorID[rowa].value;			
	tempBSentToNovatorProd = document.forms[0].sentToNovatorProd[rowa].value;				
	tempBSentToNovatorContent = document.forms[0].sentToNovatorContent[rowa].value;				
	tempBSentToNovatorUAT = document.forms[0].sentToNovatorUAT[rowa].value;				
	tempBSentToNovatorTest = document.forms[0].sentToNovatorTest[rowa].value;				
        tempBDefaultSku = document.forms[0].defaultSku[rowa].checked;

	//update hidden & text fields - ROW A 
	document.forms[0].detailid[rowa].value = tempADetailID;
	document.forms[0].detailname[rowa].value = tempADetailname;
	document.forms[0].detailtype[rowa].value = tempADetailtype;
	document.forms[0].detailprice[rowa].value = tempADetailprice;
	document.forms[0].detailavailable[rowa].value = tempADetailavailable;			
	document.forms[0].novatorID[rowa].value = tempANovatorID;			
	document.forms[0].sentToNovatorProd[rowa].value = tempASentToNovatorProd;			
	document.forms[0].sentToNovatorContent[rowa].value = tempASentToNovatorContent;			
	document.forms[0].sentToNovatorUAT[rowa].value = tempASentToNovatorUAT;			
	document.forms[0].sentToNovatorTest[rowa].value = tempASentToNovatorTest;
        document.forms[0].defaultSku[rowa].checked = tempADefaultSku;
	//update hidden & text fields - ROW B
	document.forms[0].detailid[rowb].value = tempBDetailID;
	document.forms[0].detailname[rowb].value = tempBDetailname;
	document.forms[0].detailtype[rowb].value = tempBDetailtype;
	document.forms[0].detailprice[rowb].value = tempBDetailprice;
	document.forms[0].detailavailable[rowb].value = tempBDetailavailable;		
	document.forms[0].novatorID[rowb].value = tempBNovatorID;			
	document.forms[0].sentToNovatorProd[rowb].value = tempBSentToNovatorProd;			
	document.forms[0].sentToNovatorContent[rowb].value = tempBSentToNovatorContent;			
	document.forms[0].sentToNovatorUAT[rowb].value = tempBSentToNovatorUAT;			
	document.forms[0].sentToNovatorTest[rowb].value = tempBSentToNovatorTest;			
        document.forms[0].defaultSku[rowb].checked = tempBDefaultSku;
	
	//update label\dislay values - ROWA
	eval('document.all["lbldetailid"][rowa].innerHTML = tempADetailID');
	eval('document.all["lbldetailtype"][rowa].innerHTML = tempADetailtype');
	eval('document.all["lbldetailprice"][rowa].innerHTML = tempADetailprice');
	eval('document.all["lbldetailavailable"][rowa].innerHTML = tempADetailavailable');
	//update label\dislay values - ROWB
	eval('document.all["lbldetailid"][rowb].innerHTML = tempBDetailID');
	eval('document.all["lbldetailtype"][rowb].innerHTML = tempBDetailtype');
	eval('document.all["lbldetailprice"][rowb].innerHTML = tempBDetailprice');
	eval('document.all["lbldetailavailable"][rowb].innerHTML = tempBDetailavailable');

}

function removeSKU(id)
{	
    var answer = confirm ("Are you sure you want to remove the SKU?")
    if (answer){
        if (document.getElementById("gbbUpsellDetailId1").value == id) {
            document.getElementById("gbbUpsellDetailId1").value = "";
            document.getElementById("gbbPopoverFlag").checked = false;
        } else if (document.getElementById("gbbUpsellDetailId2").value == id) {
            document.getElementById("gbbUpsellDetailId2").value = "";
            document.getElementById("gbbPopoverFlag").checked = false;
        } else if (document.getElementById("gbbUpsellDetailId3").value == id) {
            document.getElementById("gbbUpsellDetailId3").value = "";
            document.getElementById("gbbPopoverFlag").checked = false;
        }
        enableGBB();
	submitForm('deleteUpsellSKU.do?deletesku=' + id);
    	}
}

function addSKU() {
    enableGBB();
    submitForm('addUpsellSKU.do');
}
function checkSpelling(value, name)
{
    value = escape(value).replace(/\+/g,"%2B");
    var url = "SpellCheckServlet?content=" + value + "&callerName=" + name;
    window.open(url,"Spell_Check","toolbar=no,resizable=yes,scrollbars=yes,width=500,height=300");
}

function submitUpsellForm() {
    msg = "";
    if (document.getElementById("gbbTitle").value == "" && document.getElementById("gbbPopoverFlag").checked) {
        msg = msg + "The Good, Better, Best pop-over must have something in the title field.\nIf you want to show a blank title, please enter a space and resubmit\n";
    }
    if ( (document.getElementById("gbbUpsellDetailId1").value != "" && document.getElementById("gbbUpsellDetailId1").value == document.getElementById("gbbUpsellDetailId2").value) ||
            (document.getElementById("gbbUpsellDetailId1").value != "" && document.getElementById("gbbUpsellDetailId1").value == document.getElementById("gbbUpsellDetailId3").value) ||
            (document.getElementById("gbbUpsellDetailId2").value != "" && document.getElementById("gbbUpsellDetailId2").value == document.getElementById("gbbUpsellDetailId3").value) ) {

        msg = msg + "Each position must have different sku numbers, please make the appropriate changes.\n";
    }
    if (msg != "" ) {
        alert(msg);
        return false;
    }
    enableGBB();
    if (document.getElementById("gbbTitle").value == defaultTitle) {
        document.getElementById("gbbTitle").value = "";
    }
    for(i = 0; i < upsellId.length; i++) {
        if (document.getElementById('defaultSku' + i).checked == true) {
            document.getElementById('defaultSkuFlag').value = i;
        }
    }
    
    for(i = 0; i < upsellName.length; i++)
    {
     replaceSymboltoHtml(document.getElementById("detailname"+i).value,"detailname"+i);
    }

    document.forms[0].submit();
}


function replaceSymboltoHtml(val,id) 
{
	var str = val;
    var elementId=id;
    var n = str.indexOf("\u00AE");
    var n1= str.indexOf("\u2122");	
    var n2= str.indexOf("\u002A");
    var n3= str.indexOf("\u00E8");
        if(n!=-1)
			{					
				str=str.replace(/\u00AE/g,"&reg;");   					
			}
        if(n1!=-1)
			{
				str=str.replace(/\u2122/g,"&trade;");											
			}
        if(n2!=-1)
        	{
        	   str=str.replace(/\u002A/g,"&#042;");
        	}
        if(n3!=-1)
        	{
        		str=str.replace(/\u00E8/g,"&#232;");
        	}
        
     document.getElementById(elementId).value =str;
    
	}

</script>

<bean:define id="upsellForm" name="upsellDetailForm" type="com.ftd.pdb.web.UpsellDetailForm" />

<html:form action="/submitUpsellDetail">
<jsp:include page="includes/security.jsp"/>


    <CENTER>

<TABLE width="98%" border="0" cellpadding="2" cellspacing="2">
    <TR>
		<TD colspan="3" class="Label">
            <input type="hidden" name="lastUpdateDateString" id="lastUpdateDateString" value="03/06/2003 11:19:17 AM">
            <input type="hidden" name="productId" id="productId" value="8112">
            <input type="hidden" name="defaultSkuFlag" value="0" id="defaultSkuFlag">
        </TD>
	</TR>
    <logic:present name="instruction">
    <TR>
		<TD colspan="3" class="Instruction">
            <bean:write name="instruction" ignore="true" scope="request" />
        </TD>
    </logic:present>

    <logic:present name="novatornote">
    <TR>
		<TD colspan="3" class="error">
            <bean:write name="novatornote" ignore="true" scope="request" />
        </TD>
	</TR>
    </logic:present>
    <TR><TD colspan="3" align="left" class="TblHeader">Upsell Details</TD></TR>

	<TR>
		<TD width="100px" align="left" class="Label">Master SKU
                <html:errors property="masterSKU"/>
		</TD>
		<TD width="80%" align="left" >
			<html:hidden property="masterSKU" />
			<bean:write name="upsellDetailForm" property="masterSKU" />
		</TD>
        <TD>&nbsp;</TD>
	</TR>
	<TR title="When checked, this product is available and will be displayed.">
		<TD align="left" class="Label" >Arrangement Available
                <html:errors property="arrangementAvailable"/>
                </TD>
		<TD align="left" >
            <html:checkbox property="arrangementAvailable" />
		</TD>
            <TD class="Instruction" />
	</TR>
	<TR title="When checked, this product will be displayed on the corporate web site.">
		<TD align="left" class="Label">Corporate Site
                <html:errors property="corporateSiteFlag"/>
                </TD>
		<TD align="left" >
            <html:checkbox property="corporateSiteFlag" />
		</TD>
        <TD class="Instruction">
            
		</TD>
	</TR>
	<TR title="Select which companies this master sku will be available for.">
    <TD align="left" valign="top" class="Label">Company
                <html:errors property="companyArray"/>
		</TD>
		<TD align="left" >
            <html:select property="companyArray" multiple="true">  
                <html:options collection="companyMasterList" property="id" labelProperty="description"/>
            </html:select>
		</TD>
        <TD/>
            
		</TD>
	</TR>
	<TR title="100 character max, HTML codes allowed">
		<TD valign="top" align="left" class="Label">Product Name
                <html:errors property="productName"/>
		</TD>
		<TD align="left"  colspan="2">
            <html:text property="productName" size="80" maxlength="100" onchange="replaceSymboltoHtml(this.value,'productName')" />
            <BR>
            <input type="button" name="btnSpellCheck" value="Spell Check" onclick="checkSpelling(productName.value,'productName');">
		</TD>
	</TR>
	<TR>
		<TD valign="top" align="left" class="Label">Description
                    <html:errors property="description"/>
		</TD>
		<TD align="left" colspan="2" class="Label">
		<html:textarea property="description" rows="7" cols="80" onchange="replaceSymboltoHtml(this.value,'description')" />

            <BR>
            <input type="button" name="btnSpellCheck" value="Spell Check" onclick="checkSpelling(description.value,'description');">
		</TD>
	</TR>
	<TR>
		<TD valign="top" align="left" class="Label" title = "<%= ProductMaintConstants.PDB_PRODUCT_MAINT_TIP_WEBSITES %>">Websites
		</TD>
		<TD align="left" colspan="2" class="Label">
		    <html:textarea property="websites" rows="3" cols="80" />
		</TD>
	</TR>
	<TR>
		<TD valign="top" align="left" colspan="3">
                    <html:errors property="websites"/>
		</TD>
	</TR>        
	<TR>
		<TD colspan="3" class="TblHeader" align="left">
			Search
		</TD>
	</TR>
	<TR>
		<TD class="Label" align="left" valign="top">Recipient Search</TD>
		<TD align="left"  colspan="2">
            <html:select property="recipientSearchArray" multiple="true">
                <html:options collection="recipientSearchList" property="id" labelProperty="description"/>
            </html:select>
		</TD>
	</TR>
	<TR>
		<TD class="Label" align="left" valign="top">Search Priority</TD>
		<TD align="left" >
            <html:select property="searchPriority">
                 <html:options collection="searchPriorityList" property="id" labelProperty="description"/>
            </html:select>
		</TD>
		<TD align="left" >
			&nbsp;
		</TD>
	</TR>
	<TR>
		<TD class="Label" align="left" valign="top">Keyword Search</TD>
		<TD colspan="2" align="left">
            <html:textarea property="keywordSearch" rows="3" cols="60" onchange="replaceSymboltoHtml(this.value,'keywordSearch')" />
		</TD>
	</TR>
	<TR>
		<TD class="TblHeader" colspan="3" align="left">
                Associated SKU's
 		</TD>
	</TR>
	<TR>
		<TD colspan="3" align="left">
                <html:errors property="associatedSkus"/>
 		</TD>
	</TR>
    <TR>


       <TD colspan="3" align="left">
        	<TABLE border="0" cellpadding="3" cellspacing="3">
	<%
	java.util.List showList = (java.util.List)request.getAttribute(ProductMaintConstants.SHOW_ASSOCIATE_LIST_KEY);
        for(int i = 0; i < showList.size(); i++)
	{
		UpsellDetailVO upsellDetailVO = (UpsellDetailVO)showList.get(i);
		String available = "N";
		if (upsellDetailVO.isAvailable()) { available = "Y"; }
		%>

		<input type="hidden" name="novatorID" value="<%=upsellDetailVO.getNovatorID()%>">
		<input type="hidden" name="sentToNovatorProd" value="<%=upsellDetailVO.isSentToNovatorProd()%>">
		<input type="hidden" name="sentToNovatorContent" value="<%=upsellDetailVO.isSentToNovatorContent()%>">
		<input type="hidden" name="sentToNovatorUAT" value="<%=upsellDetailVO.isSentToNovatorUAT()%>">
		<input type="hidden" name="sentToNovatorTest" value="<%=upsellDetailVO.isSentToNovatorTest()%>">

        	<TR>
        	    <TD>
                    <% if(showList.size() == 1 || i > 0){ %>
                        <input type="button" name="btnDeleteSKU" value="Delete" onClick="removeSKU('<%=upsellDetailVO.getDetailID()%>');"/>
                    <% } %>
                    </TD>
                    <TD> 
                    <% if(i > 1){ %> 
                        <img src="./images/arrowUp.gif" onClick="swamp(<%=i%>,<%=i-1%>);"> 
                    <% } %>  
                    </TD>
                    <TD> 
                    <% if(i+1 < showList.size() && i > 0){ %>       
                            <img src="./images/arrowDown.gif" onClick="swamp(<%=i%>,<%=i+1%>);"> 
                    <% } %> 
                    </TD>
	            <TD align="left" class="Label">
                    <b><div id="lbldetailid"><%=upsellDetailVO.getDetailID()%></div></b>
                    <input type="hidden" name="detailid" value="<%=upsellDetailVO.getDetailID()%>">
	            </TD>
                    <TD align="left" >
                        <input type="text" id="detailname<%= i %>" name="detailname" maxlength="250" size="25" value="<%=upsellDetailVO.getName().replaceAll("\"","&quot;")%>"
                        onchange="replaceSymboltoHtml(this.value,'detailname<%= i %>')" onblur="replaceSymboltoHtml(this.value,'detailname<%= i %>')"/>
                    </TD>
                    <td align="left" class="Label">Default:</td>
                    <td align="center">
                        <input type="radio" name="defaultSku" id="defaultSku<%= i %>">
                    </td>
                    <TD align="left" class="Label">Type:
                    </TD>
	            <TD align="left" >
                        <div id="lbldetailtype"><%=upsellDetailVO.getType()%></DIV>
                        <input type="hidden" name="detailtype" value="<%=upsellDetailVO.getType()%>">
                    </TD>
                    <TD align="left" class="Label">Price:
                    </TD>
	            <TD align="left" >
                        <div id="lbldetailprice"><%= FTDUtil.formatFloat(new Float(upsellDetailVO.getPrice()), 2, false, false)%></DIV>
                        <input type="hidden" name="detailprice" value="<%=upsellDetailVO.getPrice()%>"> 
                    </TD>
	            <TD align="left" class="Label">Available:
			    </TD>
	            <TD align="left" >
                        <div id="lbldetailavailable"><%=available%></DIV>
                        <input type="hidden" name="detailavailable" value="<%=available%>">
                    </TD>
	        </TR>
	    <%  }  %>

        	</TABLE>
          </TD>
      </TR>

	<%     
if(showList.size() < ProductMaintConstants.UPSELL_MAX_ASSOC_SKU_AMOUNT){ %>

		<TR>
			<TD align="left" >
                        
                        <html:button property="btnAddSku" value="Add New SKU" onclick="addSKU()"/>
			<%--	<input type="button" name="btnAddNewSubCode" value="Add New SKU" onclick="submitForm('addUpsellSKU.do');">--%>
			</TD>
			<TD align="left" ><html:text property="newSKU" size="15" maxlength="10" /></TD>
			<TD align="left" colspan="1"> &nbsp; </TD>
		</TR>
	<% } %>
    <TR>
		<TD align="left" colspan="3">&nbsp;</TD>
    </TR>

    <tr>
        <td class="TblHeader" colspan="3" align="left">
        Good, Better, Best
 	<td>
    </tr>
    <tr>

      <td colspan="3" align="left">
        <table border="0" cellpadding="3" cellspacing="3" width="100%">

        <tr>
          <td width="50%" valign = "top">
              <table border = "0" cellpadding = "0" cellspacing = "2" width="100%">
                <tbody>
                  <tr>
                      <td class = "Label" width="35%">
                          Good, Better, Best Popover
                      </td>
                      <td width="5%">
                          <html:checkbox property = "gbbPopoverFlag" styleId = "gbbPopoverFlag"/>
                      </td>
                      <td width="60%">
                          &nbsp;
                      </td>
                  </tr>
                  <tr title="255 character max">
                      <td class = "Label" colspan="3" align="left">
                          <label id = "gbbTitleLabel" for = "gbbTitle">Title</label>
                          &nbsp;&nbsp;
                          <html:text property = "gbbTitle" size = "35" maxlength="255" styleId = "gbbTitle" onchange="replaceSymboltoHtml(this.value,'gbbTitle')"/>
                      </td>
                  </tr>

                  <tr>
                      <td class = "Label" colspan="3" align="left">
                          Position 1
                          &nbsp;&nbsp;
                          <html:select property="gbbUpsellDetailId1" onchange="changePosition1()" styleId="gbbUpsellDetailId1">
                              <html:options collection="upsellDetailIdList" property="id" labelProperty="description"/>
                          </html:select>
                      </td>
                  </tr>
                  <tr title = "<%=gbbTokenDisplay %>">
                      <td class = "Label" align="right">
                          Product Name Override
                      </td>
                      <td>
                          <html:checkbox property = "gbbNameOverrideFlag1" onclick="changeName1()" styleId="gbbNameOverrideFlag1" />
                      </td>
                      <td>
                          <html:text property="gbbNameOverrideText1" styleId="gbbNameOverrideText1" size="35" maxlength="255" onchange="replaceSymboltoHtml(this.value,'gbbNameOverrideText1'); changeName1()"/>
                      </td>
                  </tr>
                  <tr title = "<%=gbbTokenDisplay %>">
                      <td class = "Label" align="right">
                          Product Price Override
                      </td>
                      <td>
                          <html:checkbox property = "gbbPriceOverrideFlag1" styleId="gbbPriceOverrideFlag1" onclick="changePrice1()"/>
                      </td>
                      <td>
                          <html:text property="gbbPriceOverrideText1" styleId="gbbPriceOverrideText1" size="35" maxlength="255" onchange="changePrice1()"/>
                      </td>
                  </tr>

                  <tr>
                      <td class = "Label" colspan="3" align="left">
                          Position 2
                          &nbsp;&nbsp;
                          <html:select property="gbbUpsellDetailId2" styleId="gbbUpsellDetailId2" onchange="changePosition2()">
                              <html:options collection="upsellDetailIdList" property="id" labelProperty="description"/>
                          </html:select>
                      </td>
                  </tr>
                  <tr title = "<%=gbbTokenDisplay %>">
                      <td class = "Label" align="right">
                          Product Name Override
                      </td>
                      <td>
                          <html:checkbox property = "gbbNameOverrideFlag2" styleId="gbbNameOverrideFlag2" onclick="changeName2()"/>
                      </td>
                     <td>
                          <html:text property="gbbNameOverrideText2" styleId="gbbNameOverrideText2" size="35" maxlength="255" onchange="replaceSymboltoHtml(this.value,'gbbNameOverrideText2');changeName2()"/>
                      </td>
                  </tr>
                  <tr title = "<%=gbbTokenDisplay %>">
                      <td class = "Label" align="right">
                          Product Price Override
                      </td>
                      <td>
                          <html:checkbox property = "gbbPriceOverrideFlag2" styleId="gbbPriceOverrideFlag2" onclick="changePrice2()"/>
                      </td>
                      <td>
                          <html:text property="gbbPriceOverrideText2" styleId="gbbPriceOverrideText2" size="35" maxlength="255" onchange="changePrice2()"/>
                      </td>
                  </tr>

                  <tr>
                      <td class = "Label" colspan="3" align="left">
                          Position 3
                          &nbsp;&nbsp;
                          <html:select property="gbbUpsellDetailId3" styleId="gbbUpsellDetailId3" onchange="changePosition3()">
                              <html:options collection="upsellDetailIdList" property="id" labelProperty="description"/>
                          </html:select>
                      </td>
                  </tr>
                  <tr title = "<%=gbbTokenDisplay %>">
                      <td class = "Label" align="right">
                          Product Name Override
                      </td>
                      <td>
                          <html:checkbox property = "gbbNameOverrideFlag3" styleId="gbbNameOverrideFlag3" onclick="changeName3()"/>
                      </td>
                      <td>
                          <html:text property="gbbNameOverrideText3" styleId="gbbNameOverrideText3" size="35" maxlength="255" onchange="replaceSymboltoHtml(this.value,'gbbNameOverrideText3');changeName3()"/>
                      </td>
                  </tr>
                  <tr title = "<%=gbbTokenDisplay %>">
                      <td class = "Label" align="right">
                          Product Price Override
                      </td>
                      <td>
                          <html:checkbox property = "gbbPriceOverrideFlag3" styleId="gbbPriceOverrideFlag3" onclick="changePrice3()"/>
                      </td>
                     <td>
                          <html:text property="gbbPriceOverrideText3" styleId="gbbPriceOverrideText3" size="35" maxlength="255" onchange="changePrice3()"/>
                      </td>
                  </tr>

                </tbody>
              </table>
          </td>

          <td width="50%" valign = "top">
              <table border = "0" cellpadding = "0" cellspacing = "2" width="100%">
                <tbody>
                  <tr>
                      <td class = "Label" align = "center" width="33%">
                          Position 1
                      </td>

                      <td class = "Label" align = "center" width="33%">
                          Position 2
                      </td>

                      <td class = "Label" align = "center" width="33%">
                          Position 3
                      </td>

                  </tr>

                  <tr>
                      <td align="center">
                          <img name="image1" id="image1" src="images/npi_a.jpg" alt="Position 1" border="0" height="120" width="120" onerror='image_error("image1")'>
                      </td>
                      <td align="center">
                          <img name="image2" id="image2" src="images/npi_a.jpg" alt="Position 2" border="0" height="120" width="120" onerror='image_error("image2")'>
                      </td>
                      <td align="center">
                          <img name="image3" id="image3" src="images/npi_a.jpg" alt="Position 3" border="0" height="120" width="120" onerror='image_error("image3")'>
                      </td>
                  </tr>

                  <tr height="50">
                      <td align="center" valign="top">
                          <label id="position1Text"/>
                      </td>
                      <td align="center" valign="top">
                          <label id="position2Text"/>
                      </td>
                      <td align="center" valign="top">
                          <label id="position3Text"/>
                      </td>
                  </tr>

                  <tr>
                      <td align="center">
                          <label id="position1Price"/>
                      </td>
                      <td align="center">
                          <label id="position2Price"/>
                      </td>
                      <td align="center">
                          <label id="position3Price"/>
                      </td>
                  </tr>

                </tbody>
            </table>
        </td>

        </tr>
        </table>
      </td>
        
    </tr>

	<TR>
		<TD align="left" colspan="3" class="TblHeader" align="left">
			Novator Database
		</TD>
	</TR>
	<TR title="Each Novator database that is selected will be updated.">
		<TD colspan="3">
		    <logic:greaterThan value="0" name="liveLen">
			<html:checkbox property="chkProd" />Live
		    </logic:greaterThan>&nbsp;
		    <logic:greaterThan value="0" name="testLen">
			<html:checkbox property="chkTest" />Test
		    </logic:greaterThan>&nbsp;
		    <logic:greaterThan value="0" name="uatLen">
			<html:checkbox property="chkUAT" />UAT
			</logic:greaterThan>&nbsp;
			<logic:greaterThan value="0" name="contentLen">
			<html:checkbox property="chkContent" />Content
			</logic:greaterThan>&nbsp;
		</TD>
	</TR>

	<TR>
		<TD colspan="3" align="center">
                    <html:button property = "btnSubmitForm" value = "Submit" onclick = "javascript: submitUpsellForm()"/>
		</TD>
	</TR>
</TABLE>




    </CENTER>
</html:form>
