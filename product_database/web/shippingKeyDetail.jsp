<%@ page  contentType="text/html;charset=windows-1252" errorPage="/error.jsp"%>
<%@ page import="com.ftd.osp.utilities.ConfigurationUtil"%>
<%@ page import="com.ftd.pdb.common.utilities.FTDUtil"%>
<%@ page import="com.ftd.pdb.common.ProductMaintConstants"%>
<%@ page import="com.ftd.pdb.common.valobjs.ShippingKeyPriceVO"%>
<%@ page import="com.ftd.pdb.common.valobjs.ShippingMethodVO"%>
<%@ page import="com.ftd.pdb.web.ShippingKeyDetailForm"%>
<%@ page import="java.util.List" %>

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<script type="text/javascript">
    function init(){}
    function unload(){}
</script>
<script type="text/javascript" src="js/util.js">
</script>
<link rel="stylesheet" type="text/css" href="css/ftd.css" />

<%
    /*
    The feed appears incomplete so we are disabling it.
    TODO: fix the feed so that it updates prices
    
    ConfigurationUtil rm = ConfigurationUtil.getInstance();
    Integer liveLen = new Integer(rm.getFrpGlobalParmNoNull(ProductMaintConstants.PDB_CONFIG_CONTEXT, ProductMaintConstants.PDB_NOVATOR_LIVE_KEY).length());
    Integer testLen = new Integer(rm.getFrpGlobalParmNoNull(ProductMaintConstants.PDB_CONFIG_CONTEXT, ProductMaintConstants.PDB_NOVATOR_TEST_KEY).length());
    Integer uatLen = new Integer(rm.getFrpGlobalParmNoNull(ProductMaintConstants.PDB_CONFIG_CONTEXT, ProductMaintConstants.PDB_NOVATOR_UAT_KEY).length());
    Integer contentLen = new Integer(rm.getFrpGlobalParmNoNull(ProductMaintConstants.PDB_CONFIG_CONTEXT, ProductMaintConstants.PDB_NOVATOR_CONTENT_KEY).length());
    */
    Integer liveLen = 0;
    Integer testLen = 0;
    Integer uatLen = 0;
    Integer contentLen = 0;

    pageContext.setAttribute("liveLen", liveLen, PageContext.PAGE_SCOPE);
    pageContext.setAttribute("testLen", testLen, PageContext.PAGE_SCOPE);
    pageContext.setAttribute("uatLen", uatLen, PageContext.PAGE_SCOPE);
    pageContext.setAttribute("contentLen", contentLen, PageContext.PAGE_SCOPE);
%>

<CENTER>
     
                <html:form action="/updateEditShipKeyDetail" method="post">
                        <jsp:include page="includes/security.jsp"/>
                        <input type="hidden" name="actionType" value="edit" />
                        <input type="hidden" name="actionStatus" value="cancelled" />


			<CENTER>

			<TABLE width="98%" border="0" cellpadding="2" cellspacing="2">
            	<TR>
                	<TD width="50%" class="LabelRight"> Shipping Key: </TD>
                	<TD width="50%" align="left"> <bean:write name="shippingKeyDetailForm" property="shippingKey" /> </TD>
                                     <html:hidden name="shippingKeyDetailForm" property="shippingKey" />
                </TR>
  

            	<TR>
                	<TD class="LabelRight"> Description: </TD>
                	<TD align="left">
                		<html:text name="shippingKeyDetailForm" property="description" size="30" />
                	</TD>
                </TR>

            	<TR>
                	<TD class="LabelRight"> Shipper: </TD>
                	<TD align="left">
                		<html:text name="shippingKeyDetailForm" property="shipper" size="30" />
                	</TD>
                </TR>
                <TR>
                  <TD align="center" colspan="2" class="Error">
                       <html:messages id="errors">
                        <bean:write name="errors" />
                        <br />
                        </html:messages>
                  </td>
                </TR>
                
			</TABLE>

			<BR>

			<TABLE width="98%" border="0" cellpadding="2" cellspacing="2">
				<TR>
					<TD WIDTH="12%" class="TblTextHeader"> Min Price </TD>
					<TD WIDTH="12%" class="TblTextHeader"> Max Price </TD>
      				<TD WIDTH="14%" class="TblTextHeader"> Same-Day Gift Cost </TD>
					<TD WIDTH="12%" class="TblTextHeader"> Standard Cost </TD>
					<TD WIDTH="12%" class="TblTextHeader"> Two Day Cost </TD>
					<TD WIDTH="12%" class="TblTextHeader"> Next Day Cost </TD>
					<TD WIDTH="12%" class="TblTextHeader"> Saturday Cost </TD>
					<TD WIDTH="12%" class="TblTextHeader"> Sunday Cost </TD>
				</TR>

                <%
                    ShippingKeyDetailForm form = (ShippingKeyDetailForm)request.getAttribute("org.apache.struts.taglib.html.BEAN");
                    List prices = (List)form.get("itemDetailList");
                    if(prices != null)
                    {
			    for(int i = 0; i < prices.size(); i++)
			    {
				ShippingKeyPriceVO priceVO = (ShippingKeyPriceVO)prices.get(i);
				List methods = priceVO.getShippingMethods();
                %>
                        <TR>
                            <TD align="center">
                                <input type="hidden" name="shippingPriceId" value="<%=priceVO.getShippingKeyPriceID()%>">
                                $ <input type="text"  name="minPrice" value="<%=FTDUtil.formatFloat(new Float(priceVO.getMinPrice().floatValue()), 2, false, false)%>" size="10" />
                            </TD>
                            <TD align="center">
                                $ <input type="text"  name="maxPrice" value="<%=FTDUtil.formatFloat(new Float(priceVO.getMaxPrice().floatValue()), 2, false, false)%>" size="10" />
                            </TD>
                <%

      			if(methods != null)
      			{
                        for(int j = 0; j < methods.size(); j++)
                        {
                            ShippingMethodVO methodVO = (ShippingMethodVO)methods.get(j);
                            if(methodVO.getId().equals(ProductMaintConstants.SHIPPING_SAME_DAY_CODE))
                            {
                            out.print("<TD align=\"center\"> $ <input type=\"text\" name=\"sameDayCost\" value=\"" + FTDUtil.formatFloat(new Float(methodVO.getCost()), 2, false, false) + "\" size=\"10\" /><input type=\"hidden\" name=\"sameDayPriceId\" value=\"" + methodVO.getPriceId() + "\"></TD>");
                            }
                        }
                        for(int j = 0; j < methods.size(); j++)
                        {
                            ShippingMethodVO methodVO = (ShippingMethodVO)methods.get(j);
                            if(methodVO.getId().equals(ProductMaintConstants.SHIPPING_STANDARD_CODE))
                            {
                            out.print("<TD align=\"center\"> $ <input type=\"text\" name=\"standardCost\" value=\"" + FTDUtil.formatFloat(new Float(methodVO.getCost()), 2, false, false) + "\" size=\"10\" /> <input type=\"hidden\" name=\"standardPriceId\" value=\"" + methodVO.getPriceId() + "\"></TD>");
                            }
                        }
                        for(int j = 0; j < methods.size(); j++)
                        {   
                            ShippingMethodVO methodVO = (ShippingMethodVO)methods.get(j);                      
                            if(methodVO.getId().equals(ProductMaintConstants.SHIPPING_TWO_DAY_CODE))
                            {
                            out.print("<TD align=\"center\"> $ <input type=\"text\" name=\"twoDayCost\" value=\"" + FTDUtil.formatFloat(new Float(methodVO.getCost()), 2, false, false) + "\" size=\"10\" /><input type=\"hidden\" name=\"twoDayPriceId\" value=\"" + methodVO.getPriceId() + "\"></TD>");
                            }
                        }
                        for(int j = 0; j < methods.size(); j++)
                        {    
                            ShippingMethodVO methodVO = (ShippingMethodVO)methods.get(j);
                            if(methodVO.getId().equals(ProductMaintConstants.SHIPPING_NEXT_DAY_CODE))
                            {
                            out.print("<TD align=\"center\"> $ <input type=\"text\" name=\"nextDayCost\" value=\"" + FTDUtil.formatFloat(new Float(methodVO.getCost()), 2, false, false) + "\" size=\"10\" /><input type=\"hidden\" name=\"nextDayPriceId\" value=\"" + methodVO.getPriceId() + "\"></TD>");
                            }
                        }
                        for(int j = 0; j < methods.size(); j++)
                        {
                            ShippingMethodVO methodVO = (ShippingMethodVO)methods.get(j);
                            if(methodVO.getId().equals(ProductMaintConstants.SHIPPING_SATURDAY_CODE))
                            {
                            out.print("<TD align=\"center\"> $ <input type=\"text\" name=\"saturdayCost\" value=\"" + FTDUtil.formatFloat(new Float(methodVO.getCost()), 2, false, false) + "\" size=\"10\" /><input type=\"hidden\" name=\"saturdayPriceId\" value=\"" + methodVO.getPriceId() + "\"></TD>");
                            }
                        }
                        for(int j = 0; j < methods.size(); j++)
                        {
                            ShippingMethodVO methodVO = (ShippingMethodVO)methods.get(j);
                            if(methodVO.getId().equals(ProductMaintConstants.SHIPPING_SUNDAY_CODE))
                            {
                            out.print("<TD align=\"center\"> $ <input type=\"text\" name=\"sundayCost\" value=\"" + FTDUtil.formatFloat(new Float(methodVO.getCost()), 2, false, false) + "\" size=\"10\" /><input type=\"hidden\" name=\"sundayPriceId\" value=\"" + methodVO.getPriceId() + "\"></TD>");
                            }
                        }
  
			}
                %>
                        </TR>
                <%
	                    }
	        	}
                %>

				<TR> <TD colspan="8"> &nbsp; </TD> </TR>

				<TR>
					<TD class="Instruction" colspan="8">Examples: </TD>
				</TR>

				<TR>
					<TD class="Instruction" colspan="8">Min_Price=0.01 Max_Price=30.00 means from $0.01 to $30.00</TD>
				</TR>

				<TR>
					<TD class="Instruction" colspan="8">Min_Price=30.01 Max_Price=60.00 means from $30.01 to $60.00</TD>
				</TR>

				<TR>
					<TD class="Instruction" colspan="8">Min_Price=140.00 Max_Price=9999.99 means from $140 and up</TD>
				</TR>

				<TR> <TD colspan="8"> &nbsp; </TD> </TR>

                <TR>
                    <TD colspan="8" align="center">
            <logic:greaterThan value="0" name="liveLen">
                <html:checkbox property="chkLive" />Live<br>
            </logic:greaterThan>
            <logic:greaterThan value="0" name="testLen">
                <html:checkbox property="chkTest" />Test<br>
            </logic:greaterThan>
            <logic:greaterThan value="0" name="uatLen">
                <html:checkbox property="chkUAT" />UAT<br>
            </logic:greaterThan>
            <logic:greaterThan value="0" name="contentLen">
                <html:checkbox property="chkContent" />Content<br>
            </logic:greaterThan>
                     </TD>
                </TR>

				<TR>
					<TD colspan="8" align="center">
						<INPUT type="button" name="cancel" value="  Cancel  "  onclick="submitForm('showShippingKeyControl.do');" /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<!-- Issue 3940-->
							<INPUT type="submit" name="update" value="  Submit  " >
					</TD>
				</TR>

				<TR> <TD colspan="8"> &nbsp; </TD> </TR>

				<TR bgcolor=CCDDCC>
					<TD colspan="8" class="header3"> List of the Product IDs for this Shipping Key </TD>
				</TR>

				<TR> <TD colspan="8">
                <%
                    List list = (List)request.getAttribute("productID_list");
                    for (int i=0; i<list.size(); i++) {
                        out.println(list.get(i) );
                    }
                %>

                     </TD>
                </TR>


				<TR> <TD colspan="8"> &nbsp; </TD> </TR>
			</TABLE>

			</CENTER>
		</html:form>
</CENTER>
