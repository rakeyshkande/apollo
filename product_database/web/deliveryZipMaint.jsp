<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=windows-1252" errorPage="/error.jsp"%>
<%@ page import="com.ftd.pdb.common.DeliveryZipMaintConstants" %>
<%@ page import="com.ftd.pdb.common.PDBXMLTags" %>
<%
  String securitytoken = request.getParameter("securitytoken");
  if( securitytoken==null ) {
    securitytoken="null";
  }
  String applicationcontext = request.getParameter("applicationcontext");
  if( applicationcontext==null ) {
    applicationcontext="null";
  }
  String context = request.getParameter("context");
  if( context==null ) {
    context="null";
  }
%>

<script type="text/javascript" src="js/clock.js"></script>
<script type="text/javascript" src="js/prototype.js"></script>
<script type="text/javascript" src="js/rico.js"></script>
<script type="text/javascript" src="js/ftddom.js"></script>
<script type="text/javascript" src="js/ftdajax.js"></script>
<script type="text/javascript" src="js/ftdxml.js"></script>
<script type="text/javascript" src="js/table.js"></script>
<script type="text/javascript" src="js/util.js"></script>

<script type="text/javascript" language="javascript">
    var accordion;
    var onloads = new Array();
    //onloads.push( accord ); function accord() {accordion = new Rico.Accordion( 'accordionDiv', {panelHeight:350, onHideTab:changeAccordionTab} ); }
    onloads.push( accord ); function accord() {accordion = new Rico.Accordion( 'accordionDiv', {panelHeight:350} ); }
    var securitytoken = "<%=securitytoken%>";
    var applicationcontext = "<%=applicationcontext%>";
    var context = "<%=context%>";
    
    var borderFocusColor = "GoldenRod";
    var labelFocusColor = "DarkGoldenRod";
    var selectedCaptionColor = 'PaleGoldenRod';
    
    function init()
    { 
        getCarriers();
        
        for ( var i = 0 ; i < onloads.length ; i++ ) {
          onloads[i]();
        }
    }
    
    function unload() {
    }
    
    function disableControls(disabledSwitch) {
        document.getElementById("searchZipButton").disabled = disabledSwitch;
        document.getElementById("carrierCombo").disabled = disabledSwitch;
        document.getElementById("removeBlockButton").disabled = disabledSwitch;
        document.getElementById("addZipTextarea").disabled = disabledSwitch;
        document.getElementById("addBlockButton").disabled = disabledSwitch;
    }
    
    function getBlockedZips() {
        var zipCode = trimString(document.getElementById('searchZipId').value);
        
        if( zipCode.length < 2 ) {
          alert("Enter a zip code which has at least two characters");
          return;
        }
        disableControls(true);
        
        //XMLHttpRequest
        var newRequest = 
            new FTD_AJAX.Request('submitDeliveryZip.do',FTD_AJAX_REQUEST_TYPE_POST,getBlockedZipsCallback,false);
        newRequest.addParam("<%=DeliveryZipMaintConstants.REQUEST_TYPE%>","<%=DeliveryZipMaintConstants.REQUEST_TYPE_GET_BLOCKED_ZIPS%>");
        newRequest.addParam("<%=DeliveryZipMaintConstants.PARAMETER_ZIP_CODE%>",zipCode);
        newRequest.send();
    }
    
    function getBlockedZipsCallback(data) {
//        alert(data);
        disableControls(false);
        
        var xmlDoc = parseXmlString(data);
        var xmlRoot = xmlDoc.documentElement;
        
        //Clear out the input parms table (dont't delete the header row)
        var blockedTable = document.getElementById('blockedZipsId');
        var rows = blockedTable.rows;
        for( var i = rows.length-1; i>=0; i-- ) {
            blockedTable.deleteRow(i);
        }
        
        var records = xmlRoot.getElementsByTagName("<%=PDBXMLTags.TAG_BLOCKED_ZIP%>");
        
        for( var i = 0; i < records.length; i++) {
            var carrierId = records[i].getAttributeNode('<%=PDBXMLTags.TAG_CARRIER_ID%>').firstChild.nodeValue;
            var zipCode = records[i].getAttributeNode('<%=PDBXMLTags.TAG_ZIP_CODE%>').firstChild.nodeValue;
            var row = blockedTable.insertRow(blockedTable.rows.length);
            
            if( i % 2 != 0 ) {
                row.className = "alternate";
            }
                  
            input = new FTD_DOM.InputElement( {
                id:'<%=DeliveryZipMaintConstants.PARAMETER_CHECKBOX_ID%>'+i,
                type:'checkbox', 
                name:'<%=DeliveryZipMaintConstants.PARAMETER_CHECKBOX_ID%>', 
                <%=DeliveryZipMaintConstants.PARAMETER_INDEX%>:i} ).createNode();
            var cell = row.insertCell(0);
            cell.appendChild(input);
            cell.setAttribute("align","center");
            
            cell = row.insertCell(1);
            cell.setAttribute("id","<%=DeliveryZipMaintConstants.PARAMETER_SEARCH_CARRIER_ID%>"+i);
            cell.setAttribute("align","center");
            cell.innerHTML = carrierId;
            
            cell = row.insertCell(2);
            cell.setAttribute("id","<%=DeliveryZipMaintConstants.PARAMETER_SEARCH_ZIP_CODE%>"+i);
            cell.setAttribute("align","center");
            cell.innerHTML = zipCode;
        }
    }
    
    function removeBlockedZips() {
        var emailAddress = trimString(document.getElementById("emailAddress").value);
        
        if( emailAddress.length > 0 && !isValidEmail(emailAddress) ) {
            alert(emailAddress+' does not appear to be a valid email address!');
            return;
        }
        
        var boxes = document.getElementsByName('<%=DeliveryZipMaintConstants.PARAMETER_CHECKBOX_ID%>');
        
        var xmlDoc = createXmlDocument("<%=PDBXMLTags.TAG_BLOCKED_ZIPS%>");
        var xmlRoot = xmlDoc.documentElement;
        
        for( var i = 0; i<boxes.length; i++) {
            if( boxes[i].checked == true ) {
                var record = xmlDoc.createElement("<%=PDBXMLTags.TAG_BLOCKED_ZIP%>");
                var idx = record.<%=DeliveryZipMaintConstants.PARAMETER_INDEX%>;
                record.setAttribute("<%=PDBXMLTags.TAG_CARRIER_ID%>",document.getElementById("<%=DeliveryZipMaintConstants.PARAMETER_SEARCH_CARRIER_ID%>"+i).firstChild.nodeValue);
                record.setAttribute("<%=PDBXMLTags.TAG_ZIP_CODE%>",document.getElementById("<%=DeliveryZipMaintConstants.PARAMETER_SEARCH_ZIP_CODE%>"+i).firstChild.nodeValue);
                xmlRoot.appendChild(record);
            }
        }
        
//        alert(getXMLNodeSerialization(xmlDoc));
        
        disableControls(true);
        
        //XMLHttpRequest
        var newRequest = 
            new FTD_AJAX.Request('submitDeliveryZip.do',FTD_AJAX_REQUEST_TYPE_POST,removeBlockedZipsCallback,false);
        newRequest.addParam("<%=DeliveryZipMaintConstants.REQUEST_TYPE%>","<%=DeliveryZipMaintConstants.REQUEST_TYPE_REMOVE_BLOCKED_ZIPS%>");
        newRequest.addParam("<%=DeliveryZipMaintConstants.PARAMETER_BLOCKED_ZIPS%>",getXMLNodeSerialization(xmlDoc));
        newRequest.addParam("<%=DeliveryZipMaintConstants.PARAMETER_EMAIL_ADDRESS%>",emailAddress);
        newRequest.send();
    }
    
    function removeBlockedZipsCallback(data) {
        alert(data);
        disableControls(false);
    }
    
    function addBlockedZips() {
        var emailAddress = trimString(document.getElementById("emailAddress").value);
        
        if( emailAddress.length > 0 && !isValidEmail(emailAddress) ) {
            alert(emailAddress+' does not appear to be a valid email address!');
            return;
        }
        
        var zipCodes = trimString(document.getElementById('addZipTextarea').value);
        
        if( zipCodes.length == 0 ) {
          alert("You must enter at least one zip code.");
          return;
        }
        disableControls(true);
        
        var carrierCombo = document.getElementById("carrierCombo");
        var carrierId = carrierCombo[carrierCombo.selectedIndex].value;
        
        //XMLHttpRequest
        var newRequest = 
            new FTD_AJAX.Request('submitDeliveryZip.do',FTD_AJAX_REQUEST_TYPE_POST,addBlockedZipsCallback,false);
        newRequest.addParam("<%=DeliveryZipMaintConstants.REQUEST_TYPE%>","<%=DeliveryZipMaintConstants.REQUEST_TYPE_ADD_BLOCKED_ZIPS%>");
        newRequest.addParam("<%=DeliveryZipMaintConstants.PARAMETER_BLOCKED_ZIPS%>",zipCodes);
        newRequest.addParam("<%=DeliveryZipMaintConstants.PARAMETER_CARRIER_ID%>",carrierId);
        newRequest.addParam("<%=DeliveryZipMaintConstants.PARAMETER_EMAIL_ADDRESS%>",emailAddress);
        newRequest.send();
    }
    
    function addBlockedZipsCallback(data) {
        alert(data);
        disableControls(false);
    }
    
    function getCarriers() {
        disableControls(true);
        
        //XMLHttpRequest
        var newRequest = 
            new FTD_AJAX.Request('submitDeliveryZip.do',FTD_AJAX_REQUEST_TYPE_POST,getCarriersCallback,false);
        newRequest.addParam("<%=DeliveryZipMaintConstants.REQUEST_TYPE%>","<%=DeliveryZipMaintConstants.REQUEST_TYPE_GET_CARRIERS%>");
        newRequest.send();
    }
    
    function getCarriersCallback(data) {
        //alert(data);
        disableControls(false);
        
        var xmlDoc = parseXmlString(data);
        
        var carrierCombo = document.getElementById("carrierCombo");
        var carriers = xmlDoc.getElementsByTagName("<%=PDBXMLTags.TAG_CARRIER%>");
        
        //Remove the old values
        for( var i = carrierCombo.length-1; i>=0; i-- ) {
            carrierCombo.remove(i);
        }
        
        //iterate through nodes to populate group select
        for( var i = 0; i < carriers.length; i++)
        {
            var carrierId = carriers[i].getAttributeNode('<%=PDBXMLTags.TAG_ID%>').firstChild.nodeValue;
            var desc = carriers[i].getAttributeNode('<%=PDBXMLTags.TAG_NAME%>').firstChild.nodeValue;
            
            carrierCombo.options[i] = new Option(desc, carrierId);
          
            if( i == 0 ) {
                carrierCombo.options[0].selected=true;
            }
        }
    }
    
    function onSelectAllSearch() {
        var checkbox = document.getElementById('selectAllCheckbox');
        var bOnOff = checkbox.checked;
        
        var boxes = document.getElementsByName('<%=DeliveryZipMaintConstants.PARAMETER_CHECKBOX_ID%>');
        
        for( var i = 0; i<boxes.length; i++) {
            boxes[i].checked = bOnOff;
        }
    }
    
    function isValidEmail(str) {
      return (str.indexOf(".") > 2) && (str.indexOf("@") > 0);
    }
    
    onloads.push(addFTDDOMEvents);

    function addFTDDOMEvents() {
        new FTD_DOM.FocusElement('carrierCombo', borderFocusColor, 'carrierComboLabel', labelFocusColor);
        new FTD_DOM.FocusElement('searchZipId', borderFocusColor, 'searchZipIdLabel', labelFocusColor);
        new FTD_DOM.FocusElement('emailAddress', borderFocusColor, 'emailAddressLabel', labelFocusColor);
        new FTD_DOM.FocusElement('addZipTextarea', borderFocusColor, 'addZipTextareaLabel', labelFocusColor);
    }
</script>


<form action="/submitDeliveryZip">
    <jsp:include page="/includes/security.jsp"/>
    <a name="top"></a>
    <table width="400px" align="center">
        <caption/>
        <thead/>
        <tbody>
            <tr>
                <td>
                    <div align="center">
                        <table border="0" cellpadding="2" cellspacing="0">
                            <caption/>
                            <thead/>
                            <tbody>
                                <tr>
                                    <td class="Label">
                                        <label id="emailAddressLabel" for="emailAddress">
                                            Email edit results to
                                        </label>
                                    </td>
                                    <td>
                                        <input type="text" name="emailAddress" size="40"
                                               id="emailAddress"
                                               title="Detailed results will be mailed to this email address"/>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div id="accordionDiv" style="border-top: 1px solid; margin-top: 6px" align="center">
                        <div id="div_search_panel">
                            <div id="div_search_header" class="accordionTabTitleBar" align="left">Zip Code Search</div>
                            <div id="div_search_context">
                                    <div align="center">
                                        <table border="0" cellpadding="2" cellspacing="0">
                                            <caption/>
                                            <thead/>
                                            <tbody>
                                                <tr>
                                                    <td class="Label">
                                                        <label id="searchZipIdLabel" for="searchZipId">Zip</label></td>
                                                    <td>
                                                    <td>
                                                        <input id="searchZipId" 
                                                             type="text" 
                                                             size="10" 
                                                             maxlength="5" 
                                                             title="Enter at least the first two characters of the zip code" >
                                                    </td>
                                                    <td>&nbsp;</td>
                                                    <td>
                                                        <button type="button" id='searchZipButton' onclick='getBlockedZips();'>Search</button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <table border="1" cellpadding="0" cellspacing="0">
                                      <caption/>
                                      <thead/>
                                      <tbody>
                                          <tr>
                                              <td valign="top">
                                                <div id="searchZipsDiv" style="overflow: auto; height: 290px;">
                                                    <table 
                                                        class="dztable sort01 table-autostripe table-autosort:2 table-stripeclass:alternate table-rowshade-alternate" 
                                                        border="0" 
                                                        cellpadding="2" 
                                                        cellspacing="0">
                                                      <caption>Blocked Zips</caption>
                                                      <thead>
                                                          <tr>
                                                              <th>
                                                                <input type="checkbox"
                                                                     class="table-sortable:default"
                                                                     name="selectAllCheckbox"
                                                                     id="selectAllCheckbox"
                                                                     onclick="onSelectAllSearch();"/>
                                                              </th>
                                                              <th width="125px" 
                                                                  align="center" 
                                                                  class="table-sortable:default">Carrier</th>
                                                              <th width="125px" 
                                                                  align="center"
                                                                  class="table-sortable:default">Zip Code</th>
                                                          </tr>
                                                      </thead>
                                                      <tbody id="blockedZipsId" />
                                                   </table>
                                                 </div>
                                              </td>
                                          </tr>
                                      </tbody>
                                </table>
                                <div align="center">
                                    <table border="0" cellpadding="2" cellspacing="0">
                                        <caption/>
                                        <thead/>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <button type="button" id='removeBlockButton' onclick='removeBlockedZips();'>Remove Block</button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        
                        <div id="div_edit_panel">
                            <div id="div_edit_header" class="accordionTabTitleBar" align="left">Zip Code Blocking</div>
                            <div id="div_edit_context">
                                <div align="center">
                                    <table border="0" cellpadding="2" cellspacing="0">
                                        <caption/>
                                        <thead/>
                                        <tbody>
                                            <tr>
                                                <td class="Label">
                                                    <label id="carrierComboLabel" for="carrierCombo">Carrier</label></td>
                                                <td>
                                                <td>
                                                    <select id="carrierCombo">
                                                      <option value="LOADING">Loading...</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Label" valign="top">
                                                    <label id="addZipTextareaLabel" for="addZipTextarea">Zip Codes</label></td>
                                                <td>
                                                <td>
                                                    <textarea name="addZipTextarea" cols="10" rows="20"
                                                              id="addZipTextarea"
                                                              title="One zip code per line"></textarea>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table border="0" cellpadding="2" cellspacing="0">
                                        <caption/>
                                        <thead/>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <button type="button" id='addBlockButton' onclick='addBlockedZips();'>Add Block</button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
<!--
    <table align="center" >
        <caption/>
        <thead/>
        <tbody>
            <tr><td><button type="button" value="Exit" onclick="doMainMenuAction();">Exit</button></td></tr>
        </tbody>
    </table>
-->
</form>