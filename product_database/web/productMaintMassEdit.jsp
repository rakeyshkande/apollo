<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
  String securitytoken = (String)request.getParameter("securitytoken");
  String context = (String)request.getParameter("context");
  String adminAction = (String)request.getParameter("adminAction");
  if (adminAction == null) 
	  adminAction = (String) request.getAttribute("adminAction");
  String pdbMassEditFileDirectory = (String) request.getAttribute("pdbMassEditFileDirectory");
  String rowColor="#CCCCCC";
  int rowCount = 0;
  
%>
 <style type="text/css">
      .MassEditSectionHeader {
	      font-family: arial,helvetica,sans-serif;
	      font-size: 11pt;
	      font-style: normal;
	      font-weight: bolder;
	      background-color: #FFFFFF;
	      text-align: left;
	      color: #000099;
       }
       .greenItalicFont
	   {
	       font-family: arial,helvetica,sans-serif;
	       font-size: 8pt;
	       font-style:italic;
	       font-weight: bold;
	       color: green;
	       TEXT-ALIGN: left;
        }
      .massEditLabelPad {
    	font-family: arial,helvetica,sans-serif;
    	font-size: 8pt;
    	font-weight: bold;
   		padding-left: 1px;
   		text-align:left;
	  }
	  .error {
	    background-color: #ff0000;
	  }
    </style>
<script type="text/javascript" src="js/prototype.js"></script>
<script type="text/javascript" src="js/rico.js"></script>
<script type="text/javascript" src="js/ftddom.js"></script>
<script type = "text/javascript" src = "js/ftdajax.js"></script>
<script language="JavaScript" type="text/javascript">

function init() { 
}

function unload() {
}

function doSearch() {
	// don't let them submit a search on an empty form
    if ( isEmpty(document.productMaintMassEditForm.masterSku1.value) &&
		isEmpty(document.productMaintMassEditForm.masterSku2.value) &&
		isEmpty(document.productMaintMassEditForm.masterSku3.value) &&
		isEmpty(document.productMaintMassEditForm.masterSku4.value) &&
	    isEmpty(document.productMaintMassEditForm.masterSku5.value) && 
	    isEmpty(document.productMaintMassEditForm.product1.value) &&
		isEmpty(document.productMaintMassEditForm.product2.value) &&
		isEmpty(document.productMaintMassEditForm.product3.value) &&
		isEmpty(document.productMaintMassEditForm.product4.value) &&
        isEmpty(document.productMaintMassEditForm.product5.value) ) {
        alert("Please make a selection");
    }
    else {
        // wipe out all errors on the form 
        // This only needs to be done for the search because when the
        // spreadsheet is produced it does not have a forwarding action since it's
        // used the output stream to write out the spreadsheet.
        var errorList = document.getElementsByClassName("error");
        for (var i=0; i < errorList.length; i++ )
        	errorList[i].style.backgroundColor = "white";  
        var errorMsgList = document.getElementsByClassName("Error");
        for (var i=0; i < errorMsgList.length; i++ )
        	errorMsgList[i].innerHTML = "";
    	// submit the form
		document.productMaintMassEditForm.formAction.value = "search";
		//document.productMaintMassEditForm.submit();
		submitForm('/pdb/submitProductMaintMassEdit.do?securitytoken=' + securitytoken + '&context=' + context + '&adminAction=' + adminAction);
		alert("Your request has been received and is being processed."); 
		
	}	
}

function doUpload() 
{
	    document.productMaintMassEditForm.formAction.value = "upload";
	 
	    var errorMessage = "Invalid file name. File name must be in the following format 'PDBMassEdit_MMDDYYYY_HHMM.xslx' where today's date is in MMDDYYYY format and HHMM is the current hour and minutes in military time.";
	    var fileName = document.forms[0].pdbMassEditFile.value;
	    if (fileName == "") {
	        alert("Please select a file to Upload.");
	        document.getElementById("pdbMassEditFile").focus();
	        return false;
	    }
	    var slash = '/' 
	    if (fileName.match(/\\/)) { 
	        slash = '\\' 
	    }
	    var tempName = fileName.substring(fileName.lastIndexOf(slash) + 1);
	    if (tempName.length == 0 ) {
	        alert("Invalid file name");
	        document.getElementById("pdbMassEditFile").focus();
	        return false;
	    }
	    var nameArray = new Array();
	    nameArray = tempName.split(".");
	    if (nameArray.length < 2) {
	        alert(errorMessage);
	        document.getElementById("pdbMassEditFile").focus();
	        return false;
	    }
	    if (nameArray[0].substr(0,12) != "PDBMassEdit_") {
	        alert(errorMessage);
	        document.getElementById("pdbMassEditFile").focus();
	        return false;
	    } else if (nameArray[1].toLowerCase() != "xlsx") {
	        alert("File name extension must be 'xlsx'");
	        document.getElementById("pdbMassEditFile").focus();
	        return false;
	    }
	    var now = new Date();
	    var month = now.getMonth()+1;
	    month = "0" + month;
	    var day = now.getDate();
	    day = "0" + day;
	    var year = now.getFullYear();
	    iLen = month.length;
	    month = month.substr(iLen-2, 2);
	    iLen = day.length;
	    day = day.substr(iLen-2, 2);	    
	    
	    var mmddyyyy = month + day + year ;
		var hrmin = nameArray[0].substr(20,25);
		var mmddyyyyhrmin = mmddyyyy;
		
		if(nameArray[0].substr(21,25) >= 0)
			mmddyyyyhrmin = mmddyyyy + hrmin;

	    if (nameArray[0].substr(12,25) != mmddyyyyhrmin) 
	    {
	        alert(errorMessage);
	        document.getElementById("pdbMassEditFile").focus();
	        return false;
	    } 
	    else if (nameArray[0].length != 25) {
	        alert(errorMessage);
	        document.getElementById("pdbMassEditFile").focus();
	        return false;
	    }
	 
	 
	 submitForm('/pdb/submitProductMaintMassEdit.do?securitytoken=' + securitytoken + '&context=' + context + '&adminAction=' + adminAction);
	 alert("Your request has been received and is being processed."); 
}

function doRefresh() {
	disableEnableMasterSkus(false);
    disableEnableProductIds(false);
    document.productMaintMassEditForm.masterSku1.value = "";
    document.productMaintMassEditForm.masterSku2.value = "";
    document.productMaintMassEditForm.masterSku3.value = "";
    document.productMaintMassEditForm.masterSku4.value = "";
    document.productMaintMassEditForm.masterSku5.value = "";
    document.productMaintMassEditForm.product1.value = "";
    document.productMaintMassEditForm.product2.value = "";
    document.productMaintMassEditForm.product3.value = "";
    document.productMaintMassEditForm.product4.value = "";
    document.productMaintMassEditForm.product5.value = "";
	document.productMaintMassEditForm.formAction.value = "refresh";
	submitForm('/pdb/submitProductMaintMassEdit.do?securitytoken=' + securitytoken + '&context=' + context + '&adminAction=' + adminAction);
}

function doShowErrorReport(fileName) {
	 document.productMaintMassEditForm.formAction.value = "showErrorReport";
	 document.productMaintMassEditForm.fileName.value = fileName;
	 submitForm('/pdb/submitProductMaintMassEdit.do?securitytoken=' + securitytoken + '&context=' + context + '&adminAction=' + adminAction);
     return false;
}

function disableEnableMasterSkus(value) {
    // if true, form elements will be disabled - if false, form elements will be enabled
    if (isEmpty(document.productMaintMassEditForm.product1.value) &&
        isEmpty(document.productMaintMassEditForm.product2.value) &&
        isEmpty(document.productMaintMassEditForm.product3.value) &&
        isEmpty(document.productMaintMassEditForm.product4.value) &&
        isEmpty(document.productMaintMassEditForm.product5.value) )
        return;
    document.productMaintMassEditForm.masterSku1.disabled = value;
    document.productMaintMassEditForm.masterSku2.disabled = value;
    document.productMaintMassEditForm.masterSku3.disabled = value;
    document.productMaintMassEditForm.masterSku4.disabled = value;
    document.productMaintMassEditForm.masterSku5.disabled = value;
}

function disableEnableProductIds(value) {
    // if true, form elements will be disabled - if false, form elements will be enabled
	if (isEmpty(document.productMaintMassEditForm.masterSku1.value) &&
	    isEmpty(document.productMaintMassEditForm.masterSku2.value) &&
	    isEmpty(document.productMaintMassEditForm.masterSku3.value) &&
	    isEmpty(document.productMaintMassEditForm.masterSku4.value) &&
	    isEmpty(document.productMaintMassEditForm.masterSku5.value) )
        return;
    document.productMaintMassEditForm.product1.disabled = value;
    document.productMaintMassEditForm.product2.disabled = value;
    document.productMaintMassEditForm.product3.disabled = value;
    document.productMaintMassEditForm.product4.disabled = value;
    document.productMaintMassEditForm.product5.disabled = value;
}


function resetForm() {
    disableEnableMasterSkus(false);
    disableEnableProductIds(false);
    document.productMaintMassEditForm.masterSku1.value = "";
    document.productMaintMassEditForm.masterSku2.value = "";
    document.productMaintMassEditForm.masterSku3.value = "";
    document.productMaintMassEditForm.masterSku4.value = "";
    document.productMaintMassEditForm.masterSku5.value = "";
    document.productMaintMassEditForm.product1.value = "";
    document.productMaintMassEditForm.product2.value = "";
    document.productMaintMassEditForm.product3.value = "";
    document.productMaintMassEditForm.product4.value = "";
    document.productMaintMassEditForm.product5.value = "";
    document.productMaintMassEditForm.formAction.value = "reset";
    submitForm('/pdb/submitProductMaintMassEdit.do?securitytoken=' + securitytoken + '&context=' + context + '&adminAction=' + adminAction);
	
}

function isEmpty(str) {
 	if(str && str!='')
		return false;
	else
		return true;
}

</script>


<html:form action="/submitProductMaintMassEdit" method="POST" enctype="multipart/form-data">
<div align="center">
<html:errors property="global"/>
<input type="hidden" name="formAction" value=""/>
<input type="hidden" name="fileName" value=""/>
<input type="hidden" name="securitytoken" value="<%=securitytoken%>" id="securitytoken"/>
<input type="hidden" name="context" value="<%=context%>" id="context"/>
<input type="hidden" name="adminAction" value="<%=adminAction%>" id="adminAction"/>
   
<div style="display:block" id="content">
 <table class="mainTable" align="center" cellpadding="3" cellspacing="3" width="98%">
	<tbody>
	  <!--- File download -->
		<tr>
			<td>
				<table id="fileDownload" align="center" cellpadding="3" cellspacing="3" width="98%">
					<tbody>
     					<tr>
     						<td class="MassEditSectionHeader">MASS EDIT PRODUCT SEARCH</td>
     					</tr>
     					<tr>
     					    <td>
     							<table align="left" border="0">
							     <tbody>
									<tr>
									  <td colspan="7">&nbsp;</td>
									</tr>
									<tr>
									  <td class="massEditLabelPad">Master SKU(s) :</td>
									  <td align="left" valign="top">
										<html:text property="masterSku1" size="10" maxlength="10" errorStyleClass="error" errorKey="org.apache.struts.action.ERROR" onblur="disableEnableProductIds(true);"/>
									    <html:errors property="masterSku1" />
									  </td>
									  <td align="left" valign="top">
										<html:text property="masterSku2" size="10" maxlength="10" errorStyleClass="error" errorKey="org.apache.struts.action.ERROR" onblur="disableEnableProductIds(true);"/>
									    <html:errors property="masterSku2" />
									  </td>
									  <td align="left" valign="top">
										<html:text property="masterSku3" size="10" maxlength="10" errorStyleClass="error" errorKey="org.apache.struts.action.ERROR" onblur="disableEnableProductIds(true);"/>
									    <html:errors property="masterSku3" />
									  </td>
									  <td align="left" valign="top">
										<html:text property="masterSku4" size="10" maxlength="10" errorStyleClass="error" errorKey="org.apache.struts.action.ERROR" onblur="disableEnableProductIds(true);"/>
									    <html:errors property="masterSku4" />
									  </td>
									  <td align="left" valign="top">
										<html:text property="masterSku5" size="10" maxlength="10" errorStyleClass="error" errorKey="org.apache.struts.action.ERROR" onblur="disableEnableProductIds(true);"/>
									    <html:errors property="masterSku5" />
									  </td>
									  <td class="greenItalicFont">Edit by Master SKU(s)</td>
							    	</tr>
							    	<tr>
							    	  <td colspan="7"></br></td>
							    	</tr>
									<tr>
									  <td class="massEditLabelPad">Product ID(s) :</td>
									  <td align="left" valign="top">
										<html:text property="product1" size="10" maxlength="10" errorStyleClass="error" errorKey="org.apache.struts.action.ERROR" onblur="disableEnableMasterSkus(true);"/>
									    <html:errors property="product1" />
									  </td>
									  <td align="left" valign="top">
										<html:text property="product2" size="10" maxlength="10" errorStyleClass="error" errorKey="org.apache.struts.action.ERROR" onblur="disableEnableMasterSkus(true);"/>
									    <html:errors property="product2" />
									  </td>
									  <td align="left" valign="top">
										<html:text property="product3" size="10" maxlength="10" errorStyleClass="error" errorKey="org.apache.struts.action.ERROR" onblur="disableEnableMasterSkus(true);"/>
									    <html:errors property="product3" />
									  </td>
									  <td align="left" valign="top">
										<html:text property="product4" size="10" maxlength="10" errorStyleClass="error" errorKey="org.apache.struts.action.ERROR" onblur="disableEnableMasterSkus(true);"/>
									    <html:errors property="product4" />
									  </td>
									  <td align="left" valign="top">
										<html:text property="product5" size="10" maxlength="10" errorStyleClass="error" errorKey="org.apache.struts.action.ERROR" onblur="disableEnableMasterSkus(true);"/>
									    <html:errors property="product5" />
									  </td>
									  <td class="greenItalicFont">Edit by Product ID(s)</td>
							    	</tr>
							    	<tr>
							    	  <td colspan="7"></br></td> 
							    	</tr>
							    	    	
							    	<tr>
							    	  
									   <td  align="left" colspan="6" valign="top">
									  	 <button onclick="doSearch();"  class="BlueButton" id="searchButton" type="button">Search</button>
									  	 <button  class="BlueButton" id="clear" type="button" onclick="resetForm();">Clear all Fields</button>
									   </td>
									   <td></td>
									</tr>
									
							     </tbody>
							    </table>
							</td>
						</tr>
						<tr>
						  <td><br/></td>
					 	</tr>
				   	</tbody>
    			</table>
    		</td>
    	</tr>
     <!-- end File download -->

     <!-- File Upload -->
    	<tr>
    		<td>
    			<table id="fileUpload" align="center" cellpadding="3" cellspacing="3" width="98%">
					<tbody>
	     				<tr>
	     					<td class="MassEditSectionHeader">MASS EDIT FILE UPLOAD</td>
						</tr>
						<tr>
						    <td>
 								<table  align="left" border="0" cellpadding="5" cellspacing="5" width="65%">
     							  <tbody>
     							    <tr>
     							       <td align="left"  class="massEditLabelPad">Input File     <font color = "green">(PDBMassEdit_MMDDYYYY_HHMM.xslx)</font></td>
     							    </tr>
     					 			<tr>
					    				<td class="labeload"  width="100%" align="left" valign="top">
						  					<input type="file" size="50" name="pdbMassEditFile" id="pdbMassEditFile"/>
					    				</td>
					    			<tr>
					    			    <td align="left">
					    					<button type="button" id="buttonUpload" class="BlueButton" onclick="doUpload();">Upload</button>
										</td>
					    			</tr>
					 				<tr>
					 					<td><br/></td>
					 				</tr>

					 			  </tbody>
					 			</table>


						    </td>
						</tr>
				    </tbody>
    			</table>
    		</td>
    	</tr>
    <!-- end File upload -->

     <!-- File Upload Status -->
	    	<tr>
	    		<td>
	    			<table id="fileUploadStatus" align="center" cellpadding="3" cellspacing="3" width="98%">
						<tbody>
		     				<tr>
		     					<td class="MassEditSectionHeader">MASS EDIT FILE UPLOAD STATUS</td>
							</tr>
							 
							<tr>
							    <td align="left">
	 								<table  align="left" border="0" cellpadding="5" cellspacing="5" width="65%">
	     							  <tbody>
	     							    <tr>
	     							      <td align="left">
							    			<button type="button" id="buttonRefresh" class="BlueButton" onclick="doRefresh();">Refresh</button>
										  </td>
	     							    </tr>
	     							    <tr>
										  <td>
										    <table class="innerTable" cellpadding="3" cellspacing="3" width="98%">
										 		<tbody>
													<tr class="TblHeader">
													  <th align="left">User</th>
													  <th align="left">File Upload Date</th>
													  <th align="left">Upload File</th>
													  <th align="left">Status</th>
													  <th align="left">Total Records</th>
													  <th align="left">Successful Records</th>
													  <th align="left">Failure Records</th>
													</tr>
											   <logic:iterate name="pdbMassEditFileStatusList" id="pdbListId">
 													<tr bgcolor="<%=rowColor%>">
									  				  <td align="left"><bean:write name="pdbListId" property="user"/></td>
									 				  <td align="left"><bean:write name="pdbListId" property="fileUploadDate"/></td>
									  				  <td align="left"><bean:write name="pdbListId" property="fileName"/></td>
									  				  <td align="left"><bean:write name="pdbListId" property="status"/></td>
									  				  <td align="right"><bean:write name="pdbListId" property="totalCount"/></td>
									 				  <td align="right"><bean:write name="pdbListId" property="successRecordsCount"/></td>
									 				  <c:choose>
     												   <c:when test="${pdbListId.failedRecordsCount == 0}">
									 				    <td align="right">
									 				     <bean:write name="pdbListId" property="failedRecordsCount"/> 
									 				     </td>
									 				  </c:when>
                                                      <c:otherwise>
   														<td align="right"><a href="#" onClick="doShowErrorReport('<bean:write name="pdbListId" property="errorFileName"/>');">
									 				       <bean:write name="pdbListId" property="failedRecordsCount"/></a>
									 				     </td>
									 				  </c:otherwise>  
									 				  </c:choose> 
													</tr>
													<% rowColor = (rowCount % 2 == 0) ? "#FFFFFF" : "#CCCCCC";
													   rowCount++;
													%>
											  </logic:iterate>
										 	  </tbody>
											</table>
										  </td>
									   </tr>
									   <tr>
									      <td align="left">
									    	<button type="button" id="buttonRefresh" class="BlueButton" onclick="doRefresh();">Refresh</button>
									  	  </td>
					    			   </tr>
								
						 			  </tbody>
						 			</table>


							    </td>
							</tr>
					    </tbody>
	    			</table>
	    		</td>
	    	</tr>
    <!-- end File upload status -->

   </tbody>
  </table>
 </div>
</div>   
</html:form>