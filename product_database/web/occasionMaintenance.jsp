<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="form" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>


<SCRIPT SRC="js/Listbox.js"></SCRIPT>
<SCRIPT language="javascript">

function init()
{
}


  function processInput(list,submitTo){
     if (changeFlag)
       {
       for (i=0;i<list.length; i++) 
          {
          list.options[i].selected=true;
          }
        }
    submitForm(submitTo);
      }


   function selectionchanged(list,SubmitTo,hiddenfield){

    if (changeFlag)
        {
        if (confirm("Would you like to save changes?  Press OK to save changes or CANCEL to continue without saving."))
            { 
            processInput(list, SubmitTo); 
            }
        else
            { 
	    hiddenfield.value = 0;
            submitForm(SubmitTo);
            }
        }
        else
            { 
	    hiddenfield.value =0;
            submitForm(SubmitTo);
            }
    }


</SCRIPT>

<html:form action="/updateOccasionMaintenance"  focus="occasionID">
<jsp:include page="includes/security.jsp"/>
			<html:hidden property="prevOccasionID"  />

			<TABLE width="98%" border="0" cellpadding="2" cellspacing="2">
            	<TR>                	<TD width="50%" class="labelright"> Occasion: </TD>
                	<TD width="50%">
                  <html:select property="occasionID" onchange="selectionchanged(selectedID,'changeOccasionMaintenance.do',prevOccasionID);">
                    <html:options collection="occasionList" property="id" labelProperty="description"/>
                  </html:select>
                	</TD>
                </TR>

				<TR> <TD colspan="2"> &nbsp; </TD> </TR>

            	<TR>
                	<TD colspan="2" align="center">
						<TABLE width="60%" border="0"  cellpadding="0" cellspacing="0">
							<TR>
								<TD class="label" align="center"> Available Cards </TD>
								<TD> &nbsp; </TD>
								<TD class="label" align="center"> Selected Cards </TD>
							</TR>

							<TR>
								<TD align="center">
                  <html:select property="availableID" multiple="Y" size="10" style="width:210px">
                    <html:options collection="availableList" property="id" labelProperty="description"/>
                  </html:select>
								</TD>
								<TD align="center">
									<img src="images/arrowSingleRight.gif" onClick="javascript:btnMoveRight_onclick(availableID, selectedID);"> <br>
									<img src="images/arrowDoubleRight.gif" onClick="javascript:btnMoveAllRight_onclick(availableID, selectedID);"> <br>
									<img src="images/arrowDoubleLeft.gif" onClick="javascript:btnMoveAllLeft_onclick(selectedID,availableID);"> <br>
									<img src="images/arrowSingleLeft.gif" onClick="javascript:btnMoveLeft_onclick(selectedID,availableID);">
								</TD>
								<TD align="center">
                  <html:select property="selectedID" multiple="Y" size="10" style="width:210px">
                    <html:options collection="selectedList" property="id" labelProperty="description"/>
                  </html:select>
								</TD>
							</TR>
						</TABLE>
                	</TD>
                </TR>

				<TR> <TD colspan="2"> &nbsp; </TD> </TR>

				<TR>
					<TD colspan="2" align="center">
						<INPUT type="button" name="cancel" value="  Cancel  " onclick="submitForm('admin.do');">
             &nbsp;&nbsp;
						<INPUT type="button" name="update" value="  Update  " onclick="processInput(selectedID,'updateOccasionMaintenance.do');">
					</TD>
				</TR>

				<TR> <TD colspan="2"> &nbsp; </TD> </TR>

				<TR>
					<TD colspan="2" align="center">&nbsp;</TD>
				</TR>
			</TABLE>

</html:form>
 
