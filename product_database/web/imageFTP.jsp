<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>

<%
	String securitytoken = (String)request.getParameter("securitytoken");
  String context = (String)request.getParameter("context");
%>


    <script language="javascript" type="text/javascript">




      var securityContext = '<%=securitytoken%>';
      var context = '<%=context%>';


        function submitFTPPage(){
            submitForm('submitImageFTP.do?securitytoken=' + securityContext + '&context=' + context);
        }

     </script>

<html:form action="/submitImageFTP.do" method="POST" enctype="multipart/form-data">
<jsp:include page="includes/security.jsp"/>

            <!-- Main Table -->
 <TABLE width="98%" border="0" cellpadding="2" cellspacing="2">
                <TR><TD colspan="2">&nbsp;</TD></TR>
                <logic:present name="instruction">
                <TR>
                    <TD colspan="2" class="instruction">
                    <bean:write name="instruction" ignore="true" scope="request" />
                    </TD>
                </TR>
                <TR><TD colspan="2">&nbsp;</TD></TR>
                </logic:present>
                <TR>
                    <TD width="15%" class="Header3"> Image Source </TD>
                    <TD width="85%"> &nbsp; </TD>
                </TR>
<%--
                <TR>
                    <TD colspan="2">
                        <APPLET CODE=FTPDropFrame.class CODEBASE="applet/">

                        </APPLET>
                    </TD>
                </TR>
--%>
				<TR>
                   <TD width="95%" class="label"> &nbsp;&nbsp; Image Path: &nbsp; <html:file name="imageFTPForm" property="imagePath1" size="60" maxlength="200" />  </TD>
               	</TR>
				<TR>
                   <TD width="95%" class="label"> &nbsp;&nbsp; Image Path: &nbsp; <html:file name="imageFTPForm" property="imagePath2" size="60" maxlength="200" />  </TD>
               	</TR>
				<TR>
                   <TD width="95%" class="label"> &nbsp;&nbsp; Image Path: &nbsp; <html:file name="imageFTPForm" property="imagePath3" size="60" maxlength="200" />  </TD>
               	</TR>
				<TR>
                   <TD width="95%" class="label"> &nbsp;&nbsp; Image Path: &nbsp; <html:file name="imageFTPForm" property="imagePath4" size="60" maxlength="200" />  </TD>
               	</TR>
				<TR>
                   <TD width="95%" class="label"> &nbsp;&nbsp; Image Path: &nbsp; <html:file name="imageFTPForm" property="imagePath5" size="60" maxlength="200" />  </TD>
               	</TR>
				<TR>
                   <TD width="95%" class="label"> &nbsp;&nbsp; Image Path: &nbsp; <html:file name="imageFTPForm" property="imagePath6" size="60" maxlength="200" />  </TD>
               	</TR>

				<TR>
					<TD> <hr> </TD>
					<TD>  </TD>
				</TR>

		<TR><TD colspan="2"> &nbsp; </TD></TR>
	<TR>
		<TD> <hr size=1> </TD>
		<TD colspan="2"> &nbsp; </TD>
	</TR>
	<TR>
		<TD colspan="2" class="Header3">
			Active Servers
		</TD>
	</TR>
	<TR>
		<TD colspan="1">
                <html:checkbox name="imageFTPForm" property="chkFTD2" /><bean:message key="imageFTPContent.ftd.name2" /><br>
                <div style="VISIBILITY: hidden;">
	                <html:checkbox name="imageFTPForm" property="chkTest2" /><bean:message key="imageFTPContent.test.name2" /><br>
	                <html:checkbox name="imageFTPForm" property="chkUAT2" /><bean:message key="imageFTPContent.uat.name2" /><br>
	                <html:checkbox name="imageFTPForm" property="chkContent2" /><bean:message key="imageFTPContent.content.name2" /><br>
                </div>
		</TD>
    </TR>
   	<TR>
		<TD colspan="3" align="center">
			<html:button property="btnFTPImage" value="Submit" onclick="submitFTPPage();" />
		</TD>
	</TR>
</TABLE>
</html:form>
