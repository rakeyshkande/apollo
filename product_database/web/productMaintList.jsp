<%@ page import="com.ftd.pdb.common.valobjs.ProductVO"%>
<%@ page import="com.ftd.pdb.common.valobjs.ProductBatchVO"%>
<%@ page import="com.ftd.pdb.common.ProductMaintConstants"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<script type="text/javascript" src="js/prototype.js"></script>
<script type="text/javascript" src="js/rico.js"></script>
<script type="text/javascript" src="js/ftddom.js"></script>
<script language="JavaScript" type="text/javascript">

var borderFocusColor = "GoldenRod";
var labelFocusColor = "DarkGoldenRod";
var onloads = new Array();

function init()
{              
    closepopup();
    document.linkColor = "#000000";
    document.vlinkColor = "#000000";
    for ( var i = 0 ; i < onloads.length ; i++ ){
        onloads[i]();
    }
    if (document.getElementById('productId') != null){
        document.getElementById('productId').focus();
    }
}

function unload() {
}

var checkflag = "false";
function batchCheckAll() 
{
  var productCount = document.productMaintListForm.prodCount.value;
  if (productCount != null && productCount > 0) {
    if (productCount > 1) {
      if (checkflag == "false") {
        for (i = 0; i < productCount; i++) {
          document.productMaintListForm.batchcheckbox[i].checked = true;
        }
        checkflag = "true";
        return "Uncheck All"; 
      } else {
        for (i = 0; i < productCount; i++) {
          document.productMaintListForm.batchcheckbox[i].checked = false; 
        }
        checkflag = "false";
        return "Check All"; 
      }
    } else {
      if (checkflag == "false") {
        document.productMaintListForm.batchcheckbox.checked = true;
        checkflag = "true";
        return "Uncheck All"; 
      } else {
        document.productMaintListForm.batchcheckbox.checked = false; 
        checkflag = "false";
        return "Check All"; 
      }
    }
  }
}

var closePupUp = false;
function popuponclick()
{
  my_window = window.open("","mywindow","width=350,height=150");
  my_window.document.write('<html><head><title>Status Window</title></head><body><H2>Processing...</H2><p><img src="images/example_please_wait.gif" alt="please wait"></p></body></html>');  
}

function closepopup()
{
  if (closePupUp) {
    popuponclick();
    if(false == my_window.closed)
    {
      my_window.close();
    }
  }
}

onloads.push( addFTDDOMEvents ); 
function addFTDDOMEvents() { 
    new FTD_DOM.FocusLabel( 'batchCheckAll', 'batchCheckAllLabel', labelFocusColor ); 
    new FTD_DOM.FocusElement( 'productId', borderFocusColor, 'productIdLabel', labelFocusColor );
    new FTD_DOM.FocusElement( 'pquadProductID', borderFocusColor, 'pquadproductIdLabel', labelFocusColor );
    new FTD_DOM.FocusElement( 'prodAvail', borderFocusColor, 'prodAvailLabel', labelFocusColor );
    new FTD_DOM.FocusElement( 'prodUnavail', borderFocusColor, 'prodUnavailLabel', labelFocusColor );
    new FTD_DOM.FocusElement( 'dateRestrict', borderFocusColor, 'dateRestrictLabel', labelFocusColor );
    new FTD_DOM.FocusElement( 'onHold', borderFocusColor, 'onHoldLabel', labelFocusColor );
}
</script>

<%
  String securitytoken = (String)request.getParameter("securitytoken");
  String context = (String)request.getParameter("context");
  String adminAction = (String)request.getParameter("adminAction");
  // Build the back link for product detail page - i.e., restore criteria used
  // in product list query
  String backArgs = "";
  if (request.getParameter("prodAvail") != null)    {backArgs += "&prodAvail=Y";}
  if (request.getParameter("prodUnavail") != null)  {backArgs += "&prodUnavail=Y";}
  if (request.getParameter("dateRestrict") != null) {backArgs += "&dateRestrict=Y";}
  if (request.getParameter("onHold") != null)       {backArgs += "&onHold=Y";}
  String closePupUp = (String) request.getAttribute(ProductMaintConstants.CLOSE_POPUP);
  if (closePupUp != null && closePupUp.equals("Y")) {
    %>
      <SCRIPT language="JavaScript" type="text/javascript">
         closePupUp = true;
      </SCRIPT>
    <%
  }
%>

<%
  boolean haveProductList = false;
  boolean havePquadId=false; 
  if (request.getAttribute("products1") != null) {
    haveProductList = true;
  }
%>

<%
if (haveProductList == false) {
//--------------------- PRODUCT MAINTENANCE FORM SECTION ----------------------
//
%>

<html:form method="POST" action="/submitProductMaintList">
<jsp:include page="includes/security.jsp"/>
<input type="hidden" name="submitType" value="edit">
<div align="center">
<html:errors property="global"/>
<table border="0" cellpadding="4" cellspacing="4">
    <tr>
      <td class="TblHeader">
      Edit Specific Product
      </td>
      <td>&nbsp;</td>
      <td class="TblHeader">
      Product List
      </td>
    </tr>
    <tr>
    <td align="center">
      <table border="0" cellpadding="2" cellspacing="2">
        <tr>
            <td colspan="3">
                <!-- Did not use html:errors since the messages are created 
                within the action as opposed to the errors which are created
                within the ActionForm.validate method
                -->
                <span class="Error">
                    <html:messages id="msg" message="true" property="ProductId">
                        <bean:write name="msg"/>
                    </html:messages>
                </span>
            </td>
        </tr>
        <tr>
          <td class="labelright" width="40%"><label id="productIdLabel" for="productId">Select Arrangement</label>
          </td>
          <td width="20%">
                  <html:text property="productId" styleId="productId" size="15" maxlength="10" onchange="javascript:this.value=this.value.toUpperCase();"/>
          </td>
          <td align="left" width="40%">
            <html:submit value="Edit" onclick="document.forms[0].submitType.value='edit'"/>
<!--                  <html:button property="btnEdit" value="EDIT" onclick="submitForm('submitProductMaintList.do');" /> -->
          </td>
        </tr>
        <!-- PQuad Product ID changes -->
        <tr>
          <td class="labelright" width="50%"><label id="pquadproductIdLabel" for=pquadProductID>PQuad Product ID</label><html:errors property="pquadProductID"/>
          </br><b><font color="red"><logic:notEmpty name="ErrorMsg"><bean:write name="ErrorMsg"/></logic:notEmpty></font></b>
          </td>
          <td width="20%">
                  <html:text property="pquadProductID" styleId="pquadProductID" size="15" maxlength="32" onchange="javascript:this.value=this.value.toUpperCase();"/>
          </td>
          <td align="left" width="40%">
            <html:submit value="Submit" onclick="document.forms[0].submitType.value='submit'"/>
          </td>
        </tr>
        
          <tr>
              <td colspan="3" align="center">
                <div style="display: none;">
                  <html:radio property="searchBy" value="searchByProdId"/>By Product ID
                  <html:radio property="searchBy" value="searchByNovatorId"/>By Novator ID
                </div>
              </td>
          </tr>
      </table>
    </td>
    <td>&nbsp;</td>
    <td>
      <table border="0" cellpadding="2" cellspacing="0">
      <tr><td class="TblText" align="left"><html:checkbox property="prodAvail" styleId="prodAvail" value="Y"/><label id="prodAvailLabel" for="prodAvail">Available products</label></td></tr>
      <tr><td class="TblText" align="left"><html:checkbox property="prodUnavail" styleId="prodUnavail" value="Y"/><label id="prodUnavailLabel" for="prodUnavail">Unavailable products</label></td></tr>
      <tr><td class="TblText" align="left"><html:checkbox property="dateRestrict" styleId="dateRestrict" value="Y"/><label id="dateRestrictLabel" for="dateRestrict">Available products with date restrictions</label></td></tr>
      <tr><td class="TblText" align="left"><html:checkbox property="onHold" styleId="onHold" value="Y"/><label id="onHoldLabel" for="onHold">Products on hold</label></td></tr>
      <tr><td>
        <!--<html:button property="btnList" value="Refresh List" onclick="document.forms[0].submitType.value='list';submitForm('submitProductMaintList.do');" />-->
        <html:submit value="Refresh List" onclick="document.forms[0].submitType.value='list'"/>
    </td></tr>
      </table>
    </td>
    </tr>
</table>
</div>
</html:form>


<%
} else {
//-------------------------- PRODUCT LIST SECTION -----------------------------
//
%>
<form action="">
<jsp:include page="includes/security.jsp"/>
<logic:empty name="pquadProductId">
<table cellpadding="5" cellspacing="2">
  <tr>
     <td class="HighlightAvail"><bean:message key="productmaintlist.availableproducts" /></td>
     <td class="HighlightUnavail"><bean:message key="productmaintlist.unavailableproducts" /></td>
     <td class="HighlightNone"><bean:message key="productmaintlist.availableproductsrestrict" /></td>
  </tr>
</table>
</logic:empty>

<logic:notEmpty name="pquadProductId">
<% havePquadId=true ; %>
<table cellpadding="5" cellspacing="2">
    <tr>
     <td class="TblText"><b><bean:message key="productmaintlist.PquadProductId" /><bean:write name="pquadProductId"/></b>
     </td>
    </tr>
</table>
</logic:notEmpty>
<table width="98%" border="0" cellpadding="2" cellspacing="2">
	<tr>
		<td class="TblHeader" align="center" colspan="3">
			Selection List
		</td>
	</tr>
    <tr>
        <td width="50%" valign="top">
            <table width="100%" border="0" cellpadding="0" cellspacing="2">
                <logic:iterate id="element" name="products1" scope="request">
                    <%
                        String highlight = null;
			String isHeld = "";
                        if(((ProductVO)element).getHoldUntilAvailable()) { isHeld = " [on hold] "; }			
                        if(((ProductVO)element).isStatus()) { highlight = "HighlightAvail"; }
                        else { highlight = "HighlightUnavail"; }
                        if(((ProductVO)element).isStatus() && (((ProductVO)element).getExceptionStartDate() != null || ((ProductVO)element).getExceptionEndDate() != null )) { highlight = "HighlightNone"; }
                        if(haveProductList && havePquadId)
                        	{ highlight = "HighlightNone";}
                    %>
                    <tr>
                    <td class="<%=highlight%>" valign="top"><a href="submitProductMaintList.do?productId=<%=((ProductVO)element).getProductId()%>&submitType=edit<%=backArgs%>&securitytoken=<%=securitytoken%>&context=<%=context%>&adminAction=<%=adminAction%>" style="text-decoration:none"><bean:write name="element" property="productId" /></a> (<bean:write name="element" property="novatorName" filter="false" />) <%=isHeld%></td>
                    </tr>
                </logic:iterate>
            </table>
        </td>
        <td width="50%" valign="top">
            <table width="100%" border="0" cellpadding="0" cellspacing="2">
                <logic:iterate id="element" name="products2" scope="request">
                    <%
                        String highlight = null;
			String isHeld = "";
                        if(((ProductVO)element).getHoldUntilAvailable()) { isHeld = " [on hold] "; }			
                        if(((ProductVO)element).isStatus()) { highlight = "HighlightAvail"; }
                        else { highlight = "HighlightUnavail"; }
                        if(((ProductVO)element).isStatus() && (((ProductVO)element).getExceptionStartDate() != null || ((ProductVO)element).getExceptionEndDate() != null )) { highlight = "HighlightNone"; }
                        if(haveProductList && havePquadId)
                    	{ highlight = "HighlightNone";  }
                    %>
                    <tr>
                    <td class="<%=highlight%>" valign="top"><a href="submitProductMaintList.do?productId=<%=((ProductVO)element).getProductId()%>&submitType=edit<%=backArgs%>&securitytoken=<%=securitytoken%>&context=<%=context%>&adminAction=<%=adminAction%>" style="text-decoration:none"><bean:write name="element" property="productId" /></a> (<bean:write name="element" property="novatorName" filter="false" />) <%=isHeld%></td>
                    </tr>
                </logic:iterate>
            </table>
        </td>
    </tr>
</table>
<form>
<%
}
%>
