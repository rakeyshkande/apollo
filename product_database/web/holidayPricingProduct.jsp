<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>


    <html:form action="/showHolidayPricingProduct"  focus="newPrice"  >
    <jsp:include page="includes/security.jsp"/>

<bean:define id="startDateBean" name="startDate" />
<bean:define id="endDateBean" name="endDate" />
    

			<html:hidden property="startDate"  />
			<html:hidden property="endDate" />
      

			<CENTER>

			<TABLE width="98%" border="0" cellpadding="2" cellspacing="2">
				<TR>
					<TD width="50%" valign="top">
						<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
							<TR>
								<TD width="50%" class="labelright"> Product: </TD>
								<TD width="50%"> C9-2985 </TD>
							</TR>

							<TR>
								<TD class="labelright"> Start Date: </td>
								<TD> <bean:write name="startDateBean" /> </TD>
							</TR>

							<TR>
								<TD class="labelright"> End Date: </td>
								<TD> <bean:write name="endDateBean" /> </TD>
							</TR>

							<TR>
								<TD class="labelright"> New Price: </TD>
								<TD> <html:text property="newPrice" size="10" maxlength="10"  />  </TD>
							</TR>

							<TR>
								<TD class="labelright"> New SKU: </td>
								<TD> <html:text property="newSKU" size="10" maxlength="10"  /> </TD>
							</TR>
						</TABLE>
					</TD>
					<TD width="50%" align="center" valign="top">
						<TABLE width="75%" border="1" cellpadding="2" cellspacing="2" bgcolor="#F0F0F0">
							<TR>
								<TD class="label" colspan="4" align="center"> Existing Price Range </TD>
							</TR>

							<TR>
								<TD width="25%" class="label" align="center"> SKU </TD>
								<TD width="25%" class="label" align="center"> Start Date </TD>
								<TD width="25%" class="label" align="center"> End Date </TD>
								<TD width="25%" class="label" align="center"> Price </TD>
							</TR>

							<TR>
								<TD align="center"> 123456789 </TD>
								<TD align="center"> 09/30/2002 </TD>
								<TD align="center"> 09/01/2002 </TD>
								<TD align="right"> $49.99 </TD>
							</TR>

							<TR>
								<TD class="labelright" colspan="3"> Regular Price: </TD>
								<TD align="right"> $64.99  </TD>
							</TR>
						</TABLE>
					</TD>
				</TR>

				<TR> <TD colspan="2"> &nbsp; </TD> </TR>

				<TR>
					<TD align="center">
						<INPUT type="button" name="btnCancel" value="  Cancel  " onclick="submitForm('admin.do')"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<INPUT type="submit" name="btnSave" value="  Save  ">
					</TD>
					<TD> &nbsp; </TD>
				</TR>
			</TABLE>
			</CENTER>
	 </html:form  >



