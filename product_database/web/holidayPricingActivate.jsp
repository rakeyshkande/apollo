<%@ page import="com.ftd.osp.utilities.ConfigurationUtil"%>
<%@ page import="com.ftd.pdb.common.ProductMaintConstants"%>

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="form" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>


    <html:form action="/submitHolidayPricingActivate"  focus="holidayPriceFlag">
    <jsp:include page="includes/security.jsp"/>
	<CENTER>
		<TABLE width="98%" border="0" cellpadding="2" cellspacing="2">
				<TR>
					<TD width="40%" class="labelright"> Holiday Pricing Flag: </TD>
					<TD width="20%"> <html:checkbox property="holidayPriceFlag" /> </TD>
					<TD width="40%" class="instruction"> Main Flag to turn on/off Holiday Pricing </TD>
				</TR>

				<TR>
					<TD class="labelright"> Start Date: </TD>
					<TD> <html:text property="startDate" size="10" maxlength="10"  /> </TD>
					<TD class="instruction"> e.g. MM/DD/YYYY </TD>
				</TR>

				<TR>
					<TD class="labelright"> End Date: </TD>
					<TD> <html:text property="endDate" size="10" maxlength="10"  /> </TD>
					<TD class="instruction"> e.g. MM/DD/YYYY </TD>
				</TR>

				<TR>
					<TD class="labelright">
						Charge based on Delivery Date:
					</TD>
					<TD> <html:checkbox property="deliveryDateFlag" /> </TD>
					<TD class="instruction"> If turn off charges will be based on Order Date </TD>
				</TR>

				<TR> <TD colspan="3"> &nbsp; </TD> </TR>

				<TR>
					
					<TD colspan="3" align="center">
						<h3>Novator Database</h3>
					</TD>
				</TR>
				<TR>
					<TD> &nbsp; </TD>
					<TD colspan="2">
				<%
				    ConfigurationUtil rm = ConfigurationUtil.getInstance();
				    if(rm.getFrpGlobalParmNoNull(ProductMaintConstants.PDB_CONFIG_CONTEXT, ProductMaintConstants.PDB_NOVATOR_LIVE_KEY).length() > 0)
				    {
					out.print("<input type=\"checkbox\" name=\"chkLive\" checked />Live<br>");
				    }
				    if(rm.getFrpGlobalParmNoNull(ProductMaintConstants.PDB_CONFIG_CONTEXT, ProductMaintConstants.PDB_NOVATOR_TEST_KEY).length() > 0)
				    {
					out.print("<input type=\"checkbox\" name=\"chkTest\" checked />Test<br>");
				    }
				    if(rm.getFrpGlobalParmNoNull(ProductMaintConstants.PDB_CONFIG_CONTEXT, ProductMaintConstants.PDB_NOVATOR_UAT_KEY).length() > 0)
				    {
					out.print("<input type=\"checkbox\" name=\"chkUAT\" checked />UAT<br>");
				    }
				    if(rm.getFrpGlobalParmNoNull(ProductMaintConstants.PDB_CONFIG_CONTEXT, ProductMaintConstants.PDB_NOVATOR_CONTENT_KEY).length() > 0)
				    {
					out.print("<input type=\"checkbox\" name=\"chkContent\" checked />Content<br>");
				    }
				%>
					</TD>
				</TR>
				
				<TR> <TD colspan="3"> &nbsp; </TD> </TR>

				<TR>
					<TD colspan="3" align="center">
						<INPUT type="button" name="btnCancel" value="  Cancel  " onclick="submitForm('admin.do')"> &nbsp;&nbsp;
						<INPUT type="submit" name="btnSave" value="  Update  ">
					</TD>
				</TR>
			</TABLE>

			<TABLE width="98%" border="0" cellpadding="2" cellspacing="2">
				<TR>
					<TD align="center">
						&nbsp;
					</TD>
				</TR>
			</TABLE>
			</CENTER>
    </html:form>
