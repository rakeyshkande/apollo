<%@ page import="com.ftd.pdb.common.valobjs.ShippingKeyVO"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<script type="text/javascript">
    function init(){}
    function unload(){}
</script>
<link rel="stylesheet" type="text/css" href="css/ftd.css" />

<%
	String securitytoken = (String)request.getParameter("securitytoken");
	String context = (String)request.getParameter("context");
        String actionType = (String)request.getParameter("actionType");
        String shippingKey = (String)request.getParameter("shippingKey");
        String actionStatus = (String)request.getParameter("actionStatus");



%>

		<html:form action="/updatePriceShipKey" method="post">
    <jsp:include page="includes/security.jsp"/>
			<CENTER>        
                        <% if (actionType != null && actionStatus != null) {
                        %>
                        <div class="Error">
                            <br />
                            Shipping Key&nbsp
                            <% if (shippingKey != null) {
                                %>
                                 <%= shippingKey %>&nbsp
                                <%
                            }
                            %>
                            <%= actionType %>&nbsp
                            <%= actionStatus %>
                      </div>

                        <%
                        }
                        %>
                        
                <br/>
		 <TABLE width="98%" border="0" cellpadding="2" cellspacing="2">
				<TR/>
				<TR>
					<TD class="TblTextHeader"> Shipping Key </TD>
					<TD class="TblTextHeader"> Description </TD>
					<TD class="TblTextHeader"> Shipper </TD>
				</TR>

				<TR bgcolor="#DDDDDD">
               
                    <logic:iterate id="list" name="keyList" scope="request">
                        <TR bgcolor=DDDDDD>
				
                            <TD>
                                <bean:write name="list" property="shippingKey_ID" />&nbsp;
                                <a href="editShipKey.do?shippingKey=<%=((ShippingKeyVO)list).getShippingKey_ID()%>&securitytoken=<%=securitytoken%>&context=<%=context%>">[edit]</a> &nbsp;
                                <a href="delShipKey.do?shippingKey=<%=((ShippingKeyVO)list).getShippingKey_ID()%>&securitytoken=<%=securitytoken%>&context=<%=context%>">[del]</a> &nbsp;
                            </TD>

                            <TD> <bean:write name="list" property="shippingKeyDescription" /> </TD>
                            <TD> <bean:write name="list" property="shipperID" /> </TD>
                        </TR>
                    </logic:iterate>
                <TR>
				    <TD>
                        <a href="addShippingKey.do?securitytoken=<%=securitytoken%>&context=<%=context%>">[add shipping key]</a>
					</TD>
					<TD> &nbsp; </TD>
					<TD> &nbsp; </TD>
				</TR>
			</TABLE>
			<br>
<%--
			<TABLE width="98%" border="0" cellpadding="2" cellspacing="2">
				<TR>
					<TD colspan="5" class="header3" align="center"> Global Shipper Price Increase/Decrease </TD>
				</TR>

				<TR>
					<TD class="tbltextheader"> Shipper Name </TD>
					<TD class="tbltextheader"> Price category </TD>
					<TD class="tbltextheader"> Amount </TD>
					<TD class="tbltextheader"> Price Change By </TD>
					<TD class="tbltextheader"> &nbsp; </TD>
				</TR>

				<TR bgcolor="#DDDDDD">
					<TD align="center">
						<html:select name="shippingKeyContentForm" property="global_shipper_name">
                            <html:options 
                                collection="shipperNameList"
                                property="shipperID" />
<!--                                labelProperty=""shipperID" />                          -->
						</html:select>
					</TD>
					<TD align="center">
						<html:select name="shippingKeyContentForm" property="global_price_category">
                            <html:options 
                                collection="priceCategoryList"
                                property="shipperID"
                                labelProperty="priceCategory"
                            />
					    </html:select>
					</TD>
					<TD align="center">
						<html:text name="shippingKeyContentForm" property="global_amount" size="10" />
					</TD>
					<TD>
						<html:radio name="shippingKeyContentForm" property="global_price_change_by" value="plus"  /> Increase <br>
						<html:radio name="shippingKeyContentForm" property="global_price_change_by" value="minus" /> Decrease
					</TD>
					<TD align="center">
						<INPUT type="submit" name="update" value="Update Price">
					</TD>
				</TR>

				<TR> <TD colspan="5"> &nbsp; </TD> </TR>

				<TR>
					<TD colspan="5" align="center">&nbsp;</TD>
				</TR>
			</TABLE>
--%>
			</CENTER>
		</html:form>
