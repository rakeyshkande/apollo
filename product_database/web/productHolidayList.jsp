<%@ page import="com.ftd.pdb.common.valobjs.ProductVO"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>


<%
	String securitytoken = (String)request.getParameter("securitytoken");
  String context = (String)request.getParameter("context");
%>


<html:form action="/submitProductMaintList">
<jsp:include page="includes/security.jsp"/>

<TABLE width="98%" border="0" cellpadding="2" cellspacing="2">
	<TR>
		<TD class="TblTextHeader" align="center" >ID</TD>
		<TD class="TblTextHeader" align="center" >Name</TD>
  		<TD class="TblTextHeader" align="center" >New SKU</TD>
  		<TD class="TblTextHeader" align="center" >New Price</TD>
   		<TD class="TblTextHeader" align="center" >Holiday Pricing</TD>
	</TR>
  
               <logic:iterate id="element" name="products1" scope="request">
                <TR>
                     <TD class="TblText"> 
                     	
                     	<a href="submitProductMaintList.do?productId=<%=((ProductVO)element).getProductId()%>&securitytoken=<%=securitytoken%>&context=<%=context%>"><bean:write name="element" property="productId" /></a>
                     </TD>
                     <TD class="TblText"><bean:write name="element" property="novatorName" filter="false" /></TD>
                     <TD class="TblText"><bean:write name="element" property="newSKU" /></TD>
                     <TD class="TblText">$<bean:write name="element" property="newStandardPrice" /></TD>
                     <TD class="TblText">
                     	<a href="deleteProductHolidayList.do?productId=<%=((ProductVO)element).getProductId()%>&securitytoken=<%=securitytoken%>&context=<%=context%>">Remove Holiday Pricing</a>
                     </TD>
                </TR>
                </logic:iterate>

</TABLE>
</html:form>
