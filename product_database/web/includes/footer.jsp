<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<script type="text/javascript" src="js/copyright.js"></script>
<%
    String securitytoken = (String)request.getParameter("securitytoken");
    String context = (String)request.getParameter("context");
    String adminAction = (String)request.getAttribute("adminAction");
    String optionalArgs = (String)request.getAttribute("optionalArgs");
    if (optionalArgs == null) {
        optionalArgs = "";
    }
%>

<div align="center">
  <table width="98%" border="0" cellpadding="0" cellspacing="0">
    <caption/>
    <tbody>
      <tr>
          <td colspan="1" class="LabelRight">
          <%
              String menuBack = (String)request.getAttribute("menuBack");
              String menuBackText = (String)request.getAttribute("menuBackText");
              if(menuBack != null && menuBackText != null)
              {
                  out.print("<a href='" + menuBack + "?securitytoken=" + securitytoken + "&context=" + context + optionalArgs + "&adminAction" + adminAction + "'>" + menuBackText + "</a>&nbsp;|&nbsp;");
              } 
          %>
          
          <a href="#" onclick="doMainMenuAction();"> Back to Main Menu</a>          
               
          </td>
      </tr>
    </tbody>
  </table>
  
  <table border="0" cellpadding="0" cellspacing="0" width="98%">
    <caption/>
    <tbody>
      <tr>
        <td>&nbsp;</td></tr>
      <tr>        
      	<td class="disclaimer"></td><script>showCopyright();</script>
      </tr>
    </tbody>
  </table>
</div>
