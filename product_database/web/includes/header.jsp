<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles"%>

<script type="text/javascript" src="js/clock.js"></script>
<%
  String securitytoken = (String)request.getParameter("securitytoken");
  String applicationcontext = (String)request.getParameter("applicationcontext");
  String context = (String)request.getParameter("context");
  String adminAction = (String)request.getAttribute("adminAction");
  String optionalArgs = (String)request.getAttribute("optionalArgs");
  if (optionalArgs == null) {
    optionalArgs = "";
  }
%>

<script language="JavaScript" type="text/javascript">
var securitytoken = "<%=securitytoken%>";
var applicationcontext = "<%=applicationcontext%>";
var context = "<%=context%>";
var adminAction = "<%=adminAction%>";
</script>
<script type="text/javascript" src="js/util.js">
</script>
<div align="center">
  <table width="98%" border="0" cellspacing="2" cellpadding="0">
    <caption/>
    <tr>
      <td width="20%" align="left" style="float:left;vertical-align:middle;padding-top:.4em;"><img src="images/wwwftdcom_131x32.gif" alt="ftd.com" border="0" height="32"></TD>
      <td width="60%" align="center" colspan="1" class="Header">
          <tiles:getAsString name="title"/>
      </td>
      <td width="20%" align="right" style="vertical-align:middle;padding-top:.4em;" class="Label">
            <span id="headerTime"> <script type="text/javascript">startClock();</script></span>
      </td>
    </tr>			
    <tr>
      <td colspan="3"><HR></td>
    </tr>
    <tr>
      <td colspan="3" class="LabelRight">
      <%
          String menuBack = (String)request.getAttribute("menuBack");
          String menuBackText = (String)request.getAttribute("menuBackText");
          if(menuBack != null && menuBackText != null)
          {
              out.print("<a href='" + menuBack + "?securitytoken=" + securitytoken + "&context=" + context + optionalArgs + "&adminAction" + adminAction + "'>" + menuBackText + "</a>&nbsp;|&nbsp;");
          } 
      %>
      <a href="#" onclick="doMainMenuAction();"> Back to Main Menu</a>
      </td>
    </tr>
  </table>
</div>
