<%@ page import="com.ftd.pdb.common.valobjs.UpsellMasterVO"%>
<%@ page import="com.ftd.pdb.common.valobjs.UpsellDetailVO"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<script type="text/javascript" src="js/util.js">
</script>
<script type="text/javascript">
    function init(){}
    function unload(){}
    
function submitForm(action)
{
    alert(document.forms[0].productId);
    alert(document.forms[0].productId.value);
    document.forms[0].action=action;    
    document.forms[0].submit();
}
</script>
<%
	String securitytoken = (String)request.getParameter("securitytoken");
	String context = (String)request.getParameter("context");
%>


<html:form action="/submitUpsellList" method="POST" focus="productId">
<jsp:include page="includes/security.jsp"/>
<center>
<TABLE border="0" cellpadding="2" cellspacing="2">
        <TR>
            <TD colspan="3" class="Error" align="center" width="40%"> 
                <!-- Did not use html:errors since the messages are created 
                within the action as opposed to the errors which are created
                within the ActionForm.validate method
                -->
                <html:messages id="msg" message="true" property="productId">
                    <bean:write name="msg"/>
                </html:messages>
            </TD>
        </TR>
	<TR>
		<TD class="Label" align="right" width="40%">Select Master SKU <html:errors property="productId"/></TD>
		<TD width="20%">
                    <input type="text" id="productId" name="productId" size="15" maxlength="10"/>
		</TD>
		<TD align="left" width="40%">
                <html:submit value="Edit"/>
		</TD>
	</TR>
    <TR>
        <TD colspan="3" align="center">
            <table cellpadding="5" cellspacing="2">
                <TR>
                    <TD class="HighlightAvail"><bean:message key="productmaintlist.availableproducts" /></TD>
                    <TD class="HighlightUnavail"><bean:message key="productmaintlist.unavailableproducts" /></TD>
                </TR>
            </table>
        </TD>
    </TR>    
</TABLE>
</center>

<TABLE width="98%" border="0" cellpadding="2" cellspacing="2">
	<TR>
		<TD class="TblHeader" align="center" colspan="3">
			Selection List
		</TD>
	</TR>
    <TR>
        <TD width="50%" valign="top">
            <TABLE width="100%" border="0" cellpadding="0" cellspacing="2">
                    <TR>
                    <TD class="Label" width=40%>Master Sku</TD>
                    <TD class="Label" width=10%>Base SKU</TD>
                    <TD class="Label" width=10%>Upsell SKU's</TD>
                    <TD class="Label" width=10%></TD>
                    <TD class="Label" width=10%></TD>
                    <TD class="Label" width=10%></TD>
                    <TD class="Label" width=10%></TD>
                    </TR>
                <logic:iterate id="element" name="upsellList" scope="request">
                    <%
                        String highlight = null;
                        if(((UpsellMasterVO)element).isMasterStatus()) { highlight = "HighlightAvail"; }
                        else { highlight = "HighlightUnavail"; }
                    %>
                    <TR>
                    <TD class="<%=highlight%>" valign="top"><a href="submitUpsellList.do?productId=<%=((UpsellMasterVO)element).getMasterID()%>&securitytoken=<%=securitytoken%>&context=<%=context%>" style="text-decoration:none"><bean:write name="element" property="masterID" /></a> (<bean:write name="element" property="masterName" filter="false" />)</TD>
                    <%
                       java.util.List detailList = ((UpsellMasterVO)element).getUpsellDetailList();
                       for(int i = 0; i < detailList.size(); i++){
                       		UpsellDetailVO detailItem = (UpsellDetailVO)detailList.get(i);
                                String id = detailItem.getDetailID();
                                if (id == null)
                                    id = "";
                        %>
                       	<TD class="<%=highlight%>"><%=id%></TD>
                       	<%	
                       }
                    %>
                
                    </TR>
                </logic:iterate>
            </TABLE>
        </TD>
    </TR>
</TABLE>
</html:form>
