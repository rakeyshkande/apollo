<%@ page import="com.ftd.osp.utilities.ConfigurationUtil"%>
<%@ page import="com.ftd.osp.utilities.feed.NovatorFeedUtil"%>
<%@ page import="com.ftd.pdb.common.ProductMaintConstants"%>
<%@ page import="java.util.HashMap"%>

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>

<bean:define id="updateForm" name="updateNovatorProductsForm" type="com.ftd.pdb.web.UpdateNovatorProductsForm" />

<script type="text/javascript" src="js/calendar2.js"></script>
<script type="text/javascript" src="js/Tokenizer.js"></script>
<script type="text/javascript" src="js/prototype.js"></script>
<script type="text/javascript" src="js/rico.js"></script>
<script type="text/javascript" src="js/ftdajax.js"></script>

<script type="text/javascript" language="JavaScript">
  var tokens;
  var idx=0;
  var READY_STATE_COMPLETE=4;
  var req;
  var method;
  var lastUpdateDate;
  var novatorUpdateContent;
  var novatorUpdateTest;
  var novatorUpdateUAT;
  var novatorUpdateLive;
  var error_message="ERROR:";
  var updatedSinceDaysBack = 90;
  
  function init() {
  }
  
  function unload() {
  }

  function submitData() {
    document.forms[0].btnSendUpdates.disabled = true;
    document.forms[0].console.value="";
    if( validateForm() ) {
      idx=0;
      document.forms[0].console.style.visibility='visible';
      
      if( method=="updateByDate" ) {
        //XMLHttpRequest
        var newRequest = 
          new FTD_AJAX.Request('submitUpdateNovatorProducts.do',FTD_AJAX_REQUEST_TYPE_POST,processGetProductsResponse,false);
        newRequest.addParam("lastUpdate",lastUpdateDate);
        newRequest.send();
      } else {
        sendProduct();
      }
    } else {
      document.forms[0].btnSendUpdates.disabled = false;
    }

    return false; //never submit the form
  }
  
  function validateForm() {
    var returnValid = true;
    var errorString = "";
    
    if (!(document.forms[0].NOVATOR_UPDATE_LIVE.checked || 
        document.forms[0].NOVATOR_UPDATE_CONTENT.checked ||
        document.forms[0].NOVATOR_UPDATE_TEST.checked ||
        document.forms[0].NOVATOR_UPDATE_UAT.checked)) {
        errorString = "Please select a Novator environment";
        returnValid = false;
    } else {
      //Validate the methods
      method = getCheckedValue(document.forms[0].updateMethod);
      if( method=="updateByDate" ) {
      
        //Validate the latest date
        lastUpdateDate = document.forms[0].updateDate.value;
        if(!validateDate(lastUpdateDate)) {
          errorString = "Please enter a properly formatted update date (MM/DD/YYYY)";
          returnValid = false;
        }
        var updateDate = new Date(lastUpdateDate);
        var now = new Date();
        var days_back_ms = new Date(now.getYear(),now.getMonth(),now.getDate()).getTime() - (updatedSinceDaysBack*86400000);
        var daysBack = new Date(days_back_ms);
        if(updateDate < daysBack)
        {
          errorString = "Updated since must be within the last "+ updatedSinceDaysBack + " days";
          returnValid = false;
        }
      } else if( method=="updateByList" ) {
        //Validate that there are values in the product list
        var strValues = document.forms[0].updateList.value;
        if( strValues==undefined || strValues.lenght==0 ) {
          errorString = "No values were entered in the product list";
          returnValid = false;
        } else {
          strValues = strValues.toUpperCase();
          tokens = specialCharsToSpaces(strValues).tokenize(" ", true, true);
          
          if( tokens==null || tokens.length==0 ) {
            errorString = "No values were entered in the product list";
            returnValid = false;
          }
        }
      } else {
        errorString = "Please select a method and try again!";
        returnValid = false;
      }
    }
    
    if( returnValid==false ) {
      alert(errorString);
    }
    
    //Save the server selections
    novatorUpdateContent = document.forms[0].NOVATOR_UPDATE_CONTENT.checked; 
    novatorUpdateTest = document.forms[0].NOVATOR_UPDATE_TEST.checked; 
    novatorUpdateUAT = document.forms[0].NOVATOR_UPDATE_UAT.checked; 
    novatorUpdateLive = document.forms[0].NOVATOR_UPDATE_LIVE.checked;
        
    return returnValid;
  }
  
  function sendProduct() {
    if( idx==tokens.length ) {
       var newValue = document.forms[0].console.value + "\rProcess complete.\r"+idx+" products processed.";
       document.forms[0].console.value = newValue;
       scrollToBottom(document.forms[0].console);
       alert("Process complete.  "+idx+" products processed.");
       document.forms[0].btnSendUpdates.disabled = false;
    }
    else {
      var productId = tokens[idx];
      if(idx == 0)
        var newValue = document.forms[0].console.value + "Sending " + tokens[idx] + "...\t";
      else
        var newValue = document.forms[0].console.value + "\nSending " + tokens[idx] + "...\t";
      document.forms[0].console.value = newValue;
      scrollToBottom(document.forms[0].console);
      
      //XMLHttpRequest
      var newRequest = 
          new FTD_AJAX.Request('submitUpdateNovatorProducts.do',FTD_AJAX_REQUEST_TYPE_POST,processProductUpdateResponse,false);
      newRequest.addParam("productId",tokens[idx]);
      //newRequest.addParam("novatorServer",novatorServer);
      newRequest.addParam("novatorUpdateContent",novatorUpdateContent);
      newRequest.addParam("novatorUpdateTest",novatorUpdateTest);
      newRequest.addParam("novatorUpdateUAT",novatorUpdateUAT);
      newRequest.addParam("novatorUpdateLive",novatorUpdateLive);
      newRequest.send();
      idx++;
    }
  }

  function processProductUpdateResponse(data) {
    //alert(data);
    var returnedText = data;

    var oldText = document.forms[0].console.value;
    document.forms[0].console.value = oldText + " " + returnedText;     
    scrollToBottom(document.forms[0].console);

    var index = data.indexOf("Error");
    if( index < 0 ) {
      sendProduct();
    } else {
      //if an error occurred display the error text and ask the user if
      //they would like to continue or abort
      var input_box=confirm("Problem: " + data + "\r\rClick OK to continue or Cancel to Abort");
      
      //if the user chooses to continue send the next vendor
      if (input_box == true) {
        sendProduct();
      } else {
        //if the user aborts enable the submit button
        document.forms[0].btnSendUpdates.disabled = false;
      }
    }
  }
  
  function processGetProductsResponse(data) {
//    alert(data);
    var testText = "NO_PRODUCTS_FOUND";
    
    if( data.substr(0,error_message.length)==error_message ) {
      var oldText = document.forms[0].console.value;
      document.forms[0].console.value = "An error has occurred while retrieving the list of products to update.\r\nSee log for details.";     
      scrollToBottom(document.forms[0].console);
      alert(data);
      document.forms[0].btnSendUpdates.disabled = false;
    } else {
        var returnedText = specialCharsToSpaces(data);
        tokens = returnedText.tokenize(" ", true, true);
        if( tokens==null || tokens.length==0 || tokens[0].substr(0,testText.length)==testText )
        {
          alert("No products were found for the specified date.");
          document.forms[0].console.value = "No products were found for the specified date.";
          document.forms[0].btnSendUpdates.disabled = false;
        } else {
          sendProduct();
        }
    }
  }
  
  // return the value of the radio button that is checked
  // return an empty string if none are checked, or
  // there are no radio buttons
  function getCheckedValue(radioObj) {
    if(!radioObj)
      return "";
    var radioLength = radioObj.length;
    if(radioLength == undefined)
      if(radioObj.checked)
        return radioObj.value;
      else
        return "";
    for(var i = 0; i < radioLength; i++) {
      if(radioObj[i].checked) {
        return radioObj[i].value;
      }
    }
    return "";
  }
  
  //Assumes that the passed in date is in the format mm/dd/yyyy
  function validateDate(srcdate) {
    if( srcdate.substring(2,3)=="/" && srcdate.substring(5,6)=="/" ) {
      return isDate(srcdate.substring(6,10),srcdate.substring(0,2),srcdate.substring(3,5));
    }
    else {
      return false;
    }
  }
  
  //Does the passed parameters make a valid date
  function isDate (year, month, day) {
    // month argument must be in the range 1 - 12
    month = month - 1;  // javascript month range : 0- 11
    var tempDate = new Date(year,month,day);
    var testYear = tempDate.getYear();
    if( testYear < 2000 ) { //Mozilla
      testYear+=1900;
    }

    if ( testYear == year &&
      (month == tempDate.getMonth()) &&
      (day == tempDate.getDate()) )
        return true;
    else
        return false
  }
  
  function specialCharsToSpaces(strObject) {
    //convert any special characters to spaces
    while(strObject.indexOf("\n") != -1)
      strObject = strObject.replace("\n"," ");

    while(strObject.indexOf("\f") != -1)
      strObject = strObject.replace("\f"," ");

    while(strObject.indexOf("\b") != -1)
      strObject = strObject.replace("\b"," ");

    while(strObject.indexOf("\r") != -1)
      strObject = strObject.replace("\r"," ");

    while(strObject.indexOf("\t") != -1)
      strObject = strObject.replace("\t"," ");
      
      return strObject;
  }
  
  function scrollToBottom (element) {
  //if (document.all)
    element.scrollTop = element.scrollHeight;
  }

</script>

<!--
<html:form action="/submitUpdateNovatorProducts">
-->
<form method="POST" action="javascript: return submitData();">
<jsp:include page="includes/security.jsp"/>
<table width="98%" border="0" cellpadding="2" cellspacing="2" align="center">
    <caption/>
  <tr>
    <td colspan="2">
      <table align="center">
        <caption/>
        <tr>
          <td>
            <html:radio property="updateMethod" value="updateByDate"/>Updated Since:&nbsp;
          </td>
          <td>
            <html:text property="updateDate"/>
            &nbsp;<a href="javascript:updateCal.popup();"><img src="images/show-calendar.gif" width="22" height="22" align="middle" border="0" alt="Click here to select the sale starting date"></a>
          </td>
        </tr>
        <tr>
          <td>
            <html:radio property="updateMethod" value="updateByList"/>Product List:&nbsp;</input>
          </td>
          <td>
            <html:textarea property="updateList" cols="50" rows="5"/>
          </td>
        </tr>
        
        <tr>
          <td>   
          </td>
          <td>
            <!-- (visibility="visible|hidden") -->
            <html:textarea property="console" cols="50" rows="5" style="visibility: hidden; vertical-align: top"/>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td colspan="2" align="center">
      <hr size=1></hr> 
    </td>
	</tr>
  <tr>
    <td colspan="2" align="center">
      <input type="checkbox"
            <%=updateForm.get("liveDisabled")%>
            <%=updateForm.get("liveChecked")%>
            name="NOVATOR_UPDATE_LIVE" 
            title="Send updates to Novator production web site"/>
      <label class="radioLabel">Live</label>&nbsp;&nbsp;
      <input type="checkbox"
            <%=updateForm.get("contentDisabled")%>
            <%=updateForm.get("contentChecked")%>
            name="NOVATOR_UPDATE_CONTENT" 
            title="Send updates to Novator content web site"/>
      <label class="radioLabel">Content</label>&nbsp;&nbsp;
      <input type="checkbox"
            <%=updateForm.get("testDisabled")%>
            <%=updateForm.get("testChecked")%>
            name="NOVATOR_UPDATE_TEST" 
            title="Send updates to Novator test web site"/>
      <label class="radioLabel">Test</label>&nbsp;&nbsp;
      <input type="checkbox"
            <%=updateForm.get("uatDisabled")%>
            <%=updateForm.get("uatChecked")%>
            name="NOVATOR_UPDATE_UAT" 
            title="Send updates to Novator UAT web site"/>
      <label class="radioLabel">UAT</label>
    </td>
	</tr>
    
	<tr>
		<td colspan="2" align="center">
			<input type="button" name="btnSendUpdates" value="Submit" onclick="javascript:submitData();" />
		</td>
	</tr>
</table>
<!--
</html:form>
-->
</form>

<script type="text/javascript" language="JavaScript">
  var updateCal = new calendar2(document.forms[0].elements['updateDate']);
  updateCal.year_scroll = true;
  updateCal.time_comp = false;
</script>
