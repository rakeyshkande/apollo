<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=windows-1252" isErrorPage="true"
         import="java.io.CharArrayWriter, java.io.PrintWriter"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252"/>
        <link rel="stylesheet" type="text/css" href="css/ftd.css" />
        <title>Error</title>
    </head>
    <body>
        <div align="center">
            <table width="98%" border="0">
                <tr><td>&nbsp;</td></tr>
                <tr><td class="HeaderError">AN ERROR HAS OCCURRED!</td></tr>
                <tr><td>&nbsp;</td></tr>
        <%
            if( exception!=null ) {
        %>
                <tr><td bgcolor="#FFCCCC">
                <%
                out.println(exception.getMessage());
                CharArrayWriter charArrayWriter = new CharArrayWriter(); 
                PrintWriter printWriter = new PrintWriter(charArrayWriter, true); 
                exception.printStackTrace(printWriter); 
                out.println(charArrayWriter.toString()); 
                %>
                </td></tr>
        <%
            }
        %>
                <tr><td class="HeaderError">Contact a FTD.COM/IT Business Analyst immediately!</td></tr>
            </table>
        </div>
    </body>
</html>