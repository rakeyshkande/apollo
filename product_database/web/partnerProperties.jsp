<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html"%>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<script type = "text/javascript" src = "js/util.js">
</script>

<script language = "JavaScript" type = "text/javascript">
 function init() {
  if (document.getElementById('partnerId').value == '') {
   document.getElementById('partnerDiv').style.display = 'none';
   document.getElementById('buttonDiv').style.display = 'none';
  } else {
   document.getElementById('partnerDiv').style.display = 'block';
   document.getElementById('buttonDiv').style.display = 'block';
  }
 }

function unload(){}


</script>

<html:form method = "POST" action = "/submitPartnerProperties">
<jsp:include page="includes/security.jsp"/>
 <bean:define id = "allPartners" name = "partnerPropertiesForm" property = "allPartners" type = "java.lang.String[]" scope = "request"/>

 <table class = "mainTable" align = "center" cellpadding = "1" cellspacing = "1" width = "98%" border = "0">
  <tr>
   <td> <!--Content table-->
    <table class = "innerTable" border = "0" width = "100%" align = "center">
     <tr>
      <td>
       <table border = "0" align = "left">
        <tr><td colspan = "5" class = "labelpad">
          <html:errors property = "generalError"/></td>
        </tr>

        <tr>
         <td class = "labelpad">
          Choose a Partner ID:
         </td>

         <td>
          <html:select property = "partnerId" onchange = "submitForm('showPartnerProperties.do')">
           <html:option value = ""></html:option>
           <c:forEach var = "partner" items = "${allPartners}" varStatus = "status">
            <html:option value = "${partner}">${partner}</html:option>
           </c:forEach>
          </html:select>
          <!-- hidden list of all options for page reload -->
          <c:forEach var = "partner" items = "${allPartners}" varStatus = "status">
           <html:hidden property = "allPartners" value = "${partner}"/>
          </c:forEach>
         </td>

         <td>
         </td>
        </tr>
       </table>
      </td>
     </tr>

     <tr>
      <td>
       <div id = "partnerDiv" style = "display:none">
        <table border = "0" width = "100%" align = "center">
         <tr><td class = "TblHeader" colspan = "5" align = "center">Partner Information</td>
         </tr>

         <tr>
          <td class = "labelpad" width = "24%">
           Partner Name:
          </td>

          <td width = "24%">
           <html:text property = "partnerName" maxlength = "100"/>
          </td>

          <td width = "50%">
           <html:errors property = "partnerName"/>
          </td>
         </tr>

         <tr>
          <td class = "labelpad" width = "24%">
           Origin ID:
          </td>

          <td width = "24%">
           <html:text property = "originId" size = "10" maxlength = "10" readonly="true"/>
          </td>

          <td width = "50%">
           <html:errors property = "originId"/>
          </td>
         </tr>

         <tr>
          <td class = "labelpad" width = "24%">
           Active:
          </td>

          <td width = "24%">
           <html:checkbox property="activeFlag"/>
          </td>

          <td width = "50%">
           <html:errors property = "activeFlag"/>
          </td>
         </tr>

         <tr><td class = "TblHeader" width = "100%" colspan = "4" align = "center">Partner Properties</td>
         </tr>

         <tr><td class = "labelpad" width = "100%" colspan = "4">
           <html:errors property = "partnerProperties"/>
          </td>
         </tr>

         <c:forEach var = "partnerProperty" items = "${propertyList}" varStatus = "status">
          <tr>
           <td class = "labelpad" width = "24%">
            <c:out value = "${partnerProperty.propertyName}"/>

            <html:hidden property = "propertyNames" value = "${partnerProperty.propertyName}"/>:
           </td>

           <td width = "24%">
            <html:text property = "propertyValues" value = "${partnerProperty.propertyValue}" size = "50" maxlength = "200"/>

            <html:hidden property = "validationRegex" value = "${partnerProperty.validationRegex}"/>
           </td>

           <td width = "30%">
           </td>
          </tr>
         </c:forEach>
        </table>
       </div>
      </td>
     </tr>
    </table>
   </td> <!--Content table-->
  </tr>
 </table>

 <br/>

 <div id = "buttonDiv" style = "display:none" align = "center" width = "100%">
  <html:button value = "Save" property = "submitButton" onclick = "document.forms[0].submit();"/>
 </div>
</html:form>
