<%
	Exception exceptionObject = (Exception)request.getAttribute("ExceptionObject");
%>
<div align="center">
    <table width="98%" border="0">
        <tr><td>&nbsp;</td></tr>
<%
    if( exceptionObject!=null ) {
%>
        <tr><td bgcolor="#FFCCCC">
        <%
        out.println(exceptionObject.getMessage());
        java.io.CharArrayWriter charArrayWriter = new java.io.CharArrayWriter(); 
        java.io.PrintWriter printWriter = new java.io.PrintWriter(charArrayWriter, true); 
        exceptionObject.printStackTrace(printWriter); 
        out.println(charArrayWriter.toString()); 
        %>
        </td></tr>
<%
    }
%>
        <tr><td class="HeaderError">Contact a FTD.COM/IT Business Analyst immediately!</td></tr>
    </table>
</div>
