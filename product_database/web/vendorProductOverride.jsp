<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ page import="com.ftd.pdb.common.ProductMaintConstants" %>
<%@ page import="com.ftd.pdb.common.valobjs.VendorProductOverrideVO" %>


<bean:define id="overrideForm" name="vendorProductOverrideForm" type="org.apache.struts.validator.DynaValidatorForm" scope="request"/>
<bean:define id="searchTypeListOptions" name="vendorProductOverrideForm" property="<%=ProductMaintConstants.SEARCH_TYPE_LIST_KEY%>" type="java.util.ArrayList" scope="request"/>
<bean:define id="vendorListOptions" name="vendorProductOverrideForm" property="<%=ProductMaintConstants.VENDOR_LIST_KEY%>" type="java.util.ArrayList" scope="request"/>
<bean:define id="carrierListOptions" name="vendorProductOverrideForm" property="<%=ProductMaintConstants.CARRIER_LIST_KEY%>" type="java.util.ArrayList" scope="request"/>
<bean:define id="searchResultsListOptions" name="vendorProductOverrideForm" property="<%=ProductMaintConstants.SEARCH_RESULT_LIST_KEY%>" type="java.util.ArrayList" scope="request"/>

<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/formCheck.js"></script>
<script type="text/javascript" src="js/Listbox.js"></script>
<script type="text/javascript" src="js/prototype.js"></script>
<script type="text/javascript" src="js/rico.js"></script>
<script type="text/javascript" src="js/ftddom.js"></script>
<script type="text/javascript" src="js/ftdajax.js"></script>
<script type="text/javascript" src="js/calendar2.js"></script>
<script type="text/javascript" src="js/Tokenizer.js"></script>

<script language="JavaScript" type="text/javascript">
var onloads = new Array();
var borderFocusColor = "GoldenRod";
var labelFocusColor = "DarkGoldenRod";

//***********************************************************************************************
// function init()
//***********************************************************************************************
function init()
{
	for ( var i = 0 ; i < onloads.length ; i++ )
			onloads[i]();

	setupSearch(document.getElementById('searchBy'));
	//setNavigationHandlers();
	getProductsForVendor();
        getSdsShipMethodsForCarrier();
	populateStartEndDate();
        displayResults();
}

//***********************************************************************************************
// function unload()
//***********************************************************************************************
function unload()
{
}


//******
//******GET VENDOR PRODUCTS
//******
//***********************************************************************************************
// function getProductsForVendor()
//***********************************************************************************************
function getProductsForVendor()
{
        cursor_wait();
	document.getElementById("addButton").disabled = true;
	document.getElementById("refreshButton").disabled = true;
     	document.getElementById("deleteButton").disabled = true;


	var productField = document.getElementById("productAdd");
	productField.options.length = 0;
	var newOpt = new Option("Loading...", "");
	productField.options[0] = newOpt;
	productField.selectedIndex = 0;

	var vendorField = document.getElementById('vendorAdd');
	var selectedValue = vendorField.options[vendorField.selectedIndex].value;

	//XMLHttpRequest
	var newRequest =
			new FTD_AJAX.Request('submitVendorProductOverride.do',FTD_AJAX_REQUEST_TYPE_POST,getProductsForVendorCallback,false);
	newRequest.addParam("<%=ProductMaintConstants.AJAX_MODE%>","<%=ProductMaintConstants.VPO_MODE_GET_VENDOR_PRODUCT_LIST%>");
	newRequest.addParam("<%=ProductMaintConstants.AJAX_PROPERTY_ID%>","productAdd");
	newRequest.addParam("<%=ProductMaintConstants.AJAX_PROPERTY_NAME%>","productAdd");
	newRequest.addParam("<%=ProductMaintConstants.AJAX_VENDOR_ID%>",selectedValue);
	newRequest.send();
}

//***********************************************************************************************
// function getProductsForVendorCallback(data)
//***********************************************************************************************
function getProductsForVendorCallback(data)
{
	//alert(data);
	var productDiv = document.getElementById("productAddDiv");
	productDiv.innerHTML = data;

	var productField = document.getElementById("productAdd");
	if( productField != undefined && productField.options.length>0 )
	{
		document.getElementById("addButton").disabled = false;
		new FTD_DOM.FocusElement( 'productAdd', borderFocusColor, 'productAddLabel', labelFocusColor );
	}
        cursor_clear();
        document.getElementById("refreshButton").disabled = false;
      	document.getElementById("deleteButton").disabled = false;

}

//***********************************************************************************************
// function getSdsShipMethodsForCarrier()
//***********************************************************************************************
function getSdsShipMethodsForCarrier()
{
        cursor_wait();
	document.getElementById("addButton").disabled = true;
	document.getElementById("refreshButton").disabled = true;
      	document.getElementById("deleteButton").disabled = true;


	var sdsShipMethodField = document.getElementById("sdsShipMethodAdd");
	sdsShipMethodField.options.length = 0;
	var newOpt = new Option("Loading...", "");
	sdsShipMethodField.options[0] = newOpt;
	sdsShipMethodField.selectedIndex = 0;

	var carrierField = document.getElementById('carrierAdd');
	var selectedValue = carrierField.options[carrierField.selectedIndex].value;

	//XMLHttpRequest
	var newRequest =
			new FTD_AJAX.Request('submitVendorProductOverride.do',FTD_AJAX_REQUEST_TYPE_POST,getSdsShipMethodsForCarrierCallback,false);
	newRequest.addParam("<%=ProductMaintConstants.AJAX_MODE%>","<%=ProductMaintConstants.VPO_MODE_GET_CARRIER_SDS_SHIP_METHOD_LIST%>");
	newRequest.addParam("<%=ProductMaintConstants.AJAX_CARRIER_ID%>",selectedValue);
        newRequest.addParam("<%=ProductMaintConstants.AJAX_PROPERTY_ID%>","sdsShipMethodAdd");
	newRequest.addParam("<%=ProductMaintConstants.AJAX_PROPERTY_NAME%>","sdsShipMethodAdd");   
	newRequest.send();
}

//***********************************************************************************************
// function getSdsShipMethodsForCarrierCallback(data)
//***********************************************************************************************
function getSdsShipMethodsForCarrierCallback(data)
{
	//alert(data);
	var sdsShipMethodDiv = document.getElementById("sdsShipMethodAddDiv");
	sdsShipMethodDiv.innerHTML = data;

	var sdsShipMethodField = document.getElementById("sdsShipMethodAdd");
	if( sdsShipMethodField != undefined && sdsShipMethodField.options.length>0 )
	{
		document.getElementById("addButton").disabled = false;
		new FTD_DOM.FocusElement( 'sdsShipMethodAdd', borderFocusColor, 'sdsShipMethodAddLabel', labelFocusColor );
	}
        cursor_clear();
        document.getElementById("refreshButton").disabled = false;
        document.getElementById("deleteButton").disabled = false;

}


//******
//******FILTER THE VENDOR OVERRIDES
//******
//***********************************************************************************************
// function doSearch()
//***********************************************************************************************
function doSearch()
{
 
        document.getElementById("dateSearch").style.backgroundColor	= "white";
	var searchBy = document.getElementById("searchBy");
	var searchByValue = searchBy.options[searchBy.selectedIndex].value;

	var searchOn;
	var searchOnValue;
        var searchDate;

	if (searchByValue == 'vendor')
	{
		searchOn = document.getElementById("vendorSearch");
		searchOnValue = searchOn.options[searchOn.selectedIndex].value;
                searchDate = document.getElementById("dateSearch").value;
	}
	else if (searchByValue == 'product')
	{
		searchOnValue = document.getElementById("productSearch").value;
                searchDate = document.getElementById("dateSearch").value;
	}
	else if (searchByValue == 'carrier')
	{
		searchOn = document.getElementById("carrierSearch");
		searchOnValue = searchOn.options[searchOn.selectedIndex].value;
                searchDate = document.getElementById("dateSearch").value;
	}
	else if (searchByValue == 'date')
	{
		searchOnValue = document.getElementById("dateSearch").value;
                if (searchOnValue == "")
                {
                  document.getElementById("dateSearch").style.backgroundColor	= "pink";
                  alert ("Date field cannot be blank on date searches");             
                  return false;
                }
	}
	else
	{
		searchOnValue = "";
	}
        cursor_wait();
	document.getElementById("addButton").disabled = true;
	document.getElementById("refreshButton").disabled = true;
      	document.getElementById("deleteButton").disabled = true;

	//XMLHttpRequest
	var newRequest =
			new FTD_AJAX.Request('submitVendorProductOverride.do',FTD_AJAX_REQUEST_TYPE_POST,getSearchResultsCallback,false);
	newRequest.addParam("<%=ProductMaintConstants.AJAX_MODE%>","<%=ProductMaintConstants.VPO_MODE_SEARCH_VPO%>");
	newRequest.addParam("<%=ProductMaintConstants.AJAX_SEARCH_BY%>",searchByValue);
	newRequest.addParam("<%=ProductMaintConstants.AJAX_SEARCH_ON%>",searchOnValue);
      	newRequest.addParam("<%=ProductMaintConstants.AJAX_SEARCH_DATE%>",searchDate);
	newRequest.send();

}


//***********************************************************************************************
// function getSearchResultsCallback(data)
//***********************************************************************************************
function getSearchResultsCallback(data)
{
    cursor_clear();
    var errorLength = <%=ProductMaintConstants.VPO_RESULTS_ERROR.length()%>;
    if( data.length >= errorLength && data.substring(0,errorLength)=="<%=ProductMaintConstants.VPO_RESULTS_ERROR%>" ) {
	alert(data);
    } else {
	var searchResultsDiv = document.getElementById("searchResultsDiv");
	searchResultsDiv.innerHTML = data;
        displayResults();
    } 

    document.getElementById("addButton").disabled = false;
    document.getElementById("refreshButton").disabled = false;
    document.getElementById("deleteButton").disabled = false;

}



//******
//******ADD THE VENDOR OVERRIDES
//******
//***********************************************************************************************
// function doAddOverrides()
//***********************************************************************************************
function doAddOverrides()
{

	if (validateAdd())
	{
                cursor_wait();
		var vendor = document.getElementById('vendorAdd');
		var vendorValue = vendor.options[vendor.selectedIndex].value;

		var product = document.getElementById('productAdd');
		var productValue = product.options[product.selectedIndex].value;

		var carrier = document.getElementById('carrierAdd');
		var carrierValue = carrier.options[carrier.selectedIndex].value;

		var startDate = document.getElementById("startDateAdd").value;

		var endDate = document.getElementById("endDateAdd").value;
                
                
                var searchBy = document.getElementById("searchBy");
		var searchByValue = searchBy.options[searchBy.selectedIndex].value;

		var searchOn;
		var searchOnValue;
                
                var sdsShipMethodField = document.getElementById("sdsShipMethodAdd");
                
                selectedSdsShipMethods = new Array(); 
                for (var i = 0; i < sdsShipMethodField.options.length; i++)
                {
                        if (sdsShipMethodField.options[ i ].selected) selectedSdsShipMethods.push(sdsShipMethodField.options[ i ].value);
                }

		if (searchByValue == 'vendor')
		{
			searchOn = document.getElementById("vendorSearch");
			searchOnValue = searchOn.options[searchOn.selectedIndex].value;
		}
		else if (searchByValue == 'product')
		{
			searchOnValue = document.getElementById("productSearch").value;
		}
		else if (searchByValue == 'carrier')
		{
			searchOn = document.getElementById("carrierSearch");
			searchOnValue = searchOn.options[searchOn.selectedIndex].value;
		}
		else if (searchByValue == 'date')
		{
			searchOnValue = document.getElementById("dateSearch").value;
		}
		else
		{
			searchOnValue = "";
		}
                document.getElementById("addButton").disabled = true;
                document.getElementById("refreshButton").disabled = true;
     	        document.getElementById("deleteButton").disabled = true;
                
		//XMLHttpRequest
		var newRequest =
				new FTD_AJAX.Request('submitVendorProductOverride.do',FTD_AJAX_REQUEST_TYPE_POST,getAddResultsCallback,false);
		newRequest.addParam("<%=ProductMaintConstants.AJAX_MODE%>","<%=ProductMaintConstants.VPO_MODE_ADD_VPO%>");
		newRequest.addParam("<%=ProductMaintConstants.AJAX_SEARCH_BY%>",searchByValue);
		newRequest.addParam("<%=ProductMaintConstants.AJAX_SEARCH_ON%>",searchOnValue);
		newRequest.addParam("<%=ProductMaintConstants.AJAX_VENDOR_ID%>",vendorValue);
		newRequest.addParam("<%=ProductMaintConstants.AJAX_PRODUCT_ID%>",productValue);
		newRequest.addParam("<%=ProductMaintConstants.AJAX_CARRIER_ID%>",carrierValue);
		newRequest.addParam("<%=ProductMaintConstants.AJAX_START_DATE%>",startDate);
		newRequest.addParam("<%=ProductMaintConstants.AJAX_END_DATE%>",endDate);
                newRequest.addParam("<%=ProductMaintConstants.AJAX_NUMBER_OF_SELECTED_SDS_SHIP_METHODS%>",selectedSdsShipMethods.length);              

                for (var j=0; j < selectedSdsShipMethods.length; j++)
                {
                      newRequest.addParam("<%=ProductMaintConstants.AJAX_SDS_SHIP_METHOD%>" + j,selectedSdsShipMethods[j]);              
                }

		newRequest.send();


	}
}


//***********************************************************************************************
// function getAddResultsCallback(data)
//***********************************************************************************************
function getAddResultsCallback(data)
{
    cursor_clear();
    alert(data); 
    if( data=="<%=ProductMaintConstants.VPO_RESULTS_ADD_SUCCESS%>" ) {
        hideResults();
    }
    else {
      document.getElementById("deleteButton").disabled = false;
    }
    document.getElementById("addButton").disabled = false;
    document.getElementById("refreshButton").disabled = false;
}


//***********************************************************************************************
// function validateAdd()
//***********************************************************************************************
function validateAdd()
{
	document.getElementById("startDateAdd").style.backgroundColor	= "white";
	document.getElementById("endDateAdd").style.backgroundColor	= "white";
        document.getElementById("sdsShipMethodAdd").style.backgroundColor	= "white";



	//***********
	//**START DATE
	//***********
	//retrieve the start date
	var startDate = document.getElementById("startDateAdd").value;

	//parse and store the start date
	var startDateTokens = startDate.tokenize("/", true, true);
	var startMonth = startDateTokens[0];
	var startDay = startDateTokens[1];
	var startYear = startDateTokens[2];

	//create a new start date as a date variable, using the user entered date
	var dStartDate = new Date();
	dStartDate.setMonth(startMonth - 1); //month is 0 based, so, subtract 1
	dStartDate.setYear(startYear);
	dStartDate.setDate(startDay);

	//***********
	//**END DATE
	//***********
	//retrieve the end date
	var endDate = document.getElementById("endDateAdd").value;

	//parse and store the end date
	var endDateTokens = endDate.tokenize("/", true, true);
	var endMonth = endDateTokens[0];
	var endDay = endDateTokens[1];
	var endYear = endDateTokens[2];

	//create a new end date as a date variable, using the user entered date
	var dEndDate = new Date();
	dEndDate.setMonth(endMonth - 1); //month is 0 based, so, subtract 1
	dEndDate.setYear(endYear);
	dEndDate.setDate(endDay);
       
        var sdsShipMethodField = document.getElementById("sdsShipMethodAdd");
                
        selectedSdsShipMethods = new Array(); 
        for (var i = 0; i < sdsShipMethodField.options.length; i++)
        {
               if (sdsShipMethodField.options[ i ].selected) selectedSdsShipMethods.push(sdsShipMethodField.options[ i ].value);
        }
        
        if (selectedSdsShipMethods.length == 0)
        {
                alert("At least one SDS Ship Method must be selected");
		document.getElementById("sdsShipMethodAdd").style.backgroundColor	= "pink";
		return false;
        }


	//***********
	//**TODAY DATE
	//***********
	//create todays date
	var dTodayDate = new Date();

	//get the time for the end date
	//var todayDateTime = dTodayDate.getTime();

	if (dStartDate <= dTodayDate)
	{
		alert("The start date should be greater than the current date");
		document.getElementById("startDateAdd").style.backgroundColor	= "pink";
		return false;
	}
	else if (dEndDate <= dTodayDate)
	{
		alert("The end date should be greater than the current date");
		document.getElementById("endDateAdd").style.backgroundColor	= "pink";
		return false;
	}
	else if (dEndDate < dStartDate)
	{
		alert("The end date should be greater than the start date");
		document.getElementById("startDateAdd").style.backgroundColor	= "pink";
		document.getElementById("endDateAdd").style.backgroundColor	= "pink";
		return false;
	}
	else
		return true;

}



//******
//******DELETE THE VENDOR OVERRIDES
//******
//***********************************************************************************************
// function deleteOverrides()
//***********************************************************************************************
function deleteOverrides()
{
    var searchBy = document.getElementById("searchBy");
    var searchByValue = searchBy.options[searchBy.selectedIndex].value;

    var searchOn;
    var searchOnValue;

    if (searchByValue == 'vendor')
    {
        searchOn = document.getElementById("vendorSearch");
        searchOnValue = searchOn.options[searchOn.selectedIndex].value;
    }
    else if (searchByValue == 'product')
    {
        searchOnValue = document.getElementById("productSearch").value;
    }
    else if (searchByValue == 'carrier')
    {
        searchOn = document.getElementById("carrierSearch");
        searchOnValue = searchOn.options[searchOn.selectedIndex].value;
    }
    else if (searchByValue == 'date')
    {
        searchOnValue = document.getElementById("dateSearch").value;
    }
    else
    {
        searchOnValue = "";
    }

    var total = document.getElementById("overrideCount").value;
    var deleteString ="";
    var checkBox;
    var deleteCount = 0;
    cursor_wait();
    for (var i= 0 ; i < total; i++)
    {
        checkBox = document.getElementById("check_" + i);
        if (checkBox != null && checkBox.checked)
        {
            deleteString += checkBox.vendor + ',';
            deleteString += checkBox.product + ','
            deleteString += checkBox.carrier + ','
            deleteString += checkBox.date +  ','
            deleteString += checkBox.shipmethod +'|';
            deleteCount++;
        }
    }
    cursor_clear();
    if (deleteString.length == 0)
    {
        alert("No Ship Method Override selected for deletion.  Select one and try again.");
    }
    else
    {
        var msgString;
        if (deleteCount>1) {
            msgString = "Are you sure you want to delete these "+deleteCount+" overrides?";
        } else {
            msgString = "Are you sure you want to delete this override?";
        }
        var input_box=confirm(msgString);
        if (input_box==true) {
            cursor_wait();
            document.getElementById("addButton").disabled = true;
            document.getElementById("refreshButton").disabled = true;
      	    document.getElementById("deleteButton").disabled = true;
            
            //XMLHttpRequest
            var newRequest =
                new FTD_AJAX.Request('submitVendorProductOverride.do',FTD_AJAX_REQUEST_TYPE_POST,getDeleteResultsCallback,false);
            newRequest.addParam("<%=ProductMaintConstants.AJAX_MODE%>","<%=ProductMaintConstants.VPO_MODE_DELETE_VPO%>");
            newRequest.addParam("<%=ProductMaintConstants.AJAX_SEARCH_BY%>",searchByValue);
            newRequest.addParam("<%=ProductMaintConstants.AJAX_SEARCH_ON%>",searchOnValue);
            newRequest.addParam("<%=ProductMaintConstants.AJAX_DELETE_STRING%>",deleteString);

            newRequest.send();
        }
    }
}


//***********************************************************************************************
// function getDeleteResultsCallback(data)
//***********************************************************************************************
function getDeleteResultsCallback(data)
{
    cursor_clear();
    alert(data);
    if( data=="<%=ProductMaintConstants.VPO_RESULTS_DELETE_SUCCESS%>" ) {
        hideResults();
    }
    else {
      document.getElementById("deleteButton").disabled = false;
    }
    document.getElementById("addButton").disabled = false;
    document.getElementById("refreshButton").disabled = false;
}




//***********************************************************************************************
// function setupSearch(combo)
//***********************************************************************************************
function setupSearch(combo)
{
    document.getElementById("dateSearch").style.backgroundColor	= "white";
    document.getElementById('vendorDiv').style.display = 'none';
    document.getElementById('productDiv').style.display = 'none';
    document.getElementById('carrierDiv').style.display = 'none';
    document.getElementById('dateDiv').style.display = 'none';

    var searchBy = combo.options[combo.selectedIndex].value;

    if( searchBy!='getall' )
    {
            document.getElementById(searchBy+'Div').style.display = 'block';
            //We now allow searching by date in all cases
            document.getElementById('dateDiv').style.display = 'block';
    }
}


//***********************************************************************************************
// function populateStartEndDate()
//***********************************************************************************************
function populateStartEndDate()
{
	//get todays date
	var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate()+1);

	//parse and retrieve the day, month, and year
	var month = tomorrow.getMonth() + 1;
	var year = tomorrow.getFullYear();
	var day = tomorrow.getDate();

	//format the delivery date in yyyymmdd format
	var	formattedDate = "";
	if (month < 10)
		formattedDate += '0';
	formattedDate += month.toString();
	formattedDate += '/';
	if (day < 10)
		formattedDate += '0';
	formattedDate += day.toString();
	formattedDate += '/';
	formattedDate += year.toString();


	document.getElementById("startDateAdd").value = formattedDate;
  document.getElementById("startDateAdd").disabled= "true";

	document.getElementById("endDateAdd").value = formattedDate;
  document.getElementById("endDateAdd").disabled= "true";


	document.getElementById("dateSearch").disabled= "true";
}


//***********************************************************************************************
// function removeOption(e)
//***********************************************************************************************
function removeOption(e)
{
	e.options.length = 0;
	var newOpt1 = new Option(newText, newValue);
	theSel.options[0] = newOpt1;
	theSel.selectedIndex = 0;
}


onloads.push( addFTDDOMEvents );
//***********************************************************************************************
// function addFTDDOMEvents()
//***********************************************************************************************
function addFTDDOMEvents()
{
	new FTD_DOM.FocusElement( 'searchBy', borderFocusColor, 'searchByLabel', labelFocusColor );
	new FTD_DOM.FocusElement( 'vendorSearch', borderFocusColor, 'vendorSearchLabel', labelFocusColor );
	new FTD_DOM.FocusElement( 'productSearch', borderFocusColor, 'productSearchLabel', labelFocusColor );
	new FTD_DOM.FocusElement( 'carrierSearch', borderFocusColor, 'carrierSearchLabel', labelFocusColor );
	new FTD_DOM.FocusElement( 'dateSearch', borderFocusColor, 'dateSearchLabel', labelFocusColor );
	new FTD_DOM.FocusElement( 'vendorAdd', borderFocusColor, 'vendorAddLabel', labelFocusColor );
	new FTD_DOM.FocusElement( 'productAdd', borderFocusColor, 'productAddLabel', labelFocusColor );
	new FTD_DOM.FocusElement( 'carrierAdd', borderFocusColor, 'carrierAddLabel', labelFocusColor );
	new FTD_DOM.FocusElement( 'startDateAdd', borderFocusColor, 'startDateAddLabel', labelFocusColor );
	new FTD_DOM.FocusElement( 'endDateAdd', borderFocusColor, 'endDateAddLabel', labelFocusColor );
     	new FTD_DOM.FocusElement( 'sdsShipMethodAdd', borderFocusColor, 'sdsShipMethodAddLabel', labelFocusColor );


	new FTD_DOM.FocusElement( 'dateSearchCalLink', borderFocusColor, 'dateSearchLabel', labelFocusColor );
	new FTD_DOM.FocusElement( 'startDateAddCalLink', borderFocusColor, 'startDateAddLabel', labelFocusColor );
	new FTD_DOM.FocusElement( 'endDateAddCalLink', borderFocusColor, 'endDateAddLabel', labelFocusColor );
}

//***********************************************************************************************
// function checkAll()
//***********************************************************************************************

function checkAll()
{
    cursor_wait();
    var total = document.getElementById("overrideCount").value;
    for (var i= 0 ; i < total; i++)
    {
        checkBox = document.getElementById("check_" + i);
        if (checkBox != null && !checkBox.checked)
        {
            checkBox.checked = true;
        }
    }
    cursor_clear();
}
//***********************************************************************************************
// function uncheckAll()
//***********************************************************************************************
  
function uncheckAll()
{
    cursor_wait();
    var total = document.getElementById("overrideCount").value;
    for (var i= 0 ; i < total; i++)
    {
        checkBox = document.getElementById("check_" + i);
        if (checkBox != null && checkBox.checked)
        {
            checkBox.checked = false;
        }
    }
    cursor_clear();
}

//***************************************************************************************************
// clearField()
//***************************************************************************************************/
function clearField(field)
{
  document.getElementById(field).value = "";
}


//***************************************************************************************************
// cursor_wait()
//***************************************************************************************************/

function cursor_wait() {
document.body.style.cursor = 'wait';
}


//***************************************************************************************************
// cursor_clear()
//***************************************************************************************************/

function cursor_clear() {
  document.body.style.cursor = 'default';
}

//***************************************************************************************************
// displayResults()
//***************************************************************************************************/

function displayResults() {
  document.getElementById('searchResultsDiv').style.display = 'block';
  document.getElementById("deleteButton").disabled = false;
  document.getElementById('blankSearchDiv').style.display = 'none';
}

//***************************************************************************************************
// hideResults()
//***************************************************************************************************/

function hideResults() 
{
  document.getElementById('searchResultsDiv').style.display = 'none';
  document.getElementById("deleteButton").disabled = true;
  document.getElementById('blankSearchDiv').style.display = 'block';
}

</script>

<html:form method="POST" action="/submitVendorProductOverride">
<jsp:include page="includes/security.jsp"/>
	<html:hidden property="mode" styleId="mode" />

	<!-- Search Criteria for Selected Overrides-->
	<table align="center" border="0" cellpadding="2" cellspacing="0">
		<caption/>
		<tbody>
			<tr>
				<td>
					<table border="0" cellpadding="2" cellspacing="0">
						<caption/>
						<tr>
							<td><label id="searchByLabel" for="searchBy">Search By</label></td>
							<td>
								<html:select property="searchBy" styleId="searchBy" onchange="setupSearch(this);" >
									<html:options collection="searchTypeListOptions" property="id" labelProperty="description"/>
								</html:select>
							</td>
						</tr>
					</table>
				</td>
				<td>
					<div id="vendorDiv">
						<table border="0" cellpadding="2" cellspacing="0">
							<caption/>
							<tr>
								<td><label id="vendorSearchLabel" for="vendorSearch">Vendor</label></td>
								<td>
									<html:select property="vendorSearch" styleId="vendorSearch">
										<html:options collection="vendorListOptions" property="vendorId" labelProperty="vendorName"/>
									</html:select>
								</td>
							</tr>
						</table>
					</div>
				</td>
				<td>
					<div id="productDiv">
						<table border="0" cellpadding="2" cellspacing="0">
							<caption/>
							<tr>
								<td><label id="productSearchLabel" for="productSearch">Product Id</label></td>
								<td><html:text property="productSearch" styleId="productSearch" size="20" maxlength="20" /></td>
							</tr>
						</table>
					</div>
				</td>
				<td>
					<div id="carrierDiv">
						<table border="0" cellpadding="2" cellspacing="0">
							<caption/>
							<tr>
								<td><label id="carrierSearchLabel" for="carrierSearch">Carrier</label></td>
								<td>
									<html:select property="carrierSearch" styleId="carrierSearch">
										<html:options collection="carrierListOptions" property="id" labelProperty="description"/>
									</html:select>
								</td>
							</tr>
						</table>
					</div>
				</td>
				<td>
					<div id="dateDiv">
						<table border="0" cellpadding="2" cellspacing="0">
							<caption/>
							<tr>
								<td><label id="dateSearchLabel" for="dateSearch">Del Override Date</label></td>
								<td valign="middle">
									<html:text property="dateSearch" styleId="dateSearch" size="10" maxlength="10" />
								</td>
								<td valign="middle">
									<a id="dateSearchCalLink" href="javascript:dateSearchCal.popup();"><img src="images/show-calendar.gif" align="middle" border="0" alt="Click here to select the delivery override date"></a>
									<img src="images/removed.gif" align="middle" border="0" alt="Click here to clear the delivery override date" onclick="clearField('dateSearch');">
								</td>
							</tr>
						</table>
					</div>
				</td>
				<td>
					&nbsp;&nbsp;<input type="button" accesskey="R" id="refreshButton" value="(R)efresh" onclick="doSearch();"/>
				</td>
			</tr>
		</tbody>
	</table>


	<!--Overrides that currently exist-->
	<table align="center" style="border-color: #336699;" border="3">
               <caption style="background-color:#FFFFFF; color:#336699; font-weight:bold; text-align:center; padding-left: 5px;">Click the refresh button to view the latest data
              </caption>

		<tbody>
			<tr>
				<td>
					<div id="searchResultsDiv" style="overflow: auto; height: 325px">
						<table border="1">
							<caption style="background-color:#336699; color:#eeeeee; font-weight:bold; text-align:center; padding-left: 5px;">Selected Overrides</caption>
							<tbody>
								<tr bgcolor="#fffff">
   									<th width="<%=ProductMaintConstants.VPO_SEARCH_DATE_COLUMN_WIDTH%>">Del Override Date</th>
                                    					<th width="<%=ProductMaintConstants.VPO_SEARCH_CARRIER_COLUMN_WIDTH%>">Carrier</th>
                                      					<th width="<%=ProductMaintConstants.VPO_SEARCH_SDS_SHIP_METHOD_COLUMN_WIDTH%>">SDS Ship Method</th>
									<th width="<%=ProductMaintConstants.VPO_SEARCH_VENDOR_COLUMN_WIDTH%>">Vendor<input id="overrideCount" type="hidden" value="<%=searchResultsListOptions.size()%>"/></th>
									<th width="<%=ProductMaintConstants.VPO_SEARCH_PRODUCT_COLUMN_WIDTH%>">Product ID</th>
									<th><a href="javascript:checkAll()"><img src="images/all_check.gif" width="42" height="16" border="0" align="absmiddle" alt="Check All"></a>
                                                                                    <a href="javascript:uncheckAll()"><img src="images/all_uncheck.gif" width="42" height="16" border="0" align="absmiddle" alt="Uncheck All"></a></th>
								</tr>

								<logic:iterate id="element" name="searchResultsList" scope="request">
								<%
                                                                     String backColor = null;
                                                                     for(int i = 0; i < searchResultsListOptions.size(); i++)
                                                                     {
                                                                        VendorProductOverrideVO vpOverrideVO = (VendorProductOverrideVO)searchResultsListOptions.get(i);
                                                                        if (i%2 == 0)
                                                                                backColor = "#eeeeee";
                                                                        else
                                                                                backColor = "#fffff";
                                                                    %>
                                                                        <TR bgcolor="<%=backColor%>">
                                                                            <TD align="center"><%=vpOverrideVO.getDeliveryOverrideDateFormatted()%></TD>           
                                                                            <TD align="center"><%=vpOverrideVO.getCarrierId()%></TD>
                                                                            <TD align="center"><%=vpOverrideVO.getSdsShipMethod()%></TD>
                                                                            <TD align="center"><%=vpOverrideVO.getVendorId()%>&nbsp;-&nbsp;<%=vpOverrideVO.getVendorName()%></TD>
                                                                            <TD align="center"><%=vpOverrideVO.getProductId()%></TD>
                                                                            <TD align="center">
                                                                                <input id="check_<%=i%>" vendor="<%=vpOverrideVO.getVendorId()%>" product="<%=vpOverrideVO.getProductId()%>" carrier="<%=vpOverrideVO.getCarrierId()%>" date="<%=vpOverrideVO.getDeliveryOverrideDateFormatted()%>" shipmethod="<%=vpOverrideVO.getSdsShipMethod()%>" type="checkbox" />
                                                                            </TD>
                                                                        </TR>
                                                                    <%
                                                                    }
                                                                    %>
								</logic:iterate>
							</tbody>
						</table>
					</div>
                                        <div id="blankSearchDiv" style="overflow: auto; height: 325px">
						<table border="1">
							<caption style="background-color:#336699; color:#eeeeee; font-weight:bold; text-align:center; padding-left: 5px;">Selected Overrides</caption>
							<tbody>
								<tr bgcolor="#fffff">
   									<th width="<%=ProductMaintConstants.VPO_SEARCH_DATE_COLUMN_WIDTH%>">Del Override Date</th>
                                    					<th width="<%=ProductMaintConstants.VPO_SEARCH_CARRIER_COLUMN_WIDTH%>">Carrier</th>
                                      					<th width="<%=ProductMaintConstants.VPO_SEARCH_SDS_SHIP_METHOD_COLUMN_WIDTH%>">SDS Ship Method</th>
									<th width="<%=ProductMaintConstants.VPO_SEARCH_VENDOR_COLUMN_WIDTH%>">Vendor<input id="overrideCount" type="hidden" value="<%=searchResultsListOptions.size()%>"/></th>
									<th width="<%=ProductMaintConstants.VPO_SEARCH_PRODUCT_COLUMN_WIDTH%>">Product ID</th>
									<th><img src="images/all_check.gif" width="42" height="16" border="0" align="absmiddle" alt="Check All">
                                                                            <img src="images/all_uncheck.gif" width="42" height="16" border="0" align="absmiddle" alt="Uncheck All">
                                                                        </th>
								</tr>
							</tbody>
						</table>
					</div>
				</td>
			</tr>
			<tr>
				<td align="center">
					<input type="button" id="deleteButton" accesskey="D" value="(D)elete Selected" onclick="deleteOverrides();"/>
				</td>
			</tr>
		</tbody>
	</table>


	<!-- Separator-->
	<table width="98%" align="center">
		<caption/>
		<tr>
			<td><hr/></td>
		</tr>
	</table>


	<!-- Controls to add Vendor/Product for Overrides-->
	<table align="center" style="border-color: #336699;" border="3">
		<caption/>
		<tbody>
			<tr>
				<th><label id="startDateAddLabel" for="startDateAdd">&nbspDel Override Start Date&nbsp</label></th>
                                <th><label id="endDateAddLabel" for="endDateAdd">&nbspDel Override End Date&nbsp</label></th>
                                <th><label id="carrierAddLabel" for="carrierAdd">Carrier</label></th>
                                <th><label id="sdsShipMethodAddLabel" for="sdsShipMethodAdd">SDS Ship Method</label></th>
				<th><label id="vendorAddLabel" for="vendorAdd">Vendor</label></th>
				<th><label id="productAddLabel" for="productAdd">Product ID</label></th>	
			</tr>
			<tr>
				<td valign="middle" align="center">
					<table border="0" cellpadding="0" cellspacing="0">
						<caption/>
						<tr>
							<td valign="middle">
								<html:text property="startDateAdd" styleId="startDateAdd" size="10" maxlength="10" />
							</td>
							<td valign="middle">
								&nbsp;<a id="startDateAddCalLink" href="javascript:startDateAddCal.popup(); " ><img src="images/show-calendar.gif" border="0" align="middle" alt="Click here to select the start delivery override date"/></a>
                                                        </td>
						</tr>
					</table>
				</td>
				<td valign="middle" align="center">
					<table border="0" cellpadding="0" cellspacing="0">
						<caption/>
						<tr>
							<td valign="middle">
								<html:text property="endDateAdd" styleId="endDateAdd" size="10" maxlength="10" />
							</td>
							<td valign="middle">
								&nbsp;<a id="endDateAddCalLink" href="javascript:endDateAddCal.popup();"><img src="images/show-calendar.gif" border="0" align="middle" alt="Click here to select the ending delivery override date"/></a>
							</td>
						</tr>
					</table>
				</td>
				<td align="center">
					<html:select property="carrierAdd" styleId="carrierAdd" onchange="getSdsShipMethodsForCarrier();">
						<html:options collection="carrierListOptions" property="id" labelProperty="description"/>
					</html:select>
				</td>
                                <td align="center">
					<div id="sdsShipMethodAddDiv">
						<select name="sdsShipMethodAdd" id="sdsShipMethodAdd" multiple="true" size="4" >
							<option selected="true">Initializing...</option>
						</select>
					</div>
                                </td>
				<td align="center">
					<html:select property="vendorAdd" styleId="vendorAdd" onchange="getProductsForVendor();">
						<html:options collection="vendorListOptions" property="vendorId" labelProperty="vendorName"/>
					</html:select>
				</td>
				<td align="center">
					<div id="productAddDiv">
						<select name="productAdd" id="productAdd">
							<option selected="true">Initializing...</option>
						</select>
					</div>
				</td>


			</tr>
			<tr>
				<td colspan="6" align="center">
					<input id="addButton" type="button" accesskey="A" value="(A)dd" onclick="doAddOverrides();"/>
				</td>
			</tr>
		</tbody>
	</table>
</html:form>

<script language="JavaScript" type="text/javascript">
var dateSearchCal = new calendar2(document.getElementById('dateSearch'));
                            dateSearchCal.year_scroll = true;
                            dateSearchCal.time_comp = false;

var startDateAddCal = new calendar2(document.getElementById('startDateAdd'));
                            startDateAddCal.year_scroll = true;
                            startDateAddCal.time_comp = false;


var endDateAddCal = new calendar2(document.getElementById('endDateAdd'));
                            endDateAddCal.year_scroll = true;
                            endDateAddCal.time_comp = false;
</script>
