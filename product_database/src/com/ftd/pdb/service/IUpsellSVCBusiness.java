package com.ftd.pdb.service;

import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBSystemException;
import com.ftd.pdb.common.valobjs.UpsellMasterVO;

import java.sql.Connection;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;


/**
* The interface of the Upsell Maintenance service layer
*/
public interface IUpsellSVCBusiness 
{
    /**
     * Returns an array of Upsell VOs
     * @return ArrayList
     * @throws PDBApplicationException, PDBSystemException
     **/
     public List getUpsellList()
          throws PDBApplicationException, PDBSystemException;

   /**
    * Returns the passed in Master SKU VO
    * @param sku record id
    * @return UpsellMasterVO
    * @throws PDBApplicationException, PDBSystemException
    **/
    public UpsellMasterVO getMasterSKU(String sku)
         throws PDBApplicationException, PDBSystemException;        

   /**
    * Update or insert master sku
    * @param masterVO to update
    * @param saveNovatorFeeds novator sites to update
    * @return none
    * @throws PDBApplicationException, PDBSystemException
    **/
    public void updateMasterSKU(UpsellMasterVO masterVO,List<String> saveNovatorFeeds)
         throws PDBApplicationException, PDBSystemException;        

   /**
    * Get a list of sku's from the request object
    * @request server request
    * @return List of sku's from the request object
    * @throws PDBApplicationException, PDBSystemException
    **/
    public List getSKUsFromRequest(HttpServletRequest request)
         throws PDBApplicationException, PDBSystemException;                

  /**
   * Update or insert master sku
   * @param productid filter
   * @return List of products that are returned
   * @throws PDBApplicationException, PDBSystemException
   **/
   public List getUpsellDetailByID(String productid)
        throws PDBApplicationException, PDBSystemException;                

    /**
     * Determine if a master sku exists in upsell master
     * @param sku to validate
     * @return true if the master sku exists
     */
    public boolean validateMasterSku(String sku) 
        throws PDBApplicationException, PDBSystemException;
}