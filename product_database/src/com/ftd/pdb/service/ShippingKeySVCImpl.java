package com.ftd.pdb.service;

import java.util.List;
import java.util.Map;

import com.ftd.pdb.businessobject.IShippingKeyBO;
import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBSystemException;
import com.ftd.pdb.common.utilities.DBUtilities;
import com.ftd.pdb.common.valobjs.ShippingKeyVO;

import java.sql.Connection;

/**
* The implementation of the Shipping Key service layer
*/
public class ShippingKeySVCImpl extends PDBServiceObject implements IShippingKeySVCService
{
    /* Spring managed resources */
     private IShippingKeyBO shippingKeyBO;
     /* end Spring managed resources */

    /**
    *   ShippingKeySVCImpl constructor
    */
    public ShippingKeySVCImpl() 
    {
        super("com.ftd.pdb.service.ShippingKeySVCImpl");
    }

   /**
   * Returns an array of ShippingKeyVOs
   *
   * @return array of ShippingKeyVOs
   * @throws PDBApplicationException, PDBSystemException
   **/
    public List getShippingKeyList()
        throws PDBApplicationException, PDBSystemException
    {
        Connection conn = null;
        List list = null;
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            list = shippingKeyBO.getShippingKeyList(conn);    
        } finally { 
            DBUtilities.close(conn);
        }
        return list;
    }
    
   /**
     * return a ShippingKeyVO for a given shippingID
     * @param  shippingID the selectecd id for the shipping key.
     * @return ShippingKeyVO
     * @throws PDBApplicationException, PDBSystemException
    **/
    public ShippingKeyVO getShippingKey (String shippingID )
        throws PDBApplicationException, PDBSystemException
    {
        Connection conn = null;
        ShippingKeyVO vo = null;
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            vo = shippingKeyBO.getShippingKey(conn,shippingID);    
        } finally { 
            DBUtilities.close(conn);
        }
        return vo;
    }

      /**
     * cascading deleting a row in Shipping_key table and rows in any table that has a foreign key reference.
     * @param shippingKeyVO the shipping key to be removed from database.
     * @param novatorMap
     * @return Boolean true if succeed, false if failed.
     * @throws PDBApplicationException, PDBSystemException
    **/
    public boolean removeShippingKey (ShippingKeyVO shippingKeyVO, Map novatorMap )
        throws PDBApplicationException, PDBSystemException
    {
        Connection conn = null;
        boolean retval;
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            retval = shippingKeyBO.removeShippingKey(conn, shippingKeyVO, novatorMap);    
        } finally { 
            DBUtilities.close(conn);
        }
        return retval;
    }

   /**
     * Insert a row in Shipping_key table and rows in Shipping_key_details table.
     * @param shippingKeyVO the selectecd id for the shipping key to be removed from database.
     * @param novatorMap
     * @return Boolean true if succeed, false if failed.
     * @throws PDBApplicationException, PDBSystemException
    **/
    public boolean insertShippingKey (ShippingKeyVO shippingKeyVO, Map novatorMap )
        throws PDBApplicationException, PDBSystemException
    {
        Connection conn = null;
        boolean retval;
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            retval = shippingKeyBO.insertShippingKey(conn, shippingKeyVO, novatorMap);    
        } finally { 
            DBUtilities.close(conn);
        }
        return retval;
    }

    /**
     * Update a row in Shipping_key table and rows in Shipping_key_details table.
     * @param shippingKeyVO the selectecd id for the shipping key to be udpated from database.
     * @param novatorMap     
     * @return Boolean true if succeed, false if failed.
     * @throws PDBApplicationException, PDBSystemException
    **/
    public boolean updateShippingKey (ShippingKeyVO shippingKeyVO, Map novatorMap )
        throws PDBApplicationException, PDBSystemException
    {
        Connection conn = null;
        boolean retval;
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            retval = shippingKeyBO.updateShippingKey(conn, shippingKeyVO, novatorMap);    
        } finally { 
            DBUtilities.close(conn);
        }
        return retval;
    }

    public void setShippingKeyBO(IShippingKeyBO shippingKeyBO) {
        this.shippingKeyBO = shippingKeyBO;
    }

    public IShippingKeyBO getShippingKeyBO() {
        return shippingKeyBO;
    }
}
