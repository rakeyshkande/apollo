package com.ftd.pdb.service;

import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.ftd.osp.utilities.VendorProductAvailabilityUtility;
import com.ftd.osp.utilities.vo.InventoryVO;
import com.ftd.pdb.businessobject.IHolidayPricingBO;
import com.ftd.pdb.businessobject.IOccasionMaintenanceBO;
import com.ftd.pdb.businessobject.IProductBO;
import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBSystemException;
import com.ftd.pdb.common.utilities.DBUtilities;
import com.ftd.pdb.common.valobjs.HolidayPricingVO;
import com.ftd.pdb.common.valobjs.MassEditFileStatusVO;
import com.ftd.pdb.common.valobjs.ProductVO;
import com.ftd.pdb.common.valobjs.VendorProductOverrideVO;


/**
 * The implementation of the Product Maintenance service layer
 */
public class ProductMaintSVCImpl extends PDBServiceObject implements IProductMaintSVCBusiness {
    /* Spring managed resources */
    IProductBO productBO;
    IHolidayPricingBO holidayBO;
    IOccasionMaintenanceBO occasionBO;
    IDeliveryZipSVC deliveryBO;
    /* end Spring managed resources */

    /**
    *   ProductSVCImpl constructor
    */
    public ProductMaintSVCImpl() {
        super("com.ftd.pdb.service.IProductMaintSVCBusiness");
    }

    /**
   * Returns an array of ProductVOs
   *
   * @return ArrayList
   * @throws PDBApplicationException, PDBSystemException
   **/
    public List getProductList() throws PDBApplicationException, 
                                        PDBSystemException {
        Connection conn = null;
        List list = null;

        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            list = productBO.getProductList(conn);
        } finally {
            DBUtilities.close(conn);
        }
        return list;
    }

    /**
   * Returns an array of ProductVOs based on filtering criteria
   *
   * @return ArrayList
   * @throws PDBApplicationException, PDBSystemException
   **/
    public List getProductListAdvanced(String avail, String unavail, 
                                       String dateRestrict, 
                                       String onHold) throws PDBApplicationException, 
                                                             PDBSystemException {
        Connection conn = null;
        List list = null;

        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            list = 
productBO.getProductListAdvanced(conn, avail, unavail, dateRestrict, onHold);
        } finally {
            DBUtilities.close(conn);
        }
        return list;
    }

    /**
   * Returns an array of ProductVOs which have holiday pricing
   *
   * @return ArrayList
   * @throws PDBApplicationException, PDBSystemException
   **/
    public List getProductListWithHolidayPricing() throws PDBApplicationException, 
                                                          PDBSystemException {
        Connection conn = null;
        List list = null;

        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            list = productBO.getProductListWithHolidayPricing(conn);
        } finally {
            DBUtilities.close(conn);
        }
        return list;
    }

    /**
     * Removes holiday pricing for the passed in product
     * @param product id
     * @exception String The exception description.
     */
    public void removeHolidayPricing(String product) throws PDBApplicationException, 
                                                            PDBSystemException {
        Connection conn = null;

        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            productBO.removeHolidayPricing(conn, product);
        } finally {
            DBUtilities.close(conn);
        }
    }

    /**
    * Returns a ProductVO
    *
    * @return ProductVO
    * @throws PDBApplicationException, PDBSystemException
    **/
    public ProductVO getProduct(String productId) throws PDBApplicationException, 
                                                         PDBSystemException {
        Connection conn = null;
        ProductVO product = null;

        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            product = productBO.getProduct(conn, productId, null);
        } finally {
            DBUtilities.close(conn);
        }
        return product;
    }

    /**
    * Returns a ProductVO
    *
    * @return ProductVO
    * @throws PDBApplicationException, PDBSystemException
    **/
    public ProductVO getProductByNovatorId(String novatorId) throws PDBApplicationException, 
                                                                    PDBSystemException {
        Connection conn = null;
        ProductVO product = null;

        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            product = productBO.getProduct(conn, null, novatorId);
        } finally {
            DBUtilities.close(conn);
        }
        return product;
    }

    /**
   * Returns a Holiday Pricing VO with data found in the database
   *
    * @return HolidayPricingVO
   * @throws PDBApplicationException
   **/
    public HolidayPricingVO getHolidayPricing() throws PDBApplicationException, 
                                                       PDBSystemException {
        Connection conn = null;
        HolidayPricingVO pricingVO = null;

        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            pricingVO = holidayBO.getHolidayPricing(conn);
        } finally {
            DBUtilities.close(conn);
        }
        return pricingVO;
    }


    /**
   * Sets holiday pricing based on passed in data
   *
   * @param holidayVO
   * @param novatorMap
   * @throws PDBApplicationException
   **/
    public void setHolidayPricing(HolidayPricingVO holidayVO, 
                                  Map novatorMap) throws PDBApplicationException, 
                                                         PDBSystemException {
        Connection conn = null;

        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            holidayBO.setHolidayPricing(conn, holidayVO, novatorMap);
        } finally {
            DBUtilities.close(conn);
        }
    }

    /**
   * Returns an array list of selected cards for the given occasion
   *
    * @return ArrayList
   * @throws PDBApplicationException
   **/
    public List getOccasions() throws PDBApplicationException, 
                                      PDBSystemException {
        Connection conn = null;
        List list = null;

        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            list = occasionBO.getOccasions(conn);
        } finally {
            DBUtilities.close(conn);
        }
        return list;
    }

    /**
   * Returns list of available cards for passed in occasion
   *
   * @return HolidayPricingVO
   * @throws PDBApplicationException
   **/
    public List getAvailableCards(int occasion) throws PDBApplicationException, 
                                                       PDBSystemException {
        Connection conn = null;
        List list = null;

        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            list = occasionBO.getAvailableCards(conn, occasion);
        } finally {
            DBUtilities.close(conn);
        }
        return list;
    }

    /**
   * Returns list of selected cards for passed in occasion
   *
   * @return HolidayPricingVO
   * @throws PDBApplicationException
   **/
    public List getSelectedCards(int occasion) throws PDBApplicationException, 
                                                      PDBSystemException {
        Connection conn = null;
        List list = null;

        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            list = occasionBO.getSelectedCards(conn, occasion);
        } finally {
            DBUtilities.close(conn);
        }
        return list;
    }


    /**
   * Updates the occasion card mapping, assigning all the passed
   * in cards to the passed in occasion, all other cards will be
   * removed from the mapping.
   *
   * @param occasion
   * @param cards
   * @return void
   * @throws PDBApplicationException
   **/
    public void updateOccasionCards(int occasion, 
                                    String[] cards) throws PDBApplicationException, 
                                                           PDBSystemException {
        Connection conn = null;

        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            occasionBO.updateOccasionCards(conn, occasion, cards);
        } finally {
            DBUtilities.close(conn);
        }
    }

    /**
   * Updates or Creates a product in the database
   * @param productVO that will be created or updated
   * @param novatorEnvKeys
   * @throws PDBApplicationException, PDBSystemException
   **/
    public void setProduct(ProductVO productVO, List novatorEnvKeys) throws PDBApplicationException, 
                                                        PDBSystemException {
        Connection conn = null;

        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            productBO.setProduct(conn, productVO, novatorEnvKeys);
        } finally {
            DBUtilities.close(conn);
        }
    }

    /**
   * Returns a list of subcodes with passed in subcode id
   * @throws PDBSystemException
   **/
    public List getSubcodesByID(String id) throws PDBApplicationException, 
                                                  PDBSystemException {
        Connection conn = null;
        List list = null;

        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            list = productBO.getSubCodesByID(conn, id);
        } finally {
            DBUtilities.close(conn);
        }
        return list;
    }

    /**
     * Returns an array of product ids that have been updated since the passed in date
     * @param lastUpdateDate return any products that have been updated since this date
     * @return array of product ids
     * @exception PDBSystemException
     * @exception PDBApplicationException
     */
    public List getProductsUpdatedSince(java.util.Date lastUpdateDate) throws PDBSystemException, 
                                                                              PDBApplicationException {
        Connection conn = null;
        List list = null;

        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            list = productBO.getProductsUpdatedSince(conn, lastUpdateDate);
        } finally {
            DBUtilities.close(conn);
        }
        return list;
    }

    /**
     * Send the product passed to Novator
     * @param product record to be sent to novator
     * @param overwriteSentTo If true overwrite any existing "sent_to_novator" values.
     * @exception PDBSystemException
     * @exception PDBApplicationException
     */
    public void sendProductToNovator(ProductVO product, 
                                     List novatorMap, boolean overwriteSentTo) throws PDBSystemException, 
                                                            PDBApplicationException {
        Connection conn = null;
        try{
            conn = this.getResourceProvider().getDatabaseConnection();
            productBO.sendProductToNovator(conn, product, novatorMap, overwriteSentTo);
        } finally {
            DBUtilities.close(conn);
        }
        
        
    }

    /**
     * Validates the passed in product id
     * @param productId to validate
     * @return true if the product id is a valid product id, novator id, or subsku
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public boolean validateProductId(String productId) throws PDBSystemException, 
                                                              PDBApplicationException {

        Connection conn = null;
        boolean retval = false;

        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            retval = productBO.validateProductId(conn, productId);
        } finally {
            DBUtilities.close(conn);
        }
        return retval;
    }
    
    /**
     * Get a list of lists of VendorProductVOs for vendors and their skus for the passed
     * in product/sku id
     * @param productSkuId to retrieve
     * @return List of VendorProductVOs for the passed in prduct/sku id
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public List getVendorsForProduct(String productSkuId) 
        throws PDBSystemException, PDBApplicationException {

            Connection conn = null;
            List retval = null;

            try {
                conn = this.getResourceProvider().getDatabaseConnection();
                retval = productBO.getVendorsForProduct(conn,productSkuId);
            } finally {
                DBUtilities.close(conn);
            }
            return retval;
        }
        
    /**
     * Returns a list of products for the passed vendorId
     * @param vendorId filter
     * @return list of Strings containing the product Ids
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public List getVendorProducts(String vendorId) throws PDBSystemException, PDBApplicationException {

        Connection conn = null;
        List retval = null;

        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            retval = productBO.getVendorProducts(conn,vendorId);
        } finally {
            DBUtilities.close(conn);
        }
        return retval;
    }
    
  /**
   * Returns a list of SDS ship methods for the passed carrier ID
   * @param carrierID filter
   * @return list of Strings containing the SDS ship methods
   * @throws PDBSystemException
   * @throws PDBApplicationException
   */
    public List getCarrierSdsShipMethods(String carrierId) throws PDBSystemException, PDBApplicationException {
  
        Connection conn = null;
        List retval = null;
  
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            retval = productBO.getCarrierSdsShipMethods(conn,carrierId);
        } finally {
            DBUtilities.close(conn);
        }
        return retval;
    }
    
    
    
    
    /**
     * Get a list of VendorProductOverrideVO objects, filtered on the passed variables
     * @param vendorId filter
     * @param productId filter
     * @param overrideDate filter
     * @param carrierId filter
     * @return
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public List getVPOverrides(String vendorId, String productId, Date overrideDate, String carrierId) 
        throws PDBSystemException, PDBApplicationException {

        Connection conn = null;
        List retval = null;

        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            retval = productBO.getVPOverrides(conn,vendorId,productId,overrideDate,carrierId);
        } finally {
            DBUtilities.close(conn);
        }
        return retval;
    }
            
    /**
     * Updates the values of the passed VendorProductOverrideVO in the VENDOR_PRODUCT_OVERRIDE table
     * @param vpoVO data to update
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public void updateVPOverride(VendorProductOverrideVO vpoVO) throws PDBSystemException, PDBApplicationException {

        Connection conn = null;
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            productBO.updateVPOverride(conn,vpoVO);
        } finally {
            DBUtilities.close(conn);
        }
    }
    
    /**
     * Delete the values of the passed VendorProductOverrideVO in the VENDOR_PRODUCT_OVERRIDE table
     * @param vpoVO data to delete
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public void deleteVPOverride(VendorProductOverrideVO vpoVO) throws PDBSystemException, PDBApplicationException {

        Connection conn = null;
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            productBO.deleteVPOverride(conn,vpoVO);
        } finally {
            DBUtilities.close(conn);
        }
    }

    /**
     * Checks to see if id exists in FTD_APPS.VENDOR_PRODUCTS
     * @param productId to check
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public String existsInVendorProduct(String productId) throws PDBSystemException, PDBApplicationException {

        Connection conn = null;
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            return productBO.existsInVendorProduct(conn,productId);
        } finally {
            DBUtilities.close(conn);
        }
    }

    /**
     * Determine if the vender has a shipping account for the passed in partner
     * @param vendorId filter
     * @param partnerId filter
     * @return true if the account exist else false
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public boolean hasPartnerShipAccount(String vendorId, String partnerId) throws PDBSystemException, PDBApplicationException {

        Connection conn = null;
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            return productBO.hasPartnerShipAccount(conn,vendorId,partnerId);
        } finally {
            DBUtilities.close(conn);
        }
    }
    
    /**
     * Validates product content that cannot be validated on the form.
     * @param product
     * @return Map of error field and error messages
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public String validateWebsites(String websites) throws PDBSystemException, PDBApplicationException {
    
        Connection conn = null;
        String invalidSourceCodes = "";
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            // Validate source codes (websites) to make sure all source codes are valid. If not, add to list.
            List invalidSourceList = this.productBO.validateSourceCode(conn, websites);
            if(invalidSourceList != null) {

                for(int i = 0; i < invalidSourceList.size(); i++) {
                    invalidSourceCodes += invalidSourceList.get(i);
                    if(i != (invalidSourceList.size()-1)) {
                        invalidSourceCodes += " ";
                    }
                }
                
            }
            
        } finally {
            DBUtilities.close(conn);
        }
        return invalidSourceCodes;

    }

    public void setProductBO(IProductBO productBO) {
        this.productBO = productBO;
    }

    public IProductBO getProductBO() {
        return productBO;
    }

    public void setHolidayBO(IHolidayPricingBO holidayBO) {
        this.holidayBO = holidayBO;
    }

    public IHolidayPricingBO getHolidayBO() {
        return holidayBO;
    }

    public void setOccasionBO(IOccasionMaintenanceBO occasionBO) {
        this.occasionBO = occasionBO;
    }

    public IOccasionMaintenanceBO getOccasionBO() {
        return occasionBO;
    }

    public void setDeliveryBO(IDeliveryZipSVC deliveryBO) {
        this.deliveryBO = deliveryBO;
    }

    public IDeliveryZipSVC getDeliveryBO() {
        return deliveryBO;
    }


	/**
	 * Method to check if the given product has inventory.
	 * @param productId
	 * @return true if product has inventory
	 * throws  PDBSystemException, PDBApplicationException
	 */
	public boolean hasInventoryRecord(String productId)	throws PDBSystemException, PDBApplicationException {
		
		InventoryVO inv = null;
		Connection conn = null;
		
		try {
			conn = this.getResourceProvider().getDatabaseConnection();
			inv = VendorProductAvailabilityUtility.getProductInventory(
					productId, conn);
			if (inv != null && inv.getOnHand() > inv.getShutdownThreshold())
				return true;
		} catch (Exception e) {
			logger.error("Unable to determine the availability of the product : " + productId + ", Will not allow this product to be made available.");			
		} finally {
			DBUtilities.close(conn);
		}
		return false;
	}
	
	/**
     * Creates the Product Maintenance Mass Edit Spreadsheet
     * 
     * @param search type 'M' - by master sku or 'P' by product id
     * @param String array containing either 1-5 master skus or 1-5 product ids
     * @return XSSFWorkbook (.xlsx file containing the spreadsheet data)
     */
    public XSSFWorkbook createProductMassEditSpreadsheet(String searchType, String[] searchItems) 
        throws Exception {
		Connection conn = null;
		XSSFWorkbook wb = null;
		try {
			conn = this.getResourceProvider().getDatabaseConnection();
			// get the product list matching the query criteria
			List<ProductVO> productList = 
				productBO.getProductMassEditList(conn, searchType, searchItems); 
			if (productList != null && !productList.isEmpty()) {
				wb = productBO.createProductMassEditSpreadsheet(productList,false);
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw e;
			
		}
		finally {
			DBUtilities.close(conn);
		}	
    	return wb;
    	
    }
 
    public void uploadProductMassEditSpreadSheet(String fileName, String user, InputStream pdbMassEditFileIS) throws Exception {
    	Connection conn = null;
    	try {
    		conn = this.getResourceProvider().getDatabaseConnection();
    		productBO.uploadProductMassEditSpreadSheet(conn, fileName, user, pdbMassEditFileIS);
    	}
    	catch(Exception e) {
			e.printStackTrace();
			throw e;
		}
		finally {
			DBUtilities.close(conn);
		}	
    }
    
    /**
	 * Gets the records from the MASS_EDIT_HEADER and MASS_EDIT_DETAIL to show the upload
	 * status table on the PDB Mass Edit screen.
	 * 
	 * @param conn
	 * @return a HashMap containing the massEditDirectory path and a 
	 *         List of MassEditFileStatusVO objects.
	 * @throws Exception
	 */
    public HashMap<String, Object>getMassEditFileStatus() throws Exception {
    	Connection conn = null;
    	HashMap<String, Object> pdbHashMap = new HashMap<String, Object>();
    	try {
    		conn = this.getResourceProvider().getDatabaseConnection();
    		pdbHashMap = productBO.getMassEditFileStatus(conn);
    	}
    	catch(Exception e) {
			e.printStackTrace();
			throw e;
		}
		finally {
			DBUtilities.close(conn);
		}
    	
        return pdbHashMap;
      
    } 
    /**
     * Returns an array of ProductVOs based on filtering criteria     *
     * @return ArrayList
     * @throws PDBApplicationException, PDBSystemException
     **/
      public List getPQuadProductList(String pquadproductId) throws PDBApplicationException,PDBSystemException 
      {
          List list = null;
          Connection conn=null;
          try {
              conn = this.getResourceProvider().getDatabaseConnection();
             list =productBO.getPQuadProductList(conn,pquadproductId);
          } finally {
              DBUtilities.close(conn);
          }
          return list;
      }
      
}
