package com.ftd.pdb.service;

import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBSystemException;
import com.ftd.pdb.common.valobjs.HolidayPricingVO;
import com.ftd.pdb.common.valobjs.MassEditFileStatusVO;
import com.ftd.pdb.common.valobjs.ProductVO;
import com.ftd.pdb.common.valobjs.VendorProductOverrideVO;


/**
* The interface of the Product Maintenance service layer
*/
public interface IProductMaintSVCBusiness
{
  
  /**
   * Returns an array of ProductVOs
   *
   * @return array of products
   * @throws PDBApplicationException, PDBSystemException
   **/
   public List getProductList()
        throws PDBApplicationException, PDBSystemException;

  /**
   * Returns an array of ProductVOs based on filtering criteria
   *
   * @return array of products
   * @throws PDBApplicationException, PDBSystemException
   **/
   public List getProductListAdvanced(String avail, String unavail, String dateRestrict, String onHold)
        throws PDBApplicationException, PDBSystemException;

  /**
   * Returns an array of ProductVOs which have holiday pricing
   *
   * @return array of products
   * @throws PDBApplicationException, PDBSystemException
   **/
   public List getProductListWithHolidayPricing()
        throws PDBApplicationException, PDBSystemException;

    /**
     * Removes holiday pricing for the passed in product
     * @param product id
     * @exception String The exception description.
     */
  public void removeHolidayPricing(String product)
        throws PDBApplicationException, PDBSystemException;
        
    /**
    * Returns a ProductVO
    *
    * @return ProductVO
    * @throws PDBApplicationException, PDBSystemException
    **/
    public ProductVO getProduct(String productId) throws PDBApplicationException, PDBSystemException;

    /**
    * Returns a ProductVO
    *
    * @return ProductVO
    * @throws PDBApplicationException, PDBSystemException
    **/
    public ProductVO getProductByNovatorId(String productId) throws PDBApplicationException, PDBSystemException;

  /**
   * Returns a Holiday Pricing VO with data found in the database
   *
    * @return HolidayPricingVO
   * @throws PDBApplicationException
   **/
    public HolidayPricingVO getHolidayPricing() 
        throws PDBApplicationException, PDBSystemException;

  /**
   * Sets holiday pricing based on passed in data
   *
   * @param holidayVO
   * @param novatorMap
   * @throws PDBApplicationException
   **/
    public void setHolidayPricing(HolidayPricingVO holidayVO, Map novatorMap) 
        throws PDBApplicationException, PDBSystemException;        

  /**
   * Returns an array list of occasion vo's
   *
    * @return ArrayList of Occasion VOs
   * @throws PDBApplicationException
   **/
    public List getOccasions() 
        throws PDBApplicationException, PDBSystemException;

  /**
   * Returns an array list of available cards for the given occasion
   *
    * @return ArrayList 
   * @throws PDBApplicationException
   **/
    public List getAvailableCards(int occasion) 
        throws PDBApplicationException, PDBSystemException;

  /**
   * Returns an array list of selected cards for the given occasion
   *
    * @return ArrayList 
   * @throws PDBApplicationException
   **/
    public List getSelectedCards(int occasion) 
        throws PDBApplicationException, PDBSystemException;

  /**
   * Updates the occasion card mapping, assigning all the passed
   * in cards to the passed in occasion, all other cards will be
   * removed from the mapping.
   *
   * @param occasion
   * @param cards
   * @return void 
   * @throws PDBApplicationException
   **/
    public void updateOccasionCards(int occasion, String[] cards) 
        throws PDBApplicationException, PDBSystemException;

  /**
   * Updates or Creates a product in the database
   * @param productVO that will be created or updated
   * @param novatorEnvKeys
   * @throws PDBApplicationException, PDBSystemException
   **/
    public void setProduct(ProductVO productVO, List novatorEnvKeys) 
        throws PDBApplicationException, PDBSystemException;                

 /**
   * Returns a list of subcodes with passed in subcode id
   * @throws PDBSystemException
   **/
    public List getSubcodesByID(String id) throws PDBSystemException, PDBApplicationException;   

    /**
     * Returns an array of product ids that have been updated since the passed in date
     * @param lastUpdateDate return any products that have been updated since this date
     * @return array of product ids
     * @exception PDBSystemException
     * @exception PDBApplicationException
     */
    public List getProductsUpdatedSince(java.util.Date lastUpdateDate) throws PDBSystemException, PDBApplicationException;         

    /**
     * Send the product passed to Novator
     * @param product record to be sent to novator
     * @param overwriteSentTo If true overwrite any existing "sent_to_novator" values.
     * @exception PDBSystemException
     * @exception PDBApplicationException
     */
    public void sendProductToNovator(ProductVO product, List novatorMap, boolean overwriteSentTo) throws PDBSystemException, PDBApplicationException;

    /**
     * Validates the passed in product id
     * @param productId to validate
     * @return true if the product id is a valid product id, novator id, or subsku
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public boolean validateProductId(String productId) throws PDBSystemException, PDBApplicationException;
    
    /**
     * Get a list of lists of VendorProductVOs for vendors and their skus for the passed
     * in product/sku id
     * @param productSkuId to retrieve
     * @return List of VendorProductVOs for the passed in prduct/sku id
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public List getVendorsForProduct(String productSkuId) throws PDBSystemException, PDBApplicationException;
    
    /**
     * Returns a list of products for the passed vendorId
     * @param vendorId filter
     * @return list of Strings containing the product Ids
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public List getVendorProducts(String vendorId) throws PDBSystemException, PDBApplicationException;
    
    /**
     * Returns a list of SDS ship methods for the passed carrier ID
     * @param carrierId filter
     * @return list of Strings containing the SDS ship methods
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public List getCarrierSdsShipMethods(String carrierId) throws PDBSystemException, PDBApplicationException;
    
    /**
     * Get a list of VendorProductOverrideVO objects, filtered on the passed variables
     * @param vendorId filter
     * @param productId filter
     * @param overrideDate filter
     * @param carrierId filter
     * @return
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public List getVPOverrides(String vendorId, String productId, Date overrideDate, String carrierId) 
        throws PDBSystemException, PDBApplicationException;
            
    /**
     * Updates the values of the passed VendorProductOverrideVO in the VENDOR_PRODUCT_OVERRIDE table
     * @param vpoVO data to update
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public void updateVPOverride(VendorProductOverrideVO vpoVO) throws PDBSystemException, PDBApplicationException;
    
    /**
     * Delete the values of the passed VendorProductOverrideVO in the VENDOR_PRODUCT_OVERRIDE table
     * @param vpoVO data to delete
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public void deleteVPOverride(VendorProductOverrideVO vpoVO) throws PDBSystemException, PDBApplicationException;
    
    /**
     * Checks to see if id exists in FTD_APPS.VENDOR_PRODUCTS
     * @param productId to check
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public String existsInVendorProduct(String productId) throws PDBSystemException, PDBApplicationException;

    /**
     * Determine if the vender has a shipping account for the passed in partner
     * @param vendorId filter
     * @param partnerId filter
     * @return true if the account exist else false
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public boolean hasPartnerShipAccount(String vendorId, String partnerId) throws PDBSystemException, PDBApplicationException;

    /**
     * Performs backend validation
     * @param product
     * @return a map of field and error messages.
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public String validateWebsites(String websites) throws PDBSystemException, PDBApplicationException;
    
    /**
     * Determine if inventory has been loaded for this product
     * @param productId
     * @return true if inventory has been loaded for this product
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public boolean hasInventoryRecord(String productId) throws PDBSystemException, PDBApplicationException;


    /**
     * Creates the Product Maintenance Mass Edit Spreadsheet
     * 
     * @param search type 'M' - by master sku or 'P' by product id
     * @param String array containing either 5 master skus or 5 product ids
      * @return XSSFWorkbook (.xlsx file containing the spreadsheet data)
     */
    public XSSFWorkbook createProductMassEditSpreadsheet(String searchType, String[] searchItems)
    	 throws Exception;
    
    /**
     * Uploads the Product Mass Edit Spreadsheet to the database
     * 
     * @param fileName
     * @param InputStream  - the spreadsheet that was uploaded
     * @throws Exception
     */
    public void uploadProductMassEditSpreadSheet(String fileName,String user, InputStream pdbMassEditFileIS) throws Exception;
    
    /**
	 * Gets the records from the MASS_EDIT_HEADER and MASS_EDIT_DETAIL to show the upload
	 * status table on the PDB Mass Edit screen.
	 * 
	 * @return a HashMap containing the massEditDirectory path and a 
	 *         List of MassEditFileStatusVO objects. 
	 * @throws Exception
	 */
    public HashMap<String, Object> getMassEditFileStatus() throws Exception;
    /**
     * Returns an array of ProductVOs based on PquardProductId
     *
     * @return array of products
     * @throws PDBApplicationException, PDBSystemException
     **/
     public List getPQuadProductList(String pquadProductId)throws PDBApplicationException, PDBSystemException;

}