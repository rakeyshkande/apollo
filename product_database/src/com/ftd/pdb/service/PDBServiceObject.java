package com.ftd.pdb.service;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pdb.common.resources.IResourceProvider;

public class PDBServiceObject {
    /* Spring managed resources */
    protected IResourceProvider resourceProvider;
    /* end Spring managed resources */
    
    protected static Logger logger;
    
    /**
    * This field is used to check if debugging is enabled.
    */
    protected boolean debugEnabled;
    
    public PDBServiceObject(String loggerCategory) {
        logger = new Logger(loggerCategory);
        
        if ( logger.isDebugEnabled() )
        {
            debugEnabled = true;
        }
    }
    
    public final IResourceProvider getResourceProvider() {
        return resourceProvider;
    }
    
    public final void setResourceProvider(IResourceProvider resourceProvider) {
        this.resourceProvider = resourceProvider;
    }
}
