package com.ftd.pdb.service;

import com.ftd.pdb.common.exceptions.PDBSystemException;
import com.ftd.pdb.common.utilities.DBUtilities;
import com.ftd.pdb.businessobject.IDeliveryZipBO;

import com.ftd.pdb.common.exceptions.PDBApplicationException;

import java.sql.Connection;

import org.w3c.dom.Document;

public class DeliveryZipSVCImpl extends PDBServiceObject implements IDeliveryZipSVC {
    IDeliveryZipBO deliveryBO;
    
    public DeliveryZipSVCImpl() {
        super("com.ftd.pdb.service.DeliveryZipSVCImpl");
    }
    
     /**
      * Get all the records from the venus.carrier table
      * @return xml representation of the returned carrier records
      * @throws Exception
      */
     public Document getCarriersXML() throws PDBSystemException {
        Connection conn = null;
        Document doc = null;
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            doc = deliveryBO.getCarriersXML(conn);
        } finally {
            DBUtilities.close(conn);
        }
        return doc;
        
    }

    /**
     * Get all the records in the venus.carrier_excluded_zips table for the
     * passed in zip code
     * @param zipCode filter
     * @return
     * @throws PDBSystemException
     */
    public Document getBlockedZips(String zipCode) throws PDBSystemException {
        Connection conn = null;
        Document doc = null;
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            doc = deliveryBO.getBlockedZips(conn,zipCode);
        } finally {
            DBUtilities.close(conn);
        }
        return doc;
    } 
    
    /**
     * Deletes a record from venus.carrier_excluded zips table
     * @param conn database connection
     * @param carrierId filter
     * @param zipCode filter
     * @throws PDBApplicationException
     * @throws PDBSystemException
     */
    public void deleteBlockedZip(Connection conn, String carrierId, String zipCode) throws PDBApplicationException, PDBSystemException {
        deliveryBO.deleteBlockedZip(conn,carrierId,zipCode);
    }

    /**
     * Inserts a record from venus.carrier_excluded zips table
     * @param conn database connection
     * @param carrierId carrier code
     * @param zipCode postal code
     * @param userId user entering the record
     * @throws PDBApplicationException
     * @throws PDBSystemException
     */
    public void insertBlockedZip(Connection conn, String carrierId, String zipCode, String userId) throws PDBApplicationException, PDBSystemException {
        deliveryBO.insertBlockedZip(conn,carrierId,zipCode,userId);
    }

    public void setDeliveryBO(IDeliveryZipBO deliveryBO) {
        this.deliveryBO = deliveryBO;
    }

    public IDeliveryZipBO getDeliveryBO() {
        return deliveryBO;
    }
}
