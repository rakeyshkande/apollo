package com.ftd.pdb.service;

import com.ftd.pdb.common.exceptions.PDBApplicationException;

import com.ftd.pdb.common.exceptions.PDBSystemException;

import java.util.List;
import java.util.Map;

import com.ftd.pdb.common.valobjs.ShippingKeyVO;

/**
* The interface of the Shipping Key service layer
*/
public interface IShippingKeySVCService 
{
  /**
   * Returns an array of ShippingKeyVOs
   *
   * @return array of ShippingKeyVOs
   * @throws PDBApplicationException, PDBSystemException
   **/
   public List getShippingKeyList()
        throws PDBApplicationException, PDBSystemException;

   /**
     * return a ShippingKeyVO for a given shippingID
     * @param shippingID the selectecd id for the shipping key.
     * @return ShippingKeyVO
     * @throws PDBApplicationException, PDBSystemException
    **/
    public ShippingKeyVO getShippingKey (String shippingID )
        throws PDBApplicationException, PDBSystemException;

   /**
     * cascading deleting a row in Shipping_key table and rows in any table that has a foreign key reference.
     * @param shippingKeyVO the shipping key to be removed from database.
     * @param novatorMap
     * @return Boolean true if succeed, false if failed.
     * @throws PDBApplicationException, PDBSystemException
    **/
    public boolean removeShippingKey (ShippingKeyVO shippingKeyVO, Map novatorMap )
        throws PDBApplicationException, PDBSystemException;

   /**
     * Insert a row in Shipping_key table and rows in Shipping_key_details table.
     * @param shippingKeyVO the selectecd id for the shipping key to be removed from database.
     * @param novatorMap
     * @return Boolean true if succeed, false if failed.
     * @throws PDBApplicationException, PDBSystemException
    **/
    public boolean insertShippingKey (ShippingKeyVO shippingKeyVO, Map novatorMap )
        throws PDBApplicationException, PDBSystemException;

    /**
     * Update a row in Shipping_key table and rows in Shipping_key_details table.
     * @param shippingKeyVO the selectecd id for the shipping key to be udpated from database.
     * @param novatorMap
     * @return Boolean true if succeed, false if failed.
     * @throws PDBApplicationException, PDBSystemException
    **/
    public boolean updateShippingKey (ShippingKeyVO shippingKeyVO, Map novatorMap )
        throws PDBApplicationException, PDBSystemException;

}