package com.ftd.pdb.service;

import com.ftd.pdb.businessobject.IProductBatchBO;
import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBSystemException;
import com.ftd.pdb.common.utilities.DBUtilities;
import com.ftd.pdb.common.valobjs.ProductBatchVO;

import java.sql.Connection;

import java.util.List;


public class ProductBatchSVCImpl extends PDBServiceObject implements IProductBatchSVCBusiness 
{
    /*Spring managed resources*/
     IProductBatchBO productBatchBO;
    /*end Spring managed resources*/
    
    /**
    *   ProductSVCImpl constructor
    */
    public ProductBatchSVCImpl() 
    {
        super("com.ftd.pdb.service.ProductBatchSVCImpl");
    }

  /**
   * Returns an array of ProductBatchVOs
   *
   * @return ArrayList
   * @throws PDBApplicationException, PDBSystemException
   **/
   public List getUserBatch(String userId)
        throws PDBApplicationException, PDBSystemException
    {
        Connection conn = null;
        List list = null;
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            list = productBatchBO.getUserBatch(conn,userId);
        } finally { 
            DBUtilities.close(conn);
        }
        return list;
    }
    
    /**
    * Returns a ProductBatchVO
    *
    * @return ProductBatchVO
    * @throws PDBApplicationException, PDBSystemException
    **/
    public ProductBatchVO getProduct(String userId, String productId) 
        throws PDBApplicationException, PDBSystemException
    {
        Connection conn = null;
        ProductBatchVO productVO = null;
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            productVO = productBatchBO.getProduct(conn,userId,productId);
        } finally { 
            DBUtilities.close(conn);
        }
    
        return productVO;
    }

    /**
    * Returns a ProductBatchVO
    *
    * @return ProductBatchVO
    * @throws PDBApplicationException, PDBSystemException
    **/
    public ProductBatchVO getProductByNovatorId(String userId, String novatorId) 
        throws PDBApplicationException, PDBSystemException
    {
        Connection conn = null;
        ProductBatchVO productVO = null;
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            productVO = productBatchBO.getProductByNovatorId(conn,userId, novatorId);
        } finally { 
            DBUtilities.close(conn);
        }
        
        return productVO;
    }       


  /**
   * Updates or Creates a product in the database
   * @param productBatchVO that will be created or updated
   * @throws PDBApplicationException, PDBSystemException
   **/
    public void setProduct(ProductBatchVO productBatchVO) 
        throws PDBApplicationException, PDBSystemException
    {
        Connection conn = null;
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            productBatchBO.setProduct(conn,productBatchVO);
        } finally { 
            DBUtilities.close(conn);
        }
    }

    /**
     * Deletes a product from the database
     * @param userId to delete
     * @param productId to delete
     * @exception PDBSystemException
     */
    public void deleteProduct(String userId, String productId) 
        throws PDBSystemException, PDBApplicationException
    {
        Connection conn = null;
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            productBatchBO.deleteProduct(conn, userId, productId);
        } finally { 
            DBUtilities.close(conn);
        }
    }

    public void setProductBatchBO(IProductBatchBO productBatchBO) {
        this.productBatchBO = productBatchBO;
    }

    public IProductBatchBO getProductBatchBO() {
        return productBatchBO;
    }
}
