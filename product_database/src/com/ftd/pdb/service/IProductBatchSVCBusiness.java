package com.ftd.pdb.service;

import com.ftd.pdb.common.exceptions.PDBApplicationException;

import com.ftd.pdb.common.exceptions.PDBSystemException;

import java.util.List;

import com.ftd.pdb.common.valobjs.ProductBatchVO;

public interface IProductBatchSVCBusiness
{
  /**
     * Returns an array of ProductBatchVOs
     * @param userId to locate
     * @return array of products
     * @throws PDBSystemException
     */
   public List getUserBatch(String userId)
        throws PDBApplicationException, PDBSystemException;

   /**
    * Returns a ProductBatchVO
    * @param userId to locate
    * @param productId to locate
    * @return ProductBatchVO
    * @throws PDBApplicationException, PDBSystemException
    **/
   public ProductBatchVO getProduct(String userId, String productId) 
        throws PDBSystemException, PDBApplicationException;

   /**
    * Returns a ProductBatchVO based on Novator ID
    * @param userId to locate
    * @param novatorId to locate
    * @return ProductXMLVO
    * @throws PDBSystemException
    **/
   public ProductBatchVO getProductByNovatorId(String userId, String novatorId) 
        throws PDBSystemException, PDBApplicationException;

    /**
     * Saves a ProductBatchVO to the database
     * @param productBatchVO that will be updated or created in the database
     * @exception PDBSystemException
     */
    public void setProduct(ProductBatchVO productBatchVO) 
        throws PDBSystemException, PDBApplicationException;

    /**
     * Deletes a product from the database
     * @param userId to delete
     * @param productId to delete
     * @exception PDBSystemException
     */
    public void deleteProduct(String userId, String productId) 
        throws PDBSystemException, PDBApplicationException;
}