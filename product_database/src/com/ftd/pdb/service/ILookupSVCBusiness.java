package com.ftd.pdb.service;

import com.ftd.pdb.common.exceptions.PDBSystemException;

import java.util.HashMap;
import java.util.List;

import org.w3c.dom.Document;


/**
 * The interface of a lookup service layer
 *
 * @author Jeff Penney - Software Architects
 **/
public interface ILookupSVCBusiness 
{

    /**
     * Get a list of  Carrier types
     * @return List of Categories
     * @exception PDBSystemException
     * Ed Mueller, 6/9/03
     */
    public List getCarrierTypes() throws PDBSystemException;
    
    /**
    * Returns a List of Categories
    *
    * @return List of Categories
    * @throws PDBSystemException
    **/
    public List getCategoryList() throws PDBSystemException;

    /**
    * Returns a List of JCPenney Categories
    *
    * @return List of Categories
    * @throws PDBSystemException
    **/
    public List getJCPenneyCategoryList() throws PDBSystemException;

    /**
    * Returns a List of Companies
    *
    * @return List of Companies
    * @throws PDBSystemException
    *  @author Ed Mueller 7/31/03
    **/
    public List getCompanyMasterList() throws PDBSystemException;


    /**
    * Returns a List of Types
    *
    * @return List of Types
    * @throws PDBSystemException
    **/
    public List getTypesList() throws PDBSystemException;

    /**
    * Returns a List of Sub types
    *
    * @return List of Sub types
    * @throws PDBSystemException
    **/
    public List getSubTypesList() throws PDBSystemException;

    /**
    * Returns a List of Countries
    *
    * @return List of Countries
    * @throws PDBSystemException
    **/
    public List getCountriesList() throws PDBSystemException;

    /**
    * Returns a List of Second Choices
    *
    * @return List of Second Choices
    * @throws PDBSystemException
    **/
    public List getSecondChoiceList() throws PDBSystemException;

    /**
    * Returns a List of States
    *
    * @return List of States
    * @throws PDBSystemException
    **/
    public List getStatesList() throws PDBSystemException;

    /**
    * Returns a List of Exception codes
    *
    * @return List of Exception codes
    * @throws PDBSystemException
    **/
    public List getExceptionCodesList() throws PDBSystemException;

    /**
     * Get a list of  Vendors
     * @return List of ValueVOs that have the vendor ID and name
     * @exception PDBSystemException
     */
    public List getVendorList() throws PDBSystemException;

    /**
     * Get a list of  Vendor Carriers
     * @return List of ValueVOs that have the vendor ID and name
     * @exception PDBSystemException
     */
    public List getVendorCarrierList() throws PDBSystemException;

    /**
     * Get a list of  ValueVOs representing source codes 
     * @return List of ValueVOs representing source codes
     * @exception PDBSystemException
     */
    public List getSourceCodeList() throws PDBSystemException;

    /**
     * Get a list of  ValueVOs representing price points
     * @return List of ValueVOs representing price points
     * @exception PDBSystemException
     */
    public List getPricePointsList() throws PDBSystemException;

    /**
     * Get a list of  ValueVOs representing shipping keys
     * @return List of ValueVOs representing shipping keys
     * @exception PDBSystemException
     */
    public List getShippingKeysList() throws PDBSystemException;
    
    /**
     * Get a list of  ValueVOs representing recipient search ids
     * @return List of ValueVOs representing recipient search ids
     * @exception PDBSystemException
     */
    public List getRecipientSearchList() throws PDBSystemException;        

    /**
     * Get a list of  ValueVOs representing personalization templates
     * @return List of ValueVOs representing personalization templates
     * @exception PDBSystemException
     */
    public List getPersonalizationTemplateList() throws PDBSystemException; 

    /**
     * Get a list of  ValueVOs representing product personalization template field 
     * order/permutation possibilities for a given template.
     * @param templateId - Template id
     * @return List of ValueVOs representing product personalization template field order/permutation possibilities
     * @exception PDBSystemException
     */
    public List getPersonalizationTemplateOrder(String templateId) throws PDBSystemException; 

    /**
     * Get a list of  ValueVOs representing search priorities
     * @return List of ValueVOs representing search priorities
     * @exception PDBSystemException
     */
    public List getSearchPriorityList() throws PDBSystemException;

    /**
     * Get a list of  ValueVOs representing shipping methods
     * @return List of ValueVOs representing shipping methods
     * @exception PDBSystemException
     */
    public List getShippingMethodsList() throws PDBSystemException;

    /**
     * Get a list of  ValueVOs representing available colors
     * @return List of ValueVOs representing available colors
     * @exception PDBSystemException
     */
    public List getColorsList() throws PDBSystemException;

    /**
     * Get a list of StateDeliveryExclusionVO representing states that have delivery exclusions
     * @return List of StateDeliveryExclusionVO representing states that have delivery exclusions
     * @exception PDBSystemException
     */
    public List getStateDeliveryExceptionList() throws PDBSystemException;
    
    /**
     * Get all the data required to prepopulate the product maint screen
     */
    public HashMap getProductMaintSetupData() throws PDBSystemException;
    
    /**
     * Get a list of VendorProductVOs representing active vendor types     
     * @param vendorType filter
     * @return List of VendorProductVOs representing active vendor types
     * @exception PDBSystemException
     */
    public List getVendorsOfType(String vendorType) throws PDBSystemException;
    
    /**
     * Get a list of carriers
     * @return List of ValueVO containing carrier data
     * @throws PDBSystemException
     */
    public List getCarrierList() throws PDBSystemException;
    
    /**
     * Get a list of  ValueVOs representing box types
     * @return List of ValueVOs representing box types
     * @exception PDBSystemException
     */
    public List getBoxList() throws PDBSystemException;
    
    /**
       * Get a list of  ValueVOs representing available component skus
       * @param conn database connection
       * @return List of ValueVOs representing available component skus
       * @exception PDBSystemException
       */
    public List getComponentSkuList() throws PDBSystemException;

    /**
      * Returns a List of durations
      *
      * @return List of States
      * @throws PDBSystemException
    **/
    public List getServiceDurationList() throws PDBSystemException;

}