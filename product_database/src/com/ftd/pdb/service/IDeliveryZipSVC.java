package com.ftd.pdb.service;

import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBSystemException;

import java.sql.Connection;

import org.w3c.dom.Document;

public interface IDeliveryZipSVC {
    
     /**
      * Get all the records from the venus.carrier table
      * @return xml representation of the returned carrier records
      * @throws Exception
      */
     public Document getCarriersXML() throws PDBSystemException;
    
     /**
     * Get all the records from the venus.carrier_excluded_zips table
     * for the passed in zip code
     * @param zipCode filter
     * @return xml representation of the returned carrier records
     * @throws Exception
     * @throws PDBSystemException
     */
     public Document getBlockedZips(String zipCode) throws PDBSystemException;
    /**
     * Deletes a record from venus.carrier_excluded zips table
     * @param conn database connection
     * @param carrierId filter
     * @param zipCode filter
     * @throws PDBApplicationException
     * @throws PDBSystemException
     */
    public void deleteBlockedZip(Connection conn, String carrierId, String zipCode) throws PDBApplicationException, PDBSystemException;

    /**
     * Inserts a record from venus.carrier_excluded zips table
     * @param conn database connection
     * @param carrierId carrier code
     * @param zipCode postal code
     * @param userId user entering the record
     * @throws PDBApplicationException
     * @throws PDBSystemException
     */
    public void insertBlockedZip(Connection conn, String carrierId, String zipCode, String userId) throws PDBApplicationException, PDBSystemException;
}
