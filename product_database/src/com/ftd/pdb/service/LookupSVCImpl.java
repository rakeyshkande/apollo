package com.ftd.pdb.service;

import com.ftd.pdb.businessobject.ILookupBO;
import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.exceptions.PDBSystemException;
import com.ftd.pdb.common.utilities.DBUtilities;

import java.sql.Connection;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.w3c.dom.Document;


/**
 * The implementation of a lookup service layer
 *
 * @author Jeff Penney - Software Architects
 **/
public class LookupSVCImpl extends PDBServiceObject implements ILookupSVCBusiness
{
    /*Spring provided resources*/
    private ILookupBO lookupBO;
    /*Spring provided resources*/
    
    /**
    * Constructor for the Lookup service layer object
    *
    **/
    public LookupSVCImpl()
    {
        super("com.ftd.pdb.service.LookupSVCImpl");
    }

    /**
    * Returns a List of Categories
    *
    * @return List of Categories
    * @throws PDBSystemException
    **/
    public List getCategoryList() throws PDBSystemException
    {
        Connection conn = null;
        List list = null;
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            list = lookupBO.getCategoryList(conn);    
        } finally { 
            DBUtilities.close(conn);
        }
        return list;
    }

    /**
     * Get a list of  Carrier types
     * @return List of Categories
     * @exception PDBSystemException
     * Ed Mueller, 6/9/03
     */
    public List getCarrierTypes() throws PDBSystemException
    {
        Connection conn = null;
        List list = null;
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            list = lookupBO.getCarrierTypes(conn);
        } finally { 
            DBUtilities.close(conn);
        }
        return list;
    }

    /**
    * Returns a List of Companies
    *
    * @return List of Companies
    * @throws PDBSystemException
    *  @author Ed Mueller 7/31/03
    **/
    public List getCompanyMasterList() throws PDBSystemException{
        Connection conn = null;
        List list = null;
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            list = lookupBO.getCompanyMasterList(conn);
        } finally { 
            DBUtilities.close(conn);
        }
        return list;
    }
        

    /**
    * Returns a List of JCPenney Categories
    *
    * @return List of Categories
    * @throws PDBSystemException
    * @deprecated JCPenney is unused lpuckett 08/30/2006
    **/
    public List getJCPenneyCategoryList() throws PDBSystemException 
    {
        Connection conn = null;
        List list = null;
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            list = lookupBO.getJCPenneyCategoryList(conn);
        } finally { 
            DBUtilities.close(conn);
        }
        return list;
    }

    /**
    * Returns a List of Types
    *
    * @return List of Types
    * @throws PDBSystemException
    **/
    public List getTypesList() throws PDBSystemException
    {
        Connection conn = null;
        List list = null;
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            list = lookupBO.getTypesList(conn);
        } finally { 
            DBUtilities.close(conn);
        }
        return list;
    }

    /**
    * Returns a List of Sub types
    *
    * @return List of Sub types
    * @throws PDBSystemException
    **/
    public List getSubTypesList() throws PDBSystemException
    {
        Connection conn = null;
        List list = null;
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            list = lookupBO.getSubTypesList(conn);
        } finally { 
            DBUtilities.close(conn);
        }
        return list;
    }

    /**
    * Returns a List of Countries
    *
    * @return List of Countries
    * @throws PDBSystemException
    **/
    public List getCountriesList() throws PDBSystemException
    {
        Connection conn = null;
        List list = null;
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            list = lookupBO.getCountriesList(conn);
        } finally { 
            DBUtilities.close(conn);
        }
        return list;
    }

    /**
    * Returns a List of Second Choices
    *
    * @return List of Second Choices
    * @throws PDBSystemException
    **/
    public List getSecondChoiceList() throws PDBSystemException
    {
        Connection conn = null;
        List list = null;
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            list = lookupBO.getSecondChoiceList(conn);
        } finally { 
            DBUtilities.close(conn);
        }
        return list;
    }

    /**
    * Returns a List of States
    *
    * @return List of States
    * @throws PDBSystemException
    **/
    public List getStatesList() throws PDBSystemException
    {
        Connection conn = null;
        List list = null;
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            list = lookupBO.getStatesList(conn);
        } finally { 
            DBUtilities.close(conn);
        }
        return list;
    }    

    /**
    * Returns a List of Exception codes
    *
    * @return List of Exception codes
    * @throws PDBSystemException
    **/
    public List getExceptionCodesList() throws PDBSystemException
    {
        Connection conn = null;
        List list = null;
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            list = lookupBO.getExceptionCodesList(conn);
        } finally { 
            DBUtilities.close(conn);
        }
        return list;
    }

    /**
     * Get a list of  Vendors
     * @return List of ValueVOs that have the vendor ID and name
     * @exception PDBSystemException
     */
    public List getVendorList() throws PDBSystemException
    {
        Connection conn = null;
        List list = null;
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            list = lookupBO.getVendorList(conn);
        } finally { 
            DBUtilities.close(conn);
        }
        return list;
    }

    /**
     * Get a list of  Vendor Carriers
     * @return List of ValueVOs that have the vendor ID and name
     * @exception PDBSystemException
     */
    public List getVendorCarrierList() throws PDBSystemException
    {
        Connection conn = null;
        List list = null;
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            list = lookupBO.getVendorCarrierList(conn);
        } finally { 
            DBUtilities.close(conn);
        }
        return list;
    }
    
    /**
     * Get a list of  ValueVOs representing shipping availability 
     * @return List of ValueVOs representing shipping availability 
     * @exception PDBSystemException
     */
    public List getShippingAvailabilityList(String typeCode) throws PDBSystemException
    {
        return lookupBO.getShippingAvailabilityList(typeCode);
    }

        /**
     * Get a list of  ValueVOs representing source codes 
     * @return List of ValueVOs representing source codes
     * @exception PDBSystemException
     */
    public List getSourceCodeList() throws PDBSystemException
    {
        Connection conn = null;
        List list = null;
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            list = lookupBO.getSourceCodeList(conn);
        } finally { 
            DBUtilities.close(conn);
        }
        return list;
    }

    /**
     * Get a list of  ValueVOs representing price points
     * @return List of ValueVOs representing price points
     * @exception PDBSystemException
     */
    public List getPricePointsList() throws PDBSystemException
    {
        return lookupBO.getPricePointsList();
    }

    /**
     * Get a list of  ValueVOs representing shipping keys
     * @return List of ValueVOs representing shipping keys
     * @exception PDBSystemException
     */
    public List getShippingKeysList() throws PDBSystemException
    {
        Connection conn = null;
        List list = null;
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            list = lookupBO.getShippingKeysList(conn);
        } finally { 
            DBUtilities.close(conn);
        }
        return list;
    }

    /**
     * Get a list of  ValueVOs representing recipient search ids
     * @return List of ValueVOs representing recipient search ids
     * @exception PDBSystemException
     */
    public List getRecipientSearchList() throws PDBSystemException
    {
        Connection conn = null;
        List list = null;
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            list = lookupBO.getRecipientSearchList(conn);
        } finally { 
            DBUtilities.close(conn);
        }
        return list;
    }        


    /**
     * Get a list of  ValueVOs representing personalization templates
     * @return List of ValueVOs representing personalization templates
     * @exception PDBSystemException
     */
    public List getPersonalizationTemplateList() throws PDBSystemException
    {
        Connection conn = null;
        List list = null;
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            list = lookupBO.getPersonalizationTemplateList(conn);
        } finally { 
            DBUtilities.close(conn);
        }
        return list;
    }   

    /**
     * Get a list of  ValueVOs representing product personalization template field 
     * order/permutation possibilities for a given template.
     * @param templateId - Template id
     * @return List of ValueVOs representing product personalization template field order/permutation possibilities
     * @exception PDBSystemException
     */
    public List getPersonalizationTemplateOrder(String templateId) throws PDBSystemException 
    {
        return lookupBO.getPersonalizationTemplateOrder(templateId);
    }   

    /**
     * Get a list of  ValueVOs representing search priorities
     * @return List of ValueVOs representing search priorities
     * @exception PDBSystemException
     */
    public List getSearchPriorityList() throws PDBSystemException
    {
        Connection conn = null;
        List list = null;
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            list = lookupBO.getSearchPriorityList(conn);
        } finally {
            DBUtilities.close(conn);
        }
        return list;
    }

    /**
     * Get a list of  ValueVOs representing shipping methods
     * @return List of ValueVOs representing shipping methods
     * @exception PDBSystemException
     */
    public List getShippingMethodsList() throws PDBSystemException
    {
        Connection conn = null;
        List list = null;
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            list = lookupBO.getShippingMethodsList(conn);
        } finally {
            DBUtilities.close(conn);
        }
        return list;
    }
    
    /**
     * Get a list of  ValueVOs representing active vendor types     
     * @return List of ValueVOs representing active vendor types
     * @exception PDBSystemException
     */
    public List getActiveVendorTypes() throws PDBSystemException
    {
        Connection conn = null;
        List list = null;
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            list = lookupBO.getActiveVendorTypes(conn);
        } finally {
            DBUtilities.close(conn);
        }
        return list;
    }
    
    /**
     * Get a list of  ValueVOs representing box types     
     * @return List of ValueVOs representing box types
     * @exception PDBSystemException
     */
    public List getBoxList() throws PDBSystemException
    {
        Connection conn = null;
        List list = null;
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            list = lookupBO.getBoxList(conn);
        } finally {
            DBUtilities.close(conn);
        }
        return list;
    }
    
    /**
     * Get a list of VendorProductVOs representing active vendor types     
     * @param vendorType filter
     * @return List of VendorProductVOs representing active vendor types
     * @exception PDBSystemException
     */
    public List getVendorsOfType(String vendorType) throws PDBSystemException
    {
        Connection conn = null;
        List list = null;
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            list = lookupBO.getVendorsOfType(conn,vendorType);
        } finally {
            DBUtilities.close(conn);
        }
        return list;
    }

    /**
     * Get a list of  ValueVOs representing available colors
     * @return List of ValueVOs representing available colors
     * @exception PDBSystemException
     */
    public List getColorsList() throws PDBSystemException
    {
        Connection conn = null;
        List list = null;
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            list = lookupBO.getColorsList(conn);
        } finally {
            DBUtilities.close(conn);
        }
        return list;
    }    

    /**
     * Get a list of StateDeliveryExclusionVO representing states that have delivery exclusions
     * @return List of StateDeliveryExclusionVO representing states that have delivery exclusions
     * @exception PDBSystemException
     */
    public List getStateDeliveryExceptionList() throws PDBSystemException
    {
        Connection conn = null;
        List list = null;
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            list = lookupBO.getStateDeliveryExceptionList(conn);
        } finally {
            DBUtilities.close(conn);
        }
        return list;
    }
    
    /**
     * Get a list of carriers
     * @return List of ValueVO containing carrier data
     * @throws PDBSystemException
     */
    public List getCarrierList() throws PDBSystemException {
        Connection conn = null;
        List list = null;
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            list = lookupBO.getCarrierList(conn);
        } finally {
            DBUtilities.close(conn);
        }
        return list;
        
    }
    
    public HashMap getProductMaintSetupData() throws PDBSystemException {
        Connection conn = null;
        HashMap map = new HashMap();
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            map.put(ProductMaintConstants.CATEGORIES_KEY,lookupBO.getCategoryList(conn));
            map.put(ProductMaintConstants.EXCEPTION_CODES_KEY,lookupBO.getExceptionCodesList(conn));
            map.put(ProductMaintConstants.PRICE_POINTS_KEY,lookupBO.getPricePointsList());
            map.put(ProductMaintConstants.SHIPPING_KEYS_KEY,lookupBO.getShippingKeysList(conn));
            map.put(ProductMaintConstants.RECIPIENT_SEARCH_KEY,lookupBO.getRecipientSearchList(conn));
            map.put(ProductMaintConstants.PERSONALIZATION_TEMPLATE_KEY,lookupBO.getPersonalizationTemplateList(conn));
            map.put(ProductMaintConstants.SEARCH_PRIORITY_KEY,lookupBO.getSearchPriorityList(conn));
            map.put(ProductMaintConstants.SHIPPING_METHODS_KEY,lookupBO.getShippingMethodsList(conn));
            map.put(ProductMaintConstants.PDB_JCPENNEY_CATEGORY_KEY,lookupBO.getJCPenneyCategoryList(conn));
            map.put(ProductMaintConstants.PDB_CARRIER_KEY,lookupBO.getCarrierTypes(conn));
            map.put(ProductMaintConstants.COLORS_KEY,lookupBO.getColorsList(conn));            
            map.put(ProductMaintConstants.COUNTRIES_KEY,lookupBO.getCountriesList(conn));
            map.put(ProductMaintConstants.PDB_VENDOR_CARRIER_LIST_KEY,lookupBO.getVendorCarrierList(conn));
            map.put(ProductMaintConstants.PDB_COMPANY_MASTER_KEY,lookupBO.getCompanyMasterList(conn));
            map.put(ProductMaintConstants.SHIPPING_METHODS_KEY,lookupBO.getShippingMethodsList(conn));
            map.put(ProductMaintConstants.SECOND_CHOICE_KEY,lookupBO.getSecondChoiceList(conn));
            map.put(ProductMaintConstants.VENDOR_LIST_KEY,lookupBO.getVendorList(conn));
            map.put(ProductMaintConstants.PDB_COLOR_LIST_KEY,lookupBO.getColorsList(conn));
            map.put(ProductMaintConstants.PRODUCT_TYPES_KEY,lookupBO.getTypesList(conn));
            map.put(ProductMaintConstants.PRODUCT_SUB_TYPES_KEY,lookupBO.getSubTypesList(conn));
            map.put(ProductMaintConstants.PDB_DELIV_STATE_EXCL_LIST_KEY,lookupBO.getStateDeliveryExceptionList(conn));   
            map.put(ProductMaintConstants.PRODUCT_TYPES_KEY,lookupBO.getTypesList(conn));
            map.put(ProductMaintConstants.PRODUCT_SUB_TYPES_KEY,lookupBO.getSubTypesList(conn));
            map.put(ProductMaintConstants.PDB_SHIPPING_SYSTEMS_KEY,lookupBO.getActiveVendorTypes(conn));
            map.put(ProductMaintConstants.BOX_KEY,lookupBO.getBoxList(conn));
            map.put(ProductMaintConstants.COMPONENTS_KEY, lookupBO.getComponentSkuList(conn));
            map.put(ProductMaintConstants.SERVICES_DURATION_KEY, lookupBO.getServiceDurationList());
        } finally {
            DBUtilities.close(conn);
        }
        
        return map;
    }

    /**
       * Get a list of  ValueVOs representing available component skus
       * @return List of ValueVOs representing available component skus
       * @exception PDBSystemException
       */
    public List getComponentSkuList() throws PDBSystemException
    {
        Connection conn = null;
        List list = null;
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            list = lookupBO.getComponentSkuList(conn);
        } finally {
            DBUtilities.close(conn);
        }
        return list;
    }
    
    /**
    * Returns a List of service durations
    *
    * @return List of Exception codes
    * @throws PDBSystemException
    **/
    public List getServiceDurationList() throws PDBSystemException
    {
          Connection conn = null;
          List list = null;
          
          try {
              conn = this.getResourceProvider().getDatabaseConnection();
              list = lookupBO.getServiceDurationList();
          } finally {
              DBUtilities.close(conn);
          }
          return list;
        
    }

    public void setLookupBO(ILookupBO lookupBO) {
        this.lookupBO = lookupBO;
    }

    public ILookupBO getLookupBO() {
        return lookupBO;
    }


}
