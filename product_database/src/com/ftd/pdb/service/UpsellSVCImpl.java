package com.ftd.pdb.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.ftd.pdb.businessobject.IUpsellBO;
import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBSystemException;
import com.ftd.pdb.common.utilities.DBUtilities;
import com.ftd.pdb.common.valobjs.UpsellMasterVO;

import java.sql.Connection;


/**
* The implementation of the Upsell Maintenance service layer
*/
public class UpsellSVCImpl extends PDBServiceObject implements IUpsellSVCBusiness
{
    /*Spring manager resources*/
     IUpsellBO upsellBO;
    /*end Spring manager resources*/
     
    /**
    *   UpsellSVCImpl constructor
    */
    public UpsellSVCImpl() 
    {
        super("com.ftd.pdb.service.UpsellSVCImpl");
    }

  /**
   * Returns an array of Upsell VOs
   * @return ArrayList
   * @throws PDBApplicationException, PDBSystemException
   **/
   public List getUpsellList()
        throws PDBApplicationException, PDBSystemException
    {
        Connection conn = null;
        List list = null;
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            list = upsellBO.getUpsellList(conn);
        } finally {
            DBUtilities.close(conn);
        }
        return list;
    }

  /**
   * Returns the passed in Master SKU VO
   * @param sku record id
   * @return UpsellMasterVO
   * @throws PDBApplicationException, PDBSystemException
   **/
   public UpsellMasterVO getMasterSKU(String sku)
        throws PDBApplicationException, PDBSystemException
    {
        Connection conn = null;
        UpsellMasterVO masterVO = null;
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            masterVO =  upsellBO.getMasterSKU(conn,sku);
        } finally {
            DBUtilities.close(conn);
        }
        return masterVO;
    }

  /**
   * Update or insert master sku
   * @param masterVO to update
   * @param saveNovatorFeeds novator sites to update
   * @return none
   * @throws PDBApplicationException, PDBSystemException
   **/
   public void updateMasterSKU(UpsellMasterVO masterVO,List<String> saveNovatorFeeds)
        throws PDBApplicationException, PDBSystemException
    {
        Connection conn = null;
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            upsellBO.updateMasterSKU(conn,masterVO,saveNovatorFeeds);
        } finally {
            DBUtilities.close(conn);
        }
    }

  /**
   * Get a list of sku's from the request object
   * @request server request
   * @return List of sku's from the request object
   * @throws PDBApplicationException, PDBSystemException
   **/
   public List getSKUsFromRequest(HttpServletRequest request)
        throws PDBApplicationException, PDBSystemException
    {
        return upsellBO.getSKUsFromRequest(request);
    }


  /**
   * Update or insert master sku
   * @productid id of record to return
   * @return List of qualified records
   * @throws PDBApplicationException, PDBSystemException
   **/
   public List getUpsellDetailByID(String productid)
        throws PDBApplicationException, PDBSystemException
    {
        Connection conn = null;
        List list = null;
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            list = upsellBO.getUpsellDetailByID(conn,productid);
        } finally {
            DBUtilities.close(conn);
        }
        return list;
    }
        
    /**
     * Determine if a master sku exists in upsell master
     * @param sku to validate
     * @return true if the master sku exists
     */
    public boolean validateMasterSku(String sku) 
        throws PDBApplicationException, PDBSystemException {

        Connection conn = null;
        boolean retval = false;
        
        try {
            conn = this.getResourceProvider().getDatabaseConnection();
            retval = upsellBO.validateMasterSku(conn,sku);
        } finally {
            DBUtilities.close(conn);
        }
        return retval;
    }

    public void setUpsellBO(IUpsellBO upsellBO) {
        this.upsellBO = upsellBO;
    }

    public IUpsellBO getUpsellBO() {
        return upsellBO;
    }
}
