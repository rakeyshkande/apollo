package com.ftd.pdb.integration.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.exceptions.BadConnectionException;
import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBSystemException;
import com.ftd.pdb.common.utilities.FTDUtil;
import com.ftd.pdb.common.valobjs.HolidayPricingVO;


import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.HashMap;


/**
   *  Holiday Pricing DAO Implemenation
   **/
public class HolidayPricingDAOImpl extends PDBDataAccessObject implements IHolidayPricingDAO {
    public HolidayPricingDAOImpl()
    {
         super("com.ftd.pdb.dao.HolidayPricingDAOImpl");
    }


  /**
   * Retrieves holiday pricing record from database.
   * @param conn database connection
   * @deprecated Holiday Pricing is unused and will be removed in the future.
   *   lpuckett 08/16/2006
   * @return HolidayPricingVO
   *
   */
    public HolidayPricingVO getHolidayPricing(Connection conn) 
            throws PDBSystemException, PDBApplicationException
    {
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        HolidayPricingVO holidayPricingVO = null;
        
        try
        {
            HashMap inputParams = new HashMap();
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_HOLIDAY_PRICING");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
            if (rs.next())
            {
                holidayPricingVO = new HolidayPricingVO();
                holidayPricingVO.setStartDate(rs.getDate(ProductMaintConstants.OE_HOLIDAY_PRICING_START_DATE));
                holidayPricingVO.setEndDate(rs.getDate(ProductMaintConstants.OE_HOLIDAY_PRICING_END_DATE));
                holidayPricingVO.setHolidayPriceFlag(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.OE_HOLIDAY_PRICING_FLAG)));
                holidayPricingVO.setDeliveryDateFlag(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.OE_HOLIDAY_PRICING_DELIVERY_DATE_FLAG)));
            }  
        }
        catch(Exception e)
        {
            logger.error(e);
            throw new BadConnectionException(ProductMaintConstants.OE_BAD_CONNECTION_EXCEPTION, e);
        }

        return holidayPricingVO;            
            }

 /**
   * Sets the holiday pricing based values passed in.
   * @param conn database connection
   * @param holidayVO contains values to update
   * @deprecated Holiday Pricing is unused and will be removed in the future.
   *   lpuckett 08/16/2006
   */
    public void setHolidayPricing(Connection conn, HolidayPricingVO holidayVO) 
            throws PDBSystemException, PDBApplicationException
    {  
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
                
        try
        {
            HashMap inputParams = new HashMap();
            inputParams.put("START_DATE", new java.sql.Date(holidayVO.getStartDate().getTime()));
            inputParams.put("END_DATE", new java.sql.Date(holidayVO.getEndDate().getTime()));
            inputParams.put("HOLIDAY_PRICE_FLAG", FTDUtil.convertBooleanToString(holidayVO.getHolidayPriceFlag()));
            inputParams.put("DELIVERY_DATE_FLAG", FTDUtil.convertBooleanToString(holidayVO.getDeliveryDateFlag()));
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_UPDATE_HOLIDAY_PRICING");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        }
        catch(Exception e)
        {
            logger.error(e);
            throw new BadConnectionException(ProductMaintConstants.OE_BAD_CONNECTION_EXCEPTION, e);
        }
    }
}