package com.ftd.pdb.integration.dao;

import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.pdb.common.PDBXMLTags;
import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.exceptions.BadConnectionException;
import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBSystemException;
import com.ftd.pdb.common.utilities.FTDUtil;
import com.ftd.pdb.common.utilities.TransactionHelper;
import com.ftd.pdb.common.valobjs.AmazonProductAttributesVO;
import com.ftd.pdb.common.valobjs.AmazonProductMasterVO;
import com.ftd.pdb.common.valobjs.ColorVO;
import com.ftd.pdb.common.valobjs.DeliveryOptionVO;
import com.ftd.pdb.common.valobjs.ProductAddonVO;
import com.ftd.pdb.common.valobjs.ProductSubCodeVO;
import com.ftd.pdb.common.valobjs.ProductVO;
import com.ftd.pdb.common.valobjs.StateDeliveryExclusionVO;
import com.ftd.pdb.common.valobjs.ValueVO;
import com.ftd.pdb.common.valobjs.VendorProductOverrideVO;
import com.ftd.pdb.common.valobjs.VendorProductVO;

import java.math.BigDecimal;

import java.sql.Connection;
import java.sql.SQLException;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.naming.InitialContext;

import org.apache.commons.lang.StringUtils;


/**
 * This class is used to read and write Product information from the database
 *
 * @author Jeff Penney - Software Architects
 **/
public class ProductDAOImpl extends PDBDataAccessObject implements IProductDAO
{
    /**
    *   Constructor for the ProductDAOImpl class
    */
    public ProductDAOImpl()
    {
        super("com.ftd.pdb.dao.ProductDAOImpl");
    }
    
    /**
     * Returns an array of ProductVOs
     * @param conn database connection
     * @return array of products
     * @exception PDBSystemException
     */
    public List getProductList(Connection conn) throws PDBSystemException, PDBApplicationException
    {        
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        ArrayList products = new ArrayList();

        try
        {
            /* build DataRequest object */
            HashMap inputParams = new HashMap();
            dataRequest.setInputParams(inputParams);
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_PRODUCT_LIST");
      
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

            while(rs.next())
            {
                ProductVO product = new ProductVO();
                product.setProductId(rs.getString(ProductMaintConstants.OE_PRODUCT_LIST_ID));
                //product.setNovatorName(FTDUtil.convertToXMLUnsafeCharacters(rs.getString(ProductMaintConstants.OE_PRODUCT_LIST_NAME)));
                product.setNovatorName(rs.getString(ProductMaintConstants.OE_PRODUCT_LIST_NAME));
                String status = rs.getString(ProductMaintConstants.OE_PRODUCT_LIST_STATUS);
                if(status.equals(ProductMaintConstants.PRODUCT_AVAILABLE_KEY))
                {
                    product.setStatus(true);
                }
                else
                {
                    product.setStatus(false);
                }
                //added 10/02/03 ivan vojinovic
                String holdUntilAvailable = rs.getString(ProductMaintConstants.OE_PRODUCT_LIST_HOLD_UNTIL_AVAILABLE);
                if(holdUntilAvailable.equals(ProductMaintConstants.PRODUCT_HOLD_UNTIL_AVAILABLE_KEY_Y))
                {
                    product.setHoldUntilAvailable(true);
                }
                else
                {
                    product.setHoldUntilAvailable(false);
                }
                Date utilDate = rs.getDate(ProductMaintConstants.OE_PRODUCT_LIST_START);
                if( utilDate!=null ) {
                    product.setExceptionStartDate(ProductVO.PDB_FORMAT.format(utilDate));
                } else {
                    product.setExceptionStartDate(null);
                }
                //product.setExceptionStartDate(rs.getDate(ProductMaintConstants.OE_PRODUCT_LIST_START));
                
                 utilDate = rs.getDate(ProductMaintConstants.OE_PRODUCT_LIST_END);
                 if( utilDate!=null ) {
                     product.setExceptionEndDate(ProductVO.PDB_FORMAT.format(utilDate));
                 } else {
                     product.setExceptionEndDate(null);
                 }
                //product.setExceptionEndDate(rs.getDate(ProductMaintConstants.OE_PRODUCT_LIST_END));
                products.add(product);
            }
        }
        catch(Exception e) {
            String[] args = new String[1];
            args[0] = new String(e.getMessage());
            logger.error(e);
            throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, args, e);
        }

        return products;
    }

    /**
     * Returns an array of ProductVOs based on filtering criteria
     * @param conn database connection
     * @param avail filter
     * @param unavail filter
     * @param dateRestrict filter
     * @param onHold filter
     * @return array of products
     * @exception PDBSystemException
     */
    public List getProductListAdvanced(Connection conn, String avail, String unavail, String dateRestrict, String onHold)
    throws PDBSystemException, PDBApplicationException
    {
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        ArrayList products = new ArrayList();

        try
        {
            HashMap inputParams = new HashMap();
            inputParams.put("AVAIL", avail);
            inputParams.put("UNAVAIL", unavail);
            inputParams.put("DATE_RESTRICT", dateRestrict);
            inputParams.put("ON_HOLD", onHold);
            inputParams.put("PQUAD_PRODUCT_ID", null);
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_PRODUCT_LIST_ADVANCED");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);


            while(rs.next())
            {
                ProductVO product = new ProductVO();
                product.setProductId(rs.getString(ProductMaintConstants.OE_PRODUCT_LIST_ID));
                product.setNovatorName(rs.getString(ProductMaintConstants.OE_PRODUCT_LIST_NAME));
                String status = rs.getString(ProductMaintConstants.OE_PRODUCT_LIST_STATUS);
                if(status.equals(ProductMaintConstants.PRODUCT_AVAILABLE_KEY))
                {
                    product.setStatus(true);
                }
                else
                {
                    product.setStatus(false);
                }
                String holdUntilAvailable = rs.getString(ProductMaintConstants.OE_PRODUCT_LIST_HOLD_UNTIL_AVAILABLE);
                if(holdUntilAvailable.equals(ProductMaintConstants.PRODUCT_HOLD_UNTIL_AVAILABLE_KEY_Y))
                {
                    product.setHoldUntilAvailable(true);
                }
                else
                {
                    product.setHoldUntilAvailable(false);
                }
                //product.setExceptionStartDate(rs.getDate(ProductMaintConstants.OE_PRODUCT_LIST_START));
                //product.setExceptionEndDate(rs.getDate(ProductMaintConstants.OE_PRODUCT_LIST_END));
                 Date utilDate = rs.getDate(ProductMaintConstants.OE_PRODUCT_LIST_START);
                 if( utilDate!=null ) {
                     product.setExceptionStartDate(ProductVO.PDB_FORMAT.format(utilDate));
                 } else {
                     product.setExceptionStartDate(null);
                 }
                 
                  utilDate = rs.getDate(ProductMaintConstants.OE_PRODUCT_LIST_END);
                  if( utilDate!=null ) {
                      product.setExceptionEndDate(ProductVO.PDB_FORMAT.format(utilDate));
                  } else {
                      product.setExceptionEndDate(null);
                  }
                                  
                products.add(product);
            }
        }
        catch(Exception e)
        {
            String[] args = new String[1];
            args[0] = new String(e.getMessage());
            logger.error(e);
            throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, args, e);
        }

        return products;
    }

    /**
     * Returns an array of ProductVOs that have holiday pricing
     * @param conn database connection
     * @return array of products
     * @exception PDBSystemException
     */
    public List getProductListWithHolidayPricing(Connection conn) throws PDBSystemException, PDBApplicationException
    {
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        ArrayList products = new ArrayList();

        try
        {
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("GET_PDB_PRODUCT_LIST_HOLIDAY");
      
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance(); 
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

            
            while(rs.next())
            {
                ProductVO product = new ProductVO();
                product.setProductId(rs.getString(ProductMaintConstants.PRODUCT_HOLIDAY_PRICE_PRODUCT_ID));
                product.setNovatorName(rs.getString(ProductMaintConstants.PRODUCT_HOLIDAY_PRICE_NOVATOR_NAME));
                product.setNewStandardPrice(rs.getFloat(ProductMaintConstants.PRODUCT_HOLIDAY_PRICE_HOLIDAY_PRICE));
                product.setNewSKU(rs.getString(ProductMaintConstants.PRODUCT_HOLIDAY_PRICE_HOLIDAY_SKU));
                products.add(product);
            }
        }
        catch(Exception e)
        {
            String[] args = new String[1];
            args[0] = new String(e.getMessage());
            logger.error(e);
            throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, args, e);
        }

        return products;
    }

    /**
     * Returns a ProductVO based on the product id
     * @param conn database connection
     * @param productId to be returned
     * @return ProductVO
     * @exception PDBSystemException
     */
    public ProductVO getProduct(Connection conn, String productId) throws PDBSystemException, PDBApplicationException
    {
        ProductVO product = new ProductVO();

        productId = productId.toUpperCase();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;

        try
        {
            HashMap inputParams = new HashMap();
            inputParams.put("PRODUCT_ID", productId);
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_PRODUCT_DETAILS");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance(); 
            HashMap outMap = (HashMap) dataAccessUtil.execute(dataRequest);
            rs = (CachedResultSet) outMap.get("OUT_PRODUCT_CUR");
            
            if(rs!=null && rs.next())
            {
                product.setProductId(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_PRODUCT_ID));
                product.setNovatorId(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_NOVATOR_ID));
                product.setProductName(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_PRODUCT_NAME));                
                product.setNovatorName(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_NOVATOR_NAME));
                String status = rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_STATUS);
                product.setDeliveryType(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_DELIVERY_TYPE));
                product.setCategory(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_CATEGORY));
                product.setProductType(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_PRODUCT_TYPE));
                product.setProductSubType(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_PRODUCT_SUB_TYPE));
                String colorSize = rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_COLOR_SIZE_FLAG);
                product.setStandardPrice(rs.getFloat(ProductMaintConstants.OE_PRODUCT_MASTER_STANDARD_PRICE));
                product.setDeluxePrice(rs.getFloat(ProductMaintConstants.OE_PRODUCT_MASTER_DELUXE_PRICE));
                product.setPremiumPrice(rs.getFloat(ProductMaintConstants.OE_PRODUCT_MASTER_PREMIUM_PRICE));
                product.setPreferredPricePoint(rs.getInt(ProductMaintConstants.OE_PRODUCT_MASTER_PREFERRED_PRICE_POINT));
                product.setVariablePriceMax(rs.getFloat(ProductMaintConstants.OE_PRODUCT_MASTER_VARIABLE_PRICE_MAX));
                product.setLongDescription(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_LONG_DESCRIPTION));
                product.setFloristReferenceNumber(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_FLORIST_REFERENCE_NUMBER));
                product.setMercuryDescription(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_MERCURY_DESCRIPTION));
                product.setItemComments(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_ITEM_COMMENTS));
                product.setAddOnBalloonsFlag(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_ADD_ON_BALLOONS_FLAG)));
                product.setAddOnBearsFlag(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_ADD_ON_BEARS_FLAG)));
                product.setAddOnGreetingCardsFlag(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_ADD_ON_CARDS_FLAG)));
                product.setAddOnFuneralFlag(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_ADD_ON_FUNERAL_FLAG)));
                product.setCodifiedFlag(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_CODIFIED_FLAG));
                product.setExceptionCode(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_EXCEPTION_CODE));
                product.setPersonalizationTemplate(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_PERSONALIZATION_TEMPLATE));
                product.setPersonalizationTemplateOrder(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_PERSONALIZATION_TEMPLATE_ORDER));
                product.setPersonalizationLeadDays(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_PERSONALIZATION_LEAD_DAYS));
                product.setPersonalizationCaseFlag(StringUtils.equals("Y", rs.getString("personalizationCaseFlag")));
                product.setAllAlphaFlag(StringUtils.equals("Y", rs.getString("allAlphaFlag")));
                product.setBulletDescription(rs.getString(ProductMaintConstants.BULLET_DESCRIPTION));
                product.setDepartmentCode(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_DEPARTMENT_CODE));
//                product.setExceptionStartDate(rs.getDate(ProductMaintConstants.OE_PRODUCT_MASTER_EXCEPTION_START_DATE));
//                product.setExceptionEndDate(rs.getDate(ProductMaintConstants.OE_PRODUCT_MASTER_EXCEPTION_END_DATE));
                Date utilDate = rs.getDate(ProductMaintConstants.OE_PRODUCT_MASTER_EXCEPTION_START_DATE);
                if( utilDate!=null ) {
                  product.setExceptionStartDate(ProductVO.PDB_FORMAT.format(utilDate));
                } else {
                  product.setExceptionStartDate(null);
                }
                
                utilDate = rs.getDate(ProductMaintConstants.OE_PRODUCT_MASTER_EXCEPTION_END_DATE);
                if( utilDate!=null ) {
                   product.setExceptionEndDate(ProductVO.PDB_FORMAT.format(utilDate));
                } else {
                   product.setExceptionEndDate(null);
                }
                product.setExceptionMessage(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_EXCEPTION_MESSAGE));
                product.setSecondChoice(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_SECOND_CHOICE));
                product.setHolidaySecondChoice(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_HOLIDAY_SECOND_CHOICE));
                product.setDropShipCode(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_DROP_SHIP_CODE));
                product.setDiscountAllowedFlag(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_DISCOUNT_ALLOWED_FLAG)));
                product.setDeliveryIncludedFlag(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_DELIVERY_INCLUDED_FLAG)));
                product.setTaxFlag(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_TAX_FLAG)));
                product.setAllowFreeShippingFlag(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_ALLOW_FREE_SHIPPING_FLAG)));
                product.setMorningDeliveryFlag(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_MORNING_DELIVERY_FLAG)));
                product.setDuration(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_SERVICE_DURATION));
                product.setServiceFeeFlag(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_SERVICE_FEE_FLAG)));
                product.setExoticFlag(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_EXOTIC_FLAG)));
                product.setEGiftFlag(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_EGIFT_FLAG)));
                product.setCountry(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_COUNTRY));
                product.setArrangementSize(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_ARRANGEMENT_SIZE));
                //product.setArrangementColors(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_ARRANGEMENT_COLORS));
                product.setDominantFlowers(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_DOMINANT_FLOWERS));
                product.setSearchPriority(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_SEARCH_PRIORITY));
                product.setRecipe(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_RECIPE));
                product.setSendStandardRecipe(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_SEND_STANDARD_RECIPE)));
                product.setDeluxeRecipe(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_DELUXE_RECIPE));
                product.setSendDeluxeRecipe(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_SEND_DELUXE_RECIPE)));
                product.setPremiumRecipe(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_PREMIUM_RECIPE));
                product.setSendPremiumRecipe(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_SEND_PREMIUM_RECIPE)));
                product.setSubcodeFlag(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_SUB_CODE_FLAG)));
                product.setDimWeight(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_DIM_WEIGHT));
                product.setNextDayUpgrade(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_NEXT_DAY_UPGRADE_FLAG)));
                product.setCorporateSiteFlag(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_CORPORATE_SITE_FLAG)));
                product.setUnspscCode(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_UNSPSC_CODE));
                product.setStandardPriceRank(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_PRICE_RANK_1));
                product.setDeluxePriceRank(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_PRICE_RANK_2));
                product.setPremiumPriceRank(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_PRICE_RANK_3));
                product.setShipMethodCarrier(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_SHIP_METHOD_CARRIER)));
                product.setShipMethodFlorist(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_SHIP_METHOD_FLORIST)));
                product.setShippingKey(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_SHIPPING_KEY)); 
                product.setMondayDeliveryFreshCuts(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_MONDAY_DELIVERY_FRESHCUT)));
                product.setTwoDaySaturdayShipFreshCuts(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_TWO_DAY_SHIP_SAT_FRESHCUT)));
                product.setWeboeBlocked(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_WEBOE_BLOCKED)));
//                product.setSentToNovatorProd(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_SENT_TO_NOVATOR_PROD_FLAG)));
//                product.setSentToNovatorUAT(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_SENT_TO_NOVATOR_UAT_FLAG)));
//                product.setSentToNovatorTest(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_SENT_TO_NOVATOR_TEST_FLAG)));
//                product.setSentToNovatorContent(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_SENT_TO_NOVATOR_CONTENT_FLAG)));
                
                // OE_PRODUCT_MASTER_EXPRESS_SHIPPING New for FedEx home project
                product.setExpressShippingOnly(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_EXPRESS_SHIPPING)));
                
//                java.sql.Timestamp sqlTimeStamp = rs.getTimestamp(ProductMaintConstants.OE_PRODUCT_MASTER_LAST_UPDATE);
//                String strDate = FTDUtil.formatSQLTimestampToString(sqlTimeStamp);
//                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss aa");
//                java.util.Date utilDate = new java.util.Date();
//                if(strDate != null)
//                {
//                    try
//                    {
//                        utilDate = sdf.parse(strDate);
//                    }
//                    catch(ParseException pe)
//                    {
//                        logger.error(pe);
//                        throw new PDBSystemException(ProductMaintConstants.PDB_DATE_PARSE_EXCEPTION);
//                    }
//                }
//                product.setLastUpdateDate(utilDate);
                
//                java.sql.Timestamp sqlTimeStamp = rs.getTimestamp(ProductMaintConstants.OE_PRODUCT_MASTER_LAST_UPDATE);
//                java.util.Date utilDate;
//                if( sqlTimeStamp != null ) {
//                    utilDate = new java.util.Date(sqlTimeStamp.getTime());
//                } else{
//                    utilDate = null;
//                }
//                product.setLastUpdateDate(utilDate);

//               java.sql.Timestamp sqlTimeStamp = rs.getTimestamp(ProductMaintConstants.OE_PRODUCT_MASTER_LAST_UPDATE);
//                 if( sqlTimeStamp!=null ) {
//                    product.setLastUpdateDate(ProductVO.PDB_FORMAT.format(new java.util.Date(sqlTimeStamp.getTime())));
//                 } else {
//                    product.setExceptionEndDate(null);
//                 }
                 
                product.setVariablePricingFlag(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_VARIABLE_PRICE_FLAG)));
                product.setNewSKU(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_HOLIDAY_SKU));
                product.setNewStandardPrice(rs.getFloat(ProductMaintConstants.OE_PRODUCT_MASTER_HOLIDAY_PRICE));
                product.setCatelogFlag(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_CATELOG_FLAG)));

                product.setNewDeluxePrice(rs.getFloat(ProductMaintConstants.OE_PRODUCT_MASTER_HOLIDAY_DELUXE_PRICE));
                product.setNewPremiumPrice(rs.getFloat(ProductMaintConstants.OE_PRODUCT_MASTER_HOLIDAY_PREMIUM_PRICE));
                //product.setNewStartDate(rs.getDate(ProductMaintConstants.OE_PRODUCT_MASTER_NEW_START_DATE));
                utilDate = rs.getDate(ProductMaintConstants.OE_PRODUCT_MASTER_NEW_START_DATE);
                if( utilDate!=null ) {
                    product.setNewStartDate(ProductVO.PDB_FORMAT.format(utilDate));
                } else {
                    product.setNewStartDate(null);
                }
                //product.setNewEndDate(rs.getDate(ProductMaintConstants.OE_PRODUCT_MASTER_NEW_END_DATE));
                utilDate = rs.getDate(ProductMaintConstants.OE_PRODUCT_MASTER_NEW_END_DATE);
                if( utilDate!=null ) {
                    product.setNewEndDate(ProductVO.PDB_FORMAT.format(utilDate));
                } else {
                    product.setNewEndDate(null);
                }
                product.setMercurySecondChoice(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_MERCURY_SECOND_CHOICE));
                product.setMercuryHolidaySecondChoice(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_MERCURY_HOLIDAY_SECOND_CHOICE));
                product.setAddOnChocolateFlag(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_ADD_ON_CHOCOLATE_FLAG)));
                product.setCrossRefNovatorID(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_CROSS_REF_NOVATOR_ID));
                product.setGeneralComments(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_GENERAL_COMMENTS));
                product.setDefaultCarrier(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_DEFAULT_CARRIER));

                String holdUntilAvailable = rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_HOLD_UNTIL_AVAILABLE);
                if(holdUntilAvailable.equals(ProductMaintConstants.PRODUCT_HOLD_UNTIL_AVAILABLE_KEY_Y)){
                    product.setHoldUntilAvailable(true);
                }else{
                    product.setHoldUntilAvailable(false);
                }                
                if(status.equals(ProductMaintConstants.PRODUCT_AVAILABLE_KEY)){
                    product.setStatus(true);
                }else{
                    product.setStatus(false);
                }
                if(colorSize.equals(ProductMaintConstants.PRODUCT_COLOR_FLAG)){
                    product.setColorSizeFlag(true);
                }else{
                    product.setColorSizeFlag(false);
                }
                product.setLastUpdateUserId(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_LAST_UPDATE_USER_ID));
                product.setLastUpdateSystem(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_LAST_UPDATE_SYSTEM));
                product.setOver21Flag(StringUtils.equals("Y",rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_OVER_21)));
                product.setShippingSystem(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_SHIPPING_SYSTEM));
                product.setCustomFlag(StringUtils.equals("Y",rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_CUSTOM_FLAG)));
                product.setPersonalGreetingFlag(StringUtils.equals("Y",rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_PERSONAL_GREETING_FLAG)));
                product.setPremierCollectionFlag(StringUtils.equals("Y",rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_PREMIER_COLLECTION_FLAG)));
                product.setKeywords(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_KEYWORDS));
                product.setZoneJumpEligibleFlag(StringUtils.equals("Y",rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_ZONE_JUMP_ELIGIBLE_FLAG)));
                product.setBoxId(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_BOX_ID));
                product.setSupplyExpense(rs.getFloat(ProductMaintConstants.OE_PRODUCT_MASTER_SUPPLY_EXPENSE));
                utilDate = rs.getDate(ProductMaintConstants.OE_PRODUCT_MASTER_SUPPLY_EXPENSE_EFF_DATE);
                if( utilDate!=null ) {
                   product.setSupplyExpenseEffDate(ProductVO.PDB_FORMAT.format(utilDate));
                } else {
                   product.setSupplyExpenseEffDate(null);
                }
                product.setRoyaltyPercent(rs.getFloat(ProductMaintConstants.OE_PRODUCT_MASTER_ROYALTY_PERCENT));
                utilDate = rs.getDate(ProductMaintConstants.OE_PRODUCT_MASTER_ROYALTY_PERCENT_EFF_DATE);
                if( utilDate!=null ) {
                   product.setRoyaltyPercentEffDate(ProductVO.PDB_FORMAT.format(utilDate));
                } else {
                   product.setRoyaltyPercentEffDate(null);
                }
                product.setGbbPopoverFlag(StringUtils.equals("Y",rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_GBB_POPOVER_FLAG)));
                product.setGbbTitle(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_GBB_TITLE));
                product.setGbbNameOverrideFlag1(StringUtils.equals("Y",rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_GBB_NAME_OVERRIDE_FLAG_1)));
                product.setGbbNameOverrideText1(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_GBB_NAME_OVERRIDE_TEXT_1));
                product.setGbbPriceOverrideFlag1(StringUtils.equals("Y",rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_GBB_PRICE_OVERRIDE_FLAG_1)));
                product.setGbbPriceOverrideText1(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_GBB_PRICE_OVERRIDE_TEXT_1));
                product.setGbbNameOverrideFlag2(StringUtils.equals("Y",rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_GBB_NAME_OVERRIDE_FLAG_2)));
                product.setGbbNameOverrideText2(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_GBB_NAME_OVERRIDE_TEXT_2));
                product.setGbbPriceOverrideFlag2(StringUtils.equals("Y",rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_GBB_PRICE_OVERRIDE_FLAG_2)));
                product.setGbbPriceOverrideText2(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_GBB_PRICE_OVERRIDE_TEXT_2));
                product.setGbbNameOverrideFlag3(StringUtils.equals("Y",rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_GBB_NAME_OVERRIDE_FLAG_3)));
                product.setGbbNameOverrideText3(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_GBB_NAME_OVERRIDE_TEXT_3));
                product.setGbbPriceOverrideFlag3(StringUtils.equals("Y",rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_GBB_PRICE_OVERRIDE_FLAG_3)));
                product.setGbbPriceOverrideText3(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_GBB_PRICE_OVERRIDE_TEXT_3));
                product.setPquadProductID(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_PQUAD_PRODUCT_ID));
                product.setFtdwestAccessoryId(rs.getString(ProductMaintConstants.PQUAD_PC_ID));
                product.setFtdwestTemplateMapping(rs.getString(ProductMaintConstants.PQUAD_PC_DISPLAY_NAMES));
            }

            if(product != null && product.getProductId() != null)
            {
                // get the keywords
                rs = (CachedResultSet) outMap.get("OUT_KEYWORD_CUR");
                
                if( rs!=null ) {
                    List keywords = new ArrayList();
                    while(rs.next())
                    {
                        String keyword = rs.getString(ProductMaintConstants.OE_PRODUCT_VALUE_ID);
                        keywords.add(keyword);
                    }
                    StringBuffer sb = new StringBuffer();
                    for(int i = 0; i < keywords.size(); i++)
                    {
                        sb.append(keywords.get(i));
                        sb.append(" ");
                    }
                    product.setKeywordSearch(sb.toString());
                }
                
                // get the colors
                rs = (CachedResultSet) outMap.get("OUT_COLOR_CUR");
                
                if( rs!=null ) {
                    List colors = new ArrayList();
                    while(rs.next())
                    {
                        ColorVO color = new ColorVO();
                        color.setId(rs.getString(ProductMaintConstants.OE_PRODUCT_VALUE_ID));
                        color.setOrderBy(rs.getInt(ProductMaintConstants.OE_PRODUCT_VALUE_DESCRIPTION));
                        colors.add(color);
                    }
                    product.setColorsList(colors);
                }
                // set all colors list//
                LookupDAOImpl lookupDAO = new LookupDAOImpl();
                product.setAllColorsList(lookupDAO.getColorsList(conn));

                // get the product component skus
                rs = (CachedResultSet) outMap.get("OUT_COMPONENT_CUR");
                if (rs != null) {
                    List componentSkus = new ArrayList();
                    while (rs.next()) {
                        ValueVO component = new ValueVO();
                        component.setId(rs.getString("componentSkuId"));
                        component.setDescription(rs.getString("componentSkuName"));
                        componentSkus.add(component);
                        logger.debug(component.getId());
                    }
                    product.setComponentSkuList(componentSkus);
                }
                product.setAllComponentSkuList(lookupDAO.getComponentSkuList(conn));

                // Get the recipient search list
                rs = (CachedResultSet) outMap.get("OUT_RECIPIENT_SEARCH_CUR");
                
                if( rs!=null ) {
                    List recipientList = new ArrayList();
                    while(rs.next())
                    {
                        String recipient = rs.getString(ProductMaintConstants.OE_PRODUCT_VALUE_ID);
                        recipientList.add(recipient);
                    }
                    String[] recipientArray = new String[recipientList.size()];
                    for( int idx=0; idx<recipientList.size(); idx++) {
                        recipientArray[idx]=(String)recipientList.get(idx);   
                    }
                    product.setRecipientSearch(recipientArray);
                }
                
                // Get the recipient search list
                rs = (CachedResultSet) outMap.get("OUT_COMPANY_CUR");
                
                if( rs!=null ) {
                    List companyList = new ArrayList();
                    while(rs.next())
                    {
                        String company = rs.getString(ProductMaintConstants.OE_COMPANY_ID);
                        companyList.add(company);
                    }
                    String[] companyArray = new String[companyList.size()];
                    for( int idx=0; idx<companyList.size(); idx++) {
                        companyArray[idx]=(String)companyList.get(idx);   
                    }
                    product.setCompanyList(companyArray);
                }

                // Get all the excluded states
                rs = (CachedResultSet) outMap.get("OUT_EXCL_STATES_CUR");
                
                if( rs!=null ) {
                    List excludedStates = getStateDeliveryExceptionList(conn);
                    String tmpStateId;
                    StateDeliveryExclusionVO sdeVO;
                    while(rs.next())
                    {
                        tmpStateId = rs.getString(ProductMaintConstants.EXCLUDED_STATE);
                        if( tmpStateId!=null ) {
                            for( int idx=0; idx<excludedStates.size(); idx++ ) {
                                sdeVO = (StateDeliveryExclusionVO)excludedStates.get(idx);
                                if( tmpStateId.equals(sdeVO.getExcludedState()) ) {
                                    sdeVO.setSunExcluded(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.SUNDAY)));
                                    sdeVO.setMonExcluded(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.MONDAY)));
                                    sdeVO.setTueExcluded(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.TUESDAY)));
                                    sdeVO.setWedExcluded(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.WEDNESDAY)));
                                    sdeVO.setThuExcluded(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.THURSDAY)));
                                    sdeVO.setFriExcluded(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.FRIDAY)));
                                    sdeVO.setSatExcluded(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.SATURDAY)));
                                    break;
                                }
                            }
                        }
                    }
                    
                    product.setExcludedDeliveryStates(excludedStates);
                        
                }		

                // Get the shipping methods for this product
                rs = (CachedResultSet) outMap.get("OUT_SHIP_METHODS_CUR");
                
                if( rs!=null ) {
                    List methodsList = new ArrayList();
                    while(rs.next())
                    {
                        String methodId = rs.getString(ProductMaintConstants.GET_PRODUCT_SHIP_METHODS);
                        String defaultCarrier = rs.getString(ProductMaintConstants.GET_PRODUCT_SHIP_METHOD_CARRIER);
                        String novatorTag = rs.getString(ProductMaintConstants.GET_PRODUCT_SHIP_METHOD_NOVATOR_TAG);
                        DeliveryOptionVO vo = new DeliveryOptionVO();
                        vo.setShippingMethodId(methodId);
                        vo.setDefaultCarrier(defaultCarrier);
                        vo.setNovatorTag(novatorTag);
                        methodsList.add(vo);
                    }
                    product.setShipMethods(methodsList);
                }

                // Get the subcodes for this product
                rs = (CachedResultSet) outMap.get("OUT_SUB_CODES_CUR");
                
                if( rs!=null ) {
                    List subcodeList = new ArrayList();
                    while(rs.next())
                    {
                        ProductSubCodeVO vo = new ProductSubCodeVO();
                        vo.setProductId(rs.getString(ProductMaintConstants.PDB_PRODUCT_SUBCODE_PRODUCT_ID));
                        vo.setProductSubCodeId(rs.getString(ProductMaintConstants.PDB_PRODUCT_SUBCODE_SUBCODE_ID));
                        vo.setSubCodeDescription(rs.getString(ProductMaintConstants.PDB_PRODUCT_SUBCODE_DESCRIPTION));
                        vo.setSubCodeRefNumber(rs.getString(ProductMaintConstants.PDB_PRODUCT_SUBCODE_REF_NUMBER));
                        vo.setSubCodePrice(rs.getFloat(ProductMaintConstants.PDB_PRODUCT_SUBCODE_PRICE));
                        vo.setSubCodeHolidaySKU(rs.getString(ProductMaintConstants.PDB_PRODUCT_SUBCODE_HOLIDAY_SKU));
                        vo.setSubCodeHolidayPrice(rs.getFloat(ProductMaintConstants.PDB_PRODUCT_SUBCODE_HOLIDAY_PRICE));
                        vo.setActiveFlag(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.PDB_PRODUCT_SUBCODE_AVAILABLE)));
                        vo.setDimWeight(rs.getString(ProductMaintConstants.PDB_PRODUCT_SUBCODE_DIM_WEIGHT));
                        vo.setDisplayOrder(rs.getInt(ProductMaintConstants.PDB_PRODUCT_SUBCODE_DISPLAY_ORDER));
    
                        subcodeList.add(vo);
                    }
                    
                    product.setSubCodeList(subcodeList);
                }
                
                // Get the vendor products
               List vendorList = new ArrayList();
                rs = (CachedResultSet) outMap.get("OUT_VENDORS_CUR");
                if( rs!=null ) {
                    while(rs.next()) {
                        VendorProductVO vo = new VendorProductVO();
                        vo.setAvailable(FTDUtil.convertStringToBoolean(rs.getString("AVAILABLE")));
                        vo.setProductSkuId(rs.getString("PRODUCT_SUBCODE_ID"));
                        vo.setRemoved(FTDUtil.convertStringToBoolean(rs.getString("REMOVED")));
                        vo.setVendorCost(rs.getFloat("VENDOR_COST"));
                        vo.setVendorId(rs.getString("VENDOR_ID"));
                        vo.setVendorName(rs.getString("VENDOR_NAME"));
                        vo.setVendorSku(rs.getString("VENDOR_SKU"));
                        vendorList.add(vo);
                    }
                }
                product.setVendorProductsList(vendorList);
                
                // Get the ship dates
                 rs = (CachedResultSet) outMap.get("OUT_SHIP_DATES_CUR");
                
                if( rs!=null ) {
                    while(rs.next())
                    {
                        product.setShipDayMonday(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.PDB_PRODUCT_SHIP_DATES_MONDAY)));
                        product.setShipDayTuesday(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.PDB_PRODUCT_SHIP_DATES_TUESDAY)));
                        product.setShipDayWednesday(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.PDB_PRODUCT_SHIP_DATES_WEDNESDAY)));
                        product.setShipDayThursday(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.PDB_PRODUCT_SHIP_DATES_THURSDAY)));
                        product.setShipDayFriday(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.PDB_PRODUCT_SHIP_DATES_FRIDAY)));
                        product.setShipDaySaturday(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.PDB_PRODUCT_SHIP_DATES_SATURDAY)));
                        product.setShipDaySunday(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.PDB_PRODUCT_SHIP_DATES_SUNDAY)));
                    }
                }
                
                AmazonProductMasterVO apmVO = getAmazonProductMaster(conn, product.getProductId());
                if (apmVO != null) {
                	if (apmVO.getCatalogStatusStandard() != null && apmVO.getCatalogStatusStandard().equalsIgnoreCase(ProductMaintConstants.PRODUCT_AVAILABLE_KEY)) {
                    	product.setAzCatalogStatusStandard(true);
                	} else {
                    	product.setAzCatalogStatusStandard(false);
                	}
                	product.setAzProductNameStandard(apmVO.getProductNameStandard());
                	product.setAzProductDescriptionStandard(apmVO.getProductDescriptionStandard());
                	if (apmVO.getCatalogStatusDeluxe() != null && apmVO.getCatalogStatusDeluxe().equalsIgnoreCase(ProductMaintConstants.PRODUCT_AVAILABLE_KEY)) {
                    	product.setAzCatalogStatusDeluxe(true);
                	} else {
                    	product.setAzCatalogStatusDeluxe(false);
                	}
                	product.setAzProductNameDeluxe(apmVO.getProductNameDeluxe());
                	product.setAzProductDescriptionDeluxe(apmVO.getProductDescriptionDeluxe());
                	if (apmVO.getCatalogStatusPremium() != null && apmVO.getCatalogStatusPremium().equalsIgnoreCase(ProductMaintConstants.PRODUCT_AVAILABLE_KEY)) {
                    	product.setAzCatalogStatusPremium(true);
                	} else {
                    	product.setAzCatalogStatusPremium(false);
                	}
                	product.setAzProductNamePremium(apmVO.getProductNamePremium());
                	product.setAzProductDescriptionPremium(apmVO.getProductDescriptionPremium());
                	product.setAzItemType(apmVO.getItemType());
                	
                }
                
                List<AmazonProductAttributesVO> apaList = getAmazonProductAttributes(conn, 
                		product.getProductId(), "BULLET_POINTS");
                for (int i=0; i<apaList.size(); i++) {
                	AmazonProductAttributesVO apaVO = apaList.get(i);
                	switch (i) {
                    	case 0:
                    		product.setAzBulletPoint1(apaVO.getProductAttributeValue());
                    		break;
                    	case 1:
                    		product.setAzBulletPoint2(apaVO.getProductAttributeValue());
                    		break;
                    	case 2:
                    		product.setAzBulletPoint3(apaVO.getProductAttributeValue());
                    		break;
                    	case 3:
                    		product.setAzBulletPoint4(apaVO.getProductAttributeValue());
                    		break;
                    	case 4:
                    		product.setAzBulletPoint5(apaVO.getProductAttributeValue());
                    		break;
                    	default:
                    		logger.error("Too many results: " + apaVO.getProductAttributeValue());
                	}
                }
                apaList = getAmazonProductAttributes(conn, 
                		product.getProductId(), "SEARCH_TERMS");
                for (int i=0; i<apaList.size(); i++) {
                	AmazonProductAttributesVO apaVO = apaList.get(i);
                	switch (i) {
                    	case 0:
                    		product.setAzSearchTerm1(apaVO.getProductAttributeValue());
                    		break;
                    	case 1:
                    		product.setAzSearchTerm2(apaVO.getProductAttributeValue());
                    		break;
                    	case 2:
                    		product.setAzSearchTerm3(apaVO.getProductAttributeValue());
                    		break;
                    	case 3:
                    		product.setAzSearchTerm4(apaVO.getProductAttributeValue());
                    		break;
                    	case 4:
                    		product.setAzSearchTerm5(apaVO.getProductAttributeValue());
                    		break;
                    	default:
                    		logger.error("Too many results: " + apaVO.getProductAttributeValue());
                	}
                }
                apaList = getAmazonProductAttributes(conn, 
                		product.getProductId(), "INTENDED_USE");
                for (int i=0; i<apaList.size(); i++) {
                	AmazonProductAttributesVO apaVO = apaList.get(i);
                	switch (i) {
                    	case 0:
                    		product.setAzIntendedUse1(apaVO.getProductAttributeValue());
                    		break;
                    	case 1:
                    		product.setAzIntendedUse2(apaVO.getProductAttributeValue());
                    		break;
                    	case 2:
                    		product.setAzIntendedUse3(apaVO.getProductAttributeValue());
                    		break;
                    	case 3:
                    		product.setAzIntendedUse4(apaVO.getProductAttributeValue());
                    		break;
                    	case 4:
                    		product.setAzIntendedUse5(apaVO.getProductAttributeValue());
                    		break;
                    	default:
                    		logger.error("Too many results: " + apaVO.getProductAttributeValue());
                	}
                }
                apaList = getAmazonProductAttributes(conn, 
                		product.getProductId(), "TARGET_AUDIENCE");
                for (int i=0; i<apaList.size(); i++) {
                	AmazonProductAttributesVO apaVO = apaList.get(i);
                	switch (i) {
                    	case 0:
                    		product.setAzTargetAudience1(apaVO.getProductAttributeValue());
                    		break;
                    	case 1:
                    		product.setAzTargetAudience2(apaVO.getProductAttributeValue());
                    		break;
                    	case 2:
                    		product.setAzTargetAudience3(apaVO.getProductAttributeValue());
                    		break;
                    	case 3:
                    		product.setAzTargetAudience4(apaVO.getProductAttributeValue());
                    		break;
                    	default:
                    		logger.error("Too many results: " + apaVO.getProductAttributeValue());
                	}
                }
                apaList = getAmazonProductAttributes(conn, 
                		product.getProductId(), "OTHER_ATTRIBUTES");
                for (int i=0; i<apaList.size(); i++) {
                	AmazonProductAttributesVO apaVO = apaList.get(i);
                	switch (i) {
                    	case 0:
                    		product.setAzOtherAttribute1(apaVO.getProductAttributeValue());
                    		break;
                    	case 1:
                    		product.setAzOtherAttribute2(apaVO.getProductAttributeValue());
                    		break;
                    	case 2:
                    		product.setAzOtherAttribute3(apaVO.getProductAttributeValue());
                    		break;
                    	case 3:
                    		product.setAzOtherAttribute4(apaVO.getProductAttributeValue());
                    		break;
                    	case 4:
                    		product.setAzOtherAttribute5(apaVO.getProductAttributeValue());
                    		break;
                    	default:
                    		logger.error("Too many results: " + apaVO.getProductAttributeValue());
                	}
                }
                apaList = getAmazonProductAttributes(conn, 
                		product.getProductId(), "SUBJECT_MATTER");
                for (int i=0; i<apaList.size(); i++) {
                	AmazonProductAttributesVO apaVO = apaList.get(i);
                	switch (i) {
                    	case 0:
                    		product.setAzSubjectMatter1(apaVO.getProductAttributeValue());
                    		break;
                    	case 1:
                    		product.setAzSubjectMatter2(apaVO.getProductAttributeValue());
                    		break;
                    	case 2:
                    		product.setAzSubjectMatter3(apaVO.getProductAttributeValue());
                    		break;
                    	case 3:
                    		product.setAzSubjectMatter4(apaVO.getProductAttributeValue());
                    		break;
                    	case 4:
                    		product.setAzSubjectMatter5(apaVO.getProductAttributeValue());
                    		break;
                    	default:
                    		logger.error("Too many results: " + apaVO.getProductAttributeValue());
                	}
                }

                //Website source codes
                rs = (CachedResultSet) outMap.get("OUT_PRODUCT_SOURCE_CUR");
                if( rs!=null ) {
                    List websites = new ArrayList();
                    while(rs.next())
                    {
                        String sourceCode = rs.getString(ProductMaintConstants.OE_PRODUCT_VALUE_ID);
                        websites.add(sourceCode);
                    }
                    StringBuffer sb = new StringBuffer();
                    for(int i = 0; i < websites.size(); i++)
                    {
                        sb.append(websites.get(i));
                        sb.append(" ");
                    }
                    product.setWebsites(sb.toString());
                    logger.info("");
                    logger.info("websites: " + sb.toString());
                    logger.info("");
                }
            }
            HashMap <String, ArrayList<ProductAddonVO>> productAddonMap = new HashMap <String, ArrayList<ProductAddonVO>>();
            ArrayList <ProductAddonVO> productAddonVOList = new ArrayList<ProductAddonVO>();
            rs = (CachedResultSet) outMap.get("OUT_PRODUCT_ADDON_CUR");
            if( rs!=null ) {
                while( rs.next() ) {
                    ProductAddonVO productAddonVO = new ProductAddonVO();
                    productAddonVO.setAddonId(rs.getString("ADDON_ID"));
                    productAddonVO.setAddonTypeId(rs.getString("ADDON_TYPE_ID"));
                    productAddonVO.setAddonDescription(rs.getString("DESCRIPTION"));
                    productAddonVO.setAddonPrice(rs.getString("PRICE"));
                    productAddonVO.setActiveFlag(StringUtils.equals("Y",rs.getString("PRODUCT_ADDON_ACTIVE_FLAG")));
                    productAddonVO.setAddonAvailableFlag(StringUtils.equals("Y",rs.getString("ADDON_ACTIVE_FLAG")));
                    productAddonVO.setAddonTypeIncludedInProductFeedFlag(StringUtils.equals("Y",rs.getString("PRODUCT_FEED_FLAG")));
                    productAddonVO.setDisplaySequenceNumber(rs.getString("DISPLAY_SEQ_NUM"));
                    productAddonVO.setMaxQuantity(rs.getString("MAX_QTY"));
                    productAddonVOList.add(productAddonVO);
                    logger.debug("Addon found with id " + productAddonVO.getAddonId());
        }
                productAddonMap.put(ProductMaintConstants.PRODUCT_ADDON_VO_ADDON_KEY,productAddonVOList);
            }
            productAddonVOList = new ArrayList<ProductAddonVO>();
            rs = (CachedResultSet) outMap.get("OUT_PRODUCT_ADDON_VASE_CUR");
            if( rs!=null ) {
                while( rs.next() ) {
                    ProductAddonVO productAddonVO = new ProductAddonVO();
                    productAddonVO.setAddonId(rs.getString("ADDON_ID"));
                    productAddonVO.setAddonTypeId(rs.getString("ADDON_TYPE_ID"));
                    productAddonVO.setAddonDescription(rs.getString("DESCRIPTION"));
                    productAddonVO.setAddonPrice(rs.getString("PRICE"));
                    productAddonVO.setActiveFlag(StringUtils.equals("Y",rs.getString("PRODUCT_ADDON_ACTIVE_FLAG")));
                    productAddonVO.setAddonAvailableFlag(StringUtils.equals("Y",rs.getString("ADDON_ACTIVE_FLAG")));
                    productAddonVO.setAddonTypeIncludedInProductFeedFlag(StringUtils.equals("Y",rs.getString("PRODUCT_FEED_FLAG")));
                    productAddonVO.setDisplaySequenceNumber(rs.getString("DISPLAY_SEQ_NUM"));
                    productAddonVO.setMaxQuantity(rs.getString("MAX_QTY"));
                    productAddonVOList.add(productAddonVO);
                    logger.debug("Vase found with id " + productAddonVO.getAddonId());

                }
                productAddonMap.put(ProductMaintConstants.PRODUCT_ADDON_VO_VASE_KEY,productAddonVOList);
            }
            product.setProductAddonMap(productAddonMap);


            
        }
        catch(Exception e)
        {
            String[] args = new String[1];
            args[0] = new String(e.getMessage());
            logger.error(e);
            throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, args, e);
        }

        if(debugEnabled)
        {
            logger.debug("ProductDAOImpl:getProduct() End");
        }
        return product;
    }

    /**
     * Returns a ProductVO based on the Novator id
     * @param conn database connection
     * @param novatorId for product to be returned
     * @return ProductVO
     * @exception PDBSystemException
     */
    public ProductVO getProductByNovatorId(Connection conn, String novatorId) throws PDBSystemException, PDBApplicationException
    {
        return getProduct(conn,novatorId);
    }

    /**
     * Saves a ProductVO to the database
     * @param connection Database connection
     * @param productVO that will be updated or created in the database
     * @exception PDBSystemException
     */
    public void setProduct(Connection connection, ProductVO productVO) throws PDBSystemException, PDBApplicationException
    {
        if(productVO == null)
        {
            logger.error("ProductVO is null.  Cannot save product.");
            throw new PDBApplicationException(ProductMaintConstants.OE_CANNOT_SAVE_NULL_PRODUCT_EXCEPTION);
        }
        
        DataRequest dataRequest = new DataRequest();
        Boolean performPasUpdate = false;
        Boolean performMyBuysFeed = false;
        TransactionHelper txHelper = new TransactionHelper(logger, debugEnabled, "ProductDAOImpl:setProduct()");
        
        try
        {
        	txHelper.setAutoCommit(connection, ProductMaintConstants.AUTO_COMMIT_KEY);
        	
            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
            
            String  myBuysFeedEnabled  = configUtil.getFrpGlobalParm("MY_BUYS", "FEED_ENABLED_FLAG");
    
            ProductVO storedProductVO = this.getProduct(connection,productVO.getProductId());
            
            //This code will determine if an update to PAS if necessary
            if(storedProductVO != null && storedProductVO.getProductId() != null)
            {
                if (productVO.isShipMethodFlorist() != storedProductVO.isShipMethodFlorist()
                 || productVO.isShipMethodCarrier() != storedProductVO.isShipMethodCarrier()
                 || !FTDUtil.stringEqualsWithBlankCheck(productVO.getCategory(), storedProductVO.getCategory())
                 || !FTDUtil.stringEqualsWithBlankCheck(productVO.getProductType(), storedProductVO.getProductType())
                 || !FTDUtil.stringEqualsWithBlankCheck(productVO.getProductSubType(), storedProductVO.getProductSubType())
                 || !FTDUtil.stringEqualsWithBlankCheck(productVO.getPersonalizationLeadDays(), storedProductVO.getPersonalizationLeadDays())
                 || productVO.isStatus() != storedProductVO.isStatus()
                 || !FTDUtil.stringEqualsWithBlankCheck(productVO.getExceptionCode(), storedProductVO.getExceptionCode())
                 || !FTDUtil.stringEqualsWithBlankCheck(productVO.getExceptionStartDate(), storedProductVO.getExceptionStartDate())
                 || !FTDUtil.stringEqualsWithBlankCheck(productVO.getExceptionEndDate(), storedProductVO.getExceptionEndDate())
                 || !FTDUtil.stringEqualsWithBlankCheck(productVO.getDeliveryType(), storedProductVO.getDeliveryType())
                 || !FTDUtil.stringEqualsWithBlankCheck(productVO.getCodifiedFlag(), storedProductVO.getCodifiedFlag())
                 || productVO.getTwoDaySaturdayShipFreshCuts() != storedProductVO.getTwoDaySaturdayShipFreshCuts()
                 || productVO.isNextDayUpgrade() != storedProductVO.isNextDayUpgrade()
                 || productVO.getMondayDeliveryFreshCuts() != storedProductVO.getMondayDeliveryFreshCuts()
                 || productVO.isExpressShippingOnly() != storedProductVO.isExpressShippingOnly()
                 || productVO.isShipDayMonday() != storedProductVO.isShipDayMonday()
                 || productVO.isShipDayTuesday() != storedProductVO.isShipDayTuesday()
                 || productVO.isShipDayWednesday() != storedProductVO.isShipDayWednesday()
                 || productVO.isShipDayThursday() != storedProductVO.isShipDayThursday()
                 || productVO.isShipDayFriday() != storedProductVO.isShipDayFriday()
                 || productVO.isShipDaySaturday() != storedProductVO.isShipDaySaturday()
                 || productVO.isShipDaySunday() != storedProductVO.isShipDaySunday()
                   )
                {                   
                    performPasUpdate = true;
                }
                else
                {
                    List productMethodsList = productVO.getShipMethods();
                    List storedMethodsList = storedProductVO.getShipMethods();
                    
                    performPasUpdate = true;
                    
                    if(productMethodsList != null && storedMethodsList != null)
                    {
                        if (productMethodsList.size() == storedMethodsList.size())
                        {
                            performPasUpdate = false;
                            HashMap <String, Boolean> productMethodsMap = new HashMap <String, Boolean>();
                            
                            for(int i = 0; i < productMethodsList.size(); i++)
                            {
                                DeliveryOptionVO productDeliveryOptionVO = (DeliveryOptionVO)productMethodsList.get(i); 
                                productMethodsMap.put(productDeliveryOptionVO.getShippingMethodId(), productDeliveryOptionVO.isSelected());
                            }
                            for(int i = 0; i < storedMethodsList.size(); i++)
                            {
                                DeliveryOptionVO storedDeliveryOptionVO = (DeliveryOptionVO)storedMethodsList.get(i); 
                                Boolean storedAvailable = productMethodsMap.get(storedDeliveryOptionVO.getShippingMethodId());                                    
                                if (storedDeliveryOptionVO != null && (storedAvailable == null  || storedDeliveryOptionVO.isSelected() != storedAvailable))
                                {
                                    performPasUpdate = true;
                                    break;
                                }
                            }
                        }                        
                    }
                    
                   
                            
                    if (!performPasUpdate)
                    {  
                        performPasUpdate = true;
                        List productVendorProductsList = productVO.getVendorProductsList();
                        List storedVendorProductsList = storedProductVO.getVendorProductsList();
                        
                        if(productVendorProductsList != null && storedVendorProductsList != null)
                        {
                            if (productVendorProductsList.size() == storedVendorProductsList.size())
                            {
                                HashMap <String, Boolean> productVendorProductsAvailabilityMap = new HashMap <String, Boolean>();
                                performPasUpdate = false;
                                for(int i = 0; i < productVendorProductsList.size(); i++)
                                {
                                    VendorProductVO vendorProductVO = (VendorProductVO)productVendorProductsList.get(i);
                                    productVendorProductsAvailabilityMap.put(vendorProductVO.getVendorId(),vendorProductVO.isAvailable());
                                }
                                for(int i = 0; i < storedVendorProductsList.size(); i++)
                                {
                                    VendorProductVO vendorProductVO = (VendorProductVO)storedVendorProductsList.get(i);
                                    Boolean storedAvailable = productVendorProductsAvailabilityMap.get(vendorProductVO.getVendorId());                                    
                                    
                                    if (vendorProductVO != null && (storedAvailable == null || vendorProductVO.isAvailable() != storedAvailable))
                                    {
                                        performPasUpdate = true;
                                        logger.info("Vendor " + vendorProductVO.getVendorId() + " availability changed");
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    if (!performPasUpdate)
                    {
                        List productExcludedDeliveryStates = productVO.getExcludedDeliveryStates();
                        List storedExcludedDeliveryStates = storedProductVO.getExcludedDeliveryStates();

                        performPasUpdate = true;

                        if(productExcludedDeliveryStates != null && storedExcludedDeliveryStates != null)
                        {
                            if (productExcludedDeliveryStates.size() == storedExcludedDeliveryStates.size())
                            {
                                HashMap <String, StateDeliveryExclusionVO> storedExcludedDeliveryStatesMap = new HashMap <String, StateDeliveryExclusionVO>();
                                performPasUpdate = false;
                                          
                                for( int idx=0; idx<storedExcludedDeliveryStates.size(); idx++)
                                {
                                    StateDeliveryExclusionVO storedStateDeliveryExclusionVO = (StateDeliveryExclusionVO)storedExcludedDeliveryStates.get(idx);
                                    if(storedStateDeliveryExclusionVO!=null)
                                    {
                                        storedExcludedDeliveryStatesMap.put(storedStateDeliveryExclusionVO.getStateName(),storedStateDeliveryExclusionVO);
                                    }
                                }
                                for( int idx=0; idx<productExcludedDeliveryStates.size(); idx++)
                                {
                                    StateDeliveryExclusionVO productStateDeliveryExclusionVO = (StateDeliveryExclusionVO)productExcludedDeliveryStates.get(idx);
                                    if (productStateDeliveryExclusionVO != null)
                                    {
                                        StateDeliveryExclusionVO storedStateDeliveryExclusionVO = storedExcludedDeliveryStatesMap.get(productStateDeliveryExclusionVO.getStateName());
                                        if (productStateDeliveryExclusionVO.isMonExcluded() != storedStateDeliveryExclusionVO.isMonExcluded()
                                         || productStateDeliveryExclusionVO.isTueExcluded() != storedStateDeliveryExclusionVO.isTueExcluded()
                                         || productStateDeliveryExclusionVO.isWedExcluded() != storedStateDeliveryExclusionVO.isWedExcluded()
                                         || productStateDeliveryExclusionVO.isThuExcluded() != storedStateDeliveryExclusionVO.isThuExcluded()
                                         || productStateDeliveryExclusionVO.isFriExcluded() != storedStateDeliveryExclusionVO.isFriExcluded()
                                         || productStateDeliveryExclusionVO.isSatExcluded() != storedStateDeliveryExclusionVO.isSatExcluded()
                                         || productStateDeliveryExclusionVO.isSunExcluded() != storedStateDeliveryExclusionVO.isSunExcluded()       
                                            )
                                        {
                                            performPasUpdate = true;
                                            logger.info("Exclusion for " + productStateDeliveryExclusionVO.getStateName() + " changed");
                                            break;                                        
                                        }
                                    }                                    
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                performPasUpdate = true;
            }
            
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance(); 

            HashMap inputParams = new HashMap();
            inputParams.put("PRODUCT_ID", productVO.getProductId());
            inputParams.put("NOVATOR_ID",   productVO.getNovatorId());
            inputParams.put("PRODUCT_NAME",  productVO.getProductName());
            inputParams.put("NOVATOR_NAME",  productVO.getNovatorName());

            String strStatus = null;
            if(productVO.isStatus())
            {
                strStatus = ProductMaintConstants.PRODUCT_AVAILABLE_KEY;
            }
            else
            {
                strStatus = ProductMaintConstants.PRODUCT_UNAVAILABLE_KEY;
            }
            inputParams.put("STATUS", strStatus);
            inputParams.put("DELIVERY_TYPE",   productVO.getDeliveryType());
            inputParams.put("CATEGORY",   productVO.getCategory());
            inputParams.put("PRODUCT_TYPE",   productVO.getProductType());
            inputParams.put("PRODUCT_SUB_TYPE",   productVO.getProductSubType());
            String strColorSize = null;
            if(productVO.isColorSizeFlag())
            {
                strColorSize = ProductMaintConstants.PRODUCT_COLOR_FLAG;
            }
            else
            {
                strColorSize = ProductMaintConstants.PRODUCT_SIZE_FLAG;
            }
            inputParams.put("COLOR_SIZE_FLAG",  strColorSize);
            inputParams.put("STANDARD_PRICE",  new BigDecimal(productVO.getStandardPrice()));
            inputParams.put("DELUXE_PRICE", new BigDecimal(productVO.getDeluxePrice()));
            inputParams.put("PREMIUM_PRICE",  new BigDecimal(productVO.getPremiumPrice()));
            inputParams.put("PREFERRED_PRICE_POINT", new BigDecimal(productVO.getPreferredPricePoint()));
            inputParams.put("VARIABLE_PRICE_MAX",  new BigDecimal(productVO.getVariablePriceMax()));
            inputParams.put("SHORT_DESCRIPTION",   productVO.getShortDescription());
            inputParams.put("LONG_DESCRIPTION",  productVO.getLongDescription());
            inputParams.put("FLORIST_REFERENCE_NUMBER",  productVO.getFloristReferenceNumber());
            inputParams.put("MERCURY_DESCRIPTION",  productVO.getMercuryDescription());
            inputParams.put("ITEM_COMMENTS",  productVO.getItemComments());
            inputParams.put("ADD_ON_BALLOONS_FLAG", FTDUtil.convertBooleanToString(productVO.isAddOnBalloonsFlag()));
            inputParams.put("ADD_ON_BEARS_FLAG", FTDUtil.convertBooleanToString(productVO.isAddOnBearsFlag()));
            inputParams.put("ADD_ON_CARDS_FLAG", FTDUtil.convertBooleanToString(productVO.isAddOnGreetingCardsFlag()));
            inputParams.put("ADD_ON_FUNERAL_FLAG", FTDUtil.convertBooleanToString(productVO.isAddOnFuneralFlag()));
            inputParams.put("CODIFIED_FLAG", productVO.getCodifiedFlag());
            //inputParams.put("EXCEPTION_CODE",   productVO.getExceptionCode());
            inputParams.put("PERSONALIZATION_TEMPLATE_ID", productVO.getPersonalizationTemplate());
            inputParams.put("PERSONALIZATION_TEMPLATE_ORDER", productVO.getPersonalizationTemplateOrder());
            
            logger.info("productVO.getFtdwestAccessoryId()==>"+productVO.getFtdwestAccessoryId());
            logger.info("productVO.getFtdwestTemplateMapping()==>"+productVO.getFtdwestTemplateMapping());
            
            inputParams.put("PQUAD_PC_ID", productVO.getFtdwestAccessoryId());
            inputParams.put("PQUAD_PC_DISPLAY_NAMES", productVO.getFtdwestTemplateMapping());
            
            inputParams.put("PERSONALIZATION_LEAD_DAYS", productVO.getPersonalizationLeadDays());
            logger.info("productVO.isPersonalizationCaseFlag()==>"+productVO.isPersonalizationCaseFlag());
            inputParams.put("PERSONALIZATION_CASE_FLAG", FTDUtil.convertBooleanToString(productVO.isPersonalizationCaseFlag()));
            logger.info("productVO.isAllAlphaFlag()==>"+productVO.isAllAlphaFlag());
            inputParams.put("ALL_ALPHA_FLAG", FTDUtil.convertBooleanToString(productVO.isAllAlphaFlag()));
            inputParams.put("DEPARTMENT_CODE", productVO.getDepartmentCode());
            
            // Defect fix 7926
            String endDate = productVO.getExceptionEndDate();
            boolean isExpired = false;
            java.sql.Date endSqlDate = null;
            java.sql.Date sqlDate;
            if(!StringUtils.isEmpty(endDate)) {
                try {
                    java.util.Date utilDate = ProductVO.PDB_FORMAT.parse(endDate);
                    endSqlDate = new java.sql.Date(utilDate.getTime());
                    if(!endSqlDate.after(new Date())) {
                    	isExpired = true;                    	
                    }
                } catch (Exception e) {
                	endSqlDate = null;
                }
            }
            
            if(isExpired) {
            	if(logger.isDebugEnabled()) {
            		logger.debug("Exception End date is completed. Exception end date: " + endSqlDate);
            	}
            	 inputParams.put("EXCEPTION_CODE",  null);
            	 inputParams.put("EXCEPTION_START_DATE", null);
            	 inputParams.put("EXCEPTION_END_DATE", null);                 
                 inputParams.put("EXCEPTION_MESSAGE",  null);
            } else {
            	inputParams.put("EXCEPTION_CODE",   productVO.getExceptionCode());
            	inputParams.put("EXCEPTION_END_DATE", endSqlDate);
            	
                String strDate = productVO.getExceptionStartDate();
                if( strDate!=null && !strDate.equalsIgnoreCase("")) {
                    try {
                        java.util.Date utilDate = ProductVO.PDB_FORMAT.parse(strDate);
                        sqlDate = new java.sql.Date(utilDate.getTime());
                    } catch (Exception e) {
                        sqlDate = null;
                    }
                } else {
                    sqlDate = null;
                }
                inputParams.put("EXCEPTION_START_DATE", sqlDate);
                inputParams.put("EXCEPTION_MESSAGE",  productVO.getExceptionMessage());
            }
            
            
            inputParams.put("VENDOR_ID",   null);
            inputParams.put("VENDOR_COST",   null);
            inputParams.put("VENDOR_SKU", null);
            inputParams.put("SECOND_CHOICE",   productVO.getSecondChoice());
            inputParams.put("HOLIDAY_SECOND_CHOICE",   productVO.getHolidaySecondChoice());
            inputParams.put("DROP_SHIP_CODE",   productVO.getDropShipCode());
            inputParams.put("DISCOUNT_ALLOWED_FLAG",  FTDUtil.convertBooleanToString(productVO.isDiscountAllowedFlag()));
            inputParams.put("DELIVERY_INCLUDED_FLAG", FTDUtil.convertBooleanToString(productVO.isDeliveryIncludedFlag()));
            inputParams.put("TAX_FLAG",  FTDUtil.convertBooleanToString(productVO.isTaxFlag()));
            inputParams.put("SERVICE_FEE_FLAG",  FTDUtil.convertBooleanToString(productVO.isServiceFeeFlag())); 
            
           if(ProductMaintConstants.PRODUCT_SUBTYPE_EXOTIC.equalsIgnoreCase(productVO.getProductSubType())) {
           		productVO.setExoticFlag(true);           
            } else {
            	productVO.setExoticFlag(false); 
            }
            
            inputParams.put("EXOTIC_FLAG",  FTDUtil.convertBooleanToString(productVO.isExoticFlag()));
            inputParams.put("EGIFT_FLAG", FTDUtil.convertBooleanToString(productVO.isEGiftFlag()));
            inputParams.put("COUNTRY",   productVO.getCountry());
            inputParams.put("ARRANGEMENT_SIZE",   productVO.getArrangementSize());
            //inputParams.put("ARRANGEMENT_COLORS",   productVO.getArrangementColors());
            inputParams.put("DOMINANT_FLOWERS",   productVO.getDominantFlowers());
            inputParams.put("SEARCH_PRIORITY",   productVO.getSearchPriority());
            inputParams.put("STANDARD_RECIPE",   productVO.getRecipe());
            inputParams.put("SEND_STANDARD_RECIPE_FLAG", FTDUtil.convertBooleanToString(productVO.isSendStandardRecipe()));
            inputParams.put("DELUXE_RECIPE",   productVO.getDeluxeRecipe());
            inputParams.put("SEND_DELUXE_RECIPE_FLAG", FTDUtil.convertBooleanToString(productVO.isSendDeluxeRecipe()));
            inputParams.put("PREMIUM_RECIPE",   productVO.getPremiumRecipe());
            inputParams.put("SEND_PREMIUM_RECIPE_FLAG", FTDUtil.convertBooleanToString(productVO.isSendPremiumRecipe()));

            logger.debug("standard recipe: " + productVO.isSendStandardRecipe() + " " + productVO.getRecipe());
            logger.debug("deluxe recipe: " + productVO.isSendDeluxeRecipe() + " " + productVO.getDeluxeRecipe());
            logger.debug("premium recipe: " + productVO.isSendPremiumRecipe() + " " + productVO.getPremiumRecipe());

            inputParams.put("SUB_CODE_FLAG",  FTDUtil.convertBooleanToString(productVO.isSubcodeFlag()));
            inputParams.put("DIM_WEIGHT",   productVO.getDimWeight());
            inputParams.put("NEXT_DAY_UPGRADE_FLAG",  FTDUtil.convertBooleanToString(productVO.isNextDayUpgrade()));
            inputParams.put("CORPORATE_SITE_FLAG",  FTDUtil.convertBooleanToString(productVO.isCorporateSiteFlag()));
            inputParams.put("UNSPSC_CODE",   productVO.getUnspscCode());
            inputParams.put("BULLET_DESCRIPTION",   (  (null == productVO.getBulletDescription() || "".equals(productVO.getBulletDescription())? "" : productVO.getBulletDescription())) );
            
            inputParams.put("PRICE_RANK_1",   productVO.getStandardPriceRank());
            inputParams.put("PRICE_RANK_2",   productVO.getDeluxePriceRank());
            inputParams.put("PRICE_RANK_3",   productVO.getPremiumPriceRank());
            inputParams.put("SHIP_METHOD_CARRIER",  FTDUtil.convertBooleanToString(productVO.isShipMethodCarrier()));
            inputParams.put("SHIP_METHOD_FLORIST",  FTDUtil.convertBooleanToString(productVO.isShipMethodFlorist()));
            inputParams.put("SHIPPING_KEY",   productVO.getShippingKey());
            String strDate = productVO.getLastUpdateDate();
            java.sql.Timestamp sqlTimestamp = FTDUtil.formatStringToSQLTimestamp(strDate);
            inputParams.put("LAST_UPDATE", sqlTimestamp);
            inputParams.put("VARIABLE_PRICE_FLAG", FTDUtil.convertBooleanToString(productVO.isVariablePricingFlag()));
            inputParams.put("HOLIDAY_SKU", productVO.getNewSKU());
            inputParams.put("HOLIDAY_PRICE", new Float(productVO.getNewStandardPrice()).toString());
            inputParams.put("CATELOG_FLAG", FTDUtil.convertBooleanToString(productVO.isCatelogFlag()));
            inputParams.put("HOLIDAY_DELUXE_PRICE", new Float(productVO.getNewDeluxePrice()).toString());
            inputParams.put("HOLIDAY_PREMIUM_PRICE", new Float(productVO.getNewPremiumPrice()).toString());
            strDate = productVO.getNewStartDate();
            if( strDate!=null ) {
                try {
                    java.util.Date utilDate = ProductVO.PDB_FORMAT.parse(strDate);
                    sqlDate = new java.sql.Date(utilDate.getTime());
                } catch (Exception e) {
                    sqlDate = null;
                }
            } else {
                sqlDate = null;
            }
            inputParams.put("NEW_START_DATE", sqlDate);
            strDate = productVO.getNewEndDate();
            if( strDate!=null ) {
             try {
                 java.util.Date utilDate = ProductVO.PDB_FORMAT.parse(strDate);
                 sqlDate = new java.sql.Date(utilDate.getTime());
             } catch (Exception e) {
                 sqlDate = null;
             }
            } else {
             sqlDate = null;
            }
            inputParams.put("NEW_END_DATE", sqlDate);
            inputParams.put("MERCURY_SECOND_CHOICE",   productVO.getMercurySecondChoice());
            inputParams.put("MERCURY_HOLIDAY_SECOND_CHOICE",   productVO.getMercuryHolidaySecondChoice());
            inputParams.put("ADD_ON_CHOCOLATE_FLAG", FTDUtil.convertBooleanToString(productVO.isAddOnChocolateFlag()));
            inputParams.put("JCPENNEY_CATEGORY", productVO.getJCPenneyCategory());
            inputParams.put("CROSS_REF_NOVATOR_ID", productVO.getCrossRefNovatorID());
            inputParams.put("GENERAL_COMMENTS", productVO.getGeneralComments());
            //ADDED BY ED, 3/25/03
            inputParams.put("SENT_TO_NOVATOR_PROD_FLAG",  FTDUtil.convertBooleanToString(productVO.isSentToNovatorProd()));
            inputParams.put("SENT_TO_NOVATOR_UAT_FLAG",  FTDUtil.convertBooleanToString(productVO.isSentToNovatorUAT()));
            inputParams.put("SENT_TO_NOVATOR_TEST_FLAG",  FTDUtil.convertBooleanToString(productVO.isSentToNovatorTest()));
            inputParams.put("SENT_TO_NOVATOR_CONTENT_FLAG",  FTDUtil.convertBooleanToString(productVO.isSentToNovatorContent()));
            //ed mueller, added 7/11/03
            inputParams.put("DEFAULT_CARRIER",  productVO.getDefaultCarrier());
            //added 10/02/03  ivan vojinovic
            String strHoldUntilAvailable = null;
            if(productVO.getHoldUntilAvailable())
            {
                strHoldUntilAvailable = ProductMaintConstants.PRODUCT_HOLD_UNTIL_AVAILABLE_KEY_Y;
            }
            else
            {
                strHoldUntilAvailable = ProductMaintConstants.PRODUCT_HOLD_UNTIL_AVAILABLE_KEY_N;
            }
            inputParams.put("HOLD_UNTIL_AVAILABLE", strHoldUntilAvailable);
               //added 10/24/03  ivan vojinovic
            inputParams.put("LAST_UPDATE_USER_ID", productVO.getLastUpdateUserId());
            inputParams.put("LAST_UPDATE_SYSTEM", productVO.getLastUpdateSystem());
            inputParams.put("MONDAY_DELIVERY_FRESHCUT",  FTDUtil.convertBooleanToString(productVO.getMondayDeliveryFreshCuts()));
            inputParams.put("TWO_DAY_SHIP_SAT_FRESHCUT",  FTDUtil.convertBooleanToString(productVO.getTwoDaySaturdayShipFreshCuts()));
            inputParams.put("WEBOE_BLOCKED", FTDUtil.convertBooleanToString(productVO.isWeboeBlocked()));
            // OE_PRODUCT_MASTER_EXPRESS_SHIPPING New for FedEx home project
            inputParams.put("EXPRESS_SHIPPING", FTDUtil.convertBooleanToString(productVO.isExpressShippingOnly()));
            inputParams.put("OVER_21", FTDUtil.convertBooleanToString(productVO.isOver21Flag()));
            inputParams.put("SHIPPING_SYSTEM", productVO.getShippingSystem() );
            inputParams.put("ZONE_JUMP_ELIGIBLE_FLAG", FTDUtil.convertBooleanToString(productVO.isZoneJumpEligibleFlag()));
            inputParams.put("ZONE_JUMP_ELIGIBLE_FLAG", FTDUtil.convertBooleanToString(productVO.isZoneJumpEligibleFlag()));
            String boxId = productVO.getBoxId();
            
            if(boxId != null && boxId.equals(ProductMaintConstants.NONE))
            {
                boxId = null;     
            }
            inputParams.put("BOX_ID", boxId );
            inputParams.put("UPDATED_BY", productVO.getLastUpdateUserId());
            inputParams.put("CUSTOM_FLAG", FTDUtil.convertBooleanToString(productVO.isCustomFlag()));
            inputParams.put("KEYWORDS_TXT", productVO.getKeywords());
            inputParams.put("SUPPLY_EXPENSE", new BigDecimal(productVO.getSupplyExpense()));
            strDate = productVO.getSupplyExpenseEffDate();
            if( strDate!=null && !strDate.equalsIgnoreCase("") ) {
                try {
                    java.util.Date utilDate = ProductVO.PDB_FORMAT.parse(strDate);
                    sqlDate = new java.sql.Date(utilDate.getTime());
                } catch (Exception e) {
                    sqlDate = null;
                }
            } else {
                sqlDate = null;
            }
            inputParams.put("SUPPLY_EXPENSE_EFF_DATE", sqlDate);
            inputParams.put("ROYALTY_PERCENT", new BigDecimal(productVO.getRoyaltyPercent()));
            strDate = productVO.getRoyaltyPercentEffDate();
            if( strDate!=null && !strDate.equalsIgnoreCase("") ) {
                try {
                    java.util.Date utilDate = ProductVO.PDB_FORMAT.parse(strDate);
                    sqlDate = new java.sql.Date(utilDate.getTime());
                } catch (Exception e) {
                    sqlDate = null;
                }
            } else {
                sqlDate = null;
            }
            inputParams.put("ROYALTY_PERCENT_EFF_DATE", sqlDate);

            inputParams.put("GBB_POPOVER_FLAG", FTDUtil.convertBooleanToString(productVO.getGbbPopoverFlag()));
            inputParams.put("GBB_TITLE", productVO.getGbbTitle());
            inputParams.put("GBB_NAME_OVERRIDE_FLAG_1", FTDUtil.convertBooleanToString(productVO.getGbbNameOverrideFlag1()));
            inputParams.put("GBB_NAME_OVERRIDE_TEXT_1", productVO.getGbbNameOverrideText1());
            inputParams.put("GBB_PRICE_OVERRIDE_FLAG_1", FTDUtil.convertBooleanToString(productVO.getGbbPriceOverrideFlag1()));
            inputParams.put("GBB_PRICE_OVERRIDE_TEXT_1", productVO.getGbbPriceOverrideText1());
            inputParams.put("GBB_NAME_OVERRIDE_FLAG_2", FTDUtil.convertBooleanToString(productVO.getGbbNameOverrideFlag2()));
            inputParams.put("GBB_NAME_OVERRIDE_TEXT_2", productVO.getGbbNameOverrideText2());
            inputParams.put("GBB_PRICE_OVERRIDE_FLAG_2", FTDUtil.convertBooleanToString(productVO.getGbbPriceOverrideFlag2()));
            inputParams.put("GBB_PRICE_OVERRIDE_TEXT_2", productVO.getGbbPriceOverrideText2());
            inputParams.put("GBB_NAME_OVERRIDE_FLAG_3", FTDUtil.convertBooleanToString(productVO.getGbbNameOverrideFlag3()));
            inputParams.put("GBB_NAME_OVERRIDE_TEXT_3", productVO.getGbbNameOverrideText3());
            inputParams.put("GBB_PRICE_OVERRIDE_FLAG_3", FTDUtil.convertBooleanToString(productVO.getGbbPriceOverrideFlag3()));
            inputParams.put("GBB_PRICE_OVERRIDE_TEXT_3", productVO.getGbbPriceOverrideText3());
            inputParams.put("PERSONAL_GREETING_FLAG", FTDUtil.convertBooleanToString(productVO.isPersonalGreetingFlag()));
            inputParams.put("PREMIER_COLLECTION_FLAG", FTDUtil.convertBooleanToString(productVO.isPremierCollectionFlag()));
            if(productVO.getDuration() == null || productVO.getDuration().trim().length() == 0)
            {
              inputParams.put("SERVICE_DURATION", null);
            }
            else
              inputParams.put("SERVICE_DURATION", Integer.parseInt(productVO.getDuration().trim()));
             
            inputParams.put("ALLOW_FREE_SHIPPING_FLAG", FTDUtil.convertBooleanToString(productVO.isAllowFreeShippingFlag()));
            inputParams.put("MORNING_DELIVERY_FLAG", FTDUtil.convertBooleanToString(productVO.isMorningDeliveryFlag()));
            if(productVO.getPquadProductID() == null || productVO.getPquadProductID().trim().length() == 0)
                inputParams.put("PQUAD_PRODUCT_ID",null);
            else
            	inputParams.put("PQUAD_PRODUCT_ID",new java.math.BigDecimal(productVO.getPquadProductID().trim()));
            
            
            logger.info("PQUAD_PRODUCT_ID is inserted "+productVO.getPquadProductID());

            // build DataRequest object //
            dataRequest.setConnection(connection);
            dataRequest.setStatementID("PDB_UPDATE_PRODUCT_DETAILS");
            dataRequest.setInputParams(inputParams);
            
            // execute the store prodcedure //
            dataAccessUtil.execute(dataRequest);
            

            if(debugEnabled)
            {
                logger.debug("ProductDAOImpl:setProduct() Executed the update product stored proc");
            }

            // Update the HP table
            inputParams = new HashMap();
            inputParams.put("PRODUCT_ID", productVO.getProductId());
            
            // Convert the ship dates into a space delimited string
            StringBuffer sbDates = new StringBuffer();
            sbDates.append(FTDUtil.convertBooleanToString(productVO.isShipDayMonday()));
            sbDates.append(FTDUtil.convertBooleanToString(productVO.isShipDayTuesday()));
            sbDates.append(FTDUtil.convertBooleanToString(productVO.isShipDayWednesday()));
            sbDates.append(FTDUtil.convertBooleanToString(productVO.isShipDayThursday()));
            sbDates.append(FTDUtil.convertBooleanToString(productVO.isShipDayFriday()));
            sbDates.append(FTDUtil.convertBooleanToString(productVO.isShipDaySaturday()));
            sbDates.append(FTDUtil.convertBooleanToString(productVO.isShipDaySunday()));
            inputParams.put("SHIP_DATES", sbDates.toString());    
            
            List colorsList = productVO.getColorsList();
            StringBuffer sbColors = new StringBuffer();
            
            // Colors being re-purposed for Project Fresh - commented out March 2018
            /*
            if(colorsList != null)
            {
                for(int i = 0; i < colorsList.size(); i++)
                {
                    sbColors.append(((ColorVO)colorsList.get(i)).getId()).append(ProductMaintConstants.HP_DELIMITER);
                }
            }
            */
            inputParams.put("COLORS", sbColors.toString());

            
            // Convert subcodes to a space delimited String
            StringBuffer subcodes = new StringBuffer();
            if(productVO.getSubCodeList() != null) {
              for (int i = 0; i < productVO.getSubCodeList().size(); i++)
              {
                  ProductSubCodeVO vo = (ProductSubCodeVO)productVO.getSubCodeList().get(i);
                  subcodes.append(vo.getProductSubCodeId()).append(ProductMaintConstants.HP_DELIMITER);
              }
            }
            inputParams.put("SUBCODES", subcodes.toString());

            
            StateDeliveryExclusionVO sdeVO;
            List excludedStates = productVO.getExcludedDeliveryStates();
            StringBuffer sbStates = new StringBuffer();
            for( int idx=0; idx<excludedStates.size(); idx++)
            {
                sdeVO = (StateDeliveryExclusionVO)excludedStates.get(idx);
                if( sdeVO!=null && (
                    sdeVO.isMonExcluded() ||
                    sdeVO.isTueExcluded() ||
                    sdeVO.isWedExcluded() ||
                    sdeVO.isThuExcluded() ||
                    sdeVO.isFriExcluded() ||
                    sdeVO.isSatExcluded() ||
                    sdeVO.isSunExcluded()) ) 
                {
                    sbStates.append(sdeVO.getExcludedState());
                    sbStates.append(FTDUtil.convertBooleanToString(sdeVO.isMonExcluded()));
                    sbStates.append(FTDUtil.convertBooleanToString(sdeVO.isTueExcluded()));
                    sbStates.append(FTDUtil.convertBooleanToString(sdeVO.isWedExcluded()));
                    sbStates.append(FTDUtil.convertBooleanToString(sdeVO.isThuExcluded()));
                    sbStates.append(FTDUtil.convertBooleanToString(sdeVO.isFriExcluded()));
                    sbStates.append(FTDUtil.convertBooleanToString(sdeVO.isSatExcluded()));
                    sbStates.append(FTDUtil.convertBooleanToString(sdeVO.isSunExcluded()));
                    sbStates.append(ProductMaintConstants.HP_DELIMITER);
                }
            }
            inputParams.put("EXCLUDED_STATES", sbStates.toString());
            dataRequest.reset();
            dataRequest.setConnection(connection);
            dataRequest.setStatementID("PDB_UPDATE_PRODUCT_HP");
            dataRequest.setInputParams(inputParams);
            
            // execute the store prodcedure //
            dataAccessUtil.execute(dataRequest);

            if(debugEnabled)
            {
                logger.debug("ProductDAOImpl:setProduct() The update HP stored proc was called");
            }

            // Remove all keywords for the product
            inputParams = new HashMap();
            inputParams.put("PRODUCT_ID", productVO.getProductId());
            dataRequest.reset();
            dataRequest.setConnection(connection);
            dataRequest.setStatementID("PDB_REMOVE_KEYWORDS");
            dataRequest.setInputParams(inputParams);
            
            // execute the store prodcedure //
            dataAccessUtil.execute(dataRequest);

            if(debugEnabled)
            {
                logger.debug("ProductDAOImpl:setProduct() The old keywords have been removed");
            }
            
            // Insert into the keyword table
            String keywords = productVO.getKeywordSearch();
            if(keywords == null)
            {
                keywords = "";
            }
            StringTokenizer st = new StringTokenizer(keywords);
            List keywordList = new ArrayList();
            while (st.hasMoreTokens())
            {
                keywordList.add(st.nextToken());
            }

            if( keywordList!=null ) {
                for(int i = 0; i < keywordList.size(); i++)
                {
                    inputParams = new HashMap();
                    inputParams.put("PRODUCT_ID", productVO.getProductId());
                    inputParams.put("KEYWORD", (String)keywordList.get(i));
                    dataRequest.reset();
                    dataRequest.setConnection(connection);
                    dataRequest.setInputParams(inputParams);
                    dataRequest.setStatementID("PDB_INSERT_KEYWORD");
                    dataAccessUtil.execute(dataRequest);                    

                }
                if(debugEnabled)
                {
                    logger.debug("ProductDAOImpl:setProduct() Executed the update keywords stored proc");
                }
            }

            // Remove the old recipient search
            inputParams = new HashMap();
            inputParams.put("PRODUCT_ID", productVO.getProductId());
            dataRequest.reset();
            dataRequest.setConnection(connection);
            dataRequest.setStatementID("PDB_REMOVE_RECIPIENT_SEARCH");
            dataRequest.setInputParams(inputParams);
             
             // execute the store prodcedure //
            dataAccessUtil.execute(dataRequest);

            if(debugEnabled)
            {
                logger.debug("ProductDAOImpl:setProduct() The old recipient search has been removed");
            }
            
            // Insert the new recipient search
            String[] recipientList = productVO.getRecipientSearch();
            if(recipientList != null)
            {
                for(int i = 0; i < recipientList.length; i++)
                {
                    inputParams = new HashMap();
                    inputParams.put("PRODUCT_ID", productVO.getProductId());
                    inputParams.put("RECIPIENT_SEARCH_ID", (String)recipientList[i]);
                    dataRequest.reset();
                    dataRequest.setConnection(connection);
                    dataRequest.setInputParams(inputParams);
                    dataRequest.setStatementID("PDB_INSERT_RECIPIENT_SEARCH");
                    dataAccessUtil.execute(dataRequest);
                }
            }
            if(debugEnabled)
            {
                logger.debug("ProductDAOImpl:setProduct() Executed the update recipient search stored proc");
            }

            // Remove the old companies
            inputParams = new HashMap();
            inputParams.put("PRODUCT_ID", productVO.getProductId());
            dataRequest.reset();
            dataRequest.setConnection(connection);
            dataRequest.setStatementID("PDB_REMOVE_PRODUCT_COMPANY");
            dataRequest.setInputParams(inputParams);
            
            // execute the store prodcedure //
            dataAccessUtil.execute(dataRequest);

            if(debugEnabled)
            {
                logger.debug("ProductDAOImpl:setProduct() The old companies have been removed");
            }
            
            // Insert the new companies
            String[] companyList = productVO.getCompanyList();
            if(companyList != null)
            {
                for(int i = 0; i < companyList.length; i++)
                {
                    inputParams = new HashMap();
                    inputParams.put("PRODUCT_ID", productVO.getProductId());
                    inputParams.put("COMPANY_ID", (String)companyList[i]);
                    dataRequest.reset();
                    dataRequest.setConnection(connection);
                    dataRequest.setInputParams(inputParams);
                    dataRequest.setStatementID("PDB_INSERT_PRODUCT_COMPANY");
                    dataAccessUtil.execute(dataRequest);
                }
            }           
            if(debugEnabled)
            {
                logger.debug("ProductDAOImpl:setProduct() Executed the update product company stored proc");
            }

            // Remove the old colors
            inputParams = new HashMap();
            inputParams.put("PRODUCT_ID", productVO.getProductId());
            dataRequest.reset();
            dataRequest.setConnection(connection);
            dataRequest.setStatementID("PDB_REMOVE_PRODUCT_COLORS");
            dataRequest.setInputParams(inputParams);
            
            // execute the store prodcedure //
            dataAccessUtil.execute(dataRequest);

            if(debugEnabled)
            {
                logger.debug("ProductDAOImpl:setProduct() The old colors have been removed");
            }
            
            // set the product colors table
            
            colorsList = productVO.getColorsList();
            if(colorsList != null)
            {
                for(int i = 0; i < colorsList.size(); i++)
                {
                    inputParams = new HashMap();
                    inputParams.put("PRODUCT_ID", productVO.getProductId());
                    inputParams.put("COLOR_ID", ((ColorVO)colorsList.get(i)).getId());
                    inputParams.put("CARDINAL_POS", new BigDecimal(((ColorVO)colorsList.get(i)).getOrderBy()));
                    dataRequest.reset();
                    dataRequest.setConnection(connection);
                    dataRequest.setInputParams(inputParams);
                    dataRequest.setStatementID("PDB_INSERT_PRODUCT_COLOR");
                    dataAccessUtil.execute(dataRequest);                    
                }
            }
            if(debugEnabled)
            {
                logger.debug("ProductDAOImpl:setProduct() Executed the update product colors stored proc");
            }   

            // Remove the old components
            inputParams = new HashMap();
            inputParams.put("PRODUCT_ID", productVO.getProductId());
            dataRequest.reset();
            dataRequest.setConnection(connection);
            dataRequest.setStatementID("PDB_REMOVE_PRODUCT_COMPONENTS");
            dataRequest.setInputParams(inputParams);
            
            // execute the store prodcedure //
            dataAccessUtil.execute(dataRequest);

            if(debugEnabled)
            {
                logger.debug("ProductDAOImpl:setProduct() The old components have been removed");
            }
            
            // set the product components table
            List componentList = productVO.getComponentSkuList();
            if(componentList != null)
            {
                logger.debug("componentList size: " + componentList.size());
                for(int i = 0; i < componentList.size(); i++)
                {
                    logger.debug("Saving " + ((ValueVO)componentList.get(i)).getId());
                    inputParams = new HashMap();
                    inputParams.put("IN_PRODUCT_ID", productVO.getProductId());
                    inputParams.put("IN_COMPONENT_SKU_ID", ((ValueVO)componentList.get(i)).getId());
                    dataRequest.reset();
                    dataRequest.setConnection(connection);
                    dataRequest.setInputParams(inputParams);
                    dataRequest.setStatementID("PDB_INSERT_PRODUCT_COMPONENTS");
                    dataAccessUtil.execute(dataRequest);                    
                }
            }
            if(debugEnabled)
            {
                logger.debug("ProductDAOImpl:setProduct() Executed the update product components stored proc");
            }   

            //Remove the existing excluded states
            inputParams = new HashMap();
            inputParams.put("PRODUCT_ID", productVO.getProductId());
            dataRequest.reset();
            dataRequest.setConnection(connection);
            dataRequest.setStatementID("PDB_REMOVE_EXCL_STATES");
            dataRequest.setInputParams(inputParams);
             
            // execute the store prodcedure //
            dataAccessUtil.execute(dataRequest);

            if(debugEnabled)
            {
                logger.debug("ProductDAOImpl:setProduct() The old excluded states have been removed");
            }
             
            //Save the state excluded delivery days
            if( excludedStates!=null ) {
                for( int idx=0; idx<excludedStates.size(); idx++)
                {
                    sdeVO = (StateDeliveryExclusionVO)excludedStates.get(idx);
                    if( sdeVO.isSunExcluded() || sdeVO.isMonExcluded() || sdeVO.isTueExcluded() ||
                        sdeVO.isWedExcluded() || sdeVO.isThuExcluded() || sdeVO.isFriExcluded() ||
                        sdeVO.isSatExcluded() ) 
                    {
                        inputParams = new HashMap();
                        inputParams.put("PRODUCT_ID",  productVO.getProductId());
                        inputParams.put("EXCL_STATE",  sdeVO.getExcludedState());
                        inputParams.put("EXCL_SUN", FTDUtil.convertBooleanToString(sdeVO.isSunExcluded()));
                        inputParams.put("EXCL_MON", FTDUtil.convertBooleanToString(sdeVO.isMonExcluded()));
                        inputParams.put("EXCL_TUE", FTDUtil.convertBooleanToString(sdeVO.isTueExcluded()));
                        inputParams.put("EXCL_WED", FTDUtil.convertBooleanToString(sdeVO.isWedExcluded()));
                        inputParams.put("EXCL_THR", FTDUtil.convertBooleanToString(sdeVO.isThuExcluded()));
                        inputParams.put("EXCL_FRI", FTDUtil.convertBooleanToString(sdeVO.isFriExcluded()));
                        inputParams.put("EXCL_SAT", FTDUtil.convertBooleanToString(sdeVO.isSatExcluded()));
                        dataRequest.reset();
                        dataRequest.setConnection(connection);
                        dataRequest.setInputParams(inputParams);
                        dataRequest.setStatementID("PDB_INSERT_EXCL_STATE");
                        dataAccessUtil.execute(dataRequest);
                    }
                    
                }
                if(debugEnabled)
                {
                    logger.debug("ProductDAOImpl:setProduct() Executed the update excluded state delivery days stored proc");
                }
            }

            // Remove the old ship methods
            inputParams = new HashMap();
            inputParams.put("PRODUCT_ID", productVO.getProductId());
            dataRequest.reset();
            dataRequest.setConnection(connection);
            dataRequest.setStatementID("PDB_REMOVE_SHIP_METHODS");
            dataRequest.setInputParams(inputParams);
             
            // execute the store prodcedure //
            dataAccessUtil.execute(dataRequest);
            

            if(debugEnabled)
            {
                logger.debug("ProductDAOImpl:setProduct() The old ship methods have been removed");
            }
                        
            // set the shipping methods
            List methodsList = productVO.getShipMethods();
            if(methodsList != null)
            {
                for(int i = 0; i < methodsList.size(); i++)
                {
                    //get delivery option vo
                    DeliveryOptionVO vo = (DeliveryOptionVO)methodsList.get(i); 
                    inputParams = new HashMap();               
                    inputParams.put("PRODUCT_ID", productVO.getProductId());
                    inputParams.put("SHIPPING_ID", vo.getShippingMethodId());
                    inputParams.put("CARRIER", vo.getDefaultCarrier());
                    
                    dataRequest.reset();
                    dataRequest.setConnection(connection);
                    dataRequest.setInputParams(inputParams);
                    dataRequest.setStatementID("PDB_INSERT_SHIP_METHOD");
                    dataAccessUtil.execute(dataRequest);
                }
                if(debugEnabled)
                {
                    logger.debug("ProductDAOImpl:setProduct() Executed the update delivery types stored proc");
                }
            }

            //Remove the old subcodes
            inputParams = new HashMap();
            inputParams.put("PRODUCT_ID", productVO.getProductId());
            dataRequest.reset();
            dataRequest.setConnection(connection);
            dataRequest.setStatementID("PDB_REMOVE_PRODUCT_SUBCODES");
            dataRequest.setInputParams(inputParams);
             
            // Set each of the subcodes
            List subcodesList = productVO.getSubCodeList();
            if(subcodesList != null)
            {
                for(int i = 0; i < subcodesList.size(); i++)
                {
                    inputParams = new HashMap();
                    inputParams.put("PRODUCT_ID", productVO.getProductId());
                    inputParams.put("SUBCODE_ID",  ((ProductSubCodeVO)subcodesList.get(i)).getProductSubCodeId());
                    inputParams.put("DESCRIPTION",  ((ProductSubCodeVO)subcodesList.get(i)).getSubCodeDescription());
                    inputParams.put("REF_NUMBER",  ((ProductSubCodeVO)subcodesList.get(i)).getSubCodeRefNumber());
                    inputParams.put("PRICE", new BigDecimal(((ProductSubCodeVO)subcodesList.get(i)).getSubCodePrice()));
                    inputParams.put("HOLIDAY_SKU",  ((ProductSubCodeVO)subcodesList.get(i)).getSubCodeHolidaySKU());
                    inputParams.put("HOLIDAY_PRICE", new BigDecimal(((ProductSubCodeVO)subcodesList.get(i)).getSubCodeHolidayPrice()));
                    inputParams.put("ACTIVE_FLAG",  FTDUtil.convertBooleanToString(((ProductSubCodeVO)subcodesList.get(i)).isActiveFlag()));
                    inputParams.put("DIM_WEIGHT",  ((ProductSubCodeVO)subcodesList.get(i)).getDimWeight());
                    inputParams.put("DISPLAY_ORDER",  new BigDecimal(((ProductSubCodeVO)subcodesList.get(i)).getDisplayOrder()));
                    inputParams.put("UPDATED_BY",  productVO.getLastUpdateUserId());
                    
                    dataRequest.reset();
                    dataRequest.setConnection(connection);
                    dataRequest.setInputParams(inputParams);
                    dataRequest.setStatementID("PDB_INSERT_PRODUCT_SUBCODE");
                    dataAccessUtil.execute(dataRequest);

                }
                if(debugEnabled)
                {
                    logger.debug("ProductDAOImpl:setProduct() Executed the update subcodes stored proc");
                }
            }
            
            // Check if any vendor has been removed or become unavailable. If so, those vendor-products need to be invalidated by PAS.
            List origVendorList = getVendorsForProduct(connection, productVO.getProductId());
            List toVendorList = productVO.getVendorProductsList();
            if(origVendorList != null) {
            	for(int i = 0; i < origVendorList.size(); i++)
                {
                    VendorProductVO vendorProductVO = (VendorProductVO)origVendorList.get(i);
                    String vendorId = vendorProductVO.getVendorId();
                    if(isVendorGone(vendorProductVO, toVendorList)) {
                    	logger.debug("ProductDAOImpl:setProduct() Inserting PAS messge for vendor invalidation");
                        insertPASJMSMessage(connection,"MODIFIED_VENDOR_PRODUCT " + vendorId +  " " + productVO.getProductId());
                    }
                }
            }
            
            // Mark old vendor records removed
            inputParams = new HashMap();
            inputParams.put("PRODUCT_ID", productVO.getProductId());
            dataRequest.reset();
            dataRequest.setConnection(connection);
            dataRequest.setStatementID("PDB_REMOVE_PRODUCT_VENDORS");
            dataRequest.setInputParams(inputParams);
            
            // execute the store prodcedure //
            dataAccessUtil.execute(dataRequest);
            
            if(debugEnabled)
            {
                logger.debug("ProductDAOImpl:setProduct() The old product vendors have been removed");
            }
            
            // Insert the new product vendors
            List vendorList = productVO.getVendorProductsList();
            if(vendorList != null)
            {
                for(int i = 0; i < vendorList.size(); i++)
                {
                    VendorProductVO vendorProductVO = (VendorProductVO)vendorList.get(i);
                    inputParams = new HashMap();
                    inputParams.put("VENDOR_ID", vendorProductVO.getVendorId());
                    inputParams.put("PRODUCT_SKU_ID", vendorProductVO.getProductSkuId());
                    inputParams.put("VENDOR_COST", new BigDecimal(vendorProductVO.getVendorCost()));
                    inputParams.put("VENDOR_SKU", vendorProductVO.getVendorSku());
                    inputParams.put("AVAILABLE", vendorProductVO.isAvailable()?"Y":"N");
                    inputParams.put("REMOVED", vendorProductVO.isRemoved()?"Y":"N");
                    inputParams.put("UPDATED_BY",  productVO.getLastUpdateUserId());
                    dataRequest.reset();
                    dataRequest.setConnection(connection);
                    dataRequest.setInputParams(inputParams);
                    dataRequest.setStatementID("PDB_UPDATE_PRODUCT_VENDOR");
                    dataAccessUtil.execute(dataRequest);

                }
            }           
            if(debugEnabled)
            {
                logger.debug("ProductDAOImpl:setProduct() Updated vendors for product");
            }
            
            // set the shipping dates
            inputParams = new HashMap();
            inputParams.put("PRODUCT_ID", productVO.getProductId());
            inputParams.put("MONDAY", FTDUtil.convertBooleanToString(productVO.isShipDayMonday()));
            inputParams.put("TUESDAY", FTDUtil.convertBooleanToString(productVO.isShipDayTuesday()));
            inputParams.put("WEDNESDAY", FTDUtil.convertBooleanToString(productVO.isShipDayWednesday()));
            inputParams.put("THURSDAY", FTDUtil.convertBooleanToString(productVO.isShipDayThursday()));
            inputParams.put("FRIDAY", FTDUtil.convertBooleanToString(productVO.isShipDayFriday()));
            inputParams.put("SATURDAY", FTDUtil.convertBooleanToString(productVO.isShipDaySaturday()));
            inputParams.put("SUNDAY", FTDUtil.convertBooleanToString(productVO.isShipDaySunday()));
            
            dataRequest.reset();
            dataRequest.setConnection(connection);
            dataRequest.setStatementID("PDB_UPDATE_PRODUCT_SHIP_DATES");
            dataRequest.setInputParams(inputParams);
            dataAccessUtil.execute(dataRequest);

            if(debugEnabled)
            {
                logger.debug("ProductDAOImpl:setProduct() set ship dates");
            }

            HashMap <String, ArrayList <ProductAddonVO>> productAddonMap;
            productAddonMap = productVO.getProductAddonMap();
            if (productAddonMap != null) 
            {
                for (String key : productAddonMap.keySet())
                {
                    for (ProductAddonVO productAddonVO : productAddonMap.get(key))
                    {
                        if (productAddonVO != null && productAddonVO.isModified() != null && 
                        		(productAddonVO.isModified()||(!productAddonVO.isModified() && productVO.isCopyProduct())))
                        {    
                            logger.debug("Setting product addon with id " + productAddonVO.getAddonId());
                                               
                            inputParams = new HashMap();
                            inputParams.put("IN_PRODUCT_ID", productVO.getProductId());
                            inputParams.put("IN_ADDON_ID", productAddonVO.getAddonId());
                            inputParams.put("IN_DISPLAY_SEQ_NUM", productAddonVO.getDisplaySequenceNumber());
                            inputParams.put("IN_MAX_QTY", productAddonVO.getMaxQuantity());
                            inputParams.put("IN_ACTIVE_FLAG", FTDUtil.convertBooleanToString(productAddonVO.getActiveFlag()));
                            inputParams.put("IN_UPDATED_BY", productVO.getLastUpdateUserId());
    
                            dataRequest.reset();
                            dataRequest.setConnection(connection);
                            dataRequest.setStatementID("PDB_UPDATE_PRODUCT_ADDON");
                            dataRequest.setInputParams(inputParams);
                            dataAccessUtil.execute(dataRequest);
                        }
                    } 
                }
            }
                
            if(debugEnabled)
            {
                logger.debug("ProductDAOImpl:setProduct() updated product add-ons");
            }

            // Inform Product Availability to update the product
            // Command is MODIFIED_PRODUCT
            // Arguments are productId YES
            // Yes to cascade changes through all product tables
            // No to not invalidate all previous calculation
            if (performPasUpdate)
            {
                logger.info("ProductDAOImpl:setProduct() Inserting PAS messge for product update");
                insertPASJMSMessage(connection,"MODIFIED_PRODUCT " + productVO.getProductId() + " Y N");
            }
            
            if(debugEnabled)
            {
                logger.debug("ProductDAOImpl:setProduct() Executed the update shipping days stored proc");
            }
            
            // Remove all website source codes for the product
            inputParams = new HashMap();
            inputParams.put("PRODUCT_ID", productVO.getProductId());
            dataRequest.reset();
            dataRequest.setConnection(connection);
            dataRequest.setStatementID("PDB_REMOVE_PRODUCT_SOURCE");
            dataRequest.setInputParams(inputParams);
            
            // execute the store prodcedure //
            dataAccessUtil.execute(dataRequest);

            if(debugEnabled)
            {
                logger.debug("ProductDAOImpl:setProduct() The old website source codes have been removed");
            }
            
            // Insert into the product_source table
            String websites = productVO.getWebsites();
            logger.info("");
            logger.info("websites: " + websites);
            if(websites == null)
            {
                websites = "";
            }
            st = new StringTokenizer(websites);
            List websitesList = new ArrayList();
            while (st.hasMoreTokens())
            {
                websitesList.add(st.nextToken());
            }

            if( websitesList!=null ) {
                for(int i = 0; i < websitesList.size(); i++)
                {
                    inputParams = new HashMap();
                    inputParams.put("PRODUCT_ID", productVO.getProductId());
                    inputParams.put("SOURCE_CODE", websitesList.get(i));
                    dataRequest.reset();
                    dataRequest.setConnection(connection);
                    dataRequest.setInputParams(inputParams);
                    dataRequest.setStatementID("PDB_INSERT_PRODUCT_SOURCE");
                    dataAccessUtil.execute(dataRequest);                    
                    logger.info("inserting " + websitesList.get(i));
                }
                if(debugEnabled)
                {
                    logger.debug("ProductDAOImpl:setProduct() Executed the update website source codes stored proc");
                }
            }

            txHelper.commitTransaction();
            
         // calling muBuys MDB after checking myBuysFeed flaq and also checking if current product price has been changed or product has been made unavailable  
            if (StringUtils.equalsIgnoreCase(myBuysFeedEnabled, "Y"))
            {
                if ((!productVO.isStatus() && storedProductVO.isStatus()) || 
                    (productVO.getStandardPrice() != storedProductVO.getStandardPrice()) || 
                    (productVO.getDeluxePrice() != storedProductVO.getDeluxePrice()) || 
                    (productVO.getPremiumPrice() != storedProductVO.getPremiumPrice()))
                {
                      sendMyBuys(productVO.getProductId());
                }
            }
            
          }
        catch(SQLException sqlException)
        {
            logger.error(sqlException);
            if(connection != null)
            {
                try
                {
                    connection.rollback();
                }
                catch(SQLException sqle)
                {
                    throw new BadConnectionException(ProductMaintConstants.OE_COULD_NOT_ROLLBACK, sqle);
                }
            }

            int errorCode;
            
            if(sqlException.getErrorCode() == ProductMaintConstants.LAST_UPDATE_EXCEPTION_CODE)
            {
                errorCode = ProductMaintConstants.LAST_UPDATE_EXCEPTION;
            }
            else
            {
                errorCode = ProductMaintConstants.SQL_EXCEPTION;
            }
            
            String[] args = new String[1];
            args[0] = new String(sqlException.getMessage());
            throw new PDBApplicationException(errorCode, args, sqlException);
        }
        catch(Exception e)
        {
            logger.error(e);
            if(connection != null)
            {
                try
                {
                    connection.rollback();
                }
                catch(SQLException sqle)
                {
                    throw new BadConnectionException(ProductMaintConstants.OE_COULD_NOT_ROLLBACK, sqle);
                }
            }
            throw new BadConnectionException(ProductMaintConstants.OE_BAD_CONNECTION_EXCEPTION,e);
        }
    }
    
    /**
     * Vendor is gone if vendor in original list is available and not removed and the same vendor in
     * the new list is either unavailable or removed or not found.
     * @param VendorProductVO: vendor in original list
     * @param vendorProductList: list to be updated to
     * @return
     * @throws PDBSystemException
     */
    private boolean isVendorGone(VendorProductVO vendorProductVO, List vendorProductList) throws PDBSystemException
    {
    	boolean result = false;
    	boolean found = false;
    	logger.debug(vendorProductVO.getVendorId()+ " Vendor available: " + vendorProductVO.isAvailable());
    	logger.debug(vendorProductVO.getVendorId()+ " Vendor removed: " + vendorProductVO.isRemoved());
    	
    	if(!vendorProductVO.isRemoved() && vendorProductVO.isAvailable()) {
    		if(vendorProductList != null) {
    			for(int i = 0; i < vendorProductList.size(); i++)
                {
                    VendorProductVO cur = (VendorProductVO)vendorProductList.get(i);
                    if(vendorProductVO.getVendorId().equals(cur.getVendorId())) {
                    	found = true;
                    	logger.debug(vendorProductVO.getVendorId()+ " In update to list vendor available: " + cur.isAvailable());
                    	logger.debug(vendorProductVO.getVendorId()+ " In update to list vendor removed: " + cur.isRemoved());
                    	if (cur.isRemoved() || !cur.isAvailable()) {
                    		result = true;
                    	}
                    	break;
                    }
                }
    			
    		} 
    		if(!found) {
    			result = true;
    			logger.debug(vendorProductVO.getVendorId()+ " Vendor is not found in update to list.");
    		}
    	}
    	
    	logger.debug(vendorProductVO.getVendorId()+ " isVendorGone returning: " + result);
    	return result;
    }

    /**
     * Returns an array of AribaCatalogRecordVOs
     * @param conn database connection
     * @param catalogFlag filter
     * @return array of catalog products
     * @exception PDBSystemException
     */
    public List getProductsByCatalog(Connection conn, String catalogFlag) throws PDBSystemException, PDBApplicationException
    {
        ArrayList products = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;

        try
        {
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_PRODUCT_BY_CATALOG");
            HashMap inputParams = new HashMap();
            inputParams.put("CATALOG_FLAG", catalogFlag);
            dataRequest.setInputParams(inputParams);
      
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);


            while(rs.next())
            {
                ProductVO product = new ProductVO();

                product.setNovatorId(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_NOVATOR_ID));
                product.setProductName(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_NOVATOR_NAME));
                product.setStandardPrice(rs.getFloat(ProductMaintConstants.OE_PRODUCT_MASTER_STANDARD_PRICE));
                product.setLongDescription(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_LONG_DESCRIPTION));
                product.setUnspscCode(rs.getString(ProductMaintConstants.OE_PRODUCT_MASTER_UNSPSC_CODE));
//                product.setExceptionStartDate(rs.getDate(ProductMaintConstants.OE_PRODUCT_MASTER_EXCEPTION_START_DATE));
//                product.setExceptionEndDate(rs.getDate(ProductMaintConstants.OE_PRODUCT_MASTER_EXCEPTION_END_DATE));

                 Date utilDate = rs.getDate(ProductMaintConstants.OE_PRODUCT_MASTER_EXCEPTION_START_DATE);
                 if( utilDate!=null ) {
                     product.setExceptionStartDate(ProductVO.PDB_FORMAT.format(utilDate));
                 } else {
                     product.setExceptionStartDate(null);
                 }
                 
                  utilDate = rs.getDate(ProductMaintConstants.OE_PRODUCT_MASTER_EXCEPTION_END_DATE);
                  if( utilDate!=null ) {
                      product.setExceptionEndDate(ProductVO.PDB_FORMAT.format(utilDate));
                  } else {
                      product.setExceptionEndDate(null);
                  }

                products.add(product);
            }
        }
        catch(Exception e)
        {
            String[] args = new String[1];
            args[0] = new String(e.getMessage());
            logger.error(e);
            throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, args, e);
        }

        return products;
    }


    /**
     * Remove holiday pricing for the passed in product
     * @param conn database connection
     * @param product id
     * @exception PDBSystemException
     */
    public void removeHolidayPricing(Connection conn, String product) throws PDBSystemException, PDBApplicationException
    {
        DataRequest dataRequest = new DataRequest();

        try
        {
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_REMOVE_HOLIDAY_PRICING");
            HashMap inputParams = new HashMap();
            inputParams.put("PRODUCT_ID", product);
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            dataAccessUtil.execute(dataRequest);

        }
        catch(Exception e)
        {
            String[] args = new String[1];
            args[0] = new String(e.getMessage());
            logger.error(e);
            throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, args, e);
        }
    }

    /**
     * Deletes a product from the database
     * @param conn database connection
     * @param productId that will be deleted in the database
     * @exception PDBSystemException
     */
    public void deleteProduct(Connection conn, String productId) throws PDBSystemException, PDBApplicationException
    {
        DataRequest dataRequest = new DataRequest();
        
        try
        {
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_DELETE_PRODUCT");
            HashMap inputParams = new HashMap();
            inputParams.put("PRODUCT_ID", productId);
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            dataAccessUtil.execute(dataRequest);           

        }
        catch(Exception e)
        {
            String[] args = new String[1];
            args[0] = new String(e.getMessage());
            logger.error(e);
            throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, args, e);
        }
    }

    /**
    * Returns a list of holiday SKUs for all products
    * @param conn database connection
    * @throws PDBSystemException
    **/
    public List getHolidaySKUList(Connection conn) throws PDBSystemException, PDBApplicationException
    {
        List list = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        try
        {
            HashMap inputParams = new HashMap();
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_SEND_MERCURY_RECIPE");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);


            while (rs.next())
            {
                ValueVO vo = new ValueVO();
                vo.setId(rs.getString(1));
                vo.setDescription(rs.getString(2));
                list.add(vo);
            }

            inputParams = new HashMap();
            /* build DataRequest object */
            dataRequest.reset();
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_SUBCODE_HOLIDAY_SKUS");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

            while (rs.next())
            {
                ValueVO vo = new ValueVO();
                vo.setId(rs.getString(1));
                vo.setDescription(rs.getString(2));
                list.add(vo);
            }            
        }
        catch(Exception e)
        {
            String[] args = new String[1];
            args[0] = new String(e.getMessage());
            logger.error(e);
            throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, args, e);
        }    
        
        return list;
    }

    /**
    * Returns a list of subcodes for all products
    * @param conn database connection
    * @throws PDBSystemException
   **/
    public List getSubCodeList(Connection conn) throws PDBSystemException, PDBApplicationException
    {
        List list = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        try
        {
            HashMap inputParams = new HashMap();
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_SUB_CODE_LIST");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
            


            while(rs.next())
            {
                ProductSubCodeVO vo = new ProductSubCodeVO();
                vo.setProductId(rs.getString(ProductMaintConstants.PDB_PRODUCT_SUBCODE_PRODUCT_ID));
                vo.setProductSubCodeId(rs.getString(ProductMaintConstants.PDB_PRODUCT_SUBCODE_SUBCODE_ID));
                //vo.setSubCodeDescription(FTDUtil.convertToXMLUnsafeCharacters(rs.getString(ProductMaintConstants.PDB_PRODUCT_SUBCODE_DESCRIPTION)));
                vo.setSubCodeDescription(rs.getString(ProductMaintConstants.PDB_PRODUCT_SUBCODE_DESCRIPTION));
                vo.setSubCodePrice(rs.getFloat(ProductMaintConstants.PDB_PRODUCT_SUBCODE_PRICE));
                vo.setSubCodeHolidaySKU(rs.getString(ProductMaintConstants.PDB_PRODUCT_SUBCODE_HOLIDAY_SKU));
                vo.setSubCodeHolidayPrice(rs.getFloat(ProductMaintConstants.PDB_PRODUCT_SUBCODE_HOLIDAY_PRICE));
//                vo.setVendorSKU(rs.getString(ProductMaintConstants.PDB_PRODUCT_SUBCODE_LIST_VENDOR_SKU));
                 vo.setDimWeight(rs.getString(ProductMaintConstants.PDB_PRODUCT_SUBCODE_LIST_DIM_WEIGHT));                    

                list.add(vo);
            }
        }
        catch(Exception e)
        {
            String[] args = new String[1];
            args[0] = new String(e.getMessage());
            logger.error(e);
            throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, args, e);
        }
        
        return list;
    }

  /**
   * Returns a list of subcodes with passed in subcode id
   * @param conn database connection
   * @param id (not used)
   * @throws PDBSystemException
   **/
   public List getSubCodesByID(Connection conn, String id) throws PDBSystemException, PDBApplicationException
   {
       List list = new ArrayList();
       DataRequest dataRequest = new DataRequest();
       CachedResultSet rs = null;
       try
       {
           HashMap inputParams = new HashMap();
           /* build DataRequest object */
           dataRequest.setConnection(conn);
           dataRequest.setStatementID("PDB_GET_HOLIDAY_SKU_LIST");
           dataRequest.setInputParams(inputParams);
           
           /* execute the store prodcedure */
           DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
           rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
           


           while (rs.next())
           {
               ValueVO vo = new ValueVO();
               vo.setId(rs.getString(1));
               vo.setDescription(rs.getString(2));
               list.add(vo);
           }
           
           inputParams = new HashMap();
           /* build DataRequest object */
           dataRequest.reset();
           dataRequest.setConnection(conn);
           dataRequest.setStatementID("PDB_GET_SUBCODE_HOLIDAY_SKUS");
           dataRequest.setInputParams(inputParams);
           
           /* execute the store prodcedure */
           dataAccessUtil = DataAccessUtil.getInstance();    
           rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
           

           
           while (rs.next())
           {
               ValueVO vo = new ValueVO();
               vo.setId(rs.getString(1));
               vo.setDescription(rs.getString(2));
               list.add(vo);
           }            
       }
       catch(Exception e)
       {
           String[] args = new String[1];
           args[0] = new String(e.getMessage());
           logger.error(e);
           throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, args, e);
       }   
       
       return list;
   }

    /**
    * Returns a list of states that can have delivery exceptions
    * @param conn database connection
    * @throws PDBSystemException
    **/
    public List getStateDeliveryExceptionList(Connection conn) throws PDBSystemException, PDBApplicationException
    {
        List excludedStateList = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        try
        {
            // Get all the excluded states
            if(debugEnabled)
            {
                logger.debug("ProductDAOImpl:getProduct() Getting excluded states");
            }
            HashMap inputParams = new HashMap();
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_DEL_EXCL_STATES");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

            
            StateDeliveryExclusionVO sdeVO;
            while(rs.next())
            {
                sdeVO = new StateDeliveryExclusionVO();
                sdeVO.setExcludedState(rs.getString(ProductMaintConstants.STATE_ID));
                sdeVO.setStateName(rs.getString(ProductMaintConstants.STATE_NAME));
                sdeVO.setSunExcluded(false);
                sdeVO.setMonExcluded(false);
                sdeVO.setTueExcluded(false);
                sdeVO.setWedExcluded(false);
                sdeVO.setThuExcluded(false);
                sdeVO.setFriExcluded(false);
                sdeVO.setSatExcluded(false);
                excludedStateList.add(sdeVO);
            }
        }
        catch(Exception e)
        {
            String[] args = new String[1];
            args[0] = new String(e.getMessage());
            logger.error(e);
            throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, args, e);
        }
        
        return excludedStateList;
    }

    /**
     * Returns an array of product ids that have been updated since the passed in date
     * @param conn database connection
     * @param lastUpdateDate return any products that have been updated since this date
     * @return array of product ids
     * @exception PDBSystemException
     * @exception PDBApplicationException
     */
    public List getProductsUpdatedSince(Connection conn, java.util.Date lastUpdateDate) throws PDBSystemException, PDBApplicationException
    {
        ArrayList products = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;

        try
        {
            HashMap inputParams = new HashMap();
            inputParams.put("IN_UPDATE_DATE",new java.sql.Date(lastUpdateDate.getTime()));
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_PRODUCTS_UPDATED_SINCE");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);


            while(rs.next())
            {
                products.add(rs.getString(1));
            }
        }
        catch(Exception e)
        {
            String[] args = new String[1];
            args[0] = new String(e.getMessage());
            logger.error(e);
            throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, args, e);
        }

        return products;
    }
    
    /**
     * Validates the passed in product id
     * @param conn database connection
     * @param productId to validate
     * @return true if the product id is a valid product id, novator id, or subsku
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public boolean validateProductId(Connection conn, String productId) 
        throws PDBSystemException, PDBApplicationException {
        
        boolean retval=false;
        DataRequest dataRequest = new DataRequest();

        try
        {
            HashMap inputParams = new HashMap();
            inputParams.put("PRODUCT_ID", productId);
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_VALIDATE_PRODUCT_ID");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance(); 
            
            Map outputParameters = (Map) dataAccessUtil.execute(dataRequest);
            

            String status = (String) outputParameters.get("STATUS");
            
            if( status.equalsIgnoreCase("N") ) 
            {
                String message = (String) outputParameters.get("MESSAGE");
                logger.error("Error returned from database in validateProductId: "+message);
                throw new Exception(message);
            }
            
            String testString = (String) outputParameters.get("RESULTS");
            if( StringUtils.equals("Y",testString) ) {
                retval=true;
            }
        }
        catch(Exception e)
        {
            String[] args = new String[1];
            args[0] = new String(e.getMessage());
            logger.error(e);
            throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, args, e);
        }
        
        return retval;
    }
    
    /**
     * Get a list of VendorProductVOs for vendors and their skus for the passed
     * in product/sku id
     * @param conn database connection
     * @param productSkuId to retrieve
     * @return List of VendorProductVOs for the passed in prduct/sku id
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public List getVendorsForProduct(Connection conn, String productSkuId) 
        throws PDBSystemException, PDBApplicationException {
        
        List list = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;

        try
        {
            HashMap inputParams = new HashMap();
            inputParams.put("PRODUCT_SKU_ID", productSkuId);
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_VENDORS_FOR_PRODUCT");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store procedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

            while(rs.next())
            {
                VendorProductVO vo = new VendorProductVO();
                vo.setVendorId(rs.getString("VENDOR_ID"));
                vo.setProductSkuId(rs.getString("PRODUCT_SUBCODE_ID"));
                vo.setVendorCost(rs.getDouble("VENDOR_COST"));
                vo.setVendorSku(rs.getString("VENDOR_SKU"));
                vo.setAvailable(StringUtils.equals(rs.getString("AVAILABLE"),"Y"));
                vo.setRemoved(StringUtils.equals(rs.getString("REMOVED"),"Y"));
                vo.setVendorName(rs.getString("VENDOR_NAME"));
                list.add(vo);
            }
        }  
        catch(Exception e)
        {
            String[] args = new String[1];
            args[0] = new String(e.getMessage());
            logger.error(e);
            throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, args, e);
        }
        
        return list;
    }

    /**
     * Returns a list of products for the passed vendorId
     * @param conn
     * @param vendorId filter
     * @return list of Strings containing the product Ids
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public List getVendorProducts(Connection conn, String vendorId) throws PDBSystemException, PDBApplicationException {
        
        List list = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;

        try
        {
            HashMap inputParams = new HashMap();
            inputParams.put("VENDOR_ID", vendorId);
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_VENDOR_PRODUCTS");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
            
            
            while (rs.next()) {
                list.add(rs.getString(1));
            }
        }  
        catch(Exception e)
        {
            String[] args = new String[1];
            args[0] = new String(e.getMessage());
            logger.error(e);
            throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, args, e);
        }
        
        return list;
    }


    /**
     * Returns a list of SDS ship methods for the passed carrierId
     * @param conn database connection
     * @param carrierId filter
     * @return list of Strings containing the product Ids
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public List getCarrierSdsShipMethods(Connection conn, String carrierId) throws PDBSystemException, PDBApplicationException {
    
        List list = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
      
        try
        {
            HashMap inputParams = new HashMap();
            inputParams.put("CARRIER_ID", carrierId);
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_CARRIER_SDS_SHIP_METHODS");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
            
            
            while (rs.next()) {
                list.add(rs.getString(1));
            }
        }  
        catch(Exception e)
        {
            String[] args = new String[1];
            args[0] = new String(e.getMessage());
            logger.error(e);
            throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, args, e);
        }
        
        return list;
    }

    /**
     * Get a list of VendorProductOverrideVO objects, filtered on the passed variables
     * @param conn database connection
     * @param vendorId filter
     * @param productId filter
     * @param overrideDate filter
     * @param carrierId filter
     * @return
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public List getVPOverrides(Connection conn, String vendorId, String productId, Date overrideDate, String carrierId) 
        throws PDBSystemException, PDBApplicationException {
        
        List list = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;

        try
        {
            HashMap inputParams = new HashMap();
            inputParams.put("VENDOR_ID", vendorId);
            inputParams.put("PRODUCT_ID", productId);
            if (overrideDate == null || StringUtils.isEmpty(overrideDate.toString()))
                inputParams.put("DELIVERY_OVERRIDE_DATE", null);
            else
                inputParams.put("DELIVERY_OVERRIDE_DATE", new java.sql.Date(overrideDate.getTime()));
  
            inputParams.put("CARRIER_ID", carrierId);
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_VENDOR_PRODUCT_OVERRIDES");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

            
            VendorProductOverrideVO vpoVO;
  
            //Define the format
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
  
            Date rsDeliveryOverrideDate = null;
            
            while (rs.next()) {
                vpoVO = new VendorProductOverrideVO();
                vpoVO.setVendorId(rs.getString("VENDOR_ID"));

                rsDeliveryOverrideDate = new java.util.Date(rs.getDate("DELIVERY_OVERRIDE_DATE").getTime());
                vpoVO.setDeliveryOverrideDate(rsDeliveryOverrideDate);
                vpoVO.setDeliveryOverrideDateFormatted(sdf.format(rsDeliveryOverrideDate));
  
                vpoVO.setCarrierId(rs.getString("CARRIER_ID"));
                vpoVO.setSdsShipMethod(rs.getString("SDS_SHIP_METHOD"));
                vpoVO.setVendorName(rs.getString("VENDOR_NAME"));
              
                vpoVO.setProductId(rs.getString("PRODUCT_SUBCODE_ID"));
               
                list.add(vpoVO);
            }
        }  
        catch(Exception e)
        {
            String[] args = new String[1];
            args[0] = new String(e.getMessage());
            logger.error(e);
            throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, args, e);
        }
        
        return list;
    }
    
    /**
     * Updates the values of the passed VendorProductOverrideVO in the VENDOR_PRODUCT_OVERRIDE table
     * @param conn
     * @param vpoVO data to update
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public void updateVPOverride(Connection conn, VendorProductOverrideVO vpoVO) throws PDBSystemException, PDBApplicationException {
        
        DataRequest dataRequest = new DataRequest();
        try
        {
            HashMap inputParams = new HashMap();
            inputParams.put("VENDOR_ID", vpoVO.getVendorId());
            inputParams.put("PRODUCT_ID", vpoVO.getProductId());
            inputParams.put("DELIVERY_OVERRIDE_DATE", new java.sql.Date(vpoVO.getDeliveryOverrideDate().getTime()));
            inputParams.put("CARRIER_ID", vpoVO.getCarrierId());
            inputParams.put("SDS_SHIP_METHOD", vpoVO.getSdsShipMethod());

            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_UPDATE_VENDOR_PRODUCT_OVERRIDE");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            Map outputs = (Map)dataAccessUtil.execute(dataRequest);
            

            
            String status = (String)outputs.get("OUT_STATUS");
            if (status != null && status.equalsIgnoreCase("N")) {
                String message = 
                    (String)outputs.get("OUT_MESSAGE");
                throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, new Exception(message));
            }
        }  
        catch(Exception e)
        {
            String[] args = new String[1];
            args[0] = new String(e.getMessage());
            logger.error(e);
            throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, args, e);
        }
    }
    
    /**
     * Delete the values of the passed VendorProductOverrideVO in the VENDOR_PRODUCT_OVERRIDE table
     * @param conn
     * @param vpoVO data to delete
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public void deleteVPOverride(Connection conn, VendorProductOverrideVO vpoVO) throws PDBSystemException, PDBApplicationException {
        
        DataRequest dataRequest = new DataRequest();
        try
        {
            HashMap inputParams = new HashMap();
            inputParams.put("VENDOR_ID", vpoVO.getVendorId());
            inputParams.put("PRODUCT_SUBCODE_ID", vpoVO.getProductId());
            inputParams.put("DELIVERY_OVERRIDE_DATE", new java.sql.Date(vpoVO.getDeliveryOverrideDate().getTime()));
            inputParams.put("CARRIER_ID", vpoVO.getCarrierId());
            inputParams.put("SDS_SHIP_METHOD", vpoVO.getSdsShipMethod());
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_DELETE_VENDOR_PRODUCT_OVERRIDE");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            Map outputs = (Map)dataAccessUtil.execute(dataRequest);

            
            String status = (String)outputs.get("OUT_STATUS");
            if (status != null && status.equalsIgnoreCase("N")) {
                String message = 
                    (String)outputs.get("OUT_MESSAGE");
                throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, new Exception(message));
            }
        }  
        catch(Exception e)
        {
            String[] args = new String[1];
            args[0] = new String(e.getMessage());
            logger.error(e);
            throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, args, e);
        }
    }

    /**
     * Checks to see if id exists in FTD_APPS.VENDOR_PRODUCTS
     * @param conn
     * @param productId to check
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */

    public String existsInVendorProduct(Connection conn, String productId) throws PDBSystemException, PDBApplicationException {
        
        DataRequest dataRequest = new DataRequest();
        try
        {
            HashMap inputParams = new HashMap();
            inputParams.put("IN_PRODUCT_SUBCODE_ID",productId);

            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("EXISTS_IN_VENDOR_PRODUCT");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            Map outputs = (Map)dataAccessUtil.execute(dataRequest);

            
            String status = (String)outputs.get("OUT_STATUS");
            if (status != null && status.equalsIgnoreCase("N")) {
                String message = 
                    (String)outputs.get("OUT_MESSAGE");
                throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, new Exception(message));
            }
            
            return (String)outputs.get("OUT_EXISTS");
        }  
        catch(Exception e)
        {
            String[] args = new String[1];
            args[0] = new String(e.getMessage());
            logger.error(e);
            throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, args, e);
        }
    }

    /**
     * Get a list of products who's availability need to be flipped because of 
     * product availablity needing to be triggered.
     * @param conn database connection
     * @return list of product ids that need to be switched.
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public List<String> getProductExceptionsToProcess(Connection conn) throws PDBSystemException, PDBApplicationException {
        List<String> list = new ArrayList<String>();
        CachedResultSet rs = null;
        DataRequest dataRequest = new DataRequest();
        
        try
        {
            HashMap inputParams = new HashMap();
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_EX_PRODUCTS_TO_PROCESS");
            dataRequest.setInputParams(inputParams);
            
             /* execute the store prodcedure */
             DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
             rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

             while(rs.next()) {
                 list.add(rs.getString(1));
             }
        }  
        catch(Exception e)
        {
            String[] args = new String[1];
            args[0] = new String(e.getMessage());
            logger.error(e);
            throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, args, e);
        }
        
        return list;
    }

    /**
     * Get the Amazon product master record from the PTN_AMAZON.AZ_PRODUCT_MASTER table 
     * for the passed in product id.
     * @param conn database connection
     * @param productId product id
     * @return AmazonProductMasterVO object
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public AmazonProductMasterVO getAmazonProductMaster(Connection conn, String productId) throws PDBSystemException, PDBApplicationException {
    	logger.info("getAmazonProductMaster(" + productId + ")");
    	AmazonProductMasterVO vo = new AmazonProductMasterVO();
        CachedResultSet rs = null;
        DataRequest dataRequest = new DataRequest();
        
        try
        {
            HashMap inputParams = new HashMap();
            inputParams.put("IN_PRODUCT_ID", productId);
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_AZ_PRODUCT_MASTER");
            dataRequest.setInputParams(inputParams);
            
             /* execute the store procedure */
             DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
             rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

             while (rs.next()) {
                 vo.setProductId(rs.getString("PRODUCT_ID"));
                 vo.setCatalogStatusStandard(rs.getString("CATALOG_STATUS_STANDARD"));
                 vo.setProductNameStandard(rs.getString("PRODUCT_NAME_STANDARD"));
                 vo.setProductDescriptionStandard(rs.getString("PRODUCT_DESCRIPTION_STANDARD"));
                 vo.setCatalogStatusDeluxe(rs.getString("CATALOG_STATUS_DELUXE"));
                 vo.setProductNameDeluxe(rs.getString("PRODUCT_NAME_DELUXE"));
                 vo.setProductDescriptionDeluxe(rs.getString("PRODUCT_DESCRIPTION_DELUXE"));
                 vo.setCatalogStatusPremium(rs.getString("CATALOG_STATUS_PREMIUM"));
                 vo.setProductNamePremium(rs.getString("PRODUCT_NAME_PREMIUM"));
                 vo.setProductDescriptionPremium(rs.getString("PRODUCT_DESCRIPTION_PREMIUM"));
                 vo.setItemType(rs.getString("ITEM_TYPE"));
                 logger.debug("found AZ_PRODUCT_MASTER: " + vo.getCatalogStatusStandard() + " " +
                		 vo.getCatalogStatusDeluxe() + " " + vo.getCatalogStatusPremium());
              }
        }  
        catch(Exception e)
        {
            String[] args = new String[1];
            args[0] = new String(e.getMessage());
            logger.error(e);
            throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, args, e);
        }
        
        return vo;
    }

    /**
     * Get all the Amazon product attribute records from the PTN_AMAZON.AZ_PRODUCT_ATTRIBUTES table 
     * for the passed in product id.
     * @param conn database connection
     * @param productId product id
     * @return list of AmazonProductAttributesVO objects
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public List<AmazonProductAttributesVO> getAmazonProductAttributes(Connection conn, String productId, String attributeType) throws PDBSystemException, PDBApplicationException {
    	logger.info("getAmazonProductAttributes(" + productId + ", " + attributeType + ")");
        List<AmazonProductAttributesVO> list = new ArrayList<AmazonProductAttributesVO>();
        CachedResultSet rs = null;
        DataRequest dataRequest = new DataRequest();
        
        try
        {
            HashMap inputParams = new HashMap();
            inputParams.put("IN_PRODUCT_ID",productId);
            inputParams.put("IN_ATTRIBUTE_TYPE", attributeType);
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_AZ_PRODUCT_ATTRIBUTES");
            dataRequest.setInputParams(inputParams);
            
             /* execute the store procedure */
             DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
             rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

             if( rs!=null ) {
                 while(rs.next()) {
                	 AmazonProductAttributesVO vo = new AmazonProductAttributesVO();
                     vo.setProductId(rs.getString("PRODUCT_ID"));
                     vo.setProductAttributeType(rs.getString("PRODUCT_ATTRIBUTE_TYPE"));
                     vo.setProductAttributeValue(rs.getString("PRODUCT_ATTRIBUTE_VALUE"));
                     vo.setSequence(rs.getInt("PRODUCT_ATTRIBUTE_SEQUENCE"));
                     list.add(vo);
                     logger.debug("found AZ_PRODUCT_ATTR: " + vo.getProductAttributeType() + " " +
                    		 vo.getProductAttributeValue() + " " + vo.getSequence());
                 }
             }
        }  
        catch(Exception e)
        {
            String[] args = new String[1];
            args[0] = new String(e.getMessage());
            logger.error(e);
            throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, args, e);
        }
        
        return list;
    }

    /**
     * Updates or inserts a record into the PTN_AMAZON.AZ_PRODUCT_MASTER table
     * @param conn database connection
     * @param vo contains data to apply to database record
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public void updateAmazonProductMaster(Connection conn, AmazonProductMasterVO vo) throws PDBSystemException, PDBApplicationException {        
        logger.debug("updateAmazonProductMaster: " + vo.getProductId());
    	DataRequest dataRequest = new DataRequest();
        try
        {
            // Remove the old Amazon product attributes
            HashMap inputParams = new HashMap();
            inputParams = new HashMap();
            inputParams.put("IN_PRODUCT_ID", vo.getProductId());
            inputParams.put("IN_CATALOG_STATUS_STANDARD", vo.getCatalogStatusStandard());
            inputParams.put("IN_PRODUCT_NAME_STANDARD", vo.getProductNameStandard());
            inputParams.put("IN_PRODUCT_DESC_STANDARD", vo.getProductDescriptionStandard());
            inputParams.put("IN_CATALOG_STATUS_DELUXE", vo.getCatalogStatusDeluxe());
            inputParams.put("IN_PRODUCT_NAME_DELUXE", vo.getProductNameDeluxe());
            inputParams.put("IN_PRODUCT_DESC_DELUXE", vo.getProductDescriptionDeluxe());
            inputParams.put("IN_CATALOG_STATUS_PREMIUM", vo.getCatalogStatusPremium());
            inputParams.put("IN_PRODUCT_NAME_PREMIUM", vo.getProductNamePremium());
            inputParams.put("IN_PRODUCT_DESC_PREMIUM", vo.getProductDescriptionPremium());
            inputParams.put("IN_ITEM_TYPE", vo.getItemType());
            dataRequest.reset();
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_UPDATE_AZ_PRODUCT_MASTER");
            dataRequest.setInputParams(inputParams);
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            dataAccessUtil.execute(dataRequest);

        }  
        catch(Exception e)
        {
            String[] args = new String[1];
            args[0] = new String(e.getMessage());
            logger.error(e);
            throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, args, e);
        }
    }

    /**
     * Removes and inserts records into the PTN_AMAZON.AZ_PRODUCT_ATTRIBUTES table
     * @param conn database connection
     * @param productId
     * @param vo contains data to apply to database record
     * @param attributeType
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public void updateAmazonProductAttributes(Connection conn, String productId, List<AmazonProductAttributesVO> attributeList, String attributeType) throws PDBSystemException, PDBApplicationException {        
        logger.debug("updateAmazonProductAttributes");
        DataRequest dataRequest = new DataRequest();
        try
        {
            // Remove the old Amazon product attributes
        	logger.debug("Removing old records for " + productId + " / " + attributeType);
            HashMap inputParams = new HashMap();
            inputParams = new HashMap();
            inputParams.put("IN_PRODUCT_ID", productId);
            inputParams.put("IN_ATTRIBUTE_TYPE", attributeType);
            dataRequest.reset();
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_REMOVE_AZ_PRODUCT_ATTR");
            dataRequest.setInputParams(inputParams);
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            dataAccessUtil.execute(dataRequest);

            // Insert the Amazon product attributes
            if( attributeList != null ) {
                for(int i = 0; i < attributeList.size(); i++ ) {
                	AmazonProductAttributesVO apaVO = attributeList.get(i);
                	if (apaVO.getProductAttributeValue() != null && apaVO.getProductAttributeValue() != "") {
                		logger.debug("Adding " + apaVO.getSequence() + " "  + apaVO.getProductAttributeValue());
                        inputParams = new HashMap();
                        inputParams.put("IN_PRODUCT_ID", apaVO.getProductId());
                        inputParams.put("IN_ATTRIBUTE_TYPE", apaVO.getProductAttributeType());
                        inputParams.put("IN_ATTRIBUTE_VALUE", apaVO.getProductAttributeValue());
                        inputParams.put("IN_SEQUENCE", apaVO.getSequence());
                        dataRequest.reset();
                        dataRequest.setConnection(conn);
                        dataRequest.setInputParams(inputParams);
                        dataRequest.setStatementID("PDB_INSERT_AZ_PRODUCT_ATTR");
                        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
                    
                        String status = (String) outputs.get("OUT_STATUS");
                        if(status != null && status.equalsIgnoreCase("N")) {
                            String message = (String) outputs.get("OUT_MESSAGE");
                            logger.error(message);
                            throw new Exception(message);
                        }
                	}
                }
            }

        }  
        catch(Exception e)
        {
            String[] args = new String[1];
            args[0] = new String(e.getMessage());
            logger.error(e);
            throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, args, e);
        }
    }

    /**
     * Determine if the vender has a shipping account for the passed in partner
     * @param conn database connection
     * @param vendorId filter
     * @param partnerId filter
     * @return true if the account exist else false
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public boolean hasPartnerShipAccount(Connection conn, String vendorId, String partnerId) throws PDBSystemException, PDBApplicationException {
        boolean retval = false;    
        DataRequest dataRequest = new DataRequest();
        try
        {
            HashMap inputParams = new HashMap();
            inputParams.put("IN_VENDOR_ID", vendorId);
            inputParams.put("IN_PARTNER_ID", partnerId);
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_HAS_PARTNER_SHIP_ACCOUNT");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            String result = (String)dataAccessUtil.execute(dataRequest);
            
            retval = StringUtils.equals("Y",result);
        }  
        catch(Exception e)
        {
            String[] args = new String[1];
            args[0] = new String(e.getMessage());
            logger.error(e);
            throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, args, e);
        }
        return retval;
    }
    
    /**
     * Determine if the source code is valid.
     * @param conn database conneciton
     * @param vendorId filter
     * @param partnerId filter
     * @return true if the account exist else false
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public boolean sourceCodeExists(Connection conn, String sourceCode) throws PDBSystemException, PDBApplicationException {
        boolean retval = false;    
        DataRequest dataRequest = new DataRequest();
        try
        {
            HashMap inputParams = new HashMap();
            inputParams.put("IN_SOURCE_CODE", sourceCode);
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_SOURCE_CODE_EXISTS");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            String result = (String)dataAccessUtil.execute(dataRequest);
            logger.debug("sourceCodeExists returning:" + result);
            retval = StringUtils.equals("Y",result);
        }  
        catch(Exception e)
        {
            String[] args = new String[1];
            args[0] = new String(e.getMessage());
            logger.error(e);
            throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, args, e);
        }
        return retval;
    }    
    
    /**
     * Insert a message into the PAS JMS queue.
     * @param con
     * @param payload message payload
     * @throws Exception
     */
    public void insertPASJMSMessage(Connection con, String payload) 
            throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(con);
        dataRequest.setStatementID("PIF_POST_A_MESSAGE");

        HashMap inputParams = new HashMap();
        inputParams.put("IN_QUEUE_NAME", "OJMS.PAS_COMMAND");
        inputParams.put("IN_CORRELATION_ID", "");
        inputParams.put("IN_PAYLOAD", payload);
        inputParams.put("IN_DELAY_SECONDS", 0L);
        dataRequest.setInputParams(inputParams);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map)dataAccessUtil.execute(dataRequest);

        String status = (String)outputs.get("OUT_STATUS");
        if (status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String)outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }

    }
    
    
   /**
     * calls the MDB for sending JMS message to myBuys queue
     * @param String productId of object to for whom messages need to be sent
     * @return void
   */
      
    public void sendMyBuys(String productId) throws Exception
    {
         
               MessageToken messageToken = new MessageToken();
               messageToken.setMessage(productId);
               messageToken.setStatus("MY BUYS");
               Dispatcher dispatcher = Dispatcher.getInstance();
               dispatcher.dispatchTextMessage(new InitialContext(),messageToken);
        
    }
    
    
    /**
     * Gets the upsell master product ID if the passed product is an upsell base product
     * @param conn
     * @param prodId
     * @return
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public List getUpsellMasterForBaseId(Connection conn, String prodId) 
            throws PDBSystemException, PDBApplicationException
    {
        ArrayList upsellProds = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;

        try
        {
            HashMap inputParams = new HashMap();
            inputParams.put("IN_PRODUCT_ID",prodId);
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_UPSELL_MASTER_FOR_BASE_ID");
            dataRequest.setInputParams(inputParams);
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

            while(rs.next())
            {
                upsellProds.add(rs.getString(1));
            }
        }
        catch(Exception e)
        {
            String[] args = new String[1];
            args[0] = new String(e.getMessage());
            logger.error(e);
            throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, 
                                              args, e);
        }
        return upsellProds;
    }


    /**
     * Gets the web-side product ID (i.e., Novator ID) for the passed product ID
     * @param conn
     * @param productId
     * @return
     * @throws Exception
     */
    public String getWebsiteProductId(Connection conn, String productId) throws Exception
    {
      String webProdId = null;
      CachedResultSet outputs = null;
      DataRequest dataRequest = new DataRequest();
      
      try {
          dataRequest.setConnection(conn);
          dataRequest.setStatementID("GET_PRODUCT_BY_ID");
          Map paramMap = new HashMap();
          paramMap.put("IN_PRODUCT_ID", productId);
          dataRequest.setInputParams(paramMap);
          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    
          outputs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
    
          while (outputs.next())
          {
            webProdId = outputs.getString("novatorId");
          }
      }
      catch(Exception e)
      {
        String[] args = new String[1];
        args[0] = new String(e.getMessage());
        logger.error(e);
        throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, 
                                          args, e);
      }

      return webProdId;
    }

    public void setAmazonProduct(Connection connection, ProductVO productVO) throws PDBSystemException, PDBApplicationException {
        logger.info("setAmazonProduct()");
    	try {
            AmazonProductMasterVO apmVO = new AmazonProductMasterVO();
            apmVO.setProductId(productVO.getProductId());
            if (productVO.isAzCatalogStatusStandard()) {
            	apmVO.setCatalogStatusStandard("A");
            } else {
            	apmVO.setCatalogStatusStandard("I");
            }
            apmVO.setProductNameStandard(productVO.getAzProductNameStandard());
            apmVO.setProductDescriptionStandard(productVO.getAzProductDescriptionStandard());
            if (productVO.isAzCatalogStatusDeluxe()) {
            	apmVO.setCatalogStatusDeluxe("A");
            } else {
            	apmVO.setCatalogStatusDeluxe("I");
            }
            apmVO.setProductNameDeluxe(productVO.getAzProductNameDeluxe());
            apmVO.setProductDescriptionDeluxe(productVO.getAzProductDescriptionDeluxe());
            if (productVO.isAzCatalogStatusPremium()) {
            	apmVO.setCatalogStatusPremium("A");
            } else {
            	apmVO.setCatalogStatusPremium("I");
            }
            apmVO.setProductNamePremium(productVO.getAzProductNamePremium());
            apmVO.setProductDescriptionPremium(productVO.getAzProductDescriptionPremium());
            apmVO.setItemType(productVO.getAzItemType());
            
            updateAmazonProductMaster(connection, apmVO);
            
            if(debugEnabled)
            {
                logger.debug("ProductDAOImpl:setProduct() Updated Amazon Product Master");
            }

            List<AmazonProductAttributesVO> apaList = new ArrayList();
            int sequenceCounter = 0;
            for (int i=1; i<6; i++) {
                AmazonProductAttributesVO apaVO = new AmazonProductAttributesVO();
                apaVO.setProductId(productVO.getProductId());
                apaVO.setProductAttributeType("BULLET_POINTS");
                apaVO.setSequence(i);
            	switch (i) {
            	case 1:
            		apaVO.setProductAttributeValue(productVO.getAzBulletPoint1());
            		break;
            	case 2:
            		apaVO.setProductAttributeValue(productVO.getAzBulletPoint2());
            		break;
            	case 3:
            		apaVO.setProductAttributeValue(productVO.getAzBulletPoint3());
            		break;
            	case 4:
            		apaVO.setProductAttributeValue(productVO.getAzBulletPoint4());
            		break;
            	case 5:
            		apaVO.setProductAttributeValue(productVO.getAzBulletPoint5());
            		break;
            	default:
            		logger.error("Too many attributes: " + i);
        	    }
            	if (apaVO.getProductAttributeValue() != null && apaVO.getProductAttributeValue() != "") {
            		sequenceCounter += 1;
            		apaVO.setSequence(sequenceCounter);
            	}
            	apaList.add(apaVO);
            }
            updateAmazonProductAttributes(connection, productVO.getProductId(), 
            		apaList, "BULLET_POINTS");
            
            apaList = new ArrayList();
            sequenceCounter = 0;
            for (int i=1; i<6; i++) {
                AmazonProductAttributesVO apaVO = new AmazonProductAttributesVO();
                apaVO.setProductId(productVO.getProductId());
                apaVO.setProductAttributeType("SEARCH_TERMS");
            	switch (i) {
            	case 1:
            		apaVO.setProductAttributeValue(productVO.getAzSearchTerm1());
            		break;
            	case 2:
            		apaVO.setProductAttributeValue(productVO.getAzSearchTerm2());
            		break;
            	case 3:
            		apaVO.setProductAttributeValue(productVO.getAzSearchTerm3());
            		break;
            	case 4:
            		apaVO.setProductAttributeValue(productVO.getAzSearchTerm4());
            		break;
            	case 5:
            		apaVO.setProductAttributeValue(productVO.getAzSearchTerm5());
            		break;
            	default:
            		logger.error("Too many attributes: " + i);
        	    }
            	if (apaVO.getProductAttributeValue() != null && apaVO.getProductAttributeValue() != "") {
            		sequenceCounter += 1;
            		apaVO.setSequence(sequenceCounter);
            	}
            	apaList.add(apaVO);
            }
            updateAmazonProductAttributes(connection, productVO.getProductId(), 
            		apaList, "SEARCH_TERMS");

            apaList = new ArrayList();
            sequenceCounter = 0;
            for (int i=1; i<6; i++) {
                AmazonProductAttributesVO apaVO = new AmazonProductAttributesVO();
                apaVO.setProductId(productVO.getProductId());
                apaVO.setProductAttributeType("INTENDED_USE");
            	switch (i) {
            	case 1:
            		apaVO.setProductAttributeValue(productVO.getAzIntendedUse1());
            		break;
            	case 2:
            		apaVO.setProductAttributeValue(productVO.getAzIntendedUse2());
            		break;
            	case 3:
            		apaVO.setProductAttributeValue(productVO.getAzIntendedUse3());
            		break;
            	case 4:
            		apaVO.setProductAttributeValue(productVO.getAzIntendedUse4());
            		break;
            	case 5:
            		apaVO.setProductAttributeValue(productVO.getAzIntendedUse5());
            		break;
            	default:
            		logger.error("Too many attributes: " + i);
        	    }
            	if (apaVO.getProductAttributeValue() != null && apaVO.getProductAttributeValue() != "") {
            		sequenceCounter += 1;
            		apaVO.setSequence(sequenceCounter);
            	}
            	apaList.add(apaVO);
            }
            updateAmazonProductAttributes(connection, productVO.getProductId(), 
            		apaList, "INTENDED_USE");
            
            apaList = new ArrayList();
            sequenceCounter = 0;
            for (int i=1; i<6; i++) {
                AmazonProductAttributesVO apaVO = new AmazonProductAttributesVO();
                apaVO.setProductId(productVO.getProductId());
                apaVO.setProductAttributeType("TARGET_AUDIENCE");
            	switch (i) {
            	case 1:
            		apaVO.setProductAttributeValue(productVO.getAzTargetAudience1());
            		break;
            	case 2:
            		apaVO.setProductAttributeValue(productVO.getAzTargetAudience2());
            		break;
            	case 3:
            		apaVO.setProductAttributeValue(productVO.getAzTargetAudience3());
            		break;
            	case 4:
            		apaVO.setProductAttributeValue(productVO.getAzTargetAudience4());
            		break;
            	default:
            		logger.error("Too many attributes: " + i);
        	    }
            	if (apaVO.getProductAttributeValue() != null && apaVO.getProductAttributeValue() != "") {
            		sequenceCounter += 1;
            		apaVO.setSequence(sequenceCounter);
            	}
            	apaList.add(apaVO);
            }
            updateAmazonProductAttributes(connection, productVO.getProductId(), 
            		apaList, "TARGET_AUDIENCE");
            
            apaList = new ArrayList();
            sequenceCounter = 0;
            for (int i=1; i<6; i++) {
                AmazonProductAttributesVO apaVO = new AmazonProductAttributesVO();
                apaVO.setProductId(productVO.getProductId());
                apaVO.setProductAttributeType("OTHER_ATTRIBUTES");
            	switch (i) {
            	case 1:
            		apaVO.setProductAttributeValue(productVO.getAzOtherAttribute1());
            		break;
            	case 2:
            		apaVO.setProductAttributeValue(productVO.getAzOtherAttribute2());
            		break;
            	case 3:
            		apaVO.setProductAttributeValue(productVO.getAzOtherAttribute3());
            		break;
            	case 4:
            		apaVO.setProductAttributeValue(productVO.getAzOtherAttribute4());
            		break;
            	case 5:
            		apaVO.setProductAttributeValue(productVO.getAzOtherAttribute5());
            		break;
            	default:
            		logger.error("Too many attributes: " + i);
        	    }
            	if (apaVO.getProductAttributeValue() != null && apaVO.getProductAttributeValue() != "") {
            		sequenceCounter += 1;
            		apaVO.setSequence(sequenceCounter);
            	}
            	apaList.add(apaVO);
            }
            updateAmazonProductAttributes(connection, productVO.getProductId(), 
            		apaList, "OTHER_ATTRIBUTES");
            
            apaList = new ArrayList();
            sequenceCounter = 0;
            for (int i=1; i<6; i++) {
                AmazonProductAttributesVO apaVO = new AmazonProductAttributesVO();
                apaVO.setProductId(productVO.getProductId());
                apaVO.setProductAttributeType("SUBJECT_MATTER");
            	switch (i) {
            	case 1:
            		apaVO.setProductAttributeValue(productVO.getAzSubjectMatter1());
            		break;
            	case 2:
            		apaVO.setProductAttributeValue(productVO.getAzSubjectMatter2());
            		break;
            	case 3:
            		apaVO.setProductAttributeValue(productVO.getAzSubjectMatter3());
            		break;
            	case 4:
            		apaVO.setProductAttributeValue(productVO.getAzSubjectMatter4());
            		break;
            	case 5:
            		apaVO.setProductAttributeValue(productVO.getAzSubjectMatter5());
            		break;
            	default:
        	    }
            	if (apaVO.getProductAttributeValue() != null && apaVO.getProductAttributeValue() != "") {
            		sequenceCounter += 1;
            		apaVO.setSequence(sequenceCounter);
            	}
            	apaList.add(apaVO);
            }
            updateAmazonProductAttributes(connection, productVO.getProductId(), 
            		apaList, "SUBJECT_MATTER");

            if(debugEnabled)
            {
                logger.debug("ProductDAOImpl:setProduct() Updated Amazon Product Attributes");
            }
        	
        } catch (Exception e) {
        	logger.error(e);
            if(connection != null)
            {
                try {
                    connection.rollback();
                } catch(SQLException sqle) {
                    throw new BadConnectionException(ProductMaintConstants.OE_COULD_NOT_ROLLBACK, sqle);
                }
            }
            throw new BadConnectionException(ProductMaintConstants.OE_BAD_CONNECTION_EXCEPTION,e);
        	
        }
    }
    
    /**
     * Gets the Product List for PDB Mass Edit based on search criteria entered.
     * 
     * @param conn
     * @param search type 'M' - by master sku or 'P' by product id
     * @param String array containing either 1-5 master skus or 1-5 product ids
     * @return
     * @throws PDBApplicationException
     * @throws PDBSystemException
     */
    public List<ProductVO> getProductMassEditList(Connection conn, String searchType, 
            String[] searchItems) 
           throws PDBApplicationException, PDBSystemException {
    
    	List<ProductVO> pdbMassEditList = new ArrayList<ProductVO>();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        HashMap inputParams = new HashMap();
     
        try {
        	if ("M".equalsIgnoreCase(searchType)) {
        	   // searching by master sku
        	   inputParams.put("IN_MASTER_SKU1",searchItems[0]);
        	   inputParams.put("IN_MASTER_SKU2",searchItems[1]);
        	   inputParams.put("IN_MASTER_SKU3",searchItems[2]);
        	   inputParams.put("IN_MASTER_SKU4",searchItems[3]);
        	   inputParams.put("IN_MASTER_SKU5",searchItems[4]);
        	   dataRequest.setStatementID("PDB_GET_MASS_EDIT_BY_SKU");
        	}
        	else {
         	   // searching by master product id
         	   inputParams.put("IN_PRODUCT1",searchItems[0]);
         	   inputParams.put("IN_PRODUCT2",searchItems[1]);
         	   inputParams.put("IN_PRODUCT3",searchItems[2]);
         	   inputParams.put("IN_PRODUCT4",searchItems[3]);
         	   inputParams.put("IN_PRODUCT5",searchItems[4]);
         	   dataRequest.setStatementID("PDB_GET_MASS_EDIT_BY_PRODUCT");
         	}
        	dataRequest.setConnection(conn);
            dataRequest.setInputParams(inputParams);
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
            ProductVO vo = null;
            while(rs.next())
             {
                vo = new ProductVO();
                // master sku is not needed in the spreadsheet so don't worry about it for now. 
                vo.setMassEditMasterSKU(rs.getString("MASTER_SKU"));  
                vo.setProductId(rs.getString("PRODUCT_ID"));
                vo.setProductName(rs.getString("PRODUCT_NAME"));
                vo.setNovatorId(rs.getString("NOVATOR_ID")); 
                vo.setNovatorName(rs.getString("NOVATOR_NAME")); 
                vo.setFloristReferenceNumber(rs.getString("FLORIST_REFERENCE_NUMBER"));
                vo.setCategory(rs.getString("CATEGORY"));
                vo.setProductType(rs.getString("PRODUCT_TYPE"));
                vo.setProductSubType(rs.getString("PRODUCT_SUB_TYPE"));
                vo.setLongDescription(rs.getString("LONG_DESCRIPTION"));
                String dbStatus = rs.getString("STATUS"); 
                if(dbStatus.equals(ProductMaintConstants.PRODUCT_AVAILABLE_KEY)) {
                    vo.setMassEditProductStatus("Available");
                }
                else {
                	vo.setMassEditProductStatus("Unavailable");
                }
                vo.setDeliveryType(rs.getString("DELIVERY_TYPE"));
                vo.setShipMethodFlorist(FTDUtil.convertStringToBoolean(rs.getString("SHIP_METHOD_FLORIST")));
                vo.setShipMethodCarrier(FTDUtil.convertStringToBoolean(rs.getString("SHIP_METHOD_CARRIER")));
                vo.setRecipe(rs.getString("STANDARD_RECIPE")); 
                vo.setDeluxeRecipe(rs.getString("DELUXE_RECIPE"));
                vo.setPremiumRecipe(rs.getString("PREMIUM_RECIPE"));
                vo.setDimWeight(rs.getString("DIM_WEIGHT"));
                vo.setStandardPrice(rs.getFloat("STANDARD_PRICE"));
                vo.setDeluxePrice(rs.getFloat("DELUXE_PRICE"));
                vo.setPremiumPrice(rs.getFloat("PREMIUM_PRICE"));
                vo.setSentToNovatorProd(FTDUtil.convertStringToBoolean(rs.getString("SENT_TO_NOVATOR_PROD")));
                vo.setGeneralComments(rs.getString("GENERAL_COMMENTS"));
                /*
                 * DI-32: PDB Mass Edit - Add new fields that are coming from West.
                 * Populate ProductVO object with properties: pQuad product ID and MercuryDescription.
                 */
                vo.setMercuryDescription(rs.getString("MERCURY_DESCRIPTION"));
                vo.setPquadProductID(rs.getString("PQUAD_PRODUCT_ID"));
                pdbMassEditList.add(vo);
             }
         }
         catch(Exception e)
         {
             String[] args = new String[1];
             args[0] = new String(e.getMessage());
             logger.error(e);
             throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, 
                                               args, e);
         }
         return pdbMassEditList;
    }
    
    /**
     * Gets a count of the vendors available for a product (for PDB Mass Edit)
     * @param conn
     * @param productId
     * @return count returned from query
     * @throws PDBApplicationException
     * @throws PDBSystemException
     */
    public BigDecimal getVendorsAvailableForProduct(Connection conn, String productId) 
            throws PDBApplicationException, PDBSystemException {
    
    	BigDecimal vendorCount = new BigDecimal(-1);
        DataRequest dataRequest = new DataRequest();
        HashMap inputParams = new HashMap();
        try {
           inputParams.put("IN_PRODUCT",productId);
           dataRequest.setStatementID("PDB_GET_VENDOR_PRDCT_AVAIL");
           dataRequest.setConnection(conn);
           dataRequest.setInputParams(inputParams);
           DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
           vendorCount =  (BigDecimal) dataAccessUtil.execute(dataRequest);
         }
         catch(Exception e)
         {
             e.printStackTrace();
        	 String[] args = new String[1];
             args[0] = new String(e.getMessage());
             logger.error(e);
             throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, 
                                               args, e);
         }
         return vendorCount;
    }
    /**
     * Returns an array of ProductVOs based on filtering criteria
     * @param conn database connection
     * @return array of products
     * @exception PDBSystemException
     */
    public List getPQuadProductList(Connection conn, String pquadproductId)throws PDBSystemException, PDBApplicationException
    {
    	 DataRequest dataRequest = new DataRequest();
         CachedResultSet rs = null;
         ArrayList<ProductVO> products = new ArrayList<ProductVO>();

         try
         {
             HashMap inputParams = new HashMap();
             inputParams.put("AVAIL", null);
             inputParams.put("UNAVAIL", null);
             inputParams.put("DATE_RESTRICT", null);
             inputParams.put("ON_HOLD", null);
             inputParams.put("PQUAD_PRODUCT_ID", new BigDecimal(pquadproductId));
             
             /* build DataRequest object */
             dataRequest.setConnection(conn);
             dataRequest.setStatementID("PDB_GET_PRODUCT_LIST_ADVANCED");
            // dataRequest.setStatementID("PDB_GET_PQUAD_PRODUCTS");
             dataRequest.setInputParams(inputParams);
             
             /* execute the store prodcedure */
             DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
             rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);


             while(rs.next())
             {
			     ProductVO product = new ProductVO();
			     product.setProductId(rs.getString(ProductMaintConstants.OE_PRODUCT_LIST_ID));
			     product.setNovatorName(rs.getString(ProductMaintConstants.OE_PRODUCT_LIST_NAME));
			     products.add(product);
			 }
             
             //Logic to sort based on ProductId's even we have Procedure .It is sort the records based on Order by status and ProductId.That should not be changeable.
             //So we wrote this sort logic .No Null Pointer Exception
              Collections.sort(products,new Comparator<ProductVO>() {
            	  @Override
              	public int compare(ProductVO p1, ProductVO p2)
              	{
              		return p1.getProductId().compareTo(p2.getProductId());
              				
              	}
    		});
         }
         
         catch(Exception e)
         {
             String[] args = new String[1];
             args[0] = new String(e.getMessage());
             logger.error(e);
             throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, args, e);
         }

         return products;
    }

    
    public void projectFreshBulkUpdate(Connection conn, String source, String dateStr) throws PDBApplicationException
    {
      Map outputs = null;
      DataRequest dataRequest = new DataRequest();
      
      try {
          dataRequest.setConnection(conn);
          dataRequest.setStatementID("PROJECT_FRESH_BULK_UPDATE");
          Map paramMap = new HashMap();
          paramMap.put("IN_SOURCE", source);
          paramMap.put("IN_UPDATED_AFTER", dateStr);
          dataRequest.setInputParams(paramMap);
          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    
          outputs = (Map)dataAccessUtil.execute(dataRequest);
          String status = (String)outputs.get("OUT_STATUS");
          if (status != null && status.equalsIgnoreCase("N"))
          {
              String message = (String)outputs.get("OUT_MESSAGE");
              throw new Exception(message);
          }    
      }
      catch(Exception e)
      {
        String[] args = new String[1];
        args[0] = new String(e.getMessage());
        logger.error(e);
        throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, 
                                          args, e);
      }
    }
    
    
    public void projectFreshAddonUpdate(Connection conn, String addonId) throws PDBApplicationException
    {
      Map outputs = null;
      DataRequest dataRequest = new DataRequest();
      
      try {
          dataRequest.setConnection(conn);
          dataRequest.setStatementID("PROJECT_FRESH_ADDON_UPDATE");
          Map paramMap = new HashMap();
          paramMap.put("IN_ADDON_ID", addonId);
          dataRequest.setInputParams(paramMap);
          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    
          outputs = (Map)dataAccessUtil.execute(dataRequest);
          String status = (String)outputs.get("OUT_STATUS");
          if (status != null && status.equalsIgnoreCase("N"))
          {
              String message = (String)outputs.get("OUT_MESSAGE");
              throw new Exception(message);
          }    
      }
      catch(Exception e)
      {
        String[] args = new String[1];
        args[0] = new String(e.getMessage());
        logger.error(e);
        throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, 
                                          args, e);
      }
    }

}

