package com.ftd.pdb.integration.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.exceptions.BadConnectionException;
import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBSystemException;
import com.ftd.pdb.common.valobjs.AddonVO;


import java.sql.Connection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
   *  Addon DAO Implemenation
   **/
public class AddonDAOImpl extends PDBDataAccessObject implements IAddonDAO {
    public AddonDAOImpl() {
        super("com.ftd.pdb.dao.AddonDAOImpl");
    }


    /**
   * Retrieves all addon data from database for a given occasion and type.
   * @param conn database connection
   * @param type of addon
   * @param occasionID id
   * @return CardVO[]
   *
   */
    public List getAddonsByTypeOccasion(Connection conn, String type, 
                                        int occasionID) throws PDBSystemException, 
                                                               PDBApplicationException {
        ArrayList addonList = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;

        try {
            HashMap inputParams = new HashMap();
            inputParams.put("TYPE", type);
            inputParams.put("OCCASION", occasionID);
            inputParams.put("ADDON_ID", "");

            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_ADDON_BY_TYPE_OCCASION");
            dataRequest.setInputParams(inputParams);

            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);

            while (rs.next()) {
                AddonVO addon = new AddonVO();
                addon.setId(rs.getString(ProductMaintConstants.OE_ADDON_ID));
                addon.setDescription(rs.getString(ProductMaintConstants.OE_ADDON_DESCRIPTION));
                addonList.add(addon);
            }
        } catch (Exception e) {
            logger.error(e);
            throw new BadConnectionException(ProductMaintConstants.OE_BAD_CONNECTION_EXCEPTION, 
                                             e);
        }

        return addonList;
    }


    /**
   * Retrieves all addon data from database for a type excluding the
   * passed in occasion.
   * @param conn database connection
   * @param type of addon
   * @param occasionID ID
   * @return CardVO[]
   *
   */
    public List getAddonsByTypeExcludingOccasion(Connection conn, String type, 
                                                 int occasionID) throws PDBSystemException, 
                                                                        PDBApplicationException {
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        ArrayList addonList = new ArrayList();

        try {
            HashMap inputParams = new HashMap();
            inputParams.put("TYPE", type);
            inputParams.put("OCCASION_ID", occasionID);

            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_ADDON_BY_TYPE_EX_OCC");
            dataRequest.setInputParams(inputParams);

            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);

            while (rs.next()) {
                AddonVO addon = new AddonVO();
                addon.setId(rs.getString(ProductMaintConstants.OE_ADDON_ID));
                addon.setDescription(rs.getString(ProductMaintConstants.OE_ADDON_DESCRIPTION));
                addonList.add(addon);
            }
        } catch (Exception e) {
            logger.error(e);
            throw new BadConnectionException(ProductMaintConstants.OE_BAD_CONNECTION_EXCEPTION, 
                                             e);
        }

        return addonList;
    }


}
