package com.ftd.pdb.integration.dao;

import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBSystemException;
import com.ftd.pdb.common.valobjs.MassEditDetailVO;
import com.ftd.pdb.common.valobjs.MassEditFileStatusVO;
import com.ftd.pdb.common.valobjs.MassEditHeaderVO;
import java.sql.Connection;
import java.util.List;

public interface IProductMassEditDAO {
	
	public Long insertMassEditHeader(Connection conn,MassEditHeaderVO headerVO) 
	   throws PDBSystemException, PDBApplicationException;
	
	public Long insertMassEditDetail(Connection conn,MassEditDetailVO detailVO) 
	   throws PDBSystemException, PDBApplicationException;
	
	public void updateMassEditHeader(Connection conn,MassEditHeaderVO headerVO) 
	   throws PDBSystemException, PDBApplicationException;
	
	public List<MassEditFileStatusVO> getMassEditFileStatus(Connection conn) 
	   throws PDBSystemException, PDBApplicationException;
	

}
