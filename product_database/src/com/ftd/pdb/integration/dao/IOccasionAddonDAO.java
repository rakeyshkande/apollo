package com.ftd.pdb.integration.dao;

import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBSystemException;

import java.sql.Connection;

public interface IOccasionAddonDAO
{

   /**
    * Insert passed in occasion, addon.
    * @param conn database connection
    * @param occasionID ID
    * @param addonID ID
    * @return CardVO[]
    *
    */
     public void insert(Connection conn, int occasionID, String addonID) 
             throws PDBSystemException, PDBApplicationException;


   /**
    * Delete all rows for the passed in occasion.
    * @param conn database connection
    * @param occasionID ID
    * @return CardVO[]
    *
    */
     public void deleteByOccasion(Connection conn, int occasionID) 
             throws PDBSystemException, PDBApplicationException;

            

}