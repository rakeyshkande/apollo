package com.ftd.pdb.integration.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.exceptions.BadConnectionException;
import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBSystemException;
import com.ftd.pdb.common.utilities.FTDUtil;
import com.ftd.pdb.common.valobjs.ProductVO;
import com.ftd.pdb.common.valobjs.SearchKeyVO;
import com.ftd.pdb.common.valobjs.UpsellDetailVO;
import com.ftd.pdb.common.valobjs.UpsellMasterVO;


import com.ftd.pdb.common.valobjs.ValueVO;

import java.math.BigDecimal;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import java.util.Map;

import org.apache.commons.lang.StringUtils;


/**
   * Implementation of Upsell DAO
   *
   */

public class UpsellDAOImpl  extends PDBDataAccessObject implements IUpsellDAO
{

    protected static Logger logger = new Logger("com.ftd.pdb.integration.dao.UpsellDAOImpl");

    /**
    *   Constructor for the ProductDAOImpl class
    */
    public UpsellDAOImpl()
    {
        super("com.ftd.pdb.dao.UpsellDAOImpl");
    }

 /**
   * Returns an array of MasterUpsellVOs
   * @param conn database connection
   * @return array of MasterUpsellVOs
   * @throws PDBApplicationException, PDBSystemException
   **/
   public List getUpsellList(Connection conn)  throws PDBSystemException, PDBApplicationException
    {
        ArrayList skus = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;

        try
        {
            HashMap inputParams = new HashMap();
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_UPSELL_LIST");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

            while(rs.next())
            {
                UpsellMasterVO upsellMasterVO = new UpsellMasterVO();
                upsellMasterVO.setMasterID(rs.getString(ProductMaintConstants.PDB_UPSELLMASTER_MASTER_ID));
                //upsellMasterVO.setMasterName(FTDUtil.convertToXMLUnsafeCharacters(rs.getString(ProductMaintConstants.PDB_UPSELLMASTER_MASTER_NAME)));
                upsellMasterVO.setMasterName(rs.getString(ProductMaintConstants.PDB_UPSELLMASTER_MASTER_NAME));
                //upsellMasterVO.setMasterDescription(FTDUtil.convertToXMLUnsafeCharacters(rs.getString(ProductMaintConstants.PDB_UPSELLMASTER_MASTER_DESCRIPTION)));
                upsellMasterVO.setMasterDescription(rs.getString(ProductMaintConstants.PDB_UPSELLMASTER_MASTER_DESCRIPTION));
                String status = rs.getString(ProductMaintConstants.PDB_UPSELLMASTER_MASTER_STATUS);
                if(status.equals(ProductMaintConstants.SKU_AVAILABLE_KEY))
                {
                    upsellMasterVO.setMasterStatus(true);
                }
                else
                {
                    upsellMasterVO.setMasterStatus(false);
                }
                //upsellMasterVO.setGbbPopoverFlag(StringUtils.equals("Y",rs.getString(ProductMaintConstants.PDB_UPSELLMASTER_GBB_POPOVER_FLAG)));
                //upsellMasterVO.setGbbTitle(rs.getString(ProductMaintConstants.PDB_UPSELLMASTER_GBB_TITLE));

                //populate the associated skus
                upsellMasterVO.setUpsellDetailList(getUpsellDetailList(conn,upsellMasterVO));
                
                skus.add(upsellMasterVO);
            }
        }
        catch(Exception e)
        {
            String[] args = new String[1];
            args[0] = new String(e.getMessage());
            logger.error(e);
            throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, args, e);
        }

        return skus;
    }


 /**
   * Return the passed in Master SKU
   * @param conn database connection
   * @return MasterSKUVO
   * @throws PDBApplicationException, PDBSystemException
   **/
   public UpsellMasterVO getMasterSKU(Connection conn,String sku)
        throws PDBApplicationException, PDBSystemException
    {
        UpsellMasterVO upsellVO = null;
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;

        try
        {
            HashMap inputParams = new HashMap();
            inputParams.put("IN_SKU_ID", sku);
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_SKU_DETAILS");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance(); 
            HashMap outMap = (HashMap) dataAccessUtil.execute(dataRequest);
            rs = (CachedResultSet) outMap.get("OUT_SKU_CUR");

            if(rs.next())
            {
                upsellVO = new UpsellMasterVO();
                upsellVO.setMasterID(rs.getString(ProductMaintConstants.PDB_UPSELLMASTER_MASTER_ID));
                //upsellVO.setMasterName(FTDUtil.convertToXMLUnsafeCharacters(rs.getString(ProductMaintConstants.PDB_UPSELLMASTER_MASTER_NAME)));
                upsellVO.setMasterName(rs.getString(ProductMaintConstants.PDB_UPSELLMASTER_MASTER_NAME));
                //upsellVO.setMasterDescription(FTDUtil.convertToXMLUnsafeCharacters(rs.getString(ProductMaintConstants.PDB_UPSELLMASTER_MASTER_DESCRIPTION)));
                upsellVO.setMasterDescription(rs.getString(ProductMaintConstants.PDB_UPSELLMASTER_MASTER_DESCRIPTION));
                upsellVO.setSearchPriority(rs.getString(ProductMaintConstants.PDB_UPSELLMASTER_SEARCH_PRIORITY ));
                upsellVO.setCorporateSiteFlag(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.PDB_UPSELLMASTER_CORPORATE_SITE)));                
                String status = rs.getString(ProductMaintConstants.PDB_UPSELLMASTER_MASTER_STATUS);
                if(status.equals(ProductMaintConstants.SKU_AVAILABLE_KEY))
                {
                    upsellVO.setMasterStatus(true);
                }
                else
                {
                    upsellVO.setMasterStatus(false);
                }
                upsellVO.setGbbPopoverFlag(StringUtils.equals("Y",rs.getString(ProductMaintConstants.PDB_UPSELLMASTER_GBB_POPOVER_FLAG)));
                upsellVO.setGbbTitle(rs.getString(ProductMaintConstants.PDB_UPSELLMASTER_GBB_TITLE));
                
                // Get the company list
                rs = (CachedResultSet) outMap.get("OUT_COMPANY_CUR");
                String[] companyList = new String[rs.getRowCount()];
                int i = 0;
                while(rs.next())
                {   
                    companyList[i]= rs.getString(ProductMaintConstants.OE_COMPANY_ID);
                    i++;
                }
                upsellVO.setCompanyList(companyList);

                //populate the associated skus
                upsellVO.setUpsellDetailList(getUpsellDetailList(conn,upsellVO));

                if(debugEnabled)
                {
                    logger.debug("UpsellDAOImpl:getMasterSKU() Getting keywords");
                }

                // get the keywords
                rs = (CachedResultSet) outMap.get("OUT_KEYWORD_CUR");
                List keywords = new ArrayList();
                while(rs.next())
                {
                    SearchKeyVO keyVO = new SearchKeyVO();
                    keyVO.setSearchKey(rs.getString(ProductMaintConstants.OE_PRODUCT_VALUE_ID));
                    keywords.add(keyVO);
                }
                upsellVO.setSearchKeyList(keywords);


                if(debugEnabled)
                {
                    logger.debug("ProductDAOImpl:getMasterSKU() Getting the product recipient search list");
                }
                // Get the recipient search list
                rs = (CachedResultSet) outMap.get("OUT_RECIPIENT_SEARCH_CUR");
                String[] recipientList = new String[rs.getRowCount()];
                i = 0;
                while(rs.next())
                {
                    recipientList[i] = rs.getString(ProductMaintConstants.OE_PRODUCT_VALUE_ID);
                    i++;
                }
                upsellVO.setRecipientSearchList(recipientList);
                
                // get the website source codes
                rs = (CachedResultSet) outMap.get("OUT_UPSELL_SOURCE_CUR");
                List websites = new ArrayList();
                while(rs.next())
                {
                    ValueVO valueVO = new ValueVO();
                    valueVO.setId(rs.getString(ProductMaintConstants.OE_PRODUCT_VALUE_ID));
                    websites.add(valueVO);
                }
                upsellVO.setWebsites(websites);;

            }
        }
        catch(Exception e)
        {
            String[] args = new String[1];
            args[0] = new String(e.getMessage());
            logger.error(e);
            throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, args, e);
        }

        return upsellVO;
    }

 /**
   * Returns an array of UpsellDetailVOs for the given master SKU
   * @param conn database connection
   * @return array of UpsellDetailVOs
   * @throws PDBApplicationException, PDBSystemException
   **/
   private List getUpsellDetailList(Connection conn, UpsellMasterVO upsellVO)  throws PDBSystemException, PDBApplicationException
    {
        ArrayList skus = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;

        try
        {
            String sku = upsellVO.getMasterID();
            HashMap inputParams = new HashMap();
            inputParams.put("SKU", sku);
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_UPSELL_DETAIL_LIST");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance(); 
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
            while(rs.next())
            {
                UpsellDetailVO upsellDetailVO = new UpsellDetailVO();
                upsellDetailVO.setDetailID(rs.getString(ProductMaintConstants.PDB_UPSELLDETAIL_DETAIL_ID));
                upsellDetailVO.setSequence(rs.getInt(ProductMaintConstants.PDB_UPSELLDETAIL_SEQUENCE));
                //upsellDetailVO.setName(FTDUtil.convertToXMLUnsafeCharacters(rs.getString(ProductMaintConstants.PDB_UPSELLDETAIL_NAME)));
                upsellDetailVO.setName(rs.getString(ProductMaintConstants.PDB_UPSELLDETAIL_NAME));
                upsellDetailVO.setType(rs.getString(ProductMaintConstants.PDB_UPSELLDETAIL_TYPE));
                upsellDetailVO.setPrice(rs.getFloat(ProductMaintConstants.PDB_UPSELLDETAIL_PRICE));                
                upsellDetailVO.setNovatorID(rs.getString(ProductMaintConstants.PDB_UPSELLDETAIL_NOVATOR_ID));
                String status = rs.getString(ProductMaintConstants.PDB_UPSELLDETAIL_AVAILABLE);
                upsellDetailVO.setSentToNovatorProd(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.PDB_UPSELLDETAIL_SENT_TO_NOVATOR_PROD)));
                upsellDetailVO.setSentToNovatorContent(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.PDB_UPSELLDETAIL_SENT_TO_NOVATOR_CONTENT)));
                upsellDetailVO.setSentToNovatorUAT(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.PDB_UPSELLDETAIL_SENT_TO_NOVATOR_UAT)));
                upsellDetailVO.setSentToNovatorTest(FTDUtil.convertStringToBoolean(rs.getString(ProductMaintConstants.PDB_UPSELLDETAIL_SENT_TO_NOVATOR_TEST)));
                if(status.equals(ProductMaintConstants.PRODUCT_AVAILABLE_KEY))
                {
                    upsellDetailVO.setAvailable(true);
                }
                else
                {
                    upsellDetailVO.setAvailable(false);
                }
                String gbbSequence = rs.getString(ProductMaintConstants.PDB_UPSELLDETAIL_GBB_SEQUENCE);
                if (gbbSequence != null) {
                    if (gbbSequence.equals("1")) {
                        upsellVO.setGbbUpsellDetailId1(upsellDetailVO.getDetailID());
                        upsellVO.setGbbNameOverrideFlag1(StringUtils.equals("Y",rs.getString(ProductMaintConstants.PDB_UPSELLDETAIL_GBB_NAME_OVERRIDE_FLAG)));
                        upsellVO.setGbbNameOverrideText1(rs.getString(ProductMaintConstants.PDB_UPSELLDETAIL_GBB_NAME_OVERRIDE_TEXT));
                        upsellVO.setGbbPriceOverrideFlag1(StringUtils.equals("Y",rs.getString(ProductMaintConstants.PDB_UPSELLDETAIL_GBB_PRICE_OVERRIDE_FLAG)));
                        upsellVO.setGbbPriceOverrideText1(rs.getString(ProductMaintConstants.PDB_UPSELLDETAIL_GBB_PRICE_OVERRIDE_TEXT));
                    } else if (gbbSequence.equals("2")) {
                        upsellVO.setGbbUpsellDetailId2(upsellDetailVO.getDetailID());
                        upsellVO.setGbbNameOverrideFlag2(StringUtils.equals("Y",rs.getString(ProductMaintConstants.PDB_UPSELLDETAIL_GBB_NAME_OVERRIDE_FLAG)));
                        upsellVO.setGbbNameOverrideText2(rs.getString(ProductMaintConstants.PDB_UPSELLDETAIL_GBB_NAME_OVERRIDE_TEXT));
                        upsellVO.setGbbPriceOverrideFlag2(StringUtils.equals("Y",rs.getString(ProductMaintConstants.PDB_UPSELLDETAIL_GBB_PRICE_OVERRIDE_FLAG)));
                        upsellVO.setGbbPriceOverrideText2(rs.getString(ProductMaintConstants.PDB_UPSELLDETAIL_GBB_PRICE_OVERRIDE_TEXT));
                    } else if (gbbSequence.equals("3")) {
                        upsellVO.setGbbUpsellDetailId3(upsellDetailVO.getDetailID());
                        upsellVO.setGbbNameOverrideFlag3(StringUtils.equals("Y",rs.getString(ProductMaintConstants.PDB_UPSELLDETAIL_GBB_NAME_OVERRIDE_FLAG)));
                        upsellVO.setGbbNameOverrideText3(rs.getString(ProductMaintConstants.PDB_UPSELLDETAIL_GBB_NAME_OVERRIDE_TEXT));
                        upsellVO.setGbbPriceOverrideFlag3(StringUtils.equals("Y",rs.getString(ProductMaintConstants.PDB_UPSELLDETAIL_GBB_PRICE_OVERRIDE_FLAG)));
                        upsellVO.setGbbPriceOverrideText3(rs.getString(ProductMaintConstants.PDB_UPSELLDETAIL_GBB_PRICE_OVERRIDE_TEXT));
                    }
                }
                upsellDetailVO.setDefaultSkuFlag(rs.getString(ProductMaintConstants.PDB_UPSELLDETAIL_DEFAULT_SKU_FLAG));

                skus.add(upsellDetailVO);
            }//end while

 
        }
        catch(Exception e)
        {
            String[] args = new String[1];
            args[0] = new String(e.getMessage());
            logger.error(e);
            throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, args, e);
        }

        return skus;
    }


/**
   * Delete the passed in Master SKU
   * @param conn database connection
   * @return void
   * @throws PDBApplicationException, PDBSystemException
   **/
   public void deleteMasterSKU(Connection conn,UpsellMasterVO masterVO)
        throws PDBApplicationException, PDBSystemException{

        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;

        try
        {
            HashMap inputParams = new HashMap();
            
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance(); 
            // build DataRequest object //
            dataRequest.reset();
            dataRequest.setConnection(conn);
            inputParams.put("MASTER_ID", masterVO.getMasterID());
            dataRequest.setInputParams(inputParams);
            dataRequest.setStatementID("SP_REMOVE_KEYWORD");
            dataAccessUtil.execute(dataRequest);

            dataRequest.reset();
            dataRequest.setConnection(conn);
            inputParams.put("MASTER_ID", masterVO.getMasterID());
            dataRequest.setInputParams(inputParams);
            dataRequest.setStatementID("SP_REMOVE_RECIPIENT_SEARCH");
            dataAccessUtil.execute(dataRequest);

            dataRequest.reset();
            dataRequest.setConnection(conn);
            inputParams.put("MASTER_ID", masterVO.getMasterID());
            dataRequest.setInputParams(inputParams);
            dataRequest.setStatementID("SP_REMOVE_UPSELL_COMPANY_2247");
            dataAccessUtil.execute(dataRequest);

            dataRequest.reset();
            dataRequest.setConnection(conn);
            inputParams.put("MASTER_ID", masterVO.getMasterID());
            dataRequest.setInputParams(inputParams);
            dataRequest.setStatementID("SP_REMOVE_UPSELL_DETAIL");
            dataAccessUtil.execute(dataRequest);

            dataRequest.reset();
            dataRequest.setConnection(conn);
            inputParams.put("MASTER_ID", masterVO.getMasterID());
            dataRequest.setInputParams(inputParams);
            dataRequest.setStatementID("PDB_REMOVE_UPSELL_SOURCE");
            dataAccessUtil.execute(dataRequest);

        }
        catch(Exception e)
        {
            String[] args = new String[1];
            args[0] = new String(e.getMessage());
            logger.error(e);
            throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, args, e);
        }
    }

  /**
   * Insert the passed in Master SKU and associate skus
   * @param conn database connection
   * @return void
   * @throws PDBApplicationException, PDBSystemException
   **/
   public void insertMasterSKU(Connection conn,UpsellMasterVO masterVO)
        throws PDBApplicationException, PDBSystemException{
        DataRequest dataRequest = new DataRequest();

        try
        {
            //determine status value
            String status = "";
            if (masterVO.isMasterStatus()) {
                status = "Y";
            }
            else {
                status = "N";
            }
            
            HashMap inputParams = new HashMap();
            inputParams.put("MASTER_ID", masterVO.getMasterID());
            inputParams.put("NAME", masterVO.getMasterName());
            inputParams.put("DESCRIPTION", masterVO.getMasterDescription());
            inputParams.put("STATUS", status);
            inputParams.put("PRIORITY", masterVO.getSearchPriority());
            inputParams.put("CORP_FLAG", FTDUtil.convertBooleanToString(masterVO.isCorporateSiteFlag()));
            inputParams.put("GBB_POPOVER_FLAG", FTDUtil.convertBooleanToString(masterVO.getGbbPopoverFlag()));
            inputParams.put("GBB_TITLE_TXT", masterVO.getGbbTitle());

            //  build DataRequest object //
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_INSERT_UPSELL_MASTER");
            dataRequest.setInputParams(inputParams);
            
            // execute the store prodcedure //
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance(); 
            dataAccessUtil.execute(dataRequest);
            
            //Remove any company records that may still exist
            //TODO:  This is carry over from the old code...do we really need to do this?
            inputParams = new HashMap();
            inputParams.put("MASTER_ID", masterVO.getMasterID());
            
            // build DataRequest object //
            dataRequest.reset();
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_REMOVE_UPSELL_COMPANY");
            dataRequest.setInputParams(inputParams);
            
            // execute the store prodcedure //
            dataAccessUtil.execute(dataRequest);
            
            //Insert the new company records
            String[] companyList = masterVO.getCompanyList();
            if(companyList != null)
            {
                for(int i = 0; i < companyList.length; i++)
                {
                    inputParams = new HashMap();
                    inputParams.put("MASTER_ID", masterVO.getMasterID());
                    inputParams.put("COMPANY_ID", (String)companyList[i]);
                    
                    // build DataRequest object //
                    dataRequest.reset();
                    dataRequest.setConnection(conn);
                    dataRequest.setStatementID("PDB_INSERT_UPSELL_COMPANY");
                    dataRequest.setInputParams(inputParams);
                    dataAccessUtil.execute(dataRequest);
                }
            }

            //insert associate skus
            List detailList = masterVO.getUpsellDetailList();
            for(int i = 0; i < detailList.size(); i++){
                UpsellDetailVO detailVO = (UpsellDetailVO)detailList.get(i);
                
                inputParams = new HashMap();
                inputParams.put("DETAIL_ID", detailVO.getDetailID());
                inputParams.put("MASTER_ID", masterVO.getMasterID());
                inputParams.put("SEQUENCE", new BigDecimal(detailVO.getSequence()));
                inputParams.put("NAME", detailVO.getName());
                inputParams.put("GBB_NAME_OVERRIDE_FLAG", FTDUtil.convertBooleanToString(detailVO.getGbbNameOverrideFlag()));
                inputParams.put("GBB_NAME_OVERRIDE_TXT", detailVO.getGbbNameOverrideText());
                inputParams.put("GBB_PRICE_OVERRIDE_FLAG", FTDUtil.convertBooleanToString(detailVO.getGbbPriceOverrideFlag()));
                inputParams.put("GBB_PRICE_OVERRIDE_TXT", detailVO.getGbbPriceOverrideText());
                inputParams.put("GBB_SEQUENCE", new BigDecimal(detailVO.getGbbSequence()));
                inputParams.put("DEFAULT_SKU_FLAG", detailVO.getDefaultSkuFlag());
                
                // build DataRequest object //
                dataRequest.reset();
                dataRequest.setConnection(conn);
                dataRequest.setStatementID("PDB_INSERT_UPSELL_DETAIL");
                dataRequest.setInputParams(inputParams);
                dataAccessUtil.execute(dataRequest);
                
            }   //end assoc sku loop      

            //save keywords
            List keywordList = masterVO.getSearchKeyList();
            for(int i = 0; i < keywordList.size(); i++)
            {
                SearchKeyVO keyVO = (SearchKeyVO)keywordList.get(i);
                inputParams = new HashMap();
                inputParams.put("PRODUCT_ID", masterVO.getMasterID());
                inputParams.put("KEYWORD", keyVO.getSearchKey());
                
                // build DataRequest object //
                dataRequest.reset();
                dataRequest.setConnection(conn);
                dataRequest.setInputParams(inputParams);
                dataRequest.setStatementID("PDB_INSERT_KEYWORD");
                dataAccessUtil.execute(dataRequest);
                
            }//end keyword looop

            //Save recipient search
            String[] recipientList = masterVO.getRecipientSearchList();
            if(recipientList != null)
            {
                for(int i = 0; i < recipientList.length; i++)
                {
                    inputParams = new HashMap();
                    inputParams.put("PRODUCT_ID", masterVO.getMasterID());
                    inputParams.put("RECIPIENT_SEARCH_ID", (String)recipientList[i]);
                
                // build DataRequest object //
                    dataRequest.reset();
                    dataRequest.setConnection(conn);
                    dataRequest.setInputParams(inputParams);
                    dataRequest.setStatementID("PDB_INSERT_RECIPIENT_SEARCH");
                    dataAccessUtil.execute(dataRequest);
                }
            }

            //save website source codes
            List websitesList = masterVO.getWebsites();
            for(int i = 0; i < websitesList.size(); i++)
            {
                ValueVO valueVO = (ValueVO)websitesList.get(i);
                inputParams = new HashMap();
                inputParams.put("MASTER_ID", masterVO.getMasterID());
                inputParams.put("SOURCE_CODE", valueVO.getId());
                
                // build DataRequest object //
                dataRequest.reset();
                dataRequest.setConnection(conn);
                dataRequest.setInputParams(inputParams);
                dataRequest.setStatementID("PDB_INSERT_UPSELL_SOURCE");
                dataAccessUtil.execute(dataRequest);
                
            }//end website source code looop

        }
        catch(Exception e)
        {
            String[] args = new String[1];
            args[0] = new String(e.getMessage());
            logger.error(e);
            throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, args, e);
        }
    }

 /**
   * Returns an array of DetailUpsellVOs based on passed in product id
   * @param conn database connection
   * @return array of MasterUpsellVOs
   * @throws PDBApplicationException, PDBSystemException
   **/
   public List getUpsellDetailByID(Connection conn,String id)
        throws PDBApplicationException, PDBSystemException
    {
        ArrayList skus = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;

        try
        {
            HashMap inputParams = new HashMap();
            inputParams.put("UPSELL_ID", id);
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_UPSELL_DETAIL_BY_ID");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance(); 
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
            while(rs.next())
            {
                UpsellDetailVO upsellDetailVO = new UpsellDetailVO();
                upsellDetailVO.setDetailMasterID(rs.getString(ProductMaintConstants.PDB_UPSELLDETAIL_BYID_MASTER_ID));
                upsellDetailVO.setDetailID(rs.getString(ProductMaintConstants.PDB_UPSELLDETAIL_BYID_DETAIL_ID));
                upsellDetailVO.setSequence(rs.getInt(ProductMaintConstants.PDB_UPSELLDETAIL_BYID_SEQUENCE));
                //upsellDetailVO.setName(FTDUtil.convertToXMLUnsafeCharacters(rs.getString(ProductMaintConstants.PDB_UPSELLDETAIL_BYID_NAME)));
                upsellDetailVO.setName(rs.getString(ProductMaintConstants.PDB_UPSELLDETAIL_BYID_NAME));
                //upsellDetailVO.setDetailMasterName(FTDUtil.convertToXMLUnsafeCharacters(rs.getString(ProductMaintConstants.PDB_UPSELLDETAIL_BYID_MASTERNAME)));
                upsellDetailVO.setDetailMasterName(rs.getString(ProductMaintConstants.PDB_UPSELLDETAIL_BYID_MASTERNAME));
                upsellDetailVO.setGbbNameOverrideFlag(StringUtils.equals("Y",rs.getString(ProductMaintConstants.PDB_UPSELLDETAIL_BYID_GBB_NAME_OVERRIDE_FLAG)));
                upsellDetailVO.setGbbNameOverrideText(rs.getString(ProductMaintConstants.PDB_UPSELLDETAIL_BYID_GBB_NAME_OVERRIDE_TEXT));
                upsellDetailVO.setGbbPriceOverrideFlag(StringUtils.equals("Y",rs.getString(ProductMaintConstants.PDB_UPSELLDETAIL_BYID_GBB_PRICE_OVERRIDE_FLAG)));
                upsellDetailVO.setGbbPriceOverrideText(rs.getString(ProductMaintConstants.PDB_UPSELLDETAIL_BYID_GBB_PRICE_OVERRIDE_TEXT));
                upsellDetailVO.setDefaultSkuFlag(rs.getString(ProductMaintConstants.PDB_UPSELLDETAIL_BYID_DEFAULT_SKU_FLAG));
                skus.add(upsellDetailVO);
            }
        }
        catch(Exception e)
        {
            String[] args = new String[1];
            args[0] = new String(e.getMessage());
            logger.error(e);
            throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, args, e);
        }

        return skus;
    }

    /**
     * Determine if a master sku exists in upsell master
     * @param conn database connection
     * @param sku to validate
     * @return true if the master sku exists
     */
    public boolean validateMasterSku(Connection conn, String sku) 
        throws PDBApplicationException, PDBSystemException{
        
        boolean retval=false;
        DataRequest dataRequest = new DataRequest();
        try
        {
            HashMap inputParams = new HashMap();
            inputParams.put("SKU_ID", sku);
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_VALIDATE_MASTER_SKU");
            dataRequest.setInputParams(inputParams);
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance(); 
            Map outputParameters = (Map) dataAccessUtil.execute(dataRequest);
            String status = (String) outputParameters.get("STATUS");
            if( status.equalsIgnoreCase("N") ) 
            {
                String message = (String) outputParameters.get("MESSAGE");
                logger.error("Error returned from database in validateMasterSku: "+message);
                throw new Exception(message);
            }
            String testString = (String) outputParameters.get("RESULTS");
            if( StringUtils.equals("Y",testString) ) {
                retval=true;
            }
        }
        catch(Exception e)
        {
            String[] args = new String[1];
            args[0] = new String(e.getMessage());
            logger.error(e);
            throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, args, e);
        }
        
        return retval;
    }
    
}