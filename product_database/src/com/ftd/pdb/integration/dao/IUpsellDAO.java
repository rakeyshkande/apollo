package com.ftd.pdb.integration.dao;

import java.util.List;

import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBSystemException;
import com.ftd.pdb.common.valobjs.UpsellMasterVO;

import java.sql.Connection;

public interface IUpsellDAO 
{

 /**
   * Returns an array of MasterUpsellVOs
   * @param conn database connection
   * @return array of MasterUpsellVOs
   * @throws PDBApplicationException, PDBSystemException
   **/
   public List getUpsellList(Connection conn)
        throws PDBApplicationException, PDBSystemException;


 /**
   * Return the passed in Master SKU
   * @param conn database connection
   * @return MasterSKUVO
   * @throws PDBApplicationException, PDBSystemException
   **/
   public UpsellMasterVO getMasterSKU(Connection conn, String sku)
        throws PDBApplicationException, PDBSystemException;


 /**
   * Delete the passed in Master SKU
   * @param conn database connection
   * @return void
   * @throws PDBApplicationException, PDBSystemException
   **/
   public void deleteMasterSKU(Connection conn, UpsellMasterVO masterVO)
        throws PDBApplicationException, PDBSystemException;

  /**
     * Insert the passed in Master SKU and associate skus
     * @param conn database connection
     * @return void
     * @throws PDBApplicationException, PDBSystemException
     **/
     public void insertMasterSKU(Connection conn,UpsellMasterVO masterVO)
          throws PDBApplicationException, PDBSystemException;

 /**
   * Returns an array of DetailUpsellVOs based on passed in product id
   * @param conn database connection
   * @return array of MasterUpsellVOs
   * @throws PDBApplicationException, PDBSystemException
   **/
   public List getUpsellDetailByID(Connection conn, String id)
        throws PDBApplicationException, PDBSystemException; 
        
    /**
     * Determine if a master sku exists in upsell master
     * @param conn database connection
     * @param sku to validate
     * @return true if the master sku exists
     */
    public boolean validateMasterSku(Connection conn, String sku) 
        throws PDBApplicationException, PDBSystemException;
}