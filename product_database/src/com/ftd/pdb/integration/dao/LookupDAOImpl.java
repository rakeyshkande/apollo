package com.ftd.pdb.integration.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.exceptions.PDBSystemException;
import com.ftd.pdb.common.valobjs.ColorVO;
import com.ftd.pdb.common.valobjs.ProductSecondChoiceVO;
import com.ftd.pdb.common.valobjs.ProductSubTypeVO;
import com.ftd.pdb.common.valobjs.StateDeliveryExclusionVO;
import com.ftd.pdb.common.valobjs.ValueVO;
import com.ftd.pdb.common.valobjs.VendorVO;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * This class is used to lookup read-only information from the database
 *
 * @author Jeff Penney - Software Architects
 **/
public class LookupDAOImpl extends PDBDataAccessObject implements ILookupDAO
{
    /**
    * Constructor for the LookupDAOImpl class
    */
    public LookupDAOImpl()
    {
        super("com.ftd.pdb.dao.LookupDAOImpl");
    }

    /**
     * Get a list of  Product Types
     * @param conn database connection
     * @return List of Types
     * @exception PDBSystemException
     */
    public List getTypesList(Connection conn) throws PDBSystemException
    {
        List list = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        try
        {
            HashMap inputParams = new HashMap();
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_TYPES_LIST");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

            while(rs.next())
            {
                ValueVO value = new ValueVO();
                value.setId(rs.getString(ProductMaintConstants.OE_PRODUCT_VALUE_ID));
                value.setDescription(rs.getString(ProductMaintConstants.OE_PRODUCT_VALUE_DESCRIPTION));
                list.add(value);
            }
        }  
        catch(Exception e)
        {
            logger.error(e);
            throw new PDBSystemException(ProductMaintConstants.SQL_EXCEPTION, e);
        }

        return list;        
    }

    /**
     * Get a list of Companies
     * @param conn database connection
     * @return List of Companies
     * @exception PDBSystemException
     */
    public List getCompanyMasterList(Connection conn) throws PDBSystemException
    {
        List list = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        try
        {
            HashMap inputParams = new HashMap();
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_COMPANY_MASTER_LIST");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
            
            while(rs.next())
            {
                ValueVO value = new ValueVO();
                value.setId(rs.getString(ProductMaintConstants.OE_PRODUCT_VALUE_ID));
                value.setDescription(rs.getString(ProductMaintConstants.OE_PRODUCT_VALUE_DESCRIPTION));
                list.add(value);
            }
        } 
        catch(Exception e)
        {
            logger.error(e);
            throw new PDBSystemException(ProductMaintConstants.SQL_EXCEPTION, e);
        }

        return list;        
    }


    /**
     * Get a list of  Product Sub Types
     * @param conn database connection
     * @return List of Sub Types
     * @exception PDBSystemException
     */
    public List getSubTypesList(Connection conn) throws PDBSystemException
    {
        List list = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        
        try
        {
            HashMap inputParams = new HashMap();
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_SUB_TYPES_LIST");
            dataRequest.setInputParams(inputParams);  
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

            while(rs.next())
            {
                ProductSubTypeVO value = new ProductSubTypeVO();
                value.setId(rs.getString(ProductMaintConstants.OE_PRODUCT_SUBTYPE_ID));
                value.setDescription(rs.getString(ProductMaintConstants.OE_PRODUCT_SUBTYPE_DESCRIPTION));
                value.setTypeId(rs.getString(ProductMaintConstants.OE_PRODUCT_TYPE_ID));
                list.add(value);
            } 
        }    
        catch(Exception e)
        {
            logger.error(e);
            throw new PDBSystemException(ProductMaintConstants.SQL_EXCEPTION, e);
        }

        return list;        
    }

    /**
     * Get a list of  Carrier types
     * @param conn database connection
     * @return List of Categories
     * @exception PDBSystemException
     * Ed Mueller, 6/9/03
     */
    public List getCarrierTypes(Connection conn) throws PDBSystemException
    {
        List list = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        
        try
        {
            HashMap inputParams = new HashMap();
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_CARRIER_LIST");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

            while(rs.next())
            {
                ValueVO carrier = new ValueVO();
                carrier.setId(rs.getString(ProductMaintConstants.OE_PRODUCT_VALUE_ID));
                carrier.setDescription(rs.getString(ProductMaintConstants.OE_PRODUCT_VALUE_DESCRIPTION));
                list.add(carrier);
            } 
        }    
        catch(Exception e)
        {
            logger.error(e);
            throw new PDBSystemException(ProductMaintConstants.SQL_EXCEPTION, e);
        }

        return list;        
    }

    
    /**
     * Get a list of  Product Categories
     * @param conn database connection
     * @return List of Categories
     * @exception PDBSystemException
     */
    public List getCategoryList(Connection conn) throws PDBSystemException
    {
        List categories = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;

        try
        {
            HashMap inputParams = new HashMap();
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_CATEGORY_LIST");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
            
            while(rs.next())
            {
                ValueVO category = new ValueVO();
                category.setId(rs.getString(ProductMaintConstants.OE_PRODUCT_VALUE_ID));
                category.setDescription(rs.getString(ProductMaintConstants.OE_PRODUCT_VALUE_DESCRIPTION));
                categories.add(category);
            }
        }    
        catch(Exception e)
        {
            logger.error(e);
            throw new PDBSystemException(ProductMaintConstants.SQL_EXCEPTION, e);
        }

        return categories;
	}

    /**
     * Get a list of  JCPenney Product Categories
     * @param conn database connection
     * @return List of Categories
     * @exception PDBSystemException
     * @deprecated JCPenney is unused lpuckett 08/30/2006
     */
    public List getJCPenneyCategoryList(Connection conn) throws PDBSystemException
    {
        List categories = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;

        try
        {
            HashMap inputParams = new HashMap();
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_JCPENNEY_CATEGORY_LIST");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

            while(rs.next())
            {
                ValueVO category = new ValueVO();
                category.setId(rs.getString(ProductMaintConstants.OE_PRODUCT_VALUE_ID));
                category.setDescription(rs.getString(ProductMaintConstants.OE_PRODUCT_VALUE_DESCRIPTION));
                categories.add(category);
            }
        }    
        catch(Exception e)
        {
            logger.error(e);
            throw new PDBSystemException(ProductMaintConstants.SQL_EXCEPTION, e);
        }

        return categories;
    }


    /**
    * Returns a List of Second Choices
    * @param conn database connection
    * @return List of Second Choices
    * @throws PDBSystemException
    **/
    public List getSecondChoiceList(Connection conn) throws PDBSystemException
    {
        List list = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        
        try
        {
            HashMap inputParams = new HashMap();
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_SECOND_CHOICE_LIST");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

            while(rs.next())
            {
                ProductSecondChoiceVO vo = new ProductSecondChoiceVO();
                vo.setProductSecondChoiceID(rs.getString(ProductMaintConstants.SECOND_CHOICE_ID));
                vo.setDescription(rs.getString(ProductMaintConstants.SECOND_CHOICE_DESCRIPTION));
                vo.setMercuryDescription(rs.getString(ProductMaintConstants.SECOND_CHOICE_MERCURY_DESCRIPTION));
                list.add(vo);
            }
        }    
        catch(Exception e)
        {
            logger.error(e);
            throw new PDBSystemException(ProductMaintConstants.SQL_EXCEPTION, e);
        }

        return list;    
    }

    /**
    * Returns a List of States
    * @param conn database connection
    * @return List of States
    * @throws PDBSystemException
    **/
    public List getStatesList(Connection conn) throws PDBSystemException
    {
        List list = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        
        try
        {
            HashMap inputParams = new HashMap();
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_STATE_LIST");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
            
            while(rs.next())
            {
                ValueVO valueVO = new ValueVO();
                valueVO.setId(rs.getString(ProductMaintConstants.OE_PRODUCT_VALUE_ID));
                valueVO.setDescription(rs.getString(ProductMaintConstants.OE_PRODUCT_VALUE_DESCRIPTION));
                list.add(valueVO);
            }
        }    
        catch(Exception e)
        {
            logger.error(e);
            throw new PDBSystemException(ProductMaintConstants.SQL_EXCEPTION, e);
        }

        return list;    
    }

    /**
    * Returns a List of Countries
    * @param conn database connection
    * @return List of Countries
    * @throws PDBSystemException
    **/
    public List getCountriesList(Connection conn) throws PDBSystemException
    {
        List list = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        
        try
        {
            HashMap inputParams = new HashMap();
            inputParams.put("COUNTRY_TYPE","I");
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_COUNTRY_LIST");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

            while(rs.next())
            {
                ValueVO valueVO = new ValueVO();
                valueVO.setId(rs.getString(ProductMaintConstants.OE_PRODUCT_VALUE_ID));
                valueVO.setDescription(rs.getString(ProductMaintConstants.OE_PRODUCT_VALUE_DESCRIPTION));
                list.add(valueVO);
            }
        }    
        catch(Exception e)
        {
            logger.error(e);
            throw new PDBSystemException(ProductMaintConstants.SQL_EXCEPTION, e);
        }

        return list;    
    }

    /**
     * Get a list of  Vendors
     * @param conn database connection
     * @return List of ValueVOs that have the vendor ID and name
     * @exception PDBSystemException
     */
    public List getVendorList(Connection conn) throws PDBSystemException
    {
        List list = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        
        try
        {
            HashMap inputParams = new HashMap();
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_VENDOR_LIST");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

            while(rs.next())
            {
                VendorVO vendorVO = new VendorVO();
                vendorVO.setVendorId(rs.getString(ProductMaintConstants.OE_VENDOR_MASTER_VENDOR_ID));
                vendorVO.setVendorName(rs.getString(ProductMaintConstants.OE_VENDOR_MASTER_VENDOR_NAME));
                vendorVO.setVendorType(rs.getString(3));
                list.add(vendorVO);
            }
        }    
        catch(Exception e)
        {
            logger.error(e);
            throw new PDBSystemException(ProductMaintConstants.SQL_EXCEPTION, e);
        }
        return list;            
    }

    /**
     * Get a list of  Vendor Carriers
     * @param conn database connection
     * @return List of ValueVOs that have the vendor ID and name
     * @exception PDBSystemException
     */
    public List getVendorCarrierList(Connection conn) throws PDBSystemException
    {
        List list = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        
        try
        {
            HashMap inputParams = new HashMap();
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_VENDOR_CARRIER_LIST");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

            while(rs.next())
            {
                ValueVO valueVO = new ValueVO();
                valueVO.setId(rs.getString(ProductMaintConstants.OE_PRODUCT_VALUE_ID));
                valueVO.setDescription(rs.getString(ProductMaintConstants.OE_PRODUCT_VALUE_DESCRIPTION));
                list.add(valueVO);
            }
        }    
        catch(Exception e)
        {
            logger.error(e);
            throw new PDBSystemException(ProductMaintConstants.SQL_EXCEPTION, e);
        }
        return list;            
    }

    /**
     * Get a list of  ValueVOs representing shipping availability 
     * @param conn database connection
     * @param typeCode filter
     * @return List of ValueVOs representing shipping availability 
     * @exception PDBSystemException
     */
    public List getShippingAvailabilityList(Connection conn, String typeCode) throws PDBSystemException
    {
        List list = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        
        try
        {
            HashMap inputParams = new HashMap();
            inputParams.put("TYPE_CODE",typeCode);
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_SHIPPING_AVAIL_LIST");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

            while(rs.next())
            {
                ValueVO valueVO = new ValueVO();
                valueVO.setId(rs.getString(ProductMaintConstants.OE_VENDOR_MASTER_VENDOR_ID));
                valueVO.setDescription(rs.getString(ProductMaintConstants.OE_VENDOR_MASTER_VENDOR_NAME));
                list.add(valueVO);
            }
        }    
        catch(Exception e)
        {
            logger.error(e);
            throw new PDBSystemException(ProductMaintConstants.SQL_EXCEPTION, e);
        }

        return list;            
    }

    /**
     * Get a list of  ValueVOs representing price points
     * @param conn database connection
     * @return List of ValueVOs representing price points
     * @exception PDBSystemException
     */
    public List getPricePointsList(Connection conn) throws PDBSystemException
    {
        List list = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        
        try
        {
            HashMap inputParams = new HashMap();
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_PRICE_POINTS_LIST");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

            while(rs.next())
            {
                ValueVO valueVO = new ValueVO();
                valueVO.setId(rs.getString(ProductMaintConstants.OE_PRODUCT_VALUE_ID));
                valueVO.setDescription(rs.getString(ProductMaintConstants.OE_PRODUCT_VALUE_DESCRIPTION));
                list.add(valueVO);
            }
        }    
        catch(Exception e)
        {
            logger.error(e);
            throw new PDBSystemException(ProductMaintConstants.SQL_EXCEPTION, e);
        }

        return list;            
    }
    
    /**
     * Get a list of  ValueVOs representing shipping keys
     * @param conn database connection
     * @return List of ValueVOs representing shipping keys
     * @exception PDBSystemException
     */
    public List getShippingKeysList(Connection conn) throws PDBSystemException
    {
        List list = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        
        try
        {
            HashMap inputParams = new HashMap();
            HashMap output = new HashMap();
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_SHIPPING_KEYS");
            inputParams.put("IN_SHIPPING_KEY_ID", null);
            dataRequest.setInputParams(inputParams);

            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            output = (HashMap)dataAccessUtil.execute(dataRequest);
            String status = (String) output.get("OUT_STATUS");
            if (status != null && status.equalsIgnoreCase("N")){
                logger.error("LookupDAOImpl.getShippingKeyList: "+(String)output.get("OUT_MESSAGE"));
            }
            
            rs = (CachedResultSet) output.get("OUT_CUR");

            while(rs.next())
            {
                ValueVO valueVO = new ValueVO();
                valueVO.setId(rs.getString(ProductMaintConstants.OE_PRODUCT_VALUE_ID));
                valueVO.setDescription(rs.getString(ProductMaintConstants.OE_PRODUCT_VALUE_DESCRIPTION));
                list.add(valueVO);
            }
        }   
        catch(Exception e)
        {
            logger.error(e);
            throw new PDBSystemException(ProductMaintConstants.SQL_EXCEPTION, e);
        }
        
        return list;            
    }

    /**
     * Get a list of  ValueVOs representing recipient search ids
     * @param conn database connection
     * @return List of ValueVOs representing recipient search ids
     * @exception PDBSystemException
     */
    public List getRecipientSearchList(Connection conn) throws PDBSystemException
    {
        List list = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        
        try
        {
            HashMap inputParams = new HashMap();
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_RECIPIENT_SEARCH_LIST");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
            while(rs.next())
            {
                ValueVO valueVO = new ValueVO();
                valueVO.setId(rs.getString(ProductMaintConstants.OE_PRODUCT_VALUE_ID));
                valueVO.setDescription(rs.getString(ProductMaintConstants.OE_PRODUCT_VALUE_DESCRIPTION));
                list.add(valueVO);
            }
        }  
        catch(Exception e)
        {
            logger.error(e);
            throw new PDBSystemException(ProductMaintConstants.SQL_EXCEPTION, e);
        }

        return list;            
    }        

    /**
     * Get a list of  ValueVOs representing search priorities
     * @param conn database connection
     * @return List of ValueVOs representing search priorities
     * @exception PDBSystemException
     */
    public List getSearchPriorityList(Connection conn) throws PDBSystemException
    {
        List list = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
         
        try
        {
            HashMap inputParams = new HashMap();
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_SEARCH_PRIORITY_LIST");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
            
            while(rs.next())
            {
                ValueVO valueVO = new ValueVO();
                valueVO.setId(rs.getString(ProductMaintConstants.OE_PRODUCT_VALUE_ID));
                valueVO.setDescription(rs.getString(ProductMaintConstants.OE_PRODUCT_VALUE_DESCRIPTION));
                list.add(valueVO);
            }
        }   
        catch(Exception e)
        {
            logger.error(e);
            throw new PDBSystemException(ProductMaintConstants.SQL_EXCEPTION, e);
        }

        return list;            
    }

    /**
     * Get a list of  ValueVOs representing search priorities
     * @param conn database connection
     * @return List of ValueVOs representing search priorities
     * @exception PDBSystemException
     */
    public List getExceptionCodesList(Connection conn) throws PDBSystemException
    {
        List list = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
         
        try
        {
            HashMap inputParams = new HashMap();
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_EXCEPTIONS_LIST");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

            while(rs.next())
            {
                ValueVO valueVO = new ValueVO();
                valueVO.setId(rs.getString(ProductMaintConstants.OE_PRODUCT_VALUE_ID));
                valueVO.setDescription(rs.getString(ProductMaintConstants.OE_PRODUCT_VALUE_DESCRIPTION));
                list.add(valueVO);
            }
        }    
        catch(Exception e)
        {
            logger.error(e);
            throw new PDBSystemException(ProductMaintConstants.SQL_EXCEPTION, e);
        }

        return list;            
    }    
    
    /**
     * Get a list of  ValueVOs representing source codes 
     * @param conn database connection
     * @return List of ValueVOs representing source codes
     * @exception PDBSystemException
     */
    public List getSourceCodeList(Connection conn) throws PDBSystemException
    {
        List list = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        
        try
        {
            HashMap inputParams = new HashMap();
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_SOURCE_CODE_LIST");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

            while(rs.next())
            {
                ValueVO valueVO = new ValueVO();
                valueVO.setId(rs.getString(ProductMaintConstants.OE_PRODUCT_VALUE_ID));
                valueVO.setDescription(rs.getString(ProductMaintConstants.OE_PRODUCT_VALUE_DESCRIPTION));
                list.add(valueVO);
            }
        } 
        catch(Exception e)
        {
            logger.error(e);
            throw new PDBSystemException(ProductMaintConstants.SQL_EXCEPTION, e);
        }

        return list;            
    }

    /**
     * Get a list of  ValueVOs representing shipping methods
     * @param conn database connection
     * @return List of ValueVOs representing shipping methods
     * @exception PDBSystemException
     */
    public List getShippingMethodsList(Connection conn) throws PDBSystemException
    {
        List list = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        
        try
        {
            HashMap inputParams = new HashMap();
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_SHIPPING_METHODS");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

            while(rs.next())
            {
                ValueVO valueVO = new ValueVO();
                valueVO.setId(rs.getString(ProductMaintConstants.OE_PRODUCT_VALUE_ID));
                valueVO.setDescription(rs.getString(ProductMaintConstants.OE_PRODUCT_VALUE_DESCRIPTION));
                list.add(valueVO);
            }
        }    
        catch(Exception e)
        {
            logger.error(e);
            throw new PDBSystemException(ProductMaintConstants.SQL_EXCEPTION, e);
        }

        return list;
    }

    /**
     * Get a list of  ValueVOs representing available colors
     * @param conn database connection
     * @return List of ValueVOs representing available colors
     * @exception PDBSystemException
     */
    public List getColorsList(Connection conn) throws PDBSystemException
    {
        List list = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        
        try
        {
            HashMap inputParams = new HashMap();
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_COLORS_LIST");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

            while(rs.next())
            {
                ColorVO valueVO = new ColorVO();
                valueVO.setId(rs.getString(ProductMaintConstants.OE_PRODUCT_VALUE_ID));
                valueVO.setDescription(rs.getString(ProductMaintConstants.OE_PRODUCT_VALUE_DESCRIPTION));
                valueVO.setMarkerType(rs.getString(ProductMaintConstants.OE_PRODUCT_VALUE_MARKER_TYPE));
                list.add(valueVO);
            }
        }  
        catch(Exception e)
        {
            logger.error(e);
            throw new PDBSystemException(ProductMaintConstants.SQL_EXCEPTION, e);
        }

        return list;
    }    

    /**
     * Get a list of StateDeliveryExclusionVO representing states that have delivery exclusions
     * @param conn database connection
     * @return List of StateDeliveryExclusionVO representing states that have delivery exclusions
     * @exception PDBSystemException
     */
    public List getStateDeliveryExceptionList(Connection conn) throws PDBSystemException
    {
        List excludedStateList = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        
        try
        {
            HashMap inputParams = new HashMap();
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_DEL_EXCL_STATES");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
            StateDeliveryExclusionVO sdeVO;
            
            while(rs.next())
            {
                sdeVO = new StateDeliveryExclusionVO();
                sdeVO.setExcludedState(rs.getString(ProductMaintConstants.STATE_ID));
                sdeVO.setStateName(rs.getString(ProductMaintConstants.STATE_NAME));
                sdeVO.setSunExcluded(false);
                sdeVO.setMonExcluded(false);
                sdeVO.setTueExcluded(false);
                sdeVO.setWedExcluded(false);
                sdeVO.setThuExcluded(false);
                sdeVO.setFriExcluded(false);
                sdeVO.setSatExcluded(false);
                excludedStateList.add(sdeVO);
            }
        }
        catch(Exception e)
        {
            logger.error(e);
            throw new PDBSystemException(ProductMaintConstants.SQL_EXCEPTION, e);
        }

        return excludedStateList;
    }    

    /**
     * Get a list of  ValueVOs representing active vendor types
     * @param conn database connection
     * @return List of ValueVOs representing active vendor types
     * @exception PDBSystemException
     */
    public List getActiveVendorTypes(Connection conn) throws PDBSystemException
    {
        List list = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        
        try
        {
            HashMap inputParams = new HashMap();
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_VENDOR_TYPES_LIST");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

            while(rs.next())
            {
                ValueVO valueVO = new ValueVO();
                valueVO.setId(rs.getString(ProductMaintConstants.OE_PRODUCT_VALUE_ID));
                valueVO.setDescription(rs.getString(ProductMaintConstants.OE_PRODUCT_VALUE_DESCRIPTION));
                list.add(valueVO);
            }
        }    
        catch(Exception e)
        {
            logger.error(e);
            throw new PDBSystemException(ProductMaintConstants.SQL_EXCEPTION, e);
        }

        return list;
    }
    
    /**
     * Get a list of  ValueVOs representing box types
     * @param conn database connection
     * @return List of ValueVOs representing box types
     * @exception PDBSystemException
     */
    public List getBoxList(Connection conn) throws PDBSystemException
    {
        List list = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        
        try
        {
            HashMap inputParams = new HashMap();
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_BOX_INFO");
            inputParams.put("IN_BOX_ID", null);
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

            while(rs.next())
            {
                ValueVO valueVO = new ValueVO();
                valueVO.setId(rs.getString("box_id"));
                String boxName = rs.getString("box_name");
                String boxDesc = rs.getString("box_desc");
                valueVO.setDescription(boxName + " " + boxDesc);
                list.add(valueVO);
            }
        }    
        catch(Exception e)
        {
            logger.error(e);
            throw new PDBSystemException(ProductMaintConstants.SQL_EXCEPTION, e);
        }

        return list;
    }

    /**
       * Get a list of  ValueVOs representing available component skus
       * @param conn database connection
       * @return List of ValueVOs representing available component skus
       * @exception PDBSystemException
       */
    public List getComponentSkuList(Connection conn) throws PDBSystemException
    {
        List list = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        
        try
        {
            HashMap inputParams = new HashMap();
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_COMPONENT_SKU_LIST");
            inputParams.put("IN_COMPONENT_SKU_ID", null);
            inputParams.put("IN_SHOW_AVAILABLE", "Y");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

            while(rs.next())
            {
                ValueVO valueVO = new ValueVO();
                valueVO.setId(rs.getString("component_sku_id"));
                valueVO.setDescription(rs.getString("component_sku_name"));
                list.add(valueVO);
                logger.debug(valueVO.getId() + " " + valueVO.getDescription());
            }
        }    
        catch(Exception e)
        {
            logger.error(e);
            throw new PDBSystemException(ProductMaintConstants.SQL_EXCEPTION, e);
        }

        return list;
    }

    /**
     * Get a list of  Vendors that are of a specific type
     * @param conn database connection
     * @param vendorType filter
     * @return List of ValueVOs that have the vendor ID and name
     * @exception PDBSystemException
     */
    public List getVendorsOfType(Connection conn, String vendorType) throws PDBSystemException
    {
        List list = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        
        try
        {
            HashMap inputParams = new HashMap();
            inputParams.put("VENDOR_TYPE",vendorType);
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_VENDORS_OF_TYPE");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

            while(rs.next())
            {
                ValueVO valueVO = new ValueVO();
                valueVO.setId(rs.getString(ProductMaintConstants.OE_VENDOR_MASTER_VENDOR_ID));
                valueVO.setDescription(rs.getString(ProductMaintConstants.OE_VENDOR_MASTER_VENDOR_NAME));
                list.add(valueVO);
            }
        }    
        catch(Exception e)
        {
            logger.error(e);
            throw new PDBSystemException(ProductMaintConstants.SQL_EXCEPTION, e);
        }
        return list;            
    }

    /**
     * Retrieves a record from the FRP.GLOBAL_PARMS table
     * Calls FRP.MISC_PKG.GET_GLOBAL_PARM_VALUE.
     *
     * @param conn database connection
     * @param context filter
     * @param name filter
     * @return String value of record retrieved.  Null is returned if no 
     * matching record was found.
     */
    public String getGlobalParameter(Connection conn, String context, 
                                     String name) throws Exception {
        DataRequest dataRequest = new DataRequest();
        boolean isEmpty = true;
        String retval = null;

        try {
            logger.debug("getGlobalParameter (String context, String name)");
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("CONTEXT", context);
            inputParams.put("NAME", name);

            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_GLOBAL_PARM_VALUE");
            dataRequest.setInputParams(inputParams);

            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            retval = (String)dataAccessUtil.execute(dataRequest);
            if (retval != null)
                isEmpty = false;
        } catch (Exception e) {
            logger.error(e);
            throw e;
        }
        if (isEmpty)
            return null;
        else
            return retval;
    }

    /**
     * Get a list of carriers
     * @param conn database connection
     * @return List of ValueVO containing carrier data
     * @throws PDBSystemException
     */
    public List getCarrierList(Connection conn) throws PDBSystemException {
        List list = new ArrayList();        
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        
        try
        {
            HashMap inputParams = new HashMap();
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_CARRIER_LIST");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

            while(rs.next())
            {
                ValueVO valueVO = new ValueVO();
                valueVO.setId(rs.getString("CARRIER_ID"));
                valueVO.setDescription(rs.getString("CARRIER_NAME"));
                list.add(valueVO);
            }
        }    
        catch(Exception e)
        {
            logger.error(e);
            throw new PDBSystemException(ProductMaintConstants.SQL_EXCEPTION, e);
        }

//        ValueVO valueVO = new ValueVO();
//        valueVO.setId("DHL");
//        valueVO.setDescription("DHL");
//        list.add(valueVO);
//        
//        valueVO = new ValueVO();
//        valueVO.setId("FEDEX");
//        valueVO.setDescription("FedEx");
//        list.add(valueVO);
//        
//        valueVO = new ValueVO();
//        valueVO.setId("UPS");
//        valueVO.setDescription("UPS");
//        list.add(valueVO);
         
        return list;
    }
}