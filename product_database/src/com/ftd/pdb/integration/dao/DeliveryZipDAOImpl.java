package com.ftd.pdb.integration.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.pdb.common.PDBXMLTags;
import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBSystemException;

import java.sql.Connection;

import java.util.HashMap;

import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class DeliveryZipDAOImpl extends PDBDataAccessObject implements IDeliveryZipDAO {
    public DeliveryZipDAOImpl() {
        super("com.ftd.pdb.integration.dao.DeliveryZipDAOImpl");
    }

    /**
     * Get all the records from the venus.carrier table
     * @param conn database connection
     * @return xml representation of the returned carrier records
     * @throws Exception
     */
    public Document getCarriersXML(Connection conn) throws PDBSystemException {
        Document doc = null;
        
        try
        {
            doc = JAXPUtil.createDocument();        
            Element root = doc.createElement(PDBXMLTags.TAG_CARRIERS);
            doc.appendChild(root);
            
            DataRequest dataRequest = new DataRequest();
            CachedResultSet rs = null;
            HashMap inputParams = new HashMap();
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_CARRIER_LIST");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

            while(rs.next())
            {
                Element carrier = doc.createElement(PDBXMLTags.TAG_CARRIER);
                JAXPUtil.addAttribute(carrier,PDBXMLTags.TAG_ID,rs.getString("CARRIER_ID"));
                JAXPUtil.addAttribute(carrier,PDBXMLTags.TAG_NAME,rs.getString("CARRIER_NAME"));
                root.appendChild(carrier);
            }
        }    
        catch(Exception e)
        {
            logger.error(e);
            throw new PDBSystemException(ProductMaintConstants.SQL_EXCEPTION, e);
        }
        
        return doc;
    }

    /**
     * @param conn database connection
     * @param zipCode filter
     * @return
     */
    public Document getBlockedZips(Connection conn, String zipCode) throws PDBSystemException {
        Document doc = null;
        
        try
        {
            doc = JAXPUtil.createDocument();        
            Element root = doc.createElement(PDBXMLTags.TAG_BLOCKED_ZIPS);
            doc.appendChild(root);
            
            DataRequest dataRequest = new DataRequest();
            CachedResultSet rs = null;
            HashMap inputParams = new HashMap();
            inputParams.put("IN_ZIP_CODE",zipCode);
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_BLOCKED_ZIPS");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

            while(rs.next())
            {
                Element carrier = doc.createElement(PDBXMLTags.TAG_BLOCKED_ZIP);
                JAXPUtil.addAttribute(carrier,PDBXMLTags.TAG_CARRIER_ID,rs.getString("CARRIER_ID"));
                JAXPUtil.addAttribute(carrier,PDBXMLTags.TAG_ZIP_CODE,rs.getString("ZIP_CODE"));
                root.appendChild(carrier);
            }
        }    
        catch(Exception e)
        {
            logger.error(e);
            throw new PDBSystemException(ProductMaintConstants.SQL_EXCEPTION, e);
        }
        
        return doc;
    }

    /**
     * Deletes a record from venus.carrier_excluded zips table
     * @param conn database connection
     * @param carrierId filter
     * @param zipCode filter
     * @throws PDBApplicationException
     * @throws PDBSystemException
     */
    public void deleteBlockedZip(Connection conn, String carrierId, String zipCode) throws PDBApplicationException, PDBSystemException {        
        try
        {   
            DataRequest dataRequest = new DataRequest();
            HashMap inputParams = new HashMap();
            inputParams.put("IN_CARRIER_ID",carrierId);
            inputParams.put("IN_ZIP_CODE",zipCode);
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_DELETE_BLOCKED_ZIP");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();   
            Map outputs = (Map)dataAccessUtil.execute(dataRequest);
            
            String status = (String)outputs.get("OUT_STATUS");
            if (status != null && status.equalsIgnoreCase("N")) {
                String message = 
                    (String)outputs.get("OUT_MESSAGE");
                throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, new Exception(message));
            }
        }    
        catch(Exception e)
        {
            logger.error(e);
            if( e instanceof PDBApplicationException ) {
                 PDBApplicationException pe =(PDBApplicationException)e;
                 throw pe;
            } else {
                throw new PDBSystemException(ProductMaintConstants.SQL_EXCEPTION, e);
            }
        }
    }

    /**
     * Inserts a record from venus.carrier_excluded zips table
     * @param conn database connection
     * @param carrierId carrier code
     * @param zipCode postal code
     * @param userId user entering the record
     * @throws PDBApplicationException
     * @throws PDBSystemException
     */
    public void insertBlockedZip(Connection conn, String carrierId, String zipCode, String userId) throws PDBApplicationException, PDBSystemException {        
        try
        {   
            DataRequest dataRequest = new DataRequest();
            HashMap inputParams = new HashMap();
            inputParams.put("IN_CARRIER_ID",carrierId);
            inputParams.put("IN_ZIP_CODE",zipCode);
            inputParams.put("IN_USER",userId);
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_INSERT_BLOCKED_ZIP");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();   
            Map outputs = (Map)dataAccessUtil.execute(dataRequest);
            
            String status = (String)outputs.get("OUT_STATUS");
            if (status != null && status.equalsIgnoreCase("N")) {
                String message = 
                    (String)outputs.get("OUT_MESSAGE");
                throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, new Exception(message));
            }
        }    
        catch(Exception e)
        {
            logger.error(e);
            if( e instanceof PDBApplicationException ) {
                 PDBApplicationException pe =(PDBApplicationException)e;
                 throw pe;
            } else {
                throw new PDBSystemException(ProductMaintConstants.SQL_EXCEPTION, e);
            }
        }
    }
}
