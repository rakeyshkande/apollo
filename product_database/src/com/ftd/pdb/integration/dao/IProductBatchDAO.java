package com.ftd.pdb.integration.dao;

import java.util.List;

import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBSystemException;
import com.ftd.pdb.common.valobjs.ProductBatchVO;

import java.sql.Connection;

public interface IProductBatchDAO 
{
     /**
      * Returns an array of ProductBatchVOs
      * @param conn database connection
      * @param userId to locate
      * @return array of products
      * @exception PDBSystemException
      */
     public List getUserBatch(Connection conn, String userId) throws PDBSystemException, PDBApplicationException;

    /**
     * Returns a ProductBatchVO
     * @param conn database connection
     * @param userId to locate
     * @param productId to locate
     * @return ProductBatchVO
     * @throws PDBApplicationException, PDBSystemException
     **/
    public ProductBatchVO getProduct(Connection conn, String userId, String productId) throws PDBSystemException, PDBApplicationException;

    /**
     * Returns a ProductBatchVO based on Novator ID
     * @param conn database connection
     * @param userId to locate
     * @param novatorId to locate
     * @return ProductXMLVO
     * @throws PDBSystemException
     **/
    public ProductBatchVO getProductByNovatorId(Connection conn, String userId, String novatorId) throws PDBSystemException, PDBApplicationException;

     /**
      * Saves a ProductBatchVO to the database
      * @param conn database connection
      * @param productBatchVO that will be updated or created in the database
      * @exception PDBSystemException
      */
     public void setProduct(Connection conn, ProductBatchVO productBatchVO) throws PDBSystemException, PDBApplicationException;

     /**
      * Deletes a product from the database
      * @param conn database connection
      * @param userId to delete
      * @param productId to delete
      * @exception PDBSystemException
      */
     public void deleteProduct(Connection conn, String userId, String productId) throws PDBSystemException, PDBApplicationException;
}