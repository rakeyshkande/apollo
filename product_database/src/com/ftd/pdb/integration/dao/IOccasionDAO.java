package com.ftd.pdb.integration.dao;

import java.util.List;

import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBSystemException;

import java.sql.Connection;

public interface IOccasionDAO
{

   /**
    * Retrieves occasion data from database.
    * @param conn database connection
    * @return OccasionVO[]
    *
    */
     public List getOccasions(Connection conn) 
             throws PDBSystemException, PDBApplicationException;

}