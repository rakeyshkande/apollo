package com.ftd.pdb.integration.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.exceptions.BadConnectionException;
import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBSystemException;
import com.ftd.pdb.common.valobjs.ProductVO;
import com.ftd.pdb.common.valobjs.ShippingKeyPriceVO;
import com.ftd.pdb.common.valobjs.ShippingKeyVO;
import com.ftd.pdb.common.valobjs.ShippingMethodVO;
import com.ftd.pdb.common.valobjs.comparator.ShippingMethodVOComparator;

import java.math.BigDecimal;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * This class is used to read and write Product information from the database
 *
 * @author Ting Wang - ProaAlliance
 **/
public class ShippingKeyDAOImpl extends PDBDataAccessObject implements IShippingKeyDAO {
    /**
    *   Constructor for the ShippingKeyDAOImpl class
    */
    public ShippingKeyDAOImpl() {
        super("com.ftd.pdb.dao.ShippingKeyDAOImpl");
    }

    /**
   * Returns an array of ShippingKeyVOs
   * @param conn database connection
   * @return array of ShippingKeyVOs
   * @throws PDBApplicationException, PDBSystemException
   **/
    public List getShippingKeyList(Connection conn) throws PDBApplicationException, 
                                                           PDBSystemException {
                                                           
        ArrayList list = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;

        try {
            HashMap inputParams = new HashMap();
            HashMap output = new HashMap();
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_SHIPPING_KEYS");
            inputParams.put("IN_SHIPPING_KEY_ID", null);
            dataRequest.setInputParams(inputParams);

            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            output = (HashMap)dataAccessUtil.execute(dataRequest);
            String status = (String) output.get("OUT_STATUS");
            if (status != null && status.equalsIgnoreCase("N")){
                logger.error("ShippingKeyDAOImpl.getShippingKeyList: "+(String)output.get("OUT_MESSAGE"));
            }
            
            rs = (CachedResultSet) output.get("OUT_CUR");

            while (rs.next()) {
                ShippingKeyVO vo = new ShippingKeyVO();
                vo.setShippingKey_ID(rs.getString("shipping_key_id"));
                vo.setShippingKeyDescription(rs.getString("shipping_key_description"));
                vo.setShipperID(rs.getString("shipper"));

                list.add(vo);
            }

            return list;
        } catch (Exception e) {
            logger.error(e);
            throw new BadConnectionException(ProductMaintConstants.OE_BAD_CONNECTION_EXCEPTION, 
                                             e);
        }
    }

    /**
     * return a ShippingKeyVO for a given shippingID
     * @param conn database connection
     * @param  shippingID the selectecd id for the shipping key.
     * @return ShippingKeyVO
     * @throws PDBApplicationException, PDBSystemException
    **/
    public ShippingKeyVO getShippingKey(Connection conn, 
                                        String shippingID) throws PDBApplicationException, 
                                                                  PDBSystemException {
        
        ShippingKeyVO vo = new ShippingKeyVO();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs_key = null;
        CachedResultSet rs_details = null;
        CachedResultSet rsShipMethods = null;
        CachedResultSet rs_productID = null;

        try {

            HashMap output = new HashMap();
            // build DataRequest object //
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_SHIPPING_KEYS");
            HashMap inputParams = new HashMap();
            inputParams.put("IN_SHIPPING_KEY_ID", shippingID);
            dataRequest.setInputParams(inputParams);

            // execute the store prodcedure //
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            output = (HashMap)dataAccessUtil.execute(dataRequest);
            String status = (String) output.get("OUT_STATUS");
            if (status != null && status.equalsIgnoreCase("N")){
                logger.error("ShippingKeyDAOImpl.getShippingKey: "+(String)output.get("OUT_MESSAGE"));
            }
            
            rs_key = (CachedResultSet) output.get("OUT_CUR");

            while (rs_key.next()) {
                vo.setShippingKey_ID(rs_key.getString(1));
                vo.setShippingKeyDescription(rs_key.getString(2));
                vo.setShipperID(rs_key.getString(3));
            }

            /* build DataRequest object */
            dataRequest.reset();
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_SHIPPING_KEY_DETAILS");
            inputParams = new HashMap();
            inputParams.put("IN_SHIPPING_KEY_ID",shippingID);
            dataRequest.setInputParams(inputParams);
            output = (HashMap)dataAccessUtil.execute(dataRequest);
            status = (String) output.get("OUT_STATUS");
            if (status != null && status.equalsIgnoreCase("N")){
                logger.error("ShippingKeyDAOImpl.GET_SHIPPING_KEY_DETAILS: "+(String)output.get("OUT_MESSAGE"));
            }
            rs_details = (CachedResultSet)output.get("OUT_CUR");
            
            List array_details = new ArrayList();
            while (rs_details.next()) {
                ShippingKeyPriceVO priceVO = new ShippingKeyPriceVO();
                priceVO.setShippingKeyID(shippingID);
                priceVO.setShippingKeyPriceID(rs_details.getString("shipping_detail_id"));
                priceVO.setMinPrice(new Double(rs_details.getDouble("min_price")));
                priceVO.setMaxPrice(new Double(rs_details.getDouble("max_price")));
                array_details.add(priceVO);
            }

            vo.setPrices(array_details);

            // Get the shipping methods for each ShippingKeyPriceVO
            for (int i = 0; i < vo.getPrices().size(); i++) {
                ShippingKeyPriceVO priceVO = 
                    (ShippingKeyPriceVO)vo.getPrices().get(i);


                /* build DataRequest object */
                dataRequest.reset();
                dataRequest.setConnection(conn);
                dataRequest.setStatementID("PB_GET_SHIPPING_KEY_COSTS");
                inputParams = new HashMap();
                inputParams.put("IN_SHIPPING_KEY_DETAIL_ID",priceVO.getShippingKeyPriceID());
                dataRequest.setInputParams(inputParams);

                output = (HashMap)dataAccessUtil.execute(dataRequest);
                status = (String) output.get("OUT_STATUS");
                if (status != null && status.equalsIgnoreCase("N")){
                    logger.error("ShippingKeyDAOImpl.GET_SHIPPING_KEY_DETAILS: "+(String)output.get("OUT_MESSAGE"));
                }
                    /* execute the store prodcedure */
                rsShipMethods = (CachedResultSet) output.get("OUT_CUR");

                List shippingMethodList = new ArrayList();
                while (rsShipMethods.next()) {
                    ShippingMethodVO shipMethodVO = new ShippingMethodVO();
                    shipMethodVO.setPriceId(priceVO.getShippingKeyPriceID());
                    shipMethodVO.setId(rsShipMethods.getString("shipping_method_id"));
                    shipMethodVO.setCost(rsShipMethods.getFloat("shipping_cost"));
                    shippingMethodList.add(shipMethodVO);
                }

                ShippingMethodVOComparator shippingMethodVOComparator = 
                    new ShippingMethodVOComparator();
                Collections.sort(shippingMethodList, 
                                 shippingMethodVOComparator);
                priceVO.setShippingMethods(shippingMethodList);
            }

 
            /* build DataRequest object */
            dataRequest.reset();
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PB_GET_SHIPPING_KEY_PRODUCT_LIST");
            inputParams = new HashMap();
            inputParams.put("IN_SHIPPING_KEY_ID", shippingID);
            dataRequest.setInputParams(inputParams);

            output = (HashMap)dataAccessUtil.execute(dataRequest);
            status = (String) output.get("OUT_STATUS");
            if (status != null && status.equalsIgnoreCase("N")){
                logger.error("ShippingKeyDAOImpl.GET_SHIPPING_KEY_PRODUCT_LIST: "+(String)output.get("OUT_MESSAGE"));
            }
            
                /* execute the store prodcedure */
            rs_productID = (CachedResultSet) output.get("OUT_CUR");
            
            ArrayList array_productID = new ArrayList();
            while (rs_productID.next()) {
                ProductVO productVO = new ProductVO();
                productVO.setShippingKey(shippingID);
                productVO.setProductId(rs_productID.getString("product_id"));

                array_productID.add(productVO);
            }

            ProductVO[] productID_list = new ProductVO[array_productID.size()];
            for (int i = 0; i < array_productID.size(); i++) {
                productID_list[i] = (ProductVO)array_productID.get(i);
            }

            vo.setProducts(productID_list);
            return vo;
        } catch (Exception e) {
            logger.error(e);
            throw new BadConnectionException(ProductMaintConstants.OE_BAD_CONNECTION_EXCEPTION, 
                                             e);
        }
    }

    /**
     * cascading deleting a row in Shipping_key table and rows in any table that has a foreign key reference.
     * @param conn database connection
     * @param pShippingKeyVO the selected id for the shipping key to be removed from database.
     * @return Boolean true if succeed, false if failed.
     * @throws PDBApplicationException, PDBSystemException
    **/
    public boolean removeShippingKey(Connection conn, ShippingKeyVO pShippingKeyVO) 
        throws PDBApplicationException, PDBSystemException {
        DataRequest dataRequest = new DataRequest();
        HashMap output = new HashMap();
        boolean retval = false;
        String shippingID = pShippingKeyVO.getShippingKey_ID();
        String userId = "";
        if( pShippingKeyVO.getUpdatedBy() != null )
        	userId = pShippingKeyVO.getUpdatedBy().toUpperCase();

        try {
            conn.setAutoCommit(false);
            //Mark records in SHIPPING_KEY_COSTS so we know who deleted them
            HashMap inputParams = new HashMap();
            java.util.Date now = new java.util.Date();
            inputParams.put("IN_SHIPPING_KEY_ID", shippingID);
            inputParams.put("IN_UPDATED_BY",userId+" - DELETE");
            
            logger.info("Deleting shipping key " + shippingID + ".");

            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_MARK_SHIPPING_KEY_COSTS_UPDATED");
            dataRequest.setInputParams(inputParams);

            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            output = (HashMap)dataAccessUtil.execute(dataRequest);
            String status = (String) output.get("OUT_STATUS");
            if (status != null && status.equalsIgnoreCase("N")){
                logger.error("ShippingKeyDAOImpl.MARK_SHIPPING_KEY_COSTS_UPDATED: "+(String)output.get("OUT_MESSAGE"));
            }
            
            //Delete SHIPPING_KEY_COSTS record
            inputParams = new HashMap();
            inputParams.put("IN_SHIPPING_KEY_ID", shippingID);

            /* build DataRequest object */
            dataRequest.reset();
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_DELETE_SHIPPING_KEY_COSTS");
            dataRequest.setInputParams(inputParams);

            /* execute the store prodcedure */
            dataAccessUtil = DataAccessUtil.getInstance();
            output = (HashMap)dataAccessUtil.execute(dataRequest);
            status = (String) output.get("OUT_STATUS");
            if (status != null && status.equalsIgnoreCase("N")){
                logger.error("ShippingKeyDAOImpl.DELETE_SHIPPING_KEY_COSTS: "+(String)output.get("OUT_MESSAGE"));
            }
            
            //Mark records in SHIPPING_KEY_DETAILS so that we know who changed them

            /* build DataRequest object */
            dataRequest.reset();
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_MARK_SHIPPING_KEY_DETAILS_UPDATED");
            inputParams = new HashMap();
            inputParams.put("IN_UPDATED_BY",userId+" - DELETE");
            inputParams.put("IN_SHIPPING_KEY_ID", shippingID);
            dataRequest.setInputParams(inputParams);

            /* execute the store prodcedure */
            dataAccessUtil = DataAccessUtil.getInstance();
            dataAccessUtil.execute(dataRequest);
             
            //Delete the SHIPPING_KEY_DETAILS records

            /* build DataRequest object */
            dataRequest.reset();
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_DELETE_SHIPPING_KEY_DETAILS");
            inputParams = new HashMap();
            inputParams.put("IN_SHIPPING_KEY_ID", shippingID);
            dataRequest.setInputParams(inputParams);

            /* execute the store prodcedure */
            dataAccessUtil = DataAccessUtil.getInstance();
            output = (HashMap)dataAccessUtil.execute(dataRequest);
            status = (String) output.get("OUT_STATUS");
            if (status != null && status.equalsIgnoreCase("N")){
                logger.error("ShippingKeyDAOImpl.DELETE_SHIPPING_KEY_DETAILS: "+(String)output.get("OUT_MESSAGE"));
            }
            
            //Mark records in SHIPPING_KEYS so that we know who changed them
            /* build DataRequest object */
            dataRequest.reset();
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_MARK_SHIPPING_KEY_UPDATED");
            inputParams = new HashMap();
            inputParams.put("IN_UPDATED_BY",userId+" - DELETE");
            inputParams.put("IN_SHIPPING_KEY_ID", shippingID);
            dataRequest.setInputParams(inputParams);

            /* execute the store prodcedure */
            dataAccessUtil = DataAccessUtil.getInstance();
            output = (HashMap)dataAccessUtil.execute(dataRequest);
            status = (String) output.get("OUT_STATUS");
            if (status != null && status.equalsIgnoreCase("N")){
                logger.error("ShippingKeyDAOImpl.MARK_SHIPPING_KEY_UPDATED: "+(String)output.get("OUT_MESSAGE"));
            }
             
            //Delete the SHIPPING_KEYS records
            /* build DataRequest object */
            dataRequest.reset();
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_DELETE_SHIPPING_KEYS");
            inputParams = new HashMap();
            inputParams.put("IN_SHIPPING_KEY_ID", shippingID);
            dataRequest.setInputParams(inputParams);

            /* execute the store prodcedure */
             output = (HashMap)dataAccessUtil.execute(dataRequest);
             status = (String) output.get("OUT_STATUS");
             if (status != null && status.equalsIgnoreCase("N")){
                 logger.error("ShippingKeyDAOImpl.DELETE_SHIPPING_KEY: "+(String)output.get("OUT_MESSAGE"));
             }

            retval = true;
            conn.commit();
        } catch (Exception e) {
            logger.error(e);
            if (conn != null) {
                try {
                    conn.rollback();
                } catch (SQLException rollbackException) {
                    logger.error(rollbackException);
                }
            }
            throw new BadConnectionException(ProductMaintConstants.OE_BAD_CONNECTION_EXCEPTION, e);
        }
        return retval;
    }

    /**
     * Insert a row in Shipping_key table and rows in Shipping_key_details table.
     * @param conn database connection
     * @param  shippingKeyVO the selectecd id for the shipping key to be removed from database.
     * @return  Boolean true if succeed, false if failed.
     * @throws PDBApplicationException, PDBSystemException
    **/
    public boolean insertShippingKey(Connection conn, 
                                     ShippingKeyVO shippingKeyVO) throws PDBApplicationException, 
                                                                         PDBSystemException {
        boolean retval = false;
        String userId = "";
        if( shippingKeyVO.getUpdatedBy() != null ) {
            userId = shippingKeyVO.getUpdatedBy().toUpperCase();
        }
        
        DataRequest dataRequest = new DataRequest();

        try {
            //Manually handle the commit
            conn.setAutoCommit(false);
            java.util.Date now = new java.util.Date();

            // Insert record into shipping key table, returns the new id

            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_INSERT_SHIPPING_KEY");
            HashMap inputParams = new HashMap();
            inputParams.put("DESCRIPTION", 
                            shippingKeyVO.getShippingKeyDescription());
            inputParams.put("SHIPPER", shippingKeyVO.getShipperID());
            inputParams.put("CREATED_BY", userId);
            inputParams.put("UPDATED_BY", userId);
            dataRequest.setInputParams(inputParams);

            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            String shipping_key_id = (String)dataAccessUtil.execute(dataRequest);
            
            logger.info("Executed PDB_INSERT_SHIPPING_KEY and got shipping key id " + shipping_key_id + ".");

            //outputs = (HashMap)dataAccessUtil.execute(dataRequest);
            
            //String shipping_key_id = (String)outputs.get("ID");
            
            //BigDecimal shipping_key_id = 
             //   new BigDecimal((Long)outputs.get("ID"));

            List prices = shippingKeyVO.getPrices();
            for (int i = 0; i < prices.size(); i++) {
                inputParams = new HashMap();
                inputParams.put("IN_SHIPPING_KEY_ID", shipping_key_id);
                inputParams.put("IN_MIN_PRICE", 
                                new BigDecimal(((ShippingKeyPriceVO)prices.get(i)).getMinPrice().doubleValue()));
                inputParams.put("IN_MAX_PRICE", 
                                new BigDecimal(((ShippingKeyPriceVO)prices.get(i)).getMaxPrice().doubleValue()));
                
                inputParams.put("IN_CREATED_BY", userId);
                inputParams.put("IN_UPDATED_BY", userId);
            
                /* build DataRequest object */
                dataRequest.reset();
                dataRequest.setConnection(conn);
                dataRequest.setStatementID("PDB_INSERT_SHIPPING_KEY_DETAILS");
                dataRequest.setInputParams(inputParams);

                /* execute the store prodcedure */
                //outputs = (HashMap)dataAccessUtil.execute(dataRequest);
                String shippingDetailId = (String)dataAccessUtil.execute(dataRequest);
                logger.info("Executed PDB_INSERT_SHIPPING_KEY_DETAILS and got shipping detail id " + shippingDetailId + ".");
                //BigDecimal shippingDetailId = 
                 //   new BigDecimal((Long)outputs.get("ID"));
                List methods = 
                    ((ShippingKeyPriceVO)prices.get(i)).getShippingMethods();
                for (int j = 0; j < methods.size(); j++) {
                    inputParams = new HashMap();
                    inputParams.put("IN_SHIPPING_KEY_DETAIL_ID", 
                                    new BigDecimal(shippingDetailId));
                    inputParams.put("IN_SHIPPING_METHOD", 
                                    ((ShippingMethodVO)methods.get(j)).getId());
                    inputParams.put("IN_COST", 
                                    new BigDecimal(((ShippingMethodVO)methods.get(j)).getCost()));
                    inputParams.put("IN_CREATED_BY", userId);
                    inputParams.put("IN_UPDATED_BY", userId);
                    logger.info("Adding Cost:\nDetail ID " + inputParams.get("IN_SHIPPING_KEY_DETAIL_ID") + 
                                "\nMethod " + inputParams.get("IN_SHIPPING_METHOD") + " \nCost: " + inputParams.get("IN_COST") +
                                "\nCreated by " + inputParams.get("IN_CREATED_BY") + "\nUpdated by " + inputParams.get("IN_UPDATED_BY"));
                    /* build DataRequest object */
                    dataRequest.reset();
                    dataRequest.setConnection(conn);
                    dataRequest.setStatementID("PDB_INSERT_SHIPPING_KEY_COSTS");
                    dataRequest.setInputParams(inputParams);

                    /* execute the store prodcedure */
                    dataAccessUtil.execute(dataRequest);
                }
            }

            conn.commit();
            retval = true;
        } catch (Exception e) {
            logger.error(e);
            if (conn != null) {
                try {
                    conn.rollback();
                } catch (SQLException rollbackException) {
                    logger.error(rollbackException);
                }
            }
            throw new BadConnectionException(ProductMaintConstants.OE_BAD_CONNECTION_EXCEPTION, 
                                             e);
        }

        return retval;
    }

    /**
     * Update a row in Shipping_key table and rows in Shipping_key_details table.
     * @param conn database connection
     * @param  shippingKeyVO the selecteced id for the shipping key to be udpated from database.
     * @return  Boolean true if succeed, false if failed.
     * @throws PDBApplicationException, PDBSystemException
    **/
    public boolean updateShippingKey(Connection conn, 
                                     ShippingKeyVO shippingKeyVO) throws PDBApplicationException, 
                                                                         PDBSystemException {
        boolean retval = false;
        String userId = "";
        HashMap output = new HashMap();
        HashMap inputParams = new HashMap();
        
        if( shippingKeyVO.getUpdatedBy() != null)
            userId = shippingKeyVO.getUpdatedBy().toUpperCase();
            
        DataRequest dataRequest = new DataRequest();

        try {

            conn.setAutoCommit(false);
            java.util.Date now = new java.util.Date();
            
            int shipping_key_ID = 
                (new Integer(shippingKeyVO.getShippingKey_ID())).intValue();

            // Insert record into shipping key table, returns the new id
            inputParams.clear();
            inputParams.put("IN_SHIPPING_KEY_ID", shippingKeyVO.getShippingKey_ID());
            inputParams.put("IN_SHIPPING_KEY_DESCRIPTION", shippingKeyVO.getShippingKeyDescription());
            inputParams.put("IN_SHIPPER", shippingKeyVO.getShipperID());
            inputParams.put("IN_UPDATED_BY", userId);

            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_UPDATE_SHIPPING_KEYS");
            dataRequest.setInputParams(inputParams);

            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            output = (HashMap)dataAccessUtil.execute(dataRequest);
            String status = (String) output.get("OUT_STATUS");
            if (status != null && status.equalsIgnoreCase("N")){
                logger.error("ShippingKeyDAOImpl.UPDATE_SHIPPING_KEYS: "+(String)output.get("OUT_MESSAGE"));
            }
            
            
            //Mark records in SHIPPING_KEY_COSTS so we know who deleted them
            inputParams.clear();
            inputParams.put("IN_UPDATED_BY",userId+" - UPDATE");
            inputParams.put("IN_SHIPPING_KEY_ID", shippingKeyVO.getShippingKey_ID());

            /* build DataRequest object */
            dataRequest.reset();
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_MARK_SHIPPING_KEY_COSTS_UPDATED");
            dataRequest.setInputParams(inputParams);

            /* execute the store prodcedure */
            dataAccessUtil = DataAccessUtil.getInstance();
            dataAccessUtil.execute(dataRequest);

            // delete associated rows from the Shipping_key_costs table
            inputParams.clear();
            inputParams.put("IN_SHIPPING_KEY_ID", shippingKeyVO.getShippingKey_ID());

            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_DELETE_SHIPPING_KEY_COSTS");
            dataRequest.setInputParams(inputParams);

            /* execute the store prodcedure */
            dataAccessUtil.execute(dataRequest);
            
            //Mark records in SHIPPING_KEY_DETAILS so that we know who changed them
            inputParams.clear();
            inputParams.put("IN_UPDATED_BY",userId+" - DELETE");
            inputParams.put("IN_SHIPPING_KEY_ID", shippingKeyVO.getShippingKey_ID());

            /* build DataRequest object */
            dataRequest.reset();
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_MARK_SHIPPING_KEY_DETAILS_UPDATED");
            dataRequest.setInputParams(inputParams);

            /* execute the store prodcedure */
            dataAccessUtil = DataAccessUtil.getInstance();
            output = (HashMap)dataAccessUtil.execute(dataRequest);
            status = (String) output.get("OUT_STATUS");
            if (status != null && status.equalsIgnoreCase("N")){
                logger.error("ShippingKeyDAOImpl.getShippingKeyList: "+(String)output.get("OUT_MESSAGE"));
            }

            
            // delete associated rows in Shipping_key_Details table
            inputParams.clear();
            inputParams.put("IN_SHIPPING_KEY_ID", shippingKeyVO.getShippingKey_ID());

            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_DELETE_SHIPPING_KEY_DETAILS");
            dataRequest.setInputParams(inputParams);

            /* execute the store prodcedure */
            dataAccessUtil.execute(dataRequest);

            List prices = shippingKeyVO.getPrices();
            for (int i = 0; i < prices.size(); i++) {
                inputParams.clear();
                inputParams.put("IN_SHIPPING_KEY_ID",shippingKeyVO.getShippingKey_ID() );
                inputParams.put("IN_MIN_PRICE", 
                                new BigDecimal(((ShippingKeyPriceVO)prices.get(i)).getMinPrice().doubleValue()));
                inputParams.put("IN_MAX_PRICE", 
                                new BigDecimal(((ShippingKeyPriceVO)prices.get(i)).getMaxPrice().doubleValue()));
                inputParams.put("IN_CREATED_BY", userId);
                inputParams.put("IN_UPDATED_BY", userId);
            
                // build DataRequest object //
                dataRequest.reset();
                dataRequest.setConnection(conn);
                dataRequest.setStatementID("PDB_INSERT_SHIPPING_KEY_DETAILS");
                dataRequest.setInputParams(inputParams);
                // execute the store prodcedure //
                String id = (String)dataAccessUtil.execute(dataRequest);


                BigDecimal shippingDetailId = new BigDecimal(id);
                List methods = 
                    ((ShippingKeyPriceVO)prices.get(i)).getShippingMethods();
                for (int j = 0; j < methods.size(); j++) {



                    // build DataRequest object //
                    dataRequest.reset();
                    dataRequest.setConnection(conn);
                    inputParams.clear();
                    inputParams.put("IN_SHIPPING_KEY_DETAIL_ID", 
                                    new BigDecimal(id));
                    inputParams.put("IN_SHIPPING_METHOD", 
                                    ((ShippingMethodVO)methods.get(j)).getId());
                    inputParams.put("IN_COST", 
                                    new BigDecimal(((ShippingMethodVO)methods.get(j)).getCost()));
                    inputParams.put("IN_CREATED_BY", userId);
                    inputParams.put("IN_UPDATED_BY", userId);
                    dataRequest.setStatementID("PDB_INSERT_SHIPPING_KEY_COSTS");
                    dataRequest.setInputParams(inputParams);

                    // execute the store prodcedure //
                    dataAccessUtil.execute(dataRequest);
                }
            }
            conn.commit();
            retval = true;
        } catch (Exception e) {
            logger.error(e);
            if (conn != null) {
                try {
                    conn.rollback();
                } catch (SQLException rollbackException) {
                    logger.error(rollbackException);
                }
            }
            throw new BadConnectionException(ProductMaintConstants.OE_BAD_CONNECTION_EXCEPTION, 
                                             e);
        }

        return retval;
    }
}
