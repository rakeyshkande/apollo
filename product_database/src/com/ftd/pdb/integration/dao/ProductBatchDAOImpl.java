package com.ftd.pdb.integration.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBSystemException;
import com.ftd.pdb.common.valobjs.ProductBatchVO;


import java.io.IOException;

import java.sql.Clob;
import java.sql.Connection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.w3c.dom.Document;


public class ProductBatchDAOImpl extends PDBDataAccessObject implements IProductBatchDAO
{
    /**
    *   Constructor for the ProductBatchDAOImpl class
    */
    public ProductBatchDAOImpl()
    {
        super("com.ftd.pdb.dao.ProductBatchDAOImpl");
    }
    
    /**
     * Returns an array of ProductBatchVOs
     * @param conn database connection
     * @param userId to locate
     * @return array of products
     * @exception PDBSystemException
     */
    public List getUserBatch(Connection conn, String userId) throws PDBSystemException, PDBApplicationException
    {
        ArrayList products = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        try
        {
            HashMap inputParams = new HashMap();
            inputParams.put("USER_ID", userId);
            inputParams.put("PRODUCT_ID", null);
            inputParams.put("NOVATOR_ID", null);
            
            // build DataRequest object //
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_PRODUCT_BATCH");
            dataRequest.setInputParams(inputParams);
            
            // execute the store prodcedure //
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance(); 
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

            while(rs.next())
            {
                ProductBatchVO product = new ProductBatchVO();
                product.setProductId(rs.getString(1));
                product.setNovatorId(rs.getString(2));
                product.setUserId(rs.getString(3));
                product.setStatus(rs.getString(4));
                Clob tClob = rs.getClob(5);
                Document doc = JAXPUtil.parseDocument(tClob.getSubString(1, (int) tClob.length()));
                product.setXmlDocument(doc);
                tClob = rs.getClob(6);
                if (tClob != null) {
                  doc =JAXPUtil.parseDocument(tClob.getSubString(1, (int) tClob.length()));
                  product.setXmlErrors(doc);
                }
                products.add(product);
            }
        }
        catch(IOException ioException) 
        {
            String[] args = new String[1];
            args[0] = new String(ioException.getMessage());
            logger.error(ioException);
            throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, args, ioException);
        }
        catch(Exception e)
        {
            String[] args = new String[1];
            args[0] = new String(e.getMessage());
            logger.error(e);
            throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, args, e);
        }
        return products;
    }

   /**
    * Returns a ProductBatchVO
    * @param conn database connection
    * @param userId to locate
    * @param productId to locate
    * @return ProductBatchVO
    * @throws PDBApplicationException, PDBSystemException
    **/
   public ProductBatchVO getProduct(Connection conn, String userId, String productId) throws PDBSystemException, PDBApplicationException
    {
        ProductBatchVO product = null;
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;

        try
        {
            HashMap inputParams = new HashMap();
            inputParams.put("USER_ID", userId);
            inputParams.put("PRODUCT_ID", productId);
            inputParams.put("NOVATOR_ID", null);
            
            // build DataRequest object //
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_PRODUCT_BATCH");
            dataRequest.setInputParams(inputParams);
            
            // execute the store prodcedure //
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

            if( rs.next() ) {
                product = new ProductBatchVO();
                product.setProductId(rs.getString(1));
                product.setNovatorId(rs.getString(2));
                product.setUserId(rs.getString(3));
                product.setStatus(rs.getString(4));
                Clob tClob = rs.getClob(5);
                Document doc = JAXPUtil.parseDocument(tClob.getSubString(1, (int) tClob.length()));
                product.setXmlDocument(doc);
                tClob = rs.getClob(6);
                if (tClob != null) {
                  doc = JAXPUtil.parseDocument(tClob.getSubString(1, (int) tClob.length()));
                  product.setXmlErrors(doc);
                }
            }
        }
        catch(Exception e)
        {
            String[] args = new String[1];
            args[0] = new String(e.getMessage());
            logger.error(e);
            throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, args, e);
        }
        return product;
    }

   /**
    * Returns a ProductBatchVO based on Novator ID
    * @param conn database connection
    * @param userId to locate
    * @param novatorId to locate
    * @return ProductXMLVO
    * @throws PDBSystemException
    **/
   public ProductBatchVO getProductByNovatorId(Connection conn, String userId, String novatorId) throws PDBSystemException, PDBApplicationException
    {
        ProductBatchVO product = null;
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;

        try
        {
            HashMap inputParams = new HashMap();
            inputParams.put("USER_ID", userId);
            inputParams.put("PRODUCT_ID", null);
            inputParams.put("NOVATOR_ID", novatorId);
            
            // build DataRequest object //
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_PRODUCT_BATCH");
            dataRequest.setInputParams(inputParams);
            
            // execute the store prodcedure //
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

            if( rs.next() ) {
                product = new ProductBatchVO();
                product.setProductId(rs.getString(1));
                product.setNovatorId(rs.getString(2));
                product.setUserId(rs.getString(3));
                product.setStatus(rs.getString(4));
                Clob tClob = rs.getClob(5);
                Document doc = JAXPUtil.parseDocument(tClob.getSubString(1, (int) tClob.length()));
                product.setXmlDocument(doc);
                tClob = rs.getClob(6);
                if (tClob != null) {
                  doc = JAXPUtil.parseDocument(tClob.getSubString(1, (int) tClob.length()));
                  product.setXmlErrors(doc);
                }
            }
        }
        catch(Exception e)
        {
            String[] args = new String[1];
            args[0] = new String(e.getMessage());
            logger.error(e);
            throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, args, e);
        }
        return product;
    }

    /**
     * Saves a ProductBatchVO to the database
     * @param conn database connection
     * @param productBatchVO that will be updated or created in the database
     * @exception PDBSystemException
     */
    public void setProduct(Connection conn, ProductBatchVO productBatchVO) throws PDBSystemException, PDBApplicationException
    {
        DataRequest dataRequest = new DataRequest();

        try
        {
            HashMap inputParams = new HashMap();
            inputParams.put("PRODUCT_ID", productBatchVO.getProductId());
            inputParams.put("NOVATOR_ID", productBatchVO.getNovatorId());
            inputParams.put("USER_ID", productBatchVO.getUserId());
            inputParams.put("STATUS", productBatchVO.getStatus());    
            
            inputParams.put("PRODUCT_RECORD", JAXPUtil.toString(productBatchVO.getXmlDocument()));
            Document xmlErrors = productBatchVO.getXmlErrors();
            if (xmlErrors != null) {
               inputParams.put("ERRORS",JAXPUtil.toString(productBatchVO.getXmlErrors()));
            } 
            else 
            {
              inputParams.put("ERRORS","");              
            }
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_UPDATE_PRODUCT_BATCH");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            dataAccessUtil.execute(dataRequest);
        }
        catch(Exception e)
        {
            String[] args = new String[1];
            args[0] = new String(e.getMessage());
            logger.error(e);
            throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, args, e);
        }
    }

    /**
     * Deletes a product from the database
     * @param conn database connection
     * @param userId to delete
     * @param productId to delete
     * @exception PDBSystemException
     */
    public void deleteProduct(Connection conn, String userId, String productId) throws PDBSystemException, PDBApplicationException
    {
        DataRequest dataRequest = new DataRequest();

        try
        {
            HashMap inputParams = new HashMap();
            inputParams.put("USER_ID", userId);
            inputParams.put("PRODUCT_ID", productId);
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_DELETE_PRODUCT_BATCH");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            dataAccessUtil.execute(dataRequest);
        }
        catch(Exception e)
        {
            String[] args = new String[1];
            args[0] = new String(e.getMessage());
            logger.error(e);
            throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, args, e);
        }
    }
}