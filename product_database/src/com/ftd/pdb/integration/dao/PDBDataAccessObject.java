package com.ftd.pdb.integration.dao;

import com.ftd.osp.utilities.plugins.Logger;

public abstract class PDBDataAccessObject {

    protected static Logger logger;
    
    /**
    * This field is used to check if debugging is enabled.
    */
    protected boolean debugEnabled;
    
    public PDBDataAccessObject(String loggerCategory) {
        logger = new Logger(loggerCategory);
        
        if ( logger.isDebugEnabled() )
        {
            debugEnabled = true;
        }
    }
}
