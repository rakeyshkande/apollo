package com.ftd.pdb.integration.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBSystemException;
import com.ftd.pdb.common.valobjs.MassEditDetailVO;
import com.ftd.pdb.common.valobjs.MassEditFileStatusVO;
import com.ftd.pdb.common.valobjs.MassEditHeaderVO;

public class ProductMassEditDAOImpl extends PDBDataAccessObject implements IProductMassEditDAO {
	
	
	public ProductMassEditDAOImpl() {
	   super("com.ftd.pdb.dao.ProductMassEditDAOImpl");
	}
	
	/**
	 * Inserts a row into the Mass Edit Header table
	 * 
	 * @param Connection
	 * @param MassEditHeaderVO
	 * @return Long - id of row just inserted
	 * @throws PDBSystemException
	 * @throws PDBApplicationException
	 */
	public Long insertMassEditHeader(Connection conn, MassEditHeaderVO headerVO) 
		throws PDBSystemException, PDBApplicationException   {
		long rowId = -1L;
		
		try {
			DataRequest dataRequest = new DataRequest();
			dataRequest.setConnection(conn);
			dataRequest.setStatementID("PDB_INSERT_MASS_EDIT_HEADER");

			HashMap inputParams = new HashMap();
			inputParams.put("IN_FILE_NAME",headerVO.getFileName());
			inputParams.put("IN_FILE_STATUS",headerVO.getFileStatus()); 
			inputParams.put("IN_TOTAL_RECORDS",headerVO.getTotalRecords().toString());
			inputParams.put("IN_CREATED_BY",headerVO.getCreatedBy());
			inputParams.put("IN_UPDATED_BY",headerVO.getUpdatedBy());
			dataRequest.setInputParams(inputParams);

			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			Map outputs = (Map)dataAccessUtil.execute(dataRequest);
        
			String status = (String)outputs.get("OUT_STATUS");
			if (status != null && status.equalsIgnoreCase("N")) {
				String message = (String)outputs.get("OUT_MESSAGE");
				String[] errors = {message};
	            throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION,errors);
			}
			else {
				BigDecimal newRowId = (BigDecimal) outputs.get("OUT_MASS_EDIT_HEADER_ID");
				rowId = newRowId.longValue();
			}
		}
		 catch(Exception e) {
             String[] args = new String[1];
             args[0] = new String(e.getMessage());
             logger.error(e);
             throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, 
                                               args, e);
         }
		return rowId;
	}
	
	/**
	 * Inserts a row into the Mass Edit Detail table
	 * 
	 * @param Connection
	 * @param MassEditDetailVO
	 * @return Long - id of row just inserted
	 * @throws PDBSystemException
	 * @throws PDBApplicationException
	 */
	public Long insertMassEditDetail(Connection conn, MassEditDetailVO detailVO) 
		throws PDBSystemException, PDBApplicationException   {
		long rowId = -1L;
		
		try {
			DataRequest dataRequest = new DataRequest();
			dataRequest.setConnection(conn);
			dataRequest.setStatementID("PDB_INSERT_MASS_EDIT_DETAIL");

			HashMap inputParams = new HashMap();
			inputParams.put("IN_MASS_EDIT_HEADER_ID",detailVO.getMassEditHeaderId().toString());
			inputParams.put("IN_UPSELL_MASTER_ID",detailVO.getUpsellMasterId());
			inputParams.put("IN_UPSELL_DETAIL_ID",detailVO.getUpsellDetailId()); 
			inputParams.put("IN_STATUS",detailVO.getStatus());
			inputParams.put("IN_ERROR_MSG",detailVO.getErrorMsg());
			inputParams.put("IN_CREATED_BY",detailVO.getCreatedBy());
			inputParams.put("IN_UPDATED_BY",detailVO.getUpdatedBy());
			inputParams.put("IN_PRODUCT_ID",detailVO.getProductId());
			dataRequest.setInputParams(inputParams);

			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			Map outputs = (Map)dataAccessUtil.execute(dataRequest);
        
			String status = (String)outputs.get("OUT_STATUS");
			if (status != null && status.equalsIgnoreCase("N")) {
				String message = (String)outputs.get("OUT_MESSAGE");
				String[] errors = {message};
	            throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION,errors);
			}
			else {
				BigDecimal newRowId = (BigDecimal) outputs.get("OUT_MASS_EDIT_HEADER_ID");
				rowId = newRowId.longValue();
			}
		}
		 catch(Exception e) {
             String[] args = new String[1];
             args[0] = new String(e.getMessage());
             logger.error(e);
             throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, 
                                               args, e);
         }
		return rowId;
	}
	
	
	
	/**
	 * Updates a row into the Mass Edit Header table
	 * 
	 * @param Connection
	 * @param MassEditHeaderVO
	 * 
	 * @throws PDBSystemException
	 * @throws PDBApplicationException
	 */
	public void updateMassEditHeader(Connection conn, MassEditHeaderVO headerVO) 
		throws PDBSystemException, PDBApplicationException   {
		
		try {
			DataRequest dataRequest = new DataRequest();
			dataRequest.setConnection(conn);
			dataRequest.setStatementID("PDB_UPDATE_MASS_EDIT_HEADER");

			HashMap inputParams = new HashMap();
			inputParams.put("IN_FILE_STATUS",headerVO.getFileStatus()); 
			inputParams.put("IN_TOTAL_RECORDS",headerVO.getTotalRecords().toString());
			inputParams.put("IN_UPDATED_BY",headerVO.getUpdatedBy());
			inputParams.put("IN_MASS_EDIT_HEADER_ID",headerVO.getMassEditHeaderId().toString());
			dataRequest.setInputParams(inputParams);


			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			Map outputs = (Map)dataAccessUtil.execute(dataRequest);
        
			String status = (String)outputs.get("OUT_STATUS");
			if (status != null && status.equalsIgnoreCase("N")) {
				String message = (String)outputs.get("OUT_MESSAGE");
				String[] errors = {message};
	            throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION,errors);
			}			
		}
		 catch(Exception e) {
             String[] args = new String[1];
             args[0] = new String(e.getMessage());
             logger.error(e);
             throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, args, e);
         }
		 
	}
	
	/**
	 * Gets the records from the MASS_EDIT_HEADER and MASS_EDIT_DETAIL to show the upload
	 * status table on the PDB Mass Edit screen.
	 * 
	 * @param conn
	 * @return
	 * @throws PDBSystemException
	 * @throws PDBApplicationException
	 */
	public List<MassEditFileStatusVO> getMassEditFileStatus(Connection conn)
			throws PDBSystemException, PDBApplicationException {
		List<MassEditFileStatusVO> pdbMassEditFileStatusList = new ArrayList<MassEditFileStatusVO>();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        HashMap inputParams = new HashMap();
       // SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
     
        try {
        	dataRequest.setStatementID("PDB_GET_MASS_EDIT_FILE_STATUS");
        	dataRequest.setConnection(conn);
            dataRequest.setInputParams(inputParams);
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
            MassEditFileStatusVO vo = null;
            while(rs.next())
             {
                vo = new MassEditFileStatusVO();
                vo.setUser(rs.getString("CREATED_BY"));
                vo.setFileUploadDate(rs.getString("FILE_UPLOAD_DATE"));
                vo.setFileName(rs.getString("FILE_NAME")); 
                vo.setStatus(rs.getString("FILE_STATUS"));
                vo.setTotalCount(rs.getLong("TOTAL_RECORDS"));
                vo.setFailedRecordsCount(rs.getLong("RECORDSFAILED"));
                vo.setSuccessRecordsCount(rs.getLong("RECORDSSUCCESS"));
                vo.setErrorFileName(rs.getString("ERROR_FILE_NAME"));
                pdbMassEditFileStatusList.add(vo);
             }
         }
         catch(Exception e)
         {
             String[] args = new String[1];
             args[0] = new String(e.getMessage());
             logger.error(e);
             throw new PDBApplicationException(ProductMaintConstants.SQL_EXCEPTION, 
                                               args, e);
         }
         return pdbMassEditFileStatusList;
	}

} 