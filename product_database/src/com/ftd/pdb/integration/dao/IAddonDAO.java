package com.ftd.pdb.integration.dao;

import java.util.List;

import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBSystemException;

import java.sql.Connection;

public interface IAddonDAO
{
   /**
    * Retrieves all addon data from database for a given occasion and type.
    * @param conn database connection
    * @param type of addon
    * @param occasionID id
    * @return CardVO[]
    *
    */
     public List getAddonsByTypeOccasion(Connection conn,String type,int occasionID) 
             throws PDBSystemException, PDBApplicationException;


   /**
    * Retrieves all addon data from database for a type excluding the 
    * passed in occasion.
    * @param conn database connection
    * @param type of addon
    * @param occasionID ID
    * @return CardVO[]
    *
    */
     public List getAddonsByTypeExcludingOccasion(Connection conn, String type, int occasionID) 
             throws PDBSystemException, PDBApplicationException;            

}