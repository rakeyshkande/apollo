package com.ftd.pdb.integration.dao;

import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBSystemException;
import com.ftd.pdb.common.valobjs.HolidayPricingVO;

import java.sql.Connection;

public interface IHolidayPricingDAO
{

   /**
    * Retrieves holiday pricing record from database.
    * @param conn database connection
    * @return HolidayPricingVO
    * @deprecated Holiday Pricing is unused and will be removed in the future.
    *   lpuckett 08/16/2006
    */
     public HolidayPricingVO getHolidayPricing(Connection conn) 
             throws PDBSystemException, PDBApplicationException;


  /**
    * Sets the holiday pricing based values passed in.
    * @param conn database connection
    * @param holidayVO contains values to update
    * @deprecated Holiday Pricing is unused and will be removed in the future.
    *   lpuckett 08/16/2006
    */
     public void setHolidayPricing(Connection conn, HolidayPricingVO holidayVO) 
             throws PDBSystemException, PDBApplicationException;

}