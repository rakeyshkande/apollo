package com.ftd.pdb.integration.dao;

import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBSystemException;
import com.ftd.pdb.common.valobjs.AmazonProductAttributesVO;
import com.ftd.pdb.common.valobjs.AmazonProductMasterVO;
import com.ftd.pdb.common.valobjs.ProductVO;

import com.ftd.pdb.common.valobjs.VendorProductOverrideVO;

import java.util.List;
import java.math.BigDecimal;
import java.sql.Connection;

import java.util.Date;


/**
 * This interface is used to lookup Product information from the database
 *
 * @author Jeff Penney - Software Architects
 **/
public interface IProductDAO 
{
    /**
     * Returns an array of ProductVOs
     * @param conn database connection
     * @return array of products
     * @exception PDBSystemException
     */
    public List getProductList(Connection conn) throws PDBSystemException, PDBApplicationException;

    /**
     * Returns an array of ProductVOs based on filtering criteria
     * @param conn database connection
     * @param avail filter
     * @param unavail filter
     * @param dateRestrict filter
     * @param onHold filter
     * @return array of products
     * @exception PDBSystemException
     */
    public List getProductListAdvanced(Connection conn, String avail, String unavail, String dateRestrict, String onHold)
    throws PDBSystemException, PDBApplicationException;

    /**
     * Remove holiday pricing for the passed in product
     * @param conn database connection
     * @param product id
     * @exception PDBSystemException
     */
    public void removeHolidayPricing(Connection conn, String product) throws PDBSystemException, PDBApplicationException;

    /**
     * Returns an array of ProductVOs with holiday pricing
     * @param conn database connection
     * @return array of products
     * @exception PDBSystemException
     */
    public List getProductListWithHolidayPricing(Connection conn) throws PDBSystemException, PDBApplicationException;

    /**
    * Returns a ProductVO
    * @param conn database connection
    * @param productId product to be returned
    * @return ProductVO
    * @throws PDBApplicationException, PDBSystemException
    **/
   public ProductVO getProduct(Connection conn, String productId) throws PDBSystemException, PDBApplicationException;

    /**
    * Returns a ProductVO based on Novator ID
    * @param conn database connection
    * @param novatorId product to be returned
    * @return ProductVO
    * @throws PDBSystemException
    **/
   public ProductVO getProductByNovatorId(Connection conn, String novatorId) throws PDBSystemException, PDBApplicationException;

    /**
     * Returns an array of AribaCatalogRecordVOs
     * @param conn database connection
     * @param catalogFlag flag to be saved
     * @return array of catalog products
     * @exception PDBSystemException
     */
    public List getProductsByCatalog(Connection conn, String catalogFlag) throws PDBSystemException, PDBApplicationException;

    /**
     * Saves a ProductVO to the database
     * @param productVO that will be updated or created in the database
     * @exception PDBSystemException
     */
    public void setProduct(Connection conn, ProductVO productVO) throws PDBSystemException, PDBApplicationException;

    /**
     * Deletes a product from the database
     * @param conn database connection
     * @param productId that will be deleted in the database
     * @exception PDBSystemException
     */
    public void deleteProduct(Connection conn, String productId) throws PDBSystemException, PDBApplicationException;

  /**
   * Returns a list of holiday SKUs for all products
   * @param conn database connection
   * @throws PDBSystemException
   **/
    public List getHolidaySKUList(Connection conn) throws PDBSystemException, PDBApplicationException;

  /**
   * Returns a list of subcodes for all products
   * @param conn database connection
   * @throws PDBSystemException
   **/
    public List getSubCodeList(Connection conn) throws PDBSystemException, PDBApplicationException;    

  /**
   * Returns a list of subcodes with passed in subcode id
   * @param conn database connection
   * @param id (not used)
   * @throws PDBSystemException
   **/
    public List getSubCodesByID(Connection conn, String id) throws PDBSystemException, PDBApplicationException;        

    /**
     * Returns an array of product ids that have been updated since the passed in date
     * @param conn database connection
     * @param lastUpdateDate return any products that have been updated since this date
     * @return array of product ids
     * @exception PDBSystemException
     * @exception PDBApplicationException
     */
    public List getProductsUpdatedSince(Connection conn, java.util.Date lastUpdateDate) throws PDBSystemException, PDBApplicationException;    /**
     * Validates the passed in product id
     * @param conn database connection
     * @param productId to validate
     * @return true if the product id is a valid product id, novator id, or subsku
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public boolean validateProductId(Connection conn, String productId) throws PDBSystemException, PDBApplicationException;
    
    /**
     * Get a list of lists of VendorProductVOs for vendors and their skus for the passed
     * in product/sku id
     * @param conn database connection
     * @param productSkuId to retrieve
     * @return List of VendorProductVOs for the passed in prduct/sku id
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public List getVendorsForProduct(Connection conn, String productSkuId) throws PDBSystemException, PDBApplicationException;

    /**
     * Returns a list of products for the passed vendorId
     * @param conn database connection
     * @param vendorId filter
     * @return list of Strings containing the product Ids
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public List getVendorProducts(Connection conn, String vendorId) throws PDBSystemException, PDBApplicationException;
    
    /**
     * Returns a list of SDS ship methods for the passed carrierId
     * @param conn database connection
     * @param carrierId filter
     * @return list of Strings containing the product Ids
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public List getCarrierSdsShipMethods(Connection conn, String carrierId) throws PDBSystemException, PDBApplicationException;


    /**
     * Get a list of VendorProductOverrideVO objects, filtered on the passed variables
     * @param conn database connection
     * @param vendorId filter
     * @param productId filter
     * @param overrideDate filter
     * @param carrierId filter
     * @return
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public List getVPOverrides(Connection conn, String vendorId, String productId, Date overrideDate, String carrierId) throws PDBSystemException, PDBApplicationException;

    /**
     * Updates the values of the passed VendorProductOverrideVO in the VENDOR_PRODUCT_OVERRIDE table
     * @param conn
     * @param vpoVO data to update
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public void updateVPOverride(Connection conn, VendorProductOverrideVO vpoVO) throws PDBSystemException, PDBApplicationException;
    
    /**
     * Delete the values of the passed VendorProductOverrideVO in the VENDOR_PRODUCT_OVERRIDE table
     * @param conn
     * @param vpoVO data to update
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public void deleteVPOverride(Connection conn, VendorProductOverrideVO vpoVO) throws PDBSystemException, PDBApplicationException;
    
    /**
     * Checks to see if id exists in FTD_APPS.VENDOR_PRODUCTS
     * @param conn
     * @param productId to check
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public String existsInVendorProduct(Connection conn, String productId) throws PDBSystemException, PDBApplicationException;


    /**
     * Get a list of products who's availability need to be flipped because of 
     * product availablity needing to be triggered.
     * @param conn database connection
     * @return list of product ids that need to be switched.
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public List<String> getProductExceptionsToProcess(Connection conn) throws PDBSystemException, PDBApplicationException;

    /**
     * Get all the Amazon product record from the PTN_AMAZON.AZ_PRODUCT_MASTER table 
     * for the passed in product id.
     * @param conn database connection
     * @param productId product id
     * @return AmazonProductMasterVO object
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public AmazonProductMasterVO getAmazonProductMaster(Connection conn, String productId) throws PDBSystemException, PDBApplicationException;

    /**
     * Get all the Amazon product attributes records from the PTN_AMAZON.AZ_PRODUCT_ATTRIBUTES table 
     * for the passed in product id and attribute type.
     * @param conn database connection
     * @param productId product id
     * @param attributeType attribute type
     * @return list of AmazonProductAttributesVO objects
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public List<AmazonProductAttributesVO> getAmazonProductAttributes(Connection conn, String productId, String attributeType) throws PDBSystemException, PDBApplicationException;

    /**
     * Update or insert a record into the PTN_AMAZON.AZ_PRODUCT_MASTER table
     * @param conn database connection
     * @param vo contains data to apply to database record
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public void updateAmazonProductMaster(Connection conn, AmazonProductMasterVO vo) throws PDBSystemException, PDBApplicationException;

    /**
     * Update or insert a record into the PTN_AMAZON.AZ_PRODUCT_ATTRIBUTES table
     * @param conn database connection
     * @param vo contains data to apply to database record
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public void updateAmazonProductAttributes(Connection conn, String productId, List<AmazonProductAttributesVO> attributeList, String attributeType) throws PDBSystemException, PDBApplicationException;

    /**
     * Determine if the vender has a shipping account for the passed in partner
     * @param conn database connection
     * @param vendorId filter
     * @param partnerId filter
     * @return true if the account exist else false
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public boolean hasPartnerShipAccount(Connection conn, String vendorId, String partnerId) throws PDBSystemException, PDBApplicationException;
    
    /**
     * Determine if the source code exists.
     * @param conn
     * @param sourceCode
     * @return
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public boolean sourceCodeExists(Connection conn, String sourceCode) throws PDBSystemException, PDBApplicationException;
    
     /**
     * Gets the upsell master product ID if the passed product is an upsell base product
     * @param conn
     * @param prodId
     * @return
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
     public List getUpsellMasterForBaseId(Connection conn, String prodId) throws PDBSystemException, PDBApplicationException;

     /**
     * Gets the web-side product ID (i.e., Novator ID) for the passed product ID
     * @param conn
     * @param productId
     * @return
     * @throws Exception
     */
     public String getWebsiteProductId(Connection conn, String productId) throws Exception;
     
     /**
      * Saves Amazon data to the database
      * @param productVO that will be updated or created in the database
      * @exception PDBSystemException
      */
     public void setAmazonProduct(Connection conn, ProductVO productVO) throws PDBSystemException, PDBApplicationException;
     
     /**
      * Gets the Product List for PDB Mass Edit based on search criteria entered.
      * 
      * @param conn
      * @param search type 'M' - by master sku or 'P' by product id
      * @param String array containing either 1-5 master skus or 1-5 product ids
      * @return
      * @throws PDBApplicationException
      * @throws PDBSystemException
      */
     public List<ProductVO> getProductMassEditList(Connection conn, String searchType, 
             String[] searchItems) 
            throws PDBApplicationException, PDBSystemException;
     
     /**
      * Gets a count of the vendors available for a product (for PDB Mass Edit)
      * @param conn
      * @param productId
      * @return count returned from query
      * @throws PDBApplicationException
      * @throws PDBSystemException
      */
     public BigDecimal getVendorsAvailableForProduct(Connection conn, String productId) 
             throws PDBApplicationException, PDBSystemException;
     
     
     /**
      * Returns an array of ProductVOs based on PQuadproductId
      * @param conn database connection
      * @return array of products
      * @exception PDBSystemException
      */
     public List getPQuadProductList(Connection conn,String pquadProductId)throws PDBSystemException, PDBApplicationException;
     
     public void projectFreshBulkUpdate(Connection conn, String source, String dateStr) throws PDBApplicationException;
     public void projectFreshAddonUpdate(Connection conn, String dateStr) throws PDBApplicationException;

}