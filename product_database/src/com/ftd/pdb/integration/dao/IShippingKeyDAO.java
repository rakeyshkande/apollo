package com.ftd.pdb.integration.dao;

import java.util.List;

import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBSystemException;
import com.ftd.pdb.common.valobjs.ShippingKeyVO;

import java.sql.Connection;

public interface IShippingKeyDAO 
{
  /**
   * Returns an array of ShippingKeyVOs
   * @param conn database connection
   * @return array of ShippingKeyVOs
   * @throws PDBApplicationException, PDBSystemException
   **/
    public List getShippingKeyList(Connection conn)
        throws PDBApplicationException, PDBSystemException;

     /**
       * return a ShippingKeyVO for a given shippingID
       * @param conn database connection
       * @param  shippingID the selectecd id for the shipping key.
       * @return ShippingKeyVO
       * @throws PDBApplicationException, PDBSystemException
      **/
      public ShippingKeyVO getShippingKey (Connection conn, String shippingID )
          throws PDBApplicationException, PDBSystemException;

    /**
     * cascading deleting a row in Shipping_key table and rows in any table that has a foreign key reference.
     * @param conn database connection
     * @param  pShippingKeyVO the selected shipping key to be removed from database.
     * @return  Boolean true if succeed, false if failed.
     * @throws PDBApplicationException, PDBSystemException
    **/
    public boolean removeShippingKey (Connection conn, ShippingKeyVO pShippingKeyVO )
        throws PDBApplicationException, PDBSystemException;

    /**
     * Insert a row in Shipping_key table and rows in Shipping_key_details table.
     * @param conn database connection
     * @param  shippingKeyVO the selectecd id for the shipping key to be removed from database.
     * @return  Boolean true if succeed, false if failed.
     * @throws PDBApplicationException, PDBSystemException
    **/
    public boolean insertShippingKey (Connection conn, ShippingKeyVO shippingKeyVO )
        throws PDBApplicationException, PDBSystemException;

     /**
      * Update a row in Shipping_key table and rows in Shipping_key_details table.
      * @param conn database connection
      * @param  shippingKeyVO the selecteced id for the shipping key to be udpated from database.
      * @return  Boolean true if succeed, false if failed.
      * @throws PDBApplicationException, PDBSystemException
     **/
     public boolean updateShippingKey (Connection conn, ShippingKeyVO shippingKeyVO )
         throws PDBApplicationException, PDBSystemException;


}