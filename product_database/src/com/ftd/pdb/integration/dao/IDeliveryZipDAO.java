package com.ftd.pdb.integration.dao;

import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBSystemException;

import java.sql.Connection;

import org.w3c.dom.Document;

public interface IDeliveryZipDAO {
    
    /**
     * Get all the records from the venus.carrier table
     * @param conn database connection
     * @return xml representation of the returned carrier records
     * @throws Exception
     */
    public Document getCarriersXML(Connection conn) throws PDBSystemException;

    /**
     * @param conn database connection
     * @param zipCode filter
     * @return
     */
    public Document getBlockedZips(Connection conn, String zipCode) throws PDBSystemException;
    
    /**
     * Deletes a record from venus.carrier_excluded zips table
     * @param conn database connection
     * @param carrierId filter
     * @param zipCode filter
     * @throws PDBApplicationException
     * @throws PDBSystemException
     */
    public void deleteBlockedZip(Connection conn, String carrierId, String zipCode) throws PDBApplicationException, PDBSystemException;

    /**
     * Inserts a record from venus.carrier_excluded zips table
     * @param conn database connection
     * @param carrierId carrier code
     * @param zipCode postal code
     * @param userId user entering the record
     * @throws PDBApplicationException
     * @throws PDBSystemException
     */
    public void insertBlockedZip(Connection conn, String carrierId, String zipCode, String userId) throws PDBApplicationException, PDBSystemException;
}
