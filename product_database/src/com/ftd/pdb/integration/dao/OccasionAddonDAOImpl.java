package com.ftd.pdb.integration.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.exceptions.BadConnectionException;
import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBSystemException;


import java.sql.Connection;

import java.util.HashMap;


public class OccasionAddonDAOImpl extends PDBDataAccessObject implements IOccasionAddonDAO
{
    public OccasionAddonDAOImpl()
    {
         super("com.ftd.pdb.dao.OccasionAddonDAOImpl");
   }


  /**
   * Insert passed in occasion, addon.
   * @param conn database connection
   * @param occasionID ID
   * @param addonID ID
   * @return CardVO[]
   *
   */
    public void insert(Connection conn, int occasionID, String addonID) 
            throws PDBSystemException, PDBApplicationException
    {
        DataRequest dataRequest = new DataRequest();
          
        try
        {
            HashMap inputParams = new HashMap();
            inputParams.put("OCCASION_ID", occasionID);
            inputParams.put("ADDON_ID", addonID);
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_INSERT_OCCASION_ADDON");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            dataAccessUtil.execute(dataRequest);
        }
        catch(Exception e)
        {
            logger.error(e);
            throw new BadConnectionException(ProductMaintConstants.OE_BAD_CONNECTION_EXCEPTION, e);
        }

      }

  /**
   * Delete all rows for the passed in occasion.
   * @param conn database connection
   * @param occasionID ID
   * @return CardVO[]
   *
   */
    public void deleteByOccasion(Connection conn, int occasionID) 
            throws PDBSystemException, PDBApplicationException
    {
        DataRequest dataRequest = new DataRequest();
          
        try
        {
            HashMap inputParams = new HashMap();
            inputParams.put("OCCASION_ID", occasionID);
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_DELETE_OCC_ADDON_BY_OCC");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            dataAccessUtil.execute(dataRequest);
        }
        catch(Exception e)
        {
            logger.error(e);
            throw new BadConnectionException(ProductMaintConstants.OE_BAD_CONNECTION_EXCEPTION, e);
        }
      }
}