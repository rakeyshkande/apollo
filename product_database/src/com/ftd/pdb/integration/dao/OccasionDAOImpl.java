package com.ftd.pdb.integration.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.exceptions.BadConnectionException;
import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBSystemException;
import com.ftd.pdb.common.valobjs.OccasionVO;


import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


  /**
   * Implementation of Occasion DAO
   *
   */
public class OccasionDAOImpl extends PDBDataAccessObject implements IOccasionDAO
{
    public OccasionDAOImpl()
    {
         super("com.ftd.pdb.dao.OccasionDAOImpl");
    }


  /**
   * Retrieves occasion data from database.
   * @param conn database connection
   * @return OccasionVO[]
   *
   */
    public List getOccasions(Connection conn) 
            throws PDBSystemException, PDBApplicationException
    {
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;        
        ArrayList occasionList= new ArrayList();
        
        try
        {
            HashMap inputParams = new HashMap();
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_OCCASION_LIST");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

            while (rs.next())
            {
                OccasionVO occasion = new OccasionVO();
                occasion.setId(rs.getInt(ProductMaintConstants.OE_OCCASION_ID));
                occasion.setDescription(rs.getString(ProductMaintConstants.OE_OCCASION_DESCRIPTION));
                occasionList.add(occasion);
            } 
        }
        catch(Exception e)
        {
            logger.error(e);
            throw new BadConnectionException(ProductMaintConstants.OE_BAD_CONNECTION_EXCEPTION, e);
        }

        return occasionList;            
      }

  
}