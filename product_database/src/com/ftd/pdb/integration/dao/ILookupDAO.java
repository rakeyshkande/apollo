package com.ftd.pdb.integration.dao;

import com.ftd.pdb.common.exceptions.PDBSystemException;

import java.sql.Connection;

import java.util.List;

import org.w3c.dom.Document;


/**
 * This interface is used to lookup read-only information from the database
 *
 * @author Jeff Penney - Software Architects
 **/
public interface ILookupDAO {
    /**
       * Get a list of  Product Types
       * @param conn database connection
       * @return List of Types
       * @exception PDBSystemException
       */
    public List getTypesList(Connection conn) throws PDBSystemException;

    /**
       * Get a list of Companies
       * @param conn database connection
       * @return List of Companies
       * @exception PDBSystemException
       */
    public List getCompanyMasterList(Connection conn) throws PDBSystemException;

    /**
       * Get a list of  Product Sub Types
       * @param conn database connection
       * @return List of Sub Types
       * @exception PDBSystemException
       */
    public List getSubTypesList(Connection conn) throws PDBSystemException;

    /**
       * Get a list of  Carrier types
       * @param conn database connection
       * @return List of Categories
       * @exception PDBSystemException
       * Ed Mueller, 6/9/03
       */
    public List getCarrierTypes(Connection conn) throws PDBSystemException;


    /**
       * Get a list of  Product Categories
       * @param conn database connection
       * @return List of Categories
       * @exception PDBSystemException
       */
    public List getCategoryList(Connection conn) throws PDBSystemException;

    /**
       * Get a list of  JCPenney Product Categories
       * @param conn database connection
       * @return List of Categories
       * @exception PDBSystemException
       */
    public List getJCPenneyCategoryList(Connection conn) throws PDBSystemException;


    /**
      * Returns a List of Second Choices
      * @param conn database connection
      * @return List of Second Choices
      * @throws PDBSystemException
      **/
    public List getSecondChoiceList(Connection conn) throws PDBSystemException;

    /**
      * Returns a List of States
      * @param conn database connection
      * @return List of States
      * @throws PDBSystemException
      **/
    public List getStatesList(Connection conn) throws PDBSystemException;

    /**
      * Returns a List of Countries
      * @param conn database connection
      * @return List of Countries
      * @throws PDBSystemException
      **/
    public List getCountriesList(Connection conn) throws PDBSystemException;

    /**
       * Get a list of  Vendors
       * @param conn database connection
       * @return List of ValueVOs that have the vendor ID and name
       * @exception PDBSystemException
       */
    public List getVendorList(Connection conn) throws PDBSystemException;

    /**
       * Get a list of  Vendor Carriers
       * @param conn database connection
       * @return List of ValueVOs that have the vendor ID and name
       * @exception PDBSystemException
       */
    public List getVendorCarrierList(Connection conn) throws PDBSystemException;

    /**
       * Get a list of  ValueVOs representing shipping availability
       * @param conn database connection
       * @param typeCode filter
       * @return List of ValueVOs representing shipping availability
       * @exception PDBSystemException
       */
    public List getShippingAvailabilityList(Connection conn, 
                                            String typeCode) throws PDBSystemException;

    /**
       * Get a list of  ValueVOs representing price points
       * @param conn database connection
       * @return List of ValueVOs representing price points
       * @exception PDBSystemException
       */
    public List getPricePointsList(Connection conn) throws PDBSystemException;
   

    /**
       * Get a list of  ValueVOs representing shipping keys
       * @param conn database connection
       * @return List of ValueVOs representing shipping keys
       * @exception PDBSystemException
       */
    public List getShippingKeysList(Connection conn) throws PDBSystemException;

    /**
       * Get a list of  ValueVOs representing recipient search ids
       * @param conn database connection
       * @return List of ValueVOs representing recipient search ids
       * @exception PDBSystemException
       */
    public List getRecipientSearchList(Connection conn) throws PDBSystemException;

    /**
       * Get a list of  ValueVOs representing search priorities
       * @param conn database connection
       * @return List of ValueVOs representing search priorities
       * @exception PDBSystemException
       */
    public List getSearchPriorityList(Connection conn) throws PDBSystemException;

    /**
       * Get a list of  ValueVOs representing search priorities
       * @param conn database connection
       * @return List of ValueVOs representing search priorities
       * @exception PDBSystemException
       */
    public List getExceptionCodesList(Connection conn) throws PDBSystemException;

    /**
       * Get a list of  ValueVOs representing source codes
       * @param conn database connection
       * @return List of ValueVOs representing source codes
       * @exception PDBSystemException
       */
    public List getSourceCodeList(Connection conn) throws PDBSystemException;

    /**
       * Get a list of  ValueVOs representing shipping methods
       * @param conn database connection
       * @return List of ValueVOs representing shipping methods
       * @exception PDBSystemException
       */
    public List getShippingMethodsList(Connection conn) throws PDBSystemException;

    /**
       * Get a list of  ValueVOs representing available colors
       * @param conn database connection
       * @return List of ValueVOs representing available colors
       * @exception PDBSystemException
       */
    public List getColorsList(Connection conn) throws PDBSystemException;

    /**
       * Get a list of StateDeliveryExclusionVO representing states that have delivery exclusions
       * @param conn database connection
       * @return List of StateDeliveryExclusionVO representing states that have delivery exclusions
       * @exception PDBSystemException
       */
    public List getStateDeliveryExceptionList(Connection conn) throws PDBSystemException;

    /**
     * Get a list of  ValueVOs representing active vendor types
     * @param conn database connection
     * @return List of ValueVOs representing active vendor types
     * @exception PDBSystemException
     */
    public List getActiveVendorTypes(Connection conn) throws PDBSystemException;
    
    /**
     * Get a list of  ValueVOs representing box types
     * @param conn database connection
     * @return List of ValueVOs representing box types
     * @exception PDBSystemException
     */
    public List getBoxList(Connection conn) throws PDBSystemException;
    
    /**
     * Get a list of  Vendors that are of a specific type
     * @param conn database connection
     * @param vendorType filter
     * @return List of ValueVOs that have the vendor ID and name
     * @exception PDBSystemException
     */
    public List getVendorsOfType(Connection conn, String vendorType) throws PDBSystemException;

    /**
     * Retrieves a record from the FRP.GLOBAL_PARMS table
     * Calls FRP.MISC_PKG.GET_GLOBAL_PARM_VALUE.
     *
     * @param conn database connection
     * @param context filter
     * @param name filter
     * @return String value of record retrieved.  Null is returned if no 
     * matching record was found.
     */
    public String getGlobalParameter(Connection conn, String context, 
                                     String name) throws Exception;
    
    /**
     * Get a list of carriers
     * @param conn database connection
     * @return List of ValueVO containing carrier data
     * @throws PDBSystemException
     */
    public List getCarrierList(Connection conn) throws PDBSystemException;

    /**
       * Get a list of  ValueVOs representing available component skus
       * @param conn database connection
       * @return List of ValueVOs representing available component skus
       * @exception PDBSystemException
       */
    public List getComponentSkuList(Connection conn) throws PDBSystemException;

}
