package com.ftd.pdb.web;

import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.valobjs.ValueVO;
import com.ftd.pdb.service.ILookupSVCBusiness;
import com.ftd.pdb.service.IProductMaintSVCBusiness;

import java.io.IOException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.validator.DynaValidatorForm;
import com.ftd.pdb.common.utilities.XMLUtilities;
import com.ftd.pdb.common.valobjs.VendorVO;
import com.ftd.pdb.service.IProductMaintSVCBusiness;

import java.io.IOException;

import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;


public class VendorProductOverrideFlowAction extends PDBAction {
    /*Spring managed resources */
    ILookupSVCBusiness lookupSVC;
    IProductMaintSVCBusiness productSVC;
    /*end Spring managed resources */

    public VendorProductOverrideFlowAction() {
        super("com.ftd.pdb.web.VendorProductOverrideFlowAction");
    }

    public ActionForward execute(ActionMapping mapping, ActionForm form, 
                                 HttpServletRequest request, 
                                 HttpServletResponse response) throws IOException, 
                                                                      ServletException {
              
        DynaValidatorForm thisForm = (DynaValidatorForm)form;                                              
        //Set up the search types
        List searchTypeList = new ArrayList();
        ValueVO vo = new ValueVO();
        vo.setId("getall");
        vo.setDescription("Get All");
        searchTypeList.add(vo);
        
        vo = new ValueVO();
        vo.setId("carrier");
        vo.setDescription("Carrier");
        searchTypeList.add(vo);
        
        vo = new ValueVO();
        vo.setId("date");
        vo.setDescription("Date");
        searchTypeList.add(vo);
        
        vo = new ValueVO();
        vo.setId("product");
        vo.setDescription("Product ID");
        searchTypeList.add(vo);
        
        vo = new ValueVO();
        vo.setId("vendor");
        vo.setDescription("Vendor");
        searchTypeList.add(vo);
        
        thisForm.set(ProductMaintConstants.SEARCH_TYPE_LIST_KEY,searchTypeList);
        request.getSession().setAttribute(ProductMaintConstants.SEARCH_TYPE_LIST_KEY,searchTypeList);
        
        try {
            List vendorList = lookupSVC.getVendorList();
            VendorVO allVendorVO = new VendorVO();
            allVendorVO.setVendorId(ProductMaintConstants.ALL_VENDORS_VENDOR_ID);
            allVendorVO.setVendorName(ProductMaintConstants.ALL_VENDORS_VENDOR_NAME);
            allVendorVO.setVendorType(ProductMaintConstants.ALL_VENDORS_VENDOR_TYPE);
            vendorList.add(0,allVendorVO);
            thisForm.set(ProductMaintConstants.VENDOR_LIST_KEY,vendorList);
            request.getSession().setAttribute(ProductMaintConstants.VENDOR_LIST_KEY,vendorList);
            
            List carrierList = lookupSVC.getCarrierList();
            thisForm.set(ProductMaintConstants.CARRIER_LIST_KEY,carrierList);
            request.getSession().setAttribute(ProductMaintConstants.CARRIER_LIST_KEY,carrierList);
            
            //This is where the initial search is performed on page load
            //For now, we display nothing so the page loads quickly
            List searchResultsList = new ArrayList();
            thisForm.set(ProductMaintConstants.SEARCH_RESULT_LIST_KEY,searchResultsList);
            request.getSession().setAttribute(ProductMaintConstants.SEARCH_RESULT_LIST_KEY,searchResultsList);
            
        } catch (Exception e) {
            logger.error(e);
            return super.handleException(request, mapping, e);
        }

        return (mapping.findForward("success"));
    }

    public void setLookupSVC(ILookupSVCBusiness lookupSVC) {
        this.lookupSVC = lookupSVC;
    }

    public ILookupSVCBusiness getLookupSVC() {
        return lookupSVC;
    }

    public void setProductSVC(IProductMaintSVCBusiness productSVC) {
        this.productSVC = productSVC;
    }

    public IProductMaintSVCBusiness getProductSVC() {
        return productSVC;
    }

}
