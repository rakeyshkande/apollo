package com.ftd.pdb.web;

import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.exceptions.PDBException;
import com.ftd.pdb.common.valobjs.SearchKeyVO;
import com.ftd.pdb.common.valobjs.UpsellDetailVO;
import com.ftd.pdb.common.valobjs.UpsellMasterVO;
import com.ftd.pdb.common.valobjs.ValueVO;
import com.ftd.pdb.service.ILookupSVCBusiness;
import com.ftd.pdb.service.IUpsellSVCBusiness;
import com.sun.org.apache.xerces.internal.parsers.DOMParser;

import java.io.IOException;
import java.io.StringReader;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.w3c.dom.Document;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;


/**
 * Implementation of <strong>Action</strong> that processes a produst
 * list request.
 *
 * @author Ed Mueller
 */
public class UpsellDetailFlowAction extends PDBAction {
    /*Spring managed resources */
    private ILookupSVCBusiness lookupSVC;
    private IUpsellSVCBusiness upsellSVC;
    /*end Spring managed resources */

    public UpsellDetailFlowAction() {
        super("com.ftd.pdb.web.UpsellDetailFlowAction");
    }

    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param mapping The ActionMapping used to select this instance
     * @param form The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     */
    public

    ActionForward execute(ActionMapping mapping, ActionForm form, 
                          HttpServletRequest request, 
                          HttpServletResponse response) throws IOException, 
                                                               ServletException {

        //list used to hold associate skus
        List detailList = new ArrayList();
        try {
            // Validate the request parameters specified by the user
            ActionErrors errors = 
                (ActionErrors)request.getAttribute(Globals.ERROR_KEY);

            //if there are no errors add assoc flag to request
            if (errors == null || errors.size() <= 0) {
                request.setAttribute(ProductMaintConstants.PDB_PRODUCT_UPSELL_ERROR_DESC_KEY, 
                                     new String[ProductMaintConstants.PCB_UPSELL_MAINT_ERROR_MAX_IDX]);
                request.setAttribute(ProductMaintConstants.PDB_PRODUCT_UPSELL_ASSOC_ID_ERROR_KEY, 
                                     new String[ProductMaintConstants.UPSELL_MAX_ASSOC_SKU_AMOUNT]);
                request.setAttribute(ProductMaintConstants.PDB_PRODUCT_UPSELL_ASSOC_NAME_ERROR_KEY, 
                                     new String[ProductMaintConstants.UPSELL_MAX_ASSOC_SKU_AMOUNT]);
                request.setAttribute(ProductMaintConstants.PDB_PRODUCT_UPSELL_ASSOC_PRICE_ERROR_KEY, 
                                     new String[ProductMaintConstants.UPSELL_MAX_ASSOC_SKU_AMOUNT]);
                request.setAttribute(ProductMaintConstants.PDB_PRODUCT_UPSELL_ASSOC_TYPE_ERROR_KEY, 
                                     new String[ProductMaintConstants.UPSELL_MAX_ASSOC_SKU_AMOUNT]);
                request.setAttribute(ProductMaintConstants.PDB_PRODUCT_UPSELL_ASSOC_STATUS_ERROR_KEY, 
                                     new String[ProductMaintConstants.UPSELL_MAX_ASSOC_SKU_AMOUNT]);
            }

            //grab the  form object
            UpsellDetailForm upsellForm = (UpsellDetailForm)form;
            String sku = (String)upsellForm.get("masterSKU");

            // Get all values needed to prepopulate the form        
            request.setAttribute(ProductMaintConstants.RECIPIENT_SEARCH_KEY, 
                                 lookupSVC.getRecipientSearchList());
            request.setAttribute(ProductMaintConstants.SEARCH_PRIORITY_KEY, 
                                 lookupSVC.getSearchPriorityList());
            request.setAttribute(ProductMaintConstants.PDB_COMPANY_MASTER_KEY, 
                                 lookupSVC.getCompanyMasterList());

            //if form not populated then grab sku from request and retreive sku from database
            UpsellMasterVO upsellMasterVO = null;
            if (sku == null || sku.length() < 1) {
                sku = 
(String)request.getAttribute(ProductMaintConstants.OE_PRODUCT_ID_KEY);
                sku = sku.toUpperCase();
                upsellForm.set("masterSKU", sku);

                // Call the Service layer to get the master sku

                //retrieve master sku
                upsellMasterVO = upsellSVC.getMasterSKU(sku);
                //Only populate other fields if a master sku was found
                if (upsellMasterVO != null) {
                    upsellForm.set("masterSKU", sku);
                    upsellForm.set("arrangementAvailable", 
                                   upsellMasterVO.isMasterStatus());
                    upsellForm.set("productName", 
                                   upsellMasterVO.getMasterName());
                    upsellForm.set("description", 
                                   upsellMasterVO.getMasterDescription());
                    upsellForm.set("searchPriority", 
                                   upsellMasterVO.getSearchPriority());
                    upsellForm.set("corporateSiteFlag", 
                                   upsellMasterVO.isCorporateSiteFlag());
                    upsellForm.set("companyArray", 
                                   upsellMasterVO.getCompanyList());
                    upsellForm.set("gbbPopoverFlag",
                                   upsellMasterVO.getGbbPopoverFlag());
                    upsellForm.set("gbbTitle",
                                   upsellMasterVO.getGbbTitle());
                    upsellForm.set("gbbUpsellDetailId1",
                                   upsellMasterVO.getGbbUpsellDetailId1());
                    upsellForm.set("gbbNameOverrideFlag1",
                                   upsellMasterVO.getGbbNameOverrideFlag1());
                    upsellForm.set("gbbNameOverrideText1",
                                   upsellMasterVO.getGbbNameOverrideText1());
                    upsellForm.set("gbbPriceOverrideFlag1",
                                   upsellMasterVO.getGbbPriceOverrideFlag1());
                    upsellForm.set("gbbPriceOverrideText1",
                                   upsellMasterVO.getGbbPriceOverrideText1());
                    upsellForm.set("gbbUpsellDetailId2",
                                   upsellMasterVO.getGbbUpsellDetailId2());
                    upsellForm.set("gbbNameOverrideFlag2",
                                   upsellMasterVO.getGbbNameOverrideFlag2());
                    upsellForm.set("gbbNameOverrideText2",
                                   upsellMasterVO.getGbbNameOverrideText2());
                    upsellForm.set("gbbPriceOverrideFlag2",
                                   upsellMasterVO.getGbbPriceOverrideFlag2());
                    upsellForm.set("gbbPriceOverrideText2",
                                   upsellMasterVO.getGbbPriceOverrideText2());
                    upsellForm.set("gbbUpsellDetailId3",
                                   upsellMasterVO.getGbbUpsellDetailId3());
                    upsellForm.set("gbbNameOverrideFlag3",
                                   upsellMasterVO.getGbbNameOverrideFlag3());
                    upsellForm.set("gbbNameOverrideText3",
                                   upsellMasterVO.getGbbNameOverrideText3());
                    upsellForm.set("gbbPriceOverrideFlag3",
                                   upsellMasterVO.getGbbPriceOverrideFlag3());
                    upsellForm.set("gbbPriceOverrideText3",
                                   upsellMasterVO.getGbbPriceOverrideText3());

                    //create search key string
                    List keys = upsellMasterVO.getSearchKeyList();
                    StringBuffer sb = new StringBuffer();

                    for (int i = 0; i < keys.size(); i++) {
                        if (i > 0) {
                            sb.append(" ");
                        }

                        SearchKeyVO keyVO = (SearchKeyVO)keys.get(i);
                        sb.append(keyVO.getSearchKey());
                    }

                    upsellForm.set("keywordSearch", sb.toString());

                    // Set the recipient search array
                     upsellForm.set("recipientSearchArray",upsellMasterVO.getRecipientSearchList());
                     
                    //create website source codes string
                    List websites = upsellMasterVO.getWebsites();
                    sb = new StringBuffer();

                    for (int i = 0; i < websites.size(); i++) {
                        if (i > 0) {
                            sb.append(" ");
                        }

                        ValueVO valueVO = (ValueVO)websites.get(i);
                        sb.append(valueVO.getId());
                    }
                    upsellForm.set("websites", sb.toString());

                    //ensure sku fields are not null
                    detailList = upsellMasterVO.getUpsellDetailList();
                    for (int x = 0; x < detailList.size(); x++) {
                        UpsellDetailVO detailVO = 
                            (UpsellDetailVO)detailList.get(x);
                        if (detailVO.getType() == null) {
                            detailVO.setType("");
                        }
                    }

                    request.setAttribute(ProductMaintConstants.SHOW_ASSOCIATE_LIST_KEY, 
                                         detailList);

                    //put error checking here
                } else {
                    //place empty upsellMasterVO in requet
                    request.setAttribute(ProductMaintConstants.SHOW_ASSOCIATE_LIST_KEY, 
                                         new ArrayList());
                    request.setAttribute(ProductMaintConstants.PRODUCT_MAINT_INSTRUCTION_KEY, 
                                         super.getResources(request).getMessage(ProductMaintConstants.UPSELL_NEW_SKU_KEY));
                } //end if sku was found in databsase
            } else {
                //we have the Master sku in the form already
                detailList = upsellSVC.getSKUsFromRequest(request);
                request.setAttribute(ProductMaintConstants.SHOW_ASSOCIATE_LIST_KEY, 
                                     detailList);
            }

            List upsellDetailIdList = new ArrayList();
            ValueVO valueVO = new ValueVO();
            valueVO.setId("");
            valueVO.setDescription("");
            upsellDetailIdList.add(valueVO);

            //determine which check boxes to default  as checked
            for (int i = 0; i < detailList.size(); i++) {
                if (((UpsellDetailVO)detailList.get(i)).isSentToNovatorProd()) {
                    upsellForm.set("chkProd", true);
                }

                if (((UpsellDetailVO)detailList.get(i)).isSentToNovatorContent()) {
                    upsellForm.set("chkContent", true);
                }

                if (((UpsellDetailVO)detailList.get(i)).isSentToNovatorUAT()) {
                    upsellForm.set("chkUAT", true);
                }

                if (((UpsellDetailVO)detailList.get(i)).isSentToNovatorTest()) {
                    upsellForm.set("chkTest", true);
                }
                
                valueVO = new ValueVO();
                valueVO.setId(((UpsellDetailVO)detailList.get(i)).getDetailID());
                valueVO.setDescription(((UpsellDetailVO)detailList.get(i)).getDetailID());
                upsellDetailIdList.add(valueVO);

            } //end for loop

            request.setAttribute("upsellDetailIdList", upsellDetailIdList);

            if (errors != null && errors.size() > 0) {
                Iterator it = 
                    errors.get(ProductMaintConstants.APPLICATION_CATEGORY_CONSTANT);
                while (it.hasNext()) {
                    ActionMessage error = (ActionMessage)it.next();
                    if (error.getKey().equals(String.valueOf(ProductMaintConstants.PDB_NOVATOR_EXCEPTION))) {
                        Object errobjs[] = error.getValues();
                        if (errobjs != null && errobjs.length > 0) {
                            //Only worry about the first entry
                            String strError = 
                                ((String)errobjs[0]).toLowerCase();
                            if (strError == null) //Sanity check
                                continue;

                            //parse string in XML document
                            DOMParser parser = new DOMParser();
                            Document doc;
                            NodeList errorFields = null;
                            Element errorField;
                            boolean bValidDocument;
                            try {
                                parser.parse(new InputSource(strError));
                                doc = parser.getDocument();
                                errorFields = DOMUtil.selectNodes(doc, "//error");
                                bValidDocument = true;
                            } catch (Exception e) {
                                //Not a valid xml document so assume just pass the
                                //error string along
                                bValidDocument = false;
                            }

                            //Assumption made here is that the error string was 
                            //processed properly by NovatorTransmission.class
                            if (bValidDocument) {
                                boolean bHasNovatorErrors = false;

                                int iErrorCount = errorFields.getLength();
                                String[] assocIdErrorStrings = 
                                    new String[ProductMaintConstants.UPSELL_MAX_ASSOC_SKU_AMOUNT];
                                String[] assocNameErrorStrings = 
                                    new String[ProductMaintConstants.UPSELL_MAX_ASSOC_SKU_AMOUNT];
                                String[] assocPriceErrorStrings = 
                                    new String[ProductMaintConstants.UPSELL_MAX_ASSOC_SKU_AMOUNT];
                                String[] assocTypeErrorStrings = 
                                    new String[ProductMaintConstants.UPSELL_MAX_ASSOC_SKU_AMOUNT];
                                String[] assocStatusErrorStrings = 
                                    new String[ProductMaintConstants.UPSELL_MAX_ASSOC_SKU_AMOUNT];
                                String[] errorStrings = 
                                    new String[ProductMaintConstants.PCB_UPSELL_MAINT_ERROR_MAX_IDX];

                                for (int idx = 0; idx < iErrorCount; idx++) {
                                    errorField = 
                                            (Element)errorFields.item(idx);
                                    if (errorField == null) //Sanity check 
                                        continue;

                                    String strNodeName = null;
                                    String strFieldValue = null;
                                    String strDescription = null;
                                    String strSku = null;

                                    NodeList childNodes = 
                                        errorField.getChildNodes();
                                    Node childNode;

                                    for (int chIdx = 0; 
                                         chIdx < childNodes.getLength(); 
                                         chIdx++) {
                                        childNode = childNodes.item(chIdx);
                                        if (childNode == null)
                                            continue;

                                        strNodeName = childNode.getNodeName();
                                        childNode = childNode.getFirstChild();

                                        if (childNode != null) {
                                            if (strNodeName.compareToIgnoreCase("field") == 
                                                0)
                                                strFieldValue = 
                                                        childNode.getNodeValue();
                                            else if (strNodeName.compareToIgnoreCase("description") == 
                                                     0)
                                                strDescription = 
                                                        childNode.getNodeValue();
                                            else if (strNodeName.compareToIgnoreCase("subcode") == 
                                                     0)
                                                strSku = 
                                                        childNode.getNodeValue();
                                        }
                                    }

                                    if (strFieldValue == null || 
                                        strFieldValue.length() == 
                                        0) //Sanity check
                                        continue;

                                    if (strFieldValue.compareToIgnoreCase(ProductMaintConstants.PDB_NOVATOR_UPSELL_FIELD_NAME_TITLE) == 
                                        0) {
                                        upsellForm.set("ProductNameError", 
                                                       true);
                                        bHasNovatorErrors = true;
                                        errorStrings[ProductMaintConstants.PDB_UPSELL_MAINT_ERROR_IDX_NAME] = 
                                                strDescription;
                                    }

                                    else if (strFieldValue.compareToIgnoreCase(ProductMaintConstants.PDB_NOVATOR_UPSELL_FIELD_NAME_DESCRIPTION) == 
                                             0) {
                                        upsellForm.set("DescriptionError", 
                                                       true);
                                        bHasNovatorErrors = true;
                                        errorStrings[ProductMaintConstants.PDB_UPSELL_MAINT_ERROR_IDX_DESCRIPTION] = 
                                                strDescription;
                                    }

                                    else if (strFieldValue.compareToIgnoreCase(ProductMaintConstants.PDB_NOVATOR_UPSELL_FIELD_NAME_DETAIL) == 
                                             0) {
                                        upsellForm.set("DetailError", true);
                                        bHasNovatorErrors = true;
                                        errorStrings[ProductMaintConstants.PDB_UPSELL_MAINT_ERROR_IDX_DETAIL] = 
                                                strDescription;
                                    }

                                    else if (strFieldValue.compareToIgnoreCase(ProductMaintConstants.PDB_NOVATOR_UPSELL_FIELD_NAME_MASTER_ID) == 
                                             0) {
                                        upsellForm.set("MasterIdError", true);
                                        bHasNovatorErrors = true;
                                        errorStrings[ProductMaintConstants.PDB_UPSELL_MAINT_ERROR_IDX_MASTER_ID] = 
                                                strDescription;
                                    }

                                    else if (strFieldValue.compareToIgnoreCase(ProductMaintConstants.PDB_NOVATOR_UPSELL_FIELD_NAME_DETAIL_ID) == 
                                             0) {
                                        for (int skuidx = 0; 
                                             skuidx < detailList.size(); 
                                             skuidx++) {
                                            UpsellDetailVO udvo = 
                                                (UpsellDetailVO)detailList.get(skuidx);
                                            if (udvo != null) {
                                                if (strSku.compareToIgnoreCase(udvo.getDetailID()) == 
                                                    0) {
                                                    assocIdErrorStrings[skuidx] = 
                                                            strDescription;
                                                    bHasNovatorErrors = true;
                                                    break;
                                                }
                                            }
                                        }
                                    }

                                    else if (strFieldValue.compareToIgnoreCase(ProductMaintConstants.PDB_NOVATOR_UPSELL_FIELD_NAME_DETAIL_TITLE) == 
                                             0) {
                                        for (int skuidx = 0; 
                                             skuidx < detailList.size(); 
                                             skuidx++) {
                                            UpsellDetailVO udvo = 
                                                (UpsellDetailVO)detailList.get(skuidx);
                                            if (udvo != null) {
                                                if (strSku.compareToIgnoreCase(udvo.getDetailID()) == 
                                                    0) {
                                                    assocNameErrorStrings[skuidx] = 
                                                            strDescription;
                                                    bHasNovatorErrors = true;
                                                    break;
                                                }
                                            }
                                        }
                                    }

                                    else if (strFieldValue.compareToIgnoreCase(ProductMaintConstants.PDB_NOVATOR_UPSELL_FIELD_NAME_DETAIL_PRICE) == 
                                             0) {
                                        for (int skuidx = 0; 
                                             skuidx < detailList.size(); 
                                             skuidx++) {
                                            UpsellDetailVO udvo = 
                                                (UpsellDetailVO)detailList.get(skuidx);
                                            if (udvo != null) {
                                                if (strSku.compareToIgnoreCase(udvo.getDetailID()) == 
                                                    0) {
                                                    assocPriceErrorStrings[skuidx] = 
                                                            strDescription;
                                                    bHasNovatorErrors = true;
                                                    break;
                                                }
                                            }
                                        }
                                    }

                                    else if (strFieldValue.compareToIgnoreCase(ProductMaintConstants.PDB_NOVATOR_UPSELL_FIELD_NAME_DETAIL_TYPE) == 
                                             0) {
                                        for (int skuidx = 0; 
                                             skuidx < detailList.size(); 
                                             skuidx++) {
                                            UpsellDetailVO udvo = 
                                                (UpsellDetailVO)detailList.get(skuidx);
                                            if (udvo != null) {
                                                if (strSku.compareToIgnoreCase(udvo.getDetailID()) == 
                                                    0) {
                                                    assocTypeErrorStrings[skuidx] = 
                                                            strDescription;
                                                    bHasNovatorErrors = true;
                                                    break;
                                                }
                                            }
                                        }
                                    }

                                    else if (strFieldValue.compareToIgnoreCase(ProductMaintConstants.PDB_NOVATOR_UPSELL_FIELD_NAME_DETAIL_STATUS) == 
                                             0) {
                                        for (int skuidx = 0; 
                                             skuidx < detailList.size(); 
                                             skuidx++) {
                                            UpsellDetailVO udvo = 
                                                (UpsellDetailVO)detailList.get(skuidx);
                                            if (udvo != null) {
                                                if (strSku.compareToIgnoreCase(udvo.getDetailID()) == 
                                                    0) {
                                                    assocStatusErrorStrings[skuidx] = 
                                                            strDescription;
                                                    bHasNovatorErrors = true;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }

                                if (bHasNovatorErrors == true) {
                                    request.setAttribute(ProductMaintConstants.PDB_PRODUCT_UPSELL_ASSOC_ID_ERROR_KEY, 
                                                         assocIdErrorStrings);
                                    request.setAttribute(ProductMaintConstants.PDB_PRODUCT_UPSELL_ASSOC_NAME_ERROR_KEY, 
                                                         assocNameErrorStrings);
                                    request.setAttribute(ProductMaintConstants.PDB_PRODUCT_UPSELL_ASSOC_PRICE_ERROR_KEY, 
                                                         assocPriceErrorStrings);
                                    request.setAttribute(ProductMaintConstants.PDB_PRODUCT_UPSELL_ASSOC_TYPE_ERROR_KEY, 
                                                         assocTypeErrorStrings);
                                    request.setAttribute(ProductMaintConstants.PDB_PRODUCT_UPSELL_ASSOC_STATUS_ERROR_KEY, 
                                                         assocStatusErrorStrings);
                                    request.setAttribute(ProductMaintConstants.PDB_PRODUCT_UPSELL_ERROR_DESC_KEY, 
                                                         errorStrings);
                                    request.setAttribute(ProductMaintConstants.PDB_PRODUCT_ERROR_HOLIDAY_SUB_PRICE_KEY, 
                                                         errorStrings); //jsp is expecting it
                                    errobjs[0] = 
                                            "Database has not been updated.  The fields marked in RED are in error and need to be changed.";
                                }
                            } //end if bValidXMLDocument
                        } //end if( errobjs!=null && errobjs.length>0 )
                    } //end if(error.getKey().equals(String.valueOf(ProductMaintConstants.PDB_NOVATOR_EXCEPTION)))
                } //end while(it.hasNext())
            } //end if(errors != null && errors.size() > 0)

            request.setAttribute(mapping.getAttribute(), upsellForm);

        } catch (PDBException ftdException) {
            return super.handleException(request, mapping, ftdException);
        }

        // Set the back link
        request.setAttribute(ProductMaintConstants.MENU_BACK_KEY, 
                             ProductMaintConstants.MENU_BACK_UPSELL_LIST_KEY);
        request.setAttribute(ProductMaintConstants.MENU_BACK_TEXT_KEY, 
                             ProductMaintConstants.MENU_BACK_TEXT_UPSELL_LIST_KEY);

        // Forward control to the specified success URI
        return (mapping.findForward("success"));
    }

    public void setLookupSVC(ILookupSVCBusiness lookupSVC) {
        this.lookupSVC = lookupSVC;
    }

    public ILookupSVCBusiness getLookupSVC() {
        return lookupSVC;
    }

    public void setUpsellSVC(IUpsellSVCBusiness upsellSVC) {
        this.upsellSVC = upsellSVC;
    }

    public IUpsellSVCBusiness getUpsellSVC() {
        return upsellSVC;
    }
}
