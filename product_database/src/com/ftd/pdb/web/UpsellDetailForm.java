package com.ftd.pdb.web;

import com.ftd.pdb.common.ProductMaintConstants;

import com.ftd.pdb.service.IProductMaintSVCBusiness;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionServlet;


public class UpsellDetailForm extends PDBActionForm {

    private IProductMaintSVCBusiness productSVC;
    
    public UpsellDetailForm() {
        super("com.ftd.pdb.web.UpsellDetailForm");
    }

    public void setServlet(ActionServlet actionServlet) {
        super.setServlet(actionServlet);
        productSVC = (IProductMaintSVCBusiness)getWebApplicationContext().getBean("productSVC");
    }
    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public

    ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

        ActionErrors errors = super.validate(mapping, request);

        //name is required
        String testString = (String)get("productName");
        if (StringUtils.isBlank(testString)) {
            errors.add("productName",new ActionMessage(ProductMaintConstants.PDB_UPSELL_MAINT_ERROR_STRING_NAME_MISSING, false) );
        } else {
            if (StringUtils.trim(testString).length() > 
                ProductMaintConstants.UPSELL_PRODUCT_MASTER_NAME_MAX) {
                errors.add("productName",new ActionMessage(ProductMaintConstants.PDB_UPSELL_MAINT_ERROR_STRING_NAME_MAX, false) );
            }
        }

        //description is required
        testString = (String)get("description");
        if (StringUtils.isBlank(testString)) {
            errors.add("description",new ActionMessage(ProductMaintConstants.PDB_UPSELL_MAINT_ERROR_STRING_DESCRIPTION_MISSING, false) );
        } else {
            if (StringUtils.trim(testString).length() > 
                ProductMaintConstants.UPSELL_PRODUCT_DESCRIPTION_MAX) {
                errors.add("description",new ActionMessage(ProductMaintConstants.PDB_UPSELL_MAINT_ERROR_STRING_DESCRIPTION_MAX, false) );
            }
        }

        String[] detailIdArray = 
            request.getParameterValues(ProductMaintConstants.PDB_UPSELL_DETAIL_ID_KEY);
        String[] detailNameArray = 
            request.getParameterValues(ProductMaintConstants.PDB_UPSELL_DETAIL_NAME_KEY);

        //subcode array of error flags
        // Check for company
        String[] list = (String[])get("companyArray");
        if (list == null || list.length == 0) {
            errors.add("companyArray",new ActionMessage(ProductMaintConstants.PDB_UPSELL_MAINT_ERROR_STRING_COMPANY, false) );
        }

        //check for min amount of assoc skus
        if (detailIdArray == null || 
            detailIdArray.length < ProductMaintConstants.UPSELL_MIN_ASSOC_SKU_AMOUNT) {
            errors.add("associatedSkus",new ActionMessage(ProductMaintConstants.PDB_UPSELL_MAINT_ERROR_STRING_DETAIL_MIN, false) );
        }

        //check for min amount of assoc skus
        if (detailIdArray == null || 
            detailIdArray.length > ProductMaintConstants.UPSELL_MAX_ASSOC_SKU_AMOUNT) {
            errors.add("associatedSkus",new ActionMessage(ProductMaintConstants.PDB_UPSELL_MAINT_ERROR_STRING_DETAIL_MAX, false) );
        }

        boolean hasName = true;
        boolean nameIsRightSize = true;
        //if something was entred
        if (detailIdArray != null) {
                 //make sure each name contains a value and is not to large
                for (int i = 0; i < detailNameArray.length; i++) {
                    if (detailNameArray[i] != null) {
                        if (detailNameArray[i].trim().equals(ProductMaintConstants.BLANK_STRING)) {
                            hasName = false;
                        } else if (detailNameArray[i].trim().length() > 
                                   ProductMaintConstants.UPSELL_ASSOC_NAME_MAX) {
                            nameIsRightSize = false;
                        }
                    }
                } //end for
            }//end if detailarray not null   
        if (!hasName){
            errors.add("associatedSkus",new ActionMessage(ProductMaintConstants.PDB_UPSELL_MAINT_ERROR_ASSOC_SKU_NAME_MISSING, false) );           
        }
        if (!nameIsRightSize){
            errors.add("associatedSkus",new ActionMessage(ProductMaintConstants.PDB_UPSELL_MAINT_ERROR_ASSOC_SKU_NAME_MAX, false) );
        }
        
        // Validate source codes in websites field.
        try {
            String invalidSourceCodes = productSVC.validateWebsites((String)get("websites"));
            if(!StringUtils.isBlank(invalidSourceCodes)) {
                errors.add("websites", new ActionMessage("Invalid source code:" + invalidSourceCodes, false));
            }
        } catch (Exception e) {
            logger.error(e);
            errors.add("websites", new ActionMessage("Source code cannot be validated. Please contact IT support.", false));
        }

        return errors;
    }
}
