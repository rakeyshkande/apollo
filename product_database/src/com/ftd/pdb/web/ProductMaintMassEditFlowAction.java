package com.ftd.pdb.web;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;

import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBSystemException;
import com.ftd.pdb.common.valobjs.MassEditFileStatusVO;
import com.ftd.pdb.service.ILookupSVCBusiness;
import com.ftd.pdb.service.IProductMaintSVCBusiness;


public class ProductMaintMassEditFlowAction extends PDBAction {
	
	/*Spring managed resources */
    IProductMaintSVCBusiness productSVC;
    /*end Spring managed resources */
	
    public ProductMaintMassEditFlowAction() {
    	super("com.ftd.pdb.web.ProductMaintMassEditFlowAction");
    }

    public ActionForward execute(ActionMapping mapping, ActionForm form, 
                                 HttpServletRequest request, 
                                 HttpServletResponse response) throws IOException, 
                                                                      ServletException {
        ActionMessages errors = new ActionMessages();
        if (logger.isDebugEnabled()) {
        	logger.debug(">>> Begin ProductMaintMassEditFlowAction");
        }
        request.setAttribute("adminAction", "merchandising");
        // get the Mass Edit File status rows to populate the table in the jsp
        try {
        	HashMap<String, Object> pdbHashMap = productSVC.getMassEditFileStatus();
        	String pdbMassEditFileDirectory = (String) pdbHashMap.get("pdbMassEditFileDirectory");
        	List<MassEditFileStatusVO> pdbMassEditFileStatusList = 
        		(List<MassEditFileStatusVO>) pdbHashMap.get("pdbMassEditFileStatusList");
        	request.setAttribute("pdbMassEditFileDirectory", pdbMassEditFileDirectory);
        	request.setAttribute("pdbMassEditFileStatusList", pdbMassEditFileStatusList);
        }
        catch(Exception e) {
        	System.out.println(">>> Could not create Mass Edit Spreadsheet due to the following error: " + e.getMessage());
          	 
       		logger.error(">>> Could not create Mass Edit Spreadsheet due to the following error: " + e.getMessage());
       	   	e.printStackTrace();                           
       		//put the errors into the request
        	errors.add("global", 
                        new ActionError(">>> Error trying to get PDB Mass Edit File Status due to the following " + e.getMessage())); 
        		 
        }
        return (mapping.findForward("success"));
        
    }

	public IProductMaintSVCBusiness getProductSVC() {
		return productSVC;
	}

	public void setProductSVC(IProductMaintSVCBusiness productSVC) {
		this.productSVC = productSVC;
	}
    
    
    
  }
