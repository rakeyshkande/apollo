package com.ftd.pdb.web;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.valobjs.HolidayPricingVO;
import com.ftd.pdb.common.valobjs.ValueVO;
import com.ftd.pdb.service.IProductMaintSVCBusiness;

import java.io.IOException;

import java.text.SimpleDateFormat;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * Implementation of <strong>Action</strong> that processes a
 * holiday pricing activation change.
 *
 * @author Ed Mueller
 */

public final class HolidayPricingActivateAction extends PDBAction
{
    /*Spring managed resources */
    private IProductMaintSVCBusiness productSVC;
    /*end Spring managed resources */
    
    /**
      * Constructor
      */
    public HolidayPricingActivateAction()
    {
      super("com.ftd.pdb.web.HolidayPricingActivateAction");
    }

	/**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param mapping The ActionMapping used to select this instance
     * @param form The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     * @deprecated Holiday Pricing is unused and will be removed in the future.
     *   lpuckett 08/16/2006
     */    
    public ActionForward execute(ActionMapping mapping,
				 ActionForm form,
				 HttpServletRequest request,
				 HttpServletResponse response)
	throws IOException, ServletException
    {
        try {          

            HolidayPricingActivateForm holidayForm = (HolidayPricingActivateForm)form;
          
            SimpleDateFormat sdf = new SimpleDateFormat( "MM/dd/yy" ); 

            //Create holiday VO
            HolidayPricingVO holidayVO = new HolidayPricingVO();
            holidayVO.setStartDate(sdf.parse((String)holidayForm.get("startDate")));
            holidayVO.setEndDate(sdf.parse((String)holidayForm.get("endDate")));
            holidayVO.setHolidayPriceFlag((Boolean)holidayForm.get("holidayPriceFlag"));
            holidayVO.setDeliveryDateFlag((Boolean)holidayForm.get("deliveryDateFlag"));            
        
            // instantiate service delegate and call method to get data

            // Get the Novator servers to send updates
            Map novatorMap = new HashMap();
            
            try {
                ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
                
                if((Boolean)holidayForm.get("chkLive"))
                {
                    String key = configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, ProductMaintConstants.PDB_NOVATOR_LIVE_IP_KEY);
                    String value = configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, ProductMaintConstants.PDB_NOVATOR_LIVE_PORT_KEY);
                    ValueVO mapVO = new ValueVO();
                    mapVO.setId(key);
                    mapVO.setDescription(value);
                    novatorMap.put("live", mapVO);
                }
                if((Boolean)holidayForm.get("chkTest"))
                {
                    String key = configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, ProductMaintConstants.PDB_NOVATOR_TEST_IP_KEY);
                    String value = configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, ProductMaintConstants.PDB_NOVATOR_TEST_PORT_KEY);
                    ValueVO mapVO = new ValueVO();
                    mapVO.setId(key);
                    mapVO.setDescription(value);
                    novatorMap.put("test", mapVO);
                }
                if((Boolean)holidayForm.get("chkUAT"))
                {
                    String key = configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, ProductMaintConstants.PDB_NOVATOR_UAT_IP_KEY);
                    String value = configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, ProductMaintConstants.PDB_NOVATOR_UAT_PORT_KEY);
                    ValueVO mapVO = new ValueVO();
                    mapVO.setId(key);
                    mapVO.setDescription(value);
                    novatorMap.put("uat", mapVO);
                }
                if((Boolean)holidayForm.get("chkContent"))
                {
                    String key = configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, ProductMaintConstants.PDB_NOVATOR_CONTENT_IP_KEY);
                    String value = configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, ProductMaintConstants.PDB_NOVATOR_CONTENT_PORT_KEY);
                    ValueVO mapVO = new ValueVO();
                    mapVO.setId(key);
                    mapVO.setDescription(value);
                    novatorMap.put("content", mapVO);
                }
            } catch (Exception e) {
                logger.error(e);
                throw new ServletException("Failed to retrieve configuration property", 
                                           e);
            }
                    
            productSVC.setHolidayPricing(holidayVO, novatorMap);

           
        }

        catch(java.text.ParseException e) {
           return super.handleException(request,mapping,e);
        }
        catch(Exception e)  {
            return super.handleException(request,mapping,e);
        }


		// Forward control to the specified success URI
		return (mapping.findForward("success"));
    }

    public void setProductSVC(IProductMaintSVCBusiness productSVC) {
        this.productSVC = productSVC;
    }

    public IProductMaintSVCBusiness getProductSVC() {
        return productSVC;
    }
}
