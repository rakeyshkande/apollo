package com.ftd.pdb.web;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBException;
import com.ftd.pdb.common.utilities.FTDUtil;
import com.ftd.pdb.common.valobjs.ColorVO;
import com.ftd.pdb.common.valobjs.ProductAddonVO;
import com.ftd.pdb.common.valobjs.ProductBatchVO;
import com.ftd.pdb.common.valobjs.ProductSubCodeVO;
import com.ftd.pdb.common.valobjs.ProductVO;
import com.ftd.pdb.common.valobjs.StateDeliveryExclusionVO;
import com.ftd.pdb.common.valobjs.ValueVO;
import com.ftd.pdb.service.IProductBatchSVCBusiness;
import com.ftd.pdb.service.IProductMaintSVCBusiness;
import com.ftd.pdb.service.IUpsellSVCBusiness;

import java.io.IOException;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.sql.Connection;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.validator.DynaValidatorForm;

import org.springframework.context.ApplicationContext;


public class ProductMaintDetailAction extends PDBAction {
    /*Spring managed resources */
    private IProductBatchSVCBusiness productBatchSVC;
    private IProductMaintSVCBusiness productSVC;
    private IUpsellSVCBusiness upsellSVC;
    /*end Spring managed resources */
    

    public ProductMaintDetailAction() {
        super("com.ftd.pdb.web.ProductMaintDetailAction");
    }


    public ActionForward execute(ActionMapping mapping, ActionForm form, 
                                 HttpServletRequest request, 
                                 HttpServletResponse response) throws IOException, 
                                                                      ServletException {

        ProductMaintDetailForm productDetailForm = (ProductMaintDetailForm)form;
        HttpSession session = request.getSession();
       //flag to indicate if this product has upsells
        boolean hasUpsells = false;
            
        // Call the Service layer to update the Product
        ProductVO product = new ProductVO();
        
        try {
            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
            // Set all the product properties ---------------------------------
            BeanUtils.copyProperties(product, productDetailForm);
            // End price conversion -------------------------------------------
            
            // add the state delivery exclusion
            List excludedStates = ProductMaintDetailForm.extractExcludedStatesFromMap(request.getParameterMap());
            product.setExcludedDeliveryStates(excludedStates);

            List deliveryOptionList = product.buildDeliveryOptionsList(request);
            product.setShipMethods(deliveryOptionList);
           List subCodeList = ProductMaintDetailForm.extractSubcodeListFromMap(request.getParameterMap());
            // add sub code to the ProductVO
            product.setSubCodeList(subCodeList);
           
            String[] recipientSearch = (String[])productDetailForm.get("recipientSearch");
            product.setRecipientSearch(recipientSearch);
          
            String personalizationTemplate = (String)productDetailForm.get("personalizationTemplate");
            if(personalizationTemplate == null || personalizationTemplate.equals("NONE"))
            {
                product.setPersonalizationTemplate(null);
                product.setPersonalizationTemplateOrder(null);
            }
            else
            {
                product.setPersonalizationTemplate((String)productDetailForm.get("personalizationTemplate"));
                product.setPersonalizationTemplateOrder((String)productDetailForm.get("personalizationTemplateOrder"));
                logger.info("ftdwestAccessoryId:"+(String)productDetailForm.get("ftdwestAccessoryId"));
                product.setFtdwestAccessoryId((String)productDetailForm.get("ftdwestAccessoryId"));
                logger.info("ftdwestTemplateMapping:"+(String)productDetailForm.get("ftdwestTemplateMapping"));
                product.setFtdwestTemplateMapping((String)productDetailForm.get("ftdwestTemplateMapping"));
                product.setPersonalizationCaseFlag(productDetailForm.get("personalizationCaseFlag") != null ? (Boolean)productDetailForm.get("personalizationCaseFlag") : false);
                product.setAllAlphaFlag(productDetailForm.get("allAlphaFlag") != null ? (Boolean)productDetailForm.get("allAlphaFlag") : false);
            }
            product.setPersonalizationLeadDays((String)productDetailForm.get("personalizationLeadDays"));

            String[] companyList = (String[])productDetailForm.get("companyList");
            product.setCompanyList(companyList);
           
            // Add the colors list in the correct order
             String[] colorsArray = 
                 (String[])productDetailForm.get("arrangementColorsArray");
             List colors = new ArrayList();
             if (colorsArray != null) {
                 for (int i = 0; i < colorsArray.length; i++) {
                     ColorVO color = new ColorVO();
                     color.setId((String)colorsArray[i]);
                     color.setOrderBy(i);
                     colors.add(color);
                 }
             }            
            
            HashMap <String, ArrayList<ProductAddonVO>> productAddonMap = new HashMap <String, ArrayList<ProductAddonVO>>();
            
            ArrayList<ProductAddonVO> productAddonList = ProductMaintDetailForm.extractProductAddonListFromMap(request.getParameterMap(),ProductMaintConstants.ADD_ON_ACTIVE_FLAG_KEY,
                                                                  ProductMaintConstants.ADD_ON_PRODUCT_DISPLAY_SEQUENCE_NUMBER_KEY, ProductMaintConstants.ADD_ON_PRODUCT_MAX_QUANTITY_KEY,
                                                                  (ArrayList<ProductAddonVO>)session.getAttribute(ProductMaintConstants.PRODUCT_ADD_ON_LIST));
            ArrayList<ProductAddonVO> productAddonVaseList = ProductMaintDetailForm.extractProductAddonListFromMap(request.getParameterMap(),ProductMaintConstants.ADD_ON_VASE_ACTIVE_FLAG_KEY,
                                                                  ProductMaintConstants.ADD_ON_VASE_PRODUCT_DISPLAY_SEQUENCE_NUMBER_KEY, ProductMaintConstants.ADD_ON_VASE_MAX_QUANTITY_KEY,
                                                                  (ArrayList<ProductAddonVO>)session.getAttribute(ProductMaintConstants.PRODUCT_ADD_ON_VASE_LIST));
                                    
            productAddonMap.put(ProductMaintConstants.PRODUCT_ADDON_VO_ADDON_KEY,productAddonList);
            productAddonMap.put(ProductMaintConstants.PRODUCT_ADDON_VO_VASE_KEY,productAddonVaseList);
            product.setProductAddonMap(productAddonMap);
      
            product.setColorsList(colors);
            product.setAllColorsList((List)session.getAttribute(ProductMaintConstants.COLORS_KEY ));

            String[] componentsArray = (String[])productDetailForm.get("componentSkuArray");
            List components = new ArrayList();
            if (componentsArray != null) {
                for (int i=0; i<componentsArray.length; i++ ) {
                    ValueVO component = new ValueVO();
                    component.setId(componentsArray[i]);
                    components.add(component);
                }
            }
            product.setComponentSkuList(components);
            product.setAllComponentSkuList((List)session.getAttribute(ProductMaintConstants.COMPONENTS_KEY));

            List vendorProducts = ProductMaintDetailForm.extractVendorProductsListFromMap(request.getParameterMap());
            product.setVendorProductsList(vendorProducts);
            product.setExceptionStartDate((String)productDetailForm.get("exceptionStartDate"));
            product.setExceptionEndDate((String)productDetailForm.get("exceptionEndDate"));

            product.setLastUpdateDate((String)productDetailForm.get("lastUpdateDate"));
            product.setLastUpdateUserId(getUserId(request));
            product.setLastUpdateSystem(ProductMaintConstants.APPLICATION_ID);
            
            // Morning Delivery Flag
            if(productDetailForm.get("morningDeliveryFlag")!=null && (Boolean)productDetailForm.get("morningDeliveryFlag")) {
            	product.setMorningDeliveryFlag(true);
            } else {
            	product.setMorningDeliveryFlag(false);
            }

            if(productDetailForm.get(ProductMaintConstants.COPY_PRODUCT) != null  && 
            		((String)productDetailForm.get(ProductMaintConstants.COPY_PRODUCT)).equals(ProductMaintConstants.COPY_PRODUCT_COPY)) {
            	product.setCopyProduct(true);
            }

            // Get the Novator servers to send to
            List novatorEnvKeys = new ArrayList();
            if (BooleanUtils.toBoolean((Boolean)productDetailForm.get("sentToNovatorProd"))) {
                novatorEnvKeys.add(com.ftd.osp.utilities.feed.NovatorFeedUtil.PRODUCTION);
                product.setSentToNovatorProd(true);
            }
           
            if (BooleanUtils.toBoolean((Boolean)productDetailForm.get("sentToNovatorTest"))) {
                novatorEnvKeys.add(com.ftd.osp.utilities.feed.NovatorFeedUtil.TEST);
                product.setSentToNovatorTest(true);
            }
            if (BooleanUtils.toBoolean((Boolean)productDetailForm.get("sentToNovatorUAT"))) {
                novatorEnvKeys.add(com.ftd.osp.utilities.feed.NovatorFeedUtil.UAT);
                product.setSentToNovatorUAT(true);
            }
            if (BooleanUtils.toBoolean((Boolean)productDetailForm.get("sentToNovatorContent"))) {
                novatorEnvKeys.add(com.ftd.osp.utilities.feed.NovatorFeedUtil.CONTENT);
                product.setSentToNovatorContent(true);
            }

            product.setBatchMode(BooleanUtils.toBoolean((Boolean)productDetailForm.get("batchMode")));
            if (product.isBatchMode()) {
                ProductBatchVO batch = new ProductBatchVO();
                batch.setProductId(product.getProductId());
                batch.setNovatorId(product.getNovatorId());
                batch.setStatus(ProductMaintConstants.PDB_BATCH_STATUS_READY);
                batch.setUserId(product.getLastUpdateUserId());
                batch.setXmlDocument(product.toXMLDocument());
                productBatchSVC.setProduct(batch);
                batch = null;
            } else {
                productSVC.setProduct(product, novatorEnvKeys);
                productBatchSVC.deleteProduct(getUserId(request), product.getProductId());
                //get upsell skus
                List upsellList = 
                    upsellSVC.getUpsellDetailByID(product.getProductId());
                if (upsellList != null && upsellList.size() > 0) {
                    hasUpsells = true;
                    request.setAttribute(ProductMaintConstants.PRODUCT_MAINT_UPSELL_INSTRUCTION_KEY, 
                                         super.getResources(request).getMessage(ProductMaintConstants.PRODUCT_MAINT_UPSELL_ATTACHED_UPDATE_KEY));
                }
            }

        } catch (Exception e) {
            logger.error(e);
            Exception exception = null;
            try {
                Class class1 = e.getClass();
                Class class2 = 
                    Class.forName(ProductMaintConstants.PDBSYSTEMEXCEPTION_CLASS_NAME);
                Class class3 = 
                    Class.forName(ProductMaintConstants.PDBAPPLICATIONEXCEPTION_CLASS_NAME);
                if (!class1.equals(class2) && !class1.equals(class3)) {
                    String[] args = new String[1];
                    args[0] = new String(e.getMessage());
                    logger.error(e);
                    PDBException ftde = 
                        new PDBApplicationException(ProductMaintConstants.GENERAL_APPLICATION_EXCEPTION, 
                                                    args, e);
                    exception = ftde;
                } else {
                    exception = e;
                }
            } catch (ClassNotFoundException cnfe) {
                logger.error(cnfe);
            }
            request.setAttribute(ProductMaintConstants.OE_PRODUCT_ID_KEY, 
                                 productDetailForm.get("productId"));
            request.setAttribute(ProductMaintConstants.PDB_PRODUCT_SEARCH_BY_KEY, 
                                 ProductMaintConstants.PDB_SEARCH_BY_PRODUCT_ID);
            return super.handleException(request, mapping, exception);
        }

        request.removeAttribute(mapping.getAttribute());
        request.setAttribute(ProductMaintConstants.OE_PRODUCT_ID_KEY, 
                             productDetailForm.get("productId"));
        request.setAttribute(ProductMaintConstants.PDB_PRODUCT_SEARCH_BY_KEY, 
                             ProductMaintConstants.PDB_SEARCH_BY_PRODUCT_ID);
       
        String message;
        if (hasUpsells) {
            message = 
                    super.getResources(request).getMessage(ProductMaintConstants.PRODUCT_UPSELL_UPDATED);
        
        } else {
            message = 
                    super.getResources(request).getMessage(ProductMaintConstants.PRODUCT_MAINT_PRODUCT_SAVED_KEY);
        }
        request.setAttribute(ProductMaintConstants.PRODUCT_MAINT_INSTRUCTION_KEY, 
                             message);
        String forwardMapping;
        if (product.isBatchMode()) {
            forwardMapping = "batchsuccess";
            request.setAttribute(ProductMaintConstants.PDB_SUBMIT_TYPE, 
                                 ProductMaintConstants.PDB_EDIT_BATCH);
        } else {
            // Set the back link
            request.setAttribute(ProductMaintConstants.MENU_BACK_KEY, 
                                 ProductMaintConstants.MENU_BACK_PRODUCT_MAINT_LIST_KEY);
            request.setAttribute(ProductMaintConstants.MENU_BACK_TEXT_KEY, 
                                 ProductMaintConstants.MENU_BACK_TEXT_PRODUCT_MAINT_LIST_KEY);
            forwardMapping = "success";
        }
        request.setAttribute(ProductMaintConstants.COPY_PRODUCT_COPY, ProductMaintConstants.COPY_PRODUCT_LOAD);
        // Forward control to the specified success URI
        return (mapping.findForward(forwardMapping));
    }

    public void setProductBatchSVC(IProductBatchSVCBusiness productBatchSVC) {
        this.productBatchSVC = productBatchSVC;
    }

    public IProductBatchSVCBusiness getProductBatchSVC() {
        return productBatchSVC;
    }

    public void setProductSVC(IProductMaintSVCBusiness productSVC) {
        this.productSVC = productSVC;
    }

    public IProductMaintSVCBusiness getProductSVC() {
        return productSVC;
    }

    public void setUpsellSVC(IUpsellSVCBusiness upsellSVC) {
        this.upsellSVC = upsellSVC;
    }

    public IUpsellSVCBusiness getUpsellSVC() {
        return upsellSVC;
    }
    
}
