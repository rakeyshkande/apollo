package com.ftd.pdb.web;

import com.enterprisedt.net.ftp.FTPClient;
import com.enterprisedt.net.ftp.FTPException;
import com.enterprisedt.net.ftp.FTPTransferType;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.pdb.common.ProductMaintConstants;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;


/**
 * Implementation of <strong>Action</strong> that processes a
 * holiday pricing update.
 *
 * @author Ed Mueller
 */
public final class

ImageFTPUpdateAction extends PDBAction {
    public boolean errorInd = false;
    public boolean ftpError = false;
    Exception exception = null;
    String errorMessage = "ImageFTPUpdateAction error report: \n";

    public ImageFTPUpdateAction() {
        super("com.ftd.pdb.web.ImageFTPUpdateAction");
    }

    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param mapping The ActionMapping used to select this instance
     * @param form The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, 
                                 HttpServletRequest request, 
                                 HttpServletResponse response) throws IOException, 
                                                                      ServletException {

        ImageFTPForm imageFTPForm = (ImageFTPForm)form;
        ActionErrors errors = new ActionErrors();
        StringBuffer strProcess = new StringBuffer();
        String forwardStr = "error";

        try {
            byte[] image = null;

            for (int i = 1; i < 7; i++) {
                FormFile source;
                if (i == 1)
                    source = (FormFile)imageFTPForm.get("imagePath1");
                else if (i == 2)
                    source = (FormFile)imageFTPForm.get("imagePath2");
                else if (i == 3)
                    source = (FormFile)imageFTPForm.get("imagePath3");
                else if (i == 4)
                    source = (FormFile)imageFTPForm.get("imagePath4");
                else if (i == 5)
                    source = (FormFile)imageFTPForm.get("imagePath5");
                else
                    source = (FormFile)imageFTPForm.get("imagePath6");

                if (source.getFileName().length() == 0) {
                    continue;
                }

                logger.debug("Processing file name:" + source.getFileName());

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                InputStream stream = source.getInputStream();
                byte[] buffer = new byte[8192];
                int bytesRead = 0;
                int byteCount = 0;
                while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
                    baos.write(buffer, 0, bytesRead);
                    byteCount += bytesRead;
                }

                if (byteCount == 0) {
                    errorMessage = 
                            errorMessage + "Source file name:" + source.getFileName() + 
                            " is empty or could not be found.";
                    stream.close();
                    continue;
                }

                image = baos.toByteArray();
                //close the stream
                stream.close();

                String destinationName = source.getFileName();
                String destinationPath = new String();

                if (destinationName == null || destinationName.length() < 1)
                    break;

                String destAddress = null;
                String loginId = null;
                String password = null;
                errorInd = false;
                ;
                
                try {
                    

                } catch (Exception e) {
                    logger.error(e);
                    throw new ServletException("Failed to retrieve configuration property", 
                                               e);
                }
                
                ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
                
                int numberOfServers = 
                    Integer.parseInt(configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, 
                                                    ProductMaintConstants.PDB_NOVATOR_NUMBER_OF_LIVE_SERVERS));

                
                for (int serverNumber = 1; serverNumber <= numberOfServers; serverNumber++) {
                    if ( (Boolean)imageFTPForm.get("chkLive2") ) {
                        ftpError = false;
                        destAddress = 
                                configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, 
                                               ProductMaintConstants.PDB_NOVATOR_LIVE_FTP_IP_KEY + 
                                               serverNumber);
                        destinationPath = 
                                configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, 
                                               ProductMaintConstants.PDB_NOVATOR_LIVE_PRODUCTS_IMAGE_DIRECTORY + 
                                               serverNumber) + "/" + 
                                destinationName;
                        loginId = ConfigurationUtil.getInstance().getSecureProperty(ProductMaintConstants.SECURE_CONFIG_CONTEXT, ProductMaintConstants.PDB_NOVATOR_LIVE_LOGIN + serverNumber);
                       password = ConfigurationUtil.getInstance().getSecureProperty(ProductMaintConstants.SECURE_CONFIG_CONTEXT, ProductMaintConstants.PDB_NOVATOR_LIVE_PASSWORD + serverNumber);

                        performFTP(destAddress, image, destinationPath, 
                                   loginId, password);
                        if (ftpError) {
                            errorMessage = 
                                    errorMessage + " Destination address:" + 
                                    destAddress + ". Destination path:" + 
                                    destinationPath + 
                                    ". Type: LIVE_IMAGE_UPLOAD_EXCEPTION \n";
                            errors.add(ProductMaintConstants.APPLICATION_CATEGORY_CONSTANT_2, 
                                       new ActionMessage(String.valueOf(ProductMaintConstants.LIVE_IMAGE_UPLOAD_EXCEPTION)));
                        }
                    }
                }

                numberOfServers = 
                        Integer.parseInt(configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, 
                                                        ProductMaintConstants.PDB_NOVATOR_NUMBER_OF_TEST_SERVERS));
                for (int serverNumber = 1; serverNumber <= numberOfServers; 
                     serverNumber++) {

                    if ( (Boolean)imageFTPForm.get("chkTest2")) {
                        ftpError = false;
                        destAddress = 
                                configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, 
                                               ProductMaintConstants.PDB_NOVATOR_TEST_FTP_IP_KEY + 
                                               serverNumber);
                        destinationPath = 
                                configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, 
                                               ProductMaintConstants.PDB_NOVATOR_TEST_PRODUCTS_IMAGE_DIRECTORY + 
                                               serverNumber) + "/" + 
                                destinationName;
                        loginId = ConfigurationUtil.getInstance().getSecureProperty(ProductMaintConstants.SECURE_CONFIG_CONTEXT, ProductMaintConstants.PDB_NOVATOR_TEST_LOGIN + serverNumber);
                        password = ConfigurationUtil.getInstance().getSecureProperty(ProductMaintConstants.SECURE_CONFIG_CONTEXT, ProductMaintConstants.PDB_NOVATOR_TEST_PASSWORD + serverNumber);
                        performFTP(destAddress, image, destinationPath, 
                                   loginId, password);
                        if (ftpError) {
                            errorMessage = 
                                    errorMessage + " Destination address:" + 
                                    destAddress + ". Destination path:" + 
                                    destinationPath + 
                                    ". Type: TEST_IMAGE_UPLOAD_EXCEPTION \n";
                            errors.add(ProductMaintConstants.APPLICATION_CATEGORY_CONSTANT_2, 
                                       new ActionMessage(String.valueOf(ProductMaintConstants.TEST_IMAGE_UPLOAD_EXCEPTION)));
                        }
                    }
                }

                numberOfServers = 
                        Integer.parseInt(configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, 
                                                        ProductMaintConstants.PDB_NOVATOR_NUMBER_OF_UAT_SERVERS));
                for (int serverNumber = 1; serverNumber <= numberOfServers; 
                     serverNumber++) {

                    if ( (Boolean)imageFTPForm.get("chkUAT2")) {
                        ftpError = false;
                        destAddress = 
                                configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, 
                                               ProductMaintConstants.PDB_NOVATOR_UAT_FTP_IP_KEY + 
                                               serverNumber);
                        destinationPath = 
                                configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, 
                                               ProductMaintConstants.PDB_NOVATOR_UAT_PRODUCTS_IMAGE_DIRECTORY + 
                                               serverNumber) + "/" + 
                                destinationName;
                        loginId = ConfigurationUtil.getInstance().getSecureProperty(ProductMaintConstants.SECURE_CONFIG_CONTEXT, ProductMaintConstants.PDB_NOVATOR_UAT_LOGIN + serverNumber);
                        password = ConfigurationUtil.getInstance().getSecureProperty(ProductMaintConstants.SECURE_CONFIG_CONTEXT, ProductMaintConstants.PDB_NOVATOR_UAT_PASSWORD + serverNumber);
                        performFTP(destAddress, image, destinationPath, 
                                   loginId, password);
                        if (ftpError) {
                            errorMessage = 
                                    errorMessage + " Destination address:" + 
                                    destAddress + ". Destination path:" + 
                                    destinationPath + 
                                    ". Type: UAT_IMAGE_UPLOAD_EXCEPTION \n";
                            errors.add(ProductMaintConstants.APPLICATION_CATEGORY_CONSTANT_2, 
                                       new ActionMessage(String.valueOf(ProductMaintConstants.UAT_IMAGE_UPLOAD_EXCEPTION)));
                        }
                    }
                }

                numberOfServers = 
                        Integer.parseInt(configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, 
                                                        ProductMaintConstants.PDB_NOVATOR_NUMBER_OF_CONTENT_SERVERS));
                for (int serverNumber = 1; serverNumber <= numberOfServers; 
                     serverNumber++) {

                    if ( (Boolean)imageFTPForm.get("chkContent2")) {
                        ftpError = false;
                        destAddress = 
                                configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, 
                                               ProductMaintConstants.PDB_NOVATOR_CONTENT_FTP_IP_KEY + 
                                               serverNumber);
                        destinationPath = 
                                configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, 
                                               ProductMaintConstants.PDB_NOVATOR_CONTENT_PRODUCTS_IMAGE_DIRECTORY + 
                                               serverNumber) + "/" + 
                                destinationName;
 
                        loginId = ConfigurationUtil.getInstance().getSecureProperty(ProductMaintConstants.SECURE_CONFIG_CONTEXT, ProductMaintConstants.PDB_NOVATOR_CONTENT_LOGIN + serverNumber);
                        password = ConfigurationUtil.getInstance().getSecureProperty(ProductMaintConstants.SECURE_CONFIG_CONTEXT, ProductMaintConstants.PDB_NOVATOR_CONTENT_PASSWORD + serverNumber);
                        performFTP(destAddress, image, destinationPath, 
                                   loginId, password);
                        if (ftpError) {
                            errorMessage = 
                                    errorMessage + " Destination address:" + 
                                    destAddress + ". Destination path:" + 
                                    destinationPath + 
                                    ". Type: CONTENT_IMAGE_UPLOAD_EXCEPTION \n";
                            errors.add(ProductMaintConstants.APPLICATION_CATEGORY_CONSTANT_2, 
                                       new ActionMessage(String.valueOf(ProductMaintConstants.CONTENT_IMAGE_UPLOAD_EXCEPTION)));
                        }
                    }
                }

                numberOfServers = 
                        Integer.parseInt(configUtil.getFrpGlobalParmNoNull(ProductMaintConstants.PDB_CONFIG_CONTEXT, 
                                                        ProductMaintConstants.PDB_FTD_NUMBER_OF_SERVERS));
                for (int serverNumber = 1; serverNumber <= numberOfServers; 
                     serverNumber++) {

                    if ( (Boolean)imageFTPForm.get("chkFTD2")) {
                        ftpError = false;
                        destAddress = 
                                configUtil.getFrpGlobalParmNoNull(ProductMaintConstants.PDB_CONFIG_CONTEXT, 
                                               ProductMaintConstants.PDB_FTD_FTP_IP_KEY + 
                                               serverNumber);
                        destinationPath = 
                                configUtil.getFrpGlobalParmNoNull(ProductMaintConstants.PDB_CONFIG_CONTEXT, 
                                               ProductMaintConstants.PDB_FTD_PRODUCTS_IMAGE_DIRECTORY + 
                                               serverNumber) + "/" + 
                                destinationName;
                        loginId = ConfigurationUtil.getInstance().getSecureProperty(ProductMaintConstants.SECURE_CONFIG_CONTEXT, ProductMaintConstants.PDB_FTD_LOGIN + serverNumber);
                        password = ConfigurationUtil.getInstance().getSecureProperty(ProductMaintConstants.SECURE_CONFIG_CONTEXT, ProductMaintConstants.PDB_FTD_PASSWORD + serverNumber);
                        performFTP(destAddress, image, destinationPath, 
                                   loginId, password);
                        if (ftpError) {
                            errorMessage = 
                                    errorMessage + " Destination address:" + 
                                    destAddress + ". Destination path:" + 
                                    destinationPath + 
                                    ". Type: FTD_IMAGE_UPLOAD_EXCEPTION \n";
                            errors.add(ProductMaintConstants.APPLICATION_CATEGORY_CONSTANT_2, 
                                       new ActionMessage(String.valueOf(ProductMaintConstants.FTD_IMAGE_UPLOAD_EXCEPTION)));
                        }
                    }
                }

                strProcess.append("Processed file ");
                strProcess.append(source.getFileName());
                strProcess.append(".  ");
            }

        } catch (Exception e) {
            errorInd = true;
        } finally {
            // Forward control to the specified success URI
            if (errorInd) {
                sendMail(errorMessage);
                request.setAttribute(mapping.getAttribute(), imageFTPForm);
                request.setAttribute(Globals.ERROR_KEY, errors);
                forwardStr = "error";

            } else {
                request.setAttribute(ProductMaintConstants.PRODUCT_MAINT_INSTRUCTION_KEY, 
                                     strProcess.toString());
                request.setAttribute(mapping.getAttribute(), imageFTPForm);
                forwardStr = "success";
            }
        }
        return (mapping.findForward(forwardStr));
    }

    /**
     * This Method will FTP a file to the specified directory
     * @param destination The IP if the destination server
     * @param imageSource The Path and file name of source image
     * @param destinationPath The Path and Image name of the destination image
     * @param loginId The login Id of the destination server
     * @param password The password
     */
    public void performFTP(String destination, byte[] imageSource, 
                           String destinationPath, String loginId, 
                           String password) {
        FTPClient ftpClient = null;
        try {
            ftpClient = new FTPClient(destination);
            ftpClient.login(loginId, password);
            ftpClient.setType(FTPTransferType.BINARY);
            ftpClient.put(imageSource, destinationPath);

        } catch (FTPException FTPe) {
            logger.error(FTPe.toString());
            errorInd = true;
            ftpError = true;
        } catch (Exception e) {
            logger.error(e.toString());
            errorInd = true;
            ftpError = true;
        } finally {
            if (ftpClient != null) {
                try {
                    ftpClient.quit();
                } catch (Exception e) {
                     logger.warn(e.toString());
                }
            }
        }
    }

    private void sendMail(String messageText) {
        try {
            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
            String mailServer = 
                configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, 
                               ProductMaintConstants.PDB_APP_CONFIG_MAILSERVER_KEY);
            String mailLogin = ConfigurationUtil.getInstance().getSecureProperty(ProductMaintConstants.SECURE_CONFIG_CONTEXT, ProductMaintConstants.PDB_APP_CONFIG_MAILLOGIN_KEY);
            String mailPassowrd = ConfigurationUtil.getInstance().getSecureProperty(ProductMaintConstants.SECURE_CONFIG_CONTEXT, ProductMaintConstants.PDB_APP_CONFIG_MAILPASSWORD_KEY);
            int mailSendToCNT = 
                Integer.parseInt(configUtil.getFrpGlobalParmNoNull(ProductMaintConstants.PDB_CONFIG_CONTEXT, 
                                                ProductMaintConstants.PDB_APP_CONFIG_MAILSENDTOCNT_KEY));


            Properties props = new Properties();
            props.setProperty("mail.transport.protocol", "smtp");

            props.setProperty("mail.host", mailServer);
            props.setProperty("mail.user", mailLogin);
            props.setProperty("mail.password", mailPassowrd);

            Session mailSession = Session.getDefaultInstance(props, null);
            Transport transport = mailSession.getTransport();

            MimeMessage message = new MimeMessage(mailSession);
            message.setContent(messageText, "text/plain");

            for (int i = 1; i <= mailSendToCNT; i++) {
                String mailSendTo = 
                    configUtil.getFrpGlobalParmNoNull(ProductMaintConstants.PDB_CONFIG_CONTEXT, 
                                   ProductMaintConstants.PDB_APP_CONFIG_MAILSENDTO_KEY + 
                                   i);
                message.addRecipient(Message.RecipientType.TO, 
                                     new InternetAddress(mailSendTo));
            }

            transport.connect();
            transport.sendMessage(message, 
                                  message.getRecipients(Message.RecipientType.TO));
            transport.close();
        } catch (Exception e) {
            logger.error(e.toString());
        }

    }

}
