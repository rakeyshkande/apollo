package com.ftd.pdb.web;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBSystemException;
import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import org.springframework.web.struts.ActionSupport;


public class PDBAction extends ActionSupport {
    protected Logger logger = null;
    
    public PDBAction(String className) {
        super();
        logger = new Logger(className);
    }

    /**
     * Handles exceptions that come from the application. 
     * Retrieves the error code from the exception, creates an Action Error
     * object, and adds it to the request. It then returns the appropriate action
     * forward. 
     * It also set the bundle that can be printed on a default error page. JSPs that
     * handle ActionMessage objects can get the bundle by calling request.getAttribute(MESSAGE_BUNDLE)
     *
     * @param request the request object
     * @param mapping the action mapping
     * @param exception the exception
//       * @param bundle the bundle used for displaying error messages If bundle is null, the default bundle will be used.
     * @return ActionForward determines what the next page will be
    **/
//    protected ActionForward handleException(HttpServletRequest request, 
//                                              ActionMapping mapping, 
//                                              Exception exception, 
//                                              String bundle)
    protected ActionForward handleException(HttpServletRequest request, 
                                           ActionMapping mapping, 
                                           Throwable exception)
    {
        logger.debug("PDBAction.handleException");
        logger.error(exception);
        ActionMessages errors = new ActionMessages();
          
        try {
            //Get the configuration object
            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
            
            //unwrap the exception
            exception = unwrapException(exception);
            //put the exception into the request object
            if (exception != null){
            request.setAttribute(configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, "framework.exception.object.name")
                                 , exception);
            }
    
              //handle the exception
              if (exception == null) {
                logger.debug("Exception is not defined in handleException!");
                errors.add(configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, "framework.system.category.name"),
                            new ActionMessage("error.000", "The exception in handleException() was null!"));
                saveErrors(request, errors); // struts 1.1 problem
                return mapping.findForward(configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, "framework.system.category.name"));
              } else if (exception instanceof PDBApplicationException) {
                  PDBApplicationException appEx = (PDBApplicationException) exception;
                  //handle the exception
                  return handleApplicationException(request, mapping, errors, appEx);
              } else if (exception instanceof PDBSystemException) {
                  PDBSystemException sysEx = (PDBSystemException) exception;
                  //handle the exception
                  return handleSystemException(request, mapping, errors, sysEx);
              } else {
                  //handle the exception
                  return handleUnexpectedException(request, mapping, errors, exception);
              }
            } catch (Exception e) {
            logger.error(e);
            return handleUnexpectedException(request, mapping, errors, exception);
        }
    }

      /**
       * Handles all application exceptions that come from the application. 
       *
       * @param request the request object
       * @param mapping the action mapping
       * @param errors the action errors object
       * @param appEx the application exception
       * @return ActionForward determines what the next page will be
       **/
    protected ActionForward handleApplicationException(HttpServletRequest request,
                                                          ActionMapping mapping, 
                                                          ActionMessages errors, 
                                                          PDBApplicationException appEx)
    {
        logger.debug("PDBAction.handleApplicationException");
        logger.error(appEx);
        //make sure passed variables are all valid
        if (request == null || mapping == null || errors == null || appEx == null){
            IllegalArgumentException e = new IllegalArgumentException("Invalid passed arguments in handleApplicationException(): request, mapping, errors, or appEx is null");
            logger.error(e);
            throw e;
        }
          
        ConfigurationUtil configUtil = null;
        try {
            //Get the configuration object
            configUtil = ConfigurationUtil.getInstance();
        } catch (Exception e) {
            logger.error("Error while getting configuration instance",e);
            throw new PDBRuntimeException("Error while getting configuration instance",e);
        }
        
        //get exception's arguments
        String[] argList = appEx.getArgList();

        try {
          //create the ActionMessage with the errorCode (used for the error message.)
          //call the ActionMessage constructor that corresponds to the number of exception args
          if (argList == null || argList.length == 0){
            errors.add(configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, "framework.application.category.name"), new ActionMessage(String.valueOf(appEx.getErrorCode())));
          } else if (argList.length == 1) {
            errors.add(configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, "framework.application.category.name"), new ActionMessage(String.valueOf(appEx.getErrorCode()), argList[0]));
          } else if (argList.length == 2) {
            errors.add(configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, "framework.application.category.name"), new ActionMessage(String.valueOf(appEx.getErrorCode()), argList[0], argList[1]));
          } else if (argList.length == 3) {
            errors.add(configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, "framework.application.category.name"), new ActionMessage(String.valueOf(appEx.getErrorCode()), argList[0], argList[1], argList[2]));
          } else{
            errors.add(configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, "framework.application.category.name"), new ActionMessage(String.valueOf(appEx.getErrorCode()), argList[0], argList[1], argList[2], argList[3]));
          }
        } catch (Exception e) {
            logger.error("Error while getting configuration property",e);
            throw new PDBRuntimeException("Error while getting configuration property",e);
        }
        
        String forwardAction = null;
        try {
            forwardAction = configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, "framework.application.error.name");
        } catch (Exception e) {
            logger.error("Error while retrieving forward action from property file",e);
            throw new PDBRuntimeException("Error while retrieving forward action from property file",e);
        }

        //put the errors into the request
        saveErrors(request, errors); // struts 1.1 problem
        
        //go to the application error page.
        return mapping.findForward(forwardAction);                                                        
      }                                                      


      /**
       * Handles all system exceptions that come from the application. 
       *
       * @param request the request object
       * @param mapping the action mapping
       * @param errors the action errors object
       * @param sysEx the system exception
       * @return ActionForward determines what the next page will be
       **/
      protected ActionForward handleSystemException(HttpServletRequest request, 
                                                    ActionMapping mapping, 
                                                    ActionMessages errors, 
                                                    PDBSystemException sysEx) {
        
         logger.debug("PDBAction.handleSystemException");
         logger.error(sysEx);
        //make sure passed vars are all valid
        if (request == null || mapping == null || errors == null || sysEx == null){
          IllegalArgumentException e = new IllegalArgumentException("Invalid passed arguments: request, mapping, errors, or appEx is null");
          logger.error(e);
          throw e;
        }
          
        ConfigurationUtil configUtil = null;
        try {
            //Get the configuration object
            configUtil = ConfigurationUtil.getInstance();
        } catch (Exception e) {
            logger.error("Error while getting configuration instance",e);
            throw new PDBRuntimeException("Error while getting configuration instance",e);
        }
        
        try {
        //create the ActionMessage with the errorCode (used for the error message.)
            errors.add(configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, "framework.system.category.name"), new ActionMessage( String.valueOf( sysEx.getErrorCode() )) );
        } catch (Exception e) {
         logger.error("Error while getting configuration property",e);
            throw new PDBRuntimeException("Error while getting configuration property",e);
        }
        

        //put the errors into the request
        saveErrors(request, errors); // struts 1.1 problem
        
        String forwardAction = null;
        try {
            forwardAction = configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, "framework.system.error.name");
        } catch (Exception e) {
            logger.error("Error while retrieving forward action from property file",e);
            throw new PDBRuntimeException("Error while retrieving forward action from property file",e);
        }

        //go to the application error page.
        return mapping.findForward(forwardAction);
      }

      /**
       * Handles all system exceptions that come from the application. 
       *
       * @param request the request object
       * @param mapping the action mapping
       * @param errors the action errors object
       * @param exception the exception
       * @return ActionForward determines what the next page will be
       **/
    protected ActionForward handleUnexpectedException(HttpServletRequest request, 
                                                            ActionMapping mapping, 
                                                            ActionMessages errors, 
                                                            Throwable exception) 
    {
        logger.debug("PDBAction.handleUnexpectedException");
        logger.error(exception);
        //make sure passed vars are all valid
        if (request == null || mapping == null || errors == null) {
          IllegalArgumentException e = new IllegalArgumentException("Invalid passed arguments: request, mapping, or errors is null");
          logger.error(e);
          throw e;
        }
          
        ConfigurationUtil configUtil = null;
        try {
            //Get the configuration object
            configUtil = ConfigurationUtil.getInstance();
        } catch (Exception e) {
            logger.error("Error while getting configuration instance",e);
            throw new PDBRuntimeException("Error while getting configuration instance",e);
        }
        
        try {
            //create the ActionMessage with the errorCode (used for the error message.)
            errors.add(configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, "framework.system.category.name"), new ActionMessage("error.000", exception.getClass().toString(), exception.getMessage()));
        } catch (Exception e) {
         logger.error("Error while getting configuration property",e);
            throw new PDBRuntimeException("Error while getting configuration property",e);
        }
        
        //put the errors into the request
        saveErrors(request, errors); // struts 1.1 problem
        
        String forwardAction = null;
        try {
            forwardAction = configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, "framework.system.error.name");
        } catch (Exception e) {
            logger.error("Error while retrieving forward action from property file",e);
            throw new PDBRuntimeException("Error while retrieving forward action from property file",e);
        }

          //go to the system error page.
          return mapping.findForward(forwardAction);
      }

      /**
       * Unwraps the exception.  This method attempts to get the nested exception
       * out of wrapper exceptions.
       *
       * @param exception the exception object
       * @return the unwrapped exceptions
       */
    protected Throwable unwrapException(Throwable exception)
    {

        Throwable originalEx = exception;

        if (exception instanceof java.rmi.ServerException){
          logger.debug("Got a ServerException");
          java.rmi.ServerException serverEx = (java.rmi.ServerException) exception;

          if (serverEx.detail == null){
            logger.debug("Nested exception is null.  Returning original exception");
            return originalEx;
          } else if (serverEx.detail instanceof java.rmi.RemoteException){
            logger.debug("Got another RemoteException");
            java.rmi.RemoteException remoteEx = (java.rmi.RemoteException) serverEx.detail;

            if (remoteEx.detail == null) {
              logger.debug("RemoteException detail was null.  Returning originalException");
              return originalEx;
            } else {
              if (remoteEx.detail instanceof Throwable) {
                exception = remoteEx.detail;
              }
            }
          }
        }

        return exception;
    }

    /**
     * Gets the userId based on the sessionId found in the request object.
     * @param request
     */
    public String getUserId(HttpServletRequest request) {

        String userId = null;
        String defaultUserId = "nouserfound";
        String sessionId = request.getParameter(ProductMaintConstants.SEC_TOKEN);
        if( sessionId!=null ) {
            try {
                SecurityManager sm = SecurityManager.getInstance();
                UserInfo userInfo = sm.getUserInfo(sessionId);
                userId = userInfo.getUserID();
            } catch (Exception e) {
                logger.debug(e);
                logger.warn("Unable to determine user id");
            }
        }
        // if still no user, because of error or problem
        if( userId==null || userId.equalsIgnoreCase("")) {
            logger.debug("Setting userId to default....");
            userId = defaultUserId;
        }
        return userId;
    }

}
