package com.ftd.pdb.web;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.utilities.FTDUtil;
import com.ftd.pdb.common.valobjs.ColorVO;
import com.ftd.pdb.common.valobjs.DeliveryOptionVO;
import com.ftd.pdb.common.valobjs.ProductAddonVO;
import com.ftd.pdb.common.valobjs.ProductSubCodeVO;
import com.ftd.pdb.common.valobjs.ProductVO;
import com.ftd.pdb.common.valobjs.StateDeliveryExclusionVO;
import com.ftd.pdb.common.valobjs.ValueVO;
import com.ftd.pdb.common.valobjs.VendorProductVO;
import com.ftd.pdb.common.valobjs.VendorVO;
import com.ftd.pdb.common.valobjs.comparator.ColorVOComparator;
import com.ftd.pdb.service.IProductMaintSVCBusiness;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.action.ActionServlet;


public class ProductMaintDetailForm extends PDBActionForm {
    /*Spring managed resources */
    private IProductMaintSVCBusiness productSVC;
    /*end Spring managed resources */
    
    public ProductMaintDetailForm() {
        super("com.ftd.pdb.web.ProductMaintDetailForm");
    }
    
    public void setServlet(ActionServlet actionServlet) {
        super.setServlet(actionServlet);
        productSVC = (IProductMaintSVCBusiness)getWebApplicationContext().getBean("productSVC");
    }
    
    public void reset(ActionMapping mapping, javax.servlet.http.HttpServletRequest request) {
        try {
            if (!request.getServletPath().equalsIgnoreCase("/showProductMaintDetail.do")){
                List vendorProducts = this.extractVendorProductsListFromMap(request.getParameterMap());
                request.setAttribute(ProductMaintConstants.VENDOR_PRODUCTS, vendorProducts);
                List subcodeList = this.extractSubcodeListFromMap(request.getParameterMap());
                request.setAttribute(ProductMaintConstants.SUB_CODE_LIST_KEY, subcodeList);
                List shippingMethods = this.extractShippingMethodsListFromMap(request);
                request.setAttribute(ProductMaintConstants.SHIPPING_METHODS_KEY, shippingMethods);
                Map colorsMap = this.extractColorListsFromMap(request);
                request.setAttribute(ProductMaintConstants.INCLUDED_COLORS, colorsMap.get(ProductMaintConstants.INCLUDED_COLORS));
                request.setAttribute(ProductMaintConstants.EXCLUDED_COLORS, colorsMap.get(ProductMaintConstants.EXCLUDED_COLORS));
                request.setAttribute(ProductMaintConstants.EXCLUDED_COLORS_STATIC, colorsMap.get(ProductMaintConstants.EXCLUDED_COLORS));
                request.setAttribute(ProductMaintConstants.INCLUDED_COLORS_STATIC, colorsMap.get(ProductMaintConstants.INCLUDED_COLORS));
                List excludedStates = this.extractExcludedStatesFromMap(request.getParameterMap());
                request.setAttribute(ProductMaintConstants.EXCLUDED_STATES, excludedStates);
                Map componentsMap = this.extractComponentListsFromMap(request);
                request.setAttribute(ProductMaintConstants.INCLUDED_COMPONENT_SKUS, componentsMap.get(ProductMaintConstants.INCLUDED_COMPONENT_SKUS));
                request.setAttribute(ProductMaintConstants.AVAILABLE_COMPONENT_SKUS, componentsMap.get(ProductMaintConstants.AVAILABLE_COMPONENT_SKUS));
            }
        } catch(Exception ex){
            ex.printStackTrace();
            logger.error(ex);
        }
    }
    public ActionErrors validate(ActionMapping actionMapping, 
                                 HttpServletRequest request){
    	
        ActionErrors errors = super.validate(actionMapping, request); // struts 1.1
        HttpSession session = request.getSession();
        boolean bErrorsFound = errors.size()>0;
        // make sure Status Window is closed when we come back
        request.setAttribute(ProductMaintConstants.CLOSE_POPUP,"Y");  
        set(ProductMaintConstants.CLOSE_POPUP,"Y");
        if (errors.size() == 0){
        try
        {
        boolean hasSubcodes = false;
        boolean typeError = false;
        boolean deliveryTypeError = false;
        boolean subcodeDuplicateError = false;
        boolean subcodePricesEqualError = false;
        boolean canBeFloral = false;
        boolean canBeVendor = false;

        //Get subcode arrays
        String[] subCodeIdArray = request.getParameterValues(ProductMaintConstants.PDB_PRODUCT_SUB_CODE_ID_KEY);
        String[] subID = request.getParameterValues(ProductMaintConstants.PDB_PRODUCT_SUB_CODE_ID_KEY);
        String[] subDescription = request.getParameterValues(ProductMaintConstants.PDB_PRODUCT_SUB_CODE_DESC_KEY);
        String[] subRef = request.getParameterValues(ProductMaintConstants.PDB_PRODUCT_SUB_CODE_REF_KEY);
        String[] subPrice = request.getParameterValues(ProductMaintConstants.PDB_PRODUCT_SUB_CODE_PRICE_KEY);
        String[] subDimWeight = request.getParameterValues(ProductMaintConstants.PDB_PRODUCT_SUB_CODE_DIM_WEIGHT_KEY);
        
        if (subCodeIdArray == null){
            hasSubcodes = false;
        } else {
            hasSubcodes = true;
        }
        if(get(ProductMaintConstants.COPY_PRODUCT) != null && 
        		get(ProductMaintConstants.COPY_PRODUCT).equals(ProductMaintConstants.COPY_PRODUCT_COPY)&& 
        		get("productId") != null ){
        	if(productSVC.validateProductId(StringUtils.trimToEmpty((String)get("productId")))){
        		bErrorsFound = true;
        		errors.add("productId", new ActionMessage("Product Id already exists.", false));
        		
        	}
        }
        boolean availableFlag = BooleanUtils.isTrue((Boolean)get("status"));
        if( hasSubcodes &&  availableFlag)
        {
            bErrorsFound = true;
            errors.add("subcode",new ActionMessage(ProductMaintConstants.PDB_PRODUCT_MAINT_ERROR_STRING_RETIRED_SUBCODES, false) );
        }

        // Check for company
        String[] list = (String[]) this.get("companyList");
        if(list == null || list.length == 0)
        {
            bErrorsFound = true;
            errors.add("companyList",new ActionMessage(ProductMaintConstants.PDB_PRODUCT_MAINT_ERROR_STRING_COMPANY, false) );
        }
        
        // Check for product type
        String productType = StringUtils.trimToEmpty((String)get("productType"));
        if (productType.equalsIgnoreCase(ProductMaintConstants.PRODUCT_TYPE_FLORAL))
        {
            canBeFloral=true;
            canBeVendor=false;
        }
        else if (productType.equalsIgnoreCase(ProductMaintConstants.NONE) || productType.equalsIgnoreCase(ProductMaintConstants.PRODUCT_TYPE_SERVICES))
        {
            canBeFloral=false;
            canBeVendor=false;
        }
        else if (productType.equalsIgnoreCase(ProductMaintConstants.PRODUCT_TYPE_FRESHCUTS) || productType.equalsIgnoreCase(ProductMaintConstants.PRODUCT_TYPE_SPECIALTY))
        {
            canBeFloral=false;
            canBeVendor=true;
        }
        else if (productType.equalsIgnoreCase(ProductMaintConstants.PRODUCT_TYPE_SAME_DAY_FRESH_CUTS_CODE) || productType.equalsIgnoreCase(ProductMaintConstants.PRODUCT_TYPE_SAME_DAY_GIFT_CODE))
        {
            canBeFloral=true;
            canBeVendor=true;
        }
        logger.debug("productType: " + productType + " canBeFloral: " + canBeFloral + " canBeVendor: " + canBeVendor);

        // Check for category
        // For same day gift the category must be present
        String strCheck = StringUtils.trimToEmpty((String)get("category"));
        if(StringUtils.isBlank(strCheck) ||
         ( StringUtils.equals(strCheck,ProductMaintConstants.NONE) 
         && (canBeFloral && canBeVendor)))
        {
            bErrorsFound = true;
            errors.add("category",new ActionMessage(ProductMaintConstants.PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_CATAGORY, false) );
        }
        // Must have a value for Services
        if (StringUtils.equals(productType, ProductMaintConstants.PRODUCT_TYPE_SERVICES) &&
          (StringUtils.isBlank(strCheck) || StringUtils.equals(strCheck,ProductMaintConstants.NONE))) {
            bErrorsFound = true;
            errors.add("category",new ActionMessage(ProductMaintConstants.PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_CATAGORY, false) );
        }
        
        // Department code is required for Services
        String deptCode = StringUtils.trimToEmpty((String)get("departmentCode"));
        logger.debug("departmentCode: " + deptCode);
        if (StringUtils.equals(productType, ProductMaintConstants.PRODUCT_TYPE_SERVICES) &&
          (StringUtils.isBlank(deptCode) || StringUtils.equals(deptCode,ProductMaintConstants.NONE))) {
            bErrorsFound = true;
            errors.add("departmentCode",new ActionMessage(ProductMaintConstants.PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_DEPARTMENT_CODE, false) );
        }

        // Check for  subtype if  Specialty Gift
        strCheck = StringUtils.trimToEmpty((String)get("productSubType"));
        if((productType.equalsIgnoreCase(ProductMaintConstants.PRODUCT_TYPE_SPECIALTY) || productType.equalsIgnoreCase(ProductMaintConstants.PRODUCT_TYPE_SERVICES)) &&
            (StringUtils.isBlank(strCheck) || StringUtils.equals(strCheck,ProductMaintConstants.NONE)))
        {
            errors.add("productSubType",new ActionMessage(ProductMaintConstants.PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_SUB_TYPE, false) );
        }
        
        // Check for  delivery type
        String deliveryType = StringUtils.trimToEmpty((String)get("deliveryType"));
        if(StringUtils.isBlank(deliveryType) || StringUtils.equals(deliveryType,ProductMaintConstants.NONE))
        {
            bErrorsFound = true;
            errors.add("deliveryType",new ActionMessage(ProductMaintConstants.PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_DELIVERY_TYPE, false) );
            deliveryTypeError = true;
        }
        
        if (canBeVendor && deliveryType.equalsIgnoreCase(ProductMaintConstants.PRODUCT_DELIVERY_TYPE_INTL) ){
            errors.add("deliveryType",new ActionMessage("Delivery type cannot be international for a vendor product.", false) );
        }
        
        // Check for  florist reference number if floral, intl, specialy without subcodes, or frest cut without subcodes
        strCheck = StringUtils.trimToEmpty((String)get("floristReferenceNumber"));
        if(  (!typeError && !deliveryTypeError) &&
          ( (productType.equalsIgnoreCase(ProductMaintConstants.PRODUCT_TYPE_FLORAL) ||
            deliveryType.equalsIgnoreCase(ProductMaintConstants.PRODUCT_DELIVERY_TYPE_INTL)) ||
            (productType.equalsIgnoreCase(ProductMaintConstants.PRODUCT_TYPE_SPECIALTY) && !hasSubcodes) ||
            (productType.equalsIgnoreCase(ProductMaintConstants.PRODUCT_TYPE_FRESHCUTS) && !hasSubcodes)) &&
            StringUtils.isBlank(strCheck))
        {
            bErrorsFound = true;
            errors.add("floristReferenceNumber",new ActionMessage(ProductMaintConstants.PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_FLORIST_REFERENCE, false) );
        }

        
        // Check for  mercury description if floral, intl, specialy without subcodes, or frest cut without subcodes
        strCheck = StringUtils.trimToEmpty((String)get("mercuryDescription"));
        if(  (!typeError && !deliveryTypeError) &&
            ( (productType.equalsIgnoreCase(ProductMaintConstants.PRODUCT_TYPE_FLORAL) &&
            deliveryType.equalsIgnoreCase(ProductMaintConstants.PRODUCT_DELIVERY_TYPE_INTL)) ||
            (productType.equalsIgnoreCase(ProductMaintConstants.PRODUCT_TYPE_SPECIALTY) && !hasSubcodes) ||
            (productType.equalsIgnoreCase(ProductMaintConstants.PRODUCT_TYPE_FRESHCUTS) && !hasSubcodes)) &&
            StringUtils.isBlank(strCheck))
        {
            bErrorsFound = true;
            errors.add("mercuryDescription",new ActionMessage(ProductMaintConstants.PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_MERCURY_DESCRIPTION, false) );
        }
        // Check for second choice
        if( !typeError )
        {
            strCheck = StringUtils.trimToEmpty((String)get("secondChoice"));
            if((canBeFloral && canBeVendor) && StringUtils.isBlank(strCheck))
            {
                this.set("secondChoice","0");
            }
            else if(canBeFloral && StringUtils.isBlank(strCheck))
            {
                bErrorsFound = true;
                errors.add("secondChoice",new ActionMessage(ProductMaintConstants.PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_SECOND_CHOICE, false) );
            }
        }

        // Check for  arrangement colors included, if  floral or international and the color flag is checked
        boolean colorSizeFlag = BooleanUtils.isTrue((Boolean)get("colorSizeFlag"));
        
        // Colors being re-purposed for Project Fresh - commented out March 2018
        /*
        String[] arrangementColorsArray = (String[])get("arrangementColorsArray");
        if(  (!typeError && !deliveryTypeError) &&
           ( canBeFloral || deliveryType.equalsIgnoreCase(ProductMaintConstants.PRODUCT_DELIVERY_TYPE_INTL) )  
             && BooleanUtils.isTrue(colorSizeFlag) 
             && arrangementColorsArray == null)
        {
            bErrorsFound = true;
            errors.add("excludedColorsArray",new ActionMessage(ProductMaintConstants.PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_ARRNG_COLOR_CHOICE, false) );
        }
        // Check for  arrangement colors included, if  floral or international and the color flag is checked
        if(  (!typeError && !deliveryTypeError) &&
           ( productType.equalsIgnoreCase(ProductMaintConstants.PRODUCT_TYPE_FLORAL) ||
            deliveryType.equalsIgnoreCase(ProductMaintConstants.PRODUCT_DELIVERY_TYPE_INTL) )  &&
            arrangementColorsArray.length != 0 &&
            !colorSizeFlag )
        {
            bErrorsFound = true;
            errors.add("colorSizeFlag",new ActionMessage(ProductMaintConstants.PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_USE_COLOR, false) );
        }
        */
        
        // Check recipe fields
        String standardRecipe = StringUtils.trimToEmpty((String)get("recipe"));
        String deluxeRecipe = StringUtils.trimToEmpty((String)get("deluxeRecipe"));
        String premiumRecipe = StringUtils.trimToEmpty((String)get("premiumRecipe"));
        boolean sendStandardRecipe = BooleanUtils.isTrue((Boolean)get("sendStandardRecipe"));
        boolean sendDeluxeRecipe = BooleanUtils.isTrue((Boolean)get("sendDeluxeRecipe"));
        boolean sendPremiumRecipe = BooleanUtils.isTrue((Boolean)get("sendPremiumRecipe"));
        if (sendStandardRecipe && (standardRecipe == null || standardRecipe.equals(""))) {
            bErrorsFound = true;
            errors.add("recipe",new ActionMessage(ProductMaintConstants.PDB_PRODUCT_MAINT_ERROR_STRING_SEND_STANDARD_RECIPE, false) );
        }
        if (sendDeluxeRecipe && (deluxeRecipe == null || deluxeRecipe.equals(""))) {
            bErrorsFound = true;
            errors.add("deluxeRecipe",new ActionMessage(ProductMaintConstants.PDB_PRODUCT_MAINT_ERROR_STRING_SEND_DELUXE_RECIPE, false) );
        }
        if (sendPremiumRecipe && (premiumRecipe == null || premiumRecipe.equals(""))) {
            bErrorsFound = true;
            errors.add("premiumRecipe",new ActionMessage(ProductMaintConstants.PDB_PRODUCT_MAINT_ERROR_STRING_SEND_PREMIUM_RECIPE, false) );
        }

        // Check for  dimension and weight if speicalty gift or fresh cut..and no subcodes
        strCheck = StringUtils.trimToEmpty((String)get("dimWeight"));
        if(canBeVendor && !hasSubcodes && StringUtils.isBlank(strCheck))
        {
            bErrorsFound = true;
            errors.add("dimWeight",new ActionMessage(ProductMaintConstants.PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_DIM_WEIGHT, false) );
        }
        
        boolean vendorError = false;
        // Check for Vendors //
        String[] vendors = request.getParameterValues("vendorId");
        if (canBeVendor && (vendors == null || vendors.length == 0)){
            errors.add("vendorId",new ActionMessage("A vendor is required.", false));
            vendorError = true;
        }          
        // check to see if shipping system is NONE
        // Check to make sure Vendor is valid for shipping system //
         int numVendors = 0;
         // if we have subcodes, divide the number of vendors by subcodes
         if (hasSubcodes && canBeVendor && (vendors !=null) ){
             numVendors = vendors.length/subCodeIdArray.length;
         }
        
        //check to see if zone jump eligible vendor product has a box type associated with it
        //if it doesn't, display an error.
        boolean zoneJumpEligibleFlag  = BooleanUtils.isTrue((Boolean)get("zoneJumpEligibleFlag"));
        String boxId = StringUtils.trimToEmpty((String)get("boxId"));
        if (canBeVendor && zoneJumpEligibleFlag && (StringUtils.isBlank(boxId)  || boxId.equalsIgnoreCase(ProductMaintConstants.NONE))){
            errors.add("boxId",new ActionMessage(ProductMaintConstants.PDB_PRODUCT_MAINT_ERROR_STRING_BOX_ID, false));
        } 

        // validate the SupplyExpense field
        float supplyExpense = ((Float)get("supplyExpense")).floatValue();
        String supplyExpenseEffDate = StringUtils.trimToEmpty((String)get("supplyExpenseEffDate"));
        logger.debug("supplyExpense: " + supplyExpense + " " + supplyExpenseEffDate);
        if (supplyExpense < 0) {
            bErrorsFound = true;
            errors.add("supplyExpense",new ActionMessage(ProductMaintConstants.PDB_PRODUCT_MAINT_ERROR_STRING_SUPPLY_EXPENSE, false) );
        } else if ( (supplyExpense > 0) && (supplyExpenseEffDate == null || supplyExpenseEffDate.equals("")) ) {
            bErrorsFound = true;
            errors.add("supplyExpenseEffDate",new ActionMessage(ProductMaintConstants.PDB_PRODUCT_MAINT_ERROR_STRING_SUPPLY_EXPENSE_EFF_DATE, false) );
        }
        // validate the royaltyPercent field
        float royaltyPercent = ((Float)get("royaltyPercent")).floatValue();
        String royaltyPercentEffDate = StringUtils.trimToEmpty((String)get("royaltyPercentEffDate"));
        logger.debug("royaltyPercent: " + royaltyPercent + " " + royaltyPercentEffDate);
        if (royaltyPercent < 0 || royaltyPercent > 100) {
            bErrorsFound = true;
            errors.add("royaltyPercent",new ActionMessage(ProductMaintConstants.PDB_PRODUCT_MAINT_ERROR_STRING_ROYALTY_PERCENT, false) );
        } else if ( (royaltyPercent > 0) && (royaltyPercentEffDate == null || royaltyPercentEffDate.equals("")) ) {
            bErrorsFound = true;
            errors.add("royaltyPercentEffDate",new ActionMessage(ProductMaintConstants.PDB_PRODUCT_MAINT_ERROR_STRING_ROYALTY_PERCENT_EFF_DATE, false) );
        }

        String shippingSystem = StringUtils.trimToEmpty((String)get("shippingSystem"));
        if (canBeVendor && (StringUtils.isBlank(shippingSystem)  || shippingSystem.equalsIgnoreCase(ProductMaintConstants.NONE))){
            errors.add("shippingSystem",new ActionMessage("A shipping system is required.", false));
        }
        else if (canBeVendor && numVendors > 1 && (shippingSystem.equalsIgnoreCase(ProductMaintConstants.SHIPPING_SYSTEM_TYPE_ESCALATE) || shippingSystem.equalsIgnoreCase(ProductMaintConstants.SHIPPING_SYSTEM_TYPE_FTP))){
            errors.add("vendorId",new ActionMessage("This shipping system type can only have one vendor. Please remove vendors or change shipping system type.", false));
        }
        if (!StringUtils.isBlank(shippingSystem) && !shippingSystem.equalsIgnoreCase(ProductMaintConstants.NONE) && (vendors != null))
        {
            ArrayList vendorList = (ArrayList)session.getAttribute(ProductMaintConstants.VENDOR_LIST_KEY);
            for (int i=0; i<vendors.length; i++){
                for (int j=0; j<vendorList.size(); j++) {
                    VendorVO vendor = (VendorVO)vendorList.get(j);
                    if (vendors[i].equalsIgnoreCase(vendor.getVendorId())){
                        if (!shippingSystem.equalsIgnoreCase(vendor.getVendorType())){
                            errors.add("vendorId",new ActionMessage(vendors[i]+" is not a valid vendor for this shipping system. Please remove vendor or change shipping system type.", false));
                            vendorError = true;
                        }
                    }
                }
            }
        }
        
        List vendorProducts = null;
        if (canBeVendor && vendorError != true){
            vendorProducts = this.extractVendorProductsListFromMap(request.getParameterMap());
            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();  
            String productVendorLimit = configUtil.getFrpGlobalParm(ProductMaintConstants.PDB_CONFIG_CONTEXT,"PRODUCT_VENDOR_LIMIT");
            if(vendorProducts.size() > Integer.parseInt(productVendorLimit)){
            	errors.add("vendorId",new ActionMessage(ProductMaintConstants.PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_VENDOR_LIMIT, false));
            }
            for (int i=0; i< vendorProducts.size(); i++){
                VendorProductVO vpVO = (VendorProductVO)vendorProducts.get(i);
                String vendorId = vpVO.getVendorId();
                if (!ProductMaintConstants.SHIPPING_SYSTEM_TYPE_FTDWEST.equalsIgnoreCase(shippingSystem) && vpVO.getVendorCost() <= 0 ){
                    errors.add("vendorId",new ActionMessage("Vendor Cost must be greater than zero for Vendor "+vendorId+".", false));
                }
                if (!StringUtils.isBlank(shippingSystem)){
                    if (!shippingSystem.equals(ProductMaintConstants.SHIPPING_SYSTEM_TYPE_FTP)){
                        if (vpVO.getVendorSku() == null || vpVO.getVendorSku().equalsIgnoreCase("")){ 
                            errors.add("vendorId",new ActionMessage("Vendor SKU is required for Vendor "+vendorId+".", false));
                        }
                    }
                }
            }
        }
        
        String azProductNameStandard = StringUtils.trimToEmpty((String)get("azProductNameStandard"));
        String azProductNameDeluxe = StringUtils.trimToEmpty((String)get("azProductNameDeluxe"));
        String azProductNamePremium = StringUtils.trimToEmpty((String)get("azProductNamePremium"));
        logger.debug("form: " + azProductNameStandard);
        logger.debug("form: " + azProductNameDeluxe);
        logger.debug("form: " + azProductNamePremium);
        if (azProductNameStandard != null && !azProductNameStandard.equals("") 
        		&& azProductNameDeluxe != null && !azProductNameDeluxe.equals("")
        		&& azProductNameStandard.equalsIgnoreCase(azProductNameDeluxe)) {
        	errors.add("amazonList", new ActionMessage("Standard Product Name and Deluxe Product Name cannot be the same", false));
        }
        if (azProductNameStandard != null && !azProductNameStandard.equals("")
        		&& azProductNamePremium != null && !azProductNamePremium.equals("")
        		&& azProductNameStandard.equalsIgnoreCase(azProductNamePremium)) {
        	errors.add("amazonList", new ActionMessage("Standard Product Name and Premium Product Name cannot be the same", false));
        }
        if (azProductNameDeluxe != null && !azProductNameDeluxe.equals("")
        		&& azProductNamePremium != null && !azProductNamePremium.equals("")
        		&& azProductNameDeluxe.equalsIgnoreCase(azProductNamePremium)) {
        	errors.add("amazonList", new ActionMessage("Deluxe Product Name and Premium Product Name cannot be the same", false));
        }
        
        // Validate source codes in websites field.
        String invalidSourceCodes = productSVC.validateWebsites((String)get("websites"));
        if(!StringUtils.isBlank(invalidSourceCodes)) {
            errors.add("websites", new ActionMessage("Invalid source code:" + invalidSourceCodes, false));
        }
        
        // Check for shipping key if specialty gift or fresh cut.
        strCheck = StringUtils.trimToEmpty((String)get("shippingKey"));
        logger.debug("shippingKey: " + strCheck);
        if(  !typeError && canBeVendor && StringUtils.isBlank(strCheck))
        {
            logger.error("shippingKey error");
            bErrorsFound = true;
            errors.add("shippingKey",new ActionMessage(ProductMaintConstants.PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_SHIPPING_KEY, false) );
        }
        
        boolean shipMethodCarrier = BooleanUtils.isTrue((Boolean)get("shipMethodCarrier"));
        boolean shipMethodFlorist = BooleanUtils.isTrue((Boolean)get("shipMethodFlorist"));
        //If status is available, check to make sure one delivery method is selected
        if(shipMethodCarrier == false && shipMethodFlorist == false )
        {
            if (!productType.equalsIgnoreCase(ProductMaintConstants.PRODUCT_TYPE_SERVICES)) {
                bErrorsFound = true;
                errors.add("shipMethod",new ActionMessage(ProductMaintConstants.PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_DELIVERY_METHOD, false) );
            }
        }
        
        if (!canBeVendor && shipMethodCarrier == true ){
            errors.add("shipMethodCarrier",new ActionMessage("Drop Ship Delivery is not available for this product type.", false) );
        }
        
        if (!canBeFloral && shipMethodFlorist == true ){
            errors.add("shipMethodFlorist",new ActionMessage("Florist Delivery is not available for this product type.", false) );
        }
        // Same day gift allows both ship methods all others do not
        if(!typeError && (shipMethodCarrier == true && shipMethodFlorist == true) && (!(canBeVendor && canBeFloral)))
        {
            bErrorsFound = true;
            errors.add("deliveryMethodsError",new ActionMessage(ProductMaintConstants.PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_DELIVERY_METHOD_ONE, false) );
        }
    
        // Check for delivery options if speicalty gift or fresh cut.
        if(  !typeError && canBeVendor && !deliveryMethodChecked(request))
        {
            bErrorsFound = true;
            errors.add("deliveryOptionsError",new ActionMessage(ProductMaintConstants.PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_DELIVERY_OPTION, false) );
        }
        
        // Check if there is a blank free shipping option selected with a product with free membership service product
        String productSubType = StringUtils.trimToEmpty((String)get("productSubType"));
        String durationCheck = StringUtils.trimToEmpty((String)get("duration"));
        if((durationCheck.equals("")) && (productSubType.equals("FREESHIP"))) {
            errors.add("duration", new ActionMessage("Must specify a duration option", false));
        }
        // BBN - validate morning Delivery
        boolean morningDelivery = BooleanUtils.isTrue((Boolean)get("morningDeliveryFlag"));
        if(canBeVendor && morningDelivery) {
        	logger.debug("Is vendor product and morning delivery is 'Yes'");
        	
        	if(!isNextDayDeliverySelected(request)) {
        		errors.add("morningDeliveryError", new ActionMessage(ProductMaintConstants.PDB_PRODUCT_MAINT_ERROR_STRING_MORNING_DELIVERY, false));
        	}
        }
        
        // Check for  valid ship days if speicalty gift or fresh cut.
        boolean shipDaySunday = BooleanUtils.isTrue((Boolean)get("shipDaySunday"));
        boolean shipDayMonday = BooleanUtils.isTrue((Boolean)get("shipDayMonday"));
        boolean shipDayTuesday = BooleanUtils.isTrue((Boolean)get("shipDayTuesday"));
        boolean shipDayWednesday = BooleanUtils.isTrue((Boolean)get("shipDayWednesday"));
        boolean shipDayThursday = BooleanUtils.isTrue((Boolean)get("shipDayThursday"));
        boolean shipDayFriday = BooleanUtils.isTrue((Boolean)get("shipDayFriday"));
        boolean shipDaySaturday = BooleanUtils.isTrue((Boolean)get("shipDaySaturday"));
        if(!typeError && canBeVendor&&(!shipDaySunday && !shipDayMonday && !shipDayTuesday  && !shipDayWednesday && !shipDayThursday && !shipDayFriday && !shipDaySaturday) &&!ProductMaintConstants.SHIPPING_SYSTEM_TYPE_FTDWEST.equalsIgnoreCase(shippingSystem))
        {
            bErrorsFound = true;
            errors.add("validShipDaysError",new ActionMessage(ProductMaintConstants.PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_SHIP_DAY, false) );
        }
        
        // validate the standard rank field
        float premiumPrice = ((Float)get("premiumPrice")).floatValue();
        float deluxePrice = ((Float)get("deluxePrice")).floatValue();
        
        // validate the colorFlag field
        if(colorSizeFlag && ( premiumPrice > 0 || deluxePrice > 0) )
        {
            bErrorsFound = true;
            errors.add("colorSizeError",new ActionMessage(ProductMaintConstants.PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_COLOR_SIZE, false) );
        }

        //Variable pricing
        Boolean variablePricing = (Boolean) get("variablePricingFlag");
        Float testFloat = (Float)get("variablePriceMax");
        
        if ( BooleanUtils.isTrue(variablePricing) && (testFloat==null || testFloat < .01)  ) {
            bErrorsFound = true;
            errors.add("variablePricingFlag",new ActionMessage(ProductMaintConstants.PDB_PRODUCT_MAINT_ERROR_STRING_VARIABLE_PRICE_REQUIRED, false) );
        }

        boolean subcodePriceError = false;

        Set subcodes = new HashSet();
        if(subCodeIdArray != null )
         {
             String firstPrice = subPrice[0];
             if (firstPrice == null ) { firstPrice = ProductMaintConstants.BLANK_STRING; }
        
             for (int i = 0; i < subCodeIdArray.length; i++)
             {
                 if(subCodeIdArray.length > 0)
                 {
                     // Check for unique subcode SKUs
                     if(subCodeIdArray[i].length() > 0 && (subcodes.add(subCodeIdArray[i])) == false)
                     {
                         subcodeDuplicateError = true;
                     }
                     //if specialty or fresh cut check for missing values
                     // change to require values for all subcodes
                     if(  !typeError && hasSubcodes)
                     {
                         if (subDescription[i] == null || subDescription[i].trim().equals(ProductMaintConstants.BLANK_STRING))
                         {
                             errors.add("subcode",new ActionMessage("Subcode "+subCodeIdArray[i]+ " description is required.", false) );
                         }
                         if (subRef[i] == null || subRef[i].trim().equals(ProductMaintConstants.BLANK_STRING))
                         {
                             errors.add("subcode",new ActionMessage("Subcode "+subCodeIdArray[i]+ " reference is required.", false) );
                         }
                         if (subPrice[i] == null || subPrice[i].trim().equals(ProductMaintConstants.BLANK_STRING))
                         {
                             errors.add("subcode",new ActionMessage("Subcode "+subCodeIdArray[i]+ " price is required.", false) );
                         }
                         if (subDimWeight[i] == null || subDimWeight[i].trim().equals(ProductMaintConstants.BLANK_STRING))
                         {
                             errors.add("subcode",new ActionMessage("Subcode "+subCodeIdArray[i]+ " dim/weight is required.", false) );
                         }
                         if(productType.equalsIgnoreCase(ProductMaintConstants.PRODUCT_TYPE_SPECIALTY))
                         {
                             if (subPrice[i] != null && !firstPrice.equalsIgnoreCase(subPrice[i]))
                             {
                                 subcodePricesEqualError = true;
                             }
                         }//end if specialty gift
                     }
                 }
             }
         }
        
         if( subcodeDuplicateError )
         {
             bErrorsFound = true;
             errors.add("subcode",new ActionMessage(ProductMaintConstants.PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_SUBCODE_UNIQUE, false) );
         }
        
         if( subcodePricesEqualError && !subcodePriceError)
         {
             bErrorsFound = true;
             errors.add("subcode",new ActionMessage(ProductMaintConstants.PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_SUBPRICE_EQUAL, false) );
         }
         if (canBeFloral && !canBeVendor && hasSubcodes){
             errors.add("subcode",new ActionMessage("Florist products cannot have subcodes.", false) );
         }
         
         //Exception validations...formatting of dates is done in pdb-avalidations.xml
        SimpleDateFormat sdf = new SimpleDateFormat(ProductMaintConstants.PDB_DATE_FORMAT);
        String exceptionStartDateStr = StringUtils.trimToEmpty((String)get("exceptionStartDate"));
        String exceptionEndDateStr = StringUtils.trimToEmpty((String)get("exceptionEndDate"));
        String exceptionCodeStr = StringUtils.trimToEmpty((String)get("exceptionCode"));
        String exceptionMessageStr = StringUtils.trimToEmpty((String)get("exceptionMessage"));
        // if product isn't available, mark it empty //
        if (!exceptionCodeStr.equalsIgnoreCase(ProductMaintConstants.PRODUCT_AVAILABLE_KEY)){
            exceptionCodeStr = "";
        }
        Date exceptionEndDate = null;
        Date exceptionStartDate = null;
        if (exceptionCodeStr.equalsIgnoreCase(ProductMaintConstants.PRODUCT_AVAILABLE_KEY)){
            if( exceptionStartDateStr.equalsIgnoreCase("")) {
                bErrorsFound = true;
                errors.add("exceptionStartDate",new ActionMessage(ProductMaintConstants.PDB_PRODUCT_MAINT_ERROR_STRING_EX_START_DATE_REQUIRED, false) );
            }else{
               exceptionStartDate = sdf.parse(exceptionStartDateStr); 
            }
            if( exceptionEndDateStr.equalsIgnoreCase("") ) {
                bErrorsFound = true;
                errors.add("exceptionEndDate",new ActionMessage(ProductMaintConstants.PDB_PRODUCT_MAINT_ERROR_STRING_EX_END_DATE_REQUIRED, false) );
            }else{
                exceptionEndDate = sdf.parse(exceptionEndDateStr); 
            }
            if(exceptionEndDate!=null && exceptionStartDate!=null && exceptionEndDate.before(exceptionStartDate)) {
                bErrorsFound = true;
                errors.add("exceptionEndDate",new ActionMessage(ProductMaintConstants.PDB_PRODUCT_MAINT_ERROR_STRING_EX_START_BEFORE_END_DATE, false) );
            }
        }
        if( (!exceptionStartDateStr.equalsIgnoreCase("") || !exceptionEndDateStr.equalsIgnoreCase("") || !exceptionMessageStr.equalsIgnoreCase("")) && exceptionCodeStr.equalsIgnoreCase("")) {
            bErrorsFound = true;
            errors.add("exceptionCode",new ActionMessage(ProductMaintConstants.PDB_PRODUCT_MAINT_ERROR_STRING_EX_NO_EXCEPTION_DATE_TYPE, false) );
        }
            
        //Check for validation errors in the product addon fields
            
        ArrayList<ProductAddonVO> productAddonList = ProductMaintDetailForm.extractProductAddonListFromMap(request.getParameterMap(),ProductMaintConstants.ADD_ON_ACTIVE_FLAG_KEY,
                                                              ProductMaintConstants.ADD_ON_PRODUCT_DISPLAY_SEQUENCE_NUMBER_KEY, ProductMaintConstants.ADD_ON_PRODUCT_MAX_QUANTITY_KEY,
                                                              (ArrayList<ProductAddonVO>)session.getAttribute(ProductMaintConstants.PRODUCT_ADD_ON_LIST));
        ArrayList<ProductAddonVO> productAddonVaseList = ProductMaintDetailForm.extractProductAddonListFromMap(request.getParameterMap(),ProductMaintConstants.ADD_ON_VASE_ACTIVE_FLAG_KEY,
                                                              ProductMaintConstants.ADD_ON_VASE_PRODUCT_DISPLAY_SEQUENCE_NUMBER_KEY, ProductMaintConstants.ADD_ON_VASE_MAX_QUANTITY_KEY,
                                                              (ArrayList<ProductAddonVO>)session.getAttribute(ProductMaintConstants.PRODUCT_ADD_ON_VASE_LIST));
        //Validations for the product add-on addons
        HashSet <String> uniqueNumberSet = new HashSet <String> ();
        for (ProductAddonVO productAddonVO : productAddonList)
        {
            if (productAddonVO.getDisplaySequenceNumber() != null && StringUtils.isNotBlank(productAddonVO.getDisplaySequenceNumber()))
            {
                logger.debug("info " + productAddonVO.getAddonDescription() + " seq " + productAddonVO.getDisplaySequenceNumber() + " qty " +  productAddonVO.getMaxQuantity());
         
                if (!uniqueNumberSet.add(productAddonVO.getDisplaySequenceNumber()))
                {
                    logger.debug("duplicate sequence number recieved " + productAddonVO.getAddonDescription() + " seq " + productAddonVO.getDisplaySequenceNumber());
                    errors.add("productAddons",new ActionMessage(ProductMaintConstants.PDB_PRODUCT_ADD_ON_ERROR_STRING_DUPLICATE_SEQUENCE.replaceFirst("AAA",productAddonVO.getAddonId() + " - " + StringEscapeUtils.unescapeHtml(productAddonVO.getAddonDescription()))
                                                                 .replaceFirst("BBB",productAddonVO.getDisplaySequenceNumber()), false) );
                }

                try
                {
                    Integer sequenceNumber = Integer.parseInt(productAddonVO.getDisplaySequenceNumber());
                    if (sequenceNumber < ProductMaintConstants.PRODUCT_ADDON_MIN_DISPLAY_SEQUENCE_NUMBER
                        || sequenceNumber > ProductMaintConstants.PRODUCT_ADDON_MAX_DISPLAY_SEQUENCE_NUMBER)
                    {
                        logger.debug("sequence number out of range recieved " + productAddonVO.getAddonDescription());
                        errors.add("productAddons",new ActionMessage(ProductMaintConstants.PDB_PRODUCT_ADD_ON_ERROR_STRING_INVALID_SEQUENCE.replaceFirst("AAA",productAddonVO.getAddonId() + " - " + StringEscapeUtils.unescapeHtml(productAddonVO.getAddonDescription())), false) );
                    }
                }
                catch(NumberFormatException nfe)
                {
                    logger.debug("non-numeric sequence number recieved " + productAddonVO.getAddonDescription());
                    errors.add("productAddons",new ActionMessage(ProductMaintConstants.PDB_PRODUCT_ADD_ON_ERROR_STRING_INVALID_SEQUENCE.replaceFirst("AAA",productAddonVO.getAddonId() + " - " + StringEscapeUtils.unescapeHtml(productAddonVO.getAddonDescription())), false) );
                }
            }
            else
            {
                if (productAddonVO.getActiveFlag())
                {
                    logger.debug("missing display sequence number on active product addon " + productAddonVO.getAddonDescription());
                    errors.add("productAddons",new ActionMessage(ProductMaintConstants.PDB_PRODUCT_ADD_ON_ERROR_STRING_MISSING_SEQUENCE.replaceFirst("AAA",productAddonVO.getAddonId() + " - " + StringEscapeUtils.unescapeHtml(productAddonVO.getAddonDescription())), false) );                    
                }
                    
            }
            if (productAddonVO.getMaxQuantity() != null && StringUtils.isNotBlank(productAddonVO.getMaxQuantity()))
            {
                try
                {
                    Integer maxQuantity = Integer.parseInt(productAddonVO.getMaxQuantity());
                    if (maxQuantity < ProductMaintConstants.PRODUCT_ADDON_MIN_DISPLAY_SEQUENCE_NUMBER
                        || maxQuantity > ProductMaintConstants.PRODUCT_ADDON_MAX_MAX_QUANTITY)
                    {
                        logger.debug("quantity out of range recieved " + productAddonVO.getAddonDescription());
                        errors.add("productAddons",new ActionMessage(ProductMaintConstants.PDB_PRODUCT_ADD_ON_ERROR_STRING_INVALID_MAX_QUANTITY.replaceFirst("AAA",productAddonVO.getAddonId() + " - " + StringEscapeUtils.unescapeHtml(productAddonVO.getAddonDescription())), false) );
                    }
                }
                catch(NumberFormatException nfe)
                {
                    logger.debug("non-numeric quantity number recieved " + productAddonVO.getAddonDescription());
                    errors.add("productAddons",new ActionMessage(ProductMaintConstants.PDB_PRODUCT_ADD_ON_ERROR_STRING_INVALID_MAX_QUANTITY.replaceFirst("AAA",productAddonVO.getAddonId() + " - " + StringEscapeUtils.unescapeHtml(productAddonVO.getAddonDescription())), false) );
                }
            }
            else
            {
                if (productAddonVO.getActiveFlag())
                {
                    logger.debug("missing quantity on active product addon " + productAddonVO.getAddonDescription());
                    errors.add("productAddons",new ActionMessage(ProductMaintConstants.PDB_PRODUCT_ADD_ON_ERROR_STRING_MISSING_MAX_QUANTITY.replaceFirst("AAA",productAddonVO.getAddonId() + " - " + StringEscapeUtils.unescapeHtml(productAddonVO.getAddonDescription())), false) );
                }
            }
        }
        
        //Validations for the product add-on vases
        uniqueNumberSet = new HashSet <String> ();
        for (ProductAddonVO productAddonVO : productAddonVaseList)
        {
            if (productAddonVO.getDisplaySequenceNumber() != null && StringUtils.isNotBlank(productAddonVO.getDisplaySequenceNumber()))
            {
                logger.debug("info " + productAddonVO.getAddonDescription() + " seq " + productAddonVO.getDisplaySequenceNumber() + " qty " +  productAddonVO.getMaxQuantity());
         
                if (!uniqueNumberSet.add(productAddonVO.getDisplaySequenceNumber()))
                {
                    logger.debug("duplicate sequence number recieved " + productAddonVO.getAddonDescription() + " seq " + productAddonVO.getDisplaySequenceNumber());
                    errors.add("productAddons",new ActionMessage(ProductMaintConstants.PDB_PRODUCT_ADD_ON_ERROR_STRING_DUPLICATE_SEQUENCE.replaceFirst("AAA",productAddonVO.getAddonId() + " - " + StringEscapeUtils.unescapeHtml(productAddonVO.getAddonDescription()))
                                                                 .replaceFirst("BBB",productAddonVO.getDisplaySequenceNumber()), false) );
                }

                try
                {
                    Integer sequenceNumber = Integer.parseInt(productAddonVO.getDisplaySequenceNumber());
                    if (sequenceNumber < ProductMaintConstants.PRODUCT_ADDON_MIN_DISPLAY_SEQUENCE_NUMBER
                        || sequenceNumber > ProductMaintConstants.PRODUCT_ADDON_MAX_DISPLAY_SEQUENCE_NUMBER)
                    {
                        logger.info("sequence number out of range recieved " + productAddonVO.getAddonDescription());
                        errors.add("productAddons",new ActionMessage(ProductMaintConstants.PDB_PRODUCT_ADD_ON_ERROR_STRING_INVALID_SEQUENCE.replaceFirst("AAA",productAddonVO.getAddonId() + " - " + StringEscapeUtils.unescapeHtml(productAddonVO.getAddonDescription())), false) );
                    }
                }
                catch(NumberFormatException nfe)
                {
                    errors.add("productAddons",new ActionMessage(ProductMaintConstants.PDB_PRODUCT_ADD_ON_ERROR_STRING_INVALID_SEQUENCE.replaceFirst("AAA",productAddonVO.getAddonId() + " - " + StringEscapeUtils.unescapeHtml(productAddonVO.getAddonDescription())), false) );
                    logger.debug("non-numeric sequence number recieved " + productAddonVO.getAddonDescription());
                }
            }
            else
            {
                if (productAddonVO.getActiveFlag())
                {
                    logger.debug("missing display sequence number on active product addon " + productAddonVO.getAddonDescription());
                    errors.add("productAddons",new ActionMessage(ProductMaintConstants.PDB_PRODUCT_ADD_ON_ERROR_STRING_MISSING_SEQUENCE.replaceFirst("AAA",productAddonVO.getAddonId() + " - " + StringEscapeUtils.unescapeHtml(productAddonVO.getAddonDescription())), false) );                    
                }
                    
            }
        }  
        //pquadProductID is	Required field when Shipping System FTD West is selected
        String pquadProductID=StringUtils.trimToEmpty((String)get("pquadProductID"));
        if(ProductMaintConstants.SHIPPING_SYSTEM_TYPE_FTDWEST.equalsIgnoreCase(shippingSystem) && "".equalsIgnoreCase(pquadProductID))
        {
        	logger.debug("pquadProductID is required for FTD WEST shipping system");
        	errors.add("pquadProductID",new ActionMessage("error.pquadProductID.required",true));        	
        }
            
        }catch(Exception e) {
            e.printStackTrace();
            errors.add("global",new ActionMessage("An internal error has occurred: "+e.toString()+". Please try again.", false));
            logger.error(e);
        }
            
        }

        // Ensure a personalization template order has been chosen
        String personalizationTemplate = StringUtils.trimToEmpty((String)get("personalizationTemplate"));
        String personalizationTemplateOrder = StringUtils.trimToEmpty((String)get("personalizationTemplateOrder"));
        if(!personalizationTemplate.equals("") && !personalizationTemplate.equalsIgnoreCase("none") && personalizationTemplateOrder.equals(""))
        {
            bErrorsFound = true;
            errors.add("personalizationTemplateOrder",new ActionMessage(ProductMaintConstants.PDB_PRODUCT_MAINT_ERROR_STRING_TEMPLATE_ORDER, false) );
        }
        
        
        
        if(bErrorsFound || errors.size() > 0)
        {
            //Add the global error message
            errors.add("global",new ActionMessage("Please correct the errors below.", false));
            // Set the required attributes
            request.setAttribute(ProductMaintConstants.OE_PRODUCT_ID_KEY, StringUtils.trimToEmpty((String)get("productId")));
            request.setAttribute(ProductMaintConstants.PDB_PRODUCT_SEARCH_BY_KEY, ProductMaintConstants.PDB_SEARCH_BY_PRODUCT_ID);
            // Set the back link
            request.setAttribute(ProductMaintConstants.MENU_BACK_KEY, 
                                 ProductMaintConstants.MENU_BACK_PRODUCT_MAINT_LIST_KEY);
            request.setAttribute(ProductMaintConstants.MENU_BACK_TEXT_KEY, 
                                 ProductMaintConstants.MENU_BACK_TEXT_PRODUCT_MAINT_LIST_KEY);
        }
        return errors;
    }

    public boolean isFieldPopulated(String memberName) {
        return StringUtils.isNotBlank((String)this.get(memberName));
    }

    private boolean subcodeEmpty(String id, String description, String ref, String price, String dimWeight){
        if ((id != null && !id.trim().equals(ProductMaintConstants.BLANK_STRING)) ||
            (description != null && !description.trim().equals(ProductMaintConstants.BLANK_STRING)) ||
            (ref != null && !ref.trim().equals(ProductMaintConstants.BLANK_STRING)) ||
            (price != null && !price.trim().equals(ProductMaintConstants.BLANK_STRING)) ||
            (dimWeight != null && !dimWeight.trim().equals(ProductMaintConstants.BLANK_STRING)))
        {
            return false;
        }
        else {
            return true;
        }
     }
     
    /* This method checks if the user checked  a delivery option. */
    private boolean deliveryMethodChecked(HttpServletRequest request) throws Exception
    {
        
        List deliveryOptionList = null;
        ProductVO productVO = new ProductVO();
        deliveryOptionList = productVO.buildDeliveryOptionsList(request);
        boolean somethingChecked = false;
        if (deliveryOptionList != null && deliveryOptionList.size() > 0){
            somethingChecked =true;
        }
    
        return somethingChecked;
    }

    public IProductMaintSVCBusiness getProductSVC() {
        return productSVC;    
    }
    
    public void setProductSVC(IProductMaintSVCBusiness productSVC) {
        this.productSVC=productSVC;    
    }

    static public List extractVendorProductsListFromMap(Map map) {
        List vendorProducts = new ArrayList();
        String[] vendor = (String[])map.get("vendorId");
        if(map!=null && vendor!= null)
        {
            String[] vendorName = (String[])map.get("vendorName");
            String[] vendorCost = (String[])map.get("vendorCost");
            String[] vendorSKU = (String[])map.get("vendorSKU");
            String[] vendorAvailable = (String[])map.get("vendorAvailable");
            String[] productId = (String[])map.get("productSubcodeVendorId");
            if (vendor!= null){
                for (int i = 0; i< vendor.length; i++){
                    VendorProductVO vendorProductVO = new VendorProductVO();
                    vendorProductVO.setVendorId(vendor[i]);
                    vendorProductVO.setVendorName(vendorName[i]);
                    if (vendorCost[i] == null || vendorCost[i].equalsIgnoreCase(""))
                        vendorProductVO.setVendorCost(new Double(0.0));
                    else
                        vendorProductVO.setVendorCost(new Double(vendorCost[i]));
                    vendorProductVO.setVendorSku(vendorSKU[i]);
                    vendorProductVO.setProductSkuId(productId[i]);
                    vendorProductVO.setRemoved(false);
                    if (vendorAvailable[i].equalsIgnoreCase("true")){
                        vendorProductVO.setAvailable(true);
                    }else {
                        vendorProductVO.setAvailable(false);
                    }
                    vendorProductVO.setProductSkuId(productId[i]);
                    vendorProducts.add(vendorProductVO);
                }
            }
        }
        return vendorProducts;
    }

    static public List extractSubcodeListFromMap(Map map) {
         // this is a hack - someone fix this someday //
        String productId = "";
        if (map.get("productId").getClass().equals(productId.getClass())){
            productId = (String) map.get("productId");
        }else{
            String[] productIdArr = (String[])map.get("productId");
            productId = productIdArr[0];
        }
        String[] subCodeIdArray = 
            (String[])map.get("productSubCodeId");
        String[] subCodeDescArray = 
            (String[])map.get(ProductMaintConstants.PDB_PRODUCT_SUB_CODE_DESC_KEY);
        String[] subCodeRefArray = 
            (String[])map.get(ProductMaintConstants.PDB_PRODUCT_SUB_CODE_REF_KEY);
        String[] subCodePriceArray = 
            (String[])map.get(ProductMaintConstants.PDB_PRODUCT_SUB_CODE_PRICE_KEY);
        String[] subCodeAvailableArray = 
            (String[])map.get(ProductMaintConstants.PDB_PRODUCT_SUB_CODE_AVAILABLE_KEY);
        String[] subCodeDimWeightArray = 
            (String[])map.get(ProductMaintConstants.PDB_PRODUCT_SUB_CODE_DIM_WEIGHT_KEY);
        
        List subCodeList = new ArrayList();
        if (subCodeIdArray != null) {
            for (int i = 0; i < subCodeIdArray.length; i++) {
                if (subCodeIdArray[i].length() > 0) {
                    ProductSubCodeVO vo = new ProductSubCodeVO();
                    vo.setProductId(productId);
                    vo.setProductSubCodeId(subCodeIdArray[i]);
                    if (subCodeDescArray[i] != null && 
                        !subCodeDescArray[i].equals(ProductMaintConstants.BLANK_STRING)) {
                        vo.setSubCodeDescription(subCodeDescArray[i]);
                    }
                    if (subCodeRefArray[i] != null && 
                        !subCodeRefArray[i].equals(ProductMaintConstants.BLANK_STRING)) {
                        vo.setSubCodeRefNumber(subCodeRefArray[i]);
                    }
                    if (subCodePriceArray[i] != null && 
                        !subCodePriceArray[i].equals(ProductMaintConstants.BLANK_STRING)) {
                        vo.setSubCodePrice(Float.parseFloat(FTDUtil.removeFormattingCurrency(subCodePriceArray[i])));
                    }
                    if (subCodeDimWeightArray[i] != null && 
                        !subCodeDimWeightArray[i].equals(ProductMaintConstants.BLANK_STRING)) {
                        vo.setDimWeight(subCodeDimWeightArray[i]);
                    }
                    if (subCodeAvailableArray[i] != null && 
                        !subCodeAvailableArray[i].equals(ProductMaintConstants.BLANK_STRING)) {
                        String status = subCodeAvailableArray[i];
                        if (status.equalsIgnoreCase("true")){
                            vo.setActiveFlag(true);
                        }else{
                            vo.setActiveFlag(false);
                        }
                    }
                    vo.setDisplayOrder(i + 1);
                    subCodeList.add(vo);
                }
            }
        }
        return subCodeList;
    }

    static public List extractShippingMethodsListFromMap(HttpServletRequest request) throws Exception{
        ArrayList deliveryOptionList = new ArrayList();
        HttpSession session = request.getSession();
        Map map =  request.getParameterMap();
        String[] pageShippingMethods = 
                    (String[])map.get("shippingMethodList");
                
        //get list of shipping methods and populate delivery options array
        List methodList = (List)session.getAttribute(ProductMaintConstants.SHIPPING_METHODS_KEY);
        boolean containsId = false;
        for (int i = 0; i < methodList.size(); i++) {
            DeliveryOptionVO vo = (DeliveryOptionVO)methodList.get(i);
            containsId = false;
            if (pageShippingMethods != null){
                for (int j=0; j < pageShippingMethods.length; j++){
                    if (vo.getShippingMethodId().equalsIgnoreCase(pageShippingMethods[j])){
                        containsId = true;
                    }
                }
            }
            if (containsId){
                vo.setSelected(true);
            }else{
                vo.setSelected(false);
            }
            deliveryOptionList.add(vo);
        }
        return deliveryOptionList;
    }
    
    static public Map extractColorListsFromMap(HttpServletRequest request) throws Exception{
        HttpSession session = request.getSession();
        HashMap colorsMap = new HashMap();
        Map map = request.getParameterMap();
        ArrayList colors = (ArrayList)session.getAttribute(ProductMaintConstants.COLORS_KEY );
        String[] colorsArray = 
            (String[])map.get("arrangementColorsArray");
        ArrayList includedColors = new ArrayList();
        if (colorsArray != null){
            for (int i = 0; i < colorsArray.length; i++) {
                ColorVO dummyVO = new ColorVO();
                dummyVO.setId((String)colorsArray[i]);
                int index = colors.indexOf(dummyVO);
                ColorVO color = (ColorVO)colors.get(index);
                includedColors.add(color);
            }
        }

        ArrayList excludedColors = new ArrayList(colors);
        List tempCurrentColors = new ArrayList(includedColors.size());
        for (int i = 0; i < includedColors.size(); i++) {
        ColorVO colorVO = (ColorVO)includedColors.get(i);
        int orderBy = colorVO.getOrderBy();
        if (excludedColors.contains(colorVO)) {
            int index = excludedColors.indexOf(colorVO);
            colorVO = (ColorVO)excludedColors.remove(index);
            colorVO.setOrderBy(orderBy);
        }
        tempCurrentColors.add(colorVO);
        }

        includedColors = new ArrayList(tempCurrentColors);
        ColorVOComparator colorVOComparator = new ColorVOComparator();
        Collections.sort(includedColors, colorVOComparator);
    
        colorsMap.put(ProductMaintConstants.INCLUDED_COLORS, includedColors);
        colorsMap.put(ProductMaintConstants.EXCLUDED_COLORS, excludedColors);
        colorsMap.put(ProductMaintConstants.EXCLUDED_COLORS_STATIC, excludedColors);
        colorsMap.put(ProductMaintConstants.INCLUDED_COLORS_STATIC, includedColors);
        return colorsMap;
    }

    static public Map extractComponentListsFromMap(HttpServletRequest request) throws Exception{
        HttpSession session = request.getSession();
        HashMap componentsMap = new HashMap();
        Map map = request.getParameterMap();
        ArrayList components = (ArrayList)session.getAttribute(ProductMaintConstants.COMPONENTS_KEY );
        ArrayList includedTest = (ArrayList) session.getAttribute(ProductMaintConstants.INCLUDED_COMPONENT_SKUS);
        for (int i=0; i<includedTest.size(); i++) {
            ValueVO test = (ValueVO)includedTest.get(i);
            if (!components.contains(test)) {
                components.add(test);
            }
        }
        String[] componentsArray = (String[])map.get("componentSkuArray");
        ArrayList includedComponents = new ArrayList();
        if (componentsArray != null){
            for (int i = 0; i < componentsArray.length; i++) {
                ValueVO dummyVO = new ValueVO();
                dummyVO.setId(componentsArray[i]);
                for (int j=0; j<components.size(); j++) {
                    ValueVO testVO = (ValueVO)components.get(j);
                    if (testVO.getId().equalsIgnoreCase(dummyVO.getId())) {
                        includedComponents.add(testVO);
                        break;
                    }
                }
            }
        }

        ArrayList availableComponents = new ArrayList(components);
        for (int i = 0; i < includedComponents.size(); i++) {
            ValueVO valueVO = (ValueVO) includedComponents.get(i);
            if (availableComponents.contains(valueVO)) {
                availableComponents.remove(valueVO);
            }
        }

        componentsMap.put(ProductMaintConstants.INCLUDED_COMPONENT_SKUS, includedComponents);
        componentsMap.put(ProductMaintConstants.AVAILABLE_COMPONENT_SKUS, availableComponents);
        return componentsMap;
    }
    
    static public ArrayList<ProductAddonVO> extractProductAddonListFromMap(Map map, String activeFlagKey,
                                                                                            String displaySequenceNumberKey,
                                                                                            String maxQuantityKey, ArrayList<ProductAddonVO> originalProductAddonList) throws Exception{
        
        
        ArrayList <ProductAddonVO> productAddonList = new ArrayList <ProductAddonVO> ();
        String[] productAddonActiveFlagArray = ((String[])map.get(activeFlagKey));
        HashSet <String> productAddonActiveFlagSet = new HashSet <String> ();
        
        if (productAddonActiveFlagArray != null) {
            for (String productAddonActiveFlag : productAddonActiveFlagArray)
            {
                productAddonActiveFlagSet.add(productAddonActiveFlag);   
            }
        }
        String[] productAddonDisplaySequenceNumbers = (String[]) map.get(displaySequenceNumberKey);
        String[] productAddonMaxQuantities = (String[])map.get(maxQuantityKey);

        for (int i = 0; i< originalProductAddonList.size(); i++)
        {
            ProductAddonVO productAddonVO = originalProductAddonList.get(i);
            if(productAddonVO.isModified(productAddonActiveFlagSet.contains(productAddonVO.getAddonId()),
                                         StringUtils.isBlank(productAddonMaxQuantities[i]) ? null : productAddonMaxQuantities[i],  
                                         StringUtils.isBlank(productAddonDisplaySequenceNumbers[i]) ? null : productAddonDisplaySequenceNumbers[i]))
            {
                productAddonVO.setModified(true);
                if (productAddonVO.getAddonAvailableFlag())
                {
                    productAddonVO.setActiveFlag(productAddonActiveFlagSet.contains(productAddonVO.getAddonId()));
                }
                if (productAddonDisplaySequenceNumbers != null && StringUtils.isNotBlank(productAddonDisplaySequenceNumbers[i]))
                {
                    productAddonVO.setDisplaySequenceNumber(productAddonDisplaySequenceNumbers[i].trim());
                }
                else
                {
                    productAddonVO.setDisplaySequenceNumber(null);
                }
                if (productAddonMaxQuantities != null && StringUtils.isNotBlank(productAddonMaxQuantities[i]))
                {
                    productAddonVO.setMaxQuantity(productAddonMaxQuantities[i].trim());
                }
                else if (productAddonActiveFlagSet.contains(productAddonVO.getAddonId()) &&
                                                       productAddonVO.getAddonTypeId().equals("" + ProductMaintConstants.ADDON_TYPE_VASE_ID))
                {
                    productAddonVO.setMaxQuantity("1");
                }
                else
                {
                    productAddonVO.setMaxQuantity(null);
                }
            }
            else if (!(productAddonVO.isModified() != null && productAddonVO.isModified()))
            {
                productAddonVO.setModified(false);
            }
            productAddonList.add(productAddonVO);
        }
        return productAddonList;
    }


    static public List extractExcludedStatesFromMap(Map map) throws Exception{
        String[] sdeStateIdArray = 
            (String[])map.get(ProductMaintConstants.EXCLUDED_STATE_ID);
        String[] sdeStateNameArray = 
            (String[])map.get(ProductMaintConstants.EXCLUDED_STATE_NAME);
        String[] sdeSunExcluded = 
            (String[])map.get(ProductMaintConstants.DELV_DAY_SUN_KEY);
        String[] sdeMonExcluded = 
            (String[])map.get(ProductMaintConstants.DELV_DAY_MON_KEY);
        String[] sdeTueExcluded = 
            (String[])map.get(ProductMaintConstants.DELV_DAY_TUE_KEY);
        String[] sdeWedExcluded = 
            (String[])map.get(ProductMaintConstants.DELV_DAY_WED_KEY);
        String[] sdeThuExcluded = 
            (String[])map.get(ProductMaintConstants.DELV_DAY_THU_KEY);
        String[] sdeFriExcluded = 
            (String[])map.get(ProductMaintConstants.DELV_DAY_FRI_KEY);
        String[] sdeSatExcluded = 
            (String[])map.get(ProductMaintConstants.DELV_DAY_SAT_KEY);
        
        ArrayList excludedStates = new ArrayList();
        StateDeliveryExclusionVO sdeVO;
        if (sdeStateIdArray != null && sdeStateIdArray.length > 0) {
            List sdeSunExcludedList = null;
            List sdeMonExcludedList = null;
            List sdeTueExcludedList = null;
            List sdeWedExcludedList = null;
            List sdeThuExcludedList = null;
            List sdeFriExcludedList = null;
            List sdeSatExcludedList = null;
           
            if (sdeSunExcluded != null)
                sdeSunExcludedList = Arrays.asList(sdeSunExcluded);

            if (sdeMonExcluded != null)
                sdeMonExcludedList = Arrays.asList(sdeMonExcluded);

            if (sdeTueExcluded != null)
                sdeTueExcludedList = Arrays.asList(sdeTueExcluded);

            if (sdeWedExcluded != null)
                sdeWedExcludedList = Arrays.asList(sdeWedExcluded);

            if (sdeThuExcluded != null)
                sdeThuExcludedList = Arrays.asList(sdeThuExcluded);

            if (sdeFriExcluded != null)
                sdeFriExcludedList = Arrays.asList(sdeFriExcluded);

            if (sdeSatExcluded != null)
                sdeSatExcludedList = Arrays.asList(sdeSatExcluded);
           
            for (int i = 0; i < sdeStateIdArray.length; i++) {
                sdeVO = new StateDeliveryExclusionVO();

                if (sdeStateIdArray[i] != null && 
                    sdeStateIdArray[i].length() > 0) {
                    sdeVO.setExcludedState(sdeStateIdArray[i]);
                }

                if (sdeStateNameArray[i] != null && 
                    sdeStateNameArray[i].length() > 0) {
                    sdeVO.setStateName(sdeStateNameArray[i]);
                }

                if (sdeSunExcludedList != null && 
                    sdeSunExcludedList.contains(String.valueOf(i))) {
                    sdeVO.setSunExcluded(true);
                }

                if (sdeMonExcludedList != null && 
                    sdeMonExcludedList.contains(String.valueOf(i))) {
                    sdeVO.setMonExcluded(true);
                }

                if (sdeTueExcludedList != null && 
                    sdeTueExcludedList.contains(String.valueOf(i))) {
                    sdeVO.setTueExcluded(true);
                }

                if (sdeWedExcludedList != null && 
                    sdeWedExcludedList.contains(String.valueOf(i))) {
                    sdeVO.setWedExcluded(true);
                }

                if (sdeThuExcludedList != null && 
                    sdeThuExcludedList.contains(String.valueOf(i))) {
                    sdeVO.setThuExcluded(true);
                }

                if (sdeFriExcludedList != null && 
                    sdeFriExcludedList.contains(String.valueOf(i))) {
                    sdeVO.setFriExcluded(true);
                }

                if (sdeSatExcludedList != null && 
                    sdeSatExcludedList.contains(String.valueOf(i))) {
                    sdeVO.setSatExcluded(true);
                }

                excludedStates.add(sdeVO);
            }
        }
        return excludedStates;
    }
    
    /** Convenient method to check if NextDay delivery is selected
     * @param request
     * @return
     * @throws Exception
     */
    private boolean isNextDayDeliverySelected(HttpServletRequest request) throws Exception {
        Map map =  request.getParameterMap();
        String[] pageShippingMethods = (String[])map.get("shippingMethodList");
        if (pageShippingMethods != null) {
            for (int j=0; j < pageShippingMethods.length; j++){
                if (ProductMaintConstants.SHIPPING_NEXT_DAY_CODE.equalsIgnoreCase(pageShippingMethods[j])){
                    return true;
                }
            }
        }        
        return false;      
    }
}

