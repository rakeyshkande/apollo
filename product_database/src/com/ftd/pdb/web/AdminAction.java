package com.ftd.pdb.web;

import com.ftd.pdb.common.ProductMaintConstants;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * Implementation of <strong>Action</strong> that processes a
 * holiday pricing activation change.
 *
 * @author Ed Mueller
 */

public final class AdminAction extends PDBAction
{

  private static final String LOGGER_CATEGORY =
                    ProductMaintConstants.PDB_GENERAL_CATEGORY_NAME;

    /**
      * Constructor
      */
    public AdminAction()
    {
      super("com.ftd.pdb.struts.AdminSetupAction");
    }

        /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param mapping The ActionMapping used to select this instance
     * @param actionForm The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     */
    public ActionForward execute(ActionMapping mapping,
                                 ActionForm form,
                                 HttpServletRequest request,
                                 HttpServletResponse response)
    throws IOException, ServletException
    {
        ActionForward forward = new ActionForward();
        forward.setName("success");
        // Forward control to the specified success URI
        return (forward);
    }
}