
package com.ftd.pdb.web;

import com.ftd.pdb.common.ProductMaintConstants;

import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;


public class UpsellListForm extends PDBActionForm {
    private String productId;
    private String searchBy;

    public UpsellListForm() {
        super("com.ftd.pdb.web.UpsellListForm");
    }
/*
    public String getProductId() {
        return productId;
    }

    public void setProductId(String newId) {
        productId = newId;
    }
 public String getSearchBy() {
     return searchBy;
 }

 public void setSearchBy(String newSearchBy) {
     searchBy = newSearchBy;
 }

*/
    /**
     * Reset all properties to their default values.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
     /*
    public void reset(ActionMapping mapping, HttpServletRequest request) {
        //productId = " ";
    }
    */

    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public ActionErrors validate(ActionMapping mapping, 
                                 HttpServletRequest request) {

        ActionErrors errors = new ActionErrors();
        errors = super.validate(mapping, request);
        if (errors.size() > 0){
            HttpSession session = request.getSession();
            request.setAttribute("upsellList",session.getAttribute("upsellList"));
        }
        return errors;

    }

}
