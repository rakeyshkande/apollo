package com.ftd.pdb.web;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pdb.common.ProductMaintConstants;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.Field;
import org.apache.commons.validator.Validator;
import org.apache.commons.validator.ValidatorAction;
import org.apache.commons.validator.util.ValidatorUtils;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.validator.Resources;


public class ProductMaintDetailValidation {

    private static Logger LOGGER = new Logger("com.ftd.pdb.struts.ProductMaintValidation");

    public static boolean validateProductType(Object bean,
                                           ValidatorAction va, Field field,
                                           ActionMessages errors,
                                           Validator validator,
                                           HttpServletRequest request) 
    {
        boolean retval = true;
        String value = ValidatorUtils.getValueAsString(bean, field.getProperty());
        
        if(StringUtils.isBlank(value) || StringUtils.equals(value,ProductMaintConstants.NONE) ) {
            LOGGER.error("Failed validation validateProductType.  Product type was "+value);
            errors.add(field.getKey(), Resources.getActionMessage(validator, request, va, field)); // struts 1.1 problem

            retval = false;
        }
        
        return retval;
    }
    
    public static boolean validateProductSubType(Object bean,
                                           ValidatorAction va, Field field,
                                           ActionMessages errors,
                                           Validator validator,
                                           HttpServletRequest request) 
    {
        boolean retval = true;
        String value = ValidatorUtils.getValueAsString(bean, field.getProperty());
        String propertyName = field.getVarValue("productType");
        String productType= ValidatorUtils.getValueAsString(bean, propertyName);
        
        if(StringUtils.isNotBlank(productType) && !StringUtils.equals(productType,ProductMaintConstants.NONE) ) {
            if(StringUtils.isBlank(value) || StringUtils.equals(value,ProductMaintConstants.NONE) ) {
                LOGGER.error("Failed validation validateProductSubType.  Product type was "+productType+".  Product sub type was "+value);
                errors.add(field.getKey(), Resources.getActionMessage(validator, request, va, field)); // struts 1.1 problem
    
                retval = false;
            }
        }
        
        return retval;
    }

    public static boolean validateDeliveryType(Object bean,
                                           ValidatorAction va, Field field,
                                           ActionMessages errors,
                                           Validator validator,
                                           HttpServletRequest request) 
    {
        boolean retval = true;
        String value = ValidatorUtils.getValueAsString(bean, field.getProperty());
        
        if(StringUtils.isBlank(value) || StringUtils.equals(value,ProductMaintConstants.NONE) ) {
            LOGGER.error("Failed validation validateDeliveryType.  Delivery type was "+value);
            errors.add(field.getKey(), Resources.getActionMessage(validator, request, va, field)); // struts 1.1 problem

            retval = false;
        }
        
        return retval;
    }
    
    public static boolean validateSecondChoice(Object bean,
                                           ValidatorAction va, Field field,
                                           ActionMessages errors,
                                           Validator validator,
                                           HttpServletRequest request) 
    {
        boolean retval = true;
        String value = ValidatorUtils.getValueAsString(bean, field.getProperty());
        String propertyName = field.getVarValue("productType");
        String productType= ValidatorUtils.getValueAsString(bean, propertyName);
        
        if( StringUtils.equalsIgnoreCase(productType,ProductMaintConstants.PRODUCT_TYPE_FLORAL) ) {
            if(StringUtils.isBlank(value) || StringUtils.equals(value,ProductMaintConstants.NONE) ) {
                LOGGER.error("Failed validation validateSecondChoice.  Product type was "+productType+".  Second choice was "+value);
                errors.add(field.getKey(), Resources.getActionMessage(validator, request, va, field)); // struts 1.1 problem
    
                retval = false;
            }
        }
        
        return retval;
    }
}
