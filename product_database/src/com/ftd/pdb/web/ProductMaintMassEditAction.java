package com.ftd.pdb.web;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.upload.FormFile;

import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.valobjs.MassEditFileStatusVO;
import com.ftd.pdb.common.valobjs.ProductVO;
import com.ftd.pdb.service.ILookupSVCBusiness;
import com.ftd.pdb.service.IProductMaintSVCBusiness;


public class ProductMaintMassEditAction extends PDBAction {
    /*Spring managed resources */
    ILookupSVCBusiness lookupSVC;
    IProductMaintSVCBusiness productSVC;
    /*end Spring managed resources */
    
    private String pdbMassEditFileDirectory = null;

    public ProductMaintMassEditAction() {
        super("com.ftd.pdb.web.ProductMaintMassEditAction");
    }

    public ActionForward execute(ActionMapping mapping, ActionForm form, 
                                 HttpServletRequest request, 
                                 HttpServletResponse response) throws IOException, 
                                                                      ServletException {
        if (logger.isDebugEnabled()) {
        	logger.debug(">>> begin ProductMaintMassEditAction") ;   
        }	
        ActionErrors errors = new ActionErrors();
        ProductMaintMassEditForm theForm = (ProductMaintMassEditForm)form;
        String formAction =  theForm.getFormAction();
        FormFile pdbMassEditFile  = (FormFile)theForm.get("pdbMassEditFile");
        refreshStatusList(request,errors);
        request.setAttribute("adminAction", "merchandising");
        
        if ("search".equalsIgnoreCase(formAction)) {
        	if (createSpreadSheet(theForm, request, response, errors) )
        		return null;
        	else {
        		saveErrors(request, errors);
        		return (mapping.getInputForward());
        	}	
        }	
        	
        else if ("upload".equalsIgnoreCase(formAction)) {
        	if (pdbMassEditFile != null) {
        		String fileName = pdbMassEditFile.getFileName();	
        		InputStream pdbMassEditFileIS = pdbMassEditFile.getInputStream();
        		String user = getUserId(request);
        		try {
        			productSVC.uploadProductMassEditSpreadSheet(fileName, user, pdbMassEditFileIS);
        		}
        		catch (Exception e) {
        			if ("INVALID_SPREADSHEET".equals(e.getMessage()) )
        				errors.add("global",new ActionError(String.valueOf(ProductMaintConstants.PDB_MASS_EDIT_INVALID_SPREADSHEET_FORMAT)));
        			else	
        				errors.add("global",new ActionError(String.valueOf(ProductMaintConstants.PDB_MASS_EDIT_ERROR_UPLOADING_SPREADSHEET)));
        			logger.error(">>> Error in upload of PDB Mass Edit spreadsheet");
        			e.printStackTrace();
        			saveErrors(request, errors);
        			return (mapping.getInputForward());
        		} 
        		 
        	}
        	else {
        		if (logger.isDebugEnabled())
        		   logger.debug("Mass Edit upload file is null");
        		errors.add("global",new ActionError(String.valueOf(ProductMaintConstants.PDB_MASS_EDIT_ERROR_NO_DATA_TO_UPLOAD)));
        		saveErrors(request, errors);
        		return (mapping.getInputForward());
        	}
        	
        }
        else if ("refresh".equalsIgnoreCase(formAction) || "reset".equalsIgnoreCase(formAction)  ) {
          refreshStatusList(request, errors);
          return (mapping.getInputForward()); 
        }
        else if ("showErrorReport".equalsIgnoreCase(formAction)) {
            if (showErrorReport(request, response, errors)) 
                return null;
            else {
        		saveErrors(request, errors);
        		return (mapping.getInputForward());
        	}	
         }
        else {
        	// Some unknown button magically got clicked on
        	errors.add("global",new ActionError(String.valueOf(ProductMaintConstants.PDB_MASS_EDIT_MAKE_A_SELECTION)));
        	saveErrors(request, errors);
           	return (mapping.getInputForward());
        }
        return (mapping.findForward("success"));
    }
    
    private boolean isNotNullOrEmpty(String str) {
    	if (str == null || str.isEmpty()) 
    		return false;
    	else 
    		return true;
    }
    
    /**
     * This will create the spreadsheet and download it in the response stream.
     * 
     * @param theForm
     * @param response
     * @param errors
     */
    private boolean createSpreadSheet(ProductMaintMassEditForm theForm, HttpServletRequest request, HttpServletResponse response, ActionErrors errors) {
    	if (logger.isDebugEnabled())
    		logger.debug(">>> begin createSpreadSheet");
    	XSSFWorkbook wb = null;
    	String[] searchItems = new String[5];
    	int count = 0;
    	String masterSku1 =  theForm.getMasterSku1();
        String masterSku2 =  theForm.getMasterSku2();
        String masterSku3 =  theForm.getMasterSku3();
        String masterSku4 =  theForm.getMasterSku4();
        String masterSku5 =  theForm.getMasterSku5();
        String product1   =  theForm.getProduct1();
        String product2   =  theForm.getProduct2();
        String product3   =  theForm.getProduct3();
        String product4   =  theForm.getProduct4();
        String product5   =  theForm.getProduct5();
      	try {
    		if (isNotNullOrEmpty(masterSku1) || isNotNullOrEmpty(masterSku2) || 
    			isNotNullOrEmpty(masterSku3) || isNotNullOrEmpty(masterSku4) ||
    			isNotNullOrEmpty(masterSku5)) {
    			// search by master sku
    			if (isNotNullOrEmpty(masterSku1)) {
    				searchItems[count] = masterSku1.trim();
    				count++;
    			}	
    			if (isNotNullOrEmpty(masterSku2)) {
    				searchItems[count] = masterSku2.trim();
    				count++;
    			}
    			if (isNotNullOrEmpty(masterSku3)) {
    				searchItems[count] = masterSku3.trim();
    				count++;
    			}
    			if (isNotNullOrEmpty(masterSku4)) {
    				searchItems[count] = masterSku4.trim();
    				count++;
    			}
    			if (isNotNullOrEmpty(masterSku5)) {
    				searchItems[count] = masterSku5.trim();
    				count++;
    			}
    			wb = productSVC.createProductMassEditSpreadsheet("M", searchItems);
        	}
    		else {
    			// search by product id
    			count = 0;
    			if (isNotNullOrEmpty(product1)) {
    				searchItems[count] = product1.trim();
    				count++;
    			}	
    			if (isNotNullOrEmpty(product2)) {
    				searchItems[count] = product2.trim();
    				count++;
    			}
    			if (isNotNullOrEmpty(product3)) {
    				searchItems[count] = product3.trim();
    				count++;
    			}
    			if (isNotNullOrEmpty(product4)) {
    				searchItems[count] = product4.trim();
    				count++;
    			}
    			if (isNotNullOrEmpty(product5)) {
    				searchItems[count] = product5.trim();
    				count++;
    			}
    			wb = productSVC.createProductMassEditSpreadsheet("P", searchItems);

    	
    		}
    	 if (wb != null) {	
    		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    		response.setHeader("Content-Disposition","attachment; filename=PDBMassEdit" + ".xlsx");
        	ServletOutputStream out = response.getOutputStream();                  
            try {   
                wb.write(out);                        
            } 
            catch (IOException e) {
                 e.printStackTrace();
            } 
            finally {
                 if (out != null) {
                     try {
                         out.flush();
                         out.close();
                     } catch (IOException e) {
                         e.printStackTrace();
                     }
                 }
           }
           return true; 
    	 }
    	 else {
     		// This should never happen unless some bug occurs somewhere
    		if (logger.isDebugEnabled()) 
    			logger.debug(">>> spreadsheet is null");
     		errors.add("global",new ActionError(String.valueOf(ProductMaintConstants.PDB_MASS_EDIT_NO_SEARCH_RESULTS)));
     		saveErrors(request, errors);
     		return false;
     	 }
     	}
     	catch(Exception e) {
     		errors.add("global",new ActionError(String.valueOf(ProductMaintConstants.PDB_MASS_EDIT_ERROR_CREATING_SPREADSHEET)));
       		logger.error(">>> Could not create Mass Edit Spreadsheet due to the following error: " + e.getMessage());
     	   	e.printStackTrace();  
     	    saveErrors(request, errors);
     	  	return false;
     	}
     	 
     
    }
    
    /**
     * This method will get the data for the Mass Edit File Upload status and store
     * the files in the request.
     * 
     * @param request
     * @param errors
     */
    private void refreshStatusList(HttpServletRequest request, ActionErrors errors) {
    	 try {	
       	  	HashMap<String, Object> pdbHashMap = productSVC.getMassEditFileStatus();
       	  	pdbMassEditFileDirectory = (String) pdbHashMap.get("pdbMassEditFileDirectory");
       	  	List<MassEditFileStatusVO> pdbMassEditFileStatusList = 
       	  		(List<MassEditFileStatusVO>) pdbHashMap.get("pdbMassEditFileStatusList");
       	  	request.setAttribute("pdbMassEditFileDirectory", pdbMassEditFileDirectory);
       	  	request.setAttribute("pdbMassEditFileStatusList", pdbMassEditFileStatusList);
          }
          catch(Exception e) {
        	  errors.add("global",new ActionError(String.valueOf(ProductMaintConstants.PDB_MASS_EDIT_ERROR_GETTING_FILE_STATUS_DATA)));
        	  logger.error(">>>> Error trying to get PDB Mass Edit File Status data");
        	  e.printStackTrace();
        	  saveErrors(request, errors);
         }
    }
    
    private boolean showErrorReport(HttpServletRequest request, HttpServletResponse response, ActionErrors errors) {
   	 	String fileName = request.getParameter("fileName");
    	try {
            FileInputStream  errorFileIS = new FileInputStream(new File(pdbMassEditFileDirectory,fileName));
            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    		response.setHeader("Content-Disposition","attachment; filename=" + fileName);
        	ServletOutputStream out = response.getOutputStream();                  
            try {
            	byte[] buffer = new byte[1024];
            	while(true) {
            	    int bytesRead = errorFileIS.read(buffer);
            	    if (bytesRead < 0)
            	      break;
            	    out.write(buffer, 0, bytesRead);
            	}
                                 
            } 
            catch (IOException e) {
                 e.printStackTrace();
            } 
            finally {
                 if (out != null) {
                     try {
                    	 errorFileIS.close();
                         out.flush();
                         out.close();
                     } catch (IOException e) {
                         e.printStackTrace();
                     }
                 }
           }
           return true;
         }
        catch(Exception e) {
       	  errors.add("global",new ActionError(String.valueOf(ProductMaintConstants.PDB_MASS_EDIT_GET_ERROR_SPREADSHEET_ERROR)));
       	  logger.error(">>>> Error trying to get PDB Mass Edit Error Spreadsheet.  File=" + fileName);
       	  e.printStackTrace();
       	  saveErrors(request, errors);
       	  return false;
        }
   }
   
  
    public void setLookupSVC(ILookupSVCBusiness lookupSVC) {
        this.lookupSVC = lookupSVC;
    }
     
    public ILookupSVCBusiness getLookupSVC() {
        return lookupSVC;
    }
   
    public void setProductSVC(IProductMaintSVCBusiness productSVC) {
        this.productSVC = productSVC;
    }

    public IProductMaintSVCBusiness getProductSVC() {
        return productSVC;
    }

	public String getPdbMassEditFileDirectory() {
		return pdbMassEditFileDirectory;
	}

	public void setPdbMassEditFileDirectory(String pdbMassEditFileDirectory) {
		this.pdbMassEditFileDirectory = pdbMassEditFileDirectory;
	}
    
}

