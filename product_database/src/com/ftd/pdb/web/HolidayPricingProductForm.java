package com.ftd.pdb.web;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;


/**
 * Form bean for the holiday pricing activate page.  This form has the following
 * fields, with default values in square brackets:
 *
 * @author Ed Mueller
 */
public final class

HolidayPricingProductForm extends PDBActionForm {
    public HolidayPricingProductForm() {
        super("com.ftd.pdb.web.HolidayPricingProductForm");
    }

    /**
     * Reset all properties to their default values.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     * @deprecated Holiday Pricing is unused and will be removed in the future.
     *   lpuckett 08/16/2006
     */
    public void reset(ActionMapping mapping, HttpServletRequest request) {
        set("productID", "");
        set("newPrice", "");
        set("newSKU", "");
        set("startDate", new java.util.Date().toString());
        set("endDate", null);
    }


    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     * @deprecated Holiday Pricing is unused and will be removed in the future.
     *   lpuckett 08/16/2006
     */
    public ActionErrors validate(ActionMapping mapping, 
                                 HttpServletRequest request) {

        ActionErrors errors = super.validate(mapping, request);

        //validate price
        String testString = (String)get("newPrice");
        if (StringUtils.isBlank(testString)) {
            errors.add("newPrice", 
                       new ActionMessage("error.newprice.required"));
        }

        try {
            new Double(testString);
        } catch (NumberFormatException e) {
            errors.add("newPrice", 
                       new ActionMessage("error.newprice.invalidprice"));
        }

        //sku
        testString = (String)get("newPrice");
        if (StringUtils.isBlank(testString)) {
            errors.add("newSKU", new ActionMessage("error.newsku.required"));
        }

        return errors;
    }
}
