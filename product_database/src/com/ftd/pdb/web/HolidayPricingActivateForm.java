package com.ftd.pdb.web;

import java.text.DateFormat;
import java.text.ParseException;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;


/**
 * Form bean for the holiday pricing activate page.  This form has the following
 * fields, with default values in square brackets:
 * @author Ed Mueller
 */
public final class

HolidayPricingActivateForm extends PDBActionForm {

    public HolidayPricingActivateForm() {
        super("com.ftd.pdb.web.HolidayPricingActivateForm");
    }


    /**
     * Reset all properties to their default values.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     * @deprecated Holiday Pricing is unused and will be removed in the future.
     *   lpuckett 08/16/2006
     */
    public void reset(ActionMapping mapping, HttpServletRequest request) {
        set("holidayPriceFlag", Boolean.FALSE);
        set("startDate", null);
        set("endDate", null);
        set("deliveryDateFlag", Boolean.FALSE);
    }


    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     * @deprecated Holiday Pricing is unused and will be removed in the future.
     *   lpuckett 08/16/2006
     */
    public ActionErrors validate(ActionMapping mapping, 
                                 HttpServletRequest request) {

        ActionErrors errors = new ActionErrors();
        Date enteredStartDate = null;
        Date enteredEndDate = null;

        DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT);
        df.setLenient(false);

        //validate start date
        String startDateString = (String)get("startDate");
        if (StringUtils.isBlank(startDateString)) {
            errors.add("startdate", 
                       new ActionMessage("error.startdate.required"));
        } else {
            try {
                enteredStartDate = df.parse(startDateString);
            } catch (ParseException e) {
                errors.add("startdate", 
                           new ActionMessage("error.startdate.invaliddate"));
            }
        }

        //validate end date
        String endDateString = (String)get("endDate");
        if (StringUtils.isBlank(endDateString)) {
            errors.add("enddate", new ActionMessage("error.enddate.required"));
        } else {
            try {
                enteredEndDate = df.parse(endDateString);
            } catch (ParseException e) {
                errors.add("enddate", 
                           new ActionMessage("error.enddate.invaliddate"));
            }
        }

        //if entered start and end dates are valid dates, make sure start < end date
        if (!(enteredStartDate == null || enteredEndDate == null)) {
            if (enteredStartDate.compareTo(enteredEndDate) > 0) {
                errors.add("enddate", 
                           new ActionMessage("error.startenddate.daterange"));

            }
        }


        return errors;
    }
}
