package com.ftd.pdb.web;

import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.exceptions.PDBException;
import com.ftd.pdb.common.valobjs.ProductVO;
import com.ftd.pdb.service.IProductMaintSVCBusiness;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;


public class UpsellListAction extends PDBAction {
    /*Spring managed resources */
    private IProductMaintSVCBusiness productSVC;
    /*end Spring managed resources */

    public UpsellListAction() {
        super("com.ftd.pdb.web.UpsellListAction");
    }

    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param mapping The ActionMapping used to select this instance
     * @param form The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, 
                                 HttpServletRequest request, 
                                 HttpServletResponse response) throws IOException, 
                                                                      ServletException {

        boolean errorInd = false;

        /* Validate the request parameters specified by the user.
         * Created ActionMessages as opposed to ActionErrors to avoid conflicts
         * since the ActionForm.validate method had already created an errors object.  
         */ 
        ActionMessages messages = new ActionMessages();

        UpsellListForm upsellListForm = (UpsellListForm)form;
        String sku = (String)upsellListForm.get("productId");
        request.setAttribute(ProductMaintConstants.OE_PRODUCT_ID_KEY, sku);

        //Make sure the entered Master SKU does not exist in the product database
        // Call the Service layer to get the upsell list
        try {
            //get list of  upsell products
            ProductVO productVO = productSVC.getProduct(sku);
            if (productVO != null && productVO.getProductId() != null) {
                errorInd = true;
                messages.add("productId",new ActionMessage(String.valueOf(ProductMaintConstants.UPSELL_MASTER_SKU_EXISTS_IN_PRODUCT_MASTER)));
            }

        } catch (PDBException ftdException) {
            return super.handleException(request, mapping, ftdException);
        }


        // Forward control to the specified success URI
        if (errorInd) {
            saveMessages(request, messages);
            return (mapping.findForward("error"));

        } else {
/*
            UpsellDetailForm inputform = new UpsellDetailForm();
            inputform.set("masterSKU",sku);
            request.setAttribute(mapping.getAttribute(), inputform);
*/
            return (mapping.findForward("success"));
        }
    }

    public void setProductSVC(IProductMaintSVCBusiness productSVC) {
        this.productSVC = productSVC;
    }

    public IProductMaintSVCBusiness getProductSVC() {
        return productSVC;
    }
}
