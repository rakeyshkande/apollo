package com.ftd.pdb.web;

import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.exceptions.PDBException;
import com.ftd.pdb.common.valobjs.HolidayPricingVO;
import com.ftd.pdb.service.IProductMaintSVCBusiness;

import java.io.IOException;

import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * Implementation of <strong>Action</strong> that processes a
 * holiday pricing flow.
 *
 * @author Ed Mueller
 */

public final class HolidayPricingFlowAction extends PDBAction
{
    /*Spring managed resources */
    private IProductMaintSVCBusiness productSVC;
    /*end Spring managed resources */
        
    /**
     * Constructor
     */
    public HolidayPricingFlowAction()
    {
        super("com.ftd.pdb.web.HolidayPricingFlowAction");
    }

	/**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param mapping The ActionMapping used to select this instance
     * @param form The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     * @deprecated Holiday Pricing is unused and will be removed in the future.
     *   lpuckett 08/16/2006
     */
    public ActionForward execute(ActionMapping mapping,
				 ActionForm form,
				 HttpServletRequest request,
				 HttpServletResponse response)
	throws IOException, ServletException
    {
    
        HolidayPricingActivateForm holidayForm = (HolidayPricingActivateForm) form;

        try {
            HolidayPricingVO holidayPricingVO = productSVC.getHolidayPricing();

            //create object to format dates
            SimpleDateFormat sdf = new SimpleDateFormat(ProductMaintConstants.PDB_DATE_FORMAT);

            //Populate form based on data from DB
            holidayForm.set("startDate",sdf.format(holidayPricingVO.getStartDate()));
            holidayForm.set("endDate",sdf.format(holidayPricingVO.getEndDate()));
            holidayForm.set("holidayPriceFlag",holidayPricingVO.getHolidayPriceFlag());
            holidayForm.set("heliveryDateFlag",holidayPricingVO.getDeliveryDateFlag());

            //Place form in session
            HttpSession session = request.getSession();
            if ("request".equals(mapping.getScope()))
                request.setAttribute(mapping.getAttribute(), holidayForm);
            else
                session.setAttribute(mapping.getAttribute(), holidayForm);

        }
        catch(PDBException pdbException) {
            return super.handleException(request,mapping,pdbException);
        }


        // Forward control to the specified success URI
        return (mapping.findForward("success"));
    }

    public void setProductSVC(IProductMaintSVCBusiness productSVC) {
        this.productSVC = productSVC;
    }

    public IProductMaintSVCBusiness getProductSVC() {
        return productSVC;
    }
}
