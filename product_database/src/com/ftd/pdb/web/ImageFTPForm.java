package com.ftd.pdb.web;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;


/**
 * Form bean for the holiday pricing activate page.  This form has the following
 * fields, with default values in square brackets:
 *
 * @author Ed Mueller
 */
public final class

ImageFTPForm extends PDBActionForm {

    public ImageFTPForm() {
        super("com.ftd.pdb.web.ImageFTPForm");
    }

    /**
     * Reset all properties to their default values.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public void reset(ActionMapping mapping, HttpServletRequest request) {
    }


    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public ActionErrors validate(ActionMapping mapping, 
                                 HttpServletRequest request) {

        ActionErrors errors = super.validate(mapping,request);

//        if(imagePaths == null || imagePath.getFileName().equals(ProductMaintConstants.BLANK_STRING))
//        {
//            errors.add(ProductMaintConstants.APPLICATION_CATEGORY_CONSTANT_2, new ActionError(String.valueOf(ProductMaintConstants.EMPTY_IMAGE_PATH_EXCEPTION)));
//        }

        return errors;
    }

}
