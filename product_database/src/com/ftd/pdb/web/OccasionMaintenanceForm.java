package com.ftd.pdb.web;

import com.ftd.pdb.common.ProductMaintConstants;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;


/**
 * Form bean for the Occasion Maintenance page.  This form has the following
 * fields, with default values in square brackets:
 *
 * @author Ed Mueller
 */
public final class

OccasionMaintenanceForm extends PDBActionForm {

    public OccasionMaintenanceForm() {
        super(ProductMaintConstants.PDB_OCCASION_MAINTENANCE_CATEGORY_NAME);
    }

    /**
     * Reset all properties to their default values.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public void reset(ActionMapping mapping, HttpServletRequest request) {

    }


    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public ActionErrors validate(ActionMapping mapping, 
                                 HttpServletRequest request) {

        ActionErrors errors = super.validate(mapping, request);
        /*
        //validate price
        if ((newPrice == null) || (newPrice.equalsIgnoreCase("")))
          {
            errors.add("newPrice", new ActionError("error.newprice.required"));
          }
        try{
            new Double(newPrice);
           }
        catch(NumberFormatException e) {
           errors.add("newPrice",new ActionError("error.newprice.invalidprice"));
           }

        //sku
        if ((newPrice == null) || (newSKU.equalsIgnoreCase("")))
          {
            errors.add("newSKU", new ActionError("error.newsku.required"));
          }
*/
        return errors;
    }
}
