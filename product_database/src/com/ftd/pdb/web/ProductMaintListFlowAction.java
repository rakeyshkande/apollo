package com.ftd.pdb.web;

import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.exceptions.PDBException;
import com.ftd.pdb.service.ILookupSVCBusiness;
import com.ftd.pdb.service.IProductBatchSVCBusiness;

import java.io.IOException;

import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


public class ProductMaintListFlowAction extends PDBAction {
    /*Spring managed resources */
    IProductBatchSVCBusiness productBatchSVC;
    ILookupSVCBusiness lookupSVC;
    /*end Spring managed resources */

    public ProductMaintListFlowAction() {
        super("com.ftd.pdb.struts.ProductMaintListSetupAction");
    }

    public ActionForward execute(ActionMapping mapping, ActionForm form, 
                                 HttpServletRequest request, 
                                 HttpServletResponse response) throws IOException, 
                                                                      ServletException {
        HttpSession session = request.getSession();

        //ProductMaintListForm productListForm = new ProductMaintListForm();
        //??? Commented out original line above in favor of line below.  This gets form
        // data if we return to this form.  Was concerned about initial entry to this
        // page since form would not exist - but it seems to work.
        ProductMaintListForm productListForm = (ProductMaintListForm)form;

        List productBatch = null;
        productListForm.set("productId", "");
        productListForm.set("searchBy", "searchByProdId");
        try{
            session.setAttribute(ProductMaintConstants.COLORS_KEY, (List)lookupSVC.getColorsList());
        } catch (Exception e) {
            return super.handleException(request, mapping, e);
        }

        // Call the Service layer to get the product batch info

        try {
            productBatch = productBatchSVC.getUserBatch(getUserId(request));
        } catch (PDBException e) {
            logger.error(e);
            return super.handleException(request, mapping, e);
        }

        request.setAttribute("productBatch", productBatch);

        // ??? Needed these when doing "new ProductMaintListForm()" above, but
        // don't appear to need after replacing with "(ProductMaintListForm)form".
        //request.setAttribute("productBatch", null);

        //request.setAttribute(mapping.getAttribute(), productListForm);

        // Forward control to the specified success URI
        return (mapping.findForward("success"));
    }

    public void setProductBatchSVC(IProductBatchSVCBusiness productBatchSVC) {
        this.productBatchSVC = productBatchSVC;
    }

    public IProductBatchSVCBusiness getProductBatchSVC() {
        return productBatchSVC;
    }

    public void setLookupSVC(ILookupSVCBusiness lookupSVC) {
        this.lookupSVC = lookupSVC;
    }

    public ILookupSVCBusiness getLookupSVC() {
        return lookupSVC;
    }

}
