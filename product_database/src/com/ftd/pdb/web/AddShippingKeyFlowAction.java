package com.ftd.pdb.web;

import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.valobjs.ShippingKeyPriceVO;
import com.ftd.pdb.common.valobjs.ShippingKeyVO;
import com.ftd.pdb.common.valobjs.ShippingMethodVO;
import com.ftd.pdb.service.IShippingKeySVCService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


public final class AddShippingKeyFlowAction extends PDBAction {
    /* Spring managed resources */
    private IShippingKeySVCService shippingKeySVC;
    /* end Spring managed resources */

    public AddShippingKeyFlowAction() {
        super("com.ftd.pdb.web.AddShippingKeyFlowAction");
    }

    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param mapping The ActionMapping used to select this instance
     * @param form The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     */
    public

    ActionForward execute(ActionMapping mapping, ActionForm form, 
                          HttpServletRequest request, 
                          HttpServletResponse response) throws IOException, 
                                                               ServletException {

        ShippingKeyDetailForm shippingKeyDetailForm = (ShippingKeyDetailForm)form;
        try {
            List list = shippingKeySVC.getShippingKeyList();

            int max_num = 1;
            for (int i = 0; i < list.size(); i++) {
                ShippingKeyVO vo = (ShippingKeyVO)list.get(i);
                int temp = Integer.parseInt(vo.getShippingKey_ID());
                max_num = Math.max(max_num, temp);
            }

            //addShippingKeyForm.setShippingKey((max_num + 1) + "");
            shippingKeyDetailForm.set("shippingKey", (max_num + 1) + "");

            ShippingKeyVO shippingKeyVO = new ShippingKeyVO();
            shippingKeyVO.setShippingKey_ID((String)shippingKeyDetailForm.get("shippingKey"));

            List prices = new ArrayList();
            for (int i = 0; i < ShippingKeyDetailForm.NUMBER_OF_ROW; i++) {
                ShippingKeyPriceVO vo = new ShippingKeyPriceVO();
                vo.setMinPrice(new Double(0));
                vo.setMaxPrice(new Double(0));
                List methods = new ArrayList();
                ShippingMethodVO methodVO = new ShippingMethodVO();
                methodVO.setId(ProductMaintConstants.SHIPPING_STANDARD_CODE);
                methods.add(methodVO);
                methodVO = new ShippingMethodVO();
                methodVO.setId(ProductMaintConstants.SHIPPING_TWO_DAY_CODE);
                methods.add(methodVO);
                methodVO = new ShippingMethodVO();
                methodVO.setId(ProductMaintConstants.SHIPPING_NEXT_DAY_CODE);
                methods.add(methodVO);
                methodVO = new ShippingMethodVO();
                methodVO.setId(ProductMaintConstants.SHIPPING_SATURDAY_CODE);
                methods.add(methodVO);
                methodVO = new ShippingMethodVO();
                methodVO.setId(ProductMaintConstants.SHIPPING_SUNDAY_CODE);
                methods.add(methodVO);
                methodVO = new ShippingMethodVO();
                methodVO.setId(ProductMaintConstants.SHIPPING_SAME_DAY_CODE);
                methods.add(methodVO);
                vo.setShippingMethods(methods);

                prices.add(vo);
            }
            shippingKeyDetailForm.set("itemDetailList", prices);
        } catch (Exception e) {
            logger.error(e);
            return super.handleException(request, mapping, e);
            
        }

        try {


            shippingKeyDetailForm.set("chkContent", true);
            shippingKeyDetailForm.set("chkLive", true);
            shippingKeyDetailForm.set("chkTest", true);
            shippingKeyDetailForm.set("chkUAT", true);

            request.setAttribute(mapping.getAttribute(), shippingKeyDetailForm);
    
            // Set the back link
            request.setAttribute(ProductMaintConstants.MENU_BACK_KEY, 
                                 ProductMaintConstants.MENU_BACK_SHIPPING_KEY_KEY);
            request.setAttribute(ProductMaintConstants.MENU_BACK_TEXT_KEY, 
                                 ProductMaintConstants.MENU_BACK_TEXT_SHIPPING_KEY_KEY);
        }
        catch (Throwable t) 
        {
            logger.error(t);
        }
        return (mapping.findForward("success"));
    }

    public void setShippingKeySVC(IShippingKeySVCService shippingKeySVC) {
        this.shippingKeySVC = shippingKeySVC;
    }

    public IShippingKeySVCService getShippingKeySVC() {
        return shippingKeySVC;
    }
}
