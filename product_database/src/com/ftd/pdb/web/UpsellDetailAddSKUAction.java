package com.ftd.pdb.web;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.exceptions.PDBException;
import com.ftd.pdb.common.valobjs.ProductVO;
import com.ftd.pdb.common.valobjs.UpsellDetailVO;
import com.ftd.pdb.common.valobjs.UpsellMasterVO;
import com.ftd.pdb.common.valobjs.ValueVO;
import com.ftd.pdb.service.ILookupSVCBusiness;
import com.ftd.pdb.service.IProductMaintSVCBusiness;
import com.ftd.pdb.service.IUpsellSVCBusiness;

import java.io.IOException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.Globals;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;


/**
 * Implementation of <strong>Action</strong> that processes a produst
 * list request.
 *
 * @author Ed Mueller
 */
public class UpsellDetailAddSKUAction  extends PDBAction
{
    /*Spring managed resources */
    private ILookupSVCBusiness lookupSVC;
    private IProductMaintSVCBusiness productSVC;
    private IUpsellSVCBusiness upsellSVC;
    /*end Spring managed resources */

    protected static Logger logger = new Logger("com.ftd.pdb.web.UpsellDetailAddSKUAction");
    
    public UpsellDetailAddSKUAction()
    {
        super("com.ftd.pdb.web.UpsellDetailAddSKUAction");
    }
	/**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param mapping The ActionMapping used to select this instance
     * @param form The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     */

    public ActionForward execute(ActionMapping mapping,
				 ActionForm form,
				 HttpServletRequest request,
				 HttpServletResponse response)
	throws IOException, ServletException
	{
            ProductVO productVO = null;
            ActionErrors errors = new ActionErrors();
             boolean errorFound = false;
             UpsellDetailForm inputForm = null;
             List origArray = new ArrayList();             
        List detailArray = new ArrayList();
            
    try{
   
        //get form object
        inputForm = (UpsellDetailForm)form;
        String newSKU = StringUtils.upperCase((String)inputForm.get("newSKU"));

        // Get all values needed to prepopulate the form      
         request.setAttribute(ProductMaintConstants.RECIPIENT_SEARCH_KEY, lookupSVC.getRecipientSearchList());
         request.setAttribute(ProductMaintConstants.SEARCH_PRIORITY_KEY, lookupSVC.getSearchPriorityList());
         request.setAttribute(ProductMaintConstants.PDB_COMPANY_MASTER_KEY, lookupSVC.getCompanyMasterList());

        // Set the back link
        request.setAttribute(ProductMaintConstants.MENU_BACK_KEY, ProductMaintConstants.MENU_BACK_UPSELL_LIST_KEY);
        request.setAttribute(ProductMaintConstants.MENU_BACK_TEXT_KEY, ProductMaintConstants.MENU_BACK_TEXT_UPSELL_LIST_KEY);

        UpsellMasterVO upsellMasterVO = new UpsellMasterVO();               

        //Builder MasterVo using data on request
        upsellMasterVO.setMasterID((String)inputForm.get("masterSKU"));
        //upsellMasterVO.setMasterStatus((Boolean)inputForm.get("arrangementAvailable"));
        upsellMasterVO.setMasterStatus(BooleanUtils.toBoolean((Boolean)inputForm.get("arrangementAvailable")));
        upsellMasterVO.setMasterName((String)inputForm.get("productName"));
        upsellMasterVO.setMasterDescription((String)inputForm.get("description"));
        String[] companyArray = (String[])inputForm.get("companyArray");
        upsellMasterVO.setCompanyList(companyArray);

         //Get detail SKUs from request
         //get serfvice
          try{
                detailArray = upsellSVC.getSKUsFromRequest(request);
                origArray =  upsellSVC.getSKUsFromRequest(request); //save original array in case of errors
                }
            catch(PDBException ftdException)    {  
                return super.handleException(request, mapping, ftdException);
                }           

                // Get the ProductVO for the new SKU
                productVO = productSVC.getProduct(newSKU);

                //add new sku to array if product found in database 
                //Issue 7028 - NPE. Previously, in the ProductDAOImpl, ProductVO was instantiated ONLY if a product record was found.  With 
                //Addon changes, this logic was changed, and thus, the DAO may return an empty but not a NULL VO. Thus, added the check for 
                //productId != null
                if (productVO != null && productVO.getProductId() != null) 
                {
                            UpsellDetailVO detailVO = new UpsellDetailVO();
                            detailVO.setDetailID(newSKU);
                            detailVO.setName(productVO.getProductName());
                            detailVO.setPrice(productVO.getStandardPrice());
                            detailVO.setType(productVO.getProductType());
                            detailVO.setSequence(detailArray.size());
                            detailVO.setAvailable(productVO.isStatus());
                            detailVO.setNovatorID(productVO.getNovatorId());
                            detailVO.setSentToNovatorProd(productVO.isSentToNovatorProd());
                            detailVO.setSentToNovatorContent(productVO.isSentToNovatorContent());
                            detailVO.setSentToNovatorUAT(productVO.isSentToNovatorUAT());
                            detailVO.setSentToNovatorTest(productVO.isSentToNovatorTest());                            
                            //make sure is not null
                            if (detailVO.getType() == null){
                                detailVO.setType("");
                                }
                            
                            detailArray.add(detailVO);
                }
                upsellMasterVO.setUpsellDetailList(detailArray);    
                request.setAttribute(ProductMaintConstants.SHOW_ASSOCIATE_LIST_KEY,detailArray);     



        /* Validate that SKU is a valid Upsell SKU */
        //SKU must exist in product database
        if (productVO == null){
                //check if user entered an upsell code
                List subcodeList = productSVC.getSubcodesByID(newSKU);
                if (subcodeList != null && subcodeList.size() > 0){
                            //product must exist in product master database
                            errors.add("newsku", new ActionMessage(String.valueOf(ProductMaintConstants.UPSELL_SKU_IS_SUBCODE)));
                            errorFound = true;                    
                    }
                else{
                        //product must exist in product master database
                        errors.add("newsku", new ActionMessage(String.valueOf(ProductMaintConstants.UPSELL_SKU_NOT_IN_PRODUCT_DATABASE)));
                        errorFound = true;
                        }
                }
        else{               
                        //SKU cannot  have subcodes
                        if (productVO.getSubCodeList() != null && productVO.getSubCodeList().size() > 0){
                                errors.add("newsku", new ActionMessage(String.valueOf(ProductMaintConstants.UPSELL_PRODUCT_HAS_SUBCODES)));
                                errorFound = true;                
                        }


              }

        //sku cannot already exist as associate sku for this master sku
        for (int i = 0; i < origArray.size();i++){
                UpsellDetailVO detailVO = (UpsellDetailVO)origArray.get(i);
                if (detailVO.getDetailID().equals(newSKU)){
                    errors.add("newsku", new ActionMessage(String.valueOf(ProductMaintConstants.UPSELL_SKU_ALREADY_EXISTS)));
                    errorFound = true;
                    }
        }


        }
        catch(Exception e)
        {
            return super.handleException(request, mapping, e);
        }

            //ed mueller, 7/24/03 - fix bug when which occurs when adding sku to upsell
            //reset error flags
            request.setAttribute(ProductMaintConstants.PDB_PRODUCT_UPSELL_ERROR_DESC_KEY, new String[ProductMaintConstants.PCB_UPSELL_MAINT_ERROR_MAX_IDX]);
            request.setAttribute(ProductMaintConstants.PDB_PRODUCT_UPSELL_ASSOC_ID_ERROR_KEY, new String[ProductMaintConstants.UPSELL_MAX_ASSOC_SKU_AMOUNT]);
            request.setAttribute(ProductMaintConstants.PDB_PRODUCT_UPSELL_ASSOC_NAME_ERROR_KEY, new String[ProductMaintConstants.UPSELL_MAX_ASSOC_SKU_AMOUNT]);
            request.setAttribute(ProductMaintConstants.PDB_PRODUCT_UPSELL_ASSOC_PRICE_ERROR_KEY, new String[ProductMaintConstants.UPSELL_MAX_ASSOC_SKU_AMOUNT]);
            request.setAttribute(ProductMaintConstants.PDB_PRODUCT_UPSELL_ASSOC_TYPE_ERROR_KEY, new String[ProductMaintConstants.UPSELL_MAX_ASSOC_SKU_AMOUNT]);
            request.setAttribute(ProductMaintConstants.PDB_PRODUCT_UPSELL_ASSOC_STATUS_ERROR_KEY, new String[ProductMaintConstants.UPSELL_MAX_ASSOC_SKU_AMOUNT]);
            //end, ed 7/27/03

        
		// Forward control to the specified success URI
        if (errorFound) {
            request.setAttribute(ProductMaintConstants.SHOW_ASSOCIATE_LIST_KEY,origArray);     
            request.setAttribute(Globals.ERROR_KEY, errors);
            request.setAttribute(mapping.getAttribute(), form);
    		return (mapping.findForward("error"));
            }
        else{
            inputForm.set("newSKU","");
            request.setAttribute(mapping.getAttribute(), form);            

            List upsellDetailIdList = new ArrayList();
            ValueVO valueVO = new ValueVO();
            valueVO.setId("");
            valueVO.setDescription("");
            upsellDetailIdList.add(valueVO);

            for (int i = 0; i < detailArray.size(); i++) {
                valueVO = new ValueVO();
                valueVO.setId(((UpsellDetailVO)detailArray.get(i)).getDetailID());
                valueVO.setDescription(((UpsellDetailVO)detailArray.get(i)).getDetailID());
                upsellDetailIdList.add(valueVO);
            } //end for loop
            request.setAttribute("upsellDetailIdList", upsellDetailIdList);

    	    return (mapping.findForward("success"));
            }        
    }

    public void setLookupSVC(ILookupSVCBusiness lookupSVC) {
        this.lookupSVC = lookupSVC;
    }

    public ILookupSVCBusiness getLookupSVC() {
        return lookupSVC;
    }

    public void setProductSVC(IProductMaintSVCBusiness productSVC) {
        this.productSVC = productSVC;
    }

    public IProductMaintSVCBusiness getProductSVC() {
        return productSVC;
    }

    public void setUpsellSVC(IUpsellSVCBusiness upsellSVC) {
        this.upsellSVC = upsellSVC;
    }

    public IUpsellSVCBusiness getUpsellSVC() {
        return upsellSVC;
    }
}
