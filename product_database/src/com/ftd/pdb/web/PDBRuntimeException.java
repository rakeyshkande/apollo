package com.ftd.pdb.web;


import com.ftd.osp.utilities.plugins.Logger;

import org.apache.commons.lang.exception.NestableRuntimeException;

public class PDBRuntimeException extends NestableRuntimeException {
    Throwable exceptionCause = null;
    Logger logger = new Logger("com.ftd.pdb.struts.PDBRuntimeException");

    /** Creates a new instance of PDBRuntimeException */
    public PDBRuntimeException(String msg) {
        super(msg);
    }

    public PDBRuntimeException(String msg, Throwable exception){
      super(msg, exception);
      exceptionCause = exception;
    }

    /**Overriding the printStackTraceMethod*/
    public void printStackTrace(){
      if (exceptionCause!=null){
        logger.error(exceptionCause);
      }
    }
}
