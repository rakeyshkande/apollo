package com.ftd.pdb.web;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionServlet;
import org.apache.struts.upload.FormFile;

import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.exceptions.PDBException;
import com.ftd.pdb.common.valobjs.ProductVO;
import com.ftd.pdb.common.valobjs.UpsellMasterVO;
import com.ftd.pdb.service.IProductMaintSVCBusiness;
import com.ftd.pdb.service.IUpsellSVCBusiness;

public class ProductMaintMassEditForm extends PDBActionForm {
     
	private static final long serialVersionUID = 1L;
	/*Spring managed resources */
    private IProductMaintSVCBusiness productSVC;
    private IUpsellSVCBusiness upsellSVC;
    /*end Spring managed resources */
    
    private String formAction;
    private String masterSku1;
    private String masterSku2;
    private String masterSku3;
    private String masterSku4;
    private String masterSku5;
    private String product1;
    private String product2;
    private String product3;
    private String product4;
    private String product5;
    private FormFile pdbMassEditFile; 
    private ActionErrors errors = null;

	public ProductMaintMassEditForm() {
		super("com.ftd.pdb.web.ProductMaintMassEditForm");
    }
	
	public void setServlet(ActionServlet actionServlet) {
        super.setServlet(actionServlet);
        upsellSVC  = (IUpsellSVCBusiness)getWebApplicationContext().getBean("upsellSVC");
        productSVC = (IProductMaintSVCBusiness)getWebApplicationContext().getBean("productSVC");
    }
	
	/**
     * Reset all properties to their default values.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public void reset(ActionMapping mapping, HttpServletRequest request) {
         masterSku1 = "";
         masterSku2 = "";
         masterSku3 = "";
         masterSku4 = "";
         masterSku5 = "";
         product1 = "";
         product2 = "";
         product3 = "";
         product4 = "";
         product5 = "";
         errors = null;
    }
	    
    public ActionErrors validate(ActionMapping actionMapping, 
                                 HttpServletRequest request) {
    	
    	ActionErrors errors = super.validate(actionMapping, request);
    
        formAction = request.getParameter("formAction");
    	masterSku1 = request.getParameter("masterSku1");
        masterSku2 = request.getParameter("masterSku2");
        masterSku3 = request.getParameter("masterSku3");
        masterSku4 = request.getParameter("masterSku4");
        masterSku5 = request.getParameter("masterSku5");
        product1   = request.getParameter("product1");
        product2   = request.getParameter("product2");
        product3   = request.getParameter("product3");
        product4   = request.getParameter("product4");
        product5   = request.getParameter("product5");
        /* 
         * They have 2 options for searching.  They can either search by masterSku or
         * product Id.  
         */
        try {
        	if ("reset".equalsIgnoreCase(request.getParameter("formAction"))) {
        		reset(actionMapping, request);
            }
        	else if ("search".equalsIgnoreCase(request.getParameter("formAction"))) {
        		 
              if (isNotNullOrEmpty(masterSku1) || isNotNullOrEmpty(masterSku2) || isNotNullOrEmpty(masterSku3) ||
        		isNotNullOrEmpty(masterSku4) || isNotNullOrEmpty(masterSku5) ) {
        		// They are trying to search by master sku - validate the master skus entered
        		if (isNotNullOrEmpty(masterSku1)) {
        	   		UpsellMasterVO upsellVO = upsellSVC.getMasterSKU(masterSku1.trim());
        	   		if (upsellVO == null || upsellVO.getMasterID() == null) {
        	   			errors.add("masterSku1",new ActionError("*"));
        	   		}
        	   	}
        	   	if (isNotNullOrEmpty(masterSku2)) {
        	   		UpsellMasterVO upsellVO = upsellSVC.getMasterSKU(masterSku2.trim());
        	   		if (upsellVO == null || upsellVO.getMasterID() == null) {
        	   			errors.add("masterSku2",new ActionError("*"));
        	   		}
        	   	}
        	   	if (isNotNullOrEmpty(masterSku3)) {
        	   		UpsellMasterVO upsellVO = upsellSVC.getMasterSKU(masterSku3.trim());
        	   		if (upsellVO == null || upsellVO.getMasterID() == null) {
        	   			errors.add("masterSku3",new ActionError("*"));
        	   		}
        	   	}
        	   	if (isNotNullOrEmpty(masterSku4)) {
        	   		UpsellMasterVO upsellVO = upsellSVC.getMasterSKU(masterSku4.trim());
        	   		if (upsellVO == null || upsellVO.getMasterID() == null) {
        	   			errors.add("masterSku4",new ActionError("*"));
        	   		}
        	   	}
        	   	if (isNotNullOrEmpty(masterSku5)) {
        	   		UpsellMasterVO upsellVO = upsellSVC.getMasterSKU(masterSku5.trim());
        	   		if (upsellVO == null || upsellVO.getMasterID() == null) {
        	   			errors.add("masterSku5",new ActionError("*"));
        	   		}
        	   	}
        	   	if (errors.size() > 0) {
        	   		errors.add("global",new ActionError(String.valueOf(ProductMaintConstants.PDB_MASS_EDIT_INVALID_MASTER_SKU)));
        	   	}
        	}
        	else if (isNotNullOrEmpty(product1) || isNotNullOrEmpty(product2) || isNotNullOrEmpty(product3) ||
        			isNotNullOrEmpty(product4) || isNotNullOrEmpty(product5) ) {
            	// They are trying to search by product id - validate the product ids entered
            	if (isNotNullOrEmpty(product1)) {
            	   	ProductVO productVO = productSVC.getProduct(product1.trim());
            	   	if (productVO == null || productVO.getProductId() == null || !productVO.getProductId().toUpperCase().equals(product1.trim().toUpperCase())) {
            	   		errors.add("product1",new ActionError("*"));
            	   	}
            	   	else if ("NONE".equalsIgnoreCase(productVO.getProductType())) {
            	   		// new with revision 10 - don't allow product to be downloaded if product type = NONE
            	   		errors.add("product1",new ActionError("*"));
            	   	}
            	}
            	if (isNotNullOrEmpty(product2)) {
            	   	ProductVO productVO = productSVC.getProduct(product2.trim());
            	   	if (productVO == null || productVO.getProductId() == null || !productVO.getProductId().toUpperCase().equals(product2.trim().toUpperCase())) {
            	   		errors.add("product2",new ActionError("*"));
            	   	}
            		else if ("NONE".equalsIgnoreCase(productVO.getProductType())) {
            	   		// new with revision 10 - don't allow product to be downloaded if product type = NONE
            	   		errors.add("product2",new ActionError("*"));
            	   	}
            	}
            	if (isNotNullOrEmpty(product3)) {
            	   	ProductVO productVO = productSVC.getProduct(product3.trim());
            	   	if (productVO == null || productVO.getProductId() == null  || !productVO.getProductId().toUpperCase().equals(product3.trim().toUpperCase())) {
            	   		errors.add("product3",new ActionError("*"));
            	   	}
            	   	else if ("NONE".equalsIgnoreCase(productVO.getProductType())) {
            	   		// new with revision 10 - don't allow product to be downloaded if product type = NONE
            	   		errors.add("product3",new ActionError("*"));
            	   	}
            	}
            	if (isNotNullOrEmpty(product4)) {
            	   	ProductVO productVO = productSVC.getProduct(product4.trim());
            	   	if (productVO == null || productVO.getProductId() == null  || !productVO.getProductId().toUpperCase().equals(product4.trim().toUpperCase())) {
            	   		errors.add("product4",new ActionError("*"));
            	   	}
            	   	else if ("NONE".equalsIgnoreCase(productVO.getProductType())) {
            	   		// new with revision 10 - don't allow product to be downloaded if product type = NONE
            	   		errors.add("product4",new ActionError("*"));
            	   	}
            	}
            	if (isNotNullOrEmpty(product5)) {
            	   	ProductVO productVO = productSVC.getProduct(product5.trim());
            	   	if (productVO == null || productVO.getProductId() == null  || !productVO.getProductId().toUpperCase().equals(product5.trim().toUpperCase())) {
            	   		errors.add("product5",new ActionError("*"));
            	   	}
            	   	else if ("NONE".equalsIgnoreCase(productVO.getProductType())) {
            	   		// new with revision 10 - don't allow product to be downloaded if product type = NONE
            	   		errors.add("product5",new ActionError("*"));
            	   	}
            	}	
            	if (errors.size() > 0) {
            		errors.add("global",new ActionError(String.valueOf(ProductMaintConstants.PDB_MASS_EDIT_INVALID_PRODUCT_ID)));
             	}
        	}
        	else {
        		// They did not enter anything - display an error message
         		errors.add("global",new ActionError(String.valueOf(ProductMaintConstants.PDB_MASS_EDIT_MAKE_A_SELECTION)));
         	}
          }	

        } 
        catch (PDBException ftdException) {
     		errors.add("global",new ActionError(String.valueOf(ProductMaintConstants.PDB_MASS_EDIT_ERROR_CREATING_SPREADSHEET)));
            logger.error(">>> exception in PDB Mass Edit Form validation");
        	ftdException.printStackTrace();
        }
        return errors;
    }
    
    private boolean isNotNullOrEmpty(String str) {
    	if (str == null || str.isEmpty()) 
    		return false;
    	else 
    		return true;
    }

	public IProductMaintSVCBusiness getProductSVC() {
		return productSVC;
	}

	public void setProductSVC(IProductMaintSVCBusiness productSVC) {
		this.productSVC = productSVC;
	}

	public IUpsellSVCBusiness getUpsellSVC() {
		return upsellSVC;
	}

	public void setUpsellSVC(IUpsellSVCBusiness upsellSVC) {
		this.upsellSVC = upsellSVC;
	}

	public String getFormAction() {
		return formAction;
	}

	public void setFormAction(String formAction) {
		this.formAction = formAction;
	}

	public String getMasterSku1() {
		return masterSku1;
	}

	public void setMasterSku1(String masterSku1) {
		this.masterSku1 = masterSku1;
	}

	public String getMasterSku2() {
		return masterSku2;
	}

	public void setMasterSku2(String masterSku2) {
		this.masterSku2 = masterSku2;
	}

	public String getMasterSku3() {
		return masterSku3;
	}

	public void setMasterSku3(String masterSku3) {
		this.masterSku3 = masterSku3;
	}

	public String getMasterSku4() {
		return masterSku4;
	}

	public void setMasterSku4(String masterSku4) {
		this.masterSku4 = masterSku4;
	}

	public String getMasterSku5() {
		return masterSku5;
	}

	public void setMasterSku5(String masterSku5) {
		this.masterSku5 = masterSku5;
	}

	public String getProduct1() {
		return product1;
	}

	public void setProduct1(String product1) {
		this.product1 = product1;
	}

	public String getProduct2() {
		return product2;
	}

	public void setProduct2(String product2) {
		this.product2 = product2;
	}

	public String getProduct3() {
		return product3;
	}

	public void setProduct3(String product3) {
		this.product3 = product3;
	}

	public String getProduct4() {
		return product4;
	}

	public void setProduct4(String product4) {
		this.product4 = product4;
	}

	public String getProduct5() {
		return product5;
	}

	public void setProduct5(String product5) {
		this.product5 = product5;
	}

	public FormFile getPdbMassEditFile() {
		return pdbMassEditFile;
	}

	public void setPdbMassEditFile(FormFile pdbMassEditFile) {
		this.pdbMassEditFile = pdbMassEditFile;
	}

	public ActionErrors getErrors() {
		return errors;
	}

	public void setErrors(ActionErrors errors) {
		this.errors = errors;
	}
    
    
}

