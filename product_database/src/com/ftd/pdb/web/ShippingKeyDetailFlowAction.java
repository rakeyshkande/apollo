package com.ftd.pdb.web;

import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.valobjs.ProductVO;
import com.ftd.pdb.common.valobjs.ShippingKeyPriceVO;
import com.ftd.pdb.common.valobjs.ShippingKeyVO;
import com.ftd.pdb.common.valobjs.ShippingMethodVO;
import com.ftd.pdb.service.IShippingKeySVCService;

import java.io.IOException;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


public final class ShippingKeyDetailFlowAction extends PDBAction {
    /*Spring managed resources */
    private IShippingKeySVCService shippingKeySVC;
    /*end Spring managed resources */

    public ShippingKeyDetailFlowAction() {
        super("com.ftd.pdb.web.ShippingKeyDetailFlowAction");
    }

    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param mapping The ActionMapping used to select this instance
     * @param form The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     */
    public

    ActionForward execute(ActionMapping mapping, ActionForm form, 
                          HttpServletRequest request, 
                          HttpServletResponse response) throws IOException, 
                                                               ServletException {

        ShippingKeyDetailForm shippingKeyDetailForm = (ShippingKeyDetailForm)form;
        shippingKeyDetailForm.set("chkContent",true);
        shippingKeyDetailForm.set("chkLive",true);
        shippingKeyDetailForm.set("chkTest",true);
        shippingKeyDetailForm.set("chkUAT",true);
        String selectedShippingKeyID = (String)shippingKeyDetailForm.get("shippingKey");

        try {
            ShippingKeyVO shippingKeyVO = 
                shippingKeySVC.getShippingKey(selectedShippingKeyID);
            shippingKeyDetailForm.set("shipper", shippingKeyVO.getShipperID());
            shippingKeyDetailForm.set("description", shippingKeyVO.getShippingKeyDescription());

            // Run through each price adding missing delivery methods
            for (int i = 0; i < shippingKeyVO.getPrices().size(); i++) {
                ShippingKeyPriceVO vo = 
                    (ShippingKeyPriceVO)shippingKeyVO.getPrices().get(i);
                List list = vo.getShippingMethods();

                ShippingMethodVO compareVO = new ShippingMethodVO();
                compareVO.setId(ProductMaintConstants.SHIPPING_STANDARD_CODE);
                compareVO.setPriceId(vo.getShippingKeyPriceID());
                if (!list.contains(compareVO)) {
                    ShippingMethodVO methodVO = new ShippingMethodVO();
                    methodVO.setId(ProductMaintConstants.SHIPPING_STANDARD_CODE);
                    list.add(methodVO);
                }
                compareVO.setId(ProductMaintConstants.SHIPPING_TWO_DAY_CODE);
                compareVO.setPriceId(vo.getShippingKeyPriceID());
                if (!list.contains(compareVO)) {
                    ShippingMethodVO methodVO = new ShippingMethodVO();
                    methodVO.setId(ProductMaintConstants.SHIPPING_TWO_DAY_CODE);
                    list.add(methodVO);
                }
                compareVO.setId(ProductMaintConstants.SHIPPING_NEXT_DAY_CODE);
                compareVO.setPriceId(vo.getShippingKeyPriceID());
                if (!list.contains(compareVO)) {
                    ShippingMethodVO methodVO = new ShippingMethodVO();
                    methodVO.setId(ProductMaintConstants.SHIPPING_NEXT_DAY_CODE);
                    list.add(methodVO);
                }
                compareVO.setId(ProductMaintConstants.SHIPPING_SATURDAY_CODE);
                compareVO.setPriceId(vo.getShippingKeyPriceID());
                if (!list.contains(compareVO)) {
                    ShippingMethodVO methodVO = new ShippingMethodVO();
                    methodVO.setId(ProductMaintConstants.SHIPPING_SATURDAY_CODE);
                    list.add(methodVO);
                }
                compareVO.setId(ProductMaintConstants.SHIPPING_SUNDAY_CODE);
                compareVO.setPriceId(vo.getShippingKeyPriceID());
                if (!list.contains(compareVO)) {
                    ShippingMethodVO methodVO = new ShippingMethodVO();
                    methodVO.setId(ProductMaintConstants.SHIPPING_SUNDAY_CODE);
                    list.add(methodVO);
                }
                compareVO.setId(ProductMaintConstants.SHIPPING_SAME_DAY_CODE);
                compareVO.setPriceId(vo.getShippingKeyPriceID());
                if (!list.contains(compareVO)) {
                    ShippingMethodVO methodVO = new ShippingMethodVO();
                    methodVO.setId(ProductMaintConstants.SHIPPING_SAME_DAY_CODE);
                    list.add(methodVO);
                }
            }

            int remain_rows = ShippingKeyDetailForm.NUMBER_OF_ROW - shippingKeyVO.getPrices().size();
            for (int i = 0; i < remain_rows; i++) {
                ShippingKeyPriceVO vo = new ShippingKeyPriceVO();
                vo.setMinPrice(new Double(0));
                vo.setMaxPrice(new Double(0));
                List methods = new ArrayList();
                ShippingMethodVO methodVO = new ShippingMethodVO();
                methodVO.setId(ProductMaintConstants.SHIPPING_STANDARD_CODE);
                methods.add(methodVO);
                methodVO = new ShippingMethodVO();
                methodVO.setId(ProductMaintConstants.SHIPPING_TWO_DAY_CODE);
                methods.add(methodVO);
                methodVO = new ShippingMethodVO();
                methodVO.setId(ProductMaintConstants.SHIPPING_NEXT_DAY_CODE);
                methods.add(methodVO);
                methodVO = new ShippingMethodVO();
                methodVO.setId(ProductMaintConstants.SHIPPING_SATURDAY_CODE);
                methods.add(methodVO);
                methodVO = new ShippingMethodVO();
                methodVO.setId(ProductMaintConstants.SHIPPING_SUNDAY_CODE);
                methods.add(methodVO);
                methodVO = new ShippingMethodVO();
                methodVO.setId(ProductMaintConstants.SHIPPING_SAME_DAY_CODE);
                methods.add(methodVO);
                vo.setShippingMethods(methods);
                shippingKeyVO.getPrices().add(vo);
            }

            ArrayList productID_list = new ArrayList();
            ProductVO[] productVOs = shippingKeyVO.getProducts();
            if (productVOs != null) {
                for (int i = 0; i < productVOs.length; i++) {
                    productID_list.add(productVOs[i].getProductId());
                }
            }

            shippingKeyDetailForm.set("itemDetailList", shippingKeyVO.getPrices());
            request.setAttribute("productID_list", productID_list);

        } catch (Exception e) {
            return super.handleException(request, mapping, e);
        }

        request.setAttribute(mapping.getAttribute(), shippingKeyDetailForm);

        // Set the back link
        request.setAttribute(ProductMaintConstants.MENU_BACK_KEY, 
                             ProductMaintConstants.MENU_BACK_SHIPPING_KEY_KEY);
        request.setAttribute(ProductMaintConstants.MENU_BACK_TEXT_KEY, 
                             ProductMaintConstants.MENU_BACK_TEXT_SHIPPING_KEY_KEY);

        return (mapping.findForward("success"));
    }

    public void setShippingKeySVC(IShippingKeySVCService shippingKeySVC) {
        this.shippingKeySVC = shippingKeySVC;
    }

    public IShippingKeySVCService getShippingKeySVC() {
        return shippingKeySVC;
    }
}
