package com.ftd.pdb.web;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBException;
import com.ftd.pdb.common.utilities.PersonalizationTemplateHandler;
import com.ftd.pdb.common.valobjs.ProductBatchVO;
import com.ftd.pdb.common.valobjs.ProductVO;
import com.ftd.pdb.common.valobjs.UpsellMasterVO;
import com.ftd.pdb.common.valobjs.ValueVO;
import com.ftd.pdb.service.ILookupSVCBusiness;
import com.ftd.pdb.service.IProductBatchSVCBusiness;
import com.ftd.pdb.service.IProductMaintSVCBusiness;
import com.ftd.pdb.service.IUpsellSVCBusiness;

import java.io.IOException;

import java.io.PrintWriter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


public class ProductMaintAjaxAction extends PDBAction {
    /*Spring managed resources */
    IProductMaintSVCBusiness productSVC;
    ILookupSVCBusiness lookupSVC;
    /*end Spring managed resources */

    public ProductMaintAjaxAction() {
        super("com.ftd.pdb.struts.ProductMaintAjaxAction");
    }

    public ActionForward execute(ActionMapping mapping, ActionForm form, 
                                 HttpServletRequest request, 
                                 HttpServletResponse response) throws IOException, 
                                                                      ServletException {
        String actionType = null;
        try{
            actionType = request.getParameter("action_type"); 
            /* get action type request parameter */
            if(actionType!=null)
            {
                if(actionType.equals("validate_subcode"))
                {
                    processValidateSubcode(request, response);
                }
                else if(actionType.equals("get_template_order"))
                {
                    getPersonalizationTemplateOrder(request, response);
                }
            }
        } 
        catch(Exception e){
            logger.debug(e);
        }
        return null;                                                
    }

    /**
     * @param request http request object
     * @param response http response object
     * @throws Exception
     */
    private void processValidateSubcode(HttpServletRequest request, HttpServletResponse response)
      throws Exception
    {

      String responseText = "success";
      try 
      {
        String productId = request.getParameter("product_id");
        try 
        {
            // checks to see if the product id exists as a product id, subcode id, or novator id
            boolean productIdExists = productSVC.validateProductId(productId);
            if (productIdExists){
                responseText = "This subcode already exists in the system.  Please try again.";
            }
        } 
        catch (Exception e) 
        {
          responseText = "Error - Failed to check if subcode already exists.  Please try again.\r\n"+e.getMessage();
          logger.error("Failed to see if subcode exists in VENDOR PRODUCTS",e);
        }
      } 
      finally 
      {
        response.setContentType("text/plain");
        PrintWriter out = response.getWriter();
        out.println(responseText);
        out.flush();  
      }

    }

    /**
     * Obtains and returns personalization template order information
     * @param request http request object
     * @param response http response object
     * @throws Exception
     */
    private void getPersonalizationTemplateOrder(HttpServletRequest request, HttpServletResponse response)
      throws Exception
    {
      String responseText = null;
      StringBuffer data = new StringBuffer();
      
      try 
      {
        String templateId = request.getParameter("template_id");
        try 
        {
            // Obtains order for selected template
            List<ValueVO> list = lookupSVC.getPersonalizationTemplateOrder(templateId);

            // Set in the session as well as returning the data since PDB is tied to the session.
            request.getSession().setAttribute(ProductMaintConstants.PERSONALIZATION_TEMPLATE_ORDER_KEY, list); 
            
            for(Iterator<ValueVO> it = list.iterator(); it.hasNext();)
            {
              data.append(it.next().getId());
              data.append("_");
            }
            responseText = data.toString();
        } 
        catch (Exception e) 
        {
          responseText = "Error - Could not obtain Personalized Template order.  Please try again.\r\n"+e.getMessage();
          logger.error("Error - Could not obtain Personalized Template order.  Please try again.\r\n",e);
        }
      } 
      finally 
      {
        response.setContentType("text/plain");
        PrintWriter out = response.getWriter();
        out.println(responseText);
        out.flush();  
      }
  
    }

    public void setProductSVC(IProductMaintSVCBusiness productSVC) {
        this.productSVC = productSVC;
    }

    public IProductMaintSVCBusiness getProductSVC() {
        return productSVC;
    }

    public void setLookupSVC(ILookupSVCBusiness lookupSVC) {
        this.lookupSVC = lookupSVC;
    }
  
    public ILookupSVCBusiness getLookupSVC() {
        return lookupSVC;
    }
}
