package com.ftd.pdb.web;

import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.exceptions.PDBException;
import com.ftd.pdb.common.valobjs.UpsellDetailVO;
import com.ftd.pdb.common.valobjs.UpsellMasterVO;
import com.ftd.pdb.service.IUpsellSVCBusiness;

import java.io.IOException;

import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * Implementation of <strong>Action</strong> that processes a produst
 * list request.
 *
 * @author Ed Mueller
 */
public class UpsellListFlowAction extends PDBAction {
    /*Spring managed resources */
    private IUpsellSVCBusiness upsellSVC;
    /*end Spring managed resources */

    public UpsellListFlowAction() {
        super("com.ftd.pdb.web.UpsellListFlowAction");
    }

    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param mapping The ActionMapping used to select this instance
     * @param form The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, 
                                 HttpServletRequest request, 
                                 HttpServletResponse response) throws IOException, 
                                                                      ServletException {
        UpsellListForm upsellListForm = new UpsellListForm();
        List products = null;

        // Call the Service layer to get the upsell list
        try {
            products = upsellSVC.getUpsellList();

            //pad the detail lists to make sure it contains the min about
            for (int i = 0; i < products.size(); i++) {
                UpsellMasterVO upsellMasterVO = 
                    (UpsellMasterVO)products.get(i);
                List detailList = upsellMasterVO.getUpsellDetailList();
                int detailSize = detailList.size();
                for (int y = detailSize; 
                     y < ProductMaintConstants.PDB_UPSELL_DISPLAY_AMOUNT; 
                     y++) {
                    detailList.add(new UpsellDetailVO());
                }
            }
        } catch (PDBException ftdException) {
            return super.handleException(request, mapping, ftdException);
        }

        request.setAttribute("upsellList", products);
        request.setAttribute("productId", "");
        HttpSession session = request.getSession();
        session.setAttribute("upsellList", products);
        request.setAttribute(mapping.getAttribute(), upsellListForm);


        // Forward control to the specified success URI
        return (mapping.findForward("success"));
    }

    public void setUpsellSVC(IUpsellSVCBusiness upsellSVC) {
        this.upsellSVC = upsellSVC;
    }

    public IUpsellSVCBusiness getUpsellSVC() {
        return upsellSVC;
    }
}
