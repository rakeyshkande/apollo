package com.ftd.pdb.web;

import com.ftd.pdb.common.utilities.FTDUtil;
import com.ftd.pdb.common.utilities.UserInputValidator;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public final class ShippingKeyDetailForm extends PDBActionForm {

    public static final int NUMBER_OF_ROW = 8;
    private ActionErrors errors = new ActionErrors();
    private UserInputValidator validator = new UserInputValidator();


    public ShippingKeyDetailForm() {
        super("com.ftd.pdb.web.ShippingKeyDetailForm");
    }


    /**
     * Reset all properties to their default values.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public void reset(ActionMapping mapping, HttpServletRequest request) {
    }

    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public ActionErrors validate(ActionMapping mapping, 
                                 HttpServletRequest request) {
        
        //This boolean controls some of the validations.  
        //Blank rows are treated differently
        boolean [] rowHasData = new boolean[NUMBER_OF_ROW];
        
        //This boolean sanitizes the error output.  If there is a currency error
        //in the prices fields, the other checks on those fields are skipped.
        boolean [] rowHasError = new boolean[NUMBER_OF_ROW];

        try 
        {
            logger.info("Entering ShippingKeyDetailForm validation \nKey: " + getString("shippingKey") + "\nAction: " + getString("actionType"));
            errors = super.validate(mapping, request);


            //Start of Shipping Key Validation
             if ("1".equals(getString("shippingKey")) && "delete".equals(getString("actionType")) ) {
                 errors.add("shippingKey", new ActionMessage("error.shippingKeyDetail.cannotDeleteDefault", "Shipping Key"));
                 logger.error("Deletion of Shipping Key 1 failed because it is the default shipping key.");
             }
            
            if (validator.isShippingKeyHasProducts(getString("shippingKey")) && "delete".equals(getString("actionType")) )
            {
                errors.add("shippingKey", new ActionMessage("error.shippingKeyDetail.cannotDeleteKeyWithProducts", getString("shippingKey")) );
                logger.error("Deletion of Shipping Key" + getString("shippingKey") + "failed because it has products associated with it.");           
            }
            
            String testString = getString("shippingKey");
            if (StringUtils.isBlank(testString)) {
                errors.add("shippingKey", 
                           new ActionMessage("error.shippingKeyDetail.nullInput", 
                                             "Shipping Key"));
                logger.info("Validation of shippingkey " + testString + " failed because it is blank.");
            }
            
            //Start of Description Validation
            testString = getString("description");
            if (StringUtils.isBlank(testString)) {
                errors.add("shippingKey", 
                           new ActionMessage("error.shippingKeyDetail.nullInput", 
                                             "Description"));
                logger.info("Validation of description " + testString + " failed because it is blank.");
            }
            if (testString.length() > 25) {
                errors.add("shippingKey", new ActionMessage("error.shippingKeyDetail.tooLong", "Description"));
                logger.info("Validation of description " + testString + " failed because it is too long.");
                
            }
            
            if ( "add".equals(getString("actionType")) && validator.isDuplicateDescriptionAdd(testString) ) 
            {
                errors.add("shippingKey", new ActionMessage("error.shippingKeyDetail.duplicate", "Description"));
                logger.info("Attempted to add duplicate description in the database " + testString + " already exists.");
                
            }
            
            if ( "edit".equals(getString("actionType")) && validator.isDuplicateDescriptionEdit(testString,getString("shippingKey")) )  
            {
                errors.add("shippingKey", new ActionMessage("error.shippingKeyDetail.duplicate", "Description"));
                logger.info("Attempted to add duplicate description in the database " + testString + " already exists.");
                
            }
            
            //Start of shipper Validation
            testString = getString("shipper");
            if (StringUtils.isBlank(testString)) {
                errors.add("shippingKey", 
                           new ActionMessage("error.shippingKeyDetail.nullInput", 
                                             "Shipper"));
                logger.info("Validation of shipper " + testString + " failed because it is blank.");
            }
            if (testString.length() > 25) {
                errors.add("shippingKey", new ActionMessage("error.shippingKeyDetail.tooLong", "Shipper"));
                logger.error("Validation of shipper " + testString + " failed because it is too long.");
            }
            
            //Min Price Validation
            String [] min_price = getStrings("minPrice");
            for (int i = 0; i < min_price.length; i++) {
                if (StringUtils.isNotBlank(min_price[i]))
                {
                    rowHasData[i] = true;
                }
                rowHasError[i] = validateCurrency ("Min Price", min_price[i], i);
            }
            
            //Max Price Validation
    
            String [] max_price = getStrings("maxPrice");
            for (int i = 0; i < max_price.length; i++) {
                if (StringUtils.isNotBlank(max_price[i]))
                {
                    rowHasData[i] = true;
                }
                rowHasError[i] = validateCurrency ("Max Price", max_price[i], i);
            }
            
            // Validation of Standard Cost
            String [] standard_cost = getStrings("standardCost");
              for (int i = 0; i < standard_cost.length; i++) {
                  if (StringUtils.isNotBlank(standard_cost[i]))
                  {
                      rowHasData[i] = true;
                  }
                  validateCurrency("Standard Cost", standard_cost[i], i);
              }
            
            //Validation of two day cost
            String [] two_day_cost = getStrings("twoDayCost");
            for (int i = 0; i < two_day_cost.length; i++) {
                if (StringUtils.isNotBlank(two_day_cost[i]))
                {
                    rowHasData[i] = true;
                }
                validateCurrency("Two Day Cost", two_day_cost[i], i);
            }
    
            //Validation of next day cost
            String [] next_day_cost = getStrings("nextDayCost");
            for (int i = 0; i < next_day_cost.length; i++) {
                
                if (StringUtils.isNotBlank(next_day_cost[i]))
                {
                    rowHasData[i] = true;
                }
                validateCurrency("Next Day Cost", next_day_cost[i], i);
            }
    
            // Validation of Saturday cost
            String [] saturday_cost = getStrings("saturdayCost");
            for (int i = 0; i < saturday_cost.length; i++) {
                if (StringUtils.isNotBlank(saturday_cost[i]))
                {
                    rowHasData[i] = true;
                }
                validateCurrency("Saturday Cost", saturday_cost[i], i);
            }
    
            // Validation of same day cost
            String [] same_day_cost = getStrings("sameDayCost");
            for (int i = 0; i < same_day_cost.length; i++) {
                
                if (StringUtils.isNotBlank(same_day_cost[i]))
                {
                    rowHasData[i] = true;
                }
                validateCurrency("Same Day Cost", same_day_cost[i], i);
            }
            
            // Validation of Sunday cost
            String [] sunday_cost = getStrings("sundayCost");
            for (int i = 0; i < sunday_cost.length; i++) {
                
                if (StringUtils.isNotBlank(sunday_cost[i]))
                {
                    rowHasData[i] = true;
                }
                validateCurrency("Sunday Cost", sunday_cost[i], i);
            }
            
            // We only want to do the advanced checking if the format validations pass
            //Checking for validity of values of Min and Max price
            if (min_price.length != max_price.length)
            {
                errors.add("dollarAmount", 
                           new ActionMessage("error.shippingKeyDetail.arrayLengthMismatch"));
                logger.info("There min price array has " + min_price.length + " elements, while the max_price array has " + max_price.length + " elements.");
            }
            
            
            for (int i = 0; i < min_price.length; i++) {
                if (!rowHasError[i] && !validator.isMaxValueGreaterThanMinValue(min_price[i], max_price[i])) 
                {
                    errors.add("dollarAmount", 
                               new ActionMessage("error.shippingKeyDetail.maxNotGreaterThanMin", 
                                                 "Max Price", i+1));
                    logger.info("Validation of max price " + max_price[i] + " failed because it is not greater than the minimum value: "+ min_price[i] + " in the row.");
                }
                if (i != 0)
                {
                    if (!rowHasError[i-1] && !rowHasError[i] && !validator.isMinValueValid(max_price[i-1] ,min_price[i], max_price[i]))
                    {
                        errors.add("dollarAmount", 
                                   new ActionMessage("error.shippingKeyDetail.minNotGreaterThanLastMax", 
                                                     "Min Price", i+1));
                        logger.info("Validation of min price " + min_price[i] + " failed because it is not .01 greater than the previous max value " + max_price[i-1] + ".");
                    }
                }
            }
            
            
            if (! "delete".equals(getString("actionType")) )
            {
                // Making sure there are no duplicates in Min Price or Max Price
                //Delete doesn't pass the max_price array, so don't check its length.
                if (min_price.length != rowHasData.length )
                {
                    errors.add("dollarAmount", 
                               new ActionMessage("error.shippingKeyDetail.arrayLengthMismatch"));
                    logger.info("There rowHasData array has " + rowHasData.length + " elements, while the min_price array has " + min_price.length + " elements.");   
                }
                if (validator.isThereDuplicatePrices(min_price, rowHasData))
                {
                    errors.add("dollarAmount", 
                               new ActionMessage("error.shippingKeyDetail.duplicateEntry", "Min Price"));
                    logger.info("A duplicate amount was detected in the min_price column .");
              
                }
                
                if (!validator.isOnlyFirstMinPriceBlank(min_price, rowHasData))
                {
                    errors.add("dollarAmount", 
                               new ActionMessage("error.shippingKeyDetail.minBlank"));
                    logger.info("There was a blank entry that was not in the first min price field.");
                
                }

                if (max_price.length != rowHasData.length)
                {
                    errors.add("dollarAmount", 
                               new ActionMessage("error.shippingKeyDetail.arrayLengthMismatch"));
                    logger.info("There rowHasData array has " + rowHasData.length + " elements, while the max_price array has " + max_price.length + " elements.");   
                }
                
                if (validator.isThereDuplicatePrices(max_price, rowHasData))
                {
                    errors.add("dollarAmount", 
                               new ActionMessage("error.shippingKeyDetail.duplicateEntry", "Max Price"));
                    logger.info("A duplicate amount was detected in the max_price column.");       
                }
                
                if (!validator.isOnlyLastMaxPriceBlank(max_price, rowHasData))
                {
                    errors.add("dollarAmount", 
                               new ActionMessage("error.shippingKeyDetail.maxBlank"));
                    logger.info("There was a blank entry that was not in the last max price field.");
                
                }
            }
        }
        catch (Throwable e )
        {
            logger.error(e);
           
        }
            
        return errors;
        
    }
    
    /**
     * This is a function that wraps the currency validation together
     * @param fieldName
     * @param fieldValue
     * @param index
     */
    
    private boolean validateCurrency ( String fieldName, String fieldValue, int index)
    {
        boolean errorFound = false;
        if (validator.isValidCurrenty(fieldValue)) {
            /*if (!validator.isGreaterThanZero(fieldValue)) 
            {
                errors.add("dollarAmount", 
                           new ActionMessage("error.shippingKeyDetail.tooSmallCurrency", 
                                             fieldName, index+1));
                logger.info("Validation of min price " + fieldValue + " failed because it is not greater than zero.");
            }*/
            if (!validator.isInPriceRange(fieldValue)) 
            {
                errorFound = true;
                errors.add("dollarAmount", new ActionMessage("error.shippingKeyDetail.outOfPriceRange", fieldName, index+1));
            logger.info("Validation of " + fieldName + " " + fieldValue + "  failed because it is out of the valid price range.");
            }
            
        }
        else
        {
            errorFound = true;
            errors.add("dollarAmount", new ActionMessage("error.shippingKeyDetail.invalidCurrency", fieldName, index+1));
            logger.info("Validation of " + fieldName + " " + fieldValue + " failed because it is not in currency format.");
        }
        return errorFound;
    }

}
