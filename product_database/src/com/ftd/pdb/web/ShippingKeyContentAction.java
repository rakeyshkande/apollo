package com.ftd.pdb.web;

import com.ftd.pdb.common.ProductMaintConstants;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


public final class ShippingKeyContentAction extends PDBAction
{
    public ShippingKeyContentAction()
    {
        super("com.ftd.pdb.web.ShippingKeyContentAction");
    }
    
    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param mapping The ActionMapping used to select this instance
     * @param form The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     */
    
    public ActionForward execute(ActionMapping mapping,
				 ActionForm form,
				 HttpServletRequest request,
				 HttpServletResponse response)
	throws IOException, ServletException
	{
        
        ShippingKeyContentForm shippingKeyContentForm = (ShippingKeyContentForm)form;


        // Get all values needed to prepopulate the form
     //   try
     //   {
   //     ProductMaintSVCDelegate productDelegate = new ProductMaintSVCDelegate();
   //        ProductVO productVO = productDelegate.getProduct(productId);
    /**        if(productVO != null)
            {
                PropertyUtils.copyProperties(productDetailForm, productVO);
            }
            else
            {
                productDetailForm.setProductID(productId);
            }
            
            LookupSVCDelegate lookupDelegate = new LookupSVCDelegate();
            request.setAttribute(ProductMaintConstants.CATEGORIES_KEY, lookupDelegate.getCategoryList());
            request.setAttribute(ProductMaintConstants.PRODUCT_TYPES_KEY, lookupDelegate.getTypesList());
            request.setAttribute(ProductMaintConstants.PRODUCT_SUB_TYPES_KEY, lookupDelegate.getSubTypesList());
        request.setAttribute(mapping.getAttribute(), productDetailForm);
         
		// Forward control to the specified success URI
		return (mapping.findForward("success"));
    }

     
        request.setAttribute( "keyList", getKeyList() );
        request.setAttribute( "shipperNameList", getShipperNameList() );
        request.setAttribute( "priceCategoryList", getPriceCategoryList() );

        ShippingKeyContentForm contentForm = new ShippingKeyContentForm();
        request.setAttribute( "shippingKeyContentForm", contentForm );
*/
        return (mapping.findForward( "success" ) );

    }
}