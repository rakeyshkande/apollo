package com.ftd.pdb.web;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.StoreFeedUtil;
import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBException;
import com.ftd.pdb.common.valobjs.ShippingKeyVO;
import com.ftd.pdb.common.valobjs.ValueVO;
import com.ftd.pdb.service.IShippingKeySVCService;

import java.io.IOException;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.util.ResponseUtils;


public final class DeleteShippingKeyAction extends PDBAction {
    
    private final String BASE_CONFIG = "BASE_CONFIG";
    private final String BASE_URL = "BASE_URL";
    /* Spring managed resources */
    private IShippingKeySVCService shippingKeySVC;
    /* end Spring managed resources */
    

    public DeleteShippingKeyAction() {
        super("com.ftd.pdb.web.DeleteShippingKeyAction");
    }

    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param mapping The ActionMapping used to select this instance
     * @param form The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     */
    public

    ActionForward execute(ActionMapping mapping, ActionForm form, 
                          HttpServletRequest request, 
                          HttpServletResponse response) throws IOException, 
                                                               ServletException {
        //Determine user id from request object
        ShippingKeyDetailForm deleteShippingKeyForm = (ShippingKeyDetailForm)form;
        String selectedShippingKeyID = (String)deleteShippingKeyForm.get("shippingKey");
        ShippingKeyVO vo = new ShippingKeyVO();
        vo.setShippingKey_ID( selectedShippingKeyID );
        vo.setUpdatedBy(getUserId(request));
        String baseUrl = "";

        // Get the Novator servers to send to
        Map novatorMap = new HashMap();

        try {
            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
            baseUrl = configUtil.getFrpGlobalParm(BASE_CONFIG,BASE_URL);


            if (BooleanUtils.isTrue((Boolean)deleteShippingKeyForm.get("chkLive"))) {
                String key = 
                    configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, 
                                           ProductMaintConstants.PDB_NOVATOR_LIVE_IP_KEY);
                String value = 
                    configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, 
                                           ProductMaintConstants.PDB_NOVATOR_LIVE_PORT_KEY);
                ValueVO mapVO = new ValueVO();
                mapVO.setId(key);
                mapVO.setDescription(value);
                novatorMap.put("live", mapVO);
            }
            if (BooleanUtils.isTrue((Boolean)deleteShippingKeyForm.get("chkTest"))) {
                String key = 
                    configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, 
                                           ProductMaintConstants.PDB_NOVATOR_TEST_IP_KEY);
                String value = 
                    configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, 
                                           ProductMaintConstants.PDB_NOVATOR_TEST_PORT_KEY);
                ValueVO mapVO = new ValueVO();
                mapVO.setId(key);
                mapVO.setDescription(value);
                novatorMap.put("test", mapVO);
            }
            if (BooleanUtils.isTrue((Boolean)deleteShippingKeyForm.get("chkUAT"))) {
                String key = 
                    configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, 
                                           ProductMaintConstants.PDB_NOVATOR_UAT_IP_KEY);
                String value = 
                    configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, 
                                           ProductMaintConstants.PDB_NOVATOR_UAT_PORT_KEY);
                ValueVO mapVO = new ValueVO();
                mapVO.setId(key);
                mapVO.setDescription(value);
                novatorMap.put("uat", mapVO);
            }
            if (BooleanUtils.isTrue((Boolean)deleteShippingKeyForm.get("chkContent"))) {
                String key = 
                    configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, 
                                           ProductMaintConstants.PDB_NOVATOR_CONTENT_IP_KEY);
                String value = 
                    configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, 
                                           ProductMaintConstants.PDB_NOVATOR_CONTENT_PORT_KEY);
                ValueVO mapVO = new ValueVO();
                mapVO.setId(key);
                mapVO.setDescription(value);
                novatorMap.put("content", mapVO);
            }
            
            
        } catch (Exception e) {
            logger.error(e);
            throw new ServletException("Failed to retrieve configuration property", 
                                       e);
        }

        try {
            shippingKeySVC.removeShippingKey(vo, novatorMap);
            
            // Once the shipping key is deleted - post a message to STORE_FEEDS queue to send shipping key feed.
            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
			String allowedFeedTypes = configUtil.getFrpGlobalParm("PI_CONFIG", "PROCESS_FEEDS");
			if (allowedFeedTypes!=null && allowedFeedTypes.contains("SHIPPING_KEY_FEED")) {
				
				StoreFeedUtil util = new StoreFeedUtil();
				String msgTxt = "SHIPPING_KEY_FEED "
						+ vo.getShippingKey_ID()+" Delete";
				if (logger.isDebugEnabled()) {
					logger.debug("Shipping key updated. Save Store feed, "
							+ msgTxt);
				}
				util.postStoreFeedMessage(msgTxt, msgTxt);
			}  
			
        } catch (Exception e) {
            Exception exception = null;
            try {
                Class class1 = e.getClass();
                Class class2 = 
                    Class.forName(ProductMaintConstants.PDBSYSTEMEXCEPTION_CLASS_NAME);
                Class class3 = 
                    Class.forName(ProductMaintConstants.PDBAPPLICATIONEXCEPTION_CLASS_NAME);
                if (!class1.equals(class2) && !class1.equals(class3)) {
                    String[] args = new String[1];
                    args[0] = new String(e.getMessage());
                    logger.error(e);
                    PDBException ftde = 
                        new PDBApplicationException(ProductMaintConstants.GENERAL_APPLICATION_EXCEPTION, 
                                                    args, e);
                    exception = ftde;
                } else {
                    exception = e;
                }
            } catch (ClassNotFoundException cnfe) {
                logger.error(cnfe);
            }

            return super.handleException(request, mapping, exception);
        }
        
        // Set the back link
        request.setAttribute(ProductMaintConstants.MENU_BACK_KEY, 
                             ProductMaintConstants.MENU_BACK_SHIPPING_KEY_KEY);
        request.setAttribute(ProductMaintConstants.MENU_BACK_TEXT_KEY, 
                             ProductMaintConstants.MENU_BACK_TEXT_SHIPPING_KEY_KEY);
        
 

        StringBuffer newPath = new StringBuffer(mapping.findForward("success").getPath());
           newPath.append("?securitytoken=" + request.getParameter("securitytoken") + "&context=" + request.getParameter("context") + 
                          "&actionType=" + request.getParameter("actionType") + "&shippingKey=" + request.getParameter("shippingKey") + "&actionStatus=successful");
        return (new ActionForward(baseUrl + newPath.toString(), true));

    }

    public void setShippingKeySVC(IShippingKeySVCService shippingKeySVC) {
        this.shippingKeySVC = shippingKeySVC;
    }

    public IShippingKeySVCService getShippingKeySVC() {
        return shippingKeySVC;
    }

}
