package com.ftd.pdb.web;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.utilities.FTDUtil;
import com.ftd.pdb.common.valobjs.SearchKeyVO;
import com.ftd.pdb.common.valobjs.UpsellDetailVO;
import com.ftd.pdb.common.valobjs.UpsellMasterVO;
import com.ftd.pdb.common.valobjs.ValueVO;
import com.ftd.pdb.service.IUpsellSVCBusiness;

import java.io.IOException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.BooleanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * Implementation of <strong>Action</strong> that processes a produst
 * list request.
 *
 * @author Ed Mueller
 */
public class UpsellDetailAction extends PDBAction {
    /*Spring managed resources */
    private IUpsellSVCBusiness upsellSVC;
    /*end Spring managed resources */

    public UpsellDetailAction() {
        super("com.ftd.pdb.web.UpsellDetailAction");
    }

    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param mapping The ActionMapping used to select this instance
     * @param form The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, 
                                 HttpServletRequest request, 
                                 HttpServletResponse response) throws IOException, 
                                                                      ServletException {

        try {

            boolean userSelectedEnvironments = false;
            boolean novatorEnvironmentError = false;


            //get form object
            UpsellDetailForm inputForm = (UpsellDetailForm)form;
            UpsellMasterVO upsellMasterVO = new UpsellMasterVO();

            //Builder MasterVo using data on request
            upsellMasterVO.setMasterID((String)inputForm.get("masterSKU"));
            upsellMasterVO.setMasterStatus(BooleanUtils.toBoolean((Boolean)inputForm.get("arrangementAvailable")));
            upsellMasterVO.setMasterName((String)inputForm.get("productName"));
            upsellMasterVO.setSearchPriority((String)inputForm.get("searchPriority"));
            upsellMasterVO.setCorporateSiteFlag(BooleanUtils.toBoolean((Boolean)inputForm.get("corporateSiteFlag")));
            upsellMasterVO.setGbbPopoverFlag(BooleanUtils.toBoolean((Boolean)inputForm.get("gbbPopoverFlag")));
            upsellMasterVO.setGbbTitle((String)inputForm.get("gbbTitle"));

            //remove DOS line break from description
            String longDesc = 
                FTDUtil.replaceAll((String)inputForm.get("description"), 
                                   ProductMaintConstants.END_OF_LINE_CHARACTER, 
                                   ProductMaintConstants.LINE_BREAK);
            upsellMasterVO.setMasterDescription(longDesc);

            //Associated companies
            String[] companyArray = (String[])inputForm.get("companyArray");
            upsellMasterVO.setCompanyList(companyArray);

            //get keywords
            ArrayList keywords = new ArrayList();
            StringTokenizer st = 
                new StringTokenizer((String)inputForm.get("keywordSearch"));
            while (st.hasMoreTokens()) {
                SearchKeyVO keyVO = new SearchKeyVO();
                keyVO.setSearchKey(st.nextToken());
                keywords.add(keyVO);
            }
            upsellMasterVO.setSearchKeyList(keywords);

            //Get recipient Info
            String[] recipientArray = (String[])inputForm.get("recipientSearchArray");
            upsellMasterVO.setRecipientSearchList(recipientArray);

            //get websites
            ArrayList websites = new ArrayList();
            st = new StringTokenizer((String)inputForm.get("websites"));
            while (st.hasMoreTokens()) {
                ValueVO valueVO = new ValueVO();
                valueVO.setId(st.nextToken());
                websites.add(valueVO);
            }
            upsellMasterVO.setWebsites(websites);

            //Get detail SKUs from request
            List detailArray = new ArrayList();
            detailArray = upsellSVC.getSKUsFromRequest(request);

            //add array to master vo
            upsellMasterVO.setUpsellDetailList(detailArray);

            for (int i=0; i<detailArray.size(); i++) {
                UpsellDetailVO detailVO = (UpsellDetailVO) detailArray.get(i);
                if (detailVO.getGbbSequence() == 1) {
                    upsellMasterVO.setGbbUpsellDetailId1(detailVO.getDetailID());
                    upsellMasterVO.setGbbNameOverrideFlag1(detailVO.getGbbNameOverrideFlag());
                    upsellMasterVO.setGbbNameOverrideText1(detailVO.getGbbNameOverrideText());
                    upsellMasterVO.setGbbPriceOverrideFlag1(detailVO.getGbbPriceOverrideFlag());
                    upsellMasterVO.setGbbPriceOverrideText1(detailVO.getGbbPriceOverrideText());
                } else if (detailVO.getGbbSequence() == 2) {
                    upsellMasterVO.setGbbUpsellDetailId2(detailVO.getDetailID());
                    upsellMasterVO.setGbbNameOverrideFlag2(detailVO.getGbbNameOverrideFlag());
                    upsellMasterVO.setGbbNameOverrideText2(detailVO.getGbbNameOverrideText());
                    upsellMasterVO.setGbbPriceOverrideFlag2(detailVO.getGbbPriceOverrideFlag());
                    upsellMasterVO.setGbbPriceOverrideText2(detailVO.getGbbPriceOverrideText());
                } else if (detailVO.getGbbSequence() == 3) {
                    upsellMasterVO.setGbbUpsellDetailId3(detailVO.getDetailID());
                    upsellMasterVO.setGbbNameOverrideFlag3(detailVO.getGbbNameOverrideFlag());
                    upsellMasterVO.setGbbNameOverrideText3(detailVO.getGbbNameOverrideText());
                    upsellMasterVO.setGbbPriceOverrideFlag3(detailVO.getGbbPriceOverrideFlag());
                    upsellMasterVO.setGbbPriceOverrideText3(detailVO.getGbbPriceOverrideText());
                }
            }

            // Get the Novator servers to send to
            List<String> envKeys = new ArrayList();
            try {
                ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
                if (inputForm.get("chkProd")!= null && (BooleanUtils.toBoolean((Boolean)inputForm.get("chkProd")))) {
                    envKeys.add(com.ftd.osp.utilities.feed.NovatorFeedUtil.PRODUCTION);
                }
                if (inputForm.get("chkTest")!= null && (BooleanUtils.toBoolean((Boolean)inputForm.get("chkTest")))) {
                    envKeys.add(com.ftd.osp.utilities.feed.NovatorFeedUtil.TEST);
                }
                if (inputForm.get("chkUAT")!= null &&(BooleanUtils.toBoolean((Boolean)inputForm.get("chkUAT")))) {
                    envKeys.add(com.ftd.osp.utilities.feed.NovatorFeedUtil.UAT);
                }
                if (inputForm.get("chkContent")!= null && (BooleanUtils.toBoolean((Boolean)inputForm.get("chkContent")))) {
                    envKeys.add(com.ftd.osp.utilities.feed.NovatorFeedUtil.CONTENT);
                }
            } catch (Exception e) {
                logger.error(e);
                throw new ServletException("Failed to retrieve configuration property", e);
            }

            //check if nothing is being sent to Novator
            if (envKeys == null || envKeys.size() <= 0) {
                userSelectedEnvironments = false;
            } else {
                userSelectedEnvironments = true;
            }

            String novatorMessage = "";
            if (userSelectedEnvironments) {
                //verify that this order can be sent to Novator                       
                for (int i = 0; i < detailArray.size(); i++) {
                    String thisItemsErrors = "";
                    UpsellDetailVO detailVO = 
                        (UpsellDetailVO)detailArray.get(i);
                    if (inputForm.get("chkProd") != null &&
                        BooleanUtils.toBoolean((Boolean)inputForm.get("chkProd")) && 
                        !detailVO.isSentToNovatorProd()) {
                        //add comma if needed
                        if (!thisItemsErrors.equals("")) {
                            thisItemsErrors = thisItemsErrors + ",";
                        }
                        thisItemsErrors = thisItemsErrors + "Live";
                        novatorEnvironmentError = true;
                    }

                    if (inputForm.get("chkContent") != null &&
                        BooleanUtils.toBoolean((Boolean)inputForm.get("chkContent")) && 
                        !detailVO.isSentToNovatorContent()) {
                        //add comma if needed
                        if (!thisItemsErrors.equals("")) {
                            thisItemsErrors = thisItemsErrors + ",";
                        }
                        thisItemsErrors = thisItemsErrors + "Content";
                        novatorEnvironmentError = true;
                    }

                    if (inputForm.get("chkUAT") != null &&
                        BooleanUtils.toBoolean((Boolean)inputForm.get("chkUAT")) && 
                        !detailVO.isSentToNovatorUAT()) {
                        //add comma if needed
                        if (!thisItemsErrors.equals("")) {
                            thisItemsErrors = thisItemsErrors + ",";
                        }
                        thisItemsErrors = thisItemsErrors + "UAT";
                        novatorEnvironmentError = true;
                    }

                    if (inputForm.get("chkTest") != null &&
                        (Boolean)inputForm.get("chkTest") && 
                        !detailVO.isSentToNovatorTest()) {
                        //add comma if needed
                        if (!thisItemsErrors.equals("")) {
                            thisItemsErrors = thisItemsErrors + ",";
                        }
                        thisItemsErrors = thisItemsErrors + "Test";
                        novatorEnvironmentError = true;
                    }

                    //build error msg for this item if needed
                    if (!thisItemsErrors.equals("")) {
                        if (novatorMessage.equals("")) {
                            novatorMessage = 
                                    " Please submit " + detailVO.getDetailID() + 
                                    " to " + thisItemsErrors + ".";
                        } else {
                            novatorMessage = 
                                    novatorMessage + "  Submit " + detailVO.getDetailID() + 
                                    " to " + thisItemsErrors + ".";
                        }
                    }

                } //end for loop


            } //end if user user selected environments

            //if environment errors occured do not send to Novator
            if (novatorEnvironmentError) {
                envKeys = null;
            } //End env error check            

            // Update the database
            upsellSVC.updateMasterSKU(upsellMasterVO, envKeys);

            //set response message
            if (!userSelectedEnvironments) {
                //user did not select anyting
                String notSentMsg = 
                    super.getResources(request).getMessage(ProductMaintConstants.NOVATOR_NOT_UPDATED_MESSAGE_KEY, 
                                                           upsellMasterVO.getMasterID());
                String selectToSend = 
                    super.getResources(request).getMessage(ProductMaintConstants.NOVATOR_BOXES_NOT_CHECKED_KEY, 
                                                           upsellMasterVO.getMasterID());
                String message = notSentMsg + " " + selectToSend;
                request.setAttribute(ProductMaintConstants.NOVATOR_NOTE_KEY, 
                                     message);
            } else {
                if (novatorEnvironmentError) {
                    //environment selection error
                    String notSentMsg = 
                        super.getResources(request).getMessage(ProductMaintConstants.NOVATOR_NOT_UPDATED_MESSAGE_KEY, 
                                                               upsellMasterVO.getMasterID());
                    String returnMsg = 
                        super.getResources(request).getMessage(ProductMaintConstants.NOVATOR_RETURN_TO_SUBMIT_KEY, 
                                                               upsellMasterVO.getMasterID());
                    String message = 
                        notSentMsg + " " + novatorMessage + " " + returnMsg;
                    request.setAttribute(ProductMaintConstants.NOVATOR_NOTE_KEY, 
                                         message);
                } else {
                    //no errors
                    String submitSuccess = 
                        super.getResources(request).getMessage(ProductMaintConstants.NOVATOR_SUBMIT_SUCCESS_MESSAGE_KEY, 
                                                               upsellMasterVO.getMasterID());
                    request.setAttribute(ProductMaintConstants.PRODUCT_MAINT_INSTRUCTION_KEY, 
                                         submitSuccess);
                }
            }

            request.setAttribute(mapping.getAttribute(), inputForm);


        } catch (Exception e) {
            return super.handleException(request, mapping, e);
        }

        // Forward control to the specified success URI
        return (mapping.findForward("success"));
    }

    public void setUpsellSVC(IUpsellSVCBusiness upsellSVC) {
        this.upsellSVC = upsellSVC;
    }

    public IUpsellSVCBusiness getUpsellSVC() {
        return upsellSVC;
    }
}
