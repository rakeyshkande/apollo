package com.ftd.pdb.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.StoreFeedUtil;
import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBException;
import com.ftd.pdb.common.utilities.FTDUtil;
import com.ftd.pdb.common.valobjs.ShippingKeyPriceVO;
import com.ftd.pdb.common.valobjs.ShippingKeyVO;
import com.ftd.pdb.common.valobjs.ShippingMethodVO;
import com.ftd.pdb.common.valobjs.ValueVO;
import com.ftd.pdb.service.IShippingKeySVCService;


public final class ShippingKeyDetailAction extends PDBAction
{
    private final String BASE_CONFIG = "BASE_CONFIG";
    private final String BASE_URL = "BASE_URL";
    /*Spring managed resources */
    private IShippingKeySVCService shippingKeySVC;
    /*end Spring managed resources */
    public ShippingKeyDetailAction()
    {
        super("com.ftd.pdb.web.ShippingKeyDetailAction");
    }
    
    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param mapping The ActionMapping used to select this instance
     * @param form The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     */
    
    public ActionForward execute(ActionMapping mapping,
				 ActionForm form,
				 HttpServletRequest request,
				 HttpServletResponse response)
	throws IOException, ServletException
    {
        ShippingKeyDetailForm shippingKeyDetailForm = (ShippingKeyDetailForm)form;
        String selectedShippingKeyID = (String)shippingKeyDetailForm.get("shippingKey");
        ShippingKeyVO vo = new ShippingKeyVO();
        String baseUrl = "";
        
        
        try {

            String userId = getUserId(request);
            vo.setCreatedBy(userId);
            vo.setUpdatedBy(userId);
    
            vo.setShippingKey_ID( selectedShippingKeyID );
            vo.setShipperID( (String)shippingKeyDetailForm.get("shipper"));
            vo.setShippingKeyDescription( (String)shippingKeyDetailForm.get("description"));
    
    
    // remove the empty input rows
    
            ArrayList priceVO_list = new ArrayList();
            String[] minPriceList = (String[])shippingKeyDetailForm.get("minPrice");
            String[] maxPriceList = (String[])shippingKeyDetailForm.get("maxPrice");
            String[] standardCostList = (String[])shippingKeyDetailForm.get("standardCost");
            String[] twoDayCostList = (String[])shippingKeyDetailForm.get("twoDayCost");
            String[] nextDayCostList = (String[])shippingKeyDetailForm.get("nextDayCost");
            String[] saturdayCostList = (String[])shippingKeyDetailForm.get("saturdayCost");
            String[] sundayCostList = (String[])shippingKeyDetailForm.get("sundayCost");
            String[] sameDayCostList = (String[])shippingKeyDetailForm.get("sameDayCost");
            String[] shippingKeyPriceIdList = (String[])shippingKeyDetailForm.get("shippingPriceId");
            String[] standardPriceIdList = (String[])shippingKeyDetailForm.get("standardPriceId");
            String[] twoDayPriceIdList = (String[])shippingKeyDetailForm.get("twoDayPriceId");
            String[] nextDayPriceIdList = (String[])shippingKeyDetailForm.get("nextDayPriceId");
            String[] saturdayPriceIdList = (String[])shippingKeyDetailForm.get("saturdayPriceId");
            String[] sundayPriceIdList = (String[])shippingKeyDetailForm.get("sundayPriceId");
            String[] sameDayPriceIdList = (String[])shippingKeyDetailForm.get("sameDayPriceId");
            
            for (int i=0; i<ShippingKeyDetailForm.NUMBER_OF_ROW; i++) {
                if (StringUtils.isNotBlank(minPriceList[i]) || 
                    StringUtils.isNotBlank(maxPriceList[i]) || 
                    StringUtils.isNotBlank(standardCostList[i]) || 
                    StringUtils.isNotBlank(twoDayCostList[i]) || 
                    StringUtils.isNotBlank(nextDayCostList[i]) || 
                    StringUtils.isNotBlank(saturdayCostList[i]) ||
                    StringUtils.isNotBlank(sameDayCostList[i])) 
                {
                    ShippingKeyPriceVO priceVO = new ShippingKeyPriceVO();
                    priceVO.setShippingKeyID( selectedShippingKeyID );
                    priceVO.setShippingKeyPriceID((String)shippingKeyPriceIdList[i]);
                    if ( StringUtils.isBlank((String)minPriceList[i]) )
                        priceVO.setMinPrice( Double.valueOf( "0.0" ));
                    else 
                        priceVO.setMinPrice( Double.valueOf( FTDUtil.removeFormattingCurrency((String)minPriceList[i])));
                        if( StringUtils.isBlank((String)maxPriceList[i]))
                        priceVO.setMaxPrice( Double.valueOf( "0.0" ));
                    else
                        priceVO.setMaxPrice( Double.valueOf(FTDUtil.removeFormattingCurrency((String)maxPriceList[i])));
    
                    List methods = new ArrayList();
                    ShippingMethodVO methodVO = new ShippingMethodVO();
                    methodVO.setPriceId((String)standardPriceIdList[i]);
                    methodVO.setId(ProductMaintConstants.SHIPPING_STANDARD_CODE);
                    float fltPrice = 0;
                    String strPrice = (String)standardCostList[i];
                    if(strPrice != null && strPrice.length() > 0)
                    {
                        fltPrice = Float.parseFloat(FTDUtil.removeFormattingCurrency(strPrice));
                    }
                    methodVO.setCost(fltPrice);
                    methods.add(methodVO);
    
                    methodVO = new ShippingMethodVO();
                    methodVO.setPriceId((String)twoDayPriceIdList[i]);
                    methodVO.setId(ProductMaintConstants.SHIPPING_TWO_DAY_CODE);
                    fltPrice = 0;
                    strPrice = (String)twoDayCostList[i];
                    if(strPrice != null && strPrice.length() > 0)
                    {
                        fltPrice = Float.parseFloat(FTDUtil.removeFormattingCurrency(strPrice));
                    }
                    methodVO.setCost(fltPrice);
                    methods.add(methodVO);
    
                    methodVO = new ShippingMethodVO();
                    methodVO.setPriceId((String)nextDayPriceIdList[i]);
                    methodVO.setId(ProductMaintConstants.SHIPPING_NEXT_DAY_CODE);
                    fltPrice = 0;
                    strPrice = (String)nextDayCostList[i];
                    if(strPrice != null && strPrice.length() > 0)
                    {
                        fltPrice = Float.parseFloat(FTDUtil.removeFormattingCurrency(strPrice));
                    }
                    methodVO.setCost(fltPrice);
                    methods.add(methodVO);                   
    
                    methodVO = new ShippingMethodVO();
                    methodVO.setPriceId((String)saturdayPriceIdList[i]);
                    methodVO.setId(ProductMaintConstants.SHIPPING_SATURDAY_CODE);
                    fltPrice = 0;
                    strPrice = (String)saturdayCostList[i];
                    if(strPrice != null && strPrice.length() > 0)
                    {
                        fltPrice = Float.parseFloat(FTDUtil.removeFormattingCurrency(strPrice));
                    }
                    methodVO.setCost(fltPrice);
                    methods.add(methodVO);                 
    
                    methodVO = new ShippingMethodVO();
                    methodVO.setPriceId((String)sameDayPriceIdList[i]);
                    methodVO.setId(ProductMaintConstants.SHIPPING_SAME_DAY_CODE);
                    fltPrice = 0;
                    strPrice = (String)sameDayCostList[i];
                    if(strPrice != null && strPrice.length() > 0)
                    {
                        fltPrice = Float.parseFloat(FTDUtil.removeFormattingCurrency(strPrice));
                    }
                    methodVO.setCost(fltPrice);
                    methods.add(methodVO);
                    
                    methodVO = new ShippingMethodVO();
                    methodVO.setPriceId((String)sundayPriceIdList[i]);
                    methodVO.setId(ProductMaintConstants.SHIPPING_SUNDAY_CODE);
                    fltPrice = 0;
                    strPrice = (String)sundayCostList[i];
                    if(strPrice != null && strPrice.length() > 0)
                    {
                        fltPrice = Float.parseFloat(FTDUtil.removeFormattingCurrency(strPrice));
                    }
                    methodVO.setCost(fltPrice);
                    methods.add(methodVO);
    
                    priceVO.setShippingMethods(methods);
                    
                    priceVO_list.add( priceVO );
                }
            }
    
            vo.setPrices(priceVO_list);
            
            try {
                 
                // Get the Novator servers to send updates
                Map novatorMap = new HashMap();
                ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
                
                baseUrl = configUtil.getFrpGlobalParm(BASE_CONFIG,BASE_URL);
               
                
                if(shippingKeyDetailForm.get("chkLive") != null &&(Boolean)shippingKeyDetailForm.get("chkLive"))
                {
                    String key = configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, ProductMaintConstants.PDB_NOVATOR_LIVE_IP_KEY);
                    String value = configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, ProductMaintConstants.PDB_NOVATOR_LIVE_PORT_KEY);
                    ValueVO mapVO = new ValueVO();
                    mapVO.setId(key);
                    mapVO.setDescription(value);
                    novatorMap.put("live", mapVO);
                }
                if(shippingKeyDetailForm.get("chkTest") != null && (Boolean)shippingKeyDetailForm.get("chkTest"))
                {
                    String key = configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, ProductMaintConstants.PDB_NOVATOR_TEST_IP_KEY);
                    String value = configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, ProductMaintConstants.PDB_NOVATOR_TEST_PORT_KEY);
                    ValueVO mapVO = new ValueVO();
                    mapVO.setId(key);
                    mapVO.setDescription(value);
                    novatorMap.put("test", mapVO);
                }
                if(shippingKeyDetailForm.get("chkUAT") != null && (Boolean)shippingKeyDetailForm.get("chkUAT"))
                {
                    String key = configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, ProductMaintConstants.PDB_NOVATOR_UAT_IP_KEY);
                    String value = configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, ProductMaintConstants.PDB_NOVATOR_UAT_PORT_KEY);
                    ValueVO mapVO = new ValueVO();
                    mapVO.setId(key);
                    mapVO.setDescription(value);
                    novatorMap.put("uat", mapVO);
                }
                if(shippingKeyDetailForm.get("chkContent") != null && (Boolean)shippingKeyDetailForm.get("chkContent"))
                {
                    String key = configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, ProductMaintConstants.PDB_NOVATOR_CONTENT_IP_KEY);
                    String value = configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, ProductMaintConstants.PDB_NOVATOR_CONTENT_PORT_KEY);
                    ValueVO mapVO = new ValueVO();
                    mapVO.setId(key);
                    mapVO.setDescription(value);
                    novatorMap.put("content", mapVO);
                }             
                
                shippingKeySVC.updateShippingKey( vo, novatorMap );
            	
                // Once the shipping key is updated - post a message to STORE_FEEDS queue to send shipping key feed.
                
    			String allowedFeedTypes = configUtil.getFrpGlobalParm("PI_CONFIG", "PROCESS_FEEDS");
				if (allowedFeedTypes!=null && allowedFeedTypes.contains("SHIPPING_KEY_FEED")) {
					
					StoreFeedUtil util = new StoreFeedUtil();
					String msgTxt = "SHIPPING_KEY_FEED "
							+ vo.getShippingKey_ID()+" Update";
					if (logger.isDebugEnabled()) {
						logger.debug("Shipping key updated. Save Store feed, "
								+ msgTxt);
					}
					util.postStoreFeedMessage(msgTxt, msgTxt);
				}  
				
	        }
            catch(Exception e)
            {
                Exception exception = null;
                try
                {
                    Class class1 = e.getClass();
                    Class class2 = Class.forName(ProductMaintConstants.PDBSYSTEMEXCEPTION_CLASS_NAME);
                    Class class3 = Class.forName(ProductMaintConstants.PDBAPPLICATIONEXCEPTION_CLASS_NAME);
                    if(!class1.equals(class2) && !class1.equals(class3))
                    {
                        String[] args = new String[1];
                        args[0] = new String(e.getMessage());
                        logger.error(e);
                        PDBException ftde = new PDBApplicationException(ProductMaintConstants.GENERAL_APPLICATION_EXCEPTION, args, e);
                        exception = ftde;
                    }
                    else
                    {
                        exception = e;
                    }
                }
                catch(ClassNotFoundException cnfe)
                {
                    logger.error(cnfe);
                }
                
                return super.handleException(request, mapping, exception);
            }
            // Set the back link
            request.setAttribute(ProductMaintConstants.MENU_BACK_KEY, ProductMaintConstants.MENU_BACK_SHIPPING_KEY_KEY);
            request.setAttribute(ProductMaintConstants.MENU_BACK_TEXT_KEY, ProductMaintConstants.MENU_BACK_TEXT_SHIPPING_KEY_KEY);
        }
        catch (Throwable t)
        {
            logger.error(t);
        }
        

        
        StringBuffer newPath = new StringBuffer(mapping.findForward("success").getPath());
           newPath.append("?securitytoken=" + request.getParameter("securitytoken") + "&context=" + request.getParameter("context") + 
                          "&actionType=" + request.getParameter("actionType") + "&shippingKey=" + request.getParameter("shippingKey") + "&actionStatus=successful");
        return (new ActionForward(baseUrl + newPath.toString(), true));
    }

    public void setShippingKeySVC(IShippingKeySVCService shippingKeySVC) {
        this.shippingKeySVC = shippingKeySVC;
    }

    public IShippingKeySVCService getShippingKeySVC() {
        return shippingKeySVC;
    }
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) throws Throwable {
        try 
        {     
            ShippingKeyDetailForm shippingKeyDetailForm = new ShippingKeyDetailForm();
            ActionErrors errors = shippingKeyDetailForm.validate(mapping, request);
            return errors;
        }
        catch (Throwable e )
        {
            logger.error(e);   
            throw (e);
        }

    }
    
}
