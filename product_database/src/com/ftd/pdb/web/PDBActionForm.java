package com.ftd.pdb.web;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pdb.service.IProductMaintSVCBusiness;

import java.io.File;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionServlet;
import org.apache.struts.validator.DynaValidatorForm;

import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.struts.DelegatingActionUtils;
import org.springframework.web.util.WebUtils;

public class PDBActionForm extends DynaValidatorForm {

    Logger logger = null;
    private WebApplicationContext webApplicationContext;
    private MessageSourceAccessor messageSourceAccessor;

    public PDBActionForm(String className) {
        logger = new Logger(className);
    }

    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public ActionErrors validate(ActionMapping mapping, 
                                 HttpServletRequest request) {

        ActionErrors errors = super.validate(mapping, request);

        if (errors == null) {
            errors = new ActionErrors();
        }

        return errors;
    }

    public void setServlet(ActionServlet actionServlet) {
        super.setServlet(actionServlet);
        if (actionServlet != null) {
            this.webApplicationContext = 
                    initWebApplicationContext(actionServlet);
            this.messageSourceAccessor = 
                    new MessageSourceAccessor(this.webApplicationContext);
        }
    }

    /**
     * Fetch ContextLoaderPlugIn's WebApplicationContext from the ServletContext,
     * falling back to the root WebApplicationContext (the usual case).
     * @param actionServlet the associated ActionServlet
     * @return the WebApplicationContext
     * @throws IllegalStateException if no WebApplicationContext could be found
     * @see DelegatingActionUtils#findRequiredWebApplicationContext
     */
    protected WebApplicationContext initWebApplicationContext(ActionServlet actionServlet) throws IllegalStateException {

        return DelegatingActionUtils.findRequiredWebApplicationContext(actionServlet, 
                                                                       null);
    }


    /**
     * Return the current Spring WebApplicationContext.
     */
    protected final WebApplicationContext getWebApplicationContext() {
        return this.webApplicationContext;
    }

    /**
     * Return a MessageSourceAccessor for the application context
     * used by this object, for easy message access.
     */
    protected final MessageSourceAccessor getMessageSourceAccessor() {
        return this.messageSourceAccessor;
    }

    /**
     * Return the current ServletContext.
     */
    protected final ServletContext getServletContext() {
        return this.webApplicationContext.getServletContext();
    }

    /**
     * Return the temporary directory for the current web application,
     * as provided by the servlet container.
     * @return the File representing the temporary directory
     */
    protected final File getTempDir() {
        return WebUtils.getTempDir(getServletContext());
    }
}
