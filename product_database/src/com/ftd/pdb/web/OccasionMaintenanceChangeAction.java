package com.ftd.pdb.web;

import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.service.IProductMaintSVCBusiness;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * Implementation of <strong>Action</strong> that processes a
 * Occasion Maintenance change.
 *
 * @author Ed Mueller
 */
public final class

OccasionMaintenanceChangeAction extends PDBAction {
    /*Spring managed resources */
    private IProductMaintSVCBusiness productSVC;
    /*end Spring managed resources */

    /**
* Constructor
*/
    public OccasionMaintenanceChangeAction() {
        super("com.ftd.pdb.web.OccasionMaintenanceChangeAction");
    }

    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param mapping The ActionMapping used to select this instance
     * @param form The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, 
                                 HttpServletRequest request, 
                                 HttpServletResponse response) throws IOException, 
                                                                      ServletException {

        String[] cards = null;
        int prevOccasion = 0;

        //Get selected Occasion and previous occasion
        OccasionMaintenanceForm occasionForm = (OccasionMaintenanceForm)form;
        String prevOccasionString = (String)occasionForm.get("prevOccasionID");
        int occasion = new Integer((String)occasionForm.get("occasionID")).intValue();

        //Update the previous occasion value
        occasionForm.set("prevOccasionID",Integer.toString(occasion));


        try {
            //Update data with user's changes
            //Must have the user's previous occasion id in order to update data
            if (!(prevOccasionString == null || 
                  prevOccasionString.equalsIgnoreCase(""))) {
                prevOccasion = new Integer(prevOccasionString).intValue();

                if (prevOccasion > 0) {
                    cards = request.getParameterValues("selectedID");
                    if (cards == null) {
                        cards = new String[] { };
                    }

                    //update data
                    productSVC.updateOccasionCards(prevOccasion, cards);
                }
            }

            //Populate list to show to user
            request.setAttribute(ProductMaintConstants.OCCASION_LIST_KEY, 
                                 productSVC.getOccasions());
            request.setAttribute(ProductMaintConstants.AVAILABLE_CARDS_KEY, 
                                 productSVC.getAvailableCards(occasion));
            request.setAttribute(ProductMaintConstants.SELECTED_CARDS_KEY, 
                                 productSVC.getSelectedCards(occasion));

            //Place form in session
            HttpSession session = request.getSession();
            if ("request".equals(mapping.getScope()))
                request.setAttribute(mapping.getAttribute(), occasionForm);
            else
                session.setAttribute(mapping.getAttribute(), occasionForm);


        } catch (Exception e) {
            return super.handleException(request, mapping, e);
        }


        // Forward control to the specified success URI
        return (mapping.findForward("success"));
    }

    public void setProductSVC(IProductMaintSVCBusiness productSVC) {
        this.productSVC = productSVC;
    }

    public IProductMaintSVCBusiness getProductSVC() {
        return productSVC;
    }
}
