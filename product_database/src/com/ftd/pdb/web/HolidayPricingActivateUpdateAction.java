package com.ftd.pdb.web;

import com.ftd.pdb.common.valobjs.HolidayPricingVO;
import com.ftd.pdb.service.IProductMaintSVCBusiness;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * Implementation of <strong>Action</strong> that processes a
 * holiday pricing update.
 *
 * @author Ed Mueller
 */
public final class

HolidayPricingActivateUpdateAction extends PDBAction {
    /*Spring managed resources */
    private IProductMaintSVCBusiness productSVC;
    /*end Spring managed resources */

    public HolidayPricingActivateUpdateAction() {
        super("com.ftd.pdb.web.HolidayPricingActivateUpdateAction");
    }

    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param mapping The ActionMapping used to select this instance
     * @param form The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     * @deprecated Holiday Pricing is unused and will be removed in the future.
     *   lpuckett 08/16/2006
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, 
                                 HttpServletRequest request, 
                                 HttpServletResponse response) throws IOException, 
                                                                      ServletException {
        HolidayPricingActivateForm holidayForm = 
            (HolidayPricingActivateForm)form;

        //Create and populate VO
        HolidayPricingVO holidayPricing = new HolidayPricingVO();
        holidayPricing.setHolidayPriceFlag((Boolean)holidayForm.get("holidayPriceFlag"));
        holidayPricing.setDeliveryDateFlag((Boolean)holidayForm.get("deliveryDateFlag"));

        HolidayPricingVO dbHolidayPricingVO = null;
        try {
            dbHolidayPricingVO = productSVC.getHolidayPricing();
        } catch (Exception e) {
            return super.handleException(request, mapping, e);
        }

        // Forward control to the specified success URI
        return (mapping.findForward("success"));
    }

    public void setProductSVC(IProductMaintSVCBusiness productSVC) {
        this.productSVC = productSVC;
    }

    public IProductMaintSVCBusiness getProductSVC() {
        return productSVC;
    }
}
