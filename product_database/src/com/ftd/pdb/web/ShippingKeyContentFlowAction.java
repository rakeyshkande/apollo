package com.ftd.pdb.web;

import com.ftd.pdb.common.exceptions.PDBException;
import com.ftd.pdb.common.valobjs.ShippingKeyVO;
import com.ftd.pdb.service.IShippingKeySVCService;

import java.io.IOException;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


public final class ShippingKeyContentFlowAction extends PDBAction {
    /*Spring managed resources */
    private IShippingKeySVCService shippingKeySVC;
    /*end Spring managed resources */

    public ShippingKeyContentFlowAction() {
        super("com.ftd.pdb.web.ShippingKeyContentFlowAction");
    }

    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param mapping The ActionMapping used to select this instance
     * @param form The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, 
                          HttpServletRequest request, 
                          HttpServletResponse response) throws IOException, 
                                                               ServletException {
        ShippingKeyContentForm shippingKeyContentForm = 
            (ShippingKeyContentForm)form;

        // Get all values needed to prepopulate the form
        try {
            List list = shippingKeySVC.getShippingKeyList();

            request.setAttribute("keyList", list);
            request.setAttribute("shipperNameList", getShipperNameList());
            request.setAttribute("priceCategoryList", getPriceCategoryList());

            request.setAttribute("shippingKeyContentForm", 
                                 shippingKeyContentForm);
        } catch (PDBException pdbException) {
            return super.handleException(request, mapping, pdbException);
        }

        return (mapping.findForward("success"));
    }

    private List getPriceCategoryList() {
        ArrayList list = new ArrayList();
        ShippingKeyVO shippingKeyVO1 = new ShippingKeyVO();
        shippingKeyVO1.setShipperID("All_cost_Category");
        shippingKeyVO1.setPriceCategory("shipping category1");

        ShippingKeyVO shippingKeyVO2 = new ShippingKeyVO();
        shippingKeyVO2.setShipperID("One_day_cost");
        shippingKeyVO2.setPriceCategory("shipping category 2");

        ShippingKeyVO shippingKeyVO3 = new ShippingKeyVO();
        shippingKeyVO3.setShipperID("two_day_cost");
        shippingKeyVO3.setPriceCategory("shipping category 3");

        list.add(shippingKeyVO1);
        list.add(shippingKeyVO2);
        list.add(shippingKeyVO3);

        return list;
    }

    private List getShipperNameList() {
        ArrayList list = new ArrayList();
        ShippingKeyVO shippingKeyVO1 = new ShippingKeyVO();
        shippingKeyVO1.setShipperID("FedEx");

        ShippingKeyVO shippingKeyVO2 = new ShippingKeyVO();
        shippingKeyVO2.setShipperID("Primary");

        list.add(shippingKeyVO1);
        list.add(shippingKeyVO2);

        return list;
    }

    public void setShippingKeySVC(IShippingKeySVCService shippingKeySVC) {
        this.shippingKeySVC = shippingKeySVC;
    }

    public IShippingKeySVCService getShippingKeySVC() {
        return shippingKeySVC;
    }
}
