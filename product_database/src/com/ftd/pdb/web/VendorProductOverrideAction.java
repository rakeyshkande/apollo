package com.ftd.pdb.web;

import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.utilities.FTDUtil;
import com.ftd.pdb.common.valobjs.VendorProductOverrideVO;
import com.ftd.pdb.service.IProductMaintSVCBusiness;

import java.io.IOException;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


public class VendorProductOverrideAction extends PDBAction {
    /*Spring managed resources */
    IProductMaintSVCBusiness productSVC;
    /*end Spring managed resources */

    public VendorProductOverrideAction() {
        super("com.ftd.pdb.web.VendorProductOverrideAction");
    }

  /**
   * This is the main action called from the Struts framework.
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   * @exception IOException
   * @exception ServletException
   */
   public ActionForward execute(ActionMapping mapping, ActionForm form, 
                                 HttpServletRequest request, 
                                 HttpServletResponse response) throws IOException, 
                                                                      ServletException 
    {
      String mode = request.getParameter(ProductMaintConstants.AJAX_MODE);
        
      String returnString = null; 
      try
      {
        response.setContentType("text/html");
        if( StringUtils.equals(mode,ProductMaintConstants.VPO_MODE_GET_VENDOR_PRODUCT_LIST) ) 
        {
          returnString = getProductListByVendor(request); 
        }
        else if (StringUtils.equals(mode,ProductMaintConstants.VPO_MODE_GET_CARRIER_SDS_SHIP_METHOD_LIST))
        {
          returnString = getSdsShipMethodsByCarrier(request);
        }        
        else if (StringUtils.equals(mode,ProductMaintConstants.VPO_MODE_SEARCH_VPO) ) 
        {
          HashMap requestParms = processSearchRequest(request); 

          //retrieve the results to be passed back
          returnString = filterVPO(
                  (String)requestParms.get("vendorId"), 
                  (String)requestParms.get("productId"), 
                  (String)requestParms.get("carrierId"), 
                  (String)requestParms.get("overrideDate")); 
        }
        else if (StringUtils.equals(mode,ProductMaintConstants.VPO_MODE_ADD_VPO) ) 
        {
          //Add the override
          processAddVPO(request);
          returnString = ProductMaintConstants.VPO_RESULTS_ADD_SUCCESS;
        }
        else if (StringUtils.equals(mode,ProductMaintConstants.VPO_MODE_DELETE_VPO) ) 
        {
          //Delete the override
          processDeleteVPO(request); 
          returnString = ProductMaintConstants.VPO_RESULTS_DELETE_SUCCESS;
        }    
        response.getWriter().write(returnString);
      }
      catch (Exception e)
      {
        //return super.handleException(request, mapping, e);
        response.getWriter().write(ProductMaintConstants.VPO_RESULTS_ERROR+"\r\n"+e.getMessage());
      }
      
      return null;
    }


  /**
   * setter for the IProductMaintSVCBusiness
   * @param IProductMaintSVCBusiness
   * @return none
   * @exception none
   */
    public void setProductSVC(IProductMaintSVCBusiness productSVC) 
    {
        this.productSVC = productSVC;
    }


  /**
   * getter for the IProductMaintSVCBusiness
   * @param none
   * @return IProductMaintSVCBusiness
   * @exception none
   */
    public IProductMaintSVCBusiness getProductSVC() 
    {
        return productSVC;
    }


  /**
   * get the products based on vendor it
   * @param HttpServletRequest
   * @return String - results 
   * @throws Exception
   * 
   */
    private String getProductListByVendor(HttpServletRequest request) throws Exception
    {
      String selectId = request.getParameter(ProductMaintConstants.AJAX_PROPERTY_ID);            
      String selectName = request.getParameter(ProductMaintConstants.AJAX_PROPERTY_NAME);
      String vendorId = request.getParameter(ProductMaintConstants.AJAX_VENDOR_ID);
      
      StringBuffer sb = new StringBuffer();
      try 
      {
        List list = new ArrayList();

        if (vendorId.equalsIgnoreCase(ProductMaintConstants.ALL_VENDORS_VENDOR_ID))
        {
          list.add(0,ProductMaintConstants.ALL_PRODUCTS_PRODUCT_NAME);
        }
        else
        {
          list.addAll(productSVC.getVendorProducts(vendorId));
          //Greater than one because if there is one, it will be automatically selected
          if (list.size() > 1)
          {
          list.add(0,ProductMaintConstants.ALL_PRODUCTS_PRODUCT_NAME);
          }
        }

        sb.append("<select id=\"");
        sb.append(selectId);
        sb.append("\" name=\"");
        sb.append(selectName);
        sb.append("\">");
        
        for( int idx=0; idx<list.size(); idx++ ) 
        {
          sb.append("<option value=\"");
          sb.append((String)list.get(idx));
          sb.append("\">");
          sb.append((String)list.get(idx));
          sb.append("</option>");
        }
          
        sb.append("</select>");
      } 
      catch (Exception e) 
      {
        logger.error(e);
        throw new Exception(e); 
      }
  
      return sb.toString(); 
    }


  /**
   * get the sds ship methods based on carrier id
   * @param HttpServletRequest
   * @return String - results 
   * @throws Exception
   * 
   */
    private String getSdsShipMethodsByCarrier(HttpServletRequest request) throws Exception
    {
      String selectId = request.getParameter(ProductMaintConstants.AJAX_PROPERTY_ID);            
      String selectName = request.getParameter(ProductMaintConstants.AJAX_PROPERTY_NAME);
      String carrierId = request.getParameter(ProductMaintConstants.AJAX_CARRIER_ID);            
      
      StringBuffer responseHTML = new StringBuffer();
      try 
      {
        List list = new ArrayList();
        list.addAll(productSVC.getCarrierSdsShipMethods(carrierId));
 
        responseHTML.append("<select id=\"");
        responseHTML.append(selectId);
        responseHTML.append("\" name=\"");
        responseHTML.append(selectName);
        responseHTML.append("\" multiple=\"true\" size=\"4\">");
        
        for( int idx=0; idx<list.size(); idx++ ) 
        {
          responseHTML.append("<option value=\"");
          responseHTML.append((String)list.get(idx));
          responseHTML.append("\">");
          responseHTML.append((String)list.get(idx));
          responseHTML.append("</option>");
        }
          
        responseHTML.append("</select>");
      } 
      catch (Exception e) 
      {
        logger.error(e);
        throw new Exception(e); 
      }
  
      return responseHTML.toString(); 
    }


  /**
   * insert the data in the VendorProductOverride table
   * @param vendor id
   * @param product id
   * @param override date
   * @param carrier id
   * @return none
   * @throws Exception
   * 
   */
    private HashMap processSearchRequest(HttpServletRequest request) throws Exception
    {
      String searchBy = request.getParameter(ProductMaintConstants.AJAX_SEARCH_BY);
      String searchOn = request.getParameter(ProductMaintConstants.AJAX_SEARCH_ON);
      String searchDate = request.getParameter(ProductMaintConstants.AJAX_SEARCH_DATE);

      String vendorId = null;
      String productId = null;
      String carrierId = null; 
      String overrideDate = null; 

      if (StringUtils.equalsIgnoreCase(searchBy, "getall"))
      {}
      else if (StringUtils.equalsIgnoreCase(searchBy, "vendor"))
      {
        vendorId = searchOn; 
        overrideDate = searchDate;
      }
      else if (StringUtils.equalsIgnoreCase(searchBy, "product"))
      {
        productId = searchOn.toUpperCase();
        overrideDate = searchDate;
      }
      else if (StringUtils.equalsIgnoreCase(searchBy, "carrier"))
      {
        carrierId = searchOn; 
        overrideDate = searchDate;
      }
      else if (StringUtils.equalsIgnoreCase(searchBy, "date"))
      {
        overrideDate = searchOn; 
      }
      else 
      {
        throw new Exception("invalid search criteria found");
      }
      if (vendorId != null && vendorId.equalsIgnoreCase(ProductMaintConstants.ALL_VENDORS_VENDOR_ID))
      {
        vendorId = null;
      }
      HashMap returnParms = new HashMap(); 
      returnParms.put("vendorId", vendorId); 
      returnParms.put("productId", productId); 
      returnParms.put("carrierId", carrierId); 
      returnParms.put("overrideDate", overrideDate); 

      return returnParms; 
    }


  /**
   * filter the results 
   * @param vendor id
   * @param product id
   * @param override date
   * @param carrier id
   * @return String - results 
   * @throws Exception
   * 
   */
    private String filterVPO(String vendorId, String productId, String carrierId, String overrideDate) throws Exception
    {

      List searchResultsList;
      StringBuffer sb = new StringBuffer();
      try 
      {
        Date searchOnDate = null; 
        if (overrideDate != null)
        {
          SimpleDateFormat sdfInput = new SimpleDateFormat("MM/dd/yyyy");
          searchOnDate = sdfInput.parse(overrideDate);
        }

        //retrieve the results. 
        searchResultsList = productSVC.getVPOverrides(vendorId, productId, searchOnDate, carrierId); 

        //create the output string 
        sb.append("<table border=\"1\"><caption style=\"");
        sb.append("background-color:#336699; color:#eeeeee; font-weight:bold; text-align:center; padding-left: 5px;\"");
        sb.append(">Selected Overrides</caption><tbody><tr bgcolor=\"");
        sb.append("#fffff\"");
        sb.append("><th width=\"");
        sb.append(ProductMaintConstants.VPO_SEARCH_DATE_COLUMN_WIDTH);
        sb.append("\">Del Override Date</th><th width=\"");
        sb.append(ProductMaintConstants.VPO_SEARCH_CARRIER_COLUMN_WIDTH);
        sb.append("\">Carrier</th><th width=\"");
        sb.append(ProductMaintConstants.VPO_SEARCH_SDS_SHIP_METHOD_COLUMN_WIDTH);
        sb.append("\">SDS Ship Method</th><th width=\"");
        sb.append(ProductMaintConstants.VPO_SEARCH_VENDOR_COLUMN_WIDTH);
        sb.append("\">Vendor<input id=\"overrideCount\" type=\"hidden\" value=\"");
        sb.append(searchResultsList.size());
        sb.append("\"/></th><th width=\"");
        sb.append(ProductMaintConstants.VPO_SEARCH_PRODUCT_COLUMN_WIDTH);
        sb.append("\">Product ID</th>");
        sb.append("<th>" +
                  "<a href=\"javascript:checkAll()\"><img src=\"images/all_check.gif\" width=\"42\" height=\"16\" border=\"0\" align=\"absmiddle\" alt=\"Check All\"></a>\n" + 
                  "<a href=\"javascript:uncheckAll()\"><img src=\"images/all_uncheck.gif\" width=\"42\" height=\"16\" border=\"0\" align=\"absmiddle\" alt=\"Uncheck All\"></a>" +
                  "</th>");
        sb.append("</tr>");
        
        //this is where the loop will go
        String backColor = null; 
        for(int i = 0; i < searchResultsList.size(); i++)
        {
          VendorProductOverrideVO vpoVO = (VendorProductOverrideVO)searchResultsList.get(i);
          if (i%2 == 0)
            backColor = "#eeeeee";
          else
            backColor = "#fffff";
            
          sb.append("<TR bgcolor=\"" + backColor + "\">");  
          sb.append("<TD align=\"center\">" + vpoVO.getDeliveryOverrideDateFormatted() + "</TD>");
          sb.append("<TD align=\"center\">" + vpoVO.getCarrierId() + "</TD>");
          sb.append("<TD align=\"center\">" + vpoVO.getSdsShipMethod() + "</TD>");
          sb.append("<TD align=\"center\">" + vpoVO.getVendorId() + "&nbsp;-&nbsp;" + vpoVO.getVendorName() + "</TD>");
          sb.append("<TD align=\"center\">" + vpoVO.getProductId() + "</TD>");
          sb.append("<TD align=\"center\"><input id=check_" + i); 
          sb.append(" vendor=" + vpoVO.getVendorId()); 
          sb.append(" product=" + vpoVO.getProductId()); 
          sb.append(" carrier=" + vpoVO.getCarrierId()); 
          sb.append(" date=" + vpoVO.getDeliveryOverrideDateFormatted()); 
          sb.append(" shipmethod=\"" + vpoVO.getSdsShipMethod() + "\""); 
          sb.append(" type=\"checkbox\" /></TD>");
          sb.append("</TR>");
          
        }
                                        
        sb.append("</tbody></table>");
       
      }

      catch (Exception e) 
      {
        logger.error(e);
        throw new Exception(e); 
      }
    
      String returnString = sb.toString(); 
      return returnString; 

    }


  /**
   * process the add request. 
   * @param HttpServletRequest
   * @return String - results 
   * @throws Exception
   * 
   */
    private void processAddVPO(HttpServletRequest request) throws Exception
    {
      String vendorId = request.getParameter(ProductMaintConstants.AJAX_VENDOR_ID);
      String productId = request.getParameter(ProductMaintConstants.AJAX_PRODUCT_ID);
      String carrierId = request.getParameter(ProductMaintConstants.AJAX_CARRIER_ID);
      String sStartDate = request.getParameter(ProductMaintConstants.AJAX_START_DATE);
      String sEndDate = request.getParameter(ProductMaintConstants.AJAX_END_DATE);
      ArrayList <String> sdsShipMethods = new ArrayList();
      
      int numberOfSelectedSdsShipMethods = Integer.parseInt(request.getParameter(ProductMaintConstants.AJAX_NUMBER_OF_SELECTED_SDS_SHIP_METHODS));
      
      for (int i=0; i < numberOfSelectedSdsShipMethods; i++)
      {
        String sdsShipMethod = request.getParameter(ProductMaintConstants.AJAX_SDS_SHIP_METHOD + i);
        if (sdsShipMethod != "nul")
        {
          sdsShipMethods.add(sdsShipMethod);
        }
        logger.debug("SDS Ship Method " + sdsShipMethod + " added for processing");
      }

      

      SimpleDateFormat sdfInput = new SimpleDateFormat("MM/dd/yyyy");
      Date dEndDate = sdfInput.parse(sEndDate);

      Calendar cProcessing = Calendar.getInstance();
      //Set the Calendar Object using the date retrieved in sStartDate
      cProcessing.setTime(sdfInput.parse(sStartDate));
      //Since the ship date's time is 00:00:00, set today's date time to 00:00:00 too
      cProcessing.set(Calendar.HOUR, 0); 
      cProcessing.set(Calendar.MINUTE, 0); 
      cProcessing.set(Calendar.SECOND, 0); 
      cProcessing.set(Calendar.MILLISECOND, 0);
      cProcessing.set(Calendar.AM_PM, Calendar.AM );
      
      boolean keepAdding = true; 
      
      try
      {
        while (keepAdding)
        {
          for (int i=0; i< sdsShipMethods.size(); i++)
          {
            addVPO(vendorId, productId, carrierId, cProcessing.getTime(), sdsShipMethods.get(i)); 
          }
          
          //get the difference in the ship date and the current date
          int diffInDays = FTDUtil.getDiffInDays(cProcessing.getTime(), dEndDate);

          //if end date is greater than the currrently processed date, add 1 
          //to the currently processing date for the record to be added
          if (diffInDays > 0)
            cProcessing.add(Calendar.DATE, 1);
          //else trip the switch
          else
            keepAdding = false; 
        }
      }
      catch (Exception e)
      {
        logger.error(e);
        throw new Exception(e);
      }
    }

  /**
   * insert the data in the VendorProductOverride table
   * @param vendor id
   * @param product id
   * @param override date
   * @param carrier id
   * @return none
   * @throws Exception
   * 
   */
    private void addVPO(String vendorId, String productId, String carrierId, Date overrideDate, String sdsShipMethod) throws Exception
    {

      //instantiate new VendorProductOverrideVO
      VendorProductOverrideVO vpoVO = new VendorProductOverrideVO();
      
      //set the fields
      vpoVO.setVendorId(vendorId);
      vpoVO.setProductId(productId);
      vpoVO.setCarrierId(carrierId);
      vpoVO.setDeliveryOverrideDate(overrideDate);
      vpoVO.setSdsShipMethod(sdsShipMethod);
      
      //insert data in the VendorProductOverride table
      productSVC.updateVPOverride(vpoVO); 


    }


  /**
   * process the add request. 
   * @param HttpServletRequest
   * @return String - results 
   * @throws Exception
   * 
   */
    private void processDeleteVPO(HttpServletRequest request) throws Exception
    {
      String vendorId = null; 
      String productId = null;
      String carrierId = null; 
      String overrideDate = null; 
      String sdsShipMethod = null;
      SimpleDateFormat sdfInput = new SimpleDateFormat("MM/dd/yyyy");
      String deleteString = request.getParameter(ProductMaintConstants.AJAX_DELETE_STRING); 
      Calendar cOverrideDate = Calendar.getInstance();

      StringTokenizer st = new StringTokenizer(deleteString, "|");
      while (st.hasMoreTokens()) 
      {
        String token = st.nextToken();
        logger.debug("Delete token recieved\n" + token);
        if (token != null && token.length() > 0) 
        {
          StringTokenizer st1 = new StringTokenizer(token, ",");
          vendorId = st1.nextToken(); 
          productId = st1.nextToken(); 
          carrierId = st1.nextToken();
          overrideDate = st1.nextToken(); 
          sdsShipMethod = st1.nextToken();
        
          if (StringUtils.isEmpty(vendorId) &&
              StringUtils.isEmpty(productId) &&
              StringUtils.isEmpty(carrierId) &&
              StringUtils.isEmpty(overrideDate) &&
              StringUtils.isEmpty(sdsShipMethod)
             )
             throw new Exception ("You are trying to delete the entire table. ");

          //Set the Calendar Object using the date retrieved in sStartDate
          cOverrideDate.setTime(sdfInput.parse(overrideDate));
          //Since the ship date's time is 00:00:00, set today's date time to 00:00:00 too
          cOverrideDate.set(Calendar.HOUR, 0); 
          cOverrideDate.set(Calendar.MINUTE, 0); 
          cOverrideDate.set(Calendar.SECOND, 0); 
          cOverrideDate.set(Calendar.MILLISECOND, 0);
          cOverrideDate.set(Calendar.AM_PM, Calendar.AM );

          deleteVPO(vendorId, productId, carrierId, cOverrideDate.getTime(), sdsShipMethod); 

        }
      }
     
    }

  /**
   * insert the data in the VendorProductOverride table
   * @param vendor id
   * @param product id
   * @param override date
   * @param carrier id
   * @return none
   * @throws Exception
   * 
   */
    private void deleteVPO(String vendorId, String productId, String carrierId, Date overrideDate, String sdsShipMethod) throws Exception
    {

      //instantiate new VendorProductOverrideVO
      VendorProductOverrideVO vpoVO = new VendorProductOverrideVO();
      
      //set the fields
      vpoVO.setVendorId(vendorId);
      vpoVO.setProductId(productId);
      vpoVO.setCarrierId(carrierId);
      vpoVO.setDeliveryOverrideDate(overrideDate);
      vpoVO.setSdsShipMethod(sdsShipMethod);
      
      if(logger.isDebugEnabled())
      {
        logger.debug("Object passed on for deletion " +
          " vendorId: " + vpoVO.getVendorId()  +
          " productId: " +  vpoVO.getProductId() +
          " carrierId: " + vpoVO.getCarrierId() +
          " deliveryOverrideDate: " + vpoVO.getDeliveryOverrideDate().toString()+
          " sdsShipMethod: " + vpoVO.getSdsShipMethod());
      }
      
      //insert data in the VendorProductOverride table
      productSVC.deleteVPOverride(vpoVO); 


    }


}
