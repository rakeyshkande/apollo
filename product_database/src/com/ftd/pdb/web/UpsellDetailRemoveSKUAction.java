package com.ftd.pdb.web;

import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.exceptions.PDBException;
import com.ftd.pdb.common.valobjs.UpsellDetailVO;
import com.ftd.pdb.common.valobjs.UpsellMasterVO;
import com.ftd.pdb.common.valobjs.ValueVO;
import com.ftd.pdb.service.ILookupSVCBusiness;
import com.ftd.pdb.service.IUpsellSVCBusiness;

import java.io.IOException;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.BooleanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * Implementation of <strong>Action</strong> that processes a produst
 * list request.
 *
 * @author Ed Mueller
 */
public class UpsellDetailRemoveSKUAction  extends PDBAction
{
    /*Spring managed resources */
    private ILookupSVCBusiness lookupSVC;
    private IUpsellSVCBusiness upsellSVC;
    /*end Spring managed resources */
    
    public UpsellDetailRemoveSKUAction()
    {
        super("com.ftd.pdb.web.UpsellDetailRemoveSKUAction");
    }

	/**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param mapping The ActionMapping used to select this instance
     * @param form The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     */
    public ActionForward execute(ActionMapping mapping,
				 ActionForm form,
				 HttpServletRequest request,
				 HttpServletResponse response)
	throws IOException, ServletException
    {
    try{
        //get form object
        UpsellDetailForm inputForm = (UpsellDetailForm)form;

        // Get all values needed to prepopulate the form  
         request.setAttribute(ProductMaintConstants.RECIPIENT_SEARCH_KEY, lookupSVC.getRecipientSearchList());
         request.setAttribute(ProductMaintConstants.SEARCH_PRIORITY_KEY, lookupSVC.getSearchPriorityList());
         request.setAttribute(ProductMaintConstants.PDB_COMPANY_MASTER_KEY, lookupSVC.getCompanyMasterList());

        // Set the back link
        request.setAttribute(ProductMaintConstants.MENU_BACK_KEY, ProductMaintConstants.MENU_BACK_UPSELL_LIST_KEY);
        request.setAttribute(ProductMaintConstants.MENU_BACK_TEXT_KEY, ProductMaintConstants.MENU_BACK_TEXT_UPSELL_LIST_KEY);

        //get deleted sku from request
        String deletesku = request.getParameter(ProductMaintConstants.PDB_DELETED_SKU_KEY);

        logger.debug("deletesku: " + deletesku);

        UpsellMasterVO upsellMasterVO = new UpsellMasterVO();               

        //Builder MasterVo using data on request
        upsellMasterVO.setMasterID((String)inputForm.get("masterSKU"));
        //upsellMasterVO.setMasterStatus((Boolean)inputForm.get("arrangementAvailable"));
        upsellMasterVO.setMasterStatus(BooleanUtils.toBoolean((Boolean)inputForm.get("arrangementAvailable")));
        upsellMasterVO.setMasterName((String)inputForm.get("productName"));
        upsellMasterVO.setMasterDescription((String)inputForm.get("description"));
        String[] companyArray = (String[])inputForm.get("companyArray");
        upsellMasterVO.setCompanyList(companyArray);

        List upsellDetailIdList = new ArrayList();
        ValueVO valueVO = new ValueVO();
        valueVO.setId("");
        valueVO.setDescription("");
        upsellDetailIdList.add(valueVO);

         //Get detail SKUs from request
         List detailArray = new ArrayList();
          try{
                detailArray = upsellSVC.getSKUsFromRequest(request);
                }
            catch(PDBException ftdException)    {  
                return super.handleException(request, mapping, ftdException);
                }           

                //remove the deleted sku from array
                ArrayList  newDetailList = new ArrayList();
                for(int i=0; i < detailArray.size();i++){
                    UpsellDetailVO detailVO = (UpsellDetailVO)detailArray.get(i);
                    if(!detailVO.getDetailID().equals(deletesku)){
                        newDetailList.add(detailVO);
                        valueVO = new ValueVO();
                        valueVO.setId(detailVO.getDetailID());
                        valueVO.setDescription(detailVO.getDetailID());
                        upsellDetailIdList.add(valueVO);
                    }
                }



                upsellMasterVO.setUpsellDetailList(newDetailList);    
                request.setAttribute("upsellDetailIdList", upsellDetailIdList);
                request.setAttribute(ProductMaintConstants.SHOW_ASSOCIATE_LIST_KEY,newDetailList);     
                request.setAttribute(mapping.getAttribute(), form);

        }
        catch(Exception e)
        {
            return super.handleException(request, mapping, e);
        }

          //ed mueller, 7/24/03 - fix bug when which occurs when adding sku to upsell
            //reset error flags
            request.setAttribute(ProductMaintConstants.PDB_PRODUCT_UPSELL_ERROR_DESC_KEY, new String[ProductMaintConstants.PCB_UPSELL_MAINT_ERROR_MAX_IDX]);
            request.setAttribute(ProductMaintConstants.PDB_PRODUCT_UPSELL_ASSOC_ID_ERROR_KEY, new String[ProductMaintConstants.UPSELL_MAX_ASSOC_SKU_AMOUNT]);
            request.setAttribute(ProductMaintConstants.PDB_PRODUCT_UPSELL_ASSOC_NAME_ERROR_KEY, new String[ProductMaintConstants.UPSELL_MAX_ASSOC_SKU_AMOUNT]);
            request.setAttribute(ProductMaintConstants.PDB_PRODUCT_UPSELL_ASSOC_PRICE_ERROR_KEY, new String[ProductMaintConstants.UPSELL_MAX_ASSOC_SKU_AMOUNT]);
            request.setAttribute(ProductMaintConstants.PDB_PRODUCT_UPSELL_ASSOC_TYPE_ERROR_KEY, new String[ProductMaintConstants.UPSELL_MAX_ASSOC_SKU_AMOUNT]);
            request.setAttribute(ProductMaintConstants.PDB_PRODUCT_UPSELL_ASSOC_STATUS_ERROR_KEY, new String[ProductMaintConstants.UPSELL_MAX_ASSOC_SKU_AMOUNT]);
            //end, ed 7/27/03
          
		// Forward control to the specified success URI
		return (mapping.findForward("success"));
    }

    public void setLookupSVC(ILookupSVCBusiness lookupSVC) {
        this.lookupSVC = lookupSVC;
    }

    public ILookupSVCBusiness getLookupSVC() {
        return lookupSVC;
    }

    public void setUpsellSVC(IUpsellSVCBusiness upsellSVC) {
        this.upsellSVC = upsellSVC;
    }

    public IUpsellSVCBusiness getUpsellSVC() {
        return upsellSVC;
    }
}
