package com.ftd.pdb.web;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;


public final class ShippingKeyContentForm extends PDBActionForm {
    private static final String PRICE_CHANGE_BY_INCREASE = "plus";
    //    private static final String PRICE_CHANGE_BY_DECREASE = "minus";


    public ShippingKeyContentForm() {
        super("com.ftd.pdb.web.ShippingKeyContentForm");
        this.set("global_price_change_by", PRICE_CHANGE_BY_INCREASE);
    }

    /**
     * Reset all properties to their default values.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public void reset(ActionMapping mapping, HttpServletRequest request) {

    }

    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public ActionErrors validate(ActionMapping mapping, 
                                 HttpServletRequest request) {

        ActionErrors errors = new ActionErrors();

        //        if ((searchCriteria == null) || (searchCriteria.trim().equals(ProductMaintConstants.OE_BLANK_STRING)))
        //       {
        //          errors.add("searchCriteria", new ActionError("error.searchcriteria.required"));
        //        }

        return errors;
    }
}
