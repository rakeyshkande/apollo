package com.ftd.pdb.web;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.StoreFeedUtil;
import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBException;
import com.ftd.pdb.common.utilities.FTDUtil;
import com.ftd.pdb.common.valobjs.ShippingKeyPriceVO;
import com.ftd.pdb.common.valobjs.ShippingKeyVO;
import com.ftd.pdb.common.valobjs.ShippingMethodVO;
import com.ftd.pdb.common.valobjs.ValueVO;
import com.ftd.pdb.service.IShippingKeySVCService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


public final class AddShippingKeyAction extends PDBAction 
{
    
    private final String BASE_CONFIG = "BASE_CONFIG";
    private final String BASE_URL = "BASE_URL";
    /* Spring managed resources */
    private IShippingKeySVCService shippingKeySVC;
    /* end Spring managed resources */

    public AddShippingKeyAction() {
        super("com.ftd.pdb.web.AddShippingKeyAction");
    }

    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param mapping The ActionMapping used to select this instance
     * @param form The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     */
    public

    ActionForward execute(ActionMapping mapping, ActionForm form, 
                          HttpServletRequest request, 
                          HttpServletResponse response) throws IOException, 
                                                               ServletException {
        
        String baseUrl="";
        ShippingKeyDetailForm addShippingKeyForm = (ShippingKeyDetailForm)form;

        String selectedShippingKeyID = (String)addShippingKeyForm.get("shippingKey");

        ShippingKeyVO vo = new ShippingKeyVO();
        vo.setShippingKey_ID(selectedShippingKeyID);
        vo.setShipperID((String)addShippingKeyForm.get("shipper"));
        vo.setShippingKeyDescription((String)addShippingKeyForm.get("description"));

        try {

            // remove the empty input rows
    
            List priceVO_list = new ArrayList();
            String [] minPriceList = addShippingKeyForm.getStrings("minPrice");
            String [] maxPriceList = addShippingKeyForm.getStrings("maxPrice");
            String [] standardCostList = addShippingKeyForm.getStrings("standardCost");
            String [] twoDayCostList = addShippingKeyForm.getStrings("twoDayCost");
            String [] nextDayCostList = addShippingKeyForm.getStrings("nextDayCost");
            String [] saturdayCostList = addShippingKeyForm.getStrings("saturdayCost");
            String [] sundayCostList = addShippingKeyForm.getStrings("sundayCost");
            String [] sameDayCostList = addShippingKeyForm.getStrings("sameDayCost");

            for (int i = 0; i < ShippingKeyDetailForm.NUMBER_OF_ROW; i++) {
                //TODO:  ShippKeyDetailAction also checks and sets "sameDayCost".  
                //       Should that be done here too?
                if (StringUtils.isNotBlank(minPriceList[i]) || 
                    StringUtils.isNotBlank(maxPriceList[i]) || 
                    StringUtils.isNotBlank(standardCostList[i]) || 
                    StringUtils.isNotBlank(twoDayCostList[i]) || 
                    StringUtils.isNotBlank(nextDayCostList[i]) || 
                    StringUtils.isNotBlank(saturdayCostList[i]) ||
                    StringUtils.isNotBlank(sameDayCostList[i])) {
                    ShippingKeyPriceVO priceVO = new ShippingKeyPriceVO();
                    priceVO.setShippingKeyID(selectedShippingKeyID);
                    priceVO.setShippingKeyPriceID(String.valueOf(i));
                    if (StringUtils.isBlank((String)minPriceList[i]) )
                        priceVO.setMinPrice(Double.valueOf("0.0"));
                    else
                        priceVO.setMinPrice(Double.valueOf(FTDUtil.removeFormattingCurrency(minPriceList[i])));
                    if( StringUtils.isBlank((String)maxPriceList[i]))
                        priceVO.setMaxPrice(Double.valueOf("0.0"));
                    else
                        priceVO.setMaxPrice(Double.valueOf(FTDUtil.removeFormattingCurrency(maxPriceList[i])));
    
                    List methods = new ArrayList();
                    ShippingMethodVO methodVO = new ShippingMethodVO();
                    methodVO.setPriceId(priceVO.getShippingKeyPriceID());
                    methodVO.setId(ProductMaintConstants.SHIPPING_STANDARD_CODE);
                    float fltPrice = 0;
                    String strPrice = standardCostList[i];
                    if (strPrice != null && strPrice.length() > 0) {
                        fltPrice = 
                                Float.parseFloat(FTDUtil.removeFormattingCurrency(strPrice));
                    }
                    methodVO.setCost(fltPrice);
                    methods.add(methodVO);
    
                    methodVO = new ShippingMethodVO();
                    methodVO.setPriceId(priceVO.getShippingKeyPriceID());
                    methodVO.setId(ProductMaintConstants.SHIPPING_TWO_DAY_CODE);
                    fltPrice = 0;
                    strPrice = twoDayCostList[i];
                    if (strPrice != null && strPrice.length() > 0) {
                        fltPrice = 
                                Float.parseFloat(FTDUtil.removeFormattingCurrency(strPrice));
                    }
                    methodVO.setCost(fltPrice);
                    methods.add(methodVO);
    
                    methodVO = new ShippingMethodVO();
                    methodVO.setPriceId(priceVO.getShippingKeyPriceID());
                    methodVO.setId(ProductMaintConstants.SHIPPING_NEXT_DAY_CODE);
                    fltPrice = 0;
                    strPrice = nextDayCostList[i];
                    if (strPrice != null && strPrice.length() > 0) {
                        fltPrice = 
                                Float.parseFloat(FTDUtil.removeFormattingCurrency(strPrice));
                    }
                    methodVO.setCost(fltPrice);
                    methods.add(methodVO);
    
                    methodVO = new ShippingMethodVO();
                    methodVO.setPriceId(priceVO.getShippingKeyPriceID());
                    methodVO.setId(ProductMaintConstants.SHIPPING_SATURDAY_CODE);
                    fltPrice = 0;
                    strPrice = (String)saturdayCostList[i];
                    if (strPrice != null && strPrice.length() > 0) {
                        fltPrice = 
                                Float.parseFloat(FTDUtil.removeFormattingCurrency(strPrice));
                    }
                    
                    methodVO.setCost(fltPrice);
                    methods.add(methodVO);
                    
                    methodVO = new ShippingMethodVO();
                    methodVO.setPriceId(priceVO.getShippingKeyPriceID());
                    methodVO.setId(ProductMaintConstants.SHIPPING_SAME_DAY_CODE);
                    fltPrice = 0;
                    strPrice = sameDayCostList[i];
                    if (strPrice != null && strPrice.length() > 0) {
                        fltPrice = 
                                Float.parseFloat(FTDUtil.removeFormattingCurrency(strPrice));
                    }
                    
                    methodVO.setCost(fltPrice);
                    methods.add(methodVO);
                    
                    methodVO = new ShippingMethodVO();
                    methodVO.setPriceId(priceVO.getShippingKeyPriceID());
                    methodVO.setId(ProductMaintConstants.SHIPPING_SUNDAY_CODE);
                    fltPrice = 0;
                    strPrice = (String)sundayCostList[i];
                                        
                    if (strPrice != null && strPrice.length() > 0) {
                        fltPrice = 
                                Float.parseFloat(FTDUtil.removeFormattingCurrency(strPrice));
                    }
                    
                    methodVO.setCost(fltPrice);
                    methods.add(methodVO);
                    
                    priceVO.setShippingMethods(methods);
    
                    priceVO_list.add(priceVO);
                }
            }
    
            vo.setPrices(priceVO_list);
            vo.setUpdatedBy(getUserId(request));
            vo.setCreatedBy(getUserId(request));
        }
        catch (Throwable t)
        {
            logger.error(t);
        }

        // Get the Novator servers to send to
        Map novatorMap = new HashMap();
        try {
            
            
            ConfigurationUtil rm = ConfigurationUtil.getInstance();
            baseUrl = rm.getFrpGlobalParm(BASE_CONFIG,BASE_URL);

            if (addShippingKeyForm.get("chkLive") != null && ((Boolean)addShippingKeyForm.get("chkLive"))) {
                String key = 
                    rm.getProperty(ProductMaintConstants.PROPERTY_FILE, 
                                   ProductMaintConstants.PDB_NOVATOR_LIVE_IP_KEY);
                String value = 
                    rm.getProperty(ProductMaintConstants.PROPERTY_FILE, 
                                   ProductMaintConstants.PDB_NOVATOR_LIVE_PORT_KEY);
                ValueVO mapVO = new ValueVO();
                mapVO.setId(key);
                mapVO.setDescription(value);
                novatorMap.put("live", mapVO);
            }
            if (addShippingKeyForm.get("chkTest") != null && ((Boolean)addShippingKeyForm.get("chkTest"))) {
                String key = 
                    rm.getProperty(ProductMaintConstants.PROPERTY_FILE, 
                                   ProductMaintConstants.PDB_NOVATOR_TEST_IP_KEY);
                String value = 
                    rm.getProperty(ProductMaintConstants.PROPERTY_FILE, 
                                   ProductMaintConstants.PDB_NOVATOR_TEST_PORT_KEY);
                ValueVO mapVO = new ValueVO();
                mapVO.setId(key);
                mapVO.setDescription(value);
                novatorMap.put("test", mapVO);
            }
            if (addShippingKeyForm.get("chkUAT") != null && ((Boolean)addShippingKeyForm.get("chkUAT"))) {
                String key = 
                    rm.getProperty(ProductMaintConstants.PROPERTY_FILE, 
                                   ProductMaintConstants.PDB_NOVATOR_UAT_IP_KEY);
                String value = 
                    rm.getProperty(ProductMaintConstants.PROPERTY_FILE, 
                                   ProductMaintConstants.PDB_NOVATOR_UAT_PORT_KEY);
                ValueVO mapVO = new ValueVO();
                mapVO.setId(key);
                mapVO.setDescription(value);
                novatorMap.put("uat", mapVO);
            }
            if (addShippingKeyForm.get("chkContent") != null && ((Boolean)addShippingKeyForm.get("chkContent"))) {
                String key = 
                    rm.getProperty(ProductMaintConstants.PROPERTY_FILE, 
                                   ProductMaintConstants.PDB_NOVATOR_CONTENT_IP_KEY);
                String value = 
                    rm.getProperty(ProductMaintConstants.PROPERTY_FILE, 
                                   ProductMaintConstants.PDB_NOVATOR_CONTENT_PORT_KEY);
                ValueVO mapVO = new ValueVO();
                mapVO.setId(key);
                mapVO.setDescription(value);
                novatorMap.put("content", mapVO);
            }
            
        } catch (Exception e) {
            logger.error("Configuration property not found", e);
            throw new ServletException("Configuration property not found", e);
        }

        try {
            shippingKeySVC.insertShippingKey(vo, novatorMap);
            
            // Once the shipping key is inserted - post a message to STORE_FEEDS queue to send shipping key feed.
            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
			String allowedFeedTypes = configUtil.getFrpGlobalParm("PI_CONFIG", "PROCESS_FEEDS");
			if (allowedFeedTypes!=null && allowedFeedTypes.contains("SHIPPING_KEY_FEED")) {
				
				StoreFeedUtil util = new StoreFeedUtil();
				String msgTxt = "SHIPPING_KEY_FEED "
						+ vo.getShippingKey_ID()+" Add";
				if (logger.isDebugEnabled()) {
					logger.debug("Shipping key updated. Save Store feed, "
							+ msgTxt);
				}
				util.postStoreFeedMessage(msgTxt, msgTxt);
			}  
			
        } catch (Exception e) {
            Exception exception = null;
            try {
                Class class1 = e.getClass();
                Class class2 = 
                    Class.forName(ProductMaintConstants.PDBSYSTEMEXCEPTION_CLASS_NAME);
                Class class3 = 
                    Class.forName(ProductMaintConstants.PDBAPPLICATIONEXCEPTION_CLASS_NAME);
                if (!class1.equals(class2) && !class1.equals(class3)) {
                    String[] args = new String[1];
                    args[0] = new String(e.getMessage());
                    logger.error(e);
                    PDBException ftde = 
                        new PDBApplicationException(ProductMaintConstants.GENERAL_APPLICATION_EXCEPTION, 
                                                    args, e);
                    exception = ftde;
                } else {
                    exception = e;
                }
            } catch (ClassNotFoundException cnfe) {
                logger.error(cnfe);
            }

                                                               
            catch (Throwable t)
            {
                logger.error(t);
            }
                                                                                                           
       

            return super.handleException(request, mapping, exception);
        }

        // Set the back link
        request.setAttribute(ProductMaintConstants.MENU_BACK_KEY, 
                             ProductMaintConstants.MENU_BACK_SHIPPING_KEY_KEY);
        request.setAttribute(ProductMaintConstants.MENU_BACK_TEXT_KEY, 
                             ProductMaintConstants.MENU_BACK_TEXT_SHIPPING_KEY_KEY);

        StringBuffer newPath = new StringBuffer(mapping.findForward("success").getPath());
           newPath.append("?securitytoken=" + request.getParameter("securitytoken") + "&context=" + request.getParameter("context") + 
                          "&actionType=" + request.getParameter("actionType") + "&shippingKey=" + request.getParameter("shippingKey") + "&actionStatus=successful");
        return (new ActionForward(baseUrl + newPath.toString(), true));
    }

    public void setShippingKeySVC(IShippingKeySVCService shippingKeySVC) {
        this.shippingKeySVC = shippingKeySVC;
    }

    public IShippingKeySVCService getShippingKeySVC() {
        return shippingKeySVC;
    }    
}
