package com.ftd.pdb.web;

import com.ftd.pdb.common.ProductMaintConstants;

import com.ftd.pdb.service.IProductMaintSVCBusiness;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.springframework.context.ApplicationContext;


public class ProductMaintDetailReloadFlowAction extends PDBAction {

    public ProductMaintDetailReloadFlowAction() {
        super("com.ftd.pdb.web.ProductMaintDetailReloadFlowAction");
    }

    public ActionForward execute(ActionMapping mapping, ActionForm actionForm, 
                                 HttpServletRequest request, 
                                 HttpServletResponse response) throws Exception {

        ProductMaintDetailForm form = (ProductMaintDetailForm)actionForm;
        HttpSession session = request.getSession();
        
        //Selection list values
        form.set(ProductMaintConstants.CATEGORIES_KEY,session.getAttribute(ProductMaintConstants.CATEGORIES_KEY));
        form.set(ProductMaintConstants.EXCEPTION_CODES_KEY,session.getAttribute(ProductMaintConstants.EXCEPTION_CODES_KEY));
        form.set(ProductMaintConstants.PRICE_POINTS_KEY,session.getAttribute(ProductMaintConstants.PRICE_POINTS_KEY));
        form.set(ProductMaintConstants.SHIPPING_KEYS_KEY,session.getAttribute(ProductMaintConstants.SHIPPING_KEYS_KEY));
        form.set(ProductMaintConstants.RECIPIENT_SEARCH_KEY,session.getAttribute(ProductMaintConstants.RECIPIENT_SEARCH_KEY));
        form.set(ProductMaintConstants.SEARCH_PRIORITY_KEY,session.getAttribute(ProductMaintConstants.SEARCH_PRIORITY_KEY));
        form.set(ProductMaintConstants.SHIPPING_METHODS_KEY,session.getAttribute(ProductMaintConstants.SHIPPING_METHODS_KEY));
        form.set(ProductMaintConstants.PDB_JCPENNEY_CATEGORY_KEY,session.getAttribute(ProductMaintConstants.PDB_JCPENNEY_CATEGORY_KEY));
        form.set(ProductMaintConstants.PDB_CARRIER_KEY,session.getAttribute(ProductMaintConstants.PDB_CARRIER_KEY));
        form.set(ProductMaintConstants.COUNTRIES_KEY,session.getAttribute(ProductMaintConstants.COUNTRIES_KEY));
        form.set(ProductMaintConstants.PDB_VENDOR_CARRIER_LIST_KEY,session.getAttribute(ProductMaintConstants.PDB_VENDOR_CARRIER_LIST_KEY));
        form.set(ProductMaintConstants.PDB_COMPANY_MASTER_KEY,session.getAttribute(ProductMaintConstants.PDB_COMPANY_MASTER_KEY));
        form.set(ProductMaintConstants.PRODUCT_TYPES_KEY, session.getAttribute(ProductMaintConstants.PRODUCT_TYPES_KEY));
        form.set(ProductMaintConstants.PRODUCT_SUB_TYPES_KEY, session.getAttribute(ProductMaintConstants.PRODUCT_SUB_TYPES_KEY));
        form.set(ProductMaintConstants.SECOND_CHOICE_KEY, session.getAttribute(ProductMaintConstants.SECOND_CHOICE_KEY));
        form.set(ProductMaintConstants.VENDOR_LIST_KEY, session.getAttribute(ProductMaintConstants.VENDOR_LIST_KEY));
        form.set(ProductMaintConstants.SHIPPING_AVAIL_KEY, session.getAttribute(ProductMaintConstants.SHIPPING_AVAIL_KEY));
        form.set(ProductMaintConstants.PDB_SHIPPING_SYSTEMS_KEY, session.getAttribute(ProductMaintConstants.PDB_SHIPPING_SYSTEMS_KEY));
        form.set(ProductMaintConstants.BOX_KEY, session.getAttribute(ProductMaintConstants.BOX_KEY));
        form.set(ProductMaintConstants.SERVICES_DURATION_KEY, session.getAttribute(ProductMaintConstants.SERVICES_DURATION_KEY));
        form.set(ProductMaintConstants.ALLOW_FREE_SHIPPING_KEY, session.getAttribute(ProductMaintConstants. ALLOW_FREE_SHIPPING_KEY));
        //Product specific values
        //TODO:  These values are going to have to be pulled from page, the value objects rebuilt, and saved in both the 
        //session and the form.
        form.set(ProductMaintConstants.LAST_SUBCODE_KEY, session.getAttribute(ProductMaintConstants.LAST_SUBCODE_KEY));
        form.set(ProductMaintConstants.SUB_CODE_LIST_KEY, session.getAttribute(ProductMaintConstants.SUB_CODE_LIST_KEY));
        form.set(ProductMaintConstants.PRODUCT_CURRENT_SUB_TYPES_KEY, session.getAttribute(ProductMaintConstants.PRODUCT_CURRENT_SUB_TYPES_KEY));
        form.set(ProductMaintConstants.EXCLUDED_STATES, session.getAttribute(ProductMaintConstants.EXCLUDED_STATES));
        form.set(ProductMaintConstants.INCLUDED_COLORS, session.getAttribute(ProductMaintConstants.INCLUDED_COLORS));            
        form.set(ProductMaintConstants.EXCLUDED_COLORS, session.getAttribute(ProductMaintConstants.EXCLUDED_COLORS));
        form.set(ProductMaintConstants.EXCLUDED_COLORS_STATIC, session.getAttribute(ProductMaintConstants.EXCLUDED_COLORS));
        form.set(ProductMaintConstants.INCLUDED_COLORS_STATIC, session.getAttribute(ProductMaintConstants.INCLUDED_COLORS));
        form.set(ProductMaintConstants.SHIPPING_METHODS_KEY, session.getAttribute(ProductMaintConstants.SHIPPING_METHODS_KEY));
        form.set(ProductMaintConstants.CURRENT_COUNTRY_LIST_KEY, session.getAttribute(ProductMaintConstants.CURRENT_COUNTRY_LIST_KEY));
        List vendorProducts = ProductMaintDetailForm.extractVendorProductsListFromMap(form.getMap());
        session.setAttribute(ProductMaintConstants.VENDOR_PRODUCTS, vendorProducts);
        form.set(ProductMaintConstants.VENDOR_PRODUCTS, session.getAttribute(ProductMaintConstants.VENDOR_PRODUCTS));
        form.set(ProductMaintConstants.INCLUDED_COMPONENT_SKUS, session.getAttribute(ProductMaintConstants.INCLUDED_COMPONENT_SKUS));            
        form.set(ProductMaintConstants.AVAILABLE_COMPONENT_SKUS, session.getAttribute(ProductMaintConstants.AVAILABLE_COMPONENT_SKUS));
        
        List subcodeList = ProductMaintDetailForm.extractSubcodeListFromMap(form.getMap());
        session.setAttribute(ProductMaintConstants.SUB_CODE_LIST_KEY, subcodeList);
        request.setAttribute(ProductMaintConstants.MENU_BACK_KEY, 
                             ProductMaintConstants.MENU_BACK_PRODUCT_MAINT_LIST_KEY);
        request.setAttribute(ProductMaintConstants.MENU_BACK_TEXT_KEY, 
                             ProductMaintConstants.MENU_BACK_TEXT_PRODUCT_MAINT_LIST_KEY);
        form.set(ProductMaintConstants.SUB_CODE_LIST_KEY, session.getAttribute(ProductMaintConstants.SUB_CODE_LIST_KEY));
        form.set(ProductMaintConstants.PRODUCT_ADD_ON_LIST,session.getAttribute(ProductMaintConstants.PRODUCT_ADD_ON_LIST));
        form.set(ProductMaintConstants.PRODUCT_ADD_ON_VASE_LIST,session.getAttribute(ProductMaintConstants.PRODUCT_ADD_ON_VASE_LIST));

        return (mapping.findForward("success"));
    }
}
