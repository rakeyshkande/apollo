package com.ftd.pdb.web;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.exceptions.PDBException;
import com.ftd.pdb.common.valobjs.ProductVO;
import com.ftd.pdb.common.valobjs.ValueVO;
import com.ftd.pdb.service.IProductMaintSVCBusiness;

import java.io.IOException;
import java.io.PrintWriter;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


public class UpdateNovatorProductsAction extends PDBAction {
    /*Spring managed resources */
    private IProductMaintSVCBusiness productSVC;
    /*end Spring managed resources */

    private static final SimpleDateFormat format = 
        new SimpleDateFormat("MM/dd/yyyy");

    public UpdateNovatorProductsAction() {
        super("com.ftd.pdb.web.UpdateNovatorProductsAction");
    }

    public ActionForward execute(ActionMapping mapping, ActionForm form, 
                                 HttpServletRequest request, 
                                 HttpServletResponse response) throws IOException, 
                                                                      ServletException {

        String responseText = "";

        try {
            String passedParameter = request.getParameter("productId");
            if (passedParameter == null) {
                passedParameter = request.getParameter("lastUpdate");
                if (passedParameter != null) {

                    List list = 
                        productSVC.getProductsUpdatedSince(format.parse(passedParameter));

                    Iterator it = list.iterator();
                    StringBuffer sb = new StringBuffer();

                    while (it.hasNext()) {
                        sb.append(it.next());
                        sb.append(" ");
                    }


                    if (sb.length() == 0) {
                        sb.append("NO_PRODUCTS_FOUND");
                    }

                    responseText = sb.toString();
                } else {
                    logger.error("Unknown operation");
                    throw new ServletException("Unknown operation!");
                }
            } else {
                StringBuffer sb = new StringBuffer();
                passedParameter = passedParameter.trim();
                if (passedParameter.length() > 0) {
                    ProductVO productVO;
                    try {
                        productVO = productSVC.getProduct(passedParameter);
                    } catch (Exception e) {
                        logger.error("No record found for product " + 
                                     passedParameter, e);
                        throw new ServletException("No record found for product " + 
                                                   passedParameter);
                    }

                    if (productVO == null) {
                        logger.error("No record found for product " + 
                                     passedParameter);
                        throw new ServletException("No record found for product " + 
                                                   passedParameter);
                    }

                    List envKeys = null;
                    
                    //Set Novator environment values to null so the current
                    //database values are not overwritten if the 
                    //product was not fed to the specific environment
                    productVO.setSentToNovatorProd(null);
                    productVO.setSentToNovatorTest(null);
                    productVO.setSentToNovatorUAT(null);
                    productVO.setSentToNovatorContent(null);

                    if (request.getParameter("novatorUpdateLive") != null &&
                          request.getParameter("novatorUpdateLive").equals("true")) {
                        try {
                            envKeys = new ArrayList();
                            envKeys.add(com.ftd.osp.utilities.feed.NovatorFeedUtil.PRODUCTION);
                            logger.debug("Sending update to Live server");
                            productSVC.sendProductToNovator(productVO, envKeys, false);
                            logger.debug("Sent update to Live server\n");
                            productVO.setSentToNovatorProd(true);

                            sb.append("\nLive: Success");
                        }
                        catch(Exception e) {
                              logger.error(e);
                              sb.append("\nLive : Error");  
                        }
                    } 
                    if (request.getParameter("novatorUpdateTest") != null &&
                                request.getParameter("novatorUpdateTest").equals("true")) {
                        try {
                            envKeys = new ArrayList();
                            envKeys.add(com.ftd.osp.utilities.feed.NovatorFeedUtil.TEST);
                            logger.debug("Sending update to Test server");
                            productSVC.sendProductToNovator(productVO, envKeys, false);
                            logger.debug("Sent update to Test server\n");
                            productVO.setSentToNovatorTest(true);

                            sb.append("\nTest: Success");
                        }
                        catch(Exception e) {
                              logger.error(e);
                              sb.append("\nTest : Error");  
                        }
                    } 
                    if (request.getParameter("novatorUpdateUAT") != null &&
                                request.getParameter("novatorUpdateUAT").equals("true")) {
                        try {
                            envKeys = new ArrayList();
                            envKeys.add(com.ftd.osp.utilities.feed.NovatorFeedUtil.UAT);
                            logger.debug("Sending update to UAT server");
                            productSVC.sendProductToNovator(productVO, envKeys, false);
                            logger.debug("Sent update to UAT server\n");
                            productVO.setSentToNovatorUAT(true);

                            sb.append("\nUAT: Success");
                        }
                        catch(Exception e) {
                              logger.error(e);
                              sb.append("\nUAT : Error");  
                        }
                    } 
                    if (request.getParameter("novatorUpdateContent") != null &&
                                request.getParameter("novatorUpdateContent").equals("true")) {
                        try {
                            envKeys = new ArrayList();
                            envKeys.add(com.ftd.osp.utilities.feed.NovatorFeedUtil.CONTENT);
                            logger.debug("Sending update to Content server");
                            productSVC.sendProductToNovator(productVO, envKeys, false);
                            logger.debug("Sent update to Content server\n");
                            productVO.setSentToNovatorContent(true);
                        
                            sb.append("\nContent: Success");
                        }
                        catch(Exception e) {
                              logger.error(e);
                              sb.append("\nContent : Error");  
                        }
                    }
                    responseText = sb.toString();
                } else {
                    responseText = "no product id found...continuing";
                }
            }
        }
        catch (ParseException pe) {
            logger.error("Invalid date passed into UpdateNovatorProductsAction");
            logger.error(pe);
            responseText = "\nError: "+pe.getMessage();
            //throw new ServletException(pe);
        } catch (Exception e) {
            logger.error(e);
            responseText = "\nError: "+e.getMessage();
            //throw new ServletException(e);  
        } finally {
            //response.setContentType("text/html");
            response.setContentType("text/plain");
            PrintWriter out = response.getWriter();
            out.println(responseText);
            out.flush();
        }

        return null;
    }

    public void setProductSVC(IProductMaintSVCBusiness productSVC) {
        this.productSVC = productSVC;
    }

    public IProductMaintSVCBusiness getProductSVC() {
        return productSVC;
    }
}
