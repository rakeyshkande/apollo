package com.ftd.pdb.web;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBException;
import com.ftd.pdb.common.exceptions.PDBSystemException;
import com.ftd.pdb.common.valobjs.ProductBatchVO;
import com.ftd.pdb.common.valobjs.ProductVO;
import com.ftd.pdb.common.valobjs.UpsellMasterVO;
import com.ftd.pdb.common.valobjs.ValueVO;
import com.ftd.pdb.service.ILookupSVCBusiness;
import com.ftd.pdb.service.IProductBatchSVCBusiness;
import com.ftd.pdb.service.IProductMaintSVCBusiness;
import com.ftd.pdb.service.IUpsellSVCBusiness;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.apache.commons.lang.StringUtils;


public class ProductMaintListAction extends PDBAction {
    /*Spring managed resources */
    ILookupSVCBusiness lookupSVC;
    IProductBatchSVCBusiness productBatchSVC;
    IProductMaintSVCBusiness productSVC;
    IUpsellSVCBusiness upsellSVC;
    /*end Spring managed resources */

    public ProductMaintListAction() {
        super("com.ftd.pdb.struts.ProductMaintListAction");
    }

    public ActionForward execute(ActionMapping mapping, ActionForm form, 
                                 HttpServletRequest request, 
                                 HttpServletResponse response) throws IOException, 
                                                                      ServletException {
                                                            
        HttpSession session = request.getSession();
        // Validate the request parameters specified by the user
        ActionMessages errors = new ActionMessages();
        boolean errorFound = false;
        String forwardStr = "error";

        ProductMaintListForm productListForm = (ProductMaintListForm)form;
        String productId = StringUtils.upperCase((String)productListForm.get("productId"));
        String searchBy = (String)productListForm.get("searchBy");
        String prodAvail = (String)productListForm.get("prodAvail");
        String prodUnavail = (String)productListForm.get("prodUnavail");
        String dateRestrict = (String)productListForm.get("dateRestrict");
        String onHold = (String)productListForm.get("onHold");
        String pquadproductId = (String)productListForm.get("pquadProductID");

        String submitType = (String)productListForm.get("submitType");
        if (submitType == null) {
            submitType = ProductMaintConstants.PDB_LIST;
        }

        // make sure we close Status Window if any of these buttons were clicked
        if (submitType.equals(ProductMaintConstants.PDB_BATCH_SUBMIT_SELECTED) || 
            submitType.equals(ProductMaintConstants.PDB_BATCH_SUBMIT_ALL) || 
            submitType.equals(ProductMaintConstants.PDB_BATCH_REMOVE)) {
            request.setAttribute(ProductMaintConstants.CLOSE_POPUP, "Y");
        }

        try {
            ProductVO productVO = null;
            //If the pquadid having one product we changed the submitype to edit for calling the existing functionality. .
            if (submitType.equals(ProductMaintConstants.PDB_PQUAD_PRODUCT_SUBMIT)) {
            	forwardStr = "filtersuccess";
            	try{
            	 List products = productSVC.getPQuadProductList(pquadproductId);
                     List products1 = new ArrayList();
                     List products2 = new ArrayList();
                     int size=products.size();
                     if (size > 1) {
                         int mid = size / 2;
                         products1 = products.subList(0, mid);
                         products2 = products.subList(mid, size);
                         request.setAttribute("pquadProductId", pquadproductId);
                         productListForm.set("searchBy", "searchByProdId");
                         request.setAttribute(mapping.getAttribute(), productListForm);
                         request.setAttribute(ProductMaintConstants.MENU_BACK_KEY,ProductMaintConstants.MENU_BACK_PRODUCT_MAINT_LIST_KEY);
                         request.setAttribute(ProductMaintConstants.MENU_BACK_TEXT_KEY,ProductMaintConstants.MENU_BACK_TEXT_PRODUCT_MAINT_LIST_KEY);
                         request.setAttribute("products1", products1);
                         request.setAttribute("products2", products2);
                     } else if (size == 1) {
                    	 //Apollo user will view product in Product Detail if only one product is found with entered PQuad Product ID.To display single product submitType changed to Edit.
                    	 submitType = ProductMaintConstants.PDB_EDIT;
                    	 productId=((ProductVO)products.get(0)).getProductId();
                    	 productListForm.set("productId", productId);
                     }
                     if(size<=0){
                   	    request.setAttribute("ErrorMsg",ProductMaintConstants.PQUAD_PRODUCT_LIST_ERRORMSG);
                   	    request.setAttribute("products1", null);
                     }
            	   }
            	catch(Exception e)
            	{
            		    logger.error("Error while retriving the Pquad Products "+ e);
            		    request.setAttribute("ErrorMsg",ProductMaintConstants.PQUAD_PRODUCT_LIST_ERRORMSG);
                	    request.setAttribute("products1", null);
            	}
                     //Handle List of Pquad Products
            }

            if (productIsInBatch(productId, getUserId(request))) {
                submitType = ProductMaintConstants.PDB_EDIT_BATCH;
            }
            //  Handle request to edit product
            if (submitType.equals(ProductMaintConstants.PDB_EDIT)) {
                forwardStr = "success";
                if (searchBy == null) {
                    searchBy = ProductMaintConstants.PDB_SEARCH_BY_PRODUCT_ID;
                }

                if (!productSVC.validateProductId(productId)) {
                    if (upsellSVC.validateMasterSku(productId)) {
                        String msg = productId + " is an upsell master sku.";
                        errors.add("ProductId", 
                                   new ActionMessage(String.valueOf(ProductMaintConstants.MASTER_SKU_ON_PRODUCT_PAGE_ERROR), 
                                                     msg));
                        errorFound = true;
                    } else {
                        request.setAttribute(ProductMaintConstants.PRODUCT_MAINT_INSTRUCTION_KEY, 
                                             super.getResources(request).getMessage(ProductMaintConstants.PRODUCT_MAINT_NEW_PRODUCT_KEY));
                    }
                }
            }

            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
            List userBatch = productBatchSVC.getUserBatch(getUserId(request));
            int batchSize = userBatch.size();
            String maxBatchSize = 
                configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, 
                                       ProductMaintConstants.PDB_MAX_USER_BATCH_SIZE);
            if (maxBatchSize != null) {
                int maxBatchSizeInt = Integer.parseInt(maxBatchSize);
                if (batchSize >= maxBatchSizeInt && 
                    !submitType.equals(ProductMaintConstants.PDB_EDIT_BATCH)) {
                    request.setAttribute(ProductMaintConstants.DISABLE_BATCH_MODE, 
                                         "true");
                }
            }

            //  Handle request to edit product
            if (submitType.equals(ProductMaintConstants.PDB_EDIT_BATCH)) {
                forwardStr = "success";
                if (searchBy == null) {
                    searchBy = ProductMaintConstants.PDB_SEARCH_BY_PRODUCT_ID;
                }

                ProductBatchVO productBatchVO = 
                    productBatchSVC.getProduct(getUserId(request), productId);
                productVO = new ProductVO();
                productVO.setAllColorsList((List)session.getAttribute(ProductMaintConstants.COLORS_KEY ));
                productVO.copy(productBatchVO.getXmlDocument());
               
                //ed, added 4/11/03 ---make sure product does not exist as Master SKU
                UpsellMasterVO vo = upsellSVC.getMasterSKU(productId);
                if (vo != null) {
                    String msg = 
                        vo.getMasterID() + " is attached to " + vo.getMasterName();
                    errors.add("ProductId", 
                               new ActionMessage(String.valueOf(ProductMaintConstants.MASTER_SKU_ON_PRODUCT_PAGE_ERROR), 
                                                 msg));
                    errorFound = true;
                }

                if (productVO == null) {
                    request.setAttribute(ProductMaintConstants.PRODUCT_MAINT_INSTRUCTION_KEY, 
                                         super.getResources(request).getMessage(ProductMaintConstants.PRODUCT_MAINT_NEW_PRODUCT_KEY));
                }

            }

            // Handle request to list products
            if (submitType.equals(ProductMaintConstants.PDB_LIST)) {
                forwardStr = "filtersuccess";
                List products = 
                    productSVC.getProductListAdvanced(prodAvail, prodUnavail, 
                                                      dateRestrict, onHold);
                List products1 = new ArrayList();
                List products2 = new ArrayList();
                if (products.size() > 1) {
                    int mid = products.size() / 2;
                    products1 = products.subList(0, mid);
                    products2 = products.subList(mid, products.size() - 1);
                } else if (products.size() == 1) {
                    products1 = products;
                }
                request.setAttribute("products1", products1);
                request.setAttribute("products2", products2);
                productListForm.set("productId", "");
                productListForm.set("searchBy", "searchByProdId");
                request.setAttribute(mapping.getAttribute(), productListForm);
                // Build the back link
                request.setAttribute(ProductMaintConstants.MENU_BACK_KEY, 
                                     ProductMaintConstants.MENU_BACK_PRODUCT_MAINT_LIST_KEY);
                request.setAttribute(ProductMaintConstants.MENU_BACK_TEXT_KEY, 
                                     ProductMaintConstants.MENU_BACK_TEXT_PRODUCT_MAINT_LIST_KEY);

                // Handle 'Remove Selected' Button
            }
            else if (submitType.equals(ProductMaintConstants.PDB_BATCH_REMOVE)) {
                forwardStr = "filtersuccess";
                ArrayList selectedProducts = getSelectedProductList(request);
                for (int i = 0; i < selectedProducts.size(); i++) {
                    String productIdBatch = (String)selectedProducts.get(i);
                    productBatchSVC.deleteProduct(getUserId(request), productIdBatch);
                }

                // Handle 'Submit Selected' Button
            } else if (submitType.equals(ProductMaintConstants.PDB_BATCH_SUBMIT_SELECTED)) {
                forwardStr = "filtersuccess";
                ArrayList selectedProducts = getSelectedProductList(request);
                for (int i = 0; i < selectedProducts.size(); i++) {
                    String productIdBatch = (String)selectedProducts.get(i);
                    sendProductToNovator(getUserId(request), productIdBatch);
                }

                // Handle 'Submit All' Button
            } else if (submitType.equals(ProductMaintConstants.PDB_BATCH_SUBMIT_ALL)) {
                forwardStr = "filtersuccess";
                List productBatch = productBatchSVC.getUserBatch(getUserId(request));
                for (int i = 0; i < productBatch.size(); i++) {
                    ProductBatchVO productBatchVO = 
                        (ProductBatchVO)productBatch.get(i);
                    String productIdBatch = productBatchVO.getProductId();
                    sendProductToNovator(getUserId(request), productIdBatch);
                }
            }

        } catch (Throwable e) {
            Throwable exception = null;
            logger.error(e);
            try {
                Class class1 = e.getClass();
                Class class2 = 
                    Class.forName(ProductMaintConstants.PDBSYSTEMEXCEPTION_CLASS_NAME);
                Class class3 = 
                    Class.forName(ProductMaintConstants.PDBAPPLICATIONEXCEPTION_CLASS_NAME);
                if (!class1.equals(class2) && !class1.equals(class3)) {
                    String[] args = new String[1];
                    args[0] = new String(e.getMessage());
                    logger.error(e);
                    PDBException ftde = 
                        new PDBApplicationException(ProductMaintConstants.GENERAL_APPLICATION_EXCEPTION, 
                                                    args, e);
                    exception = ftde;
                } else {
                    exception = e;
                }
            } catch (ClassNotFoundException cnfe) {
                logger.error(cnfe);
            }

            return super.handleException(request, mapping, exception);
        }
        request.setAttribute(ProductMaintConstants.OE_PRODUCT_ID_KEY, 
                             productId);
        request.setAttribute(ProductMaintConstants.PDB_PRODUCT_SEARCH_BY_KEY, 
                             searchBy);
        request.setAttribute(ProductMaintConstants.PDB_SUBMIT_TYPE, 
                             submitType);


        // Forward control to the specified success URI
        if (errorFound) {
            saveMessages(request, errors);
            return (mapping.findForward("error"));
        } else {
            if (submitType.equals(ProductMaintConstants.PDB_EDIT) || 
                submitType.equals(ProductMaintConstants.PDB_EDIT_BATCH)||submitType.equals(ProductMaintConstants.PDB_PQUAD_PRODUCT_SUBMIT)) {
                request.removeAttribute(mapping.getAttribute());
            }
            return (mapping.findForward(forwardStr));
        }
    }

    private ArrayList getSelectedProductList(HttpServletRequest request) {
        ArrayList selectedProducts = new ArrayList();
        String[] productIds = request.getParameterValues("batchcheckbox");
        if (productIds != null) {
            for (int i = 0; i < productIds.length; i++) {
                String productId = productIds[i];
                selectedProducts.add(productId);
            }
        }
        return selectedProducts;
    }

    private boolean productIsInBatch(String productId, 
                                     String userId) throws PDBException {
        boolean found = false;
        List productBatch = productBatchSVC.getUserBatch(userId);
        if (productBatch != null) {
            for (int i = 0; i < productBatch.size(); i++) {
                ProductBatchVO productBatchVO = 
                    (ProductBatchVO)productBatch.get(i);
                if (productBatchVO.getProductId().equals(productId)) {
                    found = true;
                    break;
                }
            }
        }
        return found;
    }

    private void sendProductToNovator(String userId, 
                                      String productId) throws Exception {
        // get the productVO
        ProductBatchVO productBatchVO = 
            productBatchSVC.getProduct(userId, productId);
        ProductVO productVO = new ProductVO();
        productVO.setAllColorsList((List)lookupSVC.getColorsList());
        boolean errorsFound = false;
        try {
            productVO.copy(productBatchVO.getXmlDocument());
            // Get the Novator servers to send to
            productSVC.setProduct(productVO, setNovatorEnvKeys(productVO));
        } catch (Exception e) {
            logger.info("sendProductToNovator received exception");
            logger.error(e);
            errorsFound = true;
            Exception exception = null;
            try {
                Class class1 = e.getClass();
                Class class2 = 
                    Class.forName(ProductMaintConstants.PDBSYSTEMEXCEPTION_CLASS_NAME);
                Class class3 = 
                    Class.forName(ProductMaintConstants.PDBAPPLICATIONEXCEPTION_CLASS_NAME);
                if (!class1.equals(class2) && !class1.equals(class3)) {
                    String[] args = new String[1];
                    args[0] = new String(e.getMessage());
                    logger.error(e);
                    PDBException ftde = 
                        new PDBApplicationException(ProductMaintConstants.GENERAL_APPLICATION_EXCEPTION, 
                                                    args, e);
                    exception = ftde;
                } else {
                    exception = e;
                }
            } catch (ClassNotFoundException cnfe) {
                logger.error(cnfe);
            }

            productBatchVO.setStatus(ProductMaintConstants.PDB_BATCH_STATUS_ERROR);
            Document doc = JAXPUtil.createDocument();
            Element errorElement = 
                doc.createElement(ProductMaintConstants.PDB_BATCH_STATUS_ERROR);
            errorElement = 
                    JAXPUtil.buildSimpleXmlNode(doc, ProductMaintConstants.PDB_BATCH_STATUS_ERROR, 
                                                    extractErrorMessage(exception.getMessage()));
            productBatchVO.setXmlErrors(doc);
            productBatchSVC.setProduct(productBatchVO);

        }

        if (!errorsFound) {
            productBatchSVC.deleteProduct(userId, productId);
        }

    }
    
    private List setNovatorEnvKeys(ProductVO productVO) throws PDBApplicationException {
        // Get the Novator servers to send to
        List envKeys = new ArrayList();

        try {
            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();

            if (productVO.isSentToNovatorProd()) {
                envKeys.add(com.ftd.osp.utilities.feed.NovatorFeedUtil.PRODUCTION);
            }
            if (productVO.isSentToNovatorTest()) {
                envKeys.add(com.ftd.osp.utilities.feed.NovatorFeedUtil.TEST);
            }
            if (productVO.isSentToNovatorUAT()) {
                envKeys.add(com.ftd.osp.utilities.feed.NovatorFeedUtil.UAT);
            }
            if (productVO.isSentToNovatorContent()) {
                envKeys.add(com.ftd.osp.utilities.feed.NovatorFeedUtil.CONTENT);
            }
        } catch (Exception e) {
            logger.error("Unable to obtain configuration object", e);
            throw new PDBApplicationException(ProductMaintConstants.CONFIG_PROPERTY_NOT_FOUND_EXCEPTION, 
                                              e);
        }
        return envKeys;
    }    

    private String extractErrorMessage(String errorString) {
        String message = errorString;
        int start = errorString.indexOf("|");
        if (start >= 0) {
            int end = errorString.indexOf("|", start + 1);
            if (end >= 0) {
                message = errorString.substring(start + 1, end);
            }
        }
        return message;
    }

    public void setLookupSVC(ILookupSVCBusiness lookupSVC) {
        this.lookupSVC = lookupSVC;
    }

    public ILookupSVCBusiness getLookupSVC() {
        return lookupSVC;
    }

    public void setProductBatchSVC(IProductBatchSVCBusiness productBatchSVC) {
        this.productBatchSVC = productBatchSVC;
    }

    public IProductBatchSVCBusiness getProductBatchSVC() {
        return productBatchSVC;
    }

    public void setProductSVC(IProductMaintSVCBusiness productSVC) {
        this.productSVC = productSVC;
    }

    public IProductMaintSVCBusiness getProductSVC() {
        return productSVC;
    }

    public void setUpsellSVC(IUpsellSVCBusiness upsellSVC) {
        this.upsellSVC = upsellSVC;
    }

    public IUpsellSVCBusiness getUpsellSVC() {
        return upsellSVC;
    }
}
