package com.ftd.pdb.web;

import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.exceptions.PDBException;
import com.ftd.pdb.service.IProductMaintSVCBusiness;

import java.io.IOException;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * Implementation of <strong>Action</strong> that processes a
 * Occasion Maintenance Flow Action.
 *
 * @author Ed Mueller
 */

public final class OccasionMaintenanceFlowAction extends PDBAction
{
    /*Spring managed resources */
    private IProductMaintSVCBusiness productSVC;
    /*end Spring managed resources */
/**
 *Constructor
*/
    public OccasionMaintenanceFlowAction()
    {
        super("com.ftd.pdb.web.OccasionMaintenanceFlowAction");
    }

	/**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param mapping The ActionMapping used to select this instance
     * @param form The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     */    
    public ActionForward execute(ActionMapping mapping,
				 ActionForm form,
				 HttpServletRequest request,
				 HttpServletResponse response)
	throws IOException, ServletException
    {
        try
        {          
            List occasionList = productSVC.getOccasions();
            request.setAttribute(ProductMaintConstants.OCCASION_LIST_KEY, occasionList);
            request.setAttribute(ProductMaintConstants.AVAILABLE_CARDS_KEY, new ArrayList());
            request.setAttribute(ProductMaintConstants.SELECTED_CARDS_KEY, new ArrayList());            
            
        }
        catch(PDBException pdbException)
        {
            return super.handleException(request,mapping,pdbException);
        }


        // Forward control to the specified success URI
        return (mapping.findForward("success"));
    }

    public void setProductSVC(IProductMaintSVCBusiness productSVC) {
        this.productSVC = productSVC;
    }

    public IProductMaintSVCBusiness getProductSVC() {
        return productSVC;
    }
}
