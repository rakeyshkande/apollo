package com.ftd.pdb.web;

import java.util.List;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.validator.DynaValidatorForm;

import com.ftd.pdb.service.ProductMaintSVCImpl;


public class ProductMaintListForm extends DynaValidatorForm {
    public ProductMaintListForm() {
        super();
    }
    
    public ActionErrors validate(ActionMapping actionMapping, 
                                 HttpServletRequest request){
        ActionErrors  errors = super.validate(actionMapping, request); //Avoid this statement usage
        //Please  avoid  validation plug-in validations for this Form .Try to validate validations at filed level type.If not One validation will impact the other process . 
        String submitType = request.getParameter("submitType");
        if(submitType.equalsIgnoreCase("edit"))
        {
            String productId = (String)get("productId");
            if (StringUtils.isBlank(productId))
            	 errors.add("productId",new ActionMessage("error.productId.required",true));
            else if(productId.trim().length()>10)
            	errors.add("productId",new ActionMessage("error.productId.maxlength",true));
            
        }else if(submitType.equalsIgnoreCase("submit")){
        	 String pquadProductId = (String)get("pquadProductID");
        	 Pattern pattern = Pattern.compile(".*[^0-9].*");
        	 if(StringUtils.isBlank(pquadProductId))
        		 errors.add("pquadProductID",new ActionMessage("error.pquadproductId.required",true));
        	 else if(pattern.matcher(pquadProductId).matches())
        	     errors.add("pquadProductID",new ActionMessage("error.pquadproductId.numeric",true));
        }
        else {
            errors = new ActionErrors();
        }
        return errors;
    }
}
