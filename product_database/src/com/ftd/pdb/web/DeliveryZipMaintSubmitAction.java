package com.ftd.pdb.web;

import com.ftd.osp.utilities.email.EmailVO;
import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.pdb.common.DeliveryZipMaintConstants;
import com.ftd.pdb.common.PDBXMLTags;
import com.ftd.pdb.common.resources.IResourceProvider;
import com.ftd.pdb.common.utilities.FTDUtil;
import com.ftd.pdb.service.IDeliveryZipSVC;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;


public class DeliveryZipMaintSubmitAction extends PDBAction {

    IDeliveryZipSVC deliverySVC;
    IResourceProvider resourceProvider;
        
    public DeliveryZipMaintSubmitAction() {
        super("com.ftd.pdb.web.DeliveryZipMaintSubmitAction");
    }
    
    public ActionForward execute(ActionMapping mapping, ActionForm form, 
                                 HttpServletRequest request, 
                                 HttpServletResponse response) throws IOException, 
                                                                      ServletException {
        
        try {
            String requestType = request.getParameter(DeliveryZipMaintConstants.REQUEST_TYPE);
               
            if( StringUtils.equals(requestType,DeliveryZipMaintConstants.REQUEST_TYPE_GET_CARRIERS)) {
                response.setContentType("text/xml");
                response.getWriter().write(getCarriers());
            } else if( StringUtils.equals(requestType,DeliveryZipMaintConstants.REQUEST_TYPE_GET_BLOCKED_ZIPS)) {
                response.setContentType("text/xml");
                response.getWriter().write(getBlockedZips(request));
            } else if( StringUtils.equals(requestType,DeliveryZipMaintConstants.REQUEST_TYPE_REMOVE_BLOCKED_ZIPS)) {
                response.setContentType("text/html");
                response.getWriter().write(removeBlockedZips(request));
            } else if( StringUtils.equals(requestType,DeliveryZipMaintConstants.REQUEST_TYPE_ADD_BLOCKED_ZIPS)) {
                response.setContentType("text/html");
                response.getWriter().write(addBlockedZips(request));
            }  else {
                throw new Exception("Unknown AJAX request type of "+requestType);
            }
        } catch (Throwable t) {
            logger.error(t);
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            t.printStackTrace(pw);
            request.getSession().setAttribute("errorStr",sw.toString());
            response.setContentType("text/html");
            response.getWriter().write("REDIRECT="+mapping.findForward("error").getPath()+".do");
        }
        
        return null;
    }
    
    private String addBlockedZips(HttpServletRequest request) throws Exception {
        String retval = null;
        Connection conn = null;
        
        try {
            conn = resourceProvider.getDatabaseConnection();
            String carrierId = StringUtils.trimToEmpty(request.getParameter(DeliveryZipMaintConstants.PARAMETER_CARRIER_ID));
            String strBlockedZips = StringUtils.trimToEmpty(request.getParameter(DeliveryZipMaintConstants.PARAMETER_BLOCKED_ZIPS));
            String emailAddress = StringUtils.trimToEmpty(request.getParameter(DeliveryZipMaintConstants.PARAMETER_EMAIL_ADDRESS));
            boolean bSendEmail = StringUtils.isNotBlank(emailAddress);
            
            String[] blockedZips = strBlockedZips.split("\\n");
            int size = blockedZips.length;
            
            if( size==0 ) {
                return ("Nothing to do.");
            }
            
            StringBuilder sb = new StringBuilder();
            StringBuilder successBuilder = new StringBuilder();
            successBuilder.append("The following zips were successuflly added:");
            StringBuilder errorBuilder = new StringBuilder();
            errorBuilder.append("The following zips failed to be added:");
            
            int successCount = 0;
            int errorCount = 0;
            
            conn = resourceProvider.getDatabaseConnection();
            
            for( int idx = 0; idx < size; idx++) {
                String zipCode = StringUtils.trimToEmpty(blockedZips[idx]);
                if( StringUtils.isBlank(zipCode) ) {
                    continue;
                }
                
                try {
                    deliverySVC.insertBlockedZip(conn,carrierId,zipCode,getUserId(request));
                    successCount++;
                    if( bSendEmail ) {
                        successBuilder.append("\r\n");
                        successBuilder.append(carrierId);
                        successBuilder.append(" - ");
                        successBuilder.append(zipCode);
                    }
                } catch (Exception e) {
                    errorCount++;
                    logger.error("Error while adding "+carrierId+"/"+zipCode,e);
                    if( bSendEmail ) {
                        errorBuilder.append("\r\n");
                        errorBuilder.append(carrierId);
                        errorBuilder.append(" - ");
                        errorBuilder.append(zipCode);
                        errorBuilder.append(": ");
                        errorBuilder.append(e.getMessage());
                    }
                }
            }
            
            sb.append("Successful: ");
            sb.append(successCount);            
            sb.append("\r\nErrors: ");
            sb.append(errorCount);
            sb.append("\r\nTotal records processed: ");
            sb.append(successCount+errorCount);
            retval = sb.toString();
            
            if( bSendEmail ) {
                sb.append("\r\n");
                sb.append(successBuilder);
                if( errorCount > 0) {
                    sb.append("\r\n");
                    sb.append(errorBuilder);
                }
                
                try {
                     EmailVO emailVO = new EmailVO();
                     emailVO.setRecipient(emailAddress);
                     emailVO.setSender(FTDUtil.getHostName()+"@ftdi.com");
                     emailVO.setSubject("Delivery Zip Blocking Results");
                     emailVO.setContent(sb.toString());
                     FTDUtil.sendPlainTextEmail(resourceProvider.getEmailSession(),emailVO);  
                 } catch (Exception e) {
                     logger.error("Failed to send email to "+emailAddress+".  Will continue...",e);
                 }
            }
            
        } finally {
            if( conn!=null ) {
                try {
                    conn.close();
                } catch (Exception e) {
                    logger.error("Error while closing database connection");
                }
            }
        }
        
        return retval;
    }
    
    private String removeBlockedZips(HttpServletRequest request) throws Exception {
        String retval = null;
        Connection conn = null;
        
        try {
            conn = resourceProvider.getDatabaseConnection();
            String strXml = request.getParameter(DeliveryZipMaintConstants.PARAMETER_BLOCKED_ZIPS);
            String emailAddress = StringUtils.trimToEmpty(request.getParameter(DeliveryZipMaintConstants.PARAMETER_EMAIL_ADDRESS));
            boolean bSendEmail = (emailAddress.length()>0)?true:false;
            
            Document doc = JAXPUtil.parseDocument(strXml);
            NodeList list = JAXPUtil.selectNodes(doc.getDocumentElement(),PDBXMLTags.TAG_BLOCKED_ZIP);
            
            int size = list.getLength();
            
            if( size==0 ) {
                return ("Nothing to do.");
            }
            
            StringBuilder sb = new StringBuilder();
            StringBuilder successBuilder = new StringBuilder();
            successBuilder.append("The following zips were successuflly removed:");
            StringBuilder errorBuilder = new StringBuilder();
            errorBuilder.append("The following zips failed to be removed:");
            int successCount = 0;
            int errorCount = 0;
            
            for( int idx=0; idx<size; idx++ ) {
                Element blockedZip = (Element)list.item(idx);
                String carrierId = blockedZip.getAttribute(PDBXMLTags.TAG_CARRIER_ID);
                String zipCode = blockedZip.getAttribute(PDBXMLTags.TAG_ZIP_CODE);
                
                try {
                    deliverySVC.deleteBlockedZip(conn,carrierId,zipCode);
                    successCount++;
                    if( bSendEmail ) {
                        successBuilder.append("\r\n");
                        successBuilder.append(carrierId);
                        successBuilder.append(" - ");
                        successBuilder.append(zipCode);
                    }
                } catch (Exception e) {
                    errorCount++;
                    logger.error("Error while processing "+carrierId+"/"+zipCode,e);
                    if( bSendEmail ) {
                        errorBuilder.append("\r\n");
                        errorBuilder.append(carrierId);
                        errorBuilder.append(" - ");
                        errorBuilder.append(zipCode);
                        errorBuilder.append(": ");
                        errorBuilder.append(e.getMessage());
                    }
                }
            }
            
            sb.append("Successful: ");
            sb.append(successCount);            
            sb.append("\r\nErrors: ");
            sb.append(errorCount);
            sb.append("\r\nTotal records processed: ");
            sb.append(successCount+errorCount);
            retval = sb.toString();
            
            if( bSendEmail ) {
                sb.append("\r\n");
                sb.append(successBuilder);
                if( errorCount > 0) {
                    sb.append("\r\n");
                    sb.append(errorBuilder);
                }
                
                try {
                     EmailVO emailVO = new EmailVO();
                     emailVO.setRecipient(emailAddress);
                     emailVO.setSender(FTDUtil.getHostName()+"@ftdi.com");
                     emailVO.setSubject("Delivery Zip Blocking Results");
                     emailVO.setContent(sb.toString());
                     FTDUtil.sendPlainTextEmail(resourceProvider.getEmailSession(),emailVO);  
                 } catch (Exception e) {
                     logger.error("Failed to send email to "+emailAddress+".  Will continue...",e);
                 }
            }
            
        } finally {
            if( conn!=null ) {
                try {
                    conn.close();
                } catch (Exception e) {
                    logger.error("Error while closing database connection");
                }
            }
        }
        
        return retval;
    }
    
    private String getBlockedZips(HttpServletRequest request) throws Exception {
        String zipCode = request.getParameter(DeliveryZipMaintConstants.PARAMETER_ZIP_CODE);
        return JAXPUtil.toString(deliverySVC.getBlockedZips(zipCode));
    }
    
    private String getCarriers() throws Exception {
        return JAXPUtil.toString(deliverySVC.getCarriersXML());
    }

    public void setDeliverySVC(IDeliveryZipSVC deliverySVC) {
        this.deliverySVC = deliverySVC;
    }

    public IDeliveryZipSVC getDeliverySVC() {
        return deliverySVC;
    }

    public void setResourceProvider(IResourceProvider resourceProvider) {
        this.resourceProvider = resourceProvider;
    }

    public IResourceProvider getResourceProvider() {
        return resourceProvider;
    }
}
