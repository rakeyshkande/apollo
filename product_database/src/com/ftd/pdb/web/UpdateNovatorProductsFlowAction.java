package com.ftd.pdb.web;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.feed.NovatorFeedUtil;
import com.ftd.pdb.common.ProductMaintConstants;

import java.io.IOException;

import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


public class UpdateNovatorProductsFlowAction extends PDBAction {
    private static final SimpleDateFormat format = 
        new SimpleDateFormat("MM/dd/yyyy");

    public UpdateNovatorProductsFlowAction() {
        super("com.ftd.pdb.web.UpdateNovatorProductsFlowAction");
    }

    public ActionForward execute(ActionMapping mapping, ActionForm form, 
                                 HttpServletRequest request, 
                                 HttpServletResponse response) throws IOException, 
                                                                      ServletException {

        UpdateNovatorProductsForm updateForm = (UpdateNovatorProductsForm)form;
        updateForm.set("updateMethod", "updateByList");
        updateForm.set("updateDate", format.format(new java.util.Date()));

        //Set Novator environments
        try {
          String checked = "checked";
          String disabled = "disabled";
          NovatorFeedUtil nfu = new NovatorFeedUtil();
          if(!nfu.isNovatorEnvironmentAllowed(NovatorFeedUtil.TEST))
            updateForm.set("testDisabled", disabled);
          if(nfu.isNovatorEnvironmentChecked(NovatorFeedUtil.TEST))
            updateForm.set("testChecked", checked);
          
          if(!nfu.isNovatorEnvironmentAllowed(NovatorFeedUtil.UAT))
            updateForm.set("uatDisabled", disabled);
          if(nfu.isNovatorEnvironmentChecked(NovatorFeedUtil.UAT))
            updateForm.set("uatChecked", checked);

          if(!nfu.isNovatorEnvironmentAllowed(NovatorFeedUtil.CONTENT))
            updateForm.set("contentDisabled", disabled);
          if(nfu.isNovatorEnvironmentChecked(NovatorFeedUtil.CONTENT))
            updateForm.set("contentChecked", checked);

          if(!nfu.isNovatorEnvironmentAllowed(NovatorFeedUtil.PRODUCTION))
            updateForm.set("liveDisabled", disabled);
          if(nfu.isNovatorEnvironmentChecked(NovatorFeedUtil.PRODUCTION))
            updateForm.set("liveChecked", checked);
        } catch (Exception e) {
            logger.error(e);
            throw new ServletException("Failed to retrieve Novator property", 
                                       e);
        }

        // Forward control to the specified success URI
        return (mapping.findForward("success"));
    }
}
