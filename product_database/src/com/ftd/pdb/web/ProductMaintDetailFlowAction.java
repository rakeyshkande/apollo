package com.ftd.pdb.web;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.valobjs.ColorVO;
import com.ftd.pdb.common.valobjs.DeliveryOptionVO;
import com.ftd.pdb.common.valobjs.ProductAddonVO;
import com.ftd.pdb.common.valobjs.ProductBatchVO;
import com.ftd.pdb.common.valobjs.ProductSecondChoiceVO;
import com.ftd.pdb.common.valobjs.ProductSubCodeVO;
import com.ftd.pdb.common.valobjs.ProductSubTypeVO;
import com.ftd.pdb.common.valobjs.ProductVO;
import com.ftd.pdb.common.valobjs.StateDeliveryExclusionVO;
import com.ftd.pdb.common.valobjs.ValueVO;
import com.ftd.pdb.common.valobjs.VendorVO;
import com.ftd.pdb.common.valobjs.comparator.ColorVOComparator;
import com.ftd.pdb.service.ILookupSVCBusiness;
import com.ftd.pdb.service.IProductBatchSVCBusiness;
import com.ftd.pdb.service.IProductMaintSVCBusiness;
import com.ftd.pdb.service.IUpsellSVCBusiness;

import java.io.IOException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class ProductMaintDetailFlowAction extends PDBAction {
    /*Spring managed resources */
    ILookupSVCBusiness lookupSVC;
    IProductBatchSVCBusiness productBatchSVC;
    IProductMaintSVCBusiness productSVC;
    IUpsellSVCBusiness upsellSVC;
    /*end Spring managed resources */

    private String strPrefix = 
        null; //key prefix attached to novator fields i.e. LIVE, TEST, CONTEXT, UAT
    private boolean bAttemptedLookup = 
        false; //has an attempt been made to find the key prefix 

    public ProductMaintDetailFlowAction() {
        super("com.ftd.pdb.web.ProductMaintDetailFlowAction");
    }

    public ActionForward execute(ActionMapping mapping, ActionForm form, 
                                 HttpServletRequest request, 
                                 HttpServletResponse response) throws IOException, 
                                                                      ServletException {
        ProductMaintDetailForm productDetailForm = (ProductMaintDetailForm)form;
                
        String productId = 
            (String)request.getAttribute(ProductMaintConstants.OE_PRODUCT_ID_KEY);
        ProductVO productVO2 = new ProductVO();
        if (productId == null) {
            try {
                BeanUtils.copyProperties(productVO2, productDetailForm);
                productId = productVO2.getProductId();
            } 
            catch (Exception ex) 
            {
                logger.debug(ex);
            }
        }
        //list of delivery options
        ArrayList deliveryOptionList = new ArrayList();

        // Get all values needed to prepopulate the form
        HttpSession session = request.getSession();
        try {
            HashMap dataMap = lookupSVC.getProductMaintSetupData();
            session.setAttribute(ProductMaintConstants.CATEGORIES_KEY, (List)dataMap.get(ProductMaintConstants.CATEGORIES_KEY));
            session.setAttribute(ProductMaintConstants.SERVICES_DURATION_KEY, (List) dataMap.get(ProductMaintConstants.SERVICES_DURATION_KEY));             
            session.setAttribute(ProductMaintConstants.EXCEPTION_CODES_KEY, (List)dataMap.get(ProductMaintConstants.EXCEPTION_CODES_KEY));
            session.setAttribute(ProductMaintConstants.PRICE_POINTS_KEY, (List)dataMap.get(ProductMaintConstants.PRICE_POINTS_KEY));
            session.setAttribute(ProductMaintConstants.SHIPPING_KEYS_KEY, (List)dataMap.get(ProductMaintConstants.SHIPPING_KEYS_KEY));
            session.setAttribute(ProductMaintConstants.RECIPIENT_SEARCH_KEY, (List)dataMap.get(ProductMaintConstants.RECIPIENT_SEARCH_KEY));
            session.setAttribute(ProductMaintConstants.PERSONALIZATION_TEMPLATE_KEY, (List)dataMap.get(ProductMaintConstants.PERSONALIZATION_TEMPLATE_KEY));
            session.setAttribute(ProductMaintConstants.SEARCH_PRIORITY_KEY, (List)dataMap.get(ProductMaintConstants.SEARCH_PRIORITY_KEY));
            session.setAttribute(ProductMaintConstants.COLORS_KEY, (List)dataMap.get(ProductMaintConstants.COLORS_KEY));
//            session.setAttribute(ProductMaintConstants.SHIPPING_METHODS_KEY, (List)dataMap.get(ProductMaintConstants.SHIPPING_METHODS_KEY));
            session.setAttribute(ProductMaintConstants.PDB_JCPENNEY_CATEGORY_KEY, (List)dataMap.get(ProductMaintConstants.PDB_JCPENNEY_CATEGORY_KEY));
            session.setAttribute(ProductMaintConstants.PDB_CARRIER_KEY, (List)dataMap.get(ProductMaintConstants.PDB_CARRIER_KEY));
            List countries = (List)dataMap.get(ProductMaintConstants.COUNTRIES_KEY);
            session.setAttribute(ProductMaintConstants.COUNTRIES_KEY, countries);
            session.setAttribute(ProductMaintConstants.PDB_VENDOR_CARRIER_LIST_KEY, (List)dataMap.get(ProductMaintConstants.PDB_VENDOR_CARRIER_LIST_KEY));
            session.setAttribute(ProductMaintConstants.PDB_COMPANY_MASTER_KEY, (List)dataMap.get(ProductMaintConstants.PDB_COMPANY_MASTER_KEY));
            List types = (List)dataMap.get(ProductMaintConstants.PRODUCT_TYPES_KEY);
            session.setAttribute(ProductMaintConstants.PRODUCT_TYPES_KEY, types);
            List subTypes = (List)dataMap.get(ProductMaintConstants.PRODUCT_SUB_TYPES_KEY);
            session.setAttribute(ProductMaintConstants.PRODUCT_SUB_TYPES_KEY, subTypes);
            session.setAttribute(ProductMaintConstants.PDB_SHIPPING_SYSTEMS_KEY, (List)dataMap.get(ProductMaintConstants.PDB_SHIPPING_SYSTEMS_KEY));
            session.setAttribute(ProductMaintConstants.BOX_KEY, (List)dataMap.get(ProductMaintConstants.BOX_KEY));
            session.setAttribute(ProductMaintConstants.COMPONENTS_KEY, dataMap.get(ProductMaintConstants.COMPONENTS_KEY));
            
            productDetailForm.set(ProductMaintConstants.CATEGORIES_KEY, 
                                  (List)dataMap.get(ProductMaintConstants.CATEGORIES_KEY));
            productDetailForm.set(ProductMaintConstants.SERVICES_DURATION_KEY,
                                  (List)dataMap.get(ProductMaintConstants.SERVICES_DURATION_KEY));                   
            productDetailForm.set(ProductMaintConstants.EXCEPTION_CODES_KEY, 
                                  (List)dataMap.get(ProductMaintConstants.EXCEPTION_CODES_KEY));
            productDetailForm.set(ProductMaintConstants.PRICE_POINTS_KEY, 
                                  (List)dataMap.get(ProductMaintConstants.PRICE_POINTS_KEY));
            productDetailForm.set(ProductMaintConstants.SHIPPING_KEYS_KEY, 
                                  (List)dataMap.get(ProductMaintConstants.SHIPPING_KEYS_KEY));
            productDetailForm.set(ProductMaintConstants.RECIPIENT_SEARCH_KEY, 
                                  (List)dataMap.get(ProductMaintConstants.RECIPIENT_SEARCH_KEY));
                                  
            productDetailForm.set(ProductMaintConstants.PERSONALIZATION_TEMPLATE_KEY,
                                  (List)dataMap.get(ProductMaintConstants.PERSONALIZATION_TEMPLATE_KEY));
                                  
            productDetailForm.set(ProductMaintConstants.SEARCH_PRIORITY_KEY, 
                                  (List)dataMap.get(ProductMaintConstants.SEARCH_PRIORITY_KEY));
//            productDetailForm.set(ProductMaintConstants.SHIPPING_METHODS_KEY, 
//                                  (List)dataMap.get(ProductMaintConstants.SHIPPING_METHODS_KEY));
            productDetailForm.set(ProductMaintConstants.PDB_JCPENNEY_CATEGORY_KEY, 
                                  (List)dataMap.get(ProductMaintConstants.PDB_JCPENNEY_CATEGORY_KEY));
            productDetailForm.set(ProductMaintConstants.PDB_CARRIER_KEY, 
                                  (List)dataMap.get(ProductMaintConstants.PDB_CARRIER_KEY));
            productDetailForm.set(ProductMaintConstants.COUNTRIES_KEY, 
                                  countries);
            productDetailForm.set(ProductMaintConstants.PDB_VENDOR_CARRIER_LIST_KEY, 
                                  (List)dataMap.get(ProductMaintConstants.PDB_VENDOR_CARRIER_LIST_KEY));
            productDetailForm.set(ProductMaintConstants.PDB_COMPANY_MASTER_KEY, 
                                  (List)dataMap.get(ProductMaintConstants.PDB_COMPANY_MASTER_KEY));
            productDetailForm.set(ProductMaintConstants.PRODUCT_TYPES_KEY, 
                                  types);
            productDetailForm.set(ProductMaintConstants.PRODUCT_SUB_TYPES_KEY, 
                                  subTypes);
            productDetailForm.set(ProductMaintConstants.PDB_SHIPPING_SYSTEMS_KEY, 
                                  (List)dataMap.get(ProductMaintConstants.PDB_SHIPPING_SYSTEMS_KEY));
            productDetailForm.set(ProductMaintConstants.BOX_KEY, 
                                  (List)dataMap.get(ProductMaintConstants.BOX_KEY));
//            productDetailForm.set(ProductMaintConstants.COMPONENTS_KEY, 
//                                  (List)dataMap.get(ProductMaintConstants.COMPONENTS_KEY));

            //get list of shipping methods and populate delivery options array
            List methodList = 
                (List)dataMap.get(ProductMaintConstants.SHIPPING_METHODS_KEY);
            for (int i = 0; i < methodList.size(); i++) {
                DeliveryOptionVO deliveryOptionVO = new DeliveryOptionVO();
                ValueVO valueVO = (ValueVO)methodList.get(i);
                deliveryOptionVO.setShippingMethodId(valueVO.getId());
                deliveryOptionVO.setShippingMethodDesc(valueVO.getDescription());
                deliveryOptionList.add(deliveryOptionVO);
            }

            // put the id and name in the text descrption
            List choiceList = new ArrayList();
            StringBuffer sb = new StringBuffer();
            List secondChoiceList = 
                (List)dataMap.get(ProductMaintConstants.SECOND_CHOICE_KEY);
            for (int i = 0; i < secondChoiceList.size(); i++) {
                sb.setLength(0);
                ProductSecondChoiceVO vo = new ProductSecondChoiceVO();
                vo.setProductSecondChoiceID(((ProductSecondChoiceVO)secondChoiceList.get(i)).getProductSecondChoiceID());
                vo.setDescription(((ProductSecondChoiceVO)secondChoiceList.get(i)).getDescription());
                sb.append(vo.getProductSecondChoiceID());
                sb.append(" - ");
                sb.append(vo.getDescription());
                vo.setDescription(sb.toString());
                vo.setMercuryDescription(((ProductSecondChoiceVO)secondChoiceList.get(i)).getMercuryDescription());
                choiceList.add(vo);
            }
            session.setAttribute(ProductMaintConstants.SECOND_CHOICE_KEY, choiceList);
            productDetailForm.set(ProductMaintConstants.SECOND_CHOICE_KEY, 
                                  choiceList);

            // put the id and name in the text descrption
            List vendorList = new ArrayList();
            sb = new StringBuffer();
            List vendorListData = 
                (List)dataMap.get(ProductMaintConstants.VENDOR_LIST_KEY);
            for (int i = 0; i < vendorListData.size(); i++) {
                sb.setLength(0);
                VendorVO vo = new VendorVO();
                vo.setVendorId(((VendorVO)vendorListData.get(i)).getVendorId());
                vo.setVendorName(((VendorVO)vendorListData.get(i)).getVendorName());
                vo.setVendorType(((VendorVO)vendorListData.get(i)).getVendorType());
                //sb.append(vo.getVendorId());
                //sb.append(" - ");
                //sb.append(vo.getVendorName());
                //vo.setVendorName(sb.toString());
                vendorList.add(vo);
            }
            session.setAttribute(ProductMaintConstants.VENDOR_LIST_KEY, vendorList);
            productDetailForm.set(ProductMaintConstants.VENDOR_LIST_KEY, 
                                  vendorList);

            // Get the ProductVO
            String searchBy = 
                (String)request.getAttribute(ProductMaintConstants.PDB_PRODUCT_SEARCH_BY_KEY);
            String submitType = 
                (String)request.getAttribute(ProductMaintConstants.PDB_SUBMIT_TYPE);
            ProductVO productVO = null;
            if (submitType != null && 
                submitType.equals(ProductMaintConstants.PDB_EDIT_BATCH)) {
                ProductBatchVO productBatchVO = null;
                if (searchBy.equals(ProductMaintConstants.PDB_SEARCH_BY_PRODUCT_ID)) {
                    productBatchVO = 
                            productBatchSVC.getProduct(getUserId(request), productId);
                } else {
                    productBatchVO = 
                            productBatchSVC.getProductByNovatorId(getUserId(request), 
                                                                  productId);
                }
                productVO = new ProductVO();
                productVO.setAllColorsList((List)session.getAttribute(ProductMaintConstants.COLORS_KEY ));
                productVO.setAllComponentSkuList((List)session.getAttribute(ProductMaintConstants.COMPONENTS_KEY ));
                productVO.copy(productBatchVO.getXmlDocument());
                if (productBatchVO.getStatus().equals(ProductMaintConstants.PDB_BATCH_STATUS_ERROR)) {
                    String errorString = "";
                    Document doc = productBatchVO.getXmlErrors();
                    if (doc != null) {
                        Element root = (Element)doc.getFirstChild();
                        errorString = 
                                StringUtils.trimToEmpty(root.getNodeValue());
                        //request.setAttribute("batchError", errorString);
                        productDetailForm.set(ProductMaintConstants.BATCH_ERROR, 
                                              errorString);
                    }
                }
            } else {
                if (searchBy != null && searchBy.equals(ProductMaintConstants.PDB_SEARCH_BY_PRODUCT_ID)) {
                    productVO = productSVC.getProduct(productId);
                } else {
                    productVO = productSVC.getProductByNovatorId(productId);
                }
            }

            //Check if Product Duration from product is not in duration list.
            //If it's not, add it to the list.            
            List durationList = (List) dataMap.get(ProductMaintConstants.SERVICES_DURATION_KEY);
            boolean durationFound = false;
            String productVODuration = productVO.getDuration(); 

            if(productVODuration != null){
              for (int i = 0; i < durationList.size(); i++){
                  ValueVO durationListVO = (ValueVO) durationList.get(i);
                  String durationListVODuration = durationListVO.getId();
                  
                  if(productVODuration.equals(durationListVODuration)){
                    durationFound = true;
                    break;
                  }
              }
              if(!durationFound){
                ValueVO vo = new ValueVO();
                vo.setDescription(productVODuration);
                vo.setId(productVODuration);
                durationList.add(vo);
                session.setAttribute(ProductMaintConstants.SERVICES_DURATION_KEY, durationList);
                productDetailForm.set(ProductMaintConstants.SERVICES_DURATION_KEY, durationList);
              }
            }

            List excludedStates = null;
            List colors = lookupSVC.getColorsList();
            List includedColors = null;
            List excludedColors = null;
            List vendorProductsList = null;
            List components = lookupSVC.getComponentSkuList();
            List availableComponents = null;
            List includedComponents = null;

            Object[] subCodeArray = null;
            List subCodeList = null;
            // Get the type and sub types
            List currentSubTypes = new ArrayList();

            ActionMessages errors = getMessages(request); // struts 1.1 problem
            //6/9/03 - Ed Mueller - set check values in delivery option array
            if (productVO != null && productVO.getProductId() != null) {
                // set the selectd shipping methods
                List shipMethodsList = productVO.getShipMethods();
                for (int i = 0; i < deliveryOptionList.size(); i++) {
                    DeliveryOptionVO deliveryVO = 
                        (DeliveryOptionVO)deliveryOptionList.get(i);
                    updateDeliveryData(shipMethodsList, deliveryVO);
                }
            }
            //6/9/03 - end

           // BEGIN Set personalization template order information
           // If a template doesn't exist set to default value
           List<ValueVO> personalizationTemplateOrder = null;
           String personalizationTemplate = null;
           if(productVO == null || productVO.getPersonalizationTemplate() == null || productVO.getPersonalizationTemplate().equals(""))
           {
             personalizationTemplate = ProductMaintConstants.PERSONALIZATION_DEFAULT_TEMPLATE;
             if(productVO != null)
                productVO.setPersonalizationTemplate(personalizationTemplate);
           }
           else
           {
             personalizationTemplate = productVO.getPersonalizationTemplate();
           }
           personalizationTemplateOrder = lookupSVC.getPersonalizationTemplateOrder(personalizationTemplate);
           session.setAttribute(ProductMaintConstants.PERSONALIZATION_TEMPLATE_ORDER_KEY, (List)personalizationTemplateOrder);
           productDetailForm.set(ProductMaintConstants.PERSONALIZATION_TEMPLATE_ORDER_KEY, (List)personalizationTemplateOrder);
           productDetailForm.set("ftdwestAccessoryId", productVO.getFtdwestAccessoryId());
           productDetailForm.set("ftdwestTemplateMapping", productVO.getFtdwestTemplateMapping());
           productDetailForm.set("personalizationCaseFlag", productVO.isPersonalizationCaseFlag());
           productDetailForm.set("allAlphaFlag", productVO.isAllAlphaFlag());
          // END Set personalization template order information

            if (productVO != null && productVO.getProductId() != null && (errors == null || errors.size() <= 0)) {
                BeanUtils.copyProperties(productDetailForm, productVO);
                
                productDetailForm.set("exceptionStartDate", 
                                      productVO.getExceptionStartDate());
                productDetailForm.set("exceptionEndDate", 
                                      productVO.getExceptionEndDate());

                // get the excluded delivery states and sub codes
                excludedStates = productVO.getExcludedDeliveryStates();
                includedColors = productVO.getColorsList();
                subCodeList = productVO.getSubCodeList();
                vendorProductsList = productVO.getVendorProductsList();
                includedComponents = productVO.getComponentSkuList();

                //set Novator Flags
                productDetailForm.set("sentToNovatorContent", 
                                      productVO.isSentToNovatorContent());
                productDetailForm.set("sentToNovatorUAT", 
                                      productVO.isSentToNovatorUAT());
                productDetailForm.set("sentToNovatorProd", 
                                      productVO.isSentToNovatorProd());
                productDetailForm.set("sentToNovatorTest", 
                                      productVO.isSentToNovatorTest());

                //Setup JCPenney Category
                String strTest = 
                    (String)productDetailForm.get("JCPenneyCategory");
                if (strTest == null || 
                    strTest.equals(ProductMaintConstants.BLANK_STRING)) {
                    productDetailForm.set("JCPenneyCategory", 
                                          ProductMaintConstants.DEFAULT_JCPENNEY_CATEGORY);
                }

                // Setup the correct sub types for this type
                String typeId = productVO.getProductType();
                if (typeId != null) {
                    for (int j = 0; j < subTypes.size(); j++) {
                        String subTypeId = 
                            ((ProductSubTypeVO)subTypes.get(j)).getTypeId();
                        if (typeId.equals(subTypeId)) {
                            currentSubTypes.add(subTypes.get(j));
                        }
                    }
                }
            } //if(productVO != null && errors == null)              
            else if (errors == null || errors.size() <= 0) {
                // This is a new Product, ensure the Product Id is trimmed
                productId = StringUtils.trim(productId);
                
                // Since we don't have a product init some fields
                productDetailForm.set("productId", productId);
                excludedStates = 
                        (List)dataMap.get(ProductMaintConstants.PDB_DELIV_STATE_EXCL_LIST_KEY);
                excludedColors = colors;
                includedColors = new ArrayList();
                vendorProductsList = new ArrayList();
                subCodeArray = new Object[0];
                currentSubTypes = new ArrayList();
                availableComponents = components;
                includedComponents = new ArrayList();
                productDetailForm.set("standardPriceRank", "1");
                productDetailForm.set("deluxePriceRank", "2");
                productDetailForm.set("premiumPriceRank", "3");
                productDetailForm.set("country", 
                                      ProductMaintConstants.DOMESTIC_COUNTRY_CODE);
                productDetailForm.set("deliveryType", 
                                      ProductMaintConstants.DOMESTIC_CODE);
                productDetailForm.set("JCPenneyCategory", 
                                      ProductMaintConstants.DEFAULT_JCPENNEY_CATEGORY);
                productDetailForm.set("sentToNovatorContent", true);
                productDetailForm.set("sentToNovatorUAT", true);
                productDetailForm.set("sentToNovatorProd", true);
                productDetailForm.set("sentToNovatorTest", true);
                productDetailForm.set("allowFreeShippingFlag", true);
            }

            if (errors != null && errors.size() > 0) {
                Iterator it = 
                    errors.get(ProductMaintConstants.APPLICATION_CATEGORY_CONSTANT);
                productDetailForm.set("lastUpdateDateString", 
                                      productVO.getLastUpdateDate());
                while (it.hasNext()) {
                    ActionMessage error = (ActionMessage)it.next();
                    if (error.getKey().equals(String.valueOf(ProductMaintConstants.PDB_NOVATOR_EXCEPTION))) {
                        Object errobjs[] = error.getValues();
                        if (errobjs != null && errobjs.length > 0) {
                            //Only worry about the first entry
                            String strError = 
                                ((String)errobjs[0]).toLowerCase();
                            if (strError == null) //Sanity check
                                continue;

                            //parse string in XML document
                            //                                DOMParser parser = new DOMParser();
                            boolean bValidDocument;
                            Document doc = null;
                            try {
                                doc = JAXPUtil.parseDocument(strError);
                                //                                    parser.parse(new StringReader(strError));
                                bValidDocument = true;
                            } catch (Exception e) {
                                //Not a valid xml document so assume just pass the
                                //error string along
                                bValidDocument = false;
                            }

                            //Assumption made here is that the error string was 
                            //processed properly by NovatorTransmission.class
                            if (bValidDocument) {
                                boolean bHasNovatorErrors = false;
                                NodeList errorFields = 
                                    JAXPUtil.selectNodes(doc, "//error");
                                int iErrorCount = errorFields.getLength();
                                Element errorField;
                                boolean[] optionErrorFlags = new boolean[100];
                                boolean[] priceErrorFlags = new boolean[100];
                                //String[] errorStrings = new String[ProductMaintConstants.PCB_PRODUCT_MAINT_ERROR_MAX_IDX];
                                List subCodes = productVO.getSubCodeList();

                                for (int idx = 0; idx < iErrorCount; idx++) {
                                    errorField = 
                                            (Element)errorFields.item(idx);
                                    if (errorField == null) //Sanity check 
                                        continue;

                                    String strNodeName = null;
                                    String strFieldValue = null;
                                    String strDescription = null;
                                    String strSubCode = null;

                                    NodeList childNodes = 
                                        errorField.getChildNodes();
                                    Node childNode;

                                    for (int chIdx = 0; 
                                         chIdx < childNodes.getLength(); 
                                         chIdx++) {
                                        childNode = childNodes.item(chIdx);
                                        if (childNode == null)
                                            continue;

                                        strNodeName = childNode.getNodeName();
                                        childNode = childNode.getFirstChild();

                                        if (childNode != null) {
                                            if (strNodeName.compareToIgnoreCase("field") == 
                                                0)
                                                strFieldValue = 
                                                        childNode.getNodeValue();
                                            else if (strNodeName.compareToIgnoreCase("description") == 
                                                     0)
                                                strDescription = 
                                                        childNode.getNodeValue();
                                            else if (strNodeName.compareToIgnoreCase("subcode") == 
                                                     0)
                                                strSubCode = 
                                                        childNode.getNodeValue();
                                        }
                                    }

                                    if (strFieldValue == null || 
                                        strFieldValue.length() == 
                                        0) //Sanity check
                                        continue;

                                    if (strFieldValue.compareToIgnoreCase(ProductMaintConstants.PDB_NOVATOR_FIELD_NAME_PRODUCT_ID) == 
                                        0) {
                                        bHasNovatorErrors = true;
                                        errors.add("novatorId", 
                                                   new ActionMessage(strDescription, 
                                                                     false));
                                        //productDetailForm.set("novatorIDError",true);
                                        //errorStrings[ProductMaintConstants.PDB_PRODUCT_MAINT_ERROR_IDX_NOVATOR_ID]=strDescription;
                                    }

                                    else if (strFieldValue.compareToIgnoreCase(ProductMaintConstants.PDB_NOVATOR_FIELD_NAME_TITLE) == 
                                             0) {
                                        bHasNovatorErrors = true;
                                        errors.add("novatorName", 
                                                   new ActionMessage(strDescription, 
                                                                     false));
                                        //productDetailForm.set("novatorNameError",true);
                                        //errorStrings[ProductMaintConstants.PDB_PRODUCT_MAINT_ERROR_IDX_NOVATOR_NAME]=strDescription;
                                    }

                                    else if (strFieldValue.compareToIgnoreCase(ProductMaintConstants.PDB_NOVATOR_FIELD_NAME_LONG_DESCRIPTION) == 
                                             0) {
                                        bHasNovatorErrors = true;
                                        errors.add("longDescription", 
                                                   new ActionMessage(strDescription, 
                                                                     false));
                                        //productDetailForm.set("arrangementLongDescriptionError",true);
                                        //errorStrings[ProductMaintConstants.PDB_PRODUCT_MAINT_ERROR_IDX_PRODUCT_LONG_DESCRIPTION]=strDescription;
                                    }

                                    else if (strFieldValue.compareToIgnoreCase(ProductMaintConstants.PDB_NOVATOR_FIELD_NAME_TYPE) == 
                                             0) {
                                        bHasNovatorErrors = true;
                                        errors.add("productType", 
                                                   new ActionMessage(strDescription, 
                                                                     false));
                                        //productDetailForm.set("typeError",true);
                                        //errorStrings[ProductMaintConstants.PDB_PRODUCT_MAINT_ERROR_IDX_PRODUCT_TYPE]=strDescription;
                                    }

                                    else if (strFieldValue.compareToIgnoreCase(ProductMaintConstants.PDB_NOVATOR_FIELD_NAME_SUB_TYPE) == 
                                             0) {
                                        bHasNovatorErrors = true;
                                        errors.add("productSubType", 
                                                   new ActionMessage(strDescription, 
                                                                     false));
                                        //productDetailForm.set("subtypeError",true);
                                        //errorStrings[ProductMaintConstants.PDB_PRODUCT_MAINT_ERROR_IDX_PRODUCT_SUB_TYPE]=strDescription;
                                    }

                                    else if (strFieldValue.compareToIgnoreCase(ProductMaintConstants.PDB_NOVATOR_FIELD_NAME_VENDOR_ID) == 
                                             0) {
                                        bHasNovatorErrors = true;
                                        errors.add("vendor", 
                                                   new ActionMessage(strDescription, 
                                                                     false));
                                        //productDetailForm.set("vendorListError",true);
                                        //errorStrings[ProductMaintConstants.PDB_PRODUCT_MAINT_ERROR_IDX_PRODUCT_VENDOR_LIST]=strDescription;
                                    }

                                    else if (strFieldValue.compareToIgnoreCase(ProductMaintConstants.PDB_NOVATOR_FIELD_NAME_SHIPPING_KEY) == 
                                             0) {
                                        bHasNovatorErrors = true;
                                        errors.add("shippingKey", 
                                                   new ActionMessage(strDescription, 
                                                                     false));
                                        //productDetailForm.set("shippingKeyError",true);
                                        //errorStrings[ProductMaintConstants.PDB_PRODUCT_MAINT_ERROR_IDX_PRODUCT_SHIPPING_KEY]=strDescription;
                                    }

                                    else if (strFieldValue.compareToIgnoreCase(ProductMaintConstants.PDB_NOVATOR_FIELD_NAME_VALID_SHIP_DAYS) == 
                                             0) {
                                        bHasNovatorErrors = true;
                                        errors.add("validShipDaysError", 
                                                   new ActionMessage(strDescription, 
                                                                     false));
                                        //productDetailForm.set("validShipDaysError",true);
                                        //errorStrings[ProductMaintConstants.PDB_PRODUCT_MAINT_ERROR_IDX_PRODUCT_SHIP_DAY]=strDescription;
                                    }

                                    else if (strFieldValue.compareToIgnoreCase(ProductMaintConstants.PDB_NOVATOR_FIELD_NAME_DELIVERY_OPTIONS) == 
                                             0) {
                                        bHasNovatorErrors = true;
                                        errors.add("deliveryOptionsError", 
                                                   new ActionMessage(strDescription, 
                                                                     false));
                                        //productDetailForm.set("deliveryOptionsError",true);
                                        //errorStrings[ProductMaintConstants.PDB_PRODUCT_MAINT_ERROR_IDX_PRODUCT_DELIVERY_OPTION]=strDescription;
                                    }

                                    else if (strFieldValue.compareToIgnoreCase(ProductMaintConstants.PDB_NOVATOR_FIELD_NAME_SUB_PRODUCT_ID_OPTION) == 
                                             0) {
                                        if (productVO.isSubcodeFlag() && 
                                            subCodes != null) {
                                            for (int subidx = 0; 
                                                 subidx < subCodes.size(); 
                                                 subidx++) {
                                                ProductSubCodeVO pscvo = 
                                                    (ProductSubCodeVO)subCodes.get(subidx);
                                                if (pscvo != null) {
                                                    if (strSubCode.compareToIgnoreCase(pscvo.getProductSubCodeId()) == 
                                                        0) {
                                                        optionErrorFlags[subidx] = 
                                                                true;
                                                        bHasNovatorErrors = 
                                                                true;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    else if (strFieldValue.compareToIgnoreCase(ProductMaintConstants.PDB_NOVATOR_FIELD_NAME_SUB_PRODUCT_ID_PRICE) == 
                                             0) {
                                        if (productVO.isSubcodeFlag() && 
                                            subCodes != null) {
                                            for (int subidx = 0; 
                                                 subidx < subCodes.size(); 
                                                 subidx++) {
                                                ProductSubCodeVO pscvo = 
                                                    (ProductSubCodeVO)subCodes.get(subidx);
                                                if (pscvo != null) {
                                                    if (strSubCode.compareToIgnoreCase(pscvo.getProductSubCodeId()) == 
                                                        0) {
                                                        priceErrorFlags[subidx] = 
                                                                true;
                                                        bHasNovatorErrors = 
                                                                true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                if (bHasNovatorErrors == true) {
                                    //                                        request.setAttribute(ProductMaintConstants.PDB_PRODUCT_SUB_CODE_ID_ERROR_KEY, optionErrorFlags);
                                    //                                        request.setAttribute(ProductMaintConstants.PDB_PRODUCT_SUB_CODE_PRICE_ERROR_KEY, priceErrorFlags);
                                    ////                                        request.setAttribute(ProductMaintConstants.PDB_PRODUCT_ERROR_DESC_KEY, errorStrings);
                                    productDetailForm.set(ProductMaintConstants.PDB_PRODUCT_SUB_CODE_ID_ERROR_KEY, 
                                                          optionErrorFlags);
                                    productDetailForm.set(ProductMaintConstants.PDB_PRODUCT_SUB_CODE_PRICE_ERROR_KEY, 
                                                          priceErrorFlags);
                                    errobjs[0] = 
                                            "Database has not been updated.  The fields marked in RED are in error and need to be changed.";
                                }
                            } //end if bValidXMLDocument

                            //                                request.setAttribute(ProductMaintConstants.OE_PRODUCT_ID_KEY, productId);
                            //                                request.setAttribute(ProductMaintConstants.PDB_PRODUCT_SEARCH_BY_KEY, ProductMaintConstants.PDB_SEARCH_BY_PRODUCT_ID);   
                            productDetailForm.set(ProductMaintConstants.OE_PRODUCT_ID_KEY, 
                                                  productId);
                            productDetailForm.set(ProductMaintConstants.PDB_PRODUCT_SEARCH_BY_KEY, 
                                                  ProductMaintConstants.PDB_SEARCH_BY_PRODUCT_ID);
                        } //end if error array length > 0
                    } //end if actionerror
                }

                String[] colorsArray = 
                    (String[])productDetailForm.get("arrangementColorsArray");
                includedColors = new ArrayList();
                if (colorsArray == null){
                    for (int i = 0; i < colorsArray.length; i++) {
                        ColorVO dummyVO = new ColorVO();
                        dummyVO.setId((String)colorsArray[i]);
                        int index = colors.indexOf(dummyVO);
                        ColorVO color = (ColorVO)colors.get(index);
                        includedColors.add(color);
                    }
                }

                String[] componentsArray = (String[]) productDetailForm.get("componentSkuArray");
                includedComponents = new ArrayList();
                if (componentsArray != null) {
                    for (int i=0; i<componentsArray.length; i++) {
                        ValueVO dummyVO = new ValueVO();
                        dummyVO.setId(componentsArray[i]);
                        int index = components.indexOf(dummyVO);
                        ValueVO component = (ValueVO)components.get(index);
                        includedComponents.add(component);
                        logger.debug("errorComponent: " + dummyVO.getId() + " " + dummyVO.getDescription());
                    }
                }
                // add the state delivery exclusion
                String[] sdeStateIdArray = 
                    request.getParameterValues(ProductMaintConstants.EXCLUDED_STATE_ID);
                String[] sdeStateNameArray = 
                    request.getParameterValues(ProductMaintConstants.EXCLUDED_STATE_NAME);
                String[] sdeSunExcluded = 
                    request.getParameterValues(ProductMaintConstants.DELV_DAY_SUN_KEY);
                String[] sdeMonExcluded = 
                    request.getParameterValues(ProductMaintConstants.DELV_DAY_MON_KEY);
                String[] sdeTueExcluded = 
                    request.getParameterValues(ProductMaintConstants.DELV_DAY_TUE_KEY);
                String[] sdeWedExcluded = 
                    request.getParameterValues(ProductMaintConstants.DELV_DAY_WED_KEY);
                String[] sdeThuExcluded = 
                    request.getParameterValues(ProductMaintConstants.DELV_DAY_THU_KEY);
                String[] sdeFriExcluded = 
                    request.getParameterValues(ProductMaintConstants.DELV_DAY_FRI_KEY);
                String[] sdeSatExcluded = 
                    request.getParameterValues(ProductMaintConstants.DELV_DAY_SAT_KEY);

                excludedStates = new ArrayList();
                StateDeliveryExclusionVO sdeVO;
                if (sdeStateIdArray != null && sdeStateIdArray.length > 0) {
                    List sdeSunExcludedList = null;
                    List sdeMonExcludedList = null;
                    List sdeTueExcludedList = null;
                    List sdeWedExcludedList = null;
                    List sdeThuExcludedList = null;
                    List sdeFriExcludedList = null;
                    List sdeSatExcludedList = null;

                    if (sdeSunExcluded != null)
                        sdeSunExcludedList = Arrays.asList(sdeSunExcluded);

                    if (sdeMonExcluded != null)
                        sdeMonExcludedList = Arrays.asList(sdeMonExcluded);

                    if (sdeTueExcluded != null)
                        sdeTueExcludedList = Arrays.asList(sdeTueExcluded);

                    if (sdeWedExcluded != null)
                        sdeWedExcludedList = Arrays.asList(sdeWedExcluded);

                    if (sdeThuExcluded != null)
                        sdeThuExcludedList = Arrays.asList(sdeThuExcluded);

                    if (sdeFriExcluded != null)
                        sdeFriExcludedList = Arrays.asList(sdeFriExcluded);

                    if (sdeSatExcluded != null)
                        sdeSatExcludedList = Arrays.asList(sdeSatExcluded);

                    for (int i = 0; i < sdeStateIdArray.length; i++) {
                        sdeVO = new StateDeliveryExclusionVO();

                        if (sdeStateIdArray[i] != null && 
                            sdeStateIdArray[i].length() > 0) {
                            sdeVO.setExcludedState(sdeStateIdArray[i]);
                        }

                        if (sdeStateNameArray[i] != null && 
                            sdeStateNameArray[i].length() > 0) {
                            sdeVO.setStateName(sdeStateNameArray[i]);
                        }

                        if (sdeSunExcludedList != null && 
                            sdeSunExcludedList.contains(String.valueOf(i))) {
                            sdeVO.setSunExcluded(true);
                        }

                        if (sdeMonExcludedList != null && 
                            sdeMonExcludedList.contains(String.valueOf(i))) {
                            sdeVO.setMonExcluded(true);
                        }

                        if (sdeTueExcludedList != null && 
                            sdeTueExcludedList.contains(String.valueOf(i))) {
                            sdeVO.setTueExcluded(true);
                        }

                        if (sdeWedExcludedList != null && 
                            sdeWedExcludedList.contains(String.valueOf(i))) {
                            sdeVO.setWedExcluded(true);
                        }

                        if (sdeThuExcludedList != null && 
                            sdeThuExcludedList.contains(String.valueOf(i))) {
                            sdeVO.setThuExcluded(true);
                        }

                        if (sdeFriExcludedList != null && 
                            sdeFriExcludedList.contains(String.valueOf(i))) {
                            sdeVO.setFriExcluded(true);
                        }

                        if (sdeSatExcludedList != null && 
                            sdeSatExcludedList.contains(String.valueOf(i))) {
                            sdeVO.setSatExcluded(true);
                        }

                        excludedStates.add(sdeVO);
                    }
                }

            
                // add sub codes to the subcode list
                subCodeList = ProductMaintDetailForm.extractSubcodeListFromMap(request.getParameterMap());

                // Setup the correct sub types for this type
                String typeId = (String)productDetailForm.get("productType");
                if (typeId != null) {
                    for (int j = 0; j < subTypes.size(); j++) {
                        String subTypeId = 
                            ((ProductSubTypeVO)subTypes.get(j)).getTypeId();
                        if (typeId.equals(subTypeId)) {
                            currentSubTypes.add(subTypes.get(j));
                        }
                    }
                }
            }

            // Put the correct countries in the current country list
            List currCountries = new ArrayList();
            if (StringUtils.equals((String)productDetailForm.get("country"), 
                                   ProductMaintConstants.DOMESTIC_COUNTRY_CODE)) {
                for (int i = 0; i < countries.size(); i++) {
                    if (!((ValueVO)countries.get(i)).getId().equals(ProductMaintConstants.DOMESTIC_COUNTRY_CODE)) {
                        currCountries.add(countries.get(i));
                    }
                }
            } else {
                ValueVO vo = new ValueVO();
                vo.setId(ProductMaintConstants.DOMESTIC_COUNTRY_CODE);
                vo.setDescription(ProductMaintConstants.DOMESTIC_COUNTRY_DESC);
                currCountries.add(vo);
            }
            session.setAttribute(ProductMaintConstants.CURRENT_COUNTRY_LIST_KEY, currCountries);
            productDetailForm.set(ProductMaintConstants.CURRENT_COUNTRY_LIST_KEY, 
                                  currCountries);

            // Remove any excluded colors from the included colors list
            excludedColors = new ArrayList(colors);
            List tempCurrentColors = new ArrayList(includedColors.size());
            for (int i = 0; i < includedColors.size(); i++) {
                ColorVO colorVO = (ColorVO)includedColors.get(i);
                int orderBy = colorVO.getOrderBy();
                if (excludedColors.contains(colorVO)) {
                    int index = excludedColors.indexOf(colorVO);
                    colorVO = (ColorVO)excludedColors.remove(index);
                    colorVO.setOrderBy(orderBy);
                }

                tempCurrentColors.add(colorVO);
            }

            includedColors = new ArrayList(tempCurrentColors);
            ColorVOComparator colorVOComparator = new ColorVOComparator();
            Collections.sort(includedColors, colorVOComparator);

            // Create the subcode array
            if (subCodeList == null) {
                subCodeList = new ArrayList();
            }

            // Remove component skus from the available component sku list
            availableComponents = new ArrayList(components);
            for (int i=0; i<includedComponents.size(); i++) {
                ValueVO valueVO = (ValueVO) includedComponents.get(i);
                if (availableComponents.contains(valueVO)) {
                    availableComponents.remove(valueVO);
                }
            }
            // Save the lists to the request
            session.setAttribute(ProductMaintConstants.LAST_SUBCODE_KEY, new Integer((subCodeList.size() - 1)));
            session.setAttribute(ProductMaintConstants.SUB_CODE_LIST_KEY, subCodeList);
//                request.setAttribute(ProductMaintConstants.HIDE_SUBCODE_LIST_KEY, hideSubCodeList);
            session.setAttribute(ProductMaintConstants.PRODUCT_CURRENT_SUB_TYPES_KEY, currentSubTypes);
            session.setAttribute(ProductMaintConstants.EXCLUDED_STATES, excludedStates);
            session.setAttribute(ProductMaintConstants.INCLUDED_COLORS, includedColors);            
            session.setAttribute(ProductMaintConstants.EXCLUDED_COLORS, excludedColors);
            session.setAttribute(ProductMaintConstants.EXCLUDED_COLORS_STATIC, excludedColors);
            session.setAttribute(ProductMaintConstants.VENDOR_PRODUCTS, vendorProductsList);
            session.setAttribute(ProductMaintConstants.AVAILABLE_COMPONENT_SKUS, availableComponents);
            session.setAttribute(ProductMaintConstants.INCLUDED_COMPONENT_SKUS, includedComponents);
            session.setAttribute(ProductMaintConstants.PRODUCT_ADD_ON_LIST, productVO.getProductAddonMap().get(ProductMaintConstants.PRODUCT_ADDON_VO_ADDON_KEY));
            session.setAttribute(ProductMaintConstants.PRODUCT_ADD_ON_VASE_LIST, productVO.getProductAddonMap().get(ProductMaintConstants.PRODUCT_ADDON_VO_VASE_KEY));

            productDetailForm.set(ProductMaintConstants.LAST_SUBCODE_KEY, 
                                  new Integer((subCodeList.size() - 1)));
            productDetailForm.set(ProductMaintConstants.SUB_CODE_LIST_KEY, 
                                  subCodeList);
            productDetailForm.set(ProductMaintConstants.PRODUCT_CURRENT_SUB_TYPES_KEY, 
                                  currentSubTypes);
            productDetailForm.set(ProductMaintConstants.EXCLUDED_STATES, 
                                  excludedStates);
            productDetailForm.set(ProductMaintConstants.INCLUDED_COLORS, 
                                  includedColors);
            productDetailForm.set(ProductMaintConstants.EXCLUDED_COLORS, 
                                  excludedColors);
            productDetailForm.set(ProductMaintConstants.EXCLUDED_COLORS_STATIC, 
                    excludedColors);
            productDetailForm.set(ProductMaintConstants.VENDOR_PRODUCTS,
                                  vendorProductsList);
            productDetailForm.set(ProductMaintConstants.AVAILABLE_COMPONENT_SKUS, 
                                  availableComponents);
            productDetailForm.set(ProductMaintConstants.INCLUDED_COMPONENT_SKUS, 
                                  includedComponents);
            productDetailForm.set(ProductMaintConstants.PRODUCT_ADD_ON_LIST, 
                                  productVO.getProductAddonMap().get(ProductMaintConstants.PRODUCT_ADDON_VO_ADDON_KEY));
            productDetailForm.set(ProductMaintConstants.PRODUCT_ADD_ON_VASE_LIST, 
                                  productVO.getProductAddonMap().get(ProductMaintConstants.PRODUCT_ADDON_VO_VASE_KEY));

            List availList = new ArrayList();
            ValueVO valueVO = new ValueVO();
            valueVO.setId(ProductMaintConstants.INTERNATIONAL_CODE);
            valueVO.setDescription("International");
            availList.add(valueVO);
            valueVO = new ValueVO();
            valueVO.setId(ProductMaintConstants.DOMESTIC_CODE);
            valueVO.setDescription("Domestic");
            availList.add(valueVO);
            session.setAttribute(ProductMaintConstants.SHIPPING_AVAIL_KEY, availList);            
            productDetailForm.set(ProductMaintConstants.SHIPPING_AVAIL_KEY, 
                                  availList);
            if(request.getAttribute(ProductMaintConstants.COPY_PRODUCT_COPY) == null || 
             		!request.getAttribute(ProductMaintConstants.COPY_PRODUCT_COPY).equals(ProductMaintConstants.COPY_PRODUCT_COPY)) {

            //get upsell skus
            List upsellList = upsellSVC.getUpsellDetailByID(productId);
            //                request.setAttribute(ProductMaintConstants.PRODUCT_MASTER_SKU_KEY, upsellList);
            productDetailForm.set(ProductMaintConstants.PRODUCT_MASTER_SKU_KEY, 
                                  upsellList);
            }
            //only insert instruction if one does not exist
            String upsellInst = 
                (String)request.getAttribute(ProductMaintConstants.PRODUCT_MAINT_UPSELL_INSTRUCTION_KEY);
            if (upsellInst == null || upsellInst.equals("")) {
                //                    request.setAttribute(ProductMaintConstants.PRODUCT_MAINT_UPSELL_INSTRUCTION_KEY, super.getResources(request).getMessage(ProductMaintConstants.PRODUCT_MAINT_UPSELL_ATTACHED_VIEW_KEY));
                productDetailForm.set(ProductMaintConstants.PRODUCT_MAINT_UPSELL_INSTRUCTION_KEY, 
                                      super.getResources(request).getMessage(ProductMaintConstants.PRODUCT_MAINT_UPSELL_ATTACHED_VIEW_KEY));
            }

            //8/14/03 - Jeff Penney - set check values in delivery option array
            // set the selected shipping methods
            List shipMethodsList = productVO.buildDeliveryOptionsList(request);
            for (int i = 0; i < deliveryOptionList.size(); i++) {
                DeliveryOptionVO deliveryVO = 
                    (DeliveryOptionVO)deliveryOptionList.get(i);
                updateDeliveryData(shipMethodsList, deliveryVO);
            }
            //8/14/03 - end
            
            // SKU IT - CHANGES to not allow user to make product available when there is no inventory loaded for the product
            boolean hasInventoryRecord = productSVC.hasInventoryRecord(productId);
            productDetailForm.set(ProductMaintConstants.HAS_INVENTORY_RECORD, hasInventoryRecord);
        } catch (Exception e) {
            return super.handleException(request, mapping, e);
        }

        session.setAttribute(ProductMaintConstants.SHIPPING_METHODS_KEY, deliveryOptionList);
        productDetailForm.set(ProductMaintConstants.SHIPPING_METHODS_KEY, 
                              deliveryOptionList);

        // Get the instructional text if any exists
        String instruction = 
            (String)request.getAttribute(ProductMaintConstants.PRODUCT_MAINT_INSTRUCTION_KEY);
//        session.setAttribute(ProductMaintConstants.PRODUCT_MAINT_INSTRUCTION_KEY, instruction);  //should show up in the form bean since it's a string
        productDetailForm.set(ProductMaintConstants.PRODUCT_MAINT_INSTRUCTION_KEY, 
                              instruction);

        // Build the back link.
        // If we came from product list page, build back link for product 
        // detail page - i.e., restore criteria used in product list query.
        // Otherwise go back to product maintenance form.
        String backArgs = "";
        if (request.getParameter("prodAvail") != null) {
            backArgs += "&prodAvail=Y";
        }
        if (request.getParameter("prodUnavail") != null) {
            backArgs += "&prodUnavail=Y";
        }
        if (request.getParameter("dateRestrict") != null) {
            backArgs += "&dateRestrict=Y";
        }
        if (request.getParameter("onHold") != null) {
            backArgs += "&onHold=Y";
        }
        if (backArgs.length() > 0) {
            productDetailForm.set(ProductMaintConstants.MENU_BACK_OPTIONAL_ARGS, 
                                  backArgs); // Referred to in header.jsp
        }
        // Set the back link
        request.setAttribute(ProductMaintConstants.MENU_BACK_KEY, 
                             ProductMaintConstants.MENU_BACK_PRODUCT_MAINT_LIST_KEY);
        request.setAttribute(ProductMaintConstants.MENU_BACK_TEXT_KEY, 
                             ProductMaintConstants.MENU_BACK_TEXT_PRODUCT_MAINT_LIST_KEY);

        String disableBatchMode = 
            (String)request.getAttribute(ProductMaintConstants.DISABLE_BATCH_MODE);
        if (StringUtils.isBlank(disableBatchMode)) {
            disableBatchMode = "false";
        }
        productDetailForm.set(ProductMaintConstants.DISABLE_BATCH_MODE, 
                              disableBatchMode);

        String closePopUp = 
            (String)request.getAttribute(ProductMaintConstants.CLOSE_POPUP);
        productDetailForm.set(ProductMaintConstants.CLOSE_POPUP, closePopUp);
        
        productDetailForm.set(ProductMaintConstants.OPEN_ALL_TABS,"N");


        request.setAttribute(mapping.getAttribute(), productDetailForm);
        
        return (mapping.findForward("success"));
    }

    /**
        * This method searches a Delivery Option list for the  passed in delivery
        * object.  If found the select flag is set and other object data is copied..
        * @param valueList Array of delivery option objects to be searched
        * @param searchForVO to be searched for
        * @retrun boolean  True= found, False=not found
        *
        * Created 6/9/03 - Ed Mueller
        */
    private void updateDeliveryData(List valueList, 
                                    DeliveryOptionVO searchForVO) {
        boolean found = false;

        int i = 0;
        //loop through each element
        while (i < valueList.size() && !found) {
            DeliveryOptionVO listVO = (DeliveryOptionVO)valueList.get(i);
            if (listVO.getShippingMethodId().equals(searchForVO.getShippingMethodId())) {
                searchForVO.setSelected(true);
                searchForVO.setDefaultCarrier(listVO.getDefaultCarrier());
                found = true;
            } //end check if equal
            i++;
        } //end while


    }

    /**
         * Builds the string used to find fieleds in the Novator response
         * @param strFieldName raw Novator field name
         * @return the search string used to locate the field in the Novator response
         */
    public String buildNovatorTestString(String strFieldName) throws Exception {
        StringBuffer sb = new StringBuffer();

        if (bAttemptedLookup == false) {
            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
            strPrefix = 
                    configUtil.getFrpGlobalParmNoNull(ProductMaintConstants.PDB_CONFIG_CONTEXT, 
                                           ProductMaintConstants.PDB_NOVATOR_LIVE_KEY);

            if (strPrefix == null || strPrefix.length() == 0) {
                strPrefix = 
                        configUtil.getFrpGlobalParmNoNull(ProductMaintConstants.PDB_CONFIG_CONTEXT, 
                                               ProductMaintConstants.PDB_NOVATOR_TEST_KEY);
            }

            if (strPrefix == null || strPrefix.length() == 0) {
                strPrefix = 
                        configUtil.getFrpGlobalParmNoNull(ProductMaintConstants.PDB_CONFIG_CONTEXT, 
                                               ProductMaintConstants.PDB_NOVATOR_UAT_KEY);
            }

            if (strPrefix == null || strPrefix.length() == 0) {
                strPrefix = 
                        configUtil.getFrpGlobalParmNoNull(ProductMaintConstants.PDB_CONFIG_CONTEXT, 
                                               ProductMaintConstants.PDB_NOVATOR_CONTENT_KEY);
            }

            bAttemptedLookup = true;
        }

        sb.append("<BR>");
        if (strPrefix != null && strPrefix.length() > 0) {
            sb.append(strPrefix);
            sb.append(":");
        }

        sb.append(strFieldName);
        sb.append(" - ");

        return sb.toString().toLowerCase();
    }

    public void setLookupSVC(ILookupSVCBusiness lookupSVC) {
        this.lookupSVC = lookupSVC;
    }

    public ILookupSVCBusiness getLookupSVC() {
        return lookupSVC;
    }

    public void setProductBatchSVC(IProductBatchSVCBusiness productBatchSVC) {
        this.productBatchSVC = productBatchSVC;
    }

    public IProductBatchSVCBusiness getProductBatchSVC() {
        return productBatchSVC;
    }

    public void setProductSVC(IProductMaintSVCBusiness productSVC) {
        this.productSVC = productSVC;
    }

    public IProductMaintSVCBusiness getProductSVC() {
        return productSVC;
    }

    public void setUpsellSVC(IUpsellSVCBusiness upsellSVC) {
        this.upsellSVC = upsellSVC;
    }

    public IUpsellSVCBusiness getUpsellSVC() {
        return upsellSVC;
    }
    
}
