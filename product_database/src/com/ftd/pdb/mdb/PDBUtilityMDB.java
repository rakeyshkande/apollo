package com.ftd.pdb.mdb;

import com.ftd.osp.utilities.LockUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.stats.ServiceResponseTrackingUtil;
import com.ftd.osp.utilities.vo.ServiceResponseTrackingVO;
import com.ftd.pdb.businessobject.IProductBO;
import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.resources.IResourceProvider;
import com.ftd.pdb.common.utilities.FTDUtil;
import com.ftd.pdb.common.utilities.GrouponMsgProducer;
import com.ftd.pdb.common.utilities.FreshMsgProducer;
import com.ftd.pdb.common.valobjs.ProductVO;
import com.ftd.pdb.integration.dao.IProductDAO;
import com.ftd.pdb.pif.businessobject.IProductIndexBO;
import com.ftd.pdb.pif.dao.IProductIndexFeedDAO;
import com.ftd.pdb.sif.businessobject.ShoppingIndexBO;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.StringTokenizer;

import javax.ejb.MessageDrivenContext;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.commons.lang.StringUtils;

import org.springframework.context.access.ContextSingletonBeanFactoryLocator;
import org.springframework.ejb.support.AbstractJmsMessageDrivenBean;



public class PDBUtilityMDB extends AbstractJmsMessageDrivenBean implements MessageListener {
    private MessageDrivenContext context;
    private static Logger logger = 
        new Logger("com.ftd.pdb.mdb.PDBUtilityMDB");
    private IResourceProvider resourceProvider;
    private IProductBO productBO;
    private IProductIndexBO pifBO;
    private ShoppingIndexBO sifBO;
    private IProductIndexFeedDAO pifDAO;
    private IProductDAO prodDAO;
    private static final String OPERATION_INDEX_LOAD = "INDEX_LOAD";
    private static final String FRESH_SERVICE_TRIGGER = "Fresh Trigger";
    
    public void ejbCreate() {
        setBeanFactoryLocatorKey("pdbApp");
        setBeanFactoryLocator(ContextSingletonBeanFactoryLocator.getInstance());
        super.ejbCreate();
    }

    protected void onEjbCreate() {
        resourceProvider = (IResourceProvider)this.getBeanFactory().getBean("resourceProvider");
        productBO = (IProductBO)this.getBeanFactory().getBean("productBO");
        pifBO = (IProductIndexBO)this.getBeanFactory().getBean("pifBO");
        sifBO = (ShoppingIndexBO)this.getBeanFactory().getBean("sifBO");
        pifDAO = (IProductIndexFeedDAO)this.getBeanFactory().getBean("pifDAO");
        prodDAO = (IProductDAO)this.getBeanFactory().getBean("productDAO");
    }
        
    public void onMessage(Message message) {
        logger.debug("Start: onMessage()");

        Connection connection = null;
        String arg = null;
        String errorMsg="";
        LockUtil lockUtil = new LockUtil();
        boolean lockObtained = false;
        String sessionId = "";

        try {
            arg = StringUtils.trim(((TextMessage)message).getText());

            if (StringUtils.isBlank(arg)) {
//                assert (false) : "PDB Utility MDB has received an empty payload";
                //Send out system notification 
                try {
                    FTDUtil.sendSystemMessage(resourceProvider.getDatabaseConnection(),"PDB Utility MDB has received an empty payload.");
                } catch (Exception e) {
                    logger.error("Unable to send message to support pager.");
                    logger.fatal(e);
                }
                return;
            }
            
            errorMsg = "PDB error on server "+FTDUtil.getHostName()+" while processing MDB process "+arg;

            connection = resourceProvider.getDatabaseConnection();
            
            if( StringUtils.equals(arg,"EOD") ) {
                productBO.updateExceptionProducts(connection);
            } else if (StringUtils.equals(arg,"INDEX_GET")) {
                if( pifBO.get(connection) > 0) {
                    pifDAO.postAMessage(connection, "OJMS.PDB", OPERATION_INDEX_LOAD, OPERATION_INDEX_LOAD, 0 );
                }
            } else if (arg.startsWith(OPERATION_INDEX_LOAD) ) {
                //if there is a second arg and it's numeric
                //then it's the primary key from the FRP.FILE_ARCHIVE table
                String strTmp = StringUtils.trimToEmpty(StringUtils.substringAfter(arg,OPERATION_INDEX_LOAD));
                Long fileArchiveId;
                
                if( StringUtils.isBlank(strTmp) ) {
                    fileArchiveId = null;
                } else {
                    try {
                        fileArchiveId = new Long(strTmp);
                        if( fileArchiveId.longValue() < 1) {
                            logger.warn("Id value of "+strTmp+" passed in with "+OPERATION_INDEX_LOAD+".  Will work with the last index files received.");
                            fileArchiveId = null;
                        }
                    } catch (Exception e) {
                            logger.warn("Invalid id value of "+strTmp+" passed in with "+OPERATION_INDEX_LOAD+".  Will work with the last index files received.");
                        fileArchiveId = null;
                    }
                }
                if( pifBO.load(connection,fileArchiveId) > 0 ) {
                     pifDAO.postAMessage(connection, "OJMS.PDB", "INDEX_STAGE", "INDEX_STAGE", 0 );
                }
            } else if (StringUtils.equals(arg,"INDEX_STAGE")) {
                pifBO.stage(connection);  
//            } else if (StringUtils.equals(arg,"INDEX_BACKUP")) {
//                pifBO.backup(connection);  
            } else if (StringUtils.equals(arg,"INDEX_MOVE")) {
                pifBO.backup(connection);
                pifBO.move(connection);
            } else if (StringUtils.equals(arg,"SHOPPING_INDEX_GET")) {
                // Get all shopping index files from remote server.
                
                // Create process lock.
                try {
                    sessionId = FTDUtil.getLockSessionId();
                    lockObtained = lockUtil.obtainProcessLock(connection, "SHOPPING_INDEX_PROCESS", sessionId);
                    logger.info("SessionId for process SHOPPING_INDEX_PROCESS:" + sessionId);
                } catch(Exception ex) {
                    throw new Exception("Failed to obtain lock.");
                } 
                
                if(!lockObtained) {
                    throw new Exception("Process is locked. Please verify that another process is" +
                    " running. You can ignore this page if so.");
                } else {
                    sifBO.getShoppingIndex(connection, sessionId); 
                }
                 
            } else if (arg.startsWith("SHOPPING_INDEX_READ")) {
                // Saves indexes in one file to stage tables.
                // Payload is in the format of SHOPPING_INDEX_READ|filename|sessionId
                StringTokenizer st = new StringTokenizer(arg, "|");
                String actionType = st.nextToken(); 
                String filename = st.nextToken();
                String retry = st.nextToken();
                logger.debug(actionType + " " + filename + " " + retry);
                
                sifBO.readShoppingIndex(connection, filename, Integer.parseInt(retry));   
            } else if (arg.startsWith("SHOPPING_INDEX_DELETE")) {
                // Saves indexes in one file to stage tables.
                sifBO.deleteShoppingIndex(connection);    

            } else if (arg.startsWith("GROUPON_MSG_PRODUCER")) {
               // Forwards message to the Groupon Message Producer microservice.
               // Payload is in the format of GROUPON_MSG_PRODUCER|message|topic
               StringTokenizer st = new StringTokenizer(arg, "|");
               String actionType = st.nextToken(); 
               String prodId = st.nextToken();
               String topic = null;
               ProductVO pvo = null;
               boolean prodAvailable = true;
               if (st.hasMoreTokens()) {
                  topic = st.nextToken();
               } else {
                  // Topic was not specified, so get product status 
                  // (since that will help determine default topic)
                  pvo = productBO.getProduct(connection, prodId, null);
                  prodAvailable = pvo.isStatus();
               }
               GrouponMsgProducer gmp = new GrouponMsgProducer();
               gmp.sendToMessageProducer(prodId, topic, prodAvailable);

            } else if (arg.startsWith("FRESH_MSG_PRODUCER")) {
               // Publishes message to Fresh PubSub topic.
               // Payload is in the format of FRESH_MSG_PRODUCER|source|reason|id
               long startTime = (new Date()).getTime();
               StringTokenizer st = new StringTokenizer(arg, "|");
               String actionType = st.nextToken(); 
               String source  = st.nextToken();
               String reason = st.nextToken();
               String prodId = st.nextToken();
               FreshMsgProducer fmp = new FreshMsgProducer();
               fmp.sendToMessageProducer(prodId, source, reason);
               logServiceResponseTracking(prodId, reason, startTime);

            } else if (arg.startsWith("FRESH_MULTI_MSG_PRODUCER")) {
               // Publishes multiple messages to Fresh PubSub topic
               // Payload is in the format of FRESH_MULTI_MSG_PRODUCER|multType|reason|param
               long startTime = (new Date()).getTime();
               StringTokenizer st = new StringTokenizer(arg, "|");
               String actionType = st.nextToken(); 
               String multiType  = st.nextToken();
               String reason = st.nextToken();
               String param = st.nextToken();
               if ("product".equals(multiType)) {
                  // Determines all products updated since passed date and re-enqueues
                  // to FRESH_MSG_PRODUCER so they are sent to Fresh PubSub topic.
                  // "param" will be the date string.
                  logger.info("Enqueuing bulk update of products for Project Fresh: " + multiType + ", " + reason + ", " + param);
                  prodDAO.projectFreshBulkUpdate(connection, multiType, param); 
                  logServiceResponseTracking(param, reason, startTime);
               } else if ("sourcecode".equals(multiType)) {
                  // Determines all sourcecodes updated since passed date and re-enqueues
                  // to FRESH_MSG_PRODUCER so they are sent to Fresh PubSub topic.
                  // "param" will be the date string.
                  logger.info("Enqueuing bulk update of sourcecodes for Project Fresh: " + multiType + ", " + reason + ", " + param);
                  prodDAO.projectFreshBulkUpdate(connection, multiType, param); 
                  logServiceResponseTracking(param, reason, startTime);
               } else if ("addon".equals(multiType)) {
                  // Determines all products associated with the addon and re-enqueues the products
                  // to FRESH_MSG_PRODUCER so they are sent to Fresh PubSub topic.
                  // "param" will be the addon ID.
                  logger.info("Enqueuing bulk update of products for Project Fresh: " + multiType + ", " + reason + ", " + param);
                  prodDAO.projectFreshAddonUpdate(connection, param); 
                  logServiceResponseTracking(param, reason, startTime);
               } else {
                  throw new Exception("Unsupported FRESH_MULTI_MSG_PRODUCER type of " + multiType);                      
               }
                              
            } else {
                throw new Exception("Unsupported operation of "+arg);    
            }
            
        } catch (Throwable e) {
        
            //Log the exception
            logger.error(e);

            //Send out a system page
            try {
                FTDUtil.sendSystemMessage(resourceProvider.getDatabaseConnection(),errorMsg+"\r\n"+e.getMessage());
            } catch (Exception e4) {
                logger.error("Failed to send system message: "+e4.getMessage(),e4);
            }
            
            // Release lock for SIF process. Only release the lock in case of an exception. 
            // In normal processing, the lock is released after all files are processed.
            if(lockObtained) {
                try{
                    lockUtil.releaseProcessLock(connection, "SHOPPING_INDEX_PROCESS", sessionId);
                } catch (Exception le) {
                    logger.error(le);
                    try {
                        FTDUtil.sendSystemMessage(resourceProvider.getDatabaseConnection(),"Failed to release lock: " + le.getMessage());
                    } catch (Exception se) {
                        logger.error("Failed to send system message: " + se.getMessage());
                    }
                }
            }
                        
            //Roll back the transaction
            context.setRollbackOnly();
           	throw new RuntimeException("Rollback detected, exception thrown to force rollback.");
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (Exception e) {
                logger.warn(e);
            }
        }
    }

    private void logServiceResponseTracking(String requestId, String serviceMethod, long startTime) {      
      Connection con = null;
      try {
         con = resourceProvider.getDatabaseConnection();
         long responseTime = System.currentTimeMillis() - startTime;
         ServiceResponseTrackingVO srtVO = new ServiceResponseTrackingVO();
         srtVO.setServiceName(FRESH_SERVICE_TRIGGER);
         srtVO.setServiceMethod(serviceMethod);
         srtVO.setTransactionId(requestId);
         srtVO.setResponseTime(responseTime);
         srtVO.setCreatedOn(new Date());
         ServiceResponseTrackingUtil srtUtil = new ServiceResponseTrackingUtil();
         srtUtil.insert(con, srtVO);
      } catch (Exception e) {
         logger.error("Error in logServiceResponseTracking " + e.getMessage());
      }
         finally
          {
            if (con != null)
            {
              try
              {
                con.close();
              }
              catch (SQLException sx)
              {
                logger.error(sx);
              }
            }
          }
    }
    
    
    public void setMessageDrivenContext(MessageDrivenContext ctx) {
        this.context = ctx;
    }
    
}
