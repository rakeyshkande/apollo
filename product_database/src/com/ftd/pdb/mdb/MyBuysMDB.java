package com.ftd.pdb.mdb;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pdb.integration.dao.IProductDAO;
import com.ftd.pdb.common.resources.IResourceProvider;
import com.ftd.pdb.common.utilities.FTDUtil;
import com.ftd.pdb.common.ProductMaintConstants;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.ejb.MessageDrivenContext;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.commons.lang.StringUtils;

import org.springframework.context.access.ContextSingletonBeanFactoryLocator;
import org.springframework.ejb.support.AbstractJmsMessageDrivenBean;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;

/**
 * MDB to send product unavailability feed to MyBuys
 */
public class MyBuysMDB extends AbstractJmsMessageDrivenBean implements MessageListener {
    private MessageDrivenContext context;
    private static Logger logger = 
        new Logger("com.ftd.pdb.mdb.MyBuysMDB");
    private IResourceProvider resourceProvider;
    private IProductDAO productDAO ;
    
    public void ejbCreate() {
        setBeanFactoryLocatorKey("pdbApp");
        setBeanFactoryLocator(ContextSingletonBeanFactoryLocator.getInstance());
        super.ejbCreate();
    }

    protected void onEjbCreate() {
        resourceProvider = (IResourceProvider)this.getBeanFactory().getBean("resourceProvider");
        productDAO = (IProductDAO)this.getBeanFactory().getBean("productDAO");
    }
        
    public void onMessage(Message message) {
        logger.debug("Start: onMessage()");

        Connection connection = null;
        String prodId = null;
        String webId = null;
        String url = null;
        String errorMsg="";
        GetMethod httpget = null;
        try {
            prodId = StringUtils.trim(((TextMessage)message).getText());

            if (StringUtils.isBlank(prodId)) {
                throw new Exception("MyBuys MDB received an empty payload");
            }
            errorMsg = "PDB error on server "+FTDUtil.getHostName()+" while processing MyBuys MDB for product: "+prodId;

            // Confirm MyBuys feeds are enabled
            String myBuysEnabled = resourceProvider.getGlobalParameter(ProductMaintConstants.MY_BUYS_CONTEXT, ProductMaintConstants.MY_BUYS_FEED_ENABLED_FLAG);
            if (myBuysEnabled != null && myBuysEnabled.equalsIgnoreCase("Y")) {
                
                connection = resourceProvider.getDatabaseConnection();

                // Get Website product ID
                webId = productDAO.getWebsiteProductId(connection,prodId);
                if (StringUtils.isBlank(webId)) {
                    throw new Exception("MyBuys MDB could not obtain Novator ID");
                }

                // Get MyBuys URL and replace tokens with product ID in message
                url = resourceProvider.getGlobalParameter(ProductMaintConstants.MY_BUYS_CONTEXT, ProductMaintConstants.MY_BUYS_FEED_REQUEST_URL);
                if (StringUtils.isBlank(url)) {
                    throw new Exception("No MyBuys feed URL was defined");
                }
                
                // Send to MyBuys
                httpget = executeURL(webId, url);
                
                // Get Upsell Master for product (if any)
                List upsellProds = productDAO.getUpsellMasterForBaseId(connection,prodId);
                if (upsellProds != null && upsellProds.size() > 0)
                {
                  for (Iterator it = upsellProds.iterator();it.hasNext();)
                  {
                      String upsellProdMasterId = (String)it.next();                    
                      httpget = executeURL(upsellProdMasterId,url);   // Send these to MyBuys too
                  }
                }
            } else {
                logger.info("MyBuys feed is not currently enabled, so ignoring message for product: " + prodId);
            }
            
        } catch (Throwable e) {
        
            //Log the exception
            logger.error(e);

            //Send out a system page
            try {
                FTDUtil.sendSystemMessage(resourceProvider.getDatabaseConnection(),errorMsg+"\r\n"+e.getMessage());
            } catch (Exception e4) {
                logger.error("Failed to send system message: "+e4.getMessage(),e4);
            }
                                    
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (Exception e) {
                logger.warn(e);
            }
            try {
                if (httpget != null) {
                     httpget.releaseConnection();
                }
            } catch (Exception e) {
                logger.warn(e);
            }
        }
    }

    public GetMethod executeURL(String Id, String url) throws Exception
    {
      url = url.replace(ProductMaintConstants.MY_BUYS_TOKEN_PROD_ID, Id);
      url = url.replace(ProductMaintConstants.MY_BUYS_TOKEN_SKU_ID, Id);
      if (logger.isDebugEnabled())
      {
        logger.debug("Sending MyBuys feed to: " + url);
      }

      // Send HTTP request to MyBuys
      HttpClient httpclient  = new HttpClient();
      GetMethod httpget = new GetMethod(url);
      int result = httpclient.executeMethod(httpget);
      if (result != HttpStatus.SC_OK)
      {
        throw new Exception("MyBuys HTTP request failed: " + httpget.getStatusLine());
      }
      return httpget ;
    }
  
  public void setMessageDrivenContext(MessageDrivenContext ctx) {
        this.context = ctx;
    }
    

}
