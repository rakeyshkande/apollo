package com.ftd.pdb.tests;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.List;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.ftd.pdb.businessobject.ProductBOImpl;
import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBSystemException;
import com.ftd.pdb.common.resources.IResourceProvider;
import com.ftd.pdb.common.resources.TestResourceProviderImpl;
import com.ftd.pdb.common.valobjs.ProductVO;
import com.ftd.pdb.integration.dao.ProductDAOImpl;
import com.ftd.pdb.service.LookupSVCImpl;
import com.ftd.pdb.service.ProductMaintSVCImpl;
import com.ftd.pdb.service.UpsellSVCImpl;


/**
 * The test class will extend junit.framework.TestCase
 **/
public class PDBMassEditTest extends TestCase  {
    Connection conn = null;
    ApplicationContext context = null;
    IResourceProvider resourceProvider = null;
    ProductBOImpl bo = null;
    LookupSVCImpl lookupSVC = null;
    ProductMaintSVCImpl productSVC = null;
    UpsellSVCImpl upsellSVC = null;
    ProductDAOImpl productDAO = null;
   
  public PDBMassEditTest(String name) {
      super(name);
  }

  /**
   * Override setup() to initialize variables
   **/
  protected void setUp() throws Exception{
	 resourceProvider = new TestResourceProviderImpl(); 
	 bo = new ProductBOImpl(); 
	 //Start up spring framework
     //context = new FileSystemXmlApplicationContext("C:\\Projects\\Workspaces\\FTDBranchGopherWoodDrop2\\product_database\\web\\WEB-INF\\spring-pdb-servlet.xml");
	 context = new ClassPathXmlApplicationContext("spring-pdb-servlet.xml");
     // get the database connection
	 this.conn = resourceProvider.getDatabaseConnection();
    
	 // Initialize the lookupSVC and set the resource provider to the test resource.  
  	 lookupSVC = (LookupSVCImpl)context.getBean("lookupSVC");
  	 bo.setLookupSVC(lookupSVC);
  	 lookupSVC.setResourceProvider(resourceProvider);
     //Initialize the lookupSVC and set the resource provider to the test resource. 
  	 productSVC = (ProductMaintSVCImpl) context.getBean("productSVC");
     bo.setProductSVC(productSVC);
     productSVC.setResourceProvider(resourceProvider);
     //Initialize the upsellSVC and set the resource provider to the test resource. 
  	 upsellSVC = (UpsellSVCImpl) context.getBean("upsellSVC");
     bo.setUpsellSVC(upsellSVC);
     upsellSVC.setResourceProvider(resourceProvider);
     //Initialize the productDAO and set the resource provider to the test resource. 
  	 productDAO = (ProductDAOImpl) context.getBean("productDAO");
     bo.setProductDAO(productDAO);
  }

  /**
   * Override tearDown() to release any permanent resources you allocated in setUp
   */
   public void tearDown(){
   }

   /**
    * Write the test case method in the fixture class. Be sure to make it public,
    * or it can't be invoked through reflection
    **/
  public void testGetPDBMassEditList(){
       // Call the BO to get the product mass edit list
        try
        {
        	// get the product for the master sku '00BB'
        	String[] searchItems = {"00BB", "", "", "", ""};
        	//System.out.println("connection=" + conn);
        	List<ProductVO> productList = 
				bo.getProductMassEditList(conn, "M", searchItems); 
            System.out.println("\n******* Start testGetPDBMassEditList for Master SKU 00BB *******");
            for(ProductVO vo: productList) {
                 System.out.println("Product ID = " + vo.getProductId() + 
                		" Product Name=" + vo.getProductName());
            }
        }
        catch(PDBApplicationException PDBApplicationException)
        {
            PDBApplicationException.printStackTrace();
        }
        catch(PDBSystemException PDBSystemException)
        {
            PDBSystemException.printStackTrace();
        }
        catch (Exception e) {
        	e.printStackTrace();
        }
  }
 
  /*
   * Change the validateProductMassEdit method in ProductBOImpl from private to public 
   * before testing this.
   * 
   * This is a test of 20 fields:
   *   Master SKU, productID, productName, novatorID, novatorName, floristRefNumber, category,
   *   productType, productSubType, description, status, typeOfDelivery, 
   *   standardRecipe, deluxeRecipe, premiumRecipe, dimWeight, standardPrice,
   *   deluxePrice, premiumPrice
   *   
   *   Master SKU, productID, 
   */
  public void testGeneralValidateProductMassEdits() {
	    ProductVO productVO = null;
	    System.out.println("\n****** BEGIN GENERAL TESTS *******");
	    try {
	     	//******************************************************************
	    	//   Test 1 - null ProductVO
	    	//******************************************************************
	    	String results =  bo.validateProductMassEdit(conn,productVO);
	    	System.out.println("General Test 1\t18822\tProductVO object is null\tExpect:\tActual: " + results);
	    	//******************************************************************
	    	//   Test 2 - instantiate ProductVO but don't set any fields
	    	//******************************************************************
	    	productVO = new ProductVO();
	    	results =  bo.validateProductMassEdit(conn,productVO);
	    	System.out.println("General Test 2\t18822\tRequired field test VO fields have not been initialized.\tExpect:\tActual: " + results);
	    	//******************************************************************
	    	//   Test 3 - instantiate ProductVO and test all length requirements.
	    	//******************************************************************
	    	productVO = new ProductVO();
	    	productVO.setMassEditMasterSKU(generateString(15));  // must be <= 10
	    	productVO.setProductId(generateString(15)); // must be <= 10
	    	productVO.setProductName(generateString(30)); //must be <= 25
	    	productVO.setNovatorId(generateString(15)); // must be <= 10
	    	productVO.setNovatorName(generateString(101)); // must be <=100
	    	productVO.setFloristReferenceNumber(generateString(65)); //must be <= 62
	    	productVO.setCategory(generateString(15));  // no check for length
	    	productVO.setProductType(generateString(15)); // no check for length
	    	productVO.setProductSubType(generateString(15)); //no check for length
	    	productVO.setLongDescription(generateString(1025)); // must be <= 1024
	    	productVO.setDeliveryType(generateString(10));
	    	productVO.setRecipe(generateString(256)); // must be <= 255
	    	productVO.setDeluxeRecipe(generateString(256)); // must be <= 255
	    	productVO.setPremiumRecipe(generateString(256)); // must be <= 255
	    	productVO.setDimWeight(generateString(15)); // must be <= 10
	    	productVO.setStandardPrice(-1);
	    	productVO.setDeluxePrice(-1);
	    	productVO.setPremiumPrice(-1);
	     	results =  bo.validateProductMassEdit(conn,productVO);
	    	System.out.println("General Test 3\t18822\tLength Test all fields tested together.\tExpect:\tActual: " + results);
	      }
	    catch(Exception e) {
	    	e.printStackTrace();
	    }
	    
  }  
  
    /*
	 *   Test Floral Product
	 */
    public void testFloralValidateProductMassEdits() {
    	String[] searchItems = {"7044", "", "", "", ""};
    	String productType = "FLORAL";
    	validateProduct(searchItems, productType);
    }
    
    /*
	 *   Test FreshCut Product
	 */
    public void testFreshCutValidateProductMassEdits() {
    	String[] searchItems = {"FFDO", "", "", "", ""};
    	String productType = "FRECUT";
    	validateProduct(searchItems, productType);
    }
    
    /*
	 *   Test Specialty Gifts Product
	 */
    public void testSpecialtyGiftsValidateProductMassEdits() {
    	String[] searchItems = {"2010", "", "", "", ""};
    	String productType = "SPEGFT";
    	validateProduct(searchItems, productType);
    }
    
    /*
	 *   Test SDG Product
	 */
    public void testSDGValidateProductMassEdits() {
    	String[] searchItems = {"6050", "", "", "", ""};
    	String productType = "SDG";
    	validateProduct(searchItems, productType);
    }
    
    /*
	 *   Test SDFC Product
	 */
    public void testSDFCValidateProductMassEdits() {
    	String[] searchItems = {"F038", "", "", "", ""};
    	String productType = "SDFC";
    	validateProduct(searchItems, productType);
    }
    
    /*
	 *   Test SERVICES Product
	 */
    public void testServicesValidateProductMassEdits() {
    	String[] searchItems = {"GM03", "", "", "", ""};
    	String productType = "SERVICES";
    	validateProduct(searchItems, productType);
    }
    
    /*
	 *   Test MasterSKU Product
	 */
    public void testMasterSKUValidateProductMassEdits() {
    	String[] searchItems = {"00BC", "", "", "", ""};
    	validateMasterSku(searchItems);
    }
    
	
   public void validateMasterSku(String[] searchItems) {
	  try {
	 	   	// get the product list but convert the descriptions back to db codes, since
	 	   	// this is done before the validateProductMassEdits() method
	 	   	List<ProductVO> productList = 
	 	   		bo.convertDescriptionsToDbCodes(bo.getProductMassEditList(conn, "M", searchItems)); 
	 	   	System.out.println("\n\n******* BEGIN VALIDATE MASTER SKU for MASTER SKU: " + searchItems[0] + " *******");
	 	   	for (ProductVO vo: productList) {
	 	   		String[] products = new String[5];
	 	   		products[0] = vo.getProductId();
	 	   		validateProduct(products, vo.getProductType());
	 	   		
	 	   	}
	  }
	  catch(Exception e) {
		  e.printStackTrace();
	  }
	 	
   }
   
   /*
	 *   Test Specialty Gifts Product - test for new vendor product availability error
	 */
   public void testSpecialtyGiftsValidateProductMassEdits2() {
   	String[] searchItems = {"JN81", "", "", "", ""};
   	String productType = "SPEGFT";
   	validateProduct(searchItems, productType);
   }
   
   /*
	 *   Test Specialty Floral/Dropship product for SKU-IT error
	 *   
	  */
  public void testFloralDropshipSKUIT() {
  	String[] searchItems = {"F103", "", "", "", ""};
  	String productType = "SDFC";
  	validateProduct(searchItems, productType);
  }
    
   /**
   * 
   * @param searchItems Product Ids or Master Skus
   * @param typeOfSearch P = Product M = Master SKU
   * @param productType  
   */
	public void validateProduct(String[] searchItems, String productType) {
	  try {
	   	// get the product list but convert the descriptions back to db codes, since
	   	// this is done before the validateProductMassEdits() method
	   	List<ProductVO> productList = 
	   		bo.convertDescriptionsToDbCodes(bo.getProductMassEditList(conn, "P", searchItems)); 
	   	ProductVO vo = productList.get(0);
	   	String deliveryType = bo.getSpreadSheetDeliveryType(vo.isShipMethodFlorist(), vo.isShipMethodCarrier());     
			System.out.println("\n****** BEGIN " + productType + " PRODUCT TESTS *******");
			System.out
					.println("Product:" + vo.getProductId() + " Product Name: "
							+ vo.getProductName() + " Category="
							+ vo.getCategory() + " Product Type="
							+ vo.getProductType() + " Delivery Type=" 
							+ deliveryType + " Available=" + vo.isStatus());
			// Test 1 - validate data loaded from db
			String results = bo.validateProductMassEdit(conn,vo);
			System.out.println(productType + " Test 1\t18822\tValidate db values downloaded with no changes.\tExpect:\tActual nothing error string is empty. "
							+ results); // expect nothing - no errors

			// Floral test 2 - Product ID
			productList = bo.convertDescriptionsToDbCodes(bo
					.getProductMassEditList(conn, "P", searchItems));
			vo = productList.get(0);
			vo.setProductId(null);
			results = bo.validateProductMassEdit(conn,vo);
			System.out.println(productType + " Test 2.1\t18822\tProduct Id is null.\tExpect:\tActual: "
					+ results); // expect error
			vo.setProductId(" ");
			results = bo.validateProductMassEdit(conn,vo);
			System.out.println(productType + " Test 2.2\t18822\tProduct Id is ' '\tExpect:\tActual: " + results); // expect
																				// error
			vo.setProductId(generateString(11));
			results = bo.validateProductMassEdit(conn,vo);
			System.out.println(productType + " Test 2.3\t18822\tProduct Id is set to 11 X's\tExpect:\tActual: "
					+ results); // expect error
			vo.setProductId("BARB");
			results = bo.validateProductMassEdit(conn,vo);
			System.out.println(productType + " Test 2.4\t18822\tProduct Id is set to 'BARB'.\tExpect:\tActual: "
					+ results); // expect error
			vo.setProductId("111111111111111111111");
			results = bo.validateProductMassEdit(conn,vo);
			System.out.println(productType + " Test 2.5\t18822\tProduct Id is set to '111111111111111111111'\tExpect:\tActual: "
					+ results); // expect error

			// Floral test 3 - Product Name
			productList = bo.convertDescriptionsToDbCodes(bo
					.getProductMassEditList(conn, "P", searchItems));
			vo = productList.get(0);
			vo.setProductName(null);
			results = bo.validateProductMassEdit(conn,vo);
			System.out.println(productType + " Test 3.1\t18822\tProduct Name is null\tExpect:\tActual : "
					+ bo.validateProductMassEdit(conn,vo)); // expect error
			vo.setProductName(" ");
			results = bo.validateProductMassEdit(conn,vo);
			System.out.println(productType + " Test 3.2\t18822\tProduct Name is ' '\tExpect:\tActual: "
					+ bo.validateProductMassEdit(conn,vo)); // expect error
			vo.setProductName(generateString(26));
			results = bo.validateProductMassEdit(conn,vo);
			System.out
					.println(productType + " Test 3.3\t18822\tProduct name is set to 26 X's\tExpect:\tActual: "
							+ bo.validateProductMassEdit(conn,vo)); // expect error

			// Floral test 4 - Novator ID
			productList = bo.convertDescriptionsToDbCodes(bo
					.getProductMassEditList(conn, "P", searchItems));
			vo = productList.get(0);
			vo.setNovatorId(null);
			results = bo.validateProductMassEdit(conn,vo);
			System.out.println(productType + " Test 4.1\t18822\tNovator ID is null\tExpect:\tActual: "
					+ results); // expect error
			vo.setNovatorId(" ");
			results = bo.validateProductMassEdit(conn,vo);
			System.out.println(productType + " Test 4.2\t18822\tNovator ID is ' '\tExpect:\tActual: "
					+ results); // expect error
			vo.setNovatorId(generateString(15));
			results = bo.validateProductMassEdit(conn,vo);
			System.out
					.println(productType + " Test 4.3\t18822\tNovator ID is set to 15 X's: "
							+ results); // expect error
			vo.setNovatorId("ABCD@EFG");
			results = bo.validateProductMassEdit(conn,vo);
			System.out
					.println(productType + " Test 4.4\t18822\tNovator ID contains ABCD@EFG\tExpect:\tActual: "
							+ results); // expect error
			vo.setNovatorId("ABCD EFG");
			results = bo.validateProductMassEdit(conn,vo);
			System.out
					.println(productType + " Test 4.5\t18822\tNovator ID contains ABCD EFG\tExpect:\tActual: "
							+ results); // ok
			vo.setNovatorId("ABCD-EFG");
			results = bo.validateProductMassEdit(conn,vo);
			System.out
					.println(productType + " Test 4.6\t18822\tNovator ID contains ABCD-EFG\tExpect:\tActual: "
							+ results); // ok

			// Floral test 5 - Novator Name
			productList = bo.convertDescriptionsToDbCodes(bo
					.getProductMassEditList(conn, "P", searchItems));
			vo = productList.get(0);
			vo.setNovatorName(null);
			results = bo.validateProductMassEdit(conn,vo);
			System.out.println(productType + " Test 5.1\t18822\tNovator Name is null\tExpect:\tActual: "
					+ results); // expect error
			vo.setNovatorName(" ");
			results = bo.validateProductMassEdit(conn,vo);
			System.out.println(productType + " Test 5.2\t18822\tNovator Name is ' '\tExpect:\tActual: "
					+ results); // expect error
			vo.setNovatorName(generateString(101));
			results = bo.validateProductMassEdit(conn,vo);
			System.out
					.println(productType + " Test 5.3\t18822\tNovator Name is set to 101 X's\tExpect:\tActual: "
							+ results); // expect error

			// Floral test 6 - Florist Reference Number
			productList = bo.convertDescriptionsToDbCodes(bo
					.getProductMassEditList(conn, "P", searchItems));
			vo = productList.get(0);
			vo.setFloristReferenceNumber(null);
			results = bo.validateProductMassEdit(conn,vo);
			System.out
					.println(productType + " Test 6.1\t18822\tFlorist Reference Number is null\tExpect:\tActual: "
							+ results); // expect error
			vo.setFloristReferenceNumber(" ");
			results = bo.validateProductMassEdit(conn,vo);
			System.out
					.println(productType + " Test 6.2\t18822\tFlorist Reference Number is ' '\tExpect:\tActual: "
							+ results); // expect error
			vo.setFloristReferenceNumber(generateString(63));
			results = bo.validateProductMassEdit(conn,vo);
			System.out
					.println(productType + " Test 6.3\t18822\tFlorist Reference Number is set to 63 X's\tExpect:\tActual: "
							+ results); // expect error
			vo.setFloristReferenceNumber("ABCD@EFG");
			results = bo.validateProductMassEdit(conn,vo);
			System.out
					.println(productType + " Test 6.4\t18822\tFlorist Reference Number is set to ABCD@EFG\tExpect:\tActual: "
							+ results); // expect error
			vo.setFloristReferenceNumber("ABCD EFG");
			results = bo.validateProductMassEdit(conn,vo);
			System.out
					.println(productType + " Test 6.5\t18822\tFlorist Reference Number is set to ABCD EFG\tExpect:\tActual: "
							+ results); // expect error
			vo.setFloristReferenceNumber("ABCD-EFG");
			results = bo.validateProductMassEdit(conn,vo);
			System.out
					.println(productType + " Test 6.6\t18822\tFlorist Reference Number is set to ABCD-EFG\tExpect:\tActual: "
							+ results); // expect error

			// Floral test 7 - Category
			productList = bo.convertDescriptionsToDbCodes(bo
					.getProductMassEditList(conn, "P", searchItems));
			vo = productList.get(0);
			vo.setCategory(null);
			results = bo.validateProductMassEdit(conn,vo);
			System.out
					.println(productType + " Test 7.1\t18822\tCategory is null\tExpect:\tActual: " + results); // expect
																				// error
			vo.setCategory(" ");
			results = bo.validateProductMassEdit(conn,vo);
			System.out.println(productType + " Test 7.2\t18822\tCategory is ' '\tExpect:\tActual: " + results); // expect
																					// error
			vo.setCategory(generateString(25));
			results = bo.validateProductMassEdit(conn,vo);
			System.out.println(productType + " Test 7.3\t18822\tCategory is set to 25 X's\tExpect:\tActual: "
					+ results); // expect error

			// Floral test 8 - Product Type
			productList = bo.convertDescriptionsToDbCodes(bo
					.getProductMassEditList(conn, "P", searchItems));
			vo = productList.get(0);
			vo.setProductType(null);
			results = bo.validateProductMassEdit(conn,vo);
			System.out
					.println(productType + " Test 8.1\t18822\tProduct Type is null Sub-Type is "
							+ vo.getProductSubType() + "\tExpect:\tActual: " + results); // expect
																		// error
			vo.setProductType(" ");
			results = bo.validateProductMassEdit(conn,vo);
			System.out
					.println(productType + " Test 8.2\t18822\tProduct Type is ' ' Sub-Type is "
							+ vo.getProductSubType() + "\tExpect:\tActual: " + results); // expect
																		// error
			vo.setProductType(generateString(25));
			results = bo.validateProductMassEdit(conn,vo);
			System.out
					.println(productType + " Test 8.3\t18822\tProduct Type is set to 25 X's Sub-Type is "
							+ vo.getProductSubType() + "\tExpect:\tActual: " + results); // expect
																		// error

			// Floral test 9 - Sub Type
			productList = bo.convertDescriptionsToDbCodes(bo
					.getProductMassEditList(conn, "P", searchItems));
			vo = productList.get(0);
			vo.setProductSubType(null);
			results = bo.validateProductMassEdit(conn,vo);
			System.out.println(productType + " Test 9.1\t18822\tProduct Type is "
					+ vo.getProductType() + " Sub Type is null\tExpect:\tActual: " + results); // expect
																				// error
			vo.setProductSubType(" ");
			results = bo.validateProductMassEdit(conn,vo);
			System.out.println(productType + " Test 9.2\t18822\tProduct Type is "
					+ vo.getProductType() + " Sub Type is ' '\tExpect:\tActual: " + results); // expect
																				// error
			vo.setProductSubType(generateString(25));
			results = bo.validateProductMassEdit(conn,vo);
			System.out.println(productType + " Test 9.3\t18822\tProduct Type is "
					+ vo.getProductType() + " Sub Type is set to 25 X's\tExpect:\tActual: "
					+ results); // expect error

			// Floral test 10 - Description
			productList = bo.convertDescriptionsToDbCodes(bo
					.getProductMassEditList(conn, "P", searchItems));
			vo = productList.get(0);
			vo.setLongDescription(null);
			results = bo.validateProductMassEdit(conn,vo);
			System.out.println(productType + " Test 10.1\t18822\tDescription is null\tExpect:\tActual: "
					+ results); // expect error
			vo.setLongDescription(" ");
			results = bo.validateProductMassEdit(conn,vo);
			System.out.println(productType + " Test 10.2\t18822\tDescription is ' '\tExpect:\tActual: "
					+ results); // expect error
			vo.setLongDescription(generateString(1025));
			results = bo.validateProductMassEdit(conn,vo);
			System.out
					.println(productType + " Test 10.3\t18822\tDescription is set to 1025 X's\tExpect:\tActual: "
							+ results); // expect error

			// Floral test 11 - Availability
			productList = bo.convertDescriptionsToDbCodes(bo
					.getProductMassEditList(conn, "P", searchItems));
			vo = productList.get(0);
			vo.setMassEditProductStatus(null);
			results = bo.validateProductMassEdit(conn,vo);
			System.out.println(productType + " Test 11.1\t18822\tAvailability is null\tExpect:\tActual: "
					+ results); // expect error
			vo.setMassEditProductStatus(" ");
			results = bo.validateProductMassEdit(conn,vo);
			System.out.println(productType + " Test 11.2\t18822\tAvailability is ' '\tExpect:\tActual: "
					+ results); // expect error
			vo.setMassEditProductStatus(generateString(10));
			results = bo.validateProductMassEdit(conn,vo);
			System.out
					.println(productType + " Test 11.3\t18822\tAvailability is set to 10 X's\tExpect:\tActual: "
							+ results); // expect error

			// Floral test 12 - Standard Recipe
			productList = bo.convertDescriptionsToDbCodes(bo
					.getProductMassEditList(conn, "P", searchItems));
			vo = productList.get(0);
			vo.setRecipe(null);
			results = bo.validateProductMassEdit(conn,vo);
			System.out.println(productType + " Test 12.1\t18822\tRecipe is null\tExpect:\tActual: " + results); // expect
																					// error
			vo.setRecipe(" ");
			results = bo.validateProductMassEdit(conn,vo);
			System.out.println(productType + " Test 12.2\t18822\tRecipe is ' '\tExpect:\tActual: " + results); // expect
																				// error
			vo.setRecipe(generateString(256));
			results = bo.validateProductMassEdit(conn,vo);
			System.out.println(productType + " Test 12.3\t18822\tRecipe is set to 256 X's\tExpect:\tActual: "
					+ results); // expect error

			// Floral test 13 - Deluxe Recipe
			productList = bo.convertDescriptionsToDbCodes(bo
					.getProductMassEditList(conn, "P", searchItems));
			vo = productList.get(0);
			vo.setDeluxeRecipe(null);
			results = bo.validateProductMassEdit(conn,vo);
			System.out.println(productType + " Test 13.1\t18822\tDeluxe Recipe is null\tExpect:\tActual: "
					+ results); // expect error
			vo.setDeluxeRecipe(" ");
			results = bo.validateProductMassEdit(conn,vo);
			System.out.println(productType + " Test 13.2\t18822\tDeluxe Recipe is ' '\tExpect:\tActual: "
					+ results); // expect error
			vo.setDeluxeRecipe(generateString(256));
			results = bo.validateProductMassEdit(conn,vo);
			System.out
					.println(productType + " Test 13.3\t18822\tDeluxe Recipe is set to 256 X's\tExpect:\tActual:\tActual: "
							+ results); // expect error

			// Floral test 14 - Premium Recipe
			productList = bo.convertDescriptionsToDbCodes(bo
					.getProductMassEditList(conn, "P", searchItems));
			vo = productList.get(0);
			vo.setPremiumRecipe(null);
			results = bo.validateProductMassEdit(conn,vo);
			System.out.println(productType + " Test 14.1\t18822\tPremium Recipe is null\tExpect:\tActual: "
					+ results); // expect error
			vo.setPremiumRecipe(" ");
			results = bo.validateProductMassEdit(conn,vo);
			System.out.println(productType + " Test 14.2\t18822\tPremium Recipe is ' '\tExpect:\tActual: "
					+ results); // expect error
			vo.setPremiumRecipe(generateString(256));
			results = bo.validateProductMassEdit(conn,vo);
			System.out
					.println(productType + " Test 14.3\t18822\tPremium Recipe is set to 256 X's\tExpect:\tActual: "
							+ results); // expect error

			// Floral test 15 - Dim/Weight
			productList = bo.convertDescriptionsToDbCodes(bo
					.getProductMassEditList(conn, "P", searchItems));
			vo = productList.get(0);
			vo.setDimWeight(null);
			results = bo.validateProductMassEdit(conn,vo);
			System.out.println(productType + " Test 15.1\t18822\tDim/Weight is null\tExpect:\tActual: "
					+ results); // expect error
			vo.setDimWeight(" ");
			results = bo.validateProductMassEdit(conn,vo);
			System.out.println(productType + " Test 15.2\t18822\tDim/Weight is ' '\tExpect:\tActual: "
					+ results); // expect error
			vo.setDimWeight(generateString(25));
			results = bo.validateProductMassEdit(conn,vo);
			System.out
					.println(productType + " Test 15.3\t18822\tDim/Weight is set to 25 X's\tExpect:\tActual: "
							+ results); // expect error
			vo.setDimWeight("ABCDE");
			results = bo.validateProductMassEdit(conn,vo);
			System.out
					.println(productType + " Test 15.4\t18822\tDim/Weight is set to ABCDE\tExpect:\tActual: "
							+ results); // expect error
			vo.setDimWeight("-1");
			results = bo.validateProductMassEdit(conn,vo);
			System.out.println(productType + " Test 15.5\t18822\tDim/Weight is set to -1\tExpect:\tActual: "
					+ results); // expect error
			vo.setDimWeight("0");
			results = bo.validateProductMassEdit(conn,vo);
			System.out.println(productType + " Test 15.5\t18822\tDim/Weight is set to 0\tExpect:\tActual: "
					+ results); // expect error
			vo.setDimWeight("14.5");
			results = bo.validateProductMassEdit(conn,vo);
			System.out.println(productType + " Test 15.7\t18822\tDim/Weight is set to 14.5\tExpect\tActual: "
					+ results); // expect error

			// Floral test 16 - Standard Price
			productList = bo.convertDescriptionsToDbCodes(bo
					.getProductMassEditList(conn, "P", searchItems));
			vo = productList.get(0);
			vo.setStandardPrice(-1);
			results = bo.validateProductMassEdit(conn,vo);
			System.out
					.println(productType + " Test 16.1\t18822\tStandard Price is set to -1\tExpect:\tActual: "
							+ results); // expect error
			vo.setStandardPrice(0F);
			results = bo.validateProductMassEdit(conn,vo);
			System.out
					.println(productType + " Test 16.2\t18822\tStandard Price is set to 0\tExpect:\tActual: "
							+ results); // expect error
			vo.setStandardPrice(10000F);
			results = bo.validateProductMassEdit(conn,vo);
			System.out
					.println(productType + " Test 16.3\t18822\tStandard Price is set to 10000\tExpect:\tActual: "
							+ results); // expect error
			vo.setStandardPrice(.01F);
			results = bo.validateProductMassEdit(conn,vo);
			System.out
					.println(productType + " Test 16.4\t18822\tStandard Price is set to .01\tExpect:\tActual: "
							+ results); // expect error
			vo.setStandardPrice(9999.99F);
			results = bo.validateProductMassEdit(conn,vo);
			System.out
					.println(productType + " Test 16.5\t18822\tStandard Price is set to 9999.99\tExpect:\tActual: "
							+ results); // expect error

			// Floral test 17 - Deluxe Price
			productList = bo.convertDescriptionsToDbCodes(bo
					.getProductMassEditList(conn, "P", searchItems));
			vo = productList.get(0);
			vo.setDeluxePrice(-1);
			results = bo.validateProductMassEdit(conn,vo);
			System.out.println(productType + " Test 17.1\t18822\tDeluxe Price is set to -1\tExpect:\tActual: "
					+ results); // expect error
			vo.setDeluxePrice(0F);
			results = bo.validateProductMassEdit(conn,vo);
			System.out.println(productType + " Test 17.2\t18822\tDeluxe Price is set to 0\tExpect:\tActual: "
					+ results); // expect error
			vo.setDeluxePrice(10000F);
			results = bo.validateProductMassEdit(conn,vo);
			System.out
					.println(productType + " Test 17.3\t18822\tDeluxe Price is set to 10000\tExpect:\tActual: "
							+ results); // expect error
			vo.setDeluxePrice(.01F);
			results = bo.validateProductMassEdit(conn,vo);
			System.out
					.println(productType + " Test 17.4\t18822\tDeluxe Price is set to .01\tExpect:\tActual: "
							+ results); // expect error
			vo.setDeluxePrice(9999.99F);
			results = bo.validateProductMassEdit(conn,vo);
			System.out
					.println(productType + " Test 17.5\t18822\tDeluxe Price is set to 9999.99\tExpect:\tActual: "
							+ results); // expect error

			// Floral test 18 - Premium Price
			productList = bo.convertDescriptionsToDbCodes(bo
					.getProductMassEditList(conn, "P", searchItems));
			vo = productList.get(0);
			vo.setPremiumPrice(-1);
			results = bo.validateProductMassEdit(conn,vo);
			System.out
					.println(productType + " Test 18.1\t18822\tPremium Price is set to -1\tExpect\tActual: "
							+ results); // expect error
			vo.setPremiumPrice(0F);
			results = bo.validateProductMassEdit(conn,vo);
			System.out.println(productType + " Test 18.2\t18822\tPremium Price is set to 0\tExpect:\tActual: "
					+ results); // expect error
			vo.setPremiumPrice(10000F);
			results = bo.validateProductMassEdit(conn,vo);
			System.out
					.println(productType + " Test 18.3\t18822\tPremium Price is set to 10000\tExpect\tActual: "
							+ results); // expect error
			vo.setPremiumPrice(.01F);
			results = bo.validateProductMassEdit(conn,vo);
			System.out
					.println(productType + " Test 18.4\t18822\tPremium Price is set to .01\tExpect\tActual: "
							+ results); // expect error
			vo.setPremiumPrice(9999.99F);
			results = bo.validateProductMassEdit(conn,vo);
			System.out
					.println(productType + " Test 18.5\t18822\tPremium Price is set to 9999.99\tExpect\tActual: "
							+ results); // expect error
			
			// Status change test
			productList = bo.convertDescriptionsToDbCodes(bo
					.getProductMassEditList(conn, "P", searchItems));
			vo = productList.get(0);
			vo.setStatus(true);
			vo.setMassEditProductStatus("Available");
			results = bo.validateProductMassEdit(conn,vo);
			System.out
					.println(productType + " Test 18.6\t18822\tStatus is true (A)\tExpect\tActual: "
							+ results); // expect depends on whether dropship or not
		

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	    * Write the test case method in the fixture class. Be sure to make it public,
	    * or it can't be invoked through reflection
	    **/
	  public void testGetVendorsAvailableForProduct(){
	       // Call the BO to get the count of vendors available for the product
	        try
	        {
	        	System.out.println("\n******* Start testGetVendorsAvailableForProduct  *******");
	        	BigDecimal vendorCount = 
					bo.getVendorsAvailableForProduct(conn,null); 
	            System.out.println("Product id: null\tCount=" + vendorCount);
	            vendorCount = bo.getVendorsAvailableForProduct(conn," "); 
	            System.out.println("Product id: ABCD\tCount=" + vendorCount);
	        	vendorCount = bo.getVendorsAvailableForProduct(conn,"BB44"); 
	            System.out.println("Product id: BB44\tCount=" + vendorCount);
	            vendorCount = bo.getVendorsAvailableForProduct(conn,"0PB1"); 
	            System.out.println("Product id: 0PB1\tCount=" + vendorCount);
	        }
	        catch(PDBApplicationException PDBApplicationException)
	        {
	            PDBApplicationException.printStackTrace();
	        }
	        catch(PDBSystemException PDBSystemException)
	        {
	            PDBSystemException.printStackTrace();
	        }
	        catch (Exception e) {
	        	e.printStackTrace();
	        }
	  }

	private String generateString(int length) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < length; i++) {
			sb.append("X");
		}
		return sb.toString();
	}

	/**
	 * To run several tests at once JUnit provides an object, TestSuite which
	 * runs any number of test cases together. Each test is run as a separate
	 * instance of the test class You make your suite accessible to a TestRunner
	 * tool with a static method suite that returns a test suite.
	 **/
	public static Test suite() {
		TestSuite suite = new TestSuite();
		// Create an instance of of Test class that will run this test case
		// When the test is run, the name of the test is used to look up the
		// method
		// to run, using reflection
		suite.addTest(new PDBMassEditTest("testGetPDBMassEditList"));
		suite.addTest(new PDBMassEditTest("testGeneralValidateProductMassEdits"));
		suite.addTest(new PDBMassEditTest("testFloralValidateProductMassEdits"));
		suite.addTest(new PDBMassEditTest("testFreshCutValidateProductMassEdits"));
		//suite.addTest(new PDBMassEditTest("testSpecialtyGiftsValidateProductMassEdits"));
		//suite.addTest(new PDBMassEditTest("testSDGProductMassEdits"));
		suite.addTest(new PDBMassEditTest("testSDFCValidateProductMassEdits"));
		//suite.addTest(new PDBMassEditTest("testServicesValidateProductMassEdits"));
		//suite.addTest(new PDBMassEditTest("testMasterSKUValidateProductMassEdits"));
		suite.addTest(new PDBMassEditTest("testGetVendorsAvailableForProduct"));
		suite.addTest(new PDBMassEditTest("testSpecialtyGiftsValidateProductMassEdits2"));
		suite.addTest(new PDBMassEditTest("testFloralDropshipSKUIT"));
		return suite;
	}
	
	 

	/**
	 * You can either user the text ui or swing ui to run the test
	 **/
	public static void main(String[] args) {
		// pass in the class of the test that needs to be run
		// junit.swingui.TestRunner.run(ResourceManagerTest.class);
		junit.textui.TestRunner.run(suite());
	}

}
