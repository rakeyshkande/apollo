package com.ftd.pdb.tests;

import com.ftd.pdb.businessobject.ProductBOImpl;
import com.ftd.pdb.common.resources.IResourceProvider;
import com.ftd.pdb.common.resources.TestResourceProviderImpl;

import com.ftd.pdb.integration.dao.ProductDAOImpl;
import com.ftd.pdb.service.ShippingKeySVCImpl;

import java.sql.Connection;

import java.util.List;

import junit.framework.TestCase;

import junit.framework.TestSuite;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class PDBUtilitiesTest extends TestCase {
    Connection conn;
    ApplicationContext context;
    IResourceProvider resourceProvider = new TestResourceProviderImpl();
    
    public PDBUtilitiesTest(String name){
      super(name);
  }

  /**
   * Override setup() to initialize variables
   **/
  protected void setUp() throws Exception {
        //Start up spring framework
        context = new FileSystemXmlApplicationContext("C:\\_RLSE_2.10\\FTD\\product_database\\web\\WEB-INF\\spring-pdb-servlet.xml");
        this.conn = resourceProvider.getDatabaseConnection();
  }

  /**
   * Override tearDown() to release any permanent resources you allocated in setUp
   */
   public void tearDown() throws Exception {
        if( conn!=null && !conn.isClosed() ) {
            conn.close();    
        }
   }
    
    public static TestSuite suite()
    { 
        TestSuite suite = null;
        
        try 
        {
            suite = new TestSuite(PDBUtilitiesTest.class);
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        
        return suite;
    }
    
    public void testGetExProductsDAO() {
        try {
            ProductDAOImpl productDAO = (ProductDAOImpl)context.getBean("productDAO");
            
            List<String> list = productDAO.getProductExceptionsToProcess(conn);
            
            for( int idx = 0; idx < list.size(); idx++ ) {
                String productId = list.get(idx);
                System.out.println(productId);
            }
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }
    
    public void testGetExProductsBO() {
        try {
            ProductBOImpl productBO = (ProductBOImpl)context.getBean("productBO");
            
            productBO.updateExceptionProducts(conn);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    public static void main(String[] args) {
//        junit.textui.TestRunner.run(suite());

        PDBUtilitiesTest test = new PDBUtilitiesTest("testGetExProductsBO");
        try {
            test.runBare();
            
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
