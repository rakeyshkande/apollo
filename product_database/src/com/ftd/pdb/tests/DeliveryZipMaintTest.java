package com.ftd.pdb.tests;

import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.pdb.integration.dao.DeliveryZipDAOImpl;

import junit.framework.TestSuite;

import org.w3c.dom.Document;


public class DeliveryZipMaintTest extends PDBTestCase {
    public DeliveryZipMaintTest(String testCase) {
        super(testCase);
    }

    public static TestSuite suite() {
        TestSuite suite = null;

        try {
            suite = new TestSuite(DeliveryZipMaintTest.class);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return suite;
    }

    public static void main(String[] args) {
        DeliveryZipMaintTest testCase = new DeliveryZipMaintTest("testGetBlockedZips");
        try {
            testCase.setUp();
            testCase.testGetCarriers();
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }
    
    public void testGetCarriers() {
        try {
            DeliveryZipDAOImpl deliveryDAO = (DeliveryZipDAOImpl)context.getBean("deliveryDAO");
            
            Document doc = deliveryDAO.getCarriersXML(conn);
            
            System.out.println(JAXPUtil.toString(doc));
            
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }
    
    public void testGetBlockedZips() {
        try {
            DeliveryZipDAOImpl deliveryDAO = (DeliveryZipDAOImpl)context.getBean("deliveryDAO");
            
            Document doc = deliveryDAO.getBlockedZips(conn,"9781");
            
            System.out.println(JAXPUtil.toString(doc));
            
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }
}
