package com.ftd.pdb.tests;

import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBSystemException;
import com.ftd.pdb.common.resources.IResourceProvider;
import com.ftd.pdb.common.resources.TestResourceProviderImpl;
import com.ftd.pdb.common.valobjs.ProductVO;
import com.ftd.pdb.common.valobjs.ShippingKeyPriceVO;
import com.ftd.pdb.common.valobjs.ShippingKeyVO;
import com.ftd.pdb.common.valobjs.ShippingMethodVO;
import com.ftd.pdb.service.ProductMaintSVCImpl;
import com.ftd.pdb.service.ShippingKeySVCImpl;
import com.ftd.pdb.web.ProductMaintDetailForm;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;


/**
 * The test class will extend junit.framework.TestCase
 **/
public class ProductMaintTest extends TestCase  {
    Connection conn;
    ApplicationContext context;
    IResourceProvider resourceProvider = new TestResourceProviderImpl();

  /**
   * Create a constructor that take a String parameter and passes it
   * to the super class
   **/
  public ProductMaintTest(String name) {
      super(name);
  }

  /**
   * Override setup() to initialize variables
   **/
  protected void setUp() throws Exception{
        //Start up spring framework
        context = new FileSystemXmlApplicationContext("C:\\_RLSE_2.10\\FTD\\product_database\\web\\WEB-INF\\spring-pdb-servlet.xml");
        this.conn = resourceProvider.getDatabaseConnection();
  }

  /**
   * Override tearDown() to release any permanent resources you allocated in setUp
   */
   public void tearDown(){
   }

   /**
    * Write the test case method in the fixture class. Be sure to make it public,
    * or it can't be invoked through reflection
    **/
  public void testProductMaintList(){
    try
    {
        List products = null;
        
        // Call the Service layer to get the product list
        try
        {
            ProductMaintSVCImpl service = (ProductMaintSVCImpl)context.getBean("productSVC");
            products = service.getProductList();

            for(int i = 0; i < products.size(); i++)
            {   
                System.out.println("Product ID = " + ((ProductVO)products.get(i)).getProductId());
                System.out.println("Novator Name = " + ((ProductVO)products.get(i)).getNovatorName());
            }
        }
        catch(PDBApplicationException PDBApplicationException)
        {
            PDBApplicationException.printStackTrace();
        }
        catch(PDBSystemException PDBSystemException)
        {
            PDBSystemException.printStackTrace();
        }
    }
    catch (Exception e)
    {
        e.printStackTrace();
    }
  }

   /**
    * Write the test case method in the fixture class. Be sure to make it public,
    * or it can't be invoked through reflection
    **/
  public void testGetProduct(){
    try
    {

        ProductVO product = null;
        
        // Call the Service layer to get the product 
        try
        {
            ProductMaintSVCImpl service = (ProductMaintSVCImpl)context.getBean("productSVC");
//            product = service.getProduct("JMP1");
//            product = service.getProduct("P236");
            product = service.getProduct("JMP1");
            List envKeys = new ArrayList();
            envKeys.add(com.ftd.osp.utilities.feed.NovatorFeedUtil.TEST);
            service.setProduct(product, envKeys);

//            product = service.getProductByNovatorId("JMPN");

            ProductMaintDetailForm form = new ProductMaintDetailForm();

            String[] dummyArray = new String[0];
            String[] list = product.getRecipientSearch();
            Object[] recipientSearchArray = list;
//            form.setRecipientSearchArray((String[])recipientSearchArray);    
            
 //           PropertyUtils.copyProperties(form, product);
        }
        catch(PDBApplicationException PDBApplicationException)
        {
            PDBApplicationException.printStackTrace();
        }
        catch(PDBSystemException PDBSystemException)
        {
            PDBSystemException.printStackTrace();
        }
    }
    catch (Exception e)
    {
		e.printStackTrace();
    }
  }

   /**
    * Write the test case method in the fixture class. Be sure to make it public,
    * or it can't be invoked through reflection
    **/
  public void testUpdateProduct(){
    try
    {
        ProductVO product = new ProductVO();
        
        // Call the Service layer to update the product 
        try
        {
/*            product.setProductId("JMP1");
            product.setNovatorId("JMPN1");
            product.setProductName("Jeff Product 7");
            product.setNovatorName("Jeff Novator 7");
            product.setStatus(true);
            product.setDeliveryType("D");
            product.setCategory("GEN");
            product.setProductType("SPEGFT");
            product.setProductSubType("BEARS");
            product.setKeywordSearch("JMP Jeff");
            List stateList = new ArrayList();
            stateList.add("MI");
            stateList.add("IL");
            product.setExcludedDeliveryStates(stateList);
            List recipientList = new ArrayList();
            recipientList.add("RM");
            recipientList.add("RF");
            product.setRecipientSearch(recipientList);
            List methodList = new ArrayList();
            methodList.add("ND");
            methodList.add("2F");
            product.setShipMethods(methodList);
            product.setVendorBlockDateStart(new Date());
            product.setVendorBlockDateEnd(new Date());
*/
            ProductMaintSVCImpl service = (ProductMaintSVCImpl)context.getBean("productSVC");
            List envKeys = new ArrayList();
            envKeys.add(com.ftd.osp.utilities.feed.NovatorFeedUtil.TEST);
/*            
            product.setShipDayMonday(true);
            product.setShipDayTuesday(true);
            product.setShipDayWednesday(true);
            product.setShipDayThursday(true);
            product.setShipDayFriday(true);
            product.setShipDaySaturday(false);
            product.setShipDaySunday(false);
            List subCodeList = new ArrayList();
            ProductSubCodeVO vo = new ProductSubCodeVO();
            vo.setProductId(product.getProductId());
            vo.setProductSubCodeId("TEST1");
            vo.setSubCodeDescription("Subcode 1");
            vo.setSubCodePrice(12.99F);
            subCodeList.add(vo);
            vo = new ProductSubCodeVO();
            vo.setProductId(product.getProductId());
            vo.setProductSubCodeId("TEST2");
            vo.setSubCodeDescription("Subcode 2");
            vo.setSubCodePrice(15.99F);
            subCodeList.add(vo);
            product.setSubCodeList(subCodeList);
*/
            product = service.getProduct("JMP1");
            service.setProduct(product, envKeys);

            service = null;
        }
        catch(PDBApplicationException PDBApplicationException)
        {
            PDBApplicationException.printStackTrace();
        }
        catch(PDBSystemException PDBSystemException)
        {
            PDBSystemException.printStackTrace();
        }
    }
    catch (Exception e)
    {
		e.printStackTrace();
    }
  }

   /**
    * Write the test case method in the fixture class. Be sure to make it public,
    * or it can't be invoked through reflection
    **/
  public void testGetShippingKeyDetails(){
    try
    {
        ShippingKeySVCImpl delegate = (ShippingKeySVCImpl)context.getBean("shippingKeySVC");

        List shippingKeys = delegate.getShippingKeyList();

        ShippingKeyVO keyVO = (ShippingKeyVO)shippingKeys.get(0);

        keyVO = delegate.getShippingKey(keyVO.getShippingKey_ID());

        System.out.println("Shipping Key ID = " + keyVO.getShippingKey_ID());
        System.out.println("Shipping Key Description = " + keyVO.getShippingKeyDescription());
        System.out.println("Shipping Key Shipper ID = " + keyVO.getShipperID());
        System.out.println("Shipping Key Price Category = " + keyVO.getPriceCategory());
        for (int i = 0; i < keyVO.getPrices().size(); i++) 
        {
            ShippingKeyPriceVO priceVO = (ShippingKeyPriceVO)keyVO.getPrices().get(i);
            System.out.println("Shipping Key Price Min = " + priceVO.getMinPrice());
            System.out.println("Shipping Key Price Max = " + priceVO.getMaxPrice());
            System.out.println("Shipping Key ID = " + priceVO.getShippingKeyID());
            System.out.println("Shipping Key Price ID = " + priceVO.getShippingKeyPriceID());

            for (int j = 0; j < priceVO.getShippingMethods().size(); j++) 
            {
                 ShippingMethodVO methodVO = (ShippingMethodVO)priceVO.getShippingMethods().get(j);
                 System.out.println("Shipping Method ID = " + methodVO.getId());
                 System.out.println("Shipping Method Cost = " + methodVO.getCost());
            }
        }
        
        System.out.println("End Get Shipping Key");
    }
    catch (Exception e)
    {
		e.printStackTrace();
    }
  }

   /**
    * Write the test case method in the fixture class. Be sure to make it public,
    * or it can't be invoked through reflection
    **/
  public void testInsertShippingKeyDetails(){
    try
    {
        ShippingKeySVCImpl delegate = (ShippingKeySVCImpl)context.getBean("shippingKeySVC");

        ShippingKeyVO shippingKeyVO = new ShippingKeyVO();

        shippingKeyVO.setShippingKey_ID("5");
        shippingKeyVO.setShipperID("UPS");
        shippingKeyVO.setShippingKeyDescription("My new key");
        shippingKeyVO.setPriceCategory("my price category");
        
        List prices = new ArrayList();
        ShippingKeyPriceVO shippingKeyPriceVO = new ShippingKeyPriceVO();
        shippingKeyPriceVO.setShippingKeyID("5");
        shippingKeyPriceVO.setShippingKeyPriceID("0");
        shippingKeyPriceVO.setMinPrice(new Double(100.00));
        shippingKeyPriceVO.setMaxPrice(new Double(500.00));

        List methods = new ArrayList();
        ShippingMethodVO shippingMethodVO = new ShippingMethodVO();
        shippingMethodVO.setId("2F");
        shippingMethodVO.setCost(250.00F);
        methods.add(shippingMethodVO);
        
        shippingKeyPriceVO.setShippingMethods(methods);

        prices.add(shippingKeyPriceVO);
        
        shippingKeyVO.setPrices(prices);
        
        delegate.insertShippingKey(shippingKeyVO, new HashMap());

        System.out.println("End Insert Shipping Key");
    }
    catch (Exception e)
    {
		e.printStackTrace();
    }
  }

   /**
    * Write the test case method in the fixture class. Be sure to make it public,
    * or it can't be invoked through reflection
    **/
  public void testUpdateShippingKeyDetails(){
    try
    {
        ShippingKeySVCImpl delegate = (ShippingKeySVCImpl)context.getBean("shippingKeySVC");

        ShippingKeyVO shippingKeyVO = new ShippingKeyVO();

        shippingKeyVO.setShippingKey_ID("4");
        shippingKeyVO.setShipperID("USPS");
        shippingKeyVO.setShippingKeyDescription("Shipping Key 4");
        shippingKeyVO.setPriceCategory("my price category");
        
        List prices = new ArrayList();
        ShippingKeyPriceVO shippingKeyPriceVO = new ShippingKeyPriceVO();
        shippingKeyPriceVO.setShippingKeyID("4");
        shippingKeyPriceVO.setShippingKeyPriceID("0");
        shippingKeyPriceVO.setMinPrice(new Double(100.00));
        shippingKeyPriceVO.setMaxPrice(new Double(300.00));

        List methods = new ArrayList();
        ShippingMethodVO shippingMethodVO = new ShippingMethodVO();
        shippingMethodVO.setId("2F");
        shippingMethodVO.setCost(250.00F);
        methods.add(shippingMethodVO);
        shippingMethodVO = new ShippingMethodVO();
        shippingMethodVO.setId("ND");
        shippingMethodVO.setCost(50.00F);
        methods.add(shippingMethodVO);

        shippingKeyPriceVO = new ShippingKeyPriceVO();
        shippingKeyPriceVO.setShippingKeyID("4");
        shippingKeyPriceVO.setShippingKeyPriceID("1");
        shippingKeyPriceVO.setMinPrice(new Double(1.00));
        shippingKeyPriceVO.setMaxPrice(new Double(5.50));

        shippingMethodVO = new ShippingMethodVO();
        shippingMethodVO.setId("2F");
        shippingMethodVO.setCost(1.25F);
        methods.add(shippingMethodVO);
        shippingMethodVO = new ShippingMethodVO();
        shippingMethodVO.setId("ND");
        shippingMethodVO.setCost(0.99F);
        methods.add(shippingMethodVO);
        
        
        shippingKeyPriceVO.setShippingMethods(methods);

        prices.add(shippingKeyPriceVO);
        
        shippingKeyVO.setPrices(prices);
        
        delegate.updateShippingKey(shippingKeyVO, new HashMap());
        
        System.out.println("End Update Shipping Key");
    }
    catch (Exception e)
    {
		e.printStackTrace();
    }
  }
  
  /**
   * To run several tests at once JUnit provides an object, TestSuite which runs
   * any number of test cases together.
   * Each test is run as a separate instance of the test class
   * You make your suite accessible to a TestRunner tool with a static method
   * suite that returns a test suite.
   **/
  public static Test suite(){
    TestSuite suite = new TestSuite();
    // Create an instance of of Test class that will run this test case
    // When the test is run, the name of the test is used to look up the method
    // to run, using reflection
//    suite.addTest(new ProductMaintTest("testProductMaintList"));
//    suite.addTest(new ProductMaintTest("testUpdateProduct"));
//    suite.addTest(new ProductMaintTest("testGetProduct"));

//    suite.addTest(new ProductMaintTest("testInsertShippingKeyDetails"));
//    suite.addTest(new ProductMaintTest("testUpdateShippingKeyDetails"));
    suite.addTest(new ProductMaintTest("testGetShippingKeyDetails"));
    
    return suite;
  }

  /**
   * You can either user the text ui or swing ui to run the test
   **/
  public static void main(String[] args){
    // pass in the class of the test that needs to be run
    //junit.swingui.TestRunner.run(ResourceManagerTest.class);
    junit.textui.TestRunner.run(suite());
  }


}
