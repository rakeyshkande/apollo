package com.ftd.pdb.tests;

import com.ftd.pdb.common.ProductMaintConstants;

import java.text.SimpleDateFormat;

import java.util.Date;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;


/**
 * The test class will extend junit.framework.TestCase
 **/
public class SendUpdateFeedTest extends TestCase  {

  /**
   * Create a constructor that take a String parameter and passes it
   * to the super class
   **/
  public SendUpdateFeedTest(String name) {
      super(name);
  }

  /**
   * Override setup() to initialize variables
   **/
  protected void setUp(){
  }

  /**
   * Override tearDown() to release any permanent resources you allocated in setUp
   */
   public void tearDown(){
   }

   /**
    * Write the test case method in the fixture class. Be sure to make it public,
    * or it can't be invoked through reflection
    **/
  public void testSendUpdateFeed(){
    try
    {
//<<<<<<< SendUpdateFeedTest.java
//        NovatorTransmission suf = new SendUpdateFeed();
//        String response = suf.update("TEST");
//        suf.send("TEST");
      //  lm.debug("Server response was " + response);
//        suf = null;
//=======
//        SendUpdateFeed suf = new SendUpdateFeed();
//        String response = suf.update("TEST");
//        lm.debug("Server response was " + response);
//        suf = null;
//>>>>>>> 1.4
    }
    catch (Exception e)
    {
		e.printStackTrace();
    }
  }

   /**
    * Write the test case method in the fixture class. Be sure to make it public,
    * or it can't be invoked through reflection
    **/
  public void testDateParser(){
    try
    {
        SimpleDateFormat sdf = new SimpleDateFormat(ProductMaintConstants.NOVATOR_DATEFORMAT);
        Date vStartDate = null;
        String strDate = "01/20/2003";
        if(strDate != null && strDate.length() > 0) 
        { 
            vStartDate = sdf.parse(strDate); 
        }
        System.out.println(vStartDate.toString());
    }
    catch (Exception e)
    {
		e.printStackTrace();
    }
  }

  /**
   * To run several tests at once JUnit provides an object, TestSuite which runs
   * any number of test cases together.
   * Each test is run as a separate instance of the test class
   * You make your suite accessible to a TestRunner tool with a static method
   * suite that returns a test suite.
   **/
  public static Test suite(){
    TestSuite suite = new TestSuite();
    // Create an instance of of Test class that will run this test case
    // When the test is run, the name of the test is used to look up the method
    // to run, using reflection
//    suite.addTest(new SendUpdateFeedTest("testSendUpdateFeed"));
    suite.addTest(new SendUpdateFeedTest("testDateParser"));
    
    return suite;
  }

  /**
   * You can either user the text ui or swing ui to run the test
   **/
  public static void main(String[] args){
    // pass in the class of the test that needs to be run
    //junit.swingui.TestRunner.run(ResourceManagerTest.class);
    junit.textui.TestRunner.run(suite());
  }


}
