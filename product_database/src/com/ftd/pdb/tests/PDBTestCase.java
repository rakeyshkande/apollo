package com.ftd.pdb.tests;

import com.ftd.pdb.common.resources.IResourceProvider;
import com.ftd.pdb.common.resources.TestResourceProviderImpl;

import java.sql.Connection;

import junit.framework.TestCase;

import junit.framework.TestSuite;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public abstract class PDBTestCase extends TestCase {
    Connection conn;
    ApplicationContext context;
    IResourceProvider resourceProvider = new TestResourceProviderImpl();

    public PDBTestCase(String testCase) {
        super(testCase);
    }

    /**
     * Override setup() to initialize variables
     **/
    protected void setUp() throws Exception {
        //Start up spring framework
        context = 
                new FileSystemXmlApplicationContext("C:\\_RLSE_2.4\\FTD\\product_database\\web\\WEB-INF\\spring-pdb-servlet.xml");
        this.conn = resourceProvider.getDatabaseConnection();
    }

    /**
     * Override tearDown() to release any permanent resources you allocated in setUp
     */
    public void tearDown() throws Exception {
        if (conn != null && !conn.isClosed()) {
            conn.close();
        }
    }
}
