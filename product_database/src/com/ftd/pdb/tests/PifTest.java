package com.ftd.pdb.tests;

import com.ftd.pdb.pif.businessobject.ProductIndexBOImpl;
import com.ftd.pdb.pif.common.PifUtils;
import com.ftd.pdb.pif.common.valobjs.CompanyConfigVO;

import com.ftd.pdb.pif.dao.ProductIndexFeedDAOImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import junit.framework.TestSuite;


public class PifTest extends PDBTestCase {
    public PifTest(String testCase) {
        super(testCase);
    }

    public static void main(String[] args) {
        PifTest pifTest = 
//         new PifTest("testGetConfigs");
//         new PifTest("testReadIndexFile");
//         new PifTest("testLoadLatestIndexes");
//         new PifTest("testLoadIndexes");
//         new PifTest("testStageIndexes");
//         new PifTest("testBackupIndexes");
//         new PifTest("testMoveIndexes");
//        new PifTest("testAll");
//        new PifTest("testGetConfigs2");
        new PifTest("testGetIndexFiles");
        
        try {
            pifTest.runBare();
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    public static TestSuite suite() {
        TestSuite suite = null;

        try {
            suite = new TestSuite(PifTest.class);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return suite;
    }
    
    public void testAll() throws Exception {
        ProductIndexBOImpl pifBO = (ProductIndexBOImpl)context.getBean("pifBO");
        conn.setAutoCommit(false);
        try {
            System.out.println("Start: "+new Date());
            pifBO.load(conn,null);
            pifBO.stage(conn);
            pifBO.backup(conn);
            pifBO.move(conn);
            conn.commit();
            System.out.println("Finish: "+new Date());
        } catch (Throwable t) {
            t.printStackTrace();
            conn.rollback();
        }
    }
    
    public void testGetIndexFiles() throws Exception {
        ProductIndexBOImpl pifBO = (ProductIndexBOImpl)context.getBean("pifBO");
        pifBO.get(conn);
    }
    
    public void testMoveIndexes() throws Exception {
        ProductIndexBOImpl pifBO = (ProductIndexBOImpl)context.getBean("pifBO");
        conn.setAutoCommit(false);
        try {
            pifBO.move(conn);
            conn.commit();
        } catch (Throwable t) {
            t.printStackTrace();
            conn.rollback();
        }
    }
    
    public void testBackupIndexes() throws Exception {
        ProductIndexBOImpl pifBO = (ProductIndexBOImpl)context.getBean("pifBO");
        pifBO.backup(conn);
    }
    
    public void testStageIndexes() throws Exception {
        ProductIndexBOImpl pifBO = (ProductIndexBOImpl)context.getBean("pifBO");
        pifBO.stage(conn);
    }
    
    public void testLoadLatestIndexes() throws Exception {
        ProductIndexBOImpl pifBO = (ProductIndexBOImpl)context.getBean("pifBO");
        pifBO.load(conn,null);
    }
    
    public void testLoadIndexes() throws Exception {
        ProductIndexBOImpl pifBO = (ProductIndexBOImpl)context.getBean("pifBO");
        pifBO.load(conn,new Long(1));
    }
    
//    public void testReadIndexFile() throws Exception {
//        ProductIndexBOImpl pifBO = (ProductIndexBOImpl)context.getBean("pifBO");
//
//        List indexDataResult = new ArrayList();
//        List indexDataParentInfo = new ArrayList();
//        List<CompanyConfigVO> companyConfigData = PifUtils.getInstance().getCompanyData(conn);
//        pifBO.readCSVFile(companyConfigData.get(0), indexDataResult, indexDataParentInfo);
//        
//    }
    
    public void testGetConfigs() throws Exception {
        List<CompanyConfigVO> companies = PifUtils.getInstance().getCompanyData(conn);
        
        for( int idx=0; idx<companies.size(); idx++) {
            CompanyConfigVO vo = companies.get(idx);
            System.out.println(vo.toString());
            System.out.println();
        }
    }
    
    public void testGetConfigs2() throws Exception {
        ProductIndexFeedDAOImpl pifDAO = (ProductIndexFeedDAOImpl)context.getBean("pifDAO");
        List<CompanyConfigVO> companies = pifDAO.getPifCompanyData(conn);
        
        for( int idx=0; idx<companies.size(); idx++) {
            CompanyConfigVO vo = companies.get(idx);
            System.out.println(vo.toString());
            System.out.println();
        }
    }
}
