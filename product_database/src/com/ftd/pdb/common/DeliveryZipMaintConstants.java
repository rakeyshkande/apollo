package com.ftd.pdb.common;

public class DeliveryZipMaintConstants {
    public static final String REQUEST_TYPE = "AJAX_REQUEST_TYPE";
    public static final String REQUEST_TYPE_GET_CARRIERS = "GET_CARRIERS";
    public static final String REQUEST_TYPE_GET_BLOCKED_ZIPS = "GET_BLOCKED_ZIPS";
    public static final String REQUEST_TYPE_REMOVE_BLOCKED_ZIPS = "REMOVED_BLOCKED_ZIPS";
    public static final String REQUEST_TYPE_ADD_BLOCKED_ZIPS = "ADD_BLOCKED_ZIPS";
    
    public static final String PARAMETER_CARRIER_ID = "CARRIER_ID";    
    public static final String PARAMETER_ZIP_CODE = "ZIP_CODE";
    public static final String PARAMETER_CHECKBOX_ID = "CHECKBOX_ID";
    public static final String PARAMETER_INDEX = "idx";
    public static final String PARAMETER_BLOCKED_ZIPS = "BLOCKED_ZIPS";
    public static final String PARAMETER_EMAIL_ADDRESS = "EMAIL_ADDR";
    
    public static final String PARAMETER_SEARCH_CARRIER_ID = "SEARCH_CARRIER_ID";    
    public static final String PARAMETER_SEARCH_ZIP_CODE = "SEARCH_ZIP_CODE";    
}
