package com.ftd.pdb.common;

import org.apache.struts.action.ActionError;

/**
 * Contains all the constant values used within the Order Entry project.
 *
 * @author Jeff Penney
 * @version 1.0
 **/
public class ProductMaintConstants
{
    /*********************************************************************************************
    //config flie locations
    *********************************************************************************************/
    public static final String PROPERTY_FILE                = "pdb_config.xml";
    public static final String NOVATOR_MAPPING_FILE         = "novatormapping.xml";
    public static final String SECURITY_FILE                = "security-config.xml";
    public static final String DATASOURCE_NAME              = "application.db.datasource";
    public static final String PDB_CONFIG_CONTEXT           = "PDB_CONFIG";
    public static final String FTD_APPS_CONTEXT             = "FTDAPPS_PARMS";
    
    // ************** Logger Categories *********************
    // Functional categories
    public final static String VO_CATEGORY_NAME = "com.ftd.pdb";
    public static final String APPLICATION_CATEGORY_CONSTANT = "Application";
    public final static String APPLICATION_CATEGORY_CONSTANT_2 = "application.category";
    public final static String APPLICATION_FRAMEWORK_CONFIG_REALM_KEY = "ftd.framework.configuration";

    public final static String PDB_IMAGE_FTD_ACTION_CATEGORY_NAME = "com.ftd.pdb";

    public final static String PDB_GENERAL_CATEGORY_NAME = "com.ftd.pdb";
    
    public final static String PDB_SHIPPING_KEY_CATEGORY_NAME = "com.ftd.pdb.shippingkey";
    public final static String PDB_SHIPPING_KEY_CONTENT_NAME = "com.ftd.pdb.shippingkey";
    public final static String PDB_SHIPPING_KEY_DETAIL_NAME = "com.ftd.pdb.shippingkey";

    public final static String PDB_HOLIDAY_PRICING_ACTIVATE_CATEGORY_NAME = "com.ftd.pdb.holidaypricing";
    public final static String PDB_HOLIDAY_PRICING_UPDATE_CATEGORY_NAME = "com.ftd.pdb.holidaypricing";
    public final static String PDB_HOLIDAY_PRICING_FLOW_CATEGORY_NAME = "com.ftd.pdb.holidaypricing";
    public final static String PDB_PRODUCT_HOLIDAY_LIST_CATEGORY_NAME = "com.ftd.pdb.holidaypricing";
    public final static String PDB_PRODUCT_HOLIDAY_DELETE_CATEGORY_NAME = "com.ftd.pdb.holidaypricing";

    public final static String PDB_PRODUCT_LIST_CATEGORY_NAME = "com.ftd.pdb.productlist";
    public final static String PDB_PRODUCT_DETAIL_CATEGORY_NAME = "com.ftd.pdb.productdetail";
    public final static String PDB_UPDATE_NOVATOR_CATEGORY_NAME = "com.ftd.pdb.updatenovator";
    public final static String PDB_HOLIDAY_PRICING_CATEGORY_NAME = "com.ftd.pdb.holidaypricing";
    public final static String PDB_TESTS_CATEGORY_NAME = "com.ftd.pdb.tests";

    public final static String PDB_OCCASION_MAINT_DELEGATE_CATEGORY_NAME = "com.ftd.pdb.occasionmaint.delegate";
    public final static String PDB_OCCASION_MAINT_SERVICE_CATEGORY_NAME = "com.ftd.pdb.occasionmaint.service";
    public final static String PDB_OCCASION_MAINT_BUSINESS_CATEGORY_NAME = "com.ftd.pdb.occasionmaint.business";
    public final static String PDB_OCCASION_MAINTENANCE_CHANGE_CATEGORY_NAME = "com.ftd.pdb.occasionmaint.change";

    public final static String PDB_OCCASION_DAO_CATEGORY_NAME = "com.ftd.pdb.occasionmaint.dao";
    public final static String PDB_ADDON_DAO_CATEGORY_NAME = "com.ftd.pdb";
    public final static String PDB_OCCASIONADDON_DAO_CATEGORY_NAME = "com.ftd.pdb";
    public final static String PDB_OCCASION_MAINTENANCE_CATEGORY_NAME = "com.ftd.pdb";

    // Layer categories
    public final static String OE_LOOKUP_DELEGATE_CATEGORY_NAME = "com.ftd.pdb.lookup";
    public final static String OE_LOOKUP_SERVICE_CATEGORY_NAME = "com.ftd.pdb.lookup";
    public final static String OE_LOOKUP_BUSINESS_CATEGORY_NAME = "com.ftd.pdb.lookup";
    public final static String OE_LOOKUP_DAO_CATEGORY_NAME = "com.ftd.pdb.lookup";

    public final static String PDB_PRODUCT_DELEGATE_CATEGORY_NAME = "com.ftd.pdb.product.delegate";
    public final static String PDB_PRODUCT_SERVICE_CATEGORY_NAME = "com.ftd.pdb.product.service";
    public final static String OE_PRODUCT_BUSINESS_CATEGORY_NAME = "com.ftd.pdb.product.business";
    public final static String OE_PRODUCT_DAO_CATEGORY_NAME = "com.ftd.pdb.product.dao";

    public final static String PDB_PRODUCT_BATCH_DELEGATE_CATEGORY_NAME = "com.ftd.pdb.product.batch.delegate";
    public final static String PDB_PRODUCT_BATCH_SERVICE_CATEGORY_NAME = "com.ftd.pdb.product.batch.service";
    public final static String OE_PRODUCT_BATCH_BUSINESS_CATEGORY_NAME = "com.ftd.pdb.product.batch.business";
    public final static String OE_PRODUCT_BATCH_DAO_CATEGORY_NAME = "com.ftd.pdb.product.batch.dao";

    public final static String PDB_HOLIDAY_PRICING_DELEGATE_CATEGORY_NAME = "com.ftd.pdb.holidaypricing.delegate";
    public final static String PDB_HOLIDAY_PRICING_SERVICE_CATEGORY_NAME = "com.ftd.pdb.holidaypricing.service";
    public final static String PDB_HOLIDAY_PRICING_BUSINESS_CATEGORY_NAME = "com.ftd.pdb.holidaypricing.business";
    public final static String PDB_HOLIDAY_PRICING_DAO_CATEGORY_NAME = "com.ftd.pdb.holidaypricing.dao";

    public final static String PDB_COMMON_UTILITIES_CATEGORY_NAME = "com.ftd.pdb.common.utilities";
    public final static String PDB_SHIPPING_KEY_DELEGATE_CATEGORY_NAME = "com.ftd.pdb.shippingkey.delegate";

    public final static String PDB_GBB_DEFAULT_TITLE = "GBB_DEFAULT_TITLE";
    public final static String PDB_IMAGE_URL_PREFIX = "PDB_IMAGE_URL_PREFIX";
    public final static String PDB_IMAGE_URL_STANDARD_SUFFIX = "PDB_IMAGE_URL_STANDARD_SUFFIX";
    public final static String PDB_IMAGE_URL_DELUXE_SUFFIX = "PDB_IMAGE_URL_DELUXE_SUFFIX";
    public final static String PDB_IMAGE_URL_PREMIUM_SUFFIX = "PDB_IMAGE_URL_PREMIUM_SUFFIX";
    public final static String PDB_IMAGE_URL_TYPE = "PDB_IMAGE_URL_TYPE";
    public final static String PDB_FLORAL_LABEL_STANDARD = "FLORAL_LABEL_STANDARD";
    public final static String PDB_FLORAL_LABEL_DELUXE = "FLORAL_LABEL_DELUXE";
    public final static String PDB_FLORAL_LABEL_PREMIUM = "FLORAL_LABEL_PREMIUM";

    // *************** Cache ***************************
    public final static String CACHE_PERS_TEMPLATE_HANDLER = "PersonalizationTemplateHandler";

    // *************** Keys ***************************
    public final static String PDB_MAX_USER_BATCH_SIZE = "max.user.batch.size";
    
    public final static String PDB_NOVATOR_MAPPING_REALM_KEY = "ftd.application.novatormapping";
//    public final static String PDB_APP_CONFIG_REALM_KEY = "ftd.pdb.application.configuration";

    public final static String PDB_NOVATOR_PROD_IP_KEY = "novator.prod.server.ip";
    public final static String PDB_NOVATOR_LIVE_IP_KEY = "novator.live.server.ip";
    
    public final static String PDB_NOVATOR_CONTENT_IP_KEY = "novator.content.server.ip";
    public final static String PDB_NOVATOR_UAT_IP_KEY = "novator.uat.server.ip";
    public final static String PDB_NOVATOR_TEST_IP_KEY = "novator.test.server.ip";

    public final static String PDB_NOVATOR_PROD_PORT_KEY = "novator.prod.server.port";
    public final static String PDB_NOVATOR_LIVE_PORT_KEY = "novator.live.server.port";
    public final static String PDB_NOVATOR_CONTENT_PORT_KEY = "novator.content.server.port";
    public final static String PDB_NOVATOR_UAT_PORT_KEY = "novator.uat.server.port";
    public final static String PDB_NOVATOR_TEST_PORT_KEY = "novator.test.server.port";

    public final static String PDB_NOVATOR_LIVE_KEY = "novator.live";
    public final static String PDB_NOVATOR_PROD_KEY = "novator.prod";
    public final static String PDB_NOVATOR_UAT_KEY = "novator.uat";
    public final static String PDB_NOVATOR_TEST_KEY = "novator.test";
    public final static String PDB_NOVATOR_CONTENT_KEY = "novator.content";
    public final static String PDB_DEVELOPMENT_KEY = "ftd.server";

    public final static String PDB_NOVATOR_TIMEOUT_KEY = "novator.timeout";

    public final static String PDB_FTD_NUMBER_OF_SERVERS = "ftd.number.of.servers";
    public final static String PDB_FTD_LOGIN = "ftd.server.login";
    public final static String PDB_FTD_PASSWORD = "ftd.server.password";
    public final static String PDB_FTD_FTP_IP_KEY = "ftd.ftp.server.ip";
    public final static String PDB_FTD_PICS_IMAGE_DIRECTORY = "ftd.pics.server.image.directory";
    public final static String PDB_FTD_PRODUCTS_IMAGE_DIRECTORY = "ftd.products.server.image.directory";

    public final static String PDB_APP_CONFIG_MAILSERVER_KEY = "application.mail.server";
    public final static String PDB_APP_CONFIG_MAILLOGIN_KEY = "application.mail.login";
    public final static String PDB_APP_CONFIG_MAILPASSWORD_KEY = "application.mail.password";
    public final static String PDB_APP_CONFIG_MAILSENDTO_KEY = "application.mail.sendto";
    public final static String PDB_APP_CONFIG_MAILSENDTOCNT_KEY = "application.mail.sendtocnt";

    public final static String PDB_NOVATOR_NUMBER_OF_LIVE_SERVERS = "novator.live.number.of.servers";
    public final static String PDB_NOVATOR_LIVE_LOGIN = "novator.live.server.login";
    public final static String PDB_NOVATOR_LIVE_PASSWORD = "novator.live.server.password";
    public final static String PDB_NOVATOR_LIVE_FTP_IP_KEY = "novator.live.ftp.server.ip";
    public final static String PDB_NOVATOR_LIVE_PICS_IMAGE_DIRECTORY = "novator.live.pics.server.image.directory";
    public final static String PDB_NOVATOR_LIVE_PRODUCTS_IMAGE_DIRECTORY = "novator.live.products.server.image.directory";

    public final static String PDB_NOVATOR_NUMBER_OF_TEST_SERVERS = "novator.test.number.of.servers";
    public final static String PDB_NOVATOR_TEST_LOGIN = "novator.test.server.login";
    public final static String PDB_NOVATOR_TEST_PASSWORD = "novator.test.server.password";
    public final static String PDB_NOVATOR_TEST_FTP_IP_KEY = "novator.test.ftp.server.ip";
    public final static String PDB_NOVATOR_TEST_PICS_IMAGE_DIRECTORY = "novator.test.pics.server.image.directory";
    public final static String PDB_NOVATOR_TEST_PRODUCTS_IMAGE_DIRECTORY = "novator.test.products.server.image.directory";

    public final static String PDB_NOVATOR_NUMBER_OF_UAT_SERVERS = "novator.uat.number.of.servers";
    public final static String PDB_NOVATOR_UAT_LOGIN = "novator.uat.server.login";
    public final static String PDB_NOVATOR_UAT_PASSWORD = "novator.uat.server.password";
    public final static String PDB_NOVATOR_UAT_FTP_IP_KEY = "novator.uat.ftp.server.ip";
    public final static String PDB_NOVATOR_UAT_PICS_IMAGE_DIRECTORY = "novator.uat.pics.server.image.directory";
    public final static String PDB_NOVATOR_UAT_PRODUCTS_IMAGE_DIRECTORY = "novator.uat.products.server.image.directory";

    public final static String PDB_NOVATOR_NUMBER_OF_CONTENT_SERVERS = "novator.content.number.of.servers";
    public final static String PDB_NOVATOR_CONTENT_LOGIN = "novator.content.server.login";
    public final static String PDB_NOVATOR_CONTENT_PASSWORD = "novator.content.server.password";
    public final static String PDB_NOVATOR_CONTENT_FTP_IP_KEY = "novator.content.ftp.server.ip";    
    public final static String PDB_NOVATOR_CONTENT_PICS_IMAGE_DIRECTORY = "novator.content.pics.server.image.directory";
    public final static String PDB_NOVATOR_CONTENT_PRODUCTS_IMAGE_DIRECTORY = "novator.content.products.server.image.directory";
    
    public final static String PDB_FTP_TEMP_DIRECTORY_KEY = "ftp.temp.directory";
    
    public final static String SUB_CODE_LIST_KEY = "subCodeList";
    public final static String SERVICES_DURATION_KEY = "durations";
    public final static String ALLOW_FREE_SHIPPING_KEY = "allowFreeShippingFlag";
    public final static String EXCLUDE_STATES_KEY = "excludeStates";
    public final static String CURRENT_COUNTRY_LIST_KEY = "currCountries";

    public final static String PDB_UPSELL_DETAIL_ID_KEY = "detailid";
    public final static String PDB_UPSELL_DETAIL_NAME_KEY = "detailname";
    public final static String PDB_UPSELL_DETAIL_TYPE_KEY = "detailtype";
    public final static String PDB_UPSELL_DETAIL_PRICE_KEY = "detailprice";
    public final static String PDB_UPSELL_DETAIL_AVAILABLE_KEY = "detailavailable";
    public final static String PRODUCT_MASTER_SKU_KEY = "masterskulist";
    public final static String PDB_UPSELL_DETAIL_NOVATORID_KEY = "novatorID";
    public final static String PDB_UPSELL_DETAIL_SENT_TO_NOVATOR_PROD_KEY = "sentToNovatorProd";
    public final static String PDB_UPSELL_DETAIL_SENT_TO_NOVATOR_CONTENT_KEY = "sentToNovatorContent";
    public final static String PDB_UPSELL_DETAIL_SENT_TO_NOVATOR_UAT_KEY = "sentToNovatorUAT";
    public final static String PDB_UPSELL_DETAIL_SENT_TO_NOVATOR_TEST_KEY = "sentToNovatorTest";
    public final static String PDB_EXCLUDED_STATES_KEY = "excludedDeliveryStates";
    
    public final static String SHOW_ASSOCIATE_LIST_KEY = "associateskus";
    public final static String USER_KEY = "UserProfile";
    public final static String CATEGORIES_KEY = "categories";
    public final static String PRODUCT_TYPES_KEY = "types";
    public final static String PRODUCT_SUB_TYPES_KEY = "subTypes";
    public final static String PRODUCT_CURRENT_SUB_TYPES_KEY = "currentSubTypes";
    public final static String COUNTRIES_KEY = "countries";
    public final static String SECOND_CHOICE_KEY = "secondChoices";
    public final static String STATES_KEY = "states";
    public final static String EXCEPTION_CODES_KEY = "exceptions";
    public final static String PRICE_POINTS_KEY = "pricePoints";
    public final static String SHIPPING_KEYS_KEY = "shippingKeys";
    public final static String RECIPIENT_SEARCH_KEY = "recipientSearchList";
    public final static String PERSONALIZATION_TEMPLATE_KEY = "personalizationTemplateList";
    public final static String PERSONALIZATION_TEMPLATE_ORDER_KEY = "personalizationTemplateOrderList";
    public final static String SEARCH_PRIORITY_KEY = "searchPriorityList";
    public final static String COLORS_KEY = "colorsList";
    public final static String CPN_SOURCE_CODE_KEY = "cpnsourcecodelist";
    public final static String NOVATOR_NOTE_KEY = "novatornote";
    public final static String NOVATOR_NOT_UPDATED_MESSAGE_KEY = "productmaintdetail.novatornotupdated";
    public final static String NOTHING_UPDATED_KEY = "productmaintdetail.nothingupdated";
    public final static String NOVATOR_BOXES_NOT_CHECKED_KEY = "productmaintdetail.novatornothingchecked";
    public final static String NOVATOR_RETURN_TO_SUBMIT_KEY = "productmaintdetail.novatorreturntosubmit";
    public final static String NOVATOR_SUBMIT_SUCCESS_MESSAGE_KEY="productmaintdetail.novatorsuccess";
    public final static String SOURCE_CODES_KEY = "sourceCodes";
    public final static String PDB_ADMIN_LINK_KEY = "adminLink";
    public final static String PDB_COMPANY_MASTER_KEY = "companyMasterList";
    public final static String PDB_SHIPPING_SYSTEMS_KEY = "shippingSystemsList";
    public final static String BOX_KEY = "boxList";
    public final static String COMPONENTS_KEY = "componentsList";
    public final static String AVAILABLE_COMPONENT_SKUS = "availableComponentSkuList";
    public final static String INCLUDED_COMPONENT_SKUS = "includedComponentSkuList";

    public final static String OE_PRODUCT_ID_KEY = "productId";
    public final static String PDB_PRODUCT_SEARCH_BY_KEY = "searchBy";
    public final static String PDB_SEARCH_BY_PRODUCT_ID = "searchByProdId";
    public final static String PDB_CARRIER_KEY = "carrier";
    public final static String PDB_VENDOR_CARRIER_LIST_KEY = "vendorcarrierlist";
    public final static String PDB_COLOR_LIST_KEY = "colorlist";
    public final static String PDB_DELIV_STATE_EXCL_LIST_KEY = "deliverystateexclusionlist";
    public static final String EXCLUDED_STATES = "excludedDeliveryStates";
    public static final String INCLUDED_COLORS = "includedColorsList";
    public static final String EXCLUDED_COLORS = "excludedColorsList";
    public static final String EXCLUDED_COLORS_STATIC = "excludedColorsListStatic";
    public static final String INCLUDED_COLORS_STATIC = "includedColorsListStatic";
    public static final String VENDOR_PRODUCTS = "vendorProductsList";
    public static final String BATCH_ERROR = "batchError";
    public static final String DISABLE_BATCH_MODE = "disableBatchMode";
    public static final String CLOSE_POPUP = "closePopUp";
    public static final String OPEN_ALL_TABS = "openAllTabsFlag";
    public static final String CARRIER_LIST_KEY = "carrierList";
    public static final String SEARCH_TYPE_LIST_KEY = "searchTypeList";
    public static final String SEARCH_RESULT_LIST_KEY = "searchResultsList";
    
    public final static String SKU_AVAILABLE_KEY = "Y";
    public final static String PRODUCT_AVAILABLE_KEY = "A";
    public final static String PRODUCT_HOLD_UNTIL_AVAILABLE_KEY_Y = "Y";
    public final static String PRODUCT_HOLD_UNTIL_AVAILABLE_KEY_N = "N";
    public final static String PRODUCT_COLOR_FLAG = "C";
    public final static String PRODUCT_SIZE_FLAG = "S";
    public final static String PRODUCT_UNAVAILABLE_KEY = "U";
    public final static String ADDON_CARD_FLAG = "4";
    public final static String DISPLAY_EXPIRED_SOURCE_FLAG = "Y";
    public final static String DEFAULT_JCPENNEY_CATEGORY = "A";
    public final static String PRODUCT_SENT_TO_NOVATOR = "Y";
    public final static String PRODUCT_NOT_SENT_TO_NOVATOR = "N";    
    public final static String OE_ERROR_STRINGS_TEMP = "applicationmessages";
    public final static String EXCLUDED_STATE = "excludedState";
	public final static String STATE_ID = "stateId";
    public final static String STATE_NAME = "stateName";
    public final static String SUNDAY = "sun";
    public final static String MONDAY = "mon";
	public final static String TUESDAY = "tue";
	public final static String WEDNESDAY = "wed";
	public final static String THURSDAY = "thu";
	public final static String FRIDAY = "fri";
	public final static String SATURDAY = "sat";

    public final static String START_DATE = "startDate";
    public final static String END_DATE = "endDate";

    public final static String OCCASION_LIST_KEY = "occasionList";
    public final static String AVAILABLE_CARDS_KEY = "availableList";
    public final static String SELECTED_CARDS_KEY = "selectedList";

    public final static String VENDOR_LIST_KEY = "vendorList";
    public final static String SHIPPING_AVAIL_KEY = "shippingAvailabilityList";

   //USED FOR VALIDATION ON THE PRODUCT  DETAIL FORM
    public final static String PRODUCT_TYPE_FLORAL = "FLORAL";
    public final static String PRODUCT_TYPE_SPECIALTY = "SPEGFT";
    public final static String PRODUCT_TYPE_FRESHCUTS = "FRECUT";
    public final static String PRODUCT_DELIVERY_TYPE_INTL = "I";
    public final static String PRODUCT_SUBTYPE_FUNERAL = "SYMPAT";
    public final static String PRODUCT_SUBTYPE_EXOTIC = "EXOTIC";
    public final static String PRODUCT_TYPE_SAME_DAY_FRESH_CUTS_CODE = "SDFC";
    public final static String PRODUCT_TYPE_SAME_DAY_GIFT_CODE = "SDG";
    public final static String PRODUCT_TYPE_SERVICES = "SERVICES";

    public final static String DAY_MONDAY_KEY = "mon";
    public final static String DAY_TUESDAY_KEY = "tue";
    public final static String DAY_WEDNESDAY_KEY = "wed";
    public final static String DAY_THURSDAY_KEY = "thu";
    public final static String DAY_FRIDAY_KEY = "fri";
    public final static String DAY_SATURDAY_KEY = "sat";
    public final static String DAY_SUNDAY_KEY = "sun";
    
    public final static String DELV_DAY_SUN_KEY = "deliveryDaySunday";
    public final static String DELV_DAY_MON_KEY = "deliveryDayMonday";
    public final static String DELV_DAY_TUE_KEY = "deliveryDayTuesday";
    public final static String DELV_DAY_WED_KEY = "deliveryDayWednesday";
    public final static String DELV_DAY_THU_KEY = "deliveryDayThursday";
    public final static String DELV_DAY_FRI_KEY = "deliveryDayFriday";
    public final static String DELV_DAY_SAT_KEY = "deliveryDaySaturday";
    public final static String EXCLUDED_STATE_ID ="excludedStateId";
    public final static String EXCLUDED_STATE_NAME ="excludedStateName";

    public final static String ADDON_BEARS_KEY = "bears";
    public final static String ADDON_CHOCOLATE_KEY = "chocolate";
    public final static String ADDON_CARDS_KEY = "cards";
    public final static String ADDON_BALLOONS_KEY = "balloons";
    public final static String ADDON_FUNERAL_KEY = "funeral";

    public final static String CURRENCY_KEY = "USD";

    public final static boolean AUTO_COMMIT_KEY = true;

    public final static String SHIPPING_METHODS_KEY = "shippingMethods";

    public final static String PDB_JCPENNEY_CATEGORY_KEY = "JCPenneyCategories";

    public final static String PDB_PRODUCT_SUB_CODE_ID_KEY = "productSubCodeId";
    public final static String PDB_PRODUCT_SUB_CODE_DESC_KEY = "productSubCodeDescription";
    public final static String PDB_PRODUCT_SUB_CODE_REF_KEY = "productSubCodeReference";
    public final static String PDB_PRODUCT_SUB_CODE_PRICE_KEY = "productSubCodePrice";
    public final static String PDB_PRODUCT_SUB_CODE_HOLIDAY_SKU_KEY = "subCodeHolidaySKU";
    public final static String PDB_PRODUCT_SUB_CODE_HOLIDAY_PRICE_KEY = "subCodeHolidayPrice";
    public final static String PDB_PRODUCT_SUB_CODE_AVAILABLE_KEY = "productSubCodeAvailable";
    public final static String PDB_PRODUCT_SUB_CODE_VENDOR_PRICE_KEY = "subCodeVendorPrice";
    public final static String PDB_PRODUCT_SUB_CODE_VENDOR_SKU_KEY = "subCodeVendorSku";
    public final static String PDB_PRODUCT_SUB_CODE_DIM_WEIGHT_KEY = "productSubCodeDimWeight";    
    public final static String PDB_PRODUCT_SUB_CODE_ID_ERROR_KEY = "subcodeIDError";
    public final static String PDB_PRODUCT_SUB_CODE_DESCRIPTION_ERROR_KEY = "subcodeDescriptionError";
    public final static String PDB_PRODUCT_SUB_CODE_REF_ERROR_KEY = "subcodeREFError";
    public final static String PDB_PRODUCT_SUB_CODE_PRICE_ERROR_KEY = "subcodePriceError";
    public final static String PDB_PRODUCT_SUB_CODE_VENDOR_SKU_ERROR_KEY = "subcodeVendorSKUError";
    public final static String PDB_PRODUCT_SUB_CODE_DIM_WEIGHT_ERROR_KEY = "subcodeDimWeightError";
    public final static String PDB_PRODUCT_SUB_CODE_VENDOR_COST_ERROR_KEY = "subcodeVendorCostError";
    public final static String PDB_PRODUCT_UPSELL_ERROR_DESC_KEY = "upsellErrorDescriptions";
    public final static String PDB_PRODUCT_UPSELL_ASSOC_ID_ERROR_KEY = "upsellAssocIdError";
    public final static String PDB_PRODUCT_UPSELL_ASSOC_NAME_ERROR_KEY = "upsellAssocNameError";
    public final static String PDB_PRODUCT_UPSELL_ASSOC_PRICE_ERROR_KEY = "upsellAssocPriceError";
    public final static String PDB_PRODUCT_UPSELL_ASSOC_TYPE_ERROR_KEY = "upsellAssocTypeError";
    public final static String PDB_PRODUCT_UPSELL_ASSOC_STATUS_ERROR_KEY = "upsellAssocStatusError";
    //public final static String PDB_PRODUCT_ERROR_DESC_KEY = "productErrorDescriptions";
    public final static String PDB_PRODUCT_ERROR_HOLIDAY_SUB_PRICE_KEY = "productErrorHolidaySubPrice";

    public final static int PDB_EMPTY_PRICE = 0;
    public final static String PDB_SPECIALTY_GIFT_CODE = "SPEGFT";
    public final static String PDB_FRESH_CUTS_CODE = "FRECUT";
    public final static String OE_SHIPPING_KEY_CONTENT_NAME = "";
    public final static String PDB_DATE_FORMAT = "MM/dd/yyyy";
    public final static String BLANK_STRING = "";
    public final static String NONE = "NONE";
    public final static int PDB_PRODUCT_SUB_CODE_LIST_MAX = 100;
    public final static int SHORT_DESCRIPTION_MAX = 80;
    public final static int LONG_DESCRIPTION_MAX = 1024;
    public final static String HP_DELIMITER = " ";
    public final static String NOVATOR_DELIMITER = " ";
    public final static String SKIP = "SKIP";

    public final static int CPN_SKU_MAX_LENGTH = 10;
    public final static int CPN_MAX_LENGTH = 20;

    public final static String PRODUCT_NO_VENDOR = "00000";

    public final static String PDB_DELETED_SKU_KEY="deletesku";
    public final static String UPSELL_NEW_SKU_KEY = "upselldetail.newsku";
    public final static String PRODUCT_MAINT_INSTRUCTION_KEY = "instruction";
    public final static String COPY_PRODUCT = "copyProduct";
    public final static String COPY_PRODUCT_LOAD = "LOAD";
    public final static String COPY_PRODUCT_COPY = "COPY";
    public final static String PRODUCT_MAINT_UPSELL_INSTRUCTION_KEY = "upsellinstruction";
    public final static String PRODUCT_MAINT_NEW_PRODUCT_KEY = "productmaintdetail.newproduct";
    public final static String PRODUCT_MAINT_PRODUCT_SAVED_KEY = "productmaintdetail.saved";
    public final static String PRODUCT_UPSELL_UPDATED =  "productmaintdetail.skuupdated";
    public final static String PRODUCT_MAINT_UPSELL_ATTACHED_VIEW_KEY = "productmaintdetail.upsellattachedview";
    public final static String PRODUCT_MAINT_UPSELL_ATTACHED_UPDATE_KEY = "productmaintdetail.upsellattachedupdate";
    
    public final static String PDBSYSTEMEXCEPTION_CLASS_NAME = "com.ftd.pdb.common.exceptions.PDBSystemException";
    public final static String PDBAPPLICATIONEXCEPTION_CLASS_NAME = "com.ftd.pdb.common.exceptions.PDBApplicationException";

    public final static String END_OF_LINE_CHARACTER = "\r\n";
    public final static String LINE_BREAK = "<BR>";

    public final static int PDB_UPSELL_DISPLAY_AMOUNT = 6;
    public final static int PDB_UPSELL_MASTER_SKU_SIZE = 4;
    public final static int UPSELL_ASSOC_NAME_MAX = 250;
    public final static int UPSELL_MIN_ASSOC_SKU_AMOUNT = 2;
    public final static int UPSELL_MAX_ASSOC_SKU_AMOUNT = 6;
    public final static String DOMESTIC_CODE = "D";
    public final static String INTERNATIONAL_CODE = "I";
    public final static String DOMESTIC_COUNTRY_CODE = "01";
    public final static String DOMESTIC_COUNTRY_DESC = " ";

    public final static String SENT_TO_NOVATOR_TRUE = "Y";

    public final static int PRODUCT_ID_LENGTH_MAX = 4;
    public final static int PDB_PRODUCT_NAME_MAX = 25;
    public final static int PDB_GENERAL_COMMENT_MAX = 1024;
    public final static int PDB_ITEM_COMMENT_MAX = 62;
    public final static int UPSELL_PRODUCT_NAME_MAX = PDB_PRODUCT_NAME_MAX;
    public final static int UPSELL_PRODUCT_MASTER_NAME_MAX = 100;    
    public final static int UPSELL_PRODUCT_DESCRIPTION_MAX = 1024;
    public final static int NOVATOR_ID_LENGTH_MAX = 10;
    public final static int RECIPE_LENGTH_MAX = 255;

//    public final static String SHOW_SUBCODE_LIST_KEY = "subcodeList";
//    public final static String HIDE_SUBCODE_LIST_KEY = "hideSubcodes";
    public final static String LAST_SUBCODE_KEY = "lastSubcodeKey";

    public final static String MENU_BACK_KEY = "menuBack";
    public final static String MENU_BACK_TEXT_KEY = "menuBackText";
    public final static String MENU_BACK_OPTIONAL_ARGS = "optionalArgs";
    public final static String MENU_BACK_DEFAULT_KEY = "admin.do";
    public final static String MENU_BACK_PRODUCT_MAINT_LIST_KEY = "showProductMaintList.do";
    public final static String MENU_BACK_SHIPPING_KEY_KEY = "showShippingKeyControl.do";
    public final static String MENU_BACK_TEXT_DEFAULT = "";
    public final static String MENU_BACK_TEXT_PRODUCT_MAINT_LIST_KEY = "Back to Product Maintenance";
    public final static String MENU_BACK_TEXT_SHIPPING_KEY_KEY = "Back to Shipping Key Maintenance";
    public final static String MENU_BACK_UPSELL_LIST_KEY = "showUpsellMaintList.do";
    public final static String MENU_BACK_TEXT_UPSELL_LIST_KEY = "Back to Upsell List";
    public final static String MENU_BACK_TEXT_CPN_LIST_KEY = "Back to Associate SKU List";
    public final static String MENU_BACK_PROD_LIST_KEY = "ProductMaintListSetupAction.do";
    public final static String MENU_BACK_TEXT_PROD_LIST_KEY = "Back to Product List";
    
    public final static String SHIPPING_STANDARD_CODE = "GR";
    public final static String SHIPPING_NEXT_DAY_CODE = "ND";
    public final static String SHIPPING_TWO_DAY_CODE = "2F";
    public final static String SHIPPING_SATURDAY_CODE = "SA";
    public final static String SHIPPING_SAME_DAY_CODE = "SD";
    public final static String SHIPPING_SUNDAY_CODE = "SU";

    public final static int LAST_UPDATE_EXCEPTION_CODE = 20001;

    public final static String SHIPPING_SYSTEM_TYPE_SDS = "SDS";
    public final static String SHIPPING_SYSTEM_TYPE_ESCALATE = "ESCALATE";
    public final static String SHIPPING_SYSTEM_TYPE_FTP = "FTP";

    public final static String COMPANY_DEFAULT_VALUE = "FTD";
    public final static String COMPANY_SFMB_VALUE = "SFMB";
    public final static String SFMB_CATEGORY_VALUE = "DMB";
    public final static String SFMB_SUBTYPE_VALUE = "SFMB";
    public final static String PRODUCT_TYPE_NONE = "NONE";    
    
    public final static String APPLICATION_ID = "PDB";    
    
    public static final String PRODUCT_ADD_ON_MAP_KEY = "productAddOnMap";
    public static final String PRODUCT_ADD_ON_LIST = "productAddOnList";
    public static final String PRODUCT_ADD_ON_VASE_LIST = "productAddOnVaseList";
    public static final String ADD_ON_ACTIVE_FLAG_KEY = "add_on_active_flag";
    public static final String ADD_ON_AVAILABLE_FLAG_KEY = "add_on_available_flag";
    public static final String ADD_ON_ID_KEY = "add_on_id";
    public static final String ADD_ON_TYPE_ID_KEY = "add_on_type_id";
    public static final String ADD_ON_PRODUCT_FEED_FLAG_KEY = "add_on_product_feed_flag";
    public static final String ADD_ON_PRICE_KEY = "add_on_price";
    public static final String PRODUCT_PRODUCT_ADD_ON_DESCRIPTION_KEY = "add_on_description";
    public static final String ADD_ON_PRODUCT_DISPLAY_SEQUENCE_NUMBER_KEY = "add_on_product_display_sequence_number";
    public static final String ADD_ON_PRODUCT_MAX_QUANTITY_KEY = "add_on_product_max_quantity";
    
    public static final String ADD_ON_VASE_ACTIVE_FLAG_KEY = "add_on_vase_active_flag";
    public static final String ADD_ON_VASE_PRODUCT_DISPLAY_SEQUENCE_NUMBER_KEY = "add_on_vase_product_display_sequence_number";
    public static final String ADD_ON_VASE_MAX_QUANTITY_KEY = "add_on_vase_max_quantity";
 
    public static final String PRODUCT_ADDON_VO_VASE_KEY = "VASE";
    public static final String PRODUCT_ADDON_VO_ADDON_KEY = "ADDON";
    public static final Integer ADDON_TYPE_VASE_ID = new Integer(7);
    
    public static final Integer PRODUCT_ADDON_MIN_DISPLAY_SEQUENCE_NUMBER = new Integer(1);
    public static final Integer PRODUCT_ADDON_MAX_DISPLAY_SEQUENCE_NUMBER = new Integer(9999);
    public static final Integer PRODUCT_ADDON_MIN_MAX_QUANTITY = new Integer(1);
    public static final Integer PRODUCT_ADDON_MAX_MAX_QUANTITY = new Integer(99);

    
    // ************* Interface Names ******************
    public final static String OE_IPRODUCTSVCBUSINESS_INTERFACE = "IProductMaintSVCBusiness";
    public final static String OE_IPRODUCTBO_INTERFACE = "IProductBO";
    public final static String OE_IPRODUCTDAO_INTERFACE = "IProductDAO";
    
    public final static String OE_IPRODUCTBATCHSVCBUSINESS_INTERFACE = "IProductBatchSVCBusiness";
    public final static String OE_IPRODUCT_BATCH_BO_INTERFACE = "IProductBatchBO";
    public final static String OE_IPRODUCT_BATCH_DAO_INTERFACE = "IProductBatchDAO";

    public final static String PDB_IUPSELLDAO_INTERFACE = "IUpsellDAO";
    public final static String PDB_IUPSELLBO_INTERFACE = "IUpsellBO";
    public final static String PDB_IUPSELLSVCBUSINESS_INTERFACE = "IUpsellSVCBusiness";
    
    public final static String OE_ILOOKUPSVCBUSINESS_INTERFACE = "ILookupSVCBusiness";
    public final static String OE_ILOOKUPBO_INTERFACE = "ILookupBO";
    public final static String OE_ILOOKUPDAO_INTERFACE = "ILookupDAO";

    public final static String OE_IADDONDAO_INTERFACE = "IAddonDAO";

    public final static String OE_IHOLIDAYSVCBUSINESS_INTERFACE = "IHolidaySVCBusiness";
    public final static String OE_IHOLIDAYPRICINGBO_INTERFACE = "IHolidayPricingBO";
    public final static String OE_IHOLIDAYPRICINGDAO_INTERFACE = "IHolidayPricingDAO";
    public final static String OE_IHOLIDAYDAO_INTERFACE = "IHolidayDAO";

    public final static String OE_IOCCASIONMAINTSVCBUSINESS_INTERFACE = "IOccasionMaintenanceSVCService";
    public final static String OE_IOCCASIONMAINTENANCEBO_INTERFACE = "IOccasionMaintenanceBO";
    public final static String OE_IOCCASIONMAINTENANCEDAO_INTERFACE = "IOccasionMaintenanceDAO";
    public final static String OE_IOCCASIONADDONDAO_INTERFACE = "IOccasionAddonDAO";
    public final static String OE_IOCCASIONDAO_INTERFACE = "IOccasionDAO";

    public final static String PDB_ISHIPPINGKEYSVC_INTERFACE = "IShippingKeySVCService";
    public final static String PDB_ISHIPPINGKEYBO_INTERFACE = "IShippingKeyBO";
    public final static String PDB_ISHIPPINGKEYDAO_INTERFACE = "IShippingKeyDAO";

    // ************** Application Execption Constants ********************
    public final static int OE_INACTIVE_USER_PROFILE_EXCEPTION = 150;
    public final static int OE_PASSWORD_DOES_NOT_MATCH_EXCEPTION = 151;
    public final static int OE_USERNAME_EMPTY_EXCEPTION = 152;
    public final static int OE_DATE_PARSE_EXCEPTION = 153;
    public final static int OE_CONVERT_VO_TO_XML_EXCEPTION = 154;
    public final static int OE_INVALID_USERNAME_EXCEPTION = 155;
    public final static int OE_SEARCH_CRITERIA_EMPTY_EXCEPTION = 156;
    public final static int OE_CANNOT_SAVE_NULL_PRODUCT_EXCEPTION = 157;
    public final static int OE_ERROR_ARCHIVING_FILE = 158;
    public final static int OE_ERROR_EMAILING_CATALOG_FILE = 159;
    public final static int OE_ERROR_CREATING_FILE = 160;
    public final static int OE_ERROR_WRITING_FILE = 161;
    public final static int PDB_CANNOT_SAVE_EMPTY_SHIPPING_RESTRICTIONS_EXCEPTION = 162;
    public final static int PDB_CANNOT_SAVE_EMPTY_KEYWORDS_EXCEPTION = 163;
    public final static int PDB_NOVATOR_ID_DOES_NOT_EXIST_EXCEPTION = 164;
    public final static int SQL_EXCEPTION = 165;
    public final static int EMPTY_PRODUCT_ID_EXCEPTION = 166;
    public final static int EMPTY_PRODUCT_NAME_EXCEPTION = 167;
    public final static int PRICE_RANK_SAME_EXCEPTION = 168;
    public final static int PRODUCT_SUBCODE_DESCRIPTION_EXCEPTION = 169;
    public final static int COLOR_FLAG_EXCEPTION = 170;
    public final static int SHORT_DESCRIPTION_MAX_EXCEPTION = 171;
    public final static int LONG_DESCRIPTION_MAX_EXCEPTION = 172;
    public final static int HOLIDAY_PRICING_INCOMPLETE_EXCEPTION = 173;
    public final static int HOLIDAY_PRICING_SUBCODE_INCOMPLETE_EXCEPTION = 174;
    public final static int GENERAL_APPLICATION_EXCEPTION = 175;
    public final static int PRODUCT_ID_LENGTH_MAX_EXCEPTION = 176;
    public final static int PDB_NOVATOR_ID_DOES_EXIST_EXCEPTION = 177;
    public final static int HOLIDAY_SKU_NOT_UNIQUE_EXCEPTION = 178;
    public final static int SUBCODE_NOT_UNIQUE_EXCEPTION = 179;
    public final static int SUBCODE_NOT_UNIQUE_ON_PRODUCT_EXCEPTION = 180;
    public final static int HOLIDAY_SKU_NOT_UNIQUE_ON_PRODUCT_EXCEPTION = 181;
    public final static int LIVE_IMAGE_UPLOAD_EXCEPTION = 186;
    public final static int TEST_IMAGE_UPLOAD_EXCEPTION = 187;
    public final static int UAT_IMAGE_UPLOAD_EXCEPTION = 188;
    public final static int CONTENT_IMAGE_UPLOAD_EXCEPTION = 189;
    public final static int FTD_IMAGE_UPLOAD_EXCEPTION = 190;
    public final static int EMPTY_IMAGE_SOURCE_EXCEPTION = 191;
    public final static int EMPTY_IMAGE_PATH_EXCEPTION = 192;  
    public final static int ILLEGAL_CHARACTER_IN_NOVATOR_ID_EXCEPTION = 193;
    public final static int LAST_UPDATE_EXCEPTION = 194;

    public final static int PRODUCT_NOVATOR_NAME_REQUIRED_EXCEPTION = 195;
    public final static int PRODUCT_NOVATOR_ID_REQUIRED_EXCEPTION = 196;
    public final static int PRODUCT_CATEGORY = 197;
    public final static int PRODUCT_TYPE_REQUIRED_EXCEPTION = 198;
    public final static int PRODUCT_SUBTYPE_REQUIRED_EXCEPTION = 199;
    public final static int PRODUCT_SHIPPING_AVAILABILITY_REQUIRED_EXCEPTION = 200;
    public final static int PRODUCT_COUNTRY_AVAILABILITY_REQUIRED_EXCEPTION = 201;
    public final static int PRODUCT_FLORIST_REFERENCE_NUMBER_REQUIRED_EXCEPTION = 202;
    public final static int PRODUCT_MERCURY_DESCRIPTION_REQUIRED_EXCEPTION = 203;
    public final static int PRODUCT_SECOND_CHOICE_REQUIRED_EXCEPTION = 203;
    public final static int PRODUCT_MERCURY_SECOND_CHOICE_REQUIRED_EXCEPTION= 204;
    public final static int PRODUCT_LONG_DESCRIPTION_REQUIRED_EXCEPTION = 205;
    public final static int PRODUCT_USE_COLORS_FLAG_REQUIRED_EXCEPTION = 206;
    public final static int PRODUCT_DIMWEIGHT_REQUIRED_EXCEPTION = 207;
    public final static int PRODUCT_VENDOR_COST_REQUIRED_EXCEPTION = 208;
    public final static int PRODUCT_JCPENNEY_CATEGORY_REQUIRED_EXCEPTION = 209;
    public final static int PRODUCT_VENDOR_LIST_REQUIRED_EXCEPTION = 210;
    public final static int PRODUCT_SHIPPING_KEY_REQUIRED_EXCEPTION = 211;
    public final static int PRODUCT_DELIVERY_OPTIONS_REQUIRED_EXCEPTION = 212;
    public final static int PRODUCT_VALID_SHIP_DAYS_REQUIRED_EXCEPTION = 213;
    public final static int PRODUCT_PRICE1_REQUIRED_EXCEPTION = 214;
    public final static int PRODUCT_SUBCODE_SUBCODE_REQUIRED_EXCEPTION = 215;
    public final static int PRODUCT_SUBCODE_DESCRIPTION_REQUIRED_EXCEPTION = 216;
    public final static int PRODUCT_SUBCODE_REF_REQUIRED_EXCEPTION = 217;
    public final static int PRODUCT_SUBCODE_PRICE_REQUIRED_EXCEPTION = 218;
    public final static int PRODUCT_SUBCODE_VENDOR_SKU_REQUIRED_EXCEPTION = 219;
    public final static int PRODUCT_SUBCODE_DIMWEIGHT_REQUIRED_EXCEPTION = 220;
    public final static int PRODUCT_SUBCODE_VENDOR_COST_REQUIRED_EXCEPTION = 221;
    public final static int PRODUCT_ARRANGEMENT_COLORS_REQUIRED = 222;
    public final static int PRODUCT_SUBCODE_PRICES_MUST_BE_EQUAL = 223;
    public final static int PRODUCT_REQUIRED_FIELDS_MISSING = 224;

    public final static int  UPSELL_MASTER_SKU_REQUIRED = 226;
    public final static int  UPSELL_MASTER_SKU_LENGTH_ERROR = 227;
    public final static int  UPSELL_MASTER_SKU_EXISTS_IN_PRODUCT_MASTER = 228;
    public final static int  UPSELL_MASTER_NAME_REQUIRED = 229;
    public final static int  UPSELL_MASTER_NAME_MAX = 230;
    public final static int  UPSELL_MASTER_DESCRIPTION_REQUIRED = 231;
    public final static int  UPSELL_MASTER_DESCRIPTION_MAX = 232;    
    public final static int  UPSELL_ASSOC_NAME_REQURIED = 233;
    public final static int  UPSELL_ASSOC_NAME_MAX_ERROR = 234;
    public final static int UPSELL_MIN_ASSOC_SKU_ERROR = 235;
    public final static int UPSELL_SKU_NOT_IN_PRODUCT_DATABASE=236;
    public final static int UPSELL_PRODUCT_HAS_SUBCODES=237;
    public final static int UPSELL_NOVATOR_FLAG_NOT_SET=238;
    public final static int UPSELL_SKU_ALREADY_EXISTS=239;
    public final static int UPSELL_SKU_IS_SUBCODE=240;
    public final static int CPN_SKU_NOT_IN_PRODUCT_DATABASE=241;
    public final static int CPN_SKU_REQUIRED=242;
    public final static int CPN_SKU_MAX_ERROR=243;
    public final static int CPN_REQUIRED=244;
    public final static int CPN_MAX_ERROR=245;
    public final static int CPN_DUPLICATE=246;
    public final static int MASTER_SKU_ON_PRODUCT_PAGE_ERROR=247;
    public final static int START_DATE_REQUIRED = 248;
    public final static int END_DATE_REQUIRED = 249;    
    public final static int START_END_DATE_ERROR = 250;    
    public final static int DELIVERY_METHODS_EXCEPTION = 251;
    
    public final static int PRODUCT_VALIDATION_ERRORS = 252;
    public final static int PRODUCT_MISSING_DESCRIPTIONS = 253;
    public final static int PRODUCT_XML_DOCUMENT_INVALID = 254;
    
    public final static int PRODUCT_LIST_OPTION_REQUIRED = 255;
    
    // For PDB Mass Edit
    public final static int PDB_MASS_EDIT_INVALID_MASTER_SKU = 256;
    public final static int PDB_MASS_EDIT_INVALID_PRODUCT_ID = 257;
    public final static int PDB_MASS_EDIT_MAKE_A_SELECTION = 258;
    public final static int PDB_MASS_EDIT_NO_SEARCH_RESULTS = 259;
    public final static int PDB_MASS_EDIT_ERROR_CREATING_SPREADSHEET = 260;
    public final static int PDB_MASS_EDIT_ERROR_UPLOADING_SPREADSHEET = 261;
    public final static int PDB_MASS_EDIT_ERROR_NO_DATA_TO_UPLOAD = 262;
    public final static int PDB_MASS_EDIT_ERROR_GETTING_FILE_STATUS_DATA = 263;
    public final static int PDB_MASS_EDIT_INVALID_SPREADSHEET_FORMAT = 264;
    public final static int PDB_MASS_EDIT_GET_ERROR_SPREADSHEET_ERROR = 265;
    public final static int PDB_MASS_EDIT_INVALID_FIELD_LENGTH = 266;
    public final static int PDB_MASS_EDIT_REQUIRED_FIELD = 267;
    public final static int PDB_MASS_EDIT_INVALID_FIELD = 268;
    public final static int PDB_MASS_EDIT_IS_ALPHANUMERIC_AND_HYPHENS = 269;
    public final static int PDB_MASS_EDIT_ONLY_APPLIES_TO_FLORAL = 270;
    public final static int PDB_MASS_EDIT_DIM_WEIGHT_ONLY_REQUIRED_FOR_DROPSHIP = 271;
    public final static int PDB_MASS_EDIT_DIM_WEIGHT_REQUIRED_FOR_DROPSHIP = 272;
    public final static int PDB_MASS_EDIT_IS_NUMERIC_AND_DECIMAL = 273;
    public final static int PDB_MASS_EDIT_IS_VALID_STANDARD_PRICE = 274;
    public final static int PDB_MASS_EDIT_NO_INVENTORY_ADDED_FOR_PRODUCT = 275;
    public final static int PDB_MASS_EDIT_FOR_SERVICES_NOT_ALLOWED = 276;
    public final static int PDB_MASS_EDIT_PRICE_BLANK_NOT_ALLOWED = 277;
    public final static int PDB_MASS_EDIT_IS_VALID_DELUXE_OR_PREMIUM_PRICE = 278;
    public final static int PDB_MASS_EDIT_IS_SPACES_ALPHANUMERIC_AND_HYPHENS = 279;
    public final static int PDB_MASS_EDIT_COMMENTS_INVALID_LENGTH= 280;
    public final static int PDB_MASS_EDIT_NO_VENDORS_AVAILABLE= 281;
    public final static int PDB_MASS_EDIT_PQUAD_PRODUCT_ID_INVALID_FIELD = 282;
    public final static int PDB_MASS_EDIT_MERCURY_DESCRIPTION_INVALID_FIELD = 283;
    
    // ************** System Execption Constants ********************
    public final static int OE_BAD_CONNECTION_EXCEPTION = 300;
    public final static int OE_INTERFACE_NOT_FOUND_EXCEPTION = 301;
    public final static int OE_NOVATOR_TRANSMISSION_ERROR = 302;
    public final static int PDB_UNKNOWN_HOST_EXCEPTION = 304;
    public final static int PDB_IO_EXCEPTION = 306;
    public final static int OE_COULD_NOT_ROLLBACK = 307;
    public final static int PDB_DATE_PARSE_EXCEPTION = 308;
    public final static int SEND_EMAIL_ERROR = 309;
    public final static int GENERAL_SYSTEM_EXCEPTION = 310;
    public final static int PDB_NOVATOR_EXCEPTION = 311;
    public final static int IMAGE_SOURCE_EXCEPTION = 312;
    public final static int PDB_INVALID_XML_EXCEPTION = 313;
    public final static int CONFIG_PROPERTY_NOT_FOUND_EXCEPTION = 314;
    public final static int PDB_EXCEPTION_UPDATE_ERROR = 315;
    
    // ************** Database fields
    // *** Lookup Table Definitions *** //
    public final static int OE_VENDOR_MASTER_VENDOR_ID = 1;
    public final static int OE_VENDOR_MASTER_VENDOR_NAME = 2;

    public final static int OE_OCCASION_ID = 1;
    public final static int OE_OCCASION_DESCRIPTION = 2;

    public final static int OE_ADDON_ID = 1;
    public final static int OE_ADDON_DESCRIPTION = 2;

    public final static int OE_COMPANY_ID = 1;

    public final static int OE_PRODUCT_VALUE_ID = 1;
    public final static int OE_PRODUCT_VALUE_DESCRIPTION = 2;
    public final static int OE_PRODUCT_VALUE_MARKER_TYPE = 3;

    public final static int OE_PRODUCT_MASTER_PRODUCT_ID = 1;
    public final static int OE_PRODUCT_MASTER_NOVATOR_ID = 2;
    public final static int OE_PRODUCT_MASTER_PRODUCT_NAME = 3;
    public final static int OE_PRODUCT_MASTER_NOVATOR_NAME = 4;
    public final static int OE_PRODUCT_MASTER_STATUS = 5;
    public final static int OE_PRODUCT_MASTER_DELIVERY_TYPE = 6;
    public final static int OE_PRODUCT_MASTER_CATEGORY = 7;
    public final static int OE_PRODUCT_MASTER_PRODUCT_TYPE = 8;
    public final static int OE_PRODUCT_MASTER_PRODUCT_SUB_TYPE = 9;
    public final static int OE_PRODUCT_MASTER_COLOR_SIZE_FLAG = 10;
    public final static int OE_PRODUCT_MASTER_STANDARD_PRICE = 11;
    public final static int OE_PRODUCT_MASTER_DELUXE_PRICE = 12;
    public final static int OE_PRODUCT_MASTER_PREMIUM_PRICE = 13;
    public final static int OE_PRODUCT_MASTER_PREFERRED_PRICE_POINT = 14;
    public final static int OE_PRODUCT_MASTER_VARIABLE_PRICE_MAX = 15;
    public final static int OE_PRODUCT_MASTER_SHORT_DESCRIPTION = 16;
    public final static int OE_PRODUCT_MASTER_LONG_DESCRIPTION = 17;
    public final static int OE_PRODUCT_MASTER_FLORIST_REFERENCE_NUMBER = 18;
    public final static int OE_PRODUCT_MASTER_MERCURY_DESCRIPTION = 19;
    public final static int OE_PRODUCT_MASTER_ITEM_COMMENTS = 20;
    public final static int OE_PRODUCT_MASTER_ADD_ON_BALLOONS_FLAG = 21;
    public final static int OE_PRODUCT_MASTER_ADD_ON_BEARS_FLAG = 22;
    public final static int OE_PRODUCT_MASTER_ADD_ON_CARDS_FLAG = 23;
    public final static int OE_PRODUCT_MASTER_ADD_ON_FUNERAL_FLAG = 24;
    public final static int OE_PRODUCT_MASTER_CODIFIED_FLAG = 25;
    public final static int OE_PRODUCT_MASTER_EXCEPTION_CODE = 26;
    public final static int OE_PRODUCT_MASTER_EXCEPTION_START_DATE = 27;
    public final static int OE_PRODUCT_MASTER_EXCEPTION_END_DATE = 28;
    public final static int OE_PRODUCT_MASTER_EXCEPTION_MESSAGE = 29;
    public final static int OE_PRODUCT_MASTER_VENDOR_ID = 30;
    public final static int OE_PRODUCT_MASTER_VENDOR_COST = 31;
    public final static int OE_PRODUCT_MASTER_VENDOR_SKU = 32;
    public final static int OE_PRODUCT_MASTER_SECOND_CHOICE = 33;
    public final static int OE_PRODUCT_MASTER_HOLIDAY_SECOND_CHOICE = 34;
    public final static int OE_PRODUCT_MASTER_DROP_SHIP_CODE = 35;
    public final static int OE_PRODUCT_MASTER_DISCOUNT_ALLOWED_FLAG = 36;
    public final static int OE_PRODUCT_MASTER_DELIVERY_INCLUDED_FLAG = 37;
    public final static int OE_PRODUCT_MASTER_TAX_FLAG = 38;
    public final static int OE_PRODUCT_MASTER_SERVICE_FEE_FLAG = 39;
    public final static int OE_PRODUCT_MASTER_EXOTIC_FLAG = 40;
    public final static int OE_PRODUCT_MASTER_EGIFT_FLAG = 41;
    public final static int OE_PRODUCT_MASTER_COUNTRY = 42;
    public final static int OE_PRODUCT_MASTER_ARRANGEMENT_SIZE = 43;
    public final static int OE_PRODUCT_MASTER_ARRANGEMENT_COLORS = 44;
    public final static int OE_PRODUCT_MASTER_DOMINANT_FLOWERS = 45;
    public final static int OE_PRODUCT_MASTER_SEARCH_PRIORITY = 46;
    public final static int OE_PRODUCT_MASTER_RECIPE = 47;
    public final static int OE_PRODUCT_MASTER_SUB_CODE_FLAG = 48;
    public final static int OE_PRODUCT_MASTER_DIM_WEIGHT = 49;
    public final static int OE_PRODUCT_MASTER_NEXT_DAY_UPGRADE_FLAG = 50;
    public final static int OE_PRODUCT_MASTER_CORPORATE_SITE_FLAG = 51;
    public final static int OE_PRODUCT_MASTER_UNSPSC_CODE = 52;
    public final static int OE_PRODUCT_MASTER_PRICE_RANK_1 = 53;
    public final static int OE_PRODUCT_MASTER_PRICE_RANK_2 = 54;
    public final static int OE_PRODUCT_MASTER_PRICE_RANK_3 = 55;
    public final static int OE_PRODUCT_MASTER_SHIP_METHOD_CARRIER = 56;
    public final static int OE_PRODUCT_MASTER_SHIP_METHOD_FLORIST = 57;
    public final static int OE_PRODUCT_MASTER_SHIPPING_KEY = 58;
    public final static int OE_PRODUCT_MASTER_LAST_UPDATE = 59;
    public final static int OE_PRODUCT_MASTER_VARIABLE_PRICE_FLAG = 60;
    public final static int OE_PRODUCT_MASTER_HOLIDAY_SKU = 61;
    public final static int OE_PRODUCT_MASTER_HOLIDAY_PRICE = 62;
    public final static int OE_PRODUCT_MASTER_CATELOG_FLAG = 63;
    public final static int OE_PRODUCT_MASTER_HOLIDAY_DELUXE_PRICE = 64;
    public final static int OE_PRODUCT_MASTER_HOLIDAY_PREMIUM_PRICE = 65;
    public final static int OE_PRODUCT_MASTER_NEW_START_DATE = 66;
    public final static int OE_PRODUCT_MASTER_NEW_END_DATE = 67;
    public final static int OE_PRODUCT_MASTER_MERCURY_SECOND_CHOICE = 68;
    public final static int OE_PRODUCT_MASTER_MERCURY_HOLIDAY_SECOND_CHOICE = 69;
    public final static int OE_PRODUCT_MASTER_ADD_ON_CHOCOLATE_FLAG = 70;
    public final static int OE_PRODUCT_MASTER_JCPENNEY_CATEGORY = 71;
    public final static int OE_PRODUCT_MASTER_CROSS_REF_NOVATOR_ID = 72;
    public final static int OE_PRODUCT_MASTER_GENERAL_COMMENTS = 73;
    public final static int  OE_PRODUCT_MASTER_SENT_TO_NOVATOR_PROD_FLAG =74;
    public final static int  OE_PRODUCT_MASTER_SENT_TO_NOVATOR_UAT_FLAG =75;
    public final static int  OE_PRODUCT_MASTER_SENT_TO_NOVATOR_TEST_FLAG =76;
    public final static int  OE_PRODUCT_MASTER_SENT_TO_NOVATOR_CONTENT_FLAG =77;
    public final static int  OE_PRODUCT_MASTER_DEFAULT_CARRIER =78;
    public final static int  OE_PRODUCT_MASTER_HOLD_UNTIL_AVAILABLE =79;
    public final static int  OE_PRODUCT_MASTER_LAST_UPDATE_USER_ID =80;
    public final static int  OE_PRODUCT_MASTER_LAST_UPDATE_SYSTEM =81;
    public final static int  OE_PRODUCT_MASTER_MONDAY_DELIVERY_FRESHCUT = 82;
    public final static int  OE_PRODUCT_MASTER_TWO_DAY_SHIP_SAT_FRESHCUT = 83;
    public final static int  OE_PRODUCT_MASTER_WEBOE_BLOCKED = 84;
    public final static int  OE_PRODUCT_MASTER_EXPRESS_SHIPPING = 85;
    public final static int  OE_PRODUCT_MASTER_OVER_21 = 86;
    public final static int  OE_PRODUCT_MASTER_SHIPPING_SYSTEM = 87;
    public final static int  OE_PRODUCT_MASTER_PERSONALIZATION_TEMPLATE = 88;
    public final static int  OE_PRODUCT_MASTER_PERSONALIZATION_LEAD_DAYS = 89;
    public final static int  OE_PRODUCT_MASTER_DEPARTMENT_CODE = 90;
    public final static int  OE_PRODUCT_MASTER_PERSONALIZATION_TEMPLATE_ORDER = 91;
    public final static int  OE_PRODUCT_MASTER_ZONE_JUMP_ELIGIBLE_FLAG = 92;
    public final static int  OE_PRODUCT_MASTER_BOX_ID = 93;
    public final static int  OE_PRODUCT_MASTER_CUSTOM_FLAG = 94;
    public final static int  OE_PRODUCT_MASTER_KEYWORDS = 95;
    public final static int  OE_PRODUCT_MASTER_SUPPLY_EXPENSE = 96;
    public final static int  OE_PRODUCT_MASTER_SUPPLY_EXPENSE_EFF_DATE = 97;
    public final static int  OE_PRODUCT_MASTER_ROYALTY_PERCENT = 98;
    public final static int  OE_PRODUCT_MASTER_ROYALTY_PERCENT_EFF_DATE = 99;
    public final static int  OE_PRODUCT_MASTER_GBB_POPOVER_FLAG = 100;
    public final static int  OE_PRODUCT_MASTER_GBB_TITLE = 101;
    public final static int  OE_PRODUCT_MASTER_GBB_NAME_OVERRIDE_FLAG_1 = 102;
    public final static int  OE_PRODUCT_MASTER_GBB_NAME_OVERRIDE_TEXT_1 = 103;
    public final static int  OE_PRODUCT_MASTER_GBB_PRICE_OVERRIDE_FLAG_1 = 104;
    public final static int  OE_PRODUCT_MASTER_GBB_PRICE_OVERRIDE_TEXT_1 = 105;
    public final static int  OE_PRODUCT_MASTER_GBB_NAME_OVERRIDE_FLAG_2 = 106;
    public final static int  OE_PRODUCT_MASTER_GBB_NAME_OVERRIDE_TEXT_2 = 107;
    public final static int  OE_PRODUCT_MASTER_GBB_PRICE_OVERRIDE_FLAG_2 = 108;
    public final static int  OE_PRODUCT_MASTER_GBB_PRICE_OVERRIDE_TEXT_2 = 109;
    public final static int  OE_PRODUCT_MASTER_GBB_NAME_OVERRIDE_FLAG_3 = 110;
    public final static int  OE_PRODUCT_MASTER_GBB_NAME_OVERRIDE_TEXT_3 = 111;
    public final static int  OE_PRODUCT_MASTER_GBB_PRICE_OVERRIDE_FLAG_3 = 112;
    public final static int  OE_PRODUCT_MASTER_GBB_PRICE_OVERRIDE_TEXT_3 = 113;
    public final static int  OE_PRODUCT_MASTER_SEND_STANDARD_RECIPE = 115;
    public final static int  OE_PRODUCT_MASTER_DELUXE_RECIPE = 116;
    public final static int  OE_PRODUCT_MASTER_SEND_DELUXE_RECIPE = 117;
    public final static int  OE_PRODUCT_MASTER_PREMIUM_RECIPE = 118;
    public final static int  OE_PRODUCT_MASTER_SEND_PREMIUM_RECIPE = 119;
    public final static int  OE_PRODUCT_MASTER_PERSONAL_GREETING_FLAG = 120;
    public final static int  OE_PRODUCT_MASTER_PREMIER_COLLECTION_FLAG = 121;
    //public final static int  OE_PRODUCT_MASTER_ADD_ON_FREE_ID = 122;
    public final static int  OE_PRODUCT_MASTER_SERVICE_DURATION = 122;
    public final static int  OE_PRODUCT_MASTER_ALLOW_FREE_SHIPPING_FLAG = 123;
    public static final String OE_PRODUCT_MASTER_MORNING_DELIVERY_FLAG = "morningDeliveryFlag";
    
    public static final int OE_PRODUCT_MASTER_PQUAD_PRODUCT_ID=125;

    public final static int  PDB_UPSELLDETAIL_BYID_MASTER_ID = 1;
    public final static int  PDB_UPSELLDETAIL_BYID_DETAIL_ID = 2;
    public final static int  PDB_UPSELLDETAIL_BYID_SEQUENCE = 3;
    public final static int  PDB_UPSELLDETAIL_BYID_NAME = 4;
    public final static int  PDB_UPSELLDETAIL_BYID_MASTERNAME = 5;
    public final static int PDB_UPSELLDETAIL_BYID_GBB_NAME_OVERRIDE_FLAG = 6;
    public final static int PDB_UPSELLDETAIL_BYID_GBB_NAME_OVERRIDE_TEXT = 7;
    public final static int PDB_UPSELLDETAIL_BYID_GBB_PRICE_OVERRIDE_FLAG = 8;
    public final static int PDB_UPSELLDETAIL_BYID_GBB_PRICE_OVERRIDE_TEXT = 9;
    public final static int PDB_UPSELLDETAIL_BYID_DEFAULT_SKU_FLAG = 10;
    
    public final static int OE_HOLIDAY_PRICING_START_DATE = 1;
    public final static int OE_HOLIDAY_PRICING_END_DATE = 2;
    public final static int OE_HOLIDAY_PRICING_FLAG = 3;
    public final static int OE_HOLIDAY_PRICING_DELIVERY_DATE_FLAG = 4;

    public final static int OE_COUNTRY_ID = 1;
    public final static int OE_COUNTRY_NAME = 2;
    public final static int OE_COUNTRY_TYPE = 3;
    public final static int OE_COUNTRY_DISPLAY_ORDER = 4;

    public final static int OE_PRODUCT_LIST_ID = 1;
    public final static int OE_PRODUCT_LIST_NAME = 2;
    public final static int OE_PRODUCT_LIST_STATUS = 3;
    public final static int OE_PRODUCT_LIST_START = 4;
    public final static int OE_PRODUCT_LIST_END = 5;
    public final static int OE_PRODUCT_LIST_HOLD_UNTIL_AVAILABLE = 6;

    public final static int PDB_PRODUCT_SHIPPING_RESTRICTIONS_ID = 1;
    public final static int PDB_PRODUCT_SHIPPING_RESTRICTIONS_START = 2;
    public final static int PDB_PRODUCT_SHIPPING_RESTRICTIONS_END = 3;

    public final static int PDB_PRODUCT_KEYWORDS_ID = 1;
    public final static int PDB_PRODUCT_KEYWORDS_KEYWORD = 2;

    public final static int PDB_PRODUCT_RECIPIENT_PRODUCT_ID = 1;
    public final static int PDB_PRODUCT_RECIPIENT_ID = 2;

    public final static int PDB_PRODUCT_COMPANY_PRODUCT_ID = 1;
    public final static int PDB_PRODUCT_COMPANY_COMPANY_ID = 2;

    public final static int PDB_UPSELL_COMPANY_PRODUCT_ID = 1;
    public final static int PDB_UPSELL_COMPANY_COMPANY_ID = 2;

    public final static int PDB_PRODUCT_EXCLUDE_STATE_PRODUCT_ID = 1;
    public final static int PDB_PRODUCT_EXCLUDE_ID = 2;

    public final static int PDB_REMOVE_FROM_EXCLUDED_STATES_ID = 1;
    public final static int PDB_REMOVE_FROM_RECIPIENT_SEARCH_ID = 1;
    public final static int PDB_REMOVE_FROM_PRODUCT_COMPANY_PRODUCT_ID = 1;
    public final static int PDB_REMOVE_FROM_UPSELL_COMPANY_PRODUCT_ID = 1;

    public final static int PDB_REMOVE_SHIP_METHODS_ID = 1;
    public final static int PDB_PRODUCT_SHIP_METHODS_PRODUCT_ID = 1;
    public final static int PDB_PRODUCT_SHIP_METHODS_SHIPPING_ID = 2;
    public final static int PDB_PRODUCT_SHIP_METHODS_CARRIER = 3;

    public final static int PRODUCT_HOLIDAY_PRICE_PRODUCT_ID = 1;
    public final static int PRODUCT_HOLIDAY_PRICE_NOVATOR_NAME = 2;
    public final static int PRODUCT_HOLIDAY_PRICE_HOLIDAY_PRICE = 3;
    public final static int PRODUCT_HOLIDAY_PRICE_HOLIDAY_SKU = 4;    

    public final static int PDB_PRODUCT_SHIP_DATES_PRODUCT_ID = 1;
    public final static int PDB_PRODUCT_SHIP_DATES_MONDAY = 2;
    public final static int PDB_PRODUCT_SHIP_DATES_TUESDAY = 3;
    public final static int PDB_PRODUCT_SHIP_DATES_WEDNESDAY = 4;
    public final static int PDB_PRODUCT_SHIP_DATES_THURSDAY = 5;
    public final static int PDB_PRODUCT_SHIP_DATES_FRIDAY = 6;
    public final static int PDB_PRODUCT_SHIP_DATES_SATURDAY = 7;
    public final static int PDB_PRODUCT_SHIP_DATES_SUNDAY = 8;

    public final static int PDB_REMOVE_PRODUCT_SHIP_DATES_ID = 1;

    public final static int PDB_PRODUCT_SUBCODE_PRODUCT_ID = 1;
    public final static int PDB_PRODUCT_SUBCODE_SUBCODE_ID = 2;
    public final static int PDB_PRODUCT_SUBCODE_DESCRIPTION = 3;
    public final static int PDB_PRODUCT_SUBCODE_PRICE = 4;
    public final static int PDB_PRODUCT_SUBCODE_HOLIDAY_SKU = 5;
    public final static int PDB_PRODUCT_SUBCODE_HOLIDAY_PRICE = 6;
    public final static int PDB_PRODUCT_SUBCODE_AVAILABLE = 7;
    public final static int PDB_PRODUCT_SUBCODE_REF_NUMBER = 8;
    public final static int PDB_PRODUCT_SUBCODE_VENDOR_PRICE = 9;    
    public final static int PDB_PRODUCT_SUBCODE_VENDOR_SKU = 10;    
    public final static int PDB_PRODUCT_SUBCODE_DIM_WEIGHT = 11;
    public final static int PDB_PRODUCT_SUBCODE_DISPLAY_ORDER = 12;
    public final static int PDB_PRODUCT_SUBCODE_LIST_VENDOR_SKU = 7;    
    public final static int PDB_PRODUCT_SUBCODE_LIST_DIM_WEIGHT = 8;        
    public final static int PDB_REMOVE_PRODUCT_SUBCODE_PRODUCT_ID = 1;

    public final static int OE_PRODUCT_SUBTYPE_ID = 1;
    public final static int OE_PRODUCT_SUBTYPE_DESCRIPTION = 2;
    public final static int OE_PRODUCT_TYPE_ID = 3;

    public final static int GET_PRODUCT_SHIP_METHODS = 1;
    public final static int GET_PRODUCT_SHIP_METHOD_CARRIER = 2;
    public final static int GET_PRODUCT_SHIP_METHOD_NOVATOR_TAG = 3;
   
    public final static int PDB_PRODUCT_HP_PRODUCT_ID = 1;
//    public final static int PDB_PRODUCT_HP_DEL_RESTRICTIONS_START = 2;
//    public final static int PDB_PRODUCT_HP_SHIP_RESTRICTIONS_START = 3;
    public final static int PDB_PRODUCT_HP_SHIP_DATES = 2;
    public final static int PDB_PRODUCT_HP_COLORS = 3;
    public final static int PDB_PRODUCT_HP_SUBCODES = 4;
    public final static int PDB_PRODUCT_HP_EXCLUDED_STATES = 5;
    public final static int PDB_REMOVE_PRODUCT_HP_ID = 1;

    public final static int PDB_REMOVE_PRODUCT_COLORS_PRODUCT_ID = 1;
    public final static int PDB_PRODUCT_COLORS_PRODUCT_ID = 1;
    public final static int PDB_PRODUCT_COLORS_ID = 2;
    public final static int PDB_PRODUCT_COLORS_ORDERBY = 3;

    public final static int SEND_MERCURY_RECIPE_FLAG = 1;

    public final static int SECOND_CHOICE_ID = 1;
    public final static int SECOND_CHOICE_DESCRIPTION = 2;
    public final static int SECOND_CHOICE_MERCURY_DESCRIPTION = 3;

    //upsell tables
    public final static int PDB_UPSELLMASTER_MASTER_ID = 1;
    public final static int PDB_UPSELLMASTER_MASTER_NAME = 2;
    public final static int PDB_UPSELLMASTER_MASTER_DESCRIPTION = 3;
    public final static int PDB_UPSELLMASTER_MASTER_STATUS = 4;
    public final static int PDB_UPSELLMASTER_SEARCH_PRIORITY  = 5;
    public final static int PDB_UPSELLMASTER_CORPORATE_SITE   = 6;    
    public final static int PDB_UPSELLMASTER_GBB_POPOVER_FLAG = 7;
    public final static int PDB_UPSELLMASTER_GBB_TITLE = 8;
    public final static int PDB_UPSELLDETAIL_DETAIL_ID = 1;
    public final static int PDB_UPSELLDETAIL_SEQUENCE = 2;
    public final static int PDB_UPSELLDETAIL_NAME = 3;
    public final static int PDB_UPSELLDETAIL_TYPE = 4;
    public final static int PDB_UPSELLDETAIL_PRICE = 5;
    public final static int PDB_UPSELLDETAIL_AVAILABLE = 6;
    public final static int PDB_UPSELLDETAIL_NOVATOR_ID = 7;
    public final static int PDB_UPSELLDETAIL_SENT_TO_NOVATOR_PROD = 8;
    public final static int PDB_UPSELLDETAIL_SENT_TO_NOVATOR_CONTENT = 9;
    public final static int PDB_UPSELLDETAIL_SENT_TO_NOVATOR_UAT = 10;
    public final static int PDB_UPSELLDETAIL_SENT_TO_NOVATOR_TEST = 11;
    public final static int PDB_UPSELLDETAIL_GBB_NAME_OVERRIDE_FLAG = 12;
    public final static int PDB_UPSELLDETAIL_GBB_NAME_OVERRIDE_TEXT = 13;
    public final static int PDB_UPSELLDETAIL_GBB_PRICE_OVERRIDE_FLAG = 14;
    public final static int PDB_UPSELLDETAIL_GBB_PRICE_OVERRIDE_TEXT = 15;
    public final static int PDB_UPSELLDETAIL_GBB_SEQUENCE = 16;
    public final static int PDB_UPSELLDETAIL_DEFAULT_SKU_FLAG = 17;
    
    // ************** Variables used in XML transmissions to Novator *********************
    public final static String NOVATOR_DATEFORMAT = "yyyyMMdd";
    public final static String NOVATOR_BOOLEAN_TRUE = "true";
    public final static String NOVATOR_BOOLEAN_FALSE = "false";
    public final static String NOVATOR_STARTDATE = "start_date";
    public final static String NOVATOR_ENDDATE = "end_date";
    public final static String NOVATOR_HOLIDAYPRICEFLAG = "switch";
    public final static String NOVATOR_DELIVERYDATEFLAG = "delivery_date_flag";
    public final static String NOVATOR_HOLIDAYPRICINGVO = "holiday_pricing";
    public final static String NOVATOR_XML_FOOTER = "</XML>";
    public final static String NOVATOR_XML_HOLIDAY_PRICING_HEADER =
                "<?xml version='1.0'?>";
//              + " <!-- filename = product_holiday_pricing.xml -->"
//              + " <!DOCTYPE product SYSTEM 'product_holiday_pricing.dtd'>";
    public final static String NOVATOR_XML_HEADER = "<XML>";

    public final static String NOVATOR_XML_UPSELL_HEADER =  "<?xml version='1.0'?>";
    public final static String NOVATOR_XML_CPN_HEADER =  "<?xml version='1.0'?>";

    public final static String NOVATOR_XML_PRODUCT_HEADER =
                "<?xml version='1.0'?>";
//              + " <!-- filename = product.xml -->"
//              + " <!DOCTYPE product SYSTEM 'product.dtd'>";

    public final static String NOVATOR_XML_SHIPPING_KEY_HEADER = "<?xml version='1.0'?>";
//              + "<!-- filename = shipping_key.xml -->"
//              + " <!DOCTYPE product SYSTEM 'shipping_key.dtd'>";      

    public final static String PDB_NOVATOR_FIELD_NAME_TITLE = "product.title";
    public final static String PDB_NOVATOR_FIELD_NAME_LONG_DESCRIPTION = "product.long_description";
    public final static String PDB_NOVATOR_FIELD_NAME_TYPE = "product.type";
    public final static String PDB_NOVATOR_FIELD_NAME_SUB_TYPE = "product.sub_type";
    public final static String PDB_NOVATOR_FIELD_NAME_VENDOR_ID = "product.vendor_id";
    public final static String PDB_NOVATOR_FIELD_NAME_SHIPPING_KEY = "product.shipping_key";
    public final static String PDB_NOVATOR_FIELD_NAME_VALID_SHIP_DAYS = "product.valid_ship_days";
    public final static String PDB_NOVATOR_FIELD_NAME_DELIVERY_OPTIONS = "product.delivery_options";
    public final static String PDB_NOVATOR_FIELD_NAME_SUB_PRODUCT_ID_OPTION = "sub_product_id.option";
    public final static String PDB_NOVATOR_FIELD_NAME_SUB_PRODUCT_ID_PRICE = "sub_product_id.price";
    public final static String PDB_NOVATOR_FIELD_NAME_PRODUCT_ID = "product.product_id";
    public final static String PDB_NOVATOR_FIELD_NAME_SHIPPING_METHOD = "product.shipping_method";
    public final static String PDB_NOVATOR_FIELD_NAME_ADD_ON_FLAG = "product.add_on_flag";   

    public final static String PDB_SHIPCHECKBOX = "shipcheckbox";    
    public final static String PDB_SHIPCARRIER="shipcarrier";
    public final static String PDB_NOVATORTAG="novatortag";
    public final static String PDB_SUBMIT_TYPE = "submitType";    
    public final static String PDB_LIST = "list";    
    public final static String PDB_EDIT = "edit";    
    public final static String PDB_PQUAD_PRODUCT_SUBMIT = "submit";
    public final static String PDB_EDIT_BATCH = "editBatch";    
    public final static String PDB_BATCH_REMOVE = "batchRemove";    
    public final static String PDB_BATCH_SUBMIT_SELECTED = "batchSubmitSelected";    
    public final static String PDB_BATCH_SUBMIT_ALL = "batchSubmitAll";    
    public final static String PDB_PRODUCT_BATCH = "productBatch";    
    public final static String PDB_BATCH_STATUS_READY = "READY";    
    public final static String PDB_BATCH_STATUS_ERROR = "ERROR";    

    public final static String PDB_NOVATOR_UPSELL_FIELD_NAME_TITLE = "upsell_master.upsell_name";
    public final static String PDB_NOVATOR_UPSELL_FIELD_NAME_DESCRIPTION = "upsell_master.upsell_description";
    public final static String PDB_NOVATOR_UPSELL_FIELD_NAME_DETAIL_ID = "upsell_master.upsell_detail_id";
    public final static String PDB_NOVATOR_UPSELL_FIELD_NAME_DETAIL_TITLE = "upsell_master.upsell_detail_name";
    public final static String PDB_NOVATOR_UPSELL_FIELD_NAME_DETAIL_PRICE = "upsell_master.upsell_price";
    public final static String PDB_NOVATOR_UPSELL_FIELD_NAME_DETAIL_TYPE = "upsell_master.upsell_type";
    public final static String PDB_NOVATOR_UPSELL_FIELD_NAME_DETAIL_STATUS = "upsell_master.upsell_status";
    public final static String PDB_NOVATOR_UPSELL_FIELD_NAME_DETAIL = "upsell_master.upsell_detail";
    public final static String PDB_NOVATOR_UPSELL_FIELD_NAME_MASTER_ID = "upsell_master.upsell_master_id";

	/** error reporting for product maintenance **/
    public final static int PDB_PRODUCT_MAINT_ERROR_IDX_PRODUCT_ID = 0;
    public final static int PDB_PRODUCT_MAINT_ERROR_IDX_PRODUCT_NAME = 1;
    public final static int PDB_PRODUCT_MAINT_ERROR_IDX_NOVATOR_ID = 2;
    public final static int PDB_PRODUCT_MAINT_ERROR_IDX_NOVATOR_NAME = 3;
    public final static int PDB_PRODUCT_MAINT_ERROR_IDX_PRODUCT_CATAGORY = 4;
    public final static int PDB_PRODUCT_MAINT_ERROR_IDX_PRODUCT_TYPE = 5;
    public final static int PDB_PRODUCT_MAINT_ERROR_IDX_PRODUCT_SUB_TYPE = 6;
    public final static int PDB_PRODUCT_MAINT_ERROR_IDX_PRODUCT_DELIVERY_TYPE = 7;
    public final static int PDB_PRODUCT_MAINT_ERROR_IDX_PRODUCT_FLORIST_REFERENCE = 8;
    public final static int PDB_PRODUCT_MAINT_ERROR_IDX_PRODUCT_MERCURY_DESCRIPTION = 9;
    public final static int PDB_PRODUCT_MAINT_ERROR_IDX_PRODUCT_SECOND_CHOICE = 10;
    public final static int PDB_PRODUCT_MAINT_ERROR_IDX_PRODUCT_LONG_DESCRIPTION = 11;
    public final static int PDB_PRODUCT_MAINT_ERROR_IDX_PRODUCT_ARRNG_COLOR_CHOICE = 12;
    public final static int PDB_PRODUCT_MAINT_ERROR_IDX_PRODUCT_USE_COLOR = 13;
    public final static int PDB_PRODUCT_MAINT_ERROR_IDX_PRODUCT_DIM_WEIGHT = 14;
    public final static int PDB_PRODUCT_MAINT_ERROR_IDX_PRODUCT_VENDOR_COST = 15;
    public final static int PDB_PRODUCT_MAINT_ERROR_IDX_PRODUCT_VENDOR_LIST = 16;
    public final static int PDB_PRODUCT_MAINT_ERROR_IDX_PRODUCT_SHIPPING_KEY = 17;
    public final static int PDB_PRODUCT_MAINT_ERROR_IDX_PRODUCT_DELIVERY_METHOD = 18;
    public final static int PDB_PRODUCT_MAINT_ERROR_IDX_PRODUCT_DELIVERY_OPTION = 19;
    public final static int PDB_PRODUCT_MAINT_ERROR_IDX_PRODUCT_SHIP_DAY = 20;
    public final static int PDB_PRODUCT_MAINT_ERROR_IDX_PRODUCT_PRICE = 21;
    public final static int PDB_PRODUCT_MAINT_ERROR_IDX_PRODUCT_STANDARD_RANK = 22;
    public final static int PDB_PRODUCT_MAINT_ERROR_IDX_PRODUCT_DELUXE_RANK = 23;
    public final static int PDB_PRODUCT_MAINT_ERROR_IDX_PRODUCT_COLOR_SIZE = 24;
    public final static int PDB_PRODUCT_MAINT_ERROR_IDX_PRODUCT_SHORT_DESCRIPTION = 25;
    public final static int PDB_PRODUCT_MAINT_ERROR_IDX_PRODUCT_HOLIDAY_PRICE = 26;
    public final static int PDB_PRODUCT_MAINT_ERROR_IDX_PRODUCT_SUBPRICE_EQUAL = 27;
    public final static int PDB_PRODUCT_MAINT_ERROR_IDX_PRODUCT_SUBCODE_UNIQUE = 28;
    public final static int PDB_PRODUCT_MAINT_ERROR_IDX_PRODUCT_ADD_ON_PRODUCT = 29;
    public final static int PDB_PRODUCT_MAINT_ERROR_IDX_COMPANY = 30;
    public final static int PDB_PRODUCT_MAINT_ERROR_IDX_PRODUCT_COMMENT = 31;
    //public final static int PCB_PRODUCT_MAINT_ERROR_MAX_IDX = 32;

    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_ID = "A product id is required.";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_NAME = "A product name is required.";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_NOVATOR_ID = "A novator id is required.";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_NOVATOR_ID_BAD_CHARS = "Only characters, numbers, and dashes are allowed";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_NOVATOR_NAME = "A novator name is required.";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_COMPANY = "At least one company is required.";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_CATAGORY = "A category is required.";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_DEPARTMENT_CODE = "A department code is required.";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_TYPE = "A type is required.";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_SUB_TYPE = "A subtype is required.";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_DELIVERY_TYPE = "A delivery type is required.";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_FLORIST_REFERENCE = "A florist reference is required.";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_MERCURY_DESCRIPTION = "A mercury description is required.";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_SECOND_CHOICE = "A second choice is required.";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_LONG_DESCRIPTION = "A long description is required.";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_ARRNG_COLOR_CHOICE = "A color choice is required.";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_USE_COLOR = "A color use is required.";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_DIM_WEIGHT = "A dimension or weight is required.";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_VENDOR_COST = "A vendor cost is required.";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_VENDOR_LIMIT = "Vendor limit for this product has been reached. \n Please remove one(or more) before trying to add a new vendor.";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_VENDOR_LIST = "Select a vendor.";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_SHIPPING_KEY = "A shipping key is required.";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_DELIVERY_METHOD = "A delivery method is required.";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_DELIVERY_METHOD_ONE = "Only one delivery method is allowed.";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_DELIVERY_OPTION = "A shipping option is required.";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_SHIP_DAY = "A ship day is required.";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_PRICE = "A price is required.";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_STANDARD_RANK = "Standard cannot be equal to deluxe or premium.";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_DELUXE_RANK = "Deluxe cannot be equal to premium.";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_COLOR_SIZE = "Deluxe and premium price cannot have values greater than zero when use color is selected.";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_SHORT_DESC_MAX = "The product short description cannot be greater than 80 characters.";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_LONG_DESC_MAX = "The product long description cannot be greater than 1024 characters.";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_HOLIDAY_PRICE = "All holiday pricing fields must be filled.";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_HOLIDAY_SUBPRICE = "All holiday pricing subcode fields must be filled";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_SUBPRICE_EQUAL = "All subcode prices must be the same.";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_PRODUCT_SUBCODE_UNIQUE = "All subcodes must be unique.";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_NAME_MAX = "The product name cannot exceed " + PDB_PRODUCT_NAME_MAX + " characters.";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_COMMENT_MAX = "The general comments cannot exceed " + PDB_GENERAL_COMMENT_MAX + " characters.";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_ITEM_COMMENT_MAX = "The product comments cannot exceed " + PDB_ITEM_COMMENT_MAX + " characters.";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_VARIABLE_PRICE_REQUIRED = "A positive variable price is required.";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_EX_START_DATE_REQUIRED = "A valid start date is required.";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_EX_END_DATE_REQUIRED = "A valid end date is required.";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_EX_START_BEFORE_END_DATE = "End date is before the start date.";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_EX_NO_EXCEPTION_DATE_TYPE = "Type is required.";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_RECIPE_MAX = "The product recipe cannot exceed " + RECIPE_LENGTH_MAX + " characters.";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_TEMPLATE_ORDER = "Please select a template order";
    public final static String PDB_PRODUCT_MAINT_NEW_PRODUCT="You have entered a product id that does not exist.  Please fill in the information below to add the product.";            
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_BOX_ID = "Please select a box type";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_SUPPLY_EXPENSE = "Supplies Expense must be a positive number";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_SUPPLY_EXPENSE_EFF_DATE = "The Supply Expense Effective Date is required if the Supply Expense is populated";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_ROYALTY_PERCENT = "The Royalty Percent must be a number between 0 and 100";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_ROYALTY_PERCENT_EFF_DATE = "The Royalty Percent Effective Date is required if the Royalty Percent is populated";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_RETIRED_SUBCODES = "Subcodes have been retired.  Please make product unavailable.";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_SEND_STANDARD_RECIPE = "Send Standard Recipe flag cannot be checked if Standard Recipe is blank";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_SEND_DELUXE_RECIPE = "Send Deluxe Recipe flag cannot be checked if Deluxe Recipe is blank";
    public final static String PDB_PRODUCT_MAINT_ERROR_STRING_SEND_PREMIUM_RECIPE = "Send Premium Recipe flag cannot be checked if Premium Recipe is blank";
    public final static String PDB_PRODUCT_ADD_ON_ERROR_STRING_DUPLICATE_SEQUENCE = "AAA has a duplicate Display Order of BBB assigned.  Enter a unique number from "+ PRODUCT_ADDON_MIN_DISPLAY_SEQUENCE_NUMBER + "-" + PRODUCT_ADDON_MAX_DISPLAY_SEQUENCE_NUMBER + ".";
    public final static String PDB_PRODUCT_ADD_ON_ERROR_STRING_MISSING_SEQUENCE = "AAA is missing a Display Order.  Enter a unique number from "+ PRODUCT_ADDON_MIN_DISPLAY_SEQUENCE_NUMBER + "-" + PRODUCT_ADDON_MAX_DISPLAY_SEQUENCE_NUMBER + ".";
    public final static String PDB_PRODUCT_ADD_ON_ERROR_STRING_MISSING_MAX_QUANTITY = "AAA is missing a Max Quantity. Enter a number from "+ PRODUCT_ADDON_MIN_MAX_QUANTITY+ "-" + PRODUCT_ADDON_MAX_MAX_QUANTITY + ".";
    public final static String PDB_PRODUCT_ADD_ON_ERROR_STRING_INVALID_SEQUENCE = "AAA needs a valid Display Order.  Enter a unique number from "+ PRODUCT_ADDON_MIN_DISPLAY_SEQUENCE_NUMBER + "-" + PRODUCT_ADDON_MAX_DISPLAY_SEQUENCE_NUMBER + ".";
    public final static String PDB_PRODUCT_ADD_ON_ERROR_STRING_INVALID_MAX_QUANTITY = "AAA needs a valid Max Quantity.  Enter a number from "+ PRODUCT_ADDON_MIN_MAX_QUANTITY+ "-" + PRODUCT_ADDON_MAX_MAX_QUANTITY + ".";
    
    public final static String PDB_PRODUCT_MAINT_TIP_NOVATOR_ID = "If the Novator ID is changed, a new product will be created.  If an incorrect Novator ID is submitted, mark the product as unavailable, submit it, and then correct the ID and submit it as available.";
    public final static String PDB_PRODUCT_MAINT_TIP_LONG_DESCRIPTION = "Please use only &amp;#153; and &amp;reg; for the trademark and register symbols.  The use of additional HTML codes will cause problems.  HTML tags entered within brackets (&lt;&gt;) may also be used.";
    public final static String PDB_PRODUCT_MAINT_BULLET_DESC_TIP_LONG_DESCRIPTION = "Please use '|' delimiter between each bullet item (leading and trailing '|' not required).\nDo not use any HTML tags or special characters.";
    public final static String PDB_PRODUCT_MAINT_TIP_SIZE = "eg. 24H x 10W x 5L (do not use a quote sign (&quot;) as short form for inch)";
    public final static String PDB_PRODUCT_MAINT_TIP_DOMINANT_FLOWER = "Only use flower names, spaces and commas in this field";
    public final static String PDB_PRODUCT_MAINT_TIP_COMMENT = "Internal use only and will not be transmitted to Novator.";
        public final static String PDB_PRODUCT_MAINT_TIP_NOVATOR_UPDATE = "Each Novator database that is selected will be updated";
    public final static String PDB_PRODUCT_MAINT_TIP_AVAILABLE = "When checked, this product is available and will be displayed";
    public final static String PDB_PRODUCT_MAINT_TIP_WEBOE_BLOCKED = "When checked, this product will not be sold on web order entry";
    public final static String PDB_PRODUCT_MAINT_TIP_CORP_SITE = "When checked, this product will be displayed on the corporate web site";
    public final static String PDB_PRODUCT_MAINT_TIP_EXCEPTION_TYPE = "Only products marked as Available will be sent to Novator";
    public final static String PDB_PRODUCT_MAINT_TIP_DATE_FORMAT = "Use the popup calendar to choose a date.";
    public final static String PDB_PRODUCT_MAINT_TIP_EXCEPTION_REASON = "Not sent to Novator. For WebOE/Apollo only";
    public final static String PDB_PRODUCT_MAINT_TIP_HOLD_UNTIL_AVAILABLE = "Orders will be sent to the florist 14 days before delivery when checked";
    public final static String PDB_PRODUCT_MAINT_TIP_DELIVERY_TYPE = "When International is selected, the product is only available outside North America and same day delivery is not available";
    public final static String PDB_PRODUCT_MAINT_TIP_MONEY = "Do not use a dollar sign or commas";
    public final static String PDB_PRODUCT_MAINT_TIP_VARIABLE_PRICING = "To have variable pricing for a product, both fields must be filled in.";
    public final static String PDB_PRODUCT_MAINT_TIP_FREE_SHIP_ELIGIBILITY = "When checked, this product is eligible to be used with the free shipping program.";
    public final static String PDB_PRODUCT_MAINT_TIP_DISCOUNT_ALLOWED = "If this box is checked, then this product can be discounted.  This will affect the prices of partner sites that offer discounts.";
    public final static String PDB_PRODUCT_MAINT_TIP_NO_TAX = "If this box is checked, then sales tax will not be applied to this product";
    public final static String PDB_PRODUCT_MAINT_TIP_SUBCODES = "If a subcode is changed, a new product will be created. Availability is driven off of availability at the vendor level. If an incorrect subcode is submitted, mark the subcode as unavailable at the vendor level, submit it, and then correct the ID and submit it as available.";
    public final static String PDB_PRODUCT_MAINT_TIP_EXPRESS_ONLY = "Selecting this item will force the item to be shipped by \"Air\" ship methods only.";
    public final static String PDB_PRODUCT_MAINT_TIP_MONDAY_FRECUT = "Allows item to be delivered on Monday unless prevented by the \"Delivery Day Exclusion\".  This only applies to fresh cut products";
    public final static String PDB_PRODUCT_MAINT_TIP_PERCENT = "Enter a number between 0 and 100 without the % sign";
    public final static String PDB_PRODUCT_MAINT_TIP_WEBSITES = "Use space or line delimiter to separate source codes.";
    
    /** error reporting for upsell maintenance **/
    public final static int PDB_UPSELL_MAINT_ERROR_IDX_NAME = 0;
    public final static int PDB_UPSELL_MAINT_ERROR_IDX_DESCRIPTION = 1;
    public final static int PDB_UPSELL_MAINT_ERROR_IDX_DETAIL = 2;
    public final static int PDB_UPSELL_MAINT_ERROR_IDX_MASTER_ID = 3;
    public final static int PDB_UPSELL_MAINT_ERROR_IDX_COMPANY = 4;
    public final static int PCB_UPSELL_MAINT_ERROR_MAX_IDX = 5;
 
    public final static String PDB_UPSELL_MAINT_ERROR_STRING_NAME_MISSING = "A product name is required.";
    public final static String PDB_UPSELL_MAINT_ERROR_STRING_NAME_MAX = "The product name cannot exceed " + UPSELL_PRODUCT_NAME_MAX + " characters.";
    public final static String PDB_UPSELL_MAINT_ERROR_STRING_DESCRIPTION_MISSING = "A product description is required.";
    public final static String PDB_UPSELL_MAINT_ERROR_STRING_DESCRIPTION_MAX = "The description cannot exceed 1024 characters.";
    public final static String PDB_UPSELL_MAINT_ERROR_STRING_DETAIL_MIN = "A minimum of two associated SKUs is required.";
    public final static String PDB_UPSELL_MAINT_ERROR_STRING_DETAIL_MAX = "A maximum of six associated SKUs is required.";
    public final static String PDB_UPSELL_MAINT_ERROR_ASSOC_SKU_NAME_MISSING = "Associated SKU name is required.";
    public final static String PDB_UPSELL_MAINT_ERROR_ASSOC_SKU_NAME_MAX = "Cannot exceed " + UPSELL_ASSOC_NAME_MAX + " characters.";
    public final static String PDB_UPSELL_MAINT_ERROR_STRING_COMPANY = "At least one company is required.";
    
    public final static String VPO_MODE_GET_VENDOR_PRODUCT_LIST = "get_vendor_product_list";
    public final static String VPO_MODE_GET_CARRIER_SDS_SHIP_METHOD_LIST = "get_carrier_sds_ship_method_list";
    public final static String VPO_MODE_SEARCH_VPO = "search_vpo";
    public final static String VPO_MODE_ADD_VPO = "add_vpo";
    public final static String VPO_MODE_DELETE_VPO = "delete_vpo";
    public final static String VPO_RESULTS_ADD_SUCCESS = "Override was successfully added.";
    public final static String VPO_RESULTS_DELETE_SUCCESS = "Delete operation was successful.";
    public final static String VPO_RESULTS_ERROR = "An error has occurred:";
    public final static String VPO_SEARCH_VENDOR_COLUMN_WIDTH = "200px";
    public final static String VPO_SEARCH_PRODUCT_COLUMN_WIDTH = "125px";
    public final static String VPO_SEARCH_CARRIER_COLUMN_WIDTH = "125px";
    public final static String VPO_SEARCH_DATE_COLUMN_WIDTH = "125px";
    public final static String VPO_SEARCH_SDS_SHIP_METHOD_COLUMN_WIDTH = "125px";

    
    /** AJAX Tags **/
    public final static String AJAX_CARRIER_ID = "carrier_id";
    public final static String AJAX_DELETE_STRING = "delete_string";
    public final static String AJAX_END_DATE = "end_date";
    public final static String AJAX_MODE = "mode";
    public final static String AJAX_PRODUCT_ID = "product_id";
    public final static String AJAX_PROPERTY_ID = "property_id";
    public final static String AJAX_PROPERTY_NAME = "propety_name";
    public final static String AJAX_SEARCH_BY = "search_by";
    public final static String AJAX_SEARCH_ON = "search_on";
    public final static String AJAX_START_DATE = "start_date";
    public final static String AJAX_SEARCH_DATE = "search_date";
    public final static String AJAX_VENDOR_ID = "vendor_id";
    public final static String AJAX_SDS_SHIP_METHOD = "sds_ship_method";
    public final static String AJAX_NUMBER_OF_SELECTED_SDS_SHIP_METHODS  = "number_of_selected_sds_ship_method";


    
    /** Artificial option entries **/
    public final static String ALL_VENDORS_VENDOR_ID = "ALL_VENDORS";
    public final static String ALL_VENDORS_VENDOR_NAME = "ALL_VENDORS";
    public final static String ALL_VENDORS_VENDOR_TYPE = "SELECT ALL";

    public final static String ALL_PRODUCTS_PRODUCT_NAME = "ALL_PRODUCTS";


    /** MyBuys global parms **/
    public static final String MY_BUYS_CONTEXT           = "MY_BUYS";
    public static final String MY_BUYS_FEED_ENABLED_FLAG = "FEED_ENABLED_FLAG";
    public static final String MY_BUYS_FEED_REQUEST_URL  = "FEED_REQUEST_URL";
    public static final String MY_BUYS_TOKEN_PROD_ID     = "<productid>";
    public static final String MY_BUYS_TOKEN_SKU_ID     = "<skuid>";
    
    /** Groupon global parms **/
    public static final String GROUPON_CONTEXT                = "SERVICE";
    public static final String GROUPON_MSG_PRODUCER_URL       = "GROUPON_MSG_PRODUCER_URL";
    public static final String GROUPON_MSG_PRODUCER_TOPIC     = "GROUPON_MSG_PRODUCER_TOPIC";
    public static final String GROUPON_MSG_PRODUCER_TOPIC_DEL = "GROUPON_MSG_PRODUCER_TOPIC_DEL";
    public static final String GROUPON_MSG_PRODUCER_TIMEOUT   = "GROUPON_MSG_PRODUCER_TIMEOUT";
    public static final String GROUPON_TOKEN_MSG              = "<msg>";
    public static final String GROUPON_TOKEN_TOPIC            = "<topic>";

    /** Project Fresh global parms **/
    public static final String FRESH_CONTEXT                  = "SERVICE"; 
    public static final String FRESH_MSG_PRODUCER_URL         = "FRESH_MSG_PRODUCER_URL";
    public static final String FRESH_MSG_PRODUCER_TIMEOUT     = "FRESH_MSG_PRODUCER_TIMEOUT";
    public static final String FRESH_TOKEN_ID                 = "<id>";
    public static final String FRESH_TOKEN_SOURCE             = "<source>";

    /*********************************************************************************************
     // Security parameters
    *********************************************************************************************/
    public static final String CONTEXT    = "context";
    public static final String SEC_TOKEN  = "securitytoken";
    public static final String CONS_MAIN_MENU_URL   = "security.main.menu";    
    public static final String ADMIN_ACTION = "adminAction";    
    public static final String SECURITY_IS_ON  = "SECURITY_IS_ON";
    public static final String SECURE_CONFIG_CONTEXT = "product_database";

    /** General **/
    public static final String PERSONALIZATION_DEFAULT_TEMPLATE = "NONE";    
    
    /** BBN - Morning Delivery**/   
    public static final String PDB_PRODUCT_MAINT_ERROR_STRING_MORNING_DELIVERY = "Morning Delivery is not available without Next Day Delivery for this product, please make the necessary updates.";
    
    /** To determine if inventory has been loaded for the product**/
    public static final String HAS_INVENTORY_RECORD = "hasInventoryRecord";
    public final static String PQuad_Product_ID = "PQuad Product ID";
    public final static String SHIPPING_SYSTEM_TYPE_FTDWEST = "FTD WEST";
    public final static String PQUAD_PRODUCT_LIST_ERRORMSG ="Cannot find a product with entered PQuad Product ID";
	public static final String PQUAD_PC_ID = "pquadPcID";
	public static final String PQUAD_PC_DISPLAY_NAMES = "pquadPcDisplayNames";
	public static final String BULLET_DESCRIPTION = "bulletDescription";
	
    
}
