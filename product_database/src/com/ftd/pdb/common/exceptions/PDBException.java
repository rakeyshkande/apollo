package com.ftd.pdb.common.exceptions;

import java.io.PrintWriter;
import java.io.StringWriter;


public abstract class PDBException extends Exception implements java.io.Serializable {
  private String[] argList = null;
  private int errorCode = 0;
  private String errorMsg = null;
  private String stackTraceString;
  private Throwable nestedException;

/**
 * Used when the exception does not need a custom message with it.
 *
 * @param errorCode the error code
 * @author Anshu Gaind
 */
  protected PDBException(int errorCode){
    super();
    this.errorCode = errorCode;
  }

/**
 * Used when the error code has an associated error message.
 *
 * @param errorCode the error code
 * @param args the arguments that go in the custom error message
 * @author Anshu Gaind
 **/
  protected PDBException(int errorCode, String[] args){
    this(errorCode, args, null);
  }

/**
 * Used when there is a need to embed a nested exception
 *
 * @param errorCode the error code
 * @param nestedThrowable the nested exception
 * @author Anshu Gaind
 **/
  protected PDBException(int errorCode, Throwable nestedThrowable){
    this.errorCode = errorCode;
    setNestedException(nestedThrowable);
  }


/**
 * Used when there is a need to embed a nested exception
 *
 * @param errorCode the error code
 * @param args the arguments that go in the custom error message
 * @param nestedThrowable the nested exception
 * @author Anshu Gaind
 **/
  protected PDBException(int errorCode, String[] args, Throwable nestedThrowable){
    this.errorCode = errorCode;
    setNestedException(nestedThrowable);
    argList=new String[args.length];
    System.arraycopy(args,0,argList,0,args.length);
  }

/**
 * This is overridden equals function of Object class
 * @param obj java.lang.Object
 * @author Anshu Gaind 
 */
  public boolean equals(Object obj) {
    boolean retVal=false;
    if(obj instanceof PDBException) {
      PDBException copy= (PDBException
      ) obj;
      if (this.errorCode == copy.errorCode) {
        if((this.argList != null) && (copy.argList != null)) {
          if (this.argList.length == copy.argList.length) {
            for(int count=0; count < this.argList.length; count++) {
                if(!(this.argList[count].equals(copy.argList[count]))) {
                  retVal=false;
                  break;
                }
                retVal=true;
            }
          }
			} else if((this.argList == null) && (copy.argList == null)) {
				retVal=true;
			}
		}
	}
	return retVal;
}
/**
 * Returns the error code.
 * 
 * @return int the error code
 * @author Anshu Gaind
 */
  public int getErrorCode() {
    return errorCode;
  }
/**
 * It converts the error code to error String using ErrorCodeStore 
 * and customizes it with the help of argList stored in it.
 * 
 * @return java.lang.String
 * @author Anshu Gaind
 */
  public String getMessage() {
    StringBuffer msg=null;
    if(errorMsg == null) {
       errorMsg = ErrorCodeStore.getInstance().findValue(errorCode, argList);
    }
    if( stackTraceString != null) {
      msg=new StringBuffer(errorMsg);
      msg.append("|nested Exception Stack Trace: [");
      msg.append(stackTraceString);
      msg.append("]");
    }
    if(msg != null)
      return msg.toString();	
    return errorMsg;
  }
/**
 * Retrieve the nested exception.
 *
 * @return java.lang.Throwable
 * @author Anshu Gaind
 */
public Throwable getNestedException() {
	return nestedException;
}
/**
 * Returns the stack trace string
 * 
 * @return java.lang.String
 * @author Anshu Gaind
 */
public String getStackTraceString() {
	return stackTraceString;
}
/**
 * This function is used to store any nested exception.
 * If in LOG4J config  info is enabled it will store
 * all the stack trace of throwable object. If getMessage() is invoked it will give
 * the stack trace of throwable object.
 * 
 * @param exp java.lang.Throwable any exception object.
 * @author Anshu Gaind
 */
  public void setNestedException(Throwable exp) {
    nestedException=exp;
    setStackTraceString();
  }
/**
 * Sets the stack trace string if a nested exception is present
 *
 * @author Anshu Gaind
 */
  private void setStackTraceString()  {	
    if(nestedException != null) {
      //LogManager dl=new LogManager(FrameworkConstants.FRAMEWORK_EXCEPTIONS_CATEGORY);
      //if(dl.isInfoEnabled()) {
        StringWriter str=new StringWriter();
        nestedException.printStackTrace(new PrintWriter(str));
        stackTraceString=str.toString();
      //}
    }		
  }
/**
 * Returns a String that represents the value of this object.
 * @return a string representation of the receiver
 * @author Anshu Gaind
 */
  public String toString() {
    // Insert code to print the receiver here.
    // This implementation forwards the message to super. You may replace or supplement this.
    String msg="[FTDException[errorCode: "+ errorCode + ", argList [";
    if((argList != null) && (argList.length > 0)) {
      for(int count=0; count < argList.length; count++) {
        String arg=argList[count];
        msg += arg;
        if(count != argList.length-1)
          msg += ", ";
      }
      msg += "]]]";
    }
    return msg;	
  }

/**
 * Returns the runtime arguments for this exception.
 * If there are no arguments, this method returns a 0 length String[]
 * @return  the custom arguments
 * @author Anshu Gaind
 */
  public String[] getArgList() {
    if (argList == null){
      return new String[0];
    }else{
      return argList;
    }
  }
}
