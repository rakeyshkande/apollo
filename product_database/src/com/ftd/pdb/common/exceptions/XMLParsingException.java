package com.ftd.pdb.common.exceptions;

public class XMLParsingException extends Exception {
    public XMLParsingException() {
    }
    
    public XMLParsingException(Throwable t) {
        super(t);
    }
}
