package com.ftd.pdb.common.utilities;


/**
 * Performs miscellaneous utility functionality such as data type conversions
 & and formatting
 * Creation date: (08/13/2002)
 * @author: Chris Fagalde
 */

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.email.EmailVO;
import com.ftd.osp.utilities.j2ee.NotificationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.vo.NotificationVO;
import com.ftd.pdb.common.ProductMaintConstants;

import java.security.MessageDigest;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.valobjs.ValueVO;
import org.apache.commons.lang.time.DateUtils;

import java.math.BigDecimal;

import java.sql.Connection;

import java.text.DecimalFormat;

import javax.mail.Session;

import org.apache.commons.lang.StringUtils;

/**
 * Performs miscellaneous utility functionality such as data type conversions
 & and formatting
 * Creation date: (08/13/2002)
 * @author: Chris Fagalde
 */
public class

FTDUtil {

    protected static Logger LOGGER = 
        new Logger("com.ftd.pdb.common.utilities.FTDUtil");

    /**
     * Util constructor comment.
     */
    private FTDUtil() {
        super();
    }

    /**
     * This method is used to addDays to the current date.
     * Creation date: (4/4/02 1:37:05 PM)
     * It takes one parameter, an integer, which specifies how many days
     * should be added to the current date
     * The function returns a date that is the equivalent of the
     * current date + number of months passed to the function
     */
    public static Date addDays(int numberOfDays) {

        Calendar calendar = Calendar.getInstance();
        calendar.getTime();
        calendar.add(Calendar.DATE, numberOfDays);

        return calendar.getTime();

    }

    /**
     * This method is used to add Days to the date passed in as a parameter.
     * Creation date: (4/4/02 1:37:05 PM)
     *
     * The function returns a date that is the equivalent of the
     * date + number of days passed to the function
     */
    public static Date addDays(Date date, int numberOfDays) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, numberOfDays);
        return calendar.getTime();

    }

    /**
     * This method is used to add Hours to the curent date passed in as a
     * parameter.
     * Creation date: (4/4/02 1:37:05 PM)
     *
     * The function returns a date that is the equivalent of the
     * date + number of hours passed to the function
     */
    public static Date addHours(int numberOfHours) {

        Calendar calendar = Calendar.getInstance();
        calendar.getTime();
        calendar.add(Calendar.HOUR, numberOfHours);
        return calendar.getTime();

    }

    /**
     * This method is used to add Hours to the date passed in as a parameter.
     * Creation date: (4/4/02 1:37:05 PM)
     *
     * The function returns a date that is the equivalent of the
     * date + number of hours passed to the function
     */
    public static Date addHours(Date date, int numberOfHours) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.HOUR, numberOfHours);
        return calendar.getTime();

    }

    /**
     * This method is used to add Minutes to the date passed in as a parameter.
     * Creation date: (4/4/02 1:37:05 PM)
     *
     * The function returns a date that is the equivalent of the
     * date + number of minutes passed to the function
     */
    public static Date addMinutes(Date date, int numberOfMinutes) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MINUTE, numberOfMinutes);
        return calendar.getTime();

    }

    /**
     * Name: addMonths
     * Creation date: (4/4/02 1:37:05 PM)
     *
     * This function is used to add months to the date that is passed in
     * as a parameter.
     *
     * Parameters: Date originalDate
     *             int the number of months to add to the original date
     *
     * Return: Date which will represent the date passed in plus the
     *         number of months that was also passed in.
     */
    public static Date addMonths(Date originalDate, int numberOfMonths) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(originalDate);
        calendar.add(Calendar.MONTH, numberOfMonths);

        return calendar.getTime();

    }

    /**
     * Converts boolean value to a String value
     * Creation date: (3/13/02 12:54:09 PM)
     * @return java.lang.String
     */
    public static String convertBooleanToString(boolean bFlag) {
        String flag = String.valueOf(bFlag).toUpperCase();
        if (flag.equals("TRUE"))
            flag = "Y";
        else
            flag = "N";
        return flag;
    }

    public static boolean convertStringToBoolean(String sFlag) {
        boolean flag = false;
        if (sFlag != null && 
            ((sFlag.toUpperCase()).equals("Y") || (sFlag.toUpperCase()).equals("TRUE")))
            flag = true;
        return flag;
    }

    /**
     * Encrypts text
     * Creation date: (5/24/02 3:29:04 PM)
     * @return java.lang.String
     * @param text java.lang.String
     */
    public static String encryptText(String text) throws PDBApplicationException {
        String encryptedText = null;
        try {

            // Create a Message Digest from a Factory method
            MessageDigest md = MessageDigest.getInstance("MD5");

            byte[] msg = text.getBytes();

            // Update the message digest with some more bytes
            // This can be performed multiple times before creating the hash
            md.update(msg);

            // Create the digest from the message
            byte[] aMessageDigest = md.digest();

            encryptedText = new String(aMessageDigest);
        } catch (Exception e) {
            e.printStackTrace();

            String msg[] = new String[1];
            msg[0] = e.getMessage();

            throw new PDBApplicationException(999, msg);
        }

        return encryptedText;
    }

    public static Object convertNulls(Object in) {
        if (in == null) {
            in = "";
        }

        return in;
    }

    public static String getDescrption(String code, List list) {
        String ret = null;

        for (int i = 0; i < list.size(); i++) {
            if (((ValueVO)list.get(i)).getId().equals(code)) {
                ret = ((ValueVO)list.get(i)).getDescription();
                break;
            }
        }

        return ret;
    }

    public static Object newImplementationInstance(String implementationName) throws Exception {
        String className;
        className = 
                ConfigurationUtil.getInstance().getProperty(ProductMaintConstants.PROPERTY_FILE, 
                                                            implementationName);
        Class classDefinition = Class.forName(className);
        return classDefinition.newInstance();
    }

    //    public static BigDecimal toBigDecimal(float floatValue) {
    //        BigDecimal retval = new BigDecimal(floatValue);
    //        return retval;
    //    }

    public static String formatBigDecimalAsCurrency(java.math.BigDecimal amount, 
                                                    int scale) {

        String strFormattedCurrency = null;
        StringBuffer formattedCurrency = null;
        boolean negNumber = false;

        return formatBigDecimalAsCurrency(amount, scale, true);

    }

    /**
     * Description: Method for format big decimal as string with (), and optional
     *              $  as needed. Also sets precision to length given
     *
     *
     * @return java.lang.String
     */
    public static String formatBigDecimalAsCurrency(BigDecimal amount, 
                                                    int scale, 
                                                    boolean dollarSign) {

        String strFormattedCurrency = null;
        StringBuffer formattedCurrency = null;
        boolean negNumber = false;

        // make sure that amount is not null
        if (amount != null) {
            if (amount.signum() < 0) // negative
            {
                negNumber = true;
                amount = amount.abs();
            }
        } else {
            amount = new BigDecimal("0");
        }

        // set scale as user requested via SCALE parm
        amount = amount.setScale(scale, BigDecimal.ROUND_HALF_UP);

        strFormattedCurrency = amount.toString();

        // get whole number portion
        int idx = strFormattedCurrency.indexOf(".");

        if (idx == -1) /// no decimal so set idx to length
        {
            idx = strFormattedCurrency.length();
        }

        formattedCurrency = new StringBuffer(strFormattedCurrency);

        int pos = 3;

        while (pos < idx) {
            formattedCurrency.insert(idx - pos, ",");
            pos = pos + 3;
        } // while

        if (negNumber == true) {
            // negative # -  add parens
            if (dollarSign) {
                formattedCurrency.insert(0, "($");
            } else {
                formattedCurrency.insert(0, "(");
            }

            formattedCurrency.append(")");
        } else {
            if (dollarSign)
                formattedCurrency.insert(0, "$");
        }

        return formattedCurrency.toString();

    }

    /**
     * Description: Method for format big decimal as string with and optional
     *              %  as needed. Also sets precision to length given.
     *
     * @return java.lang.String
     */
    public static String formatBigDecimalAsPercentage(BigDecimal amount) {
        return formatBigDecimalAsPercentage(amount, 2, true);
    }

    /**
     * Description: Method for format big decimal as string with and optional
     *              %  as needed. Also sets precision to length given.
     *
     *
     * @return java.lang.String
     */
    public static String formatBigDecimalAsPercentage(BigDecimal amount, 
                                                      int scale) {
        return formatBigDecimalAsPercentage(amount, scale, true);
    }

    /**
     * Description: Method for format big decimal as string with and optional
     *              %  as needed. Also sets precision to length given.
     *
     *
     * @return java.lang.String
     */
    public static String formatBigDecimalAsPercentage(BigDecimal amount, 
                                                      int scale, 
                                                      boolean percentSign) {

        String strFormattedPercentage = null;
        StringBuffer formattedPercentage = null;
        boolean negNumber = false;
        String outPercentage = null;

        // make sure that amount is not null
        if (amount != null) {

            // move 2 places to left and set scale as user requested via SCALE parm
            amount = amount.multiply(new BigDecimal("100"));
            amount = amount.setScale(scale, BigDecimal.ROUND_FLOOR);

            strFormattedPercentage = amount.toString();

            // get whole number portion
            int idx = strFormattedPercentage.indexOf(".");

            if (idx == -1) /// no decimal so set idx to length
            {
                idx = strFormattedPercentage.length();
            }

            formattedPercentage = new StringBuffer(strFormattedPercentage);

            int pos = 3;

            while (pos < idx) {
                formattedPercentage.insert(idx - pos, ",");
                pos = pos + 3;
            } // while

            if (percentSign) {
                formattedPercentage.append("%");
            }

            outPercentage = formattedPercentage.toString();

        } else {
            outPercentage = "-";
        }

        return outPercentage;

    }

    /**
     * Description: Formats a big decimal as string with () and $
     *              as needed. Also sets precision to length given.
     *              Calls overloaded method to do all the work.
     *
     *
     * @return java.lang.String
     */
    public static String formatFloat(Float amount, 
                                     int scale) throws PDBApplicationException {
        return formatFloat(amount, scale, true);
    }

    /**
     * Description: Method for format float as string with (), and optional
     *              $  as needed. Also sets precision to length given. DO NOT USE THIS FOR
     *              UUPDATABLE NUMBERS ARE PRECISION IS NOT DEALT WITH MATHMATICALLY. ITS LOPPED
     *              OFF!
     *
     * @return java.lang.String
     */
    public static String formatFloat(Float amount, int scale, 
                                     boolean dollarSign) throws PDBApplicationException {

        return formatBigDecimalAsCurrency(new BigDecimal(amount.doubleValue()), 
                                          2, dollarSign);

    }

    /**
     * Description: Method for format Float as string with (), and optional
     *              $  as needed. Also sets precision to length given. The final boolean
     *              tells if we should return a zero string as blank. DO NOT USE THIS FOR
     *              UUPDATABLE NUMBERS ARE PRECISION IS NOT DEALT WITH MATHMATICALLY. ITS LOPPED
     *              OFF!
     *
     * @return java.lang.String
     */
    public static String formatFloat(Float amount, int scale, 
                                     boolean dollarSign, 
                                     boolean returnZero) throws PDBApplicationException {

        String ret = null;

        if (returnZero == false && amount.floatValue() == 0) {
            ret = new String("");
        } else {
            ret = 
formatBigDecimalAsCurrency(new BigDecimal(amount.doubleValue()), 2, 
                           dollarSign);
        }
        return ret;
    }
    
    /**
   * Formats a string dollar amount to the specified format.
   * @param amount
   * @return String
   * @throws java.lang.Exception
   */
  public static String formatAmountToPattern(Float amount, String pattern) throws Exception
  {
      DecimalFormat myFormatter = new DecimalFormat(pattern);
      String output = "0.00";
      try 
      {
          output = myFormatter.format(amount.doubleValue());
      } catch (NumberFormatException nfe) {
          // ignore. return initialized value.
          LOGGER.error(nfe);
      } catch (IllegalArgumentException iae) {
          // ignore. return initialized value.
          LOGGER.error(iae);
      }
      return output;
  }

    /**
     * Description:
     * @return java.lang.String
     */
    public static String formatInteger(int amount) throws PDBApplicationException {
        return formatInteger(amount, false);

    }

    /**
     * Description:         Formats an integer with commas, () if negative and optionally with $.
     *
     * @return java.lang.String
     */
    public static String formatInteger(int amount, 
                                       boolean dollarSign) throws PDBApplicationException {

        String strFormattedInteger = null;
        StringBuffer bufFormattedInteger = null;
        boolean negNumber = false;

        strFormattedInteger = Integer.toString(amount);
        bufFormattedInteger = new StringBuffer(strFormattedInteger);

        // see if a negative number. Use string because buf does not have this method
        int negativeIndex = strFormattedInteger.indexOf("-");
        // we should probably add in checks for () eventually?
        if (negativeIndex == 0) {
            bufFormattedInteger.delete(negativeIndex, negativeIndex + 1);
            negNumber = true;
        } else if (negativeIndex != -1) {
            throw new PDBApplicationException(999);
        }

        // format with commas
        int pos = 3;
        int len = bufFormattedInteger.length();
        while (pos < len) {
            bufFormattedInteger.insert(len - pos, ",");
            pos = pos + 3;
        } // while

        // deal with negative and $ needs
        if (negNumber == true) {
            // negative # -  add parens
            if (dollarSign) {
                bufFormattedInteger.insert(0, "($");
            } else {
                bufFormattedInteger.insert(0, "(");
            }

            bufFormattedInteger.append(")");
        } else {
            if (dollarSign)
                bufFormattedInteger.insert(0, "$");
        }

        return bufFormattedInteger.toString();

    }

    /**
     * Description: Method to take the parts of a name and format them into
     *                              last name, first name middle initial
     * Creation date: 04/04/2002
     *
     * @return String
     */
    public static String formatName(String firstName, String middleInit, 
                                    String lastName) throws Exception {

        String name = null;

        if (firstName == null && middleInit == null && lastName == null) {
            name = " ";

        } else {

            StringBuffer formattedName = new StringBuffer();

            // output last name
            if (lastName != null && !lastName.trim().equals("")) {
                formattedName.append(lastName);
            }

            // output first name
            if (firstName != null && !firstName.trim().equals("")) {
                if (formattedName.length() > 0) {
                    formattedName.append(", ");
                }
                formattedName.append(firstName);
            }

            // only output the middle initial if there is something else there
            if (middleInit != null && !middleInit.trim().equals("")) {
                if (formattedName.length() > 0) {
                    formattedName.append(" ");
                    formattedName.append(middleInit);
                }
            }

            name = formattedName.toString();

        }

        return name;
    }

    /**
     * Description: Takes a SQL Date in the format yyyy-mm-dd and formats it
     *              as MM/dd/yyyy (which is the format we show in the screens).
     *
     *
     * @return String
     */
    public static String formatSQLDateToString(java.sql.Date sqlDate) throws PDBApplicationException {
        String strDate = "";
        java.util.Date utilDate = null;

        if (sqlDate != null) {
            strDate = sqlDate.toString();
            SimpleDateFormat dfIn = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat dfOut = new SimpleDateFormat("MM/dd/yyyy");

            try {
                utilDate = dfIn.parse(strDate);
            } catch (ParseException pe) {
                throw new PDBApplicationException(999);
            }

            strDate = dfOut.format(utilDate);
        }

        return strDate;
    }

    /**
     * Description: Takes a SQL Date in the format yyyy-mm-dd hh:mm:ss and formats it
     *              as MM/dd/yyyy hh:mm:ss (which is the format we show in the screens).
     *
     *
     * @return String
     */
    public static String formatSQLTimestampToString(java.sql.Timestamp sqlDate) throws PDBApplicationException {
        String strDate = "";
        java.util.Date utilDate = null;

        if (sqlDate != null) {
            strDate = sqlDate.toString();
            SimpleDateFormat dfIn = 
                new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            SimpleDateFormat dfOut = 
                new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");

            try {
                utilDate = dfIn.parse(strDate);
            } catch (ParseException pe) {
                throw new PDBApplicationException(999);
            }

            strDate = dfOut.format(utilDate);
        }

        return strDate;
    }

    /**
     * Description: Method to make sure a SSN if formatted nicely.
     *              First removes all non-numeric and then formats as nnn-nn-nnnn.
     *              Probably do not really need to strip these off
     *              when saving data in the handler for updates because
     *              its a string after all.  We can do more validation and
     *              exception checking here but I did not bother for now...
     * @return java.lang.String
     */
    public static String formatSSN(String inputSSN) throws Exception {

        String SSN = null;

        if ((inputSSN != null) && (!inputSSN.trim().equals(""))) {

            StringBuffer formattedSSN = new StringBuffer(inputSSN);

            if ((formattedSSN.length() > 0) && (formattedSSN != null) && 
                (!formattedSSN.equals(""))) {

                // remove all non numeric - same as the strip method
                // on main handler but can't use that because where it sits
                char c;
                Character ch = null;

                for (int i = 0; i < formattedSSN.length(); ) {
                    c = formattedSSN.charAt(i);

                    if (!Character.isDigit(c)) {
                        formattedSSN.delete(i, i + 1);
                    } else {
                        i++;
                    }
                }

                if (formattedSSN.length() > 3) {
                    formattedSSN.insert(3, "-");
                }

                if (formattedSSN.length() > 6) {
                    formattedSSN.insert(6, "-");
                }

                SSN = formattedSSN.toString();
            }

        } else {
            SSN = " ";
        }

        return SSN;
    }

    /**
     * Description: Takes in a string Date, checks for valid formatting
     *                              in the form of mm/dd/yyyy and converts it
     *                              to a SQL date of yyyy-mm-dd.
     *
     *
     * @return java.sql.Date
     */
    public static java.sql.Date formatStringToSQLDate(String strDate) throws PDBApplicationException {

        java.sql.Date sqlDate = null;

        if ((strDate != null) && (!strDate.trim().equals(""))) {
            try {
                if (strDate.length() < 10) {
                    throw new PDBApplicationException(999);
                }
                SimpleDateFormat sdfInput = new SimpleDateFormat("MM/dd/yyyy");
                SimpleDateFormat sdfOutput = 
                    new SimpleDateFormat("yyyy-MM-dd");

                java.util.Date date = sdfInput.parse(strDate);
                String outDateString = sdfOutput.format(date);

                // now that we have no errors, use the string to make a SQL date
                sqlDate = java.sql.Date.valueOf(outDateString);

            } catch (Exception e) {
                throw new PDBApplicationException(999);
            }
        }

        return sqlDate;
    }

    /**
     * Description: Takes in a string Date, checks for valid formatting
     *                              in the form of mm/dd/yyyy hh:mm:ss and converts it
     *                              to a SQL date of yyyy-mm-dd hh:mm:ss.
     *
     * @return java.sql.Date
     */
    public static java.sql.Timestamp formatStringToSQLTimestamp(String strDate) throws PDBApplicationException {

        java.sql.Timestamp sqlDate = null;

        if ((strDate != null) && (!strDate.trim().equals(""))) {
            try {
                if (strDate.length() < 19) {
                    throw new PDBApplicationException(999);
                }
                SimpleDateFormat sdfInput = 
                    new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
                SimpleDateFormat sdfOutput = 
                    new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

                java.util.Date date = sdfInput.parse(strDate);
                String outDateString = sdfOutput.format(date);

                // now that we have no errors, use the string to make a SQL date
                sqlDate = java.sql.Timestamp.valueOf(outDateString);

            } catch (Exception e) {
                throw new PDBApplicationException(999);
            }
        }

        return sqlDate;
    }

    /**
     * Description: Takes in a string Date, checks for valid formatting
     *                              nd converts it to a Util date of yyyy-mm-dd.
     * @return java.sql.Date
     */
    public static java.util.Date formatStringToUtilDate(String strDate) throws PDBApplicationException {

        java.util.Date utilDate = null;
        String inDateFormat = "";
        int dateLength = 0;
        int firstSep = 0;
        int lastSep = 0;

        if ((strDate != null) && (!strDate.trim().equals(""))) {
            try {

                // set input date format
                dateLength = strDate.length();
                if (dateLength > 10) {
                    inDateFormat = "yyyy-MM-dd hh:mm:ss";
                } else {
                    firstSep = strDate.indexOf("/");
                    lastSep = strDate.lastIndexOf("/");

                    switch (dateLength) {
                    case 10:
                        inDateFormat = "MM/dd/yyyy";
                        break;
                    case 9:
                        if (firstSep == 1) {
                            inDateFormat = "M/dd/yyyy";
                        } else {
                            inDateFormat = "MM/d/yyyy";
                        }
                        break;
                    case 8:
                        if (firstSep == 1) {
                            inDateFormat = "M/d/yyyy";
                        } else {
                            inDateFormat = "MM/dd/yy";
                        }
                        break;
                    case 7:
                        if (firstSep == 1) {
                            inDateFormat = "M/dd/yy";
                        } else {
                            inDateFormat = "MM/d/yy";
                        }
                        break;
                    case 6:
                        inDateFormat = "M/d/yy";
                        break;
                    default:
                        break;
                    }
                }
                SimpleDateFormat sdfInput = new SimpleDateFormat(inDateFormat);
                SimpleDateFormat sdfOutput = 
                    new SimpleDateFormat("yyyy-MM-dd");

                java.util.Date date = sdfInput.parse(strDate);
                String outDateString = sdfOutput.format(date);

                // now that we have no errors, use the string to make a Util date
                utilDate = sdfOutput.parse(outDateString);

            } catch (Exception e) {
                throw new PDBApplicationException(999);
            }
        }

        return utilDate;
    }

    /**
     * Description:
     *
     *
     * @return java.lang.String
     */
    public static String formatUSPhone(String phoneNotFormatted) throws Exception {

        if ((phoneNotFormatted != null) && 
            (!phoneNotFormatted.trim().equals(""))) {
            StringBuffer formattedPhone = 
                new StringBuffer(stripNonNumeric(phoneNotFormatted));

            if ((formattedPhone.length() > 0) && (formattedPhone != null) && 
                (!formattedPhone.equals(""))) {
                formattedPhone.insert(0, "(");

                if (formattedPhone.length() > 3) {
                    formattedPhone.insert(4, ')');
                    formattedPhone.insert(5, ' ');

                    if (formattedPhone.length() > 7) {
                        formattedPhone.insert(9, '-');
                    }
                }
                return formattedPhone.toString();
            }
        }

        return "";
    }

    /**
     * Description: Takes a Util Date in the format yyyy-mm-dd and formats it
     *              as MM/dd/yyyy (which is the format we show in the screens).
     *
     *
     * @return String
     */
    public static String formatUtilDateToString(java.util.Date utilDate) throws PDBApplicationException {
        String strDate = "";
        String inDateFormat = "";
        java.util.Date newDate = null;

        if (utilDate != null) {
            SimpleDateFormat dfOut = new SimpleDateFormat("MM/dd/yyyy");

            try {
                newDate = dfOut.parse(dfOut.format(utilDate));
            } catch (ParseException pe) {
                throw new PDBApplicationException(999);
            }

            strDate = dfOut.format(newDate);

        } else {
            strDate = "";
        }

        return strDate;
    }

    /**
     * Description:   From Phase 1 - wbet delivery.
     *                Replaces ALL occurences of a string(what) in a string(source)
     *                with a given string (with).
     *
     * @return java.lang.String
     */
    public static String replaceAll(String source, String what, String with) {
        String strReturn = null;

        strReturn = translateNullToString(source);
        strReturn = translateNullToString(strReturn);
        int index = -1;

        if (with == null) {
            return strReturn;
        }

        index = strReturn.indexOf(what);
        while (index >= 0) {
            strReturn = 
                    strReturn.substring(0, index) + with + strReturn.substring(index + 
                                                                               what.length(), 
                                                                               strReturn.length());
            index = strReturn.indexOf(what, index + with.length());
        }

        return strReturn;
    }

    /**
     * Description:   Copied from Phase 1 - wbet delivery and modified to replace only 1
     *                occurence. Replaces a string(what) in a string(source)
     *                with a given string (with).
     *
     *
     * @return java.lang.String
     */
    public static String replaceOnce(String source, String what, String with) {
        String strReturn = null;

        strReturn = translateNullToString(source);
        strReturn = translateNullToString(strReturn);
        int index = -1;
        if (with == null) {
            return strReturn;
        }

        index = strReturn.indexOf(what);

        if (index >= 0) {
            strReturn = 
                    strReturn.substring(0, index) + with + strReturn.substring(index + 
                                                                               what.length(), 
                                                                               strReturn.length());
            index = strReturn.indexOf(what, index + with.length());
        }

        return strReturn;
    }

    public static String stripNonNumeric(String inString) {

        char c;
        Character ch = null;

        if ((inString != null) && (!inString.trim().equals(""))) {

            StringBuffer outString = new StringBuffer(inString);

            for (int i = 0; i < outString.length(); ) {
                c = outString.charAt(i);

                if (!Character.isDigit(c)) {
                    outString.delete(i, i + 1);

                } else {
                    i++;
                }
            }

            return outString.toString();
        } else {
            return null;
        }

    }

    public static String stripNonNumericForDecimal(String inString) {

        char c;
        Character ch = null;
        boolean negative = false;

        if ((inString != null) && (!inString.trim().equals(""))) {
            StringBuffer outString = new StringBuffer(inString);

            for (int i = 0; i < outString.length(); ) {
                c = outString.charAt(i);

                if (!Character.isDigit(c)) {

                    if (c == '.') {
                        i++;
                    } else {
                        if (((c == '(') || (c == '-')) && (i == 0)) {
                            negative = true;
                        }
                        outString.delete(i, i + 1);
                    }
                } else {
                    i++;
                }

            }

            if (negative == true)
                return "-" + outString.toString();

            else
                return outString.toString();

        } else // null inString
            return null;
    }

    /**
     * Insert the method's description here.
     * @return java.lang.String
     */
    public static String translateEmptyStringToNbsp(String a_sInput) {
        if (a_sInput == null) {
            a_sInput = "";
        }
        return a_sInput.trim().length() == 0 ? "&nbsp;" : a_sInput;
    }

    /**
     * Insert the method's description here.
     * @return java.lang.String
     */
    public static String translateEmptyStringToSpace(String a_sInput) {
        if (a_sInput == null) {
            a_sInput = "";
        }
        return a_sInput.equals("") ? " " : a_sInput;
    }

    /**
     * Insert the method's description here.
     * @return java.lang.String
     */
    public static String translateNbspToSpace(String a_sInput) {
        return a_sInput.equals("&nbsp;") ? " " : a_sInput;
    }

    /**
     * Description:   From Phase 1 - wbet delivery.
     *                returns "" vs null string for null string
     *
     * @return java.lang.String
     */
    public static String translateNullToString(Object source) {
        String strReturn = "";
        if (source == null)
            strReturn = "";
        else
            strReturn = "" + source;
        return (String)strReturn;
    }

    /**
     * Description:   From Phase 1 - wbet delivery.
     *                returns "" vs null string for null string
     *
     *
     * @return java.lang.String
     */
    public static

    String translateNullToString(String source) {
        String strReturn = "";
        if (source == null)
            strReturn = "";
        else
            strReturn = source;
        return strReturn;
    }

    /**
     * Insert the method's description here.
     * @return java.lang.String
     */
    public static String truncateString(String a_sInput) {
        if (a_sInput == null) {
            a_sInput = "";
        }
        return a_sInput.trim();
    }

    /**
     * Insert the method's description here.
     * @return java.lang.String
     * @param utilDate java.util.Date
     */
    public static String formatUtilDateToStringWithTimestamp(java.util.Date utilDate) throws Exception {
        String strDate = "";
        String inDateFormat = "";
        java.util.Date newDate = null;

        if (utilDate != null) {
            SimpleDateFormat dfOut = 
                new SimpleDateFormat("MM/dd/yyyy hh:mm a");

            try {
                newDate = dfOut.parse(dfOut.format(utilDate));
            } catch (ParseException pe) {
                throw new PDBApplicationException(999);
            }

            strDate = dfOut.format(newDate);

        } else {
            strDate = "";
        }

        return strDate;
    }

    /**
         * Strips out commas from currency
         * Creation date: (5/20/02 3:29:04 PM)
         * @return java.lang.String
         * @param currency java.lang.String
         */
    public static String removeFormattingCurrency(String inString) {
        char c;
        Character ch = null;
        boolean negative = false;

        if ((inString != null) && (!inString.trim().equals(""))) {
            StringBuffer outString = new StringBuffer(inString);

            for (int i = 0; i < outString.length(); ) {
                c = outString.charAt(i);

                if (!Character.isDigit(c)) {

                    if (c == '.') {
                        i++;
                    } else {
                        if (((c == '(') || (c == '-')) && (i == 0)) {
                            negative = true;
                        }
                        outString.delete(i, i + 1);
                    }
                } else {
                    i++;
                }

            }

            if (negative == true)
                return "-" + outString.toString();

            else
                return outString.toString();

        } else // blank inString
            return "";
    }

  /**
   * Returns the difference in days between the startDate and endDate.
   * 
   * @param startDate
   * @param endDate
   * @return The difference in days between the startDate and endDate
   * @throws IllegalArgumentException if either startDate or endDate are null
   */
  public static int getDiffInDays(Date startDate, Date endDate) {

    // null check inputs
    if ( startDate == null || endDate == null ) {
      throw new IllegalArgumentException("Invalid parameter: one or both of startDate='" + startDate + "', endDate='" + endDate + "' were null.");
    }

    long diffInMillis = endDate.getTime() - startDate.getTime();

    int diffInDays = (int)(diffInMillis / DateUtils.MILLIS_PER_DAY);

    return diffInDays;
  }
        
    public static void sendPlainTextEmail(Session session, EmailVO emailVO) throws Exception
    {
        NotificationVO notificationVO = new NotificationVO();
        notificationVO.setMessageFromAddress(emailVO.getSender());
        notificationVO.setMessageTOAddress(emailVO.getRecipient());
        notificationVO.setMessageSubject(emailVO.getSubject());
        notificationVO.setMessageContent(emailVO.getContent());
        notificationVO.setMessageMimeType("text/plain; charset=us-ascii");
        NotificationUtil.getInstance().notify(session, notificationVO);
    }
    
    public static String getHostName() {
        String retval;
        
        try
        {
            java.net.InetAddress localMachine = java.net.InetAddress.getLocalHost();    
            retval = localMachine.getHostName();
        }
        catch(java.net.UnknownHostException uhe)
        {
            retval = "unknown host";
        }
        
        return retval;
    }

    /**
   * This method sends a message to the System Messenger.
   * WARNING:  passed in connection is closed by System Messager.
   * @param conn database connection
   * @param message
   * @return message id
   */
    public static String sendSystemMessage(Connection conn, String message) throws Exception {

        LOGGER.error("Sending System Message: " + message);

        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        String messageSource = 
            configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, "message.source");

        String messageID = "";

        //build system vo
        SystemMessengerVO sysMessage = new SystemMessengerVO();
        sysMessage.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
        sysMessage.setSource(messageSource);
        sysMessage.setType("ERROR");
        sysMessage.setMessage(message);
        sysMessage.setSubject("PRODUCT DATABASE ERROR");

        SystemMessenger sysMessenger = SystemMessenger.getInstance();
        messageID = sysMessenger.send(sysMessage, conn);

        if (messageID == null) {
            String msg = 
                "Error occured while attempting to send out a system message. Msg not sent: " + 
                message;
            LOGGER.error(msg);
        }

        return messageID;
    }
    
    public static String getLockSessionId() throws Throwable {
        long current =  Calendar.getInstance().getTimeInMillis();
        return String.valueOf(current);
    }   
    
    
    /**
     * This method will compare two strings and consider null and blank as equal
     * @param stringLeft
     * @param stringRight
     * @return true if they are equal
     */
    public static Boolean stringEqualsWithBlankCheck (String stringLeft, String stringRight)
    {
        Boolean returnValue = false;
        
        if (StringUtils.equals(stringLeft, stringRight))
        {
            returnValue = true;
        }
        else if(StringUtils.isBlank(stringLeft) && StringUtils.isBlank(stringRight))
        {
            returnValue = true;
        }
    
        return returnValue;
    }
}
