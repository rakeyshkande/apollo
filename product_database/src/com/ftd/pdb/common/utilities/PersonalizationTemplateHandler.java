package com.ftd.pdb.common.utilities;

import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;

import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.valobjs.ValueVO;

import java.math.BigInteger;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * A cache handler is responsible for the following:
 *
 * 1. Act as a cache loader, to load the data that needs to be cached.
 * 2. Provide application specific API, to enable access to the cached data.
 *
 */

public class PersonalizationTemplateHandler extends CacheHandlerBase 
{
    // set by the setCachedObject method
    private Map<String,String> templates;
    private Map<String,ArrayList> templatePermutations;
    
    public PersonalizationTemplateHandler()
    {
    }

   /**
   * Returns the object that needs to be cached.
   * 
   * The cache cannot be configured as distributable, if the cache handler 
   * returns a custom object. In order to utilize the benefits of a distributable 
   * cache, the return objects should only be a part JDK API.
   * 
   * @param con
   * @return the object to be cached
   * @throws com.ftd.osp.utilities.cacheMgr.exception.CacheException
   */
    public Object load(Connection con) throws CacheException
    {
        Map<String,String> templates = new HashMap<String,String>();
        try
        {
            super.logger.debug("BEGIN LOADING PERSONALIZATION TEMPLATE HANDLER...");
            
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.setStatementID("PDB_GET_PERSONALIZATION_TEMPLATE_LIST");
            CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
            while(rs.next())
            {
                templates.put(rs.getString(ProductMaintConstants.OE_PRODUCT_VALUE_ID), rs.getString(ProductMaintConstants.OE_PRODUCT_VALUE_DESCRIPTION));
            }
            super.logger.debug("END LOADING PERSONALIZATION TEMPLATE HANDLER...");
        }
        catch(Exception e)
        {
            super.logger.error(e);
            throw new CacheException("Could not load Personalization Template cache.");
        }
        
        logger.debug(templates.size() + " records loaded");
        return templates;
    }


  /**
   * Set the cached object in the cache handler. The cache handler is then 
   * responsible for fulfilling all application level API calls, to access the
   * data in the cached object.
   * 
   * @param cachedObject
   * @throws com.ftd.osp.utilities.cacheMgr.exception.CacheException
   */    
    public void setCachedObject(Object cachedObject) throws CacheException
    {
      try 
      {
        this.templates = (Map) cachedObject;  
        this.templatePermutations = generatePermutations(this.templates);
      } catch (Exception ex) 
      {
        super.logger.error(ex);
        throw new CacheException("Could not set the cached object.");
      } 
    }

  /**
   * Generate template permutations 
   * 
   * @param templates - All personalization templates
   * @return Map - Map of template permutations by id
   * @throws com.ftd.osp.utilities.cacheMgr.exception.CacheException
   */    
    public Map generatePermutations(Map templates)
    {
        ArrayList<String> permutations;
        String templateId;
        StringTokenizer st;
        ArrayList<String> elements;
        Map<String,ArrayList> templatePermutations = new HashMap<String,ArrayList>();
        for(Iterator it = templates.keySet().iterator(); it.hasNext();)
        {
          permutations = new ArrayList<String>();
          elements = new ArrayList<String>();
          templateId = (String)it.next();
          st = new StringTokenizer(templateId, "_");
          while(st.hasMoreTokens())
          {
            // Obtains element name
            elements.add(st.nextToken());
            
            // Throw away element length
            if(st.hasMoreTokens())
              st.nextToken();
          }

          int[] indices;
          //String[] elements = {"date", "name1", "name2", "message1"};
          PermutationGenerator x = new PermutationGenerator (elements.size());
          StringBuffer permutation;
          while (x.hasMore ()) {
              permutation = new StringBuffer ();
              indices = x.getNext ();
              for (int i = 0; i < indices.length; i++) {
                  permutation.append (elements.get(indices[i]));
                  if((i + 1) < indices.length)
                    permutation.append(",");
              }
              permutations.add(permutation.toString());
          }
          templatePermutations.put(templateId, permutations);
        }
        return templatePermutations;
    }
    
    
  /**
   * Returns all templates 
   * 
   * @return List - List of template value objects
   * @throws com.ftd.osp.utilities.cacheMgr.exception.CacheException
   */    
    public List getTemplates()
    {
        ArrayList<ValueVO> values = new ArrayList<ValueVO>();
        ValueVO valueVO;
        String id;
        for(Iterator<String> it = this.templates.keySet().iterator(); it.hasNext();)
        {
            id = it.next();
            valueVO = new ValueVO();
            valueVO.setId(id);
            valueVO.setDescription(this.templates.get(id));
            values.add(valueVO);
        }
        return values;
    }


  /**
   * Returns all template permutations
   * 
   * @return List - List of template permutation value objects
   * @throws com.ftd.osp.utilities.cacheMgr.exception.CacheException
   */    
    public List getTemplatePermutations()
    {
        ArrayList<ValueVO> values = new ArrayList<ValueVO>();
        ValueVO valueVO;
        String id;
        ArrayList<String> permutations;
        for(Iterator<String> it = this.templatePermutations.keySet().iterator(); it.hasNext();)
        {
            id = (String)it.next();
            permutations = templatePermutations.get(id);
            for(Iterator<String> it2 = permutations.iterator(); it2.hasNext();)
            {
                valueVO = new ValueVO();
                valueVO.setId(id);
                valueVO.setDescription(it2.next());
                values.add(valueVO);
            }
        }
        return values;
    }

  /**
   * Returns all template permutations
   * 
   * @return List - List of template permutation value objects
   * @throws com.ftd.osp.utilities.cacheMgr.exception.CacheException
   */    
    public List getTemplatePermutations(String templateId)
    {
        String id = null;
        ValueVO valueVO = null;
        ArrayList<String> permutations;
        ArrayList<ValueVO> values = new ArrayList<ValueVO>();
  
        permutations = templatePermutations.get(templateId);
        for(Iterator<String> it = permutations.iterator(); it.hasNext();)
        {
            id = it.next();
            valueVO = new ValueVO();
            valueVO.setId(id);
            valueVO.setDescription(id);
            values.add(valueVO);
        }

        return values;
    }
}



//--------------------------------------
// Systematically generate permutations. 
//--------------------------------------


class PermutationGenerator {

  private int[] a;
  private BigInteger numLeft;
  private BigInteger total;

  //-----------------------------------------------------------
  // Constructor. WARNING: Don't make n too large.
  // Recall that the number of permutations is n!
  // which can be very large, even when n is as small as 20 --
  // 20! = 2,432,902,008,176,640,000 and
  // 21! is too big to fit into a Java long, which is
  // why we use BigInteger instead.
  //----------------------------------------------------------

  public PermutationGenerator (int n) {
    if (n < 1) {
      throw new IllegalArgumentException ("Min 1");
    }
    a = new int[n];
    total = getFactorial (n);
    reset ();
  }

  //------
  // Reset
  //------

  public void reset () {
    for (int i = 0; i < a.length; i++) {
      a[i] = i;
    }
    numLeft = new BigInteger (total.toString ());
  }

  //------------------------------------------------
  // Return number of permutations not yet generated
  //------------------------------------------------

  public BigInteger getNumLeft () {
    return numLeft;
  }

  //------------------------------------
  // Return total number of permutations
  //------------------------------------

  public BigInteger getTotal () {
    return total;
  }

  //-----------------------------
  // Are there more permutations?
  //-----------------------------

  public boolean hasMore () {
    return numLeft.compareTo (BigInteger.ZERO) == 1;
  }

  //------------------
  // Compute factorial
  //------------------

  private static BigInteger getFactorial (int n) {
    BigInteger fact = BigInteger.ONE;
    for (int i = n; i > 1; i--) {
      fact = fact.multiply (new BigInteger (Integer.toString (i)));
    }
    return fact;
  }

  //--------------------------------------------------------
  // Generate next permutation (algorithm from Rosen p. 284)
  //--------------------------------------------------------

  public int[] getNext () {

    if (numLeft.equals (total)) {
      numLeft = numLeft.subtract (BigInteger.ONE);
      return a;
    }

    int temp;

    // Find largest index j with a[j] < a[j+1]

    int j = a.length - 2;
    while (a[j] > a[j+1]) {
      j--;
    }

    // Find index k such that a[k] is smallest integer
    // greater than a[j] to the right of a[j]

    int k = a.length - 1;
    while (a[j] > a[k]) {
      k--;
    }

    // Interchange a[j] and a[k]

    temp = a[k];
    a[k] = a[j];
    a[j] = temp;

    // Put tail end of permutation after jth position in increasing order

    int r = a.length - 1;
    int s = j + 1;

    while (r > s) {
      temp = a[s];
      a[s] = a[r];
      a[r] = temp;
      r--;
      s++;
    }

    numLeft = numLeft.subtract (BigInteger.ONE);
    return a;

  }

}
