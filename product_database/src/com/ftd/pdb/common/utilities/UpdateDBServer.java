package com.ftd.pdb.common.utilities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;

public class UpdateDBServer extends Thread
{
	private ServerSocket serverSocket;
	public boolean listening;
	private int port;
	private int timeout;

	public UpdateDBServer()
	{
		serverSocket = null;
	    listening = true;
	    port = 7777;
	    timeout = 10000;
	}

	public void run()
	{
		try
		{
			serverSocket = new ServerSocket(port);
//			serverSocket.setSoTimeout(timeout);
		}
		catch (IOException e)
		{
			System.err.println("Could not listen on port: " + port + ".");
			System.exit(-1);
		}

		while (listening)
		{
			try
			{
				new UpdateDBServerThread(serverSocket.accept()).start();
			}
//			catch(SocketTimeoutException stoe)
//			{
//			}
			catch(IOException ioe)
			{
				ioe.printStackTrace();
			}
		}
		//System.out.println("Done with while loop");

		try
		{
			System.out.println("Closing the ServerSocket");
			serverSocket.close();
		}
		catch(IOException ioe)
		{
			ioe.printStackTrace();
		}
	}

	public static void main(String[] args)
	{
		UpdateDBServer updateDbServer = new UpdateDBServer();
		updateDbServer.start();

		BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
		String fromConsole = "";

		while(true)
		{
			try
			{
				fromConsole = stdIn.readLine();
			}
			catch(IOException ioe)
			{
				ioe.printStackTrace();
			}


			if(fromConsole.toLowerCase().equals("quit"))
			{
//				updateDbServer.interrupt();
				updateDbServer.listening = false;
//				try
//				{
//					Thread.sleep(10000);
//					System.out.println("Done sleeping");
//				}
//				catch(Exception e)
//				{}

				break;
			}
		}
		//System.out.println("Out of main while loop");
	}
}