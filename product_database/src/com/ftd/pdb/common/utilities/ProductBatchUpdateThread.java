package com.ftd.pdb.common.utilities;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pdb.common.valobjs.ProductVO;
import com.ftd.pdb.service.IProductMaintSVCBusiness;

import java.util.List;

/**
 * 09/17/2008 (CHU): This class does not seem to be used. Updated to
 * match the interface signature change in fixing defect 2619.
 */
public class ProductBatchUpdateThread extends Thread
{
    IProductMaintSVCBusiness productSVC;
    String productId;
    List novatorFeeds;
    Logger logger = new Logger("com.ftd.pdb.common.utilities.ProductBatchUpdateThread");
    
    public ProductBatchUpdateThread(IProductMaintSVCBusiness _productSVC, String prodId, List feeds)
    {
        super("ProductBatchUpdateThread");
        productSVC = _productSVC;
        productId = prodId;
        novatorFeeds = feeds;
    }

    public void run()
    {
        ProductVO productVO = new ProductVO();

        try
        {
            // Get the Product VO
            productVO = productSVC.getProduct(productId);

            // Save and Send to Novator
            productSVC.setProduct(productVO, novatorFeeds);
            logger.info("Product " + productId + " (" + productVO.getNovatorId() + ") saved");
        }
        catch(Exception e)
        {
            logger.error("Product " + productId + " (" + productVO.getNovatorId() + ") not saved");
        }
    }
}
