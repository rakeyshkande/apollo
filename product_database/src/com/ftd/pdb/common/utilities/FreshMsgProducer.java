package com.ftd.pdb.common.utilities;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pdb.common.ProductMaintConstants;
/*
import com.google.api.core.ApiFuture;
import com.google.api.core.ApiFutureCallback;
import com.google.api.core.ApiFutures;
import com.google.cloud.pubsub.v1.Publisher;
import com.google.pubsub.v1.TopicName;
import com.google.protobuf.ByteString; 
import com.google.pubsub.v1.PubsubMessage;
import com.google.pubsub.v1.TopicName;
*/

public class FreshMsgProducer {
   private static final String DEFAULT_TOPIC = "default";
   private Logger logger;
   
   public FreshMsgProducer() {
      logger = new Logger("com.ftd.pdb.common.utilities.FreshMsgProducer");
   }

   public void sendToMessageProducer(String _id, String _source, String _reason) throws Exception {
      HttpClient httpclient;
      GetMethod httpget = null;
      
      try {
         ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
         String url = configUtil.getFrpGlobalParm(ProductMaintConstants.FRESH_CONTEXT, ProductMaintConstants.FRESH_MSG_PRODUCER_URL);
         String timeoutStr = configUtil.getFrpGlobalParm(ProductMaintConstants.FRESH_CONTEXT, ProductMaintConstants.FRESH_MSG_PRODUCER_TIMEOUT);
         int timeout = Integer.parseInt(timeoutStr) * 1000;  // Convert seconds to millisec
         
         if (url == null || url.isEmpty()) {
            logger.info("Fresh Message Producer URL not defined in global_parms, so doing nothing for: " + _id + " - " + _reason);
            return;
         } else {
            logger.info("Project Fresh event triggered for: " + _id + ", reason=" + _reason);            
         }
         url = url.replace(ProductMaintConstants.FRESH_TOKEN_ID, _id);
         url = url.replace(ProductMaintConstants.FRESH_TOKEN_SOURCE, _source);
         logger.info("Sending to Project Fresh Message Producer microservice: " + url);
         
         // Send HTTP request to Message Producer 
         httpclient  = new HttpClient();
         httpget = new GetMethod(url);
         httpclient.getHttpConnectionManager().getParams().setConnectionTimeout(timeout);  
         httpclient.getHttpConnectionManager().getParams().setSoTimeout(timeout);          
         int result = httpclient.executeMethod(httpget);
         if (result != HttpStatus.SC_OK) {
           logger.error("Call to Project Fresh Message Producer microservice failed");
           throw new Exception("Fresh HTTP request failed: " + httpget.getStatusLine());
         }
      } finally {
         try {
             if (httpget != null) {
                  httpget.releaseConnection();
             }
         } catch (Exception e) {
             logger.warn(e);
         }
      }
      return;
   }
   
   
   
   /**
    * Sends message to appropriate topic on (Gooble PubSub) messaging system.
    * 
    * @param _prodId
    * @param _topic
    * @throws Exception
   public void sendToPubSub(String _prodId, String _topic) throws Exception {
      String defaultTopic = null;
      ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
      Publisher publisher = null;
      
      try {
         String projectId   = configUtil.getFrpGlobalParm(ProductMaintConstants.FRESH_CONTEXT, ProductMaintConstants.FRESH_PUBSUB_PROJECT_ID);
         String jsonMessage = configUtil.getFrpGlobalParm(ProductMaintConstants.FRESH_CONTEXT, ProductMaintConstants.FRESH_PUBSUB_MESSAGE);
         if (_topic == null || _topic.isEmpty()) {
            defaultTopic = configUtil.getFrpGlobalParm(ProductMaintConstants.FRESH_CONTEXT, ProductMaintConstants.FRESH_PUBSUB_TOPIC);
         } else {
            defaultTopic = _topic;
         }
         logger.info("Sending to PubSub - product id: " + _prodId + " topic: " + defaultTopic);
         
         TopicName topicObj = TopicName.of(projectId, defaultTopic);
         publisher = Publisher.newBuilder(topicObj).build();
         long unixTime = System.currentTimeMillis() / 1000L;
         jsonMessage = jsonMessage.replace(ProductMaintConstants.FRESH_TOKEN_ID, _prodId);
         jsonMessage = jsonMessage.replace(ProductMaintConstants.FRESH_TOKEN_TIME, String.valueOf(unixTime));
         ByteString bdata = ByteString.copyFromUtf8(jsonMessage);
         PubsubMessage pubsubMessage = PubsubMessage.newBuilder()
                                                    .setData(bdata)
                                                    .build();
         ApiFuture<String> messageIdFuture = publisher.publish(pubsubMessage);
         addCallBack(messageIdFuture);
         
      } finally {
         if (publisher != null) {
            publisher.shutdown();
         }
      }      
   }   
   
   private void addCallBack(ApiFuture<String> messageIdFuture) {
      ApiFutures.addCallback(messageIdFuture, new ApiFutureCallback<String>() {
         public void onSuccess(String msgId) {
           logger.info("Fresh pubsub posting successful " + msgId); 
         }
         public void onFailure(Throwable t) {
            logger.info("Fresh pubsub posting failure " + t); 
         }
      });
   }
   */
}
