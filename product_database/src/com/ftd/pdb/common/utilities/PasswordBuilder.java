package com.ftd.pdb.common.utilities;

import java.io.FileOutputStream;

public class PasswordBuilder 
{
    public PasswordBuilder()
    {
    }

    public String passwordEncrypt(String inPassword) throws Exception
    {
        String outPassword = "";
        if(inPassword != null)
        {
            outPassword = FTDUtil.encryptText(inPassword);
        }

        return outPassword;
    }

    public static void main(String[] args)
    {
        PasswordBuilder passwordBuilder = new PasswordBuilder();
        String password = "";
        try
        {
            FileOutputStream fos = new FileOutputStream("c:\\temp\\passwords.txt", false);
            for(int i = 0; i < args.length; i++)
            {
                try
                {
                    password = passwordBuilder.passwordEncrypt(args[i]);
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }

                System.out.println("password=" + password + ":");
                fos.write(password.getBytes());
                fos.write(System.getProperty("line.separator").getBytes());            
            }
            fos.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }

/*        Properties props = System.getProperties();
        //Enumeration enum = props.elements();
        Enumeration enum = props.keys();
        while(enum.hasMoreElements())
        {
            Object obj = enum.nextElement();
            System.out.println(obj + " = " + props.get(obj));
            
        }
*/
    }
}