package com.ftd.pdb.common.utilities;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import com.ftd.pdb.common.ProductMaintConstants;

public class UpdateDBServerThread extends Thread
{
    private Socket socket = null;
    private int timeout;

    public UpdateDBServerThread(Socket socket)
    {
		super("UpdateDBServerThread");
		this.socket = socket;
		timeout = 10000;
    }

    public void run()
    {
		try
		{
			PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
			BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

			String inputLine, outputLine;
            StringBuffer sb = new StringBuffer();
            /* ***Novator feed test for product maintenance using product GG23 **** *
            sb.append("<?xml version='1.0'?>\r\n");
			sb.append("<validationError>");
            sb.append("<error><field>");
            sb.append(ProductMaintConstants.PDB_NOVATOR_FIELD_NAME_TITLE);
            sb.append("</field><description>Needs to be set for this product.</description></error>");
            sb.append("<error><field>");
            sb.append(ProductMaintConstants.PDB_NOVATOR_FIELD_NAME_LONG_DESCRIPTION);
            sb.append("</field><description>Needs to be set for this product.</description></error>");
            sb.append("<error><field>");
            sb.append(ProductMaintConstants.PDB_NOVATOR_FIELD_NAME_TYPE);
            sb.append("</field><description>Needs to be set for this product.</description></error>");
            sb.append("<error><field>");
            sb.append(ProductMaintConstants.PDB_NOVATOR_FIELD_NAME_SUB_TYPE);
            sb.append("</field><description>Needs to be set for this product.</description></error>");
            sb.append("<error><field>");
            sb.append(ProductMaintConstants.PDB_NOVATOR_FIELD_NAME_VENDOR_ID);
            sb.append("</field><description>Needs to be set for this product.</description></error>");
            sb.append("<error><field>");
            sb.append(ProductMaintConstants.PDB_NOVATOR_FIELD_NAME_SHIPPING_KEY);
            sb.append("</field><description>Needs to be set for this product.</description></error>");
            sb.append("<error><field>");
            sb.append(ProductMaintConstants.PDB_NOVATOR_FIELD_NAME_SHIPPING_METHOD);
            sb.append("</field><description>Shipping Methods, common carrier and florist, should be both true for Sameday Gift or Sameday Freshcut.</description></error>");
            sb.append("<error><field>");
            sb.append(ProductMaintConstants.PDB_NOVATOR_FIELD_NAME_ADD_ON_FLAG);
            sb.append("</field><description>Specialty gifts and fresh cut products cannot have any add-ons.</description></error>");
            sb.append("<error><field>");
            sb.append(ProductMaintConstants.PDB_NOVATOR_FIELD_NAME_VALID_SHIP_DAYS);
            sb.append("</field><description>Needs to be set for this product.</description></error>");
            sb.append("<error><field>");
            sb.append(ProductMaintConstants.PDB_NOVATOR_FIELD_NAME_DELIVERY_OPTIONS);
            sb.append("</field><description>Need to set at least one of these fields: allow_standard, allow_two_day, allow_next_day, allow_saturday</description></error>");
            sb.append("<error><field>");
            sb.append(ProductMaintConstants.PDB_NOVATOR_FIELD_NAME_SUB_PRODUCT_ID_OPTION);
            sb.append("</field><description>Needs to be set for sub_product_id &lt;SUBCODE&gt;</description><subcode>G024</subcode></error>");
            
            sb.append("<error><field>");
            sb.append(ProductMaintConstants.PDB_NOVATOR_FIELD_NAME_SUB_PRODUCT_ID_OPTION);
            sb.append("</field><description>Needs to be set for sub_product_id &lt;SUBCODE&gt;</description><subcode>G025</subcode></error>");
            
            sb.append("<error><field>");
            sb.append(ProductMaintConstants.PDB_NOVATOR_FIELD_NAME_SUB_PRODUCT_ID_PRICE);
            sb.append("</field><description>Needs to be set for sub_product_id &lt;SUBCODE&gt;</description><subcode>G024</subcode></error>");
            
            sb.append("<error><field>");
            sb.append(ProductMaintConstants.PDB_NOVATOR_FIELD_NAME_SUB_PRODUCT_ID_PRICE);
            sb.append("</field><description>Needs to be set for sub_product_id &lt;SUBCODE&gt;</description><subcode>G025</subcode></error>");
            
            sb.append("<error><field>");
            sb.append(ProductMaintConstants.PDB_NOVATOR_FIELD_NAME_PRODUCT_ID);
            sb.append("</field><description>Maximum 10 characters for product ID</description><subcode>abc</subcode></error>");
            sb.append("</validationError>\r\nCON");
            outputLine = sb.toString();
            * **** END Novator feed test for product maintenance using product GG23 **** */

            //Successful product update
            //outputLine = "<update><product_id>JMP1</product_id><status>success</status></update>";

            /* **** Novator feed for upsell maintenance for product code FF01 **** */
            sb.append("<?xml version='1.0'?>\r\n");
			sb.append("<validationError>");
            sb.append("<error><field>");
            sb.append(ProductMaintConstants.PDB_NOVATOR_UPSELL_FIELD_NAME_TITLE);
            sb.append("</field><description>Maximum 100 characters for arrangement name exceeded.</description></error>");
            sb.append("<error><field>");
            sb.append(ProductMaintConstants.PDB_NOVATOR_UPSELL_FIELD_NAME_DESCRIPTION);
            sb.append("</field><description>Needs to be set for this product.</description></error>");
            sb.append("<error><field>");
            sb.append(ProductMaintConstants.PDB_NOVATOR_UPSELL_FIELD_NAME_DETAIL);
            sb.append("</field><description>A minimum of two associated SKUs is required.</description></error>");
            sb.append("<error><field>");
            sb.append(ProductMaintConstants.PDB_NOVATOR_UPSELL_FIELD_NAME_MASTER_ID);
            sb.append("</field><description>Maximum 10 characters for product ID.</description></error>");
            //Begin SKUs
            sb.append("<error><field>");
            sb.append(ProductMaintConstants.PDB_NOVATOR_UPSELL_FIELD_NAME_DETAIL_ID);
            sb.append("</field><description>Needs to be set for sub_product_id &lt;SUBCODE&gt;</description><subcode>F001</subcode></error>");
            sb.append("<error><field>");
            sb.append(ProductMaintConstants.PDB_NOVATOR_UPSELL_FIELD_NAME_DETAIL_TITLE);
            sb.append("</field><description>Needs to be set for sub_product_id &lt;SUBCODE&gt;</description><subcode>F001</subcode></error>");
            sb.append("<error><field>");
            sb.append(ProductMaintConstants.PDB_NOVATOR_UPSELL_FIELD_NAME_DETAIL_PRICE);
            sb.append("</field><description>Needs to be set for sub_product_id &lt;SUBCODE&gt;</description><subcode>F101</subcode></error>");
            sb.append("<error><field>");
            sb.append(ProductMaintConstants.PDB_NOVATOR_UPSELL_FIELD_NAME_DETAIL_TYPE);
            sb.append("</field><description>Needs to be set for sub_product_id &lt;SUBCODE&gt;</description><subcode>F101</subcode></error>");
            sb.append("<error><field>");
            sb.append(ProductMaintConstants.PDB_NOVATOR_UPSELL_FIELD_NAME_DETAIL_STATUS);
            sb.append("</field><description>Needs to be set for sub_product_id &lt;SUBCODE&gt;</description><subcode>8112</subcode></error>");
            //End SKUs
            sb.append("</validationError>\r\nCON");
            outputLine = sb.toString();
            /* **** END Novator feed for upsell maintenance for product code FF01 **** */
    
			socket.setSoTimeout(timeout);
			inputLine = in.readLine();

			System.out.println(inputLine);
            FileOutputStream fos = new FileOutputStream("c:\\temp\\in.xml");
            fos.write(inputLine.getBytes());
            fos.close();

            fos = new FileOutputStream("c:\\temp\\out.xml");
            fos.write(outputLine.getBytes());
            fos.close();
            
			out.println(outputLine);

			out.close();
			in.close();
			socket.close();
		}
		catch (IOException e)
		{
	    	e.printStackTrace();
		}
    }
}
