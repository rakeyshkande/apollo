package com.ftd.pdb.common.utilities;

import java.sql.Connection;
import java.sql.SQLException;

import com.ftd.osp.utilities.plugins.Logger;

/**
 * This class encapsulates auto-commit handling for transaction. In some contexts it is uncertain whether auto-commit 
 * can be set on a transaction or if that action will cause an exception. This class handles that uncertainty.
 * 
 * Intended use is to call setAutoCommit() and commitTransaction() as pairs, with setAutoCommit at the start of 
 * the "transaction" and commit at the end
 *
 */
public class TransactionHelper {
	private Logger logger = null;
	private boolean debugEnabled = true;
	private boolean autoCommitChanged = false; // was the auto-commit value for the connection successfully changed
	private Connection conn = null;
	private String callingContext = null; // description of where this helper class is being called from for logging purposes.

	/**
	 * Constructor
	 * @param logger - Logger class
	 * @param debugEnabled - should logging/debugging be performed
	 * @param ctx - the contexts from which this class is being used. This is for logging purposes. e.g. "ProductDAOImpl:setProduct()"
	 */
	public TransactionHelper(Logger logger, boolean debugEnabled, String ctx) {
		this.logger = logger;
		this.debugEnabled = debugEnabled;
		this.callingContext = ctx;
	}

	/**
	 * To be called at the start of a transaction, this method attempts to change the auto-commit setting on the specified connection,
	 * handling the exception if such a change is not allowed and retaining the success of that change for the commit action.
	 * @param conn - datasource connection
	 * @param newAutoCommit - desired new auto-commit value for the connection 
	 */
	public void setAutoCommit(Connection conn, boolean newAutoCommit) {
		this.conn = conn;
        if(debugEnabled)
        {
            logger.debug(callingContext + ": Attempting to set auto commit to " + newAutoCommit);
        }
        try {
        	conn.setAutoCommit(newAutoCommit);
        }
        catch (SQLException e) {
        	autoCommitChanged = false;
            logger.warn(callingContext + ": Failed setting auto commit to " + newAutoCommit + ". Processing will continue.");
        }
	}
	
	/**
	 * Commit a transaction using the connection specified in setAutoCommit as well as the information about the auto-commit setting
	 * from the same method. The commit is conditional upon auto-commit = false (meaning commits have not happened automatically) and 
	 * the connection's auto-commit value having been successfully changed in setAutoCommit. If it was not successfully changed the 
	 * assumption is that such a change is illegal and therefore so is a commit. Presumably the container will take care of the commit/it is
	 * a Container Managed Transaction (CMT). 
	 * @throws Exception
	 */
	public void commitTransaction() throws Exception {
		if (conn == null) 
			throw new Exception(callingContext + ": Invalid use, must set auto-commit prior to commiting transaction.");
		
        if (!conn.getAutoCommit() && autoCommitChanged) {
        	conn.commit();
        	if(debugEnabled)
        	{
        		logger.debug(callingContext + ": Committed the transaction");
        	}
        }
	}

}
