package com.ftd.pdb.common.utilities;

import java.util.List;
import java.io.IOException;

public interface ISpellCheck
{

  /**
    * The name of the file containing word dictionary,
    * this system property will be set at runtime
    */
    public static final String SMALL_DICTIONARY_FILE_NAME = "small.dictionary.file";
    public static final String MEDIUM_DICTIONARY_FILE_NAME = "medium.dictionary.file";
    public static final String LARGE_DICTIONARY_FILE_NAME = "large.directory.file";
    /**
    * Returns true if the word is spelled correctly, false if not.
    * @param String word to be checked
    * @return boolean
    **/
    public boolean spellCheck(String word) throws IOException;

    /**
     * Gets a list of spelling suggestions for a word
     * @param A word
     * @return List A Collection of suggestions.
     */
    public List suggest(String strWord);

    /**
     * Set the maximum suggestions for a given mis-spelled word.
     */
    public void setMaximumSuggestion ( int maxSuggestions );
}