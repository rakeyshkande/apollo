package com.ftd.pdb.common.utilities;

import com.ftd.pdb.common.valobjs.ShippingKeyVO;
import com.ftd.pdb.common.valobjs.ProductVO;


import com.ftd.pdb.service.IShippingKeySVCService;

import java.math.BigDecimal;

import java.util.Iterator;
import java.util.List;

/**
 * Utility class for user input validation
 * @author Ting Wang
 * @version 1.0
 */
public class UserInputValidator 
{
    
    /*Spring managed resources */
    private static IShippingKeySVCService shippingKeySVC;
    /*end Spring managed resources */
    
    
    public UserInputValidator()
    {
        
    }

 /**
  * Check to see if the input String could be converted to US dollar
  * @param String input dollar
  * @return boolean true if the input is valid dollar amount
  */
  
    public boolean isValidCurrenty (String dollar )
    {
        try {
            if (( dollar == null ) || dollar.equals(""))
            {
                return true;
            }
            dollar = FTDUtil.removeFormattingCurrency(dollar);
            if ( dollar.equals("")) {
                return false;
            }
            // make sure there are only 2 digits after the decimal
            if ( (dollar.length() - dollar.indexOf('.')) <= 3 || dollar.indexOf('.' ) == -1) {
              double value = Double.parseDouble( dollar );
              return true;
            }
        }
        catch (Throwable e) {
            return false;
        }
        return false;
    }
    public boolean isGreaterThanZero (String dollar )
    {
        try {
            if (( dollar == null ) || dollar.equals(""))
            {
                // This isn't greater than zero of course, but we must deal
                // with blank entries
                return true;
            }
            dollar = FTDUtil.removeFormattingCurrency(dollar);
            if ( dollar.equals("")) {
                return false;
            }
            double value = Double.parseDouble( dollar );
            if (value > 0.00) {
                return true;
            }
            else {
                return false;
            }
        } 
        catch (Throwable e) {
            return false;
        }
    }
    public boolean isInPriceRange (String dollar )
    {
        try {
            if (( dollar == null ) || dollar.equals(""))
            {
                // This isn't in our price range but we must deal
                // with blank entries
                return true;
            }
            dollar = FTDUtil.removeFormattingCurrency(dollar);
            if ( dollar.equals("")) {
                return false;
            }
            double value = Double.parseDouble( dollar );
            if (value < 10000 && value > 0) {
                return true;
            }
            else {
                return false;
            }
        } 
        catch (Throwable e) {
            return false;
        }
    }
    
    /**
     * 
     * @param previousValue
     * @param nextValue
     * @return boolean
     * This function will check if the amounts are in order and do not leave a gap
     * For example, if the max value is 10.00 the expected next min value is 10.01
     */
    public boolean isMinValueValid (String lastMaxValue, String minValue, String maxValue )
    {
        try {
            
            minValue = FTDUtil.removeFormattingCurrency(minValue);
            maxValue = FTDUtil.removeFormattingCurrency(maxValue);
            lastMaxValue = FTDUtil.removeFormattingCurrency(lastMaxValue);


            //This gets called even on the blank rows.  We must not report an error when the entries are blank
            
            if ( ((lastMaxValue == null ) || lastMaxValue.equals("")) && ((minValue == null ) || minValue.equals("")))
            {
                return true;
            }
            //If neither of the current fields are filled, we call it good
            if ( ((maxValue == null ) || maxValue.equals("")) && ((minValue == null ) || minValue.equals("")))
            {
                return true;
            }

            if (( minValue == null ) || minValue.equals(""))
            {
                // only the first min value can be null.  But since this can be run a
                return false;
            }
            if (( lastMaxValue == null ) || lastMaxValue.equals(""))
            {
                // only the last max value can be null
                return false;
            }
            BigDecimal lastMaxValueBD = new BigDecimal(lastMaxValue) ;
            BigDecimal minValueBD = new BigDecimal (minValue);
            BigDecimal difference = new BigDecimal("0.01");
            
            if ( lastMaxValueBD.add(difference).compareTo(minValueBD) == 0 )
            {
                return true;
            }
            else {
                return false;
            }
        } 
        catch (Throwable e) {
            return false;
        }
    }
    
    /**
     * This function will ensure that the max value is greater than the minimum value
     * For example, if the mininum value is 10.00 the expected max value is >= 10.01
     * @param minValue
     * @param maxValue
     * @return boolean
     */
    
    public boolean isMaxValueGreaterThanMinValue (String minValue, String maxValue )
    {
        try {
            
            minValue = FTDUtil.removeFormattingCurrency(minValue);
            maxValue = FTDUtil.removeFormattingCurrency(maxValue);
            
            if (( minValue == null ) || minValue.equals(""))
            {
                return true;
            }
            if (( maxValue == null ) || maxValue.equals(""))
            {
                return true;
            }
        
            double minValueDouble = Double.parseDouble( minValue );
            double maxValueDouble = Double.parseDouble( maxValue );

            
            if ( minValueDouble < maxValueDouble) 
            {
                return true;
            }
            else {
                return false;
            }
        } 
        catch (Throwable e) {
            return false;
        }
        
    }
    
    /**
     * This function will detect duplicate entries in populated price rows
     * @param priceArray 
     * An array of prices in string form to check for duplicates in
     * @param 
     * populated An array of booleans that specifies if the row is populated or not
     * @return
     * true if there are no duplicates, else false
     */
    public boolean isThereDuplicatePrices (String[] priceArray, boolean [] populated) throws Throwable
    {
        try 
        {
            for (int i=0; i < priceArray.length; i++)
            {
                priceArray[i] = FTDUtil.removeFormattingCurrency(priceArray[i]);
            }
            for (int i=0; i < priceArray.length; i++)
            {
                if (populated[i])
                {     
                    for (int j=0; j < priceArray.length; j++)
                    {
                        // of course the value will be equal to itself
                        // skip this case
                        if ( i != j)
                        {
                             if ( populated[j] && (priceArray[i].equals(priceArray[j]) || (priceArray[i] == null && priceArray[j] == null) ) )
                            {     
                                //We've detected a dupe
                                return true;
                            }   
                        }       
                    }        
                }
            }
        }
        catch (Throwable e) 
        {
            throw (e);      
        }
        //We made it all the way through.  There must've been no duplicates
        return false;
    }
    
    /**
     * This function checks to see if a description is already in use
     * This should be called on an edi of a shipping key as it errors whenever a shipping key 
     * with this description is found that does not match the shipping key id that is passed
     * @param description -- Description of the key that is to be added
     * @return
     */
    
    public boolean isDuplicateDescriptionEdit(String description, String shippingKey) throws Throwable
    {
        try
        {
            List shippingKeyVOList = shippingKeySVC.getShippingKeyList();
        
            for (Iterator shippingKeyIterator = shippingKeyVOList.iterator (); shippingKeyIterator.hasNext ();) {
                ShippingKeyVO vo = (ShippingKeyVO)shippingKeyIterator.next();
                if (vo.getShippingKeyDescription().trim().equals(description.trim()) && ! vo.getShippingKey_ID().equals(shippingKey))
                //There was a match on a shipping key that is not ours.  Time to error out.
                {
                    return true;
                }
            }   
        }
        catch (Throwable e) 
        {
            throw (e);
        }
        //We made it all the way through.  There must've been no duplicates
        return false;
    }
    
    
    /**
     * Checks if there are products on a shipping key
     * @param shippingKey
     * @return
     * @throws Throwable
     */
    
    
    public boolean isShippingKeyHasProducts (String shippingKey) throws Throwable
    {    
        try 
        {
            ShippingKeyVO vo = shippingKeySVC.getShippingKey(shippingKey);
            ProductVO [] products = vo.getProducts();
            if (products.length > 0) 
            {
                return true;
            }
            else
            {
                return false;
            }
        }
            
        catch (Throwable e) 
        {
            throw (e);
        }    
    }
    
    /**
     * This function checks to see if a description is already in use
     * This should be called on an add of a shipping key as it errors whenever a shipping key with this description is found
     * @param description -- Description of the key that is to be added
     * @return
     */
    
    public boolean isDuplicateDescriptionAdd(String description) throws Throwable
    {
        try
        {
            List shippingKeyVOList = shippingKeySVC.getShippingKeyList();
        
            for (Iterator shippingKeyIterator = shippingKeyVOList.iterator (); shippingKeyIterator.hasNext ();) {
                ShippingKeyVO vo = (ShippingKeyVO)shippingKeyIterator.next();
                if (vo.getShippingKeyDescription().trim().equals(description.trim()))
                //There was a match on a shipping key that is not ours.  Time to error out.
                {
                    return true;
                }
            }   
        }
        catch (Throwable e) 
        {
            throw (e);
        }
        //We made it all the way through.  There must've been no duplicates
        return false;
    }
 
 
    /**
     * This function ensures that only the first min price is allowed to be blank
     * @param priceArray
     * @param populated
     * @return
     * @throws Throwable
     */
    
    public boolean isOnlyFirstMinPriceBlank (String[] priceArray, boolean [] populated) throws Throwable
    {
        boolean populatedLineHit = false;
        for (int i=0; i< populated.length; i++)
        {
            if (populated[i])
            {
                if (populatedLineHit  && priceArray[i].equals("") )
                {
                    return false;
                }
            populatedLineHit = true;
            }
        }
        return true;
    }
    
    /**
     * This function ensures that only the last populated row is allowed to be blank 
     * @param priceArray
     * @param populated
     * @return
     * @throws Throwable
     */
    
    public boolean isOnlyLastMaxPriceBlank (String[] priceArray, boolean [] populated) throws Throwable
    {
        boolean populatedLineHit = false;
        
        for (int i=populated.length - 1; i >= 0; i--)
        {
            if (populated[i])
            {
                if (populatedLineHit && priceArray[i].equals(""))
                {
                    return false;
                }
            populatedLineHit = true;
            }
        }
        return true;
        
    }
    
    public void setShippingKeySVC(IShippingKeySVCService shippingKeySVC) {
        this.shippingKeySVC = shippingKeySVC;
    }
}