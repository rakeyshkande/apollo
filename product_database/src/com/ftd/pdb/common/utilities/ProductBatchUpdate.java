package com.ftd.pdb.common.utilities;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.valobjs.ProductVO;
import com.ftd.pdb.common.valobjs.ValueVO;

import com.ftd.pdb.service.IProductMaintSVCBusiness;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;


public class ProductBatchUpdate {
    /*Spring managed resources*/
    IProductMaintSVCBusiness productSVC;
    /*end Spring managed resources*/

    private static int count;

    public ProductBatchUpdate() {
        count = 0;
    }

    public void runBatch(String productIDFile, List novatorFeeds) {

        FileInputStream fis = null;

        try {
            fis = new FileInputStream(productIDFile);
        } catch (FileNotFoundException fnfe) {
            System.out.println("File " + productIDFile + " not found");
        }

        StringBuffer sb = new StringBuffer();
        int b;
        try {
            while ((b = fis.read()) != -1) {
                sb.append((char)b);
            }


        } catch (IOException ioe) {
            System.out.println("Could not read " + productIDFile);
        }

        String inString = sb.toString();

        StringTokenizer tokenizer = 
            new StringTokenizer(inString, "\r\n", false);

        try {
            productSVC.getProduct("XXXX");
        } catch (Exception e) {
            System.out.println("Error loading database connection");
        }

        while (tokenizer.hasMoreTokens()) {
            String productId = tokenizer.nextToken();
            ProductVO productVO = new ProductVO();

            try {
                // Get the Product VO
                productVO = productSVC.getProduct(productId);

                // Save and Send to Novator
                productSVC.setProduct(productVO, novatorFeeds);
                
                count++;
                System.out.println("Product " + productId + " (" + 
                                   productVO.getNovatorId() + ") saved");
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Product " + productId + " (" + 
                                   productVO.getNovatorId() + ") not saved");
                //                break;
            }

        }

        System.out.println(count + " products saved");
    }

    public static void main(String[] args) {
        List<String> envKeys = new ArrayList();
        ProductBatchUpdate batch = new ProductBatchUpdate();

        try {
            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();

            if (args[0] == null || args[0].length() < 1) {
                System.out.println("Error: No product ID file supplied.");
                System.exit(0);
            }

            for (int i = 1; i < args.length; i++) {
                ValueVO mapVO = new ValueVO();
                if (args[i] != null && args[i].equalsIgnoreCase("test")) {
                    envKeys.add(com.ftd.osp.utilities.feed.NovatorFeedUtil.TEST);
                } else if (args[i] != null && 
                           args[i].equalsIgnoreCase("uat")) {
                    envKeys.add(com.ftd.osp.utilities.feed.NovatorFeedUtil.UAT);
                } else if (args[i] != null && 
                           args[i].equalsIgnoreCase("content")) {
                    envKeys.add(com.ftd.osp.utilities.feed.NovatorFeedUtil.CONTENT);
                } else if (args[i] != null && 
                           args[i].equalsIgnoreCase("live")) {
                    envKeys.add(com.ftd.osp.utilities.feed.NovatorFeedUtil.PRODUCTION);
                }
            }

            batch.runBatch(args[0], envKeys);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setProductSVC(IProductMaintSVCBusiness productSVC) {
        this.productSVC = productSVC;
    }

    public IProductMaintSVCBusiness getProductSVC() {
        return productSVC;
    }
}
