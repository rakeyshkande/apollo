package com.ftd.pdb.common.utilities;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBSystemException;
import com.ftd.pdb.common.valobjs.NovatorResponseVO;
import com.ftd.pdb.common.valobjs.PDBValueObject;
import com.ftd.pdb.common.valobjs.ValueVO;

import com.sun.org.apache.xpath.internal.NodeSet;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.w3c.dom.Document;
import com.ftd.osp.utilities.SendUpdateFeed;

import com.ftd.osp.utilities.plugins.Logger;

import org.w3c.dom.Element;

public class NovatorTransmission 
{

  protected static Logger logger = new Logger("com.ftd.pdb.common.utilities.NovatorTransmission");

  public NovatorTransmission()
  {
  }

  /**
   * Send the passed in XML message to Novator.
   * The passed in header will be used as the XML header.
   * @param header xml header
   * @param vo ValueObject containing data to send
   * @param saveNovatorFeeds map of what novator instances to update
   *
   */
    public void send(String header, PDBValueObject vo, Map saveNovatorFeeds) throws PDBApplicationException, PDBSystemException
    {
        ConfigurationUtil configUtil;
        int timeout;
        
        try {
           configUtil = ConfigurationUtil.getInstance();
           timeout = Integer.parseInt(configUtil.getProperty(ProductMaintConstants.PROPERTY_FILE, ProductMaintConstants.PDB_NOVATOR_TIMEOUT_KEY));
        } catch (Exception e) {
            String[] args = {e.getMessage()};
            throw new PDBApplicationException(ProductMaintConstants.PDB_NOVATOR_EXCEPTION, args);
        }

        String xml = header + vo.toXML();
        logger.debug("Feeding xml to Novator:\n" + xml);
        String errorString = "";
        
        // Send the configured feeds to Novator
        Set keys = saveNovatorFeeds.keySet();
        Iterator it = keys.iterator();
        while(it.hasNext())
        {
            String key = (String)it.next();
            ValueVO value = (ValueVO)saveNovatorFeeds.get(key);
            
//            SendUpdateFeed sender = new SendUpdateFeed(value.getId(), Integer.parseInt(value.getDescription()), timeout);
/*          try {
                FileOutputStream fos = new FileOutputStream("c:\\temp\\out.xml");
                fos.write(xml.getBytes());
                fos.close();
            } catch (Exception e) 
            {
                
            }
*/

            try {
                NovatorResponseVO responseVO = parseResponse(SendUpdateFeed.send(value.getId(), Integer.parseInt(value.getDescription()), timeout, xml));
                //could response be parsed?
                if (responseVO.isParseable()){
                    //did an error occur?
                    if(!responseVO.isSuccess()){
                        errorString = responseVO.getDescription();
                        if (errorString.equalsIgnoreCase("")){
                            errorString = "<BR>" + key.toUpperCase() + ":" +"Could not interrupt Novator's response:" + responseVO.getDescription();
                        }
                    }//end if success
                } 
                else{
                    errorString = "<BR>" + key.toUpperCase() + ":" +"Could not interrupt Novator's response:" + responseVO.getDescription();
                }//end parse response         
            }catch(Exception e){
                String[] args = {e.getMessage()};
                throw new PDBSystemException(ProductMaintConstants.PDB_UNKNOWN_HOST_EXCEPTION, args, e);
            }

        }//end Novator send loop
        
        //if any error occured throw exception
        if (!errorString.equals("")){
            String[] args = {errorString};
            throw new PDBApplicationException(ProductMaintConstants.PDB_NOVATOR_EXCEPTION, args);        
        }
    }  

    /**
     * Converts the passed in VO field name to the name required by Novator.
     *
     * !! Note !! In the future it may be possible to rewrite this funcion
     *            so that all the key mappings are loaded into a hash table
     *            based on values in configuration file, search can then
     *            be done against that table.  This would provide for a 
     *            more configurable solution.
     *
     * Creation date: (9/03/02)
     * @return java.lang.String - field name
     * @param field Novator field name
     */
    public static synchronized String getNovatorFieldName(String field) throws Exception
    {
        String novatorField=null;

        if( novatorFields==null )
            novatorFields = new HashMap(100);
            
        if( novatorFields.containsKey((Object)field) ) 
        {
            novatorField=(String)(novatorFields.get(field));
        }
        else
        {
            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
            try 
            {
                novatorField = configUtil.getProperty(ProductMaintConstants.NOVATOR_MAPPING_FILE, field);
            } catch (Exception e) {
                novatorField=null;
            }

            novatorFields.put(field,novatorField);
        }
        return novatorField;
    }

    /**
     * Interrupts response from Novator
     *
     *
     * Creation date: (4/18/03) -ed
     * @return java.lang.String - NovatorResponseVO
     * @param novatorResponse Novator response string
     */
    private static NovatorResponseVO parseResponse(String novatorResponse)
    {
        NovatorResponseVO responseVO = new NovatorResponseVO();

        // Some times the response comes back with CON on the end.  
        // Remove CON if present
        if(novatorResponse.endsWith("CON"))
        {
            novatorResponse = novatorResponse.substring(0,(novatorResponse.length() - 3));
        }

        //parse string in XML document
        try
        {
            Document doc = JAXPUtil.parseDocument(new String(novatorResponse.getBytes()));

            //attempt to get update tab
            Element updateElement = JAXPUtil.selectSingleNode(doc, "//update");

            if (updateElement == null )    	
            {
                //update was not found...look for Validation Error
                Element errorElement = JAXPUtil.selectSingleNode(doc, "//validationError");
                if (errorElement ==  null )  
                {
                    //Not Validation Error...dont know what this message means
                    responseVO.setSuccess(false);
                    responseVO.setParseable(false);
                    responseVO.setDescription(novatorResponse);                                            
                }
                else
                {
                    //We got a validation error
                    responseVO.setSuccess(false);
                    responseVO.setParseable(true);
                    responseVO.setDescription(JAXPUtil.getTextValue(errorElement));
                }           
            }    
            else 
            {
                //We got an update message
                //Get Status
                String status = "";
                Element statusElement = JAXPUtil.selectSingleNode(doc, "//update/status");
                if(statusElement != null)
                    status = JAXPUtil.getTextValue(statusElement);
                    
                if (status.equals("success"))
                {            
                    responseVO.setSuccess(true);
                    responseVO.setParseable(true);
                    responseVO.setDescription("");
                }
                else
                {
                    responseVO.setSuccess(false);
                    responseVO.setParseable(true);
                    responseVO.setDescription(status);                    
                }
            }        
        }//end try        
        catch (Throwable ioe)    	
        {
            //Could not parse Novator response            
            responseVO.setSuccess(false);
            responseVO.setParseable(false);
            responseVO.setDescription(novatorResponse);                	
        }

        return responseVO;
    }

    private static HashMap novatorFields;  
}