package com.ftd.pdb.common.utilities;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.exceptions.PDBSystemException;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;


/**
     * SendUpdateFeed
     * This thread send a socket message.
     */
public class SendUpdateFeed 
{
//    private Socket socket;
//    private PrintWriter out;
//    private BufferedReader in;
//    private int portNumber;
//    private String serverName;
//    private int timeout;
    
    private static final Logger LOGGER = new Logger(ProductMaintConstants.PDB_COMMON_UTILITIES_CATEGORY_NAME);

    /**
    * Constructor for the SendUpdateFeed
    *
    */
    public SendUpdateFeed()
    {
//        socket = null;
//        out = null;
//        in = null;
//        portNumber = 7777;
//        serverName = "localhost";
//        timeout = 30000;
    }

//    /**
//    * Constructor for the SendUpdateFeed
//    *
//    * @param server name
//    * @param port for server
//    * @param socketTimeout  if zero then no timeout is set.
//    */
//    public SendUpdateFeed(String server, int port, int socketTimeout)
//    {
//        socket = null;
//        out = null;
//        in = null;
//
//        serverName = server;
//        portNumber = port;
//        timeout = socketTimeout;
//    }
    
  /**
   * Send data over the socket.
   * @param serverName server at novator
   * @param portNumber http port
   * @param timeout socket timeout
   * @param transmitData data to be sent
   * @return java.lang.String - socket response
   */
    public static String send(String serverName, int portNumber, int timeout, String transmitData) throws PDBSystemException
    {
        String response = "";
        Socket socket;
        PrintWriter out;
        BufferedReader in;

        try 
        {
            LOGGER.debug("SendUpdateFeed: Connecting to " + serverName + " on port " + portNumber + " for feed upload");
            // JDK 1.4
            InetSocketAddress address = new InetSocketAddress(serverName, portNumber);
            socket = new Socket(serverName, portNumber);
            socket.setSoTimeout(timeout);
            socket.connect(address, timeout);

            out = new PrintWriter(socket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        }
        catch (UnknownHostException e)
        {            
            String[] args = {serverName};
            LOGGER.error(e);
            throw new PDBSystemException(ProductMaintConstants.PDB_UNKNOWN_HOST_EXCEPTION, args, e);
        }
        catch (IOException e)
        {
            e.printStackTrace();
            String[] args = {serverName};
            LOGGER.error(e);
            throw new PDBSystemException(ProductMaintConstants.PDB_IO_EXCEPTION, args, e);        
        }

        LOGGER.debug("SendUpdateFeed: Sending " + transmitData + " to " + serverName + " on port " + portNumber);
        out.println(transmitData);
        out.flush();
        
        LOGGER.debug("SendUpdateFeed: Data sent");
        
        try
        {
            LOGGER.debug("SendUpdateFeed: Waiting for response");
            StringBuffer sb = new StringBuffer();
            response = in.readLine();
            while( response!=null && response.length()>0 ) {
                sb.append(response);
                response = in.readLine();
            }
            response = sb.toString();
            LOGGER.debug("Novator Response:" + response);
        } 
        catch(SocketTimeoutException stoe)
        {
            String[] args = {serverName};
            LOGGER.error(stoe);
            throw new PDBSystemException(103, args, stoe);
        }
        catch(IOException ioe)
        {
            String[] args = {serverName};
            LOGGER.error(ioe);
            throw new PDBSystemException(ProductMaintConstants.PDB_IO_EXCEPTION, args, ioe);   
        } 
        finally {
            try
            {
        	out.close();
       		in.close();
        	socket.close();
		}
		catch(IOException ioe)
		{
            String[] args = {serverName};
            LOGGER.error(ioe);
            throw new PDBSystemException(ProductMaintConstants.PDB_IO_EXCEPTION, args, ioe);   
		}
        }
        
        LOGGER.debug("Ending send method");

        return response;

    }

    public static void main(String[] args)
    {
        FileInputStream fis = null;
        
        try
        {
            fis = new FileInputStream(args[2]);
        }
        catch(FileNotFoundException fnfe)
        {
            System.out.println("File " + args[2] + " not found");
        }

        StringBuffer sb = new StringBuffer();
        int b;
        try
        {
            while((b = fis.read()) != -1)
            {
                sb.append((char)b);
            }
        }
        catch(IOException ioe)
        {
            System.out.println("Could not read " + args[2]);
        }
        
        String inString = sb.toString();

        try
        {
            send(args[0], Integer.parseInt(args[1]),0,inString);
        }
        catch(PDBSystemException ftdse)
        {
           ftdse.printStackTrace();
        }

        try
        {
            fis.close();
        }
        catch(IOException ioe)
        {
            System.out.println("Could not clode file " + args[2]);
        }
        
        System.out.println("XML sent to " + args[0] + ":" + args[1]);
    }
}
