package com.ftd.pdb.common.utilities;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pdb.common.ProductMaintConstants;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;



public class GrouponMsgProducer {
   private Logger logger;
   
   public GrouponMsgProducer() {
      logger = new Logger("com.ftd.pdb.common.utilities.GrouponMsgProducer");
   }
   
   /**
    * Sends message to appropriate topic on (Kafka) messaging system.
    * If topic is null, then we default to appropriate topic based on 
    * whether or not product is available. 
    * 
    * @param _msg
    * @param _topic
    * @param _prodAvail
    * @throws Exception
    */
   public void sendToMessageProducer(String _msg, String _topic, boolean _prodAvail) throws Exception {
      HttpClient httpclient;
      GetMethod httpget = null;
      String defaultTopic = null;
      
      try {
         ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
         String url = configUtil.getFrpGlobalParm(ProductMaintConstants.GROUPON_CONTEXT, ProductMaintConstants.GROUPON_MSG_PRODUCER_URL);
         String timeoutStr = configUtil.getFrpGlobalParm(ProductMaintConstants.GROUPON_CONTEXT, ProductMaintConstants.GROUPON_MSG_PRODUCER_TIMEOUT);
         int timeout = Integer.parseInt(timeoutStr) * 1000;  // Convert seconds to millisec
         
         if (_topic == null || _topic.isEmpty()) {
            if (_prodAvail) {
               // Since product is available, send to product modified topic
               defaultTopic = configUtil.getFrpGlobalParm(ProductMaintConstants.GROUPON_CONTEXT, ProductMaintConstants.GROUPON_MSG_PRODUCER_TOPIC);               
            } else {
               // Since product is unavailable, send to product deleted topic
               defaultTopic = configUtil.getFrpGlobalParm(ProductMaintConstants.GROUPON_CONTEXT, ProductMaintConstants.GROUPON_MSG_PRODUCER_TOPIC_DEL);               
            }
         }
         if (url == null || url.isEmpty()) {
            logger.info("Groupon Message Producer URL not defined in global_parms, so doing nothing for: " + _msg + 
                        " (" + (_topic == null ? defaultTopic : _topic) + ")");
            return;
         }
         url = url.replace(ProductMaintConstants.GROUPON_TOKEN_MSG, _msg);
         url = url.replace(ProductMaintConstants.GROUPON_TOKEN_TOPIC, (_topic == null ? defaultTopic : _topic));
         logger.info("Sending message to Groupon Message Producer microservice: " + url);
         
         // Send HTTP request to Message Producer 
         httpclient  = new HttpClient();
         httpget = new GetMethod(url);
         httpclient.getHttpConnectionManager().getParams().setConnectionTimeout(timeout);  
         httpclient.getHttpConnectionManager().getParams().setSoTimeout(timeout);          
         int result = httpclient.executeMethod(httpget);
         if (result != HttpStatus.SC_OK) {
           logger.error("Call to Groupon Message Producer microservice failed");
           throw new Exception("Groupon HTTP request failed: " + httpget.getStatusLine());
         }
      } finally {
         try {
             if (httpget != null) {
                  httpget.releaseConnection();
             }
         } catch (Exception e) {
             logger.warn(e);
         }
      }
      return;
   }
   
}
