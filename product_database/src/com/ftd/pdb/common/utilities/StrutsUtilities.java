package com.ftd.pdb.common.utilities;

import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionMessages;

public class StrutsUtilities {
    public static ActionMessages getMessages(HttpServletRequest request) {
        ActionMessages messages =
            (ActionMessages) request.getAttribute(Globals.MESSAGE_KEY);
        if (messages == null) {
                messages = new ActionMessages();
        }
        return messages;
    }
    
    public static ActionMessages getErrors(HttpServletRequest request) {
        ActionMessages errors =
            (ActionMessages) request.getAttribute(Globals.ERROR_KEY);
        if (errors == null) {
            errors = new ActionMessages();
        }
        return errors;
    }
    
    public static boolean isFieldInMessage(ActionMessages messages, String property) {
        if( messages==null || property==null ) {
            return false;
        }
        
        return messages.size(property)>0?true:false;
    }
    
    public static boolean isFieldInError(HttpServletRequest request, String property) {
        if( property==null ) {
            return false;
        }
        
        ActionMessages errors = getErrors(request);
        
        return errors.size(property)>0?true:false;
    }
    
    public static boolean isFieldInMessage(HttpServletRequest request, String property) {
        if( property==null ) {
            return false;
        }
        
        ActionMessages errors = getMessages(request);
        
        return errors.size(property)>0?true:false;
    }
}
