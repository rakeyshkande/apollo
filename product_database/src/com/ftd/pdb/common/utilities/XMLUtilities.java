package com.ftd.pdb.common.utilities;

import com.ftd.pdb.common.exceptions.XMLParsingException;

import com.sun.org.apache.xpath.internal.NodeSet;

import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.lang.StringUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;


public class XMLUtilities {

    private static final DocumentBuilderFactory DOCUMENT_BUILDER_FACTORY = DocumentBuilderFactory.newInstance();
    private static final TransformerFactory XFORM_FACTORY = TransformerFactory.newInstance();
    private static final XPathFactory XPATH_FACTORY = XPathFactory.newInstance();
    
    public static String toString(Document document) throws XMLParsingException {
        String retval = null;
        
        try {
            //TransformerFactory xformFactory 
            //       = TransformerFactory.newInstance();
            Transformer idTransform = XFORM_FACTORY.newTransformer();
            Source input = new DOMSource(document);
            StringWriter sw = new StringWriter();
            Result output = new StreamResult(sw);
            idTransform.transform(input, output);
            retval = sw.toString();
        } catch (Exception e) {
            throw new XMLParsingException(e);
        }
        
        return retval;
    }
    
    public static String toString(Element element) throws XMLParsingException {
        String retval = null;
        
        try {
            //TransformerFactory xformFactory 
            //       = TransformerFactory.newInstance();
            Transformer idTransform = XFORM_FACTORY.newTransformer();
            Source input = new DOMSource(element);
            StringWriter sw = new StringWriter();
            Result output = new StreamResult(sw);
            idTransform.transform(input, output);
            retval = output.toString();
        } catch (Exception e) {
            throw new XMLParsingException(e);
        }
        
        return retval;
    }
    
    public static Document parseDocument(String strDocument) throws XMLParsingException {
        Document d = null;
        try {
            //DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder parser = DOCUMENT_BUILDER_FACTORY.newDocumentBuilder();
            d = parser.parse(strDocument);
        } catch (Exception e) {
            throw new XMLParsingException(e);
        }
        return d;
    }
    
    public static Document createDocument() throws XMLParsingException {
        Document d = null;
        try {
            //DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder parser = DOCUMENT_BUILDER_FACTORY.newDocumentBuilder();
            d = parser.newDocument();
        } catch (Exception e) {
            throw new XMLParsingException(e);
        }
        return d;
    }
    
    public static boolean getBooleanValue(Element e) 
    {
        boolean retval;
        
        if( e==null )
        {
            retval = false;
        }
        else
        {
            retval = FTDUtil.convertStringToBoolean(StringUtils.trim(e.getNodeValue()));
        }
        
        return retval;
    }
    


        /**
         * Description:     Takes a tag name and text and creates a simple xml node
         * Creation date:   07/09/2003
         * @author          TPeterson
         * @param           doc parent XML document
         * @param:          tag the xml tag name
         * @param:          text the value to be inserted into the node
         * @return:         xml element
         */
        public static Element buildSimpleXmlNode(Document doc, String tag, String text)
        {
            Element retElem = doc.createElement(tag);
            
            retElem.appendChild(doc.createTextNode((String)FTDUtil.convertNulls(text)));
            return retElem;
        }

        /**
         * Description:     Takes a tag name and text and creates a simple xml node 
         *                  with CDATA.
         * @param           doc parent document
         * @param           tag the xml tag name
         * @param           text the value to be inserted into the node
         * @return          xml element
         */
        public static Element buildSimpleXmlNodeCDATA(Document doc, String tag, String text)
        {
            Element retElem = doc.createElement(tag);
            retElem.appendChild(doc.createCDATASection((String)FTDUtil.convertNulls(text)));
            return retElem;
        }


        /**
         * Description:     Takes a tag name and boolean value and creates a simple xml node
         * Creation date:   07/09/2003
         * @author          TPeterson
         * @param           tag the xml tag name
         * @param           value the value to be inserted into the node
         * @return          xml element
         */
        public static Element buildSimpleXmlNode(Document doc, String tag, boolean value)
        {
            Element retElem = doc.createElement(tag);
            retElem.setNodeValue(String.valueOf(value));
            return retElem;
        }


        /**
         * Description:     Takes a tag name and int value and creates a simple xml node
         * Creation date:   07/09/2003
         * @author:                 TPeterson
         * @param           doc parent document
         * @param           tag the xml tag name
         * @param           value the value to be inserted into the node
         * @return          xml element
         */
        public static Element buildSimpleXmlNode(Document doc, String tag, int value)
        {
            Element retElem = doc.createElement(tag);
            retElem.setNodeValue(String.valueOf(value));
            return retElem;
        }


        /**
         * Description:     Takes a tag name and float value and creates a simple xml node
         * Creation date:   07/09/2003
         * @author         TPeterson
         * @param          tag the xml tag name
         * @param          value the value to be inserted into the node
         * @return         xml node
         */
        public static Element buildSimpleXmlNode(Document doc, String tag, float value)
        {
            Element retElem = doc.createElement(tag);
            retElem.setNodeValue(String.valueOf(value));
            return retElem;
        }
        
        public static Element selectSingleNode(Document doc, String xpathQuery) 
            throws XPathExpressionException {
            
            javax.xml.xpath.XPath xpath = XPATH_FACTORY.newXPath();
            javax.xml.xpath.XPathExpression expression = xpath.compile(xpathQuery);
            NodeSet set = (NodeSet) expression.evaluate(doc,XPathConstants.NODESET);
            return (Element) set.elementAt(0);
        }
        
        public static Element selectSingleNode(Element element, String xpathQuery) 
            throws XPathExpressionException {
            
            javax.xml.xpath.XPath xpath = XPATH_FACTORY.newXPath();
            javax.xml.xpath.XPathExpression expression = xpath.compile(xpathQuery);
            NodeSet set = (NodeSet) expression.evaluate(element,XPathConstants.NODESET);
            return (Element) set.elementAt(0);
        }
        
        public static NodeSet selectNodes(Element element, String xpathQuery) 
            throws XPathExpressionException {
            
            javax.xml.xpath.XPath xpath = XPATH_FACTORY.newXPath();
            javax.xml.xpath.XPathExpression expression = xpath.compile(xpathQuery);
            return (NodeSet) expression.evaluate(element,XPathConstants.NODESET);
        }
        
        public static NodeSet selectNodes(Document doc, String xpathQuery) 
            throws XPathExpressionException {
            
            javax.xml.xpath.XPath xpath = XPATH_FACTORY.newXPath();
            javax.xml.xpath.XPathExpression expression = xpath.compile(xpathQuery);
            return (NodeSet) expression.evaluate(doc,XPathConstants.NODESET);
        }

      /**
       * Extracts the Node.TEXT value of a node. If one does not exist,
       * it returns an empty string.
       *
       * @param node The Node object containing the text.
       * @return String
       * @author York Davis
       **/
      public static String getTextValue(Node node)
      {
          Node textNode = node.getFirstChild();
          return textNode == null ? "" : textNode.getNodeValue().trim();
      }
}
