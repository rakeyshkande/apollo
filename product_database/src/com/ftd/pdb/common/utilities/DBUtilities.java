package com.ftd.pdb.common.utilities;

import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;
import java.sql.SQLException;

public class DBUtilities {
    private static final Logger LOGGER = new Logger("com.ftd.pdb.common.utilities.DBUtilities");
    public static void close(Connection conn) {
        try {
            if( conn!=null && !conn.isClosed() ) {
                conn.close();
            }
        } catch (SQLException sqle) {
            LOGGER.warn("Failed to closed database connection",sqle);
        }
    }
}
