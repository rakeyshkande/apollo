package com.ftd.pdb.common.resources;

import com.ftd.osp.utilities.ConfigurationUtil;

import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.exceptions.PDBSystemException;

import com.ftd.pdb.integration.dao.ILookupDAO;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;

import java.util.Properties;

import javax.mail.Session;

public class TestResourceProviderImpl implements IResourceProvider {
    private static boolean INITIALIZED=false;
    private static final String CONFIG_FILE = "pdb_test_config.xml";
    private static String DB_DRIVER_CLASS;
    private static String DB_USER_NAME;
    private static String DB_USER_CREDENTIALS;
    private static String DB_JDBC_URL;
    private ILookupDAO lookupDAO;
    
    public TestResourceProviderImpl() {
    }
    
    public Connection getDatabaseConnection() throws PDBSystemException {
        Connection conn = null;
         try {
            if( !INITIALIZED ) {
                init();    
            }
             conn = DriverManager.getConnection(DB_JDBC_URL, DB_USER_NAME, DB_USER_CREDENTIALS);
        }
        catch (Exception e) {
        	e.printStackTrace();
            throw new PDBSystemException(ProductMaintConstants.SQL_EXCEPTION,e);   
            
        }

        return conn;
    }
    
    private synchronized void init() throws Exception {
        if( !INITIALIZED ) {
           try{	
            ConfigurationUtil config = ConfigurationUtil.getInstance();
            DB_DRIVER_CLASS = config.getProperty(CONFIG_FILE,"db.driver.class");
            DB_USER_NAME = config.getProperty(CONFIG_FILE,"db.user.name");
            DB_USER_CREDENTIALS = config.getProperty(CONFIG_FILE,"db.user.credentials");
            DB_JDBC_URL = config.getProperty(CONFIG_FILE,"db.jdbc.url");
           }
           catch(Exception e) {
        	   DB_DRIVER_CLASS = "oracle.jdbc.driver.OracleDriver";
        	   DB_USER_NAME ="ftd_apps";
        	   DB_USER_CREDENTIALS = "*******";  // change this to the real password in local code but make sure not to check it in with password visible
        	   DB_JDBC_URL="jdbc:oracle:thin:@adonis.ftdi.com:1521:fozzie";  
           }
            DriverManager.registerDriver((Driver)(Class.forName(DB_DRIVER_CLASS).newInstance()));
            INITIALIZED=true;
        }
    }


    public Session getEmailSession() throws Exception {
        ConfigurationUtil config = ConfigurationUtil.getInstance();
        String mailHost;
        try {
            mailHost = config.getProperty(CONFIG_FILE,"email.server");    
        } catch (Exception e) {
            mailHost = "mail4.ftdi.com";
        }
        Properties properties = new Properties();
        properties.setProperty("mail.transport.protocol","smtp");
        properties.setProperty("mail.host",mailHost);
        Session session = Session.getInstance(properties);
        return session;
    }
    
    /** Return the value of the record for the frp.global_parms table
     * @param context filter
     * @param name filter
     * @return the value column returned from the database or null
     * @throws Exception for all errors and exceptions
     */
    public String getGlobalParameter( String context, String name) throws Exception {
        Connection conn = null;
        String retval;
        
        try {
            conn = getDatabaseConnection();
            retval = lookupDAO.getGlobalParameter(conn,context,name);
        } catch (Exception e) {
            retval = null;
        } finally {
            try {
                if( conn!=null ) {
                    conn.close();
                }
            } catch (Exception e2) {
                //Ignore it
            }
        }
        
        return retval;
    }

    /**
     * Returns the unecripted value from the test configuration file
     * @param context filter
     * @param name filter
     * @return value of record returned from database or null
     * @throws Exception
     */
    public String getSecureProperty(String context, String name) throws Exception {
        ConfigurationUtil config = ConfigurationUtil.getInstance();
        return config.getProperty(CONFIG_FILE,name);
    }

    public void setLookupDAO(ILookupDAO lookupDAO) {
        this.lookupDAO = lookupDAO;
    }

    public ILookupDAO getLookupDAO() {
        return lookupDAO;
    }
}
