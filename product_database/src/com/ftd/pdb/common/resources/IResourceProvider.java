package com.ftd.pdb.common.resources;

import com.ftd.pdb.common.exceptions.PDBSystemException;

import java.sql.Connection;

import javax.mail.Session;

public interface IResourceProvider {
    
    public Connection getDatabaseConnection() 
        throws PDBSystemException;
        
    public Session getEmailSession() 
        throws Exception;

    /**
     * Returns the value column from the frp.global_parms table
     * @param context filter
     * @param name filter
     * @return value of record returned from database or null
     * @throws Exception for all errors and exceptions
     */
    public String getGlobalParameter(String context, 
                                     String name) throws Exception;

    /**
     * Returns the unecripted value from frp.secure_config
     * @param context filter
     * @param name filter
     * @return value of record returned from database or null
     * @throws Exception
     */
    public String getSecureProperty(String context, String name) throws Exception;
}
