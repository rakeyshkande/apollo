package com.ftd.pdb.common.resources;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.j2ee.InitialContextUtil;
import com.ftd.pdb.common.ProductMaintConstants;

import com.ftd.pdb.common.exceptions.PDBSystemException;

import com.ftd.pdb.integration.dao.ILookupDAO;

import java.sql.Connection;

import javax.mail.Session;

import javax.naming.InitialContext;


public class J2EEResourceProvider implements IResourceProvider {
    private static final String SESSION_JNDI_NAME = "mail/pdb_session";
    private ILookupDAO lookupDAO;
        
    public J2EEResourceProvider() {
    }
    
  /**
   * Obtain connectivity with the database
   * @return Connection - db connection
   * @throws PDBSystemException
   */

    public Connection getDatabaseConnection()
            throws PDBSystemException
    {
        Connection conn = null;
        try {
            conn = DataSourceUtil.getInstance().getConnection(
                               ConfigurationUtil.getInstance().getFrpGlobalParmNoNull(ProductMaintConstants.PDB_CONFIG_CONTEXT,ProductMaintConstants.DATASOURCE_NAME));
        }
        catch (Exception e) {
            throw new PDBSystemException(ProductMaintConstants.SQL_EXCEPTION,e);
        }
        return conn;
    }

    public Session getEmailSession() throws Exception {
        InitialContext initialContext = InitialContextUtil.getInstance().getInitialContext("LOCALHOST");
        Session session = (Session) initialContext.lookup(SESSION_JNDI_NAME);
        
        return session;
    }

    /** Return the cached value of the record for the frp.global_parms table
     * @param context filter
     * @param name filter
     * @return the value column returned from the database or null
     * @throws Exception for all errors and exceptions
     */
    public String getGlobalParameter( String context, String name) throws Exception {
                                                       
        String retval = ConfigurationUtil.getInstance().getFrpGlobalParm(context,name);
        
        return retval;
    }

    /**
     * Returns the unecripted value from frp.secure_config
     * @param context filter
     * @param name filter
     * @return value of record returned from database or null
     * @throws Exception
     */
    public String getSecureProperty(String context, String name) throws Exception {
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        return configUtil.getSecureProperty(context, name);
    }

    public void setLookupDAO(ILookupDAO lookupDAO) {
        this.lookupDAO = lookupDAO;
    }

    public ILookupDAO getLookupDAO() {
        return lookupDAO;
    }
}
