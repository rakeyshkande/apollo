package com.ftd.pdb.common.valobjs;

public class DeliveryOptionVO extends PDBValueObject
{
    private String shippingMethodId;
    private String shippingMethodDesc;
    private boolean selected;
    private String defaultCarrier;
    private String novatorTag;

    public DeliveryOptionVO()
    {
        super("com.ftd.pdb.common.valobjs.DeliveryOptionVO");
    }

    public String getShippingMethodId()
    {
        return shippingMethodId;
    }

    public void setShippingMethodId(String newShippingMethodId)
    {
        shippingMethodId = newShippingMethodId;
    }

    public String getShippingMethodDesc()
    {
        return shippingMethodDesc;
    }

    public void setShippingMethodDesc(String newShippingMethodDesc)
    {
        shippingMethodDesc = newShippingMethodDesc;
    }

    public boolean isSelected()
    {
        return selected;
    }

    public void setSelected(boolean newSelected)
    {
        selected = newSelected;
    }

    public String getDefaultCarrier()
    {
        return defaultCarrier;
    }

    public void setDefaultCarrier(String newDefaultCarrier)
    {
        defaultCarrier = newDefaultCarrier;
    }

    public String getNovatorTag()
    {
        return novatorTag;
    }

    public void setNovatorTag(String newNovatorTag)
    {
        novatorTag = newNovatorTag;
    }
}