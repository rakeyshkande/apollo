package com.ftd.pdb.common.valobjs;

import com.ftd.osp.utilities.xml.JAXPException;
import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.utilities.FTDUtil;


import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


/**
  * This class should change to pdb
  */

public class ShippingKeyVO extends PDBValueObject 
{
    String shippingKey_ID;
    String shippingKeyDescription;
    String shipperID;
    String priceCategory;
    List prices;
    ProductVO products[];

    public ShippingKeyVO()
    {
        super("com.ftd.pdb.common.valobjs.ShippingKeyVO");
    }

    public String getShippingKey_ID()
    {
        return shippingKey_ID;
    }

    public void setShippingKey_ID(String newShippingKeyID)
    {
        shippingKey_ID = newShippingKeyID;
    }

    public String getShippingKeyDescription()
    {
        return shippingKeyDescription;
    }

    public void setShippingKeyDescription(String newShippingKeyDescription)
    {
        shippingKeyDescription = newShippingKeyDescription;
    }

    public String getShipperID()
    {
        return shipperID;
    }

    public void setShipperID(String newShipperID)
    {
        shipperID = newShipperID;
    }

    public String getPriceCategory()
    {
        return priceCategory;
    }

    public void setPriceCategory(String newPriceCategory)
    {
        priceCategory = newPriceCategory;
    }

    public List getPrices()
    {
        return prices;
    }

    public void setPrices(List newPrices)
    {
        prices = newPrices;
    }

    public ProductVO[] getProducts()
    {
        return products;
    }

    public void setProducts(ProductVO[] newProducts)
    {
        products = newProducts;
    }

    /**
    * Returns a representation ofthe object in XML
    */
    public String toXML() throws PDBApplicationException
    {
        String retval = null;
        
        try {
            Document doc = JAXPUtil.createDocument();
            Element root = doc.createElement("shipping_key");
            doc.appendChild(root);
            root.appendChild(JAXPUtil.buildSimpleXmlNode(doc,"shipper",(String)FTDUtil.convertNulls(this.shipperID)));
            root.appendChild(JAXPUtil.buildSimpleXmlNodeCDATA(doc,"description",(String)FTDUtil.convertNulls(this.shippingKeyDescription)));

            if(this.prices != null)
            {
                for(int i = 0; i < prices.size(); i++)
                {
                    ShippingKeyPriceVO vo = (ShippingKeyPriceVO)prices.get(i);
                    Element shippingCosts = doc.createElement("shipping_cost");
                    shippingCosts.appendChild(JAXPUtil.buildSimpleXmlNode(doc,"shipping_key",vo.getShippingKeyID()));
                    shippingCosts.appendChild(JAXPUtil.buildSimpleXmlNode(doc,"min_price",FTDUtil.removeFormattingCurrency(FTDUtil.formatFloat(new Float(vo.getMinPrice().floatValue()), 2, false, false))));
                    shippingCosts.appendChild(JAXPUtil.buildSimpleXmlNode(doc,"max_price",FTDUtil.removeFormattingCurrency(FTDUtil.formatFloat(new Float(vo.getMaxPrice().floatValue()), 2, false, false))));
                    
                    if(vo.getShippingMethods() != null)
                    {
                        for(int j = 0; j < vo.getShippingMethods().size(); j++)
                        {
                            ShippingMethodVO method = (ShippingMethodVO)vo.getShippingMethods().get(j);
                            if(method.getId().equals(ProductMaintConstants.SHIPPING_NEXT_DAY_CODE))
                            {
                                shippingCosts.appendChild(JAXPUtil.buildSimpleXmlNode(doc,"next_day_cost",FTDUtil.removeFormattingCurrency(FTDUtil.formatFloat(new Float(method.getCost()), 2, false, false))));
                            }
                            else if(method.getId().equals(ProductMaintConstants.SHIPPING_SATURDAY_CODE))
                            {
                                shippingCosts.appendChild(JAXPUtil.buildSimpleXmlNode(doc,"saturday_cost",FTDUtil.removeFormattingCurrency(FTDUtil.formatFloat(new Float(method.getCost()), 2, false, false))));
                            }
                            else if(method.getId().equals(ProductMaintConstants.SHIPPING_STANDARD_CODE))
                            {
                                shippingCosts.appendChild(JAXPUtil.buildSimpleXmlNode(doc,"standard_cost",FTDUtil.removeFormattingCurrency(FTDUtil.formatFloat(new Float(method.getCost()), 2, false, false))));
                            }
                            else if(method.getId().equals(ProductMaintConstants.SHIPPING_TWO_DAY_CODE))
                            {
                                shippingCosts.appendChild(JAXPUtil.buildSimpleXmlNode(doc,"two_day_cost",FTDUtil.removeFormattingCurrency(FTDUtil.formatFloat(new Float(method.getCost()), 2, false, false))));
                            }
                            else if(method.getId().equals(ProductMaintConstants.SHIPPING_SAME_DAY_CODE))
                            {
                                shippingCosts.appendChild(JAXPUtil.buildSimpleXmlNode(doc,"same_day_cost",FTDUtil.removeFormattingCurrency(FTDUtil.formatFloat(new Float(method.getCost()), 2, false, false))));
                            }
                        }
                    }
                }
            }
            
            retval = JAXPUtil.toStringNoFormat(doc);
            
        } catch (Exception e) {
            logger.error(e);
            throw new PDBApplicationException(ProductMaintConstants.OE_CONVERT_VO_TO_XML_EXCEPTION,e);
        }

        return retval;
    }
}
