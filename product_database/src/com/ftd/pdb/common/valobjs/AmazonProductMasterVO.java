package com.ftd.pdb.common.valobjs;


public class AmazonProductMasterVO extends PDBValueObject
{

    public AmazonProductMasterVO() {
        super("com.ftd.pdb.common.valobjs.AmazonProductMasterVO");
    }

    String productId;
	String catalogStatusStandard;
    String productNameStandard;
    String productDescriptionStandard;
    String catalogStatusDeluxe;
    String productNameDeluxe;
    String productDescriptionDeluxe;
    String catalogStatusPremium;
    String productNamePremium;
    String productDescriptionPremium;
    String itemType;
    
    public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getCatalogStatusStandard() {
		return catalogStatusStandard;
	}

	public void setCatalogStatusStandard(String catalogStatusStandard) {
		this.catalogStatusStandard = catalogStatusStandard;
	}

	public String getProductNameStandard() {
		return productNameStandard;
	}

	public void setProductNameStandard(String productNameStandard) {
		this.productNameStandard = productNameStandard;
	}

	public String getProductDescriptionStandard() {
		return productDescriptionStandard;
	}

	public void setProductDescriptionStandard(String productDescriptionStandard) {
		this.productDescriptionStandard = productDescriptionStandard;
	}

	public String getCatalogStatusDeluxe() {
		return catalogStatusDeluxe;
	}

	public void setCatalogStatusDeluxe(String catalogStatusDeluxe) {
		this.catalogStatusDeluxe = catalogStatusDeluxe;
	}

	public String getProductNameDeluxe() {
		return productNameDeluxe;
	}

	public void setProductNameDeluxe(String productNameDeluxe) {
		this.productNameDeluxe = productNameDeluxe;
	}

	public String getProductDescriptionDeluxe() {
		return productDescriptionDeluxe;
	}

	public void setProductDescriptionDeluxe(String productDescriptionDeluxe) {
		this.productDescriptionDeluxe = productDescriptionDeluxe;
	}

	public String getCatalogStatusPremium() {
		return catalogStatusPremium;
	}

	public void setCatalogStatusPremium(String catalogStatusPremium) {
		this.catalogStatusPremium = catalogStatusPremium;
	}

	public String getProductNamePremium() {
		return productNamePremium;
	}

	public void setProductNamePremium(String productNamePremium) {
		this.productNamePremium = productNamePremium;
	}

	public String getProductDescriptionPremium() {
		return productDescriptionPremium;
	}

	public void setProductDescriptionPremium(String productDescriptionPremium) {
		this.productDescriptionPremium = productDescriptionPremium;
	}

	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

}
