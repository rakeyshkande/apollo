package com.ftd.pdb.common.valobjs;

import org.w3c.dom.Document;



public class ProductBatchVO extends PDBValueObject
{
    private String productId;
    private String novatorId;
    private String userId;
    private String status;
    private Document xmlDocument;
    private Document xmlErrors;
    
    public ProductBatchVO() {
        super("com.ftd.pdb.common.valobjs.ProductBatchVO");
    }
    
    public void setProductId(String newProductId) 
    {
        productId = newProductId;
    }
    
    public String getProductId() 
    {
        return productId;
    }
    
    public void setNovatorId(String newNovatorId) 
    {
        novatorId = newNovatorId;
    }
    
    public String getNovatorId() 
    {
        return novatorId;
    }
    
    public void setUserId(String newUserId) 
    {
        userId = newUserId;
    }
    
    public String getUserId() 
    {
        return userId;
    }
    
    public void setStatus(String newStatus) 
    {
        status = newStatus;
    }
    
    public String getStatus() 
    {
        return status;
    }
    
    public void setXmlDocument(Document newXmlDocument) 
    {
        xmlDocument = newXmlDocument;
    }
    
    public Document getXmlDocument() 
    {
        return xmlDocument;
    }
    
    public void setXmlErrors(Document newXmlErrorsDocument) 
    {
        xmlErrors = newXmlErrorsDocument;
    }
    
    public Document getXmlErrors() 
    {
        return xmlErrors;
    }
}