package com.ftd.pdb.common.valobjs;

public class ValueVO extends PDBValueObject
{
	private String id;
	private String description;
        
    public ValueVO() {
        super("com.ftd.pdb.common.valobjs.ValueVO");
    }

    public String getDescription() 
    { 
        return description; 
    }
    
    public String getId()
    {
        return id;
    }
    
    public void setDescription(String description) 
    {
        this.description = description; 
    }
    
    public void setId(String id)
    {
        this.id = id;
    }

    public boolean equals(Object obj)
    {
        boolean ret = false;
        if((id.equals(((ValueVO)obj).getId())) && (description.equals(((ValueVO)obj).getDescription())))
        {
            ret = true;
        }

        return ret;
    }
}