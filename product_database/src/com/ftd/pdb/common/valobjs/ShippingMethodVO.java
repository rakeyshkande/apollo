package com.ftd.pdb.common.valobjs;

public class ShippingMethodVO extends PDBValueObject 
{
    private String id;
    private float cost;
    private String priceId;
    
    public ShippingMethodVO() {
        super("com.ftd.pdb.common.valobjs.ShippingMethodVO");
    }

    public String getId()
    {
        return id;
    }

    public void setId(String newId)
    {
        id = newId;
    }



    public float getCost()
    {
        return cost;
    }

    public void setCost(float newCost)
    {
        cost = newCost;
    }



    public String getPriceId()
    {
        return priceId;
    }

    public void setPriceId(String newPriceId)
    {
        priceId = newPriceId;
    }

    public boolean equals(Object obj)
    {
        ShippingMethodVO vo = (ShippingMethodVO)obj;
        boolean ret = false;
        
        if(vo.getId().equals(this.getId()) && vo.getPriceId().equals(this.getPriceId()))
        {
            ret = true;
        }

        return ret;
    }
}