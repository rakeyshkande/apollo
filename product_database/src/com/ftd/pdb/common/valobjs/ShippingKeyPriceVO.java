package com.ftd.pdb.common.valobjs;

import java.util.List;

public class ShippingKeyPriceVO extends PDBValueObject 
{
    String shippingKeyID;
    String shippingKeyPriceID;
    Double minPrice;
    Double maxPrice;
    private List shippingMethods;

    public ShippingKeyPriceVO()
    {
        super("com.ftd.pdb.common.valobjs.ShippingKeyPriceVO");
    }

    public String getShippingKeyID()
    {
        return shippingKeyID;
    }

    public void setShippingKeyID(String newShippingKeyID)
    {
        shippingKeyID = newShippingKeyID;
    }

    public String getShippingKeyPriceID()
    {
        return shippingKeyPriceID;
    }

    public void setShippingKeyPriceID(String newShippingKeyPriceID)
    {
        shippingKeyPriceID = newShippingKeyPriceID;
    }

    public Double getMinPrice()
    {
        return minPrice;
    }

    public void setMinPrice(Double newMinPrice)
    {
        minPrice = newMinPrice;
    }

    public Double getMaxPrice()
    {
        return maxPrice;
    }

    public void setMaxPrice(Double newMaxPrice)
    {
        maxPrice = newMaxPrice;
    }

    public List getShippingMethods()
    {
        return shippingMethods;
    }

    public void setShippingMethods(List newShippingMethods)
    {
        shippingMethods = newShippingMethods;
    }









}