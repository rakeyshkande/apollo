package com.ftd.pdb.common.valobjs.comparator;

import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.valobjs.ShippingMethodVO;
/**
 * Compares the orderby to return the values in sorted order
 */
public class ShippingMethodVOComparator implements java.util.Comparator {
    /**
     *  The compare method compares the ColorVO objects based on orderby
     *
     *  @param o1   The first ShippingMethodVO
     *  @param o2   The second ShippingMethodVO
     *  @return int negative if the first ShippingMethodVO orderby comes before the second's
     *              positive if the first ShippingMethodVO orderby comes after the second's
     *              zero if the ShippingMethodVO have the same type
     */
public int compare(Object o1, Object o2) {
	int ret = 0;
	try {
		ShippingMethodVO first = (ShippingMethodVO) o1;
		ShippingMethodVO second = (ShippingMethodVO) o2;

		int firstOrder = 0;
		int secondOrder = 0;

		if(first.getId().equals(ProductMaintConstants.SHIPPING_STANDARD_CODE))
		{
			firstOrder = 1;
		}
		else if(first.getId().equals(ProductMaintConstants.SHIPPING_TWO_DAY_CODE))
		{
			firstOrder = 2;
		}
		else if(first.getId().equals(ProductMaintConstants.SHIPPING_NEXT_DAY_CODE))
		{
			firstOrder = 3;
		}
		else if(first.getId().equals(ProductMaintConstants.SHIPPING_SATURDAY_CODE))
		{
			firstOrder = 4;
		}
		else
		{
			firstOrder = 5;
		}

		if(second.getId().equals(ProductMaintConstants.SHIPPING_STANDARD_CODE))
		{
			secondOrder = 1;
		}
		else if(second.getId().equals(ProductMaintConstants.SHIPPING_TWO_DAY_CODE))
		{
			secondOrder = 2;
		}
		else if(second.getId().equals(ProductMaintConstants.SHIPPING_NEXT_DAY_CODE))
		{
			secondOrder = 3;
		}
		else if(second.getId().equals(ProductMaintConstants.SHIPPING_SATURDAY_CODE))
		{
			secondOrder = 4;
		}
		else
		{
			secondOrder = 5;
		}

		ret = new Integer(firstOrder).compareTo(new Integer(secondOrder));


	} catch(Exception e) {}

	return ret;
}
/**
 * Insert the method's description here.
 * @return boolean
 * @param obj java.lang.Object
 */
public boolean equals(Object obj) {
	return obj instanceof ColorVOComparator;
}
}
