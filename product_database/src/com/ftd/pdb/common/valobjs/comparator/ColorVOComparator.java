package com.ftd.pdb.common.valobjs.comparator;

import com.ftd.pdb.common.valobjs.ColorVO;
/**
 * Compares the orderby to return the values in sorted order
 */
public class ColorVOComparator implements java.util.Comparator {
    /**
     *  The compare method compares the ColorVO objects based on orderby
     *
     *  @param o1   The first ColorVO
     *  @param o2   The second ColorVO
     *  @return int negative if the first ColorVO orderby comes before the second's
     *              positive if the first ColorVO orderby comes after the second's
     *              zero if the ColorVO have the same type
     */
public int compare(Object o1, Object o2) {
	int ret = 0;
	try {
		ColorVO first = (ColorVO) o1;
		ColorVO second = (ColorVO) o2;

		ret = new Integer(first.getOrderBy()).compareTo(new Integer(second.getOrderBy()));

	} catch(Exception e) {}

	return ret;
}
/**
 * Insert the method's description here.
 * @return boolean
 * @param obj java.lang.Object
 */
public boolean equals(Object obj) {
	return obj instanceof ColorVOComparator;
}
}
