package com.ftd.pdb.common.valobjs;


/**
 * Occasion VO contains occasion information.
 *
 * @author 	Ed Mueller
 */
public class OccasionVO extends PDBValueObject
{

    private int id;
    private String description;
    
    public OccasionVO() {
        super("com.ftd.pdb.common.valobjs.OccasionVO");
    }

/** 
 * Get occasion description
 *
 * @return String - occasion description
 */
    public String getDescription()
    {
        return description;
    }

/** 
 * Set occasion description
 *
 * @param newDescription Occasion description
 */
    public void setDescription(String newDescription)
    {
        description = newDescription;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int newId)
    {
        id = newId;
    }
}