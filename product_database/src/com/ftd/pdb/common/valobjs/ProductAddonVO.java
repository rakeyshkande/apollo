package com.ftd.pdb.common.valobjs;

public class ProductAddonVO extends PDBValueObject
{
    String  addonId;
    String  addonTypeId;
    String  addonDescription;
    String  addonPrice;
    Boolean addonAvailableFlag;
    Boolean addonTypeIncludedInProductFeedFlag;
    Boolean activeFlag;
    String maxQuantity;
    String displaySequenceNumber;
    Boolean modified;

    public ProductAddonVO()
    {
        super("com.ftd.pdb.common.valobjs.ProductAddonVO");
    }
    
    public String getAddonId()
    {
        return addonId;
    }

    public void setAddonId(String addonId)
    {
        this.addonId = addonId;
    }

    public String getAddonTypeId()
    {
        return addonTypeId;
    }

    public void setAddonTypeId(String addonTypeId)
    {
        this.addonTypeId = addonTypeId;
    }

    public String getAddonDescription()
    {
        return addonDescription;
    }

    public void setAddonDescription(String addonDescription)
    {
        this.addonDescription = addonDescription;
    }

    public String getAddonPrice()
    {
        return addonPrice;
    }

    public void setAddonPrice(String addonPrice)
    {
        this.addonPrice = addonPrice;
    }

    public Boolean getAddonAvailableFlag()
    {
        return addonAvailableFlag;
    }

    public void setAddonAvailableFlag(Boolean addonAvailableFlag)
    {
        this.addonAvailableFlag = addonAvailableFlag;
    }

    public Boolean getAddonTypeIncludedInProductFeedFlag()
    {
        return addonTypeIncludedInProductFeedFlag;
    }

    public void setAddonTypeIncludedInProductFeedFlag(Boolean addonTypeIncludedInProductFeedFlag)
    {
        this.addonTypeIncludedInProductFeedFlag = addonTypeIncludedInProductFeedFlag;
    }

    public Boolean getActiveFlag()
    {
        return activeFlag;
    }

    public void setActiveFlag(Boolean activeFlag)
    {
        this.activeFlag = activeFlag;
    }

    public String getMaxQuantity()
    {
        return maxQuantity;
    }

    public void setMaxQuantity(String maxQuantity)
    {
        this.maxQuantity = maxQuantity;
    }

    public String getDisplaySequenceNumber()
    {
        return displaySequenceNumber;
    }

    public void setDisplaySequenceNumber(String displaySequenceNumber)
    {
        this.displaySequenceNumber = displaySequenceNumber;
    }

    public Boolean isModified(Boolean activeFlag, String maxQuantity, String displaySequenceNumber) throws Exception
    {
        if ((this.activeFlag == null ^ activeFlag == null) || (this.activeFlag != null && activeFlag !=null && this.activeFlag.compareTo(activeFlag) != 0) ||
            (this.maxQuantity == null ^ maxQuantity == null) || (this.maxQuantity != null && maxQuantity != null && !this.maxQuantity.equals(maxQuantity)) ||
            (this.displaySequenceNumber == null ^ displaySequenceNumber == null) || (this.displaySequenceNumber != null && displaySequenceNumber != null && !this.displaySequenceNumber.equals(displaySequenceNumber))
            )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public Boolean isModified()
    {
        return this.modified;
    }

    public void setModified(Boolean modified)
    {
        this.modified = modified;
    }
}
