package com.ftd.pdb.common.valobjs;

public class SearchKeyVO   extends PDBValueObject
{
    private String searchKey;
    
    public SearchKeyVO() {
        super("com.ftd.pdb.common.valobjs.SearchKeyVO");
    }

    public String getSearchKey()
    {
        return searchKey;
    }

    public void setSearchKey(String newSearchKey)
    {
        searchKey = newSearchKey;
    }
}