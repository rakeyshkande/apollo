package com.ftd.pdb.common.valobjs;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.JAXPException;
import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.utilities.NovatorTransmission;

import java.lang.reflect.Method;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;


public class PDBValueObject  
{
    protected Logger logger;
    private String createdBy = null;
    private String updatedBy = null;

    public PDBValueObject(String className)
    {
        logger = new Logger(className);
    }
    
    /**
    * Returns a representation ofthe object in XML
    */
    public String toXML() throws PDBApplicationException
    {
        logger.debug("PBDValueObject.toXML()");
        //Buffer to hold complete XML string
        StringBuffer xml = new StringBuffer();
        String className = null;
        
        //Get methods of passed in class object
        Class c = this.getClass(); 
        Method[] theMethods = c.getMethods();
      
        try
        {
            //Start XML string
            className = NovatorTransmission.getNovatorFieldName(c.getName());
            if (className != null){
                xml.append("<"+ className+ ">");
                }

            //Go through each method in class, looking for getters
            for (int i = 0; i < theMethods.length; i++) 
            {
                String methodString = theMethods[i].getName();

                //Check if this is a 'get' method
                if ((methodString.startsWith("get") || methodString.startsWith("is")) && !methodString.equalsIgnoreCase("getClass"))
                {      
                    //get field name
                    String field = null;
                    if(methodString.startsWith("get"))
                    {
                        field = methodString.substring(3,methodString.length());
                    }
                    else
                    {
                        field = methodString.substring(2,methodString.length());
                    }
                    // change the first character to lower case
                    StringBuffer sb = new StringBuffer();
                    String fisrtChar = field.substring(0, 1);
                    fisrtChar = fisrtChar.toLowerCase();
                    sb.append(fisrtChar).append(field.substring(1));
                    field = NovatorTransmission.getNovatorFieldName(sb.toString());
                    

                    if(field == null)
                    {
                        continue;
                    }

                    //Get property value
                    String propertyValue = "";
                    Object args[] = null; 

                    //Call getter method
                    Object value = theMethods[i].invoke(this,args );

                    //value must not be null
                    if (value == null)
                    {
                        value = "";
                    }
              
                    //Do processing for special return types which need conversion
                    if (value instanceof java.util.Date)
                    {
                        SimpleDateFormat sdf = new SimpleDateFormat(ProductMaintConstants.NOVATOR_DATEFORMAT);
                        propertyValue = sdf.format((Date)value);
                    }
                    else if (value instanceof Boolean)
                    {
                        if (((Boolean)value).booleanValue())
                        {
                            propertyValue = ProductMaintConstants.NOVATOR_BOOLEAN_TRUE;
                        }
                        else
                        {
                            propertyValue = ProductMaintConstants.NOVATOR_BOOLEAN_FALSE;
                        }                    
                    }
                    else if (value instanceof PDBValueObject)
                    {
                        propertyValue = ((PDBValueObject)value).toXML();                    
                    } 
                    else if (value instanceof PDBValueObject[] )
                    {
                        PDBValueObject[] values = (PDBValueObject[])value;
                        for (int j=0; j<values.length; j++) 
                        {
                            propertyValue += values[j].toXML();
                        }
                    }
                    else if (value instanceof List )
                    {
                        List values = (List)value;
                        for (int j=0; j<values.size(); j++) 
                        {
                            PDBValueObject vo = (PDBValueObject)values.get(j);
                            if( vo!=null && vo instanceof PDBValueObject )  //Sanity check
                                propertyValue += vo.toXML();
                        }
                    }                    
                    else
                    {
                        //Use toString method for all other types
                        propertyValue = value.toString();
                    }

                    //ed 3/26/03 -- added code see that nulls are not included
                    //Create xml element for all fields, except value objects & nulls
                    if (!(value instanceof PDBValueObject) & !(value instanceof PDBValueObject[])  )
                    {
                        //some field levels are skipped
                        if (field.equals(ProductMaintConstants.SKIP)){
                                xml.append(propertyValue);
                                }
                            else {
                                xml.append("<" + field + ">" + propertyValue + "</" + field + ">");
                            }
                    }
                    else
                    {
                        xml.append(propertyValue);
                    }
                }
            }
        }
        catch (Exception e) 
        {
            String[] message = {e.toString()};
            throw new PDBApplicationException(ProductMaintConstants.OE_CONVERT_VO_TO_XML_EXCEPTION, message);
        } 
 
        //complete xml string
            if (className != null){
                xml.append("</"+ className+ ">");
                }
                
        return xml.toString() ;        
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }
}
