package com.ftd.pdb.common.valobjs;

public class ProductSubTypeVO extends PDBValueObject 
{
    private String id;
    private String description;
    private String typeId;

    public ProductSubTypeVO()
    {
        super("com.ftd.pdb.common.valobjs.ProductSubTypeVO");
    }

    public String getId()
    {
        return id;
    }

    public void setId(String newId)
    {
        id = newId;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String newDescription)
    {
        description = newDescription;
    }

    public String getTypeId()
    {
        return typeId;
    }

    public void setTypeId(String newTypeId)
    {
        typeId = newTypeId;
    }
}