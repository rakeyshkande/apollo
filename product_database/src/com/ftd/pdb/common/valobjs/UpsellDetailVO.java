package com.ftd.pdb.common.valobjs;

import com.ftd.osp.utilities.xml.JAXPException;
import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.pdb.common.PDBXMLTags;
import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.exceptions.PDBApplicationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


public class UpsellDetailVO  extends PDBValueObject
{
    private String DetailID;
    private int Sequence;
    private String Name;
    private String Type;
    private float Price;
    private boolean Available;
    private String NovatorID;
    private String DetailMasterID;
    private boolean SentToNovatorProd;
    private boolean SentToNovatorUAT;
    private boolean SentToNovatorContent;
    private boolean SentToNovatorTest;
    private String DetailMasterName;
    private boolean gbbNameOverrideFlag;
    private String gbbNameOverrideText;
    private boolean gbbPriceOverrideFlag;
    private String gbbPriceOverrideText;
    private int gbbSequence;
    private String defaultSkuFlag;
    
    public UpsellDetailVO() {
        super("com.ftd.pdb.common.valobjs.UpsellDetailVO");
    }

    public String getDetailID()
    {
        return DetailID;
    }

    public void setDetailID(String newDetailID)
    {
        DetailID = newDetailID;
    }

    public int getSequence()
    {
        return Sequence;
    }

    public void setSequence(int newSequence)
    {
        Sequence = newSequence;
    }

    public String getName()
    {
        return Name;
    }

    public void setName(String newName)
    {
        Name = newName;
    }

    public String getType()
    {
        return Type;
    }

    public void setType(String newType)
    {
        Type = newType;
    }

    public float getPrice()
    {
        return Price;
    }

    public void setPrice(float newPrice)
    {
        Price = newPrice;
    }

    public boolean isAvailable()
    {
        return Available;
    }

    public void setAvailable(boolean newAvailable)
    {
        Available = newAvailable;
    }

    public String getNovatorID()
    {
        return NovatorID;
    }

    public void setNovatorID(String newNovatorID)
    {
        NovatorID = newNovatorID;
    }

    public String getDetailMasterID()
    {
        return DetailMasterID;
    }

    public void setDetailMasterID(String newDetailMasterID)
    {
        DetailMasterID = newDetailMasterID;
    }

    public boolean isSentToNovatorProd()
    {
        return SentToNovatorProd;
    }

    public void setSentToNovatorProd(boolean newSentToNovatorProd)
    {
        SentToNovatorProd = newSentToNovatorProd;
    }

    public boolean isSentToNovatorUAT()
    {
        return SentToNovatorUAT;
    }

    public void setSentToNovatorUAT(boolean newSentToNovatorUAT)
    {
        SentToNovatorUAT = newSentToNovatorUAT;
    }

    public boolean isSentToNovatorContent()
    {
        return SentToNovatorContent;
    }

    public void setSentToNovatorContent(boolean newSentToNovatorContent)
    {
        SentToNovatorContent = newSentToNovatorContent;
    }

    public boolean isSentToNovatorTest()
    {
        return SentToNovatorTest;
    }

    public void setSentToNovatorTest(boolean newSentToNovatorTest)
    {
        SentToNovatorTest = newSentToNovatorTest;
    }

    public String getDetailMasterName()
    {
        return DetailMasterName;
    }

    public void setDetailMasterName(String newDetailMasterName)
    {
        DetailMasterName = newDetailMasterName;
    }
    

  	/**
    * Returns a string representation of the object in XML
  	*/  
    public String toXML() throws PDBApplicationException
    {	
        String retval = null;
        
        try {
            Document doc = toXMLDocument();
            Element root = (Element)doc.getFirstChild();
            JAXPUtil.toString(root);
        } catch (JAXPException xpe) {
            String[] message = {xpe.toString()};
            throw new PDBApplicationException(ProductMaintConstants.OE_CONVERT_VO_TO_XML_EXCEPTION, message);
        }
        
        return retval;
    }

    /**
    * Returns a representation of the object in XML for transmission to
    * Novator
    */
    public Document toXMLDocument() throws PDBApplicationException
    {
        Document doc = null;
        try 
        {
            doc = JAXPUtil.createDocument();
            Element root = doc.createElement(PDBXMLTags.TAG_UPSELL_DETAIL);
            root.appendChild(JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_UPSELL_DETAIL_SEQUENCE, Sequence));
            root.appendChild(JAXPUtil.buildSimpleXmlNodeCDATA(doc,PDBXMLTags.TAG_UPSELL_DETAIL_NAME, Name));
            root.appendChild(JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_UPSELL_TYPE, Type));
            root.appendChild(JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_UPSELL_PRICE, Price));
            root.appendChild(JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_UPSELL_STATUS, Available));
            root.appendChild(JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_UPSELL_DETAIL_ID, NovatorID));
            doc.appendChild(root);
        }
        catch (Exception e) 
        {
            String[] message = {e.toString()};
            throw new PDBApplicationException(ProductMaintConstants.OE_CONVERT_VO_TO_XML_EXCEPTION, message);
        }
        
        return doc;        
    }

    public void setGbbNameOverrideFlag(boolean gbbNameOverrideFlag) {
        this.gbbNameOverrideFlag = gbbNameOverrideFlag;
    }

    public boolean getGbbNameOverrideFlag() {
        return gbbNameOverrideFlag;
    }

    public void setGbbNameOverrideText(String gbbNameOverrideText) {
        this.gbbNameOverrideText = gbbNameOverrideText;
    }

    public String getGbbNameOverrideText() {
        return gbbNameOverrideText;
    }

    public void setGbbPriceOverrideFlag(boolean gbbPriceOverrideFlag) {
        this.gbbPriceOverrideFlag = gbbPriceOverrideFlag;
    }

    public boolean getGbbPriceOverrideFlag() {
        return gbbPriceOverrideFlag;
    }

    public void setGbbPriceOverrideText(String gbbPriceOverrideText) {
        this.gbbPriceOverrideText = gbbPriceOverrideText;
    }

    public String getGbbPriceOverrideText() {
        return gbbPriceOverrideText;
    }

    public void setGbbSequence(int gbbSequence) {
        this.gbbSequence = gbbSequence;
    }

    public int getGbbSequence() {
        return gbbSequence;
    }

    public void setDefaultSkuFlag(String defaultSkuFlag) {
        this.defaultSkuFlag = defaultSkuFlag;
    }

    public String getDefaultSkuFlag() {
        return defaultSkuFlag;
    }
}
