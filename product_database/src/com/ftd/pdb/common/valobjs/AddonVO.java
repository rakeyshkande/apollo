package com.ftd.pdb.common.valobjs;


/**
 * Card VO contains card information.
 *
 * @author 	Ed Mueller
 */
public class AddonVO extends PDBValueObject 
{

    private String id;
    private String type;
    private String description;
    private float price;
    private boolean value;
    
    public AddonVO() {
        super("com.ftd.pdb.common.valobjs.AddonVO");
    }

/** 
 * Get card description
 *
 * @return card description
 */
    public String getDescription()
    {
        return description;
    }

/** 
 * Set card description
 *
 * @param newDescription card description
 */
    public void setDescription(String newDescription)
    {
        description = newDescription;
    }


/** 
 * Get type
 *
 * @return type of addon
 */
    public String getType()
    {
        return type;
    }

/** 
 * Set type
 *
 * @param newType type of addon
 */
    public void setType(String newType)
    {
        type = newType;
    }


/** 
 * Get price
 *
 * @return type of price
 */
    public float getPrice()
    {
        return price;
    }

/** 
 * Set price
 *
 * @param newPrice
 */
    public void setPrice(float newPrice)
    {
        price = newPrice;
    }

    public boolean isValue()
    {
        return value;
    }

    public void setValue(boolean newValue)
    {
        value = newValue;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String newId)
    {
        id = newId;
    }

    
}