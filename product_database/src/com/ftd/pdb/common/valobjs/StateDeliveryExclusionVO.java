/*
 * Created on Dec 20, 2003
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.ftd.pdb.common.valobjs;

import com.ftd.osp.utilities.xml.JAXPException;
import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.pdb.common.PDBXMLTags;
import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.XMLParsingException;
import com.ftd.pdb.common.utilities.FTDUtil;
import com.ftd.pdb.common.utilities.XMLUtilities;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


/**
 * @author tpeterson
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class StateDeliveryExclusionVO extends PDBValueObject {
	protected String excludedState;
	protected String stateName;
	protected boolean sunExcluded;
	protected boolean monExcluded;
	protected boolean tueExcluded;
	protected boolean wedExcluded;
	protected boolean thuExcluded;
	protected boolean friExcluded;
	protected boolean satExcluded;
        
        public StateDeliveryExclusionVO() {
            super("com.ftd.pdb.common.valobjs.StateDeliveryExclusionVO");
        }
	/**
	 * @return Abbreviation for the excluded state
	 */
	public String getExcludedState() {
		return excludedState;
	}

	/**
	 * @return is Friday excluded from delivery
	 */
	public boolean isFriExcluded() {
		return friExcluded;
	}

	/**
	 * @return is Monday excluded from delivery
	 */
	public boolean isMonExcluded() {
		return monExcluded;
	}

	/**
	 * @return is Saturday excluded from delivery
	 */
	public boolean isSatExcluded() {
		return satExcluded;
	}

	/**
	 * @return is Sunday excluded from delivery
	 */
	public boolean isSunExcluded() {
		return sunExcluded;
	}

	/**
	 * @return is Thursday excluded from delivery
	 */
	public boolean isThuExcluded() {
		return thuExcluded;
	}

	/**
	 * @return is Tuesday excluded from delivery
	 */
	public boolean isTueExcluded() {
		return tueExcluded;
	}

	/**
	 * @return is Wednesday excluded from delivery
	 */
	public boolean isWedExcluded() {
		return wedExcluded;
	}

	/**
	 * @param string set the state to be excluded
	 */
	public void setExcludedState(String string) {
		excludedState = string;
	}

	/**
	 * @param b set is Friday to be excluded from delivery
	 */
	public void setFriExcluded(boolean b) {
		friExcluded = b;
	}

	/**
	 * @param b set is Monday to be excluded from delivery
	 */
	public void setMonExcluded(boolean b) {
		monExcluded = b;
	}

	/**
	 * @param b set is Saturday to be excluded from delivery
	 */
	public void setSatExcluded(boolean b) {
		satExcluded = b;
	}

	/**
	 * @param b set is Sunday to be excluded from delivery
	 */
	public void setSunExcluded(boolean b) {
		sunExcluded = b;
	}

	/**
	 * @param b set is Thursday to be excluded from delivery
	 */
	public void setThuExcluded(boolean b) {
		thuExcluded = b;
	}

	/**
	 * @param b set is Tuesday to be excluded from delivery
	 */
	public void setTueExcluded(boolean b) {
		tueExcluded = b;
	}

	/**
	 * @param b set is Wednesday to be excluded from delivery
	 */
	public void setWedExcluded(boolean b) {
		wedExcluded = b;
	}
	
	/**
	 * @return The state name for the excluded state
	 */
	public String getStateName() {
		return stateName;
	}

	/**
	 * @param string Sets the state name for the excluded state
	 */
	public void setStateName(String string) {
		stateName = string;
	}

	/**
	* Returns a string representation of the object in XML
	*/
	public String toXML() throws PDBApplicationException
	{	
            String retval = null;
            
            try {
                Document doc = toXMLDocument();
                Element root = (Element)doc.getFirstChild();
                JAXPUtil.toString(root);
            } catch (JAXPException xpe) {
                String[] message = {xpe.toString()};
                throw new PDBApplicationException(ProductMaintConstants.OE_CONVERT_VO_TO_XML_EXCEPTION, message);
            }
            
            return retval;
	}


    public Document toXMLDocument() throws PDBApplicationException
    {
        Document doc = null;
        try 
        {
            doc = JAXPUtil.createDocument();
            return this.toXMLDocument(doc);
        }
        catch (Exception e) 
        {
            String[] message = {e.toString()};
            throw new PDBApplicationException(ProductMaintConstants.OE_CONVERT_VO_TO_XML_EXCEPTION, message);
        }
    }
	/**
	* Returns an xml element representation of the object
	*/
	public Document toXMLDocument(Document doc) throws PDBApplicationException
	{
//		<detail_entity>
//			<key>excludeStates</key>
//			<value>AZ</value>					//State abbreviation
//			<days>
//				<day>sat</day>					//If a day of week is listed then it is excluded
//				<day>sun</day>					//otherwise it is a valid delivery date for that state
//			</days>								//Valid values are sun mon tue wed thu fri sat 
//		</detail_entity>

            //Document doc = null;
            try 
            {
                Element root = doc.createElement(PDBXMLTags.TAG_DETAIL_ENTITY);    
		
		//Excluded State Key
		Element child1 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_KEY,PDBXMLTags.TAG_EXCLUDED_STATES);
		root.appendChild(child1);
		
		//Excluded State Value
		child1 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_VALUE,FTDUtil.convertNulls(getExcludedState()).toString());
		root.appendChild(child1);
		
		//Excluded State Name
		child1 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_TITLE,FTDUtil.convertNulls(getStateName()).toString());
		root.appendChild(child1);
		
		//Days
		Element day;
		child1 = doc.createElement(PDBXMLTags.TAG_DAYS);
		//Sunday
		if( this.isSunExcluded() ) {
			day = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_DAY,PDBXMLTags.TAG_DAY_SUN); 
			child1.appendChild(day);
		}
		//Monday
		if( this.isMonExcluded() ) {
			day = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_DAY,PDBXMLTags.TAG_DAY_MON); 
			child1.appendChild(day);
		}
		//Tuesday
		if( this.isTueExcluded() ) {
			day = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_DAY,PDBXMLTags.TAG_DAY_TUE); 
			child1.appendChild(day);
		}
		//Wednesday
		if( this.isWedExcluded() ) {
			day = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_DAY,PDBXMLTags.TAG_DAY_WED); 
			child1.appendChild(day);
		}
		//Thursday
		if( this.isThuExcluded() ) {
			day = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_DAY,PDBXMLTags.TAG_DAY_THU); 
			child1.appendChild(day);
		}
		//Friday
		if( this.isFriExcluded() ) {
			day = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_DAY,PDBXMLTags.TAG_DAY_FRI); 
			child1.appendChild(day);
		}
		//Saturday
		if( this.isSatExcluded() ) {
			day = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_DAY,PDBXMLTags.TAG_DAY_SAT); 
			child1.appendChild(day);
		}
		root.appendChild(child1);
                doc.appendChild(root);
        }
        catch (Exception e) 
        {
            String[] message = {e.toString()};
            throw new PDBApplicationException(ProductMaintConstants.OE_CONVERT_VO_TO_XML_EXCEPTION, message);
        }
        
        return doc;  
    }

}
