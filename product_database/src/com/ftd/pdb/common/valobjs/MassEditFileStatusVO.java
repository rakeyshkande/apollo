package com.ftd.pdb.common.valobjs;

public class MassEditFileStatusVO {
	
	private String user;
	private String fileUploadDate;
	private String fileName;
	private String status;
	private Long totalCount;
	private Long successRecordsCount;
	private Long failedRecordsCount;
	private String errorFileName;
	
	
	public MassEditFileStatusVO() {
		super();
	}

	public MassEditFileStatusVO(String user, String fileUploadDate,
			String fileName, String status, Long totalCount,
			Long successRecordsCount, Long failedRecordsCount) {
		super();
		this.user = user;
		this.fileUploadDate = fileUploadDate;
		this.fileName = fileName;
		this.status = status;
		this.totalCount = totalCount;
		this.successRecordsCount = successRecordsCount;
		this.failedRecordsCount = failedRecordsCount;
	}

	
	public String getErrorFileName() {
		return errorFileName;
	}

	public void setErrorFileName(String errorFileName) {
		this.errorFileName = errorFileName;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getFileUploadDate() {
		return fileUploadDate;
	}

	public void setFileUploadDate(String fileUploadDate) {
		this.fileUploadDate = fileUploadDate;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}

	public Long getSuccessRecordsCount() {
		return successRecordsCount;
	}

	public void setSuccessRecordsCount(Long successRecordsCount) {
		this.successRecordsCount = successRecordsCount;
	}

	public Long getFailedRecordsCount() {
		return failedRecordsCount;
	}

	public void setFailedRecordsCount(Long failedRecordsCount) {
		this.failedRecordsCount = failedRecordsCount;
	}
	
}
