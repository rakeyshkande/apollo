package com.ftd.pdb.common.valobjs;

public class RecipientVO   extends PDBValueObject
{
    private String Recipient;
    
    public RecipientVO() {
        super("com.ftd.pdb.common.valobjs.RecipientVO");
    }

    public String getRecipient()
    {
        return Recipient;
    }

    public void setRecipient(String newRecipient)
    {
        Recipient = newRecipient;
    }
}