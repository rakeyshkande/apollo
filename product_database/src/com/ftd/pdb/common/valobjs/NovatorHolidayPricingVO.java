package com.ftd.pdb.common.valobjs;

import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.XMLParsingException;
import com.ftd.pdb.common.utilities.FTDUtil;
import com.ftd.pdb.common.utilities.XMLUtilities;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


public class NovatorHolidayPricingVO extends PDBValueObject 
{
    private String productId;
    private String startDate;
    private String endDate;
    private float standardPrice;
    private float deluxePrice;
    private float premiumPrice;
    private String sku;
    private List subCodes;
    
    public NovatorHolidayPricingVO() {
        super("com.ftd.pdb.common.valobjs.NovatorHolidayPricingVO");
    }

    /**
     * @return
     * @deprecated Holiday Pricing is unused and will be removed in the future.
     *   lpuckett 08/16/2006
     */
    public String getProductId()
    {
        return productId;
    }

    /**
     * @param newProductId
     * @deprecated Holiday Pricing is unused and will be removed in the future.
     *   lpuckett 08/16/2006
     */
    public void setProductId(String newProductId)
    {
        productId = newProductId;
    }

    /**
     * @return
     * @deprecated Holiday Pricing is unused and will be removed in the future.
     *   lpuckett 08/16/2006
     */
    public String getStartDate()
    {
        return startDate;
    }

    /**
     * @param newStartDate
     * @deprecated Holiday Pricing is unused and will be removed in the future.
     *   lpuckett 08/16/2006
     */
    public void setStartDate(String newStartDate)
    {
        startDate = newStartDate;
    }

    /**
     * @return
     * @deprecated Holiday Pricing is unused and will be removed in the future.
     *   lpuckett 08/16/2006
     */
    public String getEndDate()
    {
        return endDate;
    }

    /**
     * @param newEndDate
     * @deprecated Holiday Pricing is unused and will be removed in the future.
     *   lpuckett 08/16/2006
     */
    public void setEndDate(String newEndDate)
    {
        endDate = newEndDate;
    }

    /**
     * @return
     * @deprecated Holiday Pricing is unused and will be removed in the future.
     *   lpuckett 08/16/2006
     */
    public float getStandardPrice()
    {
        return standardPrice;
    }

    /**
     * @param newStandardPrice
     * @deprecated Holiday Pricing is unused and will be removed in the future.
     *   lpuckett 08/16/2006
     */
    public void setStandardPrice(float newStandardPrice)
    {
        standardPrice = newStandardPrice;
    }

    /**
     * @return
     * @deprecated Holiday Pricing is unused and will be removed in the future.
     *   lpuckett 08/16/2006
     */
    public float getDeluxePrice()
    {
        return deluxePrice;
    }

    /**
     * @param newDeluxePrice
     * @deprecated Holiday Pricing is unused and will be removed in the future.
     *   lpuckett 08/16/2006
     */
    public void setDeluxePrice(float newDeluxePrice)
    {
        deluxePrice = newDeluxePrice;
    }

    /**
     * @return
     * @deprecated Holiday Pricing is unused and will be removed in the future.
     *   lpuckett 08/16/2006
     */
    public float getPremiumPrice()
    {
        return premiumPrice;
    }

    /**
     * @param newPremiumPrice
     * @deprecated Holiday Pricing is unused and will be removed in the future.
     *   lpuckett 08/16/2006
     */
    public void setPremiumPrice(float newPremiumPrice)
    {
        premiumPrice = newPremiumPrice;
    }

    /**
     * @return
     * @deprecated Holiday Pricing is unused and will be removed in the future.
     *   lpuckett 08/16/2006
     */
    public String getSku()
    {
        return sku;
    }

    /**
     * @param newSku
     * @deprecated Holiday Pricing is unused and will be removed in the future.
     *   lpuckett 08/16/2006
     */
    public void setSku(String newSku)
    {
        sku = newSku;
    }

    /**
     * @return
     * @deprecated Holiday Pricing is unused and will be removed in the future.
     *   lpuckett 08/16/2006
     */
    public List getSubCodes()
    {
        return subCodes;
    }

    /**
     * @param newSubCodes
     * @deprecated Holiday Pricing is unused and will be removed in the future.
     *   lpuckett 08/16/2006
     */
    public void setSubCodes(List newSubCodes)
    {
        subCodes = newSubCodes;
    }

    /**
    * Returns a representation ofthe object in XML
    * @deprecated Holiday Pricing is unused and will be removed in the future.
    *   lpuckett 08/16/2006
    */
     public String toXML() throws PDBApplicationException
     {   
         String retval = null;
         
         try {
             Document doc = toXMLDocument();
             Element root = (Element)doc.getFirstChild();
             XMLUtilities.toString(root);
         } catch (XMLParsingException xpe) {
             String[] message = {xpe.toString()};
             throw new PDBApplicationException(ProductMaintConstants.OE_CONVERT_VO_TO_XML_EXCEPTION, message);
         }
         
         return retval;
     }


    /**
     * @return
     * @throws PDBApplicationException
     * @deprecated Holiday Pricing is unused and will be removed in the future.
     *   lpuckett 08/16/2006
     */
    public Document toXMLDocument() throws PDBApplicationException
    {
        Document doc = null;
        try 
        {
            doc = XMLUtilities.createDocument();
            Element root = doc.createElement("product_holiday_pricing");
            root.appendChild(XMLUtilities.buildSimpleXmlNode(doc,"product_id",productId));
            
            SimpleDateFormat sdf = new SimpleDateFormat(ProductMaintConstants.NOVATOR_DATEFORMAT);
            String strDate = null;
            if(startDate != null)
            {
               strDate = sdf.format(startDate);
            }
            root.appendChild(XMLUtilities.buildSimpleXmlNode(doc,"start_date", (String)FTDUtil.convertNulls(strDate)));
            
            strDate = null;
            if(endDate != null)
            {
               strDate = sdf.format(endDate);
            }
            root.appendChild(XMLUtilities.buildSimpleXmlNodeCDATA(doc,"end_date", (String)FTDUtil.convertNulls(strDate)));
            
            root.appendChild(XMLUtilities.buildSimpleXmlNode(doc,"new_price_standard",FTDUtil.removeFormattingCurrency(FTDUtil.formatFloat(new Float(standardPrice), 2, false, false))));
            root.appendChild(XMLUtilities.buildSimpleXmlNode(doc,"new_price_deluxe",FTDUtil.removeFormattingCurrency(FTDUtil.formatFloat(new Float(deluxePrice), 2, false, false))));
            root.appendChild(XMLUtilities.buildSimpleXmlNode(doc,"new_price_premium",FTDUtil.removeFormattingCurrency(FTDUtil.formatFloat(new Float(premiumPrice), 2, false, false))));
            root.appendChild(XMLUtilities.buildSimpleXmlNode(doc,"new_sku",(String)FTDUtil.convertNulls(sku)));
            
            Element subCodesElement = doc.createElement("subcodes");
            if(subCodes != null)
            {
                for(int i = 0; i < subCodes.size(); i++)
                {
                    Element subCodeElement = doc.createElement("subcode");
                    subCodeElement.appendChild(XMLUtilities.buildSimpleXmlNode(doc,"subcode_ID",(String)FTDUtil.convertNulls(((ProductSubCodeVO)subCodes.get(i)).getProductSubCodeId())));
                    subCodeElement.appendChild(XMLUtilities.buildSimpleXmlNode(doc,"new_sku",(String)FTDUtil.convertNulls(((ProductSubCodeVO)subCodes.get(i)).getSubCodeHolidaySKU())));
                    subCodeElement.appendChild(XMLUtilities.buildSimpleXmlNode(doc,"new_price",(String)FTDUtil.removeFormattingCurrency(FTDUtil.formatFloat(new Float(((ProductSubCodeVO)subCodes.get(i)).getSubCodeHolidayPrice()), 2, false, false))));
                    subCodesElement.appendChild(subCodeElement);
                }
            }
            
            root.appendChild(subCodesElement);
            doc.appendChild(root);
        }
        catch (Exception e) 
        {
            String[] message = {e.toString()};
            throw new PDBApplicationException(ProductMaintConstants.OE_CONVERT_VO_TO_XML_EXCEPTION, message);
        }
        
        return doc;        
    }
}