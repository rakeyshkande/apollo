package com.ftd.pdb.common.valobjs;

import java.util.ArrayList;


/**
 * Card VO contains available and selected.
 *
 * @author 	Ed Mueller
 */
public class CardVO extends PDBValueObject 
{

    private ArrayList availableCards;
    private ArrayList selectedCards;
    
    public CardVO() {
        super("com.ftd.pdb.common.valobjs.CardVO");
    }

/** 
 * Get available cards
 *
 * @return ArrayList of available cards
 */
    public ArrayList getAvailableCards()
    {
        return availableCards;
    }

/** 
 * Set available cards
 *
 * @param newAvailableCards ArrayList of ew available cards
 */
    public void setAvailableCards(ArrayList newAvailableCards)
    {
        availableCards = newAvailableCards;
    }

/** 
 * Get selected cards
 *
 * @return ArrayList of selected cards
 */
    public ArrayList getSelectedCards()
    {
        return selectedCards;
    }

/** 
 * Set selected cards
 *
 * @param newSelectedCards ArrayList of new selected cards
 */
    public void setSelectedCards(ArrayList newSelectedCards)
    {
        selectedCards = newSelectedCards;
    }
   
}