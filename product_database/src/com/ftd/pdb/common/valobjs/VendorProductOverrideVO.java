package com.ftd.pdb.common.valobjs;

import java.util.Date;

public class VendorProductOverrideVO extends PDBValueObject {
    private String vendorId;
    private String productId;
    private String carrierId;
    private Date   deliveryOverrideDate;
    private String deliveryOverrideDateFormatted;
    private String vendorName;
    private String sdsShipMethod;
    
    public VendorProductOverrideVO() {
        super("com.ftd.pdb.common.valobjs.VendorProductOverrideVO");
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductId() {
        return productId;
    }

    public void setCarrierId(String carrierId) {
        this.carrierId = carrierId;
    }

    public String getCarrierId() {
        return carrierId;
    }

    public void setDeliveryOverrideDate(Date deliveryOverrideDate) {
        this.deliveryOverrideDate = deliveryOverrideDate;
    }

    public Date getDeliveryOverrideDate() {
        return deliveryOverrideDate;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setDeliveryOverrideDateFormatted(String overrideDeliveryDateFormatted) {
        this.deliveryOverrideDateFormatted = overrideDeliveryDateFormatted;
    }

    public String getDeliveryOverrideDateFormatted() {
        return deliveryOverrideDateFormatted;
    }

    public void setSdsShipMethod(String sdsShipMethod) {
        this.sdsShipMethod = sdsShipMethod;
    }

    public String getSdsShipMethod() {
        return sdsShipMethod;
    }

}
