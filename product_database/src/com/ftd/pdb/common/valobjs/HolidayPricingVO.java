package com.ftd.pdb.common.valobjs;

import java.util.Date;

/**
 * HolidayPricing VO contains pricing information.
 *
 * @author 	Ed Mueller
 */
public class HolidayPricingVO extends PDBValueObject 
{

    private boolean holidayPriceFlag;
    private Date startDate;
    private Date endDate;
    private boolean deliveryDateFlag;
    
    public HolidayPricingVO() {
        super("com.ftd.pdb.common.valobjs.HolidayPricingVO");
    }

/** 
 * getHolidayPriceFlag - Indicates if holiday pricing is on or off
 *
 * @return boolean
 *          true if holiday pricing is on, else false
 * @deprecated Holiday Pricing is unused and will be removed in the future.
 *   lpuckett 08/16/2006
 */
    public boolean getHolidayPriceFlag()
    {
        return holidayPriceFlag;
    }

/** 
 * setHolidayPriceFlag - Sets holiday pricing on or off
 *
 * @param newHolidayPriceFlag
 *          true if holiday pricing is on, else false
 * @deprecated Holiday Pricing is unused and will be removed in the future.
 *   lpuckett 08/16/2006
 */
    public void setHolidayPriceFlag(boolean newHolidayPriceFlag)
    {
        holidayPriceFlag = newHolidayPriceFlag;
    }

/** 
 * getStartDate - Gets holiday pricing start date
 *
 * @return Date - holiday prcing start date
 * @deprecated Holiday Pricing is unused and will be removed in the future.
 *   lpuckett 08/16/2006
 */
    public Date getStartDate()
    {
        return startDate;
    }

/** 
 * setStartDate - Sets holiday pricing start date
 *
 * @param newStartDate - holiday prcing start date
 * @deprecated Holiday Pricing is unused and will be removed in the future.
 *   lpuckett 08/16/2006
 */
    public void setStartDate(Date newStartDate)
    {
        startDate = newStartDate;
    }

/** 
 * getEndDate - Gets holiday pricing end date
 *
 * @return Date - holiday prcing end date
 * @deprecated Holiday Pricing is unused and will be removed in the future.
 *   lpuckett 08/16/2006
 */
    public Date getEndDate()
    {
        return endDate;
    }

/** 
 * setEndDate - Sets holiday pricing end date
 *
 * @param newEndDate - holiday prcing end date
 * @deprecated Holiday Pricing is unused and will be removed in the future.
 *   lpuckett 08/16/2006
 */
    public void setEndDate(Date newEndDate)
    {
        endDate = newEndDate;
    }

/** 
 * getDeliveryDateFlag - Flag that denotes if the shipping charges are based
 *                       on Shipping or Order Date.
 *
 * @return boolean
 *          true if charges based on shipping date, else false
 * @deprecated Holiday Pricing is unused and will be removed in the future.
 *   lpuckett 08/16/2006
 */
    public boolean getDeliveryDateFlag()
    {
        return deliveryDateFlag;
    }

/** 
 * setDeliveryDateFlag - Set flag that denotes if the shipping charges are based
 *                       on Shipping or Order Date.
 *
 * @return boolean
 *          true if charges based on shipping date, else false
 * @deprecated Holiday Pricing is unused and will be removed in the future.
 *   lpuckett 08/16/2006
 */
    public void setDeliveryDateFlag(boolean newDeliveryDateFlag)
    {
        deliveryDateFlag = newDeliveryDateFlag;
    }
}