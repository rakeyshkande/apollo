package com.ftd.pdb.common.valobjs;

public class ProductSecondChoiceVO extends PDBValueObject
{
  private String description;
  private String mercuryDescription;
  private String productSecondChoiceID;

  public ProductSecondChoiceVO() {
      super("com.ftd.pdb.common.valobjs.ProductSecondChoiceVO");
  }
  public String getDescription()
  {
    return description;
  }

  public void setDescription(String newDescription)
  {
    description = newDescription;
  }

  public String getMercuryDescription()
  {
    return mercuryDescription;
  }

  public void setMercuryDescription(String newMercuryDescription)
  {
    mercuryDescription = newMercuryDescription;
  }


  public String getProductSecondChoiceID()
  {
    return productSecondChoiceID;
  }

  public void setProductSecondChoiceID(String newChoice)
  {
    productSecondChoiceID = newChoice;
  }
}