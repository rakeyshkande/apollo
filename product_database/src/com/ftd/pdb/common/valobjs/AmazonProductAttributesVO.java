package com.ftd.pdb.common.valobjs;


public class AmazonProductAttributesVO extends PDBValueObject
{

    public AmazonProductAttributesVO() {
        super("com.ftd.pdb.common.valobjs.AmazonProductAttributesVO");
    }

    String productId;
    String productAttributeType;
	String productAttributeValue;
    int sequence;
    
    public String getProductId() {
		return productId;
	}

    public void setProductId(String productId) {
		this.productId = productId;
	}
	
	public String getProductAttributeType() {
		return productAttributeType;
	}

	public void setProductAttributeType(String productAttributeType) {
		this.productAttributeType = productAttributeType;
	}

    public String getProductAttributeValue() {
		return productAttributeValue;
	}
	
    public void setProductAttributeValue(String productAttributeValue) {
		this.productAttributeValue = productAttributeValue;
	}
	
    public int getSequence() {
		return sequence;
	}
	
    public void setSequence(int sequence) {
		this.sequence = sequence;
	}

 }
