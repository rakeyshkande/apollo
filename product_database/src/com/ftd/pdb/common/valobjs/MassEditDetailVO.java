package com.ftd.pdb.common.valobjs;

import java.util.Date;

public class MassEditDetailVO {
	
	private Long massEditHeaderId;
	private Long massEditDetailId;
	private String upsellMasterId;
	private String upsellDetailId;
	private String productId;
	private String status;
	private String errorMsg;
	private Date createdOn;
	private String createdBy;
	private String updatedBy;
	private Date updatedOn;
	
	
	public MassEditDetailVO() {
		super();
	}

	public MassEditDetailVO(Long massEditHeaderId, Long massEditDetailId,
			String upsellMasterId, String upsellDetailId, String productId,
			String status, String errorMsg, Date createdOn, String createdBy,
			String updatedBy, Date updatedOn) {
		super();
		this.massEditHeaderId = massEditHeaderId;
		this.massEditDetailId = massEditDetailId;
		this.upsellMasterId = upsellMasterId;
		this.upsellDetailId = upsellDetailId;
		this.productId = productId;
		this.status = status;
		this.errorMsg = errorMsg;
		this.createdOn = createdOn;
		this.createdBy = createdBy;
		this.updatedBy = updatedBy;
		this.updatedOn = updatedOn;
	}
	
	public MassEditDetailVO(Long massEditHeaderId,
			String upsellMasterId, String upsellDetailId, String productId,
			String status, String errorMsg, String createdBy,
			String updatedBy) {
		super();
		this.massEditHeaderId = massEditHeaderId;
		this.upsellMasterId = upsellMasterId;
		this.upsellDetailId = upsellDetailId;
		this.productId = productId;
		this.status = status;
		this.errorMsg = errorMsg;
		this.createdBy = createdBy;
		this.updatedBy = updatedBy;
	}

	public Long getMassEditHeaderId() {
		return massEditHeaderId;
	}

	public void setMassEditHeaderId(Long massEditHeaderId) {
		this.massEditHeaderId = massEditHeaderId;
	}

	public Long getMassEditDetailId() {
		return massEditDetailId;
	}

	public void setMassEditDetailId(Long massEditDetailId) {
		this.massEditDetailId = massEditDetailId;
	}

	public String getUpsellMasterId() {
		return upsellMasterId;
	}

	public void setUpsellMasterId(String upsellMasterId) {
		this.upsellMasterId = upsellMasterId;
	}

	public String getUpsellDetailId() {
		return upsellDetailId;
	}

	public void setUpsellDetailId(String upsellDetailId) {
		this.upsellDetailId = upsellDetailId;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	
	

}
