package com.ftd.pdb.common.valobjs;

import java.util.Date;

public class ProductShippingRestrictionsVO extends PDBValueObject
{
  private String productID;
  private Date startDate;
  private Date endDate;
  
  public ProductShippingRestrictionsVO() {
      super("com.ftd.pdb.common.valobjs.ProductShippingRestrictionsVO");
  }

  public String getProductID()
  {
    return productID;
  }

  public void setProductID(String newProductID)
  {
    productID = newProductID;
  }

  public Date getStartDate()
  {
    return startDate;
  }

  public void setStartDate(Date newStartDate)
  {
    startDate = newStartDate;
  }

  public Date getEndDate()
  {
    return endDate;
  }

  public void setEndDate(Date newEndDate)
  {
    endDate = newEndDate;
  }


}