package com.ftd.pdb.common.valobjs;

public class VendorVO extends PDBValueObject
{
	private String vendorId;
	private String vendorName;
        private String vendorType;
        
    public VendorVO() {
        super("com.ftd.pdb.common.valobjs.VendorVO");
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorType(String vendorType) {
        this.vendorType = vendorType;
    }

    public String getVendorType() {
        return vendorType;
    }
}
