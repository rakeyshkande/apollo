package com.ftd.pdb.common.valobjs;

//Response obtained from Novator
public class NovatorResponseVO extends PDBValueObject
{

    //Indicates if Novator responsed with a success or failure
    private boolean Success;

    //indicates if the Novator response could be determined
    private boolean Parseable;

    //Description received from Novator
    private String Description;
    
    public NovatorResponseVO() {
        super("com.ftd.pdb.common.valobjs.NovatorResponseVO");
    }

    public boolean isSuccess()
    {
        return Success;
    }

    public void setSuccess(boolean newSuccess)
    {
        Success = newSuccess;
    }

    public boolean isParseable()
    {
        return Parseable;
    }

    public void setParseable(boolean newCouldParse)
    {
        Parseable = newCouldParse;
    }

    public String getDescription()
    {
        return Description;
    }

    public void setDescription(String newDescription)
    {
        Description = newDescription;
    }

 
}