package com.ftd.pdb.common.valobjs;

public class ColorVO extends PDBValueObject 
{
    private String id;
    private String description;
    private int orderBy;
    private String productId;
    private String markerType;
    
    public ColorVO() {
        super("com.ftd.pdb.common.valobjs.ColorVO");
    }

    public String getId()
    {
        return id;
    }

    public void setId(String newId)
    {
        id = newId;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String newDescription)
    {
        description = newDescription;
    }

    public int getOrderBy()
    {
        return orderBy;
    }

    public void setOrderBy(int newOrderBy)
    {
        orderBy = newOrderBy;
    }

    public String getProductId()
    {
        return productId;
    }

    public void setProductId(String newProductId)
    {
        productId = newProductId;
    }
    
    public String getMarkerType() {
		return markerType;
	}

	public void setMarkerType(String markerType) {
		this.markerType = markerType;
	}

	public boolean equals(Object obj)
    {
        boolean ret = false;
        if(id.equals(((ColorVO)obj).getId()))
        {
            ret = true;
        }

        return ret;
    }    
}