package com.ftd.pdb.common.valobjs;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.feed.FeedXMLTags;
import com.ftd.osp.utilities.xml.JAXPException;
import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.pdb.common.PDBXMLTags;
import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBSystemException;
import com.ftd.pdb.common.exceptions.XMLParsingException;
import com.ftd.pdb.common.utilities.FTDUtil;
import com.ftd.pdb.common.utilities.NovatorTransmission;
import com.ftd.pdb.integration.dao.ILookupDAO;
import com.ftd.pdb.integration.dao.LookupDAOImpl;

import java.math.BigDecimal;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import javax.servlet.http.HttpServletRequest;


import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 * ProductVO is a ValueObject representing a Product in the Order Entry
 * application.
 * A ProductVO encapsulates all data associated with an Product.
 *
 * @author 	William Sandner
 * @version 	%I%, %G%
 * @since       JDK1.3.1
 */                                            
public class ProductVO extends PDBValueObject
{
    /*Spring provided resources*/
    ILookupDAO lookupDAO = new LookupDAOImpl();
    /*end Spring provided resources*/
    
    private String standardPriceRank;
    private String premiumPriceRank;
    private String deluxePriceRank;
    private String productId;
    private String novatorId;
    private boolean status;
    private String deliveryType;
    private String productType;
    private String productSubType;
    private boolean colorSizeFlag;
    private float standardPrice;
    private float deluxePrice;
    private float premiumPrice;
    private int preferredPricePoint;
    private float variablePriceMax;
    private String longDescription;
    private String floristReferenceNumber;
    private String mercuryDescription;
    private String itemComments;
    private boolean addOnBalloonsFlag;
    private boolean addOnBearsFlag;
    private boolean addOnGreetingCardsFlag;
    private boolean addOnFuneralFlag;
    private String codifiedFlag;
    private String exceptionCode;
    private String exceptionStartDate;
    private String exceptionEndDate;
    private String exceptionMessage;
    private String dropShipCode;
    private boolean discountAllowedFlag;
    private boolean deliveryIncludedFlag;
    private boolean taxFlag;
    private boolean serviceFeeFlag;
    private boolean exoticFlag;
    private boolean eGiftFlag;
    private String arrangementSize;
    private String dominantFlowers;
    private String searchPriority;
    private String[] recipientSearch;
    private String personalizationTemplate;
    private String personalizationTemplateOrder;
    private String personalizationLeadDays;
    private boolean personalizationCaseFlag;
    private boolean allAlphaFlag;
    private String recipe;
    private boolean sendStandardRecipe;
    private String deluxeRecipe;
    private boolean sendDeluxeRecipe;
    private String premiumRecipe;
    private boolean sendPremiumRecipe;
    private boolean subcodeFlag;
    private String dimWeight;
    private boolean nextDayUpgrade;
    private String novatorName;
    private String productName;
    private String lastUpdateDate;
    private boolean corporateSiteFlag;
    private String category;
    private String shippingAvailability;
    private String country;
    private String unspscCode;
    private boolean variablePricingFlag;
    private String keywordSearch;
    private String keywords;
    private String secondChoice;
    private List excludedDeliveryStates;
    private boolean shipDaySunday;
    private boolean shipDayMonday;
    private boolean shipDayTuesday;
    private boolean shipDayWednesday;
    private boolean shipDayThursday;
    private boolean shipDayFriday;
    private boolean shipDaySaturday;

    private String shippingKey;
    private boolean shipMethodCarrier;
    private boolean shipMethodFlorist;
    private boolean zoneJumpEligibleFlag;
    private String boxId;
    private boolean catelogFlag;
    private List subCodeList;
    private List shipMethods;
    private String holidaySecondChoice;
    private String mercurySecondChoice;
    private String mercuryHolidaySecondChoice;
    private List colorsList;
    private List allColorsList;
    private String newSKU;
    private float newStandardPrice;
    private float newDeluxePrice;
    private float newPremiumPrice;
    private String newStartDate;
    private String newEndDate;
    //private String arrangementColors;
    private boolean addOnChocolateFlag;
    private String crossRefNovatorID;
    private String generalComments;
    private Boolean sentToNovatorProd;
    private Boolean sentToNovatorUAT;
    private Boolean sentToNovatorTest;
    private Boolean sentToNovatorContent;
    private boolean holdUntilAvailable;
    private String defaultCarrier;
    private String[] companyList;
    private String lastUpdateUserId;
    private String lastUpdateSystem;
    private boolean mondayDeliveryFreshCuts;
    private boolean expressShippingOnly;
    private boolean twoDaySaturdayShipFreshCuts;
    private boolean batchMode;
    private boolean weboeBlocked;
    private boolean over21Flag;
    private boolean customFlag;
    private boolean personalGreetingFlag;
    private boolean premierCollectionFlag;
    private float supplyExpense;
    private String supplyExpenseEffDate;
    private float royaltyPercent;
    private String royaltyPercentEffDate;
    private String shippingSystem;
    private List vendorProductsList;
    private String[] shippingMethodList;
    private String[] novatorTagList;
    private String[] arrangementColorsArray;
    private String departmentCode;
    //private List arrangementColorsArray = new ArrayList();
    private List componentSkuList;
    private List allComponentSkuList;
    private String[] componentSkuArray;
    private boolean gbbPopoverFlag;
    private String gbbTitle;
    private boolean gbbNameOverrideFlag1;
    private String gbbNameOverrideText1;
    private boolean gbbPriceOverrideFlag1;
    private String gbbPriceOverrideText1;
    private boolean gbbNameOverrideFlag2;
    private String gbbNameOverrideText2;
    private boolean gbbPriceOverrideFlag2;
    private String gbbPriceOverrideText2;
    private boolean gbbNameOverrideFlag3;
    private String gbbNameOverrideText3;
    private boolean gbbPriceOverrideFlag3;
    private String gbbPriceOverrideText3;   
    private HashMap <String, ArrayList <ProductAddonVO>> productAddonMap;
    private String websites;
    private String duration;
    private boolean allowFreeShippingFlag;
    private boolean morningDeliveryFlag;
    private boolean isCopyProduct;
    private boolean azCatalogStatusStandard;
    private String azProductNameStandard;
    private String azProductDescriptionStandard;
    private boolean azCatalogStatusDeluxe;
    private String azProductNameDeluxe;
    private String azProductDescriptionDeluxe;
    private boolean azCatalogStatusPremium;
    private String azProductNamePremium;
    private String azProductDescriptionPremium;
    private String azBulletPoint1;
    private String azBulletPoint2;
    private String azBulletPoint3;
    private String azBulletPoint4;
    private String azBulletPoint5;
    private String azSearchTerm1;
    private String azSearchTerm2;
    private String azSearchTerm3;
    private String azSearchTerm4;
    private String azSearchTerm5;
    private String azIntendedUse1;
    private String azIntendedUse2;
    private String azIntendedUse3;
    private String azIntendedUse4;
    private String azIntendedUse5;
    private String azTargetAudience1;
    private String azTargetAudience2;
    private String azTargetAudience3;
    private String azTargetAudience4;
    private String azOtherAttribute1;
    private String azOtherAttribute2;
    private String azOtherAttribute3;
    private String azOtherAttribute4;
    private String azOtherAttribute5;
    private String azSubjectMatter1;
    private String azSubjectMatter2;
    private String azSubjectMatter3;
    private String azSubjectMatter4;
    private String azSubjectMatter5;
    private String azItemType;
    
    // These fields are temporary fields only used for PDB Mass Edit
    private String massEditMasterSKU;
    private String massEditProductStatus;
    private String massEditSpreadSheetGeneralComments;
    private String massEditExceptionMsg;
    // End of PDB Mass Edit fields 
    private String pquadProductID; 
    
    private String ftdwestAccessoryId;
    private String ftdwestTemplateMapping;
    
    private String bulletDescription;
    

	public static final SimpleDateFormat NOVATOR_FORMAT = new SimpleDateFormat(ProductMaintConstants.NOVATOR_DATEFORMAT);
    public static final SimpleDateFormat PDB_FORMAT = new SimpleDateFormat (ProductMaintConstants.PDB_DATE_FORMAT);
    
    public ProductVO() {
        super("com.ftd.pdb.common.valobjs.ProductVO");
    }

    public void setLastUpdateUserId(String newLastUpdateUserId)
    {
        lastUpdateUserId = newLastUpdateUserId;
    }
    
    public String getLastUpdateUserId()
    {
        return lastUpdateUserId;
    }
    
    public void setLastUpdateSystem(String newLastUpdateSystem)
    {
        lastUpdateSystem = newLastUpdateSystem;
    }
    
    public String getLastUpdateSystem()
    {
        return lastUpdateSystem;
    }
    
    public String getProductId()
    {
        return productId;
    }
    
    public void setProductId(String newProductId)
    {
        productId = newProductId;
    }
    
    public String getNovatorId()
    {
        return novatorId;
    }
    
    public void setNovatorId(String newNovatorId)
    {
        novatorId = newNovatorId;
    }
    
    public boolean isStatus()
    {
        return status;
    }
    
    public void setStatus(boolean newStatus)
    {
        status = newStatus;
    }
    
    public void setHoldUntilAvailable(boolean newHoldUntilAvailable)
    {
        holdUntilAvailable = newHoldUntilAvailable;
    }
    
    public boolean getHoldUntilAvailable()
    {
        return holdUntilAvailable;
    }
    
    public String getDeliveryType()
    {
        return deliveryType;
    }
    
    public void setDeliveryType(String newDeliveryType)
    {
        deliveryType = newDeliveryType;
    }
    
    public String getProductType()
    {
    return productType;
    }
    
    public void setProductType(String newProductType)
    {
    productType = newProductType;
    }
    
    public String getProductSubType()
    {
    return productSubType;
    }
    
    public void setProductSubType(String newProductSubType)
    {
    productSubType = newProductSubType;
    }
    
    public boolean isColorSizeFlag()
    {
        return colorSizeFlag;
    }
    
    public void setColorSizeFlag(boolean newColorSizeFlag)
    {
        colorSizeFlag = newColorSizeFlag;
    }
    
    public float getStandardPrice()
    {
        return standardPrice;
    }
    
    public void setStandardPrice(float newStandardPrice)
    {
        standardPrice = newStandardPrice;
    }
    
    public float getDeluxePrice()
    {
        return deluxePrice;
    }
    
    public void setDeluxePrice(float newDeluxePrice)
    {
        deluxePrice = newDeluxePrice;
    }
    
    public float getPremiumPrice()
    {
        return premiumPrice;
    }
    
    public void setPremiumPrice(float newPremiumPrice)
    {
        premiumPrice = newPremiumPrice;
    }
    
    public int getPreferredPricePoint()
    {
        return preferredPricePoint;
    }
    
    public void setPreferredPricePoint(int newPreferredPricePoint)
    {
        preferredPricePoint = newPreferredPricePoint;
    }
    
    public float getVariablePriceMax()
    {
        return variablePriceMax;
    }
    
    public void setVariablePriceMax(float newVariablePriceMax)
    {
        variablePriceMax = newVariablePriceMax;
    }
    //shortDescription is removed 
    public String getShortDescription()
    {
        return null;
    }
    
    public String getLongDescription()
    {
        return longDescription;
    }
    
    public void setLongDescription(String newLongDescription)
    {
        longDescription = newLongDescription;
    }
    
    public String getFloristReferenceNumber()
    {
        return floristReferenceNumber;
    }
    
    public void setFloristReferenceNumber(String newFloristReferenceNumber)
    {
        floristReferenceNumber = newFloristReferenceNumber;
    }
    
    public String getMercuryDescription()
    {
        return mercuryDescription;
    }
    
    public void setMercuryDescription(String newMercuryDescription)
    {
        mercuryDescription = newMercuryDescription;
    }
    
    public String getItemComments()
    {
        return itemComments;
    }
    
    public void setItemComments(String newItemComments)
    {
        itemComments = newItemComments;
    }
    
    public boolean isAddOnBalloonsFlag()
    {
        return addOnBalloonsFlag;
    }
    
    public void setAddOnBalloonsFlag(boolean newAddOnBalloonsFlag)
    {
        addOnBalloonsFlag = newAddOnBalloonsFlag;
    }
    
    public boolean isAddOnBearsFlag()
    {
        return addOnBearsFlag;
    }
    
    public void setAddOnBearsFlag(boolean newAddOnBearsFlag)
    {
        addOnBearsFlag = newAddOnBearsFlag;
    }
    
    public boolean isAddOnGreetingCardsFlag()
    {
        return addOnGreetingCardsFlag;
    }
    
    public void setAddOnGreetingCardsFlag(boolean newAddOnGreetingCardsFlag)
    {
        addOnGreetingCardsFlag = newAddOnGreetingCardsFlag;
    }
    
    public boolean isAddOnFuneralFlag()
    {
        return addOnFuneralFlag;
    }
    
    public void setAddOnFuneralFlag(boolean newAddOnFuneralFlag)
    {
        addOnFuneralFlag = newAddOnFuneralFlag;
    }
    
    public String getCodifiedFlag()
    {
        return codifiedFlag;
    }
    
    public void setCodifiedFlag(String newCodifiedFlag)
    {
        codifiedFlag = newCodifiedFlag;
    }
    
    public String getExceptionCode()
    {
        return exceptionCode;
    }
    
    public void setExceptionCode(String newExceptionCode)
    {
        // trying to make it so Exception code can only be the available code, null, or blank..
        // thus gradually removing the unavailable key
        if( newExceptionCode == null ||
            newExceptionCode.equalsIgnoreCase(ProductMaintConstants.PRODUCT_AVAILABLE_KEY) ||
            newExceptionCode.equalsIgnoreCase("")){
                this.exceptionCode = newExceptionCode;
        }else{
            this.exceptionCode = null;
        }
    }
    
    public String getExceptionStartDate()
    {
        return exceptionStartDate;
    }
    
    public void setExceptionStartDate(String newExceptionStartDate)
    {
        exceptionStartDate = newExceptionStartDate;
    }
    
    public String getExceptionEndDate()
    {
        return exceptionEndDate;
    }
    
    public void setExceptionEndDate(String newExceptionEndDate)
    {
        exceptionEndDate = newExceptionEndDate;
    }
    
    public String getExceptionMessage()
    {
        return exceptionMessage;
    }
    
    public void setExceptionMessage(String newExceptionMessage)
    {
        exceptionMessage = newExceptionMessage;
    }
    
    public String getDropShipCode()
    {
        return dropShipCode;
    }
    
    public void setDropShipCode(String newDropShipCode)
    {
        dropShipCode = newDropShipCode;
    }
    
    public boolean isDiscountAllowedFlag()
    {
        return discountAllowedFlag;
    }
    
    public void setDiscountAllowedFlag(boolean newDiscountAllowedFlag)
    {
        discountAllowedFlag = newDiscountAllowedFlag;
    }
    
    public boolean isDeliveryIncludedFlag()
    {
        return deliveryIncludedFlag;
    }
    
    public void setDeliveryIncludedFlag(boolean newDeliveryIncludedFlag)
    {
        deliveryIncludedFlag = newDeliveryIncludedFlag;
    }
    
    public boolean isTaxFlag()
    {
        return taxFlag;
    }
    
    public void setTaxFlag(boolean newTaxFlag)
    {
        taxFlag = newTaxFlag;
    }
    
    public boolean isServiceFeeFlag()
    {
        return serviceFeeFlag;
    }
    
    public void setServiceFeeFlag(boolean newServiceFeeFlag)
    {
        serviceFeeFlag = newServiceFeeFlag;
    }
    
    public boolean isExoticFlag()
    {
        return exoticFlag;
    }
    
    public void setExoticFlag(boolean newExoticFlag)
    {
        exoticFlag = newExoticFlag;
    }
    
    public boolean isEGiftFlag()
    {
        return eGiftFlag;
    }
    
    public void setEGiftFlag(boolean newEGiftFlag)
    {
        eGiftFlag = newEGiftFlag;
    }
    
    public String getArrangementSize()
    {
    return arrangementSize;
    }
    
    public void setArrangementSize(String newArrangementSize)
    {
        arrangementSize = newArrangementSize;
    }
    
    
    public String getDominantFlowers()
    {
        return dominantFlowers;
    }
    
    public void setDominantFlowers(String newDominantFlowers)
    {
        dominantFlowers = newDominantFlowers;
    }
    
    public String getSearchPriority()
    {
        return searchPriority;
    }
    
    public void setSearchPriority(String newSearchPriority)
    {
        searchPriority = newSearchPriority;
    }
    
    public String[] getRecipientSearch()
    {
        return recipientSearch;
    }
    
    public void setRecipientSearch(String[] newRecipientSearch)
    {
        recipientSearch = newRecipientSearch;
    }
    
    public String getPersonalizationTemplate()
    {
        return personalizationTemplate;
    }
    
    public void setPersonalizationTemplate(String newPersonalizationTemplate)
    {
        personalizationTemplate = newPersonalizationTemplate;
    }
    
    public void setPersonalizationTemplateOrder(String personalizationTemplateOrder)
    {
      this.personalizationTemplateOrder = personalizationTemplateOrder;
    }
  
    public String getPersonalizationTemplateOrder()
    {
      return personalizationTemplateOrder;
    }

    public String getPersonalizationLeadDays()
    {
        return personalizationLeadDays;
    }
    
    public void setPersonalizationLeadDays(String newPersonalizationLeadDays)
    {
        personalizationLeadDays = newPersonalizationLeadDays;
    }
    
	public boolean isPersonalizationCaseFlag() {
		return personalizationCaseFlag;
	}

	public void setPersonalizationCaseFlag(boolean personalizationCaseFlag) {
		this.personalizationCaseFlag = personalizationCaseFlag;
	}

    public boolean isAllAlphaFlag() {
		return allAlphaFlag;
	}
	
	public void setAllAlphaFlag(boolean allAlphaFlag) {
		this.allAlphaFlag = allAlphaFlag;
	}
	
	public String getRecipe()
    {
        return recipe;
    }
    
    public void setRecipe(String newRecipe)
    {
        recipe = newRecipe;
    }
    
    public boolean isSubcodeFlag()
    {
        return subcodeFlag;
    }
    
    public void setSubcodeFlag(boolean newSubcodeFlag)
    {
        subcodeFlag = newSubcodeFlag;
    }
    
    public String getDimWeight()
    {
        return dimWeight;
    }
    
    public void setDimWeight(String newDimWeight)
    {
        dimWeight = newDimWeight;
    }
    
    public boolean getNextDayUpgrade()
    {
        return nextDayUpgrade;
    }
    
    public void setNextDayUpgrade(boolean newNextDayUpgrade)
    {
        nextDayUpgrade = newNextDayUpgrade;
    }

    public boolean getMondayDeliveryFreshCuts()
    {
        return mondayDeliveryFreshCuts;
    }

    public void setMondayDeliveryFreshCuts(boolean newMondayDeliveryFreshCuts)
    {
        mondayDeliveryFreshCuts = newMondayDeliveryFreshCuts;
    }
    
    public boolean isExpressShippingOnly()
    {
        return expressShippingOnly;
    }

    public void setExpressShippingOnly(boolean newExpressShippingOnly)
    {
        expressShippingOnly = newExpressShippingOnly;
    }

    public boolean getTwoDaySaturdayShipFreshCuts()
    {
        return twoDaySaturdayShipFreshCuts;
    }

    public void setTwoDaySaturdayShipFreshCuts(boolean newTwoDaySaturdayShipFreshCuts)
    {
        twoDaySaturdayShipFreshCuts = newTwoDaySaturdayShipFreshCuts;
    }

    public String getNovatorName()
    {
        return novatorName;
    }

    public void setNovatorName(String newNovatorName)
    {
        novatorName = newNovatorName;
    }

    public String getProductName()
    {
        return productName;
    }

    public void setProductName(String newProductName)
    {
        productName = newProductName;
    }

    public String getLastUpdateDate()
    {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(String newLastUpdateDate)
    {
        lastUpdateDate = newLastUpdateDate;
    }

    public boolean isCorporateSiteFlag()
    {
        return corporateSiteFlag;
    }

    public void setCorporateSiteFlag(boolean newCorporateSiteFlag)
    {
        corporateSiteFlag = newCorporateSiteFlag;
    }

    public String getCategory()
    {
        return category;
    }

    public void setCategory(String newCategory)
    {
        category = newCategory;
    }



    public String getShippingAvailability()
    {
        return shippingAvailability;
    }

    public void setShippingAvailability(String newShippingAvailability)
    {
        shippingAvailability = newShippingAvailability;
    }

    public String getCountry()
    {
        return country;
    }

    public void setCountry(String newCountry)
    {
        country = newCountry;
    }

    public String getUnspscCode()
    {
        return unspscCode;
    }

    public void setUnspscCode(String newUnspscCode)
    {
        unspscCode = newUnspscCode;
    }

/*   JMP 10/3 Removed per defect #54
    public Date getVendorBlockDateStart()
    {
        return vendorBlockDateStart;
    }

    public void setVendorBlockDateStart(Date newVendorBlockDateStart)
    {
        vendorBlockDateStart = newVendorBlockDateStart;
    }

    public Date getVendorBlockDateEnd()
    {
        return vendorBlockDateEnd;
    }

    public void setVendorBlockDateEnd(Date newVendorBlockDateEnd)
    {
        vendorBlockDateEnd = newVendorBlockDateEnd;
    }
*/
    public String getStandardPriceRank()
    {
        return standardPriceRank;
    }

    public void setStandardPriceRank(String newStandardPriceRank)
    {
        standardPriceRank = newStandardPriceRank;
    }

    public String getPremiumPriceRank()
    {
        return premiumPriceRank;
    }

    public void setPremiumPriceRank(String newPremiumPriceRank)
    {
        premiumPriceRank = newPremiumPriceRank;
    }

    public String getDeluxePriceRank()
    {
        return deluxePriceRank;
    }

    public void setDeluxePriceRank(String newDeluxePriceRank)
    {
        deluxePriceRank = newDeluxePriceRank;
    }

    public boolean isVariablePricingFlag()
    {
        return variablePricingFlag;
    }

    public void setVariablePricingFlag(boolean newVariablePricingFlag)
    {
        variablePricingFlag = newVariablePricingFlag;
    }

    public String getKeywordSearch()
    {
        return keywordSearch;
    }

    public void setKeywordSearch(String newKeywordSearch)
    {
        keywordSearch = newKeywordSearch;
    }

    public String getKeywords()
    {
        return keywords;
    }
  
    public void setKeywords(String newKeywords)
    {
        keywords = newKeywords;
    }

    public String getSecondChoice()
    {
        return secondChoice;
    }

    public void setSecondChoice(String newSecondChoice)
    {
        secondChoice = newSecondChoice;
    }

    public List getExcludedDeliveryStates()
    {
        return excludedDeliveryStates;
    }

    public void setExcludedDeliveryStates(List newExcludedDeliveryStates)
    {
        excludedDeliveryStates = newExcludedDeliveryStates;
    }

//	public List getStatesWithPossibleExclusions()
//	{
//		return statesWithPossibleExclusions;
//	}
//
//	public void setStatesWithPossibleExclusions(List newStatesWithPossibleExclusions)
//	{
//		statesWithPossibleExclusions = newStatesWithPossibleExclusions;
//	}

    public boolean isNextDayUpgrade()
    {
        return nextDayUpgrade;
    }

    public boolean isShipDaySunday()
    {
        return shipDaySunday;
    }

    public void setShipDaySunday(boolean newShipDaySunday)
    {
        shipDaySunday = newShipDaySunday;
    }

    public boolean isShipDayMonday()
    {
        return shipDayMonday;
    }

    public void setShipDayMonday(boolean newShipDayMonday)
    {
        shipDayMonday = newShipDayMonday;
    }

    public boolean isShipDayTuesday()
    {
        return shipDayTuesday;
    }

    public void setShipDayTuesday(boolean newShipDayTuesday)
    {
        shipDayTuesday = newShipDayTuesday;
    }

    public boolean isShipDayWednesday()
    {
        return shipDayWednesday;
    }

    public void setShipDayWednesday(boolean newShipDayWednesday)
    {
        shipDayWednesday = newShipDayWednesday;
    }

    public boolean isShipDayThursday()
    {
        return shipDayThursday;
    }

    public void setShipDayThursday(boolean newShipDayThursday)
    {
        shipDayThursday = newShipDayThursday;
    }

    public boolean isShipDayFriday()
    {
        return shipDayFriday;
    }

    public void setShipDayFriday(boolean newShipDayFriday)
    {
        shipDayFriday = newShipDayFriday;
    }

    public boolean isShipDaySaturday()
    {
        return shipDaySaturday;
    }

    public void setShipDaySaturday(boolean newShipDaySaturday)
    {
        shipDaySaturday = newShipDaySaturday;
    }    

    public String getShippingKey()
    {
        return shippingKey;
    }

    public void setShippingKey(String newShippingKey)
    {
        shippingKey = newShippingKey;
    }
   
    public String getBoxId()
    {
        return boxId;
    }
    
    public void setBoxId(String newBoxId)
    {
        boxId = newBoxId;
    }

    public boolean isShipMethodCarrier()
    {
        return shipMethodCarrier;
    }
    
    public void setShipMethodCarrier(boolean newShipMethodCarrier)
    {
        shipMethodCarrier = newShipMethodCarrier;
    }

    public boolean isShipMethodFlorist()
    {
        return shipMethodFlorist;
    }

    public void setShipMethodFlorist(boolean newShipMethodFlorist)
    {
        shipMethodFlorist = newShipMethodFlorist;
    }

    public boolean isCatelogFlag()
    {
        return catelogFlag;
    }

    public void setCatelogFlag(boolean newCatelogFlag)
    {
        catelogFlag = newCatelogFlag;
    }

    public List getSubCodeList()
    {
        return subCodeList;
    }

    public void setSubCodeList(List newSubCodeList)
    {
        subCodeList = newSubCodeList;
    }


    public List getShipMethods()
    {
        return shipMethods;
    }

    public void setShipMethods(List newShipMethods)
    {
        shipMethods = newShipMethods;
    }

    public boolean isBatchMode()
    {
        return batchMode;
    }

    public void setBatchMode(boolean newBatchMode)
    {
        batchMode = newBatchMode;
    }

    public String getHolidaySecondChoice()
    {
        return holidaySecondChoice;
    }

    public void setHolidaySecondChoice(String newHolidaySecondChoice)
    {
        holidaySecondChoice = newHolidaySecondChoice;
    }

    public String getMercurySecondChoice()
    {
        return mercurySecondChoice;
    }

    public void setMercurySecondChoice(String newMercurySecondChoice)
    {
        mercurySecondChoice = newMercurySecondChoice;
    }

    public String getMercuryHolidaySecondChoice()
    {
        return mercuryHolidaySecondChoice;
    }

    public void setMercuryHolidaySecondChoice(String newMercuryHolidaySecondChoice)
    {
        mercuryHolidaySecondChoice = newMercuryHolidaySecondChoice;
    }

    public List getColorsList()
    {
        return colorsList;
    }

    public void setColorsList(List newColorsList)
    {
        colorsList = newColorsList;
    }

    public String getNewSKU()
    {
        return newSKU;
    }

    public void setNewSKU(String newNewSKU)
    {
        newSKU = newNewSKU;
    }

    public float getNewStandardPrice()
    {
        return newStandardPrice;
    }

    public void setNewStandardPrice(float newNewStandardPrice)
    {
        newStandardPrice = newNewStandardPrice;
    }

    public float getNewDeluxePrice()
    {
        return newDeluxePrice;
    }

    public void setNewDeluxePrice(float newNewDeluxePrice)
    {
        newDeluxePrice = newNewDeluxePrice;
    }

    public float getNewPremiumPrice()
    {
        return newPremiumPrice;
    }

    public void setNewPremiumPrice(float newNewPremiumPrice)
    {
        newPremiumPrice = newNewPremiumPrice;
    }

    public String getNewStartDate()
    {
        return newStartDate;
    }

    public void setNewStartDate(String newNewStartDate)
    {
        newStartDate = newNewStartDate;
    }

    public String getNewEndDate()
    {
        return newEndDate;
    }

    public void setNewEndDate(String newNewEndDate)
    {
        newEndDate = newNewEndDate;
    }
    public boolean isAddOnChocolateFlag()
    {
        return addOnChocolateFlag;
    }

    public void setAddOnChocolateFlag(boolean newAddOnChocolateFlag)
    {
        addOnChocolateFlag = newAddOnChocolateFlag;
    }

    // JCPenneyCategory is unused
    public String getJCPenneyCategory()
    {
        return null;
    }

        public String getCrossRefNovatorID()
    {
        return crossRefNovatorID;
    }

    public void setCrossRefNovatorID(String newCrossRefNovatorID)
    {
        crossRefNovatorID = newCrossRefNovatorID;
    }

    public String getGeneralComments()
    {
        return generalComments;
    }

    public void setGeneralComments(String newGeneralComments)
    {
        generalComments = newGeneralComments;
    }

    public Boolean isSentToNovatorProd()
    {
      if(sentToNovatorProd == null)
        return false;
      else
        return sentToNovatorProd;
    }

    public void setSentToNovatorProd(Boolean newSentToNovatorProd)
    {
        sentToNovatorProd = newSentToNovatorProd;
    }

    public Boolean isSentToNovatorUAT()
    {
      if(sentToNovatorUAT == null)
        return false;
      else
        return sentToNovatorUAT;
    }

    public void setSentToNovatorUAT(Boolean newSentToNovatorUAT)
    {
        sentToNovatorUAT = newSentToNovatorUAT;
    }

    public Boolean isSentToNovatorTest()
    {
      if(sentToNovatorTest == null)
        return false;
      else
        return sentToNovatorTest;
    }

    public void setSentToNovatorTest(Boolean newSentToNovatorTest)
    {
        sentToNovatorTest = newSentToNovatorTest;
    }

    public Boolean isSentToNovatorContent()
    {
        if(sentToNovatorContent == null)
          return false;
        else
          return sentToNovatorContent;
    }

    public void setSentToNovatorContent(Boolean newSentToNovatorContent)
    {
        sentToNovatorContent = newSentToNovatorContent;
    }

    public String getDefaultCarrier()
    {
        return defaultCarrier;
    }

    public void setDefaultCarrier(String newDefaultCarrier)
    {
        defaultCarrier = newDefaultCarrier;
    }

    public String[] getCompanyList()
    {
        return companyList;
    }

    public void setCompanyList(String[] newCompanyList)
    {
        companyList = newCompanyList;
    }


    public void setWeboeBlocked(boolean weboeBlocked)
    {
    this.weboeBlocked = weboeBlocked;
    }


    public boolean isWeboeBlocked()
    {
    return weboeBlocked;
    }

    public void setOver21Flag(boolean over21Flag) {
        this.over21Flag = over21Flag;
    }

    public boolean isOver21Flag() {
        return over21Flag;
    }

    public void setCustomFlag(boolean customFlag) {
        this.customFlag = customFlag;
    }
  
    public boolean isCustomFlag() {
        return customFlag;
    }
    
    public void setPersonalGreetingFlag(boolean personalGreetingFlag) {
        this.personalGreetingFlag = personalGreetingFlag;
    }
    
    public boolean isPersonalGreetingFlag() {
        return personalGreetingFlag;
    }

    public void setPremierCollectionFlag(boolean premierCollectionFlag) {
        this.premierCollectionFlag = premierCollectionFlag;
    }
    
    public boolean isPremierCollectionFlag() {
        return premierCollectionFlag;
    }

    public void setZoneJumpEligibleFlag(boolean zoneJumpEligibleFlag) {
        this.zoneJumpEligibleFlag = zoneJumpEligibleFlag;
    }

    public boolean isZoneJumpEligibleFlag() {
        return zoneJumpEligibleFlag;
    }

    public void setShippingSystem(String shippingSystem) {
        this.shippingSystem = shippingSystem;
    }

    public String getShippingSystem() {
        return shippingSystem;
    }

    public void setVendorProductsList(List vendorProductsList) {
        this.vendorProductsList = vendorProductsList;
    }

    public List getVendorProductsList() {
        return vendorProductsList;
    }

    public void setShippingMethodList(String[] shippingMethodList) {
        this.shippingMethodList = shippingMethodList;
    }

    public String[] getShippingMethodList() {
        return shippingMethodList;
    }

    public void setNovatorTagList(String[] novatorTagList) {
        this.novatorTagList = novatorTagList;
    }

    public String[] getNovatorTagList() {
        return novatorTagList;
    }
    
    public void setAllColorsList(List allColorsList) {
        this.allColorsList = allColorsList;
    }

    public List getAllColorsList() {
        return allColorsList;
    }

    public void setArrangementColorsArray(String[] arrangementColorsArray) {
        this.arrangementColorsArray = arrangementColorsArray;
    }

    public String[] getArrangementColorsArray() {
        return arrangementColorsArray;
    }
    
    public void setDepartmentCode(String departmentCode) {
        this.departmentCode = departmentCode;
    }

    public String getDepartmentCode() {
        return departmentCode;
    }


    /**
    * Returns a string representation of the object in XML
    */
    public String toXML() throws PDBApplicationException
    {
        try {
            return toXMLString();
        }
        catch(JAXPException jpe){
            String[] message = {jpe.toString()};
            throw new PDBApplicationException(ProductMaintConstants.OE_CONVERT_VO_TO_XML_EXCEPTION, message);   
        }
    }

   /**
    * Returns a string representation of the object in XML
    */  
    public String toXMLString() throws JAXPException, PDBApplicationException
    {
        logger.debug("ProductVO.toXMLString()");
        String retval = null;
        try {
            retval = JAXPUtil.toStringNoFormat(toXMLDocument());
        } catch (Exception e) {
            String[] message = {e.toString()};
            throw new PDBApplicationException(ProductMaintConstants.OE_CONVERT_VO_TO_XML_EXCEPTION, message);	
        }		
        return retval;
    }

     /**
      * Returns an xml element representation of the object
      */
    public Document toXMLDocument() throws XMLParsingException
    {
        logger.debug("ProductVO.toXMLDocument()");
        Document doc = null;

        try {
            doc = JAXPUtil.createDocument();
                    
            Element root = doc.createElement(PDBXMLTags.TAG_PRODUCT);
            Element child1;
            Element child2;
            Element child3;
            Element child1ship = null;
            Element child2ship;
            Element child3ship;
            Element child1sub = null;
            Element child2sub;
            Element child3sub;
            
            // Available           
                    
            child1 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_AVAILABLE,String.valueOf(this.isStatus()));
            root.appendChild(child1);
            // Contents               
            child1 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_CONTENTS,(String)FTDUtil.convertNulls(this.getDominantFlowers()));
            root.appendChild(child1);
            // Country ID
              child1 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_COUNTRY_ID,(String)FTDUtil.convertNulls(this.getCountry()));
            root.appendChild(child1);
            // UNSPSC Code
              child1 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_UNSPSC_CODE,(String)FTDUtil.convertNulls(this.getUnspscCode()));
            root.appendChild(child1);
            
            if( this.getPersonalizationTemplate() != null && !(this.getPersonalizationTemplate().equalsIgnoreCase("NONE")) ) {
                // Personalization Template
                child1 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_PERSONALIZATION_TEMPLATE,String.valueOf(this.getPersonalizationTemplate()));
                root.appendChild(child1);
                
                // Personalization Template Order
                child1 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_PERSONALIZATION_TEMPLATE_ORDER,String.valueOf(this.getPersonalizationTemplateOrder()));
                root.appendChild(child1);

                // Personalization Lead Days
                child1 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_PERSONALIZATION_LEAD_DAYS,String.valueOf(this.getPersonalizationLeadDays()));
                root.appendChild(child1);         
                
                // pquad/ftdwestAccessoryId
                child1 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_PQUAD_ACCESSORY_ID,String.valueOf(this.getFtdwestAccessoryId()));
                root.appendChild(child1);        
                
                
//                // pquad/ ftdwesttemplate mapping
//                child1 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_PQUAD_TEMPLATE_MAPPING,String.valueOf(this.getFtdwestTemplateMapping()));
//                root.appendChild(child1);

                //personalization_case_flag
                child1 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_PERSONALIZATION_CASE_FLAG,String.valueOf(this.isPersonalizationCaseFlag()));
                root.appendChild(child1);
                
              //all_alpha_flag
                child1 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_ALL_ALPHA_FLAG,String.valueOf(this.isAllAlphaFlag()));
                root.appendChild(child1);
            
            }            
            // Dominant color
            StringBuffer sb = new StringBuffer();
            // If you can figure out a better way to get all the colors, do it//
            if(this.colorsList != null && this.colorsList.size() !=0 )
            {
                List colors = null;
                colors = this.getAllColorsList();
                for (int i = 0; i < colorsList.size(); i++) 
                {
                    int index = colors.indexOf(colorsList.get(i));
                    ColorVO color = (ColorVO)colors.get(index);
                    sb.append(color.getDescription()).append(ProductMaintConstants.NOVATOR_DELIMITER);
                }               
            }        
            child1 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_DOMINANT_COLOUR,(String)FTDUtil.convertNulls(sb.toString()));
            root.appendChild(child1);
            // E-Gift
              child1 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_EGIFT,String.valueOf(this.isEGiftFlag()));
            root.appendChild(child1);
            // Start Avail
            
            String strDate = null;
            Date utilDate = null;
            if(this.exceptionCode!=null && this.exceptionCode.equals(ProductMaintConstants.PRODUCT_AVAILABLE_KEY))
            {
                try{ 
                    utilDate = PDB_FORMAT.parse(getExceptionStartDate());
                    strDate = NOVATOR_FORMAT.format(utilDate);
                    child1 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_START_AVAIL,(String)FTDUtil.convertNulls(strDate));
                } catch (Exception e) {
                    child1 = doc.createElement(PDBXMLTags.TAG_START_AVAIL);    
                }
            }
            else {
                child1 = doc.createElement(PDBXMLTags.TAG_START_AVAIL);
            }
            root.appendChild(child1);
            // End Avail
            if(this.exceptionCode!=null && this.exceptionCode.equals(ProductMaintConstants.PRODUCT_AVAILABLE_KEY))
            {

                try{ 
                    utilDate = PDB_FORMAT.parse(getExceptionEndDate());
                    strDate = NOVATOR_FORMAT.format(utilDate);
                    child1 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_END_AVAIL,(String)FTDUtil.convertNulls(strDate));
                } catch (Exception e) {
                    child1 = doc.createElement(PDBXMLTags.TAG_END_AVAIL);    
                }
            }
            else {
                child1 = doc.createElement(PDBXMLTags.TAG_END_AVAIL);
            }
            root.appendChild(child1);
            // Long description
            child1 = JAXPUtil.buildSimpleXmlNodeCDATA(doc,PDBXMLTags.TAG_LONG_DESCRIPTION,(String)FTDUtil.convertNulls(this.getLongDescription()));
            root.appendChild(child1);
            // Novator Product ID
              child1 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_PRODUCT_ID,(String)FTDUtil.convertNulls(this.getNovatorId()));
            root.appendChild(child1);
            // FTD Product ID
              child1 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_FTD_PRODUCT_ID,(String)FTDUtil.convertNulls(this.getProductId()));
            root.appendChild(child1);
            // Type
              child1 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_TYPE,(String)FTDUtil.convertNulls(this.getProductType()));
            root.appendChild(child1);
            // Sub-type
              child1 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_SUB_TYPE,(String)FTDUtil.convertNulls(this.getProductSubType()));
            root.appendChild(child1);
            // Arrangement size
              child1 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_SIZE,(String)FTDUtil.convertNulls(this.getArrangementSize()));
            root.appendChild(child1);
            // Title
            child1 = JAXPUtil.buildSimpleXmlNodeCDATA(doc,PDBXMLTags.TAG_TITLE,(String)FTDUtil.convertNulls(this.getNovatorName()));
            root.appendChild(child1);
            // Valid ship days - Monday
              child1 = doc.createElement(PDBXMLTags.TAG_VALID_SHIP_DAYS);
            child2 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_DAY,ProductMaintConstants.DAY_MONDAY_KEY);
            child1.appendChild(child2);
            child2 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_VALUE,this.isShipDayMonday());
            child1.appendChild(child2);
            root.appendChild(child1);
            // Valid ship days - Tuesday
              child1 = doc.createElement(PDBXMLTags.TAG_VALID_SHIP_DAYS);
            child2 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_DAY,ProductMaintConstants.DAY_TUESDAY_KEY);
            child1.appendChild(child2);
            child2 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_VALUE,this.isShipDayTuesday());
            child1.appendChild(child2);
            root.appendChild(child1);
            // Valid ship days - Wednesday
              child1 = doc.createElement(PDBXMLTags.TAG_VALID_SHIP_DAYS);
            child2 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_DAY,ProductMaintConstants.DAY_WEDNESDAY_KEY);
            child1.appendChild(child2);
            child2 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_VALUE,this.isShipDayWednesday());
            child1.appendChild(child2);
            root.appendChild(child1);
            // Valid ship days - Thursday
              child1 = doc.createElement(PDBXMLTags.TAG_VALID_SHIP_DAYS);
            child2 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_DAY,ProductMaintConstants.DAY_THURSDAY_KEY);
            child1.appendChild(child2);
            child2 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_VALUE,this.isShipDayThursday());
            child1.appendChild(child2);
            root.appendChild(child1);
            // Valid ship days - Friday
              child1 = doc.createElement(PDBXMLTags.TAG_VALID_SHIP_DAYS);
            child2 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_DAY,ProductMaintConstants.DAY_FRIDAY_KEY);
            child1.appendChild(child2);
            child2 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_VALUE,this.isShipDayFriday());
            child1.appendChild(child2);
            root.appendChild(child1);
            // Valid ship days - Saturday
              child1 = doc.createElement(PDBXMLTags.TAG_VALID_SHIP_DAYS);
            child2 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_DAY,ProductMaintConstants.DAY_SATURDAY_KEY);
            child1.appendChild(child2);
            child2 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_VALUE,this.isShipDaySaturday());
            child1.appendChild(child2);
            root.appendChild(child1);
            // Valid ship days - Sunday
              child1 = doc.createElement(PDBXMLTags.TAG_VALID_SHIP_DAYS);
            child2 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_DAY,ProductMaintConstants.DAY_SUNDAY_KEY);
            child1.appendChild(child2);
            child2 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_VALUE,this.isShipDaySunday());
            child1.appendChild(child2);
            root.appendChild(child1);

            //Vendors
            boolean hasSubcodes = false;
            if( this.subCodeList!=null && subCodeList.size()>0 ) {
                hasSubcodes = true;
            }
            
            child1 = doc.createElement(PDBXMLTags.TAG_VENDORS);
            if( vendorProductsList!=null ) {
                for( int i=0; i<vendorProductsList.size(); i++ ) {
                    VendorProductVO vendorProductVO= (VendorProductVO) vendorProductsList.get(i);
                    child2 = doc.createElement(PDBXMLTags.TAG_VENDOR);
                    child2.setAttribute(PDBXMLTags.TAG_VENDOR_ID, StringUtils.trimToEmpty(vendorProductVO.getVendorId()));
                    child2.setAttribute(PDBXMLTags.TAG_VENDOR_NAME, StringUtils.trimToEmpty(vendorProductVO.getVendorName()));
                    child2.setAttribute(PDBXMLTags.TAG_VENDOR_COST,StringUtils.trimToEmpty(FTDUtil.formatFloat(new Float(vendorProductVO.getVendorCost()), 2, false, false)));
                    child2.setAttribute(PDBXMLTags.TAG_VENDOR_SKU, StringUtils.trimToEmpty(vendorProductVO.getVendorSku()));
                    child2.setAttribute(PDBXMLTags.TAG_AVAILABLE,StringUtils.trimToEmpty(String.valueOf(vendorProductVO.isAvailable())));
                    child2.setAttribute(PDBXMLTags.TAG_REMOVED,StringUtils.trimToEmpty(String.valueOf(vendorProductVO.isRemoved())));
                    if (hasSubcodes){
                        child2.setAttribute(PDBXMLTags.TAG_SUBCODE,StringUtils.trimToEmpty(vendorProductVO.getProductSkuId()));    
                    }
                    child1.appendChild(child2);
                }
            }
            root.appendChild(child1);
            
            // Shipping key
              child1 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_SHIPPING_KEY,(String)FTDUtil.convertNulls(this.getShippingKey()));
            root.appendChild(child1);
            
            // Shipping methods
            child1 = doc.createElement(PDBXMLTags.TAG_SHIPPING_METHODS);
            child1ship = doc.createElement(PDBXMLTags.TAG_SHIPPING_METHODS);
            child2 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_COMMON_CARRIER,this.isShipMethodCarrier());
            child1.appendChild(child2);
            child2 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_FLORIST,this.isShipMethodFlorist());
            child1.appendChild(child2);
            root.appendChild(child1);
            
            List shipMethods;
            shipMethods = this.getShipMethods();
            if(shipMethods != null)
            {
              for(int i = 0; i < shipMethods.size(); i++)
                {
                  DeliveryOptionVO vo = (DeliveryOptionVO)shipMethods.get(i);
            
                  // This for Novator
                  //
                  String method = vo.getShippingMethodId();
                  String field = NovatorTransmission.getNovatorFieldName(method);
                  child1 = JAXPUtil.buildSimpleXmlNode(doc,field,"true");
                  root.appendChild(child1);
                  
                  // This for Batch 
                  //
                  child2ship = doc.createElement(PDBXMLTags.TAG_SHIP_METHOD);
                  // id
                  child3ship = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_SHIP_METHOD_ID,vo.getShippingMethodId());
                  child2ship.appendChild(child3ship);
                  // value
                  child3ship = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_VALUE,"true");
                  child2ship.appendChild(child3ship);
                  // ntag
                  child3ship = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_SHIP_METHOD_NTAG,vo.getNovatorTag());
                  child2ship.appendChild(child3ship);
                  // description
                  child3ship = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_SHIP_METHOD_DESC,vo.getShippingMethodDesc());
                  child2ship.appendChild(child3ship);
                  // default
                  child3ship = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_SHIP_METHOD_DEFAULT,vo.getDefaultCarrier());
                  child2ship.appendChild(child3ship);
                  // 
                  child1ship.appendChild(child2ship);
                }
            }    
            // Add-on flag - Cards
              child1 = doc.createElement(PDBXMLTags.TAG_ADD_ON_FLAG);
            child2 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_TYPE,ProductMaintConstants.ADDON_CARDS_KEY);
            child1.appendChild(child2);
            child2 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_VALUE,this.isAddOnGreetingCardsFlag());
            child1.appendChild(child2);
            root.appendChild(child1);

            // Pricing - Standard price
              child1 = doc.createElement(PDBXMLTags.TAG_PRICING);
            child2 = doc.createElement(PDBXMLTags.TAG_PRICE_ENTITY);
            child3 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_PRODUCT_ID,(String)FTDUtil.convertNulls(this.getNovatorId()));
            child2.appendChild(child3);
            child3 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_PRICE_POINT,(String)FTDUtil.convertNulls(this.getStandardPriceRank()));
            child2.appendChild(child3);
            child3 = doc.createElement(PDBXMLTags.TAG_START_DATE);
            child2.appendChild(child3);
            child3 = doc.createElement(PDBXMLTags.TAG_END_DATE);
            child2.appendChild(child3);
            child3 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_PRICE,FTDUtil.removeFormattingCurrency(FTDUtil.formatFloat(new Float(this.getStandardPrice()), 2, false, false)));
            child2.appendChild(child3);
            child3 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_SORT_ORDER,(String)FTDUtil.convertNulls(this.getStandardPriceRank()));
            child2.appendChild(child3);
            child1.appendChild(child2);
            // Pricing - Deluxe price
            child2 = doc.createElement(PDBXMLTags.TAG_PRICE_ENTITY);
            child3 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_PRODUCT_ID,(String)FTDUtil.convertNulls(this.getNovatorId()));
            child2.appendChild(child3);
            child3 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_PRICE_POINT,(String)FTDUtil.convertNulls(this.getDeluxePriceRank()));
            child2.appendChild(child3);
            child3 = doc.createElement(PDBXMLTags.TAG_START_DATE);
            child2.appendChild(child3);
            child3 = doc.createElement(PDBXMLTags.TAG_END_DATE);
            child2.appendChild(child3);
            child3 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_PRICE,FTDUtil.removeFormattingCurrency(FTDUtil.formatFloat(new Float(this.getDeluxePrice()), 2, false, false)));
            child2.appendChild(child3);
            child3 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_SORT_ORDER,(String)FTDUtil.convertNulls(this.getDeluxePriceRank()));
            child2.appendChild(child3);
            child1.appendChild(child2);
            // Pricing - Premium price
            child2 = doc.createElement(PDBXMLTags.TAG_PRICE_ENTITY);
            child3 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_PRODUCT_ID,(String)FTDUtil.convertNulls(this.getNovatorId()));
            child2.appendChild(child3);
            child3 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_PRICE_POINT,(String)FTDUtil.convertNulls(this.getPremiumPriceRank()));
            child2.appendChild(child3);
            child3 = doc.createElement(PDBXMLTags.TAG_START_DATE);
            child2.appendChild(child3);
            child3 = doc.createElement(PDBXMLTags.TAG_END_DATE);
            child2.appendChild(child3);
            child3 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_PRICE,FTDUtil.removeFormattingCurrency(FTDUtil.formatFloat(new Float(this.getPremiumPrice()), 2, false, false)));
            child2.appendChild(child3);
            child3 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_SORT_ORDER,(String)FTDUtil.convertNulls(this.getPremiumPriceRank()));
            child2.appendChild(child3);
            child1.appendChild(child2);
            // Pricing - Discount allowed
            child2 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_DISCOUNT_ALLOWED,this.isDiscountAllowedFlag());
            child1.appendChild(child2);
            root.appendChild(child1);
            // Details 

            child1 = doc.createElement(PDBXMLTags.TAG_DETAILS);
            List excludedStates = this.getExcludedDeliveryStates();
            if(excludedStates != null)
            {
              StateDeliveryExclusionVO sdeVO;
              for(int i = 0; i < excludedStates.size(); i++)
              {
                sdeVO = (StateDeliveryExclusionVO)excludedStates.get(i);
                child1.appendChild(sdeVO.toXMLDocument(doc).getFirstChild());              }
            }       
            root.appendChild(child1);

            // Options
            child1 = doc.createElement(PDBXMLTags.TAG_OPTIONS);
            child1sub = doc.createElement(PDBXMLTags.TAG_OPTIONS);
            List subCodeList = this.getSubCodeList();
            if(subCodeList != null)
            {
                for(int i = 0; i < subCodeList.size(); i++)
                {
                    ProductSubCodeVO psubVO = (ProductSubCodeVO) subCodeList.get(i);
                    
                    // This for Novator
                    //
                    if(psubVO.isActiveFlag())
                    {
                        child2 = doc.createElement(PDBXMLTags.TAG_OPTION_ENTITY);
                        child3 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_PRODUCT_ID,this.getNovatorId());
                        child2.appendChild(child3);
                        child3 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_SUB_PRODUCT_ID,psubVO.getProductSubCodeId());
                        child2.appendChild(child3);
                        child3 = JAXPUtil.buildSimpleXmlNodeCDATA(doc,PDBXMLTags.TAG_OPTION,psubVO.getSubCodeDescription());
                        child2.appendChild(child3);
                        child3 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_PRICE,FTDUtil.removeFormattingCurrency(FTDUtil.formatFloat(new Float(psubVO.getSubCodePrice()), 2, false, false)));
                        child2.appendChild(child3);
                        child1.appendChild(child2);
                    }
            
                    // This for Batch
                    //
                    child2sub = doc.createElement(PDBXMLTags.TAG_OPTION_ENTITY);
                    child3sub = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_PRODUCT_ID,this.getNovatorId());
                    child2sub.appendChild(child3sub);
                    child3sub = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_SUB_PRODUCT_ID,psubVO.getProductSubCodeId());
                    child2sub.appendChild(child3sub);
                    child3sub = JAXPUtil.buildSimpleXmlNodeCDATA(doc,PDBXMLTags.TAG_OPTION,psubVO.getSubCodeDescription());
                    child2sub.appendChild(child3sub);
                    child3sub = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_PRICE,FTDUtil.removeFormattingCurrency(FTDUtil.formatFloat(new Float(psubVO.getSubCodePrice()), 2, false, false)));
                    child2sub.appendChild(child3sub);
                    child3sub = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_HOLIDAY_PRICE,FTDUtil.removeFormattingCurrency(FTDUtil.formatFloat(new Float(psubVO.getSubCodeHolidayPrice()), 2, false, false)));
                    child2sub.appendChild(child3sub);
                    child3sub = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_SUB_REFNUM,psubVO.getSubCodeRefNumber());
                    child2sub.appendChild(child3sub);
                    child3sub = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_HOLIDAY_SKU,psubVO.getSubCodeHolidaySKU());
                    child2sub.appendChild(child3sub);
                    child3sub = JAXPUtil.buildSimpleXmlNodeCDATA(doc,PDBXMLTags.TAG_DIM_WEIGHT,(String)FTDUtil.convertNulls(psubVO.getDimWeight()));
                    child2sub.appendChild(child3sub);
                    child1sub.appendChild(child2sub);
                }
            }
            root.appendChild(child1);
            // Search priority
            child1 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_SEARCH_PRIORITY,(String)FTDUtil.convertNulls(this.getSearchPriority()));
            root.appendChild(child1);
            // Recipient search
            child1 = doc.createElement(PDBXMLTags.TAG_RECIPIENT_SEARCH);
            String[] recipientList = this.getRecipientSearch();
            if(recipientList != null)
            {
                for(int i = 0; i < recipientList.length; i++)
                 {
                    child2 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_RECIPIENT_SEARCH_ENTITY, recipientList[i]);
                    child1.appendChild(child2);
                 }
            }
            root.appendChild(child1);
            // Keywords
            child1 = doc.createElement(PDBXMLTags.TAG_KEYWORDS);
            if(this.getKeywordSearch() != null)
            {
                StringTokenizer st = new StringTokenizer(this.getKeywordSearch());
                while (st.hasMoreTokens())
                {
                    child2 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_KEY,st.nextToken());
                    child1.appendChild(child2);
                }
            }
            root.appendChild(child1);
            // Corporate site
            child1 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_CORPORATE_SITE,this.isCorporateSiteFlag());
            root.appendChild(child1);
            // Companies
            child1 = doc.createElement(PDBXMLTags.TAG_COMPANIES);
            String[] companyList = this.getCompanyList();
            if(companyList != null)
            {
                for(int i = 0; i < companyList.length; i++)
                 {
                    child2 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_COMPANY, companyList[i]);
                    child1.appendChild(child2);
                 }
            }
            root.appendChild(child1);
            
            // The following are "batch" mode tags (which will be ignored by Novator)
            //
            child1 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_BATCH_MODE,this.isBatchMode()?"true":"false");
            root.appendChild(child1);
            child1 = doc.createElement(PDBXMLTags.TAG_BATCH_MODE_ENTITY);
            // Product name
              child2 = JAXPUtil.buildSimpleXmlNodeCDATA(doc,PDBXMLTags.TAG_PRODUCT_TITLE,(String)FTDUtil.convertNulls(this.getProductName()));
            child1.appendChild(child2);
            // Add shipping methods calculated earlier
            if (child1ship != null) {
              child1.appendChild(child1ship);
            }
            // Add subcode info calculated earlier
            if (child1sub != null) {
              child1.appendChild(child1sub);
            }
            // Category
              child2 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_CATEGORY,(String)FTDUtil.convertNulls(this.getCategory()));
            child1.appendChild(child2);
            // JC Penney Category
              child2 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_JCPENNEY_CATEGORY,(String)FTDUtil.convertNulls(this.getJCPenneyCategory()));
            child1.appendChild(child2);
            // Florist reference number
              child2 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_FLORIST_REFERENCE_NUM,(String)FTDUtil.convertNulls(this.getFloristReferenceNumber()));
            child1.appendChild(child2);
            // Mercury description
              child2 = JAXPUtil.buildSimpleXmlNodeCDATA(doc,PDBXMLTags.TAG_MERCURY_DESCRIPTION,(String)FTDUtil.convertNulls(this.getMercuryDescription()));
            child1.appendChild(child2);
            // Standard Recipe
              child2 = JAXPUtil.buildSimpleXmlNodeCDATA(doc,PDBXMLTags.TAG_RECIPE,(String)FTDUtil.convertNulls(this.getRecipe()));
            child1.appendChild(child2);
            // Send Standard Recipe
              child2 = JAXPUtil.buildSimpleXmlNodeCDATA(doc,PDBXMLTags.TAG_SEND_STANDARD_RECIPE,(String)FTDUtil.convertNulls(this.isSendStandardRecipe()));
            child1.appendChild(child2);
            // Deluxe Recipe
              child2 = JAXPUtil.buildSimpleXmlNodeCDATA(doc,PDBXMLTags.TAG_DELUXE_RECIPE,(String)FTDUtil.convertNulls(this.getDeluxeRecipe()));
            child1.appendChild(child2);
            // Send Deluxe Recipe
              child2 = JAXPUtil.buildSimpleXmlNodeCDATA(doc,PDBXMLTags.TAG_SEND_DELUXE_RECIPE,(String)FTDUtil.convertNulls(this.isSendDeluxeRecipe()));
            child1.appendChild(child2);
            // Premium Recipe
              child2 = JAXPUtil.buildSimpleXmlNodeCDATA(doc,PDBXMLTags.TAG_PREMIUM_RECIPE,(String)FTDUtil.convertNulls(this.getPremiumRecipe()));
            child1.appendChild(child2);
            // Send Premium Recipe
              child2 = JAXPUtil.buildSimpleXmlNodeCDATA(doc,PDBXMLTags.TAG_SEND_PREMIUM_RECIPE,(String)FTDUtil.convertNulls(this.isSendPremiumRecipe()));
            child1.appendChild(child2);
            // Hold-until-available 
              child2 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_HOLD_UNTIL_AVAIL,this.getHoldUntilAvailable());
            child1.appendChild(child2);
            // Tax flag
              child2 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_TAX_FLAG,this.isTaxFlag());
            child1.appendChild(child2);
            // Color size flag
              child2 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_COLOR_SIZE_FLAG,this.isColorSizeFlag());
            child1.appendChild(child2);
            // Item comments
              child2 = JAXPUtil.buildSimpleXmlNodeCDATA(doc,PDBXMLTags.TAG_ITEM_COMMENTS,(String)FTDUtil.convertNulls(this.getItemComments()));
            child1.appendChild(child2);
            // General comments
            String genComments = FTDUtil.replaceAll(this.getGeneralComments(), ProductMaintConstants.END_OF_LINE_CHARACTER, ProductMaintConstants.LINE_BREAK);
              child2 = JAXPUtil.buildSimpleXmlNodeCDATA(doc,PDBXMLTags.TAG_GENERAL_COMMENTS, genComments);
            child1.appendChild(child2);
            // Codify flag
              child2 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_CODIFY_FLAG,(String)FTDUtil.convertNulls(this.getCodifiedFlag()));
            child1.appendChild(child2);
            // Second choice
              child2 = JAXPUtil.buildSimpleXmlNodeCDATA(doc,PDBXMLTags.TAG_SECOND_CHOICE,(String)FTDUtil.convertNulls(this.getSecondChoice()));
            child1.appendChild(child2);
            // Mercury second choice
              child2 = JAXPUtil.buildSimpleXmlNodeCDATA(doc,PDBXMLTags.TAG_MERCURY_SECOND_CHOICE,(String)FTDUtil.convertNulls(this.getMercurySecondChoice()));
            child1.appendChild(child2);
            // Holiday second choice
              child2 = JAXPUtil.buildSimpleXmlNodeCDATA(doc,PDBXMLTags.TAG_HOLIDAY_SECOND_CHOICE,(String)FTDUtil.convertNulls(this.getHolidaySecondChoice()));
            child1.appendChild(child2);
            // Mercury holiday second choice
              child2 = JAXPUtil.buildSimpleXmlNodeCDATA(doc,PDBXMLTags.TAG_MERCURY_HOLIDAY_SECOND_CHOICE,(String)FTDUtil.convertNulls(this.getMercuryHolidaySecondChoice()));
            child1.appendChild(child2);
            // Short description
              child2 = JAXPUtil.buildSimpleXmlNodeCDATA(doc,PDBXMLTags.TAG_SHORT_DESCRIPTION,(String)FTDUtil.convertNulls(this.getShortDescription()));
            child1.appendChild(child2);
            // Dimension / weight
              child2 = JAXPUtil.buildSimpleXmlNodeCDATA(doc,PDBXMLTags.TAG_DIM_WEIGHT,(String)FTDUtil.convertNulls(this.getDimWeight()));
            child1.appendChild(child2);
              child2 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_VARIABLE_PRICING,this.isVariablePricingFlag());
            child1.appendChild(child2);
            // Variable price maximum
              child2 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_VARIABLE_PRICE_MAX,FTDUtil.removeFormattingCurrency(FTDUtil.formatFloat(new Float(this.getVariablePriceMax()), 2, false, false)));
            child1.appendChild(child2);
            // Delivery type
              child2 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_DELIVERY_TYPE,(String)FTDUtil.convertNulls(this.getDeliveryType()));
            child1.appendChild(child2);
            // Availability exception
              child2 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_AVAIL_EXCEPTION,(String)FTDUtil.convertNulls(this.getExceptionCode()));
            child1.appendChild(child2);
            //Always stored in the PDB FORMAT so no need to convert any longer
            child2 = JAXPUtil.buildSimpleXmlNodeCDATA(doc,PDBXMLTags.TAG_START_AVAIL,(String)FTDUtil.convertNulls(getExceptionStartDate()));
            child1.appendChild(child2);
             //Always stored in the PDB FORMAT so no need to convert any longer
            child2 = JAXPUtil.buildSimpleXmlNodeCDATA(doc,PDBXMLTags.TAG_END_AVAIL,(String)FTDUtil.convertNulls(getExceptionEndDate()));
            child1.appendChild(child2);
            // Avail exception message (reason)
            String reason = FTDUtil.replaceAll(this.getExceptionMessage(), ProductMaintConstants.END_OF_LINE_CHARACTER, ProductMaintConstants.LINE_BREAK);
              child2 = JAXPUtil.buildSimpleXmlNodeCDATA(doc,PDBXMLTags.TAG_EXCEPTION_MSG,reason);
            child1.appendChild(child2);
            // Update tags
            child2 = doc.createElement(PDBXMLTags.TAG_UPDATE);
            if( this.isSentToNovatorProd() ) 
            {
                child3 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_SEND_TO,ProductMaintConstants.PDB_NOVATOR_LIVE_KEY);
                child2.appendChild(child3);
            }
            if( this.isSentToNovatorContent() ) 
            {
                child3 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_SEND_TO,ProductMaintConstants.PDB_NOVATOR_CONTENT_KEY);
                child2.appendChild(child3);
            }
            if( this.isSentToNovatorUAT() ) 
            {
                child3 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_SEND_TO,ProductMaintConstants.PDB_NOVATOR_UAT_KEY);
                child2.appendChild(child3);
            }
            if( this.isSentToNovatorTest() ) 
            {
                child3 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_SEND_TO,ProductMaintConstants.PDB_NOVATOR_TEST_KEY);
                child2.appendChild(child3);
            }
            child1.appendChild(child2);
            // Done with batch tags
            root.appendChild(child1);
        
            // WebOE Block
            child1 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_WEBOE_BLOCK,this.isWeboeBlocked());
            root.appendChild(child1);
            
            // Express Shipping Flag
            child1 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_EXPRESS_SHIPPING_ONLY,this.isExpressShippingOnly());
            root.appendChild(child1);
            
            // Over 21 Flag
            child1 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_OVER_21,this.isOver21Flag());
            root.appendChild(child1);
            
            //Shipping System
            child1 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_SHIPPING_SYSTEM,this.getShippingSystem());
            root.appendChild(child1);
            
            // Personal Greeting Flag
            child1 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_PERSONAL_GREETING_FLAG,this.isPersonalGreetingFlag());
            root.appendChild(child1);
            
            // Zone Jump Eligible Flag
            child1 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_ZONE_JUMP_ELIGIBLE_FLAG,this.isZoneJumpEligibleFlag());
            root.appendChild(child1);
            
            //Box Type
            child1 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_BOX_ID,this.getBoxId());
            root.appendChild(child1);

            child1 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_SUPPLY_EXPENSE,FTDUtil.removeFormattingCurrency(FTDUtil.formatFloat(new Float(this.getSupplyExpense()), 2, false, false)));
            root.appendChild(child1);

            //
            child1 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_SUPPLY_EXPENSE_EFF_DATE,this.getSupplyExpenseEffDate());
            root.appendChild(child1);

            //
            child1 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_ROYALTY_PERCENT,FTDUtil.removeFormattingCurrency(FTDUtil.formatFloat(new Float(this.getRoyaltyPercent()), 0, false, false)));
            root.appendChild(child1);

            //
            child1 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_ROYALTY_PERCENT_EFF_DATE,this.getRoyaltyPercentEffDate());
            root.appendChild(child1);

            child1 = doc.createElement(PDBXMLTags.TAG_GBB);
            if (this.getGbbPopoverFlag()) {
                child2 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_GBB_POPOVER_FLAG, "1");
            } else {
                child2 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_GBB_POPOVER_FLAG, "");
            }
            child1.appendChild(child2);

            String gbbTitle = this.getGbbTitle();
            if (gbbTitle == null || gbbTitle.equals("")) {
                    ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
                    gbbTitle = configUtil.getFrpGlobalParmNoNull(ProductMaintConstants.PDB_CONFIG_CONTEXT, ProductMaintConstants.PDB_GBB_DEFAULT_TITLE);
                    logger.debug("Substituting default GBB title: " + gbbTitle);
            }
            child2 = JAXPUtil.buildSimpleXmlNodeCDATA(doc,PDBXMLTags.TAG_GBB_TITLE, gbbTitle);
            child1.appendChild(child2);

            child2 = doc.createElement(PDBXMLTags.TAG_GBB_POSITION_1);
            child3 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_GBB_SKU, "");
            child2.appendChild(child3);
            if (this.getGbbNameOverrideFlag1()) {
                child3 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_GBB_NAME_OVERRIDE_FLAG, "1");
            } else {
                child3 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_GBB_NAME_OVERRIDE_FLAG, "");
            }
            child2.appendChild(child3);
            child3 = JAXPUtil.buildSimpleXmlNodeCDATA(doc,PDBXMLTags.TAG_GBB_NAME_OVERRIDE_TEXT, this.getGbbNameOverrideText1());
            child2.appendChild(child3);
            if (this.getGbbPriceOverrideFlag1()) {
                child3 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_GBB_PRICE_OVERRIDE_FLAG, "1");
            } else {
                child3 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_GBB_PRICE_OVERRIDE_FLAG, "");
            }
            child2.appendChild(child3);
            child3 = JAXPUtil.buildSimpleXmlNodeCDATA(doc,PDBXMLTags.TAG_GBB_PRICE_OVERRIDE_TEXT, this.getGbbPriceOverrideText1());
            child2.appendChild(child3);
            child1.appendChild(child2);

            child2 = doc.createElement(PDBXMLTags.TAG_GBB_POSITION_2);
            child3 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_GBB_SKU, "");
            child2.appendChild(child3);
            if (this.getGbbNameOverrideFlag2()) {
                child3 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_GBB_NAME_OVERRIDE_FLAG, "1");
            } else {
                child3 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_GBB_NAME_OVERRIDE_FLAG, "");
            }
            child2.appendChild(child3);
            child3 = JAXPUtil.buildSimpleXmlNodeCDATA(doc,PDBXMLTags.TAG_GBB_NAME_OVERRIDE_TEXT, this.getGbbNameOverrideText2());
            child2.appendChild(child3);
            if (this.getGbbPriceOverrideFlag2()) {
                child3 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_GBB_PRICE_OVERRIDE_FLAG, "1");
            } else {
                child3 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_GBB_PRICE_OVERRIDE_FLAG, "");
            }
            child2.appendChild(child3);
            child3 = JAXPUtil.buildSimpleXmlNodeCDATA(doc,PDBXMLTags.TAG_GBB_PRICE_OVERRIDE_TEXT, this.getGbbPriceOverrideText2());
            child2.appendChild(child3);
            child1.appendChild(child2);

            child2 = doc.createElement(PDBXMLTags.TAG_GBB_POSITION_3);
            child3 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_GBB_SKU, "");
            child2.appendChild(child3);
            if (this.getGbbNameOverrideFlag3()) {
                child3 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_GBB_NAME_OVERRIDE_FLAG, "1");
            } else {
                child3 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_GBB_NAME_OVERRIDE_FLAG, "");
            }
            child2.appendChild(child3);
            child3 = JAXPUtil.buildSimpleXmlNodeCDATA(doc,PDBXMLTags.TAG_GBB_NAME_OVERRIDE_TEXT, this.getGbbNameOverrideText3());
            child2.appendChild(child3);
            if (this.getGbbPriceOverrideFlag3()) {
                child3 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_GBB_PRICE_OVERRIDE_FLAG, "1");
            } else {
                child3 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_GBB_PRICE_OVERRIDE_FLAG, "");
            }
            child2.appendChild(child3);
            child3 = JAXPUtil.buildSimpleXmlNodeCDATA(doc,PDBXMLTags.TAG_GBB_PRICE_OVERRIDE_TEXT, this.getGbbPriceOverrideText3());
            child2.appendChild(child3);
            child1.appendChild(child2);

            root.appendChild(child1);    

            // Website source codes
            child1 = doc.createElement(PDBXMLTags.TAG_WEBSITES);
            if(this.getWebsites() != null)
            {
                StringTokenizer st = new StringTokenizer(this.getWebsites());
                while (st.hasMoreTokens())
                {
                    child2 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_WEBSITE,st.nextToken());
                    child1.appendChild(child2);
                }
            }
            root.appendChild(child1);

            // Free-Shipping Duration (toXML())
            child1 = JAXPUtil.buildSimpleXmlNode(doc, PDBXMLTags.TAG_SERVICE_DURATION, this.getDuration());
            root.appendChild(child1);
            
            // Free-Shipping Allow Free Shipping (toXML())
            child1 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_ALLOW_FREE_SHIPPING,this.isAllowFreeShippingFlag());
            root.appendChild(child1);

            //addons
            child1 = doc.createElement(PDBXMLTags.TAG_ADD_ONS);

            if (this.getProductAddonMap() != null) 
            {
                for (String key: this.getProductAddonMap().keySet())
                {
                    for (ProductAddonVO productAddon: this.getProductAddonMap().get(key))
                    {
                        if (productAddon != null && productAddon.getAddonTypeIncludedInProductFeedFlag() && productAddon.getActiveFlag())
                        {    
                            child2 = doc.createElement(PDBXMLTags.TAG_ADD_ON);
                            child3 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_ADD_ON_ID, productAddon.getAddonId());
                            child2.appendChild(child3);
                            child3 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_DISPLAY_ORDER, productAddon.getDisplaySequenceNumber() != null ? productAddon.getDisplaySequenceNumber().toString() : "");
                            child2.appendChild(child3);
                            child3 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_MAX_QUANTITY, productAddon.getMaxQuantity() != null ? productAddon.getMaxQuantity().toString() : "");
                            child2.appendChild(child3);
                            child3 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_ADD_ON_TYPE_ID, productAddon.getAddonTypeId());
                            child2.appendChild(child3);
                            child1.appendChild(child2);
                        }
                    } 
                }
            }
            root.appendChild(child1);
            
            // Morning Delivery Flag (toXML())
			child1 = JAXPUtil.buildSimpleXmlNode(doc, PDBXMLTags.TAG_MORNING_DELIVERY_FLAG, this.isMorningDeliveryFlag());
			root.appendChild(child1);
			
			
			child1 = JAXPUtil.buildSimpleXmlNode(doc, PDBXMLTags.TAG_BULLET_DESCRIPTION, this.getBulletDescription());
			root.appendChild(child1);
			
			
            doc.appendChild(root);

            logger.debug(JAXPUtil.toString(doc));
        } catch ( Exception e ) {
            throw new XMLParsingException(e);
        }
        
        return doc;
      }

    /**
     * Set the object to the values in the xml document.
     * There is no validation done on the values in the document
     * since the document is internally created by the app.  If
     * the use of this fuction expands outside of this assumption, then
     * validation routines will have to be added.
     * 
     * @param doc XML document representation of a value object 
     */
     public void copy(Document doc) throws PDBApplicationException
     {
         logger.debug("ProductVO.copy()");
         try 
         {    
             logger.debug(JAXPUtil.toString(doc));

             Element root = (Element)doc.getFirstChild();
             Element child1;
             Element child2;
             Element child3;
             Element child4;
             
             if( root==null || !root.getTagName().equals(PDBXMLTags.TAG_PRODUCT) ) 
             {
                 String[] args = new String[1];
                 args[0] = "Cannot create productVO from xml document " + root.getTagName();
                 logger.error(args[0]);
                 throw new PDBApplicationException(ProductMaintConstants.PRODUCT_XML_DOCUMENT_INVALID, args);        
             }
             
             // Available
             child1 = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_AVAILABLE);
             setStatus(BooleanUtils.toBoolean(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child1))));
             // Contents         
             child1 = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_CONTENTS);
             setDominantFlowers(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child1)));
             
             // Country ID
             child1 = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_COUNTRY_ID);
             setCountry(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child1)));
             
             // UNSPSC Code
             child1 = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_UNSPSC_CODE);
             this.setUnspscCode(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child1)));
    
             if( this.getPersonalizationTemplate() != null && !(this.getPersonalizationTemplate().equalsIgnoreCase("NONE")) ) {
                 // Personalization Template
                 child1 = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_PERSONALIZATION_TEMPLATE);
                 this.setPersonalizationTemplate(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child1)));
                 
                 // Personalization Template
                 child1 = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_PERSONALIZATION_TEMPLATE_ORDER);
                 this.setPersonalizationTemplateOrder(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child1)));

                 // Personalization Template
                 child1 = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_PERSONALIZATION_LEAD_DAYS);
                 this.setPersonalizationLeadDays(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child1)));         
                 
              // pquad accessory id
                 child1 = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_PQUAD_ACCESSORY_ID);
                 this.setFtdwestAccessoryId(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child1)));
                 
//              // pquad template mapping
//                 child1 = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_PQUAD_TEMPLATE_MAPPING);
//                 this.setFtdwestTemplateMapping(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child1)));
                 
                 //personalization_case_flag                 
                 child1 = JAXPUtil.selectSingleNode(root,FeedXMLTags.TAG_PERSONALIZATION_CASE_FLAG);
                 this.setPersonalizationCaseFlag(FTDUtil.convertStringToBoolean(JAXPUtil.getTextValue(child1))); 
                 
                 //personalization_case_flag                 
                 child1 = JAXPUtil.selectSingleNode(root,FeedXMLTags.TAG_ALL_ALPHA_FLAG);
                 this.setAllAlphaFlag(FTDUtil.convertStringToBoolean(JAXPUtil.getTextValue(child1))); 
                 
             }
             
             
             // Dominant color
             child1 = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_DOMINANT_COLOUR);
             String strTmp = StringUtils.trimToEmpty(JAXPUtil.getTextValue(child1));
             String[] strColors = strTmp.split(ProductMaintConstants.NOVATOR_DELIMITER);
             List colors = null;
            // if you can figure out a better way to get the colors, do it//
             colors = this.getAllColorsList();
             if (colorsList == null) 
             {
               colorsList = new ArrayList();
             } else {
               colorsList.clear();
             }
             for (int i = 0; i < strColors.length; i++)
             {
                 String strColor=strColors[i];
                 if( strColor!=null ) 
                 {
                     for( int j = 0; j < colors.size() ; j++ )
                     {
                         ColorVO color = (ColorVO)colors.get(j);
                         if( strColor.equalsIgnoreCase(color.getDescription()) ) 
                         {
                             colorsList.add(color);
                             break;
                         }
                     }
                 }       
             }  

             //E-Gift
             child1 = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_EGIFT);
             setEGiftFlag(BooleanUtils.toBoolean(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child1))));
         
             // Long description
             child1 = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_LONG_DESCRIPTION);
             setLongDescription(StringUtils.trimToEmpty(((Element)child1).getTextContent()));
             
             // FTD Product ID
             child1 = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_FTD_PRODUCT_ID);
             this.setProductId(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child1)));
         
             // Novator Product ID
             child1 = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_PRODUCT_ID);
             this.setNovatorId(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child1)));
             
             // Type
             child1 = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_TYPE);
             this.setProductType(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child1)));
         
             // Sub-type
             child1 = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_SUB_TYPE);
             this.setProductSubType(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child1)));
         
             // Arrangement size
             child1 = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_SIZE);
             this.setArrangementSize(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child1)));
             
             // Title
             child1 = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_TITLE);
             this.setNovatorName(StringUtils.trimToEmpty(((Element)child1).getTextContent()));

             // Valid ship days 
             NodeList nodes = JAXPUtil.selectNodes(root,PDBXMLTags.TAG_VALID_SHIP_DAYS);
             setShipDayMonday(false);
             setShipDayTuesday(false);
             setShipDayWednesday(false);
             setShipDayThursday(false);
             setShipDayFriday(false);
             setShipDaySaturday(false);
             setShipDayFriday(false);
             setShipDaySaturday(false);
             setShipDaySunday(false);
             for( int idx=0; idx<nodes.getLength(); idx++ )
             {
                 child1 = (Element)nodes.item(idx);
                 child2 = JAXPUtil.selectSingleNode(child1,PDBXMLTags.TAG_DAY);
                 child3 = JAXPUtil.selectSingleNode(child1,PDBXMLTags.TAG_VALUE);
                 if( child2!=null ) 
                 {
                     String strDay = StringUtils.trimToEmpty(JAXPUtil.getTextValue(child2));
                     if(ProductMaintConstants.DAY_MONDAY_KEY.equalsIgnoreCase(strDay) ) 
                     {
                         this.setShipDayMonday(BooleanUtils.toBoolean(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child3))));    
                     } 
                     else if(ProductMaintConstants.DAY_TUESDAY_KEY.equalsIgnoreCase(strDay) ) 
                     {
                         this.setShipDayTuesday(BooleanUtils.toBoolean(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child3))));
                     }
                     else if(ProductMaintConstants.DAY_WEDNESDAY_KEY.equalsIgnoreCase(strDay) ) 
                     {
                         this.setShipDayWednesday(BooleanUtils.toBoolean(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child3))));
                     }
                     else if(ProductMaintConstants.DAY_THURSDAY_KEY.equalsIgnoreCase(strDay) ) 
                     {
                         this.setShipDayThursday(BooleanUtils.toBoolean(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child3))));
                     }
                     else if(ProductMaintConstants.DAY_FRIDAY_KEY.equalsIgnoreCase(strDay) ) 
                     {
                         this.setShipDayFriday(BooleanUtils.toBoolean(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child3))));
                     }
                     else if(ProductMaintConstants.DAY_SATURDAY_KEY.equalsIgnoreCase(strDay) ) 
                     {
                         this.setShipDaySaturday(BooleanUtils.toBoolean(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child3))));
                     }
                     else if(ProductMaintConstants.DAY_SUNDAY_KEY.equalsIgnoreCase(strDay) ) 
                     {
                         this.setShipDaySunday(BooleanUtils.toBoolean(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child3))));
                     }
                 }
             }
             
             // Vendors
             ArrayList vendorProducts = new ArrayList();
             VendorProductVO vpVO;
             
             Element vendors = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_VENDORS);
             NodeList vendor = JAXPUtil.selectNodes(vendors ,PDBXMLTags.TAG_VENDOR);
             for( int idx = 0; idx < vendor.getLength(); idx++) 
             {
                 child1 = (Element)vendor.item(idx);
                 vpVO = new VendorProductVO();
                 vpVO.setVendorId(child1.getAttribute(PDBXMLTags.TAG_VENDOR_ID));
                 vpVO.setAvailable(BooleanUtils.toBoolean(child1.getAttribute(PDBXMLTags.TAG_AVAILABLE)));
                 String vendor_subcode = child1.getAttribute(PDBXMLTags.TAG_SUBCODE);
                 if( StringUtils.isEmpty(vendor_subcode) ) {
                     vendor_subcode = productId;
                 }
                 vpVO.setProductSkuId(vendor_subcode);
                 vpVO.setRemoved(false);
                 vpVO.setVendorCost(new Float(child1.getAttribute(PDBXMLTags.TAG_VENDOR_COST)));
                 vpVO.setVendorName(child1.getAttribute(PDBXMLTags.TAG_VENDOR_NAME));
                 vpVO.setVendorSku(child1.getAttribute(PDBXMLTags.TAG_VENDOR_SKU));
                 vendorProducts.add(vpVO);
             }
             this.setVendorProductsList(vendorProducts);
             
             // Shipping key
             child1 = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_SHIPPING_KEY);
             this.setShippingKey(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child1)));
             
             //Shipping Methods
             child1 = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_SHIPPING_METHODS);
             child2 = JAXPUtil.selectSingleNode(child1,PDBXMLTags.TAG_COMMON_CARRIER);
             this.setShipMethodCarrier(BooleanUtils.toBoolean(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child2))));
             child2 = JAXPUtil.selectSingleNode(child1,PDBXMLTags.TAG_FLORIST);
             this.setShipMethodFlorist(BooleanUtils.toBoolean(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child2))));
             
             //Add Ons
             nodes = JAXPUtil.selectNodes(root,PDBXMLTags.TAG_ADD_ON_FLAG);
             for( int idx = 0; idx < nodes.getLength(); idx++) 
             {
                 child1 = (Element)nodes.item(idx);
                 child2 = JAXPUtil.selectSingleNode(child1,PDBXMLTags.TAG_TYPE);
                 child3 = JAXPUtil.selectSingleNode(child1,PDBXMLTags.TAG_VALUE);
                     
                 if( PDBXMLTags.TAG_ADD_ON_CARD.equals(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child2))) ) 
                 {
                     this.setAddOnGreetingCardsFlag(BooleanUtils.toBoolean(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child3))));
                 }
                 else if( PDBXMLTags.TAG_ADD_ON_FUNERAL.equals(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child2)))) 
                 {
                     this.setAddOnFuneralFlag(BooleanUtils.toBoolean(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child3))));
                 }
             }
             
             
             // Pricing 
             child1 = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_PRICING);
             if( child1!=null ) 
             {
                 child2 = JAXPUtil.selectSingleNode(child1,PDBXMLTags.TAG_DISCOUNT_ALLOWED);
                 this.setDiscountAllowedFlag(BooleanUtils.toBoolean(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child2))));
                 
                 nodes = JAXPUtil.selectNodes(child1,PDBXMLTags.TAG_PRICE_ENTITY);
                 for( int idx = 0; idx < nodes.getLength(); idx++) 
                 {
                     child3 = (Element)nodes.item(idx);
                     String priceValue = StringUtils.trimToEmpty(((Element)JAXPUtil.selectSingleNode(child3,PDBXMLTags.TAG_PRICE)).getTextContent());
                     // LAURA
                     String pricePointValue = StringUtils.trimToEmpty(((Element)JAXPUtil.selectSingleNode(child3,PDBXMLTags.TAG_PRICE_POINT)).getTextContent());
                     Float price=new Float(0.00);
                     if (priceValue != null && !priceValue.equalsIgnoreCase("")) {
                         price = new Float(priceValue);
                     }
                     if( price.floatValue() >0 ) 
                     {
                         if( pricePointValue.equals("3") )
                         {
                             this.setPremiumPrice(price.floatValue());
                             this.setPremiumPriceRank("3");
                         }
                         else if( pricePointValue.equals("2") )
                         {
                             this.setDeluxePrice(price.floatValue());
                             this.setDeluxePriceRank("2");
                         }
                         else if( pricePointValue.equals("1") )
                         {
                             this.setStandardPrice(price.floatValue());
                             this.setStandardPriceRank("1");
                         }
                     }
                 }
             }
             
             //Details
             child1 = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_DETAILS);
             if( child1!=null ) 
             {
                 ArrayList exStates = new ArrayList();
                 StateDeliveryExclusionVO sdeVO;
                 nodes = JAXPUtil.selectNodes(child1,PDBXMLTags.TAG_DETAIL_ENTITY);
                 for( int idx = 0; idx < nodes.getLength(); idx++) 
                 {
                     child2 = (Element)nodes.item(idx);
                     if( child2!=null )
                     {
                         sdeVO = new StateDeliveryExclusionVO();
                         child3 = JAXPUtil.selectSingleNode(child2,PDBXMLTags.TAG_KEY);
                         if( child3!=null && PDBXMLTags.TAG_EXCLUDED_STATES.equals(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child3))) ) 
                         {
                             child3 = JAXPUtil.selectSingleNode(child2,PDBXMLTags.TAG_VALUE);
                             if( child3!=null ) 
                             {
                                 sdeVO.setExcludedState(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child3)));
                             }
                             child3 = JAXPUtil.selectSingleNode(child2,PDBXMLTags.TAG_TITLE);
                             if( child3!=null ) 
                             {
                                 sdeVO.setStateName(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child3)));
                             }
                         }
                         
                         child3 = JAXPUtil.selectSingleNode(child2,PDBXMLTags.TAG_DAYS);
                         if( child3!=null )
                         {
                             NodeList nodeset2 = JAXPUtil.selectNodes(child3,PDBXMLTags.TAG_DAY);
                             for( int idx2=0; idx2<nodeset2.getLength(); idx2++ ) 
                             {
                                 child4 = (Element)nodeset2.item(idx2);
                                 if( child4!=null )
                                 {
                                     String strTest = StringUtils.trimToEmpty(JAXPUtil.getTextValue(child4));
                                     if( PDBXMLTags.TAG_DAY_SAT.equals(strTest) )
                                     {
                                         sdeVO.setSatExcluded(true);
                                     }
                                     if( PDBXMLTags.TAG_DAY_SUN.equals(strTest) )
                                     {
                                         sdeVO.setSunExcluded(true);
                                     }
                                     if( PDBXMLTags.TAG_DAY_MON.equals(strTest) )
                                     {
                                         sdeVO.setMonExcluded(true);
                                     }
                                     if( PDBXMLTags.TAG_DAY_TUE.equals(strTest) )
                                     {
                                         sdeVO.setTueExcluded(true);
                                     }
                                     if( PDBXMLTags.TAG_DAY_WED.equals(strTest) )
                                     {
                                         sdeVO.setWedExcluded(true);
                                     }
                                     if( PDBXMLTags.TAG_DAY_THU.equals(strTest) )
                                     {
                                         sdeVO.setThuExcluded(true);
                                     }
                                     if( PDBXMLTags.TAG_DAY_FRI.equals(strTest) )
                                     {
                                         sdeVO.setFriExcluded(true);
                                     }
                                 }
                             }
                         }
                     
                         exStates.add(sdeVO);
                     }
                 }
                 this.setExcludedDeliveryStates(exStates);
             }
                         
             // Search priority
             child1 = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_SEARCH_PRIORITY);
             if( child1!=null )
             {
                 this.setSearchPriority(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child1)));
             }
             
             // Recipient search
             child1 = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_RECIPIENT_SEARCH);
             if( child1!=null ) 
             {
                 nodes = JAXPUtil.selectNodes(child1,PDBXMLTags.TAG_RECIPIENT_SEARCH_ENTITY);
                 String[] recipientList = new String[nodes.getLength()];
                 for( int idx=0; idx < nodes.getLength(); idx++) 
                 {
                     child2 = (Element)nodes.item(idx);
                     recipientList[idx]=StringUtils.trimToEmpty(JAXPUtil.getTextValue(child2));
                 }
                 this.setRecipientSearch(recipientList);
             }
             
             //Keywords
             child1 = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_KEYWORDS);
             if( child1!=null ) 
             {
                 nodes = JAXPUtil.selectNodes(child1,PDBXMLTags.TAG_KEY);
                 StringBuffer sb = new StringBuffer();
                 
                 for( int idx=0; idx < nodes.getLength(); idx++) 
                 {
                     child2 = (Element)nodes.item(idx);
                     sb.append(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child2)));
                     sb.append(" ");
                 }
                 this.setKeywordSearch(sb.toString());
             }
             
             // Corporate site
             child1 = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_CORPORATE_SITE);
             this.setCorporateSiteFlag(BooleanUtils.toBoolean(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child1))));
             
             // Companies
             child1 = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_COMPANIES);
             if( child1!=null ) 
             {
                 nodes = JAXPUtil.selectNodes(child1,PDBXMLTags.TAG_COMPANY);
                 String[] companyList = new String[nodes.getLength()];
                 
                 for( int idx=0; idx<nodes.getLength(); idx++) 
                 {
                     child2 = (Element)nodes.item(idx);
                     companyList[idx]=StringUtils.trimToEmpty(JAXPUtil.getTextValue(child2));
                 }
                 this.setCompanyList(companyList);
             }
                         
             // Batch mode flag LAURA
             child1 = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_BATCH_MODE);
             this.setBatchMode(BooleanUtils.toBoolean(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child1))));
             
             // Batch mode specific tags
             //
             child1 = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_BATCH_MODE_ENTITY);
             // Product name
             child2 = JAXPUtil.selectSingleNode(child1,PDBXMLTags.TAG_PRODUCT_TITLE);
             this.setProductName(StringUtils.trimToEmpty((((Element)child2).getTextContent())));

             // Batch Shipping Methods 
             child2 = JAXPUtil.selectSingleNode(child1,PDBXMLTags.TAG_SHIPPING_METHODS);
             shipMethods = new ArrayList();
             DeliveryOptionVO dovo;
             nodes = JAXPUtil.selectNodes(child2,PDBXMLTags.TAG_SHIP_METHOD);
             for( int idx=0; idx<nodes.getLength(); idx++) 
             {
                 child3 = (Element)nodes.item(idx);
                 child4 = JAXPUtil.selectSingleNode(child3,PDBXMLTags.TAG_VALUE);
                 if(BooleanUtils.toBoolean(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child4))))
                 {
                     dovo = new DeliveryOptionVO();
                     dovo.setSelected(true);
                     child4 = JAXPUtil.selectSingleNode(child3,PDBXMLTags.TAG_SHIP_METHOD_DEFAULT);
                     if( child4!=null ) 
                     {
                         dovo.setDefaultCarrier(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child4)));
                     }
                     
                     child4 = JAXPUtil.selectSingleNode(child3,PDBXMLTags.TAG_SHIP_METHOD_ID);
                     if( child4!=null ) 
                     {
                         dovo.setShippingMethodId(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child4)));
                     }
                     
                     child4 = JAXPUtil.selectSingleNode(child3,PDBXMLTags.TAG_SHIP_METHOD_NTAG);
                     if( child4!=null ) 
                     {
                         dovo.setNovatorTag(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child4)));
                     }
                     
                     child4 = JAXPUtil.selectSingleNode(child3,PDBXMLTags.TAG_SHIP_METHOD_DESC);
                     if( child4!=null ) 
                     {
                         dovo.setShippingMethodDesc(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child4)));
                     }
                     
                     getShipMethods().add(dovo);
                 }
             }
             // Category
             child2 = JAXPUtil.selectSingleNode(child1,PDBXMLTags.TAG_CATEGORY);
             this.setCategory(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child2)));
             // Florist reference number
             child2 = JAXPUtil.selectSingleNode(child1,PDBXMLTags.TAG_FLORIST_REFERENCE_NUM);
             this.setFloristReferenceNumber(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child2)));
             // Mercury description
             child2 = JAXPUtil.selectSingleNode(child1,PDBXMLTags.TAG_MERCURY_DESCRIPTION);
             this.setMercuryDescription(StringUtils.trimToEmpty((((Element)child2).getTextContent())));
             // Standard Recipe
             child2 = JAXPUtil.selectSingleNode(child1,PDBXMLTags.TAG_RECIPE);
             this.setRecipe(StringUtils.trimToEmpty((((Element)child2).getTextContent())));
             // Send Standard Recipe
             child2 = JAXPUtil.selectSingleNode(child1,PDBXMLTags.TAG_SEND_STANDARD_RECIPE);
             this.setSendStandardRecipe(BooleanUtils.toBoolean(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child2))));
             // Deluxe Recipe
             child2 = JAXPUtil.selectSingleNode(child1,PDBXMLTags.TAG_DELUXE_RECIPE);
             this.setDeluxeRecipe(StringUtils.trimToEmpty((((Element)child2).getTextContent())));
             // Send Deluxe Recipe
             child2 = JAXPUtil.selectSingleNode(child1,PDBXMLTags.TAG_SEND_DELUXE_RECIPE);
             this.setSendDeluxeRecipe(BooleanUtils.toBoolean(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child2))));
             // Premium Recipe
             child2 = JAXPUtil.selectSingleNode(child1,PDBXMLTags.TAG_PREMIUM_RECIPE);
             this.setPremiumRecipe(StringUtils.trimToEmpty((((Element)child2).getTextContent())));
             // Send Premium Recipe
             child2 = JAXPUtil.selectSingleNode(child1,PDBXMLTags.TAG_SEND_PREMIUM_RECIPE);
             this.setSendPremiumRecipe(BooleanUtils.toBoolean(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child2))));
             // Hold-until-available 
             child2 = JAXPUtil.selectSingleNode(child1,PDBXMLTags.TAG_HOLD_UNTIL_AVAIL);
             this.setHoldUntilAvailable(BooleanUtils.toBoolean(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child2))));
             // Tax flag
             child2 = JAXPUtil.selectSingleNode(child1,PDBXMLTags.TAG_TAX_FLAG);
             this.setTaxFlag(BooleanUtils.toBoolean(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child2))));
             // Color size flag
             child2 = JAXPUtil.selectSingleNode(child1,PDBXMLTags.TAG_COLOR_SIZE_FLAG);
             this.setColorSizeFlag(BooleanUtils.toBoolean(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child2))));
             // Item comments
             child2 = JAXPUtil.selectSingleNode(child1,PDBXMLTags.TAG_ITEM_COMMENTS);
             this.setItemComments(StringUtils.trimToEmpty((((Element)child2).getTextContent())));
             // General comments
             child2 = JAXPUtil.selectSingleNode(child1,PDBXMLTags.TAG_GENERAL_COMMENTS);
             // Convert <BR>'s back to end of line characters
             String genComments = FTDUtil.replaceAll(StringUtils.trimToEmpty((((Element)child2).getTextContent())), ProductMaintConstants.LINE_BREAK, ProductMaintConstants.END_OF_LINE_CHARACTER);
             this.setGeneralComments(genComments);
             // Codify flag
             child2 = JAXPUtil.selectSingleNode(child1,PDBXMLTags.TAG_CODIFY_FLAG);
             this.setCodifiedFlag(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child2)));
             // Second choice
             child2 = JAXPUtil.selectSingleNode(child1,PDBXMLTags.TAG_SECOND_CHOICE);
             this.setSecondChoice(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child2)));
             // Mercury second choice
             child2 = JAXPUtil.selectSingleNode(child1,PDBXMLTags.TAG_MERCURY_SECOND_CHOICE);
             this.setMercurySecondChoice(StringUtils.trimToEmpty(((Element)child2).getTextContent()));
             // Holiday second choice
             child2 = JAXPUtil.selectSingleNode(child1,PDBXMLTags.TAG_HOLIDAY_SECOND_CHOICE);
             this.setHolidaySecondChoice(StringUtils.trimToEmpty(((Element)child2).getTextContent()));
             // Mercury holiday second choice
             child2 = JAXPUtil.selectSingleNode(child1,PDBXMLTags.TAG_MERCURY_HOLIDAY_SECOND_CHOICE);
             this.setMercuryHolidaySecondChoice(StringUtils.trimToEmpty(((Element)child2).getTextContent()));
             // Dimension / weight
             child2 = JAXPUtil.selectSingleNode(child1,PDBXMLTags.TAG_DIM_WEIGHT);
             this.setDimWeight(StringUtils.trimToEmpty((((Element)child2).getTextContent())));
             // Variable pricing flag
             child2 = JAXPUtil.selectSingleNode(child1,PDBXMLTags.TAG_VARIABLE_PRICING);
             this.setVariablePricingFlag(BooleanUtils.toBoolean(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child2))));
             // Variable price maximum
             child2 = JAXPUtil.selectSingleNode(child1,PDBXMLTags.TAG_VARIABLE_PRICE_MAX);
             if( child2!=null ) {
                 String strTest = StringUtils.trimToEmpty(JAXPUtil.getTextValue(child2));
                 try {
                     if( strTest!=null && strTest.length()>0 ) {
                         Float price = Float.valueOf(strTest);
                         this.setVariablePriceMax(price.floatValue());
                     }    
                 } catch (NumberFormatException e) {logger.error(e);}
             }
             // Availability exception
             child2 = JAXPUtil.selectSingleNode(child1,PDBXMLTags.TAG_AVAIL_EXCEPTION);
             this.setExceptionCode(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child2)));
             // Delivery type
             child2 = JAXPUtil.selectSingleNode(child1,PDBXMLTags.TAG_DELIVERY_TYPE);
             this.setDeliveryType(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child2)));
             // Start Avail
             child2 = JAXPUtil.selectSingleNode(child1,PDBXMLTags.TAG_START_AVAIL);
             //setExceptionStartDate(FTDUtil.formatStringToUtilDate(StringUtils.trimToEmpty(child2.getNodeValue())));
              setExceptionStartDate(StringUtils.trimToEmpty((StringUtils.trimToEmpty(((Element)child2).getTextContent()))));
             // End Avail
             child2 = JAXPUtil.selectSingleNode(child1,PDBXMLTags.TAG_END_AVAIL);
             //setExceptionEndDate(FTDUtil.formatStringToUtilDate(StringUtils.trimToEmpty(child2.getNodeValue())));
             setExceptionEndDate(StringUtils.trimToEmpty((StringUtils.trimToEmpty(((Element)child2).getTextContent()))));
             // Avail exception message
             child2 = JAXPUtil.selectSingleNode(child1,PDBXMLTags.TAG_EXCEPTION_MSG);
             // Convert <BR>'s back to end of line characters
             String reason = FTDUtil.replaceAll((StringUtils.trimToEmpty(((Element)child2).getTextContent())), ProductMaintConstants.LINE_BREAK, ProductMaintConstants.END_OF_LINE_CHARACTER);
             this.setExceptionMessage(reason);
             //update
             child2 = JAXPUtil.selectSingleNode(child1,PDBXMLTags.TAG_UPDATE);
             if( child2!=null )
             {
                 nodes = JAXPUtil.selectNodes(child2,PDBXMLTags.TAG_SEND_TO);
                 
                 for( int idx=0; idx<nodes.getLength(); idx++ )
                 {
                     child3 = (Element)nodes.item(idx);
                     if( child3!=null )
                     {
                         String strTest = StringUtils.trimToEmpty(JAXPUtil.getTextValue(child3));
                         
                         if(ProductMaintConstants.PDB_NOVATOR_CONTENT_KEY.equals(strTest) )
                         {
                             this.setSentToNovatorContent(true);
                         }
                         else if(ProductMaintConstants.PDB_NOVATOR_UAT_KEY.equals(strTest) )
                         {
                             this.setSentToNovatorUAT(true);
                         }
                         else if(ProductMaintConstants.PDB_NOVATOR_TEST_KEY.equals(strTest) )
                         {
                             this.setSentToNovatorTest(true);
                         }
                         else if(ProductMaintConstants.PDB_NOVATOR_LIVE_KEY.equals(strTest) || ProductMaintConstants.PDB_NOVATOR_PROD_KEY.equals(strTest) )
                         {
                             this.setSentToNovatorProd(true);
                         }
                     }
                 }
             }
             // Options
             child2 = JAXPUtil.selectSingleNode(child1,PDBXMLTags.TAG_OPTIONS);
             if( child2!=null ) 
             {
                 ArrayList subCodes = new ArrayList();
                 ProductSubCodeVO pscVO;
                 nodes = JAXPUtil.selectNodes(child2,PDBXMLTags.TAG_OPTION_ENTITY);
                 
                 for( int idx=0; idx<nodes.getLength(); idx++ ) 
                 {
                     child3 = (Element)nodes.item(idx);
                     if( child3!=null )
                     {
                         pscVO = new ProductSubCodeVO();
                         child4 = JAXPUtil.selectSingleNode(child3,PDBXMLTags.TAG_PRODUCT_ID);
                         if( child4!=null ) 
                         {
                             pscVO.setProductId(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child4)));
                         }
                         child4 = JAXPUtil.selectSingleNode(child3,PDBXMLTags.TAG_SUB_PRODUCT_ID);
                         if( child4!=null ) 
                         {
                             pscVO.setProductSubCodeId(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child4)));
                         }
                         child4 = JAXPUtil.selectSingleNode(child3,PDBXMLTags.TAG_OPTION);
                         if( child4!=null ) 
                         {
                             pscVO.setSubCodeDescription(StringUtils.trimToEmpty(((Element)child4).getTextContent()));
                         }
                         child4 = JAXPUtil.selectSingleNode(child3,PDBXMLTags.TAG_PRICE);
                         if( child4!=null ) 
                         {
                             String strTest = StringUtils.trimToEmpty(JAXPUtil.getTextValue(child4));
                             float fPrice = (float)0.00;
                             try {
                                 if( strTest!=null && strTest.length()>0 ) {
                                     fPrice = Float.valueOf(strTest).floatValue();
                                 }    
                             } catch (NumberFormatException e) { fPrice=0.0f; } // Do nothing
                             pscVO.setSubCodePrice(fPrice);
                         }
                         child4 = JAXPUtil.selectSingleNode(child3,PDBXMLTags.TAG_HOLIDAY_PRICE);
                         if( child4!=null ) 
                         {
                             String strTest = StringUtils.trimToEmpty(JAXPUtil.getTextValue(child4));
                             float fPrice = (float)0.00;
                             try {
                                 if( strTest!=null && strTest.length()>0 ) {
                                     fPrice = Float.valueOf(strTest).floatValue();
                                 }    
                             } catch (NumberFormatException e) {fPrice=0.0f;} // Do nothing
                             pscVO.setSubCodeHolidayPrice(fPrice);
                         }
                         child4 = JAXPUtil.selectSingleNode(child3,PDBXMLTags.TAG_SUB_REFNUM);
                         if( child4!=null ) {
                             pscVO.setSubCodeRefNumber(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child4)));
                         }
                         child4 = JAXPUtil.selectSingleNode(child3,PDBXMLTags.TAG_HOLIDAY_SKU);
                         if( child4!=null ) {
                             pscVO.setSubCodeHolidaySKU(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child4)));
                         }
                         child4 = JAXPUtil.selectSingleNode(child3,PDBXMLTags.TAG_DIM_WEIGHT);
                         if( child4!=null ) {
                             pscVO.setDimWeight(StringUtils.trimToEmpty(((Element)child4).getTextContent()));
                         }
                         
                         subCodes.add(pscVO);
                     }
                 } //End while( it.hasNext() )
                 
                 this.setSubCodeList(subCodes);
             } //End if( child2!=null ) 
             // End of batch mode tags

             // WebOE Block
             child1 = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_WEBOE_BLOCK);
             this.setWeboeBlocked(BooleanUtils.toBoolean(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child1))));

             // Express shipping only
             child1 = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_EXPRESS_SHIPPING_ONLY);
             this.setExpressShippingOnly(BooleanUtils.toBoolean(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child1))));

             // Over 21
             child1 = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_OVER_21);
             this.setOver21Flag(BooleanUtils.toBoolean(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child1))));
             
             // Personal Greeting Flag
             child1 = JAXPUtil.buildSimpleXmlNode(doc,PDBXMLTags.TAG_PERSONAL_GREETING_FLAG,this.isPersonalGreetingFlag());
             root.appendChild(child1);

             // Shipping System
             child1 = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_SHIPPING_SYSTEM);
             this.setShippingSystem(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child1)));
             
             // Zone Jump Eligible Flag
             child1 = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_ZONE_JUMP_ELIGIBLE_FLAG);
             this.setZoneJumpEligibleFlag(BooleanUtils.toBoolean(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child1))));

             // Box Id
             child1 = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_BOX_ID);
             this.setBoxId(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child1)));

             // Supply Expense
             child1 = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_SUPPLY_EXPENSE);
             if (JAXPUtil.getTextValue(child1) != null && !JAXPUtil.getTextValue(child1).equals("")) {
                 this.setSupplyExpense(new Float(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child1))));
             }
             
             // Supply Expense Effective Date
             child1 = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_SUPPLY_EXPENSE_EFF_DATE);
             this.setSupplyExpenseEffDate(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child1)));
             
             // Royalty Percent
             child1 = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_ROYALTY_PERCENT);
             if (JAXPUtil.getTextValue(child1) != null && !JAXPUtil.getTextValue(child1).equals("")) {
                 this.setRoyaltyPercent(new Float(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child1))));
             }
             
             // Royalty Percent Effective Date
             child1 = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_ROYALTY_PERCENT_EFF_DATE);
             this.setRoyaltyPercentEffDate(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child1)));
             
             // Good, Better, Best fields
             child1 = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_GBB);
             if( child1 != null ) {
                 child2 = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_GBB_POPOVER_FLAG);
                 this.setGbbPopoverFlag(BooleanUtils.toBoolean(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child2))));
                 child2 = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_GBB_TITLE);
                 this.setGbbTitle(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child2)));
                 child2 = JAXPUtil.selectSingleNode(child1,PDBXMLTags.TAG_GBB_POSITION_1);
                 if (child2 != null) {
                     child3 = JAXPUtil.selectSingleNode(child2,PDBXMLTags.TAG_GBB_NAME_OVERRIDE_FLAG);
                     this.setGbbNameOverrideFlag1(BooleanUtils.toBoolean(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child1))));
                     child3 = JAXPUtil.selectSingleNode(child2,PDBXMLTags.TAG_GBB_NAME_OVERRIDE_TEXT);
                     this.setGbbNameOverrideText1(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child1)));
                     child3 = JAXPUtil.selectSingleNode(child2,PDBXMLTags.TAG_GBB_PRICE_OVERRIDE_FLAG);
                     this.setGbbPriceOverrideFlag1(BooleanUtils.toBoolean(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child1))));
                     child3 = JAXPUtil.selectSingleNode(child2,PDBXMLTags.TAG_GBB_PRICE_OVERRIDE_TEXT);
                     this.setGbbPriceOverrideText1(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child1)));
                 }
                 child2 = JAXPUtil.selectSingleNode(child1,PDBXMLTags.TAG_GBB_POSITION_2);
                 if (child2 != null) {
                     child3 = JAXPUtil.selectSingleNode(child2,PDBXMLTags.TAG_GBB_NAME_OVERRIDE_FLAG);
                     this.setGbbNameOverrideFlag2(BooleanUtils.toBoolean(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child1))));
                     child3 = JAXPUtil.selectSingleNode(child2,PDBXMLTags.TAG_GBB_NAME_OVERRIDE_TEXT);
                     this.setGbbNameOverrideText2(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child1)));
                     child3 = JAXPUtil.selectSingleNode(child2,PDBXMLTags.TAG_GBB_PRICE_OVERRIDE_FLAG);
                     this.setGbbPriceOverrideFlag2(BooleanUtils.toBoolean(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child1))));
                     child3 = JAXPUtil.selectSingleNode(child2,PDBXMLTags.TAG_GBB_PRICE_OVERRIDE_TEXT);
                     this.setGbbPriceOverrideText2(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child1)));
                 }
                 child2 = JAXPUtil.selectSingleNode(child1,PDBXMLTags.TAG_GBB_POSITION_3);
                 if (child2 != null) {
                     child3 = JAXPUtil.selectSingleNode(child2,PDBXMLTags.TAG_GBB_NAME_OVERRIDE_FLAG);
                     this.setGbbNameOverrideFlag3(BooleanUtils.toBoolean(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child1))));
                     child3 = JAXPUtil.selectSingleNode(child2,PDBXMLTags.TAG_GBB_NAME_OVERRIDE_TEXT);
                     this.setGbbNameOverrideText3(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child1)));
                     child3 = JAXPUtil.selectSingleNode(child2,PDBXMLTags.TAG_GBB_PRICE_OVERRIDE_FLAG);
                     this.setGbbPriceOverrideFlag3(BooleanUtils.toBoolean(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child1))));
                     child3 = JAXPUtil.selectSingleNode(child2,PDBXMLTags.TAG_GBB_PRICE_OVERRIDE_TEXT);
                     this.setGbbPriceOverrideText3(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child1)));
                 }
             }

             //Website source codes
             child1 = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_WEBSITES);
             if( child1!=null ) 
             {
                 nodes = JAXPUtil.selectNodes(child1,PDBXMLTags.TAG_WEBSITE);
                 StringBuffer sb = new StringBuffer();
                 
                 for( int idx=0; idx < nodes.getLength(); idx++) 
                 {
                     child2 = (Element)nodes.item(idx);
                     sb.append(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child2)));
                     sb.append(" ");
                 }
                 this.setWebsites(sb.toString());
             }
             
             //Service Duration (copy())
             child1 = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_SERVICE_DURATION);
             this.setDuration(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child1)));
             
             // Free-Shipping Flag (copy())
             child1 = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_ALLOW_FREE_SHIPPING);
             this.setAllowFreeShippingFlag(BooleanUtils.toBoolean(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child1))));
             
             // Morning Delivery flag
             child1 = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_MORNING_DELIVERY_FLAG);
             if(child1 != null) {
            	 this.setMorningDeliveryFlag(BooleanUtils.toBoolean(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child1))));
             }

             
            
 			child1 = JAXPUtil.selectSingleNode(root,PDBXMLTags.TAG_BULLET_DESCRIPTION);
 			this.setBulletDescription(StringUtils.trimToEmpty(JAXPUtil.getTextValue(child1)));
 				
             
             
         } catch (Exception e) 
         {
               String[] args = new String[1];
               args[0] = e.getMessage();
               logger.error(e);
               throw new PDBApplicationException(ProductMaintConstants.PDB_INVALID_XML_EXCEPTION, args, e);
         }
     }

    static public List buildDeliveryOptionsList(HttpServletRequest request) throws PDBApplicationException, 
                                                                      PDBSystemException {
        String[] shipCarrierArray = 
            request.getParameterValues("shippingMethodList");
        // This array is always blank (I think)
        //String[] novatorTagArray = 
        //    request.getParameterValues("novatorTagList");
        ArrayList deliveryArray = new ArrayList();
        //only continue if array has items
        if (shipCarrierArray != null) {
            //loop through all items in  carrier array
            for (int i = 0; i < shipCarrierArray.length; i++) {
                String checkFlag = shipCarrierArray[i];
                //only save if flag was checked
                if (checkFlag != null) {
                    DeliveryOptionVO deliveryVO = new DeliveryOptionVO();
                    deliveryVO.setShippingMethodId(checkFlag);
                    //Novator Tag (hidden on page)
                    //if (novatorTagArray!=null){
                    //    System.out.println("novatorTagArray[i]: "+novatorTagArray[i]);
                    //    deliveryVO.setNovatorTag(novatorTagArray[i]);
                    //}
                    deliveryArray.add(deliveryVO);   
                }
            } //end for
        } //end if array contains values
        return deliveryArray;
    }

    public void setSupplyExpense(float supplyExpense) {
        this.supplyExpense = supplyExpense;
    }

    public float getSupplyExpense() {
        return supplyExpense;
    }

    public void setSupplyExpenseEffDate(String supplyExpenseEffDate) {
        this.supplyExpenseEffDate = supplyExpenseEffDate;
    }

    public String getSupplyExpenseEffDate() {
        return supplyExpenseEffDate;
    }

    public void setRoyaltyPercent(float royaltyPercent) {
        this.royaltyPercent = royaltyPercent;
    }

    public float getRoyaltyPercent() {
        return royaltyPercent;
    }

    public void setRoyaltyPercentEffDate(String royaltyPercentEffDate) {
        this.royaltyPercentEffDate = royaltyPercentEffDate;
    }

    public String getRoyaltyPercentEffDate() {
        return royaltyPercentEffDate;
    }

    public void setComponentSkuList(List componentSkuList) {
        this.componentSkuList = componentSkuList;
    }

    public List getComponentSkuList() {
        return componentSkuList;
    }

    public void setAllComponentSkuList(List allComponentSkuList) {
        this.allComponentSkuList = allComponentSkuList;
    }

    public List getAllComponentSkuList() {
        return allComponentSkuList;
    }

    public void setComponentSkuArray(String[] componentSkuArray) {
        this.componentSkuArray = componentSkuArray;
    }

    public String[] getComponentSkuArray() {
        return componentSkuArray;
    }

    public void setGbbPopoverFlag(boolean gbbPopoverFlag) {
        this.gbbPopoverFlag = gbbPopoverFlag;
    }

    public boolean getGbbPopoverFlag() {
        return gbbPopoverFlag;
    }

    public void setGbbTitle(String gbbTitle) {
        this.gbbTitle = gbbTitle;
    }

    public String getGbbTitle() {
        return gbbTitle;
    }

    public void setGbbNameOverrideFlag1(boolean gbbNameOverrideFlag1) {
        this.gbbNameOverrideFlag1 = gbbNameOverrideFlag1;
    }

    public boolean getGbbNameOverrideFlag1() {
        return gbbNameOverrideFlag1;
    }

    public void setGbbNameOverrideText1(String gbbNameOverrideText1) {
        this.gbbNameOverrideText1 = gbbNameOverrideText1;
    }

    public String getGbbNameOverrideText1() {
        return gbbNameOverrideText1;
    }

    public void setGbbPriceOverrideFlag1(boolean gbbPriceOverrideFlag1) {
        this.gbbPriceOverrideFlag1 = gbbPriceOverrideFlag1;
    }

    public boolean getGbbPriceOverrideFlag1() {
        return gbbPriceOverrideFlag1;
    }

    public void setGbbPriceOverrideText1(String gbbPriceOverrideText1) {
        this.gbbPriceOverrideText1 = gbbPriceOverrideText1;
    }

    public String getGbbPriceOverrideText1() {
        return gbbPriceOverrideText1;
    }

    public void setGbbNameOverrideFlag2(boolean gbbNameOverrideFlag2) {
        this.gbbNameOverrideFlag2 = gbbNameOverrideFlag2;
    }

    public boolean getGbbNameOverrideFlag2() {
        return gbbNameOverrideFlag2;
    }

    public void setGbbNameOverrideText2(String gbbNameOverrideText2) {
        this.gbbNameOverrideText2 = gbbNameOverrideText2;
    }

    public String getGbbNameOverrideText2() {
        return gbbNameOverrideText2;
    }

    public void setGbbPriceOverrideFlag2(boolean gbbPriceOverrideFlag2) {
        this.gbbPriceOverrideFlag2 = gbbPriceOverrideFlag2;
    }

    public boolean getGbbPriceOverrideFlag2() {
        return gbbPriceOverrideFlag2;
    }

    public void setGbbPriceOverrideText2(String gbbPriceOverrideText2) {
        this.gbbPriceOverrideText2 = gbbPriceOverrideText2;
    }

    public String getGbbPriceOverrideText2() {
        return gbbPriceOverrideText2;
    }

    public void setGbbNameOverrideFlag3(boolean gbbNameOverrideFlag3) {
        this.gbbNameOverrideFlag3 = gbbNameOverrideFlag3;
    }

    public boolean getGbbNameOverrideFlag3() {
        return gbbNameOverrideFlag3;
    }

    public void setGbbNameOverrideText3(String gbbNameOverrideText3) {
        this.gbbNameOverrideText3 = gbbNameOverrideText3;
    }

    public String getGbbNameOverrideText3() {
        return gbbNameOverrideText3;
    }

    public void setGbbPriceOverrideFlag3(boolean gbbPriceOverrideFlag3) {
        this.gbbPriceOverrideFlag3 = gbbPriceOverrideFlag3;
    }

    public boolean getGbbPriceOverrideFlag3() {
        return gbbPriceOverrideFlag3;
    }

    public void setGbbPriceOverrideText3(String gbbPriceOverrideText3) {
        this.gbbPriceOverrideText3 = gbbPriceOverrideText3;
    }

    public String getGbbPriceOverrideText3() {
        return gbbPriceOverrideText3;
    }

  public Boolean getSentToNovatorProd()
  {
    return sentToNovatorProd;
  }

  public Boolean getSentToNovatorUAT()
  {
    return sentToNovatorUAT;
  }

  public Boolean getSentToNovatorTest()
  {
    return sentToNovatorTest;
  }

  public Boolean getSentToNovatorContent()
  {
    return sentToNovatorContent;
  }

    public void setSendStandardRecipe(boolean sendStandardRecipe) {
        this.sendStandardRecipe = sendStandardRecipe;
    }

    public boolean isSendStandardRecipe() {
        return sendStandardRecipe;
    }

    public void setDeluxeRecipe(String deluxeRecipe) {
        this.deluxeRecipe = deluxeRecipe;
    }

    public String getDeluxeRecipe() {
        return deluxeRecipe;
    }

    public void setSendDeluxeRecipe(boolean sendDeluxeRecipe) {
        this.sendDeluxeRecipe = sendDeluxeRecipe;
    }

    public boolean isSendDeluxeRecipe() {
        return sendDeluxeRecipe;
    }

    public void setPremiumRecipe(String premiumRecipe) {
        this.premiumRecipe = premiumRecipe;
    }

    public String getPremiumRecipe() {
        return premiumRecipe;
    }

    public void setSendPremiumRecipe(boolean sendPremiumRecipe) {
        this.sendPremiumRecipe = sendPremiumRecipe;
    }

    public boolean isSendPremiumRecipe() {
        return sendPremiumRecipe;
    }

    public HashMap<String, ArrayList<ProductAddonVO>> getProductAddonMap()
    {
        return productAddonMap;
    }

    public void setProductAddonMap(HashMap<String, ArrayList<ProductAddonVO>> productAddonMap)
    {
        this.productAddonMap = productAddonMap;
    }

    public void setWebsites(String websites) {
        this.websites = websites;
    }

    public String getWebsites() {
        return websites;
    }

  public void setAllowFreeShippingFlag(boolean allowFreeShippingFlag)
  {
    this.allowFreeShippingFlag = allowFreeShippingFlag;
  }

  public boolean isAllowFreeShippingFlag()
  {
    return allowFreeShippingFlag;
  }

  public void setDuration(String duration)
  {
    this.duration = duration;
  }

  public String getDuration()
  {
    return duration;
	}

	public boolean isMorningDeliveryFlag() {
		return morningDeliveryFlag;
	}

	public void setMorningDeliveryFlag(boolean morningDeliveryFlag) {
		this.morningDeliveryFlag = morningDeliveryFlag;
	}

  public boolean isCopyProduct() {
	 return isCopyProduct;
  }
	
  public void setCopyProduct(boolean isCopyProduct) {
	  this.isCopyProduct = isCopyProduct;
  }

  public void setAzCatalogStatusStandard(boolean azCatalogStatusStandard) {
	  this.azCatalogStatusStandard = azCatalogStatusStandard;
  }

  public boolean isAzCatalogStatusStandard() {
	  return azCatalogStatusStandard;
  }

  public void setAzProductNameStandard(String azProductNameStandard) {
	  this.azProductNameStandard = azProductNameStandard;
  }

  public String getAzProductNameStandard() {
	  return azProductNameStandard;
  }

  public void setAzProductDescriptionStandard(String azProductDescriptionStandard) {
	  this.azProductDescriptionStandard = azProductDescriptionStandard;
  }

  public String getAzProductDescriptionStandard() {
	  return azProductDescriptionStandard;
  }

  public void setAzCatalogStatusDeluxe(boolean azCatalogStatusDeluxe) {
	  this.azCatalogStatusDeluxe = azCatalogStatusDeluxe;
  }

  public boolean isAzCatalogStatusDeluxe() {
	  return azCatalogStatusDeluxe;
  }

  public void setAzProductNameDeluxe(String azProductNameDeluxe) {
	  this.azProductNameDeluxe = azProductNameDeluxe;
  }

  public String getAzProductNameDeluxe() {
	  return azProductNameDeluxe;
  }

  public void setAzProductDescriptionDeluxe(String azProductDescriptionDeluxe) {
	  this.azProductDescriptionDeluxe = azProductDescriptionDeluxe;
  }

  public String getAzProductDescriptionDeluxe() {
	  return azProductDescriptionDeluxe;
  }

  public void setAzCatalogStatusPremium(boolean azCatalogStatusPremium) {
	  this.azCatalogStatusPremium = azCatalogStatusPremium;
  }

  public boolean isAzCatalogStatusPremium() {
	  return azCatalogStatusPremium;
  }

  public void setAzProductNamePremium(String azProductNamePremium) {
	  this.azProductNamePremium = azProductNamePremium;
  }

  public String getAzProductNamePremium() {
	  return azProductNamePremium;
  }

  public void setAzProductDescriptionPremium(String azProductDescriptionPremium) {
	  this.azProductDescriptionPremium = azProductDescriptionPremium;
  }

  public String getAzProductDescriptionPremium() {
	  return azProductDescriptionPremium;
  }

public String getAzBulletPoint1() {
	return azBulletPoint1;
}

public void setAzBulletPoint1(String azBulletPoint1) {
	this.azBulletPoint1 = azBulletPoint1;
}

public String getAzBulletPoint2() {
	return azBulletPoint2;
}

public void setAzBulletPoint2(String azBulletPoint2) {
	this.azBulletPoint2 = azBulletPoint2;
}

public String getAzBulletPoint3() {
	return azBulletPoint3;
}

public void setAzBulletPoint3(String azBulletPoint3) {
	this.azBulletPoint3 = azBulletPoint3;
}

public String getAzBulletPoint4() {
	return azBulletPoint4;
}

public void setAzBulletPoint4(String azBulletPoint4) {
	this.azBulletPoint4 = azBulletPoint4;
}

public String getAzBulletPoint5() {
	return azBulletPoint5;
}

public void setAzBulletPoint5(String azBulletPoint5) {
	this.azBulletPoint5 = azBulletPoint5;
}

public String getAzSearchTerm1() {
	return azSearchTerm1;
}

public void setAzSearchTerm1(String azSearchTerm1) {
	this.azSearchTerm1 = azSearchTerm1;
}

public String getAzSearchTerm2() {
	return azSearchTerm2;
}

public void setAzSearchTerm2(String azSearchTerm2) {
	this.azSearchTerm2 = azSearchTerm2;
}

public String getAzSearchTerm3() {
	return azSearchTerm3;
}

public void setAzSearchTerm3(String azSearchTerm3) {
	this.azSearchTerm3 = azSearchTerm3;
}

public String getAzSearchTerm4() {
	return azSearchTerm4;
}

public void setAzSearchTerm4(String azSearchTerm4) {
	this.azSearchTerm4 = azSearchTerm4;
}

public String getAzSearchTerm5() {
	return azSearchTerm5;
}

public void setAzSearchTerm5(String azSearchTerm5) {
	this.azSearchTerm5 = azSearchTerm5;
}

public String getAzIntendedUse1() {
	return azIntendedUse1;
}

public void setAzIntendedUse1(String azIntendedUse1) {
	this.azIntendedUse1 = azIntendedUse1;
}

public String getAzIntendedUse2() {
	return azIntendedUse2;
}

public void setAzIntendedUse2(String azIntendedUse2) {
	this.azIntendedUse2 = azIntendedUse2;
}

public String getAzIntendedUse3() {
	return azIntendedUse3;
}

public void setAzIntendedUse3(String azIntendedUse3) {
	this.azIntendedUse3 = azIntendedUse3;
}

public String getAzIntendedUse4() {
	return azIntendedUse4;
}

public void setAzIntendedUse4(String azIntendedUse4) {
	this.azIntendedUse4 = azIntendedUse4;
}

public String getAzIntendedUse5() {
	return azIntendedUse5;
}

public void setAzIntendedUse5(String azIntendedUse5) {
	this.azIntendedUse5 = azIntendedUse5;
}

public String getAzTargetAudience1() {
	return azTargetAudience1;
}

public void setAzTargetAudience1(String azTargetAudience1) {
	this.azTargetAudience1 = azTargetAudience1;
}

public String getAzTargetAudience2() {
	return azTargetAudience2;
}

public void setAzTargetAudience2(String azTargetAudience2) {
	this.azTargetAudience2 = azTargetAudience2;
}

public String getAzTargetAudience3() {
	return azTargetAudience3;
}

public void setAzTargetAudience3(String azTargetAudience3) {
	this.azTargetAudience3 = azTargetAudience3;
}

public String getAzTargetAudience4() {
	return azTargetAudience4;
}

public void setAzTargetAudience4(String azTargetAudience4) {
	this.azTargetAudience4 = azTargetAudience4;
}

public String getAzOtherAttribute1() {
	return azOtherAttribute1;
}

public void setAzOtherAttribute1(String azOtherAttribute1) {
	this.azOtherAttribute1 = azOtherAttribute1;
}

public String getAzOtherAttribute2() {
	return azOtherAttribute2;
}

public void setAzOtherAttribute2(String azOtherAttribute2) {
	this.azOtherAttribute2 = azOtherAttribute2;
}

public String getAzOtherAttribute3() {
	return azOtherAttribute3;
}

public void setAzOtherAttribute3(String azOtherAttribute3) {
	this.azOtherAttribute3 = azOtherAttribute3;
}

public String getAzOtherAttribute4() {
	return azOtherAttribute4;
}

public void setAzOtherAttribute4(String azOtherAttribute4) {
	this.azOtherAttribute4 = azOtherAttribute4;
}

public String getAzOtherAttribute5() {
	return azOtherAttribute5;
}

public void setAzOtherAttribute5(String azOtherAttribute5) {
	this.azOtherAttribute5 = azOtherAttribute5;
}

public String getAzSubjectMatter1() {
	return azSubjectMatter1;
}

public void setAzSubjectMatter1(String azSubjectMatter1) {
	this.azSubjectMatter1 = azSubjectMatter1;
}

public String getAzSubjectMatter2() {
	return azSubjectMatter2;
}

public void setAzSubjectMatter2(String azSubjectMatter2) {
	this.azSubjectMatter2 = azSubjectMatter2;
}

public String getAzSubjectMatter3() {
	return azSubjectMatter3;
}

public void setAzSubjectMatter3(String azSubjectMatter3) {
	this.azSubjectMatter3 = azSubjectMatter3;
}

public String getAzSubjectMatter4() {
	return azSubjectMatter4;
}

public void setAzSubjectMatter4(String azSubjectMatter4) {
	this.azSubjectMatter4 = azSubjectMatter4;
}

public String getAzSubjectMatter5() {
	return azSubjectMatter5;
}

public void setAzSubjectMatter5(String azSubjectMatter5) {
	this.azSubjectMatter5 = azSubjectMatter5;
}

public String getAzItemType() {
	return azItemType;
}

public void setAzItemType(String azItemType) {
	this.azItemType = azItemType;
}

public String getMassEditMasterSKU() {
	return massEditMasterSKU;
}

public void setMassEditMasterSKU(String massEditMasterSKU) {
	this.massEditMasterSKU = massEditMasterSKU;
}

public String getMassEditProductStatus() {
	return massEditProductStatus;
}

public void setMassEditProductStatus(String massEditProductStatus) {
	this.massEditProductStatus = massEditProductStatus;
}

public String getMassEditSpreadSheetGeneralComments() {
	return massEditSpreadSheetGeneralComments;
}

public void setMassEditSpreadSheetGeneralComments(
		String massEditSpreadSheetGeneralComments) {
	this.massEditSpreadSheetGeneralComments = massEditSpreadSheetGeneralComments;
}

public String getMassEditExceptionMsg() {
	return massEditExceptionMsg;
}

public void setMassEditExceptionMsg(String massEditExceptionMsg) {
	this.massEditExceptionMsg = massEditExceptionMsg;
}

public String getPquadProductID() {
	return pquadProductID;
}

public void setPquadProductID(String pquadProductID) {
	this.pquadProductID = pquadProductID;
}

public String getFtdwestAccessoryId() {
	return ftdwestAccessoryId;
}

public void setFtdwestAccessoryId(String ftdwestAccessoryId) {
	this.ftdwestAccessoryId = ftdwestAccessoryId;
}

public String getFtdwestTemplateMapping() {
	return ftdwestTemplateMapping;
}

public void setFtdwestTemplateMapping(String ftdwestTemplateMapping) {
	this.ftdwestTemplateMapping = ftdwestTemplateMapping;
}

public String getBulletDescription() {
	return bulletDescription;
}

public void setBulletDescription(String bulletDescription) {
	this.bulletDescription = bulletDescription;
}







  
}
