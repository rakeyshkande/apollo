package com.ftd.pdb.common.valobjs;

public class VendorProductVO extends PDBValueObject {
    private String vendorId;
    private String productSkuId;
    private double vendorCost;
    private String vendorSku;
    private boolean available;
    private boolean removed;
    private String vendorName;
    
    public VendorProductVO() {
        super("com.ftd.pdb.common.valobjs.VendorProductVO");
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setProductSkuId(String productSkuId) {
        this.productSkuId = productSkuId;
    }

    public String getProductSkuId() {
        return productSkuId;
    }

    public void setVendorCost(double vendorCost) {
        this.vendorCost = vendorCost;
    }

    public double getVendorCost() {
        return vendorCost;
    }

    public void setVendorSku(String vendorSku) {
        this.vendorSku = vendorSku;
    }

    public String getVendorSku() {
        return vendorSku;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setRemoved(boolean removed) {
        this.removed = removed;
    }

    public boolean isRemoved() {
        return removed;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getVendorName() {
        return vendorName;
    }
}
