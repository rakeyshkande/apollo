package com.ftd.pdb.common.valobjs;

import java.io.Serializable;
import java.util.Date;

public class MassEditHeaderVO implements Serializable {
	
	private Long massEditHeaderId;
	private String fileName;
	private Date fileUploadDate;
	private String fileStatus;
	private Long totalRecords;
	private Date createdOn;
	private String createdBy;
	private String updatedBy;
	private Date updatedOn;
	
		
	public MassEditHeaderVO(Long massEditHeaderId, String fileName,
			Date fileUploadDate, String fileStatus, Long totalRecords,
			Date createdOn, String createdBy, String updatedBy, Date updatedOn) {
		super();
		this.massEditHeaderId = massEditHeaderId;
		this.fileName = fileName;
		this.fileUploadDate = fileUploadDate;
		this.fileStatus = fileStatus;
		this.totalRecords = totalRecords;
		this.createdOn = createdOn;
		this.createdBy = createdBy;
		this.updatedBy = updatedBy;
		this.updatedOn = updatedOn;
	}
	
	public MassEditHeaderVO(String fileName,
			String fileStatus,
			String updatedBy, Date updatedOn){
		super();
		this.fileName = fileName;
		this.fileStatus = fileStatus;
		this.updatedBy = updatedBy;
		this.updatedOn = updatedOn;
	}
	
	public MassEditHeaderVO() {
		super();
	}

	public Long getMassEditHeaderId() {
		return massEditHeaderId;
	}
	public void setMassEditHeaderId(Long massEditHeaderId) {
		this.massEditHeaderId = massEditHeaderId;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public Date getFileUploadDate() {
		return fileUploadDate;
	}
	public void setFileUploadDate(Date fileUploadDate) {
		this.fileUploadDate = fileUploadDate;
	}
	public String getFileStatus() {
		return fileStatus;
	}
	public void setFileStatus(String fileStatus) {
		this.fileStatus = fileStatus;
	}
	public Long getTotalRecords() {
		return totalRecords;
	}
	public void setTotalRecords(Long totalRecords) {
		this.totalRecords = totalRecords;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}


}
