package com.ftd.pdb.businessobject;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.Writer;
import java.math.BigDecimal;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.StringTokenizer;
import org.apache.commons.io.FileUtils;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.BuiltinFormats;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Name;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFDataValidation;
import org.apache.poi.xssf.usermodel.XSSFDataValidationConstraint;
import org.apache.poi.xssf.usermodel.XSSFDataValidationHelper;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xssf.usermodel.extensions.XSSFCellBorder.BorderSide;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.feed.NovatorFeedProductUtil;
import com.ftd.osp.utilities.feed.NovatorFeedUtil;
import com.ftd.osp.utilities.feed.vo.NovatorFeedResponseVO;
import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.exceptions.ErrorCodeStore;
import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBSystemException;
import com.ftd.pdb.common.resources.IResourceProvider;
import com.ftd.pdb.common.utilities.FTDUtil;
import com.ftd.pdb.common.valobjs.MassEditDetailVO;
import com.ftd.pdb.common.valobjs.MassEditFileStatusVO;
import com.ftd.pdb.common.valobjs.MassEditHeaderVO;
import com.ftd.pdb.common.valobjs.ProductSubCodeVO;
import com.ftd.pdb.common.valobjs.ProductSubTypeVO;
import com.ftd.pdb.common.valobjs.ProductVO;
import com.ftd.pdb.common.valobjs.UpsellMasterVO;
import com.ftd.pdb.common.valobjs.ValueVO;
import com.ftd.pdb.common.valobjs.VendorProductOverrideVO;
import com.ftd.pdb.integration.dao.IProductDAO;
import com.ftd.pdb.integration.dao.IProductMassEditDAO;
import com.ftd.pdb.service.ILookupSVCBusiness;
import com.ftd.pdb.service.IProductMaintSVCBusiness;
import com.ftd.pdb.service.IUpsellSVCBusiness;


/**
 * This class is used to perform business logic for functions related to a product
 *
 * @author Jeff Penney - Software Architects
 **/
public class ProductBOImpl extends PDBBusinessObject implements IProductBO {
    /* Spring manager resources */
    IProductDAO productDAO;
    IProductMassEditDAO productMassEditDAO;
    IUpsellSVCBusiness upsellSVC;
    ILookupSVCBusiness lookupSVC;
    IResourceProvider resourceProvider;
    IProductMaintSVCBusiness productSVC;
    
    private static final int MAX_NUM_SPREADSHEET_CELLS = 22; // DI-32: PDB Mass Edit - Add new fields that are coming from West - PQuad Product Id, Mercury Description.
   
    /* end Spring managed resources */

    public IProductMaintSVCBusiness getProductSVC() {
		return productSVC;
	}

	public void setProductSVC(IProductMaintSVCBusiness productSVC) {
		this.productSVC = productSVC;
	}

	/**
    * Constructor for the ProductBOImpl object.  Sets the debugEnabled
    * variable and inits the logger category.
    */
    public ProductBOImpl() {
        super("com.ftd.pdb.businessobject.ProductBOImpl");
    }

    /**
    * Creates a Lookup BO
    */
    public void create() {
    }

    /**
    * Returns a string representation of this BO
    *
    * @return a string representation of this BO
    */
    public String toString() {
        return null;
    }

    /**
    * Checks to see if this object is internally equal to the parameter object
    * @param object1 an object to be compared to this object
    */
    public boolean equals(java.lang.Object object1) {
        return false;
    }

    /**
    *   Saves this BO to persistent storage
    */
    public void save() {
    }

    /**
     * Return a list of products
     * @conn database connection
     * @return array of products
     * @exception String The exception description.
     */
    public List getProductList(Connection conn) throws PDBApplicationException, 
                                                       PDBSystemException {
        return productDAO.getProductList(conn);
    }

    /**
     * Returns search results based on the passed in search criteria
     * @param conn database connection
     * @param avail filter
     * @param unavail filter
     * @param dateRestrict filter
     * @param onHold filter
     * @return array of products based on filtered criteria
     * @exception String The exception description.
     */
    public List getProductListAdvanced(Connection conn, String avail, 
                                       String unavail, String dateRestrict, 
                                       String onHold) throws PDBApplicationException, 
                                                             PDBSystemException {
        return productDAO.getProductListAdvanced(conn, avail, unavail, 
                                                 dateRestrict, onHold);
    }

    /**
     * Returns an array of products with holiday pricing
     * @param conn database connection
     * @return array of products with holiday pricing
     * @exception String The exception description.
     */
    public List getProductListWithHolidayPricing(Connection conn) throws PDBApplicationException, 
                                                                         PDBSystemException {
        return productDAO.getProductListWithHolidayPricing(conn);
    }


    /**
     * Removes holiday pricing for the passed in product
     * @param conn database connection
     * @param product id
     * @throws PDBApplicationException
     * @throws PDBSystemException
     */
    public void removeHolidayPricing(Connection conn, 
                                     String product) throws PDBApplicationException, 
                                                            PDBSystemException {
        productDAO.removeHolidayPricing(conn, product);

        if (debugEnabled)
            logger.debug("ProductBOImpl: remove holiday pricing");
    }

    /**
    * Returns a ProductVO base on the passed product or novator id
    * @param conn database connection
    * @param productId
    * @param novatorId
    * @return ProductVO
    * @throws PDBApplicationException, PDBSystemException
    **/
    public ProductVO getProduct(Connection conn, String productId, 
                                String novatorId) throws PDBApplicationException, 
                                                         PDBSystemException {
        ProductVO productVO = null;
       if (productId != null) {
            productVO = productDAO.getProduct(conn, productId);
        } else {
            productVO = productDAO.getProductByNovatorId(conn, novatorId);
            if (productVO == null) {
                logger.error("Product and Novator id is null");
                throw new PDBApplicationException(ProductMaintConstants.PDB_NOVATOR_ID_DOES_NOT_EXIST_EXCEPTION);
            }
        }

        if (productVO != null) {
            // populate the delivery type field
            if (productVO.getCountry() != null && 
                productVO.getCountry().equals(ProductMaintConstants.DOMESTIC_COUNTRY_CODE)) {
                productVO.setDeliveryType(ProductMaintConstants.DOMESTIC_CODE);
            } else {
                productVO.setDeliveryType(ProductMaintConstants.INTERNATIONAL_CODE);
            }
        }

        return productVO;
    }

    
    /**
   * Updates or Creates a product in the database
   * @param conn database connection
   * @param productVO that will be created or updated
   * @param saveNovatorFeeds what novator servers will be updated
   * @throws PDBApplicationException, PDBSystemException
   **/
    public void setProduct(Connection conn, ProductVO productVO, 
                           List novatorEnvKeys) throws PDBApplicationException, 
                                                        PDBSystemException {
        //Get the rollback vo
        ProductVO rollbackProductVO = 
            getProduct(conn, productVO.getProductId(), null);

        //If Novator flag was true before update, then make sure it stays true
        if (rollbackProductVO != null) {
            if (rollbackProductVO.isSentToNovatorProd()) {
                productVO.setSentToNovatorProd(true);
            }
            if (rollbackProductVO.isSentToNovatorTest()) {
                productVO.setSentToNovatorTest(true);
            }
            if (rollbackProductVO.isSentToNovatorUAT()) {
                productVO.setSentToNovatorUAT(true);
            }
            if (rollbackProductVO.isSentToNovatorContent()) {
                productVO.setSentToNovatorContent(true);
            }
        }

        // Make the product ID upper case
        String productId = productVO.getProductId();
        productId = productId.toUpperCase();
        productVO.setProductId(productId);

        // Make the Novator ID upper case
        String novatorId = 
            FTDUtil.translateNullToString(productVO.getNovatorId());
        novatorId = novatorId.toUpperCase();
        productVO.setNovatorId(novatorId);

        // check to make sure the Novator ID is unique
        ProductVO tempProductVO = 
            productDAO.getProductByNovatorId(conn, productVO.getNovatorId());
        if (tempProductVO != null && 
            tempProductVO.getProductId() != null &&
            productVO.getProductId() != null &&
            (!tempProductVO.getProductId().equals(productVO.getProductId()))) {
            String[] args = new String[1];
            args[0] = new String(tempProductVO.getProductId());
            logger.error(ProductMaintConstants.PDB_NOVATOR_ID_DOES_EXIST_EXCEPTION + 
                         ": " + args[0]);
            throw new PDBApplicationException(ProductMaintConstants.PDB_NOVATOR_ID_DOES_EXIST_EXCEPTION, 
                                              args);
        }

        // Set the subcode flag if we have subcodes
        if (productVO.getSubCodeList() != null && 
            productVO.getSubCodeList().size() > 0) {
            productVO.setSubcodeFlag(true);

            List subcodes = productDAO.getSubCodeList(conn);
            // check to make sure the subcode ids are unique
            for (int i = 0; i < productVO.getSubCodeList().size(); i++) {
                for (int j = 0; j < subcodes.size(); j++) {
                    // unique subcode ID
                    if ((((ProductSubCodeVO)subcodes.get(j)).getProductSubCodeId().equals(((ProductSubCodeVO)productVO.getSubCodeList().get(i)).getProductSubCodeId())) && 
                        (!((ProductSubCodeVO)subcodes.get(j)).getProductId().equals(productVO.getProductId()))) {
                        String[] args = new String[1];
                        args[0] = 
                                new String(((ProductSubCodeVO)productVO.getSubCodeList().get(i)).getProductSubCodeId());
                        logger.error("Product id: " + args[0]);
                        throw new PDBApplicationException(ProductMaintConstants.SUBCODE_NOT_UNIQUE_EXCEPTION, 
                                                          args);
                    }
                }
            }
        }

        // Set the delivery included flag and service fee flag when
        // the type is Fresh Cuts
        if (FTDUtil.translateNullToString(productVO.getProductType()).equals(ProductMaintConstants.PDB_FRESH_CUTS_CODE) || 
            FTDUtil.translateNullToString(productVO.getProductType()).equals(ProductMaintConstants.PRODUCT_TYPE_SAME_DAY_FRESH_CUTS_CODE)) {
            productVO.setDeliveryIncludedFlag(true);
            productVO.setServiceFeeFlag(true);
        }

        // Convert the long description end of line characters to <BR>
        String longDesc = 
            FTDUtil.replaceAll(productVO.getLongDescription(), ProductMaintConstants.END_OF_LINE_CHARACTER, 
                               ProductMaintConstants.LINE_BREAK);
        productVO.setLongDescription(longDesc);

        // Convert the dominant flowers end of line characters to a space
        String flowers = 
            FTDUtil.replaceAll(productVO.getDominantFlowers(), ProductMaintConstants.END_OF_LINE_CHARACTER, 
                               " ");
        productVO.setDominantFlowers(flowers);

        // if this is a specialty gift then do special processing
        if (FTDUtil.translateNullToString(productVO.getProductType()).equals(ProductMaintConstants.PDB_SPECIALTY_GIFT_CODE)) {
            if (rollbackProductVO != null && 
                (productVO.getStandardPrice() != rollbackProductVO.getStandardPrice())) {
                List subCodes = productVO.getSubCodeList();
                for (int i = 0; i < subCodes.size(); i++) {
                    ProductSubCodeVO vo = (ProductSubCodeVO)subCodes.get(i);
                    vo.setSubCodePrice(productVO.getStandardPrice());
                }
            }
        }

        productDAO.setProduct(conn, productVO);

        // If disabling a product do special processing to update related upsells
        if (!productVO.isStatus() && rollbackProductVO != null && rollbackProductVO.isStatus()) {
        	NovatorFeedProductUtil feedUtil = new NovatorFeedProductUtil();
        	try {
        		feedUtil.updateUpsellsForDisabledProduct(conn, productVO.getProductId(), novatorEnvKeys);
        	} catch (Exception e1) {
                // Update the last update timestamp so we can rollback the changes
                tempProductVO = getProduct(conn, productVO.getProductId(), null);
                rollbackProductVO.setLastUpdateDate(tempProductVO.getLastUpdateDate());
                productDAO.setProduct(conn, rollbackProductVO);
        		logger.error("Error updating upsells for disabled product " + productVO.getProductId() + ". " + e1);

                throw new PDBSystemException(ProductMaintConstants.GENERAL_SYSTEM_EXCEPTION, e1);
        	}
        }

        if(novatorEnvKeys != null && novatorEnvKeys.size() > 0) {
            try {
                //Send updates to Novator 
                this.sendProduct(conn, productVO.getProductId(), novatorEnvKeys, true);
            } catch (Exception e) {
                // If this was a new product then we need to remove it
                if (rollbackProductVO == null) {
                    productDAO.deleteProduct(conn, productVO.getProductId());
                }
                // Roll back the changes to an existing product
                else {
                    // Update the last update timestamp so we can rollback the changes
                    tempProductVO = 
                            getProduct(conn, productVO.getProductId(), null);
                    rollbackProductVO.setLastUpdateDate(tempProductVO.getLastUpdateDate());
                    productDAO.setProduct(conn, rollbackProductVO);
                }

                if (e instanceof PDBApplicationException) {
                    PDBApplicationException pae = (PDBApplicationException)e;
                    throw pae;
                } else {
                    throw new PDBSystemException(ProductMaintConstants.OE_NOVATOR_TRANSMISSION_ERROR, 
                                                 e);
                }
            }
        }
        
        //The last step needs to be the saving of the Amazon data
        productDAO.setAmazonProduct(conn, productVO);
        
        if (debugEnabled)
            logger.debug("ProductBOImpl: Updated product successfully");
    }


    /**
   * Returns a list of subcodes with passed in subcode id
   * @param conn database connection
   * @param id subcode filter
   * @throws PDBSystemException
   **/
    public List getSubCodesByID(Connection conn, 
                                String id) throws PDBApplicationException, 
                                                  PDBSystemException {
        return productDAO.getSubCodesByID(conn, id);
    }


    /**
     * Returns an array of product ids that have been updated since the passed in date
     * @param conn database connection
     * @param lastUpdateDate return any products that have been updated since this date
     * @return array of product ids
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public List getProductsUpdatedSince(Connection conn, 
                                        java.util.Date lastUpdateDate) throws PDBSystemException, 
                                                                              PDBApplicationException {
        return productDAO.getProductsUpdatedSince(conn, lastUpdateDate);
    }

    /**
     * Send the product passed to Novator
     * @param conn
     * @param productVO
     * @param envKeys list of novator sites to update
     * @param overwriteSentTo If true overwrite any existing "sent_to_novator" values.
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public void sendProductToNovator(Connection conn, ProductVO productVO, 
                                     List envKeys, boolean overwriteSentTo) throws PDBSystemException, 
                                                            PDBApplicationException {
        sendProduct(conn, productVO.getProductId(), envKeys, overwriteSentTo);
    }

    /**
     * Send the product passed to Novator
     * @param conn
     * @param productId
     * @param envKeys
     * @param overwriteSentTo If true overwrite any existing "sent_to_novator" values.
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    private void sendProduct(Connection conn, String productId, 
                                     List envKeys, boolean overwriteSentTo) throws PDBSystemException, 
                                                    PDBApplicationException {

        try {
            NovatorFeedProductUtil feedUtil = new NovatorFeedProductUtil();
            NovatorFeedResponseVO resVO = feedUtil.sendProductFeed(conn, productId, envKeys, overwriteSentTo);
            String errorString = "";
            if(!resVO.isSuccess()) {
                errorString = resVO.getErrorString();
            }
            if (!errorString.equals("")){
                 logger.info("errorString returned:" + errorString);
                 String[] args = {errorString};
                 throw new PDBApplicationException(ProductMaintConstants.PDB_NOVATOR_EXCEPTION, args);        
            }
        } catch (PDBApplicationException pe) {
            logger.error(pe);
            throw pe;
        } catch (Exception e) {
            logger.error(e);
            String[] args = {e.getMessage()};
            throw new PDBApplicationException(ProductMaintConstants.PDB_NOVATOR_EXCEPTION, args);   
        }
    }

    /**
     * Validates the passed in product id
     * @param conn database conneciton
     * @param productId to validate
     * @return true if the product id is a valid product id, novator id, or subsku
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public boolean validateProductId(Connection conn, 
                                     String productId) throws PDBSystemException, 
                                                              PDBApplicationException {
        return productDAO.validateProductId(conn, productId);
    }
    
    /**
     * Get a list of lists of VendorProductVOs for vendors and their skus for the passed
     * in product/sku id
     * @param conn database connection
     * @param productSkuId to retrieve
     * @return List of VendorProductVOs for the passed in prduct/sku id
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public List getVendorsForProduct(Connection conn, String productSkuId) 
        throws PDBSystemException, PDBApplicationException {
            return productDAO.getVendorsForProduct(conn,productSkuId);
        }
        
    /**
     * Returns a list of products for the passed vendorId
     * @param conn
     * @param vendorId filter
     * @return list of Strings containing the product Ids
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public List getVendorProducts(Connection conn, String vendorId) throws PDBSystemException, PDBApplicationException {
        return productDAO.getVendorProducts(conn, vendorId);
    }
   
    /**
     * Returns a list of SDS ship methods for the passed carrierId
     * @param conn
     * @param carrierId filter
     * @return list of Strings containing the product Ids
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public List getCarrierSdsShipMethods(Connection conn, String carrierId) throws PDBSystemException, PDBApplicationException {
        return productDAO.getCarrierSdsShipMethods(conn, carrierId);
    }
    
    /**
     * Get a list of VendorProductOverrideVO objects, filtered on the passed variables
     * @param conn database connection
     * @param vendorId filter
     * @param productId filter
     * @param overrideDate filter
     * @param carrierId filter
     * @return
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public List getVPOverrides(Connection conn, String vendorId, String productId, Date overrideDate, String carrierId) 
        throws PDBSystemException, PDBApplicationException {
        return productDAO.getVPOverrides(conn,vendorId,productId,overrideDate,carrierId);    
    }
    
    /**
     * Updates the values of the passed VendorProductOverrideVO in the VENDOR_PRODUCT_OVERRIDE table
     * @param conn
     * @param vpoVO data to update
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public void updateVPOverride(Connection conn, VendorProductOverrideVO vpoVO) throws PDBSystemException, PDBApplicationException {
        productDAO.updateVPOverride(conn, vpoVO);
    }
    
    /**
     * Delete the values of the passed VendorProductOverrideVO in the VENDOR_PRODUCT_OVERRIDE table
     * @param conn
     * @param vpoVO data to delete
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public void deleteVPOverride(Connection conn, VendorProductOverrideVO vpoVO) throws PDBSystemException, PDBApplicationException {
        productDAO.deleteVPOverride(conn,vpoVO);
    }
    
    /**
     * Updates products that have exceptions to active or inactive
     * @param conn
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public void updateExceptionProducts(Connection conn) throws PDBSystemException, PDBApplicationException {

        boolean bSendLive = false;
        boolean bSendTest = false;
        boolean bSendUAT = false;
        boolean bSendContent = false;
        List<String> envKeys = new ArrayList();
                    
        try {            
            // Get the Novator servers to send to
            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
            
            String testString = configUtil.getFrpGlobalParmNoNull(ProductMaintConstants.PDB_CONFIG_CONTEXT,ProductMaintConstants.PDB_NOVATOR_LIVE_KEY);
            if( StringUtils.isNotBlank(testString) ) {
                envKeys.add(com.ftd.osp.utilities.feed.NovatorFeedUtil.PRODUCTION);
                bSendLive = true;
            }
            
            testString = configUtil.getFrpGlobalParmNoNull(ProductMaintConstants.PDB_CONFIG_CONTEXT,ProductMaintConstants.PDB_NOVATOR_TEST_KEY);
            if( StringUtils.isNotBlank(testString) ) {
                envKeys.add(com.ftd.osp.utilities.feed.NovatorFeedUtil.TEST);
                bSendTest = true;
            }
            
            testString = configUtil.getFrpGlobalParmNoNull(ProductMaintConstants.PDB_CONFIG_CONTEXT,ProductMaintConstants.PDB_NOVATOR_UAT_KEY);
            if( StringUtils.isNotBlank(testString) ) {
                envKeys.add(com.ftd.osp.utilities.feed.NovatorFeedUtil.UAT);
                bSendUAT = true;
            }
            
            testString = configUtil.getFrpGlobalParmNoNull(ProductMaintConstants.PDB_CONFIG_CONTEXT,ProductMaintConstants.PDB_NOVATOR_CONTENT_KEY);
            if( StringUtils.isNotBlank(testString) ) {
                envKeys.add(com.ftd.osp.utilities.feed.NovatorFeedUtil.CONTENT);
                bSendContent = true;
            }    
        } catch (Exception e) {
            logger.error("Unabled to determine who to send updates to.");
            throw new PDBApplicationException(ProductMaintConstants.CONFIG_PROPERTY_NOT_FOUND_EXCEPTION,e);
        }
        
        List<String> list = productDAO.getProductExceptionsToProcess(conn);
        int errCount = 0;;
        ProductVO productVO = null;
        
        for( int idx=0; idx < list.size(); idx++ ) {
            try {
                String productId = list.get(idx);
                productVO = productDAO.getProduct(conn,productId);
                logger.info("Old info: " + productVO.getProductId() + 
                		" status: " + productVO.isStatus() +
                		" exception code: " + productVO.getExceptionCode());
                
                //Set it to the opposite of what it is now
                //Defect 7926, 12565 - When product is made unavailable, set the exception_end_date, Exception_start_date, 
                //exception_message, exception-code to null values, along with status being updated to 'U'
                if (productVO != null && productVO.getExceptionCode() != null && productVO.getExceptionCode().equalsIgnoreCase("A")) {
                    productVO.setStatus(false);
                }
                if(!productVO.isStatus()) {
                    productVO.setExceptionEndDate(null);
                    productVO.setExceptionStartDate(null);
                    productVO.setExceptionMessage(null);
                    productVO.setExceptionCode(null);
                }
                productVO.setSentToNovatorContent(bSendContent);
                productVO.setSentToNovatorProd(bSendLive);
                productVO.setSentToNovatorTest(bSendTest);
                productVO.setSentToNovatorUAT(bSendUAT);
                this.setProduct(conn,productVO,envKeys);

                logger.info("New info: " + productVO.getProductId() + 
                		" status: " + productVO.isStatus() +
                		" exception code: " + productVO.getExceptionCode());

            } catch (Exception e) {
                errCount++;
                logger.error("Error updating product "+productVO.getProductId(),e);
            }
        }
        
        if( errCount>0 ) {
            String[] msgs = new String[1];
            msgs[0] = "Errors occurred in the update of exception product availability.  See logs for details.";
            throw new PDBApplicationException(ProductMaintConstants.PDB_EXCEPTION_UPDATE_ERROR,msgs);
        }
    }

    /**
     * Checks to see if id exists in FTD_APPS.VENDOR_PRODUCTS
     * @param conn
     * @param productId to check
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public String existsInVendorProduct(Connection conn, String productId) throws PDBSystemException, PDBApplicationException {
        return productDAO.existsInVendorProduct(conn,productId);
    }
    
    /**
     * Checks to see if source codes exist. Returns a list of source codes that do not exist.
     * @param conn
     * @param sourceCodeList
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public List validateSourceCode(Connection conn, String sourceCodeList) throws PDBSystemException, PDBApplicationException {
        List invalidSourceList = null;
        if(sourceCodeList != null) {
            StringTokenizer st = new StringTokenizer(sourceCodeList);
            boolean sourceCodeExists = true;
            while(st.hasMoreTokens()) {
                String sourceCode = st.nextToken();

                sourceCodeExists = productDAO.sourceCodeExists(conn, sourceCode);
                logger.debug("validateSourceCode:" + sourceCode + "." + sourceCodeExists);
                if(!sourceCodeExists) {
                    if(invalidSourceList == null) {
                        invalidSourceList = new ArrayList();
                    }
                    invalidSourceList.add(sourceCode);
                }
            }
        }
        return invalidSourceList;
        
    }
    
    /**
     * Creates the Product Maintenance Mass Edit Spreadsheet
     * 
     * @param result list from database query
     * @return XSSFWorkbook containing the spreadsheet data
     */
    public XSSFWorkbook createProductMassEditSpreadsheet(List<ProductVO> productList, boolean errorFlag) throws Exception {
		if (logger.isDebugEnabled())
			logger.debug(">>> Start createProductMassEditSpreadsheet");
		
		String[] errorParms = new String[2];
		String errorMsg = "";
		DecimalFormat df = new DecimalFormat("####0.00");		
		XSSFWorkbook wb = new XSSFWorkbook();
		CreationHelper createHelper = wb.getCreationHelper();
		XSSFSheet sheet = wb.createSheet("Products");
		sheet.setDisplayGridlines(true);
		sheet.setDefaultColumnWidth(14);
		//set column widths, the width is measured in units of 1/256th of a character width
		sheet.setColumnWidth(2, 256*15); // product name
		sheet.setColumnWidth(4, 256*15); // novator name
		sheet.setColumnWidth(5, 256*33); // PQuad Product ID
		sheet.setColumnWidth(7, 256*20); // category
		sheet.setColumnWidth(9, 256*12); // sub type
		sheet.setColumnWidth(10, 256*60); // long desc
		sheet.setColumnWidth(11, 256*60); // mercury description
		sheet.setColumnWidth(13, 256*20); // delivery type
		sheet.setColumnWidth(14, 256*40); // standard recipe
		sheet.setColumnWidth(15, 256*40); // deluxe recipe
		sheet.setColumnWidth(16, 256*40); // premium recipe
		sheet.setColumnWidth(22, 256*40); // comments
		sheet.setColumnWidth(23, 256*40); // validations errors
		
		sheet.createFreezePane(0, 2);
		
		// Styles
		XSSFCellStyle titleStyle;
	    XSSFFont titleFont = wb.createFont();
	    titleFont.setFontHeightInPoints((short)18);
	    titleFont.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
	    titleStyle = wb.createCellStyle();
	    titleStyle.setAlignment(CellStyle.ALIGN_CENTER);
	    titleStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
	    titleStyle.setFont(titleFont);
	    titleStyle.setLocked(true);
	    
	    XSSFFont monthFont = wb.createFont();
        monthFont.setFontHeightInPoints((short)11);
        monthFont.setColor(IndexedColors.WHITE.getIndex());
        XSSFCellStyle headerStyle = wb.createCellStyle();
        headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
        headerStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        headerStyle.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
        headerStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
        headerStyle.setFont(monthFont);
        headerStyle.setWrapText(true);
        headerStyle.setLocked(true);
		
		XSSFCellStyle readOnlyCell = (XSSFCellStyle) wb.createCellStyle();
        readOnlyCell.setAlignment(XSSFCellStyle.ALIGN_LEFT); 
        readOnlyCell.setWrapText(true);
        readOnlyCell.setLocked(true); 
        readOnlyCell.setDataFormat((short)BuiltinFormats.getBuiltinFormat("text"));
        
        XSSFCellStyle editableCell = (XSSFCellStyle) wb.createCellStyle();
        editableCell.setAlignment(XSSFCellStyle.ALIGN_LEFT);
        editableCell.setWrapText(true);
        editableCell.setLocked(false);
        editableCell.setDataFormat((short)BuiltinFormats.getBuiltinFormat("text"));
				    	
    	XSSFCellStyle editableNumericCell = (XSSFCellStyle) wb.createCellStyle();
		editableNumericCell.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
		editableNumericCell.setWrapText(true);
    	editableNumericCell.setLocked(false);
    	editableNumericCell.setDataFormat(createHelper.createDataFormat().getFormat("###0.00"));
      
    	XSSFCellStyle newRowCell = (XSSFCellStyle) wb.createCellStyle();
		newRowCell.setAlignment(XSSFCellStyle.ALIGN_LEFT);
		newRowCell.setWrapText(true);
    	newRowCell.setLocked(false);
    	//newRowCell.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
    	newRowCell.setBorderTop((short) 1);
    	newRowCell.setBorderColor(BorderSide.TOP, new XSSFColor(Color.RED));
    	//newRowCell.setFillPattern(CellStyle.SOLID_FOREGROUND);
   	
    	 //title row
        XSSFRow titleRow = sheet.createRow(0);
        titleRow.setHeightInPoints(45);
        XSSFCell titleCell = titleRow.createCell(0);
        titleCell.setCellValue("Product List");
        titleCell.setCellStyle(titleStyle);
        //sheet.addMergedRegion(CellRangeAddress.valueOf("$A$1:$S$1"));
        sheet.addMergedRegion(new CellRangeAddress(0,0,0,21)); 
       	// header row
        XSSFRow row = sheet.createRow(1);
        XSSFCell cell = row.createCell(0);
        cell.setCellValue("Master SKU");
        cell.setCellStyle(headerStyle);
        cell =  row.createCell(1);
        cell.setCellValue("Product ID");
        cell.setCellStyle(headerStyle);
        cell = row.createCell(2);
        cell.setCellValue("Product Name");
        cell.setCellStyle(headerStyle);
        cell = row.createCell(3);
        cell.setCellValue("Novator ID");
        cell.setCellStyle(headerStyle);
        cell = row.createCell(4);
        cell.setCellValue("Novator Name");
        cell.setCellStyle(headerStyle);
        /*
         * DI-32: PDB Mass Edit - Add new fields that are coming from West.
         * Adding new column "PQuad Product ID" in the header row
        */
        cell = row.createCell(5);
        cell.setCellValue("PQuad Product ID");
        cell.setCellStyle(headerStyle);
        cell = row.createCell(6);
        cell.setCellValue("Florist Reference #");
        cell.setCellStyle(headerStyle);
        cell = row.createCell(7);
        cell.setCellValue("Category");
        cell.setCellStyle(headerStyle);
        cell = row.createCell(8);
        cell.setCellValue("Product Type");
        cell.setCellStyle(headerStyle);
        cell = row.createCell(9);
        cell.setCellValue("Sub-Type");
        cell.setCellStyle(headerStyle);
        cell = row.createCell(10);
        cell.setCellValue("Description");
        cell.setCellStyle(headerStyle);
        /*
         * DI-32: PDB Mass Edit - Add new fields that are coming from West.
         * Adding new column "Mercury Description" in the header row
        */
        cell = row.createCell(11);
        cell.setCellValue("Mercury Description");
        cell.setCellStyle(headerStyle);
        cell = row.createCell(12);
        cell.setCellValue("Availability");
        cell.setCellStyle(headerStyle);
        cell = row.createCell(13);
        cell.setCellValue("Type of Delivery");
        cell.setCellStyle(headerStyle);
        cell = row.createCell(14);
        cell.setCellValue("Floral Standard Recipe");
        cell.setCellStyle(headerStyle);
        cell = row.createCell(15);
        cell.setCellValue("Floral Deluxe Recipe");
        cell.setCellStyle(headerStyle);
        cell = row.createCell(16);
        cell.setCellValue("Floral Premium Recipe");
        cell.setCellStyle(headerStyle);
        cell = row.createCell(17);
        cell.setCellValue("Dropship Dim/Weight");
        cell.setCellStyle(headerStyle);
        cell = row.createCell(18);
        cell.setCellValue("Standard Price");
        cell.setCellStyle(headerStyle);
        cell = row.createCell(19);
        cell.setCellValue("Deluxe Price");
        cell.setCellStyle(headerStyle);
        cell = row.createCell(20);
        cell.setCellValue("Premium Price");
        cell.setCellStyle(headerStyle);
        cell = row.createCell(21);
        cell.setCellValue("Novator Update");
        cell.setCellStyle(headerStyle);
        cell = row.createCell(22);
        cell.setCellValue("Comments");
        cell.setCellStyle(headerStyle);
        
        if(errorFlag)
        {
        	cell = row.createCell(23);
        	cell.setCellValue("Validation Errors");
            cell.setCellStyle(headerStyle);
        }
         
        XSSFDataValidationHelper dvHelper = new XSSFDataValidationHelper(sheet);
        // validations for sub type (based on type)
        
        String[] floralSubTypeText = buildSubTypeTextArray("FLORAL");
        String[] freshCutSubTypeText = buildSubTypeTextArray("FRECUT"); 
        String[] sameDayFreshCutSubTypeText = buildSubTypeTextArray("SDFC"); 
        String[] noneSubTypeText = buildSubTypeTextArray("NONE"); 
        String[] sameDayGiftSubTypeText = buildSubTypeTextArray("SDG"); 
        String[] servicesSubTypeText = buildSubTypeTextArray("SERVICES"); 
        String[] specialtyGiftSubTypeText = buildSubTypeTextArray("SPEGFT"); 
        
        /*
         * The Category List exceeds 255 characters when you try to combine all of the descriptions
         * into an array (like Product Sub-Type and Availability dropdowns).  You need to build lists
         * that exceed this 255 Excel limit using a hidden sheet like the code below - BK
         */
       	XSSFSheet hiddenCategorySheet = wb.createSheet("HiddenCategory");
        int categorySize = buildCategoryDropdown(hiddenCategorySheet);
        Name namedCell = wb.createName();
        namedCell.setNameName("hiddenCategory");
        namedCell.setRefersToFormula("hiddenCategory!$A$1:$A$" + categorySize);
        XSSFDataValidationConstraint categoriesConstraint = 
            (XSSFDataValidationConstraint) dvHelper.createFormulaListConstraint("hiddenCategory");
        wb.setSheetHidden(1, true);
        
        
        XSSFDataValidationConstraint floralSubTypesConstraint = 
        	(XSSFDataValidationConstraint) dvHelper.createExplicitListConstraint(floralSubTypeText);
        XSSFDataValidationConstraint freshCutSubTypesConstraint = 
        	(XSSFDataValidationConstraint) dvHelper.createExplicitListConstraint(freshCutSubTypeText);
        XSSFDataValidationConstraint noneSubTypesConstraint = 
        	(XSSFDataValidationConstraint) dvHelper.createExplicitListConstraint(noneSubTypeText);
        XSSFDataValidationConstraint sameDayGiftSubTypesConstraint = 
        	(XSSFDataValidationConstraint) dvHelper.createExplicitListConstraint(sameDayGiftSubTypeText);
        XSSFDataValidationConstraint sameDayFreshCutSubTypesConstraint = 
        	(XSSFDataValidationConstraint) dvHelper.createExplicitListConstraint(sameDayFreshCutSubTypeText);
        XSSFDataValidationConstraint servicesSubTypesConstraint = 
        	(XSSFDataValidationConstraint) dvHelper.createExplicitListConstraint(servicesSubTypeText);
        XSSFDataValidationConstraint specialtyGiftSubTypesConstraint = 
        	(XSSFDataValidationConstraint) dvHelper.createExplicitListConstraint(specialtyGiftSubTypeText);
        
        int rowCount = 2;
        for (ProductVO p : productList) {
        	// skip products where product type is null or = "NONE"
        	if (p.getProductType() == null || "None".equalsIgnoreCase(p.getProductType() ))
        		continue;
        	
        	row = (XSSFRow) sheet.createRow(rowCount);
        	        	
        	// master sku - locked
        	cell = row.createCell(0);
        	cell.setCellValue(p.getMassEditMasterSKU());
        	cell.setCellStyle(readOnlyCell);
        	cell.setCellType(XSSFCell.CELL_TYPE_STRING);
        	
        	// product id - locked
        	cell = row.createCell(1);
        	cell.setCellValue(p.getProductId());
        	cell.setCellStyle(readOnlyCell);
        	cell.setCellType(XSSFCell.CELL_TYPE_STRING);
        	
        	// product name - editable
        	cell = row.createCell(2);
        	cell.setCellValue(p.getProductName());
        	cell.setCellStyle(editableCell);
        	cell.setCellType(XSSFCell.CELL_TYPE_STRING);
        	
        	// novator id - editable
        	cell = row.createCell(3);
        	cell.setCellValue(p.getNovatorId());
        	cell.setCellStyle(editableCell);
        	cell.setCellType(XSSFCell.CELL_TYPE_STRING);
        	
        	// novator name - editable
        	cell = row.createCell(4);
        	cell.setCellValue(p.getNovatorName());
        	cell.setCellStyle(editableCell);
        	cell.setCellType(XSSFCell.CELL_TYPE_STRING);
        	
        	/*
            * DI-32: PDB Mass Edit - Add new fields that are coming from West.
            * Add value to the new column: PQuad Product ID
            */
        	cell = row.createCell(5);
        	cell.setCellValue(p.getPquadProductID());
        	cell.setCellStyle(editableCell);
        	cell.setCellType(XSSFCell.CELL_TYPE_STRING);
        	
        	
        	// florist ref # - readOnly if product type = SERVICES
        	cell = row.createCell(6);
        	cell.setCellValue(p.getFloristReferenceNumber());
        	if ("Services".equals(p.getProductType()))
            	cell.setCellStyle(readOnlyCell);
            else
            	cell.setCellStyle(editableCell);
        	cell.setCellType(XSSFCell.CELL_TYPE_STRING);
        	
        	// category - editable
        	cell = row.createCell(7);
        	cell.setCellValue(p.getCategory());
        	cell.setCellStyle(editableCell);
        	cell.setCellType(XSSFCell.CELL_TYPE_STRING);
              	
        	// product type - locked
        	cell = row.createCell(8);
        	cell.setCellValue(p.getProductType());
        	cell.setCellStyle(readOnlyCell);
        	cell.setCellType(XSSFCell.CELL_TYPE_STRING);
        	
        	// product sub type - editable
        	cell = row.createCell(9);
        	cell.setCellValue(p.getProductSubType());
        	cell.setCellStyle(editableCell);
        	cell.setCellType(XSSFCell.CELL_TYPE_STRING);
        	// need to do set the validation here because the sub type varies depending on
        	// the product type - DON'T DELETE THIS or you will delete the error message.
            // We did this once already!  ;)  - BK
        	errorParms[0] = "Sub-Type";
     		errorParms[1] = "";
     	    errorMsg = ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_INVALID_FIELD, errorParms);
     	
        		
        	CellRangeAddressList subTypeColumn = new CellRangeAddressList(rowCount, rowCount, 9, 9);
         	if ("Floral".equalsIgnoreCase(p.getProductType())) {
         		XSSFDataValidation cellValidation = (XSSFDataValidation) dvHelper.createValidation(floralSubTypesConstraint, subTypeColumn);
        		cellValidation.createErrorBox("Invalid value for Sub-Type", errorMsg);
        		cellValidation.setShowErrorBox(true);
        		cellValidation.setSuppressDropDownArrow(true);
        		cellValidation.setEmptyCellAllowed(true); 
        		sheet.addValidationData(cellValidation);
        	}
        	else if ("Fresh Cuts".equalsIgnoreCase(p.getProductType())) {
        		XSSFDataValidation cellValidation = (XSSFDataValidation) dvHelper.createValidation(freshCutSubTypesConstraint, subTypeColumn);
        		cellValidation.createErrorBox("Invalid value for Sub-Type", errorMsg);
        		cellValidation.setShowErrorBox(true);
        		cellValidation.setSuppressDropDownArrow(true);
        		cellValidation.setEmptyCellAllowed(true); 
        		sheet.addValidationData(cellValidation);
        	}
         	/*  This is not longer a valid choice for product type - it should be skipped
        	else if ("None".equalsIgnoreCase(p.getProductType())) {
        		XSSFDataValidation cellValidation = (XSSFDataValidation) dvHelper.createValidation(noneSubTypesConstraint, subTypeColumn);
        		cellValidation.createErrorBox("Invalid value for None Sub Type", "Please select a valid type from the dropdown list");
        		cellValidation.setShowErrorBox(true);
        		cellValidation.setSuppressDropDownArrow(true);
        		cellValidation.setEmptyCellAllowed(true); 
        		sheet.addValidationData(cellValidation);
        	}
        	*/
        	else if ("Same Day Fresh Cuts".equalsIgnoreCase(p.getProductType())) {
        		XSSFDataValidation cellValidation = (XSSFDataValidation) dvHelper.createValidation(sameDayFreshCutSubTypesConstraint, subTypeColumn);
        		cellValidation.createErrorBox("Invalid value for Sub-Type", errorMsg);
        		cellValidation.setShowErrorBox(true);
        		cellValidation.setSuppressDropDownArrow(true);
        		cellValidation.setEmptyCellAllowed(true); 
        		sheet.addValidationData(cellValidation);
        	}
  	     	else if ("Same Day Gift".equalsIgnoreCase(p.getProductType())) {
        		XSSFDataValidation cellValidation = (XSSFDataValidation) dvHelper.createValidation(sameDayGiftSubTypesConstraint, subTypeColumn);
        		cellValidation.createErrorBox("Invalid value for Sub-Type", errorMsg);
        		cellValidation.setShowErrorBox(true);
        		cellValidation.setSuppressDropDownArrow(true);
        		cellValidation.setEmptyCellAllowed(true); 
        		sheet.addValidationData(cellValidation);
        	}
        	else if ("Services".equalsIgnoreCase(p.getProductType())) {
        		XSSFDataValidation cellValidation = (XSSFDataValidation) dvHelper.createValidation(servicesSubTypesConstraint, subTypeColumn);
        		cellValidation.createErrorBox("Invalid value for Sub-Type", errorMsg);
        		cellValidation.setShowErrorBox(true);
        		cellValidation.setSuppressDropDownArrow(true);
        		sheet.addValidationData(cellValidation);
        	}
        	else if ("Specialty Gift".equalsIgnoreCase(p.getProductType())) {
        		XSSFDataValidation cellValidation = (XSSFDataValidation) dvHelper.createValidation(specialtyGiftSubTypesConstraint, subTypeColumn);
        		cellValidation.createErrorBox("Invalid value for Sub-Type", errorMsg);
        		cellValidation.setShowErrorBox(true);
        		cellValidation.setSuppressDropDownArrow(true);
        		cellValidation.setEmptyCellAllowed(true); 
        		sheet.addValidationData(cellValidation);
        	}
        
        	// long description - editable
        	cell = row.createCell(10);
        	cell.setCellValue(p.getLongDescription());
        	cell.setCellStyle(editableCell);
        	cell.setCellType(XSSFCell.CELL_TYPE_STRING);
        	
        	/*
             * DI-32: PDB Mass Edit - Add new fields that are coming from West.
             * Add value to the new column: Mercury Description
             */
         	cell = row.createCell(11);
         	cell.setCellValue(p.getMercuryDescription());
         	cell.setCellStyle(editableCell);
         	cell.setCellType(XSSFCell.CELL_TYPE_STRING);
        	
        	// status - editable
        	cell = row.createCell(12);
          	cell.setCellValue(p.getMassEditProductStatus());
        	cell.setCellStyle(editableCell);
        	cell.setCellType(XSSFCell.CELL_TYPE_STRING);
        	
        	// delivery type - locked
        	String deliveryType = getSpreadSheetDeliveryType(p.isShipMethodFlorist(), p.isShipMethodCarrier());
        	cell = row.createCell(13);
        	cell.setCellValue(deliveryType);
        	cell.setCellStyle(readOnlyCell);
        	cell.setCellType(XSSFCell.CELL_TYPE_STRING);
        	
        	// standard recipe - readOnly if product type = FRECUT, SPEGFT, or SERVICES
        	cell = row.createCell(14);
        	cell.setCellValue(p.getRecipe());
        	cell.setCellType(XSSFCell.CELL_TYPE_STRING);
        	if ("Fresh Cuts".equals(p.getProductType()) ||  
        		"Specialty Gift".equals(p.getProductType()) || 
        		"Services".equals(p.getProductType()))
        		cell.setCellStyle(readOnlyCell);
        	else
        		cell.setCellStyle(editableCell);
        	
        	// deluxe recipe - readOnly if product type = FRECUT, SPEGFT, or SERVICES
        	cell = row.createCell(15);
        	cell.setCellValue(p.getDeluxeRecipe());
        	cell.setCellType(XSSFCell.CELL_TYPE_STRING);
        	if ("Fresh Cuts".equals(p.getProductType()) ||  
            	"Specialty Gift".equals(p.getProductType()) || 
            	"Services".equals(p.getProductType()))
        		cell.setCellStyle(readOnlyCell);
        	else
        		cell.setCellStyle(editableCell);
        	
        	// premium recipe - readOnly if product type = FRECUT, SPEGFT, or SERVICES
        	cell = row.createCell(16);
        	cell.setCellValue(p.getPremiumRecipe());
        	cell.setCellType(XSSFCell.CELL_TYPE_STRING);
        	if ("Fresh Cuts".equals(p.getProductType()) ||  
            	"Specialty Gift".equals(p.getProductType()) || 
            	"Services".equals(p.getProductType()))
        		cell.setCellStyle(readOnlyCell);
        	else
        		cell.setCellStyle(editableCell);
        	
        	// dim weight - readOnly if product type = FLORAL or SERVICES
        	cell = row.createCell(17);
        	cell.setCellType(XSSFCell.CELL_TYPE_STRING);
        	cell.setCellValue(p.getDimWeight());
        	if ("Floral".equals(p.getProductType()) ||  
               	"Services".equals(p.getProductType()))
        		cell.setCellStyle(readOnlyCell);
        	else
        		cell.setCellStyle(editableCell);
        	
        	// standard price - editable  - have to convert it to a formatted
        	// string before putting it in the cell or extra decimals get added
        	// to it when trying to edit the cell.  Weird POI problem - BK  3/19/2014
        	cell = row.createCell(18);
        	String holdStandardPrice = df.format(p.getStandardPrice());
        	cell.setCellValue(holdStandardPrice);
        	cell.setCellStyle(editableNumericCell);
        	         	
        	// deluxe price - readOnly if product type = SERVICES
        	cell = row.createCell(19);
        	String holdDeluxePrice = df.format(p.getDeluxePrice());
        	cell.setCellValue(holdDeluxePrice);
        	if ("Services".equals(p.getProductType()))
            	cell.setCellStyle(readOnlyCell);
            else
        	    cell.setCellStyle(editableNumericCell);
        	
        	// premium price - readOnly if product type = SERVICES
        	cell = row.createCell(20);
        	String holdPremiumPrice = df.format(p.getPremiumPrice());
        	cell.setCellValue(holdPremiumPrice);
        	if ("Services".equals(p.getProductType()))
            	cell.setCellStyle(readOnlyCell);
            else
        	    cell.setCellStyle(editableNumericCell);
        	
        	// novator update - set default value to No - value choices are either Yes or No
        	cell = row.createCell(21);
        	cell.setCellValue("No");
        	cell.setCellStyle(editableCell);
        	cell.setCellType(XSSFCell.CELL_TYPE_STRING);
        	// comments - comes in empty  
        	if(!errorFlag)
        	{
        		cell = row.createCell(22);
              	cell.setCellValue("");
            	cell.setCellStyle(editableCell);
            	cell.setCellType(XSSFCell.CELL_TYPE_STRING);
        	}
        	else
        	{
        		cell = row.createCell(22);
        		cell.setCellType(XSSFCell.CELL_TYPE_STRING);
        		if(p.getMassEditSpreadSheetGeneralComments() == null)
        			cell.setCellValue("");
        		else
        			cell.setCellValue(p.getMassEditSpreadSheetGeneralComments());
            	cell.setCellStyle(editableCell);
        	}
  
        	
        	//Validation errors
        	if(errorFlag)
        	{
        		cell = row.createCell(23);
        		cell.setCellType(XSSFCell.CELL_TYPE_STRING);
            	cell.setCellValue(p.getMassEditExceptionMsg());
            	cell.setCellStyle(editableCell);
        	}
        	
          	rowCount++;
      }	
        
    // ***** Validations *****
  
    // Standard price must be numeric and in range .01 to 9999.99
    XSSFDataValidationConstraint standardPriceConstraint = 
    	(XSSFDataValidationConstraint) dvHelper.createNumericConstraint(XSSFDataValidationConstraint.ValidationType.DECIMAL,
    			XSSFDataValidationConstraint.OperatorType.BETWEEN, ".01", "9999.99");

    // Deluxe or Premium price must be numeric and in range 0.00 to 9999.99
    XSSFDataValidationConstraint deluxeOrPremiumPriceConstraint = 
    	(XSSFDataValidationConstraint) dvHelper.createNumericConstraint(XSSFDataValidationConstraint.ValidationType.DECIMAL,
    			XSSFDataValidationConstraint.OperatorType.BETWEEN, "0.00", "9999.99");

    
    //Recipe constraint			
    XSSFDataValidationConstraint recipeConstraint =   
       	(XSSFDataValidationConstraint) dvHelper.createTextLengthConstraint(XSSFDataValidationConstraint.OperatorType.LESS_OR_EQUAL,"255",null);  
    
    // Product name must be <= 25 characters and is required.
    CellRangeAddressList productNameColumn = new CellRangeAddressList(2, rowCount, 2, 2);
    XSSFDataValidationConstraint productNameConstraint =   
    	(XSSFDataValidationConstraint) dvHelper.createTextLengthConstraint(XSSFDataValidationConstraint.OperatorType.BETWEEN,"1","25");  
    XSSFDataValidation productNameValidation = (XSSFDataValidation) dvHelper.createValidation(productNameConstraint, productNameColumn);
    errorParms[0] = "Product Name";
	errorParms[1] = "25";
    errorMsg = ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_INVALID_FIELD_LENGTH, errorParms);
    productNameValidation.createErrorBox("Invalid value for Product Name", errorMsg);
    productNameValidation.setShowErrorBox(true);
    productNameValidation.setEmptyCellAllowed(false);
    
    // Novator id must be <= 10 characters and is required.
    CellRangeAddressList novatorIdColumn = new CellRangeAddressList(2, rowCount, 3, 3);
    XSSFDataValidationConstraint novatorIdConstraint =   
    	(XSSFDataValidationConstraint) dvHelper.createTextLengthConstraint(XSSFDataValidationConstraint.OperatorType.BETWEEN,"1","10");  
    XSSFDataValidation novatorIdValidation = (XSSFDataValidation) dvHelper.createValidation(novatorIdConstraint, novatorIdColumn);
    errorParms[0] = "Novator ID";
	errorParms[1] = "10";
    errorMsg = ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_INVALID_FIELD_LENGTH, errorParms);
    novatorIdValidation.createErrorBox("Invalid value for Novator Id", errorMsg);
    novatorIdValidation.setShowErrorBox(true);
    novatorIdValidation.setSuppressDropDownArrow(false);
    
    // Novator name must be <= 100 characters and is required.
    CellRangeAddressList novatorNameColumn = new CellRangeAddressList(2, rowCount, 4, 4);
    XSSFDataValidationConstraint novatorNameConstraint =   
    	(XSSFDataValidationConstraint) dvHelper.createTextLengthConstraint(XSSFDataValidationConstraint.OperatorType.BETWEEN,"1","100");  
    XSSFDataValidation novatorNameValidation = (XSSFDataValidation) dvHelper.createValidation(novatorNameConstraint, novatorNameColumn);
    errorParms[0] = "Novator Name";
	errorParms[1] = "100";
    errorMsg = ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_INVALID_FIELD_LENGTH, errorParms);
    novatorNameValidation.createErrorBox("Invalid value for Novator Name", errorMsg);
    novatorNameValidation.setShowErrorBox(true);
    novatorNameValidation.setEmptyCellAllowed(false);
 
    
    // Florist reference # must be <= 62 characters and is required.
    CellRangeAddressList floristRefColumn = new CellRangeAddressList(2, rowCount, 6, 6);
    XSSFDataValidationConstraint floristRefConstraint =   
    	(XSSFDataValidationConstraint) dvHelper.createTextLengthConstraint(XSSFDataValidationConstraint.OperatorType.BETWEEN,"1","30");  
    XSSFDataValidation floristRefValidation = (XSSFDataValidation) dvHelper.createValidation(floristRefConstraint, floristRefColumn);
    errorParms[0] = "Florist Reference Number";
	errorParms[1] = "30";
    errorMsg = ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_INVALID_FIELD_LENGTH, errorParms);
    floristRefValidation.createErrorBox("Invalid value for Florist Reference Number", errorMsg);
    floristRefValidation.setShowErrorBox(true);
    floristRefValidation.setSuppressDropDownArrow(false);
    
    // Categories - optional but if entered must be selected from dropdown list
    CellRangeAddressList categoryColumn = new CellRangeAddressList(2, rowCount, 7, 7);
    XSSFDataValidation categoryValidation = (XSSFDataValidation) dvHelper.createValidation(categoriesConstraint,categoryColumn );
    errorParms[0] = "Category";
	errorParms[1] = "";
    errorMsg = ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_INVALID_FIELD, errorParms);
    categoryValidation.createErrorBox("Invalid value for Category", errorMsg);
	categoryValidation.setShowErrorBox(true);
	categoryValidation.setSuppressDropDownArrow(true);
	categoryValidation.setEmptyCellAllowed(true); 
	
	// Description must be <= 1024 characters and is required.
    CellRangeAddressList descriptionColumn = new CellRangeAddressList(2, rowCount, 10, 10);
    XSSFDataValidationConstraint descriptionConstraint =   
    	(XSSFDataValidationConstraint) dvHelper.createTextLengthConstraint(XSSFDataValidationConstraint.OperatorType.BETWEEN,"1","1024");  
    XSSFDataValidation descriptionValidation = (XSSFDataValidation) dvHelper.createValidation(descriptionConstraint, descriptionColumn);
    errorParms[0] = "Description";
	errorParms[1] = "1024";
    errorMsg = ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_INVALID_FIELD_LENGTH, errorParms);
    descriptionValidation.createErrorBox("Invalid value for Description", errorMsg);
    descriptionValidation.setShowErrorBox(true);
    descriptionValidation.setEmptyCellAllowed(false);
    

    //Status (valid values 'A' or 'U')
    CellRangeAddressList statusColumn = new CellRangeAddressList(2, rowCount, 12, 12);
    XSSFDataValidationConstraint statusConstraint = 
    	(XSSFDataValidationConstraint) dvHelper.createExplicitListConstraint(new String[] {"Available","Unavailable"});
    XSSFDataValidation statusValidation = (XSSFDataValidation) dvHelper.createValidation(statusConstraint, statusColumn);
    errorParms[0] = "Availability";
	errorParms[1] = "";
    errorMsg = ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_INVALID_FIELD, errorParms);
  statusValidation.createErrorBox("Invalid value for Availability", errorMsg);
    statusValidation.setShowErrorBox(true);
    statusValidation.setSuppressDropDownArrow(true);
    
    // Standard recipe must not be > 255 characters if entered
    CellRangeAddressList standardRecipeColumn = new CellRangeAddressList(2, rowCount, 14, 14);
    XSSFDataValidation standardRecipeValidation = (XSSFDataValidation) dvHelper.createValidation(recipeConstraint, standardRecipeColumn);
    errorParms[0] = "Floral Standard Recipe";
	errorParms[1] = "255";
    errorMsg = ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_INVALID_FIELD_LENGTH, errorParms);
    standardRecipeValidation.createErrorBox("Invalid value for Floral Standard Recipe", errorMsg);
    standardRecipeValidation.setShowErrorBox(true);
    standardRecipeValidation.setEmptyCellAllowed(false);
    
    // Deluxe recipe must not be > 255 characters if entered
    CellRangeAddressList deluxeRecipeColumn = new CellRangeAddressList(2, rowCount, 15, 15);
    XSSFDataValidation deluxeRecipeValidation = (XSSFDataValidation) dvHelper.createValidation(recipeConstraint, deluxeRecipeColumn);
    errorParms[0] = "Floral Deluxe Recipe";
	errorParms[1] = "255";
    errorMsg = ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_INVALID_FIELD_LENGTH, errorParms);
    deluxeRecipeValidation.createErrorBox("Invalid value for Floral Deluxe Recipe", errorMsg);
    deluxeRecipeValidation.setShowErrorBox(true);
    deluxeRecipeValidation.setEmptyCellAllowed(false);
    
    // Premium recipe must be not be > 255 characters if entered
    CellRangeAddressList premiumRecipeColumn = new CellRangeAddressList(2, rowCount, 16, 16);
    XSSFDataValidation premiumRecipeValidation = (XSSFDataValidation) dvHelper.createValidation(recipeConstraint, premiumRecipeColumn);
    errorParms[0] = "Floral Premium Recipe";
	errorParms[1] = "255";
    errorMsg = ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_INVALID_FIELD_LENGTH, errorParms);
    premiumRecipeValidation.createErrorBox("Invalid value for Floral Premium Recipe", errorMsg);
    premiumRecipeValidation.setShowErrorBox(true);
    premiumRecipeValidation.setEmptyCellAllowed(false);
    
    // Dim/weight must be <= 10 characters and is required.
    CellRangeAddressList dimWeightColumn = new CellRangeAddressList(2, rowCount, 17, 17);
    XSSFDataValidationConstraint dimWeightConstraint =   
    	(XSSFDataValidationConstraint) dvHelper.createTextLengthConstraint(XSSFDataValidationConstraint.OperatorType.BETWEEN,"1","10");  
    XSSFDataValidation dimWeightValidation = (XSSFDataValidation) dvHelper.createValidation(dimWeightConstraint, dimWeightColumn);
    errorParms[0] = "Dropship Dim/Weight";
	errorParms[1] = "10";
    errorMsg = ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_INVALID_FIELD_LENGTH, errorParms);
    dimWeightValidation.createErrorBox("Invalid value for Dropship Dim/Weight", errorMsg);
    dimWeightValidation.setShowErrorBox(true);
    dimWeightValidation.setSuppressDropDownArrow(false);
    
  
    CellRangeAddressList standardPriceColumn = new CellRangeAddressList(2, rowCount, 18, 18);
    XSSFDataValidation standardPriceValidation = (XSSFDataValidation) dvHelper.createValidation(standardPriceConstraint, standardPriceColumn);
    errorParms[0] = "Standard Price";
	errorParms[1] = "";
    errorMsg = ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_IS_VALID_STANDARD_PRICE, errorParms);
    standardPriceValidation.createErrorBox("Invalid value for Standard Price", errorMsg);
    standardPriceValidation.setShowErrorBox(true);
    
    CellRangeAddressList deluxePriceColumn = new CellRangeAddressList(2, rowCount, 19, 19);
    XSSFDataValidation deluxePriceValidation = (XSSFDataValidation) dvHelper.createValidation(deluxeOrPremiumPriceConstraint, deluxePriceColumn);
    errorParms[0] = "Deluxe Price";
	errorParms[1] = "";
    errorMsg = ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_IS_VALID_DELUXE_OR_PREMIUM_PRICE, errorParms);
    deluxePriceValidation.createErrorBox("Invalid value for Deluxe Price", errorMsg);
    deluxePriceValidation.setShowErrorBox(true);
    
    CellRangeAddressList premiumPriceColumn = new CellRangeAddressList(2, rowCount, 20, 20);
    XSSFDataValidation premiumPriceValidation = (XSSFDataValidation) dvHelper.createValidation(deluxeOrPremiumPriceConstraint, premiumPriceColumn);
    errorParms[0] = "Premium Price";
	errorParms[1] = "";
    errorMsg = ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_IS_VALID_DELUXE_OR_PREMIUM_PRICE, errorParms);
    premiumPriceValidation.createErrorBox("Invalid value for Premium Price", errorMsg);
    premiumPriceValidation.setShowErrorBox(true);
    
  //Novator Update (valid values 'Yes' or 'No')
    CellRangeAddressList novatorUpdateColumn = new CellRangeAddressList(2, rowCount, 21, 21);
    XSSFDataValidationConstraint novatorUpdateConstraint = 
    	(XSSFDataValidationConstraint) dvHelper.createExplicitListConstraint(new String[] {"Yes", "No"});
    XSSFDataValidation novatorUpdateValidation = 
    	(XSSFDataValidation) dvHelper.createValidation(novatorUpdateConstraint, novatorUpdateColumn);
    errorParms[0] = "Novator Update";
	errorParms[1] = "";
    errorMsg = ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_INVALID_FIELD, errorParms);
    novatorUpdateValidation.createErrorBox("Invalid value for Novator Update", errorMsg);
    novatorUpdateValidation.setShowErrorBox(true);
    novatorUpdateValidation.setSuppressDropDownArrow(true);
    
    sheet.addValidationData(productNameValidation);
    sheet.addValidationData(novatorIdValidation);
    sheet.addValidationData(novatorNameValidation);
    sheet.addValidationData(floristRefValidation);
    sheet.addValidationData(categoryValidation);
    sheet.addValidationData(descriptionValidation);
    sheet.addValidationData(statusValidation);
    sheet.addValidationData(standardRecipeValidation);
    sheet.addValidationData(deluxeRecipeValidation);
    sheet.addValidationData(premiumRecipeValidation);
    sheet.addValidationData(dimWeightValidation);
    sheet.addValidationData(standardPriceValidation);
    sheet.addValidationData(deluxePriceValidation);
    sheet.addValidationData(premiumPriceValidation);
    sheet.addValidationData(novatorUpdateValidation);
    
    ConfigurationUtil configUtil = new ConfigurationUtil();
    String password = configUtil.getSecureProperty("PDB_MASS_EDIT", "EXCEL_PASSWORD");    
    sheet.protectSheet(password);  
    hiddenCategorySheet.protectSheet(password); 
    

    return wb;
    	
    }
    
    /**
     * Returns search results based on the passed in search criteria
     * @param conn database connection
     * @param search type 'M' - by master sku or 'P' by product id
     * @param String array containing either 1-5 master skus or 1-5 product ids
     * @return
     * @throws PDBApplicationException
     * @throws PDBSystemException
      */
    public List<ProductVO> getProductMassEditList(Connection conn, String searchType, 
                                       String[] searchItems) 
                                      throws PDBApplicationException, PDBSystemException {
    	List<ProductVO> massEditList = 
    		convertDbCodesToDescriptions(productDAO.getProductMassEditList(conn, searchType, searchItems));
        return massEditList;
    }
    
    /**
     * Converts the Category Codes into Category Descriptions
     * Converts the Product Type Codes into Product Type Descriptions
     * Converts the Product Sub Type Codes into Product Type Descriptions
     * 
     * @param List of product VOs
     * @return
     */
    public List<ProductVO> convertDbCodesToDescriptions(List<ProductVO> inputList) throws PDBSystemException {
    	 List<ProductVO> theList = new ArrayList<ProductVO>();
    	 HashMap<String, String> categoryMap = createCodeDescriptionMap(lookupSVC.getCategoryList());	
    	 HashMap<String, String> productTypeMap = createCodeDescriptionMap(lookupSVC.getTypesList());	
    	 HashMap<String, String> productSubTypeMap = createCodeDescriptionMapForSubTypes(lookupSVC.getSubTypesList());	
     	
    	 for (ProductVO vo: inputList) {
    		 if (categoryMap.containsKey(vo.getCategory()))
    			 vo.setCategory(categoryMap.get(vo.getCategory()));
    		 if (productTypeMap.containsKey(vo.getProductType())) {
    			 vo.setProductType(productTypeMap.get(vo.getProductType()));
    		 }
    		 if (productSubTypeMap.containsKey(vo.getProductSubType()))
    			 vo.setProductSubType(productSubTypeMap.get(vo.getProductSubType()));
    		 theList.add(vo);
    	 }
    	
    	 return theList;
    }
    
    /**
     * Converts the Category Descriptions into Category Codes
     * Converts the Product Type Descriptions into Product Type Codes
     * Converts the Product Sub Type Descriptions into Product Sub Type Codes
     * 
     * @param List of product VOs
     * @return
     */
    public List<ProductVO> convertDescriptionsToDbCodes(List<ProductVO> inputList) throws PDBSystemException {
    	 List<ProductVO> theList = new ArrayList<ProductVO>();
    	 
    	 HashMap<String, String> categoryMap =  createCodeDescriptionMap(lookupSVC.getCategoryList());	    
    	 HashMap<String, String> productSubTypeMap = createCodeDescriptionMapForSubTypes(lookupSVC.getSubTypesList());	
		 HashMap<String, String> productTypeMap = createCodeDescriptionMap(lookupSVC.getTypesList());	
     	
    	 for (ProductVO vo: inputList) {
    		 if (categoryMap.containsValue(vo.getCategory())) {
    			 for (Entry<String, String> entry : categoryMap.entrySet()) {
    		         if (vo.getCategory().equals(entry.getValue())) {
    		             vo.setCategory(entry.getKey());
    		             break;
    		         }
    		     }
    		 }	 
    		 if (productTypeMap.containsValue(vo.getProductType())) {
    			 for (Entry<String, String> entry : productTypeMap.entrySet()) {
    		         if (vo.getProductType().equals(entry.getValue())) {
    		             vo.setProductType(entry.getKey());
    		             break;
    		         }
    		     }
    		 }
    	     if (productSubTypeMap.containsValue(vo.getProductSubType())) {
    	    	 for (Entry<String, String> entry : productSubTypeMap.entrySet()) {
    		         if (vo.getProductSubType().equals(entry.getValue())) {
    		             vo.setProductSubType(entry.getKey());
    		             break;
    		         }
    		     }
    	     }
    		 theList.add(vo);
    	 }
    	    
    	 return theList;
    }  
    
    /**
     * Gets the category list from the PRODUCT_CATEGORY_TYPE table and returns back
     * an array containing the descriptions.
     * 
     * @return String[]
     */
    public int buildCategoryDropdown(XSSFSheet hiddenCategorySheet) throws PDBSystemException {
        List<ValueVO> categoryListFromDb = (List<ValueVO>) lookupSVC.getCategoryList();
        int rowCount = 0;
        if (categoryListFromDb != null && !categoryListFromDb.isEmpty()) {
        	for (ValueVO vo :  categoryListFromDb) {
         	   	if (StringUtils.isEmpty(vo.getId())) 
         			continue;
         	   	else
         	   		if(vo.getId().equals("NONE"))
         	   			vo.setDescription("None");
         	   		
         		XSSFRow row = hiddenCategorySheet.createRow(rowCount);
         		XSSFCell cell = row.createCell(0);
         		cell.setCellValue(vo.getDescription());
         		rowCount++;
          	}
         }
        return rowCount;
        
   }
      
    /**
     * Creates a HashMap for the "code" data (like categories and product types, 
     * etc), where the ID is the HashMap key and the description is the HashMap value.
     * 
     * @param List of ValueVO objects
     * @return HashMap  
     */
    public HashMap createCodeDescriptionMap(List<ValueVO> inputList) {
    	HashMap<String, String> codeMap = null;
    	if (inputList != null && !inputList.isEmpty())
    	{
    		codeMap = new HashMap<String, String>();
    		for (ValueVO vo :  inputList) 
    			if(vo.getId().equals("NONE"))
    				codeMap.put(vo.getId(),"None");
    			else
    				codeMap.put(vo.getId(),vo.getDescription());
    	}	
    	return codeMap;
    }
    
    /**
     * Creates a HashMap for the product sub type where the ID is the HashMap key and the
     * description is the HashMap value.
     * 
     * @param List of ProductSubTypeVO objects
     * @return HashMap  
     */
    public HashMap createCodeDescriptionMapForSubTypes(List<ProductSubTypeVO> inputList) {
    	HashMap<String, String> codeMap = new HashMap<String, String>();
    	if (inputList != null && !inputList.isEmpty()) {
    		for (ProductSubTypeVO vo :  inputList) {
    			if (vo.getId().equalsIgnoreCase("NONE"))
    				vo.setDescription("None");
    			codeMap.put(vo.getId(),vo.getDescription());
      		}
    	}	
    	return codeMap;
    }
    
    /**
     * Inserts a new row into Mass Edit Header table
     * 
     * @param Connection
	 * @param MassEditDetailVO
	 * @return Long - id of row just inserted
	 * @throws PDBSystemException
	 * @throws PDBApplicationException
     */
    public Long insertMassEditHeader(Connection conn, MassEditHeaderVO headerVO) 
    	throws PDBSystemException, PDBApplicationException {
    	return productMassEditDAO.insertMassEditHeader(conn, headerVO);
    }
    
    /**
     * Inserts a new row into Mass Edit Detail table
     * 
     * @param Connection
	 * @param MassEditDetailVO
	 * @return Long - id of row just inserted
	 * @throws PDBSystemException
	 * @throws PDBApplicationException
     */
    public Long insertMassEditDetail(Connection conn, MassEditDetailVO detailVO) 
    	throws PDBSystemException, PDBApplicationException {
    	return productMassEditDAO.insertMassEditDetail(conn, detailVO);
    }

    /**
     * Updates a row into Mass Edit Header table
     * 
     * @param Connection
	 * @param MassEditDetailVO
	 *  
	 * @throws PDBSystemException
	 * @throws PDBApplicationException
     */
    public void updateMassEditHeader(Connection conn, MassEditHeaderVO headerVO) 
    	throws PDBSystemException, PDBApplicationException {
    	productMassEditDAO.updateMassEditHeader(conn, headerVO);
    }

    
    private String getNoExtFileName(String filename)
    {
    	String fileExt = "";
    	int i = -1;
    	if((i = filename.indexOf('.')) != -1)
    	{
    		fileExt = filename.substring(i);
    		filename = filename.substring(0,i);
    	}
    	return filename;
    }
    
    /**
     * Uploads the Mass Edit spreadsheet data to the database.
     * 
     * @param conn
     * @param fileName
     * @param user
     * @param uploadFileDate (needs to use java.sql.Date to pass into the stored proc.
     * @throws Exception
     */
    public void uploadProductMassEditSpreadSheet(Connection conn,String filename,String user,InputStream pdbMassEditFileIS)
    throws Exception
    {
		//Move to server /u02/apollo/pdb directory
        String masseditDirectory =  resourceProvider.getGlobalParameter("PDB_MASS_EDIT","PDB_MASS_EDIT_DIRECTORY");
          	
    	//1. create arrayList of ProductVO from massEditWorkbook .Go through entire workbook and prepare the productVO lists.
    	List<ProductVO> productVoList = getProductVOListFromSpreadSheet(conn,user,pdbMassEditFileIS);
    	    	
    	//Insert record in Mass_edit_header table (filename) with status of "In Process"
    	MassEditHeaderVO headerVO = new MassEditHeaderVO();
    	headerVO.setFileName(filename);
    	headerVO.setFileStatus("In process");
    	headerVO.setCreatedBy(user);
    	headerVO.setUpdatedBy(user);
    	if(productVoList == null)
    		headerVO.setTotalRecords(new Long(0));
    	else
    		headerVO.setTotalRecords(new Long(productVoList.size()));    
    	

    	Long massEditHeaderId = productMassEditDAO.insertMassEditHeader(conn,headerVO);
    	
    	headerVO.setMassEditHeaderId(massEditHeaderId);
    	
    	//Loop through the ProductVoList
    	if(productVoList != null && productVoList.size() > 0)
    	{
    		List<ProductVO> invalidProductVOList = new ArrayList<ProductVO>();
    		
    		for (ProductVO pvo : productVoList)
    		{    			
    	    	List novatorEnvKeys = new ArrayList();
    	        if (pvo.getSentToNovatorContent() != null && pvo.getSentToNovatorContent()) {
    	            novatorEnvKeys.add(NovatorFeedUtil.CONTENT);    	            
    	        }    	       
    	        if (pvo.getSentToNovatorTest() != null && pvo.getSentToNovatorTest()) {
    	            novatorEnvKeys.add(NovatorFeedUtil.TEST);
    	        }
    	        if (pvo.getSentToNovatorUAT() != null && pvo.getSentToNovatorUAT()) {
    	            novatorEnvKeys.add(NovatorFeedUtil.UAT);    	           
    	        }
    	        if (pvo.getSentToNovatorProd() != null && pvo.getSentToNovatorProd()) {
    	            novatorEnvKeys.add(NovatorFeedUtil.PRODUCTION);
    	        }
    	        
    			String validationErrMsg = this.validateProductMassEdit(conn, pvo);
    			if(StringUtils.isEmpty(validationErrMsg))
    			{
    				try
    				{    					
    					//call the setProduct
    					setProduct(conn,pvo,novatorEnvKeys);
    					insertMassEditDetail(conn,massEditHeaderId, pvo);
    				} 
    				catch(Exception e)
					{
						//Feed failed due to exception
						pvo.setMassEditExceptionMsg("Updating of product failed." + e.getMessage());
	    				invalidProductVOList.add(pvo);
	    				
	    				//insert into mass edit detail table the status
						insertMassEditDetail(conn,massEditHeaderId, pvo);
					}    
    			}
    			else
    			{
    				//Not valid  product.
    				pvo.setMassEditExceptionMsg(validationErrMsg);
    				invalidProductVOList.add(pvo);
    				insertMassEditDetail(conn,massEditHeaderId, pvo);
    			}    			
    		}
    		// Check for InvalidProduct invalidProductVOList
    		if(invalidProductVOList != null && invalidProductVOList.size() > 0)
    		{
    			
    			List<ProductVO> convertedVOList = convertDbCodesToDescriptions(invalidProductVOList);
    			Workbook errorFile = createProductMassEditSpreadsheet(convertedVOList,true);
    			
    			String fileName = getNoExtFileName(headerVO.getFileName())+ "_" + headerVO.getMassEditHeaderId()+".xlsx";
    			
    			FileOutputStream  file= new FileOutputStream(new File(masseditDirectory,fileName));
    			errorFile.write(file);
    			headerVO.setFileStatus("Complete");
    			
    			productMassEditDAO.updateMassEditHeader(conn, headerVO);
    		}
    		else
    		{
    			headerVO.setFileStatus("Complete");
    			productMassEditDAO.updateMassEditHeader(conn, headerVO);
    		}    		
    	}
    }
    
    private void insertMassEditDetail(Connection conn,Long massEditHeaderId,ProductVO pvo) throws Exception
    {
    	Date sysdate = new Date();
    	MassEditDetailVO detailVO = null;
    	
		if (StringUtils.isNotEmpty(pvo.getMassEditMasterSKU()) && pvo.getMassEditMasterSKU().length() > 10)
			pvo.setMassEditMasterSKU(pvo.getMassEditMasterSKU().substring(1, 10));
		else if (StringUtils.isNotEmpty(pvo.getProductId())
				&& pvo.getProductId().length() > 10)
			pvo.setProductId(pvo.getProductId().substring(1, 10));

		if(StringUtils.isNotEmpty(pvo.getMassEditMasterSKU()))
			{
				detailVO = new MassEditDetailVO(
	        			massEditHeaderId,
	    				pvo.getMassEditMasterSKU(),pvo.getProductId(),null,
	    				pvo.getMassEditExceptionMsg() == null ? "Success" : "Failure",
	    				pvo.getMassEditExceptionMsg() == null ? null : pvo.getMassEditExceptionMsg(),
	    				pvo.getCreatedBy(),pvo.getUpdatedBy());  
			}
			else if (StringUtils.isNotEmpty(pvo.getProductId()))
			{
			 	detailVO = new MassEditDetailVO(
	        			massEditHeaderId,
	    				null,null,pvo.getProductId(),
	    				pvo.getMassEditExceptionMsg() == null ? "Success" : "Failure",
	    				pvo.getMassEditExceptionMsg() == null ? null : pvo.getMassEditExceptionMsg(),
	    				pvo.getCreatedBy(),pvo.getUpdatedBy());   
			}	
			else
			{
			 	detailVO = new MassEditDetailVO(
	        			massEditHeaderId,
	        			pvo.getMassEditMasterSKU(),pvo.getProductId(),pvo.getProductId(),
	    				pvo.getMassEditExceptionMsg() == null ? "Success" : "Failure",
	    				pvo.getMassEditExceptionMsg() == null ? null : pvo.getMassEditExceptionMsg(),
	    				pvo.getCreatedBy(),pvo.getUpdatedBy());  
			}
			productMassEditDAO.insertMassEditDetail(conn, detailVO);    				
   	
    }

    private List<ProductVO> getProductVOListFromSpreadSheet(Connection conn,String user, InputStream in) throws Exception
    {
    	logger.debug("Entered the getProductVOListFromSpreadSheet");
		List<ProductVO> productVOList = null;
		
		XSSFWorkbook massEditWorkbook = new XSSFWorkbook(in);	
		String[] errorParms = new String[2];
		
		if (massEditWorkbook != null) {
			productVOList = new ArrayList<ProductVO>();

			// Get the first sheet from workbook
			XSSFSheet mySheet = massEditWorkbook.getSheetAt(0);
			
			// Check to see if the user is actually uploading a PDB Mass Edit spreadsheet
			if (!isValidPDBMassEditSpreadsheet(mySheet, MAX_NUM_SPREADSHEET_CELLS) )  
				throw new Exception("INVALID_SPREADSHEET");
			
			// Skip the 2 header rows and iterate through the cells.
			Iterator<Row> rowIter = mySheet.rowIterator();
			rowIter.next();
			rowIter.next();
 
			while (rowIter.hasNext())
			{
				XSSFRow row = (XSSFRow) rowIter.next();
				String masterSku = null;
				String productId = null;
				
				if (row.getCell(0,Row.CREATE_NULL_AS_BLANK).getCellType() == XSSFCell.CELL_TYPE_NUMERIC) 
						masterSku = Double.toString(row.getCell(0,Row.CREATE_NULL_AS_BLANK).getNumericCellValue());
					else
						masterSku = row.getCell(0,Row.CREATE_NULL_AS_BLANK).getStringCellValue();


				if (row.getCell(1,Row.CREATE_NULL_AS_BLANK).getCellType() == XSSFCell.CELL_TYPE_NUMERIC) 
						productId = Double.toString(row.getCell(1,Row.CREATE_NULL_AS_BLANK).getNumericCellValue());
				else
						productId = row.getCell(1,Row.CREATE_NULL_AS_BLANK).getStringCellValue();

						
				
				ProductVO pvo = null;
				boolean productIdGrtrChars = false;
				StringBuilder errorMsgs = new StringBuilder();
				
				//Fields which are not updated to DB even though it comes from excel
				String productType = (row.getCell(8,Row.CREATE_NULL_AS_BLANK)).getStringCellValue();
						
	    		
				if (StringUtils.isNotEmpty(productId))
				{
					
					//Pull the db info for the productId and set the values with what in spreadsheet only for fields which are not locked.	
					if(productId.length() > 10 )
					{
						productIdGrtrChars = true;
						productId = productId.substring(0,10);
					}
					
					pvo =  getProduct(conn, productId.trim().toUpperCase(), null);		
					
					if(pvo != null)
			    	{
			    		if(productIdGrtrChars)
			    		{	    			
			    			pvo.setProductId(productId);
			    			errorParms[0] = "Product ID " + "("   + productId + ")"; 
			    			errorParms[1] = "10";
			    			errorMsgs.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_INVALID_FIELD_LENGTH, errorParms));
			    		}
			    		else
			    		{			    			
			    			//check if productname is missing from in db that mean product does not exist
			    			if(StringUtils.isEmpty(pvo.getProductName()))
			    			{
			    				pvo.setProductId(productId);
			    				errorParms[0] = "Product ID";
			    				errorParms[1] = "";
			    				errorMsgs.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_INVALID_FIELD, errorParms));
			    			}
			    		}
			    	}
				}
				else
				{
					pvo = new ProductVO();
					pvo.setProductId(productId);	
				}
			    	
					pvo.setMassEditMasterSKU(masterSku);
					pvo.setProductName((row.getCell(2, Row.CREATE_NULL_AS_BLANK)).getStringCellValue());
					
					//Novator ID
					pvo.setNovatorId(row.getCell(3,Row.CREATE_NULL_AS_BLANK).getStringCellValue());
					
					
					pvo.setNovatorName((row.getCell(4, Row.CREATE_NULL_AS_BLANK)).getStringCellValue());
					
					/*
					 * DI-32: PDB Mass Edit - Add new fields that are coming from West.
					 * Populate ProductVO object with properties: pQuad product ID.
					 */
					pvo.setPquadProductID((row.getCell(5, Row.CREATE_NULL_AS_BLANK)).getStringCellValue());
					
					//florist reference number
					if (row.getCell(6,Row.CREATE_NULL_AS_BLANK).getCellType() == XSSFCell.CELL_TYPE_NUMERIC) 
						pvo.setFloristReferenceNumber(Double.toString(row.getCell(6,Row.CREATE_NULL_AS_BLANK).getNumericCellValue()));
					else
						pvo.setFloristReferenceNumber((row.getCell(6,Row.CREATE_NULL_AS_BLANK)).getStringCellValue());
					
					
					pvo.setCategory((row.getCell(7, Row.CREATE_NULL_AS_BLANK)).getStringCellValue());		
					pvo.setProductType(productType);
					pvo.setProductSubType((row.getCell(9,Row.CREATE_NULL_AS_BLANK)).getStringCellValue());
					pvo.setLongDescription((row.getCell(10,Row.CREATE_NULL_AS_BLANK)).getStringCellValue());
					/*
					 * DI-32: PDB Mass Edit - Add new fields that are coming from West.
					 * Populate ProductVO object with properties: MercuryDescription.
					 */
					pvo.setMercuryDescription((row.getCell(11,Row.CREATE_NULL_AS_BLANK)).getStringCellValue());
					
					String spreadSheetstatus = (row.getCell(12, Row.CREATE_NULL_AS_BLANK)).getStringCellValue();
					//Spread sheet status.			
					pvo.setMassEditProductStatus(spreadSheetstatus);
					
					pvo.setRecipe((row.getCell(14, Row.CREATE_NULL_AS_BLANK)).getStringCellValue());
					pvo.setDeluxeRecipe((row.getCell(15,Row.CREATE_NULL_AS_BLANK)).getStringCellValue());
					pvo.setPremiumRecipe((row.getCell(16,Row.CREATE_NULL_AS_BLANK)).getStringCellValue());
					if (row.getCell(17, Row.CREATE_NULL_AS_BLANK).getCellType() == XSSFCell.CELL_TYPE_NUMERIC) {
						pvo.setDimWeight(Double.toString(row.getCell(15, Row.CREATE_NULL_AS_BLANK).getNumericCellValue()));
					}
					else {
						pvo.setDimWeight((row.getCell(17, Row.CREATE_NULL_AS_BLANK)).getStringCellValue());
					}
					// If the user somehow enters non-numeric data into this cell, set the 
				
					if (row.getCell(18, Row.CREATE_NULL_AS_BLANK).getCellType() == XSSFCell.CELL_TYPE_NUMERIC) {
						pvo.setStandardPrice(new Float(row.getCell(18, Row.CREATE_NULL_AS_BLANK).getNumericCellValue()).floatValue());
					}
					else if (row.getCell(18, Row.CREATE_NULL_AS_BLANK).getCellType() == XSSFCell.CELL_TYPE_STRING) {
						pvo.setStandardPrice(new Float(row.getCell(18, Row.CREATE_NULL_AS_BLANK).getStringCellValue()));
					}
					else if (row.getCell(18, Row.CREATE_NULL_AS_BLANK).getCellType() == XSSFCell.CELL_TYPE_BLANK) {
						errorParms[0] = "Standard Price";
						errorParms[1] = "Standard Price";
						errorMsgs.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_PRICE_BLANK_NOT_ALLOWED, errorParms));
					}
					else {
						errorParms[0] = "Standard Price";
						errorParms[1] = "Standard Price";
						errorMsgs.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_IS_NUMERIC_AND_DECIMAL, errorParms));
					}
					
					if (row.getCell(19, Row.CREATE_NULL_AS_BLANK).getCellType() == XSSFCell.CELL_TYPE_NUMERIC) {
						pvo.setDeluxePrice(new Float(row.getCell(19, Row.CREATE_NULL_AS_BLANK).getNumericCellValue()).floatValue());
					}
					else if (row.getCell(19, Row.CREATE_NULL_AS_BLANK).getCellType() == XSSFCell.CELL_TYPE_STRING) {
						pvo.setDeluxePrice(new Float(row.getCell(19, Row.CREATE_NULL_AS_BLANK).getStringCellValue()));
					}
					else if (row.getCell(19, Row.CREATE_NULL_AS_BLANK).getCellType() == XSSFCell.CELL_TYPE_BLANK) {
						errorParms[0] = "Deluxe Price";
						errorParms[1] = "Deluxe Price";
						errorMsgs.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_PRICE_BLANK_NOT_ALLOWED, errorParms));			
					}
					else {
						errorParms[0] = "Deluxe Price";
						errorParms[1] = "Deluxe Price";
						errorMsgs.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_IS_NUMERIC_AND_DECIMAL, errorParms));
					}
					if (row.getCell(20, Row.CREATE_NULL_AS_BLANK).getCellType() == XSSFCell.CELL_TYPE_NUMERIC) {
						pvo.setPremiumPrice(new Float(row.getCell(20, Row.CREATE_NULL_AS_BLANK).getNumericCellValue()).floatValue());
					}
					else if (row.getCell(20, Row.CREATE_NULL_AS_BLANK).getCellType() == XSSFCell.CELL_TYPE_STRING) {
						pvo.setPremiumPrice(new Float(row.getCell(20, Row.CREATE_NULL_AS_BLANK).getStringCellValue()));
					}
					else if (row.getCell(20, Row.CREATE_NULL_AS_BLANK).getCellType() == XSSFCell.CELL_TYPE_BLANK) {
						errorParms[0] = "Premium Price";
						errorParms[1] = "Premium Price";
						errorMsgs.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_PRICE_BLANK_NOT_ALLOWED, errorParms));				
					}
					else {
						errorParms[0] = "Premium Price";
						errorParms[1] = "Premium Price";
						errorMsgs.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_IS_NUMERIC_AND_DECIMAL, errorParms));
					}
					
					//Novator update flag
					String sendToNovator = (row.getCell(21,Row.CREATE_NULL_AS_BLANK)).getStringCellValue();
					if(StringUtils.isEmpty(sendToNovator))
					{
						errorParms[0] = "Novator Update";
						errorParms[1] = "";
						errorMsgs.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_REQUIRED_FIELD, errorParms));
					}
					else if( !(sendToNovator.equalsIgnoreCase("Yes") || sendToNovator.equalsIgnoreCase("No")))
						{
							errorParms[0] = "Novator Update";
							errorParms[1] = "";
							errorMsgs.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_INVALID_FIELD, errorParms));
						}
					else
					{
						NovatorFeedUtil feedUtil = new NovatorFeedUtil();
				    	List<String> novatorEnvKeys = feedUtil.getNovatorEnvironmentsAllowedAndChecked();				    	
				    	if(novatorEnvKeys != null && novatorEnvKeys.size() > 0 )
				    	{
				    		for (String key : novatorEnvKeys)
				    		{
				    			if(key.equalsIgnoreCase(NovatorFeedUtil.CONTENT))	
				    			{
				    				if(sendToNovator.equalsIgnoreCase("Yes"))
				    					pvo.setSentToNovatorContent(true);
				    				else
				    					pvo.setSentToNovatorContent(false);
				    			}
				    			else if (key.equalsIgnoreCase(NovatorFeedUtil.TEST)){
				    				if(sendToNovator.equalsIgnoreCase("Yes"))
				    					pvo.setSentToNovatorTest(true);
				    				else
				    					pvo.setSentToNovatorTest(false);
				    			}
				    			else if (key.equalsIgnoreCase(NovatorFeedUtil.UAT))
				    			{
				    				if(sendToNovator.equalsIgnoreCase("Yes"))
				    					pvo.setSentToNovatorUAT(true);
				    				else
				    					pvo.setSentToNovatorUAT(false);
				    			}
				    			else if (key.equalsIgnoreCase(NovatorFeedUtil.PRODUCTION))
				    			{
				    				if(sendToNovator.equalsIgnoreCase("Yes"))
				    					pvo.setSentToNovatorProd(true);
				    				else
				    					pvo.setSentToNovatorProd(false);
				    			}
				    		}
				    	}	
					}	
					if (errorMsgs.length() > 0)
						pvo.setMassEditExceptionMsg(errorMsgs.toString()); 
					 
					 
					//Comments
					String comments = (row.getCell(22,Row.CREATE_NULL_AS_BLANK)).getStringCellValue();
					pvo.setMassEditSpreadSheetGeneralComments(comments);	
					
					if(pvo.getGeneralComments() != null)
					{	
						StringBuffer commentsBuffer = new StringBuffer();						
						commentsBuffer.append(pvo.getGeneralComments());
						if(!StringUtils.isBlank(comments))
						{	
							commentsBuffer.append("\r");
							commentsBuffer.append(comments);
						}							
						pvo.setGeneralComments(commentsBuffer.toString());	
					}					
					else
					{
						if(!StringUtils.isBlank(comments))
						 pvo.setGeneralComments(comments);	
					}
								
			
					pvo.setCreatedBy(user);
					pvo.setUpdatedBy(user);

					productVOList.add(pvo);
				}
		}
		//Convert all the desc to codes 
		 List<ProductVO> convertedVOList = convertDescriptionsToDbCodes(productVOList);
		
		return convertedVOList;
	}
    
    /**
     * Checks to make sure that the spreadsheet being uploaded was actually 
     * created by the PDB Mass Edit code.
     * 
     * @param sheet
     * @return
     */
    private boolean isValidPDBMassEditSpreadsheet(XSSFSheet sheet, int maxNumCells) {
    	try {
    		/* do some checks to make sure user is uploading correct sheet
    		 *  sheetname=Products
    		 *  isSheetLocked=true
    		 *  isProtected=true
    		 *  row0/cell0 value=Product List
    		 *  row1/cell6 value=Category
    		 *  row2/cell20 length=10240
    		 */
    		/*
    		 * DI-32: PDB Mass Edit - Add new fields that are coming from West.
    		 * Added 2 fields to the spreadsheet, so the maxNumCells is now 22
    		 */
    		if (maxNumCells != 22)
    			return false;
    		if (!"Products".equals(sheet.getSheetName() ) )
    			return false;
		    if (!sheet.isSheetLocked()) 
		    	return false;
	        if (!sheet.getProtect()) 
	        	return false;
	        if (!"Product List".equals(sheet.getRow(0).getCell(0).getStringCellValue()) )
	            return false;
	        if (!"Category".equals(sheet.getRow(1).getCell(7).getStringCellValue()) )
	        	return false;
		    if (sheet.getColumnWidth(22) != 10240) 
		    	return false;
		    if (sheet.getRow(2).getCell(0).getCellType() != XSSFCell.CELL_TYPE_STRING)
		    	return false;
		    if (sheet.getRow(2).getCell(1).getCellType() != XSSFCell.CELL_TYPE_STRING)
		    	return false;
		    return true;
    	}
    	catch(Exception e) {
    		e.printStackTrace();
    		return false;
    	}
    	 
    }

    
    /**
     * This method will validate each product entered on the spreadsheet.
     * 
     * @param conn
     * @param productVo
     * @return String - comma delimited String containing the error messages 
     */
    public String validateProductMassEdit(Connection conn, ProductVO productVO) 
    	throws PDBSystemException, PDBApplicationException
    {
    	String validationErrors = null;
    	StringBuilder errors = new StringBuilder();
    	if (productVO == null)
    		return null;
    	String errorParms[] = new String[2];
    	HashMap<String, String> categoryMap =  createCodeDescriptionMap(getLookupSVC().getCategoryList());	
    	HashMap<String, String> productTypeMap = createCodeDescriptionMap(getLookupSVC().getTypesList());	
    	List<ProductSubTypeVO> productSubTypeList = getLookupSVC().getSubTypesList();	
     	
    	// get the spreadsheet delivery type
    	String deliveryType = getSpreadSheetDeliveryType(productVO.isShipMethodFlorist(), productVO.isShipMethodCarrier());
    	 /**
    	  * check for exception message to see if product id exists or not
    	  */
    	if(StringUtils.isNotEmpty(productVO.getMassEditExceptionMsg()))
    		errors.append(productVO.getMassEditExceptionMsg());
    	 
       /* 
    	* Validate Master SKU
    	* 
    	* If this field is entered, the length must be <=10
        * 
        * This field should not be able to be modified by the user, unless they
        * screw around and create their own spreadsheet, so just checking to make
        * sure it is still there.
        */
    	if (StringUtils.isNotEmpty(productVO.getMassEditMasterSKU())) {
    		if (productVO.getMassEditMasterSKU().trim().length() > 10) 
    		{
    			errorParms[0] = "Master SKU " + "(" + productVO.getMassEditMasterSKU() + ")";
    			errorParms[1] = "10";
        	    errors.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_INVALID_FIELD_LENGTH, errorParms));
    		}
    		else {
    			UpsellMasterVO upsellVO = upsellSVC.getMasterSKU(productVO.getMassEditMasterSKU().trim());
    	   		if (upsellVO == null || upsellVO.getMasterID() == null) {
    	   			errorParms[0] = "Master SKU";
        			errorParms[1] = "";
            	    errors.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_INVALID_FIELD, errorParms));
           		}
    		}
    	  }
    	   	
       /* 
    	* Validate Product ID
    	* 
    	* This is required field and length must be <=10
        * 
        * This field should not be able to be modified by the user, unless they
        * screw around and create their own spreadsheet, so just checking to make
        * sure it is still there.
        */
    	boolean isValidProductId = true;
    	if (StringUtils.isEmpty(productVO.getProductId())) {
    		errorParms[0] = "Product ID ";
			errorParms[1] = "";
    	    errors.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_REQUIRED_FIELD, errorParms));
		    isValidProductId = false;
    	}	
        else if (productVO.getProductId().trim().length() > 10) {
        	errorParms[0] = "Product ID " + "("   + productVO.getProductId() + ")"; 
			errorParms[1] = "10";
    	    errors.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_INVALID_FIELD_LENGTH, errorParms));
		    isValidProductId = false;
        }
         	
       /* 
    	* Validate Product Name
    	* 
    	* This is required field and length must be <=25 
        * It can contain alphanumeric and special characters
        */
    	if (StringUtils.isEmpty(productVO.getProductName())) {
    		errorParms[0] = "Product Name";
			errorParms[1] = "";
    	    errors.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_REQUIRED_FIELD, errorParms));
	
    	}	
        else if (productVO.getProductName().trim().length() > 25) {
        	errorParms[0] = "Product Name";
			errorParms[1] = "25";
    	    errors.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_INVALID_FIELD_LENGTH, errorParms));
	
        }
    	
       /* 
    	* Validate Novator ID
    	* 
    	* This is required field and length must be <=10
    	* It can contain alphanumeric and hyphen characters
        */
    	if (StringUtils.isEmpty(productVO.getNovatorId())) {
    		errorParms[0] = "Novator ID";
			errorParms[1] = "";
    	    errors.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_REQUIRED_FIELD, errorParms));
	
    	}	
        else if (productVO.getNovatorId().trim().length() > 10) {
        	errorParms[0] = "Novator ID";
			errorParms[1] = "10";
    	    errors.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_INVALID_FIELD_LENGTH, errorParms));
	
        }
       	else {
       		//validate for alphanumeric and hyphen data
       		if (!productVO.getNovatorId().matches("[a-zA-Z0-9-]+")) {
       			errorParms[0] = "Novator ID";
    			errorParms[1] = "";
        	    errors.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_IS_ALPHANUMERIC_AND_HYPHENS, errorParms));
    	
       		}
       	}
    	
       /* 
    	* Validate Novator Name
    	* 
    	* This is required field and length must be <=100 
        * It can contain alphanumeric and special characters
        */
    	if (StringUtils.isEmpty(productVO.getNovatorName())) {
    		errorParms[0] = "Novator Name";
			errorParms[1] = "";
    	    errors.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_REQUIRED_FIELD, errorParms));
	  	}	
        else if (productVO.getNovatorName().trim().length() > 100) {
        	errorParms[0] = "Novator Name";
			errorParms[1] = "100";
    	    errors.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_INVALID_FIELD_LENGTH, errorParms));
	    }
    	
       /* 
    	* Validate Florist Reference Number
    	* 
    	* This is required field and length must be <=62
    	* It can contain alphanumeric and hyphen characters
        */
    	if ("SERVICES".equalsIgnoreCase(productVO.getProductType()) ) {
    		if (StringUtils.isNotEmpty(productVO.getFloristReferenceNumber())){
    			errorParms[0] = "Florist Reference Number";
    			errorParms[1] = "";
    			errors.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_FOR_SERVICES_NOT_ALLOWED, errorParms));
		
    		}
    	}
    	else {
    		if (StringUtils.isEmpty(productVO.getFloristReferenceNumber())) {
    			errorParms[0] = "Florist Reference Number";
    			errorParms[1] = "";
    			errors.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_REQUIRED_FIELD, errorParms));
    		}	
    		else if (productVO.getFloristReferenceNumber().trim().length() > 30) {
    			errorParms[0] = "Florist Reference Number";
    			errorParms[1] = "30";
    			errors.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_INVALID_FIELD_LENGTH, errorParms));
    		}
       	}
    	
       /* 
    	* Validate Category
    	* 
    	* This is an optional field.
    	* If entered, make sure it is a valid category 
        */
		if (StringUtils.isNotEmpty(productVO.getCategory())) {
			if (productVO.getCategory().equalsIgnoreCase("None")) {
				productVO.setCategory("NONE");
			}

			if (!categoryMap.containsKey(productVO.getCategory())) {
				errorParms[0] = "Category";
				errorParms[1] = "";
				errors.append(ErrorCodeStore.getInstance().findValue(
						ProductMaintConstants.PDB_MASS_EDIT_INVALID_FIELD,
						errorParms));
			}
		}
    	      
       /* 
    	* Validate Product Type
    	* 
    	* The user should not be able to change this field because it is locked on
    	* the spreadsheet.  Make sure it is still there and is a valid product type
        */
    	if (StringUtils.isEmpty(productVO.getProductType())) {
     		errorParms[0] = "Product Type";
			errorParms[1] = "";
    	    errors.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_REQUIRED_FIELD, errorParms));
		}
    	else if ("NONE".equalsIgnoreCase(productVO.getProductType().trim())) {
    		errorParms[0] = "Product Type";
			errorParms[1] = "";
    	    errors.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_INVALID_FIELD, errorParms));
		}
    	else if (!productTypeMap.containsKey(productVO.getProductType().trim())) {
    		errorParms[0] = "Product Type";
			errorParms[1] = "";
    	    errors.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_INVALID_FIELD, errorParms));
		}
    	
       /* 
    	* Validate Product Sub Type
    	* 
    	* This field is optional.  If it is entered, make sure it is a valid sub-type
    	* for the product type.  If product type is empty though, skip this check
        */
    	 if (StringUtils.isNotEmpty(productVO.getProductType()) && StringUtils.isNotEmpty(productVO.getProductSubType())) {
    		boolean foundIt = false;
    		for (ProductSubTypeVO pstvo : productSubTypeList) {
    			if (productVO.getProductType().equals(pstvo.getTypeId()) &&
    				productVO.getProductSubType().equals(pstvo.getId())	) {
    				foundIt = true;
    				break;
    			}    				
    		}
    		if (!foundIt) {
    			errorParms[0] = "Sub-Type";
    			errorParms[1] = "";
        	    errors.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_INVALID_FIELD, errorParms));
    	
    		}	
		}
     	
       /* 
    	* Validate Description
    	* 
    	* This field is required and length must be <=1024
    	* It can contain alphanumeric and special characters
        */
    	if (StringUtils.isEmpty(productVO.getLongDescription())) {
     		errorParms[0] = "Description";
			errorParms[1] = "";
    	    errors.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_REQUIRED_FIELD, errorParms));
     	}
    	else if (productVO.getLongDescription().length() > 1024) {
    		if (errors.length() > 0)
        		errors.append("; ");
    		errorParms[0] = "Description";
			errorParms[1] = "1024";
    	    errors.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_INVALID_FIELD_LENGTH, errorParms));
	
		}
    	
    	/**
    	 * Validate status
    	 * 
    	 * Field is Required, but field is boolean in ProductVO object, so it is checked when
    	 * data is read from the spreadsheet and placed into the object. 
    	 * 
    	 * If Delivery Type = "Dropship or Florist/Dropship" and Status is changed from U to A , validate with
    	 * SKU-IT.  If no inventory is available, flag it as an error.
    	 * 
    	 */
    	 if(StringUtils.isEmpty(productVO.getMassEditProductStatus())) {
    		errorParms[0] = "Availability";
			errorParms[1] = "";
    	    errors.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_REQUIRED_FIELD, errorParms));
    
    	 }
    	 else{ 	
    		if( productVO.getMassEditProductStatus().equalsIgnoreCase("Available"))
    			productVO.setStatus(true);
    		else if (productVO.getMassEditProductStatus().equalsIgnoreCase("Unavailable"))
    			productVO.setStatus(false);
    		else{
    				errorParms[0] = "Availability";
        			errorParms[1] = "";
            	    errors.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_INVALID_FIELD, errorParms));
    			}            		
    	}
    	 
    	 // SKU-IT check
    	 if (("Dropship".equalsIgnoreCase(deliveryType) || "Florist/Dropship".equalsIgnoreCase(deliveryType))&& isValidProductId ) {
    	  	// get the current Product status
    		ProductVO dbProductVO = productSVC.getProduct(productVO.getProductId());
    		if(dbProductVO.getShippingSystem() != null && !dbProductVO.getShippingSystem().equalsIgnoreCase("FTP")) // fix for 19449
    		{
    			String dbProductStatus = dbProductVO.isStatus() == true ? "A" : "U";
    			String spreadSheetProductStatus = productVO.isStatus() == true ? "A" : "U";
    			if ("U".equals(dbProductStatus) && "A".equals(spreadSheetProductStatus)) {
    				if (!productSVC.hasInventoryRecord(productVO.getProductId()) ) {
    					errorParms[0] = "";
    					errorParms[1] = "";
    					errors.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_NO_INVENTORY_ADDED_FOR_PRODUCT, errorParms));
    				}
    			}
    		}
    	}
    	 
    	/*
    	 * New check (as of 3/18/2014) to check for vendors available on product.
    	 * If status = "A" (true) and no vendors are available on the product (0) and
    	 * ship_method_carrier = 'Y' (deliveryType = Dropship or Florist/Dropship),
    	 * validation error message is "The product is available and no vendors are available"
    	 * 
    	 */
    	if (("Dropship".equalsIgnoreCase(deliveryType) || "Florist/Dropship".equalsIgnoreCase(deliveryType))&& isValidProductId ) {
    		String spreadSheetProductStatus = productVO.isStatus() == true ? "A" : "U";
    		if ("A".equals(spreadSheetProductStatus)) {
    			BigDecimal vendorCount = getVendorsAvailableForProduct(conn, productVO.getProductId());
    			if (vendorCount.intValue() == 0) {
    				errorParms[0] = "";
    				errorParms[1] = "";
    				errors.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_NO_VENDORS_AVAILABLE, errorParms));
    			}
    		}
    	} 
    	 
    	 /* 
     	* Validate Floral Recipes
     	* 
     	* If Product Type = FLORAL, SDG, or SDFC and data is entered for any of the recipes, it must not be > 255.
     	* 
     	* Else if Product Type = FRECUT, SPEGFT, or SERVICES and one of the recipes is entered, error is
     	* "Field name (ie Floral Standard Recipe) only applies to floral product types
     	* 
         */ 
     	if ("FLORAL".equalsIgnoreCase(productVO.getProductType()) || 
     		"SDG".equalsIgnoreCase(productVO.getProductType()) || 
     		"SDFC".equalsIgnoreCase(productVO.getProductType())) {
     		
     		 if (StringUtils.isNotEmpty(productVO.getRecipe()) && 
     			 productVO.getRecipe().length() > 255 ) {
     			errorParms[0] = "Floral Standard Recipe";
    			errorParms[1] = "255";
        	    errors.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_INVALID_FIELD_LENGTH, errorParms));
    	
     		 }
     		 if (StringUtils.isNotEmpty(productVO.getDeluxeRecipe()) && 
         			 productVO.getDeluxeRecipe().length() > 255 ) {
         			errorParms[0] = "Floral Deluxe Recipe";
        			errorParms[1] = "255";
            	    errors.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_INVALID_FIELD_LENGTH, errorParms));
        	
         	 }
     		 if (StringUtils.isNotEmpty(productVO.getPremiumRecipe()) && 
         			 productVO.getPremiumRecipe().length() > 255 ) {
         			errorParms[0] = "Floral Premium Recipe";
        			errorParms[1] = "255";
            	    errors.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_INVALID_FIELD_LENGTH, errorParms));
        	
         	 }
     	}
     	else if ("FRECUT".equalsIgnoreCase(productVO.getProductType()) || 
         		"SPEGFT".equalsIgnoreCase(productVO.getProductType()) || 
         		"SERVICES".equalsIgnoreCase(productVO.getProductType())) {
         		
         		 if (StringUtils.isNotEmpty(productVO.getRecipe())) {
         			errorParms[0] = "Floral Standard Recipe";
        			errorParms[1] = "";
            	    errors.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_ONLY_APPLIES_TO_FLORAL, errorParms));
        	
         		 }
         		 if (StringUtils.isNotEmpty(productVO.getDeluxeRecipe())) {
             			errorParms[0] = "Floral Deluxe Recipe";
            			errorParms[1] = "";
                	    errors.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_ONLY_APPLIES_TO_FLORAL, errorParms));
            	
             	 }
         		 if (StringUtils.isNotEmpty(productVO.getPremiumRecipe()) ) {
             			errorParms[0] = "Floral Premium Recipe";
            			errorParms[1] = "";
                	    errors.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_ONLY_APPLIES_TO_FLORAL, errorParms));
            	
             	 }
         	} 
     		 
       /* 
    	* Validate Dim/Weight
    	* 
    	* if product type = FRECUT, SDFC, SDG, or SPEGFT
    	*   1.  It is required.
    	*   2.  It must be <= 10.
    	*   3.  It only allows numbers and a decimal point.
    	* else if product type = FLORAL or SERVICES
    	*   1.  It should be empty
        */
    	if ("FRECUT".equalsIgnoreCase(productVO.getProductType()) ||
    		"SDFC".equalsIgnoreCase(productVO.getProductType()) ||	
    		"SDG".equalsIgnoreCase(productVO.getProductType()) ||
    		"SPEGFT".equalsIgnoreCase(productVO.getProductType()) ) {
    		if (StringUtils.isEmpty(productVO.getDimWeight())) {
         		errorParms[0] = "";
    			errorParms[1] = "";
        	    errors.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_DIM_WEIGHT_REQUIRED_FOR_DROPSHIP, errorParms));
    	
        	}
        	else if (productVO.getDimWeight().trim().length() > 10) {
        		errorParms[0] = "Dropship Dim/Weight";
    			errorParms[1] = "10";
        	    errors.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_INVALID_FIELD_LENGTH, errorParms));
    	
        	}
    	}
    	else if ("SERVICES".equalsIgnoreCase(productVO.getProductType()) ||
        		 "FLORAL".equalsIgnoreCase(productVO.getProductType()) ) {
    		 if (StringUtils.isNotEmpty(productVO.getDimWeight())) {
         		errorParms[0] = "";
    			errorParms[1] = "";
        	    errors.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_DIM_WEIGHT_ONLY_REQUIRED_FOR_DROPSHIP, errorParms));
    	
        	 }
    	} 
    	
    	/** 
    	 * validate the comments. If > 1024 throw the error message
    	 */
    	
    	if(StringUtils.isNotEmpty(productVO.getGeneralComments()))
    	{
    		if(productVO.getGeneralComments().trim().length() > 1024)
    		{
    			errorParms[0] = "";
    			errorParms[1] = "";
        	    errors.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_COMMENTS_INVALID_LENGTH, errorParms));
    		}
    	}
    		
    	
    	validatePrices(productVO, errors);
    	
    	/*
    	 * DI-32: PDB Mass Edit - Add new fields that are coming from West.
    	 * Validate new properties included in the sheet: pQuad product ID and Mercury Description.
    	 */
    	boolean isPQuadProdIdInvalid = false;
    	
    	if(StringUtils.isNotEmpty(productVO.getPquadProductID()))
    	{
	    	if(productVO.getPquadProductID().trim().length() > 32)
	    	{
	    		errorParms[0] = "PQuad product ID";
				errorParms[1] = "";
				errors.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_PQUAD_PRODUCT_ID_INVALID_FIELD, errorParms));
				isPQuadProdIdInvalid = true;
	    	}
	    	
	    	if(!productVO.getPquadProductID().matches("[0-9]+") && !isPQuadProdIdInvalid)
	    	{
	    		errorParms[0] = "PQuad product ID";
				errorParms[1] = "";
	    		errors.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_PQUAD_PRODUCT_ID_INVALID_FIELD, errorParms));
	    	}
    	}	
    	
    	if ("FRECUT".equalsIgnoreCase(productVO.getProductType()) || "SPEGFT".equalsIgnoreCase(productVO.getProductType())) 
    	{
    		if (StringUtils.isEmpty(productVO.getMercuryDescription())) 
    		{
         		errorParms[0] = "Mercury Description";
    			errorParms[1] = "";
        	    errors.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_REQUIRED_FIELD, errorParms));
        	}
    	}
    	
    	if(StringUtils.isNotEmpty(productVO.getMercuryDescription()))
    	{
	    	if(productVO.getMercuryDescription().trim().length() > 62)
	    	{
	    		errorParms[0] = "Mercury Description";
				errorParms[1] = "";
				errors.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_MERCURY_DESCRIPTION_INVALID_FIELD, errorParms));
	    	}
    	}
    	    	
        validationErrors = errors.toString();
    	return validationErrors;
    }
    
    /**
	 * Gets the records from the MASS_EDIT_HEADER and MASS_EDIT_DETAIL to show the upload
	 * status table on the PDB Mass Edit screen.
	 * 
	 * @param conn
	 * @return a HashMap containing the mass edit file directory path and a 
	 *         List of MassEditFileStatusVO objects.
     * @throws Exception 
	 */
    public HashMap<String, Object> getMassEditFileStatus(Connection conn) throws Exception {
       HashMap<String, Object> pdbHashMap = new HashMap<String, Object>();
       String pdbMassEditFileDirectory =  resourceProvider.getGlobalParameter("PDB_MASS_EDIT","PDB_MASS_EDIT_DIRECTORY");
       pdbHashMap.put("pdbMassEditFileDirectory", pdbMassEditFileDirectory);   
       List<MassEditFileStatusVO> pdbMassEditFileStatusList =  
    	   productMassEditDAO.getMassEditFileStatus(conn);
       pdbHashMap.put("pdbMassEditFileStatusList", pdbMassEditFileStatusList);
       return pdbHashMap;	
    } 
    
    /**
     * Creates a "Delivery Type" for the PDB Mass Edit spreadsheet based on the
     * values in isShipMethodFlorist and isShipMethodCarrier in the db.
     * 
     * @param isShipMethodFlorist
     * @param isShipMethodCarrier
     * @return
     */
    public String getSpreadSheetDeliveryType(boolean isShipMethodFlorist, boolean isShipMethodCarrier) {
    	if (isShipMethodFlorist && !isShipMethodCarrier )
    	    return "Florist";
    	else if (!isShipMethodFlorist && isShipMethodCarrier  ) 
    	    return "Dropship";
    	else if (isShipMethodFlorist && isShipMethodCarrier  ) 
    	    return "Florist/Dropship";
    	else
    	    return "None";
    	 
    }
    
    public String[] buildSubTypeTextArray(String productType) throws PDBSystemException {
    	 List<String> descList = new ArrayList<String>();
    	 List<ProductSubTypeVO> subTypeList = lookupSVC.getSubTypesList();
         for (ProductSubTypeVO vo: subTypeList) {
         	if (productType.equalsIgnoreCase(vo.getTypeId()) ) {
         	    if ("NONE".equalsIgnoreCase(vo.getId()) )	 {
         	    	descList.add("None");
         	    }
         	    else {
         	    	descList.add(vo.getDescription());
         	    }
         	}
         }
         String [] text = descList.toArray(new String[descList.size()]);
         return text;
    }
    
    /**
     * Validates the Standard Price, Deluxe Price, and Premium Price that were entered on the PDB Mass Edit 
     * spreadsheet.
     * 
     * If user somehow unlocks the spreadsheet and enters character data into these fields, it will be caught
     * in getProductVOListFromSpreadSheet() method.  Check the productVO exceptionMessage field for any errors
     * passed along.
     *    
     * else :   
     *    
     * 1. If all prices are <= 0, error is that Standard Price must be between .01 and 9999.99.
     * 2. If standard price = 0, error is that Standard Price must be between .01 and 9999.99.
     * 3. If standard price > 0, validate that the amount entered is between .01 and 9999.99.
     * 4. If deluxe or premium prices > 0, validate that the amount entered is between 0.00 and 9999.99.
     * 4. If Product Type = SERVICE, both deluxe and premium price must = 0. 
     * 
     * @param productVO
     * @param errors
     */
    public void validatePrices(ProductVO productVO, StringBuilder errors) {
     
    	// Check the productVO exceptionMessage field, looking for any price errors already found.
    	boolean isValidStandardPrice = true;
    	boolean isValidDeluxePrice = true;
    	boolean isValidPremiumPrice = true;
    	String[] errorParms = new String[2];
    	
    	if (StringUtils.isNotEmpty(productVO.getMassEditExceptionMsg())) {
    		if (productVO.getMassEditExceptionMsg().indexOf("Standard Price") >= 0)
    			isValidStandardPrice = false;
    		if (productVO.getMassEditExceptionMsg().indexOf("Deluxe Price") >= 0)
    			isValidDeluxePrice = false;
    		if (productVO.getMassEditExceptionMsg().indexOf("Premium Price") >= 0)
    			isValidPremiumPrice = false;
    	}
    	
    	// If no errors were found for the prices in the getProductVOListFromSpreadSheet(), and no price
    	// is entered for any price point, set error = Standard Price must be between .01 and 9999.99
    	if (isValidStandardPrice && isValidDeluxePrice && isValidPremiumPrice) {
    		if (productVO.getStandardPrice() <= 0 && productVO.getDeluxePrice() <= 0 && productVO.getPremiumPrice() <= 0 ) {
        		errorParms[0] = "Standard Price";
        		errorParms[1] = "";
    	 	    errors.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_IS_VALID_STANDARD_PRICE, errorParms));
    	 	    return;
    		}
    	}

     	// Validate Standard Price
    	if (isValidStandardPrice) {
    		// if Standard Price = 0 (error is Standard Price must be between .01 and 9999.99
    		if (productVO.getStandardPrice() == 0) {
	        	errorParms[0] = "Standard Price";
	        	errorParms[1] = "";
	            errors.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_IS_VALID_STANDARD_PRICE, errorParms));
	    	}
    		else {
    		// else make sure it is within the valid price range
    		   if (productVO.getStandardPrice() <  0.01F || productVO.getStandardPrice() >  9999.99F ) {
    	        	errorParms[0] = "Standard Price";
    	        	errorParms[1] = "";
    	            errors.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_IS_VALID_STANDARD_PRICE, errorParms));
    	  	   }	
    			 
    		}
    	} // end of Standard Price validation
    	
    	// Validate Deluxe Price
    	if (isValidDeluxePrice && productVO.getDeluxePrice() != 0) {
    		// Price is !=0, make sure it is within the valid range.    
    		if (productVO.getDeluxePrice() <  0.00F || productVO.getDeluxePrice() >  9999.99F ) {
				errorParms[0] = "Deluxe Price";
				errorParms[1] = "";
				errors.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_IS_VALID_DELUXE_OR_PREMIUM_PRICE, errorParms));
			}
    		else {
    			// it is in the valid range, but if product type = SERVICES it is not allowed
    			if ("SERVICES".equalsIgnoreCase(productVO.getProductType())) {
    				errorParms[0] = "Deluxe Price";
    				errorParms[1] = "";
    				errors.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_FOR_SERVICES_NOT_ALLOWED, errorParms));
    			}
			}
    			 
    	} // end of Deluxe Price Validation
    	
    	// Validate Premium Price
    	if (isValidPremiumPrice && productVO.getPremiumPrice() != 0) {
    		// Price is !=0, make sure it is within the valid range.    
    		if (productVO.getPremiumPrice() <  0.00F || productVO.getPremiumPrice() >  9999.99F ) {
				errorParms[0] = "Premium Price";
				errorParms[1] = "";
				errors.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_IS_VALID_DELUXE_OR_PREMIUM_PRICE, errorParms));
			}
    		else {
    			// it is in the valid range, but if product type = SERVICES it is not allowed
    			if ("SERVICES".equalsIgnoreCase(productVO.getProductType())) {
    				errorParms[0] = "Premium Price";
    				errorParms[1] = "";
    				errors.append(ErrorCodeStore.getInstance().findValue(ProductMaintConstants.PDB_MASS_EDIT_FOR_SERVICES_NOT_ALLOWED, errorParms));
    			}
			}
    			 
    	} // end of Premium Price Validation
    	
    }
    
    /**
     * Gets a count of the vendors available for a product (for PDB Mass Edit)
     * @param conn
     * @param productId
     * @return count returned from query
     * @throws PDBApplicationException
     * @throws PDBSystemException
     */
    public BigDecimal getVendorsAvailableForProduct(Connection conn, String productId)  
            throws PDBApplicationException, PDBSystemException {
     	return productDAO.getVendorsAvailableForProduct(conn, productId);
    }
    
    /**
     * Determine if the vender has a shipping account for the passed in partner
     * @param conn database connection
     * @param vendorId filter
     * @param partnerId filter
     * @return true if the account exist else false
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public boolean hasPartnerShipAccount(Connection conn, String vendorId, String partnerId) throws PDBSystemException, PDBApplicationException {
        return productDAO.hasPartnerShipAccount(conn,vendorId,partnerId);
    }

    public void setProductDAO(IProductDAO productDAO) {
        this.productDAO = productDAO;
    }
    
    public IProductDAO getProductDAO() {
        return productDAO;
    }
    
    public IProductMassEditDAO getProductMassEditDAO() {
        return productMassEditDAO;
    }
    
    public void setProductMassEditDAO(IProductMassEditDAO productMassEditDAO) {
        this.productMassEditDAO = productMassEditDAO;
    }
    public IResourceProvider getResourceProvider() {
		return resourceProvider;
	}

	public void setResourceProvider(IResourceProvider resourceProvider) {
		this.resourceProvider = resourceProvider;
	}

	public void setUpsellSVC(IUpsellSVCBusiness upsellSVC) {
        this.upsellSVC = upsellSVC;
    }

    public IUpsellSVCBusiness getUpsellSVC() {
        return upsellSVC;
    }
    
    public void setLookupSVC(ILookupSVCBusiness lookupSVC) {
        this.lookupSVC = lookupSVC;
    }

    public ILookupSVCBusiness getLookupSVC() {
        return lookupSVC;
    }
    
    /**
     * Returns search results based on the passed in search criteria
     * @param conn database connection
     * @return array of products based on filtered criteria
     * @exception String The exception description.
     */
    public List getPQuadProductList(Connection conn,String pquadproductId) throws PDBApplicationException,PDBSystemException 
    {
        return productDAO.getPQuadProductList( conn,pquadproductId);
    }

      
}
