package com.ftd.pdb.businessobject;

import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.feed.NovatorFeedProductUtil;
import com.ftd.osp.utilities.feed.vo.NovatorFeedResponseVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBSystemException;
import com.ftd.pdb.common.utilities.FTDUtil;
import com.ftd.pdb.common.utilities.NovatorTransmission;
import com.ftd.pdb.common.valobjs.UpsellDetailVO;
import com.ftd.pdb.common.valobjs.UpsellMasterVO;
import com.ftd.pdb.integration.dao.IUpsellDAO;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.naming.InitialContext;

import javax.servlet.http.HttpServletRequest;
import com.ftd.op.common.framework.util.CommonUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;


/**
 * This class is used to perform business logic for functions related to a upsell
 *
 * @author Ed Mueller
 **/
public class UpsellBOImpl extends PDBBusinessObject implements IUpsellBO
{

    protected static Logger logger = new Logger("com.ftd.pdb.businessobject.UpsellBOImpl");

    /*Spring managed resources*/
     IUpsellDAO upsellDAO;
    /*end Spring managed resources*/
    /**
    * Constructor for the LookupBOImpl object.  Sets the debugEnabled
    * variable and inits the logger category.
    */
    public UpsellBOImpl()
    {
        super("com.ftd.pdb.businessobject.UpsellBOImpl");
    }

   /**
    * Creates a Lookup BO
    */
    public void create()
    {
    }

    /**
    * Returns a string representation of this BO
    *
    * @return a string representation of this BO
    */
    public String toString()
    {
        return null;
    }

    /**
    * Checks to see if this object is internally equal to the parameter object
    * @param object1 object to be compared to this object
    */
    public boolean equals(java.lang.Object object1)
    {
        return false;
    }

    /**
    *   Saves this BO to persistent storage
    */
    public void save()
    {
    }

    /**
     * Get an upsell list
     * @param conn database connection
     * @return array of upsell skus
     * @exception String The exception description.
     */
    public List getUpsellList(Connection conn)
        throws PDBApplicationException, PDBSystemException
    {
        return upsellDAO.getUpsellList(conn);
    }

    /**
     * Insert or update Master SKU
     * @param conn database connection
     * @param masterVO record to save
     * @param novatorEnvKeys novator servers to send update to
     * @return UpsellMasterVO
     * @exception String The exception description.
     */
    public void updateMasterSKU(Connection conn, UpsellMasterVO masterVO,List<String> novatorEnvKeys) 
        throws PDBApplicationException, PDBSystemException
    {
        //update database
        upsellDAO.deleteMasterSKU(conn,masterVO);
        upsellDAO.insertMasterSKU(conn,masterVO);
        String  myBuysFeedEnabled  = "N" ;  
        try
        {
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        myBuysFeedEnabled  = configUtil.getFrpGlobalParm("MY_BUYS", "FEED_ENABLED_FLAG");
          
        }
        catch (Exception e)
        {
          logger.error("unable to get my buys feed from global param", e);
        }

        //Create new VO for Novator
        UpsellMasterVO novatorMasterVO = new UpsellMasterVO();
        novatorMasterVO.setCorporateSiteFlag(masterVO.isCorporateSiteFlag());              
        //novatorMasterVO.setMasterDescription("<![CDATA[" + FTDUtil.convertNulls((masterVO.getMasterDescription())) + "]]>");
        novatorMasterVO.setMasterDescription((String)FTDUtil.convertNulls((masterVO.getMasterDescription())));
        novatorMasterVO.setCorporateSiteFlag(masterVO.isCorporateSiteFlag());
        novatorMasterVO.setMasterStatus(masterVO.isMasterStatus());
        novatorMasterVO.setMasterID(masterVO.getMasterID());
        //novatorMasterVO.setMasterName( "<![CDATA[" + FTDUtil.convertNulls((masterVO.getMasterName())) + "]]>");
        novatorMasterVO.setMasterName((String)FTDUtil.convertNulls((masterVO.getMasterName())));
        novatorMasterVO.setMasterType(masterVO.getMasterType());
        novatorMasterVO.setRecipientSearchList(masterVO.getRecipientSearchList());
        novatorMasterVO.setSearchKeyList(masterVO.getSearchKeyList());
        novatorMasterVO.setSearchPriority(masterVO.getSearchPriority());
        novatorMasterVO.setCompanyList(masterVO.getCompanyList());
        novatorMasterVO.setGbbPopoverFlag(masterVO.getGbbPopoverFlag());
        novatorMasterVO.setGbbTitle(masterVO.getGbbTitle());
        novatorMasterVO.setGbbUpsellDetailId1(masterVO.getGbbUpsellDetailId1());
        novatorMasterVO.setGbbNameOverrideFlag1(masterVO.getGbbNameOverrideFlag1());
        novatorMasterVO.setGbbNameOverrideText1(masterVO.getGbbNameOverrideText1());
        novatorMasterVO.setGbbPriceOverrideFlag1(masterVO.getGbbPriceOverrideFlag1());
        novatorMasterVO.setGbbPriceOverrideText1(masterVO.getGbbPriceOverrideText1());
        novatorMasterVO.setGbbUpsellDetailId2(masterVO.getGbbUpsellDetailId2());
        novatorMasterVO.setGbbNameOverrideFlag2(masterVO.getGbbNameOverrideFlag2());
        novatorMasterVO.setGbbNameOverrideText2(masterVO.getGbbNameOverrideText2());
        novatorMasterVO.setGbbPriceOverrideFlag2(masterVO.getGbbPriceOverrideFlag2());
        novatorMasterVO.setGbbPriceOverrideText2(masterVO.getGbbPriceOverrideText2());
        novatorMasterVO.setGbbUpsellDetailId3(masterVO.getGbbUpsellDetailId3());
        novatorMasterVO.setGbbNameOverrideFlag3(masterVO.getGbbNameOverrideFlag3());
        novatorMasterVO.setGbbNameOverrideText3(masterVO.getGbbNameOverrideText3());
        novatorMasterVO.setGbbPriceOverrideFlag3(masterVO.getGbbPriceOverrideFlag3());
        novatorMasterVO.setGbbPriceOverrideText3(masterVO.getGbbPriceOverrideText3());
        novatorMasterVO.setWebsites(masterVO.getWebsites());
        
        
        //loop through upsell detail list
        List upsellList = masterVO.getUpsellDetailList();
        List novatorList = new ArrayList();
        if (upsellList != null){
            for(int i=0;i<upsellList.size();i++){
                    UpsellDetailVO upsellDetailVO= (UpsellDetailVO)upsellList.get(i);
                    UpsellDetailVO novatorDetailVO= new UpsellDetailVO();
                    novatorDetailVO.setAvailable(upsellDetailVO.isAvailable());
                    novatorDetailVO.setSentToNovatorContent(upsellDetailVO.isSentToNovatorContent());
                    novatorDetailVO.setSentToNovatorProd(upsellDetailVO.isSentToNovatorProd());
                    novatorDetailVO.setSentToNovatorTest(upsellDetailVO.isSentToNovatorTest());
                    novatorDetailVO.setSentToNovatorUAT(upsellDetailVO.isSentToNovatorUAT());
                    novatorDetailVO.setAvailable(upsellDetailVO.isAvailable());
                    novatorDetailVO.setDetailID(upsellDetailVO.getDetailID());
                    novatorDetailVO.setDetailMasterID(upsellDetailVO.getDetailMasterID());
                    //novatorDetailVO.setDetailMasterName("<![CDATA[" + FTDUtil.convertNulls((upsellDetailVO.getDetailMasterName())) + "]]>");                    
                    novatorDetailVO.setDetailMasterName((String)FTDUtil.convertNulls((upsellDetailVO.getDetailMasterName())));
                    //novatorDetailVO.setName("<![CDATA[" + FTDUtil.convertNulls((upsellDetailVO.getName())) + "]]>");
                    novatorDetailVO.setName((String)FTDUtil.convertNulls((upsellDetailVO.getName())));
                    novatorDetailVO.setNovatorID(upsellDetailVO.getNovatorID());
                    novatorDetailVO.setPrice(upsellDetailVO.getPrice());
                    novatorDetailVO.setSequence(upsellDetailVO.getSequence());
                    novatorDetailVO.setType(upsellDetailVO.getType());
                    novatorDetailVO.setDefaultSkuFlag(upsellDetailVO.getDefaultSkuFlag());
                    
                                //determine status value
                      String status = "";
                     
                    if ( (!masterVO.isMasterStatus()) && StringUtils.equalsIgnoreCase(upsellDetailVO.getDefaultSkuFlag(), "Y") &&  StringUtils.equalsIgnoreCase(myBuysFeedEnabled, "Y") )
                    {
                        logger.debug("calling MyBuys feed method");
                        try{
                        sendMyBuys(upsellDetailVO.getDetailID());
                        logger.debug("Succesfull in sending MyBuys message() for order_processing for product id: "+ upsellDetailVO.getDetailID());
                        } catch (Exception e)
                        {
                           logger.error("Error in sending MyBuys message() for product id"+ e);
                           try{
                           CommonUtils.sendSystemMessage("Error sending MyBuys." +  e.getMessage() );
                           } catch (Exception ex)
                           {
                             logger.error(" Error in calling CommonUtils.sendSysttemMessage for My Buys. "+ e);
                           }
                        }
                    }
                    
                    novatorList.add(novatorDetailVO);
            }//end for
            novatorMasterVO.setUpsellDetailList(novatorList);
            }//end upsell list not null
     


        //send to novator
        if(novatorEnvKeys != null && novatorEnvKeys.size() > 0) {
            sendToNovator(conn, novatorMasterVO.getMasterID(),novatorEnvKeys);
        }
        
        if(debugEnabled)
            logger.debug("ProductBOImpl: complete delete master sku");
    }        


    /** 
     * Retrieve a sku
     * @param conn database connection
     * @sku to retrieve
     * @return UpsellMasterVO
     * @exception String The exception description.
     */
    public UpsellMasterVO getMasterSKU(Connection conn, String sku) 
        throws PDBApplicationException, PDBSystemException
    {
        return upsellDAO.getMasterSKU(conn,sku);
    }        

    /**
     *  Create and array of upsell data based on a passed in request
     * @param request object
     * @return array of upsell VOs
     * @exception String The exception description.
     */
    public List getSKUsFromRequest(HttpServletRequest request) 
        throws PDBApplicationException, PDBSystemException{

        String[] detailIdArray = request.getParameterValues(ProductMaintConstants.PDB_UPSELL_DETAIL_ID_KEY);
	String[] detailNameArray = request.getParameterValues(ProductMaintConstants.PDB_UPSELL_DETAIL_NAME_KEY);
	String[] detailTypeArray = request.getParameterValues(ProductMaintConstants.PDB_UPSELL_DETAIL_TYPE_KEY);                
	String[] detailPriceArray = request.getParameterValues(ProductMaintConstants.PDB_UPSELL_DETAIL_PRICE_KEY);
	String[] detailAvailableArray = request.getParameterValues(ProductMaintConstants.PDB_UPSELL_DETAIL_AVAILABLE_KEY);                
	String[] detailNovatorIDArray = request.getParameterValues(ProductMaintConstants.PDB_UPSELL_DETAIL_NOVATORID_KEY);                
	String[] sentToNovatorProdArray = request.getParameterValues(ProductMaintConstants.PDB_UPSELL_DETAIL_SENT_TO_NOVATOR_PROD_KEY);                
	String[] sentToNovatorContentArray = request.getParameterValues(ProductMaintConstants.PDB_UPSELL_DETAIL_SENT_TO_NOVATOR_CONTENT_KEY);                
	String[] sentToNovatorUATArray = request.getParameterValues(ProductMaintConstants.PDB_UPSELL_DETAIL_SENT_TO_NOVATOR_UAT_KEY);                
	String[] sentToNovatorTestArray = request.getParameterValues(ProductMaintConstants.PDB_UPSELL_DETAIL_SENT_TO_NOVATOR_TEST_KEY);                         
        
        String gbbUpsellDetailId1 = request.getParameter("gbbUpsellDetailId1");
        String gbbNameOverrideFlag1 = request.getParameter("gbbNameOverrideFlag1");
	String gbbNameOverrideText1 = request.getParameter("gbbNameOverrideText1");
        String gbbPriceOverrideFlag1 = request.getParameter("gbbPriceOverrideFlag1");
        String gbbPriceOverrideText1 = request.getParameter("gbbPriceOverrideText1");
	String gbbUpsellDetailId2 = request.getParameter("gbbUpsellDetailId2");
	String gbbNameOverrideFlag2 = request.getParameter("gbbNameOverrideFlag2");
	String gbbNameOverrideText2 = request.getParameter("gbbNameOverrideText2");
	String gbbPriceOverrideFlag2 = request.getParameter("gbbPriceOverrideFlag2");
	String gbbPriceOverrideText2 = request.getParameter("gbbPriceOverrideText2");
	String gbbUpsellDetailId3 = request.getParameter("gbbUpsellDetailId3");
	String gbbNameOverrideFlag3 = request.getParameter("gbbNameOverrideFlag3");
	String gbbNameOverrideText3 = request.getParameter("gbbNameOverrideText3");
	String gbbPriceOverrideFlag3 = request.getParameter("gbbPriceOverrideFlag3");
	String gbbPriceOverrideText3 = request.getParameter("gbbPriceOverrideText3");
        String defaultSkuFlag = request.getParameter("defaultSkuFlag");

	ArrayList detailArray = new ArrayList();
  	//only continue if detail array has items
	if(detailIdArray != null && detailIdArray[0].length() > 0 )
	{
            //loop through all items in array
	    for(int i = 0; i < detailIdArray.length; i++) {
	        UpsellDetailVO detailVO = new UpsellDetailVO();
	        detailVO.setDetailID(detailIdArray[i]);
                detailVO.setName(detailNameArray[i]);
	        detailVO.setPrice(Float.parseFloat(FTDUtil.removeFormattingCurrency(detailPriceArray[i])));
	        detailVO.setNovatorID(detailNovatorIDArray[i]);
	        detailVO.setType(detailTypeArray[i]);
	        detailVO.setSentToNovatorProd(FTDUtil.convertStringToBoolean(sentToNovatorProdArray[i]));
	        detailVO.setSentToNovatorContent(FTDUtil.convertStringToBoolean(sentToNovatorContentArray[i]));
	        detailVO.setSentToNovatorUAT(FTDUtil.convertStringToBoolean(sentToNovatorUATArray[i]));
	        detailVO.setSentToNovatorTest(FTDUtil.convertStringToBoolean(sentToNovatorTestArray[i]));                        
	        detailVO.setSequence(i + 1);
	        if (detailAvailableArray[i].equalsIgnoreCase("Y")){
	            detailVO.setAvailable(true);
	        } else {
	            detailVO.setAvailable(false);
	        }
                if (Integer.toString(i).equals(defaultSkuFlag)) {
                    detailVO.setDefaultSkuFlag("Y");
                } else {
                    detailVO.setDefaultSkuFlag("N");
                }
                if (detailVO.getDetailID() != null && detailVO.getDetailID().equalsIgnoreCase(gbbUpsellDetailId1)) {
                    logger.debug("found position 1: " + detailVO.getDetailID());
                    detailVO.setGbbNameOverrideFlag(BooleanUtils.toBoolean(gbbNameOverrideFlag1));
                    detailVO.setGbbNameOverrideText(gbbNameOverrideText1);
                    detailVO.setGbbPriceOverrideFlag(BooleanUtils.toBoolean(gbbPriceOverrideFlag1));
                    detailVO.setGbbPriceOverrideText(gbbPriceOverrideText1);
                    detailVO.setGbbSequence(1);
                } else if (detailVO.getDetailID() != null && detailVO.getDetailID().equalsIgnoreCase(gbbUpsellDetailId2)) {
    	            logger.debug("found position 2: " + detailVO.getDetailID());
                    detailVO.setGbbNameOverrideFlag(BooleanUtils.toBoolean(gbbNameOverrideFlag2));
                    detailVO.setGbbNameOverrideText(gbbNameOverrideText2);
                    detailVO.setGbbPriceOverrideFlag(BooleanUtils.toBoolean(gbbPriceOverrideFlag2));
                    detailVO.setGbbPriceOverrideText(gbbPriceOverrideText2);
                    detailVO.setGbbSequence(2);
    	        } else if (detailVO.getDetailID() != null && detailVO.getDetailID().equalsIgnoreCase(gbbUpsellDetailId3)) {
                    logger.debug("found position 3: " + detailVO.getDetailID());
    	            detailVO.setGbbNameOverrideFlag(BooleanUtils.toBoolean(gbbNameOverrideFlag3));
    	            detailVO.setGbbNameOverrideText(gbbNameOverrideText3);
    	            detailVO.setGbbPriceOverrideFlag(BooleanUtils.toBoolean(gbbPriceOverrideFlag3));
    	            detailVO.setGbbPriceOverrideText(gbbPriceOverrideText3);
    	            detailVO.setGbbSequence(3);
                }

	        detailArray.add(detailVO);
    	    }//end for
    	}//end if array contains values
    
    	return detailArray;
	}
	
    /**
     *  Create and array of upsell data based on a passed in request
     * @param conn database connection
     * @param productid id of upsell to retrieve
     * @return array of upsell VOs
     * @exception String The exception description.
     */
    public List getUpsellDetailByID(Connection conn, String productid) 
        throws PDBApplicationException, PDBSystemException
    {
        return upsellDAO.getUpsellDetailByID(conn,productid);
    }        

    /**
     *  Create and array of upsell data based on a passed in request
     * @param conn database connection
     * @param masterSKU to transmit
     * @param envKeys novator sites to update
     * @return array of upsell VOs
     * @exception String The exception description.
     */
    public void sendToNovator(Connection conn, String masterSKU, List<String> envKeys) 
        throws PDBApplicationException, PDBSystemException
    {
        try {
            NovatorFeedProductUtil feedUtil = new NovatorFeedProductUtil();
            NovatorFeedResponseVO resVO = feedUtil.sendUpsellFeed(conn, masterSKU, envKeys);
            
            String errorString = "";
            if(!resVO.isSuccess()) {
                errorString = resVO.getErrorString();
            }
            
            if (!errorString.equals("")){
                logger.info("Received error in feed:" + errorString);
                String[] args = {errorString};
                throw new PDBSystemException(ProductMaintConstants.PDB_NOVATOR_EXCEPTION, args);
            }
        } catch (PDBSystemException pe) {
            logger.error(pe);
            throw pe;
        } catch (Exception e) {
            logger.error(e);
            String[] args = {e.getMessage()};
            throw new PDBSystemException(ProductMaintConstants.PDB_NOVATOR_EXCEPTION, args, e);   
        }
    }

        
    /**
     * Determine if a master sku exists in upsell master
     * @param conn database connection
     * @param sku to validate
     * @return true if the master sku exists
     */
    public boolean validateMasterSku(Connection conn, String sku) 
        throws PDBApplicationException, PDBSystemException {
        return upsellDAO.validateMasterSku(conn, sku);
    }

    /**
     * calls the MDB for sending JMS message to myBuys queue
     * @param String productID of object for whom messages need to be sent
     * @return void
     */
      
    public void sendMyBuys(String productId) throws Exception
    {

         MessageToken messageToken = new MessageToken();
         messageToken.setMessage(productId);
         messageToken.setStatus("MY BUYS");
         Dispatcher dispatcher = Dispatcher.getInstance();
         dispatcher.dispatchTextMessage(new InitialContext(),messageToken);
        
    }

    public void setUpsellDAO(IUpsellDAO upsellDAO) {
        this.upsellDAO = upsellDAO;
    }

    public IUpsellDAO getUpsellDAO() {
        return upsellDAO;
    }
}

