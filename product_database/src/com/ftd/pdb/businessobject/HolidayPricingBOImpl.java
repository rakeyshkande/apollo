package com.ftd.pdb.businessobject;

import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBSystemException;
import com.ftd.pdb.common.utilities.NovatorTransmission;
import com.ftd.pdb.common.valobjs.HolidayPricingVO;
import com.ftd.pdb.integration.dao.IHolidayPricingDAO;

import java.sql.Connection;

import java.util.Map;


public class HolidayPricingBOImpl extends PDBBusinessObject implements IHolidayPricingBO
{
    /* Spring manager resources */
     IHolidayPricingDAO holidayDAO;
    /* end Spring managed resources */

/**
 * Constructor for HolidayPricingBOImple object
 */
    public HolidayPricingBOImpl()
    {
        super("com.ftd.pdb.businessobject.PDBBusinessObject");
    }

   /**
    * Update holiday pricing based on user input.
    * @param conn database connection
    * @param holidayVO HolidayPricingVO containing user input
    * @deprecated Holiday Pricing is unused and will be removed in the future.
    *   lpuckett 08/16/2006
    */
    public void setHolidayPricing(Connection conn, HolidayPricingVO holidayVO, Map novatorMap) 
        throws PDBApplicationException, PDBSystemException
    {

        //Obtain current holiday pricing..needed for rollback
        HolidayPricingVO rollbackHolidayVO = getHolidayPricing(conn);
        
        // Set the holiday pricing in DB
        holidayDAO.setHolidayPricing(conn,holidayVO);

        //Send updates to Novator
        try {
            new NovatorTransmission().send(ProductMaintConstants.NOVATOR_XML_HOLIDAY_PRICING_HEADER,holidayVO, novatorMap);
          }
        catch (Exception e){
            holidayDAO.setHolidayPricing(conn,rollbackHolidayVO);
            throw new PDBSystemException(ProductMaintConstants.OE_NOVATOR_TRANSMISSION_ERROR,  e);   
        }

    }

  /** 
   * Retrieve holiday pricing data from database
   * @param conn database connection
   * @return HolidayPricingVO containing data from database
   * @deprecated Holiday Pricing is unused and will be removed in the future.
   *   lpuckett 08/16/2006
   */
    public HolidayPricingVO getHolidayPricing(Connection conn) 
        throws PDBApplicationException, PDBSystemException
    {
        // Load the holiday pricing
        return holidayDAO.getHolidayPricing(conn);
    }

    public void create()
    {
    }

  
    /**
     * Returns a string representation of this BO
     *
     * @return a string representation of this bo
     */
    public String toString()
    {
        return null;
    }

    /**
     * Compares the passed in objec with this object.
     *
     * @return true if objects equal, else returns false
     */
    public boolean equals(java.lang.Object object1)
    {
        return false;
    }

    /**
     * Saves this bo to a persistent state.
     */     
    public void save()
    {
    }

    /**
     * @param holidayDAO
     * @deprecated Holiday Pricing is unused and will be removed in the future.
     *   lpuckett 08/16/2006
     */
    public void setHolidayDAO(IHolidayPricingDAO holidayDAO) {
        this.holidayDAO = holidayDAO;
    }


    /**
     * @return
     * @deprecated We're not using Holiday Pricing...at some point we'll remove it
     *      lpuckett 08/16/2006
     */
    public IHolidayPricingDAO getHolidayDAO() {
        return holidayDAO;
    }
}
