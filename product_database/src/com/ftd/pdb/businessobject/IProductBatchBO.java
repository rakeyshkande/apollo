package com.ftd.pdb.businessobject;

import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBSystemException;
import com.ftd.pdb.common.valobjs.ProductBatchVO;

import java.sql.Connection;

import java.util.List;


public interface IProductBatchBO 
{
   /**
    * Returns an array of ProductBatchVOs
    * @param conn database connection
    * @param userId to locate
    * @return array of products
    * @exception PDBSystemException
    */
   public List getUserBatch(Connection conn,String userId)
       throws PDBApplicationException, PDBSystemException;

    /**
    * Returns a ProductBatchVO
    * @param conn database connection
    * @param userId to locate
    * @param productId to locate
    * @return ProductBatchVO
    * @throws PDBApplicationException, PDBSystemException
    **/
    public ProductBatchVO getProduct(Connection conn, String userId, String productId) 
        throws PDBApplicationException, PDBSystemException;

    /**
    * Returns a ProductBatchVO
    * @param conn database connection
    * @param userId to locate
    * @param novatorId to locate
    * @return ProductBatchVO
    * @throws PDBApplicationException, PDBSystemException
    **/
    public ProductBatchVO getProductByNovatorId(Connection conn, String userId, String novatorId) 
        throws PDBApplicationException, PDBSystemException;

     /**
      * Updates or Creates a product in the database
      * @param conn database connection
      * @param productVO that will be created or updated
      * @throws PDBApplicationException, PDBSystemException
      **/
       public void setProduct(Connection conn, ProductBatchVO productVO)
           throws PDBApplicationException, PDBSystemException;

     /**
      * Deletes a product from the database
      * @param conn database connection
      * @param userId to delete
      * @param productId to delete
      * @exception PDBSystemException
      */
     public void deleteProduct(Connection conn, String userId, String productId) 
         throws PDBSystemException, PDBApplicationException;
}