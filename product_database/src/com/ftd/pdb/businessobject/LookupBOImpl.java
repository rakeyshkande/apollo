package com.ftd.pdb.businessobject;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.exceptions.PDBSystemException;
import com.ftd.pdb.common.utilities.PersonalizationTemplateHandler;
import com.ftd.pdb.common.valobjs.ValueVO;
import com.ftd.pdb.integration.dao.ILookupDAO;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;


/**
 * This class is used to lookup read-only lists and peform any Business logic needed
 * before they are returned.
 *
 * @author Jeff Penney - Software Architects
 **/
public class LookupBOImpl extends PDBBusinessObject implements ILookupBO
{
    /* Spring manager resources */
     ILookupDAO lookupDAO;
    /* end Spring managed resources */
    
    /**
    * Constructor for the LookupBOImpl object.  Sets the debugEnabled
    * variable and inits the logger category.
    */
    public LookupBOImpl()
    {
        super("com.ftd.pdb.businessobject.LookupBOImpl");
    }

    /**
    * Creates a Lookup BO
    */
    public void create()
    {
    }

    /**
    * Returns a string representation of this BO
    *
    * @return a string representation of this BO
    */
    public String toString()
    {
        return null;
    }

    /**
    * Checks to see if this object is internally equal to the parameter object
    * @param object1 object to be compared to this object
    */    
    public boolean equals(java.lang.Object object1)
    {
        return false;
    }

    /**
    *   Saves this BO to persistent storage
    */
    public void save()
    {
    }

    /**
     * Get a list of  Companies
     * @param conn database connection
     * @return List of Categories
     * @exception PDBSystemException
     * @auther Ed Mueller 7/31/03
     */
    public List getCompanyMasterList(Connection conn) throws PDBSystemException
    {
        return lookupDAO.getCompanyMasterList(conn); 
    }    

    /**
     * Get a list of  Product Categories
     * @param conn database connection
     * @return List of Categories
     * @exception PDBSystemException
     */
    public List getCategoryList(Connection conn) throws PDBSystemException
    {
        return lookupDAO.getCategoryList(conn);
    }

    /**
     * Get a list of  Carrier types
     * @param conn database connection
     * @return List of Categories
     * @exception PDBSystemException
     * Ed Mueller, 6/9/03
     */
    public List getCarrierTypes(Connection conn) throws PDBSystemException
    {
        return lookupDAO.getCarrierTypes(conn);
    }

    /**
     * Get a list of JCPenney Product Categories
     * @param conn database connection
     * @return List of Categories
     * @exception PDBSystemException
     * @deprecated JCPenney is unused lpuckett 08/30/2006
     */
    public List getJCPenneyCategoryList(Connection conn) throws PDBSystemException
    {
        return lookupDAO.getJCPenneyCategoryList(conn);
    }

    /**
    * Returns a List of Types
    * @param conn database connection
    * @return List of Types
    * @throws PDBSystemException
    **/
    public List getTypesList(Connection conn) throws PDBSystemException
    {
        return lookupDAO.getTypesList(conn);
    }

    /**
    * Returns a List of Sub types
    * @param conn database connection
    * @return List of Sub types
    * @throws PDBSystemException
    **/
    public List getSubTypesList(Connection conn) throws PDBSystemException
    {
        return lookupDAO.getSubTypesList(conn);
    }

    /**
    * Returns a List of Countries
    * @param conn database connection
    * @return List of Countries
    * @throws PDBSystemException
    **/
    public List getCountriesList(Connection conn) throws PDBSystemException
    {
        return lookupDAO.getCountriesList(conn);
    }

    /**
    * Returns a List of Second Choices
    * @param conn database connection
    * @return List of Second Choices
    * @throws PDBSystemException
    **/
    public List getSecondChoiceList(Connection conn) throws PDBSystemException
    {
        return lookupDAO.getSecondChoiceList(conn);
    }

    /**
    * Returns a List of States
    * @param conn database connection
    * @return List of States
    * @throws PDBSystemException
    **/
    public List getStatesList(Connection conn) throws PDBSystemException
    {
        return lookupDAO.getStatesList(conn);
    }    

    /**
    * Returns a List of Exception codes
    * @param conn database connection
    * @return List of Exception codes
    * @throws PDBSystemException
    **/
    public List getExceptionCodesList(Connection conn) throws PDBSystemException
    {
        return lookupDAO.getExceptionCodesList(conn);
    }

    /**
     * Get a list of  Vendors
     * @param conn database connection
     * @return List of ValueVOs that have the vendor ID and name
     * @exception PDBSystemException
     */
    public List getVendorList(Connection conn) throws PDBSystemException
    {
        return lookupDAO.getVendorList(conn);
    }


    /**
     * Get a list of  Vendor Carriers
     * @param conn database connection
     * @return List of ValueVOs that have the vendor ID and name
     * @exception PDBSystemException
     */
    public List getVendorCarrierList(Connection conn) throws PDBSystemException
    {
        return lookupDAO.getVendorCarrierList(conn);
    }
    
    /**
     * Get a list of  ValueVOs representing shipping availability 
     * 
     * @return List of ValueVOs representing shipping availability 
     * @exception PDBSystemException
     */
    public List getShippingAvailabilityList(String typeCode) throws PDBSystemException
    {
        List list = new ArrayList();
        if(typeCode.equals("FLORAL"))
        {
            ValueVO vo = new ValueVO();
            vo.setId("I");
            vo.setDescription("International");
            list.add(vo);

            vo = new ValueVO();
            vo.setId("D");
            vo.setDescription("Domestic");
            list.add(vo);
        }

        return list;
    }

    /**
     * Get a list of  ValueVOs representing price points
     * @return List of ValueVOs representing price points
     * @exception PDBSystemException
     */
    public List getPricePointsList() throws PDBSystemException
    {
        List list = new ArrayList();

        ValueVO valueVO = new ValueVO();
        valueVO.setId(" ");
        valueVO.setDescription(" ");
        list.add(valueVO);
        
        valueVO = new ValueVO();
        valueVO.setId("1");
        valueVO.setDescription("1");
        list.add(valueVO);

        valueVO = new ValueVO();
        valueVO.setId("2");
        valueVO.setDescription("2");
        list.add(valueVO);

        valueVO = new ValueVO();
        valueVO.setId("3");
        valueVO.setDescription("3");
        list.add(valueVO);
        return list;
    }


    /**
     * Get a list of  ValueVOs representing shipping keys
     * @param conn database connection
     * @return List of ValueVOs representing shipping keys
     * @exception PDBSystemException
     */
    public List getShippingKeysList(Connection conn) throws PDBSystemException
    {
       return lookupDAO.getShippingKeysList(conn);
    }    

    /**
     * Get a list of  ValueVOs representing recipient search ids
     * @param conn database connection
     * @return List of ValueVOs representing recipient search ids
     * @exception PDBSystemException
     */
    public List getRecipientSearchList(Connection conn) throws PDBSystemException
    {
        return lookupDAO.getRecipientSearchList(conn);
    }      
    
    /**
     * Get a list of  ValueVOs representing personalization templates
     * @param conn database connection
     * @return List of ValueVOs representing personalization templates
     * @exception PDBSystemException
     */
    public List getPersonalizationTemplateList(Connection conn) throws PDBSystemException
    {
        PersonalizationTemplateHandler pth = (PersonalizationTemplateHandler)CacheManager.getInstance().getHandler(ProductMaintConstants.CACHE_PERS_TEMPLATE_HANDLER);
        return pth.getTemplates();
    }    

    /**
     * Get a list of  ValueVOs representing product personalization template field 
     * order/permutation possibilities for a given template.
     * @param templateId - Template id
     * @return List of ValueVOs representing product personalization template field order/permutation possibilities
     * @exception PDBSystemException
     */
    public List getPersonalizationTemplateOrder(String templateId) throws PDBSystemException
    {
        PersonalizationTemplateHandler pth = (PersonalizationTemplateHandler)CacheManager.getInstance().getHandler(ProductMaintConstants.CACHE_PERS_TEMPLATE_HANDLER);
        return pth.getTemplatePermutations(templateId);
    }

    /**
     * Get a list of  ValueVOs representing search priorities
     * @param conn database connection
     * @return List of ValueVOs representing search priorities
     * @exception PDBSystemException
     */
    public List getSearchPriorityList(Connection conn) throws PDBSystemException
    {
        return lookupDAO.getSearchPriorityList(conn);
    }
    
    /**
     * Get a list of  ValueVOs representing source codes 
     * @param conn database connection
     * @return List of ValueVOs representing source codes
     * @exception PDBSystemException
     */
    public List getSourceCodeList(Connection conn) throws PDBSystemException
    {
        return lookupDAO.getSourceCodeList(conn);
    }

    /**
     * Get a list of  ValueVOs representing shipping methods
     * @param conn database connection
     * @return List of ValueVOs representing shipping methods
     * @exception PDBSystemException
     */
    public List getShippingMethodsList(Connection conn) throws PDBSystemException
    {
        return lookupDAO.getShippingMethodsList(conn);
    }

    /**
     * Get a list of  ValueVOs representing available colors
     * @param conn database connection
     * @return List of ValueVOs representing available colors
     * @exception PDBSystemException
     */
    public List getColorsList(Connection conn) throws PDBSystemException
    {
        return lookupDAO.getColorsList(conn);
    }

    /**
     * Get a list of StateDeliveryExclusionVO representing states that have delivery exclusions
     * @param conn database connection
     * @return List of StateDeliveryExclusionVO representing states that have delivery exclusions
     * @exception PDBSystemException
     */
    public List getStateDeliveryExceptionList(Connection conn) throws PDBSystemException
    {
        return lookupDAO.getStateDeliveryExceptionList(conn);
    }

     /**
      * Get a list of  ValueVOs representing active vendor types
      * @param conn database connection
      * @return List of ValueVOs representing active vendor types
      * @exception PDBSystemException
      */
     public List getActiveVendorTypes(Connection conn) throws PDBSystemException
    {
        return lookupDAO.getActiveVendorTypes(conn);
    }   
    
    /**
     * Get a list of  box types
     * @param conn database connection
     * @return List of ValueVOs representing box types
     * @exception PDBSystemException
     */
    public List getBoxList(Connection conn) throws PDBSystemException
    {
       return lookupDAO.getBoxList(conn);
    }
    
    /**
     * Get a list of  Vendors that are of a specific type
     * @param conn database connection
     * @param vendorType filter
     * @return List of ValueVOs that have the vendor ID and name
     * @exception PDBSystemException
     */
    public List getVendorsOfType(Connection conn, String vendorType) throws PDBSystemException {
        return lookupDAO.getVendorsOfType(conn,vendorType);
    }
    
    /**
     * Get a list of carriers
     * @param conn database connection
     * @return List of ValueVO containing carrier data
     * @throws PDBSystemException
     */
    public List getCarrierList(Connection conn) throws PDBSystemException {
        return lookupDAO.getCarrierList(conn);
    }

    /**
     * Get a list of  ValueVOs representing available component skus
     * @param conn database connection
     * @return List of ValueVOs representing available component skus
     * @exception PDBSystemException
     */
    public List getComponentSkuList(Connection conn) throws PDBSystemException
    {
        return lookupDAO.getComponentSkuList(conn);
    }
    
     /**
     * Get a list of service durations
     * @return List of durations
     * @exception PDBSystemException
     */
    public List getServiceDurationList() throws PDBSystemException
    {
        List list = new ArrayList();
        
        try{
          ConfigurationUtil configUtil = ConfigurationUtil.getInstance();  
          String serviceDurationValues = configUtil.getFrpGlobalParm(ProductMaintConstants.PDB_CONFIG_CONTEXT,"SERVICE_DURATION_VALUES");    
          ValueVO valueVO = new ValueVO();
          
          //Adding blank element
          valueVO = new ValueVO();
          valueVO.setId(" ");
          valueVO.setDescription(" ");
          list.add(valueVO);
          
          //Adding other elements defined in global parms
          if(serviceDurationValues != null)
          {
            StringTokenizer st = new StringTokenizer(serviceDurationValues, ",");
            while(st.hasMoreElements())
            {
              valueVO = new ValueVO();
              String nextDuration = (String) st.nextElement();
              valueVO.setId(nextDuration);
              valueVO.setDescription(nextDuration);
              list.add(valueVO);
            }
          }
        }
        catch(Exception e)
        {
          logger.error(e);
          throw new PDBSystemException(ProductMaintConstants.GENERAL_APPLICATION_EXCEPTION, e);
        }
        return list;
    }

    public void setLookupDAO(ILookupDAO lookupDAO) {
        this.lookupDAO = lookupDAO;
    }

    public ILookupDAO getLookupDAO() {
        return lookupDAO;
    }


}
