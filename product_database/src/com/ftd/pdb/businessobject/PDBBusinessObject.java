package com.ftd.pdb.businessobject;

import com.ftd.osp.utilities.plugins.Logger;

public abstract class PDBBusinessObject {

    protected static Logger logger;
    
    /**
    * This field is used to check if debugging is enabled.
    */
    protected boolean debugEnabled;
    
    public PDBBusinessObject(String loggerCategory) {
        logger = new Logger(loggerCategory);
        
        if ( logger.isDebugEnabled() )
        {
            debugEnabled = true;
        }
    }

  /**
   * Responsible for creating  the business object
   **/
  public abstract void create();

  /**
   * Responsible for saving the business object
   **/
  public abstract void save();

  /**
   * Indicates whether some other object is "equal to" this one.
   * The subclass should ensure that the argument is an instance of LWBO
   *
   * @param obj the object that will be compared
   * @return boolean
   **/
  public abstract boolean equals(Object obj);

  /**
   * Returns a string representation of the object.
   * The default implementation will be comma separated name value pairs.
   *
   * @return a string representation of the object
   **/
  public abstract String toString();
}
