package com.ftd.pdb.businessobject;

import com.ftd.pdb.common.exceptions.PDBApplicationException;

import com.ftd.pdb.common.exceptions.PDBSystemException;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.ftd.pdb.common.valobjs.UpsellMasterVO;

import java.sql.Connection;

public interface IUpsellBO 
{
    /**
     * Get an upsell list
     * @param conn database connection
     * @return array of upsell skus
     * @exception String The exception description.
     */
    public List getUpsellList(Connection conn)
        throws PDBApplicationException, PDBSystemException;

     /**
      * Insert or update Master SKU
      * @param conn database connection
      * @param masterVO record to save
      * @param saveNovatorFeeds novator servers to send update to
      * @return UpsellMasterVO
      * @exception String The exception description.
      */
     public void updateMasterSKU(Connection conn, UpsellMasterVO masterVO,List<String> saveNovatorFeeds) 
         throws PDBApplicationException, PDBSystemException;        
         
    /** 
     * Retrieve a sku
     * @param conn database connection
     * @sku to retrieve
     * @return UpsellMasterVO
     * @exception String The exception description.
     */
    public UpsellMasterVO getMasterSKU(Connection conn, String sku) 
        throws PDBApplicationException, PDBSystemException;

     /**
      *  Create and array of upsell data based on a passed in request
      * @param request object
      * @return array of upsell VOs
      * @exception String The exception description.
      */
     public List getSKUsFromRequest(HttpServletRequest request) 
         throws PDBApplicationException, PDBSystemException;

     /**
      *  Create and array of upsell data based on a passed in request
      * @param conn database connection
      * @param productid id of upsell to retrieve
      * @return array of upsell VOs
      * @exception String The exception description.
      */
     public List getUpsellDetailByID(Connection conn, String productid) 
         throws PDBApplicationException, PDBSystemException;


     /**
      *  Create and array of upsell data based on a passed in request
      * @param conn database connection
      * @param masterSKU to transmit
      * @param saveNovatorFeeds novator sites to update
      * @return array of upsell VOs
      * @exception String The exception description.
      */
     public void sendToNovator(Connection conn, String masterSKU, List<String> saveNovatorFeeds) 
         throws PDBApplicationException, PDBSystemException;

    /**
     * Determine if a master sku exists in upsell master
     * @param conn database connection
     * @param sku to validate
     * @return true if the master sku exists
     */
    public boolean validateMasterSku(Connection conn, String sku) 
        throws PDBApplicationException, PDBSystemException;
}