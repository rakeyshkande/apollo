package com.ftd.pdb.businessobject;

import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBSystemException;
import com.ftd.pdb.common.valobjs.ProductVO;
import com.ftd.pdb.common.valobjs.VendorProductOverrideVO;

public interface IProductBO {
    /**
      * Return a list of products
      * @conn database connection
      * @return array of products
      * @exception String The exception description.
      */
    public List getProductList(Connection conn) throws PDBApplicationException, 
                                                       PDBSystemException;

    /**
      * Returns search results based on the passed in search criteria
      * @param conn database connection
      * @param avail filter
      * @param unavail filter
      * @param dateRestrict filter
      * @param onHold filter
      * @return array of products based on filtered criteria
      * @exception String The exception description.
      */
    public List getProductListAdvanced(Connection conn, String avail, 
                                       String unavail, String dateRestrict, 
                                       String onHold) throws PDBApplicationException, 
                                                             PDBSystemException;

    /**
      * Removes holiday pricing for the passed in product
      * @param conn database connection
      * @param product id
      * @throws PDBApplicationException
      * @throws PDBSystemException
      */
    public void removeHolidayPricing(Connection conn, 
                                     String product) throws PDBApplicationException, 
                                                            PDBSystemException;

    /**
      * Returns an array of products with holiday pricing
      * @param conn database connection
      * @return array of products with holiday pricing
      * @throws PDBApplicationException
      * @throws PDBSystemException
      */
    public List getProductListWithHolidayPricing(Connection conn) throws PDBApplicationException, 
                                                                         PDBSystemException;

    /**
     * Returns a ProductVO base on the passed product or novator id
     * @param conn database connection
     * @param productId
     * @param novatorId
     * @return ProductVO
     * @throws PDBApplicationException, PDBSystemException
     **/
    public ProductVO getProduct(Connection conn, String productId, 
                                String novatorId) throws PDBApplicationException, 
                                                         PDBSystemException;

    /**
    * Updates or Creates a product in the database
    * @param conn database connection
    * @param productVO
    * @param envKeys environments that novator servers will be updated
    * @throws PDBApplicationException, PDBSystemException
    **/
    public void setProduct(Connection conn, ProductVO productVO, 
                           List envKeys) throws PDBApplicationException, 
                                                        PDBSystemException;

    /**
    * Returns a list of subcodes with passed in subcode id
    * @param conn database connection
    * @param id subcode filter
    * @throws PDBSystemException
    **/
    public List getSubCodesByID(Connection conn, 
                                String id) throws PDBApplicationException, 
                                                  PDBSystemException;

    /**
      * Returns an array of product ids that have been updated since the passed in date
      * @param conn database connection
      * @param lastUpdateDate return any products that have been updated since this date
      * @return array of product ids
      * @throws PDBSystemException
      * @throws PDBApplicationException
      */
    public List getProductsUpdatedSince(Connection conn, 
                                        java.util.Date lastUpdateDate) throws PDBSystemException, 
                                                                              PDBApplicationException;

    /**
      * Send the product passed to Novator
      * @param conn
      * @param productVO
      * @param envKeys
      * @param overwriteSentTo If true overwrite any existing "sent_to_novator" values.
      * @throws PDBSystemException
      * @throws PDBApplicationException
      */
    public void sendProductToNovator(Connection conn, ProductVO productVO, 
                                     List envKeys, boolean overwriteSentTo) throws PDBSystemException, 
                                                            PDBApplicationException;

    /**
     * Validates the passed in product id
     * @param conn database connection
     * @param productId to validate
     * @return true if the product id is a valid product id, novator id, or subsku
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public boolean validateProductId(Connection conn, 
                                     String productId) throws PDBSystemException, 
                                                              PDBApplicationException;
                                                              
    /**
     * Get a list of lists of VendorProductVOs for vendors and their skus for the passed
     * in product/sku id
     * @param conn database connection
     * @param productSkuId to retrieve
     * @return List of VendorProductVOs for the passed in prduct/sku id
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public List getVendorsForProduct(Connection conn, String productSkuId) 
        throws PDBSystemException, PDBApplicationException;
        
    /**
     * Returns a list of products for the passed vendorId
     * @param conn
     * @param vendorId filter
     * @return list of Strings containing the product Ids
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public List getVendorProducts(Connection conn, String vendorId) throws PDBSystemException, PDBApplicationException;
    
    
    
    /**
     * Returns a list of SDS ship methods for the passed carrierId
     * @param conn
     * @param carrier Id filter
     * @return list of Strings containing the SDS ship methods
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public List getCarrierSdsShipMethods(Connection conn, String vendorId) throws PDBSystemException, PDBApplicationException;
  
  
    /**
     * Get a list of VendorProductOverrideVO objects, filtered on the passed variables
     * @param conn database connection
     * @param vendorId filter
     * @param productId filter
     * @param overrideDate filter
     * @param carrierId filter
     * @return
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public List getVPOverrides(Connection conn, String vendorId, String productId, Date overrideDate, String carrierId) 
        throws PDBSystemException, PDBApplicationException;
        
    /**
     * Updates the values of the passed VendorProductOverrideVO in the VENDOR_PRODUCT_OVERRIDE table
     * @param conn
     * @param vpoVO data to update
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public void updateVPOverride(Connection conn, VendorProductOverrideVO vpoVO) throws PDBSystemException, PDBApplicationException;
    
    /**
     * Delete the values of the passed VendorProductOverrideVO in the VENDOR_PRODUCT_OVERRIDE table
     * @param conn
     * @param vpoVO data to delete
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public void deleteVPOverride(Connection conn, VendorProductOverrideVO vpoVO) throws PDBSystemException, PDBApplicationException;
    
    /**
     * Checks to see if id exists in FTD_APPS.VENDOR_PRODUCTS
     * @param conn
     * @param productId to check
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public String existsInVendorProduct(Connection conn, String productId) throws PDBSystemException, PDBApplicationException;

    /**
     * Updates products that have exceptions to active or inactive
     * @param conn
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public void updateExceptionProducts(Connection conn) throws PDBSystemException, PDBApplicationException;

    /**
     * Determine if the vender has a shipping account for the passed in partner
     * @param conn database conneciton
     * @param vendorId filter
     * @param partnerId filter
     * @return true if the account exist else false
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public boolean hasPartnerShipAccount(Connection conn, String vendorId, String partnerId) throws PDBSystemException, PDBApplicationException;
    
    /**
     * Validates if source code list contains invalid source codes.
     * @param conn
     * @param sourceCodeList
     * @return
     * @throws PDBSystemException
     * @throws PDBApplicationException
     */
    public List validateSourceCode(Connection conn, String sourceCodeList) throws PDBSystemException, PDBApplicationException;

    /**
     * Creates the Product Maintenance Mass Edit Spreadsheet
     * 
     * @param result list from database query
     * @return XSSFWorkbook containing the spreadsheet data
     */
    public XSSFWorkbook createProductMassEditSpreadsheet(List<ProductVO> productList,boolean errorFlag) throws Exception;
	
    
    /**
     * 
     * @param conn
     * @param search type 'M' - by master sku or 'P' by product id
     * @param String array containing either 1-5 master skus or 1-5 product ids
     * @return
     * @throws PDBApplicationException
     * @throws PDBSystemException
     */
    public List<ProductVO> getProductMassEditList(Connection conn, String searchType, 
            String[] searchItems) 
           throws PDBApplicationException, PDBSystemException;
    
    /**
     * Uploads the Mass Edit spreadsheet data to the database.
     * 
     * @param conn
     * @param pdbMassEditFile
     * @param user
     * @param uploadFileDate (needs to use java.sql.Date to pass into the stored proc.
     * @throws Exception
     */
    public void uploadProductMassEditSpreadSheet(Connection conn,String filename,String user,InputStream pdbMassEditFileIS)
    throws Exception;
        
    /**
	 * Gets the records from the MASS_EDIT_HEADER and MASS_EDIT_DETAIL to show the upload
	 * status table on the PDB Mass Edit screen.
	 * 
	 * @param conn
	 * @return a HashMap containing the massEditDirectory path and a 
	 *         List of MassEditFileStatusVO objects.
	 * @throws Exception
	 */
    public HashMap<String, Object> getMassEditFileStatus(Connection conn) throws Exception;
	  
    /**
     * Gets a count of the vendors available for a product (for PDB Mass Edit)
     * @param conn
     * @param productId
     * @return count returned from query
     * @throws PDBApplicationException
     * @throws PDBSystemException
     */
    public BigDecimal getVendorsAvailableForProduct(Connection conn, String productId) 
            throws PDBApplicationException, PDBSystemException;
    
    /**
     * Returns search results based on the passed in search criteria
     * @param conn database connection
     * @return array of products based on filtered criteria
     * @exception String The exception description.
     */
   public List getPQuadProductList(Connection conn,String pquadId) throws PDBApplicationException,PDBSystemException;
}
