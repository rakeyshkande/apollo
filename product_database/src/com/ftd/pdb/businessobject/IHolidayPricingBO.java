package com.ftd.pdb.businessobject;

import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBSystemException;
import com.ftd.pdb.common.valobjs.HolidayPricingVO;

import java.sql.Connection;

import java.util.Map;


public interface IHolidayPricingBO 
{
    /**
     * Update holiday pricing based on user input.
     * @param conn database connection
     * @param holidayVO HolidayPricingVO containing user input
     * @deprecated Holiday Pricing is unused and will be removed in the future.
     *   lpuckett 08/16/2006
     */
     public void setHolidayPricing(Connection conn, HolidayPricingVO holidayVO, Map novatorMap) 
         throws PDBApplicationException, PDBSystemException;

    /** 
     * Retrieve holiday pricing data from database
     * @param conn database connection
     * @return HolidayPricingVO containing data from database
     * @deprecated We're not using Holiday Pricing...at some point we'll remove it
     *      lpuckett 08/16/2006
     */
      public HolidayPricingVO getHolidayPricing(Connection conn) 
          throws PDBApplicationException, PDBSystemException;
}