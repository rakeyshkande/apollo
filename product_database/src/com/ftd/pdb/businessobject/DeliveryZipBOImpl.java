package com.ftd.pdb.businessobject;

import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBSystemException;

import com.ftd.pdb.integration.dao.IDeliveryZipDAO;

import java.sql.Connection;

import org.w3c.dom.Document;

public class DeliveryZipBOImpl extends PDBBusinessObject implements IDeliveryZipBO {
    IDeliveryZipDAO deliveryDAO;
    
    public DeliveryZipBOImpl() {
        super("com.ftd.pdb.businessobject.DeliveryZipBOImpl");
    }
    
     /**
      * Get all the records from the venus.carrier table
      * @param conn datbase connection
      * @return xml representation of the returned carrier records
      * @throws Exception
      */
     public Document getCarriersXML(Connection conn) throws PDBSystemException {
         return deliveryDAO.getCarriersXML(conn);
     }

    /**
     * @param conn database connection
     * @param zipCode filter
     * @return
     */
    public Document getBlockedZips(Connection conn, String zipCode) throws PDBSystemException {
        return deliveryDAO.getBlockedZips(conn,zipCode);
    }
    /**
     * Deletes a record from venus.carrier_excluded zips table
     * @param conn database connection
     * @param carrierId filter
     * @param zipCode filter
     * @throws PDBApplicationException
     * @throws PDBSystemException
     */
    public void deleteBlockedZip(Connection conn, String carrierId, String zipCode) throws PDBApplicationException, PDBSystemException {
        deliveryDAO.deleteBlockedZip(conn,carrierId,zipCode);
    }

    /**
     * Inserts a record from venus.carrier_excluded zips table
     * @param conn database connection
     * @param carrierId carrier code
     * @param zipCode postal code
     * @param userId user entering the record
     * @throws PDBApplicationException
     * @throws PDBSystemException
     */
    public void insertBlockedZip(Connection conn, String carrierId, String zipCode, String userId) throws PDBApplicationException, PDBSystemException {
        deliveryDAO.insertBlockedZip(conn,carrierId,zipCode,userId);
    }

    public void create() {
    }

    public void save() {
    }

    public String toString() {
        return null;
    }

    public boolean equals(Object obj) {
        return false;
    }

    public void setDeliveryDAO(IDeliveryZipDAO deliveryDAO) {
        this.deliveryDAO = deliveryDAO;
    }

    public IDeliveryZipDAO getDeliveryDAO() {
        return deliveryDAO;
    }
}
