package com.ftd.pdb.businessobject;

import com.ftd.pdb.common.exceptions.PDBApplicationException;

import com.ftd.pdb.common.exceptions.PDBSystemException;

import java.sql.Connection;

import java.util.List;


public interface IOccasionMaintenanceBO 
{
    /** 
     * Retrieve occasion maintenance data from database
     * @param conn database connection
     * @return List containing OccasionVO's
     */
      public List getOccasions(Connection conn) 
          throws PDBApplicationException, PDBSystemException;

    /** 
     * Retrieve list of available cards for given occasion
     * @param conn database connection
     * @return List containing cardvo's
     */
      public List getAvailableCards(Connection conn,int occasion) 
          throws PDBApplicationException, PDBSystemException;

    /** 
     * Retrieve list of selected cards for given occasion
     * @param conn database connection
     * @return List containing cardvo's
     */
      public List getSelectedCards(Connection conn,int occasion) 
          throws PDBApplicationException, PDBSystemException;        

  /**
   * Updates the occasion card mapping, assigning all the passed
   * in cards to the passed in occasion, all other cards will be
   * removed from the mapping.
   * @param conn database connection
   * @param occasion id
   * @param cards card array
   * @return void 
   * @throws PDBApplicationException
   */
    public void updateOccasionCards(Connection conn, int occasion, String cards[]) 
        throws PDBApplicationException, PDBSystemException;           
}