package com.ftd.pdb.businessobject;

import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBSystemException;
import com.ftd.pdb.common.valobjs.ProductBatchVO;
import com.ftd.pdb.integration.dao.IProductBatchDAO;

import java.sql.Connection;

import java.util.List;


public class ProductBatchBOImpl extends PDBBusinessObject implements IProductBatchBO
{
    /*Spring managed resources*/
     IProductBatchDAO productBatchDAO;
    /*end Spring managed resources*/
    /**
    * Constructor for the LookupBOImpl object.  Sets the debugEnabled
    * variable and inits the logger category.
    */
    public ProductBatchBOImpl()
    {
        super("com.ftd.pdb.businessobject.ProductBatchBOImpl");
    }

    /**
    * Creates a Lookup BO
    */
    public void create()
    {
    }

    /**
    * Returns a string representation of this BO
    *
    * @return a string representation of this BO
    */
    public String toString()
    {
        return null;
    }

    /**
    * Checks to see if this object is internally equal to the parameter object
    * @param object1 object to be compared to this object
    */
    public boolean equals(java.lang.Object object1)
    {
        return false;
    }

    /**
    *   Saves this BO to persistent storage
    */
    public void save()
    {
    }
    
    /**
     * Returns an array of ProductBatchVOs
     * @param conn database connection
     * @param userId to locate
     * @return array of products
     * @exception PDBSystemException
     */
   public List getUserBatch(Connection conn,String userId)
        throws PDBApplicationException, PDBSystemException
    {
        return productBatchDAO.getUserBatch(conn,userId);
    }

    /**
    * Returns a ProductBatchVO
    * @param conn database connection
    * @param userId to locate
    * @param productId to locate
    * @return ProductBatchVO
    * @throws PDBApplicationException, PDBSystemException
    **/
    public ProductBatchVO getProduct(Connection conn, String userId, String productId) 
        throws PDBApplicationException, PDBSystemException
    {
        return productBatchDAO.getProduct(conn,userId,productId);
    }

    /**
    * Returns a ProductBatchVO
    * @param conn database connection
    * @param userId to locate
    * @param novatorId to locate
    * @return ProductBatchVO
    * @throws PDBApplicationException, PDBSystemException
    **/
    public ProductBatchVO getProductByNovatorId(Connection conn, String userId, String novatorId) 
        throws PDBApplicationException, PDBSystemException
    {
        return productBatchDAO.getProductByNovatorId(conn, userId, novatorId);
    }

  /**
   * Updates or Creates a product in the database
   * @param conn database connection
   * @param productVO that will be created or updated
   * @throws PDBApplicationException, PDBSystemException
   **/
    public void setProduct(Connection conn, ProductBatchVO productVO)
        throws PDBApplicationException, PDBSystemException
    {
        productBatchDAO.setProduct(conn,productVO);
    }

    /**
     * Deletes a product from the database
     * @param conn database connection
     * @param userId to delete
     * @param productId to delete
     * @exception PDBSystemException
     */
    public void deleteProduct(Connection conn, String userId, String productId) 
        throws PDBSystemException, PDBApplicationException
    {
        productBatchDAO.deleteProduct(conn, userId, productId);
    }

    public void setProductBatchDAO(IProductBatchDAO productBatchDAO) {
        this.productBatchDAO = productBatchDAO;
    }

    public IProductBatchDAO getProductBatchDAO() {
        return productBatchDAO;
    }
}
