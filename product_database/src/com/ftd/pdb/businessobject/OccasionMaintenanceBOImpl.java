package com.ftd.pdb.businessobject;

import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBSystemException;
import com.ftd.pdb.common.valobjs.OccasionVO;
import com.ftd.pdb.integration.dao.IAddonDAO;
import com.ftd.pdb.integration.dao.IOccasionAddonDAO;
import com.ftd.pdb.integration.dao.IOccasionDAO;

import java.sql.Connection;

import java.util.List;


public class OccasionMaintenanceBOImpl extends PDBBusinessObject implements IOccasionMaintenanceBO
{
    /* Spring manager resources */
     IOccasionDAO occasionDAO;
     IAddonDAO addonDAO;
     IOccasionAddonDAO occAddonDAO;
    /* end Spring managed resources */
    
    /**
     * Constructor for OccasionMaintenanceBOImple object
     */
    public OccasionMaintenanceBOImpl()
    {
        super("com.ftd.pdb.businessobject.OccasionMaintenanceBOImpl");
    }


  /** 
   * Retrieve occasion maintenance data from database
   * @param conn database connection
   * @return List containing OccasionVO's
   */
    public List getOccasions(Connection conn) 
        throws PDBApplicationException, PDBSystemException
    {
        // Load the holiday pricing
        List occasionList = occasionDAO.getOccasions(conn);

        //Add blank item 
        OccasionVO vo = new OccasionVO();
        vo.setId(-1);
        vo.setDescription("-SELECT-");
        occasionList.add(0,vo);

        return occasionList;
    }


  /** 
   * Retrieve list of available cards for given occasion
   * @param conn database connection
   * @return List containing cardvo's
   */
    public List getAvailableCards(Connection conn,int occasion) 
        throws PDBApplicationException, PDBSystemException
    {
        return addonDAO.getAddonsByTypeExcludingOccasion(conn, ProductMaintConstants.ADDON_CARD_FLAG,occasion);
    }

  /** 
   * Retrieve list of selected cards for given occasion
   * @param conn database connection
   * @return List containing cardvo's
   */
    public List getSelectedCards(Connection conn,int occasion) 
        throws PDBApplicationException, PDBSystemException
    {
        return addonDAO.getAddonsByTypeOccasion(conn, ProductMaintConstants.ADDON_CARD_FLAG,occasion);
    }


  /**
   * Updates the occasion card mapping, assigning all the passed
   * in cards to the passed in occasion, all other cards will be
   * removed from the mapping.
   * @param conn database connection
   * @param occasion id
   * @param cards 
   * @return void 
   * @throws PDBApplicationException
   **/
    public void updateOccasionCards(Connection conn,int occasion, String[] cards) 
        throws PDBApplicationException, PDBSystemException
    {
        /* Do not do update for occasion id's less then zero,
           this is the value used to display the 'SELECT' text
           in the page drop down. */
        if (occasion < 0)
        {
          return;
        }

        //Remove all mappings related to this occasoin
        occAddonDAO.deleteByOccasion(conn, occasion);

        //Insert all occasion, addon mappings
        for (int i = 0; i < cards.length ; i++)
        {
            occAddonDAO.insert(conn, occasion,cards[i]);
        }
        
    }


    public void create()
    {
    }

  
    /**
     * Returns a string representation of this BO
     *
     * @return a string representation of this bo
     */
    public String toString()
    {
        return null;
    }

    /**
     * Compares the passed in objec with this object.
     *
     * @return true if objects equal, else returns false
     */
    public boolean equals(java.lang.Object object1)
    {
        return false;
    }

    /**
     * Saves this bo to a persistent state.
     */     
    public void save()
    {
    }

    public void setOccasionDAO(IOccasionDAO occasionDAO) {
        this.occasionDAO = occasionDAO;
    }

    public IOccasionDAO getOccasionDAO() {
        return occasionDAO;
    }

    public void setAddonDAO(IAddonDAO addonDAO) {
        this.addonDAO = addonDAO;
    }

    public IAddonDAO getAddonDAO() {
        return addonDAO;
    }

    public void setOccAddonDAO(IOccasionAddonDAO occAddonDAO) {
        this.occAddonDAO = occAddonDAO;
    }

    public IOccasionAddonDAO getOccAddonDAO() {
        return occAddonDAO;
    }
}
