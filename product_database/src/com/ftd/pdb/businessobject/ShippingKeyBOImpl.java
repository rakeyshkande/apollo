package com.ftd.pdb.businessobject;

import com.ftd.pdb.common.ProductMaintConstants;
import com.ftd.pdb.common.exceptions.PDBApplicationException;
import com.ftd.pdb.common.exceptions.PDBSystemException;
import com.ftd.pdb.common.utilities.NovatorTransmission;
import com.ftd.pdb.common.valobjs.ShippingKeyVO;
import com.ftd.pdb.integration.dao.IShippingKeyDAO;

import java.sql.Connection;

import java.util.List;
import java.util.Map;


public class ShippingKeyBOImpl extends PDBBusinessObject implements IShippingKeyBO
{
    /*Spring managed resources*/
    private IShippingKeyDAO shippingKeyDAO;
    /*end Spring managed resources*/


/**
 * Constructor for ShippingKeyBOImpl object
 */
    public ShippingKeyBOImpl()
    {
        super("com.ftd.pdb.businessobject.ShippingKeyBOImpl");
    }


 /**
   * Returns an array of ShippingKeyVOs
   * @param conn database connection
   * @return array of ShippingKeyVOs
   * @throws PDBApplicationException, PDBSystemException
   **/
   public List getShippingKeyList(Connection conn)
        throws PDBApplicationException, PDBSystemException
   {
        List shippingKeys = shippingKeyDAO.getShippingKeyList(conn);

        if(debugEnabled)
           logger.debug("ShippingKeyBOImpl: complete shipping key list requested");
        return shippingKeys;
   }

   /**
     * return a ShippingKeyVO for a given shippingID
     * @param conn database connection
     * @param  shippingID the selectecd id for the shipping key.
     * @return ShippingKeyVO
     * @throws PDBApplicationException, PDBSystemException
    **/
    public ShippingKeyVO getShippingKey (Connection conn, String shippingID)
        throws PDBApplicationException, PDBSystemException
    {
        ShippingKeyVO key = shippingKeyDAO.getShippingKey( conn, shippingID );

        if(debugEnabled)
            logger.debug("ShippingKeyBOImpl: select shipping key requested ID = " + shippingID);
        return key;
    }


    /**
     * cascading deleting a row in Shipping_key table and rows in any table that has a foreign key reference.
     * @param conn database connection
     * @param shippingKeyVO the selectecd id for the shipping key to be removed from database.
     * @param novatorMap
     * @return Boolean true if succeed, false if failed.
     * @throws PDBApplicationException, PDBSystemException
    **/
    public boolean removeShippingKey (Connection conn, ShippingKeyVO shippingKeyVO, Map novatorMap )
        throws PDBApplicationException, PDBSystemException
    {
        boolean removed = shippingKeyDAO.removeShippingKey( conn, shippingKeyVO );

        if(debugEnabled)
            logger.debug("ShippingKeyBOImpl: complete shipping key removal: " + shippingKeyVO.getShippingKey_ID());

        // Remove the shipping key from App context so it will be loaded next 
        // time it is accessed
//        ApplicationContext ctx = ApplicationContext.getInstance();
//        ctx.removeAttribute(ProductMaintConstants.SHIPPING_KEYS_KEY);
        
        return removed;
    }

   /**
     * Insert a row in Shipping_key table and rows in Shipping_key_details table.
     * @param conn database connection
     * @param shippingKeyVO the selectecd id for the shipping key to be removed from database.
     * @param novatorMap
     * @return  Boolean true if succeed, false if failed.
     * @throws PDBApplicationException, PDBSystemException
    **/
    public boolean insertShippingKey (Connection conn, ShippingKeyVO shippingKeyVO, Map novatorMap )
        throws PDBApplicationException, PDBSystemException
    {
        boolean inserted = shippingKeyDAO.insertShippingKey( conn, shippingKeyVO );

        //Send updates to Novator
        try {
          new NovatorTransmission().send( ProductMaintConstants.NOVATOR_XML_SHIPPING_KEY_HEADER, shippingKeyVO, novatorMap );
        } catch (Exception e) {
           inserted = shippingKeyDAO.removeShippingKey(conn,shippingKeyVO);
           throw new PDBSystemException(ProductMaintConstants.OE_NOVATOR_TRANSMISSION_ERROR,  e);  
        }
        if(debugEnabled)
            logger.debug("ShippingKeyBOImpl: complete shipping key insertion: " + shippingKeyVO.getShippingKey_ID() );

        // Remove the shipping key from App context so it will be loaded next 
        // time it is accessed
//        ApplicationContext ctx = ApplicationContext.getInstance();
//        ctx.removeAttribute(ProductMaintConstants.SHIPPING_KEYS_KEY);
        
        return inserted;
    }

    /**
     * Update a row in Shipping_key table and rows in Shipping_key_details table.
     * @param conn database connection
     * @param shippingKeyVO the selectecd id for the shipping key to be udpated from database.
     * @param novatorMap
     * @return boolean true if succeed, false if failed.
     * @throws PDBApplicationException, PDBSystemException
    **/
    public boolean updateShippingKey (Connection conn, ShippingKeyVO shippingKeyVO, Map novatorMap)
        throws PDBApplicationException, PDBSystemException
    {

        boolean succeeded = false;
        
        //Obtain current shipping key ..needed for rollback
        ShippingKeyVO rollbackShippingKeyVO = this.getShippingKey( conn, shippingKeyVO.getShippingKey_ID() );
        
        // Set the updated shipping key in DB        
        succeeded = shippingKeyDAO.updateShippingKey( conn, shippingKeyVO );

        //String xml = ProductMaintConstants.NOVATOR_XML_SHIPPING_KEY_HEADER + shippingKeyVO.toXML();
        //System.out.println("xml " + xml);
        
        //Send updates to Novator
        try {
          new NovatorTransmission().send( ProductMaintConstants.NOVATOR_XML_SHIPPING_KEY_HEADER, shippingKeyVO, novatorMap );
        } catch (Exception e) {
           succeeded = shippingKeyDAO.updateShippingKey( conn,rollbackShippingKeyVO );
           throw new PDBSystemException(ProductMaintConstants.OE_NOVATOR_TRANSMISSION_ERROR,  e);  
        }
        if(debugEnabled)
            logger.debug("ShippingKeyBOImpl: complete shipping key update: " + shippingKeyVO.getShippingKey_ID() );

        // Remove the shipping key from App context so it will be loaded next 
        // time it is accessed
//        ApplicationContext ctx = ApplicationContext.getInstance();
//        ctx.removeAttribute(ProductMaintConstants.SHIPPING_KEYS_KEY);
        
        return succeeded;
    }
    
   /**
    * Creates a Lookup BO
    */
    public void create()
    {
    }

    /**
    * Returns a string representation of this BO
    *
    * @return a string representation of this BO
    */
    public String toString()
    {
        return this.getClass().getName() + ": " + super.hashCode();
    }

    /**
    * Checks to see if this object is internally equal to the parameter object
    * @param object1 object to be compared to this object
    */
    public boolean equals(java.lang.Object object1)
    {
        return false;
    }

    /**
    *   Saves this BO to persistent storage
    */
    public void save()
    {
    }

    public void setShippingKeyDAO(IShippingKeyDAO shippingKeyDAO) {
        this.shippingKeyDAO = shippingKeyDAO;
    }

    public IShippingKeyDAO getShippingKeyDAO() {
        return shippingKeyDAO;
    }
}
