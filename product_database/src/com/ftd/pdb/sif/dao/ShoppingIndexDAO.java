package com.ftd.pdb.sif.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pdb.sif.common.valobjs.StageShoppingIndexVO;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class ShoppingIndexDAO {
    private Logger logger = new Logger("com.ftd.pdb.sif.dao.ShoppingIndexDAO");
    
    public ShoppingIndexDAO() {
    }
    
    public void persistShoppingIndex(Connection conn, StageShoppingIndexVO indexVO) throws Exception {       
        DataRequest dataRequest = new DataRequest();
        
        /* build DataRequest object */
        HashMap inputParams = new HashMap();
        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("SIF_INSERT_SHOP_INDEX_STAGE");
        inputParams.put("IN_INDEX_NAME", indexVO.getIndexName());
        inputParams.put("IN_SI_TEMPLATE_ID", indexVO.getTemplateId());
        inputParams.put("IN_INDEX_DESC", indexVO.getIndexDesc());
        inputParams.put("IN_COUNTRY_ID", indexVO.getCountryId());
        inputParams.put("IN_DISPLAY_SEQ_NUM", Long.valueOf(indexVO.getDisplaySeqNum()));
        
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();  
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
          
    }
    
    public void persistShoppingIndexProduct(Connection conn, StageShoppingIndexVO indexVO) throws Exception {       
        DataRequest dataRequest = new DataRequest();
        
        ArrayList productIdList = indexVO.getProductIdList();
        if(productIdList != null) {
            for(int i = 0; i < productIdList.size(); i++) {
                String productId = (String)productIdList.get(i);
                /* build DataRequest object */
                HashMap inputParams = new HashMap();
                dataRequest.setInputParams(inputParams);
                dataRequest.setConnection(conn);
                dataRequest.setStatementID("SIF_INSERT_SHOP_INDEX_PROD_STAGE");
                inputParams.put("IN_INDEX_NAME", indexVO.getIndexName());
                inputParams.put("IN_SI_TEMPLATE_ID", indexVO.getTemplateId());
                inputParams.put("IN_PRODUCT_ID", productId);
                inputParams.put("IN_DISPLAY_SEQ_NUM", Long.valueOf(i+1));
                
                /* execute the store prodcedure */
                DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();  
                Map outputs = (Map) dataAccessUtil.execute(dataRequest);
                
                String status = (String) outputs.get("OUT_STATUS");
                if(status != null && status.equalsIgnoreCase("N"))
                {
                    String message = (String) outputs.get("OUT_MESSAGE");
                    throw new Exception(message);
                }
                
            }
        }

    }
    
    public void buildSubIndex(Connection conn, StageShoppingIndexVO indexVO) throws Exception {       

        DataRequest dataRequest = new DataRequest();
        ArrayList childIndexList = indexVO.getChildIndexList();
        if (childIndexList != null) {
            for (int i = 0; i < childIndexList.size(); i++) {
                String childIndex = (String)childIndexList.get(i);
                // build DataRequest object
                HashMap inputParams = new HashMap();
                dataRequest.setInputParams(inputParams);
                dataRequest.setConnection(conn);
                dataRequest.setStatementID("SIF_BUILD_INDEX_SUB_INDEX");
                inputParams.put("IN_INDEX_NAME", indexVO.getIndexName());
                inputParams.put("IN_SI_TEMPLATE_ID", indexVO.getTemplateId());
                inputParams.put("IN_SUB_INDEX_NAME", childIndex);
                inputParams.put("IN_DISPLAY_SEQ_NUM", Long.valueOf(i+1));

                // execute the store prodcedure
                 DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();  
                 Map outputs = (Map) dataAccessUtil.execute(dataRequest);
                 
                 String status = (String) outputs.get("OUT_STATUS");
                 if(status != null && status.equalsIgnoreCase("N"))
                 {
                     String message = (String) outputs.get("OUT_MESSAGE");
                     throw new Exception(message);
                 }
            }
        }
    }
    
    public void convertNovatorProductIds(Connection conn, String templateId) throws Exception {       
        DataRequest dataRequest = new DataRequest();
        
        /* build DataRequest object */
        HashMap inputParams = new HashMap();
        inputParams.put("IN_SI_TEMPLATE_ID", templateId);
        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("SIF_CONVERT_NOVATOR_ID");
        
        /* execute the store prodcedure */
         DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();  
         Map outputs = (Map) dataAccessUtil.execute(dataRequest);
         
         String status = (String) outputs.get("OUT_STATUS");
         if(status != null && status.equalsIgnoreCase("N"))
         {
             String message = (String) outputs.get("OUT_MESSAGE");
             throw new Exception(message);
         }
    }
    
    public void moveIndexFeeds(Connection conn, String templateId) throws Exception {       
        DataRequest dataRequest = new DataRequest();
        
        /* build DataRequest object */
        HashMap inputParams = new HashMap();
        inputParams.put("IN_SI_TEMPLATE_ID", templateId);
        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("SIF_MOVE_SHOPPING_INDEX_STAGE");
        
        /* execute the store prodcedure */
         DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();  
         Map outputs = (Map) dataAccessUtil.execute(dataRequest);
         
         String status = (String) outputs.get("OUT_STATUS");
         if(status != null && status.equalsIgnoreCase("N"))
         {
             String message = (String) outputs.get("OUT_MESSAGE");
             throw new Exception(message);
         }
    }
    
    
    public void updateShoppingIndexTracking(Connection conn, String name) throws Exception {
        DataRequest dataRequest = new DataRequest();
        
        /* build DataRequest object */
        HashMap inputParams = new HashMap();
        inputParams.put("IN_FILE_NAME", name);
        inputParams.put("IN_UPDATED_BY", "SIF");
        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("SIF_INSERT_UPDATE_SHOPPING_INDEX_TRACKING");
        
        /* execute the store prodcedure */
         DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();  
         Map outputs = (Map) dataAccessUtil.execute(dataRequest);
         
         String status = (String) outputs.get("OUT_STATUS");
         if(status != null && status.equalsIgnoreCase("N"))
         {
             String message = (String) outputs.get("OUT_MESSAGE");
             throw new Exception(message);
         }
    }
    
    public void deleteShoppingIndexTracking(Connection conn, Long thresholdHour) throws Exception {
        DataRequest dataRequest = new DataRequest();

        /* build DataRequest object */
        HashMap inputParams = new HashMap();
        inputParams.put("IN_HOUR_THRESHOLD", thresholdHour);
        inputParams.put("IN_UPDATED_BY", "SIF");
        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("SIF_DELETE_SHOPPING_INDEX");
        
        /* execute the store prodcedure */
         DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();  
         Map outputs = (Map) dataAccessUtil.execute(dataRequest);
         
         String status = (String) outputs.get("OUT_STATUS");
         if(status != null && status.equalsIgnoreCase("N"))
         {
             String message = (String) outputs.get("OUT_MESSAGE");
             throw new Exception(message);
         }
    }

    public void buildTemplateIndexStage(Connection conn, String templateId, List dataSet) throws Exception {       

        DataRequest dataRequest = new DataRequest();
        if (dataSet != null) {
            Iterator iter = dataSet.iterator();
            while(iter.hasNext()) {
                StageShoppingIndexVO indexVO = (StageShoppingIndexVO)iter.next();
                
                HashMap inputParams = new HashMap();
                dataRequest.setInputParams(inputParams);
                dataRequest.setConnection(conn);
                dataRequest.setStatementID("SIF_INS_SI_TPL_INDEX_CHILD_STAGE");
                
                inputParams.put("IN_TEMPLATE_ID", templateId);
                inputParams.put("IN_INDEX_NAME", indexVO.getIndexName());
                //inputParams.put("IN_LEFT_NUM", Long.valueOf(indexVO.getLeft()));
                //inputParams.put("IN_RIGHT_NUM", Long.valueOf(indexVO.getRight()));

                // execute the store prodcedure 
                 DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();  
                 Map outputs = (Map) dataAccessUtil.execute(dataRequest);
                 
                 String status = (String) outputs.get("OUT_STATUS");
                 if(status != null && status.equalsIgnoreCase("N"))
                 {
                     String message = (String) outputs.get("OUT_MESSAGE");
                     throw new Exception(message);
                 }
            }
        }
    }   

    /**
     * Retrieves existing template id for given type and key. Returns -1 if template does not exist.
     * Throws excpetion if type is not valid.
     * @param conn
     * @param type
     * @param key
     * @return
     * @throws Exception
     */
    public int getExistingTemplate(Connection conn, String type, String key) throws Exception {       

        logger.debug("In getExistingTemplate(" + type + "," + key + ")");
        DataRequest dataRequest = new DataRequest();

        HashMap inputParams = new HashMap();
                
        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("SIF_GET_EXISTING_TEMPLATE_ID");
                        
        inputParams.put("IN_SI_TEMPLATE_TYPE_CODE", type);
        inputParams.put("IN_SI_TEMPLATE_KEY", key);

        // execute the store prodcedure 
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();  
        String output = (String) dataAccessUtil.execute(dataRequest);
                
        int templateId = -1;
                
        try {
            templateId = Integer.parseInt(output);    
        } catch (Exception e) {
            logger.error(e);
        }
        logger.debug("getExistingTemplate returning " + templateId);
        return templateId;
                  
    }       
    /**
    * Retrieves existing template id for given type and key. Create new template if the 
    * given type and key combination does not exist.
    * Throws excpetion if type is not valid.
    */
    public int insertTemplateStage(Connection conn, String type, String key) throws Exception {       

            DataRequest dataRequest = new DataRequest();
            HashMap inputParams = new HashMap();
            
            dataRequest.setInputParams(inputParams);
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("SIF_INS_SI_TPL_STAGE");
                    
            inputParams.put("IN_SI_TEMPLATE_TYPE_CODE", type);
            inputParams.put("IN_SI_TEMPLATE_KEY", key);

            // execute the store prodcedure 
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();  
            Map output = (Map) dataAccessUtil.execute(dataRequest);
            String templateIdStr = (String)output.get("OUT_SI_TEMPLATE_ID");
            String status = (String)output.get("OUT_STATUS");
            String message = (String)output.get("OUT_MESSAGE");
            
            if(!"Y".equals(status)) {
                throw new Exception("Error insertTemplateStage. Type=" + type + ";key=" + key + ";message=" + message);
            }
            int templateId = -1;
            
            try {
                templateId = Integer.parseInt(templateIdStr);    
            } catch (Exception e) {
                logger.error(e);
            }
            
            return templateId;
              
    }       
    
    public void updateSourceTemplateRef(Connection conn, String sourceCode, String templateIds) throws Exception {       
        logger.debug("In updateSourceTemplateRef(" + sourceCode + "," + templateIds + ")");
        DataRequest dataRequest = new DataRequest();
       
        HashMap inputParams = new HashMap();
        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("SIF_UPD_SI_SOURCE_TPL_REF");
        inputParams.put("IN_SOURCE_CODE", sourceCode);               
        inputParams.put("IN_SI_TEMPLATE_IDS", templateIds);

        // execute the store prodcedure 
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();  
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);

        String status = (String) outputs.get("OUT_STATUS");
        logger.debug("returning status:" + status);
        if(status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);

        }
    }       
        
}
