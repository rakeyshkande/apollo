package com.ftd.pdb.sif.common.valobjs;

import java.util.ArrayList;


public class StageShoppingIndexVO {
    private String indexName;
    private String templateId;
    private String indexDesc;
    private String countryId;
    private ArrayList productIdList;
    private ArrayList childIndexList;
    private long displaySeqNum;

    public void setIndexName(String indexName) {
        this.indexName = indexName;
    }

    public String getIndexName() {
        return indexName;
    }

    public void setIndexDesc(String indexDesc) {
        this.indexDesc = indexDesc;
    }

    public String getIndexDesc() {
        return indexDesc;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setProductIdList(ArrayList productIdList) {
        this.productIdList = productIdList;
    }

    public ArrayList getProductIdList() {
        return productIdList;
    }

    public void setChildIndexList(ArrayList childIndexList) {
        this.childIndexList = childIndexList;
    }

    public ArrayList getChildIndexList() {
        return childIndexList;
    }

    public void setDisplaySeqNum(long displaySeqNum) {
        this.displaySeqNum = displaySeqNum;
    }

    public long getDisplaySeqNum() {
        return displaySeqNum;
    }

/*
    public boolean equals(StageShoppingIndexVO data) {
    
        if(this.indexName.equals(data.getIndexName()) && 
           this.getLeft() == data.getLeft() &&
           this.getRight() == data.getRight()) {
            return true;
        }
        return false;
    }
    
    public StageShoppingIndexVO copy() {
        StageShoppingIndexVO c = new StageShoppingIndexVO();
        
        c.setIndexName(this.getIndexName());
        c.setIndexDesc(this.getIndexDesc());
        c.setCountryId(this.getCountryId());
        c.setDisplaySeqNum(this.getDisplaySeqNum());
        c.setLeft(this.getLeft());
        c.setRight(this.getRight());
        // no need to copy child.
        
        return c;
    }
    */

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getTemplateId() {
        return templateId;
    }
}
