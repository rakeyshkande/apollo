package com.ftd.pdb.sif.businessobject;

import com.enterprisedt.net.ftp.FTPClient;
import com.enterprisedt.net.ftp.FTPConnectMode;
import com.enterprisedt.net.ftp.FTPException;
import com.enterprisedt.net.ftp.FTPFile;
import com.enterprisedt.net.ftp.FTPTransferType;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.LockUtil;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.filearchive.dao.FileArchiveDAO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.pdb.common.resources.IResourceProvider;
import com.ftd.pdb.common.utilities.FTDUtil;
import com.ftd.pdb.pif.dao.ProductIndexFeedDAOImpl;
import com.ftd.pdb.sif.common.valobjs.StageShoppingIndexVO;
import com.ftd.pdb.sif.dao.ShoppingIndexDAO;

import com.sun.org.apache.xpath.internal.XPathAPI;

import java.io.File;

import java.sql.Connection;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import java.util.StringTokenizer;

import org.apache.commons.io.FileUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 * This class contains the business logic to processing the shopping index feed from the website.
 * The difference between this feed and the PIF is that this feed include source code relationship.
 * The process will create a lock at the beginning. It retrieves all the files from remove server,
 * creates a JMS message for each file, and deletes the file from the remove server; each JMS message
 * handler will parse the index in the file and archive the file. If all files are archived, the process
 * completes the final step of replacing the novator product id, and then move the index from stage to
 * production tables. It finally releases the process lock.
 */
public class ShoppingIndexBO {
    private Logger logger;
    private ShoppingIndexDAO sifDAO;
    private ProductIndexFeedDAOImpl pifDAO;
    private FileArchiveDAO fileArchiveDAO;
    private IResourceProvider resourceProvider;
    private static final String GLOBAL_PARMS_CONTEXT = "product_database";
    private static final String PARM_SERVER = "sif_ftp_server";
    private static final String PARM_LOGIN = "sif_ftp_login";
    private static final String PARM_PASSWORD = "sif_ftp_password";
    private static final String PARM_WORKING_DIRECTORY = "sif_working_directory";
    private static final String PARM_REMOTE_DIRECTORY = "sif_remote_directory";
    private static final String PARM_ARCHIVE_DIRECTORY = "sif_archive_directory";
    private static final String PARM_ERROR_DIRECTORY = "sif_error_directory";
    private static final String PARM_REQUEUE_DELAY = "SHOPPING_INDEX_REQUEUE_DELAY_SECONDS";
    private static final String PARM_MAX_RETRY_COUNT = "SHOPPING_INDEX_MAX_RETRY_COUNT";

    public ShoppingIndexBO() {
        logger = new Logger("com.ftd.pdb.sif.businessobject.ShoppingIndexBO");
    }

    /**
     * Retrieves a list of all shopping index files from remote server.
     * Sends a JMS message for each file name.
     * @throws Exception
     */
    public void getShoppingIndex(Connection conn, String sessionId) throws Exception {
        // Read files from remote server
        String host;
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        host = configUtil.getFrpGlobalParm(GLOBAL_PARMS_CONTEXT, PARM_SERVER);

        try {
            String userId = configUtil.getSecureProperty(GLOBAL_PARMS_CONTEXT, PARM_LOGIN);
            String credentials = configUtil.getSecureProperty(GLOBAL_PARMS_CONTEXT,  PARM_PASSWORD);
            String remoteDirectory = configUtil.getFrpGlobalParm(GLOBAL_PARMS_CONTEXT, PARM_REMOTE_DIRECTORY);

            logger.debug("host: " + host);
            logger.debug("userId: " + userId);
            logger.debug("credentials: ##########");
            logger.debug("remoteDirectory: " + remoteDirectory);

            FTPClient ftpClient = new FTPClient();
            ftpClient.setRemoteHost(host);
            ftpClient.connect();
            ftpClient.login(userId, credentials);
            ftpClient.setType(FTPTransferType.ASCII);
            ftpClient.setConnectMode(FTPConnectMode.PASV);
            if (remoteDirectory != null && remoteDirectory.length() > 0) {
                ftpClient.chdir(remoteDirectory);
            }

            String[] fileNames = null;
            try {
                logger.info("Getting the list of files");
                fileNames = ftpClient.dir();
            } catch (Exception e) {
                logger.error("Encountered error while trying to list remote directory", e);
                throw e;
            }

            if (fileNames.length == 0) {
                logger.info("Nothing to download");
            } else {
                for (int i = 0; i < fileNames.length; i++) {
                    String fileName = fileNames[i];
                    if(fileName.endsWith(".inprocess")) {
                        logger.info("Ignored. File write is in process:" + fileName);
                        break;
                    }
                    
                    logger.debug("Creating MDB for file: " + fileName);
                    try {
                        // Send a JMS message for each file: SHOPPING_INDEX_READ|filename|retry count of 0
                        String payload = "SHOPPING_INDEX_READ"+ "|" + fileName + "|0";
                        pifDAO.postAMessage(conn, "OJMS.PDB", payload, payload, 0 );
                    } catch (Exception e) {
                        logger.error("Error posting MDB for index file " + fileName + ".", e);
                        throw e;
                    }

                } // end for
            } // end has file
            
            // close ftp client session
            logger.debug("Quitting ftp session");
            ftpClient.quit();

        } catch (Exception e) {
            logger.error(e);
        } finally {

            // Release process lock.
            LockUtil lockUtil = new LockUtil();
            try {
                logger.debug("Releasing lock: " + sessionId);
                lockUtil.releaseProcessLock(conn, "SHOPPING_INDEX_PROCESS", sessionId);
            } catch (Exception le) {
                logger.error(le);
                try {
                    FTDUtil.sendSystemMessage(resourceProvider.getDatabaseConnection(),"Failed to release lock: " + le.getMessage());
                } catch (Exception se) {
                    logger.error("Failed to send system message: " + se.getMessage());
                }
            }
        }
        
    }

    /**
     * This method will read the local file that has been previously downloaded,
     * save the indexes to stage table, and archive the file. If there are no more
     * files in the local working directory, it performs the index move step.
     * If processing of this file fails, it will not archive the file nor move
     * the index and will send a system message.
     * Delete the file from remote server.
     * @param conn
     * @param fileName
     * @throws Exception
     */
    public void readShoppingIndex(Connection conn, String fileName, int retryCount) throws Exception {
        // Read file from local server
        StageShoppingIndexVO ssiVO = null;
        // This list is used to keep all shipping index for updating parent index when all indexes have been persisted.
        ArrayList<StageShoppingIndexVO> shoppingIndexList = new ArrayList();

        File indexFile = null;
        File localDir = null;
        String archiveLocation = null;
        String userId = null;
        String credentials = null;
        String localDirectory = null;
        String remoteDirectory = null;
        FTPClient ftpClient = new FTPClient();
        String remoteFileName = null;
        String host = null;
        
        try {
        	// First check if retry has exceeded. If so send system message and quit.
            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();

            host = configUtil.getFrpGlobalParm(GLOBAL_PARMS_CONTEXT, PARM_SERVER);

             userId = configUtil.getSecureProperty(GLOBAL_PARMS_CONTEXT, PARM_LOGIN);
             credentials = configUtil.getSecureProperty(GLOBAL_PARMS_CONTEXT,  PARM_PASSWORD);
             localDirectory = configUtil.getFrpGlobalParm(GLOBAL_PARMS_CONTEXT,  PARM_WORKING_DIRECTORY);
             remoteDirectory = configUtil.getFrpGlobalParm(GLOBAL_PARMS_CONTEXT, PARM_REMOTE_DIRECTORY);

             logger.debug("host: " + host);
             logger.debug("userId: " + userId);
             logger.debug("credentials: ##########");
             logger.debug("localDirectory: " + localDirectory);
             logger.debug("remoteDirectory: " + remoteDirectory);

             ftpClient.setRemoteHost(host);
             ftpClient.connect();
             ftpClient.login(userId, credentials);
             ftpClient.setType(FTPTransferType.ASCII);
             ftpClient.setConnectMode(FTPConnectMode.PASV);
             if (remoteDirectory != null && remoteDirectory.length() > 0) {
                 ftpClient.chdir(remoteDirectory);
             }

            //Check to see if the download directory exists
            File fLocal = new File(localDirectory);
            if (!fLocal.exists()) {
                FileUtils.forceMkdir(fLocal);
            }

            remoteFileName = fileName;       
            
            //Download the file from the server
            logger.info("Downloading file: " + fileName);
            try {
                ftpClient.get(localDirectory + "/" + fileName, remoteFileName);
            } catch (FTPException fe) {
                if(fe.getMessage().indexOf("No such file") >= 0) {
                    //File has been processed. OK to ignore.
                    logger.info("File has been processed. Quitting.");
                    return;
                } else {
                    throw fe;
                }
            }
            logger.info("File downloaded: " + fileName);
            
            // close ftp client session
            logger.debug("Quitting ftp session");
            ftpClient.quit();

            logger.info("Saving file to stage:" + fileName);
            localDir = new File(localDirectory);
            indexFile = new File(localDirectory + "/" + fileName);
            String siTemplateIdList = "";
            String sourceCode = null;
            boolean stopRetry = false;
            Document doc = null;
            
            try {
                doc = DOMUtil.getDocument(indexFile);
            } catch (Exception pe) {
                // If file cannot be parsed, retry.
                logger.info("File cannot be parsed. Will retry.");
                logger.error(pe);
                stopRetry = this.retryIndexFile(conn, configUtil, fileName, retryCount);
                // Max retry reached. Move file to error folder. Keep file on remote server.
                if(stopRetry) {
                    //moveIndexFileToTargetFolder(indexFile, PARM_ERROR_DIRECTORY);
                    archiveLocation = PARM_ERROR_DIRECTORY;
                }                
                return;
            }
            
            // Determine if this is an index definition or is a source code rule.
            // Index definitions: domain, template, default, custom index definition. Look for index-type tag.
            // Source code rule: defines what index definition the source follows. Look for default-index-type-list tag.
            NodeList indexRefList = doc.getElementsByTagName("index-type-ref");
            
            if (indexRefList != null && indexRefList.getLength() > 0) {
                // This file is a source code rule.
                //hasIndexRef = true;
                try {
                    Element sourceCodeElem = 
                     (Element)doc.getElementsByTagName("source-code").item(0);
                    sourceCode = sourceCodeElem.getFirstChild().getNodeValue();
                } catch (Exception se) {
                    logger.warn("No source-code tag.");
                }
                // If no source-code tag, the source code is the value field in /index-list/index-type-def
                if(sourceCode == null) {
                    try {
                        Node sourceCodeNode = XPathAPI.selectSingleNode(doc,
                            "/index-list/index-type-def/value/text()");
                        sourceCode = sourceCodeNode!=null?sourceCodeNode.getNodeValue():"";
                    } catch (Exception se) {
                        logger.error("Source code not found.");
                        throw se;
                    }
                }
                
                indexRefList = ((Element)indexRefList.item(0)).getElementsByTagName("index-type");
                
                for (int i = 0; i < indexRefList.getLength(); i++) {
                    // If any index definition does not exist, requeue JMS message.
                    String templateType = (((Element)indexRefList.item(i)).getElementsByTagName("name")).item(0).getFirstChild().getNodeValue();
                    String templateKey = (((Element)indexRefList.item(i)).getElementsByTagName("value")).item(0).getFirstChild().getNodeValue();
                    templateType = templateType.trim();
                    templateKey = templateKey.trim();
                    logger.debug("template type:" + templateType);
                    logger.debug("template key:" + templateKey);
                    
                    // Find template using type and key. 
                    int templateId = sifDAO.getExistingTemplate(conn, templateType, templateKey);
                    
                    if(templateId < 0) {
                        // The template it refers to has not been populated. Retry.
                        
                        logger.info("Requeuing message for file:" + fileName);
                        stopRetry = this.retryIndexFile(conn, configUtil, fileName, retryCount);
                        // Max retry reached. Move file to error folder. Keep file on remote server.
                        if(stopRetry) {
                            //moveIndexFileToTargetFolder(indexFile, PARM_ERROR_DIRECTORY);
                             archiveLocation = PARM_ERROR_DIRECTORY;
                        }   
                        return;
                    } else {
                        // Found template. Add to list.
                        siTemplateIdList = siTemplateIdList == ""? String.valueOf(templateId) : (siTemplateIdList + "," + templateId);
                    }
                    
                }

            } 
            
            // Check if this file has index definition.
            NodeList indexTypeDef = null;
            String templateType = null;
            String templateKey = null;

            indexTypeDef = doc.getElementsByTagName("index-type-def");
                
            if (indexTypeDef != null && indexTypeDef.getLength() > 0) {
                templateType = (((Element)indexTypeDef.item(0)).getElementsByTagName("name")).item(0).getFirstChild().getNodeValue();
                templateKey = (((Element)indexTypeDef.item(0)).getElementsByTagName("value")).item(0).getFirstChild().getNodeValue();
                
                logger.debug("templateType:" + templateType);
                logger.debug("templateKey:" + templateKey);
              
                NodeList indexList = doc.getElementsByTagName("index");
                // Has index definition.
                if (indexList != null) {
                    // Retrieve template id by type and key. If not exists, get new id. Create template in stage.
                    int templateId = sifDAO.insertTemplateStage(conn, templateType, templateKey);
                    
                    if(sourceCode == null) {
                        sourceCode = templateKey;
                    }
                    logger.debug("templateId:" + templateId);
                    //hasIndexDef = true;
                    for (int i = 0; i < indexList.getLength(); i++) {
                        ssiVO = new StageShoppingIndexVO();
                        ArrayList<String> subIndexArrayList = new ArrayList();
                        ArrayList<String> productArrayList = new ArrayList();
                        Element indexElem = (Element)indexList.item(i);
                        String indexName = null;
                        String indexDesc = null;
                        String countryId = null;
                        
                        try {
                            indexName = indexElem.getElementsByTagName("index-name").item(0).getFirstChild().getNodeValue();
                            indexName = indexName.trim();
                        } catch (Exception ne) {
                            throw new Exception("Index name cannot be null. file:" + fileName);
                        }
                        
                        try {
                            indexDesc = indexElem.getElementsByTagName("index-description").item(0).getFirstChild().getNodeValue();
                            indexDesc = indexDesc.trim();
                        } catch (Exception ne) {
                            // Index description is null.
                            logger.debug("No description, defaulting to null");
                        }
                        
                        try {
                            countryId = indexElem.getElementsByTagName("country-id").item(0).getFirstChild().getNodeValue();
                            countryId = countryId.trim();
                        } catch (Exception ne) {
                            countryId = "US";
                        }
                        
            
                        ssiVO.setTemplateId(String.valueOf(templateId));;
                        ssiVO.setDisplaySeqNum(i + 1);
                        ssiVO.setIndexName(indexName);
                        ssiVO.setIndexDesc(indexDesc);
                        ssiVO.setCountryId(countryId);
                        
                        logger.debug("indexName:" + ssiVO.getIndexName());
                        logger.debug("indexDesc:" + ssiVO.getIndexDesc());
                        logger.debug("countryId:" + ssiVO.getCountryId());
                        logger.debug("displaySeqNum:" + ssiVO.getDisplaySeqNum());
            
                        NodeList subIndexNodeList = 
                            indexElem.getElementsByTagName("sub-index");
                        for (int j = 0; j < subIndexNodeList.getLength(); j++) {
                            String subIndexName = 
                                subIndexNodeList.item(j).getFirstChild().getNodeValue();
                            subIndexArrayList.add(subIndexName);
                            
                        }
                        NodeList productNodeList = 
                            indexElem.getElementsByTagName("product");
                        for (int k = 0; k < productNodeList.getLength(); k++) {
                            String productId = 
                                productNodeList.item(k).getFirstChild().getNodeValue();
                            productArrayList.add(productId);
                        }
            
                        ssiVO.setChildIndexList(subIndexArrayList);
                        ssiVO.setProductIdList(productArrayList);
            
                        // Persists index source
                        sifDAO.persistShoppingIndex(conn, ssiVO);
                        // Persists index source product
                        sifDAO.persistShoppingIndexProduct(conn, ssiVO);
            
                        // Saves the shopping index to list for updating parent index.
                        shoppingIndexList.add(ssiVO);
            
                    }
            
                    // All indexes have been persisted. Update parent index.
                    for (int k = 0; k < shoppingIndexList.size(); k++) {
                        StageShoppingIndexVO shoppingIndexVO = shoppingIndexList.get(k);
                        // Update sub index
                        logger.debug("index-name:" + shoppingIndexVO.getIndexName());
                        sifDAO.buildSubIndex(conn, shoppingIndexVO);
                    }         
                    
                    logger.debug("Converting Novator Product ids");
                    sifDAO.convertNovatorProductIds(conn, String.valueOf(templateId));

                    // move index from stage to production tables.
                    logger.debug("Moving staging data");
                    sifDAO.moveIndexFeeds(conn, String.valueOf(templateId));  
                    
                    // Only update source template ref for custom or default or domain type
                    // custom and default are true source codes; domain is used in text search.
                    if("custom".equalsIgnoreCase(templateType) || "default".equalsIgnoreCase(templateType) || "domain".equalsIgnoreCase(templateType)) {
                        siTemplateIdList = siTemplateIdList == ""? String.valueOf(templateId) : (siTemplateIdList + "," + templateId);
                    }
            
                }
            
            }            

                
            //if(siTemplateIdList == null || siTemplateIdList.length() == 0) {
            //    throw new Exception(" Invalid file format");
            //}
            // Update source rule for custom source codes and default source codes themselves.
            if(siTemplateIdList != null && siTemplateIdList.length() > 0) {
                sifDAO.updateSourceTemplateRef(conn, sourceCode, siTemplateIdList);
            }
            
            // Update source code index file time
            sifDAO.updateShoppingIndexTracking(conn, sourceCode);
            
            archiveLocation = PARM_ARCHIVE_DIRECTORY;

        } catch (Exception e) {
            logger.error("Error processing file (" + fileName + "):");
            logger.error(e);
            // Send page.
            String errorMsg = "fileName: " + fileName + " " + e.getMessage();
            if (ssiVO != null) {
                errorMsg = 
                        errorMsg + "\n\r index-name:" + ssiVO.getIndexName() + 
                        "; Display sequence: " + ssiVO.getDisplaySeqNum();
            }
                                      
            archiveLocation = PARM_ERROR_DIRECTORY;
            throw e;
        } finally {  
            // Archive file
            if(archiveLocation != null) {
                moveIndexFileToTargetFolder(indexFile, archiveLocation);
                
                //Delete the file off of the remote file system
                logger.debug("Removing file " + remoteFileName + " from FTP server.");
                ftpClient = new FTPClient();
                ftpClient.setRemoteHost(host);
                ftpClient.connect();
                ftpClient.login(userId, credentials);
                ftpClient.setType(FTPTransferType.ASCII);
                ftpClient.setConnectMode(FTPConnectMode.PASV);
                if (remoteDirectory != null && remoteDirectory.length() > 0) {
                    ftpClient.chdir(remoteDirectory);
                }
                ftpClient.delete(remoteFileName);
                

                // close ftp client session
                logger.debug("Quitting ftp session");
                ftpClient.quit();
            }
        }
    }

   /**
   * Moves the file to the specific folder. 
   * @param indexFile
   * @param targetFolderParamName
   * @throws Exception
   */
    private void moveIndexFileToTargetFolder(File indexFile, String targetFolderParamName) throws Exception {      
        if(indexFile == null || (indexFile.exists() && indexFile.isFile()) == false) {
            return; // Nothing to move
        }
    
        ConfigurationUtil configUtil = new ConfigurationUtil(); 
        String targetDirectory = configUtil.getFrpGlobalParm(GLOBAL_PARMS_CONTEXT, targetFolderParamName);

        // Destination directory
        File targetDir = new File(targetDirectory);
        if (!targetDir.exists()) {
            FileUtils.forceMkdir(targetDir);
        }
            
        SimpleDateFormat sdf = new SimpleDateFormat ("yyyyMMdd-HHmmss");
        Date now = new Date();
        String newFileName = indexFile.getName() + "-" + sdf.format(now);
        //String newFileName = indexFile.getName();
        // Move file to target directory
        logger.debug("moving file to " + targetDirectory + "/" + newFileName);
        boolean success = indexFile.renameTo(new File(targetDirectory, newFileName));
        if (!success) {
            // File was not successfully moved
            throw new Exception("Failed to move file:" + indexFile.getName() + " to: " + targetDirectory);
        }

    }
    public void deleteShoppingIndex(Connection conn) throws Exception {
        // Get threshold
        logger.info("Begin delete shopping index...");
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        String thresholdHour = configUtil.getFrpGlobalParmNoNull("product_database", "sif_delete_threshold_hour");
        Long thresholdHoursLong = Long.valueOf("0");
        try {
            thresholdHoursLong = Long.valueOf(thresholdHour);
        } catch (Exception e) {
            logger.error(e);
            thresholdHoursLong = Long.valueOf("25");
        }
        
        sifDAO.deleteShoppingIndexTracking(conn, thresholdHoursLong);
        logger.info("End delete shopping index.");
    }
    
    
    /**
     * Requeue JMS message to process index file. Increment retry. 
     * If retry count has exceeded threshold, send a system message.
     * @param conn
     * @param configUtil
     * @param fileName
     * @param retry
     * @throws Exception
     */
    private boolean retryIndexFile(Connection conn, ConfigurationUtil configUtil, String fileName, int retry) throws Exception {
        logger.info("Requeuing message for file:" + fileName);
        String requeueDelayStr = configUtil.getFrpGlobalParm(GLOBAL_PARMS_CONTEXT, PARM_REQUEUE_DELAY);
        String maxRetryCountStr = configUtil.getFrpGlobalParm(GLOBAL_PARMS_CONTEXT, PARM_MAX_RETRY_COUNT);
        
        int requeueDelaySeconds = 0;
        int nextRetryCount = retry + 1;
        int maxRetryCount = 0;
        boolean stopRetry = false;
        

        try {
            maxRetryCount = Integer.parseInt(maxRetryCountStr);
        } catch (Exception e) {
            maxRetryCount = 10;
        }
        try {
            requeueDelaySeconds = Integer.parseInt(requeueDelayStr);
        } catch (Exception e) {
            requeueDelaySeconds = 300;
        }        
        
        if(nextRetryCount >= maxRetryCount) {
            // Max retry exceeded. Send system message.
            String errorMsg = "Index file max retry reached. The file either has invalid format or it references a template that has not been processed. " + fileName;
            try {
                 FTDUtil.sendSystemMessage(resourceProvider.getDatabaseConnection(),errorMsg);
                 stopRetry = true;                  
            } catch (Exception e3) {
                  logger.error("Failed to send system message: "+e3.getMessage(),e3);
            }
            
        } else {
            String payload = "SHOPPING_INDEX_READ"+ "|" + fileName + "|" + String.valueOf(nextRetryCount);
            pifDAO.postAMessage(conn, "OJMS.PDB", payload, payload, requeueDelaySeconds);  
        }
        return stopRetry;
    }

    public void setPifDAO(ProductIndexFeedDAOImpl pifDAO) {
        this.pifDAO = pifDAO;
    }

    public ProductIndexFeedDAOImpl getPifDAO() {
        return pifDAO;
    }
    
    public void setSifDAO(ShoppingIndexDAO sifDAO) {
        this.sifDAO = sifDAO;
    }
    
    public ShoppingIndexDAO getsifDAO() {
        return sifDAO;
    }

    public void setFileArchiveDAO(FileArchiveDAO fileArchiveDAO) {
        this.fileArchiveDAO = fileArchiveDAO;
    }

    public FileArchiveDAO getFileArchiveDAO() {
        return fileArchiveDAO;
    }

    public void setResourceProvider(IResourceProvider resourceProvider) {
        this.resourceProvider = resourceProvider;
    }

    public IResourceProvider getResourceProvider() {
        return resourceProvider;
    }
}
