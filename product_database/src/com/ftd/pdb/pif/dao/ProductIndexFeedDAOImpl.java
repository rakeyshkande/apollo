package com.ftd.pdb.pif.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.pdb.pif.common.valobjs.CompanyConfigVO;
import com.ftd.pdb.pif.common.valobjs.IndexKeyViolationVO;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;


public class ProductIndexFeedDAOImpl implements IProductIndexFeedDAO {
    public ProductIndexFeedDAOImpl() {
    }
    
    public void populateProductIndexXref(Connection conn) throws Exception {       
        DataRequest dataRequest = new DataRequest();
        
        /* build DataRequest object */
        HashMap inputParams = new HashMap();
        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("PIF_POPULATE_STAGE_PRODUCT_INDEX_XREF");
        
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        dataAccessUtil.execute(dataRequest);
    }
    
    public void populateStageProductIndex(Connection conn) throws Exception {       
        DataRequest dataRequest = new DataRequest();
        
        /* build DataRequest object */
        HashMap inputParams = new HashMap();
        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("PIF_POPULATE_STAGE_PRODUCT_INDEX");
        
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        dataAccessUtil.execute(dataRequest);
    }
    
    public void populateStageProductIndexXrefIntl(Connection conn) throws Exception {       
        DataRequest dataRequest = new DataRequest();
        
        /* build DataRequest object */
        HashMap inputParams = new HashMap();
        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("PIF_POPULATE_STAGE_PRODUCT_INDEX_XREF_INTL");
        
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        dataAccessUtil.execute(dataRequest);
    }
    
    public void populateStageProductIndexIntl(Connection conn) throws Exception {       
        DataRequest dataRequest = new DataRequest();
        
        /* build DataRequest object */
        HashMap inputParams = new HashMap();
        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("PIF_POPULATE_STAGE_PRODUCT_INDEX_INTL");
        
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        dataAccessUtil.execute(dataRequest);
    }
    
    public void updateLoadProductIdxProduct(Connection conn) throws Exception {       
        DataRequest dataRequest = new DataRequest();
        
        /* build DataRequest object */
        HashMap inputParams = new HashMap();
        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("PIF_UPDATE_LOAD_PROD_IDX_PRODUCT");
        
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        dataAccessUtil.execute(dataRequest);
    }
    
    public void updateLoadProductIndex(Connection conn) throws Exception {       
        DataRequest dataRequest = new DataRequest();
        
        /* build DataRequest object */
        HashMap inputParams = new HashMap();
        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("PIF_UPDATE_LOAD_PRODUCT_INDEX");
        
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        dataAccessUtil.execute(dataRequest);
    }
    
    public void updateStageProductIndex(Connection conn) throws Exception {       
        DataRequest dataRequest = new DataRequest();
        
        /* build DataRequest object */
        HashMap inputParams = new HashMap();
        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("PIF_UPDATE_STAGE_PRODUCT_INDEX");
        
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        dataAccessUtil.execute(dataRequest);
    }
    
    public void updateStageProductIndexCompany(Connection conn, String companyId, String companyPrefix) throws Exception {       
        DataRequest dataRequest = new DataRequest();
        
        /* build DataRequest object */
        HashMap inputParams = new HashMap();
        inputParams.put("IN_COMPANY_ID",companyId);
        inputParams.put("IN_COMPANY_PREFIX",companyPrefix);
        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("PIF_UPDATE_STAGE_PRODUCT_INDEX_COMPANY");
        
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        dataAccessUtil.execute(dataRequest);
    }
    
    public void emptyLoadTables(Connection conn) throws Exception {       
        DataRequest dataRequest = new DataRequest();
        
        /* build DataRequest object */
        HashMap inputParams = new HashMap();
        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("PIF_EMPTY_LOAD_TABLES");
        
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        dataAccessUtil.execute(dataRequest);
    }
    
    public void backupProductIndexTables(Connection conn) throws Exception {     
        DataRequest dataRequest = new DataRequest();
        
        /* build DataRequest object */
        HashMap inputParams = new HashMap();
        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("PIF_BACKUP_PRODUCT_INDEX_TABLES");
        
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        dataAccessUtil.execute(dataRequest);
    }
    
    public void copyToProductIndexTables(Connection conn) throws Exception {     
        DataRequest dataRequest = new DataRequest();
        
        /* build DataRequest object */
        HashMap inputParams = new HashMap();
        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("PIF_COPY_TO_PRODUCT_INDEX_TABLES");
        
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        dataAccessUtil.execute(dataRequest);
    }
    
    public void insertProductIndex(Connection conn, 
                                   String indexFileName, 
                                   String indexName,
                                   String parentIndexFileName,
                                   String countryId,
                                   Long displayOrder) throws Exception {       
        DataRequest dataRequest = new DataRequest();
        
        /* build DataRequest object */
        HashMap inputParams = new HashMap();
        inputParams.put("IN_INDEX_FILE_NAME",indexFileName);
        inputParams.put("IN_INDEX_NAME",indexName);
        inputParams.put("IN_PARENT_INDEX_FILE_NAME",parentIndexFileName);
        inputParams.put("IN_COUNTRY_ID",countryId);
        inputParams.put("IN_DISPLAY_ORDER",displayOrder);
        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("PIF_INSERT_PRODUCT_INDEX");
        
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        dataAccessUtil.execute(dataRequest);
    }
    
    public void insertProductIndexProduct(Connection conn, 
                                          String indexFileName, 
                                          String productId,
                                          Long displayOrder,
                                          boolean loadFailed) throws Exception {       
        DataRequest dataRequest = new DataRequest();
        
        /* build DataRequest object */
        HashMap inputParams = new HashMap();
        inputParams.put("IN_INDEX_FILE_NAME",indexFileName);
        inputParams.put("IN_PRODUCT_ID",productId);
        inputParams.put("IN_DISPLAY_ORDER",displayOrder);
        inputParams.put("IN_LOAD_FAILED",loadFailed?"Y":"N");
        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("PIF_INSERT_PRODUCT_INDEX_PRODUCT");
        
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        dataAccessUtil.execute(dataRequest);
    }
    
    public HashMap getLoadFailedProducts(Connection conn) throws Exception {   
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        HashMap map = new HashMap();
        
        /* build DataRequest object */
        HashMap inputParams = new HashMap();
        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("PIF_GET_LOAD_FAILED_PRODUCTS");
        
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
        
        String index_id;
        String product_id;
        if( rs!=null ) {
            while( rs.next() ) {
                index_id = rs.getString("INDEX_ID");
                product_id = rs.getString("LOOKUP_PRODUCT_ID");
                map.put(index_id, product_id);
            }
        }
        
        return map;
    }
    
    public List<IndexKeyViolationVO> getIndexKeyViolations(Connection conn) throws Exception {   
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        List<IndexKeyViolationVO> results = new ArrayList<IndexKeyViolationVO>();
        
        /* build DataRequest object */
        HashMap inputParams = new HashMap();
        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("PIF_GET_PK_VIOL_FOR_XREF2_LOAD");
        
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
        
        if( rs != null ) {
            while( rs.next() ) {
                IndexKeyViolationVO vo = new IndexKeyViolationVO();
                vo.setCountry(rs.getString("COUNTRY")); 
                vo.setIndexId(rs.getString("INDEX_ID")); 
                vo.setProductId(rs.getString("PRODUCT_ID")); 
                results.add(vo);
            }
        }
        
        return results;
    }
    
    public List<CompanyConfigVO> getPifCompanyData(Connection conn) throws Exception {
        DataRequest dataRequest = new DataRequest();
        List<CompanyConfigVO> results = new ArrayList<CompanyConfigVO>();
        HashMap<String,CompanyConfigVO> map = new HashMap<String,CompanyConfigVO>();
        
        /* build DataRequest object */
        HashMap inputParams = new HashMap();
        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("PIF_GET_COMPANY_DATA");
        
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        Map outputs = (Map) DataAccessUtil.getInstance().execute(dataRequest);
        CachedResultSet companies = (CachedResultSet)outputs.get("OUT_COMPANIES");
        
        if( companies != null ) {
            while( companies.next() ) {
                CompanyConfigVO vo = new CompanyConfigVO();
                vo.setId(companies.getString("PIF_COMPANY_ID")); 
                vo.setFileName(companies.getString("FILE_NAME_TXT")); 
                vo.setFieldCount(companies.getInt("FIELD_COUNT"));
                vo.setPrefixUsed(StringUtils.equals("Y",companies.getString("PREFIX_USED_FLAG")));
                vo.setPrefix(companies.getString("PREFIX_TXT")); 
                vo.setAdvOccasionSearchFlag(StringUtils.equals("Y",companies.getString("ADV_SEARCH_OCCASION_FLAG")));
                vo.setActive(StringUtils.equals("Y",companies.getString("ACTIVE_FLAG")));
                results.add(vo);
                map.put(vo.getId(),vo);
            }
            
            CachedResultSet occasions = (CachedResultSet)outputs.get("OUT_OCCASIONS");
            if( occasions != null ) {
                while( occasions.next() ) {
                    String companyId = occasions.getString("PIF_COMPANY_ID");
                    String occasionId = occasions.getString("OCCASION_IDX_FILE_NAME");
                    
                    CompanyConfigVO company = map.get(companyId);
                    if(company!=null) {
                        company.addOccasion(occasionId);
                    }
                }
            }
            
            CachedResultSet products = (CachedResultSet)outputs.get("OUT_PRODUCTS");
            if( products!=null ) {
                while( products.next() ) {
                    String companyId = products.getString("PIF_COMPANY_ID");
                    String productId = products.getString("PRODUCT_IDX_FILE_NAME");
                    
                    CompanyConfigVO company = map.get(companyId);
                    if(company!=null) {
                        company.addProduct(productId);
                    }
                }
            }
        }
        
        return results;
        
    }
    
    public void postAMessage(Connection conn, String queueName, String correlationId, String payload, long delaySeconds ) throws Exception {
        StringBuilder sb = new StringBuilder();
        sb.append("Calling postamessage");
        sb.append("\r\nqueueName: ");
        sb.append(queueName);
        sb.append("\r\ncorrelationId: ");
        sb.append(correlationId);
        sb.append("\r\npayload: ");
        sb.append(payload);
        sb.append("\r\ndelaySeconds: ");
        sb.append(delaySeconds);
        DataRequest dataRequest = new DataRequest();
        
        /* build DataRequest object */
        HashMap inputParams = new HashMap();
        inputParams.put("IN_QUEUE_NAME",queueName);
        inputParams.put("IN_CORRELATION_ID",correlationId);
        inputParams.put("IN_PAYLOAD",payload);
        inputParams.put("IN_DELAY_SECONDS",new Long(delaySeconds));
        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("PIF_POST_A_MESSAGE");
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
    }
}
