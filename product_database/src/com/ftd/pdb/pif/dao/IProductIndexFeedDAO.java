package com.ftd.pdb.pif.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.pdb.pif.common.valobjs.CompanyConfigVO;
import com.ftd.pdb.pif.common.valobjs.IndexKeyViolationVO;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

public interface IProductIndexFeedDAO {
    
    public void populateProductIndexXref(Connection conn) throws Exception;
    
    public void populateStageProductIndex(Connection conn) throws Exception;
    
    public void populateStageProductIndexXrefIntl(Connection conn) throws Exception;
    
    public void populateStageProductIndexIntl(Connection conn) throws Exception;
    
    public void updateLoadProductIdxProduct(Connection conn) throws Exception;
    
    public void updateLoadProductIndex(Connection conn) throws Exception;
    
    public void updateStageProductIndex(Connection conn) throws Exception;
    
    public void updateStageProductIndexCompany(Connection conn, String companyId, String companyPrefix) throws Exception;
    
    public void emptyLoadTables(Connection conn) throws Exception;
    
    public void backupProductIndexTables(Connection conn) throws Exception;
    
    public void copyToProductIndexTables(Connection conn) throws Exception;
    
    public void insertProductIndex(Connection conn, 
                                   String indexFileName, 
                                   String indexName,
                                   String parentIndexFileName,
                                   String countryId,
                                   Long displayOrder) throws Exception;
    
    public void insertProductIndexProduct(Connection conn, 
                                          String indexFileName, 
                                          String productId,
                                          Long displayOrder,
                                          boolean loadFailed) throws Exception;
    
    public HashMap getLoadFailedProducts(Connection conn) throws Exception;
    
    public List<IndexKeyViolationVO> getIndexKeyViolations(Connection conn) throws Exception;
    
    public List<CompanyConfigVO> getPifCompanyData(Connection conn) throws Exception;
    
    public void postAMessage(Connection conn, String queueName, String correlationId, String payload, long delaySeconds ) throws Exception;
}
