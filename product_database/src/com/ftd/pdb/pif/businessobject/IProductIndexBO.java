package com.ftd.pdb.pif.businessobject;

import java.sql.Connection;

public interface IProductIndexBO {
    
    public int get(Connection conn) throws Exception;
    
    public int load(Connection conn, Long fileArchiveId) throws Exception;
    
    public void stage(Connection conn) throws Exception;
    
    public void backup(Connection conn) throws Exception;
    
    public void move(Connection conn) throws Exception;
}
