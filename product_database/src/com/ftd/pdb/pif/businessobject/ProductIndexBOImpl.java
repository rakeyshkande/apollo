package com.ftd.pdb.pif.businessobject;

import com.csvreader.CsvReader;

import com.enterprisedt.net.ftp.FTPClient;
import com.enterprisedt.net.ftp.FTPConnectMode;
import com.enterprisedt.net.ftp.FTPTransferType;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.filearchive.dao.FileArchiveDAO;
import com.ftd.osp.utilities.filearchive.vo.FileArchiveVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.pdb.common.resources.IResourceProvider;
import com.ftd.pdb.pif.common.NovatorIndexRecord;
import com.ftd.pdb.pif.common.PifUtils;
import com.ftd.pdb.pif.common.valobjs.CompanyConfigVO;
import com.ftd.pdb.pif.common.valobjs.IndexKeyViolationVO;
import com.ftd.pdb.pif.dao.ProductIndexFeedDAOImpl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;


public class ProductIndexBOImpl implements IProductIndexBO {
    private Logger logger;
    private ProductIndexFeedDAOImpl pifDAO;
    private FileArchiveDAO fileArchiveDAO;
    private IResourceProvider resourceProvider;
    private static final String CONFIG_FILE = "pif_config.xml";
//    private static final String FILE_SEPERATOR = System.getProperty("file.separator");
    private static final String GLOBAL_PARMS_CONTEXT = "product_database";
    private static final String PARM_SERVER = "pif_ftp_server";
    private static final String PARM_LOGIN = "pif_ftp_login";
    private static final String PARM_PASSWORD = "pif_ftp_password";
    private static final String PARM_WORKING_DIRECTORY = "pif_working_directory";
    private static final String PARM_REMOTE_DIRECTORY = "pif_remote_directory";
    private static final String PARM_DELETE_REMOTE_FILE = "pif_delete_remote_file";
    private static final String PARM_FILE_PATTERN = "pif_file_pattern"; 
    
    public ProductIndexBOImpl() throws Exception {
        logger = new Logger("com.ftd.pdb.pif.businessobject.ProductIndexBOImpl");
    }
    
    public int get(Connection conn) throws Exception {
        return getIndexFiles(conn);
    }
    
    public int load(Connection conn, Long fileArchiveId) throws Exception {
        return loadIndexes(conn,fileArchiveId);
    }
    
    public void stage(Connection conn) throws Exception {
        stageIndexes(conn);
    }
    
    public void backup(Connection conn) throws Exception {
        backupProductIndexTables(conn);
    }
    
    public void move(Connection conn) throws Exception {
        loadProductIndexTablesFromStagingTables(conn);
    }

    private void populateStageProductIndexXREF(Connection conn)
        throws Exception
    {

        logger.info("begin population of the stage_product_index_xref table");
        
        try
        {
            logger.info("updating load_prod_index_product");
            pifDAO.updateLoadProductIdxProduct(conn);

            logger.info("populating (cursor) stage_product_index_xref from load_prod_index_product");
            cursorPopulateStageProductIndexXREF(conn);
            
            logger.info("populating stage_product_index_xref from INTL_PRODUCT_INDEX_XREF");
            pifDAO.populateStageProductIndexXrefIntl(conn);
        }
        catch(Exception e)
        {

            int cntFailed = 0;
            logger.error("EXCEPTION caught in PIF.populateStageProductIndexXREF()",e);
            logger.debug("Checking what international products would fail to load");
            
            Iterator it = pifDAO.getIndexKeyViolations(conn).iterator();
            
            while( it.hasNext() ) {
                IndexKeyViolationVO vo = (IndexKeyViolationVO)it.next();
                logger.info("LOAD FAILED (already exists): COUNTRY:" + vo.getCountry() + ". INDEX ID:" + vo.getIndexId() + ". Product ID:" + vo.getProductId() + ".");
            }
            
            logger.error("load results: international products failed:" + cntFailed);
            throw e;
        }
    }

    private void cursorPopulateStageProductIndexXREF(Connection conn)
        throws Exception
    {
        try
        {
            logger.info("populate the STAGE_PRODUCT_INDEX_XREF to do the cursor load");
            pifDAO.populateProductIndexXref(conn);
            
            logger.info("Checking for failed product index xref loads");            
            HashMap map = pifDAO.getLoadFailedProducts(conn);
            
            String index_id;
            String product_id;
            Iterator it = map.keySet().iterator();
            int cntFailed = 0;
            
            while( it.hasNext() ) {
                cntFailed++;
                index_id = (String)it.next();
                product_id = (String)map.get(index_id);
                
                logger.error("----LOAD FAILED (duplicates in LOAD_PROD_INDEX_PRODUCT): INDEX ID:" + index_id + ". Product ID:" + product_id + ".");
            }
            
            if( cntFailed>0 ) {
                logger.error("load results: failed:" + cntFailed);
            }
        }
        catch(Exception e)
        {
            logger.error("EXCEPTION caught in PIF.cursorPopulateStageProductIndexXREF()",e);
            throw e;
        }
    }

    private void populateStageProductIndex(Connection conn)
        throws Exception
    {

        logger.info("begin populating stage_product_index");
                    
        try
        {
            List<CompanyConfigVO> companyConfigData = PifUtils.getInstance().getCompanyData(conn);
            logger.info("updating load_product_index");
            pifDAO.updateLoadProductIndex(conn);
            
            logger.info("populating stage_product_index from load_product_index");
            pifDAO.populateStageProductIndex(conn);
            
            logger.info("populating stage_product_index from intl_product_index");
            pifDAO.populateStageProductIndexIntl(conn);
            
            logger.info("updating stage_product_index");
            pifDAO.updateStageProductIndex(conn);
            
            for(int i = 0; i < companyConfigData.size(); i++)
            {
                CompanyConfigVO company = companyConfigData.get(i);
                if(!company.getId().equals("FTD"))
                {
                    pifDAO.updateStageProductIndexCompany(conn,company.getId(),company.getPrefix());
                    setOccasionsOrProductsFlag(conn, company.getPrefix(), "occasion", company.getOccasions(), company.getAdvOccasionSearchFlag());
                    setOccasionsOrProductsFlag(conn, company.getPrefix(), "product", company.getProducts(), false);
                }
            }
        }
        catch(Exception e)
        {
            logger.error("EXCEPTION caught in PIF.populateStageProductIndex()",e);
            throw e;
        }
    }

    private void setOccasionsOrProductsFlag(Connection conn, String companyIndexPrefix, String flagType, List specialIndexNames, boolean setAdvSearchOccasionsFlag)
        throws Exception
    {
        Statement stmt = null;
        
        try
        {
            String sql = " UPDATE STAGE_PRODUCT_INDEX SET PARENT_INDEX_ID=NULL, ";
            if(flagType.equals("occasion"))
                sql = sql + " OCCASIONS_FLAG='Y' ";
            if(flagType.equals("product"))
                sql = sql + " PRODUCTS_FLAG='Y' ";
            if(setAdvSearchOccasionsFlag)
                sql = sql + " , ADV_SEARCH_OCCASIONS_FLAG = 'Y' ";
            sql = sql + " WHERE DISPLAY_ORDER>0 AND PARENT_INDEX_ID IN " + "   (SELECT INDEX_ID FROM STAGE_PRODUCT_INDEX WHERE INDEX_FILE_NAME ";
            if(specialIndexNames != null && specialIndexNames.size() > 0)
            {
                sql = sql + " IN (";
                for(int i = 0; i < specialIndexNames.size(); i++)
                {
                    String indexName = (String)specialIndexNames.get(i);
                    sql = sql + "'" + indexName + "'";
                    if(i < specialIndexNames.size() - 1)
                        sql = sql + ", ";
                }
            
                sql = sql + "))";
            } else
            {
                sql = sql + " LIKE '" + companyIndexPrefix + flagType + "')";
            }
            
            logger.debug(sql);
            
            stmt = conn.createStatement();
            stmt.execute(sql);
        }
        catch(Exception e)
        {
            logger.error("EXCEPTION caught in PIF.setOccasionsOrProductsFlag()",e);
            throw e;
        } finally {
            if( stmt!=null ) {
                try {
                    stmt.close();
                    stmt = null;
                } catch (SQLException sqle) {
                    logger.warn("Error while closing statement in setOccasionsOrProductsFlag",sqle);
                }
            }
        }
    }

    private void stageIndexes(Connection conn)
        throws Exception
    {
        logger.info("loading product indexes into staging tables");
        
        try
        {
            populateStageProductIndex(conn);
            populateStageProductIndexXREF(conn);
            logger.info("loading product indexes into staging tables...successful");
        }
        catch(Exception e)
        {
            logger.error("EXCEPTION caught in PIF.stageIndexes()",e);
            throw e;
        }
    }

    private void backupProductIndexTables(Connection conn)
        throws Exception
    {
        try
        {
            logger.info("backing up production product index tables");
            pifDAO.backupProductIndexTables(conn);
        }
        catch(Exception e)
        {
            logger.error("EXCEPTION caught in PIF.backupProductIndexTables()",e);
            throw e;
        }
    }

    private void loadProductIndexTablesFromStagingTables(Connection conn)
        throws Exception
    {
        logger.info("loading product index tables from staging tables");
        
        try
        {
            pifDAO.copyToProductIndexTables(conn);

            logger.info("Product index tables have been successfully undated.");
        }
        catch(Exception e)
        {
            logger.error("EXCEPTION caught in PIF.loadProductIndexTablesFromStagingTables()",e);
            throw e;
        }
    }

    private boolean indexFound(List indexData, NovatorIndexRecord nir)
        throws Exception
    {
        try
        {
            for(int i = 0; i < indexData.size(); i++)
            {
                NovatorIndexRecord nirOld = (NovatorIndexRecord)indexData.get(i);
                if(nir.getIndexID().equals(nirOld.getIndexID()))
                {
                    boolean flag = true;
                    return flag;
                }
            }

        }
        catch(Exception se)
        {
            logger.error("EXCEPTION caught in PIF.indexFound()",se);
            throw se;
        }
        return false;
    }

//    public void readCSVFile(CompanyConfigVO company, List indexDataResult, List indexDataParentInfo)
//        throws Exception
//    {
//        String fileName = company.getFileName();
//        logger.info("reading a CSV file:" + fileName + " for company "+company.getId());
//        
//        try
//        {
//            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
//            String directory = configUtil.getProperty(CONFIG_FILE, "working_directory");
//            
//            String filePath = StringUtils.trimToEmpty(directory);
//            if( filePath.length()>0 && !filePath.endsWith(FILE_SEPERATOR) ) {
//                filePath += FILE_SEPERATOR;
//            }
//            filePath+=fileName;
//            
//            File file = new File(filePath);
//            if( !file.exists() ) {
//                logger.info("Novator index file "+filePath+" does not exist.");
//                return;
//            }
//            
//            if( !file.isFile() ) {
//                logger.info("Novator index file "+filePath+" cannot be processed because it is not a file.");
//                return;
//            }
//            
//            FileReader reader = new FileReader(file);
//            parseIndexData(reader, company, indexDataResult, indexDataParentInfo);
//        }
//        catch(Exception se)
//        {
//            logger.error("EXCEPTION caught in PIF.readCSVFile()",se);
//            throw se;
//        } 
//    }
    
    private void parseIndexData(Reader indexData, CompanyConfigVO company, List indexDataResult, List indexDataParentInfo) throws Exception {
        CsvReader reader = null;
        
        try
        {
            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
            String specialHomePageIndex = configUtil.getProperty(CONFIG_FILE, "special_homepage_index");
            
            reader = new CsvReader(indexData);
            reader.setSkipEmptyRecords(true);
            
            while( reader.readRecord() ) 
            {
                NovatorIndexRecord nir = new NovatorIndexRecord();
                nir.setIndexID(StringUtils.trimToEmpty(reader.get(0)));
                nir.setIndexName(StringUtils.trimToEmpty(reader.get(1)));
                nir.addMultipleElements(StringUtils.trimToEmpty(reader.get(2)), " ", "SUBINDEXIDS");
                nir.addMultipleElements(StringUtils.trimToEmpty(reader.get(3)), " ", "PRODUCTIDS");
                if(company.getFieldCount() > 4)
                    nir.setCountryID(StringUtils.trimToEmpty(reader.get(4)));
                if(company.isPrefixUsed())
                    nir.prefixMe(company.getPrefix(), specialHomePageIndex);
                if(indexFound(indexDataResult, nir))
                {
                    if(nir.getIndexID().equals(specialHomePageIndex))
                    {
                        List subIndexIDs = nir.getSubIndexIDs();
                        for(int i = 0; i < indexDataResult.size(); i++)
                        {
                            NovatorIndexRecord nirOld = (NovatorIndexRecord)indexDataResult.get(i);
                            if(nir.getIndexID().equals(nirOld.getIndexID()))
                            {
                                for(int j = 0; j < subIndexIDs.size(); j++)
                                {
                                    String subIndexID = (String)subIndexIDs.get(j);
                                    nirOld.addSubIndexID(subIndexID);
                                }
                            }
                        }

                    } else
                    {
                        throw new Exception("Duplicate index record found: index id = " + nir.getIndexID());
                    }
                } else
                {
                    indexDataParentInfo.add(nir);
                    indexDataResult.add(nir);
                }
            }
        }
        catch(Exception se)
        {
            logger.error("EXCEPTION caught in PIF.parseIndexData()",se);
            throw se;
        } finally {
            if( reader!=null ) {
                reader.close();
            }
        }
    }

    private void findAndSetParentIndexID(List indexData, List indexDataCopy, NovatorIndexRecord me)
        throws Exception
    {
        try
        {
            for(int i = 0; i < indexData.size(); i++)
            {
                NovatorIndexRecord nir = (NovatorIndexRecord)indexData.get(i);
                if(nir.hasSubIndex(me.getIndexID()))
                    if(!me.getParentIndexID().equals(""))
                    {
                        NovatorIndexRecord newNir = me.copy();
                        newNir.setIndexID(nir.getIndexID() + me.getIndexID());
                        newNir.setParentIndexID(nir.getIndexID());
                        if(indexFound(indexDataCopy, newNir))
                            throw new Exception("Duplicate index record found: index id = " + nir.getIndexID());
                        indexDataCopy.add(newNir);
                    } else
                    {
                        me.setParentIndexID(nir.getIndexID());
                    }
            }

        }
        catch(Exception se)
        {
            logger.info("EXCEPTION caught in PIF.findAndSetParentIndexID()",se);
            throw se;
        }
    }

    private void setParentsAndDisplayOrder(List indexDataResult, List indexDataParentInfo)
        throws Exception
    {
        try
        {
            for(int i = 0; i < indexDataParentInfo.size(); i++)
            {
                NovatorIndexRecord nir = (NovatorIndexRecord)indexDataParentInfo.get(i);
                findAndSetParentIndexID(indexDataParentInfo, indexDataResult, nir);
            }

            for(int i = 0; i < indexDataResult.size(); i++)
            {
                NovatorIndexRecord nir = (NovatorIndexRecord)indexDataResult.get(i);
                nir.sortChildren(indexDataResult);
            }

        }
        catch(Exception se)
        {
            logger.error("EXCEPTION caught in PIF.readCSVFile()",se);
            throw se;
        }
    }

    private void storeIndexData(List indexData, Connection conn)
        throws Exception
    {
        NovatorIndexRecord nir = null;
        try
        {
            for(int i = 0; i < indexData.size(); i++)
            {
                nir = (NovatorIndexRecord)indexData.get(i);
                pifDAO.insertProductIndex(conn,
                                          StringUtils.trim(nir.getIndexID()),
                                          nir.getIndexName(),
                                          nir.getParentIndexID(),
                                          nir.getCountryID(),
                                          nir.getSubIndexDisplayOrder()==0?new Long(-1):new Long(nir.getSubIndexDisplayOrder()));


                List productIDs = nir.getProductIDs();
                int displayOrder = 10;
                for(int j = 0; j < productIDs.size(); j++)
                {
                    String productID = (String)productIDs.get(j);
                    pifDAO.insertProductIndexProduct(conn,
                                                     nir.getIndexID(),
                                                     productID,
                                                     new Long(displayOrder),
                                                     false);
                    displayOrder += 10;
                }

            }

        }
        catch(Exception se)
        {
            if( nir!=null ) {
                logger.info("Novator index record: "+nir.toString());
            }
            logger.error("EXCEPTION caught in PIF.readCSVFile()",se);
            throw se;
        }
    }
    
    private int getIndexFiles(Connection conn) throws Exception {
        logger.info("Retrieving Novator indexes from FTP server.");
        String host = "unknown";
        int count = 0;
        
        try {
        	host = this.resourceProvider.getGlobalParameter(GLOBAL_PARMS_CONTEXT,PARM_SERVER);

            String userId = resourceProvider.getSecureProperty(GLOBAL_PARMS_CONTEXT,PARM_LOGIN);
            String credentials = resourceProvider.getSecureProperty(GLOBAL_PARMS_CONTEXT,PARM_PASSWORD);
            String localDirectory = resourceProvider.getGlobalParameter(GLOBAL_PARMS_CONTEXT,PARM_WORKING_DIRECTORY);
            String remoteDirectory = resourceProvider.getGlobalParameter(GLOBAL_PARMS_CONTEXT,PARM_REMOTE_DIRECTORY);
            boolean deleteRemoteFile = BooleanUtils.toBoolean(resourceProvider.getGlobalParameter(GLOBAL_PARMS_CONTEXT,PARM_DELETE_REMOTE_FILE));
            String filePattern = resourceProvider.getGlobalParameter(GLOBAL_PARMS_CONTEXT,PARM_FILE_PATTERN);

            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
            String archive_context = configUtil.getProperty(CONFIG_FILE,"file_archive_application_context");            
            String archive_name = configUtil.getProperty(CONFIG_FILE,"file_archive_applicaiton_name");
            Timestamp timestamp = new Timestamp(new Date().getTime());
            
            logger.debug("host: "+host);
            logger.debug("userId: "+userId);
            logger.debug("credentials: ##########");
            logger.debug("localDirectory: "+localDirectory);
            logger.debug("remoteDirectory: "+remoteDirectory);
            logger.debug("delete file: "+deleteRemoteFile);
            logger.debug("file pattern: "+filePattern);
            
            FTPClient ftpClient = new FTPClient();
            ftpClient.setRemoteHost(host);
            ftpClient.connect();
            ftpClient.login(userId, credentials);
            ftpClient.setType(FTPTransferType.ASCII);
            ftpClient.setConnectMode(FTPConnectMode.PASV);
            //if( remoteDirectory!=null && remoteDirectory.length()>0) {
                ftpClient.chdir(remoteDirectory);
           // }

            String[] fileNames = null;
            try {
                logger.info("Getting the list of files");
                fileNames = ftpClient.dir();
                
                if( fileNames.length==0 ) {
                    logger.info("Nothing to download");
                    return count;
                }
            } catch (Exception e) {
                logger.info("Nothing to download");
                return count;
            }
                
            //Check to see if the download directory exists
            File fLocal = new File(localDirectory);
            if( !fLocal.exists() ) {
                FileUtils.forceMkdir(fLocal);
            }
            
            //Compile the regex expression if needed
            Pattern p = null;
            if( filePattern!=null && filePattern.length()>0) {
                p = Pattern.compile(filePattern);
            }
            
            for (int i = 0; i < fileNames.length; i++) {
                String fileName = fileNames[i];
                
                try {
                
                    //It there is a file pattern, then validate against it
                    if( p!=null ) {
                        // search for a match within a string
                        Matcher m = p.matcher(fileName);
                        if( !m.find() ) {
                            continue;
                        }
                    }
                    
                    String remoteFileName;
                    if( remoteDirectory!=null && remoteDirectory.length()>0) {
                        remoteFileName = fileName; //remoteDirectory + "/" + fileName;
                    } else {
                        remoteFileName = fileName;
                    }
                    
                    //Download the file from the server
                    logger.info("Downloading file: " + fileName);
                    ftpClient.get(localDirectory + "/" + fileName,remoteFileName);                
                    logger.info("File downloaded: " + fileName);
                    
                    //Save to the database
                    File indexFile = new File(localDirectory,fileName);
                    long length = indexFile.length();
                    FileInputStream fis = new FileInputStream(indexFile);
                    int readSize;
                    StringBuilder sb = new StringBuilder();
                    
                    do {
                        if (length > Integer.MAX_VALUE) {
                            readSize = Integer.MAX_VALUE;
                        } else {
                            readSize = (int)length;
                        }    
                        
                        // Create the byte array to hold the data
                        byte[] bytes = new byte[(int)length];
                    
                        // Read in the bytes
                        int offset = 0;
                        int numRead = 0;
                        while (offset < bytes.length && (numRead=fis.read(bytes, offset, bytes.length-offset)) >= 0) {
                            offset += numRead;
                        }
                    
                        // Ensure all the bytes have been read in
                        if (offset < bytes.length) {
                            throw new IOException("Could not completely read file "+fileName);
                        }
                        
                        //Add to the string builder
                        sb.append(new String(bytes));
    
                        length = length - offset;
                        
                    } while(length > 0);
                    fis.close();
                    
                    FileArchiveVO faVO = new FileArchiveVO();
                    faVO.setContext(archive_context);
                    faVO.setName(archive_name);
                    faVO.setFileName(fileName);
                    faVO.setData(sb.toString());
                    faVO.setCreatedOn(timestamp);
                    faVO.setCreatedBy("PIF");
                    
                    logger.debug("Saving file "+fileName+" to FRP.FILE_ARCHIVE.");
                    fileArchiveDAO.insertFileArchive(conn,faVO);
                    
                    //Delete the file off of the remote file system
                    logger.debug("Removing file "+remoteFileName+" from FTP server.");
                    if( deleteRemoteFile ) {
                        ftpClient.delete(remoteFileName);
                    }
                    
                    logger.debug("Removing "+fileName+" from file system.");
                    FileUtils.forceDelete(indexFile);
                    
                    count++;
                    
                } catch (IOException e) {
                    logger.error("Error while retrieving index file "+fileName+".",e);
                    throw e;
                }
            }
            
        } catch (Exception e) {
            logger.error("Unable to ftp index files from "+host,e);
            throw e;
        } 
        
        logger.info(String.valueOf(count)+" files were downloaded.");
        return count;
    }

    private int loadIndexes(Connection conn, Long fileArchiveId)
        throws Exception
    {
        logger.info("Loading Novator indexes.");
        int filesProcessed = 0;
        
        try
        {
            HashMap<String,FileArchiveVO> indexFiles;
            
            if( fileArchiveId==null ) {
                ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
                indexFiles = 
                  fileArchiveDAO.getLatestArchiveFiles(conn,configUtil.getProperty(CONFIG_FILE,"file_archive_application_context"),configUtil.getProperty(CONFIG_FILE,"file_archive_applicaiton_name"));
            } else {
                indexFiles = fileArchiveDAO.getRelatedArchiveFiles(conn,fileArchiveId);
            }
            
            if(indexFiles.size()>0) {
                List<CompanyConfigVO> companyConfigData = PifUtils.getInstance().getCompanyData(conn);
                
                pifDAO.emptyLoadTables(conn);
                List indexDataResult = new ArrayList();
                List indexDataParentInfo = new ArrayList();
                for(int i = 0; i < companyConfigData.size(); i++)
                {
                    CompanyConfigVO company = companyConfigData.get(i);
                    FileArchiveVO fileArchive = indexFiles.get(company.getFileName());
                    
                    if( fileArchive==null ) {
                        logger.error("Unable to find index file "+company.getFileName()+" for company "+company.getId());
                    } else {
                        StringReader sr = new StringReader(fileArchive.getData());
                        parseIndexData(sr, company, indexDataResult, indexDataParentInfo);
                        filesProcessed++;
                    }
                }
    
                setParentsAndDisplayOrder(indexDataResult, indexDataParentInfo);
                storeIndexData(indexDataResult, conn);
            } else {
                logger.info("No index files were found to process.");
            }
        }
        catch(Exception se)
        {
            logger.error("EXCEPTION caught in PIF.loadIndexes()",se);
            throw se;
        }
        
        return filesProcessed;
    }

    public void setPifDAO(ProductIndexFeedDAOImpl pifDAO) {
        this.pifDAO = pifDAO;
    }

    public ProductIndexFeedDAOImpl getPifDAO() {
        return pifDAO;
    }

    public void setFileArchiveDAO(FileArchiveDAO fileArchiveDAO) {
        this.fileArchiveDAO = fileArchiveDAO;
    }

    public FileArchiveDAO getFileArchiveDAO() {
        return fileArchiveDAO;
    }

    public void setResourceProvider(IResourceProvider resourceProvider) {
        this.resourceProvider = resourceProvider;
    }

    public IResourceProvider getResourceProvider() {
        return resourceProvider;
    }
}
