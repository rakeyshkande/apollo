package com.ftd.pdb.pif.common;

import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.pdb.pif.common.valobjs.CompanyConfigVO;

import com.ftd.pdb.pif.dao.ProductIndexFeedDAOImpl;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;


public class PifUtils {

    private static List<CompanyConfigVO> COMPANY_DATA = null;
//    private static List<String> EMAIL_ADDRESSES = null;
    private static Object MUTEX = new Object();
    private static PifUtils PIF_UTILS = new PifUtils();
//    private static String CONFIG_FILE = "pif_companies_config.xml";
        
    private PifUtils() {
    }
    
    public static PifUtils getInstance() {
        return PIF_UTILS;
    }
    
    public List<CompanyConfigVO> getCompanyData(Connection conn) throws Exception {
        if(COMPANY_DATA==null) {
            synchronized (MUTEX) {
                if(COMPANY_DATA==null) {
                    ProductIndexFeedDAOImpl dao = new ProductIndexFeedDAOImpl();
                    COMPANY_DATA = dao.getPifCompanyData(conn);
                }
            }
        }
        
        return COMPANY_DATA;
    }
}
