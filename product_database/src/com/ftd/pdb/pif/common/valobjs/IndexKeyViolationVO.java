package com.ftd.pdb.pif.common.valobjs;

public class IndexKeyViolationVO {
    private String country;
    private String indexId;
    private String productId;
    
    public IndexKeyViolationVO() {
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountry() {
        return country;
    }

    public void setIndexId(String indexId) {
        this.indexId = indexId;
    }

    public String getIndexId() {
        return indexId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductId() {
        return productId;
    }
}
