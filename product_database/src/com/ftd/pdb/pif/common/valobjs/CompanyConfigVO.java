package com.ftd.pdb.pif.common.valobjs;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

public class CompanyConfigVO {
    private String id;
    private String fileName;
    private int fieldCount;
    private boolean prefixUsed;
    private String prefix;
    private boolean advOccasionSearchFlag;
    private boolean active;
    private List<String> occasions;
    private List<String> products;
        
    public CompanyConfigVO() {
        id = "";
        fileName = "";
        fieldCount = 0;
        prefixUsed = false;
        prefix = "";
        advOccasionSearchFlag = false;
        occasions = new ArrayList<String>();
        products = new ArrayList<String>();
    }
    
    public String toString() {
        StringBuilder sb = new StringBuilder();
        String ls = System.getProperty("line.separator");
        sb.append("Id: ");
        sb.append(this.id);
        sb.append(ls);
        sb.append("File name: ");
        sb.append(this.fileName);
        sb.append(ls);
        sb.append("Field count: ");
        sb.append(this.fieldCount);
        sb.append(ls);
        sb.append("Prefix used: ");
        sb.append(this.prefixUsed);
        sb.append(ls);
        sb.append("Prefix: ");
        sb.append(this.prefix);
        sb.append(ls);
        sb.append("Search flag: ");
        sb.append(this.advOccasionSearchFlag);
        sb.append(ls);
        sb.append("Occasions: ");
        
        for( int idx=0; idx<this.occasions.size(); idx++) {
            sb.append(occasions.get(idx));
            sb.append(" ");
        }

        sb.append(ls);
        sb.append("Products: ");
        
        for( int idx=0; idx<this.products.size(); idx++) {
            sb.append(products.get(idx));
            sb.append(" ");
        }
        
        return sb.toString();
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFieldCount(int fieldCount) {
        this.fieldCount = fieldCount;
    }

    public int getFieldCount() {
        return fieldCount;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setAdvOccasionSearchFlag(boolean advOccasionSearchFlag) {
        this.advOccasionSearchFlag = advOccasionSearchFlag;
    }

    public boolean getAdvOccasionSearchFlag() {
        return advOccasionSearchFlag;
    }

    public int getOccasionCount() {
        return occasions.size();
    }

    public List getOccasions() {
        return occasions;
    }
    
    public void addOccasion(String occasion) {
        if(StringUtils.isNotBlank(occasion)) {
            this.occasions.add(occasion);
        }
    }

    public int getProductCount() {
        return products.size();
    }

    public List getProducts() {
        return products;
    }
    
    public void addProduct(String product) {
        if(StringUtils.isNotBlank(product)) {
            this.products.add(product);
        }
    }

    public void setPrefixUsed(boolean prefixUsed) {
        this.prefixUsed = prefixUsed;
    }

    public boolean isPrefixUsed() {
        return prefixUsed;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isActive() {
        return active;
    }
}
