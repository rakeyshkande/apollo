package com.ftd.pdb.pif.common;

import com.ftd.osp.utilities.plugins.Logger;

import java.util.ArrayList;
import java.util.List;


public class NovatorIndexRecord {
    private Logger logger;
    private String indexID;
    private String indexName;
    private List<String> subIndexIDs;
    private List<String> productIDs;
    private String parentIndexID;
    private String countryID;
    private int subIndexDisplayOrder;

    public NovatorIndexRecord() {
        indexID = "";
        indexName = "";
        subIndexIDs = null;
        productIDs = null;
        parentIndexID = "";
        countryID = "";
        subIndexDisplayOrder = 0;
        subIndexIDs = new ArrayList<String>();
        productIDs = new ArrayList<String>();
    }

    public NovatorIndexRecord copy() throws Exception {
        NovatorIndexRecord newVO = new NovatorIndexRecord();
        newVO.setIndexID(indexID);
        newVO.setIndexName(indexName);
        newVO.setSubIndexIDs(subIndexIDs);
        newVO.setProductIDs(productIDs);
        newVO.setParentIndexID(parentIndexID);
        return newVO;
    }

    public void setIndexName(String indexNamein) throws Exception {
        try {
            indexName = "";

            for (int i = 0; i < indexNamein.length(); i++) {
                String character = 
                    new String(new char[] { indexNamein.charAt(i) });
                if (character.equals("'"))
                    indexName = indexName + "''";
                else
                    indexName = indexName + character;
            }
        } catch (Exception se) {
            logger.error("EXCEPTION caught in NovatorIndexRecord.setIndexName(),se");
            throw se;
        }
    }

    public void addMultipleElements(String elements, String delimiter, 
                                    String listName) throws Exception {
        try {
            String[] tokens = elements.split(delimiter);
            boolean bSubIndex = false;
            boolean bProductIndex = false;

            if (listName.equals("SUBINDEXIDS")) {
                bSubIndex = true;
                bProductIndex = false;
            } else if (listName.equals("PRODUCTIDS")) {
                bSubIndex = false;
                bProductIndex = true;
            }
            
            for( int idx=0; idx < tokens.length; idx++ ) {
                if( bSubIndex ) {
                    subIndexIDs.add(tokens[idx]);
                } else if( bProductIndex ) {
                    productIDs.add(tokens[idx]);
                }
            }
        } catch (Exception se) {
            logger.error("EXCEPTION caught in NovatorIndexRecord.addMultipleElements()", 
                         se);
            throw se;
        }
    }

    public boolean hasSubIndex(String subIndexIDin) throws Exception {
        try {
            for (int i = 0; i < subIndexIDs.size(); i++) {
                String subIndexID = subIndexIDs.get(i);

                if (subIndexID.equals(subIndexIDin)) {
                    boolean flag = true;
                    return flag;
                }
            }
        } catch (Exception se) {
            logger.error("EXCEPTION caught in NovatorIndexRecord.hasSubIndex()", 
                         se);
            throw se;
        }
        return false;
    }

    public void sortChildren(List indexData) throws Exception {
        try {
            int x = 10;

            for (int i = 0; i < subIndexIDs.size(); i++) {
                String subIndexID = subIndexIDs.get(i);
                for (int j = 0; j < indexData.size(); j++) {
                    NovatorIndexRecord nir = 
                        (NovatorIndexRecord)indexData.get(j);

                    if (nir.getIndexID().equals(subIndexID)) {
                        nir.setSubIndexDisplayOrder(x);
                        x += 10;
                    }
                }

            }

        } catch (Exception se) {
            logger.error("EXCEPTION caught in NovatorIndexRecord.sortMyChildren()", se);
            throw se;
        }
    }
    
    public void prefixMe(String prefix, 
                         String specialHomePageIndex) throws Exception {
        try {
            if (!getIndexID().equals(specialHomePageIndex))
                setIndexID(prefix + getIndexID());
            List<String> newSubIndexList = new ArrayList<String>();
            for (int i = 0; i < subIndexIDs.size(); i++) {
                String subIndexID = prefix + (String)subIndexIDs.get(i);
                newSubIndexList.add(subIndexID);
            }

            subIndexIDs = newSubIndexList;
        } catch (Exception se) {
            logger.debug("EXCEPTION caught in NovatorIndexRecord.prefixMe()",se);
            throw se;
        }
    }
    
    public String toString(){
        StringBuilder sb = new StringBuilder();
        
        String ls = System.getProperty("line.separator");
        sb.append("indexID: ");
        sb.append(this.indexID);
        sb.append(ls);
        sb.append("indexName: ");
        sb.append(this.indexName);
        sb.append(ls);
        sb.append("parentIndexID: ");
        sb.append(this.parentIndexID);
        sb.append(ls);
        sb.append("countryID: ");
        sb.append(this.countryID);
        sb.append(ls);
        sb.append("subIndexDisplayOrder: ");
        sb.append(this.subIndexDisplayOrder);
        sb.append(ls);
        sb.append("subIndexIDs: ");
        
        for( int idx=0; idx<this.subIndexIDs.size(); idx++) {
            sb.append(subIndexIDs.get(idx));
            sb.append(" ");
        }
        
        sb.append(ls);
        sb.append("productIDs: ");
        
        for( int idx=0; idx<this.productIDs.size(); idx++) {
            sb.append(productIDs.get(idx));
            sb.append(" ");
        }
        
        return sb.toString();
    }

    public void addSubIndexID(String subIndexID) {
        subIndexIDs.add(subIndexID);
    }

    public void addProductID(String productID) {
        productIDs.add(productID);
    }

    public void setIndexID(String indexID) {
        this.indexID = indexID;
    }

    public String getIndexID() {
        return indexID;
    }

    public String getIndexName() {
        return indexName;
    }

    public void setSubIndexIDs(List<String> subIndexIDs) {
        this.subIndexIDs = subIndexIDs;
    }

    public List<String> getSubIndexIDs() {
        return subIndexIDs;
    }

    public void setProductIDs(List<String> productIDs) {
        this.productIDs = productIDs;
    }

    public List<String> getProductIDs() {
        return productIDs;
    }

    public void setParentIndexID(String parentIndexID) {
        this.parentIndexID = parentIndexID;
    }

    public String getParentIndexID() {
        return parentIndexID;
    }

    public void setCountryID(String countryID) {
        this.countryID = countryID;
    }

    public String getCountryID() {
        return countryID;
    }

    public void setSubIndexDisplayOrder(int subIndexDisplayOrder) {
        this.subIndexDisplayOrder = subIndexDisplayOrder;
    }

    public int getSubIndexDisplayOrder() {
        return subIndexDisplayOrder;
    }
}
