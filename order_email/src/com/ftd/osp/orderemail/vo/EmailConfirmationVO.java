package com.ftd.osp.orderemail.vo;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.List;

/**
 * Email Confirmation Value Object that maps to EMAIL_CONFIRMATION database table
 * 
 *
 * @author Doug Johnson
 */
public class EmailConfirmationVO implements Serializable  
{

  private String emailConfirmationGuid;
  private int emailStyleSheetId;
  private String toAddress;
  private String fromAddress;
  private String subject;
  private String orderContent;
  private String orderContentHTML;

  public EmailConfirmationVO()
  {
  }

  public String toString()
  {
    String lineSep = System.getProperty("line.separator");
    StringBuffer sb = new StringBuffer();
    StringBuffer listSb = new StringBuffer();
    // Get this Class
    Class thisClass = this.getClass();
    sb.append("Object Type = ").append(thisClass.getName()).append(lineSep);
        
    // Use reflection to loop through all the fields
    Field[] fields = thisClass.getDeclaredFields();
    for(int i = 0; i < fields.length; i++)
    {
        try
        {
            if(fields[i].getType().equals(Class.forName("java.lang.String")))
            {
                sb.append(fields[i].getName()).append(" = ").append(fields[i].get(this)).append(lineSep);
            }
            else if(fields[i].getType().equals(Class.forName("java.util.List")))
            {
                List list = (List)fields[i].get(this);
                if(list != null)
                {
                    for (int j = 0; j < list.size(); j++) 
                    {
                        listSb.append(list.get(j).toString());
                    }
                }
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    sb.append(listSb);
        
    return sb.toString();
  } 

  public String getEmailConfirmationGuid()
  {
    return emailConfirmationGuid;
  }

  public void setEmailConfirmationGuid(String newEmailConfirmationGuid)
  {
    emailConfirmationGuid = newEmailConfirmationGuid;
  }

  public int getEmailStyleSheetId()
  {
    return emailStyleSheetId;
  }

  public void setEmailStyleSheetId(int newEmailStyleSheetId)
  {
    emailStyleSheetId = newEmailStyleSheetId;
  }

  public String getToAddress()
  {
    return toAddress;
  }

  public void setToAddress(String newToAddress)
  {
    toAddress = newToAddress;
  }

  public String getFromAddress()
  {
    return fromAddress;
  }

  public void setFromAddress(String newFromAddress)
  {
    fromAddress = newFromAddress;
  }

  public String getSubject()
  {
    return subject;
  }

  public void setSubject(String newSubject)
  {
    subject = newSubject;
  }





  public String getOrderContent()
  {
    return orderContent;
  }

  public void setOrderContent(String newOrderContent)
  {
    orderContent = newOrderContent;
  }

  public String getOrderContentHTML()
  {
    return orderContentHTML;
  }

  public void setOrderContentHTML(String newOrderContentHTML)
  {
    orderContentHTML = newOrderContentHTML;
  }
}