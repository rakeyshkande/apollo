package com.ftd.osp.orderemail.vo;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.List;

/**
 * Email Confirmation Value Object that maps to EMAIL_CONFIRMATION database table
 * 
 *
 * @author Doug Johnson
 */
public class EmailContentsVO  implements Serializable  
{
  private String emailConfirmationGuid;
  private int section;
  private String content;

  public EmailContentsVO()
  {
  }

  public String toString()
  {
    String lineSep = System.getProperty("line.separator");
    StringBuffer sb = new StringBuffer();
    StringBuffer listSb = new StringBuffer();
    // Get this Class
    Class thisClass = this.getClass();
    sb.append("Object Type = ").append(thisClass.getName()).append(lineSep);
        
    // Use reflection to loop through all the fields
    Field[] fields = thisClass.getDeclaredFields();
    for(int i = 0; i < fields.length; i++)
    {
        try
        {
            if(fields[i].getType().equals(Class.forName("java.lang.String")))
            {
                sb.append(fields[i].getName()).append(" = ").append(fields[i].get(this)).append(lineSep);
            }
            else if(fields[i].getType().equals(Class.forName("java.util.List")))
            {
                List list = (List)fields[i].get(this);
                if(list != null)
                {
                    for (int j = 0; j < list.size(); j++) 
                    {
                        listSb.append(list.get(j).toString());
                    }
                }
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    sb.append(listSb);
        
    return sb.toString();
  } 

  public String getEmailConfirmationGuid()
  {
    return emailConfirmationGuid;
  }

  public void setEmailConfirmationGuid(String newEmailConfirmationGuid)
  {
    emailConfirmationGuid = newEmailConfirmationGuid;
  }

  public int getSection()
  {
    return section;
  }

  public void setSection(int newSection)
  {
    section = newSection;
  }

  public String getContent()
  {
    return content;
  }

  public void setContent(String newContent)
  {
    content = newContent;
  }
  
}