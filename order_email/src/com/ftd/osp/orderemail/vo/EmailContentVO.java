package com.ftd.osp.orderemail.vo;

public class EmailContentVO 
{
  public static final int PLAIN_TEXT = 0;
  public static final int HTML = 1;
  public static final int MULTIPART = 2;

  private int contentType = 0;
  private String subject = new String();
  private String sender = new String();
  private String recipient = new String();
  private String content = new String();

  public void setContentType(int contentType)
  {
    this.contentType = contentType;
  }

  public int getContentType(int contentType)
  {
    return this.contentType;
  }

  public void setSubject(String subject)
  {
    this.subject = subject;
  }

  public String getSubject()
  {
    return this.subject;
  }

  public void setRecipient(String recipient)
  {
    this.recipient = recipient;
  }

  public String getRecipient()
  {
    return this.recipient;
  }

  public void setSender(String sender)
  {
    this.sender = sender;
  }

  public String getSender()
  {
    return this.sender;
  }

  public String getContent()
  {
    return this.content;
  }

  public void prependContent(String section)
  {
    this.content = (section + content);
  }

  public void appendContent(String section)
  {
    this.content = (this.content + section);
  }
}