package com.ftd.osp.orderemail.ejb;

import com.ftd.osp.framework.receiver.ReceiverMDB;
import com.ftd.osp.utilities.plugins.Logger;

import javax.ejb.MessageDrivenContext;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.sql.DataSource;

public class OrderEmailMDB extends ReceiverMDB
{
    /**
       * Set the associated message-driven context. The container calls this method after the instance creation.
       * The enterprise Bean instance should store the reference to the context object in an instance variable.
       * This method is called with no transaction context.
       *
       * @param ctx A MessageDrivenContext interface for the instance.
       * @exception EJBException Thrown by the method to indicate a failure caused by a system-level error.
       */
    public void setMessageDrivenContext(MessageDrivenContext ctx)
    {
        this.messageDrivenContext = ctx;
        InitialContext initContext = null;

        try
        {
            logger = new Logger("com.ftd.osp.framework.receiver.Receiver");
            initContext = new InitialContext();
            myenv = (Context)initContext.lookup("");

            logger.debug("Begin Registering Component");
            businessObjectClassName = "com.ftd.osp.orderemail.OrderEmailBO";
            logger.debug("Business Object Class Name :: " + businessObjectClassName);
            rollbackStatusName = "ROLLBACK";
            logger.debug("Rollback Status Name :: " + rollbackStatusName);
            boRequiresJDBCConnection = true;
            logger.debug("Business Object Requires JDBC Connection :: " + boRequiresJDBCConnection);
            if (boRequiresJDBCConnection.booleanValue())
            {
                dataSourceName = "jdbc/ORDER_SCRUBDS";
                logger.debug("Data Source Name :: " + dataSourceName);
                dataSource = (DataSource)myenv.lookup(dataSourceName);
            }

            dispatchOrderToQueue = false;
            logger.debug("Dispatch Order To Queue :: " + dispatchOrderToQueue);
            persistMessageToken = false;
            logger.debug("Persist Message Token :: " + persistMessageToken);
            componentRole = "EMAIL CONFIRMATION";
            logger.debug("Component Role :: " + componentRole);
            logger.debug("End Registering Component");

        } catch (Exception ex)
        {
            logger.error(ex);
        } finally
        {
            ++count;
            logger.debug("MDB Count " + count);
            try
            {
                initContext.close();
            } catch (Exception ex)
            {
                logger.error(ex);
            } finally
            {
            }
        }

    }
}
