package com.ftd.osp.orderemail;

import com.ftd.messagegenerator.constants.MessageConstants;
import com.ftd.osp.utilities.email.BuildOrderEmailHelper;
import com.ftd.osp.utilities.email.EmailVO;
import com.ftd.osp.utilities.j2ee.NotificationUtil;
import com.ftd.osp.utilities.vo.NotificationVO;

import com.ftd.osp.orderemail.vo.*;

import com.ftd.osp.utilities.ConfigurationUtil;

import java.sql.Connection;
import java.util.*;
import com.ftd.osp.utilities.j2ee.*;

import javax.mail.Session;
import javax.naming.*;
import com.ftd.osp.utilities.plugins.Logger;

public class EmailActionHandlerBase 
{
  private Connection connection = null;
  private String actionGUID = null;
  private Logger log;
  
  public EmailActionHandlerBase()
  {
    log = new Logger("com.ftd.osp.orderemail.emailactionhandlers.EmailActionHandlerBase");  
  }

  protected void sendEmail(EmailVO emailVO) throws Exception
  {

    if(emailVO.getGuid() != null)
    {
      log.debug("Sending email GUID:" + emailVO.getGuid());
    }
    else
    {
      log.debug("Email guid null");
    }

    //Check what type of email should be sent using global parms
    BuildOrderEmailHelper helper = new BuildOrderEmailHelper(this.getConnection());
    emailVO.setContentType(helper.getGlobalEmailFlag());

    try
    {
      log.debug("Recipient:" + emailVO.getRecipient() + ":");
      log.debug("Sender:" + emailVO.getSender() + ":");
      log.debug("Subject:" + emailVO.getSubject() + ":");
    }
    catch(Throwable t)
    {
      log.debug("Could not print email contents because of the following error.  THIS IS NOT A CRITICAL ERROR< THIS IS FOR DEBUGGING ONLY!");
      log.debug(t);
    }
    
    //some email domains require special formatting.
    emailVO = helper.performSpecialFormatting(emailVO);
    
    if(log.isDebugEnabled()) 
    {
      try
      {
        logEmailDebugInformation(emailVO);
      }
      catch (Throwable t)
      {
        log.debug("Exception generating debug information for email", t);
      }
    }

    if(emailVO.getContentType() == emailVO.PLAIN_TEXT)
    {
      log.debug("Sending plain text email");
      this.sendPlainTextEmail(emailVO); 
    }
    else
    {
      log.debug("Sending multipart email");
      this.sendMultipartEmail(emailVO);
    }
  }

  public void setConnection(Connection connection)
  {
    this.connection = connection;
  }

  public Connection getConnection()
  {
    return this.connection;
  }

  public void setActionGUID(String actionGUID) throws Exception
  {
    this.actionGUID = actionGUID;
  }

  public String getActionGUID()
  {
    return this.actionGUID;
  }

  private void sendMultipartEmail(EmailVO emailVO) throws Exception
  {                                                                                  
      NotificationVO notificationVO = new NotificationVO();
      notificationVO.setMessageFromAddress(emailVO.getSender());
      notificationVO.setMessageTOAddress(emailVO.getRecipient());
      notificationVO.setMessageSubject(emailVO.getSubject());
      notificationVO.setMessageContent(emailVO.getContent());
      notificationVO.setMessageContentHTML(emailVO.getHTMLContent());
      notificationVO.setMessageMimeType("multipart/alternative");
      
      notificationVO.setSMTPHost(this.getSMTPHostName());
      NotificationUtil.getInstance().notifyMulipart(notificationVO);
 
  }

  private void sendPlainTextEmail(EmailVO emailVO) throws Exception
  {
      NotificationVO notificationVO = new NotificationVO();
      notificationVO.setMessageFromAddress(emailVO.getSender());
      notificationVO.setMessageTOAddress(emailVO.getRecipient());
      notificationVO.setMessageSubject(emailVO.getSubject());
      notificationVO.setMessageContent(emailVO.getContent());
      notificationVO.setMessageMimeType("text/plain; charset=us-ascii");
      notificationVO.setSMTPHost(this.getSMTPHostName());

      NotificationUtil.getInstance().notify(notificationVO);
  }
  
  private String getSMTPHostName() throws Exception
  {
      String smtpTransformerEnabled = null;
      String smtpMailserverKey = null;
      String smtpHostName = null;
      
      smtpTransformerEnabled = ConfigurationUtil.getInstance().getFrpGlobalParm(MessageConstants.MESSAGE_GEN_CONFIG_CONTEXT,
          MessageConstants.MAILSERVER_TRANSFORMER_ENABLED);


      if (smtpTransformerEnabled != null && "Y".equalsIgnoreCase(smtpTransformerEnabled))
      {        
          smtpMailserverKey = ConfigurationUtil.getInstance().getFrpGlobalParm(MessageConstants.MESSAGE_GEN_CONFIG_CONTEXT,
              MessageConstants.MAILSERVER_KEY); 
          
          if (smtpMailserverKey != null)
          {
              smtpHostName = ConfigurationUtil.getInstance().getFrpGlobalParm(MessageConstants.MESSAGE_GEN_CONFIG_CONTEXT,
                  MessageConstants.MAILSERVER_HOST_NAME_PREFIX + smtpMailserverKey);      
              if (smtpHostName == null)
              {
                  log.error("Mailserver hostname was unable to be retrieved from global parms.  Parm: " + 
                            MessageConstants.MESSAGE_GEN_CONFIG_CONTEXT + " " + MessageConstants.MAILSERVER_HOST_NAME_PREFIX + smtpMailserverKey);
              }
          }
          else
          {
              log.error("Mailserver key was unable to be retrieved from global parms.  Parm:" + 
                         MessageConstants.MESSAGE_GEN_CONFIG_CONTEXT + " " + MessageConstants.MAILSERVER_KEY);
          }
      }
      
      if (smtpHostName == null)
      {
          //Get the default host
          smtpHostName = ConfigurationUtil.getInstance().getFrpGlobalParm(MessageConstants.MESSAGE_GEN_CONFIG_CONTEXT,
              MessageConstants.SMTP_HOST_NAME);
          
          if (smtpHostName == null)
          {
              throw new Exception("Null Global Parm retrieved when looking for the SMTP host name parm. Parm: " + 
                                  MessageConstants.MESSAGE_GEN_CONFIG_CONTEXT + MessageConstants.SMTP_HOST_NAME);
          }
      }  
      return smtpHostName;
  }
  
  /**
   * Logs debug information with the content of the Email
   * Called prior to the email being sent
   * @param emailVO
   */
  private void logEmailDebugInformation(EmailVO emailVO)
  {
    if(!log.isDebugEnabled())
    {
      return;
    }

    StringBuffer emailInfo = new StringBuffer("Sending Email:");
    
    emailInfo.append("\nTo: ").append(emailVO.getRecipient());
    emailInfo.append("\nFrom: ").append(emailVO.getSender());
    emailInfo.append("\nSubject: ").append(emailVO.getSubject());
    emailInfo.append("\nEmail Handler: ").append(getClass().getName());
   
    if(emailVO.getContentType() == emailVO.PLAIN_TEXT)
    {
      emailInfo.append("\nContent Type: Plain Text");
      emailInfo.append("\n").append(emailVO.getContent());
    }
    else
    {
      emailInfo.append("\nContent Type: Multipart");
      emailInfo.append("\n").append(emailVO.getHTMLContent());
    } 
    
      log.debug(emailInfo.toString());
  }
  
}
