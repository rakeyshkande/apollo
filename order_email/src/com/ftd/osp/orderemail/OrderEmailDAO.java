package com.ftd.osp.orderemail;

import com.ftd.osp.orderemail.constants.OrderEmailConstants;
import com.ftd.osp.orderemail.vo.EmailConfirmationVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.SourceMasterVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * Data access class for email confirmation
 *
 * @author Doug Johnson
 */
public class OrderEmailDAO
{
    private Connection connection;
    private Logger logger;

    private static final String EMAIL_GUID_KEY = "EMAIL_GUID";

    //procedures
    private static final String VIEW_EMAIL_CONFIRMATION = "FRP.VIEW_EMAIL_CONFIRMATION_INFO";

    //Order ref cursors returned from database procedure
    private static final String EMAIL_CONFIRMATION_CURSOR = "RegisterOutParameterEmailConfirmationcur";
    private static final String EMAIL_ORDER_CONTENT_CURSOR = "RegisterOutParameterEmailOrderContentcur";
    private static final String EMAIL_SECTION_CONTENT_CURSOR = "RegisterOutParameterEmailSectionContentcur";

  /**
   * OrderEmailDAO cunstructor
   * @param connection The Connection that will be used to access the order
   *
   */
  public OrderEmailDAO(Connection connection)
  {
      this.connection = connection;
      this.logger = new Logger("com.ftd.osp.orderemail.OrderEmailDAO");
  }

 /**
   *
   *
   * @param email guid
   * @return OrderVO
   */
  public EmailConfirmationVO getEmailConfirmation(EmailConfirmationVO emailConfirmation) throws Exception
  {
      Map emailMap = null;

      // Get the email confirmation information
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(connection);
      dataRequest.setStatementID(VIEW_EMAIL_CONFIRMATION);
      dataRequest.addInputParam(EMAIL_GUID_KEY, emailConfirmation.getEmailConfirmationGuid());

      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

      //rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
      emailMap = (Map) dataAccessUtil.execute(dataRequest);

      //CachedResultSet rs = new CachedResultSet();
      CachedResultSet rs = (CachedResultSet) emailMap.get(EMAIL_CONFIRMATION_CURSOR);

      while(rs != null && rs.next())
      {
          emailConfirmation.setEmailConfirmationGuid((String)rs.getObject(1));
          emailConfirmation.setEmailStyleSheetId(new Integer(rs.getObject(2).toString()).intValue());
          emailConfirmation.setToAddress((String)rs.getObject(3));
          emailConfirmation.setFromAddress((String)rs.getObject(4));
          emailConfirmation.setSubject((String)rs.getObject(5));
      }

      // Get Order Content
      //rs = new CachedResultSet();
      rs = (CachedResultSet) emailMap.get(EMAIL_ORDER_CONTENT_CURSOR);
      StringBuffer allOrderContent = new StringBuffer();

      boolean firstRow = true;
      // Result is sorted by content type.
      while(rs != null && rs.next()) {
          if("html".equals(rs.getObject(2))) {
              allOrderContent.append((String)rs.getObject(4));
          } else if(firstRow) {
              firstRow = false;
              emailConfirmation.setOrderContentHTML(allOrderContent.toString());
              allOrderContent = new StringBuffer();
              allOrderContent.append((String)rs.getObject(4));
          } else {
              allOrderContent.append((String)rs.getObject(4));
          }
      }
      emailConfirmation.setOrderContent(allOrderContent.toString());


      /*
      EmailContentsVO orderContent = null;
      boolean firstRow = true;

      String holdContentType = "";
      int rowCount = rs.getRowCount();
      int count = 0;

      while(rs != null && rs.next())
      {
         count++;
         String contentType = rs.getObject(2).toString();

         if(firstRow)
         {
            holdContentType = contentType;
            firstRow = false;

            //last record
            if(count == rowCount)
            {
                allOrderContent.append((String)rs.getObject(4));
                if(holdContentType.equals("text"))
                {
                  emailConfirmation.setOrderContent(allOrderContent.toString());
                }
                else
                {
                  emailConfirmation.setOrderContentHTML(allOrderContent.toString());
                }
                break;
            }
         }

         if(holdContentType.equals(contentType))
         {
            allOrderContent.append((String)rs.getObject(4));

            //last record
            if(count == rowCount)
            {
                allOrderContent.append((String)rs.getObject(4));
                if(holdContentType.equals("text"))
                {
                  emailConfirmation.setOrderContent(allOrderContent.toString());
                }
                else
                {
                  emailConfirmation.setOrderContentHTML(allOrderContent.toString());
                }
            }
         }
         else
         {
            if(holdContentType.equals("text"))
            {
              emailConfirmation.setOrderContent(allOrderContent.toString());
            }
            else
            {
              emailConfirmation.setOrderContentHTML(allOrderContent.toString());
            }

            allOrderContent = new StringBuffer();
            allOrderContent.append((String)rs.getObject(4));
            holdContentType = contentType;
         }
      }
      */
      return emailConfirmation;
  }

  /**
   * Obtains source code information
   *
   *
   * @return SourceCodeVO
   *
   * @throws java.lang.Exception
   */
  public SourceMasterVO getSourceCodeRecord(String sourceCode)
        throws Exception
  {
    CachedResultSet rs = null;
		DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("GET_SOURCE_CODE_RECORD");
    dataRequest.addInputParam("IN_SOURCE_CODE",    sourceCode);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

    dataRequest.reset();
    SourceMasterVO sourceVO = null;
    while(rs.next())
    {
      sourceVO = new SourceMasterVO();
      sourceVO.setBillingInfoLogic(rs.getString("billing_info_logic"));
      sourceVO.setBillingInfoPrompt(rs.getString("billing_info_prompt"));
      sourceVO.setBinNumberCheckFlag(rs.getString("bin_number_check_flag"));
      sourceVO.setBonusPoints(rs.getBigDecimal("bonus_points"));
      sourceVO.setBonusType(rs.getString("bonus_type"));
      sourceVO.setCompanyId(rs.getString("company_id"));
      sourceVO.setDefaultSourceCodeFlag(rs.getString("default_source_code_flag"));
      sourceVO.setDepartmentCode(rs.getString("department_code"));
      sourceVO.setDescription(rs.getString("description"));
      sourceVO.setDiscountAllowedFlag(rs.getString("discount_allowed_flag"));
      sourceVO.setEmergencyTextFlag(rs.getString("emergency_text_flag"));
      sourceVO.setEnableLpProcessing(rs.getString("enable_lp_processing"));
      sourceVO.setEndDate((Date)rs.getObject("end_date"));
      sourceVO.setExternalCallCenterFlag(rs.getString("external_call_center_flag"));
      sourceVO.setFraudFlag(rs.getString("fraud_flag"));
      sourceVO.setHighlightDescriptionFlag(rs.getString("highlight_description_flag"));
      sourceVO.setJcpenneyFlag(rs.getString("jcpenney_flag"));
      sourceVO.setListCodeFlag(rs.getString("list_code_flag"));
      sourceVO.setMarketingGroup(rs.getString("marketing_group"));
      sourceVO.setMaximumPointsMiles(rs.getString("maximum_points_miles"));
      sourceVO.setMileageBonus(rs.getString("mileage_bonus"));
      sourceVO.setMileageCalculationSource(rs.getString("mileage_calculation_source"));
      sourceVO.setOeDefaultFlag(rs.getString("oe_default_flag"));
      sourceVO.setOrderSource(rs.getString("order_source"));
      sourceVO.setPartnerId(rs.getString("partner_id"));
      sourceVO.setPointsMilesPerDollar(rs.getString("points_miles_per_dollar"));
      sourceVO.setPricingCode(rs.getString("pricing_code"));
      sourceVO.setPromotionCode(rs.getString("promotion_code"));
      sourceVO.setRequiresDeliveryConfirmation(rs.getString("requires_delivery_confirmation"));
      sourceVO.setSendToScrub(rs.getString("send_to_scrub"));
      sourceVO.setSeparateData(rs.getString("separate_data"));
      sourceVO.setShippingCode(rs.getString("shipping_code"));
      sourceVO.setSourceCode(rs.getString("source_code"));
      sourceVO.setSourceType(rs.getString("source_type"));
      sourceVO.setStartDate((Date)rs.getObject("start_date"));
      sourceVO.setValidPayMethod(rs.getString("valid_pay_method"));
      sourceVO.setWebloyaltyFlag(rs.getString("webloyalty_flag"));
      sourceVO.setYellowPagesCode(rs.getString("yellow_pages_code"));
      sourceVO.setCompanyName(rs.getString("company_name"));
      sourceVO.setInternetOrigin(rs.getString("internet_origin"));
    }

        return sourceVO;
  }


  /**
   * get an encrypted value for the master order number
   *
   * @return SourceCodeVO
   *
   * @throws java.lang.Exception
   */
  public String getCrefMD5(String masterOrderNumber)
        throws Exception
  {

      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(this.connection);
      dataRequest.setStatementID("MD5_ENCRYPT");
      dataRequest.addInputParam("IN_STRING", masterOrderNumber);

      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

      String encryptedString = (String) dataAccessUtil.execute(dataRequest);

      return encryptedString;
  }

  /**
   * insert a record in the clean.webloyalty_master
   *
   * @param OrderVO - order information
   * @param cref - MD5 encrypted master order number
   * @param userId - user id
   *
   * @throws java.lang.Exception
   */
  public void insertWebloyalty(OrderVO order, String cref, String userId) throws Exception
  {
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(this.connection);

      dataRequest.setStatementID("INSERT_WEBLOYALTY_MASTER");
      dataRequest.addInputParam("IN_ORDER_GUID", order.getGUID());
      dataRequest.addInputParam("IN_CUSTOMER_TXN_REF_TXT", cref);
      dataRequest.addInputParam("IN_CREATED_BY", userId);
      dataRequest.addInputParam("IN_UPDATED_BY", userId);

      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

      Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      String status = (String) outputs.get(OrderEmailConstants.STATUS_PARAM);
      if(status != null && status.equals("N"))
      {
          String message = (String) outputs.get(OrderEmailConstants.MESSAGE_PARAM);
          throw new Exception(message);
      }

  }


  /**
   * Obtains global parms
   *
   * @return global parms map
   *
   * @throws java.lang.Exception
   */
  public HashMap getGlobalParms()
        throws Exception
  {
    CachedResultSet rs = null;
		DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("GET_FRP_GLOBAL_PARMS");

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

    dataRequest.reset();
    HashMap globalMap = new HashMap();
    while(rs.next())
    {
      globalMap.put(rs.getString("context") + rs.getString("name"), rs.getString("value"));
    }

    return globalMap;

  }


}