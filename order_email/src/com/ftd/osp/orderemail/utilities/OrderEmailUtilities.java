package com.ftd.osp.orderemail.utilities;

import com.ftd.messagegenerator.StockMessageGenerator;
import com.ftd.messagegenerator.constants.MessageConstants;
import com.ftd.messagegenerator.vo.PointOfContactVO;
import com.ftd.osp.orderemail.OrderEmailDAO;
import com.ftd.osp.orderemail.constants.OrderEmailConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.SegmentationUtil;
import com.ftd.osp.utilities.WebloyaltyUtilities;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.SourceMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.SourceMasterVO;
import com.ftd.osp.utilities.email.BuildEmailConstants;
import com.ftd.osp.utilities.email.BuildOrderEmailHelper;
import com.ftd.osp.utilities.email.EmailVO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;


/**
 * This class was initially created while implementing Webloyalty
 *
 * It will contain all the common methods that can be used and called upon from order_email
 *
 * @author Ali Lakhani
 */
public class OrderEmailUtilities
{

  private Logger logger;
  private static final String LOGGER_CATEGORY = "com.ftd.osp.orderemail.utilities.OrderEmailUtilities";
  private static final String SOURCE_CODE_HANDLER_NAME = "CACHE_NAME_SOURCE_MASTER";
  private static final String GLOBAL_PARM_HANDLER = "CACHE_NAME_GLOBAL_PARM";

/*******************************************************************************************
 * Constructor - default constructor
 *******************************************************************************************/
  public OrderEmailUtilities()
  {
    this.logger = new Logger(LOGGER_CATEGORY);
  }


  /**
     * retrieve and return the source code info
     * @param connection
     * @param source code
     * @return source code vo
     * @throws Exception
     */
  public SourceMasterVO retrieveSourceCodeInfo(Connection conn, 
                                         String sourceCode)
    throws Exception
  {
    //check source code
     SourceMasterVO sVO;

    // Get source code info from cache handler.  If nothing found return with error.
    try
    {
      SourceMasterHandler sch = (SourceMasterHandler) CacheManager.getInstance().getHandler(SOURCE_CODE_HANDLER_NAME);
      sVO = sch.getSourceCodeById(sourceCode);
    }
    catch (Exception e)
    {
      logger.info("WARNING - WARNING -------------------- Source code could not be retrieved from the cache  -------------------- ");
    }
    finally
    {
      OrderEmailDAO oeDAO = new OrderEmailDAO(conn);
      sVO = oeDAO.getSourceCodeRecord(sourceCode);
    }

    if (sVO != null)
      return sVO;
    else
      throw new Exception("Source code not found");

  }

  /**
     * retrieve and return the global parm info for webloyalty
     * @param connection
     * @return Hashmap containing Webloyalty global parms
     * @throws Exception
     */
  public HashMap retrieveWebloyaltyGlobalParms(Connection conn)
    throws Exception
  {
    HashMap wlMap = new HashMap();
    Map globalMap = new HashMap();
    try
    {
      GlobalParmHandler gph = (GlobalParmHandler) CacheManager.getInstance().getHandler(GLOBAL_PARM_HANDLER);
      globalMap = (HashMap) gph.getFrpGlobalParmMap();
    }
    catch (Exception e)
    {
      logger.info("WARNING - WARNING -------------------- Global Parms could not be retrieved from the cache  -------------------- ");
    }
    finally
    {
      if (globalMap == null)
      {
        OrderEmailDAO oeDAO = new OrderEmailDAO(conn);
        globalMap = oeDAO.getGlobalParms();
      }
    }

    if (globalMap != null)
    {
      wlMap.put(OrderEmailConstants.HK_EMAIL_SLOGAN_1, globalMap.get(OrderEmailConstants.HK_EMAIL_SLOGAN_1));
      wlMap.put(OrderEmailConstants.HK_EMAIL_SLOGAN_2, globalMap.get(OrderEmailConstants.HK_EMAIL_SLOGAN_2));
      wlMap.put(OrderEmailConstants.HK_OFFER_POOL_ID_DATA_ORDER_CONF, globalMap.get(OrderEmailConstants.HK_OFFER_POOL_ID_DATA_ORDER_CONF));
      wlMap.put(OrderEmailConstants.HK_OFFER_POOL_ID_LITERAL, globalMap.get(OrderEmailConstants.HK_OFFER_POOL_ID_LITERAL));
      wlMap.put(OrderEmailConstants.HK_OFFER_SRC_ID_DATA_ORDER_CONF, globalMap.get(OrderEmailConstants.HK_OFFER_SRC_ID_DATA_ORDER_CONF));
      wlMap.put(OrderEmailConstants.HK_OFFER_SRC_ID_LITERAL, globalMap.get(OrderEmailConstants.HK_OFFER_SRC_ID_LITERAL));
      wlMap.put(OrderEmailConstants.HK_SERVER, globalMap.get(OrderEmailConstants.HK_SERVER));
      wlMap.put(OrderEmailConstants.HK_URL_PARAMETER_1, globalMap.get(OrderEmailConstants.HK_URL_PARAMETER_1));
      wlMap.put(OrderEmailConstants.HK_URL_PARAMETER_2, globalMap.get(OrderEmailConstants.HK_URL_PARAMETER_2));
      wlMap.put(OrderEmailConstants.HK_VENDOR_ID_DATA, globalMap.get(OrderEmailConstants.HK_VENDOR_ID_DATA));
      wlMap.put(OrderEmailConstants.HK_VENDOR_ID_LITERAL, globalMap.get(OrderEmailConstants.HK_VENDOR_ID_LITERAL));
      return wlMap;
    }
    else
      throw new Exception("");

  }

  /**
     * create the unencrypted offer string
     * 
     * @param Hashmap containing webloyalty offer data to be included as part of href
     * @return String that will contain webloyalty offer data to be included as part of href
     */
  public String createUnEncryptedOffer(HashMap wlOfferMap)
  {
    StringBuffer unEncryptedSB = new StringBuffer();

    char cTokenizer = (char) OrderEmailConstants.DEFAULT_TOKENIZER;

    //append src literal
    unEncryptedSB.append(wlOfferMap.get(OrderEmailConstants.HK_OFFER_SRC_ID_LITERAL));
    unEncryptedSB.append(cTokenizer);

    //append src data
    unEncryptedSB.append(wlOfferMap.get(OrderEmailConstants.HK_OFFER_SRC_ID_DATA_ORDER_CONF));
    unEncryptedSB.append(cTokenizer);

    //append pool literal
    unEncryptedSB.append(wlOfferMap.get(OrderEmailConstants.HK_OFFER_POOL_ID_LITERAL));
    unEncryptedSB.append(cTokenizer);

    //append pool data
    unEncryptedSB.append(wlOfferMap.get(OrderEmailConstants.HK_OFFER_POOL_ID_DATA_ORDER_CONF));

    return unEncryptedSB.toString();

  }


  /**
     * create the unencrypted token - note the master order is MD5 encrypted
     * 
     * @param Hashmap containing webloyalty token data to be included as part of href
     * @return String that will contain webloyalty token data to be included as part of href
     */
  public String createUnEncryptedToken(HashMap wlMap, String MD5EncryptedMasterOrderNumber)
  {
    StringBuffer unEncryptedSB = new StringBuffer();

    char cTokenizer = (char) OrderEmailConstants.DEFAULT_TOKENIZER;

    //append vendor literal
    unEncryptedSB.append(wlMap.get(OrderEmailConstants.HK_VENDOR_ID_LITERAL));
    unEncryptedSB.append(cTokenizer);

    //append vendor data
    unEncryptedSB.append(wlMap.get(OrderEmailConstants.HK_VENDOR_ID_DATA));
    unEncryptedSB.append(cTokenizer);

    //append cref literal
    unEncryptedSB.append(OrderEmailConstants.CREF);
    unEncryptedSB.append(cTokenizer);

    //append cref data - this is MD5 encrypted master order number
    unEncryptedSB.append(MD5EncryptedMasterOrderNumber);

    return unEncryptedSB.toString();

  }


  /**
     * process Web loyalty code, and return the URL with encrypted info.  
     * 
     * processing includes:
     *    retrieve and create an offer string (a Webloyalty URL parm) from the Global
     *    encrypt the offer string using Webloyalty encryptiong
     *    encrypt the master order number using MD5 encryption
     *    insert record in the CLEAN.webloyalty_master table
     *    create and return a url for webloyalty
     *    
     * @param connection
     * @param source code
     * @return source code vo
     */
  public String processWebloyalty(Connection conn, OrderVO order, String userId) throws Exception
  {
    WebloyaltyUtilities wlUtil = new WebloyaltyUtilities();
    OrderEmailDAO oeDAO = new OrderEmailDAO(conn);
    String wlUrl = "";
    HashMap wlGlobalMap; 

    //variables used to create a token
    String MD5EncryptedMasterOrderNumber = null;
    String unEncryptedToken = null;
    String wlEncryptedToken = null;

    //variables uses to create offer
    String unEncryptedOffer = null;
    String wlEncryptedOffer = null;

    //retrieve the global parms
    wlGlobalMap = this.retrieveWebloyaltyGlobalParms(conn);
    
    //create unencrypted Offer string using the parms from wlMap
    unEncryptedOffer = this.createUnEncryptedOffer(wlGlobalMap);
    
    //encrypted the offer string
    wlEncryptedOffer = wlUtil.encrypt(unEncryptedOffer);

    //retrieve todays date
    Calendar cToday = Calendar.getInstance(); 

    String toBeMD5Encrypted = order.getMasterOrderNumber()+cToday.getTimeInMillis(); 

    //encrypt the master order number using MD5 hash
    MD5EncryptedMasterOrderNumber = oeDAO.getCrefMD5(toBeMD5Encrypted);

    String MD5EncryptedHexMasterOrderNumber = "";

    //convert the MD5 hash value to its hex representation
    for (int i = 0; i<MD5EncryptedMasterOrderNumber.length(); i++)
    {
      MD5EncryptedHexMasterOrderNumber += Integer.toHexString(MD5EncryptedMasterOrderNumber.charAt(i));
    }

    //create an unencrypted token.
    unEncryptedToken = this.createUnEncryptedToken(wlGlobalMap, MD5EncryptedHexMasterOrderNumber);

    //encrypt the token using Webloyalty encryption
    wlEncryptedToken = wlUtil.encrypt(unEncryptedToken);

    logger.info("OrderEmailUtilities - toBeMD5Encrypted = " + toBeMD5Encrypted +
                " and MD5EncryptedHexMasterOrderNumber = " + MD5EncryptedHexMasterOrderNumber +
                " and unEncryptedOffer = " + unEncryptedOffer +
                " and wlEncryptedOffer = " + wlEncryptedOffer +
                " and unEncryptedToken = " + unEncryptedToken +
                " and wlEncryptedToken = " + wlEncryptedToken);

    //insert records in the webloyalty master
    oeDAO.insertWebloyalty(order, MD5EncryptedHexMasterOrderNumber, userId);

    //create the URL
    wlUrl += ((String)wlGlobalMap.get(OrderEmailConstants.HK_SERVER)); 
    wlUrl += "?"; 
    wlUrl += ((String)wlGlobalMap.get(OrderEmailConstants.HK_URL_PARAMETER_1)); 
    wlUrl += "="; 
    wlUrl += wlEncryptedOffer; 
    wlUrl += "&"; 
    wlUrl += ((String)wlGlobalMap.get(OrderEmailConstants.HK_URL_PARAMETER_2)); 
    wlUrl += "="; 
    wlUrl += wlEncryptedToken; 

    return wlUrl;   
  }
  
  
    /**
       * Retrive information for a point of contact VO and insert it into the database 
       * it can cart an order with multiple orders
       * @param Connection conn
       * @param EmailVO emailVO
       * @param OrderVO orderVO
       */
    
    public void insertOrderIntoPointOfContactTable(Connection conn, EmailVO emailVO, OrderVO orderVO) throws Exception
    {
  
      StockMessageGenerator stockMessageGenerator = new StockMessageGenerator(conn);
      ArrayList <PointOfContactVO> pointOfContactVOs = new ArrayList <PointOfContactVO> ();
      
      if(orderVO != null && emailVO != null && conn != null)
      {
        if (orderVO.getOrderDetail() != null)
        {
          for (int i = 0; i < orderVO.getOrderDetail().size(); i++)
          {
            OrderDetailsVO orderDetailsVO = ((OrderDetailsVO) orderVO.getOrderDetail().get(i));
            if(orderDetailsVO != null && orderDetailsVO.getExternalOrderNumber() != null && orderDetailsVO.getOrderDetailId() != 0
               && orderVO.getMasterOrderNumber() != null && orderVO.getGUID() != null)
            {
       
              PointOfContactVO pointOfContactVO = new PointOfContactVO();
             
              pointOfContactVO.setEmailSubject(emailVO.getSubject());
              pointOfContactVO.setBody(emailVO.getContent());
              pointOfContactVO.setPointOfContactType(MessageConstants.EMAIL_MESSAGE_TYPE);
              pointOfContactVO.setSentReceivedIndicator("O");
              pointOfContactVO.setExternalOrderNumber(orderDetailsVO.getExternalOrderNumber());
              pointOfContactVO.setOrderDetailId(orderDetailsVO.getOrderDetailId());
              pointOfContactVO.setMasterOrderNumber(orderVO.getMasterOrderNumber());
              pointOfContactVO.setOrderGuid(orderVO.getGUID());
              pointOfContactVO.setUpdatedBy("SYS");
              pointOfContactVO.setSenderEmailAddress(emailVO.getSender());
            
              pointOfContactVO.setRecipientEmailAddress(emailVO.getRecipient());
              pointOfContactVO.setCommentType(MessageConstants.EMAIL_MESSAGE_TYPE);
                
              pointOfContactVOs.add(pointOfContactVO);
            }
          }
        }
      }
      for (PointOfContactVO pointOfContactVO : pointOfContactVOs)
      {
        stockMessageGenerator.insertMessage(pointOfContactVO);
      }

    }

  public String replacePhoneToken(Connection conn, String originalContent, OrderVO order)
    throws Exception
  {
  
    try 
    {
      BuildOrderEmailHelper helper = new BuildOrderEmailHelper(conn);
      return helper.replacePhoneToken(originalContent, order);
    }
    catch (Exception e)
    {
        SystemMessengerVO sysMessage = new SystemMessengerVO();
        sysMessage.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
        sysMessage.setSource("EMAIL CONFIRMATION");
        sysMessage.setType("ERROR");
        sysMessage.setMessage(e.getMessage());
        sysMessage.setSubject("Error obtaing email confirmation tier level phone number");
        SystemMessenger.getInstance().send(sysMessage, conn);
        throw e;
      }
  }


  public String replaceSourceCodeToken(Connection conn, String originalContent, OrderVO order)
    throws Exception
  {
    try 
    {
      BuildOrderEmailHelper helper = new BuildOrderEmailHelper(conn);
      return helper.replaceSourceCodeToken(originalContent, order);
    } catch (Exception e)
    {
        SystemMessengerVO sysMessage = new SystemMessengerVO();
        sysMessage.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
        sysMessage.setSource("EMAIL CONFIRMATION");
        sysMessage.setType("ERROR");
        sysMessage.setMessage(e.getMessage());
        sysMessage.setSubject("Error obtaing email confirmation source code information");
        SystemMessenger.getInstance().send(sysMessage, conn);
        throw e;    
    }

  }

}


