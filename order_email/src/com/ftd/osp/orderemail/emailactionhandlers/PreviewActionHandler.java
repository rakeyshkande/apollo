package com.ftd.osp.orderemail.emailactionhandlers;

import com.ftd.osp.utilities.email.EmailDAO;
import com.ftd.osp.utilities.email.EmailVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.orderemail.interfaces.IEmailActionHandler;
import com.ftd.osp.orderemail.EmailActionHandlerBase;
import com.ftd.osp.utilities.email.BuildOrderEmailHelper;

/**
 * This object handles requests to send a preview email.  This object receives
 * an email guid, retrieves the email from the databse, and then sends the email.
 */
public class PreviewActionHandler extends EmailActionHandlerBase implements IEmailActionHandler 
{

  private Logger logger;

  /** Constructor **/
  public PreviewActionHandler()
  {
    //logger
    logger = new Logger("com.ftd.osp.orderemail.emailactionhandlers.PreviewActionHandler");  
  }

  /** The main method of the handler **/
  public void process() throws Exception
  {
      logger.debug("Executing email preview action for action guid " + this.getActionGUID());  

      BuildOrderEmailHelper helper = new BuildOrderEmailHelper(this.getConnection());

      //Load email object from DB
      EmailDAO emailDAO = new EmailDAO(this.getConnection());

      logger.debug("Starting to process email GUID:" + this.getActionGUID());
      
      EmailVO emailVO = emailDAO.getEmail(this.getActionGUID());      


      
      //send email
      this.sendEmail(emailVO);
    
  }
}