package com.ftd.osp.orderemail.emailactionhandlers;

import com.ftd.osp.orderemail.EmailActionHandlerBase;
import com.ftd.osp.orderemail.interfaces.IEmailActionHandler;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.email.BuildOrderEmailHelper;
import com.ftd.osp.utilities.email.EmailDAO;
import com.ftd.osp.utilities.email.EmailVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.email.BuildEmailConstants;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;


public class RemovePendingActionHandler extends EmailActionHandlerBase implements IEmailActionHandler 
{
    private Logger logger;
    
    public RemovePendingActionHandler()
    {
        logger = new Logger("com.ftd.osp.orderemail.emailactionhandlers.RemovePendingActionHandler");
    }

    public void process() throws Exception
    {
        logger.debug("Executing email remove or pending action for action guid " + this.getActionGUID());

        logger.debug("Starting to process email GUID:" + this.getActionGUID());
  
        // Load email from database
        EmailDAO emailDAO = new EmailDAO(this.getConnection());
        EmailVO emailVO = emailDAO.getEmail(this.getActionGUID());
        emailVO.setContentType(EmailVO.MULTIPART);

        
        //retrieve the email content and replace any hard returns with html break tags
        //replace link tokens as well
        String emailContent = emailVO.getContent();
        emailContent = emailContent.replaceAll("\n", "<BR>");
        emailContent = emailContent.replaceAll("http://www.ftd.com/350/catalog/international.epl", "internationalflowerdelivery");
        emailContent = emailContent.replaceAll("http://www.ftd.com/rspecial", "emailspecial");
        emailContent = emailContent.replaceAll("http://custserv.ftd.com/email.epl", "<a href=\"http://custserv.ftd.com/\"><font size=\"2\">http://custserv.ftd.com</font></a>");
        emailContent = emailContent.replaceAll("custserv@ftd.com", "<a href=\"http://custserv.ftd.com/\"><font size=\"2\">http://custserv.ftd.com</font></a>");
        emailContent = emailContent.replaceAll("http://www.ftd.com", "<a href=\"http://www.ftd.com/\"><font size=\"2\">http://www.ftd.com</font></a>");
        emailContent = emailContent.replaceAll("www.walmart.com/ftdflowers", "<a href=\"http://www.walmart.com/ftdflowers/\"><font size=\"2\">http://www.walmart.com/ftdflowers</font></a>");
        emailContent = emailContent.replaceAll("http://www.walmart.com/cservice/cu_commentsonline.gsp\\?cu_heading\\=8", "<a href=\"http://www.walmart.com/cservice/cu_commentsonline.gsp?cu_heading=8/\"><font size=\"2\">http://www.walmart.com/cservice/cu_commentsonline.gsp?cu_heading=8</font></a>");
        emailContent = emailContent.replaceAll("internationalflowerdelivery", "<a href=\"http://www.ftd.com/international-flower-delivery//\"><font size=\"2\">http://www.ftd.com/international-flower-delivery/</font></a>");
        emailContent = emailContent.replaceAll("emailspecial", "<a href=\"http://www.ftd.com/rspecial/\"><font size=\"2\">http://www.ftd.com/rspecial</font></a>");
       
        BuildOrderEmailHelper helper = null;
        helper = new BuildOrderEmailHelper(this.getConnection());
        emailVO.appendHTMLContent(helper.startGlobalHTML());
        emailVO.appendHTMLContent(emailContent);
        emailVO.appendHTMLContent(helper.closeGlobalHTML());
        
        this.sendEmail(emailVO);
    }
}
