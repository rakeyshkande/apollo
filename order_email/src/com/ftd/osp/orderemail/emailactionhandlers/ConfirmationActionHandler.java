package com.ftd.osp.orderemail.emailactionhandlers;

import java.util.HashMap;
import java.util.Map;

import com.ftd.osp.orderemail.EmailActionHandlerBase;
import com.ftd.osp.orderemail.constants.OrderEmailConstants;
import com.ftd.osp.orderemail.interfaces.IEmailActionHandler;
import com.ftd.osp.orderemail.utilities.OrderEmailUtilities;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.SourceMasterVO;
import com.ftd.osp.utilities.email.BuildEmailConstants;
import com.ftd.osp.utilities.email.BuildOrderEmailHelper;
import com.ftd.osp.utilities.email.EmailVO;
import com.ftd.osp.utilities.order.MercentOrderPrefixes;
import com.ftd.osp.utilities.order.RecalculateOrderBO;
import com.ftd.osp.utilities.order.ScrubMapperDAO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.plugins.Logger;


public class ConfirmationActionHandler extends EmailActionHandlerBase implements IEmailActionHandler
{
  BuildOrderEmailHelper helper = null;
  private Logger logger;
  private static final String SOURCE_CODE_HANDLER_NAME  = "SOURCE";


  public ConfirmationActionHandler()
  {
    //logger
    logger = new Logger("com.ftd.osp.orderemail.emailactionhandlers.ConfirmationActionHandler");
  }

  public void process() throws Exception
  {
      logger.debug("Starting to process order GUID:" + this.getActionGUID());

      helper = new BuildOrderEmailHelper(this.getConnection());
      OrderEmailUtilities oeUtil = new OrderEmailUtilities();
      ScrubMapperDAO dao = new ScrubMapperDAO(this.getConnection());
      OrderVO order = this.prepareOrder(dao.mapOrderFromDB(this.getActionGUID()));
      
      EmailVO emailVO = this.prepareEmail(order);
      
      BuildOrderEmailHelper helper = new BuildOrderEmailHelper(this.getConnection());
      helper.updateEmailOrderTokens(emailVO, order);
      
      // Send email
      this.sendEmail(emailVO);
      
      /* This is referenced in defect 1498 and was to be part of 6335, but it was pulled
       * Uncommenting this will put the confirmation email into the POC table.
       * 
      //Log it to the POC table
      try
      {
          oeUtil.insertOrderIntoPointOfContactTable(this.getConnection(), emailVO, order);
      }
      catch (Exception e)
      {
          logger.error("Exception thrown while saving to point of contact table");
          logger.error(e);
      }
      */
      
      //System.out.println(emailVO.getHTMLContent());
  }

  public EmailVO prepareEmail(OrderVO order) throws Exception
  {
      
    String contactInformationMessageText;
    String contactInformationMessageHtml;
    String headerText;
    String headerHtml;
    HashMap wlGlobalMap = new HashMap(); 
    
    boolean isMercentOrder = false;
    MercentOrderPrefixes mercentOrderPrefixes = new MercentOrderPrefixes(this.getConnection());
	if (mercentOrderPrefixes != null && order.getOrderOrigin() != null && mercentOrderPrefixes.isMercentOrder(order.getOrderOrigin())) {
		logger.debug("The order is mercent order. Skip Body and manage spaces.");
		isMercentOrder = true;
	}

    EmailVO emailVO = new EmailVO();

    /**************Issue 3113 - start*************/
    OrderEmailUtilities oeUtil = new OrderEmailUtilities();
    String wlUrl="";

    //retrieve Source code information.
    SourceMasterVO sVO = oeUtil.retrieveSourceCodeInfo(this.getConnection(),  order.getSourceCode());
    if (sVO == null)
      throw new Exception ("Source code was not found");

    //find out if the source code was for webloyalty
    boolean webloyaltyOrder = sVO.getWebloyaltyFlag().equalsIgnoreCase("Y")?true:false;

    //if webloyalty order, process for webloyalty.
    if (webloyaltyOrder)
    {
      wlUrl = oeUtil.processWebloyalty(this.getConnection(), order, OrderEmailConstants.CONFIRMATION_USER);

      //retrieve the global parms
      wlGlobalMap = oeUtil.retrieveWebloyaltyGlobalParms(this.getConnection());

    }
    /**************Issue 3113 - end*************/

      String buyerEmail = helper.extractBuyerEmail(order);
      if(buyerEmail == null || buyerEmail.equals(""))
      throw new Exception("Order " + order.getGUID() + "does not have buyer email");

      Map sections = helper.getEmailSectionContent(order);
      emailVO.setRecipient(buyerEmail);
      emailVO.setSender((String)sections.get(BuildEmailConstants.FROM_ADDRESS));
      emailVO.setSubject((String)sections.get(BuildEmailConstants.SUBJECT_CONTEXT));

      emailVO.appendHTMLContent(helper.startGlobalHTML());
//HEADER
      //As part of 7191, it was decided that the header section may also contain tokens, like sourcecode.  
      headerText = (String)sections.get(BuildEmailConstants.HEADER_CONTENT);
      headerHtml = (String)sections.get(BuildEmailConstants.HEADER_CONTENT_HTML);

      //replace phone number tokens
      headerText = oeUtil.replacePhoneToken(this.getConnection(), headerText, order); 
      headerHtml = oeUtil.replacePhoneToken(this.getConnection(), headerHtml, order );
        
      //replace source code tokens
      headerText = oeUtil.replaceSourceCodeToken(this.getConnection(), headerText, order); 
      headerHtml = oeUtil.replaceSourceCodeToken(this.getConnection(), headerHtml, order );

      emailVO.appendContent(headerText);
      emailVO.appendHTMLContent(OrderEmailConstants.SPAN_HEADER);
      emailVO.appendHTMLContent(headerHtml);
      emailVO.appendHTMLContent(OrderEmailConstants.SPAN_END);
      
      //BIN changed message
      binCheck: 
        if (order.getOrderDetail() != null)
        {
          for (int i = 0; i < order.getOrderDetail().size(); i++)
          {
            OrderDetailsVO od = ((OrderDetailsVO) order.getOrderDetail().get(i));
            if(od != null && od.getBinSourceChangedFlag() != null && od.getBinSourceChangedFlag().equalsIgnoreCase("Y"))
            {
              ConfigurationUtil cu = ConfigurationUtil.getInstance();
              String binChangeConfirmationHTML  = cu.getContentWithFilter(this.getConnection(), BuildEmailConstants.BIN_CONTEXT, 
                                                                          BuildEmailConstants.CONFIRMATION_EMAIL_TEXT, BuildEmailConstants.HTML, null);
              String binChangeConfirmationText  = cu.getContentWithFilter(this.getConnection(), BuildEmailConstants.BIN_CONTEXT, 
                                                                          BuildEmailConstants.CONFIRMATION_EMAIL_TEXT, BuildEmailConstants.TEXT_FILTER, null);
              if (binChangeConfirmationHTML != null && binChangeConfirmationText != null)
              {
                emailVO.appendContent("\n");
                emailVO.appendContent(binChangeConfirmationText);
                emailVO.appendHTMLContent(OrderEmailConstants.SPAN_BIN_CHANGE);
                emailVO.appendHTMLContent(binChangeConfirmationHTML);
                emailVO.appendHTMLContent(OrderEmailConstants.SPAN_END);

                logger.debug("Inserting BIN changed message into confirmation email");
                logger.debug("Text version: " + binChangeConfirmationText);
                logger.debug("HTML version: " + binChangeConfirmationHTML);
              }
              else
              {
                if (binChangeConfirmationText == null)
                {
                  logger.error("Could not find message to insert into email for bin change." +
                    "Context: " + BuildEmailConstants.BIN_CONTEXT + ", " + BuildEmailConstants.CONFIRMATION_EMAIL_TEXT +
                    " Fliter: " +BuildEmailConstants.TEXT_FILTER);
                }
                
                if (binChangeConfirmationHTML == null)
                {
                  logger.error("Could not find message to insert into email for bin change." +
                    "Context: " + BuildEmailConstants.BIN_CONTEXT + ", " + BuildEmailConstants.CONFIRMATION_EMAIL_TEXT +
                    "Fliter: " +BuildEmailConstants.HTML);
                }
              }
  
              break binCheck;
            }
          }
        }
//WEBLOYALTY - FIRST LINK
      if (webloyaltyOrder)
      {
        //append content
        //note that we no longer will put the bracketed html in the text emails due to rendering issues.
        emailVO.appendContent("\n\n");
        emailVO.appendContent((String)wlGlobalMap.get(OrderEmailConstants.HK_EMAIL_SLOGAN_1));
        emailVO.appendContent("\n");
        emailVO.appendContent(wlUrl);
        emailVO.appendContent("\n");

        //append html content
        emailVO.appendHTMLContent(OrderEmailConstants.SPAN_LOYALTY_1);
        emailVO.appendHTMLContent("<BR><BR>");
        emailVO.appendHTMLContent("<a href=");
        emailVO.appendHTMLContent(wlUrl);
        emailVO.appendHTMLContent(">");
        emailVO.appendHTMLContent((String)wlGlobalMap.get(OrderEmailConstants.HK_EMAIL_SLOGAN_1));
        emailVO.appendHTMLContent("</a>");
        emailVO.appendHTMLContent("<BR>");
        emailVO.appendHTMLContent(OrderEmailConstants.SPAN_END);
      }

//ORDER INFORMATION
      emailVO.appendHTMLContent(OrderEmailConstants.SPAN_ORDER_DETAIL);
      if(order.getMasterOrderNumber().startsWith("B"))
      {
        emailVO.appendContent((String)helper.getBulkOrderInfoSection(order).get(BuildEmailConstants.PLAIN_TEXT));
        emailVO.appendHTMLContent((String)helper.getBulkOrderInfoSection(order).get(BuildEmailConstants.HTML));
      }
      else
      {		// #18531 - For mercent orders it should be skipped.
			if (!isMercentOrder) {
				emailVO.appendContent((String) helper.getOrderInfoSection(order).get(BuildEmailConstants.PLAIN_TEXT));
				emailVO.appendHTMLContent((String) helper.getOrderInfoSection(order).get(BuildEmailConstants.HTML));
			}
      }
      emailVO.appendHTMLContent(OrderEmailConstants.SPAN_END);


//WEBLOYALTY - SECOND LINK
      if (webloyaltyOrder)
      {
        //append content
        //note that we no longer will put the bracketed html in the text emails due to rendering issues.
        emailVO.appendContent("\n\n");
        emailVO.appendContent((String)wlGlobalMap.get(OrderEmailConstants.HK_EMAIL_SLOGAN_2));
        emailVO.appendContent("\n");
        emailVO.appendContent(wlUrl);
        emailVO.appendContent("\n");

        //append html content
        emailVO.appendHTMLContent(OrderEmailConstants.SPAN_LOYALTY_2);
        emailVO.appendHTMLContent("<BR><BR>");
        emailVO.appendHTMLContent("<a href=");
        emailVO.appendHTMLContent(wlUrl);
        emailVO.appendHTMLContent(">");
        emailVO.appendHTMLContent((String)wlGlobalMap.get(OrderEmailConstants.HK_EMAIL_SLOGAN_2));
        emailVO.appendHTMLContent("</a>");
        emailVO.appendHTMLContent("<BR>");
        emailVO.appendHTMLContent(OrderEmailConstants.SPAN_END);
      }

//GUARANTEE

      emailVO.appendContent("\n");
      emailVO.appendContent((String)sections.get(BuildEmailConstants.GUARANTEE_CONTENT));
      emailVO.appendHTMLContent(OrderEmailConstants.SPAN_GUARANTEE);
      emailVO.appendHTMLContent((String)sections.get(BuildEmailConstants.GUARANTEE_CONTENT_HTML));
      emailVO.appendHTMLContent(OrderEmailConstants.SPAN_END);

//MARKETING
      emailVO.appendContent("\n");     
      emailVO.appendContent("\n");
      emailVO.appendHTMLContent(OrderEmailConstants.SPAN_MARKETING);
      if (!isMercentOrder) {
    	  emailVO.appendHTMLContent("<BR>");
    	  emailVO.appendHTMLContent("<BR>");
      }
      emailVO.appendContent((String)sections.get(BuildEmailConstants.MARKETING_CONTENT));
      emailVO.appendHTMLContent((String)sections.get(BuildEmailConstants.MARKETING_CONTENT_HTML));
      emailVO.appendHTMLContent(OrderEmailConstants.SPAN_END);

//CONTACT US
      emailVO.appendContent("\n");     
      emailVO.appendContent("\n");
      emailVO.appendHTMLContent(OrderEmailConstants.SPAN_CONTACT);
      if (!isMercentOrder) {
    	  emailVO.appendHTMLContent("<BR>");
    	  emailVO.appendHTMLContent("<BR>");
      }
      
      contactInformationMessageText = ((String)sections.get(BuildEmailConstants.CONTACT_CONTENT));
      contactInformationMessageHtml = ((String)sections.get(BuildEmailConstants.CONTACT_CONTENT_HTML));

      //replace phone number tokens
      contactInformationMessageText = oeUtil.replacePhoneToken(this.getConnection(), contactInformationMessageText, order); 
      contactInformationMessageHtml = oeUtil.replacePhoneToken(this.getConnection(), contactInformationMessageHtml, order );
        
      //replace source code tokens
      contactInformationMessageText = oeUtil.replaceSourceCodeToken(this.getConnection(), contactInformationMessageText, order); 
      contactInformationMessageHtml = oeUtil.replaceSourceCodeToken(this.getConnection(), contactInformationMessageHtml, order );

      emailVO.appendContent(contactInformationMessageText);
      emailVO.appendHTMLContent(contactInformationMessageHtml);
      emailVO.appendHTMLContent(OrderEmailConstants.SPAN_END);

      
      //free-shipping indicator
        boolean freeshippingFlag = false;
        if (order.getOrderDetail() != null)
        {
        	for (int i = 0; i < order.getOrderDetail().size(); i++)
            {
              OrderDetailsVO od = ((OrderDetailsVO) order.getOrderDetail().get(i));
              if(od != null && "Y".equalsIgnoreCase(od.getFreeShipping()))
              {
            	  logger.debug("FreeShipping :"+od.getFreeShipping());
            	  freeshippingFlag = true;
                  break;
              }
            }
        }
        String goldMembCartInd = freeshippingFlag?"Y":"N";
        emailVO.appendHTMLContent(OrderEmailConstants.LABEL_GOLD_MEMBERSHIP_CART_INDICATOR);
        emailVO.appendHTMLContent(goldMembCartInd);
        emailVO.appendHTMLContent(OrderEmailConstants.LABEL_END);
        
      emailVO.appendHTMLContent(helper.closeGlobalHTML());
      emailVO.setContentType(emailVO.MULTIPART);
      
      return emailVO;
  }

  private OrderVO prepareOrder(OrderVO order) throws Exception
  {
      for (int i = 0; i < order.getOrderDetail().size(); i++)
      {
         if(!((OrderDetailsVO) order.getOrderDetail().get(i)).getStatus().equals("2006") && 
            !((OrderDetailsVO) order.getOrderDetail().get(i)).getStatus().equals("2007") )         
         {
               logger.info("Items in cart:" +  order.getOrderDetail().size());
               logger.info("Removing order detail " + ((OrderDetailsVO) order.getOrderDetail().get(i)).getOrderDetailId() + " with status " + ((OrderDetailsVO) order.getOrderDetail().get(i)).getStatus());
               order.getOrderDetail().remove(i);
               i--;
         }
      }
      // Recalculate order with new item list
      if(order.isPartnerOrder()) {
          logger.info("Skipping recalculateOrder since Partner order");
      } else if (order.getOrderOrigin().equalsIgnoreCase("roses")) {
          logger.info("Skipping recalculateOrder since Roses.com web order");
      } else {
    	  RecalculateOrderBO recalculateOrder = new RecalculateOrderBO();
    	  recalculateOrder.recalculate(this.getConnection(), order);
      }
      return order;
  }

}