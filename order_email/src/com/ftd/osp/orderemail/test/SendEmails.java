package com.ftd.osp.orderemail.test;

import java.util.*;
import java.sql.*;

import org.w3c.dom.*;
import javax.xml.parsers.*;

import com.ftd.osp.utilities.interfaces.IBusinessObject;
import com.ftd.osp.utilities.vo.*;
import com.ftd.osp.utilities.j2ee.*;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.*;
import com.ftd.osp.utilities.dataaccess.util.*;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.orderemail.*;
import com.ftd.osp.orderemail.vo.*;
import java.net.*;

public class SendEmails
{
    public SendEmails() 
    {
    }

    
    public static void main(String[] args)
    {
        Connection conn = null;
        String database_ = "jdbc:oracle:thin:@stheno-dev.ftdi.com:1521:DEV2";
        String user_ = "djohnson";
        String password_ = "djohnson";

        try
        {
            OSPConnectionPool connectionPool = OSPConnectionPool.getInstance();
            conn = connectionPool.getConnection(database_,user_,password_);

            OrderEmailBO orderEmailBO = new OrderEmailBO();

            BusinessConfig config = new BusinessConfig();
            MessageToken token = new MessageToken();
            token.setMessage("FTD_GUID_383962357056767398601745070006441187590144512684801124393530-5603261160986132602189483349901597418883012560217930-2184597062495040425571-18535752321-1469872555228-208890832186:DISPATCHER");
            config.setConnection(conn);
            config.setInboundMessageToken(token);
            orderEmailBO.execute(config);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }  
     }
}