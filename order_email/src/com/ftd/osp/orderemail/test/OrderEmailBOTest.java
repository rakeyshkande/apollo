package com.ftd.osp.orderemail.test;

import com.ftd.osp.orderemail.*;
import com.ftd.osp.utilities.interfaces.IBusinessObject;
import com.ftd.osp.utilities.vo.*;
import com.ftd.osp.utilities.dataaccess.util.*;
import java.io.*;
import java.sql.*;
import java.sql.Connection;

import junit.framework.TestCase;
import junit.framework.TestSuite;


/**
 * Test class for Sending email confirmations to customer via JMS Email Service 
 *
 * @author Doug Johnson
 */
public class OrderEmailBOTest  extends TestCase
{
    Connection connection = null;
    private boolean mappedSuccessfully = false;
    
    
    /** 
     * Create a constructor that take a String parameter and passes it 
     * to the super class 
     * @param name String
     **/ 
    public OrderEmailBOTest(String name) { 
        super(name); 
        try 
        {
        } catch (Exception ex) 
        {
          ex.printStackTrace();
        } 
    }

    /**
     * Method for setting up initialization parameters
     */
    protected void setUp()
    { 
       
    }

   /** 
    * Test sending order email confirmation message to customer
    * A boolean value of false indicates that there was
    * an error building record.
    **/ 
    public void testSendEmailToCustomer() throws Exception
    { 
        System.out.println("testing build email confirmation database records for SCRUB messages");
 
        connection = this.getConnection();

        //get a connection
        //check to see that the connection is not null
        assertNotNull(connection);

        OrderEmailBO orderEmailBO = new OrderEmailBO();

        BusinessConfig config = new BusinessConfig();
        MessageToken token = new MessageToken();
        //token.setMessage("FTD_GUID_383962357056767398601745070006441187590144512684801124393530-5603261160986132602189483349901597418883012560217930-2184597062495040425571-18535752321-1469872555228-208890832186:DISPATCHER");
        //String guid = "CANCELLED_AMAZON_ORDER.FTD_GUID_-3611505980718788907068338282901313396651018010004160-6562644530-6055101106521022421-17921378070-122809908605411858380-1771112660251-52581962368121630732173-119370388474-399152074214";
        
        String guid = "CANCELLED_AMAZON_ORDER.FTD_GUID_-9174731690-1698296754016201102720-11592223610-11394813260-4870424206911286460-7572753931-172918322207399583740-2722294110-1833644647254-386063502187-40833171151-11901520482431637320487208";

        token.setMessage(guid);
        config.setConnection(connection);
        config.setInboundMessageToken(token);
        orderEmailBO.execute(config);
       
        //Check if the result is the same as expected
        //A result of true is expected if the order is
        //successfully converted to XML
        assertEquals("testEmailFromSCRUB", true, true);
    } 

    
    private Connection getConnection() 
    {
        Connection conn = null;
        String database_ = "jdbc:oracle:thin:@stheno-dev.ftdi.com:1521:DEV5";
        String user_ = "osp";
        String password_ = "osp";

        try
        {
            OSPConnectionPool connectionPool = OSPConnectionPool.getInstance();
            conn = connectionPool.getConnection(database_,user_,password_);
        }
        catch(Exception e)
        {
             e.printStackTrace();
        }
       
        return conn;
    }

    public static TestSuite suite()
    {
      TestSuite suite = new TestSuite();
      try 
      {
        //test for Scrub email
        suite.addTest(new OrderEmailBOTest("testSendEmailToCustomer"));

      } catch (Exception e) {
          e.printStackTrace();
      }

      return suite;
    } 

    public static void main(String args[])
    {
      junit.textui.TestRunner.run( suite() );
    }

}