package com.ftd.osp.orderemail;

import com.ftd.osp.orderemail.interfaces.*;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.j2ee.ResourceUtil;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;
import java.util.HashMap;
import java.io.InputStream;
import org.w3c.dom.Document;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;


public class EmailActionHandlerManager
{
  private static Logger logger = new Logger("com.ftd.osp.orderemail.EmailActionHandlerManager");
  private static final String ACTION_DELIMITER = ".";
  private static HashMap handlerMap = null;
  private String configFile = "email-action-config.xml";
  private Connection connection = null;
  
  public EmailActionHandlerManager(Connection connection)
  {
      this.connection = connection;
  }

  public IEmailActionHandler resolveActionHandler(String message) throws Exception
  {
     if(handlerMap == null)
        this.loadHandlerMap();

     String handlerClassName = null;
     String[] messagePart = this.resolveMessage(message);
     try
     {
        handlerClassName = (String)(handlerMap.get(messagePart[0]));
     }
     catch(Exception e)
     {
       logger.error(e);
       throw new Exception("Invalid action specified (->" + messagePart[0] + "<-");
     }

     if(handlerClassName == null || handlerClassName.equals(""))
     {
       throw new Exception("Invalid action specified (->" + messagePart[0] + "<-)");
     }

     Object o = this.loadHandler(handlerClassName);
     ((EmailActionHandlerBase)o).setConnection(this.connection);
     ((EmailActionHandlerBase)o).setActionGUID(messagePart[1]);     
      
     return (IEmailActionHandler)o;
  }

  private String[] resolveMessage(String message) throws Exception
  {
    if(message == null || message.equals("") || message.indexOf(ACTION_DELIMITER) < 0)
    {
      throw new Exception("Message does not contain valid Action and/or Action GUID"); 
    }
    String[] ret = new String[2]; 
    ret[0] = message.substring(0, message.indexOf(ACTION_DELIMITER));
    ret[1] = message.substring(message.indexOf(ACTION_DELIMITER)+1, message.length());    
    return ret;
  }

  private synchronized void loadHandlerMap() throws Exception
  {
      if(handlerMap == null)
      {
        handlerMap = new HashMap();
   
        InputStream actionConfigFile = ResourceUtil.getInstance().getResourceFileAsStream(this.configFile);
    
        Document emailConfig = (Document)DOMUtil.getDocument(actionConfigFile);   
        NodeList emailActionsNodeList = (NodeList) DOMUtil.selectNodes(emailConfig, "/email-actions-config/actions/action");
        Element emailActionsElement;

        if(emailActionsNodeList.getLength() == 0)
          throw new Exception(this.configFile + " was empty or invalid"); 

        for (int i = 0; i < emailActionsNodeList.getLength(); i++)  
        {
          emailActionsElement = (Element) (emailActionsNodeList.item(i));
          handlerMap.put(emailActionsElement.getAttribute("id"), emailActionsElement.getAttribute("handler"));
        }
      } 
  }

  private Object loadHandler(String handlerName) throws Exception
  {
     Object handler = null;

     try
     {
       handler = (Class.forName(handlerName)).newInstance();
     }
     catch(Exception e)
     {
       logger.error(e);
       throw new Exception ("Unable to create Action Handler object for name " + handlerName);
     }
     
     return handler;
  }  
}