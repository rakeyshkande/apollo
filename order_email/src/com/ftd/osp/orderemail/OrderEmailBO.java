package com.ftd.osp.orderemail;

import java.sql.Connection;

import com.ftd.osp.utilities.order.test.order.ConnectionTest;

import com.ftd.osp.orderemail.interfaces.*;
import com.ftd.osp.orderemail.vo.EmailConfirmationVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.interfaces.IBusinessObject;
import com.ftd.osp.utilities.j2ee.NotificationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.BusinessConfig;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.vo.NotificationVO;



public class OrderEmailBO implements IBusinessObject
{
    private static Logger logger = new Logger("com.ftd.osp.orderemail.OrderEmailBO");
    private final static String EMAIL_CONFIG_FILE = "email-config.xml";

    public OrderEmailBO()
    {}

    public void execute(BusinessConfig config)
    {
      MessageToken outBoundToken = new MessageToken();

      try
      {
          logger.debug("EMAIL - Send email to server:");
          MessageToken inboundToken = config.getInboundMessageToken();
          EmailActionHandlerManager eahm = new EmailActionHandlerManager(config.getConnection());
          IEmailActionHandler handler = eahm.resolveActionHandler((String)inboundToken.getMessage());
          handler.process();  
      }
      catch(Throwable t)
      {
          logger.error(t);
          System.out.println(t.toString());
          outBoundToken.setStatus("ROLLBACK");
          config.setOutboundMessageToken(outBoundToken);
      }
      finally
      {
          //handle
      }
    }

    public static void main(String args[]) throws Exception
    {
      Connection connection = null;
      try
      {
        connection = ConnectionTest.getConnection();
        MessageToken tm = new MessageToken();
        tm.setMessage("PREVIEW.FTD_GUID_-11527525530-11193525210-16666394201313169956018201225730-19373506100-10563743170819293508116257838240-15782860400-5698687610-1477739392252-202054285945-744439002178-218434446119-3519697131");
        //tm.setMessage("REINSTATE_CONFIRMATION.FTD_GUID_8423882620126772587401476333708012178482700-20488985300-5384957120-7678180380-133161816615085500390-190407018608706742601540144673249-134991000555-136735482143-843708173190-2023660418204.1");
        BusinessConfig config = new BusinessConfig();
        config.setInboundMessageToken(tm); //<--------------------------------------------------------
        config.setConnection(connection);
        OrderEmailBO bo = new OrderEmailBO();
        bo.execute(config);  
      }
      catch(Exception e)
      {
        e.printStackTrace();
      }
      finally
      {
        //close the connection
        try
        {
          connection.close();
        }
        catch(Exception e)
        {}
      }
      
    }
}