package com.ftd.osp.orderemail.interfaces;

public interface IEmailActionHandler 
{
  public void process() throws Exception;  
}