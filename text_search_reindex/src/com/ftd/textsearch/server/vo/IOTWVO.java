package com.ftd.textsearch.server.vo;

import org.apache.solr.common.SolrInputDocument;


/**
 * Value object for Item of the Week data for Text Search.
 */
public class IOTWVO implements SolrVO
{
    private String productId;
    private String novatorId;
    private String productName;
    private String longDescription;
    private String dominantFlowers;
    private String standardPrice;
    private String companyId;
    private String sourceCode;
    private String sourceCodeDescription;
    private String sourceCodeType;
    private String sourceCodePriceHeaderId;
    private String iotwId;
    private String iotwSourceCode;
    private String iotwSourceCodeDescription;
    private String iotwSourceCodeType;
    private String iotwSourceCodePriceHeaderId;
    private String productPageMessage;

    /**
     * Convert the VO to a format tha can be used by Solr for processing.
     * @return
     */
    public SolrInputDocument getSolrDocument()
    {
        SolrInputDocument doc = new SolrInputDocument();
        doc.setField("product_id", this.getProductId());
        doc.setField("novator_id", this.getNovatorId());
        doc.setField("product_name", this.getProductName());
        doc.setField("long_description", this.getLongDescription());
        doc.setField("dominant_flowers", this.getDominantFlowers());
        doc.setField("standard_price", this.getStandardPrice());
        doc.setField("company_id", this.getCompanyId());
        doc.setField("source_code", this.getSourceCode());
        doc.setField("source_code_description", this.getSourceCodeDescription());
        doc.setField("source_type", this.getSourceCodeType());
        doc.setField("source_code_price_header_id", this.getSourceCodePriceHeaderId());
        doc.setField("iotw_source_code", this.getIotwSourceCode());
        doc.setField("iotw_source_code_description", this.getIotwSourceCodeDescription());
        doc.setField("iotw_source_type", this.getIotwSourceCodeType());
        doc.setField("iotw_source_code_price_header_id", this.getIotwSourceCodePriceHeaderId());
        doc.setField("product_page_message", this.getProductPageMessage());
        doc.setField("iotw_company", this.getIOTWCompany());
        
        return doc;
    }

    public void setProductId(String param)
    {
        this.productId = param;
    }

    public String getProductId()
    {
        return productId;
    }

    public void setNovatorId(String param)
    {
        this.novatorId = param;
    }

    public String getNovatorId()
    {
        return novatorId;
    }

    public void setProductName(String param)
    {
        this.productName = param;
    }

    public String getProductName()
    {
        return productName;
    }

    public void setLongDescription(String param)
    {
        this.longDescription = param;
    }

    public String getLongDescription()
    {
        return longDescription;
    }

    public void setDominantFlowers(String param)
    {
        this.dominantFlowers = param;
    }

    public String getDominantFlowers()
    {
        return dominantFlowers;
    }

    public void setCompanyId(String param)
    {
        this.companyId = param;
    }

    public String getCompanyId()
    {
        return companyId;
    }

    public void setSourceCode(String param)
    {
        this.sourceCode = param;
    }

    public String getSourceCode()
    {
        return sourceCode;
    }

    public void setSourceCodeDescription(String param)
    {
        this.sourceCodeDescription = param;
    }

    public String getSourceCodeDescription()
    {
        return sourceCodeDescription;
    }

    public void setSourceCodeType(String param)
    {
        this.sourceCodeType = param;
    }

    public String getSourceCodeType()
    {
        return sourceCodeType;
    }

    public void setIotwSourceCode(String param)
    {
        this.iotwSourceCode = param;
    }

    public String getIotwSourceCode()
    {
        return iotwSourceCode;
    }

    public void setIotwSourceCodeDescription(String param)
    {
        this.iotwSourceCodeDescription = param;
    }

    public String getIotwSourceCodeDescription()
    {
        return iotwSourceCodeDescription;
    }

    public void setIotwSourceCodeType(String param)
    {
        this.iotwSourceCodeType = param;
    }

    public String getIotwSourceCodeType()
    {
        return iotwSourceCodeType;
    }

    public void setProductPageMessage(String param)
    {
        this.productPageMessage = param;
    }

    public String getProductPageMessage()
    {
        return productPageMessage;
    }

    public String getIOTWCompany()
    {
        return iotwId + companyId;
    }

    public void setIotwId(String param)
    {
        this.iotwId = param;
    }

    public String getIotwId()
    {
        return iotwId;
    }

    public void setStandardPrice(String standardPrice)
    {
        this.standardPrice = standardPrice;
    }

    public String getStandardPrice()
    {
        return standardPrice;
    }

    public void setSourceCodePriceHeaderId(String sourceCodePriceHeaderId)
    {
        this.sourceCodePriceHeaderId = sourceCodePriceHeaderId;
    }

    public String getSourceCodePriceHeaderId()
    {
        return sourceCodePriceHeaderId;
    }

    public void setIotwSourceCodePriceHeaderId(String iotwSourceCodePriceHeaderId)
    {
        this.iotwSourceCodePriceHeaderId = iotwSourceCodePriceHeaderId;
    }

    public String getIotwSourceCodePriceHeaderId()
    {
        return iotwSourceCodePriceHeaderId;
    }
}
