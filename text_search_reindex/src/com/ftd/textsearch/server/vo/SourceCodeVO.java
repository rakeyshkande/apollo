package com.ftd.textsearch.server.vo;

import org.apache.solr.common.SolrInputDocument;

/**
 * Value object for Source Code.
 */
public class SourceCodeVO implements SolrVO
{
    String sourceCode;
    String sourceType;
    String description;
    String companyId;

    /**
     * Convert the VO to a format tha can be used by Solr for processing.
     * @return
     */
    public SolrInputDocument getSolrDocument()
    {
        SolrInputDocument doc = new SolrInputDocument();
        doc.setField("source_code", this.getSourceCode());
        doc.setField("source_type", this.getSourceType());
        doc.setField("description", this.getDescription());
        doc.setField("company_id", this.getCompanyId());
        return doc;
    }

    public String getSourceCode()
    {
        return sourceCode;
    }

    public void setSourceCode(String sourceCode)
    {
        this.sourceCode = sourceCode;
    }

    public String getSourceType()
    {
        return sourceType;
    }

    public void setSourceType(String sourceType)
    {
        this.sourceType = sourceType;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getCompanyId()
    {
        return companyId;
    }

    public void setCompanyId(String companyId)
    {
        this.companyId = companyId;
    }
}
