package com.ftd.textsearch.server.vo;

import org.apache.solr.common.SolrInputDocument;


/**
 * Value object for Product information for the Text Search.
 */
public class ProductVO implements SolrVO
{
    private String productId;
    private String novatorId;
    private String productName;
    private String novatorName;
    private String standardPrice;
    private String deluxePrice;
    private String premiumPrice;
    private String shortDescription;
    private String longDescription;
    private String shipMethodCarrier;
    private String shipMethodFlorist;
    private String dominantFlowers;
    private String countryId;
    private String orderEntryKeyword;
    private String discountAllowedFlag;
    private String indexName;
    private String companyId;
    private String popularity;
    private String codificationId;
    private String over21Flag;
    private String sourceCodes;
    private String allowFreeShippingFlag;
        
    /**
     * Convert the VO to a format tha can be used by Solr for processing.
     * @return
     */
    public SolrInputDocument getSolrDocument()
    {
        SolrInputDocument doc = new SolrInputDocument();
        doc.setField("product_id", this.getProductId());
        doc.setField("novator_id", this.getNovatorId());
        doc.setField("product_name", this.getProductName());
        doc.setField("novator_name", this.getNovatorName());
        doc.setField("standard_price", this.getStandardPrice());
        doc.setField("deluxe_price", this.getDeluxePrice());
        doc.setField("premium_price", this.getPremiumPrice());
        doc.setField("short_description", this.getShortDescription());
        doc.setField("long_description", this.getLongDescription());
        doc.setField("ship_method_carrier", this.getShipMethodCarrier());
        doc.setField("ship_method_florist", this.getShipMethodFlorist());
        doc.setField("dominant_flowers", this.getDominantFlowers());
        doc.setField("country_id", this.getCountryId());
        doc.setField("order_entry_keyword", this.getOrderEntryKeyword());
        doc.setField("index_name", this.getIndexName());
        doc.setField("company_id", this.getCompanyId());
        doc.setField("discount_allowed_flag", this.getDiscountAllowedFlag());
        doc.setField("popularity", this.getPopularity());
        doc.setField("product_company", this.getProductCompany());
        doc.setField("codification_id", this.getCodificationId());
        doc.setField("over_21", this.getOver21Flag());
        doc.setField("source_codes", this.getSourceCodes());
        doc.setField("allow_free_shipping_flag", this.getAllowFreeShippingFlag());
        return doc;
    }

    public void setProductId(String param)
    {
        this.productId = param;
    }

    public String getProductId()
    {
        return productId;
    }

    public void setNovatorId(String param)
    {
        this.novatorId = param;
    }

    public String getNovatorId()
    {
        return novatorId;
    }

    public void setProductName(String param)
    {
        this.productName = param;
    }

    public String getProductName()
    {
        return productName;
    }

    public void setStandardPrice(String param)
    {
        this.standardPrice = param;
    }

    public String getStandardPrice()
    {
        return standardPrice;
    }

    public void setDeluxePrice(String param)
    {
        this.deluxePrice = param;
    }

    public String getDeluxePrice()
    {
        return deluxePrice;
    }

    public void setPremiumPrice(String param)
    {
        this.premiumPrice = param;
    }

    public String getPremiumPrice()
    {
        return premiumPrice;
    }

    public void setShortDescription(String param)
    {
        this.shortDescription = param;
    }

    public String getShortDescription()
    {
        return shortDescription;
    }

    public void setLongDescription(String param)
    {
        this.longDescription = param;
    }

    public String getLongDescription()
    {
        return longDescription;
    }

    public void setShipMethodCarrier(String param)
    {
        this.shipMethodCarrier = param;
    }

    public String getShipMethodCarrier()
    {
        return shipMethodCarrier;
    }

    public void setShipMethodFlorist(String param)
    {
        this.shipMethodFlorist = param;
    }

    public String getShipMethodFlorist()
    {
        return shipMethodFlorist;
    }

    public void setDominantFlowers(String param)
    {
        this.dominantFlowers = param;
    }

    public String getDominantFlowers()
    {
        return dominantFlowers;
    }

    public void setCountryId(String param)
    {
        this.countryId = param;
    }

    public String getCountryId()
    {
        return countryId;
    }

    public void setOrderEntryKeyword(String param)
    {
        this.orderEntryKeyword = param;
    }

    public String getOrderEntryKeyword()
    {
        return orderEntryKeyword;
    }

    public void setIndexName(String param)
    {
        this.indexName = param;
    }

    public String getIndexName()
    {
        return indexName;
    }

    public void setCompanyId(String param)
    {
        this.companyId = param;
    }

    public String getCompanyId()
    {
        return companyId;
    }

    public void setDiscountAllowedFlag(String param)
    {
        this.discountAllowedFlag = param;
    }

    public String getDiscountAllowedFlag()
    {
        return discountAllowedFlag;
    }

    public void setNovatorName(String param)
    {
        this.novatorName = param;
    }

    public String getNovatorName()
    {
        return novatorName;
    }

    public void setPopularity(String param)
    {
        this.popularity = param;
    }

    public String getPopularity()
    {
        return popularity;
    }
    
    public String getProductCompany()
    {
        return productId + companyId;
    }

    public void setCodificationId(String codificationId)
    {
        this.codificationId = codificationId;
    }

    public String getCodificationId()
    {
        return codificationId;
    }

    public void setOver21Flag(String param)
    {
        this.over21Flag = param;
    }

    public String getOver21Flag()
    {
        return over21Flag;
    }
    
    public void setSourceCodes(String param)
    {
        this.sourceCodes = param;
    }

    public String getSourceCodes()
    {
        return sourceCodes;
    }


    public void setAllowFreeShippingFlag(String allowFreeShippingFlag) {
        this.allowFreeShippingFlag = allowFreeShippingFlag;
    }

    public String getAllowFreeShippingFlag() {
        return allowFreeShippingFlag;
    }
}
