package com.ftd.textsearch.server.vo;

import org.apache.solr.common.SolrInputDocument;


/**
 * Interface for Value Objects used the by the Solr text searching.
 */
public interface SolrVO
{
    /**
     * Convert the VO to a format tha can be used by Solr for processing.
     * @return
     */
    public SolrInputDocument getSolrDocument();
}
