package com.ftd.textsearch.server.vo;

import org.apache.solr.common.SolrInputDocument;

/**
 * Value object for the Facility data.
 */
public class FacilityVO implements SolrVO
{
    private String businessId;
    private String businessType;
    private String businessName;
    private String address;
    private String city;
    private String state;
    private String zipCode;
    private String phoneNumber;

    /**
     * Convert the VO to a format tha can be used by Solr for processing.
     * @return
     */
    public SolrInputDocument getSolrDocument()
    {
        SolrInputDocument doc = new SolrInputDocument();
        doc.setField("business_id", this.getBusinessId());
        doc.setField("business_type", this.getBusinessType());
        doc.setField("business_name", this.getBusinessName());
        doc.setField("address", this.getAddress());
        doc.setField("city", this.getCity());
        doc.setField("state", this.getState());
        doc.setField("zip_code", this.getZipCode());
        doc.setField("phone_number", this.getPhoneNumber());
        doc.setField("business_type_id", this.getBusinessTypeId());

        return doc;
    }

    public void setBusinessId(String param)
    {
        this.businessId = param;
    }

    public String getBusinessId()
    {
        return businessId;
    }

    public void setBusinessType(String param)
    {
        this.businessType = param;
    }

    public String getBusinessType()
    {
        return businessType;
    }

    public void setBusinessName(String param)
    {
        this.businessName = param;
    }

    public String getBusinessName()
    {
        return businessName;
    }

    public void setAddress(String param)
    {
        this.address = param;
    }

    public String getAddress()
    {
        return address;
    }

    public void setCity(String param)
    {
        this.city = param;
    }

    public String getCity()
    {
        return city;
    }

    public void setState(String param)
    {
        this.state = param;
    }

    public String getState()
    {
        return state;
    }

    public void setZipCode(String param)
    {
        this.zipCode = param;
    }

    public String getZipCode()
    {
        return zipCode;
    }

    public void setPhoneNumber(String param)
    {
        this.phoneNumber = param;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public String getBusinessTypeId()
    {
        return getBusinessType() + getBusinessId();
    }

}
