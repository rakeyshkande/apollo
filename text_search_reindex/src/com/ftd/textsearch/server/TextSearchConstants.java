package com.ftd.textsearch.server;

/**
 * Constants used by the Text Search Reindex process.
 */
public interface TextSearchConstants
{
    public final static String PROPERTY_FILE = "tsr-config.xml";
    public final static String DATASOURCE_NAME = "DATASOURCE";
    
    public final static String REINDEX_COMMAND             = "REINDEX";
    public final static String UPDATE_COMMAND              = "UPDATE";
    public final static String COMMAND_TARGET_ALL          = "ALL";
    public final static String COMMAND_TARGET_PRODUCT      = "PRODUCT";
    public final static String COMMAND_TARGET_SOURCE_CODE  = "SOURCE_CODE";
    public final static String COMMAND_TARGET_IOTW         = "IOTW";
    public final static String COMMAND_TARGET_FACILITY     = "FACILITY";
    
}
