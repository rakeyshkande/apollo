    package com.ftd.textsearch.server.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.textsearch.server.vo.IOTWVO;
import com.ftd.textsearch.server.vo.ProductVO;
import com.ftd.textsearch.server.vo.SolrVO;

import com.ftd.textsearch.server.vo.SourceCodeVO;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Text Search Data Access Object for performing database queries
 * for the Text Search Reindex.
 */
public class TextSearchDAO
{
    protected Logger logger = new Logger(this.getClass().getName());

    public TextSearchDAO()
    {
    }

    /**
     * Get the list of all active source codes.  The list is 
     * filtered by the the number of days the source code was last used
     * in.
     * @param conn Database connection
     * @param lastUsedInDays filter for number of days ago the source code must have been used in
     * @return List of SourceCodeVO
     * @throws Exception
     */
    public List<SolrVO> getActiveSourceCodes(Connection conn,
                                             Long lastUsedInDays) throws Exception 
    {
        SourceCodeVO vo;
        List<SolrVO> sourceCodeList = new ArrayList<SolrVO>();

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_LAST_USED_IN_DAYS", lastUsedInDays);

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_ACTIVE_SOURCE_CODES");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        while ( results != null && results.next() ) 
        {
            vo = new SourceCodeVO();

            vo.setCompanyId(results.getString("COMPANY_ID"));
            vo.setDescription(results.getString("DESCRIPTION"));
            vo.setSourceCode(results.getString("SOURCE_CODE"));
            vo.setSourceType(results.getString("SOURCE_TYPE"));
            
            sourceCodeList.add(vo);
        }

        return sourceCodeList;
    }

    /**
     * Get the list of active products.
     * @param conn Database Connection
     * @return List of ProductVO
     * @throws Exception
     */
    public void getActiveProduct(Connection conn, String productID, List<SolrVO> productList) throws Exception
    {
        ProductVO vo = null;

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("PRODUCT_ID", productID);

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_ACTIVE_PRODUCT");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);


        // Loop because of multiple companies for one product
        while ( results != null && results.next() )
        {
            vo = new ProductVO();

            vo.setProductId(results.getString("PRODUCT_ID"));
            vo.setNovatorId(results.getString("NOVATOR_ID"));
            vo.setProductName(results.getString("PRODUCT_NAME"));
            vo.setNovatorName(results.getString("NOVATOR_NAME"));
            vo.setLongDescription(results.getString("LONG_DESCRIPTION"));
            vo.setDominantFlowers(results.getString("DOMINANT_FLOWERS"));
            vo.setCountryId(results.getString("COUNTRY_ID"));
            vo.setOrderEntryKeyword(results.getString("ORDER_ENTRY_KEYWORD"));
            if (vo.getOrderEntryKeyword() == null)
            {
                vo.setOrderEntryKeyword("");
            }
            vo.setCompanyId(results.getString("COMPANY_ID"));
            vo.setStandardPrice(results.getString("STANDARD_PRICE"));
            vo.setDeluxePrice(results.getString("DELUXE_PRICE"));
            vo.setPremiumPrice(results.getString("PREMIUM_PRICE"));
            vo.setShortDescription(results.getString("SHORT_DESCRIPTION"));
            if (vo.getShortDescription() == null)
            {
                vo.setShortDescription("");
            }
            vo.setShipMethodCarrier(results.getString("SHIP_METHOD_CARRIER"));
            vo.setShipMethodFlorist(results.getString("SHIP_METHOD_FLORIST"));
            vo.setDiscountAllowedFlag(results.getString("DISCOUNT_ALLOWED_FLAG"));
            vo.setIndexName(results.getString("INDEX_NAME"));
            if (vo.getIndexName() == null)
            {
                vo.setIndexName("");
            }
            vo.setPopularity(results.getString("POPULARITY_ORDER_CNT"));
            vo.setCodificationId(results.getString("CODIFICATION_ID"));
            vo.setOver21Flag(results.getString("OVER_21"));
            vo.setSourceCodes(results.getClob("SOURCE_CODES").getSubString((long)1, (int)results.getClob("SOURCE_CODES").length()));
            if (vo.getSourceCodes() == null)
            {
                vo.setSourceCodes("");
            }
            vo.setAllowFreeShippingFlag(results.getString("ALLOW_FREE_SHIPPING_FLAG"));
            productList.add(vo);
        }

    }

    /**
     * Get the list of active products.
     * @param conn Database Connection
     * @return List of String productIds
     * @throws Exception
     */
    public List<String> getActiveProductList(Connection conn) throws Exception
    {
        List<String> productIDList = new ArrayList<String>();

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_ACTIVE_PRODUCT_LIST");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);


        while ( results != null && results.next() ) 
        {

            String productID = results.getString("PRODUCT_ID");
            productIDList.add(productID);
        }

        return productIDList;
    }

    /**
     * Get the list of active Item Of the Weeks.  The list is filtered by the number
     * of days the code was last used on.
     * @param conn Database Connection
     * @param lastUsedInDays Number of days the code must have been last used in to be considered active.
     * @return List of IOTWVO
     * @throws Exception
     */
    public List<SolrVO> getActiveIOTWs(Connection conn,
                                Long lastUsedInDays) throws Exception 
    {
        IOTWVO vo;
        List<SolrVO> productList = new ArrayList<SolrVO>();

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_LAST_USED_IN_DAYS", lastUsedInDays);

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_ACTIVE_IOTW");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        while ( results != null && results.next() ) 
        {
            vo = new IOTWVO();

            vo.setProductId(results.getString("PRODUCT_ID"));
            vo.setNovatorId(results.getString("NOVATOR_ID"));
            vo.setProductName(results.getString("PRODUCT_NAME"));
            vo.setLongDescription(results.getString("LONG_DESCRIPTION"));
            vo.setDominantFlowers(results.getString("DOMINANT_FLOWERS"));
            vo.setStandardPrice(results.getString("STANDARD_PRICE"));
            vo.setCompanyId(results.getString("COMPANY_ID"));
            vo.setSourceCode(results.getString("SOURCE_CODE"));
            vo.setSourceCodeDescription(results.getString("SOURCE_CODE_DESCRIPTION"));
            vo.setSourceCodeType(results.getString("SOURCE_TYPE"));
            vo.setSourceCodePriceHeaderId(results.getString("SOURCE_CODE_PRICE_HEADER_ID"));
            vo.setIotwId(results.getString("IOTW_ID"));
            vo.setIotwSourceCode(results.getString("IOTW_SOURCE_CODE"));
            vo.setIotwSourceCodeDescription(results.getString("IOTW_SOURCE_CODE_DESCRIPTION"));
            vo.setIotwSourceCodeType(results.getString("IOTW_SOURCE_TYPE"));
            vo.setIotwSourceCodePriceHeaderId(results.getString("IOTW_SC_PRICE_HEADER_ID"));
            vo.setProductPageMessage(results.getString("PRODUCT_PAGE_MESSAGING_TXT"));
            
            productList.add(vo);
        }

        return productList;
    }

}
