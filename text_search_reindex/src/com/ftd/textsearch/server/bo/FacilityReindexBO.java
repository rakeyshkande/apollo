package com.ftd.textsearch.server.bo;

import com.enterprisedt.net.ftp.FTPClient;

import com.ftd.textsearch.server.vo.FacilityVO;
import com.ftd.textsearch.server.vo.SolrVO;

import java.io.BufferedReader;
import java.io.File;

import java.io.FileReader;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.List;

/**
 * Business object for handling the logic specific to facility text
 * search reindex processing.  See AbstractReindexBO for more information.
 */
public class FacilityReindexBO extends AbstractReindexBO
{
    protected static final String FACILITY_REINDEX_URL   = "FACILITY_REINDEX_URL";
    protected static final String FACILITY_FILE_SERVER   = "FACILITY_FILE_SERVER";
    protected static final String FACILITY_FILE_DIR      = "FACILITY_FILE_DIR";
    protected static final String FACILITY_FILE          = "FACILITY_FILE";
    protected static final String FACILITY_FILE_LOGIN    = "FACILITY_FILE_LOGIN";
    protected static final String FACILITY_FILE_PASSWORD = "FACILITY_FILE_PASSWORD";
    protected static final String FACILITY_LOCAL_DIR     = "FACILITY_LOCAL_DIR";
    protected static final String FACILITY_LOCAL_FILE    = "FACILITY_LOCAL_FILE";

    protected static final String FACILITY_UPDATE_END = "/textsearchfacility";

    public FacilityReindexBO(Connection conn)
    {
        super(conn);
    }

    /**
     * Get the url for the text search server for processing the reindex on.
     * @return
     * @throws Exception
     */
    protected String getSearchUrl() throws Exception
    {
        logger.debug("getSearchURL");
        String param_value = cu.getFrpGlobalParm(CONFIG_CONTEXT, FACILITY_REINDEX_URL);

        logger.info("URL = [" + param_value + "]");

        return param_value;
    }

    /**
     * Retrieve the Facililty date for adding to the index.
     * @return
     * @throws Exception
     */
    protected List<SolrVO> retrieveData() throws Exception
    {
        logger.debug("retrieveData");

        List<SolrVO> list = getFacilities();

        logger.info("retrieved " + list.size() + " facilities");   

        return list;
    }

    /**
     * Get the list of facilities.  The list of facilities is retrieved from
     * a remote server using ftp.  The file received is then parsed for the contents.
     * @return List of FacilityVO
     * @throws Exception
     */
    public List<SolrVO> getFacilities() throws Exception
    {
        logger.debug("Getting facility list");
        List<SolrVO> list = new ArrayList<SolrVO>();

        String ftpServer = cu.getFrpGlobalParm(CONFIG_CONTEXT, FACILITY_FILE_SERVER);
        String ftpDirectory = cu.getFrpGlobalParm(CONFIG_CONTEXT, FACILITY_FILE_DIR);
        String ftpFileName = cu.getFrpGlobalParm(CONFIG_CONTEXT, FACILITY_FILE);

        String localDirectory = cu.getFrpGlobalParm(CONFIG_CONTEXT, FACILITY_LOCAL_DIR);
        String localFile = cu.getFrpGlobalParm(CONFIG_CONTEXT, FACILITY_LOCAL_FILE);
        
        String ftpUser = cu.getSecureProperty(SECURE_CONFIG_CONTEXT, FACILITY_FILE_LOGIN);
        String ftpPassword = cu.getSecureProperty(SECURE_CONFIG_CONTEXT, FACILITY_FILE_PASSWORD);

        logger.info("fetching facilities via ftp from " + ftpServer);
        
        FTPClient client = null;
        try
        {
            client = new FTPClient(ftpServer);
            client.login(ftpUser, ftpPassword);
            client.chdir(ftpDirectory);
            client.get(localDirectory + "/" + localFile, ftpFileName);
        }
        finally
        {
        	// if there are any errors we are purposely swallowing them.  It is ok if it fails. per Mark Campbell
            client.quit();
        }

        File file = new File(localDirectory + "/" + localFile);
        if (file.exists())
        {
            BufferedReader inFile = new BufferedReader(new FileReader(file));

            String line = inFile.readLine();
            while (line != null)
            {
                list.add(parseLine(line));
                line = inFile.readLine();
            }

            inFile.close();
        }

        return list;
    }

    /**
     * Parse a line of the facility file and product a FacilityVO.
     * @param line Line to process
     * @return FacilityVO
     */
    protected FacilityVO parseLine(String line) 
    {
        FacilityVO facility = new FacilityVO();
        try 
        {       
            facility.setBusinessType(line.substring(0, 1).trim());
            facility.setBusinessId(line.substring(1, 6).trim());
            facility.setBusinessName(line.substring(6, 37).trim());
            facility.setAddress(line.substring(37, 68).trim());
            facility.setCity(line.substring(68, 98).trim());
            facility.setState(line.substring(98, 100).trim());
            facility.setZipCode(line.substring(100, 110).trim());
            facility.setPhoneNumber(line.substring(110, line.length()).trim());
        } 
        catch (StringIndexOutOfBoundsException sioe) 
        {
            logger.error("bad line: " + line, sioe);
        }
        
        return facility;
    }

    /**
     * Get the end of the url for the update.
     * @return
     * @throws Exception
     */
    protected String getUpdateEnd() 
    {
        return FACILITY_UPDATE_END;
    }

}
