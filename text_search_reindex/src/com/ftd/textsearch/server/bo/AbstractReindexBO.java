package com.ftd.textsearch.server.bo;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.textsearch.server.util.TextSearchUtil;
import com.ftd.textsearch.server.vo.SolrVO;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import java.net.URL;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import java.util.Map;
import java.util.StringTokenizer;

import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.CommonsHttpSolrServer;
import org.apache.solr.client.solrj.impl.XMLResponseParser;
import org.apache.solr.common.SolrInputDocument;


/**
 * Abstract base class for the logic to perform for a reindex
 * of a text search.
 */
public abstract class AbstractReindexBO
{
    protected Logger logger = new Logger(this.getClass().getName());
    
    protected static final String CONFIG_CONTEXT = "TEXT_SEARCH";
    protected static final String SECURE_CONFIG_CONTEXT = "text_search";
    protected static final String LAST_USED_DAYS = "LAST_USED_DAYS";
    protected static final String CLIENT_LIST = "CLIENT_LIST";
    protected static final String CLIENT_URL_LIST = "CLIENT_URL_LIST";

    protected static final String UPDATE_START = "http://";
    
    protected static final String LD_LIBRARY_PATH = "LD_LIBRARY_PATH";
    protected static final String LD_LIBRARY_PATH_VALUE = "/usr/sfw/lib:/usr/local/lib";
    
    protected static final String RSYNC_COMMAND = "/usr/local/bin/rsync";
    protected static final String RSYNC_ARG_DELETE = "--delete";
    protected static final String RSYNC_ARG_VERBOSE = "--verbose";
    protected static final String RSYNC_ARG_RECURSE = "-r";
    protected static final String RSYNC_ARG_REMOTE_START = "rsync@";
    protected static final String RSYNC_ARG_REMOTE_END = "::solr";
    protected static final String RSYNC_DIRECTORY = "/u02/solr";
    
    protected static final int BUNDLE_COUNT = 100;
    
    protected ConfigurationUtil cu;
    protected Connection conn;
    
    public AbstractReindexBO(Connection conn)
    {
        this.conn = conn;
        try
        {
            cu = ConfigurationUtil.getInstance();
        }
        catch (Exception e)
        {
            logger.error("Fatal Error on Creating AbstractReindexBO",e);
        }
    }

    /**
     * Reindex a text search.  The process will be:
     * 1)  Clear the index
     * 2)  Retrieve all the data for the index
     * 3)  Add all the records to the index
     * 4)  Optimize the index
     * @throws Exception
     */
    public void reindex() throws Exception
    {
        logger.info("Processing reindex");
        String url = getSearchUrl();
        SolrServer client = buildSolrClient(url);
        List data = retrieveData();
        clearIndex(client);
        buildIndex(client,data);
        optimizeIndex(client);
        
        updateClients();
    }

    /**
     * Update the clients of the text search with the new index.
     */
    public void updateClients() throws Exception
    {
        // Perform the rsync
        // Get the list of clients
        String[] clients = getClients();
        for (int i=0; i < clients.length; i++)
        {
            String remote = RSYNC_ARG_REMOTE_START + clients[i] + RSYNC_ARG_REMOTE_END;
            
            logger.debug("Sending command [" + RSYNC_COMMAND + " " + RSYNC_ARG_DELETE+ " " +RSYNC_ARG_VERBOSE+ " " + RSYNC_ARG_RECURSE+ " " +getDirectoryName()+ " " +remote);
            ProcessBuilder processBuilder = new ProcessBuilder(RSYNC_COMMAND,RSYNC_ARG_DELETE, RSYNC_ARG_VERBOSE, RSYNC_ARG_RECURSE,getDirectoryName(),remote);
            Map<String,String> env = processBuilder.environment();
            env.put(LD_LIBRARY_PATH,LD_LIBRARY_PATH_VALUE);
            processBuilder.redirectErrorStream(true);
            
            Process process = processBuilder.start();
            InputStreamReader isr = new InputStreamReader(process.getInputStream());
            BufferedReader br = new BufferedReader(isr);

            process.waitFor();
            int exitValue = process.exitValue();
            if (exitValue != 0 || logger.isDebugEnabled())
            {
                StringBuilder sb = new StringBuilder();
                String line = br.readLine();
                while (line != null)
                {
                    sb.append(line);
                    sb.append("\n");
                    line = br.readLine();
                }
                if (exitValue != 0)
                {    
                    logger.error(sb.toString());
                    TextSearchUtil.sendSystemMessage(conn,"Text Search Reindex rsnyc Failed",sb.toString());
                }
                else
                {
                    logger.debug(sb.toString());
                }
            }
            logger.info("rsync for " + clients[i] + " returned " + exitValue);
        }                

        // Get the list of clients URLs
        String[] clientURLs = getClientUrls();
        // Inform the client to update through HTTP
        for (int i=0; i < clientURLs.length; i++)
        {
            SolrServer client = buildSolrClient(clientURLs[i]);
            client.commit(true, true);
        }
        
    }
    
    /**
     * Get the url for the specific text search server to reindex on.
     * @return
     * @throws Exception
     */
    protected abstract String getSearchUrl() throws Exception;
    /**
     * Retrieve all of the specific data for to store in the text search.
     * @return
     * @throws Exception
     */
    protected abstract List<SolrVO> retrieveData() throws Exception;

    /**
     * Get the URL portion specific to the type of text search. 
     * @return
     */
    protected abstract String getUpdateEnd();

    /**
     * Get the name of the directory for the solr files.
     * @return
     */
    protected String getDirectoryName()
    {
        return RSYNC_DIRECTORY;
    }
    
    /**
     * Build the SolrClient object using the text search url.
     * @param url URL of the server to reindex on
     * @throws Exception
     */
    protected SolrServer buildSolrClient(String url) throws Exception
    {
        logger.debug("buildSolrClient");
        CommonsHttpSolrServer client = new CommonsHttpSolrServer(new URL(url));
        client.setParser(new XMLResponseParser());
        
        return client;
    }

    /**
     * Build the index using the list of SolrVO objects.  Each object is added
     * one by one to the list.
     * @param list
     */
    protected void buildIndex(SolrServer client, List list)
    {
        logger.debug("buildIndex");
        Iterator iter = list.iterator();
        try
        {
            List<SolrInputDocument> docList = new ArrayList<SolrInputDocument>();
            while (iter.hasNext())
            {
                SolrVO solrVO = (SolrVO)iter.next();
                SolrInputDocument doc = solrVO.getSolrDocument();
                docList.add(doc);
                
                if (docList.size() >= BUNDLE_COUNT)
                {
                    client.add(docList);
                    docList = new ArrayList<SolrInputDocument>();                    
                }
            }
            if (docList.size() > 0)
            {
                client.add(docList);
            }
            logger.debug("commiting index adds");
            client.commit(true, true);
        } 
        catch (IOException ioe)
        {
            logger.error("Error adding to index", ioe);
        } 
        catch (SolrServerException sse)
        {
            logger.error("Error adding to index", sse);
        }
    }


    /**
     * Clear the index.
     */
    protected void clearIndex(SolrServer client)
    {
        logger.debug("clearIndex");
        try
        {
            logger.debug("deleteByQuery");
            client.deleteByQuery("*:*");

            logger.debug("commit");
            client.commit(true, true);
        } 
        catch (IOException ioe)
        {
            logger.error("Error adding to index", ioe);
        } 
        catch (SolrServerException sse)
        {
            logger.error("Error adding to index", sse);
        }
    }

    /**
     * Optimize the index.
     */
    protected void optimizeIndex(SolrServer client)
    {
        logger.debug("optimizeIndex");
        try
        {
            client.optimize(true, true);
        } 
        catch (IOException ioe)
        {
            logger.error("Error adding to index", ioe);
        } 
        catch (SolrServerException sse)
        {
            logger.error("Error adding to index", sse);
        }
    }

    /**
     * Get the text search client URLs for processing an update of the IOTW text search.
     * @return
     * @throws Exception
     */
    protected String[] getClientUrls() throws Exception
    {
        logger.debug("getClientURLs");
        
        String clientStringList = cu.getFrpGlobalParm(CONFIG_CONTEXT, CLIENT_URL_LIST);
        StringTokenizer tokens = new StringTokenizer(clientStringList," ");
        String[] clientList = new String[tokens.countTokens()];
        int i = 0;
        StringBuilder sb;
        while (tokens.hasMoreTokens())
        {
            String hostName = tokens.nextToken();
            sb = new StringBuilder();
            sb.append(UPDATE_START);
            sb.append(hostName);
            sb.append(getUpdateEnd());
            clientList[i++] = sb.toString();
            logger.info("URL = [" + sb.toString() + "]");
        }

        return clientList;
    }

    /**
     * Get the text search client hostnames.
     * @return
     * @throws Exception
     */
    protected String[] getClients() throws Exception
    {
        logger.debug("getClients");
        
        String clientStringList = cu.getFrpGlobalParm(CONFIG_CONTEXT, CLIENT_LIST);
        StringTokenizer tokens = new StringTokenizer(clientStringList," ");
        String[] clientList = new String[tokens.countTokens()];
        int i = 0;
        while (tokens.hasMoreTokens())
        {
            String hostName = tokens.nextToken();
            clientList[i++] = hostName;
        }

        return clientList;
    }
    
}
