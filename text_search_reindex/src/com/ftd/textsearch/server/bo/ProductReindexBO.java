package com.ftd.textsearch.server.bo;

import com.ftd.textsearch.server.dao.TextSearchDAO;
import com.ftd.textsearch.server.vo.SolrVO;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.List;

/**
 * Handles the Product specific processing for the reindex of the Product
 * Text search.
 */
public class ProductReindexBO extends AbstractReindexBO
{
    protected static final String PRODUCT_REINDEX_URL = "PRODUCT_REINDEX_URL";
    protected static final String PRODUCT_UPDATE_END = "/textsearchproduct";

    public ProductReindexBO(Connection conn)
    {
        super(conn);
    }

    /**
     * Get the URL of the product text search server to reindex.
     * @return
     * @throws Exception
     */
    protected String getSearchUrl() throws Exception
    {
        logger.info("getSearchURL");
        String param_value = cu.getFrpGlobalParm(CONFIG_CONTEXT, PRODUCT_REINDEX_URL);

        logger.info("URL = [" + param_value + "]");

        return param_value;
    }

    /**
     * Retrieve the data for the product index.  The data is retrived from the database
     * using the TextSearchDAO.
     * @return List of ProductVO
     * @throws Exception
     */
    protected List<SolrVO> retrieveData() throws Exception
    {
        logger.info("retrieveData");

        TextSearchDAO dao = new TextSearchDAO();

        List<String> productIDList = dao.getActiveProductList(conn);
        logger.info("retrieved " + productIDList.size() + " products");

        List<SolrVO> productDetailList = new ArrayList<SolrVO>();
        // Get the list of products
        // Loop through the list of products, getting the details one at a time
        for (int i=0; i < productIDList.size(); i++)
        {
            String productID = productIDList.get(i);
            dao.getActiveProduct(conn,productID,productDetailList);
            if ((i % 1000) == 0)
            {
                logger.info("processed " + i);
            }
        }

        logger.info("retrieved " + productDetailList.size() + "product details");

        return productDetailList;
    }
    
    /**
     * Get the end of the url for the update.
     * @return
     * @throws Exception
     */
    protected String getUpdateEnd() 
    {
        return PRODUCT_UPDATE_END;
    }

}
