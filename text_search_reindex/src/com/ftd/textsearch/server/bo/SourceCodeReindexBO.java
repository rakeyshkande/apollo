package com.ftd.textsearch.server.bo;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.textsearch.server.dao.TextSearchDAO;
import com.ftd.textsearch.server.vo.SolrVO;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.List;

/**
 * Business Object for the reindex of the Text SEarch for Source Code.
 */
public class SourceCodeReindexBO extends AbstractReindexBO
{
    protected final static String SOURCE_CODE_REINDEX_URL = "SOURCE_CODE_REINDEX_URL";
    protected static final String SOURCE_CODE_UPDATE_END = "/textsearchsourcecode";
   
    public SourceCodeReindexBO(Connection conn)
    {
        super(conn);
    }

    /**
     * Get the URL for the text search server used for reindexing.
     * @return
     * @throws Exception
     */
    protected String getSearchUrl() throws Exception
    {
        logger.debug("getSearchURL");
        String param_value = cu.getFrpGlobalParm(CONFIG_CONTEXT, SOURCE_CODE_REINDEX_URL);
        
        logger.info("URL = [" + param_value + "]");
        
        return param_value;
    }

    /**
     * Retrieve the data needed for reindexing the text search.
     * @return
     * @throws Exception
     */
    protected List<SolrVO> retrieveData() throws Exception
    {
        logger.debug("retrieveData");
        
        TextSearchDAO dao = new TextSearchDAO();
        
        String param_value = cu.getFrpGlobalParm(CONFIG_CONTEXT, LAST_USED_DAYS);
        
        Long lastUsedInDays = Long.valueOf(param_value);
        
        List<SolrVO> list = dao.getActiveSourceCodes(conn, lastUsedInDays);
     
        logger.info("retrieved " + list.size() + " source codes");   
        
        return list;
    }
    /**
     * Get the end of the url for the update.
     * @return
     * @throws Exception
     */
    protected String getUpdateEnd() 
    {
        return SOURCE_CODE_UPDATE_END;
    }

}
