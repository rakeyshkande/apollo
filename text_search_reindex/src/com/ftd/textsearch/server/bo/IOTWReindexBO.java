package com.ftd.textsearch.server.bo;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.textsearch.server.dao.TextSearchDAO;
import com.ftd.textsearch.server.vo.SolrVO;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Reindex the Text Search for Item of the Week (IOTW).  See base
 * class AbstractReindexBO for details.
 */
public class IOTWReindexBO extends AbstractReindexBO
{
    protected static final String IOTW_REINDEX_URL = "IOTW_REINDEX_URL";
    protected static final String IOTW_UPDATE_END = "/textsearchiotw";

    public IOTWReindexBO(Connection conn)
    {
        super(conn);
    }

    /**
     * Get the text search server URL for processing a reindex of the IOTW text search.
     * @return
     * @throws Exception
     */
    protected String getSearchUrl() throws Exception
    {
        logger.debug("getSearchURL");
        String param_value = cu.getFrpGlobalParm(CONFIG_CONTEXT, IOTW_REINDEX_URL);

        logger.info("URL = [" + param_value + "]");

        return param_value;
    }

    /**
     * Get the end of the url for the update.
     * @return
     * @throws Exception
     */
    protected String getUpdateEnd() 
    {
        return IOTW_UPDATE_END;
    }

    /**
     * Retrieve all the data needed for the IOTW search.  The data is retrieved from 
     * the database through the TextSearchDAO.
     * @return List of IOTWVO objects
     * @throws Exception
     */
    protected List<SolrVO> retrieveData() throws Exception
    {
        logger.debug("retrieveData");

        TextSearchDAO dao = new TextSearchDAO();

        String param_value = cu.getFrpGlobalParm(CONFIG_CONTEXT, LAST_USED_DAYS);

        Long lastUsedInDays = Long.valueOf(param_value);

        List<SolrVO> list = dao.getActiveIOTWs(conn, lastUsedInDays);

        logger.info("retrieved " + list.size() + " iotws");   

        return list;
    }

}
