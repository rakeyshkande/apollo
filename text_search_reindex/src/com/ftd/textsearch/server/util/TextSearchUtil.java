package com.ftd.textsearch.server.util;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;

import java.sql.Connection;

public class TextSearchUtil
{
    protected static Logger logger = new Logger("com.ftd.textsearch.server.util.TextSearchUtil");

    public static String sendSystemMessage(Connection con, String subject, String message)
    {
        String messageID = "";

        try
        {
            logger.error("Sending System Message: " + message);
            String messageSource = "TEXT SEARCH REINDEX";

            //build system vo
            SystemMessengerVO sysMessage = new SystemMessengerVO();
            sysMessage.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
            sysMessage.setSource(messageSource);
            sysMessage.setType("ERROR");
            sysMessage.setMessage(message);
            sysMessage.setSubject(subject);

            SystemMessenger sysMessenger = SystemMessenger.getInstance();
            messageID = sysMessenger.send(sysMessage, con, false);

            if (messageID == null)
            {
                String msg = "Error occured while attempting to send out a system message. Msg not sent: " + message;
                logger.error(msg);
            }
        } catch (Exception e)
        {
            logger.error("Error sending system message.", e);
        }
        
        return messageID;
    }
    
}
