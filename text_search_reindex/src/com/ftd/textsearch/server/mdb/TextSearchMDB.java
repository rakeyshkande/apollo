package com.ftd.textsearch.server.mdb;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.textsearch.server.TextSearchConstants;
import com.ftd.textsearch.server.bo.FacilityReindexBO;
import com.ftd.textsearch.server.bo.IOTWReindexBO;
import com.ftd.textsearch.server.bo.ProductReindexBO;
import com.ftd.textsearch.server.bo.SourceCodeReindexBO;

import com.ftd.textsearch.server.util.TextSearchUtil;

import java.sql.Connection;

import java.util.StringTokenizer;

import javax.ejb.EJBException;
import javax.ejb.MessageDrivenBean;

import javax.ejb.MessageDrivenContext;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

/**
 * Message Drive Bean for handling reindex requests for the Text Search.
 * The bean can reindex all text searches or specific ones.
 */
public class TextSearchMDB implements MessageDrivenBean, MessageListener
{
    private MessageDrivenContext context;
    protected Logger logger = new Logger(this.getClass().getName());
    
    /**
     * Set the message drive context.
     * @param context
     * @throws EJBException
     */
    public void setMessageDrivenContext(MessageDrivenContext context) throws EJBException
    {
        this.context = context;
    }

    /**
     * Handle the processing of a message.  The message will be checked for a specific
     * command to perform.  The expected command will be to reindex with a parameter
     * to specify which index to reindex.  It can be specified to reindex all text searches.
     * @param message JMS message
     */
    public void onMessage(Message message)
    {
        Connection conn = null;

        //  Called the MDB, do something
        logger.info("Message Recived");
        
        try
        {
            if (!(message instanceof TextMessage))
            {
                throw new IllegalArgumentException("Invalid Message type " + message.getClass().getName() + " expecting TextMessage");
            }
            TextMessage textMessage = (TextMessage) message;
            
            String messageText = textMessage.getText();
            
            // The messageText holds the command to run
            logger.info("message = [" + messageText + "]");
            StringTokenizer tokens = new StringTokenizer(messageText);
            String command = "fubar";
            String commandArgument = "fubar";
            // First is the command
            if (tokens.hasMoreTokens())
            {
                command = tokens.nextToken();
            }
            // Second is the command argument
            if (tokens.hasMoreTokens())
            {
                // First is the command
                commandArgument = tokens.nextToken();
            }
            
            // get database connection      
            ConfigurationUtil cu = ConfigurationUtil.getInstance();
            String datasource = cu.getProperty(TextSearchConstants.PROPERTY_FILE, TextSearchConstants.DATASOURCE_NAME);
            conn = DataSourceUtil.getInstance().getConnection(datasource);

            if (command != null && command.equals(TextSearchConstants.REINDEX_COMMAND))
            {
                if (commandArgument != null)
                {

                    if (commandArgument.equals(TextSearchConstants.COMMAND_TARGET_ALL))    
                    {
                        reindexProduct(conn);
                        reindexSourceCode(conn);
                        reindexIOTW(conn);
                        reindexFacility(conn);
                    }
                    else if (commandArgument.equals(TextSearchConstants.COMMAND_TARGET_PRODUCT))    
                    {
                        reindexProduct(conn);
                    }
                    else if (commandArgument.equals(TextSearchConstants.COMMAND_TARGET_SOURCE_CODE))    
                    {
                        reindexSourceCode(conn);
                    }
                    else if (commandArgument.equals(TextSearchConstants.COMMAND_TARGET_IOTW))    
                    {
                        reindexIOTW(conn);
                    }
                    else if (commandArgument.equals(TextSearchConstants.COMMAND_TARGET_FACILITY))    
                    {
                        reindexFacility(conn);
                    }
                }
                else
                {
                    logger.error("Invalid Command Argument");
                }
                
            }
            else if (command != null && command.equals(TextSearchConstants.UPDATE_COMMAND))
            {
                if (commandArgument != null)
                {

                    if (commandArgument.equals(TextSearchConstants.COMMAND_TARGET_ALL))    
                    {
                        updateProduct(conn);
                        updateSourceCode(conn);
                        updateIOTW(conn);
                        updateFacility(conn);
                    }
                    else if (commandArgument.equals(TextSearchConstants.COMMAND_TARGET_PRODUCT))    
                    {
                        updateProduct(conn);
                    }
                    else if (commandArgument.equals(TextSearchConstants.COMMAND_TARGET_SOURCE_CODE))    
                    {
                        updateSourceCode(conn);
                    }
                    else if (commandArgument.equals(TextSearchConstants.COMMAND_TARGET_IOTW))    
                    {
                        updateIOTW(conn);
                    }
                    else if (commandArgument.equals(TextSearchConstants.COMMAND_TARGET_FACILITY))    
                    {
                        updateFacility(conn);
                    }
                }
                else
                {
                    logger.error("Invalid Command Argument");
                }
                
            }
            else
            {
                logger.error("Invalid Command");
            }

            
        } 
        catch (Throwable t)
        {
            logger.error("Error",t);
            TextSearchUtil.sendSystemMessage(conn,"Text Search Reindex Command Failed",t.getMessage());
        } 
        finally
        {
            //close the connection
            try
            {
                conn.close();
            } 
            catch (Exception e)
            {
                logger.error("Error",e);
            }
        }
    }

    /**
     * Execute a reindex of the SourceCode text search.
     * @param conn
     * @throws Exception
     */
    private void reindexSourceCode(Connection conn) throws Exception
    {
        // Call the appropriate BO
        logger.info("Creating the SC BO");
        SourceCodeReindexBO bo = new SourceCodeReindexBO(conn);
        logger.info("Calling the SC BO");
        bo.reindex();
        logger.info("Done SC BO");
    }

    /**
     * Execute a reindex of the Product text search.
     * @param conn
     * @throws Exception
     */
    private void reindexProduct(Connection conn) throws Exception
    {
        logger.info("Creating the P BO");
        ProductReindexBO pbo = new ProductReindexBO(conn);
        logger.info("Calling the P BO");
        pbo.reindex();
        logger.info("Done P BO");
    }

    /**
     * Perform a reindex of the IOTW text search.
     * @param conn
     * @throws Exception
     */
    private void reindexIOTW(Connection conn) throws Exception
    {
        logger.info("Creating the IOTW BO");
        IOTWReindexBO ibo = new IOTWReindexBO(conn);
        logger.info("Calling the IOTW BO");
        ibo.reindex();
        logger.info("Done IOTW BO");
    }

    /**
     * perform a reindex of the Facility text search.
     * @param conn
     * @throws Exception
     */
    private void reindexFacility(Connection conn) throws Exception
    {
        logger.info("Creating the F BO");
        FacilityReindexBO fbo = new FacilityReindexBO(conn);
        logger.info("Calling the F BO");
        fbo.reindex();
        logger.info("Done F BO");
    }

    /**
     * Execute a update of the SourceCode text search.
     * @param conn
     * @throws Exception
     */
    private void updateSourceCode(Connection conn) throws Exception
    {
        // Call the appropriate BO
        logger.info("Creating the SC BO");
        SourceCodeReindexBO bo = new SourceCodeReindexBO(conn);
        logger.info("Calling the SC BO");
        bo.updateClients();
        logger.info("Done SC BO");
    }

    /**
     * Execute an update of the Product text search.
     * @param conn
     * @throws Exception
     */
    private void updateProduct(Connection conn) throws Exception
    {
        logger.info("Creating the P BO");
        ProductReindexBO pbo = new ProductReindexBO(conn);
        logger.info("Calling the P BO");
        pbo.updateClients();
        logger.info("Done P BO");
    }

    /**
     * Perform a update of the IOTW text search.
     * @param conn
     * @throws Exception
     */
    private void updateIOTW(Connection conn) throws Exception
    {
        logger.info("Creating the IOTW BO");
        IOTWReindexBO ibo = new IOTWReindexBO(conn);
        logger.info("Calling the IOTW BO");
        ibo.updateClients();
        logger.info("Done IOTW BO");
    }

    /**
     * perform a update of the Facility text search.
     * @param conn
     * @throws Exception
     */
    private void updateFacility(Connection conn) throws Exception
    {
        logger.info("Creating the F BO");
        FacilityReindexBO fbo = new FacilityReindexBO(conn);
        logger.info("Calling the F BO");
        fbo.updateClients();
        logger.info("Done F BO");
    }

    public void ejbRemove()
    {
    }
    
    public void ejbCreate() {
    }
    
    
}
