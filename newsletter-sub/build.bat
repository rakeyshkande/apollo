@echo off

if not exist dist.com.cnf (
    @echo .
    @echo .
    @echo . Configuration is required
    @echo .
    @echo . Use "cnf [configuration name]" first
    @echo .
) else (
    ant -file build.xml
)
