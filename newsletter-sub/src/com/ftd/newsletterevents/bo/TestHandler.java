package com.ftd.newsletterevents.bo;

import com.ftd.newsletterevents.core.XmlService;
import com.ftd.newsletterevents.vo.INewsletterEvent;
import com.ftd.osp.utilities.plugins.Logger;


public class TestHandler implements IEventHandler {
    protected static Logger logger = new Logger(TestHandler.class.getName());

    /*
     * Empty constructors are required of all IEventHandlers
     */
    public TestHandler() {
    }

    public void processMessage(INewsletterEvent neVO) {
        // This is only a test.
        try {
            logger.info(
                "processMessage: Dynamic client successfully processed payload: " +
                XmlService.marshallToXmlString(INewsletterEvent.class, neVO));
        } catch (Exception e) {
            logger.error(e);
        }
    }
}
