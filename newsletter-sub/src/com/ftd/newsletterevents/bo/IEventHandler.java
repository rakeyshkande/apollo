package com.ftd.newsletterevents.bo;
import com.ftd.newsletterevents.vo.INewsletterEvent;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IEventHandler extends Remote {
    public void processMessage(INewsletterEvent neVO) throws RemoteException;
}
