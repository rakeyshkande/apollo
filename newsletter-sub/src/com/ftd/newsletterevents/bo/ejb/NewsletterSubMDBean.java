package com.ftd.newsletterevents.bo.ejb;

import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.naming.InitialContext;

import com.ftd.newsletterevents.bo.IEventHandler;
import com.ftd.newsletterevents.core.ErrorService;
import com.ftd.newsletterevents.core.XmlService;
import com.ftd.newsletterevents.exception.FtdNewsletterException;
import com.ftd.newsletterevents.vo.IMetadata;
import com.ftd.newsletterevents.vo.INewsletterEvent;
import com.ftd.osp.utilities.j2ee.InitialContextUtil;
import com.ftd.osp.utilities.plugins.Logger;


public class NewsletterSubMDBean implements MessageDrivenBean, MessageListener {
    private Logger logger = new Logger(com.ftd.newsletterevents.bo.ejb.NewsletterSubMDBean.class.getName());
    private MessageDrivenContext context;
    private static String _HANDLER_BIND_NAME;
    private IEventHandler handler;
    //private final static String PROPERTY_FILE = "newsletter_sub_config.xml";
    private InitialContext jndiContext;
    
    public void ejbCreate() {
    	

        try {
        	jndiContext = new InitialContext();

            try {
                // Initialize vars.
                _HANDLER_BIND_NAME = (String) jndiContext.lookup(
                        "java:comp/env/ieventhandler-bind-name");
                handler = (IEventHandler) jndiContext.lookup(_HANDLER_BIND_NAME);
            } finally {
                jndiContext.close();
            }
        } catch (Throwable e) {
            logger.error("ejbCreate: Failed to initialize JMS subscriber.", e);
        }
    }

    public void onMessage(Message msg) {
        logger.info("onMessage: message received.");

        try {

            // Only process message if it does not originate from a sender process
            // within the same app.
                if (msg instanceof TextMessage) {
                    logger.debug("onMessage: TEXT message received.");

                    TextMessage textMessage = (TextMessage) msg;

                    logger.debug("onMessage: JMSMessageID: " +
                        msg.getJMSMessageID());
                    logger.debug("onMessage: JMSTimestamp: " +
                        msg.getJMSTimestamp());
                    logger.debug("onMessage: JMSExpiration: " +
                        msg.getJMSExpiration());

                    //                logger.debug("onMessage: JMSDeliveryMode: " +
                    //                    msg.getJMSDeliveryMode());
                    //                logger.debug("onMessage: Persistent: " + DeliveryMode.PERSISTENT);
                    logger.debug("onMessage: JMSRedelivered: " +
                        msg.getJMSRedelivered());

                    if ((textMessage != null) &&
                            (textMessage.getText() != null)) {
                        try {
                            // Set the timestamp value for the IHandler use (if any).
                            Object vo = XmlService.unmarshall(textMessage.getText());

                            if (vo instanceof IMetadata) {
                                ((IMetadata) vo).setTimestamp(new Long(
                                        msg.getJMSTimestamp()).toString());
                                ((IMetadata) vo).setRedelivered(msg.getJMSRedelivered());
                            }

                            handler.processMessage((INewsletterEvent) vo);
                        } catch (Throwable e) {
                            logger.error("onMessage: Failed: " +
                                textMessage.getText(), e);

                            // Handle any application errors.
                            // A Runtime error would cause all transactional
                            // datasources to rollback; therefore the
                            // persistence operation below would be useless.
                            if (!(e instanceof RuntimeException)) { 
                            ErrorService.process(ErrorService.ERROR_TYPE_SUBSCRIBE_FAILURE,
                                e, textMessage.getText());
                            }
                        }
                    }
                } else {
                    throw new FtdNewsletterException(
                        "onMessage: Message class is unrecognized: " +
                        msg.getClass().getName());
                }
        } catch (Throwable e) {
            logger.error("onMessage: Failed.", e);

            // Handle the error.
            ErrorService.process(ErrorService.ERROR_TYPE_SUBSCRIBE_FAILURE, e);
        }
    }

    public void ejbRemove() {
    }

    public void setMessageDrivenContext(MessageDrivenContext ctx) {
        this.context = ctx;
    }

}
