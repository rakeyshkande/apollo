package com.ftd.oracleservlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ftd.osp.utilities.plugins.Logger;

/**
 * This class sets up a servlet that is autoloaded to start a subscription
 * to a topic.  It uses the real MDB to do the subscription.  This is the wrong way
 * to setup a subscription but in the current version of the application server of oracle
 * you cannot subscribe to the topic.
 * 
 * Some Oracle AQ debug code exists in this servlet.  It is commented out for non-development.
 */
public class DebugAQ extends HttpServlet {

   static boolean trace=true;
   static int  level=15;
   private final static String PROPERTY_FILE = "newsletter_sub_config.xml";
   private Logger logger = new Logger(com.ftd.oracleservlet.DebugAQ.class.getName());

   public void init(ServletConfig config) throws ServletException
   {
       super.init(config);
       //AQjmsOracleDebug.setTraceLevel(level);
       //AQjmsOracleDebug.setDebug(trace);
       
   }
   
   /** Since we're not using OC4J, this method doesn't actually do anything
    * 
    */
   public void service(HttpServletRequest req, HttpServletResponse res)
           throws ServletException,IOException
   {
      res.setContentType("text/html");
      PrintWriter out = res.getWriter();
      out.println("<html><body>");
      trace =!trace;
      level=trace?0:15;
//      AQjmsOracleDebug.setTraceLevel(level);
//      AQjmsOracleDebug.setDebug(trace);
      out.println("Debug has been "+(trace?"disabled":"enabled"));
      out.println("</body></html>");
   }
   
}

