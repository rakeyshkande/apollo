The jar files in this directory are needed for the ExactTarget Email implementation.

The following are NOT required for compilation, but must be deployed to JBoss server for runtime use:

  saaj-api-1.3.jar
  saaj-impl-1.3.3.jar
  wss4j-1.6.9.jar
  xmlschema-core-2.0.1.jar

The following is only needed for compliation of the JUnit test 
(src.com.ftd.message_generator.exacttarget.ExactTargetRawTests.java).  
It should NOT be deployed to JBoss.

  xmlsec-1.3.0.jar


NOTE: These jars were not put in the usual place (FTD/lib) since some of them cause conflicts when compiling other modules
(e.g., utilities uses FTD/lib/* as classpath but saaj-api-1.3.jar causes issues).
