package com.ftd.messagegenerator.test;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.vo.MessageToken;

import java.io.IOException;
import java.io.PrintWriter;

import javax.jms.JMSException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class MdbTestServlet extends HttpServlet 
{
  private static final String CONTENT_TYPE = "text/html; charset=windows-1252";

  public void init(ServletConfig config) throws ServletException
  {
    super.init(config);
  }

  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    response.setContentType(CONTENT_TYPE);
    try {
    dispatchEmailRequest();
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
    PrintWriter out = response.getWriter();
    out.println("<html>");
    out.println("<head><title>MdbTestServlet</title></head>");
    out.println("<body>");
    out.println("<p>The servlet has received a GET. This is the reply.</p>");
    out.println("</body></html>");
    out.close();
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    response.setContentType(CONTENT_TYPE);
    try {
    dispatchEmailRequest();
    }
    catch( Exception ex)
    {
      ex.printStackTrace();
    }
    PrintWriter out = response.getWriter();
    out.println("<html>");
    out.println("<head><title>MdbTestServlet</title></head>");
    out.println("<body>");
    out.println("<p>The servlet has received a POST. This is the reply.</p>");
    out.println("</body></html>");
    out.close();
  }
    /**
     * send message through jms
     * @param xmlRequest - E-mail XML request
     * @return n/a
     * @throws Exception
     * @todo: set status for message token.  need from DBA
     */
	public void dispatchEmailRequest() throws NamingException, JMSException,Exception
	{

		try {
			MessageToken token = new MessageToken();
			Context context = new InitialContext();
			token.setMessage("PROCESSBOUNCE"); // is this sufficient?
			token.setStatus("SENDMESSAGE");
			Dispatcher dispatcher = Dispatcher.getInstance();
			dispatcher.dispatchTextMessage(context, token);			
		} 
		finally {
		}
	}   
}