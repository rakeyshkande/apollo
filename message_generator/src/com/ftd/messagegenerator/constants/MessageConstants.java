package com.ftd.messagegenerator.constants;

import com.ftd.osp.utilities.plugins.Logger;

/**
 * Static information for queues
 */

public class MessageConstants
{
    private Logger logger = new Logger("com.ftd.messagegenerator.constants.MessageConstants");


    public static final String PROPERTY_FILE                = "message_generator_config.xml";
    public static final String DATASOURCE_NAME              = "DATASOURCE_NAME";
    public static final String SMTP_HOST_NAME               = "SMTP_HOST_NAME";
    public static final String SMTP_ALTERNATE_HOST_NAME     = "SMTP_ALTERNATE_HOST_NAME";
    public static final String SEND_CONTENT_TYPE            = "SEND_CONTENT_TYPE";
    public static final String TEXT_CONTENT_TYPE            = "TXT";
    public static final String DEFAULT_TOKEN_COMPANY        = "DEFAULT_TOKEN_COMPANY";
    public static final String TEST_EMAILS_ONLY             = "TEST_EMAILS_ONLY";
    public static final String EMAIL_MESSAGE_TYPE           = "Email";
    public static final String EMAIL_MESSAGE_TYPE_PHOENIX   = "Email_phoenix";
    public static final String MAILSERVER_CODE_ACXIOM       = "ACXIOM_WS";
    public static final String MESSAGE_GEN_CONFIG_CONTEXT   = "MESSAGE_GENERATOR_CONFIG";
    public static final String MAILSERVER_KEY                 = "MAILSERVER_KEY";
    public static final String MAILSERVER_TRANSFORMER_ENABLED = "MAILSERVER_TRANSFORMER_ENABLED";
    public static final String MAILSERVER_HOST_NAME_PREFIX    = "MAILSERVER_";
    public static final String WEB_SERVICE_ENABLED            = "WEB_SERVICE_ENABLED";
    public static final String DEFAULT_TEMPLATE_ID            = "DEFAULT_TEMPLATE_ID";
    public static final String EMAIL_PARTNER_ORDER            = "EMAIL_PARTNER_ORDER";
    public static final String SAVE_CONF_DETAIL_TO_POC		  = "SAVE_CONFIRMATION_DETAIL_TO_POC";
    
    public static final String LAST_BOUNCE_EMAIL_BATCH_TIME = "LAST_BOUNCE_EMAIL_BATCH_TIME";
    public static final String BOUNCE_SENDER_EMAIL_ADDRESS = "BOUNCE_SENDER_EMAIL_ADDRESS";
    
    
    // ExactTarget Web Service Global Parms and Secure Config 
    //
    public static final String EXACT_TARGET_WS_CONTEXT       = "SERVICE";
    public static final String EXACT_TARGET_WS_USERNAME      = "EXACT_TARGET_WS_USERNAME";
    public static final String EXACT_TARGET_WS_PASSWORD      = "EXACT_TARGET_WS_PASSWORD";
    public static final String EXACT_TARGET_WS_URL           = "EXACT_TARGET_WS_URL";
    public static final String EXACT_TARGET_PROTOCOL         = "EXACT_TARGET_PROTOCOL";
    public static final String EXACT_TARGET_CIPHER           = "EXACT_TARGET_CIPHER";

    // Phoenix Global Parms
    //
    public static final String PHOENIX_TEMPLATE_ID      = "TEMPLATE_ID_PHOENIX";
    public static final String PHOENIX_IMAGE_PROD_URL   = "PHOENIX_IMAGE_PROD_URL";
    public static final String PHOENIX_IMAGE_ADDON_URL  = "PHOENIX_IMAGE_ADDON_URL";
    public static final String PHOENIX_EMAIL_TYPE      = "PHOENIX";

    // Types of Phoenix emails (these match email_type_key from ftd_apps.phoenix_email_types)
    //
    public static final String PHOENIX_EMAIL_APOLOGY     = "apology_email"; 
    public static final String PHOENIX_EMAIL_STANDARD    = "phoenix_email"; 
    
    // Template names for Phoenix emails (these match ftd_apps.content_detail values and
    // are also the values sent to ExactTarget)
    //
    public static final String PHOENIX_TEMPLATE_APOLOGY     = "TMPL_APOLOGY"; 
    public static final String PHOENIX_TEMPLATE_DEFAULT     = "TMPL_PHOENIX_DEFAULT";  
    public static final String PHOENIX_TEMPLATE_DATE_CHANGE = "TMPL_PHOENIX_NEWDATE"; 
    
    // Phoenix Content (these match ftd_apps.content_master values)
    //
    public static final String PHOENIX_CONTEXT = "PHOENIX_EMAIL"; 
    public static final String PHOENIX_FOOTER_BAR_TEXT = "FOOTER_BAR_TEXT";
    public static final String PHOENIX_FOOTER_END_TEXT = "FOOTER_END_TEXT";
    public static final String PHOENIX_COMMENT_TEXT_TEMPLATE = "COMMENT_TEXT_TEMPLATE";
    public static final String PHOENIX_EMAIL_SUBJECT_TEMPLATE = "EMAIL_SUBJECT_TEMPLATE";
    
    // Phoenix Content Tokens 
    //
    public static final String PHOENIX_TOKEN_YEAR    = "~year~";   // ...tokens in ftd_apps.content_detail
    public static final String PHOENIX_TOKEN_ORDER   = "~ordernumber~"; 
    public static final String PHOENIX_TOKEN_EMAIL   = "~customeremail~"; 
    
    public static final String PHOENIX_TOKEN_PROD_ID = "~prodId~";  // ...tokens in global_parms
    public static final String PHOENIX_TOKEN_ADDON   = "~addon~"; 
    
    public static final String PHOENIX_TOKEN_BEAR_ADDON = "B";     // Tokens replacement values for ~addon~
    public static final String PHOENIX_TOKEN_CHOC_ADDON = "C";

    
    /*********************************************************************************************
    // Security parameters
    *********************************************************************************************/
    public static final String CONTEXT    = "context";
    public static final String SEC_TOKEN  = "securitytoken";



    /*********************************************************************************************
    //custom parameters returned from database procedure
    *********************************************************************************************/
    public static final String STATUS_PARAM = "OUT_STATUS";
    public static final String MESSAGE_PARAM = "OUT_MESSAGE";
    
    public static final String SET_SOCKET_PARAMS = "SET_SOCKET_PARAMS";
    public static final String LOG_SOAP_MESSAGE = "LOG_SOAP_MESSAGE";


    // Ship confirmation Email Type
    public static final String SHIP_CONFIRMATION_EMAIL_TYPE       = "SHIP_CONFIRMATION";
    // Queue Delete Email Type
    public static String COM_EMAIL_TYPE = "CUSTOMER_SERVICE";
    
    /**
     * constructor
     * @param none
     * @return n/a
     * @throws none
     */
    public MessageConstants()
    {
        super();
    }
}