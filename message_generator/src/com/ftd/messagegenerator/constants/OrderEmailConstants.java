package com.ftd.messagegenerator.constants;

public class OrderEmailConstants
{
  public static final int DEFAULT_TOKENIZER = 255;
  public static final String CREF           = "cref";

  /*********************************************************************************************
  //custom parameters returned from database procedure
  *********************************************************************************************/
  public static final String STATUS_PARAM = "OUT_STATUS";
  public static final String MESSAGE_PARAM = "OUT_MESSAGE";

  /*********************************************************************************************
  //default user ids for the order email process
  *********************************************************************************************/
  public static final String CONFIRMATION_USER    = "ORDER_EMAIL_CONFIRMATION";
  public static final String REINSTATE_USER       = "ORDER_EMAIL_REINSTATE";
  public static final String REMOVE_PENDING_USER  = "ORDER_EMAIL_REMOVE_PENDING";

  /*********************************************************************************************
  //webloyalty URL constants that will be retrieve from Global parms
  *********************************************************************************************/
  public static final String CONTEXT_WEBLOYALTY				          = "WEBLOYALTY";
  public static final String NAME_EMAIL_SLOGAN_1	              = "email_slogan_1";
  public static final String NAME_EMAIL_SLOGAN_2	              = "email_slogan_2";
  public static final String NAME_OFFER_POOL_ID_DATA_ORDER_CONF	= "offer_pool_id_data_order_conf";
  public static final String NAME_OFFER_POOL_ID_LITERAL	        = "offer_pool_id_literal";
  public static final String NAME_SERVER							          = "server";
  public static final String NAME_OFFER_SRC_ID_DATA_ORDER_CONF	= "offer_src_id_data_order_conf";
  public static final String NAME_OFFER_SRC_ID_LITERAL          = "offer_src_id_literal";
  public static final String NAME_URL_PARAMETER_1	              = "url_parameter_1";
  public static final String NAME_URL_PARAMETER_2	              = "url_parameter_2";
  public static final String NAME_VENDOR_ID_LITERAL	            = "vendor_id_literal";
  public static final String NAME_VENDOR_ID_DATA	              = "vendor_id_data";


  /*********************************************************************************************
  //HashMap constants for webloyalty global parms
  *********************************************************************************************/
  public static final String HK_EMAIL_SLOGAN_1	                = OrderEmailConstants.CONTEXT_WEBLOYALTY + OrderEmailConstants.NAME_EMAIL_SLOGAN_1;
  public static final String HK_EMAIL_SLOGAN_2	                = OrderEmailConstants.CONTEXT_WEBLOYALTY + OrderEmailConstants.NAME_EMAIL_SLOGAN_2;
  public static final String HK_OFFER_POOL_ID_DATA_ORDER_CONF	  = OrderEmailConstants.CONTEXT_WEBLOYALTY + OrderEmailConstants.NAME_OFFER_POOL_ID_DATA_ORDER_CONF;
  public static final String HK_OFFER_POOL_ID_LITERAL	          = OrderEmailConstants.CONTEXT_WEBLOYALTY + OrderEmailConstants.NAME_OFFER_POOL_ID_LITERAL;
  public static final String HK_OFFER_SRC_ID_DATA_ORDER_CONF    = OrderEmailConstants.CONTEXT_WEBLOYALTY + OrderEmailConstants.NAME_OFFER_SRC_ID_DATA_ORDER_CONF;
  public static final String HK_OFFER_SRC_ID_LITERAL 	          = OrderEmailConstants.CONTEXT_WEBLOYALTY + OrderEmailConstants.NAME_OFFER_SRC_ID_LITERAL;
  public static final String HK_SERVER 				                  = OrderEmailConstants.CONTEXT_WEBLOYALTY + OrderEmailConstants.NAME_SERVER;
  public static final String HK_URL_PARAMETER_1	                = OrderEmailConstants.CONTEXT_WEBLOYALTY + OrderEmailConstants.NAME_URL_PARAMETER_1;
  public static final String HK_URL_PARAMETER_2	                = OrderEmailConstants.CONTEXT_WEBLOYALTY + OrderEmailConstants.NAME_URL_PARAMETER_2;
  public static final String HK_VENDOR_ID_LITERAL	              = OrderEmailConstants.CONTEXT_WEBLOYALTY + OrderEmailConstants.NAME_VENDOR_ID_LITERAL;
  public static final String HK_VENDOR_ID_DATA	                = OrderEmailConstants.CONTEXT_WEBLOYALTY + OrderEmailConstants.NAME_VENDOR_ID_DATA;

  /*********************************************************************************************
  //HTML Constants to build the spans surrounding the content parts
  *********************************************************************************************/
  public static final String SPAN_HEADER        = "<span id=\"header\">";
  public static final String SPAN_BIN_CHANGE    = "<span id=\"bin-change\">";
  public static final String SPAN_LOYALTY_1     = "<span id=\"loyalty-1\">";
  public static final String SPAN_LOYALTY_2     = "<span id=\"loyalty-2\">";
  public static final String SPAN_ORDER_DETAIL  = "<span id=\"order_detail\">";
  public static final String SPAN_GUARANTEE     = "<span id=\"guarantee\">";
  public static final String SPAN_MARKETING     = "<span id=\"marketing\">";
  public static final String SPAN_CONTACT       = "<span id=\"contact\">";
  public static final String SPAN_END           = "</span>";  
  public static final String LABEL_GOLD_MEMBERSHIP_CART_INDICATOR    = "<label id=\"gold_membership_cart_indicator\" style=\"visibility:hidden\">";
  public static final String LABEL_END           = "</label>"; 
  
  public static final String TAG_HEADER_CONTENT = "HeaderContent";
  public static final String TAG_GUARANTEE_CONTENT = "GuaranteeContent";
  public static final String TAG_MARKETING_CONTENT = "MarketingContent";
  public static final String TAG_CONTACT_CONTENT = "ContactContent";
  public static final String TAG_ORDER_DETAILS = "OrderDetails";
  public static final String TAG_BIN_CHANGE_CONTENT = "BinChangeContent";
  
  /**
   * constructor
   * @param none
   * @return n/a
   * @throws none
   */
  public OrderEmailConstants()
  {
  }
}
