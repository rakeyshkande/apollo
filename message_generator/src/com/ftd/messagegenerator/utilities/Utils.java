package com.ftd.messagegenerator.utilities;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

import com.ftd.osp.utilities.plugins.Logger;

public class Utils {

	private static Logger logger = new Logger("com.ftd.messagegenerator.utilities.Utils");
	
	
	public static String convertDate(String inDate, int type) throws Exception
	{
		String outDate = null;
		String inDateFormat = "";
		if(type == 1)
			inDateFormat = "EEE MMM dd HH:mm:ss zzz yyyy";
		if(type == 2)
			inDateFormat = "MMM dd yyyy HH:mma";
		if(type == 3)
			inDateFormat = "MM/dd/yyyy";

		try
		{
			String outDateFormat = "EEEE MM/dd/yyyy";
			SimpleDateFormat sdfInput = new SimpleDateFormat(inDateFormat);
			SimpleDateFormat sdfOut = new SimpleDateFormat(outDateFormat);
			java.util.Date date = sdfInput.parse(inDate);
			outDate = sdfOut.format(date);
		}
		catch(Exception ex)
		{
			outDate = inDate;
		}

		return outDate;
	}
	
	public static Calendar convertStringToDate(String inDate, String dateFormat) throws Exception
	{
		Calendar cal = null;
		try
		{
			SimpleDateFormat sdfOut = new SimpleDateFormat(dateFormat);
			java.util.Date date = sdfOut.parse(inDate);
			cal = new GregorianCalendar();
			cal.setTime(date);
		}
		catch(Exception ex)
		{
			logger.info("Exception in convertStringToDate");
		}

		return cal;
	}

	public static String fixZipCode(String inZip)
	{
		String outZip = "";

		if(inZip.length() <= 5)
			return inZip;

		if(inZip.indexOf("-") >= 0)
			return inZip;

		try
		{
			String first = inZip.substring(0, 5);
			String second = inZip.substring(5, inZip.length());
			outZip = first + "-" + second;
		}
		catch(Exception e)
		{
			outZip = inZip;
		}

		return outZip;
	}

	public static ArrayList splitAddressLine(String inAddress)
	{
		ArrayList outAddress = new ArrayList();
		if(inAddress == null || inAddress.length() <= 30)
		{
			outAddress.add(inAddress);
			return outAddress;
		}
		else
		{
			try
			{
				String one = inAddress.substring(0, 30);
				String remain = inAddress.substring(30, inAddress.length());

				for(int i = one.length()-1; i >= 0; i--)
				{
					if(one.charAt(i) == ' ')
					{
						break;
					}
					else
					{
						remain = one.charAt(i) + remain;
						one = one.substring(0, one.length() - 1);
					}
				}
				outAddress.add(one);
				outAddress.add(remain);
			}
			catch(Exception e)
			{
				e.printStackTrace();
				outAddress.clear();
				outAddress.add(inAddress);
			}
		}

		return outAddress;
	}

	public static String rightAlign(String s)
	{
		if(s.length() >= 9)
			return s;

		int len = s.length();
		int spaces = 9 - len;
		String ret = (createSpaces(spaces) + s );
		return ret;
	}

	public static String truncDescription(String description)
	{
		if(description.length() > 25)
			description = description.substring(0, 25);
		else
			if(description.length() < 25)
				description = description + createSpaces(25 - description.length());

		return description;
	}

	/**
	 * Description: Takes in a string Date, checks for valid formatting
	 * 				in the form of mm/dd/yyyy and converts it
	 * 				to a SQL date of yyyy-mm-dd.
	 *
	 * @param String date in string format
	 * @return java.sql.Date
	 *
	 * Note..copied from OE project
	 */
	public static java.sql.Date getSQLDate(String strDate) throws Exception
	{
		java.sql.Date sqlDate = null;
		String inDateFormat = "";
		int dateLength = 0;
		int firstSep = 0;
		int lastSep = 0;

		try{
			if ((strDate != null) && (!strDate.trim().equals(""))) {

				// set input date format
				dateLength = strDate.length();
				if ( dateLength > 10) {
					firstSep = strDate.indexOf("/");
					if(firstSep == 1)
					{
						inDateFormat = "M/dd/yyyy hh:mm:ss";
					}
					else if (firstSep == 2)
					{
						inDateFormat = "MM/dd/yyyy hh:mm:ss";
					}
					else
					{
						inDateFormat = "yyyy-MM-dd hh:mm:ss";
					}

				} else {
					firstSep = strDate.indexOf("/");
					lastSep = strDate.lastIndexOf("/");

					switch ( dateLength ) {
					case 10:
						inDateFormat = "MM/dd/yyyy";
						break;
					case 9:
						if ( firstSep == 1 ) {
							inDateFormat = "M/dd/yyyy";
						} else {
							inDateFormat = "MM/d/yyyy";
						}
						break;
					case 8:
						if ( firstSep == 1 ) {
							inDateFormat = "M/d/yyyy";
						} else {
							inDateFormat = "MM/dd/yy";
						}
						break;
					case 7:
						if ( firstSep == 1 ) {
							inDateFormat = "M/dd/yy";
						} else {
							inDateFormat = "MM/d/yy";
						}
						break;
					case 6:
						inDateFormat = "M/d/yy";
						break;
					default:
						break;
					}
				}
				//SimpleDateFormat sdfInput = new SimpleDateFormat( "MM/dd/yyyy" );
				SimpleDateFormat sdfInput = new SimpleDateFormat(inDateFormat);
				SimpleDateFormat sdfOutput = new SimpleDateFormat ( "yyyy-MM-dd" );

				java.util.Date date = sdfInput.parse( strDate );
				String outDateString = sdfOutput.format( date );

				// now that we have no errors, use the string to make a SQL date
				sqlDate = sqlDate.valueOf(outDateString);
			}
		}
		catch(Exception ex)
		{
			throw(ex);
		}
		return sqlDate;
	}

	
	public static String formatDollars(double amt)
	{
		return formatDollars(Double.toString(amt));
	}

	public static String formatDollars(String amt)
	{
		String ret = "";
		if(amt != null || amt.length() > 0)
		{
			try{
				NumberFormat dollarFormat = NumberFormat.getCurrencyInstance(Locale.US);
				double value = Double.parseDouble(amt);
				ret = dollarFormat.format(value);
			}
			catch(Throwable t)
			{
				//return unformatted value
				ret = amt;
			}//end catch
		}//end amt null

		return ret;
	}

	public static String end()
	{
		return "\n";
	}

	public static String line()
	{
		return "\n-------------------------------------------------------------------------------\n";
	}

	public static String line(int count)
	{
		if(count < 0)
		{
			return "";
		}

		StringBuffer buf = new StringBuffer("");
		for(int i = 0; i < count; i++)
		{
			buf.append("-");
		}
		return buf.toString();
	}

	public static String justifyToLine(String start, String end )
	{
		StringBuffer buf = new StringBuffer("");
		buf.append(start);
		buf.append(createSpaces(56-(start.length() + end.length())));
		buf.append(end);

		return buf.toString();
	}

	public static String addSpacesToLine(String s)
	{
		int charsToLine = 28;
		int spacessToAdd = charsToLine - s.length();
		if(spacessToAdd < 1)
			return s;
		s = (s + createSpaces(spacessToAdd));
		return s;
	}

	public static String createSpaces(int count)
	{
		if(count < 0)
		{
			return "";
		}

		StringBuffer buf = new StringBuffer("");
		for(int i = 0; i < count; i++)
		{
			buf.append(" ");
		}
		return buf.toString();
	}
	
	public static String translateAddressType(String inType)
	{
		if(inType == null || inType.equals(""))
		{
			return "Residence";
		}
		else
			if(inType.equalsIgnoreCase("H") || inType.equalsIgnoreCase("HOSPITAL"))
			{
				return "Hospital";
			}
			else
				if(inType.equalsIgnoreCase("F") || inType.toUpperCase().startsWith("FUNERAL"))
				{
					return "Funeral";
				}
				else
					if(inType.equalsIgnoreCase("B") || inType.equalsIgnoreCase("BUSINESS") || inType.toUpperCase().startsWith("BUS"))
					{
						return "Business";
					}
					else
						if(inType.equalsIgnoreCase("C") || inType.toUpperCase().startsWith("CEMETERY"))
						{
							return "Cemetery";
						}
						else
							if(inType.equalsIgnoreCase("N") || inType.toUpperCase().startsWith("NURSING"))
							{
								return "Nursing Home";
							}
							else
							{
								return "Residence";
							}
	}

	public static String translateCountryType(String inType)
	{
		if(inType == null || inType.equals(""))
			return " ";
		else
			if(inType.equalsIgnoreCase("US"))
				return "USA";
			else
				if(inType.equalsIgnoreCase("CA"))
					return "CANADA";
				else
					if(inType.equalsIgnoreCase("UK"))
						return "GREAT BRITAIN";
					else
						if(inType.equalsIgnoreCase("FR"))
							return "FRANCE";
						else
							if(inType.equalsIgnoreCase("DE"))
								return "GERMANY";
							else
								if(inType.equalsIgnoreCase("AU"))
									return "AUSTRALIA";
								else
									return inType;

	}

	public static String translateShippingMethod(String s)
	{
		if(s == null || s.equals(""))
		{
			return "";
		}
		else
			if(s.equalsIgnoreCase("SA"))
			{
				return "Saturday Delivery";
			}
			else
				if(s.equalsIgnoreCase("SD"))
				{
					return "Saturday Delivery";
				}
				else
					if(s.equalsIgnoreCase("ND"))
					{
						return "Next Day Delivery";
					}
					else
						if(s.equalsIgnoreCase("2F"))
						{
							return "Two Day Delivery";
						}
						else
							if(s.equalsIgnoreCase("GR"))
							{
								return "Standard Delivery";
							}
							else
							{
								return s;
							}
	}
	
	public static  boolean  isPriceZero(String price)
	{
		boolean isZero = true;

		if(price != null && price.length() > 0)
		{
			int intPrice = new Double(price).intValue();
			if(intPrice > 0)
			{
				isZero = false;
			}
		}

		return isZero;
	}
	
	public static String formatNumber(double amt) {
		return formatNumber(Double.toString(amt));
	}

	public static String formatNumber(String amt) {
		String ret = "";
		if (amt != null) {
			try {
				DecimalFormat df = new DecimalFormat( "#,###,##0.00" );
				double value = Double.parseDouble(amt);
				ret = df.format(value);
			}
			catch(Throwable t) {
				//return unformatted value
				ret = amt;
			}
		}

		return ret;
	}

}
