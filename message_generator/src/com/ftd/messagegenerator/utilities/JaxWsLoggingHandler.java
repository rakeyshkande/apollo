package com.ftd.messagegenerator.utilities;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Set;
 


import javax.xml.namespace.QName;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import com.ftd.osp.utilities.plugins.Logger;
 
public class JaxWsLoggingHandler implements SOAPHandler<SOAPMessageContext> {
	 private static Logger logger = new Logger("com.ftd.messagegenerator.utilities.JaxWsLoggingHandler"); 
	
    @Override
    public void close(MessageContext arg0) {
    }
 
    @Override
    public boolean handleFault(SOAPMessageContext arg0) {
        SOAPMessage message = arg0.getMessage();
        try {
            message.writeTo(System.out);
        } catch (SOAPException e) {
            logger.debug("***Exception while trying to log the SOAP message", e);
        } catch (IOException e) {
        	logger.debug("Exception while trying to log the SOAP message");
        } catch (Exception e) {
        	logger.debug("Exception while trying to log the SOAP message", e);
        }
        return true;
    }
 
    @Override
    public boolean handleMessage(SOAPMessageContext arg0) {
        SOAPMessage message = arg0.getMessage();
        boolean isOutboundMessage = (Boolean) arg0.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
        if (isOutboundMessage) {
        	logger.info("OUTBOUND MESSAGE \n");
 
        } else {
        	logger.info("INBOUND MESSAGE \n");
        }
        try {
            //message.writeTo(System.out);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            message.writeTo(out);
            String strMsg = new String(out.toByteArray());
            
            logger.info("SOAPMessage :"+strMsg);
            
        } catch (SOAPException e) {
        	logger.debug("Exception while trying to log the SOAP message ", e);
        } catch (IOException e) {
        	logger.debug("Exception while trying to log the SOAP message", e);
        } catch (Exception e) {
        	logger.debug("Exception while trying to log the SOAP message", e);
        }
        return true;
    }
 
    @Override
    public Set<QName> getHeaders() {
        return null;
    }
 
}