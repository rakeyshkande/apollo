package com.ftd.messagegenerator;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import javax.naming.Context;
import javax.naming.InitialContext;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.Text;

import com.ftd.messagegenerator.cache.handler.StockMessageTokenHandler;
import com.ftd.messagegenerator.constants.BuildEmailConstants;
import com.ftd.messagegenerator.constants.MessageConstants;
import com.ftd.messagegenerator.dao.MessageGeneratorDAO;
import com.ftd.messagegenerator.vo.PointOfContactVO;
import com.ftd.messagegenerator.vo.StockMessageVO;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.PartnerUtility;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.MessageTokenVO;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.order.MercentOrderPrefixes;
import com.ftd.osp.utilities.order.ScrubMapperDAO;
import com.ftd.osp.utilities.order.vo.BuyerAddressesVO;
import com.ftd.osp.utilities.order.vo.BuyerPhonesVO;
import com.ftd.osp.utilities.order.vo.BuyerVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.vo.PartnerMappingVO;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.osp.utilities.xml.TraxUtil;


/**
 *
 * StockMessageGenerator utility to build, insert and send a message
 *
 * @author Srinivas Makam
 *
 */
public class StockMessageGenerator
{
  private static Logger logger = new Logger("com.ftd.messagegenerator.StockMessageGenerator");

  private Connection classConnection = null;

  private final static String EXTERNAL_ORDER_NUMBER_NODE = "//external_order_number/text()";
  private static final String DEFAULT_TOKEN_COMPANY = "FTD";
  private static final String TOKEN_DELIMETER = "~";

  //Determines if connection should be closed after use.
  //The connection should be closed if it was passed to this object.
  private boolean closeConnection = true;

/**
 * constructor
 *
 **/
  public StockMessageGenerator() throws Exception
  {
    super();
  }

    public StockMessageGenerator(Connection dbConnection) throws Exception
    {
      super();
      this.classConnection = dbConnection;
      this.closeConnection = false;
    }




  /**
   * buildMessage() This method takes in POC object and builds a message to send using the tokens
   * from the tokenHanlder by mnatching the token in the message template read from the db.
   *
   * @param connection
   * @param PointOfContactVO
   * @return PointOfContactVO
   * @throws java.lang.Exception
   */
    public PointOfContactVO buildMessage(PointOfContactVO pocObj) throws Exception
    {
        boolean attachedToOrder = true;
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();

        StockMessageTokenHandler stockMessageTokenHandler = (StockMessageTokenHandler) CacheManager.getInstance().getHandler("CACHE_NAME_STOCK_MESSAGE_TOKENS");

        if(stockMessageTokenHandler==null)
        {
            logger.error("StockMessageTokenHandler object not found by the CacheManager");
            throw new Exception("StockMessageTokenHandler object not found by the CacheManager");
        }

        ArrayList tokenList = null;
        if(pocObj.getOriginId() != null && pocObj.getOriginId().equalsIgnoreCase("TARGI"))
        {
            //do nothing
        }
        else if(pocObj.getOriginId() != null)
        {
            // Pull token list based on origin if an origin is specified
            tokenList = stockMessageTokenHandler.getTokensByCompany(pocObj.getOriginId());
        }
        else
        {
            if(pocObj.getCompanyId() != null && !pocObj.getCompanyId().equals(""))
            {
                // Pull token list by company
                tokenList = stockMessageTokenHandler.getTokensByCompany(pocObj.getCompanyId());
            }
            else
            {
                // Update attached flag on order to false
                attachedToOrder = false;
                // Pull token list by default company
                tokenList = stockMessageTokenHandler.getTokensByCompany(DEFAULT_TOKEN_COMPANY);
            }
        }

        HashMap replacementMap = new HashMap();
        Connection connection = null;

        try
        {
            //if class contains a connection use it, otherwise obtain one
            if(this.classConnection != null)
            {
                connection = this.classConnection;
            }
            else
            {
                connection =
                DataSourceUtil.getInstance().getConnection(ConfigurationUtil.getInstance().getProperty(
                        MessageConstants.PROPERTY_FILE, MessageConstants.DATASOURCE_NAME));
                logger.debug("Creating connection. (buildMessage)");
            }

            if(tokenList!=null)
            {
                MessageTokenVO messageToken = null;
                Text xmlText = null;

                for(int i=0; i<tokenList.size(); i++)
                {
                    if(attachedToOrder)
                    {
                        messageToken = (MessageTokenVO) tokenList.get(i);

                        if(messageToken.getTokenType().equals("value"))
                        {

                            xmlText = (Text) DOMUtil.selectSingleNode(pocObj.getDataDocument(), messageToken.getTokenValue());
                            if(xmlText==null)
                            {
                                logger.debug("XMLtext for " + messageToken.getTokenValue() + " has null value in the POC data document");
                            }

                            replacementMap.put(messageToken.getTokenId(), getTextValue(xmlText, messageToken.getDefaultValue()));
                        }
                        else if(messageToken.getTokenType().equals("text"))
                        {
                            replacementMap.put(messageToken.getTokenId(), messageToken.getTokenValue());
                        }
                        else if(messageToken.getTokenType().equals("function"))
                        {
                            replacementMap.put(messageToken.getTokenId(), TraxUtil.getInstance().transform((Document) pocObj.getDataDocument(), messageToken.getTokenValue(), null).trim());
                        }
                        else if(messageToken.getTokenType().equals("content"))
                        {
                            if (messageToken.getTokenValue() != null) {
                                String contentValue = contentLookup(connection, pocObj, messageToken);  
                                if (contentValue != null) {
                                    replacementMap.put(messageToken.getTokenId(), contentValue);
                                    logger.debug(messageToken.getTokenId() + " contentValue: " + contentValue);                                  
                                }
                            }
                        }
                    }
                    else
                    {
                        messageToken = (MessageTokenVO) tokenList.get(i);
                        replacementMap.put(messageToken.getTokenId(), null);
                    }
                }
            }

            MessageGeneratorDAO messageAccess = new MessageGeneratorDAO(connection);
            StockMessageVO messageObj = null;

            logger.info("pointOfContactType: " + pocObj.getPointOfContactType());
            if(pocObj.getTemplateId() != null && !pocObj.getTemplateId().equals(""))
            {
                logger.info("Loading by template id:" + pocObj.getTemplateId());
                messageObj = messageAccess.loadMessageTemplate(pocObj.getPointOfContactType(), pocObj.getCompanyId(), pocObj.getTemplateId());
            }
            else  if(pocObj.getOriginId() != null && pocObj.getOriginId().equalsIgnoreCase("TARGI"))
            {
                logger.info("Loading by origin/title:" + pocObj.getLetterTitle());
                messageObj = messageAccess.loadMessageTemplateByTitle(pocObj.getPointOfContactType(), pocObj.getLetterTitle(), null, null);
            }
            else if(pocObj.getLetterTitle() != null && !pocObj.getLetterTitle().equals(""))
            {
                logger.info("Loading by title:" + pocObj.getLetterTitle());
                messageObj = messageAccess.loadMessageTemplateByTitle(pocObj.getPointOfContactType(), pocObj.getLetterTitle(), null, null);
            }
            else if(pocObj.getBody() != null && !pocObj.getBody().equals("") &&
            pocObj.getEmailSubject() != null && !pocObj.getEmailSubject().equals(""))
            {
                logger.info("Loading by message body:" + pocObj.getBody());
                messageObj = new StockMessageVO();
                messageObj.setContent(pocObj.getBody());
                messageObj.setSubject(pocObj.getEmailSubject());
            }

            if(messageObj!=null)
            {
                pocObj.setBody(replaceTokens(messageObj.getContent(), replacementMap));
                if(pocObj.getPointOfContactType().equalsIgnoreCase(MessageConstants.EMAIL_MESSAGE_TYPE))
                {
                    pocObj.setEmailSubject(replaceTokens(messageObj.getSubject(), replacementMap));

                    //for non-Target orders if an external order number exists in the passed in XML, then include it on subject
                    //Issue 3864 - Applications such as DCON sends a blank origin id for all but WLMTI, AMZNI, TARGI.  Thus,
                    //if the origin is null, put the order # in the subject
                    if( pocObj.getOriginId() == null ||
                        pocObj.getOriginId().equalsIgnoreCase("") ||
                        ( pocObj.getOriginId() != null &&
                          !pocObj.getOriginId().equalsIgnoreCase("TARGI")
                        )
                      )
                    {
                        /*Text xmlText = (Text) DOMUtil.selectSingleNode(pocObj.getDataDocument(), EXTERNAL_ORDER_NUMBER_NODE);
                        if(xmlText != null)
                        {
                            String updatedSubject = pocObj.getEmailSubject() + " (Order Number:" + xmlText.getNodeValue() + ")";;
                            pocObj.setEmailSubject(updatedSubject);
                        }*/
                        String ordNumber = pocObj.getExternalOrderNumber();

                        // There are certain companies (e.g., ProFlowers) we want to send partner order number instead of external_order_number.
                        // 
                        String partnerCompany = configUtil.getFrpGlobalParm(MessageConstants.MESSAGE_GEN_CONFIG_CONTEXT, MessageConstants.EMAIL_PARTNER_ORDER);
                        // Add spaces to ensure no sub-string matches
                        partnerCompany = " " + partnerCompany + " ";  
                        if (pocObj.getCompanyId() != null && partnerCompany.indexOf(" " + pocObj.getCompanyId().trim() + " ") > -1) {
                            CachedResultSet result = new PartnerUtility().getPtnOrderDetailByConfNumber(pocObj.getExternalOrderNumber(), null, connection);
                            if (result != null && result.next()) {
                                if (!StringUtils.isEmpty(result.getString("PARTNER_ORDER_ITEM_NUMBER"))) {
                                    ordNumber = result.getString("PARTNER_ORDER_ITEM_NUMBER");
                                    if (logger.isDebugEnabled()) {
                                        logger.debug("Using partner order number (" +  ordNumber + ") in subject instead of " + pocObj.getExternalOrderNumber() );
                                    }
                                }
                            }                            
                        }
                    	
                    	StringBuffer emailSubj = new StringBuffer(pocObj.getEmailSubject());
                    	if(ordNumber != null && ordNumber.length() > 0) {
                    		emailSubj.append(" (Order Number:").append(ordNumber).append(")");
                    	} else {
                    		Text xmlText = (Text) DOMUtil.selectSingleNode(pocObj.getDataDocument(), EXTERNAL_ORDER_NUMBER_NODE);
                    		if(xmlText != null)  {
                    			emailSubj.append(" (Order Number:").append(xmlText.getNodeValue()).append(")");
                    		}
                    	}
                        pocObj.setEmailSubject(emailSubj.toString());  
                    	
                    }

                }
            }
            else
            {
                logger.error("Failed to load message template from db. poc_type="+pocObj.getPointOfContactType()+
                "company_id="+pocObj.getCompanyId()+" template_id="+pocObj.getTemplateId());
                throw new Exception("Failed to load message template from db");
            }

            return pocObj;
        }
        catch (Throwable ex)
        {
            logger.error(ex);
            throw new Exception(ex.getMessage());
        }
        finally
        {
            if(this.closeConnection && connection!=null)
                connection.close();
        }
    }

    

    /**
     * buildMessageSegment() This method takes in POC object and builds a message segment using 
     * token values from tokenHandler and replacing their corresponding placeholders in passed string.
     * 
     * This is a stripped-down version of buildMessage() but instead of building from a stock email/letter template,
     * it builds from the passed string.  This method was added for the Phoenix project since it needed
     * FTD company data (e.g., phone,address,url) from clean.message_tokens (and associated ftd_apps.content_detail)
     * but was not associated with any stock emails/letters.
     * Ideally this replacement logic should have been made into it's own generic class, but time did not permit.
     * 
     * @param pocObj
     * @param bodySegment
     * @return
     * @throws Exception
     */
      public String buildMessageSegmentByCompany(PointOfContactVO pocObj, String bodySegment) throws Exception
      {
          StockMessageTokenHandler stockMessageTokenHandler = (StockMessageTokenHandler) CacheManager.getInstance().getHandler("CACHE_NAME_STOCK_MESSAGE_TOKENS");
          if(stockMessageTokenHandler==null)
          {
              logger.error("StockMessageTokenHandler object not found by the CacheManager");
              throw new Exception("StockMessageTokenHandler object not found by the CacheManager");
          }

          ArrayList tokenList = null;
          if(pocObj.getCompanyId() != null && !pocObj.getCompanyId().equals(""))
          {
              // Pull token list by company
              tokenList = stockMessageTokenHandler.getTokensByCompany(pocObj.getCompanyId());
          }
          else
          {
              // Pull token list by default company
              tokenList = stockMessageTokenHandler.getTokensByCompany(DEFAULT_TOKEN_COMPANY);
          }

          HashMap replacementMap = new HashMap();
          Connection connection = null;

          try
          {
              //if class contains a connection use it, otherwise obtain one
              if(this.classConnection != null)
              {
                  connection = this.classConnection;
              }
              else
              {
                  connection =
                  DataSourceUtil.getInstance().getConnection(ConfigurationUtil.getInstance().getProperty(
                          MessageConstants.PROPERTY_FILE, MessageConstants.DATASOURCE_NAME));
                  logger.debug("Creating connection. (buildMessage)");
              }

              if(tokenList!=null)
              {
                  MessageTokenVO messageToken = null;
                  Text xmlText = null;

                  for(int i=0; i<tokenList.size(); i++)
                  {
                      messageToken = (MessageTokenVO) tokenList.get(i);
                      if (messageToken.getTokenType().equals("value") && pocObj.getDataDocument() != null)
                      {
                          xmlText = (Text) DOMUtil.selectSingleNode(pocObj.getDataDocument(), messageToken.getTokenValue());
                          if(xmlText==null)
                          {
                              logger.debug("XMLtext for " + messageToken.getTokenValue() + " has null value in the POC data document");
                          }
                          replacementMap.put(messageToken.getTokenId(), getTextValue(xmlText, messageToken.getDefaultValue()));
                      }
                      else if(messageToken.getTokenType().equals("text"))
                      {
                          replacementMap.put(messageToken.getTokenId(), messageToken.getTokenValue());
                      }
                      else if(messageToken.getTokenType().equals("function"))
                      {
                          replacementMap.put(messageToken.getTokenId(), TraxUtil.getInstance().transform((Document) pocObj.getDataDocument(), messageToken.getTokenValue(), null).trim());
                      }
                      else if(messageToken.getTokenType().equals("content"))
                      {
                          if (messageToken.getTokenValue() != null) {
                              String contentValue = contentLookup(connection, pocObj, messageToken);  
                              if (contentValue != null) {
                                  replacementMap.put(messageToken.getTokenId(), contentValue);
                                  logger.debug(messageToken.getTokenId() + " contentValue: " + contentValue);                                  
                              }
                          }
                      }
                  }
              }

              return replaceTokens(bodySegment, replacementMap);
          }
          catch (Throwable ex)
          {
              logger.error(ex);
              throw new Exception(ex.getMessage());
          }
          finally
          {
              if(this.closeConnection && connection!=null)
                  connection.close();
          }
      }

      

    /** contentLookup()
     *  Private method to get content filter data for buildMessage methods
     *  
     * @param connection
     * @param pocObj
     * @param messageToken
     * @return
     * @throws Exception
     */
    private String contentLookup(Connection connection, PointOfContactVO pocObj, MessageTokenVO messageToken) throws Exception {
        String contentValue = null;
        String params = messageToken.getTokenValue();
        Document doc = JAXPUtil.parseDocument(params);
        //logger.info(JAXPUtil.toString(doc));
        //logger.info("dataDocument: " + JAXPUtil.toString(pocObj.getDataDocument()));
        String contextParam = getNodeValue(doc, "context", pocObj);
        String nameParam = getNodeValue(doc, "name", pocObj);
        String filter1Value = getNodeValue(doc, "filter1value", pocObj);
        String filter2Value = getNodeValue(doc, "filter2value", pocObj);
        if (contextParam != null & nameParam != null) {
            ConfigurationUtil cu = ConfigurationUtil.getInstance();
            contentValue = cu.getContentWithFilter(connection, contextParam, nameParam, filter1Value, filter2Value);
            if (contentValue == null) {
                contentValue = messageToken.getDefaultValue();
                //logger.info("Using default value");
            }
        }        
        return contentValue;
    }
    
    
    /**
     * replaceTokens()
     * This private method uses a replacement hash map to match and replace tokens in a message template
     * @param String
     * @param HashMap
     * @return String
     */
    private String replaceTokens(String template, HashMap replacementMap)
    {
        String currentToken = null;
        String tokenValue = null;
        StringTokenizer tokenizer = new StringTokenizer(template, TOKEN_DELIMETER);
        StringBuffer mergedString = new StringBuffer();

        if (logger.isDebugEnabled()) {
            logger.debug("String in: " + template);
        }
        while(tokenizer.hasMoreTokens())
        {
            currentToken = tokenizer.nextToken();

            if(replacementMap.containsKey(currentToken))
            {
                tokenValue = (String) replacementMap.get(currentToken);

                if(tokenValue != null)
                {
                    mergedString.append(tokenValue);
                }
                else
                {
                    mergedString.append(TOKEN_DELIMETER + currentToken + TOKEN_DELIMETER);
                }
            }
            else
            {
                mergedString.append(currentToken);
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug("mergedString: " + mergedString);
        }
        
        return mergedString.toString();
    }

  /**
   *
   * getTextValue()
   * @param xmlText
   * @param defaultValue
   * @return String
   */
    private String getTextValue(Text xmlText, String defaultValue)
    {
        if(xmlText != null)
            return xmlText.getNodeValue();
        else
            return defaultValue;
    }

  /**
   * insertMessage()
   *
   * @param pocObj
   * @return PointOfContactVO
   * @throws java.lang.Exception
   */
    public PointOfContactVO insertMessage(PointOfContactVO pocObj) throws Exception
    {
        Connection connection = null;

        try
        {
            //if class contains a connection use it, otherwise obtain one
            if(this.classConnection != null)
            {
                connection = this.classConnection;
            }
            else
            {
                logger.debug("Creating connection. (insertMessage)");
                connection = DataSourceUtil.getInstance().getConnection(ConfigurationUtil.getInstance().getProperty(
                                    MessageConstants.PROPERTY_FILE, MessageConstants.DATASOURCE_NAME));
            }

            MessageGeneratorDAO messageAccess = new MessageGeneratorDAO(connection);
            pocObj.setPointOfContactId(new Long(messageAccess.insertPointOfContact(pocObj)).longValue());
        }
        catch (Throwable ex)
        {
            logger.error(ex);
            throw new Exception(ex.getMessage());
        }
        finally
        {
            if(this.closeConnection && connection!=null)
                connection.close();
        }

        return pocObj;
    }

  /**
   * sendMessage()
   *
   * @param pocObj
   * @return PointOfContactVO
   * @throws java.lang.Exception
   */
    public static PointOfContactVO sendMessage(PointOfContactVO pocObj) throws Exception
    {
        MessageToken token = new MessageToken();
        Context context = new InitialContext();
        token.setMessage(new Long(pocObj.getPointOfContactId()).toString());
        token.setStatus("SENDMESSAGE");
        Dispatcher dispatcher = Dispatcher.getInstance();
        dispatcher.dispatchTextMessage(context, token);

        return pocObj;
    }


  /**
   * processMessage()
   * builds, inserts and sends a message
   * @param pocObj
   * @return PointOfContactVO
   * @throws java.lang.Exception
   */
    public List processMessages(ArrayList pocList) throws Exception
    {
        List sendEmailList = new ArrayList();
        List tokenList = new ArrayList();

        //flag so other private methods will not close connection
        this.closeConnection = false;

        logger.debug("Creating connection. (insertMessage)");
        this.classConnection = DataSourceUtil.getInstance().getConnection(ConfigurationUtil.getInstance().getProperty(
                            MessageConstants.PROPERTY_FILE, MessageConstants.DATASOURCE_NAME));


        try
        {
            //loop through list and create email objects and insert into database
            Iterator iter = pocList.iterator();
            while(iter.hasNext())
            {
                PointOfContactVO pocObj = (PointOfContactVO)iter.next();

                pocObj = this.buildMessage(pocObj);

                pocObj = this.insertMessage(pocObj);

                //create email token
                MessageToken token = new MessageToken();
                token.setMessage(new Long(pocObj.getPointOfContactId()).toString());
                token.setStatus("SENDMESSAGE");
                sendEmailList.add(pocObj);
                tokenList.add(token);
            }

            //send out messages
            if(sendEmailList !=null && sendEmailList.size() > 0)
            {
                Context context = new InitialContext();
                Dispatcher dispatcher = Dispatcher.getInstance();
                dispatcher.dispatchTextMessages(context, tokenList);
            }

        }
        finally
        {
            if(this.classConnection != null && !this.classConnection.isClosed())
            {
                this.classConnection.close();
            }
        }


        return sendEmailList;
    }


  /**
   * processMessage()
   * builds, inserts and sends a message
   * @param pocObj
   * @return PointOfContactVO
   * @throws java.lang.Exception
   */
    public PointOfContactVO processMessage(PointOfContactVO pocObj) throws Exception
    {
        this.buildMessage(pocObj);

        this.insertMessage(pocObj);

        this.sendMessage(pocObj);

        return pocObj;
    }
    
    private String getNodeValue(Document doc, String nodeName, PointOfContactVO pocObj) {
    	String returnValue = null;
    	
	    Element item = (Element) doc.getElementsByTagName(nodeName).item(0);
        if (item != null && item.hasChildNodes()) {
            Node n = item.getFirstChild();
            if (n.getNodeValue() != null) {
            	if (item.getAttribute("type") == null || item.getAttribute("type").equalsIgnoreCase("text")) {
            		returnValue = n.getNodeValue();
            	} else if (item.getAttribute("type").equalsIgnoreCase("value") && pocObj.getDataDocument() != null) {
            		try {
            		    Text xmlText = (Text) DOMUtil.selectSingleNode(pocObj.getDataDocument(), n.getNodeValue());
            		    returnValue = getTextValue(xmlText, null);
            		} catch (Exception e) {
            			logger.error(e);
            		}
            	}
            }
        }
        if (logger.isDebugEnabled()) {
    	    logger.debug(nodeName + ": " + returnValue);
        }

        return returnValue;
    }
    
    public String generateRecordAttributeXMLFromOrderGuid(String orderGuid) throws SQLException  
    {
    	if (StringUtils.isEmpty(orderGuid)) {
			logger.error("Invalid/Null OrderGuid passed");
			return null;
		}
    	
    	Connection connection = null;
    	String recordAttributeXml = null;
	    try {
	    	 //if class contains a connection use it, otherwise obtain one
	        if(this.classConnection != null)
	        {
	            connection = this.classConnection;
	        }
	        else
	        {
	            connection =
	            DataSourceUtil.getInstance().getConnection(ConfigurationUtil.getInstance().getProperty(
	                    MessageConstants.PROPERTY_FILE, MessageConstants.DATASOURCE_NAME));
	            logger.debug("Creating connection. (buildMessage)");
	        }        
			
			ScrubMapperDAO scrubMapperDao = new ScrubMapperDAO(connection);
			OrderVO orderVo = null;
			try {
				orderVo = scrubMapperDao.mapOrderFromDB(orderGuid);
			} catch (Exception e) {
				logger.error("Exception occured while fetching orderVo for orderGuid: "	+ orderGuid, e);
			}
			if (orderVo != null) {
				recordAttributeXml = generateRecordAttributeXML(orderVo);
			} else {
				logger.error("Null OrderVO returned for order guid: " + orderGuid);
			}
    	}
		catch (Throwable ex)
        {			
            logger.error("Exception occured while generating record attribute xml from order guid: " + orderGuid, ex);            
        }
        finally
        {
            if(this.closeConnection && connection!=null)
                connection.close();
        }	    
	    return recordAttributeXml;
    }

    
    public String generateRecordAttributeXML(OrderVO order) throws SQLException
    {
    	String recordAttributeXml = null;
    	Connection connection = null;
    	try 
    	{

            HashMap<String,String> attributeMap = new HashMap<String, String>();
            String sourceType = order.getSourceType();
            logger.info("sourceType: " + sourceType);
            if (sourceType != null) {
            	attributeMap.put(BuildEmailConstants.NAME_SOURCE_TYPE, sourceType);
            }
            String partnerId = null;
            logger.info("partnerId: " + order.getPartnerId()); 
            logger.info("partnerName: " + order.getPartnerName());
            if (order.getPreferredProcessingPartners() != null) {
                logger.info(" preferredPartner: " + order.getPreferredProcessingPartners().iterator().next());
            }
            if (order.getPartnerName() != null) {
            	partnerId = order.getPartnerName();
            }
            
       	 	//if class contains a connection use it, otherwise obtain one
            if(this.classConnection != null)
            {
                connection = this.classConnection;
            }
            else
            {
                connection =
                DataSourceUtil.getInstance().getConnection(ConfigurationUtil.getInstance().getProperty(
                        MessageConstants.PROPERTY_FILE, MessageConstants.DATASOURCE_NAME));
                logger.debug("Creating connection. (buildMessage)");
            }
            
    		if(partnerId == null) {
    			MercentOrderPrefixes mercentOrderPrefixes = new MercentOrderPrefixes(connection);
    			if (mercentOrderPrefixes != null && mercentOrderPrefixes.isMercentOrder(order.getOrderOrigin())) {
    				partnerId = order.getOrderOrigin();
    			}
    		}
    		
    		if(partnerId == null) {
    			PartnerMappingVO partnerMappingVO = new PartnerUtility().getPartnerOriginsInfo(order.getOrderOrigin(),order.getSourceCode(), connection);
    			if(partnerMappingVO != null && partnerMappingVO.isDefaultBehaviour()) {
    				partnerId = order.getOrderOrigin();
    			}
    		}
            logger.info("partnerId: " + partnerId);
            if (partnerId != null) {
            	attributeMap.put(BuildEmailConstants.NAME_PARTNER_ID, partnerId);
            }
            String freeShippingFlag = order.getFreeShippingUseFlag();
            logger.info("freeShippingFlag: " + freeShippingFlag);
            if (freeShippingFlag != null) {
            	attributeMap.put(BuildEmailConstants.NAME_GOLD_MEMBERSHIP_FLAG, freeShippingFlag);
            }
            
            if (order.getBuyer() != null && order.getBuyer().size() > 0) {
                BuyerVO buyerVO = (BuyerVO) order.getBuyer().get(0);
            	attributeMap.put(BuildEmailConstants.NAME_CUSTOMER_FIRST_NAME, buyerVO.getFirstName());
            	attributeMap.put(BuildEmailConstants.NAME_CUSTOMER_LAST_NAME, buyerVO.getLastName());
            	if(buyerVO.getBuyerAddresses() != null && buyerVO.getBuyerAddresses().size() > 0) {   
            		BuyerAddressesVO buyerAddressVO = (BuyerAddressesVO) buyerVO.getBuyerAddresses().get(0);
            		String buyerAddress = buyerAddressVO.getAddressLine1();
            		if (buyerAddressVO.getAddressLine2() != null) {
            			buyerAddress = buyerAddress + " " + buyerAddressVO.getAddressLine2();
            		}
            		attributeMap.put(BuildEmailConstants.NAME_CUSTOMER_ADDRESS, buyerAddress);
            		attributeMap.put(BuildEmailConstants.NAME_CUSTOMER_CITY, buyerAddressVO.getCity());
            		attributeMap.put(BuildEmailConstants.NAME_CUSTOMER_STATE, buyerAddressVO.getStateProv());
            		attributeMap.put(BuildEmailConstants.NAME_CUSTOMER_ZIP_CODE, buyerAddressVO.getPostalCode());
            		attributeMap.put(BuildEmailConstants.NAME_CUSTOMER_COUNTRY, buyerAddressVO.getCountry());
                }
                if (buyerVO.getBuyerPhones() != null && buyerVO.getBuyerPhones().size() > 0) {
                    BuyerPhonesVO buyerPhoneVO = (BuyerPhonesVO) buyerVO.getBuyerPhones().get(0);
                    String buyerPhone = buyerPhoneVO.getPhoneNumber();
                    if (buyerPhone != null) {
                    	attributeMap.put(BuildEmailConstants.NAME_CUSTOMER_PHONE_NUMBER, buyerPhone);
                    }
                }
            }
            
    		if(attributeMap != null && attributeMap.size() > 0)
    		{
    			// root elements
    			Document attributeDocument = DOMUtil.getDefaultDocument();

    			// Create validation root
    			Element attributesElement = attributeDocument.createElement("attributes");
    			attributeDocument.appendChild(attributesElement);

    			Set<String>   keys = attributeMap.keySet();
    			if(keys.size() > 0)
    			{
    				for (Iterator<String> iter = keys.iterator(); iter.hasNext(); )
    				{
    					String fieldName = iter.next();
    					String fieldValue = attributeMap.get(fieldName);

    					Element attributeElement = attributeDocument.createElement("attribute");
    					attributesElement.appendChild(attributeElement);

    					Element nameElement = attributeDocument.createElement("name");
    					nameElement.appendChild(attributeDocument.createTextNode(fieldName));
    					attributeElement.appendChild(nameElement);

    					Element valueElement = attributeDocument.createElement("value");
    					valueElement.appendChild(attributeDocument.createCDATASection(fieldValue));
    					attributeElement.appendChild(valueElement);

    				}
    			}
    			StringWriter sw = new StringWriter();
    			PrintWriter pw = new PrintWriter(sw);
    			DOMUtil.print(attributeDocument, pw);
    			recordAttributeXml = sw.toString() ;
    		}
    	}
    	catch (Exception pce) {
    		//pce.printStackTrace();
    		logger.error( "Exception occured while generating record attribute xml. ", pce);
    	} 
    	finally
        {
            if(this.closeConnection && connection!=null)
                connection.close();
        }

    	return recordAttributeXml;
    }

}