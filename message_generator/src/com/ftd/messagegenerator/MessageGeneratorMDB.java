package com.ftd.messagegenerator;
import com.ftd.messagegenerator.constants.BuildEmailConstants;
import com.ftd.messagegenerator.constants.MessageConstants;
import com.ftd.messagegenerator.dao.MessageGeneratorDAO;
import com.ftd.messagegenerator.vo.PointOfContactVO;
import com.ftd.messagegenerator.bo.BounceEmailBO;
import com.ftd.messagegenerator.bo.ConfirmationEmailGeneratorBO;
import com.ftd.messagegenerator.bo.ExactTargetBO;
import com.ftd.messagegenerator.bo.MassagePointOfContactBO;
import com.ftd.messagegenerator.bo.PhoenixGeneratorBO;
import com.ftd.messagegenerator.bo.PreviewEmailGeneratorBO;
import com.ftd.messagegenerator.bo.ReinstateConfirmationEmailGeneratorBO;
import com.ftd.messagegenerator.bo.RemovePendingEmailGeneratorBO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.email.EmailVO;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.j2ee.NotificationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.vo.NotificationVO;

import java.sql.Connection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import com.ftd.ftdutilities.CustProfileTimeoutException;

public class MessageGeneratorMDB implements MessageDrivenBean, MessageListener 
{
	private MessageDrivenContext context;
	private static Logger logger = new Logger("com.ftd.messagegenerator.MessageGeneratorMDB"); 
	private static final String MESSAGE_SOURCE = "MESSAGE_GENERATOR";
	private static final String CONNECT_ERROR_MESSAGE = "Could not connect to SMTP host";

	public void ejbCreate()
	{
	}

	public void onMessage(Message msg)
	{
		Connection connection = null;
		PointOfContactVO pocObj = new PointOfContactVO();
		NotificationVO nvo = new NotificationVO();
		boolean isPhoenixEmail = false;
		boolean retryJms = false;

		try {
			TextMessage textMessage = (TextMessage)msg;
			//System.out.println(textMessage.getText());SMTP_HOST_NAME
			String pocId = textMessage.getText();
			logger.info("onMessage started for pocId: " + pocId);

			// get database connection      
			connection =
				DataSourceUtil.getInstance().getConnection(ConfigurationUtil.getInstance().getProperty(
						MessageConstants.PROPERTY_FILE, MessageConstants.DATASOURCE_NAME));

			MessageGeneratorDAO msgAccess = new MessageGeneratorDAO(connection);
			nvo.setConnection(connection);

			if("PROCESSBOUNCE".equalsIgnoreCase(pocId))
			{
				pocObj.setPointOfContactType("BOUNCE");
			}
			else
			{      
				// load POC from the database to get the email information
				pocObj = msgAccess.loadPointOfContact(pocId);
			}

			if(pocObj==null)
			{
				// use notification utility to send an error page to tech support
				logger.error("Requested POC id **"  + pocId +  "** not found in database");
				throw new Exception("Requested POC id **"  + pocId +  "** not found in database");
			}
			else
			{ 	  

				// ---------------------
				//
				// Normal email handling
				//
				if(pocObj.getPointOfContactType().equalsIgnoreCase(MessageConstants.EMAIL_MESSAGE_TYPE)) {
					// Test system flag
					String testEmailFlag = ConfigurationUtil.getInstance().getFrpGlobalParm(
							MessageConstants.MESSAGE_GEN_CONFIG_CONTEXT, MessageConstants.TEST_EMAILS_ONLY);

					if(testEmailFlag != null && testEmailFlag.equalsIgnoreCase("Y") && pocObj.getRecipientEmailAddress() != null 
							&& pocObj.getRecipientEmailAddress().toUpperCase().indexOf("FTDI.COM") < 0) {

						logger.info("*** Email to " + pocObj.getRecipientEmailAddress() + " not sent because currently in test environment ***");
					} else {

						String emailType = pocObj.getEmailType();
						logger.info("emailType: " + emailType);
						boolean htmlNeedsConverting = true;

						String contentType = ConfigurationUtil.getInstance().getFrpGlobalParm(
								MessageConstants.MESSAGE_GEN_CONFIG_CONTEXT, MessageConstants.SEND_CONTENT_TYPE);

						if (emailType != null && emailType.equalsIgnoreCase(BuildEmailConstants.CONFIRMATION_ACTION)) {
							if (logger.isDebugEnabled()) {
							    logger.debug("Order Confirmation Email");
							}
							ConfirmationEmailGeneratorBO cegBO = new ConfirmationEmailGeneratorBO();
							EmailVO emailVO = cegBO.process(connection, pocObj);
							
							StringBuffer emailSubj = new StringBuffer(emailVO.getSubject());
							emailSubj.append(" (Order Number: ").append(emailVO.getOrderNumber()).append(")");
							
							logger.info("Email Subject :: "+emailSubj.toString());
                    		pocObj.setEmailSubject(emailSubj.toString());
							pocObj.setSenderEmailAddress(emailVO.getSender());
							pocObj.setRecipientEmailAddress(emailVO.getRecipient());
							pocObj.setRecordAttributesXML(emailVO.getRecordAttributesXML());
							if (contentType == null || contentType.equalsIgnoreCase(MessageConstants.TEXT_CONTENT_TYPE)) {
								pocObj.setBody(emailVO.getContent());
							} else {
								pocObj.setBody(emailVO.getHTMLContent());
							}
							htmlNeedsConverting = false;
						} else if (emailType != null && emailType.equalsIgnoreCase(BuildEmailConstants.PEND_REMOVE_ACTION)) {
							if (logger.isDebugEnabled()) {
							    logger.debug("Pending/Remove Email");
							}
							RemovePendingEmailGeneratorBO rpegBO = new RemovePendingEmailGeneratorBO();
							EmailVO emailVO = rpegBO.process(connection, pocObj);
							if (contentType == null || contentType.equalsIgnoreCase(MessageConstants.TEXT_CONTENT_TYPE)) {
								pocObj.setBody(emailVO.getContent());
							} else {
								pocObj.setBody(emailVO.getHTMLContent());
							}
						} else if (emailType != null && emailType.equalsIgnoreCase(BuildEmailConstants.REINSTATE_CONFIRMATION_ACTION)) {
							if (logger.isDebugEnabled()) {
							    logger.debug("Reinstate Email");
							}
							ReinstateConfirmationEmailGeneratorBO rcegBO = new ReinstateConfirmationEmailGeneratorBO();
							EmailVO emailVO = rcegBO.process(connection, pocObj);
							pocObj.setEmailSubject(emailVO.getSubject());
							pocObj.setSenderEmailAddress(emailVO.getSender());
							pocObj.setRecipientEmailAddress(emailVO.getRecipient());
							pocObj.setRecordAttributesXML(emailVO.getRecordAttributesXML());
							if (contentType == null || contentType.equalsIgnoreCase(MessageConstants.TEXT_CONTENT_TYPE)) {
								pocObj.setBody(emailVO.getContent());
							} else {
								pocObj.setBody(emailVO.getHTMLContent());
							}
							htmlNeedsConverting = false;
						} else if (emailType != null && emailType.equalsIgnoreCase(BuildEmailConstants.PREVIEW_ACTION)) {
							if (logger.isDebugEnabled()) {
							    logger.debug("Preview Email");
							}
							PreviewEmailGeneratorBO pegBO = new PreviewEmailGeneratorBO();
							EmailVO emailVO = pegBO.process(connection, pocObj);
							pocObj.setEmailSubject(emailVO.getSubject());
							pocObj.setSenderEmailAddress(emailVO.getSender());
							pocObj.setRecordAttributesXML(emailVO.getRecordAttributesXML());
							if (contentType == null || contentType.equalsIgnoreCase(MessageConstants.TEXT_CONTENT_TYPE)) {
								pocObj.setBody(emailVO.getContent());
							} else {
								pocObj.setBody(emailVO.getHTMLContent());
							}
							htmlNeedsConverting = false;
						} else if (emailType != null && emailType.equalsIgnoreCase(BuildEmailConstants.MASSAGE_POC)) {
                            if (logger.isDebugEnabled()) {
                                logger.debug("Massage PointOfContact");
                            }
						    MassagePointOfContactBO mpocBO = new MassagePointOfContactBO();
						    mpocBO.process(connection, pocObj);
						}

						String webServiceEnabled = ConfigurationUtil.getInstance().getFrpGlobalParm(
								MessageConstants.MESSAGE_GEN_CONFIG_CONTEXT, MessageConstants.WEB_SERVICE_ENABLED);
						if (logger.isDebugEnabled()) {
						    logger.debug("webServiceEnabled: " + webServiceEnabled);
						}

						if (webServiceEnabled != null && webServiceEnabled.equalsIgnoreCase("Y") &&	pocObj.getEmailType() != null) {
							if (htmlNeedsConverting && pocObj.getBody() != null) {
								if (logger.isDebugEnabled()) {
								    logger.debug("Converting body text to html");
								}
								NotificationUtil mail = NotificationUtil.getInstance();
								StringBuffer messageHtmlContent = new StringBuffer(pocObj.getBody());
								messageHtmlContent = mail.convertStockToHtmlEmail(messageHtmlContent, connection);
								pocObj.setBody(messageHtmlContent.toString());
							}
							ExactTargetBO etBO = new ExactTargetBO();
							etBO.prepareAndSendEmailRequest(connection, pocObj);
						}             
						else {

							// Obtain SMTP host from configuration file
							String smtpHostName = ConfigurationUtil.getInstance().getFrpGlobalParm(
									MessageConstants.MESSAGE_GEN_CONFIG_CONTEXT, MessageConstants.SMTP_HOST_NAME);

							//Mailserver transformer flag
							String msTransformerEnabled = ConfigurationUtil.getInstance().getFrpGlobalParm(
									MessageConstants.MESSAGE_GEN_CONFIG_CONTEXT, MessageConstants.MAILSERVER_TRANSFORMER_ENABLED);
							String msTransformerKey = ConfigurationUtil.getInstance().getFrpGlobalParm(
									MessageConstants.MESSAGE_GEN_CONFIG_CONTEXT, MessageConstants.MAILSERVER_KEY);

							StringBuffer messageTextContent = null;
							StringBuffer messageHtmlContent = null;
							NotificationUtil mail = NotificationUtil.getInstance();
							//populate a NotificationVO
							nvo.setMessageFromAddress(pocObj.getSenderEmailAddress());

							nvo.setMessageSubject(pocObj.getEmailSubject());
							nvo.setMessageTOAddress(pocObj.getRecipientEmailAddress());

							// The mailserver transformer SMTP host is used only if it's enabled and the POC reflects it should be used
							if ("Y".equalsIgnoreCase(msTransformerEnabled) && pocObj.getMailserverCode() != null) {
								msTransformerKey = MessageConstants.MAILSERVER_HOST_NAME_PREFIX + 
								ConfigurationUtil.getInstance().getFrpGlobalParm(
										MessageConstants.MESSAGE_GEN_CONFIG_CONTEXT, MessageConstants.MAILSERVER_KEY);
								String msTransformerHostName = ConfigurationUtil.getInstance().getFrpGlobalParm(
										MessageConstants.MESSAGE_GEN_CONFIG_CONTEXT, msTransformerKey);
								nvo.setSMTPHost(msTransformerHostName);
							} else {
								nvo.setSMTPHost(smtpHostName);
							}

							if (logger.isDebugEnabled()) {
								logger.debug("MessageFromAddress: " + nvo.getMessageFromAddress());
								logger.debug("MessageSubject: " + nvo.getMessageSubject());
								logger.debug("MessageTOAddress: " + nvo.getMessageTOAddress());
								logger.debug("SMTPHost: " + nvo.getSMTPHost());
								logger.debug("Content Type from global parm " + contentType);
								logger.debug("htmlNeedsConverting: " + htmlNeedsConverting);
							}

							if (contentType == null || contentType.equalsIgnoreCase(MessageConstants.TEXT_CONTENT_TYPE)) {
								messageTextContent = new StringBuffer(pocObj.getBody());
								messageTextContent = mail.convertStockToTextEmail(messageTextContent, nvo.getConnection());

								nvo.setMessageContent(messageTextContent.toString());
								if (logger.isDebugEnabled()) {
									logger.debug("MessageContent: " + nvo.getMessageContent());
								}
								mail.notify(nvo);                
							} else {
								//Send multipart email
								messageTextContent = mail.fixCharacterEncoding(pocObj.getBody());
								messageHtmlContent = mail.fixCharacterEncoding(pocObj.getBody());

								messageTextContent = mail.convertStockToTextEmail(messageTextContent, nvo.getConnection());
								if (htmlNeedsConverting) {
									messageHtmlContent = mail.convertStockToHtmlEmail(messageHtmlContent, nvo.getConnection());
								}

								nvo.setMessageContent(messageTextContent.toString());
								nvo.setMessageContentHTML(messageHtmlContent.toString());

								if (logger.isDebugEnabled()) {
									logger.debug("MessageContentHTML: " + nvo.getMessageContentHTML());
								}
								mail.notifyMulipart(nvo);
							}

						}
					}

					// ----------------------
					//
					// Phoenix email handling
					//
				} else if(pocObj.getPointOfContactType().equalsIgnoreCase(MessageConstants.EMAIL_MESSAGE_TYPE_PHOENIX)) {
					String webServiceEnabled = ConfigurationUtil.getInstance().getFrpGlobalParm(
							MessageConstants.MESSAGE_GEN_CONFIG_CONTEXT, MessageConstants.WEB_SERVICE_ENABLED);
					if (logger.isDebugEnabled()) {
					    logger.debug("webServiceEnabled: " + webServiceEnabled);
					}

					if (webServiceEnabled != null && webServiceEnabled.equalsIgnoreCase("Y")) {
						isPhoenixEmail = true;
						PhoenixGeneratorBO pbo = new PhoenixGeneratorBO();
						pbo.prepareAndSendEmailRequest(connection, pocObj);
					} else {
						logger.info("Web service is not enabled, Phoenix processing will not occur");
					}
				}
				else if (pocObj.getPointOfContactType().equalsIgnoreCase("BOUNCE"))
				{
					DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");  
					String newBatchTime = dateFormat.format(new Date());

					try
					{ 
						BounceEmailBO emailBo = new BounceEmailBO();
						emailBo.processBounce(connection);
						msgAccess.doUpdateGlobalParms("LAST_BOUNCE_EMAIL_BATCH_TIME", newBatchTime, "MsgGen");             		 
					}
					catch(Exception e)
					{
						logger.error("Send message MDB failed when processing bounce " + e);
						this.sendSystemMessage("Send message MDB failed when processing bounce", connection);
						return;
					}
				} 

				// -------------------------
				//
				// Email type not recognized
				//
				else {
					// for this release ONLY emails should exist
					logger.error("Invalid POC type received in Stock Message Generator. Type="+pocObj.getPointOfContactType());
					// using the SystemNotificationUtil send out a page to tech support          
				}
			}
		}
		catch (Throwable mdbe)
		{
			try
			{
				// Handling for exception during Phoenix email processing
				//
				if (isPhoenixEmail) {
					retryJms = true;
					logger.error("Send message MDB failed to send Phoenix order email. POC Id = " + pocObj.getPointOfContactId());
					logger.error(mdbe);              
					this.sendSystemMessage("Send message MDB failed to send Phoenix order email. POC Id = " + pocObj.getPointOfContactId(), connection);

					// Handling for exception during normal email processing
					//
				} else {
					logger.error("Send message MDB failed to send email. POC Id = " + pocObj.getPointOfContactId());
					logger.error(mdbe);

					// If connect error, try again using alternate SMTP host
					if (mdbe.toString().indexOf(CONNECT_ERROR_MESSAGE) != -1) {
						// Obtain SMTP host from configuration file
						String smtpHostName = ConfigurationUtil.getInstance().getFrpGlobalParm(MessageConstants.MESSAGE_GEN_CONFIG_CONTEXT,
								MessageConstants.SMTP_ALTERNATE_HOST_NAME);
						nvo.setSMTPHost(smtpHostName);
						logger.debug("Retrying email send via " + smtpHostName);
						// send out the email
						NotificationUtil mail = NotificationUtil.getInstance();
						mail.notify(nvo);
					} else if( mdbe instanceof CustProfileTimeoutException){
						retryJms = true;
						logger.error("Send message MDB failed to send email due to CustProfileTimeoutException. POC Id = " + pocObj.getPointOfContactId());
						logger.error(mdbe);
						this.sendSystemMessage("Send message MDB failed to send email due to CustProfileTimeoutException. POC Id = " + pocObj.getPointOfContactId(), connection,"NOPAGE CAMSTimeOut Error");
					}else {
						retryJms = true;
						this.sendSystemMessage("Send message MDB failed to send email. POC Id = " + pocObj.getPointOfContactId(), connection);
					}
				}
			}
			catch(Exception sme)
			{
				retryJms = true;
				logger.error(sme);
			}
		}
		finally 
		{
			try 
			{
				if(connection!=null)
					connection.close();        
			}
			catch (Exception e)
			{
				logger.error("MessageGeneratorMDB caught exception in finally clause: " + e);
			}
			if (retryJms) {
				// Throw RuntimeException so JMS message will be retried
			   logger.error("MessageGeneratorMDB throwing runtime exception so JMS is not acknowledged but retried instead");
			   if (context != null) {
			      context.setRollbackOnly();
			   } else {
	            logger.error("No context, so rollback-only flag could not be set");			      
			   }
				throw new RuntimeException("MessageGeneratorMDB failed to send email");            
			}
			logger.info("onMessage completed");
		}
	}

	public void ejbRemove()
	{
	}

	public void setMessageDrivenContext(MessageDrivenContext ctx)
	{
		this.context = ctx;
	}
    private String sendSystemMessage(String message, Connection connection)throws Exception{
    	return sendSystemMessage(message,connection,null);
    }
	private String sendSystemMessage(String message, Connection connection,String subject) throws Exception
	{
		logger.error("Sending System Message:" + message);        

		String messageID = "";

		//build system vo
		SystemMessengerVO sysMessage = new SystemMessengerVO();
		sysMessage.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
		sysMessage.setSource(MESSAGE_SOURCE);
		sysMessage.setType("ERROR");
		sysMessage.setMessage(message);
		if(subject != null)
	    	  sysMessage.setSubject(subject);
		
		SystemMessenger sysMessenger = SystemMessenger.getInstance();
		messageID = sysMessenger.send(sysMessage, getConnection());

		if(messageID == null) 
		{
			String msg = "Error occured while attempting to send out a system message. Msg not sent: " + message;
			logger.error(msg);
		}

		return messageID;  
	}
	public Connection getConnection()throws Exception{
		
		    String datasource = ConfigurationUtil.getInstance().getProperty(MessageConstants.PROPERTY_FILE, MessageConstants.DATASOURCE_NAME);
		    Connection conn = DataSourceUtil.getInstance().getConnection(datasource);
		    return conn;
	}
}