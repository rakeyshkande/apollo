package com.ftd.messagegenerator.dao;

import com.ftd.messagegenerator.constants.MessageConstants;
import com.ftd.messagegenerator.vo.PointOfContactVO;
import com.ftd.messagegenerator.vo.QueueVO;
import com.ftd.messagegenerator.vo.StockMessageVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;

import java.sql.Clob;
import java.sql.Connection;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * MessageGeneratorDAO
 * 
 */
public class MessageGeneratorDAO 
{
    private Connection connection = null;
    
/**
 * Constructor
 * @param conn
 */
  public MessageGeneratorDAO(Connection conn)
  {
    this.connection = conn;
  }
    
  public StockMessageVO loadMessageTemplate(String messageType, String companyId, String stockMessageId) throws Exception
  {
    StockMessageVO msgObj = null;
    
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(connection);
    
    dataRequest.setStatementID(messageType.equalsIgnoreCase(MessageConstants.EMAIL_MESSAGE_TYPE)? "VIEW_STOCK_EMAIL":"VIEW_STOCK_LETTER");
    dataRequest.addInputParam(messageType.equalsIgnoreCase(MessageConstants.EMAIL_MESSAGE_TYPE) ? "IN_STOCK_EMAIL_ID":"IN_STOCK_LETTER_ID", stockMessageId);
    //dataRequest.addInputParam("IN_COMPANY_ID", companyId);
    //dataRequest.addInputParam("IN_ORIGIN_ID", null); //???
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    
    CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
    if(rs.next())
    {
      msgObj = new StockMessageVO();
      msgObj.setMessageId(stockMessageId);
      msgObj.setMessageType(messageType);
      msgObj.setOriginId("");
      msgObj.setTitle(rs.getString("title"));
      if(messageType.equalsIgnoreCase(MessageConstants.EMAIL_MESSAGE_TYPE))
      {
        msgObj.setSubject(rs.getString("subject"));
        //msgObj.setAutoResponseIndicator(rs.getString("AUTO_RESPONSE_INDICATOR"));
      }
      msgObj.setContent(rs.getClob("body").getSubString((long)1, (int)rs.getClob("body").length()));
    }
    
    return msgObj;    
  }
  
  public StockMessageVO loadMessageTemplateByTitle(String messageType, String messageTitle, String originId, String partnerName) throws Exception
  {
    StockMessageVO msgObj = null;
    
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(connection);
    
    dataRequest.setStatementID(messageType.equalsIgnoreCase(MessageConstants.EMAIL_MESSAGE_TYPE)? "VIEW_STOCK_EMAIL_BY_TITLE":"VIEW_STOCK_LETTER_BY_TITLE");
    dataRequest.addInputParam("IN_TITLE", messageTitle);
    dataRequest.addInputParam("IN_ORIGIN_ID", originId);
    dataRequest.addInputParam("IN_PARTNER_NAME", partnerName);
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    
    CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
    if(rs.next())
    {
      msgObj = new StockMessageVO();
      msgObj.setMessageId(rs.getObject(1).toString());
      msgObj.setMessageType(messageType);
      msgObj.setOriginId("");
      msgObj.setTitle(rs.getString("title"));
      if(messageType.equalsIgnoreCase(MessageConstants.EMAIL_MESSAGE_TYPE))
      {
        msgObj.setSubject(rs.getString("subject"));
        //msgObj.setAutoResponseIndicator(rs.getString("AUTO_RESPONSE_INDICATOR"));
      }
      msgObj.setContent(rs.getClob("body").getSubString((long)1, (int)rs.getClob("body").length()));
    }
    
    return msgObj;    
  }
  
  public String insertPointOfContact(PointOfContactVO pocObj) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    try {
    dataRequest.setConnection(connection);
    dataRequest.setStatementID("INSERT_POINT_OF_CONTACT");
    if(pocObj.getCustomerId()==0)
      dataRequest.addInputParam("IN_CUSTOMER_ID", null);
    else
      dataRequest.addInputParam("IN_CUSTOMER_ID", new Long (pocObj.getCustomerId()).toString());
    dataRequest.addInputParam("IN_ORDER_GUID", pocObj.getOrderGuid());
    if(pocObj.getOrderDetailId()==0)
      dataRequest.addInputParam("IN_ORDER_DETAIL_ID", null);
    else
      dataRequest.addInputParam("IN_ORDER_DETAIL_ID", new Long (pocObj.getOrderDetailId()).toString());
    dataRequest.addInputParam("IN_MASTER_ORDER_NUMBER", pocObj.getMasterOrderNumber());
    dataRequest.addInputParam("IN_EXTERNAL_ORDER_NUMBER", pocObj.getExternalOrderNumber());
    
    java.sql.Date deliveryDate = null;
    if(pocObj.getDeliveryDate() != null)
    {
        deliveryDate = new java.sql.Date(pocObj.getDeliveryDate().getTimeInMillis());
    }
    java.sql.Date newDeliveryDate = null;
    if(pocObj.getNewDeliveryDate() != null)
    {
        newDeliveryDate = new java.sql.Date(pocObj.getNewDeliveryDate().getTimeInMillis());
    } else {
        newDeliveryDate = deliveryDate;
    }
    
    dataRequest.addInputParam("IN_DELIVERY_DATE", deliveryDate);
    dataRequest.addInputParam("IN_COMPANY_ID", pocObj.getCompanyId());
    dataRequest.addInputParam("IN_FIRST_NAME", pocObj.getFirstName());
    dataRequest.addInputParam("IN_LAST_NAME", pocObj.getLastName());
    dataRequest.addInputParam("IN_DAYTIME_PHONE_NUMBER", pocObj.getDaytimePhoneNumber());
    dataRequest.addInputParam("IN_EVENING_PHONE_NUMBER", pocObj.getEveningPhoneNumber());
    dataRequest.addInputParam("IN_SENDER_EMAIL_ADDRESS", pocObj.getSenderEmailAddress());
    dataRequest.addInputParam("IN_LETTER_TITLE", pocObj.getLetterTitle());
    dataRequest.addInputParam("IN_EMAIL_SUBJECT", pocObj.getEmailSubject());
    dataRequest.addInputParam("IN_BODY", pocObj.getBody());
    dataRequest.addInputParam("IN_COMMENT_TYPE", pocObj.getCommentType());
    dataRequest.addInputParam("IN_RECIPIENT_EMAIL_ADDRESS", pocObj.getRecipientEmailAddress());
    dataRequest.addInputParam("IN_NEW_RECIP_FIRST_NAME", pocObj.getNewRecipFirstName());
    dataRequest.addInputParam("IN_NEW_RECIP_LAST_NAME", pocObj.getNewRecipLastName());
    dataRequest.addInputParam("IN_NEW_ADDRESS", pocObj.getNewAddress());
    dataRequest.addInputParam("IN_NEW_CITY", pocObj.getNewCity());
    dataRequest.addInputParam("IN_NEW_PHONE", pocObj.getNewPhone());
    dataRequest.addInputParam("IN_NEW_ZIP_CODE", pocObj.getNewZipCode());
    dataRequest.addInputParam("IN_NEW_STATE", pocObj.getNewState());
    dataRequest.addInputParam("IN_NEW_COUNTRY", pocObj.getNewCountry());
    dataRequest.addInputParam("IN_NEW_INSTITUTION", pocObj.getNewInstitution());
    dataRequest.addInputParam("IN_NEW_DELIVERY_DATE", newDeliveryDate);
    dataRequest.addInputParam("IN_NEW_CARD_MESSAGE", pocObj.getNewCardMessage());
    if(pocObj.getNewProduct()==0)
      dataRequest.addInputParam("IN_NEW_PRODUCT", null);
    else
      dataRequest.addInputParam("IN_NEW_PRODUCT", new Integer(pocObj.getNewProduct()).toString());
    dataRequest.addInputParam("IN_CHANGE_CODES", pocObj.getChangeCodes());
    dataRequest.addInputParam("IN_SENT_RECEIVED_INDICATOR", pocObj.getSentReceivedIndicator());
    dataRequest.addInputParam("IN_SEND_FLAG", pocObj.getSendFlag());
    dataRequest.addInputParam("IN_PRINT_FLAG", pocObj.getPrintFlag());
    dataRequest.addInputParam("IN_POINT_OF_CONTACT_TYPE", pocObj.getPointOfContactType());
    dataRequest.addInputParam("IN_EMAIL_HEADER", null);
    dataRequest.addInputParam("IN_CREATED_BY", pocObj.getUpdatedBy());
    dataRequest.addInputParam("IN_NEW_CARD_SIGNATURE", pocObj.getNewCardSignature());
    dataRequest.addInputParam("IN_SOURCE_CODE", pocObj.getSourceCode());
    dataRequest.addInputParam("IN_MAILSERVER_CODE", pocObj.getMailserverCode());
    dataRequest.addInputParam("IN_EMAIL_TYPE", pocObj.getEmailType());
    dataRequest.addInputParam("IN_RECORD_ATTRIBUTES_XML", pocObj.getRecordAttributesXML());
    
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();        
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);
    
    String status = (String) outputs.get(MessageConstants.STATUS_PARAM);
    if(status != null && status.equals("N"))
    {
        String message = (String) outputs.get(MessageConstants.MESSAGE_PARAM);
        throw new Exception(message);
    }
    
    return (String) outputs.get("OUT_SEQUENCE_NUMBER");
    
    } finally {
      // Since this DML involved CLOBs, cleanup any temporarily generated CLOBs.
         dataRequest.reset();
    }
  }
  
  public PointOfContactVO loadPointOfContact(String pocId) throws Exception
  {
    PointOfContactVO pocObj = null;    
    DataRequest dataRequest = new DataRequest();
    
    dataRequest.setConnection(connection);
    dataRequest.setStatementID("VIEW_POINT_OF_CONTACT");
    dataRequest.addInputParam("IN_POINT_OF_CONTACT_ID", pocId);
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();        

    CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
    if(rs.next())
    {
      pocObj = new PointOfContactVO();   
      pocObj.setPointOfContactId(rs.getLong("POINT_OF_CONTACT_ID"));
      pocObj.setPointOfContactType(rs.getString("POINT_OF_CONTACT_TYPE"));
      pocObj.setCustomerId(rs.getInt("customer_id"));
      pocObj.setSenderEmailAddress(rs.getString("sender_email_address"));
      pocObj.setRecipientEmailAddress(rs.getString("RECIPIENT_EMAIL_ADDRESS"));
      pocObj.setEmailSubject(rs.getString("EMAIL_SUBJECT"));
      Clob pocBody = rs.getClob("body");
      if(pocBody!=null)
        pocObj.setBody(pocBody.getSubString((long)1, (int) pocBody.length()));
      pocObj.setOrderDetailId(rs.getLong("order_detail_id"));
      pocObj.setOrderGuid(rs.getString("order_guid"));
      pocObj.setFirstName(rs.getString("FIRST_NAME"));
      pocObj.setLastName(rs.getString("LAST_NAME"));
      pocObj.setDaytimePhoneNumber(rs.getString("DAYTIME_PHONE_NUMBER"));
      if(rs.getDate("DELIVERY_DATE") != null)
      {
          Calendar cal = Calendar.getInstance();
          cal.setTime(rs.getDate("DELIVERY_DATE"));
          pocObj.setDeliveryDate(cal);
      }
      pocObj.setNewRecipFirstName(rs.getString("NEW_RECIP_FIRST_NAME"));
      pocObj.setNewRecipLastName(rs.getString("NEW_RECIP_LAST_NAME"));
      pocObj.setNewAddress(rs.getString("NEW_ADDRESS"));
      pocObj.setNewCity(rs.getString("NEW_CITY"));
      pocObj.setNewState(rs.getString("NEW_STATE"));
      pocObj.setNewZipCode(rs.getString("NEW_ZIP_CODE"));
      pocObj.setNewCountry(rs.getString("NEW_COUNTRY"));
      pocObj.setMailserverCode(rs.getString("MAILSERVER_CODE"));
      pocObj.setLetterTitle(rs.getString("LETTER_TITLE"));
      pocObj.setExternalOrderNumber(rs.getString("EXTERNAL_ORDER_NUMBER"));
      pocObj.setCompanyId(rs.getString("COMPANY_ID"));
      if(rs.getDate("NEW_DELIVERY_DATE") != null)
      {
          Calendar cal = Calendar.getInstance();
          cal.setTime(rs.getDate("NEW_DELIVERY_DATE"));
          pocObj.setNewDeliveryDate(cal);
      }
      pocObj.setEmailType(rs.getString("EMAIL_TYPE"));
      Clob pocAttributesXML = rs.getClob("RECORD_ATTRIBUTES_XML");
      if(pocAttributesXML != null) {
          pocObj.setRecordAttributesXML(pocAttributesXML.getSubString((long)1, (int) pocAttributesXML.length()));
      }
      pocObj.setSourceCode(rs.getString("SOURCE_CODE"));
      pocObj.setMasterOrderNumber(rs.getString("MASTER_ORDER_NUMBER"));
    }
    
    return pocObj;
  }

    /**
     * Method takes a messageTitle and companyId and return the appropriate 
     * sender email address ("From:").  Created to implement the noreply address
     * for defect 3370 on 03/12/2007.
     * @param messageTitle
     * @param companyId
     * @return String emailAddress
     * @throws Exception
     */
    public String loadSenderEmailAddress(String messageTitle, String companyId, Long orderDetailId) throws Exception
    {
      String emailAddress = null;   
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(connection);
        
      dataRequest.setStatementID("GET_SENDER_EMAIL");
      dataRequest.addInputParam("IN_TITLE", messageTitle);
      dataRequest.addInputParam("IN_COMPANY_ID", companyId);
      
      if (orderDetailId == null)
      {
          orderDetailId = new Long(0);
      }
      dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);

      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
      emailAddress = (String) dataAccessUtil.execute(dataRequest);
      return emailAddress;
    }
    /**
     * Method takes a messageTitle and companyId and return the appropriate 
     * sender email address ("From:").  Created to implement the noreply address
     * for defect 3370 on 03/12/2007.
     * @param messageTitle
     * @param companyId
     * @param orderDetailId
     * @param partnerName
     * @return String emailAddress
     * @throws Exception
     */
    public String loadSenderEmailAddress(String messageTitle, String companyId, Long orderDetailId, String partnerName) throws Exception
    {
      String emailAddress = null;   
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(connection);
        
      dataRequest.setStatementID("GET_SENDER_EMAIL_PARTNER");
      dataRequest.addInputParam("IN_TITLE", messageTitle);
      dataRequest.addInputParam("IN_COMPANY_ID", companyId);
      dataRequest.addInputParam("IN_PARTNER_NAME", partnerName);
      
      if (orderDetailId == null)
      {
          orderDetailId = new Long(0);
      }
      dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);

      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
      emailAddress = (String) dataAccessUtil.execute(dataRequest);
      return emailAddress;
    }

  /**
     * This method load the "FromAddress" from FTD_APPS.EMAIL_COMPANY_DATA
     * associated with the passed in company_id. This should no longer be used.
     * Use loadSenderEmailAddress(String messageTitle, String companyId).
     * @deprecated
     * @param company_id
     * @return
     * @throws Exception
     */
  public String loadSenderEmailAddress(String company_id) throws Exception
  {
    String emailAddress = null;
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(connection);
    
    dataRequest.setStatementID("GET_COMPANY_EMAIL");
    dataRequest.addInputParam("IN_COMPANY_ID", company_id);        
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    
    CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
    while(rs.next())
    {
      if(rs.getString(1).equalsIgnoreCase("FromAddress"))
      {
      emailAddress = rs.getString(2);        
      }
    }
    
    return emailAddress;
  }
  
  /**
   * This method is used to update the bounce_processed_flag in clean.point_of_contact table.
   * @param pocId
   * @param processedFlag
   * @param updatedBy
   * @throws Exception
   */
  public void updatePOCBounceProcessedFlag(String pocId, String processedFlag, String updatedBy) throws Exception
  {
    DataRequest dataRequest = new DataRequest();

    dataRequest.setConnection(connection);
    dataRequest.setStatementID("UPDATE_POC_BOUNCE_FLAG");
    
    dataRequest.addInputParam("IN_POINT_OF_CONTACT_ID", pocId);
    dataRequest.addInputParam("IN_BOUNCE_PROCESSED_FLAG", processedFlag);
    dataRequest.addInputParam("IN_UPDATED_BY", updatedBy);
    
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();        
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);
    
    String status = (String) outputs.get("OUT_STATUS");
    if(status != null && status.equals("N"))
    {
        String message = (String) outputs.get("OUT_MESSAGE");
        throw new Exception(message);
    }
  }
  
 
  public List<PointOfContactVO> getPocByRecipientEmail(String recipientEmail,String emailType) throws Exception
  {
    PointOfContactVO pocObj = null;    
    List<PointOfContactVO> pocList = new ArrayList<PointOfContactVO>();
    
    DataRequest dataRequest = new DataRequest();
    
    dataRequest.setConnection(connection);
    dataRequest.setStatementID("GET_POC_BY_RECIPIENT_EMAIL");
    dataRequest.addInputParam("IN_RECIPIENT_EMAIL_ADDRESS", recipientEmail);
    dataRequest.addInputParam("IN_EMAIL_TYPE", emailType);
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();        

    CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest); 
    while(rs.next())
    {
      pocObj = new PointOfContactVO();    
      pocObj.setPointOfContactId(rs.getLong("POINT_OF_CONTACT_ID"));
      pocObj.setCustomerId(rs.getInt("customer_id"));
      pocObj.setOrderGuid(rs.getString("ORDER_GUID"));
      pocObj.setOrderDetailId(rs.getInt("ORDER_DETAIL_ID"));
      pocObj.setMasterOrderNumber(rs.getString("MASTER_ORDER_NUMBER"));
      pocObj.setExternalOrderNumber(rs.getString("EXTERNAL_ORDER_NUMBER"));
      pocObj.setEmailSubject(rs.getString("EMAIL_SUBJECT"));
      if(rs.getDate("DELIVERY_DATE") != null)
      {
          Calendar cal = Calendar.getInstance();
          cal.setTime(rs.getDate("DELIVERY_DATE"));
          pocObj.setDeliveryDate(cal);
      }
      pocObj.setCompanyId(rs.getString("COMPANY_ID"));
      pocObj.setFirstName(rs.getString("FIRST_NAME"));
      pocObj.setLastName(rs.getString("LAST_NAME"));
      pocObj.setDaytimePhoneNumber(rs.getString("DAYTIME_PHONE_NUMBER"));
      pocObj.setEveningPhoneNumber(rs.getString("EVENING_PHONE_NUMBER"));
      pocObj.setCommentType(rs.getString("COMMENT_TYPE"));
      pocObj.setRecipientEmailAddress(rs.getString("RECIPIENT_EMAIL_ADDRESS"));
      pocObj.setPointOfContactType(rs.getString("POINT_OF_CONTACT_TYPE"));
      pocObj.setSourceCode(rs.getString("SOURCE_CODE"));
      Clob pocBody = rs.getClob("body");
      if(pocBody!=null) {
          pocObj.setBody(pocBody.getSubString((long)1, (int) pocBody.length()));
      }
      if(rs.getDate("DELIVERY_DATE") != null) {
          Calendar cal = Calendar.getInstance();
          cal.setTime(rs.getDate("DELIVERY_DATE"));
          pocObj.setDeliveryDate(cal);
      }
      
      pocList.add(pocObj);
      
    }    
    return pocList;
  }
  
  
  public void insertQueueRecord(QueueVO queueRecord) throws Exception
  {
      DataRequest dataRequest = new DataRequest();
      
      dataRequest.setConnection(connection);
      dataRequest.setStatementID("INSERT_QUEUE_RECORD");
      
      dataRequest.addInputParam("IN_QUEUE_TYPE", new String(queueRecord.getQueueType()));
      dataRequest.addInputParam("IN_MESSAGE_TYPE", new String(queueRecord.getMessageType()));
      
      java.sql.Timestamp sqlTimeStamp = new java.sql.Timestamp(queueRecord.getMessageTimestamp().getTime());      
      dataRequest.addInputParam("IN_MESSAGE_TIMESTAMP", sqlTimeStamp);
      
      dataRequest.addInputParam("IN_SYSTEM", queueRecord.getSystem() != null ? queueRecord.getSystem() : null);
      dataRequest.addInputParam("IN_MERCURY_NUMBER", queueRecord.getMercuryNumber() != null ? queueRecord.getMercuryNumber() : null);
      dataRequest.addInputParam("IN_MASTER_ORDER_NUMBER", queueRecord.getMasterOrderNumber() != null ? queueRecord.getMasterOrderNumber() : null);
      dataRequest.addInputParam("IN_ORDER_GUID", queueRecord.getOrderGuid() != null ? queueRecord.getOrderGuid() : null);
      dataRequest.addInputParam("IN_ORDER_DETAIL_ID", queueRecord.getOrderDetailId());
      dataRequest.addInputParam("IN_POINT_OF_CONTACT_ID", queueRecord.getPointOfContactId());
      dataRequest.addInputParam("IN_EMAIL_ADDRESS", queueRecord.getEmailAddress());      
      dataRequest.addInputParam("IN_EXTERNAL_ORDER_NUMBER", queueRecord.getExternalOrderNumber() != null ? queueRecord.getExternalOrderNumber() : null);
      dataRequest.addInputParam("IN_MERCURY_ID", queueRecord.getMercuryId());
      
      
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
      String status = (String) outputs.get("OUT_STATUS");
      if(status != null && status.equalsIgnoreCase("N"))
      {
          String message = (String) outputs.get("OUT_MESSAGE");
          throw new Exception(message);
      }
  }

  public void doUpdateGlobalParms(String name, String value, String updatedBy) throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        
        dataRequest.setStatementID("UPDATE_GLOBAL_PARMS");
        dataRequest.addInputParam("IN_CONTEXT", "MESSAGE_GENERATOR_CONFIG");
        dataRequest.addInputParam("IN_NAME", name);
        dataRequest.addInputParam("IN_VALUE", value);
        dataRequest.addInputParam("IN_UPDATED_BY", updatedBy);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);      
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equals("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new SQLException(message);
        }
   }   
  
  public void updateConfirmationDetailsToPOC(String pocId, String emailSubj, String params) throws Exception
  {
    DataRequest dataRequest = new DataRequest();

    dataRequest.setConnection(connection);   
    dataRequest.setStatementID("UPDATE_CONFIRMATION_DETAILS");
    
    dataRequest.addInputParam("IN_POINT_OF_CONTACT_ID", pocId);
    dataRequest.addInputParam("IN_EMAIL_SUBJECT", emailSubj);
    dataRequest.addInputParam("IN_RECORD_ATTRIBUTES_XML", params);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();        
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);
    
    String status = (String) outputs.get("OUT_STATUS");
    if(status != null && status.equals("N"))
    {
        String message = (String) outputs.get("OUT_MESSAGE");
        throw new Exception(message);
    }
  }

  public HashMap getOrderInfoForPrint(String orderDetailId, String includeComments, String processingId) throws Exception
  {
    HashMap searchResults = new HashMap();
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("GET_ORDER_INFO_FOR_PRINT");

    dataRequest.addInputParam("IN_ORDER_DETAIL_ID",       orderDetailId);
    dataRequest.addInputParam("IN_INCLUDE_COMMENTS",        includeComments);
    dataRequest.addInputParam("IN_PROCESSING_ID",           processingId);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    searchResults = (HashMap) dataAccessUtil.execute(dataRequest);

    return searchResults;

  }

  public void updatePointOfContactRecord(PointOfContactVO pocVO) throws Exception {
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(connection);
      
      dataRequest.setStatementID("UPDATE_POINT_OF_CONTACT");
      dataRequest.addInputParam("IN_POINT_OF_CONTACT_ID", Long.toString(pocVO.getPointOfContactId()));
      dataRequest.addInputParam("IN_EMAIL_SUBJECT", pocVO.getEmailSubject());
      dataRequest.addInputParam("IN_BODY", pocVO.getBody());
      dataRequest.addInputParam("IN_UPDATED_BY", "MASSAGE_POC");
      
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      Map outputs = (Map) dataAccessUtil.execute(dataRequest);      
      String status = (String) outputs.get("OUT_STATUS");
      if(status != null && status.equals("N"))
      {
          String message = (String) outputs.get("OUT_MESSAGE");
          throw new SQLException(message);
      }
    
  }
}
