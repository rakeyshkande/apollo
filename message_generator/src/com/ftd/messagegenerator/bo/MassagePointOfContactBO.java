package com.ftd.messagegenerator.bo;

import java.sql.Connection;
import java.util.HashMap;

import org.w3c.dom.Document;

import com.ftd.messagegenerator.StockMessageGenerator;
import com.ftd.messagegenerator.dao.MessageGeneratorDAO;
import com.ftd.messagegenerator.vo.PointOfContactVO;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.XMLFormat;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

public class MassagePointOfContactBO {

    private Logger logger;
    
    public MassagePointOfContactBO()
    {
        logger = new Logger("com.ftd.messagegenerator.bo.MassagePointOfContactBO");
    }

    public void process(Connection con, PointOfContactVO pocVO) {
        logger.info("pocId: " + pocVO.getPointOfContactId());
        
        try {
            StockMessageGenerator smg = new StockMessageGenerator(con);
            MessageGeneratorDAO mgDAO = new MessageGeneratorDAO(con);
        
            Document primaryOrderXML = DOMUtil.getDocument();
            HashMap printOrderInfo = new HashMap();

            String orderDetailId = Long.toString(pocVO.getOrderDetailId());
            printOrderInfo = mgDAO.getOrderInfoForPrint(orderDetailId, "N", "CUSTOMER");

            DOMUtil.addSection(primaryOrderXML, buildXML(printOrderInfo, "OUT_ORDERS_CUR", "ORDERS", "ORDER").getChildNodes());
            DOMUtil.addSection(primaryOrderXML, buildXML(printOrderInfo, "OUT_ORDER_ADDON_CUR", "ORDER_ADDONS", "ORDER_ADDON").getChildNodes());
            DOMUtil.addSection(primaryOrderXML, buildXML(printOrderInfo, "OUT_ORDER_BILLS_CUR", "ORDER_BILLS", "ORDER_BILL").getChildNodes());
            DOMUtil.addSection(primaryOrderXML, buildXML(printOrderInfo, "OUT_ORDER_REFUNDS_CUR", "ORDER_REFUNDS", "ORDER_REFUND").getChildNodes());
            DOMUtil.addSection(primaryOrderXML, buildXML(printOrderInfo, "OUT_ORDER_TOTAL_CUR", "ORDER_TOTALS", "ORDER_TOTAL").getChildNodes());
            DOMUtil.addSection(primaryOrderXML, buildXML(printOrderInfo, "OUT_ORDER_PAYMENT_CUR", "ORDER_PAYMENTS", "ORDER_PAYMENT").getChildNodes());
            DOMUtil.addSection(primaryOrderXML, buildXML(printOrderInfo, "CUST_CART_CUR", "CUSTOMERS", "CUSTOMER").getChildNodes());
            DOMUtil.addSection(primaryOrderXML, buildXML(printOrderInfo, "OUT_CUSTOMER_PHONE_CUR", "CUSTOMER_PHONES", "CUSTOMER_PHONE").getChildNodes());
            DOMUtil.addSection(primaryOrderXML, buildXML(printOrderInfo, "OUT_MEMBERSHIP_CUR", "MEMBERSHIPS", "MEMBERSHIP").getChildNodes());
            DOMUtil.addSection(primaryOrderXML, buildXML(printOrderInfo, "OUT_TAX_REFUND_CUR", "TAX_REFUNDS", "TAX_REFUND").getChildNodes());

            pocVO.setDataDocument(primaryOrderXML);
            
            // Replace the tokens in the body
            pocVO = smg.buildMessage(pocVO);
            
            // Create the record_attributes_xml
            String recordAttributeXml = smg.generateRecordAttributeXMLFromOrderGuid(pocVO.getOrderGuid());
            pocVO.setRecordAttributesXML(recordAttributeXml);

            // get the sender email address
            pocVO.setSenderEmailAddress(mgDAO.loadSenderEmailAddress(pocVO.getLetterTitle(), pocVO.getCompanyId(),
                    pocVO.getOrderDetailId()));
            logger.info("senderEmailAddress: " + pocVO.getSenderEmailAddress());

            // Save the updated VO
            mgDAO.updatePointOfContactRecord(pocVO);

        } catch (Exception e) {
            logger.error("error: " + e);
        }
    }

    /*******************************************************************************************
    * buildXML()
    *******************************************************************************************/
    private Document buildXML(HashMap outMap, String cursorName, String topName, String bottomName)
          throws Exception
    {

      //Create a CachedResultSet object using the cursor name that was passed.
      CachedResultSet rs = (CachedResultSet) outMap.get(cursorName);

      //Create an XMLFormat, and initialize the top and bottom node.  Note that since this method
      //can be/is called by various other methods, the top and botton names MUST be passed to it.
      XMLFormat xmlFormat = new XMLFormat();
      xmlFormat.setAttribute("type", "element");
      xmlFormat.setAttribute("top", topName);
      xmlFormat.setAttribute("bottom", bottomName );

      return (Document) DOMUtil.convertToXMLDOM(rs, xmlFormat);

    }

}
