package com.ftd.messagegenerator.bo;

import java.net.URL;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Calendar;
import java.util.Map;

import com.ftd.messagegenerator.cache.handler.StockMessageTokenHandler;
import com.ftd.messagegenerator.StockMessageGenerator;
import com.ftd.messagegenerator.constants.MessageConstants;
import com.ftd.messagegenerator.dao.MessageGeneratorDAO;
import com.ftd.messagegenerator.mailer.ExactTargetMailer;
import com.ftd.messagegenerator.vo.PointOfContactVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.MessageTokenVO;
import com.ftd.osp.utilities.constants.VendorProductAvailabilityConstants;
import com.ftd.osp.utilities.plugins.Logger;

import org.apache.commons.lang.StringUtils;

/**
 * This class is responsible for constructing the appropriate ExactTarget (ET) email request message for an order 
 * and communicating with the ExactTarge Web Service (which actually sends the email to the recipient).
 */
public class PhoenixGeneratorBO {
    private static Logger logger = new Logger("com.ftd.messagegenerator.bo.PhoenixGeneratorBO"); 
    private String productId;
    private boolean addonChocolate;
    private boolean addonBear;
    
    // These following constants are defined here since there should be no need to reference them outside this class
   
    // Dynamic message property element names for ExactTarget Attributes.
    // These values should not be changed without coordination with ExactTarget.
    //
    private static final String MPROP_EMAIL_TYPE        = "cellId";
    private static final String MPROP_EMAIL_SUBJECT     = "emailSubjectTxt";
    private static final String MPROP_PROD_IMG_URL      = "productImgUrl";
    private static final String MPROP_ADDON_IMG1_URL    = "addonImg1Url";
    private static final String MPROP_ADDON_IMG2_URL    = "addonImg2Url";
    private static final String MPROP_DELIVERY_DATE_TXT = "deliveryDateTxt";
    private static final String MPROP_FOOTER1_TXT       = "footer1Txt";
    private static final String MPROP_FOOTER2_TXT       = "footer2Txt";
    
    // Constructor
    //
    public PhoenixGeneratorBO() {
        productId = null;
    }

    
    /**
     * Construct the appropriate ExactTarget request message for an order 
     * then invoke ExactTarget Web Service to send an email.
     * 
     * This is the only public method for this class.
     */
    public void prepareAndSendEmailRequest(Connection conn, PointOfContactVO pocObj) throws Exception {
        String emailTemplateKey = null;
        boolean isApologyEmail = false;
        boolean isDeliveryDateChange = false;
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        String tmpString = null;
        addonChocolate = false;
        addonBear = false;

        Calendar origDeliveryDate = pocObj.getDeliveryDate();
        Calendar newDeliveryDate  = pocObj.getNewDeliveryDate();

        // Products and any addons will be in body field of POC
        parseProductAndAddons(pocObj.getBody());  
        
        // Determine which email template to use
        //
        if (MessageConstants.PHOENIX_EMAIL_APOLOGY.equalsIgnoreCase(pocObj.getLetterTitle())) {
            emailTemplateKey = MessageConstants.PHOENIX_TEMPLATE_APOLOGY;
            isApologyEmail = true;
        } else if (MessageConstants.PHOENIX_EMAIL_STANDARD.equalsIgnoreCase(pocObj.getLetterTitle())) {
            if (newDeliveryDate != null && !newDeliveryDate.equals(origDeliveryDate)) {
                emailTemplateKey = MessageConstants.PHOENIX_TEMPLATE_DATE_CHANGE;
                isDeliveryDateChange = true;
            } else {
                emailTemplateKey = MessageConstants.PHOENIX_TEMPLATE_DEFAULT;
            }
        } else {
            throw new Exception("Unrecognized Phoenix template type, so ignoring: " + pocObj.getLetterTitle());
        }
        if (logger.isDebugEnabled()) {
            logger.debug("Phoenix email being prepared for: " + pocObj.getExternalOrderNumber() + 
                         " using template: " + emailTemplateKey);
        }
        
        // Build dynamic components of email message from order data and tokenized strings and add to hashmap
        //
        HashMap<String, String> paramMap = new HashMap<String, String>();
        paramMap.put(MPROP_EMAIL_TYPE, emailTemplateKey);
        
        // Email subject
        tmpString = configUtil.getContentWithFilter(conn, MessageConstants.PHOENIX_CONTEXT, 
                                                          MessageConstants.PHOENIX_EMAIL_SUBJECT_TEMPLATE, emailTemplateKey, null);
        paramMap.put(MPROP_EMAIL_SUBJECT, tmpString.replace(MessageConstants.PHOENIX_TOKEN_ORDER, pocObj.getExternalOrderNumber()));
        
        // Product image
        tmpString = configUtil.getFrpGlobalParm(MessageConstants.EXACT_TARGET_WS_CONTEXT, MessageConstants.PHOENIX_IMAGE_PROD_URL);
        paramMap.put(MPROP_PROD_IMG_URL, tmpString.replace(MessageConstants.PHOENIX_TOKEN_PROD_ID, productId));

        // Addon images (may or may not be present)
        if (addonBear || addonChocolate) {
            tmpString = configUtil.getFrpGlobalParm(MessageConstants.EXACT_TARGET_WS_CONTEXT, MessageConstants.PHOENIX_IMAGE_ADDON_URL);
            String bearUrl = tmpString.replace(MessageConstants.PHOENIX_TOKEN_ADDON, MessageConstants.PHOENIX_TOKEN_BEAR_ADDON);
            String chocUrl = tmpString.replace(MessageConstants.PHOENIX_TOKEN_ADDON, MessageConstants.PHOENIX_TOKEN_CHOC_ADDON);
            if (addonBear && addonChocolate) {
                paramMap.put(MPROP_ADDON_IMG1_URL, bearUrl);
                paramMap.put(MPROP_ADDON_IMG2_URL, chocUrl);
            } else if (addonBear) {
                paramMap.put(MPROP_ADDON_IMG1_URL, bearUrl);
            } else if (addonChocolate) {
                paramMap.put(MPROP_ADDON_IMG1_URL, chocUrl);
            }
        }
        
        // Delivery date (may or may not be present)
        if (isDeliveryDateChange) {
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            paramMap.put(MPROP_DELIVERY_DATE_TXT, sdf.format(newDeliveryDate.getTime()));
        }
        
        // Footers
        int valYear = Calendar.getInstance().get(Calendar.YEAR);
        StockMessageGenerator stockGen = new StockMessageGenerator(conn);

        tmpString = configUtil.getContentWithFilter(conn, MessageConstants.PHOENIX_CONTEXT, MessageConstants.PHOENIX_FOOTER_BAR_TEXT, emailTemplateKey, null);
        tmpString = stockGen.buildMessageSegmentByCompany(pocObj, tmpString);
        paramMap.put(MPROP_FOOTER1_TXT, tmpString);

        tmpString = configUtil.getContentWithFilter(conn, MessageConstants.PHOENIX_CONTEXT, MessageConstants.PHOENIX_FOOTER_END_TEXT, emailTemplateKey, null);
        tmpString = tmpString.replace(MessageConstants.PHOENIX_TOKEN_YEAR, String.valueOf(valYear));
        tmpString = tmpString.replace(MessageConstants.PHOENIX_TOKEN_EMAIL, pocObj.getRecipientEmailAddress());
        tmpString = stockGen.buildMessageSegmentByCompany(pocObj, tmpString);
        paramMap.put(MPROP_FOOTER2_TXT, tmpString);
        
        if (logger.isDebugEnabled()) {
            logger.debug("Dynamic email component values being sent to ExactTarget" + buildLogStr(paramMap));
        }

        // Call ExactTarget Web Service to process email request 
        //
        String emailTmpl = configUtil.getFrpGlobalParm(MessageConstants.MESSAGE_GEN_CONFIG_CONTEXT, MessageConstants.PHOENIX_TEMPLATE_ID);
        ExactTargetMailer etMailer = new ExactTargetMailer();
        // Pass null as Email 'from' address, so it uses default configured on ET side
        boolean success = etMailer.sendEmail(null, pocObj.getRecipientEmailAddress(), emailTmpl, paramMap, null);
        if (!success) {
            throw new Exception("ExactTarget Web Service call for Phoenix email did not return successfully");
        }

    }


    
    // Gets product ID and any addons from string of format:
    //    <product_id>[:addon[,addon]]   
    //    e.g., "6022:B,C"
    //
    private void parseProductAndAddons(String prodStr) throws Exception {
        String[] prodAndAddons = StringUtils.split(prodStr, ":");
        if (prodAndAddons == null || prodAndAddons.length < 1) {
            throw new Exception("Product ID was not specified");
        }
        productId = prodAndAddons[0];
        if (prodAndAddons.length > 1) {
            String[] addons = StringUtils.split(prodAndAddons[1], ",");
            for (int i=0; i<addons.length; i++) {
                if (MessageConstants.PHOENIX_TOKEN_CHOC_ADDON.equalsIgnoreCase(StringUtils.strip(addons[i]))) {
                    addonChocolate = true;
                } else if (MessageConstants.PHOENIX_TOKEN_BEAR_ADDON.equalsIgnoreCase(StringUtils.strip(addons[i]))) {
                    addonBear = true;
                } else {
                    logger.error("Addon '" + addons[i] + "' not recognized, so ignoring and continuing");
                }
            }
        }
    }
    
    
    // Add to logValues string so we can dump values we created for ExactTarget to log
    //
    private String buildLogStr(HashMap<String,String> paramMap) {
        StringBuffer logValues = new StringBuffer();
        for (Map.Entry<String,String> entry : paramMap.entrySet()) {
            logValues.append(entry.getKey());
            logValues.append("='");
            logValues.append(entry.getValue());
            logValues.append("' ");                
        }
        return logValues.toString();
    }
}
