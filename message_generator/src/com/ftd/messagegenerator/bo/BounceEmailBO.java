package com.ftd.messagegenerator.bo;

import java.sql.Connection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import com.ftd.messagegenerator.constants.MessageConstants;
import com.ftd.messagegenerator.dao.MessageGeneratorDAO;
import com.ftd.messagegenerator.dao.OrderEmailDAO;
import com.ftd.messagegenerator.mailer.ExactTargetMailer;
import com.ftd.messagegenerator.vo.PointOfContactVO;
import com.ftd.messagegenerator.vo.QueueVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

public class BounceEmailBO
{
 	private final static String SYSTEM_NAME = "MsgGen";
    private static Logger logger = new Logger("com.ftd.messagegenerator.bo.BounceEmailBO"); 
 	
	public void processBounce(Connection conn) throws Exception
	{
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss"); 

		OrderEmailDAO oeDAO = new OrderEmailDAO(conn);	  
		String last_bounce_batch_time = oeDAO.getGlobalParm(MessageConstants.MESSAGE_GEN_CONFIG_CONTEXT, MessageConstants.LAST_BOUNCE_EMAIL_BATCH_TIME); 	

    	String separateCompanies = oeDAO.getGlobalParm(MessageConstants.MESSAGE_GEN_CONFIG_CONTEXT,
		        "EXACT_TARGET_SEPARATE_COMPANIES");
        String[] companyList = separateCompanies.split(" ");
        HashMap<String, String> companyMap = new HashMap<String, String>();
        for (int a=0; a<companyList.length; a++) {
	        companyMap.put(companyList[a], companyList[a]);
        }
        // insert a null key for the existing logic
        companyMap.put(null, null);

		Iterator<String> companyIt = companyMap.keySet().iterator();
        while (companyIt.hasNext()) {
        	String companyKey = companyIt.next();
        	String companyId = companyMap.get(companyKey);
        	logger.info("Processing companyId = " + companyId);
    		HashMap<String,String> bounceEmailTemplateMaps = oeDAO.getBounceEmailTemplateIds(companyId);
    		if(bounceEmailTemplateMaps != null && bounceEmailTemplateMaps.size() > 0)
    		{
    			HashMap<String, ArrayList<String>> myMap = new HashMap<String, ArrayList<String>>();
    			Set<String> templateIdSet = bounceEmailTemplateMaps.keySet();
    			for(String templateId : templateIdSet) {
    				String customerKey = bounceEmailTemplateMaps.get(templateId);
    				logger.info(templateId + " " + customerKey);
    	            if (companyId != null && templateId.indexOf(companyId) > -1) {
    	            	templateId = templateId.substring(0, templateId.indexOf(companyId)-1);
    	            }
    				ArrayList<String> tempList = myMap.get(customerKey);
    				if (tempList == null) {
    					tempList = new ArrayList<String>();
    				}
    				tempList.add(templateId);
    				myMap.put(customerKey, tempList);
    			}

    			Iterator<String> it = myMap.keySet().iterator();
    			while (it.hasNext()) {
    				String customerKey = (String) it.next();
    				ArrayList<String> tempList = myMap.get(customerKey);

    				StringBuffer templateSB = new StringBuffer();
    				for (int ii=0; ii<tempList.size(); ii++) {
    					if (templateSB != null && templateSB.length() > 0) {
    						templateSB.append(", ");
    					}
    					templateSB.append("'" + tempList.get(ii) + "'");
    				}
    				String templateList = templateSB.toString();
    				logger.info(customerKey + " / " + templateList);

    				//Make call to ET to get the ObjectId based on TemplateId which is customerKey
    				ExactTargetMailer etMailer = new ExactTargetMailer();
    				String objectId = etMailer.getObjectIdByCustomerKey(customerKey, companyId);
    				logger.info("Template List = "+ templateList + " Object ID = "+ objectId);
				
    				//Using objectId make call to ET to get the email address based on the last_bounce_batch_time which is set in global_parms
    				GregorianCalendar gc = new GregorianCalendar();     
    				gc.setTime( dateFormat.parse(last_bounce_batch_time));  
    				logger.info("Last Bounce batch time ="+ last_bounce_batch_time);
    				ArrayList<String>  bounceEmailAddressList = etMailer.getBouncedEmailAddresses(objectId, gc, companyId);

    				//for each email address and templateId pull the record from clean.point_of_contact
    				if(bounceEmailAddressList != null && bounceEmailAddressList.size() > 0) {
    					processList(conn, templateList, bounceEmailAddressList);
    				}
				
    				ArrayList<String>  notSentEmailAddressList = etMailer.getNotSentEmailAddresses(objectId, gc, companyId);
    				if (notSentEmailAddressList != null && notSentEmailAddressList.size() > 0) {
    					processList(conn, templateList, notSentEmailAddressList);
    				}

    			}
    		}
		}
	}
	
	private void processList(Connection conn, String templateList, ArrayList<String> processList) throws Exception {

        ConfigurationUtil cu = ConfigurationUtil.getInstance();
        String senderEmailAddress = cu.getContentWithFilter(conn, "MESSAGE_GENERATOR", "BOUNCED_EMAIL_FROM_ADDRESS", null, null);
        String newSubject = cu.getContentWithFilter(conn, "MESSAGE_GENERATOR", "BOUNCED_EMAIL_SUBJECT", null, null);
        String newBody = cu.getContentWithFilter(conn, "MESSAGE_GENERATOR", "BOUNCED_EMAIL_BODY", null, null);

		MessageGeneratorDAO dao = new MessageGeneratorDAO(conn);


		for(String emailAddress: processList) {					 
			//we might get multiple poc if it is multicart. Then we need to process each of them and add it to queue.
			logger.info("email address which bounced = "+ emailAddress + " and template list = " + templateList);
			ArrayList<PointOfContactVO> pocList = (ArrayList<PointOfContactVO>)dao.getPocByRecipientEmail(emailAddress, templateList);
			if(pocList != null && pocList.size() > 0 )
			{
				for(PointOfContactVO pocVo : pocList)
				{
					logger.info("found record: " + pocVo.getPointOfContactId() + " " +
							pocVo.getExternalOrderNumber());
					String pocId = new Long(pocVo.getPointOfContactId()).toString();

					//Insert inbound record into POC								
					String tempBody = newBody + pocVo.getBody();
					tempBody = tempBody.replaceAll("~emailAddress~", pocVo.getRecipientEmailAddress());
					tempBody = tempBody.replaceAll("~subject~", pocVo.getEmailSubject());
					pocVo.setBody(tempBody);
					pocVo.setSentReceivedIndicator("I");		
					pocVo.setEmailSubject(newSubject);
					pocVo.setSenderEmailAddress(senderEmailAddress);
					String sequenceNumber = dao.insertPointOfContact(pocVo);
												
					//Insert into queue
					QueueVO queueVO = new QueueVO();
					queueVO.setExternalOrderNumber(pocVo.getExternalOrderNumber());
					queueVO.setMasterOrderNumber(pocVo.getMasterOrderNumber());
					queueVO.setOrderGuid(pocVo.getOrderGuid());
					if (pocVo.getOrderDetailId() > 0) {
					    queueVO.setOrderDetailId(Long.toString(pocVo.getOrderDetailId()));
					}
					queueVO.setQueueType("OT");
					queueVO.setMessageType("OT");
					queueVO.setPointOfContactId(sequenceNumber);
					queueVO.setMessageTimestamp(new Date());
					queueVO.setEmailAddress(pocVo.getRecipientEmailAddress());
					//queueVO.setSystem(SYSTEM_NAME);
					
					dao.insertQueueRecord(queueVO);

					//update the existing poc bounce_processed_flag to Y.		
					dao.updatePOCBounceProcessedFlag(pocId, "Y", SYSTEM_NAME);		
				}
			}				  
		}			  

	}

}
