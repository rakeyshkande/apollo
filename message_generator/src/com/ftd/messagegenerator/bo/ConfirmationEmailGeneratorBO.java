package com.ftd.messagegenerator.bo;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.ftd.messagegenerator.bo.helper.OrderEmailHelper;
import com.ftd.messagegenerator.constants.BuildEmailConstants;
import com.ftd.messagegenerator.constants.OrderEmailConstants;
import com.ftd.messagegenerator.vo.PointOfContactVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.PartnerUtility;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.SourceMasterVO;
import com.ftd.osp.utilities.email.EmailVO;
import com.ftd.osp.utilities.order.MercentOrderPrefixes;
import com.ftd.osp.utilities.order.RecalculateOrderBO;
import com.ftd.osp.utilities.order.ScrubMapperDAO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.PartnerMappingVO;
import com.ftd.osp.utilities.xml.DOMUtil;


public class ConfirmationEmailGeneratorBO
{
  OrderEmailHelper helper = null;
  private Logger logger;

  public ConfirmationEmailGeneratorBO()
  {
    //logger
    logger = new Logger("com.ftd.messagegenerator.bo.ConfirmationEmailGeneratorBO");
  }

  public EmailVO process(Connection connection,PointOfContactVO pocObj) throws Exception
  {
	  if (logger.isDebugEnabled()) {
          logger.debug("Starting to process order GUID:" );
	  }

      helper = new OrderEmailHelper(connection);
      ScrubMapperDAO dao = new ScrubMapperDAO(connection);
      OrderVO order = this.prepareOrder(dao.mapOrderFromDB(pocObj.getOrderGuid()),connection);

      EmailVO emailVO = this.prepareEmail(order,connection);

      OrderEmailHelper helper = new OrderEmailHelper(connection);
      helper.updateEmailOrderTokens(emailVO, order);
      emailVO = helper.performSpecialFormatting(emailVO);

      //Q2SP17-27
      if(Integer.parseInt(order.getProductsTotal()) > 1)
      {
    	  emailVO.setOrderNumber(order.getMasterOrderNumber());
      }
      //Q2SP17-8
      else
      {
	      List<OrderDetailsVO> orderDetailsVO  = order.getOrderDetail();
	      for(OrderDetailsVO orderDetail : orderDetailsVO)
	      {
	    	  emailVO.setOrderNumber(orderDetail.getExternalOrderNumber());
	      }
      }
      return emailVO;
  }

  public EmailVO prepareEmail(OrderVO order,Connection connection) throws Exception
  {

    String contactInformationMessageText;
    String contactInformationMessageHtml;
    String headerText;
    String headerHtml;
    HashMap wlGlobalMap = new HashMap();

    boolean isMercentOrder = false;
    MercentOrderPrefixes mercentOrderPrefixes = new MercentOrderPrefixes(connection);
	if (mercentOrderPrefixes != null && order.getOrderOrigin() != null && mercentOrderPrefixes.isMercentOrder(order.getOrderOrigin())) {
		logger.info("The order is mercent order. Skip Body and manage spaces.");
		isMercentOrder = true;
	}

    EmailVO emailVO = new EmailVO();

	Document attributeDocument = DOMUtil.getDefaultDocument();
	Element attributesElement = attributeDocument.createElement("attributes");
	attributeDocument.appendChild(attributesElement);

    /**************Issue 3113 - start*************/
    OrderEmailHelper oeHelper = new OrderEmailHelper();
    String wlUrl="";

    //retrieve Source code information.
    SourceMasterVO sVO = oeHelper.retrieveSourceCodeInfo(connection,  order.getSourceCode());
    if (sVO == null)
      throw new Exception ("Source code was not found");

    //find out if the source code was for webloyalty
    boolean webloyaltyOrder = sVO.getWebloyaltyFlag().equalsIgnoreCase("Y")?true:false;

    //if webloyalty order, process for webloyalty.
    if (webloyaltyOrder)
    {
      wlUrl = oeHelper.processWebloyalty(connection, order, OrderEmailConstants.CONFIRMATION_USER);

      //retrieve the global parms
      wlGlobalMap = oeHelper.retrieveWebloyaltyGlobalParms(connection);

    }
    /**************Issue 3113 - end*************/

      String buyerEmail = helper.extractBuyerEmail(order);
      if(buyerEmail == null || buyerEmail.equals(""))
      throw new Exception("Order " + order.getGUID() + "does not have buyer email");

      Map sections = helper.getEmailSectionContent(order);
      emailVO.setRecipient(buyerEmail);
      emailVO.setSender((String)sections.get(BuildEmailConstants.FROM_ADDRESS));
      emailVO.setSubject((String)sections.get(BuildEmailConstants.SUBJECT_CONTEXT));

      emailVO.appendHTMLContent(helper.startGlobalHTML());
      //HEADER
      //As part of 7191, it was decided that the header section may also contain tokens, like sourcecode.
      headerText = (String)sections.get(BuildEmailConstants.HEADER_CONTENT);
      headerHtml = (String)sections.get(BuildEmailConstants.HEADER_CONTENT_HTML);

      //replace tokens
      headerText = oeHelper.replaceOrderTokens(connection, headerText, "TEXT", order);
      headerHtml = oeHelper.replaceOrderTokens(connection, headerHtml, "HTML", order );

      emailVO.appendContent(headerText);
      emailVO.appendHTMLContent(OrderEmailConstants.SPAN_HEADER);
      emailVO.appendHTMLContent(headerHtml);
      emailVO.appendHTMLContent(OrderEmailConstants.SPAN_END);

	  Element attributeElement = attributeDocument.createElement("attribute");
	  attributesElement.appendChild(attributeElement);
	  Element nameElement = attributeDocument.createElement("name");
	  nameElement.appendChild(attributeDocument.createTextNode(OrderEmailConstants.TAG_HEADER_CONTENT));
	  attributeElement.appendChild(nameElement);
	  Element valueElement = attributeDocument.createElement("value");
	  valueElement.appendChild(attributeDocument.createCDATASection(headerHtml));
	  attributeElement.appendChild(valueElement);

      //BIN changed message
      binCheck:
        if (order.getOrderDetail() != null)
        {
          for (int i = 0; i < order.getOrderDetail().size(); i++)
          {
            OrderDetailsVO od = ((OrderDetailsVO) order.getOrderDetail().get(i));
            if(od != null && od.getBinSourceChangedFlag() != null && od.getBinSourceChangedFlag().equalsIgnoreCase("Y"))
            {
              ConfigurationUtil cu = ConfigurationUtil.getInstance();
              String binChangeConfirmationHTML  = cu.getContentWithFilter(connection, BuildEmailConstants.BIN_CONTEXT,
                                                                          BuildEmailConstants.CONFIRMATION_EMAIL_TEXT, BuildEmailConstants.HTML, null);
              String binChangeConfirmationText  = cu.getContentWithFilter(connection, BuildEmailConstants.BIN_CONTEXT,
                                                                          BuildEmailConstants.CONFIRMATION_EMAIL_TEXT, BuildEmailConstants.TEXT_FILTER, null);
              if (binChangeConfirmationHTML != null && binChangeConfirmationText != null)
              {
                emailVO.appendContent("\n");
                emailVO.appendContent(binChangeConfirmationText);
                emailVO.appendHTMLContent(OrderEmailConstants.SPAN_BIN_CHANGE);
                emailVO.appendHTMLContent(binChangeConfirmationHTML);
                emailVO.appendHTMLContent(OrderEmailConstants.SPAN_END);

          	    attributeElement = attributeDocument.createElement("attribute");
        	    attributesElement.appendChild(attributeElement);
        	    nameElement = attributeDocument.createElement("name");
        	    nameElement.appendChild(attributeDocument.createTextNode(OrderEmailConstants.TAG_BIN_CHANGE_CONTENT));
        	    attributeElement.appendChild(nameElement);
        	    valueElement = attributeDocument.createElement("value");
        	    valueElement.appendChild(attributeDocument.createCDATASection(binChangeConfirmationHTML));
        	    attributeElement.appendChild(valueElement);

        		if (logger.isDebugEnabled()) {
                    logger.debug("Inserting BIN changed message into confirmation email");
                    logger.debug("Text version: " + binChangeConfirmationText);
                    logger.debug("HTML version: " + binChangeConfirmationHTML);
        		}
              }
              else
              {
                if (binChangeConfirmationText == null)
                {
                  logger.error("Could not find message to insert into email for bin change." +
                    "Context: " + BuildEmailConstants.BIN_CONTEXT + ", " + BuildEmailConstants.CONFIRMATION_EMAIL_TEXT +
                    " Fliter: " +BuildEmailConstants.TEXT_FILTER);
                }

                if (binChangeConfirmationHTML == null)
                {
                  logger.error("Could not find message to insert into email for bin change." +
                    "Context: " + BuildEmailConstants.BIN_CONTEXT + ", " + BuildEmailConstants.CONFIRMATION_EMAIL_TEXT +
                    "Fliter: " +BuildEmailConstants.HTML);
                }
              }

              break binCheck;
            }
          }
        }
//WEBLOYALTY - FIRST LINK
      if (webloyaltyOrder)
      {
        //append content
        //note that we no longer will put the bracketed html in the text emails due to rendering issues.
        emailVO.appendContent("\n\n");
        emailVO.appendContent((String)wlGlobalMap.get(OrderEmailConstants.HK_EMAIL_SLOGAN_1));
        emailVO.appendContent("\n");
        emailVO.appendContent(wlUrl);
        emailVO.appendContent("\n");

        //append html content
        emailVO.appendHTMLContent(OrderEmailConstants.SPAN_LOYALTY_1);
        emailVO.appendHTMLContent("<BR><BR>");
        emailVO.appendHTMLContent("<a href=");
        emailVO.appendHTMLContent(wlUrl);
        emailVO.appendHTMLContent(">");
        emailVO.appendHTMLContent((String)wlGlobalMap.get(OrderEmailConstants.HK_EMAIL_SLOGAN_1));
        emailVO.appendHTMLContent("</a>");
        emailVO.appendHTMLContent("<BR>");
        emailVO.appendHTMLContent(OrderEmailConstants.SPAN_END);
      }

//ORDER INFORMATION
      emailVO.appendHTMLContent(OrderEmailConstants.SPAN_ORDER_DETAIL);
      if(order.getMasterOrderNumber().startsWith("B"))
      {
        emailVO.appendContent((String)helper.getBulkOrderInfoSection(order).get(BuildEmailConstants.PLAIN_TEXT));
        emailVO.appendHTMLContent((String)helper.getBulkOrderInfoSection(order).get(BuildEmailConstants.HTML));
      }
      else
      {		// #18531 - For mercent orders it should be skipped.
			if (!isMercentOrder) {
				Map sectionMap = helper.getOrderInfoSection(order);
				emailVO.appendContent((String) sectionMap.get(BuildEmailConstants.PLAIN_TEXT));
				emailVO.appendHTMLContent((String) sectionMap.get(BuildEmailConstants.HTML));
				String orderDetailsXML = (String) sectionMap.get(BuildEmailConstants.ORDER_DETAILS_XML);

				attributeElement = attributeDocument.createElement("attribute");
				attributesElement.appendChild(attributeElement);
				nameElement = attributeDocument.createElement("name");
				nameElement.appendChild(attributeDocument.createTextNode(OrderEmailConstants.TAG_ORDER_DETAILS));
				attributeElement.appendChild(nameElement);
				valueElement = attributeDocument.createElement("value");
				valueElement.appendChild(attributeDocument.createCDATASection(orderDetailsXML));
				attributeElement.appendChild(valueElement);
			}
      }
      emailVO.appendHTMLContent(OrderEmailConstants.SPAN_END);


//WEBLOYALTY - SECOND LINK
      if (webloyaltyOrder)
      {
        //append content
        //note that we no longer will put the bracketed html in the text emails due to rendering issues.
        emailVO.appendContent("\n\n");
        emailVO.appendContent((String)wlGlobalMap.get(OrderEmailConstants.HK_EMAIL_SLOGAN_2));
        emailVO.appendContent("\n");
        emailVO.appendContent(wlUrl);
        emailVO.appendContent("\n");

        //append html content
        emailVO.appendHTMLContent(OrderEmailConstants.SPAN_LOYALTY_2);
        emailVO.appendHTMLContent("<BR><BR>");
        emailVO.appendHTMLContent("<a href=");
        emailVO.appendHTMLContent(wlUrl);
        emailVO.appendHTMLContent(">");
        emailVO.appendHTMLContent((String)wlGlobalMap.get(OrderEmailConstants.HK_EMAIL_SLOGAN_2));
        emailVO.appendHTMLContent("</a>");
        emailVO.appendHTMLContent("<BR>");
        emailVO.appendHTMLContent(OrderEmailConstants.SPAN_END);
      }

//GUARANTEE

      emailVO.appendContent("\n");
      emailVO.appendContent((String)sections.get(BuildEmailConstants.GUARANTEE_CONTENT));
      emailVO.appendHTMLContent(OrderEmailConstants.SPAN_GUARANTEE);
      emailVO.appendHTMLContent((String)sections.get(BuildEmailConstants.GUARANTEE_CONTENT_HTML));
      emailVO.appendHTMLContent(OrderEmailConstants.SPAN_END);

	  attributeElement = attributeDocument.createElement("attribute");
	  attributesElement.appendChild(attributeElement);
	  nameElement = attributeDocument.createElement("name");
	  nameElement.appendChild(attributeDocument.createTextNode(OrderEmailConstants.TAG_GUARANTEE_CONTENT));
	  attributeElement.appendChild(nameElement);
	  valueElement = attributeDocument.createElement("value");
	  valueElement.appendChild(attributeDocument.createCDATASection((String)sections.get(BuildEmailConstants.GUARANTEE_CONTENT_HTML)));
	  attributeElement.appendChild(valueElement);

//MARKETING
      emailVO.appendContent("\n");
      emailVO.appendContent("\n");
      emailVO.appendHTMLContent(OrderEmailConstants.SPAN_MARKETING);
      if (!isMercentOrder) {
    	  emailVO.appendHTMLContent("<BR>");
    	  emailVO.appendHTMLContent("<BR>");
      }
      emailVO.appendContent((String)sections.get(BuildEmailConstants.MARKETING_CONTENT));
      emailVO.appendHTMLContent((String)sections.get(BuildEmailConstants.MARKETING_CONTENT_HTML));
      emailVO.appendHTMLContent(OrderEmailConstants.SPAN_END);

	  attributeElement = attributeDocument.createElement("attribute");
	  attributesElement.appendChild(attributeElement);
	  nameElement = attributeDocument.createElement("name");
	  nameElement.appendChild(attributeDocument.createTextNode(OrderEmailConstants.TAG_MARKETING_CONTENT));
	  attributeElement.appendChild(nameElement);
	  valueElement = attributeDocument.createElement("value");
	  valueElement.appendChild(attributeDocument.createCDATASection((String)sections.get(BuildEmailConstants.MARKETING_CONTENT_HTML)));
	  attributeElement.appendChild(valueElement);

//CONTACT US
      emailVO.appendContent("\n");
      emailVO.appendContent("\n");
      emailVO.appendHTMLContent(OrderEmailConstants.SPAN_CONTACT);
      if (!isMercentOrder) {
    	  emailVO.appendHTMLContent("<BR>");
    	  emailVO.appendHTMLContent("<BR>");
      }

      contactInformationMessageText = ((String)sections.get(BuildEmailConstants.CONTACT_CONTENT));
      contactInformationMessageHtml = ((String)sections.get(BuildEmailConstants.CONTACT_CONTENT_HTML));

      //replace tokens
      contactInformationMessageText = oeHelper.replaceOrderTokens(connection, contactInformationMessageText, "TEXT", order);
      contactInformationMessageHtml = oeHelper.replaceOrderTokens(connection, contactInformationMessageHtml, "HTML", order );

      emailVO.appendContent(contactInformationMessageText);
      emailVO.appendHTMLContent(contactInformationMessageHtml);
      emailVO.appendHTMLContent(OrderEmailConstants.SPAN_END);

	  attributeElement = attributeDocument.createElement("attribute");
	  attributesElement.appendChild(attributeElement);
	  nameElement = attributeDocument.createElement("name");
	  nameElement.appendChild(attributeDocument.createTextNode(OrderEmailConstants.TAG_CONTACT_CONTENT));
	  attributeElement.appendChild(nameElement);
	  valueElement = attributeDocument.createElement("value");
	  valueElement.appendChild(attributeDocument.createCDATASection(contactInformationMessageHtml));
	  attributeElement.appendChild(valueElement);

      //free-shipping indicator
        boolean freeshippingFlag = false;
        if (order.getOrderDetail() != null)
        {
        	for (int i = 0; i < order.getOrderDetail().size(); i++)
            {
              OrderDetailsVO od = ((OrderDetailsVO) order.getOrderDetail().get(i));
              if(od != null && "Y".equalsIgnoreCase(od.getFreeShipping()))
              {
          		  if (logger.isDebugEnabled()) {
            	      logger.debug("FreeShipping :"+od.getFreeShipping());
          		  }
            	  freeshippingFlag = true;
                  break;
              }
            }
        }
        String goldMembCartInd = freeshippingFlag?"Y":"N";
        emailVO.appendHTMLContent(OrderEmailConstants.LABEL_GOLD_MEMBERSHIP_CART_INDICATOR);
        emailVO.appendHTMLContent(goldMembCartInd);
        emailVO.appendHTMLContent(OrderEmailConstants.LABEL_END);

  	  attributeElement = attributeDocument.createElement("attribute");
	  attributesElement.appendChild(attributeElement);
	  nameElement = attributeDocument.createElement("name");
	  nameElement.appendChild(attributeDocument.createTextNode(BuildEmailConstants.NAME_GOLD_MEMBERSHIP_FLAG));
	  attributeElement.appendChild(nameElement);
	  valueElement = attributeDocument.createElement("value");
	  valueElement.appendChild(attributeDocument.createCDATASection(goldMembCartInd));
	  attributeElement.appendChild(valueElement);

      String sourceType = order.getSourceType();
      if (logger.isDebugEnabled()) {
          logger.debug("sourceType: " + sourceType);
      }
      if (sourceType != null) {
    	  attributeElement = attributeDocument.createElement("attribute");
    	  attributesElement.appendChild(attributeElement);
    	  nameElement = attributeDocument.createElement("name");
    	  nameElement.appendChild(attributeDocument.createTextNode(BuildEmailConstants.NAME_SOURCE_TYPE));
    	  attributeElement.appendChild(nameElement);
    	  valueElement = attributeDocument.createElement("value");
    	  valueElement.appendChild(attributeDocument.createCDATASection(sourceType));
    	  attributeElement.appendChild(valueElement);
      }
      String partnerId = null;
	  if (logger.isDebugEnabled()) {
          logger.debug("partnerId: " + order.getPartnerId());
          logger.debug("partnerName: " + order.getPartnerName());
	  }
      if (order.getPartnerName() != null) {
      	partnerId = order.getPartnerName();
      }
		if(partnerId == null && isMercentOrder) {
				partnerId = order.getOrderOrigin();
		}

		if(partnerId == null) {
			PartnerMappingVO partnerMappingVO = new PartnerUtility().getPartnerOriginsInfo(order.getOrderOrigin(),order.getSourceCode(), connection);
			if(partnerMappingVO != null && partnerMappingVO.isDefaultBehaviour()) {
				partnerId = order.getOrderOrigin();
			}
		}
	  if (logger.isDebugEnabled()) {
          logger.debug("partnerId: " + partnerId);
	  }
      if (partnerId != null) {
    	  attributeElement = attributeDocument.createElement("attribute");
    	  attributesElement.appendChild(attributeElement);
    	  nameElement = attributeDocument.createElement("name");
    	  nameElement.appendChild(attributeDocument.createTextNode(BuildEmailConstants.NAME_PARTNER_ID));
    	  attributeElement.appendChild(nameElement);
    	  valueElement = attributeDocument.createElement("value");
    	  valueElement.appendChild(attributeDocument.createCDATASection(partnerId));
    	  attributeElement.appendChild(valueElement);
      }
	  StringWriter sw = new StringWriter();
	  PrintWriter pw = new PrintWriter(sw);
	  DOMUtil.print(attributeDocument, pw);
	  emailVO.setRecordAttributesXML(sw.toString());

      emailVO.appendHTMLContent(helper.closeGlobalHTML());
      emailVO.setContentType(emailVO.MULTIPART);

	  return emailVO;
  }

  private OrderVO prepareOrder(OrderVO order,Connection connection) throws Exception
  {
      for (int i = 0; i < order.getOrderDetail().size(); i++)
      {
         if(!((OrderDetailsVO) order.getOrderDetail().get(i)).getStatus().equals("2006") &&
            !((OrderDetailsVO) order.getOrderDetail().get(i)).getStatus().equals("2007") )
         {
               logger.info("Items in cart:" +  order.getOrderDetail().size());
               logger.info("Removing order detail " + ((OrderDetailsVO) order.getOrderDetail().get(i)).getOrderDetailId() + " with status " + ((OrderDetailsVO) order.getOrderDetail().get(i)).getStatus());
               order.getOrderDetail().remove(i);
               i--;
         }
      }
      // Recalculate order with new item list
      if(order.isPartnerOrder()) {
          logger.info("Skipping recalculateOrder since Partner order");
      } else if (order.getOrderOrigin().equalsIgnoreCase("roses")) {
          logger.info("Skipping recalculateOrder since Roses.com web order");
      } else if(order.getOrderCalcType() != null && order.getOrderCalcType().equalsIgnoreCase("CALYX")) {
    	  logger.info("Skipping recalculateOrder since New Website(Calyx) order");
      } else {
    	  //SP-101 setting InvokedAsynchronously as true in order to re-process this message in MDB level in case of CAMSTimeout Exception.
    	  order.setInvokedAsynchronously(true);
          RecalculateOrderBO recalculateOrder = new RecalculateOrderBO();
          recalculateOrder.recalculate(connection, order);
      }
      return order;
  }

}