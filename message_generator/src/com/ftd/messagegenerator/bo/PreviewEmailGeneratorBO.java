package com.ftd.messagegenerator.bo;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


import com.ftd.messagegenerator.bo.helper.OrderEmailHelper;
import com.ftd.messagegenerator.constants.BuildEmailConstants;
import com.ftd.messagegenerator.constants.OrderEmailConstants;
import com.ftd.messagegenerator.utilities.Utils;
import com.ftd.osp.utilities.PartnerUtility;
import com.ftd.osp.utilities.email.EmailVO;
import com.ftd.messagegenerator.vo.PointOfContactVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.MercentOrderPrefixes;
import com.ftd.osp.utilities.order.ScrubMapperDAO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.PartnerMappingVO;
import com.ftd.osp.utilities.xml.DOMUtil;


/**
 * This object handles requests to send a preview email.  This object receives
 * an email guid, retrieves the email from the databse, and then sends the email.
 */
public class PreviewEmailGeneratorBO 
{

  private Logger logger;

  /** Constructor **/
  public PreviewEmailGeneratorBO()
  {
    //logger
    logger = new Logger("com.ftd.messagegenerator.bo.PreviewEmailGeneratorBO");  
  }

  /** The main method of the handler **/
  public EmailVO process(Connection connection,PointOfContactVO pocObj) throws Exception
  {
	  
	  OrderVO order = null;
	  
	  String orderGuid = pocObj.getOrderGuid();
	 
		  
	  SimpleDateFormat sdfTime = new SimpleDateFormat("MM/dd/yyyy");	
	  String runDate = sdfTime.format(pocObj.getDeliveryDate().getTime());
	  String comments = pocObj.getCommentText();
	  String toAddress = pocObj.getRecipientEmailAddress();
	  String programId = pocObj.getBody();
	  logger.info("programID: " + programId + " " + runDate);

      OrderEmailHelper helper = new OrderEmailHelper(connection);

      //retrieve order from database        
      ScrubMapperDAO scrubMapperDAO = new ScrubMapperDAO(connection);
      order = scrubMapperDAO.mapOrderFromDB(orderGuid);

      //if there there are no order details then assume the order does not exist in DB
      if(order.getOrderDetail() == null || order.getOrderDetail().size() <= 0)
      {
        String msg = "Order GUID does not exist.  Update Email Admin config file with a vaild order. (" + orderGuid + ")";
        logger.error(msg);
        throw new Exception(msg);
      }

      boolean isMercentOrder = false;
      MercentOrderPrefixes mercentOrderPrefixes = new MercentOrderPrefixes(connection);
  	  if (mercentOrderPrefixes != null && order.getOrderOrigin() != null && mercentOrderPrefixes.isMercentOrder(order.getOrderOrigin())) {
  		  logger.info("The order is mercent order. Skip Body and manage spaces.");
  		  isMercentOrder = true;
  	  }

      // Get the program information
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(connection);
      dataRequest.setStatementID("FRP.VIEW_CONTENT_BY_PROGRAM");
      dataRequest.addInputParam("IN_PROGRAM_ID", new Long(programId));  //for preview email we are saving the program id in the body.
      dataRequest.addInputParam("IN_DATE",Utils.getSQLDate(runDate));
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      CachedResultSet rs = new CachedResultSet();
      rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
      if(rs == null || rs.getRowCount() <= 0)
      {
        throw new Exception("Program information does not exist for the entered date.");
      }

      //Place data from recordset into map
      Map<String,String> sectionContentMap = getEmailSectionContent(rs);

      //Add preview comments
      String headerContent = (String)sectionContentMap.get(BuildEmailConstants.HEADER_CONTENT);
      String headerContentHtml = (String)sectionContentMap.get(BuildEmailConstants.HEADER_CONTENT_HTML);
      headerContent = comments == null? headerContent : comments + "\n" + headerContent;
      headerContentHtml = comments == null? headerContentHtml : "<p>" + comments + "</p>" + headerContentHtml;
      sectionContentMap.put(BuildEmailConstants.HEADER_CONTENT, headerContent);
      sectionContentMap.put(BuildEmailConstants.HEADER_CONTENT_HTML, headerContentHtml);

      //build email object
      EmailVO email = new EmailVO();
      email.setRecipient(toAddress);
      email.setSender((String)sectionContentMap.get(BuildEmailConstants.FROM_ADDRESS));
      email.setSubject((String)sectionContentMap.get(BuildEmailConstants.SUBJECT_CONTEXT));

     //section info
     Map<String,String> infoSectionMap = helper.getOrderInfoSection(order);

      //text content
      email.appendContent((String)sectionContentMap.get(BuildEmailConstants.HEADER_CONTENT));
      //email.appendContent("\n");
      if(connection != null && new MercentOrderPrefixes(connection).isMercentOrder(order.getOrderOrigin())){
      	logger.info("Is mercent order skip body message!!");
      } else {
      	email.appendContent((String)infoSectionMap.get(BuildEmailConstants.PLAIN_TEXT));
      }
      //email.appendContent("\n");
      email.appendContent((String)sectionContentMap.get(BuildEmailConstants.GUARANTEE_CONTENT));
      email.appendContent("\n");
      email.appendContent("\n");
      email.appendContent((String)sectionContentMap.get(BuildEmailConstants.MARKETING_CONTENT));
      email.appendContent("\n");
      email.appendContent("\n");
      email.appendContent((String)sectionContentMap.get(BuildEmailConstants.CONTACT_CONTENT));

      //html content
      String headerHtml = (String)sectionContentMap.get(BuildEmailConstants.HEADER_CONTENT_HTML);
      email.appendHTMLContent(helper.startGlobalHTML());
      email.appendHTMLContent(headerHtml);
      email.appendHTMLContent((String)infoSectionMap.get(BuildEmailConstants.HTML));

      String guaranteeHtml = (String)sectionContentMap.get(BuildEmailConstants.GUARANTEE_CONTENT_HTML);
      email.appendHTMLContent(guaranteeHtml);
      if (!isMercentOrder) {
    	  email.appendHTMLContent("<BR>");
    	  email.appendHTMLContent("<BR>");
      }
      
      String marketingHtml = (String)sectionContentMap.get(BuildEmailConstants.MARKETING_CONTENT_HTML);
      email.appendHTMLContent(marketingHtml);
      if (!isMercentOrder) {
    	  email.appendHTMLContent("<BR>");
    	  email.appendHTMLContent("<BR>");
      }
      
      String contactHtml = (String)sectionContentMap.get(BuildEmailConstants.CONTACT_CONTENT_HTML);
      email.appendHTMLContent(contactHtml);
      email.appendHTMLContent(helper.closeGlobalHTML());
      
  	  Document attributeDocument = DOMUtil.getDefaultDocument();
	  Element attributesElement = attributeDocument.createElement("attributes");
	  attributeDocument.appendChild(attributesElement);

	  Element attributeElement = attributeDocument.createElement("attribute");
	  attributesElement.appendChild(attributeElement);
	  Element nameElement = attributeDocument.createElement("name");
	  nameElement.appendChild(attributeDocument.createTextNode(OrderEmailConstants.TAG_HEADER_CONTENT));
	  attributeElement.appendChild(nameElement);
	  Element valueElement = attributeDocument.createElement("value");
	  valueElement.appendChild(attributeDocument.createCDATASection(headerHtml));
	  attributeElement.appendChild(valueElement);

	  attributeElement = attributeDocument.createElement("attribute");
	  attributesElement.appendChild(attributeElement);
	  nameElement = attributeDocument.createElement("name");
	  nameElement.appendChild(attributeDocument.createTextNode(OrderEmailConstants.TAG_GUARANTEE_CONTENT));
	  attributeElement.appendChild(nameElement);
	  valueElement = attributeDocument.createElement("value");
	  valueElement.appendChild(attributeDocument.createCDATASection(guaranteeHtml));
	  attributeElement.appendChild(valueElement);

	  attributeElement = attributeDocument.createElement("attribute");
	  attributesElement.appendChild(attributeElement);
	  nameElement = attributeDocument.createElement("name");
	  nameElement.appendChild(attributeDocument.createTextNode(OrderEmailConstants.TAG_MARKETING_CONTENT));
	  attributeElement.appendChild(nameElement);
	  valueElement = attributeDocument.createElement("value");
	  valueElement.appendChild(attributeDocument.createCDATASection(marketingHtml));
	  attributeElement.appendChild(valueElement);

	  attributeElement = attributeDocument.createElement("attribute");
	  attributesElement.appendChild(attributeElement);
	  nameElement = attributeDocument.createElement("name");
	  nameElement.appendChild(attributeDocument.createTextNode(OrderEmailConstants.TAG_CONTACT_CONTENT));
	  attributeElement.appendChild(nameElement);
	  valueElement = attributeDocument.createElement("value");
	  valueElement.appendChild(attributeDocument.createCDATASection(contactHtml));
	  attributeElement.appendChild(valueElement);

      //free-shipping indicator
      boolean freeshippingFlag = false;
      if (order.getOrderDetail() != null) {
          for (int i = 0; i < order.getOrderDetail().size(); i++) {
              OrderDetailsVO od = ((OrderDetailsVO) order.getOrderDetail().get(i));
              if(od != null && "Y".equalsIgnoreCase(od.getFreeShipping())) {
            	  if (logger.isDebugEnabled()) {
            	      logger.debug("FreeShipping :"+od.getFreeShipping());
            	  }
            	  freeshippingFlag = true;
                  break;
              }
          }
      }
      String goldMembCartInd = freeshippingFlag?"Y":"N";
        
  	  attributeElement = attributeDocument.createElement("attribute");
	  attributesElement.appendChild(attributeElement);
	  nameElement = attributeDocument.createElement("name");
	  nameElement.appendChild(attributeDocument.createTextNode(BuildEmailConstants.NAME_GOLD_MEMBERSHIP_FLAG));
	  attributeElement.appendChild(nameElement);
	  valueElement = attributeDocument.createElement("value");
	  valueElement.appendChild(attributeDocument.createCDATASection(goldMembCartInd));
	  attributeElement.appendChild(valueElement);

	  if (!isMercentOrder) {
	      String orderDetailsXML = (String) infoSectionMap.get(BuildEmailConstants.ORDER_DETAILS_XML);
	      attributeElement = attributeDocument.createElement("attribute");
          attributesElement.appendChild(attributeElement);
	      nameElement = attributeDocument.createElement("name");
	      nameElement.appendChild(attributeDocument.createTextNode(OrderEmailConstants.TAG_ORDER_DETAILS));
	      attributeElement.appendChild(nameElement);
    	  valueElement = attributeDocument.createElement("value");
	      valueElement.appendChild(attributeDocument.createCDATASection(orderDetailsXML));
	      attributeElement.appendChild(valueElement);
	  }

      String sourceType = order.getSourceType();
      logger.info("sourceType: " + sourceType);
      if (sourceType != null) {
    	  attributeElement = attributeDocument.createElement("attribute");
    	  attributesElement.appendChild(attributeElement);
    	  nameElement = attributeDocument.createElement("name");
    	  nameElement.appendChild(attributeDocument.createTextNode(BuildEmailConstants.NAME_SOURCE_TYPE));
    	  attributeElement.appendChild(nameElement);
    	  valueElement = attributeDocument.createElement("value");
    	  valueElement.appendChild(attributeDocument.createCDATASection(sourceType));
    	  attributeElement.appendChild(valueElement);
      }
      String partnerId = null;
      logger.info("partnerId: " + order.getPartnerId()); 
      logger.info("partnerName: " + order.getPartnerName());
      if (order.getPreferredProcessingPartners() != null) {
          logger.info(" preferredPartner: " + order.getPreferredProcessingPartners().iterator().next());
      }
      if (order.getPartnerName() != null) {
      	partnerId = order.getPartnerName();
      }
	  if(partnerId == null && isMercentOrder) {
	      partnerId = order.getOrderOrigin();
	  }
		
	  if(partnerId == null) {
	      PartnerMappingVO partnerMappingVO = new PartnerUtility().getPartnerOriginsInfo(order.getOrderOrigin(),order.getSourceCode(), connection);
	      if(partnerMappingVO != null && partnerMappingVO.isDefaultBehaviour()) {
	          partnerId = order.getOrderOrigin();
	      }
	  }
      logger.info("partnerId: " + partnerId);
      if (partnerId != null) {
    	  attributeElement = attributeDocument.createElement("attribute");
    	  attributesElement.appendChild(attributeElement);
    	  nameElement = attributeDocument.createElement("name");
    	  nameElement.appendChild(attributeDocument.createTextNode(BuildEmailConstants.NAME_PARTNER_ID));
    	  attributeElement.appendChild(nameElement);
    	  valueElement = attributeDocument.createElement("value");
    	  valueElement.appendChild(attributeDocument.createCDATASection(partnerId));
    	  attributeElement.appendChild(valueElement);
      }
	  StringWriter sw = new StringWriter();
	  PrintWriter pw = new PrintWriter(sw);
	  DOMUtil.print(attributeDocument, pw);
	  email.setRecordAttributesXML(sw.toString());

      helper.updateEmailOrderTokens(email, order);      
      email = helper.performSpecialFormatting(email);
      logger.info("Executing email preview action for guid " + orderGuid);  

      return email;
  }
  
  private Map<String,String> getEmailSectionContent(CachedResultSet rs) throws Exception {
      String seperator = "~";
      //String fromAddress = "";

      String sectionType = "";
      String contentType = "";
      int sectionId = 0;
      int holdsectionId = 0;
      boolean firstTime = true;
      StringBuffer content = new StringBuffer();
      int rowCount = rs.getRowCount();
      int count = 0;

      String subjectContent = "";
      String headerContent = "";
      String headerContentHtml = "";
      String guaranteeContent = "";
      String guaranteeContentHtml = "";
      String marketingContent = "";
      String marketingContentHtml = "";
      String contactContent = "";
      String contactContentHtml = "";
      String fromAddress = "";
      Map<String,String> sectionContentMap = new HashMap<String,String>();

      while(rs != null && rs.next())
      {
          count++;
          sectionId = new Integer(rs.getObject(4).toString()).intValue();

          if(firstTime)
          {
              sectionType = (String) rs.getObject(1);
              contentType = (String) rs.getObject(2);
              content.append((String)rs.getObject(3));
              holdsectionId = sectionId;
              firstTime = false;
          }
          else if (holdsectionId == sectionId)
          {
              //multiple records for a section
              content.append((String)rs.getObject(3));
          }
          else
          {
              //sectionId change figure out content type
              //process each content by section type/content type
              if(sectionType.equals(BuildEmailConstants.SUBJECT))
              {
                  //pull from address from subject content
                  int seperatorPosition = content.toString().indexOf(seperator);
                  fromAddress = content.toString().substring(0,seperatorPosition);
                  subjectContent = content.toString().substring(seperatorPosition + 1,content.toString().length());
              }
              else if(sectionType.equals(BuildEmailConstants.HEADER))
              {
                  if(contentType.equals(BuildEmailConstants.TEXT))
                  {
                      headerContent = content.toString();
                  }
                  else
                  {
                      headerContentHtml = content.toString();
                  }
              }
              else if(sectionType.equals(BuildEmailConstants.GUARANTEE))
              {
                  if(contentType.equals(BuildEmailConstants.TEXT))
                  {
                      guaranteeContent = content.toString();
                  }
                  else
                  {
                      guaranteeContentHtml = content.toString();
                  }
              }
              else if(sectionType.equals(BuildEmailConstants.MARKETING))
              {
                  if(contentType.equals(BuildEmailConstants.TEXT))
                  {
                      marketingContent = content.toString();
                  }
                  else
                  {
                      marketingContentHtml = content.toString();
                  }
              }
              else if(sectionType.equals(BuildEmailConstants.CONTACT))
              {
                  if(contentType.equals(BuildEmailConstants.TEXT))
                  {
                      contactContent = content.toString();
                  }
                  else
                  {
                      contactContentHtml = content.toString();
                  }
              }

              //deal with new sectionId
              content = new StringBuffer();
              sectionType = (String) rs.getObject(1);
              contentType = (String) rs.getObject(2);
              content.append((String)rs.getObject(3));
              holdsectionId = sectionId;

              if(count == rowCount)
              {
                  //last record
                  if(sectionType.equals(BuildEmailConstants.SUBJECT))
                  {
                      //pull from address from subject content
                      int seperatorPosition = content.toString().indexOf(seperator);
                      fromAddress = content.toString().substring(0,seperatorPosition);
                      subjectContent = content.toString().substring(seperatorPosition + 1,content.toString().length());
                  }
                  else if(sectionType.equals(BuildEmailConstants.HEADER))
                  {
                      if(contentType.equals(BuildEmailConstants.TEXT))
                      {
                          headerContent = content.toString();
                      }
                      else
                      {
                          headerContentHtml = content.toString();
                      }
                  }
                  else if(sectionType.equals(BuildEmailConstants.GUARANTEE))
                  {
                      if(contentType.equals(BuildEmailConstants.TEXT))
                      {
                          guaranteeContent = content.toString();
                      }
                      else
                      {
                          guaranteeContentHtml = content.toString();
                      }
                  }
                  else if(sectionType.equals(BuildEmailConstants.MARKETING))
                  {
                      if(contentType.equals(BuildEmailConstants.TEXT))
                      {
                          marketingContent = content.toString();
                      }
                      else
                      {
                          marketingContentHtml = content.toString();
                      }
                  }
                  else if(sectionType.equals(BuildEmailConstants.CONTACT))
                  {
                      if(contentType.equals(BuildEmailConstants.TEXT))
                      {
                          contactContent = content.toString();
                      }
                      else
                      {
                          contactContentHtml = content.toString();
                      }
                  }
              }

          }
      }

      sectionContentMap.put(BuildEmailConstants.SUBJECT_CONTEXT, subjectContent);
      sectionContentMap.put(BuildEmailConstants.HEADER_CONTENT, headerContent);
      sectionContentMap.put(BuildEmailConstants.HEADER_CONTENT_HTML, headerContentHtml);
      sectionContentMap.put(BuildEmailConstants.GUARANTEE_CONTENT, guaranteeContent);
      sectionContentMap.put(BuildEmailConstants.GUARANTEE_CONTENT_HTML, guaranteeContentHtml);
      sectionContentMap.put(BuildEmailConstants.MARKETING_CONTENT, marketingContent);
      sectionContentMap.put(BuildEmailConstants.MARKETING_CONTENT_HTML, marketingContentHtml);
      sectionContentMap.put(BuildEmailConstants.CONTACT_CONTENT, contactContent);
      sectionContentMap.put(BuildEmailConstants.CONTACT_CONTENT_HTML, contactContentHtml);
      sectionContentMap.put(BuildEmailConstants.FROM_ADDRESS, fromAddress);
      return sectionContentMap;
  }
  
}