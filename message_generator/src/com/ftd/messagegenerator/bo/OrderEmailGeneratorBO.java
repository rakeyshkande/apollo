package com.ftd.messagegenerator.bo;

import java.sql.Connection;

import com.ftd.messagegenerator.StockMessageGenerator;
import com.ftd.messagegenerator.constants.BuildEmailConstants;
import com.ftd.messagegenerator.utilities.Utils;
import com.ftd.messagegenerator.vo.PointOfContactVO;
import com.ftd.osp.utilities.email.EmailVO;
import com.ftd.osp.utilities.order.ScrubMapperDAO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.plugins.Logger;

import org.apache.commons.lang.StringUtils;


public class OrderEmailGeneratorBO 
{
	
	 private Connection connection;
	 private Logger logger;
	
	
	public OrderEmailGeneratorBO(Connection connection) {
		 this.connection = connection;
	     this.logger = new Logger("com.ftd.messagegenerator.bo.OrderEmailGeneratorBO");
	}

	private void insertPointOfContact(PointOfContactVO pocObj) throws Exception
    {
        // Insert into Point-Of-Contact table      
    	StockMessageGenerator stockGen = new StockMessageGenerator(connection);
        stockGen.insertMessage(pocObj);   
        
   	    if(BuildEmailConstants.SEND_JMS_MSG) {
      	    StockMessageGenerator.sendMessage(pocObj);
   	    }
      
    }
	
	
	private PointOfContactVO createPointOfContact(OrderVO ordervo,String emailType) throws Exception
    {
		logger.info("Entered the create Point of Contact function for emailType " + emailType);
    	PointOfContactVO pocObj = new PointOfContactVO();
    	
        pocObj.setOrderGuid(ordervo.getGUID());
        pocObj.setMasterOrderNumber(ordervo.getMasterOrderNumber());
        pocObj.setCompanyId(ordervo.getCompanyId());
        pocObj.setSourceCode(ordervo.getSourceCode());
        if(!StringUtils.isEmpty(ordervo.getCsrId()))
       	{
        	pocObj.setUpdatedBy(ordervo.getCsrId());
        	pocObj.setCreatedBy(ordervo.getCsrId());
       	}
        else
        {
        	pocObj.setUpdatedBy("SYS");
        	pocObj.setCreatedBy("SYS");
        }
        pocObj.setCommentType("Email");
        pocObj.setPointOfContactType("Email");
        pocObj.setEmailType(emailType);
        
        return pocObj;
    }
	

    public void generateConfirmationEmail(OrderVO order) throws Exception
    {
    	//create clean.point Of contact record
    	PointOfContactVO pocObj = createPointOfContact(order, BuildEmailConstants.CONFIRMATION_ACTION);

    	insertPointOfContact(pocObj);
        
    }

    public void generateReinstateConfirmationEmail(OrderVO order, String lineNumber) throws Exception
    {
    	logger.info("generateReinstateConfirmationEmail(): " + order.getMasterOrderNumber() + " / " + lineNumber);
    	//create clean.point Of contact record
    	PointOfContactVO pocObj = createPointOfContact(order, BuildEmailConstants.REINSTATE_CONFIRMATION_ACTION);
    	if(!StringUtils.isEmpty(lineNumber)) {
    		pocObj.setOrderDetailId(new Long(lineNumber).longValue());
    	}

    	insertPointOfContact(pocObj);

    }

    public void generateScrubEmail(EmailVO email, OrderVO order, String externalOrderNumber) throws Exception
    {
    	
    	PointOfContactVO pocObj = new PointOfContactVO();
    	pocObj.setOrderGuid(order.getGUID());
    	pocObj.setBody(email.getContent());
       	pocObj.setRecipientEmailAddress(email.getRecipient());
       	pocObj.setSenderEmailAddress(email.getSender());
    	pocObj.setEmailSubject(email.getSubject());
    	pocObj.setUpdatedBy(order.getCsrId());
    	pocObj.setCreatedBy(order.getCsrId());
    	pocObj.setPointOfContactType("Email");
        pocObj.setSourceCode(order.getSourceCode());
        pocObj.setMasterOrderNumber(order.getMasterOrderNumber());
        pocObj.setCompanyId(order.getCompanyId());
        pocObj.setEmailType(BuildEmailConstants.PEND_REMOVE_ACTION);
      	pocObj.setExternalOrderNumber(externalOrderNumber);
        
    	StockMessageGenerator stockGen = new StockMessageGenerator(connection);
        String recordAttributesXML = stockGen.generateRecordAttributeXML(order);
        pocObj.setRecordAttributesXML(recordAttributesXML);
        
        insertPointOfContact(pocObj);
    	
    }
    
    public void generatePreviewEmail(long programId,String runDate,String orderGuid,String toAddress, String comments) throws Exception
    {
    	
        //retrieve order from database        
        ScrubMapperDAO scrubMapperDAO = new ScrubMapperDAO(connection);
        OrderVO order = scrubMapperDAO.mapOrderFromDB(orderGuid);

    	PointOfContactVO pocObj = new PointOfContactVO();
    	pocObj.setOrderGuid(orderGuid);
    	pocObj.setBody(new Long(programId).toString());
    	pocObj.setDeliveryDate(Utils.convertStringToDate(runDate, "MM/dd/yyyy"));
    	pocObj.setRecipientEmailAddress(toAddress);
    	pocObj.setCommentText(comments);
    	pocObj.setUpdatedBy("SYS");
    	pocObj.setCreatedBy("SYS");
    	pocObj.setCommentType("Email");
        pocObj.setPointOfContactType("Email");
        pocObj.setEmailType(BuildEmailConstants.PREVIEW_ACTION);
        if (order != null) {
            pocObj.setCompanyId(order.getCompanyId());
            pocObj.setSourceCode(order.getSourceCode());
            pocObj.setMasterOrderNumber(order.getMasterOrderNumber());
        }
        
        insertPointOfContact(pocObj);
    	
    }

}
