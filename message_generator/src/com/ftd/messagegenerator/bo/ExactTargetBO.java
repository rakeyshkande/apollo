package com.ftd.messagegenerator.bo;

import java.sql.Connection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ftd.messagegenerator.constants.BuildEmailConstants;
import com.ftd.messagegenerator.constants.MessageConstants;
import com.ftd.messagegenerator.dao.MessageGeneratorDAO;
import com.ftd.messagegenerator.mailer.ExactTargetMailer;
import com.ftd.messagegenerator.vo.PointOfContactVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.ServiceResponseTrackingVO;
import com.ftd.osp.utilities.stats.ServiceResponseTrackingUtil;
import com.ftd.osp.utilities.xml.JAXPUtil;

public class ExactTargetBO {
    private static Logger logger = new Logger("com.ftd.messagegenerator.bo.ExactTargetBO"); 
    
    // Constructor
    //
    public ExactTargetBO() {
    }

    
    /**
     * Construct the appropriate Exact Target request message
     * then invoke ET Web Service to send an email.
     * 
     * This is the only public method for this class.
     */
    public void prepareAndSendEmailRequest(Connection conn, PointOfContactVO pocObj) throws Exception {
    	logger.info("prepareAndSendEmailRequest()");
    	
    	String subject = pocObj.getEmailSubject();
    	String emailBody = pocObj.getBody();
    	String companyId = pocObj.getCompanyId();
    	String sourceCode = pocObj.getSourceCode();
    	String externalOrderNumber = pocObj.getExternalOrderNumber();
    	String masterOrderNumber = pocObj.getMasterOrderNumber();
    	String orderNumber = externalOrderNumber;
    	if (orderNumber == null) {
    		orderNumber = masterOrderNumber;
    	}
    	String fromEmail = pocObj.getSenderEmailAddress();
    	String toEmail = pocObj.getRecipientEmailAddress();
		if (logger.isDebugEnabled()) {
    	    logger.debug("fromEmail: " + fromEmail);
    	    logger.debug("toEmail: " + toEmail);
		}
    	String emailType = pocObj.getEmailType();
    	String templateId = null;
    	logger.info("companyId: " + companyId);
    	ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
    	String separateCompanies = configUtil.getFrpGlobalParm(MessageConstants.MESSAGE_GEN_CONFIG_CONTEXT,
    			"EXACT_TARGET_SEPARATE_COMPANIES");
    	String separateCompanyId = null;
        String[] companyList = separateCompanies.split(" ");
        HashMap<String, String> companyMap = new HashMap<String, String>();
        for (int a=0; a<companyList.length; a++) {
        	companyMap.put(companyList[a], companyList[a]);
        }
    	if (companyId != null && companyMap.get(companyId) != null) {
    		templateId = configUtil.getFrpGlobalParm(MessageConstants.MESSAGE_GEN_CONFIG_CONTEXT,
            		"TEMPLATE_ID_" + emailType + "_" + companyId);
    		separateCompanyId = companyId;
    	}
    	if (templateId == null) {
            templateId = configUtil.getFrpGlobalParm(MessageConstants.MESSAGE_GEN_CONFIG_CONTEXT,
            		"TEMPLATE_ID_" + emailType);
    	}
        if (templateId == null) {
            templateId = configUtil.getFrpGlobalParm(MessageConstants.MESSAGE_GEN_CONFIG_CONTEXT,
            		MessageConstants.DEFAULT_TEMPLATE_ID);
        }
    	logger.info("templateId: " + templateId);
    	
    	HashMap<String, String> paramMap = new HashMap<String, String>();
    	
    	paramMap.put("Subject", subject);
		if (logger.isDebugEnabled()) {
    	    logger.debug("Subject: " + subject);
		}
    	if (emailBody != null) {
    		//emailBody = emailBody.replaceAll("(\r\n|\n\r|\n|\r)", "<br />");
    		paramMap.put("EmailBody", emailBody);
    	}
    	if (companyId != null) {
    		paramMap.put("CompanyId", companyId);
			if (logger.isDebugEnabled()) {
    		    logger.debug("companyId: " + companyId);
			}
    	}
    	if (sourceCode != null) {
    		paramMap.put("SourceCode", sourceCode);
			if (logger.isDebugEnabled()) {
    		    logger.debug("sourceCode: " + sourceCode);
			}
    	}
    	if (orderNumber != null) {
    		paramMap.put("OrderNumber", orderNumber);
			if (logger.isDebugEnabled()) {
    		    logger.debug("orderNumber: " + orderNumber);
			}
    	}

    	if (pocObj.getRecordAttributesXML() != null) {
    		logger.info(pocObj.getRecordAttributesXML());
        	try {
        	    Document doc = JAXPUtil.parseDocument(pocObj.getRecordAttributesXML());

        	    NodeList nodeList = doc.getDocumentElement().getChildNodes(); 
                for (int i = 0; i < nodeList.getLength(); i++) {  
                	Node node = nodeList.item(i);  
                	if (node.getNodeType() == Node.ELEMENT_NODE) {  
                    	Element elem = (Element) node;  
                        String name = elem.getElementsByTagName("name").item(0).getChildNodes().item(0).getNodeValue();
                        Node nodeValue = elem.getElementsByTagName("value").item(0);
                        if (nodeValue != null && nodeValue.hasChildNodes()) {
                            String value = nodeValue.getChildNodes().item(0).getNodeValue();
                            paramMap.put(name, value);
                        }
                	}
                }
        	} catch (Exception e) {
        		logger.error("Error parsing RECORD_ATTRIBUTES_XML: " + e);
        	}
    	}
    	//save details into POC if the Flag is  before sending them to ET
    	
    	if((BuildEmailConstants.CONFIRMATION_ACTION).equalsIgnoreCase(pocObj.getEmailType()))
    	{
    		String  params = null;
    		String pocId = new Long(pocObj.getPointOfContactId()).toString();
    		String emailSubj = pocObj.getEmailSubject();
    		
    		StringBuffer paramMapValues = new StringBuffer("");
			if(paramMap!= null)
			{
    		for (Map.Entry<String,String> entry : paramMap.entrySet()) {
    			  String key = entry.getKey();
    			  String value = entry.getValue();
    			  paramMapValues.append(key).append(" : ").append(value).append(" , ");
    			}
			}
    		logger.info("paramMap values :: "+paramMapValues.toString());
    		params = paramMapValues.toString();
    			MessageGeneratorDAO dao = new MessageGeneratorDAO(conn);
    			dao.updateConfirmationDetailsToPOC(pocId, emailSubj, params);
    		
    	}
    	
    	
    	// call ET application
    	ExactTargetMailer etMailer = new ExactTargetMailer();
    	long startTime = System.currentTimeMillis();
    	boolean success = etMailer.sendEmail(fromEmail, toEmail, templateId, paramMap, separateCompanyId);
    	long responseTime = System.currentTimeMillis() - startTime;
    	logger.info("result: " + success);
    	logger.info("responseTime: " + responseTime);
    	
        ServiceResponseTrackingVO srtVO = new ServiceResponseTrackingVO();
        srtVO.setServiceName("Exact Target");
        srtVO.setServiceMethod("create");
        srtVO.setTransactionId(Long.toString(pocObj.getPointOfContactId()));
        srtVO.setResponseTime(responseTime);
        srtVO.setCreatedOn(new Date());

        ServiceResponseTrackingUtil srtUtil = new ServiceResponseTrackingUtil();
        srtUtil.insert(conn, srtVO);
        
    }
}
