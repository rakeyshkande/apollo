package com.ftd.messagegenerator.bo.helper;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.sql.Connection;
import java.text.CharacterIterator;
import java.text.NumberFormat;
import java.text.StringCharacterIterator;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ftd.messagegenerator.constants.BuildEmailConstants;
import com.ftd.messagegenerator.constants.OrderEmailConstants;
import com.ftd.messagegenerator.dao.OrderEmailDAO;
import com.ftd.messagegenerator.utilities.Utils;
import com.ftd.osp.utilities.AddOnUtility;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.GiftCodeUtil;
import com.ftd.osp.utilities.PartnerUtility;
import com.ftd.osp.utilities.PreferredPartnerUtility;
import com.ftd.osp.utilities.SegmentationUtil;
import com.ftd.osp.utilities.WebloyaltyUtilities;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.CacheUtil;
import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.SourceMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.SourceMasterVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.email.EmailVO;
import com.ftd.osp.utilities.order.vo.AddOnsVO;
import com.ftd.osp.utilities.order.vo.BuyerAddressesVO;
import com.ftd.osp.utilities.order.vo.BuyerEmailsVO;
import com.ftd.osp.utilities.order.vo.BuyerPhonesVO;
import com.ftd.osp.utilities.order.vo.BuyerVO;
import com.ftd.osp.utilities.order.vo.CreditCardsVO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.PartnerVO;
import com.ftd.osp.utilities.order.vo.PaymentsVO;
import com.ftd.osp.utilities.order.vo.RecalculateOrderSourceCodeVO;
import com.ftd.osp.utilities.order.vo.RecipientAddressesVO;
import com.ftd.osp.utilities.order.vo.RecipientPhonesVO;
import com.ftd.osp.utilities.order.vo.RecipientsVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.AddOnVO;
import com.ftd.osp.utilities.vo.RedemptionDetails;
import com.ftd.osp.utilities.vo.RetrieveGiftCodeResponse;
import com.ftd.osp.utilities.xml.DOMUtil;

public class OrderEmailHelper {
	
	static final Logger logger = new Logger(OrderEmailHelper.class.getName());

	private static String GLOBAL_PARAM_EMAIL_CONTEXT = "EMAIL_SERVICE";
	private static String GLOBAL_PARAM_EMAIL_TYPE_KEY = "SEND_CONTENT_TYPE";
	private static String EMAIL_SERVICE_CONFIG_FILE = "message_generator_config.xml";
	private static final String SOURCE_CODE_HANDLER_NAME = "CACHE_NAME_SOURCE_MASTER";
	private static final String GLOBAL_PARM_HANDLER = "CACHE_NAME_GLOBAL_PARM";

	private Connection connection = null;
	private static final int SPLIT_SIZE = 52;

	private static String ENGLISH = "ENUS";
	private static String SYMPATHY = "SYMPATHY";
	private static String SCI = "SCI";
	private static final String PRODUCT_TYPE_SERVICES = "SERVICES";

	ConfigurationUtil cu = null;

	public OrderEmailHelper()
	{
		
	}
	
	public OrderEmailHelper(Connection connection)
	{
		this.connection = connection;
		// Instantiating ConfigurationUtil as well. Note that there is no static method in this class.
		try {
			cu = ConfigurationUtil.getInstance();
		} catch (Exception e) {
			logger.error("Error caught instantiating Configuration util " + e.getMessage());
			throw new RuntimeException(e);
		}
	}

	private Map getProgramBySource(String sourceCode) throws Exception
	{
		Map emailMap = null;

		try
		{

			DataRequest dataRequest = new DataRequest();
			dataRequest.setConnection(connection);
			dataRequest.setStatementID(BuildEmailConstants.VIEW_CURRENT_EMAIL_SECTIONS);
			dataRequest.addInputParam("IN_SOURCE_CODE", sourceCode);
			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			emailMap = (Map)dataAccessUtil.execute(dataRequest);
			String status = (String) emailMap.get(BuildEmailConstants.STATUS_PARAM);

			if(status.equals("N"))
			{
				String message = (String) emailMap.get(BuildEmailConstants.MESSAGE_PARAM);
				throw new Exception("Unable to load program by source code. Specific: " + message);
			}
		}
		catch(Exception e)
		{
			throw new Exception("Unable to load program by source code. Specific: " + e.toString());
		}

		return emailMap;
	}

	public Map getEmailSectionContent(OrderVO order) throws Exception
	{
		Map emailMap = this.getProgramBySource(order.getSourceCode());

		String seperator = "~";
		CachedResultSet rs = new CachedResultSet();
		rs = (CachedResultSet) emailMap.get(BuildEmailConstants.EMAIL_CONTENT_CURSOR);

		String sectionType = "";
		String contentType = "";
		int sectionId = 0;
		int holdsectionId = 0;
		boolean firstTime = true;
		StringBuffer content = new StringBuffer();
		int rowCount = rs.getRowCount();
		int count = 0;

		String subjectContent = "";
		String headerContent = "";
		String headerContentHtml = "";
		String guaranteeContent = "";
		String guaranteeContentHtml = "";
		String marketingContent = "";
		String marketingContentHtml = "";
		String contactContent = "";
		String contactContentHtml = "";
		String fromAddress = "";
		Map sectionContentMap = new HashMap();

		while(rs != null && rs.next())
		{
			count++;
			sectionId = new Integer(rs.getObject(4).toString()).intValue();

			if(firstTime)
			{
				sectionType = (String) rs.getObject(1);
				contentType = (String) rs.getObject(2);
				content.append((String)rs.getObject(3));
				holdsectionId = sectionId;
				firstTime = false;
			}
			else if (holdsectionId == sectionId)
			{
				//multiple records for a section
				content.append((String)rs.getObject(3));
			}
			else
			{
				//sectionId change figure out content type
				//process each content by section type/content type
				if(sectionType.equals(BuildEmailConstants.SUBJECT))
				{
					//pull from address from subject content
					int seperatorPosition = content.toString().indexOf(seperator);
					fromAddress = content.toString().substring(0,seperatorPosition);
					subjectContent = content.toString().substring(seperatorPosition + 1,content.toString().length());
				}
				else if(sectionType.equals(BuildEmailConstants.HEADER))
				{
					if(contentType.equals(BuildEmailConstants.TEXT))
					{
						headerContent = content.toString();
					}
					else
					{
						headerContentHtml = content.toString();
					}
				}
				else if(sectionType.equals(BuildEmailConstants.GUARANTEE))
				{
					if(contentType.equals(BuildEmailConstants.TEXT))
					{
						guaranteeContent = content.toString();
					}
					else
					{
						guaranteeContentHtml = content.toString();
					}
				}
				else if(sectionType.equals(BuildEmailConstants.MARKETING))
				{
					if(contentType.equals(BuildEmailConstants.TEXT))
					{
						marketingContent = content.toString();
					}
					else
					{
						marketingContentHtml = content.toString();
					}
				}
				else if(sectionType.equals(BuildEmailConstants.CONTACT))
				{
					if(contentType.equals(BuildEmailConstants.TEXT))
					{
						contactContent = content.toString();
					}
					else
					{
						contactContentHtml = content.toString();
					}
				}

				//deal with new sectionId
				content = new StringBuffer();
				sectionType = (String) rs.getObject(1);
				contentType = (String) rs.getObject(2);
				content.append((String)rs.getObject(3));
				holdsectionId = sectionId;

				if(count == rowCount)
				{
					//last record
					if(sectionType.equals(BuildEmailConstants.SUBJECT))
					{
						//pull from address from subject content
						int seperatorPosition = content.toString().indexOf(seperator);
						fromAddress = content.toString().substring(0,seperatorPosition);
						subjectContent = content.toString().substring(seperatorPosition + 1,content.toString().length());
					}
					else if(sectionType.equals(BuildEmailConstants.HEADER))
					{
						if(contentType.equals(BuildEmailConstants.TEXT))
						{
							headerContent = content.toString();
						}
						else
						{
							headerContentHtml = content.toString();
						}
					}
					else if(sectionType.equals(BuildEmailConstants.GUARANTEE))
					{
						if(contentType.equals(BuildEmailConstants.TEXT))
						{
							guaranteeContent = content.toString();
						}
						else
						{
							guaranteeContentHtml = content.toString();
						}
					}
					else if(sectionType.equals(BuildEmailConstants.MARKETING))
					{
						if(contentType.equals(BuildEmailConstants.TEXT))
						{
							marketingContent = content.toString();
						}
						else
						{
							marketingContentHtml = content.toString();
						}
					}
					else if(sectionType.equals(BuildEmailConstants.CONTACT))
					{
						if(contentType.equals(BuildEmailConstants.TEXT))
						{
							contactContent = content.toString();
						}
						else
						{
							contactContentHtml = content.toString();
						}
					}
				}

			}
		}

		sectionContentMap.put(BuildEmailConstants.SUBJECT_CONTEXT, subjectContent);
		sectionContentMap.put(BuildEmailConstants.HEADER_CONTENT, headerContent);
		sectionContentMap.put(BuildEmailConstants.HEADER_CONTENT_HTML, headerContentHtml);
		sectionContentMap.put(BuildEmailConstants.GUARANTEE_CONTENT, guaranteeContent);
		sectionContentMap.put(BuildEmailConstants.GUARANTEE_CONTENT_HTML, guaranteeContentHtml);
		sectionContentMap.put(BuildEmailConstants.MARKETING_CONTENT, marketingContent);
		sectionContentMap.put(BuildEmailConstants.MARKETING_CONTENT_HTML, marketingContentHtml);
		sectionContentMap.put(BuildEmailConstants.CONTACT_CONTENT, contactContent);
		sectionContentMap.put(BuildEmailConstants.CONTACT_CONTENT_HTML, contactContentHtml);
		sectionContentMap.put(BuildEmailConstants.FROM_ADDRESS, fromAddress);
		return sectionContentMap;
	}

	public String extractBuyerEmail(OrderVO order) throws Exception
	{
		String toAddress =  ((BuyerEmailsVO)((BuyerVO)(order.getBuyer().get(0))).getBuyerEmails().get(0)).getEmail().toLowerCase();
		if(toAddress == null || toAddress.equals(""))
			throw new Exception("Unable to extract buyer email for order_guid: " + order.getGUID());
		return toAddress;
	}

	public HashMap getBulkOrderInfoSection(OrderVO order) throws Exception
	{
		HashMap sections = new HashMap();
		OrderDetailsVO tmpDetailVO = null;
		BuyerVO tmpBuyerVO = (BuyerVO)order.getBuyer().get(0);
		StringBuffer orderInfoSection = new StringBuffer();
		StringBuffer HTMLorderInfoSection = new StringBuffer();
		ArrayList itemGroups = new ArrayList();
		ArrayList itemGroup = null;
		boolean inGroup = false;
		double subtotal = 0;      
		//Start---Same day upcharge fields display
		boolean separateLineSameDayFee = false;
		String displaySameDayUpcharge;
		String gDisplaySameDayUpcharge;
		RecalculateOrderSourceCodeVO recalculateOrderSourceCodeVO = new RecalculateOrderSourceCodeVO();
		recalculateOrderSourceCodeVO = getSourceCodeDetails(order.getSourceCode());
		displaySameDayUpcharge = recalculateOrderSourceCodeVO.getDisplaySameDayUpcharge();
		gDisplaySameDayUpcharge = CacheUtil.getInstance().getGlobalParm("FTDAPPS_PARMS", "SAME_DAY_UPCHARGE_DISPLAY");
		if(displaySameDayUpcharge.equalsIgnoreCase("Y") || 
				(displaySameDayUpcharge.equalsIgnoreCase("D") && gDisplaySameDayUpcharge.equalsIgnoreCase("Y"))) {       
			separateLineSameDayFee = true;      
		}
		//End-----Same day upcharge fields display            

		orderInfoSection.append(Utils.end()+ Utils.end() + Utils.end());
		HTMLorderInfoSection.append(this.startHTML());
		HTMLorderInfoSection.append(this.startHTMLTable());

		orderInfoSection.append("Customer:" + Utils.createSpaces(17) + tmpBuyerVO.getFirstName() + " " + tmpBuyerVO.getLastName() + Utils.end());
		HTMLorderInfoSection.append(this.addBulkStandardUpperRow("Customer:", tmpBuyerVO.getFirstName() + "&nbsp;" + tmpBuyerVO.getLastName()));
		orderInfoSection.append("Order Date:" + Utils.createSpaces(15) + Utils.convertDate(order.getOrderDate(),1) + Utils.end());
		HTMLorderInfoSection.append(this.addBulkStandardUpperRow("Order Date:", Utils.convertDate(order.getOrderDate(),1)));
		orderInfoSection.append("Master Order Number:" + Utils.createSpaces(6) + order.getMasterOrderNumber() + Utils.end());
		HTMLorderInfoSection.append(this.addBulkStandardUpperRow("Master Order Number:", order.getMasterOrderNumber()));
		HTMLorderInfoSection.append(this.endHTMLTable());
		HTMLorderInfoSection.append("<br>");
		orderInfoSection.append(Utils.end() + Utils.end());

		orderInfoSection.append("QTY\t" + Utils.createSpaces(28) + "\t\tDelivery\tShip" + Utils.end());
		orderInfoSection.append("   \t" + Utils.createSpaces(28) + "\t\tDate\t\tMethod" + Utils.end() + Utils.end());
		HTMLorderInfoSection.append(this.startBulkInfoTable());
		HTMLorderInfoSection.append(this.addBulkInfoHeader());

		for(int i = 0; i < order.getOrderDetail().size(); i++)
		{
			tmpDetailVO = (OrderDetailsVO)(order.getOrderDetail().get(i));
			subtotal += Double.parseDouble(tmpDetailVO.getProductsAmount());
			inGroup = false;

			if(itemGroups.size() == 0)
			{
				itemGroup = new ArrayList();
				itemGroup.add(tmpDetailVO);
				itemGroups.add(itemGroup);
			}
			else
			{
				for(int j = 0; j < itemGroups.size();j++)
				{
					if(
							tmpDetailVO.getProductId().equals(((OrderDetailsVO)((ArrayList)itemGroups.get(j)).get(0)).getProductId()) &&
							tmpDetailVO.getShipMethod().equals(((OrderDetailsVO)((ArrayList)itemGroups.get(j)).get(0)).getShipMethod()) &&
							tmpDetailVO.getDeliveryDate().equals(((OrderDetailsVO)((ArrayList)itemGroups.get(j)).get(0)).getDeliveryDate()) &&
							tmpDetailVO.getProductsAmount().equals(((OrderDetailsVO)((ArrayList)itemGroups.get(j)).get(0)).getProductsAmount())
					)
					{
						((ArrayList)itemGroups.get(j)).add(tmpDetailVO);
						inGroup = true;
						break;
					}
				} //close for
				if(!inGroup)
				{
					itemGroup = new ArrayList();
					itemGroup.add(tmpDetailVO);
					itemGroups.add(itemGroup);
				}
			} //close else
		}

		ArrayList tmpArrayList = null;
		double totalLinePrice = 0;
		double itemPrice = 0;
		String shippingMethod = null;
		java.text.NumberFormat nf = java.text.NumberFormat.getCurrencyInstance(Locale.US);

		for(int i =0; i < itemGroups.size(); i++)
		{
			tmpArrayList = (ArrayList)(itemGroups.get(i));
			tmpDetailVO = (OrderDetailsVO)tmpArrayList.get(0);
			itemPrice = Double.parseDouble(tmpDetailVO.getProductsAmount());
			totalLinePrice = itemPrice * tmpArrayList.size();
			shippingMethod = Utils.translateShippingMethod(tmpDetailVO.getShipMethod());
			shippingMethod = this.replace(shippingMethod, " Delivery", "");
			orderInfoSection.append(tmpArrayList.size() + "\t" + Utils.truncDescription(tmpDetailVO.getItemDescription()) + " @" + Utils.formatDollars(tmpDetailVO.getProductsAmount()) + "\t" + tmpDetailVO.getDeliveryDate() + "\t" + shippingMethod + "\t" +  Utils.rightAlign(nf.format(totalLinePrice)) + Utils.end());
			orderInfoSection.append("\t" + tmpDetailVO.getProductId() + Utils.end());
			HTMLorderInfoSection.append(this.addBulkStandardInfoRow(String.valueOf(tmpArrayList.size()), Utils.truncDescription(tmpDetailVO.getItemDescription()),"&nbsp;@" + Utils.formatDollars(tmpDetailVO.getProductsAmount()), tmpDetailVO.getDeliveryDate(), shippingMethod, nf.format(totalLinePrice)));
			HTMLorderInfoSection.append(this.addBulkStandardInfoRow("&nbsp;", tmpDetailVO.getProductId(), "&nbsp;", "&nbsp;","&nbsp;", "&nbsp;"));
		}
		HTMLorderInfoSection.append(this.endHTMLTable());
		HTMLorderInfoSection.append(this.startHTMLTable()); /////////////////////////////////////////////here starts the summary table

		orderInfoSection.append(Utils.line(16) + "\t\t\t\t\t\t\t\t\t" + Utils.line(9) + Utils.end());
		HTMLorderInfoSection.append(this.addBulkStandardSummaryRow(Utils.line(16), Utils.line(9)));

		orderInfoSection.append("Subtotal:" + "\t\t\t\t\t\t\t\t\t\t" + Utils.rightAlign(nf.format(subtotal)) +  Utils.end() + Utils.end() + Utils.end());
		HTMLorderInfoSection.append(this.addBulkStandardSummaryRow("Subtotal:", nf.format(subtotal)));
		//DISCOUNT
		if(order.getDiscountTotal() != null && !order.getDiscountTotal().startsWith("0"))
		{
			orderInfoSection.append("Discount:\t\t\t\t\t\t\t" + Utils.rightAlign("  (" + Utils.formatDollars(order.getDiscountTotal()) + ")") + Utils.end());
			HTMLorderInfoSection.append(this.addBulkStandardSummaryRow("Discount:", "(" + Utils.formatDollars(order.getDiscountTotal()) + ")"));
		}

		//SHIPPING + SERVICE CHARGES
		BigDecimal totalFees = new BigDecimal(0);

		if(order.getShippingFeeTotal() != null && !order.getShippingFeeTotal().startsWith("0"))
		{
			BigDecimal shipFee = new BigDecimal( order.getShippingFeeTotal());
			totalFees = totalFees.add(shipFee);
		}

		if(order.getServiceFeeTotal() != null && !order.getServiceFeeTotal().startsWith("0"))
		{
			BigDecimal serviceFee = new BigDecimal(order.getServiceFeeTotal());
			//Same day upcharge code. If separateLineSameDayFee is true and has same day upcharge then subtract from service fee and then add
			//service fee to total fees. Or else service fee contains same day upcharge fees included.            
			if(separateLineSameDayFee && tmpDetailVO.getSameDayUpcharge() != null && (Float.parseFloat(tmpDetailVO.getSameDayUpcharge()) > 0)){
				serviceFee = serviceFee.subtract(new BigDecimal(tmpDetailVO.getSameDayUpcharge()));            	
			}
			totalFees = totalFees.add(serviceFee);
		}

		// subtract Morning Delivery Fees from shipping/service fees if separateLineMorningDeliveryFee is Y and if morningDeliveryFee is greater than or equal to 0
		if(tmpDetailVO.getMorningDeliveryFee() != null && (Float.parseFloat(tmpDetailVO.getMorningDeliveryFee()) > 0)){
			if(totalFees.doubleValue() > 0)
			{
				totalFees = totalFees.subtract(new BigDecimal(tmpDetailVO.getMorningDeliveryFee()));
			}
		}

		//subtract Surcharge Fees  from shipping/service fee if displaySurcharge flag is Y and we will display separately from shipping/service fee       
		if ( StringUtils.equalsIgnoreCase(tmpDetailVO.getDisplaySurcharge(), "Y") &&  StringUtils.equalsIgnoreCase(tmpDetailVO.getApplySurchargeCode(), "ON"))
		{
			BigDecimal fuelSurchargeFee = new BigDecimal( tmpDetailVO.getFuelSurcharge());// we will show surcharge fee even if its zero 
			if(totalFees.doubleValue() > 0)
			{
				totalFees = totalFees.subtract(fuelSurchargeFee);
			}             
		}

		if(totalFees.doubleValue() > 0)
		{
			orderInfoSection.append("Shipping/Service Fees:" + "\t\t\t\t\t\t" + Utils.rightAlign("" + Utils.formatDollars(totalFees.doubleValue())) + Utils.end());
			HTMLorderInfoSection.append(this.addBulkStandardSummaryRow("Shipping/Service Fees:", "" + Utils.formatDollars(totalFees.doubleValue())));
		}

		//Start---Same day upcharge fields display
		logger.debug("In BuildOrderEmailHelper getOrderInfoSection displaySameDayUpcharge value is "+ displaySameDayUpcharge);
		logger.debug("In BuildOrderEmailHelper getOrderInfoSection global displaySameDayUpcharge value is "+ gDisplaySameDayUpcharge);
		logger.debug("In BuildOrderEmailHelper getOrderInfoSection sameDayUpcharge fee value is "+ tmpDetailVO.getSameDayUpcharge());
		if(separateLineSameDayFee && tmpDetailVO.getSameDayUpcharge() != null && (Float.parseFloat(tmpDetailVO.getSameDayUpcharge()) > 0)){
			String sameDayUpchargeLabel = CacheUtil.getInstance().getContentWithFilter("SAME_DAY_UPCHARGE", "CONFIRMATION_EMAIL_LABEL", null, null, false);
			logger.debug("In BuildOrderEmailHelper getOrderInfoSection sameDayUpcharge label is "+ sameDayUpchargeLabel);
			orderInfoSection.append(Utils.justifyToLine(sameDayUpchargeLabel+ ":", "" + Utils.formatDollars(tmpDetailVO.getSameDayUpcharge()))+ Utils.end());
			HTMLorderInfoSection.append(addStandardLowerRow(sameDayUpchargeLabel+ ":", "" + Utils.formatDollars(tmpDetailVO.getSameDayUpcharge())) + Utils.end());
		}
		//End-----Same day upcharge fields display

		//Start---Morning delivery fields display      
		logger.debug("In BuildOrderEmailHelper getBulkOrderInfoSection morningDeliveryFee value is "+ tmpDetailVO.getMorningDeliveryFee());

		if(tmpDetailVO.getMorningDeliveryFee() !=null && (Float.parseFloat(tmpDetailVO.getMorningDeliveryFee()) > 0)){
			String morningDeliveryFeeLabel = CacheUtil.getInstance().getContentWithFilter("MORNING_DELIVERY", "MORNING_DELIVERY_LABEL", null, null, false);
			orderInfoSection.append(Utils.justifyToLine(morningDeliveryFeeLabel + ":","" + Utils.formatDollars(tmpDetailVO.getMorningDeliveryFee())) + Utils.end());
			HTMLorderInfoSection.append(addStandardLowerRow(morningDeliveryFeeLabel + ":","" + Utils.formatDollars(tmpDetailVO.getMorningDeliveryFee())) + Utils.end());
		}
		//End-----Morning delivery fields display

		// add the fuel surcharge fee line after shipping/service fee line          
		logger.debug("In Bulk BuildOrderEmailHelper getDisplaySurcharge value is "+ tmpDetailVO.getDisplaySurcharge());
		logger.debug("In Bulk BuildOrderEmailHelper getApplySurchargeCode value is "+ tmpDetailVO.getApplySurchargeCode());
		if ( StringUtils.equalsIgnoreCase(tmpDetailVO.getDisplaySurcharge(), "Y") &&  StringUtils.equalsIgnoreCase(tmpDetailVO.getApplySurchargeCode(), "ON"))
		{
			BigDecimal fuelSurchargeFee = new BigDecimal( tmpDetailVO.getFuelSurcharge());// we will show surcharge fee even if its zero 
			String fuelSurchargeDescription = tmpDetailVO.getFuelSurchargeDescription();
			logger.debug("fuelSurchargeDescription to display in email is"+ fuelSurchargeDescription);
			orderInfoSection.append(Utils.justifyToLine(fuelSurchargeDescription+ ":", "" + Utils.formatDollars(fuelSurchargeFee.doubleValue())) + Utils.end());
			HTMLorderInfoSection.append(addStandardLowerRow(fuelSurchargeDescription+ ":", "" + Utils.formatDollars(fuelSurchargeFee.doubleValue())) + Utils.end());
		}

		//TAX
		if(order.getTaxTotal() != null && !order.getTaxTotal().startsWith("0"))
		{
			orderInfoSection.append("Tax:" + "\t\t\t\t\t\t\t\t" + Utils.rightAlign(""+Utils.formatDollars(order.getTaxTotal())) + Utils.end());
			HTMLorderInfoSection.append(this.addBulkStandardSummaryRow("Tax:", ""+Utils.formatDollars(order.getTaxTotal())));
		}
		orderInfoSection.append(Utils.end());
		orderInfoSection.append(Utils.line(16) + "\t\t\t\t\t\t" + Utils.line(9) + Utils.end());
		HTMLorderInfoSection.append(this.addBulkStandardSummaryRow(Utils.line(16), Utils.line(9)));

		//TOTAL CHARGE
		orderInfoSection.append("Total Charge:" + "\t\t\t\t\t\t" + Utils.rightAlign(""+Utils.formatDollars(order.getOrderTotal())) + Utils.end());
		HTMLorderInfoSection.append(this.addBulkStandardSummaryRow("Total Charge:", "" + Utils.formatDollars(order.getOrderTotal())));
		orderInfoSection.append(Utils.end());

		//CREDIT CARD TYPE
		PaymentsVO tmpPaymentsVO = (PaymentsVO)order.getPayments().get(0);
		CreditCardsVO tmpCreditCardsVO = null;
		if(!tmpPaymentsVO.getCreditCards().isEmpty())
			tmpCreditCardsVO = (CreditCardsVO)tmpPaymentsVO.getCreditCards().get(0);
		else
			tmpCreditCardsVO = new CreditCardsVO(); //just blank

		if(tmpPaymentsVO.getPaymentMethodType() != null && tmpPaymentsVO.getPaymentMethodType().equalsIgnoreCase("C"))
		{
			orderInfoSection.append("Credit Card Type:\t" + translateCCType(tmpCreditCardsVO.getCCType()));
			HTMLorderInfoSection.append(this.addBulkStandardSummaryRow("Credit Card Type:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + translateCCType(tmpCreditCardsVO.getCCType()), "&nbsp;"));
		}
		else
			if(tmpPaymentsVO.getPaymentMethodType() != null && tmpPaymentsVO.getPaymentMethodType().equalsIgnoreCase("I"))
			{
				orderInfoSection.append("Payment Type:\tInvoice");
				HTMLorderInfoSection.append(this.addBulkStandardSummaryRow("Payment Type:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Invoice", "&nbsp;"));
			}

		orderInfoSection.append(Utils.line() + Utils.end());

		HTMLorderInfoSection.append(this.endHTMLTable());
		HTMLorderInfoSection.append("<hr align=\"left\" width=\"610\" noshade size=\"1\">");

		HTMLorderInfoSection.append(this.startHTMLTable());
		orderInfoSection.append("All prices are in US dollars");
		HTMLorderInfoSection.append(addStandardLowerRow("All prices are in US dollars", "&nbsp;"));
		HTMLorderInfoSection.append(HTMLend());

		//REWARD INFORMATION
		String partnerId = recalculateOrderSourceCodeVO.getPartnerId();
		PartnerVO partner = this.getPartner(partnerId);
		if(partnerId != null && !partnerId.equals(""))
		{
			if(partner.getEmailExludeFlag() != null && !partner.getEmailExludeFlag().equalsIgnoreCase("Y"))
			{
				if(partner.getRewardName() != null && partner.getRewardName().equalsIgnoreCase("CHARITY"))
				{
					orderInfoSection.append("FTD.COM will donate 5% of the product price to " + partner.getPartnerName() + Utils.end());
					HTMLorderInfoSection.append(addStandardLowerRow("FTD.COM will donate 5% of the product price to " + partner.getPartnerName(), "&nbsp;"));
				}
				else
					if(partner.getPartnerId().equalsIgnoreCase("DISC1"))
					{
						orderInfoSection.append("Your purchase from FTD.COM has earned you " + Utils.formatDollars(order.getPartnershipBonusPoints()) + " towards your Cash Back Bonus with Discover." + Utils.end());
						HTMLorderInfoSection.append(addStandardLowerRow("Your purchase from FTD.COM has earned you " + Utils.formatDollars(order.getPartnershipBonusPoints()) + " towards your Cash Back Bonus with Discover.", "&nbsp;"));
					}
					else
					{
						long points = 0;
						if(order.getPartnershipBonusPoints() != null)
						{
							points = Math.round(Double.parseDouble(order.getPartnershipBonusPoints()));
						}
						orderInfoSection.append("You will earn " + points + " " + partner.getPartnerName() + " " + partner.getRewardName() + Utils.end());
						orderInfoSection.append("Please allow 6-8 weeks for miles/points to post to your account." + Utils.end());
						HTMLorderInfoSection.append(addStandardLowerRow("You will earn " + order.getPartnershipBonusPoints() + " " + partner.getPartnerName() + " " + partner.getRewardName(), "&nbsp;"));
						HTMLorderInfoSection.append(addStandardLowerRow("Please allow 6-8 weeks for miles/points to post to your account.", "&nbsp;"));
					}
			}
		}
		HTMLorderInfoSection.append(this.endHTMLTable());
		HTMLorderInfoSection.append(this.closeHTML());

		sections.put(BuildEmailConstants.HTML, HTMLorderInfoSection.toString());
		sections.put(BuildEmailConstants.PLAIN_TEXT, orderInfoSection.toString());

		return sections;
	}

	public HashMap getOrderInfoSection(OrderVO order) throws Exception
	{
		HashMap sections = new HashMap();
		float pa;
		float da;
		HashMap tmpHashMap = null;
		String tmpStr = "";
		StringBuffer orderInfoSection = new StringBuffer();
		StringBuffer HTMLorderInfoSection = new StringBuffer();
		OrderDetailsVO tmpDetailVO = null;
		RecipientsVO tmpRecipientsVO = null;
		RecipientAddressesVO tmpRecipientAddressesVO = null;
		RecipientPhonesVO tmpRecipientPhoneVO = null;
		PaymentsVO tmpPaymentsVO = null;
		CreditCardsVO tmpCreditCardsVO = null;
		AddOnsVO tmpAddOnsVO = null;
		boolean isMilesPointsOrder = false;
		String mpRedemptionRateAmt = order.getMpRedemptionRateAmt();
		AddOnUtility addOnUtility = new AddOnUtility();

		//cu = ConfigurationUtil.getInstance();

		//Start---Same day upcharge fields display
		boolean separateLineSameDayFee = false;
		String displaySameDayUpcharge;
		String gDisplaySameDayUpcharge;
		RecalculateOrderSourceCodeVO recalculateOrderSourceCodeVO = new RecalculateOrderSourceCodeVO();
		recalculateOrderSourceCodeVO = getSourceCodeDetails(order.getSourceCode());
		displaySameDayUpcharge = recalculateOrderSourceCodeVO.getDisplaySameDayUpcharge();
		gDisplaySameDayUpcharge = CacheUtil.getInstance().getGlobalParm("FTDAPPS_PARMS", "SAME_DAY_UPCHARGE_DISPLAY");
		if(displaySameDayUpcharge.equalsIgnoreCase("Y") || 
				(displaySameDayUpcharge.equalsIgnoreCase("D") && gDisplaySameDayUpcharge.equalsIgnoreCase("Y"))) {       
			separateLineSameDayFee = true;      
		}
		//End-----Same day upcharge fields display

		Document orderDocument = DOMUtil.getDefaultDocument();
		Element ordersElement = orderDocument.createElement("orders");
		orderDocument.appendChild(ordersElement);

		try {
			if( mpRedemptionRateAmt!= null && !mpRedemptionRateAmt.equals("") && Double.valueOf(mpRedemptionRateAmt) > 0) {
				isMilesPointsOrder = true;
			}
		} catch (Exception e) {
			// Ignore the exception and treat the email as non-miles order.
		}

		orderInfoSection.append("\n");
		HTMLorderInfoSection.append(this.startHTML());



		orderInfoSection.append(Utils.line() + Utils.end());

		for(int i = 0; i < order.getOrderDetail().size(); i++)
		{
			tmpDetailVO = (OrderDetailsVO)(order.getOrderDetail().get(i));
			tmpRecipientsVO = (RecipientsVO)(tmpDetailVO.getRecipients().get(0));
			tmpRecipientAddressesVO = (RecipientAddressesVO)(tmpRecipientsVO.getRecipientAddresses().get(0));
			tmpRecipientPhoneVO = (RecipientPhonesVO)(tmpRecipientsVO.getRecipientPhones().get(0));

			HTMLorderInfoSection.append(startHTMLTable());

			orderInfoSection.append(Utils.addSpacesToLine("Order Number:") + getOrderNumber(tmpDetailVO, order.getOrderOrigin()) + Utils.end() + Utils.end());
			HTMLorderInfoSection.append(addStandardUpperRow("Order Number:", getOrderNumber(tmpDetailVO, order.getOrderOrigin())) + HTMLend());
			orderInfoSection.append(Utils.addSpacesToLine("Order Date:") + Utils.convertDate(order.getOrderDate(), 1) + Utils.end());
			HTMLorderInfoSection.append(addStandardUpperRow("Order Date:", Utils.convertDate(order.getOrderDate(), 1)));

			Element detailsElement = orderDocument.createElement("order");
			ordersElement.appendChild(detailsElement);

			Element nameElement = orderDocument.createElement("orderNumber");
			nameElement.appendChild(orderDocument.createTextNode(getOrderNumber(tmpDetailVO, order.getOrderOrigin())));
			detailsElement.appendChild(nameElement);
			nameElement = orderDocument.createElement("orderDate");
			nameElement.appendChild(orderDocument.createTextNode(Utils.convertDate(order.getOrderDate(), 1)));
			detailsElement.appendChild(nameElement);

			String productType = tmpDetailVO.getProductType();
			logger.debug("ProductType : "+tmpDetailVO.getProductType());

			if(!PRODUCT_TYPE_SERVICES.equals(productType))
			{
				if(tmpDetailVO.getShipMethod() != null && tmpDetailVO.getShipMethod().equals("GR"))
				{
					String deliveryDate = Utils.convertDate(tmpDetailVO.getDeliveryDate(),3);
					orderInfoSection.append(Utils.addSpacesToLine("Delivery On or Before:") + deliveryDate + Utils.end());
					HTMLorderInfoSection.append(addStandardUpperRow("Delivery On or Before:", deliveryDate));

					nameElement = orderDocument.createElement("deliveryDateLabel");
					nameElement.appendChild(orderDocument.createTextNode("Delivery On or Before:"));
					detailsElement.appendChild(nameElement);
					nameElement = orderDocument.createElement("deliveryDate");
					nameElement.appendChild(orderDocument.createTextNode(deliveryDate));
					detailsElement.appendChild(nameElement);
				}
				else
				{
					if(tmpDetailVO.getDeliveryDateRangeEnd() != null && !tmpDetailVO.getDeliveryDateRangeEnd().equals(""))
					{
						String deliveryDate = Utils.convertDate(tmpDetailVO.getDeliveryDate(),3) + " - " + Utils.convertDate(tmpDetailVO.getDeliveryDateRangeEnd(), 2);
						orderInfoSection.append(Utils.addSpacesToLine("Delivery Date:") +  deliveryDate + Utils.end());
						HTMLorderInfoSection.append(addStandardUpperRow("Delivery Date:", deliveryDate));

						nameElement = orderDocument.createElement("deliveryDateLabel");
						nameElement.appendChild(orderDocument.createTextNode("Delivery Date:"));
						detailsElement.appendChild(nameElement);
						nameElement = orderDocument.createElement("deliveryDate");
						nameElement.appendChild(orderDocument.createTextNode(deliveryDate));
						detailsElement.appendChild(nameElement);
					}
					else
					{
						String deliveryDate = Utils.convertDate(tmpDetailVO.getDeliveryDate(),3);
						orderInfoSection.append(Utils.addSpacesToLine("Delivery On:") + deliveryDate + Utils.end());
						HTMLorderInfoSection.append(addStandardUpperRow("Delivery On:", deliveryDate));

						nameElement = orderDocument.createElement("deliveryDateLabel");
						nameElement.appendChild(orderDocument.createTextNode("Delivery On:"));
						detailsElement.appendChild(nameElement);
						nameElement = orderDocument.createElement("deliveryDate");
						nameElement.appendChild(orderDocument.createTextNode(deliveryDate));
						detailsElement.appendChild(nameElement);
					}
				}
				orderInfoSection.append(Utils.addSpacesToLine("Delivery To:") + tmpRecipientsVO.getFirstName() + " " + tmpRecipientsVO.getLastName() + Utils.end());
				HTMLorderInfoSection.append(addStandardUpperRow("Delivery To:", tmpRecipientsVO.getFirstName() + " " + tmpRecipientsVO.getLastName()));
				nameElement = orderDocument.createElement("recipientFirstName");
				nameElement.appendChild(orderDocument.createTextNode(tmpRecipientsVO.getFirstName()));
				detailsElement.appendChild(nameElement);
				nameElement = orderDocument.createElement("recipientLastName");
				nameElement.appendChild(orderDocument.createTextNode(tmpRecipientsVO.getLastName()));
				detailsElement.appendChild(nameElement);

				String addressType = Utils.translateAddressType(tmpRecipientAddressesVO.getAddressType());
				orderInfoSection.append(Utils.addSpacesToLine("") + addressType + Utils.end());
				HTMLorderInfoSection.append(addStandardUpperRow("&nbsp;", addressType));
				nameElement = orderDocument.createElement("deliveryLocationType");
				nameElement.appendChild(orderDocument.createTextNode(addressType));
				detailsElement.appendChild(nameElement);

				if(!Utils.translateAddressType(tmpRecipientAddressesVO.getAddressType()).equals("Residence"))
				{

					if(tmpRecipientAddressesVO.getName() != null && !tmpRecipientAddressesVO.getName().equals("")) {
						orderInfoSection.append(Utils.addSpacesToLine("") + tmpRecipientAddressesVO.getName() + Utils.end());
					    HTMLorderInfoSection.append(addStandardUpperRow("&nbsp;", tmpRecipientAddressesVO.getName()));
					    nameElement = orderDocument.createElement("deliveryLocationName");
					    nameElement.appendChild(orderDocument.createTextNode(tmpRecipientAddressesVO.getName()));
					    detailsElement.appendChild(nameElement);
					}
				}
				if(tmpRecipientAddressesVO.getAddressLine1().length() <= 30)
				{
					orderInfoSection.append(Utils.addSpacesToLine("") + tmpRecipientAddressesVO.getAddressLine1() + Utils.end());
					HTMLorderInfoSection.append(addStandardUpperRow("&nbsp;", tmpRecipientAddressesVO.getAddressLine1()));
				}
				else
				{
					ArrayList addressLines = Utils.splitAddressLine(tmpRecipientAddressesVO.getAddressLine1());
					for(int b = 0; b < addressLines.size(); b++)
					{
						orderInfoSection.append(Utils.addSpacesToLine("") + (String)(addressLines.get(b)) + Utils.end());
						HTMLorderInfoSection.append(addStandardUpperRow("&nbsp;", (String)(addressLines.get(b))));
					}
				}
				nameElement = orderDocument.createElement("recipientAddress1");
				nameElement.appendChild(orderDocument.createTextNode(tmpRecipientAddressesVO.getAddressLine1()));
				detailsElement.appendChild(nameElement);

				if(tmpRecipientAddressesVO.getAddressLine2() != null && !tmpRecipientAddressesVO.getAddressLine2().equals(""))
				{
					if(tmpRecipientAddressesVO.getAddressLine2().length() <=30)
					{
						orderInfoSection.append(Utils.addSpacesToLine("") + tmpRecipientAddressesVO.getAddressLine2() + Utils.end());
						HTMLorderInfoSection.append(addStandardUpperRow("&nbsp;", tmpRecipientAddressesVO.getAddressLine2()));
					}
					else
					{
						ArrayList addressLinesTwo = Utils.splitAddressLine(tmpRecipientAddressesVO.getAddressLine2());
						for(int q = 0;q < addressLinesTwo.size();q++)
						{
							orderInfoSection.append(Utils.addSpacesToLine("") + (String)(addressLinesTwo.get(q)) + Utils.end());
							HTMLorderInfoSection.append(addStandardUpperRow("&nbsp;", (String)(addressLinesTwo.get(q))));
						}
					}
					nameElement = orderDocument.createElement("recipientAddress2");
					nameElement.appendChild(orderDocument.createTextNode(tmpRecipientAddressesVO.getAddressLine2()));
					detailsElement.appendChild(nameElement);
				}

				if((tmpRecipientAddressesVO.getCountry() != null && tmpRecipientAddressesVO.getCountry().equalsIgnoreCase("US")) ||
						(tmpRecipientAddressesVO.getCountry() != null && tmpRecipientAddressesVO.getCountry().equalsIgnoreCase("CA")))
				{
					orderInfoSection.append( Utils.addSpacesToLine("") +
							tmpRecipientAddressesVO.getCity() + ", " +
							tmpRecipientAddressesVO.getStateProvince() + " " +
							Utils.fixZipCode(tmpRecipientAddressesVO.getPostalCode()) + Utils.end());
					HTMLorderInfoSection.append(addStandardUpperRow("&nbsp;",
							tmpRecipientAddressesVO.getCity() + ",&nbsp;" +
							tmpRecipientAddressesVO.getStateProvince() + "&nbsp;" +
							Utils.fixZipCode(tmpRecipientAddressesVO.getPostalCode())));
					nameElement = orderDocument.createElement("recipientCity");
					nameElement.appendChild(orderDocument.createTextNode(tmpRecipientAddressesVO.getCity()));
					detailsElement.appendChild(nameElement);
					nameElement = orderDocument.createElement("recipientState");
					nameElement.appendChild(orderDocument.createTextNode(tmpRecipientAddressesVO.getStateProvince()));
					detailsElement.appendChild(nameElement);
					nameElement = orderDocument.createElement("recipientZipCode");
					nameElement.appendChild(orderDocument.createTextNode(Utils.fixZipCode(tmpRecipientAddressesVO.getPostalCode())));
					detailsElement.appendChild(nameElement);
				}
				else
				{
					orderInfoSection.append( Utils.addSpacesToLine("") + tmpRecipientAddressesVO.getCity() + Utils.end());
					HTMLorderInfoSection.append(addStandardUpperRow("&nbsp;", tmpRecipientAddressesVO.getCity()));
					nameElement = orderDocument.createElement("recipientCity");
					nameElement.appendChild(orderDocument.createTextNode(tmpRecipientAddressesVO.getCity()));
					detailsElement.appendChild(nameElement);
				}

				String countryType = Utils.translateCountryType(tmpRecipientAddressesVO.getCountry());
				orderInfoSection.append(Utils.addSpacesToLine("") + countryType + Utils.end());
				HTMLorderInfoSection.append(addStandardUpperRow("&nbsp;", countryType));
				nameElement = orderDocument.createElement("recipientCountry");
				nameElement.appendChild(orderDocument.createTextNode(countryType));
				detailsElement.appendChild(nameElement);

				if(tmpRecipientPhoneVO.getPhoneNumber() != null)
				{
					orderInfoSection.append(Utils.addSpacesToLine("") + tmpRecipientPhoneVO.getPhoneNumber().replace(' ','-') + Utils.end() + Utils.end());
					HTMLorderInfoSection.append(addStandardUpperRow("&nbsp;", tmpRecipientPhoneVO.getPhoneNumber().replace(' ','-')));
					nameElement = orderDocument.createElement("recipientPhoneNumber");
					nameElement.appendChild(orderDocument.createTextNode(tmpRecipientPhoneVO.getPhoneNumber().replace(' ','-')));
					detailsElement.appendChild(nameElement);
				}

				String giftMessageTxt = "";
				String giftMessageHtml = "";
				if(tmpDetailVO.getCardMessage() != null)
				{
					giftMessageTxt += tmpDetailVO.getCardMessage();
					giftMessageHtml += tmpDetailVO.getCardMessage();
				}
				if(tmpDetailVO.getCardSignature() != null){
					giftMessageTxt += ("  " + tmpDetailVO.getCardSignature());
					giftMessageHtml += ("&nbsp;&nbsp;" + tmpDetailVO.getCardSignature());
				}
				ArrayList linesTxt = splitText(giftMessageTxt);
				ArrayList linesHtml = splitText(giftMessageHtml);
				orderInfoSection.append(Utils.addSpacesToLine("Gift Message:") + ((String)linesTxt.get(0)).trim() + Utils.end());
				HTMLorderInfoSection.append(HTMLend());
				HTMLorderInfoSection.append(addStandardUpperRow("Gift Message:", ((String)linesHtml.get(0)).trim()));

				if(linesTxt.size() > 1)
				{
					for(int j = 1; j < linesTxt.size(); j++)
					{
						orderInfoSection.append(Utils.addSpacesToLine("") + (String)linesTxt.get(j) + Utils.end());
					}
				}
				if(linesHtml.size() > 1)
				{
					for(int j = 1; j < linesHtml.size(); j++)
					{
						HTMLorderInfoSection.append(addStandardUpperRow("&nbsp;", (String)linesHtml.get(j)));
					}
				}
				nameElement = orderDocument.createElement("giftMessage");
				nameElement.appendChild(orderDocument.createTextNode(giftMessageHtml));
				detailsElement.appendChild(nameElement);
			}
			/* Added personalization to confirmation e-mail. */
			if(tmpDetailVO.getPersonalizationData() != null && !tmpDetailVO.getPersonalizationData().equals("")) {
				String personalization = null;
				Document doc = DOMUtil.getDocument(tmpDetailVO.getPersonalizationData());

				Node itemNode =  DOMUtil.selectSingleNode(doc, "personalizations/personalization/name/text()");
				if(itemNode != null) {
					NodeList names = DOMUtil.selectNodes(doc, "personalizations/personalization/name/text()");
					NodeList data = DOMUtil.selectNodes(doc, "personalizations/personalization/data/text()");

					/* Get personalized portion only.
					 *    Ex: date=January 1, 2000  -->  January 1, 2000
					 *        name=glenn wil lewis  -->  glenn wil lewis
					 */
	          	    nameElement = orderDocument.createElement("personalizationData");
	        	    detailsElement.appendChild(nameElement);
					HTMLorderInfoSection.append(HTMLend());
					for(int j = 0; j < names.getLength(); j++) {
						personalization = data.item(j).getNodeValue();
						/* If it is not the first personalization, do not display the label. */
						if(j > 0) {
							orderInfoSection.append(Utils.addSpacesToLine("") + personalization + Utils.end());
							HTMLorderInfoSection.append(addStandardUpperRow("&nbsp;", personalization));
						}
						/* Else, display "Personalization: " as the label. */
						else {
							orderInfoSection.append(Utils.addSpacesToLine("Personalization:") + personalization + Utils.end());
							HTMLorderInfoSection.append(addStandardUpperRow("Personalization:", personalization));
						}
		        	    Element personalElement = orderDocument.createElement("personalization");
		        	    personalElement.appendChild(orderDocument.createTextNode(personalization));
		        	    nameElement.appendChild(personalElement);
					}
				}
			}

			orderInfoSection.append(Utils.end());
			HTMLorderInfoSection.append(HTMLend());
			HTMLorderInfoSection.append(endHTMLTable());

			//SPECIAL INSTRUCTIONS


			if(tmpDetailVO.getSpecialInstructions() != null && !tmpDetailVO.getSpecialInstructions().equals(""))
			{
				HTMLorderInfoSection.append(this.startHTMLTable());
				orderInfoSection.append("Additional Recipient Information:\n");
				HTMLorderInfoSection.append(addSpecialInstructionsRow("Additional Recipient Information:"));

				orderInfoSection.append(Utils.addSpacesToLine("Special Instructions :") + tmpDetailVO.getSpecialInstructions());
				HTMLorderInfoSection.append(addStandardLowerRow("Special Instructions :" + tmpDetailVO.getSpecialInstructions(),"&nbsp;"));
				
				nameElement = orderDocument.createElement("specialInstructions");
				nameElement.appendChild(orderDocument.createTextNode(tmpDetailVO.getSpecialInstructions()));
        	    detailsElement.appendChild(nameElement);
        	    Element instructionElement = orderDocument.createElement("instruction");
        	    instructionElement.appendChild(orderDocument.createTextNode("Additional Recipient Information:"));
        	    nameElement.appendChild(instructionElement);

				if(!tmpRecipientAddressesVO.getAddressType().equalsIgnoreCase("FUNERAL HOME"))
				{
					orderInfoSection.append("PLEASE NOTE: We cannot honor requests for delivery at\nspecific times of day." + Utils.end());
					HTMLorderInfoSection.append(addSpecialInstructionsRow("PLEASE NOTE: We cannot honor requests for delivery at"));
					HTMLorderInfoSection.append(addSpecialInstructionsRow("specific times of day."));
	        	    instructionElement = orderDocument.createElement("instruction");
	        	    instructionElement.appendChild(orderDocument.createTextNode("PLEASE NOTE: We cannot honor requests for delivery at specific times of day."));
	        	    nameElement.appendChild(instructionElement);
				}
				
								
				orderInfoSection.append(Utils.end());
				HTMLorderInfoSection.append("<tr><td>&nbsp;</td></tr>");
				HTMLorderInfoSection.append(endHTMLTable());
				HTMLorderInfoSection.append("<br>");
			}


			//PRODUCT DESCRIPTION AND PRICE

			HTMLorderInfoSection.append(this.startHTMLTable());
			orderInfoSection.append(Utils.justifyToLine(cleanHTMLEncoding(tmpDetailVO.getItemDescription()), "" + 
					Utils.formatDollars(tmpDetailVO.getProductsAmount())) + 
					formatMilesPoints(isMilesPointsOrder, tmpDetailVO.getMilesPointsAmtProduct()) +
					Utils.end());
			HTMLorderInfoSection.append(addStandardLowerRow(tmpDetailVO.getItemDescription(), "" + 
					Utils.formatDollars(tmpDetailVO.getProductsAmount()) , "" +
					formatMilesPoints(isMilesPointsOrder, tmpDetailVO.getMilesPointsAmtProduct())
			));
			nameElement = orderDocument.createElement("productId");
			nameElement.appendChild(orderDocument.createTextNode(tmpDetailVO.getProductId()));
			detailsElement.appendChild(nameElement);
			nameElement = orderDocument.createElement("websiteProductId");
			nameElement.appendChild(orderDocument.createTextNode(tmpDetailVO.getNovatorId()));
			detailsElement.appendChild(nameElement);
			nameElement = orderDocument.createElement("productDescription");
			nameElement.appendChild(orderDocument.createTextNode(tmpDetailVO.getItemDescription()));
			detailsElement.appendChild(nameElement);
			nameElement = orderDocument.createElement("productPrice");
			nameElement.appendChild(orderDocument.createTextNode(Utils.formatNumber(tmpDetailVO.getProductsAmount())));
			detailsElement.appendChild(nameElement);
			if (isMilesPointsOrder) {
			    nameElement = orderDocument.createElement("milesPointsAmount");
			    nameElement.appendChild(orderDocument.createTextNode(formatMilesPoints(isMilesPointsOrder, tmpDetailVO.getMilesPointsAmtProduct())));
			    detailsElement.appendChild(nameElement);
			}
			nameElement = orderDocument.createElement("productType");
			nameElement.appendChild(orderDocument.createTextNode(tmpDetailVO.getProductType()));
			detailsElement.appendChild(nameElement);

			orderInfoSection.append(tmpDetailVO.getProductId());

			tmpStr = getColorSizeFlag(tmpDetailVO.getProductId());
			//COLOR
			if(tmpStr != null && tmpStr.equalsIgnoreCase("C"))
			{
				tmpHashMap = this.getColorDescription();
				orderInfoSection.append(" ");
				if(tmpHashMap != null && tmpHashMap.size() > 0)
				{
					orderInfoSection.append((String)tmpHashMap.get(tmpDetailVO.getColorFirstChoice()) + "\n2nd Choice - " + (String)tmpHashMap.get(tmpDetailVO.getColorSecondChoice()));
					HTMLorderInfoSection.append(addStandardLowerRow(tmpDetailVO.getProductId() + "&nbsp;" + (String)tmpHashMap.get(tmpDetailVO.getColorFirstChoice()), "&nbsp;"));
					HTMLorderInfoSection.append(addStandardLowerRow("2nd&nbspChoice&nbsp-&nbsp" + (String)tmpHashMap.get(tmpDetailVO.getColorSecondChoice())  , "&nbsp"));

					Element choiceElement = orderDocument.createElement("firstChoiceColor");
					detailsElement.appendChild(choiceElement);
					choiceElement.appendChild(orderDocument.createTextNode((String)tmpHashMap.get(tmpDetailVO.getColorFirstChoice())));
					choiceElement = orderDocument.createElement("secondChoiceColor");
					detailsElement.appendChild(choiceElement);
					choiceElement.appendChild(orderDocument.createTextNode((String)tmpHashMap.get(tmpDetailVO.getColorSecondChoice())));
				}
				else
				{
					orderInfoSection.append(tmpDetailVO.getColorFirstChoice() + " \n2nd Choice - " + tmpDetailVO.getColorSecondChoice());
					HTMLorderInfoSection.append(addStandardLowerRow(tmpDetailVO.getProductId() + "&nbsp" + tmpDetailVO.getColorFirstChoice(), "&nbsp;"));
					HTMLorderInfoSection.append(addStandardLowerRow("2nd&nbspChoice&nbsp-&nbsp" +  tmpDetailVO.getColorSecondChoice(), "&nbsp;"));

					Element choiceElement = orderDocument.createElement("firstChoiceColor");
					detailsElement.appendChild(choiceElement);
					choiceElement.appendChild(orderDocument.createTextNode(tmpDetailVO.getColorFirstChoice()));
					choiceElement = orderDocument.createElement("secondChoiceColor");
					detailsElement.appendChild(choiceElement);
					choiceElement.appendChild(orderDocument.createTextNode(tmpDetailVO.getColorSecondChoice()));
				}
			}
			else
			{
				HTMLorderInfoSection.append(addStandardLowerRow(tmpDetailVO.getProductId(), "&nbsp;"));

			}

			//adding personal greeting identifier and id
			if (tmpDetailVO.getPersonalGreetingId() != null && tmpDetailVO.getPersonalGreetingId() != "" )
			{
				String greetingText = cu.getContent(this.connection ,BuildEmailConstants.PERSONAL_GREETING_CONTEXT , BuildEmailConstants.PERSONAL_GREETING_IDENTIFIER);
				orderInfoSection.append(Utils.end() + greetingText +  "\t" + tmpDetailVO.getPersonalGreetingId() + Utils.end());
				HTMLorderInfoSection.append(addStandardLowerRow(greetingText +  "\t" + tmpDetailVO.getPersonalGreetingId() , "&nbsp;"));                

				Element greetingElement = orderDocument.createElement("personalGreetingId");
				detailsElement.appendChild(greetingElement);
				greetingElement.appendChild(orderDocument.createTextNode(tmpDetailVO.getPersonalGreetingId()));
				greetingElement = orderDocument.createElement("personalGreetingIdText");
				detailsElement.appendChild(greetingElement);
				greetingElement.appendChild(orderDocument.createTextNode(greetingText));
			}

			orderInfoSection.append(Utils.end());
			HTMLorderInfoSection.append(HTMLend());

			Element addonsElement = orderDocument.createElement("addons");
			detailsElement.appendChild(addonsElement);
			for(int y=0; y < tmpDetailVO.getAddOns().size(); y++)
			{
				tmpAddOnsVO = (AddOnsVO)tmpDetailVO.getAddOns().get(y);
				String totalPriceString = "";
				//ADDONS
				if(tmpAddOnsVO.getAddOnCode() != null && tmpAddOnsVO.getAddOnCode().startsWith("RC"))
				{
					orderInfoSection.append(Utils.justifyToLine("Greeting Card Qty " + 
							tmpAddOnsVO.getAddOnQuantity(), "" + 
							Utils.formatDollars(tmpAddOnsVO.getPrice())) + 
							formatMilesPoints(isMilesPointsOrder, tmpDetailVO.getMilesPointsAmtAddon()) +
							Utils.end());
					HTMLorderInfoSection.append(addStandardLowerRow("Greeting Card Qty " + 
							tmpAddOnsVO.getAddOnQuantity(), "" + 
							Utils.formatDollars(tmpAddOnsVO.getPrice()), "" +
							formatMilesPoints(isMilesPointsOrder, tmpDetailVO.getMilesPointsAmtAddon())));
					BigDecimal totalPrice = new BigDecimal(tmpAddOnsVO.getPrice());
					totalPriceString = totalPrice.toString();
				} 
				else
				{
					AddOnVO addOnVO = null;
					if (tmpAddOnsVO.getAddOnCode() != null)
					{
						addOnVO = addOnUtility.getAddOnById(tmpAddOnsVO.getAddOnCode(),false,false, this.connection);
					}
					BigDecimal totalPrice = new BigDecimal(tmpAddOnsVO.getPrice());
					totalPrice = totalPrice.multiply(new BigDecimal(tmpAddOnsVO.getAddOnQuantity()));
					totalPriceString = totalPrice.toString();

					String quantityString = addOnVO != null ? AddOnVO.ADD_ON_TYPE_VASE_ID.toString().equalsIgnoreCase(addOnVO.getAddOnTypeId()) ? "" : " Qty " + tmpAddOnsVO.getAddOnQuantity() : " Qty " + tmpAddOnsVO.getAddOnQuantity();
					orderInfoSection.append(Utils.justifyToLine("" + tmpAddOnsVO.getDescription() + 
							quantityString, "" +
							addOnVO == null ? Utils.formatDollars(tmpAddOnsVO.getPrice()) : addOnVO.getDisplayPriceFlag() ? Utils.formatDollars(totalPriceString) : "") + 
							formatMilesPoints(isMilesPointsOrder, tmpDetailVO.getMilesPointsAmtAddon()) +
							Utils.end());
					HTMLorderInfoSection.append(addStandardLowerRow(tmpAddOnsVO.getDescription() + 
							quantityString, "" +
							addOnVO == null ? Utils.formatDollars(tmpAddOnsVO.getPrice()) : addOnVO.getDisplayPriceFlag() ? Utils.formatDollars(totalPriceString) : "" +
									formatMilesPoints(isMilesPointsOrder, tmpDetailVO.getMilesPointsAmtAddon())
					));
				}
				Element addonElement = orderDocument.createElement("addon");
				addonsElement.appendChild(addonElement);
				Element addonDetailElement = orderDocument.createElement("addonId");
				addonElement.appendChild(addonDetailElement);
				addonDetailElement.appendChild(orderDocument.createTextNode(tmpAddOnsVO.getAddOnCode()));
				addonDetailElement = orderDocument.createElement("addonDescription");
				addonElement.appendChild(addonDetailElement);
				addonDetailElement.appendChild(orderDocument.createTextNode(tmpAddOnsVO.getDescription()));
				addonDetailElement = orderDocument.createElement("addonQty");
				addonElement.appendChild(addonDetailElement);
				addonDetailElement.appendChild(orderDocument.createTextNode(tmpAddOnsVO.getAddOnQuantity()));
				addonDetailElement = orderDocument.createElement("addonPrice");
				addonElement.appendChild(addonDetailElement);
				addonDetailElement.appendChild(orderDocument.createTextNode(Utils.formatNumber(totalPriceString)));
				if (isMilesPointsOrder) {
					addonDetailElement = orderDocument.createElement("milesPointsAddonAmount");
					addonElement.appendChild(addonDetailElement);
					addonDetailElement.appendChild(orderDocument.createTextNode(formatMilesPoints(isMilesPointsOrder, tmpDetailVO.getMilesPointsAmtAddon())));
				}

			}
			orderInfoSection.append(Utils.end());
			HTMLorderInfoSection.append(HTMLend());
			//DISCOUNT
			if(tmpDetailVO.getDiscountAmount() != null && !tmpDetailVO.getDiscountAmount().startsWith("0"))
			{
				orderInfoSection.append(Utils.justifyToLine("Discount:", "(" + Utils.formatDollars(tmpDetailVO.getDiscountAmount())) + ")" + Utils.end());
				HTMLorderInfoSection.append(addStandardLowerRow("Discount:","(" + Utils.formatDollars(tmpDetailVO.getDiscountAmount()) + ")"));

				Element discountAmountElement = orderDocument.createElement("discountAmount");
				detailsElement.appendChild(discountAmountElement);
				discountAmountElement.appendChild(orderDocument.createTextNode(Utils.formatNumber(tmpDetailVO.getDiscountAmount())));
			}
			//SHIPPING + SERVICE CHARGES
			BigDecimal totalFees = new BigDecimal(0);

			if(tmpDetailVO.getShippingFeeAmount() != null && !tmpDetailVO.getShippingFeeAmount().startsWith("0"))
			{
				BigDecimal shipFee = new BigDecimal( tmpDetailVO.getShippingFeeAmount());
				totalFees = totalFees.add(shipFee);
			}

			if(tmpDetailVO.getServiceFeeAmount() != null && !tmpDetailVO.getServiceFeeAmount().startsWith("0"))
			{
				BigDecimal serviceFee = new BigDecimal( tmpDetailVO.getServiceFeeAmount());
				//Same day upcharge code. If separateLineSameDayFee is true and has same day upcharge then subtract from service fee and then add
				//service fee to total fees. Or else service fee contains same day upcharge fees included. 
				if(separateLineSameDayFee && tmpDetailVO.getSameDayUpcharge() != null && (Float.parseFloat(tmpDetailVO.getSameDayUpcharge()) > 0)){
					serviceFee = serviceFee.subtract(new BigDecimal(tmpDetailVO.getSameDayUpcharge()));            	
				}
				totalFees = totalFees.add(serviceFee);                      
			}
		
			// subtract Morning Delivery Fees from shipping/service fees if separateLineMorningDeliveryFee is Y and if morningDeliveryFee is greater than or equal to 0
			if(tmpDetailVO.getMorningDeliveryFee() != null && (Float.parseFloat(tmpDetailVO.getMorningDeliveryFee()) > 0)){
				if(totalFees.doubleValue() > 0)
				{
					totalFees = totalFees.subtract(new BigDecimal(tmpDetailVO.getMorningDeliveryFee()));
				}
			}

			//subtract Surcharge Fees  from shipping/service fee if displaySurcharge flag is Y and we will display separately from shipping/service fee       
			if ( StringUtils.equalsIgnoreCase(tmpDetailVO.getDisplaySurcharge(), "Y")  && StringUtils.equalsIgnoreCase(tmpDetailVO.getApplySurchargeCode(), "ON"))
			{
				BigDecimal fuelSurchargeFee = new BigDecimal( tmpDetailVO.getFuelSurcharge());
				if(totalFees.doubleValue() > 0)
				{
					totalFees = totalFees.subtract(fuelSurchargeFee);
				}             
			}
			
			// subtract Sunday Upcharge from shipping/service fees if separateLineSundayCharge is Y and if sundayUpcharge is greater than 0
			if(tmpDetailVO.getSundayUpcharge() !=null && (Float.parseFloat(tmpDetailVO.getSundayUpcharge()) > 0)){
				if(totalFees.doubleValue() > 0)
				{
					totalFees = totalFees.subtract(new BigDecimal(tmpDetailVO.getSundayUpcharge()));
				}
			}

			if(totalFees.doubleValue() > 0)
			{
				orderInfoSection.append(Utils.justifyToLine("Shipping/Service Fees:", "" + Utils.formatDollars(totalFees.doubleValue())) + Utils.end());
				HTMLorderInfoSection.append(addStandardLowerRow("Shipping/Service Fees:", "" + Utils.formatDollars(totalFees.doubleValue())));

				Element shippingServiceFeeElement = orderDocument.createElement("shippingServiceFee");
				detailsElement.appendChild(shippingServiceFeeElement);
				shippingServiceFeeElement.appendChild(orderDocument.createTextNode(Utils.formatNumber(totalFees.toString())));
			}

			//Start---Same day upcharge fields display
			logger.debug("In BuildOrderEmailHelper getOrderInfoSection displaySameDayUpcharge value is "+ displaySameDayUpcharge);
			logger.debug("In BuildOrderEmailHelper getOrderInfoSection global displaySameDayUpcharge value is "+ gDisplaySameDayUpcharge);
			logger.debug("In BuildOrderEmailHelper getOrderInfoSection sameDayUpcharge fee value is "+ tmpDetailVO.getSameDayUpcharge());
			if(separateLineSameDayFee && tmpDetailVO.getSameDayUpcharge() != null && (Float.parseFloat(tmpDetailVO.getSameDayUpcharge()) > 0)){
				String sameDayUpchargeLabel = CacheUtil.getInstance().getContentWithFilter("SAME_DAY_UPCHARGE", "CONFIRMATION_EMAIL_LABEL", null, null, false);
				logger.debug("In BuildOrderEmailHelper getOrderInfoSection sameDayUpcharge label is "+ sameDayUpchargeLabel);
				orderInfoSection.append(Utils.justifyToLine(sameDayUpchargeLabel+ ":", "" + Utils.formatDollars(tmpDetailVO.getSameDayUpcharge())) + Utils.end());
				HTMLorderInfoSection.append(addStandardLowerRow(sameDayUpchargeLabel+ ":", "" + Utils.formatDollars(tmpDetailVO.getSameDayUpcharge())) + Utils.end());

				Element sameDayDeliveryElement = orderDocument.createElement("sameDayUpchargeLabel");
				detailsElement.appendChild(sameDayDeliveryElement);
				sameDayDeliveryElement.appendChild(orderDocument.createTextNode(sameDayUpchargeLabel));
				sameDayDeliveryElement = orderDocument.createElement("sameDayUpcharge");
				detailsElement.appendChild(sameDayDeliveryElement);
				sameDayDeliveryElement.appendChild(orderDocument.createTextNode(Utils.formatNumber(tmpDetailVO.getSameDayUpcharge())));
			}
			//End-----Same day upcharge fields display
			
			//Start---Sunday Upcharge fields display         
			logger.debug("In BuildOrderEmailHelper getOrderInfoSection sundayUpcharge value is "+ tmpDetailVO.getSundayUpcharge());

			if(tmpDetailVO.getSundayUpcharge() !=null && (Float.parseFloat(tmpDetailVO.getSundayUpcharge()) > 0)){
				String sundayUpchargeLabel = CacheUtil.getInstance().getContentWithFilter("SUNDAY_UPCHARGE", "CONFIRMATION_EMAIL_LABEL", null, null, false);
				orderInfoSection.append(Utils.justifyToLine(sundayUpchargeLabel + ":","" + Utils.formatDollars(tmpDetailVO.getSundayUpcharge())) + Utils.end());
				HTMLorderInfoSection.append(addStandardLowerRow(sundayUpchargeLabel + ":","" + Utils.formatDollars(tmpDetailVO.getSundayUpcharge())) + Utils.end());

				Element sundayUpchargeElement = orderDocument.createElement("sundayUpchargeLabel");
				detailsElement.appendChild(sundayUpchargeElement);
				sundayUpchargeElement.appendChild(orderDocument.createTextNode(sundayUpchargeLabel));
				sundayUpchargeElement = orderDocument.createElement("sundayUpcharge");
				detailsElement.appendChild(sundayUpchargeElement);
				sundayUpchargeElement.appendChild(orderDocument.createTextNode(Utils.formatNumber(tmpDetailVO.getSundayUpcharge())));
            }
			//End-----Sunday Upcharge fields display

			//Start---Morning delivery fields display         
			logger.debug("In BuildOrderEmailHelper getOrderInfoSection morningDeliveryFee value is "+ tmpDetailVO.getMorningDeliveryFee());

			if(tmpDetailVO.getMorningDeliveryFee() !=null && (Float.parseFloat(tmpDetailVO.getMorningDeliveryFee()) >= 0)){
				String morningDeliveryFeeLabel = CacheUtil.getInstance().getContentWithFilter("MORNING_DELIVERY", "MORNING_DELIVERY_LABEL", null, null, false);
				orderInfoSection.append(Utils.justifyToLine(morningDeliveryFeeLabel + ":","" + Utils.formatDollars(tmpDetailVO.getMorningDeliveryFee())) + Utils.end());
				HTMLorderInfoSection.append(addStandardLowerRow(morningDeliveryFeeLabel + ":","" + Utils.formatDollars(tmpDetailVO.getMorningDeliveryFee())) + Utils.end());

				Element morningDeliveryElement = orderDocument.createElement("morningDeliveryFeeLabel");
				detailsElement.appendChild(morningDeliveryElement);
				morningDeliveryElement.appendChild(orderDocument.createTextNode(morningDeliveryFeeLabel));
				morningDeliveryElement = orderDocument.createElement("morningDeliveryFee");
				detailsElement.appendChild(morningDeliveryElement);
				morningDeliveryElement.appendChild(orderDocument.createTextNode(Utils.formatNumber(tmpDetailVO.getMorningDeliveryFee())));
            }
			//End-----Morning delivery fields display

			// add the fuel surcharge fee line after shipping/service fee line          
			logger.debug("In BuildOrderEmailHelper getOrderInfoSection getDisplaySurcharge value is "+ tmpDetailVO.getDisplaySurcharge());
			logger.debug("In BuildOrderEmailHelper getOrderInfoSection getApplySurchargeCode value is "+ tmpDetailVO.getApplySurchargeCode());

			if ( StringUtils.equalsIgnoreCase(tmpDetailVO.getDisplaySurcharge(), "Y") && StringUtils.equalsIgnoreCase(tmpDetailVO.getApplySurchargeCode(), "ON"))
			{
				BigDecimal fuelSurchargeFee = new BigDecimal( tmpDetailVO.getFuelSurcharge());// we will show surcharge fee even if its zero 
				String fuelSurchargeDescription = tmpDetailVO.getFuelSurchargeDescription();
				logger.debug("fuelSurchargeDescription is"+ fuelSurchargeDescription);
				orderInfoSection.append(Utils.justifyToLine(fuelSurchargeDescription+ ":", "" + Utils.formatDollars(fuelSurchargeFee.doubleValue())) + Utils.end());
				HTMLorderInfoSection.append(addStandardLowerRow(fuelSurchargeDescription+ ":", "" + Utils.formatDollars(fuelSurchargeFee.doubleValue())) + Utils.end());
				
				Element fuelSurchargeElement = orderDocument.createElement("fuelSurchargeLabel");
				detailsElement.appendChild(fuelSurchargeElement);
				fuelSurchargeElement.appendChild(orderDocument.createTextNode(fuelSurchargeDescription));
				fuelSurchargeElement = orderDocument.createElement("fuelSurcharge");
				detailsElement.appendChild(fuelSurchargeElement);
				fuelSurchargeElement.appendChild(orderDocument.createTextNode(Utils.formatNumber(fuelSurchargeFee.toString())));
			}

			//TAX
			logger.info("Checking TAX Value for Email");
			if(StringUtils.isNotEmpty(tmpDetailVO.getTaxAmount()))
			{
				BigDecimal taxAmt = new BigDecimal(tmpDetailVO.getTaxAmount());
				logger.info("Tax Amount on Order : "+taxAmt);
				if(taxAmt.compareTo(BigDecimal.ZERO) > 0){
					String taxAmount = Utils.formatDollars(tmpDetailVO.getTaxAmount());
					orderInfoSection.append(Utils.justifyToLine("Tax:", "" + taxAmount) + Utils.end());
					HTMLorderInfoSection.append(addStandardLowerRow("Tax:", "" + taxAmount));
					Element orderTotalElement = orderDocument.createElement("taxAmount");
					detailsElement.appendChild(orderTotalElement);
					orderTotalElement.appendChild(orderDocument.createTextNode(Utils.formatNumber(tmpDetailVO.getTaxAmount())));
				}
            }

			orderInfoSection.append(Utils.justifyToLine(Utils.line(16), Utils.line(9)) + Utils.end());
			HTMLorderInfoSection.append(addStandardLowerRow(Utils.line(16), Utils.line(9)));
			//SUBTOTAL
			String itemTotal = Utils.formatDollars(tmpDetailVO.getExternalOrderTotal());
			String rewardTotal = formatMilesPoints(isMilesPointsOrder, tmpDetailVO.getMilesPointsAmt());
			orderInfoSection.append(Utils.justifyToLine("Subtotal", "" + itemTotal + rewardTotal + Utils.end() + Utils.end()));
			HTMLorderInfoSection.append(addStandardLowerRow("Subtotal", "" + itemTotal, "" + rewardTotal));
			if (isMilesPointsOrder) {
			    nameElement = orderDocument.createElement("milesPointsSubTotal");
			    nameElement.appendChild(orderDocument.createTextNode(rewardTotal));
			    detailsElement.appendChild(nameElement);
			}

			Element orderTotalElement = orderDocument.createElement("itemTotal");
			detailsElement.appendChild(orderTotalElement);
			orderTotalElement.appendChild(orderDocument.createTextNode(Utils.formatNumber(tmpDetailVO.getExternalOrderTotal())));

			//LAST MINUT GIFT EMAIL
			if(tmpDetailVO.getLastMinuteGiftEmail() != null && !tmpDetailVO.getLastMinuteGiftEmail().equals(""))
			{
				String lastMinuteGift = "An e-mail will be sent to " + tmpDetailVO.getLastMinuteGiftEmail() + " from " + tmpDetailVO.getLastMinuteGiftSignature();
				orderInfoSection.append(lastMinuteGift);
				HTMLorderInfoSection.append(addStandardLowerRow(lastMinuteGift, "&nbsp;"));

				Element lastMinuteGiftElement = orderDocument.createElement("lastMinuteGift");
				detailsElement.appendChild(lastMinuteGiftElement);
				lastMinuteGiftElement.appendChild(orderDocument.createTextNode(lastMinuteGift));
			}

			HTMLorderInfoSection.append(endHTMLTable());

			orderInfoSection.append(Utils.line());
			HTMLorderInfoSection.append("<br><hr align=\"left\" width=\"580\" noshade size=\"1\"></br>");

		}

		orderInfoSection.append(Utils.end());
		HTMLorderInfoSection.append(startHTMLTable());

		//GIFT CERTIFICATE
		List payments = order.getPayments();
		logger.info("PAyments Size : "+payments.size());
		

		double giftCertTotal = 0;
		boolean hasGiftCert = false;
		if(payments != null)
		{
			for(int i=0;i<payments.size();i++)
			{
				PaymentsVO payment = (PaymentsVO)payments.get(i);
				String giftID = payment.getGiftCertificateId();
				if(giftID != null && giftID.length() > 0)
				{
					hasGiftCert = true;
					// USECASE 23038 - Unlike GC, GD retains balance.
					double giftCertAmount = 0;
					if(payment.getPaymentType().equals("GC")) { 
						giftCertAmount = getGiftCertificateAmount(giftID);									
					} else if(payment.getPaymentType().equals("GD")) { 
						giftCertAmount =  Double.parseDouble(payment.getAmount());
					}
					giftCertTotal = giftCertTotal + giftCertAmount;

					String giftCertString = Utils.formatDollars(giftCertTotal);
					orderInfoSection.append(Utils.justifyToLine("Gift Card/Certificate:", "("+ giftCertString + ")") + Utils.end());
					HTMLorderInfoSection.append(addStandardLowerRow("Gift Card/Certificate:", "("+ giftCertString + ")"));

					Element giftCertElement = orderDocument.createElement("giftCertificateText");
					ordersElement.appendChild(giftCertElement);
					giftCertElement.appendChild(orderDocument.createTextNode("Gift Card/Certificate: ("+ giftCertString + ")"));
				}else{
					tmpPaymentsVO = payment;
					if(!tmpPaymentsVO.getCreditCards().isEmpty())
					{
						tmpCreditCardsVO = (CreditCardsVO)tmpPaymentsVO.getCreditCards().get(0);
					}
					else
					{
						tmpCreditCardsVO = new CreditCardsVO(); //just blank
					}
				}
			}
		}

		//TOTAL CHARGE
		//Get total amount from order object
		double orderTotal = 0;
		if(order.getOrderTotal() != null && order.getOrderTotal().length() > 0)
		{
			orderTotal = Double.parseDouble(order.getOrderTotal());
		}

		//calculate total charge if get certificate used
		if(giftCertTotal > 0)
		{
			if(orderTotal < giftCertTotal)
			{
				orderTotal = 0;
			}
			else
			{
				orderTotal = orderTotal - giftCertTotal;
			}
		}

		//format total for display
		NumberFormat n = NumberFormat.getCurrencyInstance(Locale.US);
		String orderTotalFormatted = n.format(orderTotal);
		String milesTotalFormatted = "";
		if(isMilesPointsOrder) {
			milesTotalFormatted = formatMilesPoints(true, tmpPaymentsVO.getMilesPointsAmt());
		}

		if(recalculateOrderSourceCodeVO.getJcpenneyFlag() != null && recalculateOrderSourceCodeVO.getJcpenneyFlag().equals("Y"))
		{
			orderInfoSection.append(Utils.justifyToLine("Approximate Total:",  orderTotalFormatted) + milesTotalFormatted + Utils.end());
			HTMLorderInfoSection.append(addStandardLowerRow("Approximate Total:",  orderTotalFormatted, milesTotalFormatted));
		}
		else
		{
			orderInfoSection.append(Utils.justifyToLine("Total Charge:", orderTotalFormatted) + milesTotalFormatted + Utils.end());
			HTMLorderInfoSection.append(addStandardLowerRow("Total Charge:", orderTotalFormatted, milesTotalFormatted));

			Element orderTotalElement = orderDocument.createElement("orderTotal");
			ordersElement.appendChild(orderTotalElement);
			orderTotalElement.appendChild(orderDocument.createTextNode(Utils.formatNumber(orderTotal)));

			if (isMilesPointsOrder) {
			    Element nameElement = orderDocument.createElement("milesPointsTotal");
			    nameElement.appendChild(orderDocument.createTextNode(milesTotalFormatted));
			    ordersElement.appendChild(nameElement);
			}
		}

		//CREDIT CARD TYPE
		if(tmpPaymentsVO != null && tmpPaymentsVO.getPaymentMethodType() != null && tmpPaymentsVO.getPaymentMethodType().equalsIgnoreCase("C"))
		{
			//only display if the order total is > 0
			if(orderTotal > 0){
				String ccType = translateCCType(tmpCreditCardsVO.getCCType());
				orderInfoSection.append(Utils.addSpacesToLine("Credit Card Type:") + ccType);
				HTMLorderInfoSection.append(addStandardLowerRow("Credit Card Type:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + ccType,"&nbsp;"));

				Element ccElement = orderDocument.createElement("paymentType");
				ordersElement.appendChild(ccElement);
				ccElement.appendChild(orderDocument.createTextNode(ccType));
}
		}

		//ALT PAY PAYMENT
		if(tmpPaymentsVO != null && tmpPaymentsVO.getPaymentMethodType() != null && tmpPaymentsVO.getPaymentMethodType().equalsIgnoreCase("P"))
		{
			String paymentTypeDesc = tmpPaymentsVO.getPaymentTypeDesc();
			orderInfoSection.append("Payment Method:\t" + paymentTypeDesc);
			HTMLorderInfoSection.append(this.addBulkStandardSummaryRow("Payment Method:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + paymentTypeDesc, "&nbsp;"));

			Element ccElement = orderDocument.createElement("alternatePaymentText");
			ordersElement.appendChild(ccElement);
			ccElement.appendChild(orderDocument.createTextNode(paymentTypeDesc));
		}

		orderInfoSection.append(Utils.end() + Utils.end());
		HTMLorderInfoSection.append(HTMLend());

		orderInfoSection.append("All prices are in US dollars"  );
		HTMLorderInfoSection.append(addStandardLowerRow("All prices are in US dollars", "&nbsp;"));

		Element pricesElement = orderDocument.createElement("pricesVerbiage");
		ordersElement.appendChild(pricesElement);
		pricesElement.appendChild(orderDocument.createTextNode("All prices are in US dollars"));

		//PERSONAL GREETING INSTRUCTIONS
		if (tmpDetailVO.getPersonalGreetingId() != null && tmpDetailVO.getPersonalGreetingId() != "" )
		{
			String personalGreetingPhoneToken = BuildEmailConstants.PERSONAL_GREETING_PHONE_NUMBER_TOKEN;
			String phoneNumber = cu.getContent(this.connection, BuildEmailConstants.PERSONAL_GREETING_CONTEXT, BuildEmailConstants.PERSONAL_GREETING_PHONE_NUMBER_FILTER);

			//get the html instructions and replace the phone number

			String personalGreetingInstructions = cu.getContent(this.connection ,BuildEmailConstants.PERSONAL_GREETING_CONTEXT, BuildEmailConstants.PERSONAL_GREETING_INSTRUCTIONS_HTML);
			personalGreetingInstructions = personalGreetingInstructions.replaceAll(personalGreetingPhoneToken,phoneNumber);
			HTMLorderInfoSection.append(addStandardLowerRow(personalGreetingInstructions , "&nbsp"));

			Element greetingElement = orderDocument.createElement("personalGreetingInstructions");
			ordersElement.appendChild(greetingElement);
			greetingElement.appendChild(orderDocument.createTextNode(personalGreetingInstructions));

			//get the text instructions and replace the phone number

			personalGreetingInstructions = cu.getContent(this.connection ,BuildEmailConstants.PERSONAL_GREETING_CONTEXT, BuildEmailConstants.PERSONAL_GREETING_INSTRUCTIONS_TEXT);
			personalGreetingInstructions = personalGreetingInstructions.replaceAll(personalGreetingPhoneToken,phoneNumber);   
			orderInfoSection.append(Utils.end() + personalGreetingInstructions + Utils.end());
		}


		//REWARD INFORMATION
		String partnerId = recalculateOrderSourceCodeVO.getPartnerId();
		PartnerVO partner = this.getPartner(partnerId);
		if(partnerId != null && !partnerId.equals(""))
		{
			if(partner.getEmailExludeFlag() != null && !partner.getEmailExludeFlag().equalsIgnoreCase("Y"))
			{
				Element partnerElement = orderDocument.createElement("partnerName");
				ordersElement.appendChild(partnerElement);
				partnerElement.appendChild(orderDocument.createTextNode(partner.getPartnerName()));
				partnerElement = orderDocument.createElement("rewardName");
				ordersElement.appendChild(partnerElement);
				partnerElement.appendChild(orderDocument.createTextNode(partner.getRewardName()));

				if(partner.getRewardName() != null && partner.getRewardName().equalsIgnoreCase("CHARITY"))
				{
					orderInfoSection.append("\n");
					orderInfoSection.append("FTD.COM will donate 5% of the product price to " + partner.getPartnerName() + Utils.end());
					HTMLorderInfoSection.append(addStandardLowerRow("FTD.COM will donate 5% of the product price to " + partner.getPartnerName(), "&nbsp;"));
				}
				else if(partner.getPartnerName() != null && partner.getPartnerName().equalsIgnoreCase("DISCOVER"))
				{
					orderInfoSection.append("\n");
					orderInfoSection.append("Your purchase from FTD.COM has earned you " + Utils.formatDollars(order.getPartnershipBonusPoints()) + " towards your Cash Back Bonus with Discover." + Utils.end());
					HTMLorderInfoSection.append(addStandardLowerRow("Your purchase from FTD.COM has earned you " + Utils.formatDollars(order.getPartnershipBonusPoints()) + " towards your Cash Back Bonus with Discover.", "&nbsp;"));
				} 
				else if(partner.getPartnerName() != null && partner.getPartnerName().equalsIgnoreCase("UPROMISE"))
				{
					orderInfoSection.append("\n");
					orderInfoSection.append("Your purchase has earned you " + Utils.formatDollars(order.getPartnershipBonusPoints()) + " back as college savings into your Upromise account!" + Utils.end());
					HTMLorderInfoSection.append(addStandardLowerRow("Your purchase has earned you " + Utils.formatDollars(order.getPartnershipBonusPoints()) + " back as college savings into your Upromise account!", "&nbsp;"));
				}
				else
				{
					long points = 0;
					if(order.getPartnershipBonusPoints() != null)
					{
						points = Math.round(Double.parseDouble(order.getPartnershipBonusPoints()));
					}
					orderInfoSection.append("\n");
					orderInfoSection.append("You will earn " + points + " " + partner.getPartnerName() + " " + partner.getRewardName() + Utils.end());
					orderInfoSection.append("Please allow 6-8 weeks for miles/points to post to your account." + Utils.end());
					HTMLorderInfoSection.append(addStandardLowerRow("You will earn " + points + " " + partner.getPartnerName() + " " + partner.getRewardName(), "&nbsp;"));
					HTMLorderInfoSection.append(addStandardLowerRow("Please allow 6-8 weeks for miles/points to post to your account.", "&nbsp;"));
					
					partnerElement = orderDocument.createElement("rewardTotal");
					ordersElement.appendChild(partnerElement);
					partnerElement.appendChild(orderDocument.createTextNode(Long.toString(points)));
				}
				
			}
		}
		
		BuyerVO buyerVO = (BuyerVO)order.getBuyer().get(0);
		Element custElement = orderDocument.createElement("customerFirstName");
		ordersElement.appendChild(custElement);
		custElement.appendChild(orderDocument.createTextNode(buyerVO.getFirstName()));
		custElement = orderDocument.createElement("customerLastName");
		ordersElement.appendChild(custElement);
		custElement.appendChild(orderDocument.createTextNode(buyerVO.getLastName()));
    	if(buyerVO.getBuyerAddresses() != null && buyerVO.getBuyerAddresses().size() > 0) {   
    		BuyerAddressesVO buyerAddressVO = (BuyerAddressesVO) buyerVO.getBuyerAddresses().get(0);
    		String buyerAddress = buyerAddressVO.getAddressLine1();
    		if (buyerAddressVO.getAddressLine2() != null) {
    			buyerAddress = buyerAddress + " " + buyerAddressVO.getAddressLine2();
    		}
    		if (buyerAddress != null) {
    			custElement = orderDocument.createElement("customerAddress");
    			ordersElement.appendChild(custElement);
    			custElement.appendChild(orderDocument.createTextNode(buyerAddress));
    		}
    		if (buyerAddressVO.getCity() != null) {
    			custElement = orderDocument.createElement("customerCity");
    			ordersElement.appendChild(custElement);
    			custElement.appendChild(orderDocument.createTextNode(buyerAddressVO.getCity()));
    		}
    		if (buyerAddressVO.getStateProv() != null) {
    			custElement = orderDocument.createElement("customerState");
    			ordersElement.appendChild(custElement);
    			custElement.appendChild(orderDocument.createTextNode(buyerAddressVO.getStateProv()));
    		}
    		if (buyerAddressVO.getPostalCode() != null) {
    			custElement = orderDocument.createElement("customerZipCode");
    			ordersElement.appendChild(custElement);
    			custElement.appendChild(orderDocument.createTextNode(buyerAddressVO.getPostalCode()));
    		}
    		if (buyerAddressVO.getCountry() != null) {
    			custElement = orderDocument.createElement("customerCountry");
    			ordersElement.appendChild(custElement);
    			custElement.appendChild(orderDocument.createTextNode(buyerAddressVO.getCountry()));
    		}
        }
        if (buyerVO.getBuyerPhones() != null && buyerVO.getBuyerPhones().size() > 0) {
            BuyerPhonesVO buyerPhoneVO = (BuyerPhonesVO) buyerVO.getBuyerPhones().get(0);
            String buyerPhone = buyerPhoneVO.getPhoneNumber();
            if (buyerPhone != null) {
    			custElement = orderDocument.createElement("customerPhoneNumber");
    			ordersElement.appendChild(custElement);
    			custElement.appendChild(orderDocument.createTextNode(buyerPhone));
            }
        }

		orderInfoSection.append(Utils.end());
		//HTMLorderInfoSection.append(HTMLend());
		HTMLorderInfoSection.append(this.endHTMLTable());
		HTMLorderInfoSection.append(this.closeHTML());

		sections.put(BuildEmailConstants.PLAIN_TEXT, orderInfoSection.toString());
		sections.put(BuildEmailConstants.HTML, HTMLorderInfoSection.toString());

		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		DOMUtil.print(orderDocument, pw);
		sections.put(BuildEmailConstants.ORDER_DETAILS_XML, sw.toString());
		
		return sections;
	}

	private String cleanHTMLEncoding(String s)
	{
		String tmp = "";
		ArrayList htmlEncs = new ArrayList();
		ArrayList decEncs = new ArrayList();
		htmlEncs.add("&reg;");
		htmlEncs.add("&#153;");
		decEncs.add(new String(new byte[]{(byte)174}));
		decEncs.add(new String(new byte[]{(byte)153}));

		tmp = s;
		for(int i = 0; i < htmlEncs.size(); i++)
		{
			tmp = this.replace(tmp, (String)htmlEncs.get(i), (String)decEncs.get(i));
		}

		return tmp;
	}


	private String replace(String strSource, String strFrom, String strTo)
	{
		if(strFrom == null || strFrom.equals(""))
			return strSource;
		String strDest = "";
		int intFromLen = strFrom.length();
		int intPos;

		while((intPos = strSource.indexOf(strFrom)) != -1)
		{
			strDest = strDest + strSource.substring(0,intPos);
			strDest = strDest + strTo;
			strSource = strSource.substring(intPos + intFromLen);
		}
		strDest = strDest + strSource;

		return strDest;
	}

	private ArrayList splitText(String giftMessage)
	{



		if(giftMessage == null)
		{
			return new ArrayList();
		}

		String part1 = giftMessage;
		String part2 = "";

		if(giftMessage.length() > SPLIT_SIZE)
		{
			StringCharacterIterator cIt = null;
			// Go back to first space
			int spaceIndex = SPLIT_SIZE;
			cIt = new StringCharacterIterator(giftMessage);
			cIt.setIndex(spaceIndex);
			for(char c = cIt.current(); c != CharacterIterator.DONE; c = cIt.previous())
			{
				if(c == ' ')
				{
					break;
				}
				spaceIndex--;
			}

			if(spaceIndex != -1)
			{
				part1 = giftMessage.substring(0, spaceIndex).trim();
				part2 = giftMessage.substring(spaceIndex).trim();
			}
			else
			{
				part1 = giftMessage.substring(0, SPLIT_SIZE).trim();
				part2 = giftMessage.substring(SPLIT_SIZE).trim();
			}
		}

		ArrayList tmplines = new ArrayList();
		tmplines.add(part1);
		if(part2 != null && part2.length() > 0){
			tmplines.add(part2);
		}

		return tmplines;
	}



	private String translateCCType(String inType)
	{

		if(inType == null || inType.equals(""))
		{
			return "";
		}
		else
		{
			CachedResultSet rs = null;
			String outType = "";
			boolean found = false;

			try
			{
				DataRequest dataRequest = new DataRequest();
				DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

				dataRequest.setConnection(this.connection);
				dataRequest.setStatementID("GET CC TYPES");
				rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
				while(rs.next())
				{
					if(((String)rs.getObject(1)).equalsIgnoreCase(inType))
					{
						outType = (String)rs.getObject(2);
						found = true;
					}
				}
			}
			catch(Exception e)
			{}

			if(!found)
				return inType.toUpperCase();

			return outType.toUpperCase();
		}
	}

	

	private HashMap getColorDescription()
	{
		CachedResultSet rs = null;
		HashMap colors = new HashMap();

		try
		{
			DataRequest dataRequest = new DataRequest();
			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

			dataRequest.setConnection(this.connection);
			dataRequest.setStatementID("GET_COLORS");
			rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
			while(rs.next())
			{
				colors.put((String)rs.getObject(1), (String)rs.getObject(2));
			}
		}
		catch(Exception e)
		{}

		return colors;
	}

	private String getColorSizeFlag(String productId)
	{
		CachedResultSet rs = null;
		String colorSizeFlag = null;

		try
		{
			DataRequest dataRequest = new DataRequest();
			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

			dataRequest.setConnection(this.connection);
			dataRequest.setStatementID("GET_PRODUCT_BY_ID");
			dataRequest.addInputParam("IN_PRODUCT_ID", productId);
			rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
			rs.next();
			colorSizeFlag = (String)rs.getObject(10);
		}
		catch(Exception e)
		{
			colorSizeFlag = null;
		}
		return colorSizeFlag;
	}

	/**
	 *  
	 */
	private RecalculateOrderSourceCodeVO  getSourceCodeDetails(String sourceIn)
	{
		CachedResultSet rs = null;       
		RecalculateOrderSourceCodeVO recalculateOrderSourceCodeVO = new RecalculateOrderSourceCodeVO();
		try
		{
			DataRequest dataRequest = new DataRequest();
			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

			dataRequest.setConnection(this.connection);
			dataRequest.setStatementID("GLOBAL.GET_SOURCE_CODE_RECORD");
			dataRequest.addInputParam("IN_SOURCE_CODE", sourceIn);
			rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
			rs.next();
			recalculateOrderSourceCodeVO.setPartnerId((String)rs.getString("PARTNER_ID"));
			recalculateOrderSourceCodeVO.setJcpenneyFlag((String)rs.getString("JCPENNEY_FLAG"));
			recalculateOrderSourceCodeVO.setDisplaySameDayUpcharge((String)rs.getString("DISPLAY_SAME_DAY_UPCHARGE"));
			recalculateOrderSourceCodeVO.setMorningDeliveryFlag((String)rs.getString("MORNING_DELIVERY_FLAG"));
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return recalculateOrderSourceCodeVO;
	}

	private PartnerVO getPartner(String partnerId)
	{
		CachedResultSet rs = null;
		PartnerVO partner = new PartnerVO();

		try
		{
			DataRequest dataRequest = new DataRequest();
			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

			dataRequest.setConnection(this.connection);
			dataRequest.setStatementID("GET_PARTNER");
			dataRequest.addInputParam("PARTNER_ID", partnerId);
			rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
			rs.next();

			partner.setPartnerId(partnerId);
			partner.setPartnerName((String)rs.getObject(1));
			partner.setCustomerInfo((String)rs.getObject(2));
			partner.setRewardName((String)rs.getObject(3));
			partner.setIdLength("");
			partner.setEmailExludeFlag((String)rs.getObject(5));

		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

		return partner;
	}

	
	
	private String startHTML()
	{
		return "<p>";
	}

	private String closeHTML()
	{
		return "</p>";
	}

	private String startHTMLTable()
	{
		return "<table width=\"700px\" style='font-family: Verdana, Arial, Helvetica, sans-serif; font: 10pt;'>";
		//return "<table width=\"700px\" border=\"2px\"  cellspacing=\"2px\"  cellpadding=\"2px\" style='font-family: Verdana, Arial, Helvetica, sans-serif; font: 10pt;'>";
	}

	private String endHTMLTable()
	{
		return "</table>";
	}

	private String addStandardUpperRow(String one, String two)
	{
		return 	"<tr>\n<td width=\"250px\"><font size=\"2\">" + one + "</font></td>\n<td><font size=\"2\">" + two + "</font></td>\n</tr>\n";
	}

	private String addStandardLowerRow(String one, String two)
	{
		return 	"<tr>\n<td width=\"500px\"><font size=\"2\">" + one + "</font></td>\n<td width=\"70px\" align=\"right\"><font size=\"2\">" + two + "</font></td>\n</tr>\n";
	}

	private String addStandardLowerRow(String one, String two, String three)
	{
		return    "<tr>\n<td width=\"500px\"><font size=\"2\">" + one + 
		"</font></td>\n<td width=\"70px\" align=\"right\"><font size=\"2\">" + two + 
		"</font></td>\n<td width=\"160px\" align=\"right\"><font size=\"2\">" + three + "</font></td>\n</tr>\n";
	}    

	private String HTMLend()
	{
		return "<tr><td><br></td></tr>\n";
	}

	private String addBulkStandardUpperRow(String one, String two)
	{
		return 	"<tr>\n<td width=\"150px\"><font size=\"2\">" + one + "</font></td>\n<td><font size=\"2\">" + two + "</font></td>\n</tr>\n";
	}

	private String addBulkInfoHeader()
	{
		return "<tr><td width=\"40px\" align=\"left\">QTY</td>" +
		"<td width=\"200px\">&nbsp;</td>" +
		"<td width=\"100px\">&nbsp;</td>" +
		"<td width=\"95px\">Delivery Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>" +
		"<td width=\"65px\">Ship Method&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>" +
		"<td>&nbsp;</td></tr>" +
		"<tr><td width=\"40px\" align=\"left\">&nbsp;</td>" +
		"<td width=\"200px\">&nbsp;</td>" +
		"<td width=\"100px\">&nbsp;</td>" +
		"<td width=\"95px\">&nbsp;</td>" +
		"<td width=\"65px\">&nbsp;</td>" +
		"<td>&nbsp;</td></tr>";
	}

	private String addBulkStandardInfoRow(String one, String two, String three, String four, String five, String six)
	{
		return  "<tr><td width=\"40px\" align=\"left\"> <font size=\"2\">" + one + "</font>" +
		"</td><td width=\"200px\"><font size=\"2\">" + two  + "</font>" +
		"</td><td width=\"100px\"><font size=\"2\">" + three  + "</font>" +
		"</td><td width=\"95px\"><font size=\"2\">" + four  + "</font>" +
		"</td><td width=\"65px\"><font size=\"2\">" + five  + "</font>" +
		"</td><td width=\"90px\" align=\"right\"><font size=\"2\">" + six  + "</font>" + "</td></tr>";
	}

	private String addBulkStandardSummaryRow(String one, String two)
	{
		return  "<tr>" +
		"<td width=\"500px\" align=\"left\"> <font size=\"2\">" + one + "</font></td>" +
		"<td width=\"100px\" align=\"right\"><font size=\"2\">" + two + "</font></td>" +
		"</tr>";
	}

	private String startBulkInfoTable()
	{
		return "<table cellpadding=\"1\" cellspacing=\"1\">";
	}

	private String addSpecialInstructionsRow(String s)
	{
		return  "<tr>" +
		"<td><font size=\"2\">" + s + "</font></td>" +
		"</tr>";
	}

	

	/**
	 * This method retrieves the email type flag from the database.  When possible
	 * this value will be retrieved from cache instead of the database.
	 */
	public int getGlobalEmailFlag() throws Exception
	{
		String value = null;

		// Try cache first
		GlobalParmHandler parmHandler = (GlobalParmHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_GLOBAL_PARM);
		if(parmHandler != null)
		{
			value = parmHandler.getFrpGlobalParm(GLOBAL_PARAM_EMAIL_CONTEXT, GLOBAL_PARAM_EMAIL_TYPE_KEY);
		}
		else
		{
			DataRequest dataRequest = new DataRequest();
			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			dataRequest.setConnection(connection);
			dataRequest.addInputParam("IN_CONTEXT", GLOBAL_PARAM_EMAIL_CONTEXT);
			dataRequest.addInputParam("IN_PARAM", GLOBAL_PARAM_EMAIL_TYPE_KEY);
			dataRequest.setStatementID("GET_GLOBAL_PARAM");
			String rs = (String)dataAccessUtil.execute(dataRequest);
			value = rs;
			dataRequest.reset();
		}

		int returnValue = -1;
		if(value.equals(BuildEmailConstants.MULTIPART_DB_FLAG))
		{
			returnValue = EmailVO.MULTIPART;
		}
		else if(value.equals(BuildEmailConstants.TEXT_DB_FLAG))
		{
			returnValue = EmailVO.PLAIN_TEXT;
		}

		return returnValue;
	}

	public String startGlobalHTML()
	{
		return "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">" +
		"<html>" +
		"<head>" +
		"<title>Untitled</title>" +
		"</head>" +
		"<body style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font: 10pt;\">";
	}

	public String closeGlobalHTML()
	{
		return "</body></html>";
	}



	private double getGiftCertificateAmount(String giftID) throws Exception
	{
		double returnValue = 0;
		RetrieveGiftCodeResponse retrieveResp = GiftCodeUtil.getGiftCodeById(giftID);
		if (null != retrieveResp) {
			List<RedemptionDetails> redemptionDtlsLst = retrieveResp
					.getRedemptionDetails();
			if (null != redemptionDtlsLst && redemptionDtlsLst.size() > 0) {
				BigDecimal totalAmt = BigDecimal.ZERO;
				for (RedemptionDetails dtls : redemptionDtlsLst) {
					if (null != dtls.getRedemptionAmount()) {
						totalAmt = totalAmt.add(dtls.getRedemptionAmount());
					}
					returnValue = totalAmt.doubleValue();
				}
			}
		}
		return returnValue;
	}

	/**
	 * This method performs special formatting on the email content.
	 *
	 * @params EmailVO
	 * @returns EmailVO
	 */
	public EmailVO performSpecialFormatting(EmailVO emailVO) throws Exception
	{

		//ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
		int posStart = 0;
		int posEnd = 0;

		//get the from address domain
		String fromAddrDomain = "";
		if(emailVO.getRecipient() != null){
			posStart = emailVO.getRecipient().indexOf("@");
			if(posStart > 0){
				fromAddrDomain = emailVO.getRecipient().substring(posStart + 1);
			}
		}
		fromAddrDomain = fromAddrDomain.toUpperCase();

		//For certain orders the brackets within the text version of the email
		//need to be removed.  For all other orders the brackets along with the
		//text within the brackets needs to be removed.
		String removeBracketsKeepContent = cu.getProperty(EMAIL_SERVICE_CONFIG_FILE,"KEEP_BRACKET_CONTENT");

		removeBracketsKeepContent = removeBracketsKeepContent.toUpperCase();
		posStart = removeBracketsKeepContent.indexOf(fromAddrDomain);
		if(posStart >= 0)
		{
			//Remove brackets, keep content
			String content = replace(emailVO.getContent(),"[","");
			content = replace(content,"]","");
			emailVO.setContent(content);
		}
		else
		{
			//remove brackets and everything in them
			String content = emailVO.getContent();
			posStart = content.indexOf("[");
			posEnd = content.indexOf("]");
			while(posStart > 0 && posEnd > posStart)
			{
				String sub = content.substring(posStart,posEnd + 1);
				content = replace(content,sub,"");
				posStart = content.indexOf("[");
				posEnd = content.indexOf("]");
			}
			emailVO.setContent(content);
		}


		//For certain email address domains only a text version of the email should
		//be sent.
		String textOnly = cu.getProperty(EMAIL_SERVICE_CONFIG_FILE,"TEXT_ONLY_EMAIL");

		textOnly = textOnly.toUpperCase();
		posStart = textOnly.indexOf(fromAddrDomain);
		if(posStart >= 0)
		{
			emailVO.setContentType(EmailVO.AOL);
		}

		return emailVO;
	}

	
	private String formatMilesPoints(boolean isMilesPoints, String milesPointsAmt)
	{
		String ret = "";

		if (isMilesPoints) {
			if(milesPointsAmt == null || milesPointsAmt.equals("")) {
				milesPointsAmt = "0";
			}
			ret = "(" + milesPointsAmt + " Miles/Points)";
		}
		return ret;
	}


	/**
	 * Replaces the Source Code Token in the email link: ~sourcecode~
	 * 
	 * @param originalContent Email Text Content to search and replace
	 * @param order Order to replace content for
	 * @return The content with source code tokens replaced
	 * @throws Exception On error retrieving source code information for the order
	 */
	public String replaceSourceCodeToken(String originalContent, OrderVO order)
	throws Exception
	{
		String newContent = originalContent;

		if (originalContent.contains(BuildEmailConstants.SOURCE_CODE_TOKEN))
		{
			String sourceCode = order.getSourceCode();

			if (sourceCode == null || sourceCode.trim().length() == 0)
			{
				throw new Exception("The source code could not be retrieved for order " + order.getGUID());
			}
			newContent = originalContent.replaceAll(BuildEmailConstants.SOURCE_CODE_TOKEN, sourceCode);
		}

		return newContent;

	}

	/**
	 * Replaces the various phone number tokens in the content.
	 * 
	 * Tokens replaced are: ~customertierphone~, ~<partnerlowercase>phonenumber~, ~phone~
	 * 
	 * @param originalContent Email Text Content to search and replace
	 * @param order Order to replace content for
	 * @return Content with phone number tokens replaced
	 * @throws Exception On error retrieving token values information for the order
	 */
	public String replacePhoneToken(String originalContent, OrderVO order)
	throws Exception
	{
		String newContent = originalContent;
		//ConfigurationUtil cu = ConfigurationUtil.getInstance();
		SegmentationUtil su = new SegmentationUtil();

		String tierLevel = su.getCustomerTierByScrubOrderGUID(connection, order.getGUID());
		String companyId = order.getCompanyId();
		String language = order.getLanguageId();

		logger.debug("###companyId: " + companyId);
		logger.debug("###languageId: " + language);
		logger.debug("###tierLevel: " + tierLevel);

		// #12961 - UC 24606 Include PC phone numbers for a PC orders when there
		// is no partner associated with order.
		HashSet<String> partners = PreferredPartnerUtility.getPreferredPartnerNames();

		if (replaceWidPCPhone(partners, order, originalContent)) {
			return replacePhoneTokenWidPCNo(tierLevel, order, newContent);
		}
		// UC 24606 - till here

		if (originalContent.contains(BuildEmailConstants.PHONE_NUMBER_TOKEN))
		{
			String phoneNumber = "";
			OrderDetailsVO tmpDetailVO = new OrderDetailsVO();
			Boolean premierFound = false;

			for (int i = 0; i < order.getOrderDetail().size(); i++)
			{
				tmpDetailVO = ((OrderDetailsVO) (order.getOrderDetail().get(i)));
				if (tmpDetailVO.getPremierCollectionFlag().equalsIgnoreCase("Y") && (!premierFound))
				{
					phoneNumber = 
						cu.getContentWithFilter(connection, BuildEmailConstants.PHONE_NUMBER_CONTEXT, BuildEmailConstants.PREMIER_CALL_CENTER_CONTACT_NUMBER, 
								null, null);
					premierFound = true;
				}
			}
			if (!premierFound)
			{

				if(companyId != null && language != null){	
					if(tierLevel != null){
						if((tierLevel.equals("A") && language.equals(this.ENGLISH)) || tierLevel.equals(this.SCI) || tierLevel.equals(this.SYMPATHY)){
							phoneNumber = 
								cu.getContentWithFilter(connection, BuildEmailConstants.PHONE_NUMBER_CONTEXT, BuildEmailConstants.CALL_CENTER_CONTACT_NUMBER, 
										tierLevel, null);
							logger.debug("###phoneNumber A: " + phoneNumber);
						}
						else{
							phoneNumber = 
								cu.getContentWithFilter(connection, BuildEmailConstants.LANGUAGE_CONTEXT, BuildEmailConstants.PHONE_NUMBER_CONTEXT, 
										companyId, language);   
							logger.debug("###phoneNumber B: " + phoneNumber);
						}
					}
					else if (tierLevel == null){
						phoneNumber = 
							cu.getContentWithFilter(connection, BuildEmailConstants.LANGUAGE_CONTEXT, BuildEmailConstants.PHONE_NUMBER_CONTEXT, 
									companyId, language);
						logger.debug("###phoneNumber C: " + phoneNumber);
					}
				}
				else if(companyId != null && language == null){
					if(companyId.equals("FTD") || companyId.equals("FTDCA")){
						phoneNumber = 
							cu.getContentWithFilter(connection, BuildEmailConstants.LANGUAGE_CONTEXT, BuildEmailConstants.PHONE_NUMBER_CONTEXT, 
									companyId, null);
						logger.debug("###phoneNumber D: " + phoneNumber);
					}
				}
			}

			//Check to make sure phone number is set to something
			//If not, set it appropriately and retest.
			if (phoneNumber == null || "".equals(phoneNumber))
			{    	  
				phoneNumber = 
					cu.getContentWithFilter(connection, BuildEmailConstants.PHONE_NUMBER_CONTEXT, BuildEmailConstants.CALL_CENTER_CONTACT_NUMBER, 
							tierLevel, null);

				logger.debug("###phoneNumber E: " + phoneNumber);
				if (phoneNumber == null || "".equals(phoneNumber))
				{ 
					String error = "Error retrieving the phone number for order " + 
					order.getGUID() + ".  Company= "  + companyId + 
					", language= " + language + ", tier level=" + tierLevel + 
					". Ensure that a phone number entry exists in context.";

					throw new Exception(error);
				}
			}
			newContent = originalContent.replaceAll(BuildEmailConstants.PHONE_NUMBER_TOKEN, phoneNumber);
		}


		// Replace any Partner Phone Tokens if they exist in the content
		for(String partner: PreferredPartnerUtility.getPreferredPartnerNames())
		{
			String partnerPhoneToken = "~" + partner.toLowerCase() + "phonenumber~";
			String partnerUpperCase = "" + partner.toUpperCase();

			if(logger.isDebugEnabled())
			{
				logger.debug("Checking for Partner Phone Token: " + partnerPhoneToken);
			}

			if (newContent.contains(partnerPhoneToken))
			{
				String phoneNumber = cu.getContentWithFilter(connection, BuildEmailConstants.PHONE_NUMBER_CONTEXT, BuildEmailConstants.CALL_CENTER_CONTACT_NUMBER, 
						partnerUpperCase, null);

				if (phoneNumber == null || phoneNumber.trim().length() == 0)
				{
					String error = "The Partner phone number could not be retreved for order " + order.getGUID() + 
					" Ensure that a phone number entry exists in context " + 
					BuildEmailConstants.PHONE_NUMBER_CONTEXT + " and filter value " + partnerUpperCase;

					throw new Exception(error);
				}

				if(logger.isDebugEnabled())
				{
					logger.debug("Replacing Partner Token: " + partnerPhoneToken + " with: " + phoneNumber);
				}

				newContent = newContent.replaceAll(partnerPhoneToken, phoneNumber);
			}
		}

		if (originalContent.contains(BuildEmailConstants.STOCK_MESSAGE_PHONE_NUMBER_TOKEN)) {
			String phoneNumber = cu.getContentWithFilter(connection, BuildEmailConstants.LANGUAGE_CONTEXT, BuildEmailConstants.PHONE_NUMBER_CONTEXT, 
					companyId, language);   
			logger.debug("###phoneNumber now equal: " + phoneNumber);

			if (phoneNumber == null || "".equals(phoneNumber))
			{    	  
				phoneNumber = 
					cu.getContentWithFilter(connection, BuildEmailConstants.PHONE_NUMBER_CONTEXT, BuildEmailConstants.CALL_CENTER_CONTACT_NUMBER, 
							tierLevel, null);

				logger.debug("###phoneNumber was empty, but now set to: " + phoneNumber);
				if (phoneNumber == null || "".equals(phoneNumber))
				{ 
					String error = "Error retrieving the phone number for order " + 
					order.getGUID() + ".  Company= "  + companyId + 
					", language= " + language + ", tier level=" + tierLevel + 
					". Ensure that a phone number entry exists in context.";

					throw new Exception(error);
				}
			}
			newContent = newContent.replaceAll(BuildEmailConstants.STOCK_MESSAGE_PHONE_NUMBER_TOKEN, phoneNumber);
		}

		return newContent;

	}


	/**
	 * Replaces the Partner Email Link Token. ~<partnerlowercase>emaillink~
	 * 
	 * @param originalContent Content to perform token replacement on
	 * @param contentType Content Type for filtering the Content Tables: TEXT, HTML
	 * @param order The order
	 * @return Content with tokens replaced
	 * @throws Exception
	 */
	public String replaceEmailLinkToken(String originalContent, String contentType, OrderVO order)
	throws Exception
	{

		String newContent = originalContent;
		//ConfigurationUtil cu = ConfigurationUtil.getInstance();
		HashSet<String> partnerNames = PreferredPartnerUtility.getPreferredPartnerNames();
		if(partnerNames != null && partnerNames.size() > 0)
		{
		
		for(String partner: partnerNames)
		{
			String partnerEmailToken = "~" + partner.toLowerCase() + "emaillink~";
			String partnerUpperCase = "" + partner.toUpperCase();

			if(logger.isDebugEnabled())
			{
				logger.debug("Checking for Partner Email Token: " + partnerEmailToken);
			}

			if (newContent.contains(partnerEmailToken))
			{
				String emailLink = cu.getContentWithFilter(connection, "STOCK_EMAIL", "TOKEN", contentType, partnerUpperCase + "_EMAIL_LINK");

				if (emailLink == null || emailLink.trim().length() == 0)
				{
					String error = "The Partner email link could not be retrieved for order " + order.getGUID() + 
					" Ensure that a email link entry exists in context=STOCK_EMAIL, name=TOKEN, Filter1= " + 
					contentType + " and Filter2=" + partnerUpperCase + "_EMAIL_LINK";

					throw new Exception(error);
				}

				if(logger.isDebugEnabled())
				{
					logger.debug("Replacing Partner Token: " + partnerEmailToken + " with: " + emailLink);
				}

				newContent = newContent.replaceAll(partnerEmailToken, emailLink);
			}
		}    
		}

		return newContent;
	}

	/**
	 * Replaces the Company URL Token in the email link: ~companyurl~
	 * 
	 * @param originalContent Email Text Content to search and replace
	 * @param order Order to replace content for
	 * @return The content with CompanyUrl tokens replaced
	 * @throws Exception On error retrieving CompanyUrl information for the order
	 */
	public String replaceCompanyUrlToken(String originalContent, OrderVO order)
	throws Exception
	{
		String newContent = originalContent;

		if (originalContent.contains(BuildEmailConstants.COMPANY_URL_TOKEN))
		{
			String companyUrl = order.getCompanyUrl();

			if (companyUrl == null || companyUrl.trim().length() == 0)
			{
				throw new Exception("The company url could not be retrieved for order " + order.getGUID());
			}
			newContent = originalContent.replaceAll(BuildEmailConstants.COMPANY_URL_TOKEN, companyUrl);
		}

		return newContent;

	}

	/**
	 * Replaces the Language Token "~language.message~".
	 * 
	 * @param originalContent Content to perform token replacement on
	 * @param contentType Content Type for filtering the Content Tables: TEXT, HTML
	 * @param order The order
	 * @return Content with language tokens replaced
	 * @throws Exception
	 */
	public String replaceLanguageToken(String originalContent,
			String contentType, 
			OrderVO order) throws Exception {

		String newContent = originalContent;
		//ConfigurationUtil cu = ConfigurationUtil.getInstance();
		String languageId = StringUtils.trim(order.getLanguageId());

		if (originalContent.contains(BuildEmailConstants.LANGUAGE_TOKEN)) {
			String languageMessage = cu.getContentWithFilter(
					connection,
					BuildEmailConstants.LANGUAGE_CONTEXT,
					BuildEmailConstants.LANGUAGE_MESSAGE, 
					contentType, 
					languageId);

			if (StringUtils.isEmpty(languageMessage)){
				newContent = originalContent.replaceAll(
						BuildEmailConstants.LANGUAGE_TOKEN, "");
			} else {
				newContent = originalContent.replaceAll(
						BuildEmailConstants.LANGUAGE_TOKEN, languageMessage);
			}
		}
		return newContent;
	}


	/**
	 * General function for replacing the various order specific tokens in the email content.
	 * Replaces the Phone Token using {@link #replacePhoneToken}(). 
	 * Replaces the Email Link Token using {@link #replaceEmailLinkToken}(). 
	 * Replaces the Source Code Token using {@link #replaceSourceCodeToken}(). 
	 * Replaces the Language Token using {@link #replaceLanguageToken}(). 
	 * 
	 * @param originalContent Content with tokens in it
	 * @param contentType HTML or TEXT
	 * @param order Order details
	 * @return Content with tokens replaced
	 * @throws Exception On any invalid tokens.
	 */
	public String replaceOrderTokens(String originalContent, String contentType, OrderVO order) 
	throws Exception
	{
		if(logger.isDebugEnabled())
		{
			logger.debug("Replacing Original Content of type: " + contentType);
			logger.debug("sourceCode: " + order.getSourceCode());
			logger.debug("companyUrl: " + order.getCompanyUrl());
			logger.debug("Preferred Partners on Order: " + order.getPreferredProcessingPartners());
			logger.debug("Defined Preferred Partner Resources: " + PreferredPartnerUtility.getPreferredPartnerNames());
		}

		if(originalContent == null || originalContent.trim().length() == 0) 
		{
			return originalContent;
		}

		String replacedContent = originalContent;

		replacedContent = replacePhoneToken(replacedContent, order);
		replacedContent = replaceEmailLinkToken(replacedContent, contentType, order);
		replacedContent = replaceSourceCodeToken(replacedContent, order);
		replacedContent = replaceCompanyUrlToken(replacedContent, order);
		replacedContent = replaceLanguageToken(replacedContent, contentType, order);
		return replacedContent;
	}


	/**
	 * Replaces the email tokens for the content of the passed in email with the information 
	 * pertaining to the order.
	 * 
	 * Uses {@link #replaceOrderTokens(Connection, String, String , OrderVO)}
	 * 
	 * @param email Email content to update
	 * @param order Order information for the email
	 * @throws Exception
	 */
	public void updateEmailOrderTokens(EmailVO email, OrderVO order) 
	throws Exception
	{
		email.setContent(replaceOrderTokens (email.getContent(), "TEXT", order));
		email.setHTMLContent(replaceOrderTokens (email.getHTMLContent(), "HTML", order));
	}

	/** #12961 - Check if the email contains phone tokens and replace them with PC phone numbers.
	 * @param tierLevel
	 * @param order
	 * @param emailContent
	 * @return
	 * @throws Exception
	 */
	private String replacePhoneTokenWidPCNo(String tierLevel, OrderVO order, String emailContent) throws Exception {		
		String phoneNumber = cu.getContentWithFilter(connection, BuildEmailConstants.PHONE_NUMBER_CONTEXT,
				BuildEmailConstants.MEMBER_CALL_CENTER_CONTACT_NUMBER, "PC", null);
		if(StringUtils.isEmpty(phoneNumber)) {
			if(logger.isDebugEnabled()) {
				logger.debug("Unable to retrieve PC phone number from DB. Will be using the common call center contact number.");
			}
			phoneNumber = cu.getContentWithFilter(connection, BuildEmailConstants.PHONE_NUMBER_CONTEXT,
					BuildEmailConstants.CALL_CENTER_CONTACT_NUMBER, tierLevel, null);
		}		
		if(StringUtils.isEmpty(phoneNumber)) {
			if(logger.isDebugEnabled()) {
				logger.debug("Unable to retrieve common call center contact number. throwing exception");
			}
			String error = "Error retrieving the phone number for order " + order.getGUID()
			+ ".  Company= " + order.getCompanyId() + ", language= " + order.getLanguageId() + ", tier level="
			+ tierLevel + ". Ensure that a phone number entry exists in context.";	
			throw new Exception(error);
		}		
		if (emailContent.contains(BuildEmailConstants.PHONE_NUMBER_TOKEN)) {				
			emailContent = emailContent.replaceAll(BuildEmailConstants.PHONE_NUMBER_TOKEN, phoneNumber);				
		}		
		if (emailContent.contains(BuildEmailConstants.STOCK_MESSAGE_PHONE_NUMBER_TOKEN)) {
			emailContent = emailContent.replaceAll(BuildEmailConstants.STOCK_MESSAGE_PHONE_NUMBER_TOKEN, phoneNumber);
		}
		return emailContent;	
	}

	/** #12961 - Checks if the order is PC member order and checks if the order is associated with any partner.
	 * @param partners
	 * @param order
	 * @param originalContent
	 * @return
	 */
	private boolean replaceWidPCPhone(HashSet<String> partners, OrderVO order, String originalContent) {
		boolean replaceWithPCPhNo = false;
		for (Object orderDetailVo : order.getOrderDetail()) {
			if (!replaceWithPCPhNo && ("Y".equals(((OrderDetailsVO) orderDetailVo).getPcFlag()))) {
				if (logger.isDebugEnabled()) {
					logger.debug("Is a premier circle member order");
				}
				replaceWithPCPhNo = true;
				break;
			}
		}		
		if (replaceWithPCPhNo) {
			for (String partner : partners) {
				String partnerPhoneToken = "~" + partner.toLowerCase() + "phonenumber~";
				if (replaceWithPCPhNo && originalContent.contains(partnerPhoneToken)) {
					if (logger.isDebugEnabled()) {
						logger.debug("Order contains partner phone token " + partnerPhoneToken);
					}
					replaceWithPCPhNo = false;
					break;
				}
			}
		}		
		return replaceWithPCPhNo;
	}

	/** Get order number as per the origin. If not FTD order, replace order number with the XX partner order number.
	 * @param tmpDetailVO
	 * @param orderOrigin
	 * @return
	 */
	private String getOrderNumber(OrderDetailsVO tmpDetailVO, String orderOrigin) {
		// Default value is FTD external order number.
		String orderNumber = tmpDetailVO.getExternalOrderNumber();

		if(StringUtils.isEmpty(orderOrigin)) {
			logger.error("Unable to determine if the order is partner order, invalid order origin");
			return orderNumber;
		}

		// If partner order replace it with partner order number
		CachedResultSet result = new PartnerUtility()
		.getPtnOrderDetailByConfNumber(tmpDetailVO.getExternalOrderNumber(), orderOrigin, connection);
		if (result != null && result.next()) {
			if (!StringUtils.isEmpty(result.getString("PARTNER_ORDER_ITEM_NUMBER"))) {
				orderNumber = result.getString("PARTNER_ORDER_ITEM_NUMBER");
			}
		}
		return orderNumber;
	}


	/**
	 * retrieve and return the source code info
	 * @param connection
	 * @param source code
	 * @return source code vo
	 * @throws Exception
	 */
	public SourceMasterVO retrieveSourceCodeInfo(Connection conn, String sourceCode) throws Exception
	{
		//check source code
		SourceMasterVO sVO;

		// Get source code info from cache handler.  If nothing found return with error.
		try
		{
			SourceMasterHandler sch = (SourceMasterHandler) CacheManager.getInstance().getHandler(SOURCE_CODE_HANDLER_NAME);
			sVO = sch.getSourceCodeById(sourceCode);
		}
		catch (Exception e)
		{
			logger.info("WARNING - WARNING -------------------- Source code could not be retrieved from the cache  -------------------- ");
		}
		finally
		{
			OrderEmailDAO oeDAO = new OrderEmailDAO(conn);
			sVO = oeDAO.getSourceCodeRecord(sourceCode);
		}

		if (sVO != null)
			return sVO;
		else
			throw new Exception("Source code not found");

	}

	/**
	 * retrieve and return the global parm info for webloyalty
	 * @param connection
	 * @return Hashmap containing Webloyalty global parms
	 * @throws Exception
	 */
	public HashMap retrieveWebloyaltyGlobalParms(Connection conn)
	throws Exception
	{
		HashMap wlMap = new HashMap();
		Map globalMap = new HashMap();
		try
		{
			GlobalParmHandler gph = (GlobalParmHandler) CacheManager.getInstance().getHandler(GLOBAL_PARM_HANDLER);
			globalMap = (HashMap) gph.getFrpGlobalParmMap();
		}
		catch (Exception e)
		{
			logger.info("WARNING - WARNING -------------------- Global Parms could not be retrieved from the cache  -------------------- ");
		}
		finally
		{
			if (globalMap == null)
			{
				OrderEmailDAO oeDAO = new OrderEmailDAO(conn);
				globalMap = oeDAO.getGlobalParms();
			}
		}

		if (globalMap != null)
		{
			wlMap.put(OrderEmailConstants.HK_EMAIL_SLOGAN_1, globalMap.get(OrderEmailConstants.HK_EMAIL_SLOGAN_1));
			wlMap.put(OrderEmailConstants.HK_EMAIL_SLOGAN_2, globalMap.get(OrderEmailConstants.HK_EMAIL_SLOGAN_2));
			wlMap.put(OrderEmailConstants.HK_OFFER_POOL_ID_DATA_ORDER_CONF, globalMap.get(OrderEmailConstants.HK_OFFER_POOL_ID_DATA_ORDER_CONF));
			wlMap.put(OrderEmailConstants.HK_OFFER_POOL_ID_LITERAL, globalMap.get(OrderEmailConstants.HK_OFFER_POOL_ID_LITERAL));
			wlMap.put(OrderEmailConstants.HK_OFFER_SRC_ID_DATA_ORDER_CONF, globalMap.get(OrderEmailConstants.HK_OFFER_SRC_ID_DATA_ORDER_CONF));
			wlMap.put(OrderEmailConstants.HK_OFFER_SRC_ID_LITERAL, globalMap.get(OrderEmailConstants.HK_OFFER_SRC_ID_LITERAL));
			wlMap.put(OrderEmailConstants.HK_SERVER, globalMap.get(OrderEmailConstants.HK_SERVER));
			wlMap.put(OrderEmailConstants.HK_URL_PARAMETER_1, globalMap.get(OrderEmailConstants.HK_URL_PARAMETER_1));
			wlMap.put(OrderEmailConstants.HK_URL_PARAMETER_2, globalMap.get(OrderEmailConstants.HK_URL_PARAMETER_2));
			wlMap.put(OrderEmailConstants.HK_VENDOR_ID_DATA, globalMap.get(OrderEmailConstants.HK_VENDOR_ID_DATA));
			wlMap.put(OrderEmailConstants.HK_VENDOR_ID_LITERAL, globalMap.get(OrderEmailConstants.HK_VENDOR_ID_LITERAL));
			return wlMap;
		}
		else
			throw new Exception("");

	}

	/**
	 * create the unencrypted offer string
	 * 
	 * @param Hashmap containing webloyalty offer data to be included as part of href
	 * @return String that will contain webloyalty offer data to be included as part of href
	 */
	public String createUnEncryptedOffer(HashMap wlOfferMap)
	{
		StringBuffer unEncryptedSB = new StringBuffer();

		char cTokenizer = (char) OrderEmailConstants.DEFAULT_TOKENIZER;

		//append src literal
		unEncryptedSB.append(wlOfferMap.get(OrderEmailConstants.HK_OFFER_SRC_ID_LITERAL));
		unEncryptedSB.append(cTokenizer);

		//append src data
		unEncryptedSB.append(wlOfferMap.get(OrderEmailConstants.HK_OFFER_SRC_ID_DATA_ORDER_CONF));
		unEncryptedSB.append(cTokenizer);

		//append pool literal
		unEncryptedSB.append(wlOfferMap.get(OrderEmailConstants.HK_OFFER_POOL_ID_LITERAL));
		unEncryptedSB.append(cTokenizer);

		//append pool data
		unEncryptedSB.append(wlOfferMap.get(OrderEmailConstants.HK_OFFER_POOL_ID_DATA_ORDER_CONF));

		return unEncryptedSB.toString();

	}


	/**
	 * create the unencrypted token - note the master order is MD5 encrypted
	 * 
	 * @param Hashmap containing webloyalty token data to be included as part of href
	 * @return String that will contain webloyalty token data to be included as part of href
	 */
	public String createUnEncryptedToken(HashMap wlMap, String MD5EncryptedMasterOrderNumber)
	{
		StringBuffer unEncryptedSB = new StringBuffer();

		char cTokenizer = (char) OrderEmailConstants.DEFAULT_TOKENIZER;

		//append vendor literal
		unEncryptedSB.append(wlMap.get(OrderEmailConstants.HK_VENDOR_ID_LITERAL));
		unEncryptedSB.append(cTokenizer);

		//append vendor data
		unEncryptedSB.append(wlMap.get(OrderEmailConstants.HK_VENDOR_ID_DATA));
		unEncryptedSB.append(cTokenizer);

		//append cref literal
		unEncryptedSB.append(OrderEmailConstants.CREF);
		unEncryptedSB.append(cTokenizer);

		//append cref data - this is MD5 encrypted master order number
		unEncryptedSB.append(MD5EncryptedMasterOrderNumber);

		return unEncryptedSB.toString();

	}


	/**
	 * process Web loyalty code, and return the URL with encrypted info.  
	 * 
	 * processing includes:
	 *    retrieve and create an offer string (a Webloyalty URL parm) from the Global
	 *    encrypt the offer string using Webloyalty encryptiong
	 *    encrypt the master order number using MD5 encryption
	 *    insert record in the CLEAN.webloyalty_master table
	 *    create and return a url for webloyalty
	 *    
	 * @param connection
	 * @param source code
	 * @return source code vo
	 */
	public String processWebloyalty(Connection conn, OrderVO order, String userId) throws Exception
	{
		WebloyaltyUtilities wlUtil = new WebloyaltyUtilities();
		OrderEmailDAO oeDAO = new OrderEmailDAO(conn);
		String wlUrl = "";
		HashMap wlGlobalMap; 

		//variables used to create a token
		String MD5EncryptedMasterOrderNumber = null;
		String unEncryptedToken = null;
		String wlEncryptedToken = null;

		//variables uses to create offer
		String unEncryptedOffer = null;
		String wlEncryptedOffer = null;

		//retrieve the global parms
		wlGlobalMap = this.retrieveWebloyaltyGlobalParms(conn);

		//create unencrypted Offer string using the parms from wlMap
		unEncryptedOffer = this.createUnEncryptedOffer(wlGlobalMap);

		//encrypted the offer string
		wlEncryptedOffer = wlUtil.encrypt(unEncryptedOffer);

		//retrieve todays date
		Calendar cToday = Calendar.getInstance(); 

		String toBeMD5Encrypted = order.getMasterOrderNumber()+cToday.getTimeInMillis(); 

		//encrypt the master order number using MD5 hash
		MD5EncryptedMasterOrderNumber = oeDAO.getCrefMD5(toBeMD5Encrypted);

		String MD5EncryptedHexMasterOrderNumber = "";

		//convert the MD5 hash value to its hex representation
		for (int i = 0; i<MD5EncryptedMasterOrderNumber.length(); i++)
		{
			MD5EncryptedHexMasterOrderNumber += Integer.toHexString(MD5EncryptedMasterOrderNumber.charAt(i));
		}

		//create an unencrypted token.
		unEncryptedToken = this.createUnEncryptedToken(wlGlobalMap, MD5EncryptedHexMasterOrderNumber);

		//encrypt the token using Webloyalty encryption
		wlEncryptedToken = wlUtil.encrypt(unEncryptedToken);

		logger.info("OrderEmailUtilities - toBeMD5Encrypted = " + toBeMD5Encrypted +
				" and MD5EncryptedHexMasterOrderNumber = " + MD5EncryptedHexMasterOrderNumber +
				" and unEncryptedOffer = " + unEncryptedOffer +
				" and wlEncryptedOffer = " + wlEncryptedOffer +
				" and unEncryptedToken = " + unEncryptedToken +
				" and wlEncryptedToken = " + wlEncryptedToken);

		//insert records in the webloyalty master
		oeDAO.insertWebloyalty(order, MD5EncryptedHexMasterOrderNumber, userId);

		//create the URL
		wlUrl += ((String)wlGlobalMap.get(OrderEmailConstants.HK_SERVER)); 
		wlUrl += "?"; 
		wlUrl += ((String)wlGlobalMap.get(OrderEmailConstants.HK_URL_PARAMETER_1)); 
		wlUrl += "="; 
		wlUrl += wlEncryptedOffer; 
		wlUrl += "&"; 
		wlUrl += ((String)wlGlobalMap.get(OrderEmailConstants.HK_URL_PARAMETER_2)); 
		wlUrl += "="; 
		wlUrl += wlEncryptedToken; 

		return wlUrl;   
	}

	public String replaceOrderTokens(Connection conn, String originalContent, String contentType,
			OrderVO order) throws Exception {
		
		try {
			cu = ConfigurationUtil.getInstance();
		} catch (Exception e) {
			logger.error("Error caught instantiating Configuration util " + e.getMessage());
			throw new RuntimeException(e);
		}
		this.connection = conn;
		String returnString = replaceOrderTokens(originalContent, contentType, order);
		return returnString;
		
	}
}
