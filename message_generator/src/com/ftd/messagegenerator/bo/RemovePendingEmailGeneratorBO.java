package com.ftd.messagegenerator.bo;


import java.sql.Connection;

import com.ftd.messagegenerator.bo.helper.OrderEmailHelper;
import com.ftd.osp.utilities.email.EmailVO;
import com.ftd.messagegenerator.vo.PointOfContactVO;
import com.ftd.osp.utilities.order.ScrubMapperDAO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.plugins.Logger;



public class RemovePendingEmailGeneratorBO 
{
    private Logger logger;
    
    public RemovePendingEmailGeneratorBO()
    {
        logger = new Logger("com.ftd.messagegenerator.bo.RemovePendingEmailGeneratorBO");
    }

    public EmailVO process(Connection connection,PointOfContactVO pocObj) throws Exception
    {
  	    if (logger.isDebugEnabled()) {
            logger.debug("Starting to process email GUID:" + pocObj.getOrderGuid());
  	    }
        
        ScrubMapperDAO dao = new ScrubMapperDAO(connection);
        OrderVO order =   dao.mapOrderFromDB(pocObj.getOrderGuid());      
                
        EmailVO emailVO = new EmailVO();
        emailVO.setContentType(EmailVO.MULTIPART);
        emailVO.setSubject(pocObj.getEmailSubject());
        emailVO.setContent(pocObj.getBody());
        emailVO.setRecipient(pocObj.getRecipientEmailAddress());
        emailVO.setSender(pocObj.getSenderEmailAddress());
    
        OrderEmailHelper helper = new OrderEmailHelper(connection);       
        helper.updateEmailOrderTokens(emailVO, order);
        
        
        //retrieve the email content and replace any hard returns with html break tags
        //replace link tokens as well
        String emailContent = emailVO.getContent();
        emailContent = emailContent.replaceAll("\n", "<BR>");
        emailContent = emailContent.replaceAll("http://www.ftd.com/350/catalog/international.epl", "internationalflowerdelivery");
        emailContent = emailContent.replaceAll("http://www.ftd.com/rspecial", "emailspecial");
        emailContent = emailContent.replaceAll("http://custserv.ftd.com/email.epl", "<a href=\"http://custserv.ftd.com/\"><font size=\"2\">http://custserv.ftd.com</font></a>");
        emailContent = emailContent.replaceAll("custserv@ftd.com", "<a href=\"http://custserv.ftd.com/\"><font size=\"2\">http://custserv.ftd.com</font></a>");
        emailContent = emailContent.replaceAll("http://www.ftd.com", "<a href=\"http://www.ftd.com/\"><font size=\"2\">http://www.ftd.com</font></a>");
        emailContent = emailContent.replaceAll("www.walmart.com/ftdflowers", "<a href=\"http://www.walmart.com/ftdflowers/\"><font size=\"2\">http://www.walmart.com/ftdflowers</font></a>");
        emailContent = emailContent.replaceAll("http://www.walmart.com/cservice/cu_commentsonline.gsp\\?cu_heading\\=8", "<a href=\"http://www.walmart.com/cservice/cu_commentsonline.gsp?cu_heading=8/\"><font size=\"2\">http://www.walmart.com/cservice/cu_commentsonline.gsp?cu_heading=8</font></a>");
        emailContent = emailContent.replaceAll("internationalflowerdelivery", "<a href=\"http://www.ftd.com/international-flower-delivery//\"><font size=\"2\">http://www.ftd.com/international-flower-delivery/</font></a>");
        emailContent = emailContent.replaceAll("emailspecial", "<a href=\"http://www.ftd.com/rspecial/\"><font size=\"2\">http://www.ftd.com/rspecial</font></a>");
         
        emailVO.appendHTMLContent(helper.startGlobalHTML());
        emailVO.appendHTMLContent(emailContent);
        emailVO.appendHTMLContent(helper.closeGlobalHTML());        
 
        emailVO = helper.performSpecialFormatting(emailVO);
        
        return emailVO;
    }
}
