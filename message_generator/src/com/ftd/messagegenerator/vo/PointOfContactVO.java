package com.ftd.messagegenerator.vo;
import java.io.StringReader;
import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import com.ftd.osp.utilities.xml.DOMUtil;


public class PointOfContactVO extends BaseVO implements XMLInterface
{

	private long		  pointOfContactId;
	private long 		  customerId;
	private String		orderGuid;
	private long		  orderDetailId;
	private String    masterOrderNumber;
	private String    externalOrderNumber;
	private Calendar  deliveryDate;
	private String    companyId;
	private String    firstName;
	private String    lastName;
	private String    daytimePhoneNumber;
	private String    eveningPhoneNumber;  
	private String		senderEmailAddress;
	private String    letterTitle;
	private String		emailSubject;
	private String  	body;
	private String    commentType;
	private String		recipientEmailAddress;
	private String 		newRecipFirstName;
	private String 		newRecipLastName;
	private String		newAddress;
	private String		newCity;
	private String    newPhone;
	private String    newZipCode;
	private String    newState;
	private String    newCountry;
	private String    newInstitution;
	private Calendar  newDeliveryDate;
	private String    newCardMessage;
	private int       newProduct;
	private String    changeCodes;
	private String    sentReceivedIndicator;
	private String    sendFlag;
	private String    printFlag;
	private Calendar 	createdOn;
	private String 		createdBy;
	private Calendar 	updatedOn;
	private String 		updatedBy;
	private String    pointOfContactType;
	private String    templateId;
	private Document dataDocument;
	private String newCardSignature;
	private String originId;
	private String mailserverCode;
	private String sourceCode;
	private String emailType;
	private String commentText;
	private String recordAttributesXML;
	private String bounceProcessedFlag;

  public PointOfContactVO()
  {
  }
  
  public String getBounceProcessedFlag() {
	return bounceProcessedFlag;
}

public void setBounceProcessedFlag(String bounceProcessedFlag) {
	this.bounceProcessedFlag = bounceProcessedFlag;
}

public String getEmailType() {
	  return emailType;
  }


  public void setEmailType(String emailType) {
	  this.emailType = emailType;
  }


  public String getSourceCode() {
	  return sourceCode;
  }


  public void setSourceCode(String sourceCode) {
	  this.sourceCode = sourceCode;
  }


  public void setCustomerId(long customerId)
  {
	  if(valueChanged(this.customerId, customerId ))
	  {
		  setChanged(true);
	  }
	  this.customerId = customerId;
  }


  public long getCustomerId()
  {
	  return customerId;
  }

  public void setCreatedOn(Calendar createdOn)
  {
	  if(valueChanged(this.createdOn, createdOn))
	  {
		  setChanged(true);
	  }
	  this.createdOn = createdOn;
  }


  public Calendar getCreatedOn()
  {
	  return createdOn;
  }


  public void setCreatedBy(String createdBy)
  {
	  if(valueChanged(this.createdBy, createdBy))
	  {
		  setChanged(true);
	  }

	  this.createdBy = trim(createdBy);
  }


  public String getCreatedBy()
  {
	  return createdBy;
  }


  public void setUpdatedOn(Calendar updatedOn)
  {
	  if(valueChanged(this.updatedOn, updatedOn ))
	  {
		  setChanged(true);
	  }
	  this.updatedOn = updatedOn;
  }


  public Calendar getUpdatedOn()
  {
	  return updatedOn;
  }


  public void setUpdatedBy(String updatedBy)
  {
	  if(valueChanged(this.updatedBy, updatedBy ))
	  {
		  setChanged(true);
	  }
	  this.updatedBy = trim(updatedBy);
  }


  public String getUpdatedBy()
  {
	  return updatedBy;
  }


  public void setPointOfContactId(long pointOfContactId)
  {
	  if(valueChanged(this.pointOfContactId, pointOfContactId))
	  {
		  setChanged(true);
	  }
	  this.pointOfContactId = pointOfContactId;
  }


  public long getPointOfContactId()
  {
	  return pointOfContactId;
  }


  public void setOrderGuid(String orderGuid)
  {
	  if(valueChanged(this.orderGuid, orderGuid))
	  {
		  setChanged(true);
	  }
	  this.orderGuid = trim(orderGuid);
  }


  public String getOrderGuid()
  {
	  return orderGuid;
  }


  public void setOrderDetailId(long orderDetailId)
  {
	  if(valueChanged(this.orderDetailId, orderDetailId))
	  {
		  setChanged(true);
	  }
	  this.orderDetailId = orderDetailId;
  }


  public long getOrderDetailId()
  {
	  return orderDetailId;
  }



  public void setBody(String body)
  {
	  if(valueChanged(this.body, body))
	  {
		  setChanged(true);
	  }
	  this.body = body;
  }


  public String getBody()
  {
	  return body;
  }


  public void setSentReceivedIndicator(String sentReceivedIndicator)
  {
	  if(valueChanged(this.sentReceivedIndicator, sentReceivedIndicator))
	  {
		  setChanged(true);
	  }
	  this.sentReceivedIndicator = sentReceivedIndicator;
  }


  public String getSentReceivedIndicator()
  {
	  return sentReceivedIndicator;
  }


  public void setFirstName(String firstName)
  {
	  if(valueChanged(this.firstName, firstName))
	  {
		  setChanged(true);
	  }
	  this.firstName = trim(firstName);
  }


  public String getFirstName()
  {
	  return firstName;
  }


  public void setLastName(String lastName)
  {
	  if(valueChanged(this.lastName, lastName))
	  {
		  setChanged(true);
	  }
	  this.lastName = trim(lastName);
  }


  public String getLastName()
  {
	  return lastName;
  }


  public void setEveningPhoneNumber(String eveningPhoneNumber)
  {
	  if(valueChanged(this.eveningPhoneNumber, eveningPhoneNumber))
	  {
		  setChanged(true);
	  }
	  this.eveningPhoneNumber = trim(eveningPhoneNumber);
  }


  public String getEveningPhoneNumber()
  {
	  return eveningPhoneNumber;
  }


  /**
   * This method uses the Reflection API to generate an XML string that will be
   * passed back to the calling module.
   * The XML string will contain all the fields within this VO, including the
   * variables as well as (a collection of) ValueObjects.
   *
   * @param  None
   * @return XML string
   **/
  public String toXML()
  {
	  StringBuffer sb = new StringBuffer();
	  try
	  {
		  sb.append("<PointOfContactVO>");
		  Field[] fields = this.getClass().getDeclaredFields();

		  for (int i = 0; i < fields.length; i++)
		  {
			  //if the field retrieved was a list of VO
			  if(fields[i].getType().equals(Class.forName("java.util.List")))
			  {
				  List list = (List)fields[i].get(this);
				  if(list != null)
				  {
					  for (int j = 0; j < list.size(); j++)
					  {
						  XMLInterface xmlInt = (XMLInterface)list.get(j);
						  String sXmlVO = xmlInt.toXML();
						  sb.append(sXmlVO);
					  }
				  }
			  }
			  else
			  {
				  //if the field retrieved was a VO
				  if (fields[i].getType().toString().matches("(?i).*vo"))
				  {
					  XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
					  String sXmlVO = xmlInt.toXML();
					  sb.append(sXmlVO);
				  }
				  //if the field retrieved was a Calendar object
				  else if (fields[i].getType().toString().matches("(?i).*calendar"))
				  {
					  Date date;
					  String fDate = null;
					  if (fields[i].get(this) != null)
					  {
						  date = (((GregorianCalendar)fields[i].get(this)).getTime());
						  SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
						  fDate = sdf.format(date).toString();
						  sb.append("<" + fields[i].getName() + ">");
						  sb.append(fDate);
						  sb.append("</" + fields[i].getName() + ">");
					  }
					  else
					  {
						  sb.append("<" + fields[i].getName() + "/>");
					  }
				  }
				  else
				  {
					  if(fields[i].get(this)!=null)
					  {
						  sb.append("<" + fields[i].getName() + ">");
						  sb.append(fields[i].get(this));
						  sb.append("</" + fields[i].getName() + ">");
					  }
					  else
					  {
						  sb.append("<" + fields[i].getName() + "/>");
					  }
				  }
			  }
		  }
		  sb.append("</PointOfContactVO>");
	  }

	  catch (Exception e)
	  {
		  e.printStackTrace();
	  }

	  return sb.toString();
  }


  private String trim(String str)
  {
	  return (str != null)?str.trim():str;
  }


  public void setMasterOrderNumber(String masterOrderNumber)
  {
	  this.masterOrderNumber = masterOrderNumber;
  }


  public String getMasterOrderNumber()
  {
	  return masterOrderNumber;
  }


  public void setDeliveryDate(Calendar deliveryDate)
  {
	  this.deliveryDate = deliveryDate;
  }


  public Calendar getDeliveryDate()
  {
	  return deliveryDate;
  }


  public void setCompanyId(String companyId)
  {
	  this.companyId = companyId;
  }


  public String getCompanyId()
  {
	  return companyId;
  }


  public void setDaytimePhoneNumber(String daytimePhoneNumber)
  {
	  this.daytimePhoneNumber = daytimePhoneNumber;
  }


  public String getDaytimePhoneNumber()
  {
	  return daytimePhoneNumber;
  }


  public void setSenderEmailAddress(String senderEmailAddress)
  {
	  this.senderEmailAddress = senderEmailAddress;
  }


  public String getSenderEmailAddress()
  {
	  return senderEmailAddress;
  }


  public void setLetterTitle(String letterTitle)
  {
	  this.letterTitle = letterTitle;
  }


  public String getLetterTitle()
  {
	  return letterTitle;
  }


  public void setEmailSubject(String emailSubject)
  {
	  this.emailSubject = emailSubject;
  }


  public String getEmailSubject()
  {
	  return emailSubject;
  }


  public void setCommentType(String commentType)
  {
	  this.commentType = commentType;
  }


  public String getCommentType()
  {
	  return commentType;
  }


  public void setRecipientEmailAddress(String recipientEmailAddress)
  {
	  this.recipientEmailAddress = recipientEmailAddress;
  }


  public String getRecipientEmailAddress()
  {
	  return recipientEmailAddress;
  }


  public void setNewRecipFirstName(String newRecipFirstName)
  {
	  this.newRecipFirstName = newRecipFirstName;
  }


  public String getNewRecipFirstName()
  {
	  return newRecipFirstName;
  }


  public void setNewRecipLastName(String newRecipLastName)
  {
	  this.newRecipLastName = newRecipLastName;
  }


  public String getNewRecipLastName()
  {
	  return newRecipLastName;
  }


  public void setNewAddress(String newAddress)
  {
	  this.newAddress = newAddress;
  }


  public String getNewAddress()
  {
	  return newAddress;
  }


  public void setNewCity(String newCity)
  {
	  this.newCity = newCity;
  }


  public String getNewCity()
  {
	  return newCity;
  }


  public void setNewPhone(String newPhone)
  {
	  this.newPhone = newPhone;
  }


  public String getNewPhone()
  {
	  return newPhone;
  }


  public void setNewZipCode(String newZipCode)
  {
	  this.newZipCode = newZipCode;
  }


  public String getNewZipCode()
  {
	  return newZipCode;
  }


  public void setNewCountry(String newCountry)
  {
	  this.newCountry = newCountry;
  }


  public String getNewCountry()
  {
	  return newCountry;
  }


  public void setNewInstitution(String newInstitution)
  {
	  this.newInstitution = newInstitution;
  }


  public String getNewInstitution()
  {
	  return newInstitution;
  }


  public void setNewDeliveryDate(Calendar newDeliveryDate)
  {
	  this.newDeliveryDate = newDeliveryDate;
  }


  public Calendar getNewDeliveryDate()
  {
	  return newDeliveryDate;
  }


  public void setNewCardMessage(String newCardMessage)
  {
	  this.newCardMessage = newCardMessage;
  }


  public String getNewCardMessage()
  {
	  return newCardMessage;
  }


  public void setNewProduct(int newProduct)
  {
	  this.newProduct = newProduct;
  }


  public int getNewProduct()
  {
	  return newProduct;
  }


  public void setChangeCodes(String changeCodes)
  {
	  this.changeCodes = changeCodes;
  }


  public String getChangeCodes()
  {
	  return changeCodes;
  }


  public void setSendFlag(String sendFlag)
  {
	  this.sendFlag = sendFlag;
  }


  public String getSendFlag()
  {
	  return sendFlag;
  }


  public void setPrintFlag(String printFlag)
  {
	  this.printFlag = printFlag;
  }


  public String getPrintFlag()
  {
	  return printFlag;
  }


  public void setPointOfContactType(String pointOfContactType)
  {
	  this.pointOfContactType = pointOfContactType;
  }


  public String getPointOfContactType()
  {
	  return pointOfContactType;
  }


  public void setExternalOrderNumber(String externalOrderNumber)
  {
	  this.externalOrderNumber = externalOrderNumber;
  }


  public String getExternalOrderNumber()
  {
	  return externalOrderNumber;
  }


  public void setTemplateId(String templateId)
  {
	  this.templateId = templateId;
  }


  public String getTemplateId()
  {
	  return templateId;
  }


  public void setDataDocument(Document dataDocument)
  {
	  this.dataDocument = dataDocument;
  }


  public Document getDataDocument()
  {
	  return dataDocument;
  }

  public Document xmlStringToDOM(String xmlString) throws Exception
  {
	  Document parser = null;
	  parser = DOMUtil.getDocument(xmlString);
	  return parser;
  }


  public void setNewState(String newState)
  {
	  this.newState = newState;
  }


  public String getNewState()
  {
	  return newState;
  }


  public void set_printFlag(String printFlag)
  {
	  this.printFlag = printFlag;
  }


  public String get_printFlag()
  {
	  return printFlag;
  }

  public String getNewCardSignature()
  {
	  return newCardSignature;
  }

  public void setNewCardSignature(String newCardSignature)
  {
	  this.newCardSignature = newCardSignature;
  }

  public String getOriginId()
  {
	  return originId;
  }

  public void setOriginId(String originId)
  {
	  this.originId = originId;
  }

  public String getMailserverCode() 
  {
	  return mailserverCode;    
  }

  public void setMailserverCode(String msCode) 
  {
	  this.mailserverCode = msCode;    
  }

  public String getCommentText() {
	  return commentText;
  }

  public void setCommentText(String commentText) {
	  this.commentText = commentText;
  }

  public void setRecordAttributesXML(String recordAttributesXML) {
	  this.recordAttributesXML = recordAttributesXML;
  }

  public String getRecordAttributesXML() {
	  return recordAttributesXML;
  }
 
  
}