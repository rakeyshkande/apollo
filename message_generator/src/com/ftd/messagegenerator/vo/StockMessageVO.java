package com.ftd.messagegenerator.vo;
import java.lang.reflect.Field;

import java.sql.Clob;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class StockMessageVO  extends BaseVO implements XMLInterface
{
  private String    messageId;
  private String    messageType; // email or letter
  private String    title;
  private String    subject; //will be null for letter
  private String    content;
  private String    originId;
  private String    autoResponseIndicator;
  //private String    customerId;
  //private String    companyId;
  //private String    emailAddress;
  //private String    activeIndicator;
  //private String    subscribeStatus;
  //private Calendar  subscribeDate;
  private Calendar  createdOn;
  private String    createdBy;
  private Calendar  updatedOn;
  private String    updatedBy;


  public StockMessageVO()
  {
  }

  public void setMessageId(String messageId)
  {
    if(valueChanged(this.messageId, messageId))
    {
      setChanged(true);
    }
    this.messageId = messageId;
  }


  public String getMessageId()
  {
    return messageId;
  }

   public void setCreatedOn(Calendar createdOn)
  {
    if(valueChanged(this.createdOn, createdOn))
    {
      setChanged(true);
    }
    this.createdOn = createdOn;
  }


  public Calendar getCreatedOn()
  {
    return createdOn;
  }


  public void setCreatedBy(String createdBy)
  {
    if(valueChanged(this.createdBy, createdBy))
    {
      setChanged(true);
    }

    this.createdBy = trim(createdBy);
  }


  public String getCreatedBy()
  {
    return createdBy;
  }


  public void setUpdatedOn(Calendar updatedOn)
  {
    if(valueChanged(this.updatedOn, updatedOn ))
    {
      setChanged(true);
    }
    this.updatedOn = updatedOn;
  }


  public Calendar getUpdatedOn()
  {
    return updatedOn;
  }


  public void setUpdatedBy(String updatedBy)
  {
    if(valueChanged(this.updatedBy, updatedBy ))
    {
      setChanged(true);
    }
    this.updatedBy = trim(updatedBy);
  }


  public String getUpdatedBy()
  {
    return updatedBy;
  }

  public void setMessageType(String messageType)
  {
    this.messageType = messageType;
  }


  public String getMessageType()
  {
    return messageType;
  }


  /**
    * This method uses the Reflection API to generate an XML string that will be
    * passed back to the calling module.
    * The XML string will contain all the fields within this VO, including the
    * variables as well as (a collection of) ValueObjects.
    *
    * @param  None
    * @return XML string
   **/
  public String toXML()
  {
    StringBuffer sb = new StringBuffer();
    try
    {
      sb.append("<StockMessageVO>");
      Field[] fields = this.getClass().getDeclaredFields();

      for (int i = 0; i < fields.length; i++)
      {
        //if the field retrieved was a list of VO
        if(fields[i].getType().equals(Class.forName("java.util.List")))
        {
          List list = (List)fields[i].get(this);
          if(list != null)
          {
            for (int j = 0; j < list.size(); j++)
            {
              XMLInterface xmlInt = (XMLInterface)list.get(j);
              String sXmlVO = xmlInt.toXML();
              sb.append(sXmlVO);
            }
          }
        }
        else
        {
          //if the field retrieved was a VO
          if (fields[i].getType().toString().matches("(?i).*vo"))
          {
            XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
            String sXmlVO = xmlInt.toXML();
            sb.append(sXmlVO);
          }
          //if the field retrieved was a Calendar object
          else if (fields[i].getType().toString().matches("(?i).*calendar"))
          {
            Date date;
            String fDate = null;
            if (fields[i].get(this) != null)
            {
              date = (((GregorianCalendar)fields[i].get(this)).getTime());
              SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
              fDate = sdf.format(date).toString();
            }
            sb.append("<" + fields[i].getName() + ">");
            sb.append(fDate);
            sb.append("</" + fields[i].getName() + ">");

          }
          else
          {
            sb.append("<" + fields[i].getName() + ">");
            sb.append(fields[i].get(this));
            sb.append("</" + fields[i].getName() + ">");
          }
        }
      }
      sb.append("</StockMessageVO>");
    }

    catch (Exception e)
    {
      e.printStackTrace();
    }

    return sb.toString();
  }


  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }


  public void setTitle(String title)
  {
    if(valueChanged(this.title, title ))
    {
      setChanged(true);
    }
    this.title = title;
  }


  public String getTitle()
  {
    return title;
  }


  public void setSubject(String subject)
  {
    if(valueChanged(this.subject, subject ))
    {
      setChanged(true);
    }
    this.subject = subject;
  }


  public String getSubject()
  {
    return subject;
  }


  public void setContent(String content)
  {
    if(valueChanged(this.content, content ))
    {
      setChanged(true);
    }
    this.content = content;
  }


  public String getContent()
  {
    return content;
  }


  public void setOriginId(String originId)
  {
    if(valueChanged(this.originId, originId ))
    {
      setChanged(true);
    }
    this.originId = originId;
  }


  public String getOriginId()
  {
    return originId;
  }


  public void setAutoResponseIndicator(String autoResponseIndicator)
  {
    if(valueChanged(this.autoResponseIndicator, autoResponseIndicator ))
    {
      setChanged(true);
    }
    this.autoResponseIndicator = autoResponseIndicator;
  }


  public String getAutoResponseIndicator()
  {
    return autoResponseIndicator;
  }


}