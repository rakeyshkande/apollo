package com.ftd.messagegenerator.mailer;

import java.io.IOException;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

import org.apache.ws.security.WSPasswordCallback;

/**
 * WSS4J CallbackHandler class that is invoked to retrieve the login password for ExactTarget from global_parms
 */
public class ExactTargetPasswordHandler implements CallbackHandler {
    private String password;
    public void handle( Callback[] callbacks ) throws IOException, UnsupportedCallbackException {
        WSPasswordCallback wsPasswordCallback = (WSPasswordCallback) callbacks[0];
        wsPasswordCallback.setPassword(password);
    }
    
    public void setPassword(String password) {
    	this.password = password;
    }
}
