package com.ftd.messagegenerator.mailer;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.cxf.binding.soap.saaj.SAAJOutInterceptor;   //used with bug CXF-922 workaround
import org.apache.cxf.configuration.jsse.TLSClientParameters; //used with bug CXF-922 workaround
import org.apache.cxf.configuration.security.FiltersType;     //used with bug CXF-922 workaround
import org.apache.cxf.endpoint.Client;                        //used with bug CXF-922 workaround
import org.apache.cxf.endpoint.Endpoint;                      //used with bug CXF-922 workaround
import org.apache.cxf.frontend.ClientProxy;                   //used with bug CXF-922 workaround
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.transport.http.HTTPConduit;             //used with bug CXF-922 workaround
import org.apache.cxf.ws.security.wss4j.WSS4JOutInterceptor;  //used with bug CXF-922 workaround
import org.apache.ws.security.WSConstants;
import org.apache.ws.security.handler.WSHandlerConstants;

import com.exacttarget.wsdl.partnerapi.APIObject;
import com.exacttarget.wsdl.partnerapi.Attribute;
import com.exacttarget.wsdl.partnerapi.BounceEvent;
import com.exacttarget.wsdl.partnerapi.ComplexFilterPart;
import com.exacttarget.wsdl.partnerapi.CreateOptions;
import com.exacttarget.wsdl.partnerapi.CreateRequest;
import com.exacttarget.wsdl.partnerapi.CreateResponse;
import com.exacttarget.wsdl.partnerapi.LogicalOperators;
import com.exacttarget.wsdl.partnerapi.NotSentEvent;
import com.exacttarget.wsdl.partnerapi.Owner;
import com.exacttarget.wsdl.partnerapi.PartnerAPI;
import com.exacttarget.wsdl.partnerapi.RequestType;
import com.exacttarget.wsdl.partnerapi.RetrieveRequest;
import com.exacttarget.wsdl.partnerapi.RetrieveRequestMsg;
import com.exacttarget.wsdl.partnerapi.RetrieveResponseMsg;
import com.exacttarget.wsdl.partnerapi.SimpleFilterPart;
import com.exacttarget.wsdl.partnerapi.SimpleOperators;
import com.exacttarget.wsdl.partnerapi.Soap;
import com.exacttarget.wsdl.partnerapi.Subscriber;
import com.exacttarget.wsdl.partnerapi.TriggeredSend;
import com.exacttarget.wsdl.partnerapi.TriggeredSendDefinition;
import com.ftd.messagegenerator.constants.MessageConstants;
import com.ftd.messagegenerator.utilities.JaxWsHandlerResolver;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * This class is responsible for communications with the ExactTarget (ET) Web Service 
 * for purposes of sending emails to customers.  It is meant to encapsulate and isolate 
 * all ExactTarget interaction and provide a generic class interface that can be utilized 
 * by other Message-Generator Business Objects.
 */
public class ExactTargetMailer {
    private static Logger logger = new Logger("com.ftd.messagegenerator.mailer.ExactTargetMailer"); 
    
    private static String ET_SUCCESSFUL_SEND = "OK";  // Status returned from ET when mail sent successfully

    /** 
     * Public method to construct appropriate ET object structure for an email message
     * then invoke ET Web Service to process and send email to recipient.  Returns true
     * if message was sent to ET successfully.
     * 
     * A hash of name/value pairs is passed (emailAttrMap) and contains order specific data
     * which is then mapped to ET "Attribute" objects and included in WS call.
     */
    public boolean sendEmail(String fromEmail, String toEmail, String templateName, HashMap<String,String> emailAttrMap, String companyId) throws Exception {
        boolean returnStatus = false;
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        logger.info("Sending email via ExactTarget WS to: " + toEmail + " using template: " + templateName);

        // Create ET service stub using WS URL from global_parms
        //
        String wsUrl = configUtil.getFrpGlobalParm(MessageConstants.EXACT_TARGET_WS_CONTEXT, MessageConstants.EXACT_TARGET_WS_URL);
        logger.info("ExactTarget URL: " + wsUrl);
        
        PartnerAPI etApi = new PartnerAPI(new URL(wsUrl));
        
        try {
        	//For testing - cordial integration
	        String logSoapMessage = configUtil.getFrpGlobalParm(MessageConstants.MESSAGE_GEN_CONFIG_CONTEXT, MessageConstants.LOG_SOAP_MESSAGE);
	        if("Y".equalsIgnoreCase(logSoapMessage)){
	        	etApi.setHandlerResolver(new JaxWsHandlerResolver());
	        }
        } catch(Exception e) {
        	logger.error("Unable to log soapXML, ignoring the exception and continuing with sending email. ", e); 
        }
        //end
        Soap serviceStub = etApi.getSoap();

        // Begin programmatic workaround for bug CXF-922 associated with transport via HTTPS.
        // CXF 2.0.2 does not load the http conduit XML configuration as designed using Spring.
        //
        Map<String, Object> outProperties = new HashMap<String, Object>();
        Client client = ClientProxy.getClient(serviceStub);

        // Add WSS4J security header
        Endpoint endPoint = client.getEndpoint();
        WSS4JOutInterceptor wssOut = new WSS4JOutInterceptor( outProperties );
        endPoint.getOutInterceptors().add( wssOut );
        endPoint.getOutInterceptors().add( new SAAJOutInterceptor() );
        endPoint.getInInterceptors().add( new LoggingInInterceptor() );
        endPoint.getOutInterceptors().add( new LoggingOutInterceptor() );

        String userId = null;
        String password = null;
        if (companyId != null) {
        	userId = configUtil.getSecureProperty(MessageConstants.EXACT_TARGET_WS_CONTEXT, MessageConstants.EXACT_TARGET_WS_USERNAME + "_" + companyId);
        	password = configUtil.getSecureProperty(MessageConstants.EXACT_TARGET_WS_CONTEXT, MessageConstants.EXACT_TARGET_WS_PASSWORD + "_" + companyId);
        }
        if (userId == null) {
        	userId = configUtil.getSecureProperty(MessageConstants.EXACT_TARGET_WS_CONTEXT, MessageConstants.EXACT_TARGET_WS_USERNAME);
        	password = configUtil.getSecureProperty(MessageConstants.EXACT_TARGET_WS_CONTEXT, MessageConstants.EXACT_TARGET_WS_PASSWORD);
        }
        outProperties.put( WSHandlerConstants.ACTION, WSHandlerConstants.USERNAME_TOKEN );
        outProperties.put( WSHandlerConstants.USER, userId);
        outProperties.put( WSHandlerConstants.PASSWORD_TYPE, WSConstants.PW_TEXT );
        ExactTargetPasswordHandler handler = new ExactTargetPasswordHandler();
        handler.setPassword(password);
        outProperties.put( WSHandlerConstants.PW_CALLBACK_REF, handler);
        HTTPConduit conduit = ( HTTPConduit )client.getConduit();
        
        setTLSSocketParams(configUtil, conduit);

        // Specify ET TriggeredSendDefinition and initialize the TriggeredSend
        //
        TriggeredSendDefinition triggeredSendDefinition = new TriggeredSendDefinition();
        triggeredSendDefinition.setCustomerKey(templateName);  // External_Key
        TriggeredSend triggeredSend = new TriggeredSend();
        triggeredSend.setTriggeredSendDefinition(triggeredSendDefinition);
        
        // Create ET Subscriber and Owner with to/from email addresses
        //
        Subscriber subscriber = new Subscriber();
        subscriber.setEmailAddress(toEmail);
        subscriber.setSubscriberKey(toEmail);  //unique identifier for this email_Id
        // Create an owner for subscriber
        Owner ownerSubscriber = new Owner();
        ownerSubscriber.setFromAddress(fromEmail);
        // ownerSubscriber.setFromName("FTD Custserv");  // This can be used to include descriptive name in "From" email field. If not set, defaults to "From" email. 
        subscriber.setOwner(ownerSubscriber);  // Add owner to subscriber
        
        // Take all order attribute data and convert to ET Attribute objects in ET Subscriber obj
        //
        mapAttributes(emailAttrMap, subscriber);
        
        // Add subscriber/owner/attributes to TriggerSend obj
        triggeredSend.getSubscribers().addAll(Arrays.asList(new Subscriber[] {subscriber}));
        
        // Send the TriggeredSend (asynchronously) using Create call
        //
        CreateOptions createOptions = new CreateOptions();
        createOptions.setRequestType(RequestType.ASYNCHRONOUS);  
        CreateRequest createRequest = new CreateRequest();
        createRequest.setOptions( createOptions );
        java.util.List<APIObject> listAPIObject = Arrays.asList( new APIObject[] {triggeredSend} );
        createRequest.getObjects().addAll( listAPIObject );
        CreateResponse createResponse = serviceStub.create( createRequest );
        logger.info("Request id: " + createResponse.getRequestID());
        if (ET_SUCCESSFUL_SEND.equalsIgnoreCase(createResponse.getOverallStatus())) {
            returnStatus = true;
            logger.info("Mail sent to ExactTarget successfully");
        } else {
            logger.error("Response status from ExactTarget indicates mail was not queueued successfully");                
        }
        
        return returnStatus;
    }

    /** 
     * Set TLS Socket params only when SET_SOCKET_PARAMS config is set to Y
     */
	private void setTLSSocketParams(ConfigurationUtil configUtil, HTTPConduit conduit) throws Exception {
		String setSocketParams = configUtil.getFrpGlobalParm(MessageConstants.MESSAGE_GEN_CONFIG_CONTEXT, MessageConstants.SET_SOCKET_PARAMS);
        if("Y".equalsIgnoreCase(setSocketParams)){ 
			String secureSocketProtocol = configUtil.getFrpGlobalParm(MessageConstants.EXACT_TARGET_WS_CONTEXT, MessageConstants.EXACT_TARGET_PROTOCOL);
	        String cipherSuite = configUtil.getFrpGlobalParm(MessageConstants.EXACT_TARGET_WS_CONTEXT, MessageConstants.EXACT_TARGET_CIPHER);
	        logger.info(secureSocketProtocol + " " + cipherSuite);
	        TLSClientParameters tlsClientParameters = new TLSClientParameters();
	        tlsClientParameters.setSecureSocketProtocol(secureSocketProtocol);
	        conduit.setTlsClientParameters(tlsClientParameters);
	        FiltersType filters = new FiltersType();
	    	filters.getInclude().add(cipherSuite);
	        tlsClientParameters.setCipherSuitesFilter( filters ); //sets filter to include added cipher suites
        }  
	}
    
    /** 
     * Private method to map order (name/value) attributes into ExactTarget Attribute objects
     */
    private void mapAttributes(HashMap<String,String> emailAttrMap, Subscriber subscriber) {
        Attribute attr = null;
        
        for (Map.Entry<String,String> entry : emailAttrMap.entrySet()) {
            attr = new Attribute();
            attr.setName(entry.getKey());
            attr.setValue(entry.getValue());
            subscriber.getAttributes().add(attr);                    
        }
    }
    
    /**
     * This method will retrieve Object Id based on the customerKey
     */
    public String getObjectIdByCustomerKey(String customerKey, String companyId) throws Exception 
    {
        String objectId = null;
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
		if (logger.isDebugEnabled()) {
            logger.debug("Getting ObjectID using customerKey by calling ExactTarget WS using customerKey: " + customerKey);
		}

        // Create ET service stub using WS URL from global_parms
        //
        String wsUrl = configUtil.getFrpGlobalParm(MessageConstants.EXACT_TARGET_WS_CONTEXT, MessageConstants.EXACT_TARGET_WS_URL);
        if (logger.isDebugEnabled()) {
            logger.debug("Utilizing ExactTarget WS URL: " + wsUrl);
        }
        PartnerAPI etApi = new PartnerAPI(new URL(wsUrl));
        Soap serviceStub = etApi.getSoap();
                    
        // Begin programmatic workaround for bug CXF-922 associated with transport via HTTPS.
        // CXF 2.0.2 does not load the http conduit XML configuration as designed using Spring.
        //
        Map<String, Object> outProperties = new HashMap<String, Object>();
        Client client = ClientProxy.getClient(serviceStub);

        // Add WSS4J security header
        Endpoint endPoint = client.getEndpoint();
        WSS4JOutInterceptor wssOut = new WSS4JOutInterceptor( outProperties );
        endPoint.getOutInterceptors().add( wssOut );
        endPoint.getOutInterceptors().add( new SAAJOutInterceptor() );
        endPoint.getInInterceptors().add( new LoggingInInterceptor() );
        endPoint.getOutInterceptors().add( new LoggingOutInterceptor() );

        String userId = null;
        String password = null;
        if (companyId != null) {
        	userId = configUtil.getSecureProperty(MessageConstants.EXACT_TARGET_WS_CONTEXT, MessageConstants.EXACT_TARGET_WS_USERNAME + "_" + companyId);
        	password = configUtil.getSecureProperty(MessageConstants.EXACT_TARGET_WS_CONTEXT, MessageConstants.EXACT_TARGET_WS_PASSWORD + "_" + companyId);
        }
        if (userId == null) {
        	userId = configUtil.getSecureProperty(MessageConstants.EXACT_TARGET_WS_CONTEXT, MessageConstants.EXACT_TARGET_WS_USERNAME);
        	password = configUtil.getSecureProperty(MessageConstants.EXACT_TARGET_WS_CONTEXT, MessageConstants.EXACT_TARGET_WS_PASSWORD);
        }
        
        outProperties.put( WSHandlerConstants.ACTION, WSHandlerConstants.USERNAME_TOKEN );
        outProperties.put( WSHandlerConstants.USER, userId );
        outProperties.put( WSHandlerConstants.PASSWORD_TYPE, WSConstants.PW_TEXT );
        //outProperties.put( WSHandlerConstants.PW_CALLBACK_CLASS, ExactTargetPasswordHandler.class.getName() );  
        ExactTargetPasswordHandler handler = new ExactTargetPasswordHandler();
        handler.setPassword(password);
        outProperties.put( WSHandlerConstants.PW_CALLBACK_REF, handler);
        HTTPConduit conduit = ( HTTPConduit )client.getConduit();
       
        setTLSSocketParams(configUtil, conduit);
        
        RetrieveRequest rr  = new RetrieveRequest();
        RetrieveRequestMsg rrm = new RetrieveRequestMsg();
        
        rr.setObjectType("TriggeredSendSummary");
        rr.getProperties().add("ObjectID");
        
        SimpleFilterPart filter = new SimpleFilterPart();
        filter.setProperty("CustomerKey");
        filter.setSimpleOperator(SimpleOperators.EQUALS);
        filter.getValue().add(customerKey);
        
        rr.setFilter(filter);  
        rrm.setRetrieveRequest(rr);
        
        RetrieveResponseMsg rsm = serviceStub.retrieve(rrm);
        java.util.List<APIObject> listResp = rsm.getResults();
        if(listResp != null && listResp.size() > 0)
        {
        	APIObject result = listResp.get(0); //Assuming only one objectId is being returned for the customerkey
        	objectId = result.getObjectID();
        }        
        
        return objectId;
   }
    
    public ArrayList<String> getBouncedEmailAddresses(String objectId,GregorianCalendar gc, String companyId) throws Exception 
    {
    	ArrayList<String> emailAddresses = null;

    	ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
    	logger.info("Getting bounce emails for objectId: " + objectId);

    	// Create ET service stub using WS URL from global_parms
    	//
    	String wsUrl = configUtil.getFrpGlobalParm(MessageConstants.EXACT_TARGET_WS_CONTEXT, MessageConstants.EXACT_TARGET_WS_URL);
    	if (logger.isDebugEnabled()) {
    		logger.debug("Utilizing ExactTarget WS URL: " + wsUrl);
    	}
    	PartnerAPI etApi = new PartnerAPI(new URL(wsUrl));
    	Soap serviceStub = etApi.getSoap();

    	// Begin programmatic workaround for bug CXF-922 associated with transport via HTTPS.
    	// CXF 2.0.2 does not load the http conduit XML configuration as designed using Spring.
    	//
    	Map<String, Object> outProperties = new HashMap<String, Object>();
    	Client client = ClientProxy.getClient(serviceStub);

    	// Add WSS4J security header
    	Endpoint endPoint = client.getEndpoint();
    	WSS4JOutInterceptor wssOut = new WSS4JOutInterceptor( outProperties );
    	endPoint.getOutInterceptors().add( wssOut );
    	endPoint.getOutInterceptors().add( new SAAJOutInterceptor() );
    	endPoint.getInInterceptors().add( new LoggingInInterceptor() );
    	endPoint.getOutInterceptors().add( new LoggingOutInterceptor() );

        String userId = null;
        String password = null;
        if (companyId != null) {
        	userId = configUtil.getSecureProperty(MessageConstants.EXACT_TARGET_WS_CONTEXT, MessageConstants.EXACT_TARGET_WS_USERNAME + "_" + companyId);
        	password = configUtil.getSecureProperty(MessageConstants.EXACT_TARGET_WS_CONTEXT, MessageConstants.EXACT_TARGET_WS_PASSWORD + "_" + companyId);
        }
        if (userId == null) {
        	userId = configUtil.getSecureProperty(MessageConstants.EXACT_TARGET_WS_CONTEXT, MessageConstants.EXACT_TARGET_WS_USERNAME);
        	password = configUtil.getSecureProperty(MessageConstants.EXACT_TARGET_WS_CONTEXT, MessageConstants.EXACT_TARGET_WS_PASSWORD);
        }
        
    	outProperties.put( WSHandlerConstants.ACTION, WSHandlerConstants.USERNAME_TOKEN );
    	outProperties.put( WSHandlerConstants.USER, userId );
    	outProperties.put( WSHandlerConstants.PASSWORD_TYPE, WSConstants.PW_TEXT );
    	//outProperties.put( WSHandlerConstants.PW_CALLBACK_CLASS, ExactTargetPasswordHandler.class.getName() );  
        ExactTargetPasswordHandler handler = new ExactTargetPasswordHandler();
        handler.setPassword(password);
        outProperties.put( WSHandlerConstants.PW_CALLBACK_REF, handler);
    	HTTPConduit conduit = ( HTTPConduit )client.getConduit();
    	
    	setTLSSocketParams(configUtil, conduit);
    	
    	RetrieveRequest rr  = new RetrieveRequest();
    	RetrieveRequestMsg rrm = new RetrieveRequestMsg();      
    	rr.setObjectType("BounceEvent");
    	rr.getProperties().add("ObjectID");
    	rr.getProperties().add("BounceType");
    	rr.getProperties().add("BatchID");
    	rr.getProperties().add("SendID");
    	rr.getProperties().add("SMTPCode");
    	rr.getProperties().add("SMTPReason");
    	rr.getProperties().add("SubscriberKey");
    	rr.getProperties().add("TriggeredSendDefinitionObjectID");
    	rr.getProperties().add("EventDate");
    	rr.getProperties().add("ModifiedDate");

    	TimeZone utc = TimeZone.getTimeZone("UTC");
    	gc.setTimeZone(utc);
    	DatatypeFactory dtf = DatatypeFactory.newInstance();
    	XMLGregorianCalendar xgc =  dtf.newXMLGregorianCalendar(gc);
    	logger.info("DateValue: " + xgc);

    	SimpleFilterPart dateFilter = new SimpleFilterPart();
    	dateFilter.setProperty("EventDate");
    	dateFilter.setSimpleOperator(SimpleOperators.GREATER_THAN);      
    	dateFilter.getDateValue().add(xgc);

    	SimpleFilterPart tsFilter = new SimpleFilterPart();
    	tsFilter.setProperty("TriggeredSendDefinitionObjectID");
    	tsFilter.getValue().add(objectId);

    	ComplexFilterPart cfp = new ComplexFilterPart();
    	cfp.setLeftOperand(dateFilter);
    	cfp.setRightOperand(tsFilter);
    	cfp.setLogicalOperator(LogicalOperators.AND);

    	rr.setFilter(cfp);  
    	rrm.setRetrieveRequest(rr);

    	RetrieveResponseMsg rsm = serviceStub.retrieve(rrm);
    	java.util.List<APIObject> listResp = rsm.getResults();
    	if(listResp != null && listResp.size() > 0)
    	{
    		emailAddresses = new ArrayList<String>(); 
    		for(int i=0;i<listResp.size();i++)
    		{
    			BounceEvent bounceEvent = (BounceEvent)listResp.get(i);
    			emailAddresses.add(bounceEvent.getSubscriberKey());
    		}
    	}        
    	return emailAddresses;
    }
    
    public ArrayList<String> getNotSentEmailAddresses(String objectId,GregorianCalendar gc, String companyId)
            throws Exception {
    
    	ArrayList<String> emailAddresses = null;

    	ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
    	logger.info("Getting not sent emails for objectId: " + objectId);

    	// Create ET service stub using WS URL from global_parms
    	//
    	String wsUrl = configUtil.getFrpGlobalParm(MessageConstants.EXACT_TARGET_WS_CONTEXT, MessageConstants.EXACT_TARGET_WS_URL);
    	if (logger.isDebugEnabled()) {
    		logger.debug("Utilizing ExactTarget WS URL: " + wsUrl);
    	}
    	PartnerAPI etApi = new PartnerAPI(new URL(wsUrl));
    	Soap serviceStub = etApi.getSoap();

    	// Begin programmatic workaround for bug CXF-922 associated with transport via HTTPS.
    	// CXF 2.0.2 does not load the http conduit XML configuration as designed using Spring.
    	//
    	Map<String, Object> outProperties = new HashMap<String, Object>();
    	Client client = ClientProxy.getClient(serviceStub);

    	// Add WSS4J security header
    	Endpoint endPoint = client.getEndpoint();
    	WSS4JOutInterceptor wssOut = new WSS4JOutInterceptor( outProperties );
    	endPoint.getOutInterceptors().add( wssOut );
    	endPoint.getOutInterceptors().add( new SAAJOutInterceptor() );
    	endPoint.getInInterceptors().add( new LoggingInInterceptor() );
    	endPoint.getOutInterceptors().add( new LoggingOutInterceptor() );

        String userId = null;
        String password = null;
        if (companyId != null) {
        	userId = configUtil.getSecureProperty(MessageConstants.EXACT_TARGET_WS_CONTEXT, MessageConstants.EXACT_TARGET_WS_USERNAME + "_" + companyId);
        	password = configUtil.getSecureProperty(MessageConstants.EXACT_TARGET_WS_CONTEXT, MessageConstants.EXACT_TARGET_WS_PASSWORD + "_" + companyId);
        }
        if (userId == null) {
        	userId = configUtil.getSecureProperty(MessageConstants.EXACT_TARGET_WS_CONTEXT, MessageConstants.EXACT_TARGET_WS_USERNAME);
        	password = configUtil.getSecureProperty(MessageConstants.EXACT_TARGET_WS_CONTEXT, MessageConstants.EXACT_TARGET_WS_PASSWORD);
        }
        
    	outProperties.put( WSHandlerConstants.ACTION, WSHandlerConstants.USERNAME_TOKEN );
    	outProperties.put( WSHandlerConstants.USER, userId );
    	outProperties.put( WSHandlerConstants.PASSWORD_TYPE, WSConstants.PW_TEXT );
    	//outProperties.put( WSHandlerConstants.PW_CALLBACK_CLASS, ExactTargetPasswordHandler.class.getName() );  
        ExactTargetPasswordHandler handler = new ExactTargetPasswordHandler();
        handler.setPassword(password);
        outProperties.put( WSHandlerConstants.PW_CALLBACK_REF, handler);
    	HTTPConduit conduit = ( HTTPConduit )client.getConduit();
    	
    	setTLSSocketParams(configUtil, conduit);
    	
    	RetrieveRequest rr  = new RetrieveRequest();
    	RetrieveRequestMsg rrm = new RetrieveRequestMsg();      
    	rr.setObjectType("NotSentEvent");
    	rr.getProperties().add("BatchID");
    	rr.getProperties().add("SendID");
    	rr.getProperties().add("SubscriberKey");
    	rr.getProperties().add("TriggeredSendDefinitionObjectID");
    	rr.getProperties().add("EventDate");

    	TimeZone utc = TimeZone.getTimeZone("UTC");
    	gc.setTimeZone(utc);
    	DatatypeFactory dtf = DatatypeFactory.newInstance();
    	XMLGregorianCalendar xgc =  dtf.newXMLGregorianCalendar(gc);
    	logger.info("DateValue: " + xgc);

    	SimpleFilterPart dateFilter = new SimpleFilterPart();
    	dateFilter.setProperty("EventDate");
    	dateFilter.setSimpleOperator(SimpleOperators.GREATER_THAN);      
    	dateFilter.getDateValue().add(xgc);

    	SimpleFilterPart tsFilter = new SimpleFilterPart();
    	tsFilter.setProperty("TriggeredSendDefinitionObjectID");
    	tsFilter.getValue().add(objectId);

    	ComplexFilterPart cfp = new ComplexFilterPart();
    	cfp.setLeftOperand(dateFilter);
    	cfp.setRightOperand(tsFilter);
    	cfp.setLogicalOperator(LogicalOperators.AND);

    	rr.setFilter(cfp);  
    	rrm.setRetrieveRequest(rr);

    	RetrieveResponseMsg rsm = serviceStub.retrieve(rrm);
    	java.util.List<APIObject> listResp = rsm.getResults();
    	if(listResp != null && listResp.size() > 0)
    	{
    		emailAddresses = new ArrayList<String>(); 
    		for(int i=0;i<listResp.size();i++)
    		{
    			NotSentEvent notSentEvent = (NotSentEvent)listResp.get(i);
    			emailAddresses.add(notSentEvent.getSubscriberKey());
    		}
    	}        
    	return emailAddresses;
    }

}
