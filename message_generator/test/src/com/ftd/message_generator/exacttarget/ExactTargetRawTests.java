package src.com.ftd.message_generator.exacttarget;

import org.apache.cxf.binding.soap.saaj.SAAJOutInterceptor;   //used with bug CXF-922 workaround
import org.apache.cxf.configuration.jsse.TLSClientParameters; //used with bug CXF-922 workaround
import org.apache.cxf.configuration.security.FiltersType;     //used with bug CXF-922 workaround
import org.apache.cxf.endpoint.Client;                        //used with bug CXF-922 workaround
import org.apache.cxf.frontend.ClientProxy;                   //used with bug CXF-922 workaround
import org.apache.cxf.endpoint.Endpoint;                      //used with bug CXF-922 workaround
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.transport.http.HTTPConduit;             //used with bug CXF-922 workaround
import org.apache.cxf.ws.security.wss4j.WSS4JOutInterceptor;  //used with bug CXF-922 workaround

import org.apache.ws.security.handler.WSHandlerConstants;
import org.apache.ws.security.WSConstants;

import com.exacttarget.wsdl.partnerapi.APIObject;
import com.exacttarget.wsdl.partnerapi.Attribute;
import com.exacttarget.wsdl.partnerapi.ComplexFilterPart;
import com.exacttarget.wsdl.partnerapi.CreateOptions;
import com.exacttarget.wsdl.partnerapi.CreateRequest;
import com.exacttarget.wsdl.partnerapi.CreateResponse;
import com.exacttarget.wsdl.partnerapi.CreateResult;
import com.exacttarget.wsdl.partnerapi.LogicalOperators;
import com.exacttarget.wsdl.partnerapi.Owner;
import com.exacttarget.wsdl.partnerapi.PartnerAPI;
import com.exacttarget.wsdl.partnerapi.RequestType;
import com.exacttarget.wsdl.partnerapi.Result;
import com.exacttarget.wsdl.partnerapi.ResultMessage;
import com.exacttarget.wsdl.partnerapi.RetrieveRequest;
import com.exacttarget.wsdl.partnerapi.RetrieveRequestMsg;
import com.exacttarget.wsdl.partnerapi.RetrieveResponseMsg;
import com.exacttarget.wsdl.partnerapi.SimpleFilterPart;
import com.exacttarget.wsdl.partnerapi.SimpleOperators;
import com.exacttarget.wsdl.partnerapi.Soap;
import com.exacttarget.wsdl.partnerapi.Subscriber;
import com.exacttarget.wsdl.partnerapi.TriggeredSend;
import com.exacttarget.wsdl.partnerapi.TriggeredSendCreateResult;
import com.exacttarget.wsdl.partnerapi.TriggeredSendDefinition;

import junit.framework.TestCase;

import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.net.URL;

public class ExactTargetRawTests extends TestCase {
    //static String user = null;
    //static String password = null;
    private static int RETRIEVE_EVENT = 1;   // 0 will send email to ET, 
                                             // 1 to retrieve BounceEvent
                                             // 2 to retrieve TriggeredSendSummary
                                             // 3 to retrieve ResultMessage

    public ExactTargetRawTests(String name) {
		super(name);
	}

    public void testGary() {
        try {
            
            // Create PartnerAPI stub
            System.out.println("Before PartnerAPI instantiate");
            PartnerAPI service = new PartnerAPI(new URL("https://webservice.s6.exacttarget.com/etframework.wsdl"));
            System.out.println("After PartnerAPI instantiate");
            Soap stub = service.getSoap();
            
            // Begin programmatic workaround for bug CXF-922 associated with transport via HTTPS.
            // CXF 2.0.2 does not load the http conduit XML configuration as designed using Spring.
            Map<String, Object> outProperties = new HashMap<String, Object>();
            Client client = ClientProxy.getClient( stub );
            
            // Add WSS4J security header
            Endpoint endPoint = client.getEndpoint();
            WSS4JOutInterceptor wssOut = new WSS4JOutInterceptor( outProperties );
            endPoint.getOutInterceptors().add( wssOut );
            endPoint.getOutInterceptors().add( new SAAJOutInterceptor() );
            endPoint.getInInterceptors().add( new LoggingInInterceptor() );
            endPoint.getOutInterceptors().add( new LoggingOutInterceptor() );
            
            outProperties.put( WSHandlerConstants.ACTION, WSHandlerConstants.USERNAME_TOKEN );
            outProperties.put( WSHandlerConstants.USER, "ftd_api");
            outProperties.put( WSHandlerConstants.PASSWORD_TYPE, WSConstants.PW_TEXT );
            outProperties.put( WSHandlerConstants.PW_CALLBACK_CLASS, ExactTargetTestPasswordHandler.class.getName() );  
            ///HTTPConduit conduit = ( HTTPConduit )client.getConduit();
            ///TLSClientParameters tlsClientParameters = new TLSClientParameters();
            ///tlsClientParameters.setSecureSocketProtocol("SSL");
            ///conduit.setTlsClientParameters(tlsClientParameters);
            ///FiltersType filters = new FiltersType();
            //filters.getInclude().add( ".*_EXPORT_.*" );             //add all ".*_EXPORT_.*" cipher suites
            //filters.getInclude().add( ".*_EXPORT1024_.*" );         //add all ".*_EXPORT1024_.*" cipher suites
            //filters.getInclude().add( ".*_WITH_DES_.*" );           //add all ".*_WITH_DES_.*" cipher suites
            //filters.getInclude().add( ".*_WITH_NULL_.*" );          //add all ".*_WITH_NULL_.*" cipher suites
            //filters.getInclude().add( ".*_DH_anon_.*" );            //add all ".*_DH_anon_.*" cipher suites
            //filters.getInclude().add( "SSL_RSA_WITH_RC4_128_MD5" ); //add all "SSL_RSA_WITH_RC4_128_MD5" cipher suites
            ///filters.getInclude().add( "SSL_RSA_WITH_RC4_128_SHA" ); //add all "SSL_RSA_WITH_RC4_128_SHA" cipher suites
            ///tlsClientParameters.setCipherSuitesFilter( filters );   //sets filter to include added cipher suites
            //End workaround for bug CXF-922.
            
            System.out.println( "Starting Exact Target email test" );

            // Specify TriggeredSendDefinition and initialize the TriggeredSend
            TriggeredSendDefinition triggeredSendDefinition = new TriggeredSendDefinition();
//            triggeredSendDefinition.setCustomerKey("Original Test");  //this is that External_Key
            triggeredSendDefinition.setCustomerKey("Standard Test");  //this is that External_Key
            triggeredSendDefinition.setEmailSubject("Test GPS mail");  // ???
            TriggeredSend triggeredSend = new TriggeredSend();
            triggeredSend.setTriggeredSendDefinition(triggeredSendDefinition);
            
            // Create a Subscriber
            Subscriber subscriber = new Subscriber();
            subscriber.setEmailAddress("gsergey@ftdi.com");
            subscriber.setSubscriberKey("gsergey@ftdi.com");  //unique identifier for this email_Id

            // Create an owner for subscriber
            Owner ownerSubscriber = new Owner();
            ownerSubscriber.setFromAddress("noreply@ftd.com");
            ownerSubscriber.setFromName("FTD Tester");
            subscriber.setOwner(ownerSubscriber);  // Add owner to subscriber
            
            // Add attributes
            Attribute attr = new Attribute();
            attr.setName("OrderNumber");
            attr.setValue("1122334460");
            subscriber.getAttributes().add(attr);
            //
            attr = new Attribute();
            attr.setName("FirstName");
            attr.setValue("George");
            subscriber.getAttributes().add(attr);
            //
            attr = new Attribute();
            attr.setName("LastName");
            attr.setValue("Jetson");
            subscriber.getAttributes().add(attr);
            //
            attr = new Attribute();
            attr.setName("Item1");
            attr.setValue("Moon boots");
            subscriber.getAttributes().add(attr);
            //
            attr = new Attribute();
            attr.setName("EmailBody");
            attr.setValue("<h2>Hello Body this is me again</h2>");
            subscriber.getAttributes().add(attr);
            //
            attr = new Attribute();
            attr.setName("Subject"); 
            attr.setValue("This is an email subject");
            subscriber.getAttributes().add(attr);
            //
            attr = new Attribute();
            attr.setName("CompanyId"); 
            attr.setValue("FTD");
            subscriber.getAttributes().add(attr);
            
            // Add subscriber/owner/attributes to TriggerSend obj
            triggeredSend.getSubscribers().addAll(Arrays.asList(new Subscriber[] {subscriber}));
            triggeredSend.setCorrelationID("3113");  // ???
            
            // Send the TriggeredSend using Create call
            CreateOptions createOptions = new CreateOptions();
//            createOptions.setRequestType(RequestType.ASYNCHRONOUS); //???  
            createOptions.setRequestType(RequestType.SYNCHRONOUS);  //???  
            CreateRequest createRequest = new CreateRequest();
            createRequest.setOptions( createOptions );
            java.util.List<APIObject> listAPIObject = Arrays.asList( new APIObject[] {triggeredSend} );
            createRequest.getObjects().addAll( listAPIObject );
            CreateResponse createResponse = null;
            
            // Test send of email to ET
            //
            if (RETRIEVE_EVENT < 1) {
                createResponse = stub.create( createRequest );

                // Interpret response
                System.out.println( "[overall status message] " + createResponse.getOverallStatus() );
                System.out.println( "[request ID] " + createResponse.getRequestID());
                java.util.List<CreateResult> listCreateResult = createResponse.getResults();
                CreateResult[] createResult = listCreateResult.toArray( new CreateResult[listCreateResult.size()] );              
                for ( CreateResult dumpStepThrough : createResult ) {
                    System.out.println( "[create status message] " + dumpStepThrough.getStatusMessage() );
                    System.out.println( "[create status code] " + dumpStepThrough.getStatusCode() );
                    System.out.println( "[create getConversationID] " + dumpStepThrough.getConversationID());
                    System.out.println( "[create getNewObjectID] " + dumpStepThrough.getNewObjectID());
                    System.out.println( "[create getNewID] " + dumpStepThrough.getNewID());
                }
                
                // Validate the send and get failure information
                TriggeredSendCreateResult triggeredSendCreateResult = ( TriggeredSendCreateResult )createResult[0];
                assert( triggeredSendCreateResult.getSubscriberFailures() != null );
                System.out.println( "[size of failures array] " + triggeredSendCreateResult.getSubscriberFailures().size() );
                if (triggeredSendCreateResult.getSubscriberFailures().size() > 0) {
                    System.out.println( "[email address]     " + triggeredSendCreateResult.getSubscriberFailures().get( 0 ).getSubscriber().getEmailAddress() );
                    System.out.println( "[error description] " + triggeredSendCreateResult.getSubscriberFailures().get( 0 ).getErrorDescription() );
                    System.out.println( "[error code]        " + triggeredSendCreateResult.getSubscriberFailures().get( 0 ).getErrorCode() );
                }
                assert( createResult != null );
                assert( createResult[0].getStatusMessage().equals("OK") );

            // Test retrieval from ET
            //
            } else {
                RetrieveRequest rr  = new RetrieveRequest();
                RetrieveRequestMsg rrm = new RetrieveRequestMsg();
                
                // Retrieve BounceEvent
                if (RETRIEVE_EVENT == 1) {
                    rr.setObjectType("BounceEvent");
                    rr.getProperties().add("ObjectID");
                    rr.getProperties().add("BounceType");
                    rr.getProperties().add("BatchID");
                    rr.getProperties().add("SendID");
                    rr.getProperties().add("SMTPCode");
                    rr.getProperties().add("SMTPReason");
                    
                    // Setup filter for date
                    //
                    SimpleFilterPart dateFilter = new SimpleFilterPart();
                    dateFilter.setProperty("EventDate");
                    dateFilter.setSimpleOperator(SimpleOperators.GREATER_THAN);
                    GregorianCalendar gc = new GregorianCalendar(2014,5,4);  // Year,month,day
                    DatatypeFactory dtf = DatatypeFactory.newInstance();
                    dateFilter.getDateValue().add(dtf.newXMLGregorianCalendar(gc));
                    
                    // Setup filter to look for two different templates (triggeredSend ID's)
                    //
                    SimpleFilterPart tsFilter1 = new SimpleFilterPart();
                    tsFilter1.setProperty("TriggeredSendDefinitionObjectID");
                    tsFilter1.getValue().add("b942e9aa-9395-e311-8480-ac162dbc8b1c");
                    //
                    SimpleFilterPart tsFilter2 = new SimpleFilterPart();
                    tsFilter2.setProperty("TriggeredSendDefinitionObjectID");
                    tsFilter2.getValue().add("bbbbbbbb-9999-eeee-9999-aaaaaaaaaaaa");
                    //
                    ComplexFilterPart cfpNest = new ComplexFilterPart();
                    cfpNest.setLeftOperand(tsFilter1);
                    cfpNest.setRightOperand(tsFilter2);
                    cfpNest.setLogicalOperator(LogicalOperators.OR);
                    
                    // Combine filters
                    //
                    ComplexFilterPart cfpTop = new ComplexFilterPart();
                    cfpTop.setLeftOperand(dateFilter);
                    cfpTop.setRightOperand(cfpNest);
                    cfpTop.setLogicalOperator(LogicalOperators.AND);
                    rr.setFilter(cfpTop);
                
                // Retrieve TriggeredSendSummary
                } else if (RETRIEVE_EVENT == 2) {
                    rr.setObjectType("TriggeredSendSummary");
                    rr.getProperties().add("ObjectID");
                    rr.getProperties().add("CustomerKey");
                    
                // Retrieve ResultMessage    
                } else {
                    rr.setObjectType("ResultMessage");
                    rr.getProperties().add("CreatedDate");
                    rr.getProperties().add("RequestID");
                    rr.getProperties().add("ErrorCode");
                    rr.getProperties().add("StatusCode");
                }
                // rr.setRetrieveAllSinceLastBatch(true);  Un-comment this to only get data since last retrieve
                rrm.setRetrieveRequest(rr);
                
                RetrieveResponseMsg rsm = stub.retrieve(rrm);
                java.util.List<APIObject> listResp = rsm.getResults();
                Result[] respResult = listResp.toArray( new Result[listResp.size()] );              
                for ( Result dumpStepThrough : respResult ) {
                    
                    System.out.println( "[resp status message] " + dumpStepThrough.getStatusMessage() );
                    System.out.println( "[resp status code] " + dumpStepThrough.getStatusCode() );
                    System.out.println( "[requestID] " + dumpStepThrough.getRequestID());
                }       
            }
            System.out.println( "Exact Target email test complete" );
            
        } catch(Exception e) {
            System.out.println("GPS Exception: " + e);
            e.printStackTrace();            
        }
    }


}
