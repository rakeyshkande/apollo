package src.com.ftd.message_generator.exacttarget;

import java.io.IOException;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

import org.apache.ws.security.WSPasswordCallback;

public class ExactTargetTestPasswordHandler implements CallbackHandler {
    public void handle( Callback[] callbacks ) throws IOException, UnsupportedCallbackException {
        WSPasswordCallback wsPasswordCallback = (WSPasswordCallback) callbacks[0];
//        System.out.println("Client password for user: " + wsPasswordCallback.getIdentifer()); //"Identifier" misspelled in API
        wsPasswordCallback.setPassword("dummy");
    }
}
