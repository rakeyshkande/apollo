package com.ftd.accountingreporting.ejb;
import java.math.BigDecimal;
import java.util.StringTokenizer;

import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.naming.InitialContext;

import com.ftd.accountingreporting.bo.GiftCodeMaintenanceBO;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;

public class GiftCodeMaintenanceMDBBean implements MessageDrivenBean, MessageListener 
{
    private MessageDrivenContext context;
    
    private Logger logger = 
          new Logger("com.ftd.accountingreporting.ejb.GiftCodeMaintenanceMDBBean");
  
    public void ejbCreate()
    {
    }
   
        
    public void onMessage(Message msg)
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering processMessage in GiftCodeMaintenanceMDBBean");
            logger.debug("Message : " + msg.toString());
        }
        
            try
            {
            	TextMessage textMessage = (TextMessage) msg;
                String payLoad = null;
                String retryCount = null;
                int numOfRetries = 0;
                if(textMessage != null){
              	  payLoad = textMessage.getText();
              	}
                logger.info("Payload : "+ payLoad);
                
                GiftCodeMaintenanceBO giftCodeMaintenanceBO = new  GiftCodeMaintenanceBO();
            	           	 
            	StringTokenizer tokenizer = new StringTokenizer(payLoad, "|");
            	 
            	String action = tokenizer.nextToken();
            	String giftCodeId = tokenizer.nextToken();
         		String orderDetailId = tokenizer.nextToken();
         		String updatedBy = tokenizer.nextToken();
         		String orderAmount = tokenizer.nextToken();
         		String delayCount = tokenizer.nextToken();
         		if(tokenizer.hasMoreTokens()){
         			retryCount = tokenizer.nextToken();
         		}
         		         		 
         		logger.info("action: " + action);
         		logger.info("giftCodeId: " + giftCodeId);
         		logger.info("orderDetailId: " + orderDetailId);
         		logger.info("updatedBy: " + updatedBy);
         		logger.info("orderAmount: " + orderAmount);
         		logger.info("delayCount: " + delayCount);
         		if(retryCount != null){
         			logger.info("retryCount: " + retryCount);
         			numOfRetries = Integer.valueOf(retryCount);
         		}
         		
         		if(action != null){ 
         			if("REDEEM-GIFT-CODE".equalsIgnoreCase(action)){
	            		 logger.info("redeem gift code");
	            		 String maxRetryCount = null;
	         			 String retryInterval = null;
	         			 boolean gcRedeemed = false;
	         			 ConfigurationUtil cu = ConfigurationUtil.getInstance();
	            		 gcRedeemed = giftCodeMaintenanceBO.processRedemption(giftCodeId, orderDetailId, updatedBy, new BigDecimal(orderAmount), Integer.parseInt(delayCount));
	         			 if(cu != null && !gcRedeemed)
	        			 {
	        	            maxRetryCount = cu.getFrpGlobalParm("SERVICE", "GCS_TIMEOUT_MAX_RETRIES");
	        	            retryInterval = cu.getFrpGlobalParm("SERVICE", "GCS_TIMEOUT_DELAY");
	        	            logger.info("maxRetryCount: " + maxRetryCount);
	        	            logger.info("retryInterval: " + retryInterval);
	        	            if(!gcRedeemed) {
	        			        if(numOfRetries < Integer.valueOf(maxRetryCount)) {
	        			        	gcRedeemed = giftCodeMaintenanceBO.processRedemption(giftCodeId, orderDetailId, updatedBy, new BigDecimal(orderAmount), Integer.parseInt(delayCount));
	        			        	if(!gcRedeemed) {
	        			        		//increment the retry count and enqueue the message again for retry
	        			        		//'REDEEM-GIFT-CODE|' || p.gc_coupon_number || '|' || in_order_detail_id || '|SYS|' || p.order_total_amount || '|' || p.line_number;  
	        			        		String newPayLoad = payLoad.substring(0, payLoad.lastIndexOf("|"));
	        			        		numOfRetries++;
	        			      	        newPayLoad = newPayLoad + "|" + String.valueOf(numOfRetries);
	        			      	        logger.info("newPayLoad: " + newPayLoad);
	        			        		
	        			        		sendJMSMessageForGiftCodeMaintenance(newPayLoad, retryInterval);
	        			        	}
            					}
	        			        else{
	        			        	logger.info("max retries reached.  Sending system message");
	        			        	//reset the retry count to zero
	        			        	String newPayLoad = payLoad.substring(0, payLoad.lastIndexOf("|"));
	        			        	newPayLoad = newPayLoad + "|0";
	        			        	giftCodeMaintenanceBO.sendSystemMessage("Error occured while trying to redeem gift code id " + giftCodeId +
											".  Please investigate if there are issues with Gift Code Service.  If there are GCS connectivity issues, then once"
											+ " issues are resolved, go to Scheduler and put the following in the payload: " + newPayLoad);
									gcRedeemed = true;
	        			        }
	        			    }
	        			 }
         			}
         		}
            	 
            } catch(Exception e){
                logger.error(e);
        } finally {

            if(logger.isDebugEnabled()){
            logger.debug("Exiting onMessage of GiftCodeMaintenanceMDBBean" + 
                     " Message Driven Bean");
            }
        }
    }
    
  public void ejbRemove()
  {
  }

  public void setMessageDrivenContext(MessageDrivenContext ctx)
  {
    this.context = ctx;
  }
  
	private void sendJMSMessageForGiftCodeMaintenance(String payload, String retryInterval) throws Exception {
		logger.info("sendJMSMessageForGiftCodeMaintenance: " + payload);
		MessageToken messageToken = new MessageToken();
		messageToken.setStatus("GIFT_CODE_MAINT");
		messageToken.setJMSCorrelationID("REDEEM-GIFT-CODE");
		messageToken.setMessage(payload);
		messageToken.setProperty("JMS_OracleDelay", retryInterval,"int");
		Dispatcher dispatcher = Dispatcher.getInstance();
		dispatcher.dispatchTextMessage(new InitialContext(), messageToken);
	}
}

