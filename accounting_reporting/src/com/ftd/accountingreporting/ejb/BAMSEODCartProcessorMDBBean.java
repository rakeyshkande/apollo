package com.ftd.accountingreporting.ejb;
import com.ftd.accountingreporting.bo.EODShoppingCartBO;
import com.ftd.accountingreporting.cache.handler.EODPCardConfigHandler;
import com.ftd.accountingreporting.dao.EODDAO;
import com.ftd.accountingreporting.exception.NoCacheRegionException;
import com.ftd.accountingreporting.util.EODUtil;
import com.ftd.accountingreporting.util.SystemMessager;
import com.ftd.accountingreporting.vo.EODMessageVO;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.IOException;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import org.xml.sax.SAXException;

public class BAMSEODCartProcessorMDBBean implements MessageDrivenBean, MessageListener 
{
    private MessageDrivenContext context;
    private List paymentIds;
    private List refundIds;
    private Logger logger = 
          new Logger("com.ftd.accountingreporting.ejb.BAMSEODCartProcessorMDBBean");
  
    public void ejbCreate()
    {
    }

    private void processMessage(Message msg) throws Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering processMessage in BAMSEODCartProcessorMDBBean");
            logger.debug("Message : " + msg.toString());
        }
        EODPCardConfigHandler pcardConfig;
        Connection conn = EODUtil.createDatabaseConnection();
        EODDAO dao = new EODDAO(conn);
        EODMessageVO eodMessageVO = null;

        try
        {
            /* Extract text message from msg */
            MessageToken token = new MessageToken();
            TextMessage textMessage = (TextMessage) msg;
            
            // retrieve Master Order Number, Batch Number, Batch Date, payment ids, refund ids from message.
            eodMessageVO = this.extractMsg(textMessage);
            // Process shopping cart. Update bill/refund status. Create billing detail.
        
            pcardConfig = (EODPCardConfigHandler)CacheManager.getInstance().getHandler("PARTNER_PCARD");
            Map pcardConfigMap = pcardConfig.getPcardConfigMap();
            if(pcardConfigMap == null) {
                throw new NoCacheRegionException("Cannot load cache handler map! Rolling back event...");
            }

            EODShoppingCartBO cartbo = new EODShoppingCartBO(dao, eodMessageVO.getBatchTime(), eodMessageVO.getBatchNumber(), pcardConfig);
            
            cartbo.setMasterOrderNumber(eodMessageVO.getMasterOrderNumber());
            cartbo.process(paymentIds, refundIds);
            dao.doDeleteRecoveryCart(eodMessageVO.getMasterOrderNumber());

        } catch(Exception e) {
            logger.error(e);
            // delete shopping cart from eod_payment_recovery
            dao.doDeleteRecoveryCart(eodMessageVO.getMasterOrderNumber());    
            
            // send system message (email only)
            SystemMessager.sendSystemMessage(conn, e);
        } finally {
            if(conn != null && !conn.isClosed()) {
                conn.close();
            }
        }        
    }
    
  /**
   * Extract information from the messages.
   * @param msg
   * @return 
   * @throws java.lang.Exception
   */
    private EODMessageVO extractMsg(TextMessage msg) throws Exception{
        String textMsg = msg.getText();
        EODMessageVO eodMessageVO = new EODMessageVO();
        logger.debug(textMsg);
        
        paymentIds = new ArrayList();
        refundIds = new ArrayList();

        Document cartMsgDoc = DOMUtil.getDocument(textMsg);
        eodMessageVO.setMasterOrderNumber((((NodeList)cartMsgDoc.getElementsByTagName("MON")).item(0).getFirstChild()).getNodeValue());
        eodMessageVO.setBatchTime((Long.valueOf((((NodeList)cartMsgDoc.getElementsByTagName("BATCH_TIME")).item(0).getFirstChild()).getNodeValue())).longValue()); 
        eodMessageVO.setBatchNumber((Long.valueOf((((NodeList)cartMsgDoc.getElementsByTagName("BATCH_NUMBER")).item(0).getFirstChild()).getNodeValue())).longValue());

        NodeList paymentNL = (NodeList)cartMsgDoc.getElementsByTagName("PID");
        NodeList refundNL = (NodeList)cartMsgDoc.getElementsByTagName("RID");
        
        if(paymentNL != null) {
            for(int i = 0; i < paymentNL.getLength(); i++) {
                paymentIds.add((paymentNL.item(i).getFirstChild()).getNodeValue());
            }
        }
        if(refundNL != null) {
            for(int i = 0; i < refundNL.getLength(); i++) {
                refundIds.add((refundNL.item(i).getFirstChild()).getNodeValue());
            }
        }        
        return eodMessageVO;
    }
    
    public void onMessage(Message msg)
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering processMessage in BAMSEODCartProcessorMDBBean");
            logger.debug("Message : " + msg.toString());
        }
        
            try
            {
                processMessage(msg);

            } catch(Exception e){
                logger.error(e);
        } finally {

            if(logger.isDebugEnabled()){
            logger.debug("Exiting onMessage of BAMSEODCartProcessor" + 
                     " Message Driven Bean");
            }
        }
    }
    
  public void ejbRemove()
  {
  }

  public void setMessageDrivenContext(MessageDrivenContext ctx)
  {
    this.context = ctx;
  }
}

