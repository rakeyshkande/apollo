package com.ftd.accountingreporting.ejb;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJBException;
import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import com.ftd.accountingreporting.dao.EODDAO;
import com.ftd.accountingreporting.pg.bo.PGEODShoppingCartBO;
import com.ftd.accountingreporting.util.EODUtil;
import com.ftd.accountingreporting.util.SystemMessager;
import com.ftd.accountingreporting.vo.PGEODMessageVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

public class PGEODCartProcessorMDB implements MessageDrivenBean, MessageListener {

	private MessageDrivenContext context;
	private static Logger logger = new Logger("com.ftd.accountingreporting.eventhandler.PGEODCartProcessorMDB");
	public void ejbCreate() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.jms.MessageListener#onMessage(javax.jms.Message)
	 */
	public void onMessage(Message message) {

		if (logger.isDebugEnabled()) {
			logger.debug("Entering onMessage in PGEODCartProcessorMDB");
			logger.debug("Message : " + message.toString());
		}

		try {
			processMessage(message);

		} catch (Throwable e) {
			logger.error(e);
		} finally {

			if (logger.isDebugEnabled()) {
				logger.debug("Exiting onMessage of PGEODCartProcessorMDB" + " Message Driven Bean");
			}
		}
	}

	private void processMessage(Message message) throws Throwable {

		Connection conn = EODUtil.createDatabaseConnection();

		EODDAO dao = new EODDAO(conn);
		PGEODMessageVO eodMessageVO = null;
		long billingId =0;
		
		try {
			/* Extract text message from msg */
			TextMessage textMessage = (TextMessage) message;

			// retrieve Master Order Number, Batch Number, Batch Date, payment
			// ids, refund ids from message.
			eodMessageVO = this.extractMsg(textMessage);
			PGEODShoppingCartBO pgEODcartbo = new PGEODShoppingCartBO(dao);
			billingId = eodMessageVO.getBillingDetailId();
			if (billingId > 0) {
				pgEODcartbo.reProcessPG(eodMessageVO);
			} else {
				pgEODcartbo.processPG(eodMessageVO);
			}	
			
		} catch (Exception e) {
			logger.error(e);
			// send system message (email only)
			/*SystemMessager.sendSystemMessage(conn, e);*/
		} finally {
			if (conn != null && !conn.isClosed()) {
				conn.close();
			}
		}

	}

	public void ejbRemove() throws EJBException {

	}

	@Override
	public void setMessageDrivenContext(MessageDrivenContext ctx) throws EJBException {
		this.context = ctx;

	}

	/**
	 * Extract information from the messages.
	 * 
	 * @param msg
	 * @return
	 * @throws java.lang.Exception
	 */
	private PGEODMessageVO extractMsg(TextMessage msg) throws Exception {
		String textMsg = msg.getText();
		PGEODMessageVO eodMessageVO = new PGEODMessageVO();
		logger.debug(textMsg);

		List paymentIds = new ArrayList();
		List refundIds = new ArrayList();

		Document cartMsgDoc = DOMUtil.getDocument(textMsg);
		eodMessageVO.setMasterOrderNumber((((NodeList) cartMsgDoc.getElementsByTagName("MON")).item(0).getFirstChild())
				.getNodeValue());
		eodMessageVO.setBatchTime((Long.valueOf((((NodeList) cartMsgDoc.getElementsByTagName("BATCH_TIME")).item(0)
				.getFirstChild()).getNodeValue())).longValue());
		eodMessageVO.setBatchNumber((Long.valueOf((((NodeList) cartMsgDoc.getElementsByTagName("BATCH_NUMBER")).item(0)
				.getFirstChild()).getNodeValue())).longValue());

		NodeList paymentNL = (NodeList) cartMsgDoc.getElementsByTagName("PID");
		NodeList refundNL = (NodeList) cartMsgDoc.getElementsByTagName("RID");

		eodMessageVO.setBillingDetailId((Long.valueOf((((NodeList) cartMsgDoc.getElementsByTagName("DETAIL_ID"))
				.item(0).getFirstChild()).getNodeValue())).longValue());
		
		if (paymentNL != null) {
			for (int i = 0; i < paymentNL.getLength(); i++) {
				paymentIds.add((paymentNL.item(i).getFirstChild()).getNodeValue());
			}
			eodMessageVO.setPaymentIdList(paymentIds);
		}
		
		if (refundNL != null) {
			for (int i = 0; i < refundNL.getLength(); i++) {
				refundIds.add((refundNL.item(i).getFirstChild()).getNodeValue());
			}
			eodMessageVO.setRefundIdList(refundIds);
		}
		
		eodMessageVO.setRetryCounter((Long.valueOf((((NodeList) cartMsgDoc.getElementsByTagName("RETRY_COUNTER"))
				.item(0).getFirstChild()).getNodeValue())).longValue());
		
		return eodMessageVO;
	}

}
