package com.ftd.accountingreporting.ejb;
import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import com.ftd.accountingreporting.pts.bo.BAMSACKFileProcessorBO;
import com.ftd.osp.utilities.plugins.Logger;

public class BAMSACKMDBBean implements MessageDrivenBean, MessageListener 
{
    private MessageDrivenContext context;
    
    private Logger logger = 
          new Logger("com.ftd.accountingreporting.ejb.BAMSACKMDBBean");
  
    public void ejbCreate()
    {
    }
   
        
    public void onMessage(Message msg)
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering processMessage in BAMSACKMDBBean");
            logger.debug("Message : " + msg.toString());
        }
        
            try
            {
            	TextMessage textMessage = (TextMessage) msg;
                String payLoad = null;
                if(textMessage != null){
              	  payLoad = textMessage.getText();
                }
            	
            	 BAMSACKFileProcessorBO bamsACKBO = new BAMSACKFileProcessorBO();
            	 logger.debug("Pay Load : "+ payLoad);
            	 if("BAMS-ACK".equalsIgnoreCase(payLoad)){
            		 bamsACKBO.processAckFile();
            	 }else if("DAILY-BAMS-ACK-CHECK".equalsIgnoreCase(payLoad)){
            		 logger.debug("Check BAMS ACK file status");
            		 bamsACKBO.checkAckFileStatus();
            	 }
            	 
            } catch(Exception e){
                logger.error(e);
        } finally {

            if(logger.isDebugEnabled()){
            logger.debug("Exiting onMessage of BAMSACK" + 
                     " Message Driven Bean");
            }
        }
    }
    
  public void ejbRemove()
  {
  }

  public void setMessageDrivenContext(MessageDrivenContext ctx)
  {
    this.context = ctx;
  }
}

