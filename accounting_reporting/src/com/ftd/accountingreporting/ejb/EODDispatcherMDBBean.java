package com.ftd.accountingreporting.ejb;

import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.constant.AltPayConstants;
import com.ftd.accountingreporting.dao.EODDAO;
import com.ftd.accountingreporting.timer.PayPalConnectionTimerData;
import com.ftd.accountingreporting.timer.TimerServiceSessionEJBLocal;
import com.ftd.accountingreporting.timer.TimerServiceSessionEJBLocalHome;
import com.ftd.accountingreporting.util.AccountingUtil;
import com.ftd.accountingreporting.util.EODUtil;
import com.ftd.accountingreporting.util.SystemMessager;
import com.ftd.accountingreporting.vo.BillingHeaderVO;
import com.ftd.accountingreporting.vo.EODMessageVO;
import com.ftd.accountingreporting.vo.EODPaymentVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;

import javax.jms.Message;
import javax.jms.MessageListener;

import javax.naming.Context;
import javax.naming.InitialContext;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;


public class EODDispatcherMDBBean implements MessageDrivenBean, MessageListener 
{
  private MessageDrivenContext context;
  private static Logger logger  = 
        new Logger("com.ftd.accountingreporting.eventhandler.EODDispatcherMDBBean");
  private Connection conn = null;
  

  public void ejbCreate()
  {
  }

   /**
    * This method will:
    * 1.	Obtain the process lock
    * 2.	Update removed order payment
    * 3.	Retrieve batch number
    *     3.1 if this is fresh run, persist the payment ids in recovery table and create new batch number
    *     3.2 if this is a recovery run, retrieve the batch number of the previous run.
    *     3.3 if there is already a sucessful run for the day, exit program.
    * 4.  In the case of 3.1 and 3.2, retrieve lists of payment ids for charges and refunds for the day.
    * 5.  For each shopping cart, dispatch a message to the cart processor.
    * 6.  Dispatch a message to the eod finalizer to compile the file and complete ending tasks.
    * 
    **/ 
  public void onMessage(Message msg)
  {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering onMessage");
        }
        EODDAO eodDAO = null;
        long eodStartTime = 0;
        long eodBatchTime = 0;
        Calendar batchDate = null;
        EODMessageVO eodMessageVO = new EODMessageVO();
        ConfigurationUtil configUtil = null;

        try {
            configUtil = ConfigurationUtil.getInstance();
            eodStartTime = Calendar.getInstance().getTimeInMillis();
            eodBatchTime = this.getEODBatchTime(eodStartTime);
            batchDate = EODUtil.convertLongDate(eodBatchTime);
            eodMessageVO.setStartTime(eodStartTime);
            eodMessageVO.setBatchTime(eodBatchTime);

            // retrieve connection to database 
            conn = DataSourceUtil.getInstance().
                getConnection(configUtil.getProperty(ARConstants.CONFIG_FILE,ARConstants.DATASOURCE_NAME));
            eodDAO = new EODDAO(conn);
            
            try {
                // Create billing batch and get the batch number.
                long batchNumber = this.getBatchNumber(eodDAO, batchDate);
                eodMessageVO.setBatchNumber(batchNumber);
                
                // send message to finalizer
                this.notifyFinalizer(eodMessageVO);
                
                // send message to processor
                this.dispatchCarts(eodDAO, eodMessageVO);   
            } catch (Exception e) {
                    logger.error(e);
                    throw e;
            }
            
        } catch (SQLException e) {
            logger.error(e);
            /* Send system message */
            SystemMessager.sendSystemMessage(conn, e);
        } catch (Exception e) {
            logger.error(e);
            /* Send system message */
            SystemMessager.sendSystemMessage(conn, e);
        } finally {
            try {
                if(conn != null && !conn.isClosed()) {
                    conn.close();
                }
            } catch(Exception e) {
                try {
                     SystemMessager.sendSystemMessage(conn, e);
                } catch(Exception ee) {
                    logger.error(ee);
                }
            }
            if(logger.isDebugEnabled()){
                logger.debug("Exiting invoke");
            } 
        }  
  }

  public void ejbRemove()
  {
  }

  public void setMessageDrivenContext(MessageDrivenContext ctx)
  {
    this.context = ctx;
  }
    
  /**
   * Retrieve the date time for which the batch is run. This is calculated from current date
   * and a configurable offset value.
   * 
   * @param eodStartTime
   * @return 
   * @throws java.lang.Exception
   */
    private long getEODBatchTime(long eodStartTime) throws Exception {
       long eodBatchTime = 0;
       String eodBatchDateOffset = ConfigurationUtil.getInstance().getProperty(ARConstants.CONFIG_FILE, ARConstants.CONST_EOD_BATCH_DATE_OFFSET);
       Calendar batchDate = Calendar.getInstance();
       batchDate.setTimeInMillis(eodStartTime);
       batchDate.add(Calendar.DATE, new Integer(eodBatchDateOffset).intValue());
     
       eodBatchTime = batchDate.getTime().getTime();
       logger.debug("eodBatchTime is " + eodBatchTime);
       return eodBatchTime;
   }  
   
   
  /**
   * Gets and returns the batchNumber. 
   * @throws Exception
   */
   public long getBatchNumber(EODDAO dao, Calendar batchDate) throws Exception
   {
      // Get processed indicator
      BillingHeaderVO bhvo = dao.doGetProcessedIndicator(batchDate, null);
      String processIndicator = bhvo.getProcessedIndicator();
      long batchNumber;
      
      logger.debug("processedIndicator is: " + processIndicator);
      // If null start new EOD process
      if (null == processIndicator) {
          batchNumber = dao.doCreateHeadPersRecovRec(batchDate, null);
      } else if ("Y".equalsIgnoreCase(processIndicator)) {
          throw new Exception("EOD header already processed for " + batchDate);
      } else {
          batchNumber =  bhvo.getBatchNumber().longValue();
      }
      logger.debug("eodBatchNumber is " + batchNumber);
      return batchNumber;
   }
   
   /**
   * Retrieves carts that need to be processed by EOD and dispatch messages
   * to processorMDB.
   * @param eodBatchTime
   * @throws java.lang.Exception
   */
   private void dispatchCarts(EODDAO eodDAO, EODMessageVO eodMessageVO) throws Exception
   {
        Map cartMap = eodDAO.doGetRecoveryCarts(null);
        ArrayList paymentList = (ArrayList)cartMap.get("PAYMENT");
        ArrayList refundList = (ArrayList)cartMap.get("REFUND");
        String cartMessage = null;
        //int transCount = 0;
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        int blockSize = Integer.parseInt(configUtil.getProperty(ARConstants.CONFIG_FILE, "eodCartDispatchBlockSize"));
        int curBlockSize = 0;
        long timeToLive = Long.parseLong(configUtil.getProperty(ARConstants.CONFIG_FILE, "eodCartMessageExpire"));
        List messageList = new ArrayList();
        String masterOrderNumber = null;
        Document cartMsgDoc = null;
        
        try {       
            do {
                // Compose a cart message
                cartMessage = this.composeMessage(paymentList, refundList, eodMessageVO);
                               
                if(cartMessage != null) {
                    cartMsgDoc = DOMUtil.getDocument(cartMessage);
                    masterOrderNumber = (((NodeList)cartMsgDoc.getElementsByTagName("MON")).item(0).getFirstChild()).getNodeValue();
                    messageList.add(cartMessage);
                    eodDAO.doUpdateCartDispatched(masterOrderNumber);
                    // dispatch a message to the processsor MDB
                    curBlockSize++;
                    
                    if(curBlockSize == blockSize) {
                        AccountingUtil.dispatchJMSMessageList("EOD_CART", messageList, timeToLive, 0);
                        curBlockSize = 0;
                        messageList = new ArrayList();
                        //uts.commit();
                        //uts.begin();
                    }
                }
                
            } while(cartMessage != null);
            
            if(messageList.size() > 0) {
                AccountingUtil.dispatchJMSMessageList("EOD_CART", messageList, timeToLive, 0);
            }
            //uts.commit();
        } catch (Exception e) {
            logger.error(e);
            throw e;
        }
   }
   
   /**
   * Dispatches a message to the finalizer.
   * @param message
   * @throws javax.naming.NamingException
   * @throws javax.jms.JMSException
   * @throws java.lang.Exception
   */
   private void notifyFinalizer(EODMessageVO eodMessageVO) throws Exception {
      String xmlDocument = "<ROOT><BATCH_TIME>" + eodMessageVO.getBatchTime() + "</BATCH_TIME>" +
                           "<START_TIME>" + eodMessageVO.getStartTime() + "</START_TIME>" +
                           "<BATCH_NUMBER>" + eodMessageVO.getBatchNumber() + "</BATCH_NUMBER></ROOT>";
      Document payload =  DOMUtil.getDocument(xmlDocument);
      logger.debug("Sending message to finalizer...");
      logger.debug(xmlDocument);
      AccountingUtil.queueToEventsQueue(ARConstants.EVENT_CONTEXT, "EOD-FINALIZER", "0", payload);
   }
   
 
  /**
   * Construct a message to be sent to the cart processor.
   * @param paymentList
   * @param refundList
   * @return 
   * @throws java.lang.Exception
   */
    private String composeMessage(List paymentList, List refundList, EODMessageVO eodMessageVO) throws Exception
    {
        String message = null;
        String masterOrderNumber = null;
        List pids = null;
        List rids = null;
        if(paymentList != null && paymentList.size() > 0) {
            pids = this.getNextCartPaymentIds(paymentList);
            masterOrderNumber = ((EODPaymentVO)pids.get(0)).getMasterOrderNumber();
            rids = this.findIdsByCart(refundList, masterOrderNumber);
        } else if (refundList != null && refundList.size() > 0) {
            rids = this.getNextCartPaymentIds(refundList);
            masterOrderNumber = ((EODPaymentVO)rids.get(0)).getMasterOrderNumber();
        }
        
        if((pids != null && pids.size() > 0 ) || 
           (rids != null && rids.size() > 0 ) ) {
               message = "<ROOT><MON>" + masterOrderNumber + "</MON>" +
                               "<BATCH_TIME>" + eodMessageVO.getBatchTime() + "</BATCH_TIME>" +
                               "<BATCH_NUMBER>" + eodMessageVO.getBatchNumber() + "</BATCH_NUMBER>" +
                               "<PIDS>" + getIdAsString(pids, "<PID>", "</PID>") + "</PIDS>" +
                               "<RIDS>" + getIdAsString(rids, "<RID>", "</RID>") + "</RIDS>" +
                         "</ROOT>";
        }
        return message;
    }
    
  /**
   * Returns a string of payment ids for the given list of EODPaymentVO with tags around the id.
   * @param idList
   * @param sTag
   * @param eTag
   * @return 
   * @throws java.lang.Exception
   */
    private String getIdAsString(List idList, String sTag, String eTag) throws Exception
    {
        String ids = "";
        EODPaymentVO eodPaymentVO = null;
        if(idList != null && idList.size() > 0) {
            for(int i=0; i<idList.size(); i++) {
                eodPaymentVO = (EODPaymentVO)idList.get(i);
                ids += (sTag + eodPaymentVO.getPaymentId() + eTag); 
            }
        }
        return ids;
    }
    
    
  /**
   * Retrieves the next available cart payment list. 
   * 1. Put all payment ids for the next eligible shopping cart and return it.
   * 2. Remove the elements that have been processed from the payment list 
   *    so that the next process can start from index 0.
   * @param paymentList
   * @return 
   * @throws java.lang.Exception
   */
  private List getNextCartPaymentIds(List paymentList) throws Exception 
  {
      
      ArrayList cartPaymentList = null;
      if(paymentList != null && paymentList.size() > 0) {
          cartPaymentList = new ArrayList();
          EODPaymentVO paymentvo = (EODPaymentVO)paymentList.get(0);
          String masterOrderNumber = paymentvo.getMasterOrderNumber();
          String nextMasterOrderNumber = null;
          
          for(int i = 0; i < paymentList.size(); i++) {
              paymentvo = (EODPaymentVO)paymentList.get(i);
              if(masterOrderNumber.equals(paymentvo.getMasterOrderNumber()) ) {
                  cartPaymentList.add(paymentList.remove(i));  
                  i--;
              } else {
                  break;
              }              
          }         
      }
      return cartPaymentList;
  }
    
  /**
   * Returns a list of ids for the given master order number
   * @param refundList
   * @param masterOrderNumber
   * @return 
   * @throws java.lang.Exception
   */
    private List findIdsByCart(List refundList, String masterOrderNumber) throws Exception
    {
        ArrayList idList = null;
        EODPaymentVO paymentvo = null;
        if(refundList != null && refundList.size() > 0) {
          idList = new ArrayList();
          for(int i = 0; i < refundList.size(); i++) {
              paymentvo = (EODPaymentVO)refundList.get(i);
              if(masterOrderNumber.compareTo(paymentvo.getMasterOrderNumber()) < 0 ) {
                  break;
              } else if(masterOrderNumber.equals(paymentvo.getMasterOrderNumber())) {
                  idList.add(refundList.remove(i));
                  i--;
              } //else {
                  // continue;
                //}              
          } 
        }
        return idList;   
    }      
    
}