package com.ftd.accountingreporting.ejb;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;
import javax.jms.Message;
import javax.jms.MessageListener;

import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.dao.EODDAO;
import com.ftd.accountingreporting.util.AccountingUtil;
import com.ftd.accountingreporting.util.EODUtil;
import com.ftd.accountingreporting.util.SystemMessager;
import com.ftd.accountingreporting.vo.BillingHeaderPGVO;
import com.ftd.accountingreporting.vo.EODPaymentVO;
import com.ftd.accountingreporting.vo.PGEODMessageVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

public class PGEODDispatcherMDB implements MessageDrivenBean, MessageListener {

	private MessageDrivenContext context;
	private static Logger logger = new Logger("com.ftd.accountingreporting.ejb.PGEODDispatcherMDB");
	
	
	
	public void ejbCreate() {

	}

	/* (non-Javadoc)
	 * @see javax.jms.MessageListener#onMessage(javax.jms.Message)
	 */
	public void onMessage(Message message) {

		if (logger.isDebugEnabled()) {
			logger.debug("Entering onMessage in PGEODDispatcherMDB");
		}
		
		Connection conn = null;
		EODDAO eodDAO = null;
		long eodStartTime = 0;
		long eodBatchTime = 0;
		Calendar batchDate = null;
		PGEODMessageVO eodMessageVO = new PGEODMessageVO();
		
		try {
			
			eodStartTime = Calendar.getInstance().getTimeInMillis();
			eodBatchTime = this.getEODBatchTime(eodStartTime);
			batchDate = EODUtil.convertLongDate(eodBatchTime);
			eodMessageVO.setStartTime(eodStartTime);
			eodMessageVO.setBatchTime(eodBatchTime);
			
			// retrieve connection to database
			conn = EODUtil.createDatabaseConnection();;
			eodDAO = new EODDAO(conn);

				try {

					// Create billing batch and get the batch number and
					long batchNumber = this.getCCBatchNumber(eodDAO, batchDate, ARConstants.CREDIT_CARD);
					logger.info("PG EOD Credit Card Job batchNumber: " + batchNumber);
					eodMessageVO.setBatchNumber(batchNumber);
					this.dispatchPGCarts(eodMessageVO,batchDate, ARConstants.CREDIT_CARD,eodDAO);
					
				} catch (Exception e) {
					logger.error(e);
					throw e;
				}
	
		} catch (Exception e) {
			logger.error(e);
			SystemMessager.sendSystemMessage(conn, e);
		} finally {
			try {
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (Exception e) {
				try {
					SystemMessager.sendSystemMessage(conn, e);
				} catch (Exception ee) {
					logger.error(ee);
				}
			}
			if (logger.isDebugEnabled()) {
				logger.debug("Exit onMessage(Message message)");
			}
		}

	}

	public void ejbRemove() {
	}

	public void setMessageDrivenContext(MessageDrivenContext ctx) {
		this.context = ctx;
	}

	/**
	 * Retrieve the date time for which the batch is run. This is calculated
	 * from current date and a configurable offset value.
	 * 
	 * @param eodStartTime
	 * @return
	 * @throws java.lang.Exception
	 */
	private long getEODBatchTime(long eodStartTime) throws Exception {
		long eodBatchTime = 0;
		ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
		String eodBatchDateOffset = configUtil.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT,
				ARConstants.PG_EOD_BATCH_OFFSET_DATE);
		Calendar batchDate = Calendar.getInstance();
		batchDate.setTimeInMillis(eodStartTime);
		batchDate.add(Calendar.DATE, new Integer(eodBatchDateOffset).intValue());

		eodBatchTime = batchDate.getTime().getTime();
		logger.debug("PG eodBatchTime is " + eodBatchTime);
		return eodBatchTime;
	}

	/**
	 * Gets and returns the batchNumber.
	 * @param dao
	 * @param batchDate
	 * @param paymentType
	 * @return
	 * @throws Exception
	 */
	public long getCCBatchNumber(EODDAO dao, Calendar batchDate, String paymentType) throws Exception {

		// Get processed indicator
		BillingHeaderPGVO bhpgvo = dao.doGetPGProcessedIndicator(batchDate, paymentType);
		String processIndicator = bhpgvo.getProcessedIndicator();
		long batchNumber;

		// If null start new EOD process
		if (null == processIndicator) {
			batchNumber = dao.doCreatePGHeadPersRecovRec(batchDate, paymentType);
		} else if ("Y".equalsIgnoreCase(processIndicator)) {
			throw new Exception("PG EOD header already processed for " + batchDate);
		} else {
			batchNumber = bhpgvo.getBatchNumber().longValue();
		}
		return batchNumber;
	}

	
	
	/**
	 * Retrieves carts that need to be processed by PG EOD and dispatch messages
	 * to processorMDB.
	 * 
	 * @param eodMessageVO
	 * @param batchDate
	 * @param paymentType
	 * @param eodDAO
	 * @throws Exception
	 */
	private void dispatchPGCarts(PGEODMessageVO eodMessageVO, Calendar batchDate, String paymentType, EODDAO eodDAO)
			throws Exception {

		Map<String, List<EODPaymentVO>> cartMap = null;
		List<EODPaymentVO> paymentList = null;
		List<EODPaymentVO> refundList = null;
		String cartMessage = null;
		ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
		//int blockSize = Integer.parseInt(configUtil.getProperty(ARConstants.CONFIG_FILE, "eodCartDispatchBlockSize"));
		int dispatchCount = 0;
		//int curBlockSize = 0;
		long timeToLive = Long.parseLong(configUtil.getProperty(ARConstants.CONFIG_FILE, "eodCartMessageExpire"));
		List<String> messageList = new ArrayList<String>();

		try {

			// Retrieving the Payment and Refunds
			cartMap = eodDAO.doGetPGCarts(paymentType, batchDate);

			paymentList = (ArrayList<EODPaymentVO>) cartMap.get("PAYMENT");

			refundList = (ArrayList<EODPaymentVO>) cartMap.get("REFUND");

			do {
				// Compose a cart message
				cartMessage = this.composeMessage(paymentList, refundList, eodMessageVO);
				logger.debug("dispatching message=" + cartMessage);
				if (cartMessage != null) {
					messageList.add(cartMessage);
					//curBlockSize++;
					dispatchCount++;
					//if (curBlockSize == blockSize) {
					AccountingUtil.dispatchJMSMessageList("PG_EOD_CART", messageList, timeToLive, 0);
					//curBlockSize = 0;
					messageList = new ArrayList<String>();

					//}
				}
			} while (cartMessage != null);

			/*if (messageList.size() > 0) {
				AccountingUtil.dispatchJMSMessageList("PG_EOD_CART", messageList, timeToLive, 0);
			}*/

			eodDAO.doUpdateDispatchedCount(eodMessageVO.getBatchNumber(), dispatchCount);

		} catch (Exception e) {
			logger.error(e);
			throw e;
		}
	}

	/**
	 * Construct a message to be sent to the PG EOD cart processor.
	 * 
	 * @param paymentList
	 * @param refundList
	 * @return
	 * @throws java.lang.Exception
	 */
	private String composeMessage(List paymentList, List refundList, PGEODMessageVO eodMessageVO) throws Exception {
		String message = null;
		String masterOrderNumber = null;
		List pids = null;
		List rids = null;
		if (paymentList != null && paymentList.size() > 0) {
			pids = this.getNextCartPaymentIds(paymentList);
			masterOrderNumber = ((EODPaymentVO) pids.get(0)).getMasterOrderNumber();
			rids = this.findIdsByCart(refundList, masterOrderNumber);
		} else if (refundList != null && refundList.size() > 0) {
			rids = this.getNextCartPaymentIds(refundList);
			masterOrderNumber = ((EODPaymentVO) rids.get(0)).getMasterOrderNumber();
		}

		if ((pids != null && pids.size() > 0) || (rids != null && rids.size() > 0)) {
			message = "<ROOT><MON>" + masterOrderNumber + "</MON>" + "<BATCH_TIME>" + eodMessageVO.getBatchTime()
					+ "</BATCH_TIME>" + "<BATCH_NUMBER>" + eodMessageVO.getBatchNumber() + "</BATCH_NUMBER>" + "<PIDS>"
					+ getIdAsString(pids, "<PID>", "</PID>") + "</PIDS>" + "<RIDS>"
					+ getIdAsString(rids, "<RID>", "</RID>") + "</RIDS>"+"<DETAIL_ID>" + eodMessageVO.getBillingDetailId() + "</DETAIL_ID>"
					+ "<RETRY_COUNTER>" + eodMessageVO.getRetryCounter()  + "</RETRY_COUNTER>" 
				    + "</ROOT>";
		}
		return message;
	}

	/**
	 * Returns a string of payment ids for the given list of EODPaymentVO with
	 * tags around the id.
	 * 
	 * @param idList
	 * @param sTag
	 * @param eTag
	 * @return
	 * @throws java.lang.Exception
	 */
	private String getIdAsString(List idList, String sTag, String eTag) throws Exception {
		String ids = "";
		EODPaymentVO eodPaymentVO = null;
		if (idList != null && idList.size() > 0) {
			for (int i = 0; i < idList.size(); i++) {
				eodPaymentVO = (EODPaymentVO) idList.get(i);
				ids += (sTag + eodPaymentVO.getPaymentId() + eTag);
			}
		}
		return ids;
	}

	/**
	 * Retrieves the next available cart payment list. 1. Put all payment ids
	 * for the next eligible shopping cart and return it. 2. Remove the elements
	 * that have been processed from the payment list so that the next process
	 * can start from index 0.
	 * 
	 * @param paymentList
	 * @return
	 * @throws java.lang.Exception
	 */
	private List getNextCartPaymentIds(List paymentList) throws Exception {

		ArrayList cartPaymentList = null;
		if (paymentList != null && paymentList.size() > 0) {
			cartPaymentList = new ArrayList();
			EODPaymentVO paymentvo = (EODPaymentVO) paymentList.get(0);
			String masterOrderNumber = paymentvo.getMasterOrderNumber();
			String nextMasterOrderNumber = null;

			for (int i = 0; i < paymentList.size(); i++) {
				paymentvo = (EODPaymentVO) paymentList.get(i);
				if (masterOrderNumber.equals(paymentvo.getMasterOrderNumber())) {
					cartPaymentList.add(paymentList.remove(i));
					i--;
				} else {
					break;
				}
			}
		}
		return cartPaymentList;
	}

	/**
	 * Returns a list of ids for the given master order number
	 * 
	 * @param refundList
	 * @param masterOrderNumber
	 * @return
	 * @throws java.lang.Exception
	 */
	private List findIdsByCart(List refundList, String masterOrderNumber) throws Exception {
		ArrayList idList = null;
		EODPaymentVO paymentvo = null;
		if (refundList != null && refundList.size() > 0) {
			idList = new ArrayList();
			for (int i = 0; i < refundList.size(); i++) {
				paymentvo = (EODPaymentVO) refundList.get(i);
				if (masterOrderNumber.compareTo(paymentvo.getMasterOrderNumber()) < 0) {
					break;
				} else if (masterOrderNumber.equals(paymentvo.getMasterOrderNumber())) {
					idList.add(refundList.remove(i));
					i--;
				} 
			}
		}
		return idList;
	}
}
