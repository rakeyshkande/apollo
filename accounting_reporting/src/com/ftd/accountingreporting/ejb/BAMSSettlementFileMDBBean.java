package com.ftd.accountingreporting.ejb;
import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import com.ftd.accountingreporting.pts.bo.BAMSSettlementFileProcessorBO;
import com.ftd.osp.utilities.plugins.Logger;

public class BAMSSettlementFileMDBBean implements MessageDrivenBean, MessageListener 
{
    private MessageDrivenContext context;
    
    private Logger logger = 
          new Logger("com.ftd.accountingreporting.ejb.BAMSSettlementFileMDBBean");
  
    public void ejbCreate()
    {
    }
   
        
    public void onMessage(Message msg)
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering processMessage in BAMSSettlementFileMDBBean");
            logger.debug("Message : " + msg.toString());
        }
        
            try
            {
            	TextMessage textMessage = (TextMessage) msg;
                String payLoad = null;
                if(textMessage != null){
              	  payLoad = textMessage.getText();
                }
            	
                BAMSSettlementFileProcessorBO bamsSettlementBO = new BAMSSettlementFileProcessorBO();
            	logger.debug("Pay Load : "+ payLoad);
            	bamsSettlementBO.processSettlementFile(payLoad);
                        	 
            } catch(Exception e){
                logger.error(e);
        } finally {

            if(logger.isDebugEnabled()){
            logger.debug("Exiting onMessage of BAMSSettlementFile" + 
                     " Message Driven Bean");
            }
        }
    }
    
  public void ejbRemove()
  {
  }

  public void setMessageDrivenContext(MessageDrivenContext ctx)
  {
    this.context = ctx;
  }
}

