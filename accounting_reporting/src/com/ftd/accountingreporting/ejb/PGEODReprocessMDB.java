package com.ftd.accountingreporting.ejb;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import com.ftd.accountingreporting.altpay.dao.PGAltPayDAO;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.constant.AltPayConstants;
import com.ftd.accountingreporting.dao.EODDAO;
import com.ftd.accountingreporting.pg.bo.PGEODReprocessBO;
import com.ftd.accountingreporting.util.AccountingUtil;
import com.ftd.accountingreporting.util.EODUtil;
import com.ftd.accountingreporting.vo.PGEODMessageVO;
import com.ftd.accountingreporting.vo.PGEODRecordVO;
import com.ftd.osp.utilities.plugins.Logger;

public class PGEODReprocessMDB implements MessageDrivenBean, MessageListener {
	private MessageDrivenContext context;

	private Logger logger = new Logger("com.ftd.accountingreporting.ejb.PGEODReprocessMDB");
	
	
	public void ejbCreate() {
	}

	public void onMessage(Message msg) {
		logger.info("Pay Load in message PGEODReprocessMDB ::" + msg);
		TextMessage textMessage = (TextMessage) msg;
		String payLoad = null;
		String[] strArray = null;
		String paymentType = null;
		long billingHeaderId = 0;
		long billingDBHeaderID = 0;
		String queueString = null;
		Connection conn;
		List<PGEODRecordVO> billingIdList = new ArrayList<PGEODRecordVO>();

		try {
			conn = EODUtil.createDatabaseConnection();
			EODDAO dao = new EODDAO(conn);
			PGAltPayDAO pgAltPayDAO = new PGAltPayDAO(conn);

			if (textMessage != null) {
				try {
					payLoad = textMessage.getText();
					strArray = payLoad.split(Pattern.quote("|"));
					if (strArray != null && strArray.length == 2) {
						billingHeaderId = Long.parseLong(strArray[0]);
						paymentType = strArray[1];
					} else {
						logger.error("Error while Formating the Payload::" + payLoad
								+ "Expected payload Format is :: Billing Header ID|Payment Type");
					}

				} catch (NumberFormatException e) {
					logger.error("Error while Formating the Payload::" + payLoad); //TODO Alerting
					logger.error(e.getMessage());
				} catch (Exception e) {
					logger.error("Error while Reading/Tokenizing the Payload::" + payLoad);
					logger.error(e.getMessage());
				}

			}
			
			logger.debug("Given PayLoad After Tokenizing billingHeaderId ::"+billingHeaderId+" Payment Type::"+paymentType);
			if (billingHeaderId > 0 && paymentType != null) {
				
				PGEODReprocessBO reprocessBO = new PGEODReprocessBO(dao,pgAltPayDAO);
				billingDBHeaderID = reprocessBO.getDBBillingHeaderId(billingHeaderId, paymentType);
				
				billingIdList = reprocessBO.getFailedCCBillingDetailsList(billingDBHeaderID, paymentType);
				List<String> messageList = new ArrayList<String>();
				PGEODMessageVO mvo = null;
				
				
				logger.debug("billingIdList Size::"+billingIdList.size());
				
				for (PGEODRecordVO pgEODRecordVO : billingIdList) {
					mvo = new PGEODMessageVO();
					mvo.setBillingDetailId(pgEODRecordVO.getBillingDetailId());
					mvo.setBatchNumber(billingHeaderId);
					mvo.setPaymentMethodId(paymentType.toUpperCase());
					mvo.setRetryCounter(1); //hard coded for isRetry tag = true;
					messageList.add((mvo.toMessageXML()));
				
				}

				if (ARConstants.CREDIT_CARD.equalsIgnoreCase(paymentType))
					queueString = ARConstants.PG_EOD_CART;

				if (ARConstants.PAYPAL.equalsIgnoreCase(paymentType))
					queueString = AltPayConstants.PG_ALTPAY_CART;

				if (messageList != null && messageList.size() > 0)
					AccountingUtil.dispatchJMSMessageList(queueString, messageList, 0, 0);

			}

		} catch (Exception e) {
			logger.error(e.getMessage());
		} catch (Throwable e) {
			logger.error(e.getMessage());
		}
	}

	public void ejbRemove() {
	}

	public void setMessageDrivenContext(MessageDrivenContext ctx) {
		this.context = ctx;
	}
}
