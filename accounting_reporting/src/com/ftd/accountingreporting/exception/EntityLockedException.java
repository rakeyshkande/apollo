package com.ftd.accountingreporting.exception;

public class EntityLockedException extends Exception
{
    Throwable exceptionCause = null;
    
    public EntityLockedException(String message) 
    {
        super("Entity is locked by CSR : " + message);
    }
  
    public EntityLockedException(String msg, Throwable exception){
        super("Entity is locked by CSR : " + msg, exception);
        exceptionCause = exception;
    }
    
    /**Overriding the printStackTraceMethod*/
    public void printStackTrace(){
        if (exceptionCause!=null){
            System.err.println("An exception has been caused by: " + exceptionCause.toString());
            exceptionCause.printStackTrace();
        }
    }
}