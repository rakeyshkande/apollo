package com.ftd.accountingreporting.exception;

public class CancelWithNoDenialException extends Exception
{
     Throwable exceptionCause = null;
    
    public CancelWithNoDenialException(String message){
        super(message);
    }
  
    public CancelWithNoDenialException(String msg, Throwable exception){
        super(msg, exception);
        exceptionCause = exception;
    }
    
    /**Overriding the printStackTraceMethod*/
    public void printStackTrace(){
        if (exceptionCause!=null){
            System.err.println("An exception has been caused by: " + exceptionCause.toString());
            exceptionCause.printStackTrace();
        }
    }
}