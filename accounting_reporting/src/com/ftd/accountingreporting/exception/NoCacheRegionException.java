package com.ftd.accountingreporting.exception;

public class NoCacheRegionException extends Exception
{
     Throwable exceptionCause = null;
    
    public NoCacheRegionException(String message) 
    {
        super(message);
    }
  
    public NoCacheRegionException(String msg, Throwable exception){
        super(msg, exception);
        exceptionCause = exception;
    }
    
    /**Overriding the printStackTraceMethod*/
    public void printStackTrace(){
        if (exceptionCause!=null){
            System.err.println("An exception has been caused by: " + exceptionCause.toString());
            exceptionCause.printStackTrace();
        }
    }
}