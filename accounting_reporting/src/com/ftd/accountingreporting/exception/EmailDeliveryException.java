package com.ftd.accountingreporting.exception;

public class EmailDeliveryException extends Exception
{
     Throwable exceptionCause = null;
    
    public EmailDeliveryException(String message){
        super(message);
    }
  
    public EmailDeliveryException(String msg, Throwable exception){
        super(msg, exception);
        exceptionCause = exception;
    }
    
    /**Overriding the printStackTraceMethod*/
    public void printStackTrace(){
        if (exceptionCause!=null){
            System.err.println("An exception has been caused by: " + exceptionCause.toString());
            exceptionCause.printStackTrace();
        }
    }
}