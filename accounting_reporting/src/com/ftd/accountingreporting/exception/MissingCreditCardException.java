package com.ftd.accountingreporting.exception;

public class MissingCreditCardException extends Exception
{
     Throwable exceptionCause = null;
    
    public MissingCreditCardException(String message){
        super(message);
    }
  
    public MissingCreditCardException(String msg, Throwable exception){
        super(msg, exception);
        exceptionCause = exception;
    }
    
    /**Overriding the printStackTraceMethod*/
    public void printStackTrace(){
        if (exceptionCause!=null){
            System.err.println("An exception has been caused by: " + exceptionCause.toString());
            exceptionCause.printStackTrace();
        }
    }
}