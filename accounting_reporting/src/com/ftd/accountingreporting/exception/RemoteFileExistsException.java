package com.ftd.accountingreporting.exception;

public class RemoteFileExistsException extends Exception
{
     Throwable exceptionCause = null;
    
    public RemoteFileExistsException(String message){
        super(message);
    }
  
    public RemoteFileExistsException(String msg, Throwable exception){
        super(msg, exception);
        exceptionCause = exception;
    }
    
    /**Overriding the printStackTraceMethod*/
    public void printStackTrace(){
        if (exceptionCause!=null){
            System.err.println("An exception has been caused by: " + exceptionCause.toString());
            exceptionCause.printStackTrace();
        }
    }
}