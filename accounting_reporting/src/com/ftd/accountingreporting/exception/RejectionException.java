package com.ftd.accountingreporting.exception;

public class RejectionException extends Exception
{
     Throwable exceptionCause = null;
    
    public RejectionException(String message){
        super(message);
    }
  
    public RejectionException(String msg, Throwable exception){
        super(msg, exception);
        exceptionCause = exception;
    }
    
    /**Overriding the printStackTraceMethod*/
    public void printStackTrace(){
        if (exceptionCause!=null){
            System.err.println("An exception has been caused by: " + exceptionCause.toString());
            exceptionCause.printStackTrace();
        }
    }
}