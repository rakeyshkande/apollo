/**
 * 
 */
package com.ftd.accountingreporting.exception;

/**
 * PTS Settlement file field validation exception
 * 
 * @author kvasant
 *
 */
public class PTSFieldValidationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 00533453567771L;
	
	public PTSFieldValidationException() {
		super();
	}
	
	public PTSFieldValidationException(String message) {
		super(message);
	}
	
	public PTSFieldValidationException(Throwable t) {
		super(t);
	}
	
	public PTSFieldValidationException(String message, Throwable t) {
		super(message, t);
	}
	
}
