package com.ftd.accountingreporting.exception;

public class NoMatchFoundException extends Exception
{
     Throwable exceptionCause = null;
    
    public NoMatchFoundException(String message) 
    {
        super(message);
    }
  
    public NoMatchFoundException(String msg, Throwable exception){
        super(msg, exception);
        exceptionCause = exception;
    }
    
    /**Overriding the printStackTraceMethod*/
    public void printStackTrace(){
        if (exceptionCause!=null){
            System.err.println("An exception has been caused by: " + exceptionCause.toString());
            exceptionCause.printStackTrace();
        }
    }
}