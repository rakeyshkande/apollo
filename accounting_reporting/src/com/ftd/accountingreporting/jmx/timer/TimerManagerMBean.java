package com.ftd.accountingreporting.jmx.timer;

/**
 * The MBean interface for managing a Timer.
 */

public interface TimerManagerMBean
{
  public String stopTimer() throws Throwable;
  public String timerRunning() throws Throwable;
  public String viewTimers() throws Throwable;
  public String getTimerName();
  public void setTimerName(String timerName);
}