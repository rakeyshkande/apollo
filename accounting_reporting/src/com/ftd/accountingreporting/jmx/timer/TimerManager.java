package com.ftd.accountingreporting.jmx.timer;

import com.ftd.accountingreporting.timer.TimerServiceSessionEJB;
import com.ftd.accountingreporting.timer.TimerServiceSessionEJBHome;

import javax.naming.Context;
import javax.naming.InitialContext;


/**
 * MBean manages Timers.
 */
public class TimerManager
       implements TimerManagerMBean
{
  private String _timerName = "PayPalConnectionTester";

  public TimerManager()
  {
  }

  /**
   * Determines if a Timer is running.
   * @return String - Message to display based on outcome of test
   * @throws Throwable
   */
  public String timerRunning() throws Throwable
  {
    Context jndiContext = new InitialContext();
    TimerServiceSessionEJBHome  home = (TimerServiceSessionEJBHome)
        jndiContext.lookup("TimerServiceSessionEJB");
    
    TimerServiceSessionEJB tsejb = home.create();
    
    if(tsejb.isRunning(_timerName))
      return("Timer is running!");
    else
      return("Timer is not running!");
  }


  /**
   * Stops a Timer.
   * @return String - Message to display based on outcome of operation
   * @throws Throwable
   */
  public String stopTimer() throws Throwable
  {
    Context jndiContext = new InitialContext();
    TimerServiceSessionEJBHome  home = (TimerServiceSessionEJBHome)
        jndiContext.lookup("TimerServiceSessionEJB");
    
    TimerServiceSessionEJB tsejb = home.create();
    
    tsejb.stop(_timerName);
    return("Timer was stopped!");
  }


  /**
   * View all timers.
   * @return String - String of all timers
   * @throws Throwable
   */
  public String viewTimers() throws Throwable
  {
    Context jndiContext = new InitialContext();
    TimerServiceSessionEJBHome  home = (TimerServiceSessionEJBHome)
        jndiContext.lookup("TimerServiceSessionEJB");
    
    TimerServiceSessionEJB tsejb = home.create();
    
    String timers = tsejb.getTimers();
    if(timers == null || "".equals(timers)) 
      timers = "No Timers are running!";
    
    return timers;
  }


  /**
   * Returns the timer name
   * @return String - timer name
   */
  public String getTimerName()
  {
    return _timerName;
  }


  /**
   * Sets the timer name
   * @param timerName - new name of timer
   */
  public void setTimerName(String timerName)
  {
     _timerName = timerName;
  }
}