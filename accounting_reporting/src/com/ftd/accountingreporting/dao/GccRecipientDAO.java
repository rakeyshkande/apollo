package com.ftd.accountingreporting.dao;

import com.ftd.accountingreporting.vo.GccRecipientVO;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import org.w3c.dom.Document;


import org.w3c.dom.Document;

/**
 * GccRecipientDAO
 * 
 * This is the DAO that handles the maintenance functions for Gift Certificate Coupon Receipts
 * 
 */
public class GccRecipientDAO 
{
  private Connection connection;
  private static Logger logger  = new Logger("com.ftd.accountingreporting.dao.GccRecipientDAO");
  
  /**
   * Constructor for GccRecipientDAO Class, Initializes the connection object.
   * @param connection
   */
  public GccRecipientDAO(Connection connection)
  {
    this.connection = connection;
  }
  
  /**
   * This method inserts the row in to GC_COUPON_RECIPIENTS
   * @param GccRecipientVO recipient
   * @return boolean (true or false)
   * @throws java.lang.Exception
   */
  public boolean doInsertRecipient(GccRecipientVO recipient) throws Exception
  {
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(this.connection);
      dataRequest.setStatementID("GCC_INSERT_RECIPIENT");
      dataRequest.addInputParam("IN_REQUEST_NUMBER", recipient.getRequestNumber());
      dataRequest.addInputParam("IN_FIRST_NAME", recipient.getFirstName());
      dataRequest.addInputParam("IN_LAST_NAME", recipient.getLastName());
      dataRequest.addInputParam("IN_ADDRESS_1", recipient.getAddress1());
      dataRequest.addInputParam("IN_ADDRESS_2", recipient.getAddress2());
      dataRequest.addInputParam("IN_CITY", recipient.getCity());
      dataRequest.addInputParam("IN_STATE", recipient.getState());
      dataRequest.addInputParam("IN_ZIP_CODE", recipient.getZipCode());
      dataRequest.addInputParam("IN_COUNTRY", recipient.getCountry());
      dataRequest.addInputParam("IN_PHONE", recipient.getPhone());
      dataRequest.addInputParam("IN_EMAIL_ADDRESS", recipient.getEmailAddress());
       
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      Map outputs = (Map) dataAccessUtil.execute(dataRequest);      
      String status = (String) outputs.get(ARConstants.STATUS_PARAM);
      if(status != null && status.equals(ARConstants.COMMON_VALUE_NO))
      {
          String message = (String) outputs.get(ARConstants.MESSAGE_PARAM);
          throw new SQLException(message);
      }
      logger.debug("Insert Recipient successfull. Status="+status);
                
      return (status).equals(ARConstants.COMMON_VALUE_YES)? true:false;     
   }
  
  /**
   * This method deletes all the rows from GC_COUPON_RECIPIENTS for the requestNumberId specified
   * @param String requestNumberId
   * @return boolean (true or false)
   * @throws java.lang.Exception
   */
  public boolean doDeleteRecipient(String requestNumberId) throws Exception
  {
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(this.connection);
      dataRequest.setStatementID("GCC_DELETE_RECIPIENT");
      dataRequest.addInputParam("IN_GC_COUPON_RECIP_ID", new Long(requestNumberId));
      
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      
      Map outputs = (Map) dataAccessUtil.execute(dataRequest);      
      String status = (String) outputs.get(ARConstants.STATUS_PARAM);
      if(status != null && status.equals(ARConstants.COMMON_VALUE_NO))
      {
          String message = (String) outputs.get(ARConstants.MESSAGE_PARAM);
          throw new SQLException(message);
      }
      logger.debug("Delete Recipient successfull. Status="+status);
                
      return (status).equals(ARConstants.COMMON_VALUE_YES)? true:false;     
   }
  
  /**
   * This method gets the Recipient from the GC_COUPON_RECIPIENTS for the requestNumber specified
   * @param String requestNumber
   * @return Document
   * @throws java.lang.Exception
   */
  public Document doGetRecipient(String requestNumber) throws Exception
  {
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(this.connection);
      dataRequest.setStatementID("GET_GCCREC_BY_GCCNUM_OR_REQNUM");
      dataRequest.addInputParam("IN_REQUEST_NUMBER", requestNumber);
      
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      return (Document) dataAccessUtil.execute(dataRequest); 
  }
  
  /**
   * This method gets the Recipient from the GC_COUPON_RECIPIENTS for the requestNumber specified
   * @param String requestNumber
   * @return Document
   * @throws java.lang.Exception
   */
  public Document doGetCouponRecipient(String requestNumber) throws Exception
  {
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(this.connection);
      dataRequest.setStatementID("GET_GCC_AND_GCCREC_BY_REQNUM");
      dataRequest.addInputParam("IN_REQUEST_NUMBER", requestNumber);
      
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      return (Document) dataAccessUtil.execute(dataRequest); 
  }
  
  /**
   * This method updates the GCC Recipient for the requestNumberId specified
   * @param GccRecipientVO recipient
   * @return boolean (true or false)
   * @throws java.lang.Exception
   */
  public boolean doUpdateRecipient(GccRecipientVO recipient) throws Exception
  {
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(this.connection);
      dataRequest.setStatementID("GCC_UPDATE_RECIPIENT");
      dataRequest.addInputParam("IN_GC_COUPON_RECIP_ID", new Long(recipient.getGcCouponReceiptId()));
      dataRequest.addInputParam("IN_REQUEST_NUMBER", recipient.getRequestNumber());
      dataRequest.addInputParam("IN_FIRST_NAME", recipient.getFirstName());
      dataRequest.addInputParam("IN_LAST_NAME", recipient.getLastName());
      dataRequest.addInputParam("IN_ADDRESS_1", recipient.getAddress1());
      dataRequest.addInputParam("IN_ADDRESS_2", recipient.getAddress2());
      dataRequest.addInputParam("IN_CITY", recipient.getCity());
      dataRequest.addInputParam("IN_STATE", recipient.getState());
      dataRequest.addInputParam("IN_ZIP_CODE", recipient.getZipCode());
      dataRequest.addInputParam("IN_COUNTRY", recipient.getCountry());
      dataRequest.addInputParam("IN_PHONE", recipient.getPhone());
      dataRequest.addInputParam("IN_EMAIL_ADDRESS", recipient.getEmailAddress());
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      
      Map outputs = (Map) dataAccessUtil.execute(dataRequest);      
      String status = (String) outputs.get(ARConstants.STATUS_PARAM);
      if(status != null && status.equals(ARConstants.COMMON_VALUE_NO))
      {
          String message = (String) outputs.get(ARConstants.MESSAGE_PARAM);
          throw new SQLException(message);
      }
      logger.debug("Update Recipient successfull. Status="+status);
              
      return (status).equals(ARConstants.COMMON_VALUE_YES)? true:false;     
  }
  
  /**
   * This method gets the Recipient from the GC_COUPON_RECIPIENTS for the coupontNumber specified
   * @param String couponNumber
   * @return Document
   * @throws java.lang.Exception
   */
  public Document doGetRecipientByCouponNumber(String couponNumber) throws Exception
  {
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(this.connection);
      dataRequest.setStatementID("GET_GCCREC_BY_GCCNUM_OR_REQNUM");
      dataRequest.addInputParam("IN_GC_COUPON_NUMBER", couponNumber);
      
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      return (Document) dataAccessUtil.execute(dataRequest); 
  }
}
