package com.ftd.accountingreporting.dao;

import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.sql.Connection;
import java.sql.DriverManager;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import org.w3c.dom.Document;


import org.w3c.dom.Document;
/**
 * GccUtilDAO
 * 
 * This is the DAO that handles the utilities function for Gift Certificate Coupon
 * 
 */
 
public class GccUtilDAO 
{
  private Connection connection;
  private static Logger logger  = new Logger("com.ftd.accountingreporting.dao.GccUtilDAO");
   
  /**
   * Constructor for GccUtilDao Class, Initializes the connection object.
   * @param connection
   */
  public GccUtilDAO(Connection connection)
  {
    this.connection = connection;
  }
  
  /**
   * This method get the company names from database
   * @return Document
   * @throws java.lang.Exception
   */
  public Document doGetCompanyNames() throws Exception
  {
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(this.connection);
      dataRequest.setStatementID("GET_GIFT_CODE_COMPANY");
       
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      
      return (Document) dataAccessUtil.execute(dataRequest);  
  }
  
  /**
   * This method gets the country list from the database
   * @return Document
   * @throws java.lang.Exception
   */
   public Document doGetCountryList() throws Exception
   {
     DataRequest dataRequest = new DataRequest();
     dataRequest.setConnection(this.connection);
     dataRequest.setStatementID("GET_COUNTRY_LIST");
     
     DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
     
     return (Document) dataAccessUtil.execute(dataRequest);
   }
   
  /**
   * This method gets the state list from the database
   * @return Document
   * @throws java.lang.Exception
   */
   public Document doGetStateList() throws Exception
   {
     DataRequest dataRequest = new DataRequest();
     dataRequest.setConnection(this.connection);
     dataRequest.setStatementID("GET_STATE_LIST");
     
     DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
     
     return (Document) dataAccessUtil.execute(dataRequest);
   }
  
  /**
   * This method gets the source code list for the specified company id from the database
   * @param  String sourceCode
   * @param  String desc
   * @param  String companyId
   * @param  String dateFlag
   * @return Document
   * @throws java.lang.Exception
   */
   public Document doGetSourceCodeListByCompanyId(String sourceCode,String desc,String companyId,String dateFlag) throws Exception
   {
     DataRequest dataRequest = new DataRequest();
     dataRequest.setConnection(this.connection);
     dataRequest.setStatementID("GET_SOURCECODELIST_BY_COMPANY");
     dataRequest.addInputParam("IN_SOURCE_CODE", sourceCode);
     dataRequest.addInputParam("IN_DESCRIPTION", desc);
     dataRequest.addInputParam("IN_COMPANY_ID", companyId);
     dataRequest.addInputParam("IN_DATE_FLAG", dateFlag);
     
     DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
     
     return (Document) dataAccessUtil.execute(dataRequest);
   }

  /**
   * This method gets the source code list for the specified company id from the database
   * @param  String companyId
   * @return Document
   * @throws java.lang.Exception
   */
   public Document doGetSourceCodeListByCompanyId(String companyId) throws Exception
   {
     DataRequest dataRequest = new DataRequest();
     dataRequest.setConnection(this.connection);
     dataRequest.setStatementID("GET_SOURCE_CODE_BY_COMPANY_ID");
     dataRequest.addInputParam("IN_COMPANY_ID", companyId);
    
     DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
     
     return (Document) dataAccessUtil.execute(dataRequest);
   }
 
  /** Stored proc is not yet created
   * This method checks whether the orderNumber pass is an WEBOE (ie order by telephone) order
   * @param  String orderNumber
   * @return boolean
   * @throws java.lang.Exception
   */
   public boolean orderExists(String orderNumber) throws Exception
   {
     DataRequest dataRequest = new DataRequest();
     dataRequest.setConnection(this.connection);
     dataRequest.setStatementID("IS_WEB_OE_ORDER");
     dataRequest.addInputParam("IN_EXTERNAL_ORDER_NUMBER", orderNumber);
     
     DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
     CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
     
     return (rs.getRowCount()>0)? true:false; 
   }  

    /**
   * Retrieve list of GC program names
   * @return 
   * @throws java.lang.Exception
   */
    public Document doListPartnerPrograms() throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("LIST_GC_PROGRAMS");
        dataRequest.addInputParam("IN_PROGRAM_TYPE", "GC");
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        Document outputs = (Document) dataAccessUtil.execute(dataRequest);  
        
        return outputs;
    }

}
