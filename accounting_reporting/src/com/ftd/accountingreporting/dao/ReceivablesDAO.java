package com.ftd.accountingreporting.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;


public class ReceivablesDAO
{
  private Connection connection;
  private static Logger logger = new Logger("com.ftd.customerordermanagement.dao.CustomerDAO");
  private static final String NAME_DELIMITER = ",";

  public ReceivablesDAO(Connection connection)
  {
    this.connection = connection;
  }


  /**
   * Method to retrieve the outstanding receivables data
   *
   * @param partnerId
   * @param originId
   * @param customer service representative id
   * @param start position
   * @param maxRecords
   * @param column to sort on
   * @param sort direction - ascending or descending
   *
   * @return HashMap containing cursors
   *
   * @throws java.lang.Exception
   */
  public

  HashMap getRemitInfo(String partnerId, String originId, String csrId, long startPosition, long recordsToBeReturned, 
                       String sortColumn, String sortDirection)
    throws Exception
  {
    HashMap searchResults = new HashMap();
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("GET_OUTSTANDING_RECEIVABLES");

    dataRequest.addInputParam("IN_PARTNER_ID", partnerId);
    dataRequest.addInputParam("IN_ORIGIN_ID", originId);
    dataRequest.addInputParam("IN_START_POSITION", new Long(startPosition));
    dataRequest.addInputParam("IN_MAX_NUMBER_RETURNED", new Long(recordsToBeReturned));
    dataRequest.addInputParam("IN_CREATED_BY", csrId);
    dataRequest.addInputParam("IN_SORT_COLUMN", sortColumn);
    dataRequest.addInputParam("IN_SORT_DIRECTION", sortDirection);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    searchResults = (HashMap) dataAccessUtil.execute(dataRequest);

    return searchResults;

  }


  /**
   * This method updates the temp table that holds the remittance (header) information
   *
   * @param partnerId - partner id
   * @param remitNumber - Remittance number
   * @param cRemitDate - Remittance date - calendar object
   * @param remitAmount - Remittance amount
   * @param csrId - csr id
	 *
   * @throws Exception
	 */
  public void updateTempRemitHeader(String partnerId, String remitNumber, Calendar cRemitDate, String remitAmount, String csrId)
    throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    Map outputs = null;

    java.sql.Date dRemitDate = null;
    if (cRemitDate != null) 
      dRemitDate = new java.sql.Date(cRemitDate.getTimeInMillis());

    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_PARTNER_ID", partnerId);
    inputParams.put("IN_REMITTANCE_NUMBER", remitNumber);
    inputParams.put("IN_REMITTANCE_DATE", dRemitDate);
    inputParams.put("IN_REMITTANCE_AMT", remitAmount==null||remitAmount.equalsIgnoreCase("")?new Double(0):new Double(remitAmount));
    inputParams.put("IN_UPDATED_BY", csrId);

    /* build DataRequest object */
    dataRequest.setConnection(connection);
    dataRequest.setStatementID("UPDATE_REM_UPDATE");
    dataRequest.setInputParams(inputParams);

    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    outputs = (Map) dataAccessUtil.execute(dataRequest);

    /* read store prodcedure output parameters to determine if the procedure executed successfully */
    String status = (String) outputs.get("OUT_STATUS");
    if (status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }
  }


  /**
   * This method updates the temp table that holds the remittance (detail) information
   *
   * @param partnerId - partner id
   * @param remitNumber - Remittance number
   * @param orderDetailId - order detail id
   * @param cashReceived - cash received
   * @param writeOff - write off amount
   * @param remainingBal - remaining balanace amount
   * @param settleFlag - settle flag
   * @param csrId - csr id
	 *
   * @throws Exception
	 */
  public void updateTempRemitDetail(String partnerId, String remitNumber, String orderDetailId, String cashReceived, 
                                    String writeOff, String remainingBal, String settleFlag, String csrId)
    throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    Map outputs = null;

    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_PARTNER_ID", partnerId);
    inputParams.put("IN_REMITTANCE_NUMBER", remitNumber);
    inputParams.put("IN_ORDER_DETAIL_ID", orderDetailId);
    inputParams.put("IN_CASH_RECEIVED_AMT", cashReceived==null||cashReceived.equalsIgnoreCase("")?new Double(0):new Double(cashReceived));
    inputParams.put("IN_WRITE_OFF_AMT", writeOff==null||writeOff.equalsIgnoreCase("")?new Double(0):new Double(writeOff));
    inputParams.put("IN_REMAINING_BALANCE_DUE_AMT", remainingBal==null||remainingBal.equalsIgnoreCase("")?new Double(0):new Double(remainingBal));
    inputParams.put("IN_SETTLE_FLAG", settleFlag);
    inputParams.put("IN_UPDATED_BY", csrId);

    /* build DataRequest object */
    dataRequest.setConnection(connection);
    dataRequest.setStatementID("UPDATE_REM_DTLS_UPDATE");
    dataRequest.setInputParams(inputParams);

    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    outputs = (Map) dataAccessUtil.execute(dataRequest);

    /* read store prodcedure output parameters to determine if the procedure executed successfully */
    String status = (String) outputs.get("OUT_STATUS");
    if (status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }
  }


  /**
   * This method deletes records from the remittance (header) update table
   *
   * @param partnerId - partner id
	 *
   * @throws Exception
	 */
  public void deleteTempRemitHeader(String partnerId)
    throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    Map outputs = null;

    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_PARTNER_ID", partnerId);

    /* build DataRequest object */
    dataRequest.setConnection(connection);
    dataRequest.setStatementID("DELETE_REM_UPDATE");
    dataRequest.setInputParams(inputParams);

    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    outputs = (Map) dataAccessUtil.execute(dataRequest);

    /* read store prodcedure output parameters to determine if the procedure executed successfully */
    String status = (String) outputs.get("OUT_STATUS");
    if (status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }
  }


  /**
   * This method deletes records from the remittance (detail) update table
   *
   * @param partnerId - partner id
	 *
   * @throws Exception
	 */
  public void deleteTempRemitDetail(String partnerId)
    throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    Map outputs = null;

    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_PARTNER_ID", partnerId);

    /* build DataRequest object */
    dataRequest.setConnection(connection);
    dataRequest.setStatementID("DELETE_REM_DTLS_UPDATE");
    dataRequest.setInputParams(inputParams);

    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    outputs = (Map) dataAccessUtil.execute(dataRequest);

    /* read store prodcedure output parameters to determine if the procedure executed successfully */
    String status = (String) outputs.get("OUT_STATUS");
    if (status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }
  }


  /**
   * This method insert records in the remittance (header) table
   *
   * @param partnerId - partner id
   * @param remitNumber - remittance number
	 *
   * @throws Exception
	 */
  public void insertRemitHeader(String partnerId, String remitNumber)
    throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    Map outputs = null;

    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_PARTNER_ID", partnerId);
    inputParams.put("IN_REMITTANCE_NUMBER", remitNumber);

    /* build DataRequest object */
    dataRequest.setConnection(connection);
    dataRequest.setStatementID("INSERT_REMITTANCE");
    dataRequest.setInputParams(inputParams);

    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    outputs = (Map) dataAccessUtil.execute(dataRequest);

    /* read store prodcedure output parameters to determine if the procedure executed successfully */
    String status = (String) outputs.get("OUT_STATUS");
    if (status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }
  }


  /**
   * This method insert records in the remittance (detail) table
   *
   * @param partnerId - partner id
   * @param remitNumber - remittance number
	 *
   * @throws Exception
	 */
  public void insertRemitDetail(String partnerId, String remitNumber)
    throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    Map outputs = null;

    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_PARTNER_ID", partnerId);
    inputParams.put("IN_REMITTANCE_NUMBER", remitNumber);

    /* build DataRequest object */
    dataRequest.setConnection(connection);
    dataRequest.setStatementID("INSERT_REMITTANCE_DETAILS");
    dataRequest.setInputParams(inputParams);

    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    outputs = (Map) dataAccessUtil.execute(dataRequest);

    /* read store prodcedure output parameters to determine if the procedure executed successfully */
    String status = (String) outputs.get("OUT_STATUS");
    if (status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }
  }


  /**
   * This method checks if the remittance number already exists
   *
   * @param remitNumber - remittance number
	 *
   * @throws Exception
	 */
  public String remitNumberExists(String remitNumber)
    throws Exception
  {
    DataRequest dataRequest = new DataRequest();

    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_REMITTANCE_NUMBER", remitNumber);

    /* build DataRequest object */
    dataRequest.setConnection(connection);
    dataRequest.setStatementID("REMIT_NUM_EXISTS");
    dataRequest.setInputParams(inputParams);

    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    String remitNumExists = (String) dataAccessUtil.execute(dataRequest);

    return remitNumExists;

  }



}
