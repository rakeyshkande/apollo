package com.ftd.accountingreporting.dao;

import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.exception.CancelWithNoDenialException;
import com.ftd.accountingreporting.exception.NoMatchFoundException;
import com.ftd.accountingreporting.exception.RejectionException;
import com.ftd.accountingreporting.vo.CCSettlementVO;
import com.ftd.accountingreporting.vo.ReconMessageVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;

import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

public class ReconDAO 
{
    private Connection dbConnection = null;
    private Logger logger = 
        new Logger("com.ftd.accountingreporting.dao.ReconDAO");
        
    public ReconDAO(Connection conn)
    {
        super();
        dbConnection = conn;
    }

    /**
     * This method Updates all RECON_MESSAGES.RECON_DISP_CODE to �ARCHIVED� 
     * with mercury.mercury.transmission_time 
     * (if recon_messages.merc_or_venus is merc) and venus.venus.transmission_time 
     * (if recon_messages.verc_or_venus is venus) on or before in_date 
     * if recon_messages.recon_disp_code is �RECONCILED� or �PRICE VARIANCE�.
     * Call CLEAN.ORDER_MESG_PKG.ARCHIVE_RECON_MESG.
     * @param n/a
     * @return CachedResultSet
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     * @throws Exception
     */
    public void doArchiveReconMesg(Date cutoff_date)
        throws SAXException, ParserConfigurationException, IOException,
            SQLException, Exception
    {
       if(logger.isDebugEnabled()){
            logger.debug("Entering doArchiveReconMesg");
        }
        
        DataRequest request = new DataRequest();
        
        try
        {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_DATE", new Timestamp(cutoff_date.getTime()));

            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("ARCHIVE_RECON_MESSAGES");
            
            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            String status = (String) outputs.get("OUT_STATUS");
            if(!status.equals(ARConstants.COMMON_VALUE_YES))
            {
                String errorMessage = (String) outputs.get("OUT_MESSAGE");
                if(!errorMessage.startsWith("WARNING")) {
                    throw new Exception(errorMessage);
                } else {
                    logger.debug(errorMessage);
                }
            }

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting doArchiveReconMesg");
            } 
        }     
    }

    /**
     * This method checks if the florist and vendor is sending a duplicate 
     * reconciliation record. Call CLEAN.ORDER_MESG_PKG.IS_DOUBLE_DIP.
     * @param message_id - String
     * @param merc_or_venus - String
     * @return boolean
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     * @todo Need stored proc to be created
     */
    public boolean isDoubleDip(String message_id, String merc_or_venus)
         throws SAXException, ParserConfigurationException, IOException,
            SQLException, Exception
    {
       if(logger.isDebugEnabled()){
            logger.debug("Entering isDoubleDip");
        }
        
        String isDoubleDip = "";
        DataRequest request = new DataRequest();

        try
        {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_SYSTEM", merc_or_venus);
            inputParams.put("IN_MESSAGE_ID", message_id);
            
            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("IS_DOUBLE_DIP");
            
            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            isDoubleDip = (String) dau.execute(request);

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting isDoubleDip");
            } 
        }  
        
        return isDoubleDip.equals("Y")?true:false;
        
    }
    
    /**
     * This method calculates the amount to be reconciled for a particular 
     * order number. Check for the following conditions and 
     * throw exception back:
     * �	NO MATCH FOUND
     * �	CAN WITH NO DEN
     * �	REJ
     * @param message_id - String
     * @param merc_or_venus - String
     * @param cutoff_date - Date
     * @return double
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     * @todo Need stored proc to be created
     */
    public double doCalcAmtToRecon(String message_id, String merc_or_venus, 
        Date cutoff_date) 
        throws CancelWithNoDenialException, NoMatchFoundException,
            RejectionException, SAXException, ParserConfigurationException, 
            IOException, SQLException, Exception
    {
       if(logger.isDebugEnabled()){
            logger.debug("Entering doCalcAmtToRecon");
            logger.debug("message_id " + message_id);
            logger.debug("merc_or_venus " + merc_or_venus);
            logger.debug("cutoff_date " + cutoff_date);
        }
        
        DataRequest request = new DataRequest();
        double amtToRecon = 0;
        
        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
          
            inputParams.put("IN_MESSAGE_ID", message_id);
            inputParams.put("IN_SYSTEM", merc_or_venus);
            inputParams.put("IN_DATE", new Timestamp(cutoff_date.getTime()));

            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("CALCULATE_AMOUNT_TO_RECONCILE");
            
            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            String status = "";
          
            status = (String) outputs.get("OUT_STATUS");
            if(status.equals(ARConstants.COMMON_VALUE_YES))
            {
                amtToRecon = Double.parseDouble(outputs.get("OUT_PRICE").toString());
            }
            else
            {
                String errorMessage = (String) outputs.get("OUT_MESSAGE");
                if(errorMessage.equals("No records found"))
                {
                    throw new NoMatchFoundException(errorMessage);
                }else if(errorMessage.equals("A rejected record was found"))
                {
                    throw new RejectionException(errorMessage);
                }else if(errorMessage.equals("A CANCEL record with no DENY record was found"))
                {
                    throw new CancelWithNoDenialException(errorMessage);
                }else{
                    throw new Exception(errorMessage);
                }
            }  
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting doCalcAmtToRecon");
            } 
        }
        return amtToRecon;
    } 

    /**
     * This method inserts a record in RECON_MESSAGES table.
     * @param ReconMessageVO - reconMsg
     * @return n/a
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     * @todo Need stored proc to be created
     */
    public void doInsertReconMessage(ReconMessageVO reconMsg)
        throws SAXException, ParserConfigurationException, IOException,
            SQLException, Exception
    {
       if(logger.isDebugEnabled()){
            logger.debug("Entering doInsertReconMessage");
        }
        
        DataRequest request = new DataRequest();

        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_FILLER_ID", reconMsg.getFillerId());
            inputParams.put("IN_SYSTEM", reconMsg.getSystem());
            inputParams.put("IN_MESSAGE_ID", reconMsg.getMessageId());
            inputParams.put("IN_AMOUNT_RECONCILED", new Double(reconMsg.getAmtReconciled()));
            inputParams.put("IN_AMOUNT_TO_RECONCILE", new Double(reconMsg.getAmtToReconcile()));
            inputParams.put("IN_RECON_DISP_CODE", reconMsg.getReconDispCode());

            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("INSERT_RECON_MESSAGES");
            
            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            String status = (String) outputs.get("OUT_STATUS");
            if(!status.equals(ARConstants.COMMON_VALUE_YES))
            {
                String errorMessage = (String) outputs.get("OUT_MESSAGE");
                throw new Exception(errorMessage);
            }

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting doInsertReconMessage");
            } 
        }
    }
 
    /**
     * This method inserts a record in RECON_MESSAGES table.
     * @param ReconMessageVO - reconMsg
     * @return n/a
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     * @todo Need stored proc to be created
     */
    public void doInsertCCSettlement(CCSettlementVO ccSettlement)
        throws SAXException, ParserConfigurationException, IOException,
            SQLException, Exception
    {
       if(logger.isDebugEnabled()){
            logger.debug("Entering doInsertCCSettlement");
        }
        
        DataRequest request = new DataRequest();

        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_TRANSACTION_DATE", new Timestamp(ccSettlement.getTransactionDate().getTime()));
            inputParams.put("IN_CC_NUMBER", ccSettlement.getCCNumber());
            inputParams.put("IN_AMOUNT", new Double(ccSettlement.getAmount()));
            inputParams.put("IN_AUTH_NUMBER", ccSettlement.getAuthNumber());
            inputParams.put("IN_RECON_DISP_CODE", ccSettlement.getReconDispCode());

            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("INSERT_CC_SETTLEMENT_ERRORS");
            
            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            String status = (String) outputs.get("OUT_STATUS");
            if(!status.equals(ARConstants.COMMON_VALUE_YES))
            {
                String errorMessage = (String) outputs.get("OUT_ERROR_MESSAGE");
                throw new Exception(errorMessage);
            }

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting doInsertCCSettlement");
            } 
        }
    }
    public String doGetFillerID(String systemID, String messageID)
        throws SAXException, ParserConfigurationException, IOException,
            SQLException
    {
       if(logger.isDebugEnabled()){
            logger.debug("Entering doGetFillerID");
            logger.debug("System ID : " + systemID);
            logger.debug("Message ID : " + messageID);
        }
        
        DataRequest request = new DataRequest();
        
        String fillerID = "";
        
        try
        {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_SYSTEM", systemID);
            inputParams.put("IN_MESSAGE_ID", messageID);
                
            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("GET_FILLER_ID");
            
            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            fillerID = (String) dau.execute(request);
            
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting doGetFillerID");
            } 
        }
        
        return fillerID;
    }

    public CachedResultSet doFindSettlementPayment(CCSettlementVO vo)
        throws SAXException, ParserConfigurationException, IOException,
            SQLException, Exception
    {
       if(logger.isDebugEnabled()){
            logger.debug("Entering doFindSettlementPayment");
        }
        
        CachedResultSet outputs = null;
        DataRequest request = new DataRequest();     
        String ccNumber = vo.getCCNumber();
        Date transDate = vo.getTransactionDate();
        String authNumber = vo.getAuthNumber();
        
        
        try
        {
            logger.debug(vo.toString());
            
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_CC_NUMBER", vo.getCCNumber());
            inputParams.put("IN_AUTH_DATE", new Timestamp(vo.getTransactionDate().getTime()));
            inputParams.put("IN_AUTH_NUMBER", vo.getAuthNumber());
            inputParams.put("IN_CREDIT_AMOUNT", new Double(vo.getAmount()));
            
            
            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("FIND_SETTLEMENT_PAYMENT");
            
            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            outputs = (CachedResultSet) dau.execute(request);

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting doFindSettlementPayment");
            } 
        }
        
        return outputs;
    } 

    
}