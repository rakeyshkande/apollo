package com.ftd.accountingreporting.dao;

import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.vo.PartnerRefundVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

/**
 * This is the value object that models each line item on the bill file. 
 * @author Charles Fox
 */
public class CommonDAO 
{
    private Logger logger = 
        new Logger("com.ftd.accountingreporting.dao.CommonDAO");
    private Connection dbConnection = null;
    
    public CommonDAO(Connection conn)
    {
        super();
        dbConnection = conn;
    }

    
    /**
     *  This method retrieves the partnerís refund reason that maps to the 
     *  refund disposition. Call CLEAN.PARTNERS_EOD_PKG.GET_PARTNER_REFUND_REASON. 
     * @param refundDisp - String
     * @param partnerId - String
     * @return String
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     */
     public String doGetPartnerRefundReason(String refundDisp, String partnerId)
         throws SAXException, ParserConfigurationException, IOException, 
            SQLException
     {
     
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doGetPartnerRefundReason");
            logger.debug("refunddisp: " + refundDisp);
            logger.debug("partnerId: " + partnerId);
        }
        
        String refundReason = "";
         DataRequest request = new DataRequest();
        
        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(
                "IN_REFUND_DISP_CODE", refundDisp);
            inputParams.put(
                "IN_PARTNER_ID", partnerId);

            // build DataRequest object
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("GET_PARTNER_REFUND_REASON");

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            refundReason = (String) dau.execute(request);
            
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting doGetPartnerRefundReason");
            } 
        }
        return refundReason;
     }

    /**
     *  This method inserts record in the CLEAN.PARTNER_REFUND table. 
     *  Call CLEAN.XXX_PKG.INSERT_PARTNER_REFUND.
     * @param partnerRefundVO - PartnerRefundVO
     * @return n/a
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     * @throws Exception
     */
     public void doCreatePartnerRefund(PartnerRefundVO partnerRefundVO)
        throws SAXException, ParserConfigurationException, IOException, 
            SQLException, Exception
     {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doCreatePartnerRefund");
            logger.debug("refundId: " + partnerRefundVO.getRefundId());
        }
        
        DataRequest request = new DataRequest();

        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_REFUND_ID", partnerRefundVO.getRefundId());
            inputParams.put(
                "IN_PARTNER_ID", partnerRefundVO.getPartnerId());
                
            inputParams.put("IN_EXT_TYPE", partnerRefundVO.getFieldName());
            inputParams.put("IN_EXT_DATA", partnerRefundVO.getFieldValue());

            // build DataRequest object
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("INSERT_REFUND_PARTNER_EXT");

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            String status = (String) outputs.get("OUT_STATUS");
            if(!status.equals(ARConstants.COMMON_VALUE_YES))
            {
                String errorMessage = (String) outputs.get("OUT_MESSAGE");
                throw new Exception(errorMessage);
            }
            
        
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting doCreatePartnerRefund");
            } 
        }
     }

    /**
     *  This method updates the accounting status of the order_bill records 
     *  for the given external order number to the specified status using 
     *  CLEAN.ORDER_MAINT_PKG.UPDATE_AMAZON_ORDER_BILL_STAT.
     * @param externalOrderNumber - String
     * @param status - String
     * @return n/a
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     * @throws Exception
     * @todo - code and determine exceptions
     */
     public void doUpdateOrderAcctgStatus(String externalOrderNumber, String status)
        throws SAXException, ParserConfigurationException, IOException, 
            SQLException, Exception
     { 
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doUpdateOrderAcctgStatus");
            logger.debug("externalOrderNumber: " + externalOrderNumber);
            logger.debug("status: " + status);
        }
        
        DataRequest request = new DataRequest();

        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(
                "IN_EXTERNAL_ORDER_NUMBER", externalOrderNumber);   
            inputParams.put("IN_BILL_STATUS", status); 

            // build DataRequest object
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("UPDATE_AMAZON_ORDER_BILL_STAT");

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            String procStatus = (String) outputs.get("OUT_STATUS");
            if(!procStatus.equals(ARConstants.COMMON_VALUE_YES))
            {
                String errorMessage = (String) outputs.get("OUT_MESSAGE");
                throw new Exception(errorMessage);
            }
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting doUpdateOrderAcctgStatus");
            } 
        }
     }

    /**
     *  This method updates the accounting status of the refund records for 
     *  the given external order number to the specified status using 
     *  CLEAN.REFUND_PKG.UPDATE_REFUND_ACCTG_STATUS.
     * @param externalOrderNumber - String
     * @param status - String
     * @return n/a
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     * @throws Exception
     * @todo - code and determine exceptions
     */
    public void doUpdateRefundAcctgStatus(String externalOrderNumber, String status)
        throws SAXException, ParserConfigurationException, IOException, 
            SQLException, Exception
    {  
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doUpdateRefundAcctgStatus");
            logger.debug("ExternalOrderNumber: " + externalOrderNumber);
            logger.debug("status: " + status);
        }
        DataRequest request = new DataRequest();

        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(
                "IN_EXTERNAL_ORDER_NUMBER", externalOrderNumber);   
            inputParams.put("IN_REFUND_STATUS", status); 

            // build DataRequest object
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("UPDATE_REFUND_ACCTG_STATUS");

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            String procStatus = (String) outputs.get("OUT_STATUS");
            if(!procStatus.equals(ARConstants.COMMON_VALUE_YES))
            {
                String errorMessage = (String) outputs.get("OUT_MESSAGE");
                throw new Exception(errorMessage);
            }
            
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting doUpdateRefundAcctgStatus");
            } 
        }
    }
    
    /**
     *  Updates the order_disp_code to the specified status for the
     *  order with given orderDetailId using 
     *  clean.order_maint_pkg.UPDATE_ORDER_OP_STATUS
     * @param batchDate - Date
     * @return CachedResultSet
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     * @todo - code and determine exceptions
     */
    public void doUpdateOperationStatus(String orderDetailId, 
        String orderDispCode, String updatedBy)
        throws SAXException, ParserConfigurationException, IOException, 
            SQLException, Exception
    {  
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doUpdateRefundAcctgStatus");
            logger.debug("orderDetailId: " + orderDetailId);
            logger.debug("orderDispCode: " + orderDispCode);
            
        }
        DataRequest request = new DataRequest();

        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(
                "IN_ORDER_DETAIL_ID", orderDetailId);   
            inputParams.put("IN_ORDER_DISP_CODE", orderDispCode); 
            inputParams.put("IN_UPDATED_BY", updatedBy);
            
            // build DataRequest object
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("UPDATE_ORDER_DISPOSITION");

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            String procStatus = (String) outputs.get("OUT_STATUS");
            if(!procStatus.equals(ARConstants.COMMON_VALUE_YES))
            {
                String errorMessage = (String) outputs.get("OUT_MESSAGE");
                throw new Exception(errorMessage);
            }
            
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting doUpdateRefundAcctgStatus");
            } 
        }


    }


        /**
    * This method checks if any order in the shopping cart is in the given status
    * @param payment_id long
    * @param status String
    * @return boolean
    * @throws java.lang.Exception
    */
    public boolean isShoppingCartInStatus(String master_order_number, String status) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering isShoppingCartInStatus");
            logger.debug("masterOrderNumber: " + master_order_number);
            logger.debug("status: " + status);
        }
        String inStatus = "";
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.dbConnection);
            dataRequest.setStatementID("IS_SHOPPING_CART_IN_STATUS");
            dataRequest.addInputParam("IN_MASTER_ORDER_NUMBER", master_order_number);
            dataRequest.addInputParam("IN_STATUSES", status);
            
            logger.debug("Calling IS_SHOPPING_CART_IN_STATUS: " + master_order_number);
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            inStatus = (String)dataAccessUtil.execute(dataRequest);
            dataRequest.reset();
            logger.debug(inStatus);
        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting isShoppingCartInStatus");
            } 
        }
        return "TRUE".equalsIgnoreCase(inStatus)?true:false;
    }
    
   /**
    * This method updates the status of the Bill to the status specified
    * @param payment_id long
    * @param time_stamp long
    * @param status String
    * @throws java.lang.Exception
    */
    public boolean doUpdatePaymentBillStatus(long payment_id, long time_stamp, String upstatus) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doUpdatePaymentBillStatus");
            logger.debug("paymentId: " + payment_id);
            logger.debug("status: " + upstatus);
        }
        String status = "";
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.dbConnection);
            dataRequest.setStatementID("UPDATE_BILL_REFUND_STATUS");
            dataRequest.addInputParam("IN_PAYMENT_ID", new Long(payment_id).toString());
            dataRequest.addInputParam("IN_TIMESTAMP", new Timestamp(time_stamp));
            dataRequest.addInputParam("IN_STATUS", upstatus);
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            
            logger.debug("Calling UPDATE_BILL_REFUND_STATUS: " + payment_id);
            Map outputs = (Map) dataAccessUtil.execute(dataRequest);      
            dataRequest.reset();
            status = (String) outputs.get(ARConstants.STATUS_PARAM);
            if(status != null && status.equals(ARConstants.COMMON_VALUE_NO))
            {
                String message = (String) outputs.get(ARConstants.MESSAGE_PARAM);
                throw new SQLException(message);
            }
            logger.debug("Update Bill payment status  successfull. Status="+status);

        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting doUpdatePaymentBillStatus");
            } 
        } 
        return (status).equals(ARConstants.COMMON_VALUE_YES)? true:false; 
    }

   /**
    * This method updates the status of the Refund to the status specified.
    * @param refund_id long
    * @param time_stamp long
    * @param status String
    * @throws java.lang.Exception
    */
    public boolean doUpdatePaymentRefundStatus(String refund_id, long time_stamp, String upstatus) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doUpdatePaymentRefundStatus");
            logger.debug("refund_id: " + refund_id);
        }
        String status = "";
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.dbConnection);
            dataRequest.setStatementID("UPDATE_BILL_REFUND_STATUS");
            dataRequest.addInputParam("IN_REFUND_ID", refund_id);
            dataRequest.addInputParam("IN_TIMESTAMP", new Timestamp(time_stamp));
            dataRequest.addInputParam("IN_STATUS", upstatus);
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            
            logger.debug("Calling UPDATE_BILL_REFUND_STATUS: " + refund_id);
            Map outputs = (Map) dataAccessUtil.execute(dataRequest);      
            dataRequest.reset();
            status = (String) outputs.get(ARConstants.STATUS_PARAM);
            if(status != null && status.equals(ARConstants.COMMON_VALUE_NO))
            {
                String message = (String) outputs.get(ARConstants.MESSAGE_PARAM);
                throw new SQLException(message);
            }
            logger.debug("Update refund payment status  successfull. Status="+status);

        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting doUpdatePaymentBillStatus");
            } 
        } 
        return (status).equals(ARConstants.COMMON_VALUE_YES)? true:false; 
    }

  /**
   * This method gets the global param value for the name passed
   * @param name
   * @return String
   * @throws java.lang.Exception
   */
  public String doGetGlobalParamValue(String name) throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.dbConnection);
        
        dataRequest.setStatementID("GET_GLOBAL_PARM_VALUE");
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        dataRequest.addInputParam("IN_CONTEXT", "ACCOUNTING");
        dataRequest.addInputParam("IN_NAME", name);
        logger.debug("retrieve global parm: " + name);
        
        return dataAccessUtil.execute(dataRequest).toString();
   }
   
  public void doUpdateGlobalParms(String name, String value, String updatedBy) throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.dbConnection);
        
        dataRequest.setStatementID("UPDATE_GLOBAL_PARMS");
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        dataRequest.addInputParam("IN_CONTEXT", "ACCOUNTING");
        dataRequest.addInputParam("IN_NAME", name);
        dataRequest.addInputParam("IN_VALUE", value);
        dataRequest.addInputParam("IN_UPDATED_BY", updatedBy);
        logger.debug("update global parm name: " + name);
        logger.debug("update global parm value: " + value);
        
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);      
        dataRequest.reset();
        String status = (String) outputs.get(ARConstants.STATUS_PARAM);
        if(status != null && status.equals(ARConstants.COMMON_VALUE_NO))
        {
            String message = (String) outputs.get(ARConstants.MESSAGE_PARAM);
            throw new SQLException(message);
        }
   }
  
	/**
	 * This method updates the status of the Refund/payment associated with a
	 * refund id
	 * 
	 * @param refund_id long
	 * @param status String
	 * @throws java.lang.Exception
	 */
	@SuppressWarnings("rawtypes")
	public void doUpdatePaymentRefundStatus(String refund_id, String status)
			throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("Entering doUpdatePaymentRefundStatus(refund_id, status)");
			logger.debug("refund_id: " + refund_id);
		}
		try {
			DataRequest dataRequest = new DataRequest();
			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			dataRequest.setConnection(this.dbConnection);
			dataRequest.setStatementID("UPDATE_REFUND_STATUS");
			dataRequest.addInputParam("IN_REFUND_ID", refund_id);
			dataRequest.addInputParam("IN_STATUS", status);

			logger.debug("Calling UPDATE_REFUND_STATUS: " + refund_id);
			Map outputs = (Map) dataAccessUtil.execute(dataRequest);
			dataRequest.reset();

			if (!ARConstants.COMMON_VALUE_YES.equals(outputs.get(ARConstants.STATUS_PARAM))) {
				String message = (String) outputs.get(ARConstants.MESSAGE_PARAM);
				throw new SQLException(message);
			}
			logger.debug("Updated refund/payment status successfully.");
		} finally {
			if (logger.isDebugEnabled()) {
				logger.debug("Exiting doUpdatePaymentRefundStatus(refund_id, upstatus)");
			}
		}
	}
}