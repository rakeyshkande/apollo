package com.ftd.accountingreporting.dao;

import com.ftd.accountingreporting.altpay.vo.AafesBillingDetailVO;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.vo.AafesRecordVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

/**
 * This is the data access object for billing. 
 * @author Charles Fox
 */
public class AafesDAO 
{

    private Logger logger = 
        new Logger("com.ftd.accountingreporting.dao.AafesDAO");
    private Connection dbConnection = null;
    
    public AafesDAO(Connection conn)
    {
        super();
        dbConnection = conn;
    }
    
    public AafesDAO() {}
    
    public void setConnection(Connection conn) {
        dbConnection = conn;
    }

    /**
     *  This method creates the AAFES_BILLING_HEADER record. 
     *  Call CLEAN.xxx_PKG.CREATE_BILLING_HEADER(billingDate). 
     *  Return the batch number.
     * @param billingDate - Date
     * @return String
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     * @todo - code and determine exceptions
     */
     public String doCreateBillingHeader(Date billingDate)
             throws SAXException, ParserConfigurationException, IOException, 
            SQLException
     {
       if(logger.isDebugEnabled()){
            logger.debug("Entering doCreateBillingHeader");
            logger.debug("Billing Date : " + billingDate);
        }
         DataRequest request = new DataRequest();

        String batchNumber = "";
        
        try {
            // setup store procedure input parameters 
            HashMap inputParams = new HashMap();
            java.sql.Date sqlBillingDate = 
                        new java.sql.Date(billingDate.getTime());
            inputParams.put(
                "IN_AAFES_BILLING_BATCH_DATE", sqlBillingDate);

            // build DataRequest object
            request.setConnection(dbConnection);
            
            request.setInputParams(inputParams);
            request.setStatementID("INSERT_AAFES_BILLING_HEADER");

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            request.reset();
            batchNumber = outputs.get("OUT_AAFES_BILLING_HEADER_ID").toString();

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting doCreateBillingHeader");
            } 
        }
        
         return batchNumber;
     }

   /**
   * Update batch processed indicator. 
   * @param batchNumber
   * @param processedInd
   * @return 
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws java.io.IOException
   * @throws java.sql.SQLException
   */
     public void doUpdateBillingHeader(String batchNumber, String processedInd)
             throws SAXException, ParserConfigurationException, IOException, 
            SQLException
     {
       if(logger.isDebugEnabled()){
            logger.debug("Entering doUpdateBillingHeader");
            logger.debug("batchNumber : " + batchNumber);
            logger.debug("processedInd : " + processedInd);
        }
         DataRequest request = new DataRequest();
        
        try {
            // setup store procedure input parameters 
            HashMap inputParams = new HashMap();
            inputParams.put(
                "IN_AAFES_BILLING_HEADER_ID", batchNumber);
            inputParams.put(
                "IN_PROCESSED_INDICATOR", processedInd);

            // build DataRequest object
            request.setConnection(dbConnection);
            
            request.setInputParams(inputParams);
            request.setStatementID("UPDATE_AAFES_BILLING_HEADER");

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            request.reset();
            String status = outputs.get("OUT_STATUS").toString();
            if(ARConstants.COMMON_VALUE_NO.equals(status)) {
                String message = outputs.get("OUT_MESSAGE").toString();
                logger.error(new SQLException(message));
            }

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting doUpdateBillingHeader");
            } 
        }

     }

    /**
     *  This method creates the AAFES_BILLING_DETAILS record. 
     *  Call CLEAN.xxx_PKG.CREATE_BILLING_DETAIL. Return the batch number.
     * @param batchNumber - String
     * @param aafesRecord - AafesRecordVO
     * @return String - aafes_billing_detail_id
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     * @todo - code and determine exceptions
     */
     public String doCreateBillingDetail(String batchNumber, 
        AafesBillingDetailVO aafesRecord)
        throws SAXException, ParserConfigurationException, IOException, 
            SQLException, Exception
     {
        if(logger.isDebugEnabled()){
            logger.debug("Entering doCreateBillingDetail");
            logger.debug("Batch Number : " + batchNumber);
        }
        
        DataRequest request = new DataRequest();
        String status = "";
        String paymentType = null;
        try {
            /* setup store procedure input parameters */
             paymentType = aafesRecord.getPaymentCode();
             if (paymentType!= null && paymentType.equalsIgnoreCase("R")){
                 paymentType = "Refund";
             } else {
                 paymentType = "Bill";
             }
             
            HashMap inputParams = new HashMap();
            inputParams.put(
                "IN_AAFES_BILLING_HEADER_ID", batchNumber);
                
            inputParams.put("IN_PAYMENT_ID", aafesRecord.getPaymentId()); 
            inputParams.put("IN_BILL_TYPE", paymentType);
            inputParams.put("IN_RETURN_CODE", aafesRecord.getReturnCode());
            inputParams.put("IN_AUTH_CODE", aafesRecord.getAuthNumber());
            inputParams.put("IN_TICKET_NUMBER", aafesRecord.getTicketNumber());
            inputParams.put("IN_RETURN_MESSAGE", aafesRecord.getReturnMessage());
            inputParams.put("IN_VERIFIED_FLAG", aafesRecord.getVerifiedFlag());
            inputParams.put("IN_REQ_AMT", aafesRecord.getReqAmt().toString());
            
            // build DataRequest object
            request.setConnection(dbConnection);
            request.setInputParams(inputParams);
            request.setStatementID("INSERT_AAFES_BILLING_DETAILS");

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            request.reset();
            status = (String) outputs.get("OUT_STATUS");
            if(!status.equals(ARConstants.COMMON_VALUE_YES))
            {
                String errorMessage = (String) outputs.get("OUT_MESSAGE");
                throw new Exception(errorMessage);
            }
            return (String)outputs.get("OUT_AAFES_BILLING_DETAILS_ID");
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting doCreateBillingDetail");
            } 
        }
        
     }
     

    /**
     *  This method updates the billing detail with information 
     *  coming back from AAFES.
     * @param vo - AafesRecordVO
     * @return boolean
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     * @todo - code and determine exceptions
     */
     public void doUpdateBillingDetail(AafesBillingDetailVO vo)
        throws SAXException, ParserConfigurationException, IOException, 
            SQLException
     {
       if(logger.isDebugEnabled()){
            logger.debug("Entering doUpdateBillingDetail");
            logger.debug("Billing Detail ID : " + vo.getBillingDetailId());
        }
        DataRequest request = new DataRequest();
        String status = "";
        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(
                "IN_AAFES_BILLING_DETAILS_ID", vo.getBillingDetailId());
                
            inputParams.put("IN_RETURN_CODE", vo.getReturnCode());
            
            // change back until table column size increased.
            String returnMsg = vo.getReturnMessage();
            //returnMsg = returnMsg!=null? returnMsg.substring(0,50) : "";
            inputParams.put("IN_RETURN_MESSAGE", returnMsg);
            inputParams.put("IN_VERIFIED_FLAG", vo.getVerifiedFlag());
            inputParams.put("IN_TICKET_NUMBER", vo.getTicketNumber());
            inputParams.put("IN_AUTH_CODE", vo.getAuthNumber());
            // build DataRequest object
            request.setConnection(dbConnection);
            
            request.setInputParams(inputParams);
            request.setStatementID("UPDATE_AAFES_BILLING_DETAILS");

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            request.reset();
            status = (String) outputs.get("OUT_STATUS");
            if (ARConstants.COMMON_VALUE_NO.equals(status)) {
                throw new SQLException((String)outputs.get("OUT_MESSAGE"));
            }
            logger.debug("Update successful.");
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting doUpdateBillingDetail");
            } 
        }

     }
    
    /**
     * Retrieve payment aafes_ticekt_numbers from payments table that are related
     * to this refund payment id.
     * 
     * @param paymentId
     * @return
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     * @throws Exception
     */
    public CachedResultSet doGetAafesAuthInfo(String paymentId)
       throws SAXException, ParserConfigurationException, IOException, 
           SQLException, Exception
    {
       if(logger.isDebugEnabled()){
           logger.debug("Entering doGetAafesAuthInfo");
           logger.debug("payment id : " + paymentId);
       }
       
       DataRequest request = new DataRequest();

       try {
           
            
           HashMap inputParams = new HashMap();
           inputParams.put("IN_PAYMENT_ID", paymentId);
         
           // build DataRequest object
           request.setConnection(dbConnection);
           request.setInputParams(inputParams);
           request.setStatementID("GET_AAFES_AUTH_INFO");

           // get data
           DataAccessUtil dau = DataAccessUtil.getInstance();
           CachedResultSet outputs = (CachedResultSet) dau.execute(request);
           request.reset();
           return outputs;
           
       } finally {
           if(logger.isDebugEnabled()){
              logger.debug("Exiting doGetAafesAuthInfo");
           } 
       }
       
    }     
}