package com.ftd.accountingreporting.dao;

import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.vo.AmazonOrderAdjustmentVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

/**
 * This is the data access object for billing
 * @author Charles Fox
 */
public class AmazonDAO 
{

    private Logger logger = 
        new Logger("com.ftd.accountingreporting.dao.AmazonDAO");
    private Connection dbConnection = null;
    
    public AmazonDAO(Connection conn)
    {
        super();
        dbConnection = conn;
    }
    
    /**
     *  This method retrieves the unprocessed remittance data. 
     *  Call AMAZON.GET_UNPRCSSD_SETTLEMENT_DATA. 
     *  Return the ResultSet.
     * @param n/a
     * @return CachedResultSet
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     */
    public CachedResultSet doGetUnprocessedRemittance()
        throws SAXException, 
        ParserConfigurationException, IOException, SQLException, Exception
        {
               
       if(logger.isDebugEnabled()){
            logger.debug("Entering doGetUnprocessedRemittance");
        }
        
        DataRequest request = new DataRequest();
        CachedResultSet unprocessedRemittanceOrders = null;

        try {
            /* build DataRequest object */
            request.setConnection(dbConnection);
            
            request.setStatementID("GET_NEW_SETTLEMENT_DATA");
            
            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance(); 
            unprocessedRemittanceOrders = (CachedResultSet) dau.execute(request);
            request.reset();
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting doGetUnprocessedRemittance");
            } 
        }
            return unprocessedRemittanceOrders; 
    }
    
    /**
     *  This method retrieves the settlement order item with the given 
     *  settlement data id. Call AMAZON.GET_REMITTANCE_DETAIL. 
     * @param settlementDataId - String
     * @return CachedResultSet
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     */
    public HashMap doGetRemittanceDetails(String settlementDataId)
        throws SAXException, 
        ParserConfigurationException, IOException, SQLException, Exception
        {
               
       if(logger.isDebugEnabled()){
            logger.debug("Entering doGetRemittanceDetails");
            logger.debug("Settlement Data ID : " + settlementDataId);
        }
        
        DataRequest request = new DataRequest();
        HashMap remittanceDetails = null;

        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_AZ_SETTLEMENT_DATA_ID", settlementDataId);
                
            /* build DataRequest object */
            request.setConnection(dbConnection);
            
            request.setInputParams(inputParams);
            request.setStatementID("GET_REMITTANCE_DETAIL");
            
            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            
            remittanceDetails = (HashMap) dau.execute(request);
            request.reset();
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting doGetRemittanceDetails");
            } 
        }
            return remittanceDetails;
    }

    /**
     *  This method marks the settlement data as processed with a system date. 
     *  Call AMAZON.MARK_SETTLMENT_PROCESSED(settlementDataId).
     * @param settlementDataId - String
     * @return n/a
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     */
    public void doMarkSettlementProcessed(String settlementDataId)
        throws SAXException, ParserConfigurationException, IOException, 
            SQLException, Exception
    {   
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doMarkSettlementProcessed");
        }
        DataRequest request = new DataRequest();

        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(
                "IN_SETTLEMENT_ID", settlementDataId);   

            // build DataRequest object
            request.setConnection(dbConnection);
            
            request.setInputParams(inputParams);
            request.setStatementID("MARK_SETTLEMENT_PROCESSED");

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            request.reset();
            String status = (String) outputs.get("OUT_STATUS");
            if(!status.equals(ARConstants.COMMON_VALUE_YES))
            {
                String errorMessage = (String) outputs.get("OUT_MESSAGE");
                throw new Exception(errorMessage);
            }
            
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting doMarkSettlementProcessed");
            } 
        }
    }
    
  /**
     *  This method retrieves refund records that need to have adjustments
     *  sent to Amazon. Call CLEAN.XXX_PKG.GET_REFUND_RECORDS.
     * @param n/a
     * @return CachedResultSet
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     */
    public CachedResultSet doGetRefundRecords(String partnerID, Date batchDate)
        throws SAXException, 
        ParserConfigurationException, IOException, SQLException, Exception
        {
               
       if(logger.isDebugEnabled()){
            logger.debug("Entering doGetRefundRecords");
            logger.debug("Partner ID : " + partnerID);
            logger.debug("Batch Date : " + batchDate);
        }
        
        DataRequest request = new DataRequest();
        CachedResultSet refundRecords = null;

        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();

            java.sql.Date sqlProcessDate = 
                        new java.sql.Date(batchDate.getTime());   
            inputParams.put("IN_DATE", sqlProcessDate);
            
            /* build DataRequest object */
            request.setConnection(dbConnection);
            
            request.setInputParams(inputParams);
            request.setStatementID("GET_AMAZON_REFUND_RECORDS");
            
            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            refundRecords = (CachedResultSet) dau.execute(request);
            request.reset();
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting doGetRefundRecords");
            } 
        }
            return refundRecords;
    }
    
  /**
   * Marks all Unbilled Amazon orders as Billed.
   * @param billDate
   * @param billStatus
   * @throws java.lang.Exception
   */
    public void doBillAmazonOrders(Date billDate, String billStatus) throws Exception {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doBillAmazonOrders");
        }
        DataRequest request = new DataRequest();

        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_BILL_DATE", new Timestamp(billDate.getTime()));  
            inputParams.put("IN_BILL_STATUS", billStatus);

            // build DataRequest object
            request.setConnection(dbConnection);
            request.setInputParams(inputParams);
            request.setStatementID("UPDATE_AMAZON_ORDER_BILLS");

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            request.reset();
            
            String status = (String) outputs.get("OUT_STATUS");
            if(!status.equals(ARConstants.COMMON_VALUE_YES))
            {
                String errorMessage = (String) outputs.get("OUT_MESSAGE");
                if(!errorMessage.startsWith("WARNING")) {
                    throw new Exception(errorMessage);
                } else {
                    logger.debug(errorMessage);
                }
            }
            
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting doBillAmazonOrders");
            } 
        }        
    
    
    }
    
    public void doBillAmazonRefunds(Date billDate) 
       throws SAXException, ParserConfigurationException, IOException, 
            SQLException, Exception
     { 
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doBillAmazonRefunds");
        }
        
        DataRequest request = new DataRequest();

        try {
            // setup store procedure input parameters 
            HashMap inputParams = new HashMap();
            inputParams.put("BILL_STATUS", ARConstants.ACCTG_STATUS_BILLED);
            inputParams.put("IN_BILL_DATE", new Timestamp(billDate.getTime())); 
            // build DataRequest object
            request.setConnection(dbConnection);
            
            request.setInputParams(inputParams);
            request.setStatementID("UPDATE_AMAZON_REFUNDS");

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            request.reset();
            String procStatus = (String) outputs.get("OUT_STATUS");
            if(!procStatus.equals(ARConstants.COMMON_VALUE_YES))
            {
                String errorMessage = (String) outputs.get("OUT_MESSAGE");
                if(!errorMessage.startsWith("WARNING")) {
                    throw new Exception(errorMessage);
                } else {
                    logger.debug(errorMessage);
                }
            }
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting doBillAmazonRefunds");
            } 
        }
     }     
    
}