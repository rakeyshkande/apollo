package com.ftd.accountingreporting.dao;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.vo.BillingHeaderPGVO;
import com.ftd.accountingreporting.vo.BillingHeaderVO;
import com.ftd.accountingreporting.vo.EODPaymentVO;
import com.ftd.accountingreporting.vo.EODRecordVO;
import com.ftd.accountingreporting.vo.PGEODMessageVO;
import com.ftd.accountingreporting.vo.PGEODRecordVO;
import com.ftd.accountingreporting.vo.PaymentTotalsVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pg.client.creditcard.settlement.response.SettlementResponse;

public class EODDAO 
{
    private Connection connection;
    private static Logger logger  = 
        new Logger("com.ftd.accountingreporting.dao.EODDAO");

   /**
    * Constructor for EODDAO Class, Initializes the connection object.
    * @param connection
    */    
    public EODDAO() {}
    
    public EODDAO(Connection conn)
    {
        connection = conn;
    }
      
    public Connection getConnection() throws Exception {
        return connection;
    }
    
    public void setConnection(Connection conn){
        connection = conn;
    }
    
   /**
    * 1.	Call the CLEAN.END_OF_DAY_PKG.CREATE_BILLING_HEADER 
    *       stored procedure.  
    * 2.	Return the batch number.
    * @return String billingHeader
    * @throws java.lang.Exception
    */
    public String doCreateBillingHeader(Calendar batchDate) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doCreateBillingHeader");
        }
        Map outputs = null;
        try {
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.setStatementID("CREATE_BILLING_HEADER");
            dataRequest.addInputParam("IN_BATCH_DATE", new Timestamp(batchDate.getTimeInMillis()));
      
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            
            outputs = (Map) dataAccessUtil.execute(dataRequest); 
            dataRequest.reset();
            String status = (String) outputs.get(ARConstants.STATUS_PARAM);
            if(status != null && status.equals(ARConstants.COMMON_VALUE_NO))
            {
                String message = (String) outputs.get(ARConstants.MESSAGE_PARAM);
                throw new SQLException(message);
            }
            if(logger.isDebugEnabled()){
                logger.debug("Create on Billing Header successfull. Status="+status);
            }
        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting doCreateBillingHeader");
            } 
        }
        return  outputs.get("OUT_SEQUENCE_NUMBER").toString();
    }

   /**
    * This method updates the billing Header
    * @param String billingId
    * @param String processedInd
    * @return boolean
    * @throws java.lang.Exception
    */
    public boolean doUpdateBillingHeader(String billingId, String processedInd) throws Exception
    {

        if(logger.isDebugEnabled()) {
            logger.debug("Entering doUpdateBillingHeader");
        }
        String status = "";
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.setStatementID("UPDATE_BILLING_HEADER");
            dataRequest.addInputParam("IN_BILLING_HEADER_ID", new Long(billingId));
            dataRequest.addInputParam("IN_PROCESSED_INDICATOR", processedInd);
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
                    
            logger.debug("Calling UPDATE_BILLING_HEADER..." + billingId);
            Map outputs = (Map) dataAccessUtil.execute(dataRequest);   
            dataRequest.reset();
            status = (String) outputs.get(ARConstants.STATUS_PARAM);
            if(status != null && status.equals(ARConstants.COMMON_VALUE_NO))
            {
                String message = (String) outputs.get(ARConstants.MESSAGE_PARAM);
                throw new SQLException(message);
            }
            if(logger.isDebugEnabled()){
                logger.debug("Update on Billing Header successfull. Status="+status);
            }

        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting doUpdateBillingHeader");
            } 
        } 
        return (status).equals(ARConstants.COMMON_VALUE_YES)? true:false; 
    }
    
   /**
    * This method inserts the billing detail record
    * @param String billingId
    * @param String processedInd
    * @return boolean
    * @throws java.lang.Exception
    */
    public String doCreateBillingDetail(EODRecordVO vo,long batchNumber) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doCreateBillingDetail");
        }
        String retValue = "";
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.setStatementID("CREATE_BILLING_DETAIL");
            dataRequest.addInputParam("IN_BILLING_HEADER_ID", new Long(batchNumber));
            dataRequest.addInputParam("IN_ORDER_NUMBER", vo.getOrderNumber());
            dataRequest.addInputParam("IN_ORDER_SEQUENCE", new Long(vo.getOrderSequence()));
            dataRequest.addInputParam("IN_PAYMENT_ID", new Long(vo.getPaymentId()));
            dataRequest.addInputParam("IN_CC_TYPE", vo.getCreditCardType());
            dataRequest.addInputParam("IN_CC_NUMBER", vo.getCreditCardNumber());
            dataRequest.addInputParam("IN_CC_EXPIRATION", vo.getCcExpDate());
            dataRequest.addInputParam("IN_PAYMENT_METHOD", vo.getPaymentMethod());
            /*
            * SP-72: BAMS - Additional validations for fields in settlement file.
            * Rounding order amount to 2 decimals before saving it into clean.billing_detail. This is done to avoid the side effects of using BigDecimal operations
            * resulting in lengthy decimal positions.
            * E.g: 94.9999999999999
            */
            //dataRequest.addInputParam("IN_ORDER_AMOUNT", new Double(vo.getOrderAmount()));
            dataRequest.addInputParam("IN_ORDER_AMOUNT", BigDecimal.valueOf(vo.getOrderAmount()).setScale(2, BigDecimal.ROUND_DOWN).doubleValue());
            if (vo.getSalesTaxAmount()!=null && !"".equals(vo.getSalesTaxAmount())) {
                dataRequest.addInputParam("IN_SALES_TAX_AMOUNT", new Double(vo.getSalesTaxAmount()));
            }
            dataRequest.addInputParam("IN_TRANSACTION_TYPE", vo.getTransactionType());
            if (vo.getAuthorizationDate() != null) {
                dataRequest.addInputParam("IN_AUTH_DATE", new Timestamp(vo.getAuthorizationDate().getTimeInMillis()));
            }
            dataRequest.addInputParam("IN_AUTH_APPROVAL_CODE", vo.getApprovalCode());
            dataRequest.addInputParam("IN_ACQ_REFERENCE_NUMBER", vo.getAcqReferenceNumber());
            dataRequest.addInputParam("IN_AUTH_CHAR_INDICATOR", vo.getAuthCharIndicator());
            dataRequest.addInputParam("IN_AUTH_TRANSACTION_ID", vo.getAuthTranId());
            dataRequest.addInputParam("IN_AUTH_VALIDATION_CODE", vo.getAuthValidationCode());
            dataRequest.addInputParam("IN_AUTH_SOURCE_CODE", vo.getAuthSourceCode());
            dataRequest.addInputParam("IN_AVS_CODE", vo.getAVSResultCode());
            dataRequest.addInputParam("IN_RESPONSE_CODE", vo.getResponseCode());
            dataRequest.addInputParam("IN_CLEARING_MEMBER_NUMBER", vo.getClearingMemberNumber());
            dataRequest.addInputParam("IN_RECIPIENT_ZIP_CODE", vo.getRecipientZip());
            dataRequest.addInputParam("IN_ORDER_ORIGIN", vo.getOriginType());
            if (vo.getOrderDate()!=null)
                dataRequest.addInputParam("IN_ORDER_DATE", new Timestamp(vo.getOrderDate().getTimeInMillis()));
            dataRequest.addInputParam("IN_BILL_TYPE", vo.getBillType());
            if (vo.getCancelDate()!=null)
                dataRequest.addInputParam("IN_CANCEL_DATE", new Timestamp(vo.getCancelDate().getTimeInMillis()));
            dataRequest.addInputParam("IN_CARDHOLDER_REF", vo.getCardHolderRefNumber());
            dataRequest.addInputParam("IN_ADD_INFO1", vo.getAddField1());
            dataRequest.addInputParam("IN_ADD_INFO2", vo.getAddField2());
            dataRequest.addInputParam("IN_ADD_INFO3", vo.getAddField3());
            dataRequest.addInputParam("IN_ADD_INFO4", vo.getAddField4());
            dataRequest.addInputParam("IN_COMPANY_ID", vo.getCompanyId());
            
            //MasterPass iteration 7 changes  
            dataRequest.addInputParam("IN_WALLET_INDICATOR", vo.getWalletIndicator());
            //BAMS Integration changes  
            dataRequest.addInputParam("IN_CSC_RESPONSE_CODE", vo.getCscResponseCode());
            dataRequest.addInputParam("IN_INITIAL_AUTH_AMOUNT", new Double(vo.getInitialAuthAmount()));
            dataRequest.addInputParam("IN_VOICE_AUTH_FLAG", vo.getVoiceAuthFlag());
            
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
                    
            Map outputs = (Map) dataAccessUtil.execute(dataRequest);  
            dataRequest.reset();
            String status = (String) outputs.get(ARConstants.STATUS_PARAM);
            if(status != null && status.equals(ARConstants.COMMON_VALUE_NO))
            {
                String message = (String) outputs.get(ARConstants.MESSAGE_PARAM);
                throw new SQLException(message);
            }
            if(logger.isDebugEnabled()){
                logger.debug("Insert on Billing Detail successfull. Status=" 
                    + status);
            }
            retValue = outputs.get("OUT_SEQUENCE_NUMBER").toString(); 
        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting doCreateBillingDetail");
            } 
        } 

        return retValue; 
    }

  /**
   * Retrieves a list of EODPaymentVO for all payments that should be billed.
   * @param batchDate
   * @return 
   * @throws java.lang.Exception
   */
    public ArrayList doGetPayments(Calendar batchDate) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doGetPayments");
        }
        ResultSet rs = null;
        String masterOrderNumber = null;
        String paymentId = null;
        String paymentInd = null;
        EODPaymentVO paymentvo = null;
        ArrayList paymentList = new ArrayList();
        
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.setStatementID("GET_PAYMENT_BILLING_IDS");
            dataRequest.addInputParam("IN_DATE", new Timestamp(batchDate.getTimeInMillis()));
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            
            rs = (ResultSet)dataAccessUtil.execute(dataRequest);
            dataRequest.reset();
            
            while(rs.next()) {
                masterOrderNumber = rs.getString("master_order_number");
                paymentId = rs.getString("payment_id");
                paymentInd = rs.getString("payment_indicator");
                paymentvo = new EODPaymentVO(masterOrderNumber, paymentId, paymentInd);
                paymentList.add(paymentvo);
            }
            
            if(rs != null) {
                rs.getStatement().close();
                rs.close();
            }

        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting doGetPayments");
            } 
        } 
        return  paymentList;
    }
    
  /*
    public ResultSet doGetPaymentIds(Calendar batchDate) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doGetPaymentIds");
        }
        ResultSet rs = null;
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.setStatementID("GET_PAYMENT_BILLING_IDS");
            dataRequest.addInputParam("IN_DATE", new Timestamp(batchDate.getTimeInMillis()));
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            
            rs = (ResultSet)dataAccessUtil.execute(dataRequest);
            dataRequest.reset();

        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting doGetPaymentIds");
            } 
        } 
        return  rs;
    }
    */
    public CachedResultSet doGetPaymentById(String paymentId) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doGetPaymentById");
        }
        CachedResultSet rs = null;
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.setStatementID("GET_PAYMENT_BY_ID");
            dataRequest.addInputParam("IN_PAYMENT_ID", paymentId);
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    
            logger.debug("Calling GET_PAYMENT_BY_ID: " + paymentId);
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
            dataRequest.reset();
        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting doGetPaymentById");
            } 
        } 
        return  rs;
    }
 /*
    public CachedResultSet doGetRefunds(Calendar batchDate) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doGetRefunds");
        }
        
        CachedResultSet rs = null;
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.setStatementID("GET_REFUND_BILLING_RECORDS");
            dataRequest.addInputParam("IN_DATE", new Timestamp(batchDate.getTimeInMillis()));
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
            dataRequest.reset();
        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting doGetRefunds");
            } 
        } 
        return  rs;
    } 
 */  
    public CachedResultSet doGetRefundByPaymentId(String paymentId) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doGetRefunds");
        }
        
        CachedResultSet rs = null;
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.setStatementID("GET_REFUND_BY_PAYMENT_ID");
            dataRequest.addInputParam("IN_PAYMENT_ID", paymentId);
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
            dataRequest.reset();
        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting doGetRefunds");
            } 
        } 
        return  rs;
    } 
    
   /**
    * This method updates the status of the Bill to 'BILLED'
    * @param payment_id long
    * @param time_stamp long
    * @param status String
    * @throws java.lang.Exception
    */
    public boolean doUpdatePaymentBillStatus(long payment_id, long time_stamp, String upstatus) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doUpdatePaymentBillStatus");
        }
        String status = "";
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.setStatementID("UPDATE_BILL_REFUND_STATUS");
            dataRequest.addInputParam("IN_PAYMENT_ID", new Long(payment_id).toString());
            //dataRequest.addInputParam("IN_REFUND_ID", null);
            dataRequest.addInputParam("IN_TIMESTAMP", new Timestamp(time_stamp));
            dataRequest.addInputParam("IN_STATUS", upstatus);
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            
            logger.debug("Calling UPDATE_BILL_REFUND_STATUS: " + payment_id);
            Map outputs = (Map) dataAccessUtil.execute(dataRequest);      
            dataRequest.reset();
            status = (String) outputs.get(ARConstants.STATUS_PARAM);
            if(status != null && status.equals(ARConstants.COMMON_VALUE_NO))
            {
                String message = (String) outputs.get(ARConstants.MESSAGE_PARAM);
                throw new SQLException(message);
            }
            logger.debug("Update Bill payment status  successfull. Status="+status);

        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting doUpdatePaymentBillStatus");
            } 
        } 
        return (status).equals(ARConstants.COMMON_VALUE_YES)? true:false; 
    }

   /**
    * This method updates the status of the Bill to 'BILLED'
    * @param refund_id long
    * @param time_stamp long
    * @param status String
    * @throws java.lang.Exception
    */
    public boolean doUpdateRefundBillStatus(long refund_id, long time_stamp, String upstatus) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doUpdateRefundBillStatus");
        }
        String status = "";
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.setStatementID("UPDATE_BILL_REFUND_STATUS");
            //dataRequest.addInputParam("IN_PAYMENT_ID", null);
            dataRequest.addInputParam("IN_REFUND_ID", new Long(refund_id).toString());
            dataRequest.addInputParam("IN_TIMESTAMP", new Timestamp(time_stamp));
            dataRequest.addInputParam("IN_STATUS", upstatus);
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            
            logger.debug("Calling UPDATE_BILL_REFUND_STATUS: " + refund_id);
            Map outputs = (Map) dataAccessUtil.execute(dataRequest);     
            dataRequest.reset();
            status = (String) outputs.get(ARConstants.STATUS_PARAM);
            if(status != null && status.equals(ARConstants.COMMON_VALUE_NO))
            {
                String message = (String) outputs.get(ARConstants.MESSAGE_PARAM);
                throw new SQLException(message);
            }
            if(logger.isDebugEnabled()){
                logger.debug("Update Bill Refund status  successfull. Status=" 
                    + status);
            }

        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting doUpdateRefundBillStatus");
            } 
        } 
        return (status).equals(ARConstants.COMMON_VALUE_YES)? true:false; 
    }
    
    
   /**
    * This method retrieves the customer name for the given payment_id 
    * @param payment_id long
    * @return String
    * @throws java.lang.Exception
    */
    public String doGetCustomerName(long payment_id) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doGetCustomerName");
        }
        String result = "";
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.setStatementID("GET_CUST_NAME_BY_PAYMENT_ID");
            dataRequest.addInputParam("IN_PAYMENT_ID", new Long(payment_id));
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            
            logger.debug("Calling GET_CUST_NAME_BY_PAYMENT_ID: " + payment_id);
            result = (String)dataAccessUtil.execute(dataRequest);
            dataRequest.reset();

        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting doGetCustomerName");
            } 
        }
        return result;
    }
    
   /**
    * This method retrieves the clearing member number for hte given payment_id
    * @param payment_id long
    * @return String
    * @throws java.lang.Exception
    */
    public String doGetClearingMemberNumber(long payment_id) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doGetClearingMemberNumber");
        }
        String result = "";
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.setStatementID("GET_CLEARING_MEMBER_NUMBER");
            dataRequest.addInputParam("IN_PAYMENT_ID", new Long(payment_id));
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            
            logger.debug("Calling GET_CLEARING_MEMBER_NUMBER: " + payment_id);
            result = (String)dataAccessUtil.execute(dataRequest);
            dataRequest.reset();
        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting doGetClearingMemberNumber");
            } 
        }
        return result;
    }
    
   /**
    * This method retrieves the origin origin Id for the specified origin guid
    * @param origin_guid String
    * @return String
    * @throws java.lang.Exception
    */
    public String doGetOriginId(String origin_guid) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doGetOriginId");
        }
        String originId = "";
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.setStatementID("GET_CART");
            dataRequest.addInputParam("IN_ORDER_GUID", origin_guid);
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            
            logger.debug("Calling GET_CART: " + origin_guid);
            Map outputs = (Map) dataAccessUtil.execute(dataRequest);  
            dataRequest.reset();
            CachedResultSet rs = (CachedResultSet) outputs.get("OUT_CUR");
            
            if (rs.next()) 
            {
                Object o = rs.getObject(3);
                if(o!=null) {
                    originId = (String)o;
                }
            }
        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting doGetOriginId");
            } 
        }
        return originId;
    }
    
   /**
    * This method retrieves the origin type with the given origin_id
    * @param origin_id String
    * @return String
    * @throws java.lang.Exception
    */
    public String doGetType(String origin_id) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doGetType");
        }
        String result = "";
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.setStatementID("GET_ORIGIN_TYPE");
            dataRequest.addInputParam("IN_ORIGIN_ID", origin_id);
            
            logger.debug("Calling GET_ORIGIN_TYPE: " + origin_id);
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            result = (String)dataAccessUtil.execute(dataRequest);
            dataRequest.reset();

        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting doGetType");
            } 
        } 
        return result;
    }
    

   
   /**
    * This method retrieves the pcard configuration fields associated with the partner
    * @param payment_id long
    * @return ResultSet
    * @throws java.lang.Exception
    */
    public CachedResultSet doGetPcardConfigFields(long payment_id) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doGetPcardConfigFields");
        }
        CachedResultSet rs = null;
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.setStatementID("GET_PARTNER_CONFIG_FIELDS");
            dataRequest.addInputParam("IN_PAYMENT_ID", new Long(payment_id));
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            
            logger.debug("Calling GET_PARTNER_CONFIG_FIELDS: " + payment_id);
            rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
            dataRequest.reset();
        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting doGetPcardConfigFields");
            } 
        }
        return rs;
    }

   /**
    * This method retrieves the pcard configuration fields associated with the partner
    * @param payment_id long
    * @return ResultSet
    * @throws java.lang.Exception
    */
    public CachedResultSet doGetPcardConfigFields() throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doGetPcardConfigFields");
        }
        CachedResultSet rs = null;
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.setStatementID("GET_PARTNER_CONFIG_FIELDS");
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            
            rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
            dataRequest.reset();
        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting doGetPcardConfigFields");
            } 
        }
        return rs;
    }
   /**
    * This method retrieves the co brand information associated with the order
    * @param payment_id long
    * @return ResultSet
    * @throws java.lang.Exception
    */
    public CachedResultSet doGetCoBrandFields(long payment_id) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doGetCoBrandFields");
        }
        CachedResultSet rs = null;
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.setStatementID("GET_PARTNER_CO_BRAND_FIELDS");
            dataRequest.addInputParam("IN_PAYMENT_ID", new Long(payment_id));
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            
            logger.debug("Calling GET_PARTNER_CO_BRAND_FIELDS: " + payment_id);
            rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
            dataRequest.reset();
        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting doGetCoBrandFields");
            } 
        }
        return rs;
    }

  /**
   * This method gets the order details
   * @param order_detail_id long
   * @return resultSet
   * @throws java.lang.Exception
   */
   public CachedResultSet doGetOrderDetails(long order_detail_id) throws Exception
   {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doGetOrderDetails");
        }
        CachedResultSet rs = null;
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.setStatementID("GET_ORDER_DETAILS");
            dataRequest.addInputParam("IN_ORDER_DETAIL_ID", new Long(order_detail_id));
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            
            logger.debug("Calling GET_ORDER_DETAILS: " + order_detail_id);
            rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
            dataRequest.reset();
        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting doGetOrderDetails");
            } 
        }
        return rs;
   }

   /**
    * This method retrieves the recipient information associated with the recipient id
    * @param recipient_id long
    * @return ResultSet
    * @throws java.lang.Exception
    */
    public CachedResultSet doGetCusotmerInfo(long recipient_id) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doGetCusotmerInfo");
        }
        CachedResultSet rs = null;
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.setStatementID("GET_CUSTOMER");
            dataRequest.addInputParam("IN_CUSTOMER_ID", new Long(recipient_id));
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            
            logger.debug("Calling get_customer: " + recipient_id);
            rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
            dataRequest.reset();
        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting doGetCusotmerInfo");
            } 
        }
        return rs;
    }
    
   /**
    * This method retrieves the country code based on the format fo the 
    * zip code. returns null if not found
    * @param zip String
    * @return String
    * @throws java.lang.Exception
    */
    public String doGetCountryFromZip(String zip) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doGetCountryFromZip");
        }
        String result = "";
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.setStatementID("GET_COUNTRY_FROM_ZIP");
            dataRequest.addInputParam("IN_ZIP_CODE", zip);
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            
            logger.debug("Calling get_country_from_zip: " + zip);
            result = (String)dataAccessUtil.execute(dataRequest);
            dataRequest.reset();
        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting doGetCountryFromZip");
            } 
        }
        return result;
    }

   /**
    * This method retrieves the payments total for the specified payment_id
    * @param payment_id long
    * @return PaymentTotalsVO
    * @throws java.lang.Exception
    */
    public PaymentTotalsVO doGetPaymentTotals(long payment_id) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doGetPaymentTotals");
        }
        PaymentTotalsVO totalVO = new PaymentTotalsVO();
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.setStatementID("GET_PAYMENT_TOTALS");
            dataRequest.addInputParam("IN_PAYMENT_ID", new Long(payment_id));
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            
            logger.debug("Calling get_payment_totals: " + payment_id);
            Map outputs = (Map) dataAccessUtil.execute(dataRequest);  
            dataRequest.reset();
            
            if (outputs.get("OUT_SERVICE_FEE_TOTAL") != null)
                totalVO.setServiceFeeTotal(new Double (
                outputs.get("OUT_SERVICE_FEE_TOTAL").toString()).doubleValue());
            if (outputs.get("OUT_SHIPPING_FEE_TOTAL") != null)
                totalVO.setShippingFeeTotal(new Double (
                outputs.get("OUT_SHIPPING_FEE_TOTAL").toString()).doubleValue());
            if (outputs.get("OUT_SHIPPING_TAX_TOTAL") != null)
                totalVO.setShippingTaxTotal(new Double (
                outputs.get("OUT_SHIPPING_TAX_TOTAL").toString()).doubleValue());
            if (outputs.get("OUT_TAX_TOTAL") != null)
                totalVO.setTaxTotal(new Double (
                outputs.get("OUT_TAX_TOTAL").toString()).doubleValue());
        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting doGetPaymentTotals");
            } 
        }
        return totalVO;
    }

   /**
    * 1.	Call the CLEAN.END_OF_DAY_PKG.GET_BILLING_DETAILS stored procedure.  
    * 2.	Return the 2 result sets in an Array List.
    * @param billing_header_id long
    * @return ArrayList
    * @throws java.lang.Exception
    */
    @SuppressWarnings("rawtypes")
	public List<CachedResultSet> doGetBillingDetails(String ccAuthProvider) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doGetBillingDetails");
        }
        List<CachedResultSet> oList = new ArrayList<CachedResultSet>();
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.setStatementID("GET_BILLING_DETAILS");
            dataRequest.addInputParam("IN_CC_AUTH_PROVIDER", ccAuthProvider);
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            Map outputs = (Map) dataAccessUtil.execute(dataRequest);
            dataRequest.reset();
            oList.add((CachedResultSet)outputs.get("OUT_DETAILS"));
            oList.add((CachedResultSet)outputs.get("OUT_BATCH_NUMBERS"));
        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting doGetBillingDetails");
            } 
        }
        return oList;  
    }


   /**
    * This method calls CLEAN.XXX_PKG.GET_ADD_BILL to retrieve the 
    * add_bill_object and sum up shipping_tax and tax.
    * @param recipient_id long
    * @return double
    * @throws java.lang.Exception
    */
    public double doGetAddBillTax(long payment_id) throws Exception
    {
        
        double salesTax = 0;
        
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doGetAddBillTax");
        }
        
        try{
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("GET_ADD_BILL");
        dataRequest.addInputParam("IN_PAYMENT_ID", new Long(payment_id));
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        logger.debug("Calling GET_ADD_BILL: " + payment_id);
        ResultSet rs =  (ResultSet)dataAccessUtil.execute(dataRequest);
        dataRequest.reset();
        
        if (rs.next())
            salesTax = rs.getDouble("shipping_tax")+rs.getDouble("tax");
        
        if(rs!=null) {
            rs.getStatement().close();
            rs.close();
        }
        
        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting doGetAddBillTax");
            } 
        }
        return salesTax;
    }
    
   /**
    * This method calls CLEAN.XXX_PKG.SET_PROCESSED_INDICATOR to set the 
    * completion_status of the billing_header.
    * @param batchNumber
    * @param status
    * @throws java.lang.Exception
    */
    public void doUpdateProcessedIndicator(String batchNumbers, String inStatus) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doUpdateProcessedIndicator");
        }
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.setStatementID("UPDATE_BILLING_HEADER");
            dataRequest.addInputParam("IN_BILLING_HEADER_IDS", batchNumbers);
            dataRequest.addInputParam("IN_PROCESSED_INDICATOR", inStatus);
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            
            logger.debug("Calling UPDATE_BILLING_HEADER: " + batchNumbers);
            Map outputs = (Map) dataAccessUtil.execute(dataRequest);     
            dataRequest.reset();
            String status = (String) outputs.get(ARConstants.STATUS_PARAM);
            if(status != null && status.equals(ARConstants.COMMON_VALUE_NO))
            {
                String message = (String) outputs.get(ARConstants.MESSAGE_PARAM);
                throw new SQLException(message);
            }
            if(logger.isDebugEnabled()){
                logger.debug("Updated Order Details. Status="+status);  
            }
        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting doUpdateProcessedIndicator");
            } 
        }
    }
    
   /**
    * 
    * @param masterOrderNumber String
    * @return long
    * @throws java.lang.Exception
    */
    public long doGetOrderDetailId(String masterOrderNumber) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doGetOrderDetailId");
        }
        long detailID = 0;
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.setStatementID("FIND_ORDER_NUMBER");
            dataRequest.addInputParam("IN_ORDER_NUMBER", masterOrderNumber);
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            
            logger.debug("Calling find_order_number: " + masterOrderNumber);
            Map outputs = (Map) dataAccessUtil.execute(dataRequest);  
            dataRequest.reset();
            detailID = (Long.valueOf(outputs.get("OUT_ORDER_DETAIL_ID").toString())).longValue();
        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting doGetOrderDetailId");
            } 
        }
        return detailID;
    }
    
  /**
   * The procedure mark all vendor-delivered order 'Delivered' with a ship date prior or on the
   * specified date and order_disp_code=Shipped. It also updates florist-delivered 'Delivered'
   * if delivery date is prior or on the specified date and order_disp_code=Processed.
   * It sets the 'Delivered' status by setting the eod_delivered_indicator and captures 
   * delivery_date to eod_delivery_date.
   * 
   * @param timeStamp
   * @param toStatus
   * @throws java.lang.Exception
   */
    public void doSetEODDeliveredInd(long timeStamp, String ccAuthProvider) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doSetEODDeliveredInd");
        }
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.setStatementID("SET_EOD_DELIVERY_FLAG");
            dataRequest.addInputParam("IN_DATE", new Timestamp(timeStamp));
            dataRequest.addInputParam("IN_CC_AUTH_PROVIDER", ccAuthProvider);
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            
            Map outputs = (Map) dataAccessUtil.execute(dataRequest);     
            dataRequest.reset();
            String status = (String) outputs.get(ARConstants.STATUS_PARAM);
            if(status != null && status.equals(ARConstants.COMMON_VALUE_NO))
            {
                String message = (String) outputs.get(ARConstants.MESSAGE_PARAM);
                if(message != null && message.startsWith("WARNING")) {
                    //no orders were updated.
                    logger.debug(message);
                } else {
                    throw new SQLException(message);
                }
            }
            if(logger.isDebugEnabled()){
                logger.debug("Updated eod delivery flag: Status="+status);  
            }
        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting doUpdateVendorFlag");
            } 
        }
    }    
    
  /**
   * The procedure retrieves all gift certs that have been put in the 'Refund'
   * status by the refund screen 'Reinstate' button. It updates all such 
   * gift certs to 'Reinstate' status and creates a reinstate history
   * in the gc_coupon_history table. 
   * 
   * @param timeStamp
   * @param fromStatus
   * @throws java.lang.Exception
   */
    public void doEodReinstateGcc() throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doEodReinstateGcc");
        }
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.setStatementID("EOD_REINSTATE_GCC");
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            
            Map outputs = (Map) dataAccessUtil.execute(dataRequest);     
            dataRequest.reset();
            String status = (String) outputs.get(ARConstants.STATUS_PARAM);
            
            if(status != null && status.equals(ARConstants.COMMON_VALUE_NO))
            {
                String message = (String) outputs.get(ARConstants.MESSAGE_PARAM);
                logger.error(message);
            }

        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting doEodReinstateGcc");
            } 
        }
    }  
    /**
   * Checks if sum of billed payments minus sum of billed refunds is
   * greater than the refund amount attempting to bill for the given cart.
   * @param payment_id
   * @return 'Y' if shuuld be billed; 'N' if should not bill.
   * @throws java.lang.Exception
   */
    public double shouldRefundBeBilled(long payment_id) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering shouldRefundBeBilled");
        }
        double balance = 0;
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.setStatementID("SHOULD_REFUND_BE_BILLED");
            dataRequest.addInputParam("IN_DEBIT_PAYMENT_ID", new Long(payment_id));
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            
            logger.debug("Calling SHOULD_REFUND_BE_BILLED: " + payment_id);
            balance = ((BigDecimal)dataAccessUtil.execute(dataRequest)).doubleValue();
            dataRequest.reset();

        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting shouldRefundBeBilled");
            } 
        }
        return balance;
    }    
    
  /**
   * Checks if the original payment for the add bill has been billed.
   * @param payment_id
   * @return 'Y' if shuuld be billed; 'N' if should not bill.
   * @throws java.lang.Exception
   */
    public String shouldAddBillBeBilled(long payment_id) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering shouldAddBillBeBilled");
        }
        String result = "";
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.setStatementID("SHOULD_ADD_BILL_BE_BILLED");
            dataRequest.addInputParam("IN_ADD_BILL_PAYMENT_ID", new Long(payment_id));
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            
            logger.debug("Calling SHOULD_ADD_BILL_BE_BILLED: " + payment_id);
            result = (String)dataAccessUtil.execute(dataRequest);
            dataRequest.reset();

        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting shouldAddBillBeBilled");
            } 
        }
        return result;
    }  
    
  /**
   * The procedure udpates the payment credit amount for the shopping carts that are ready
   * to be billed because of removed orders.
   * 
   * @param timeStamp
   * @param fromStatus
   * @throws java.lang.Exception
   */
    public void doUpdateRemovedOrderPayment(Calendar inDate, String payload) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doUpdateRemovedOrderPayment : payload: " + payload);
        }
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.setStatementID("UPDATE_REMOVED_ORDER_PAYMENT");
            dataRequest.addInputParam("IN_DATE", new Timestamp(inDate.getTimeInMillis()));
            dataRequest.addInputParam("IN_CC_AUTH_PROVIDER", payload);
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            
            Map outputs = (Map) dataAccessUtil.execute(dataRequest);     
            dataRequest.reset();
            String status = (String) outputs.get(ARConstants.STATUS_PARAM);
            
            if(status != null && status.equals(ARConstants.COMMON_VALUE_NO))
            {
                String message = (String) outputs.get(ARConstants.MESSAGE_PARAM);
                throw new SQLException(message);
            }
            
            if(logger.isDebugEnabled()){
                logger.debug("doUpdateRemovedOrderPayment: Status="+status);  
            }
        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting doUpdateRemovedOrderPayment");
            } 
        }
    }     
    
    /**
    * This method retrieves 'Y' or 'N' if the EOD process has started 
    * for today. It returns a null reference if it has not started. 
    * 
    * @param today Date
    * @return BillingHeaderVO
    * @throws java.lang.Exception
    */
    public BillingHeaderVO doGetProcessedIndicator(Calendar today, String ccAuthProvider) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doGetProcessedIndicator");
        }
        Map result = null;
        BillingHeaderVO billingVo = null;
        
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.setStatementID("GET_PROCESSED_INDICATOR");
            dataRequest.addInputParam("IN_DATE", new Date(today.getTimeInMillis()));
            dataRequest.addInputParam("IN_CC_AUTH_PROVIDER", ccAuthProvider);
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            
            logger.debug("Calling GET_CUST_NAME_BY_PAYMENT_ID: " + today);
            result = (Map) dataAccessUtil.execute(dataRequest);
            dataRequest.reset();
 
            billingVo = new BillingHeaderVO((String) result.get("OUT_PROCESS_IND"),
                                    (BigDecimal)result.get("OUT_BILLING_HEADER_ID"));

        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting doGetProcessedIndicator");
            } 
        }
        return billingVo;
    }
    
  /**
   * This method calls the procedure CREATE_HEAD_PERS_RECOV_REC. This kicks off 
   * the first phase of the EOD process which creates the header, adds unbilled
   * shopping carts that contain only CC payments into the BILLING_DETAIL table
   * for billing, updates the unbilled shopping carts that contain only CC 
   * payments as billed, updates bills that should not be charged as billed, and 
   * copies all remaining unbilled orders to a staging table for processing. 
   * 
   * This procedure commits changes to the database upon completion.
   * 
   * @param today Date
   * @throws java.lang.Exception
   */
    public long doCreateHeadPersRecovRec(Calendar today, String ccAuthProvider) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doCreateHeadPersRecovRec");
        }
        long batchNumber = 0;
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.setStatementID("CREATE_HEAD_PERS_RECOV_REC");
            dataRequest.addInputParam("IN_BILLING_BATCH_DATE", new Date(today.getTimeInMillis()));
            dataRequest.addInputParam("IN_CC_AUTH_PROVIDER", ccAuthProvider);
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            
            Map outputs = (Map) dataAccessUtil.execute(dataRequest);     
            dataRequest.reset();
            String status = (String) outputs.get(ARConstants.STATUS_PARAM);
            String message = (String) outputs.get(ARConstants.MESSAGE_PARAM);
            if(status != null && status.equals(ARConstants.COMMON_VALUE_NO))
            {
                if(message != null && message.startsWith("WARNING")) {
                    logger.debug(message);
                } else {
                    throw new SQLException(message);
                }
            } else if (ARConstants.COMMON_VALUE_YES.equals(status)) {
                batchNumber = Long.parseLong(outputs.get("OUT_SEQUENCE_NUMBER").toString());
            } else {
                throw new SQLException(message);
            }
            return batchNumber;
        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting doCreateHeadPersRecovRec");
            } 
        }
    }
    
  /**
   * This method deletes the recovery table used for recovery from a double
   * run of EOD.
   * 
   * @throws java.lang.Exception
   */
    public void doDeleteRecoveryTab(String ccAuthProvider) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doDeleteRecoveryTab");
        }
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.addInputParam("IN_CC_AUTH_PROVIDER", ccAuthProvider);
            dataRequest.setStatementID("DELETE_RECOVERY_TAB");
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

            Map outputs = (Map) dataAccessUtil.execute(dataRequest);     
            dataRequest.reset();
            String status = (String) outputs.get(ARConstants.STATUS_PARAM);

            if(status != null && status.equals(ARConstants.COMMON_VALUE_NO))
            {
                String message = (String) outputs.get(ARConstants.MESSAGE_PARAM);
                if(message != null && message.startsWith("WARNING")) {
                    logger.debug(message);
                } else {
                    throw new SQLException(message);
                }
            }
        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting doDeleteRecoveryTab");
            } 
        }
    }
    
    public CachedResultSet doGetRecoveryRefunds() throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doGetRecoveryRefunds");
        }
        
        CachedResultSet rs = null;
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.setStatementID("GET_UNPROCESS_RECOVERY_REFUND");
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
            dataRequest.reset();
        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting doGetRecoveryRefunds");
            } 
        } 
        return  rs;
    }
    
  /**
   * Retrieves two list of EODPaymentVO from the recovery table. One for payments
   * and one for refunds. Both lists are sorted by master order number.
   * @return 
   * @throws java.lang.Exception
   */
    public Map doGetRecoveryCarts(String ccAuthProvider) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doGetRecoveryCarts");
        }
        CachedResultSet prs = null;
        CachedResultSet rrs = null;
        String masterOrderNumber = null;
        String paymentId = null;
        String paymentInd = null;
        EODPaymentVO paymentvo = null;
        ArrayList paymentList = new ArrayList();
        ArrayList refundList = new ArrayList();
        Map outmap = null;
        Map cartMap = new HashMap();
        
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.addInputParam("IN_CC_AUTH_PROVIDER", ccAuthProvider);
            dataRequest.setStatementID("GET_UNPROC_RECOVERY_CARTS");
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            
            outmap = (Map)dataAccessUtil.execute(dataRequest);
            dataRequest.reset();
            prs = (CachedResultSet)outmap.get("OUT_PAYMENT_CURSOR");
            rrs = (CachedResultSet)outmap.get("OUT_REFUND_CURSOR");
            
            while(prs.next()) {
                masterOrderNumber = prs.getString("master_order_number");
                paymentId = prs.getString("payment_id");
                paymentInd = prs.getString("payment_indicator");
                paymentvo = new EODPaymentVO(masterOrderNumber, paymentId,paymentInd);
                paymentList.add(paymentvo);
            }
            while(rrs.next()) {
                masterOrderNumber = rrs.getString("master_order_number");
                paymentId = rrs.getString("payment_id");
                paymentInd = rrs.getString("payment_indicator");
                paymentvo = new EODPaymentVO(masterOrderNumber, paymentId,paymentInd);
                refundList.add(paymentvo);
            }        
            
            cartMap.put("PAYMENT", paymentList);
            cartMap.put("REFUND", refundList);
            
        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting doGetRecoveryCarts");
            } 
        } 
        return  cartMap;
    }
    
  /**
   * This method deletes the recovery table for the given master order number
   * 
   * @throws java.lang.Exception
   */
    public void doDeleteRecoveryCart(String masterOrderNumber) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doDeleteRecoveryCart");
        }
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.addInputParam("IN_MASTER_ORDER_NUMBER", masterOrderNumber);
            dataRequest.setStatementID("DELETE_PAYMENT_RECOVERY_CART");
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

            Map outputs = (Map) dataAccessUtil.execute(dataRequest);     
            dataRequest.reset();
            String status = (String) outputs.get(ARConstants.STATUS_PARAM);

            if(status != null && status.equals(ARConstants.COMMON_VALUE_NO))
            {
                String message = (String) outputs.get(ARConstants.MESSAGE_PARAM);
                if(message != null && message.startsWith("WARNING")) {
                    logger.debug(message);
                } else {
                    throw new SQLException(message);
                }
            }
        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting doDeleteRecoveryCart");
            } 
        }
    }
    
  /**
   * This method deletes the recovery table for the given master order number
   * 
   * @throws java.lang.Exception
   */
    public int doCountRecoveryRec(String ccAuthProvider) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doCountRecoveryRec");
        }
        int c = 0;
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.addInputParam("IN_CC_AUTH_PROVIDER", ccAuthProvider);
            dataRequest.setStatementID("COUNT_PAYMENT_RECOVERY_REC");
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

            BigDecimal count = (BigDecimal) dataAccessUtil.execute(dataRequest);  
            c = count.intValue();
            dataRequest.reset();
            
        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting doCountRecoveryRec");
            } 
        }
        return c;
    }    

  /**
   * This method updates the dispatched_indicator to 'Y' in the recovery table for 
   * the given master order number
   * 
   * @throws java.lang.Exception
   */
    public void doUpdateCartDispatched(String masterOrderNumber) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doUpdateCartDispatched");
        }
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.addInputParam("IN_MASTER_ORDER_NUMBER", masterOrderNumber);
            dataRequest.setStatementID("UPDATE_CART_DISPATCHED");
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

            Map outputs = (Map) dataAccessUtil.execute(dataRequest);     
            dataRequest.reset();
            String status = (String) outputs.get(ARConstants.STATUS_PARAM);

            if(status != null && status.equals(ARConstants.COMMON_VALUE_NO))
            {
                String message = (String) outputs.get(ARConstants.MESSAGE_PARAM);
                if(message != null && message.startsWith("WARNING")) {
                    logger.debug(message);
                } else {
                    throw new SQLException(message);
                }
            }
        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting doUpdateCartDispatched");
            } 
        }
    }
    
    public CachedResultSet doGetPaymentExtByPaymentId(long paymentId) throws Exception
    {
    	if(logger.isDebugEnabled()) {
            logger.debug("doGetPaymentExtByPaymentId");
        }
    	CachedResultSet rs;
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.addInputParam("IN_PAYMENT_ID", paymentId);
            dataRequest.setStatementID("GET_PAYMENT_EXT_BY_ID");
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            
            logger.debug("Calling GET_PAYMENT_EXT_BY_ID: " + paymentId);
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
            dataRequest.reset();
        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting doGetPaymentExtByPaymentId");
            } 
        }
    	return rs;
    }
    
    public String doCreateSettlementTotals(String submissionId, String julianDate, double paymentTotal, double refundTotal, long billingHeaderId, String settlementStatus, String fileName) throws Exception
    {
    	String retValue = null;
    	try{
    		DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.addInputParam("IN_SUBMISSION_ID", new Long(submissionId));
            dataRequest.addInputParam("IN_JULIAN_DATE", new Long(julianDate));
            dataRequest.addInputParam("IN_PAYMENT_TOTAL", paymentTotal);
            dataRequest.addInputParam("IN_REFUND_TOTAL", refundTotal);
            dataRequest.addInputParam("IN_BILLING_HEADER_ID", billingHeaderId);
            dataRequest.addInputParam("IN_SETTLEMENT_STATUS", settlementStatus);
            dataRequest.addInputParam("IN_FILE_NAME", fileName);
            
            dataRequest.setStatementID("INSERT_SETTLEMENT_TOTALS");
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            
            Map outputs = (Map) dataAccessUtil.execute(dataRequest);  
            dataRequest.reset();
            String status = (String) outputs.get(ARConstants.STATUS_PARAM);
            if(status != null && status.equals(ARConstants.COMMON_VALUE_NO))
            {
                String message = (String) outputs.get(ARConstants.MESSAGE_PARAM);
                throw new SQLException(message);
            }
            if(logger.isDebugEnabled()){
                logger.debug("Insert on Settlement Totals successfull. Status="+ status);
            }
            retValue = outputs.get("OUT_SETTLEMENT_TOTALS_ID").toString(); 
    	}finally{
    		
    	}
    	return retValue;
    }
    
    
    public CachedResultSet getSettlementTotals(String submissionId, String julianDate) throws Exception
    {
    	if(logger.isDebugEnabled()) {
            logger.debug("Entering getSettlementTotals");
        }
        
        CachedResultSet rs = null;
        if(submissionId == null || julianDate == null){
        	return rs;
        }
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.setStatementID("GET_SETTLEMENT_TOTALS");
            dataRequest.addInputParam("IN_SUBMISSION_ID", new Long(submissionId));
            dataRequest.addInputParam("IN_JULIAN_DATE", new Long(julianDate));
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    
            
            Map outputs = (Map) dataAccessUtil.execute(dataRequest);
            dataRequest.reset();
            rs = (CachedResultSet) outputs.get("OUT_CURSOR");
            
        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting getSettlementTotals");
            } 
        } 
        return  rs;
    }
    
    public CachedResultSet getSettlementDetailsByDate(String julianDate) throws Exception
    {
    	if(logger.isDebugEnabled()) {
            logger.debug("Entering getSettlementDetailsByDate");
        }
        
        CachedResultSet rs = null;
        if(julianDate == null){
        	return rs;
        }
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.setStatementID("GET_SETTLEMENT_DETAILS");
            dataRequest.addInputParam("IN_JULIAN_DATE", new Long(julianDate));
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    
            
            Map outputs = (Map) dataAccessUtil.execute(dataRequest);
            dataRequest.reset();
            rs = (CachedResultSet) outputs.get("OUT_CURSOR");
            
        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting getSettlementDetailsByDate");
            } 
        } 
        return  rs;
    }
    
    public boolean updateSettlementTotalsStatus(String submissionId, String julianDate, String settlementStatus) throws Exception
    {
    	if(logger.isDebugEnabled()) {
            logger.debug("Entering updateSettlementTotalsStatus");
        }
        String status = "";
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.setStatementID("UPDATE_SETTLEMENT_TOTALS");
            dataRequest.addInputParam("IN_SUBMISSION_ID", new Long(submissionId));
            dataRequest.addInputParam("IN_JULIAN_DATE", new Long(julianDate));
            dataRequest.addInputParam("IN_SETTLEMENT_STATUS", settlementStatus);
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            Map outputs = (Map) dataAccessUtil.execute(dataRequest);   
            dataRequest.reset();
            status = (String) outputs.get(ARConstants.STATUS_PARAM);
            if(status != null && status.equals(ARConstants.COMMON_VALUE_NO))
            {
                String message = (String) outputs.get(ARConstants.MESSAGE_PARAM);
                throw new SQLException(message);
            }
            if(logger.isDebugEnabled()){
                logger.debug("Update on Settlement Totals successfull. Status="+status);
            }

        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting updateSettlementTotalsStatus");
            } 
        } 
        return (status).equals(ARConstants.COMMON_VALUE_YES)? true:false;
    }
    
    public void doUpdateOrderBills(String companyId, String submissionId, String julianDate, String refNums) throws Exception
    {
    	if(logger.isDebugEnabled()) {
            logger.debug("Entering updateOrderBills");
        }
    	String status = "";
    	try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.setStatementID("UPDATE_ORDER_BILLS");
            dataRequest.addInputParam("IN_SUBMISSION_ID", new Long(submissionId));
            dataRequest.addInputParam("IN_JULIAN_DATE", new Long(julianDate));
            dataRequest.addInputParam("IN_COMPANY_ID", companyId);
            dataRequest.addInputParam("IN_REF_NUMS", refNums);
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            Map outputs = (Map) dataAccessUtil.execute(dataRequest);   
            dataRequest.reset();
            status = (String) outputs.get(ARConstants.STATUS_PARAM);
            if(status != null && status.equals(ARConstants.COMMON_VALUE_NO))
            {
                String message = (String) outputs.get(ARConstants.MESSAGE_PARAM);
                throw new SQLException(message);
            }
            

        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting updateOrderBills");
            } 
        } 
    }
    
    public CachedResultSet getSettlementFileToSend(String companyId) throws Exception
    {
    	if(logger.isDebugEnabled()) {
            logger.debug("Entering getSettlementFileToSend");
        }
        
        CachedResultSet rs = null;
        
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.setStatementID("GET_SETTLEMENT_FILE_TO_SEND");
            dataRequest.addInputParam("IN_COMPANY_ID", companyId);
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
                
            Map outputs = (Map) dataAccessUtil.execute(dataRequest);
            dataRequest.reset();
            rs = (CachedResultSet) outputs.get("OUT_CURSOR");
            
        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting getSettlementFileToSend");
            } 
        } 
        return  rs;
    }
    
    public boolean updateSettlementTotalsStatusByFilename(String fileName, String settlementStatus) throws Exception
    {
    	if(logger.isDebugEnabled()) {
            logger.debug("Entering updateSettlementTotalsStatusByFilename(String fileName, String settlementStatus)");
        }
        String status = "";
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.setStatementID("UPDATE_SETTLEMENT_TOTALS_BY_FILENAME");
            dataRequest.addInputParam("IN_FILE_NAME", fileName);
            dataRequest.addInputParam("IN_SETTLEMENT_STATUS", settlementStatus);
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            Map outputs = (Map) dataAccessUtil.execute(dataRequest);   
            dataRequest.reset();
            status = (String) outputs.get(ARConstants.STATUS_PARAM);
            if(status != null && status.equals(ARConstants.COMMON_VALUE_NO))
            {
                String message = (String) outputs.get(ARConstants.MESSAGE_PARAM);
                throw new SQLException(message);
            }
            if(logger.isDebugEnabled()){
                logger.debug("Update on Settlement Totals successful. Status="+status);
            }

        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting updateSettlementTotalsStatusByFilename");
            } 
        } 
        return (status).equals(ARConstants.COMMON_VALUE_YES)? true:false;
    }
    
    public CachedResultSet getSettlementFileToArchive(String companyId) throws Exception
    {
    	if(logger.isDebugEnabled()) {
            logger.debug("Entering getSettlementFileToArchive");
        }
        
        CachedResultSet rs = null;
        
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.setStatementID("GET_SETTLEMENT_FILE_TO_ARCHIVE");
            dataRequest.addInputParam("IN_COMPANY_ID", companyId);
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
                
            Map outputs = (Map) dataAccessUtil.execute(dataRequest);
            dataRequest.reset();
            rs = (CachedResultSet) outputs.get("OUT_CURSOR");
            
        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting getSettlementFileToArchive");
            } 
        } 
        return  rs;
    }
    
    public boolean updateArchiveStatus(String fileName) throws Exception
    {
    	if(logger.isDebugEnabled()) {
            logger.debug("Entering updateArchiveStatus");
        }
        String status = "";
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.setStatementID("UPDATE_ARCHIVE_STATUS");
            dataRequest.addInputParam("IN_FILE_NAME", fileName);
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            Map outputs = (Map) dataAccessUtil.execute(dataRequest);   
            dataRequest.reset();
            status = (String) outputs.get(ARConstants.STATUS_PARAM);
            if(status != null && status.equals(ARConstants.COMMON_VALUE_NO))
            {
                String message = (String) outputs.get(ARConstants.MESSAGE_PARAM);
                throw new SQLException(message);
            }
            if(logger.isDebugEnabled()){
                logger.debug("Update archive status on Settlement Totals successful. Status="+status);
            }

        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting updateArchiveStatus");
            } 
        } 
        return (status).equals(ARConstants.COMMON_VALUE_YES)? true:false;
    }
    
    /**
     * This method deletes the settlement totals records by billing header id
     * 
     * @throws java.lang.Exception
     */
      public void deleteSettlementTotals(long billingHeaderId) throws Exception
      {
          if(logger.isDebugEnabled()) {
              logger.debug("Entering doDeleteRecoveryTab");
          }
          try{
              DataRequest dataRequest = new DataRequest();
              dataRequest.setConnection(this.connection);
              dataRequest.addInputParam("IN_BILLING_HEADER_ID", billingHeaderId);
              dataRequest.setStatementID("DELETE_SETTLEMENT_TOTALS");
              DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

              Map outputs = (Map) dataAccessUtil.execute(dataRequest);     
              dataRequest.reset();
              String status = (String) outputs.get(ARConstants.STATUS_PARAM);

              if(status != null && status.equals(ARConstants.COMMON_VALUE_NO))
              {
                  String message = (String) outputs.get(ARConstants.MESSAGE_PARAM);
                  if(message != null && message.startsWith("WARNING")) {
                      logger.debug(message);
                  } else {
                      throw new SQLException(message);
                  }
              }
          } finally {
              if(logger.isDebugEnabled()){
                  logger.debug("Exiting deleteSettlementTotals");
              } 
          }
      }

	public Map<String,List<EODPaymentVO>> doGetPGCarts(String paymentType,Calendar batchDate) throws SAXException, ParserConfigurationException, IOException, SQLException 
	{
		if (logger.isDebugEnabled()) {
			logger.debug("Entering doGetPGRecoveryCarts");
		}
		CachedResultSet prs = null;
		CachedResultSet rrs = null;
		String masterOrderNumber = null;
		String paymentId = null;
		String paymentInd = null;
		EODPaymentVO paymentvo = null;
		List<EODPaymentVO> paymentList = new ArrayList<EODPaymentVO>();
		List<EODPaymentVO> refundList =  new ArrayList<EODPaymentVO>();
		Map outmap = null;
		Map<String, List<EODPaymentVO>> cartMap = new HashMap<String, List<EODPaymentVO>>();

		try {
			DataRequest dataRequest = new DataRequest();
			
			dataRequest.setConnection(this.connection);
			dataRequest.addInputParam("IN_PAYMENT_TYPE", paymentType);
			java.sql.Date sqlBillingDate = new java.sql.Date(batchDate.getTimeInMillis());//TODO
			dataRequest.addInputParam("IN_BILLING_BATCH_DATE", sqlBillingDate);
			dataRequest.setStatementID("GET_PG_UNPROC_RECOVERY_CARTS");
			
			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			
			outmap = (Map) dataAccessUtil.execute(dataRequest);
			dataRequest.reset();
			prs = (CachedResultSet) outmap.get("OUT_PAYMENT_CURSOR");
			rrs = (CachedResultSet) outmap.get("OUT_REFUND_CURSOR");
			
			while (prs.next()) {
				masterOrderNumber = prs.getString("master_order_number");
				paymentId = prs.getString("payment_id");
				paymentInd = prs.getString("payment_indicator");
				paymentvo = new EODPaymentVO(masterOrderNumber, paymentId, paymentInd);
				paymentList.add(paymentvo);
			}
			while (rrs.next()) {
				masterOrderNumber = rrs.getString("master_order_number");
				paymentId = rrs.getString("payment_id");
				paymentInd = rrs.getString("payment_indicator");
				paymentvo = new EODPaymentVO(masterOrderNumber, paymentId, paymentInd);
				refundList.add(paymentvo);
			}

			cartMap.put("PAYMENT", paymentList);
			cartMap.put("REFUND", refundList);

		} finally {
			if (logger.isDebugEnabled()) {
				logger.debug("Exiting doGetPGRecoveryCarts");
			}
		}
		return cartMap;

	}
	
	/**
	    * This method retrieves 'Y' or 'N' if the EOD process has started 
	    * for today. It returns a null reference if it has not started. 
	    * 
	    * @param today Date
	    * @return BillingHeaderPGVO
	    * @throws java.lang.Exception
	    */
	    public BillingHeaderPGVO doGetPGProcessedIndicator(Calendar today, String paymentType) throws Exception
	    {
	        if(logger.isDebugEnabled()) {
	            logger.debug("Entering doGetPGProcessedIndicator");
	        }
	        Map result = null;
	        BillingHeaderPGVO billingVo = null;
	        
	        try{
	            DataRequest dataRequest = new DataRequest();
	            dataRequest.setConnection(this.connection);
	            dataRequest.setStatementID("GET_PG_PROCESSED_INDICATOR");
	            dataRequest.addInputParam("IN_DATE", new Date(today.getTimeInMillis()));
	            dataRequest.addInputParam("IN_PAYEMNT_TYPE", paymentType);
	            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	            
	            logger.debug("Calling GET_PG_PROCESSED_INDICATOR: " + today);
	            result = (Map) dataAccessUtil.execute(dataRequest);
	            dataRequest.reset();
	 
	            billingVo = new BillingHeaderPGVO((String) result.get("OUT_PROCESS_IND"),(BigDecimal)result.get("OUT_BILLING_HEADER_PG_ID"));

	        } finally {
	            if(logger.isDebugEnabled()){
	                logger.debug("Exiting doGetPGProcessedIndicator");
	            } 
	        }
	        return billingVo;
	    }
	
	    /**
	     * This method calls the procedure CREATE_PG_HEAD_PERS_RECOV_REC. This kicks off 
	     * the first phase of the EOD process which creates the header, adds unbilled
	     * shopping carts that contain only CC payments into the BILLING_DETAIL_PG_CC table
	     * for billing, updates the unbilled shopping carts that contain only CC 
	     * payments as billed, updates bills that should not be charged as billed, and 
	     * copies all remaining unbilled orders to a staging table for processing. 
	     * 
	     * This procedure commits changes to the database upon completion.
	     * 
	     * @param today Date
	     * @throws java.lang.Exception
	     */
	      public long doCreatePGHeadPersRecovRec(Calendar today, String paymentType) throws Exception
	      {
	          if(logger.isDebugEnabled()) {
	              logger.debug("Entering doCreatePGHeadPersRecovRec");
	          }
	          long batchNumber = 0;
	          try{
	              DataRequest dataRequest = new DataRequest();
	              dataRequest.setConnection(this.connection);
	              dataRequest.setStatementID("CREATE_BILLING_HEADER_PG");
	              dataRequest.addInputParam("IN_BILLING_BATCH_DATE", new Date(today.getTimeInMillis()));
	              dataRequest.addInputParam("IN_PAYMENT_TYPE", paymentType);
	              DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	              
	              Map outputs = (Map) dataAccessUtil.execute(dataRequest);     
	              dataRequest.reset();
	              String status = (String) outputs.get(ARConstants.STATUS_PARAM);
	              String message = (String) outputs.get(ARConstants.MESSAGE_PARAM);
	              if(status != null && status.equals(ARConstants.COMMON_VALUE_NO))
	              {
	                  if(message != null && message.startsWith("WARNING")) {
	                      logger.debug(message);
	                  } else {
	                      throw new SQLException(message);
	                  }
	              } else if (ARConstants.COMMON_VALUE_YES.equals(status)) {
	                  batchNumber = Long.parseLong(outputs.get("OUT_SEQUENCE_NUMBER").toString());
	              } else {
	                  throw new SQLException(message);
	              }
	              return batchNumber;
	          } finally {
	              if(logger.isDebugEnabled()){
	                  logger.debug("Exiting doCreatePGHeadPersRecovRec");
	              } 
	          }
	      }
	      
	
	          
	public CachedResultSet doGetPGPaymentById(String paymentId) throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("Entering doGetPGPaymentById");
		}
		CachedResultSet rs = null;
		try {
			DataRequest dataRequest = new DataRequest();
			dataRequest.setConnection(this.connection);
			dataRequest.setStatementID("GET_PG_PAYMENT_BY_ID");
			dataRequest.addInputParam("IN_PAYMENT_ID", paymentId);
			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

			logger.debug("Calling GET_PG_PAYMENT_BY_ID: " + paymentId);
			rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
			dataRequest.reset();
		} finally {
			if (logger.isDebugEnabled()) {
				logger.debug("Exiting doGetPGPaymentById");
			}
		}
		return rs;
	}

	
	public String doCreatePGBillingDetail(PGEODRecordVO vo, long batchNumber) throws Exception{
		
		if(logger.isDebugEnabled()) {
            logger.debug("Entering doCreatePGBillingDetail");
        }
        String retValue = "";
        
        
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.connection);
            dataRequest.setStatementID("CREATE_BILLING_DETAIL_PG_CC");
            
            dataRequest.addInputParam("IN_BILLING_HEADER_ID", new Long(batchNumber));
            dataRequest.addInputParam("IN_ORDER_NUMBER", vo.getMasterOrderNumber());
            dataRequest.addInputParam("IN_PAYMENT_ID", new Long(vo.getPaymentId()));
            //Rounding should be done because of BigDecimal may give more than 2 decimals
            dataRequest.addInputParam("IN_ORDER_AMOUNT", BigDecimal.valueOf(vo.getOrderAmount()).setScale(2, BigDecimal.ROUND_DOWN).doubleValue());
            dataRequest.addInputParam("IN_TRANSACTION_TYPE", vo.getTransactionType());
            dataRequest.addInputParam("IN_BILL_TYPE", vo.getBillType());
            dataRequest.addInputParam("IN_IS_VOICE_AUTH", vo.getVoiceAuthFlag());
            dataRequest.addInputParam("IN_AUTH_TRANSACTION_ID", vo.getAuthTranId());
            dataRequest.addInputParam("IN_MERCHANT_REF_ID", vo.getMerchantRefId());
            dataRequest.addInputParam("IN_SETTLEMENT_TRANSACTION_ID", vo.getSettlmentTransactionId());
            dataRequest.addInputParam("IN_REFUND_TRANSACTION_ID", vo.getRefundTransactionId());
            dataRequest.addInputParam("IN_REQUEST_ID",vo.getRequestToken());
            
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
                    
            Map outputs = (Map) dataAccessUtil.execute(dataRequest); 
            
            dataRequest.reset();
            String status = (String) outputs.get(ARConstants.STATUS_PARAM);
            
            if(status != null && status.equals(ARConstants.COMMON_VALUE_NO))
            {
                String message = (String) outputs.get(ARConstants.MESSAGE_PARAM);
                throw new SQLException(message);
            }
            if(logger.isDebugEnabled()){
                logger.debug("Insert on Billing Detail PG CC successfull. Status=" 
                    + status);
            }
            retValue = outputs.get("OUT_SEQUENCE_NUMBER").toString(); 
            logger.debug(" retValue "+ retValue);
        }
        finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting doCreatePGBillingDetail");
            } 
        } 

        return retValue; 
		
	}
	
	/**
	   * This method updates the dispatched_indicator to 'Y' in the recovery table for 
	   * the given master order number
	   * 
	   * @throws java.lang.Exception
	   */
	/*    public void doUpdateCartDispatchedPG(String masterOrderNumber) throws Exception
	    {
	        if(logger.isDebugEnabled()) {
	            logger.debug("Entering doUpdateCartDispatchedPG");
	        }
	        try{
	            DataRequest dataRequest = new DataRequest();
	            dataRequest.setConnection(this.connection);
	            dataRequest.addInputParam("IN_MASTER_ORDER_NUMBER", masterOrderNumber);
	            dataRequest.setStatementID("UPDATE_CART_DISPATCHED_PG");
	            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

	            Map outputs = (Map) dataAccessUtil.execute(dataRequest);     
	            dataRequest.reset();
	            String status = (String) outputs.get(ARConstants.STATUS_PARAM);

	            if(status != null && status.equals(ARConstants.COMMON_VALUE_NO))
	            {
	                String message = (String) outputs.get(ARConstants.MESSAGE_PARAM);
	                if(message != null && message.startsWith("WARNING")) {
	                    logger.debug(message);
	                } else {
	                    throw new SQLException(message);
	                }
	            }
	        } finally {
	            if(logger.isDebugEnabled()){
	                logger.debug("Exiting doUpdateCartDispatchedPG");
	            } 
	        }
	    }*/

	/**
	 * This method gets count in recovery table for the given payment type
	 * 
	 * @throws java.lang.Exception
	 */
	public int doCountRecoveryRecPG(String paymentType) throws Exception {
		
		if (logger.isDebugEnabled()) {
			logger.debug("Entering doCountRecoveryRecPG");
		}
		
		int c = 0;
		try {
			DataRequest dataRequest = new DataRequest();
			dataRequest.setConnection(this.connection);
			dataRequest.addInputParam("IN_PAYMENT_TYPE", paymentType);
			dataRequest.setStatementID("COUNT_PAYMENT_RCVRY_REC_PG");
			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

			BigDecimal count = (BigDecimal) dataAccessUtil.execute(dataRequest);
			c = count.intValue();
			dataRequest.reset();

		} finally {
			if (logger.isDebugEnabled()) {
				logger.debug("Exiting doCountRecoveryRecPG");
			}
		}
		return c;
	}

	public CachedResultSet doGetBillingCCDetailsPG(long billingId,long headerId) throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("Entering doGetBillingCCDetailsPG");
		}
		CachedResultSet rs = null;
		try {
			DataRequest dataRequest = new DataRequest();
			dataRequest.setConnection(this.connection);
			dataRequest.setStatementID("GET_BILLING_DET_PG_CC_BY_ID");
			dataRequest.addInputParam("IN_BILLING_DETAIL_ID", billingId);
			dataRequest.addInputParam("IN_BILLING_HEADER_ID", headerId);
			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
			dataRequest.reset();
			
		} finally {
			if (logger.isDebugEnabled()) {
				logger.debug("Exiting doGetBillingCCDetailsPG");
			}
		}
		return rs;
	}

	
	 
	   /**
	    * This method calls CLEAN.XXX_PKG.SET_PROCESSED_INDICATOR to set the 
	    * completion_status of the billing_header.
	    * @param batchNumber
	    * @param status
	    * @throws java.lang.Exception
	    */
	    public void doUpdatePGProcessedIndicator(String batchNumbers, String inStatus) throws Exception
	    {
	        if(logger.isDebugEnabled()) {
	            logger.debug("Entering doUpdatePGProcessedIndicator");
	        }
	        try{
	            DataRequest dataRequest = new DataRequest();
	            dataRequest.setConnection(this.connection);
	            dataRequest.setStatementID("UPDATE_BILLING_HEADER_PG");
	            dataRequest.addInputParam("IN_BILLING_HEADER_IDS", batchNumbers);
	            dataRequest.addInputParam("IN_PROCESSED_INDICATOR", inStatus);
	            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	            
	            logger.debug("Calling UPDATE_BILLING_HEADER_PG: " + batchNumbers);
	            Map outputs = (Map) dataAccessUtil.execute(dataRequest);     
	            dataRequest.reset();
	            String status = (String) outputs.get(ARConstants.STATUS_PARAM);
	            if(status != null && status.equals(ARConstants.COMMON_VALUE_NO))
	            {
	                String message = (String) outputs.get(ARConstants.MESSAGE_PARAM);
	                throw new SQLException(message);
	            }
	            if(logger.isDebugEnabled()){
	                logger.debug("Updated doUpdatePGProcessedIndicator Details. Status="+status);  
	            }
	        } finally {
	            if(logger.isDebugEnabled()){
	                logger.debug("Exiting doUpdatePGProcessedIndicator");
	            } 
	        }
	    }
	    
	public void doUpdatePGSettlementResponse(SettlementResponse settlementResponse,PGEODRecordVO pgEODRecordVO) throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("Entering doUpdatePGSettlementResponse");
		}
		try {
			DataRequest dataRequest = new DataRequest();
			dataRequest.setConnection(this.connection);
			dataRequest.setStatementID("UPDATE_BILLING_DTL_PG_CC_RES");
			dataRequest.addInputParam("IN_AUTH_TRANS_ID", pgEODRecordVO.getAuthTranId());
			dataRequest.addInputParam("IN_ORDER_NUMBER", pgEODRecordVO.getMasterOrderNumber());
			dataRequest.addInputParam("IN_BILLING_DETAIL_ID", pgEODRecordVO.getBillingDetailId());
			dataRequest.addInputParam("IN_PAYMENT_ID", pgEODRecordVO.getPaymentId());
			dataRequest.addInputParam("IN_REQUEST_ID", settlementResponse.getRequestId());
			
			if(pgEODRecordVO.getTransactionType().equals("1"))
			{	
				dataRequest.addInputParam("IN_SETTLEMENT_TRANSACTION_ID", settlementResponse.getSettlementTransactionId());
				dataRequest.addInputParam("IN_REQUEST_TOKEN", settlementResponse.getRequestToken());
			}
			
			if(pgEODRecordVO.getTransactionType().equals("3"))
			{	
				dataRequest.addInputParam("IN_SETTLEMENT_TRANSACTION_ID", pgEODRecordVO.getSettlmentTransactionId()); //Existing Settlement ID
				dataRequest.addInputParam("IN_REFUND_TRANSACTION_ID", settlementResponse.getSettlementTransactionId());//Refund Transaction ID
			}	
						
			dataRequest.addInputParam("IN_RESPONSE_MESSAGE", settlementResponse.getMessage());
			dataRequest.addInputParam("IN_STATUS", settlementResponse.getStatus());
			dataRequest.addInputParam("IN_RESPONSE_CODE",settlementResponse.getReasonCode());
			dataRequest.addInputParam("IN_SETTLEMENT_DATE",settlementResponse.getSettlementDate());
			dataRequest.addInputParam("IN_PROCESS_ACCOUNT",settlementResponse.getProcessAccount());
			
			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

			Map outputs = (Map) dataAccessUtil.execute(dataRequest);
			dataRequest.reset();
			String status = (String) outputs.get(ARConstants.STATUS_PARAM);
			if (status != null && status.equals(ARConstants.COMMON_VALUE_NO)) {
				String message = (String) outputs.get(ARConstants.MESSAGE_PARAM);
				throw new SQLException(message);
			}
			if (logger.isDebugEnabled()) {
				logger.debug("Updated doUpdatePGSettlementResponse Details. Status=" + status);
			}

		} finally {
			if (logger.isDebugEnabled()) {
				logger.debug("Exiting doUpdatePGSettlementResponse");
			}
		}
	}
	
	 public String doGetOrderGuid(String masterOrderNumber) throws Throwable
	    {
	        if(logger.isDebugEnabled()) {
	            logger.debug("Entering doGetOrderGuid");
	        }
	        String orderGuid = null;
	        try{
	            DataRequest dataRequest = new DataRequest();
	            dataRequest.setConnection(this.connection);
	            dataRequest.setStatementID("GET_ORDER_GUID");
	            dataRequest.addInputParam("IN_MASTER_ORDER_NUMBER", masterOrderNumber);
	            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	            
	            logger.info("master_order_number=" + masterOrderNumber);
	            orderGuid = ((String)dataAccessUtil.execute(dataRequest));
	            dataRequest.reset();
	            logger.debug("Exiting doGetOrderGuid");
	        } finally {
	        }
	        return orderGuid;
	    }

	
	public void doUpdateDispatchedCount(long batchNumber, int dispatchedCount) throws Exception {
		logger.debug("Entering doUpdateDispatchedCount");

		DataRequest request = new DataRequest();
		String status = "";
		try {
			/* setup store procedure input parameters */
			Map inputParams = new HashMap();
			inputParams.put("IN_BILLING_HEADER_ID", String.valueOf(batchNumber));
			inputParams.put("IN_DISPATCHED_COUNT", String.valueOf(dispatchedCount));

			logger.debug("billing_header_id=" + batchNumber);
			logger.debug("dispatched_count=" + dispatchedCount);
			// build DataRequest object
			request.setConnection(this.connection);

			request.setInputParams(inputParams);
			request.setStatementID("PG_UPDATE_DISPATCHED_COUNT_CC");

			// get data
			DataAccessUtil dau = DataAccessUtil.getInstance();
			Map outputs = (Map) dau.execute(request);
			request.reset();
			status = (String) outputs.get("OUT_STATUS");
			if (ARConstants.COMMON_VALUE_NO.equals(status)) {
				throw new SQLException((String) outputs.get("OUT_MESSAGE"));
			}
			logger.debug("Exiting doUpdateDispatchedCount");
		} finally {
		}
	}

	public boolean checkAndIncrementProcessedCount(long batchNumber) throws Exception {
		logger.debug("Entering doIncrementProcessedCount");

		DataRequest request = new DataRequest();
		String status = "";
		boolean allProcessedFlag = false;
		String doneProcessingStr = "N";
		try {
			/* setup store procedure input parameters */
			HashMap inputParams = new HashMap();
			inputParams.put("IN_BILLING_HEADER_ID", String.valueOf(batchNumber));

			logger.debug("billing_header_id=" + batchNumber);
			// build DataRequest object
			request.setConnection(this.connection);

			request.setInputParams(inputParams);
			request.setStatementID("PG_INCR_AND_CHECK_PROC_COUNT");

			// get data
			DataAccessUtil dau = DataAccessUtil.getInstance();
			Map outputs = (Map) dau.execute(request);
			request.reset();
			status = (String) outputs.get("OUT_STATUS");
			doneProcessingStr = (String) outputs.get("OUT_IS_EQUAL");
			if("Y".equals(doneProcessingStr))
			{
				allProcessedFlag = true;
			}	
			if (ARConstants.COMMON_VALUE_NO.equals(status)) {
				throw new SQLException((String) outputs.get("OUT_MESSAGE"));
			}
		} finally {
			logger.debug("Exiting doIncrementProcessedCount ::"+allProcessedFlag);
		}
		
		return allProcessedFlag;
	}

	public long getDBBillingHeaderId(long billingHeaderID, String paymentType) throws Exception {

		long retValue = 0;

		CachedResultSet crs = null;
		if (logger.isDebugEnabled()) {
			logger.debug("Entering getPGBillingHeader");
		}

		try {
			DataRequest dataRequest = new DataRequest();
			dataRequest.setConnection(this.connection);
			dataRequest.setStatementID("GET_BILLING_HEADER_ID_PG");
			dataRequest.addInputParam("IN_BILLING_HEADER_ID", billingHeaderID);
			dataRequest.addInputParam("IN_PAYMENT_TYPE", paymentType);
			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			crs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
			dataRequest.reset();

			if (crs.next()) {
				retValue = crs.getLong("BILLING_HEADER_ID");
			}

		} finally {
			if (logger.isDebugEnabled()) {
				logger.debug("Exiting getDBBillingHeaderId " + retValue);
			}
		}
		return retValue;
	}

	public List<PGEODRecordVO> getFailedCCBillingDetailsList(long billingHeaderId,String paymentType) throws IOException, SQLException, ParserConfigurationException, SAXException {
		
		if (logger.isDebugEnabled()) {
			logger.debug("Entering getFailedCCBillingDetailsList");
		}
		
		 CachedResultSet ccbillingRecords = null;
		 List<PGEODRecordVO> billingDetList = new ArrayList<PGEODRecordVO>(); 
		 PGEODRecordVO pgEODRec = null; 
		try {
			DataRequest dataRequest = new DataRequest();
			dataRequest.setConnection(this.connection);
			dataRequest.setStatementID("GET_FAILED_BILLING_DETAILS_CC");
			dataRequest.addInputParam("IN_BILLING_HEADER_ID", billingHeaderId);
			dataRequest.addInputParam("IN_PAYMENT_TYPE", paymentType);
			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			ccbillingRecords = (CachedResultSet)dataAccessUtil.execute(dataRequest);
			dataRequest.reset();
			
			while (ccbillingRecords.next()) {
				pgEODRec = new PGEODRecordVO();
				pgEODRec.setBillingDetailId(ccbillingRecords.getLong("billing_detail_id"));
				pgEODRec.setBillingHeaderId(ccbillingRecords.getLong("billing_header_id"));
				pgEODRec.setStatus(ccbillingRecords.getString("status"));
				pgEODRec.setMasterOrderNumber(ccbillingRecords.getString("ORDER_NUMBER"));
				pgEODRec.setBillType(ccbillingRecords.getString("BILL_TYPE"));
				pgEODRec.setTransactionType(ccbillingRecords.getString("TRANSACTION_TYPE"));
				pgEODRec.setPaymentId(ccbillingRecords.getLong("PAYMENT_ID"));
				billingDetList.add(pgEODRec);
			}	

		} finally {
			if (logger.isDebugEnabled()) {
				logger.debug("Exiting getFailedCCBillingDetailsList");
			}
		}
		return billingDetList;
	}

	public void updateHeaderIndicator(long batchNumber,String paymentType)	throws Throwable {
			
			DataRequest request = new DataRequest();
			String status = "";
			try {
				/* setup store procedure input parameters */
				HashMap inputParams = new HashMap();
				inputParams.put("IN_BILLING_HEADER_ID", String.valueOf(batchNumber));
				inputParams.put("IN_PAYMENT_TYPE", paymentType);

				logger.debug("billing_header_id=" + batchNumber);
				// build DataRequest object
				request.setConnection(this.connection);

				request.setInputParams(inputParams);
				request.setStatementID("PG_UPDATE_HEADER_IND");

				// get data
				DataAccessUtil dau = DataAccessUtil.getInstance();
				Map outputs = (Map) dau.execute(request);
				request.reset();
				status = (String) outputs.get("OUT_STATUS");
				logger.info("Updated  Processed Indicator status "+status);
				if (ARConstants.COMMON_VALUE_NO.equals(status)) {
					throw new SQLException((String) outputs.get("OUT_MESSAGE"));
				}
				logger.debug("Exiting checkAndUpdateHeaderIndicator");
			} finally {
			}
	}

	
	
}