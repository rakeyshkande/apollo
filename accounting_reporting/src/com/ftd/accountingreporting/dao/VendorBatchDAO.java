package com.ftd.accountingreporting.dao;

import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

public class VendorBatchDAO 
{
    private Connection dbConnection = null;
    private Logger logger = 
        new Logger("com.ftd.accountingreporting.dao.VendorBatchDAO");
        
    public VendorBatchDAO(Connection conn)
    {
        super();
        dbConnection = conn;
    }
    
    public Connection getConnection() {
        return dbConnection;
    }
    /**
     * This method inserts into clean.JDE_AUDIT_LOG
     * @param runDate
     * @return CachedResultSet
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     * @throws Exception
     */
    public void insertJdeAuditLog(Date runDate)
        throws SAXException, ParserConfigurationException, IOException,
            SQLException, Exception
    {
       if(logger.isDebugEnabled()){
            logger.debug("Entering insertJdeAuditLog");
            logger.debug("run date : " + runDate);
        }
        
        DataRequest request = new DataRequest();
        
        try
        {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_DATE", new Timestamp(runDate.getTime()));

            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("INSERT_JDE_AUDIT_LOG");
            
            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            String status = (String) outputs.get("OUT_STATUS");
            if(!status.equals(ARConstants.COMMON_VALUE_YES))
            {
                String errorMessage = (String) outputs.get("OUT_MESSAGE");
                if(!errorMessage.startsWith("WARNING")) {
                    throw new Exception(errorMessage);
                } else {
                    logger.debug(errorMessage);
                }
            }

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting doArchiveReconMesg");
            } 
        }     
    }

    public CachedResultSet getJdeCredits(Date runDate)
        throws SAXException, ParserConfigurationException, IOException,
            SQLException
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering getJdeCredits");
            logger.debug("run date : " + runDate);
        }
        CachedResultSet results;
        DataRequest request = new DataRequest();
        
        try
        {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_DATE", new Timestamp(runDate.getTime()));
                
            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("GET_JDE_CREDITS");
            
            // get data
            
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            results = (CachedResultSet) outputs.get("OUT_CURSOR");
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getJdeCredits");
            } 
        }
        
        return results;
    }

  /**
   * Retrieves all JDE adjustments for the month that includes the date.
   * @param runDate
   * @return 
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws java.io.IOException
   * @throws java.sql.SQLException
   */
    public CachedResultSet getJdeAdjustments(Date runDate)
        throws SAXException, ParserConfigurationException, IOException,
            SQLException
    {
       if(logger.isDebugEnabled()){
            logger.debug("Entering getJdeAdjustments");
            logger.debug("run date : " + runDate);
        }
        CachedResultSet results;
        DataRequest request = new DataRequest();
        
        try
        {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_DATE", new Timestamp(runDate.getTime()));
                
            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("GET_JDE_ADJ");
            
            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            results = (CachedResultSet) outputs.get("OUT_CURSOR");
               
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getJdeAdjustments");
            } 
        }
        
        return results;
    }
    
    
    /**
     * This method inserts data from venus.venus into clean.WEST_VENDOR_JDE_AUDIT_LOG
     * @param runDate
     * @return CachedResultSet
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     * @throws Exception
     */
    public void insertWestJdeAuditLog(Date runDate)
        throws SAXException, ParserConfigurationException, IOException,
            SQLException, Exception
    {
       if(logger.isDebugEnabled()){
            logger.debug("Entering insertWestJdeAuditLog");
            logger.debug("run date : " + runDate);
        }
        
        DataRequest request = new DataRequest();
        
        try
        {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_DATE", new Timestamp(runDate.getTime()));

            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("INSERT_WEST_JDE_AUDIT_LOG");
            
            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            String status = (String) outputs.get("OUT_STATUS");
            if(!status.equals(ARConstants.COMMON_VALUE_YES))
            {
                String errorMessage = (String) outputs.get("OUT_MESSAGE");
                if(!errorMessage.startsWith("WARNING")) {
                    throw new Exception(errorMessage);
                } else {
                    logger.debug(errorMessage);
                }
            }

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting insertWestJdeAuditLog");
            } 
        }     
    }
    
    /**
     * Retrieves all JDE credits for west vendor fulfilled orders for the month that includes the date.
     * @param runDate
     * @return 
     * @throws org.xml.sax.SAXException
     * @throws javax.xml.parsers.ParserConfigurationException
     * @throws java.io.IOException
     * @throws java.sql.SQLException
     */
    public CachedResultSet getWestJdeCredits(Date runDate)
            throws SAXException, ParserConfigurationException, IOException,
                SQLException
        {
            if(logger.isDebugEnabled()){
                logger.debug("Entering getWestJdeCredits");
                logger.debug("run date : " + runDate);
            }
            CachedResultSet results;
            DataRequest request = new DataRequest();
            
            try
            {
                /* setup store procedure input parameters */
                HashMap inputParams = new HashMap();
                inputParams.put("IN_DATE", new Timestamp(runDate.getTime()));
                    
                /* build DataRequest object */
                request.setConnection(dbConnection);
                request.reset();
                request.setInputParams(inputParams);
                request.setStatementID("GET_WEST_JDE_CREDITS");
                
                // get data
                
                DataAccessUtil dau = DataAccessUtil.getInstance();
                Map outputs = (Map) dau.execute(request);
                results = (CachedResultSet) outputs.get("OUT_CURSOR");
            } finally {
                if(logger.isDebugEnabled()){
                   logger.debug("Exiting getWestJdeCredits");
                } 
            }
            
            return results;
        }
    
    
    /**
     * Retrieves all JDE adjustments for west vendor fulfilled orders for the month that includes the date.
     * @param runDate
     * @return 
     * @throws org.xml.sax.SAXException
     * @throws javax.xml.parsers.ParserConfigurationException
     * @throws java.io.IOException
     * @throws java.sql.SQLException
     */
      public CachedResultSet getWestJdeAdjustments(Date runDate)
          throws SAXException, ParserConfigurationException, IOException,
              SQLException
      {
         if(logger.isDebugEnabled()){
              logger.debug("Entering getWestJdeAdjustments");
              logger.debug("run date : " + runDate);
          }
          CachedResultSet results;
          DataRequest request = new DataRequest();
          
          try
          {
              /* setup store procedure input parameters */
              HashMap inputParams = new HashMap();
              inputParams.put("IN_DATE", new Timestamp(runDate.getTime()));
                  
              /* build DataRequest object */
              request.setConnection(dbConnection);
              request.reset();
              request.setInputParams(inputParams);
              request.setStatementID("GET_WEST_JDE_ADJ");
              
              // get data
              DataAccessUtil dau = DataAccessUtil.getInstance();
              Map outputs = (Map) dau.execute(request);
              results = (CachedResultSet) outputs.get("OUT_CURSOR");
                 
          } finally {
              if(logger.isDebugEnabled()){
                 logger.debug("Exiting getWestJdeAdjustments");
              } 
          }
          
          return results;
      }

    
}