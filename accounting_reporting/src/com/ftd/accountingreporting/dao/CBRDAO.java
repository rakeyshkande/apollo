package com.ftd.accountingreporting.dao;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.vo.CbrCommentVO;
import com.ftd.accountingreporting.vo.CbrOrderBillVO;
import com.ftd.accountingreporting.vo.CbrPaymentVO;
import com.ftd.accountingreporting.vo.CbrRefundVO;
import com.ftd.accountingreporting.vo.CbrVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;
import java.math.BigDecimal;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import org.xml.sax.SAXException;

/**
 * This is the data access object for Charge Back and Retrieval. 
 * @author Charles Fox
 */
public class CBRDAO 
{
    Connection dbConnection = null;
    private Logger logger = 
        new Logger("com.ftd.accountingreporting.dao.CBRDAO");
        
    public CBRDAO(Connection conn)
    {
        super();
        dbConnection = conn;
    }

   /**
    * This method retrieves the customer Id for the specified origin guid
    * @param origin_guid String
    * @return String
    * @throws java.lang.Exception
    */
    public String doGetCustomerID(String order_guid) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doGetCustomerID");
        }
        String customerID = "";
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(dbConnection);
            dataRequest.setStatementID("GET_CART");
            dataRequest.addInputParam("IN_ORDER_GUID", order_guid);
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            
            logger.debug("Calling GET_CART: " + order_guid);
           // Map outputs = (Map) dataAccessUtil.execute(dataRequest);  
            CachedResultSet outputs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

         //   CachedResultSet rs = (CachedResultSet) outputs.get("OUT_CUR");
            
            if (outputs.next()) 
            {
                customerID = outputs.getString("customer_id");
            }
        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting doGetCustomerID");
            } 
        }
        return customerID;
    }
    /**
     * This method retrieves the Mercury and Venus FTD messages for the given 
     * external order number. Call CLEAN.ORDER_MESG_PKG.GET_MERC_VENUS_TRANSACTIONS.
     * @param external_order_number - String
     * @param cutoff_date - Date
     * @return Document
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     */    
    public Document doGetMercVenusTransactions(String order_detail_id, 
        Date cutoff_date)
        throws SAXException, ParserConfigurationException, IOException,
            SQLException
    {
       if(logger.isDebugEnabled()){
            logger.debug("Entering doGetMercVenusTransactions");
            logger.debug("External Order Number : " + order_detail_id);
            logger.debug("Cutoff Date : " + cutoff_date);
        }
        
        DataRequest request = new DataRequest();
        Document mercVenusTransactions = null;
        
        try
        {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_REFERENCE_NUMBER", order_detail_id);
            inputParams.put("IN_DATE", new Timestamp(cutoff_date.getTime())); 
            
            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("GET_MERC_VENUS_TRANSACTIONS");
            
            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            mercVenusTransactions = (Document) dau.execute(request);
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting doGetMercVenusTransactions");
            } 
        }
        
        return mercVenusTransactions;
    }

    /**
     * This method retrieves the billing trsactions for the given master 
     * order number. 
     * Call CLEAN.RECON_CHARGEBACK_PKG.GET_BILLING_TRANSACTIONS.
     * @param master_order_number - String
     * @return Document
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     */
    public Document doGetBillingTransactions(String master_order_number)
        throws SAXException, ParserConfigurationException, IOException,
            SQLException
    {
       if(logger.isDebugEnabled()){
            logger.debug("Entering doGetBillingTransactions");
            logger.debug("Master Order Number : " + master_order_number);
        }
        
        DataRequest request = new DataRequest();
        Document billingTransactions = null;
        
        try
        {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_MASTER_ORDER_NUMBER", master_order_number);
                
            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("GET_BILLING_TRANSACTIONS");
            
            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            billingTransactions = (Document) dau.execute(request);

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting doGetBillingTransactions");
            } 
        }
        
        return billingTransactions;
    }


    /**
     * This method retrieves payments for the user to select against which 
     * the Charge Back is created. Call CLEAN.RECON_CHARGEBACK_PKG.GET_PAYMENTS_FOR_CB
     * @param master_order_number - String
     * @return Document
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     */
    public Document doGetPaymentsForCB(String master_order_number, String type_code)
        throws SAXException, ParserConfigurationException, IOException,
            SQLException
    {
       if(logger.isDebugEnabled()){
            logger.debug("Entering doGetPaymentsForCB");
            logger.debug("Master Order Number : " + master_order_number);
            logger.debug("Type Code: " + type_code);
        }
        
        DataRequest request = new DataRequest();
        Document cbPayments = null;
        
        try
        {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_MASTER_ORDER_NUMBER", master_order_number);
            inputParams.put("IN_TYPE_CODE", type_code);
                
            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("GET_PAYMENTS_FOR_CB");
            
            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            cbPayments = (Document) dau.execute(request);

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting doGetPaymentsForCB");
            } 
        }
        
        return cbPayments;
    }

    /**
     * This method retrieves payments and refunds that the users cannot select 
     * from when a Charge Back is created. 
     * Call CLEAN.RECON_CHARGEBACK_PKG.GET_PAYMENTS_NOT_FOR_CB
     * @param master_order_number - String
     * @return Document
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     */
    public Document doGetPaymentsNotForCB(String master_order_number)
        throws SAXException, ParserConfigurationException, IOException,
            SQLException
    {
       if(logger.isDebugEnabled()){
            logger.debug("Entering doGetPaymentsNotForCB");
            logger.debug("Master Order Number : " + master_order_number);
        }
        
        DataRequest request = new DataRequest();
        Document cbPayments = null;
        
        try
        {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_MASTER_ORDER_NUMBER", master_order_number);
                
            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("GET_PAYMENTS_NOT_FOR_CB");
            
            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            cbPayments = (Document) dau.execute(request);

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting doGetPaymentsNotForCB");
            } 
        }
        
        return cbPayments;
    }

    /**
     * This method inserts payment ref for the charge back. 
     * Call CLEAN.XXX_PKG.INSERT_CBR_PAYMENTS_REF. 
     * refundIds is a comma delimited string of refund ids.
     * @param vo - CbrRefundVO
     * @return String
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     */
    public void doInsertCBRPaymentRef(String cbrId, String paymentId, String createdBy)
        throws SAXException, ParserConfigurationException, IOException,
            SQLException, Exception
    {
       if(logger.isDebugEnabled()){
            logger.debug("Entering doInsertCBRPaymentRef");
            logger.debug("cbrId: " + cbrId);
            logger.debug("paymentId: " + paymentId);
            logger.debug("createdBy: " + createdBy);
        }
        
        DataRequest request = new DataRequest();
        String paymentID = "";
        
        try
        {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_CBR_ID", cbrId);
            inputParams.put("IN_PAYMENT_ID", paymentId);
            inputParams.put("IN_CREATED_BY", createdBy);
        
            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("INSERT_CBR_PAYMENTS_REF");
            
            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            String status = (String) outputs.get("OUT_STATUS");
            if(status.equals(ARConstants.COMMON_VALUE_NO))
            {
                String errorMessage = (String) outputs.get("OUT_MESSAGE");
                throw new Exception(errorMessage);
            }

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting doInsertCBRPaymentRef");
            } 
        }
    }
    
  /**
   * Inserts into clean.payments table.
   * @param creditPaymentId
   * @param refundId
   * @param createdBy
   * @return 
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws java.io.IOException
   * @throws java.sql.SQLException
   * @throws java.lang.Exception
   */
    public String doInsertCBRPayment(String creditPaymentId, String refundId, String createdBy, String sMilesPointToBeRefunded, String ccAuthProvider)
        throws SAXException, ParserConfigurationException, IOException,
            SQLException, Exception
    {
       if(logger.isDebugEnabled()){
            logger.debug("Entering doInsertCBRPayment");
            logger.debug("creditPaymentId: " + creditPaymentId);
            logger.debug("refundId: " + refundId);
            logger.debug("createdBy: " + createdBy);
            logger.debug("sMilesPointToBeRefunded: " + sMilesPointToBeRefunded);
            logger.debug("ccAuthProvider: " + ccAuthProvider);
        }
        
        DataRequest request = new DataRequest();
        String paymentID = "";
        
        try
        {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_CREDIT_PAYMENT_ID", creditPaymentId);
            inputParams.put("IN_REFUND_ID", refundId);
            inputParams.put("IN_CREATED_BY", createdBy);
            inputParams.put("IN_MILES_POINTS_DEBIT_AMT", sMilesPointToBeRefunded);
            inputParams.put("IN_CC_AUTH_PROVIDER", ccAuthProvider);
        
            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("INSERT_CBR_PAYMENT");
            
            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            String status = (String) outputs.get("OUT_STATUS");
            if(status.equals(ARConstants.COMMON_VALUE_NO))
            {
                String errorMessage = (String) outputs.get("OUT_MESSAGE");
                throw new Exception(errorMessage);
            }
            
            return (String)outputs.get("OUT_DEBIT_PAYMENT_ID");

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting doInsertCBRPayment");
            } 
        }
    }
    
    /**
     * Inserts a refund. Return the payment_id for the debit payment record 
     * that is associated with the refund. Call CLEAN.REFUND_PKG.INSERT_REFUND.
     * @param vo - CbrRefundVO
     * @return String
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     */
    public String doInsertRefund(CbrRefundVO vo)
        throws SAXException, ParserConfigurationException, IOException,
            SQLException, Exception
    {
       if(logger.isDebugEnabled()){
            logger.debug("Entering doInsertRefund");
        }
        
        DataRequest request = new DataRequest();
        String refundID = "";
        
        try
        {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_REFUND_DISP_CODE", vo.getRefundDispCode());
            inputParams.put("IN_CREATED_BY", vo.getCreatedBy());
            inputParams.put("IN_REFUND_PRODUCT_AMOUNT", new Double(vo.getRefundProductAmount()));
            inputParams.put("IN_REFUND_ADDON_AMOUNT", new Double(vo.getRefundAddonAmount()));
            inputParams.put("IN_REFUND_SERVICE_FEE", new Double(vo.getRefundServiceFee()));
            inputParams.put("IN_REFUND_TAX", new Double(vo.getRefundTax()));
            inputParams.put("IN_ORDER_DETAIL_ID", vo.getOrderDetailId());
            inputParams.put("IN_RESPONSIBLE_PARTY", vo.getResponsibleParty());
            inputParams.put("IN_REFUND_STATUS", vo.getRefundStatus());
            inputParams.put("IN_ACCT_TRANS_IND", vo.getAcctTransInd());
            inputParams.put("IN_REFUND_ADMIN_FEE", new Double(vo.getRefundAdminFee()));
            inputParams.put("IN_REFUND_SHIPPING_FEE", new Double(vo.getRefundShippingFee()));
            inputParams.put("IN_REFUND_SERVICE_FEE_TAX", new Double(vo.getRefundServiceFeeTax()));
            inputParams.put("IN_REFUND_SHIPPING_TAX", new Double(vo.getRefundShippingTax()));
            inputParams.put("IN_REFUND_DISCOUNT_AMOUNT", new Double(vo.getRefundDiscountAmount()));
            inputParams.put("IN_REFUND_COMMISSION_AMOUNT", new Double(vo.getRefundCommissionAmount()));
            inputParams.put("IN_REFUND_WHOLESALE_AMOUNT", new Double(vo.getRefundWholesaleAmount()));
            inputParams.put("IN_REFUND_WHOLESALE_SERV_FEE", new Double(vo.getRefundWholesaleServiceFee()));
            inputParams.put("IN_ORIGIN_COMPLAINT_COMM_TYPE_ID", "NC");
            inputParams.put("IN_NOTIF_COMPLAINT_COMM_TYPE_ID", "NC");
            inputParams.put("IN_TAX1_NAME", vo.getTax1Name());
            inputParams.put("IN_TAX1_AMOUNT", vo.getTax1Amount());
            inputParams.put("IN_TAX1_DESCRIPTION", vo.getTax1Description());
            inputParams.put("IN_TAX1_RATE", vo.getTax1Rate());
            inputParams.put("IN_TAX2_NAME", vo.getTax2Name());
            inputParams.put("IN_TAX2_AMOUNT", vo.getTax2Amount());
            inputParams.put("IN_TAX2_DESCRIPTION", vo.getTax2Description());
            inputParams.put("IN_TAX2_RATE", vo.getTax2Rate());
            inputParams.put("IN_TAX3_NAME", vo.getTax3Name());
            inputParams.put("IN_TAX3_AMOUNT", vo.getTax3Amount());
            inputParams.put("IN_TAX3_DESCRIPTION", vo.getTax3Description());
            inputParams.put("IN_TAX3_RATE", vo.getTax3Rate());
            inputParams.put("IN_TAX4_NAME", vo.getTax4Name());
            inputParams.put("IN_TAX4_AMOUNT", vo.getTax4Amount());
            inputParams.put("IN_TAX4_DESCRIPTION", vo.getTax4Description());
            inputParams.put("IN_TAX4_RATE", vo.getTax4Rate());
            inputParams.put("IN_TAX5_NAME", vo.getTax5Name());
            inputParams.put("IN_TAX5_AMOUNT", vo.getTax5Amount());
            inputParams.put("IN_TAX5_DESCRIPTION", vo.getTax5Description());
            inputParams.put("IN_TAX5_RATE", vo.getTax5Rate());
          
            
            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("INSERT_REFUND");
            
            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            String status = (String) outputs.get("OUT_STATUS");
            if(status.equals(ARConstants.COMMON_VALUE_YES))
            {
                refundID = (String) outputs.get("OUT_REFUND_ID");
            }
            else
            {
                String errorMessage = (String) outputs.get("OUT_MESSAGE");
                throw new Exception(errorMessage);
            }

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting doInsertRefund");
            } 
        }
        
        return refundID;
    }

    /**
     * This method deletes refunds for the charge back. 
     * Call CLEAN.XXX_PKG.DELETE_CBR_REFUNDS. 
     * @param cbrId - String
     * @return n/a
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     */
    public void doDeleteCBRRefunds(String cbrId)
        throws SAXException, ParserConfigurationException, IOException,
            SQLException, Exception
    {
       if(logger.isDebugEnabled()){
            logger.debug("Entering doInsertCBRRefunds");
            logger.debug("cbrId: " + cbrId);
        }
        
        DataRequest request = new DataRequest();
        String paymentID = "";
        
        try
        {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_CBR_ID", cbrId);
        
            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("DELETE_CBR_REFUNDS");
            
            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            String status = (String) outputs.get("OUT_STATUS");
            if(status.equals(ARConstants.COMMON_VALUE_NO))
            {
                String errorMessage = (String) outputs.get("OUT_MESSAGE");
                throw new Exception(errorMessage);
            }

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting doInsertCBRRefunds");
            } 
        }
        
    }


    /**
     * Retrieves all order bills associated with the payment. 
     * Call CLEAN.REFUND_PKG.VIEW_ORDER_BILLS_BY_PAYMENT. 
     * Return an array list of CBROrderBillVOs.
     * @param paymentId - String
     * @return ArrayList
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     * @throws Exception
     */
    public ArrayList doGetOrderBills(String paymentId)
        throws SAXException, ParserConfigurationException, IOException,
            SQLException, Exception
    {
       if(logger.isDebugEnabled()){
            logger.debug("Entering doGetOrderBills");
        }
        
        DataRequest request = new DataRequest();
        ArrayList orderBillVOs = new ArrayList();
        
        try
        {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_PAYMENT_ID", paymentId);

            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("VIEW_ORDER_BILLS_BY_PAYMENT");
            
            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            CachedResultSet outputs = (CachedResultSet) dau.execute(request);
            while (outputs.next())
            {
                CbrOrderBillVO orderBill = new CbrOrderBillVO();
                orderBill.setAddonAmount(outputs.getDouble("add_on_amount"));
                orderBill.setOrderDetailId(outputs.getString("order_detail_id"));
                orderBill.setProductAmount(outputs.getDouble("product_amount"));
                orderBill.setDiscountAmount(outputs.getDouble("discount_amount"));
                orderBill.setServiceFee(outputs.getDouble("service_fee"));
                orderBill.setShippingFee(outputs.getDouble("shipping_fee"));
                orderBill.setShippingTax(outputs.getDouble("shipping_tax"));
                orderBill.setTax(outputs.getDouble("tax"));
                orderBill.setServiceFeeTax(outputs.getDouble("service_fee_tax"));
                orderBill.setCommissionAmount(outputs.getDouble("commission_amount"));
                orderBill.setWholesaleAmount(outputs.getDouble("wholesale_amount"));
                orderBill.setAdminFee(outputs.getDouble("admin_fee"));
                orderBill.setTax1Name(outputs.getString("tax1_name"));
                orderBill.setTax1Amount(outputs.getBigDecimal("tax1_amount"));
                orderBill.setTax1Description(outputs.getString("tax1_description"));
                orderBill.setTax1Rate(outputs.getBigDecimal("tax1_rate"));
                orderBill.setTax2Name(outputs.getString("tax2_name"));
                orderBill.setTax2Amount(outputs.getBigDecimal("tax2_amount"));
                orderBill.setTax2Description(outputs.getString("tax2_description"));
                orderBill.setTax2Rate(outputs.getBigDecimal("tax2_rate"));
                orderBill.setTax3Name(outputs.getString("tax3_name"));
                orderBill.setTax3Amount(outputs.getBigDecimal("tax3_amount"));
                orderBill.setTax3Description(outputs.getString("tax3_description"));
                orderBill.setTax3Rate(outputs.getBigDecimal("tax3_rate"));
                orderBill.setTax4Name(outputs.getString("tax4_name"));
                orderBill.setTax4Amount(outputs.getBigDecimal("tax4_amount"));
                orderBill.setTax4Description(outputs.getString("tax4_description"));
                orderBill.setTax4Rate(outputs.getBigDecimal("tax4_rate"));
                orderBill.setTax5Name(outputs.getString("tax5_name"));
                orderBill.setTax5Amount(outputs.getBigDecimal("tax5_amount"));
                orderBill.setTax5Description(outputs.getString("tax5_description"));
                orderBill.setTax5Rate(outputs.getBigDecimal("tax5_rate"));
                orderBillVOs.add(orderBill);
            }

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting doGetOrderBills");
            } 
        }  
        
        return orderBillVOs;
    }

    /**
     * Retrieve payment information for the given payment id. 
     * Call CLEAN.BILLING_END_OF_DAY_PKG.GET_PAYMENT_BY_ID.
     * @param n/a
     * @return CachedResultSet
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     */
    public CbrPaymentVO doGetPaymentInfo(String paymentId)
        throws SAXException, ParserConfigurationException, IOException,
            SQLException, Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doGetPaymentInfo");
        }
        CbrPaymentVO cbrPayVO = new CbrPaymentVO();
        DataRequest request = new DataRequest();
        
        try{
        
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_PAYMENT_ID", paymentId);

            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("GET_PAYMENT_BY_ID");
            
            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            CachedResultSet outputs = (CachedResultSet) dau.execute(request);
            while (outputs.next())
            {
                cbrPayVO.setCCId(outputs.getObject("cc_id")!=null?outputs.getString("cc_id"):"");
                cbrPayVO.setPaymentID(outputs.getObject("payment_id")!=null?outputs.getString("payment_id"):"");
                cbrPayVO.setPaymentType(outputs.getObject("payment_type")!=null?outputs.getString("payment_type"):"");
                cbrPayVO.setMpRedemptionRateAmt(outputs.getObject("mp_redemption_rate_amt")!=null?new Double(outputs.getString("mp_redemption_rate_amt")).doubleValue():0);

            }
            
        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting doGetPaymentInfo");
            } 
        } 
        return  cbrPayVO;
    }
    
    /**
     * This method inserts a CBRVO into the CLEAN.CHARGEBACK_RETRIEVAL table. 
     * Call CLEAN.RECON_CHARGEBACK_PKG.INSERT_CBR.
     * @param n/a
     * @return String
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     */
    public String doInsertCbr(CbrVO cbrVO, CbrCommentVO commentVO)
         throws SAXException, ParserConfigurationException, IOException,
            SQLException, Exception
    {
       if(logger.isDebugEnabled()){
            logger.debug("Entering doInsertCbr");
        }
        
        DataRequest request = new DataRequest();
        String cbrID = "";
        try{
    
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_PAYMENT_ID", cbrVO.getCreditPaymentID());
            inputParams.put("IN_TYPE_CODE", cbrVO.getTypeCode());
            inputParams.put("IN_AMOUNT", new Double(cbrVO.getAmount()));
            if(cbrVO.getReceivedDate()!=null){
                inputParams.put("IN_RECEIVED_DATE", new Timestamp(cbrVO.getReceivedDate().getTime()));
            }
            else
            {
                inputParams.put("IN_RECEIVED_DATE", null);
            }
            
            if(cbrVO.getReversalDate()!=null){
                inputParams.put("IN_REVERSAL_DATE", new Timestamp(cbrVO.getReversalDate().getTime()));
            }
            else
            {
                inputParams.put("IN_REVERSAL_DATE", null);
            }
            if(cbrVO.getDueDate()!=null){
                inputParams.put("IN_DUE_DATE", new Timestamp(cbrVO.getDueDate().getTime()));
            }
            else
            {
                inputParams.put("IN_DUE_DATE", null);
            }

            if(cbrVO.getReturnedDate()!=null){
                inputParams.put("IN_RETURNED_DATE", new Timestamp(cbrVO.getReturnedDate().getTime()));
            }
            else
            {
                inputParams.put("IN_RETURNED_DATE", null);
            }
            inputParams.put("IN_REASON_CODE", cbrVO.getReasonCode());
            inputParams.put("IN_CREATED_BY", commentVO.getCreatedBy());
            inputParams.put("IN_COMMENT_TEXT", commentVO.getCommentText());

            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("INSERT_CBR");
            
            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            String status = (String) outputs.get("OUT_STATUS");
            if(!status.equals(ARConstants.COMMON_VALUE_YES))
            {
                String errorMessage = (String) outputs.get("OUT_ERROR_MESSAGE");
                throw new Exception(errorMessage);
            }
            else
            {
                cbrID = (String) outputs.get("OUT_CBR_ID");
            }

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting doInsertCbr");
            } 
        }
        return cbrID;
    }

    /**
     * This method updates a CBRVO in the CLEAN.CHARGEBACK_RETRIEVAL table 
     * for the given cbr_id. Call CLEAN.XXX_PKG.UPDATE_CBR.
     * @param n/a
     * @return CachedResultSet
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     */
    public void doUpdateCBR(CbrVO cbrVO, CbrCommentVO commentVO)
         throws SAXException, ParserConfigurationException, IOException,
            SQLException, Exception
    {
       if(logger.isDebugEnabled()){
            logger.debug("Entering doUpdateCBR");
        }
        
        DataRequest request = new DataRequest();
        
        try
        {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_CBR_ID", cbrVO.getCBRId());
            if(cbrVO.getReceivedDate()!=null){
                inputParams.put("IN_RECEIVED_DATE", new Timestamp(cbrVO.getReceivedDate().getTime()));
            }
            else
            {
                inputParams.put("IN_RECEIVED_DATE", null);
            }
            if(cbrVO.getReversalDate()!=null){
                inputParams.put("IN_REVERSAL_DATE", new Timestamp(cbrVO.getReversalDate().getTime()));
            }
            else
            {
                inputParams.put("IN_REVERSAL_DATE", null);
            }
            if(cbrVO.getDueDate()!=null){
                inputParams.put("IN_DUE_DATE", new Timestamp(cbrVO.getDueDate().getTime()));
            }
            else
            {
                inputParams.put("IN_DUE_DATE", null);
            }

            if(cbrVO.getReturnedDate()!=null){
                inputParams.put("IN_RETURNED_DATE", new Timestamp(cbrVO.getReturnedDate().getTime()));
            }
            else
            {
                inputParams.put("IN_RETURNED_DATE", null);
            }
            inputParams.put("IN_REASON_CODE", cbrVO.getReasonCode());
            inputParams.put("IN_UPDATED_BY", commentVO.getCreatedBy());
            inputParams.put("IN_COMMENT_TEXT", commentVO.getCommentText());

            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("UPDATE_CBR");
            
            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            String status = (String) outputs.get("OUT_STATUS");
            if(!status.equals(ARConstants.COMMON_VALUE_YES))
            {
                String errorMessage = (String) outputs.get("OUT_ERROR_MESSAGE");
                throw new Exception(errorMessage);
            }

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting doUpdateCBR");
            } 
        } 
    }

    /**
     * This method retrieves a CBR object as document for the given cbr_id. 
     * Call CLEAN.RECON_CHARGEBACK_PKG.VIEW_CHARGEBACK_RETRIEVAL.
     * @param cbr_id - String
     * @return CbrVO
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     */
    public CbrVO doGetCbr(String cbr_id)
        throws SAXException, ParserConfigurationException, IOException,
            SQLException
    {
       if(logger.isDebugEnabled()){
            logger.debug("Entering doGetCbr");
            logger.debug("CBR ID : " + cbr_id);
        }
        
        DataRequest request = new DataRequest();
        CbrVO cbrVO = new CbrVO();
        cbrVO.setCBRId(cbr_id);
        
        try
        {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_CBR_ID", cbr_id);
                
            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("VIEW_CHARGEBACK_RETRIEVAL");
            
            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            CachedResultSet outputs = (CachedResultSet) dau.execute(request);
            while (outputs.next())
            {
                cbrVO.setAmount(outputs.getDouble("amount"));
                cbrVO.setCreditPaymentID(outputs.getString("credit_payment_id"));
                cbrVO.setDueDate(outputs.getDate("due_date"));
                cbrVO.setReasonCode(outputs.getString("reason_code"));
                cbrVO.setReceivedDate(outputs.getDate("received_date"));
                cbrVO.setReturnedDate(outputs.getDate("returned_date"));
                cbrVO.setReversalDate(outputs.getDate("reversal_date"));
                cbrVO.setTypeCode(outputs.getString("type_code"));
            }
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting doGetCbr");
            } 
        }
        
        return cbrVO;
    }

    /**
     * This method retrieves comment history for the given cbr_id. 
     * Call CLEAN.RECON_CHARGEBACK_PKG.GET_CBR_COMMENTS.
     * @param n/a
     * @return CachedResultSet
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     */
    public Document doGetCommentHistory(String cbr_id)
        throws SAXException, ParserConfigurationException, IOException,
            SQLException, TransformerException, Exception
    {
       if(logger.isDebugEnabled()){
            logger.debug("Entering doGetCommentHistory");
            logger.debug("CBR ID : " + cbr_id);
        }
        
        DataRequest request = new DataRequest();
        Document cbrComments = null;
        
        try
        {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_CBR_ID", cbr_id);
                
            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("GET_CBR_COMMENTS");
            
            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            cbrComments = (Document) dau.execute(request);

            // Remove seconds and mini seconds from time. created on is originally in the format:
            // yyyy-mm-dd hh:mm:ss.ms
            if(cbrComments != null) {
                 NodeList commentList = cbrComments.getElementsByTagName("created_on");
                if(commentList != null) {
                    for(int i = 0; i < commentList.getLength(); i++) {
                        Node commentNode = commentList.item(i);
                        if(commentNode != null) {
                            Text createdOnText = (Text)commentNode.getFirstChild();
                            String createdOnString = createdOnText.getNodeValue();
                            if(createdOnString != null) {
                                createdOnString = createdOnString.substring(0, createdOnString.lastIndexOf(":"));
                                createdOnText.setNodeValue(createdOnString);
                            }
                        }
                    }
                }
            }
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting doGetCommentHistory");
            } 
        }
        
        return cbrComments;    
    }
    
    /**
     * This method checks if the shopping cart is at least billed.  
     * Call CLEAN.ORDER_QUERY_PKG.IS_SHOPPING_CART_BILLED.
     * @param master_order_number - String
     * @return boolean
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     * @throws Exception
     */
     public boolean isShoppingCartBilled(String master_order_number)
         throws SAXException, ParserConfigurationException, IOException,
            SQLException, Exception
    {
       if(logger.isDebugEnabled()){
            logger.debug("Entering isShoppingCartBilled");
        }
        
        DataRequest request = new DataRequest();
        String isShopCartBilled = "";
        try
        {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_MASTER_ORDER_NUMBER", master_order_number);

            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("IS_SHOPPING_CART_BILLED");
            
            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            isShopCartBilled = dau.execute(request).toString();

            
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting isShoppingCartBilled");
            } 
        } 
        
        return isShopCartBilled.equals("Y")?true:false;
     }
     
    /**
     * This method checks if the user is authorized to access the named 
     * resource. Call SecurityManager.assertPermission.
     * @param context - String
     * @param session_id - String
     * @param resource - String
     * @param permission - String
     * @return boolean
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     * @throws Exception
     */
     public boolean isUserAuthorized(String context, String session_id, 
        String resource, String permission)
         throws SAXException, ParserConfigurationException, IOException,
            SQLException, Exception
    {
       if(logger.isDebugEnabled()){
            logger.debug("Entering isUserAuthorized");
        }
        
        String userAuthorized = "";
        DataRequest request = new DataRequest();
        
        try
        {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("context", context);
            inputParams.put("securityToken", session_id);
            inputParams.put("resource", resource);
            inputParams.put("permission", permission);

            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("assert_permission");
            
            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            userAuthorized = (String) outputs.get("status");

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting isUserAuthorized");
            } 
        }  
        
        return userAuthorized.equals("Y")?true:false;
    }

    /**
     * This method retrieves cbr_reason_val reason codes. 
     * Call CLEAN.RECON_CHARGEBACK_PKG.GET_CBR_REASON_VAL_LIST.
     * @param n/a
     * @return CachedResultSet
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     */
    public Document doGetCbrReasons()
        throws SAXException, ParserConfigurationException, IOException,
            SQLException
    {
       if(logger.isDebugEnabled()){
            logger.debug("Entering doGetCbrReaons");
        }
        
        DataRequest request = new DataRequest();
        Document cbrReasons = null;
        
        try
        {
            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setStatementID("GET_CBR_REASON_VAL_LIST");
            
            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            cbrReasons = (Document) dau.execute(request);

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting doGetCbrReaons");
            } 
        }
        
        return cbrReasons;
    }
  /**
   * Retrieves the CBR Payment record for which the charge back or retrieval
   * is created.
   * @param cbr_id
   * @return 
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws java.io.IOException
   * @throws java.sql.SQLException
   */
    public Document doGetCBRPayment(String cbr_id)
        throws SAXException, ParserConfigurationException, IOException,
            SQLException
    {
       if(logger.isDebugEnabled()){
            logger.debug("Entering doGetCBRPayment");
            logger.debug("CBR ID : " + cbr_id);
        }
        
        DataRequest request = new DataRequest();
        Document cbrPayment = null;
        
        try
        {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_CBR_ID", cbr_id);
                
            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("GET_CBR_PAYMENT");
            
            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            cbrPayment = (Document) dau.execute(request);

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting doGetCBRPaymetn");
            } 
        }
        
        return cbrPayment;   
    }

    /**
     * This method calculates the amount to be reconciled for a particular 
     * order number. Return 0 in the following conditions:
     * �	NO MATCH FOUND
     * �	CAN WITH NO DEN
     * �	REJ
     * @param message_id - String
     * @param merc_or_venus - String
     * @param cutoff_date - Date
     * @return double
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     * @todo Need stored proc to be created
     */
    public String doGetPriceByMsgNumber(String message_id, String merc_or_venus, 
        Date cutoff_date) 
        throws SAXException, ParserConfigurationException, 
            IOException, SQLException, Exception
    {
       if(logger.isDebugEnabled()){
            logger.debug("Entering doGetPriceByMsgNumber");
            logger.debug("message_id " + message_id);
            logger.debug("merc_or_venus " + merc_or_venus);
            logger.debug("cutoff_date " + cutoff_date);
        }
        
        DataRequest request = new DataRequest();
        String amtToRecon = "";
        
        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
          
            inputParams.put("IN_MESSAGE_ID", message_id);
            inputParams.put("IN_SYSTEM", merc_or_venus);
            inputParams.put("IN_DATE", new Timestamp(cutoff_date.getTime()));

            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("CALCULATE_AMOUNT_TO_RECONCILE");
            
            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            String status = "";
          
            status = (String)outputs.get("OUT_STATUS");
            if(status.equals(ARConstants.COMMON_VALUE_YES))
            {
                amtToRecon = outputs.get("OUT_PRICE").toString();
            }
            else
            {
                String errorMessage = (String) outputs.get("OUT_MESSAGE");
                logger.debug("Message is: " + errorMessage);
                
                /*
                if(errorMessage.equals("No records found")
                || errorMessage.equals("A rejected record was found")
                || errorMessage.equals("A CANCEL record with no DENY record was found"))
                {
                    amtToRecon = "0";
                } else {
                    amtToRecon = "EXECPTION";
                }  
                */
            }
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting doCalcAmtToRecon");
            } 
        }
        return amtToRecon;
    } 
    
   
    
  
}