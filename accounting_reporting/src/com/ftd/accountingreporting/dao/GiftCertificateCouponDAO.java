package com.ftd.accountingreporting.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Document;

import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.vo.GiftCertificateCouponVO;
import com.ftd.accountingreporting.vo.GiftCertificateSourceCodeVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * GiftCertificateCouponDAO
 * 
 * This is the DAO that handles maintenance of Gift Certificate Coupon
 * 
 */
 
public class GiftCertificateCouponDAO 
{
  private Connection connection;
  private static Logger logger  = new Logger("com.ftd.accountingreporting.dao.GiftCertificateCouponDAO");
  
   /**
   * Constructor
   * @param connection Connection
   */
  public GiftCertificateCouponDAO(Connection connection)
  {
    this.connection = connection;
  }
  
  /**
   * This method gets the coupon types
   * @return Document
   * @throws java.lang.Exception
   */
  public Document doGetGCCouponTypes() throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("GET_GC_COUPON_TYPES");
       
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      
       return (Document) dataAccessUtil.execute(dataRequest);      
  }
  
  /**
   * This method gets status list
   * @return Document
   * @throws java.lang.Exception
   */
  public Document doGetStatusList() throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("GCC_GET_STATUS");
       
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      
       return (Document) dataAccessUtil.execute(dataRequest);      
  }
  
  /**
   * This method gets next request number
   * @return String
   * @throws java.lang.Exception
   */
  public String doGetNextRequestNumber(String inPrefix) throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("GCC_GET_NEXT_REQUEST_NUMBER");
       
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        dataRequest.addInputParam("IN_PREFIX", inPrefix);
        String reqnum = dataAccessUtil.execute(dataRequest).toString();
    
        return reqnum;
  }
  
  /**
   * This method gets the request numbers data for matching request numbers
   * @param String requestNumber
   * @param String gcCouponNumber
   * @return Document
   * @throws java.lang.Exception
   */
  public Document doSearchRequest(String requestNumber, String gcCouponNumber) throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("GCC_SEARCH_REQUEST");
       
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        dataRequest.addInputParam("IN_REQUEST_NUMBER", requestNumber);
        dataRequest.addInputParam("IN_GC_COUPON_NUMBER", gcCouponNumber);
        
        return (Document) dataAccessUtil.execute(dataRequest);      
  }
  
  /**
   * This method gets the coupon numbers data for matching gcCoupon numbers
   * @param String requestNumber
   * @param String gcCouponNumber
   * @return Document
   * @throws java.lang.Exception
   */
  public Document doSearchGcCoupon(String requestNumber, String gcCouponNumber) throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("GCC_SEARCH_COUPON");
       
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        dataRequest.addInputParam("IN_REQUEST_NUMBER", requestNumber);
        dataRequest.addInputParam("IN_GC_COUPON_NUMBER", gcCouponNumber);
        
        return (Document) dataAccessUtil.execute(dataRequest);      
  }
  
  /**
   * This method updates the status for matching request number
   * @param String requestNumber
   * @param String status
   * @return boolean
   * @throws java.lang.Exception
   */
  public CachedResultSet doUpdateGcCouponStatus(String requestNumber, String status) throws Exception
  {
        CachedResultSet coupons = new CachedResultSet();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("GCC_UPDATE_COUPON_STATUS");
       
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        dataRequest.addInputParam("IN_REQUEST_NUMBER", requestNumber);
        dataRequest.addInputParam("IN_GCC_STATUS", status);
        
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);      
        String status1 = (String) outputs.get(ARConstants.STATUS_PARAM);
        if(status1 != null && status1.equals(ARConstants.COMMON_VALUE_NO))
        {
            String message = (String) outputs.get(ARConstants.MESSAGE_PARAM);
            if (message.startsWith("WARNING"))
                status1 = ARConstants.COMMON_VALUE_YES;
            else
                throw new SQLException(message);
        }
        else
        {
            coupons = (CachedResultSet) outputs.get("OUT_CURSOR");
        }
        logger.debug("Update on GC_COUPON table for status by request number successfull. Status="+status1);
                
        return coupons;     
  }

 /**
   * This method updates the status for matching request number
   * @param String requestNumber
   * @param String status
   * @return boolean
   * @throws java.lang.Exception
   */
  public CachedResultSet doUpdateGcCouponInfoAndStatus(GiftCertificateCouponVO giftCertCouponVO) throws Exception
  {
        CachedResultSet coupons = new CachedResultSet();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("UPDATE_GC_COUPON_N_GCC_REQUEST");
       
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        dataRequest.addInputParam("IN_REQUEST_NUMBER", giftCertCouponVO.getRequestNumber());
        dataRequest.addInputParam("IN_REQUESTED_BY", giftCertCouponVO.getRequestedBy());
        dataRequest.addInputParam("IN_PROGRAM_NAME", giftCertCouponVO.getProgramName());
        dataRequest.addInputParam("IN_PROGRAM_DESC", giftCertCouponVO.getProgramDesc());
        dataRequest.addInputParam("IN_PROGRAM_COMMENT", giftCertCouponVO.getProgramComment());
        dataRequest.addInputParam("IN_PROMOTION_ID", giftCertCouponVO.getPromotionID());
        if (giftCertCouponVO.getGcCouponStatus()==null || giftCertCouponVO.getGcCouponStatus().equals("none"))
        {
            dataRequest.addInputParam("IN_GCC_STATUS", "");
        }
        else
        {
            dataRequest.addInputParam("IN_GCC_STATUS", giftCertCouponVO.getGcCouponStatus());
        }
        dataRequest.addInputParam("IN_UPDATED_BY", giftCertCouponVO.getUpdatedBy());
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);  
        
        
        String status1 = (String) outputs.get(ARConstants.STATUS_PARAM);
        if(status1 != null && status1.equals(ARConstants.COMMON_VALUE_NO))
        {
            String message = (String) outputs.get(ARConstants.MESSAGE_PARAM);
            if (message.startsWith("WARNING"))
                status1 = ARConstants.COMMON_VALUE_YES;
            else
                throw new SQLException(message);
        }
        else
        {
            coupons = (CachedResultSet) outputs.get("OUT_CURSOR");
        }
        logger.debug("Update on GC_COUPON table for status by request number successfull. Status="+status1);
                
        return coupons;     
  }
  
  /**
   * This method deletes the gc coupon recipients for the matcing request nunber
   * @param String requestNumber
   * @return boolean
   * @throws java.lang.Exception
   */
  public boolean doDeleteRequest(String requestNumber) throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("GCC_DELETE_COUPON_REQUEST");
       
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        dataRequest.addInputParam("IN_REQUEST_NUMBER", requestNumber);
               
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);      
        String status = (String) outputs.get(ARConstants.STATUS_PARAM);
        if(status != null && status.equals(ARConstants.COMMON_VALUE_NO))
        {
            String message = (String) outputs.get(ARConstants.MESSAGE_PARAM);
            throw new SQLException(message);
        }
        logger.debug("Delete on GC_COUPON_REQUEST successfull. Status="+status);
                
        return (status).equals(ARConstants.COMMON_VALUE_YES)? true:false;     
  }
  
  /**
   * This method deletes the gc coupon history for matching gcCouponNumber
   * @param String gcCouponNumber
   * @return boolean
   * @throws java.lang.Exception
   */
  public boolean doDeleteHistory(String gcCouponNumber) throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("GCC_DELETE_COUPON_HISTORY");
       
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        dataRequest.addInputParam("IN_GC_COUPON_NUMBER", gcCouponNumber);
               
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);      
        String status = (String) outputs.get(ARConstants.STATUS_PARAM);
        if(status != null && status.equals(ARConstants.COMMON_VALUE_NO))
        {
            String message = (String) outputs.get(ARConstants.MESSAGE_PARAM);
            throw new SQLException(message);
        }
        logger.debug("Delete on GC_COUPON_HISTORY successfull. Status="+status);
                
        return (status).equals(ARConstants.COMMON_VALUE_YES)? true:false;     
  }
  
  /**
   * This method inserts the gc coupon history 
   * @param GiftCertificateCouponVO vo
   * @return boolean
   * @throws java.lang.Exception
   */
  public boolean doInsertHistory(GiftCertificateCouponVO vo) throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("INSERT_GC_COUPON_HISTORY");
       
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        dataRequest.addInputParam("IN_GC_COUPON_NUMBER", vo.getGcCouponNumber());
        if(vo.getGcRedemptionDate() != null) {
            dataRequest.addInputParam("IN_REDEMPTION_DATE", new Timestamp(vo.getGcRedemptionDate().getTimeInMillis()));
        }
        if(vo.getGcReinstateDate() != null) {
            dataRequest.addInputParam("IN_REINSTATE_DATE", new Timestamp(vo.getGcReinstateDate().getTimeInMillis()));   
        }
        if(vo.getGcOrderNumber() != null) {
            dataRequest.addInputParam("IN_ORDER_DETAIL_ID", String.valueOf(vo.getGcOrderDetailId()));
        }
        dataRequest.addInputParam("IN_STATUS", "Active");
                 
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);      
        String status = (String) outputs.get(ARConstants.STATUS_PARAM);
        if(status != null && status.equals(ARConstants.COMMON_VALUE_NO))
        {
            String message = (String) outputs.get(ARConstants.MESSAGE_PARAM);
            throw new SQLException(message);
        }
        logger.debug("Insert on GC_COUPON_HISTORY successfull. Status="+status);
                
        return (status).equals(ARConstants.COMMON_VALUE_YES)? true:false;     
  }
  
  /**
   * This method gets the gc coupon status for matching request number
   * @param String requestNumber
   * @return Document
   * @throws java.lang.Exception
   */
  public Document doGetStatusCount(String requestNumber) throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("GCC_GET_STATUS_COUNT");
       
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        dataRequest.addInputParam("IN_REQUEST_NUMBER", requestNumber);
               
        return (Document) dataAccessUtil.execute(dataRequest);   
  }
  
  /**
   * This method creates the gc coupon request
   * @param GiftCertificateCouponVO vo
   * @return boolean
   * @throws java.lang.Exception
   */
  public boolean doCreateGcCouponRequest(GiftCertificateCouponVO vo) throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("INSERT_GC_COUPON_REQUEST");
       
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        dataRequest.addInputParam("IN_REQUEST_NUMBER", vo.getRequestNumber());
        dataRequest.addInputParam("IN_GC_COUPON_TYPE", vo.getGcCouponType());
        dataRequest.addInputParam("IN_ISSUE_AMOUNT", new Double(vo.getIssueAmount()));
        dataRequest.addInputParam("IN_PAID_AMOUNT", new Double(vo.getPaidAmount()));
        
        if (vo.getIssueDate() != null)
            dataRequest.addInputParam("IN_ISSUE_DATE", new Timestamp(vo.getIssueDate().getTimeInMillis()));
        if (vo.getExpirationDate() != null)
            dataRequest.addInputParam("IN_EXPIRATION_DATE", new Timestamp(vo.getExpirationDate().getTimeInMillis()));
            
        dataRequest.addInputParam("IN_QUANTITY", new Long(vo.getQuantity()));
        dataRequest.addInputParam("IN_FORMAT", vo.getFormat());
        dataRequest.addInputParam("IN_FILE_NAME", vo.getFileName());
        dataRequest.addInputParam("IN_REQUESTED_BY", vo.getRequestedBy());
        dataRequest.addInputParam("IN_ORDER_SOURCE", vo.getOrderSource());
        dataRequest.addInputParam("IN_PROGRAM_NAME", vo.getProgramName());
        dataRequest.addInputParam("IN_PROGRAM_DESC", vo.getProgramDesc());
        dataRequest.addInputParam("IN_PROGRAM_COMMENT", vo.getProgramComment());
        dataRequest.addInputParam("IN_COMPANY_ID", vo.getCompanyId());        
        dataRequest.addInputParam("IN_PREFIX", vo.getPrefix());
        dataRequest.addInputParam("IN_OTHER", vo.getOther());
        dataRequest.addInputParam("IN_PROMOTION_ID", vo.getPromotionID());
        dataRequest.addInputParam("IN_GC_PARTNER_PROGRAM_NAME", vo.getGcPartnerProgramName());
        dataRequest.addInputParam("IN_CREATED_BY",vo.getCreatedBy());
        
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);      
        String status = (String) outputs.get(ARConstants.STATUS_PARAM);
        if(status != null && status.equals(ARConstants.COMMON_VALUE_NO))
        {
            String message = (String) outputs.get(ARConstants.MESSAGE_PARAM);
            throw new SQLException(message);
        }
        logger.debug("Insert on GC_COUPON_REQUEST successfull. Status="+status);
        
       
                       
        return (status).equals(ARConstants.COMMON_VALUE_YES)? true:false;       
  }
  
  /**
   * This method updates GC Coupons 
   * @param GiftCertificateCouponVO vo
   * @return boolean
   * @throws java.lang.Exception
   */
  public boolean doUpdateGcCoupon(GiftCertificateCouponVO vo) throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        
        dataRequest.setStatementID("UPDATE_GCC_STATUS_N_RECIP_ID");
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        dataRequest.addInputParam("IN_GC_COUPON_NUMBER", vo.getGcCouponNumber());
        dataRequest.addInputParam("IN_GC_COUPON_RECIP_ID", new Long(vo.getGcCouponRecipId()));
        dataRequest.addInputParam("IN_GC_COUPON_STATUS", vo.getGcCouponStatus());
        
        Map m = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) m.get(ARConstants.STATUS_PARAM);
        String gcCouponNumber = vo.getGcCouponNumber();
        Calendar redemptionDate = vo.getGcRedemptionDate();
        Calendar reinstateDate = vo.getGcReinstateDate();
        String orderNumber = vo.getGcOrderNumber();
        logger.debug("gcCouponNumber is : " + gcCouponNumber);
        logger.debug("redemption_date is: " + redemptionDate);
        logger.debug("gcReinstateDate is: " + reinstateDate);

        if (status.equals(ARConstants.COMMON_VALUE_YES))
        {
           // Status changes to "Redeemed"
           if ((vo.getGcOrderNumber() != null) && (vo.getGcRedemptionDate() != null))
           {
               String orderDetailId = doGetOrderDetailId(vo.getGcOrderNumber());
               logger.debug("orderDetaild is : " + orderDetailId);

               vo.setGcOrderDetailId(Long.valueOf(orderDetailId).longValue());
               doInsertHistory(vo);

           } else if(vo.getGcCouponStatus().equals("Reinstate")) {
               // Status changes to "Reinstate"
               vo.setGcReinstateDate(Calendar.getInstance());
               doInsertHistory(vo);
           }
        }
            
        return (status).equals(ARConstants.COMMON_VALUE_YES)? true:false;        
  }
  
  /**
   * This method gets the history data by coupon number
   * @param String couponNumber
   * @return Document
   * @throws java.lang.Exception
   */
  public Document doGetHistoryByCouponNum (String couponNumber) throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        
        dataRequest.setStatementID("GET_GCC_HISTORY_BY_COUPON_NUM");
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        dataRequest.addInputParam("IN_GC_COUPON_NUMBER", couponNumber);
        
        return (Document) dataAccessUtil.execute(dataRequest);
  }
  
  /**
   * This method updates the history status by gc_coupon_history_id
   * @param GiftCertificateCouponVO vo
   * @return boolean
   * @throws java.lang.Exception
   */
  public boolean doUpdateGccHistoryStatus(GiftCertificateCouponVO vo) throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        
        dataRequest.setStatementID("UPDATE_GCC_HISTORY_STATUS");
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        dataRequest.addInputParam("IN_GC_COUPON_HISTORY_ID", vo.getCouponHistoryId());
        dataRequest.addInputParam("IN_STATUS", vo.getCouponHistoryStatus());
        
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);      
        String status = (String) outputs.get(ARConstants.STATUS_PARAM);
        if(status != null && status.equals(ARConstants.COMMON_VALUE_NO))
        {
            String message = (String) outputs.get(ARConstants.MESSAGE_PARAM);
            throw new SQLException(message);
        }
        logger.debug("Update Gcc Coupon History Status successfull. Status="+status);
                
        return (status).equals(ARConstants.COMMON_VALUE_YES)? true:false;    
  }

  /**
   * This method updates the coupon history status to expire if the date passed is greater 
   * than expiry date
   * @param Calendar inDate
   * @return boolean
   * @throws java.lang.Exception
   */
  public boolean doUpdateGccToExpire(Calendar inDate) throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        
        dataRequest.setStatementID("UPDATE_ACTIVE_GCC_TO_EXPIRE");
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        dataRequest.addInputParam("IN_DATE", new Timestamp(inDate.getTimeInMillis()));
        
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);      
        String status = (String) outputs.get(ARConstants.STATUS_PARAM);
        if(status != null && status.equals(ARConstants.COMMON_VALUE_NO))
        {
            String message = (String) outputs.get(ARConstants.MESSAGE_PARAM);
            throw new SQLException(message);
        }
        logger.debug("Update Gcc Coupon To Expire successfull. Status="+status);
                
        return (status).equals(ARConstants.COMMON_VALUE_YES)? true:false; 
  }
    
  /**
   * This method updates the gc coupon request
   * @param GiftCertificateCouponVO vo
   * @return boolean
   * @throws java.lang.Exception
   */
  public boolean doUpdateGcCouponRequest(GiftCertificateCouponVO vo) throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("UPDATE_GC_COUPON_REQUEST");
       
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        dataRequest.addInputParam("IN_REQUEST_NUMBER", vo.getRequestNumber());
        dataRequest.addInputParam("IN_GC_COUPON_TYPE", vo.getGcCouponType());
        dataRequest.addInputParam("IN_ISSUE_AMOUNT", new Double(vo.getIssueAmount()));
        dataRequest.addInputParam("IN_PAID_AMOUNT", new Double(vo.getPaidAmount()));
        
        if (vo.getIssueDate() != null)
            dataRequest.addInputParam("IN_ISSUE_DATE", new Timestamp(vo.getIssueDate().getTimeInMillis()));
        if (vo.getExpirationDate() != null)
            dataRequest.addInputParam("IN_EXPIRATION_DATE", new Timestamp(vo.getExpirationDate().getTimeInMillis()));
            
        dataRequest.addInputParam("IN_QUANTITY", new Long(vo.getQuantity()));
        dataRequest.addInputParam("IN_FORMAT", vo.getFormat());
        dataRequest.addInputParam("IN_FILE_NAME", vo.getFileName());
        dataRequest.addInputParam("IN_REQUESTED_BY", vo.getRequestedBy());
        dataRequest.addInputParam("IN_ORDER_SOURCE", vo.getOrderSource());
        dataRequest.addInputParam("IN_PROGRAM_NAME", vo.getProgramName());
        dataRequest.addInputParam("IN_PROGRAM_DESC", vo.getProgramDesc());
        dataRequest.addInputParam("IN_PROGRAM_COMMENT", vo.getProgramComment());
        dataRequest.addInputParam("IN_COMPANY_ID", vo.getCompanyId());
        
        dataRequest.addInputParam("IN_PREFIX", vo.getPrefix());
        dataRequest.addInputParam("IN_OTHER", vo.getOther());
        dataRequest.addInputParam("IN_PROMOTION_ID", vo.getPromotionID());
        
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);      
        String status = (String) outputs.get(ARConstants.STATUS_PARAM);
        if(status != null && status.equals(ARConstants.COMMON_VALUE_NO))
        {
            String message = (String) outputs.get(ARConstants.MESSAGE_PARAM);
            throw new SQLException(message);
        }
        logger.debug("Update on GC_COUPON_REQUEST successfull. Status="+status);
                
        return (status).equals(ARConstants.COMMON_VALUE_YES)? true:false;       
  }

  /**
   * This method gets the coupons data by request number or coupon number
   * @param String requestNumber
   * @param String couponNumber
   * @return Document
   * @throws java.lang.Exception
   */
  public Document doGetCouponByReqNumOrGccNum (String requestNumber, String couponNumber) throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        
        dataRequest.setStatementID("GET_GCC_BY_REQ_NUM_OR_GCC_NUM");
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        dataRequest.addInputParam("IN_REQUEST_NUMBER", requestNumber);
        dataRequest.addInputParam("IN_GC_COUPON_NUMBER", couponNumber);
        
        return (Document) dataAccessUtil.execute(dataRequest);
  }

  public Document doGetRestrictedSrcCodesByReqNum (String requestNumber) throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        
        dataRequest.setStatementID("GET_RESTRICTED_SOURCE_CODES_BY_REQ_NUM");
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        dataRequest.addInputParam("IN_REQUEST_NUMBER", requestNumber);        
        
        return (Document) dataAccessUtil.execute(dataRequest);
  }
  /**
   * This method gets the global param value for the name passed
   * @param name
   * @return String
   * @throws java.lang.Exception
   */
  public String doGetGlobalParamValue(String name) throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        
        dataRequest.setStatementID("GET_GLOBAL_PARM_VALUE");
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        dataRequest.addInputParam("IN_CONTEXT", "ACCOUNTING");
        dataRequest.addInputParam("IN_NAME", name);
        logger.debug("retrieve global parm: " + name);
        
        return dataAccessUtil.execute(dataRequest).toString();
   }

   /**
    * This method gets the order detail id
    * @param external_order_number  String
    * @return String
    * @throws java.lang.Exception
    */
    public String doGetOrderDetailId(String value) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("FIND_ORDER_DETAIL_BY_EXTERNAL_NUMBER");
        //dataRequest.setStatementID("FIND_ORDER_NUMBER");
        dataRequest.addInputParam("IN_ORDER_NUMBER", value);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);  
        
        return outputs.get("OUT_ORDER_DETAIL_ID").toString();
    }

   /**
    * This method gets the external order number
    * @param order_detail_id  String
    * @return String
    * @throws java.lang.Exception
    */
    public String doGetExternalOrderNumber(String value) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("FIND_EXTERNAL_NUMBER_BY_DETAIL");
        dataRequest.addInputParam("IN_ORDER_NUMBER", value);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);  
        
        return (String) outputs.get("OUT_EXTERNAL_ORDER_NUMBER");
    }

    /**
     * This method deletes the gc coupon recipients for the matcing request nunber
     * @param String requestNumber
     * @return boolean
     * @throws java.lang.Exception
     */
    public boolean doDeleteRestrictedSrcCodes(String requestNumber, String srcCode) throws Exception
    {
          DataRequest dataRequest = new DataRequest();
          dataRequest.setConnection(connection);
          dataRequest.setStatementID("GCC_DELETE_RESTRICTED_SOURCE_CODES");
         
          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
          dataRequest.addInputParam("IN_REQUEST_NUMBER", requestNumber);
          dataRequest.addInputParam("IN_SOURCE_CODE", srcCode);
          Map outputs = (Map) dataAccessUtil.execute(dataRequest);      
          String status = (String) outputs.get(ARConstants.STATUS_PARAM);
          if(status != null && status.equals(ARConstants.COMMON_VALUE_NO))
          {
              String message = (String) outputs.get(ARConstants.MESSAGE_PARAM);
              throw new SQLException(message);
          }
          logger.debug("Delete on GCC_DELETE_RESTRICTED_SOURCE_CODES successfull. Status="+status);
                  
          return (status).equals(ARConstants.COMMON_VALUE_YES)? true:false;     
    }
    
	public Map<String, String> doUpdateRestrictedSourceCodes(
			GiftCertificateCouponVO giftCertCouponVO, String[] srcCodeStr)
			throws Exception {

		Map<String, String> srcCodeValidateMsgMap = new HashMap<String, String>();
		//THIS IS ONLY FOR SOURCE CODE VALIDATION ONLY, INSERT CALL is
		// COMMENTED IN PROCEDURE INSIDE
		srcCodeValidateMsgMap = doCreateSourceCodes(giftCertCouponVO
				.getCompanyId().toUpperCase(),
				giftCertCouponVO.getRequestNumber(), srcCodeStr,
				giftCertCouponVO.getCreatedBy());
		return srcCodeValidateMsgMap;
	}
    
    public void doInsertSourceCodeForGiftCoupon(DataRequest dataRequest, GiftCertificateCouponVO giftCertCouponVO) throws Exception {
    	//GC coupon source code details
          GiftCertificateSourceCodeVO gcSourceCodeVO = giftCertCouponVO.getSourceCodeVO();
          List<String> restSourceCodesList = gcSourceCodeVO.getRestrictedSourceCodes();
          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
          dataRequest.reset();
          dataRequest.setConnection(connection);
          dataRequest.setStatementID("INSERT_GC_COUPON_SOURCE_CODE");
          for(String sourceCode : restSourceCodesList) {
          	dataRequest.addInputParam("IN_REQUEST_NUMBER", giftCertCouponVO.getRequestNumber());
          	dataRequest.addInputParam("IN_SOURCE_CODE", sourceCode);
          	dataRequest.addInputParam("IN_CREATED_BY", giftCertCouponVO.getCreatedBy());        	
          	Map srcCodeOutputs = (Map) dataAccessUtil.execute(dataRequest);      
              String srcCodeStatus = (String) srcCodeOutputs.get(ARConstants.STATUS_PARAM);
              if(srcCodeStatus != null && srcCodeStatus.equals(ARConstants.COMMON_VALUE_NO))
              {
                  String message = (String) srcCodeOutputs.get(ARConstants.MESSAGE_PARAM);
                  throw new SQLException(message);
              }
              logger.debug("Insert on GC_COUPON_SOURCE_CODE successfull for source code :"+sourceCode + " and Status="+srcCodeStatus);  
          }
        //GC coupon source code details
      }
    
	/**
	 * @param companyId
	 * @param requestNumber
	 * @param sourceCodes
	 * @param createdBy
	 * @return
	 * @throws Exception
	 */
	public Map<String, String> doCreateSourceCodes(String companyId,
			String requestNumber, String[] sourceCodes, String createdBy) throws Exception {

		Map<String, String> validateMessages = new HashMap<String, String>();
		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

		StringBuilder existsMessage = new StringBuilder();
		StringBuilder expiredMessage = new StringBuilder();

		// Creating a separate connection
		Connection conn = DataSourceUtil.getInstance().getConnection(
				ConfigurationUtil.getInstance().getProperty(ARConstants.CONFIG_FILE, ARConstants.DATASOURCE_NAME));
		
		conn.setAutoCommit(false);

		try {
			for (String sourceCodeStr : sourceCodes) {
				DataRequest dataRequest = new DataRequest();
				dataRequest.setConnection(conn);
				dataRequest.addInputParam("IN_COMPANY_ID", companyId);
				dataRequest.addInputParam("IN_REQUEST_NUMBER", requestNumber);
				dataRequest.addInputParam("IN_SOURCE_CODE_STR", sourceCodeStr);
				dataRequest.addInputParam("IN_CREATED_BY", createdBy);
				dataRequest.setStatementID("SRC_CODES_VALIDATION");

				Map srcCodeCreationOutputs = (Map) dataAccessUtil.execute(dataRequest);
				String srcCodeStatus = (String) srcCodeCreationOutputs.get(ARConstants.STATUS_PARAM);

				if (srcCodeStatus != null && srcCodeStatus.equals(ARConstants.COMMON_VALUE_NO)) {
					String message = (String) srcCodeCreationOutputs.get(ARConstants.MESSAGE_PARAM);
					throw new SQLException(message);
				}

				String srcCodeValidStatus = (String) srcCodeCreationOutputs
						.get(ARConstants.SRC_CODE_VALID_MESSAGE);

				if (srcCodeValidStatus.equals(ARConstants.COMMON_VALUE_YES)) {
					if(srcCodeCreationOutputs.get(ARConstants.SRC_CODE_EXISTS_MESSAGE) != null && !" ".equals(srcCodeCreationOutputs.get(ARConstants.SRC_CODE_EXISTS_MESSAGE).toString())) {
						existsMessage.append(new StringBuilder(srcCodeCreationOutputs.get(ARConstants.SRC_CODE_EXISTS_MESSAGE).toString()) + " ");
					}
					if(srcCodeCreationOutputs.get(ARConstants.SRC_CODE_EXPIRED_MESSAGE) != null && !" ".equals(srcCodeCreationOutputs.get(ARConstants.SRC_CODE_EXPIRED_MESSAGE).toString())) {
						expiredMessage.append(new StringBuilder(srcCodeCreationOutputs.get(ARConstants.SRC_CODE_EXPIRED_MESSAGE).toString()) + " ");
					}
				}
				
			
			}
			
			conn.commit();
			
			if (existsMessage != null && existsMessage.toString().length() > 0) {
				validateMessages.put("INVALID_SOURCE_CODES_COUNT", existsMessage.toString());
				existsMessage = new StringBuilder("Source codes does not exist: ").append(existsMessage);
				validateMessages.put(ARConstants.SRC_CODE_EXISTS_MESSAGE, existsMessage.toString());
			}

			if (expiredMessage != null && expiredMessage.toString().length() > 0) {
				validateMessages.put("EXPIRED_SOURCE_CODES_COUNT", expiredMessage.toString());
				expiredMessage = new StringBuilder("Source codes expired: ").append(expiredMessage);
				validateMessages.put(ARConstants.SRC_CODE_EXPIRED_MESSAGE, expiredMessage.toString());
			}
			
		} catch (Exception e) {
			logger.error(e);
			try {
				conn.rollback();
			} catch (Exception e1) {
				logger.error("Unable to rollback the trasaction" + e1.getMessage());				
			}
			throw new Exception(e);
		} finally {
			try {
				conn.close();
			} catch (Exception e1) {
				logger.error("Unable to close connection" + e1.getMessage());
			}
		}		

		return validateMessages;
	}
	
	  /**
	   * This method gets the counts of items in a cart
	   * @param String giftCodeId
	   * @param String orderDetail_id
	   * @return int
	   * @throws java.lang.Exception
	   */
	  public int getCartCount(String giftCodeId, String orderDetailId) throws Exception
	  {
	        DataRequest dataRequest = new DataRequest();
	        dataRequest.setConnection(connection);
	        dataRequest.setStatementID("GCC_GET_CART_COUNT");
	       
	        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	        dataRequest.addInputParam("IN_GIFT_CERTIFICATE_ID", giftCodeId);
	        dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);
	        String cartCount = dataAccessUtil.execute(dataRequest).toString();
	    
	        return Integer.parseInt(cartCount);
	  }
}

