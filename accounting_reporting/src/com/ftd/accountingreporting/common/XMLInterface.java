package com.ftd.accountingreporting.common;

public interface XMLInterface 
{

  public String toXML() throws IllegalAccessException, ClassNotFoundException;
}