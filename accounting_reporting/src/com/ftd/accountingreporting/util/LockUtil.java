package com.ftd.accountingreporting.util;

import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.exception.EntityLockedException;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.Map;


/**
 * This class obtains and releases lock for the entity specified
 */
public class LockUtil
{
  private Logger logger = new Logger("com.ftd.accountingreporting.util.LockUtil");
  private Connection connection;

  /**
  * Constructor for LockUtil Class, Initializes the connection object.
  * @param connection
  */
  public LockUtil(Connection conn)
  {
    connection = conn;
  }

  /**
      * This method obtains lock for the entity specified
      * @param entityType
      * @param entityId
      * @param csrId
      * @param sessionId
      * @return boolean
      * @throws java.lang.Exception
      */
  public boolean obtainLock(String entityType, String entityId, String csrId, String sessionId)
    throws EntityLockedException, SQLException, Exception
  {
    return obtainLock(entityType, entityId, csrId, sessionId, null);
  }

  /**
      * This method obtains lock for the entity specified
      * @param entityType
      * @param entityId
      * @param csrId
      * @param sessionId
      * @return boolean
      * @throws java.lang.Exception
      */
  public boolean obtainLock(String entityType, String entityId, String csrId, String sessionId, String orderLevel)
    throws EntityLockedException, SQLException, Exception
  {
    if (logger.isDebugEnabled())
    {
      logger.debug("Entering obtainLock");
      logger.debug("entityType " + entityType);
      logger.debug("entityId " + entityId);
      logger.debug("csrId " + csrId);
      logger.debug("sessionId " + sessionId);
      logger.debug("orderLevel " + orderLevel);
    }

    DataRequest request = new DataRequest();

    boolean isLockObtained = false;
    try
    {
      /* setup store procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put("IN_ENTITY_TYPE", entityType);
      inputParams.put("IN_ENTITY_ID", entityId);
      inputParams.put("IN_SESSION_ID", sessionId);
      inputParams.put("IN_CSR_ID", csrId);
      inputParams.put("IN_ORDER_LEVEL", orderLevel);

      // build DataRequest object
      request.setConnection(connection);
      request.reset();
      request.setInputParams(inputParams);
      request.setStatementID("UPDATE_CSR_LOCKED_ENTITIES");

      // get data
      DataAccessUtil dau = DataAccessUtil.getInstance();
      Map outputs = (Map) dau.execute(request);

      String lockObtained = (String) outputs.get("OUT_LOCK_OBTAINED");
      String lockCSRId = (String) outputs.get("OUT_LOCKED_CSR_ID");
      String status = (String) outputs.get(ARConstants.STATUS_PARAM);

      logger.debug("Status is: " + status);
      logger.debug("lockObtained is: " + lockObtained);
      if (status.equals("Y"))
      {
        if (lockObtained != null)
        {
          if (lockObtained.equals("Y"))
          {
            /* lock successfully obtained */
            isLockObtained = true;
          }
          else
          {
            /* process locked by another user, throw EntityLockedException */
            throw new EntityLockedException((String) outputs.get("OUT_LOCKED_CSR_ID"));
          }
        }
        else
        {

          throw new Exception("Error: Lock cannot be checked");
        }
      }
      else
      {
        /* If OUT_STATUS is �N�, throw SQLException */
        throw new SQLException((String) outputs.get(ARConstants.MESSAGE_PARAM));
      }

    }
    finally
    {
      if (logger.isDebugEnabled())
      {
        logger.debug("Exiting obtainLock");
      }
    }

    return isLockObtained;
  }

  /**
      * This method releases lock for the entity specified
      * @param entityType
      * @param entityId
      * @param csrId
      * @param sessionId
      * @return boolean
      * @throws java.lang.Exception
      */
  public boolean releaseLock(String entityType, String entityId, String csrId, String sessionId)
    throws Exception
  {
    if (logger.isDebugEnabled())
    {
      logger.debug("Entering releaseLock");
      logger.debug("entityType " + entityType);
      logger.debug("entityId " + entityId);
      logger.debug("csrId " + csrId);
      logger.debug("sessionId " + sessionId);
    }
    DataRequest request = new DataRequest();
    String status = "";
    try
    {
      /* setup store procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put("IN_ENTITY_TYPE", entityType);
      inputParams.put("IN_ENTITY_ID", entityId);
      inputParams.put("IN_SESSION_ID", sessionId);
      inputParams.put("IN_CSR_ID", csrId);

      // build DataRequest object
      request.setConnection(connection);
      request.reset();
      request.setInputParams(inputParams);
      request.setStatementID("DELETE_CSR_LOCKED_ENTITIES");

      // get data
      DataAccessUtil dau = DataAccessUtil.getInstance();
      Map outputs = (Map) dau.execute(request);
      status = (String) outputs.get(ARConstants.STATUS_PARAM);
      if (status.equals("N"))
      {
        String message = (String) outputs.get(ARConstants.MESSAGE_PARAM);

        throw new Exception(message);
      }
    }
    finally
    {
      if (logger.isDebugEnabled())
      {
        logger.debug("Exiting releaseLock");
      }
    }
    return ARConstants.COMMON_VALUE_YES.equals(status)? true: false;
  }

}
