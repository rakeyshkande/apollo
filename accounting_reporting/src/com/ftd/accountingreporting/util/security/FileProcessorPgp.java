package com.ftd.accountingreporting.util.security;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.Security;
import java.util.Date;
import java.util.Iterator;

import org.bouncycastle.bcpg.ArmoredOutputStream;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openpgp.PGPCompressedData;
import org.bouncycastle.openpgp.PGPCompressedDataGenerator;
import org.bouncycastle.openpgp.PGPEncryptedData;
import org.bouncycastle.openpgp.PGPEncryptedDataGenerator;
import org.bouncycastle.openpgp.PGPEncryptedDataList;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPLiteralData;
import org.bouncycastle.openpgp.PGPLiteralDataGenerator;
import org.bouncycastle.openpgp.PGPObjectFactory;
import org.bouncycastle.openpgp.PGPOnePassSignatureList;
import org.bouncycastle.openpgp.PGPPrivateKey;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyEncryptedData;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.bouncycastle.openpgp.PGPPublicKeyRingCollection;
import org.bouncycastle.openpgp.PGPSecretKey;
import org.bouncycastle.openpgp.PGPSecretKeyRingCollection;
import org.bouncycastle.openpgp.PGPUtil;

import com.ftd.osp.utilities.plugins.Logger;


/**
 * A simple utility class that encrypts/decrypts public key based
 * encryption large files.
 * <p>
 * To encrypt a file: FileSecurityUtil -e [-a|-ai] fileName publicKeyFile.<br>
 * If -a is specified the output file will be "ascii-armored".
 * If -i is specified the output file will be have integrity checking added.
 * <p>
 * To decrypt: FileSecurityUtil -d fileName secretKeyFile passPhrase.
 * <p>
 * Note 1: this example will silently overwrite files, nor does it pay any attention to
 * the specification of "_CONSOLE" in the filename. It also expects that a single pass phrase
 * will have been used.
 * <p>
 * Note 2: this example generates partial packets to encode the file, the output it generates
 * will not be readable by older PGP products or products that don't support partial packet
 * encoding.
 */
public class FileProcessorPgp { 
    
    public enum ENCRYPTION_EXTN {    	
    	PGP(".pgp"), ASC(".asc"), BGP(".bgp");    	
    	private String value;    	
    	ENCRYPTION_EXTN(String value) {
    		this.value = value;
    	} 
    };
    
	static {
        Security.addProvider(new BouncyCastleProvider());
    }
	
	private static Logger logger = new Logger(FileProcessorPgp.class.getName());

    /**
     * A simple routine that opens a key ring file and loads the first available key suitable for
     * encryption.
     *
     * @param in
     * @return
     * @throws IOException
     * @throws PGPException
     */
    private static PGPPublicKey readPublicKey(InputStream in)
        throws IOException, PGPException {
        in = PGPUtil.getDecoderStream(in);

        PGPPublicKeyRingCollection pgpPub = new PGPPublicKeyRingCollection(in);

        //
        // we just loop through the collection till we find a key suitable for encryption, in the real
        // world you would probably want to be a bit smarter about this.
        //
        PGPPublicKey key = null;

        //
        // iterate through the key rings.
        //
        Iterator rIt = pgpPub.getKeyRings();

        while ((key == null) && rIt.hasNext()) {
            PGPPublicKeyRing kRing = (PGPPublicKeyRing) rIt.next();
            Iterator kIt = kRing.getPublicKeys();

            while ((key == null) && kIt.hasNext()) {
                PGPPublicKey k = (PGPPublicKey) kIt.next();

                if (k.isEncryptionKey()) {
                    key = k;
                }
            }
        }

        if (key == null) {
            throw new IllegalArgumentException(
                "Can't find encryption key in key ring.");
        }

        return key;
    }

    /**
     * Load a secret key ring collection from keyIn and find the secret key corresponding to
     * keyID if it exists.
     *
     * @param keyIn input stream representing a key ring collection.
     * @param keyID keyID we want.
     * @param pass passphrase to decrypt secret key with.
     * @return
     * @throws IOException
     * @throws PGPException
     * @throws NoSuchProviderException
     */
    private static PGPPrivateKey findSecretKey(InputStream keyIn, long keyID,
        char[] pass) throws IOException, PGPException, NoSuchProviderException {
        PGPSecretKeyRingCollection pgpSec = new PGPSecretKeyRingCollection(PGPUtil.getDecoderStream(
                    keyIn));
        PGPSecretKey pgpSecKey = pgpSec.getSecretKey(keyID);

        if (pgpSecKey == null) {
            return null;
        }

        return pgpSecKey.extractPrivateKey(pass, "BC");
    }

    /**
     * decrypt the passed in message stream
     */
    public static File decrypt(String targetFile, String privateKeyPath,
        String passPhrase) throws Exception {
        InputStream in = PGPUtil.getDecoderStream(new FileInputStream(
                    targetFile));
        InputStream keyIn = new FileInputStream(privateKeyPath);

        try {
            PGPObjectFactory pgpF = new PGPObjectFactory(in);
            PGPEncryptedDataList enc;

            Object o = pgpF.nextObject();

            //
            // the first object might be a PGP marker packet.
            //
            if (o instanceof PGPEncryptedDataList) {
                enc = (PGPEncryptedDataList) o;
            } else {
                enc = (PGPEncryptedDataList) pgpF.nextObject();
            }

            //
            // find the secret key
            //
            Iterator it = enc.getEncryptedDataObjects();
            PGPPrivateKey sKey = null;
            PGPPublicKeyEncryptedData pbe = null;

            while ((sKey == null) && it.hasNext()) {
                pbe = (PGPPublicKeyEncryptedData) it.next();

                sKey = findSecretKey(keyIn, pbe.getKeyID(),
                        passPhrase.toCharArray());
            }

            if (sKey == null) {
                throw new IllegalArgumentException(
                    "secret key for message not found.");
            }

            InputStream clear = pbe.getDataStream(sKey, "BC");

            PGPObjectFactory plainFact = new PGPObjectFactory(clear);

            PGPCompressedData cData = (PGPCompressedData) plainFact.nextObject();

            InputStream compressedStream = new BufferedInputStream(cData.getDataStream());
            InputStream unc = null;
            BufferedOutputStream bOut = null;
            String outFileName = null;

            try {
                PGPObjectFactory pgpFact = new PGPObjectFactory(compressedStream);

                Object message = pgpFact.nextObject();

                if (message instanceof PGPLiteralData) {
                    PGPLiteralData ld = (PGPLiteralData) message;

                    outFileName = (new File(targetFile).getParent()) +
                        File.separator + ld.getFileName();

                    FileOutputStream fOut = new FileOutputStream(outFileName);
                    bOut = new BufferedOutputStream(fOut);

                    unc = ld.getInputStream();

                    int ch;

                    while ((ch = unc.read()) >= 0) {
                        bOut.write(ch);
                    }
                } else if (message instanceof PGPOnePassSignatureList) {
                    throw new PGPException(
                        "encrypted message contains a signed message - not literal data.");
                } else {
                    throw new PGPException(
                        "message is not a simple encrypted file - type unknown.");
                }
            } finally {
                if (unc != null) {
                    unc.close();
                }

                if (bOut != null) {
                    bOut.close();
                }

                compressedStream.close();
            }

            if (pbe.isIntegrityProtected()) {
                if (!pbe.verify()) {
                    throw new PGPException("message failed integrity check");
                }
            }

            return new File(outFileName);
        } catch (PGPException e) {
            if (e.getUnderlyingException() != null) {
                throw e.getUnderlyingException();
            } else {
                throw e;
            }
        } finally {
            if (keyIn != null) {
                keyIn.close();
            }

            if (in != null) {
                in.close();
            }
        }
    }

    /**
     * decrypt the passed in message stream
     */
    public static ByteArrayOutputStream decryptToStream(String targetFile, String privateKeyPath,
        String passPhrase) throws Exception {
        InputStream in = PGPUtil.getDecoderStream(new FileInputStream(
                    targetFile));
        InputStream keyIn = new FileInputStream(privateKeyPath);

        try {
            PGPObjectFactory pgpF = new PGPObjectFactory(in);
            PGPEncryptedDataList enc;

            Object o = pgpF.nextObject();

            //
            // the first object might be a PGP marker packet.
            //
            if (o instanceof PGPEncryptedDataList) {
                enc = (PGPEncryptedDataList) o;
            } else {
                enc = (PGPEncryptedDataList) pgpF.nextObject();
            }

            //
            // find the secret key
            //
            Iterator it = enc.getEncryptedDataObjects();
            PGPPrivateKey sKey = null;
            PGPPublicKeyEncryptedData pbe = null;

            while ((sKey == null) && it.hasNext()) {
                pbe = (PGPPublicKeyEncryptedData) it.next();

                sKey = findSecretKey(keyIn, pbe.getKeyID(),
                        passPhrase.toCharArray());
            }

            if (sKey == null) {
                throw new IllegalArgumentException(
                    "secret key for message not found.");
            }

            InputStream clear = pbe.getDataStream(sKey, "BC");

            PGPObjectFactory plainFact = new PGPObjectFactory(clear);

            PGPCompressedData cData = (PGPCompressedData) plainFact.nextObject();

            InputStream compressedStream = new BufferedInputStream(cData.getDataStream());
            InputStream unc = null;
            ByteArrayOutputStream out = new ByteArrayOutputStream();

            try {
                PGPObjectFactory pgpFact = new PGPObjectFactory(compressedStream);

                Object message = pgpFact.nextObject();

                if (message instanceof PGPLiteralData) {
                    PGPLiteralData ld = (PGPLiteralData) message;

                    unc = ld.getInputStream();

                    int ch;

                    while ((ch = unc.read()) >= 0) {
                        out.write(ch);
                    }
                } else if (message instanceof PGPOnePassSignatureList) {
                    throw new PGPException(
                        "encrypted message contains a signed message - not literal data.");
                } else {
                    throw new PGPException(
                        "message is not a simple encrypted file - type unknown.");
                }
            } finally {
                if (unc != null) {
                    unc.close();
                }

                if (out != null) {
                    out.close();
                }

                compressedStream.close();
            }

            if (pbe.isIntegrityProtected()) {
                if (!pbe.verify()) {
                    throw new PGPException("message failed integrity check");
                }
            }

            return out;

        } catch (PGPException e) {
            if (e.getUnderlyingException() != null) {
                throw e.getUnderlyingException();
            } else {
                throw e;
            }
        } finally {
            if (keyIn != null) {
                keyIn.close();
            }

            if (in != null) {
                in.close();
            }
        }
    }

    private static File encryptFile(String targetFile, PGPPublicKey encKey,
        boolean isAsciiArmored, boolean withIntegrityCheck, boolean isBAMSFile)
        throws IOException, NoSuchProviderException, Exception {
        OutputStream out = null;
        PGPEncryptedDataGenerator cPk = null;
        PGPCompressedDataGenerator comData = null;
        String outFileName = null;

        try {
            if (isAsciiArmored) {
                outFileName = targetFile + ".asc";
                out = new ArmoredOutputStream(new FileOutputStream(outFileName));
            } else {
            	if(isBAMSFile){
            		outFileName = targetFile + ".pgp";
	                out = new FileOutputStream(outFileName);
            	}else{
	                outFileName = targetFile + ".bpg";
	                out = new FileOutputStream(outFileName);
            	}
            }

            cPk = new PGPEncryptedDataGenerator(PGPEncryptedData.TRIPLE_DES,
                    withIntegrityCheck, new SecureRandom(), "BC");

            cPk.addMethod(encKey);

            OutputStream cOut = cPk.open(out, new byte[1 << 16]);

            comData = new PGPCompressedDataGenerator(PGPCompressedData.ZIP);

            PGPUtil.writeFileToLiteralData(comData.open(cOut),
                PGPLiteralData.BINARY, new File(targetFile), new byte[1 << 16]);

            return new File(outFileName);
        } catch (PGPException e) {
            if (e.getUnderlyingException() != null) {
                throw e.getUnderlyingException();
            } else {
                throw e;
            }
        } finally {
            if (comData != null) {
                comData.close();
            }

            if (cPk != null) {
                cPk.close();
            }

            if (out != null) {
                out.close();
            }
        }
    }

    public static void encryptDataToFile(byte[] clearData, File outfile, 
                                          String publicKeyPath, boolean isAsciiArmored)
        throws IOException, NoSuchProviderException, Exception {
        
        ByteArrayOutputStream    encOut = new ByteArrayOutputStream();
        OutputStream out = encOut;
        FileOutputStream fos = null;
        PGPEncryptedDataGenerator cPk = null;
        PGPCompressedDataGenerator comData = null;
        PGPPublicKey encKey = readPublicKey(new FileInputStream(publicKeyPath));
        
        try {
          if (isAsciiArmored) {
              out = new ArmoredOutputStream(out);
          }
          ByteArrayOutputStream   bOut = new ByteArrayOutputStream();
          comData = new PGPCompressedDataGenerator(PGPCompressedDataGenerator.ZIP);
          OutputStream cos = comData.open(bOut); // open it with the final destination
          PGPLiteralDataGenerator lData = new PGPLiteralDataGenerator();
  
          OutputStream  pOut = lData.open(cos, // the compressed output stream
                                          PGPLiteralData.BINARY,
                                          outfile.getName(), 
                                          clearData.length, // length of clear data
                                          new Date()  // current time
                                        );
          pOut.write(clearData);
          lData.close();
          comData.close();
  
          cPk = new PGPEncryptedDataGenerator(PGPEncryptedData.TRIPLE_DES, new SecureRandom(), "BC");
          cPk.addMethod(encKey);
  
          byte[] bytes = bOut.toByteArray();
          OutputStream    cOut = cPk.open(out, bytes.length);
          cOut.write(bytes);  // obtain the actual bytes from the compressed stream
          cOut.close();

          fos = new FileOutputStream(outfile);
          fos.write(encOut.toByteArray());

        } catch (PGPException e) {
            if (e.getUnderlyingException() != null) {
                throw e.getUnderlyingException();
            } else {
                throw e;
            }
        } finally {
            if (comData != null) {
                comData.close();
            }
            if (cPk != null) {
                cPk.close();
            }
            if (out != null) {
                out.close();
            }
            if (fos != null) {
                fos.close();
            }
        }
    }

	public static File encrypt(String targetFile, String publicKeyPath,
			boolean isAsciiArmored, boolean isIntegrityChecked,
			boolean isBAMSFile) throws Exception {
		FileInputStream keyIn = new FileInputStream(publicKeyPath);

		return encryptFile(targetFile, readPublicKey(keyIn), isAsciiArmored,
				isIntegrityChecked, isBAMSFile);
	}

	/**
	 * @param srcFilePath
	 * @param destFilePath
	 * @param destFileName
	 * @param publicKeyPath
	 * @param extn
	 * @param withIntegrityCheck
	 * @return
	 * @throws Exception
	 */
	public static File encrypt(String srcFilePath, String destFilePath, String destFileName, String publicKeyPath, ENCRYPTION_EXTN extn,
			boolean withIntegrityCheck) throws Exception {		
		FileInputStream keyIn = new FileInputStream(publicKeyPath);
		String destFile = destFilePath + File.separator + destFileName + extn.value;
		logger.info("Source file path: " + srcFilePath + ", Encrypted File Path: " + destFile);
		return encryptFile(srcFilePath, readPublicKey(keyIn), destFile, withIntegrityCheck, extn.value);
	}

	/**
	 * @param srcFile
	 * @param encKey
	 * @param destFile
	 * @param withIntegrityCheck
	 * @param fileType
	 * @return
	 * @throws IOException
	 * @throws NoSuchProviderException
	 * @throws Exception
	 */
	private static File encryptFile(String srcFile, PGPPublicKey encKey, String destFile, boolean withIntegrityCheck, String fileType)
			throws IOException, NoSuchProviderException, Exception {

		PGPEncryptedDataGenerator cPk = null;
		PGPCompressedDataGenerator comData = null;
		OutputStream out = null;
		
		try {
			
			if (ENCRYPTION_EXTN.ASC.equals(fileType)) {
				out = new ArmoredOutputStream(new FileOutputStream(destFile));
			} else {
				out = new FileOutputStream(destFile);
			}
			
			cPk = new PGPEncryptedDataGenerator(PGPEncryptedData.TRIPLE_DES, withIntegrityCheck, new SecureRandom(), "BC");
			cPk.addMethod(encKey);
			OutputStream cOut = cPk.open(out, new byte[1 << 16]);
			comData = new PGPCompressedDataGenerator(PGPCompressedData.ZIP);
			PGPUtil.writeFileToLiteralData(comData.open(cOut), PGPLiteralData.BINARY, new File(srcFile), new byte[1 << 16]);
			
			return new File(destFile);
			
		} catch (PGPException e) {
			if (e.getUnderlyingException() != null) {
				throw e.getUnderlyingException();
			} else {
				throw e;
			}
		} finally {
			if (comData != null) {
				comData.close();
			}

			if (cPk != null) {
				cPk.close();
			}

			if (out != null) {
				out.close();
			}
		}
	}


//    public static void main(String[] args) {
//        try {
//        	//String srcFilePath, String destFilePath, String destFileName, String publicKeyPath,  
//            File outFile = FileProcessorPgp.encrypt("D:\\FTD_Keys\\PTS_FTD_5926_030315_024932.CPTD4884", "D:\\FTD_Keys\\encrypted\\", "FTD"+"FTD_PTS_FTD_5926_030315_024932.CPTD4884",
//                    "D:\\FTD_Keys\\ftd-apollo-test-pub.asc",  ENCRYPTION_EXTN.PGP, false);
//
//            System.out.println(outFile.getAbsolutePath());
//            System.out.println(outFile.getParent());
//            System.out.println(outFile.getPath());
//
//            outFile = FileProcessorPgp.decrypt(outFile.getPath(),
//                    "D:\\FTD_Keys\\ftd-apollo-test-sec.asc", "REPLACE ME");
//
//            System.out.println(outFile.getAbsolutePath());
//            System.out.println(outFile.getParent());
//            System.out.println(outFile.getPath());
//        } catch (Exception e) {
//            System.err.println(e.getMessage());
//            e.printStackTrace();
//        }
//    }

}
