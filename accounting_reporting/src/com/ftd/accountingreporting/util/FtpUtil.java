package com.ftd.accountingreporting.util;

import com.enterprisedt.net.ftp.FTPClient;
import com.enterprisedt.net.ftp.FTPConnectMode;
import com.enterprisedt.net.ftp.FTPException;
import com.enterprisedt.net.ftp.FTPTransferType;

import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
/**
 * Manages the state of and access to an FTP connection
 * 
 */
public class FtpUtil 
{
    private Logger logger = 
        new Logger("com.ftd.accountingreporting.util.FtpUtil");
  
    private boolean isLoggedIn;
    private FTPClient ftpClient;  
  
    public FtpUtil()
    {
        isLoggedIn = false;
    }
  
  /**
   * login into home directory
   * @param ftpServer
   * @param username
   * @param password
   * @throws java.lang.Exception
   */
    public void login(String ftpServer, String username, String password)
        throws Exception {
       if(logger.isDebugEnabled()){
            logger.debug("Entering login");
        }
        try{            
            if(logger.isDebugEnabled()){
                logger.debug("Opening FTP connection to " + ftpServer + " as user " + username );
            }
            
            /* connect to FTP server */
            ftpClient = new FTPClient(ftpServer);
            ftpClient.login(username, password);
            ftpClient.setConnectMode(FTPConnectMode.PASV);
            
            /* set the transfer type */
            ftpClient.setType(FTPTransferType.ASCII);                  
         
            /* set the login state */
            isLoggedIn = true;
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting login");
            } 
        }        
    }
        
    /**
    * Logs in to the FTP Location and sets the transfer type
    * 
    * @param ftpLocation
    * @param username
    * @param password
    * @throws java.lang.Exception
    */
    public void login(String ftpServer, String ftpLocation, String username, String password) 
        throws Exception
    {
       if(logger.isDebugEnabled()){
            logger.debug("Entering login");
            logger.debug("ftpServer : " + ftpLocation);
        }
        try{            
            if(logger.isDebugEnabled()){
                logger.debug("Opening FTP connection to " + ftpServer + " as user " + username );
            }
            
            /* connect to FTP server */
            ftpClient = new FTPClient(ftpServer);
            ftpClient.login(username, password);
            ftpClient.chdir(ftpLocation);
            ftpClient.setConnectMode(FTPConnectMode.PASV);
            
            /* set the transfer type */
            ftpClient.setType(FTPTransferType.ASCII);                  
         
            /* set the login state */
            isLoggedIn = true;
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting login");
            } 
        }
    }

  /**
   * Logs out from the FTP Location
   * 
   * Throws an exception if not logged in.
   * 
   * @throws java.lang.Exception
   */
  public void logout() throws Exception
  {
       if(logger.isDebugEnabled()){
            logger.debug("Entering logout");
        }
        try{
            if (isLoggedIn) 
            {
              ftpClient.quit();
              isLoggedIn = false;
            }
            else
            {
              throw new FTPException("Login Required");
            }
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting logout");
            } 
        }
  }
  
  public void getFile(String localPath, String remoteFile) throws Exception
    {
    getFile(localPath, remoteFile, false);
    }
  
    /**
    * Retrieves the file from the ftp server.
    * 
    * Throws an exception if not logged in.
    * 
    * @param remoteFile
    * @param localPath
    * @throws java.lang.Exception
    */
    public void getFile(String localPath, String remoteFile, boolean isBinary) throws Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering getFile");
        }
        try{
            if (isLoggedIn) 
            {
                if(logger.isDebugEnabled()){
                    logger.debug("Downloading file " + remoteFile + " to " + localPath);
                }
                int posOfSlash = remoteFile.lastIndexOf("/");
                String fileName = remoteFile;
                if(posOfSlash != -1 && posOfSlash < (remoteFile.length()-1)) {
                    fileName = remoteFile.substring(posOfSlash + 1);
                }
                
                if (isBinary) 
                {
                  ftpClient.setType(FTPTransferType.BINARY);
                }
                ftpClient.get(localPath + File.separator + fileName, remoteFile);
                // Return to ASCII
                if (isBinary) 
                {
                  ftpClient.setType(FTPTransferType.ASCII);
                }
            }
            else
            {
                throw new FTPException("Login Required");
            }
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getFile");
            } 
        }
    }
    
    public void getFileWithRename(String localPath, String localFile, String remoteFile) throws Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering getFileWithRename");
        }
        try{
            if (isLoggedIn) 
            {
                if(logger.isDebugEnabled()){
                    logger.debug("Downloading file " + remoteFile + " to " + localPath);
                }
                ftpClient.get(localPath + File.separator + localFile, remoteFile);
            }
            else
            {
                throw new FTPException("Login Required");
            }
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getFileWithRename");
            } 
        }
    }
    
    public void deleteFile(String remoteFile) throws Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering deleteFile");
        }
        try{
            if (isLoggedIn) 
            {
                if(logger.isDebugEnabled()){
                    logger.debug("Deleting remote file " + remoteFile);
                }
                try
                {
                  ftpClient.delete(remoteFile);
                }
                catch (Exception e)
                {
                  // Simply log any delete attempt failure.
                  logger.warn("deleteFile: Could not delete file : " + remoteFile, e);
                }
            }
            else
            {
                throw new FTPException("Login Required");
            }
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting deleteFile");
            } 
        }
    }
    
    public void renameFile(String remoteFile, String newName) throws Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering renameFile");
        }
        try{
            if (isLoggedIn) 
            {
                if(logger.isDebugEnabled()){
                    logger.debug("Renaming remote file " + remoteFile + " to " + newName);
                }
                try
                {
                  ftpClient.rename(remoteFile, newName);
                }
                catch (Exception e)
                {
                  // Simply log any delete attempt failure.
                  logger.warn("deleteFile: Could not rename file : " + remoteFile + " to " + newName, e);
                }
            }
            else
            {
                throw new FTPException("Login Required");
            }
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting renameFile");
            } 
        }
    }

    /**
    * Retrieves the file from the ftp server.
    * 
    * Throws an exception if not logged in.
    * 
    * @param remoteFile
    * @throws java.lang.Exception
    */
    public byte[] getFile(String remoteFile) throws Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering (byte) getFile");
        }
        byte[] data;
        try{
            if (isLoggedIn) 
            {
                if(logger.isDebugEnabled()){
                    logger.debug("Downloading data for file " + remoteFile);
                }
                data = ftpClient.get(remoteFile);
            }
            else
            {
                throw new FTPException("Login Required");
            }
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting (byte) getFile");
            } 
        }
        return data;
    }

    /**
    * Retrieves the file from the ftp server.
    * 
    * Throws an exception if not logged in.
    * 
    * @param remoteFile
    * @throws java.lang.Exception
    */
    public byte[] getFileData(String remoteFile, String remotePath) throws Exception
    {
        if(logger.isDebugEnabled()){
           logger.debug("Changing to remote dir: " + remotePath);
        } 
        ftpClient.chdir(remotePath);
        return getFile(remoteFile);
    }
  
  public void putFile(File jdeFile, String remoteFile) throws Exception
  {
    putFile(jdeFile, remoteFile, false);
  }
  
  /**
   * Saves the file in to ftp server.
   * 
   * Throws an exception if not logged in.
   * 
   * @param remoteFile
   * @param localPath
   * @throws java.lang.Exception
   */
    public void putFile(File jdeFile, String remoteFile, boolean isBinary) throws Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering putFile");
        }
        try{
            if (isLoggedIn) 
            {
                if(logger.isDebugEnabled()){
                    logger.debug("Uploading file " + remoteFile );
                }
                
                if (isBinary) 
                {
                  ftpClient.setType(FTPTransferType.BINARY);
                }
                ftpClient.put(new FileInputStream(jdeFile), remoteFile);
                // Return to ASCII
                if (isBinary) 
                {
                  ftpClient.setType(FTPTransferType.ASCII);
                }   
            }
            else
            {
                throw new FTPException("Login Required");
            }
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting putFile");
            } 
        }
    }
  
  /**
   * Saves data in to ftp server.
   * 
   * Throws an exception if not logged in.
   * 
   * @param remoteFile
   * @param localPath
   * @throws java.lang.Exception
   */
    public void putFile(byte[] byteData, String remoteFile, String remotePath, boolean isBinary) throws Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering data putFile");
        }
        try{
            if (isLoggedIn) 
            {
                if(logger.isDebugEnabled()){
                    logger.debug("Changing to remote dir " + remotePath );
                    logger.debug("Uploading data to " + remoteFile );
                }
                
                if (isBinary) 
                {
                  ftpClient.setType(FTPTransferType.BINARY);
                }
                ftpClient.chdir(remotePath);
                ftpClient.put(byteData, remoteFile);
                // Return to ASCII
                if (isBinary) 
                {
                  ftpClient.setType(FTPTransferType.ASCII);
                }   
            }
            else
            {
                throw new FTPException("Login Required");
            }
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting data putFile");
            } 
        }
    }
    
    /**
   * Tests if remove file exists. Returns true if it does and false otherwise.
   * Local copy is deleted to cleanup.
   * @param remoteServer
   * @param remoteLocation
   * @param localLocation
   * @param filename
   * @param username
   * @param password
   * @return 
   * @throws java.lang.Exception
   */
    public boolean remoteFileExists(String remoteServer, 
                                    String remoteLocation, 
                                    String localLocation, 
                                    String filename, 
                                    String username, 
                                    String password)
    throws Exception
    {
        boolean exists = true;
        FtpUtil ftpUtil = new FtpUtil();
        ftpUtil.login(remoteServer, remoteLocation, username, password);
        try {
            ftpUtil.getFile(localLocation, filename);
        } catch(FTPException ftpe) {
            String msg = ftpe.getMessage();
            if (msg.indexOf("No such file or directory") != -1) {
            logger.warn("FTPException message is: " + msg);
            } else {
            logger.error(ftpe);
            }
            exists = false;
            /*
            // msg: CCBILL in library HPWRKLIB not found
            if(msg.indexOf("No such file") != -1
                || msg.indexOf("not found") != -1) {
                exists = false;
            } else {
                throw ftpe;
            }
            */
        }
        if(exists) {
            // delete local copy
            File localCopy = new File(localLocation, filename);
            localCopy.delete();
        }
        return exists;
    }

    /**
    * Retrieves directory list from the ftp server.
    * 
    * Throws an exception if not logged in.
    * 
    * @param remoteDir
    * @throws java.lang.Exception
    */
    public String[] getDirectoryList(String remoteDir) throws Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering getRemoteDir");
        }
        String[] data;
        try{
            if (isLoggedIn) 
            {
                if(logger.isDebugEnabled()){
                    logger.debug("Getting directory list for: " + remoteDir);
                }
                data = ftpClient.dir(remoteDir);
            }
            else
            {
                throw new FTPException("Login Required");
            }
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getRemoteDir");
            } 
        }
        return data;
    }
    
}