package com.ftd.accountingreporting.util;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.XMLFormat;
import com.ftd.osp.utilities.j2ee.ResourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.xml.DOMUtil;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.XMLConstants;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.w3c.dom.Document;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;
import org.jdom.input.DOMBuilder;

/**
 * This Class is an utility class for XML operations.
 *
 * @author Charles Fox
 */
public class XMLUtil 
{
        private static Logger logger  = 
            new Logger("com.ftd.accountingreporting.util.XMLUtil");


    public XMLUtil()
    {
    }

    /**
     *  convert a XML Document object to a String
     * @param convertDoc - Document
     * @return String
     * @throws Exception 
     */
    public String convertDocumentToString(Document convertDoc) 
        throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering convertDocumentToString");
        }
        
        String xmlText = "";
        
        try{
            /* convert XML doc to String */
            StringWriter sw = new StringWriter();       
            DOMUtil.print(((Document) convertDoc), new PrintWriter(sw));
            xmlText=sw.toString();
            
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting convertDocumentToString");
            } 
       } 
       return xmlText;
    }

 
    /**
     *  validate XML against a XSD file
     * @param validationXML - String
     * @param validationFile - String
     * @return String
     * @throws Exception
     */
	public void validateXML(String validationXML, String validationFile) 
        throws Exception
    {
        if(logger.isDebugEnabled()){
           logger.debug("Entering validateXML. XSD= "+ validationFile);
           logger.debug("validationXML " + validationXML);
        } 
        InputStream xsdSchema = ResourceUtil.getInstance().getResourceFileAsStream(validationFile);
        if(xsdSchema==null)
        {
          throw new Exception ("com.ftd.accountingreporting.util.XMLUtil: "+ validationFile +" is not defined");
        }
        
        try {
            String requestXML = "";
            if(validationXML!=null){
                if(!validationXML.equals(""))
                {
                    requestXML = validationXML.trim();
                }    
                else
                {
                    throw new Exception("XML not received");
                }
            }
            else
            {
                throw new Exception("XML not received");
            }
                        
			     
            Schema xsdSchemadoc = DOMUtil.getSchema(new StreamSource(xsdSchema));
            // create a Validator instance, which can be used to validate an instance document
            Validator validator = xsdSchemadoc.newValidator();
            SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

            // validate the DOM tree
            validator.validate(new StreamSource(new StringReader(requestXML)));
        } finally
        {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting validateXML");
            }   
        }
    }
 
     public static void addElementToXML(String name, String value, Document doc)
    {
      
    	Element entry=(Element)doc.createElement(name);
    	entry.appendChild(doc.createTextNode(value));
      
    	doc.getDocumentElement().appendChild(entry);
        
    }
    
    
    
  public static XMLFormat createXMLFormat(String topName, String bottomName, boolean element)
  {
            XMLFormat xmlFormat = new XMLFormat();
            if(element){
               xmlFormat.setAttribute("type", "element");
            }else
            {
              xmlFormat.setAttribute("type", "attribute");
            }
            xmlFormat.setAttribute("top", topName);
            xmlFormat.setAttribute("bottom", bottomName );
            
            return xmlFormat;
  }
  
  
  
  public static String toXML(Object obj) throws Exception {
        StringBuffer sb = new StringBuffer();
        sb.append("<root>");
        if(obj!=null)
        {
          toXML(obj, sb);
        }
        
        sb.append("</root>");

        return sb.toString();
    }

    private static void toXML(Object obj, StringBuffer sb)
        throws InvocationTargetException, IllegalAccessException, 
            NoSuchMethodException {
        Map map = PropertyUtils.describe(obj);
        Iterator it = map.keySet().iterator();

        while (it.hasNext()) {
            String key = (String) it.next();
            
            Object value = map.get(key);
           
            if (isNotNull(value)) {
                if (!key.equalsIgnoreCase("class")) {
                                                           
                    sb.append("<" + key + ">");

                    
                    
                    if (value instanceof List) {
                        List objList = (List) value;
                        Iterator listIt = objList.iterator();

                        while (listIt.hasNext()) {
                            Object listVal = listIt.next();
                            sb.append("<list-item>");
                            if(listVal instanceof java.lang.String)
                            {
                              sb.append(DOMUtil.encodeChars((String)listVal));
                            }else{
                              toXML(listVal, sb);
                            }
                            sb.append("</list-item>");
                        }
                    } else if (value instanceof Date) {
                        
                         SimpleDateFormat sdf = new SimpleDateFormat ("MM/dd/yyyy");
                          Date fDate = (Date)value;
                          String dateStr= sdf.format(fDate);
                          sb.append(dateStr);
                    } else if(value instanceof Map){
                       Map objMap=(Map)value;
                       Iterator keyIt=objMap.keySet().iterator();
                       while(keyIt.hasNext())
                       {
                         sb.append("<map-item>");
                         String optionName=(String)keyIt.next();
                         sb.append("<name>"+DOMUtil.encodeChars(optionName)+"</name>");
                         String optionValue=(String)objMap.get(optionName);
                         sb.append("<value>"+DOMUtil.encodeChars(optionValue)+"</value>");
                         sb.append("</map-item>");
                         
                       }
                    }else
                    {
                       sb.append(DOMUtil.encodeChars(value.toString()));
                    }

                    sb.append("</" + key + ">");
                }
            }
        }
    }
    
    
    private static boolean isNotNull(Object obj)
    {
      if(obj instanceof String)
      {
        return StringUtils.isNotEmpty(((String)obj).trim());
      }else if(obj instanceof Collection)
      {
        return (!((Collection)obj).isEmpty());
      }else if(obj instanceof Map)
      {
        return (!((Map)obj).isEmpty());
      }else
      {
        return (obj!=null);
      }
      
    }
  
   /**
     * Converts a w3c DOM Document to a JDOM Document
     * @param w3cdoc
     * @return converted document
     */
    public static org.jdom.Document w3c2jdom(org.w3c.dom.Document w3cdoc) 
    {
        org.jdom.Document doc = null;
        
        try {
            DOMBuilder builder = new DOMBuilder();
            doc = builder.build(w3cdoc);	
        }
        catch (Exception e)
        {
        	doc = null;
        }
        
        return doc;
   }  
   
    public static String getPayloadData(Object payload, String path) throws Exception
    {
        String value = null;
        try {
            String msg = (String)((MessageToken)payload).getMessage();
            Document doc = DOMUtil.getDocument(msg);
            Node valueNode = XPathAPI.selectSingleNode(doc, path);
            
            if (valueNode == null) 
            {
              logger.warn("getPayloadData: Result does not exist for XPath: " + path);
            } else {
            value = valueNode.getNodeValue();
            }
        } catch (Exception e) {
            logger.error(e);
            return null;
        }
        
        
        return value;
        
    }  

/*******************************************************************************************
 * buildXML()
 *******************************************************************************************/
  public static Document buildXML(HashMap outMap, String cursorName, String topName, String bottomName)
        throws Exception
  {

    //Create a CachedResultSet object using the cursor name that was passed.
    CachedResultSet rs = (CachedResultSet) outMap.get(cursorName);

    Document doc =  null;

    //Create an XMLFormat, and initialize the top and bottom node.  Note that since this method
    //can be/is called by various other methods, the top and botton names MUST be passed to it.
    XMLFormat xmlFormat = new XMLFormat();
    xmlFormat.setAttribute("type", "element");
    xmlFormat.setAttribute("top", topName);
    xmlFormat.setAttribute("bottom", bottomName );

    //call the DOMUtil's converToXMLDOM method
    doc = (Document) DOMUtil.convertToXMLDOM(rs, xmlFormat);

    //return the xml document.
    return doc;

  }


}
