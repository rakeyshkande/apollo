package com.ftd.accountingreporting.util;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import java.util.StringTokenizer;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * This class is an utility class for string manipulation.
 */
public class StringUtil 
{
    private static Logger logger  = 
        new Logger("com.ftd.accountingreporting.util.StringUtil");

    public StringUtil()
    {
    }

    /**
    * this method returns the particular token in the given delimted string.
    * @param line String
    * @param delim String
    * @param pos int
    * @returns String
    * @throws Exception
    */
    public static String getToken(String line, String delim, int pos) 
        throws Exception
    {
        StringTokenizer st = new StringTokenizer(line, delim);
        String token = null;
        
        for(int i = 1; i < pos; i++) {
            // dump the previous tokens
            st.nextToken();
        }
        if(st.hasMoreTokens()) {
            token = st.nextToken();
            if(token != null) {
                token = token.trim();
            }
        }
        
        return token;
    }
}