package com.ftd.accountingreporting.util;

import com.ftd.accountingreporting.altpay.vo.GiftCardBillingDetailVO;
import com.ftd.accountingreporting.constant.AltPayConstants;
import com.ftd.ps.webservice.PaymentRequest;
import com.ftd.ps.webservice.PaymentResponse;
import com.ftd.ps.webservice.AmountVO;
import com.ftd.ps.webservice.CardVO;
import com.ftd.ps.webservice.PaymentServiceClient;
import com.ftd.ps.webservice.PaymentServiceClientSVSImplService;
import com.ftd.osp.utilities.plugins.Logger;

import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;

public class GiftCardPaymentSvc {
	private String clientId;
	private String clientPassword;
	private String serviceUrl;
	private int retryCount;
	private static PaymentServiceClient psc;
	
	private static final String PAYSVC_CURRENCY = "USD";

	private static Logger logger = 
        new Logger("com.ftd.accountingreporting.util.GiftCardPaymentSvc");

	
	/**
	 * Gets handle to Payment Service
	 * 
	 * @return
	 * @throws CacheException
	 * @throws Exception
	 */
	private static void getPaymentServiceClient(String url) throws Exception {
		if (psc == null) {
		  try {
		    URL psURL = new URL(url);
		    PaymentServiceClientSVSImplService ps = new PaymentServiceClientSVSImplService(psURL);
		    psc = ps.getPaymentServiceClientSVSImplPort();
		  } catch (MalformedURLException e) {
		    logger.error("Error when getting GiftCard payment service client: " + e.getMessage());
		    e.printStackTrace();
		  }
		}
	}
	
	/**
	 * Initializes objects needed for calls to SVS Payment Service
	 * 
	 * @param reqvo
	 * @param gdvo
	 */
	private void initSvc(PaymentRequest reqvo, GiftCardBillingDetailVO gdvo) throws Exception {
		reqvo.setClientUserName(clientId);
		reqvo.setClientPassword(clientPassword);
		reqvo.setTransactionId(gdvo.getCaptureTxt());

		CardVO cardvo = new CardVO();
		cardvo.setCardCurrency(PAYSVC_CURRENCY);
		cardvo.setCardNumber(gdvo.getAccountNumber());
		cardvo.setPinNumber(gdvo.getPin());
		reqvo.setCardVO(cardvo);
		
		AmountVO amountvo = new AmountVO();
		amountvo.setAmount(gdvo.getReqAmt().doubleValue());
		amountvo.setCurrency(PAYSVC_CURRENCY);
		reqvo.setAmountVO(amountvo);
		
		// Get handle to payment web service
		getPaymentServiceClient(serviceUrl);
	}
	
	/**
	 * Setup Service objects, call service to settle and deal with response.
	 * 
	 * @param gdvo
	 * @throws Exception
	 */
	public void settle(GiftCardBillingDetailVO gdvo) throws Exception {
		boolean isSuccess = false;
		PaymentRequest reqvo = new PaymentRequest();
		
		// Prepare and then call actual SVS service to settle payment
		//
		initSvc(reqvo, gdvo);
		PaymentResponse respvo = settleSvc(reqvo);
		
		// Deal with response
		//
		if (respvo != null) {
			String respMsgs = "GiftCard Settle Service error code/message: " + respvo.getErrorCode() + "/" + respvo.getErrorMessage() +
			                  " --- SVS response code/message: " + respvo.getResponseCode() + "/" + respvo.getResponseMessage();
			logger.info(respMsgs);

			if (respvo.isIsSuccessful()) {
				CardVO cvo = respvo.getCardVO();
				if (cvo == null) {
					gdvo.setErrorTxt(AltPayConstants.GD_SERVICE_ERR_LOGIN);  // Oddly, success status with null cvo means invalid login
				} else {
					AmountVO amtvo = respvo.getApprovedAmountVO();
					if (amtvo != null) {
						BigDecimal apprAmt = new BigDecimal(String.valueOf(amtvo.getAmount()));
						if (apprAmt.compareTo(gdvo.getReqAmt()) >= 0) {
							// Hurrah, we settled with enough to cover our requested amount						
							isSuccess = true;  
						} else {
							// Uh oh, SVS amount does not cover requested amount						
							gdvo.setErrorTxt(AltPayConstants.GD_SERVICE_ERR_AMOUNT + ". SVS approved payment: " + apprAmt + " Requested: " + gdvo.getReqAmt()); 
						}
					}
				}
			} else {
				gdvo.setErrorTxt(respMsgs);  // Boo, we failed for some reason
			}
		}
		
		// Throw exception if any failure since caller responsible for catch and retry
		//
		if (!isSuccess) {
			gdvo.setStatus(AltPayConstants.GD_SERVICE_FAILURE);
			throw new Exception(AltPayConstants.GD_SERVICE_EXCEPTION);
		}
	}

	private PaymentResponse settleSvc(PaymentRequest reqvo) throws Exception {
		return psc.settle(reqvo);
	}

	
	/**
	 * Setup Service objects, call service to refund and deal with response.
	 * 
	 * @param gdvo
	 * @throws Exception
	 */
	public void refund(GiftCardBillingDetailVO gdvo) throws Exception {
		boolean isSuccess = false;
		PaymentRequest reqvo = new PaymentRequest();

		// Prepare and then call actual SVS service to do refund 
		//
		initSvc(reqvo, gdvo);
		PaymentResponse respvo = refundSvc(reqvo);
		
		// Deal with response
		//
		if (respvo != null) {
			String respMsgs = "GiftCard Refund Service error code/message: " + respvo.getErrorCode() + "/" + respvo.getErrorMessage() +
			                  " --- SVS response code/message: " + respvo.getResponseCode() + "/" + respvo.getResponseMessage();
			logger.info(respMsgs);

			if (respvo.isIsSuccessful()) {
				CardVO cvo = respvo.getCardVO();
				if (cvo == null) {
					gdvo.setErrorTxt(AltPayConstants.GD_SERVICE_ERR_LOGIN);  // Oddly, success status with null cvo means invalid login
				} else {
					AmountVO amtvo = respvo.getApprovedAmountVO();
					if (amtvo != null) {
						BigDecimal apprAmt = new BigDecimal(String.valueOf(amtvo.getAmount()));
						if (apprAmt.compareTo(gdvo.getReqAmt()) >= 0) {
							// Hurrah, refund covered requested amount						
							isSuccess = true;  
						} else {
							// Uh oh, SVS amount does not cover requested amount						
							gdvo.setErrorTxt(AltPayConstants.GD_SERVICE_ERR_AMOUNT + " SVS approved refund: " + apprAmt + " Requested: " + gdvo.getReqAmt()); 
						}
					}
				}
			} else {
				gdvo.setErrorTxt(respMsgs);  // Boo, we failed for some reason
			}
		}
		
		// Throw exception if any failure since caller responsible for catch and retry
		//
		if (!isSuccess) {
			gdvo.setStatus(AltPayConstants.GD_SERVICE_FAILURE);
			throw new Exception(AltPayConstants.GD_SERVICE_EXCEPTION);
		}
	}

	private PaymentResponse refundSvc(PaymentRequest reqvo) throws Exception {
		return psc.refund(reqvo);
	}

	
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public String getClientId() {
		return clientId;
	}

	public String getClientPassword() {
		return clientPassword;
	}
	public void setClientPassword(String clientPassword) {
		this.clientPassword = clientPassword;
	}

	public String getServiceUrl() {
		return serviceUrl;
	}
	public void setServiceUrl(String serviceUrl) {
		this.serviceUrl = serviceUrl;
	}
	
	public int getRetryCount() {
		return retryCount;
	}
	public void setRetryCount(int retryCount) {
		this.retryCount = retryCount;
	}
}
