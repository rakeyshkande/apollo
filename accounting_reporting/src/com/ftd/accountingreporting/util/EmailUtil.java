package com.ftd.accountingreporting.util;

import com.ftd.osp.utilities.j2ee.NotificationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.NotificationVO;

/**
 * utility for sending e-mail messages
 * @author Charles Fox
 */
public class EmailUtil 
{

    private Logger logger = 
        new Logger("com.ftd.accountingreporting.util.EmailUtil");
        
    public EmailUtil()
    {
    }

    /**
     * Send an e-mail message
     * @param vo - NotificationVO
     * @return n/a
     * @throws Exception
     */
    public void sendEmail(NotificationVO vo) throws Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering sendEmail");
        }
        
        try
        {
            /* send out the email */
            NotificationUtil mail = NotificationUtil.getInstance();
            mail.notify(vo);
        }finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting sendEmail");
            } 
        }
    }
}