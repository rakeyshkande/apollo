package com.ftd.accountingreporting.util;

import java.util.ArrayList;

/**
 * This Class is an utility class.
 *
 * @author Madhusudan Parab
 */
public class LineItemGenerator 
{
  /**
   * constructor for class LineItemGenerator
   */
  public LineItemGenerator()
  {
  }
  
  /**
   * This method gets the line in specified in 'delim'
   * @param fieldList ArrayList
   * @param delim String
   * @return String
   */
  public String getDelimitedString(ArrayList fieldList, String delim)
  {
     StringBuffer sb = new StringBuffer();
     for (int i = 0; i < fieldList.size(); i++)
     {
         if (i!=0)
            sb.append(delim);
         sb.append((String)fieldList.get(i));
     }
     return sb.toString();
  }
}