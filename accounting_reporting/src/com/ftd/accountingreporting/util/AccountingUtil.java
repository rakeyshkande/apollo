package com.ftd.accountingreporting.util;

import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.dao.CommonDAO;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.vo.NotificationVO;
import com.ftd.security.SecurityManager;

import java.io.IOException;
import java.io.StringWriter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import java.util.List;
import javax.jms.JMSException;
import javax.naming.InitialContext;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.xpath.XPathAPI;

import org.jdom.CDATA;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.traversal.NodeIterator;

import org.xml.sax.SAXException;

/**
 * This Class is an utility class.
 *
 * @author Madhusudan Parab
 */
public class AccountingUtil 
{
    private static Logger logger  = new Logger("com.ftd.accountingreporting.util.AccountingUtil");
    
    /**
     * constructor for class AccountingUtil
     */
    public AccountingUtil()
    {
    }
    
    /**
    * A record is ready to bill if the order disposition is not In-Scrub or Held. 
    * Moreover if the order is part of shopping cart none of the orders 
    * disposition can be in In-Scrub or Held
    * @param paymentId long
    * @returns boolean true or false
    * @throws Exception
    */
    public boolean isReadyToBill(CommonDAO dao, String master_order_number) throws Exception
    {
        String statuses = "'In-Scrub','Pending','Held','Validated'";
        boolean notReady = dao.isShoppingCartInStatus(master_order_number, 
            statuses);
        return !notReady;
    }    
    
   /**
    * This method checks whether payment method is by credit card('AX' or 'DC' or 'DI' or 'MC' or 'VI' or 'CB')
    * @param paymentMethod
    * @return boolean
    */
    public boolean isCreditCardPayment(String paymentMethod)
    {
        if (paymentMethod.equals("AX")||
            paymentMethod.equals("DC")||
            paymentMethod.equals("DI")||
            paymentMethod.equals("MC")||
            paymentMethod.equals("VI")||
            paymentMethod.equals("CB"))
            return true;
        return false;
    }
  
  /**
   * This method checks whether payment Method equals 'IN', 'GC' or 'NC'
   * @param paymentMethod
   * @return boolean
   */
   public boolean isNonCreditCardPaymentType(String paymentMethod) throws Exception 
   {
     if (paymentMethod.equals("IN") ||
         paymentMethod.equals("GC") ||
         paymentMethod.equals("NC"))
         return true;
     return false;
   }
   
   /* Comepares two Calendar date. 
    * Returns 0 if two Calendar objects represent the same day.
    * Returns 1 if a is after b.
    * Returns -1 if a is before b.
    */
   public int compareDates(Calendar a, Calendar b) throws Exception {
      int result = -1;
      String DATE_FORMAT = "yyyy-MM-dd";
      SimpleDateFormat sdf = new java.text.SimpleDateFormat(DATE_FORMAT);
      String aDate = sdf.format(a.getTime());
      String bDate = sdf.format(b.getTime());
      
      if(aDate.equals(bDate)) {
          result = 0;
      } else if (a.after(b)) {
          result = 1;
      } // else a is before b. return -1.
      return result;
   }
   
    /**
     *  This method will calculate a date based on the 
     *  current system date and the value of offset parameter for the day 
     *  It will be formatted to MMddyy.
     * @param dayOffSet - int
     * @return Date
     * @throws IOException
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws TransformerException
     * @throws XSLException
     * @throws ParseException
     */
    public Date calculateOffSetDate(int dayOffSet) 
        throws IOException, SAXException, ParserConfigurationException, 
        TransformerException, ParseException
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering calculateOffSetDate");
        }
        
        Date offsetDate;
        
        try{
            /* retrieve system date */
            GregorianCalendar currentDate = new GregorianCalendar(); 
            /* change date based on batch date offset */
            currentDate.add(GregorianCalendar.DATE, dayOffSet);
            /* format date */
            Date d = currentDate.getTime();
            SimpleDateFormat sdfOutput = 
                       new SimpleDateFormat (
                       ARConstants.AAFES_BATCH_DATE_FORMAT);
            
            String s = sdfOutput.format(d);
            offsetDate = sdfOutput.parse(s);
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting calculateOffSetDate");
            } 
       } 
       return offsetDate;
    }

   /**
     *  This method will calculate a date based on the 
     *  current system date and the value of offset parameter for the day 
     *  It will be formatted to MMddyy.
     * @param dayOffSet - int
     * @return Date
     * @throws IOException
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws TransformerException
     * @throws XSLException
     * @throws ParseException
     */
    public Date calculateOffSetDatebyMonth(int monthOffSet) 
        throws IOException, SAXException, ParserConfigurationException, 
        TransformerException, ParseException
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering calculateOffSetDate");
        }
        
        Date offsetDate;
        
        try{
            /* retrieve system date */
            GregorianCalendar currentDate = new GregorianCalendar(); 
            /* change date based on batch date offset */
            currentDate.add(GregorianCalendar.MONTH, monthOffSet);
            /* format date */
            Date d = currentDate.getTime();
            SimpleDateFormat sdfOutput = 
                       new SimpleDateFormat (
                       "MM/dd/yyyy");
            
            String s = sdfOutput.format(d);
            offsetDate = sdfOutput.parse(s);
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting calculateOffSetDate");
            } 
       } 
       return offsetDate;
    }
    /**
     *  removes the CS coupon types for op users
     * @param couponTypes - Document
     * @param request - HttpServletRequest
     * @return n/a
     * @throws Exception
     */
    public void operationsCouponTypes(Document couponTypes, HttpServletRequest request)
        throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering operationsCouponTypes");
        }
        try{
            /* check if user is a op user */
            String user = request.getParameter(ARConstants.COMMON_GCC_USER)!=null?request.getParameter(ARConstants.COMMON_GCC_USER):"";
            if (user.equals(ARConstants.CONST_USER_OP))
            {
                /* remove GC Coupon Type CS */
                  NodeIterator nl = XPathAPI.selectNodeIterator(couponTypes, "GC_COUPON_TYPES/GC_COUPON_TYPE");
                  /* find the node for coupon type CS */
                  for (Node gccTypeNode=nl.nextNode(); gccTypeNode!=null;) {
                        /* retrieve the coupon type and check if it is CS */
                        String gccType = gccTypeNode.getFirstChild().getFirstChild().getNodeValue();
                        if(gccType.equals("CS"))
                        {
                            /* delete node from document for CS */
                            couponTypes.getFirstChild().removeChild(gccTypeNode);
                            gccTypeNode = null;
        
                        }
                        else
                        {
                            /* go to next node */
                            Node next = nl.nextNode();
                            gccTypeNode = next;
                        }
                   }
                
            }
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting operationsCouponTypes");
            } 
       } 
    }
    
    /**
     * This method retrieves the csr id from the security token. 
     * Calls SecurityManager.getUserInfo(session_id).getUserID().
     * @param session_id - String
     * @return String
     * @throws Exception
     * @todo: finish
     */
    public String getCsrID(String session_id) throws Exception
    {
        
        String userId = "";
        
        if(logger.isDebugEnabled()) {
            logger.debug("Entering getCsrID");
            logger.debug("Session ID: " + session_id);
        }
        
        try{
            if (SecurityManager.getInstance().getUserInfo(session_id)!=null)
            {
                userId = SecurityManager.getInstance().getUserInfo(session_id).getUserID();
            }
        
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getCsrID");
            } 
        } 
        
        return userId;
    }

    /**
     * Convert date of yyyy-MM-dd to MM/dd/yyyy for display.
     */
    public String formatDate(String dateToFormat) throws Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering formatDate");
        }
        String formattedDate = "";
        try{
            SimpleDateFormat sdfOutput=new java.text.SimpleDateFormat("yyyy-MM-dd");
            Date d=null;
            d=sdfOutput.parse(dateToFormat);
            sdfOutput=new java.text.SimpleDateFormat("MM/dd/yyyy");
            formattedDate = sdfOutput.format(d);

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting formatDate");
            } 
        }
        return formattedDate;
    }
    
  /**
   * Chop the string s after the first occurance of char c
   * @param s
   * @param c
   * @return 
   */
    public String chop(String s, char c) throws Exception {
        int pos = s.indexOf(c);
        
        if(pos != -1 && pos < s.length()) {
            s = s.substring(0, pos);
        }
        return s;
    }
    
   /**
    * method checks if deadline has passed for a recon process based on 
    * the deadline day
    * @param deadlineDay - String
    * @return boolean
    */
    public boolean checkProcessDeadline(int deadlineDay, String process) throws Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering processDeadline");
        }
        boolean deadlinePassed = false;
        try{
            /* retrieve system date */
            GregorianCalendar currentDate = new GregorianCalendar();
            int dayOfMonth = currentDate.get(GregorianCalendar.DAY_OF_MONTH);
            if(dayOfMonth > deadlineDay)
            {
                deadlinePassed = true;     
                /* send notification e-mail that deadline has passed */
                NotificationVO notifVO = new NotificationVO();
                /* set up e-mail */
                notifVO.setAddressSeparator(ConfigurationUtil.getInstance().getProperty(
                    ARConstants.EMAIL_CONFIG_FILE,"recon_address_separator"));
                notifVO.setMessageTOAddress(ConfigurationUtil.getInstance().getFrpGlobalParm(
                    ARConstants.CONFIG_CONTEXT,"recon_email_addresses"));
                notifVO.setMessageFromAddress(ConfigurationUtil.getInstance().getProperty(
                    ARConstants.EMAIL_CONFIG_FILE,"recon_from_address"));
                notifVO.setSMTPHost(ConfigurationUtil.getInstance().getFrpGlobalParm(
                    ARConstants.CONFIG_CONTEXT,"recon_mail_server"));
                notifVO.setMessageSubject(process + " " + ConfigurationUtil.getInstance().getProperty(
                    ARConstants.EMAIL_CONFIG_FILE,"recon_process_deadline_subject"));
                notifVO.setMessageContent(process + ConfigurationUtil.getInstance().getProperty(
                    ARConstants.EMAIL_CONFIG_FILE,"recon_process_deadline_content"));
                /* send e-mail */
                EmailUtil emaiUtil = new EmailUtil();
                emaiUtil.sendEmail(notifVO);
            }
            
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting processDeadline");
            } 
        }
        return deadlinePassed;
    }
    
  
  /**
   * Sends a payload off into FTD JMS queue land
   * This method will 
   * @param eventHandler 
   * @param eventName event name
   * @param payload xml document that will be put into the payload.
   * @throws com.ftd.amazon.api.AmazonException
   */
   
  public static void queueToEventsQueue(String contextName, String eventName, String delay, org.w3c.dom.Document payload) throws Exception
  { 
  
      try  {
          queueToEventsQueue(contextName, eventName, delay, payload, "EVENTS_QUEUE");
      } catch (Exception ex)  {
            throw new Exception("enqueueJmsMessage caught exception: " + ex);
      }    
       
  }
  
   /**
    * Sends a payload off into FTD JMS queue land
    * @param eventHandler 
    * @param eventName event name
    * @param payload xml document that will be put into the payload.
    * @throws com.ftd.amazon.api.AmazonException
    */
    
   public static void queueToEventsQueue(String contextName, String eventName, String delay, org.w3c.dom.Document payload, String pipeline) throws Exception
   { 
   
       try  {
         org.jdom.Element root = new Element("event");
         Namespace xsi = Namespace.getNamespace("xsi","http://www.w3.org/2001/XMLSchema-instance");
         root.setAttribute("noNamespaceSchemaLocation","event.xsd",xsi);
         root.addContent(new Element("event-name").setText(eventName));
         root.addContent(new Element("context").setText(contextName));
         
         Element ePayload = new Element("payload");
         XMLOutputter xml;
         Format format;
         StringWriter sw;
         if( payload!=null )
         {
           xml = new XMLOutputter();
           format = Format.getCompactFormat();
           format.setOmitDeclaration(true);
           format.setOmitEncoding(true);
           xml.setFormat(format);
           sw = new StringWriter();
         
           xml.output(XMLUtil.w3c2jdom(payload),sw);
           
           CDATA cdata = new CDATA(sw.toString());
           ePayload.setContent(cdata);  
         }
         
         root.addContent(ePayload);      
         org.jdom.Document doc = new org.jdom.Document(root);
         
         xml = new XMLOutputter();
         format = Format.getCompactFormat();
         format.setOmitDeclaration(true);
         format.setOmitEncoding(true);
         xml.setFormat(format);
         sw = new StringWriter();
         xml.output(doc,sw);
         
         MessageToken messageToken = new MessageToken();
         // name of the pipeline
         messageToken.setStatus(pipeline);
         messageToken.setMessage(sw.toString());
         messageToken.setProperty("CONTEXT", contextName, "java.lang.String");
         messageToken.setProperty("JMS_OracleDelay", delay, "long");
         //messageToken.setJMSCorrelationID(contextName + " " + eventName);
         Dispatcher dispatcher = Dispatcher.getInstance();
         dispatcher.dispatchTextMessage(new InitialContext(), messageToken);
       } catch (Exception ex)  {
             throw new Exception("enqueueJmsMessage caught exception: " + ex);
       }  
  }
  
  /**
   * Formats a string dollar amount to the format of $#,###,###.00.
   * @param amount
   * @return String
   * @throws java.lang.Exception
   */
  public static String formatAmount(String amount) throws Exception
  {
      String output = "";
      String pattern = "$#,###,###.00";
      
      output = formatAmountToPattern(amount, pattern);
      return output;
  }
 
  /**
   * Formats a string dollar amount to the specified format. Default $###.##.
   * @param amount
   * @return String
   * @throws java.lang.Exception
   */
  public static String formatAmountToPattern(String amount, String pattern) throws Exception
  {
      logger.debug("amount is: " + amount);
      logger.debug("pattern is: " + pattern);
      DecimalFormat myFormatter = new DecimalFormat(pattern);
      double doubleAmount = 0;
      String output = "$0.00";
      
      try 
      {
          doubleAmount = Double.valueOf(amount.trim()).doubleValue();
          output = myFormatter.format(doubleAmount);
         
      } catch (NumberFormatException nfe) {
         
          // ignore. return initialized value.
          logger.error(nfe);
      } catch (IllegalArgumentException iae) {
          // ignore. return initialized value.
          logger.error(iae);
      }
      return output;
  } 
  
   /**
   * Take a date string and convert it to a Calendar date.
   * @param dateString is any combination of mm/dd/yyyy and m/d/yy
   * @return 
   * @throws java.lang.Exception
   */
   public Calendar formatDateStringToCal(String dateString) throws Exception {
      Calendar cal =  null;
      SimpleDateFormat inputFormat = new SimpleDateFormat("MM/dd/yyyy");
      cal = Calendar.getInstance(); 
      
      // Get the year token from the date string. 
      String year =  dateString.substring(dateString.lastIndexOf("/") + 1);
      
      // If year is yy, convert it to yyyy.
      if(year.length() == 2) {
          // Get current year and add the first two digits to it.
          String currentY = String.valueOf(cal.get(cal.YEAR));
          year = currentY.substring(0, 2) + year;
          
          // Update year in date string
          dateString = dateString.substring(0, dateString.lastIndexOf("/") + 1) + year;
      }
      cal.setTime(inputFormat.parse(dateString));
      
      return cal;
   }  
   
    public boolean isWeekend() throws Exception {
        // returns true if today is not a Saturday or Sunday
        Calendar cal = Calendar.getInstance();
        int day = cal.get(Calendar.DAY_OF_WEEK);
        boolean isWeekend = false;
        
        if(day == Calendar.SATURDAY || day == Calendar.SUNDAY) {
            isWeekend = true;
        }
        return isWeekend;
    }   
    
    public String getCurrentDateTime() throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("MMddyy_HHmmss");
        return sdf.format(new Date());
    }
    
    public  static int getCurrentOpenCursors(Connection conn) {
    PreparedStatement psQuery = null;
    ResultSet rs = null;
    
    int cursors = -1;
    try {
      String sqlQuery = "select count(*) AS COUNT from sys.v_$open_cursor where user_name like 'osp' or user_name like 'OSP'";
      psQuery = conn.prepareStatement(sqlQuery);
      rs = psQuery.executeQuery();
      
      if (rs.next()) {
        cursors = rs.getInt("COUNT");
      }
    } catch (SQLException e) {
      logger.error(e);
    } finally {
      try {
        if (rs != null) {rs.close();}
        if (psQuery != null) {psQuery.close();}
      }
      catch (SQLException ex)  {
        logger.error(ex);
      }
    }
    return cursors;
  }
  
  /**
   * Dispatches a JMS message
   * @param message
   * @throws NamingException
   * @throws JMSException
   * @throws java.lang.Exception
   */
   /*
    public static void dispatchJMSMessageList(String queueStatus, List messageList, long timeToLive)
        throws NamingException, JMSException, Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering dispatchJMSMessageList");
            logger.debug("message size: " + messageList.size());
        }
        String message = null;
        MessageToken messageToken = null;
        List msgTokenList = new ArrayList();

        try{
          for(int i=0; i<messageList.size(); i++) {
            message = (String)(messageList.get(i));
            messageToken = new MessageToken();
            messageToken.setStatus(queueStatus);
            messageToken.setMessage(message); 
            if(timeToLive != 0) {
                messageToken.setJMSExpiration(timeToLive);
            }
            msgTokenList.add(messageToken);
          }
          Dispatcher dispatcher = Dispatcher.getInstance();

          // enlist the JMS transaction
          dispatcher.dispatchTextMessages(new InitialContext(), msgTokenList); 
        } finally
        {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting dispatchMessage");
            }
        }
    }  
    
    /**
     * Dispatches a JMS message
     * @param message
     * @throws NamingException
     * @throws JMSException
     * @throws java.lang.Exception
     */
     
      public static void dispatchJMSMessageList(String queueStatus, List messageList, long timeToLive, long delay)
          throws NamingException, JMSException, Exception
      {
          if(logger.isDebugEnabled()){
              logger.debug("Entering dispatchJMSMessageList");
              logger.debug("message size: " + messageList.size());
          }
          String message = null;
          MessageToken messageToken = null;
          List msgTokenList = new ArrayList();

          try{
            for(int i=0; i<messageList.size(); i++) {
              message = (String)(messageList.get(i));
              messageToken = new MessageToken();
              messageToken.setStatus(queueStatus);
              messageToken.setMessage(message); 
              if(timeToLive != 0) {
                  messageToken.setJMSExpiration(timeToLive);
              }
              
              if(delay != 0) {
                  messageToken.setProperty("JMS_OracleDelay", String.valueOf(delay), "long");
              }
              msgTokenList.add(messageToken);
            }
            Dispatcher dispatcher = Dispatcher.getInstance();

            // enlist the JMS transaction
            dispatcher.dispatchTextMessages(new InitialContext(), msgTokenList); 
          } finally
          {
              if(logger.isDebugEnabled()){
                 logger.debug("Exiting dispatchMessage");
              }
          }
      }      
    
}