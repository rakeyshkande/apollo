package com.ftd.accountingreporting.util;

import com.ftd.osp.utilities.j2ee.ResourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.net.URL;
import java.net.URLDecoder;

import javax.xml.parsers.ParserConfigurationException;
import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * This Class is a utility class that reads data in XML file.
 *
 */
public class ConfigFileReader 
{
    private static Logger logger  = new Logger("com.ftd.accountingreporting.util.ConfigFileReader");
    private static ConfigFileReader SINGLETON_READER = null;
    private static Document rootDoc = null;
    
    /**
     * private constructor
     */
    private ConfigFileReader()
    {
    }
    
    public Document getRootDoc() {
        return rootDoc;
    }
    
    public static ConfigFileReader getInstance(String _fileName) {
        
        if(SINGLETON_READER == null) {
            SINGLETON_READER = new ConfigFileReader();
        } 
        
        URL url2 = ResourceUtil.getInstance().getResource(_fileName);
        if (url2 != null)
        {
            File configFile = new File(new URLDecoder().decode(url2.getFile()));
            try {
	            try {
	                rootDoc = DOMUtil.getDocument(configFile);
	            } catch (FileNotFoundException fe) {
	            	try {
	            		rootDoc = (Document)DOMUtil.getDocument(ConfigFileReader.class.getClassLoader().getResourceAsStream(_fileName));
	            	} catch(Exception ee) {
	            		throw new IOException("Cannot read file." + _fileName);
	            	}
	                logger.info("configutil created doc from resource stream");
	
	            }  catch (Exception ex) {
	            	throw ex;
	            }
            } catch (IOException e) {
                logger.error(e);
            } catch (SAXException se) {
                logger.error(se);
            } catch (ParserConfigurationException pe) {
                logger.error(pe);
            } catch (Exception ep) {
            	logger.error(ep);
            }
            //requeueMsgsNodeList = requeueMsgsDoc.selectNodes(requeueMsgsXPath);
        }
        else
        {
            logger.error(new IOException(_fileName + " file could not be found."));
        }      
        return SINGLETON_READER;
    }
    
    public String getSingleNodeValue(String path) throws Exception {
        String value = null;
        
        Node node = XPathAPI.selectSingleNode(rootDoc, path);
        if(node != null) {
            value = node.getNodeValue();
        }
        
        return value;        
    }
    

}