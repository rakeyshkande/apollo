package com.ftd.accountingreporting.util;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;

import java.text.SimpleDateFormat;

import java.util.Calendar;

/**
 * This class is an utility class for handling file compression operations.
 *
 * @author Charles Fox
 */
public class FileArchiveUtil 
{
    private static Logger logger  = 
        new Logger("com.ftd.accountingreporting.util.FileArchiveUtil");

    /**
    * Move all files from fromDir to toDir, appending a timestamp
    * @param fromDir String
    * @param toDir String
    * @returns n/a
    * @throws IOException
    * @throws Exception
    */
    public static void moveToArchive(String fromDir, String toDir, boolean appendTimestamp) 
        throws IOException, Exception
    {
        logger.debug("Entering moveToArchive...");
        File[] oldFiles = null;
        File oldFile = null;
        String fileName = null;
        
        // Get current date and format to MMddyy
        SimpleDateFormat sdf = new SimpleDateFormat("MMddyy_HHmmss");
        String timestamp = sdf.format(Calendar.getInstance().getTime());
         
        logger.debug("Archiving files from directory: " + fromDir);
        // Retrieve list of files to be archived
        oldFiles = (new File(fromDir)).listFiles();
        
        if(oldFiles != null) {
            // Create destination directory
            File toFile = new File(toDir);
            logger.debug(" to directory: " + toDir);
            
            // for each file, move to the archive directory
            for(int i = 0; i < oldFiles.length; i++) {
                if(oldFiles[i].isFile()) {
                    fileName = oldFiles[i].getName();
                    if(appendTimestamp) {
                        fileName = fileName + "_" + timestamp;
                    }
                    logger.debug("Archiving file " + fileName);
                    oldFiles[i].renameTo(new File(toFile, fileName));
                }
            }
        }
    }
    
    

    
    /**
    * Overwrites contents of a file at the specified line column position.
    * Note: Header data is preserved, but trailer data may be affected.
    * @param value
    * @param column
    * @param headerLineCount
    * @param inputFile
    */
    public static void overwriteFileContents(String inputFile,
        int headerLineCount, int trailerLineCount, int column, String value) throws Exception {
        if (column == 0) {
            throw new RuntimeException(
                "overwriteFileContents: Column position input value can not be zero.");
        }
 
      
        
            // Open the file for writing.
            RandomAccessFile pointer = new RandomAccessFile(inputFile, "rw");
 
            //logger.debug("overwriteFileContents: At file position: " +
            //    pointer.getFilePointer());
 
            for (int count = 0; count < headerLineCount; count++) {
                pointer.readLine();
            }
 
            logger.debug(
                "overwriteFileContents: After headers, at file position: " +
                pointer.getFilePointer());
 
            while (pointer.skipBytes(column) == column) {
                //logger.debug(
                //    "overwriteFileContents: After bytes skip, at file position: " +
                //    pointer.getFilePointer());
                pointer.write(value.getBytes());
                logger.debug(
                    "overwriteFileContents: After write, at file position: " +
                    pointer.getFilePointer());
 
                // Read to end-of-current-line
                String endOfLineString = pointer.readLine();
                // logger.debug("overwriteFileContents: Reading line: " +
                //    endOfLineString);
 
                // Record the length.
                long offset = pointer.getFilePointer();
                logger.debug("overwriteFileContents: At file position: " +
                    offset);
 
                // The folowing logic preserves trailer lines.
                // Read forward to check for any trailers and EOF.
                if (trailerLineCount > 0) {
                    String forwardLine = null;
 
                    // Read one more line than the trailer count.
                    for (int count = 0; count <= trailerLineCount; count++) {
                        forwardLine = pointer.readLine();
                    }
                    
                    if (forwardLine == null) {
                        // EOF is reached.
                        break;
                    } else {
                        // Restore the pointer to the previous location.
                        pointer.seek(offset);
                    }
                }
            }
        
    }
    
    /**
     * Overwrites contents of a file for a specific record type at the specified line column position.
     * Note: Header data and trailer data is preserved, as well as all other record types other than D
     * @param inputFile
     * @param column
     * @param value
     */
    public static void overwriteBAMSFileContent(String inputFile, int column, String value, String lineChar) throws Exception {
        // Open the file for writing.
        RandomAccessFile pointer = new RandomAccessFile(inputFile, "rw");
        long currentFileOffset = 0;
        while(pointer != null){
        	
        	 currentFileOffset = pointer.getFilePointer();
        	 String currentRowFromFile = pointer.readLine();
        	 if (currentRowFromFile == null) {
                 // EOF is reached.
        		 break;
             } 
        	 //Mask Credit Card Number if the record id matches the input 
        	 if (currentRowFromFile.startsWith(lineChar))
        	 {
        		 pointer.seek(currentFileOffset);
        		 pointer.skipBytes(column);
    	         pointer.write(value.getBytes());
        	  }
        
       }
    }

}