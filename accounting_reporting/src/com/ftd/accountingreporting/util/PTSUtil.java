package com.ftd.accountingreporting.util;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.dao.EODDAO;
import com.ftd.accountingreporting.exception.EmailDeliveryException;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.NotificationVO;

public class PTSUtil {
	
	private static Logger logger  = 
        new Logger("com.ftd.accountingreporting.util.PTSUtil");

    public PTSUtil()
    {
    }
    private static Map<String, Integer> unSignedMap = new HashMap<String, Integer>();
    public static String formatDate(Date date, String pattern) throws Exception
    {
  	  Calendar cal  = Calendar.getInstance();
  	    cal.setTime(date);
  	    
  	    SimpleDateFormat sdf = new SimpleDateFormat(pattern);
  	    String dateStr = sdf.format(date);
  	    return dateStr;
    }
    
    public static String getDateStringByFormat(String dateStr, String inPattern, String outPattern) throws Exception{
		SimpleDateFormat inSDF = new SimpleDateFormat(inPattern);
		SimpleDateFormat outSDF = new SimpleDateFormat(outPattern);
		Date dt = inSDF.parse(dateStr);
		String outDate = outSDF.format(dt);
		
		return outDate;
	}
    
    public static Map<String, String> getComapanyMap() throws Exception
    {
    	ConfigurationUtil cu = ConfigurationUtil.getInstance();
    	String  companies = cu.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, ARConstants.PTS_SETTLEMENT_COMPANIES);
    	Map<String, String> companyMap = new HashMap<String, String>();
    	String [] companyArr = companies.split(","); 
    	int i = 1;
    	for (String company : companyArr) {
    		if(company != null){
    			company = company.trim();
    		}
    		companyMap.put(company, String.valueOf(i));
    		i = i+1;
    	}
    	return companyMap;
    }
    
    public static Map<String, String> getPaymentExtMapByPaymentId(long paymentId) throws Exception
    {
    	Map<String, String> paymentExtMap = new HashMap<String, String>();
    	DataSourceUtil dsu = DataSourceUtil.getInstance();
    	Connection conn = null;
    	try{
    		conn = dsu.getConnection(ConfigurationUtil.getInstance().getProperty(ARConstants.CONFIG_FILE,ARConstants.DATASOURCE_NAME));
    		//To-Do logic to fetch attributes from payemnt_ext
    		EODDAO eodDAO = new EODDAO(conn);
    		CachedResultSet rs = eodDAO.doGetPaymentExtByPaymentId(paymentId);
    		if(rs != null){
    			while (rs.next()){
    				String key = rs.getString("AUTH_PROPERTY_NAME");
    				String value = rs.getString("AUTH_PROPERTY_VALUE");
    				paymentExtMap.put(key, value);
    			}
    		}
    		
    		
    	}catch(Exception e){
    		logger.error("Error occured while fetching payment ext table values for a payment id."+paymentId);
    		throw e;
    	}finally{
    		dsu.close(null, null, conn);
    	}
    	return paymentExtMap;
    }
    
    public static int getNextSequenceNumber(int currSeqNumber) throws Exception
    {
    	ConfigurationUtil cu = ConfigurationUtil.getInstance();
		int maxSeqNumber = Integer.parseInt(cu.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, "MAX_PTS_SEQ_NUMBER"));
    	int nextSeqNumber = 0;
    	if(currSeqNumber == maxSeqNumber){
    		nextSeqNumber = 0;
    	}else{
    		nextSeqNumber = currSeqNumber + 1;
    	}
    	return nextSeqNumber;
    }
    static {
    	logger.debug("Entered unSignedMap initialization");
    	unSignedMap.put(" ", 0);
    	unSignedMap.put("{", 0);
    	unSignedMap.put("A", 1);
    	unSignedMap.put("B", 2);
    	unSignedMap.put("C", 3);
    	unSignedMap.put("D", 4);
    	unSignedMap.put("E", 5);
    	unSignedMap.put("F", 6);
    	unSignedMap.put("G", 7);
    	unSignedMap.put("H", 8);
    	unSignedMap.put("I", 9);
    	logger.debug("Completed unSignedMap initialization");
    	
    }
    public static String getACKUnsignedValue(String signedKey) throws Exception{
    	String retVal = null;
    	logger.debug("singedKey : "+signedKey);
    	if(signedKey == null){
    		throw new Exception("Signed value can not be null.");
    	}
    	if(unSignedMap != null && unSignedMap.containsKey(signedKey)){
    		retVal = String.valueOf(unSignedMap.get(signedKey));
    	}
    	return retVal;
    	
    }
    
    public static String parseSignedValues(String signedValue) throws Exception{
    	StringBuffer sb = new StringBuffer();
    	if(signedValue != null){
    		for(int i = 0; i < signedValue.length(); i++){
    			String str = signedValue.substring(i, i + 1);
    			if(!" ".equals(str) && StringUtils.isNumeric(str)){
    				sb.append(str);
    			}else{
    				sb.append(getACKUnsignedValue(str));
    			}
    		}
    	}
    	logger.debug("Amount : "+sb.toString());
    	return sb.toString();
    }
    
    public static void sendEmail(String toAddress, String fromAddress, String subject, String content) throws EmailDeliveryException {
        try {
            NotificationVO notifVO = new NotificationVO();

            notifVO.setMessageTOAddress(toAddress);
            notifVO.setMessageFromAddress(fromAddress);
            notifVO.setSMTPHost(ConfigurationUtil.getInstance().getFrpGlobalParm(ARConstants.MESSAGE_GENERATOR_CONFIG, ARConstants.SMTP_HOST_NAME));
            notifVO.setMessageSubject(subject);
            notifVO.setMessageContent(content);
            
            /* send e-mail */
            EmailUtil emaiUtil = new EmailUtil();
            emaiUtil.sendEmail(notifVO);
        } catch (Exception e) {
            throw new EmailDeliveryException(e.getMessage());
        }  
    }
    
    public static double amountStringToDbl(String amt) throws Exception{
    	double retVal = 0.0;
    	if(amt != null){
    		String centsVal = amt.substring(amt.length()-2);
    		String dollarVal =  amt.substring(0, amt.length()-2);
    		if(dollarVal != null && centsVal != null){
    			retVal = new Double(dollarVal+"."+centsVal);
    		}
    		
    	}
    	return retVal;
    }


}
