package com.ftd.accountingreporting.util;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
/**
 * This Class is an EOD utility class.
 */
public class EODUtil 
{

  private static final int PAD_RIGHT = 0; 
  private static final int PAD_LEFT = 1; 
  private static Logger logger  = 
        new Logger("com.ftd.accountingreporting.eventhandler.ENDUtil");
  /**
   * constructor for class EODUtil
   */
  public EODUtil()
  {
  }

  /**
   * This method pads extra chars either left or right for the size specified
   * @param value current value
   * @param padside specifies whether to pad left or right
   * @param padChar  the char to pad
   */
  public static String pad(String value,int padSide, String padChar, int size)
  {
   //null check
   if(value == null)
      value = "";
     
   String padded = value;       
  
   if(value.length() > size) {
      if(padSide == PAD_RIGHT) {
        padded = value.substring(0,size);
      }
      else {
        padded = value.substring(padded.length()-size);
      }
   }
   else
   {
     for(int i=value.length();i<size;i++)
     {
       if(padSide == PAD_LEFT)
          padded = padChar + padded;
       else
          padded = padded + padChar;
     }//end for loop
   }//end else less then max size
  
   return padded;
  }

  /**
   * This method removes decimal point from double amount.
   * @param value double
   * @return formatted decimal amount without decimal
   */
  public static String formatDoubleAmount(double value)
  {

   DecimalFormat df = new DecimalFormat("#.00");
   String formatted = df.format(value);
   
    //remove decimal
   formatted = replace(formatted,".","");
   return formatted;
  }
    
  /**
   * String replace function
   * 
   * @param String source string
   * @param String what will be replaced
   * @param String what 2nd param will be replaced with
   * @returns String new string
   */
  public static String replace(String strSource, String strFrom, String strTo)
  {
    if(strSource == null || strSource.equals(""))
        return "";
    String strDest = "";
    int intFromLen = strFrom.length();
    int intPos;

    while((intPos = strSource.indexOf(strFrom)) != -1)
    {
        strDest = strDest + strSource.substring(0,intPos);
        strDest = strDest + strTo;
        strSource = strSource.substring(intPos + intFromLen);
    }
    strDest = strDest + strSource;

    return strDest;
  }
  
 /**
  * This method converts the java.sql.Date to java.util.Calendar
  * @param sqlDate java.sql.Date
  * @return calDate java.util.Calendar
  * @throws Exception
  */
  public static Calendar convertSQLDate(java.sql.Date sqlDate)
  {
    Calendar calDate = Calendar.getInstance();
    calDate.setTimeInMillis(sqlDate.getTime());
    return calDate;
  }
  
  public static Calendar convertLongDate(long longDate) throws Exception
  {
     Calendar batchDate = Calendar.getInstance();
     batchDate.setTime(new Date(longDate));
     return batchDate;
  }
  
  
  
  
  
    /**
     * Create a connection to the database
     * @param n/a
     * @return java.sql.Connection
     * @throws Exception
     */
    public static Connection createDatabaseConnection() 
        throws IOException, SAXException, ParserConfigurationException, 
            Exception
    {

        Connection connection=null;
        try{
            /* create a connection to the database */
            connection = DataSourceUtil.getInstance().
                getConnection(ConfigurationUtil.getInstance().getProperty(
                ARConstants.CONFIG_FILE,ARConstants.DATASOURCE_NAME));
                
        }finally{
        }
        
        return connection;
    }
 
    public static void closeConnection(Connection conn) throws SQLException, Exception
    {
        try
        {
          if(conn != null && !conn.isClosed()) {
              conn.close();
          }

        }finally
        {
        }    
    }
    
  /**
   * Sends a system message
   * 
   * @param t Throwable t
   */
   /*
    public static void sendSystemMessage(Connection con, Throwable t)
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering sendSystemMessage");
        }
        try {
            String logMessage = t.getMessage();  
      
            SystemMessager.send(logMessage, t, ARConstants.EOD_BILLING_PROCESS, 
                SystemMessager.LEVEL_PRODUCTION, 
                ARConstants.EOD_BILLING_PROCESS_ERROR_TYPE, con);
        
        } catch (Exception ex) {
            logger.error(ex);
        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting sendSystemMessage");
            } 
        }   
    }    
    */
}