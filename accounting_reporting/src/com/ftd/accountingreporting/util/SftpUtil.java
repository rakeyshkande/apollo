package com.ftd.accountingreporting.util;


import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Vector;

import com.ftd.osp.utilities.plugins.Logger;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

public class SftpUtil  {

	private static Logger logger = new Logger(SftpUtil.class.getName());
	

	public SftpUtil() {
		super();
	}
	
	//Use JSCH to sftp the file
	public void uploadFile(File file, String remoteFilePath, String ftpServer,String username,String password) throws Exception
	{
		Session 	session 	= null;
		Channel 	channel 	= null;
		ChannelSftp channelSftp = null;
	

		try{
			JSch jsch = new JSch();
			session = jsch.getSession(username,ftpServer);
			session.setPassword(password);
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.connect();
			channel = session.openChannel("sftp");
			channel.connect();
			channelSftp = (ChannelSftp)channel;		
			channelSftp.cd(remoteFilePath);
			channelSftp.put(new FileInputStream(file), file.getName());
			
			channelSftp.disconnect();
			channel.disconnect();
			session.disconnect();
		}
		catch(Exception ex){			
			throw new Exception("Upload of file failed");
		}
	}
	
	
	
	/**
	 This method is used when sending the settlement file to BAMS
	 */
	public void uploadFileviaSSH(File file, String remoteFilePath,
			String ftpServer, String username, String privatekey,
			String passphrase, String sftpPassword) throws Exception {
		Session 	session 	= null;
		Channel channel = null;
		ChannelSftp channelSftp = null;

		try {
			JSch jsch = new JSch();
			jsch.addIdentity(privatekey, passphrase);
			session = jsch.getSession(username, ftpServer, 6522);
			session.setPassword(sftpPassword);
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.connect();
			channel = session.openChannel("sftp");
			channel.connect();
			channelSftp = (ChannelSftp) channel;
			channelSftp.cd(remoteFilePath);
			channelSftp.put(new FileInputStream(file), file.getName());

			channelSftp.disconnect();
			channel.disconnect();
			session.disconnect();
		} catch (Exception ex) {
			throw new Exception("Upload of file failed", ex);
			
			
		}
	}


	
public void uploadFiles(List<File> files, String remoteFilePath, String ftpServer,String username,String password) throws Exception
	{
		Session 	session 	= null;
		Channel 	channel 	= null;
		ChannelSftp channelSftp = null;
	

		try{
			JSch jsch = new JSch();
			session = jsch.getSession(username,ftpServer);
			session.setPassword(password);
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.connect();
			channel = session.openChannel("sftp");
			channel.connect();
			channelSftp = (ChannelSftp)channel;		
			channelSftp.cd(remoteFilePath);
			for(File file : files){
				channelSftp.put(new FileInputStream(file), file.getName());
			}
			channelSftp.disconnect();
			channel.disconnect();
			session.disconnect();
		}
		catch(Exception ex){			
			throw new Exception("Upload of file failed", ex);
		}finally{
			if(session != null && session.isConnected()){
				session.disconnect();
			}
			if(channel != null && channel.isConnected()){
				channel.disconnect();
			}
			
		}
	}
	
	
	
	public void downloadFilesToLocal(String filePattern, String remoteFilePath, String localFilePath, String ftpServer,String username,String password) throws Exception
	{
		Session session = null;
		Channel channel = null;
		ChannelSftp channelSftp = null;
		File newFile = null;
		String remoteFileName = null;

		try {
			JSch jsch = new JSch();
			session = jsch.getSession(username, ftpServer);
			session.setPassword(password);
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.connect();
			channel = session.openChannel("sftp");
			channel.connect();
			channelSftp = (ChannelSftp) channel;
			channelSftp.cd(remoteFilePath);

			Vector<LsEntry> v = channelSftp.ls(remoteFilePath);
		
			if (v != null && v.size() > 0) {
				for (int i = 0; i < v.size(); i++) {
					remoteFileName = v.get(i).getFilename();
					logger.debug("File Name : " +remoteFileName);
					 
					if(remoteFileName != null && (remoteFileName.endsWith(filePattern) || remoteFileName.endsWith(filePattern.toLowerCase()))){
						BufferedInputStream bis = new BufferedInputStream(channelSftp.get(remoteFileName));
						newFile = new File(localFilePath, remoteFileName);
						OutputStream os = new FileOutputStream(newFile);
						BufferedOutputStream bos = new BufferedOutputStream(os);
						byte[] buffer = new byte[1024];
						int readCount;
						while ((readCount = bis.read(buffer)) > 0) {
							bos.write(buffer, 0, readCount);
						}
						bis.close();
						bos.close();
					}
					
				}
				
			}
			else
			{	
				logger.info("No response files found on server");
			}
			channelSftp.disconnect();
			channel.disconnect();
			session.disconnect();
		} 
		catch (Exception ex) {
		
			throw new Exception("Download of file failed", ex);
		}
	}
	
	

	/**
	 * This method is used when downloading the File from BAMS.
	 */
	public void downloadFilestoLocalviaSSH(String filePattern, String remoteFilePath,
			String localFilePath, String ftpServer, String username,
			String privatekey, String passphrase, String sftpPassword) throws Exception {
		Session session = null;
		Channel channel = null;
		ChannelSftp channelSftp = null;
		File newFile = null;
		String remoteFileName = null;

		try {
			JSch jsch = new JSch();
			jsch.addIdentity(privatekey, passphrase);
			session = jsch.getSession(username, ftpServer, 6522);
			session.setPassword(sftpPassword);
	        java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.connect();
			channel = session.openChannel("sftp");
			channel.connect();
			channelSftp = (ChannelSftp) channel;
			channelSftp.cd(remoteFilePath);

			Vector<LsEntry> v = channelSftp.ls(remoteFilePath);

			if (v != null && v.size() > 0) {
				for (int i = 0; i < v.size(); i++) {
					remoteFileName = v.get(i).getFilename();
					logger.debug("File Name : " + remoteFileName);

					if (remoteFileName != null
							&& (remoteFileName.endsWith(filePattern) || remoteFileName
									.endsWith(filePattern.toLowerCase()))) {
						BufferedInputStream bis = new BufferedInputStream(
								channelSftp.get(remoteFileName));
						newFile = new File(localFilePath, remoteFileName);
						OutputStream os = new FileOutputStream(newFile);
						BufferedOutputStream bos = new BufferedOutputStream(os);
						byte[] buffer = new byte[1024];
						int readCount;
						while ((readCount = bis.read(buffer)) > 0) {
							bos.write(buffer, 0, readCount);
						}
						bis.close();
						bos.close();
					}

				}

			} else {
				logger.info("No response files found on server");
			}
			channelSftp.disconnect();
			channel.disconnect();
			session.disconnect();
		} catch (Exception ex) {

			throw new Exception("Download of file failed", ex);
		}
	}

	
	public void downloadFileToLocal(String remoteFileNamePattern, String remoteFilePath, String localFilePath, String ftpServer,String username,String password) throws Exception
    {

		Session session = null;
		Channel channel = null;
		ChannelSftp channelSftp = null;
		File newFile = null;
		String remoteFileName = null;

		try {
			JSch jsch = new JSch();
			session = jsch.getSession(username, ftpServer);
			session.setPassword(password);
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.connect();
			channel = session.openChannel("sftp");
			channel.connect();
			channelSftp = (ChannelSftp) channel;
			channelSftp.cd(remoteFilePath);

			Vector<LsEntry> v = channelSftp.ls(remoteFilePath);
		
			boolean remoteFileExist = false;
			for (int i = 0; i < v.size(); i++)
			{
				remoteFileName = v.get(i).getFilename();
				if (remoteFileName.startsWith(remoteFileNamePattern)) {
					remoteFileExist = true;
					break;
				}
			}

			if (remoteFileExist) {
				BufferedInputStream bis = new BufferedInputStream(channelSftp.get(remoteFileName));
				newFile = new File(localFilePath,remoteFileName);
				OutputStream os = new FileOutputStream(newFile);
				BufferedOutputStream bos = new BufferedOutputStream(os);
				byte[] buffer = new byte[1024];
				int readCount;
				while ((readCount = bis.read(buffer)) > 0) {
					bos.write(buffer, 0, readCount);
				}
				bis.close();
				bos.close();
			}
			else
			{	
				logger.info("No response files found on server");
			}
			channelSftp.disconnect();
			channel.disconnect();
			session.disconnect();
		} 
		catch (Exception ex) {
		
			throw new Exception("Download of file failed", ex);
		}
	}
	
	public void downloadResponseFilesToLocal(String remoteFilePath,
			String localFilePath, String ftpServer, String username,
			String password) throws Exception {

		Session session = null;
		Channel channel = null;
		ChannelSftp channelSftp = null;
		File newFile = null;
		String remoteFileName = null;

		try {
			JSch jsch = new JSch();
			session = jsch.getSession(username, ftpServer);
			session.setPassword(password);
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.connect();
			channel = session.openChannel("sftp");
			channel.connect();
			channelSftp = (ChannelSftp) channel;
			channelSftp.cd(remoteFilePath);
			
			Vector<LsEntry> v = channelSftp.ls(remoteFilePath);
			if (v != null && v.size() > 0) {
				for (int i = 0; i < v.size(); i++) {
					remoteFileName = v.get(i).getFilename();
					if (remoteFileName.matches("(?i).*_resp.*")) {
						BufferedInputStream bis = new BufferedInputStream(channelSftp.get(remoteFileName));
						newFile = new File(localFilePath, remoteFileName);
						OutputStream os = new FileOutputStream(newFile);
						BufferedOutputStream bos = new BufferedOutputStream(os);
						byte[] buffer = new byte[1024];
						int readCount;
						while ((readCount = bis.read(buffer)) > 0) {
							bos.write(buffer, 0, readCount);
						}
						bis.close();
						bos.close();
						logger.info("Removed the file from server "	+ remoteFileName);
						channelSftp.rm(remoteFileName);
					} else
						logger.info("No response files found on server");
				}
			} else {
				logger.info("No response files found on server");
			}
			channelSftp.disconnect();
			channel.disconnect();
			session.disconnect();
		} catch (Exception ex) 
		{
			throw new Exception("Download of file failed", ex);
		}
	}
	
	public File downloadFileByName(String fileName, String remoteFilePath, String localFilePath, String ftpServer,String username,String password) throws Exception
	{
		Session session = null;
		Channel channel = null;
		ChannelSftp channelSftp = null;
		File newFile = null;
		//String remoteFileName = null;

		try {
			JSch jsch = new JSch();
			session = jsch.getSession(username, ftpServer);
			session.setPassword(password);
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.connect();
			channel = session.openChannel("sftp");
			channel.connect();
			channelSftp = (ChannelSftp) channel;
			channelSftp.cd(remoteFilePath);
			
			BufferedInputStream bis = new BufferedInputStream(channelSftp.get(fileName));
			newFile = new File(localFilePath, fileName);
			OutputStream os = new FileOutputStream(newFile);
			BufferedOutputStream bos = new BufferedOutputStream(os);
			byte[] buffer = new byte[1024];
			int readCount;
			while ((readCount = bis.read(buffer)) > 0) {
				bos.write(buffer, 0, readCount);
			}
			bis.close();
			bos.close();			
		
			if(newFile == null)
			{	
				logger.info("Settlement File not found on server");
			}
			bis.close();
			channelSftp.disconnect();
			channel.disconnect();
			session.disconnect();
		} 
		catch (Exception ex) {
		
			throw new Exception("Download of file failed", ex);
		}
		
		return newFile;
	}
	
	
	//Use JSCH to archive the file
	public void archiveFile(String remoteFilePath, String localFilePath, String ftpServer,String username,String password, File bamsSettlementFileName) throws Exception
	{
		Session 	session 	= null;
		Channel 	channel 	= null;
		ChannelSftp channelSftp = null;
	

		try{
			JSch jsch = new JSch();
			session = jsch.getSession(username,ftpServer);
			session.setPassword(password);
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.connect();
			channel = session.openChannel("sftp");
			channel.connect();
			channelSftp = (ChannelSftp)channel;
			channelSftp.cd(localFilePath);
			channelSftp.put(new FileInputStream(bamsSettlementFileName), bamsSettlementFileName.getName());
			channelSftp.cd(remoteFilePath);
			channelSftp.rm(bamsSettlementFileName.getName());
			channelSftp.disconnect();
			channel.disconnect();
			session.disconnect();
		}
		catch(Exception ex){			
			throw new Exception("Archiving file failed");
		}
	}

}


