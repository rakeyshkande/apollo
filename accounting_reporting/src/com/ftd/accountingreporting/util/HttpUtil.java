package com.ftd.accountingreporting.util;

import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.util.Iterator;
import java.util.Map;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.util.HttpURLConnection;


/**
 * This is the utility class for http transmission. 
 * @author Charles Fox
 */
public class HttpUtil 
{
    private Logger logger = 
        new Logger("com.ftd.accountingreporting.util.HttpUtil");
        
    public HttpUtil()
    {
    
    }

    /**
     *  This method is responsible for sending an HTTP request to the 
     *  URL specified in the urlString and return the response packet string.
     *  It converts the parameters in the HashMap to query string and connect 
     *  to the url to get the response packet.
     * @param urlString - String
     * @param params - HashMap
     * @return String
     * @throws Exception
     * @todo - code and determine exceptions
     */
    public String send (String httpLocation, Map parameters)
        throws IOException
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering send");
        }
        String response = "";
    
        try{
            int result;
            PostMethod post = new PostMethod(httpLocation);
            
            NameValuePair nvPair = null;
            NameValuePair[] nvPairArray = new NameValuePair[parameters.size()];
            
            int i = 0;
            String name, value;
            
            /* create request */
            for(Iterator iter = parameters.keySet().iterator(); iter.hasNext();i++)
            {
              name = (String) iter.next();
              value = (String) parameters.get(name);
            
              nvPair = new NameValuePair(name, value);
              nvPairArray[i] = nvPair;
            }
            post.setRequestBody(nvPairArray);

            HttpClient httpclient = new HttpClient();
            /* Execute request */
            try {
                result = httpclient.executeMethod(post);
                response = post.getResponseBodyAsString();
            } finally {
                // Release current connection to the connection pool once you are done
                post.releaseConnection();
            }
            
            String message = "Http Response Code is " + result;
            if(logger.isDebugEnabled()) {
                logger.debug(message);
            }
            
            /* check if HTTP request was successful */
            if (result != HttpURLConnection.HTTP_OK) 
            {
                throw new IOException(message);
            }    
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting send");
            } 
        }    
        return response;
    }

}