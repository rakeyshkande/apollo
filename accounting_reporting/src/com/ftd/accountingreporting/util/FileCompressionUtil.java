package com.ftd.accountingreporting.util;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * This class is an utility class for handling file compression operations.
 *
 * @author Charles Fox
 */
public class FileCompressionUtil 
{
    private static Logger logger  = 
        new Logger("com.ftd.accountingreporting.util.FileCompressionUtil");

    public FileCompressionUtil()
    {
        super();
    }

    /**
    * this method will create a gzip file based on the input file passed in
    * @param inputFile String
    * @param outFilename String
    * @returns n/a
    * @throws IOException
    * @throws Exception
    */
    public void createGZipFile(String inputFile, String outFilename) 
        throws IOException, Exception
    {
          /* These are the files to include in the ZIP file */
        String[] filenames = new String[]{inputFile};
    
        /* Create a buffer for reading the files */
        byte[] buf = new byte[1024];
        
        try {
            /* Create the ZIP file */
            ZipOutputStream out = new ZipOutputStream(new FileOutputStream(outFilename));
        
            /* Compress the files */
            for (int i=0; i<filenames.length; i++) {
                FileInputStream in = new FileInputStream(filenames[i]);
        
                /* Add ZIP entry to output stream. */
                out.putNextEntry(new ZipEntry(filenames[i]));
        
                /* Transfer bytes from the file to the ZIP file */
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
        
                /* Complete the entry */
                out.closeEntry();
                in.close();
            }
        
            // Complete the ZIP file */
            out.close();
        } finally {
        }

    }
}