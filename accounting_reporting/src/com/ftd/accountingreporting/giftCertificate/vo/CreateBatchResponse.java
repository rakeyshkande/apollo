package com.ftd.accountingreporting.giftCertificate.vo;

import java.io.Serializable;

public class CreateBatchResponse implements Serializable{
	 /**
	 * 
	 */
	private static final long serialVersionUID = -806777357071364666L;
	private String response;
	public CreateBatchResponse(String response) {
		super();
		this.response = response;
	}
	
	public CreateBatchResponse(){
		
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}
}
