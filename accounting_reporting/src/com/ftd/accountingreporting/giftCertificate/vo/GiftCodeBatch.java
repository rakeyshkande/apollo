/**
 * 
 */
package com.ftd.accountingreporting.giftCertificate.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @author sgupta
 *
 */
public class GiftCodeBatch implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2580462724182196369L;
	
	private String batchNumber;
	private String batchName;
	private String batchDescription;
	private String batchComment;
	private String expirationDt;
	private String format;
	private String fileName;
	private BigDecimal issueAmt;
	private String issueDate;
	private String orderSource;
	private BigDecimal paidAmt;
	private String prefix;
	private String promotionId;
	private Integer quantity;
	private String requestedBy;
	private String siteId;
	private List<String> refCode;
	private String type;
	private Map<String,Integer> statusCount;

	//If batch response is for Individual Gift Code:
	private String redemptionDate;
	private String redeemedReferenceNumber;
	private String reinstateDate;
	
	public GiftCodeBatch(){
		super();
	}
	
	public GiftCodeBatch(String batchNumber, String batchName,
			String batchDescription, String batchComment, String expirationDt,
			String format, String fileName, BigDecimal issueAmt,
			String issueDate, String orderSource, BigDecimal paidAmt,
			String prefix, String promotionId, Integer quantity,
			String requestedBy, String siteId, List<String> refCode,
			String type, Map<String, Integer> statusCount,
			String redemptionDate, String redeemedReferenceNumber,
			String reinstateDate) {
		super();
		this.batchNumber = batchNumber;
		this.batchName = batchName;
		this.batchDescription = batchDescription;
		this.batchComment = batchComment;
		this.expirationDt = expirationDt;
		this.format = format;
		this.fileName = fileName;
		this.issueAmt = issueAmt;
		this.issueDate = issueDate;
		this.orderSource = orderSource;
		this.paidAmt = paidAmt;
		this.prefix = prefix;
		this.promotionId = promotionId;
		this.quantity = quantity;
		this.requestedBy = requestedBy;
		this.siteId = siteId;
		this.refCode = refCode;
		this.type = type;
		this.statusCount = statusCount;
		this.redemptionDate = redemptionDate;
		this.redeemedReferenceNumber = redeemedReferenceNumber;
		this.reinstateDate = reinstateDate;
	}
	public String getBatchNumber() {
		return batchNumber;
	}
	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}
	public String getBatchName() {
		return batchName;
	}
	public void setBatchName(String batchName) {
		this.batchName = batchName;
	}
	public String getBatchDescription() {
		return batchDescription;
	}
	public void setBatchDescription(String batchDescription) {
		this.batchDescription = batchDescription;
	}
	public String getBatchComment() {
		return batchComment;
	}
	public void setBatchComment(String batchComment) {
		this.batchComment = batchComment;
	}
	public String getExpirationDt() {
		return expirationDt;
	}
	public void setExpirationDt(String expirationDt) {
		this.expirationDt = expirationDt;
	}
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public BigDecimal getIssueAmt() {
		return issueAmt;
	}
	public void setIssueAmt(BigDecimal issueAmt) {
		this.issueAmt = issueAmt;
	}
	public String getIssueDate() {
		return issueDate;
	}
	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}
	public String getOrderSource() {
		return orderSource;
	}
	public void setOrderSource(String orderSource) {
		this.orderSource = orderSource;
	}
	public BigDecimal getPaidAmt() {
		return paidAmt;
	}
	public void setPaidAmt(BigDecimal paidAmt) {
		this.paidAmt = paidAmt;
	}
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	public String getPromotionId() {
		return promotionId;
	}
	public void setPromotionId(String promotionId) {
		this.promotionId = promotionId;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public String getRequestedBy() {
		return requestedBy;
	}
	public void setRequestedBy(String requestedBy) {
		this.requestedBy = requestedBy;
	}
	public String getSiteId() {
		return siteId;
	}
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}
	public List<String> getRefCode() {
		return refCode;
	}
	public void setRefCode(List<String> refCode) {
		this.refCode = refCode;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Map<String, Integer> getStatusCount() {
		return statusCount;
	}
	public void setStatusCount(Map<String, Integer> statusCount) {
		this.statusCount = statusCount;
	}
	public String getRedemptionDate() {
		return redemptionDate;
	}
	public void setRedemptionDate(String redemptionDate) {
		this.redemptionDate = redemptionDate;
	}
	public String getRedeemedReferenceNumber() {
		return redeemedReferenceNumber;
	}
	public void setRedeemedReferenceNumber(String redeemedReferenceNumber) {
		this.redeemedReferenceNumber = redeemedReferenceNumber;
	}
	public String getReinstateDate() {
		return reinstateDate;
	}
	public void setReinstateDate(String reinstateDate) {
		this.reinstateDate = reinstateDate;
	}
	
}
