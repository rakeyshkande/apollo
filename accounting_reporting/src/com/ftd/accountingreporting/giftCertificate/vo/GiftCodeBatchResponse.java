package com.ftd.accountingreporting.giftCertificate.vo;

import java.io.Serializable;

public class GiftCodeBatchResponse implements Serializable{
	 /**
	 * 
	 */
	private static final long serialVersionUID = -5924444899165887846L;
	private GiftCodeBatch giftCodeBatchDetails = new GiftCodeBatch();
	public GiftCodeBatchResponse(){
		super();
	}
	public GiftCodeBatchResponse(GiftCodeBatch giftCodeBatchDetails) {
		super();
		this.giftCodeBatchDetails = giftCodeBatchDetails;
	}
	public GiftCodeBatch getGiftCodeBatchDetails() {
		return giftCodeBatchDetails;
	}
	public void setGiftCodeBatchDetails(GiftCodeBatch giftCodeBatchDetails) {
		this.giftCodeBatchDetails = giftCodeBatchDetails;
	}

}
