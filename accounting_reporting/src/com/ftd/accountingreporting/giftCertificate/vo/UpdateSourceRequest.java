package com.ftd.accountingreporting.giftCertificate.vo;

import java.io.Serializable;
import java.util.List;

public class UpdateSourceRequest implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8264793142222088425L;
	private String batchNumber;
	private List<String> source;
	private String updatedBy;

	public UpdateSourceRequest() {
		super();
	}

	public UpdateSourceRequest(String batchNumber, List<String> source,
			String updatedBy) {
		super();
		this.batchNumber = batchNumber;
		this.source = source;
		this.updatedBy = updatedBy;
	}

	public String getBatchNumber() {
		return batchNumber;
	}

	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}

	public List<String> getSource() {
		return source;
	}

	public void setSource(List<String> source) {
		this.source = source;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

}
