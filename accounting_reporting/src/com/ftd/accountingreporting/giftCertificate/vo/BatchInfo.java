package com.ftd.accountingreporting.giftCertificate.vo;

import java.io.Serializable;

public class BatchInfo implements Serializable {
	private static final long serialVersionUID = -8857339974427731297L;
	private String batchDescription;

	private String batchName;

	private String batchComment;

	private String orderSource;
	
	public BatchInfo(){
		
	}

	public BatchInfo(String batchDescription, String batchName,
			String batchComment, String orderSource, String requestedBy) {
		super();
		this.batchDescription = batchDescription;
		this.batchName = batchName;
		this.batchComment = batchComment;
		this.orderSource = orderSource;
	}

	public String getBatchDescription() {
		return batchDescription;
	}

	public void setBatchDescription(String batchDescription) {
		this.batchDescription = batchDescription;
	}

	public String getBatchName() {
		return batchName;
	}

	public void setBatchName(String batchName) {
		this.batchName = batchName;
	}

	public String getBatchComment() {
		return batchComment;
	}

	public void setBatchComment(String batchComment) {
		this.batchComment = batchComment;
	}

	public String getOrderSource() {
		return orderSource;
	}

	public void setOrderSource(String orderSource) {
		this.orderSource = orderSource;
	}


	@Override
	public String toString() {
		return "BatchInfo [batchDescription=" + batchDescription
				+ ", batchName=" + batchName + ", batchComment=" + batchComment
				+ ", orderSource=" + orderSource + ", requestedBy="
				+ "]";
	}

}
