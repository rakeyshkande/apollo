package com.ftd.accountingreporting.giftCertificate.vo;

import java.io.Serializable;

public class UpdateGiftCertResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8035484885630733461L;
	private String siteId;
	private String giftCodeId;
	private String status;
	public UpdateGiftCertResponse(String siteId, String giftCodeId,
			String status) {
		super();
		this.siteId = siteId;
		this.giftCodeId = giftCodeId;
		this.status = status;
	}
	
	public UpdateGiftCertResponse(){
		
	}

	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public String getGiftCodeId() {
		return giftCodeId;
	}

	public void setGiftCodeId(String giftCodeId) {
		this.giftCodeId = giftCodeId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "UpdateGiftCertResponse [siteId=" + siteId + ", giftCodeId="
				+ giftCodeId + ", status=" + status + "]";
	}
	
}
