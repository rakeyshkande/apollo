package com.ftd.accountingreporting.giftCertificate.vo;

public class UpdateBatchResponse {
	private String batchNumber;
    private String siteId;
	public String getBatchNumber() {
		return batchNumber;
	}
	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}
	public String getSiteId() {
		return siteId;
	}
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}
    
    
}
