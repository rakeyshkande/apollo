package com.ftd.accountingreporting.giftCertificate.vo;

import java.io.Serializable;

public class Meta implements Serializable {
	private static final long serialVersionUID = -766706423705186928L;

	private String createdBy;
	private String updatedBy;
	
	public Meta(){
		
	}

	public Meta(String createdBy, String updatedBy) {
		super();
		this.createdBy = createdBy;
		this.updatedBy = updatedBy;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Override
	public String toString() {
		return "Meta [createdBy=" + createdBy + ", updatedBy=" + updatedBy
				+ "]";
	}
}
