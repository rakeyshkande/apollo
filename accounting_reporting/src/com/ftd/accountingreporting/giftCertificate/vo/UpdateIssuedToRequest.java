package com.ftd.accountingreporting.giftCertificate.vo;

import java.io.Serializable;

public class UpdateIssuedToRequest implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5968927498008237561L;
	private String giftCodeId;
    private String address1;
    private String address2;
    private String city;
    private String country;
    private String emailAddress;
    private String issuedReferenceNumber;
    private String issuedToFirstName;
    private String issuedToLastName;
    private String state;
    private String zipcode;
    private String phone;
	public UpdateIssuedToRequest(String giftCodeId, String address1,
			String address2, String city, String country, String emailAddress,
			String issuedReferenceNumber, String issuedToFirstName,
			String issuedToLastName, String state, String zipcode, String phone) {
		super();
		this.giftCodeId = giftCodeId;
		this.address1 = address1;
		this.address2 = address2;
		this.city = city;
		this.country = country;
		this.emailAddress = emailAddress;
		this.issuedReferenceNumber = issuedReferenceNumber;
		this.issuedToFirstName = issuedToFirstName;
		this.issuedToLastName = issuedToLastName;
		this.state = state;
		this.zipcode = zipcode;
		this.phone = phone;
	}
	public UpdateIssuedToRequest() {
		super();
	}
	public String getGiftCodeId() {
		return giftCodeId;
	}
	public void setGiftCodeId(String giftCodeId) {
		this.giftCodeId = giftCodeId;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getIssuedReferenceNumber() {
		return issuedReferenceNumber;
	}
	public void setIssuedReferenceNumber(String issuedReferenceNumber) {
		this.issuedReferenceNumber = issuedReferenceNumber;
	}
	public String getIssuedToFirstName() {
		return issuedToFirstName;
	}
	public void setIssuedToFirstName(String issuedToFirstName) {
		this.issuedToFirstName = issuedToFirstName;
	}
	public String getIssuedToLastName() {
		return issuedToLastName;
	}
	public void setIssuedToLastName(String issuedToLastName) {
		this.issuedToLastName = issuedToLastName;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	@Override
	public String toString() {
		return "UpdateIssuedToRequest [giftCodeId=" + giftCodeId
				+ ", address1=" + address1 + ", address2=" + address2
				+ ", city=" + city + ", country=" + country + ", emailAddress="
				+ emailAddress + ", issuedReferenceNumber="
				+ issuedReferenceNumber + ", issuedToFirstName="
				+ issuedToFirstName + ", issuedToLastName=" + issuedToLastName
				+ ", state=" + state + ", zipcode=" + zipcode + ", phone="
				+ phone + "]";
	}
}
