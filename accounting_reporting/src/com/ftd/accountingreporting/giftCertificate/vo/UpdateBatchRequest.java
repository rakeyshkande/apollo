package com.ftd.accountingreporting.giftCertificate.vo;

import java.io.Serializable;

public class UpdateBatchRequest implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4834916525430777336L;
	private String batchNumber;
    private String batchName;
    private String batchDescription;
    private String batchComment;
    private String promotionId;
    private String requestedBy;
    private String status;
    private String updatedBy;
    
    
	public UpdateBatchRequest() {
		super();
	}
	public UpdateBatchRequest(String batchNumber, String batchName,
			String batchDescription, String batchComment, String promotionId,
			String requestedBy, String status, String updatedBy) {
		super();
		this.batchNumber = batchNumber;
		this.batchName = batchName;
		this.batchDescription = batchDescription;
		this.batchComment = batchComment;
		this.promotionId = promotionId;
		this.requestedBy = requestedBy;
		this.status = status;
		this.updatedBy = updatedBy;
	}
	public String getBatchNumber() {
		return batchNumber;
	}
	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}
	public String getBatchName() {
		return batchName;
	}
	public void setBatchName(String batchName) {
		this.batchName = batchName;
	}
	public String getBatchDescription() {
		return batchDescription;
	}
	public void setBatchDescription(String batchDescription) {
		this.batchDescription = batchDescription;
	}
	public String getBatchComment() {
		return batchComment;
	}
	public void setBatchComment(String batchComment) {
		this.batchComment = batchComment;
	}
	public String getPromotionId() {
		return promotionId;
	}
	public void setPromotionId(String promotionId) {
		this.promotionId = promotionId;
	}
	public String getRequestedBy() {
		return requestedBy;
	}
	public void setRequestedBy(String requestedBy) {
		this.requestedBy = requestedBy;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Override
	public String toString() {
		return "UpdateBatchRequest [batchNumber=" + batchNumber
				+ ", batchName=" + batchName + ", batchDescription="
				+ batchDescription + ", batchComment=" + batchComment
				+ ", promotionId=" + promotionId + ", requestedBy="
				+ requestedBy + ", status=" + status + ", updatedBy="
				+ updatedBy + "]";
	}
    
}
