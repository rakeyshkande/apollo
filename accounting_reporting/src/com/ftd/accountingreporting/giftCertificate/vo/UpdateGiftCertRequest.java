package com.ftd.accountingreporting.giftCertificate.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class UpdateGiftCertRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4787252691023199487L;
	private String giftCodeId;
	private String status;
	private BigDecimal redemptionAmount;
	private String redeemedReferenceNumber;
	private String orderNo;

	public UpdateGiftCertRequest() {

	}

	public UpdateGiftCertRequest(String giftCodeId, String status,
			BigDecimal redemptionAmount, String redeemedReferenceNumber, String orderNo) {
		super();
		this.giftCodeId = giftCodeId;
		this.status = status;
		this.redemptionAmount = redemptionAmount;
		this.redeemedReferenceNumber = redeemedReferenceNumber;
		this.orderNo = orderNo;
	}

	public String getGiftCodeId() {
		return giftCodeId;
	}

	public void setGiftCodeId(String giftCodeId) {
		this.giftCodeId = giftCodeId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public BigDecimal getRedemptionAmount() {
		return redemptionAmount;
	}

	public void setRedemptionAmount(BigDecimal redemptionAmount) {
		this.redemptionAmount = redemptionAmount;
	}

	public String getRedeemedReferenceNumber() {
		return redeemedReferenceNumber;
	}

	public void setRedeemedReferenceNumber(String redeemedReferenceNumber) {
		this.redeemedReferenceNumber = redeemedReferenceNumber;
	}
	
	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

}
