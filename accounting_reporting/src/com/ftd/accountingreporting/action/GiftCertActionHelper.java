package com.ftd.accountingreporting.action;
import com.ftd.accountingreporting.bo.GiftCertBO;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;
import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class GiftCertActionHelper
{

  private static Logger logger  = new Logger("com.ftd.accountingreporting.action.GiftCertActionHelper");
   
   public Document createFieldDocWithRoot(HttpServletRequest request) throws Exception
   {
          Document fieldDoc = DOMUtil.getDefaultDocument();
          Element root = (Element) fieldDoc.createElement(ARConstants.XML_ROOT);
          
          Document doc = createFieldDoc(request);
          root.appendChild(fieldDoc.importNode(doc.getDocumentElement(), true));
          return fieldDoc;
   }
   
   /**
   * This method creates field doc for the error related pages
   * @param request
   * @return Document
   * @throws javax.servlet.ServletException
   */
   public Document createFieldDoc(HttpServletRequest request) throws Exception
   {
           Document fieldDoc = DOMUtil.getDefaultDocument();
           Element fields = (Element) fieldDoc.createElement(ARConstants.XML_FIELD_TOP);
           fieldDoc.appendChild(fields);
           
           String requestNumber = request.getParameter("gcc_request_number");
           if (requestNumber != null) {
              requestNumber = requestNumber.trim();
              if(!"".equals(requestNumber)) {
                  requestNumber = requestNumber.toUpperCase();
              }
           }
   
           fields.appendChild(getNameElement(fieldDoc,"gcc_request_number",requestNumber));
           fields.appendChild(getNameElement(fieldDoc,"gcc_type",request.getParameter("gcc_type")));
           fields.appendChild(getNameElement(fieldDoc,"gcc_issue_amount",request.getParameter("gcc_issue_amount")));
           fields.appendChild(getNameElement(fieldDoc,"gcc_issue_date",request.getParameter("gcc_issue_date")));
           fields.appendChild(getNameElement(fieldDoc,"gcc_paid_amount",request.getParameter("gcc_paid_amount")));
           fields.appendChild(getNameElement(fieldDoc,"gcc_expiration_date",request.getParameter("gcc_expiration_date")));
           fields.appendChild(getNameElement(fieldDoc,"gcc_quantity",request.getParameter("gcc_quantity")));
           fields.appendChild(getNameElement(fieldDoc,"gcc_format",request.getParameter("gcc_format")));
           fields.appendChild(getNameElement(fieldDoc,"gcc_filename",request.getParameter("gcc_filename")));
           fields.appendChild(getNameElement(fieldDoc,"gcc_requestor_name",request.getParameter("gcc_requestor_name")));
           fields.appendChild(getNameElement(fieldDoc,"gcc_redemption_type",request.getParameter("gcc_redemption_type")));
           fields.appendChild(getNameElement(fieldDoc,"gcc_program_name",request.getParameter("gcc_program_name")));
           fields.appendChild(getNameElement(fieldDoc,"gcc_program_description",request.getParameter("gcc_program_description")));
           fields.appendChild(getNameElement(fieldDoc,"gcc_comments",request.getParameter("gcc_comments")));
           fields.appendChild(getNameElement(fieldDoc,"gcc_company",request.getParameter("gcc_company")));
           fields.appendChild(getNameElement(fieldDoc,"gcc_source_code",request.getParameter("gcc_source_code")));
           fields.appendChild(getNameElement(fieldDoc,"gcc_prefix",request.getParameter("gcc_prefix")));
           fields.appendChild(getNameElement(fieldDoc,"gcc_other",request.getParameter("gcc_other")));
           fields.appendChild(getNameElement(fieldDoc,"gcc_redemption_date",request.getParameter("gcc_redemption_date")));
           fields.appendChild(getNameElement(fieldDoc,"gcc_order_number",request.getParameter("gcc_order_number")));
           fields.appendChild(getNameElement(fieldDoc,"gcc_status_multiple",request.getParameter("gcc_status_multiple")));
           fields.appendChild(getNameElement(fieldDoc,"gcc_status_single",request.getParameter("gcc_status_single")));
           fields.appendChild(getNameElement(fieldDoc,"gcc_coupon_number",request.getParameter("gcc_coupon_number")));
           fields.appendChild(getNameElement(fieldDoc,"gcc_creation_date",request.getParameter("gcc_creation_date")));
           fields.appendChild(getNameElement(fieldDoc,"gcc_promotion_id",request.getParameter("gcc_promotion_id")));
           fields.appendChild(getNameElement(fieldDoc,"gcc_partner_program_name",request.getParameter("gcc_partner_program_name")));
           
           return fieldDoc;
   }
   
   public Document createRecipFieldDoc(HttpServletRequest request) throws ServletException
   {
      Document fieldDoc = null;
      try {
           fieldDoc = DOMUtil.getDefaultDocument();
           Element root = (Element) fieldDoc.createElement(ARConstants.XML_ROOT);
           fieldDoc.appendChild(root);
           
           Element fields = (Element) fieldDoc.createElement(ARConstants.XML_FIELD_TOP);
           root.appendChild(fields);
           for (int i = 1; i <= new Integer(request.getParameter("coupon_quantity")).intValue(); i++)
           {
               fields.appendChild(getNameElement(fieldDoc,"first_name_"+i,request.getParameter("first_name_"+i)));
               fields.appendChild(getNameElement(fieldDoc,"last_name_"+i,request.getParameter("last_name_"+i)));
               fields.appendChild(getNameElement(fieldDoc,"address_1_"+i,request.getParameter("address_1_"+i)));
               fields.appendChild(getNameElement(fieldDoc,"address_2_"+i,request.getParameter("address_2_"+i)));
               fields.appendChild(getNameElement(fieldDoc,"city_"+i,request.getParameter("city_"+i)));
               fields.appendChild(getNameElement(fieldDoc,"state_"+i,request.getParameter("state_"+i)));
               fields.appendChild(getNameElement(fieldDoc,"country_"+i,request.getParameter("country_"+i)));
               fields.appendChild(getNameElement(fieldDoc,"phone_"+i,request.getParameter("phone_"+i)));
               fields.appendChild(getNameElement(fieldDoc,"email_"+i,request.getParameter("email_"+i)));
               fields.appendChild(getNameElement(fieldDoc,"gc_coupon_recip_id_"+i,request.getParameter("gc_coupon_recip_id_"+i)));
           }
      } catch (Exception e) {
          throw new ServletException(e.getMessage());
      }
           return fieldDoc;
   }   
   
   /**
   * Creates an Element for the document with the given attribute name and value.
   * @param fieldDoc
   * @param attrName
   * @param attrValue
   * @return Element
   */
   public Element getNameElement(Document fieldDoc, String attrName, String attrValue)
   {
       Element field = (Element) fieldDoc.createElement(ARConstants.XML_FIELD_BOTTOM);
       field.setAttribute(ARConstants.XML_ATTR_FIELD,attrName);
       field.setAttribute(ARConstants.XML_ATTR_VALUE,attrValue);
       return field;
   }

/*
   public HashMap setPageParams(HttpServletRequest request, String nextAction)
   {
      HashMap params = new HashMap();
         
      params.put(ARConstants.COMMON_GCC_ACTION, nextAction==null?"":nextAction);
      params.put(ARConstants.ACTION_PARM_IS_MULTIPLE_GCC, request.getParameter(ARConstants.ACTION_PARM_IS_MULTIPLE_GCC)==null?"":request.getParameter(ARConstants.ACTION_PARM_IS_MULTIPLE_GCC));
      params.put(ARConstants.COMMON_GCC_USER, request.getParameter(ARConstants.COMMON_GCC_USER)==null?"":request.getParameter(ARConstants.COMMON_GCC_USER));
      params.put(ARConstants.COMMON_PARM_SEC_TOKEN, request.getParameter(ARConstants.COMMON_PARM_SEC_TOKEN)==null?"":request.getParameter(ARConstants.COMMON_PARM_SEC_TOKEN));
      params.put(ARConstants.COMMON_PARM_CONTEXT, request.getParameter(ARConstants.COMMON_PARM_CONTEXT)==null?"":request.getParameter(ARConstants.COMMON_PARM_CONTEXT));

      return params;
  }   
  */
  
  /**
   * Updates the parameters HashMap attribute set by the DataFilter from the request object
   * with "is_multiple_gcc" key to the given value.
   * @param newValue
   * @return 
   * @throws java.lang.Exception
   */
  public HashMap updateGccAction(HttpServletRequest request, String newValue) throws Exception {
      HashMap parameters = getFilterParameters(request);
      return updateParameter(parameters, ARConstants.COMMON_GCC_ACTION, newValue);
  }
  
  /**
   * Updates the parameters HashMap attribute with "gcc_action" key to the given value.
   * @param newValue
   * @return 
   * @throws java.lang.Exception
   */
  public HashMap updateIsMultipleGcc(HashMap parameters, String newValue) throws Exception {
      return updateParameter(parameters, ARConstants.ACTION_PARM_IS_MULTIPLE_GCC, newValue);
  }
  
  /**
   * Updates / adds the name value pair to the map.
   * @param parameters
   * @param name
   * @param value
   * @return 
   * @throws java.lang.Exception
   */
  public HashMap updateParameter(HashMap parameters, String name, String value) throws Exception {
      if(value == null) {
          value = "";
      }
      if(parameters == null) {
          parameters = new HashMap();
      }
      parameters.put(name, value);
      
      return parameters;
  }
  
  /**
   * Retrieves the HashMap attribute from the request set by the DataFilter.
   * @param request
   * @return 
   * @throws java.lang.Exception
   */
  public HashMap getFilterParameters(HttpServletRequest request) throws Exception{
      HashMap parameters = (HashMap)request.getAttribute(ARConstants.PARAMETERS);
      return parameters;
  }
}