package com.ftd.accountingreporting.action;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * This class is responsible for dispatching request to AddRecipientAction or EditRecipientAction.
 * @author Christy Hu
 */
public final class RecipientAction extends Action 
{

  private static Logger logger  = new Logger("com.ftd.accountingreporting.action.RecipientAction");
  private static final String ADD = "add";
  private static final String EDIT = "edit";
  
  /**
   * This is the main action called from the Struts framework.
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException, Exception
  {
    String isMultipleGcc="";
    String forwardName = "";
    logger.debug("Entering RecipientAction...");
    
    try
    {
        String action = request.getParameter(ARConstants.COMMON_GCC_ACTION);
        isMultipleGcc = request.getParameter(ARConstants.ACTION_PARM_IS_MULTIPLE_GCC);
        
        logger.debug("action is: " + action);
        logger.debug("isMultipleGcc is: " + isMultipleGcc);
           
        //Dispatches to either AddRecipientAction or EditRecipientAction
        if (action.equals(ARConstants.ACTION_PARM_DISPLAY_ADD)||
              (action.equals(ARConstants.ACTION_PARM_ADD))||
              (action.equals(ARConstants.ACTION_PARM_EXIT_ADD))) {
            forwardName = ADD;
        } else if (action.equals(ARConstants.ACTION_PARM_DISPLAY_EDIT)||
              (action.equals(ARConstants.ACTION_PARM_EDIT))||
              (action.equals(ARConstants.ACTION_PARM_EXIT_EDIT))) {
            forwardName = EDIT;
        } else {
            forwardName = ARConstants.ACTION_ERROR;
        } 
    }
    catch (Exception e) {
       logger.error(e);
       // send to error page.
       mapping.findForward(ARConstants.ACTION_ERROR);
    } 
    logger.debug("path is: " + (mapping.findForward(forwardName)).getPath());
    return mapping.findForward(forwardName);
  }
  
}