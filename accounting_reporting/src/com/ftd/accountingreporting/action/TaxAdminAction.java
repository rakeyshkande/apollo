package com.ftd.accountingreporting.action;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.util.ServletHelper;


public class TaxAdminAction
  extends Action
{
  /*******************************************************************************************
 * Class level variables
 *******************************************************************************************/
	private HashMap<Object, Object> pageData = new HashMap<Object, Object>();

	private static final String CONTEXT = "TAX_CONFIG";
	private static final String TAX_ADMIN_URL = "TAX_ADMIN_URL";
  private File errorStylesheet;
  private String errorStylesheetName;
	public static final String FN_ERROR = "error";
	public static final String FN_SUCCESS = "Success";
  private static Logger logger = new Logger(TaxAdminAction.class.getName());

  /*******************************************************************************************
 * Constructor 1
 *******************************************************************************************/
  public TaxAdminAction()
  {
  }


  /**
   * This is the main action called from the Struts framework.
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                               HttpServletResponse response)
  {
		
		String context = (String) request.getParameter("context");
		String securityToken = (String) request.getParameter("securitytoken");
		String siteName = (String) request.getParameter("siteName");
		Document responseDoc = null;
		HashMap<Object, Object> cleanFtdParams = new HashMap<Object, Object>();
    this.errorStylesheetName = getXslPathAndName(FN_ERROR,mapping);
    this.errorStylesheet = getXSL(FN_ERROR,mapping);
		// TraxUtil traxUtil = null;

		try {
			responseDoc = DOMUtil.getDocument();

			cleanFtdParams = cleanHttpParameters(request);
//			logger.debug(cleanFtdParams);

			if (ServletHelper.isValidToken(context, securityToken)) {
				String taxAdminURL = ConfigurationUtil.getInstance().getFrpGlobalParm(
						CONTEXT, TAX_ADMIN_URL);
				if (taxAdminURL != null) {
					taxAdminURL += "?token=" + securityToken + "&frame=1";
					logger.debug(" taxAdminURL " + taxAdminURL);
					this.pageData.put("taxAdminURL", taxAdminURL);
					this.pageData.put("securityToken", securityToken);
					this.pageData.put("context", context);
					this.pageData.put("siteName", siteName);
					DOMUtil.addSection(responseDoc, "pageData", "data", pageData, false);
					cleanFtdParams.put("result", taxAdminURL);
					cleanFtdParams.put("siteName", siteName);
					appendPageDataToXml(responseDoc, cleanFtdParams);
					if (logger.isDebugEnabled()) {
						printXml(responseDoc);
					}
					goToPage(mapping, request, response, responseDoc,
							"taxAdmin");
					// return
					// mapping.findForward(MarketingConstants.XSL_PREMIER_CIRCLE_MAINT);
				}
				else {
					goToPage(mapping, request, response, responseDoc,
						"error");
				}
			} else {
				logger.error("Invalid security token " + securityToken);
				ServletHelper.redirectToLogin(request, response);
			}
		} catch (Exception e) {
			logger.error("Could not obtain "
					+ TAX_ADMIN_URL + " from "
					+ CONTEXT + " due to error: " + e);
		}
		return null;
	}

	private HashMap<Object, Object> cleanHttpParameters(HttpServletRequest request) {
		HashMap<Object, Object> result = new HashMap<Object, Object>();

		for (Enumeration i = request.getParameterNames(); i.hasMoreElements();) {
			String key = (String) i.nextElement();
			String value = request.getParameter(key);

			if ((value != null) && (value.trim().length() > 0)) {
				result.put(key, value.trim());
			}
		}

		return result;
	}

	private void appendPageDataToXml(Document responseDoc,
			HashMap<Object, Object> metadataMap) {
		DOMUtil.addSection(responseDoc, "pageData", "data", metadataMap, false);
	}

	private void goToPage(ActionMapping mapping, HttpServletRequest request,
			HttpServletResponse response, Document responseDoc, String xslFileName)
			throws IOException, ServletException {
		try {
			File xslFile = getXSL(mapping, request, xslFileName);
			TraxUtil.getInstance().transform(
					request,
					response,
					responseDoc,
					xslFile,
					(HashMap) request
							.getAttribute("parameters"));
		} catch (Throwable t) {
			logger.error("goToPage: Error: ", t);
		}
	}

	private File getXSL(ActionMapping mapping, HttpServletRequest request,
			String xslFileLookUpName) {
		// get real file name
		ActionForward forward = mapping.findForward(xslFileLookUpName);

		// get xsl file location
		return new File(request.getSession().getServletContext()
				.getRealPath(forward.getPath()));
	}

	private void printXml(Document responseDoc) throws Exception {
		StringWriter sw = new StringWriter(); // string representation of xml
																					// document
		DOMUtil.print(responseDoc, new PrintWriter(sw));
		logger.debug("printXml: request data DOM = \n" + sw.toString());
	}
	
  /**
  * Retrieve name of Action XSL file
  * @param1 String - the xsl name  
  * @param2 ActionMapping
  * @return File - XSL File name
  * @throws none
  */
  private File getXSL(String xslName, ActionMapping mapping)
  {

    File xslFile = null;
    String xslFilePathAndName = null;

    ActionForward forward = mapping.findForward(xslName);
    xslFilePathAndName = forward.getPath();
    //get real file name
    xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
    return xslFile;
  }   
  
  /**
   * Retrieves XSL path and name
   * @param forwardName
   * @param mapping
   * @return
   */
  private String getXslPathAndName(String forwardName, ActionMapping mapping)
  {
      String xslFilePathAndName = null;

      ActionForward forward = mapping.findForward(forwardName);
      xslFilePathAndName = forward.getPath();
      
      return xslFilePathAndName;
  }   



}
