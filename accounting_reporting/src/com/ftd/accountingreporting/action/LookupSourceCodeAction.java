package com.ftd.accountingreporting.action;

import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.dao.GccUtilDAO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;
import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * This class handles reqests to look up a source code for the given company.
 * @author Madhu Sudanparab
 * @updated 06/27 by chu: added exception handling.
 */
public final class LookupSourceCodeAction  extends Action
{
   private static Logger logger  = new Logger("com.ftd.accountingreporting.action.LookupSourceCodeAction");
  
  /**
   * This is the main action called from the Struts framework.
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
   
   public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
   throws IOException, ServletException
   {
      Connection conn = null;
      HashMap params = null;
      GiftCertActionHelper actionHelper = new GiftCertActionHelper();
      
      try
      {
          logger.debug("Entering LookupSourceCodeAction...");
          String dsname = ConfigurationUtil.getInstance().getProperty(ARConstants.CONFIG_FILE,
                                                    ARConstants.DATASOURCE_NAME);
          conn = DataSourceUtil.getInstance().getConnection(dsname);
          
          Document responseDocument = DOMUtil.getDefaultDocument();
          Element root = (Element) responseDocument.createElement("root");
          responseDocument.appendChild(root);
          String companyId = request.getParameter("company_id");
          String sourceCode = request.getParameter("sourceCodeInput");
          logger.debug("companyId is : " + companyId);
          logger.debug("sourceCode is : " + sourceCode);
          
          Document rd = (new GccUtilDAO(conn)).doGetSourceCodeListByCompanyId(sourceCode, sourceCode, companyId, "Y");
          responseDocument.getDocumentElement().appendChild(responseDocument.importNode(rd.getDocumentElement(), true));
          root.setAttribute("company_id", companyId);
          
          params = actionHelper.getFilterParameters(request);
        
          File styleSheetFile =  new File(this.getServlet().getServletContext().getRealPath(ARConstants.XSL_SOURCE_CODE_LOOK_UP));
          
          TraxUtil.getInstance().transform(request, response, responseDocument, styleSheetFile, params);        
      }
      catch (Throwable ex)
      {
        logger.error(ex);
        return mapping.findForward(ARConstants.ACTION_ERROR);
      }
      finally 
      {
        try
        {
              if(conn != null && !conn.isClosed()) {
                  logger.debug("Closing connection...");
                  conn.close();
              }
        }
        catch (Exception e)
        {
          logger.error(e);
          //forward to error page  
          return mapping.findForward(ARConstants.ACTION_ERROR);
        }
      }
      return null;
    }
}
