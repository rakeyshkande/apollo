package com.ftd.accountingreporting.action;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.dao.CBRDAO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class CBRMenuAction extends Action
{

    private static Logger logger = 
        new Logger("com.ftd.accountingreporting.action.CBRMenuAction");

    /**
     * This method does the following:
     * �	retrieve cbr_action from request object
     *      o	if it is �main�, menuUrl = mapping.findForward(�CS_MENU_URL�).getPath();
     *      o	If it is �cust_hold_info�, menuUrl = mapping.findForward(�CUST_HOLD_URL�).getPath();
     *      o	else then forward to ErrorAction.
     *          return mapping.findForward(ARConstants.ACTION_ERROR);
     * �	retrieve context and securitytoken from filter and add them to the query string.
     * �	Forward action to menuUrl.
     * @param forwardName - String
     * @param mapping - ActionMapping
     * @return File
     * @throws n/a
     */ 
    public ActionForward execute(ActionMapping mapping, ActionForm form, 
        HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException
        {
            if(logger.isDebugEnabled()){
                logger.debug("Entering execute");
            }
            
            ActionForward actionForward = new ActionForward();
            Connection conn = null;
            try
            {
                String action = request.getParameter(ARConstants.COMMON_CBR_ACTION);
                if(logger.isDebugEnabled()){
                    logger.debug("action is " + action);
                }
                
                /* retrieve Menu Url based on the cbr action */
                String menuUrl = "";   
                
                /* retrieve security token and context from the data filter */
                HashMap parameters = (HashMap)request.getAttribute("parameters");

                /* Create Connection object. */
                String dsname = ConfigurationUtil.getInstance().getProperty(ARConstants.CONFIG_FILE,
                                                            ARConstants.DATASOURCE_NAME);
                conn = DataSourceUtil.getInstance().getConnection(dsname);
                CBRDAO cbrDAO = new CBRDAO(conn);
                String orderGUID = (String) parameters.get(ARConstants.ORDER_GUID);
                String customerID = cbrDAO.doGetCustomerID(orderGUID);
                HashMap requestParms    = new HashMap();
                requestParms = getRequestInfo(request);
                ConfigurationUtil cu = ConfigurationUtil.getInstance();
                String baseUrl = cu.getFrpGlobalParm(ARConstants.BASE_CONFIG,ARConstants.BASE_URL);

                // Redirect to COM MainMenuAction.
                if(action.equals(ARConstants.ACTION_CBR_MAIN))
                {
                    
                    //Get info passed in the request object
                    menuUrl = baseUrl + mapping.findForward("CS_MENU_URL").getPath() + "?adminAction=customerService"
                        + "&work_complete=" + (String)requestParms.get("workComplete");
                                //append header info to the main menu url
                    
                } else if(action.equals(ARConstants.ACTION_CUST_HOLD_INFO))
                {
                    menuUrl = mapping.findForward("CUST_HOLD_URL").getPath() + "?from_page=charge_back&action_type=loadcustomer";
                    /* Add customer_id */
                    menuUrl =  baseUrl + menuUrl + "&customer_id=" + customerID;
                }else if(action.equals((ARConstants.ACTION_SHOPPING_CART_INFO)))
                {
                    menuUrl =  baseUrl + mapping.findForward("SHOPPING_CART_INFO_URL").getPath() + "?action=customer_account_search";            
                    /* Add order_guid, order_number, customer_id */
                    menuUrl = menuUrl 
                        + "&order_guid=" + orderGUID
                        + "&order_number=" + (String) parameters.get(ARConstants.MASTER_ORDER_NUMBER)
                        + "&customer_id=" + customerID;
                }else if(action.equals((ARConstants.ACTION_RECIPIENT_INFO)))
                {
                    menuUrl =  baseUrl + mapping.findForward("RECIPIENT_INFO_URL").getPath() + "?action=customer_account_search";            
                    /* Add order_guid, order_number, customer_id */
                    menuUrl = menuUrl 
                        + "&order_guid=" + orderGUID
                        + "&order_number=" + (String)parameters.get(ARConstants.EXTERNAL_ORDER_NUMBER)
                        + "&customer_id=" + customerID;
                }else
                {
                    logger.error(action + " action not valid!");
                    return mapping.findForward(ARConstants.ACTION_ERROR);
                }
                
                String securityToken = (String) parameters.get("securitytoken");
                if(securityToken == null)
                {
                    /* attempt to retrieve from request */
                    securityToken = request.getParameter("securitytoken");
                }

                String context = (String) parameters.get("context");
                if(context == null)
                {
                    /* attempt to retrieve from request */
                    context = request.getParameter("context");
                }
                    
                menuUrl = menuUrl + "&context=" + context + 
                        "&securitytoken=" + securityToken;
                menuUrl += "&call_dnis=";
                    menuUrl += (String)requestParms.get("headerDnisId");
                    menuUrl += "&call_brand_name=";
                    menuUrl += (String)requestParms.get("headerBrandName");
                    menuUrl += "&call_cs_number=";
                    menuUrl += (String)requestParms.get("headerCustomerServiceNumber");
                    menuUrl += "&call_oe_flag=";
                    menuUrl += (String)requestParms.get("headerCustomerOrderIndicator");
                    menuUrl += "&t_call_log_id=";
                    menuUrl += (String)requestParms.get("timerCallLogId");
                    menuUrl += "&t_comment_origin_type=";
                    menuUrl += (String)requestParms.get("timerCommentOriginType");
                    menuUrl += "&t_entity_history_id=";
                    menuUrl += (String)requestParms.get("timerEntityHistoryId");

                logger.debug("Forwarding to : " + menuUrl);
                actionForward= new ActionForward(menuUrl,true);
        }catch (SQLException sqlex)
        {
           logger.error("SQLException Caught!", sqlex);
           return mapping.findForward(ARConstants.ACTION_ERROR);        
        }
        catch (Throwable ex)
        {
           logger.error("Exception Caught!", ex);
           return mapping.findForward(ARConstants.ACTION_ERROR);                
        }finally{
           try {
              if(conn != null && !conn.isClosed()) {
                  logger.debug("Closing connection...");
                  conn.close();
              }
           } catch (Exception e) {
              logger.error("Exception caught when closing connection!", e);
              return mapping.findForward(ARConstants.ACTION_ERROR);
           }
                if(logger.isDebugEnabled()){
                   logger.debug("Exiting execute");
                } 
            }
            return actionForward;
        }
        
  /******************************************************************************
  * getRequestInfo(HttpServletRequest request)
  *******************************************************************************
  * Retrieve the info from the request object, and set class level variables
  * @param  HttpServletRequest
  * @return none 
  * @throws none
  */
  private HashMap getRequestInfo(HttpServletRequest request)
  {
    String  adminAction                   = "customerService";
    String  context                       = "";
    String  headerBrandName               = "";
    String  headerCustomerOrderIndicator  = "";
    String  headerCustomerServiceNumber   = "";
    String  headerDnisId                  = "";
    String  securityToken                 = ""; 
    String  timerCallLogId                = "";
    String  timerCommentOriginType        = "";
    String  timerEntityHistoryId          = "";
    String  workComplete                  = "";
    HashMap requestParms                  = new HashMap();    
  
    /**********************retreive the timer info**********************/
    //retrieve the t_comment_origin_type
    if(request.getParameter(ARConstants.TIMER_COMMENT_ORIGIN_TYPE)!=null)
      timerCommentOriginType = request.getParameter(ARConstants.TIMER_COMMENT_ORIGIN_TYPE);

    //retrieve the t_entity_history_id
    if(request.getParameter(ARConstants.TIMER_ENTITY_HISTORY_ID)!=null)
      timerEntityHistoryId = request.getParameter(ARConstants.TIMER_ENTITY_HISTORY_ID);

    //retrieve the t_call_log_id
    if(request.getParameter(ARConstants.TIMER_CALL_LOG_ID)!=null)
      timerCallLogId = request.getParameter(ARConstants.TIMER_CALL_LOG_ID);


    /**********************retreive the header info**********************/
    //retrieve the brand name
    if(request.getParameter(ARConstants.H_BRAND_NAME)!=null)
      headerBrandName = request.getParameter(ARConstants.H_BRAND_NAME);

    //retrieve the customer order indicator
    if(request.getParameter(ARConstants.H_CUSTOMER_ORDER_INDICATOR)!=null)
      headerCustomerOrderIndicator = request.getParameter(ARConstants.H_CUSTOMER_ORDER_INDICATOR);

    //retrieve the cserv number
    if(request.getParameter(ARConstants.H_CUSTOMER_SERVICE_NUMBER)!=null)
      headerCustomerServiceNumber = request.getParameter(ARConstants.H_CUSTOMER_SERVICE_NUMBER);

    //retrieve the dnis id
    if(request.getParameter(ARConstants.H_DNIS_ID)!=null)
      headerDnisId = request.getParameter(ARConstants.H_DNIS_ID);


    /**********************retreive the security info**********************/
    //retrieve the context
    if(request.getParameter(ARConstants.CONTEXT)!=null)
      context = request.getParameter(ARConstants.CONTEXT);

    //retrieve the security token
    if(request.getParameter(ARConstants.SEC_TOKEN)!=null)
      securityToken = request.getParameter(ARConstants.SEC_TOKEN);

    /**********************retreive miscellaneous info**********************/
    //retrieve the work_complete
    if(request.getParameter(ARConstants.WORK_COMPLETE)!=null)
      workComplete = request.getParameter(ARConstants.WORK_COMPLETE);

    //retrieve the admin actionwork_complete
    if(request.getParameter(ARConstants.ADMIN_ACTION)!=null)
      if(!request.getParameter(ARConstants.ADMIN_ACTION).equalsIgnoreCase(""))
        adminAction = request.getParameter(ARConstants.ADMIN_ACTION);


    requestParms.put("adminAction",         		      adminAction);
    requestParms.put("context",             		      context);
    requestParms.put("headerBrandName",     		      headerBrandName);
    requestParms.put("headerCustomerOrderIndicator",  headerCustomerOrderIndicator);
    requestParms.put("headerCustomerServiceNumber",   headerCustomerServiceNumber);
    requestParms.put("headerDnisId",                  headerDnisId);
    requestParms.put("securityToken", 			          securityToken);
    requestParms.put("timerCallLogId", 			          timerCallLogId);
    requestParms.put("timerCommentOriginType", 		    timerCommentOriginType);
    requestParms.put("timerEntityHistoryId", 		      timerEntityHistoryId);
    requestParms.put("workComplete", 			            workComplete); 
  
  logger.debug("adminAction:"+ adminAction);
    logger.debug("context"+ context);
    logger.debug("headerBrandName"+    		      headerBrandName);
    logger.debug("headerCustomerOrderIndicator "+  headerCustomerOrderIndicator);
    logger.debug("headerCustomerServiceNumber "+   headerCustomerServiceNumber);
    logger.debug("headerDnisId "+                  headerDnisId);
    logger.debug("securityToken "+ 			          securityToken);
    logger.debug("timerCallLogId "+ 			          timerCallLogId);
    logger.debug("timerCommentOriginType "+ 		    timerCommentOriginType);
    logger.debug("timerEntityHistoryId "+ 		      timerEntityHistoryId);
    logger.debug("workComplete "+ 			            workComplete); 
    return requestParms;  
  }
        
}