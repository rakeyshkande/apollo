package com.ftd.accountingreporting.action;
import com.ftd.accountingreporting.bo.GiftCertBO;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;
import java.io.IOException;

import java.sql.Connection;

import java.sql.SQLException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * This class handles requests related to searching a Gift Cert request or coupon: display_search, search.
 * @author Christy Hu
 */
public final class SearchGiftCertAction extends Action 
{

  private static Logger logger  = new Logger("com.ftd.accountingreporting.action.SearchGiftCertAction");
     
  /**
   * This is the main action called from the Struts framework.
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException, Exception
  {
  
    Connection conn = null;
    Document responseDocument = DOMUtil.getDocument();
    HashMap params = null;
    String isMultipleGcc="";
    GiftCertBO gcBO = null;
    String action = null;
    String dsname = null;
    String nextAction = null;
    File styleSheetFile = null;
    GiftCertActionHelper actionHelper = new GiftCertActionHelper();
         
    try
    {
        logger.debug("Entering SearchGiftCertAction...");

        action = request.getParameter(ARConstants.COMMON_GCC_ACTION);
        isMultipleGcc = request.getParameter(ARConstants.ACTION_PARM_IS_MULTIPLE_GCC);
        
        dsname = ConfigurationUtil.getInstance().getProperty(ARConstants.CONFIG_FILE,
                                                    ARConstants.DATASOURCE_NAME);
        conn = DataSourceUtil.getInstance().getConnection(dsname);
        
        gcBO = new GiftCertBO(conn);
        logger.debug("action is: " + action);
        logger.debug("isMultipleGcc is: " + isMultipleGcc);
                
        if (action.equals(ARConstants.ACTION_PARM_SEARCH))
        {
          if (request.getParameter(ARConstants.COMMON_GCC_REQUEST_NUMBER) == null || request.getParameter(ARConstants.COMMON_GCC_REQUEST_NUMBER).equals("")) {
              isMultipleGcc = ARConstants.COMMON_VALUE_NO;
          }
          else {
              isMultipleGcc = ARConstants.COMMON_VALUE_YES;
          }
          responseDocument = gcBO.processRequest(request);
        } else if (action.equals(ARConstants.ACTION_PARM_DISPLAY_SEARCH)) {
          // No need to generate document.
        } else {
          // Error. Undefined action.
          return mapping.findForward(ARConstants.ACTION_ERROR);
        }
        
        nextAction = nextGccAction(getForwardState(responseDocument), action);      
        
        // Retrieve the parameter map set by data filter and update gcc_action with the next action.
        params = actionHelper.updateGccAction(request, nextAction);
        
        // Update is_multiple_gcc in the parameter map.
        params = actionHelper.updateIsMultipleGcc(params, isMultipleGcc);
        
        // Retrieve the file to be transformed.
        styleSheetFile = getFile((String)params.get(ARConstants.COMMON_GCC_ACTION), mapping, isMultipleGcc);
      
        // Transform the stylesheet.
        TraxUtil.getInstance().transform(request, response, responseDocument, styleSheetFile, params); 
    }
    catch (SQLException sqlex)
    {
        logger.error("SQLException Caught!", sqlex);
        return mapping.findForward(ARConstants.ACTION_ERROR);        
    }
    catch (Throwable ex)
    {
        logger.error("Exception Caught!", ex);
        return mapping.findForward(ARConstants.ACTION_ERROR);
    }
    finally 
    {
        try {
              if(conn != null && !conn.isClosed()) {
                  logger.debug("Closing connection...");
                  conn.close();
              }
        } catch (Exception e) {
            logger.error("Exception caught when closing connection!", e);
            return mapping.findForward(ARConstants.ACTION_ERROR);
        }
    }
    return null;
  }
  
  
  /**
   * This method gets the forward state 'Y' or 'N'
   * @param responseDocument
   * @return String
   */
  private String getForwardState(Document responseDocument)
  {
    String forwardState="";
    if (responseDocument != null)
    {
      if ((responseDocument.getDocumentElement().getAttributeNode(ARConstants.FORWARD_STATE)) != null)
        forwardState = responseDocument.getDocumentElement().getAttributeNode(ARConstants.FORWARD_STATE).getValue();
    }
    return forwardState;
  }
  
  /**
   * This method gets the next action to be processed
   * @param forwardState
   * @param gccAction
   * @return String
   */
  private String nextGccAction(String forwardState, String gccAction)
  {
   if (!forwardState.equals(""))
   {
       if (forwardState.equals(ARConstants.COMMON_VALUE_YES))
       {
         if (gccAction.equals(ARConstants.ACTION_PARM_SEARCH))
             gccAction = ARConstants.ACTION_PARM_DISPLAY_MAINTAIN;
       }
       else if (forwardState.equals(ARConstants.COMMON_VALUE_NO))
       {
         if (gccAction.equals(ARConstants.ACTION_PARM_SEARCH))
             gccAction = ARConstants.ACTION_PARM_DISPLAY_SEARCH;
       }
   }
   return gccAction;   
  }
  
  /**
   * This method gets the file name depending on action and mapping
   * @param action
   * @param mapping
   * @return File
   * @throws java.lang.Exception
   */
  private File getFile(String action, ActionMapping mapping, String isMultipleGcc) throws Exception
  {
    File styleSheetFile = null;
    if ((action.equals(ARConstants.ACTION_PARM_DISPLAY_MAINTAIN)) ||
       ((action.equals(ARConstants.ACTION_PARM_SEARCH)))) 
    {
       if("Y".equals(isMultipleGcc)) {
          styleSheetFile =  getXSL("multiple", mapping);
       } else {
          styleSheetFile = getXSL("single", mapping);
       }
    }
    // This includes loading the first time and when error occurred.
    else if (action.equals(ARConstants.ACTION_PARM_DISPLAY_SEARCH))
    {  
       styleSheetFile =  getXSL("load", mapping);
    }
    return styleSheetFile;
  }
  
  /**
   * This method gets the XSL File
   * @param xslName
   * @param mapping
   * @return FIle
   */
  private File getXSL(String xslName, ActionMapping mapping)
  {
     File xslFile = null;
     String xslFilePathAndName = null;

     ActionForward forward = mapping.findForward(xslName);
     xslFilePathAndName = forward.getPath();                
     xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
     return xslFile;
  }

   /**
   * This method creates field doc for the error related pages
   * @param request
   * @return Document
   * @throws javax.servlet.ServletException
   */
   public Document createFieldDoc(HttpServletRequest request) throws ServletException
   {
       try
       {
           GiftCertActionHelper actionHelper = new GiftCertActionHelper();
           Document fieldDoc = DOMUtil.getDefaultDocument();
           Element root = (Element) fieldDoc.createElement(ARConstants.XML_ROOT);
           fieldDoc.appendChild(root);
           
           String requestNumber = request.getParameter("gcc_request_number");
           if (requestNumber != null) {
                requestNumber = requestNumber.trim();
                if(!"".equals(requestNumber)) {
                    requestNumber = requestNumber.toUpperCase();
                }
           }
           
           Element fields = (Element) fieldDoc.createElement(ARConstants.XML_FIELD_TOP);
           root.appendChild(fields);
           fields.appendChild(actionHelper.getNameElement(fieldDoc,"gcc_request_number",requestNumber));
           fields.appendChild(actionHelper.getNameElement(fieldDoc,"gcc_coupon_number",request.getParameter("gcc_coupon_number")));
                    
           return fieldDoc;
       }
       catch (Exception ex)
       {
           logger.error(ex);
           throw new ServletException(ex.getMessage());
       }   
   }
 
}