package com.ftd.accountingreporting.action;
import com.ftd.accountingreporting.bo.GiftCertRecipBO;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;
import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * This class handles requests related to adding recipients: display_add, add, exit_add
 * @author Christy Hu
 */
public class AddRecipientAction extends Action
{
  private static Logger logger  = new Logger("com.ftd.accountingreporting.action.AddRecipientAction");
  private static String ADDED = "added";
  private static String LOAD = "load";
  
  /**
   * This is the main action called from the Struts framework.
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException
    {
        logger.debug("Entering AddRecipientAction...");
        Connection conn = null;
        Document responseDocument = null;
        HashMap params = null;
        String action = null;
        String nextAction = null;
        String dsname = null;  
        File styleSheetFile = null;
        GiftCertActionHelper actionHelper = new GiftCertActionHelper();
        
        try
        {
          action = request.getParameter(ARConstants.COMMON_GCC_ACTION);
          logger.debug("action is " + action);
          
          // Create Connection object.
          dsname = ConfigurationUtil.getInstance().getProperty(ARConstants.CONFIG_FILE,
                                                    ARConstants.DATASOURCE_NAME);
          conn = DataSourceUtil.getInstance().getConnection(dsname);
          
          if ((action.equals(ARConstants.ACTION_PARM_DISPLAY_ADD)) ||
              (action.equals(ARConstants.ACTION_PARM_ADD)) ||
              (action.equals(ARConstants.ACTION_PARM_EXIT_ADD)))
          {
              responseDocument = (new GiftCertRecipBO(conn)).processRequest(request);
          } else {
              return mapping.findForward(ARConstants.ACTION_ERROR);
          }
          
          // Retrive next action.
          nextAction = nextGccAction(action);
          logger.debug("nextAction is " + nextAction);
          
          // Retrieve the parameter map set by data filter and update gcc_action with the next action.
          params = actionHelper.updateGccAction(request, nextAction);
          
          // Retrieve the file to be transformed.
          styleSheetFile = getFile(nextAction, mapping);
           
          // Transform the stylesheet.
          TraxUtil.getInstance().transform(request, response, responseDocument, styleSheetFile, params);          
        }
        catch (SQLException sqlex)
        {
           logger.error("SQLException Caught!", sqlex);
           return mapping.findForward(ARConstants.ACTION_ERROR);        
        }
        catch (Throwable ex)
        {
           logger.error("Exception Caught!", ex);
           return mapping.findForward(ARConstants.ACTION_ERROR);
        }
        finally 
        {
           try {
              if(conn != null && !conn.isClosed()) {
                  logger.debug("Closing connection...");
                  conn.close();
              }           
           } catch (Exception e) {
              logger.error("Exception caught when closing connection!", e);
              return mapping.findForward(ARConstants.ACTION_ERROR);
           }
        }
        return null;
   }
   
   /**
   * Gets the file depending upon the action and mapping
   * @param action
   * @param mapping
   * @return File
   */
   private File getFile(String action, ActionMapping mapping)
   {
      File styleSheetFile = null;
      if ((action.equals(ARConstants.ACTION_PARM_DISPLAY_CREATE)))
      {
          styleSheetFile =  getXSL(ADDED, mapping);
      }
      else if ((action.equals(ARConstants.ACTION_PARM_DISPLAY_ADD)))
      {
          styleSheetFile =  getXSL(LOAD, mapping);
      }
      return styleSheetFile;
   }
  
  /**
   * Gets the XSL file name for the xslName and mapping specified
   * @param xslName
   * @param mapping
   * @return File
   */
  private File getXSL(String xslName, ActionMapping mapping)
  {
     File xslFile = null;
     String xslFilePathAndName = null;

     ActionForward forward = mapping.findForward(xslName);
     xslFilePathAndName = forward.getPath();  
     logger.debug("XSL is " + xslFilePathAndName);
     xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
     return xslFile;
  }
   

  /**
   * gets next action for the current action passed
   * @param gccAction
   * @return String
   */
  private String nextGccAction(String gccAction)
  {
      if ((gccAction.equals(ARConstants.ACTION_PARM_EXIT_ADD)) || (gccAction.equals(ARConstants.ACTION_PARM_ADD)))
      {
          gccAction = ARConstants.ACTION_PARM_DISPLAY_CREATE;
      } // DISPLAY_ADD stays the same
      
      return gccAction;
  }



}