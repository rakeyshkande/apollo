package com.ftd.accountingreporting.action;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.util.Enumeration;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * This class dispathes to the Login page or an HTML error page.
 * @author Christy Hu
 */
public final class ErrorAction extends Action 
{

  private static Logger logger  = new Logger("com.ftd.accountingreporting.action.ErrorAction");
  private static String ERROR_PAGE = "error_page";
  
  /**
   * This is the main action called from the Struts framework.
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException, Exception
  {
      ActionForward forward = null;
      String action = null;
		
      try
      {
        logger.debug("Entering ErrorAction...");
        action = request.getParameter(ARConstants.COMMON_GCC_ACTION);      
        logger.debug("action is: " + action);
          
        /* If action is equal to login take the user to the login page
         * else take the user to the error page.
         */
        if(action != null && action.trim().equals(ARConstants.ACTION_LOGIN))
        {
          try
          {
            ConfigurationUtil cu = ConfigurationUtil.getInstance();
            String baseUrl = cu.getFrpGlobalParm(ARConstants.BASE_CONFIG, ARConstants.BASE_URL);
            forward = mapping.findForward(ARConstants.ACTION_LOGIN);
            forward = new ActionForward(forward.getPath(), true);
          }
          catch(Throwable t)
          {
            forward = mapping.findForward(ERROR_PAGE);				
          }
        }
        else
        {
          forward = mapping.findForward(ERROR_PAGE);
        }
      }
      catch(Throwable t)
      {
        logger.error(t);			
      }
      return forward;
  }
  
}