package com.ftd.accountingreporting.action;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.ftd.accountingreporting.bo.GiftCertBO;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

/**
 * This class handles request related to maintaining a gift certificate coupon.
 * display_maintain, maintain, exit_maintain
 * @author Christy Hu
 */
public final class SingleGiftCertAction extends Action 
{

  private static Logger logger  = new Logger("com.ftd.accountingreporting.action.SingleGiftCertAction");
  private static final String EXIT = "exit";
  private static final String LOAD = "load";
  
  /**
   * This is the main action called from the Struts framework.
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException, Exception
  {
    logger.debug("Entering SingleGiftCertAction...");
    String couponNumber = request.getParameter("gcc_coupon_number");
	logger.debug("COUPON NO:" + couponNumber);
    Connection conn = null;
    Document responseDocument = DOMUtil.getDocument();
    HashMap params = null;
    GiftCertBO gccBO = null;
    String action = null;
    String nextAction = null;
    String dsname = null;
    File styleSheetFile = null;
    GiftCertActionHelper actionHelper = new GiftCertActionHelper();    
         
    try
    {

        action = request.getParameter(ARConstants.COMMON_GCC_ACTION);
        logger.debug("action is: " + action);
        dsname = ConfigurationUtil.getInstance().getProperty(ARConstants.CONFIG_FILE,
                                                    ARConstants.DATASOURCE_NAME);
        conn = DataSourceUtil.getInstance().getConnection(dsname);

        gccBO = new GiftCertBO(conn);
        
        //get the related XML document for the specified GCC_ACTION
        if ((action.equals(ARConstants.ACTION_PARM_MAINTAIN))||
             action.equals(ARConstants.ACTION_PARM_DISPLAY_MAINTAIN))
        {
            responseDocument = gccBO.processRequest(request);
        } else if (action.equals(ARConstants.ACTION_PARM_EXIT_MAINTAIN)) {
            // No need to generate a document.
        } else {
            // Error. Undefined action.
            return mapping.findForward(ARConstants.ACTION_ERROR);
        }
        
        nextAction = nextGccAction(getForwardState(responseDocument), action);
        logger.debug("nextAction is : " + nextAction);
        
        // Retrieve the parameter map set by data filter and update gcc_action with the next action.
        params = actionHelper.updateGccAction(request, nextAction);
        
        // Retrieve the file to be transformed.
        styleSheetFile = getFile(nextAction, mapping);
        
        // Transform the stylesheet.
        TraxUtil.getInstance().transform(request, response, responseDocument, styleSheetFile, params); 
    }
    catch (SQLException sqlex)
    {
        logger.error("SQLException Caught!", sqlex);
        return mapping.findForward(ARConstants.ACTION_ERROR);        
    }
    catch (Throwable ex)
    {
        logger.error("Exception Caught!", ex);
        return mapping.findForward(ARConstants.ACTION_ERROR);
    }
    finally 
    {
        try {
              if(conn != null && !conn.isClosed()) {
                  logger.debug("Closing connection...");
                  conn.close();
              }
        } catch (Exception e) {
            logger.error("Exception caught when closing connection!", e);
            return mapping.findForward(ARConstants.ACTION_ERROR);
        }
    }
    return null;
  }
  
  /**
   * This method gets the forward state 'Y' or 'N'
   * @param responseDocument
   * @return String
   */
  private String getForwardState(Document responseDocument)
  {
    String forwardState="";
    if (responseDocument != null)
    {
      if ((responseDocument.getDocumentElement().getAttributeNode(ARConstants.FORWARD_STATE)) != null)
        forwardState = responseDocument.getDocumentElement().getAttributeNode(ARConstants.FORWARD_STATE).getValue();
    }
    return forwardState;
  }
  
  /**
   * This method gets the next action to be processed
   * @param forwardState
   * @param gccAction
   * @return String
   */
  private String nextGccAction(String forwardState, String gccAction) throws Exception
  {
   if (!forwardState.equals(""))
   {
       if (forwardState.equals(ARConstants.COMMON_VALUE_YES))
       {
         if ((gccAction.equals(ARConstants.ACTION_PARM_MAINTAIN)) ||
                  (gccAction.equals(ARConstants.ACTION_PARM_EXIT_MAINTAIN))) 
             gccAction = ARConstants.ACTION_PARM_DISPLAY_SEARCH;
       }
       else if (forwardState.equals(ARConstants.COMMON_VALUE_NO))
       {
         if  (gccAction.equals(ARConstants.ACTION_PARM_MAINTAIN))
             gccAction = ARConstants.ACTION_PARM_DISPLAY_MAINTAIN;
       }
   }
   return gccAction;   
  }
  
  /**
   * This method gets the file name depending on action and mapping
   * @param action
   * @param mapping
   * @return File
   * @throws java.lang.Exception
   */
  private File getFile(String action, ActionMapping mapping) throws Exception
  {
    File styleSheetFile = null;
    if (action.equals(ARConstants.ACTION_PARM_DISPLAY_MAINTAIN)) 
    {
       styleSheetFile =  getXSL(LOAD, mapping);
    }
    else if (action.equals(ARConstants.ACTION_PARM_DISPLAY_SEARCH))
    {
       styleSheetFile =  getXSL(EXIT, mapping);
    }
    return styleSheetFile;
  }
  
  /**
   * This method gets the XSL File
   * @param xslName
   * @param mapping
   * @return FIle
   */
  private File getXSL(String xslName, ActionMapping mapping)
  {
     File xslFile = null;
     String xslFilePathAndName = null;

     ActionForward forward = mapping.findForward(xslName);
     xslFilePathAndName = forward.getPath();                
     xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
     return xslFile;
  }
  

   
   /**
   * This method creates field doc for the error related pages
   * @param request
   * @return Document
   * @throws javax.servlet.ServletException
   */
   private Document createFieldDoc(HttpServletRequest request, Connection conn) throws Exception
   {
       GiftCertActionHelper actionHelper = new GiftCertActionHelper();
       Document fieldDoc = actionHelper.createFieldDocWithRoot(request);
       Element fields = (Element)(fieldDoc.getElementsByTagName(ARConstants.XML_FIELD_TOP)).item(0);
       GiftCertBO gccBO = new GiftCertBO(conn);
       String company = request.getParameter("gcc_company");
		if(null != company){
			company = company.trim().toLowerCase();
		}else{
			company = ARConstants.FTD_COMPANY_ID;
		}
       String requestNumber = request.getParameter("gcc_request_number");
       if (requestNumber != null) {
          requestNumber = requestNumber.trim();
          if(!"".equals(requestNumber)) {
              requestNumber = requestNumber.toUpperCase();
          }
       }
       
       fieldDoc = gccBO.getDropDownValues(fieldDoc,request);
           
           if (request.getParameter(ARConstants.ACTION_PARM_IS_MULTIPLE_GCC).equals(ARConstants.COMMON_VALUE_YES))
           {
              gccBO.getStatusCount(fieldDoc,requestNumber);
              fieldDoc.getDocumentElement().appendChild(fieldDoc.importNode(gccBO.getStatusList(request, ARConstants.COMMON_VALUE_YES,request.getParameter("gcc_type")).getDocumentElement(), true));
           }
           else if (request.getParameter(ARConstants.ACTION_PARM_IS_MULTIPLE_GCC).equals(ARConstants.COMMON_VALUE_NO))
           {
              fields.appendChild(actionHelper.getNameElement(fieldDoc,"gcc_status_single_display",gccBO.getStatusSingle(request.getParameter("gcc_coupon_number"), company)));
              fieldDoc.getDocumentElement().appendChild(fieldDoc.importNode(gccBO.getRedemDateAndOrderDetailId(request.getParameter("gcc_coupon_number")).getDocumentElement(), true)); 
              fieldDoc.getDocumentElement().appendChild(fieldDoc.importNode(gccBO.getStatusList(request, ARConstants.COMMON_VALUE_NO,request.getParameter("gcc_type")).getDocumentElement(), true));
           }       
       return fieldDoc;
   }
}
