package com.ftd.accountingreporting.action;

import com.ftd.accountingreporting.bo.ReconBillingBO;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;
import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;


public class PrintBillingAction extends Action
{
    private static Logger logger = 
        new Logger("com.ftd.accountingreporting.action.PrintBillingAction");

    /**
     * This method does the following: 
     * �	retrieve cbr_action from request object
     * �	if it is �print�, 
     *      o	Delegate processing to the handlePrintBilling procedure of the
     *          business object (ReconBillingBO), passing the request. 
     *          The business object will return a HashMap with 2 elements:
     *          ?	�doc�: of type Document, which is the Document to be 
     *              transformed.
     *          ?	�forwardName�: of type String, which is the forward name 
     *              used to find the forward path in struts-config.xml 
     *              for this action.
     *      o	Retrieve �forwardName� from the HashMap and find the XSL to be transformed.
     *      o	Retrieve �doc� from the HashMap
     *      o	Retrieve the HashMap parameters by calling getParameters(request);
     *      o	Transform the XSL.
     * �	If it is not �print� then forward to ErrorAction.
     *      return mapping.findForward(ARConstants.ACTION_ERROR);
     * @param forwardName - String
     * @param mapping - ActionMapping
     * @return File
     * @throws n/a
     */ 
    public ActionForward execute(ActionMapping mapping, ActionForm form, 
        HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering execute");
        }
        
        Connection conn = null;
        try
        {
            String action = request.getParameter(ARConstants.COMMON_CBR_ACTION);
            if(logger.isDebugEnabled()){
                logger.debug("action is " + action);
            }
            
            if(action.equals(ARConstants.ACTION_CBR_PRINT))
            {
                 /* Create Connection object. */
                String dsname = ConfigurationUtil.getInstance().getProperty(ARConstants.CONFIG_FILE,
                                                            ARConstants.DATASOURCE_NAME);
                conn = DataSourceUtil.getInstance().getConnection(dsname);
                ReconBillingBO reconBO = new ReconBillingBO(conn);
                HashMap handlePrint = reconBO.handlePrintBilling(request);
                
                String forwardName = (String) handlePrint.get("forwardName");
                Document print = (Document) handlePrint.get("doc");
                //Document xml = (Document) print;
                
                // Retrieve the parameter map set by data filter and update 
                //cbr_action with the next action.
                HashMap params = getParameters(request);
                
                // Retrieve the file to be transformed.
                File styleSheetFile = getXSL(forwardName, mapping);
                
                // Transform the stylesheet.
                TraxUtil.getInstance().transform(request, response, print, 
                    styleSheetFile, params); 
            }
            else
            {
                return mapping.findForward(ARConstants.ACTION_ERROR);
            }
        }
        catch (SQLException sqlex)
        {
           logger.error("SQLException Caught!", sqlex);
           return mapping.findForward(ARConstants.ACTION_ERROR);        
        }
        catch (Throwable ex)
        {
           logger.error("Exception Caught!", ex);
           return mapping.findForward(ARConstants.ACTION_ERROR);
        }
        finally 
        {
           try {
              if(conn != null && !conn.isClosed()) {
                  logger.debug("Closing connection...");
                  conn.close();
              }
           } catch (Exception e) {
              logger.error("Exception caught when closing connection!", e);
              return mapping.findForward(ARConstants.ACTION_ERROR);
           }
            if(logger.isDebugEnabled()){
               logger.debug("Exiting execute");
            } 
        }
        return null;

    }
        
    /**
     * Returns the XSL file to be transformed for the given forward name.
     * @param forwardName - String
     * @param mapping - ActionMapping
     * @return File
     * @throws n/a
     */ 
    private File getXSL(String forwardName, ActionMapping mapping)
    {
    
        if(logger.isDebugEnabled()){
            logger.debug("Entering getXSL");
            logger.debug("forwardName : " + forwardName);
        }
        
        File xslFile = null;
        
        try{
            String xslFilePathAndName = null;
            
            ActionForward forward = mapping.findForward(forwardName);
            xslFilePathAndName = forward.getPath();                
            xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
        
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getXSL");
            } 
        }
        
        return xslFile;
    }

    /**
     * �	retrieve the attribute �parameters� HashMap from request 
     * �	retrieve parameter �cbr_action� from request and reset it to the HashMap.
     * �	Return the HashMap.
     * @param request - HttpServletRequest
     * @return HashMap
     * @throws n/a
     */ 
    private HashMap getParameters(HttpServletRequest request)
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering getParameters");
        }
        
        HashMap parameters = new HashMap();
        
        try{
            String action = request.getParameter(ARConstants.COMMON_CBR_ACTION);
            parameters = (HashMap)request.getAttribute("parameters");
            parameters.put("cbr_action", action);
            //parameters.put("showprinter", "false");
           
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getParameters");
            } 
        }
        
        return parameters;
    }
}
