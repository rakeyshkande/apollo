package com.ftd.accountingreporting.action;
import com.ftd.accountingreporting.bo.ReconBillingBO;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;
import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;

public class ViewBillingAction extends Action
{
    private static Logger logger = 
        new Logger("com.ftd.accountingreporting.action.ViewBillingAction");

    /**
     * This method does the following:
     * �	retrieve cbr_action from request object
     * �	if it is �load� or �view�, 
     *      o	Delegate processing to the handleViewBilling procedure of the 
     *          business object (ReconBillingBO), passing the request. The 
     *          business object will return a HashMap with 2 elements:
     *          ?	�doc�: of type Document, which is the Document to be transformed.
     *          ?	�forwardName�: of type String, which is the forward name 
     *              used to find the forward path in struts-config.xml for this action.
     *      o	Retrieve �forwardName� from the HashMap and find the XSL to be transformed.
     *      o	Retrieve �doc� from the HashMap
     *      o	Retrieve the HashMap parameters by calling getParameters(request);
     *      o	Transform the XSL.
     * �	If it is not �load� or �view� then forward to ErrorAction.
     *      return mapping.findForward(ARConstants.ACTION_ERROR);
     * @param forwardName - String
     * @param mapping - ActionMapping
     * @return File
     * @throws n/a
     */ 
    public ActionForward execute(ActionMapping mapping, ActionForm form, 
        HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering execute");
        }
        
        Connection conn = null;
        try
        {
            String action = request.getParameter(ARConstants.COMMON_CBR_ACTION);
            if(logger.isDebugEnabled()){
                logger.debug("action is " + action);
            }
            
            if(action.equals(ARConstants.ACTION_CBR_LOAD) || action.equals(ARConstants.ACTION_CBR_VIEW))
            {
                 /* Create Connection object. */
                String dsname = ConfigurationUtil.getInstance().getProperty(ARConstants.CONFIG_FILE,
                                                            ARConstants.DATASOURCE_NAME);
                conn = DataSourceUtil.getInstance().getConnection(dsname);
                ReconBillingBO reconBO = new ReconBillingBO(conn);
                HashMap viewBilling = reconBO.handleViewBilling(request);
                
                String forwardName = (String) viewBilling.get("forwardName");
                Document billing = (Document) viewBilling.get("doc");
                //Document xmlbill = (Document) billing;
                
                // Retrieve the parameter map set by data filter and update gcc_action with the next action.
                HashMap params = getParameters(request);
                
                // Retrieve the file to be transformed.
                File styleSheetFile = getXSL(forwardName, mapping);
                
                // Transform the stylesheet.
                TraxUtil.getInstance().transform(request, response, billing, 
                    styleSheetFile, params); 
            }
            else
            {
                return mapping.findForward(ARConstants.ACTION_ERROR);
            }
        }
        catch (SQLException sqlex)
        {
           logger.error("SQLException Caught!", sqlex);
           return mapping.findForward(ARConstants.ACTION_ERROR);        
        }
        catch (Throwable ex)
        {
           logger.error("Exception Caught!", ex);
           return mapping.findForward(ARConstants.ACTION_ERROR);
        }
        finally 
        {
           try {
              if(conn != null && !conn.isClosed()) {
                  logger.debug("Closing connection...");
                  conn.close();
              }
           } catch (Exception e) {
              logger.error("Exception caught when closing connection!", e);
              return mapping.findForward(ARConstants.ACTION_ERROR);
           }
            if(logger.isDebugEnabled()){
               logger.debug("Exiting execute");
            } 
        }
        return null;

    }
        
    /**
     * Returns the XSL file to be transformed for the given forward name.
     * @param forwardName - String
     * @param mapping - ActionMapping
     * @return File
     * @throws n/a
     */ 
    private File getXSL(String forwardName, ActionMapping mapping)
    {
    
        if(logger.isDebugEnabled()){
            logger.debug("Entering getXSL");
            logger.debug("forwardName : " + forwardName);
        }
        
        File xslFile = null;
        
        try{
            String xslFilePathAndName = null;
            
            ActionForward forward = mapping.findForward(forwardName);
            xslFilePathAndName = forward.getPath();                
            xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
        
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getXSL");
            } 
        }
        
        return xslFile;
    }

    /**
     * �	If cbr_action is �load�  // This is when the user enters the 
     *      reconciliation page from COM application.
     *      o	create new HashMap. 
     *      o	Retrieve master_order_number from request and add it to the 
     *          HashMap as �master_order_number�;
     *      o	Retrieve timerEntityHistoryId from request and add it to the HashMap.
     *      o	Retrieve order_guid from request and add it to the HashMap as �order_guid�.
     *      o	retrieve parameter �cbr_action� from request and reset it to the HashMap.
     * �	else //  cbr_action is �view�. This is when the user enters the reconciliation page from within the Accounting application.
     *      o	retrieve the attribute �parameters� HashMap from request 
     *      o	retrieve parameter �cbr_action� from request and reset it to the HashMap.
     * �	Return the HashMap.
     * @param request - HttpServletRequest
     * @return HashMap
     * @throws n/a
     */ 
    private HashMap getParameters(HttpServletRequest request)
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering getParameters");
        }
        
        HashMap comParameters = new HashMap();
        
        try{
            String action = request.getParameter(ARConstants.COMMON_CBR_ACTION);
        
            
            if(action.equals(ARConstants.ACTION_CBR_LOAD))
            {
                comParameters = (HashMap)request.getAttribute("parameters");
                String masterOrderNum = 
                    request.getParameter("master_order_number")!=null?request.getParameter("master_order_number"):"";
                String cbrAction = 
                    request.getParameter("cbr_action")!=null?request.getParameter("cbr_action"):"";
                String orderGUID = 
                    request.getParameter("order_guid")!=null?request.getParameter("order_guid"):"";

                /*check if security token and context exist */
                String securityToken = (String) comParameters.get("securitytoken");
                if(securityToken==null)
                {
                    securityToken = request.getParameter("securitytoken")!=null?request.getParameter("securitytoken"):"";
                    comParameters.put("securitytoken", securityToken);
                }
                String context = (String) comParameters.get("context");
                if(context==null)
                {
                    context = request.getParameter("context")!=null?request.getParameter("context"):"";
                    comParameters.put("context", context);
                }

                comParameters.put("master_order_number",masterOrderNum);
                comParameters.put("order_guid",orderGUID);
                comParameters.put("cbr_action",cbrAction);
                request.setAttribute("parameters", comParameters);
            }else if(action.equals(ARConstants.ACTION_CBR_VIEW))
            {
                comParameters = (HashMap)request.getAttribute("parameters");
                String cbrAction = 
                    request.getParameter("cbr_action")!=null?request.getParameter("cbr_action"):"";
                comParameters.put("cbr_action", cbrAction);
                /*check if security token and context exist */
                String securityToken = (String) comParameters.get("securitytoken");
                if(securityToken.equals(""))
                {
                    securityToken = request.getParameter("securitytoken")!=null?request.getParameter("securitytoken"):"";
                    comParameters.put("securitytoken", securityToken);
                }
                String context = (String) comParameters.get("context");
                if(context.equals(""))
                {
                    context = request.getParameter("context")!=null?request.getParameter("context"):"";
                    comParameters.put("context", context);
                }
                request.setAttribute("parameters", comParameters);
            }
            
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getParameters");
            } 
        }
        
        return comParameters;
    }


}
