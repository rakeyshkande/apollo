package com.ftd.accountingreporting.action;

import com.ftd.accountingreporting.altpay.handler.PayPalCartHandler;
import com.ftd.accountingreporting.altpay.handler.PayPalEODHandler;
import com.ftd.accountingreporting.altpay.handler.UACartHandler;
import com.ftd.accountingreporting.altpay.vo.PayPalBillingDetailVO;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.timer.TimerServiceSessionEJB;
import com.ftd.accountingreporting.vo.EODMessageVO;
import com.ftd.accountingreporting.vo.EODPaymentVO;
import com.ftd.milespoints.webservice.MilesPointsException_Exception;
import com.ftd.milespoints.webservice.MilesPointsRequest;
import com.ftd.milespoints.webservice.MilesPointsService;
import com.ftd.milespoints.webservice.PartnerResponse;
import com.ftd.milespoints.webservice.UpdateMilesResp;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.id.UnitedTransactionIdGeneratorImpl;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;
import java.io.IOException;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.springframework.beans.BeansException;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class EODUtilAction extends Action
{
    private static Logger logger = 
        new Logger("com.ftd.accountingreporting.action.EODUtilAction");
    
    // Set to true when running locally to load PayPal connectivity parameters
    private boolean testMode = false;

    /**
     * This method facilitates the following actions:
     * 1.  Checks for a valid PayPal connection
     * 2.  Timer access exists but functionality to access all machines within the cluster would need to be added for the Timer functionality to work. 
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @return ActionForward
     * @throws IOException
     * @throws ServletException
     */ 
    public ActionForward execute(ActionMapping mapping, ActionForm form, 
        HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException
    {
        if(logger.isDebugEnabled())
            logger.debug("Entering execute");
        
        String action = null;
        try
        {
          action = request.getParameter("action_type");

          if(logger.isDebugEnabled())
              logger.debug("Action is " + action);

          if(action.equals("checkPPConn"))
          {
            try
            {
              response.getWriter().write(testPayPalConnection());
            }
            catch (Throwable t)
            {
               logger.error("Exception Caught!", t);
               response.getWriter().write("Error occurred:" + t);
            }
          }
          else if(action.equals("isTimerRunning"))
          {
            try
            {
              response.getWriter().write(isTimerRunning(request.getParameter("timerName")));
            }
            catch (Throwable t)
            {
               logger.error("Exception Caught!", t);
               response.getWriter().write("Error occurred:" + t);
            }
          }
          else if(action.equals("checkUnitedConn")) {
        	  response.getWriter().write(testUnitedAirlinesConnection());
          }
          else if(action.equals("stopTimer"))
          {
            try
            {
              response.getWriter().write(stopTimer(request.getParameter("timerName")));
            }
            catch (Throwable t)
            {
               logger.error("Exception Caught!", t);
               response.getWriter().write("Error occurred:" + t);
            }
          }
          else if(action.equals("viewTimers"))
          {
            try
            {
              response.getWriter().write(viewTimers());
            }
            catch (Throwable t)
            {
               logger.error("Exception Caught!", t);
               response.getWriter().write("Error occurred:" + t);
            }
          }
          else if(action.equals("load"))
          {
            // Retrieve the file to be transformed.
            File xslFile = getXSL("eodUtil", mapping);
            
            // Transform the stylesheet.
            HashMap params = (HashMap)request.getAttribute("parameters");
            TraxUtil.getInstance().transform(request, response, DOMUtil.getDefaultDocument(), 
                xslFile, params); 
          }
        }
        catch (Throwable t)
        {
           logger.error("Exception Caught!", t);
           return mapping.findForward(ARConstants.ACTION_ERROR);
        }

        if(logger.isDebugEnabled())
           logger.debug("Exiting execute");

        return null;
    }
        
  /**
   * Tests connectivity to PayPal service.
   * @return String - Message to display based on outcome of test
   * @throws Throwable
 * @throws BeansException 
   */ 
  private String testPayPalConnection() throws Throwable
  {
    String beanName = "pp_eodHandler";

    if(logger.isDebugEnabled())
    {
      logger.info("Asking Spring framework for bean " + beanName);
      logger.info("Test mode is " + testMode);
    }
    
    ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("spring-ejb-context.xml");     
    PayPalEODHandler handler = (PayPalEODHandler)ctx.getBeanFactory().getBean(beanName);
 
    if(handler.testConnection())
      return("PayPal connectivity succeeded!");
    else
      return("PayPal connectivity failed!");
  }
  
  /**
   * Call our internal MilesPointsService which calls United to get a member profile.
   * We're passing in a dummy memberID so the call should ALWAYS fail, and we detect if it fails the way we expect.
   * @return
   * @throws Exception
   */
  private String testUnitedAirlinesConnection() throws Exception {
	  
	  
	  String beanName = "uaCartHandler";
	  ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("spring-ejb-context.xml");        
	  UACartHandler cartHandler = (UACartHandler)ctx.getBeanFactory().getBean(beanName);
	  ConfigurationUtil util = new ConfigurationUtil();
	  cartHandler.setConfigurationUtil(util);
	  
	  MilesPointsService mps = cartHandler.getMilesPointsService();
	  
	  MilesPointsRequest req = new MilesPointsRequest();
	  req.setClientUserName(util.getSecureProperty("SERVICE", "SVS_CLIENT"));
	  req.setClientPassword(util.getSecureProperty("SERVICE", "SVS_HASHCODE"));
	  req.setMembershipType("UA");
	  req.setMembershipNumber("connectivity test");
	  PartnerResponse partnerResp;
	  logger.error("Performing a United Airlines connectivity check.  A stack trace after this message is OK.");
	  try {
		partnerResp = mps.checkMiles(req);
	  } catch (MilesPointsException_Exception e) {
		if (e.getMessage().equals("javax.xml.ws.soap.SOAPFaultException: ACCOUNT NOT FOUND")) {
			//this exact message tells us that we were able to hit United, and even though the memberID is not valid, the connection is alive
			return "United connectivity succeeded!";
		} else {
			return "!United connectivity failed!: " + e.getMessage();
		}
	  } catch(Exception e2) {
		  return "!United connectivity failed!: " + e2.getMessage();
	  }

	  // this should really not happen since we don't have a valid member number the call to united should throw an exception
	  // one way or another.
	  return "!United connectivity failed!";
  }


  /**
   * Obtains all Timers currently running.
   * @return String - Timers currently running
   * @throws Throwable
   */ 
  private String viewTimers() throws Throwable
  {
    // Functionality to access all machines within the cluster would need to be added for the Timer functionality to work. 
    TimerServiceSessionEJB tsejb = null;

    return tsejb.getTimers();
  }


  /**
   * Determines if a Timer is running.
   * @param timerName - Name of the timer to investigate
   * @return String - Message to display based on outcome of test
   * @throws Throwable
   */ 
  private String isTimerRunning(String timerName) throws Throwable
  {
    if(timerName == null || timerName.equals(""))
      return("Please enter a Timer name!");

    if(logger.isDebugEnabled())
      logger.debug("Timer name is " + timerName);

    // Functionality to access all machines within the cluster would need to be added for the Timer functionality to work. 
    TimerServiceSessionEJB tsejb = null;

    if(tsejb.isRunning(timerName))
      return("Timer is running!");
    else
      return("Timer is not running!");
  }


  /**
   * Stops a Timer.
   * @param timerName - Name of the timer to stop
   * @return String - Message to display based on outcome of operation
   * @throws Throwable
   */ 
  private String stopTimer(String timerName) throws Throwable
  {
    if(timerName == null || timerName.equals(""))
      return("Please enter a Timer name!");

    if(logger.isDebugEnabled())
      logger.debug("Timer name is " + timerName);

    // Functionality to access all machines within the cluster would need to be added for the Timer functionality to work. 
    TimerServiceSessionEJB tsejb = null;

    tsejb.stop(timerName);
    return("Timer was stopped!");
  }


  /**
   * Returns the XSL file to be transformed for the given forward name.
   * @param forwardName - String
   * @param mapping - ActionMapping
   * @return File
   * @throws Throwable
   */ 
    private File getXSL(String forwardName, ActionMapping mapping)
    {
    
        if(logger.isDebugEnabled()){
            logger.debug("Entering getXSL");
            logger.debug("forwardName : " + forwardName);
        }
        
        File xslFile = null;
        
        try{
            String xslFilePathAndName = null;
            
            ActionForward forward = mapping.findForward(forwardName);
            xslFilePathAndName = forward.getPath();                
            xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
        
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getXSL");
            } 
        }
        
        return xslFile;
    }

}