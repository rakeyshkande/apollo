package com.ftd.accountingreporting.action;

import com.ftd.accountingreporting.constant.ARConstants;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.ConfigurationUtil;

import java.io.IOException;

/**
 * This class handles exiting the gift cert application and dispatches to the menus.
 * @author Madhu Sudanparab
 * @updated 06/27 by chu: added exception handling.
 */
public class MainMenuAction extends Action
{
  private static Logger logger  = new Logger("com.ftd.accountingreporting.action.MainMenuAction");
  private static final String OP_MENU = "OP_MENU_URL";
  private static final String CS_MENU = "CS_MENU_URL";
  private static final String SA_MENU = "SA_MENU_URL";
  
  /**
   * This is the main action called from the Struts framework.
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException, Exception
  {
       String menuUrl = null;
       String securityToken = request.getParameter(ARConstants.COMMON_PARM_SEC_TOKEN);
       String context = request.getParameter(ARConstants.COMMON_PARM_CONTEXT);
       String user = request.getParameter(ARConstants.COMMON_GCC_USER);
       
       try {

           ConfigurationUtil cu = ConfigurationUtil.getInstance();
           String baseUrl = cu.getFrpGlobalParm(ARConstants.BASE_CONFIG,ARConstants.BASE_URL);
           
           if (user.equals(ARConstants.CONST_USER_OP)) {
               menuUrl = mapping.findForward(OP_MENU).getPath(); 
           } else if (user.equals(ARConstants.CONST_USER_CS)) {
               menuUrl = mapping.findForward(CS_MENU).getPath();
           } else if (user.equals(ARConstants.CONST_USER_SA)) {
               menuUrl = mapping.findForward(SA_MENU).getPath();
           } else {
               return mapping.findForward(ARConstants.ACTION_ERROR);
           }
          
           menuUrl = baseUrl + menuUrl + "&" + ARConstants.COMMON_PARM_SEC_TOKEN + "=" + securityToken + "&" + ARConstants.COMMON_PARM_CONTEXT + "=" + context;
           logger.debug("exiting to " + menuUrl);
           logger.debug("context=" + context);
       } catch (Exception e) {
           return mapping.findForward(ARConstants.ACTION_ERROR);
       }
       return new ActionForward(menuUrl, true);
  }
}