package com.ftd.accountingreporting.reporting.bo;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.reporting.dao.ReportDAO;
import com.ftd.accountingreporting.reporting.vo.ReportParameterVO;
import com.ftd.accountingreporting.reporting.vo.ReportParameterValueVO;
import com.ftd.accountingreporting.reporting.vo.ReportVO;
import com.ftd.accountingreporting.util.AccountingUtil;
import com.ftd.accountingreporting.util.HttpUtil;
import com.ftd.accountingreporting.util.SystemMessager;
import com.ftd.accountingreporting.util.XMLUtil;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.exception.SystemMessengerException;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.security.SecurityManager;

import java.io.IOException;
import java.io.InputStream;

import java.sql.Connection;
import java.sql.SQLException;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.jms.JMSException;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import javax.servlet.http.HttpServletRequest;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.lang.StringUtils;

import org.w3c.dom.Document;

import org.xml.sax.SAXException;


import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.util.HttpURLConnection;

public class ReportBO 
{
  
  private Logger logger = 
        new Logger("com.ftd.accountingreporting.reporting.bo.ReportBO");
  
  private Connection dbConn;
  
  private static final SimpleDateFormat REPORTING_DATE_FORMAT = new SimpleDateFormat ("MM/dd/yyyy");
  
  public ReportBO(Connection con)
  {
    this.dbConn=con;
  }
  
  
   public ReportBO()
  {
    
  }
  
  
  
  public Document retrieveReportSubmissionList( String securityToken) throws Exception
  {
    String userId=this.lookupCurrentUserId(securityToken);

    ReportDAO reportDAO=new ReportDAO(this.dbConn);
    
   // XMLDocument xml=(XMLDocument)reportDAO.getUserExecutableReportList(userId);
    
   // xml.print(System.out);
    
    return reportDAO.getUserExecutableReportList(userId);
  }
  
  
  	private String lookupCurrentUserId(String securityToken)  throws Exception
    {
      
        String userId="";
        if (SecurityManager.getInstance().getUserInfo(securityToken) != null) {
                userId = SecurityManager.getInstance().getUserInfo(securityToken)
                                        .getUserID();
        }else
        {
          throw new Exception("User can not be found for securityToken "+securityToken);
        }
        return userId;
    }
  
  public Document buildReportSubmissionDocument (String reportId)throws Exception
  {
     ReportDAO reportDAO=new ReportDAO(this.dbConn);
     HashMap resultMap=reportDAO.getReportAndParameters(reportId, true);
     ReportVO r = createReportVO(reportDAO, resultMap);
     
    String xml=r.toXML();
    Document doc= DOMUtil.getDocument(xml);
    
    //add the default date
    Calendar c=Calendar.getInstance();
    c.setTime(new Date());
    
    //Check to see if there is an offset for the run date
    List parmList = r.getParameterList();
    int offset = -1;
    logger.debug("parmList size = "+String.valueOf(parmList.size()));
    
    for( int idx=0; idx<parmList.size();idx++) {
        ReportParameterVO p = (ReportParameterVO)parmList.get(idx);
        logger.debug(p.getDisplayName());
        if( StringUtils.equals(p.getDisplayName(),"Run Date") ) {
            logger.debug("Found the [Run Date] parameter.");
            List paramValueList = p.getParameterValueList();
            Iterator it = paramValueList.iterator();
            while( it.hasNext() ) {
                ReportParameterValueVO v = (ReportParameterValueVO)it.next();
                if( StringUtils.equals("SINGLE_DATE_TEMPLATE",v.getDisplayText())) {
                    logger.debug("Found the \"SINGLE_DATE_TEMPLATE\" value");
                    String strVal = v.getDefaultFormValue();
                    logger.debug("Default value is for "+String.valueOf(p.getId())+" = "+strVal);
                    if( StringUtils.isNumeric(strVal) ) {
                        try {
                            offset = Integer.parseInt(strVal);
                        } catch (NumberFormatException nfe) {
                            logger.error("Error converting \"Run Date\" default value of "+strVal+" to a number.  Defaulting to -1",nfe);
                        }
                    } else {
                        logger.error("Invalid \"Run Date\" default value of "+strVal+".  Defaulting to -1");
                    }
                }
            }
        }
    }
    
    logger.debug("Offset is "+String.valueOf(offset));

    if( offset!=0 ) {
        c.add(Calendar.DATE,offset);
    }
    
    String dateStr=REPORTING_DATE_FORMAT.format(c.getTime());
    XMLUtil.addElementToXML("default-date", dateStr, doc);
      
    return doc;
  }

  ReportVO createReportVO(ReportDAO reportDAO, HashMap resultMap) throws java.io.IOException, Exception, javax.xml.parsers.ParserConfigurationException, org.xml.sax.SAXException, java.sql.SQLException
  {
    CachedResultSet report = (CachedResultSet)resultMap.get("OUT_RERORT_CUR");
    ReportVO r = new ReportVO();
    List parmList = new ArrayList();
    List parmNameList= new ArrayList();
    if (report.next())
    {
      r.setId(report.getString("report_id"));
      r.setName(report.getString("name"));
      r.setDescription(report.getString("description"));
      r.setFilePrefix(report.getString("report_file_prefix"));
      r.setCategoryName(report.getString("report_category"));
      r.setDaysSpan(report.getInt("report_days_span"));
      String flag = report.getString("holiday_indicator");
      r.setHolidayReport(StringUtils.isNotBlank(flag) && flag.trim().equalsIgnoreCase("Y"));
      r.setNotes(report.getString("notes"));
      r.setOracleRDFName(report.getString("oracle_rdf"));
      r.setOutputType(report.getString("report_output_type"));
      r.setRecourceId(report.getString("acl_name"));
      r.setServerName(report.getString("server_name"));
      r.setType(report.getString("REPORT_TYPE"));
      String eod=report.getString("end_of_day_required");
      r.setEndOfDayRequired(StringUtils.isNotBlank(eod) && eod.trim().equalsIgnoreCase("Y"));
    }
    CachedResultSet param = (CachedResultSet)resultMap.get("OUT_PARM_CUR");
    while (param.next())
    {
      ReportParameterVO p = new ReportParameterVO();
      p.setId(param.getString("report_parm_id"));
      p.setDisplayName(param.getString("display_name"));
      String oracleParameterNames=param.getString("oracle_parm_name");
                      
      p.setOracleReportParameterName(oracleParameterNames);
      p.setType(param.getString("report_parm_type"));
      p.setSortOrder(param.getInt("sort_order"));
      p.setValidation(param.getString("validation"));
      String flag = param.getString("required_indicator");
      p.setRequired(StringUtils.isNotEmpty(flag) && flag.trim().equalsIgnoreCase("Y"));
      String allowFutureDate=param.getString("allow_future_date");
      p.setAllowFutureDate(StringUtils.isNotBlank(allowFutureDate) && allowFutureDate.trim().equalsIgnoreCase("Y"));
      
      logger.debug("allowFutureDate="+allowFutureDate);
      
      logger.debug("allowFutureDate flag="+p.isAllowFutureDate());
      parmList.add(p);
      //add to required parameter name list
      if(p.isRequired())
      {
        parmNameList.add(p.getOracleReportParameterName());
      }
    }
    if (resultMap.get("OUT_VALUE_CUR") != null)
    {
      CachedResultSet value = (CachedResultSet)resultMap.get("OUT_VALUE_CUR");
      while (value.next())
      {
        ReportParameterValueVO v = new ReportParameterValueVO();
        v.setId(value.getString("report_parm_value_id"));
        v.setDbStatmentId(value.getString("db_statement_id"));
        v.setDefaultFormValue(value.getString("default_form_value"));
        v.setSortOrder(value.getInt("sort_order"));
        v.setDisplayText(value.getString("display_text"));
        if (StringUtils.isNotEmpty(v.getDbStatmentId()))
        {
          reportDAO.setReportParameterValueOptionMap(v, v.getDbStatmentId());
        }
        String paramId = value.getString("report_parm_id");
        if(StringUtils.isNotBlank(paramId)){
          Iterator it = parmList.iterator();
          while (it.hasNext())
          {
            ReportParameterVO p = (ReportParameterVO)it.next();
            
              if (paramId.equals(p.getId()))
              {
                p.getParameterValueList().add(v);
              }

          }
        }
      }
    }
    
    
    r.setRequiredParmList(parmNameList);
    r.setParameterList(parmList);
    return r;
  }
  
  
  
  public void setValuesFromSubmissionForm(HttpServletRequest request, Map parmValuesMap, ReportVO reportVO) throws Exception
  {
    
    List parameterVOList=reportVO.getParameterList();
    Iterator it=parameterVOList.iterator();
    while(it.hasNext())
    {
      
      ReportParameterVO param=(ReportParameterVO)it.next();
      String oracleParameterNames=param.getOracleReportParameterName();
      if(oracleParameterNames.indexOf(",")==-1)
      {
         setParameterValue(oracleParameterNames, parmValuesMap,  param, request);
     
      }else
      {
        StringTokenizer tokenizer=new StringTokenizer(oracleParameterNames, ",");
        while(tokenizer.hasMoreTokens())
        {
          String oracleParameterName=tokenizer.nextToken();
          setParameterValue(oracleParameterName, parmValuesMap, param, request);
        }
      }
      
    }
   
   
  }
  
  

  void setParameterValue(String oracleParameterName, Map parmValuesMap,  ReportParameterVO param, HttpServletRequest request) throws Exception
  {
    
    
        //for multiple selects and checkboxes.
        String[] values=request.getParameterValues(oracleParameterName);
        StringBuffer valueBuf=new StringBuffer();
        if(values!=null){
          for(int i=0; i<values.length; i++){
            if(values[i] != null && values[i].length() > 0) {
                if(validateDate(param.getValidation()))
                {
                   Date date=new AccountingUtil().formatDateStringToCal(values[i]).getTime();
                   values[i]=REPORTING_DATE_FORMAT.format(date);
                   
                }
                
                valueBuf.append(values[i]);
                if(i!=values.length-1)
                {
                  valueBuf.append(",");
                }
            }
          }
          parmValuesMap.put(oracleParameterName, valueBuf.toString());
        }
        
    
    
    
   
  }
  
  
  
  
  
  private boolean validateDate(String validation)
  {
            if(validation!=null)
            {
              return (validation.toLowerCase().indexOf("date")!=-1);
            }else
            {
              return false;
            }
  }
  
  
  public Document processSubmissionRequest(HttpServletRequest request)  throws Exception
  {
     Document doc=DOMUtil.getDocument();
     Map parmValuesMap=new HashMap();
     //HashMap errorMessageMap=new HashMap();
     ReportDAO reportDAO=new ReportDAO(this.dbConn);
     
     String reportId=request.getParameter("report_id");
     HashMap resultMap=reportDAO.getReportAndParameters(reportId, false);
     ReportVO reportVO=this.createReportVO(reportDAO, resultMap);
     
     try
        {
          //get all the parameter values.
          this.setValuesFromSubmissionForm(request, parmValuesMap, reportVO);
          String securityToken=request.getParameter(ARConstants.COMMON_PARM_SEC_TOKEN);
          String userId=this.lookupCurrentUserId(securityToken);
          String oracleReportDesFileName=this.createOracleReportDesFileName(userId, reportVO);
          String oracleReportURL=this.createOracleReportURL(parmValuesMap, reportVO);
          //log submission request.
          String submissionId=reportDAO.logSubmissionRequest(reportVO, oracleReportDesFileName, oracleReportURL, userId, "U");
          //dispatch request.
          this.dispatchSubmissionRequest(submissionId);
          //add report name
          XMLUtil.addElementToXML("report_name", reportVO.getName(), doc);
          XMLUtil.addElementToXML("OUT_STATUS", "SUCCESS", doc);
        }
        catch (Exception e)
        {
          //add error messages.
           logger.error("can't not submit report "+reportVO.getName(), e);
           XMLUtil.addElementToXML("OUT_STATUS", "ERROR", doc);
        }
    
     
     return doc;
  }
  
  
  
    public void processScheduledReport (String reportId) throws NamingException, JMSException, Exception
    {
      
      ReportDAO reportDAO=new ReportDAO(this.dbConn);
      HashMap resultMap=reportDAO.getReportAndParameters(reportId, false);
      ReportVO report=this.createReportVO(reportDAO, resultMap);
      String oracleReportDesFileName=this.createOracleReportDesFileName(ARConstants.LOCK_CSR_ID, report);
      String oracleReportURL=this.createOrderReportURL(report, true);
      String submissionId=reportDAO.logSubmissionRequest(report, oracleReportDesFileName, oracleReportURL, ARConstants.LOCK_CSR_ID, "S");
      this.dispatchSubmissionRequest(submissionId);
            

    }
  
  
  
  	public void dispatchSubmissionRequest (String submissionId) throws NamingException, JMSException, Exception
    {
      
            this.dispatchSubmissionRequest(submissionId, 0);

    }
    
    
    
    public void dispatchSubmissionRequest (String submissionId, long runtimeOffset) throws NamingException, JMSException, Exception
    {
      
            MessageToken messageToken = new MessageToken();
            messageToken.setStatus("Reporting");    
            messageToken.setMessage(submissionId); 
            messageToken.setJMSCorrelationID(submissionId);
            if(runtimeOffset>0){
              messageToken.setProperty("JMS_OracleDelay", ""+runtimeOffset, "int");
            }
                      
            Dispatcher.getInstance().dispatchTextMessage(new InitialContext(), messageToken);

    }
    
    
    private String createOrderReportURL(ReportVO reportVO, boolean scheduled) throws IOException, SAXException, ParserConfigurationException, TransformerException,  Exception
    {
      
          String url=reportVO.getServerName();
          if(reportVO.getOutputType().equalsIgnoreCase("Gen"))
          {
            url+=this.getReportConfigKey("LOGIN_REPORT_KEY");
          }else if(reportVO.getOutputType().equalsIgnoreCase("Char"))
          {
            url+=this.getReportConfigKey("CLOB_REPORT_KEY");
          }else
          {
            url+=this.getReportConfigKey("BLOB_REPORT_KEY");
          }
          url +="&report="+reportVO.getOracleRDFName();
          String scheduledParm=reportVO.getScheduleParmValue();
          if(scheduled&&StringUtils.isNotBlank(scheduledParm))
          {
            if(!scheduledParm.startsWith("&"))
            {
              url +="&";
            }
            url+=scheduledParm;
          }
          
          return url;
      
    }
    
    
    private String getReportConfigKey(String propertyName) throws IOException, SAXException, ParserConfigurationException, TransformerException,  Exception
    {
      
       return ConfigurationUtil.getInstance().getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, propertyName);
      
    }
    
    
    	public String createOracleReportURL (Map paramValuesMap, ReportVO reportVO) throws IOException, SAXException, ParserConfigurationException, TransformerException,  Exception
      {
        
          String url=this.createOrderReportURL(reportVO, false);
          Iterator paramIt=paramValuesMap.keySet().iterator();
           while(paramIt.hasNext()){
            String paramName=(String) paramIt.next();
            String value=(String)paramValuesMap.get(paramName);
            url+="&"+paramName+"="+value;
          }
                  
          
          return url;
        
      }
  
    
    private String createOracleReportDesFileName(String userId, ReportVO reportVO)
    {
      return reportVO.getFilePrefix()+"_"+new Date().getTime()+"_"+userId;
    }
    
    
    
     public void executeReport(String submissionId) throws SAXException, ParserConfigurationException, IOException, 
               SQLException,  Exception
    {
              logger.debug("in executeReport submissionId="+submissionId);  
              StringBuffer errorMsg=new StringBuffer("REPORT SUBMISSION ERROR: \n");
              boolean hasError=false;
              ReportDAO reportDAO=new ReportDAO(this.dbConn);
              String oracleReportUrl=reportDAO.getOracleReportURL(submissionId);
              logger.debug("oracle report url="+oracleReportUrl);
              
              //submit request to oracle report server.
              if(StringUtils.isBlank(oracleReportUrl))
              {
                //send systme message
                errorMsg.append("ORACLE REPORT URL IS NULL. SUBMISSION ID="+submissionId);
                hasError=true;
              }else{
                  HttpUtil httpUtil=new HttpUtil();
                  StringTokenizer tokenizer=new StringTokenizer(oracleReportUrl,"&");
                  String url="";
                  int count =0;
                  Map parmMap=new HashMap();
                  parmMap.put("p_submission_id", submissionId);
                  while(tokenizer.hasMoreTokens())
                  {
                   String token=tokenizer.nextToken();
                    if(count==0)
                    {
                      url=token;//first token is the URL.
                    }else
                    {
                      int idx=token.indexOf("=");
                      if(idx!=-1)
                      {
                        parmMap.put(token.substring(0,idx), token.substring(idx+1));
                      }
                      
                    }
                    
                    count++;
                  }
                  logger.debug("url="+url);
                  String[] values=url.split("\\?");
                  url =values[0];
                   logger.debug("report url="+url);
                  if(values.length>1){
                    parmMap.put("hidden_run_parameters", values[1]);// hidden_run_parameters is the parameter name for oracle report URL key values.
                  }
                  logger.debug("parm map="+parmMap);
                  //GetMethod getMethod=new GetMethod(oracleReportUrl+"&p_submission_id="+submissionId);
                  //HttpClient httpclient = new HttpClient();
                  /* Execute request */
                  try {
                    //  int result = httpclient.executeMethod(getMethod);
                     String response = httpUtil.send(url, parmMap); //getMethod.getResponseBodyAsString();
                      if(response!=null){
                          logger.debug("response: "+response);
                          if(response.indexOf("Successfully")==-1)
                          {
                            hasError=true;
                            //extrace error message
                            int start=response.indexOf("<pre>");
                            int end=response.indexOf("</pre>");
                            
                            if(start!=-1&&end!=-1)
                            {
                              String error=response.substring(start+5, end);
                              logger.debug("report error: "+error);
                              errorMsg.append(error);
                            }else
                            {
                              errorMsg.append("UNKNOWN ERROR");
                            }
                                               
                          }
                      }else
                      {
                        hasError=true;
                        errorMsg.append("NO RESPONSE FROM REORT SERVER");
                      }
                     
                   
                  } finally {
                      // Release current connection to the connection pool once you are done
                     // getMethod.releaseConnection();
                  }
              }
              
               if(hasError)
               {
                       //update submission log
                       reportDAO.updateSubmissionRequest(submissionId, "SYS", "E", errorMsg.toString());
                       logger.debug("send system message");
                       this.logError(null, errorMsg.toString(), this.dbConn, "REPORT SUBMISSION ERROR", "REPORT ERROR" );
                       
               }
              
                 
                 
    }
    
    
    
    
    
    
    
     public Document retrieveReportResultList(String securityToken, String startDate, String endDate) throws SAXException, ParserConfigurationException, IOException, 
               SQLException,  Exception
    {
              logger.debug("retrieveReportResultList between "+startDate+" and "+endDate);
              
              String userId=this.lookupCurrentUserId(securityToken); 
              AccountingUtil accountingUtil=new AccountingUtil();
             
              Date start=accountingUtil.formatDateStringToCal(startDate).getTime();
              Date end=accountingUtil.formatDateStringToCal(endDate).getTime();
              ReportDAO reportDAO=new ReportDAO(this.dbConn);
              Document doc= reportDAO.getReportResultList(userId, start, end);
              XMLUtil.addElementToXML("start_date", startDate, doc);
              XMLUtil.addElementToXML("end_date", endDate, doc);
              return doc;
                  
    }
    
    
    public Document creatReportViewerForm()throws SAXException, ParserConfigurationException, IOException, 
               SQLException,  Exception
    {
             Document doc=DOMUtil.getDocument();
             int range=this.getDefaultDateRange();
             Date end=new Date();
             Calendar c=Calendar.getInstance();
             c.setTime(end);
             int year=c.get(Calendar.YEAR);
             int month=c.get(Calendar.MONTH);
             int day=c.get(Calendar.DATE)-range;
             c.set(year, month, day);
             Date start=c.getTime();
             String endDate=REPORTING_DATE_FORMAT.format(end);
             String startDate=REPORTING_DATE_FORMAT.format(start);
             XMLUtil.addElementToXML("start_date", startDate, doc);
             XMLUtil.addElementToXML("end_date", endDate, doc);
             return doc; 
              
    }
        
    public InputStream retrieveGeneratedReport(String reportSubmissionId, String outputFormat)throws SAXException, ParserConfigurationException, IOException, 
               SQLException,  Exception
    {
           ReportDAO reportDAO=new ReportDAO(this.dbConn);     
           return reportDAO.getGeneratedReport(reportSubmissionId, outputFormat);      
                 
    }


    
    protected int getDefaultDateRange() throws IOException, SAXException, ParserConfigurationException, TransformerException,  Exception
    {
      String rangeStr=ConfigurationUtil.getInstance().getProperty(ARConstants.CONFIG_FILE,
                                                            "defaultViewReportDateRange");
                                                            
      return Integer.parseInt(rangeStr);
    }
    
 
    /**
     * This method is called twice a day. The non-EOD reports process will pass eodFlag of false
     * and EOD reports process will pass eodFlag of true. When eodReports is true, the EOD dependent reports
     * will run; if eodFlag is false, only EOD independent reports will run.
     * @param dateStr
     * @param eodReports
     * @return
     * @throws IOException
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws TransformerException
     * @throws XSLException
     * @throws Exception
     */
    public boolean processScheduledReports(String dateStr, boolean eodReports) throws IOException, SAXException, ParserConfigurationException, TransformerException,  Exception
    {
          long delay=0;
          int count=0;
          Date runDate=REPORTING_DATE_FORMAT.parse(dateStr);
          ReportDAO reportDAO=new ReportDAO(this.dbConn);
          HashMap resultMap=reportDAO.getScheduledReportList(runDate);
          CachedResultSet reportRS=(CachedResultSet)resultMap.get("OUT_CUR");
          String eodFlag=(String)resultMap.get("OUT_END_OF_DAY_DONE");
          boolean eodCompleted=(StringUtils.isNotBlank(eodFlag)&&eodFlag.trim().equalsIgnoreCase("Y"));
          String bypassEODCheck = ConfigurationUtil.getInstance().getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, "EOD_REPORT_BYPASS_EOD_CHECK");
        
          if (bypassEODCheck == null || bypassEODCheck.equals("")) {
                bypassEODCheck = "N";
          }
          // If eod reports are requested and eod has not completed, return false.
          if (bypassEODCheck.equals("N") && eodReports && !eodCompleted) {
                return false;
          }
          while(reportRS.next())
          {
                 ReportVO report=new ReportVO();
                 String eodReq=reportRS.getString("end_of_day_required");
                       
                 report.setEndOfDayRequired(StringUtils.isNotBlank(eodReq)&&eodReq.trim().equalsIgnoreCase("Y"));
                 report.setName(reportRS.getString("name"));
                 
                 // Submit EOD dependent reports when eodFlag is on. Submit EOD independent reports when eodFlag is off.
                 if((eodReports && report.isEndOfDayRequired()) || (!eodReports && !report.isEndOfDayRequired())){
                     delay=processScheduledReport(report, reportRS, reportDAO, delay, count);
                     count++;
                 }
              
          }            
          return true;
          
    }    
    
    private long processScheduledReport(ReportVO report, CachedResultSet reportRS, ReportDAO reportDAO, long delay, int index) throws IOException, SAXException, ParserConfigurationException, TransformerException,  Exception
    {
        long timeoffset=0;      
        populdateReportVO(report, reportRS);
        //create report name
        String oracleReportDesFileName=this.createOracleReportDesFileName(ARConstants.LOCK_CSR_ID, report);
        String oracleReportURL=this.createOrderReportURL(report, true);
        String submissionId=reportDAO.logSubmissionRequest(report, oracleReportDesFileName, oracleReportURL, ARConstants.LOCK_CSR_ID, "S");
        if(index!=0)
        {
               timeoffset=   delay +report.getRuntimeOffSet();
        }
        
        this.dispatchSubmissionRequest(submissionId, timeoffset);
        return timeoffset;
      
    }
    
    

  void populdateReportVO(ReportVO report, CachedResultSet reportRS)
  {
    report.setId(reportRS.getString("report_id"));
    report.setServerName(reportRS.getString("server_name"));
    report.setOracleRDFName(reportRS.getString("oracle_rdf"));
    report.setType(reportRS.getString("report_type"));
    report.setOutputType(reportRS.getString("report_output_type"));
    report.setRuntimeOffSet(reportRS.getLong("run_time_offset"));
    report.setFilePrefix(reportRS.getString("REPORT_FILE_PREFIX"));
    report.setScheduleParmValue(reportRS.getString("schedule_parm_value"));
  }
    
    
    /**
     * Send an error message via the system messenger 
     * @param errorMessage - String
     * @return n/a
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     * @throws SystemMessengerException
     */
    public void logError(Throwable exception, String errorMessage, Connection conn, String processName, String errorType) throws SAXException, 
        ParserConfigurationException, IOException, SQLException, 
        SystemMessengerException, TransformerException
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering logError");
        }
        
         try{
            
            /* log message to log4j log file */
            logger.error(errorMessage,exception);
            
            /* send a system message */
            SystemMessager sysMessager = new SystemMessager();
            sysMessager.send(errorMessage,exception,
                processName,SystemMessager.LEVEL_PRODUCTION,
                errorType,this.dbConn);
        
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting logError");
            } 
       } 
        
    }
    
  
}
