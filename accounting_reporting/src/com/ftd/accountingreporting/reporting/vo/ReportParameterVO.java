package com.ftd.accountingreporting.reporting.vo;
import java.util.ArrayList;
import java.util.List;

public class ReportParameterVO 
{
  
  private String id;
  private String displayName;
  private int sortOrder;
  private boolean required;
  private String type;
  private String oracleReportParameterName;
  private String validation;
  private List parameterValueList=new ArrayList();
  private boolean allowFutureDate;
  
  
  public ReportParameterVO()
  {
  }


  public void setId(String id)
  {
    this.id = id;
  }


  public String getId()
  {
    return id;
  }


  public void setDisplayName(String displayName)
  {
    this.displayName = displayName;
  }


  public String getDisplayName()
  {
    return displayName;
  }


  public void setSortOrder(int sortOrder)
  {
    this.sortOrder = sortOrder;
  }


  public int getSortOrder()
  {
    return sortOrder;
  }


  public void setRequired(boolean required)
  {
    this.required = required;
  }


  public boolean isRequired()
  {
    return required;
  }


  public void setType(String type)
  {
    this.type = type;
  }


  public String getType()
  {
    return type;
  }


  public void setOracleReportParameterName(String oracleReportParameterName)
  {
    this.oracleReportParameterName = oracleReportParameterName;
  }


  public String getOracleReportParameterName()
  {
    return oracleReportParameterName;
  }


  public void setValidation(String validation)
  {
    this.validation = validation;
  }


  public String getValidation()
  {
    return validation;
  }


  public void setParameterValueList(List parameterValueList)
  {
    this.parameterValueList = parameterValueList;
  }


  public List getParameterValueList()
  {
    return parameterValueList;
  }


  public void setAllowFutureDate(boolean allowFutureDate)
  {
    this.allowFutureDate = allowFutureDate;
  }


  public boolean isAllowFutureDate()
  {
    return allowFutureDate;
  }


  
}