package com.ftd.accountingreporting.reporting.vo;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.collections.SequencedHashMap;

public class ReportParameterValueVO 
{
  private String id;
  private String displayText;
  private int sortOrder;
  private String dbStatmentId;
  private String defaultFormValue;
  private Map optionMap=new SequencedHashMap();
  
  public ReportParameterValueVO()
  {
  }


  public void setId(String id)
  {
    this.id = id;
  }


  public String getId()
  {
    return id;
  }


  public void setDisplayText(String displayText)
  {
    this.displayText = displayText;
  }


  public String getDisplayText()
  {
    return displayText;
  }


  public void setSortOrder(int sortOrder)
  {
    this.sortOrder = sortOrder;
  }


  public int getSortOrder()
  {
    return sortOrder;
  }


  public void setDbStatmentId(String dbStatmentId)
  {
    this.dbStatmentId = dbStatmentId;
  }


  public String getDbStatmentId()
  {
    return dbStatmentId;
  }


  public void setDefaultFormValue(String defaultFormValue)
  {
    this.defaultFormValue = defaultFormValue;
  }


  public String getDefaultFormValue()
  {
    return defaultFormValue;
  }


  public void setOptionMap(Map optionMap)
  {
    this.optionMap = optionMap;
  }


  public Map getOptionMap()
  {
    return optionMap;
  }
}