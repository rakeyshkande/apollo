package com.ftd.accountingreporting.reporting.vo;
import com.ftd.accountingreporting.util.XMLUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReportVO 
{
  public ReportVO()
  {
  }
  
  private String id;
  private String name;
  private String description;
  private String filePrefix;
  private String type;
  private String outputType;
  private String categoryName;
  private String recourceId;
  private String serverName;
  private boolean holidayReport;
  private String status;
  /*oracle report definition file name */
  private String oracleRDFName;
  private int daysSpan;
  private String notes;
  private List parameterList=new ArrayList();
  private Map parameterValueMap=new HashMap();
  private List requiredParmList=new ArrayList();
  private boolean endOfDayRequired;
  private long runtimeOffSet;
  private String scheduleParmValue;
  


  public void setId(String id)
  {
    this.id = id;
  }


  public String getId()
  {
    return id;
  }


  public void setName(String name)
  {
    this.name = name;
  }


  public String getName()
  {
    return name;
  }


  public void setDescription(String description)
  {
    this.description = description;
  }


  public String getDescription()
  {
    return description;
  }


  public void setFilePrefix(String filePrefix)
  {
    this.filePrefix = filePrefix;
  }


  public String getFilePrefix()
  {
    return filePrefix;
  }


  public void setType(String type)
  {
    this.type = type;
  }


  public String getType()
  {
    return type;
  }


  public void setOutputType(String outputType)
  {
    this.outputType = outputType;
  }


  public String getOutputType()
  {
    return outputType;
  }


  public void setCategoryName(String categoryName)
  {
    this.categoryName = categoryName;
  }


  public String getCategoryName()
  {
    return categoryName;
  }


  public void setRecourceId(String recourceId)
  {
    this.recourceId = recourceId;
  }


  public String getRecourceId()
  {
    return recourceId;
  }


  public void setServerName(String serverName)
  {
    this.serverName = serverName;
  }


  public String getServerName()
  {
    return serverName;
  }


  public void setHolidayReport(boolean holidayReport)
  {
    this.holidayReport = holidayReport;
  }


  public boolean isHolidayReport()
  {
    return holidayReport;
  }


  public void setStatus(String status)
  {
    this.status = status;
  }


  public String getStatus()
  {
    return status;
  }


  public void setOracleRDFName(String oracleRDFName)
  {
    this.oracleRDFName = oracleRDFName;
  }


  public String getOracleRDFName()
  {
    return oracleRDFName;
  }


  public void setDaysSpan(int daysSpan)
  {
    this.daysSpan = daysSpan;
  }


  public int getDaysSpan()
  {
    return daysSpan;
  }


  public void setNotes(String notes)
  {
    this.notes = notes;
  }


  public String getNotes()
  {
    return notes;
  }


  public void setParameterList(List parameterList)
  {
    this.parameterList = parameterList;
  }


  public List getParameterList()
  {
    return parameterList;
  }
  
  
  public String toXML() throws Exception 
  {
    
    return XMLUtil.toXML(this);
  }


  public void setParameterValueMap(Map parameterValueMap)
  {
    this.parameterValueMap = parameterValueMap;
  }


  public Map getParameterValueMap()
  {
    return parameterValueMap;
  }


  public void setRequiredParmList(List requiredParmList)
  {
    this.requiredParmList = requiredParmList;
  }


  public List getRequiredParmList()
  {
    return requiredParmList;
  }


  public void setEndOfDayRequired(boolean endOfDayRequired)
  {
    this.endOfDayRequired = endOfDayRequired;
  }


  public boolean isEndOfDayRequired()
  {
    return endOfDayRequired;
  }


  public void setRuntimeOffSet(long runtimeOffSet)
  {
    this.runtimeOffSet = runtimeOffSet;
  }


  public long getRuntimeOffSet()
  {
    return runtimeOffSet;
  }


  public void setScheduleParmValue(String scheduleParmValue)
  {
    this.scheduleParmValue = scheduleParmValue;
  }


  public String getScheduleParmValue()
  {
    return scheduleParmValue;
  }


  
  
  
}