package com.ftd.accountingreporting.reporting.ejb;

import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.reporting.bo.ReportBO;
import com.ftd.accountingreporting.util.AccountingUtil;
import com.ftd.accountingreporting.util.LockUtil;
import com.ftd.accountingreporting.util.SystemMessager;
import com.ftd.eventhandling.events.EventHandler;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.exception.SystemMessengerException;
import com.ftd.osp.utilities.vo.MessageToken;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.text.ParseException;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;


public class ReportingScheduleEventHandler extends EventHandler 
{
  
  private Logger logger = 
        new Logger(this.getClass().getName());
  
  private static final String REPORT_SCHEDULER="REPORT_SCHEDULER";
  
  public ReportingScheduleEventHandler()
  {
  }
  
  
   public void invoke(Object payload) throws Throwable 
    {
        Connection conn = null;
        LockUtil lockUtil=null;
        boolean lockObtained=false;
        String sessionId=null;
        
        if(logger.isDebugEnabled()) {            
            logger.debug("***********************************************************");
            logger.debug("* Start Reporting Scheduler Processing");
            logger.debug("***********************************************************");
        }
        try{
            /* retrieve connection to database */
            conn = DataSourceUtil.getInstance().
                getConnection(ConfigurationUtil.getInstance().getProperty(
                ARConstants.CONFIG_FILE,ARConstants.DATASOURCE_NAME));
           //obtain lock
            lockUtil = new LockUtil(conn);
            sessionId=REPORT_SCHEDULER+this.hashCode()+new Date().getTime();
            // lock process 
            lockObtained = lockUtil.obtainLock(REPORT_SCHEDULER,
                REPORT_SCHEDULER,ARConstants.LOCK_CSR_ID,sessionId);
            if (lockObtained) {
                if(logger.isDebugEnabled()) {
                    logger.debug("obtained lock...");
                }
                
                //get date from payload.
                MessageToken token = (MessageToken)payload;
                String date = (String)token.getMessage();
                
                // Use yesterday if date is not found on payload.
                if(date == null || date.equals("")) {
                    Calendar cal = Calendar.getInstance();
                    cal.add(Calendar.DATE, -1); //yesterday
                    Date yesterday = cal.getTime();
                    SimpleDateFormat fmt = new SimpleDateFormat("MM/dd/yyyy");
                    date = fmt.format(yesterday);
                } 
                
                 logger.debug("start date = "+date);
                 ReportBO reportBO=new ReportBO(conn);
                 reportBO.processScheduledReports(date, false);

            }
           
           
           
            
        } catch (SQLException e) {
            // Do not rethrow. Force dequeue.
            logError(e,"", conn);            
        } catch (TransformerException e) {
            // Do not rethrow. Force dequeue.
            logError(e,"", conn);
        } catch (IOException e) {
            // Do not rethrow. Force dequeue.
            logError(e,"", conn);
        } catch (SAXException e) {
            // Do not rethrow. Force dequeue.
            logError(e,"", conn);
        } catch (ParserConfigurationException e) {
            // Do not rethrow. Force dequeue.
            logError(e,"", conn);
        } catch (ParseException e) {
            // Do not rethrow. Force dequeue.
            logError(e,"", conn);
        } catch (SystemMessengerException e) {
            // Do not rethrow. Force dequeue.
            logError(e,"", conn);          
        } catch (Exception e) {
            // Do not rethrow. Force dequeue.
            logError(e,"", conn);
        } catch (Throwable e) {
            // Do not rethrow. Force dequeue.
            logError(e,"", conn);
        } finally {
            
            
            /* Release process lock.  */
            if(lockUtil != null){
                if(lockObtained == true){
                    lockUtil.releaseLock(
                        REPORT_SCHEDULER,
                        REPORT_SCHEDULER, 
                        ARConstants.LOCK_CSR_ID, sessionId);
                }
            }
            
            
            if(conn != null) {
                conn.close();
            }
            if(logger.isDebugEnabled()){
                logger.debug("***********************************************************");
                logger.debug("* Reporting Scheduler Completed");
                logger.debug("***********************************************************");
            } 
        }
    }
    
    
    
    /**
     * Send an error message via the system messenger 
     * @param errorMessage - String
     * @return n/a
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     * @throws SystemMessengerException
     */
    private void logError(Throwable exception, String errorMessage, Connection conn) throws SAXException, 
        ParserConfigurationException, IOException, SQLException, 
        SystemMessengerException, TransformerException
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering logError");
        }
        
         try{
            
            /* log message to log4j log file */
            logger.error(errorMessage,exception);
            
            /* send a system message */
            SystemMessager sysMessager = new SystemMessager();
            sysMessager.send(errorMessage,exception,
                "Run Scheduled Report Process ",SystemMessager.LEVEL_DEBUG,
                ARConstants.AAFES_BILLING_PROCESS_ERROR_TYPE,conn);
        
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting logError");
            } 
       } 
        
    }
  
  
  
}