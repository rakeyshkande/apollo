package com.ftd.accountingreporting.reporting.ejb;

import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.reporting.bo.ReportBO;
import com.ftd.accountingreporting.util.AccountingUtil;
import com.ftd.accountingreporting.util.LockUtil;
import com.ftd.accountingreporting.util.SystemMessager;
import com.ftd.eventhandling.events.EventHandler;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.exception.SystemMessengerException;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.xml.DOMUtil;

import com.ftd.osp.utilities.xml.JAXPUtil;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.text.ParseException;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;

import org.apache.xpath.XPathAPI;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import org.w3c.dom.Text;

import org.xml.sax.SAXException;

public class ReportingEODReportEventHandler extends EventHandler 
{
  
  private Logger logger = 
        new Logger(this.getClass().getName());
  
  private static final String EOD_REPORT_SCHEDULER="EOD_REPORT_SCHEDULER";
  
  public ReportingEODReportEventHandler()
  {
  }
  
  
   public void invoke(Object payload) throws Throwable 
    {
        Connection conn = null;
        LockUtil lockUtil=null;
        boolean lockObtained=false;
        String sessionId=null;
        String date = null;
        boolean success = false;
        String delay = null;
        String deadlineHour = null;
        ConfigurationUtil configUtil = null;
        
        if(logger.isDebugEnabled()) {            
            logger.debug("***********************************************************");
            logger.debug("* Start Reporting Reprocessing");
            logger.debug("***********************************************************");
        }
        try{
            /* retrieve connection to database */
            configUtil = ConfigurationUtil.getInstance();
            conn = DataSourceUtil.getInstance().
                getConnection(configUtil.getProperty(
                ARConstants.CONFIG_FILE,ARConstants.DATASOURCE_NAME));
           //obtain lock
            lockUtil = new LockUtil(conn);
            
            sessionId=EOD_REPORT_SCHEDULER+this.hashCode()+new Date().getTime();
            // lock process 
            lockObtained = lockUtil.obtainLock(EOD_REPORT_SCHEDULER,
                EOD_REPORT_SCHEDULER,ARConstants.LOCK_CSR_ID,sessionId);
            if (lockObtained) {
                if(logger.isDebugEnabled()) {
                    logger.debug("obtained lock...");
                }
                
                //get date from payload.
                MessageToken token = (MessageToken)payload;
                date = (String)token.getMessage();
                
                // Use yesterday if date is not found on payload.
                if(date == null || date.equals("")) {
                    Calendar cal = Calendar.getInstance();
                    cal.add(Calendar.DATE, -1); //yesterday
                    Date yesterday = cal.getTime();
                    SimpleDateFormat fmt = new SimpleDateFormat("MM/dd/yyyy");
                    date = fmt.format(yesterday);
                } 
                
                 logger.debug("start date = "+date);
                 ReportBO reportBO=new ReportBO(conn);
                 success  = reportBO.processScheduledReports(date, true);
                 
                 //If EOD has not completed, retry.
                 if (!success) {
                    // check if EOD dealine has passed. If so Page.
                    deadlineHour = configUtil.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, "EOD_DEADLINE_HOUR");
                    if (deadlineHour == null || deadlineHour.equals("")) {
                        deadlineHour = "600";
                    }
                    Calendar rightNow = Calendar.getInstance();
                    
                    int currentTime = 
                         rightNow.get(Calendar.HOUR_OF_DAY) * 100 + rightNow.get(Calendar.MINUTE);
                       
                    if (currentTime > Integer.parseInt(deadlineHour)) {
                         this.logError(null, "End-of-Day has not completed within expected time. Current time is:" + currentTime
                             + ". Deadline time is:" + deadlineHour + ". End-of-Day reports process will continue to retry.", conn);
                         
                    }
                    logger.debug("Current time is :" + currentTime + "; deadline is:" + deadlineHour);
                    delay = configUtil.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, "EOD_REPORT_RETRY_DELAY_SECONDS");
                    
                    if(delay == null || delay.equals("")) {
                        delay = "900";
                    }
                    
                    // retry
                    AccountingUtil.queueToEventsQueue(ARConstants.EVENT_CONTEXT, "SCHEDULED_EOD_REPORT_PROCESS", delay, null);
                 } else {
                    logger.info("End-of-Day has completed. End-of-Day reports have been submitted.");
                 }
            }
            
        } catch (SQLException e) {
            // Do not rethrow. Force dequeue.
            logError(e,"", conn);            
        } catch (TransformerException e) {
            // Do not rethrow. Force dequeue.
            logError(e,"", conn);
        } catch (IOException e) {
            // Do not rethrow. Force dequeue.
            logError(e,"", conn);
        } catch (SAXException e) {
            // Do not rethrow. Force dequeue.
            logError(e,"", conn);
        } catch (ParserConfigurationException e) {
            // Do not rethrow. Force dequeue.
            logError(e,"", conn);
        } catch (ParseException e) {
            // Do not rethrow. Force dequeue.
            logError(e,"", conn);
        } catch (SystemMessengerException e) {
            // Do not rethrow. Force dequeue.
            logError(e,"", conn);          
        } catch (Exception e) {
            // Do not rethrow. Force dequeue.
            logError(e,"", conn);
        } catch (Throwable e) {
            // Do not rethrow. Force dequeue.
            logError(e,"", conn);
        } finally {
            
            
            /* Release process lock.  */
            if(lockUtil != null){
                if(lockObtained == true){
                    lockUtil.releaseLock(
                        EOD_REPORT_SCHEDULER,
                        EOD_REPORT_SCHEDULER, 
                        ARConstants.LOCK_CSR_ID, sessionId);
                }
            }
            
            
            if(conn != null) {
                conn.close();
            }
            if(logger.isDebugEnabled()){
                logger.debug("***********************************************************");
                logger.debug("* Reporting Scheduler Completed");
                logger.debug("***********************************************************");
            } 
        }
    }
    
    
    
    /**
     * Send an error message via the system messenger 
     * @param errorMessage - String
     * @return n/a
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     * @throws SystemMessengerException
     */
    private void logError(Throwable exception, String errorMessage, Connection conn) throws SAXException, 
        ParserConfigurationException, IOException, SQLException, 
        SystemMessengerException, TransformerException
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering logError");
        }
        
         try{
            
            /* log message to log4j log file */
            logger.error(errorMessage,exception);
            
            /* send a system message */
            SystemMessager sysMessager = new SystemMessager();
            sysMessager.send(errorMessage,exception,
                "Run Scheduled Report Process ",SystemMessager.LEVEL_DEBUG,
                "EOD Reports",conn);
        
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting logError");
            } 
       } 
        
    }
  
  
  
}
