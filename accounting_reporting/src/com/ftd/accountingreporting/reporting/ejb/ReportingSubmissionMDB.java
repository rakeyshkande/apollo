package com.ftd.accountingreporting.reporting.ejb;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.reporting.bo.ReportBO;
import com.ftd.accountingreporting.util.SystemMessager;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.MessageUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.vo.MessageToken;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.xml.sax.SAXException;

public class ReportingSubmissionMDB implements MessageDrivenBean, MessageListener 
{
          
  private static final String ERROR_MESSAGE="Reporting Error. Unable to process report. Submissin ID=";
  private static Logger logger;
  private MessageDrivenContext context;
  
  public void ejbCreate()
    {
    }

    private void processMessage(Message msg) throws Exception
    {
        
        if(logger.isDebugEnabled()){
            logger.debug("Entering processMessage");
            logger.debug("Message : " + msg.toString());
        }
        
        Connection conn=null;
        boolean success=false;
        String error="";
            try
            {
                /*retrieve message token from message */
                TextMessage textMessage = (TextMessage)msg;
                /*retrieve submission id from message token */
                String submissionId = textMessage.getText();
                logger.debug("submissionId="+submissionId);
                error +=ERROR_MESSAGE+submissionId;
                /* Call EmailQueueProcessBO to process message */
                conn = createDatabaseConnection();
                ReportBO reportBO = new ReportBO(conn);
                reportBO.executeReport(submissionId);
                success=true;
            }catch(ParserConfigurationException pceException){
                logger.error(pceException);
                logError(error,pceException, conn);
            }catch(SAXException saxException){
                logger.error(saxException);
                logError(ERROR_MESSAGE,saxException, conn);            
            }catch(SQLException sqlException){
                logger.error(sqlException);
                logError(error,sqlException, conn);
            }catch(TransformerException transException){
                logger.error(transException);
                logError(error,transException, conn);
            }catch(JMSException jmsException){
                logger.error(jmsException);
                logError(error,jmsException, conn);
            }catch(IOException ioException){
                logger.error(ioException);
                logError(error,ioException, conn);
            }catch(ClassNotFoundException cnfException){
                logger.error(cnfException);
                logError(error,cnfException, conn);
            }catch(InstantiationException instException){
                logger.error(instException);
                logError(error,instException, conn);
            }catch(IllegalAccessException iaException){
                logger.error(iaException);
                logError(error,iaException, conn);
            }catch(ParseException parseException){
                logger.error(parseException);
                logError(error,parseException, conn);
            }catch(Throwable e){
                logger.error(e);
                logError(error,e, conn);
                 
        }finally
        {
           
            if(conn!=null){
                conn.close();    
            }
        
            if(logger.isDebugEnabled()){
            logger.debug("Exiting processMessage");
            }
        }       
    }
   /**
     * Process JMS messages, extract Email Request XML from inbound messages, 
     * calls EmailQueueProcessorBO to process email requests.
     * @param msg - JMS Message
     * @return n/a
     * @throws n/a
     */
    public void onMessage(Message msg)
    {
        logger = 
        new Logger("com.ftd.accountingreporting.reporting.ejb.ReportingSubmissionMDB");
        if(logger.isDebugEnabled()){
            logger.debug("Entering onMessage of Reporting Submission Message Driven Bean");
           
        }
        
        try
        {
            /* process JMS Message */
           logger.debug("Message : " + msg.getIntProperty("JMS_OracleDelay"));
            processMessage(msg);
            
        }catch(Exception e){
             logger.error(e);
        }finally
        {
           if(logger.isDebugEnabled()){
            logger.debug("Exiting onMessageof ReportingSubmissionMDB Message Driven Bean");
            }
        }    

    }

    public void ejbRemove()
    {
    }

    public void setMessageDrivenContext(MessageDrivenContext ctx)
    {
        this.context = ctx;
    }

  
   
    /**
     * Create a connection to the database
     * @param n/a
     * @return java.sql.Connection
     * @throws Exception
     */
    private Connection createDatabaseConnection() 
        throws IOException, SAXException, ParserConfigurationException, 
            Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering createDatabaseConnection");
        }

        Connection connection=null;
        try{
            /* create a connection to the database */
            connection = DataSourceUtil.getInstance().
                getConnection(ConfigurationUtil.getInstance().getProperty(
                ARConstants.CONFIG_FILE,ARConstants.DATASOURCE_NAME));
                
        }finally{
            if(logger.isDebugEnabled()){
               logger.debug("Exiting createDatabaseConnection");
            }
        }
        
        return connection;
    }
    
    private void logError(String message,Throwable excep, Connection con) throws IOException,
        SAXException,ParserConfigurationException,Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering logError");
        }
       
        try
        {
            
             /* send a system message */
            SystemMessager sysMessager = new SystemMessager();
            sysMessager.send(message,excep,
                "REPORT PROCESS",SystemMessager.LEVEL_PRODUCTION,
                "REPORT PROCESS ERROR",con);
           
            logger.error(message, excep);
        }finally
        {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting logError");
            }
        }    
    }
    
    private void closeConnection(Connection conn) throws SQLException, Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering closeConnection");
        }

        try
        {
            if(conn!=null){
                conn.close();
            }

        }finally
        {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting closeConnection");
            }
        }    
    }
}