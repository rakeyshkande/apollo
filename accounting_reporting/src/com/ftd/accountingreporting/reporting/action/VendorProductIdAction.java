package com.ftd.accountingreporting.reporting.action;
import com.ftd.accountingreporting.reporting.dao.TemplateDAO;
import com.ftd.accountingreporting.util.XMLUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import java.io.IOException;
import java.sql.Connection;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;

public class VendorProductIdAction extends ReportBaseAction 
{
  private Logger logger = 
        new Logger("com.ftd.accountingreporting.reporting.action.VendorProductIdAction");
  
  public VendorProductIdAction()
  {
  }
  
  
   /**
   * It is used to validate product id entered in the Product ID field and will add the product id to the Product ID Display List.  
   * Validation checks to see if the product id exists in Product DB and if the product is vendor delivered.
   * @param mapping
   * @param form
   * @param request
   * @param response
   * @return 
   * @throws java.io.IOException
   * @throws javax.servlet.ServletException
   */
   public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException{
    
        Connection con=null;
        String productId=request.getParameter("product_id");
        try
        {
          con=this.createDatabaseConnection();
          TemplateDAO templateDAO=new TemplateDAO(con);
          String indicator=templateDAO.validateVendorProductId(productId);
          Document doc=DOMUtil.getDocument();
          //add product id and indicator to the Document
          if(StringUtils.isNotEmpty(productId)){
             XMLUtil.addElementToXML("product_id", productId, doc);
          }
          XMLUtil.addElementToXML("indicator", indicator, doc);
          this.doForward("validation-iframe", doc, mapping, request, response);
         
        }catch(Throwable t)
        {
          logger.error(t);
        }finally
        {
          this.closeConnection(con);
        }
        
         return null;
    }
  
  
}