package com.ftd.accountingreporting.reporting.action;
import com.ftd.accountingreporting.reporting.dao.TemplateDAO;
import com.ftd.accountingreporting.util.XMLUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import java.io.IOException;
import java.sql.Connection;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;
import org.w3c.dom.Document;
public class SourceCodeAction extends ReportBaseAction 
{
   private Logger logger = 
        new Logger("com.ftd.accountingreporting.reporting.action.SourceCodeAction");
  public SourceCodeAction()
  {
  }
  
  
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException{
    
        Connection con=null;
        String sourceCode=request.getParameter("source_code");
        String action=mapping.getParameter();
        logger.debug("action="+action);
        try
        {
                con=this.createDatabaseConnection();
                TemplateDAO templateDAO=new TemplateDAO(con);
                Document doc=null;
                if(action.equalsIgnoreCase("add")){
                                        
                    String indicator=templateDAO.validateSourceCode(sourceCode);
                    doc=DOMUtil.getDocument();
                    
                        if(StringUtils.isNotBlank(sourceCode)){
                          XMLUtil.addElementToXML("source_code", sourceCode, doc);
                        }
                    XMLUtil.addElementToXML("indicator", indicator, doc);
                    this.doForward("source-code-iframe", doc, mapping, request, response);
                }else
                {
                  String startStr=request.getParameter("start_page");
                  int startNumber=0;
                  int maxNumber=this.getMaxNumberPerPage();
                  if(StringUtils.isNumericSpace(startStr))
                  {
                    startNumber=(Integer.parseInt(startStr.trim())-1)*maxNumber;
                   
                  }
                  
                  doc=templateDAO.lookupSourceCodes(sourceCode,startNumber ,maxNumber );
                  setPaginationInfo(doc, startStr, maxNumber);
                  if(StringUtils.isNotBlank(sourceCode)){
                          XMLUtil.addElementToXML("source_code_lookup", sourceCode, doc);
                  }
                  this.doForward("source-code-lookup", doc, mapping, request, response);
                }
         
        }catch(Throwable t)
        {
          logger.error(t);
        }finally
        {
          this.closeConnection(con);
        }
        
         return null;
    }
  
  
}
