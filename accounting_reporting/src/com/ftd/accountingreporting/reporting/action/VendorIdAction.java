package com.ftd.accountingreporting.reporting.action;
import com.ftd.accountingreporting.reporting.dao.TemplateDAO;
import com.ftd.accountingreporting.util.XMLUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import java.io.IOException;
import java.sql.Connection;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.w3c.dom.Document;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;

public class VendorIdAction extends ReportBaseAction 
{
  private Logger logger = 
        new Logger("com.ftd.accountingreporting.reporting.action.VendorIdAction");
  public VendorIdAction()
  {
  }
  
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException{
    
        Connection con=null;
        String vendorId=request.getParameter("vendor_id");
        String action=mapping.getParameter();
        logger.debug("action="+action);
        try
        {
                con=this.createDatabaseConnection();
                TemplateDAO templateDAO=new TemplateDAO(con);
                Document doc=null;
                if(action.equalsIgnoreCase("add")){
                    
                    String indicator=templateDAO.validateVendorId(vendorId);
                    doc=DOMUtil.getDocument();
                    //add product id and indicator to the Document
                    if(StringUtils.isNotEmpty(vendorId)){
                       XMLUtil.addElementToXML("vendor_id", vendorId, doc);
                    }
                    XMLUtil.addElementToXML("indicator", indicator, doc);
                    this.doForward("vendor-id-iframe", doc, mapping, request, response);
                }else
                {
                  String startStr=request.getParameter("start_page");
                  int startNumber=0;
                  int maxNumber=this.getMaxNumberPerPage();
                   if(StringUtils.isNumericSpace(startStr))
                  {
                    startNumber=(Integer.parseInt(startStr.trim())-1)*maxNumber;
                   
                  }
                  doc=templateDAO.lookupVendorIds(vendorId,startNumber ,maxNumber );
                  setPaginationInfo(doc, startStr, maxNumber);
                  if(StringUtils.isNotBlank(vendorId)){
                          XMLUtil.addElementToXML("vendor_id_lookup", vendorId, doc);
                  }
                  this.doForward("vendor-id-lookup", doc, mapping, request, response);
                }
         
        }catch(Throwable t)
        {
          logger.error(t);
        }finally
        {
          this.closeConnection(con);
        }
        
         return null;
    }
  
}
