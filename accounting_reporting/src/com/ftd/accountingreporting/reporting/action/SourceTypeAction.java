package com.ftd.accountingreporting.reporting.action;
import com.ftd.accountingreporting.reporting.dao.TemplateDAO;
import com.ftd.accountingreporting.util.XMLUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import java.io.IOException;
import java.sql.Connection;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.w3c.dom.Document;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;

public class SourceTypeAction extends ReportBaseAction 
{
  
   private Logger logger = 
        new Logger("com.ftd.accountingreporting.reporting.action.SourceTypeAction");
  public SourceTypeAction()
  {
  }
  
  
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException{
    
        Connection con=null;
        String sourceType=request.getParameter("source_type");
        String action=mapping.getParameter();
        logger.debug("action="+action);
        try
        {
                con=this.createDatabaseConnection();
                TemplateDAO templateDAO=new TemplateDAO(con);
                Document doc=null;
                if(action.equalsIgnoreCase("add")){
                    
                    String indicator=templateDAO.validateSourceType(sourceType);
                    doc=DOMUtil.getDocument();
                    //add product id and indicator to the Document
                    if(StringUtils.isNotEmpty(sourceType)){
                       XMLUtil.addElementToXML("source_type", sourceType, doc);
                    }
                    XMLUtil.addElementToXML("indicator", indicator, doc);
                    this.doForward("source-type-iframe", doc, mapping, request, response);
                }else
                {
                  String startStr=request.getParameter("start_page");
                  int startNumber=0;
                  int maxNumber=this.getMaxNumberPerPage();
                   if(StringUtils.isNumericSpace(startStr))
                  {
                    startNumber=(Integer.parseInt(startStr.trim())-1)*maxNumber;
                   
                  }
                  doc=templateDAO.lookupSourceTypes(sourceType,startNumber ,maxNumber );
                  setPaginationInfo(doc, startStr, maxNumber);
                  if(StringUtils.isNotBlank(sourceType)){
                          XMLUtil.addElementToXML("source_type_lookup", sourceType, doc);
                  }
                  this.doForward("source-type-lookup", doc, mapping, request, response);
                }
         
        }catch(Throwable t)
        {
          logger.error(t);
        }finally
        {
          this.closeConnection(con);
        }
        
         return null;
    }
  
  
}
