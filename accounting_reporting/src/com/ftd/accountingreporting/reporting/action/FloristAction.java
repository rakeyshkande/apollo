package com.ftd.accountingreporting.reporting.action;


import com.ftd.accountingreporting.reporting.dao.TemplateDAO;
import com.ftd.accountingreporting.util.XMLUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import java.io.IOException;
import java.sql.Connection;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;
import org.w3c.dom.Document;

public class FloristAction extends ReportBaseAction 
{
  private Logger logger = 
        new Logger(this.getClass().getName());
  
  public FloristAction()
  {
  }
  
  
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException{
    
        Connection con=null;
        String action=mapping.getParameter();
        logger.debug("action="+action);
        try
        {
                con=this.createDatabaseConnection();
                TemplateDAO templateDAO=new TemplateDAO(con);
                Document doc=null;
                if(action.equalsIgnoreCase("validate")){
                    String option = request.getParameter("p_option");
                    String data = request.getParameter("p_data");
                    String errorCount = request.getParameter("error_count");
                    
                    logger.debug("option is : " + option);
                    logger.debug("data/florist id is : " + data);
                    logger.debug("error count is : " + errorCount);
                                        
                    String indicator=templateDAO.validateFlorist(option, data);
                    String simpleValidation = "Y";
                    doc=DOMUtil.getDocument();
                    XMLUtil.addElementToXML("florist_id", data, doc);
                    XMLUtil.addElementToXML("indicator", indicator, doc);

                    XMLUtil.addElementToXML("error_count", errorCount, doc);
                    this.doForward("validation-iframe", doc, mapping, request, response);
                }else
                {
                    doc=DOMUtil.getDocument();
                    
                    XMLUtil.addElementToXML("root", "invalid request", doc);
                    this.doForward("error", doc, mapping, request, response);
                }
         
        }catch(Throwable t)
        {
          logger.error(t);
        }finally
        {
          this.closeConnection(con);
        }
        
         return null;
    }

  
  
}
