package com.ftd.accountingreporting.reporting.action;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.reporting.bo.ReportBO;
import com.ftd.accountingreporting.reporting.dao.ReportDAO;
import com.ftd.accountingreporting.util.XMLUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;

public class ReportSubmissionListAction extends ReportBaseAction 
{
  
   private Logger logger = 
        new Logger("com.ftd.accountingreporting.reporting.action.ReportSubmissionListAction");
  
  
  public ReportSubmissionListAction()
  {
  }
  
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException{
    
        Connection con=null;
        String securityToken=request.getParameter(ARConstants.COMMON_PARM_SEC_TOKEN);
        
        try
        {
                con=this.createDatabaseConnection();
                Document doc=null;
                
                ReportBO reportBO=new ReportBO(con);
                Document root=reportBO.retrieveReportSubmissionList(securityToken);
                File resultXSL=this.getXSL("merge-result-list", mapping);
                String mergedXML=TraxUtil.getInstance().transform(root, resultXSL, null);
                Document mergedDoc=DOMUtil.getDocument(mergedXML);
                this.doForward("submission-list", mergedDoc, mapping, request, response);
         
        }catch(Throwable t)
        {
          logger.error(t);
        }finally
        {
          this.closeConnection(con);
        }
        
         return null;
    }
  
}