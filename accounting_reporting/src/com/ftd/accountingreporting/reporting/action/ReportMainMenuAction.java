package com.ftd.accountingreporting.reporting.action;



import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.reporting.bo.ReportBO;
import com.ftd.accountingreporting.reporting.dao.ReportDAO;
import com.ftd.accountingreporting.util.XMLUtil;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;


public class ReportMainMenuAction extends ReportBaseAction 
{
  
   private Logger logger = 
        new Logger(this.getClass().getName());
  
  public ReportMainMenuAction()
  {
  }
  
  
  
   public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException{
    
       String menuUrl = null;
       String securityToken = request.getParameter(ARConstants.COMMON_PARM_SEC_TOKEN);
       String context = request.getParameter(ARConstants.COMMON_PARM_CONTEXT);
       String mainMenu = request.getParameter(ARConstants.REPORT_MAIN_MENU);
       
       try {

           
           menuUrl = mapping.findForward(mainMenu).getPath();

           ConfigurationUtil cu = ConfigurationUtil.getInstance();
           String baseUrl = cu.getFrpGlobalParm(ARConstants.BASE_CONFIG,ARConstants.BASE_URL);
          
           menuUrl = baseUrl + menuUrl + "&" + ARConstants.COMMON_PARM_SEC_TOKEN + "=" + securityToken + "&" + ARConstants.COMMON_PARM_CONTEXT + "=" + context;
           logger.debug("exiting to " + menuUrl);
           logger.debug("context=" + context);
       } catch (Exception e) {
           return mapping.findForward(ARConstants.ACTION_ERROR);
       }
       return new ActionForward(menuUrl, true);
      
    }
  
  
  
  
  
}