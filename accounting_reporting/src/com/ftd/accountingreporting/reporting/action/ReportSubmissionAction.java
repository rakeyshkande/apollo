package com.ftd.accountingreporting.reporting.action;

import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.reporting.bo.ReportBO;
import com.ftd.accountingreporting.reporting.dao.ReportDAO;
import com.ftd.accountingreporting.util.XMLUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;

public class ReportSubmissionAction extends ReportBaseAction 
{
   private Logger logger = 
        new Logger("com.ftd.accountingreporting.reporting.action.ReportSubmissionAction");
  
  public ReportSubmissionAction()
  {
  }
  
  
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException{
    
        Connection con=null;
                
        try
        {
                con=this.createDatabaseConnection();
               
                ReportBO reportBO=new ReportBO(con);
                Document root=reportBO.processSubmissionRequest(request);
               
                this.doForward("success", root, mapping, request, response);
         
        }catch(Throwable t)
        {
          logger.error(t);
        }finally
        {
          this.closeConnection(con);
        }
        
         return null;
    }
  
  
  
}