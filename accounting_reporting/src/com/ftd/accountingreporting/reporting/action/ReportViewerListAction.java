package com.ftd.accountingreporting.reporting.action;


import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.reporting.bo.ReportBO;
import com.ftd.accountingreporting.reporting.dao.ReportDAO;
import com.ftd.accountingreporting.util.XMLUtil;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;


public class ReportViewerListAction extends ReportBaseAction 
{
  
   private Logger logger = 
        new Logger("com.ftd.accountingreporting.reporting.action.ReportViewerListAction");
  
  public ReportViewerListAction()
  {
  }
  
  
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException{
    
        Connection con=null;
        Document doc=null;
        String action=mapping.getParameter();
        if(StringUtils.isBlank(action))
        {
          action="view-list-form";
        }
        try
        {
            
            if(action.equalsIgnoreCase("view-list-form"))
            {
              ReportBO reportBO=new ReportBO();
              doc=reportBO.creatReportViewerForm();
              
            }else
            {
              String securityToken=request.getParameter(ARConstants.COMMON_PARM_SEC_TOKEN);
              String startDate=request.getParameter("start_date");
              String endDate=request.getParameter("end_date");
              con=this.createDatabaseConnection();
              ReportBO reportBO=new ReportBO(con);
              doc=reportBO.retrieveReportResultList(securityToken, startDate, endDate);
            }
            
            
             this.doForward(action, doc, mapping, request, response);   
               
         
        }catch(Throwable t)
        {
          logger.error(t);
        }finally
        {
          this.closeConnection(con);
        }
        
         return null;
    }
  
  
    
    
  
}