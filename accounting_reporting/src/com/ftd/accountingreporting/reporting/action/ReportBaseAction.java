package com.ftd.accountingreporting.reporting.action;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.util.XMLUtil;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.w3c.dom.Document;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.xml.sax.SAXException;

public class ReportBaseAction extends Action 
{
  private Logger logger = 
        new Logger("com.ftd.accountingreporting.reporting.action.ReportBaseAction");
  
  public ReportBaseAction()
  {
  }
  
  
  /**
     * Return appropriate xsl stylesheet. The XSL file is configured in 
     * struts-config.xml as an action forward
     * String xslFileName=actionMapping.findForward(forwardName).getPath();
     * File xslFile=this.servlet.getServletContext().getRealPath(xslFileName);
     * @param forwardName - String
     * @param actionMapping - ActionMapping
     * @return File - xsl stylesheet
     * @throws Exception????
     * @todo - code and determine exceptions
     */
    protected File getXSL(String forwardName, ActionMapping actionMapping) 
        throws Exception{
        
        if(logger.isDebugEnabled()){
            logger.debug("Entering getXSL");
            logger.debug("forwardName " + forwardName);
            logger.debug("actionMapping " + actionMapping.toString());
        }    
        
        File xslFile = null;
        
        try{
            String xslFileName=actionMapping.findForward(forwardName).getPath();
            xslFile=new File(this.servlet.getServletContext().getRealPath(xslFileName));
        
        }finally{
            if(logger.isDebugEnabled()){
                logger.debug("Exiting getXSL");
            }
        }
        return xslFile;

    }

    /**
     * Call the transform method in the com.ftd.osp.utilities.xml.TraxUtil 
     * class, pass over a Document object which will contain all of the 
     * message/order related data and security data and the name of the 
     * XSL page to be rendered.
     * @param doc - Document
     * @param xslFile - File
     * @param request - HttpServletRequest
     * @param response - HttpServletResponse
     * @return n/a
     * @throws TransformerException
     * @throws IOException
     * @throws Exception
     * @todo - code and determine exceptions
     */
    protected void transform(Document doc, File xslFile, String xslFilePathAndName,
        HttpServletRequest request, HttpServletResponse response, 
        HashMap parameters)
        throws TransformerException, IOException, Exception
        {
            if(logger.isDebugEnabled()){
                logger.debug("Entering transform");
                logger.debug("doc " + doc);
                logger.debug("xslFile " + xslFile);
                logger.debug("request " + request);
                logger.debug("response " + response);
            } 
            try{
               
                if(logger.isDebugEnabled()){
                    Document xml=(Document)doc;
                    StringWriter sw = new StringWriter(); 
                    DOMUtil.print(xml, new PrintWriter(sw));
                    
                    logger.debug("Reporting XML= \n" + sw.toString());
                }
                
               
                
                TraxUtil.getInstance().getInstance().transform(request, response, 
                    doc, xslFile, parameters);
            }finally{
                if(logger.isDebugEnabled()){
                    logger.debug("Exiting transform");
                } 
            }
        }

    /**
     * The main processing method that will be called by its subclasses.
     * @param forwardName - String
     * @param doc - Document
     * @param actionMapping - ActionMapping
     * @param request - HttpServletRequest
     * @return n/a
     * @throws TransformerException
     * @throws IOException
     * @throws Exception
     * @todo - code and determine exceptions
     */
    public void doForward(String forwardName, Document doc, 
        ActionMapping actionMapping, HttpServletRequest request, 
        HttpServletResponse response) 
        throws TransformerException, IOException, Exception
        {

            HashMap parameters=this.getRequestDataMap(request);
            if(logger.isDebugEnabled()){
                logger.debug("Entering doForward");
                logger.debug("forwardName " + forwardName);
                logger.debug("doc " + doc);
                logger.debug("actionMapping " + actionMapping);
                logger.debug("request " + request);
                logger.debug("response " + response);
                logger.debug("parameters " + parameters);
            } 
            try{
                /* Call the getXSL method to obtain appropriate xsl stylesheet. */
                ActionForward forward = actionMapping.findForward(forwardName);
                String xslFilePathAndName = forward.getPath(); //get real file name
            	
                 File xslFile = getXSL(forwardName,actionMapping);
                /* Call the transform method to render UI page. */
                if(parameters==null)
                {
                  transform(doc,xslFile,xslFilePathAndName,request,response,null);
                }else{
                    if(parameters.isEmpty())
                    {
                        transform(doc,xslFile,xslFilePathAndName,request,response,null);    
                    }
                    else
                    {
                        transform(doc,xslFile,xslFilePathAndName,request,response,parameters);    
                    }
                  
                }
                
            }finally{
                if(logger.isDebugEnabled()){
                    logger.debug("Exiting doForward");
                } 
            }
        }
    
    
    /**
     * Create a connection to the database
     * @param n/a
     * @return Connection
     * @throws Exception
     */
    protected Connection createDatabaseConnection() 
        throws IOException, SAXException, ParserConfigurationException, 
        TransformerException,  Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering createDatabaseConnection");
        }

        Connection connection = null;
        try{
            /* create a connection to the database */
            connection = DataSourceUtil.getInstance().
                getConnection(ConfigurationUtil.getInstance().getProperty(ARConstants.CONFIG_FILE,
                                                            ARConstants.DATASOURCE_NAME));

        }finally{
            if(logger.isDebugEnabled()){
               logger.debug("Exiting createDatabaseConnection");
            }
        }
        
        return connection;
    }
    
    
    
    
    
    /**
   * Utilize com.ftd.osp.utilities.ConfigurationUtil to retrieve max number per page from accountingreporting-config.xml
   * @return 
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws javax.xml.transform.TransformerException
   * @throws java.lang.Exception
   */
    protected int getMaxNumberPerPage() throws IOException, SAXException, ParserConfigurationException, TransformerException,  Exception
    {
      String maxStr=ConfigurationUtil.getInstance().getProperty(ARConstants.CONFIG_FILE,
                                                            "MaxNumberPerPage");
                                                            
      return Integer.parseInt(maxStr);
    }
    
    
     public HashMap getRequestDataMap(HttpServletRequest request)
    {
      HashMap dataMap=(HashMap)request.getAttribute("parameters");
      if(dataMap==null)
      {
        dataMap=new HashMap();
      }
      return dataMap;
    }
  
  
  
    public void closeConnection(Connection con)
    {
          try
          {
            if(con!=null)
            {
              con.close();
            }
          }catch(Exception e)
          {
            logger.fatal(e);
          }
    }
    
    
    
  public  void setPaginationInfo(Document doc, String startStr, int maxNumber)
  {
    XMLUtil.addElementToXML("start_page", startStr, doc);
    String total = DOMUtil.getNodeValue((Document)doc, "total-number");
    logger.debug("total number=" + total);
    int totalNum = Integer.parseInt(total);
    int totalPage = new Double(Math.ceil(totalNum / maxNumber)).intValue() + 1;
    XMLUtil.addElementToXML("total_page", "" + totalPage, doc);
  }
  
}
