package com.ftd.accountingreporting.reporting.dao;
import com.ftd.accountingreporting.util.XMLUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.dataaccess.valueobjects.XMLFormat;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;

import org.xml.sax.SAXException;

public class TemplateDAO 
{
  private Logger logger = 
        new Logger("com.ftd.accountingreporting.reporting.dao.TemplateDAO");
  
  private Connection dbConn;
  
  public TemplateDAO(Connection con)
  {
    this.dbConn=con;
  }
  
 
  /**
   * lookup source codes. return a Document object that contains sources codes likes the source code passed in. 
   * @param sourceCode
   * @param start The start row id.
   * @param max The maximum number can be returned.
   * @return 
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws java.io.IOException
   * @throws java.sql.SQLException
   * @throws java.lang.Exception
   */
  public Document lookupSourceCodes(String sourceCode, int start,  int max) throws SAXException, ParserConfigurationException, IOException, 
               SQLException, Exception
  {
        if(logger.isDebugEnabled()){
            logger.debug("Entering lookupSourceCodes "+sourceCode);
            logger.debug("start number : " + start);
            logger.debug("max number : " + max);
        }
        
        DataRequest request = new DataRequest();
        Document root =  DOMUtil.getDocument();
        if(sourceCode!=null)
        {
          sourceCode=sourceCode.trim().toUpperCase();
        }
        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_SOURCE_CODE", sourceCode);
            inputParams.put("IN_START_POSITION", ""+start);
            inputParams.put("IN_MAX_NUMBER_RETURNED", ""+max);    
            /* build DataRequest object */
            request.setConnection(dbConn);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("LOOKUP_SOURCE_CODE");

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
           
            HashMap results = (HashMap) dau.execute(request);
            
            addResultsToRootDocument(root, results, "source-code-list", "item");
          

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting lookupSourceCodes");
            } 
           
        }
        return root;
  }
  
  
  
  
  
   /**
   * lookup dnis codes. return a Document object that contains dnis codes likes the dnis passed in. 
   * @param sourceCode
   * @param start The start row id.
   * @param max The maximum number can be returned.
   * @return 
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws java.io.IOException
   * @throws java.sql.SQLException
   * @throws java.lang.Exception
   */
  public Document lookupDnis(String dnis, int start,  int max) throws SAXException, ParserConfigurationException, IOException, 
               SQLException, Exception
  {
        if(logger.isDebugEnabled()){
            logger.debug("Entering lookupDnis "+dnis);
            logger.debug("start number : " + start);
            logger.debug("max number : " + max);
        }
        
        DataRequest request = new DataRequest();
        Document root =  DOMUtil.getDocument();
        if(dnis!=null)
        {
          dnis=dnis.trim().toUpperCase();
        }
        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_DNIS_ID", dnis);
            inputParams.put("IN_START_POSITION", ""+start);
            inputParams.put("IN_MAX_NUMBER_RETURNED", ""+max);    
            /* build DataRequest object */
            request.setConnection(dbConn);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("LOOKUP_DNIS_ID");

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
           
            HashMap results = (HashMap) dau.execute(request);
            
            addResultsToRootDocument(root, results, "dnis-code-list", "item");
          

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting lookupDnis");
            } 
           
        }
        return root;
  }
  
  
  
   /**
   * lookup dnis codes. return a Document object that contains dnis codes likes the dnis passed in. 
   * @param sourceCode
   * @param start The start row id.
   * @param max The maximum number can be returned.
   * @return 
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws java.io.IOException
   * @throws java.sql.SQLException
   * @throws java.lang.Exception
   */
  public Document lookupUserId(String userId, int start,  int max) throws SAXException, ParserConfigurationException, IOException, 
               SQLException, Exception
  {
        if(logger.isDebugEnabled()){
            logger.debug("Entering lookupUserId "+userId);
            logger.debug("start number : " + start);
            logger.debug("max number : " + max);
        }
        
        DataRequest request = new DataRequest();
        Document root =  DOMUtil.getDocument();
        if(userId!=null)
        {
          userId=userId.trim().toUpperCase();
        }
        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_IDENTITY_ID", userId);
            inputParams.put("IN_START_POSITION", ""+start);
            inputParams.put("IN_MAX_NUMBER_RETURNED", ""+max);    
            /* build DataRequest object */
            request.setConnection(dbConn);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("LOOKUP_USER_ID");

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
           
            HashMap results = (HashMap) dau.execute(request);
            
            addResultsToRootDocument(root, results, "user-id-list", "item");
          

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting lookupDnis");
            } 
           
        }
        return root;
  }
  
  
  
  
  
  

  void addResultsToRootDocument(Document root, HashMap results, String topName, String bottomName) throws Exception
  {
    CachedResultSet resultSet = (CachedResultSet)results.get("OUT_CUR");
    XMLFormat xmlFormat = XMLUtil.createXMLFormat(topName, bottomName, true);
    Document sourceDoc = DOMUtil.convertToXMLDOM(resultSet, xmlFormat);
    Number total = (Number)results.get("OUT_ROW_COUNT");
    if(sourceDoc.getElementsByTagName(topName)!=null){
      DOMUtil.addSection(root, sourceDoc.getElementsByTagName(topName));
    }
    XMLUtil.addElementToXML("total-number", "" + total.intValue(), root);
  }
  
  
  
  
  
  
  /**
   * lookup source types. return a Document object that contains sources types likes the source type passed in. 
   * @param sourceType
   * @param start The start row id.
   * @param max The maximum number can be returned.
   * @return 
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws java.io.IOException
   * @throws java.sql.SQLException
   * @throws java.lang.Exception
   */
  public Document lookupSourceTypes(String sourceType, int start,  int max) throws SAXException, ParserConfigurationException, IOException, 
               SQLException, Exception
  {
        if(logger.isDebugEnabled()){
            logger.debug("Entering lookupSourceTypes "+sourceType);
            logger.debug("start number : " + start);
            logger.debug("max number : " + max);
        }
        
        DataRequest request = new DataRequest();
        Document root = DOMUtil.getDocument();
        if(sourceType!=null)
        {
          sourceType=sourceType.trim().toUpperCase();
        }
        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_SOURCE_TYPE", sourceType);
            inputParams.put("IN_START_POSITION", ""+start);
            inputParams.put("IN_MAX_NUMBER_RETURNED", ""+max);    
            /* build DataRequest object */
            request.setConnection(dbConn);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("LOOKUP_SOURCE_TYPE");

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            HashMap results = (HashMap) dau.execute(request);
            
            addResultsToRootDocument(root, results, "source-type-list", "item");

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting lookupSourceTypes");
            } 
           
        }
        return root;
  }
  
  
  
  /**
   * lookup vendor ids. return a Document object that contains vendor ids likes the vendor id passed in. 
   * @param vendorId
   * @param start The start row id.
   * @param max The maximum number can be returned.
   * @return 
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws java.io.IOException
   * @throws java.sql.SQLException
   * @throws java.lang.Exception
   */
  public Document lookupVendorIds(String vendorId, int start,  int max) throws SAXException, ParserConfigurationException, IOException, 
               SQLException, Exception
  {
        if(logger.isDebugEnabled()){
            logger.debug("Entering lookupVendorIds "+vendorId);
            logger.debug("start number : " + start);
            logger.debug("max number : " + max);
        }
        
        DataRequest request = new DataRequest();
        Document root = DOMUtil.getDocument();
        if(vendorId!=null)
        {
          vendorId=vendorId.trim().toUpperCase();
        }
        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_VENDOR_ID", vendorId);
            inputParams.put("IN_START_POSITION", ""+start);
            inputParams.put("IN_MAX_NUMBER_RETURNED", ""+max);    
            /* build DataRequest object */
            request.setConnection(dbConn);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("LOOKUP_VENDOR_ID");

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            HashMap results = (HashMap) dau.execute(request);
            
            addResultsToRootDocument(root, results, "vendor-id-list", "item");

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting lookupVendorIds");
            } 
           
        }
        return root;
  }
  
  
  
   public String validateDnis(String dnis) throws SAXException, ParserConfigurationException, IOException, 
               SQLException, Exception
  {
        if(logger.isDebugEnabled()){
            logger.debug("Entering validateDnis "+dnis);
            
        }
        
        return this.doValidation("IN_DNIS_ID", dnis, "VALIDATE_DNIS_ID");
        
      
  }
  
  
  
   public String validateUserId(String userId) throws SAXException, ParserConfigurationException, IOException, 
               SQLException, Exception
  {
        if(logger.isDebugEnabled()){
            logger.debug("Entering validateUserId "+userId);
            
        }
        
        return this.doValidation("IN_IDENTITY_ID", userId, "VALIDATE_USER_ID");
        
      
  }
  
  
  
  public String validateSourceCode(String sourceCode) throws SAXException, ParserConfigurationException, IOException, 
               SQLException, Exception
  {
        if(logger.isDebugEnabled()){
            logger.debug("Entering validateSourceCode "+sourceCode);
            
        }
        
        return this.doValidation("IN_SOURCE_CODE", sourceCode, "VALIDATE_SOURCE_CODE");
        
      
  }
  
  
  public String validateSourceType(String sourceType) throws SAXException, ParserConfigurationException, IOException, 
               SQLException, Exception
  {
        if(logger.isDebugEnabled()){
            logger.debug("Entering validateSourceType "+sourceType);
            
        }
        
        return this.doValidation("IN_SOURCE_TYPE", sourceType, "VALIDATE_SOURCE_TYPE");
       
       
  }
  
  
  
  public String validateVendorProductId(String productId) throws SAXException, ParserConfigurationException, IOException, 
               SQLException, Exception
  {
        if(logger.isDebugEnabled()){
            logger.debug("Entering validateVendorProductId "+productId);
            
        }
        
        return this.doValidation("IN_PRODUCT_ID", productId, "VALIDATE_VENDOR_PRODUCT_ID");
       
       
  }
  
  
  
  public String validateVendorId(String vendorId) throws SAXException, ParserConfigurationException, IOException, 
               SQLException, Exception
  {
        if(logger.isDebugEnabled()){
            logger.debug("Entering validateVendorId "+vendorId);
            
        }
        
        return this.doValidation("IN_VENDOR_ID", vendorId, "VALIDATE_VENDOR_ID");
       
       
  }
  
  public String validateFlorist(String option, String data) throws Exception
  {
        if(logger.isDebugEnabled()){
            logger.debug("Entering validateFlorist "+ option + ":" + data);
            
        }
        Map parmMap = new HashMap();
        parmMap.put("IN_P_OPTION", option);
        parmMap.put("IN_P_DATA", data);
        
        return this.doValidation(parmMap, "VALIDATE_FLORIST");
  }
  
  private String doValidation(Map parmMap, String statementId) throws Exception
  {
        DataRequest request = new DataRequest();
        
        String indicator="";
        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.putAll(parmMap);
            /* build DataRequest object */
            request.setConnection(dbConn);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID(statementId);

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            indicator = (String) dau.execute(request);
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting doValidation");
            }
        } 

        return indicator;  
  }
  
  private String doValidation(String inParameterName, String inParameterValue, String statementId) throws SAXException, ParserConfigurationException, IOException, 
               SQLException, Exception
  {
                 
       if(logger.isDebugEnabled()){
            logger.debug("Entering doValidation. in name: "+inParameterName+" in value: "+inParameterValue+" statement: "+statementId);
        }
        
        DataRequest request = new DataRequest();
        
        String indicator="";
        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(inParameterName, inParameterValue);
             
            /* build DataRequest object */
            request.setConnection(dbConn);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID(statementId);

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            indicator = (String) dau.execute(request);
            
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting validateSourceType");
            } 
           
        }
        return indicator;          
                 
  }
  
  
  
}