package com.ftd.accountingreporting.reporting.dao;
import com.ftd.accountingreporting.reporting.vo.ReportParameterValueVO;
import com.ftd.accountingreporting.reporting.vo.ReportVO;
import com.ftd.accountingreporting.util.XMLUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.dataaccess.valueobjects.XMLFormat;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.BufferedInputStream;
import java.io.IOException;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import java.sql.Blob;
import java.sql.Clob;
import org.w3c.dom.Document;

import org.xml.sax.SAXException;

public class ReportDAO 
{
   private Logger logger = 
        new Logger("com.ftd.accountingreporting.reporting.dao.ReportDAO");
  
  private Connection dbConn;
  
  
  public ReportDAO(Connection con)
  {
      this.dbConn=con;
  }
  
  
  public Document getUserExecutableReportList(String userId) throws SAXException, ParserConfigurationException, IOException, 
               SQLException,  Exception
  {
        if(logger.isDebugEnabled()){
            logger.debug("Entering getUserExecutableReportList "+userId);
            
        }
        
        DataRequest request = new DataRequest();
        Document root=DOMUtil.getDocument();
        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_CSR_ID", userId);
               
            /* build DataRequest object */
            request.setConnection(dbConn);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("GET_REPORT_LIST");

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            
            HashMap results= (HashMap) dau.execute(request);
            //get categories
            CachedResultSet categoryRS=(CachedResultSet)results.get("OUT_CATEGORY_CUR");  
            XMLFormat xmlFormat=XMLUtil.createXMLFormat("categories", "category", false);
            Document categoryDoc=(Document)DOMUtil.convertToXMLDOM(categoryRS, xmlFormat);
            DOMUtil.print(categoryDoc, System.out);
            DOMUtil.addSection(root, categoryDoc.getFirstChild().getChildNodes());
            //get reports.
            CachedResultSet reportRS=(CachedResultSet)results.get("OUT_CUR");  
            xmlFormat=XMLUtil.createXMLFormat("reports", "report", true);
            Document reportDoc=(Document)DOMUtil.convertToXMLDOM(reportRS, xmlFormat);
                        
            //categoryDoc.print(System.out);
            DOMUtil.addSection(root, reportDoc.getFirstChild().getChildNodes());

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getReportSubmissionList");
            } 
           
        }
        return root;
  }
  
  
  
  public HashMap getReportAndParameters(String reportId, boolean withParameterValue) throws SAXException, ParserConfigurationException, IOException, 
               SQLException, Exception
  {
        if(logger.isDebugEnabled()){
            logger.debug("Entering getReportAndParameters "+reportId);
            
        }
        
        DataRequest request = new DataRequest();
        
        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_REPORT_ID", reportId);
            inputParams.put("IN_PARM_VALUE_INDICATOR", withParameterValue?"Y":"N");  
            /* build DataRequest object */
            request.setConnection(dbConn);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("GET_REPORT_PARM");

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
           
            return  (HashMap) dau.execute(request);
                   
          

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getReportSubmissionList");
            } 
           
        }
       
  }
  
  
  
  
  public void setReportParameterValueOptionMap (ReportParameterValueVO reportParameterValueVO, String statementId) throws SAXException, ParserConfigurationException, IOException, 
               SQLException, Exception
  {
        if(logger.isDebugEnabled()){
            logger.debug("Entering setReportParameterValueOptionMap "+reportParameterValueVO.getDbStatmentId());
            
        }
        
        
        
        try {
            CachedResultSet result=this.getReportParameterValueOptionMap(statementId);
            while(result.next())
            {
              reportParameterValueVO.getOptionMap().put(result.getString(1), result.getString(2));
            }
          

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getReportSubmissionList");
            } 
           
        }
       
  }
  
  
  
  
  private CachedResultSet getReportParameterValueOptionMap(String statementId) throws SAXException, ParserConfigurationException, IOException, 
               SQLException, Exception
  {
        if(logger.isDebugEnabled()){
            logger.debug("Entering getReportParameterValueOptionMap "+statementId);
            
        }
        
        DataRequest request = new DataRequest();
        
        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            
            /* build DataRequest object */
            request.setConnection(dbConn);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID(statementId);

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
           
            return  (CachedResultSet) dau.execute(request);
                   
          

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getReportSubmissionList");
            } 
           
        }
       
  }
  
  
  
  
  public String logSubmissionRequest (ReportVO reportVO, String oracleReportDesFileName, String oracleReportUrl, String userId, String submissionType) throws SAXException, ParserConfigurationException, IOException, 
               SQLException, Exception
  {
        if(logger.isDebugEnabled()){
            logger.debug("Entering logSubmissionRequest "+oracleReportDesFileName+"\t "+oracleReportUrl);
            
        }
        
        String submissionId=null;
        
        DataRequest request = new DataRequest();
        
        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_REPORT_ID", reportVO.getId());
            inputParams.put("IN_REPORT_FILE_NAME", oracleReportDesFileName);
            inputParams.put("IN_SUBMISSION_TYPE", submissionType);
            inputParams.put("IN_SUBMITTED_BY", userId);
            inputParams.put("IN_REPORT_STATUS", "O");//open
            inputParams.put("IN_ORACLE_REPORT_URL", oracleReportUrl);
            /* build DataRequest object */
            request.setConnection(dbConn);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("INSERT_REPORT_SUBMISSION_LOG");

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
           
            Map resultMap=(Map)dau.execute(request);
            if(resultMap.get("OUT_REPORT_SUBMISSION_ID")!=null)
            {
              submissionId=(String)resultMap.get("OUT_REPORT_SUBMISSION_ID");
              
            }else
            {
              throw new Exception("can not log report submission. Error: "+resultMap.get("OUT_MESSAGE"));
            }
            
                   
          

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting logSubmissionRequest");
            } 
           
        }
        
        return submissionId;
       
  }
  
  
  
  
  
  public void updateSubmissionRequest (String submissionId, String userId,  String status, String error) throws SAXException, ParserConfigurationException, IOException, 
               SQLException, Exception
  {
        if(logger.isDebugEnabled()){
            logger.debug("Entering updateSubmissionRequest "+submissionId);
            
        }
        
               
        DataRequest request = new DataRequest();
        
        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_REPORT_SUBMISSION_ID", submissionId);
            inputParams.put("IN_COMPLETED_BY", userId);
            inputParams.put("IN_REPORT_STATUS", status);
            inputParams.put("IN_ERROR_MESSAGE", error);
            
            /* build DataRequest object */
            request.setConnection(dbConn);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("UPDATE_REPORT_SUBMISSION_LOG");

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
           
            Map resultMap=(Map)dau.execute(request);
            
            if(resultMap.get("OUT_STATUS")!=null)
            {
              String outStatus=(String)resultMap.get("OUT_STATUS");
              if(outStatus.trim().equalsIgnoreCase("N"))
              {
                throw new SQLException((String)resultMap.get("OUT_MESSAGE"));
              }
              
            }
            
                   
          

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting updateSubmissionRequest");
            } 
           
        }
        
     
       
  }
  
  
  
  
  
  
  public String getOracleReportURL ( String submissionId) throws SAXException, ParserConfigurationException, IOException, 
               SQLException, Exception
  {
        if(logger.isDebugEnabled()){
            logger.debug("Entering getOracleReportURL "+submissionId);
            
        }
        
        String reportURL=null;
        
        DataRequest request = new DataRequest();
        
        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_REPORT_SUBMISSION_ID", submissionId);
            
            /* build DataRequest object */
            request.setConnection(dbConn);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("VIEW_REPORT_SUBMISSION_LOG");

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
           
            CachedResultSet result=(CachedResultSet)dau.execute(request);
                   
            if(result.next())
            {
              reportURL=result.getString("oracle_report_url");
              
            }
            
           
            
                   
          

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getOracleReportURL");
            } 
           
        }
        
        return reportURL;
       
  }
  
  
  
  
  public Document getReportResultList(String userId, Date startDate, Date endDate) throws SAXException, ParserConfigurationException, IOException, 
               SQLException,  Exception
  {
        if(logger.isDebugEnabled()){
            logger.debug("Entering getSubmittedReportList "+userId+" between "+startDate+" and "+endDate);
            
        }
        
        DataRequest request = new DataRequest();
        
        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_CSR_ID", userId);
            inputParams.put("IN_START_DATE", new java.sql.Date(startDate.getTime()));
            inputParams.put("IN_END_DATE", new java.sql.Date(endDate.getTime()));
            /* build DataRequest object */
            request.setConnection(dbConn);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("GET_SUBMITTED_REPORTS");

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
           
            
            return (Document) dau.execute(request);
           

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getReportResultList");
            } 
           
        }
       
  }
  
  
  
  
  public InputStream getGeneratedReport (String submissionId, String type) throws SAXException, ParserConfigurationException, IOException, 
               SQLException,  Exception
  {
        if(logger.isDebugEnabled()){
            logger.debug("Entering getGeneratedReport "+submissionId+" type "+type);
            
        }
        
        DataRequest request = new DataRequest();
        InputStream report=null;
        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_REPORT_SUBMISSION_ID", submissionId);
            inputParams.put("IN_REPORT_OUTPUT_TYPE", type);
            
            /* build DataRequest object */
            request.setConnection(dbConn);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("GET_GENERATED_REPORT");

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            
            ResultSet rs=(ResultSet)dau.execute(request);
            if(rs.next())
            {
              if(type.equalsIgnoreCase("Bin"))
              {
                Blob blob=(rs).getBlob("report_detail");
                report=blob.getBinaryStream();
              }else
              {
                Clob clob=(rs).getClob("report_detail");
                report=clob.getAsciiStream();
              }
              
            }
            rs.close();
            
           

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getGeneratedReport");
            } 
           
        }
       
       return report;
  }
  
  
  
  public HashMap getScheduledReportList(java.util.Date runDate) throws SAXException, ParserConfigurationException, IOException, 
               SQLException, Exception
  {
        if(logger.isDebugEnabled()){
            logger.debug("Entering getScheduledReportList "+runDate);
            
        }
        
        DataRequest request = new DataRequest();
        
        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_RUN_DATE", new java.sql.Date(runDate.getTime()));
            
            /* build DataRequest object */
            request.setConnection(dbConn);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("GET_REPORT_LIST_TO_RUN");

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
           
            return  (HashMap) dau.execute(request);
                   
          

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getScheduledReportList");
            } 
           
        }
       
  }
  
  
  
}
