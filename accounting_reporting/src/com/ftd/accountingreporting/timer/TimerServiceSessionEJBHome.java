package com.ftd.accountingreporting.timer;

import java.rmi.RemoteException;

import javax.ejb.CreateException;
import javax.ejb.EJBHome;

public interface TimerServiceSessionEJBHome extends EJBHome {
    TimerServiceSessionEJB create() throws RemoteException, CreateException;
}
