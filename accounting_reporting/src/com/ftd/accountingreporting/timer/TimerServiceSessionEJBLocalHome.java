package com.ftd.accountingreporting.timer;

import javax.ejb.CreateException;
import javax.ejb.EJBLocalHome;

public interface TimerServiceSessionEJBLocalHome extends EJBLocalHome {
    TimerServiceSessionEJBLocal create() throws CreateException;
}
