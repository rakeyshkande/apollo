package com.ftd.accountingreporting.timer;

import java.rmi.RemoteException;

import javax.ejb.EJBObject;

public interface TimerServiceSessionEJB extends EJBObject {
    public void start(TimerData data) throws RemoteException, Exception;
    public void stop(String timerName) throws RemoteException, Exception;
    public boolean isRunning(String timerName) throws RemoteException, Exception;
    public String getTimers() throws RemoteException, Exception;
}
