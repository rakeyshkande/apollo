package com.ftd.accountingreporting.timer;

import com.ftd.accountingreporting.altpay.handler.PayPalEODHandler;
import com.ftd.accountingreporting.altpay.vo.PayPalProfileVO;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.constant.AltPayConstants;
import com.ftd.accountingreporting.util.AccountingUtil;
import com.ftd.accountingreporting.util.EODUtil;
import com.ftd.accountingreporting.util.SystemMessager;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.*;


public class PayPalConnectionTimerData extends TimerData implements Serializable {

    private static transient Logger logger = new Logger("com.ftd.accountingreporting.timer.PayPalConnectionTimerData");
    private PayPalProfileVO ppProfileVO;
    private String ppConnectInterval;
    private static final String TIMER_CONST = "900000";
    private static final String RETRY_CONST = "5";

    // default constructor
    public PayPalConnectionTimerData() {
    }
    
    public void init() throws EJBException {
        String timerMs = null;
        String retry = null;
               
        try {
            logger.debug("ppConnectInterval=" + ppConnectInterval);
            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
            timerMs = configUtil.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, ppConnectInterval);
            retry = configUtil.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, AltPayConstants.PP_CONNECT_RETRY_COUNT);

            super.setMs(Long.valueOf(timerMs).longValue());
            super.setRetry(Integer.parseInt(retry));
            logger.info("Setting retry to " + retry + ". Setting interval to " + timerMs);
        } catch (Exception e) {
            logger.error("Could not get global parm. Setting retry to " + TIMER_CONST + ". Setting interval to " + RETRY_CONST);
            super.setMs(Long.valueOf(TIMER_CONST).longValue());
            super.setRetry(Integer.parseInt(RETRY_CONST));
            
            logger.error(e);
        }    
    }

    public void onTimeout(Timer t) throws EJBException {
       logger.info("Timer " + getTimerName() + "timed out. ");
       logger.info("Testing PayPal connection...");
       
       try {
           boolean connected = this.testConnection();
           logger.info("result:" + connected);
           if(connected) {
                // start PayPal EOD
                 List messageList  = new ArrayList();
                 messageList.add(AltPayConstants.PAYPAL_EOD_PAYLOAD);
                 AccountingUtil.dispatchJMSMessageList
                     (AltPayConstants.ALTPAY_EOD_QUEUE, messageList, 0, 0);
                
                // cancel timer
                t.cancel();
           } else {
                // increment retry made
                retryMade++;
                logger.info("retry:" + retry);
                logger.info("retry made:" + retryMade);
                
                if(retryMade > retry) {
                    // Send system message and cancel timer.
                    logger.debug("Cancelling timer...");
                    t.cancel();
                    logger.debug("Timer cacelled.");
                    try {
                        dbConn = EODUtil.createDatabaseConnection();
                        
                        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
                        String retryInterval = (String)
                            configUtil.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT,AltPayConstants.PP_CONNECT_TIMER_MS);
                        logger.debug("retry interval:" + retryInterval);
                        int retryMin = (Integer.parseInt(retryInterval) * retry) / 60000;
                        String message = AltPayConstants.PP_ERROR_CONNECTION + retryMin + " minutes.";
                        logger.info(message);
                        SystemMessager.sendSystemMessage(dbConn, new Exception(message));
                    } catch (Exception e) {
                        logger.error(e);
                    } finally {
                        if(dbConn!=null) {
                            dbConn.close();
                        }
                    }
                }
           }
       } catch (Throwable ex) {
          throw new EJBException(ex.getMessage());
       }
    }

    private boolean testConnection() throws Throwable {
        ppProfileVO.createConfigsMap();
        PayPalEODHandler h = new PayPalEODHandler();
        h.setPpProfileVO(ppProfileVO);
        
        return h.testConnection();
    }

    public void setPpProfileVO(PayPalProfileVO ppProfileVO) {
        this.ppProfileVO = ppProfileVO;
    }

    public PayPalProfileVO getPpProfileVO() {
        return ppProfileVO;
    }

    public void setPpConnectInterval(String ppConnectInterval) {
        this.ppConnectInterval = ppConnectInterval;
    }

    public String getPpConnectInterval() {
        return ppConnectInterval;
    }


}
