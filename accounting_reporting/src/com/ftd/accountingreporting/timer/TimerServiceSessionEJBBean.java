package com.ftd.accountingreporting.timer;

import com.ftd.osp.utilities.plugins.Logger;

import java.rmi.RemoteException;

import java.util.Collection;

import java.util.Iterator;

import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.ejb.TimedObject;
import javax.ejb.Timer;
import javax.ejb.TimerService;

/**
 * This is a stateless session bean that provides Timer Callback service
 * to any client. It supports multiple timers that are managed by the
 * container. Invoking the beans start method and pass a TimerData
 * object to start the timer. The ejbTimeout method get called
 * by the container every time the clock expires.
 */
public class TimerServiceSessionEJBBean implements SessionBean, TimedObject {
    private SessionContext _context;
    private Logger logger = new Logger("com.ftd.accountingreporting.timer.TimerData");

    public void ejbCreate() {
    }

    public void setSessionContext(SessionContext context) throws EJBException {
        _context = context;
    }

    public void ejbRemove() throws EJBException {
    }

    public void ejbActivate() throws EJBException {
    }

    public void ejbPassivate() throws EJBException {
    }

    public void start(TimerData data)  throws RemoteException, Exception{

       long ms = data.getMs();

       try {
          TimerService ts = _context.getTimerService();

          // Execute after "secs" seconds, and then every "secs" 
          // seconds thereafter.
          Timer timer = ts.createTimer(0, ms, data);
          data.onStart(timer);

       } catch (Exception e) {
          logger.error("TimerBean.start: " + e.getMessage());
       }
    }
    
    public void stop(String timerName)  throws RemoteException, Exception{
       try {
          TimerService ts = _context.getTimerService();
          Collection timers = ts.getTimers();
          Iterator it = timers.iterator();

          // For every timer whose "info" is TimerData, if the timer's
          // name is timerName, pass that timer to
          // TimerData.onStop. The TimerData object must decide whether
          // or not to call Timer.cancel.
          while (it.hasNext()) {
             Timer t = (Timer)it.next();

             if (isCalled(t, timerName)) {
                TimerData td = (TimerData)t.getInfo();
                td.onStop(t);
             }
          }
       } catch (Exception e) {
          logger.error("TimerBean.stop: " + e.getMessage());
       }
    }

    // If the timer exists, it's running, so return true; else false
    public boolean isRunning(String timerName)  throws RemoteException, Exception{
       try {
          TimerService ts = _context.getTimerService();
          Collection timers = ts.getTimers();
          Iterator it = timers.iterator();

          if (it.hasNext()) {
             Timer t = (Timer)it.next();
             if (isCalled(t, timerName)) {
                return true;
             }
          }
       } catch (Exception e) {
          logger.error("TimerBean.isRunning: " +
                             e.getMessage());
       }
       return false;
    }
    
    public String getTimers()  throws RemoteException, Exception{
       StringBuffer sb = new StringBuffer();
       try {
          Collection timers = _context.getTimerService().getTimers();
          for(Iterator it = timers.iterator(); it.hasNext();)
          {
            TimerData td = (TimerData)((Timer)it.next()).getInfo();
            sb.append("\n" + td.getTimerName());
          }          
       } catch (Exception e) {
          logger.error("TimerBean.getTimers: " + e.getMessage());
       }
      return(sb.toString());
    }


    protected boolean isCalled(Timer t, String timerName) {
       try {
          Object info = t.getInfo();
          if (info != null && info instanceof TimerData) {
             TimerData td = (TimerData)info;
             if (td.getTimerName().equals(timerName)) {
                return true;
             }
          }
       } catch (Exception e) {
          logger.error("Exception cancelling timer: " +
                             e.getMessage());
       }
       return false;
    }
    
    // When the EJB times out, call back to the TimerData's onTimeout method
    public void ejbTimeout(Timer timer) {
       Object info = timer.getInfo();

       if (info != null && info instanceof TimerData) {
          TimerData td = (TimerData)info;
          td.onTimeout(timer);
       }
    }
}
