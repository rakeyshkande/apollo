package com.ftd.accountingreporting.timer;

import java.rmi.RemoteException;

import javax.ejb.EJBLocalObject;

public interface TimerServiceSessionEJBLocal extends EJBLocalObject {
       public void start(TimerData data) throws Exception;
       public void stop(String timerName) throws Exception;
       public boolean isRunning(String timerName) throws Exception;
       public String getTimers() throws Exception;
}
