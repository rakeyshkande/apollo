package com.ftd.accountingreporting.timer;

import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.constant.AltPayConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.Serializable;

import java.sql.Connection;

import javax.ejb.*;

//
// This is an auxiliary class for TimerServiceSessionEJB. It is an abstract
// class that contains methods to be called when the timer is started,
// cancelled, or timedout.
//
public abstract class TimerData
   implements Serializable {

   protected String timerName;
   protected Object data;
   protected long ms;
   protected static int retry;
   protected static int retryMade;
   protected Connection dbConn;
   private static transient Logger logger = new Logger("com.ftd.accountingreporting.timer.TimerData");

   public TimerData() {}
   
   public TimerData(String _name, long _ms, Object _data) {
      timerName = _name;
      data = _data;
      ms = _ms;
   }

   public Object getData() {
      return data;
   }

   public long getMs() {
      return ms;
   }


   protected void setData(Object _data) {
      data = _data;
   }

   protected void setMs(long _ms) {
      ms = _ms;
   }

   public void onStart(Timer t) throws EJBException {
      logger.info("Timer " + getTimerName() + " started, period = " +
                         getMs());
   }

   public void onStop(Timer t) throws EJBException {
      logger.info("Stopping timer " + getTimerName());
      try {
         t.cancel();
      } catch (Exception e) {
         throw new EJBException(e);
      }
   }

   public void onTimeout(Timer t) throws EJBException {
      logger.info("Timer " + getTimerName() + " timed out");
   }

    public void setRetry(int _retry) {
        this.retry = _retry;
    }

    public int getRetry() {
        return retry;
    }

    public void setRetryMade(int _retryMade) {
        this.retryMade = _retryMade;
    }

    public int getRetryMade() {
        return retryMade;
    }

    public void setTimerName(String timerName) {
        this.timerName = timerName;
    }

    public String getTimerName() {
        return timerName;
    }
}
