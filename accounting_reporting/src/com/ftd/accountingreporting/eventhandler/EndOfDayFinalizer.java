package com.ftd.accountingreporting.eventhandler;

import java.io.File;
import java.io.FileWriter;
import java.net.InetAddress;
import java.sql.Connection;
import java.util.Calendar;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import com.ftd.accountingreporting.bo.EODRecordBO;
import com.ftd.accountingreporting.bo.EODRegularBillBO;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.dao.EODDAO;
import com.ftd.accountingreporting.exception.RemoteFileExistsException;
import com.ftd.accountingreporting.util.EODUtil;
import com.ftd.accountingreporting.util.FileArchiveUtil;
import com.ftd.accountingreporting.util.FtpUtil;
import com.ftd.accountingreporting.util.LockUtil;
import com.ftd.accountingreporting.util.SystemMessager;
import com.ftd.accountingreporting.util.security.FileProcessorPgp;
import com.ftd.accountingreporting.vo.EODFileVO;
import com.ftd.accountingreporting.vo.EODMessageVO;
import com.ftd.accountingreporting.vo.EODRecordVO;
import com.ftd.eventhandling.events.EventHandler;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.GiftCodeUtil;
import com.ftd.osp.utilities.RESTUtils;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.xml.DOMUtil;

/**
 * Class that performs EOD finalizing tasks after all carts have been processed:
 * 1. create eod file
 * 2. send file to JDE
 * 3. mask local file
 */

public class EndOfDayFinalizer extends EventHandler
{
    private static Logger logger  = new Logger("com.ftd.accountingreporting.eventhandler.EndOfDayFinalizer");
    //private static final int PAD_RIGHT = 0; 
    private static final int PAD_LEFT = 1; 
    private static final String newLine = "\n";
    
   /**
    * This method is overridden method from eventhandler
    * @param payLoad
    * @throws java.lang.Throwable
    */
    public void invoke(Object payload) throws Throwable
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering invoke");
        }
        // Retrieve time interval from configuration file
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        long checkCartCountInterval = Long.parseLong(configUtil.getProperty(ARConstants.CONFIG_FILE, "eodCartCountCheckInterval"));
        long checkCartCountChangeInterval = Long.parseLong(configUtil.getProperty(ARConstants.CONFIG_FILE, "eodCartCountChangeCheckInterval"));
        long fileSendRetryInterval = Long.parseLong(configUtil.getProperty(ARConstants.CONFIG_FILE, "eodFileSendRetryInterval"));
        int fileSendRetryCount = Integer.parseInt(configUtil.getProperty(ARConstants.CONFIG_FILE, "eodFileSendRetryCount"));
        String securityPublicKeyPath = configUtil.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, "securityPublicKeyPathIseries");
        int curFileSendRetryCount = 0;
        long curCheckCartCountInterval = 0;
        int recoveryRecCount = 0;
        int lastRecoveryRecCount = 0;
        LockUtil lockUtil = null;
        String sessionId = new Integer(this.hashCode()).toString();
        boolean lockObtained = false;
        Connection conn = EODUtil.createDatabaseConnection();
        EODDAO dao = new EODDAO(conn);
        EODMessageVO eodMessageVO = null;
      
        lockUtil = new LockUtil(conn);
        // lock process 
        lockObtained = lockUtil.obtainLock(ARConstants.LOCK_ENTITY_TYPE,
                ARConstants.LOCK_ENTITY_ID,ARConstants.LOCK_CSR_ID,sessionId);
        if (lockObtained) {
            if(logger.isDebugEnabled()) {
                logger.debug("obtained lock...");
            }
            dao = new EODDAO(conn);
            eodMessageVO = this.extractMessage((MessageToken)payload);
        
          try {      
            // archive old files.
            this.archiveFiles();   
            
            // perform other tasks 
            try {
                  Calendar batchDate =  EODUtil.convertLongDate(eodMessageVO.getBatchTime());
                  performCompletionTasks(batchDate, conn);
            } catch (Exception e) {
                  logger.error(e);
                  throw e;
            }
        
            while (true) {      
                // Check if eod_recovery table is empty. Proceed with final steps if so.
                recoveryRecCount = dao.doCountRecoveryRec(null);
                
                if(recoveryRecCount == 0) {
                    EODFileVO eodFileVO = new EODFileVO();
                    eodFileVO.setStartTime(eodMessageVO.getStartTime());
                    eodFileVO.setBatchNumber(eodMessageVO.getBatchNumber());
                    eodFileVO.setBatchTime(eodMessageVO.getBatchTime());
                    
                    // compile file
                    File billingFile = this.createBillingFile(eodFileVO, dao);
                    int fileTransferResult = 0;
                    
                    try {
                    
                    // Defect 1069: Encrypt file
                    File encryptedFile = FileProcessorPgp.encrypt(
                    billingFile.getPath(), 
                    securityPublicKeyPath, false, false, false);
                    
                        while(true) {
                            
                            fileTransferResult = putFtpFileToServer(encryptedFile);
                            if(fileTransferResult == 0) {
                                // mask cc number in the local file. Skip first 2 lines, and the last line. 
                                // Start from position 6 on the rest of each line. 
                                logger.debug("Masking...");
                                FileArchiveUtil.overwriteFileContents(billingFile.getPath(), 2, 1, 5, ARConstants.CC_MASK);                                                       
                                break;
                            } else if(fileTransferResult == -2 && curFileSendRetryCount > fileSendRetryCount) {
                                InetAddress addr = InetAddress.getLocalHost();
                                String hostname = addr.getHostName();
                                String ftpTarget = configUtil.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, "eodFtpServer");
                            
                                sendSystemMessage(conn, new Exception("Connection to server failed after trying for " + (curFileSendRetryCount*fileSendRetryInterval)/60000 + 
                                " minutes. Please Manually transfer file from " + hostname + " to " + ftpTarget + "."));
                                break;
                            } else {
                                Thread.sleep(fileSendRetryInterval);
                            }
                            curFileSendRetryCount++;
                        } 
                        
                    } catch(RemoteFileExistsException rfe) {
                        logger.error(rfe);
                        sendSystemMessage(conn, rfe);
                    } catch(Exception e) {
                        logger.error(e);
                        sendSystemMessage(conn, e);
                    }      
                    
                    break;
                } else if (curCheckCartCountInterval >= checkCartCountChangeInterval) {
                    curCheckCartCountInterval = 0;
                    if (recoveryRecCount == lastRecoveryRecCount) {
                        // Counts in eod_recovery has not changed in checkCartCountInterval.
                        // Delete them from table
                        dao.doDeleteRecoveryTab(null);
                    }
                    lastRecoveryRecCount = recoveryRecCount;
                } else {
                    // Sleep for some configurable time interval.
                    Thread.sleep(checkCartCountInterval);
                    curCheckCartCountInterval += checkCartCountInterval;
                }
                
            }
          } catch (Exception e) {
              logger.error(e);
              sendSystemMessage(conn, e);
          } finally {
              if(lockObtained) {
                if(logger.isDebugEnabled()) {
                    logger.debug("releasing lock...");
                }
                lockUtil.releaseLock(ARConstants.LOCK_ENTITY_TYPE,ARConstants.LOCK_ENTITY_ID,ARConstants.LOCK_CSR_ID,sessionId);
            }
            if(conn != null && !conn.isClosed()) {
                conn.close();
            }
            if(logger.isDebugEnabled()){
                logger.debug("Exiting invoke");
            } 
          }
        } else {
            sendSystemMessage(conn, new Exception("Cannot obtain BILLING_EOD lock"));
        }
  
    }
    

  /**
   * Move files to the archive directory
   * @throws java.lang.Exception
   */
    private void archiveFiles() throws Exception
    {
        logger.debug("Entering archiveFiles...");
        String fromDir = ConfigurationUtil.getInstance().getProperty(
                ARConstants.CONFIG_FILE,ARConstants.CONST_EOD_FILE_LOCATION);
        String toDir = ConfigurationUtil.getInstance().getProperty(
                ARConstants.CONFIG_FILE,ARConstants.CONST_EOD_ARCHIVE_LOCATION);
                
        FileArchiveUtil.moveToArchive(fromDir, toDir, false);
        logger.debug("Exiting archiveFiles...");
    }    
    
  /**
   * Create the billing file.
   * @param eodFileVO
   * @param eodDAO
   * @return 
   * @throws java.lang.Exception
   */
    private File createBillingFile(EODFileVO eodFileVO, EODDAO eodDAO) throws Exception {
      List<CachedResultSet> aList = eodDAO.doGetBillingDetails(null);
      CachedResultSet rsBilled = aList.get(0);
      CachedResultSet rsBatchNumbers = aList.get(1); 
      
      File eodFile = null;
      FileWriter out = null; 
      String runDatetime = eodFileVO.getRunDateTime();
      long timestamp = System.currentTimeMillis();
      
      //write first line
      if(rsBilled != null) {
          logger.debug("Writing to file...");
          eodFile = new File(eodFileVO.getEodFileName() + "_" + runDatetime);
          out = new FileWriter(eodFile);
               
          out.write(eodFileVO.getFirstLine()+newLine);
          out.write(eodFileVO.getHeaderRecordType()+EODUtil.pad(new Long(eodFileVO.getBatchNumber()).toString(),PAD_LEFT,"0",6)+EODUtil.pad(eodFileVO.getDisplayBatchDate(),PAD_LEFT," ",6)+newLine);
  
          EODRecordVO vo = null;
          EODRecordBO bo = null;
          double settlementAmount = 0;
          int settlementCount = 0;
          int lineCount = 0;
          
          
          while(rsBilled.next())
          {
               vo = new EODRecordVO();
               bo = new EODRegularBillBO();
        
               vo.setLineItemType(rsBilled.getString("bill_type"));
               vo.setOrderNumber(rsBilled.getString("order_number"));
               vo.setOrderSequence(rsBilled.getLong("order_sequence"));
               vo.setCreditCardType(rsBilled.getString("cc_type"));
               vo.setCreditCardNumber(rsBilled.getString("cc_number"));
               vo.setCcExpDate(rsBilled.getString("cc_expiration"));
               vo.setOrderAmount(rsBilled.getDouble("order_amount"));
               vo.setSalesTaxAmount(new Double(rsBilled.getDouble("sales_tax_amount")).toString());
               vo.setTransactionType(rsBilled.getString("transaction_type"));
               if (rsBilled.getDate("auth_date")!= null)
                   vo.setAuthorizationDate(EODUtil.convertSQLDate((rsBilled.getDate("auth_date"))));
               vo.setApprovalCode(rsBilled.getString("auth_approval_code"));
               vo.setAcqReferenceNumber(rsBilled.getString("acq_reference_number"));
               vo.setAuthCharIndicator(rsBilled.getString("auth_char_indicator"));
               vo.setAuthTranId(rsBilled.getString("auth_transaction_id"));
               vo.setAuthValidationCode(rsBilled.getString("auth_validation_code"));
               vo.setAuthSourceCode(rsBilled.getString("auth_soure_code"));
               vo.setAVSResultCode(rsBilled.getString("avs_code"));
               vo.setResponseCode(rsBilled.getString("response_code"));
               vo.setclearingMemberNumber(rsBilled.getString("clearing_member_number"));
               vo.setRecipientZip(rsBilled.getString("recipient_zip_code"));
               vo.setOriginType(rsBilled.getString("origin_type"));
               vo.setCardHolderRefNumber(rsBilled.getString("cardholder_ref"));
               vo.setAddField1(rsBilled.getString("add_info1"));
               vo.setAddField2(rsBilled.getString("add_info2"));
               vo.setAddField3(rsBilled.getString("add_info3"));
               vo.setAddField4(rsBilled.getString("add_info4"));
               vo.setWalletIndicator(rsBilled.getLong("wallet_indicator"));
               bo.setRecordVO(vo);
               bo.setLineItemType();
        
               settlementCount++;
               settlementAmount += vo.getOrderAmount();
               logger.debug("****Line Number: " + lineCount + 
                            "****Amount: " + rsBilled.getDouble("order_amount") +
                            "****Order Number: " + rsBilled.getString("order_number") +
                            "****Payment ID: " + rsBilled.getString("payment_id") +
                            "****Transaction Type: " + rsBilled.getString("transaction_type")+
                            "****WALLET INDICATOR: " + rsBilled.getString("wallet_indicator"));
               try {
                    out.write(bo.buildLineText()+newLine);
               } catch (Exception e){
                    logger.error(e);
                    settlementCount--;
                    lineCount--;
                    settlementAmount -= vo.getOrderAmount();
                    //rollback status
                    if("1".equals(rsBilled.getString("transaction_type"))) {
                        eodDAO.doUpdatePaymentBillStatus(Long.parseLong(rsBilled.getString("payment_id")),timestamp,ARConstants.ACCTG_STATUS_UNBILLED);
                    } else if("3".equals(rsBilled.getString("transaction_type"))) {
                        eodDAO.doUpdateRefundBillStatus(Long.parseLong(rsBilled.getString("refund_id")),timestamp,ARConstants.ACCTG_STATUS_UNBILLED);
                    } else {
                        logger.error("Unknown transaction type");
                    }
               }
               lineCount++;
          } 
             
         //write billing trailer record with settlement amount and record count
         out.write(eodFileVO.getTrailerRecordType() + EODUtil.pad(new Integer(settlementCount).toString(),PAD_LEFT,"0",10) +  EODUtil.pad(EODUtil.formatDoubleAmount(settlementAmount).toString(),PAD_LEFT,"0",12));
         
         //update batch processed indicator.
         updateProcessedIndicator(eodDAO, rsBatchNumbers);
                
         if(out != null) {
            out.close();
         }
         logger.debug("Done writing to file!");
      }
      return eodFile;
   }

   /**
    * Updates the processed_indicator for the given batches
    */
   private void updateProcessedIndicator(EODDAO eodDAO, CachedResultSet rs) throws Exception {
      // Retrieve batch numbers and put it in comma delimited string
      String batches = "";
      String delim = ",";
      
      if(rs != null) {
          while (rs.next()) {
              batches = batches + rs.getString("billing_header_id");
              batches = batches + delim;
          }
          // Strip the last delimiter.
          if(batches.length() > 0) {
              batches = batches.substring(0,batches.length()-1);
          }
          logger.debug("Updating batches:" + batches);
          
          // Call procedure to update processed_indicator for all batches.
          eodDAO.doUpdateProcessedIndicator(batches, "Y");
      }
   }
    
  /**
   * Returns 0 if successfully put file.
   * Returns -2 if connection timed out.
   * @param eodFile
   * @return 
   * @throws com.ftd.accountingreporting.exception.RemoteFileExistsException
   * @throws java.lang.Exception
   */
    private int putFtpFileToServer(File eodFile) throws RemoteFileExistsException,Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering putFtpFileToServer");
        }
        
        try{        
            FtpUtil ftpUtil = new FtpUtil();
            
            /* retrieve server name to FTP to */
            String ftpServer = ConfigurationUtil.getInstance().getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, "eodFtpServer");
            String ftpLocation = ConfigurationUtil.getInstance().getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, "eodFtpLocation");
            String username = ConfigurationUtil.getInstance().getSecureProperty(ARConstants.SECURE_CONFIG_CONTEXT, "eodFtpUsername");
            String password = ConfigurationUtil.getInstance().getSecureProperty(ARConstants.SECURE_CONFIG_CONTEXT, "eodFtpPassword");
            //String fileName = eodFile.getName();
            String fileName = ConfigurationUtil.getInstance().getProperty(ARConstants.CONFIG_FILE, ARConstants.CONST_EOD_FILE_NAME);
            
            try {
                ftpUtil.login(ftpServer, username, password);
            } catch (Exception e) {
                // Connection to server failed.
                logger.error(e);
                return -2;
            }
            try {
            // JP Puzon 07-31-2006
            // Defect 1069: append extension ".pgp" to CCBILL filename, as requested by iSeries team. 
            fileName = ftpLocation + "/" + fileName + ".pgp";
            
            //Try to retrieve remote file of the same name to local archive directory.
            //If file with this name exists on server, do not put file.
            //String localArchiveLocation = ConfigurationUtil.getInstance().getProperty(ARConstants.CONFIG_FILE, ARConstants.CONST_EOD_ARCHIVE_LOCATION);
            
            // JP Puzon 03-27-2006
            // Defect 1167: The FTP must overwrite an existing file as mandated by the iSeries tech team.
//            boolean fileExists = ftpUtil.remoteFileExists(ftpServer, ftpLocation, localArchiveLocation, fileName,username, password);
//            
//            if(fileExists) {
                // JP Puzon 07-31-2006
                // Defect 1069: Overwrite rule removed as requested by iSeries team.
                // put the file to FTP server.
                ftpUtil.putFile(eodFile, fileName, true);                
//            } else {
//                throw new RemoteFileExistsException("Remote " + fileName + " at " + ftpServer + ":" + ftpLocation + " does NOT exist for overwrite. Transfer Cancelled.");
//            }
            } finally {
            ftpUtil.logout();
            }

        }
        finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting putFtpFileToServer");
            } 
        }
        return 0;
    }    
    
    /**
     * This method performs closing tasks necessary to End-of-Day process.
     * �	Mark all vendor-delivered orders with a ship date prior or on EOD date
     *    to a status of 'Shipped' (if in Printed status previously or Processed). 
     * �	Flag all vendor-delivered orders with an operational status of 'Shipped' 
     *    with a delivered flag if the delivery date on the order is eod date
     *    or prior. Capture delivery_date on eod_delivery_date.
     * �	Flag all florist-delivered orders with an operational status of 'Processed'
     *    with a delivered flag if the delivery date is on or beofre EOD date.
     *    Capture delivery_date on eod_delivery_date.
     *    
     * �	Update BILLING_HEADER completion_status to �Y�. 
     *      Call EODDAO.doUpdateProcessedIndicator(batchNumber, �Y�).
     * �	FTP file to the FTP server.
     * �	Archive file.
     * �	Release lock by calling LockUtil.releaseLock.
     * @param payload - Object
     * @return n/a
     * @throws Throwable
     */
    private void performCompletionTasks(Calendar batchDate, Connection conn) throws Exception
    {
    	String type = RESTUtils.getConfigParamValue("REFUND_TO_REINSTATE", false);
    	if(null == type){
    		logger.error("Type of Maintenance is null, Check Global Params");
    	}
        if(logger.isDebugEnabled()) {
            logger.debug("Entering cleanUpEOD");
        }
       try{
            EODDAO eodDAO = new EODDAO(conn);

            eodDAO.doSetEODDeliveredInd(batchDate.getTimeInMillis(), null);

            // Reinstate GCC that are put into 'Refund' Status.
            //eodDAO.doEodReinstateGcc();
            //08/25/2018 End of Day Prod Issue - commented this out GiftCodeUtil.gcMaintenance(type);
        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting cleanUpEOD");
            } 
        }
    }

    /**
     * Extracts information from the JMS message.
     */
    private EODMessageVO extractMessage(MessageToken mt) throws Exception {
        String message = (String)mt.getMessage();
        Document msgDoc = DOMUtil.getDocument(message);
        EODMessageVO eodMessageVO = new EODMessageVO();
        eodMessageVO.setBatchTime((Long.valueOf((((NodeList)msgDoc.getElementsByTagName("BATCH_TIME")).item(0).getFirstChild()).getNodeValue())).longValue());
        eodMessageVO.setStartTime((Long.valueOf((((NodeList)msgDoc.getElementsByTagName("START_TIME")).item(0).getFirstChild()).getNodeValue())).longValue());
        eodMessageVO.setBatchNumber((Long.valueOf((((NodeList)msgDoc.getElementsByTagName("BATCH_NUMBER")).item(0).getFirstChild()).getNodeValue())).longValue());
        return eodMessageVO;
    }
    
  /**
   * Sends a system message
   * 
   * @param t Throwable t
   */
    private void sendSystemMessage(Connection con, Throwable t)
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering sendSystemMessage");
        }
        try {
            String logMessage = t.getMessage();  
      
            SystemMessager.send(logMessage, t, ARConstants.EOD_BILLING_PROCESS, 
                SystemMessager.LEVEL_PRODUCTION, 
                ARConstants.EOD_BILLING_PROCESS_ERROR_TYPE, con);
        
        } catch (Exception ex) {
            logger.error(ex);
        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting sendSystemMessage");
            } 
        }   
    }
}