package com.ftd.accountingreporting.eventhandler;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.jms.JMSException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.constant.AltPayConstants;
import com.ftd.accountingreporting.dao.EODDAO;
import com.ftd.accountingreporting.exception.EntityLockedException;
import com.ftd.accountingreporting.timer.PayPalConnectionTimerData;
import com.ftd.accountingreporting.timer.TimerServiceSessionEJBLocal;
import com.ftd.accountingreporting.timer.TimerServiceSessionEJBLocalHome;
import com.ftd.accountingreporting.util.AccountingUtil;
import com.ftd.accountingreporting.util.EODUtil;
import com.ftd.accountingreporting.util.LockUtil;
import com.ftd.accountingreporting.util.SystemMessager;
import com.ftd.eventhandling.events.EventHandler;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;

/**
 * This class processes notification to run the End-of-Day job. It does the following:
 * 1. Persists a the payment ids of the charges and refunds that should be processed for the day.
 * 2. Send a message to the cart processor for each shopping cart.
 * 3. Send a message to the EOD finalizer.
 * 
 * @author Madhusudan Parab
 */

public class EndOfDayHandler extends EventHandler
{
    private static Logger logger  = 
        new Logger("com.ftd.accountingreporting.eventhandler.EndOfDayHandler");
    private Connection conn = null;
  
    /**
    * This is the invocation method for the event handler.  
    * The payload is the JMS payload. Not used in this case.  
    * This method will:
    * 1.	Obtain the process lock
    * 2.	Update removed order payment
    * 3.	Retrieve batch number
    *     3.1 if this is fresh run, persist the payment ids in recovery table and create new batch number
    *     3.2 if this is a recovery run, retrieve the batch number of the previous run.
    *     3.3 if there is already a sucessful run for the day, exit program.
    * 4.  In the case of 3.1 and 3.2, retrieve lists of payment ids for charges and refunds for the day.
    * 5.  For each shopping cart, dispatch a message to the cart processor.
    * 6.  Dispatch a message to the eod finalizer to compile the file and complete ending tasks.
    * 
    * @param payload
    * @throws java.lang.Throwable
    */
    public void invoke(Object payload) throws Throwable
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering invoke");
        }
        LockUtil lockUtil = null;
        String sessionId = null;
        boolean lockObtained = false;
        EODDAO eodDAO = null;
        long eodStartTime = 0;
        long eodBatchTime = 0;
        Calendar batchDate = null;

        //get text that is part of jms message
        MessageToken messageToken = (MessageToken)payload;
        String message = null;
        
        if (messageToken != null && !"NULL".equalsIgnoreCase((String)messageToken.getMessage())) {
        	message = (String)messageToken.getMessage();
        }
        
        logger.info("payload: " + message);
        
        try {
            sessionId = new Integer(this.hashCode()).toString();
            if(logger.isDebugEnabled()) {
                logger.debug("sessionId is " + sessionId);
            }
            // retrieve connection to database 
            conn = DataSourceUtil.getInstance().
                getConnection(ConfigurationUtil.getInstance().getProperty(
                ARConstants.CONFIG_FILE,ARConstants.DATASOURCE_NAME));
            eodDAO = new EODDAO(conn);
            lockUtil = new LockUtil(conn);
            
            // lock process 
            lockObtained = lockUtil.obtainLock(ARConstants.LOCK_ENTITY_TYPE,
                ARConstants.LOCK_ENTITY_ID,ARConstants.LOCK_CSR_ID,sessionId);
            if (lockObtained) {
                if(logger.isDebugEnabled()) {
                    logger.debug("obtained lock...");
                }
                
                try {
                    eodStartTime = Calendar.getInstance().getTimeInMillis();
                    eodBatchTime = this.getEODBatchTime(eodStartTime, message);
                    batchDate = EODUtil.convertLongDate(eodBatchTime);
                    // Update payment amount with removed orders. 
                    eodDAO.doUpdateRemovedOrderPayment(batchDate, message);
                    this.dispatchEndOfDayJobs(message);
                } catch (Exception e) {
                    logger.error(e);
                    throw e;
                }
 
            } else {
                logger.error("Unable to obtain lock");
            }
        } catch (EntityLockedException e) {
            logger.error(e);
            /* Send system message */
            SystemMessager.sendSystemMessage(conn, e);
            throw e;
        } catch (SQLException e) {
            logger.error(e);
            /* Send system message */
            SystemMessager.sendSystemMessage(conn, e);
            throw e;
        } catch (Exception e) {
            logger.error(e);
            /* Send system message */
            SystemMessager.sendSystemMessage(conn, e);
            throw e;
        } finally {
            if(lockObtained) {
                if(logger.isDebugEnabled()) {
                    logger.debug("releasing lock...");
                }
                lockUtil.releaseLock(ARConstants.LOCK_ENTITY_TYPE,ARConstants.LOCK_ENTITY_ID,ARConstants.LOCK_CSR_ID,sessionId);
            }
            if(conn != null && !conn.isClosed()) {
                conn.close();
            }
            if(logger.isDebugEnabled()){
                logger.debug("Exiting invoke");
            } 
        }
    }
   
    private void dispatchJDEEvent()
        throws NamingException, JMSException, Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering notifyDispatcher");
        }

        try{
            MessageToken messageToken = new MessageToken();
            messageToken.setStatus("EOD_DISPATCHER");       
            Dispatcher dispatcher = Dispatcher.getInstance();

            // enlist the JMS transaction
            dispatcher.dispatchTextMessage(new InitialContext(), messageToken); 
        } finally
        {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting dispatchMessage");
            }
        }
    }    

    /**
     * Retrieve the date time for which the batch is run. This is calculated from current date
     * and a configurable offset value.
     * 
     * @param eodStartTime
     * @param payload
     * @return 
     * @throws java.lang.Exception
     */
      private long getEODBatchTime(long eodStartTime, String payload) throws Exception {
         long eodBatchTime = 0;
         String eodBatchDateOffset = null;
         ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
         
         if (payload != null && payload.equalsIgnoreCase(ARConstants.BAMS_CC_AUTH_PROVIDER)){
        	 eodBatchDateOffset = configUtil.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, ARConstants.BAMS_BATCH_OFFSET_DATE);
        	 logger.debug("BAMS eodBatchDateOffset is " + eodBatchDateOffset);
         }else if ((payload != null && payload.equalsIgnoreCase(ARConstants.PAYMENT_GATEWAY_EOD))){
        	 eodBatchDateOffset = configUtil.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, ARConstants.PG_EOD_BATCH_OFFSET_DATE);
        	 logger.debug("PG EOD eodBatchDateOffset is " + eodBatchDateOffset);
         }	 
         else{
        	 eodBatchDateOffset = ConfigurationUtil.getInstance().getProperty(ARConstants.CONFIG_FILE, ARConstants.CONST_EOD_BATCH_DATE_OFFSET);
        	 logger.debug("Global Payments eodBatchDateOffset is " + eodBatchDateOffset);
         }
         logger.debug("eodBatchDateOffset is " + eodBatchDateOffset);
         Calendar batchDate = Calendar.getInstance();
         batchDate.setTimeInMillis(eodStartTime);
         batchDate.add(Calendar.DATE, new Integer(eodBatchDateOffset).intValue());
       
         eodBatchTime = batchDate.getTime().getTime();
         logger.debug("eodBatchTime is " + eodBatchTime);
         return eodBatchTime;
     }  
     
     private void dispatchEndOfDayJobs(String payload) throws Exception {
         ConfigurationUtil configUtil = null;
     
         configUtil = ConfigurationUtil.getInstance();
         String dispatchJDE = null;
         String dispatchBAMS = null;
         String dispatchPayPal = null;
         String dispatchAPReport = null;
         String dispatchBillMeLater = null;
         String dispatchAafes = null;
         String dispatchUA = null;
         String uaDelay = null;
         String dispatchGiftCard = null;
         
         
         String dispatchPG = null;
         String dispatchPGPayPal =null;
         
         if (payload != null && payload.equalsIgnoreCase(ARConstants.BAMS_CC_AUTH_PROVIDER)){
        	 //run BAMS End of Day only
        	 dispatchJDE = "N";
             dispatchBAMS = configUtil.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, ARConstants.DISPATCH_BAMS_EOD_FLAG);
             dispatchPayPal = "N";
             dispatchAPReport = "N";
             dispatchBillMeLater = "N";
             dispatchAafes = "N";
             dispatchUA = "N";
             uaDelay = "N";
             dispatchGiftCard = "N";
             dispatchPG ="N";
             dispatchPGPayPal ="N";
         }
         else if (payload != null && payload.equalsIgnoreCase(ARConstants.PAYMENT_GATEWAY_EOD))
         {	
        	 //run Payment Gateway End of Day Jobs (Credit Card and Paypal).
        	 dispatchJDE = "N";
        	 dispatchBAMS = "N";
        	 dispatchPayPal = "N";
             dispatchAPReport = "N";
             dispatchBillMeLater = "N";
             dispatchAafes = "N";
             dispatchUA = "N";
             uaDelay = "N";
             dispatchGiftCard = "N";
             dispatchPG = configUtil.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, ARConstants.DISPATCH_PG_EOD_FLAG);
             dispatchPGPayPal = configUtil.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, ARConstants.DISPATCH_PG_PAYPAL_EOD_FLAG);
         } 
         else{
        	 //run all non-bams End of Day jobs
        	 dispatchJDE = configUtil.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, ARConstants.DISPATCH_JDE_EOD_FLAG);
             dispatchBAMS = "N";
             dispatchPG ="N";
             dispatchPGPayPal ="N";
             dispatchPayPal = configUtil.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, ARConstants.DISPATCH_PAYPAL_EOD_FLAG);
             dispatchAPReport = configUtil.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, ARConstants.DISPATCH_ALTPAY_REPORT_FLAG);
             dispatchBillMeLater = configUtil.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, ARConstants.DISPATCH_BILLMELATER_EOD_FLAG);
             dispatchAafes = configUtil.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, ARConstants.DISPATCH_AAFES_EOD_FLAG);
             dispatchUA = configUtil.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, ARConstants.DISPATCH_UA_EOD_FLAG);
             uaDelay = configUtil.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, ARConstants.DISPATCH_UA_EOD_DELAY);
             dispatchGiftCard = configUtil.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, ARConstants.DISPATCH_GIFT_CARD_EOD_FLAG);
             	 
         }
         
         logger.info("dispatch JDE flag=" + dispatchJDE);
         logger.info("dispatch BAMS flag=" + dispatchBAMS);
         logger.info("dispatch pay pal flag=" + dispatchPayPal);
         logger.info("dispatch AP reprot flag=" + dispatchAPReport);
         logger.info("dispatch BillMeLater flag=" + dispatchBillMeLater);
         logger.info("dispatch UA flag=" + dispatchUA);
         logger.info("dispatch AAFES flag=" + dispatchAafes);
         logger.info("dispatch Gift Card flag=" + dispatchGiftCard);
         logger.info("dispatch PG CC flag=" + dispatchPG); //Credit Card
         logger.info("dispatch PG PayPal flag=" + dispatchPGPayPal); //PayPal
          
         
         if (ARConstants.COMMON_VALUE_YES.equals(dispatchJDE)) {
            dispatchJDEEvent();
         }
         
         if (ARConstants.COMMON_VALUE_YES.equals(dispatchBAMS)) {
        	 dispatchBAMSEvent();
          }
         
         if (ARConstants.COMMON_VALUE_YES.equals(dispatchPG)) {
        	 dispatchPGEvent();
          }
         
         if (ARConstants.COMMON_VALUE_YES.equals(dispatchPGPayPal)) {
        	 dispatchPGAltPayEvent(AltPayConstants.PG_PAYPAL,0);
          }
         
         // Dispatch message "ap_report" to queue ALTPAY_EOD with delay.
         if (ARConstants.COMMON_VALUE_YES.equals(dispatchAPReport)) {
             String reportDelay = configUtil.getFrpGlobalParm
                 (ARConstants.CONFIG_CONTEXT, AltPayConstants.ALTPAY_REPORT_DELAY);
             dispatchAltPayEvent(AltPayConstants.PAYPAL_REPORT_PAYLOAD, Long.valueOf(reportDelay));
         }           
         
         // Dispatch bm_eod to ALTPAY_EOD queue.
         if (ARConstants.COMMON_VALUE_YES.equals(dispatchBillMeLater)) {
             dispatchAltPayEvent(AltPayConstants.BILLMELATER_EOD_PAYLOAD, 0);
         }

         // Dispatch ua_eod to ALTPAY_EOD queue. No need to check connection as most orders
         // should have settled in OP if not all.
         // Run UA EOD with delay to get around United nightly backup.
         if (ARConstants.COMMON_VALUE_YES.equals(dispatchUA)) {
             dispatchAltPayEvent(AltPayConstants.UA_EOD_PAYLOAD, Long.valueOf(uaDelay));
         }
         
         // Dispatch gd_eod to ALTPAY_EOD queue.
         if (ARConstants.COMMON_VALUE_YES.equals(dispatchGiftCard)) {
             dispatchAltPayEvent(AltPayConstants.GIFT_CARD_EOD_PAYLOAD, 0);
         }
         
         // Dispatch AAFES-EOD-PROCESS to EVENTS queue.
         if (ARConstants.COMMON_VALUE_YES.equals(dispatchAafes)) {
            //AccountingUtil.queueToEventsQueue(ARConstants.EVENT_CONTEXT, ARConstants.EVENT_AAFES_EOD, "0", null);         
            dispatchAltPayEvent(AltPayConstants.AAFES_EOD_PAYLOAD, 0);
         }
         
         // Try PayPal connection. Dispatch pp_eod to ALTPAY_EOD queue if connected.
         if (ARConstants.COMMON_VALUE_YES.equals(dispatchPayPal)) {
             String timerCallbackFlag = configUtil.getFrpGlobalParm
                      (ARConstants.CONFIG_CONTEXT,AltPayConstants.PP_CONNECT_TIMER_FLAG);
            // String retryInterval = configUtil.getFrpGlobalParm
              //       (ARConstants.CONFIG_CONTEXT,AltPayConstants.PP_CONNECT_TIMER_MS);
             
             logger.info("Timer callback flag=" + timerCallbackFlag);

             if(ARConstants.COMMON_VALUE_YES.equals(timerCallbackFlag)) {
                 // Use Timer Callback service to test PayPal connection and start PP EOD
                 // Obtain the EJB service interface.
                 Context jndiContext = new InitialContext();
                 TimerServiceSessionEJBLocalHome  home = (TimerServiceSessionEJBLocalHome)
                     jndiContext.lookup("java:comp/env/ejb/local/TimerServiceSessionEJB");
                 
                 TimerServiceSessionEJBLocal tsejb = home.create();
                 
                 if(!tsejb.isRunning(ARConstants.TIMER_PP_CONNECTION_TESTER)) {
                     logger.info("Starting timer service to test PayPal Connection...");
                     String beanName = "ppConnectionTimerData";
                     ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("spring-ejb-context.xml");        
                     PayPalConnectionTimerData pptd = (PayPalConnectionTimerData)ctx.getBeanFactory().getBean(beanName);
                     pptd.init();
                     tsejb.start(pptd);  
                 } else {
                     logger.info(ARConstants.TIMER_PP_CONNECTION_TESTER + " timer is already running.");
                 }
             } else {
                 dispatchAltPayEvent(AltPayConstants.PAYPAL_EOD_PAYLOAD, 0);
             }
         }
     }
     
     
     /**
      * Dispatch messages to PG_ALTPAY_EOD JMS queue.
      * @param payload
      * @param delay
      * @throws Exception
      */
      private void dispatchPGAltPayEvent(String payload, long delay) throws Exception {
          List<String> messageList  = new ArrayList<String>();
          messageList.add(payload);
          AccountingUtil.dispatchJMSMessageList(AltPayConstants.PG_ALTPAY_EOD_QUEUE, messageList, 0, delay);
      }

	/**
     * Dispatch messages to ALTPAY_EOD JMS queue.
     * @param payload
     * @param delay
     * @throws Exception
     */
     private void dispatchAltPayEvent(String payload, long delay) throws Exception {
         List<String> messageList  = new ArrayList<String>();
         messageList.add(payload);
         AccountingUtil.dispatchJMSMessageList(AltPayConstants.ALTPAY_EOD_QUEUE, messageList, 0, delay);
     }
     
     private void dispatchBAMSEvent() throws NamingException, JMSException, Exception {
	     if(logger.isDebugEnabled()) {
	         logger.debug("Entering notifyDispatcher in dispatchBAMSEvent()");
	     }
	     try {
	         MessageToken messageToken = new MessageToken();
	         messageToken.setStatus("BAMS_EOD_DISPATCHER");       
	         Dispatcher dispatcher = Dispatcher.getInstance();
	
	         // enlist the JMS transaction
	         dispatcher.dispatchTextMessage(new InitialContext(), messageToken); 
	     } finally {
	         if(logger.isDebugEnabled()) {
	            logger.debug("Exiting dispatchMessage");
	         }
	     }
     }  
     
	/** Dispatch messages to PG_EOD_DISPATCHER JMS queue.
	 * @throws NamingException
	 * @throws JMSException
	 * @throws Exception
	 */
	private void dispatchPGEvent() throws NamingException, JMSException, Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("Entering in dispatchPGEvent()");
		}
		try {
			MessageToken messageToken = new MessageToken();
			messageToken.setStatus("PG_EOD_DISPATCHER");
			Dispatcher dispatcher = Dispatcher.getInstance();

			// en-queue the JMS transaction
			dispatcher.dispatchTextMessage(new InitialContext(), messageToken);
		} finally {
			if (logger.isDebugEnabled()) {
				logger.debug("Exiting dispatchPGEvent");
			}
		}
	}
     
}