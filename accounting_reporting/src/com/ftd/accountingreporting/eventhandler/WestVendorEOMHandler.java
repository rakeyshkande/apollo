package com.ftd.accountingreporting.eventhandler;

import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.dao.CommonDAO;
import com.ftd.accountingreporting.dao.VendorBatchDAO;
import com.ftd.accountingreporting.util.EODUtil;
import com.ftd.accountingreporting.util.FileArchiveUtil;
import com.ftd.accountingreporting.util.FtpUtil;
import com.ftd.accountingreporting.util.LockUtil;
import com.ftd.accountingreporting.util.SystemMessager;
import com.ftd.accountingreporting.vo.VendorJdeVO;
import com.ftd.eventhandling.events.EventHandler;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

import java.math.BigDecimal;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;


/**
 * Class that creates west vendor month end file and send to remote server.
 * 1. create eom file
 * 2. send file to remote server
 * 3. update next file name and next record number
 */

public class WestVendorEOMHandler extends EventHandler
{
    private static Logger logger  = new Logger("com.ftd.accountingreporting.eventhandler.WestVendorEOMHandler");
    private static final String zero8 = "00000000";
    private static final String zero10 = "0000000000";
    private static final String space15 = "               ";
    private static final String maxAmount = "999999.99";
    private static final int PAD_RIGHT = 0; 
    private static final int PAD_LEFT = 1; 
    
    
   /**
    * This method is overridden method from eventhandler
    * @param payload
    * @throws java.lang.Throwable
    */
    public void invoke(Object payload) throws Throwable
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering invoke");
        }

        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        LockUtil lockUtil = null;
        String sessionId = new Integer(this.hashCode()).toString();
        boolean lockObtained = false;
        Connection conn = EODUtil.createDatabaseConnection();
        VendorBatchDAO dao = new VendorBatchDAO(conn);
      
        lockUtil = new LockUtil(conn);
        // lock process 
        lockObtained = lockUtil.obtainLock(ARConstants.WEST_VENDOR_EOM_LOCK_ENTITY_TYPE,
                ARConstants.WEST_VENDOR_EOM_LOCK_ENTITY_ID,ARConstants.WEST_VENDOR_EOM_LOCK_CSR_ID,sessionId);
        if (lockObtained) {
            String auditLogInserted = configUtil.getProperty(
                ARConstants.CONFIG_FILE,ARConstants.WEST_VENDOR_AUDIT_LOG_INSERTED_FLAG);
            Date runDate = new Date(this.getRunDateTime());
                
            if(logger.isDebugEnabled()) {
                logger.debug("obtained lock...");
            }
            
            try {
                this.archiveFiles();
                if(auditLogInserted.equals("N")) {
                    try {      
                        // insert jde audit log
                        dao.insertWestJdeAuditLog(runDate);
                    } catch (Exception e) {
                        logger.error(e);
                        sendSystemMessage(conn, e);
                        throw e;
                    }
                }
                
                try {               
                    // create jde file
                    File jdeFile = this.createJdeFile(dao, runDate);
                    
                    // send jde file
                    this.sendFileToRemoteServer(jdeFile);
                } catch (Exception e) {
                    // Send system message. Do not throw.
                    logger.error(e);
                    sendSystemMessage(conn, e);
                }
				
          } catch (Exception e) {
              throw e;
          } finally {
            if(lockObtained) {
                if(logger.isDebugEnabled()) {
                    logger.debug("releasing lock...");
                }
                //lockUtil.releaseLock(ARConstants.LOCK_ENTITY_TYPE,ARConstants.LOCK_ENTITY_ID,ARConstants.LOCK_CSR_ID,sessionId);
                lockUtil.releaseLock(ARConstants.WEST_VENDOR_EOM_LOCK_ENTITY_TYPE,ARConstants.WEST_VENDOR_EOM_LOCK_ENTITY_ID,ARConstants.WEST_VENDOR_EOM_LOCK_CSR_ID,sessionId);
            }
            if(conn != null && !conn.isClosed()) {
                conn.close();
            }
            if(logger.isDebugEnabled()){
                logger.debug("Exiting invoke WestVendorEOMHandler");
            } 
          }
        } else {
            sendSystemMessage(conn, new Exception("Cannot obtain west vendor eom lock"));
        }
  
    }
    
  /**
   * Retrieve the date time for which the batch is run. This is calculated from current date
   * and a configurable offset value.
   * 
   * @return 
   * @throws java.lang.Exception
   */
    private long getRunDateTime() throws Exception {
       long runDateTime = 0;
       String runDateOffset = ConfigurationUtil.getInstance().getProperty(ARConstants.CONFIG_FILE, ARConstants.WEST_VENDOR_EOM_RUNDATE_OFFSET);
       Calendar runDate = Calendar.getInstance();
       runDate.setTimeInMillis(System.currentTimeMillis());
       runDate.add(Calendar.DATE, new Integer(runDateOffset).intValue());
            
       runDateTime = runDate.getTime().getTime();
       logger.debug("runDateTime is " + runDateTime);
       return runDateTime;
   }    
    

  /**
   * Move files to the archive directory
   * @throws java.lang.Exception
   */
    private void archiveFiles() throws Exception
    {
        logger.debug("Entering archiveFiles...");
        String fromDir = ConfigurationUtil.getInstance().getFrpGlobalParm(
                ARConstants.CONFIG_CONTEXT,ARConstants.VENDOR_EOM_LOCAL_LOCATION);
        String toDir = ConfigurationUtil.getInstance().getFrpGlobalParm(
                ARConstants.CONFIG_CONTEXT,ARConstants.VENDOR_EOM_ARCHIVE_LOCATION);
                
        FileArchiveUtil.moveToArchive(fromDir, toDir, false);
        logger.debug("Exiting archiveFiles...");
    }    
    
  /**
   * Create the billing file.
   * @param dao
   * @param runDate
   * @return 
   * @throws java.lang.Exception
   */
    private File createJdeFile(VendorBatchDAO dao, Date runDate) throws Exception {
        List jdeDataList = getJdeData(dao, runDate);
        CommonDAO commonDao = new CommonDAO(dao.getConnection());

        String jdeNextFileNo = commonDao.doGetGlobalParamValue(ARConstants.VENDOR_JDE_FTP_NEXT_FILENO);
        String jdeFilePath = ConfigurationUtil.getInstance().getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, ARConstants.VENDOR_EOM_LOCAL_LOCATION)
                       + jdeNextFileNo;

        String serialNo = commonDao.doGetGlobalParamValue(ARConstants.VENDOR_JDE_FTP_NEXT_SERIAL_NO);
        int lineNumber = Integer.parseInt(serialNo);
        
        VendorJdeVO jdeVo = null;
        File eomFile = null;
        FileWriter out = null; 
        
        // write the data out
        try {
            eomFile = new File(jdeFilePath);
            out = new FileWriter(eomFile);
        } catch (FileNotFoundException fe) {
            logger.error("File Exception  = " + fe);
            throw fe;
        }
  
        for (Iterator i=jdeDataList.iterator(); i.hasNext();) {
  
            jdeVo = (VendorJdeVO) i.next();  
  
            try {
              out.write(jdeVo.getVendorCode());
              out.write(jdeVo.getFTDVendorCodeNo());
              out.write(jdeVo.getFTDMemberNo());
              out.write(jdeVo.getLineType());
              out.write(jdeVo.getBillingDate());
              out.write(jdeVo.getShipDate());
              out.write(jdeVo.getVendorOrderNo());
              out.write(jdeVo.getMemberRefNo());
              out.write(jdeVo.getVendorItemNo());
              out.write(jdeVo.getVendorItemDesc1());
              out.write(jdeVo.getVendorItemDesc2());
              out.write(jdeVo.getQtyShipped());
              out.write(jdeVo.getPaymentTerms());
              out.write(jdeVo.getUnitPriceUSD());
              out.write(jdeVo.getUnitCostUSD());
              out.write(jdeVo.getUnitPriceCAD());
              out.write(jdeVo.getUnitCostCAD());
              out.write(jdeVo.getDebitCreditInd());
                
              // defect 3438
              jdeVo.setSerialNo(EODUtil.pad(String.valueOf(lineNumber++),PAD_LEFT,"0",8));
              out.write(jdeVo.getSerialNo());
          
              out.write('\n');
            } catch (IOException ie) {
              logger.error("out Exception e = " + ie);
              throw ie;
            }
        } // end loop 
          
        try {
            out.close();
        } catch (IOException ie) {
            System.out.println("close Exception e = " + ie);
        }
        // update next file no
        String fileNo = EODUtil.replace(jdeNextFileNo,"u","");
        int newFileNo = Integer.parseInt(fileNo) + 1;
        String newFile = "u" + EODUtil.pad(Integer.toString(newFileNo), PAD_LEFT, "0", 5);
        commonDao.doUpdateGlobalParms(ARConstants.VENDOR_JDE_FTP_NEXT_FILENO, newFile,"VENUS_EOM");
      
        // update serial no
        commonDao.doUpdateGlobalParms(ARConstants.VENDOR_JDE_FTP_NEXT_SERIAL_NO, String.valueOf(lineNumber),"VENUS_EOM");
        return eomFile;
    }
    
    /**
     * Retrieve a list of orders and adjustments grouped by vendor
     * @param dao
     * @param runDate
     */
    private List getJdeData(VendorBatchDAO dao, Date runDate) throws Exception {
      ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
      CachedResultSet jdeCredits = dao.getWestJdeCredits(runDate);
      CachedResultSet jdeAdj = dao.getWestJdeAdjustments(runDate);
      
      List jdeData = new ArrayList();
      String vendorCode = configUtil.getProperty(ARConstants.CONFIG_FILE, ARConstants.WEST_VENDOR_EOM_VENDOR_CODE);
      String ftdVendorCodeNo = configUtil.getProperty(ARConstants.CONFIG_FILE, ARConstants.WEST_VENDOR_EOM_FTD_VENDOR_CODE_NO);
      String lineType = configUtil.getProperty(ARConstants.CONFIG_FILE, ARConstants.WEST_VENDOR_EOM_LINE_TYPE);
      String qtyShipped = configUtil.getProperty(ARConstants.CONFIG_FILE, ARConstants.WEST_VENDOR_EOM_QUANTITY_SHIPPED);
      String vendorItemNo = configUtil.getProperty(ARConstants.CONFIG_FILE, ARConstants.WEST_VENDOR_EOM_CREDIT_ITEM_NO);
      String vendorItemNoAdj = configUtil.getProperty(ARConstants.CONFIG_FILE, ARConstants.WEST_VENDOR_EOM_ADJ_ITEM_NO);
      String vendorDesc1 = configUtil.getProperty(ARConstants.CONFIG_FILE, ARConstants.WEST_VENDOR_EOM_CREDIT_DESC);
      String vendorDesc1Adj = configUtil.getProperty(ARConstants.CONFIG_FILE, ARConstants.WEST_VENDOR_EOM_ADJ_DESC);
      //String desc1 = "Gift Orders Delivered";
      //String desc1Adj = "Gift Adjustments and Credits";
      String companyCode = "";

      BigDecimal totalCredits = new BigDecimal(0.00);
      BigDecimal totalAdj = new BigDecimal(0.00);
      int totalCreditCount = 0;
      int totalAdjCount = 0;
      
      //Calculate Billing Month - this should be end of month.
      GregorianCalendar gc = new GregorianCalendar();
      gc.setTime(runDate);
      int lastDayOfMonth = gc.getActualMaximum(gc.DAY_OF_MONTH);
      String billingDate = String.valueOf(gc.get(Calendar.YEAR)) 
        + EODUtil.pad(String.valueOf(gc.get(Calendar.MONTH)+1),PAD_LEFT,"0",2)
        + EODUtil.pad(String.valueOf(lastDayOfMonth),PAD_LEFT,"0",2);
        
      
      if(jdeCredits != null) {
          logger.debug("Writing to file...");
          VendorJdeVO jdeVo = null;
          
          while(jdeCredits.next())
          {
              jdeVo = new VendorJdeVO();
        
              jdeVo.setVendorCode(vendorCode); 
              jdeVo.setFTDVendorCodeNo(ftdVendorCodeNo);
              jdeVo.setFTDMemberNo(EODUtil.pad(jdeCredits.getString("accounts_payable_id"),PAD_LEFT,"0",8));
              jdeVo.setLineType(lineType);
              jdeVo.setBillingDate(billingDate);
              jdeVo.setShipDate(zero8);
              jdeVo.setVendorOrderNo(space15);
              companyCode = jdeCredits.getString("company");
              if (companyCode == null) {
                  companyCode = "";
              }
              
              jdeVo.setMemberRefNo(EODUtil.pad(companyCode,PAD_RIGHT," ",15));
              jdeVo.setVendorItemNo(EODUtil.pad(vendorItemNo,PAD_RIGHT," ",15));
              jdeVo.setVendorItemDesc1(EODUtil.pad(vendorDesc1,PAD_RIGHT," ",30));
              
              totalCreditCount += Integer.parseInt(jdeCredits.getString("count"));
              logger.debug("totalCreditCount = " +totalCreditCount);
              jdeVo.setVendorItemDesc2(EODUtil.pad(jdeCredits.getString("gl_account_number"),PAD_RIGHT," ",30));
              jdeVo.setQtyShipped(qtyShipped);
              jdeVo.setPaymentTerms("0  ");
              totalCredits = totalCredits.add(jdeCredits.getBigDecimal("amount"));
              logger.debug("totalCredits = " +totalCredits);
              jdeVo.setUnitPriceUSD(zero8);
              jdeVo.setUnitCostUSD(EODUtil.replace(String.valueOf((jdeCredits.getBigDecimal("amount").abs()).setScale(2,BigDecimal.ROUND_HALF_UP)),".", ""));
              jdeVo.setUnitPriceCAD(zero10);
              jdeVo.setUnitCostCAD(zero10);
              if ((jdeCredits.getBigDecimal("amount")).signum() == -1) {
                jdeVo.setDebitCreditInd("-");
              } else {
                jdeVo.setDebitCreditInd(" "); 
              }
               
              // add fedExVo to list
              this.addJdeVoToList(jdeData, jdeVo);
          }
      }
      
      if(jdeAdj != null) {
          VendorJdeVO jdeVo = null;
          while (jdeAdj.next()) {
              jdeVo = new VendorJdeVO();
              // set jdeVo field values for JDE adjustments and cancels
              
              jdeVo.setVendorCode(vendorCode); 
              jdeVo.setFTDVendorCodeNo(ftdVendorCodeNo);
              jdeVo.setFTDMemberNo(EODUtil.pad(jdeAdj.getString("accounts_payable_id"),PAD_LEFT,"0",8));
              jdeVo.setLineType(lineType);
              jdeVo.setBillingDate(billingDate);
              jdeVo.setShipDate(zero8);
              jdeVo.setVendorOrderNo(space15);
              companyCode = jdeAdj.getString("company");
              if (companyCode == null) {
                  companyCode = "";
              }
      
              jdeVo.setMemberRefNo(EODUtil.pad(companyCode,PAD_RIGHT," ",15));
              jdeVo.setVendorItemNo(EODUtil.pad(vendorItemNoAdj,PAD_RIGHT," ",15));
              jdeVo.setVendorItemDesc1(EODUtil.pad(vendorDesc1Adj,PAD_RIGHT," ",30));
              logger.debug("count = "+ Integer.parseInt(jdeAdj.getString("count")));
              totalAdjCount += Integer.parseInt(jdeAdj.getString("count"));
              logger.debug("totalAdjCount = " +totalAdjCount);
              jdeVo.setVendorItemDesc2(EODUtil.pad(jdeAdj.getString("gl_account_number"),PAD_RIGHT," ",30));
              jdeVo.setQtyShipped(qtyShipped);
              jdeVo.setPaymentTerms("0  ");
              
              totalAdj = totalAdj.add(jdeAdj.getBigDecimal("amount"));
              logger.debug("totalAdj = " +totalAdj);
              jdeVo.setUnitPriceUSD(zero8);
              jdeVo.setUnitCostUSD(EODUtil.replace(String.valueOf((jdeAdj.getBigDecimal("amount").abs()).setScale(2,BigDecimal.ROUND_HALF_UP)),".", ""));
              jdeVo.setUnitPriceCAD(zero10);
              jdeVo.setUnitCostCAD(zero10);
              if ((jdeAdj.getBigDecimal("amount")).signum() == -1) {
                jdeVo.setDebitCreditInd("-"); 
              } else {
                jdeVo.setDebitCreditInd(" ");
              }

              // add fedExVo to list
              this.addJdeVoToList(jdeData, jdeVo);
          } 
      }
      
      
      return jdeData;
   }

   
    
  /**
   * Sends a system message
   * @param con
   * @param t Throwable t
   */
    private void sendSystemMessage(Connection con, Throwable t)
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering sendSystemMessage");
        }
        try {
            String logMessage = t.getMessage();  
      
            SystemMessager.send(logMessage, t, ARConstants.EOD_BILLING_PROCESS, 
                SystemMessager.LEVEL_PRODUCTION, 
                ARConstants.EOD_BILLING_PROCESS_ERROR_TYPE, con);
        
        } catch (Exception ex) {
            logger.error(ex);
        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting sendSystemMessage");
            } 
        }   
    }
    
    private void sendFileToRemoteServer(File jdeFile) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering sendFileToRemoteServer");
        }
        
        try{        
            FtpUtil ftpUtil = new FtpUtil();
            
            /* retrieve server name to FTP to */
            String ftpServer = ConfigurationUtil.getInstance().getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, ARConstants.VENDOR_EOM_FTP_SERVER);
            String ftpLocation = ConfigurationUtil.getInstance().getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, ARConstants.VENDOR_EOM_FTP_LOCATION);
            String username = ConfigurationUtil.getInstance().getSecureProperty(ARConstants.SECURE_CONFIG_CONTEXT, ARConstants.VENDOR_EOM_FTP_USERNAME);
            String password = ConfigurationUtil.getInstance().getSecureProperty(ARConstants.SECURE_CONFIG_CONTEXT, ARConstants.VENDOR_EOM_FTP_PASSWORD);
            
            String fileName = jdeFile.getName();
            
            try {
                ftpUtil.login(ftpServer, username, password);
                
                fileName = ftpLocation + "/" + fileName;
                ftpUtil.putFile(jdeFile, fileName, true);                
            } finally {
                ftpUtil.logout();
            }
        }
        finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting sendFileToRemoteServer");
            } 
        }

    } 
    
    /**
     * Recursively add a VendorJdeVo to list if amount > $999,999.99
     * @param dataList list to add vo to
     * @param vo object to check if $ is >= 1M
     * 
    **/
    private void addJdeVoToList(List dataList, VendorJdeVO vo) {
        String unitCost = vo.getUnitCostUSD();
        String maxAmtStr = EODUtil.replace(maxAmount, ".", "");
        
        if(unitCost != null && !unitCost.equals("")) {
            BigDecimal unitCostBD = (new BigDecimal(unitCost)).movePointLeft(2);
            logger.debug("unitCost is " + unitCostBD.doubleValue());
            if(unitCostBD.compareTo(new BigDecimal(maxAmount)) <= 0) {
                vo.setUnitCostUSD(EODUtil.pad(EODUtil.replace(unitCostBD.toString(),".",""),PAD_LEFT, "0", 8));
                dataList.add(vo);
            } else {
                // add a line with $999,999.99
                VendorJdeVO fullAmtLineVo = (VendorJdeVO)vo.clone();
                
                fullAmtLineVo.setUnitCostUSD(maxAmtStr);
                dataList.add(fullAmtLineVo);
                
                VendorJdeVO extraLineVo = (VendorJdeVO)fullAmtLineVo.clone();
                BigDecimal remainingUnitCost = unitCostBD.add(new BigDecimal("-" + maxAmount)); 
                logger.debug("remainingUnitCost is " + remainingUnitCost.doubleValue());
                String remainingUnitCostStr = EODUtil.replace(remainingUnitCost.toString(),".", "");
                extraLineVo.setUnitCostUSD(remainingUnitCostStr);
                
                // recursively add the lines with the remaining amount.
                addJdeVoToList(dataList, extraLineVo);
            }
        }

    }
}