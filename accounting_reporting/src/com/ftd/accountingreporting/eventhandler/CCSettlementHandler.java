package com.ftd.accountingreporting.eventhandler;

import com.enterprisedt.net.ftp.FTPException;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.dao.CommonDAO;
import com.ftd.accountingreporting.dao.ReconDAO;
import com.ftd.accountingreporting.exception.EntityLockedException;
import com.ftd.accountingreporting.util.AccountingUtil;
import com.ftd.accountingreporting.util.FileArchiveUtil;
import com.ftd.accountingreporting.util.FileCompressionUtil;
import com.ftd.accountingreporting.util.FtpUtil;
import com.ftd.accountingreporting.util.LockUtil;
import com.ftd.accountingreporting.util.SystemMessager;
import com.ftd.accountingreporting.util.security.FileProcessorPgp;
import com.ftd.accountingreporting.vo.CCSettlementVO;
import com.ftd.eventhandling.events.EventHandler;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;

import java.io.InputStreamReader;

import java.sql.Connection;

import java.text.SimpleDateFormat;

import java.util.Date;

public class CCSettlementHandler extends EventHandler
{
    private Logger logger = 
        new Logger("com.ftd.accountingreporting.eventhandler.CCSettlementHandler");
    private Connection conn = null;
    
    public CCSettlementHandler()
    {
        super();
    }

   /**
    * This is the main invocation class. It does the following:
    * �	Obtain process lock.
    * �	Use FTPUtil to retrieve CCDETAIL from the FTP server. 
    *   If file not found, send system message.
    * �	Process the file. For each line in the file,
    *   o	Extract Credit Card Number from position 14-38.
    *   o	Extract Amount from position 45-51
    *   o	Extract Transaction date from position 94-99.
    *   o	Extract Authorization Number from position 14-38.
    *   o	Retrieve the payment record from FTD.com�s system using these 
    *       criteria: credit card number, amount, transaction date, 
    *       authorization number.
    *       ?	If not found, create a record in CC_SETTLEMENT with 
    *           RECON_DISP_CODE �NO MATCH FOUND�.
    *       ?	If the returned payment amount does not match amount
    *           reconciled, create a CC_SETTLEMENT record with RECON_DISP_CODE
    *           �PRICE VARIANCE�.
    *       ?	If the amount matches, update the corresponding payment 
    *           (order_bills) as settled with the date of the file processing.
    * �	Release process lock.
    */
    public void invoke(Object payload) throws Throwable
    {
        if(logger.isDebugEnabled()) {            
            logger.debug("***********************************************************");
            logger.debug("* Start CC Settlement Processing");
            logger.debug("***********************************************************");
        }
        
        
        LockUtil lockUtil = null;
        String sessionId = "";
        boolean lockObtained = false;
        Date curDate = new Date(System.currentTimeMillis());
        
        try{
            /* retrieve connection to database */
            conn = DataSourceUtil.getInstance().
                getConnection(ConfigurationUtil.getInstance().getProperty(
                ARConstants.CONFIG_FILE,ARConstants.DATASOURCE_NAME));            
            ReconDAO reconDAO = new ReconDAO(conn);
            /* obtain process lock */
            lockUtil = new LockUtil(conn);
            sessionId = String.valueOf(this.hashCode());
            lockObtained = lockUtil.obtainLock(
                ARConstants.CC_RECON_ENTITY_TYPE_REMITTANCE,
                ARConstants.CC_RECON_ENTITY_ID_REMITTANCE, 
                ARConstants.CC_RECON_CSR_ID, sessionId); 
            
            if(lockObtained) {
                // archive old files.
                archiveFiles();
                
                /* Use FTPUtil to retrieve CBVENUS from the FTP server */
                ByteArrayOutputStream ccFile = getCCReconFile();
                if(ccFile != null){
                    ByteArrayInputStream in = new ByteArrayInputStream(ccFile.toByteArray());
                    BufferedReader input = new BufferedReader(new InputStreamReader(in));
                    String line = null;
                    SimpleDateFormat sdfOutput = 
                           new SimpleDateFormat ("MM/dd/yy");
                    int lineNumber = 0;
                    CommonDAO commonDAO = new CommonDAO(conn);
                        
                    while (( line = input.readLine()) != null){
                      lineNumber++;
                      try {  
                        if(!line.equals("")){
                            /* Extract Mercury Order Number */      
                            String ccNumber = line.subSequence(13,38).toString().trim();
                            /* Extract amount reconciled */
                            double amountReconciled = Double.parseDouble(
                                line.subSequence(44,49).toString() + "." + 
                                line.subSequence(49,51).toString());               
                            
                            /* Extract Transaction date */
                            String transactionDate = line.subSequence(93,95).toString() + "/" + 
                                line.subSequence(95, 97).toString() + "/" + line.subSequence(97, 99).toString();
                            Date transDate = sdfOutput.parse(transactionDate);
                            
                            /* Extract Authorization Number */
                            String authNumber = line.subSequence(63,69).toString();
                            authNumber = authNumber==null? "" : authNumber.trim();
                            
                            /*Retrieve the payment record from FTD.com�s system using these 
                            * criteria: credit card number, amount, transaction date, 
                            * authorization number. */
                            CCSettlementVO settlementVO = new CCSettlementVO();
                            settlementVO.setAmount(amountReconciled);
                            settlementVO.setAuthNumber(authNumber);
                            settlementVO.setCCNumber(ccNumber);
                            settlementVO.setTransactionDate(transDate);
                            CachedResultSet payment = reconDAO.doFindSettlementPayment(settlementVO);
                    
                            if(payment.getRowCount() > 0){
                                String paymentID = "";
                                while (payment.next())
                                {
                                   paymentID = payment.getString("payment_id");
                                   System.out.println("line number is " + lineNumber);
                                   logger.debug("Updating line " + lineNumber + " with payment id: " + paymentID);
                                   System.out.println("line number is " + lineNumber);
                                   commonDAO.doUpdatePaymentBillStatus(
                                   Long.parseLong(paymentID), 
                                   curDate.getTime(),"Settled");
                                }      
                            } else {
                                logger.debug("Line " + lineNumber + " not found");
                                settlementVO.setReconDispCode("NO MATCH FOUND");
                                reconDAO.doInsertCCSettlement(settlementVO);
                            }
                        }
                      } catch (Exception e) {
                        logger.error("Cannot process line:" + line, e);
                        logError(e, "");
                      }
                    }
    
                    /* close FileReader and BufferedReader before deleting file */
                    input.close();
                    ccFile.close();
                    //ccFile.delete();
                    
                    // mask cc number in the local file. Skip first 0 lines, and the last 0 line. 
                    // Start from position 14 on the rest of each line.
                    //FileArchiveUtil.overwriteFileContents(ccFile.getPath(), 0, 0, 13, ARConstants.CC_MASK);
                }
                else
                {
                    int deadLine = Integer.parseInt(
                        ConfigurationUtil.getInstance().getProperty(
                        ARConstants.CONFIG_FILE,"RECON_CC_DEADLINE"));
                    String recheckInterval = ConfigurationUtil.getInstance().getProperty(
                        ARConstants.CONFIG_FILE,"RECON_FILE_RECHECK_INTERVAL");
                    AccountingUtil acctUtil = new AccountingUtil();
                    boolean deadlinePassed = acctUtil.checkProcessDeadline(deadLine,"CC Settlement");
                    if(deadlinePassed == false)
                    {
                        /* requeue message */
                        acctUtil.queueToEventsQueue(ARConstants.EVENT_CONTEXT, "RECON-CC", recheckInterval, null);
                    } else {
                            logError(new Exception("No file found"), "");
                    }
                    
                }
            } else {
                logger.error(new EntityLockedException(ARConstants.CC_RECON_CSR_ID));
            }
        } catch (Exception e) {
            // Do not rethrow. Force dequeue.
            logError(e,"");
            
        } finally {
            /* Release process lock.  */
            if(lockUtil != null){
                if(lockObtained == true){
                    lockUtil.releaseLock(
                        ARConstants.CC_RECON_ENTITY_TYPE_REMITTANCE,
                        ARConstants.CC_RECON_ENTITY_ID_REMITTANCE, 
                        ARConstants.CC_RECON_CSR_ID, sessionId);
                }
            }
            
            /* close db connection */
            if(conn != null && !conn.isClosed()) {
                  logger.debug("Closing connection...");
                  conn.close();
            }
            
            if(logger.isDebugEnabled()){
                logger.debug("***********************************************************");
                logger.debug("* CC Settlement Processing Completed");
                logger.debug("***********************************************************");
            } 
        }
    }

    /**
     * Send an error message via the system messenger 
     * @param errorMessage - String
     * @return n/a
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     * @throws SystemMessengerException
     */
    private void logError(Throwable exception, String errorMessage) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering logError");
        }
        
         try{
            
            /* log message to log4j log file */
            logger.error(errorMessage,exception);
            
            /* send a system message */
            SystemMessager sysMessager = new SystemMessager();
            sysMessager.send(errorMessage,exception,
                ARConstants.CC_RECON_PROCESS,SystemMessager.LEVEL_DEBUG,
                ARConstants.CC_RECON_PROCESS_ERROR_TYPE,conn);
        
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting logError");
            } 
       } 
        
    }
    /**
     * This method utilizes the FTD Utility to retrieve the CCDETAIL file
     * @return File
     * @throws Exception
     */
    private ByteArrayOutputStream getCCReconFile() throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering getCCReconFile");
        }
    
        ByteArrayOutputStream ccRecon = null;
        try{        
            /* retrieve server name to FTP to */
            String ftpServer = ConfigurationUtil.getInstance().getFrpGlobalParm( 
                ARConstants.CONFIG_CONTEXT, "ccSettlementFtpServer");
            String ftpLocation = ConfigurationUtil.getInstance().getFrpGlobalParm(
                ARConstants.CONFIG_CONTEXT, "ccSettlementFtpLocation");
            String username = ConfigurationUtil.getInstance().getSecureProperty(
                ARConstants.SECURE_CONFIG_CONTEXT, "ccSettlementFtpUsername");
            String password = ConfigurationUtil.getInstance().getSecureProperty(
                ARConstants.SECURE_CONFIG_CONTEXT, "ccSettlementFtpPassword");
            String remoteFile = ConfigurationUtil.getInstance().getProperty(
                ARConstants.CONFIG_FILE, "ccSettlementFileName");
            String localFileDirectory = ConfigurationUtil.getInstance().getFrpGlobalParm(
                ARConstants.CONFIG_CONTEXT, "CC_SETTLEMENT_LOCAL_LOCATION"); 
            String securityPrivateKeyPath = ConfigurationUtil.getInstance().getFrpGlobalParm(
                ARConstants.CONFIG_CONTEXT, "securityPrivateKeyPath");
            String securityPrivateKeyPassphrase = ConfigurationUtil.getInstance().getSecureProperty(
                ARConstants.SECURE_CONFIG_CONTEXT, "securityPrivateKeyPassphrase");
                
            //remoteFile = ftpLocation + "/" + remoteFile;
            
            FtpUtil ftpUtil = new FtpUtil();
            /* log into FTP Server */
            ftpUtil.login(ftpServer, ftpLocation, username, password);
            
            /* get the file from the FTP server and store it locally with the same name
            and a time stamp appended to the name */
            //Date d = new Date();
            //SimpleDateFormat sdfOutput = new SimpleDateFormat ("MMddyy");

            String localFile = localFileDirectory + remoteFile;
            try {
                ftpUtil.getFile(localFileDirectory,remoteFile,true);
            } catch(FTPException e) {
                ftpUtil.logout();
                return null;
            }

            /* log off of FTP Server */
            ftpUtil.logout();
            
            /* Defect 1069: Decrypt the settlement file */
            ccRecon = FileProcessorPgp.decryptToStream(localFile, securityPrivateKeyPath,
            securityPrivateKeyPassphrase);
            
            /* archive the local file */
            //FileCompressionUtil fileUtil = new FileCompressionUtil();
            //fileUtil.createGZipFile(localFile,localFile + sdfOutput.format(d) + ".gzip");
            
        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting getCCReconFile");
            } 
        }
        
        return ccRecon;
    }
    
  /**
   * Move files to the archive directory
   * @throws java.lang.Exception
   */
    private void archiveFiles() throws Exception
    {
        String fromDir = ConfigurationUtil.getInstance().getFrpGlobalParm(
                ARConstants.CONFIG_CONTEXT,"CC_SETTLEMENT_LOCAL_LOCATION");;
        String toDir = ConfigurationUtil.getInstance().getFrpGlobalParm(
                ARConstants.CONFIG_CONTEXT,"CC_SETTLEMENT_ARCHIVE_LOCATION");
        FileArchiveUtil.moveToArchive(fromDir, toDir, true);
    }    
}