package com.ftd.accountingreporting.eventhandler;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.dao.AmazonDAO;
import com.ftd.accountingreporting.util.AccountingUtil;
import com.ftd.accountingreporting.util.LockUtil;
import com.ftd.accountingreporting.util.SystemMessager;
import com.ftd.eventhandling.events.EventHandler;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.exception.SystemMessengerException;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.text.ParseException;

import java.util.Date;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

public class AmazonEodHandler extends EventHandler
{

    private Logger logger =
        new Logger("com.ftd.accountingreporting.eventhandler.AmazonEodHandler");

    Connection conn = null;

    public AmazonEodHandler()
    {
        super();
    }

    /**
     * This is the invocation method for the event handler.
     * The payload is the JMS payload. Not used in this case.
     * This method will:
     * 1.	Obtain lock
     *      a.	Get AmazonConstants.LOCK_ENTITY_TYPE_EOD
     *      b.	get AmazonConstants.LOCK_ENTITY_ID_EOD
     *      c.	Get AmazonConstants.CSR_ID
     *      d.	get session id from the hash code of this object.
     *          Set it to the class variable sessionId.
     *      e.	Call LockUtil.obtainLock. set return value to class
     *          variable obtainedLock
     * 2.	Call Process()
     * 3.	Release lock. Call LockUtil.releaseLock.
     * @param payload - Object
     * @return n/a
     * @throws Throwable
     * @todo - code and determine exceptions
     */
    public void invoke(Object payload) throws Throwable
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering invoke");
        }

        boolean lockObtained = false;
        LockUtil lockUtil = null;
        String sessionId = "";

        try{
            /* retrieve connection to database */
            conn = DataSourceUtil.getInstance().
                getConnection(ConfigurationUtil.getInstance().getProperty(
                ARConstants.CONFIG_FILE,ARConstants.DATASOURCE_NAME));

            /* setup session id */
            sessionId = String.valueOf(this.hashCode());

            /* obtain process lock */
            lockUtil = new LockUtil(conn);
            lockObtained = lockUtil.obtainLock(
                ARConstants.AMAZON_LOCK_ENTITY_TYPE_EOD,
                ARConstants.AMAZON_LOCK_ENTITY_ID_EOD,
                ARConstants.AMAZON_LOCK_CSR_ID, sessionId);

            /* analyze the remittance records */
            if(lockObtained) {
                process();
            }

       } catch (SQLException e) {
          // Do not rethrow. Force dequeue.
          logError(e,"");
       } catch (TransformerException e) {
          // Do not rethrow. Force dequeue.
          logError(e,"");
       } catch (IOException e) {
          // Do not rethrow. Force dequeue.
          logError(e,"");
       } catch (SAXException e) {
          // Do not rethrow. Force dequeue.
          logError(e,"");
       } catch (ParserConfigurationException e) {
          // Do not rethrow. Force dequeue.
          logError(e,"");
       } catch (ParseException e) {
          // Do not rethrow. Force dequeue.
          logError(e,"");
       } catch (Exception e) {
          // Do not rethrow. Force dequeue.
          logError(e,"");
       } catch (Throwable e) {
          // Do not rethrow. Force dequeue.
          logError(e,"");
       } finally {

        if(lockObtained == true)
        {
            /* Release process lock */
            lockUtil.releaseLock(
                ARConstants.AMAZON_LOCK_ENTITY_TYPE_EOD,
                ARConstants.AMAZON_LOCK_ENTITY_ID_EOD,
                ARConstants.AMAZON_LOCK_CSR_ID,sessionId);
        }
        if(conn != null && !conn.isClosed()) {
            conn.close();
        }
        if(logger.isDebugEnabled()){
           logger.debug("Exiting invoke");
        }
       }
    }

    /**
     * This method marks Amazon orders as Billed.
     * @param n/a
     * @return n/a
     * @throws IOException
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws TransformerException
     * @throws XSLException
     * @throws ParseException
     * @throws SQLException
     * @throws Exception
     * @todo - code and determine exceptions
     */
    private void process() throws IOException, SAXException,
        ParserConfigurationException, TransformerException,
        ParseException, SQLException, Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering process");
        }

        try{

            /* retrieve date offset */
            int batchDateOffset = Integer.parseInt(ConfigurationUtil.getInstance().getProperty(
                ARConstants.CONFIG_FILE, ARConstants.AMAZON_BATCH_DATE_OFFSET));
            AccountingUtil accountUtil = new AccountingUtil();
            Date processDate = accountUtil.calculateOffSetDate(batchDateOffset);

            // Set Unbilled Amazon orders to Billed.
            AmazonDAO amazonDao = new AmazonDAO(conn);
            amazonDao.doBillAmazonOrders(processDate, ARConstants.ACCTG_STATUS_BILLED);

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting process");
            }
       }
    }

    /**
     * Send an error message via the system messenger
     * @param errorMessage - String
     * @return n/a
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     * @throws SystemMessengerException
     */
    private void logError(Throwable exception, String errorMessage) throws SAXException,
        ParserConfigurationException, IOException, SQLException,
        SystemMessengerException, TransformerException
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering logError");
        }

         try{

            /* log message to log4j log file */
            logger.error(errorMessage,exception);

            /* send a system message */
            SystemMessager sysMessager = new SystemMessager();
            sysMessager.send(errorMessage,exception,
                ARConstants.AMAZON_EOD_PROCESS,SystemMessager.LEVEL_DEBUG,
                ARConstants.AMAZON_EOD_PROCESS_ERROR_TYPE,conn);

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting logError");
            }
       }

    }
}