package com.ftd.accountingreporting.eventhandler;

import java.sql.Connection;

import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.dao.GiftCertificateCouponDAO;
import com.ftd.accountingreporting.util.SystemMessager;
import com.ftd.eventhandling.events.EventHandler;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.GiftCodeUtil;
import com.ftd.osp.utilities.RESTUtils;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * This Class is an event handler which checks for expiration date on coupons for status 'Issued Active'. 
 * If current date plus grace period is greater than or equal to expiration date then set the status to 'Issued Expired'.
 * 
 */

public class GCCStatusUpdateHandler extends EventHandler
{
    private static Logger logger  = new Logger("com.ftd.accountingreporting.eventhandler.GCCStatusUpdateHandler");
    //private Connection conn;
    private GiftCertificateCouponDAO gccDAO;
    
    /**
     * constructor for GCCStatusUpdateHandler class
     */
    public GCCStatusUpdateHandler()
    {
       super();
    }
    
   /**
    * This method is overridden method from eventhandler
    * @param payLoad
    * @throws java.lang.Throwable
    */
    public void invoke(Object payLoad) throws Throwable
    {
       Connection conn = null;
       logger.debug("GCCStatusUpdateHandler - Inside Invoke");
       try {
       
    	   String type = RESTUtils.getConfigParamValue("ACTIVE_TO_ISSUEDEXPIRED", false);
       		if(null == type){
       		logger.error("Type of Maintenance is null, Check Global Params");
       		}
           //DataSource ds = (DataSource) super.lookupResource("jdbc/EVENTS_QDS");
           //conn = ds.getConnection();      
          /* conn = DataSourceUtil.getInstance().getConnection("ACCOUNTING");
           gccDAO = new GiftCertificateCouponDAO(conn);
           Calendar inDate = Calendar.getInstance();
           inDate.add(Calendar.DATE,(-1) * getGracePeriod());
           
           
           String DATE_FORMAT = "yyyy-MM-dd";
           SimpleDateFormat sdf = new java.text.SimpleDateFormat(DATE_FORMAT);
           String aDate = sdf.format(inDate.getTime());
           
           logger.debug(aDate);
           logger.debug("GCCStatusUpdateHandler - After getting Connection");*/
       		GiftCodeUtil.gcMaintenance(type);     
           //boolean result = gccDAO.doUpdateGccToExpire(inDate);
           logger.debug("GCCStatusUpdateHandler - Leaving Invoke");
       } catch (Throwable t) {
          // Do not rethrow. Force dequeue.
          logger.error(t);
          try {
              SystemMessager.sendSystemMessage(conn, "GCCStatusUpdateHandler", t);
          } catch (Exception e) {
              String errMsg = "Unable to send message to support pager.";
              logger.fatal(errMsg,e);
          }
       } finally {
          if(conn != null && !conn.isClosed()) {
              logger.debug("Closing connection...");
              conn.close();
          }
       }
    }
    
    /**
     * gets the grace period from global param
     * @return int
     * @throws java.lang.Exception
     */
    private int getGracePeriod() throws Exception
    {
       int gracePeriodint = 0;
       String gracePeriod = gccDAO.doGetGlobalParamValue(ARConstants.CONST_GRACE_PERIOD);
       
       // Take into consideration when the job is run. Offset is -1 if coupon
       // expirer runs at the beginning of the next day.
       String gracePeriodOffset = ConfigurationUtil.getInstance().getProperty(ARConstants.CONFIG_FILE, ARConstants.CONST_GRACE_PERIOD_OFFSET);
       int gp = Integer.parseInt(gracePeriod) - Integer.parseInt(gracePeriodOffset);
       gracePeriod = String.valueOf(gp);
       
       if (gracePeriod != null)
           gracePeriodint  = Integer.valueOf(gracePeriod).intValue();
       return gracePeriodint;
    }
}