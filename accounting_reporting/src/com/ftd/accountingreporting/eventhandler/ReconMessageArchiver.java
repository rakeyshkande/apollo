package com.ftd.accountingreporting.eventhandler;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.dao.ReconDAO;
import com.ftd.accountingreporting.util.AccountingUtil;
import com.ftd.accountingreporting.util.SystemMessager;
import com.ftd.eventhandling.events.EventHandler;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import java.sql.Connection;
import java.util.Date;
import org.xml.sax.SAXException;

/**
 * This class archives reconciliation messages that are in �RECONCILED� or
 * �PRICE VARIANCE� status which is ready to be archived. Extends 
 * com.ftd.eventhandler.events.EventHandler. This job is scheduled to run daily.
 * @author Charles Fox
 */
public class ReconMessageArchiver extends EventHandler
{
    private Logger logger = 
        new Logger("com.ftd.accountingreporting.eventhandler.ReconMessageArchiver");
    private Connection conn = null;
    public ReconMessageArchiver()
    {
        super();
    }
    
    /**
     * This is the main invocation class. It does the following:
     * �	Retrieve MESG_ARCHIVE_NUM_MONTHS from the config file. 
     *      This is the number of months the reconciliation messages live 
     *      before they are archived. It coincides with the number of months 
     *      the mercury order number is likely to roll.
     * �	Calculate the cut off time by subtracting MESG_ARCHIVE_NUM_MONTHS 
     *      from current system time.
     * �	Archive the reconciliation messages that are in �RECONCILED� or 
     *      �PRICE VARIANCE� status that are created before the cut off time.
     * @param payload - Object
     * @return n/a
     * @throws Throwable
     * @todo - code and determine exceptions
     */
    public void invoke(Object payload) throws Throwable 
    {
        if(logger.isDebugEnabled()) {            
            logger.debug("***********************************************************");
            logger.debug("* Start Archive Recon Message Processing");
            logger.debug("***********************************************************");
        }
        
        String archiveNumMonths = ConfigurationUtil.getInstance().getProperty(
                ARConstants.CONFIG_FILE,ARConstants.MESG_ARCHIVE_NUM_MONTHS);
        String archiveNumDays = ConfigurationUtil.getInstance().getProperty(
                ARConstants.CONFIG_FILE,ARConstants.MESG_ARCHIVE_NUM_DAYS);
        AccountingUtil acctUtil = new AccountingUtil();
        
        try{
            /* calculate cutoff_date */
            int archiveMonths = 0;
            int archiveDays = 0;
            Date cutoff = new Date();
            
            // Use archive number of months if it's configured; Otherwise use 
            // the number of days configured.
            if(archiveNumMonths != null && !"".equals(archiveNumMonths)) {
                archiveMonths = Integer.parseInt(archiveNumMonths);
                cutoff = acctUtil.calculateOffSetDatebyMonth(archiveMonths); 
            } else {
                archiveDays = Integer.parseInt(archiveNumDays);
                cutoff = acctUtil.calculateOffSetDate(archiveDays);
            }

            /* retrieve connection to database */
            conn = DataSourceUtil.getInstance().
                getConnection(ConfigurationUtil.getInstance().getProperty(
                ARConstants.CONFIG_FILE,ARConstants.DATASOURCE_NAME));            
                
            /* Archive the reconciliation messages */
            ReconDAO recon = new ReconDAO(conn);
            recon.doArchiveReconMesg(cutoff);

        } catch (Exception e) {
            // Do not rethrow. Force dequeue.
            logError(e,"");
            
        } finally {
            if(conn != null && !conn.isClosed()) {
                logger.debug("Closing connection...");
                conn.close();
            }
            if(logger.isDebugEnabled()){
                logger.debug("***********************************************************");
                logger.debug("* Archive Recon Message Processing Completed");
                logger.debug("***********************************************************");
            } 
        }
    }
    
    /**
     * Send an error message via the system messenger 
     * @param errorMessage - String
     * @return n/a
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     * @throws SystemMessengerException
     */
    private void logError(Throwable exception, String errorMessage) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering logError");
        }
        
         try{
            
            /* log message to log4j log file */
            logger.error(errorMessage,exception);
            
            /* send a system message */
            SystemMessager sysMessager = new SystemMessager();
            sysMessager.send(errorMessage,exception,
                ARConstants.RECON_MESSAGE_ARCHIVE_PROCESS,SystemMessager.LEVEL_DEBUG,
                ARConstants.RECON_MESSAGE_ARCHIVE_PROCESS_ERROR_TYPE,conn);
        
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting logError");
            } 
       } 
        
    }
}