package com.ftd.accountingreporting.eventhandler;

import java.io.File;
import java.io.FileWriter;
import java.net.InetAddress;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.dao.EODDAO;
import com.ftd.accountingreporting.exception.PTSFieldValidationException;
import com.ftd.accountingreporting.exception.RemoteFileExistsException;
import com.ftd.accountingreporting.pts.bo.SettleCreditCardsBO;
import com.ftd.accountingreporting.pts.validator.PTSFieldValidator;
import com.ftd.accountingreporting.util.EODUtil;
import com.ftd.accountingreporting.util.FileArchiveUtil;
import com.ftd.accountingreporting.util.LockUtil;
import com.ftd.accountingreporting.util.PTSUtil;
import com.ftd.accountingreporting.util.SftpUtil;
import com.ftd.accountingreporting.util.SystemMessager;
import com.ftd.accountingreporting.util.security.FileProcessorPgp;
import com.ftd.accountingreporting.vo.EODFileVO;
import com.ftd.accountingreporting.vo.EODMessageVO;
import com.ftd.accountingreporting.vo.EODRecordVO;
import com.ftd.eventhandling.events.EventHandler;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.GiftCodeUtil;
import com.ftd.osp.utilities.RESTUtils;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.xml.DOMUtil;

/**
 * Class that performs BAMS EOD finalizing tasks after all carts have been processed:
 * 1. create EOD file
 * 2. send file to BAMS
 * 3. mask local file
 */

public class BAMSEndOfDayFinalizer extends EventHandler {
	
	private static Logger logger = new Logger(BAMSEndOfDayFinalizer.class.getName());
    
	private final static String BAMS_LOCK_ISSUE = "BAMS LOCK";
	private final static String BILLING_FILE_VALIDATION_FAILED = "BILLING FILE VALIDATION FAILED"; 
	private final static String FTD_PTS_ENCRYPTION_FAILED = "FTD PTS FILE ENCRYPTION FAILED";
	private final static String BAM_PTS_ENCRYPTION_FAILED = "BAMS PTS FILE ENCRYPTION FAILED";
	private final static String BAM_FILE_TRANSFER_FAILED = "BAMS FTP FILE TRANSFER FAILED";
	
	
    
	/**
	 * This method is overridden method from event handler
	 * 
	 * @param payLoad
	 * @throws java.lang.Throwable
	 */
	public void invoke(Object payload) throws Throwable {

		if (logger.isDebugEnabled()) {
			logger.debug("invoked BAMSEndOfDayFinalizer");
		}

		ConfigurationUtil configUtil = ConfigurationUtil.getInstance();

		long checkCartCountInterval = Long.parseLong(configUtil.getProperty(ARConstants.CONFIG_FILE, ARConstants.EOD_CART_CNT_CHK_INTERVAL));
		long checkCartCountChangeInterval = Long.parseLong(configUtil.getProperty(ARConstants.CONFIG_FILE, ARConstants.EOD_CART_CNT_CHANGE_CHK_INTERVAL));
		long fileSendRetryInterval = Long.parseLong(configUtil.getProperty(ARConstants.CONFIG_FILE, ARConstants.EOD_FILE_SEND_RETRY_INTERVAL));
		int fileSendRetryCount = Integer.parseInt(configUtil.getProperty(ARConstants.CONFIG_FILE, ARConstants.EOD_FILE_SEND_RETRY_COUNT));

		String securityPublicKeyPath = configUtil.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, ARConstants.BAMS_PUBLIC_KEY_PATH);
		String ptsInternalFtpServer = configUtil.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, ARConstants.PTS_INTERNAL_FTP_SERVER);
		String ptsInternalFtpLocation = configUtil.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, ARConstants.PTS_INTERNAL_FTP_LOC);
		String ptsInternalUsername = configUtil.getSecureProperty(ARConstants.SECURE_CONFIG_CONTEXT, ARConstants.PTS_INTERNAL_FTP_USER);
		String ptsInternalPassword = configUtil.getSecureProperty(ARConstants.SECURE_CONFIG_CONTEXT, ARConstants.PTS_INTERNAL_FTP_PWD);

		String ftdSecurityPublicKeyPath = configUtil.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, ARConstants.FTD_PUBLIC_KEY_PATH);	
		String ftdcopyPTSLocation = configUtil.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, ARConstants.BAMS_PTS_SETTLEMENT_FTDCOPY_LOC);
		
		List<File> encryptedFilesForBams = new ArrayList<File>(); 

		int curFileSendRetryCount = 0;
		long curCheckCartCountInterval = 0;
		int recoveryRecCount = 0;
		int lastRecoveryRecCount = 0;

		LockUtil lockUtil = null;
		Connection conn = null;		
		boolean lockObtained = false;
		EODDAO dao = null;
		EODMessageVO eodMessageVO = null;
		
		String sessionId = new Integer(this.hashCode()).toString();

		try {

			conn = EODUtil.createDatabaseConnection();
			dao = new EODDAO(conn);
			lockUtil = new LockUtil(conn);
						
			// lock process
			lockObtained = lockUtil.obtainLock(
					ARConstants.BAMS_LOCK_ENTITY_TYPE, ARConstants.BAMS_LOCK_ENTITY_ID, ARConstants.BAMS_LOCK_CSR_ID, sessionId);

			if (!lockObtained) {
				SystemMessager.sendBAMSEODSystemMessage(conn, BAMS_LOCK_ISSUE, "Cannot obtain BILLING_EOD lock in BAMSEndOfDayFinalizer.");
				return;
			}

			logger.debug("obtained lock succesfully");
			eodMessageVO = this.extractMessage((MessageToken) payload);

			// archive old files.
			this.archiveFiles();

			// perform other tasks
			try {
				Calendar batchDate = EODUtil.convertLongDate(eodMessageVO.getBatchTime());
				performCompletionTasks(batchDate, conn);
			} catch (Exception e) {
				logger.error(e);
				throw e;
			}

			String companies = configUtil.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, ARConstants.PTS_SETTLEMENT_COMPANIES);
			String[] companyArr = null;
			
			if(!StringUtils.isEmpty(companies)) {
				companyArr = companies.split(ARConstants.COMMA_SEPERATOR);
			} else {
				logger.error("No company configured to process the BAMSEndOfDayFinalizer request");
				return;
			}
			
			StringBuffer validationResult = new StringBuffer();
			while (true) {
				// Check if eod_recovery table is empty. Proceed with final steps if so.
				recoveryRecCount = dao.doCountRecoveryRec(ARConstants.BAMS_CC_AUTH_PROVIDER);

				if (recoveryRecCount == 0) {

					List<CachedResultSet> aList = dao.doGetBillingDetails(ARConstants.BAMS_CC_AUTH_PROVIDER);
					CachedResultSet rsBilled = (CachedResultSet) aList.get(0);
					CachedResultSet rsBatchNumbers = (CachedResultSet) aList.get(1);

					for (String company : companyArr) {
						
						if (StringUtils.isEmpty(company)) {
							continue;
						}
						StringBuffer companyValidationResult = new StringBuffer();
						curFileSendRetryCount = 0;
						EODFileVO eodFileVO = new EODFileVO();
						eodFileVO.setStartTime(eodMessageVO.getStartTime());
						eodFileVO.setBatchNumber(eodMessageVO.getBatchNumber());
						eodFileVO.setBatchTime(eodMessageVO.getBatchTime());
						eodFileVO.setCompanyId(company.trim());

						File billingFile = null;
						File encryptedFile = null; 						
						
						// compile file
						try {
		                    billingFile = this.createBillingFile(eodFileVO, dao, rsBilled, companyValidationResult);
						} catch (Exception e) {
							StringBuffer errorMessage = new StringBuffer("Unable to create the billing file for batch number: ")
							.append(eodFileVO.getBatchNumber()).append(", for company: ").append(eodFileVO.getCompanyId()).append(e.getMessage());							
							logger.error("Detailed error message: ", e); 
							throw new Exception(errorMessage.toString()); //  for system message not to be repeated
						}
						
						if(!companyValidationResult.toString().isEmpty()) {							
							validationResult.append("PTS Settlement file validation errors for the company: " + company + "\n");
							validationResult.append(companyValidationResult);							
						} 
						
						// Defect QE-3, Create a FTD copy of encrypted billing file. Any exception caught here should not stop the actual process.
	                    try {	                    	
	                    	logger.info("Creating FTD copy of encrypted file...");
							FileProcessorPgp.encrypt(billingFile.getPath(), ftdcopyPTSLocation, billingFile.getName(), ftdSecurityPublicKeyPath, FileProcessorPgp.ENCRYPTION_EXTN.PGP, false);								
							
						} catch(Exception e) {
							logger.error("Creating FTD copy of encrypted file failed for the reason, ", e);
							SystemMessager.sendBAMSEODSystemMessage(conn, FTD_PTS_ENCRYPTION_FAILED, "Creating FTD copy of encrypted file failed for the reason, " + e.getMessage());
						}			
												
						try { 
                    		// Defect 1069: Encrypt billing file
							encryptedFile = FileProcessorPgp.encrypt(billingFile.getPath(), securityPublicKeyPath, false, false, true);
							encryptedFilesForBams.add(encryptedFile);								

						} catch (RemoteFileExistsException rfe) {
							logger.error("Remote file exists, exception occured ", rfe);
							SystemMessager.sendBAMSEODSystemMessage(conn, BAM_PTS_ENCRYPTION_FAILED, rfe.getMessage());
						} catch (Exception e) {
							logger.error("Error occured while encrypting file.", e);
							SystemMessager.sendBAMSEODSystemMessage(conn, BAM_PTS_ENCRYPTION_FAILED, e.getMessage());
						}
												
						// mask CC number in the local file. This will be done in the D record only. Start from position 2 of each D Record line.
						logger.debug("Masking Billing File: " + billingFile);
						FileArchiveUtil.overwriteBAMSFileContent(billingFile.getPath(), 1, ARConstants.CC_MASK, "D");
					}

					// update batch processed indicator.
					if(!validationResult.toString().isEmpty()) {
						logger.error(validationResult.toString());
						SystemMessager.sendBAMSEODSystemMessage(conn, BILLING_FILE_VALIDATION_FAILED, validationResult.toString());												
					}
					updateProcessedIndicator(dao, rsBatchNumbers);
										
					break;

				} else if (curCheckCartCountInterval >= checkCartCountChangeInterval) {
					curCheckCartCountInterval = 0;
					if (recoveryRecCount == lastRecoveryRecCount) {
						// Counts in eod_recovery has not changed in checkCartCountInterval. Delete them from table
						dao.doDeleteRecoveryTab(ARConstants.BAMS_CC_AUTH_PROVIDER);
					}
					lastRecoveryRecCount = recoveryRecCount;
				} else {
					// Sleep for some configurable time interval.
					Thread.sleep(checkCartCountInterval);
					curCheckCartCountInterval += checkCartCountInterval;
				}

			}

			int fileTransferResult = 0;

			while (true && encryptedFilesForBams != null && encryptedFilesForBams.size() > 0 && validationResult.toString().isEmpty()) {

				SftpUtil sftpUtil = new SftpUtil();

				try {
					sftpUtil.uploadFiles(encryptedFilesForBams, ptsInternalFtpLocation, ptsInternalFtpServer, ptsInternalUsername, ptsInternalPassword);
					fileTransferResult = 0;
				} catch (Exception e) {
					logger.error("Error occured while uploading Settlement files ", e);
					fileTransferResult = -2;
				}

				if (fileTransferResult == 0) {
					logger.debug("Settlement files successfully transfered to ftp server.");
					break;

				} else if (fileTransferResult == -2 && curFileSendRetryCount > fileSendRetryCount) {
					StringBuffer errorMessage = new StringBuffer(
							"Connection to server failed after trying for ")
							.append((curFileSendRetryCount * fileSendRetryInterval) / 60000)
							.append(" minutes. Please Manually transfer file settlement files from ")
							.append(InetAddress.getLocalHost().getHostName())
							.append(" to ").append(ptsInternalFtpServer)
							.append(".");
					SystemMessager.sendBAMSEODSystemMessage(conn, BAM_FILE_TRANSFER_FAILED, errorMessage.toString());
					break;
				} else {
					Thread.sleep(fileSendRetryInterval);
				}

				curFileSendRetryCount++;
			}

		} catch (Exception e) {
			logger.error("Error occured while calling invoke method.", e);
			SystemMessager.sendBAMSEODSystemMessage(conn, "FAILED", e.getMessage());

		} finally {
			if (lockObtained) {
				if (logger.isDebugEnabled()) {
					logger.debug("releasing lock...");
				}
				lockUtil.releaseLock(ARConstants.BAMS_LOCK_ENTITY_TYPE,
						ARConstants.BAMS_LOCK_ENTITY_ID,
						ARConstants.BAMS_LOCK_CSR_ID, sessionId);
			}

			if (conn != null && !conn.isClosed()) {
				conn.close();
			}

			if (logger.isDebugEnabled()) {
				logger.debug("Exiting invoke in BAMSEndOfDayFinalizer");
			}
		}
	}
    

  /**
   * Move files to the archive directory
   * @throws java.lang.Exception
   */
    private void archiveFiles() throws Exception
    {
        logger.debug("BAMSEndOfDayFinalizer entering archiveFiles...");
        String fromDir = ConfigurationUtil.getInstance().getProperty(
                ARConstants.CONFIG_FILE,ARConstants.CONST_PTS_FILE_LOCATION);
        String toDir = ConfigurationUtil.getInstance().getProperty(
                ARConstants.CONFIG_FILE,ARConstants.CONST_PTS_ARCHIVE_LOCATION);
                
        FileArchiveUtil.moveToArchive(fromDir, toDir, false);
        logger.debug("BAMSEndOfDayFinalizer exiting archiveFiles...");
    }    
    
  /**
   * Create the billing file.
   * @param eodFileVO
   * @param eodDAO
   * @return 
   * @throws java.lang.Exception
   */
    private File createBillingFile(EODFileVO eodFileVO, EODDAO eodDAO, CachedResultSet rsBilled, StringBuffer validationResult) throws Exception {
      File eodFile = null;
      FileWriter out = null; 
      String runDatetime = eodFileVO.getRunDateTime();
      long timestamp = System.currentTimeMillis();
      int recordSeq = 1;
      
      logger.debug("BAMSEndOfDayFinalizer writing to file...");
      long billingHeaderId = eodFileVO.getBatchNumber();
      String classId = ConfigurationUtil.getInstance().getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, "DEPOSIT_CLASS_ID");
      
      String fileName = ConfigurationUtil.getInstance().getProperty(ARConstants.CONFIG_FILE, ARConstants.CONST_PTS_FILE_NAME);
      String ptsFileName = eodFileVO.getPTSFileName().replace(fileName, classId + "." + fileName); //new naming convention.
      eodFile = new File( ptsFileName + "_" + eodFileVO.getBatchNumber() + "_" + runDatetime);
      out = new FileWriter(eodFile);
      
      //write first line
      if(rsBilled != null) {
          
               
          //out.write(eodFileVO.getFirstLine()+newLine);
          //out.write(eodFileVO.getHeaderRecordType()+EODUtil.pad(new Long(eodFileVO.getBatchNumber()).toString(),PAD_LEFT,"0",6)+EODUtil.pad(eodFileVO.getDisplayBatchDate(),PAD_LEFT," ",6)+newLine);
  
    	  EODRecordVO vo = null;
          double totalSalesAmount = 0;
          double totalRefundAmount = 0;
          //int settlementCount = 0;
          int lineCount = 0;
          
          SettleCreditCardsBO settleCCBO = new SettleCreditCardsBO();
          
          // Validate global params
          try {
        	  logger.debug("Validating global parameters for company id: " + eodFileVO.getCompanyId());
        	  PTSFieldValidator.getInstance().validateGlobalParams(eodFileVO.getCompanyId());
          } catch(PTSFieldValidationException e) {
        	  validationResult.append(e.getMessage());
          }
          /*
          * SP-72: BAMS - Additional validations for fields in settlement file.
          * Introducing checks on record lengths, with this release. Reference to validationResult is passed to respective method 
          * to add the results of length checks on records to it.
          */    
          recordSeq = settleCCBO.writeHeaderRecords(eodFileVO.getCompanyId(), recordSeq, out, validationResult);
          ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
          String companies = configUtil.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, ARConstants.PTS_SETTLEMENT_COMPANIES);
          String notFTD = companies.substring(4);
          String [] companyArr = notFTD.split(",");
          rsBilled.reset();
          while(rsBilled.next())
          {
        	  StringBuffer fieldValidationResult = new StringBuffer();
        	   String companyId = rsBilled.getString("company_id");
        	   boolean flag = false;
        	   for (String company : companyArr) {
        		   if(company != null){
        			   company = company.trim();
        			   if (company.equalsIgnoreCase(companyId)){
        				   flag = true;
        			   }
           		   }
        		}
        	   
        	   if(!flag){
        		   companyId = "FTD";
        	   }
        	   logger.debug("Company Id : "+ companyId+ " and eodFileVO is : "+eodFileVO.getCompanyId());
        	   if (companyId != null && companyId.equalsIgnoreCase(eodFileVO.getCompanyId()))
	        	   {        		   
	               vo = new EODRecordVO();
	               
	               vo.setLineItemType(rsBilled.getString("bill_type"));
	               vo.setOrderNumber(rsBilled.getString("order_number"));
	               vo.setOrderSequence(rsBilled.getLong("order_sequence"));
	               vo.setCreditCardType(rsBilled.getString("cc_type"));
	               vo.setCreditCardNumber(rsBilled.getString("cc_number"));
	               vo.setCcExpDate(rsBilled.getString("cc_expiration"));
	               vo.setOrderAmount(rsBilled.getDouble("order_amount"));
	               vo.setSalesTaxAmount(new Double(rsBilled.getDouble("sales_tax_amount")).toString());
	               vo.setTransactionType(rsBilled.getString("transaction_type"));
	               if (rsBilled.getDate("auth_date")!= null)
	                   vo.setAuthorizationDate(EODUtil.convertSQLDate((rsBilled.getDate("auth_date"))));
	               vo.setApprovalCode(rsBilled.getString("auth_approval_code"));
	               vo.setAcqReferenceNumber(rsBilled.getString("acq_reference_number"));
	               vo.setAuthCharIndicator(rsBilled.getString("auth_char_indicator"));
	               vo.setAuthTranId(rsBilled.getString("auth_transaction_id"));
	               vo.setAuthValidationCode(rsBilled.getString("auth_validation_code"));
	               vo.setAuthSourceCode(rsBilled.getString("auth_soure_code"));
	               vo.setAVSResultCode(rsBilled.getString("avs_code"));
	               vo.setResponseCode(rsBilled.getString("response_code"));
	               vo.setclearingMemberNumber(rsBilled.getString("clearing_member_number"));
	               vo.setRecipientZip(rsBilled.getString("recipient_zip_code"));
	               vo.setOriginType(rsBilled.getString("origin_type"));
	               vo.setCardHolderRefNumber(rsBilled.getString("cardholder_ref"));
	               vo.setAddField1(rsBilled.getString("add_info1"));
	               vo.setAddField2(rsBilled.getString("add_info2"));
	               vo.setAddField3(rsBilled.getString("add_info3"));
	               vo.setAddField4(rsBilled.getString("add_info4"));
	               vo.setWalletIndicator(rsBilled.getLong("wallet_indicator"));
	               vo.setVoiceAuthFlag(rsBilled.getString("is_voice_auth"));
	               vo.setInitialAuthAmount(rsBilled.getDouble("initial_auth_amount"));
	               vo.setPaymentId(rsBilled.getLong("payment_id"));
	               vo.setCscResponseCode(rsBilled.getString("csc_response_code"));
	               vo.setCompanyId(rsBilled.getString("company_id"));
	               vo.setCardinalVerifiedFlag(rsBilled.getString("cardinal_verified_flag"));
	               /*bo.setRecordVO(vo);
	               bo.setLineItemType();*/
	        
	               //settlementCount++;
	               
	               logger.debug("****Line Number: " + lineCount + 
	                            "****Amount: " + rsBilled.getDouble("order_amount") +
	                            "****Order Number: " + rsBilled.getString("order_number") +
	                            "****Payment ID: " + rsBilled.getString("payment_id") +
	                            "****Transaction Type: " + rsBilled.getString("transaction_type")+
	                            "****WALLET INDICATOR: " + rsBilled.getString("wallet_indicator")+
	                            "****Initial Auth Amount: " + rsBilled.getDouble("initial_auth_amount") +
	                            "****CSC Responce Code: " + rsBilled.getString("csc_response_code") +
	                            "****Cardinal Verified Flag: " + rsBilled.getString("cardinal_verified_flag"));
	               try {
	            	   
	            	   long paymentId = vo.getPaymentId();
	                    //out.write(bo.buildLineText()+newLine);
	            	   // validate EODRecordVO
	            	   logger.debug("validating eodRecordVo attributes for the payment id: " + paymentId);
	            	   try {
	                 	  PTSFieldValidator.getInstance().validateEodRecordVo(vo);
	                   } catch(PTSFieldValidationException e) {
	                 	  fieldValidationResult.append("Payment id: " + paymentId + " \n" + e.getMessage());
	                   }
	            	   
	            	   // validate payment extensio map
	            	   logger.debug("validating payment extension attributes for the payment id: " + paymentId);
	            	   Map<String, String> payExtMap  = PTSUtil.getPaymentExtMapByPaymentId(paymentId);
	           		   try {
	           				PTSFieldValidator.getInstance().validatePaymentExtnMap(payExtMap);	
	           		   } catch(PTSFieldValidationException e) {
		                 	  fieldValidationResult.append("Payment id: " + paymentId + " \n" + e.getMessage());
		               }	           		
	            	   validationResult = validationResult.append(fieldValidationResult);
	            	   
	                    //out.write(bo.buildLineText()+newLine);
	            	   /*
	            	   * SP-72: BAMS - Additional validations for fields in settlement file.
	            	   * Introducing checks on record lengths, with this release. Reference to validationResult is passed to respective methods
	            	   * to add the results of length checks on records to it.
	            	   */  
	            	   if("1".equalsIgnoreCase(vo.getTransactionType())){
	            		   if("Y".equalsIgnoreCase(vo.getVoiceAuthFlag())){
	            			   recordSeq = settleCCBO.writeVoiceAuthPayments(vo, recordSeq, out, validationResult);
	            		   }else{
	            			   if("DI".equalsIgnoreCase(vo.getCreditCardType()) || 
	            					   "DC".equalsIgnoreCase(vo.getCreditCardType())){
	            				   recordSeq = settleCCBO.writeDinersAndDiscoverPayments(vo, recordSeq, out, validationResult);
	            			   }else if ("VI".equalsIgnoreCase(vo.getCreditCardType())){
	            				   recordSeq = settleCCBO.writeVisaPayments(vo, recordSeq, out, validationResult);
	            			   }else if ("MC".equalsIgnoreCase(vo.getCreditCardType())){
	            				   recordSeq = settleCCBO.writeMasterCardPayments(vo, recordSeq, out, validationResult);
	            			   }
	            		   }
	            		   logger.debug("Total Sales Amount Before : " + totalSalesAmount);
	            		   logger.debug("Order Amount : "+ vo.getOrderAmount());
	            		   totalSalesAmount += vo.getOrderAmount();
	            		   logger.debug("Total Sales Amount After : " + totalSalesAmount);
	            	   }else if ("3".equalsIgnoreCase(vo.getTransactionType())){
	            		   recordSeq = settleCCBO.writeRefunds(vo, recordSeq, out, validationResult);
	            		   totalRefundAmount += vo.getOrderAmount();
	            	   }
	               } catch (Exception e){
	                    logger.error(e);
	                    //settlementCount--;
	                    lineCount--;
	                    
	                    
	                    //rollback status
	                    if("1".equals(rsBilled.getString("transaction_type"))) {
	                    	totalSalesAmount -= vo.getOrderAmount();
	                        eodDAO.doUpdatePaymentBillStatus(Long.parseLong(rsBilled.getString("payment_id")),timestamp,ARConstants.ACCTG_STATUS_UNBILLED);
	                    } else if("3".equals(rsBilled.getString("transaction_type"))) {
	                    	totalRefundAmount -= vo.getOrderAmount();
	                        eodDAO.doUpdateRefundBillStatus(Long.parseLong(rsBilled.getString("refund_id")),timestamp,ARConstants.ACCTG_STATUS_UNBILLED);
	                    } else {
	                        logger.error("Unknown transaction type");
	                    }
	               }
	               lineCount++;
        	   }
          } 
             
         //write billing trailer record with settlement amount and record count
         //out.write(eodFileVO.getTrailerRecordType() + EODUtil.pad(new Integer(settlementCount).toString(),PAD_LEFT,"0",10) +  EODUtil.pad(EODUtil.formatDoubleAmount(settlementAmount).toString(),PAD_LEFT,"0",12));
          settleCCBO.writeTrailerRecord(totalSalesAmount, totalRefundAmount, recordSeq, out, eodFileVO.getCompanyId(), validationResult);
          String submissionId = PTSUtil.getComapanyMap().get(eodFileVO.getCompanyId());
          String julianDate= PTSUtil.formatDate(new Date(), "yyDDD");
          String settlementTotalsId = null;
          if(submissionId != null && julianDate != null){
        	  if(totalSalesAmount > 0 || totalRefundAmount > 0){
        		  settlementTotalsId = eodDAO.doCreateSettlementTotals(submissionId, julianDate, totalSalesAmount, totalRefundAmount, billingHeaderId, null, eodFile.getName()+".pgp");
        	  }else{
        		  settlementTotalsId = eodDAO.doCreateSettlementTotals(submissionId, julianDate, totalSalesAmount, totalRefundAmount, billingHeaderId, ARConstants.SettlementStatus.Settled.toString(), eodFile.getName()+".pgp");
        	  }
          }
          if(settlementTotalsId != null){
        	  logger.debug("Settlement totals inserted successfully for submission Id :"+submissionId+" and julian date : "+julianDate);
          }
         if(out != null) {
            out.close();
         }
         logger.debug("BAMSEndOfDayFinalizer done writing to file!");
      }
      
      return eodFile;
   }

   /**
    * Updates the processed_indicator for the given batches
    */
   private void updateProcessedIndicator(EODDAO eodDAO, CachedResultSet rs) throws Exception {
      // Retrieve batch numbers and put it in comma delimited string
      String batches = "";
      String delim = ",";
      
      if(rs != null) {
          while (rs.next()) {
              batches = batches + rs.getString("billing_header_id");
              batches = batches + delim;
          }
          // Strip the last delimiter.
          if(batches.length() > 0) {
              batches = batches.substring(0,batches.length()-1);
          }
          logger.debug("BAMSEndOfDayFinalizer updating batches:" + batches);
          
          // Call procedure to update processed_indicator for all batches.
          eodDAO.doUpdateProcessedIndicator(batches, "Y");
      }
   }
    
     
    
    /**
     * This method performs closing tasks necessary to End-of-Day process.
     * �	Mark all vendor-delivered orders with a ship date prior or on EOD date
     *    to a status of 'Shipped' (if in Printed status previously or Processed). 
     * �	Flag all vendor-delivered orders with an operational status of 'Shipped' 
     *    with a delivered flag if the delivery date on the order is eod date
     *    or prior. Capture delivery_date on eod_delivery_date.
     * �	Flag all florist-delivered orders with an operational status of 'Processed'
     *    with a delivered flag if the delivery date is on or beofre EOD date.
     *    Capture delivery_date on eod_delivery_date.
     *    
     * �	Update BILLING_HEADER completion_status to �Y�. 
     *      Call EODDAO.doUpdateProcessedIndicator(batchNumber, �Y�).
     * �	FTP file to the FTP server.
     * �	Archive file.
     * �	Release lock by calling LockUtil.releaseLock.
     * @param payload - Object
     * @return n/a
     * @throws Throwable
     */
    private void performCompletionTasks(Calendar batchDate, Connection conn) throws Exception
    {
    	String type = RESTUtils.getConfigParamValue("REFUND_TO_REINSTATE", false);
    	if(null == type){
    		logger.error("Type of Maintenance is null, Check Global Params");
    	}
        if(logger.isDebugEnabled()) {
            logger.debug("Entering cleanUpEOD");
        }
       try{
            EODDAO eodDAO = new EODDAO(conn);
            String ccAuthProvider = ARConstants.BAMS_CC_AUTH_PROVIDER;
            eodDAO.doSetEODDeliveredInd(batchDate.getTimeInMillis(), ccAuthProvider);
            // Reinstate GCC that are put into 'Refund' Status.
            GiftCodeUtil.gcMaintenance(type);
        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("BAMSEndOfDayFinalizer exiting cleanUpEOD");
            } 
        }
    }

    /**
     * Extracts information from the JMS message.
     */
    private EODMessageVO extractMessage(MessageToken mt) throws Exception {
        String message = (String)mt.getMessage();
        Document msgDoc = DOMUtil.getDocument(message);
        EODMessageVO eodMessageVO = new EODMessageVO();
        eodMessageVO.setBatchTime((Long.valueOf((((NodeList)msgDoc.getElementsByTagName("BATCH_TIME")).item(0).getFirstChild()).getNodeValue())).longValue());
        eodMessageVO.setStartTime((Long.valueOf((((NodeList)msgDoc.getElementsByTagName("START_TIME")).item(0).getFirstChild()).getNodeValue())).longValue());
        eodMessageVO.setBatchNumber((Long.valueOf((((NodeList)msgDoc.getElementsByTagName("BATCH_NUMBER")).item(0).getFirstChild()).getNodeValue())).longValue());
        logger.info("BAMSEODFinalizer batchNumber: " + eodMessageVO.getBatchNumber());
        return eodMessageVO;
    }

}