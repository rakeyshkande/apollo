package com.ftd.accountingreporting.eventhandler;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.dao.AmazonDAO;
import com.ftd.accountingreporting.dao.CommonDAO;
import com.ftd.accountingreporting.util.LockUtil;
import com.ftd.accountingreporting.util.SystemMessager;
import com.ftd.eventhandling.events.EventHandler;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.osp.utilities.systemmessenger.exception.SystemMessengerException;
import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.text.ParseException;

import java.util.HashMap;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

public class AmazonRemittanceHandler extends EventHandler
{
    private Logger logger = 
        new Logger("com.ftd.accountingreporting.eventhandler.AmazonRemittanceHandler");
    
    Connection conn = null;
    
    public AmazonRemittanceHandler()
    {
        super();
    }

    /**
     * 1.	Lock Process.
     *      a.	Get AmazonConstants.LOCK_ENTITY_TYPE_REMITTANCE
     *      b.	get AmazonConstants.LOCK_ENTITY_ID_REMITTANCE
     *      c.	Get AmazonConstants.CSR_ID
     *      d.	get session id from the hash code of this object. 
     *          Set it to the class variable sessionId.
     *      e.	Call LockUtil.obtainLock. set return value to class 
     *              variable obtainedLock
     * 2.	Call process() to process the remittance records.
     * 3.	Release Lock. Call LockUtil.releaseLock
     * @param payload - Object
     * @return n/a
     * @throws Throwable
     * @todo - code and determine exceptions
     */
    public void invoke(Object payload) throws Throwable 
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering invoke");
        }

        boolean lockObtained = false;
        LockUtil lockUtil = null;
        String sessionId = "";
        
        try{
            /* retrieve connection to database */
            conn = DataSourceUtil.getInstance().
                getConnection(ConfigurationUtil.getInstance().getProperty(
                ARConstants.CONFIG_FILE,ARConstants.DATASOURCE_NAME));
            
            /* obtain process lock */    
            lockUtil = new LockUtil(conn);
            sessionId = String.valueOf(this.hashCode()); 
            lockObtained = lockUtil.obtainLock(
                ARConstants.AMAZON_LOCK_ENTITY_TYPE_REMITTANCE,
                ARConstants.AMAZON_LOCK_ENTITY_ID_REMITTANCE, 
                ARConstants.AMAZON_LOCK_CSR_ID, sessionId); 
                
            /* process the remittance records */
            process();
            
       } catch (SQLException e) {
          // Do not rethrow. Force dequeue.
          logError(e,"");            
       } catch (TransformerException e) {
          // Do not rethrow. Force dequeue.
          logError(e,"");
       } catch (IOException e) {
          // Do not rethrow. Force dequeue.
          logError(e,"");
       } catch (SAXException e) {
          // Do not rethrow. Force dequeue.
          logError(e,"");
       } catch (ParserConfigurationException e) {
          // Do not rethrow. Force dequeue.
          logError(e,"");
       } catch (ParseException e) {
          // Do not rethrow. Force dequeue.
          logError(e,"");        
       } catch (Exception e) {
          // Do not rethrow. Force dequeue.
          logError(e,"");
       } catch (Throwable e) {
          // Do not rethrow. Force dequeue.
          logError(e,"");
       } finally {
       
            if(lockObtained == true)
            {       
                /* Release Lock */
                lockUtil.releaseLock(
                    ARConstants.AMAZON_LOCK_ENTITY_TYPE_REMITTANCE,
                    ARConstants.AMAZON_LOCK_ENTITY_ID_REMITTANCE,
                    ARConstants.AMAZON_LOCK_CSR_ID,sessionId); 
            }
            
            if(conn != null && !conn.isClosed()) {
                logger.debug("Closing connection...");
                conn.close();
            }
            if(logger.isDebugEnabled()){
               logger.debug("Exiting invoke");
            } 
       }
    }

    /**
     *This method processes remittance records. 
     * 1.	Retrieves unprocessed settlement data. 
     *      Call AmazonDao.doGetUnprocessedRemittance()
     * 2.	For each settlement data id, 
     *      a.	Retrieve remittance detail orders. 
     *          Call amazonDao.doGetRemittanceOrders(settlementDataId). 
     *          For each confirmation_number (order_details.external_order_number), 
     *          mark all order_bills as �Settled�. 
     *          Call CommonDao.doUpdateOrderAcctgStatus(confirmation_number, �Settled�);
     *      b.	retrieve remittance adjustments. 
     *          Call amazonDao.doGetRemittanceRefunds(settlementDataId); 
     *          For each confirmation number (order_details.external_order_number), 
     *          mark all refunds as �Settled�. 
     *          Call CommonDao. doUpdateRefundAcctgStatus(confirmation_number, �Settled�);
     *      c.	Mark settlement data processed. 
     *      Call AmazonDao.doMarkSettlementProcessed(settlementDataId)
     * @param n/a
     * @return n/a
     * @throws IOException
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws TransformerException
     * @throws XSLException
     * @throws ParseException
     * @throws SQLException
     * @throws Exception
     * @todo - code and determine exceptions
     */
    private void process() throws SAXException, 
        ParserConfigurationException, IOException, SQLException, Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering process");
        }
        
        try{
            /* Retrieve unprocessed settlement data. */
            AmazonDAO amznDAO = new AmazonDAO(conn);
            CommonDAO commonDAO = new CommonDAO(conn);
            CachedResultSet unprocessRemitt = amznDAO.doGetUnprocessedRemittance();
            if(unprocessRemitt != null) {
                while(unprocessRemitt.next())
                {
                    String settlementDataId = unprocessRemitt.getString("SETTLEMENT_DATA_ID");
                    /* retrieve remittance details */
                    HashMap remittanceDetails = amznDAO.doGetRemittanceDetails(settlementDataId);
                    /* Retrieve remittance detail orders. */
                    CachedResultSet remittDetailOrders = 
                        (CachedResultSet) remittanceDetails.get("OUT_SETTLE_ORDER_ITEM_CURSOR");
            
                    while(remittDetailOrders.next())
                    {
                        /* mark each order as Settled */
                        try {
                            String confirmationNumber = 
                                remittDetailOrders.getString("CONFIRMATION_NUMBER");
                            commonDAO.doUpdateOrderAcctgStatus(confirmationNumber, 
                                ARConstants.ACCTG_STATUS_SETTLED);
                        } catch (Exception e) {
                            logError(e, "Cannot update order accounting status");
                        }
                    }
                    /* Retrieve remittance adjustments  */
                    CachedResultSet remittAdjustments = 
                        (CachedResultSet) remittanceDetails.get("OUT_SETTLE_ADJUST_ITEM_CURSOR");
                    while(remittAdjustments.next())
                    {
                        /* mark each adjustment as Settled */
                        try {
                            String confirmationNumber = 
                            	remittAdjustments.getString("CONFIRMATION_NUMBER");
                            commonDAO.doUpdateRefundAcctgStatus(confirmationNumber, 
                                ARConstants.ACCTG_STATUS_SETTLED);
                        } catch (Exception e) {
                            logError(e, "Cannot update refund accounting status");
                        }
                    }                
                    
                    /* Mark settlement data processed  */
                    amznDAO.doMarkSettlementProcessed(settlementDataId);
                }
            }
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting process");
            } 
       } 
    }

    /**
     * Send an error message via the system messenger 
     * @param errorMessage - String
     * @return n/a
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     * @throws SystemMessengerException
     */
    private void logError(Throwable exception, String errorMessage) throws SAXException, 
        ParserConfigurationException, IOException, SQLException, 
        SystemMessengerException, TransformerException
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering logError");
        }
        
         try{
            
            /* log message to log4j log file */
            logger.error(errorMessage,exception);
            
            /* send a system message */
            SystemMessager sysMessager = new SystemMessager();
            sysMessager.send(errorMessage,exception,
                ARConstants.AMAZON_REMITTANCE_PROCESS,
                SystemMessager.LEVEL_DEBUG,
                ARConstants.AMAZON_REMITTANCE_PROCESS_ERROR_TYPE,conn);
        
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting logError");
            } 
       } 
        
    }
}