package com.ftd.accountingreporting.eventhandler;
import com.enterprisedt.net.ftp.FTPException;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.dao.ReconDAO;
import com.ftd.accountingreporting.exception.CancelWithNoDenialException;
import com.ftd.accountingreporting.exception.EntityLockedException;
import com.ftd.accountingreporting.exception.NoFillerFoundException;
import com.ftd.accountingreporting.exception.NoMatchFoundException;
import com.ftd.accountingreporting.exception.RejectionException;
import com.ftd.accountingreporting.reporting.bo.ReportBO;
import com.ftd.accountingreporting.util.AccountingUtil;
import com.ftd.accountingreporting.util.FileArchiveUtil;
import com.ftd.accountingreporting.util.FileCompressionUtil;
import com.ftd.accountingreporting.util.FtpUtil;
import com.ftd.accountingreporting.util.LockUtil;
import com.ftd.accountingreporting.util.SystemMessager;
import com.ftd.accountingreporting.vo.ReconMessageVO;
import com.ftd.eventhandling.events.EventHandler;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This class fetches and processes the monthly Mercury reconciliation file 
 * CB908400. The job is scheduled to run monthly.
 * @author Charles Fox
 */
public class FloristReconHandler extends EventHandler
{
    private Logger logger = 
        new Logger("com.ftd.accountingreporting.eventhandler.FloristReconHandler");
    private Connection conn = null;
    private final static String ORDER_NUMBER_DELIM = "-";
    
    public FloristReconHandler()
    {
        super();
    }
    
    /**
     * This is the main invocation class. It does the following:
     * �	Obtain process lock.
     * �	Use FTPUtil to retrieve CB908400 from the FTP server. 
     *      If file not found, send system message.
     * �	Process the file. For each line in the file,
     *      o	Extract Mercury Order Number from position 20-29.
     *      o	Extract amount reconciled from position 34-39.
     *      o	Check if message is double dip. A message is a double dip if a 
     *          message with the same message id exists in RECON_MESSAGES table 
     *          with a status of �RECONCILED� or �PRICE VARIANCE�. 
     *          If so, create a RECON_MESSAGES record with 
     *          RECON_DISP_CODE �DOUBLE DIP�. System = �MERC�.
     *      o	Retrieve the last MERCURY.MERCURY record of message type �FTD� 
     *          with the given Mercury Order Number. Calculate the amount to 
     *          be reconciled. Call CBRDAO.doCalcAmtToRecon
     *          ?	If �NO MATCH FOUND� exception is caught, create a 
     *              RECON_MESSAGES record with RECON_DISP_CODE �NO MATCH FOUND�.
     *          ?	If �CAN WITH NO DEN� or �REJ� exception is caught, create a 
     *              RECON_MESSAGES record with RECON_DISP_CODE �UNEXPECTED�.
     *          ?	If the returned price does not match amount reconciled, 
     *              create a RECON_MESSAGES record with 
     *              RECON_DISP_CODE �PRICE VARIANCE�.
     *          ?	If the price mathes, create a RECON_MESSAGES record with 
     *              RECON_DISP_CODE �RECONCILED�.
     * �	Release process lock.
     * @param payload - Object
     * @return n/a
     * @throws Throwable
     * @todo - code and determine exceptions
     */
    public void invoke(Object payload) throws Throwable 
    {
        if(logger.isDebugEnabled()) {            
            logger.debug("***********************************************************");
            logger.debug("* Start Mercury Reconciliation Processing");
            logger.debug("***********************************************************");
        }
        
        
        LockUtil lockUtil = null;
        String sessionId = "";
        boolean lockObtained = false;
        
        try{
            // retrieve connection to database 
            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
            conn = DataSourceUtil.getInstance().
                getConnection(ConfigurationUtil.getInstance().getProperty(
                ARConstants.CONFIG_FILE,ARConstants.DATASOURCE_NAME));            
            ReconDAO reconDAO = new ReconDAO(conn);
            // obtain process lock 
            lockUtil = new LockUtil(conn);
            sessionId = String.valueOf(this.hashCode());
            lockObtained = lockUtil.obtainLock(
                ARConstants.MERCURY_RECON_ENTITY_TYPE_REMITTANCE,
                ARConstants.MERCURY_RECON_ENTITY_ID_REMITTANCE, 
                ARConstants.MERCURY_RECON_CSR_ID, sessionId); 
            
            if(lockObtained) {
                // archive old files.
                archiveFiles();
                
                // Use FTPUtil to retrieve CB908400 from the FTP server 
                File mercuryFile = getMercReconFile();
                if(mercuryFile != null){
                
                    FileReader readFile = new FileReader(mercuryFile);
                    BufferedReader input = new BufferedReader(readFile);
                    String line = null;
                    
                    String archiveNumMonths = configUtil.getProperty(
                        ARConstants.CONFIG_FILE,ARConstants.MESG_ARCHIVE_NUM_MONTHS);
                    String archiveNumDays = configUtil.getProperty(
                        ARConstants.CONFIG_FILE,ARConstants.MESG_ARCHIVE_NUM_DAYS);
                    AccountingUtil acctUtil = new AccountingUtil();
                    
                    // calculate cutoff_date 
                    int archiveMonths = 0;
                    int archiveDays = 0;
                    Date cutoff = new Date();
                    
                    // Use archive number of months if it's configured; Otherwise use 
                    // the number of days configured.
                    if(archiveNumMonths != null && !"".equals(archiveNumMonths)) {
                        archiveMonths = Integer.parseInt(archiveNumMonths);
                        cutoff = acctUtil.calculateOffSetDatebyMonth(archiveMonths); 
                    } else {
                        archiveDays = Integer.parseInt(archiveNumDays);
                        cutoff = acctUtil.calculateOffSetDate(archiveDays);
                    }
                    
                    while (( line = input.readLine()) != null){
                        if(!line.equals("")){
                          try {
                            // Extract Mercury Order Number      
                            String mercOrderNumber = (line.substring(19,29)).trim();
                            
                            // Extract amount reconciled 
                            double amount = Double.parseDouble(line.subSequence(33,37).toString() + "." + 
                                line.subSequence(37,39).toString());
                            // Check if message is double dip. 
                            boolean doubleDip = reconDAO.isDoubleDip(mercOrderNumber,"Merc");
                            String fillerID = reconDAO.doGetFillerID("Merc",mercOrderNumber);
                                
                            ReconMessageVO reconVO = new ReconMessageVO();
                            reconVO.setAmtReconciled(amount);
                            reconVO.setMessageId(mercOrderNumber);
                            reconVO.setSystem("Merc");
                            reconVO.setFillerId(fillerID);
                            
                            if(doubleDip == false)
                            {
                                try{
                                    double calcamt = reconDAO.doCalcAmtToRecon(
                                        mercOrderNumber,"Merc",cutoff);
                                    if(calcamt == amount)
                                    {
                                        reconVO.setAmtToReconcile(calcamt);
                                        reconVO.setReconDispCode("RECONCILED");
                                    }
                                    else
                                    {
                                        // create a RECON_MESSAGES record with 
                                        // RECON_DISP_CODE �PRICE VARIANCE�. 
                                        reconVO.setAmtToReconcile(calcamt);
                                        reconVO.setReconDispCode("PRICE VARIANCE");
                                    }
                                }catch(CancelWithNoDenialException canException)
                                {
                                    //  create a RECON_MESSAGES record with 
                                    //  RECON_DISP_CODE 'UNEXPECTED�.
                                    reconVO.setAmtToReconcile(0);
                                    reconVO.setReconDispCode("UNEXPECTED");
                                }catch(NoMatchFoundException noMatchException)
                                {
                                    //  create a RECON_MESSAGES record with 
                                    //  RECON_DISP_CODE �NO MATCH FOUND�. 
                                    reconVO.setAmtToReconcile(0);
                                    reconVO.setReconDispCode("NO MATCH FOUND");
                                }catch(RejectionException rejException)
                                {
                                    //  create a RECON_MESSAGES record with 
                                    //  RECON_DISP_CODE �UNEXPECTED�.
                                    reconVO.setAmtToReconcile(0);
                                    reconVO.setReconDispCode("UNEXPECTED");
                                } finally{
                                
                                }
                            }
                            else
                            {
                                //  If so, create a RECON_MESSAGES record with 
                                //  RECON_DISP_CODE �DOUBLE DIP�. System = �MERC� 
                                reconVO.setAmtToReconcile(0);
                                reconVO.setReconDispCode("DOUBLE DIP");
                            }
                            logger.debug(reconVO.toString());
                            reconDAO.doInsertReconMessage(reconVO);
                          } catch (Exception e) {
                            logger.error("Cannot process line:" + line, e);
                            logError(e, "");
                        }
                      }
                    }
                    input.close();
                    readFile.close();
                    //mercuryFile.delete();
                    
                    // Kick off report.
                    try {
                        String reportId = configUtil.getProperty(ARConstants.CONFIG_FILE, ARConstants.MERCURY_RECON_REPORT_ID_KEY);
                        
                        ReportBO reportBO=new ReportBO(conn);
                        reportBO.processScheduledReport(reportId);
                    } catch (Exception re) {
                        // do not fail current job if report fails.
                        logger.error("Failed to run report: " + re);
                        logError(re, "");
                    }
                    
                }
                else
                {
                    int deadLine = Integer.parseInt(
                        ConfigurationUtil.getInstance().getProperty(
                        ARConstants.CONFIG_FILE,"RECON_MERC_DEADLINE"));
                    String recheckInterval = ConfigurationUtil.getInstance().getProperty(
                        ARConstants.CONFIG_FILE,"RECON_FILE_RECHECK_INTERVAL");                        
                    AccountingUtil acctUtil = new AccountingUtil();
                    boolean deadlinePassed = acctUtil.checkProcessDeadline(deadLine,"Florist Reconciliation");
                    if(deadlinePassed==false)
                    {
                        // requeue message 
                        acctUtil.queueToEventsQueue(ARConstants.EVENT_CONTEXT, "RECON-MERCURY", recheckInterval, null);
                    } else {
                        logError(new Exception("No file found"), "");
                    }
                }
            } else {
                logger.error(new EntityLockedException(ARConstants.MERCURY_RECON_CSR_ID));
            }            
        } catch (Exception e) {
            logError(e,"");
            throw e;
        } finally {
            // Release process lock.  
            if(lockUtil != null){
                if(lockObtained == true){
                    lockUtil.releaseLock(
                        ARConstants.MERCURY_RECON_ENTITY_TYPE_REMITTANCE,
                        ARConstants.MERCURY_RECON_ENTITY_ID_REMITTANCE, 
                        ARConstants.MERCURY_RECON_CSR_ID, sessionId); 
                }
            }
            
            // close db connection 
            if(conn != null && !conn.isClosed()) {
                  logger.debug("Closing connection...");
                  conn.close();
            }
            
            if(logger.isDebugEnabled()){
                logger.debug("***********************************************************");
                logger.debug("* Mercury Reconciliation Processing Completed");
                logger.debug("***********************************************************");
            } 
        }
    }
    
    /**
     * Send an error message via the system messenger 
     * @param errorMessage - String
     * @return n/a
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     * @throws SystemMessengerException
     */
    private void logError(Throwable exception, String errorMessage) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering logError");
        }
        
         try{
            
            /* log message to log4j log file */
            logger.error(errorMessage,exception);
            
            /* send a system message */
            SystemMessager sysMessager = new SystemMessager();
            sysMessager.send(errorMessage,exception,
                ARConstants.FLORIST_RECON_PROCESS,SystemMessager.LEVEL_DEBUG,
                ARConstants.FLORIST_RECON_PROCESS_ERROR_TYPE,conn);
        
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting logError");
            } 
       } 
        
    }
    /**
     * This method utilizes the FTD Utility to retrieve the CB908400 file
     * @param n/a
     * @return File
     * @throws Throwable
     */
    private File getMercReconFile() throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering getMercReconFile");
        }
        
        File floristRecon = null;
        try{        
            /* retrieve server name to FTP to */
            String ftpServer = ConfigurationUtil.getInstance().getFrpGlobalParm( 
                ARConstants.CONFIG_CONTEXT, "mercReconFtpServer");
            String ftpLocation = ConfigurationUtil.getInstance().getFrpGlobalParm(
                ARConstants.CONFIG_CONTEXT, "mercReconFtpLocation");
            String username = ConfigurationUtil.getInstance().getSecureProperty(
                ARConstants.SECURE_CONFIG_CONTEXT, "mercReconFtpUsername");
            String password = ConfigurationUtil.getInstance().getSecureProperty(
                ARConstants.SECURE_CONFIG_CONTEXT, "mercReconFtpPassword");

            String remoteFile = ConfigurationUtil.getInstance().getProperty(
                ARConstants.CONFIG_FILE, "mercReconFileName");
            String localFileDirectory = ConfigurationUtil.getInstance().getProperty(
                ARConstants.CONFIG_FILE, "MERC_RECON_LOCAL_LOCATION");            
            
            FtpUtil ftpUtil = new FtpUtil();
            /* log into FTP Server */
            ftpUtil.login(ftpServer, ftpLocation, username, password);

            String localFile = localFileDirectory + remoteFile;
            try {
                ftpUtil.getFile(localFileDirectory,remoteFile);
            } catch(FTPException e) {
                ftpUtil.logout();
                return null;
            }

            /* log off of FTP Server */
            ftpUtil.logout();
            
            /* retrieve the local file */
            floristRecon = new File(localFile);
            
            /* archive the local file */
            //FileCompressionUtil fileUtil = new FileCompressionUtil();
            //fileUtil.createGZipFile(localFile,localFile + sdfOutput.format(d) + ".gzip");
            
        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting getMercReconFile");
            } 
        }
        
        return floristRecon;
    }
    
    
  /**
   * Move files to the archive directory
   * @throws java.lang.Exception
   */
    private void archiveFiles() throws Exception
    {
        String fromDir = ConfigurationUtil.getInstance().getProperty(
                ARConstants.CONFIG_FILE,"MERC_RECON_LOCAL_LOCATION");
        String toDir = ConfigurationUtil.getInstance().getProperty(
                ARConstants.CONFIG_FILE,"MERC_RECON_ARCHIVE_LOCATION");
        FileArchiveUtil.moveToArchive(fromDir, toDir, true);
    }        
}