package com.ftd.accountingreporting.test;

import com.ftd.accountingreporting.dao.CommonDAO;
import java.sql.Date;

import java.util.ArrayList;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.w3c.dom.Document;

import com.ftd.accountingreporting.dao.EODDAO;
import com.ftd.accountingreporting.vo.EODRecordVO;
import com.ftd.accountingreporting.vo.PaymentTotalsVO;

import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;

public class EODDAOTest extends TestCase
{
  public EODDAOTest(String test)
  {
    super(test);
  }
  
   public void testDoGetBillingRecords() {
      try {
           
            Connection con = TestHelper.getConnection();
            Calendar cal1 = Calendar.getInstance();
            //cal1.setTime(new java.util.Date("05/26/2005"));
          
            //Document result = (new EODDAO(con)).doGetBillingRecords(cal1);
            con.close();
            //System.out.println("Result is "+result);
           // ((XMLDocument)result).print(System.out);
            // assertTrue("Result...", result.equals("ACCT"));
            //assertTrue("Result...", result.equals(""));
         }catch (Exception e) {
            e.printStackTrace();
        }
  }
  
  public void testDoCreateBillingHeader() {
      try {
           
            Connection con = TestHelper.getConnection();
            String result = (new EODDAO(con)).doCreateBillingHeader(Calendar.getInstance());
            con.close();
            System.out.println("Result is "+result);
           // ((XMLDocument)result).print(System.out);
            // assertTrue("Result...", result.equals("ACCT"));
            //assertTrue("Result...", result.equals(""));
         }catch (Exception e) {
            e.printStackTrace();
        }
  }

  public void testDoUpdateBillingHeader() {
      try {
           
            Connection con = TestHelper.getConnection();
            boolean result = (new EODDAO(con)).doUpdateBillingHeader("4402", "Y");
            con.close();
            System.out.println("Result is "+result);
           // ((XMLDocument)result).print(System.out);
            // assertTrue("Result...", result.equals("ACCT"));
            //assertTrue("Result...", result.equals(""));
         }catch (Exception e) {
            e.printStackTrace();
        }
  }

  public void testDoCreateBillingDetail() {
      try {
           
            Connection con = TestHelper.getConnection();
            
            EODRecordVO vo = new EODRecordVO();
            vo.setBillingHeaderId(4201);
            vo.setBillType("Refund");
            //String result = (new EODDAO(con)).doCreateBillingDetail(vo);
            con.close();
            //System.out.println("Result is "+result);
           // ((XMLDocument)result).print(System.out);
            // assertTrue("Result...", result.equals("ACCT"));
            //assertTrue("Result...", result.equals(""));
         }catch (Exception e) {
            e.printStackTrace();
        }
  }
  
  public void testDoGetCustomerName() {
      try {
           
            Connection con = TestHelper.getConnection();
            
            String result = (new EODDAO(con)).doGetCustomerName(100443);
            con.close();
            System.out.println("Result is "+result);
           // ((XMLDocument)result).print(System.out);
            // assertTrue("Result...", result.equals("ACCT"));
            //assertTrue("Result...", result.equals(""));
         }catch (Exception e) {
            e.printStackTrace();
        }
  }

  public void testDoGetClearingMemberNumber() {
      try {
           
            Connection con = TestHelper.getConnection();
            
            String result = (new EODDAO(con)).doGetClearingMemberNumber(100443);
            con.close();
            System.out.println("Result is "+result);
           // ((XMLDocument)result).print(System.out);
            // assertTrue("Result...", result.equals("ACCT"));
            //assertTrue("Result...", result.equals(""));
         }catch (Exception e) {
            e.printStackTrace();
        }
  }
  /*
  public void testDoGetCoBrandFields() {
      try {
           
            Connection con = TestHelper.getConnection();
            
            ResultSet result = (new EODDAO(con)).doGetCoBrandFields(100302);
            while (result.next())
            {
              System.out.println("Result is "+result.getString("info_name"));
            }
            
            con.close();
            //System.out.println("Result is "+result);
           // ((XMLDocument)result).print(System.out);
            // assertTrue("Result...", result.equals("ACCT"));
            //assertTrue("Result...", result.equals(""));
         }catch (Exception e) {
            e.printStackTrace();
        }
  }
*/
  public void testDoGetPaymentTotals() {
      try {
           
            Connection con = TestHelper.getConnection();
            //can also use following payment ids 333555, 10029, 102 and 1122318
            PaymentTotalsVO result = (new EODDAO(con)).doGetPaymentTotals(1122318);
            
            System.out.println("ServiceFeeTotal is "+result.getServiceFeeTotal());
            System.out.println("ShippingFeeTotal is "+result.getShippingFeeTotal());
            System.out.println("ShippingTaxTotal is "+result.getShippingTaxTotal());
            System.out.println("TaxTotal is "+result.getTaxTotal());

            
            con.close();
            //System.out.println("Result is "+result);
           // ((XMLDocument)result).print(System.out);
            // assertTrue("Result...", result.equals("ACCT"));
            //assertTrue("Result...", result.equals(""));
         }catch (Exception e) {
            e.printStackTrace();
        }
  }
  
  public void testDoUpdateBillRefundStatus() {
      try {
           
            Connection con = TestHelper.getConnection();
            
            boolean result = (new EODDAO(con)).doUpdateRefundBillStatus(333666, Calendar.getInstance().getTimeInMillis(), "Billed");
          
            con.close();
            System.out.println("Result is "+result);
           // ((XMLDocument)result).print(System.out);
            // assertTrue("Result...", result.equals("ACCT"));
            //assertTrue("Result...", result.equals(""));
         }catch (Exception e) {
            e.printStackTrace();
        }
  }
  
  public void testDoGetBillingDetails() {
      try {
           
            Connection con = TestHelper.getConnection();
            
            //ResultSet result = (new EODDAO(con)).doGetBillingDetails(4201);
            //while (result.next())
           // {
           //   System.out.println("Result is "+result.getLong("billing_detail_id"));
           // }
            
            con.close();
            //System.out.println("Result is "+result);
           // ((XMLDocument)result).print(System.out);
            // assertTrue("Result...", result.equals("ACCT"));
            //assertTrue("Result...", result.equals(""));
         }catch (Exception e) {
            e.printStackTrace();
        }
  }

  public void testDoGetCountryFromZip() {
      try {
           
            Connection con = TestHelper.getConnection();
            
            String result = (new EODDAO(con)).doGetCountryFromZip("60805");
                 
            con.close();
            System.out.println("Result is "+result);
           // ((XMLDocument)result).print(System.out);
            // assertTrue("Result...", result.equals("ACCT"));
            //assertTrue("Result...", result.equals(""));
         }catch (Exception e) {
            e.printStackTrace();
        }
  }

  public void testDoGetCusotmerInfo() {
      try {
           
            Connection con = TestHelper.getConnection();
            
            CachedResultSet result = (new EODDAO(con)).doGetCusotmerInfo(100565);
             while (result.next())
            {
              System.out.println("Result is first name "+result.getString("customer_first_name"));
              System.out.println("Result is last name"+result.getString("customer_last_name"));
            }          
            con.close();
            //System.out.println("Result is "+result);
           // ((XMLDocument)result).print(System.out);
            // assertTrue("Result...", result.equals("ACCT"));
            //assertTrue("Result...", result.equals(""));
         }catch (Exception e) {
            e.printStackTrace();
        }
  }
  /*
    public void testDoGetRecipientId() {
      try {
           
            Connection con = TestHelper.getConnection();
            
            long result = (new EODDAO(con)).doGetRecipientId(102582);
             
            con.close();
            System.out.println("Result is "+result);
           // ((XMLDocument)result).print(System.out);
            // assertTrue("Result...", result.equals("ACCT"));
            //assertTrue("Result...", result.equals(""));
         }catch (Exception e) {
            e.printStackTrace();
        }
  }
  */
  /*
  public void testDoGetPcardConfigFields() {
      try {
           ///was not able to test as package has errors.. will test afterwards after the package is fixed
            Connection con = TestHelper.getConnection();
            
            ResultSet result = (new EODDAO(con)).doGetPcardConfigFields(100911);
             while (result.next())
            {
              System.out.println("Result is field_name "+result.getString("field_name"));
              System.out.println("Result is source_table"+result.getString("source_table"));
            }          
            con.close();
            //System.out.println("Result is "+result);
           // ((XMLDocument)result).print(System.out);
            // assertTrue("Result...", result.equals("ACCT"));
            //assertTrue("Result...", result.equals(""));
         }catch (Exception e) {
            e.printStackTrace();
        }
  }
  */
  public void testIsShoppingCartInStatus() {
      /*select o.master_order_number  FROM 
clean.orders o, clean.order_details od
--where o.master_order_number = in_master_order_number
where  od.order_guid = o.order_guid
AND od.order_disp_code IN ('In-Scrub','Held ')*/
    //was not able to test as proc requires some changes
      try {
           Connection con = TestHelper.getConnection();
            
            boolean result = (new CommonDAO(con)).isShoppingCartInStatus("99","'In-Scrub', 'Held'");
             
            con.close();
            System.out.println("Result is "+result);
           // ((XMLDocument)result).print(System.out);
            // assertTrue("Result...", result.equals("ACCT"));
            //assertTrue("Result...", result.equals(""));
         }catch (Exception e) {
            e.printStackTrace();
        }
  }

  public void testDoUpdateOrderStatus() {
   
      try {
           Connection con = TestHelper.getConnection();
           SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
           Calendar cal = Calendar.getInstance();
           cal.setTime(df.parse("04/26/2005"));
           //(new EODDAO(con)).doUpdateOrderStatus("VENDOR","AMZNI", Calendar.getInstance(), "Billed");
           //(new EODDAO(con)).doUpdateOrderStatus("VENDOR","DP", cal, "Processed", "Billed");
             
            con.close();
            //System.out.println("Result is "+result);
           // ((XMLDocument)result).print(System.out);
            // assertTrue("Result...", result.equals("ACCT"));
            //assertTrue("Result...", result.equals(""));
         }catch (Exception e) {
            e.printStackTrace();
        }
  }

  public void testDoGetType() {
      try {
            Connection con = TestHelper.getConnection();
            
            //As get cart is not in package declaration was not able to test this need this procedure
            //to be in package declaration.
            String originId = (new EODDAO(con)).doGetOriginId("FTD_GUID_959542686014410850060759377782014935415710-1346354870012693458220-1219869617015325202901-11185968970330354363014366701990-1824202456251169269463368777062018173-237423454741734506918214");
                        
            System.out.println("Origin Id is "+originId);
            
            //String originType = (new EODDAO(con)).doGetType("FOX");
            //System.out.println("Origin Type is "+originType);
            con.close();

         }catch (Exception e) {
            e.printStackTrace();
        }
  }

  public void testDoUpdateVendorFlag() {
      try {
            Connection con = TestHelper.getConnection();
            
            EODDAO eodDAO = new EODDAO(con);
            eodDAO.doSetEODDeliveredInd(Calendar.getInstance().getTimeInMillis());
            con.close();

         }catch (Exception e) {
            e.printStackTrace();
        }
  }
  
  public static Test suite() {
    	TestSuite suite= new TestSuite();
      
      //suite.addTest(new EODDAOTest("testDoGetBillingRecords"));
      //suite.addTest(new EODDAOTest("testDoCreateBillingHeader"));
      //suite.addTest(new EODDAOTest("testDoUpdateBillingHeader"));
      //suite.addTest(new EODDAOTest("testDoCreateBillingDetail"));
      //suite.addTest(new EODDAOTest("testDoGetCustomerName"));
      //suite.addTest(new EODDAOTest("testDoGetClearingMemberNumber"));
      //suite.addTest(new EODDAOTest("testDoGetCoBrandFields"));
      //suite.addTest(new EODDAOTest("testDoGetPaymentTotals"));
      //suite.addTest(new EODDAOTest("testDoUpdateBillRefundStatus"));
      //suite.addTest(new EODDAOTest("testDoGetBillingDetails"));
      //suite.addTest(new EODDAOTest("testDoGetCountryFromZip"));
      //suite.addTest(new EODDAOTest("testDoGetCusotmerInfo"));
      //suite.addTest(new EODDAOTest("testDoGetRecipientId"));
      //suite.addTest(new EODDAOTest("testDoGetPcardConfigFields"));
      //suite.addTest(new EODDAOTest("testIsShoppingCartInStatus"));
      //suite.addTest(new EODDAOTest("testDoUpdateOrderStatus"));
      suite.addTest(new EODDAOTest("testDoUpdateVendorFlag"));
      return suite;
  }
  
 /**
  * You can either user the text ui or swing ui to run the test
  **/
  public static void main(String[] args){
    junit.textui.TestRunner.run(suite());
  }

}