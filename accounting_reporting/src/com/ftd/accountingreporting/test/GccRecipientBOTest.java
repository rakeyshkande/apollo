package com.ftd.accountingreporting.test;

import java.sql.Date;

import java.util.ArrayList;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.ftd.accountingreporting.dao.GccRecipientDAO;
import com.ftd.accountingreporting.vo.GccRecipientVO;
import com.ftd.accountingreporting.vo.GiftCertificateCouponVO;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.eventhandler.GCCStatusUpdateHandler;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import javax.servlet.http.HttpServletRequest;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;

import org.w3c.dom.Document;
/**
 * Fixture to test GccRecipientBOTest class.
 * @author Madhu Parab
 */
public class GccRecipientBOTest extends TestCase
{
  public GccRecipientBOTest(String test)
  {
    super(test);
  }
  
   public void testHandleDisplayAdd() {
      try {
           
            Connection con = TestHelper.getConnection();
            //Document result = (new GccRecipientBO(con)).handleDisplayAdd();
            con.close();
            //System.out.println("Result is "+result);
            //((XMLDocument)result).print(System.out);
            // assertTrue("Result...", result.equals("ACCT"));
            //assertTrue("Result...", result.equals(""));
         }catch (Exception e) {
            e.printStackTrace();
        }
  }
  
  public void testHandleAdd() {
      try {
           
            Connection con = TestHelper.getConnection();
            HttpServletRequest request = null;
            //Document result = (new GccRecipientBO(con)).handleDisplayEdit(request);
            con.close();
            //System.out.println("Result is "+result);
            //((XMLDocument)result).print(System.out);
            // assertTrue("Result...", result.equals("ACCT"));
            //assertTrue("Result...", result.equals(""));
         }catch (Exception e) {
            e.printStackTrace();
        }
  }
  
    public void testInvoke() {
      try {
           Object obj = null;
           new GCCStatusUpdateHandler().invoke(obj);
           
         }catch (Throwable e) {
            e.printStackTrace();
        }
  }
  
  
    public static Test suite() {
    	TestSuite suite= new TestSuite();
      
      //suite.addTest(new GccRecipientBOTest("testHandleDisplayAdd"));
      //suite.addTest(new GccRecipientBOTest("testHandleDisplayEdit"));
      suite.addTest(new GccRecipientBOTest("testInvoke"));
      return suite;
    }
    
  /**
   * You can either user the text ui or swing ui to run the test
   **/
  public static void main(String[] args){
    // pass in the class of the test that needs to be run
    //junit.swingui.TestRunner.run(FrameworkTest.class);
    junit.textui.TestRunner.run(suite());
  }
}