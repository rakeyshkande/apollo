package com.ftd.accountingreporting.test;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.PrintWriter;
import java.io.IOException;

public class TestaafesCall extends HttpServlet 
{
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";

    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(CONTENT_TYPE);
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head><title>TestaafesCall</title></head>");
        out.println("<body>");
        out.println("<p>The servlet has received a GET. This is the reply.</p>");
        out.println("</body></html>");
        out.close();
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        System.out.println(request.getParameter("authcode"));
        System.out.println(request.getParameter("ticket"));
        System.out.println("TYPE " + request.getParameter("type"));
        response.setContentType(CONTENT_TYPE);
        PrintWriter out = response.getWriter();
        if(!request.getParameter("ticket").equals("23423434")){
            out.println("A," + request.getParameter("authcode") + 
            "," + request.getParameter("ticket") + ",processed");
            
        }
        else
        {
            out.println("D," + request.getParameter("authcode") + 
            "," + request.getParameter("ticket") + ",could not process");
        }
        
        
        /* 
        out.println("<html>");
        out.println("<head><title>TestaafesCall</title></head>");
        out.println("<body>");
        out.println("<p>The servlet has received a POST. This is the reply.</p>");
        out.println("</body></html>"); */
        out.close();
    }
}