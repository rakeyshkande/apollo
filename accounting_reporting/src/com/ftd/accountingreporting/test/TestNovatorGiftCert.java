package com.ftd.accountingreporting.test;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.PrintWriter;
import java.io.IOException;

public class TestNovatorGiftCert extends HttpServlet 
{
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";

    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        
        response.setContentType(CONTENT_TYPE);
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head><title>TestNovatorGiftCert</title></head>");
        out.println("<body>");
        out.println("<p>The servlet has received a GET. This is the reply.</p>");
        out.println("</body></html>");
        out.close();
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        System.out.println(request.getParameter("gift_cert_message"));
        response.setContentType(CONTENT_TYPE);
        PrintWriter out = response.getWriter();
        out.println("<?xml version='1.0' encoding='UTF-8'?>");
        out.println("<gift_cert_response>");
        out.println("<status_code>SUCCESS</status_code>");
        out.println("<gift_cert>");
        out.println("<coupon_number>CS1000</coupon_number> ");
        out.println("<promotion_id>GC800</promotion_id> ");
        out.println("<issue_amount>50</issue_amount> ");
        out.println("<order_id>12345</order_id> ");
        out.println("<redeem_date>06/13/2005</redeem_date>");
        out.println("<expire_date>05/04/2006</expire_date> ");
        out.println("<is_persistent></is_persistent>");
        out.println("</gift_cert> ");
        out.println("</gift_cert_response> ");
        out.close();
	
	
	
	
	

        

    }
}