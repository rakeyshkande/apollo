package com.ftd.accountingreporting.test;

import java.sql.Connection;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import com.ftd.accountingreporting.eventhandler.EndOfDayHandler;

public class EndOfDayHandlerTest  extends TestCase
{
  public EndOfDayHandlerTest(String test)
  {
    super(test);
  }

   public void testDoInvoke() {
      try {
           Connection con = TestHelper.getConnection();
           EndOfDayHandler eod = new EndOfDayHandler();
           eod.invoke(con);
           con.close();
         }catch (Throwable e) {
            e.printStackTrace();
        }
  }

  public static Test suite() {
    	TestSuite suite= new TestSuite();
      
      suite.addTest(new EndOfDayHandlerTest("testDoInvoke"));
      return suite;
 }
 
 public static void main(String[] args){
    junit.textui.TestRunner.run(suite());
  }
}