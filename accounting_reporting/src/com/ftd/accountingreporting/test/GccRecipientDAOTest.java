package com.ftd.accountingreporting.test;

import java.sql.Date;

import java.util.ArrayList;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.ftd.accountingreporting.dao.GccRecipientDAO;
import com.ftd.accountingreporting.vo.GccRecipientVO;
import com.ftd.accountingreporting.constant.ARConstants;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.xml.DOMUtil;

import org.w3c.dom.Document;
/**
 * Fixture to test GccRecipientDA0 class.
 * @author Madhu Parab
 */
public class GccRecipientDAOTest extends TestCase
{
  public GccRecipientDAOTest(String test)
  {
      super(test);
  }
  
  public void testDoInsertRecipient() {
      try {
           
            GccRecipientVO recipient = new GccRecipientVO();
            recipient.setRequestNumber("100000");
            recipient.setFirstName("M");
            recipient.setLastName("P");
            recipient.setAddress1("WEST BROOK DRIVE");
            recipient.setAddress2("SUITE 205");
            recipient.setCity("OAK BROOK");
            recipient.setState("ILLINOIS");
            recipient.setZipCode("66666");
            recipient.setCountry("USA");
            recipient.setPhone("630-630-1234");
            recipient.setEmailAddress("m@hotmail.com");
            
            
            Connection con = TestHelper.getConnection();
            boolean result = (new GccRecipientDAO(con)).doInsertRecipient(recipient);
            con.close();
            System.out.println("Result is "+result);
            //((XMLDocument)result).print(System.out);
            // assertTrue("Result...", result.equals("ACCT"));
            //assertTrue("Result...", result.equals(""));
         }catch (Exception e) {
            e.printStackTrace();
        }
  }
  
 public void testDoDeleteRecipient() {
      try {
           
            Connection con = TestHelper.getConnection();
            boolean result = (new GccRecipientDAO(con)).doDeleteRecipient("100402");
            con.close();
            System.out.println("Result is "+result);
            //((XMLDocument)result).print(System.out);
            // assertTrue("Result...", result.equals("ACCT"));
            //assertTrue("Result...", result.equals(""));
         }catch (Exception e) {
            e.printStackTrace();
        }
  }
  
  public void testDoGetRecipient() {
      try {
           
            Connection con = TestHelper.getConnection();
            Document result = (new GccRecipientDAO(con)).doGetRecipient("100200");
            con.close();
            //System.out.println("Result is "+result);
            DOMUtil.print(result, System.out);
            // assertTrue("Result...", result.equals("ACCT"));
            //assertTrue("Result...", result.equals(""));
         }catch (Exception e) {
            e.printStackTrace();
        }
  }
  
  public void testDoUpdateRecipient() {
      try {
           
            GccRecipientVO recipient = new GccRecipientVO();
            recipient.setGcCouponReceiptId(100400);;
            recipient.setFirstName("Madhu");
            recipient.setLastName("P");
            recipient.setAddress1("WEST BROOK DRIVE");
            recipient.setAddress2("SUITE 205");
            recipient.setCity("OAK BROOK");
            recipient.setState("ILLINOIS");
            recipient.setZipCode("66666");
            recipient.setCountry("USA");
            recipient.setPhone("630-630-1234");
            recipient.setEmailAddress("m@hotmail.com");
            
            
            Connection con = TestHelper.getConnection();
            boolean result = (new GccRecipientDAO(con)).doUpdateRecipient(recipient);
            con.close();
            System.out.println("Result is "+result);
            //((XMLDocument)result).print(System.out);
            // assertTrue("Result...", result.equals("ACCT"));
            //assertTrue("Result...", result.equals(""));
         }catch (Exception e) {
            e.printStackTrace();
        }
  }
  
  public static Test suite() {
    	TestSuite suite= new TestSuite();
      
      //suite.addTest(new GccRecipientDAOTest("testDoInsertRecipient"));
      //suite.addTest(new GccRecipientDAOTest("testDoDeleteRecipient"));
      suite.addTest(new GccRecipientDAOTest("testDoGetRecipient"));
      //suite.addTest(new GccRecipientDAOTest("testDoUpdateRecipient"));

      return suite;
    }
    
  /**
   * You can either user the text ui or swing ui to run the test
   **/
  public static void main(String[] args){
    // pass in the class of the test that needs to be run
    //junit.swingui.TestRunner.run(FrameworkTest.class);
    junit.textui.TestRunner.run(suite());
  }
}