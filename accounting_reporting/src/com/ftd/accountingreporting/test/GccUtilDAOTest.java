package com.ftd.accountingreporting.test;

import java.sql.Date;

import java.util.ArrayList;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.ftd.accountingreporting.dao.GccUtilDAO;
import com.ftd.accountingreporting.constant.ARConstants;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.xml.DOMUtil;

import org.w3c.dom.Document;
/**
 * Fixture to test GccUtilDA0 class.
 * @author Madhu Parab
 */
public class GccUtilDAOTest extends TestCase 
{
  public GccUtilDAOTest(String test)
  {
      super(test);
  }
  
  public void testDoGetBrandNames() {
        try {
           
            Connection con = TestHelper.getConnection();
            Document result = (new GccUtilDAO(con)).doGetCompanyNames();
            con.close();
            DOMUtil.print(((Document)result), System.out);
            // assertTrue("Result...", result.equals("ACCT"));
            //assertTrue("Result...", result.equals(""));
         }catch (Exception e) {
            e.printStackTrace();
        }
  }
  
  public void testDoGetCountryList()
  {
      try
      {
        Connection con = TestHelper.getConnection();
        Document result = (new GccUtilDAO(con)).doGetCountryList();
        con.close();
        DOMUtil.print(((Document)result), System.out);
      }
      catch (Exception e)
      {
        e.printStackTrace();
      }
  }
  
  public void testDoGetStateList()
  {
      try
      {
        Connection con = TestHelper.getConnection();
        Document result = (new GccUtilDAO(con)).doGetStateList();
        con.close();
        DOMUtil.print(((Document)result), System.out);
      }
      catch (Exception e)
      {
        e.printStackTrace();
      }
  }
  
  public void testDoGetSourceCodeListByCompanyId()
  {
      try
      {
        Connection con = TestHelper.getConnection();
        //Document result = (new GccUtilDAO(con)).doGetSourceCodeListByCompanyId("FTD");
        con.close();
        //((XMLDocument)result).print(System.out);
      }
      catch (Exception e)
      {
        e.printStackTrace();
      }
  }
  
  public void testOrderExists()
  {
      try
      {
        Connection con = TestHelper.getConnection();
        boolean result = (new GccUtilDAO(con)).orderExists("FNE349178");
        con.close();
        System.out.println("Order Exists "+result);
      }
      catch (Exception e)
      {
        e.printStackTrace();
      }
  }
    
   /*private Connection getDBConnection() throws Exception
   {
      String dsname = ConfigurationUtil.getInstance().getProperty(ARAdminConstants.CONFIG_FILE,
                                                    ARAdminConstants.DATASOURCE_NAME);
      
      String driver_ = "oracle.jdbc.driver.OracleDriver";
      String database_ = "jdbc:oracle:thin:@stheno-dev.ftdi.com:1521:dev5";
      String user_ = "chu";
      String password_ = "chu";
      Class.forName(driver_);
      return DriverManager.getConnection(database_, user_, password_);
      return  DataSourceUtil.getInstance().getConnection("ORDER SCRUB");
     
   }*/
   
   public static Test suite() {
    	TestSuite suite= new TestSuite();

      //suite.addTest(new GccUtilDAOTest("testDoGetBrandNames"));
      //suite.addTest(new GccUtilDAOTest("testDoGetCountryList"));
      //suite.addTest(new GccUtilDAOTest("testDoGetStateList"));
      //suite.addTest(new GccUtilDAOTest("testDoGetSourceCodeListByCompanyId"));
      suite.addTest(new GccUtilDAOTest("testOrderExists"));
      return suite;
    }
    
  /**
   * You can either user the text ui or swing ui to run the test
   **/
  public static void main(String[] args){
    // pass in the class of the test that needs to be run
    //junit.swingui.TestRunner.run(FrameworkTest.class);
    junit.textui.TestRunner.run(suite());
  }
}
