package com.ftd.accountingreporting.test;
import com.ftd.accountingreporting.util.FtpUtil;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Connection;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class TestFTPUtil extends TestCase 
{

    
    /** 
     * Create a constructor that take a String parameter and passes it 
     * to the super class 
     * @param name String
     **/ 
    public TestFTPUtil(String name) { 
        super(name); 
        try 
        {

        } catch (Exception ex) 
        {
          ex.printStackTrace();
        } 
    }

 	/*
	 * @see TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
	}

	/*
	 * @see TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

    /**
     * Overloaded method for setting up initialization variables 
     * @param fileName String name of the file with the xml
     * to test
     */
    private void setUp(String fileName)
    { 
      try
      {
        System.out.println("getting xml");
        String record = "";
                
        FileReader fr     = new FileReader(fileName);
        
        BufferedReader br = new BufferedReader(fr);
        StringBuffer sb = new StringBuffer();
        while ((record = br.readLine()) != null) {
           sb.append(record);
        }
        System.out.println("got xml");
       // this.orderXMLString = sb.toString();
      //  System.out.println("orderXMLString: " + orderXMLString);
                    
      }
      catch(Exception ex)
      {
        ex.printStackTrace();
      }
    
    } 
    public void testFTP() throws Exception{
        FtpUtil ftpUtil = new FtpUtil();
        File test = new File("c:/ftp_test/testfileforftp");

        ftpUtil.login("apollo.ftdi.com", "/home/chu","chu","kj34nB");
        ftpUtil.getFile("c:/ftp_test","accountingreporting.log");
        ftpUtil.putFile(test,test.getName());
        ftpUtil.logout();
    }
    public static TestSuite suite()
    {
      TestSuite suite = new TestSuite();
      try 
      {
        suite.addTest(new TestFTPUtil("testFTP"));
      // suite.addTest(new TestASKEventDAO("testGetMessageList"));
       // suite.addTest(new TestASKEventDAO("testStoreMessageTran"));


      } catch (Exception e) {
          e.printStackTrace();
      }

      return suite;
    } 

    public static void main(String args[])
{
  junit.textui.TestRunner.run( suite() );
}
}
