package com.ftd.accountingreporting.test;

import java.sql.Date;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.ftd.accountingreporting.dao.GiftCertificateCouponDAO;
import com.ftd.accountingreporting.vo.GiftCertificateCouponVO;
import com.ftd.accountingreporting.vo.GiftCertificateSourceCodeVO;
import com.ftd.accountingreporting.constant.ARConstants;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.xml.DOMUtil;

import org.w3c.dom.Document;
import org.w3c.dom.Document;
/**
 * Fixture to test GiftCertificateCouponDA0 class.
 * @author Madhu Parab
 */
public class GiftCertificateCouponDAOTest extends TestCase 
{
  public GiftCertificateCouponDAOTest(String test)
  {
      super(test);
  }
  
  public void testDoSearchRequest()
  {
      try
      {
          Connection con = TestHelper.getConnection();
          Document result = (new GiftCertificateCouponDAO(con)).doSearchRequest("100200", null);
          con.close();
            
          DOMUtil.print(((Document)result), System.out);
      }
      catch(Exception e)
      {
          e.printStackTrace();
      }
  }
  
  public void testDoSearchGcCoupon()
  {
      try
      {
          Connection con = TestHelper.getConnection();
          Document result = (new GiftCertificateCouponDAO(con)).doSearchGcCoupon(null, "733411844");
          con.close();
            
          DOMUtil.print(((Document)result), System.out);
      }
      catch(Exception e)
      {
          e.printStackTrace();
      }
  }

  public void testDoUpdateGcCouponStatus()
  {
      try
      {
          Connection con = TestHelper.getConnection();
          //boolean result = (new GiftCertificateCouponDAO(con)).doUpdateGcCouponStatus("100200", "Redeemed");
          con.close();
          
          //System.out.println("Result is "+result);
      }
      catch(Exception e)
      {
          e.printStackTrace();
      }
  }
  
  public void testDoGetStatusCount()
  {
      try
      {
          Connection con = TestHelper.getConnection();
          Document result = (new GiftCertificateCouponDAO(con)).doGetStatusCount("100000");
          con.close();
            
          DOMUtil.print(((Document)result), System.out);
      }
      catch(Exception e)
      {
          e.printStackTrace();
      }
  }

  public void testDoCreateGcCouponRequest()
  {
      try
      {
          GiftCertificateCouponVO vo = new GiftCertificateCouponVO();
          vo.setRequestNumber("1116");
          vo.setGcCouponType("CS");
          vo.setIssueAmount(35.45);
          vo.setPaidAmount(50l);
          Calendar cal1 = Calendar.getInstance();
          cal1.setTime(new java.util.Date("03/22/2005"));
          vo.setIssueDate(cal1);
          Calendar cal2 = Calendar.getInstance();
          cal2.setTime(new java.util.Date("04/22/2005"));
          vo.setExpirationDate(cal2);
          vo.setQuantity(2);
          vo.setFormat("$");
          vo.setFileName("FTD");
          vo.setRequestedBy("ALAN");
          vo.setOrderSource("W");
          vo.setProgramName("Discount Program");
          vo.setProgramDesc("Discount Program");
          vo.setProgramComment("Mother's Day Discount Program");
          vo.setCompanyId("5");
          GiftCertificateSourceCodeVO srcCodeVO = new GiftCertificateSourceCodeVO();
          List<String> srcCodesList = new ArrayList<String>();
          srcCodesList.add("23");
          srcCodesList.add("25");
          srcCodeVO.setRestrictedSourceCodes(srcCodesList);
          vo.setSourceCodeVO(srcCodeVO);
          vo.setPrefix("7");
          vo.setOther("8");
          
          Connection con = TestHelper.getConnection();
          boolean result = (new GiftCertificateCouponDAO(con)).doCreateGcCouponRequest(vo);
          con.close();
            
          System.out.println("Result is "+result);
      }
      catch(Exception e)
      {
          e.printStackTrace();
      }
  }
  
  public void testDoCreateGcCoupon()
  {
      try
      {
          GiftCertificateCouponVO vo = new GiftCertificateCouponVO();
          vo.setGcCouponNumber("4");
          vo.setRequestNumber("1116");
          vo.setGcCouponRecipId(100203);
          vo.setIssueAmount(50.45);
          Calendar cal1 = Calendar.getInstance();
          cal1.setTime(new java.util.Date("03/22/2005"));
          vo.setIssueDate(cal1);
          Calendar cal2 = Calendar.getInstance();
          cal2.setTime(new java.util.Date("04/22/2005"));
          vo.setExpirationDate(cal2);
          vo.setGcCouponStatus("Issued Active");
                    
          Connection con = TestHelper.getConnection();
          //boolean result = (new GiftCertificateCouponDAO(con)).doCreateGcCoupon(vo);
          con.close();
            
          //ystem.out.println("Result is "+result);
      }
      catch(Exception e)
      {
          e.printStackTrace();
      }
  }
  
  public void testDoUpdateGcCoupon()
  {
      try
      {
          GiftCertificateCouponVO vo = new GiftCertificateCouponVO();
          vo.setGcCouponNumber("4");
          vo.setGcCouponStatus("Redeemed");
          Calendar cal1 = Calendar.getInstance();
          cal1.setTime(new java.util.Date("05/22/2005"));
          vo.setGcRedemptionDate(cal1);
          Calendar cal2 = Calendar.getInstance();
          cal2.setTime(new java.util.Date("06/22/2005"));
          vo.setGcReinstateDate(cal2);
          vo.setGcOrderDetailId(5);
                    
          Connection con = TestHelper.getConnection();
          boolean result = (new GiftCertificateCouponDAO(con)).doUpdateGcCoupon(vo);
          con.close();
            
          System.out.println("Result is "+result);
      }
      catch(Exception e)
      {
          e.printStackTrace();
      }
  }
  
  public static Test suite() {
    	TestSuite suite= new TestSuite();
      
      suite.addTest(new GiftCertificateCouponDAOTest("testDoSearchRequest"));
      //suite.addTest(new GiftCertificateCouponDAOTest("testDoSearchGcCoupon"));
      //suite.addTest(new GiftCertificateCouponDAOTest("testDoUpdateGcCouponStatus"));
      //suite.addTest(new GiftCertificateCouponDAOTest("testDoGetStatusCount"));
      //suite.addTest(new GiftCertificateCouponDAOTest("testDoCreateGcCouponRequest"));
      //suite.addTest(new GiftCertificateCouponDAOTest("testDoCreateGcCoupon"));
      //suite.addTest(new GiftCertificateCouponDAOTest("testDoUpdateGcCoupon"));

      return suite;
    }
    
  /**
   * You can either user the text ui or swing ui to run the test
   **/
  public static void main(String[] args){
    // pass in the class of the test that needs to be run
    //junit.swingui.TestRunner.run(FrameworkTest.class);
    junit.textui.TestRunner.run(suite());
  }
}
