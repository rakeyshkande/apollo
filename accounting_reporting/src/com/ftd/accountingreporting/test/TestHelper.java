package com.ftd.accountingreporting.test;

import com.ftd.osp.utilities.dataaccess.util.OSPConnectionPool;
import java.sql.*;
 

public class TestHelper
{

      private static final String conUrl = "jdbc:oracle:thin:@adonis.ftdi.com:1522:dev5";
      private static final String username = "osp";
      private static final String credential = "osp";
      
      public TestHelper()
      {

      }

      public static Connection getConnection()
      {
           Connection conn = null;
           try
           {
              OSPConnectionPool pool = OSPConnectionPool.getInstance();        
              conn = pool.getConnection(conUrl, username, credential);
           }
           catch(Exception e)
           {
               e.printStackTrace();
           }
           return conn;        
     }
}
