package com.ftd.accountingreporting.test;

import java.sql.Connection;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import com.ftd.accountingreporting.eventhandler.VendorEOMHandler;

public class VendorEOMHandlerTest  extends TestCase
{
  public VendorEOMHandlerTest(String test)
  {
    super(test);
  }

   public void testDoInvoke() {
      try {
           Connection con = TestHelper.getConnection();
           VendorEOMHandler eod = new VendorEOMHandler();
           eod.invoke(con);
           con.close();
         }catch (Throwable e) {
            e.printStackTrace();
        }
  }

  public static Test suite() {
    	TestSuite suite= new TestSuite();
      
      suite.addTest(new VendorEOMHandlerTest("testDoInvoke"));
      return suite;
 }
 
 public static void main(String[] args){
    junit.textui.TestRunner.run(suite());
  }
}