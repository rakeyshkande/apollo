package com.ftd.accountingreporting.giftcertfeed;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.util.XMLUtil;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;

/**
 * This is a factory class that returns a concrete handler base on
 * the payload.
 * @author Christy Hu
 */
public class PartnerFeedHandlerFactory
{
    /**
     * Singleton instance of the object.
     */
    private static PartnerFeedHandlerFactory INSTANCE; 
    protected Logger logger = 
        new Logger("com.ftd.accountingreporting.giftcertfeed.PartnerFeedHandlerFactory");
  
    private PartnerFeedHandlerFactory()
    {
    }
    
    /**
     * Return the PartnerFeedHandlerFactory instance.  It ensures that at a 
     * given time there is only a single instance.
     * @param n/a
     * @return PartnerFeedHandlerFactory
     * @throws Exception
     */ 
    public static PartnerFeedHandlerFactory getInstance(){
        if (INSTANCE == null) {
          INSTANCE = new PartnerFeedHandlerFactory();
        }
        return INSTANCE;    
    }
    
    public PartnerFeedHandler getHandler(Object payload) throws Exception {
        PartnerFeedHandler handler = null;
        try{
            String className = null;
            String message = (String)((MessageToken)payload).getMessage();
            String eventName = XMLUtil.getPayloadData(payload, "/root/event/text()");
            String prgrmName = XMLUtil.getPayloadData(payload, "/root/partner-program/text()");
            String propertyName = eventName + "." + prgrmName;
            
            // try looking for the handler with the payload.
            className = ConfigurationUtil.getInstance().getProperty(
                  ARConstants.GC_FEED_HANDLER_FILE, propertyName);
                
            // If a handler is not found, attempt to find
            // a handler that is not qualified by the partner program name.
            if(className == null || className == "") {
                // if handler class not found, try the event + default 
                // to get the default handler for the event.
                propertyName = eventName;
                className = ConfigurationUtil.getInstance().getProperty(
                        ARConstants.GC_FEED_HANDLER_FILE, propertyName);
            } 
            
            if(className == null || className == "") {
                // if handler class not found, try the event + default 
                // to get the default handler for the event.
                propertyName = eventName + "." + ARConstants.HANDLER_DEFAULT;
                className = ConfigurationUtil.getInstance().getProperty(
                        ARConstants.GC_FEED_HANDLER_FILE, propertyName);
            } 
            
            if(className != null && className != "") {
                 handler = loadHandler(className);
            }   
            logger.debug("Class name: " + className);
        } catch (Exception e) {
            throw e;
        }
        return handler;
    }
    
    private PartnerFeedHandler loadHandler(String className) throws Exception {
        logger.debug("Entering loadHandler..."); 
        logger.debug("class name : " + className);
        
        PartnerFeedHandler handler =  
            (PartnerFeedHandler)Class.forName(className).newInstance();
        return handler;
    }
}