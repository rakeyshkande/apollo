package com.ftd.accountingreporting.giftcertfeed.handler;

import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.exception.EmailDeliveryException;
import com.ftd.accountingreporting.giftcertfeed.PartnerFeedHandler;
import com.ftd.accountingreporting.giftcertfeed.dao.GCProgramDAO;
import com.ftd.accountingreporting.giftcertfeed.util.FeedTemplateReader;
import com.ftd.accountingreporting.giftcertfeed.vo.FileFieldVO;
import com.ftd.accountingreporting.giftcertfeed.vo.ProgramConfigVO;
import com.ftd.accountingreporting.giftcertfeed.vo.TemplateFileVO;
import com.ftd.accountingreporting.util.StringUtil;
import com.ftd.accountingreporting.util.XMLUtil;

import com.ftd.novator.giftcert.vo.GiftCertMessageVO;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * This class is a handler for inbound activation file
 *
 * @author JP Puzon
 */
public class VanillaInboundStatusHandler extends PartnerFeedHandler {
    protected Logger logger = new Logger(
            "com.ftd.accountingreporting.giftcertfeed.VanillaInboundStatusHandler");

    public void processFeed(Object payload) throws Exception {
        //        ftpPath = GENERIC_FTP_LOCATION_FF;
        super.processFeed(payload);

        String inStatusChange = XMLUtil.getPayloadData(payload,
                "/root/status/text()");

        if ((inStatusChange == null) || inStatusChange.trim().equals("")) {
            throw new Exception("processFeed: Partner program " +
                programName + " inStatusChange value is invalid: " +
                inStatusChange);
        }

        String PARM_NAME = null;

        if (inStatusChange.equals(ARConstants.GCC_STATUS_ISSUED_ACTIVE)) {
            PARM_NAME = "inbound_act";
        } else if (inStatusChange.equals(ARConstants.GCC_STATUS_CANCELLED)) {
            PARM_NAME = "inbound_deact";
        } else {
            throw new Exception("processFeed: Partner program " +
                programName + " inStatusChange value is unrecognized: " +
                inStatusChange);
        }

        String inFilename = XMLUtil.getPayloadData(payload,
                "/root/filename/text()");

        if ((inFilename == null) || inFilename.trim().equals("")) {
            throw new Exception("processFeed: Partner program " +
                programName + " inFilename value is invalid: " + inFilename);
        }

        try {
            // Find template name and inbound file name from program name 
            ProgramConfigVO programInboundConfig = getProgramConfig(PARM_NAME,
                    programName);

            // Load inbound info from template file.
            //            FeedTemplateReader objRetriever = new FeedTemplateReader(programInboundConfig.getTemplateName());
            TemplateFileVO inboundTemplate = new FeedTemplateReader(programInboundConfig.getTemplateName()).getFile();

            String delim = inboundTemplate.getDelim();

            // Get file from server.
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            File inboundFile = getFileFromServerWithTimestamp(getDbConfigurationParam(
                            "gcVanillaFtpServer"),
                        getSecureConfigurationParam("gcVanillaServerUsername"),
                        getSecureConfigurationParam("gcVanillaServerPassword"),
                        getDbConfigurationParam("gcVanillaFtpLocationInbound"),
                        inFilename,
                        getDbConfigurationParam("gcVanillaLocalLocationInbound") +
                        programName + "/inbound/");

            if (inboundFile != null) {
                // Compose the local out file name.
                Date now = new Date();
                String outFilename = inboundFile.getName() + "_VER";

                //                gccNumberList = new ArrayList();
                FileReader readFile = new FileReader(inboundFile);
                BufferedReader input = new BufferedReader(readFile);

                // Retrieve all fields from template.
                List fieldList = inboundTemplate.getFields();

                List gcMsgVOList = new ArrayList();
                String line = null;

                while ((line = input.readLine()) != null) {
                    if (!line.equals("")) {
                        // Default the result as a failure.
                        String responseLine = line +
                            inboundTemplate.getDelim() + "N";

                        try {
                            //                            Map inputParams = new HashMap();
                            //String fieldName = null; 
                            String serialNumber = null;

                            for (int i = 0; i < fieldList.size(); i++) {
                                FileFieldVO fieldvo = (FileFieldVO) fieldList.get(i);
                                String fieldName = fieldvo.getFieldName();
                                int position = Integer.parseInt(fieldvo.getFieldPosition());

                                // use position to find value in the line read in.
                                String value = StringUtil.getToken(line, delim,
                                        position);

                                // add parameter in map
                                //                                inputParams.put(fieldName, value);
                                if (fieldName.equalsIgnoreCase(
                                            "IN_SERIAL_NUMBER")) {
                                    //                                    gccNumberList.add(value);
                                    serialNumber = value;
                                    logger.debug("***coupon number***" +
                                        serialNumber);
                                }
                            }

                            // Modify gcc status
                            String gcCouponNumber = this.updateGiftCertStatus(serialNumber,
                                    programName, inStatusChange);

                            // Update program request process by marking the inbound date.
                            super.insertProgramProcess(gcCouponNumber, PARM_NAME);

                            // Compose a list of gc message objects.
                            GiftCertMessageVO gcMsgVO = composeGiftCertMessageVO(gcCouponNumber,
                                    inStatusChange);

                            if (gcMsgVO != null) {
                                gcMsgVOList.add(gcMsgVO);
                            }
                            
                            // Compose the success response line.
                            responseLine = line + inboundTemplate.getDelim() +
                                "Y";
                        } catch (Exception e) {
                            // Record the single line that failed.
                            logger.error("processFeed: Line content failed: " +
                                line, e);
                        }

                        // Write the status to the response file.
                        writeToResponseFile(getDbConfigurationParam(
                                "gcVanillaLocalLocationOutbound") +
                            programName + "/outbound/", outFilename, true,
                            responseLine);
                    }
                }

                input.close();
                readFile.close();
                
                // Send to Novator gift cert q
                if (gcMsgVOList.size() > 0) {
                    super.sendToNovatorQ(gcMsgVOList);
                }

                // Send email notification
                sendSuccessEmail(programName, PARM_NAME);
            }
        } catch (EmailDeliveryException ede) {
            logger.error(ede);

            try {
                sendSystemMessage("processFeed: Email delivery failed.", ede);
            } catch (Exception ede2) {
                logger.error(ede2);
            }
        } catch (Exception e) {
            logger.error(e);

            try {
                sendSystemMessage("processFeed: Could not proceed because of unanticipated critical error.",
                    e);
            } catch (Exception e2) {
                logger.error(e2);
            }
        }
    }
}
