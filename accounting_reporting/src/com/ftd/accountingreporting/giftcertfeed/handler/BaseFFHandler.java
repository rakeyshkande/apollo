package com.ftd.accountingreporting.giftcertfeed.handler;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.exception.EmailDeliveryException;
import com.ftd.accountingreporting.giftcertfeed.PartnerFeedHandler;
import com.ftd.accountingreporting.giftcertfeed.dao.GCProgramDAO;
import com.ftd.accountingreporting.giftcertfeed.util.FeedTemplateReader;
import com.ftd.accountingreporting.giftcertfeed.vo.FileFieldVO;
import com.ftd.accountingreporting.giftcertfeed.vo.GcCouponVO;
import com.ftd.accountingreporting.giftcertfeed.vo.ProgramConfigVO;
import com.ftd.accountingreporting.giftcertfeed.vo.TemplateFileVO;
import com.ftd.accountingreporting.util.StringUtil;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class is a handler for inbound fulfillment file
 * 
 * @author Christy Hu
 */
public class BaseFFHandler extends PartnerFeedHandler
{
    protected Logger logger = 
        new Logger("com.ftd.accountingreporting.giftcertfeed.BaseGCHandler");
    
    // process indicator - sending gift cert fulfillment file.
    protected String PARM_NAME = "inbound_ff";
    
    public void processFeed(Object payload) throws Exception {
        ftpPath = GENERIC_FTP_LOCATION_FF;
        super.processFeed(payload);
        GCProgramDAO programDao = new GCProgramDAO(conn);
       
        try {
            // Find template name and inbound file name from program name 
            ProgramConfigVO programConfig = new ProgramConfigVO();
            programConfig.setParmName(PARM_NAME);
            programConfig.setProgramName(programName);
            programDao.doGetProgramConfig(programConfig);
            
            // Load info from template file.
            FeedTemplateReader objRetriever = new FeedTemplateReader(programConfig.getTemplateName());
            TemplateFileVO template = objRetriever.getFile();
            
            String delim = template.getDelim();
            
            // Get file from server.
            String inFilename = programConfig.getFileName();
            File inboundFile = getFileFromGenericServer(inFilename);
            
            if(inboundFile != null){
                gccNumberList = new ArrayList();
                FileReader readFile = new FileReader(inboundFile);
                BufferedReader input = new BufferedReader(readFile);
                
                // Retreive all fields from template.
                List fieldList = template.getFields();
                String statementId = template.getStatementId();
                
                String line = null;
                
                while (( line = input.readLine()) != null){
                    try {
                        if(!line.equals("")){
                            Map inputParams = new HashMap();
                            //String fieldName = null; 
                            String couponNumber = null;
                            
                            for(int i = 0; i < fieldList.size(); i++) {
                                FileFieldVO fieldvo = (FileFieldVO)fieldList.get(i);
                                String fieldName = fieldvo.getFieldName();
                                int position = Integer.parseInt(fieldvo.getFieldPosition());
                                    
                                // use position to find value in the line read in.
                                String value = StringUtil.getToken(line, delim, position);
    
                                // add parameter in map
                                inputParams.put(fieldName, value);
                                if(fieldName.equalsIgnoreCase(IN_GC_COUPON_NUMBER)) {
                                    gccNumberList.add(value);
                                    couponNumber = value;
                                    logger.debug("***coupon number***" + couponNumber);
                                }
                            }
                            persistPartnerData(statementId, inputParams); 
                            // Send to Novator gift cert q
                            
                            super.sendToNovatorQ(couponNumber);
                        }
                    } catch (Exception e) {
                        logger.error(e);
                    }
                } 
                input.close();
                readFile.close();        
               
                // Activate gcc
                this.activateGiftCerts();
                
                // Update program request process by marking the oubound_gc date.
                super.updateFeedProcess(PARM_NAME);
                
                // Send email notification
                sendSuccessEmail(programName, PARM_NAME);
            } else {
                try {
                    sendErrorEmail(programName, PARM_NAME, "File not found: " + inFilename);
                } catch (EmailDeliveryException ede) {
                    logger.error(ede);
                } catch (Exception e) {
                    throw e;
                }
            }
        } catch (EmailDeliveryException ede) {
            logger.error(ede);
        } catch (Exception e) {
            logger.error(e);
        }
    }
    
    protected void activateGiftCerts() throws Exception {
        if(gccNumberList != null) {
            GCProgramDAO gcDao = new GCProgramDAO(conn);
            for(int i = 0; i < gccNumberList.size(); i++) {
                String gccNumber = (String)gccNumberList.get(i);
                GcCouponVO vo = gcDao.doGetGcCoupon(gccNumber);
                if (vo != null) {
                    String gcCouponStatus = vo.getGcCouponStatus();
                    if(ARConstants.GCC_STATUS_ISSUED_INACTIVE.equals(gcCouponStatus)) {
                        gcDao.doUpdateGiftCertStatus(gccNumber, ARConstants.GCC_STATUS_ISSUED_ACTIVE);
                    } else {
                        logger.debug("Attempting to activate GC already activated: " + gccNumber);
                    } 
                } else {
                    super.sendErrorEmail("Discover", "GC Activations", "Trying to activate GC not in Apollo" + gccNumber);
                }
            }
        }        
    }    
    
}