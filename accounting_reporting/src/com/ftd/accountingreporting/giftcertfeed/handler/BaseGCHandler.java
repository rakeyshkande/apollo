package com.ftd.accountingreporting.giftcertfeed.handler;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.exception.EmailDeliveryException;
import com.ftd.accountingreporting.giftcertfeed.PartnerFeedHandler;
import com.ftd.accountingreporting.giftcertfeed.dao.GCProgramDAO;
import com.ftd.accountingreporting.giftcertfeed.util.FeedTemplateReader;
import com.ftd.accountingreporting.giftcertfeed.vo.ProgramConfigVO;
import com.ftd.accountingreporting.giftcertfeed.vo.TemplateFileVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.File;

/**
 * This class is a handler for outbound inventory file
 * 
 * @author Christy Hu
 */
public class BaseGCHandler extends PartnerFeedHandler
{
    protected Logger logger = 
        new Logger("com.ftd.accountingreporting.giftcertfeed.BaseGCHandler");
    
    // process indicator - sending initial inventory gift cert file.
    protected String PARM_NAME = "outbound_gc";
    
    public void processFeed(Object payload) throws Exception {
        ftpPath = GENERIC_FTP_LOCATION_GC;
        super.processFeed(payload);
        try {
            
            File outboundFile = this.createFile(programName);
                    
            try {
                if(outboundFile != null) {
                    // FTP file to Hawk
                    super.putFileToGenericServer(outboundFile);
                        
                    // Update program request process by marking the oubound_gc date.
                    super.updateFeedProcess(PARM_NAME);
                }
                   
                // Send email notification
                sendSuccessEmail(programName, PARM_NAME);
            } catch (Exception e) {
                logger.error(e);
                try 
                {
                    sendErrorEmail(programName, PARM_NAME, EMAIL_ERROR_MSG);
                } catch (Exception ee) {
                    logger.error(ee);
                }
            }
              
        } catch (EmailDeliveryException ede) {
            logger.error(ede);
        } catch (Exception e) {
            logger.error(e);
            try {
                sendErrorEmail(programName, PARM_NAME, EMAIL_ERROR_MSG);
            } catch (Exception ee) {
                logger.error(ee);
            }
        } 
    }
    
    protected File createFile(String programName) throws Exception {

        GCProgramDAO programDao = new GCProgramDAO(conn);
        File outboundFile = null;
        
        // Find template name and outbound file name from program name - payload
        ProgramConfigVO programConfig = new ProgramConfigVO();
        programConfig.setParmName(PARM_NAME);
        programConfig.setProgramName(programName);
        programDao.doGetProgramConfig(programConfig);
            
        // Prepend file path
        String outfileName = programConfig.getFileName();
        String outfileLocalPath =  ConfigurationUtil.getInstance().getProperty
                (ARConstants.CONFIG_FILE, LOCAL_FILE_LOCATION);
        programConfig.setFileName(outfileLocalPath + outfileName);
            
        // Load info from template file.
        FeedTemplateReader objRetriever = new FeedTemplateReader(programConfig.getTemplateName());
        TemplateFileVO template = objRetriever.getFile();
            
        // Find coupons that need to send initial gc inventory file out.
        CachedResultSet crs = programDao.doGetOutboundData(template.getStatementId(), programName);
            
        if(crs != null) {
            // Use template to create file
            outboundFile = super.createOutboundFile(crs, programConfig.getFileName(), template);

        } else {
            logger.debug("no data found");
        }
        return outboundFile;
    }    
}