package com.ftd.accountingreporting.giftcertfeed.handler;

import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.exception.EmailDeliveryException;
import com.ftd.accountingreporting.giftcertfeed.PartnerFeedHandler;
import com.ftd.accountingreporting.giftcertfeed.util.FeedTemplateReader;
import com.ftd.accountingreporting.giftcertfeed.vo.FileFieldVO;
import com.ftd.accountingreporting.giftcertfeed.vo.ProgramConfigVO;
import com.ftd.accountingreporting.giftcertfeed.vo.TemplateFileVO;
import com.ftd.accountingreporting.util.StringUtil;
import com.ftd.accountingreporting.util.XMLUtil;

import com.ftd.novator.giftcert.vo.GiftCertMessageVO;

import com.ftd.osp.utilities.plugins.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * This class is a handler for inbound activation file
 *
 * @author JP Puzon
 */
public class IntuitInboundStatusHandler extends PartnerFeedHandler {
    protected Logger logger = new Logger(
            "com.ftd.accountingreporting.giftcertfeed.IntuitInboundStatusHandler");

    public void processFeed(Object payload) throws Exception {
        //        ftpPath = GENERIC_FTP_LOCATION_FF;
        super.processFeed(payload);

        String inStatusChange = XMLUtil.getPayloadData(payload,
                "/root/status/text()");

        if ((inStatusChange == null) || inStatusChange.trim().equals("")) {
            throw new Exception("processFeed: Partner program " + programName +
                " inStatusChange value is invalid: " + inStatusChange);
        }

        String PARM_NAME = null;
        String outboundResponseParmName = null;

        if (inStatusChange.equals(ARConstants.GCC_STATUS_ISSUED_ACTIVE)) {
            PARM_NAME = "inbound_act";
            outboundResponseParmName = "outbound_act_resp";
        } else if (inStatusChange.equals(ARConstants.GCC_STATUS_CANCELLED)) {
            PARM_NAME = "inbound_deact";
            outboundResponseParmName = "outbound_deact_resp";
        } else {
            throw new Exception("processFeed: Partner program " + programName +
                " inStatusChange value is unrecognized: " + inStatusChange);
        }

        try {
            // Find template name and inbound file name from program name 
            ProgramConfigVO programInboundConfig = getProgramConfig(PARM_NAME,
                    programName);

            // Load inbound info from template file.
            //            FeedTemplateReader objRetriever = new FeedTemplateReader(programInboundConfig.getTemplateName());
            TemplateFileVO inboundTemplate = new FeedTemplateReader(programInboundConfig.getTemplateName()).getFile();

            String delim = inboundTemplate.getDelim();

            // Get file from server.
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            String inFilename = programInboundConfig.getFileName() +
                sdf.format(new Date());

            File inboundFile = null;

            try {
                inboundFile = getFileFromServerWithTimestamp(getDbConfigurationParam(
                            "gcIntuitFtpServer"),
                        getSecureConfigurationParam("gcIntuitServerUsername"),
                        getSecureConfigurationParam("gcIntuitServerPassword"),
                        getDbConfigurationParam("gcIntuitFtpLocationInbound"),
                        inFilename,
                        getDbConfigurationParam("gcIntuitLocalLocationInbound"));
            } catch (Exception e) {
                logger.error("processFeed: Could not obtain inbound file.", e);
                sendErrorEmail(programName, PARM_NAME,
                    "Could not obtain inbound file " + inFilename + ": " +
                    e.getMessage());
            }

            if (inboundFile != null) {
                // Initialize params for the output file.
                ProgramConfigVO programOutboundConfig = getProgramConfig(outboundResponseParmName,
                        programName);

                // Compose the local out and remote file names.
                Date now = new Date();
                String outFilename = inboundFile.getName() + "_VER";
                String remoteFilename = programOutboundConfig.getFileName() +
                    sdf.format(now) + "_VER";

                // Load outbound info from template file.
                //            FeedTemplateReader objRetriever = new FeedTemplateReader(programOutboundConfig.getTemplateName());
                //                TemplateFileVO outboundTemplate = new FeedTemplateReader(programOutboundConfig.getTemplateName()).getFile();
                //                gccNumberList = new ArrayList();
                FileReader readFile = new FileReader(inboundFile);
                BufferedReader input = new BufferedReader(readFile);

                // Retrieve all fields from template.
                List fieldList = inboundTemplate.getFields();

                //                String statementId = inboundTemplate.getStatementId();
                List gcMsgVOList = new ArrayList();
                String line = null;

                while ((line = input.readLine()) != null) {
                    if (!line.equals("")) {
                        // Default the result as a failure.
                        String responseLine = line +
                            inboundTemplate.getDelim() + "N";

                        try {
                            //                            Map inputParams = new HashMap();
                            //String fieldName = null; 
                            String serialNumber = null;

                            for (int i = 0; i < fieldList.size(); i++) {
                                FileFieldVO fieldvo = (FileFieldVO) fieldList.get(i);
                                String fieldName = fieldvo.getFieldName();
                                int position = Integer.parseInt(fieldvo.getFieldPosition());

                                // use position to find value in the line read in.
                                String value = StringUtil.getToken(line, delim,
                                        position);

                                // add parameter in map
                                //                                inputParams.put(fieldName, value);
                                if (fieldName.equalsIgnoreCase(
                                            "IN_SERIAL_NUMBER")) {
                                    //                                    gccNumberList.add(value);
                                    serialNumber = value;
                                    logger.debug("***coupon number***" +
                                        serialNumber);
                                }
                            }

                            // Modify gcc status
                            String gcCouponNumber = this.updateGiftCertStatus(serialNumber,
                                    programName, inStatusChange);

                            // Update program request process by marking the inbound date.
                            super.insertProgramProcess(gcCouponNumber, PARM_NAME);
                            
                            // Compose a list of gc message objects.
                            GiftCertMessageVO gcMsgVO = composeGiftCertMessageVO(gcCouponNumber,
                                    inStatusChange);

                            if (gcMsgVO != null) {
                                gcMsgVOList.add(gcMsgVO);
                            }

                            // Compose the success response line.
                            responseLine = line + inboundTemplate.getDelim() +
                                "Y";
                        } catch (Exception e) {
                            // Record the single line that failed.
                            logger.error("processFeed: Line content failed: " +
                                line, e);
                        }

                        // Write the status to the response file.
                        writeToResponseFile(getDbConfigurationParam(
                                "gcIntuitLocalLocationOutbound"), outFilename,
                            true, responseLine);
                    }
                }

                input.close();
                readFile.close();

                // Send to Novator gift cert q
                if (gcMsgVOList.size() > 0) {
                    super.sendToNovatorQ(gcMsgVOList);
                }

                // Touch the response file since we need to send a blank
                // even if no input records exist.
                writeToResponseFile(getDbConfigurationParam(
                        "gcIntuitLocalLocationOutbound"), outFilename, true, "");

                // Send the response file.
                if (!putFileToServer(getDbConfigurationParam("gcIntuitFtpServer"),
                            getSecureConfigurationParam("gcIntuitServerUsername"),
                            getSecureConfigurationParam("gcIntuitServerPassword"),
                            getDbConfigurationParam("gcIntuitFtpLocationOutbound"),
                            outFilename,
                            getDbConfigurationParam(
                                "gcIntuitLocalLocationOutbound"), remoteFilename)) {
                    sendErrorEmail(programName, PARM_NAME,
                        "Could not overwrite existing response file: " +
                        getDbConfigurationParam("gcIntuitFtpServer") + ":" +
                        getDbConfigurationParam("gcIntuitFtpLocationOutbound") +
                        File.separator + remoteFilename);
                } else {
                    // Send email notification
                    sendSuccessEmail(programName, PARM_NAME);
                }
            }
        } catch (EmailDeliveryException ede) {
            logger.error(ede);

            try {
                sendSystemMessage("processFeed: Email delivery failed.", ede);
            } catch (Exception ede2) {
                logger.error(ede2);
            }
        } catch (Exception e) {
            logger.error(e);

            try {
                sendSystemMessage("processFeed: Could not proceed because of unanticipated critical error.",
                    e);
            } catch (Exception e2) {
                logger.error(e2);
            }
        }
    }
}
