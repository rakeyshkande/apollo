package com.ftd.accountingreporting.giftcertfeed.handler;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * This class is a handler for outbound redemption file
 * 
 * @author Christy Hu
 */
public class BaseRDHandler extends BaseGCHandler
{
    protected Logger logger = 
        new Logger("com.ftd.accountingreporting.giftcertfeed.BaseRDHandler");
    
    // process indicator - sending gift cert redemption file.
    public void processFeed(Object payload) throws Exception {
        ftpPath = GENERIC_FTP_LOCATION_RD;
        super.PARM_NAME = "outbound_rd";
        super.processFeed(payload);
    }
}