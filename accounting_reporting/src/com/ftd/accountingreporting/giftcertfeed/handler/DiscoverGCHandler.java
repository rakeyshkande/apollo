package com.ftd.accountingreporting.giftcertfeed.handler;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.exception.EmailDeliveryException;
import com.ftd.accountingreporting.util.AccountingUtil;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.xml.DOMUtil;
import java.io.File;
import java.sql.Connection;

import java.util.Calendar;
import java.util.Date;
import org.w3c.dom.Document;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * This class is a handler for processing Discover (DISC2) gift cert
 * inventory feed.
 * @author Christy Hu
 */
public class DiscoverGCHandler extends BaseGCHandler
{
    private Logger logger = 
        new Logger("com.ftd.accountingreporting.giftcertfeed.DiscoverGCHandler");
    
    public DiscoverGCHandler()
    {
    }

    public void processFeed(Object payload) throws Exception {
        
        AccountingUtil acctgUtil = new AccountingUtil();
        try{
            // If today is not a weekend day (Saturday or Sunday) process it. 
            // Otherwise requeue message with 24 hr delay
            if(!acctgUtil.isWeekend()) {
                super.processFeed(payload);   
            } else {
                Document doc = DOMUtil.getDocument((String)((MessageToken)payload).getMessage());
                
                // queue event with 24 hours delay
                
                AccountingUtil.queueToEventsQueue(ARConstants.EVENT_CONTEXT,
                                                  ARConstants.EVENT_GC, 
                                                  String.valueOf(24 * 60 * 60), 
                                                  doc);

            }  
            
        } catch (EmailDeliveryException ede) {
            logger.error(ede);
        } catch (Exception e) {
            logger.error(e);
            sendErrorEmail(programName, PARM_NAME, EMAIL_ERROR_MSG);
        } 
    }
    

}
