package com.ftd.accountingreporting.giftcertfeed;

import com.enterprisedt.net.ftp.FTPException;

import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.exception.EmailDeliveryException;
import com.ftd.accountingreporting.giftcertfeed.dao.GCProgramDAO;
import com.ftd.accountingreporting.giftcertfeed.vo.FileFieldVO;
import com.ftd.accountingreporting.giftcertfeed.vo.ProgramConfigVO;
import com.ftd.accountingreporting.giftcertfeed.vo.TemplateFileVO;
import com.ftd.accountingreporting.novator.vo.GiftCertVO;
import com.ftd.accountingreporting.util.EmailUtil;
import com.ftd.accountingreporting.util.FileArchiveUtil;
import com.ftd.accountingreporting.util.FileCompressionUtil;
import com.ftd.accountingreporting.util.FtpUtil;
import com.ftd.accountingreporting.util.SystemMessager;
import com.ftd.accountingreporting.util.XMLUtil;
import com.ftd.accountingreporting.vo.GiftCertificateCouponVO;

import com.ftd.novator.giftcert.util.MessageUtil;
import com.ftd.novator.giftcert.vo.GiftCertMessageVO;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.exception.SystemMessengerException;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.vo.NotificationVO;
import com.ftd.osp.utilities.xml.DOMUtil;

import org.apache.xpath.XPathAPI;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import org.xml.sax.SAXException;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import java.sql.Connection;
import java.sql.SQLException;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;


/**
 * This class is a base handler for all gift cert partner program feed.
 * @author Christy Hu
 */
public abstract class PartnerFeedHandler implements IPartnerFeed {
    protected static Logger logger = new Logger(
            "com.ftd.accountingreporting.giftcertfeed.PartnerFeedHandler");
    protected final static String EMAIL_ERROR_MSG = " process failed.";
    protected final static String REQUEST_NUMBER = "request_number";
    protected final static String IN_GC_COUPON_NUMBER = "in_gc_coupon_number";
    protected final static String OUT_GC_COUPON_NUMBER = "gc_coupon_number";

    //location to store local files
    protected final static String LOCAL_FILE_LOCATION = "gcProgramLocalLocation";

    //location to put files on ftp server
    protected final static String GENERIC_FTP_LOCATION = "gcProgramGenericFtpLocation";
    protected final static String GENERIC_FTP_SERVER = "gcProgramGenericFtpServer";
    protected final static String GENERIC_FTP_USERNAME = "gcProgramGenericServerUsername";
    protected final static String GENERIC_FTP_PASSWORD = "gcProgramGenericServerPassword";
    protected final static String GENERIC_FTP_LOCATION_GC = "gcProgramGenericFtpLocationGC";
    protected final static String GENERIC_FTP_LOCATION_FF = "gcProgramGenericFtpLocationFF";
    protected final static String GENERIC_FTP_LOCATION_RD = "gcProgramGenericFtpLocationRD";
    private static ConfigurationUtil configurationUtil;

    static {
        try {
            configurationUtil = ConfigurationUtil.getInstance();
        } catch (Exception e) {
            logger.error("static: ConfigurationUtil initialization failed.", e);
            throw new RuntimeException(e);
        }
    }

    protected boolean obtainedLock;
    protected Connection conn = null;
    protected List gccNumberList = null; // for process update
    protected String programName = null;
    protected String ftpPath = null;

    public void processFeed(Object payload) throws Exception {
        // archive old files.
        archiveFiles();
        programName = getProgramName(payload);
    }

    /**
     * Creates a file in the format of the template.
     * @param crs
     * @param templateFile
     * @return
     * @throws java.lang.Exception
     */
    public File createOutboundFile(CachedResultSet crs, String outfileName,
        TemplateFileVO templateFile) throws Exception {
        List fieldList = templateFile.getFields();
        File outFile = new File(outfileName);
        FileWriter outWriter = new FileWriter(outFile);
        String fileDelim = templateFile.getDelim();
        String gccNumber = null;
        gccNumberList = new ArrayList();

        while (crs.next()) {
            for (int i = 1; i <= fieldList.size(); i++) {
                FileFieldVO ffv = templateFile.getFieldByPosition(i);

                if (ffv != null) {
                    String fieldName = ffv.getFieldName();
                    String fieldValue = "";

                    if (fieldName.equals(ARConstants.FILE_FIELD_NAME_CONST)) {
                        // if field name is const, pull the const value from config file.
                        fieldValue = ffv.getFieldValue();
                    } else if (!fieldName.equals(
                                ARConstants.FILE_FIELD_NAME_NULL)) {
                        // if field name is not null, use field name as a key 
                        // to retrive value from result set.
                        fieldValue = crs.getString(fieldName);
                        fieldValue = (fieldValue == null) ? "" : fieldValue.trim();
                    }

                    outWriter.write(fieldValue);

                    // update gccNumberList
                    if (fieldName.equalsIgnoreCase(OUT_GC_COUPON_NUMBER)) {
                        gccNumberList.add(fieldValue);
                    }
                }

                if (i != fieldList.size()) {
                    outWriter.write(fileDelim);
                }
            }

            outWriter.write("\n");
        }

        outWriter.close();

        return outFile;
    }

    public void putFileToGenericServer(File file) throws Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering putFtpFileToServer");
        }

        try {
            FtpUtil ftpUtil = new FtpUtil();

            /* retrieve server name to FTP to */
            String ftpLocation = ConfigurationUtil.getInstance().getFrpGlobalParm(ARConstants.CONFIG_CONTEXT,
                    ftpPath);
            String ftpServer = ConfigurationUtil.getInstance().getFrpGlobalParm(ARConstants.CONFIG_CONTEXT,
                    GENERIC_FTP_SERVER);
            String username = ConfigurationUtil.getInstance().getSecureProperty(ARConstants.SECURE_CONFIG_CONTEXT,
                    GENERIC_FTP_USERNAME);
            String password = ConfigurationUtil.getInstance().getSecureProperty(ARConstants.SECURE_CONFIG_CONTEXT,
                    GENERIC_FTP_PASSWORD);
            String fileName = file.getName();

            ftpUtil.login(ftpServer, ftpLocation, username, password);

            //Try to retrieve remote file of the same name to local archive directory.
            //If file with this name exists on server, do not put file.
            String localArchiveLocation = ConfigurationUtil.getInstance()
                                                           .getProperty(ARConstants.CONFIG_FILE,
                    "gcProgramArchiveLocation");
            boolean fileExists = ftpUtil.remoteFileExists(ftpServer,
                    ftpLocation, localArchiveLocation, fileName, username,
                    password);

            if (fileExists) {
                logger.error(
                    "*****************File Exists. Transfer Cancelled.*******************");
            } else {
                // put the file to FTP server with the same name as the local file.
                ftpUtil.putFile(file, fileName);
            }

            ftpUtil.logout();
        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting putFtpFileToServer");
            }
        }
    }

    public File getFileFromGenericServer(String fileName)
        throws Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering getFileFromGenericServer");
        }

        File inboundFile = null;

        try {
            /* retrieve server name to FTP to */
            String ftpLocation = ConfigurationUtil.getInstance().getFrpGlobalParm(ARConstants.CONFIG_CONTEXT,
                    ftpPath);
            String ftpServer = ConfigurationUtil.getInstance().getFrpGlobalParm(ARConstants.CONFIG_CONTEXT,
                    GENERIC_FTP_SERVER);
            String username = ConfigurationUtil.getInstance().getSecureProperty(ARConstants.SECURE_CONFIG_CONTEXT,
                    GENERIC_FTP_USERNAME);
            String password = ConfigurationUtil.getInstance().getSecureProperty(ARConstants.SECURE_CONFIG_CONTEXT,
                    GENERIC_FTP_PASSWORD);
            String localFileDirectory = ConfigurationUtil.getInstance()
                                                         .getProperty(ARConstants.CONFIG_FILE,
                    LOCAL_FILE_LOCATION);

            FtpUtil ftpUtil = new FtpUtil();

            /* log into FTP Server */
            ftpUtil.login(ftpServer, ftpLocation, username, password);

            String localFile = localFileDirectory + fileName;

            try {
                ftpUtil.getFile(localFileDirectory, fileName);
            } catch (FTPException e) {
                throw e;
            } finally {
                ftpUtil.logout();
            }

            // retrieve the local file 
            inboundFile = new File(localFile);

            // archive the local file 
            // get the file from the FTP server and store it locally with the same name
            // and a time stamp appended to the name 
            Date d = new Date();
            SimpleDateFormat sdfOutput = new SimpleDateFormat("MMddyy");
            FileCompressionUtil fileUtil = new FileCompressionUtil();
            fileUtil.createGZipFile(localFile,
                localFile + sdfOutput.format(d) + ".gzip");
        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting getFileFromGenericServer");
            }
        }

        return inboundFile;
    }

    public static boolean putFileToServer(String ftpServer, String username,
        String password, String ftpLocation, String fileName,
        String localFileDirectory, String remoteFileName)
        throws Exception {
        boolean success = false;

        if (logger.isDebugEnabled()) {
            logger.debug("Entering putFtpFileToServer");
        }

        try {
            FtpUtil ftpUtil = new FtpUtil();

            /* retrieve server name to FTP to */
            //            String ftpLocation = ConfigurationUtil.getInstance().getProperty(ARConstants.CONFIG_FILE, ftpPath);
            //            String ftpServer = ConfigurationUtil.getInstance().getProperty(ARConstants.CONFIG_FILE, GENERIC_FTP_SERVER);
            //            String username = ConfigurationUtil.getInstance().getSecureProperty(ARConstants.SECURE_CONFIG_CONTEXT, GENERIC_FTP_USERNAME);
            //            String password = ConfigurationUtil.getInstance().getSecureProperty(ARConstants.SECURE_CONFIG_CONTEXT, GENERIC_FTP_PASSWORD);
            //            File file = new File(fileName);
            ftpUtil.login(ftpServer, ftpLocation, username, password);

            try {
                //Try to retrieve remote file of the same name to local archive directory.
                //If file with this name exists on server, do not put file.
                //            String localArchiveLocation = ConfigurationUtil.getInstance().getProperty(ARConstants.CONFIG_FILE, "gcProgramArchiveLocation");
                boolean fileExists = ftpUtil.remoteFileExists(ftpServer,
                        ftpLocation, localFileDirectory, remoteFileName,
                        username, password);

                if (fileExists) {
                    logger.warn(
                        "*****************File Exists. Transfer Cancelled.*******************");
                } else {
                    // put the file to FTP server with the same name as the local file.
                    ftpUtil.putFile(new File(localFileDirectory + fileName),
                        remoteFileName);
                    success = true;
                }
            } finally {
                ftpUtil.logout();
            }
        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting putFtpFileToServer");
            }
        }

        return success;
    }

    protected ProgramConfigVO getProgramConfig(String configParamName,
        String programName) throws Exception {
        // Find template name and inbound file name from program name 
        ProgramConfigVO programConfig = new ProgramConfigVO();
        programConfig.setParmName(configParamName);
        programConfig.setProgramName(programName);

        GCProgramDAO programDao = new GCProgramDAO(conn);
        programDao.doGetProgramConfig(programConfig);

        return programConfig;
    }

    protected static String getConfigurationParam(String keyName) {
        try {
            return configurationUtil.getProperty(ARConstants.CONFIG_FILE,
                keyName);
        } catch (Exception e) {
            logger.error("getConfigurationParam: Failed.", e);
            throw new RuntimeException(e);
        }
    }

    protected static String getDbConfigurationParam(String keyName) {
        try {
            return configurationUtil.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT,
                keyName);
        } catch (Exception e) {
            logger.error("getConfigurationParam: Failed.", e);
            throw new RuntimeException(e);
        }
    }

    protected static String getSecureConfigurationParam(String keyName) {
        try {
            return configurationUtil.getSecureProperty(ARConstants.SECURE_CONFIG_CONTEXT,
                keyName);
        } catch (Exception e) {
            logger.error("getConfigurationParam: Failed.", e);
            throw new RuntimeException(e);
        }
    }

    public static File getFileFromServerWithTimestamp(String ftpServer,
        String username, String password, String ftpLocation, String fileName,
        String localFileDirectory) throws Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering getFileFromServer");
        }

        File inboundFile = null;

        try {
            /* retrieve server name to FTP to */

            //            String ftpLocation = ConfigurationUtil.getInstance().getProperty(ARConstants.CONFIG_FILE, ftpPath);
            //            String ftpServer = ConfigurationUtil.getInstance().getProperty(ARConstants.CONFIG_FILE, GENERIC_FTP_SERVER);
            //            String username = ConfigurationUtil.getInstance().getSecureProperty(ARConstants.SECURE_CONFIG_CONTEXT, GENERIC_FTP_USERNAME);
            //            String password = ConfigurationUtil.getInstance().getSecureProperty(ARConstants.SECURE_CONFIG_CONTEXT, GENERIC_FTP_PASSWORD);
            //            String localFileDirectory = ConfigurationUtil.getInstance().getProperty(ARConstants.CONFIG_FILE, LOCAL_FILE_LOCATION);            
            FtpUtil ftpUtil = new FtpUtil();

            /* log into FTP Server */
            ftpUtil.login(ftpServer, ftpLocation, username, password);

            // rename the local file
            SimpleDateFormat sdfOutput = new SimpleDateFormat("yyyyMMdd-HHmmss");
            String inboundFileName = fileName + sdfOutput.format(new Date());

            try {
                ftpUtil.getFileWithRename(localFileDirectory, inboundFileName,
                    fileName);

                // Delete the remote file.
                ftpUtil.deleteFile(fileName);
            } catch (FTPException e) {
                throw e;
            } finally {
                ftpUtil.logout();
            }

            inboundFile = new File(localFileDirectory + inboundFileName);

            //            File inboundFileRaw = new File(localFile);
            //            inboundFile = new File(inboundFileRaw.getCanonicalPath() + sdfOutput.format(new Date()));
            //            
            //            if (!inboundFileRaw.renameTo(inboundFile)) 
            //            {
            //              throw new RuntimeException("getFileFromServer: Could not rename " +
            //              inboundFileRaw.getCanonicalPath() + " to " +
            //              inboundFile.getCanonicalPath());
            //            }
            // archive the local file 
            // get the file from the FTP server and store it locally with the same name
            // and a time stamp appended to the name             
            //            FileCompressionUtil fileUtil = new FileCompressionUtil();
            //            fileUtil.createGZipFile(localFile, localFileArchiveDirectory + fileName + sdfOutput.format(new Date) + ".gzip");
        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting getFileFromServer");
            }
        }

        return inboundFile;
    }

    public void insertProgramProcess(String gccNumber, String parmName)
        throws Exception {
        GCProgramDAO programDao = new GCProgramDAO(conn);
        programDao.doInsertProgramProcess(gccNumber, parmName);
    }

    public void sendSuccessEmail(String programName, String parmName)
        throws EmailDeliveryException {
        try {
            NotificationVO notifVO = new NotificationVO();

            /* set up e-mail */
            notifVO.setAddressSeparator(ConfigurationUtil.getInstance()
                                                         .getProperty(ARConstants.EMAIL_CONFIG_FILE,
                    "gc_feed_address_separator"));
            notifVO.setMessageTOAddress(ConfigurationUtil.getInstance()
                                                         .getFrpGlobalParm(ARConstants.CONFIG_CONTEXT,
                    "gc_feed_email_addresses"));
            notifVO.setMessageFromAddress(ConfigurationUtil.getInstance()
                                                           .getProperty(ARConstants.EMAIL_CONFIG_FILE,
                    "gc_feed_from_address"));
            notifVO.setSMTPHost(ConfigurationUtil.getInstance().getFrpGlobalParm(ARConstants.CONFIG_CONTEXT,
                    "gc_feed_mail_server"));
            notifVO.setMessageSubject(ConfigurationUtil.getInstance()
                                                       .getProperty(ARConstants.EMAIL_CONFIG_FILE,
                    "gc_feed_subject"));
            notifVO.setMessageContent(programName + " " + parmName + " " +
                ConfigurationUtil.getInstance().getProperty(ARConstants.EMAIL_CONFIG_FILE,
                    "gc_feed_content"));

            /* send e-mail */
            sendEmailNotification(notifVO);
        } catch (Exception e) {
            throw new EmailDeliveryException(e.getMessage());
        }
    }

    public void sendErrorEmail(String programName, String parmName,
        String errorMsg) throws EmailDeliveryException {
        try {
            NotificationVO notifVO = new NotificationVO();

            /* set up e-mail */
            notifVO.setAddressSeparator(ConfigurationUtil.getInstance()
                                                         .getProperty(ARConstants.EMAIL_CONFIG_FILE,
                    "gc_feed_address_separator"));
            notifVO.setMessageTOAddress(ConfigurationUtil.getInstance()
			                                 .getFrpGlobalParm(ARConstants.CONFIG_CONTEXT,
                    "gc_feed_email_addresses"));
            notifVO.setMessageFromAddress(ConfigurationUtil.getInstance()
                                                           .getProperty(ARConstants.EMAIL_CONFIG_FILE,
                    "gc_feed_from_address"));
            notifVO.setSMTPHost(ConfigurationUtil.getInstance()
			                                   .getFrpGlobalParm(ARConstants.CONFIG_CONTEXT,
		    "gc_feed_mail_server"));
            notifVO.setMessageSubject(ConfigurationUtil.getInstance()
                                                       .getProperty(ARConstants.EMAIL_CONFIG_FILE,
                    "gc_feed_subject"));
            notifVO.setMessageContent(programName + " " + parmName + "\n" +
                errorMsg);

            /* send e-mail */
            sendEmailNotification(notifVO);
        } catch (Exception e) {
            throw new EmailDeliveryException(e.getMessage());
        }
    }

    private void sendEmailNotification(NotificationVO nvo)
        throws Exception {
        EmailUtil emaiUtil = new EmailUtil();
        emaiUtil.sendEmail(nvo);
    }

    protected void persistPartnerData(String statementId, Map params)
        throws Exception {
        GCProgramDAO gcDao = new GCProgramDAO(conn);
        gcDao.doUpdatePartnerData(statementId, params);
    }

    public void openConnection() throws Exception {
        if (conn == null) {
            conn = DataSourceUtil.getInstance().getConnection(ConfigurationUtil.getInstance()
                                                                               .getProperty(ARConstants.CONFIG_FILE,
                        ARConstants.DATASOURCE_NAME));
        }
    }

    public void closeConnection() throws Exception {
        if ((conn != null) && !conn.isClosed()) {
            logger.debug("Closing connection...");
            conn.close();
        }
    }

    /**
     * Create record in gc_program_request_process table to mark event's taking place.
     * @param crs
     * @throws java.lang.Exception
     */
    protected void updateFeedProcess(String parmName) throws Exception {
        if (gccNumberList != null) {
            for (int i = 0; i < gccNumberList.size(); i++) {
                String gccNumber = (String) gccNumberList.get(i);
                try {
                    insertProgramProcess(gccNumber, parmName);
                } catch (SQLException e) {
                    // skips already activated GC
                    if(e.getMessage().indexOf("unique constraint") != -1) {
                        logger.warn("GC already activated: " + gccNumber);
                    }
                }
            }
        }
    }

    /**
     * retrieve program name from payload.
     * @param payload
     * @return
     * @throws java.lang.Exception
     */
    protected String getProgramName(Object payload) throws Exception {
        return XMLUtil.getPayloadData(payload, "/root/partner-program/text()");
    }

    /**
     * Move files to the archive directory
     * @throws java.lang.Exception
     */
    private void archiveFiles() throws Exception {
        String fromDir = ConfigurationUtil.getInstance().getProperty(ARConstants.CONFIG_FILE,
                "gcProgramLocalLocation");
        String toDir = ConfigurationUtil.getInstance().getProperty(ARConstants.CONFIG_FILE,
                "gcProgramArchiveLocation");
        FileArchiveUtil.moveToArchive(fromDir, toDir, true);
    }

    private boolean isInternetType(String couponNumber)
        throws Exception {
        GCProgramDAO gcdao = new GCProgramDAO(conn);
        String redemptionType = gcdao.getGiftCertRedemptionType(couponNumber);

        if ("I".equals(redemptionType)) {
            return true;
        }

        return false;
    }

    /**
    * dispatch to novator gift cert queue
    * @param gcvo
    * @throws java.lang.Exception
    */
    protected void sendToNovatorQ(String couponNumber, String status)
        throws Exception {
        MessageUtil msgUtil = new MessageUtil();
        GiftCertMessageVO gcMsg = composeGiftCertMessageVO(couponNumber, status);

        if (gcMsg != null) {
            msgUtil.dispatchMessage(gcMsg.toXML());
        }
    }

    protected void sendToNovatorQ(List giftCertMessageVOList)
        throws Exception {
        if ((giftCertMessageVOList != null) &&
                (giftCertMessageVOList.size() > 0)) {
            MessageUtil msgUtil = new MessageUtil();
            msgUtil.dispatchMessages(giftCertMessageVOList);
        }
    }

    protected GiftCertMessageVO composeGiftCertMessageVO(String couponNumber,
        String status) throws Exception {
        GiftCertMessageVO giftCertMsg = null;

        if (isInternetType(couponNumber)) {
            giftCertMsg = new GiftCertMessageVO();
            giftCertMsg.setCouponNumber(couponNumber);
            giftCertMsg.setRequestType(status);
            giftCertMsg.setRetry(0);
        }

        return giftCertMsg;
    }

    protected void sendToNovatorQ(String couponNumber)
        throws Exception {
        MessageUtil msgUtil = new MessageUtil();
        GiftCertMessageVO giftCertMsg = new GiftCertMessageVO();

        if (isInternetType(couponNumber)) {
            giftCertMsg.setCouponNumber(couponNumber);
            giftCertMsg.setRequestType(ARConstants.GCC_STATUS_ISSUED_ACTIVE);
            giftCertMsg.setRetry(0);
            msgUtil.dispatchMessage(giftCertMsg.toXML());
        }
    }

    protected static void writeToResponseFile(String localDirectory,
        String filename, boolean useLineTerminator, String line) {
        try {
            File file = new File(localDirectory + File.separator + filename);

            if (!file.exists()) {
                file.createNewFile();
            }

            PrintWriter out = new PrintWriter(new BufferedWriter(
                        new FileWriter(file, true)), true);

            try {
                if (useLineTerminator) {
                    // Note: A carriage return (and a carriage return only, no new line)
                    // must be used so that we are backward compatible with
                    // partners who expect the file to be generated from
                    // an HP system.
                    out.println(line);
                } else {
                    out.print(line);
                }
            } finally {
                out.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    protected String updateGiftCertStatus(String gcId,
        String partnerProgramName, String statusChange)
        throws Exception {
        GCProgramDAO gcDao = new GCProgramDAO(conn);

        return gcDao.doUpdateGiftCertStatusStrict(gcId, statusChange,
            partnerProgramName);
    }

    /**
     * sends a system message.
     * @param systemMessage - String
     * @param systemException - Exception
     * @return n/a
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     */
    protected void sendSystemMessage(String systemMessage,
        Exception systemException)
        throws SAXException, ParserConfigurationException, IOException, 
            SQLException, SystemMessengerException {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering sendSystemMessage");
        }

        try {
            /* send a system message */
            SystemMessager sysMessager = new SystemMessager();
            sysMessager.send(systemMessage, systemException,
                ARConstants.GC_INBOUND_STATUS_PROCESS,
                SystemMessager.LEVEL_DEBUG,
                ARConstants.GC_INBOUND_STATUS_PROCESS_ERROR_TYPE, conn);
        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting sendSystemMessage");
            }
        }
    }
    
    public Connection getConnection() {
    	return conn;
    }
}
