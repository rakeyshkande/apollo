package com.ftd.accountingreporting.giftcertfeed;

/**
 * This is an interface for gift cert program feed processing.
 * @author Christy Hu
 */
public interface IPartnerFeed
{
    public void processFeed(Object payload) throws Exception;   
}