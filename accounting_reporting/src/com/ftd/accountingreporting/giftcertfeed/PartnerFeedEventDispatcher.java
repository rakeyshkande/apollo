package com.ftd.accountingreporting.giftcertfeed;
import javax.ejb.EJBException;

import com.ftd.accountingreporting.util.SystemMessager;
import com.ftd.eventhandling.events.EventHandler;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;

/**
 * This class assigns the program feed event to a specific handler.
 * @author Christy Hu
 */
public class PartnerFeedEventDispatcher extends EventHandler
{
    protected Logger logger = 
        new Logger("com.ftd.accountingreporting.giftcertfeed.PartnerFeedEventDispatcher");

    protected boolean obtainedLock;

    /**
     *  This is the invocation method for the event handler.  
     *  The payload is the JMS payload. It's the program name.  
     *  
     * @param payload - Object
     * @return n/a
     * @throws Throwable
     * @todo - code and determine exceptions
     */
    public void invoke(Object payload) throws Throwable 
    {
        if(logger.isDebugEnabled()) {            
            logger.debug("***********************************************************");
            logger.debug("* Invoking PartnerFeedEventDispatcher");
            logger.debug("***********************************************************");
        }
        
        PartnerFeedHandler feedHandler = null;
        
        try{       
            feedHandler = getPartnerFeedHandler(payload);
            if(feedHandler != null) {
                feedHandler.openConnection();
                feedHandler.processFeed(payload);
            } else {
                logger.error("Handler not found for payload: " + (String)((MessageToken)payload).getMessage());
            }
            
        } catch (Throwable t) {
            // Do not rethrow. Force dequeue.
            logger.error(t);
            try {
                SystemMessager.sendSystemMessage(feedHandler.getConnection(), "PartnerFeedEventDispatcher", t);
            } catch (Exception e) {
                String errMsg = "Unable to send message to support pager.";
                logger.fatal(errMsg,e);
            }
        } finally {
            if(feedHandler != null) {
                feedHandler.closeConnection();
            }
            if(logger.isDebugEnabled()){
                logger.debug("***********************************************************");
                logger.debug("* Exit PartnerFeedEventDispatcher");
                logger.debug("***********************************************************");
            } 
        }
    }
    

    
    private PartnerFeedHandler getPartnerFeedHandler(Object payload) throws Exception {
        PartnerFeedHandlerFactory handlerFactory = PartnerFeedHandlerFactory.getInstance();
        return handlerFactory.getHandler(payload);
    }     
}