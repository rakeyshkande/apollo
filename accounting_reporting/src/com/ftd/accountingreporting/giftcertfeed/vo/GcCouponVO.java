package com.ftd.accountingreporting.giftcertfeed.vo;
import java.util.List;

/**
 * This is the value object that models a gc coupon. 
 */
public class GcCouponVO
{
    public GcCouponVO()
    {
    }
    
    private String gcCouponNumber = null;
    private String gcCouponStatus = null;


  public void setGcCouponNumber(String gcCouponNumber)
  {
    this.gcCouponNumber = gcCouponNumber;
  }


  public String getGcCouponNumber()
  {
    return gcCouponNumber;
  }


  public void setGcCouponStatus(String gcCouponStatus)
  {
    this.gcCouponStatus = gcCouponStatus;
  }


  public String getGcCouponStatus()
  {
    return gcCouponStatus;
  }


 

    
}