package com.ftd.accountingreporting.giftcertfeed.vo;
import java.util.List;

/**
 * This is the value object that models a program config object for the feed. 
 */
public class ProgramConfigVO
{
    public ProgramConfigVO()
    {
    }
    
    private String programName = null;
    private String parmName = null;
    private String fileName = null;
    private String templateName = null;


    public void setProgramName(String programName)
    {
      this.programName = programName;
    }
  
  
    public String getProgramName()
    {
      return programName;
    }
  
  
    public void setParmName(String parmName)
    {
      this.parmName = parmName;
    }
  
  
    public String getParmName()
    {
      return parmName;
    }
  
  
    public void setFileName(String fileName)
    {
      this.fileName = fileName;
    }
  
  
    public String getFileName()
    {
      return fileName;
    }
  
  
    public void setTemplateName(String templateName)
    {
      this.templateName = templateName;
    }
  
  
    public String getTemplateName()
    {
      return templateName;
    }


    
}