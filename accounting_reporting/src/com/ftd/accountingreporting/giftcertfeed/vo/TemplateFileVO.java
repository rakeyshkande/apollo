package com.ftd.accountingreporting.giftcertfeed.vo;
import java.util.List;

/**
 * This is the value object that models a File object for the feed. 
 */
public class TemplateFileVO
{
    public TemplateFileVO()
    {
    }
    
    private String delim = null;
    private String statementId = null;
    private List fields = null;
    private String parmName = null;

    public void setDelim(String delim)
    {
      this.delim = delim;
    }
  
  
    public String getDelim()
    {
      return delim;
    }
  
  
    public void setStatementId(String statementId)
    {
      this.statementId = statementId;
    }
  
  
    public String getStatementId()
    {
      return statementId;
    }
  
  
    public void setFields(List fields)
    {
      this.fields = fields;
    }
  
  
    public List getFields()
    {
      return fields;
    }


  public void setParmName(String parmName)
  {
    this.parmName = parmName;
  }


  public String getParmName()
  {
    return parmName;
  }
  
  public FileFieldVO getFieldByPosition(int pos) throws Exception {
    FileFieldVO field = null;
    
    if(fields != null) {
        for (int i = 0; i < fields.size(); i++) {
            FileFieldVO curField = (FileFieldVO)fields.get(i);
            if(pos == Integer.parseInt(curField.getFieldPosition())) {
                return curField;
            }
        }
    }
    return field;
  }
    
}