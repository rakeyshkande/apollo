package com.ftd.accountingreporting.giftcertfeed.vo;
import java.util.List;

/**
 * This is the value object that models a file field object for the feed. 
 */
public class FileFieldVO
{
    public FileFieldVO()
    {
    }
    
    private String fieldName = null;
    private String fieldValue = null;
    private String fieldPosition = null;
    private String isKey = null;

    public void setFieldName(String fieldName)
    {
      this.fieldName = fieldName;
    }
  
  
    public String getFieldName()
    {
      return fieldName;
    }
  
  
    public void setFieldValue(String fieldValue)
    {
      this.fieldValue = fieldValue;
    }
  
  
    public String getFieldValue()
    {
      return fieldValue;
    }
  
  
    public void setFieldPosition(String fieldPosition)
    {
      this.fieldPosition = fieldPosition;
    }
  
  
    public String getFieldPosition()
    {
      return fieldPosition;
    }

    public void setIsKey(String isKey)
    {
      this.isKey = isKey;
    }
    
    public String getIsKey()
    {
      return isKey;
    }

 

    
}