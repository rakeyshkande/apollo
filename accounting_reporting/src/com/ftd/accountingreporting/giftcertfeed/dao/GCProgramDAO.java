package com.ftd.accountingreporting.giftcertfeed.dao;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.giftcertfeed.vo.GcCouponVO;
import com.ftd.accountingreporting.giftcertfeed.vo.ProgramConfigVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;
import java.sql.Connection;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * This is the data access object for gift cert program feed.   
 * @author Christy Hu
 */
public class GCProgramDAO 
{
    private Logger logger = 
        new Logger("com.ftd.accountingreporting.giftcertfeed.dao.GCProgramDAO");
    private Connection dbConnection = null;
    
    public GCProgramDAO(Connection conn)
    {
        dbConnection = conn;
    }

    /**
     *  This method retrieved outbound gc data. 
     * @param n/a
     * @return CachedResultSet
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     */
    public CachedResultSet doGetOutboundData(String statementId, String programName) 
           throws  Exception
    {
       if(logger.isDebugEnabled()){
            logger.debug("Entering doGetOutboundData");
            logger.debug("StatementId: " + statementId);
            logger.debug("programName: " + programName);
        }
        
        DataRequest request = new DataRequest();
        CachedResultSet output = null;

        try {
            HashMap inputParams = new HashMap();
            inputParams.put("IN_PROGRAM_NAME", programName);
            request.setInputParams(inputParams);
            
            /* build DataRequest object */
            request.setConnection(dbConnection);
            
            request.setStatementID(statementId);
            
            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            output = (CachedResultSet) dau.execute(request);
            request.reset();

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting doGetOutboundData");
            } 
        }
            return output;
    }
    
    public void doUpdatePartnerData(String statementId, Map params) throws Exception {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doUpdatePartnerData");
            logger.debug("statementId:" + statementId);;
        }
         DataRequest request = new DataRequest();

        try {
            request.setConnection(dbConnection);
            request.setInputParams(params);
            request.setStatementID(statementId);
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            String status = (String) outputs.get("OUT_STATUS");
            if(status.equals(ARConstants.COMMON_VALUE_NO))
            {
                String errorMessage = (String) outputs.get("OUT_MESSAGE");
                throw new Exception(errorMessage);
            }      
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting doUpdateInboundGC1");
            } 
        }     
    }
/*
    public void doUpdateInboundGC1(String statementId, InboundGC_1_VO vo) 
           throws  Exception
    {
       if(logger.isDebugEnabled()){
            logger.debug("Entering doUpdateInboundGC1");
        }
        
        DataRequest request = new DataRequest();

        try {
            HashMap inputParams = new HashMap();
            inputParams.put("IN_GC_COUPON_NUMBER", vo.getGcCouponNumber());
            inputParams.put("IN_LAST_NAME", vo.getLastName());
            inputParams.put("IN_FIRST_NAME", vo.getFirstName());
            
            // build DataRequest object 
            request.setConnection(dbConnection);
            request.reset();
            request.setStatementID(statementId);
            
            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            String status = (String) outputs.get("OUT_STATUS");
            if(status.equals(ARConstants.COMMON_VALUE_NO))
            {
                String errorMessage = (String) outputs.get("OUT_MESSAGE");
                throw new Exception(errorMessage);
            }

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting doUpdateInboundGC1");
            } 
        }     
    }
*/
    public ProgramConfigVO doGetProgramConfig(ProgramConfigVO vo) 
           throws Exception
    {
       if(logger.isDebugEnabled()){
            logger.debug("Entering doGetProgramConfig");
            logger.debug("programName: " + vo.getProgramName());
            logger.debug("parmName: " + vo.getParmName());
        }
        
        DataRequest request = new DataRequest();
        CachedResultSet output = null;

        try {
            HashMap inputParams = new HashMap();
            inputParams.put("IN_PROGRAM_NAME", vo.getProgramName());
            inputParams.put("IN_PARM_NAME", vo.getParmName());
            request.setInputParams(inputParams);
            
            /* build DataRequest object */
            request.setConnection(dbConnection);
            
            request.setStatementID("GET_GC_PROGRAM_CONFIG");
            
            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            output = (CachedResultSet) dau.execute(request);
            
            request.reset();
            if(output != null && output.next()) {
                vo.setFileName(output.getString("parm_value"));
                vo.setTemplateName(output.getString("template_name"));
            }

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting doGetProgramConfig");
            } 
        }
        return vo;
    }

    public void doInsertProgramProcess(String gccNumber, String parmName) 
           throws Exception
    {
       if(logger.isDebugEnabled()){
            logger.debug("Entering doInsertProgramProcess");
            logger.debug("gccNumber: " + gccNumber);
            logger.debug("parmName: " + parmName);
        }
        
        DataRequest request = new DataRequest();

        try {
            HashMap inputParams = new HashMap();
            inputParams.put("IN_GC_COUPON_NUMBER", gccNumber);
            inputParams.put("IN_PARM_NAME", parmName);
            
            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setStatementID("INSERT_PROGRAM_PROCESS");
            request.setInputParams(inputParams);
            
            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            
            String status = (String) outputs.get("OUT_STATUS");
            if(status.equals(ARConstants.COMMON_VALUE_NO))
            {
                String errorMessage = (String) outputs.get("OUT_MESSAGE");
                throw new SQLException(errorMessage);
            }

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting doUpdateProgramProcess");
            } 
        }     
    }
    
    public void doUpdateGiftCertStatus(String gcCouponNumber, String gcStatus) 
           throws Exception
    {
       if(logger.isDebugEnabled()){
            logger.debug("Entering doUpdateGiftCertStatus");
            logger.debug("gcCouponNUmber: " + gcCouponNumber);
            logger.debug("gcStatus: " + gcStatus);
        }
        
        DataRequest request = new DataRequest();

        try {
            HashMap inputParams = new HashMap();
            inputParams.put("IN_GC_COUPON_NUMBER", gcCouponNumber);
            inputParams.put("IN_STATUS", gcStatus);
            
            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.setInputParams(inputParams);
            request.setStatementID("UPDATE_GC_COUPON_STATUS");
            
            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            request.reset();
            String status = (String) outputs.get("OUT_STATUS");
            if(status.equals(ARConstants.COMMON_VALUE_NO))
            {
                String errorMessage = (String) outputs.get("OUT_ERROR_MESSAGE");
                throw new Exception(errorMessage);
            }

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting doUpdateGiftCertStatus");
            } 
        }     
    }
    
    public String doUpdateGiftCertStatusStrict(String partnerSequenceNbr, String gcStatus, String partnerProgramName) 
           throws Exception
    {
       if(logger.isDebugEnabled()){
            logger.debug("Entering doUpdateGiftCertStatusStrict");
            logger.debug("gcCouponNUmber: " + partnerSequenceNbr);
            logger.debug("gcStatus: " + gcStatus);
        }
        
        DataRequest request = new DataRequest();

        try {
            HashMap inputParams = new HashMap();
            inputParams.put("IN_GC_PARTNER_SEQUENCE", partnerSequenceNbr);
            inputParams.put("IN_STATUS", gcStatus);
            inputParams.put("IN_PROGRAM_NAME", partnerProgramName);
            
            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.setInputParams(inputParams);
            request.setStatementID("UPDATE_GC_COUPON_STATUS_STRICT");
            
            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            request.reset();
            String status = (String) outputs.get("OUT_STATUS");
            if(status.equals(ARConstants.COMMON_VALUE_NO))
            {
                String errorMessage = (String) outputs.get("OUT_ERROR_MESSAGE");
                throw new Exception(errorMessage);
            }
            
            return (String) outputs.get("OUT_GC_COUPON_NUMBER");

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting doUpdateGiftCertStatusStrict");
            } 
        }     
    }
    
    /**
     *  Call CLEAN.GIFT_CERTIFICATE_COUPON_PKG.GET_GCCREQ_BY_REQNUM_OR_GCCNUM(null, couponNumber)
     * @param couponNumber - String
     * @return GiftCertVO
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     */
    public String getGiftCertRedemptionType(String gccNumber)
        throws IOException, SQLException, Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering getGiftCertRedemptionType");
            logger.debug("Coupon Number : " + gccNumber);
        }
        
        DataRequest request = new DataRequest();
        String redemptionType = "";
        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_REQUEST_NUMBER", null);
            inputParams.put("IN_GC_COUPON_NUMBER", gccNumber);

            // build DataRequest object
            request.setConnection(dbConnection);
            request.setInputParams(inputParams);
            request.setStatementID("GET_GCCREQ_BY_REQNUM_OR_GCCNUM");

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            CachedResultSet outputs = (CachedResultSet) dau.execute(request);
            
            while(outputs.next())
            { 
                redemptionType = outputs.getString("order_source");
            }

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getGiftCertRedemptionType");
            } 
        } 
        return redemptionType;
    }    
  /**
   * This method gets the coupons data by request number or coupon number
   * @param String requestNumber
   * @param String couponNumber
   * @return Document
   * @throws java.lang.Exception
   */
  public GcCouponVO doGetGcCoupon (String couponNumber) throws Exception
  {
        CachedResultSet output = null;
        GcCouponVO vo = null;
        
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(dbConnection);
        
        dataRequest.setStatementID("GET_GC_BY_GC_NUMBER");
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        dataRequest.addInputParam("IN_REQUEST_NUMBER", null);
        dataRequest.addInputParam("IN_GC_COUPON_NUMBER", couponNumber);
        
        DataAccessUtil dau = DataAccessUtil.getInstance();
        output = (CachedResultSet) dau.execute(dataRequest);
            
        dataRequest.reset();
        if(output != null && output.next()) {
            vo = new GcCouponVO();
            vo.setGcCouponNumber(couponNumber);
            vo.setGcCouponStatus(output.getString("gc_coupon_status"));
        }
        return vo;
  }    
}