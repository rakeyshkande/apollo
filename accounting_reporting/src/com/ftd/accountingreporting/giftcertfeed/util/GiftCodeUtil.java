package com.ftd.accountingreporting.giftcertfeed.util;

import java.io.IOException;

import org.codehaus.jackson.map.ObjectMapper;

import com.ftd.accountingreporting.giftCertificate.vo.CreateBatchResponse;
import com.ftd.accountingreporting.giftCertificate.vo.UpdateBatchResponse;
import com.ftd.accountingreporting.giftCertificate.vo.UpdateGiftCertResponse;

public class GiftCodeUtil {
	/**
	 * Converts Response String into Response Object 
	 * @param s
	 * @return GiftCodeUpdateBatchResponseVO
	 */
	public static CreateBatchResponse convertToObject(String s) {
		CreateBatchResponse obj = new CreateBatchResponse();
		ObjectMapper mapperObj = new ObjectMapper();
		try {
			obj = mapperObj.readValue(s, CreateBatchResponse.class);
		} catch (IOException ex) {
		}
		return obj;
	}
	
	public static UpdateGiftCertResponse convertToUpdateSingleObject(String jsonString) {
		UpdateGiftCertResponse retObj = new UpdateGiftCertResponse();
		ObjectMapper mapperObj = new ObjectMapper();
		try {
			retObj = mapperObj.readValue(jsonString, UpdateGiftCertResponse.class);
		} catch (IOException ex) {
		}
		return retObj;
	}
	
	public static UpdateBatchResponse convertToUpdateBatchObject(String jsonString) {
		UpdateBatchResponse retObj = new UpdateBatchResponse();
		ObjectMapper mapperObj = new ObjectMapper();
		try {
			retObj = mapperObj.readValue(jsonString, UpdateBatchResponse.class);
		} catch (IOException ex) {
		}
		return retObj;
	}


}
