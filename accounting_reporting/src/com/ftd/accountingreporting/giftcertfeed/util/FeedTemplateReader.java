package com.ftd.accountingreporting.giftcertfeed.util;

import com.ftd.accountingreporting.giftcertfeed.vo.FileFieldVO;
import com.ftd.accountingreporting.giftcertfeed.vo.TemplateFileVO;
import com.ftd.accountingreporting.util.ConfigFileReader;

import com.ftd.osp.utilities.j2ee.ResourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.File;
import java.io.IOException;

import java.net.URL;
import java.net.URLDecoder;

import java.util.ArrayList;
import java.util.List;
import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.traversal.NodeIterator;

/**
 * This Class is a utility class retrieves data from the config file document.
 *
 */
public class FeedTemplateReader 
{
    private static Logger logger  = new Logger("com.ftd.accountingreporting.giftcertfeed.util.FeedTemplateReader");
    private ConfigFileReader reader = null;
    private Document rootDoc = null;
    
    private static final String PATH_DELIM = "/file/file-delim/text()";
    private static final String PATH_STATEMENT_ID = "/file/statement-id/text()";
    private static final String PATH_PARM_NAME = "/file/parm-name/text()";
    //private static final String PATH_FIELDS = "/file/fields";
    private static final String PATH_FIELD = "/file/fields/field";
    private static final String FIELD_NAME = "field-name/text()";
    private static final String FIELD_VALUE = "field-value/text()";
    private static final String FIELD_POSITION = "field-position/text()";
    //private static final String FIELD_TYPE = "field-type/text()";
    //private static final String FIELD_PATTERN = "field-pattern/text()";
    private static final String IS_KEY = "is-key/text()";
    
    /**
     * constructor
     */
    public FeedTemplateReader(String _fileName)
    {
        reader = ConfigFileReader.getInstance(_fileName);
        rootDoc = reader.getRootDoc();
    } 
    
   /**
   * Retrieves the list of FileFieldVO from the config file.
   * @return 
   * @throws java.lang.Exception
   */
    public TemplateFileVO getFile() throws Exception {
        TemplateFileVO configFile = new TemplateFileVO();
        
        String statementId = reader.getSingleNodeValue(PATH_STATEMENT_ID);
        String delim = reader.getSingleNodeValue(PATH_DELIM);
        String parmName = reader.getSingleNodeValue(PATH_PARM_NAME);
        NodeIterator iter = XPathAPI.selectNodeIterator(rootDoc, PATH_FIELD);
        
        List fieldList = new ArrayList();
        for(Node fieldNode = iter.nextNode(); fieldNode != null;) {
            Node nameNode = XPathAPI.selectSingleNode(fieldNode, FIELD_NAME);
            Node valueNode = XPathAPI.selectSingleNode(fieldNode, FIELD_VALUE);
            Node positionNode = XPathAPI.selectSingleNode(fieldNode, FIELD_POSITION);
            Node isKeyNode = XPathAPI.selectSingleNode(fieldNode, IS_KEY);
            //Node typeNode = XPathAPI.selectSingleNode(fieldNode, FIELD_TYPE);
            //Node patternNode = XPathAPI.selectSingleNode(fieldNode, FIELD_PATTERN);
            
            String fieldName = nameNode == null? "" : nameNode.getNodeValue();
            String fieldValue = valueNode == null? "" : valueNode.getNodeValue();
            String fieldPosition = positionNode == null? "" : positionNode.getNodeValue();
            String isKey = isKeyNode == null? "" : isKeyNode.getNodeValue();
            //String fieldType = typeNode == null? "" : typeNode.getNodeValue();
            //String fieldPattern = patternNode == null? "" : patternNode.getNodeValue();
            
            FileFieldVO fileField = new FileFieldVO();
            fileField.setFieldName(fieldName);
            fileField.setFieldValue(fieldValue);
            fileField.setFieldPosition(fieldPosition);
            fileField.setIsKey(isKey);
            //fileField.setFieldType(fieldType);
            //fileField.setFieldPattern(fieldPattern);
            
            fieldList.add(fileField);
            fieldNode = iter.nextNode();
        }
        
        configFile.setDelim(delim);
        configFile.setStatementId(statementId);
        configFile.setParmName(parmName);
        configFile.setFields(fieldList);
        
        return configFile;
    }

}