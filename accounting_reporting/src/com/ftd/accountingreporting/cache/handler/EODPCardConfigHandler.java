package com.ftd.accountingreporting.cache.handler;

import com.ftd.accountingreporting.exception.NoCacheRegionException;
import com.ftd.accountingreporting.vo.PartnerPCardVO;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * This class retrieves EOD p card configuration information from database
 * configuration tables.
 *
 * @author Charles Fox
 */
public class EODPCardConfigHandler extends CacheHandlerBase
{
    public EODPCardConfigHandler()
    {
        super();
    }


    private Logger logger = new Logger(
        "com.ftd.accountingreporting.cache.handler.EODPCardConfigHandler");

    private Map pCardConfigMap = null;

    /**
     * load pcard data
     * 
     * @param Connection      - database connection
     * @return Object         - map of pcard information
     * @throws CacheException - cachecontroller error
     */
    public Object load(Connection conn) throws CacheException
    {   
    
        if(logger.isDebugEnabled()){
            logger.debug("Entering load");
            logger.debug("Connection : " + conn);
        }
        Map pcardCacheMap = new HashMap();
        List configList = null;
        String prevProgramName = null;
        String programName = null;
        
        try
        {
            // build DataRequest object
            DataRequest request = new DataRequest();
            request.reset();
            request.setConnection(conn);
            request.setStatementID("GET_PARTNER_CONFIG_FIELDS");

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();

            CachedResultSet pcardConfigRS = (CachedResultSet) dau.execute(request);
            /* load pcard information into map as a list of PartnerPCardVO objects for each partner program*/
            while(pcardConfigRS.next())      
            {
                programName = pcardConfigRS.getString("program_name");
                PartnerPCardVO pcardVO = new PartnerPCardVO();
                
                pcardVO.setProgramName(programName);
                pcardVO.setConfigId(pcardConfigRS.getString("pcard_config_id"));
                pcardVO.setFieldName(pcardConfigRS.getString("field_name"));
                pcardVO.setFieldPrefix(pcardConfigRS.getString("field_prefix"));
                pcardVO.setSortOrder(pcardConfigRS.getInt("sort_order"));
                pcardVO.setSourceField(pcardConfigRS.getString("source_field"));
                pcardVO.setSourceTable(pcardConfigRS.getString("source_table"));
                pcardVO.setFieldSuffix(pcardConfigRS.getString("field_suffix"));
                pcardVO.setCurrencyDecimalInd(pcardConfigRS.getString("currency_decimal_ind"));
                
                if(prevProgramName == null) {
                    configList = new ArrayList();
                } else {
                    if(!programName.equals(prevProgramName)) {
                        pcardCacheMap.put(prevProgramName, configList);
                        configList = new ArrayList();
                    }
                }
                configList.add(pcardVO);
                prevProgramName = programName;   
            }        
            // add list partner program to map.
            pcardCacheMap.put(programName, configList);
        }
       catch(Exception e){
            logger.error(e);
            throw new CacheException(e.toString());
       }
       finally
        {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting load");
            }
        }
        return pcardCacheMap;
    }
    
    
    /**
     * set cached object
     * 
     * @param Object  - cached object
     * @return void
     * @throws CacheException
     */
    public void setCachedObject(Object cachedObject) throws CacheException
    {
        pCardConfigMap = (Map) cachedObject;
        return;
    }

    /**
     * lookup a pcard object based on the program name
     * 
     * @param programName  - name of program for a pcard
     * @return void
     * @throws CacheException
     */
    public List getPartnerPCard(String programName) throws NoCacheRegionException, Exception {
        List configList  = null;
        /* lookup the pcard config fields with the same program name */

        if(pCardConfigMap == null) {
            throw new NoCacheRegionException("EODPCardConfigHandler Cache region does not exist.");
        }
        configList = (List) (pCardConfigMap.get(programName)); 
        
        return configList;
    }
    
    public Map getPcardConfigMap() {
        return pCardConfigMap;
    }
 
}
