package com.ftd.accountingreporting.bo;

import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.dao.CBRDAO;
import com.ftd.accountingreporting.dao.CommonDAO;
import com.ftd.accountingreporting.exception.EntityLockedException;
import com.ftd.accountingreporting.util.AccountingUtil;
import com.ftd.accountingreporting.util.LockUtil;
import com.ftd.accountingreporting.vo.CbrCommentVO;
import com.ftd.accountingreporting.vo.CbrOrderBillVO;
import com.ftd.accountingreporting.vo.CbrPaymentVO;
import com.ftd.accountingreporting.vo.CbrRefundVO;
import com.ftd.accountingreporting.vo.CbrVO;
import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.order.vo.PaymentMethodMilesPointsVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.PrintWriter;
import java.io.StringWriter;

import java.lang.Double;
import java.math.BigDecimal;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.w3c.dom.Document;

import org.apache.xpath.XPathAPI;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.w3c.dom.traversal.NodeIterator;


/**
 * This is the business object that processes requests on the
 * charge back retrieval screens.
 * @author Charles Fox
 */
public class CBRBO 
{

    Connection dbConn = null;
    private Logger logger = 
        new Logger("com.ftd.accountingreporting.bo.CBRBO");
        
    public CBRBO(Connection conn)
    {
        this.dbConn = conn;
    }

    /**
     * This method builds the XML to be sent back when the Add Charge Back page
     * is loaded. Call displayAddCBR, passing the request, �Charge Back�, �CB�.
     * @param request - HttpServletRequest
     * @return HashMap
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     */    
    public HashMap handleDisplayAddCB(HttpServletRequest request)
        throws Exception
    {
       if(logger.isDebugEnabled()){
            logger.debug("Entering handleDisplayAddCB");
        }
        HashMap displayInfo = new HashMap();
        String orderGUID = "";
        String sessionID = "";
        String csrID = "";
        LockUtil lockUtil = null;
        boolean lockObtained = false;
        
        try{
            lockUtil = new LockUtil(dbConn);
            /* retrieve data filter parameters */
            HashMap parameters = (HashMap)request.getAttribute("parameters");
            /* Obtain lock for shopping cart to of REFFUND type. */
            /* retrieve lock settings */
            orderGUID = (String) parameters.get("order_guid");
            if(orderGUID == null || orderGUID.equalsIgnoreCase(""))
            {
                /* attempt to retrieve from request */
                orderGUID = request.getParameter("order_guid");
            }
            sessionID = (String) parameters.get("securitytoken");
            if(sessionID == null || sessionID.equalsIgnoreCase(""))
            {
                /* attempt to retrieve from request */
                sessionID = request.getParameter("securitytoken");
            }
            AccountingUtil acctUtil = new AccountingUtil();
            csrID = acctUtil.getCsrID(sessionID);
            
            try{
                // attempt to gain lock 
                lockObtained = lockUtil.obtainLock(ARConstants.CB_REFUND_LOCK_ENTITY_TYPE,orderGUID,csrID,sessionID,ARConstants.CB_REFUND_LOCK_LEVEL);
            }catch(EntityLockedException e)
            {     
                // call ReconBillingBO.retrieveBillingDoc(request, false) 
                // to get the document 
                ReconBillingBO reconBO = new ReconBillingBO(dbConn);
                Document recon = reconBO.retrieveBillingDoc(request,false);

                // Add displayMessage attribute to root node 
                Attr numAttr = recon.createAttribute("displayMessage");
                numAttr.setValue(e.getMessage());
                Node rootNode = XPathAPI.selectSingleNode(recon,"/root");
                ((Element)(rootNode)).setAttributeNode(numAttr);
                // create map and add forward name and the document to display 
                HashMap errorDisplay = new HashMap();
                errorDisplay.put("doc",recon);
                errorDisplay.put("forward","fail_display_add");
                return errorDisplay;
            }
            
            displayInfo = displayAddCBR(request,"Charge Back","CB");
            
        } finally {
           if(lockObtained) {
               lockUtil.releaseLock(ARConstants.CB_REFUND_LOCK_ENTITY_TYPE,orderGUID,csrID,sessionID);
           }
            if(logger.isDebugEnabled()){
               logger.debug("Exiting handleDisplayAddCB");
            } 
        }
        
        return displayInfo;
    }

    /**
     * This method builds the XML to be sent back when the Add Retrieval page
     * is loaded. Call displayAddCBR, passing request, �Retrieval�, �RT�.
     * @param request - HttpServletRequest
     * @return HashMap
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     */    
    public HashMap handleDisplayAddRT(HttpServletRequest request)
        throws Exception
    {
       if(logger.isDebugEnabled()){
            logger.debug("Entering handleDisplayAddRT");
        }
        HashMap displayInfo = new HashMap();
        try{
            displayInfo = displayAddCBR(request,"Retrieval","RT");
        
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting handleDisplayAddRT");
            } 
        }
        
        return displayInfo;
        
    }

    /**
     * This method builds the XML to be sent back when the Add Charge Back or 
     * Add Retrieval page is loaded.
     * �	Create Document as in Appendix B (cbr_db.xml)
     *      o	Create HEADER node with value headerName
     *      o	Create SUBHEADER node with value �Add � headerName
     *      o	Create CHARGEBACK_RETRIEVAL section by creating a new CBRVO 
     *          object; set type_code to typeCode; call its toXML method. Add 
     *          attribute �disabled� with value �false� to �AMOUNT� node.
     *      o	Create PAYMENTS_CB section by calling CBRDAO.doGetPaymentsForCB.
     *          Retrieve master_order_number from filter parameters HashMap. 
     *          Add num attribute to primary node. Add $ before the value on 
     *          AMOUNT node. 
     *      o	Create PAYMENTS_NOT_CB section by calling 
     *          CBRDAO.doGetPaymentsNotForCB. Add num attribute to primary node. 
     *          Add $ before the value on AMOUNT node. If type is �Refund�, 
     *          add () around the value.
     *      o	Create CBR_COMMENTS section by creating a new CBRCommentVO 
     *          object and call its toXML method.
     *      o	Create REASONS section by calling CBRDAO.doGetCBRReasons.
     * �	Add the Document in HashMap with key �doc�.
     * �	Add �load� in HashMap with key �forwardName�
     * �	Return the HashMap.
     * @param request - HttpServletRequest
     * @param headerName - String
     * @param typeCode - String
     * @return HashMap
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     */  
    private HashMap displayAddCBR(HttpServletRequest request, String headerName, 
        String typeCode)
        throws Exception
    {

       if(logger.isDebugEnabled()){
            logger.debug("Entering displayAddCBR");
        }
        HashMap displayCBR = new HashMap();
        try{
            /* Create Document */
            Document displayXML = null;
            /* Create HEADER node with value headerName */
            String xmlContent = "<root displayMessage=''><header>" + headerName + "</header>";
            /* Create SUBHEADER node with value �Add � headerName */
            xmlContent = xmlContent + "<subheader>Add " + headerName + "</subheader>";

            /* Retrieve master_order_number from the filter (get attribute 
              "parameters" HashMap from request) */
            HashMap parameters = (HashMap)request.getAttribute("parameters");
            String masterOrderNum = (String) parameters.get("master_order_number");
            if(masterOrderNum == null)
            {
                /* attempt to retrieve from request */
                masterOrderNum = request.getParameter("master_order_number");
            }
            xmlContent = xmlContent + "<master_order_number>" + masterOrderNum + "</master_order_number>";
          
            /* Create CHARGEBACK_RETRIEVAL section */
            CbrVO cbrVO = new CbrVO();
            /* set type_code to typeCode  */
            cbrVO.setTypeCode(typeCode);

            String cbrXML = cbrVO.toXML();
            Document cbr = DOMUtil.getDocument(cbrXML);

            /* Add attribute �disabled� with value �false� to �AMOUNT� node */
            Node amountNode = XPathAPI.selectSingleNode(cbr,"chargeback_retrieval/amount");
            Attr disabledAttr = cbr.createAttribute("disabled");
            disabledAttr.setValue("false");

            Element amountEle = (Element) amountNode;
            amountEle.setAttributeNode(disabledAttr);

            /* convert CHARGEBACK_RETRIEVAL document to a string and append to mail 
             * doc */
            StringWriter sw = new StringWriter();       
            DOMUtil.print(((Document) cbr), new PrintWriter(sw));
            xmlContent = xmlContent + sw.toString();

            /* Create PAYMENTS_CB section  */
            CBRDAO cbrDAO = new CBRDAO(dbConn);
            Document paymentsForCB = cbrDAO.doGetPaymentsForCB(masterOrderNum, typeCode);

            /* Add num attribute to primary node */
            Attr numAttr = paymentsForCB.createAttribute("num");

            numAttr.setValue(String.valueOf(paymentsForCB.getFirstChild().getChildNodes().getLength()));
            Node paymentPrimaryNode = XPathAPI.selectSingleNode(paymentsForCB,"/payments_cb");
            ((Element)(paymentPrimaryNode)).setAttributeNode(numAttr);
            
            /* Add $ before the value on AMOUNT node. */
            NodeIterator niPayCB = XPathAPI.selectNodeIterator(paymentsForCB,"/payments_cb/payment");
            for (Node paymentNode=niPayCB.nextNode(); paymentNode!=null;) {
                /* add $ */
                Node payAmountNode = XPathAPI.selectSingleNode(paymentNode,"amount/text()");
                String amount = "$0.00";
                
                if(payAmountNode == null) {
                    NodeList amounts = ((Element)paymentNode).getElementsByTagName("amount");
                    payAmountNode = amounts.item(0);
                    Text amountText = paymentsForCB.createTextNode(amount);
                    payAmountNode.appendChild(amountText);
                } else { 
                    amount = payAmountNode.getNodeValue();
                    payAmountNode.setNodeValue(AccountingUtil.formatAmount(amount));
                }

                Node next = niPayCB.nextNode();
                paymentNode = next;
                
            }
            
            /* convert PAYMENTS_CB document to a string and append to main 
             * doc */
            sw = new StringWriter();       
            DOMUtil.print(((Document) paymentsForCB), new PrintWriter(sw));
            xmlContent = xmlContent + sw.toString();

            /* Create PAYMENTS_NOT_CB section */
            Document paymentsNotForCB = cbrDAO.doGetPaymentsNotForCB(masterOrderNum);

            /* Add num attribute to primary node. */
            numAttr = paymentsNotForCB.createAttribute("num");
            numAttr.setValue(String.valueOf(paymentsNotForCB.getFirstChild().getChildNodes().getLength()));
            Node paymentNotCBPrimaryNode = XPathAPI.selectSingleNode(paymentsNotForCB,"/payments_not_cb");
            ((Element)(paymentNotCBPrimaryNode)).setAttributeNode(numAttr);
            /* todo not cb*/
            /* Add $ before the value on AMOUNT node. If type is �Refund�, 
             * add () around the value. */
            NodeIterator niPayNotCB = XPathAPI.selectNodeIterator(paymentsNotForCB,"/payments_not_cb/payment");
            for (Node notPaymentNode=niPayNotCB.nextNode(); notPaymentNode!=null;) {
                /* check type */
                Node typeNode = XPathAPI.selectSingleNode(notPaymentNode,"type/text()");
                Node nonPayAmountNode = XPathAPI.selectSingleNode(notPaymentNode,"amount/text()"); 
                String amount = "$0.00";
                
                if(nonPayAmountNode == null) {
                    NodeList amounts = ((Element)notPaymentNode).getElementsByTagName("amount");
                    nonPayAmountNode = amounts.item(0);
                    Text amountText = paymentsNotForCB.createTextNode(amount);
                    nonPayAmountNode.appendChild(amountText);
                } else { 
                    amount = nonPayAmountNode.getNodeValue();
                    nonPayAmountNode.setNodeValue(AccountingUtil.formatAmount(amount));
                }
         
                if(typeNode.getNodeValue().equals("Refund"))
                {
                    amount = "(" + amount + ")";
                }
                          
                nonPayAmountNode.setNodeValue(amount);
                Node next = niPayNotCB.nextNode();
                notPaymentNode = next;
            }
            
            /* convert PAYMENTS_NOT_CB document to a string and append to mail 
             * doc */
            sw = new StringWriter();       
            DOMUtil.print(((Document) paymentsNotForCB), new PrintWriter(sw));
            xmlContent = xmlContent + sw.toString();
            
            /* Create CBR_COMMENTS section by creating a new CBRCommentVO 
            * object and call its toXML method. */
            CbrCommentVO cbrComments = new CbrCommentVO();
            /* set type_code to typeCode */
            Node commentNode = XPathAPI.selectSingleNode(cbr,"chargeback_retrieval/comment/text()");
            if(commentNode!=null){
              //  cbrComments.setCommentText(commentNode.getNodeValue());
            }
            else
            {
               // cbrComments.setCommentText("");
            }
            AccountingUtil acctUtil = new AccountingUtil();
            String sessionID = (String) parameters.get("securitytoken");
            if(sessionID == null)
            {
                /* attempt to retrieve from request */
                sessionID = request.getParameter("securitytoken");
            }
           // cbrComments.setCreatedBy(acctUtil.getCsrID(sessionID));

       //     cbrComments.setCreateOn(new Date());
            xmlContent = xmlContent + cbrComments.toXML();
            
            
            /* Create REASONS section  section by calling CBRDAO.doGetCBRReasons*/
            Document reasons = cbrDAO.doGetCbrReasons();
            /* convert REASONS document to a string and append to mail 
             * doc */
            sw = new StringWriter();       
            DOMUtil.print(((Document) reasons), new PrintWriter(sw));
            xmlContent = xmlContent + sw.toString();
            
            xmlContent = xmlContent + "</root>";
            
            /* convert XML String to Document object */
            displayXML = DOMUtil.getDocument(xmlContent);

            /* Add the Document in HashMap with key �doc�. */
            displayCBR.put("doc",displayXML);
            
            /* Add �load� in HashMap with key �forwardName� */
            displayCBR.put("forwardName","load");
        
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting displayAddCBR");
            } 
        }
        
        return displayCBR;
    }

    /**
     * This method rebuilds the XML to be sent back in case of an error.
     * �	Create Document as in Appendix B (cbr_db.xml)
     *      o	Add displayMessage as attribute �displayMessage� on ROOT.
     *      o	Create HEADER node with value �Charge Back�
     *      o	Create SUBHEADER node with value �Add Charge Back�
     *      o	Create CHARGEBACK_RETRIEVAL section by creating CBRVO object 
     *          from request; set type_code to �CB�; call its toXML method. 
     *          Add attribute �disabled� with value �false� to �AMOUNT� node.
     *      o	Create PAYMENTS_CB section by calling CBRDAO.doGetPaymentsForCB.
     *          Retrieve master_order_number from filter parameters HashMap. 
     *          Add num attribute to primary node. Add $ before the value on 
     *          AMOUNT node. If PAYMENTS_CB/PAYMENT/PAYMENT_ID is the same as 
     *          payment_id of CBRVO, set PAYMENTS_CB/PAYMENT/@selected to �true�.
     *      o	Create PAYMENTS_NOT_CB section by calling 
     *          CBRDAO.doGetPaymentsNotForCB. Add num attribute to primary node. 
     *          Add $ before the value on AMOUNT node. 
     *          If type is �Refund�, add () around the value.
     *      o	Create CBR_COMMENTS section by creating a new CBRCommentVO 
     *          object and call its toXML method.
     *      o	Create REASONS section by calling CBRDAO.doGetCBRReasons.
     * �	Add the Document in HashMap with key �doc�.
     * �	Add �fail� in HashMap with key �forwardName�
     * �	Return the HashMap.
     * @param request - HttpServletRequest
     * @param displayMessage - String
     * @return HashMap
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     * @todo: Finish errorAddCB
     */ 
    public HashMap errorAddCB(HttpServletRequest request, String displayMessage)
        throws Exception
    {
       if(logger.isDebugEnabled()){
            logger.debug("Entering errorAddCB");
        }

        HashMap errorCB = new HashMap();
        try{
            /* Create Document */
            Document displayXML = null;
            /* Add displayMessage as attribute �displayMessage� on ROOT.
            * Create HEADER node with value �Charge Back�
            * Create SUBHEADER node with value �Add Charge Back� */
            String xmlContent = "<root displayMessage='" + displayMessage + "'><header>Charge Back</header>";
            /* Create SUBHEADER node with value �Add � headerName */
            xmlContent = xmlContent + "<subheader>Add Charge Back</subheader>";

            /* Create CHARGEBACK_RETRIEVAL section */
            CbrVO cbrVO = new CbrVO();
            cbrVO = cbrVO.toObject(request);
            cbrVO.setTypeCode("CB");
            String cbrXML = cbrVO.toXML();
            Document cbr = DOMUtil.getDocument(cbrXML);

            /* Add attribute �disabled� with value �false� to �AMOUNT� node */
            Node amountNode = XPathAPI.selectSingleNode(cbr,"/chargeback_retrieval/amount");
            Attr disabledAttr = cbr.createAttribute("disabled");
            disabledAttr.setValue("false");
            ((Element)(amountNode)).setAttributeNode(disabledAttr);

            /* convert CHARGEBACK_RETRIEVAL document to a string and append to mail 
             * doc */
            StringWriter sw = new StringWriter();       
            DOMUtil.print(((Document) cbr), new PrintWriter(sw));
            xmlContent = xmlContent + sw.toString();

            /* Retrieve master_order_number from the filter (get attribute 
              "parameters" HashMap from request) */
            HashMap parameters = (HashMap)request.getAttribute("parameters");
            String masterOrderNum = (String) parameters.get("master_order_number");
            if(masterOrderNum == null)
            {
                masterOrderNum = (String) request.getParameter("master_order_number");
            }
            /* Create PAYMENTS_CB section  */
            CBRDAO cbrDAO = new CBRDAO(dbConn);
            Document paymentsForCB = cbrDAO.doGetPaymentsForCB(masterOrderNum, "CB");
            //XMLDocument xmlpay = (XMLDocument) paymentsForCB;

            /* Add num attribute to primary node */
            Attr numAttr = paymentsForCB.createAttribute("num");
            numAttr.setValue(String.valueOf(paymentsForCB.getFirstChild().getChildNodes().getLength()));
            Node paymentPrimaryNode = XPathAPI.selectSingleNode(paymentsForCB,"/payments_cb");
            ((Element)(paymentPrimaryNode)).setAttributeNode(numAttr);
            
            /* Add $ before the value on AMOUNT node. */
            NodeIterator niPayCB = XPathAPI.selectNodeIterator(paymentsForCB,"payments_cb/payment");
            for (Node paymentNode=niPayCB.nextNode(); paymentNode!=null;) {
                /* add $ */
                Node payAmountNode = XPathAPI.selectSingleNode(paymentNode,"amount/text()"); 
                String amount = "$0.00";
                
                if(payAmountNode == null) {
                    NodeList amounts = ((Element)paymentNode).getElementsByTagName("amount");
                    payAmountNode = amounts.item(0);
                    Text amountText = paymentsForCB.createTextNode(amount);
                    payAmountNode.appendChild(amountText);
                } else { 
                    amount = payAmountNode.getNodeValue();
                    payAmountNode.setNodeValue(AccountingUtil.formatAmount(amount));
                }
                
                Node paymentIDNode = XPathAPI.selectSingleNode(paymentNode,"payment_id/text()");           
                if(paymentIDNode.getNodeValue().equals(cbrVO.getCreditPaymentID()))
                {
                    Attr numSelectedAttr = paymentsForCB.createAttribute("selected");
                    numSelectedAttr.setValue("true");
                    ((Element)(paymentNode)).setAttributeNode(numSelectedAttr);
                }
                Node next = niPayCB.nextNode();
                paymentNode = next;
            }
            
            /* convert PAYMENTS_CB document to a string and append to mail 
             * doc */
            sw = new StringWriter();       
            DOMUtil.print(((Document) paymentsForCB), new PrintWriter(sw));
            xmlContent = xmlContent + sw.toString();

            /* Create PAYMENTS_NOT_CB section */
            Document paymentsNotForCB = cbrDAO.doGetPaymentsNotForCB(masterOrderNum);
            //XMLDocument xmlnotpay = (XMLDocument) paymentsNotForCB;
           
            /* Add num attribute to primary node. */
            numAttr = paymentsNotForCB.createAttribute("num");
            numAttr.setValue(String.valueOf(paymentsNotForCB.getFirstChild().getChildNodes().getLength()));
            Node paymentNotCBPrimaryNode = XPathAPI.selectSingleNode(paymentsNotForCB,"/payments_not_cb");
            ((Element)(paymentNotCBPrimaryNode)).setAttributeNode(numAttr);
            /* todo not cb*/
            /* Add $ before the value on AMOUNT node. If type is �Refund�, 
             * add () around the value. */
            NodeIterator niPayNotCB = XPathAPI.selectNodeIterator(paymentsForCB,"payments_not_cb/payment");
            for (Node notPaymentNode=niPayNotCB.nextNode(); notPaymentNode!=null;) {
                /* check type */
                Node typeNode = XPathAPI.selectSingleNode(notPaymentNode,"type/text()");
                Node nonPayAmountNode = XPathAPI.selectSingleNode(notPaymentNode,"amount/text()"); 
                String amount = "$0.00";
                
                if(nonPayAmountNode == null) {
                    NodeList amounts = ((Element)notPaymentNode).getElementsByTagName("amount");
                    nonPayAmountNode = amounts.item(0);
                    Text amountText = paymentsNotForCB.createTextNode(amount);
                    nonPayAmountNode.appendChild(amountText);
                } else { 
                    amount = nonPayAmountNode.getNodeValue();
                    nonPayAmountNode.setNodeValue(AccountingUtil.formatAmount(amount));
                }
                if(typeNode.getNodeValue().equals("Refund"))
                {
                    amount = "(" + amount + ")";
                }
                          
                nonPayAmountNode.setNodeValue("$" + amount);
                Node next = niPayNotCB.nextNode();
                notPaymentNode = next;
            }
            
            /* convert PAYMENTS_NOT_CB document to a string and append to mail 
             * doc */
            sw = new StringWriter();       
            DOMUtil.print(((Document) paymentsNotForCB), new PrintWriter(sw));
            xmlContent = xmlContent + sw.toString();
            
            /* Create CBR_COMMENTS section */
            String comments = (String) request.getParameter("comments");
            CbrCommentVO cbrComment = new CbrCommentVO();
            cbrComment.setCommentText(comments);

            Document commentsDoc = DOMUtil.getDocument(cbrComment.toXML());
            /* convert CBR_COMMENTS document to a string and append to mail 
             * doc */
            sw = new StringWriter();       
            DOMUtil.print(((Document) commentsDoc), new PrintWriter(sw));
            xmlContent = xmlContent + sw.toString();
            
            /* Create REASONS section  section by calling CBRDAO.doGetCBRReasons*/
            Document reasons = cbrDAO.doGetCbrReasons();
            /* convert REASONS document to a string and append to mail 
             * doc */
            sw = new StringWriter();       
            DOMUtil.print(((Document) reasons), new PrintWriter(sw));
            xmlContent = xmlContent + sw.toString();
            
            xmlContent = xmlContent + "</root>";
            
            /* convert XML String to Document object */
            displayXML = DOMUtil.getDocument(xmlContent);
            //XMLDocument xmldisplay = (XMLDocument) displayXML;
            
            /* Add the Document in HashMap with key �doc�. */
            errorCB.put("doc",displayXML);
            
            /* Add �load� in HashMap with key �forwardName� */
            errorCB.put("forwardName","fail_add");

        
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting errorAddCB");
            } 
        }
        
        return errorCB;       
    }

    /**
     * �	Obtain lock for shopping cart to of REFFUND type. If 
     *      EntityLockedException is caught, return the HashMap created in 
     *      errorAddCB. Pass displayMessage �Entity locked by CSR � + csr_id.  
     *      o	Entity type: REFUND
     *      o	Entity id: order_guid (retrieve from filter)
     *      o	Order level: ORDERS
     * �	Create CBRVO with type_code �CB� and all other fields except cbr_id 
     *      and debit_payment_id from request. 
     * �	Create CBRCommentVO.
     *      o	Retrieve created_by by calling CBRDAO.doGetCSR. 
     *          Pass the securitytoken extracted from the filter parameters 
     *          attribute HashMap.
     *      o	Retrieve comment_text from request parameter �comments�.
     * �	Retrieve CBRPaymentVO from payment id. Call CBRDAO.doGetPaymentInfo.
     * �	Start transaction (set autoCommit to false, echoed by setting 
     *      autoCommit to true in finally block)
     * �	Retrieve list of order bills from payment id. 
     *      Call CBRDAO.doGetOrderBills(CBRVO.credit_payment_id); Refund the 
     *      amounts on the order bills in this order: productAmount, 
     *      addonAmount, servShipFee, tax, until the charge back amount is 
     *      filled (CBRVO.amount). For each order bill used to fill the charge 
     *      back amount (CBRVO.amount), create a CBRRefundVO. 
     *      o	Set refundDispCode to �E10�
     *      o	Set createdBy to the csr id retrieved from security token.
     *      o	Set responsibleParty to �FTD�
     *      o	Set refundProductAmount to CBROrderBillVO.productAmount if the 
     *          productAmount is used to fill the charge back amount.
     *      o	Set refundAddonAmount to CBROrderBillVO.addonAmount if the 
     *          addonAmount is used to fill the charge back amount and 0 otherwise.
     *      o	Set refundServShipFee to CBROrderBillVO.servShipFee if the 
     *          servShipFee is used to fill the charge back amount and 0 otherwise.
     *      o	Set refundTax to CBROrderBillVO.tax if the tax is used to 
     *          fill the charge back amount and 0 otherwise.
     *      o	Set orderDetailId to CBROrderBillVO.orderDetailId.
     *      o	Set paymentType to CBRPaymentVO.paymentType
     *      o	Set ccId to CBRPaymentVO.ccId
     *      o	Set gcId to null
     *      o	Call CBRDAO.doInsertRefund to insert the refund.
     * �	Set the first refund id saved from inserting the refund to 
     *      CBRVO.debit_payment_id.
     * �	Call CBRDAO.doInsertCBR to create the chargeback.
     * �	End transaction (conn.commit())
     * �	Create Document by calling ReconBillingBO.retrieveBillingDoc, 
     *      passing the request and true.
     * �	Add the Document in HashMap with key �doc�.
     * �	Add �success� in HashMap with key �forwardName�
     * �	Return the HashMap.
     * @param request - HttpServletRequest
     * @param displayMessage - String
     * @return HashMap
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     */ 
    public HashMap handleAddCB(HttpServletRequest request) 
        throws Exception
    {

       if(logger.isDebugEnabled()){
            logger.debug("Entering handleAddCB");
        }
        HashMap addCB = new HashMap();
        String orderGUID = "";
        String sessionID = "";
        String csrID = "";
        LockUtil lockUtil = null;
        boolean lockObtained = false;
        try{
            /* retrieve data filter parameters */
            HashMap parameters = (HashMap)request.getAttribute("parameters");
            
            /* Obtain lock for shopping cart to of REFFUND type. */
            /* retrieve lock settings */
            orderGUID = (String) parameters.get("order_guid");
            if(orderGUID == null)
            {
                /* attempt to retrieve from request */
                orderGUID = request.getParameter("order_guid");
            }
            sessionID = (String) parameters.get("securitytoken");
            if(sessionID == null)
            {
                /* attempt to retrieve from request */
                sessionID = request.getParameter("securitytoken");
            }
            AccountingUtil acctUtil = new AccountingUtil();
            csrID = acctUtil.getCsrID(sessionID);
            //csrID = "CHU";
            /* attempt to gain lock */
            lockUtil = new LockUtil(dbConn);
            lockObtained = lockUtil.obtainLock(ARConstants.CB_REFUND_LOCK_ENTITY_TYPE,orderGUID,csrID,sessionID,ARConstants.CB_REFUND_LOCK_LEVEL);
            
            /* Create CBRVO with type_code �CB� and all other fields except cbr_id 
            * and debit_payment_id from request. */
            CbrVO cbrVO = new CbrVO();
            cbrVO = cbrVO.toObject(request);
            cbrVO.setTypeCode("CB");
        
            /* Create CBRCommentVO. */
            String comments = (String) request.getParameter("comments");
            CbrCommentVO cbrComment = new CbrCommentVO();
            cbrComment.setCommentText(comments);
            cbrComment.setCreatedBy(csrID);
            
            /* Retrieve CBRPaymentVO using payment id. */
            CBRDAO cbrDAO = new CBRDAO(dbConn);
            String paymentID = (String) request.getParameter("payment_id");
            CbrPaymentVO cbrPayment = cbrDAO.doGetPaymentInfo(paymentID);
           
            /* Start transaction (set autoCommit to false) */
            dbConn.setAutoCommit(false);
            
            /* create the chargeback and associate the charge back with the payment record.*/
            cbrVO.setCreditPaymentID(paymentID);
            
            String cbrID = cbrDAO.doInsertCbr(cbrVO,cbrComment);
            
            /* Check if the reversal date (date_rvsl) is entered by the user. */
            String reversalDate = request.getParameter("date_rvsl")!=null?request.getParameter("date_rvsl"):"";
            if(reversalDate.equals("")){
                /* create refunds:  */
                logger.debug("applying chargeback to payment id: " + paymentID);
                
                /*Retrieve list of order bills from payment id */
                ArrayList orderBills = cbrDAO.doGetOrderBills(paymentID);
                Iterator billIterator = orderBills.iterator();
                String refundID = "";
                String debitPaymentId = "";
                Double chargebackOriginal = new Double(cbrVO.getAmount());
                Double chargebackLeft = new Double(cbrVO.getAmount());
                logger.debug("Chargeback amount is: " + cbrVO.getAmount() + " should equal to " + chargebackLeft.doubleValue());
      
                while(billIterator.hasNext() && chargebackLeft.doubleValue() > 0)
                {
                    CbrOrderBillVO orderBill = (CbrOrderBillVO) billIterator.next();
                    /*create refund */
                    CbrRefundVO cbrRefund = new CbrRefundVO();
                    cbrRefund.setRefundAddonAmount(0);
                    cbrRefund.setRefundServiceFee(0);
                    cbrRefund.setRefundShippingFee(0);
                    cbrRefund.setRefundTax(0);
                    cbrRefund.setRefundAdminFee(0);
                    cbrRefund.setRefundServiceFeeTax(0);
                    cbrRefund.setRefundShippingTax(0);
                    cbrRefund.setRefundCommissionAmount(0);
                    cbrRefund.setRefundWholesaleAmount(0);
                    cbrRefund.setRefundWholesaleServiceFee(0);
                    cbrRefund.setRefundDispCode("E10");
                    cbrRefund.setCreatedBy(csrID);
                    cbrRefund.setResponsibleParty(csrID);
                    cbrRefund.setOrderDetailId(orderBill.getOrderDetailId());
                    cbrRefund.setAcctTransInd("Y");
                    cbrRefund.setRefundStatus("Billed");
                    //cbrRefund.setCBRPaymentVO(cbrPayment);
                     
                     /* product amount */
                    HashMap refundAmounts = applyRefund
                        (new Double(chargebackLeft.doubleValue() + orderBill.getDiscountAmount()),orderBill.getProductAmount()); 
                    Double refund = (Double) refundAmounts.get("ApplyAmount");
                    cbrRefund.setRefundDiscountAmount(orderBill.getDiscountAmount());
                    cbrRefund.setRefundProductAmount(refund.doubleValue());
                    logger.debug("Calling applyRefund with: " + chargebackLeft.doubleValue() 
                        + " + " + orderBill.getDiscountAmount() + ", " + orderBill.getProductAmount());
                    logger.debug("setRefundDiscountAmount to: " + cbrRefund.getRefundDiscountAmount());
                    logger.debug("setRefundProductAmount to: " + cbrRefund.getRefundProductAmount());
                    
                    chargebackLeft = (Double) refundAmounts.get("ChargebackLeft");
                    
                    logger.debug("chargebackLeft is: " + chargebackLeft.doubleValue());
                    //cbAmount = chargebackLeft;
                    
                    if(chargebackLeft.doubleValue() > 0)
                    {
                    
                        logger.debug("Calling applyRefund with: " + chargebackLeft.doubleValue() + ", " + orderBill.getAddonAmount());
                        
                        cbrRefund.setRefundProductAmount(orderBill.getProductAmount());
                        refundAmounts = applyRefund(chargebackLeft,orderBill.getAddonAmount());  
                        refund = (Double) refundAmounts.get("ApplyAmount");
                        cbrRefund.setRefundAddonAmount(refund.doubleValue());
                        chargebackLeft = (Double) refundAmounts.get("ChargebackLeft");
                        
                        logger.debug("setRefundAddonAmount: " + cbrRefund.getRefundAddonAmount());
                        logger.debug("chargeback left:" + chargebackLeft.doubleValue());
                        
                        if(chargebackLeft.doubleValue() > 0)
                        {
                            logger.debug("Calling applyRefund with: " + chargebackLeft.doubleValue() + ", " + orderBill.getServiceFee());
                            
                            refundAmounts = applyRefund(chargebackLeft,orderBill.getServiceFee());  
                            refund = (Double) refundAmounts.get("ApplyAmount");
                            cbrRefund.setRefundServiceFee(refund.doubleValue());
                            chargebackLeft = (Double) refundAmounts.get("ChargebackLeft");
                            
                            logger.debug("setRefundServiceFee: " + cbrRefund.getRefundServiceFee());
                            logger.debug("chargeback left:" + chargebackLeft.doubleValue());
                            if(chargebackLeft.doubleValue() > 0)
                            {
                                logger.debug("Calling applyRefund with: " + chargebackLeft.doubleValue() + ", " + orderBill.getShippingFee());
                            
                                refundAmounts = applyRefund(chargebackLeft,orderBill.getShippingFee());  
                                refund = (Double) refundAmounts.get("ApplyAmount");
                                cbrRefund.setRefundShippingFee(refund.doubleValue());
                                chargebackLeft = (Double) refundAmounts.get("ChargebackLeft");
                                
                                logger.debug("setRefundShippingFee: " + cbrRefund.getRefundShippingFee());
                                logger.debug("chargeback left:" + chargebackLeft.doubleValue());
                                
                                if(chargebackLeft.doubleValue() > 0)
                                {
                                    logger.debug("Calling applyRefund with: " + chargebackLeft.doubleValue() + ", " + orderBill.getTax());
                            
                                    refundAmounts = applyRefund(chargebackLeft,orderBill.getTax());  
                                    refund = (Double) refundAmounts.get("ApplyAmount");
                                    cbrRefund.setRefundTax(refund.doubleValue());
                                    chargebackLeft = (Double) refundAmounts.get("ChargebackLeft");
                                    
                                    logger.debug("setRefundTax: " + cbrRefund.getRefundTax());
                                    logger.debug("chargeback left:" + chargebackLeft.doubleValue());
                                    //distribute the total tax refund across tax buckets, ie tax1Amount, tax2Amount, tax3Amount, tax4Amount, tax5Amount
                                    //Add each bucket to the cbrRefundVO
                                    Double totalTaxRefunded = refund;
                                    if(totalTaxRefunded.doubleValue() > 0)
                                    {
                                    	HashMap taxRefundAmounts = null;
                                    	Double taxRefunded = new Double(0);
                                    	
                                    	if (orderBill.getTax1Amount() != null && orderBill.getTax1Amount().doubleValue() > 0){
                                    		taxRefundAmounts = applyRefund(totalTaxRefunded,orderBill.getTax1Amount().doubleValue());
                                    		taxRefunded  = (Double) taxRefundAmounts.get("ApplyAmount");
                                    		cbrRefund.setTax1Name(orderBill.getTax1Name());
                                    		cbrRefund.setTax1Amount(new BigDecimal (taxRefunded.toString()));
                                    		cbrRefund.setTax1Description("Chargeback");
                                    		totalTaxRefunded = (Double) taxRefundAmounts.get("ChargebackLeft");
                                    		logger.debug("tax1 refunded: " + taxRefunded.toString());
                                    		logger.debug("tax1 remaining: " + totalTaxRefunded);
                                    	}
                                    	if (totalTaxRefunded.doubleValue() > 0 && orderBill.getTax2Amount() != null && orderBill.getTax2Amount().doubleValue() > 0){
                                    		taxRefundAmounts = applyRefund(totalTaxRefunded,orderBill.getTax2Amount().doubleValue());
                                    		taxRefunded  = (Double) taxRefundAmounts.get("ApplyAmount");
                                    		cbrRefund.setTax2Name(orderBill.getTax2Name());
                                    		cbrRefund.setTax2Amount(new BigDecimal (taxRefunded.toString()));
                                    		cbrRefund.setTax2Description("Chargeback");
                                    		totalTaxRefunded = (Double) taxRefundAmounts.get("ChargebackLeft");
                                    		logger.debug("tax2 refunded: " + taxRefunded.toString());
                                    		logger.debug("tax2 remaining: " + totalTaxRefunded);
                                    	}
                                    	if (totalTaxRefunded.doubleValue() > 0 && orderBill.getTax3Amount() != null && orderBill.getTax3Amount().doubleValue() > 0){
                                    		taxRefundAmounts = applyRefund(totalTaxRefunded,orderBill.getTax3Amount().doubleValue());
                                    		taxRefunded  = (Double) taxRefundAmounts.get("ApplyAmount");
                                    		cbrRefund.setTax3Name(orderBill.getTax3Name());
                                    		cbrRefund.setTax3Amount(new BigDecimal (taxRefunded.toString()));
                                    		cbrRefund.setTax3Description("Chargeback");
                                    		totalTaxRefunded = (Double) taxRefundAmounts.get("ChargebackLeft");
                                    		logger.debug("tax3 refunded: " + taxRefunded.toString());
                                    		logger.debug("tax3 remaining: " + totalTaxRefunded);                                    	
                                    	}
                                    	if (totalTaxRefunded.doubleValue() > 0 && orderBill.getTax4Amount() != null && orderBill.getTax4Amount().doubleValue() > 0){
                                    		taxRefundAmounts = applyRefund(totalTaxRefunded,orderBill.getTax4Amount().doubleValue());
                                    		taxRefunded  = (Double) taxRefundAmounts.get("ApplyAmount");
                                    		cbrRefund.setTax4Name(orderBill.getTax4Name());
                                    		cbrRefund.setTax4Amount(new BigDecimal (taxRefunded.toString()));
                                    		cbrRefund.setTax4Description("Chargeback");
                                    		totalTaxRefunded = (Double) taxRefundAmounts.get("ChargebackLeft");
                                    		logger.debug("tax4 refunded: " + taxRefunded.toString());
                                    		logger.debug("tax4 remaining: " + totalTaxRefunded);                                    	
                                    	}
                                    	if (totalTaxRefunded.doubleValue() > 0 && orderBill.getTax5Amount() != null && orderBill.getTax5Amount().doubleValue() > 0){
                                    		taxRefundAmounts = applyRefund(totalTaxRefunded,orderBill.getTax5Amount().doubleValue());
                                    		taxRefunded  = (Double) taxRefundAmounts.get("ApplyAmount");
                                    		cbrRefund.setTax5Name(orderBill.getTax5Name());
                                    		cbrRefund.setTax5Amount(new BigDecimal (taxRefunded.toString()));
                                    		cbrRefund.setTax5Description("Chargeback");
                                    		totalTaxRefunded = (Double) taxRefundAmounts.get("ChargebackLeft");
                                    		logger.debug("tax5 refunded: " + taxRefunded.toString());
                                    		logger.debug("tax5 remaining: " + totalTaxRefunded);
                                    	}                                   	
                                    	
                                    }
                                    
                                   
                                    if(chargebackLeft.doubleValue() > 0)
                                    {
                                        logger.debug("Calling applyRefund with: " + chargebackLeft.doubleValue() + ", " + orderBill.getServiceFeeTax());
                            
                                        refundAmounts = applyRefund(chargebackLeft,orderBill.getServiceFeeTax());  
                                        refund = (Double) refundAmounts.get("ApplyAmount");
                                        cbrRefund.setRefundServiceFeeTax(refund.doubleValue());
                                        chargebackLeft = (Double) refundAmounts.get("ChargebackLeft");
                                        logger.debug("setRefundTax: " + cbrRefund.getRefundServiceFeeTax());
                                        logger.debug("chargeback left:" + chargebackLeft.doubleValue());
                                        if(chargebackLeft.doubleValue() > 0)
                                        {
                                            logger.debug("Calling applyRefund with: " + chargebackLeft.doubleValue() + ", " + orderBill.getShippingTax());
                            
                                            refundAmounts = applyRefund(chargebackLeft,orderBill.getShippingTax());  
                                            refund = (Double) refundAmounts.get("ApplyAmount");
                                            cbrRefund.setRefundShippingTax(refund.doubleValue());
                                            chargebackLeft = (Double) refundAmounts.get("ChargebackLeft");
                                            logger.debug("setRefundTax: " + cbrRefund.getRefundShippingTax());
                                            logger.debug("chargeback left:" + chargebackLeft.doubleValue());
                                        }
                                    }
                                }
                            }
                        }
                            
                    }
                    int iMilesPointToBeRefunded = 0; 
                    String sMilesPointToBeRefunded = null; 
                    if (cbrPayment.getMpRedemptionRateAmt()>0)
                    {
                      //call calculateMilesPoints, which will calcuate the total miles to be refunded
                      iMilesPointToBeRefunded = calculateMilesPoints(chargebackOriginal, cbrPayment.getPaymentType(), new Double(cbrPayment.getMpRedemptionRateAmt()).toString());
                      
                      if (iMilesPointToBeRefunded > 0)
                        sMilesPointToBeRefunded = new Integer(iMilesPointToBeRefunded).toString(); 
                    }

                    // insert a refund 
                    logger.debug("inserting refund...");
                    refundID = cbrDAO.doInsertRefund(cbrRefund);
                    logger.debug("refundID: " + refundID);
                    
                    /*
                     * Stamp refund record with cc auth provider.
                     */
                    ConfigurationUtil cu = ConfigurationUtil.getInstance();
                    String ccAuthProvider = null;
                    String ccType = cbrPayment.getPaymentType();
                    String bamsCCList = cu.getFrpGlobalParm(ARConstants.AUTH_CONFIG, "bams.cc.list");
                    if( ccType != null && checkValidBamsCC(ccType, bamsCCList)){
                    	ccAuthProvider = cu.getFrpGlobalParm("AUTH_CONFIG", "cc.auth.provider");
                    }
                    // insert a debit payment for the refund
                    debitPaymentId = cbrDAO.doInsertCBRPayment(paymentID, refundID, csrID, sMilesPointToBeRefunded, ccAuthProvider);
                    
                    // update refund_date
                    CommonDAO commondao = new CommonDAO(dbConn);
                    long currentTime = System.currentTimeMillis();
                    commondao.doUpdatePaymentRefundStatus(refundID, currentTime, "Billed");              
                   
                    
                    // insert a cbr payment ref
                    cbrDAO.doInsertCBRPaymentRef(cbrID, debitPaymentId, csrID);

                }
                /* insert refund IDs */
                logger.debug("chargeback left:" + chargebackLeft.doubleValue());
                if(chargebackLeft.doubleValue() > 0) {
                    dbConn.rollback();
                    throw new Exception("Bill amount is less than charge back amount.");
                } 
             }
             
            /* End transaction */
            dbConn.commit();
            
            /* Create Document by calling ReconBillingBO.retrieveBillingDoc */
            ReconBillingBO reconBO = new ReconBillingBO(dbConn);
            Document reconBilling = reconBO.retrieveBillingDoc(request,true);
            /* Add the Document in HashMap with key �doc�.*/
            addCB.put("doc",reconBilling);
            /* Add �success� in HashMap with key �forwardName�*/
            addCB.put("forwardName","success");

        }catch(EntityLockedException e)
        {     
            /* lock cannot be obtained, return the error HashMap */
            logger.error(e);
            addCB = errorAddCB(request,e.getMessage());
            dbConn.rollback();
        } catch(Exception ex) {
            logger.error(ex);
            addCB = errorAddCB(request,ex.getMessage());
            dbConn.rollback();
        }finally {
            if(lockObtained == true){
                lockUtil.releaseLock(ARConstants.CB_REFUND_LOCK_ENTITY_TYPE,orderGUID,csrID,sessionID);
            }
            /* set auto commit back to true */
            dbConn.setAutoCommit(true);
            if(logger.isDebugEnabled()){
               logger.debug("Exiting handleAddCB");
            } 
        }
        
        return addCB;
    }

    private HashMap applyRefund(Double cb, double billAmount) throws Exception
    {
       if(logger.isDebugEnabled()){
            logger.debug("Entering applyRefund");
            logger.debug("Chargeback Amount" + cb);
            logger.debug("Bill Amount" + billAmount);
        }
        
        AccountingUtil acctgUtil = new AccountingUtil();
        double cbAmount = cb.doubleValue();
        logger.debug("chargeback amount before format: " + cbAmount);
        cbAmount = Double.parseDouble(acctgUtil.formatAmountToPattern(String.valueOf(cbAmount), ARConstants.CURRENCY_PATTERN_NO_DOLLAR));
        logger.debug("chargeback amount after format: " + cbAmount);
        
        logger.debug("bill amount before format: " + billAmount);
        billAmount = Double.parseDouble(acctgUtil.formatAmountToPattern(String.valueOf(billAmount), ARConstants.CURRENCY_PATTERN_NO_DOLLAR));
        logger.debug("bill amount after format: " + billAmount);
        HashMap amounts = new HashMap();
        
        try{
            Double applyAmount;
            /* refund amount */
            if(cbAmount != 0)
            {
                logger.debug("cbAmount is not 0");
                if(cbAmount >= billAmount)
                {
                    logger.debug("cbAmount >= billAmount");
                    applyAmount = new Double(billAmount);
                    Double updateRefund = new Double(cbAmount - billAmount);
                    
                    amounts.put("ApplyAmount",applyAmount);
                    amounts.put("ChargebackLeft",updateRefund);
                    logger.debug("putting applyAmount:" + applyAmount.doubleValue());
                    logger.debug("putting chargebackLeft:" + updateRefund.doubleValue());
                }
                else 
                {
                    logger.debug("cbAmount < billAmount");
                    applyAmount = new Double(cbAmount);
                    cb = new Double(0);
                    amounts.put("ApplyAmount",applyAmount);
                    amounts.put("ChargebackLeft",cb);
                    logger.debug("putting applyAmount:" + applyAmount.doubleValue());
                    logger.debug("putting chargebackLeft:" + cb.doubleValue());
                }
            }
            else
            {
                logger.debug("cbAmount is null.");
                applyAmount = new Double(0);
                amounts.put("ApplyAmount", applyAmount);
                amounts.put("ChargebackLeft",cb);
                logger.debug("putting applyAmount:" + applyAmount.doubleValue());
                logger.debug("putting chargebackLeft:" + cb.doubleValue());
            }

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting applyRefund");
            } 
        }
        logger.debug("returning: appliedAmount=" + ((Double)amounts.get("ApplyAmount")).doubleValue());
        logger.debug("returning: chargebackLeft=" + ((Double)amounts.get("ChargebackLeft")).doubleValue()); 
        
        return amounts;
    }
    /**
     * �	Create CBRVO with type_code �RT� and all other fields except 
     *      cbr_id and debit_payment_id from request. 
     * �	Create CBRCommentVO.
     *      o	Retrieve created_by by calling CBRDAO.doGetCSR. Pass the 
     *          securitytoken extracted from the filter parameters attribute HashMap.
     *      o	Retrieve comment_text from request parameter �comments�.
     * �	Call CBRDAO.doInsertCBR to create the retrieval.
     * �	Create Document by calling ReconBillingBO.retrieveBillingDoc, 
     *      pass the request and true.
     * �	Add the Document in HashMap with key �doc�.
     * �	Add �success� in HashMap with key �forwardName�
     * �	Return the HashMap.
     * @param request - HttpServletRequest
     * @param displayMessage - String
     * @return HashMap
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     */ 
    public HashMap handleAddRT(HttpServletRequest request)
        throws Exception
    {
       if(logger.isDebugEnabled()){
            logger.debug("Entering handleAddRT");
        }
        HashMap addRT = new HashMap();
        try{
            /* Create CBRVO with type_code �RT� and all other fields except 
                cbr_id and debit_payment_id from request.  */
            CbrVO cbr = new CbrVO();
            cbr = cbr.toObject(request);
            cbr.setTypeCode("RT");
            cbr.setCBRId("");
            
            /* Create CBRCommentVO. */
            HashMap parameters = (HashMap)request.getAttribute("parameters");
            CbrCommentVO cbrComment = createCBRCommentVO(parameters,request);
            
            /* Call CBRDAO.doInsertCBR to create the retrieval. */
            CBRDAO cbrDAO = new CBRDAO(dbConn);
            cbrDAO.doInsertCbr(cbr,cbrComment);
            
            /* Create Document by calling ReconBillingBO.retrieveBillingDoc, 
            * pass the request and true.*/
            ReconBillingBO reconBO = new ReconBillingBO(dbConn);
            Document reconBilling = reconBO.retrieveBillingDoc(request,true);
            /* Add the Document in HashMap with key �doc�.*/
            addRT.put("doc",reconBilling);
            /* Add �success� in HashMap with key �forwardName�*/
            addRT.put("forwardName","success");
            
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting handleAddRT");
            } 
        }
        
        return addRT;
    }

    /**
     * This method builds the XML to be sent back when the Edit Charge Back 
     * or Add Retrieval page is loaded.
     * �	Create Document as in Appendix B (cbr_db.xml)
     *      o	Extract cbr_id from request. Retrieve CBRVO from the cbr_id. 
     *          Call CBRDAO.doGetCBR.
     *      o	Create document with HEADER and SUBHEADER by calling 
     *          createEditCBRHeader(String typeCode).
     *      o	Append CHARGEBACK_RETRIEVAL section by calling CBRVO.toXML. 
     *          Add attribute �disabled� with value �true� to �AMOUNT� node.
     *      o	Append PAYMENTS_CB node with num=0. 
     *      o	Append PAYMENTS_NOT_CB section. 
     *          ?	If CBRVO.credit_payment_id is null, 
     *              create the node and set num=�0�. 
     *          ?	If not null, num=�1�. Create the section by calling 
     *              CBRDAO.doGetPaymentsNotForCB. Remove all PAYMENT but the 
     *              one with type �Payment�. Add $ before the value on AMOUNT node.
     *      o	Append CBR_COMMENTS section by calling CBRDAO.doGetCommentHistory.
     *      o	Append REASONS section by calling CBRDAO.doGetCBRReasons.
     * �	Add the Document in HashMap with key �doc�.
     * �	Add �load� in HashMap with key �forwardName�
     * �	Return the HashMap.
     * @param request - HttpServletRequest
     * @param displayMessage - String
     * @return HashMap
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     * @todo: Finish handleDisplayEdit
     */ 
    public HashMap handleDisplayEdit(HttpServletRequest request) 
        throws Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering handleDisplayEdit");
        }
        HashMap displayEdit = new HashMap();
        String orderGUID = "";
        String sessionID = "";
        String csrID = "";
        LockUtil lockUtil = null;
        boolean lockObtained = false;
        try{
            /* Extract cbr_id from request. Retrieve CBRVO from the cbr_id. 
            * Call CBRDAO.doGetCBR. */
            String cbrID = (String) request.getParameter("cbr_id");
            CBRDAO cbrDAO = new CBRDAO(dbConn);
            CbrVO cbrVO = cbrDAO.doGetCbr(cbrID);
            if(cbrVO.getTypeCode().equals("CB")){
                try{
                    lockUtil = new LockUtil(dbConn);
                    /* retrieve data filter parameters */
                    HashMap parameters = (HashMap)request.getAttribute("parameters");
                    /* Obtain lock for shopping cart to of REFFUND type. */
                    /* retrieve lock settings */
                    orderGUID = (String) parameters.get("order_guid");
                    if(orderGUID == null)
                    {
                        /* attempt to retrieve from request */
                        orderGUID = request.getParameter("order_guid");
                    }
                    sessionID = (String) parameters.get("securitytoken");
                    if(sessionID == null)
                    {
                        /* attempt to retrieve from request */
                        sessionID = request.getParameter("securitytoken");
                    }
                    AccountingUtil acctUtil = new AccountingUtil();
                    csrID = acctUtil.getCsrID(sessionID);
                    
                    /* attempt to gain lock */
                    lockObtained = lockUtil.obtainLock(ARConstants.CB_REFUND_LOCK_ENTITY_TYPE,orderGUID,csrID,sessionID,ARConstants.CB_REFUND_LOCK_LEVEL);
                } catch(EntityLockedException e) {     
                        /* call ReconBillingBO.retrieveBillingDoc(request, false) 
                         * to get the document */
                        ReconBillingBO reconBO = new ReconBillingBO(dbConn);
                        Document recon = reconBO.retrieveBillingDoc(request,false);
        
                        /* Add displayMessage attribute to root node */
                        Attr numAttr = recon.createAttribute("displayMessage");
                        numAttr.setValue(e.getMessage());
                        Node rootNode = XPathAPI.selectSingleNode(recon,"/root");
                        ((Element)(rootNode)).setAttributeNode(numAttr);
                        /* create map and add forward name and the document to display */
                        HashMap errorDisplay = new HashMap();
                        errorDisplay.put("doc",recon);
                        errorDisplay.put("forward","fail_display_edit");
                        return errorDisplay;
                  }
            }    
        
            /* Retrieve master_order_number from the filter (get attribute 
              "parameters" HashMap from request) */
            HashMap parameters = (HashMap)request.getAttribute("parameters");
            String masterOrderNum = (String) parameters.get("master_order_number");
            if(masterOrderNum == null)
            {
                /* attempt to retrieve from request */
                masterOrderNum = request.getParameter("master_order_number");
            }
            Document cbrDoc = DOMUtil.getDocument(cbrVO.toXML());

            /* Create document with HEADER and SUBHEADER */
            Node typeCodeNode = XPathAPI.selectSingleNode(cbrDoc,"/chargeback_retrieval/type_code/text()");
            Document cbr_db = createEditCBRHeader(typeCodeNode.getNodeValue());
            
            /* Append CHARGEBACK_RETRIEVAL section by calling CBRVO.toXML. 
               Add attribute �disabled� with value �true� to �AMOUNT� node. */

            Node amountNode = XPathAPI.selectSingleNode(cbrDoc,"/chargeback_retrieval/amount");
            Attr disabledAttr = cbrDoc.createAttribute("disabled");
            disabledAttr.setValue("true");
            ((Element)(amountNode)).setAttributeNode(disabledAttr);

            Node reversalDateNode = XPathAPI.selectSingleNode(cbrDoc,"/chargeback_retrieval/reversal_date");
            if(reversalDateNode.getFirstChild()!=null){
                disabledAttr = cbrDoc.createAttribute("disabled");
                disabledAttr.setValue("true");
                ((Element)(reversalDateNode)).setAttributeNode(disabledAttr);
            }

            DOMUtil.addSection(cbr_db,cbrDoc.getChildNodes());
            
            /* Append PAYMENTS_CB node with num=0. */
            Document paymentsForCB = DOMUtil.getDocument("<payments_cb num='0'></payments_cb>");
            DOMUtil.addSection(cbr_db,paymentsForCB.getChildNodes());

            // Append PAYMENTS_NOT_CB section. 
            Document paymentsNotForCB = cbrDAO.doGetCBRPayment(cbrID);
            
            // Add num attribute to primary node. 
            Attr numAttr = paymentsNotForCB.createAttribute("num");
            numAttr.setValue("1");
            
            // add $ 
            Node payAmountNode = XPathAPI.selectSingleNode(paymentsNotForCB,"/payments_not_cb/payment/amount/text()"); 
            String amount = "$0.00";
                
            if(payAmountNode == null) {
                 payAmountNode = XPathAPI.selectSingleNode(paymentsNotForCB,"/payments_not_cb/payment/amount");
                 Text amountText = paymentsNotForCB.createTextNode(amount);
                 payAmountNode.appendChild(amountText);
            } else { 
                 amount = payAmountNode.getNodeValue();
                 payAmountNode.setNodeValue(AccountingUtil.formatAmount(amount));
            }
            
            // add type 'Payment'
            Node paymentElem = XPathAPI.selectSingleNode(paymentsNotForCB,"/payments_not_cb/payment");
            Node typeElem = paymentsNotForCB.createElement("type");
            paymentElem.appendChild(typeElem);

            Text typeText = paymentsNotForCB.createTextNode("Payment");
            typeElem.appendChild(typeText);paymentsNotForCB.createTextNode(amount);
            
            DOMUtil.addSection(cbr_db, paymentsNotForCB.getChildNodes());

            /* Create CBR_COMMENTS section */
            Document comments = cbrDAO.doGetCommentHistory(cbrID);
            DOMUtil.addSection(cbr_db,comments.getChildNodes());
            
            /* Create REASONS section */
            Document reasons = cbrDAO.doGetCbrReasons();
            DOMUtil.addSection(cbr_db,reasons.getChildNodes());

            /* Add the Document in HashMap with key �doc�. */
            displayEdit.put("doc",cbr_db);
            
            /* Add �load� in HashMap with key �forwardName� */
            displayEdit.put("forwardName","load");

        } finally {
            if(lockObtained) {
                lockUtil.releaseLock(ARConstants.CB_REFUND_LOCK_ENTITY_TYPE,orderGUID,csrID,sessionID);
            }
            if(logger.isDebugEnabled()){
               logger.debug("Exiting handleDisplayEdit");
            } 
        }
      
        return displayEdit;
    }

    /**
     * �	Create document with �ROOT� as document element name.
     * �	Add HEADER node. If typeCode is �CB�, value is �Charge Back�; 
     *      if typeCode is �RT�, value is �Retrieval�.
     * �	Add SUBHEADER node. If typecode is �CB�, value is �Edit Charge Back�; 
     *      if typeCode is �RT�, value is �Edit Retrieval�.
     * �	Return document.
     * @param request - HttpServletRequest
     * @param displayMessage - String
     * @return HashMap
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     */ 
    private Document createEditCBRHeader (String typeCode)
        throws Exception
        
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering createEditCBRHeader");
        }
        Document header = null;
        try{
            String xml = "<root>";
            if(typeCode.equals("CB"))
            {
                xml = xml + "<header>Charge Back</header>";
                xml = xml + "<subheader>Edit Charge Back</subheader>";
            }else if(typeCode.equals("RT"))
            {
                xml = xml + "<header>Retrieval</header>";
                xml = xml + "<subheader>Edit Retrieval</subheader>";
            }
            
            xml = xml + "</root>";
            header = DOMUtil.getDocument(xml);
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting createEditCBRHeader");
            } 
        }
        
        return header;
    }

    /**
     * �	Create CBRVO except amount, credit_payment_id, debit_payment_id, 
     *      and type_code from request. 
     * �	Create CBRCommentVO.
     *      o	Retrieve created_by by calling CBRDAO.doGetCSR. Pass the 
     *          securitytoken extracted from the filter parameters attribute HashMap.
     *      o	Retrieve comment_text from request parameter �comments�.
     * �	Call CBRDAO.doUpdateCBR to create the retrieval.
     * �	Create Document by calling ReconBillingBO.retrieveBillingDoc, 
     *      passing the request and true.
     * �	Add the Document in HashMap with key �doc�.
     * �	Add �success� in HashMap with key �forwardName�
     * �	Return the HashMap.
     * @param request - HttpServletRequest
     * @param displayMessage - String
     * @return HashMap
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     */ 
    public HashMap handleEdit(HttpServletRequest request)
        throws Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering handleEdit");
        }
        HashMap edit = new HashMap();
        String orderGUID = "";
        String sessionID = "";
        String csrID = "";
        LockUtil lockUtil = null;
        boolean lockObtained = false;
        try{
            /* Create CBRVO except amount, credit_payment_id, debit_payment_id, 
                and type_code from request.   */
            CbrVO cbr = new CbrVO();
            cbr = cbr.toObject(request);
          //  cbr.setTypeCode("");
            cbr.setCreditPaymentID("");
            
            /* Create CBRCommentVO. */
            HashMap parameters = (HashMap)request.getAttribute("parameters");
            CbrCommentVO cbrComment = createCBRCommentVO(parameters,request);
            
            /* start transaction */
            dbConn.setAutoCommit(false);
            CBRDAO cbrDAO = new CBRDAO(dbConn);
            /* Check if the reversal date (date_rvsl) is entered by the user. */
            String reversalDate = request.getParameter("date_rvsl")!=null?request.getParameter("date_rvsl"):"";
            if(!reversalDate.equals(""))
            {
                /* Obtain lock for shopping cart of REFFUND type */
                lockUtil = new LockUtil(dbConn);

                /* Obtain lock for shopping cart to of REFFUND type. */
                /* retrieve lock settings */
                orderGUID = (String) parameters.get("order_guid");
                if(orderGUID == null)
                {
                    /* attempt to retrieve from request */
                    orderGUID = request.getParameter("order_guid");
                }
                sessionID = (String) parameters.get("securitytoken");
                if(sessionID == null)
                {
                    /* attempt to retrieve from request */
                    sessionID = request.getParameter("securitytoken");
                }
                AccountingUtil acctUtil = new AccountingUtil();
                csrID = acctUtil.getCsrID(sessionID);
                try{
                    /* attempt to gain lock */
                    lockObtained = lockUtil.obtainLock(ARConstants.CB_REFUND_LOCK_ENTITY_TYPE,orderGUID,csrID,sessionID,ARConstants.CB_REFUND_LOCK_LEVEL);
                }catch(EntityLockedException e)
                {     
                    /* return the HashMap created in errorAddCB */
                    logger.error(e);
                    HashMap errorEdit = errorAddCB(request,e.getMessage());
                    return errorEdit;
                }catch(Exception e) {
                    logger.error(e);
                    HashMap errorEdit = errorAddCB(request,e.getMessage());
                    return errorEdit;
                }
                /* delete the refunds associated with the charge back */
                cbrDAO.doDeleteCBRRefunds(cbr.getCBRId());
                
            }    
            /* Call CBRDAO.doUpdateCBR to create the retrieval. */
            cbrDAO.doUpdateCBR(cbr,cbrComment);
            
            /* End transaction */
            dbConn.commit();
            /* Create Document by calling ReconBillingBO.retrieveBillingDoc, 
            * pass the request and true.*/
            ReconBillingBO reconBO = new ReconBillingBO(dbConn);
            Document reconBilling = reconBO.retrieveBillingDoc(request,true);
            /* Add the Document in HashMap with key �doc�.*/
            edit.put("doc",reconBilling);
            /* Add �success� in HashMap with key �forwardName�*/
            edit.put("forwardName","success");
        
        } finally {
            if(lockObtained == true){
                lockUtil.releaseLock(ARConstants.CB_REFUND_LOCK_ENTITY_TYPE,orderGUID,csrID,sessionID);
            }
            dbConn.setAutoCommit(true);
            if(logger.isDebugEnabled()){
               logger.debug("Exiting handleEdit");
            } 
        }
        
        return edit;
    }

    /**
     * Create CbrCommentVO from data in the request and filter
     * @param parameters - HashMap
     * @param request - HttpServletRequest
     * @return CbrCommentVO
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     */ 
    private CbrCommentVO createCBRCommentVO(HashMap parameters,HttpServletRequest request)
        throws Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering createCBRCommentVO");
        }
        CbrCommentVO commentVO = new CbrCommentVO();
        try{
            /* Retrieve created_by by calling CBRDAO.doGetCSR. Pass the 
            * securitytoken extracted from the filter parameters 
            * attribute HashMap. */
            String securityToken = (String) parameters.get("securitytoken");
            if(securityToken == null)
            {
                /* attempt to retrieve from request */
                securityToken = request.getParameter("securitytoken");
            }
            AccountingUtil acctUtil = new AccountingUtil();
            commentVO.setCreatedBy(acctUtil.getCsrID(securityToken));
            
            /* Retrieve comment_text from request parameter �comments�.  */
            commentVO.setCommentText(request.getParameter("comments"));
            
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting createCBRCommentVO");
            } 
        }
        
        return commentVO; 
    }


  private int calculateMilesPoints(double totalRefundAmount, String paymentType,  String mpRedemptionRateAmt)
    throws Exception
  {
    int milesPoints = 0;

    PaymentMethodMilesPointsVO pmmpVO = FTDCommonUtils.getPaymentMethodMilesPoints(paymentType, this.dbConn);

    if (pmmpVO != null)
    {
      milesPoints = 
          FTDCommonUtils.convertDollarsToMilesPoints((new Double(totalRefundAmount)).toString(), mpRedemptionRateAmt, 
                                                     pmmpVO.getDollarToMpOperator(), pmmpVO.getRoundingScaleQty(), 
                                                     pmmpVO.getRoundingMethodId());
    }
    else
    {
      throw new Exception("No entry found in payment_method_miles_points for payment method: " + paymentType);
    }

    return milesPoints;
  }

  /**
	 * Checks the given ccType present in comma separated list of credit cards. Return true if present other wise false
	 * 
	 * @param ccType
	 * @param csCcList
	 * @return
	 */
	public static boolean checkValidBamsCC(String ccType, String csCcList) {
		String[] ccList = csCcList.split(",");
		for(String cardType : ccList) {
			if(ccType.equals(cardType)) {
				return true;
			}
		}
		return false;
	}


}
