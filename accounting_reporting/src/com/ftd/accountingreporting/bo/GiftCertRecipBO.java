package com.ftd.accountingreporting.bo;

import java.sql.Connection;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import com.ftd.accountingreporting.action.GiftCertActionHelper;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.dao.GccRecipientDAO;
import com.ftd.accountingreporting.dao.GccUtilDAO;
import com.ftd.accountingreporting.dao.GiftCertificateCouponDAO;
import com.ftd.accountingreporting.util.AccountingUtil;
import com.ftd.accountingreporting.vo.GccRecipientVO;
import com.ftd.osp.utilities.GiftCodeUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.IssuedToRequestList;
import com.ftd.osp.utilities.vo.RecipientInfo;
import com.ftd.osp.utilities.vo.UpdateIssuedToRequest;
import com.ftd.osp.utilities.xml.DOMUtil;

/**
 * This Class has all the business methods related to Gift Certificate
 * Recipients
 * 
 * @author Madhusudan Parab
 */
public class GiftCertRecipBO {
	private static Logger logger = new Logger(
			"com.ftd.accountingreporting.bo.GiftCertRecipBO");
	private Connection conn;

	/**
	 * Contructor for class GccRecipientBO
	 * 
	 * @param connection
	 */

	public GiftCertRecipBO(Connection connection) {
		conn = connection;
	}

	/**
	 * This method handles the client requests depending upon the request made
	 * by the client.
	 * 
	 * @param request
	 * @return Document
	 * @throws java.lang.Exception
	 */
	public Document processRequest(HttpServletRequest request) throws Exception {
		Document responseDocument = null;
		try {
			String action = request.getParameter(ARConstants.COMMON_GCC_ACTION);

			if (action.equals(ARConstants.ACTION_PARM_DISPLAY_ADD)) {
				responseDocument = handleDisplayAdd(request);
			} else if (action.equals(ARConstants.ACTION_PARM_DISPLAY_EDIT)) {
				responseDocument = handleDisplayEdit(request);
			} else if (action.equals(ARConstants.ACTION_PARM_ADD)) {
				responseDocument = handleAdd(request);
			} else if (action.equals(ARConstants.ACTION_PARM_EDIT)) {
				responseDocument = handleEdit(request);
			} else if ((action.equals(ARConstants.ACTION_PARM_EXIT_EDIT))
					|| (action.equals(ARConstants.ACTION_PARM_EXIT_ADD))) {
				responseDocument = handleExit(request);
			}
		} catch (Exception e) {
			logger.error(e);
			throw e;
		}
		return responseDocument;
	}

	/**
	 * This method handles the display add functionality of gift certificate
	 * recipient
	 * 
	 * @param HttpServletRequest
	 *            request
	 * @return Document
	 * @throws java.lang.Exception
	 */
	private Document handleDisplayAdd(HttpServletRequest request)
			throws Exception {
		String requestNumber = request.getParameter("gcc_request_number");
		if (requestNumber != null && !"".equals(requestNumber)) {
			requestNumber = requestNumber.toUpperCase();
		}

		GccUtilDAO gccUtilDAO = new GccUtilDAO(conn);
		Document displayDocument = DOMUtil.getDefaultDocument();
		Element root = (Element) displayDocument
				.createElement(ARConstants.XML_ROOT);
		displayDocument.appendChild(root);

		// append field doc
		displayDocument.getDocumentElement().appendChild(
				displayDocument.importNode(createFieldDoc(request)
						.getDocumentElement(), true));

		// append empty recipient section
		Document rDoc = createEmptyRecipientDoc(Integer.parseInt(request
				.getParameter("gcc_quantity")));
		displayDocument.getDocumentElement().appendChild(
				displayDocument.importNode(rDoc.getDocumentElement(), true));

		// append country List Doc
		displayDocument.getDocumentElement().appendChild(
				displayDocument.importNode(gccUtilDAO.doGetCountryList()
						.getDocumentElement(), true));
		// append stateList Doc
		displayDocument.getDocumentElement().appendChild(
				displayDocument.importNode(gccUtilDAO.doGetStateList()
						.getDocumentElement(), true));

		root.setAttribute("sub_header_name", "Request Number #:");
		root.setAttribute("sub_header_value", requestNumber);

		logger.debug("Handle Display Add Completed");
		return displayDocument;
	}

	/**
	 * This method handles the display edit functionality of gift certificate
	 * recipient
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @return Document
	 * @throws java.lang.Exception
	 */
	private Document handleDisplayEdit(HttpServletRequest request)
			throws Exception {
		logger.debug("entering handleDisplayEdit...");
		logger.debug(request.getParameter("gcc_type"));

		String requestNumber = request.getParameter("gcc_request_number");
		if (requestNumber != null && !"".equals(requestNumber)) {
			requestNumber = requestNumber.toUpperCase();
		}
		String couponNumber = request.getParameter("gcc_coupon_number");
		if (couponNumber != null && !couponNumber.isEmpty()) {
			couponNumber = couponNumber.trim();
		} else {
			couponNumber = null;
		}
		String company = request.getParameter("gcc_company");
		if (null != company) {
			company = company.trim().toLowerCase();
		} else {
			company = ARConstants.FTD_COMPANY_ID;
		}
		
		
		GiftCodeUtil util = GiftCodeUtil.getInstance();
		String xmlResponse = util.searchGiftCertificate(requestNumber, couponNumber, company);
		Document rd = GiftCodeUtil.getXMLDocument(xmlResponse);
		
		
		GccUtilDAO gccUtilDAO = new GccUtilDAO(conn);
		Document displayDocument = DOMUtil.getDefaultDocument();
		Element root = (Element) displayDocument
				.createElement(ARConstants.XML_ROOT);
		displayDocument.appendChild(root);

		
		Element recipients = (Element) rd.getDocumentElement().getElementsByTagName("GC_COUPON_RECIPIENTS").item(0);
		NodeList recipientList = recipients.getElementsByTagName("GC_COUPON_RECIPIENT");
		for(int i=0; i< recipientList.getLength(); i++  ) {
			Node recipient = recipientList.item(i);
			((Element)recipient).setAttribute("num", new Integer(i+1).toString());
		}
		Document recipientData = getRecipientData(request);
		if (recipientData != null) {
			displayDocument.getDocumentElement().appendChild(
					displayDocument.importNode(
							recipients, true));
		}
		
		GiftCertBO.inspect(recipientData.getDocumentElement());
		logger.debug("------------");
		GiftCertBO.inspect(recipients);
		logger.debug("------------");
		
		// displayDocument.getDocumentElement().appendChild(displayDocument.importNode(getRecipientData(request).getDocumentElement(),
		// true));

		if (request.getParameter(ARConstants.ACTION_PARM_IS_MULTIPLE_GCC) != null
				&& request
						.getParameter(ARConstants.ACTION_PARM_IS_MULTIPLE_GCC)
						.equals(ARConstants.COMMON_VALUE_YES)) {
			root.setAttribute("sub_header_name", "Request Number #:");
			root.setAttribute("sub_header_value", requestNumber);
		} else {
			root.setAttribute("sub_header_name", "Gift Certificate/Coupon #:");
			root.setAttribute("sub_header_value",
					getCertificateNumber(request));
		}
		// append field doc
		displayDocument.getDocumentElement().appendChild(
				displayDocument.importNode(createFieldDoc(request)
						.getDocumentElement(), true));

		// append country List Doc
		displayDocument.getDocumentElement().appendChild(
				displayDocument.importNode(gccUtilDAO.doGetCountryList()
						.getDocumentElement(), true));
		// append stateList Doc
		displayDocument.getDocumentElement().appendChild(
				displayDocument.importNode(gccUtilDAO.doGetStateList()
						.getDocumentElement(), true));

		logger.debug("Handle Display Edit Completed");

		return displayDocument;
	}

	private Document getRecipientData(HttpServletRequest request)
			throws Exception {
		GccRecipientDAO gccRecipDAO = new GccRecipientDAO(conn);
		Document doc = null;
		int couQuantity = 1;
		String requestNumber = request.getParameter("gcc_request_number");
		if (requestNumber != null && !"".equals(requestNumber)) {
			requestNumber = requestNumber.toUpperCase();
		}

		if (ARConstants.COMMON_VALUE_YES.equals(request
				.getParameter(ARConstants.ACTION_PARM_IS_MULTIPLE_GCC))) {
			/* check if recipient data exists in the request */
			if (request.getParameter("gc_coupon_recip_id_1") == null) {
				/* check request for data */
				doc = gccRecipDAO.doGetCouponRecipient(requestNumber);
			} else {
				doc = buildRecipientDataFromRequest(request);
			}

			couQuantity = Integer
					.parseInt(request.getParameter("gcc_quantity"));
		} else {
			/* check if recipient data exists in the request */
			if (request.getParameter("gc_coupon_recip_id_1") == null) {
				/* check request for data */
				doc = gccRecipDAO.doGetRecipientByCouponNumber(request
						.getParameter("gcc_coupon_number"));
			} else {
				doc = buildRecipientDataFromRequest(request);
			}

		}

		// if(doc!=null){
		if (doc.getChildNodes().item(0).getChildNodes().getLength() == 0)
			doc = createEmptyRecipientDoc(couQuantity);
		// }
		return doc;
	}

	private Document buildRecipientDataFromRequest(HttpServletRequest request)
			throws Exception {
		Document recipientData = null;
		String recipientXML = "";
		boolean moreRecipients = false;
		recipientXML = "<GC_COUPON_RECIPIENTS>";
		if (request.getParameter("gc_coupon_recip_id_1") != null) {
			moreRecipients = true;
		}
		int recipientCt = 0;
		while (moreRecipients == true) {
			recipientCt++;
			if (request.getParameter("gc_coupon_recip_id_" + recipientCt) != null) {
				recipientXML = recipientXML + "<GC_COUPON_RECIPIENT num='"
						+ recipientCt + "'>";
				/* set coupon recipient ID */
				String recipID = request.getParameter("gc_coupon_recip_id_"
						+ recipientCt) != null ? request
						.getParameter("gc_coupon_recip_id_" + recipientCt) : "";
				if (!recipID.equals("")) {
					recipientXML = recipientXML + "<gc_coupon_recip_id>"
							+ DOMUtil.encodeChars(recipID)
							+ "</gc_coupon_recip_id>";
				} else {
					recipientXML = recipientXML + "<gc_coupon_recip_id/>";
				}
				/* set request number */
				String requestNumber = request
						.getParameter("gcc_request_number");
				if (requestNumber != null && !"".equals(requestNumber)) {
					requestNumber = requestNumber.toUpperCase();
				} else {
					requestNumber = "";
				}

				if (!requestNumber.equals("")) {
					recipientXML = recipientXML + "<request_number>"
							+ DOMUtil.encodeChars(requestNumber)
							+ "</request_number>";
				} else {
					recipientXML = recipientXML + "<request_number/>";
				}

				/* set coupon number */
				String couponNumber = request.getParameter("gcc_coupon_number") != null ? request
						.getParameter("gcc_coupon_number") : "";
				if (!couponNumber.equals("")) {
					recipientXML = recipientXML + "<gc_coupon_number>"
							+ DOMUtil.encodeChars(couponNumber)
							+ "</gc_coupon_number>";
				} else {
					recipientXML = recipientXML + "<gc_coupon_number/>";
				}

				/* set first name */
				String firstName = request.getParameter("first_name_"
						+ recipientCt) != null ? request
						.getParameter("first_name_" + recipientCt) : "";
				if (!firstName.equals("")) {
					recipientXML = recipientXML + "<first_name>"
							+ DOMUtil.encodeChars(firstName) + "</first_name>";
				} else {
					recipientXML = recipientXML + "<first_name/>";
				}

				/* set last name */
				String lastName = request.getParameter("last_name_"
						+ recipientCt) != null ? request
						.getParameter("last_name_" + recipientCt) : "";
				if (!lastName.equals("")) {
					recipientXML = recipientXML + "<last_name>"
							+ DOMUtil.encodeChars(lastName) + "</last_name>";
				} else {
					recipientXML = recipientXML + "<last_name/>";
				}

				/* set address 1 */
				String addressOne = request.getParameter("address_1_"
						+ recipientCt) != null ? request
						.getParameter("address_1_" + recipientCt) : "";
				if (!addressOne.equals("")) {
					recipientXML = recipientXML + "<address_1>"
							+ DOMUtil.encodeChars(addressOne) + "</address_1>";
				} else {
					recipientXML = recipientXML + "<address_1/>";
				}

				/* set address 2 */
				String addressTwo = request.getParameter("address_2_"
						+ recipientCt) != null ? request
						.getParameter("address_2_" + recipientCt) : "";
				if (!addressTwo.equals("")) {
					recipientXML = recipientXML + "<address_2>"
							+ DOMUtil.encodeChars(addressTwo) + "</address_2>";
				} else {
					recipientXML = recipientXML + "<address_2/>";
				}

				/* set city */
				String city = request.getParameter("city_" + recipientCt) != null ? request
						.getParameter("city_" + recipientCt) : "";
				if (!city.equals("")) {
					recipientXML = recipientXML + "<city>"
							+ DOMUtil.encodeChars(city) + "</city>";
				} else {
					recipientXML = recipientXML + "<city/>";
				}

				/* set state */
				String state = request.getParameter("state_" + recipientCt) != null ? request
						.getParameter("state_" + recipientCt) : "";
				if (!state.equals("")) {
					recipientXML = recipientXML + "<state>"
							+ DOMUtil.encodeChars(state) + "</state>";
				} else {
					recipientXML = recipientXML + "<state/>";
				}

				/* set zip code */
				String zipCode = request.getParameter("zip_" + recipientCt) != null ? request
						.getParameter("zip_" + recipientCt) : "";
				if (!zipCode.equals("")) {
					recipientXML = recipientXML + "<zip_code>"
							+ DOMUtil.encodeChars(zipCode) + "</zip_code>";
				} else {
					recipientXML = recipientXML + "<zip_code/>";
				}

				/* set country */
				String country = request.getParameter("country_" + recipientCt) != null ? request
						.getParameter("country_" + recipientCt) : "";
				if (!country.equals("")) {
					recipientXML = recipientXML + "<country>"
							+ DOMUtil.encodeChars(country) + "</country>";
				} else {
					recipientXML = recipientXML + "<country/>";
				}

				/* set phone */
				String phone = request.getParameter("phone_" + recipientCt) != null ? request
						.getParameter("phone_" + recipientCt) : "";
				if (!phone.equals("")) {
					recipientXML = recipientXML + "<phone>"
							+ DOMUtil.encodeChars(phone) + "</phone>";
				} else {
					recipientXML = recipientXML + "<phone/>";
				}

				/* set email address */
				String emailAddress = request.getParameter("email_"
						+ recipientCt) != null ? request.getParameter("email_"
						+ recipientCt) : "";
				if (!emailAddress.equals("")) {
					recipientXML = recipientXML + "<email_address>"
							+ DOMUtil.encodeChars(emailAddress)
							+ "</email_address>";
				} else {
					recipientXML = recipientXML + "<email_address/>";
				}

				recipientXML = recipientXML + "</GC_COUPON_RECIPIENT>";
			} else {
				moreRecipients = false;
			}
		}

		recipientXML = recipientXML + "</GC_COUPON_RECIPIENTS>";

		recipientData = DOMUtil.getDocument(recipientXML);

		return recipientData;
	}

	/**
	 * Creates empty recipient document.
	 * 
	 * @param quantity
	 * @return
	 * @throws java.lang.Exception
	 */
	private Document createEmptyRecipientDoc(int quantity) throws Exception {
		Document rDoc = null;
		ArrayList<GccRecipientVO> rList = new ArrayList<GccRecipientVO>();

		for (int i = 0; i < quantity; i++) {
			GccRecipientVO vo = new GccRecipientVO();
			rList.add(vo);
		}
		rDoc = getRecipientDocFromList(rList);
		return rDoc;
	}

	/**
	 * This method handles the add functionality of gift certificate recipient
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @return Document
	 * @throws java.lang.Exception
	 */
	private Document handleAdd(HttpServletRequest request) throws Exception {
		GiftCertificateCouponDAO gccDAO = new GiftCertificateCouponDAO(conn);
		GccUtilDAO gccUtilDAO = new GccUtilDAO(conn);
		Document displayDocument = DOMUtil.getDefaultDocument();
		Element root = (Element) displayDocument
				.createElement(ARConstants.XML_ROOT);
		displayDocument.appendChild(root);

		// insertRecipientsIntoSession(request);
		displayDocument = addRecipientSection(displayDocument, request);

		// append field doc
		displayDocument.getDocumentElement().appendChild(
				displayDocument.importNode(createFieldDoc(request)
						.getDocumentElement(), true));
		// append coupon types doc
		Document gccTypes = gccDAO.doGetGCCouponTypes();
		AccountingUtil acctUtil = new AccountingUtil();
		acctUtil.operationsCouponTypes(gccTypes, request);
		displayDocument.getDocumentElement()
				.appendChild(
						displayDocument.importNode(
								gccTypes.getDocumentElement(), true));
		// append company names doc
		displayDocument.getDocumentElement().appendChild(
				displayDocument.importNode(gccUtilDAO.doGetCompanyNames()
						.getDocumentElement(), true));
		// append partner programs
		displayDocument.getDocumentElement().appendChild(
				displayDocument.importNode(gccUtilDAO.doListPartnerPrograms()
						.getDocumentElement(), true));

		logger.debug("Handle Add Completed");
		return displayDocument;
	}

	/**
	 * This method handles the edit functionality of gift certificate recipient
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @return Document
	 * @throws java.lang.Exception
	 */
	private Document handleEdit(HttpServletRequest request) throws Exception {
		GiftCertificateCouponDAO gccDAO = new GiftCertificateCouponDAO(conn);
		GccUtilDAO gccUtilDAO = new GccUtilDAO(conn);
		Document displayDocument = DOMUtil.getDefaultDocument();
		Element root = (Element) displayDocument
				.createElement(ARConstants.XML_ROOT);
		displayDocument.appendChild(root);

		
		
		
		displayDocument = addRecipientSection(displayDocument, request);
		
		GiftCertBO.inspect(displayDocument.getDocumentElement());
		Element recipients = (Element) displayDocument.getDocumentElement().getElementsByTagName("GC_COUPON_RECIPIENTS").item(0);
		NodeList recipientList = recipients.getElementsByTagName("GC_COUPON_RECIPIENT");
		IssuedToRequestList irl = new IssuedToRequestList();
		for(int i=0; i< recipientList.getLength(); i++  ) {
			Node recipient = recipientList.item(i);
			UpdateIssuedToRequest ur = new UpdateIssuedToRequest();
			Element recipientElement = (Element)recipient;
			ur.setRecipientInfo(getRecipientInfo(recipientElement));
			irl.getUpdateRecipientInfo().add(ur);
			irl.setBatchNumber(recipientElement.getElementsByTagName("request_number").item(0).getFirstChild().getNodeValue());
		}
		GiftCodeUtil.updateRecipient(irl);
		
		String company = request.getParameter("gcc_company");
		if(null != company){
			company = company.trim().toLowerCase();
		}else{
			company = ARConstants.FTD_COMPANY_ID;
		}
		// append field doc
		displayDocument.getDocumentElement().appendChild(
				displayDocument.importNode(createFieldDoc(request)
						.getDocumentElement(), true));
		Document gccTypes = gccDAO.doGetGCCouponTypes();
		AccountingUtil acctUtil = new AccountingUtil();
		acctUtil.operationsCouponTypes(gccTypes, request);
		displayDocument.getDocumentElement()
				.appendChild(
						displayDocument.importNode(
								gccTypes.getDocumentElement(), true));
		displayDocument.getDocumentElement().appendChild(
				displayDocument.importNode(gccUtilDAO.doGetCompanyNames()
						.getDocumentElement(), true));
		displayDocument.getDocumentElement().appendChild(
				displayDocument.importNode(gccUtilDAO.doListPartnerPrograms()
						.getDocumentElement(), true));
		if (ARConstants.COMMON_VALUE_YES.equals(request
				.getParameter("is_multiple_gcc"))) {
			String requestNumber = request.getParameter("gcc_request_number");
			if (requestNumber != null && !"".equals(requestNumber)) {
				requestNumber = requestNumber.toUpperCase();
			}

			displayDocument.getDocumentElement().appendChild(
					displayDocument.importNode(
							new GiftCertBO(conn).getStatusList(request, "Y",
									request.getParameter("gcc_type"))
									.getDocumentElement(), true));
			getStatusCount(displayDocument, requestNumber);
			root.setAttribute("sub_header_name", "Request Number #:");
			root.setAttribute("sub_header_value", requestNumber);
		} else {
			displayDocument.getDocumentElement().appendChild(
					displayDocument.importNode(
							new GiftCertBO(conn).getStatusList(request, "N",
									request.getParameter("gcc_type"))
									.getDocumentElement(), true));
			displayDocument.getDocumentElement().appendChild(
					displayDocument.importNode(
							getRedemDateAndOrderDetailId(
									getCertificateNumber(request), company)
									.getDocumentElement(), true));
			root.setAttribute("sub_header_name", "Gift Certificate/Coupon #:");
			root.setAttribute("sub_header_value", getCertificateNumber(request));
		}

		logger.debug("Handle Edit Completed");
		return displayDocument;
	}

	private String getCertificateNumber(HttpServletRequest request) {
		String certId  = request.getParameter("gcc_coupon_number_1");
		if(certId == null || certId.isEmpty()) {
			certId  = request.getParameter("gcc_coupon_number");
		}
		return certId;
	}

	private RecipientInfo getRecipientInfo(Element recipientElement) {
		RecipientInfo rInfo = new RecipientInfo();
		GiftCertBO.inspect(recipientElement);
		rInfo.setFirstName(getValueFromDocument(recipientElement, "first_name"));
		rInfo.setLastName(getValueFromDocument(recipientElement, "last_name"));
		rInfo.setAddress1(getValueFromDocument(recipientElement, "address_1"));
		rInfo.setAddress2(getValueFromDocument(recipientElement, "address_2"));
		rInfo.setCity(getValueFromDocument(recipientElement, "city"));
		rInfo.setState(getValueFromDocument(recipientElement, "state"));
		rInfo.setZipcode(getValueFromDocument(recipientElement, "zip_code"));
		rInfo.setCountry(getValueFromDocument(recipientElement, "country"));
		rInfo.setPhone(getValueFromDocument(recipientElement, "phone"));
		rInfo.setGiftCodeId(getValueFromDocument(recipientElement, "gc_coupon_recip_id"));
		rInfo.setIssuedReferenceNumber(getValueFromDocument(recipientElement, "issuedReferenceNumber"));
		rInfo.setEmail(getValueFromDocument(recipientElement, "email_address"));
		return rInfo;
	}

	private String getValueFromDocument(Element recipientElement, String key) {
		String retVal = "";
		NodeList nodes = recipientElement.getElementsByTagName(key);
		if(nodes !=null && nodes.getLength() > 0) {
			retVal = nodes.item(0).getFirstChild().getNodeValue();
		} else {
			logger.warn("No nodes found for key : " + key);
		}
		return retVal;
	}

	/**
	 * This method handles the exit functionality of gift certificate recipient
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @return Document
	 * @throws java.lang.Exception
	 */
	private Document handleExit(HttpServletRequest request) throws Exception {
		logger.debug("Entering handleExit...");
		String isMultipleGcc = request.getParameter("is_multiple_gcc");
		String requestNumber = request.getParameter("gcc_request_number");
		if (requestNumber != null && !"".equals(requestNumber)) {
			requestNumber = requestNumber.toUpperCase();
		}
		String couponNumber = request.getParameter("gcc_coupon_number");
		String type = request.getParameter("gcc_type");
		String company = request.getParameter("gcc_company");
		if(null != company){
			company = company.trim().toLowerCase();
		}else{
			company = ARConstants.FTD_COMPANY_ID;
		}

		logger.debug("isMultipleGcc is " + isMultipleGcc);
		logger.debug("requestNumber is " + requestNumber);
		logger.debug("couponNUmber is " + couponNumber);
		logger.debug("Type is " + type);

		GiftCertificateCouponDAO gccDAO = new GiftCertificateCouponDAO(conn);
		GccUtilDAO gccUtilDAO = new GccUtilDAO(conn);

		Document displayDocument = DOMUtil.getDefaultDocument();
		Element root = (Element) displayDocument
				.createElement(ARConstants.XML_ROOT);
		displayDocument.appendChild(root);

		// append field doc
		displayDocument.getDocumentElement().appendChild(
				displayDocument.importNode(createFieldDoc(request)
						.getDocumentElement(), true));
		// append coupontypes doc
		Document gccTypes = gccDAO.doGetGCCouponTypes();
		AccountingUtil acctUtil = new AccountingUtil();
		acctUtil.operationsCouponTypes(gccTypes, request);
		displayDocument.getDocumentElement()
				.appendChild(
						displayDocument.importNode(
								gccTypes.getDocumentElement(), true));
		// append company names doc
		displayDocument.getDocumentElement().appendChild(
				displayDocument.importNode(gccUtilDAO.doGetCompanyNames()
						.getDocumentElement(), true));

		// append partner programs
		displayDocument.getDocumentElement().appendChild(
				displayDocument.importNode(gccUtilDAO.doListPartnerPrograms()
						.getDocumentElement(), true));

		if (ARConstants.COMMON_VALUE_YES.equals(isMultipleGcc)) {
			displayDocument.getDocumentElement().appendChild(
					displayDocument.importNode(
							new GiftCertBO(conn).getStatusList(request, "Y",
									request.getParameter("gcc_type"))
									.getDocumentElement(), true));
			getStatusCount(displayDocument, requestNumber);
			root.setAttribute("sub_header_name", "Request Number #:");
			root.setAttribute("sub_header_value", requestNumber);
		} else if (ARConstants.COMMON_VALUE_NO.equals(isMultipleGcc)) {
			displayDocument.getDocumentElement().appendChild(
					displayDocument.importNode(
							new GiftCertBO(conn).getStatusList(request, "N",
									request.getParameter("gcc_type"))
									.getDocumentElement(), true));
			displayDocument.getDocumentElement().appendChild(
					displayDocument.importNode(
							getRedemDateAndOrderDetailId(couponNumber, company)
									.getDocumentElement(), true));
			root.setAttribute("sub_header_name", "Gift Certificate/Coupon #:");
			root.setAttribute("sub_header_value", couponNumber);
		}

		logger.debug("Handle Exit Completed");
		return displayDocument;
	}

	/**
	 * Retrieves recipient from request and adds to the document.
	 * 
	 * @param doc
	 * @param request
	 * @return
	 * @throws java.lang.Exception
	 */
	private Document addRecipientSection(Document doc,
			HttpServletRequest request) throws Exception {
		Element root = (Element) (doc
				.getElementsByTagName(ARConstants.XML_ROOT)).item(0);

		Document rDoc = getRecipientDocFromRequest(request);

		root.appendChild(doc.importNode(
				(rDoc.getElementsByTagName("GC_COUPON_RECIPIENTS")).item(0),
				true));

		return doc;
	}

	private Document createFieldDoc(HttpServletRequest request)
			throws Exception {
		GiftCertActionHelper actionHelper = new GiftCertActionHelper();
		Document fieldDoc = actionHelper.createFieldDoc(request);
		String company = request.getParameter("gcc_company");
		if(null != company){
			company = company.trim().toLowerCase();
		}
		Element fields = (Element) (fieldDoc
				.getElementsByTagName(ARConstants.XML_FIELD_TOP)).item(0);

		if (request.getParameter(ARConstants.ACTION_PARM_IS_MULTIPLE_GCC) != null
				&& request
						.getParameter(ARConstants.ACTION_PARM_IS_MULTIPLE_GCC)
						.equals(ARConstants.COMMON_VALUE_NO)) {
			fields.appendChild(actionHelper.getNameElement(fieldDoc,
					"gcc_status_single_display",
					getStatusSingle(request.getParameter("gcc_coupon_number"), company)));
		}
		if (request.getParameter(ARConstants.COMMON_GCC_ACTION).equals(
				ARConstants.ACTION_PARM_ADD))
			fields.appendChild(actionHelper.getNameElement(fieldDoc,
					"gcc_enable_create_cs", "Added"));
		else
			fields.appendChild(actionHelper.getNameElement(fieldDoc,
					"gcc_enable_create_cs", "Not Added"));

		return fieldDoc;
	}

	private Document getRedemDateAndOrderDetailId(String couNumber, String company)
			throws Exception {
		GiftCodeUtil util = GiftCodeUtil.getInstance();
		
		String xmlStringResp = util.searchGiftCertificate(null, couNumber, company);
		// GiftCertificateCouponDAO gccDAO = new GiftCertificateCouponDAO(conn);
		String redemptionDates = "";
		String orderIds = "";
		// Document couDoc = gccDAO.doGetHistoryByCouponNum(couNumber);
		Document couDoc = GiftCodeUtil.getXMLDocument(xmlStringResp);
		// parses to get redemption dates
		NodeList redemptionList = couDoc
				.getElementsByTagName("redemption_date");
		Node redemptionNode = redemptionList.item(0);
		if (null != redemptionNode) {
			NodeList redeemDateNodeList = redemptionNode.getChildNodes();
			for (int count = 0; count < redeemDateNodeList.getLength(); count++) {
				String value = "";
				Node redemptionInnerNode = redeemDateNodeList.item(count);
				if (redemptionInnerNode.getChildNodes().getLength() > 0) {
					value = redemptionInnerNode.getChildNodes().item(0)
							.getNodeValue().trim();
					if (redemptionDates.equals("")) {
						redemptionDates = redemptionDates + value;
					} else {
						redemptionDates = redemptionDates + ", " + value;
					}
				}
			}
		}

		// parses to get order detail ids
		NodeList orderList = couDoc.getElementsByTagName("order_detail_id");
		Node orderNode = orderList.item(0);
		if (null != orderNode) {
			NodeList textNodeList = orderNode.getChildNodes();
			for (int s = 0; s < textNodeList.getLength(); s++) {
				String value = "";
				Node childOrderNode = textNodeList.item(s);
				if (childOrderNode.getChildNodes().getLength() > 0) {
					value = childOrderNode.getChildNodes().item(0)
							.getNodeValue().trim();
					if (orderIds.equals("")) {
						orderIds = orderIds + value;
					} else {
						orderIds = orderIds + ", " + value;
					}
				}
			}
		}

		// creates a Document with above paresd values
		Document fieldDoc = DOMUtil.getDefaultDocument();
		Element fields = (Element) fieldDoc.createElement("GC_COUPON_HISTORYS");
		fieldDoc.appendChild(fields);

		Element field = (Element) fieldDoc.createElement("GC_COUPON_HISTORY");
		fields.appendChild(field);

		Element redemptionDate = (Element) fieldDoc
				.createElement("redemption_date");
		field.appendChild(redemptionDate);
		Text textNodeR = fieldDoc.createTextNode(redemptionDates);
		redemptionDate.appendChild(textNodeR);

		Element orderId = (Element) fieldDoc.createElement("order_detail_id");
		field.appendChild(orderId);
		Text textNodeO = fieldDoc.createTextNode(orderIds);
		orderId.appendChild(textNodeO);

		return fieldDoc;
	}

	/*
	 * private String formatTimestampToDate(String inTimestamp) throws Exception
	 * { if (inTimestamp != null && !inTimestamp.equals("")) { SimpleDateFormat
	 * df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S"); Calendar cal =
	 * Calendar.getInstance(); cal.setTime(df.parse(inTimestamp));
	 * 
	 * df = new SimpleDateFormat("MM/dd/yyyy");
	 * 
	 * return df.format(cal.getTime()); } return ""; }
	 */

	public Document getStatusCount(Document displayDocument,
			String requestNumber) throws Exception {
		GiftCertificateCouponDAO gccDAO = new GiftCertificateCouponDAO(conn);
		Document gccCount = gccDAO.doGetStatusCount(requestNumber);
		displayDocument.getDocumentElement()
				.appendChild(
						displayDocument.importNode(
								gccCount.getDocumentElement(), true));
		displayDocument.getDocumentElement().setAttribute(
				"status_multiple_count",
				getIssuedActiveReinstateCount(gccCount));
		return displayDocument;
	}

	private String getIssuedActiveReinstateCount(Document gccDoc)
			throws Exception {
		int statusMultipleCount = 0;
		NodeList listOfCoupons = gccDoc.getElementsByTagName("GC_COUPON");
		for (int s = 0; s < listOfCoupons.getLength(); s++) {
			Node firstCouponNode = listOfCoupons.item(s);
			if (firstCouponNode.getNodeType() == Node.ELEMENT_NODE) {
				Element firstCouponElement = (Element) firstCouponNode;
				int count = new Integer(getStringValueFromXML(
						firstCouponElement, "status_count")).intValue();
				String status = getStringValueFromXML(firstCouponElement,
						"gc_coupon_status");
				if (status.equals("Issued Active")
						|| status.equals("Reinstate"))
					statusMultipleCount = statusMultipleCount + count;
			}
		}
		return new Integer(statusMultipleCount).toString();
	}

	/**
	 * This method gets the value of the tagname passed for that element
	 * 
	 * @param parentElement
	 *            Element
	 * @param tagName
	 *            String
	 * @return String
	 * @throws java.lang.Exception
	 */
	private String getStringValueFromXML(Element parentElement, String tagName)
			throws Exception {
		NodeList nodeList = parentElement.getElementsByTagName(tagName);
		if (nodeList.getLength() == 0)
			return null;
		Element nodeElement = (Element) nodeList.item(0);
		NodeList textNodeList = nodeElement.getChildNodes();
		if (textNodeList.getLength() == 0)
			return null;
		return ((Node) textNodeList.item(0)).getNodeValue().trim();
	}

	private String getStatusSingle(String couponNumber, String company) throws Exception {
		logger.debug("Entering getStatusSingle...");
		GiftCodeUtil util = GiftCodeUtil.getInstance();
		String value = "";
		// Document statusDoc = gccDAO.doSearchGcCoupon(null, couponNumber);
		String xmlresponse = util.getSingleStatusGiftCertificate(couponNumber, company);
		logger.debug("Status Service Response : " + xmlresponse);
		Document statusDoc = GiftCodeUtil.getXMLDocument(xmlresponse);
		// parses to get status
		NodeList statusList = statusDoc
				.getElementsByTagName("gc_coupon_status");
		for (int s = 0; s < statusList.getLength(); s++) {
			Element statusNode = (Element) statusList.item(s);
			NodeList textNodeList = statusNode.getChildNodes();
			if (textNodeList.getLength() != 0)
				value = ((Node) textNodeList.item(0)).getNodeValue().trim();
		}
		return value;
	}

	/**
	 * Retrieve a recipeint document from the request
	 * 
	 * @param request
	 * @return
	 * @throws java.lang.Exception
	 */

	public Document getRecipientDocFromRequest(HttpServletRequest request)
			throws Exception {
		ArrayList<GccRecipientVO> rList = getRecipientListFromRequest(request);
		
		if (rList != null && rList.size() > 0) {
			return getRecipientDocFromList(rList);
		}

		return null;
	}

	/**
	 * Returns a list of RecipientVO from the request.
	 * 
	 * @param request
	 * @return
	 * @throws java.lang.Exception
	 */
	public ArrayList<GccRecipientVO> getRecipientListFromRequest(
			HttpServletRequest request) throws Exception {
		ArrayList<GccRecipientVO> rArray = new ArrayList<GccRecipientVO>();
		// String couponQuantity = request.getParameter("coupon_quantity");
		String couponQuantity = request.getParameter("gcc_quantity");
		// String added = request.getParameter("gcc_enable_create_cs");
		String isMultipleGcc = request.getParameter("is_multiple_gcc");
		int q = Integer.parseInt(couponQuantity);
		logger.debug("gcc_quantity is " + couponQuantity);
		logger.debug("isMultipleGcc is " + isMultipleGcc);
		String requestNumber = request.getParameter("gcc_request_number");
		if (requestNumber != null && !"".equals(requestNumber)) {
			requestNumber = requestNumber.toUpperCase();
		}

		if ("N".equals(isMultipleGcc)) {
			// Edit single recipient
			q = 1;
		} // else: Edit multiple or add
		for (int i = 0; i < q; i++) {
			int seq = i + 1;
			GccRecipientVO recipVO = new GccRecipientVO();
			if (requestNumber != null && requestNumber.length() != 0) {
				recipVO.setRequestNumber(requestNumber);
			}
//			if (request.getParameter("gc_coupon_recip_id_" + seq) != null
//					&& request.getParameter("gc_coupon_recip_id_" + seq)
//							.length() != 0)
//				recipVO.setGcCouponReceiptId(new Long(request
//						.getParameter("gc_coupon_recip_id_" + seq)).longValue());
			recipVO.setGiftCertificateId(request.getParameter("gc_coupon_recip_id_" + seq));
			recipVO.setFirstName(request.getParameter("first_name_" + seq));
			recipVO.setLastName(request.getParameter("last_name_" + seq));
			recipVO.setAddress1(request.getParameter("address_1_" + seq));
			recipVO.setAddress2(request.getParameter("address_2_" + seq));
			recipVO.setCity(request.getParameter("city_" + seq));
			recipVO.setState(request.getParameter("state_" + seq));
			recipVO.setZipCode(request.getParameter("zip_" + seq));
			recipVO.setCountry(request.getParameter("country_" + seq));
			recipVO.setPhone(request.getParameter("phone_" + seq));
			recipVO.setEmailAddress(request.getParameter("email_" + seq));
			recipVO.setIssuedReferenceNumber(request
					.getParameter("issuedReferenceNumber_" + seq));
			if (recipVO.isNullObject()) {
				break;
			}
			rArray.add(recipVO);

		}
		return rArray;
	}

	/**
	 * Creates a Document from the list of recipients. Add an attribute "num" on
	 * secondary node.
	 * 
	 * @param rList
	 * @return Document
	 * @throws java.lang.Exception
	 */
	public Document getRecipientDocFromList(ArrayList<GccRecipientVO> rList)
			throws Exception {
		Document rDoc = null;
		Element fields = null;
		Element field = null;
		GccRecipientVO rvo = null;
		Document rxml = null;

		if (rList != null && rList.size() > 0) {
			rDoc = DOMUtil.getDefaultDocument();
			fields = (Element) rDoc.createElement("GC_COUPON_RECIPIENTS");
			rDoc.appendChild(fields);

			for (int i = 0; i < rList.size(); i++) {
				rvo = (GccRecipientVO) rList.get(i);
				rxml = rvo.toXML();
				field = rxml.getDocumentElement();

				// set the num attibute sequence, starting at 1.
				field.setAttribute("num", String.valueOf(i + 1));
				fields.appendChild(rDoc.importNode(field, true));
			}
		}
		return rDoc;

	}

}
