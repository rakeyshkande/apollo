package com.ftd.accountingreporting.bo;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.dao.GiftCertificateCouponDAO;
import com.ftd.accountingreporting.util.EODUtil;
import com.ftd.osp.utilities.GiftCodeUtil;
import com.ftd.osp.utilities.RESTUtils;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.vo.RedemptionDetails;
import com.ftd.osp.utilities.vo.RetrieveGiftCodeResponse;
import com.ftd.osp.utilities.vo.UpdateGiftCodeRequest;

public class GiftCodeMaintenanceBO {
	private Logger logger = 
        new Logger("com.ftd.accountingreporting.bo.GiftCodeMaintenanceBO");
	

	
    /**
     * This method will attempt to redeem a gift code.  If the gift code is already redeemed
     * it will send a system message.
     * @param UpdateGiftCodeRequest updateGiftCodeReq
     * @throws Exception
     */
    public boolean processRedemption(String giftCodeId, String orderDetailId, String updatedBy, BigDecimal orderAmount, int delayCount) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering processRedemption");
        }
        
        BigDecimal totalRedemptionAmount = new BigDecimal(0);
        BigDecimal totalAmountAlreadyRedeemed = new BigDecimal(0);
        List lRedemptionDetails = new ArrayList();
        RedemptionDetails redemptionDetails = null;
        Connection conn = null;
        boolean gcRedeemed = false;
        
        try{
        	
       		conn = EODUtil.createDatabaseConnection();
       		GiftCertificateCouponDAO gccDAO = new GiftCertificateCouponDAO(conn);
       		
       		//get shopping cart count
        	int cartCount = gccDAO.getCartCount(giftCodeId, orderDetailId);
        	logger.info("orderAmount: " + orderAmount);
	   		logger.info("cartCount: " + cartCount);
	   		if(cartCount > 1 && delayCount > 1) {
	   		  for (int i = 0; i < delayCount; i++)
              {
	   			logger.info("Adding a delay before redeeming gift card to avoid redemption issues ");
				int processDelay = 1 + (int) (Math.random() * (((100000 - 10000) / 5) + 1));
				Thread.currentThread().sleep(Long.valueOf(processDelay));
				//do it again just to make sure the delay is long enough
				processDelay = 1 + (int) (Math.random() * (((100000 - 10000) / 5) + 1));
				Thread.currentThread().sleep(Long.valueOf(processDelay));
				logger.info("Resuming processing....");	
              }
	   		}
	   	    RetrieveGiftCodeResponse retrieveGiftCodeResponse = GiftCodeUtil.getGiftCodeById(giftCodeId);
			if (null != retrieveGiftCodeResponse) {
			
				BigDecimal remainingAmount = retrieveGiftCodeResponse
						.getRemainingAmount();

				//check remaining balance.  If it equals $0, do not redeem
				if(!remainingAmount.equals(BigDecimal.ZERO)){	

					BigDecimal redemptionAmount = new BigDecimal(0);
					String status = retrieveGiftCodeResponse.getStatus();
					List<RedemptionDetails> lRedDetails = new ArrayList();
					lRedDetails = retrieveGiftCodeResponse.getRedemptionDetails();
					if (lRedDetails == null || lRedDetails.size() == 0) {
						// do nothing
						logger.info("no redemption details");
					} else {
						// loop through the redemption details and retrieve how much
						// has been redeemed already
						// add together
						// add to remaining amount if that's > 0
						for (RedemptionDetails redemptDetails : lRedDetails) {
							redemptionAmount = redemptDetails.getRedemptionAmount();
							totalAmountAlreadyRedeemed = totalAmountAlreadyRedeemed
									.add(redemptionAmount);
							// need to re-add these to new redemption request
							redemptionDetails = new RedemptionDetails();
							redemptionDetails.setRedeemedRefNo(redemptDetails
									.getRedeemedRefNo());
							logger.info("redemptDetails.getRedeemedRefNo(): "
									+ redemptDetails.getRedeemedRefNo());
							redemptionDetails.setRedemptionAmount(redemptDetails
									.getRedemptionAmount());
							logger.info("redemptDetails.getRedemptionAmount(): "
									+ redemptDetails.getRedemptionAmount());
							redemptionDetails.setRedemptionDate(redemptDetails
									.getRedemptionDate());
							logger.info("redemptDetails.getRedemptionDate(): "
									+ redemptDetails.getRedemptionDate());
							lRedemptionDetails.add(redemptionDetails);
						}
	
					}
	
					totalRedemptionAmount = totalRedemptionAmount
							.add(totalAmountAlreadyRedeemed);
					// check if remaining amount is less than the order amount
					// if it is, then set remaining amount as the redemption amount
					// otherwise set the order amount as redemption amount
					if (remainingAmount.compareTo(orderAmount) == -1) {
						redemptionAmount = remainingAmount;
						totalRedemptionAmount = totalRedemptionAmount
								.add(redemptionAmount);
					} else {
						redemptionAmount = orderAmount;
						totalRedemptionAmount = totalRedemptionAmount
								.add(redemptionAmount);
					}
					UpdateGiftCodeRequest updateGiftCodeReq = new UpdateGiftCodeRequest();
					updateGiftCodeReq.setGiftCodeId(giftCodeId.trim());
					updateGiftCodeReq.setStatus(status);
					updateGiftCodeReq
							.setTotalRedemptionAmount(totalRedemptionAmount);
					updateGiftCodeReq.setUpdatedBy(updatedBy);
					redemptionDetails = new RedemptionDetails();
					redemptionDetails.setRedeemedRefNo(orderDetailId);
					redemptionDetails.setRedemptionAmount(redemptionAmount);
					lRedemptionDetails.add(redemptionDetails);
					updateGiftCodeReq.setRedemptionDetails(lRedemptionDetails);
	
					// Retrieve the Gift Code status.
					// If the status equals Reinstate or Issued Active or
					// Redeemed without any redemption details, then call Gift Code Service to Redeem
					// the Gift Code.
					// If the Gift Code is expired or cancelled, or the Gift Code Id is not
					// found then set status = Invalid and return the status
					if (updateGiftCodeReq.getStatus().equalsIgnoreCase("Reinstate")
							|| updateGiftCodeReq.getStatus().equalsIgnoreCase("Issued Active")
							|| updateGiftCodeReq.getStatus().equalsIgnoreCase("Redeemed")) {
						updateGiftCodeReq.setStatus("Redeemed");
						String redeemStatus = GiftCodeUtil.adminUpdateStatus(updateGiftCodeReq);
	
						retrieveGiftCodeResponse = GiftCodeUtil.getGiftCodeById(giftCodeId);
						remainingAmount = retrieveGiftCodeResponse.getRemainingAmount();
						redemptionAmount = new BigDecimal(0);
	
						logger.info("redeemStatus: " + redeemStatus);
						if (!redeemStatus.equalsIgnoreCase("Redeemed")) {
							logger.info("Gift Code Id " + giftCodeId + " is not redeemable.  Sending Java Support to investigate." + retrieveGiftCodeResponse.getStatus());
							sendSystemMessage("Error occurred while trying to redeem gift code id " + giftCodeId + " with status = " +  retrieveGiftCodeResponse.getStatus() +
									".  Please investigate.  Send to Production Support to send to Customer Service to contact customer if necessary.");					
						}
						else{
							gcRedeemed = true;
						}
					}
				  }
			}
        }catch(Exception e){
        	logger.error("Error occured while trying to redeem gift code", e);
        }
        finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting processRedemption()");
            }
            RESTUtils.closeConnection(conn);
        } 
        return gcRedeemed;
    }
    
      
      /**
       * This method sends a message to the System Messenger.
       *
       * @param String message
       * @throws Exception 
       * @returns String message id
       */
      public void sendSystemMessage(String logMessage) throws Exception
      {
  		Connection conn = null;

  		conn = EODUtil.createDatabaseConnection();
    	 
    	 
      	try
      	{
          //build system vo
          // SystemMessage closes connection passed in. Therefore, create a new connection.
      	  
          logger.debug(logMessage);
          SystemMessengerVO sysMessage = new SystemMessengerVO();
          sysMessage.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
          sysMessage.setSource(ARConstants.GIFT_CODE_MAINTENANCE_PROCESS);
          sysMessage.setType(ARConstants.GIFT_CODE_MAINTENANCE_PROCESS_ERROR_TYPE);
          sysMessage.setMessage(logMessage);
          SystemMessenger.getInstance().send(sysMessage, conn, false);
      	}
      	catch (Exception e)
      	{
      		logger.error("Sending system message failed. Message=" + logMessage);
      		logger.error(e);
      	}finally {
			closeConnection(conn);
		}
      }
      
      /**
  	 * Close connection.
  	 * 
  	 * @param connection
  	 */
  	public void closeConnection(Connection connection) {
  		try {
  			if (connection != null && !connection.isClosed()) {
  				connection.close();
  			}
  		} catch (Exception e) {
  			logger.error("Unable to close the connection." + e.getMessage());
  		}
  	}

}

