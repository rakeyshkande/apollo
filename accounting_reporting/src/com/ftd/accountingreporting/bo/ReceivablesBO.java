package com.ftd.accountingreporting.bo;

import com.ftd.accountingreporting.constant.ARConstants;
import java.io.OutputStream;
import org.apache.commons.lang.StringUtils;

import com.ftd.accountingreporting.dao.ReceivablesDAO;
import com.ftd.accountingreporting.util.AccountingUtil;
import com.ftd.accountingreporting.util.XMLUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.accountingreporting.util.LockUtil;
import com.ftd.accountingreporting.exception.EntityLockedException;
import com.ftd.osp.utilities.ConfigurationUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.InputStream;
import java.io.IOException;
import java.util.List;
import org.apache.commons.fileupload.FileItem;

import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import javax.xml.xpath.XPath;


import java.sql.Connection;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpServletResponse;

import javax.xml.xpath.XPathConstants;

import org.w3c.dom.Document;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;

import org.apache.poi.hssf.usermodel.HSSFFont;

import org.xml.sax.InputSource;


public class ReceivablesBO
{

  /*******************************************************************************************
 * Class level variables
 *******************************************************************************************/
  private static Logger logger = new Logger("com.ftd.accountingreporting.bo.ReceivablesBO");

  private HttpServletRequest request = null;
  private HttpServletResponse response = null;
  private Connection con = null;

  private String  actionType = null;
  private String  context = null;
  private String  csrId = null;
  private long    currentPagePosition = 0;
  private String  forwardToName = null;
  private String  forwardToType = null;
  private HashMap hashXML = new HashMap();
  private String  ignoreError = "N";
  private boolean lockValid = true;
  private long    maxRecordsPerPage = 0;
  private String  newOriginSelected = null;
  private long    newPagePosition = 0;
  private String  newPartnerSelected = null;
  private String  newSortColumn = null;
  private String  newSortDirection = null;
  private String  origOriginSelected = null;
  private String  origPartnerSelected = null;
  private String  origSortColumn = null;
  private String  origSortDirection = null;
  private HashMap pageData = new HashMap();
  private long    recordsDisplayedOnPage = 0;
  private long    recordsToBeReturned = 0;
  private String  remitNumber = null;
  private long    remitDetailsUpdateCount = 0;
  private String  sessionId = null;
  private long    startSearchingFrom = 0;
  private long    totalPages = 0;


  /*******************************************************************************************
 * Constructor 1
 *******************************************************************************************/
  public ReceivablesBO()
  {
  }


  /*******************************************************************************************
 * Constructor 2
 *******************************************************************************************/
  public ReceivablesBO(HttpServletRequest request, Connection con, HttpServletResponse response)
  {
    this.request = request;
    this.response = response;
    this.con = con;
  }


  /*******************************************************************************************
  * processRequest(action)
  ******************************************************************************************
  * Besides the constructor, this is the only public method in this class.
  * This method will accept a string called action and after interogating the value, will
  * process accordingly.  It will generate the search criteria, the page details, and the
  * valid data to be passed back to the calling class in a HashMap.
  * Note that the search criteria and page details are returned back as HashMap objects,
  * while all other data is returned as XML objects.
  *
  * @param  String - action
  * @return HashMap - contains 0 - many XML documents of data, 1 HashMap each for page
  *                   details and search criteria
  * @throws
  */
  public HashMap processRequest()
    throws Exception
  {
    //HashMap that will be returned to the calling class
    HashMap returnHash = new HashMap();

    //Get info from the request object
    getRequestInfo();

    //Get config file info
    getConfigInfo();

    //set the defaults
    setDefaults();

    //validate lock
    if (this.newPartnerSelected != null && !this.newPartnerSelected.equalsIgnoreCase(""))
      validateLock();

    //process the action
    processAction();

    //populate the remainder of the fields on the page data
    populatePageData();


    //At this point, we should have two HashMaps.
    // HashMap1 = hashXML         - hashmap that contains zero to many Documents
    // HashMap2 = pageData        - hashmap that contains String data at page level
    //
    //Combine all of the above HashMaps 3 into one HashMap, called returnHash

    //retrieve all the Documents from hashXML and put them in returnHash
    //retrieve the keyset
    Set ks1 = hashXML.keySet();
    Iterator iter = ks1.iterator();
    String key = null;
    Document doc = null;

    //Iterate thru the keyset
    while (iter.hasNext())
    {
      //get the key
      key = iter.next().toString();

      //retrieve the XML document
      doc = (Document) hashXML.get(key);

      //put the XML object in the returnHash
      returnHash.put(key, (Document) doc);
    }

    //append pageData to returnHash
    returnHash.put("pageData", (HashMap) this.pageData);

    return returnHash;
  }


  /*******************************************************************************************
  * getRequestInfo(HttpServletRequest request)
  ******************************************************************************************
  * Retrieve the info from the request object
  *
  * @param  HttpServletRequest - request
  * @return none
  * @throws none
  */
  private void getRequestInfo()
    throws Exception
  {
    //****retrieve the action type
    if (request.getParameter("action_type") != null)
      this.actionType = this.request.getParameter("action_type");

    //****retrieve the context
    if (request.getParameter("context") != null)
      this.context = this.request.getParameter("context");

    //****retrieve the current page position
    if (request.getParameter("current_page") != null)
    {
      if (!request.getParameter("current_page").toString().equalsIgnoreCase(""))
      {
        this.currentPagePosition = Long.valueOf(request.getParameter("current_page")).longValue();
      }
    }

    //****retrieve the current page position
    if (request.getParameter("ignore_error") != null)
    {
      if (!request.getParameter("ignore_error").toString().equalsIgnoreCase(""))
      {
        this.ignoreError = request.getParameter("ignore_error");
      }
    }

    //****retrieve the new origin id
    if (request.getParameter("new_origin_selected") != null)
      this.newOriginSelected = this.request.getParameter("new_origin_selected");

    //****retrieve the new partner id
    if (request.getParameter("new_partner_selected") != null)
      this.newPartnerSelected = this.request.getParameter("new_partner_selected");

    //****retrieve the new_sort_column
    if (request.getParameter("new_sort_column") != null)
      this.newSortColumn = this.request.getParameter("new_sort_column");

    //****retrieve the new_sort_direction
    if (request.getParameter("new_sort_direction") != null)
      this.newSortDirection = this.request.getParameter("new_sort_direction");

    //****retrieve the original origin id
    if (request.getParameter("orig_origin_selected") != null)
      this.origOriginSelected = this.request.getParameter("orig_origin_selected");

    //****retrieve the original partner id
    if (request.getParameter("orig_partner_selected") != null)
      this.origPartnerSelected = this.request.getParameter("orig_partner_selected");

    //****retrieve the orig_sort_column
    if (request.getParameter("orig_sort_column") != null)
      this.origSortColumn = this.request.getParameter("orig_sort_column");

    //****retrieve the orig_sort_direction
    if (request.getParameter("orig_sort_direction") != null)
      this.origSortDirection = this.request.getParameter("orig_sort_direction");

    //****retrieve the records displayed
    if (request.getParameter("records_displayed_on_page") != null)
    {
      if (!request.getParameter("records_displayed_on_page").toString().equalsIgnoreCase(""))
      {
        this.recordsDisplayedOnPage = Long.valueOf(request.getParameter("records_displayed_on_page")).longValue();
      }
    }

    //****retrieve the records displayed
    if (request.getParameter("remit_details_update_count") != null)
    {
      if (!request.getParameter("remit_details_update_count").toString().equalsIgnoreCase(""))
      {
        this.remitDetailsUpdateCount = Long.valueOf(request.getParameter("remit_details_update_count")).longValue();
      }
    }

    //****retrieve the remit_number_text
    if (request.getParameter("remit_number_text") != null)
      this.remitNumber = this.request.getParameter("remit_number_text");

    //****retrieve the security token
    if (request.getParameter("securitytoken") != null)
      this.sessionId = this.request.getParameter("securitytoken");

    //****retrieve the total number of pages that was initially sent to the page
    if (request.getParameter("total_pages") != null)
    {
      if (!request.getParameter("total_pages").toString().equalsIgnoreCase(""))
      {
        this.totalPages = Long.valueOf(request.getParameter("total_pages")).longValue();
      }
    }


  }


  /*******************************************************************************************
  * getConfigInfo()
  ******************************************************************************************
  * Retrieve the info from the configuration file
  *
  * @param none
  * @return
  * @throws none
  */
  private void getConfigInfo()
    throws Exception
  {

    AccountingUtil acctUtil = new AccountingUtil();
    this.csrId = acctUtil.getCsrID(this.sessionId);

    String recordsPerPage = ConfigurationUtil.getInstance().getFrpGlobalParm(
                    ARConstants.GLOBAL_OR_CONTEXT, ARConstants.GLOBAL_OR_RECORDS_PER_PAGE);

    if (StringUtils.isNotEmpty(recordsPerPage))
      this.maxRecordsPerPage = Long.valueOf(recordsPerPage).longValue();
    else
      this.maxRecordsPerPage = Long.valueOf(ARConstants.DEFAULT_RECORDS_PER_PAGE).longValue();

  }


  /*******************************************************************************************
  * setDefaults()
  ******************************************************************************************
  * Set the defaults
  *
  * @param none
  * @return
  * @throws none
  */
  private void setDefaults()
  {
    if (this.newSortColumn == null || this.newSortColumn.equalsIgnoreCase(""))
    {
      if (this.origSortColumn != null && !this.origSortColumn.equalsIgnoreCase(""))
        this.newSortColumn = this.origSortColumn;
      else
        this.newSortColumn = ARConstants.DEFAULT_SORT_COLUMN;
    }

    if (this.newSortDirection == null || this.newSortDirection.equalsIgnoreCase(""))
    {
      if (this.origSortDirection != null && !this.origSortDirection.equalsIgnoreCase(""))
        this.newSortDirection = this.origSortDirection;
      else
        this.newSortDirection = ARConstants.DEFAULT_SORT_DIRECTION;
    }

  }


  /*******************************************************************************************
  * populatePageData()
  ******************************************************************************************
  * Populate the page data with the remainder of the fields
  *
  * @param none
  * @return
  * @throws none
  */
  private void populatePageData()
  {
    //store the security token
    this.pageData.put("context", this.context);

    //store the forwardToName
    this.pageData.put("forwardToName", this.forwardToName);

    //store the forwardToType
    this.pageData.put("forwardToType", this.forwardToType);

    //store the orig origin selected
    this.pageData.put("orig_origin_selected", this.newOriginSelected);

    //store the orig partner selected
    this.pageData.put("orig_partner_selected", this.newPartnerSelected);

    //store the orig sort column
    this.pageData.put("orig_sort_column", this.newSortColumn);

    //store the orig sort direction
    this.pageData.put("orig_sort_direction", this.newSortDirection);

    //store the security token
    this.pageData.put("securitytoken", this.sessionId);


  }


  /*******************************************************************************************
  * validateLock()
  ******************************************************************************************
  * Check and/or retrieve a lock
  *
  * @param none
  * @return none
  * @throws none
  */
  private void validateLock()
    throws Exception
  {
    //create/validate the lock
    LockUtil lockUtil = null;
    lockUtil = new LockUtil(this.con);

    try
    {
      // attempt to gain lock
      this.lockValid =
          lockUtil.obtainLock(ARConstants.OUTSTANDING_RECEIVABLES_LOCK_ENTITY_TYPE, this.newPartnerSelected,
                              this.csrId, this.sessionId);
    }
    catch (EntityLockedException e)
    {
      this.lockValid = false;

      //create an ok message telling the user that the records are currently locked
      this.pageData.put("OK_MESSAGE",
                        "Records for " + this.newPartnerSelected + " are currently being edited.  " + this.newPartnerSelected + " " + e.getMessage() +
                        ".  Please try again later.");

    }
    catch (Exception e)
    {
      throw new Exception(e);
    }


  }


  /*******************************************************************************************
  * releaseLock()
  ******************************************************************************************
  * Release the lock
  *
  * @param none
  * @return none
  * @throws none
  */
  private void releaseLock()
    throws Exception
  {
    //release the lock
    LockUtil lockUtil = null;
    lockUtil = new LockUtil(this.con);
    boolean lockRelease = lockUtil.releaseLock(ARConstants.OUTSTANDING_RECEIVABLES_LOCK_ENTITY_TYPE, this.origPartnerSelected,
                          this.csrId, this.sessionId);


  }


  /*******************************************************************************************
  * processAction()
  ******************************************************************************************
  *
  * @throws
  */
  private void processAction()
    throws Exception
  {
    if (!this.lockValid)
    {
      processInvalidLock();
    }
    //If the action equals "load" - that is Initial load or Refresh data
    else if (this.actionType.equalsIgnoreCase("load"))
    {
      processLoad();
    }
    //If the action equals "exit"
    else if (this.actionType.equalsIgnoreCase("exit"))
    {
      processExit();
    }
    //If the action equals "sort"
    else if (this.actionType.equalsIgnoreCase("sort"))
    {
      processSort();
    }
    //If the action equals "settle"
    else if (this.actionType.equalsIgnoreCase("settle"))
    {
      processSettle();
    }
    //If the action equals "cancel"
    else if (this.actionType.equalsIgnoreCase("cancel"))
    {
      processCancel();
    }
    //If the action equals "download"
    else if (this.actionType.equalsIgnoreCase("download"))
    {
      processDownload();
    }
    //If the action equals "report_submission"
    else if (this.actionType.equalsIgnoreCase("report_submission"))
    {
      processReportSubmission();
    }
    //If the action equals "first_page" or "next_page" or "previous_page" or "last_page"
    else if (this.actionType.equalsIgnoreCase("first_page") || this.actionType.equalsIgnoreCase("previous_page") ||
             this.actionType.equalsIgnoreCase("next_page") || this.actionType.equalsIgnoreCase("last_page"))
    {
      processNavigation();
    }
    //If the action is not found as above, throw a new exception
    else
    {
      throw new Exception("Invalid Action Type - Please correct");
    }

  }

  /*******************************************************************************************
  * processInvalidLock()
  ******************************************************************************************
  *
  * @throws
  */
  private void processInvalidLock()
    throws Exception
  {
    //load the page as empty
    this.newPartnerSelected = null;
    this.newOriginSelected = null;

    //set the new page position
    this.newPagePosition = 1;

    //set the begin and max allowed parameters, to be passed to the DAOs.
    setBeginEndParameters(this.newPagePosition, this.maxRecordsPerPage);

    //load data as if first invokation
    loadData();

    this.forwardToName = "OutstandingReceivables";
    this.forwardToType = "XSL";

  }


  /*******************************************************************************************
  * processLoad()
  ******************************************************************************************
  *
  * @throws
  */
  private void processLoad()
    throws Exception
  {
    //if original and new origin/partner ids are different, save the data (if any) for the original origin/partner
    //before loading the data for the new origin/partner id
    if (this.origOriginSelected != null && !this.origOriginSelected.equalsIgnoreCase("") && this.newOriginSelected != null &&
        !this.newOriginSelected.equalsIgnoreCase("") && !this.origOriginSelected.equalsIgnoreCase(this.newOriginSelected) &&
        this.origPartnerSelected != null && !this.origPartnerSelected.equalsIgnoreCase("") &&
        this.newPartnerSelected != null && !this.newPartnerSelected.equalsIgnoreCase("") &&
        !this.origPartnerSelected.equalsIgnoreCase(this.newPartnerSelected))
    {
      //save the data
      updateData();
    }

    //set the new page position
    this.newPagePosition = 1;

    //set the begin and max allowed parameters, to be passed to the DAOs.
    setBeginEndParameters(this.newPagePosition, this.maxRecordsPerPage);

    //load the data as the first invokation
    loadData();

    this.forwardToName = "OutstandingReceivables";
    this.forwardToType = "XSL";

  }


  /*******************************************************************************************
  * processExit()
  ******************************************************************************************
  *
  * @throws
  */
  private void processExit()
    throws Exception
  {
    //save the data
    updateData();

    //release the lock
    releaseLock();

    //forward it to the Accounting menu
    this.forwardToName = "AccountingMenu";
    this.forwardToType = "ACTION";

  }


  /*******************************************************************************************
  * processSort()
  ******************************************************************************************
  *
  * @throws
  */
  private void processSort()
    throws Exception
  {
    //save the data
    updateData();

    //set the new page position
    this.newPagePosition = 1;

    //set the begin and max allowed parameters, to be passed to the DAOs.
    setBeginEndParameters(this.newPagePosition, this.maxRecordsPerPage);

    //load the data as the first invokation
    loadData();

    this.forwardToName = "OutstandingReceivables";
    this.forwardToType = "XSL";

  }


  /*******************************************************************************************
  * processSettle()
  ******************************************************************************************
  *
  * @throws
  */
  private void processSettle()
    throws Exception
  {
    //save the data
    updateData();

    //check if the remit number exists
    boolean remitNumberExists = checkRemitNumber();

    if (remitNumberExists)
    {
      //create an OK message to display the error
      this.pageData.put("OK_MESSAGE", "Remittance Number already exists.  Please fix before continuing...");

      //set the new page position
      this.newPagePosition = this.currentPagePosition;
    }
    else
    {
      //settle the remittance
      settle();

      //create an OK message
      this.pageData.put("OK_MESSAGE", "Settlement complete");

      //delete records from the temp table
      deleteData();

      //release the lock
      releaseLock();

      //load the page as empty
      this.newPartnerSelected = null;
      this.newOriginSelected = null;

      //set the new page position
      this.newPagePosition = 1;
    }

    //set the begin and max allowed parameters, to be passed to the DAOs.
    setBeginEndParameters(this.newPagePosition, this.maxRecordsPerPage);

    //load the data as the first invokation
    loadData();

    this.forwardToName = "OutstandingReceivables";
    this.forwardToType = "XSL";

  }


  /*******************************************************************************************
  * processCancel()
  ******************************************************************************************
  *
  * @throws
  */
  private void processCancel()
    throws Exception
  {
    //delete the data
    deleteData();

    //release the lock
    releaseLock();

    //load the page as empty
    this.newPartnerSelected = null;
    this.newOriginSelected = null;

    //set the new page position
    this.newPagePosition = 1;

    //set the begin and max allowed parameters, to be passed to the DAOs.
    setBeginEndParameters(this.newPagePosition, this.maxRecordsPerPage);

    //load the data as the first invokation
    loadData();

    //forward it to the Accounting menu
    this.forwardToName = "OutstandingReceivables";
    this.forwardToType = "XSL";

  }


  /*******************************************************************************************
  * processDownload()
  ******************************************************************************************
  *
  * @throws
  */
  private void processDownload()
    throws Exception
  {
    //save the data
    updateData();

    //invoke download process
    downloadData();

  }


  /*******************************************************************************************
  * processReportSubmission()
  ******************************************************************************************
  *
  * @throws
  */
  private void processReportSubmission()
    throws Exception
  {
    //save the data
    updateData();

    //forward it to the Report Submission page
    this.forwardToName = "ReportSubmission";
    this.forwardToType = "ACTION";

  }


  /*******************************************************************************************
  * processNavigation()
  ******************************************************************************************
  *
  * @throws
  */
  private void processNavigation()
    throws Exception
  {
    //save the data
    updateData();

    //if original and new origin/partner ids are different, save the data (if any) for the original origin/partner
    //before loading the data for the new origin/partner id
    if (this.origOriginSelected != null && !this.origOriginSelected.equalsIgnoreCase("") && this.newOriginSelected != null &&
        !this.newOriginSelected.equalsIgnoreCase("") && !this.origOriginSelected.equalsIgnoreCase(this.newOriginSelected) &&
        this.origPartnerSelected != null && !this.origPartnerSelected.equalsIgnoreCase("") &&
        this.newPartnerSelected != null && !this.newPartnerSelected.equalsIgnoreCase("") &&
        !this.origPartnerSelected.equalsIgnoreCase(this.newPartnerSelected))
    {
      //set the new page position
      this.newPagePosition = 1;
    }
    else
    {
      //set the new page position
      if (this.actionType.equalsIgnoreCase("first_page"))
        this.newPagePosition = 1;
      else if (this.actionType.equalsIgnoreCase("previous_page"))
        this.newPagePosition = this.currentPagePosition - 1;
      else if (this.actionType.equalsIgnoreCase("next_page"))
        this.newPagePosition = this.currentPagePosition + 1;
      else if (this.actionType.equalsIgnoreCase("last_page"))
        this.newPagePosition = this.totalPages;
    }

    //set the begin and max allowed parameters, to be passed to the DAOs.
    setBeginEndParameters(this.newPagePosition, this.maxRecordsPerPage);

    //load the data based on the page position
    loadData();

    this.forwardToName = "OutstandingReceivables";
    this.forwardToType = "XSL";

  }


  /*******************************************************************************************
  * loadData()
  ******************************************************************************************
  *
  * @throws
  */
  private void loadData()
    throws Exception
  {
    //Instantiate CustomerDAO
    ReceivablesDAO rDAO = new ReceivablesDAO(this.con);

    //hashmap that will contain the output from the stored proc
    HashMap remitInfo = new HashMap();

    //Call getRemitInfo method in the DAO
    remitInfo =
        rDAO.getRemitInfo(this.newPartnerSelected, this.newOriginSelected, this.csrId, this.startSearchingFrom, this.recordsToBeReturned,
                              this.newSortColumn, this.newSortDirection);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_REM_CUR into an Document.
    Document headerXML = XMLUtil.buildXML(remitInfo, "OUT_REM_CUR", "OUT_HEADERS", "OUT_HEADER");
    this.hashXML.put("HK_header", headerXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_REM_DTL_CUR into an Document.
    Document detailXML = XMLUtil.buildXML(remitInfo, "OUT_REM_DTL_CUR", "OUT_DETAILS", "OUT_DETAIL");
    this.hashXML.put("HK_detail", detailXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_REM_TOTALS_CUR into an Document.
    Document totalXML = XMLUtil.buildXML(remitInfo, "OUT_REM_TOTALS_CUR", "OUT_TOTALS", "OUT_TOTAL");
    this.hashXML.put("HK_totals", totalXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_PARTNERS_CUR into an Document.
    Document partnerXML = XMLUtil.buildXML(remitInfo, "OUT_PARTNERS_CUR", "OUT_PARTNERS", "OUT_PARTNER");
    this.hashXML.put("HK_partner", partnerXML);

    //get the value of OUT_ID_COUNT
    long lOutRemitDetailUpdateCount = 0;
    String sOutRemitDetailUpdateCount = "";
    if (remitInfo.get("OUT_REM_DTL_UPDATE_CNT") != null)
    {
      sOutRemitDetailUpdateCount = remitInfo.get("OUT_REM_DTL_UPDATE_CNT").toString();
      lOutRemitDetailUpdateCount = Long.valueOf(sOutRemitDetailUpdateCount).longValue();

    }
    this.pageData.put("remit_details_update_count", sOutRemitDetailUpdateCount);

    //create an OK message to display the error
    if (this.newOriginSelected != null &&
        !this.newOriginSelected.equalsIgnoreCase("") &&
        this.newPartnerSelected != null &&
        !this.newPartnerSelected.equalsIgnoreCase("") &&
        lOutRemitDetailUpdateCount == 0
       )
      this.pageData.put("OK_MESSAGE", "No Orders found for this partner.");


    setButtons(lOutRemitDetailUpdateCount, this.newPagePosition, this.maxRecordsPerPage);

  }


  /*******************************************************************************************
  * updateData()
  ******************************************************************************************
  *
  * @throws
  */
  private void updateData()
    throws Exception
  {
    ReceivablesDAO rDAO = new ReceivablesDAO(this.con);
    SimpleDateFormat remitFormat = new SimpleDateFormat("MMddyyyy");
    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

    /************************************************************
     * Update the Header info in the temp tables
     ************************************************************/
    String sRemitDate = null;
    Calendar cRemitDate = null;
    String remitAmount = null;

    //****retrieve the remit_date_text.
    if (request.getParameter("remit_date_text") != null)
      sRemitDate = this.request.getParameter("remit_date_text");

    if ((sRemitDate != null) && (sRemitDate.trim().length() > 0))
    {
      //Note that we are providing the user the flexibility to enter date in the format -> M/d/yyyy, MM/d/yyyy, M/dd/yyyy, or
      //MM/dd/yyyy.  Before converting to a calendar object, we will tokenize it to convert it to MMddyyyy format
      String sFormattedRemitDate = "";
      StringTokenizer st = new StringTokenizer(sRemitDate, "/");
      while (st.hasMoreTokens())
      {
        String token = st.nextToken();
        if (token.length()<2)
          sFormattedRemitDate += "0" + token;
        else
          sFormattedRemitDate += token;
      }

      cRemitDate = Calendar.getInstance();
      cRemitDate.setTime(remitFormat.parse(sFormattedRemitDate));
    }

    //****retrieve the remit_amount_text
    if (request.getParameter("remit_amount_text") != null)
      remitAmount = this.request.getParameter("remit_amount_text");

    rDAO.updateTempRemitHeader(this.origPartnerSelected, this.remitNumber, cRemitDate, remitAmount, this.csrId);


    /************************************************************
     * Update the Detail info in the temp tables
     ************************************************************/
    String settleFlag;
    String shipIdLineNum;
    String orderDetailId;
    String cashReceivedAmt;
    String writeOffAmt;
    String remainingBalDueAmt;

    for (long i = 1; i <= this.recordsDisplayedOnPage; i++)
    {
      settleFlag = null;
      shipIdLineNum = null;
      orderDetailId = null;
      cashReceivedAmt = null;
      writeOffAmt = null;
      remainingBalDueAmt = null;

      //****retrieve the settle_flag_
      if (request.getParameter("settle_flag_" + i) != null)
        settleFlag = this.request.getParameter("settle_flag_" + i);

      //****retrieve the ship_id_line_num_
      if (request.getParameter("ship_id_line_num_" + i) != null)
        shipIdLineNum = this.request.getParameter("ship_id_line_num_" + i);

      //****retrieve the order_detail_id_
      if (request.getParameter("order_detail_id_" + i) != null)
        orderDetailId = this.request.getParameter("order_detail_id_" + i);

      //****retrieve the cash_received
      if (request.getParameter("cash_received_amt_" + i) != null)
        cashReceivedAmt = this.request.getParameter("cash_received_amt_" + i);

      //****retrieve the write_off_amt_
      if (request.getParameter("write_off_amt_" + i) != null)
        writeOffAmt = this.request.getParameter("write_off_amt_" + i);

      //****retrieve the remaining_bal_due_amt
      if (request.getParameter("remaining_bal_due_amt_" + i) != null)
        remainingBalDueAmt = this.request.getParameter("remaining_bal_due_amt_" + i);

      rDAO.updateTempRemitDetail(this.origPartnerSelected, this.remitNumber, orderDetailId, cashReceivedAmt, writeOffAmt, remainingBalDueAmt, settleFlag, this.csrId);

    }

  }


  /*******************************************************************************************
  * deleteData()
  ******************************************************************************************
  *
  * @throws
  */
  private void deleteData()
    throws Exception
  {
    ReceivablesDAO rDAO = new ReceivablesDAO(this.con);

    //delete the temp detail info
    rDAO.deleteTempRemitDetail(this.origPartnerSelected);

    //delete the temp header info
    rDAO.deleteTempRemitHeader(this.origPartnerSelected);

  }


  /*******************************************************************************************
  * downloadData()
  ******************************************************************************************
  *
  * @throws
  */
  private void downloadData()
    throws Exception
  {
    Calendar cToday = Calendar.getInstance();
    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
    int currentRow = 0;

    //references to the cell name
    short cellShipIdLineNum = 0;
    short cellExternalOrderNumber = 1;
    short cellBillingDate = 2;
    short cellDeliveryDate = 3;
    short cellExpectedWholesaleAmt = 4;
    short cellPriorRemAppliedAmt = 5;
    short cellOutstandingBalDueAmt = 6;
    short cellCashReceivedAmt = 7;
    short cellDaysAged = 8;
    short cellWriteOffAmt = 9;
    short cellRemainingBalDueAmt = 10;

    //create the file name
    String fileName = "OR-" + sdf.format(cToday.getTime()) + ".xls";

    //Use the HSSFWorkbook constructor to create an Excel workbook file:
    HSSFWorkbook wb = new HSSFWorkbook();

    //Create a spreadsheet from the workbook:
    HSSFSheet spreadSheet = wb.createSheet("OR-RemitNumber=" + this.remitNumber);

    //A row in the spreadsheet has cells corresponding to each of the elements in the XML document.
    //Set the column width of each of the columns in the spreadsheet to 25.
    spreadSheet.setColumnWidth(cellShipIdLineNum, (short)(256*15));
    spreadSheet.setColumnWidth(cellExternalOrderNumber, (short)(256*15));
    spreadSheet.setColumnWidth(cellBillingDate, (short)(256*15));
    spreadSheet.setColumnWidth(cellDeliveryDate, (short)(256*15));
    spreadSheet.setColumnWidth(cellDaysAged, (short)(256*15));
    spreadSheet.setColumnWidth(cellExpectedWholesaleAmt, (short)(256*15));
    spreadSheet.setColumnWidth(cellPriorRemAppliedAmt, (short)(256*15));
    spreadSheet.setColumnWidth(cellOutstandingBalDueAmt, (short)(256*15));
    spreadSheet.setColumnWidth(cellCashReceivedAmt, (short)(256*15));
    spreadSheet.setColumnWidth(cellWriteOffAmt, (short)(256*15));
    spreadSheet.setColumnWidth(cellRemainingBalDueAmt, (short)(256*15));

    //define a row
    HSSFRow row;

    //define a cell
    HSSFCell cell;

    //Create a font object for the header row in the spreadsheet:
    HSSFFont headerFont = wb.createFont();

    //Next, specify the font setting for the header HSSFFont:
    headerFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

    //Create a style object for the header row in the spreadsheet:
    HSSFCellStyle headerTrailerStyle = wb.createCellStyle();

    //Next, set the font for the header style HSSFCellStyle object:
    headerTrailerStyle.setFont(headerFont);
    headerTrailerStyle.setWrapText(true);

    //create the header row
    row = spreadSheet.createRow((short)currentRow);

    /********** add the header cells to the row **********/
    //add the ship_id_line_num header
    cell=row.createCell(cellShipIdLineNum);
    cell.setCellValue("ship id line num");
    cell.setCellStyle(headerTrailerStyle);

    //add the external_order_number header
    cell=row.createCell(cellExternalOrderNumber);
    cell.setCellValue("external order number");
    cell.setCellStyle(headerTrailerStyle);

    //add the billing_date header
    cell=row.createCell(cellBillingDate);
    cell.setCellValue("billing date");
    cell.setCellStyle(headerTrailerStyle);

    //add the delivery_date header
    cell=row.createCell(cellDeliveryDate);
    cell.setCellValue("delivery date");
    cell.setCellStyle(headerTrailerStyle);

    //add the days_aged header
    cell=row.createCell(cellDaysAged);
    cell.setCellValue("days aged");
    cell.setCellStyle(headerTrailerStyle);

    //add the expected_wholesale_amt header
    cell=row.createCell(cellExpectedWholesaleAmt);
    cell.setCellValue("expected wholesale amt");
    cell.setCellStyle(headerTrailerStyle);

    //add the prior_rem_applied_amt header
    cell=row.createCell(cellPriorRemAppliedAmt);
    cell.setCellValue("prior rem applied amt");
    cell.setCellStyle(headerTrailerStyle);

    //add the outstanding_bal_due_amt header
    cell=row.createCell(cellOutstandingBalDueAmt);
    cell.setCellValue("outstanding bal due amt");
    cell.setCellStyle(headerTrailerStyle);

    //add the cash_received_amt header
    cell=row.createCell(cellCashReceivedAmt);
    cell.setCellValue("cash received amt");
    cell.setCellStyle(headerTrailerStyle);

    //add the write_off_amt header
    cell=row.createCell(cellWriteOffAmt);
    cell.setCellValue("write off amt");
    cell.setCellStyle(headerTrailerStyle);

    //add the remaining_bal_due_amt header
    cell=row.createCell(cellRemainingBalDueAmt);
    cell.setCellValue("remaining bal due amt");
    cell.setCellStyle(headerTrailerStyle);

    //Instantiate RemittanceDAO
    ReceivablesDAO rDAO = new ReceivablesDAO(this.con);

    //hashmap that will contain the output from the stored proc
    HashMap remitInfo = new HashMap();

    //Call getRemitInfo method in the DAO
    remitInfo =
        rDAO.getRemitInfo(this.newPartnerSelected, this.newOriginSelected, this.csrId, 1, this.remitDetailsUpdateCount,
                              this.newSortColumn, this.newSortDirection);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_REM_DTL_CUR into an Document.
    Document detailXML = XMLUtil.buildXML(remitInfo, "OUT_REM_DTL_CUR", "OUT_DETAILS", "OUT_DETAIL");

    double totalExpectedWholesaleAmt = 0;
    double totalPriorRemAppliedAmt = 0;
    double totalOutstandingBalDueAmt = 0;
    double totalCashReceivedAm = 0;
    double totalWriteOffAmt = 0;
    double totalRemainingBalDueAmt = 0;

    if (detailXML != null)
    {
      //Create an XPath object to parse the XML document:
      String xpath = "/OUT_DETAILS/OUT_DETAIL";

      //create the nodelist for the xml
      NodeList nl = DOMUtil.selectNodes(detailXML, xpath);
      Element detailElement = null;

      if ( nl.getLength() > 0 )
      {
        currentRow += 2;

        //Iterate over the node list, and add a row to the spreadsheet.  Use the HSSFRow object to add a spreadsheet row:
        for(int i=0; i < nl.getLength(); i++)
        {
          detailElement = (Element) nl.item(i);

          //create a row for the detail record
          row = spreadSheet.createRow((short)currentRow);
          currentRow++;

          /********** add the detail cells to the row **********/
          //add the ship_id_line_num detail
          cell=row.createCell(cellShipIdLineNum);
          cell.setCellValue(((Element)(nl.item(i))).getElementsByTagName("ship_id_line_num").item(0).getFirstChild().getNodeValue());

          //add the external_order_number detail
          cell=row.createCell(cellExternalOrderNumber);
          cell.setCellValue(((Element)(nl.item(i))).getElementsByTagName("external_order_number").item(0).getFirstChild().getNodeValue());

          //add the billing_date detail
          cell=row.createCell(cellBillingDate);
          cell.setCellValue(((Element)(nl.item(i))).getElementsByTagName("billing_date").item(0).getFirstChild().getNodeValue());

          //add the delivery_date detail
          cell=row.createCell(cellDeliveryDate);
          cell.setCellValue(((Element)(nl.item(i))).getElementsByTagName("delivery_date").item(0).getFirstChild().getNodeValue());

          //add the days_aged detail
          cell=row.createCell(cellDaysAged);
          cell.setCellValue(((Element)(nl.item(i))).getElementsByTagName("days_aged").item(0).getFirstChild().getNodeValue());

          //add the expected_wholesale_amt detail
          cell=row.createCell(cellExpectedWholesaleAmt);
          double expectedWholesaleAmt = Double.valueOf(((Element)(nl.item(i))).getElementsByTagName("expected_wholesale_amt").item(0).getFirstChild().getNodeValue()).doubleValue();
          totalExpectedWholesaleAmt += expectedWholesaleAmt;
          cell.setCellValue(expectedWholesaleAmt);

          //add the prior_rem_applied_amt detail
          cell=row.createCell(cellPriorRemAppliedAmt);
          double priorRemAppliedAmt = Double.valueOf(((Element)(nl.item(i))).getElementsByTagName("prior_rem_applied_amt").item(0).getFirstChild().getNodeValue()).doubleValue();
          totalPriorRemAppliedAmt += priorRemAppliedAmt;
          cell.setCellValue(priorRemAppliedAmt);

          //add the outstanding_bal_due_amt detail
          cell=row.createCell(cellOutstandingBalDueAmt);
          double outstandingBalDueAmt = Double.valueOf(((Element)(nl.item(i))).getElementsByTagName("outstanding_bal_due_amt").item(0).getFirstChild().getNodeValue()).doubleValue();
          totalOutstandingBalDueAmt += outstandingBalDueAmt;
          cell.setCellValue(outstandingBalDueAmt);

          //add the cash_received_amt detail
          cell=row.createCell(cellCashReceivedAmt);
          double cashReceivedAmt = Double.valueOf(((Element)(nl.item(i))).getElementsByTagName("cash_received_amt").item(0).getFirstChild().getNodeValue()).doubleValue();
          totalCashReceivedAm += cashReceivedAmt;
          cell.setCellValue(cashReceivedAmt);

          //add the write_off_amt detail
          cell=row.createCell(cellWriteOffAmt);
          double writeOffAmt = Double.valueOf(((Element)(nl.item(i))).getElementsByTagName("write_off_amt").item(0).getFirstChild().getNodeValue()).doubleValue();
          totalWriteOffAmt += writeOffAmt;
          cell.setCellValue(writeOffAmt);

          //add the remaining_bal_due_amt detail
          cell=row.createCell(cellRemainingBalDueAmt);
          double remainingBalDueAmt = Double.valueOf(((Element)(nl.item(i))).getElementsByTagName("remaining_bal_due_amt").item(0).getFirstChild().getNodeValue()).doubleValue();
          totalRemainingBalDueAmt += remainingBalDueAmt;
          cell.setCellValue(remainingBalDueAmt);

        }
      }
    }

    //create the trailer row
    row = spreadSheet.createRow((short)(currentRow += 3));

    /********** add the trailer cells to the row **********/
    //add the ship_id_line_num trailer
    cell=row.createCell(cellShipIdLineNum);
    cell.setCellValue("");

    //add the external_order_number trailer
    cell=row.createCell(cellExternalOrderNumber);
    cell.setCellValue("");

    //add the billing_date trailer
    cell=row.createCell(cellBillingDate);
    cell.setCellValue("");

    //add the delivery_date trailer
    cell=row.createCell(cellDeliveryDate);
    cell.setCellValue("");

    //add the days_aged trailer
    cell=row.createCell(cellDaysAged);
    cell.setCellValue("");

    //add the expected_wholesale_amt trailer
    cell=row.createCell(cellExpectedWholesaleAmt);
    cell.setCellValue(totalExpectedWholesaleAmt);
    cell.setCellStyle(headerTrailerStyle);

    //add the prior_rem_applied_amt trailer
    cell=row.createCell(cellPriorRemAppliedAmt);
    cell.setCellValue(totalPriorRemAppliedAmt);
    cell.setCellStyle(headerTrailerStyle);

    //add the outstanding_bal_due_amt trailer
    cell=row.createCell(cellOutstandingBalDueAmt);
    cell.setCellValue(totalOutstandingBalDueAmt);
    cell.setCellStyle(headerTrailerStyle);

    //add the cash_received_amt trailer
    cell=row.createCell(cellCashReceivedAmt);
    cell.setCellValue(totalCashReceivedAm);
    cell.setCellStyle(headerTrailerStyle);

    //add the write_off_amt trailer
    cell=row.createCell(cellWriteOffAmt);
    cell.setCellValue(totalWriteOffAmt);
    cell.setCellStyle(headerTrailerStyle);

    //add the remaining_bal_due_amt trailer
    cell=row.createCell(cellRemainingBalDueAmt);
    cell.setCellValue(totalRemainingBalDueAmt);
    cell.setCellStyle(headerTrailerStyle);


    this.response.setContentType("application/octet-stream");
    this.response.setHeader("Content-Disposition", "attachment; filename=" + fileName);

    //Similarly, you can set the values of the columns for the other cells in a row. Create a OutputStream to output the Excel workbook to an XLS file:
    OutputStream output=response.getOutputStream();

    //Output the Excel workbook to an XLS file, and close the OutputStream:
    wb.write(output);
    output.flush();
    output.close();

  }


  /*******************************************************************************************
  * settle()
  ******************************************************************************************
  *
  * @throws
  */
  private void settle()
    throws Exception
  {
    ReceivablesDAO rDAO = new ReceivablesDAO(this.con);

    //insert into the header info
    rDAO.insertRemitHeader(this.origPartnerSelected, this.remitNumber);

    //insert into the detail info
    rDAO.insertRemitDetail(this.origPartnerSelected, this.remitNumber);

  }


  /*******************************************************************************************
  * checkRemitNumber()
  ******************************************************************************************
  *
  * @throws
  */
  private boolean checkRemitNumber()
    throws Exception
  {
    ReceivablesDAO rDAO = new ReceivablesDAO(this.con);

    //check if the remit number exists
    String remitNumberExists = rDAO.remitNumberExists(this.remitNumber);

    if (remitNumberExists.equalsIgnoreCase("Y"))
      return true;
    else
      return false;

  }


  /*******************************************************************************************
  * setBeginEndParameters();
  ******************************************************************************************
  * This method will utilize the inputs to compute and set class level variables.   These
  * variables will be used later as start and end position, to be send to the stored procs.
  *
  * @param1 long - current page position
  * @param2 long - # of records to display
  * @return none
  * @throws none
  */
  private void setBeginEndParameters(long currentPagePosition, long maxRecordsToBeDisplay)
  {
    if (currentPagePosition == 0)
    {
      this.startSearchingFrom = 1;
      //this.recordsToBeReturned = this.startSearchingFrom * numberOfRecordsToDisplay;
      this.recordsToBeReturned = maxRecordsToBeDisplay;
    }
    else
    {
      this.startSearchingFrom = (--currentPagePosition * maxRecordsToBeDisplay) + 1;
      //this.recordsToBeReturned = (currentcurrentPagePosition + 1) * maxRecordsToBeDisplay;
      this.recordsToBeReturned = maxRecordsToBeDisplay;
    }

  }


  /*******************************************************************************************
 * setButtons()
 *******************************************************************************************
  * This method will utilize the inputs to determine which buttons should be enabled.
  *
  * @param1 long - total number of records
  * @param2 long - page position
  * @param3 long - maximumn number of records for that page
  * @return none
  * @throws none
  */
  private void setButtons(long outIdCount, long pagePosition, long maxRecordsPerPage)
  {
    //evaluate and set current_pages
    this.pageData.put("current_page", String.valueOf(pagePosition));

    //evaluate and set total_pages
    long totalPages = (long) (outIdCount / maxRecordsPerPage) + (long) (((long) (outIdCount % maxRecordsPerPage)) > 0? 1: 0);
    if (totalPages == 0)
      totalPages = 1;
    this.pageData.put("total_pages", String.valueOf(totalPages));

    //evaluate and set show_first and show_prev
    if (outIdCount > maxRecordsPerPage && pagePosition > 1)
    {
      this.pageData.put("show_first", "y");
      this.pageData.put("show_previous", "y");
    }
    else
    {
      this.pageData.put("show_first", "n");
      this.pageData.put("show_previous", "n");
    }

    //evaluate and set show_next and show_last
    if (outIdCount > maxRecordsPerPage && pagePosition < totalPages)
    {
      this.pageData.put("show_next", "y");
      this.pageData.put("show_last", "y");
    }
    else
    {
      this.pageData.put("show_next", "n");
      this.pageData.put("show_last", "n");
    }

  }


}

