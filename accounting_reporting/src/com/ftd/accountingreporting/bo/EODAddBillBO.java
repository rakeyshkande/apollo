package com.ftd.accountingreporting.bo;

import com.ftd.accountingreporting.cache.handler.EODPCardConfigHandler;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.vo.EODRecordVO;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * This is a concrete class that contains logic to compile an end of day 
 * record specific to Additional bills
 */
public class EODAddBillBO extends EODRegularBillBO
{

    private static Logger logger  = 
        new Logger("com.ftd.accountingreporting.bo.EODAddBillBO");
   
    public EODAddBillBO(EODPCardConfigHandler pcardConfig)
    {
        super(pcardConfig);
    }
    
    public void populateFields(CachedResultSet rs) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering populateFields");
        }
        
        try{
            super.populateFields(rs);
            EODRecordVO vo = super.getRecordVO();
            vo.setBillType(ARConstants.CONST_ADD_BILL);
            vo.setAuthCharIndicator(ARConstants.COMMON_VALUE_NO);
            this.setRecordVO(vo);
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting populateFields");
            } 
        } 
    }
}