package com.ftd.accountingreporting.bo;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.util.AccountingUtil;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import org.apache.xpath.XPathAPI;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import com.ftd.accountingreporting.dao.CBRDAO;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.w3c.dom.traversal.NodeIterator;

public class ReconBillingBO 
{
    private Connection dbConn;
    private Logger logger = 
        new Logger("com.ftd.accountingreporting.bo.ReconBillingBO");
        
    public ReconBillingBO(Connection conn)
    {
        super();
        dbConn = conn;
    }

    /**
     * �	retrieveReconDoc. Add the Document in HashMap with key �doc�.
     * �	Add �load� in HashMap with key �forwardName�
     * �	Return the HashMap.
     * @param request - HttpServletRequest
     * @return HashMap
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     */ 
    public HashMap handleViewRecon(HttpServletRequest request) throws Exception
    {
       if(logger.isDebugEnabled()){
            logger.debug("Entering handleViewRecon");
        }
        HashMap viewRecon = new HashMap();
        try{
            Document recon = retrieveReconDoc(request);
            viewRecon.put("doc",recon);
            viewRecon.put("forwardName","load");
        
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting handleViewRecon");
            } 
        }
        
        return viewRecon;
    }

    /**
     * �	retrieveReconDoc. Add the Document in HashMap with key �doc�.
     * �	Add �print� in HashMap with key �forwardName�
     * �	Return the HashMap. 
     * @param request - HttpServletRequest
     * @return HashMap
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     */ 
    public HashMap handlePrintRecon(HttpServletRequest request) throws Exception
    {
       if(logger.isDebugEnabled()){
            logger.debug("Entering handlePrintRecon");
        }
        HashMap printRecon = new HashMap();
        try{
            Document recon = retrieveReconDoc(request);
            printRecon.put("doc",recon);
            printRecon.put("forwardName","print");
        
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting handlePrintRecon");
            } 
        }
        
        return printRecon;   
    }

    /**
     * �	Create Document as in Appendix B (recon_db.xml) of this document.
     *      o	Retrieve external_order_number from the request
     *      o	Retrieve the Messages section by calling 
     *          CBRDAO.doGetMercVenusTransactions.
     *          ?	Define MESSAGES as TOP element and MESSAGE as BOTTOM 
     *              element in statement xml.
     *          ?	To calculate cutoff_date, substract the number of months 
     *              defined by �MESG_ARCHIVE_NUM_MONTHS� in 
     *              accountingreporting-config.xml from current system time.
     *          ?	Format dates to MM/dd/yyyy.
     *          ?	Format price to add $.
     * �	Return Document.
     * @param request - HttpServletRequest
     * @return Document
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     */ 
    public Document retrieveReconDoc(HttpServletRequest request) throws Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering retrieveReconDoc");
        }
        Document reconDoc = null;
        try{
            HashMap parameters = (HashMap)request.getAttribute("parameters");
            String orderDetailID = (String) parameters.get("order_detail_id");
            if(orderDetailID == null)
            {
                /* attempt to retrieve from request */
                orderDetailID = request.getParameter("order_detail_id");
            }
            
            /* calculate cutoff_date */
            String archiveNumMonths = ConfigurationUtil.getInstance().getProperty(
                        ARConstants.CONFIG_FILE,ARConstants.MESG_ARCHIVE_NUM_MONTHS);
            String archiveNumDays = ConfigurationUtil.getInstance().getProperty(
                        ARConstants.CONFIG_FILE,ARConstants.MESG_ARCHIVE_NUM_DAYS);
            AccountingUtil acctUtil = new AccountingUtil();
                    
            int archiveMonths = 0;
            int archiveDays = 0;
            Date cutoff = new Date();
                    
            // Use archive number of months if it's configured; Otherwise use 
            // the number of days configured.
            if(archiveNumMonths != null && !"".equals(archiveNumMonths)) {
                 archiveMonths = Integer.parseInt(archiveNumMonths);
                 cutoff = acctUtil.calculateOffSetDatebyMonth(archiveMonths); 
            } else {
                 archiveDays = Integer.parseInt(archiveNumDays);
                 cutoff = acctUtil.calculateOffSetDate(archiveDays);
            }
          
            /* retrieve messages */
            CBRDAO cbrDAO = new CBRDAO(dbConn);
            Document messages = cbrDAO.doGetMercVenusTransactions(orderDetailID,cutoff);

            if(messages!=null){
                /* Process Messages */
                NodeIterator ni = XPathAPI.selectNodeIterator(messages,"messages/message");
                for (Node messageNode=ni.nextNode(); messageNode!=null;) {
                    // Format created_on and reconciled_date to take the format MM/dd/yyyy.
                    Node reconciledDateNode = XPathAPI.selectSingleNode(
                        messageNode,"reconciled_date/text()");
                    if(reconciledDateNode!=null){
                        String reconciledDateValue = reconciledDateNode.getNodeValue();
                        reconciledDateNode.setNodeValue(acctUtil.formatDate(reconciledDateValue));
                    }

                    Node transmissionDateNode = XPathAPI.selectSingleNode(
                        messageNode,"created_on/text()");
                    if(transmissionDateNode!=null){
                        String transmissionDateValue = transmissionDateNode.getNodeValue();
                        transmissionDateNode.setNodeValue(acctUtil.formatDate(transmissionDateValue));
                    }
           
                    // Retrieve price and format price to add $. 
                    Node priceNode = XPathAPI.selectSingleNode(messageNode,"price/text()");
                    if(priceNode!=null){
                        //String price = priceNode.getNodeValue();
                        
                        Node mercVenusIdNode = XPathAPI.selectSingleNode(messageNode,"merc_venus_id/text()");
                        String mercVenusId = "";
                        if(mercVenusIdNode != null) {
                            mercVenusId = mercVenusIdNode.getNodeValue();
                        }
                        mercVenusId = mercVenusId.replaceAll("-","");
                        
                        Node systemNode = XPathAPI.selectSingleNode(messageNode,"system/text()");
                        String system = "MERC";
                        if(systemNode != null) {
                            system = systemNode.getNodeValue();
                        }
                        
                        // Retrieve price by calling calculate_price_to_reconcile.
                        String price = cbrDAO.doGetPriceByMsgNumber(mercVenusId, system, cutoff);
                        priceNode.setNodeValue(acctUtil.formatAmount(price));
                    } else {
                        logger.error("Price Node is null");
                    }
                    Node next = ni.nextNode();
                    messageNode = next;
                }
            }
            /* create root and external order number elements */
            String xml = "<root>";
            String extOrderNum = (String) parameters.get("external_order_number");
            if(extOrderNum == null)
            {
                /* attempt to retrieve from request */
                extOrderNum = request.getParameter("external_order_number");
            }
            xml = xml + "<external_order_number>" + extOrderNum + "</external_order_number>";
            /* convert Messages document to a string and append to mail 
             * doc */
            if(messages!=null){
                StringWriter sw = new StringWriter();
                DOMUtil.print(((Document) messages), new PrintWriter(sw));
                xml = xml + sw.toString();
             }
            /* create close for root element */
            xml = xml + "</root>";

            /* convert XML string to Document object */
            reconDoc = DOMUtil.getDocument(xml);
            
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting retrieveReconDoc");
            } 
        }
        
        return reconDoc; 
    }

    /**
     * �	retrieveBillingDoc with request and false. Add the Document in 
     *      HashMap with key �doc�.
     * �	Add �load� in HashMap with key �forwardName�
     * �	Return the HashMap.
     * @param request - HttpServletRequest
     * @return Document
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     */ 
    public HashMap handleViewBilling(HttpServletRequest request) 
        throws Exception
    {
       if(logger.isDebugEnabled()){
            logger.debug("Entering handleViewBilling");
        }
        HashMap viewBilling = new HashMap();
        try{
               
            Document recon = retrieveBillingDoc(request,false);
            viewBilling.put("doc",recon);
            viewBilling.put("forwardName","load");
        
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting handleViewBilling");
            } 
        }
        
        return viewBilling; 
    }

    /**
     * �	retrieveBillingDoc with request and false. Add the Document in 
     *      HashMap with key �doc�.
     * �	Add �print� in HashMap with key �forwardName�
     * �	Return the HashMap.
     * @param request - HttpServletRequest
     * @return Document
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     */
    public HashMap handlePrintBilling(HttpServletRequest request) 
        throws Exception
    {
       if(logger.isDebugEnabled()){
            logger.debug("Entering handlePrintBilling");
        }
        HashMap printBilling = new HashMap();
        try{
            
            Document billingDoc = retrieveBillingDoc(request,false);
            
            printBilling.put("doc",billingDoc);
            printBilling.put("forwardName","print");
            
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting handlePrintBilling");
            } 
        }
        
        return printBilling;
    }

    /**
     * �	Create Document as in Appendix B (billing_db.xml) of this document.
     *      o	Add POPUP node. If popup is true, display is �true�. Otherwise 
     *          it�s false.
     *      o	Retrieve master_order_number from the filter (get attribute 
     *          �parameters� HashMap from request)
     *      o	Retrieve the billing_transactions section by calling 
     *          CBRDAO.doGetBillingTransactions.
     *          ?	Define BILLING_TRANSACTIONS as TOP element and 
     *              BILLING_TRANSACTION as BOTTOM element in statement xml.
     *          ?	Add attribute �is_link� to BILLING_TRANSACTION node. 
     * �	If �TYPE� is �Charge Back� or �Retrieval�, 
     *      o	add [] around the type value.
     *      o	is_link is �true� if the user is authorized to access the resource. 
     *          This is determined by calling CBRDAO.isUserAuthorized. 
     *          ?	retrieve context and securitytoken from filter parameters HashMap.
     *          ?	Resource is �Charge Back Retrieval�
     *          ?	Permission is �Yes�.
     *      o	Otherwise, �is_link� is false.
     *          ?	Format dates to MM/dd/yyyy.
     *          ?	Format bill_refund_amount to add $. If type is 
     *              �Charge Back� or �Retrieval�, add () around the formatted value.
     *      o	Create the BUTTONS section. 
     *          ?	Both buttons have disabled = �false� if the user is not 
     *              authorized to access the �Charge Back Retrieval� resource.
     *          ?	Call CBRDAO.doGetPaymentsForCB. Both buttons have 
     *              disabled = �false� if no payments are returned.
     *          ?	Call CBRDAO.isShoppingCartBilled. 
     *              If true, both buttons should be enabled.
     *          ?	Otherwise disabled=�true� for both buttons.
     * �	Return Document.
     * @param request - HttpServletRequest
     * @return Document
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     */
    public Document retrieveBillingDoc(HttpServletRequest request, boolean popup)
        throws Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering retrieveBillingDoc");
        }
        Document billingDoc = null;
        
        try{
            String xml = "<root displayMessage=''>";
            
                /* Add POPUP node */
            xml = xml + "<popup display='" + String.valueOf(popup) + "'/>";
            
            /* Retrieve master_order_number from the filter (get attribute 
              "parameters" HashMap from request) */
            HashMap parameters = (HashMap)request.getAttribute("parameters");
            String masterOrderNum = (String) parameters.get("master_order_number");
            if(masterOrderNum == null)
            {
                /* attempt to retrieve from request */
                masterOrderNum = request.getParameter("master_order_number");
            }
            xml = xml + "<master_order_number>" + masterOrderNum + "</master_order_number>";
            
            /* Retrieve the billing_transactions section by calling 
                CBRDAO.doGetBillingTransactions.  */
            CBRDAO cbrDAO = new CBRDAO(dbConn);
            Document billingTrans = cbrDAO.doGetBillingTransactions(masterOrderNum);
           
            /* check authorization for charge back retrieval */
            String securityToken = (String) parameters.get("securitytoken");
            if(securityToken == null)
            {
                /* attempt to retrieve from request */
                securityToken = request.getParameter("securitytoken");
            }
            String context = (String) parameters.get("context");
            if(context == null)
            {
                /* attempt to retrieve from request */
                context = request.getParameter("context");
            }
            boolean authorized = cbrDAO.isUserAuthorized(context,
                        securityToken,"Charge Back Retrieval","Yes");

            /* retrieve TYPE value from billing transaction */
            NodeIterator ni = XPathAPI.selectNodeIterator(billingTrans,"billing_transactions/billing_transaction");
            AccountingUtil acctUtil = new AccountingUtil();
            for (Node billingTransNode=ni.nextNode(); billingTransNode!=null;) {
                /* check type */
                Node typeNode = XPathAPI.selectSingleNode(billingTransNode,"type/text()");
                String type = typeNode.getNodeValue();

                /* Format bill_refund_amount to add $ and 
                 * add () around the formatted value. */
                Node amountNode = XPathAPI.selectSingleNode(billingTransNode,"amount/text()");
                String amount = "$0.00";
                // amount is null.
                if(amountNode == null) {
                    NodeList amounts = ((Element)billingTransNode).getElementsByTagName("amount");
                    amountNode = amounts.item(0);
                    Text amountText = billingTrans.createTextNode(amount);
                    amountNode.appendChild(amountText);
                } else { 
                    amount = amountNode.getNodeValue();
                    amountNode.setNodeValue(AccountingUtil.formatAmount(amount));
                }

                Attr isLinkAttr = billingTrans.createAttribute("is_link");
                isLinkAttr.setValue("false");
                if(type.equals("Charge Back") || type.equals("Retrieval"))
                {
                    /* add [] around the type value */
                    type = "[" + type + "]";
                    typeNode.setNodeValue(type);
                    
                    
                    /* is_link is �true� if the user is 
                     * authorized to access the resource. */                    
                    if(authorized == true)
                    {
                        /* set is link = true */
                        isLinkAttr.setValue("true");
                    }
                    /* Format bill_refund_amount to  
                     * add () around the formatted value. */
                    amountNode = XPathAPI.selectSingleNode(billingTransNode,"amount/text()");
                    amount = amountNode.getNodeValue();
                    amount = "(" + amount + ")";
                    amountNode.setNodeValue(amount);
                }
                ((Element)(billingTransNode)).setAttributeNode(isLinkAttr);
                
                    /* Format dates to MM/dd/yyyy. */
                    Node receivedDateNode = XPathAPI.selectSingleNode(
                        billingTransNode,"cbr_received_date/text()");
                    if(receivedDateNode!=null){
                        String receivedDateValue = receivedDateNode.getNodeValue();
                        receivedDateNode.setNodeValue(acctUtil.formatDate(receivedDateValue));
                    }
                    
                    Node dueDateNode = XPathAPI.selectSingleNode(
                        billingTransNode,"cbr_due_date/text()");
                    if(dueDateNode!=null){
                        String dueDateValue = dueDateNode.getNodeValue();
                        dueDateNode.setNodeValue(acctUtil.formatDate(dueDateValue));
                    }
                    
                    Node reversalDateNode = XPathAPI.selectSingleNode(
                        billingTransNode,"reversal/text()");
                    if(reversalDateNode!=null){
                        String reversalDateValue = reversalDateNode.getNodeValue();
                        reversalDateNode.setNodeValue(acctUtil.formatDate(reversalDateValue));
                    }
                    
                    Node billedDateNode = XPathAPI.selectSingleNode(
                        billingTransNode,"billed_date/text()");
                    if(billedDateNode!=null){
                        String billedDateValue = billedDateNode.getNodeValue();
                        billedDateNode.setNodeValue(acctUtil.formatDate(billedDateValue));
                    }
                    
                    Node settledDateNode = XPathAPI.selectSingleNode(
                        billingTransNode,"settled_date/text()");
                    if(settledDateNode!=null){
                        String settledDateValue = settledDateNode.getNodeValue();
                        settledDateNode.setNodeValue(acctUtil.formatDate(settledDateValue));
                    }
                Node next = ni.nextNode();
                billingTransNode = next;
            }

            /* convert Billing Trans document to a string and append to main doc */
            StringWriter sw = new StringWriter();       
            DOMUtil.print(((Document) billingTrans), new PrintWriter(sw));
            xml = xml + sw.toString();
            
            /* Create the BUTTONS section.  */
            String onClickMsgCB = "";
            String onClickMsgRT = "";
            
            if(authorized == true)
            {
                Document paymentsCB = cbrDAO.doGetPaymentsForCB(masterOrderNum, "CB");
                Document paymentsRT = cbrDAO.doGetPaymentsForCB(masterOrderNum, "RT");
                
                if(paymentsCB.getFirstChild().hasChildNodes() == false)
                {
                    onClickMsgCB = "No payments available.";
                } 
                
                if(paymentsRT.getFirstChild().hasChildNodes() == false)
                {
                    onClickMsgRT = "No payments available.";
                } 
            } else {
                onClickMsgCB = "Not authorized.";
                onClickMsgRT = "Not authorized.";
            }
            String buttons = "<buttons><button name='add_chargeback' message='" + onClickMsgCB + "'/>" + 
                    "<button name='add_retrieval' message='" + onClickMsgRT + "'/></buttons>"; 

            xml = xml + buttons;
            xml = xml + "</root>";
            billingDoc = DOMUtil.getDocument(xml);
            
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting retrieveBillingDoc");
            } 
        }
        
        return billingDoc;
    }




}