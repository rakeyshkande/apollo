package com.ftd.accountingreporting.bo;

import com.ftd.accountingreporting.cache.handler.EODPCardConfigHandler;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.exception.MissingCreditCardException;
import com.ftd.accountingreporting.util.AccountingUtil;
import com.ftd.accountingreporting.util.EODUtil;
import com.ftd.accountingreporting.vo.EODRecordVO;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * This is an concrete class that contains logic to compile an end of day 
 * record specific to refund bills
 */
public class EODRefundBO extends EODRecordBO
{

    private static Logger logger  = 
        new Logger("com.ftd.accountingreporting.bo.EODRecordBO");
        
    public EODRefundBO(EODPCardConfigHandler pcardConfig)
    {
        super(pcardConfig);
    }

  
  protected void setTransactionType()
  {
    
    recordVO.setTransactionType(ARConstants.TRANSACTION_TYPE_REFUND);
  }
  
    public void populateFields(CachedResultSet currentRecord) throws Exception
    {

        if(logger.isDebugEnabled()) {
            logger.debug("Entering populateFields");
        }
        AccountingUtil acctgUtil = new AccountingUtil();
        String paymentMethod = null;
        String ccNumber = null;
        
        try{
            recordVO = new EODRecordVO();
            setLineItemType();
            setTransactionType();
            recordVO.setBillType(ARConstants.CONST_REFUND);

            recordVO.setRefundId(currentRecord.getLong("refund_id"));
            recordVO.setPaymentId(currentRecord.getLong("payment_id"));
            recordVO.setPaymentMethod(currentRecord.getString("payment_type"));
            recordVO.setCreditCardType(currentRecord.getString("cc_type"));
            recordVO.setCreditCardNumber(currentRecord.getString("cc_number"));
            recordVO.setCcExpDate(currentRecord.getString("cc_expiration"));
            recordVO.setOrderNumber(currentRecord.getString("master_order_number"));
            recordVO.setOrderDate(EODUtil.convertSQLDate(currentRecord.getDate("created_on"))); 

            //set auth date to refund created date.
            recordVO.setAuthorizationDate(EODUtil.convertSQLDate(currentRecord.getDate("created_on"))); 
            recordVO.setOrderAmount(currentRecord.getDouble("debit_amount"));                    
            recordVO.setOrderGuid(currentRecord.getString("order_guid"));                       
            recordVO.setCompanyId(currentRecord.getString("company_id"));
            recordVO.setOriginType(currentRecord.getString("order_source"));
            setClearingMemberNumber(currentRecord.getLong("payment_id"));
            /* set program name for pcard */
            recordVO.setProgramName(currentRecord.getString("program_name"));
            //As per BA's suggestion, Since refund doesn't have any authorization we are using created date as authorization date
            recordVO.setAuthorizationDate(EODUtil.convertSQLDate(currentRecord.getDate("created_on")));
            
            // Throw exception if credit card number is missing.
            paymentMethod = recordVO.getPaymentMethod();
            ccNumber = recordVO.getCreditCardNumber();
            
            if(paymentMethod == null) {
                throw new Exception("Missing Payment Method for refund id: " + recordVO.getRefundId());
            }
            if(!acctgUtil.isNonCreditCardPaymentType(paymentMethod) && ccNumber == null) {
                throw new MissingCreditCardException("Missing Credit Card Number for refund id: " + recordVO.getRefundId());
            }
            
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting populateFields");
            } 
        } 

  }
}