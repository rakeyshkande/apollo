package com.ftd.accountingreporting.bo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.math.BigDecimal;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TimeZone;

import javax.naming.InitialContext;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import com.ftd.accountingreporting.action.GiftCertActionHelper;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.dao.GccRecipientDAO;
import com.ftd.accountingreporting.dao.GccUtilDAO;
import com.ftd.accountingreporting.dao.GiftCertificateCouponDAO;
import com.ftd.accountingreporting.util.AccountingUtil;
import com.ftd.accountingreporting.util.LineItemGenerator;
import com.ftd.accountingreporting.vo.GccRecipientVO;
import com.ftd.accountingreporting.vo.GiftCertificateCouponVO;
import com.ftd.accountingreporting.vo.GiftCertificateSourceCodeVO;
import com.ftd.novator.giftcert.util.MessageUtil;
import com.ftd.novator.giftcert.vo.GiftCertMessageVO;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.GiftCodeUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.BatchInfo;
import com.ftd.osp.utilities.vo.CreateBatchRequest;
import com.ftd.osp.utilities.vo.CreateRequest;
import com.ftd.osp.utilities.vo.FileFormat;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.vo.Meta;
import com.ftd.osp.utilities.vo.RecipientInfo;
import com.ftd.osp.utilities.vo.RedemptionDetails;
import com.ftd.osp.utilities.vo.ReferenceCode;
import com.ftd.osp.utilities.vo.UpdateBatchRequest;
import com.ftd.osp.utilities.vo.UpdateGiftCodeRequest;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.security.SecurityManager;

/**
 * This Class has all the business methods related to Gift Certificate Coupon.
 * This class creates GiftCertificateCoupon, Search GiftCertificateCoupon based
 * on Request Number and Coupon Number and updates GiftCertificate Coupons.
 */
public class GiftCertBO {
	private static Logger logger = new Logger(
			"com.ftd.accountingreporting.bo.GiftCertBO");
	private GiftCertificateCouponDAO gccDAO;
	private GccUtilDAO gccUtilDAO;
	private GccRecipientDAO gccRecDAO;
	private static final String nullString = "(null)";
	private static final String newLineChar = "\n";
	private Connection conn;
	

	private final int ALLOWED_SRC_CDE_RANGE = 30000;

	private static String STATUS_NONE = "none";

	/**
	 * Contructor for class GiftCertificateCouponBO
	 * 
	 * @param connection
	 */
	public GiftCertBO(Connection connection) {
		conn = connection;
		gccDAO = new GiftCertificateCouponDAO(connection);
		gccUtilDAO = new GccUtilDAO(connection);
		gccRecDAO = new GccRecipientDAO(connection);
	}

	/**
	 * This method handles the client requests depending upon the request made
	 * by the client.
	 * 
	 * @param request
	 * @return Document
	 * @throws java.lang.Exception
	 */
	public Document processRequest(HttpServletRequest request) throws Exception {
		Document responseDocument = null;
		logger.debug("Entering processRequest...");
		String action = request.getParameter(ARConstants.COMMON_GCC_ACTION);
		String gccType = request.getParameter(ARConstants.COMMON_GCC_TYPE);

		if (action.equals(ARConstants.ACTION_PARM_DISPLAY_CREATE)) {
			// displays the screen for creation of GiftCertificateCoupon
			responseDocument = handleDisplayCreate(gccType, request);
		} else if (action.equals(ARConstants.ACTION_PARM_CREATE)) {
			// creates GiftCertificateCoupons
			responseDocument = handleCreate(request);
		} else if (action.equals(ARConstants.ACTION_PARM_MAINTAIN)) {
			// maintains GiftCertificateCoupon
			responseDocument = handleMaintain(request);
		} else if (action.equals(ARConstants.ACTION_PARM_SEARCH)) {
			// Searchs GiftCertificateCoupon based on RequestNumber or
			// CouponNumber
			responseDocument = handleSearch(request);
		}
		logger.debug("Exiting processRequest...");

		return responseDocument;
	}

	/**
	 * This method handles the display create functionality of gift certificate
	 * coupon
	 * 
	 * @param gccType
	 *            String
	 * @return Document
	 * @throws java.lang.Exception
	 */
	private Document handleDisplayCreate(String gccType,
			HttpServletRequest request) throws Exception {
		logger.debug("Entering handleDisplayCreate...");
		Document displayDocument = DOMUtil.getDefaultDocument();
		Element root = (Element) displayDocument
				.createElement(ARConstants.XML_ROOT);
		displayDocument.appendChild(root);

		Element fields = (Element) displayDocument
				.createElement(ARConstants.XML_FIELD_TOP);
		root.appendChild(fields);
		fields.appendChild(getNameElement(displayDocument,
				"gcc_enable_create_cs", "Not Added"));
		fields.appendChild(getNameElement(displayDocument, "gcc_company", "FTD"));

		// if gccType = 'user_cs' get the next request Number from Database
		if ((gccType != null) && (gccType.equals(ARConstants.CUSTOMER_SERVICE))) {
			String reqNumber = getNextRequestNumber(gccType);
			fields.appendChild(getNameElement(displayDocument,
					ARConstants.COMMON_GCC_REQUEST_NUMBER, reqNumber));
			fields.appendChild(getNameElement(displayDocument, "gcc_type",
					gccType));
		}
		
		// add coupontype list and company list to document
		displayDocument = getDropDownValues(displayDocument, request);

		root = displayDocument.getDocumentElement();
		root.setAttribute(ARConstants.FORWARD_STATE,
				ARConstants.COMMON_VALUE_YES);

		logger.debug("handle display create completed");
		
		return displayDocument;
	}

	/**
	 * This method handles the create functionality of gift certificate coupon
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @return Document
	 * @throws java.lang.Exception
	 */
	private Document handleCreate(HttpServletRequest request) throws Exception {
		logger.debug("Inside handleCreate");
		GiftCodeUtil util = GiftCodeUtil.getInstance();
		GiftCertRecipBO recipbo = new GiftCertRecipBO(conn);
		Document displayDocument = DOMUtil.getDefaultDocument();
		Element root = (Element) displayDocument
				.createElement(ARConstants.XML_ROOT);
		displayDocument.appendChild(root);
		String company = request.getParameter("gcc_company");
		if (null != company) {
			company = company.trim().toLowerCase();
		} else {
			company = ARConstants.FTD_COMPANY_ID;
		}
		// String requestNumber = request.getParameter("gcc_request_number");
		// populate GiftCertificateCouponVO
		GiftCertificateCouponVO vo = getGiftCertificateCouponVO(request);

		ArrayList<GccRecipientVO> rArray = recipbo
				.getRecipientListFromRequest(request);
		Document rDoc = recipbo.getRecipientDocFromList(rArray);

		// validate if the request number passed is not duplicate request
		// number, if the
		// source code passed belongs to company selected and order number is
		// valid order
		// number
		Document valDocument = validateNewGiftCertificateCoupon(vo);
		logger.debug("After validateNewGiftCertificateCoupon in handleCreate");

		// if above validation fails then throw the error to user
		if (valDocument != null) {
			// sets forward state to 'N'
			root.setAttribute(ARConstants.FORWARD_STATE,
					ARConstants.COMMON_VALUE_NO);
			// append error doc
			displayDocument.getDocumentElement().appendChild(
					displayDocument.importNode(
							valDocument.getDocumentElement(), true));
			// append field doc
			displayDocument.getDocumentElement().appendChild(
					displayDocument.importNode(createFieldDoc(request, vo)
							.getDocumentElement(), true));

			// append recipient section
			if (rDoc != null) {
				displayDocument.getDocumentElement().appendChild(
						displayDocument.importNode(rDoc.getDocumentElement(),
								true));
			}

			// add coupontype list and company list to document
			return getDropDownValues(displayDocument, request);
		}

		// create a file name if the format is electronic, format for the
		// filename is requestnumber+dateformat+timeformat
		String fileName = "";
		if (request.getParameter("gcc_format").equals("E")) {
			Calendar cal = Calendar.getInstance(TimeZone.getDefault());
			SimpleDateFormat sdfT = new SimpleDateFormat("yyyyMMddHHmmss");
			fileName = vo.getRequestNumber() + sdfT.format(cal.getTime());
			vo.setFileName(fileName);
		}
		// create GiftCertificateCouponRequest
		vo.setSourceCodeVO(getGiftCertSrcCodeVOFromRequest("I", request));
		Map<String, String> srcCodeValidateMsgMap = new HashMap<String, String>();
		if (vo.getSourceCodeVO().getRestrictedSourceCodes().size() > 0) {
			srcCodeValidateMsgMap = gccDAO.doCreateSourceCodes(vo
					.getCompanyId(), vo.getRequestNumber(),
					getSpaceDelimSrcCodes(vo.getSourceCodeVO()
							.getRestrictedSourceCodes()), vo.getCreatedBy());
			
			/*
			 * Below code is to find out if invalid or expired source code is present than remove from final source code list
			 * which gets added in GCS.
			 */
			
			String[] existsMulipleArray = null;
			String[] expiredMultipleArray = null;
			String notExistsSrcCode = srcCodeValidateMsgMap.get(ARConstants.SRC_CODE_EXISTS_MESSAGE);
			if (StringUtils.isNotEmpty(notExistsSrcCode)) {
				String[] existArray = notExistsSrcCode.split(":");
				notExistsSrcCode = existArray[1].trim();
				logger.info("notExistsSrcCode : "+notExistsSrcCode);
				existsMulipleArray = notExistsSrcCode.split(" ");
				
			}
			String expiredSrcCode = srcCodeValidateMsgMap.get(ARConstants.SRC_CODE_EXPIRED_MESSAGE);
			if (StringUtils.isNotEmpty(expiredSrcCode)) {
				String[] expiredArray = expiredSrcCode.split(":");
				expiredSrcCode = expiredArray[1].trim();
				logger.info("expiredSrcCode : "+expiredSrcCode);
				expiredMultipleArray = expiredSrcCode.split(" ");
				
			}
			for(int countSrc = 0 ; countSrc < vo.getSourceCodeVO().getRestrictedSourceCodes().size() ; countSrc++){
				if (null != existsMulipleArray && existsMulipleArray.length > 0) {
					for (String exists : existsMulipleArray) {
						if (exists.equals(vo.getSourceCodeVO()
								.getRestrictedSourceCodes().get(countSrc))) {
							vo.getSourceCodeVO().getRestrictedSourceCodes()
									.remove(countSrc);
						}
					}
				}
				if(null != expiredMultipleArray && expiredMultipleArray.length > 0){
					for(String expired: expiredMultipleArray){
						if(expired.equals(vo.getSourceCodeVO().getRestrictedSourceCodes().get(countSrc))){
							vo.getSourceCodeVO().getRestrictedSourceCodes().remove(countSrc);
						}
					}
				}
				
				
				/*if(null != notExistsSrcCode && notExistsSrcCode.equals(vo.getSourceCodeVO().getRestrictedSourceCodes().get(countSrc))){
					vo.getSourceCodeVO().getRestrictedSourceCodes().remove(countSrc);
				}
				if(null != expiredSrcCode && expiredSrcCode.equalsIgnoreCase(vo.getSourceCodeVO().getRestrictedSourceCodes().get(countSrc))){
					vo.getSourceCodeVO().getRestrictedSourceCodes().remove(countSrc);
				}*/
			}
		}
		
		// gccDAO.doCreateGcCouponRequest(vo);
		logger.debug("Creating Gift certificate...");
		CreateBatchRequest giftCert = setGiftCertificate(vo, rArray);
		boolean createSuccess = false;
		String displayMessage = request.getParameter(ARConstants.COMMON_GCC_TYPE)
				+ " " + ARConstants.MESSAGE_SUCCESSFUL_CREATE;
		try {
			util.createGiftCertificate(giftCert, company);
			createSuccess = true;
		} catch (IllegalStateException ex) {
			displayMessage = "Gift Code Service did not return OK. Details : " + ex.getMessage();
			if ( ex.getMessage().contains("Batch Number already exists")) {
				displayDocument.getDocumentElement().appendChild(
						displayDocument.importNode(
								generateErrorDocument("gcc_request_number", "Request Exists", null).getDocumentElement(), true));
				displayMessage = ""; //This is what enables the create button. DOH.
			}
		}

		// ENDING MICROSERVICE
		
		// if the format is electronic create the file
		if (request.getParameter("gcc_format").equals("E"))
			displayMessage = createElectronicFile(vo, fileName, displayMessage);

		// append field doc
		Element fields = createFieldDoc(request, vo).getDocumentElement();
		displayDocument.getDocumentElement().appendChild(displayDocument.importNode(fields, true));
		
		root = displayDocument.getDocumentElement();
		if(createSuccess) {
			displayMessage = displayMessage + "."
					+ ARConstants.GC_MESSAGE_NOTIFICATION;
		}
		root.setAttribute(ARConstants.CONST_SUCCESS_MSG, displayMessage);
		
		if (!srcCodeValidateMsgMap.isEmpty()) {
			root.setAttribute(ARConstants.SOURCE_CODE_EXISTS_ERROR_MSG,
					srcCodeValidateMsgMap
							.get(ARConstants.SRC_CODE_EXISTS_MESSAGE));
			root.setAttribute(
					ARConstants.SOURCE_CODE_EXISTS_ERROR_COUNT,
					getTokensCount(
							srcCodeValidateMsgMap
									.get("INVALID_SOURCE_CODES_COUNT") != null ? srcCodeValidateMsgMap
									.get("INVALID_SOURCE_CODES_COUNT") : " ",
							" ").toString());
			root.setAttribute(ARConstants.SOURCE_CODE_EXPIRED_ERROR_MSG,
					srcCodeValidateMsgMap
							.get(ARConstants.SRC_CODE_EXPIRED_MESSAGE));
			root.setAttribute(
					ARConstants.SOURCE_CODE_EXPIRED_ERROR_COUNT,
					getTokensCount(
							srcCodeValidateMsgMap
									.get("EXPIRED_SOURCE_CODES_COUNT") != null ? srcCodeValidateMsgMap
									.get("EXPIRED_SOURCE_CODES_COUNT") : " ",
							" ").toString());
		}
		
		root.setAttribute(ARConstants.FORWARD_STATE, ARConstants.COMMON_VALUE_YES);

		logger.debug("HandleCreate completed");
		return getDropDownValues(displayDocument, request);
	}

	/**
	 * To call Gift Code Microservice, matching object is created.
	 * 
	 * @param vo
	 * @param recipientArray
	 * @param sourceCode
	 * @return GiftCertificate
	 */
	private CreateBatchRequest setGiftCertificate(GiftCertificateCouponVO vo,
			ArrayList<GccRecipientVO> recipientArray) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
		CreateBatchRequest details = new CreateBatchRequest();
		CreateRequest gc = new CreateRequest();
		List<RecipientInfo> recipientInfo = new ArrayList<RecipientInfo>();
		BatchInfo bInfo = new BatchInfo();
		bInfo.setBatchComment(vo.getProgramComment());
		bInfo.setBatchDescription(vo.getProgramDesc());
		bInfo.setBatchName(vo.getProgramName());
		bInfo.setOrderSource(vo.getOrderSource());
		bInfo.setPrefix(vo.getPrefix());
		bInfo.setOther(vo.getOther());
		bInfo.setQuantity(Long.valueOf(vo.getQuantity()).intValue());
		List<ReferenceCode> codeList = new ArrayList<ReferenceCode>();
		if (null != vo.getSourceCodeVO()
				&& null != vo.getSourceCodeVO().getRestrictedSourceCodes()) {
			for (String str : vo.getSourceCodeVO().getRestrictedSourceCodes()) {				
				ReferenceCode code = new ReferenceCode();
				code.setRefCode(str);
				codeList.add(code);
			}
		}
		gc.setSource(codeList);
		gc.setBatchNumber(vo.getRequestNumber());
		gc.setBatchInfo(bInfo);

		gc.setCreatedOn(simpleDateFormat.format(new Date()));
		gc.setExpirationDate(null != vo.getExpirationDate() ? simpleDateFormat.format(
				vo.getExpirationDate().getTime()) : null);
		FileFormat fFormat = new FileFormat();
		fFormat.setFileName(vo.getFileName());
		fFormat.setFormat(vo.getFormat());
		gc.setFileFormat(fFormat);
		gc.setIssueAmount(BigDecimal.valueOf(vo.getIssueAmount()));

		gc.setIssueDate(null != vo.getIssueDate() ? simpleDateFormat.format(
				vo.getIssueDate().getTime()) : null);
		Meta meta = new Meta();
		meta.setCreatedBy(vo.getCreatedBy());
		meta.setUpdatedBy(vo.getUpdatedBy());
		meta.setRequestedBy(vo.getRequestedBy());
		gc.setMeta(meta);
		gc.setPaidAmount(BigDecimal.valueOf(vo.getPaidAmount()));
		gc.setQuantity(Long.valueOf(vo.getQuantity()).intValue());
		gc.setType(vo.getGcCouponType());
		gc.setPromotionId(vo.getPromotionID());
		gc.setSiteId(vo.getCompanyId());
		if (null != recipientArray) {
			for (GccRecipientVO rVo : recipientArray) {
				RecipientInfo rInfo = new RecipientInfo();
				rInfo.setAddress1(rVo.getAddress1());
				rInfo.setAddress2(rVo.getAddress2());
				rInfo.setCity(rVo.getCity());
				rInfo.setCountry(rVo.getCountry());
				rInfo.setEmail(rVo.getEmailAddress());
				rInfo.setFirstName(rVo.getFirstName());
				rInfo.setLastName(rVo.getLastName());
				rInfo.setPhone(rVo.getPhone());
				rInfo.setState(rVo.getState());
				rInfo.setZipcode(rVo.getZipCode());
				rInfo.setIssuedReferenceNumber(rVo.getIssuedReferenceNumber());
				recipientInfo.add(rInfo);
			}
		}
		details.setRecipientInfo(recipientInfo);
		details.setCreateDetails(gc);
		return details;
	}

	/**
	 * This method handles the maintain functionality of gift certificate coupon
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @return Document
	 * @throws java.lang.Exception
	 */
	private Document handleMaintain(HttpServletRequest request)
			throws Exception {
		logger.debug("HANDLE MAINTAIN Coupon Number : "
				+ request.getParameter("gcc_coupon_number"));
		if (request.getParameter("is_multiple_gcc").equals(
				ARConstants.COMMON_VALUE_YES)) {
			// maintains GiftCertificateCoupon by request number
			return maintainMultiple(request);
		} else {
			// maintains GiftCertificateCoupon by coupon number
			return maintainSingle(request);
		}
	}

	/**
	 * This method maintains single gift certificate coupon where the search is
	 * made by coupon number.
	 * 
	 * @param request
	 * @return Document
	 * @throw Exception
	 */
	private Document maintainSingle(HttpServletRequest request)
			throws Exception {
		logger.debug("Entering maintainSingle...");
		GiftCodeUtil util = GiftCodeUtil.getInstance();
		Document displayDocument = DOMUtil.getDefaultDocument();
		Element root = (Element) displayDocument
				.createElement(ARConstants.XML_ROOT);
		displayDocument.appendChild(root);
		String couponNumber = request.getParameter("gcc_coupon_number");
		Boolean isSourceCodeModified = isSourceCodeListChanged(request);
		GiftCertificateCouponVO vo = getGiftCertificateCouponVO(request);
		GiftCertRecipBO recipbo = null;
		Document rDoc = null;
		String company = request.getParameter("gcc_company");
		if (null != company) {
			company = company.trim().toLowerCase();
		} else {
			company = ARConstants.FTD_COMPANY_ID;
		}
		// Check for errors
		if (validateSingleUpdate(vo) != null) {
			recipbo = new GiftCertRecipBO(conn);
			root.setAttribute(ARConstants.FORWARD_STATE,
					ARConstants.COMMON_VALUE_NO);

			displayDocument.getDocumentElement().appendChild(
					displayDocument.importNode(validateSingleUpdate(vo)
							.getDocumentElement(), true));
			// append field doc
			displayDocument.getDocumentElement().appendChild(
					displayDocument.importNode(createFieldDoc(request, vo)
							.getDocumentElement(), true));
			// append status
			displayDocument.getDocumentElement().appendChild(
					displayDocument.importNode(
							getStatusList(request, "N",
									request.getParameter("gcc_type"))
									.getDocumentElement(), true));

			// append recipient section if there are recipient info.
			rDoc = recipbo.getRecipientDocFromRequest(request);
			if (rDoc != null) {
				displayDocument.getDocumentElement().appendChild(
						displayDocument.importNode(rDoc.getDocumentElement(),
								true));
			}
			displayDocument = getDropDownValues(displayDocument, request);
			String xmlResponse = util.searchGiftCertificate(
					vo.getRequestNumber(), couponNumber, company);
			displayDocument.getDocumentElement().appendChild(
					displayDocument.importNode(
							getRedemDateAndOrderDetailId(xmlResponse)
									.getDocumentElement(), true));
			root.setAttribute("sub_header_name", "Gift Certificate/Coupon #:");
			root.setAttribute("sub_header_value", couponNumber);
			logger.debug("Exiting maintainSingle with error...");
			return displayDocument;
		}
		// updateSingleGiftCertificateCoupon(vo);
		// call Update microservice
		logger.debug("Updating Gift Certificate...");
		UpdateGiftCodeRequest updateGiftCertRequest = setUpdateGiftCertificaterequest(vo);
		String statusUpdated = GiftCodeUtil.update(updateGiftCertRequest);
		logger.debug("Updated Gift Certificate Id : "
				+ updateGiftCertRequest.getGiftCodeId() + " with status : "
				+ statusUpdated);
		// callUpdateGiftIdService(vo, company);
		// END
		vo.setSourceCodeVO(getGiftCertSrcCodeVOFromRequest("U", request));
		Map<String, String> updateSrcCodeMessages = new HashMap<String, String>();
		// call Update Source microservice

		/*
		 * if (!(vo.getSourceCodeVO().getSrcCodeMapForUpdate().get("INSERT")
		 * .isEmpty() && vo.getSourceCodeVO().getSrcCodeMapForUpdate()
		 * .get("DELETE").isEmpty())) { isSourceCodeModified = true;
		 */
		updateSrcCodeMessages = gccDAO.doUpdateRestrictedSourceCodes(vo,
				getSpaceDelimSrcCodes(vo.getSourceCodeVO()
						.getRestrictedSourceCodes()));// THIS IS LEFT FOR
														// SOURCE CODE
														// VALIDATION ONLY.
														// INNER DAO CALLS
														// ARE COMMENTED

		root = displayDocument.getDocumentElement();
		root.setAttribute(ARConstants.CONST_SUCCESS_MSG,
				request.getParameter(ARConstants.COMMON_GCC_TYPE) + " "
						+ ARConstants.MESSAGE_SUCCESSFUL_UPDATE + "."
						+ ARConstants.GC_MESSAGE_NOTIFICATION);
		// append field doc
		displayDocument.getDocumentElement().appendChild(
				displayDocument.importNode(createFieldDoc(request, vo)
						.getDocumentElement(), true));

		if (!updateSrcCodeMessages.isEmpty()) {
			root.setAttribute(
					ARConstants.SOURCE_CODE_EXISTS_ERROR_COUNT,
					getTokensCount(
							updateSrcCodeMessages
									.get("INVALID_SOURCE_CODES_COUNT") != null ? updateSrcCodeMessages
									.get("INVALID_SOURCE_CODES_COUNT") : " ",
							" ").toString());
			root.setAttribute(
					ARConstants.SOURCE_CODE_EXPIRED_ERROR_COUNT,
					getTokensCount(
							updateSrcCodeMessages
									.get("EXPIRED_SOURCE_CODES_COUNT") != null ? updateSrcCodeMessages
									.get("EXPIRED_SOURCE_CODES_COUNT") : " ",
							" ").toString());
			root.setAttribute(ARConstants.SOURCE_CODE_EXISTS_ERROR_MSG,
					updateSrcCodeMessages
							.get(ARConstants.SRC_CODE_EXISTS_MESSAGE));
			root.setAttribute(ARConstants.SOURCE_CODE_EXPIRED_ERROR_MSG,
					updateSrcCodeMessages
							.get(ARConstants.SRC_CODE_EXPIRED_MESSAGE));
		}
		logger.debug("Updating Source for Gift Certificate...");
		// Updating Source for Gift Certificate id
		String[] existsMulipleArray = null;
		String[] expiredMultipleArray = null;
		if (!updateSrcCodeMessages.isEmpty()) {
			String notExistsSrcCode = updateSrcCodeMessages
					.get(ARConstants.SRC_CODE_EXISTS_MESSAGE);
			if (StringUtils.isNotEmpty(notExistsSrcCode)) {
				String[] existArray = notExistsSrcCode.split(":");
				notExistsSrcCode = existArray[1].trim();
				existsMulipleArray = notExistsSrcCode.split(" ");;
			}
			String expiredSrcCode = updateSrcCodeMessages
					.get(ARConstants.SRC_CODE_EXPIRED_MESSAGE);
			if (StringUtils.isNotEmpty(expiredSrcCode)) {
				String[] expiredArray = expiredSrcCode.split(":");
				expiredSrcCode = expiredArray[1].trim();
				expiredMultipleArray = expiredSrcCode.split(" ");
			}
			for (int countSrc = 0; countSrc < vo.getSourceCodeVO()
					.getRestrictedSourceCodes().size(); countSrc++) {
				if (null != existsMulipleArray && existsMulipleArray.length > 0) {
					for (String exists : existsMulipleArray) {
						if (exists.equals(vo.getSourceCodeVO()
								.getRestrictedSourceCodes().get(countSrc))) {
							vo.getSourceCodeVO().getRestrictedSourceCodes()
									.remove(countSrc);
						}
					}
				}
				if(null != expiredMultipleArray && expiredMultipleArray.length > 0){
					for(String expired: expiredMultipleArray){
						if(expired.equals(vo.getSourceCodeVO().getRestrictedSourceCodes().get(countSrc))){
							vo.getSourceCodeVO().getRestrictedSourceCodes().remove(countSrc);
						}
					}
				}
			}
		}

		/*String[] listSourceCodes = null;
		if (null != vo.getSourceCodeVO().getRestrictedSourceCodes()) {
			Collections.sort(vo.getSourceCodeVO().getRestrictedSourceCodes());
			listSourceCodes = getSpaceDelimSrcCodes(vo.getSourceCodeVO()
					.getRestrictedSourceCodes());
		}*/
		
		List<ReferenceCode> codeList = new ArrayList<ReferenceCode>();
		if (null != vo.getSourceCodeVO()
				&& null != vo.getSourceCodeVO().getRestrictedSourceCodes()) {
			for (String str : vo.getSourceCodeVO().getRestrictedSourceCodes()) {				
				ReferenceCode code = new ReferenceCode();
				code.setRefCode(str);
				codeList.add(code);
			}
		}
		
		
		if (isSourceCodeModified) {
			callUpdateSourceCode(vo.getGcCouponNumber(), null, vo.getUpdatedBy(), codeList, company);
		}


		logger.debug("Updated Source");

		root.setAttribute(ARConstants.FORWARD_STATE,
				ARConstants.COMMON_VALUE_YES);

		/* send queue message if status is Cancelled or Reinstate */
		logger.info("Calyx - Coupon status from form: '" + vo.getGcCouponStatus() + "' Source codes modified flag: " + isSourceCodeModified);
		if ((null != vo.getGcCouponStatus() && 
		         (vo.getGcCouponStatus().equals("Cancelled") ||
					 vo.getGcCouponStatus().equals("Issued Active") || 
					 vo.getGcCouponStatus().equals("Reinstate"))
		         ) || isSourceCodeModified) {
			/* retrieves the coupon info for the specified request number */
			MessageUtil msgUtil = new MessageUtil();

			String xmlResponse = util.searchGiftCertificate(
					vo.getRequestNumber(), couponNumber, company);
			if (null == xmlResponse) {
				logger.error(ARConstants.RETRIEVE_BATCH_ERROR_RESPONSE);
			}

			Document rd = GiftCodeUtil.getXMLDocument(xmlResponse);
			String source = getStringValueFromXML(rd, "order_source");
			GiftCertMessageVO giftCertMsg = new GiftCertMessageVO();
			// check order source, only send for internet orders
			if (source.equals("I")) {
				giftCertMsg.setCouponNumber(vo.getGcCouponNumber());
				giftCertMsg.setRequestType(vo.getGcCouponStatus() != null ? vo.getGcCouponStatus() : "none");
				giftCertMsg.setRetry(0);
				msgUtil.dispatchMessage(giftCertMsg.toXML());
				if (isSourceCodeModified) {
					sendJMSMessageForGiftCertMaintenance(
							vo.getGcCouponNumber(),
							"SINGLE-GC-SOURCE-CODE-MODIFIED");
				}
			}
		}
		logger.debug("Exiting maintainSingle...");
		return displayDocument;
	}

	

	/**
	 * Calls Update Batch API of Gift Code Micro service
	 * 
	 * @param vo
	 * @throws Exception
	 */
	private void callUpdateBatchService(GiftCertificateCouponVO vo,
			String company,List<ReferenceCode> codeList) {
		
		String batchNumber = vo.getRequestNumber();
		try {
			batchNumber = GiftCodeUtil.updateBatch(setUpdateBatchrequest(vo, codeList));
		} catch (IllegalStateException e) {
			logger.error(e.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		logger.info("Update Batch Number : "+batchNumber);
	}

	private Map<String, String> callUpdateSourceCode(String giftCodeId, String requestNumber, String updatedBy,
			List<ReferenceCode> codeList, String company) {
		Map<String, String> retVal = null;
		try {
			retVal = GiftCodeUtil.callUpdateSourceCode(giftCodeId,
					requestNumber, updatedBy, codeList, company);
		} catch (IllegalStateException e) {
			logger.error(e.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return retVal;
	}

	/**
	 * Setting up request for calling Update Batch Service
	 * 
	 * @param vo
	 * @return UpdateBatchRequest
	 */
	private UpdateBatchRequest setUpdateBatchrequest(GiftCertificateCouponVO vo, List<ReferenceCode> codeList) {
		UpdateBatchRequest batchReq = new UpdateBatchRequest();
		batchReq.setBatchComment(vo.getProgramComment());
		batchReq.setBatchDescription(vo.getProgramDesc());
		batchReq.setBatchName(vo.getProgramName());
		batchReq.setBatchNumber(vo.getRequestNumber());
		batchReq.setPromotionId(vo.getPromotionID());
		batchReq.setRequestedBy(vo.getRequestedBy());
		batchReq.setStatus(vo.getGcCouponStatus());
		batchReq.setUpdatedBy(vo.getUpdatedBy());
		logger.debug("In Setting codelIst size : "+codeList.size());
		batchReq.setCodeList(codeList);
		return batchReq;
	}

	/**
	 * Setting up request for Update Single Gift Certificate API
	 * 
	 * @param vo
	 * @return
	 */
	private UpdateGiftCodeRequest setUpdateGiftCertificaterequest(
			GiftCertificateCouponVO vo) {
		List<RedemptionDetails> dtlLst = new ArrayList<RedemptionDetails>();
		RedemptionDetails redemptionDtls = new RedemptionDetails();
		
		UpdateGiftCodeRequest updateGiftCertReq = new UpdateGiftCodeRequest();
		updateGiftCertReq.setGiftCodeId(vo.getGcCouponNumber());
		
		updateGiftCertReq.setTotalRedemptionAmount(BigDecimal.valueOf(vo
				.getIssueAmount()));
		updateGiftCertReq.setStatus(vo.getGcCouponStatus());
		redemptionDtls.setRedeemedRefNo(vo.getGcOrderNumber());
		dtlLst.add(redemptionDtls);
		updateGiftCertReq.setRedemptionDetails(dtlLst);
		updateGiftCertReq.setSource(null);
		return updateGiftCertReq;
	}

	/**
	 * This method maintains multiple gift certificate coupon where the search
	 * is made by request number. If the promotion id is changed, send all
	 * 'Issued Active' and 'Expired' Gift Cert to Novator to be added.
	 * 
	 * @param request
	 * @return Document
	 * @throw Exception
	 */
	private Document maintainMultiple(HttpServletRequest request)
			throws Exception {
		logger.debug("Entering maintainMultiple...");
		//GiftCodeUtil util = GiftCodeUtil.getInstance();
		Document displayDocument = DOMUtil.getDefaultDocument();
		Element root = (Element) displayDocument
				.createElement(ARConstants.XML_ROOT);
		//Boolean isSourceCodeModified = isSourceCodeListChanged(request);
		String company = request.getParameter("gcc_company");
		if (null != company) {
			company = company.trim().toLowerCase();
		} else {
			company = ARConstants.FTD_COMPANY_ID;
		}
		displayDocument.appendChild(root);

		GiftCertificateCouponVO vo = getGiftCertificateCouponVO(request);

		//callUpdateRecipient(vo, request);
		vo.setSourceCodeVO(getGiftCertSrcCodeVOFromRequest("U", request));

		

		Map<String, String> updateSrcCodeMessages = new HashMap<String, String>();
	
			updateSrcCodeMessages = gccDAO.doUpdateRestrictedSourceCodes(vo,
					getSpaceDelimSrcCodes(vo.getSourceCodeVO()
							.getRestrictedSourceCodes()));
		// THIS IS LEFT FOR SOURCE CODE VALIDATION ONLY, INSERT CALL is
			// COMMENTED IN PROCEDURE INSIDE

		

		displayDocument.getDocumentElement().appendChild(
				displayDocument.importNode(createFieldDoc(request, vo)
						.getDocumentElement(), true));

		root = displayDocument.getDocumentElement();
		root.setAttribute(ARConstants.CONST_SUCCESS_MSG,
				request.getParameter(ARConstants.COMMON_GCC_TYPE) + " "
						+ ARConstants.MESSAGE_SUCCESSFUL_UPDATE + "."
						+ ARConstants.GC_MESSAGE_NOTIFICATION);
		root.setAttribute(ARConstants.FORWARD_STATE,
				ARConstants.COMMON_VALUE_YES);

		if (null != updateSrcCodeMessages && !updateSrcCodeMessages.isEmpty()) {

			root.setAttribute(
					ARConstants.SOURCE_CODE_EXISTS_ERROR_COUNT,
					getTokensCount(
							updateSrcCodeMessages
									.get("INVALID_SOURCE_CODES_COUNT") != null ? updateSrcCodeMessages
									.get("INVALID_SOURCE_CODES_COUNT") : " ",
							" ").toString());
			root.setAttribute(
					ARConstants.SOURCE_CODE_EXPIRED_ERROR_COUNT,
					getTokensCount(
							updateSrcCodeMessages
									.get("EXPIRED_SOURCE_CODES_COUNT") != null ? updateSrcCodeMessages
									.get("EXPIRED_SOURCE_CODES_COUNT") : " ",
							" ").toString());

			root.setAttribute(ARConstants.SOURCE_CODE_EXISTS_ERROR_MSG,
					updateSrcCodeMessages
							.get(ARConstants.SRC_CODE_EXISTS_MESSAGE));
			root.setAttribute(ARConstants.SOURCE_CODE_EXPIRED_ERROR_MSG,
					updateSrcCodeMessages
							.get(ARConstants.SRC_CODE_EXPIRED_MESSAGE));
		}
		String[] existsMulipleArray = null;
		String[] expiredMultipleArray = null;
		
		if (!updateSrcCodeMessages.isEmpty()) {
			String notExistsSrcCode = updateSrcCodeMessages
					.get(ARConstants.SRC_CODE_EXISTS_MESSAGE);
			if (StringUtils.isNotEmpty(notExistsSrcCode)) {
				String[] existArray = notExistsSrcCode.split(":");
				notExistsSrcCode = existArray[1].trim();
				existsMulipleArray = notExistsSrcCode.split(" ");
			}
			String expiredSrcCode = updateSrcCodeMessages
					.get(ARConstants.SRC_CODE_EXPIRED_MESSAGE);
			if (StringUtils.isNotEmpty(expiredSrcCode)) {
				String[] expiredArray = expiredSrcCode.split(":");
				expiredSrcCode = expiredArray[1].trim();
				expiredMultipleArray = expiredSrcCode.split(" ");
			}
			for (int countSrc = 0; countSrc < vo.getSourceCodeVO()
					.getRestrictedSourceCodes().size(); countSrc++) {
				if (null != existsMulipleArray && existsMulipleArray.length > 0) {
					for (String exists : existsMulipleArray) {
						if (exists.equals(vo.getSourceCodeVO()
								.getRestrictedSourceCodes().get(countSrc))) {
							vo.getSourceCodeVO().getRestrictedSourceCodes()
									.remove(countSrc);
						}
					}
				}
				if(null != expiredMultipleArray && expiredMultipleArray.length > 0){
					for(String expired: expiredMultipleArray){
						if(expired.equals(vo.getSourceCodeVO().getRestrictedSourceCodes().get(countSrc))){
							vo.getSourceCodeVO().getRestrictedSourceCodes().remove(countSrc);
						}
					}
				}
			}
		}
		
		/*String[] listSourceCodes = null;
		if (null != vo.getSourceCodeVO().getRestrictedSourceCodes()) {
			Collections.sort(vo.getSourceCodeVO().getRestrictedSourceCodes());
			listSourceCodes = getSpaceDelimSrcCodes(vo.getSourceCodeVO()
					.getRestrictedSourceCodes());
		}*/
		logger.debug("SIZE of source codes : "+vo.getSourceCodeVO().getRestrictedSourceCodes().size());
		List<ReferenceCode> codeList = new ArrayList<ReferenceCode>();
		if (null != vo.getSourceCodeVO()
				&& null != vo.getSourceCodeVO().getRestrictedSourceCodes()) {
			for (String str : vo.getSourceCodeVO().getRestrictedSourceCodes()) {				
				ReferenceCode code = new ReferenceCode();
				code.setRefCode(str);
				codeList.add(code);
			}
		}		

		// Calling Microservice
		
		// Calling Microservice
		callUpdateBatchService(vo, company, codeList);
		
		
		
		/*callUpdateSourceCode(null, vo.getRequestNumber(), vo.getUpdatedBy(),
				codeList, company);*/
		
		// retrieve promotion id from request
		/*String promotionIdReq = vo.getPromotionID();
		promotionIdReq = (promotionIdReq == null ? "" : promotionIdReq);

		String xmlStringResp = util.searchGiftCertificate(
				vo.getRequestNumber(), vo.getGcCouponNumber(), company);
		Document rd = GiftCodeUtil.getXMLDocument(xmlStringResp);*/
		/*
		 * Document rd = gccDAO.doSearchRequest(vo.getRequestNumber(),
		 * vo.getGcCouponNumber());
		 */
		/*String promotionIdRec = getStringValueFromXML(rd, "gcc_promotion_id");
		promotionIdRec = (promotionIdRec == null ? "" : promotionIdRec);

      logger.info("Calyx - Maintain multiple - Coupon status from form: '" + vo.getGcCouponStatus() + 
                  "' Source codes modified flag: " + isSourceCodeModified);
*/
		// In the past we would enqueue here so that gift cert feeds would be sent to website,
		// but now the microservice will trigger the feeds.  Ultimately one of the following
      // will be enqueued: MULTIPLE-GC-PROMOTION_ID_CHANGED, MULTIPLE-GC-SOURCE-CODE-MODIFIED,
      // MULTIPLE-GC-CANCEL-OR-REINSTATE
		
		logger.debug("Exiting maintainMultiple...");
		return displayDocument;
	}

	private void sendJMSMessageForGiftCertMaintenance(String gcNumber,
			String correlationId) throws Exception {

		MessageToken messageToken = new MessageToken();
		messageToken.setStatus("GIFT_CERT_MAINT");
		messageToken.setJMSCorrelationID(correlationId);
		messageToken.setMessage(gcNumber);
		Dispatcher dispatcher = Dispatcher.getInstance();
		dispatcher.dispatchTextMessage(new InitialContext(), messageToken);
	}

	/**
	 * This method handles the search functionality of gift certificate coupon
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @return Document
	 * @throws java.lang.Exception
	 */
	private Document handleSearch(HttpServletRequest request) throws Exception {
		logger.debug("Entering handleSearch...");
		GiftCodeUtil util = GiftCodeUtil.getInstance();
		Document displayDocument = DOMUtil.getDefaultDocument();
		Element root = (Element) displayDocument
				.createElement(ARConstants.XML_ROOT);
		displayDocument.appendChild(root);
		String requestNumber = request.getParameter("gcc_request_number");
		if (requestNumber != null) {
			requestNumber = requestNumber.trim();
			if (!"".equals(requestNumber)) {
				requestNumber = requestNumber.toUpperCase();
			}
		}
		String couponNumber = request.getParameter("gcc_coupon_number");
		if (couponNumber != null) {
			couponNumber = couponNumber.trim();
		}
		String company = request.getParameter("gcc_company");
		if (null != company) {
			company = company.trim().toLowerCase();
		} else {
			company = ARConstants.FTD_COMPANY_ID;
		}
		logger.debug("COMPANY : " + company);
		// Calling Retrieve Batch API
		Document rd = null;
		String xmlStringResp = null;
		try {
			xmlStringResp = util.searchGiftCertificate(requestNumber,
					couponNumber, company);
			rd = GiftCodeUtil.getXMLDocument(xmlStringResp);
		}
		catch (IllegalStateException ex) {
			if(ex.getMessage().contains("204")) {
				Element errors = (Element) displayDocument
						.createElement(ARConstants.XML_ERROR_TOP);
				root.appendChild(errors);
				Element error = (Element) displayDocument
						.createElement(ARConstants.XML_ERROR_BOTTOM);
				if (requestNumber != null && !requestNumber.equals("")) {
					error.setAttribute(ARConstants.XML_ATTR_FIELD,
							"gcc_request_number");
					error.setAttribute(ARConstants.XML_ATTR_VALUE,
							"Invalid Request Number");
				} else {
					error.setAttribute(ARConstants.XML_ATTR_FIELD,
							"gcc_coupon_number");
					error.setAttribute(ARConstants.XML_ATTR_VALUE,
							"Invalid Coupon Number");
				}
				errors.appendChild(error);

				Element fields = (Element) displayDocument
						.createElement(ARConstants.XML_FIELD_TOP);
				root.appendChild(fields);
				fields.appendChild(getNameElement(displayDocument,
						"gcc_request_number", requestNumber));
				fields.appendChild(getNameElement(displayDocument,
						"gcc_coupon_number", couponNumber));
				root.setAttribute(ARConstants.FORWARD_STATE,
						ARConstants.COMMON_VALUE_NO);
				logger.debug("Exiting handleSearch with error...");
				return displayDocument;
			} 
			root.setAttribute(ARConstants.CONST_SUCCESS_MSG, ex.getMessage());
			root.setAttribute(ARConstants.FORWARD_STATE,
					ARConstants.COMMON_VALUE_YES);
			return displayDocument;
		}
		
		
		String gccType = getStringValueFromXML(rd, "gc_coupon_type");

		
		String isMultiple = "";
		if (requestNumber != null && !requestNumber.equals("")) {
			displayDocument.getDocumentElement().appendChild(
					displayDocument.importNode(getRequestDataFromXML(rd)
							.getDocumentElement(), true));

			getBatchStatusCount(displayDocument, rd);
			root.setAttribute("sub_header_name", "Request Number #:");
			displayDocument.getDocumentElement();
			root.setAttribute("sub_header_value", requestNumber);
			isMultiple = "Y";
		} else if (couponNumber != null && !couponNumber.equals("")) {
			displayDocument.getDocumentElement().appendChild(
					displayDocument.importNode(
							getRequestDataXMLSingle(rd, couponNumber)
									.getDocumentElement(), true));
			displayDocument.getDocumentElement().appendChild(
					displayDocument.importNode(
							getRedemDateAndOrderDetailId(xmlStringResp)
									.getDocumentElement(), true));
			root.setAttribute("sub_header_name", "Gift Certificate/Coupon #:");
			displayDocument.getDocumentElement();
			root.setAttribute("sub_header_value", couponNumber);
			isMultiple = "N";
		}
		displayDocument.getDocumentElement().appendChild(
				displayDocument.importNode(
						getStatusList(request, isMultiple, gccType)
								.getDocumentElement(), true));
		displayDocument = getDropDownValues(displayDocument, request);
		root = displayDocument.getDocumentElement();
		root.setAttribute(ARConstants.FORWARD_STATE,
				ARConstants.COMMON_VALUE_YES);
		logger.debug("Exiting handleSearch...");
		return displayDocument;
	}

	/**
	 * This method checks for the order number passed for its existency. This
	 * will only happen when coupon status selected is Redeemed
	 * 
	 * @param vo
	 * @return Document
	 * @throws java.lang.Exception
	 */
	private Document validateSingleUpdate(GiftCertificateCouponVO vo)
			throws Exception {
		logger.debug("Entering validateSingleUpdate...");
		Document errorDoc = null;
		if (null != vo.getGcCouponStatus()
				&& vo.getGcCouponStatus().equals("Redeemed")) {
			if (!gccUtilDAO.orderExists(vo.getGcOrderNumber())) {
				errorDoc = DOMUtil.getDefaultDocument();
				Element errors = (Element) errorDoc
						.createElement(ARConstants.XML_ERROR_TOP);
				errorDoc.appendChild(errors);
				Element error = (Element) errorDoc
						.createElement(ARConstants.XML_ERROR_BOTTOM);
				error.setAttribute(ARConstants.XML_ATTR_FIELD,
						"gcc_order_number");
				error.setAttribute(ARConstants.XML_ATTR_VALUE,
						"Invalid Order Number");
				errors.appendChild(error);
			}
		}
		logger.debug("Exiting validateSingleUpdate...");
		return errorDoc;
	}

	/**
	 * This method populates the value from Gift Certificate Screen to
	 * GiftCertificateCouponVO
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @return GiftCertificateCouponVO
	 */
	private GiftCertificateCouponVO getGiftCertificateCouponVO(
			HttpServletRequest request) throws Exception {
		logger.debug("Entering getGiftCertificateCouponVO...");
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");

		GiftCertificateCouponVO vo = new GiftCertificateCouponVO();
		String requestNumber = request.getParameter("gcc_request_number");
		if (requestNumber != null && !"".equals(requestNumber)) {
			requestNumber = requestNumber.toUpperCase();
		}
		vo.setRequestNumber(requestNumber);
		vo.setGcCouponType(request.getParameter("gcc_type"));

		String securityToken = request
				.getParameter(ARConstants.COMMON_PARM_SEC_TOKEN);
		String userId = "";
		if (securityToken != null
				&& SecurityManager.getInstance().getUserInfo(securityToken) != null) {
			userId = SecurityManager.getInstance().getUserInfo(securityToken)
					.getUserID();
		}
		// we set both createdby and updatedby to the current user,
		// and pick the one we need based on if we are creating or updating.
		vo.setCreatedBy(userId);
		vo.setUpdatedBy(userId);
		if (request.getParameter("gcc_issue_amount") != null
				&& request.getParameter("gcc_issue_amount").length() != 0)
			vo.setIssueAmount(new Double(request
					.getParameter("gcc_issue_amount")).doubleValue());

		if (request.getParameter("gcc_paid_amount") != null
				&& request.getParameter("gcc_paid_amount").length() != 0)
			vo.setPaidAmount(new Double(request.getParameter("gcc_paid_amount"))
					.doubleValue());

		if (request.getParameter("gcc_issue_date") != null
				&& request.getParameter("gcc_issue_date").length() != 0) {
			Calendar calIssue = Calendar.getInstance();
			calIssue.setTime(df.parse(request.getParameter("gcc_issue_date")));
			vo.setIssueDate(calIssue);
		}

		if (vo.getGcCouponType().equals("GC"))
			vo.setExpirationDate(null);
		else if (request.getParameter("gcc_expiration_date") != null
				&& request.getParameter("gcc_expiration_date").length() != 0) {
			Calendar calExpire = Calendar.getInstance();
			calExpire.setTime(df.parse(request
					.getParameter("gcc_expiration_date")));
			vo.setExpirationDate(calExpire);
		}

		if (request.getParameter("gcc_quantity") != null
				&& request.getParameter("gcc_quantity").length() != 0)
			vo.setQuantity(new Long(request.getParameter("gcc_quantity"))
					.longValue());

		vo.setFormat(request.getParameter("gcc_format"));
		vo.setFileName(request.getParameter("gcc_filename"));
		vo.setRequestedBy(request.getParameter("gcc_requestor_name"));
		vo.setOrderSource(request.getParameter("gcc_redemption_type"));
		vo.setProgramName(request.getParameter("gcc_program_name"));
		vo.setProgramDesc(request.getParameter("gcc_program_description"));
		vo.setProgramComment(request.getParameter("gcc_comments"));
		vo.setCompanyId(request.getParameter("gcc_company"));
		vo.setPrefix(request.getParameter("gcc_prefix"));
		vo.setOther(request.getParameter("gcc_other"));
		vo.setGcOrderNumber(request.getParameter("gcc_order_number"));
		vo.setPromotionID(request.getParameter("gcc_promotion_id"));
		if (request.getParameter("gcc_partner_program_name") != null
				&& !request.getParameter("gcc_partner_program_name").equals(
						"none")) {
			vo.setGcPartnerProgramName(request
					.getParameter("gcc_partner_program_name"));
		}

		if (request.getParameter("is_multiple_gcc") != null) {
			if (request.getParameter("is_multiple_gcc").equals(
					ARConstants.COMMON_VALUE_YES))
				vo.setGcCouponStatus(request
						.getParameter("gcc_status_multiple"));
			else
				vo.setGcCouponStatus(request.getParameter("gcc_status_single"));
		}
		// Setting status as null when no status is selected for Gift Code
		// service to identify
		if (null != vo.getGcCouponStatus()
				&& vo.getGcCouponStatus().equalsIgnoreCase(STATUS_NONE)) {
			vo.setGcCouponStatus(null);
		}
		if (request.getParameter("gcc_redemption_date") != null
				&& request.getParameter("gcc_redemption_date").length() != 0) {
			Calendar calRedemption = Calendar.getInstance();
			calRedemption.setTime(df.parse(request
					.getParameter("gcc_redemption_date")));
			vo.setGcRedemptionDate(calRedemption);
		}
		if (request.getParameter("gcc_coupon_recip_id") != null
				&& request.getParameter("gcc_coupon_recip_id").length() != 0)
			vo.setGcCouponRecipId(new Long(request
					.getParameter("gcc_coupon_recip_id")).longValue());
		vo.setGcCouponNumber(request.getParameter("gcc_coupon_number"));
		logger.debug("Exiting getGiftCertificateCouponVO...");
		return vo;
	}

	/**
	 * This method creates electronic file for create Gift Certificate Coupon if
	 * the format selected is electronic
	 * 
	 * @param vo
	 * @param fileName
	 * @param displayMessage
	 * @return String
	 * @throws java.lang.Exception
	 */
	private String createElectronicFile(GiftCertificateCouponVO vo,
			String fileName, String displayMessage) throws Exception {
		logger.debug("Entering createElectronicFile...");
		Document recipDoc = gccRecDAO.doGetCouponRecipient(vo
				.getRequestNumber());
		ArrayList<GccRecipientVO> recVOList = getRecipientDataFromXML(recipDoc);
		try {
			createFile(recVOList, vo, fileName);
		} catch (FileNotFoundException fnfe) {
			vo.setFileName("");
			gccDAO.doUpdateGcCouponRequest(vo);
			displayMessage = "Request Created. Error In File Creation";
		}
		logger.debug("Exiting createElectronicFile...");
		return displayMessage;
	}

	/**
	 * This method gets the redeemtion date and order detail id for gift
	 * certificate coupon history table.
	 * 
	 * @param couNumber
	 * @return Document
	 * @throws java.lang.Exception
	 */
	public Document getRedemDateAndOrderDetailId(String xmlStringResp)
			throws Exception {
		logger.debug("Entering getRedemDateAndOrderDetailId...");
		String redemptionDates = "";
		String orderIds = "";
		Document couDoc = GiftCodeUtil.getXMLDocument(xmlStringResp);
		NodeList redemptionList = couDoc
				.getElementsByTagName("redemption_date");
		if (null != redemptionList && redemptionList.getLength() > 0) {
			Node redemptionNode = redemptionList.item(0);
			NodeList redeemDateNodeList = redemptionNode.getChildNodes();
			for (int count = 0; count < redeemDateNodeList.getLength(); count++) {
				String value = "";
				Node redemptionInnerNode = redeemDateNodeList.item(count);
				if (redemptionInnerNode.getChildNodes().getLength() > 0) {
					value = redemptionInnerNode.getChildNodes().item(0)
							.getNodeValue().trim();
					if (redemptionDates.equals("")) {
						redemptionDates = redemptionDates + value;
					} else {
						redemptionDates = redemptionDates + ", " + value;
					}
				}
			}
		}

		// parses to get order detail ids
		NodeList orderList = couDoc.getElementsByTagName("order_detail_id");
		if (null != orderList && orderList.getLength() > 0) {
			Node orderNode = orderList.item(0);
			NodeList textNodeList = orderNode.getChildNodes();
			for (int s = 0; s < textNodeList.getLength(); s++) {
				String value = "";
				Node childOrderNode = textNodeList.item(s);
				if (childOrderNode.getChildNodes().getLength() > 0) {
					value = childOrderNode.getChildNodes().item(0)
							.getNodeValue().trim();
					if (orderIds.equals("")) {
						orderIds = orderIds + value;
					} else {
						orderIds = orderIds + ", " + value;
					}
				}
			}
		}

		// creates a Document with above paresd values
		Document fieldDoc = DOMUtil.getDefaultDocument();
		Element fields = (Element) fieldDoc.createElement("GC_COUPON_HISTORYS");
		fieldDoc.appendChild(fields);

		Element field = (Element) fieldDoc.createElement("GC_COUPON_HISTORY");
		fields.appendChild(field);

		Element redemptionDate = (Element) fieldDoc
				.createElement("redemption_date");
		field.appendChild(redemptionDate);
		Text textNodeR = fieldDoc.createTextNode(redemptionDates);
		redemptionDate.appendChild(textNodeR);

		Element orderId = (Element) fieldDoc.createElement("order_detail_id");
		field.appendChild(orderId);
		Text textNodeO = fieldDoc.createTextNode(orderIds);
		orderId.appendChild(textNodeO);
		logger.debug("Exiting getRedemDateAndOrderDetailId...");
		return fieldDoc;
	}

	/**
	 * This method creates an field doc for result of the search page by coupon
	 * number
	 * 
	 * @param rd
	 * @param couponNumber
	 * @return Document
	 * @throws java.lang.Exception
	 */
	private Document getRequestDataXMLSingle(Document rd, String couponNumber)
			throws Exception {
		logger.debug("Entering getRequestDataXMLSingle...");
		Document fieldDoc = getRequestDataFromXML(rd);
		Element fields = fieldDoc.getDocumentElement();
		fields.appendChild(getNameElement(fieldDoc,
				"gcc_status_single_display", getSearchStatusSingle(rd)));
		logger.debug("Exiting getRequestDataXMLSingle...");
		return fieldDoc;
	}

	/**
	 * This method gets the gc_coupon_status for the result of the search page
	 * by coupon number
	 * 
	 * @param couponNumber
	 * @return String
	 * @throws java.lang.Exception
	 */
	public String getStatusSingle(String couponNumber, String company)
			throws Exception {
		logger.debug("Entering getStatusSingle...");
		GiftCodeUtil util = GiftCodeUtil.getInstance();
		String value = "";
		// Document statusDoc = gccDAO.doSearchGcCoupon(null, couponNumber);
		String xmlresponse = util.getSingleStatusGiftCertificate(couponNumber,
				company);
		Document statusDoc = GiftCodeUtil.getXMLDocument(xmlresponse);
		// parses to get status
		NodeList statusList = statusDoc
				.getElementsByTagName("gc_coupon_status");
		for (int s = 0; s < statusList.getLength(); s++) {
			Element statusNode = (Element) statusList.item(s);
			NodeList textNodeList = statusNode.getChildNodes();
			if (textNodeList.getLength() != 0)
				value = ((Node) textNodeList.item(0)).getNodeValue().trim();
		}
		logger.debug("Exiting getStatusSingle...");
		return value;
	}

	public String getSearchStatusSingle(Document rd) throws Exception {
		logger.debug("Entering getStatusSingle...");
		String value = "";
		// Document statusDoc = gccDAO.doSearchGcCoupon(null, couponNumber);
		// parses to get status
		NodeList statusList = rd.getElementsByTagName("gc_coupon_status");
		for (int s = 0; s < statusList.getLength(); s++) {
			Element statusNode = (Element) statusList.item(s);
			NodeList textNodeList = statusNode.getChildNodes();
			if (textNodeList.getLength() != 0)
				value = ((Node) textNodeList.item(0)).getNodeValue().trim();
		}
		logger.debug("Exiting getStatusSingle...");
		return value;
	}

	/**
	 * This method parses the recipeint info XML to get the recipient data and
	 * store it in a list of GccRecipientVO bean
	 * 
	 * @param recipDoc
	 * @return ArrayList
	 * @throws java.lang.Exception
	 */
	private ArrayList<GccRecipientVO> getRecipientDataFromXML(Document recipDoc)
			throws Exception {
		ArrayList<GccRecipientVO> listRecipVo = new ArrayList<GccRecipientVO>();
		logger.debug("Entering getRecipientDataFromXML...");
		NodeList listOfRecipients = recipDoc
				.getElementsByTagName("GC_COUPON_RECIPIENT");
		for (int s = 0; s < listOfRecipients.getLength(); s++) {
			GccRecipientVO vo = new GccRecipientVO();
			Node firstRecipientNode = listOfRecipients.item(s);
			if (firstRecipientNode.getNodeType() == Node.ELEMENT_NODE) {
				Element firstRecipientElement = (Element) firstRecipientNode;

				vo.setGcCouponNumber(getStringValueFromXML(
						firstRecipientElement, "gc_coupon_number"));
				String recipId = getStringValueFromXML(firstRecipientElement,
						"gc_coupon_recip_id");
				if (recipId != null)
					vo.setGcCouponReceiptId(new Long(recipId).longValue());
				vo.setRequestNumber(getStringValueFromXML(
						firstRecipientElement, "request_number"));
				vo.setFirstName(getStringValueFromXML(firstRecipientElement,
						"first_name"));
				vo.setLastName(getStringValueFromXML(firstRecipientElement,
						"last_name"));
				vo.setAddress1(getStringValueFromXML(firstRecipientElement,
						"address_1"));
				vo.setAddress2(getStringValueFromXML(firstRecipientElement,
						"address_2"));
				vo.setCity(getStringValueFromXML(firstRecipientElement, "city"));
				vo.setState(getStringValueFromXML(firstRecipientElement,
						"state"));
				vo.setZipCode(getStringValueFromXML(firstRecipientElement,
						"zip_code"));
				vo.setCountry(getStringValueFromXML(firstRecipientElement,
						"country"));
				vo.setPhone(getStringValueFromXML(firstRecipientElement,
						"phone"));
				vo.setEmailAddress(getStringValueFromXML(firstRecipientElement,
						"email_address"));
				vo.setIssuedReferenceNumber(getStringValueFromXML(firstRecipientElement,
						"issuedReferenceNumber"));
			}
			listRecipVo.add(vo);
		}
		logger.debug("Exiting getRecipientDataFromXML...");
		return listRecipVo;
	}

	/**
	 * This method parses the request info XML to get the request data and store
	 * it in a field doc
	 * 
	 * @param requestDoc
	 * @return Document
	 * @throws java.lang.Exception
	 */
	private Document getRequestDataFromXML(Document requestDoc)
			throws Exception {
		Document fieldDoc = DOMUtil.getDefaultDocument();
		logger.debug("Entering getRequestDataFromXML...");
		NodeList requestNode = requestDoc
				.getElementsByTagName("GC_COUPON_REQUEST");
		Node firstRequestNode = requestNode.item(0);
		ConfigurationUtil configUtil = ConfigurationUtil.getInstance();

		if (firstRequestNode.getNodeType() == Node.ELEMENT_NODE) {

			Element firstRequestElement = (Element) firstRequestNode;

			// NodeList statusCountNode =
			// firstRequestElement.getElementsByTagName("statusCount");

			Element fields = (Element) fieldDoc
					.createElement(ARConstants.XML_FIELD_TOP);

			fieldDoc.appendChild(fields);

			// look up format and redemption type from config file.
			String format = getStringValueFromXML(firstRequestElement, "format");
			String redemptionType = getStringValueFromXML(firstRequestElement,
					"order_source");
			format = configUtil.getProperty(ARConstants.CONFIG_FILE,
					ARConstants.FORMAT_PREFIX + format);
			redemptionType = configUtil.getProperty(ARConstants.CONFIG_FILE,
					ARConstants.REDEEM_TYPE_PREFIX + redemptionType);

			fields.appendChild(getNameElement(
					fieldDoc,
					"gcc_coupon_number",
					getStringValueFromXML(firstRequestElement,
							"gc_coupon_number")));
			fields.appendChild(getNameElement(
					fieldDoc,
					"gcc_request_number",
					getStringValueFromXML(firstRequestElement, "request_number")));
			fields.appendChild(getNameElement(
					fieldDoc,
					"gcc_coupon_recip_id",
					getStringValueFromXML(firstRequestElement,
							"gc_coupon_recip_id")));
			fields.appendChild(getNameElement(
					fieldDoc,
					"gcc_type",
					getStringValueFromXML(firstRequestElement, "gc_coupon_type")));
			fields.appendChild(getNameElement(fieldDoc, "gcc_issue_amount",
					getStringValueFromXML(firstRequestElement, "issue_amount")));
			fields.appendChild(getNameElement(fieldDoc, "gcc_issue_date",
			/*
			 * formatTimestampToDate(getStringValueFromXML( firstRequestElement,
			 * "issue_date"))));
			 */
			getStringValueFromXML(firstRequestElement, "issue_date")));
			fields.appendChild(getNameElement(fieldDoc, "gcc_paid_amount",
					getStringValueFromXML(firstRequestElement, "paid_amount")));
			fields.appendChild(getNameElement(fieldDoc, "gcc_expiration_date",
			/*
			 * formatTimestampToDate(getStringValueFromXML( firstRequestElement,
			 * "expiration_date"))));
			 */
			getStringValueFromXML(firstRequestElement, "expiration_date")));

			/*
			 * NodeList statusRequestNodeList = firstRequestElement
			 * .getElementsByTagName("GC_COUPONS");
			 * logger.debug("Status Count List Size(GC_COUPONS) = " +
			 * statusRequestNodeList.getLength());
			 */
			/*
			 * Element statusElelmentFields = (Element) fieldDoc
			 * .createElement("GC_COUPONS"); fieldDoc.insertBefore(fields,
			 * statusElelmentFields);
			 */

			/*
			 * if(statusRequestNodeList.getLength() > 0){ Element nodeElement =
			 * (Element) statusRequestNodeList.item(0); NodeList couponStausList
			 * = nodeElement .getElementsByTagName("GC_COUPON"); for (int
			 * iterCount = 0; iterCount < couponStausList.getLength();
			 * iterCount++) { Element coupon = (Element)
			 * couponStausList.item(iterCount);
			 * statusElelmentFields.appendChild(
			 * getStatusElement(fieldDoc,coupon)); } }
			 */

			// firstRequestElement
			/*
			 * if (statusRequestNodeList.getLength() > 0) {
			 * 
			 * Element nodeElement = (Element) statusRequestNodeList.item(0);
			 * NodeList couponStausList = nodeElement
			 * .getElementsByTagName("GC_COUPON");
			 * logger.debug("Status Count List Size = " +
			 * couponStausList.getLength()); for (int iterCount = 0; iterCount <
			 * couponStausList.getLength(); iterCount++) {
			 * 
			 * Element coupon = (Element) couponStausList.item(iterCount);
			 * statusElelmentFields.appendChild(arg0) logger.debug("coupon = " +
			 * coupon.toString()); logger.debug("coupon= " +
			 * coupon.getNodeName()); logger.debug("Accesing Coupon Element = "
			 * + iterCount + " Size = " + coupon.getChildNodes().getLength());
			 * 
			 * NodeList textNodeList = coupon.getChildNodes(); if
			 * (textNodeList.getLength() == 0) { return null; } else {
			 * logger.debug("Key = " + getStringValueFromXML(coupon,
			 * "gc_coupon_status") + " Value = " + getStringValueFromXML(coupon,
			 * "status_count"));
			 * 
			 * displayDocument.getDocumentElement() .appendChild(
			 * displayDocument.importNode( gccCount.getDocumentElement(),
			 * true)); fieldDoc.appendChild(coupon); } // return ((Node) //
			 * textNodeList.item(0)).getNodeValue().trim();
			 * 
			 * 
			 * Node statusRequestNode = requestNode.item(iterCount); logger
			 * .debug("8273747:"+statusRequestNode.getLocalName()); logger
			 * .debug("*#&#&:"+statusRequestNode.getChildNodes().getLength ());
			 * fields.appendChild(getNameElement( fieldDoc, "gcc_coupon_number",
			 * getStringValueFromXML(firstRequestElement, "gc_coupon_number")));
			 * 
			 * } }
			 */

			fields.appendChild(getNameElement(fieldDoc, "gcc_quantity",
					getStringValueFromXML(firstRequestElement, "quantity")));
			fields.appendChild(getNameElement(fieldDoc, "gcc_format", format));
			fields.appendChild(getNameElement(fieldDoc, "gcc_filename",
					getStringValueFromXML(firstRequestElement, "filename")));
			fields.appendChild(getNameElement(fieldDoc, "gcc_requestor_name",
					getStringValueFromXML(firstRequestElement, "requested_by")));
			fields.appendChild(getNameElement(fieldDoc, "gcc_redemption_type",
					redemptionType));
			fields.appendChild(getNameElement(fieldDoc, "gcc_program_name",
					getStringValueFromXML(firstRequestElement, "program_name")));
			fields.appendChild(getNameElement(fieldDoc,
					"gcc_program_description",
					getStringValueFromXML(firstRequestElement, "program_desc")));
			fields.appendChild(getNameElement(
					fieldDoc,
					"gcc_comments",
					getStringValueFromXML(firstRequestElement,
							"program_comment")));
			fields.appendChild(getNameElement(fieldDoc, "gcc_company",
					getStringValueFromXML(firstRequestElement, "company_id")));
			// fields.appendChild(getNameElement(fieldDoc,"gcc_source_code",getStringValueFromXML(firstRequestElement,"source_code")));
			fields.appendChild(getNameElement(fieldDoc, "gcc_prefix",
					getStringValueFromXML(firstRequestElement, "gcc_prefix")));
			fields.appendChild(getNameElement(fieldDoc, "gcc_other",
					getStringValueFromXML(firstRequestElement, "gcc_other")));
			fields.appendChild(getNameElement(fieldDoc, "gcc_promotion_id",
					getStringValueFromXML(firstRequestElement, "gcc_promotion_id")));
			fields.appendChild(getNameElement(
					fieldDoc,
					"gcc_partner_program_name",
					getStringValueFromXML(firstRequestElement,
							"gc_partner_program_name")));
			fields.appendChild(getNameElement(fieldDoc, "gcc_creation_date",
					getStringValueFromXML(firstRequestElement, "created_on")));
			StringBuilder srcCodeStr = new StringBuilder();
			/*
			 * List<String> srcCodeList = getRestrictedSrcCodesFromXML(gccDAO
			 * .doGetRestrictedSrcCodesByReqNum(getStringValueFromXML(
			 * firstRequestElement, "request_number")));
			 * 
			 * for (String srcCode : srcCodeList) {
			 * srcCodeStr.append(" ").append(srcCode); }
			 */

			NodeList sourceList = firstRequestElement
					.getElementsByTagName("gcc_source_code");
			if (null != sourceList && sourceList.getLength() > 0) {
				Node sourceNode = sourceList.item(0);
				NodeList sourceNodeList = sourceNode.getChildNodes();
				for (int count = 0; count < sourceNodeList.getLength(); count++) {
					String value = new String();
					Node sourceInnerNode = sourceNodeList.item(count);
					if (sourceInnerNode.getChildNodes().getLength() > 0) {
						value = sourceInnerNode.getChildNodes().item(0)
								.getNodeValue().trim();
						if (srcCodeStr.length() < 1) {
							srcCodeStr = srcCodeStr.append(value.trim());
						} else {
							srcCodeStr = srcCodeStr.append(", ").append(value);
						}
					}
				}
			}
			fields.appendChild(getNameElement(fieldDoc, "gcc_source_code",
					srcCodeStr.toString()));
			
			
		}
		logger.debug("Exiting getRequestDataFromXML...");
		return fieldDoc;
	}

	public static void inspect(Node fields) {
		_inspect(fields, 0);
	}

	private static void _inspect(Node fields, int depth) {
		logger.debug(prettyPrint(fields, depth));
		NodeList childNodes = fields.getChildNodes();
		for(int i=0; i< childNodes.getLength(); i++) {
			Node node = childNodes.item(i);
			_inspect(node, depth+1);
		}
	}

	private static String prettyPrint(Node fields, int depth) {
		StringBuilder sb = new StringBuilder();
		for(int i=0;i<depth;i++) {
			sb.append("..");
		}
		
		if (fields.getNodeType() == Node.ELEMENT_NODE) {
			sb.append("<");
			sb.append(fields.getNodeName());
			NamedNodeMap attrs = fields.getAttributes();
			for(int i=0; i<attrs.getLength(); i++) {
				sb.append(attrs.item(i).getNodeName() + "=" + attrs.item(i).getNodeValue());
			}
			sb.append(">");
		}
		if (fields.getNodeType() == Node.TEXT_NODE) {
			sb.append(fields.getNodeValue());
		}
		return sb.toString();
	}

	/**
	 * This method gets the value of the tagname passed for that element
	 * 
	 * @param parentElement
	 *            Element
	 * @param tagName
	 *            String
	 * @return String
	 * @throws java.lang.Exception
	 */
	private String getStringValueFromXML(Element parentElement, String tagName)
			throws Exception {
		logger.debug("Entering getStringValueFromXML...");
		NodeList nodeList = parentElement.getElementsByTagName(tagName);
		if (nodeList.getLength() == 0)
			return null;
		Element nodeElement = (Element) nodeList.item(0);
		NodeList textNodeList = nodeElement.getChildNodes();
		if (textNodeList.getLength() == 0)
			return null;
		return ((Node) textNodeList.item(0)).getNodeValue().trim();
	}

	/**
	 * This method gets the value of the tagname passed for that element
	 * 
	 * @param parentElement
	 *            Document
	 * @param tagName
	 *            String
	 * @return String
	 * @throws java.lang.Exception
	 */
	private String getStringValueFromXML(Document parentElement, String tagName)
			throws Exception {
		logger.debug("Entering getStringValueFromXML...");
		if (null != tagName && !tagName.isEmpty()) {
			NodeList nodeList = parentElement.getElementsByTagName(tagName);
			if (nodeList.getLength() == 0)
				return null;
			Element nodeElement = (Element) nodeList.item(0);
			NodeList textNodeList = nodeElement.getChildNodes();
			if (textNodeList.getLength() == 0)
				return null;
			return ((Node) textNodeList.item(0)).getNodeValue().trim();
		}
		return new String();
	}

	/**
	 * This method creates a file and stores the recipient data in the format
	 * specified in business requirements
	 * 
	 * @param recipList
	 *            ArrayList
	 * @param vo
	 *            GiftCertificateCouponVO
	 * @return String
	 * @throws java.lang.Exception
	 */
	private String createFile(ArrayList<GccRecipientVO> recipList,
			GiftCertificateCouponVO vo, String fileName) throws Exception {
		String ia = vo.getIssueAmount() + "";
		logger.debug("Entering createFile...");
		String ed = nullString;
		if (vo.getExpirationDate() != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
			ed = sdf.format(vo.getExpirationDate().getTime());
		}

		fileName = ConfigurationUtil.getInstance()
				.getProperty(ARConstants.CONFIG_FILE,
						ARConstants.CONST_ELECTRONIC_FILE_PATH)
				+ fileName + ".csv";
		File outputFile = new File(fileName);
		FileWriter out = new FileWriter(outputFile);

		String programName = vo.getProgramName() == null ? nullString : vo
				.getProgramName();
		out.write("Program Name," + programName + newLineChar);
		String company = vo.getCompanyId() == null ? nullString : vo
				.getCompanyId();
		out.write("Company," + company + newLineChar);
		StringBuilder srcCodeStr = new StringBuilder();
		if (vo.getSourceCodeVO() != null) {

			List<String> srcCodeList = vo.getSourceCodeVO()
					.getRestrictedSourceCodes();
			String separator = " ";
			for (String srcCode : srcCodeList) {
				srcCodeStr.append(separator).append(srcCode);
			}

		}

		out.write("Source Code," + srcCodeStr + newLineChar);

		StringBuffer sb = new StringBuffer();
		sb.append("Gift Certificate/Coupon Number,");
		sb.append("Issue Amount,");
		sb.append("Expiration Date (Coupon Only),");
		sb.append("Recipient First Name,");
		sb.append("Recipient Last Name,");
		sb.append("Recipient Adddress 1,");
		sb.append("Recipient Address 2,");
		sb.append("Recipient City,");
		sb.append("Recipient State,");
		sb.append("Recipient Zip Code,");
		sb.append("Recipient Country,");
		sb.append("Recipient Phone,");
		sb.append("Recipient Email Address");
		sb.append(newLineChar);
		out.write(sb.toString());
		for (int i = 0; i < recipList.size(); i++) {
			GccRecipientVO rvo = (GccRecipientVO) recipList.get(i);
			out.write(getLineData(rvo, ia, ed));
		}
		out.close();

		logger.debug("filename created is " + fileName);
		return fileName;
	}

	/**
	 * This method gets the line data in comma delimited format
	 * 
	 * @param vo
	 * @param ia
	 * @param ed
	 * @return String
	 */
	private String getLineData(GccRecipientVO vo, String ia, String ed) {
		logger.debug("Entering getLineData...");
		ArrayList<String> colArray = new ArrayList<String>();
		colArray.add(vo.getGcCouponNumber());
		colArray.add(ia == null ? nullString : ia);
		colArray.add(ed == null ? nullString : ed);
		colArray.add(vo.getFirstName() == null ? nullString : vo.getFirstName());
		colArray.add(vo.getLastName() == null ? nullString : vo.getLastName());
		colArray.add(vo.getAddress1() == null ? nullString : vo.getAddress1());
		colArray.add(vo.getAddress1() == null ? nullString : vo.getAddress2());
		colArray.add(vo.getCity() == null ? nullString : vo.getCity());
		colArray.add(vo.getState() == null ? nullString : vo.getState());
		colArray.add(vo.getZipCode() == null ? nullString : vo.getZipCode());
		colArray.add(vo.getCountry() == null ? nullString : vo.getCountry());
		colArray.add(vo.getPhone() == null ? nullString : vo.getPhone());
		colArray.add(vo.getEmailAddress() == null ? nullString : vo
				.getEmailAddress());
		logger.debug("Exiting getLineData...");
		return (new LineItemGenerator()).getDelimitedString(colArray, ",")
				+ newLineChar;
	}

	/**
	 * This method creates the field doc fot the values to be send to the user
	 * 
	 * @param request
	 * @param vo
	 * @return Document
	 * @throws java.lang.Exception
	 */
	private Document createFieldDoc(HttpServletRequest request,
			GiftCertificateCouponVO vo) throws Exception {
		GiftCertActionHelper actionHelper = new GiftCertActionHelper();
		Document fieldDoc = actionHelper.createFieldDoc(request);
		String company = request.getParameter("gcc_company");
		if (null != company) {
			company = company.trim().toLowerCase();
		} else {
			company = ARConstants.FTD_COMPANY_ID;
		}
		Element fields = (Element) (fieldDoc
				.getElementsByTagName(ARConstants.XML_FIELD_TOP)).item(0);

		if (request.getParameter(ARConstants.ACTION_PARM_IS_MULTIPLE_GCC) != null
				&& request
						.getParameter(ARConstants.ACTION_PARM_IS_MULTIPLE_GCC)
						.equals(ARConstants.COMMON_VALUE_NO)) {
			fields.appendChild(getNameElement(
					fieldDoc,
					"gcc_status_single_display",
					getStatusSingle(request.getParameter("gcc_coupon_number"),
							company)));
		}

		// replace file name element
		Element filenameElem = findElementByAttr(
				fieldDoc.getElementsByTagName(ARConstants.XML_FIELD_BOTTOM),
				"fieldname", "gcc_filename");
		if (filenameElem != null) {
			fields.removeChild(filenameElem);
			fields.appendChild(getNameElement(fieldDoc, "gcc_filename",
					vo.getFileName()));
		}
		return fieldDoc;
	}

	/**
	 * This method sets the attributes value to field element of field doc
	 * 
	 * @param fieldDoc
	 * @param attrName
	 * @param attrValue
	 * @return
	 */
	private Element getNameElement(Document fieldDoc, String attrName,
			String attrValue) {
		logger.debug("Entering getNameElement...");
		Element field = (Element) fieldDoc
				.createElement(ARConstants.XML_FIELD_BOTTOM);
		field.setAttribute(ARConstants.XML_ATTR_FIELD, attrName);
		field.setAttribute(ARConstants.XML_ATTR_VALUE, attrValue);
		logger.debug("Exiting getNameElement...");
		return field;
	}

	/*
	 * private Element getStatusElement(Document statusDoc, Element coupon)
	 * throws DOMException, Exception {
	 * logger.debug("Entering getStatusElement..."); Element field =
	 * statusDoc.getDocumentElement(); // fieldDoc.im
	 * field.appendChild(statusDoc.importNode(coupon, true));
	 * 
	 * field.appendChild(statusDoc.importNode(getStatusStringValueFromXML(coupon
	 * , "status_count"),true));
	 * 
	 * 
	 * coupon.appendChild(getStatusNode)
	 * field.setAttribute(ARConstants.XML_ATTR_FIELD, attrName);
	 * field.setAttribute(ARConstants.XML_ATTR_VALUE, attrValue);
	 * 
	 * logger.debug("Exiting getStatusElement..."); return field; }
	 */

	/**
	 * This method validates the input requestNumber for duplicancy, issue
	 * amount against max cs issue amount and for valid source code
	 * 
	 * @param vo
	 *            GiftCertificateCoupon
	 * @return Document
	 * @throws java.lang.Exception
	 */
	private Document validateNewGiftCertificateCoupon(GiftCertificateCouponVO vo)
			throws Exception {
		Document errorDoc = null;
		logger.debug("Entering validateNewGiftCertificateCoupon...");

		if (vo.getGcCouponType().equals(ARConstants.CUSTOMER_SERVICE)) {
			if (checkMaxIssueAmount(new Double(vo.getIssueAmount())
					.doubleValue())) {
				errorDoc = generateErrorDocument("gcc_issue_amount", "Value entered exceeds max value allowed.", errorDoc);
			}
		}

		logger.debug("Exiting validateNewGiftCertificateCoupon...");
		return errorDoc;
	}

	private Document generateErrorDocument(String fieldName, String errorMessage, Document errorDoc) throws ParserConfigurationException {
		Element errors = null;
		if (errorDoc == null) {
			errorDoc = DOMUtil.getDefaultDocument();
			errors = (Element) errorDoc.createElement(ARConstants.XML_ERROR_TOP);
		} else {
			errors = errorDoc.getDocumentElement();
		}
		errorDoc.appendChild(errors);
		Element error = (Element) errorDoc.createElement(ARConstants.XML_ERROR_BOTTOM);
		error.setAttribute(ARConstants.XML_ATTR_FIELD, fieldName);
		error.setAttribute(ARConstants.XML_ATTR_VALUE, errorMessage);
		errors.appendChild(error);
		return errorDoc;
	}

	/**
	 * This method checks for the Issue Amount against Max CS Issue Amount
	 * 
	 * @param issueAmt
	 *            double
	 * @return boolean
	 */
	private boolean checkMaxIssueAmount(double issueAmt) throws Exception {
		logger.debug("Entering checkMaxIssueAmount...");
		double maxCSAmount = Double
				.valueOf(
						gccDAO.doGetGlobalParamValue(ARConstants.CONST_MAX_CS_ISSUE_AMOUNT))
				.doubleValue();
		logger.debug("issueAmt: " + issueAmt);
		logger.debug("maxCSAmount: " + maxCSAmount);
		logger.debug("Exiting checkMaxIssueAmount...");
		return (issueAmt > maxCSAmount);
	}

	/**
	 * This method checks for valid source code
	 * 
	 * @param companyId
	 *            String
	 * @param sourceCode
	 *            String
	 * @return boolean
	 * @throws Exception
	 */
	/*
	 * private boolean checkValidSourceCode(String companyId, String sourceCode)
	 * throws Exception { logger.debug("Entering  checkValidSourceCode...");
	 * return checkValueExists(
	 * gccUtilDAO.doGetSourceCodeListByCompanyId(companyId), sourceCode); }
	 */

	/**
	 * gets the next request Number
	 * 
	 * @return String
	 * @throws java.lang.Exception
	 */
	private String getNextRequestNumber(String in_prefix) throws Exception {
		logger.debug("Entering getNextRequestNumber...");
		return gccDAO.doGetNextRequestNumber(in_prefix);
	}

	/**
	 * gets the gift certificate coupon types
	 * 
	 * @return Document
	 * @throws java.lang.Exception
	 */
	private Document getGcCouponTypes(HttpServletRequest request)
			throws Exception {
		logger.debug("Entering getGcCouponTypes...");

		Document gccTypes = gccDAO.doGetGCCouponTypes();
		AccountingUtil acctUtil = new AccountingUtil();
		acctUtil.operationsCouponTypes(gccTypes, request);

		return gccTypes;
	}

	/**
	 * gets the Company Names
	 * 
	 * @return Document
	 * @throws java.lang.Exception
	 */
	private Document getCompanyNames() throws Exception {
		logger.debug("Entering getCompanyNames...");
		return gccUtilDAO.doGetCompanyNames();
	}

	private Document getPartnerProgramNames() throws Exception {
		logger.debug("Entering getCompanyNames...");
		return gccUtilDAO.doListPartnerPrograms();
	}

	/**
	 * This method parses the document to check the existenece of value passed
	 * in the document
	 * 
	 * @param docXML
	 * @param value
	 * @return boolean
	 * @throws java.lang.Exception
	 */
	public boolean checkValueExists(Document docXML, String value)
			throws Exception {
		logger.debug("Entering checkValueExists...");
		int num = docXML.getChildNodes().item(0).getChildNodes().getLength();
		Element x1 = (Element) docXML.getChildNodes().item(0);
		for (int i = 0; i < num; i++) {
			Element x2 = (Element) x1.getChildNodes().item(i);
			Element x3 = (Element) x2.getFirstChild();
			String title = x3.getFirstChild().getNodeValue();

			if (value.equals(title)) {
				logger.debug("Exiting checkValueExists with true...");
				return true;
			}
		}
		logger.debug("Exiting checkValueExists with false...");
		return false;
	}

	/**
	 * This method parses the coupon history info XML to get the coupon history
	 * data and store it in a list of GiftCertificateCouponVO bean
	 * 
	 * @param couponHistoryDoc
	 * @return ArrayList
	 * @throws java.lang.Exception
	 */
	/*
	 * private ArrayList getCouponHistoryDataFromXML(Document couponHistoryDoc)
	 * throws Exception {
	 * logger.debug("Entering getCouponHistoryDataFromXML..."); ArrayList
	 * listCouponHistoryVo = new ArrayList();
	 * 
	 * NodeList listOfHistory = couponHistoryDoc
	 * .getElementsByTagName("GC_COUPON_HISTORY"); for (int s = 0; s <
	 * listOfHistory.getLength(); s++) { GiftCertificateCouponVO vo = new
	 * GiftCertificateCouponVO(); Node firstHistoryNode = listOfHistory.item(s);
	 * if (firstHistoryNode.getNodeType() == Node.ELEMENT_NODE) { Element
	 * firstHistoryElement = (Element) firstHistoryNode;
	 * vo.setCouponHistoryId(getStringValueFromXML( firstHistoryElement,
	 * "gc_coupon_history_id")); } listCouponHistoryVo.add(vo); }
	 * logger.debug("Exiting getCouponHistoryDataFromXML..."); return
	 * listCouponHistoryVo; }
	 */

	/**
	 * This method gets the getbrop down values, company drop down values and
	 * coupon status drop down values
	 * 
	 * @param displayDocument
	 * @return Document
	 * @throws java.lang.Exception
	 */
	public Document getDropDownValues(Document displayDocument,
			HttpServletRequest request) throws Exception {
		logger.debug("Entering getDropDownValues...");
		// append coupon status doc

		displayDocument.getDocumentElement().appendChild(
				displayDocument.importNode(getGcCouponTypes(request)
						.getDocumentElement(), true));
		// append company names doc
		displayDocument.getDocumentElement().appendChild(
				displayDocument.importNode(getCompanyNames()
						.getDocumentElement(), true));

		// append gc program names doc
		displayDocument.getDocumentElement().appendChild(
				displayDocument.importNode(getPartnerProgramNames()
						.getDocumentElement(), true));
		logger.debug("Exiting getDropDownValues...");

		return displayDocument;
	}

	/**
	 * This method gets the status count for the request number specified. This
	 * method will be executed for multiple gift certificate coupon
	 * 
	 * @param displayDocument
	 * @param requestNumber
	 * @return Document
	 * @throws java.lang.Exception
	 */
	public Document getStatusCount(Document displayDocument,
			String requestNumber) throws Exception {
		logger.debug("Entering getStatusCount...");

		Document gccCount = gccDAO.doGetStatusCount(requestNumber);
		displayDocument.getDocumentElement()
				.appendChild(
						displayDocument.importNode(
								gccCount.getDocumentElement(), true));
		displayDocument.getDocumentElement().setAttribute(
				"status_multiple_count",
				getIssuedActiveReinstateCount(gccCount));
		logger.debug("Exiting getStatusCount...");
		return displayDocument;
	}

	public Document getBatchStatusCount(Document displayDocument, Document rd)
			throws Exception {
		logger.debug("Entering getBatchStatusCount...");
		Document statusDoc = DOMUtil.getDefaultDocument();
		NodeList statusRequestNodes = rd.getElementsByTagName("GC_COUPONS");
		Element statusElelmentFields = (Element) statusDoc
				.createElement("GC_COUPONS");
		statusDoc.appendChild(statusElelmentFields);
		if (statusRequestNodes.getLength() > 0) {
			Element nodeElement = (Element) statusRequestNodes.item(0);
			NodeList couponStatusList = nodeElement
					.getElementsByTagName("GC_COUPON");
			for (int iterCount = 0; iterCount < couponStatusList.getLength(); iterCount++) {
				Element coupon = (Element) couponStatusList.item(iterCount);
				statusElelmentFields.appendChild(statusDoc.importNode(coupon,
						true));
			}
		}
		logger.debug("Exiting getBatchStatusCount...");
		displayDocument.getDocumentElement()
				.appendChild(
						displayDocument.importNode(
								statusDoc.getDocumentElement(), true));
		displayDocument.getDocumentElement().setAttribute(
				"status_multiple_count",
				getIssuedActiveReinstateCount(statusDoc));
		return displayDocument;
	}

	/**
	 * This method gets the sum of Issued Active and Reinstate status. If the
	 * user in multiple gift certificate and user changes coupon status and
	 * click on save button a small pop up display s saying that you are
	 * updating this many coupons to to the selected coupon status
	 * 
	 * @param gccDoc
	 * @return String
	 * @throws java.lang.Exception
	 */
	private String getIssuedActiveReinstateCount(Document gccDoc)
			throws Exception {
		logger.debug("Entering getIssuedActiveReinstateCount...");
		int statusMultipleCount = 0;
		int count = 0;
		NodeList listOfCoupons = gccDoc.getElementsByTagName("GC_COUPON");
		for (int s = 0; s < listOfCoupons.getLength(); s++) {
			Node firstCouponNode = listOfCoupons.item(s);
			if (firstCouponNode.getNodeType() == Node.ELEMENT_NODE) {
				Element firstCouponElement = (Element) firstCouponNode;
				String countOfStatus = getStringValueFromXML(
						firstCouponElement, "status_count");
				if (null != countOfStatus) {
					count = new Integer(countOfStatus).intValue();
				}
				String status = getStringValueFromXML(firstCouponElement,
						"gc_coupon_status");
				if (null != status
						&& (status.equals("Issued Active") || status
								.equals("Reinstate"))) {
					statusMultipleCount = statusMultipleCount + count;
				}
			}
		}
		logger.debug("Exiting getIssuedActiveReinstateCount...");
		return new Integer(statusMultipleCount).toString();
	}

	/**
	 * This method gets the status list the logic is as follows: if single gift
	 * certificate coupon maintenance and coupon type GC then if the current
	 * status is Issued Active the only status user are allowed to change are
	 * 'Redeemed' or 'Cancelled' if the current status is Redeemed the only
	 * status user is allowed to change is 'Reinstate' if
	 * the current status is Reinstate the only status user are allowes to
	 * change are 'Redeemed' or 'Cancelled' if the current status is cancelled
	 * user are not allowed to change to any other status if single gift
	 * certificate coupon maintenance and coupon type CP or CS then if the
	 * current status is Issued Active the only status user are allowed to
	 * change are 'Redeemed' or 'Cancelled' if the current status is Expired
	 * user are not allowed to change to any other status if the current status
	 * is Redeemed the user is not allowed to change the status
	 * if the current status is cancelled user are not allowed to change to any
	 * other status if multiple gift certificate coupon maintenance and coupon
	 * type GC then if the current status is Issued Active the only status user
	 * are allowed to change is 'Cancelled' if the current status is Redeemed
	 * user are not allowed to change to any other status if the current status
	 * is Reinstate the only status user are allowes to change is 'Cancelled' if
	 * the current status is cancelled user are not allowed to change to any
	 * other status if multiple gift certificate coupon maintenance and coupon
	 * type CP or CS then if the current status is Issued Active the only status
	 * user are allowed to change is 'Cancelled' if the current status is
	 * Expired user are not allowed to change to any other status if the current
	 * status is Redeemed user are not allowed to change to any other status if
	 * the current status is cancelled user are not allowed to change to any
	 * other status
	 * 
	 * @param request
	 * @param isMultiple
	 * @param gccType
	 * @return Document
	 * @throws java.lang.Exception
	 */
	public Document getStatusList(HttpServletRequest request,
			String isMultiple, String gccType) throws Exception {
		logger.debug("Entering getStatusList...");
		ArrayList<String> status = new ArrayList<String>();
		String company = request.getParameter("gcc_company");
		if (null != company) {
			company = company.trim().toLowerCase();
		} else {
			company = ARConstants.FTD_COMPANY_ID;
		}
		if (isMultiple.equals(ARConstants.COMMON_VALUE_YES))
			status.add("Cancelled");
		else {
			String currentStatus = getStatusSingle(
					request.getParameter("gcc_coupon_number"), company);
			if (null != gccType) {
				if (gccType.equals("GC")) {
					if (currentStatus.equals("Issued Active")) {
						status.add("Redeemed");
						status.add("Cancelled");
					} else if (currentStatus.equals("Redeemed")) {
						status.add("Reinstate");
					} else if (currentStatus.equals("Reinstate")) {
						status.add("Redeemed");
						status.add("Cancelled");
					} else if (currentStatus.equals("Cancelled"))
						status.add("");
				} else {
					if (currentStatus.equals("Issued Active")) {
						status.add("Redeemed");
						status.add("Cancelled");
					} else if (currentStatus.equals("Issued Expired"))
						status.add("");
					else if (currentStatus.equals("Redeemed"))
						status.add("");
					else if (currentStatus.equals("Cancelled"))
						status.add("");
				}
			}
		}

		Document fieldDoc = DOMUtil.getDefaultDocument();
		Element fields = (Element) fieldDoc.createElement("GC_COUPON_STATUSS");
		fieldDoc.appendChild(fields);

		Element field = null;
		Element statusElement = null;
		Text statusText = null;
		for (int i = 0; i < status.size(); i++) {
			field = (Element) fieldDoc.createElement("GC_COUPON_STATUS");
			fields.appendChild(field);
			statusElement = (Element) fieldDoc
					.createElement("gc_coupon_status");
			field.appendChild(statusElement);
			statusText = fieldDoc.createTextNode((String) status.get(i));
			statusElement.appendChild(statusText);
		}
		logger.debug("Exiting getStatusList...");
		return fieldDoc;
	}

	/**
	 * Finds and returns the first Element that has an attribute with the given
	 * name value pair.
	 * 
	 * @param list
	 * @param attrName
	 * @param attrValue
	 * @return
	 * @throws java.lang.Exception
	 */
	private Element findElementByAttr(NodeList list, String attrName,
			String value) throws Exception {
		Element elem = null;
		Attr attr = null;
		String val = null;

		for (int i = 0; i < list.getLength(); i++) {
			elem = (Element) list.item(i);
			attr = elem.getAttributeNode(attrName);

			if (attr != null) {
				val = attr.getValue();
				if (value.equals(val)) {
					break;
				}
			}
		}
		return elem;
	}

	/**
	 * This methos takes the string that contains source codes with delimiters
	 * of comma, space,new line and then tokenize to convert into List
	 * 
	 * @param sourceCodeStr
	 * @return List of source codes
	 */
	private List<String> splitSourceCodes(String sourceCodeStr) {

		List<String> sourceCodesList = new ArrayList<String>();
		if (sourceCodeStr != null) {
			StringTokenizer strTokens = new StringTokenizer(sourceCodeStr,
					", \r\n");
			if (logger.isDebugEnabled()) {
				logger.debug("no of Source codes assigned - "
						+ strTokens.countTokens());
			}
			while (strTokens.hasMoreElements()) {
				String sourceCode = strTokens.nextToken();
				if (!sourceCodesList.contains(sourceCode)) {
					sourceCodesList.add(sourceCode);
				} else {
					if (logger.isDebugEnabled()) {
						logger.debug("splitSourceCodes(..) - Duplicate src code : "
								+ sourceCode);
					}
				}
			}
		}
		if (logger.isDebugEnabled()) {
			logger.debug("no of Source codes assigned in list - "
					+ sourceCodesList.size());
		}
		return sourceCodesList;
	}

	private String[] getSpaceDelimSrcCodes(List<String> sourceCodes) {
		for(int i=0;i<sourceCodes.size();i++){
			logger.info("SRC "+i+ " "+sourceCodes.get(i));
		}
		int currentSize = 0;
		StringBuffer tempSrcCode = new StringBuffer();
		List<String> srcCodeStrings = new ArrayList<String>();

		int currentRecord = 0;

		for (String srcCode : sourceCodes) {

			if (currentSize <= ALLOWED_SRC_CDE_RANGE) {
				if (currentSize > 0) {
					tempSrcCode.append(" ");
				}
				tempSrcCode.append(srcCode);

				if (currentRecord == (sourceCodes.size() - 1)) {
					srcCodeStrings.add(tempSrcCode.toString());
				}

			} else {
				srcCodeStrings.add(tempSrcCode.toString());
				tempSrcCode = new StringBuffer(srcCode);
			}

			++currentRecord;
			currentSize = tempSrcCode.length();
		}

		int i = 0;
		for (String string : srcCodeStrings) {
			if (logger.isDebugEnabled()) {
				logger.debug("Set: " + (i++) + " - " + string);
			}
		}

		return srcCodeStrings.toArray(new String[srcCodeStrings.size()]);
	}

	private GiftCertificateSourceCodeVO getGiftCertSrcCodeVOFromRequest(
			String operation, HttpServletRequest request) throws Exception {

		GiftCertificateSourceCodeVO srcCodeVO = new GiftCertificateSourceCodeVO();
		srcCodeVO.setRestrictedSourceCodes(splitSourceCodes(request
				.getParameter("gcc_source_code")));
		return srcCodeVO;
	}

	private Integer getTokensCount(String str, String delim) {
		return new StringTokenizer(str, delim).countTokens();
	}
	
	private boolean isSourceCodeListChanged(HttpServletRequest request) {
	   boolean isChange = false;
      String sourceCodeOld = request.getParameter("gcc_source_code_old");
      String sourceCodeNew = request.getParameter("gcc_source_code");
      if (!StringUtils.equals(sourceCodeOld, sourceCodeNew)) {
         isChange = true;
      }
	   return isChange;
	}
}
