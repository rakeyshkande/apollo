package com.ftd.accountingreporting.bo;

import com.ftd.accountingreporting.cache.handler.EODPCardConfigHandler;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.exception.MissingCreditCardException;
import com.ftd.accountingreporting.util.AccountingUtil;
import com.ftd.accountingreporting.util.EODUtil;
import com.ftd.accountingreporting.vo.EODRecordVO;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * This is an concrete class that contains logic to compile an end of day 
 * record specific to regular bills
 */
public class EODRegularBillBO extends EODRecordBO
{

    private static Logger logger  = 
        new Logger("com.ftd.accountingreporting.bo.EODRegularBillBO");
    
    public EODRegularBillBO() {}
    
    public EODRegularBillBO(EODPCardConfigHandler pcardConfig)
    {
        super(pcardConfig);
    }
    
  
    public void setTransactionType() throws Exception
    {
        recordVO.setTransactionType(ARConstants.TRANSACTION_TYPE_PURCHASE);
    }
  
    public void populateFields(CachedResultSet currentRecord) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering populateFields");
        }
        AccountingUtil acctgUtil = new AccountingUtil();
        String paymentMethod = null;
        String ccNumber = null;
        try{
            recordVO = new EODRecordVO();
            setLineItemType();
            setTransactionType();
            recordVO.setBillType(ARConstants.CONST_BILL);
            
            recordVO.setPaymentId(currentRecord.getLong("payment_id"));
            recordVO.setOrderGuid(currentRecord.getString("order_guid"));
            recordVO.setPaymentMethod(currentRecord.getString("payment_type"));
            recordVO.setCreditCardType(currentRecord.getString("cc_type"));
            recordVO.setCreditCardNumber(currentRecord.getString("cc_number"));
            recordVO.setCcExpDate(currentRecord.getString("cc_expiration")); 
            recordVO.setOrderNumber(currentRecord.getString("master_order_number"));
            recordVO.setOrderAmount(currentRecord.getDouble("credit_amount"));
            recordVO.setOrderDate(EODUtil.convertSQLDate(currentRecord.getDate("created_on")));
                    
            if (currentRecord.getDate("auth_date")!=null) {
                recordVO.setAuthorizationDate(EODUtil.convertSQLDate(currentRecord.getDate("auth_date")));      
            } else if(currentRecord.getDate("created_on")!=null) {
                //if no auth date, use payment created on date.
                recordVO.setAuthorizationDate(EODUtil.convertSQLDate(currentRecord.getDate("created_on"))); 
            } else {
                
            }
            
            // Truncate auth number to 6 digits.
            String authNumber = currentRecord.getString("auth_number");
            authNumber = authNumber == null? "" : authNumber;
            if(authNumber.length() > 6) {
                authNumber = authNumber.substring(0, 6);
            }
            recordVO.setApprovalCode(authNumber);
            recordVO.setAcqReferenceNumber(currentRecord.getString("acq_reference_number"));
            recordVO.setAuthCharIndicator(currentRecord.getString("auth_char_indicator"));
            recordVO.setAuthSourceCode(currentRecord.getString("auth_source_code"));
            recordVO.setAuthTranId(currentRecord.getString("transaction_id"));
            recordVO.setAuthValidationCode(currentRecord.getString("validation_code"));
            recordVO.setAVSResultCode(currentRecord.getString("avs_code"));
            recordVO.setResponseCode(currentRecord.getString("response_code"));
            recordVO.setCompanyId(currentRecord.getString("company_id"));
            setClearingMemberNumber(currentRecord.getLong("payment_id"));
            recordVO.setOriginType(currentRecord.getString("order_source"));
            /* set program name for pcard */
            recordVO.setProgramName(currentRecord.getString("program_name"));
            //BAMS Integration
            recordVO.setCscResponseCode(currentRecord.getString("csc_response_code"));
            recordVO.setInitialAuthAmount(currentRecord.getDouble("initial_auth_amount"));
            recordVO.setVoiceAuthFlag(currentRecord.getString("is_voice_auth"));
           
            
            // Throw exception if credit card number is missing.
            paymentMethod = recordVO.getPaymentMethod();
            ccNumber = recordVO.getCreditCardNumber();
            
            if(paymentMethod == null) {
                throw new Exception("Missing Payment Method for payment id: " + recordVO.getPaymentId());
            }
            if(!acctgUtil.isNonCreditCardPaymentType(paymentMethod) && ccNumber == null) {
                throw new MissingCreditCardException("Missing Credit Card Number for payment id: " + recordVO.getPaymentId());
            }
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting populateFields");
            } 
        }
        

    }
}






















