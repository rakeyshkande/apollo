package com.ftd.accountingreporting.bo;

import com.ftd.accountingreporting.cache.handler.EODPCardConfigHandler;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.dao.EODDAO;
import com.ftd.accountingreporting.exception.NoCacheRegionException;
import com.ftd.accountingreporting.util.AccountingUtil;
import com.ftd.accountingreporting.util.EODUtil;
import com.ftd.accountingreporting.vo.EODRecordVO;
import com.ftd.accountingreporting.vo.OrderDetailVO;
import com.ftd.accountingreporting.vo.PartnerPCardVO;
import com.ftd.accountingreporting.vo.PaymentTotalsVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.FileWriter;
import java.util.HashMap;
import java.util.List;

/**
 * This is an abstract class that contains logic to compile an end of day record 
 * common to regular billing , refund , additional billing and same day cancels.
 */
public abstract class EODRecordBO 
{
    private static Logger logger  = 
        new Logger("com.ftd.accountingreporting.bo.EODRecordBO");
  
    protected EODRecordVO recordVO;
    protected int orderSequence;
    protected EODDAO eodDAO;
    protected String lineItemType; 

    protected long paymentId;
   
    private static final int PAD_RIGHT = 0; 
    private static final int PAD_LEFT = 1; 
    private OrderDetailVO orderDetailVO;
    private PaymentTotalsVO paymentTotalsVO;
    private EODPCardConfigHandler pcardConfig;
  
    /**
    * Constructor for the class
    * @param newCurrentRecord ResultSet
    * @param conn Connection
    */
    public EODRecordBO() {}
    public EODRecordBO(EODPCardConfigHandler _pcardConfig) {
        pcardConfig = _pcardConfig;
    }
  
    public void reset() throws Exception {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering reset");
        }
        
        try{
            if(recordVO != null) 
            {
                recordVO.reset();
                recordVO = null;
            }
            
            eodDAO = null;
    
            orderDetailVO = null;
            paymentTotalsVO = null;
            lineItemType = null;
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting reset");
            } 
        } 
    }
  
    public void setEODAO(EODDAO newEODDAO)
    {
        eodDAO = newEODDAO;
    }
  
    public void setRecordVO(EODRecordVO newRecordVO) {
        recordVO = newRecordVO;
    }
  
    public EODRecordVO getRecordVO()
    {
        return recordVO;
    }
    
    public long getPaymentId()
    {
        return paymentId;
    }
  
    /**
    * sets the orderSequence variable
    * @param newOrderSequence int
    */
    public void setOrderSequence(int newOrderSequence)
    {
        orderSequence = newOrderSequence;
    }
  

    /**
    * This method populates the fields to eodVo object
    * @throws Exception
    */
    public abstract void populateFields(CachedResultSet rs) throws Exception;

  
    public void processPCard() throws NoCacheRegionException, Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering processPurchase");
        }
        
        try{
            orderDetailVO = getOrderDetails();
            paymentTotalsVO = getPaymentTotals();
            
            //EODPCardConfigHandler pcardConfig = (EODPCardConfigHandler)CacheController.getInstance().getHandler("PARTNER_PCARD");
            if(pcardConfig == null) {
                throw new NoCacheRegionException("Cannot load cache handler! Rolling back event...");
            }
            List pcardConfigList = pcardConfig.getPartnerPCard(recordVO.getProgramName());
            CachedResultSet  coBrandFields = eodDAO.doGetCoBrandFields(recordVO.getPaymentId());
            if (recordVO.getProgramName() != null &&
                !recordVO.getProgramName().equals("") &&
                pcardConfigList != null &&
                pcardConfigList.size() > 0)
            {
                processPcardLevel2(coBrandFields, pcardConfigList);
                processPcardAdditional();
            } 
            else if(coBrandFields != null && coBrandFields.next())
            {
                processPcardLevel1(coBrandFields);
                processPcardAdditional();
            }
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting processPurchase");
            } 
        }         
    
    }
  
  private OrderDetailVO getOrderDetails() throws Exception
  {
    
        if(logger.isDebugEnabled()) {
            logger.debug("Entering getOrderDetails");
        }
        OrderDetailVO vo = new OrderDetailVO();
        try{
            String mon = recordVO.getOrderNumber();
            logger.debug("getOrderDetails: mon=" + mon);
        
            long orderDetailId = eodDAO.doGetOrderDetailId(mon);
            logger.debug("getOrderDetails: orderDetailId=" + orderDetailId);
            CachedResultSet rs = eodDAO.doGetOrderDetails(orderDetailId);
            
            if (rs.next())
            {
              String recipId = rs.getString("recipient_id");
              String prodId = rs.getString("product_id");
              String prodLevel = rs.getString("size_indicator");
              String aribaPo = rs.getString("ariba_po_number");
        
              recipId = recipId == null? "" : recipId;
              prodId = prodId == null ? "" : prodId;
              prodLevel = prodLevel== null ? "" : prodLevel;
              aribaPo = aribaPo == null ? "" : aribaPo;      
              
              vo.setOrderDetailId(orderDetailId);
              if(!recipId.equals("")) {
                  vo.setRecipientId(Long.valueOf(recipId).longValue());
              }
              vo.setProductId((String)prodId);
              vo.setProductLevel((String)prodLevel);
              vo.setAribaPONumber((String)aribaPo);
        
            }
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getOrderDetails");
            } 
        }  
    
    
    return vo;    
  }
  
  private PaymentTotalsVO getPaymentTotals() throws Exception
  {
      return eodDAO.doGetPaymentTotals(recordVO.getPaymentId());
  }
  
    public void processRecord() throws NoCacheRegionException, Exception
    {
        processPCard();
    }
  
    protected abstract void setTransactionType() throws Exception;
 
 
    public void setLineItemType() throws Exception
    {
        lineItemType = ConfigurationUtil.getInstance().getProperty(ARConstants.CONFIG_FILE, ARConstants.CONST_EOD_LINE_ITEM_TYPE);
        recordVO.setLineItemType(lineItemType);
    }
  
    /**
    * set the clearing member number for the payment id specified, clearing member number differs for different
    * companies.
    * @param paymentId long
    * @throw Exception
    */
    protected void setClearingMemberNumber(long paymentId) throws Exception
    {
        recordVO.setclearingMemberNumber(eodDAO.doGetClearingMemberNumber(paymentId));
    }
  
    /**
    * This method sets addinfo1 through addinfo4 fields and cardholderrefnumber and salesTaxamount for vo
    * @param configFields CachedResultSet
    * @throws Exception
    */
    protected void processPcardLevel2(CachedResultSet coBrandFields, List configFields) throws Exception
    { 
        if(logger.isDebugEnabled()) {
            logger.debug("Entering processPcardLevel2");
        }

        try{
            AccountingUtil acctUtil = new AccountingUtil();
            String billInfo1 = "";
            String billInfo2 = "";
            String billInfo3 = "";
            String billInfo4 = "";
            String cardHolderRefNumber = "";
            HashMap hm = null;
            if (coBrandFields != null && coBrandFields.next())
                hm = parseMOCROCBLC(coBrandFields);
            else 
                hm = new HashMap();
        
          for (int i = 0; i < configFields.size(); i++) {
              PartnerPCardVO pcardVO = (PartnerPCardVO)configFields.get(i);
              String fieldName = pcardVO.getFieldName();
              String sortNum = String.valueOf(pcardVO.getSortOrder());
              String sourceField = pcardVO.getSourceField();
              String fieldPrefix = pcardVO.getFieldPrefix();
              String fieldSuffix = pcardVO.getFieldSuffix();
              String currencyDecimalInd = pcardVO.getCurrencyDecimalInd();
              String fieldValue = null;
          
              fieldName = (fieldName == null ? "" : fieldName);  
              sourceField = (sourceField == null ? "" : sourceField); 
              fieldPrefix = (fieldPrefix == null ? "" : fieldPrefix.replaceFirst("<sp>", " ")); 
              fieldSuffix = (fieldSuffix == null ? "" : fieldSuffix);
              currencyDecimalInd = (currencyDecimalInd == null ? "" : currencyDecimalInd);
              fieldValue = getPcardFieldValue(fieldPrefix, currencyDecimalInd, hm);
              
              if (fieldName.equals("BILLINGINFO1"))
                  billInfo1 = billInfo1 + fieldPrefix + fieldValue + fieldSuffix;
              else if (fieldName.equals("BILLINGINFO2"))
                  billInfo2 = billInfo2 + fieldPrefix + fieldValue + fieldSuffix;
              else if (fieldName.equals("BILLINGINFO3"))
                  billInfo3 = billInfo3 + fieldPrefix + fieldValue + fieldSuffix;
              else if (fieldName.equals("BILLINGINFO4"))
                  billInfo4 = billInfo4 + fieldPrefix + fieldValue + fieldSuffix;
              else if (fieldName.equals("CARD_HOLDER_REF_NUM")) {
                  //get ariba PO number
                  cardHolderRefNumber = orderDetailVO.getAribaPONumber();
              }
              else if (fieldName.equals("SALES_TAX")) {
                  recordVO.setSalesTaxAmount(String.valueOf(paymentTotalsVO.getTaxTotal()));
              }
            }
    
            recordVO.setAddField1(billInfo1);
            recordVO.setAddField2(billInfo2);
            recordVO.setAddField3(billInfo3);
            recordVO.setAddField4(billInfo4);
            recordVO.setCardHolderRefNumber(cardHolderRefNumber);
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting processPcardLevel2");
            } 
        } 
    }
  
    /**
    * parse the field prefix to get the bill info data
    * @param fieldPrefix String
    * @return String
    */
    protected String getPcardFieldValue(String fieldPrefix, String currencyDecimalInd, HashMap hm) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering parseForBillInfo");
        }

        String billInfoToAppend = "";
        
        try{
            if (fieldPrefix.equalsIgnoreCase("Frt:")
              || fieldPrefix.startsWith("FRT")) {
                //get the shipping cost from orders and add it to billInfo1 
                AccountingUtil acctgUtil = new AccountingUtil();
                // 11/15/05: adding service fee and shipping fee to get freight amount
                double freightAmt = paymentTotalsVO.getShippingFeeTotal()
                    + paymentTotalsVO.getServiceFeeTotal();
                
                billInfoToAppend = acctgUtil.formatAmountToPattern(String.valueOf(freightAmt), "0.00");
                if(ARConstants.COMMON_VALUE_NO.equals(currencyDecimalInd)) {
                    // remove dot
                    billInfoToAppend = billInfoToAppend.substring(0, billInfoToAppend.indexOf("."))
                                        + billInfoToAppend.substring(billInfoToAppend.indexOf(".") + 1);
                }
            }
            else if (fieldPrefix.equalsIgnoreCase("Po:")) {
                //get the ariba purchase order number from order and add it to billInfo1
                billInfoToAppend = orderDetailVO.getAribaPONumber();
            }
            else if (fieldPrefix.equalsIgnoreCase("id:")) {
                //get ID
                billInfoToAppend = (String) hm.get("ID");
            }
            else if (fieldPrefix.equalsIgnoreCase("duns:")) {
                //get DUNS
                billInfoToAppend = (String) hm.get("DUNS");
            }
            else if (fieldPrefix.equalsIgnoreCase("Tax"))
            {
                billInfoToAppend = "0";
                if (paymentTotalsVO.getTaxTotal() > 0)
                    billInfoToAppend = "1";
            }
            billInfoToAppend = billInfoToAppend == null? "" : billInfoToAppend;
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting processPcardLevel2");
            } 
        } 
        return billInfoToAppend;
    }
  
    /**
    * This method sets addinfo1 through addinfo4 fields and cardholderrefnumber and salesTaxamount for vo
    * @param coBrandFields ResultSet
    * @throws Exception
    */
    protected void processPcardLevel1(CachedResultSet coBrandFields) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering processPcardLevel1");
        }

        String billInfoToAppend = "";
        
        try{
            //parse MOC, ROC and BLC
            String moc = "";
            String roc = "";
            String blc = "";
            HashMap hm = parseMOCROCBLC(coBrandFields);
            moc= (String) hm.get("MOC");
            roc= (String) hm.get("ROC");
            blc= (String) hm.get("BLC");
            
            moc = moc == null? "" : moc;
            roc = roc == null? "" : roc;
            blc = blc == null? "" : blc;
            
            //set cardHolderRefNUMBER M:cobrand_data for MOC, R:cobrand_data for ROC
            recordVO.setCardHolderRefNumber("M"+moc+"R"+roc);
            
            //set addInfo1 to B:cobrand_data for BLC
            recordVO.setAddField1("B"+blc);
            
            //recordVO.setAddField2(eodDAO.doGetCustomerName((String)currentRecord.get(16)));
            recordVO.setAddField2(eodDAO.doGetCustomerName(recordVO.getPaymentId()));
            
            //set addField3 should be populated with first line of ORDER_DETAILS.PRODUCT_ID or ORDER_DETAILS.PRODUCT_LEVEL
            recordVO.setAddField3(orderDetailVO.getProductId()+orderDetailVO.getProductLevel());
            
            //recordVO.setAddField4((String)currentRecord.get(16));
            recordVO.setAddField4(Long.toString(orderDetailVO.getOrderDetailId()));            
            
            //get sales tax amount for order and set sales tax amount for vo
            recordVO.setSalesTaxAmount(String.valueOf(paymentTotalsVO.getTaxTotal()));
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting processPcardLevel1");
            } 
        } 
    }
  
    private HashMap parseMOCROCBLC(CachedResultSet coBrandFields) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering parseMOCROCBLC");
        }
        HashMap hm = new HashMap();
        try{
            do
            {
                if ("MOC".equalsIgnoreCase((String)coBrandFields.getObject(3))) {
                    hm.put("MOC",coBrandFields.getObject(4));
                }
                else if ("ROC".equalsIgnoreCase((String)coBrandFields.getObject(3))
                    || "RC".equalsIgnoreCase((String)coBrandFields.getObject(3))) {
                    hm.put("ROC",coBrandFields.getObject(4));
                }
                else if ("BLC".equalsIgnoreCase((String)coBrandFields.getObject(3))) {
                    hm.put("BLC",coBrandFields.getObject(4));
                }
                else if ("ID".equalsIgnoreCase((String)coBrandFields.getObject(3))) {
                    hm.put("ID",coBrandFields.getObject(4));
                }
                else if ("DUNS".equalsIgnoreCase((String)coBrandFields.getObject(3))) {
                    hm.put("DUNS",coBrandFields.getObject(4));
                }
            }while (coBrandFields.next());

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting parseMOCROCBLC");
            } 
        }
        return hm;
    }
    /**
    * This method sets the recipientZip field for VO
    * @throws Exception
    */
    protected void processPcardAdditional() throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering processPcardAdditional");
        }
 
        try{
            String recipientZip = null;
            CachedResultSet rs = eodDAO.doGetCusotmerInfo(orderDetailVO.getRecipientId());
            if (rs.next()) {
                recipientZip = rs.getString("customer_zip_code");
            }
            if(recipientZip != null) {
                if (eodDAO.doGetCountryFromZip(recipientZip)== null) {
                    recordVO.setRecipientZip(recipientZip);
                }
                else if (recipientZip.length() > 5){
                    recordVO.setRecipientZip(recipientZip.substring(0,5));
                }
                else {
                    recordVO.setRecipientZip(recipientZip);
                }
                
            }
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting processPcardAdditional");
            } 
        }
    }
  
    /**
    * This method builds the text line to be printed in the file
    * @return 
    * formatted string
    */
    public String buildLineText() throws Exception
    {
        // Product Identifier required for settlement (only required for Visa at this point 3/11/08)
        String productIdentifier = "";
        
        if(logger.isDebugEnabled()) {
            logger.debug("Entering buildLineText");
        }
        
        StringBuffer detail = new StringBuffer();
        AccountingUtil acctgutil = new AccountingUtil();
        
        try{
            // Obtain Product Identifier from the acq reference data
            if(recordVO.getAcqReferenceNumber() != null){
                int index = recordVO.getAcqReferenceNumber().indexOf('f');
                int length = recordVO.getAcqReferenceNumber().length();
                if(index > -1 && length > index + 1){
                    productIdentifier = recordVO.getAcqReferenceNumber().substring(index + 1, length);
                }
            }

            detail.append(EODUtil.pad(recordVO.getLineItemType(),PAD_RIGHT," ",3));
            detail.append(EODUtil.pad(recordVO.getCreditCardType(),PAD_RIGHT," ",2));
            detail.append(EODUtil.pad(recordVO.getCreditCardNumber(),PAD_RIGHT," ",20));
            
            String ccExpDate = recordVO.getCcExpDate();
            if(ccExpDate != null && ccExpDate.indexOf("/") != -1) {
                ccExpDate = EODUtil.replace(recordVO.getCcExpDate(),"/","");
            } else if (ccExpDate != null && ccExpDate.indexOf(".") != -1) {
                ccExpDate = EODUtil.replace(recordVO.getCcExpDate(),".","");
            }
            detail.append(EODUtil.pad(ccExpDate,PAD_RIGHT," ",4));
            detail.append(EODUtil.pad(EODUtil.formatDoubleAmount(recordVO.getOrderAmount()),PAD_LEFT,"0",8));
            detail.append(EODUtil.pad(recordVO.getDisplayAuthorizationDate(),PAD_RIGHT," ",6));
            detail.append(EODUtil.pad(recordVO.getApprovalCode(),PAD_RIGHT," ",6));
            detail.append(EODUtil.pad(Long.toString(recordVO.getOrderSequence()),PAD_RIGHT," ",1));
            // chop the string in order number after '/'.
            detail.append(EODUtil.pad(acctgutil.chop(recordVO.getOrderNumber(), '/'),PAD_LEFT," ",5));
            detail.append(EODUtil.pad(recordVO.getTransactionType(),PAD_RIGHT," ",1));
            detail.append(EODUtil.pad(recordVO.getAuthCharIndicator(),PAD_RIGHT," ",1));
            detail.append(EODUtil.pad(recordVO.getAuthTranId(),PAD_RIGHT," ",15));
            detail.append(EODUtil.pad(recordVO.getAuthValidationCode(),PAD_RIGHT," ",4));
            detail.append(EODUtil.pad(recordVO.getAuthSourceCode(),PAD_RIGHT," ",1));
            detail.append(EODUtil.pad(recordVO.getResponseCode(),PAD_RIGHT," ",2));
            detail.append(EODUtil.pad(recordVO.getAVSResultCode(),PAD_RIGHT," ",1));
            detail.append(EODUtil.pad(EODUtil.replace(recordVO.getClearingMemberNumber(),"-",""),PAD_RIGHT," ",6));
            detail.append(EODUtil.pad(recordVO.getOriginType(),PAD_RIGHT," ",1));
            detail.append(EODUtil.pad(recordVO.getCardHolderRefNumber(),PAD_RIGHT," ",17));
            detail.append(EODUtil.pad(EODUtil.formatDoubleAmount(Double.valueOf(recordVO.getSalesTaxAmount()).doubleValue()),PAD_LEFT,"0",5));
            detail.append(EODUtil.pad(recordVO.getAddField1(),PAD_RIGHT," ",40));
            detail.append(EODUtil.pad(recordVO.getAddField2(),PAD_RIGHT," ",40));
            detail.append(EODUtil.pad(recordVO.getAddField3(),PAD_RIGHT," ",40));
            detail.append(EODUtil.pad(recordVO.getAddField4(),PAD_RIGHT," ",20));
            detail.append(EODUtil.pad(recordVO.getRecipientZip(),PAD_RIGHT," ",6));
            detail.append(EODUtil.pad(productIdentifier,PAD_RIGHT," ",3));
            //MasterPass iteration 7 17909 changes added the check not to send 0 on file for wallet indicator
            String walletIndicator = "";
            if(recordVO.getWalletIndicator() != 0)            
            	walletIndicator = new Long(recordVO.getWalletIndicator()).toString();
            
            detail.append(EODUtil.pad(walletIndicator,PAD_RIGHT," ",3));
            detail.append(EODUtil.pad(null,PAD_RIGHT," ",39));
            
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting buildLineText");
            } 
        }
        return detail.toString();
    }
}