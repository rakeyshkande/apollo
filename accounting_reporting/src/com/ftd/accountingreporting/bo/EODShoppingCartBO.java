package com.ftd.accountingreporting.bo;

import com.ftd.accountingreporting.cache.handler.EODPCardConfigHandler;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.dao.CommonDAO;
import com.ftd.accountingreporting.dao.EODDAO;
import com.ftd.accountingreporting.exception.MissingCreditCardException;
import com.ftd.accountingreporting.exception.NoCacheRegionException;
import com.ftd.accountingreporting.util.AccountingUtil;
import com.ftd.accountingreporting.util.EODUtil;
import com.ftd.accountingreporting.util.SystemMessager;
import com.ftd.accountingreporting.vo.EODCreditBalanceVO;
import com.ftd.accountingreporting.vo.EODPaymentVO;
import com.ftd.accountingreporting.vo.EODRecordVO;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.FileWriter;

import java.math.BigDecimal;

import java.sql.Connection;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class EODShoppingCartBO 
{
    private String masterOrderNumber;
    private EODDAO dao;
    private long eodBatchTime;
    private long batchNumber;
    //private FileWriter errorOut;
    
    private static String COL_MON = "master_order_number";
    private static String COL_PID = "payment_id";
    private static String EOD_NOPAGE_SUBJECT = "EOD Exception";
    private EODPCardConfigHandler pcardConfig;
    private static Logger logger  = 
        new Logger("com.ftd.accountingreporting.bo.EODShoppingCartBO");
    private Map pMap = null;
    private Map rMap = null;
    private ArrayList paymentCCs = null; // stores unique payment cc id
    private ArrayList refundCCs = null;  // stores unique refund cc id
    int ps = 0; // order sequence for payments
    int rs = 0; // order sequence for refunds
    
 
  
    public EODShoppingCartBO(EODDAO _dao, long _eodBatchTime, long _batchNumber, EODPCardConfigHandler _pcardConfig)
    {
        dao = _dao;
        eodBatchTime = _eodBatchTime;
        batchNumber = _batchNumber;
        //errorOut = _errorOut;
        pcardConfig = _pcardConfig;
    }

    public void setMasterOrderNumber(String masterOrderNumber)
    {
        this.masterOrderNumber = masterOrderNumber;
    }

    public String getMasterOrderNumber()
    {
        return this.masterOrderNumber;
    }

/**
 * Process shopping cart.
 * 1. Create list of EODRecordVOs from payment list of payment ids.
 * 2. Add refunds to payment list. Net same day transactions.
 * 3. Empty the payment list and write the records to database.
 */
    public void process(List payments, List refunds) throws NoCacheRegionException, Exception {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering process master order number " + this.masterOrderNumber);
        }
        
        // create payment map by cc number.
        createPmap(payments);
        
        // create refund map by cc number;
        createRmap(refunds);
        
        // Process no auth condition.
        if(!pMap.isEmpty()) {
            processNoAuth();
        }        
        
        // process shopping cart with given payment and refund data.
        if(!pMap.isEmpty()) {
            processCart();
        }
        
        // process refunds that did not match with any payment by cc number.
        if(!rMap.isEmpty()) {
            processRemainingRefunds();
        }

        if(logger.isDebugEnabled()){
            logger.debug("Exiting process Master Order Number " + this.masterOrderNumber);
        } 
    }
  
  /**
   * Create a map between a distinct cc number and a list of payment objects (EODRecordVO)
   * If payment type is not credit card, simply update the status to Billed and
   * do not add it to the list.
   */
  private void createPmap(List payments) throws NoCacheRegionException, Exception
  {
      if(logger.isDebugEnabled()) {
          logger.debug("Entering createPmap. Size=" + (payments == null? 0 : payments.size()));
      }
      pMap = new HashMap();
      paymentCCs = new ArrayList(); 
      
      List paymentList = loadPaymentsByIds(payments);
      String ccNumber = null;
      ArrayList ccList = null;
      AccountingUtil acctUtil = new AccountingUtil();
      
      if(paymentList != null) {
        logger.debug("Loaded payment list size: " + paymentList.size());
      
        for (int i = 0; i < paymentList.size(); i++) {
            EODRecordVO eodRecordVO = (EODRecordVO)paymentList.get(i);
            ccNumber = eodRecordVO.getCreditCardNumber();
            ccList = (ArrayList)pMap.get(ccNumber);
            
            // if cc number does not exist in map, add it.
            if (ccList == null) {
                logger.debug("Create new payment entry list for CC number");
                ccList = new ArrayList();  
                pMap.put(ccNumber, ccList);
                // save distinct cc number to list
                paymentCCs.add(ccNumber);
            } 
            try {
                logger.debug("Adding CC number to list ending with: " + ccNumber.substring(ccNumber.length()-4));
            } catch (Exception ie) {
                logger.error(ie);
                SystemMessager.send("Invalid Credit Card Number. Payment id " + eodRecordVO.getPaymentId(), ie, ARConstants.SM_NOPAGE_SOURCE, 
                SystemMessager.LEVEL_PRODUCTION, 
                ARConstants.EOD_BILLING_PROCESS_ERROR_TYPE, EOD_NOPAGE_SUBJECT, dao.getConnection());
                //writeInvalidCCError(eodRecordVO);
            }
            ccList.add(eodRecordVO);  
        }
      }
      if(logger.isDebugEnabled()) {
          logger.debug("Exiting createPmap.");
      }
  }
  
  /**
   * Create a map between a distinct cc number and a list of refund objects (EODRecordVO)
   */
  private void createRmap(List refundIds) throws NoCacheRegionException, Exception
  {
      rMap = new HashMap();
      refundCCs = new ArrayList();
      
      String ccNumber = null;
      ArrayList ccList = null;
      
      if(refundIds != null) {
        logger.debug("Refund list size: " + refundIds.size());
        List refundList = loadRefundsByIds(refundIds);
      
        for (int i = 0; i < refundList.size(); i++) {
            EODRecordVO eodRecordVO = (EODRecordVO)refundList.get(i);
            ccNumber = eodRecordVO.getCreditCardNumber();

            ccList = (ArrayList)rMap.get(ccNumber);
            
            // if cc number does not exist in map, add it.
            if (ccList == null) {
                logger.debug("Create new refund entry list for CC number");
                ccList = new ArrayList(); 
                rMap.put(ccNumber, ccList);
                // save distinct refund cc number to list.
                refundCCs.add(ccNumber);
            } 
            try {
                logger.debug("Adding CC number to list ending with: " + ccNumber.substring(ccNumber.length()-4));
            } catch (Exception ie) {
                logger.error(ie);
                SystemMessager.send("Invalid Credit Card Number. Payment id " + eodRecordVO.getPaymentId(), ie, ARConstants.SM_NOPAGE_SOURCE, 
                        SystemMessager.LEVEL_PRODUCTION, 
                        ARConstants.EOD_BILLING_PROCESS_ERROR_TYPE, EOD_NOPAGE_SUBJECT, dao.getConnection());
            }
            ccList.add(eodRecordVO);  
        }
      }
      if(logger.isDebugEnabled()) {
          logger.debug("Exiting createRmap.");
      }
  }
  
  /**
   * For each payment record in the list that mapps to a credit card number, check auth_number.
   * If auth_number is null, check if there is a full refund (or partial refunds that add up to a full refund).
   * If so, bill payment and refund; remove payment and refunds from list.
   * If not, remove payment.
   * @throws Exception
   */
  private void processNoAuth() throws Exception
  {
      String currentCCNumber= null;
      ArrayList currentCCpList = null;
      ArrayList currentCCrList = null;
      BigDecimal paymentTotal = null;
      EODRecordVO recordVO = null;
      String authNumber = null;
      BigDecimal runningRefundTotal = new BigDecimal(0);
      double refundAmt = 0;
      EODRecordVO refundRecordVO = null;
      ArrayList fullRefundCCrList = new ArrayList(); // Keeps a list of refund that fully net with a payment.
      ArrayList fullRefundCCpList = new ArrayList();  // Keeps a list of refund that are available for evaluation.
      boolean fullyRefunded = false;
      
      // go through each cc number in the payment map.
      if(paymentCCs != null ) {
          for (int i = 0; i < paymentCCs.size(); i++) {
              // get the cc number from list
              currentCCNumber = (String)paymentCCs.get(i);  
              
              // get payment list associated with this cc number
              currentCCpList = (ArrayList)pMap.get(currentCCNumber);
              currentCCrList = (ArrayList)rMap.get(currentCCNumber);
              for (int j = 0; j < currentCCpList.size(); j++) {
                  fullyRefunded = false;
                  recordVO = (EODRecordVO)currentCCpList.get(j);
                  authNumber = recordVO.getApprovalCode();
                  
                  if(authNumber == null || authNumber.length() == 0) {
                     
                     // Go through refunds (sorted by payment_id/creation time)
                     // As soon as the refunds add up to the total of paymentTotal, 
                     // the current payment is considered fully refunded. 
                     paymentTotal = new BigDecimal(String.valueOf(recordVO.getOrderAmount()));
                     runningRefundTotal = new BigDecimal(0);
                     fullRefundCCrList.clear();
                     logger.debug("Payment has no auth:" + recordVO.getPaymentId());
                     logger.debug("Payment total:" + paymentTotal.doubleValue());
                     
                     if(currentCCrList != null) {
                          for (int k = 0; k < currentCCrList.size(); k++) {
                              refundRecordVO = (EODRecordVO)currentCCrList.get(k);
                              refundAmt = refundRecordVO.getOrderAmount();
                              runningRefundTotal = runningRefundTotal.add(new BigDecimal(String.valueOf(refundAmt)));
                              fullRefundCCrList.add(refundRecordVO); 
                              
                              logger.debug("Refund amout:" + refundAmt);
                              logger.debug("Refund running total:" + runningRefundTotal.doubleValue());         
                              
                              if(paymentTotal.compareTo(runningRefundTotal) == 0) {
                                  // Refunds have incrementally added up to be equal to the payment amount.
                                  fullRefundCCpList.clear();
                                  fullRefundCCpList.add(recordVO);
                                  handleFullRefund(fullRefundCCpList, fullRefundCCrList);
                                  
                                  // Update apayment map for credit card
                                  currentCCpList.remove(j);
                                  j--;
                                  if(currentCCpList.size() == 0) {
                                    pMap.remove(currentCCNumber);
                                  } else {
                                    pMap.put(currentCCNumber, currentCCpList);
                                  }
                                  
                                  // Create remaining refund list.
                                  for (int m = k; m >=0; m--){
                                    currentCCrList.remove(m);
                                  }
                                  
                                  // Update refund map for credit card number.
                                  if(currentCCrList.size() == 0) {
                                    rMap.remove(currentCCNumber);
                                  } else {
                                    rMap.put(currentCCNumber, currentCCrList);
                                  }
                                  fullyRefunded = true;
                                  break;
                              }
                          } // end for loop for refund list
                      }
                      
                      if(!fullyRefunded) {
                             //Payment has no auth and not fully refunded. Remove it.
                            currentCCpList.remove(j);
                            j--;
                            if(currentCCpList.size() == 0) {
                                pMap.remove(currentCCNumber);
                            } else {
                                pMap.put(currentCCNumber, currentCCpList);
                            }
                      }

                  } // else - order has auth. 
                      
            } // end for loop for payment list
            // If no lists map to the cc number, remove it from the list.
            if (pMap.size() == 0 && rMap.size() == 0) {
                paymentCCs.remove(currentCCNumber);
                i--;
            }
        } // end for loop for cc list
              
          
      }
  }
  
  /**
   * This method does the following:
   * 1. For each cc number in payment map, find the refund list with matching cc number.
   *    Determine if the total amount is enough for refunds.
   *    a. If it is, proceed with netting. Create a payment billing detail with each payment
   *       that  has a remaining balance. Mark all payments and refunds Billed. 
   *    b. If not, take into account of billed balance.
   *       i. If there's enough to issue the refund, mark all payments Billed. Create a
   *          refund billing detail with amount=(total refund today - total payment today)
   *       ii. If there's not enough to issue the refund, create a payment billing detail 
   *           for each payment. Mark all payments Billed. Disregard refunds.
   *    c. Remove refund entry from refund map.
   */
  private void processCart() throws Exception 
  {
      logger.debug("Entering processCart...");
      String currentCCNumber= null;
      ArrayList currentCCpList = null;
      ArrayList currentCCrList = null;
      BigDecimal paymentTotal = null;
      BigDecimal refundTotal = null;
      int c = 0;
      
      // go through each cc number in the payment map.
      if(paymentCCs != null ) {
          logger.debug("paymentCCs size=" + paymentCCs.size());
          for (int i = 0; i < paymentCCs.size(); i++) {
              // get the cc number from list
              currentCCNumber = (String)paymentCCs.get(i);
              try {
                  logger.debug("currentCCNumber is: " + currentCCNumber.substring(currentCCNumber.length() - 4));
              } catch (Exception ie) {
                  logger.error(ie);
                  SystemMessager.send("Invalid Credit Card Number.", ie, ARConstants.SM_NOPAGE_SOURCE, 
                      SystemMessager.LEVEL_PRODUCTION, 
                      ARConstants.EOD_BILLING_PROCESS_ERROR_TYPE, EOD_NOPAGE_SUBJECT, dao.getConnection());
              }
              // get payment list associated with this cc number
              currentCCpList = (ArrayList)pMap.get(currentCCNumber);
              logger.debug("currentCCpList size: " + (currentCCpList == null ? 0 : currentCCpList.size()));
              
              // add up payment total
              paymentTotal = getListTotal(currentCCpList);
              logger.debug("paymentTotal is: " + paymentTotal.doubleValue());
              
              // get refund list associated with this cc number
              currentCCrList = (ArrayList)rMap.get(currentCCNumber);
              logger.debug("currentCCrList size is : " + (currentCCrList == null? 0 : currentCCrList.size()));
              
              // add up refund total
              refundTotal = getListTotal(currentCCrList);
              logger.debug("refundTotal is : " + refundTotal.doubleValue());
              
              c = paymentTotal.compareTo(refundTotal);
              logger.debug("value of c is: " + c);
              if(c > 0) {
                  // payment total is greater than refund total
                  handleAllowRefund(currentCCpList, currentCCrList, paymentTotal, refundTotal);
              } else if(c == 0) {
                  // payment total is same as refund total
                  handleFullRefund(currentCCpList, currentCCrList);
              } else {
                  // Use the first refund payment_id to get previously billed balance.
                  EODRecordVO refundVO = (EODRecordVO)currentCCrList.get(0);
                  double billedAmtDouble = dao.shouldRefundBeBilled(refundVO.getPaymentId());
                  logger.debug("billedAmt is :" + billedAmtDouble);
                  BigDecimal billedAmt = new BigDecimal(String.valueOf(billedAmtDouble));
                  // Add billed amount to payment total and see if that will make refund to be billed.
                  billedAmt = billedAmt.add(paymentTotal);
                  logger.debug("billedAmt plus payment total is: " + billedAmt.doubleValue());
                  
                  c = billedAmt.compareTo(refundTotal);
                  logger.debug("value of c is : " + c);
                  if (c >= 0) {
                      // payment total plus billed balance is enough to bill refund.
                      // Mark payments billed.
                      
                      // create one refund billing detail with amount = (refund total - payment total)
                      refundTotal = refundTotal.subtract(paymentTotal);
                      EODRecordVO recordVO = (EODRecordVO)currentCCrList.get(0);
                      recordVO.setOrderAmount(refundTotal.doubleValue());
                      logger.debug("creating refund billing detail with amount: " + refundTotal.doubleValue());
                      createBillingDetail(recordVO);
                      
                      // bill all payments
                      billPaymentList(currentCCpList);
                      
                      // bill all refunds
                      billRefundList(currentCCrList);
                  } else {
                      // payment total plus billed balance is not enough to bill refund.
                      // Create billing details for payments. Bill payments. Discard refunds.
                      handleDisallowRefund(currentCCpList);
                      SystemMessager.send("Not enough payment to bill refund id " + refundVO.getRefundId(), new Exception("Not enough payment"), ARConstants.SM_NOPAGE_SOURCE, 
                      SystemMessager.LEVEL_PRODUCTION, 
                      ARConstants.EOD_BILLING_PROCESS_ERROR_TYPE, EOD_NOPAGE_SUBJECT, dao.getConnection());
                  }   
              }
              
              // remove cc from refund cc list
              refundCCs.remove(currentCCNumber);
          } 
      } else {
          logger.error("payment list is null");
      }
      logger.debug("Exiting processCart...");
  }
  
  /**
   * The method does the following:
   * 1. Create billing detail for each payment that has a remaining balance.
   * 2. Bill all payments
   * 3. Bill all refunds
   */
  private void handleAllowRefund(List pList, List rList, BigDecimal pTotal, BigDecimal rTotal) throws Exception 
  {
      logger.debug("Entering handleAllowRefund...");
      BigDecimal pRunningTotal = new BigDecimal(0);
      double pAmount = 0;
      int c = 0;
      
      for (int i = 0; i < pList.size(); i++) {
          EODRecordVO recordVO = (EODRecordVO)pList.get(i);
          pAmount = recordVO.getOrderAmount();
          pRunningTotal = new BigDecimal(String.valueOf(pRunningTotal.doubleValue() + pAmount));
          logger.debug("pRunningTotal is: " + pRunningTotal.doubleValue());
          logger.debug("refund total is : " + rTotal.doubleValue());
          
          c = rTotal.compareTo(pRunningTotal);
          if (c < 0) {
              // Current payment more than enought to net all refunds. Update current record balance.
              // Create billing detail for current record.
              // Create billing details for the rest of payments and bill them.
              pRunningTotal = pRunningTotal.subtract(rTotal);
              recordVO.setOrderAmount(pRunningTotal.doubleValue());
              logger.debug("Creating payment billing detail with amount: " + pRunningTotal.doubleValue());
              createBillingDetail(recordVO);
              i++;
              for (; i < pList.size(); i++) {
                  recordVO = (EODRecordVO)pList.get(i);
                  createBillingDetail(recordVO);
              }
          } else if(c == 0) {
              // Current payment exactly netted all refunds. Bill current payment. Create billing details for
              // the rest of the payments. Bill rest of the payments.
              dao.doUpdatePaymentBillStatus(recordVO.getPaymentId(),eodBatchTime,ARConstants.ACCTG_STATUS_BILLED);
              i++;
              for (; i < pList.size(); i++) {
                  recordVO = (EODRecordVO)pList.get(i);
                  createBillingDetail(recordVO);
              }
          } else {
              // Current payment not enough to net refund. Mark payment billed. Continue the loop.
              dao.doUpdatePaymentBillStatus(recordVO.getPaymentId(),eodBatchTime,ARConstants.ACCTG_STATUS_BILLED);
          }
      }
      // Bill all refunds.
      billRefundList(rList);
      logger.debug("Exiting handleAllowRefund...");
  }
  
  /**
   * The method does the following:
   * 1. Bill all payments
   * 2. Bill all refunds
   */
  private void handleFullRefund(List pList, List rList) throws Exception 
  {
      logger.debug("Entering handleFullRefund...");
      billPaymentList(pList);
      billRefundList(rList);
      logger.debug("Exiting handleFullRefund...");
  }  
  
  /**
   * The method does the following:
   * 1. Create billing detail for each payment.
   * 2. Bill all payments
   */
  private void handleDisallowRefund(List pList) throws Exception 
  {
      logger.debug("Entering handleDisallowRefund...");
      EODRecordVO recordVO = null;
      if(pList != null) {
        logger.debug("pList size is: " + pList.size());
        for (int i = 0; i < pList.size(); i++) {
            recordVO = (EODRecordVO)pList.get(i);
            createBillingDetail(recordVO);
        }
      }
      logger.debug("Exiting handleDisallowRefund...");
  }  
  
  /**
   * Mark all payments in the list Billed.
   */
  private void billPaymentList (List pList) throws Exception {
      logger.debug("Entering billPaymentList...");
      if(pList != null) {
          logger.debug("pList size is: " + pList.size());
          for (int i = 0; i < pList.size(); i++) {
              EODRecordVO recordVO = (EODRecordVO)pList.get(i);
              dao.doUpdatePaymentBillStatus(recordVO.getPaymentId(),eodBatchTime,ARConstants.ACCTG_STATUS_BILLED);
          }
      }
      logger.debug("Exiting billPaymentList...");
  }
  
  /**
   * Mark all payments in the list Billed.
   */
  private void billRefundList (List rList) throws Exception {
      logger.debug("Entering billRefundList...");
      if(rList != null) {
          logger.debug("rList size is: " + rList.size());
          for (int i = 0; i < rList.size(); i++) {
              EODRecordVO recordVO = (EODRecordVO)rList.get(i);
              dao.doUpdateRefundBillStatus(recordVO.getRefundId(),eodBatchTime,ARConstants.ACCTG_STATUS_BILLED);
          }
      }
      logger.debug("Exiting billRefundList...");
  }  
  /**
   * Sums and returns total amount for the records in the list.
   */
  private BigDecimal getListTotal(List recordList) throws Exception
  {
      logger.debug("Entering getListTotal...");
      BigDecimal total = new BigDecimal(0);
      double amount = 0;
      EODRecordVO recordVO = null;
      
      if(recordList != null) {
          for (int i = 0; i < recordList.size(); i++) {
              recordVO = (EODRecordVO)recordList.get(i);
              amount = recordVO.getOrderAmount();
              logger.debug("amount is: " + amount);
              total = total.add(new BigDecimal(String.valueOf(amount)));
          }
      }
      logger.debug("returning total: " + total.doubleValue());
      logger.debug("Exiting getListTotal...");
      
      return total;
  }
  
  /**
   * Create a biling detail record and update status to Billed.
   */
  private void createBillingDetail(EODRecordVO vo) throws Exception
  {
      logger.debug("Entering createBillingDetail...");
      String transactionType = vo.getTransactionType();
      EODRecordBO bo = null;
      
      if(vo != null) {
          //purchase transaction
          if (ARConstants.TRANSACTION_TYPE_PURCHASE.equals(transactionType))
          {  
              logger.debug("Gathering data to create PAYMENT billing detail with payment id:" + vo.getPaymentId());
              vo.setOrderSequence(ps++);
              bo = new EODRegularBillBO(pcardConfig);
              bo.setEODAO(dao);
              bo.setRecordVO(vo);
              bo.processRecord();  
              dao.doCreateBillingDetail(vo,batchNumber);
                        
              dao.doUpdatePaymentBillStatus(vo.getPaymentId(),eodBatchTime,ARConstants.ACCTG_STATUS_BILLED);
          }
          //refund transaction
          else if (ARConstants.TRANSACTION_TYPE_REFUND.equals(transactionType))
          {
              logger.debug("Gathering data to create REFUND billing detail with payment id:" + vo.getPaymentId());
              vo.setOrderSequence(rs++);
              bo = new EODRefundBO(pcardConfig);
              bo.setEODAO(dao);
              bo.setRecordVO(vo);
              bo.processRecord();
                               
              dao.doCreateBillingDetail(vo,batchNumber);
              dao.doUpdateRefundBillStatus(vo.getRefundId(),eodBatchTime,ARConstants.ACCTG_STATUS_BILLED);   
          } else {
              logger.error("ERROR: Transaction type not set!");
          }
      } else {
          logger.error("object is null!" );
      }
      logger.debug("Exiting createBillingDetail...");
  }
  
  
  /**
   * This method does the following for each entry in the remaining refund map:
   * 1. Add up refund amount.
   * 2. Check billed balance. 
   *    a. if refund total is less than or equal to billed balance, create one refund billing
   *       detail with total refund amount. Mark all refunds Billed. 
   *    b. if refund total is greater than billed balance, disregard entry.
   */
  public void processRemainingRefunds() throws Exception
  {
      logger.debug("Entering processRemainingRefunds...");
      List rList = null;
      String ccNumber = null;
      
      for (int i = 0; i < refundCCs.size(); i++) {
          ccNumber = (String)refundCCs.get(i);
          rList = (ArrayList)rMap.get(ccNumber);
          BigDecimal refundTotal = getListTotal(rList); 
          EODRecordVO refundVO = (EODRecordVO)rList.get(0);
          
          try {
              logger.debug("ccNumber is: " + ccNumber.substring(ccNumber.length()-4));
          } catch (Exception ie) {
              logger.error(ie);
              SystemMessager.send("Invalid Credit Card Number. Refund id " + refundVO.getRefundId(), ie, ARConstants.SM_NOPAGE_SOURCE, 
                SystemMessager.LEVEL_PRODUCTION, 
                ARConstants.EOD_BILLING_PROCESS_ERROR_TYPE, EOD_NOPAGE_SUBJECT, dao.getConnection());
          }

          logger.debug("refundTotal: " + refundTotal.doubleValue());
          // Use the first refund payment_id to get previously billed balance.
          
          double billedAmtDouble = dao.shouldRefundBeBilled(refundVO.getPaymentId());
          logger.debug("billedAmt: " + billedAmtDouble);
          
          // Enough amount has been previously billed. Allow refund.
          BigDecimal billedAmt = new BigDecimal(String.valueOf(billedAmtDouble));

          int c = billedAmt.compareTo(refundTotal);
          if (c >= 0) {
              // Billed balance is enough to bill refund.
              // create one refund billing detail with amount = refund total
              EODRecordVO recordVO = (EODRecordVO)rList.get(0);
              recordVO.setOrderAmount(refundTotal.doubleValue());
              logger.debug("Creating refund billing detail with amount: " + refundTotal.doubleValue());
              createBillingDetail(recordVO);
              
              // bill all refunds
              billRefundList(rList);
          } else {
              // Cannot bill refund. Write to error file.
              logger.debug("Cannot bill refunds");
              SystemMessager.send("Not enough payment to bill refund id " + refundVO.getRefundId(), new Exception("Not enough payment"), ARConstants.SM_NOPAGE_SOURCE, 
                  SystemMessager.LEVEL_PRODUCTION, 
                  ARConstants.EOD_BILLING_PROCESS_ERROR_TYPE, EOD_NOPAGE_SUBJECT, dao.getConnection());
          }   
      }
      logger.debug("Exiting processRemainingRefunds...");
  }
  
  /**
   * Load payments (for bill or add bill) from payment ids.
   * 1. If record is not ready to bill, ignore it.
   * 2. If record is not credit card payment type, update bill_status to 'Billed' and do not include it.
   * 3. Otherwise add the EODRecordVO to list.
   * 4. Return the list of EODRecordVO.
   */
    private List loadPaymentsByIds(List paymentIds) throws Exception {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering loadPaymentsByIds");
        }
        
        List payments = null;
        
        try{
            if(paymentIds != null) {
              payments = new ArrayList();
              for(int i = 0; i< paymentIds.size(); i++) {
                  String paymentId = (String)paymentIds.get(i);
                  logger.debug("Loading payment with id: " + paymentId);
                  CachedResultSet rs = dao.doGetPaymentById(paymentId); 

                  if(rs.next()) {                  
                      EODRecordBO eodRecordBO = null;
                      Object addBillId = rs.getObject(4);
                      String mon = (String)rs.getObject(3);
                      
                      if(addBillId == null || ((BigDecimal)addBillId).intValue() == 0) {
                          eodRecordBO = new EODRegularBillBO(pcardConfig);
                      } else {
                          eodRecordBO = new EODAddBillBO(pcardConfig);
                      }
                      eodRecordBO.setEODAO(dao);
                      eodRecordBO.populateFields(rs);
                      
                      //Ignore add bill if original bill has not been billed
                      EODRecordVO recordvo = eodRecordBO.getRecordVO();
                      
                      // MasterPass changes                         
                      long walletIndicator = rs.getLong(36);                      
                      recordvo.setWalletIndicator(walletIndicator);
                      
                      // BAMS changes                         
                      String cscResponseCode = (String)rs.getObject(37);
                      recordvo.setCscResponseCode(cscResponseCode);
                    	  
                      
                      if (new AccountingUtil().isNonCreditCardPaymentType(recordvo.getPaymentMethod())) {
                          logger.debug("Excluding paymentId " + paymentId);
                          dao.doUpdatePaymentBillStatus(recordvo.getPaymentId(),eodBatchTime,ARConstants.ACCTG_STATUS_BILLED);
                      } else {
                          logger.debug("Loaded payment " + paymentId);
                          payments.add(recordvo);
                      } 
                         
                      
                  } else {
                      String errorMsg = "No payment exists for payment id " + paymentId;
                      logger.error(errorMsg);
                      throw new Exception(errorMsg);
                  }
              }
            }
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting loadPaymentsByIds");
            } 
        } 
        
        return payments;
  }
  
  /**
   * Load refunds from payment ids.
   * 1. If record is not credit card payment type, update refund_status to 'Billed' and do not include it.
   * 2. Otherwise add the EODRecordVO to list.
   * 3. Return the list of EODRecordVO.
   */
    private List loadRefundsByIds(List paymentIds) throws Exception {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering loadPaymentsByIds");
        }
        
        List refunds = null;
        EODRecordBO refundbo = null;
        Connection conn = dao.getConnection();
        AccountingUtil acctutil = new AccountingUtil();
        CommonDAO commondao = new CommonDAO(conn);
        String paymentId = null;
        
        try{
            if(paymentIds != null) {
              refunds = new ArrayList();
              for(int i = 0; i< paymentIds.size(); i++) {
                  paymentId = (String)paymentIds.get(i);
                  logger.debug("Loading refund with payment id: " + paymentId);
                  CachedResultSet rs = dao.doGetRefundByPaymentId(paymentId); 

                  if(rs.next()) {        
                      refundbo = new EODRefundBO(pcardConfig);
             
                      refundbo.setEODAO(dao);
                      
                      try {
                          refundbo.populateFields(rs);
                      } catch (MissingCreditCardException mcce) {
                          logger.error(mcce);
                          SystemMessager.sendSystemMessage(conn, new Exception(mcce.getMessage() + " refund_id:" + rs.getString("refund_id")));
                      } catch (Exception e) {
                          logger.error(e);
                          SystemMessager.sendSystemMessage(conn, new Exception("Exception thrown when populating refund: " + rs.getString("refund_id")));
                      }
                      EODRecordVO refundvo = refundbo.getRecordVO();
                      String refundId = String.valueOf(refundvo.getRefundId());
                      if (new AccountingUtil().isNonCreditCardPaymentType(refundvo.getPaymentMethod())) {
                          logger.debug("Excluding refund: " + refundId);
                          dao.doUpdateRefundBillStatus(refundvo.getRefundId(),eodBatchTime,ARConstants.ACCTG_STATUS_BILLED);
                          
                      }
                      else {
                          logger.debug("Adding refund to list: " + refundId);
                          refunds.add(refundbo.getRecordVO());
                      }
                  } else {
                      String errorMsg = "No refund exists for payment id " + paymentId;
                      logger.error(errorMsg);
                      throw new Exception(errorMsg);
                  }
              }
            }
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting loadPaymentsByIds");
            } 
        } 
        
        return refunds;
  }
  
  public void setUpTestProcessNoAuth(HashMap testpMap, HashMap testrMap, ArrayList testPaymentCCs) {
        try {
            if(testpMap != null) {
                this.pMap = testpMap;
                this.rMap = testrMap;
                this.paymentCCs = testPaymentCCs;
                processNoAuth();
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
  }
  
}