package com.ftd.accountingreporting.pts.bo;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.net.InetAddress;
import java.sql.Connection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.dao.EODDAO;
import com.ftd.accountingreporting.util.EODUtil;
import com.ftd.accountingreporting.util.FileArchiveUtil;
import com.ftd.accountingreporting.util.PTSUtil;
import com.ftd.accountingreporting.util.SftpUtil;
import com.ftd.accountingreporting.util.SystemMessager;
import com.ftd.accountingreporting.util.security.FileProcessorPgp;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;

public class BAMSACKFileProcessorBO {
	private Logger logger = 
        new Logger("com.ftd.accountingreporting.pts.bo.BAMSACKFileProcessorBO");
	
	private static final String PAYMENT_TOTALS = "payment";
	private static final String REFUND_TOTALS = "refund";
	private static final String SUBMISSION_ID = "submissionid";
	private static final String JULIAN_DATE = "juliandate";
	private static final String ERROR_REC_FLAG = "errorrecflag";
	private static final String REF_NUMS = "refnums";
	
	
	public void checkAckFileStatus() throws Exception{
		Connection conn = null;

		conn = EODUtil.createDatabaseConnection();
		EODDAO dao = new EODDAO(conn);
		StringBuffer sb = null;
		try{
			String julianDate = PTSUtil.formatDate(new Date(), "yyDDD");
			//Check if the clean.bams_settlements_totals table has records for today's julain date
			//where status is "Sent". If so, get submission Id and billing_header_id for those records.
			CachedResultSet rs = dao.getSettlementDetailsByDate(julianDate);
			int cnt = 0;
			if (rs != null){
				sb = new StringBuffer();
				sb.append("Java Support, please investigate.\n");
				sb.append("The following Settlement Files did not receive a corresponding Acknowledgement File today:\n");
				while (rs.next()){
					sb.append("\nFILE_CREATED_DATE_JULIAN = "+rs.getString("FILE_CREATED_DATE_JULIAN")+"-");
					sb.append("SUBMISSION_ID = "+rs.getString("SUBMISSION_ID")+"-");
					sb.append("BILLING_HEADER_ID = "+rs.getString("BILLING_HEADER_ID")+"-");
					sb.append("FILE_NAME = "+rs.getString("FILE_NAME")+"\n");
					cnt = cnt + 1;
				}
				
			}
			
			if(sb != null && cnt > 0){
				logger.debug(sb.toString());
				
				  SystemMessengerVO sysMessage = new SystemMessengerVO();
			      sysMessage.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
			      sysMessage.setSource(ARConstants.EOD_BILLING_PROCESS);
			      sysMessage.setType(ARConstants.EOD_BILLING_PROCESS_ERROR_TYPE);
			      sysMessage.setMessage(sb.toString());
			      SystemMessenger.getInstance().send(sysMessage, conn);
			}
		}finally {
			if (conn != null && !conn.isClosed()) {
				conn.close();
			}
		}
	}
	
	public void processAckFile() throws Exception {
		Connection conn = null;

		conn = EODUtil.createDatabaseConnection();
		EODDAO dao = new EODDAO(conn);
		ConfigurationUtil cu = ConfigurationUtil.getInstance();
		String localFilePath = cu.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT,"BamsAckLocalLocation");
		String toAddress = cu.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT,"BamsAckSupportEmailGroup");
		int fileSendRetryCount = Integer.parseInt(cu.getProperty(ARConstants.CONFIG_FILE, "eodFileSendRetryCount"));
		long fileSendRetryInterval = Long.parseLong(cu.getProperty(ARConstants.CONFIG_FILE, "eodFileSendRetryInterval"));
		int curFileSendRetryCount = 0;
		String subject = "";
		String content = "";
			try{
			archiveFiles();
			boolean fileTransferResult = false;
			InetAddress addr = InetAddress.getLocalHost();
			String hostname = addr.getHostName();
			while (true) {
				try {
					fileTransferResult = getBAMSACKFileFromRemoteServer();
	
				} catch (Exception e) {
					fileTransferResult = false;
				}
				if (fileTransferResult == true) {
					logger.debug("File Transfer Successful : ");
					decryptACKFiles();
					break;
				} else if (fileTransferResult == false && curFileSendRetryCount > fileSendRetryCount) {
					sendSystemMessage(conn,new Exception("BAMS Acknowledgement- Connection to server failed after trying for "+ (curFileSendRetryCount * fileSendRetryInterval)/ 60000 + " minutes."));
					break;
				} else {
					Thread.currentThread().sleep(fileSendRetryInterval);
				}
				curFileSendRetryCount++;
			}
			if (fileTransferResult) {
				String fileName = null;
				File[] files = (new File(localFilePath)).listFiles();
				if (files != null && files.length != 0) {
					for (int i = 0; i < files.length; i++) {
						try {
							if (files[i].isFile()) {
								fileName = files[i].getName();
								if (fileName != null&& (!fileName.endsWith("GPG") || !fileName.endsWith("gpg"))) {
									//Mask credit card in ack file.
									FileArchiveUtil.overwriteBAMSFileContent(files[i].getPath(), 1, ARConstants.CC_MASK, "A");
									Map<String, String> ackMap = getACKValuesFromFile(files[i]);
									String paymentTotal = ackMap.get(PAYMENT_TOTALS);
									String refundTotal = ackMap.get(REFUND_TOTALS);
									double payTotDbl = PTSUtil.amountStringToDbl(paymentTotal);
									double refundTotDbl = PTSUtil.amountStringToDbl(refundTotal);
									String submissionId = ackMap.get(SUBMISSION_ID);
									String julianDate = ackMap.get(JULIAN_DATE);
									String refNums = ackMap.get(REF_NUMS);
									boolean errorRecExists = false;
									if (ackMap.containsKey(ERROR_REC_FLAG) && "Y".equalsIgnoreCase(ackMap.get(ERROR_REC_FLAG))) {
										errorRecExists = true;
									}
									CachedResultSet rs = dao.getSettlementTotals(submissionId, julianDate);
									double paymentTotalDB = 0.0;
									double refundTotalDB = 0.0;
									boolean dbRecExists = false;
									if (rs != null && rs.next()) {
										paymentTotalDB = rs.getDouble("payment_total");
										refundTotalDB = rs.getDouble("refund_total");
										dbRecExists = true;
									}
									logger.debug("Payment Total from DB: "+paymentTotalDB);
									logger.debug("Payment Total from File: "+payTotDbl);
									logger.debug("Refund Total from DB: "+refundTotalDB);
									logger.debug("Refund Total from File: "+refundTotDbl);
									subject = "BAMS Settlement Failed";
									ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
									String companies = configUtil.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, ARConstants.PTS_SETTLEMENT_COMPANIES);
				                	String [] companyArr = companies.split(",");
				                	String companyId = null;
				                	if(submissionId != null && julianDate != null){
				                		int submissionIdInt = Integer.parseInt(submissionId);
					                	companyId = companyArr[submissionIdInt-1];
					                }
									if (errorRecExists) {
										content = "Error records found in acknowledgement file "+ localFilePath+fileName+" on server "+hostname+". Manual verification required.";
										logger.debug("A record found in the file "+ fileName+ " , sending email to support team.");
										PTSUtil.sendEmail(toAddress,ARConstants.BAMS_EMAIL_FROM_ADDRESS,subject, content);
										dao.updateSettlementTotalsStatus(submissionId, julianDate,ARConstants.SettlementStatus.Failed.toString());
										//update clean.order_bills
										
					                	if(submissionId != null && julianDate != null && companyId != null){
					                		dao.doUpdateOrderBills(companyId, submissionId, julianDate, refNums);
					                	}
					                	
									}else if (dbRecExists) {
										if (payTotDbl != paymentTotalDB || refundTotDbl != refundTotalDB) {
											logger.debug("Payment/Refund totals in file "+ fileName+ " didn't match with database values, sending email to Support team");
											content = "Payment/Refund totals not matched in acknowledgement file "+localFilePath+ fileName+ " on server "+hostname+". Manual verification required.";
											PTSUtil.sendEmail(toAddress, ARConstants.BAMS_EMAIL_FROM_ADDRESS, subject, content);
											dao.updateSettlementTotalsStatus(submissionId, julianDate, ARConstants.SettlementStatus.Failed.toString());
										}  else {
											logger.debug("Totals matched and no error records found in file : "+ localFilePath + fileName);
											dao.updateSettlementTotalsStatus(submissionId, julianDate,ARConstants.SettlementStatus.Settled.toString());
										}
										if(submissionId != null && julianDate != null && companyId != null){
					                		dao.doUpdateOrderBills(companyId, submissionId, julianDate, null);
					                	}
									} else { 
										subject = "Settlement Totals not found in database.";
										content = "Settlement Totals record not found in database for file "+ fileName+" on server "+hostname+ ". File is not processed";
										logger.debug("Total record doesn't exists. Not able to process ");
										
										sendSystemMessage(conn, new Exception("Settlement Totals record not found in database for file "+ fileName+" on server "+hostname+ ". File is not processed"));
	
									}
								}
	
							}
						} catch (Exception e) {
							logger.error("Error Occured while processing ACK File.", e);
							sendSystemMessage(conn,new Exception("Failure to process bams acknowledgement file "+localFilePath+ fileName+ " on server "+hostname));						
						} 
					}
				}else{
					sendSystemMessage(conn, new Exception("Not able to find BAMS Acknowledgement file to process"));
				}
	
			}
	
		}finally {
		if (conn != null && !conn.isClosed()) {
			conn.close();
		}
	}
	}
	
	
    /**
     * This method utilizes the FTD Utility to retrieve the CCDETAIL file
     * @return File
     * @throws Exception
     */
    private boolean getBAMSACKFileFromRemoteServer() throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering getBAMSACKFileFromRemoteServer");
        }
        boolean retFlag = false;
        ByteArrayOutputStream ccRecon = null;
        try{        
            /* retrieve server name to SFTP to */
        	ConfigurationUtil cu = ConfigurationUtil.getInstance();
            String ftpServer = cu.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, "BamsAckSFtpServer");
            String ftpLocation = cu.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, "BamsAckSFtpLocation");
            String username = cu.getSecureProperty(ARConstants.CONFIG_CONTEXT, "BamsAckSFtpUsername");
            String password = cu.getSecureProperty(ARConstants.CONFIG_CONTEXT, "BamsAckSFtpPassword");
            //String remoteFile = cu.getProperty(ARConstants.CONFIG_FILE, "ccSettlementFileName");
            String localFileDirectory = cu.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, "BamsAckLocalLocation"); 
            String securityPrivateKeyPath = cu.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, "BamsSSHPrivateKeyPath");
            String securityPrivateKeyPassphrase = cu.getSecureProperty(ARConstants.CONFIG_CONTEXT, "BamsSSHPrivateKeyPassphrase");
            
                
            //remoteFile = ftpLocation + "/" + remoteFile;
            
            SftpUtil ftpUtil = new SftpUtil();
            /* log into FTP Server and download file to local*/
            logger.info(String.format("ftpServer=%s, ftpLocation=%s, username=%s, localFileDirectory=%s,securityPrivateKeyPath=%s.", 
            		ftpServer,
            		ftpLocation,
            		username,
            		localFileDirectory,
            		securityPrivateKeyPath
            		));
            ftpUtil.downloadFilestoLocalviaSSH("GPG", ftpLocation, localFileDirectory, ftpServer, username,securityPrivateKeyPath,securityPrivateKeyPassphrase, password);
            retFlag = true;
            
        }catch(Exception e){
        	logger.error("Error occured while fetching files from sftp server", e);
        	retFlag = false;
        }
        finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting getCCReconFile");
            } 
        }
        return retFlag;
        
    }
    
    private void decryptACKFiles() throws Exception
    {
    	ConfigurationUtil cu = ConfigurationUtil.getInstance();
    	String localFilePath = cu.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, "BamsAckLocalLocation");
    	
    	String privateKeyPath = ConfigurationUtil.getInstance().getFrpGlobalParm(
                ARConstants.CONFIG_CONTEXT, "BamsAckSecurityPrivateKeyPath");
        String passphrase = ConfigurationUtil.getInstance().getSecureProperty(
        		ARConstants.CONFIG_CONTEXT, "BamsAckSecurityPrivateKeyPassphrase");
    	
    	String fileName = null;
    	File[] encryptedFiles = (new File(localFilePath)).listFiles();
    	if(encryptedFiles != null) {
    		boolean decryptFlag = false;
    		// decrypt each file
            for(int i = 0; i < encryptedFiles.length; i++) {
                if(encryptedFiles[i].isFile()) {
                    fileName = encryptedFiles[i].getName();
                    if(fileName != null && (fileName.endsWith("GPG") || fileName.endsWith("gpg"))) {
                    	try{
                    		decryptFlag = false;
                    		FileProcessorPgp.decrypt(localFilePath+fileName, privateKeyPath, passphrase);
                    		decryptFlag = true;
                    	}catch(Exception e){
                    		logger.error("Error occured while decrypting file :" +fileName, e);
                    		decryptFlag = false;
                    	}
                    	if(decryptFlag){
                    		//if encrypted file is successfully decrypted then move the encrypted file to archive folder.
                    		// Create destination directory
                            File toFile = new File(localFilePath+"/archive");
                    		boolean flag = encryptedFiles[i].renameTo(new File(toFile, fileName));
                    		if(flag){
                    			logger.debug(encryptedFiles[i].getName()+ " is archived");
                    		}
                    		
                    	}
                    }
                    
                }
            }
    	}
    }
    
    /**
     * Move files to the archive directory
     * @throws java.lang.Exception
     */
      private void archiveFiles() throws Exception
      {
    	  ConfigurationUtil cu = ConfigurationUtil.getInstance();	
          logger.debug("BAMSACKFileProcessorBO entering archiveFiles...");
          String fromDir = cu.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT,"BamsAckLocalLocation");
          String toDir = cu.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT,"BamsAckLocalArchiveLocation");
                  
          FileArchiveUtil.moveToArchive(fromDir, toDir, false);
          logger.debug("BAMSACKFileProcessorBO exiting archiveFiles...");
      }
      
      private Map<String, String> getACKValuesFromFile(File inputFile) throws Exception{
    	  Map<String, String> rMap = new HashMap<String, String>();
    	  BufferedReader br = new BufferedReader(new FileReader(inputFile));
    	  // Open the file for writing.
          
          String salesAmt = null;
          String refundAmt = null;
          String submissionId = null;
          String yyddd = null;
          String line;
          StringBuffer sb = null;
          try{
	          while ((line = br.readLine()) != null) {
	             if(line != null && line.startsWith("B")){
	            	 salesAmt = line.substring(1, 12);
	            	 salesAmt = PTSUtil.parseSignedValues(salesAmt);
	            	 refundAmt = line.substring(12, 23);
	            	 refundAmt = PTSUtil.parseSignedValues(refundAmt);
	            	 yyddd = line.substring(43, 48);
	            	 submissionId = line.substring(48, 49);            	 
	             }else if ( line != null && line.startsWith("A")){
	            	 String refNum = PTSUtil.parseSignedValues(line.substring(44, 52));
	            	 if(refNum != null && !refNum.trim().equals("")){
	            		 if(sb == null){
		            		 sb = new StringBuffer();
		            		 sb.append("'"+refNum+"'");
		            	 }else{
		            		 sb.append(",");
		            		 sb.append("'"+refNum+"'");
		            	 }
	            	 }
	            	 rMap.put(ERROR_REC_FLAG, "Y");
	             }
	             
	          }
	          if(sb != null){
	        	  logger.debug("ACK file Ref Nums : "+sb.toString());
	        	  rMap.put(REF_NUMS, sb.toString());
	          }
	          
	          rMap.put(PAYMENT_TOTALS, salesAmt);
	       	  rMap.put(REFUND_TOTALS, refundAmt);
	       	  rMap.put(SUBMISSION_ID,submissionId);
	       	  rMap.put(JULIAN_DATE, yyddd);
          }finally{
        	  br.close();
          }
    	  return rMap;
      }
      
      
      /**
       * This method sends a message to the System Messenger.
       *
       * @param String message
       * @returns String message id
       */
      public void sendSystemMessage(Connection conn, Throwable t)
      {
      	String logMessage = t.getMessage();
      	try
      	{
          //build system vo
          // SystemMessage closes connection passed in. Therefore, create a new connection.
      	  
          logger.debug(logMessage);
          SystemMessengerVO sysMessage = new SystemMessengerVO();
          sysMessage.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
          sysMessage.setSource(ARConstants.EOD_BILLING_PROCESS);
          sysMessage.setType(ARConstants.EOD_BILLING_PROCESS_ERROR_TYPE);
          sysMessage.setMessage(logMessage);
          SystemMessenger.getInstance().send(sysMessage, conn, false);
      	}
      	catch (Exception e)
      	{
      		logger.error("Sending system message failed. Message=" + logMessage);
      		logger.error(e);
      	}
      }

}
