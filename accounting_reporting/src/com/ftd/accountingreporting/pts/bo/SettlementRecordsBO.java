package com.ftd.accountingreporting.pts.bo;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.util.PTSUtil;
import com.ftd.accountingreporting.vo.EODRecordVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

public class SettlementRecordsBO {
	private static Logger logger  = new Logger("com.ftd.accountingreporting.pts.bo.SettlementRecordsBO");
	
	private Map<String, String> paymentExtMap;
	
	public SettlementRecordsBO(Map<String, String> payExtMap){
		paymentExtMap = payExtMap;
	}
	
	public SettlementRecordsBO(){
		
	}
	
	public String buildMRecord(String companyId, int recordSeq) throws Exception{
		if(logger.isDebugEnabled()) {
            logger.debug("Entering buildMRecord");
        }
        ConfigurationUtil cu = ConfigurationUtil.getInstance();
        
        StringBuffer mRecord = new StringBuffer();
        //M Record ID
        mRecord.append(ARConstants.M_RECORD_ID);
        String merchantAcc = cu.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, ARConstants.MERCHANT_ACCOUNT+companyId);
        // Merchant Account Number
        mRecord.append(StringUtils.leftPad(merchantAcc, 12, "0"));
        //Reference ID
        mRecord.append(ARConstants.M_REFERENCE_ID);
        String merchantSecurityCode = cu.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, "MERCHANT_SECURITY_CODE");
        //Field 4 Merchant ID/Security Code
        mRecord.append(merchantSecurityCode);
        //Submission Type
        mRecord.append(ARConstants.M_SUBMISSION_TYPE);
        Calendar cal = new GregorianCalendar();
        //Submission Create Date Julian format yyDDD
        mRecord.append(PTSUtil.formatDate(new Date(), "yyDDD"));
        //Submission Sequence Number.
        mRecord.append(PTSUtil.getComapanyMap().get(companyId));
        //Field 8 Security Code
        mRecord.append(merchantSecurityCode);
        //Submission Create Date Gregorian format
        mRecord.append(PTSUtil.formatDate(new Date(), "MMddyy"));
        //Record Sequence Number
        mRecord.append(StringUtils.leftPad(String.valueOf(recordSeq), 6, "0"));
        //Space filled for 11 through 16 fields. 1,1,1,3,16,and 4. Total 26 spaces.
        mRecord.append(StringUtils.rightPad("", 26, " "));
        
        return mRecord.toString();
	}
	
	public String buildNRecord(String companyId, int recordSeq) throws Exception{
		if(logger.isDebugEnabled()) {
            logger.debug("Entering buildNRecord");
        }
        ConfigurationUtil cu = ConfigurationUtil.getInstance();
        
        StringBuffer nRecord = new StringBuffer();
        //M Record ID
        nRecord.append(ARConstants.N_RECORD_ID);
        String merchantName = cu.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, ARConstants.MERCHANT_NAME+companyId);
        String merchantCity = cu.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, ARConstants.MERCHANT_CITY+companyId);
        //Convert merchant name to upper case 
        if(merchantName != null){
        	merchantName = merchantName.toUpperCase();
        }
        //merchant name
        nRecord.append(StringUtils.rightPad(merchantName, 19, " "));
        //merchant city
        nRecord.append(StringUtils.rightPad(merchantCity, 13, " "));
        //State/Province/Count
        nRecord.append(StringUtils.rightPad(ARConstants.N_STATE, 3, " "));
        //Sequence Number
        nRecord.append(StringUtils.leftPad(String.valueOf(recordSeq), 6, "0"));
        //Filler -- 38 spaces
        nRecord.append(StringUtils.rightPad("", 38, " "));
        return nRecord.toString();
	}
	
	public String buildTRecord(double salesAmt, double refundAmt, int recordSeq) throws Exception{
		if(logger.isDebugEnabled()) {
            logger.debug("Entering builTNRecord");
            logger.debug("Total Sales Amount : "+salesAmt);
            logger.debug("Total refund Amount : "+ refundAmt);
        }
        StringBuffer tRecord = new StringBuffer();
        //T Record ID
        tRecord.append(ARConstants.T_RECORD_ID);
        // Total Sales Deposit Amount
        if(salesAmt > 0){
        	String finalSalesAmt = formatAmount(salesAmt);
        	logger.debug("Final Sales Amount : "+ finalSalesAmt);
        	tRecord.append(StringUtils.leftPad(finalSalesAmt, 12, "0"));
        }else{
        	tRecord.append(StringUtils.leftPad("0", 12, "0"));
        }
        
        //Total Credit Amount
        if(refundAmt > 0){
        	String finalRefundAmt = formatAmount(refundAmt);
        	logger.debug("Final Refund Amount : "+ finalRefundAmt);
        	tRecord.append(StringUtils.leftPad(finalRefundAmt, 12, "0"));
        }else{
        	tRecord.append(StringUtils.leftPad("0", 12, "0"));
        }
        //cash advance
        tRecord.append(StringUtils.leftPad("0", 12, "0"));
        //total sales deposit/auth request amount
        tRecord.append(StringUtils.leftPad("0", 12, "0"));
        //toatal cash advance deposit auth request amount
        tRecord.append(StringUtils.leftPad("0", 12, "0"));
        //Record sequence number
        tRecord.append(StringUtils.leftPad(String.valueOf(recordSeq), 6, "0"));
        
        return tRecord.toString();
	}
	
	public String buildDRecord(EODRecordVO recordVO, int recordSeq) throws Exception{
		if(logger.isDebugEnabled()) {
            logger.debug("Entering buildDRecord");
        }
		ConfigurationUtil cu = ConfigurationUtil.getInstance();
		String merchantCategoryCode = cu.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, "MERCHANT_CATEGORY_CODE");
		
        StringBuffer dRecord = new StringBuffer();
        
        //D record id
        dRecord.append(ARConstants.D_RECORD_ID);
        //Cardholder Account Number
        dRecord.append(StringUtils.leftPad(recordVO.getCreditCardNumber(), 16, "0"));
        //Transaction Code
        if("1".equalsIgnoreCase(recordVO.getTransactionType())){
        	dRecord.append(ARConstants.TRANSACTION_CODE_SALES);
        }else if("3".equalsIgnoreCase(recordVO.getTransactionType())){
        	dRecord.append(ARConstants.TRANSACTION_CODE_CREDIT);
        }
        //Transaction Amount
        if (recordVO.getOrderAmount() > 0 ){
        	String finalAmt = formatAmount(recordVO.getOrderAmount());
        	logger.debug("D Record Txn Amt : "+finalAmt);
        	dRecord.append(StringUtils.leftPad(finalAmt, 8, "0"));
        }else{
        	dRecord.append(StringUtils.leftPad("0", 8, "0"));
        }
        //Transaction Date
        if(recordVO.getAuthorizationDate() != null){
        	dRecord.append(PTSUtil.formatDate(recordVO.getAuthorizationDate().getTime(), "MMdd"));
        }else{
        	dRecord.append(StringUtils.leftPad("", 4, "0"));
        }
        //authorization code
        if("1".equalsIgnoreCase(recordVO.getTransactionType()) && recordVO.getApprovalCode() != null){
        	dRecord.append(StringUtils.rightPad(recordVO.getApprovalCode(), 6, " "));
        }else{
        	dRecord.append(StringUtils.rightPad("", 6, " "));
        }
        //authorization date
        if("1".equalsIgnoreCase(recordVO.getTransactionType())){
        	String transmissionDate = paymentExtMap.get("TrnmsnDateTime");
        	if("Y".equalsIgnoreCase(recordVO.getVoiceAuthFlag()) && recordVO.getAuthorizationDate() != null){
        		dRecord.append(PTSUtil.formatDate(recordVO.getAuthorizationDate().getTime(), "MMdd"));
        	}else if(!"Y".equalsIgnoreCase(recordVO.getVoiceAuthFlag()) && transmissionDate != null){
        		dRecord.append(PTSUtil.getDateStringByFormat(transmissionDate, "yyyyMMddhhmmss", "MMdd"));
        	}else{
        		dRecord.append(StringUtils.leftPad("", 4, "0"));
        	}
        }else{
        	dRecord.append(StringUtils.leftPad("", 4, "0"));
        }
        
        //card expiration date
        if("1".equalsIgnoreCase(recordVO.getTransactionType()) && recordVO.getCcExpDate() != null){
        	String expDate = recordVO.getCcExpDate();
        	String expDateFinal = expDate.substring(0, 2)+expDate.substring(expDate.length()-2);
        	dRecord.append(expDateFinal);
        }else{
        	dRecord.append(StringUtils.leftPad("", 4, "0"));
        }
        
        //reference number
        String refNum = paymentExtMap.get("RefNum");
        if(refNum != null){
        	dRecord.append(StringUtils.leftPad(refNum, 8, "0"));
        }else{
        	dRecord.append(StringUtils.leftPad("", 8, "0"));
        }
        //Sequence Number
        dRecord.append(StringUtils.leftPad(String.valueOf(recordSeq), 6, "0"));
        //filler
        dRecord.append(" ");
        //prepaid card indicator
        dRecord.append(" ");
        //Transaction Identifier
        String transId = paymentExtMap.get("TransID");
        if("1".equalsIgnoreCase(recordVO.getTransactionType()) && !"Y".equalsIgnoreCase(recordVO.getVoiceAuthFlag()) && transId != null && transId.length() >= 15){
        	dRecord.append(transId.substring(0, 15));
        }else{
        	dRecord.append(StringUtils.leftPad("", 15, "0"));
        }
        //Filler
        dRecord.append(" ");
        //Validation Code
        if(transId != null && transId.length() > 15){
        	dRecord.append(StringUtils.rightPad(transId.substring(15, transId.length()), 4, " "));
        }else{
        	dRecord.append(StringUtils.rightPad("", 4, " "));
        }
        
        
        return dRecord.toString();
	}
	
	public String buildERecord(EODRecordVO recordVO, int recordSeq) throws Exception{
		if(logger.isDebugEnabled()) {
            logger.debug("Entering buildERecord");
        }
		ConfigurationUtil cu = ConfigurationUtil.getInstance();
		String merchantCategoryCode = cu.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, "MERCHANT_CATEGORY_CODE");
		
        StringBuffer eRecord = new StringBuffer();
        //E Record ID
        eRecord.append(ARConstants.E_RECORD_ID);
        //Store ID
        eRecord.append(StringUtils.rightPad("",4, " "));
        //Terminal ID
        eRecord.append(StringUtils.rightPad("",4, " "));
        //Merchant Category Code
        eRecord.append(merchantCategoryCode);
        //Card Holder Activated Terminal
        
        if("MC".equalsIgnoreCase(recordVO.getCreditCardType())){
        	eRecord.append("6");
        }else{
        	eRecord.append(" ");	        	
        }
        
        
        //Magnetic Stripe Status
        if ("MC".equalsIgnoreCase(recordVO.getCreditCardType())) {
        	eRecord.append(ARConstants.E_MAGNETIC_STRIPE_STATUS_MC);
        } else {
        	eRecord.append(ARConstants.E_MAGNETIC_STRIPE_STATUS);
        }
        //Visa Service Development
        //if cc type is visa
        if("VI".equalsIgnoreCase(recordVO.getCreditCardType())){
        	eRecord.append("1");
        }else{
        	eRecord.append(" ");	        	
        }
        //Filler -- space fill 6
        eRecord.append(StringUtils.rightPad("", 6, " "));
        //Authorization Source
        if("3".equalsIgnoreCase(recordVO.getTransactionType())){
        	eRecord.append(" ");
        }else{
        	if("Y".equalsIgnoreCase(recordVO.getVoiceAuthFlag())){
        		eRecord.append("D");
        	}else{
        		eRecord.append("5");
        	}
        }
        //POS Terminal Capability
        eRecord.append(ARConstants.E_POS_TERMINAL_CAPABILITY);
        //Entry Mode
        if ("MC".equalsIgnoreCase(recordVO.getCreditCardType())) {
        	eRecord.append("F");
        } else {
        	eRecord.append("1");
        }
        //Card Holder Id
        eRecord.append("4");
        //Merchant Zip code
        eRecord.append(StringUtils.rightPad(ARConstants.E_MERCHANT_ZIP_CODE, 9, " "));
        //Filler 6 spaces
        eRecord.append(StringUtils.rightPad("", 6, " "));
        //Sequence Number
        eRecord.append(StringUtils.leftPad(String.valueOf(recordSeq), 6, "0"));
      //Filler 32 spaces
        eRecord.append(StringUtils.rightPad("", 32, " "));
        
        return eRecord.toString();
	}
	
	public String buildSRecord(EODRecordVO recordVO, int recordSeq) throws Exception{
		if(logger.isDebugEnabled()) {
            logger.debug("Entering buildSRecord");
        }
		StringBuffer sRecord = new StringBuffer();
        //S Record Id
		sRecord.append(ARConstants.S_RECORD_ID);
		//Quasi Cash Indicator
		sRecord.append(ARConstants.S_QUASI_CASH_INDICATOR);
		//Special Condition Indicator
		if("VI".equalsIgnoreCase(recordVO.getCreditCardType())){
		    //User Story 2201 - need to pass number indicated below based off of ECI value for Cardinal Commerce orders, otherwise 7
			//ECI =01, Visa =5.
			//ECI =02, Visa =6.
			//ECI =03, Visa =7.
			//ECI =04, Visa =8
			if(recordVO.getCardinalVerifiedFlag() != null && recordVO.getCardinalVerifiedFlag().equalsIgnoreCase("Y")){
				String eci = paymentExtMap.get("Eci");
		        if(eci != null){
		        	if(eci.equals("01")){
		        		sRecord.append(StringUtils.rightPad(ARConstants.CARDINAL_COMMERCE_SPECIAL_CONDITION_INDICATOR_5, 1, " "));	
		        	}
		        	else if(eci.equals("02")){
		        		sRecord.append(StringUtils.rightPad(ARConstants.CARDINAL_COMMERCE_SPECIAL_CONDITION_INDICATOR_6, 1, " "));	
		        	}
		        	else if(eci.equals("03")){
		        		sRecord.append(StringUtils.rightPad(ARConstants.CARDINAL_COMMERCE_SPECIAL_CONDITION_INDICATOR_7, 1, " "));	
		        	}
		        	else if(eci.equals("04")){
		        		sRecord.append(StringUtils.rightPad(ARConstants.CARDINAL_COMMERCE_SPECIAL_CONDITION_INDICATOR_8, 1, " "));	
		        	}	        	
		        }
			}
			else{
				sRecord.append(StringUtils.rightPad("7", 1, " "));	
			}	
    	}else{
    		sRecord.append(StringUtils.rightPad("", 1, " "));
    	}
        //Clearing Sequence
		sRecord.append(StringUtils.leftPad("", 2, "0"));
		//Clearing Count
		sRecord.append(StringUtils.leftPad("", 2, "0"));
		//Cust Serv Phone Prt Flg
		sRecord.append("Y");
		//Filler
		sRecord.append(StringUtils.rightPad("", 34, " "));
		//Sequence Number
		sRecord.append(StringUtils.leftPad(String.valueOf(recordSeq), 6, "0"));
		//Special Use Fields
		////Time
		sRecord.append(StringUtils.leftPad("", 6, "0"));
		////Order Number
		String orderNumber = recordVO.getOrderNumber();
        if(orderNumber != null){
        	sRecord.append(StringUtils.rightPad(orderNumber, 13, " "));
        }else{
        	sRecord.append(StringUtils.rightPad("", 13, " "));
        }
		//Merchant Transaction Indicator
		sRecord.append(StringUtils.rightPad("", 1, " "));
		//Certified for MasterCard Merchant Advice Code
		sRecord.append(StringUtils.rightPad("", 1, " "));
		//MasterCard Transaction Category Indicator
		sRecord.append(StringUtils.rightPad("", 2, " "));
		//Filler
		sRecord.append(StringUtils.rightPad("", 1, " "));
		//Automated Fuel Dispenser (AFD) Completion Advice
		sRecord.append(StringUtils.rightPad("", 1, " "));
		//Filler
		sRecord.append(StringUtils.rightPad("", 7, " "));
        
		return sRecord.toString();
	}
	
	public String buildXD05Record(EODRecordVO recordVO, int recordSeq) throws Exception {
		if(logger.isDebugEnabled()) {
            logger.debug("Entering buildXD05Record");
        }
		StringBuffer xd05Record = new StringBuffer();
        //XD05 Record Id
		xd05Record.append(ARConstants.XD05_RECORD_ID);
        //Sequence Number
		xd05Record.append(StringUtils.leftPad(String.valueOf(recordSeq), 6, "0"));
        //Purchase ID Format
		xd05Record.append("1");
        //No Show Indicator
		xd05Record.append("0");
        //Extra Charges Indicator
		xd05Record.append(StringUtils.rightPad("", 6, " "));
        //Market Specific Date 
		xd05Record.append(StringUtils.leftPad("", 6, "0"));
		//Total Auth Amount
        if (recordVO.getOrderAmount() > 0 ){
        	String totalAuthAmtStr = String.valueOf(recordVO.getOrderAmount());
        	logger.debug("XD05 Record Total Auth Amount : "+ totalAuthAmtStr);
        	String[] totalAuthAmtArr = totalAuthAmtStr.split("\\.");
        	String cents = totalAuthAmtArr[1];
        	if(cents.length() == 1){
        		cents = cents+"0";
        	}
        	/*
        	* SP-72: BAMS - Additional validations for fields in settlement file.
        	* Rounding order amount to 2 decimals. To make it consistent with other record types.
        	*/
        	else if(cents.length() > 2){ 
        		cents = cents.substring(0,2);
        	}
        	String finalAmt = totalAuthAmtArr[0]+cents;
        	logger.debug("XD05 Record Total Auth Amount : "+finalAmt);
        	String initialAuthAmt = "0";
        	//User Story 5044 | BAMS Settlement File - Update XD05 Record
        	//Retrieve initial auth amount and compare to Order Amount
        	//If the amounts equal, pass zeroes in this field, otherwise send order amount
        	if (recordVO.getInitialAuthAmount()> 0 ){
            	String initAmtStr = String.valueOf(recordVO.getInitialAuthAmount());
            	logger.debug("XD05 Record Initial Auth Amt : "+ initAmtStr);
            	String[] initAmtArr = initAmtStr.split("\\.");
            	cents = initAmtArr[1];
            	if(cents.length() == 1){
            		cents = cents+"0";
            	}
            	/*
            	* SP-72: BAMS - Additional validations for fields in settlement file.
            	* Rounding order amount to 2 decimals. To make it consistent with other record types.
            	*/
            	else if(cents.length() > 2){
            		cents = cents.substring(0,2);
            	}
            	initialAuthAmt = initAmtArr[0]+cents;
            	logger.debug("XD05 Record Initial Auth Amt : "+initialAuthAmt);
            }
        	logger.info("compare value: " + Double.valueOf(initialAuthAmt).compareTo(Double.valueOf(finalAmt)) );
        	if(Double.valueOf(initialAuthAmt).compareTo(Double.valueOf(finalAmt)) == 0){
        		xd05Record.append(StringUtils.leftPad("0", 12, "0"));
        	}
        	else{
        		xd05Record.append(StringUtils.leftPad(finalAmt, 12, "0"));
        	}
        }else{
        	xd05Record.append(StringUtils.leftPad("0", 12, "0"));
        }
		//Market Specific Auth Indicator
        String mktSpcAuthInd = paymentExtMap.get("MrktSpecificDataInd");
        if(mktSpcAuthInd != null){
        	xd05Record.append(StringUtils.rightPad(mktSpcAuthInd, 1, " "));
        }else{
        	xd05Record.append(StringUtils.rightPad("", 1, " "));
        }
		//Visa Card level indicator
        String cardLevelInd = paymentExtMap.get("CardLevelResult");
        if(cardLevelInd != null){
        	xd05Record.append(StringUtils.rightPad(cardLevelInd, 2, " "));
        }else{
        	xd05Record.append(StringUtils.rightPad("", 2, " "));
        }
		//Service Code
		xd05Record.append(StringUtils.rightPad("", 3, " "));
		//Visa POS Condition Code
		String posCondCode = paymentExtMap.get("POSCondCode");
        if(posCondCode != null){
        	xd05Record.append(StringUtils.rightPad(posCondCode, 2, " "));
        }else{
        	xd05Record.append(StringUtils.rightPad("", 2, " "));
        }
		//Chip Condition Code
		xd05Record.append(StringUtils.rightPad("", 1, " "));
		//Spend Qualified Indicator
		xd05Record.append(StringUtils.rightPad("", 1, " "));
        //Filler
		xd05Record.append(StringUtils.rightPad("", 34, " "));
        
		return xd05Record.toString();
	}
	
	public String buildXD01Record(EODRecordVO recordVO, int recordSeq) throws Exception {
		if(logger.isDebugEnabled()) {
            logger.debug("Entering buildXD01Record");
        }
		StringBuffer xd01Record = new StringBuffer();
        //XD01 Record Id
		xd01Record.append(ARConstants.XD01_RECORD_ID);
        //Sequence Number
		xd01Record.append(StringUtils.leftPad(String.valueOf(recordSeq), 6, "0"));
        //Initial Auth Amount
		if (recordVO.getInitialAuthAmount()> 0 ){
        	String initAmtStr = String.valueOf(recordVO.getInitialAuthAmount());
        	logger.debug("XD01 Record Initial Auth Amt : "+ initAmtStr);
        	String[] initAmtArr = initAmtStr.split("\\.");
        	String cents = initAmtArr[1];
        	if(cents.length() == 1){
        		cents = cents+"0";
        	}
        	/*
        	* SP-72: BAMS - Additional validations for fields in settlement file.
        	* Rounding order amount to 2 decimals. To make it consistent with other record types.
        	*/
        	else if(cents.length() > 2){
        		cents = cents.substring(0,2);
        	}
        	String finalAmt = initAmtArr[0]+cents;
        	logger.debug("XD05 Record Initial Auth Amt : "+finalAmt);
        	xd01Record.append(StringUtils.leftPad(finalAmt, 12, "0"));
        }else{
        	xd01Record.append(StringUtils.leftPad("0", 12, "0"));
        }
		//Auth Currency Code
		String authCurrCode = paymentExtMap.get("TxnCrncy");
        if(authCurrCode != null){
        	xd01Record.append(StringUtils.rightPad(authCurrCode, 3, " "));
        }else{
        	xd01Record.append(StringUtils.rightPad("", 3, " "));
        }
		//Auth Response Code
        String respCode = paymentExtMap.get("RespCode");
        if(respCode != null && "000".equals(respCode)){
        	xd01Record.append(respCode.substring(respCode.length()-2));
        }else{
        	xd01Record.append(StringUtils.rightPad("", 2, " "));
        }
		//Cash Back Amount
        xd01Record.append(StringUtils.leftPad("", 7, "0"));
		//ACI (Auth Characteristic Indicator)
        //User Story 2201 - need to send S for orders with Cardinal data, otherwise W
        String aciCharInd = paymentExtMap.get("ACI");
        if(aciCharInd != null){
           	xd01Record.append(StringUtils.rightPad(aciCharInd, 1, " "));
        }else{
        	xd01Record.append(StringUtils.rightPad("", 1, " "));
        }
		//AVS Response
        String avsResp = recordVO.getAVSResultCode();
        if(avsResp != null){
        	xd01Record.append(StringUtils.rightPad(avsResp, 1, " "));
        }else{
        	xd01Record.append(StringUtils.rightPad("", 1, " "));
        }
		//Acknowledgment Transaction Id
        xd01Record.append(StringUtils.leftPad("", 15, "0"));
		//Acknowledgment Validation Code
        xd01Record.append(StringUtils.rightPad("", 4, " "));
		//CVV2 Result Code
        String cvvResultCode = recordVO.getCscResponseCode();
        if(cvvResultCode != null){
        	xd01Record.append(StringUtils.rightPad(cvvResultCode, 1, " "));
        }else{
        	xd01Record.append(StringUtils.rightPad("", 1, " "));
        }
		//Filler
        xd01Record.append(StringUtils.rightPad("", 24, " "));
		       
		return xd01Record.toString();
	}
	
	public String buildXD02Record(EODRecordVO recordVO, int recordSeq) throws Exception{
		if(logger.isDebugEnabled()){
			logger.debug("Entering buildXD02Record");
		}
		ConfigurationUtil cu = ConfigurationUtil.getInstance();
		 
		StringBuffer xd02Record = new StringBuffer();
        //XV01 Record Id
		xd02Record.append(ARConstants.XD02_RECORD_ID);
		//Sequence Number
		xd02Record.append(StringUtils.leftPad(String.valueOf(recordSeq), 6, "0"));
		//Banknet Data
		String banknetData = paymentExtMap.get("BanknetData");
		String banknetRefId = "";
		String banknetDate = "";
		//Banknet Reference Id and Banknet Date
		if(banknetData != null && banknetData.length() == 13){
			banknetRefId = banknetData.substring(banknetData.length()-9);
			banknetDate = banknetData.substring(0, 4);
		}
		//Banknet Reference Id
		xd02Record.append(StringUtils.rightPad(banknetRefId, 9, " "));
		//Banknet Date
		xd02Record.append(StringUtils.leftPad(banknetDate, 4, "0"));
		//Ecomm Security Level
		xd02Record.append(ARConstants.ECOMM_SECURITY_LEVEL);
		//UCAF Status
        String ucaf = paymentExtMap.get("Ucaf");
        if (ucaf != null) {
        	xd02Record.append(ucaf);
        } else {
		    xd02Record.append("0");
        }
		//Filler 1 space
		xd02Record.append(" ");
		//Auth Amt
		String authAmt = paymentExtMap.get("TxnAmt");
		if (authAmt != null){
			double authDblAmt = Double.parseDouble(authAmt);
			String finalAmt = formatAmount(authDblAmt);
			xd02Record.append(StringUtils.leftPad(finalAmt, 12, "0"));
		}else{
			xd02Record.append(StringUtils.leftPad("", 12, "0"));
		}
		//Transaction Error Edit Code
		String tranEditErrCode = paymentExtMap.get("TranEditErrCode");
		if(tranEditErrCode == null){
			tranEditErrCode = "";
		}
		xd02Record.append(StringUtils.rightPad(tranEditErrCode, 1, " "));
		//CVC Error Code
		String cvcErrorCode = paymentExtMap.get("CCVErrorCode");
		if(cvcErrorCode == null){
			cvcErrorCode = "";
		}
		xd02Record.append(StringUtils.rightPad(cvcErrorCode, 1, " "));
		//POS Entry Mode
		String posEntryMode = paymentExtMap.get("POSEntryModeChg");
		if(posEntryMode == null){
			posEntryMode = "";
		}
		xd02Record.append(StringUtils.rightPad(posEntryMode, 1, " "));
		//Auth Time 
		String authTime = paymentExtMap.get("TrnmsnDateTime");
		if(authTime != null && authTime.trim().length()== 14){
			authTime = authTime.substring(8, 12);
		}else{
			authTime = "0001";
		}
		xd02Record.append(StringUtils.leftPad(authTime, 4, "0"));
		//Device type Indicator - space fill
		xd02Record.append("  ");
		//Auth Response Code
        String respCode = paymentExtMap.get("RespCode");
        if(respCode != null && "000".equals(respCode)){
        	xd02Record.append(respCode.substring(respCode.length()-2));
        }else{
        	xd02Record.append(StringUtils.rightPad("", 2, " "));
        }
        //Filler space fill 30 spaces
        xd02Record.append(StringUtils.rightPad("", 30, " "));
		
		return xd02Record.toString();
	}
	
	public String buildXE02Record(int recordSeq) throws Exception{
		if(logger.isDebugEnabled()){
			logger.debug("Entering buildXE02Record");
		}
		
		StringBuffer xe02Record = new StringBuffer();
        //XE01 Record Id
		xe02Record.append(ARConstants.XE02_RECORD_ID);
		//Sequence Number
		xe02Record.append(StringUtils.leftPad(String.valueOf(recordSeq), 6, "0"));
		//MCPOSData
		String mcPOSData = paymentExtMap.get("MCPOSData");
		if(mcPOSData == null){
			mcPOSData = "";
		}
		xe02Record.append(StringUtils.rightPad(mcPOSData, 12, " "));
		//Filler 58 spaces
		xe02Record.append(StringUtils.rightPad("", 58, " "));
		return xe02Record.toString();
	}
	
	public String buildXN01Record(EODRecordVO recordVO, int recordSeq) throws Exception {
		if(logger.isDebugEnabled()) {
            logger.debug("Entering buildXN01Record");
        }
		String companyId = recordVO.getCompanyId();
		logger.debug("XN01 record ---Company Id : "+ companyId);
		
		ConfigurationUtil cu = ConfigurationUtil.getInstance();

        String customerServiceNum = cu.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, ARConstants.MERCHANT_CITY+companyId);
		
		StringBuffer xn01Record = new StringBuffer();
        //XN01 Record Id
        xn01Record.append(ARConstants.XN01_RECORD_ID);
        //Sequence Number
        xn01Record.append(StringUtils.leftPad(String.valueOf(recordSeq), 6, "0"));
        //Market Specific Data 
       	xn01Record.append(StringUtils.rightPad(String.valueOf(recordVO.getPaymentId()), 15, " "));
       	//customer service number
       	xn01Record.append(StringUtils.rightPad(formatPhoneNumber(customerServiceNum), 10, " "));
        //Filler
        xn01Record.append(StringUtils.rightPad("", 45, " "));
        
		return xn01Record.toString();
	}
	
	public String buildXV01Record(EODRecordVO recordVO, int recordSeq) throws Exception {
		if(logger.isDebugEnabled()) {
            logger.debug("Entering buildXV01Record");
        }
		/*ConfigurationUtil cu = ConfigurationUtil.getInstance();
		String merchantCategoryCode = cu.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, "MERCHANT_CATEGORY_CODE");*/
		
        StringBuffer xv01Record = new StringBuffer();
        //XV01 Record Id
        xv01Record.append(ARConstants.XV01_RECORD_ID);
        //Sequence Number
        xv01Record.append(StringUtils.leftPad(String.valueOf(recordSeq), 6, "0"));
        //Local Transaction Time
        String transmissionDate = paymentExtMap.get("TrnmsnDateTime");
        if(transmissionDate != null && transmissionDate.length() == 14){
        	xv01Record.append(transmissionDate.substring(transmissionDate.length()-6));
        }else{
        	xv01Record.append(StringUtils.leftPad("", 6, "0"));
        }
        
        //Processing Code
        String discProcCode = paymentExtMap.get("DiscProcCode");
        if(discProcCode != null){
        	xv01Record.append(StringUtils.rightPad(discProcCode, 6, " "));
        }else{
        	xv01Record.append(StringUtils.rightPad("", 6, " "));
        }
        //System Trace Audit Number
        String stan = paymentExtMap.get("STAN");
        if(stan != null){
        	xv01Record.append(StringUtils.leftPad(stan, 6, "0"));
        }else{
        	xv01Record.append(StringUtils.leftPad("", 6, "0"));
        }
        //POS Entry Mode
        String discPOSEntry = paymentExtMap.get("DiscPOSEntry");
        /*
         * As per BAMS we send first 3 characters       
         */
        if(discPOSEntry != null){
        	if(discPOSEntry.length() > 3){
        		discPOSEntry = discPOSEntry.substring(0,3);
        	}
        	xv01Record.append(StringUtils.rightPad(discPOSEntry, 3, " "));
        }else{
        	xv01Record.append(StringUtils.rightPad("", 3, " "));
        }
      //Track Data Condition Code
        String discTransQualifier = paymentExtMap.get("DiscTransQualifier");
        if(discTransQualifier != null){
        	xv01Record.append(StringUtils.rightPad(discTransQualifier, 2, " "));
        }else{
        	xv01Record.append(StringUtils.rightPad("", 2, " "));
        }
        //Pos Data
        String discPOSData = paymentExtMap.get("DiscPOSData");
        if(discPOSData != null){
        	xv01Record.append(StringUtils.rightPad(discPOSData, 13, " "));
        }else{
        	xv01Record.append(StringUtils.rightPad("", 13, " "));
        }
        //Filler
        xv01Record.append(StringUtils.rightPad("", 34, " "));
        
		return xv01Record.toString();
	}
	
	public String buildXV02Record(EODRecordVO recordVO, int recordSeq) throws Exception {
		if(logger.isDebugEnabled()) {
            logger.debug("Entering buildXV02Record");
        }
		ConfigurationUtil cu = ConfigurationUtil.getInstance();
		String merchantCategoryCode = cu.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, "MERCHANT_CATEGORY_CODE");
		
        StringBuffer xv02Record = new StringBuffer();
        //XV02 Record ID
        xv02Record.append(ARConstants.XV02_RECORD_ID);
        //Sequence Number
        xv02Record.append(StringUtils.leftPad(String.valueOf(recordSeq), 6, "0"));
        //Auth Response Code
        String respCode = paymentExtMap.get("RespCode");
        if(respCode != null && "000".equals(respCode)){
        	xv02Record.append(respCode.substring(respCode.length()-2));
        }else{
        	xv02Record.append(StringUtils.rightPad("", 2, " "));
        }
        //Partial Shipment Indicator
        xv02Record.append(ARConstants.PARTIAL_SHIPMENT_INDICATOR);
        //AVS Response
        String avsResp = recordVO.getAVSResultCode();
        if(avsResp != null){
        	xv02Record.append(StringUtils.rightPad(avsResp, 1, " "));
        }else{
        	xv02Record.append(StringUtils.rightPad("", 1, " "));
        }
        //Auth Amount
        double authAmt = recordVO.getOrderAmount();
        if(authAmt > 0){
        	String finalAuthAmt = formatAmount(authAmt);
        	logger.debug("XV02 Auth Amt : "+finalAuthAmt);
        	xv02Record.append(StringUtils.leftPad(finalAuthAmt, 13, "0"));
        }else{
        	xv02Record.append(StringUtils.leftPad("", 13, "0"));
        }
        //Network Reference ID
        String discNRID = paymentExtMap.get("DiscNRID");
        if(discNRID != null){
        	xv02Record.append(StringUtils.leftPad(discNRID, 15, "0"));
        }else{
        	xv02Record.append(StringUtils.leftPad("", 15, "0"));
        }
        //Filler
        xv02Record.append(StringUtils.rightPad("", 38, " "));
        
        
        
		return xv02Record.toString();
	}
	/*
	 * This method is used to format dollar amount suitable to PTS format
	 * It removes "." between dollar amt and cents.
	 */
	private String formatAmount(double orderAmt) throws Exception{
		orderAmt = Math.round(orderAmt * 100);
		orderAmt = orderAmt/100;
		String authAmtStr = String.valueOf(orderAmt);
    	String[] authAmtArr = authAmtStr.split("\\.");
    	String cents = authAmtArr[1];
    	if(cents.length() == 1){
    		cents = cents+"0";
    	}else if(cents.length()>2){
    		cents = cents.substring(0,2);
    	}
    	String finalAuthAmt = authAmtArr[0]+cents;
    	return finalAuthAmt;
	}
	
	private String formatPhoneNumber(String phoneNum) throws Exception{
		StringBuffer sb = new StringBuffer();
		if(phoneNum != null && phoneNum.length()== 12){
			String[] phoneArr = phoneNum.split("-");
			for (String item : phoneArr) {
			    sb.append(item);
			}
		}else if (phoneNum != null && phoneNum.length() == 10){
			sb.append(phoneNum);
		}
		return sb.toString();
	}

}
