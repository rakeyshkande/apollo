package com.ftd.accountingreporting.pts.bo;

import java.io.FileWriter;
import java.util.Map;

import com.ftd.accountingreporting.util.PTSUtil;
import com.ftd.accountingreporting.vo.EODRecordVO;

import com.ftd.accountingreporting.pts.validator.PTSRecordEnum;

public class SettleCreditCardsBO {
	private static final String newLine = "\n";
	
	public int writeHeaderRecords(String companyId, int recordSeq, FileWriter out, StringBuffer validationResult)throws Exception{
		SettlementRecordsBO recordBO = new SettlementRecordsBO();
		String mRec = recordBO.buildMRecord(companyId, recordSeq);
		recordSeq = PTSUtil.getNextSequenceNumber(recordSeq);
		String nRec = recordBO.buildNRecord(companyId, recordSeq);
		recordSeq = PTSUtil.getNextSequenceNumber(recordSeq);
		
		out.write(mRec+newLine);
		out.write(nRec+newLine);
		
		/*
		 * SP-72: BAMS - Additional validations for fields in settlement file. Introducing checks on record lengths, with this release.
		 * PTSRecordEnum.validateRecord() method returns:
		 * 			0, if the record lies within specified limit.
		 * 			non-zero (the max length specified for the record), if the record exceeds the specified limit.
		 */
		int i = 0;
		
		// Validating M record
		i = PTSRecordEnum.compareRecords("M", mRec);
				
		if(i > 0)
			validationResult.append("\n Company Id: "+companyId+"\n Errors found validating Header records: Record type M is of more than specified length. "
					+ "Max length: "+i+", Actual length: "+mRec.length()+ ". \n");
		
		// Validating N record
		i = PTSRecordEnum.compareRecords("N", nRec);
		
		if(i > 0)
			validationResult.append("\n Company Id: "+companyId+"\n Errors found validating Header records: Record type N is of more than specified length. "
					+ "Max length: "+i+", Actual length: "+nRec.length()+ ". \n");
		
		return recordSeq;
	}
	
	public int writeDinersAndDiscoverPayments(EODRecordVO recordVO, int recordSeq, FileWriter out, StringBuffer validationResult) throws Exception{
		Map<String, String> payExtMap  = PTSUtil.getPaymentExtMapByPaymentId(recordVO.getPaymentId());
		SettlementRecordsBO recordBO = new SettlementRecordsBO(payExtMap);
		String eRec = recordBO.buildERecord(recordVO, recordSeq);
		recordSeq = PTSUtil.getNextSequenceNumber(recordSeq);
		String xv01Rec = recordBO.buildXV01Record(recordVO, recordSeq);
		recordSeq = PTSUtil.getNextSequenceNumber(recordSeq);
		String xv02Rec = recordBO.buildXV02Record(recordVO, recordSeq);
		recordSeq = PTSUtil.getNextSequenceNumber(recordSeq);
		String dRec = recordBO.buildDRecord(recordVO, recordSeq);
		recordSeq = PTSUtil.getNextSequenceNumber(recordSeq);
		
		out.write(eRec+newLine);
		out.write(xv01Rec+newLine);
		out.write(xv02Rec+newLine);
		out.write(dRec+newLine);
		
		/*
		* SP-72: BAMS - Additional validations for fields in settlement file.
		* Introducing checks on record lengths, with this release
		*/
		long lPaymentId = 0L;
		int i = 0;
		
		if(recordVO != null)
			lPaymentId = recordVO.getPaymentId();
		
		// Validating E record
		i = PTSRecordEnum.compareRecords("E", eRec);
		
		if(i > 0)
			validationResult.append("\n Payment Id: "+lPaymentId+"\n Errors found validating Diners And Discover Payments records: Record type E is of more than specified length. "
					+ "Max length: "+i+", Actual length: "+eRec.length()+ ". \n");
		
		// Validating XV01 record
		i = PTSRecordEnum.compareRecords("XV01", xv01Rec);
		
		if(i > 0)
			validationResult.append("\n Payment Id: "+lPaymentId+"\n Errors found validating Diners And Discover Payments records: Record type XV01 is of more than specified length. "
					+ "Max length: "+i+", Actual length: "+xv01Rec.length()+ ". \n");
		
		// Validating XV02 record
		i = PTSRecordEnum.compareRecords("XV02", xv02Rec);
		
		if(i > 0)
			validationResult.append("\n Payment Id: "+lPaymentId+"\n Errors found validating Diners And Discover Payments records: Record type XV02 is of more than specified length. "
					+ "Max length: "+i+", Actual length: "+xv02Rec.length()+ ". \n");
		
		// Validating D record
		i = PTSRecordEnum.compareRecords("D", dRec);
		
		if(i > 0)
			validationResult.append("\n Payment Id: "+lPaymentId+"\n Errors found validating Diners And Discover Payments records: Record type D is of more than specified length. "
					+ "Max length: "+i+", Actual length: "+dRec.length()+ ". \n");
		
		return recordSeq;
		
	}
	
	public int writeMasterCardPayments(EODRecordVO recordVO, int recordSeq, FileWriter out, StringBuffer validationResult) throws Exception{
		Map<String, String> payExtMap  = PTSUtil.getPaymentExtMapByPaymentId(recordVO.getPaymentId());
		SettlementRecordsBO recordBO = new SettlementRecordsBO(payExtMap);
		
		String eRec = recordBO.buildERecord(recordVO, recordSeq);
		recordSeq = PTSUtil.getNextSequenceNumber(recordSeq);
		
		String xd02Rec = recordBO.buildXD02Record(recordVO, recordSeq);
		recordSeq = PTSUtil.getNextSequenceNumber(recordSeq);
		
		String xe02Rec = recordBO.buildXE02Record(recordSeq);
		recordSeq = PTSUtil.getNextSequenceNumber(recordSeq);
		
		String dRec = recordBO.buildDRecord(recordVO, recordSeq);
		recordSeq = PTSUtil.getNextSequenceNumber(recordSeq);
		
		out.write(eRec+newLine);
		out.write(xd02Rec+newLine);
		out.write(xe02Rec+newLine);
		out.write(dRec+newLine);
		
		/*
		* SP-72: BAMS - Additional validations for fields in settlement file.
		* Introducing checks on record lengths, with this release
		*/
		long lPaymentId = 0L;
		int i = 0;
		
		if(recordVO != null)
			lPaymentId = recordVO.getPaymentId();
		
		// Validating E record
		i = PTSRecordEnum.compareRecords("E", eRec);
		
		if(i > 0)
			validationResult.append("\n Payment Id: "+lPaymentId+"\n Errors found validating MasterCard Payments records: Record type E is of more than specified length. "
					+ "Max length: "+i+", Actual length: "+eRec.length()+ ". \n");
		
		// Validating XD02 record
		i = PTSRecordEnum.compareRecords("XD02", xd02Rec);
		
		if(i > 0)
			validationResult.append("\n Payment Id: "+lPaymentId+"\n Errors found validating MasterCard Payments records: Record type XD02 is of more than specified length. "
					+ "Max length: "+i+", Actual length: "+xd02Rec.length()+ ". \n");
		
		// Validating XE02 record
		i = PTSRecordEnum.compareRecords("XE02", xe02Rec);
		
		if(i > 0)
			validationResult.append("\n Payment Id: "+lPaymentId+"\n Errors found validating MasterCard Payments records: Record type XE02 is of more than specified length. "
					+ "Max length: "+i+", Actual length: "+xe02Rec.length()+ ". \n");
		
		// Validating D record
		i = PTSRecordEnum.compareRecords("D", dRec);
		
		if(i > 0)
			validationResult.append("\n Payment Id: "+lPaymentId+"\n Errors found validating MasterCard Payments records: Record type D is of more than specified length. "
					+ "Max length: "+i+", Actual length: "+dRec.length()+ ". \n");
		
		return recordSeq;
	}
	
	public int writeRefunds(EODRecordVO recordVO, int recordSeq, FileWriter out, StringBuffer validationResult) throws Exception{
		Map<String, String> payExtMap  = PTSUtil.getPaymentExtMapByPaymentId(recordVO.getPaymentId());
		SettlementRecordsBO recordBO = new SettlementRecordsBO(payExtMap);
		String sRec = null;
		
		String eRec = recordBO.buildERecord(recordVO, recordSeq);
		recordSeq = PTSUtil.getNextSequenceNumber(recordSeq);
		if("VI".equalsIgnoreCase(recordVO.getCreditCardType())){
			sRec = recordBO.buildSRecord(recordVO, recordSeq);
			recordSeq = PTSUtil.getNextSequenceNumber(recordSeq);	   
		}
		String dRec = recordBO.buildDRecord(recordVO, recordSeq);
		recordSeq = PTSUtil.getNextSequenceNumber(recordSeq);
		
		out.write(eRec+newLine);
		if(sRec != null){
			out.write(sRec+newLine);
		}
		out.write(dRec+newLine);
		
		/*
		* SP-72: BAMS - Additional validations for fields in settlement file.
		* Introducing checks on record lengths, with this release
		*/
		long lPaymentId = 0L;
		int i = 0;
		
		if(recordVO != null)
			lPaymentId = recordVO.getPaymentId();
		
		// Validating E record
		i = PTSRecordEnum.compareRecords("E", eRec);
		
		if(i > 0)
			validationResult.append("\n Payment Id: "+lPaymentId+"\n Errors found validating Refunds records: Record type E is of more than specified length. "
					+ "Max length: "+i+", Actual length: "+eRec.length()+ ". \n");
		
		if(sRec != null)
		{
			// Validating S record
			i = PTSRecordEnum.compareRecords("S", sRec);
			
			if(i > 0)
				validationResult.append("\n Payment Id: "+lPaymentId+"\n Errors found validating Refunds records: Record type S is of more than specified length. "
					+ "Max length: "+i+", Actual length: "+sRec.length()+ ". \n");
		}	
		
		// Validating D record
		i = PTSRecordEnum.compareRecords("D", dRec);
		
		if(i > 0)
			validationResult.append("\n Payment Id: "+lPaymentId+"\n Errors found validating Refunds records: Record type D is of more than specified length. "
					+ "Max length: "+i+", Actual length: "+dRec.length()+ ". \n");
				
		return recordSeq;
	}
	
	public int writeVisaPayments(EODRecordVO recordVO, int recordSeq, FileWriter out, StringBuffer validationResult) throws Exception{
		Map<String, String> payExtMap  = PTSUtil.getPaymentExtMapByPaymentId(recordVO.getPaymentId());
		SettlementRecordsBO recordBO = new SettlementRecordsBO(payExtMap);
		String eRec = recordBO.buildERecord(recordVO, recordSeq);
		recordSeq = PTSUtil.getNextSequenceNumber(recordSeq);
		String sRec = recordBO.buildSRecord(recordVO, recordSeq);
		recordSeq = PTSUtil.getNextSequenceNumber(recordSeq);
		String xn01Rec = recordBO.buildXN01Record(recordVO, recordSeq);
		recordSeq = PTSUtil.getNextSequenceNumber(recordSeq);
		String xd05Rec = recordBO.buildXD05Record(recordVO, recordSeq);
		recordSeq = PTSUtil.getNextSequenceNumber(recordSeq);
		String xd01Rec = recordBO.buildXD01Record(recordVO, recordSeq);
		recordSeq = PTSUtil.getNextSequenceNumber(recordSeq);
		String dRec = recordBO.buildDRecord(recordVO, recordSeq);
		recordSeq = PTSUtil.getNextSequenceNumber(recordSeq);
		
		out.write(eRec+newLine);
		out.write(sRec+newLine);
		out.write(xn01Rec+newLine);
		out.write(xd05Rec+newLine);
		out.write(xd01Rec+newLine);
		out.write(dRec+newLine);
		
		/*
		* SP-72: BAMS - Additional validations for fields in settlement file.
		* Introducing checks on record lengths, with this release
		*/
		long lPaymentId = 0L;
		int i = 0;
		
		if(recordVO != null)
			lPaymentId = recordVO.getPaymentId();
		
		// Validating E record
		i = PTSRecordEnum.compareRecords("E", eRec);
		
		if(i > 0)
			validationResult.append("\n Payment Id: "+lPaymentId+"\n Errors found validating Visa Payments records: Record type E is of more than specified length. "
					+ "Max length: "+i+", Actual length: "+eRec.length()+ ". \n");
		
		// Validating S record
		i = PTSRecordEnum.compareRecords("S", sRec);
		
		if(i > 0)
			validationResult.append("\n Payment Id: "+lPaymentId+"\n Errors found validating Visa Payments records: Record type S is of more than specified length. "
					+ "Max length: "+i+", Actual length: "+sRec.length()+ ". \n");
		
		// Validating XN01 record
		i = PTSRecordEnum.compareRecords("XN01", xn01Rec);
		
		if(i > 0)
			validationResult.append("\n Payment Id: "+lPaymentId+"\n Errors found validating Visa Payments records: Record type XN01 is of more than specified length. "
					+ "Max length: "+i+", Actual length: "+xn01Rec.length()+ ". \n");
		
		// Validating XD05 record
		i = PTSRecordEnum.compareRecords("XD05", xd05Rec);
		
		if(i > 0)
			validationResult.append("\n Payment Id: "+lPaymentId+"\n Errors found validating Visa Payments records: Record type XD05 is of more than specified length. "
					+ "Max length: "+i+", Actual length: "+xd05Rec.length()+ ". \n");
		
		// Validating XD01 record 
		// SP-90: BAMS EOD file validation logic is not validating the maximum length for XD01 RECORD.
		i = PTSRecordEnum.compareRecords("XD01", xd01Rec);
				
		if(i > 0)
			validationResult.append("\n Payment Id: "+lPaymentId+"\n Errors found validating Visa Payments records: Record type XD01 is of more than specified length. "
					+ "Max length: "+i+", Actual length: "+xd01Rec.length()+ ". \n");
		
		// Validating D record
		i = PTSRecordEnum.compareRecords("D", dRec);
		
		if(i > 0)
			validationResult.append("\n Payment Id: "+lPaymentId+"\n Errors found validating Visa Payments records: Record type D is of more than specified length. "
					+ "Max length: "+i+", Actual length: "+dRec.length()+ ". \n");
		
		return recordSeq;
	}
	
	public int writeVoiceAuthPayments(EODRecordVO recordVO, int recordSeq, FileWriter out, StringBuffer validationResult) throws Exception{
		Map<String, String> payExtMap  = PTSUtil.getPaymentExtMapByPaymentId(recordVO.getPaymentId());
		SettlementRecordsBO recordBO = new SettlementRecordsBO(payExtMap);
		String eRec = recordBO.buildERecord(recordVO, recordSeq);
		recordSeq = PTSUtil.getNextSequenceNumber(recordSeq);
		String dRec = recordBO.buildDRecord(recordVO, recordSeq);
		recordSeq = PTSUtil.getNextSequenceNumber(recordSeq);
		
		out.write(eRec+newLine);
		out.write(dRec+newLine);
		
		/*
		* SP-72: BAMS - Additional validations for fields in settlement file.
		* Introducing checks on record lengths, with this release
		*/
		long lPaymentId = 0L;
		int i = 0;
		
		if(recordVO != null)
			lPaymentId = recordVO.getPaymentId();
		
		// Validating E record
		i = PTSRecordEnum.compareRecords("E", eRec);
		
		if(i > 0)
			validationResult.append("\n Payment Id: "+lPaymentId+"\n Errors found validating Voice Auth Payments records: Record type E is of more than specified length. "
					+ "Max length: "+i+", Actual length: "+eRec.length()+ ". \n");
		
		// Validating D record
		i = PTSRecordEnum.compareRecords("D", dRec);
		
		if(i > 0)
			validationResult.append("\n Payment Id: "+lPaymentId+"\n Errors found validating Voice Auth Payments records: Record type D is of more than specified length. "
					+ "Max length: "+i+", Actual length: "+dRec.length()+ ". \n");
		
		return recordSeq;
	}
	
	public void writeTrailerRecord(double depositAmt, double creditAmt, int recordSeq, FileWriter out, String sCompanyId, StringBuffer validationResult) throws Exception{
		SettlementRecordsBO recordBO = new SettlementRecordsBO();
		String tRec = recordBO.buildTRecord(depositAmt, creditAmt, recordSeq);	
		out.write(tRec);
		
		/*
		* SP-72: BAMS - Additional validations for fields in settlement file.
		* Introducing checks on record lengths, with this release
		*/
		int i = 0;
		
		// Validating T record
		i = PTSRecordEnum.compareRecords("T", tRec);
		
		if(i > 0)
			validationResult.append("\n Company Id: "+sCompanyId+"\n Errors found validating Trailer records: Record type T is of more than specified length. "
					+ "Max length: "+i+", Actual length: "+tRec.length()+ ". \n");
	}

}
