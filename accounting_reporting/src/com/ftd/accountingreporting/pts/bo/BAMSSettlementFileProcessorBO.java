package com.ftd.accountingreporting.pts.bo;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.net.InetAddress;
import java.sql.Connection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.dao.EODDAO;
import com.ftd.accountingreporting.util.EODUtil;
import com.ftd.accountingreporting.util.FileArchiveUtil;
import com.ftd.accountingreporting.util.PTSUtil;
import com.ftd.accountingreporting.util.SftpUtil;
import com.ftd.accountingreporting.util.SystemMessager;
import com.ftd.accountingreporting.util.security.FileProcessorPgp;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;

public class BAMSSettlementFileProcessorBO {
	private Logger logger = 
        new Logger("com.ftd.accountingreporting.pts.bo.BAMSSettlementFileProcessorBO");
	
		
	public void processSettlementFile(String companyId) throws Exception {
		Connection conn = null;

		conn = EODUtil.createDatabaseConnection();
		EODDAO dao = new EODDAO(conn);
		StringBuffer sb = null;
		ConfigurationUtil cu = ConfigurationUtil.getInstance();

		String subject = "";
		String content = "";
		String bamsSettlementFileName = null;
		boolean sendSettlementFile = false;
		
		try{
			//Check if the clean.bams_settlements_totals table has a settlement file for today - date offset 
			//where status is null (not sent) for the company id sent in the payload. If so, get the file name
			CachedResultSet rs = dao.getSettlementFileToSend(companyId);
			
			if (rs != null){
				
				while (rs.next()){
					bamsSettlementFileName = rs.getString("FILE_NAME");
					sendSettlementFile = true;
				}
				
			}
				
			//archiveFiles();
			boolean fileTransferResult = false;
			InetAddress addr = InetAddress.getLocalHost();
			String hostname = addr.getHostName();
			while (sendSettlementFile) {
				try {
					fileTransferResult = getBAMSSettlementFileFromRemoteServer(bamsSettlementFileName, dao);
	
				} catch (Exception e) {
					fileTransferResult = false;
				}
				if (fileTransferResult == true) {
					logger.debug("File Transfer Successful : ");
					break;
				} else if (fileTransferResult == false) {
					sendSystemMessage(conn,new Exception("BAMS Settlement File Process Failed - Connection to server failed for "+ bamsSettlementFileName));
					break;				
				}	
				
			}
			boolean archiveSettlementFile = false;
			if(!sendSettlementFile){
				//Check to see if there is an empty file for the company id sent in the payload. If so, get the file name
				rs = dao.getSettlementFileToArchive(companyId);
				
				if (rs != null){
					
					while (rs.next()){
						bamsSettlementFileName = rs.getString("FILE_NAME");
						archiveSettlementFile = true;
					}
					
				}
					
				while (archiveSettlementFile) {
					try {
						fileTransferResult = archiveBAMSSettlementFile(bamsSettlementFileName, dao);
		
					} catch (Exception e) {
						fileTransferResult = false;
					}
					if (fileTransferResult == true) {
						logger.debug("Archiving File Successful : ");
						break;
					} else if (fileTransferResult == false) {
						sendSystemMessage(conn,new Exception("Archiving BAMS Settlement File Process Failed for "+ bamsSettlementFileName));
						break;				
					}	
					
				}
			}
		}finally {
		if (conn != null && !conn.isClosed()) {
			conn.close();
		}
	}
	}
	
	
    /**
     * This method utilizes the FTD Utility to retrieve the Settlement file and send to BAMS | First Data
     * @return File
     * @throws Exception
     */
    private boolean getBAMSSettlementFileFromRemoteServer(String bamsSettlementFileName, EODDAO dao) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering getBAMSSettlementFileFromRemoteServer: filename " + bamsSettlementFileName);
        }
        boolean retFlag = false;
        try{        
            /* retrieve server name to SFTP to */
        	ConfigurationUtil cu = ConfigurationUtil.getInstance();
        	String ptsInternalFtpServer = cu.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, "ptsInternalFtpServer");
            String ptsInternalFtpLocation = cu.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, "ptsInternalFtpLocation");
            String ptsInternalUsername = cu.getSecureProperty(ARConstants.SECURE_CONFIG_CONTEXT, "ptsInternalFtpUsername");
            String ptsInternalPassword = cu.getSecureProperty(ARConstants.SECURE_CONFIG_CONTEXT, "ptsInternalFtpPassword");
            String archiveLocation = cu.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, "BamsSettlementLocalArchiveLocation");
            
            String localFileDirectory = cu.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, "BamsAckLocalLocation"); 
                   
            String sftpServer = cu.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, "bamsSFTPServer");
            String remotesFilePath = cu.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, "bamsSFTPLocation");
            String sftpUsername = cu.getSecureProperty(ARConstants.SECURE_CONFIG_CONTEXT, "bamsSFTPUsername");
            String sftpPassword = cu.getSecureProperty(ARConstants.SECURE_CONFIG_CONTEXT, "bamsSFTPPassword");
            String sftpsecurityPrivateKeyPath = cu.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, "BamsSSHPrivateKeyPath");
            String sftpPrivateKeyPassphrase = cu.getSecureProperty(ARConstants.CONFIG_CONTEXT, "BamsSSHPrivateKeyPassphrase");
   
            SftpUtil ftpUtil = new SftpUtil();
            //log into Apollo back end server and retrieve settlement file 
            logger.info(String.format("bamsSettlementFileName=%s, ptsInternalFtpLocation=%s, ptsInternalFtpServer=%s, ptsInternalUsername=%s.", 
            		bamsSettlementFileName,
            		ptsInternalFtpLocation,
            		ptsInternalFtpServer,
            		ptsInternalUsername
            		));
            File settlementFile = ftpUtil.downloadFileByName(bamsSettlementFileName, ptsInternalFtpLocation, localFileDirectory, ptsInternalFtpServer, ptsInternalUsername, ptsInternalPassword);
            //log in to BAMS/First Data and send the settlement file
            logger.info(String.format("sftpServer=%s, sftpUsername=%s, sftpsecurityPrivateKeyPath=%s, remotesFilePath=%s.",
            		sftpServer,
            		sftpUsername,
            		sftpsecurityPrivateKeyPath,
            		remotesFilePath));
			ftpUtil.uploadFileviaSSH(settlementFile, remotesFilePath, sftpServer, sftpUsername, sftpsecurityPrivateKeyPath,sftpPrivateKeyPassphrase, sftpPassword);
            //Update bams_settlement_totals status to Sent
			dao.updateSettlementTotalsStatusByFilename(settlementFile.getName(), ARConstants.SettlementStatus.Sent.toString());
            //File was successfully sent to BAMS.  Log back in to back end server and archive file. 
			ftpUtil.archiveFile(ptsInternalFtpLocation, archiveLocation, ptsInternalFtpServer, ptsInternalUsername, ptsInternalPassword, settlementFile);
			//Update bams_settlement_totals archived flag to Y
			dao.updateArchiveStatus(settlementFile.getName());
			retFlag = true;
            
        }catch(Exception e){
        	logger.error("Error occured while fetching files from sftp server", e);
        	retFlag = false;
        }
        finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting getBAMSSettlementFileFromRemoteServer");
            } 
        }
        return retFlag;
        
    }
    
        
      
      /**
       * This method sends a message to the System Messenger.
       *
       * @param String message
       * @returns String message id
       */
      public void sendSystemMessage(Connection conn, Throwable t)
      {
      	String logMessage = t.getMessage();
      	try
      	{
          //build system vo
          // SystemMessage closes connection passed in. Therefore, create a new connection.
      	  
          logger.debug(logMessage);
          SystemMessengerVO sysMessage = new SystemMessengerVO();
          sysMessage.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
          sysMessage.setSource(ARConstants.EOD_BILLING_PROCESS);
          sysMessage.setType(ARConstants.EOD_BILLING_PROCESS_ERROR_TYPE);
          sysMessage.setMessage(logMessage);
          SystemMessenger.getInstance().send(sysMessage, conn, false);
      	}
      	catch (Exception e)
      	{
      		logger.error("Sending system message failed. Message=" + logMessage);
      		logger.error(e);
      	}
      }
      
      /**
       * This method utilizes the FTD Utility to archive empty Settlement files
       * @return File
       * @throws Exception
       */
      private boolean archiveBAMSSettlementFile(String bamsSettlementFileName, EODDAO dao) throws Exception
      {
          if(logger.isDebugEnabled()) {
              logger.debug("Entering archiveBAMSSettlementFile: filename " + bamsSettlementFileName);
          }
          boolean retFlag = false;
          try{        
              /* retrieve server name to SFTP to */
          	  ConfigurationUtil cu = ConfigurationUtil.getInstance();
          	String ptsInternalFtpServer = cu.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, "ptsInternalFtpServer");
            String ptsInternalFtpLocation = cu.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, "ptsInternalFtpLocation");
            String ptsInternalUsername = cu.getSecureProperty(ARConstants.SECURE_CONFIG_CONTEXT, "ptsInternalFtpUsername");
            String ptsInternalPassword = cu.getSecureProperty(ARConstants.SECURE_CONFIG_CONTEXT, "ptsInternalFtpPassword");
              String archiveLocation = cu.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, "BamsSettlementLocalArchiveLocation");
              String localFileDirectory = cu.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, "BamsAckLocalLocation"); 
                  
              SftpUtil ftpUtil = new SftpUtil();
              //log into Apollo back end server and retrieve settlement file 
              File settlementFile = ftpUtil.downloadFileByName(bamsSettlementFileName, ptsInternalFtpLocation, localFileDirectory, ptsInternalFtpServer, ptsInternalUsername, ptsInternalPassword);
              //File was successfully sent to BAMS.  Log back in to back end server and archive file. 
              ftpUtil.archiveFile(ptsInternalFtpLocation, archiveLocation, ptsInternalFtpServer, ptsInternalUsername, ptsInternalPassword, settlementFile);
  			  //Update bams_settlement_totals archived flag to Y
  			  dao.updateArchiveStatus(settlementFile.getName());
  			  retFlag = true;
              
          }catch(Exception e){
          	logger.error("Error occured while archiving empty settlement file on back end server", e);
          	retFlag = false;
          }
          finally {
              if(logger.isDebugEnabled()){
                  logger.debug("Exiting archiveBAMSSettlementFile");
              } 
          }
          return retFlag;
          
      }

}
