package com.ftd.accountingreporting.pts.vo;

import java.math.BigDecimal;

import com.ftd.accountingreporting.vo.EODRecordVO;

public class SettlementRecordVO extends EODRecordVO {
	private String voiceAuthFlag;
	
	

	public String getVoiceAuthFlag() {
		return voiceAuthFlag;
	}

	public void setVoiceAuthFlag(String voiceAuthFlag) {
		this.voiceAuthFlag = voiceAuthFlag;
	}
	
	

}
