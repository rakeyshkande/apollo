/**
 * 
 */
package com.ftd.accountingreporting.pts.validator;

import com.ftd.accountingreporting.util.PTSUtil;

/**
 * @author kvasant
 *
 */
public class PTSValidations {
	
	
	private final static String NUMERIC_REG_EX = "^[0-9]*$";
	private final static String ALPHA_NUMERIC_REG_EX = "^[0-9a-zA-Z]*$";	
	private final static String COMPANY_REG_EX = "^[a-z.A-Z]*$";
	private final static String PHONE_NUM_REG_EX = "^[0-9-]*$";
	private final static String DATE_REG_EX = "^[0-9/]*$";
	private final static String PRICE_REG_EX = "^[0-9.]*$";
	private final static String DATE_TIME_PATTERN = "yyyyMMddhhmmss";
	private final static String MONTH_DATE_PATTERN = "MMdd";
	
	
	public static String doLengthValidation(String fieldName, String fieldValue, int length) {
		if(fieldValue.length() > length) {
			return "Length of " + fieldName + " field exceeds max length. " + "Value:" + fieldValue + ", Max length:" + length + ", Actual length:" + fieldValue.length() + "\n";
		}
		return null;
	}
	
	public static String doNumericTypeValidation(String fieldName, String fieldValue) {
		if(!fieldValue.matches(NUMERIC_REG_EX)) {
			return "Non numeric characters found in the field:" + fieldName + ", Value: " + fieldValue + "\n";  
		}
		
		return null;
	}
	
	public static String doAlphaNumericTypeValidation(String fieldName, String fieldValue) {
		if(!fieldValue.matches(ALPHA_NUMERIC_REG_EX)) {
			return "Non alpha-numeric characters found in the field. Fieldname:" + fieldName + ", Value: " + fieldValue + "\n";  
		}
		
		return null;
	}	
	
	public static String doCompanyTypeValidation(String fieldName, String fieldValue) {
		if(!fieldValue.matches(COMPANY_REG_EX)) {
			return "Invalid company type found in the field. Fieldname:" + fieldName + ", Value: " + fieldValue + "\n";  
		}
		
		return null;
	}	
	
	public static String doPhoneNumValidation(String fieldName, String fieldValue) {
		if(!fieldValue.matches(PHONE_NUM_REG_EX)) {
			return "Invalid phone number found in the field. Fieldname:" + fieldName + ", Value: " + fieldValue + "\n";  
		}
		
		return null;
	}	
	
	public static String doDateValidation(String fieldName, String fieldValue) {
		if(!fieldValue.matches(DATE_REG_EX)) {
			return "Invalid date format found in the field. Fieldname:" + fieldName + ", Value: " + fieldValue + "\n";  
		}
		
		return null;
	}	
	
	public static String doPriceValidation(String fieldName, String fieldValue) {
		if(!fieldValue.matches(PRICE_REG_EX)) {
			return "Invalid price format found in the field. Fieldname:" + fieldName + ", Value: " + fieldValue + "\n";  
		}
		
		return null;
	}
	
	public static String doMinLengthValidation(String fieldName, String fieldValue, int length) {
		if(fieldValue.length() < length) {
			return "Length of " + fieldName + " field less than minimum length. " + "Value:" + fieldValue + ", Min length:" + length + ", Actual length:" + fieldValue.length() + "\n";
		}
		return null;
	}
	
	public static String doDateTimeValidation(String fieldName, String fieldValue) {
		
		try {
			PTSUtil.getDateStringByFormat(fieldValue, DATE_TIME_PATTERN, MONTH_DATE_PATTERN);
		} catch (Exception e) {
			return "Invalid date time format found in the field. Fieldname: " + fieldName +  ", Value: " +
					fieldValue + ", Excepted format: " + DATE_TIME_PATTERN + "\n";
		}
		
		return null;
	}

}

enum ValidationType {
	LEN(1, "LENGTH_VALIDATION"),
	NUM(2, "NUMERIC_VALIDATION"),
	ALPHA_NUM(3, "ALPHA_NUMERIC_VALIDATION"),
	COMPANY(4, "COMPANY_VALIDATION"),
	PHONE_NUM(5, "PHONE_NUMBER_VALIDATION"),
	DATE(6, "DATE_VALIDATION"),
	PRICE(7, "PRICE_VALIDATION"),
	MIN_LEN(8,"MIN_LENGTH_VALIDATION"),
	DATE_TIME(9,"TRANSIMISSION_DATE_TIME_VALIDATION");
		
	private int id;
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	private String name;
	
	ValidationType(int id, String value) {
		this.id = id;
		this.name = value;
		
	}
}
