package com.ftd.accountingreporting.pts.validator;

import java.util.HashMap;
import java.util.Map;

import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.exception.PTSFieldValidationException;
import com.ftd.accountingreporting.vo.EODRecordVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * This class validates the different fields that are written to PTS settlement file
 * 
 * @author kvasant
 *
 */
public class PTSFieldValidator {

	
	private static Logger logger  = new Logger(PTSFieldValidator.class.getName());
	
	public static final PTSFieldValidator ptsFieldValidator = new PTSFieldValidator();
	
	private PTSFieldValidator() {
		
	}
	
	/**
	 * Returns singleton instance
	 * 
	 * @return PTSFieldValidator
	 */
	public static PTSFieldValidator getInstance() {
		return ptsFieldValidator;
	}
	
	/**
	 * Validates global parameters that are written to PTS settlement file
	 * 
	 * @param companyId
	 * @throws PTSFieldValidationException
	 */
	public void validateGlobalParams(String companyId) throws PTSFieldValidationException {
		
		if(companyId == null || companyId.isEmpty()) {
			logger.error("Company Id found null/empty. Not able to validate PTS settlement global params");			
			throw new PTSFieldValidationException("Company Id found null/empty. Not able to validate PTS settlement global params");
		}
		
		Map<String, String> fieldValidationMap = new HashMap<String, String>();
		
		ConfigurationUtil cu;
		try {
			cu = ConfigurationUtil.getInstance();		
			String merchantAcc = cu.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, ARConstants.MERCHANT_ACCOUNT+companyId);
			fieldValidationMap.put(ARConstants.MERCHANT_ACCOUNT, merchantAcc);
			
			String merchantSecurityCode = cu.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, "MERCHANT_SECURITY_CODE");
			fieldValidationMap.put("MERCHANT_SECURITY_CODE", merchantSecurityCode);
			
			String merchantName = cu.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, ARConstants.MERCHANT_NAME+companyId);
			fieldValidationMap.put(ARConstants.MERCHANT_NAME, merchantName);
			
			String merchantCity = cu.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, ARConstants.MERCHANT_CITY+companyId);
			fieldValidationMap.put(ARConstants.MERCHANT_CITY,merchantCity);
			
			String merchantCategoryCode = cu.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, "MERCHANT_CATEGORY_CODE");
			fieldValidationMap.put("MERCHANT_CATEGORY_CODE", merchantCategoryCode);
			
		} catch (Exception e) {
			logger.error("Exception occurred reading PTS global parameters." + e, e);
			throw new PTSFieldValidationException("Exception occurred reading PTS global parameters." + e, e);
		}	
		
		String validationResult = validatePTSField(fieldValidationMap);
		
		if(!validationResult.isEmpty()) {
			throw new PTSFieldValidationException("Errors found validating PTS global parameters: \n" + validationResult);
		}
	}
	
	/**
	 * Validates EOD Record VO elements that are written to PTS settlement file
	 * 
	 * @param eodRecordVo
	 * @throws PTSFieldValidationException
	 */
	public void validateEodRecordVo(EODRecordVO eodRecordVo) throws PTSFieldValidationException {
		
		if(eodRecordVo == null) {
			logger.error("EODRecordVO found null/empty. Unable to validate PTS settlement EODRecordVo attributes");
			throw new PTSFieldValidationException("EODRecordVO found null/empty. Unable to validate PTS settlement EODRecordVo attributes");			
		}
		
		Map<String, String> fieldValidationMap = new HashMap<String, String>();
		
		if(eodRecordVo.getAVSResultCode() != null) {
			fieldValidationMap.put("AvsResultCode", eodRecordVo.getAVSResultCode());
		}
		
		if(eodRecordVo.getCreditCardNumber() != null) {
			fieldValidationMap.put("CreditCardNumber", eodRecordVo.getCreditCardNumber());
		}
		
		if(eodRecordVo.getApprovalCode() != null) {
			fieldValidationMap.put("ApprovalCode", eodRecordVo.getApprovalCode());
		}
		
		if(eodRecordVo.getCcExpDate() != null) {
			fieldValidationMap.put("CcExpDate", eodRecordVo.getCcExpDate());
		}
		
		if(eodRecordVo.getCscResponseCode() != null) {
			fieldValidationMap.put("CscResponseCode", eodRecordVo.getCscResponseCode());
		}
		
		if(eodRecordVo.getOrderNumber() != null) {
			fieldValidationMap.put("OrderNumber", eodRecordVo.getOrderNumber());
		}
				
		fieldValidationMap.put("PaymentId", String.valueOf(eodRecordVo.getPaymentId()));
		
		/*
		 * SP-72:BAMS - Additional validations for fields in settlement file.
		 * Adding OrderAmount fetched from the table clean.billing_detail to the validations map  
		 */
		fieldValidationMap.put("OrderAmount", String.valueOf(eodRecordVo.getOrderAmount()));
		
		String validationResult = validatePTSField(fieldValidationMap);
		
		if(!validationResult.isEmpty()) {
			throw new PTSFieldValidationException("Errors found validating EOD Record Vo attributes: \n" + validationResult);
		}
		
	}
	
	/**
	 * Validates payment extension map attributes that are written to PTS settlement file
	 * 
	 * @param paymentExtMap
	 * @throws PTSFieldValidationException
	 */
	public void validatePaymentExtnMap(Map<String, String> paymentExtMap) throws PTSFieldValidationException {
				
		if(paymentExtMap == null || paymentExtMap.isEmpty()) {
			logger.error("PaymentExtMap found null/empty. Unable to validate PTS settlement payment extension attributes");
			throw new PTSFieldValidationException("PaymentExtMap found null/empty. Unable to validate PTS settlement payment extension attributes");			
		}
				
		String validationResult = validatePTSField(paymentExtMap);					
		
		if(!validationResult.isEmpty()) {
			throw new PTSFieldValidationException("Errors found validating Payment Extension attributes: \n" + validationResult);
		}
	}

	/**
	 * @param validationFieldMap
	 * @param errorMessage
	 * @return 
	 */
	private String validatePTSField(Map<String, String> validationFieldMap) {
		
		StringBuilder errorMessage = new StringBuilder();
				
		for(String attributeName : validationFieldMap.keySet()) {
			
			if(attributeName == null) {
				logger.error("Attribte name found null");
				continue;
			}
			attributeName = attributeName.trim();
			
			String attributeValue = validationFieldMap.get(attributeName);			
			if(attributeValue == null) {
				logger.error("Attribute value found null. Attribute name: " + attributeName);
				continue;
			}
			attributeValue = attributeValue.trim();
			
			PTSFieldEnum field =  PTSFieldEnum.fromName(attributeName);
			
			if(field == null) {
				logger.warn("No validation rules configured for the Field: "+ attributeName); 
				continue;
			} 			
						
			ValidationType[] validations = field.getValidations();
			
			for(ValidationType validation : validations) {
				String result = null;
				
				switch(validation.getId()) {
				
				case 1:
					result = PTSValidations.doLengthValidation(attributeName, attributeValue, field.getLength());
					break;
				case 2:
					result = PTSValidations.doNumericTypeValidation(attributeName, attributeValue);
					break;
				case 3:
					result = PTSValidations.doAlphaNumericTypeValidation(attributeName, attributeValue);
					break;
				case 4:
					result = PTSValidations.doCompanyTypeValidation(attributeName, attributeValue);
					break;
				case 5:
					result = PTSValidations.doPhoneNumValidation(attributeName, attributeValue);
					break;
				case 6:
					result = PTSValidations.doDateValidation(attributeName, attributeValue);
					break;
				case 7:
					result = PTSValidations.doPriceValidation(attributeName, attributeValue);
					break;
				case 8:
					result = PTSValidations.doMinLengthValidation(attributeName, attributeValue, field.getLength());
					break;
				case 9:
					result = PTSValidations.doDateTimeValidation(attributeName, attributeValue);
					break;
				}				
				if(result != null) {
					errorMessage.append(result);
				}
			}			
		}
		
		return errorMessage.toString();
	}
		
	
	
}
