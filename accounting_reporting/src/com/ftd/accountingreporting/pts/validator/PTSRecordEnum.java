package com.ftd.accountingreporting.pts.validator;


/**
 * SP-72
 * This enum contains all the types of records that are written to PTS settlement file from different sources.
 * Each field is described with its max length
 * 
 * @author 	sunil
 * @Date	7/27/2015
 *
 */
public enum PTSRecordEnum {
	
	Merchant("M",68),
	Descriptor("N",80),
	Totals("T",67),
	EnrichedDeposit("E",80),
	SpecialCondition("S",80),
	ExtendedDescriptor("XN01",80),
	MarketSpecificData("XD05",80),
	SupplementalVisaDetail("XD01",80),
	SupplementalMasterCardDetail("XD02",80),
	MasterCardDE22("XE02",80),
	SupplementalDiscAndDiners01("XV01",80),
	SupplementalDiscAndDiners02("XV02",80),
	CreditCardDetail("D",80);
	
	//Private properties of the enum constant.
	private String sRecType;
	private int iRecLength;
	
	//Getters and setters for the properties of enum constant.
	public String getsRecType() 
	{
		return sRecType;
	}

	public void setsRecType(String sRecType) 
	{
		this.sRecType = sRecType;
	}

	public int getiRecLength() 
	{
		return iRecLength;
	}

	public void setiRecLength(int iRecLength) 
	{
		this.iRecLength = iRecLength;
	}
	
	// Constructor of the enum constant.
	PTSRecordEnum(String sRecType, int iRecLength) 
	{
		this.sRecType = sRecType;
		this.iRecLength = iRecLength;
	}
	
	/*
	 *  This method retrieves respective enum instance based on recordType string passed to it.
	 */
	static PTSRecordEnum fromName(String sRecType) 
	{
		for(PTSRecordEnum enRec : PTSRecordEnum.values()) 
		{
			if(enRec.getsRecType().equals(sRecType))
				return enRec;
		}
		return null;
	}
	
	/*
	 * This method:
	 * 		accepts record type and reference to a specific PTS file record,
	 * 		validates its length as per the specifications,
	 * 		returns empty string if the record lies within specified limits
	 * 		if not, returns the specified max length for the record.
	 */
	public static int compareRecords(String sRecType, String sPTSRec)
	{
		int iResult = 0;
		PTSRecordEnum enRec =  PTSRecordEnum.fromName(sRecType);
		
		if(sPTSRec.length() > enRec.getiRecLength())
			return enRec.getiRecLength();
		
		return iResult;
	}
}

