package com.ftd.accountingreporting.pts.validator;

import com.ftd.accountingreporting.constant.ARConstants;

/**
 * This enum contains all the fields that are written to PTS settlement file from different sources.
 * Each field is descibed with its max length and list of validation types
 * 
 * @author kvasant
 *
 */
public enum PTSFieldEnum {
	
	// Global Param Fields
	MerchantAcc(ARConstants.MERCHANT_ACCOUNT,12, new ValidationType[] {ValidationType.NUM, ValidationType.LEN}),
	MerchantSecurityCode("MERCHANT_SECURITY_CODE",4, new ValidationType[] {ValidationType.NUM, ValidationType.LEN}),
	MerchantName(ARConstants.MERCHANT_NAME,19, new ValidationType[] {ValidationType.COMPANY, ValidationType.LEN}),
	MerchantCity(ARConstants.MERCHANT_CITY,13, new ValidationType[] {ValidationType.PHONE_NUM, ValidationType.LEN}),
	MerchantCategoryCode("MERCHANT_CATEGORY_CODE",4, new ValidationType[] {ValidationType.ALPHA_NUM, ValidationType.LEN}),
	
	// EODRecordVO Fields
	AVSResultCode("AvsResultCode", 1, new ValidationType[] {ValidationType.ALPHA_NUM, ValidationType.LEN}),
	CreditCardNumber("CreditCardNumber", 16, new ValidationType[] {ValidationType.NUM, ValidationType.LEN}),
	ApprovalCode("ApprovalCode", 6, new ValidationType[] {ValidationType.ALPHA_NUM, ValidationType.LEN}),
	CcExpDate("CcExpDate", 7, new ValidationType[] {ValidationType.DATE, ValidationType.LEN}),
	// Support can't change the order number even validation fails. Assuming this case never occur
	//OrderNumber("OrderNumber", 19, new ValidationType[] {ValidationType.AN, ValidationType.L}),
	PaymentId("PaymentId", 15, new ValidationType[] {ValidationType.ALPHA_NUM, ValidationType.LEN}),
	CscResponseCode("CscResponseCode", 1, new ValidationType[] {ValidationType.ALPHA_NUM, ValidationType.LEN}),
	/*
	* SP-72: BAMS - Additional validations for fields in settlement file.
	* Introducing length check on order amount fetched from clean.billing_detail table.
	*/
	OrderAmount("OrderAmount",12,new ValidationType[]{ValidationType.LEN}),

	// Payment extension fields
	RefNum("RefNum", 8, new ValidationType[]{ValidationType.LEN, ValidationType.NUM}),
	TransID("TransID", 19, new ValidationType[]{ValidationType.LEN, ValidationType.ALPHA_NUM}),	
	MrktSpecificDataInd("MrktSpecificDataInd",1,new ValidationType[]{ValidationType.LEN, ValidationType.ALPHA_NUM}),
	CardLevelResult("CardLevelResult",2,new ValidationType[]{ValidationType.LEN, ValidationType.ALPHA_NUM}),
	POSCondCode("POSCondCode",2,new ValidationType[]{ValidationType.LEN, ValidationType.ALPHA_NUM}),
	TxnCrncy("TxnCrncy",3,new ValidationType[]{ValidationType.LEN, ValidationType.ALPHA_NUM}),
	// No validation required for Respcode as it is hard coded while writing to file
	//RespCode("RespCode",2,new ValidationType[]{ValidationType.L, ValidationType.AN}),
	ACI("ACI",1,new ValidationType[]{ValidationType.LEN, ValidationType.ALPHA_NUM}),
	BanknetData ("BanknetData",13,new ValidationType[]{ValidationType.LEN, ValidationType.ALPHA_NUM}),	
	TxnAmt("TxnAmt",12,new ValidationType[]{ValidationType.LEN, ValidationType.PRICE}), 
	TranEditErrCode("TranEditErrCode",1,new ValidationType[]{ValidationType.LEN, ValidationType.ALPHA_NUM}),
	CCVErrorCode("CCVErrorCode",1,new ValidationType[]{ValidationType.LEN, ValidationType.ALPHA_NUM}),
	POSEntryModeChg("POSEntryModeChg",1,new ValidationType[]{ValidationType.LEN, ValidationType.ALPHA_NUM}),	
	TrnmsnDateTime("TrnmsnDateTime",4,new ValidationType[]{ValidationType.DATE_TIME}),
	MCPOSData("MCPOSData",12,new ValidationType[]{ValidationType.LEN, ValidationType.ALPHA_NUM}),
	DiscProcCode("DiscProcCode",6,new ValidationType[]{ValidationType.LEN, ValidationType.ALPHA_NUM}),
	STAN("STAN",6,new ValidationType[]{ValidationType.LEN, ValidationType.NUM}),
	DiscPOSEntry("DiscPOSEntry",3,new ValidationType[]{ValidationType.MIN_LEN, ValidationType.ALPHA_NUM}),
	DiscTransQualifier("DiscTransQualifier",2,new ValidationType[]{ValidationType.LEN, ValidationType.ALPHA_NUM}),
	DiscPOSData("DiscPOSData",13,new ValidationType[]{ValidationType.LEN, ValidationType.ALPHA_NUM}),
	DiscNRID("DiscNRID",15,new ValidationType[]{ValidationType.LEN, ValidationType.NUM});
	
	
	private String name;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public ValidationType[] getValidations() {
		return validations;
	}

	public void setValidations(ValidationType[] validations) {
		this.validations = validations;
	}

	private int length;
	private ValidationType[] validations;
	
	PTSFieldEnum(String name, int length, ValidationType[] validations) {
		this.name = name;
		this.length = length;
		this.validations = validations;
	}
	
	static PTSFieldEnum fromName(String name) {
		for(PTSFieldEnum globalParam : PTSFieldEnum.values()) {
			if(globalParam.getName().equals(name)) {
				return globalParam;
			}
		}
		return null;
	}
}

