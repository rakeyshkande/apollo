package com.ftd.accountingreporting.altpay.handler;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.ftd.accountingreporting.altpay.dao.AltPayDAO;
import com.ftd.accountingreporting.altpay.vo.AltPayProfileVOBase;
import com.ftd.accountingreporting.altpay.vo.AltPayReportDataVO;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.constant.AltPayConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.j2ee.NotificationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.NotificationVO;


/**
 * This class is responsible for reporting Alt Pay EOD process
 * related errors. The process is kicked off at the same time
 * of Alt Pay EOD but with ~6 hours of delay. If there are failed
 * PayPal or GiftCard calls (or PayPal payments with no auth), the process
 * will summarize the errors and send a page.
 */
public class AltPayReportHandler extends AltPayHandlerBase implements IAltPayEODHandler {
    private static Logger logger  = new Logger("com.ftd.accountingreporting.altpay.handler.AltPayReportHandler");
    protected Connection con;
    protected AltPayProfileVOBase apProfileVO;
    protected AltPayDAO altPayDAO;

    public void process(Connection _con) throws Throwable {
        logger.info("Entering process of AltPayReportHandler...");
        con = _con;
        // generate report of alt pay EOD errors
        processAltPayEODErrors(con);

        /** The rest of this functionality has been replaced by the above. Code left in place for now.
         * 
        apProfileVO.createConfigsMap();
        // This flag is 'Y' if we are interested in errors of all PayPal/GiftCard orders.
        // This flag is 'N' if we are only interested in errors from the latest run.
        String reportFullErrorFlag = (String)
                (apProfileVO.getConfigsMap()).get(AltPayConstants.AP_REPORT_FULL_ERROR_FLAG);

        AltPayReportDataVO dvoPP = altPayDAO.doGetReportDataPayPal(con, reportFullErrorFlag);
        logger.info("PayPal report error flag is: " + dvoPP.isReportErrorFlag());

        AltPayReportDataVO dvoGD = altPayDAO.doGetReportDataGiftCard(con, reportFullErrorFlag);
        logger.info("GiftCard report error flag is: " + dvoGD.isReportErrorFlag());

        if(dvoPP.isReportErrorFlag() || dvoGD.isReportErrorFlag()) {
            String txt = "Please investigate: \n" + dvoPP.getReportText() + dvoGD.getReportText();
            logger.info(txt);
            // Send pageable system message.
            super.sendSystemMessage(con, txt, true);
        }
         */


        logger.info("Exiting process of AltPayReportHandler...");        
    }

    /**
     * Generate and send (email) a report of alt pay EOD errors, limited to the payment types reported 
     * on by the stored proc. Visibility is public for ease of testing.
     * @param conn
     * @throws Exception
     */
    public void processAltPayEODErrors(Connection conn) throws Exception
    {
        // get report cached result set - we process it here for custom formatting, etc.
        CachedResultSet outrs = altPayDAO.doGetReportDataAltPayEODErrors(conn);
        String report = buildReport(outrs);

        // determine if there are new errors since yesterday - changes the email
        outrs.reset();
        outrs.next();
        boolean newErrors = false;
        Date createdOn =  truncateDate(outrs.getDate("created_on"), 0);
        Date today = truncateDate(new Date(), 1);
        if (createdOn.after(today))
        {
            newErrors = true;
        }
 
        // build the file that will be sent as an attachment
        File reportFile = createAltPayEODErrorFile(report);  

        NotificationVO notificationVO = createAltPayEODErrorNotificationVO(conn, newErrors, reportFile);
        NotificationUtil.getInstance().notifyMultipart(notificationVO, true); // true = delete attached file        
    }

    /**
     * build the notificationVO object to be used to send the error report email
     * @param conn
     * @param newErrors
     * @return
     * @throws Exception
     */
    private NotificationVO createAltPayEODErrorNotificationVO(Connection conn, boolean newErrors, File reportFile) 
    throws Exception
    {
        // call dao to get email list
        String toAddress = altPayDAO.doGetAltPayEODErrorEmailList(conn);
        
        String smtpHostName = ConfigurationUtil.getInstance().getFrpGlobalParm(
                ARConstants.MESSAGE_GENERATOR_CONFIG, ARConstants.SMTP_HOST_NAME);

        NotificationVO notificationVO = new NotificationVO();
        notificationVO.setMessageFromAddress("no-reply@ftdi.com");
        notificationVO.setMessageTOAddress(toAddress);

        notificationVO.setMessageSubject("Alt Pay EOD Errors");

        if (newErrors)
        {
            notificationVO.setMessageContent("New errors found. See attached error report.\n");
        }
        else
        {
            notificationVO.setMessageContent("No new errors. See attached error report.\n");           
        }
        
        notificationVO.setMessageMimeType("text/html; charset=us-ascii");
        notificationVO.setSMTPHost(smtpHostName);
        
        notificationVO.setFileAttachment(reportFile);

        return notificationVO;
    }

    /**
     * build the alt pay EOD error report from the cached result set returned from the query
     * @param outrs
     * @return
     */
    private String buildReport(CachedResultSet outrs)
    {
        StringBuffer text = new StringBuffer();
        String delimiter = ",";

        // header row
        text.append(
                "Payment Create Date" + delimiter + 
                "Payment id" + delimiter + 
                "Payment code" + delimiter + 
                "Payment type" + delimiter + 
                "Master Order#" + delimiter + 
                "Auth Date" + delimiter + 
                "Settlement Amt" + delimiter + 
                "Additional Info" + delimiter + 
                "Status" + delimiter + 
                "Error Text" + "\n");

        
        // report body
        while(outrs.next()) {
            addReportField(text, outrs, delimiter, "created_on");
            addReportField(text, outrs, delimiter, "payment_id");
            addReportField(text, outrs, delimiter, "payment_code");
            addReportField(text, outrs, delimiter, "payment_type");
            addReportField(text, outrs, delimiter, "master_order_number");
            addReportField(text, outrs, delimiter, "auth_date");
            addReportField(text, outrs, delimiter, "settlement_amt");
            addReportField(text, outrs, delimiter, "additional_info");
            addReportField(text, outrs, delimiter, "status");
            addReportField(text, outrs, delimiter, "error_text");
            text.append("\n");
        }
        return text.toString();        
    }

    /**
     * get the field from the cached result set and add to the report with the specified delimiter
     * @param report
     * @param rs
     * @param delimiter
     * @param fieldName
     */
    private void addReportField(StringBuffer report, CachedResultSet rs, String delimiter, String fieldName)
    {
        String quote = "\"";
        String field = rs.getString(fieldName);
        report.append(quote + field + quote + delimiter);
    }

    /**
     * set a date to midnight daysBack days ago, 0 for midnight of the date passed in, 1 for 1 day prior, etc.
     * @param date
     * @param daysBack
     * @return
     */
    private Date truncateDate(Date date, int daysBack)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.add(Calendar.DAY_OF_YEAR, -daysBack);

        return cal.getTime();
    }


    /**
     * create the error file attachment for alt pay EOD errors
     * @param report
     * @return
     * @throws IOException
     */
    private File createAltPayEODErrorFile(String report) throws IOException
    {
        // use tempfile to get temp directory - will rename later to more desirable name.
        File reportFile = File.createTempFile("AltPayEODErrors", ".csv");
        reportFile.deleteOnExit();
        FileWriter fw = new FileWriter(reportFile);
        fw.write(report);        
        fw.close();

        // rename file to have date/time
        String dt;
        Date now = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy_dd_MM_HH_mm_ss");
        dt = sdf.format(now);

        File newfile = new File(reportFile.getParentFile() + "/AltPayEODErrors_" + dt + ".csv");
        newfile.deleteOnExit();
        reportFile.renameTo(newfile);
        return newfile;
    }
    

    public void setAltPayDAO(AltPayDAO altPayDAO) {
        this.altPayDAO = altPayDAO;
    }

    public AltPayDAO getAltPayDAO() {
        return altPayDAO;
    }

    public void setApProfileVO(AltPayProfileVOBase apProfileVO) {
        this.apProfileVO = apProfileVO;
    }

    public AltPayProfileVOBase getApProfileVO() {
        return apProfileVO;
    }
}
