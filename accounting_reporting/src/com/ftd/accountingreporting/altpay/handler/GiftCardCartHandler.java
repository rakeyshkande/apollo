package com.ftd.accountingreporting.altpay.handler;

import com.ftd.accountingreporting.altpay.vo.AltPayBillingDetailVOBase;
import com.ftd.accountingreporting.altpay.vo.AltPayProfileVOBase;
import com.ftd.accountingreporting.altpay.vo.GiftCardBillingDetailVO;
import com.ftd.accountingreporting.altpay.vo.GiftCardProfileVO;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.constant.AltPayConstants;
import com.ftd.accountingreporting.dao.EODDAO;
import com.ftd.accountingreporting.exception.EntityLockedException;
import com.ftd.accountingreporting.util.AccountingUtil;
import com.ftd.accountingreporting.util.EODUtil;
import com.ftd.accountingreporting.util.LockUtil;
import com.ftd.accountingreporting.util.GiftCardPaymentSvc;
import com.ftd.accountingreporting.vo.EODMessageVO;
import com.ftd.accountingreporting.vo.EODPaymentVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.IdGeneratorUtil;
import com.ftd.osp.utilities.id.vo.IdRequestVO;

import java.math.BigDecimal;
import java.math.RoundingMode;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang.StringUtils;


/**
 * This class is responsible for processing the Gift Card payments and refunds
 * the shopping cart. If the payment and refund fullying net out,
 * it sends no transaction but updates accounting status to Billed for the order.
 * If there are both payements and partial refunds, it net out the refunds
 * and send the remaining payment balance to settle. If there are more refund amount
 * that payment amount, it will settle the payment and leave the refunds.
 * The process takes into account of billed payment and refund of this payment type.
 */
public class GiftCardCartHandler extends AltPayCartHandlerBase {

    Logger logger = new Logger("com.ftd.accountingreporting.altpay.handler.GiftCardCartHandler");
    GiftCardProfileVO gdProfileVO;
    GiftCardPaymentSvc gdPaymentSvc;
    EODDAO eodDAO;

	public GiftCardCartHandler() {}
    

    
    /**
     * This is the main method. It
     * 1. Attempts to lock the shopping cart. If a lock cannot be obtained, it sends a nopage notification.
     * 2. Loads payment information required to process.
     * 3. Validates the payments and refundsl
     * 4. Process the payments and refunds.
     * @param conn
     * @param mvo
     * @throws Throwable
     */     
    public void process(Connection conn, EODMessageVO mvo) throws Throwable {
        logger.info("Entering process of GiftCardCartHandler...");
        gdProfileVO.createConfigsMap();

        super.process(conn, mvo); 
        eodDAO.setConnection(conn);
        
        gdPaymentSvc.setClientId((String)gdProfileVO.getConfigsMap().get(AltPayConstants.GD_USERNAME));
        gdPaymentSvc.setClientPassword((String)gdProfileVO.getConfigsMap().get(AltPayConstants.GD_PASSWORD));
        gdPaymentSvc.setRetryCount(Integer.parseInt((String)gdProfileVO.getConfigsMap().get(AltPayConstants.GD_RETRY_COUNT)));
        gdPaymentSvc.setServiceUrl((String)gdProfileVO.getConfigsMap().get(AltPayConstants.GD_SERVICE_URL));        
        long enqueueDelay = Long.parseLong((String)gdProfileVO.getConfigsMap().get(AltPayConstants.GD_ENQUEUE_DELAY));
        
        boolean lockObtained = false;
        boolean retryInProgress = false;
        LockUtil lockUtil = new LockUtil(conn);
        String orderGuid = altPayDAO.doGetOrderGuid(conn, mvo.getMasterOrderNumber());
        String sessionId = this.getLockSessionId();

        try{
             // attempt to lock shopping cart.
             lockObtained = lockUtil.obtainLock(AltPayConstants.GD_LOCK_ENTITY_TYPE,
                                                orderGuid,
                                                AltPayConstants.GD_LOCK_CSR_ID,
                                                sessionId,
                                                AltPayConstants.GD_LOCK_LEVEL);
                                                
             if(lockObtained) {
                 List<GiftCardBillingDetailVO> pvoList = null;
                 List<GiftCardBillingDetailVO> rvoList = null;
                 
                 // There should either be one payment or refund in the message
                 pvoList = loadPaymentInfoList(mvo.getPaymentIdList(), String.valueOf(mvo.getBatchNumber()));
                 rvoList = loadPaymentInfoList(mvo.getRefundIdList(), String.valueOf(mvo.getBatchNumber()));
                                  
                 if((pvoList != null && pvoList.size() > 0) || (rvoList != null && rvoList.size() > 0)) {
                     // Process cart 
                     retryInProgress = this.processCart(conn, pvoList, rvoList, mvo, enqueueDelay);
                 }
             } else {
            	 logger.info("Going to try GiftCard again since could not lock shopping cart");
            	 if (!enqueueMeAgainPapa(mvo, gdPaymentSvc.getRetryCount(), enqueueDelay)) {  // Try again (up to limit) before sending system message
            		 super.sendSystemMessage(con, new Exception(AltPayConstants.GD_ERROR_LOCK_FAILURE), false);
            	 } else {
             		retryInProgress = true;
            	 }
             }
        } catch(EntityLockedException e) { 
        	logger.info("Going to try GiftCard again since entity locked Exception: " + e);
        	if (!enqueueMeAgainPapa(mvo, gdPaymentSvc.getRetryCount(), enqueueDelay)) {  // Try again (up to limit) before sending system message
	            // send nopage notification
	            super.sendSystemMessage(con, new Exception(AltPayConstants.GD_ERROR_LOCK_FAILURE), false);
        	} else {
        		retryInProgress = true;
        	}
        } catch(Throwable t) {
            try {
                logger.error(t);
                logger.info(mvo.toMessageXML());
                super.sendSystemMessage(con, t, false);
            } catch (Throwable tt) {
                logger.error(tt);
            }
        } finally {
            // increment processed count regardless of result (unless retry is in progress)
        	if (!retryInProgress) {
        		super.incrementProcessedCount(mvo.getBatchNumber());
        	}
            if(lockObtained) {
                try {
                    lockUtil.releaseLock(AltPayConstants.GD_LOCK_ENTITY_TYPE,orderGuid,AltPayConstants.GD_LOCK_CSR_ID,sessionId);
                } catch (Exception e){
                    logger.error(e);
                    super.sendSystemMessage(con, e, false);
                }
            }
        }
        
        logger.info("Exiting process of GiftCardCartHandler...");
    }
    
    /**
     * Process actual payment or refund
     * 
     * @param pvoList
     * @param rvoList
     * @param mvo
     * @throws Throwable
     */
    private boolean processCart(Connection conn, List<GiftCardBillingDetailVO> pvoList, List<GiftCardBillingDetailVO> rvoList, 
    							EODMessageVO mvo, long enqueueDelay)
    throws Throwable {
        GiftCardBillingDetailVO gdvo = null;
        boolean createBillingRec = false;
        boolean retryInProgress = false;

        try {
	    	if(pvoList != null && pvoList.size() > 0) {
	    		
	    		// Process payment
	    		//
	    		logger.info("Processing GiftCard payment");
	            gdvo = (GiftCardBillingDetailVO)pvoList.get(0);
	            getPaymentTransactionId(conn, gdvo);
	            gdPaymentSvc.settle(gdvo);
	            // gdPaymentSvc throws exception if error, so we only get here if success
	            createBillingRec = true;
	            gdvo.setStatus(AltPayConstants.GD_SERVICE_SUCCESS);
	            setAcctgStatusFlag(pvoList);
	            super.updateOrderBillStatus(pvoList, mvo.getBatchTime(), ARConstants.ACCTG_STATUS_BILLED);
	
	    	} else if(rvoList != null && rvoList.size() > 0) {
	    		
	        	// Process refund
	    		//
	    		logger.info("Processing GiftCard refund");
	            gdvo = (GiftCardBillingDetailVO)rvoList.get(0);
	            if (logger.isDebugEnabled()) {
	            	logger.debug(gdvo.toString());   // ??? Remove this once done unit testing 
	            }

	            // First make sure enough payment to cover this refund
	            long paymentId = Long.parseLong(gdvo.getPaymentId());
	    		double billedAmtDouble = eodDAO.shouldRefundBeBilled(paymentId);
	    		BigDecimal billedAmt = new BigDecimal(String.valueOf(billedAmtDouble));
	    		logger.debug("Billed amount is: " + billedAmt + " and GiftCard refund amount is: " + gdvo.getReqAmt());
                if (billedAmt.compareTo(gdvo.getReqAmt()) >= 0) {
                	// Use service to process refund
    	            getPaymentTransactionId(conn, gdvo);
    	            gdPaymentSvc.refund(gdvo);
    	            // gdPaymentSvc throws exception if error, so we only get here if success
    	            createBillingRec = true;
    	            gdvo.setStatus(AltPayConstants.GD_SERVICE_SUCCESS);
    	            setAcctgStatusFlag(rvoList);
    	            super.updateRefundStatus(rvoList, mvo.getBatchTime(),ARConstants.ACCTG_STATUS_BILLED);	    			
	    		} else {
		    		// Refund exceeds billed payment/refund total, so send nopage notification
		            createBillingRec = true;  // Create billing record to reflect error
		            gdvo.setStatus(AltPayConstants.GD_SERVICE_FAILURE);  
		            gdvo.setErrorTxt(AltPayConstants.GD_REFUND_NOT_ALLOWED + " - Billed: " + billedAmt + " Refund requested: " + gdvo.getReqAmt());
		            super.sendSystemMessage(con, new Exception(AltPayConstants.GD_REFUND_NOT_ALLOWED), false);
	    		}
	        }    
	    	
        } catch (Throwable t) {
        	logger.info("Going to try GiftCard again since caught throwable: " + t);
        	if (!enqueueMeAgainPapa(mvo, gdPaymentSvc.getRetryCount(), enqueueDelay)) {  // Try again (up to limit) before sending system message
	            // send nopage notification
	            createBillingRec = true;  // Create billing record to reflect error
	            gdvo.setStatus(AltPayConstants.GD_SERVICE_FAILURE);   // Any error text should already be set, but set status to be safe
	            super.sendSystemMessage(con, new Exception(t), false);
        	} else {
        		retryInProgress = true;
        	}
        } finally {
        	if (createBillingRec) {
        		altPayDAO.doCreateBillingDetailGD(con, gdProfileVO, gdvo);
        	}
        }
        return retryInProgress;
    } 
    
    
    /**
     * Retrieve payment information for the payment ids in the list.
     * @param list
     * @param paymentCode
     * @return
     * @throws Throwable
     */
    private List<GiftCardBillingDetailVO> loadPaymentInfoList(List list, String batchNumber) throws Throwable {
        logger.info("entering loadPaymentInfoList");
        List<GiftCardBillingDetailVO> infoList = null;
        if(list != null) {
            Iterator i = list.iterator();
            infoList = new ArrayList<GiftCardBillingDetailVO>();
            while (i.hasNext()) {
                EODPaymentVO pvo = (EODPaymentVO)i.next();
                GiftCardBillingDetailVO gdvo = (GiftCardBillingDetailVO)this.loadPaymentInfo(pvo);
                gdvo.setBillingHeaderId(batchNumber);
                infoList.add(gdvo);
            }
        }
        logger.info("exiting loadPaymentInfoList");
        return infoList;
    }
    
    /**
     * Retrieves payment information specific to Gift Card to prepare for the call.
     * @param pvo
     * @return
     * @throws Throwable
     */
    private AltPayBillingDetailVOBase loadPaymentInfo(EODPaymentVO pvo) throws Throwable {
        logger.info("entering loadPaymentInfo");
        BigDecimal amount = null;
        String authNumber = null;
        String orderGuid = null;
        String refundId = null;
        String accountNum = null;
        String pin = null;
        String captureId = null;
        String paymentType = null;
        String amountStr = null;
        
        CachedResultSet rs = this.getPaymentInfo(pvo);   // Get payment info from database

        GiftCardBillingDetailVO gdvo = new GiftCardBillingDetailVO();
        gdvo.setMasterOrderNumber(pvo.getMasterOrderNumber());
        gdvo.setPaymentCode(pvo.getPaymentInd());
        gdvo.setPaymentId(pvo.getPaymentId());
        
        if(rs != null && rs.next()) {
            paymentType = rs.getString("payment_indicator");
            if(paymentType != null && paymentType.equals("P")) {
            	amountStr = rs.getString("credit_amount");
            } else {
            	amountStr = rs.getString("debit_amount");
            }       
        	if(amountStr == null) {
                throw new Exception("Amount is null for payment id: " + gdvo.getPaymentId());
            } else {
            	amount = new BigDecimal(amountStr);
            }
            authNumber = rs.getString("auth_number");  // Payment authorization  
            orderGuid = rs.getString("order_guid");
            refundId = rs.getString("refund_id");
            pin = rs.getString("gift_card_pin");
            accountNum = rs.getString("cc_number");
            captureId = rs.getString("ap_auth_txt");
            if (rs.getDate("auth_date") != null) {
                gdvo.setAuthDate(EODUtil.convertSQLDate(rs.getDate("auth_date")));            	
            }
            gdvo.setPaymentRecAmt(amount); // amount in db
            gdvo.setReqAmt(amount); // amount to send
            gdvo.setAuthNumber(authNumber);  
            gdvo.setPin(pin); 
            gdvo.setAccountNumber(accountNum);
            gdvo.setRefundId(refundId);
            gdvo.setCaptureTxt(captureId);
        }
        logger.info("exiting loadPaymentInfo");
        return gdvo;
    }

    /**
     * Enqueues cart message again (if maximum retry count not exceeded).
     * Returns true if success, false if retry count exceeded (or error).
     */
    private boolean enqueueMeAgainPapa(EODMessageVO mvo, int maxRetries, long enqueueDelay) {
    	boolean retVal = false;
    	List<String> messageList = null;
    	String dispatchToQueue = gdProfileVO.getDispatchToQueue();
    	long currentAttempt = mvo.getRetryCounter();
    	
    	try {
	    	if (currentAttempt < maxRetries) {
	    		currentAttempt++;
	    		mvo.setRetryCounter(currentAttempt);
	            String cartMessage = mvo.toMessageXML();
	            // Add extra random delay of up to 4 sec.  Helps prevent lock contention on orders with multiple refunds.
	            Random generator = new Random();
	            int extraDelay = generator.nextInt(AltPayConstants.GD_ENQUEUE_EXTRA_DELAY);  
	            long delay = enqueueDelay + extraDelay;
	            logger.info("Re-dispatching with " + delay + "sec delay, GiftCard message: " + cartMessage);
	            if (cartMessage != null) {
	                messageList = new ArrayList<String>();
	                messageList.add(cartMessage);
	                AccountingUtil.dispatchJMSMessageList(dispatchToQueue, messageList, 0, delay);
	            }
	            retVal = true;
	    	} else {
	    		logger.error("GiftCard retry exceeded for " + mvo.getMasterOrderNumber());
	    	}
    	} catch (Throwable t) {
    		logger.error("GiftCard retry error: " + t);
    	}
    	return retVal;
    }
    
    
    /**
     * Obtains existing or creates new Gift Card Transaction ID for the payment/refund.
     * This ID is necessary to support dup-check processing.  That is, each payment/refund will have a
     * transaction ID associated with it so SVS can detect duplicates if we are retrying transactions.
     * 
     * @return
     */
    private String getPaymentTransactionId(Connection conn, GiftCardBillingDetailVO gdvo) throws Throwable {
    	String retId = null;
    	if (StringUtils.isNotBlank(gdvo.getCaptureTxt())) {
    		// We already have ID for this transaction, so re-use it
    		retId = gdvo.getCaptureTxt();
    		if (logger.isDebugEnabled()) {
    			logger.debug("Re-using GiftCard Transaction ID: " + retId + " for payment ID: " + gdvo.getPaymentId());
    		}
    	} else {
    		// No existing ID, so create new one then update payment record to save it
    		IdRequestVO idvo = new IdRequestVO(AltPayConstants.GD_SERVICE_CLIENT_NAME, AltPayConstants.GD_SERVICE_REQUEST_TYPE);
    		IdGeneratorUtil idUtil = new IdGeneratorUtil();
    		retId = idUtil.generateId(idvo, conn);
    		logger.info("Obtained new GiftCard Transaction ID: " + retId + " for payment ID: " + gdvo.getPaymentId());
    		gdvo.setCaptureTxt(retId);
    		altPayDAO.doUpdateApAuthTxt(con, gdvo.getPaymentId(), retId);
    	}
    	return retId;
    }
    
    
    /**
     * Overwrites getPaymentInfo in parent.
     * @param pvo
     * @return
     * @throws Throwable
     */
    protected CachedResultSet getPaymentInfo(EODPaymentVO pvo) throws Throwable {
        return eodDAO.doGetPaymentById(pvo.getPaymentId()); 
    }    

    public void setGdProfileVO(GiftCardProfileVO gdProfileVO) {
        this.gdProfileVO = gdProfileVO;
    }

    public GiftCardProfileVO getGdProfileVO() {
        return gdProfileVO;
    }
    
    public GiftCardPaymentSvc getGdPaymentSvc() {
		return gdPaymentSvc;
	}

	public void setGdPaymentSvc(GiftCardPaymentSvc gdPaymentSvc) {
		this.gdPaymentSvc = gdPaymentSvc;
	}

	public void setEodDAO(EODDAO eodDAO) {
        this.eodDAO = eodDAO;
    }

    public EODDAO getEodDAO() {
        return eodDAO;
    }
}
