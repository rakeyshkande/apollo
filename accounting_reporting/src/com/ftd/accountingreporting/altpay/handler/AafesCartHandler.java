package com.ftd.accountingreporting.altpay.handler;

import com.ftd.accountingreporting.altpay.vo.AafesBillingDetailVO;
import com.ftd.accountingreporting.altpay.vo.AafesProfileVO;
import com.ftd.accountingreporting.altpay.vo.AltPayBillingDetailVOBase;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.constant.AltPayConstants;
import com.ftd.accountingreporting.dao.AafesDAO;
import com.ftd.accountingreporting.dao.EODDAO;
import com.ftd.accountingreporting.exception.EntityLockedException;
import com.ftd.accountingreporting.util.AccountingUtil;
import com.ftd.accountingreporting.util.HttpUtil;
import com.ftd.accountingreporting.util.LockUtil;
import com.ftd.accountingreporting.vo.EODMessageVO;
import com.ftd.accountingreporting.vo.EODPaymentVO;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

import java.math.BigDecimal;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;


/**
 * This class is responsible for processing the Aafes payments and refunds
 * the the shopping cart.
 */
public class AafesCartHandler extends AltPayCartHandlerBase {

    Logger logger = new Logger("com.ftd.accountingreporting.altpay.handler.AafesCartHandler");
    AafesProfileVO aafesProfileVO;
    AafesDAO aafesDAO;
    EODDAO eodDAO;
    
    public AafesCartHandler() {}
    

    
    /**
     * This is the main method. It
     * 1. Attempts to lock the shopping cart. If a lock cannot be obtained, it sends a nopage notification.
     * 2. Loads payment information required to process.
     * 3. Validates the payments and refundsl
     * 4. Process the payments and refunds.
     * @param conn
     * @param mvo
     * @throws Throwable
     */     
    public void process(Connection conn, EODMessageVO mvo) throws Throwable {
        logger.info("Entering process of AafesCartHandler...");
        aafesProfileVO.createConfigsMap();

        super.process(conn, mvo); 
        eodDAO.setConnection(conn);
        aafesDAO.setConnection(conn);
        
        boolean lockObtained = false;
        LockUtil lockUtil = new LockUtil(conn);
        String orderGuid = altPayDAO.doGetOrderGuid(conn, mvo.getMasterOrderNumber());
        String sessionId = this.getLockSessionId();

        try{
             // attempt to lock shopping cart.
             lockObtained = lockUtil.obtainLock(AltPayConstants.AAFES_LOCK_ENTITY_TYPE,
                                                orderGuid,
                                                AltPayConstants.AAFES_LOCK_CSR_ID,
                                                sessionId,
                                                AltPayConstants.AAFES_LOCK_LEVEL);
                                                
             if(lockObtained) {
                 // for each payment id in the message, get payment info
                 List pvoList = loadPaymentInfoList(mvo.getPaymentIdList(), String.valueOf(mvo.getBatchNumber()));
                 List rvoList = loadPaymentInfoList(mvo.getRefundIdList(), String.valueOf(mvo.getBatchNumber()));
                 
                 if((pvoList != null && pvoList.size() > 0) || (rvoList != null && rvoList.size() > 0)) {
                     // Performs netting. Sets appropriate action flags. 
                     this.validateCartAndAuth(orderGuid, pvoList, rvoList);
    
                     // Call out to AAFES.
                     this.processCart(pvoList, rvoList, mvo);
                 }
             } else {
                 super.sendSystemMessage(con, new Exception(AltPayConstants.AAFES_ERROR_LOCK_FAILURE), false);
             }
        } catch(EntityLockedException e) { 
            //send nopage notification
             super.sendSystemMessage(con, new Exception(AltPayConstants.AAFES_ERROR_LOCK_FAILURE), false);
        } catch(Throwable t) {
            try {
                logger.error(t);
                logger.info(mvo.toMessageXML());
                super.sendSystemMessage(con, t, false);
            } catch (Throwable tt) {
                logger.error(tt);
            }
        } finally {
            if(lockObtained) {
                try {
                    lockUtil.releaseLock(AltPayConstants.AAFES_LOCK_ENTITY_TYPE,orderGuid,AltPayConstants.AAFES_LOCK_CSR_ID,sessionId);
                } catch (Exception e){
                    logger.error(e);
                    super.sendSystemMessage(con, e, false);
                }
            }
        }
        
        logger.info("Exiting process of AafesCartHandler...");
    }
    
    /**
     * For each payment in the list, 
     * 1. if createHistoryFlag is set for the first object
     *    1.1 created billing detail record in db
     *    1.2 make a call to AAFES
     *        1.2.1 if call successful, update accounting status for payment objects;
     *              update accounting status for refund objects
     *        
     * 2. if createHistoryFlag is not set for the first payment object
     *    2.1 if createHistoryFlag is set for the first refund object
     *        2.2.1 make a refund call to AAFES
     *        2.2.2 if successful,  update accounting status for payments and refunds.
     *    2.2 if createHistoryFlag is not set for the first refund object
     *        update accounting status for payment objects and refund objects 
     *        
     * Note: if this method is called, then there are no refunds calls to make       
     * @param pvoList
     * @throws Throwable
     */
    private void processCart(List pvoList, List rvoList, EODMessageVO mvo) throws Throwable {
        // Update bill/refund status to Billed with time stamp. 
        long timeStamp = Calendar.getInstance().getTimeInMillis();
        String billingDetailId = null;
        
        // Payments - settle each separately. 
        if(pvoList != null && pvoList.size() > 0) {
            for (int i = 0; i<pvoList.size(); i++) {
                AafesBillingDetailVO avo = (AafesBillingDetailVO)pvoList.get(i);
                if(avo.isCreateHistoryFlag()) {
                // Has payment only. 
                // May have payment and valid partial refund. 
                // May have payment and invalid refunds that are over charge amount.
                    // create billing detail
                    billingDetailId = aafesDAO.doCreateBillingDetail(String.valueOf(mvo.getBatchNumber()), avo);
                    avo.setBillingDetailId(billingDetailId);
                    this.doCaptureCall(aafesProfileVO, avo);
                    logger.debug("Calling doCaptureCall...");
                    aafesDAO.doUpdateBillingDetail(avo);
                    
                    if(ARConstants.AAFES_RETURN_CODE_AUTHORIZED.equals(avo.getReturnCode())) {
                        // update billing detail
                        logger.debug("doCaptureCall successful");
                        super.updateOrderBillStatus(pvoList, timeStamp, ARConstants.ACCTG_STATUS_BILLED);
                        super.updateRefundStatus(rvoList, timeStamp,ARConstants.ACCTG_STATUS_BILLED);
                    } 
                } else {
                // Full same day refund
                    logger.debug("processing full same day refund");
                    super.updateOrderBillStatus(pvoList, timeStamp,ARConstants.ACCTG_STATUS_BILLED);
                    super.updateRefundStatus(rvoList, timeStamp,ARConstants.ACCTG_STATUS_BILLED);
                }
            }
        } else if(rvoList != null && rvoList.size() > 0) {
        // has refund only
            logger.debug("processing refunds...");
            AafesBillingDetailVO avo = (AafesBillingDetailVO)rvoList.get(0);
            if(avo.isCreateHistoryFlag()) {
                // create billing detail
                billingDetailId = aafesDAO.doCreateBillingDetail(String.valueOf(mvo.getBatchNumber()), avo);
                avo.setBillingDetailId(billingDetailId);
                this.doRefundCall(aafesProfileVO, avo);
                aafesDAO.doUpdateBillingDetail(avo);
                
                if(ARConstants.AAFES_RETURN_CODE_AUTHORIZED.equals(avo.getReturnCode())) {
                    logger.debug("doRefundCall successful");
                    // update billing detail
                    
                    super.updateOrderBillStatus(pvoList, mvo.getBatchTime(),ARConstants.ACCTG_STATUS_BILLED);
                    super.updateRefundStatus(rvoList, mvo.getBatchTime(), ARConstants.ACCTG_STATUS_BILLED);
                } 
            } // else - refunds are invalid.
        }
    } 
    
    /**
     * Retrieve payment information for the payment ids in the list.
     * @param list
     * @param batchNumber
     * @return
     * @throws Throwable
     */
     
    private List loadPaymentInfoList(List list, String batchNumber) throws Throwable {
        logger.info("entering loadPaymentInfoList");
        List infoList = null;
        if(list != null) {
            Iterator i = list.iterator();
            infoList = new ArrayList();
            while (i.hasNext()) {
                EODPaymentVO pvo = (EODPaymentVO)i.next();
                AafesBillingDetailVO avo = (AafesBillingDetailVO)this.loadPaymentInfo(pvo);
                avo.setBillingHeaderId(batchNumber);
                infoList.add(avo);
            }
        }
        logger.info("exiting loadPaymentInfoList");
        return infoList;
    }
    
    /**
     * Retreives payment information specific to AAFES to prepare for the call.
     * @param pvo
     * @return
     * @throws Throwable
     */
    private AltPayBillingDetailVOBase loadPaymentInfo(EODPaymentVO pvo) throws Throwable {
        logger.info("entering loadPaymentInfo");
        CachedResultSet rs = this.getPaymentInfo(pvo);
        BigDecimal amount = null;
        String paymentType = null;
        
        AafesBillingDetailVO avo = new AafesBillingDetailVO();
        avo.setMasterOrderNumber(pvo.getMasterOrderNumber());
        avo.setPaymentCode(pvo.getPaymentInd());
        avo.setPaymentId(pvo.getPaymentId());
        
        if(rs != null && rs.next()) {            
            paymentType = rs.getString("payment_indicator");
            if(paymentType != null && paymentType.equals("P")) {
                amount = new BigDecimal(rs.getString("credit_amount"));
            } else {
                amount = new BigDecimal(rs.getString("debit_amount"));
            }       
            
            avo.setPaymentRecAmt(amount); // amount in db
            avo.setReqAmt(amount); // amount to send
            
            //Only payment record has aafes_ticket_number.
            avo.setTicketNumber(rs.getString("aafes_ticket_number"));
            avo.setAuthNumber(rs.getString("auth_number"));
            avo.setCcnumber(rs.getString("cc_number"));
            avo.setRefundId(rs.getString("refund_id"));
        }
        logger.info("exiting loadPaymentInfo");
        return avo;
    }
    
    /**
     * Overwrites getPaymentInfo in parent.
     * @param pvo
     * @return
     * @throws Throwable
     */
    protected CachedResultSet getPaymentInfo(EODPaymentVO pvo) throws Throwable {
        return eodDAO.doGetPaymentById(pvo.getPaymentId());
    }    
    
    public void doCaptureCall(AafesProfileVO pvo, AafesBillingDetailVO bvo) throws Throwable {
        doAafesCall(pvo, bvo);
    }
    
    /**
     * Apollo refund screen does not identify if the refund is for original bill or add bill.
     * Try aafes_ticket_number from all payments until success.
     * @param pvo
     * @param bvo
     * @throws Throwable
     */
    public void doRefundCall(AafesProfileVO pvo, AafesBillingDetailVO bvo) throws Throwable {
        // Get a list of aafes ticket numbers.
        CachedResultSet rs = aafesDAO.doGetAafesAuthInfo(bvo.getPaymentId());
        
        //Defect 5924 - try all ticket numbers until success.
        while (rs != null && rs.next()) {
            bvo.setAuthNumber(rs.getString("auth_number"));
            bvo.setTicketNumber(rs.getString("aafes_ticket_number"));
            
            doAafesCall(pvo, bvo);
            if(bvo.getReturnCode().equals(ARConstants.AAFES_RETURN_CODE_AUTHORIZED)) {
                break;
            }
        }
        
    }
    
    /**
     * Make a capture call to AAFES proxy servlet. Update billing detail with response.
     * @param pvo
     * @param bvo
     * @throws Throwable
     */
    public void doAafesCall(AafesProfileVO pvo, AafesBillingDetailVO bvo) throws Throwable {
        // create HashMap for http call 
        String transactionType = null;
        StringTokenizer token = null;
        String returnCode = "";
        String authCode = null;
        String ticketNumber = null;
        String returnMessage = null;
        String amount = "";
        AccountingUtil accountUtil = new AccountingUtil();
        
        String url = (String)pvo.getConfigsMap().get(ARConstants.AAFES_CC_SERVLET_URL);
        
        if(bvo.getPaymentCode().equals("P")) {
            transactionType = ARConstants.AAFES_TRANSACTION_TYPE_PAYMENT;
        } else {
            transactionType = ARConstants.AAFES_TRANSACTION_TYPE_REFUND;
        }
        //format amount to the patter of "###.00"
        amount = accountUtil.formatAmountToPattern(bvo.getReqAmt().toString(), ARConstants.CURRENCY_PATTERN_NO_DOLLAR);
        // remove decimal place.
        amount = amount.substring(0, amount.indexOf(".")) + amount.substring(amount.indexOf(".") + 1);
        
        HashMap params = new HashMap();
        params.put(ARConstants.AAFES_READY_TO_BUILD_TYPE, transactionType);  
        params.put(ARConstants.AAFES_READY_TO_BUILD_CC_NUMBER, bvo.getCcnumber());
        params.put(ARConstants.AAFES_READY_TO_BUILD_AMOUNT, amount);
        params.put(ARConstants.AAFES_READY_TO_BUILD_FACILITY_NBR, pvo.getConfigsMap().get(ARConstants.AAFES_FACILITY));
        params.put(ARConstants.AAFES_READY_TO_BUILD_AUTH_CODE, bvo.getAuthNumber());
        params.put(ARConstants.AAFES_READY_TO_BUILD_TICKET, bvo.getTicketNumber());
            
        HttpUtil httpUtil = new HttpUtil();
        String responseString = httpUtil.send(url, params);
        
        // Retrieve the fields off the packet  
        token = new StringTokenizer(responseString,",");
        if(token.hasMoreTokens()) {
            returnCode = token.nextToken();
        }
        if(token.hasMoreTokens()) {
            authCode = token.nextToken();
        }  
        if(token.hasMoreTokens()) {
            ticketNumber = token.nextToken();
        }
        if(token.hasMoreTokens()) {
            returnMessage  = token.nextToken();
        }
        
        if(returnCode != null) {
            returnCode = returnCode.trim();
        }
        if(returnMessage != null) {
            returnMessage = returnMessage.trim();
        }
        
        bvo.setReturnCode(returnCode);
        bvo.setReturnMessage(returnMessage);
        
        if(returnCode.equals(ARConstants.AAFES_RETURN_CODE_AUTHORIZED)) {
            bvo.setVerifiedFlag(ARConstants.AAFES_BILLING_DETAIL_VERIFIED);
        }
    }

    public void setAafesProfileVO(AafesProfileVO aafesProfileVO) {
        this.aafesProfileVO = aafesProfileVO;
    }

    public AafesProfileVO getAafesProfileVO() {
        return aafesProfileVO;
    }


    public void setAafesDAO(AafesDAO aafesDAO) {
        this.aafesDAO = aafesDAO;
    }

    public AafesDAO getAafesDAO() {
        return aafesDAO;
    }

    public void setEodDAO(EODDAO eodDAO) {
        this.eodDAO = eodDAO;
    }

    public EODDAO getEodDAO() {
        return eodDAO;
    }
}
