package com.ftd.accountingreporting.altpay.handler;

import com.ftd.accountingreporting.altpay.vo.UAProfileVO;
import com.ftd.accountingreporting.constant.AltPayConstants;
import com.ftd.accountingreporting.util.AccountingUtil;
import com.ftd.accountingreporting.vo.EODMessageVO;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class UAEODHandler extends AltPayEODHandlerBase {

    private Logger logger = new Logger("com.ftd.accountingreporting.altpay.handler.UAEODHandler");
    private UAProfileVO uaProfileVO;
    
    public UAEODHandler() {
    }

    /**
     * The process will dispatch payments and refund separately. The payment is in a 
     * message. Refunds for the same cart are consolidated.
     * Only refunds are counted as dispatched. The processed_flag on the header record
     * is set to N. The finalizer will check the dispatched_count and processed_count.
     * It sets the processed_flag to Y once the counts equal. The trigger that updates
     * the processed_flag is not in effect in this case (FTD/database/clean_plsql/trg_alt_pay_eod_ct.trg).
     * @param conn
     * @throws Throwable
     */
    public void process(Connection conn) throws Throwable {
        super.process(conn);
        logger.info("in process of UAEODHandler");
               
        // load configs from DB
        uaProfileVO.createConfigsMap();
        
        // get eod records
        Map eodRecordMap = super.getEODRecords(uaProfileVO);
        
        // create billing header. The header may already exists.
        long billingHeaderId = super.createBillingHeader(uaProfileVO);
        
        // dispatch shopping carts
        EODMessageVO emv = super.getEODMessageVO(uaProfileVO);
        emv.setBatchNumber(billingHeaderId);

        // Do not net payments and refunds.
        // int numCarts = super.dispatchCartsNoNetting(eodRecordMap, emv, uaProfileVO);
        // Perform netting
        int numCarts = super.dispatchCarts(eodRecordMap, emv, uaProfileVO);
        
        
        // If no carts dispatched, trigger finalizer anyhow so it can  set billing header status 
        // to indicate processing complete.  The cart handler will trigger finalizer
        // otherwise.
        if (numCarts == 0) {
            List msgList  = new ArrayList();
            logger.info("No carts dispatched for UA - triggering UA EOD Finalizer now since we're done");
            msgList.add(AltPayConstants.UA_EOD_FINALIZER_PAYLOAD);
            AccountingUtil.dispatchJMSMessageList(AltPayConstants.ALTPAY_EOD_QUEUE, msgList, 0, 0);
        } 
        logger.info("exiting process of UAEODHandler");
    }
    
    public boolean testConnection () {
        logger.info("entering testConnection");
        return true;
    }    

    public void setUaProfileVO(UAProfileVO uaProfileVO) {
        this.uaProfileVO = uaProfileVO;
    }

    public UAProfileVO getUaProfileVO() {
        return uaProfileVO;
    }

}
