package com.ftd.accountingreporting.altpay.handler;

import com.ftd.accountingreporting.altpay.vo.UABillingDetailVO;
import com.ftd.accountingreporting.altpay.vo.UAProfileVO;
import com.ftd.accountingreporting.constant.AltPayConstants;
import com.ftd.accountingreporting.util.SystemMessager;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.File;
import java.io.FileWriter;
import java.math.BigDecimal;

import java.sql.Connection;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;


public class UAEODFinalizer extends AltPayFileHandlerBase implements IAltPayEODHandler {

    private Logger logger = new Logger("com.ftd.accountingreporting.altpay.handler.UAEODFinalizer");
    private UAProfileVO uaProfileVO;

    private static final String FILENAME_DATE_FORMAT = "%1$tY%1$tm%1$td%1$tH%1$tM%1$tS";
    
    private static SimpleDateFormat sdf = new SimpleDateFormat("yyMMdd");
    private static SimpleDateFormat sdf2 = new SimpleDateFormat("DDD");

    public UAEODFinalizer() {
    }
    
    /**
     * Primary access point for this handler
     */
    public void process(Connection conn) throws Throwable {
        super.process(conn);
        logger.info("UAEODFinalizer started");
        String fullFileName = null;
               
        // Load configs from DB
        uaProfileVO.createConfigsMap();  
        File refundFile = null;
        FileWriter out = null;
        
        try {
            ftpServer      = (String)uaProfileVO.getConfigsMap().get(AltPayConstants.UA_FTP_URL);
            ftpOutLocation = (String)uaProfileVO.getConfigsMap().get(AltPayConstants.UA_FTP_OUTBOUND_LOCATION);
            ftpUsername    = (String)uaProfileVO.getConfigsMap().get(AltPayConstants.UA_FTP_USERNAME);
            ftpPassword    = (String)uaProfileVO.getConfigsMap().get(AltPayConstants.UA_FTP_PASSWORD);
            ftpOutArchive     = (String)uaProfileVO.getConfigsMap().get(AltPayConstants.UA_FTP_OUT_ARCHIVE_LOCATION); 
            ftpOutFilename     = (String)uaProfileVO.getConfigsMap().get(AltPayConstants.UA_FTP_OUT_FILENAME);
            String purchaseCode = (String)uaProfileVO.getConfigsMap().get(AltPayConstants.UA_REFUND_BONUS_CODE);
            String purchaseType = (String)uaProfileVO.getConfigsMap().get(AltPayConstants.UA_BONUS_TYPE);
            String partnerCode = (String)uaProfileVO.getConfigsMap().get(AltPayConstants.UA_PARTNER_CODE);
            ftpOutFilename = ftpOutFilename + String.format(FILENAME_DATE_FORMAT, calToday);
    
            //Get detail records to be included in todays settlement file
            List<UABillingDetailVO> detailList = altPayDAO.doGetBillingDetailUA(conn);
            
            //Loop over records and create the contents of the settlement file
            UABillingDetailVO bdvo;
            StringBuffer fileContents = new StringBuffer();
            settlementRecCount = 0;
            
            for (int x=0; x < detailList.size(); x++) {
                bdvo = detailList.get(x);
                String temp = createSettlementRecord(bdvo.getMembershipNumber(), bdvo.getReqRedemptionDate(), 
                		purchaseCode, purchaseType, partnerCode, bdvo.getReqMilesAmt(), bdvo.getMemberLastName(), bdvo.getExternalOrderNumber());
                if(temp != null)
                	fileContents.append(temp);
                	settlementRecCount++;
              }
             
              //Append trailer
              fileContents.append(createSettlementTrailer(settlementRecCount));
              fullFileName = ftpOutArchive + File.separator + ftpOutFilename;
              refundFile = new File(fullFileName);
              out = new FileWriter(refundFile);
              
              //Send settlement file to remote host and archive a copy locally
              String fileContent = fileContents.toString();              
              out.write(fileContent);
              out.close();
              super.sendSettlementFileToServer(ftpOutFilename, fileContent);
              
              //Mask cc number in the local file. Skip first 1 lines, and the last 1 line. 
              //Start from position 2 on the rest of each line.
              if(settlementRecCount > 0){
            	  super.maskLocalFile(refundFile.getPath(), 0, 1, 1, AltPayConstants.UA_ACCOUNT_NUMBER_MASK);
              }
              
            // Indicate we are done by setting the processed flag in the billing header table
            altPayDAO.doUpdateProcessedFlag(conn, uaProfileVO.getPaymentMethodId());
            logger.info("UAEODFinalizerHandler completed");
        } catch (Exception ue) {
            logger.error(ue);
            super.sendSystemMessage(con, "Encountered exception while creating United refund file:" + ue.getMessage(), true);
        }
    }
        
    private String createSettlementRecord(String memberNumber, Date redemptionDate, String purchaseCode, String purchaseType, String partnerCode, int bonusMiles, String memberLastName, String externalOrderNumber) throws Exception{
    	StringBuffer fileRecordFirstHalf = new StringBuffer();
    	StringBuffer fileRecordSecondHalf = new StringBuffer();
    	
        if(redemptionDate == null) {
            redemptionDate = calToday.getTime();
        }
    	
        try {
        	fileRecordFirstHalf.append('2');
        	fileRecordFirstHalf.append(padString(memberNumber, 11, externalOrderNumber, "Member Number"));
        	fileRecordFirstHalf.append(sdf.format(redemptionDate));
        	
        	fileRecordSecondHalf.append("FTD" + sdf2.format(new Date()));
        	fileRecordSecondHalf.append(padString(purchaseCode, 3, externalOrderNumber, "Purchase Code"));
        	fileRecordSecondHalf.append(padString(purchaseType, 3, externalOrderNumber, "Purchase Type"));
        	fileRecordSecondHalf.append("000000");
        	fileRecordSecondHalf.append(padNumber(bonusMiles, 6));
        	fileRecordSecondHalf.append(padString(partnerCode, 2, externalOrderNumber, "Partner Code"));
        	fileRecordSecondHalf.append('P');
        	fileRecordSecondHalf.append(padString("", 20));
        	fileRecordSecondHalf.append(padString(externalOrderNumber, 34, externalOrderNumber, "External Order Number"));
        	fileRecordSecondHalf.append(padString("", 131));
        	fileRecordSecondHalf.append('\n');
        	
        	BigDecimal transactionId = altPayDAO.doGetNextUnitedRefundTranID(con);
        	
        	fileRecordFirstHalf.append(padNumber(transactionId.intValue(), 14));
        	fileRecordFirstHalf.append("000000");
        	
			return fileRecordFirstHalf.toString() + fileRecordSecondHalf.toString();
		} catch (Throwable t) {
			
	        try {
	        	logger.error(t.getMessage());
	            SystemMessager.sendSystemMessage(con, t);
	            return null;
	        } catch (Exception e2) {
	            String errMsg = "Unable to send message to support pager.";
	            logger.fatal(errMsg,e2);
	            return null;
	        }
		}
    }
    
    private String createSettlementTrailer(int settlementRecCount) throws Throwable {
       return "T " + padNumber(settlementRecCount, 8) + padString("", 240);
    }

    private String padNumber(int number, int allowedNumberLength) throws Throwable{
    	String tempNumber = String.valueOf(number);
    	
    	if(tempNumber.length() < allowedNumberLength){
    		int paddingNeeded = allowedNumberLength - tempNumber.length();
    		
    		for(int i=0; i<paddingNeeded; i++){
    			tempNumber = '0' + tempNumber;
    		}
    	}
    	else if(tempNumber.length() > allowedNumberLength){
    			tempNumber = tempNumber.substring(0, allowedNumberLength);
    	}
    	return tempNumber;
    }
    
    private String padString(String stringValue, int allowedStringLength) throws Throwable{
    	String tempString = "";
    	
    	if(stringValue != null){
    		tempString = stringValue;
    	}

    	if(tempString.length() < allowedStringLength){
    		int paddingNeeded = allowedStringLength - tempString.length();
    		for(int i=0; i<paddingNeeded; i++){
    			tempString = tempString + " ";
    		}
    	}
    	else if(tempString.length() > allowedStringLength){
    			tempString = tempString.substring(0, allowedStringLength);
    		}

    	return tempString;
    }
    
    private String padString(String stringValue, int allowedStringLength, String externalOrderNumber, String fieldName) throws Throwable{
    	String tempString = "";
    	if(stringValue != null){
    		tempString = stringValue;
    	}

    	if(tempString.length() < allowedStringLength){
    		int paddingNeeded = allowedStringLength - tempString.length();
    		for(int i=0; i<paddingNeeded; i++){
    			tempString = tempString + " ";
    		}
    	}
    	else if(tempString.length() > allowedStringLength){
    		if(fieldName.equals("Member Number")){
				String errorMessage = "Encountered issue while creating United refund file: field length is greater than its allowed amount. "
						+ "External Order Number: (" + externalOrderNumber + "), "
						+ "Field Name: (" + fieldName + "), "
						+ "Allowed Field Length: (" + allowedStringLength + "), "
						+ "Current Field Length: (" + tempString.length() + "). This order has not been included in the United Refund and will need" 
						+ "to be manually reconciled.";
				logger.error(errorMessage);
				super.sendSystemMessage(con, errorMessage, true);
				throw new Exception(errorMessage);
    		}
    		else{
    			tempString = tempString.substring(0, allowedStringLength);
    		}
    	}

    	return tempString;
    }
    
    public void setUaProfileVO(UAProfileVO uaProfileVO) {
        this.uaProfileVO = uaProfileVO;
    }

    public UAProfileVO getUaProfileVO() {
        return uaProfileVO;
    }
}