package com.ftd.accountingreporting.altpay.handler;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import com.ftd.accountingreporting.altpay.dao.PGAltPayDAO;
import com.ftd.accountingreporting.altpay.vo.PGAltPayVO;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.constant.AltPayConstants;
import com.ftd.accountingreporting.util.AccountingUtil;
import com.ftd.accountingreporting.vo.EODMessageVO;
import com.ftd.accountingreporting.vo.EODPaymentVO;
import com.ftd.accountingreporting.vo.PGEODMessageVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

public class PGPayPalEODHandler {

	public PGAltPayDAO pgAltPayDAO;
	public Connection conn;
	public final static String dispatchToQueue = "PG_ALTPAY_CART";
	
	public PGPayPalEODHandler(Connection conn) {

		pgAltPayDAO = new PGAltPayDAO(conn);
	}

	private static Logger logger = new Logger("com.ftd.accountingreporting.altpay.handler.PGPayPalEODHandler");

	public void process() throws Throwable {

		logger.debug("in process of PayPalEODHandler");
		String paymentType = "PP";

		PGAltPayVO pvo = populatePGAltPayVO(paymentType);
		//logger.debug("PGAltPayVO pvo"+pvo);
		
		Map eodRecordMap = getPGEODRecords(pvo);
		
		// create billing header
		long billingHeaderId = createPGBillingHeader(pvo);
		
		PGEODMessageVO emv = getPGEODMessageVO(pvo);
		emv.setBatchNumber(billingHeaderId);

		// dispatch shopping carts
		dispatchPGCarts(eodRecordMap, emv, pvo);
		logger.debug("exiting process of PayPalEODHandler");
	}

	/**
	 * @param paymentType
	 * @return
	 * @throws Exception
	 */
	private PGAltPayVO populatePGAltPayVO(String paymentType) throws Exception {
		PGAltPayVO pvo = new PGAltPayVO();
		pvo.setPaymentTypeId(paymentType);
		pvo.setAltPayOffSet(getAltPayOffSet());

		return pvo;
	}

	/**
	 * Creates a message text with record keys for each shopping cart and
	 * dispatches the messages.
	 * 
	 * @param eodRecordMap
	 * @param eodMessageVO
	 * @param profile
	 * @throws Throwable
	 */
	protected int dispatchPGCarts(Map eodRecordMap, PGEODMessageVO eodMessageVO, PGAltPayVO pvo) throws Throwable {
		ArrayList paymentList = (ArrayList) eodRecordMap.get("PAYMENT");
		ArrayList refundList = (ArrayList) eodRecordMap.get("REFUND");
		String cartMessage = null;

		List messageList = new ArrayList();
		
		long timeToLive = 0;
		
		int dispatchCount = 0;
		eodMessageVO.setPaymentMethodId(pvo.getPaymentTypeId());

		try {
			do {
				// Compose a cart message
				cartMessage = this.composeMessage(paymentList, refundList, eodMessageVO);
				logger.info("dispatching message=" + cartMessage);
				if (cartMessage != null) {
					messageList.add(cartMessage);
					AccountingUtil.dispatchJMSMessageList(dispatchToQueue, messageList, timeToLive, 0);
					dispatchCount++;
					messageList = new ArrayList();
				}

			} while (cartMessage != null);

			if (messageList.size() > 0) {
				AccountingUtil.dispatchJMSMessageList(dispatchToQueue, messageList, timeToLive, 0);
			}
			
			pgAltPayDAO.doUpdatePGDispatchedCount(eodMessageVO.getBatchNumber(), dispatchCount);
			
		} catch (Exception e) {
			logger.error(e);
			throw e;
		}
		return dispatchCount;
	}

	/**
	 * Construct a message to be sent to the cart processor.
	 * 
	 * @param paymentList
	 * @param refundList
	 * @return
	 * @throws java.lang.Exception
	 */
	protected String composeMessage(List paymentList, List refundList, PGEODMessageVO eodMessageVO) throws Throwable {
		String message = null;
		String masterOrderNumber = null;
		List pids = null;
		List rids = null;
		if (paymentList != null && paymentList.size() > 0) {
			pids = this.getNextCartPaymentIds(paymentList);
			masterOrderNumber = ((EODPaymentVO) pids.get(0)).getMasterOrderNumber();
			rids = this.findIdsByCart(refundList, masterOrderNumber);
		} else if (refundList != null && refundList.size() > 0) {
			rids = this.getNextCartPaymentIds(refundList);
			masterOrderNumber = ((EODPaymentVO) rids.get(0)).getMasterOrderNumber();
		}

		if ((pids != null && pids.size() > 0) || (rids != null && rids.size() > 0)) {

			eodMessageVO.setMasterOrderNumber(masterOrderNumber);
			eodMessageVO.setPaymentIdList(pids);
			eodMessageVO.setRefundIdList(rids);
			message = eodMessageVO.toMessageXML();
		}
		return message;
	}

	/**
	 * Retrieves the next available cart payment list. 1. Put all payment ids
	 * for the next eligible shopping cart and return it. 2. Remove the elements
	 * that have been processed from the payment list so that the next process
	 * can start from index 0.
	 * 
	 * @param paymentList
	 * @return
	 * @throws java.lang.Exception
	 */
	protected List getNextCartPaymentIds(List paymentList) throws Exception {

		ArrayList cartPaymentList = null;
		if (paymentList != null && paymentList.size() > 0) {
			cartPaymentList = new ArrayList();
			EODPaymentVO paymentvo = (EODPaymentVO) paymentList.get(0);
			String masterOrderNumber = paymentvo.getMasterOrderNumber();

			for (int i = 0; i < paymentList.size(); i++) {
				paymentvo = (EODPaymentVO) paymentList.get(i);
				if (masterOrderNumber.equals(paymentvo.getMasterOrderNumber())) {
					cartPaymentList.add(paymentList.remove(i));
					i--;
				} else {
					break;
				}
			}
		}
		return cartPaymentList;
	}

	/**
	 * Returns a list of ids for the given master order number
	 * 
	 * @param refundList
	 * @param masterOrderNumber
	 * @return
	 * @throws java.lang.Exception
	 */
	protected List findIdsByCart(List refundList, String masterOrderNumber) throws Exception {
		ArrayList idList = null;
		EODPaymentVO paymentvo = null;
		if (refundList != null && refundList.size() > 0) {
			idList = new ArrayList();
			for (int i = 0; i < refundList.size(); i++) {
				paymentvo = (EODPaymentVO) refundList.get(i);
				if (masterOrderNumber.compareTo(paymentvo.getMasterOrderNumber()) < 0) {
					break;
				} else if (masterOrderNumber.equals(paymentvo.getMasterOrderNumber())) {
					idList.add(refundList.remove(i));
					i--;
				} 
			}
		}
		return idList;
	}

	private long createPGBillingHeader(PGAltPayVO pvo) throws Throwable {

		String batchNumberStr;
		long batchNumber;
		try {

			batchNumberStr = pgAltPayDAO.doPGCreateBillingHeader(pvo);
			batchNumber = Long.parseLong(batchNumberStr);

		} catch (Exception e) {
			logger.error("Exception in createPGBillingHeader the PG PayPal EOD Records " + e.getMessage());
			throw e;
		}

		return batchNumber;
	}

	private Map getPGEODRecords(PGAltPayVO pvo) throws Throwable {

		return pgAltPayDAO.doGetPGAltPayEODRecords(pvo);

	}

	private int getAltPayOffSet() throws Exception {
		int offSet = 0;
		String offSetString;

		offSetString = ConfigurationUtil.getInstance().getFrpGlobalParm(ARConstants.CONFIG_CONTEXT,
				AltPayConstants.PG_AP_EOD_RUNDATE_OFFSET);
		if (offSetString != null && !offSetString.isEmpty())
			offSet = Integer.parseInt(offSetString);

		return offSet;
	}

	/**
	 * Retrieve the date time for which the batch is run. This is calculated
	 * from current date and a configurable offset value.
	 * 
	 * @param eodStartTime
	 * @return
	 * @throws java.lang.Exception
	 */
	private long getPGEODBatchTime(long eodStartTime) throws Exception {
		long eodBatchTime = 0;

		Calendar batchDate = Calendar.getInstance();
		batchDate.setTimeInMillis(eodStartTime);
		batchDate.add(Calendar.DATE, getAltPayOffSet());
		eodBatchTime = batchDate.getTime().getTime();
		logger.debug("getPGEODBatchTime is " + eodBatchTime);
		return eodBatchTime;
	}

	/**
	 * Converts information in the profile to an object common to the project.
	 * 
	 * @param profile
	 * @return
	 * @throws Throwable
	 */
	protected PGEODMessageVO getPGEODMessageVO(PGAltPayVO pvo) throws Throwable {
		PGEODMessageVO emv = new PGEODMessageVO();
		long eodStartTime = 0;
		long eodBatchTime = 0;

		eodStartTime = Calendar.getInstance().getTimeInMillis();
		eodBatchTime = this.getPGEODBatchTime(eodStartTime);
		emv.setStartTime(eodStartTime);
		emv.setBatchTime(eodBatchTime);

		return emv;
	}

}
