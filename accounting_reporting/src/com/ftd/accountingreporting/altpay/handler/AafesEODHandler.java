package com.ftd.accountingreporting.altpay.handler;

import com.ftd.accountingreporting.altpay.vo.AafesProfileVO;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.dao.AafesDAO;
import com.ftd.accountingreporting.vo.EODMessageVO;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.Date;
import java.util.Map;


public class AafesEODHandler extends AltPayEODHandlerBase {

    private Logger logger = new Logger("com.ftd.accountingreporting.altpay.handler.AafesEODHandler");
    private AafesProfileVO aafesProfileVO;
    private AafesDAO aafesDAO;
    
    public AafesEODHandler() {
    }

    
    public void process(Connection conn) throws Throwable {
        super.process(conn);
        aafesDAO.setConnection(conn);
        logger.info("in process of AafesEODHandler");
               
        // load configs from DB
        aafesProfileVO.createConfigsMap();
        
        // get eod records
        Map eodRecordMap = super.getEODRecords(aafesProfileVO);
        
        EODMessageVO emv = super.getEODMessageVO(aafesProfileVO);
        
        // create billing header
        String billingHeaderId = aafesDAO.doCreateBillingHeader(new Date(emv.getBatchTime()));
        
        emv.setBatchNumber(Long.valueOf(billingHeaderId));        

        // dispatch shopping carts
        super.dispatchCarts(eodRecordMap, emv, aafesProfileVO);
        aafesDAO.doUpdateBillingHeader(billingHeaderId, ARConstants.COMMON_VALUE_YES);
        logger.info("exiting process of AafesEODHandler");
    }
    
    public boolean testConnection () {
        return true;
    }    

    public void setAafesProfileVO(AafesProfileVO aafesProfileVO) {
        this.aafesProfileVO = aafesProfileVO;
    }

    public AafesProfileVO getAafesProfileVO() {
        return aafesProfileVO;
    }

    public void setAafesDAO(AafesDAO aafesDAO) {
        this.aafesDAO = aafesDAO;
    }

    public AafesDAO getAafesDAO() {
        return aafesDAO;
    }
}
