package com.ftd.accountingreporting.altpay.handler;

import com.ftd.accountingreporting.altpay.vo.GiftCardProfileVO;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.constant.AltPayConstants;
import com.ftd.accountingreporting.util.AccountingUtil;
import com.ftd.accountingreporting.vo.EODMessageVO;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class GiftCardEODHandler extends AltPayEODHandlerBase {

    private Logger logger = new Logger("com.ftd.accountingreporting.altpay.handler.GiftCardEODHandler");
    private GiftCardProfileVO gdProfileVO;
    
    public GiftCardEODHandler() {
    }

    
    public void process(Connection conn) throws Throwable {
        super.process(conn);
        logger.info("in process of GiftCardEODHandler");
               
        // load configs from DB
        gdProfileVO.createConfigsMap();
        
        // get cart enqueue delay (in seconds)
        long enqueueDelay = Integer.parseInt((String)gdProfileVO.getConfigsMap().get(AltPayConstants.GD_ENQUEUE_DELAY));
        
        // get eod records
        Map eodRecordMap = super.getEODRecords(gdProfileVO);
        
        // create billing header
        long billingHeaderId = super.createBillingHeader(gdProfileVO);
        
        // dispatch shopping carts
        EODMessageVO emv = super.getEODMessageVO(gdProfileVO);
        emv.setBatchNumber(billingHeaderId);
        super.dispatchPaymentsAndRefunds(eodRecordMap, emv, gdProfileVO, enqueueDelay);
        
        logger.info("exiting process of GiftCardEODHandler");
    }
        
    public boolean testConnection () {
        logger.info("entering testConnection");
        boolean result = true;
        // ??? Does nothing right now
        logger.info("exiting testConnection returning:" + result);
        return result;
    }

    public void setGdProfileVO(GiftCardProfileVO gdProfileVO) {
        this.gdProfileVO = gdProfileVO;
    }

    public GiftCardProfileVO getGdProfileVO() {
        return gdProfileVO;
    }

}
