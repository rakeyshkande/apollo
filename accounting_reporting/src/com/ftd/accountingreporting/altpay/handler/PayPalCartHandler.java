package com.ftd.accountingreporting.altpay.handler;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import urn.ebay.api.PayPalAPI.DoCaptureReq;
import urn.ebay.api.PayPalAPI.DoCaptureRequestType;
import urn.ebay.api.PayPalAPI.DoCaptureResponseType;
import urn.ebay.api.PayPalAPI.PayPalAPIInterfaceServiceService;
import urn.ebay.api.PayPalAPI.RefundTransactionReq;
import urn.ebay.api.PayPalAPI.RefundTransactionRequestType;
import urn.ebay.api.PayPalAPI.RefundTransactionResponseType;
import urn.ebay.api.PayPalAPI.TransactionSearchReq;
import urn.ebay.api.PayPalAPI.TransactionSearchRequestType;
import urn.ebay.api.PayPalAPI.TransactionSearchResponseType;
import urn.ebay.apis.CoreComponentTypes.BasicAmountType;
import urn.ebay.apis.eBLBaseComponents.CompleteCodeType;
import urn.ebay.apis.eBLBaseComponents.CurrencyCodeType;
import urn.ebay.apis.eBLBaseComponents.DoCaptureResponseDetailsType;
import urn.ebay.apis.eBLBaseComponents.ErrorType;
import urn.ebay.apis.eBLBaseComponents.PaymentInfoType;
import urn.ebay.apis.eBLBaseComponents.PaymentTransactionSearchResultType;
import urn.ebay.apis.eBLBaseComponents.RefundType;

import com.ftd.accountingreporting.altpay.vo.AltPayBillingDetailVOBase;
import com.ftd.accountingreporting.altpay.vo.AltPayProfileVOBase;
import com.ftd.accountingreporting.altpay.vo.PayPalBillingDetailVO;
import com.ftd.accountingreporting.altpay.vo.PayPalProfileVO;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.constant.AltPayConstants;
import com.ftd.accountingreporting.exception.EntityLockedException;
import com.ftd.accountingreporting.util.LockUtil;
import com.ftd.accountingreporting.vo.EODMessageVO;
import com.ftd.accountingreporting.vo.EODPaymentVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;


/**
 * This class is responsible for processing the PayPal payments and refunds
 * the the shopping cart. If the payment and refund fullying net out,
 * it sends no transaction but updates accounting status to Billed for the order.
 * If there are both payements and partial refunds, it net out the refunds
 * and send the remaining payment balance to settle. If there are more refund amount
 * that payment amount, it will settle the payment and leave the refunds.
 * The process takes into account of billed payment and refund of this payment type.
 */
public class PayPalCartHandler extends AltPayCartHandlerBase {

    Logger logger = new Logger("com.ftd.accountingreporting.altpay.handler.PayPalCartHandler");
    PayPalProfileVO ppProfileVO;
    
    public PayPalCartHandler() {}
    

    
    /**
     * This is the main method. It
     * 1. Attempts to lock the shopping cart. If a lock cannot be obtained, it sends a nopage notification.
     * 2. Loads payment information required to process.
     * 3. Validates the payments and refunds
     * 4. Process the payments and refunds.
     * @param conn
     * @param mvo
     * @throws Throwable
     */     
    public void process(Connection conn, EODMessageVO mvo) throws Throwable {
        logger.info("Entering process of PayPalCartHandler...");
        ppProfileVO.createConfigsMap();

        super.process(conn, mvo); 
        
        boolean lockObtained = false;
        LockUtil lockUtil = new LockUtil(conn);
        String orderGuid = altPayDAO.doGetOrderGuid(conn, mvo.getMasterOrderNumber());
        String sessionId = this.getLockSessionId();

        try{
             // attempt to lock shopping cart.
             lockObtained = lockUtil.obtainLock(AltPayConstants.PP_LOCK_ENTITY_TYPE,
                                                orderGuid,
                                                AltPayConstants.PP_LOCK_CSR_ID,
                                                sessionId,
                                                AltPayConstants.PP_LOCK_LEVEL);
                                                
             if(lockObtained) {
                 // for each payment id in the message, get payment info
                 List pvoList = loadPaymentInfoList(mvo.getPaymentIdList(), String.valueOf(mvo.getBatchNumber()));
                 List rvoList = loadPaymentInfoList(mvo.getRefundIdList(), String.valueOf(mvo.getBatchNumber()));
                 
                 // Handle payments with no auth and fully refunded. If a payment has no auth and is not fully
                 // refunded, remove from list.
                 this.processNoAuth(pvoList, rvoList);
                 
                 if((pvoList != null && pvoList.size() > 0) || (rvoList != null && rvoList.size() > 0)) {
                     // Performs netting. Sets appropriate action flags. 
                     this.validateCart(orderGuid, pvoList, rvoList);
                     
                     // Call out to PayPal.
                     this.processCart(pvoList, rvoList, mvo);
                 }
             } else {
                 super.sendSystemMessage(con, new Exception(AltPayConstants.PP_ERROR_LOCK_FAILURE), false);
             }
        } catch(EntityLockedException e) { 
            //send nopage notification
             super.sendSystemMessage(con, new Exception(AltPayConstants.PP_ERROR_LOCK_FAILURE), false);
        } catch(Throwable t) {
            try {
                logger.error(t);
                logger.info(mvo.toMessageXML());
                super.sendSystemMessage(con, t, false);
            } catch (Throwable tt) {
                logger.error(tt);
            }
        } finally {
            // increment processed count regardless of result
            super.incrementProcessedCount(mvo.getBatchNumber());
            if(lockObtained) {
                try {
                    lockUtil.releaseLock(AltPayConstants.PP_LOCK_ENTITY_TYPE,orderGuid,AltPayConstants.PP_LOCK_CSR_ID,sessionId);
                } catch (Exception e){
                    logger.error(e);
                    super.sendSystemMessage(con, e, false);
                }
            }
        }
        
        logger.info("Exiting process of PayPalCartHandler...");
    }
    
    /**
     * For each payment in the list, 
     * 1. if createHistoryFlag is set for the first object
     *    1.1 created billing detail record in db
     *    1.2 make a capture call to Pay Pal
     *        1.2.1 if call successful, persist response transaction id (capture id)
     *        1.2.2 if call fails with error code 10602, persist fake capture id.
     *              Notify IT 
     *        1.2.3 update billing detail with response
     *        1.2.4 update accounting status for payment objects
     *        1.2.5 update accounting status for refund objects
     *        
     * 2. if createHistoryFlag is not set for the first payment object
     *    2.1 if createHistoryFlag is set for the first refund object
     *        2.2.1 make a refund call to Pay Pal
     *        2.2.2 if successful, persist response transaction id (refund transaction id)
     *        2.2.3 if call fails with error code 10602, persist fake capture id.
     *              Notify IT.
     *        2.2.4 if successful, update accounting status for payments and refunds.
     *    2.2 if createHistoryFlag is not set for the first refund object
     *        update accounting status for payment objects and refund objects 
     *        
     * Note: if this method is called, then there are no refunds calls to make       
     * @param pvoList
     * @throws Throwable
     */
     
    private void processCart(List pvoList, List rvoList, EODMessageVO mvo) throws Throwable {
        if(pvoList != null  && pvoList.size() > 0) {
            PayPalBillingDetailVO ppvo = (PayPalBillingDetailVO)pvoList.get(0);
            if(ppvo.isCreateHistoryFlag()) {
            // Has payment. 
            // May have valid partial refund. 
            // May have invalid refunds that are over charge amount.
                // create billing detail
                altPayDAO.doCreateBillingDetailPP(con, ppProfileVO, ppvo);
                this.doCaptureCall(ppProfileVO, ppvo);
                logger.debug("Calling doCaptureCall...");
                
                if(AltPayConstants.PP_ACK_SUCCESS.equalsIgnoreCase(ppvo.getResAck()) 
                        && ppvo.getResTransactionId() != null
                        && !ppvo.getResTransactionId().equals("")) {
                    // update billing detail
                    logger.debug("doCaptureCall successful");
                    altPayDAO.doUpdateBillingDetailPP(con, ppProfileVO, ppvo);
                    // update capture id
                    altPayDAO.doUpdateApCaptureTxt(con, ppvo.getPaymentId(),ppvo.getResTransactionId());
                    
                    super.updateOrderBillStatus(pvoList, mvo.getBatchTime(), ARConstants.ACCTG_STATUS_BILLED);
                    super.updateRefundStatus(rvoList, mvo.getBatchTime(),ARConstants.ACCTG_STATUS_BILLED);
                } else {
                    logger.debug("doCaptureCall failed");
                    altPayDAO.doUpdateBillingDetailPP(con, ppProfileVO, ppvo);
                }
            } else {
            // Full same day refund
                logger.debug("processing full same day refund");
                super.updateOrderBillStatus(pvoList, mvo.getBatchTime(),ARConstants.ACCTG_STATUS_BILLED);
                super.updateRefundStatus(rvoList, mvo.getBatchTime(),ARConstants.ACCTG_STATUS_BILLED);
            }
        } else if(rvoList != null && rvoList.size() > 0) {
        // has refund only
            logger.debug("processing refunds...");
            PayPalBillingDetailVO ppvo = (PayPalBillingDetailVO)rvoList.get(0);
            if(ppvo.isCreateHistoryFlag()) {
                // create billing detail
                altPayDAO.doCreateBillingDetailPP(con, ppProfileVO, ppvo);
                this.doRefundCall(ppProfileVO, ppvo);
                
                if(AltPayConstants.PP_ACK_SUCCESS.equalsIgnoreCase(ppvo.getResAck()) 
                    && ppvo.getResTransactionId() != null
                    && !ppvo.getResTransactionId().equals("")) {
                    logger.debug("doRefundCall successful");
                    // update billing detail
                    altPayDAO.doUpdateBillingDetailPP(con, ppProfileVO, ppvo);
                    // update refund transaction id
                    this.updateApRefundTransTxt(rvoList);
                    
                    super.updateOrderBillStatus(pvoList, mvo.getBatchTime(),ARConstants.ACCTG_STATUS_BILLED);
                    super.updateRefundStatus(rvoList, mvo.getBatchTime(), ARConstants.ACCTG_STATUS_BILLED);
                } else {
                    logger.debug("doRefundCall failed");
                    altPayDAO.doUpdateBillingDetailPP(con, ppProfileVO, ppvo);
                }
            } // else - refunds are invalid.
        }
    } 
    
    /**
     * Update refund record in DB with the ap_refund_trans_txt returned from PayPal.
     * @param refunds
     * @throws Throwable
     */
     
    private void updateApRefundTransTxt(List refunds) throws Throwable {
        logger.info("entering updateApRefundTransTxt");
        if(refunds != null) {
            Iterator i = refunds.iterator();
            PayPalBillingDetailVO ppvo = (PayPalBillingDetailVO)refunds.get(0);
            String apRefundTransTxt = ppvo.getResTransactionId();
            
            while (i.hasNext()) {
                PayPalBillingDetailVO ppvo2 = (PayPalBillingDetailVO)i.next();               
                altPayDAO.doUpdateApRefundTransTxt(con, ppvo2.getPaymentId(),apRefundTransTxt); 
            }
        }
        logger.info("exiting updateApRefundTransTxt");
    }
    
    /**
     * Retrieve payment information for the payment ids in the list.
     * @param list
     * @param paymentCode
     * @return
     * @throws Throwable
     */
     
    private List loadPaymentInfoList(List list, String batchNumber) throws Throwable {
        logger.info("entering loadPaymentInfoList");
        List infoList = null;
        if(list != null) {
            Iterator i = list.iterator();
            infoList = new ArrayList();
            while (i.hasNext()) {
                EODPaymentVO pvo = (EODPaymentVO)i.next();
                PayPalBillingDetailVO ppvo = (PayPalBillingDetailVO)this.loadPaymentInfo(pvo);
                ppvo.setBillingHeaderId(batchNumber);
                infoList.add(ppvo);
            }
        }
        logger.info("exiting loadPaymentInfoList");
        return infoList;
    }
    
    /**
     * Retreives payment information specific to PayPal to prepare for the call.
     * @param pvo
     * @return
     * @throws Throwable
     */
     
    private AltPayBillingDetailVOBase loadPaymentInfo(EODPaymentVO pvo) throws Throwable {
        logger.info("entering loadPaymentInfo");
        CachedResultSet rs = super.getPaymentInfo(pvo);
        BigDecimal amount = null;
        String reqTransactionId = null;
        String orderGuid = null;
        String refundId = null;
        
        PayPalBillingDetailVO ppvo = new PayPalBillingDetailVO();
        ppvo.setMasterOrderNumber(pvo.getMasterOrderNumber());
        ppvo.setPaymentCode(pvo.getPaymentInd());
        ppvo.setPaymentId(pvo.getPaymentId());
        
        if(rs != null && rs.next()) {
            if(rs.getString("amount") == null) {
                throw new Exception("Amount is null for payment id: " + ppvo.getPaymentId());
            }
            amount = new BigDecimal(rs.getString("amount"));
            reqTransactionId = rs.getString("req_transaction_id");
            orderGuid = rs.getString("order_guid");
            refundId = rs.getString("refund_id");
            ppvo.setPaymentRecAmt(amount); // amount in db
            ppvo.setReqAmt(amount); // amount to send
            ppvo.setReqTransactionId(reqTransactionId);
            ppvo.setAuthNumber(reqTransactionId);
            ppvo.setRefundId(refundId);
            
        }
        logger.info("exiting loadPaymentInfo");
        return ppvo;
    }
    
    /**
     * Make a capture call to PayPal API. Update billing detail with response.
     * @param pvo
     * @param bvo
     * @throws Throwable
     */
    public void doCaptureCall(PayPalProfileVO pvo, PayPalBillingDetailVO bvo) throws Throwable {
        String resAck = null;
        String resTransactionId = null;
        String resCorrelationId = null;
        String resErrorTxt = null;
        PaymentInfoType pit = null;
        String resGrossAmt = "0.00";
        String resFeeAmt = "0.00";
    
        logger.info("Begin doCapture call: " + bvo.toString());   
        PayPalAPIInterfaceServiceService caller = new PayPalAPIInterfaceServiceService(this.getPaypalConfig(pvo));
        
        BigDecimal reqAmt = bvo.getReqAmt();
        reqAmt.setScale(2, RoundingMode.HALF_UP);
        String max = (String)pvo.getConfigsMap().get(AltPayConstants.PP_MAX_CAPTURE_AMT);

        if (max != null) {
            BigDecimal maxAmt = new BigDecimal(max);
            
            // PayPal do not allow requested amount to exceed 10,000
            if (reqAmt.compareTo(maxAmt) > 0) {
                Exception e = new Exception(AltPayConstants.PP_ERROR_OVER_MAX_CAPTURE_AMT 
                                            + bvo.getPaymentId()
                                            + " Amount requested is " + bvo.getReqAmt());
                logger.error(e);
                bvo.setResErrorMessage(AltPayConstants.PP_ERROR_OVER_MAX_CAPTURE_AMT);
                
                super.sendSystemMessage(con, e, false);
                return;
            }
        }
          
        DoCaptureRequestType request = new DoCaptureRequestType();
        BasicAmountType bat = new BasicAmountType(CurrencyCodeType.USD,reqAmt.toString());

        request.setAmount(bat);
        request.setAuthorizationID(bvo.getReqTransactionId());
        
        // Can call doCapture with full auth amount and type of NotComplete. 
        // Cannot capture remaining funds if previously captured partial funds
        // with type of Complete. Confirmed with BA to set type to Complete
        request.setCompleteType(CompleteCodeType.COMPLETE);
        
        DoCaptureReq req = new DoCaptureReq();
        req.setDoCaptureRequest(request);

        DoCaptureResponseType response = (DoCaptureResponseType) caller.doCapture(req);
            
        if(response != null) {
            
            resAck = response.getAck().toString();
            resCorrelationId = response.getCorrelationID();
            
            DoCaptureResponseDetailsType responseDetail = response.getDoCaptureResponseDetails();
            resErrorTxt = this.parseError(response.getErrors());
            resErrorTxt = this.truncateError(resErrorTxt, pvo);

            // payment info type
            pit = responseDetail.getPaymentInfo();
            if (pit != null) {
                resGrossAmt = pit.getGrossAmount()==null ? resGrossAmt : pit.getGrossAmount().getValue();
                resFeeAmt = pit.getFeeAmount()==null? resFeeAmt : pit.getFeeAmount().getValue();
                resTransactionId = pit.getTransactionID();
            } else {
                Exception e = new Exception("Response payment info is null:" + bvo.toString());
                logger.error(e);
                super.sendSystemMessage(con, e, false);
            }
            
            bvo.setResAck(resAck);
            bvo.setResCorrelationId(resCorrelationId);
            bvo.setResTransactionId(resTransactionId);
            bvo.setResErrorMessage(resErrorTxt);
            bvo.setResGrossAmt(new BigDecimal(resGrossAmt));
            bvo.setResFeeAmt(new BigDecimal(resFeeAmt));
            bvo.setResNetAmt(bvo.getResGrossAmt().subtract(bvo.getResFeeAmt()));
            
            // If the response code is 10602 (transaction already completed)
            // look for the real capture id and fix it.
            if(this.autoFixRequired(response.getErrors(), AltPayConstants.PP_AUTO_FIX_CAPTURE_ERROR) ){
                autoFix(pvo, bvo);
            } 
            if(bvo.getResTransactionId() == null || bvo.getResTransactionId().equals(""))
            {
                super.sendSystemMessage(con, new Exception("PayPal Settlement failed for payment " + bvo.getPaymentId() + ":" + bvo.getResErrorMessage()), false);
            }
            
        } else {
            // Response is null. Send nopage notification
            Exception e = new Exception("Received null response from PayPal:" + bvo.toString());
            logger.error(e);
            super.sendSystemMessage(con, e, false);
        } 
        logger.info("End doCapture call: " + bvo.toString());
    }
    
    /**
     * Parses an array of errors and returns a string
     * Truncate error string to 2000 characters.
     * @param errors
     * @return
     * @throws Throwable
     */
     
    private String parseError(List<ErrorType> errors) throws Throwable {
        String errorString = "";
        logger.info("entering parseError");
        if (errors != null && errors.size() > 0) {
            for(int i=0; i<errors.size(); i++) {
                String errorCode = errors.get(i).getErrorCode();
                String shortMsg = errors.get(i).getShortMessage();
                String longMsg = errors.get(i).getLongMessage();
                String errorLine = AltPayConstants.PP_ERROR_DELIMITER
                                 + AltPayConstants.PP_ERROR_CODE 
                                 + errorCode
                                 + AltPayConstants.PP_ERROR_SHORT_MSG
                                 + shortMsg
                                 + AltPayConstants.PP_ERROR_LONG_MSG
                                 + longMsg;
                
                errorString += errorLine;
            }
        }
        logger.error(errorString);
        logger.info("exiting parseError");
        return errorString;
    }
    
    /**
     * chop string to the table column allocated size
     */
     
    private String truncateError(String errorStr, AltPayProfileVOBase pvo) throws Throwable
    {
        String errorMaxSize = (String)(pvo.getConfigsMap()).get(AltPayConstants.PP_RES_ERROR_SIZE);
        
        if(StringUtils.isNotBlank(errorMaxSize)) {
            try{
                int size = Integer.parseInt(errorMaxSize);
                if(errorStr.length() > size) {
                    errorStr = errorStr.substring(0, size-1);
                }
            } catch (Exception e) {
                logger.error(e);
                super.sendSystemMessage(con, e, false);
            }
            
        
        }
        return errorStr;
    }
     
    /**
     * 
     */
     
    private void autoFix(PayPalProfileVO pvo, PayPalBillingDetailVO bvo) throws Throwable {
        logger.info("entering autoFix");
        String transType = null;
        
        if (bvo.getPaymentCode().equals(AltPayConstants.PAYMENT_CODE_PAYMENT)) {
            transType = AltPayConstants.PP_TRANSACTION_SEARCH_TYPE_PAYMENT;
        } else  {
            transType = AltPayConstants.PP_TRANSACTION_SEARCH_TYPE_REFUND;
        }
        
        this.transactionSearchAndFix(pvo, bvo, transType);
        if(bvo.getResTransactionId() != null && !("").equals(bvo.getResTransactionId())) {
            Exception e = new Exception(AltPayConstants.PP_MSG_AUTO_FIX + bvo.getPaymentId());
            super.sendSystemMessage(con, e, false);
        }
        logger.info("exiting autoFix");
    }
    
    /**
     * Test if the response should be auto fixed.
     * The response will be attempted to be auto fixed if the error type code is 10602 for capture
     * (transaction has already been completed)
     * The response will be attempted to be auto fixed if the error type code is 10009 for refund
     * (transaction has already been fully refunded).
     * @param errors
     * @return
     * @throws Throwable
     */
     
    private boolean autoFixRequired(List<ErrorType> errors, String matchCode) throws Throwable {
        boolean autoFix = false;
        
        if (errors != null) {
            for(int i=0; i<errors.size(); i++) {
                String errorCode = errors.get(i).getErrorCode();
                 if(matchCode.equals(errorCode)) {
                    autoFix = true;
                    break;
                }
            }
        }
        return autoFix;
    }
    
    /**
     * This method is used in cases where we do not receive a response from
     * DoCapture or RefundTransaction calls but transactions succeeded on 
     * PayPal side.
     * Perform TransactionSearch to look for real capture id / refund transaction id
     * to auto fix.
     */
    public void transactionSearchAndFix(PayPalProfileVO pvo, PayPalBillingDetailVO bvo, String type) throws Throwable {
        logger.info("Perforning transaction search for auth id=" + bvo.getReqTransactionId() + ", type=" + type + ", amount="+ bvo.getReqAmt().toString());
        
        String transactionId = null;
        String transType = null;
        BigDecimal transAmt = null;
        ConfigurationUtil cu = ConfigurationUtil.getInstance();        
        PayPalAPIInterfaceServiceService caller = new PayPalAPIInterfaceServiceService(this.getPaypalConfig(pvo));
        
        BigDecimal reqAmt = bvo.getReqAmt();
        if(AltPayConstants.PP_TRANSACTION_SEARCH_TYPE_REFUND.equals(type)) {
            reqAmt = reqAmt.negate();
        }
        
        TransactionSearchRequestType request = new TransactionSearchRequestType();
        Calendar calendar = Calendar.getInstance();
        
        try {
            String searchSpanMonth = cu.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, AltPayConstants.PP_TRANSACTION_SEARCH_MONTH_SPAN);
            calendar.add(Calendar.MONTH, -1*Integer.parseInt(searchSpanMonth));
        } 
        catch (Exception e)
        {
            logger.error(e);
            
            // search for the past year by default.
            calendar.add(Calendar.MONTH, -12);
        }  
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'00:00:00Z");
        request.setStartDate(inputFormat.format(calendar.getTime()));
        request.setTransactionID(bvo.getReqTransactionId());
        
        TransactionSearchReq req = new TransactionSearchReq();
        req.setTransactionSearchRequest(request);
        
        TransactionSearchResponseType response = (TransactionSearchResponseType) caller.transactionSearch(req);
        logger.debug("TransactionSearch response ACK=" + response.getAck().toString());
        
        if (response != null && AltPayConstants.PP_ACK_SUCCESS.equalsIgnoreCase(response.getAck().toString())) {
            // Check to see if any transactions were found
        	
        	List<PaymentTransactionSearchResultType> ts = response.getPaymentTransactions();
            if (ts != null) 
            {                                       
                    logger.info("Found " + ts.size() + " records");
                                    
                    // Display the results of the first transaction returned
                    for (int i = 0; i < ts.size(); i++) 
                    {       
                            logger.info("Transaction ID: " + ts.get(i).getTransactionID());
                            logger.info("Transaction Type: " + ts.get(i).getType());
                            logger.info("Gross Amount: " +  ts.get(i).getGrossAmount().getCurrencyID() + " " +  ts.get(i).getGrossAmount().getValue());
                            logger.info("Fee Amount: " +  ts.get(i).getFeeAmount().getCurrencyID() + " " +  ts.get(i).getFeeAmount().getValue());
                            logger.info("Net Amount: " +  ts.get(i).getNetAmount().getCurrencyID() + " " +  ts.get(i).getNetAmount().getValue());
                            
                            transactionId = ts.get(i).getTransactionID();
                            transType = ts.get(i).getType();
                            transAmt = new BigDecimal(ts.get(i).getGrossAmount().getValue());
                            logger.info(transType);
                            
                            if(type.equals(transType) && reqAmt.compareTo(transAmt) == 0 ){
                                logger.info("Auto fix transaction found.");
                                bvo.setResTransactionId(transactionId);
                                bvo.setResAck(AltPayConstants.PP_ACK_SUCCESS);
                                bvo.setResGrossAmt(new BigDecimal(ts.get(i).getGrossAmount().getValue()));
                                bvo.setResFeeAmt(new BigDecimal(ts.get(i).getFeeAmount().getValue()));
                                bvo.setResNetAmt(new BigDecimal(ts.get(i).getNetAmount().getValue()));
                                bvo.setResErrorMessage(AltPayConstants.PP_AUTO_FIX_RES_ERROR);
                            }
                    }
            }
            else {
                logger.info("Transaction search returned 0 transaction.");
            } 
        } else {
            logger.info("Transaction search failed.");
        }
        logger.info("exiting transactionSearchAndFix");
    }

    
    /**
     * Make a refund call to PayPal API. Update billing detail object with response.
     * @param pvo
     * @param bvo
     * @throws Throwable
     */
     
    public void doRefundCall(PayPalProfileVO pvo, PayPalBillingDetailVO bvo) throws Throwable {
        String resAck = null;
        String resTransactionId = null;
        String resCorrelationId = null;
        String resErrorTxt = null;
        String resGrossAmt = "0.00";
        String resFeeAmt = "0.00";
        String resNetAmt = "0.00";
        
        logger.info("Begin refund call: " + bvo.toString());
        
        PayPalAPIInterfaceServiceService caller = new PayPalAPIInterfaceServiceService(this.getPaypalConfig(pvo));
        
        BigDecimal reqAmt = bvo.getReqAmt();
        reqAmt.setScale(2, RoundingMode.HALF_UP);
        
        RefundTransactionRequestType request = new RefundTransactionRequestType();
        BasicAmountType bat = new BasicAmountType(CurrencyCodeType.USD,reqAmt.toString());        
        request.setAmount(bat);
        request.setTransactionID(bvo.getReqTransactionId());
        request.setRefundType(RefundType.PARTIAL);
        
        RefundTransactionReq req = new RefundTransactionReq();
        req.setRefundTransactionRequest(request);

        RefundTransactionResponseType response = (RefundTransactionResponseType) caller.refundTransaction(req);
        
        resAck = response.getAck().toString();
        resCorrelationId = response.getCorrelationID();
        resTransactionId = response.getRefundTransactionID();
        resErrorTxt = this.parseError(response.getErrors());
        resErrorTxt = this.truncateError(resErrorTxt, pvo);
        
        resGrossAmt = response.getGrossRefundAmount()==null? resGrossAmt : response.getGrossRefundAmount().getValue();
        resNetAmt = response.getNetRefundAmount()==null? resNetAmt : response.getNetRefundAmount().getValue();
        resFeeAmt = response.getFeeRefundAmount()==null? resFeeAmt : response.getFeeRefundAmount().getValue();

        bvo.setResAck(resAck);
        bvo.setResCorrelationId(resCorrelationId);
        bvo.setResTransactionId(resTransactionId);
        bvo.setResErrorMessage(resErrorTxt);
        bvo.setResGrossAmt(new BigDecimal(resGrossAmt));
        bvo.setResFeeAmt(new BigDecimal(resFeeAmt));
        bvo.setResNetAmt(new BigDecimal(resNetAmt));
        
        // If the response code is 10009 (transaction already fully refunded)
        // look for the real refund transaction id and fix it.
        if(this.autoFixRequired(response.getErrors(), AltPayConstants.PP_AUTO_FIX_REFUND_ERROR) ){
            autoFix(pvo, bvo);  
            if(!StringUtils.isEmpty(bvo.getRefundId())) {
            	autoFixwithNoTransactionDtl(response.getErrors(), bvo);
            }
         }

        if(bvo.getResTransactionId() == null || bvo.getResTransactionId().equals("")) {
            super.sendSystemMessage(con, new Exception("PayPal Refund failed for payment " + bvo.getPaymentId() + ":" + bvo.getResErrorMessage()), false);
        }
        logger.info("End refund call: " + bvo.toString());
    }    
    

    public HashMap<String,String> getPaypalConfig(PayPalProfileVO pvo) 
    {
        logger.info("entering getPaypalConfig");
        HashMap<String,String> configMap = new HashMap<String,String>(); 
        configMap.put("acct1.UserName", (String)pvo.getConfigsMap().get(AltPayConstants.PP_API_USERNAME));
        configMap.put("acct1.Password", (String)pvo.getConfigsMap().get(AltPayConstants.PP_API_PASSWORD));
        String certPath = (String)pvo.getConfigsMap().get(AltPayConstants.PP_CERTIFICATE_FILE_LOCATION)+(String)pvo.getConfigsMap().get(AltPayConstants.PP_CERTIFICATE_FILENAME);
        configMap.put("acct1.CertPath", certPath);
        configMap.put("acct1.CertKey",  (String)pvo.getConfigsMap().get(AltPayConstants.PP_PRIVATE_KEY_PASSWORD));      
        configMap.put("mode", (String)pvo.getConfigsMap().get(AltPayConstants.PP_ENDPOINT_ENVIRONMENT));
        configMap.put("http.ConnectionTimeOut",(String) pvo.getConfigsMap().get(AltPayConstants.PP_API_CONNECTION_TIMEOUT));
			        
        logger.info("exiting getPaypalConfig");
        return configMap;
    }
    
    
    public void setPpProfileVO(PayPalProfileVO ppProfileVO) {
        this.ppProfileVO = ppProfileVO;
    }

    public PayPalProfileVO getPpProfileVO() {
        return ppProfileVO;
    }
    

	/** This method is to update the refund, payment record status as Billed when 
	 *  PP throws one of the errors with error code 10009 
	 *  		"This transaction has already been fully refunded", 
	 *  		"This transaction already has a chargeback filed"
	 * @param errors
	 * @param bvo
	 */
	private void autoFixwithNoTransactionDtl(List<ErrorType> errors, PayPalBillingDetailVO bvo) {		
		try {
			logger.debug("Entered autoFixwithNoTransactionDtl(), " + bvo.getRefundId());
			 ConfigurationUtil cu = ConfigurationUtil.getInstance();
			if (errors != null && errors.size() > 0) {
				
				String shortMessage = cu.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, AltPayConstants.PP_ERR_SHORT_MSG_TRANS_REFUSED_PARAM);
				String longMessage1 = cu.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, AltPayConstants.PP_ERR_LONG_MSG_FULLY_REFUNDED_PARAM);
				String longMessage2 = cu.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, AltPayConstants.PP_ERR_LONG_MSG_CHARGE_BCK_FILED_PARAM);
				
				for (ErrorType e : errors) {					
					if ((shortMessage.equalsIgnoreCase(e.getShortMessage()))
							&& ((longMessage1.equalsIgnoreCase(e.getLongMessage())) || (longMessage2.equalsIgnoreCase(e.getLongMessage())))) {
						logger.debug("updating refund status as Billed, " + bvo.getRefundId());
						updateRefundStatus(bvo.getRefundId(), ARConstants.ACCTG_STATUS_BILLED);						
					}
				}
			}
		} catch (Throwable e) {
			logger.error("Unable to check if auto fix is required: ", e);
		}
		logger.debug("Exiting autoFixwithNoTransactionDtl()");
	}

}
