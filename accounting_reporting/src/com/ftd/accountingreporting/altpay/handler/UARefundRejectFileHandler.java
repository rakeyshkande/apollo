package com.ftd.accountingreporting.altpay.handler;

import com.ftd.accountingreporting.altpay.vo.UABillingDetailVO;
import com.ftd.accountingreporting.altpay.vo.UAProfileVO;
import com.ftd.accountingreporting.constant.AltPayConstants;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.BufferedReader;
import java.io.File;

import java.io.FileReader;

import java.sql.Connection;

import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class UARefundRejectFileHandler extends AltPayFileHandlerBase  implements IAltPayEODHandler {

    private Logger logger = new Logger("com.ftd.accountingreporting.altpay.handler.UARefundRejectFileHandler");
    private UAProfileVO uaProfileVO;

    public UARefundRejectFileHandler() {
    }
    
    /**
     * Primary access point for this handler
     * 
     * United always sends a reject file. If everything processed correctly, the reject file will only contain a trailer record.
     * If refund file has invalid records, the reject file will contain details and a trailer.
     * 
     * The handler sends a system message if no confirmation is received or if reject file contains detail records.
     */
    public void process(Connection conn) throws Throwable {
        super.process(conn);
        logger.info("UARefundRejectFileHandler started");
               
        // Load configs from DB
        uaProfileVO.createConfigsMap();    
    
        ftpServer      = (String)uaProfileVO.getConfigsMap().get(AltPayConstants.UA_FTP_URL);
        ftpInLocation = (String)uaProfileVO.getConfigsMap().get(AltPayConstants.UA_FTP_INBOUND_LOCATION);
        ftpUsername    = (String)uaProfileVO.getConfigsMap().get(AltPayConstants.UA_FTP_USERNAME);
        ftpPassword    = (String)uaProfileVO.getConfigsMap().get(AltPayConstants.UA_FTP_PASSWORD);
        ftpInArchive     = (String)uaProfileVO.getConfigsMap().get(AltPayConstants.UA_FTP_IN_ARCHIVE_LOCATION);   
        ftpInFilename = (String)uaProfileVO.getConfigsMap().get(AltPayConstants.UA_FTP_IN_FILENAME);   
        
        List<String> rejectFileList = super.processSettlementResponseFromServer();
        String fileNameString = "";
        String pageContent = "";
        String rejFileNames = "";
        String fileName = "";
        File rejFile = null;
        FileReader readFile = null;
        BufferedReader input = null;
        String line = null;
        
        if(rejectFileList != null && rejectFileList.size() > 0) {
            fileNameString = super.fileListToString(rejectFileList);
            try {
                for(int i = 0; i<rejectFileList.size(); i++) {
                    fileName = ftpInArchive + File.separator + rejectFileList.get(i);
                    
                    rejFile = new File(fileName);
                    readFile = new FileReader(rejFile);
                    input = new BufferedReader(readFile);
                    
                    line = input.readLine();
                    // A confirmation file starts with 11 spaces.
                    if (!line.startsWith("           ")) {
                        // This is a reject file.
                        if(!rejFileNames.equals("")) {
                            rejFileNames += ",";
                        }
                        rejFileNames += rejectFileList.get(i);
                    } else {
                        logger.info("Confirmation file received:"+rejectFileList.get(i));
                    }
                }
            
                if(!rejFileNames.equals("")) {
                    // Reject files received. Send system message
                    pageContent = "United miles redemption reject file(s) received: " + rejFileNames + ". \nFile location: " + ftpInArchive;
                    logger.error(pageContent);
                    super.sendSystemMessage(super.con, pageContent, true); 
                }
            } catch (Exception e) {
                pageContent = "United miles redemption reject file(s) received but cannot be processed: " + fileNameString + ". \nFile location: " + ftpInArchive;
                logger.error(pageContent);
                super.sendSystemMessage(super.con, pageContent, true);
            }
        } else {
            pageContent = "United miles redemption refund file confirmation not received.";
            logger.error("No reject files retrieved.");
            super.sendSystemMessage(super.con, pageContent, true);
        }
       
        logger.info("UARefundRejectFileHandler completed");
    }

    public void setUaProfileVO(UAProfileVO uaProfileVO) {
        this.uaProfileVO = uaProfileVO;
    }

    public UAProfileVO getUaProfileVO() {
        return uaProfileVO;
    }
    
}
