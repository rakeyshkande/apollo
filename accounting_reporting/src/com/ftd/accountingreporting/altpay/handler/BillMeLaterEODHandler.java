package com.ftd.accountingreporting.altpay.handler;

import com.ftd.accountingreporting.altpay.vo.BillMeLaterProfileVO;
import com.ftd.accountingreporting.altpay.vo.PayPalProfileVO;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.constant.AltPayConstants;
import com.ftd.accountingreporting.util.AccountingUtil;
import com.ftd.accountingreporting.vo.EODMessageVO;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class BillMeLaterEODHandler extends AltPayEODHandlerBase {

    private Logger logger = new Logger("com.ftd.accountingreporting.altpay.handler.BillMeLaterEODHandler");
    private BillMeLaterProfileVO bmProfileVO;
    
    public BillMeLaterEODHandler() {
    }

    
    public void process(Connection conn) throws Throwable {
        super.process(conn);
        logger.info("in process of BillMeLaterEODHandler");
               
        // load configs from DB
        bmProfileVO.createConfigsMap();
        
        // get eod records
        Map eodRecordMap = super.getEODRecords(bmProfileVO);
        
        // create billing header
        long billingHeaderId = super.createBillingHeader(bmProfileVO);
        
        // dispatch shopping carts
        EODMessageVO emv = super.getEODMessageVO(bmProfileVO);
        emv.setBatchNumber(billingHeaderId);
        int numCarts = super.dispatchCarts(eodRecordMap, emv, bmProfileVO);
        
        // If no carts dispatched, trigger finalizer anyhow so it can get any
        // prior response files from remote server and set billing header status 
        // to indicate processing complete.  The cart handler will trigger finalizer
        // otherwise.
        if (numCarts == 0) {
            List msgList  = new ArrayList();
            logger.info("No carts dispatched for BML - triggering BillMeLater EOD Finalizer now since we're done");
            msgList.add(AltPayConstants.BILLMELATER_EOD_FINALIZER_PAYLOAD);
            AccountingUtil.dispatchJMSMessageList(AltPayConstants.ALTPAY_EOD_QUEUE, msgList, 0, 0);
        }

        logger.info("exiting process of BillMeLaterEODHandler");
    }
        
    public boolean testConnection () {
        logger.info("entering testConnection");
        boolean result = true;
        // ??? Does nothing right now
        logger.info("exiting testConnection returning:" + result);
        return result;
    }

    public void setBmProfileVO(BillMeLaterProfileVO bmProfileVO) {
        this.bmProfileVO = bmProfileVO;
    }

    public BillMeLaterProfileVO getBmProfileVO() {
        return bmProfileVO;
    }

}
