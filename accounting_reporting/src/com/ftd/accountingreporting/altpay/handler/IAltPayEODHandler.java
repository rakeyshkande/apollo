package com.ftd.accountingreporting.altpay.handler;

import java.sql.Connection;


/**
 * This is an interface for Alt Pay EOD processing.
 * @author Christy Hu
 */
public interface IAltPayEODHandler
{
    public void process(Connection conn) throws Throwable;   
}