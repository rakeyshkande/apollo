package com.ftd.accountingreporting.altpay.handler;

import com.ftd.accountingreporting.altpay.vo.AltPayBillingDetailVOBase;
import com.ftd.accountingreporting.altpay.vo.AltPayProfileVOBase;
import com.ftd.accountingreporting.altpay.vo.BillMeLaterBillingDetailVO;
import com.ftd.accountingreporting.altpay.vo.BillMeLaterProfileVO;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.constant.AltPayConstants;
import com.ftd.accountingreporting.exception.EntityLockedException;
import com.ftd.accountingreporting.util.AccountingUtil;
import com.ftd.accountingreporting.util.EODUtil;
import com.ftd.accountingreporting.util.LockUtil;
import com.ftd.accountingreporting.vo.EODMessageVO;
import com.ftd.accountingreporting.vo.EODPaymentVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

import java.math.BigDecimal;
import java.math.RoundingMode;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;


/**
 * This class is responsible for processing the Bill Me Later payments and refunds
 * the the shopping cart. If the payment and refund fullying net out,
 * it sends no transaction but updates accounting status to Billed for the order.
 * If there are both payements and partial refunds, it net out the refunds
 * and send the remaining payment balance to settle. If there are more refund amount
 * that payment amount, it will settle the payment and leave the refunds.
 * The process takes into account of billed payment and refund of this payment type.
 */
public class BillMeLaterCartHandler extends AltPayCartHandlerBase {

    Logger logger = new Logger("com.ftd.accountingreporting.altpay.handler.BillMeLaterCartHandler");
    BillMeLaterProfileVO bmProfileVO;
    
    public BillMeLaterCartHandler() {}
    

    
    /**
     * This is the main method. It
     * 1. Attempts to lock the shopping cart. If a lock cannot be obtained, it sends a nopage notification.
     * 2. Loads payment information required to process.
     * 3. Validates the payments and refundsl
     * 4. Process the payments and refunds.
     * @param conn
     * @param mvo
     * @throws Throwable
     */     
    public void process(Connection conn, EODMessageVO mvo) throws Throwable {
        logger.info("Entering process of BillMeLaterCartHandler...");
        bmProfileVO.createConfigsMap();

        super.process(conn, mvo); 
        
        boolean lockObtained = false;
        LockUtil lockUtil = new LockUtil(conn);
        String orderGuid = altPayDAO.doGetOrderGuid(conn, mvo.getMasterOrderNumber());
        String sessionId = this.getLockSessionId();

        try{
             // attempt to lock shopping cart.
             lockObtained = lockUtil.obtainLock(AltPayConstants.BM_LOCK_ENTITY_TYPE,
                                                orderGuid,
                                                AltPayConstants.BM_LOCK_CSR_ID,
                                                sessionId,
                                                AltPayConstants.BM_LOCK_LEVEL);
                                                
             if(lockObtained) {
                 // for each payment id in the message, get payment info
                 List pvoList = loadPaymentInfoList(mvo.getPaymentIdList(), String.valueOf(mvo.getBatchNumber()));
                 List rvoList = loadPaymentInfoList(mvo.getRefundIdList(), String.valueOf(mvo.getBatchNumber()));
                 
                 // Handle payments with no auth and fully refunded. If a payment has no auth and is not fully
                 // refunded, remove from list.
                 this.processNoAuth(pvoList, rvoList);
                 
                 if((pvoList != null && pvoList.size() > 0) || (rvoList != null && rvoList.size() > 0)) {
                     // Performs netting. Sets appropriate action flags.
                     this.validateCart(orderGuid, pvoList, rvoList);
                     
                     // Process cart 
                     this.processCart(pvoList, rvoList, mvo);
                 }
             } else {
                 super.sendSystemMessage(con, new Exception(AltPayConstants.BM_ERROR_LOCK_FAILURE), false);
             }
        } catch(EntityLockedException e) { 
            //send nopage notification
             super.sendSystemMessage(con, new Exception(AltPayConstants.BM_ERROR_LOCK_FAILURE), false);
        } catch(Throwable t) {
            try {
                logger.error(t);
                logger.info(mvo.toMessageXML());
                super.sendSystemMessage(con, t, false);
            } catch (Throwable tt) {
                logger.error(tt);
            }
        } finally {
            try {
                // Increment processed count regardless of result
                boolean areAllCartsProcessed = super.incrementAndCheckProcessedCount(mvo.getBatchNumber());

                // If all carts processed then dispatch to trigger BML EOD finalizer
                if (areAllCartsProcessed == true) {
                    List msgList  = new ArrayList();
                    logger.info("Enqueuing message for BillMeLater EOD Finalizer");
                    msgList.add(AltPayConstants.BILLMELATER_EOD_FINALIZER_PAYLOAD);
                    AccountingUtil.dispatchJMSMessageList
                        (AltPayConstants.ALTPAY_EOD_QUEUE, msgList, 0, 0);
                }
            } catch(Exception e) {
                logger.error(e);
                super.sendSystemMessage(con, e, false);
            }
            if(lockObtained) {
                try {
                    lockUtil.releaseLock(AltPayConstants.BM_LOCK_ENTITY_TYPE,orderGuid,AltPayConstants.BM_LOCK_CSR_ID,sessionId);
                } catch (Exception e){
                    logger.error(e);
                    super.sendSystemMessage(con, e, false);
                }
            }
        }
        
        logger.info("Exiting process of BillMeLaterCartHandler...");
    }
    
    /**
     * For each payment in the list, 
     * 1. if createHistoryFlag is set for the first object
     *    1.1 created billing detail record in db
     *    1.2 update accounting status for payment objects
     *    1.3 update accounting status for refund objects
     *        
     * 2. if createHistoryFlag is not set for the first payment object
     *    2.1 if createHistoryFlag is set for the first refund object
     *        2.1.1 update accounting status for payments.
     *        2.1.2 update accounting status for refunds.
     *    2.2 if createHistoryFlag is not set for the first refund object
     *        update accounting status for payment objects and refund objects 
     *        
     * @param pvoList
     * @throws Throwable
     */
     
    private void processCart(List pvoList, List rvoList, EODMessageVO mvo) throws Throwable {
        if(pvoList != null && pvoList.size() > 0) {
            BillMeLaterBillingDetailVO bmvo = (BillMeLaterBillingDetailVO)pvoList.get(0);
            if(bmvo.isCreateHistoryFlag()) {
            // Has payment. 
            // May have valid partial refund. 
            // May have invalid refunds that are over charge amount.
                // create billing detail
                logger.debug("processing BML payments");
                altPayDAO.doCreateBillingDetailBM(con, bmProfileVO, bmvo);
                super.updateOrderBillStatus(pvoList, mvo.getBatchTime(), ARConstants.ACCTG_STATUS_BILLED);
                super.updateRefundStatus(rvoList, mvo.getBatchTime(),ARConstants.ACCTG_STATUS_BILLED);
            } else {
            // Full same day refund
                logger.debug("processing BML full same day refund");
                super.updateOrderBillStatus(pvoList, mvo.getBatchTime(),ARConstants.ACCTG_STATUS_BILLED);
                super.updateRefundStatus(rvoList, mvo.getBatchTime(),ARConstants.ACCTG_STATUS_BILLED);
            }
        } else if(rvoList != null  && rvoList.size() > 0) {
        // has refund only
            logger.debug("processing BML refunds...");
            BillMeLaterBillingDetailVO bmvo = (BillMeLaterBillingDetailVO)rvoList.get(0);
            if(bmvo.isCreateHistoryFlag()) {
                // create billing detail
                altPayDAO.doCreateBillingDetailBM(con, bmProfileVO, bmvo);
                super.updateOrderBillStatus(pvoList, mvo.getBatchTime(),ARConstants.ACCTG_STATUS_BILLED);
                super.updateRefundStatus(rvoList, mvo.getBatchTime(), ARConstants.ACCTG_STATUS_BILLED);
            } // else - refunds are invalid.
        }
    } 
    
    
    /**
     * Retrieve payment information for the payment ids in the list.
     * @param list
     * @param paymentCode
     * @return
     * @throws Throwable
     */
     
    private List loadPaymentInfoList(List list, String batchNumber) throws Throwable {
        logger.info("entering loadPaymentInfoList");
        List infoList = null;
        if(list != null) {
            Iterator i = list.iterator();
            infoList = new ArrayList();
            while (i.hasNext()) {
                EODPaymentVO pvo = (EODPaymentVO)i.next();
                BillMeLaterBillingDetailVO bmvo = (BillMeLaterBillingDetailVO)this.loadPaymentInfo(pvo);
                bmvo.setBillingHeaderId(batchNumber);
                infoList.add(bmvo);
            }
        }
        logger.info("exiting loadPaymentInfoList");
        return infoList;
    }
    
    /**
     * Retreives payment information specific to BillMeLater to prepare for the call.
     * @param pvo
     * @return
     * @throws Throwable
     */
     
    private AltPayBillingDetailVOBase loadPaymentInfo(EODPaymentVO pvo) throws Throwable {
        logger.info("entering loadPaymentInfo");
        CachedResultSet rs = super.getPaymentInfo(pvo);
        BigDecimal amount = null;
        String reqTransactionId = null;
        String orderGuid = null;
        String refundId = null;
        String authOrderNum = null;
        String accountNum = null;
        
        BillMeLaterBillingDetailVO bmvo = new BillMeLaterBillingDetailVO();
        bmvo.setMasterOrderNumber(pvo.getMasterOrderNumber());
        bmvo.setPaymentCode(pvo.getPaymentInd());
        bmvo.setPaymentId(pvo.getPaymentId());
        
        if(rs != null && rs.next()) {
            if(rs.getString("amount") == null) {
                throw new Exception("Amount is null for payment id: " + bmvo.getPaymentId());
            }
            amount = new BigDecimal(rs.getString("amount"));
            reqTransactionId = rs.getString("req_transaction_id");
            orderGuid = rs.getString("order_guid");
            refundId = rs.getString("refund_id");
            authOrderNum = rs.getString("acq_reference_number");
            accountNum = rs.getString("ap_account_txt");
            bmvo.setAuthDate(EODUtil.convertSQLDate(rs.getDate("auth_date")));
            bmvo.setPaymentRecAmt(amount); // amount in db
            bmvo.setReqAmt(amount); // amount to send
            bmvo.setAuthNumber(reqTransactionId);  // Payment authorization
            bmvo.setAuthOrderNumber(authOrderNum); // Authorization order number
            bmvo.setAccountNumber(accountNum);
            bmvo.setRefundId(refundId);
        }
        logger.info("exiting loadPaymentInfo");
        return bmvo;
    }
    

    public void setBmProfileVO(BillMeLaterProfileVO bmProfileVO) {
        this.bmProfileVO = bmProfileVO;
    }

    public BillMeLaterProfileVO getBmProfileVO() {
        return bmProfileVO;
    }


}
