package com.ftd.accountingreporting.altpay.handler;

import com.ftd.accountingreporting.altpay.dao.AltPayDAO;
import com.ftd.accountingreporting.altpay.handler.IAltPayEODHandler;
import com.ftd.accountingreporting.altpay.vo.AltPayProfileVOBase;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.constant.AltPayConstants;
import com.ftd.accountingreporting.util.AccountingUtil;
import com.ftd.accountingreporting.vo.EODMessageVO;
import com.ftd.accountingreporting.vo.EODPaymentVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;


/**
 * This is the base class for Alt Pay EOD event
 */

public abstract class AltPayEODHandlerBase extends AltPayHandlerBase implements IAltPayEODHandler {
    private static Logger logger  = new Logger("com.ftd.accountingreporting.altpay.handler.AltPayEODHandlerBase");
    protected Connection con;
    protected AltPayDAO altPayDAO;
    
    
    public void process(Connection _con) throws Throwable {
        con = _con;
    }
    
    protected abstract boolean testConnection () throws Throwable;
    
    /**
     * Creates a billing header for the event
     * @param profile
     * @return
     * @throws Throwable
     */
    protected long createBillingHeader(AltPayProfileVOBase profile) throws Throwable {
        return Long.valueOf(altPayDAO.doCreateBillingHeader(con, profile));
    }
    
    /**
     * Retrieves record keys that should be billed for the payment method
     * id in the profile.
     * @param profile
     * @return Map of 2 lists with keys 'PAYMENT' and 'REFUND'
     * @throws Throwable
     */
    protected Map getEODRecords(AltPayProfileVOBase profile) throws Throwable {
        return altPayDAO.doGetAltPayEODRecords(con, profile);
    }
    
    /**
     * Creates a message text with record keys for each shopping cart
     * and dispatches the messages.
     * @param eodRecordMap
     * @param eodMessageVO
     * @param profile
     * @throws Throwable
     */
    protected int dispatchCarts (Map eodRecordMap, EODMessageVO eodMessageVO,AltPayProfileVOBase profile) throws Throwable {
        ArrayList paymentList = (ArrayList)eodRecordMap.get("PAYMENT");
        ArrayList refundList = (ArrayList)eodRecordMap.get("REFUND");
        String cartMessage = null;

        List messageList = new ArrayList();
        //String masterOrderNumber = null;
        Document cartMsgDoc = null;
        long timeToLive = 0;
        String dispatchToQueue = profile.getDispatchToQueue();
        int dispatchCount = 0;
        eodMessageVO.setPaymentMethodId(profile.getPaymentMethodId());
        
        try {       
            do {
                // Compose a cart message
                cartMessage = this.composeMessage(paymentList, refundList, eodMessageVO);
                logger.info("dispatching message=" + cartMessage);
                if(cartMessage != null) {
                    cartMsgDoc = DOMUtil.getDocument(cartMessage);
                    //masterOrderNumber = (((NodeList)cartMsgDoc.getElementsByTagName("MON")).item(0).getFirstChild()).getNodeValue();
                    messageList.add(cartMessage);

                    AccountingUtil.dispatchJMSMessageList(dispatchToQueue, messageList, timeToLive, 0);
                    dispatchCount++;
                    messageList = new ArrayList();
                }
                
            } while(cartMessage != null);
            
            if(messageList.size() > 0) {
                AccountingUtil.dispatchJMSMessageList(dispatchToQueue, messageList, timeToLive, 0);
                dispatchCount++;
            }
            
            if(!ARConstants.COMMON_VALUE_NO.equals(profile.getUpdateDispatcheCountFlag())) {
                altPayDAO.doUpdateDispatchedCount(con, eodMessageVO.getBatchNumber(), dispatchCount);
            }
        } catch (Exception e) {
            logger.error(e);
            throw e;
        }
        return dispatchCount;
    }    

    /**
     * Creates a message text with record keys for each shopping cart. Do not net between payments and refunds.
     * Dispatch payment (assuming there's only 1 payment) and refunds separately.
     * Combine refunds for the same shopping cart to 1 message (to avoid possible dup).
     * and dispatches the messages.
     * @param eodRecordMap
     * @param eodMessageVO
     * @param profile
     * @throws Throwable
     */
    protected int dispatchCartsNoNetting (Map eodRecordMap, EODMessageVO eodMessageVO,AltPayProfileVOBase profile) throws Throwable {
        ArrayList paymentList = (ArrayList)eodRecordMap.get("PAYMENT");
        ArrayList refundList = (ArrayList)eodRecordMap.get("REFUND");
        String cartMessage = null;

        List messageList = new ArrayList();
        Document cartMsgDoc = null;
        long timeToLive = 0;
        String dispatchToQueue = profile.getDispatchToQueue();
        int dispatchCount = 0;
        eodMessageVO.setPaymentMethodId(profile.getPaymentMethodId());
        
        try {       
            // Dispatch payments.
            do {
                // Compose a cart message
                cartMessage = this.composeMessage(paymentList, null, eodMessageVO);
                logger.info("dispatching message=" + cartMessage);
                if(cartMessage != null) {
                    cartMsgDoc = DOMUtil.getDocument(cartMessage);
                    messageList.add(cartMessage);

                    AccountingUtil.dispatchJMSMessageList(dispatchToQueue, messageList, timeToLive, 0);
                    messageList = new ArrayList();
                }
                
            } while(cartMessage != null);
            
            if(messageList.size() > 0) {
                AccountingUtil.dispatchJMSMessageList(dispatchToQueue, messageList, timeToLive, 0);
            }
            // Dispatch refunds
            messageList = new ArrayList();
            do {
                // Compose a cart message
                cartMessage = this.composeMessage(null, refundList, eodMessageVO);
                logger.info("dispatching message=" + cartMessage);
                if(cartMessage != null) {
                    cartMsgDoc = DOMUtil.getDocument(cartMessage);
                    messageList.add(cartMessage);

                    AccountingUtil.dispatchJMSMessageList(dispatchToQueue, messageList, timeToLive, 0);
                    dispatchCount++;
                    messageList = new ArrayList();
                }
                
            } while(cartMessage != null);
            
            if(messageList.size() > 0) {
                AccountingUtil.dispatchJMSMessageList(dispatchToQueue, messageList, timeToLive, 0);
                dispatchCount++;
            }
            //altPayDAO.doUpdateDispatchedCount(con, eodMessageVO.getBatchNumber(), dispatchCount);
        } catch (Exception e) {
            logger.error(e);
            throw e;
        }
        return dispatchCount;
    }        
    
    
    /**
     * Dispatch payment and refunds separately.  There can be a delay for refunds
     * to give time for payments to process first.
     * @param eodRecordMap
     * @param eodMessageVO
     * @param profile
     * @param refundEnqueueDelay (in seconds)
     * @throws Throwable
     */
    protected int dispatchPaymentsAndRefunds (Map eodRecordMap, EODMessageVO eodMessageVO,AltPayProfileVOBase profile, long refundEnqueueDelay) throws Throwable {
        ArrayList paymentList = (ArrayList)eodRecordMap.get("PAYMENT");
        ArrayList refundList = (ArrayList)eodRecordMap.get("REFUND");
        ArrayList tempList = null;
        String cartMessage = null;

        List messageList = null;

        String dispatchToQueue = profile.getDispatchToQueue();
        int dispatchCount = 0;
        eodMessageVO.setPaymentMethodId(profile.getPaymentMethodId());

		try {
			// Dispatch each payment
	        EODPaymentVO paymentvo;
	        if(paymentList != null && paymentList.size() > 0) {
	            for(int i = 0; i < paymentList.size(); i++) {
	                paymentvo = (EODPaymentVO)paymentList.get(i);
	                tempList = new ArrayList();
	                tempList.add(paymentvo);
	                eodMessageVO.setMasterOrderNumber(paymentvo.getMasterOrderNumber());
	                eodMessageVO.setPaymentIdList(tempList);
	                eodMessageVO.setRefundIdList(null);
	                cartMessage = eodMessageVO.toMessageXML();
	                logger.info("dispatching payment message=" + cartMessage);
	                if (cartMessage != null) {
	                    messageList = new ArrayList();
	                    messageList.add(cartMessage);
	                    AccountingUtil.dispatchJMSMessageList(dispatchToQueue, messageList, 0, 0);
	                    dispatchCount++;
	                }
	            }                 	
	        }
	
	        // Dispatch each refund with delay (to give time for payments to process first)
	        EODPaymentVO refundvo;
	        if(refundList != null && refundList.size() > 0) {
	            Random generator = new Random();
	            for(int i = 0; i < refundList.size(); i++) {
		            // Add extra random delay of up to 4 sec.  Helps prevent lock contention on orders with multiple refunds.
		            int extraDelay = generator.nextInt(AltPayConstants.GD_ENQUEUE_EXTRA_DELAY);  
		            long refundDelay = refundEnqueueDelay + extraDelay;
	                refundvo = (EODPaymentVO)refundList.get(i);
	                tempList = new ArrayList();
	                tempList.add(refundvo);
	                eodMessageVO.setMasterOrderNumber(refundvo.getMasterOrderNumber());
	                eodMessageVO.setPaymentIdList(null);
	                eodMessageVO.setRefundIdList(tempList);
	                cartMessage = eodMessageVO.toMessageXML();
	                logger.info("dispatching refund message=" + cartMessage);
	                if (cartMessage != null) {
	                    messageList = new ArrayList();
	                    messageList.add(cartMessage);
	                    AccountingUtil.dispatchJMSMessageList(dispatchToQueue, messageList, 0, refundDelay);
	                    dispatchCount++;
	                }
	            }                 	
	        }
	        
	        // Save dispatch count
            if(!ARConstants.COMMON_VALUE_NO.equals(profile.getUpdateDispatcheCountFlag())) {
                altPayDAO.doUpdateDispatchedCount(con, eodMessageVO.getBatchNumber(), dispatchCount);
            }
	        
        } catch (Exception e) {
            logger.error(e);
            throw e;
        }
        
        return dispatchCount;
    }        

    
    /**
     * Construct a message to be sent to the cart processor.
     * @param paymentList
     * @param refundList
     * @return 
     * @throws java.lang.Exception
     */
      protected String composeMessage(List paymentList, List refundList, EODMessageVO eodMessageVO) throws Throwable
      {
          String message = null;
          String masterOrderNumber = null;
          List pids = null;
          List rids = null;
          if(paymentList != null && paymentList.size() > 0) {
              pids = this.getNextCartPaymentIds(paymentList);
              masterOrderNumber = ((EODPaymentVO)pids.get(0)).getMasterOrderNumber();
              rids = this.findIdsByCart(refundList, masterOrderNumber);
          } else if (refundList != null && refundList.size() > 0) {
              rids = this.getNextCartPaymentIds(refundList);
              masterOrderNumber = ((EODPaymentVO)rids.get(0)).getMasterOrderNumber();
          }
          
          if((pids != null && pids.size() > 0 ) || 
             (rids != null && rids.size() > 0 ) ) {

             eodMessageVO.setMasterOrderNumber(masterOrderNumber);
             eodMessageVO.setPaymentIdList(pids);
             eodMessageVO.setRefundIdList(rids);
             message = eodMessageVO.toMessageXML();
          }
          return message;
      }    
    /**
     * Retrieves the next available cart payment list. 
     * 1. Put all payment ids for the next eligible shopping cart and return it.
     * 2. Remove the elements that have been processed from the payment list 
     *    so that the next process can start from index 0.
     * @param paymentList
     * @return 
     * @throws java.lang.Exception
     */
    protected List getNextCartPaymentIds(List paymentList) throws Exception 
    {
        
        ArrayList cartPaymentList = null;
        if(paymentList != null && paymentList.size() > 0) {
            cartPaymentList = new ArrayList();
            EODPaymentVO paymentvo = (EODPaymentVO)paymentList.get(0);
            String masterOrderNumber = paymentvo.getMasterOrderNumber();
            
            for(int i = 0; i < paymentList.size(); i++) {
                paymentvo = (EODPaymentVO)paymentList.get(i);
                if(masterOrderNumber.equals(paymentvo.getMasterOrderNumber()) ) {
                    cartPaymentList.add(paymentList.remove(i));  
                    i--;
                } else {
                    break;
                }              
            }         
        }
        return cartPaymentList;
    }      
    
    /**
     * Returns a list of ids for the given master order number
     * @param refundList
     * @param masterOrderNumber
     * @return 
     * @throws java.lang.Exception
     */
      protected List findIdsByCart(List refundList, String masterOrderNumber) throws Exception
      {
          ArrayList idList = null;
          EODPaymentVO paymentvo = null;
          if(refundList != null && refundList.size() > 0) {
            idList = new ArrayList();
            for(int i = 0; i < refundList.size(); i++) {
                paymentvo = (EODPaymentVO)refundList.get(i);
                if(masterOrderNumber.compareTo(paymentvo.getMasterOrderNumber()) < 0 ) {
                    break;
                } else if(masterOrderNumber.equals(paymentvo.getMasterOrderNumber())) {
                    idList.add(refundList.remove(i));
                    i--;
                } //else {
                    // continue;
                  //}              
            } 
          }
          return idList;   
      } 
      
    /**
     * Converts information in the profile to an object common to the project.
     * @param profile
     * @return
     * @throws Throwable
     */
    protected EODMessageVO getEODMessageVO(AltPayProfileVOBase profile) throws Throwable {
        EODMessageVO emv = new EODMessageVO();
        long eodStartTime = 0;
        long eodBatchTime = 0;
        
        eodStartTime = Calendar.getInstance().getTimeInMillis();
        eodBatchTime = this.getEODBatchTime(eodStartTime);
        emv.setStartTime(eodStartTime);
        emv.setBatchTime(eodBatchTime);
        
        return emv;
    }
    
    /**
     * Retrieve the date time for which the batch is run. This is calculated from current date
     * and a configurable offset value.
     * 
     * @param eodStartTime
     * @return 
     * @throws java.lang.Exception
     */
      private long getEODBatchTime(long eodStartTime) throws Exception {
         long eodBatchTime = 0;
         String eodBatchDateOffset = ConfigurationUtil.getInstance().getProperty(ARConstants.CONFIG_FILE, ARConstants.CONST_EOD_BATCH_DATE_OFFSET);
         Calendar batchDate = Calendar.getInstance();
         batchDate.setTimeInMillis(eodStartTime);
         batchDate.add(Calendar.DATE, new Integer(eodBatchDateOffset).intValue());
       
         eodBatchTime = batchDate.getTime().getTime();
         logger.debug("eodBatchTime is " + eodBatchTime);
         return eodBatchTime;
     }  
     
    public void setAltPayDAO(AltPayDAO altPayDAO) {
        this.altPayDAO = altPayDAO;
    }

    public AltPayDAO getAltPayDAO() {
        return altPayDAO;
    }
    
}