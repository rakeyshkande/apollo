package com.ftd.accountingreporting.altpay.handler;

import com.ftd.accountingreporting.vo.EODMessageVO;

import java.sql.Connection;


/**
 * This is an interface for AltPay cart processing.
 * @author Christy Hu
 */
public interface IAltPayCartHandler
{
    public void process(Connection conn, EODMessageVO mvo) throws Throwable;   
}