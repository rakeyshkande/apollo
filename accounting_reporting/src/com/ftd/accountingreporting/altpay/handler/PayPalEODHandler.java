package com.ftd.accountingreporting.altpay.handler;

import com.ftd.accountingreporting.altpay.vo.PayPalProfileVO;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.constant.AltPayConstants;
import com.ftd.accountingreporting.vo.EODMessageVO;
import com.ftd.osp.utilities.plugins.Logger;

import urn.ebay.api.PayPalAPI.PayPalAPIInterfaceServiceService;
import urn.ebay.api.PayPalAPI.GetTransactionDetailsRequestType;
import urn.ebay.api.PayPalAPI.GetTransactionDetailsResponseType;
import urn.ebay.api.PayPalAPI.GetTransactionDetailsReq;

import java.sql.Connection;

import java.util.HashMap;
import java.util.Map;


public class PayPalEODHandler extends AltPayEODHandlerBase {

    private Logger logger = new Logger("com.ftd.accountingreporting.altpay.handler.PayPalEODHandler");
    private PayPalProfileVO ppProfileVO;
    
    public PayPalEODHandler() {
    }

    
    public void process(Connection conn) throws Throwable {
        super.process(conn);
        logger.info("in process of PayPalEODHandler");
               
        // load configs from DB
        ppProfileVO.createConfigsMap();
        
        String timerCallbackFlag = (String)
                (ppProfileVO.getConfigsMap()).get(AltPayConstants.PP_CONNECT_TIMER_FLAG);
        
        // Timer call back is off. Page if fails after a number of tries.
        if (ARConstants.COMMON_VALUE_NO.equals(timerCallbackFlag) &&
            ARConstants.COMMON_VALUE_YES.equals((String)(ppProfileVO.getConfigsMap().get(AltPayConstants.PP_TEST_CONNECTION_FLAG)))) {
            
            String retryCount = (String)
                (ppProfileVO.getConfigsMap()).get(AltPayConstants.PP_CONNECT_RETRY_COUNT);
            String retryInterval = (String)
                (ppProfileVO.getConfigsMap()).get(AltPayConstants.PP_CONNECT_TIMER_MS);
            int retry = 0;
            
            while (!this.testConnection()) {
                if(retry <= Integer.valueOf(retryCount)) { 
                    Thread.currentThread().sleep(Long.valueOf(retryInterval));
                    retry++;
                } else {
                    long retryMin = (Long.valueOf(retryInterval) * Integer.valueOf(retryCount)) / 60000;
                    String message = AltPayConstants.PP_ERROR_CONNECTION + retryMin + " minutes.";
                    super.sendSystemMessage(con, new Exception(message), true);
                    return;
                }
            }
        }
        
        // get eod records
        Map eodRecordMap = super.getEODRecords(ppProfileVO);
        
        // create billing header
        long billingHeaderId = super.createBillingHeader(ppProfileVO);
        
        EODMessageVO emv = super.getEODMessageVO(ppProfileVO);
        emv.setBatchNumber(billingHeaderId);
        
        // dispatch shopping carts
        super.dispatchCarts(eodRecordMap, emv, ppProfileVO);
        logger.info("exiting process of PayPalEODHandler");
    }
    
     
    public HashMap<String,String> getPaypalConfig(PayPalProfileVO pvo) 
    {
        logger.info("entering getPaypalConfig");
        HashMap<String,String> configMap = new HashMap<String,String>(); 
        configMap.put("acct1.UserName", (String)pvo.getConfigsMap().get(AltPayConstants.PP_API_USERNAME));
        configMap.put("acct1.Password", (String)pvo.getConfigsMap().get(AltPayConstants.PP_API_PASSWORD));
        String certPath = (String)pvo.getConfigsMap().get(AltPayConstants.PP_CERTIFICATE_FILE_LOCATION)+(String)pvo.getConfigsMap().get(AltPayConstants.PP_CERTIFICATE_FILENAME);
        configMap.put("acct1.CertPath", certPath);
        configMap.put("acct1.CertKey",  (String)pvo.getConfigsMap().get(AltPayConstants.PP_PRIVATE_KEY_PASSWORD));      
        configMap.put("mode", (String)pvo.getConfigsMap().get(AltPayConstants.PP_ENDPOINT_ENVIRONMENT));
        configMap.put("http.ConnectionTimeOut",(String) pvo.getConfigsMap().get(AltPayConstants.PP_API_CONNECTION_TIMEOUT));
		logger.info("exiting getPaypalConfig");
        return configMap;
    }
    
    
    public void getTransactionDetails(PayPalAPIInterfaceServiceService caller, String transactionID) throws Throwable 
    {
        logger.info("\n########## Get Transaction Details for ##########" + transactionID + "\n");
        GetTransactionDetailsRequestType request = new GetTransactionDetailsRequestType();
        request.setTransactionID(transactionID);
        
        GetTransactionDetailsReq req = new GetTransactionDetailsReq();
        req.setGetTransactionDetailsRequest(request);
                
        GetTransactionDetailsResponseType response = (GetTransactionDetailsResponseType) caller.getTransactionDetails(req);
    
        logger.info("Test connection to PayPal successful");
    }    
    
    
    public boolean testConnection () {
        logger.info("entering testConnection");
        boolean result = true;
        try {
        	ppProfileVO.createConfigsMap();
        	PayPalAPIInterfaceServiceService caller = new PayPalAPIInterfaceServiceService(this.getPaypalConfig(ppProfileVO));        	
            this.getTransactionDetails(caller,(String)ppProfileVO.getConfigsMap().get(AltPayConstants.PP_TEST_TRANSACTION_ID));
        } 
        catch (Throwable e) 
        {
            logger.error(e);
            result = false;
        }
        logger.info("exiting testConnection returning:" + result);
        return result;
    }

    public void setPpProfileVO(PayPalProfileVO ppProfileVO) {
        this.ppProfileVO = ppProfileVO;
    }

    public PayPalProfileVO getPpProfileVO() {
        return ppProfileVO;
    }

}
