package com.ftd.accountingreporting.altpay.handler;

import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.constant.AltPayConstants;
import com.ftd.accountingreporting.exception.EmailDeliveryException;
import com.ftd.accountingreporting.util.EmailUtil;
import com.ftd.accountingreporting.util.SystemMessager;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.osp.utilities.vo.NotificationVO;

import java.sql.Connection;


/**
 * This is the base class for Alternate Payment processing.
 */
public abstract class AltPayHandlerBase {
    private static Logger logger  = new Logger("com.ftd.accountingreporting.altpay.handler.AltPayHandlerBase");
    
    /**
     * Sends a system message. The paging system looks for source of 'End of Day'.
     * For any messages with that souce, it will send a nopage email rather than
     * paging or include in the no page summary.
     * @param con DB connection
     * @param t Throwable object
     * @param whether to page or nopage
     * @throws Throwable
     */
    protected void sendSystemMessage(Connection con, Throwable t, boolean page) throws Throwable {
        try {
             logger.info("Sending system message...");
             String source = "";
             String logMessage = t.getMessage();  
             if(page) {
                source = ARConstants.SM_PAGE_SOURCE;
             } else {
                source = ARConstants.SM_NOPAGE_SOURCE;
             }
             SystemMessager.send(logMessage, 
                                 t, 
                                 source, 
                                 SystemMessager.LEVEL_PRODUCTION, 
                                 AltPayConstants.AP_PAGE_ERROR_TYPE, 
                                 ARConstants.SM_AP_PAGE_SUBJECT,
                                 con);
         
         } catch (Exception ex) {
             logger.error(ex);
         } finally {
             if(logger.isDebugEnabled()){
                 logger.debug("Exiting sendSystemMessage");
             } 
         }   
    }
 
    protected void sendSystemMessage(Connection con, String logMessage, boolean page) throws Throwable {
        try {
             logger.info("Sending system message...");
             String source = "";
             
             if(page) {
                source = ARConstants.SM_PAGE_SOURCE;
             } else {
                source = ARConstants.SM_NOPAGE_SOURCE;
             }
             SystemMessager.send(logMessage, 
                                 null, 
                                 source, 
                                 SystemMessager.LEVEL_PRODUCTION, 
                                 AltPayConstants.AP_PAGE_ERROR_TYPE, 
                                 ARConstants.SM_AP_PAGE_SUBJECT,
                                 con);
         
         } catch (Exception ex) {
             logger.error(ex);
         } finally {
             if(logger.isDebugEnabled()){
                 logger.debug("Exiting sendSystemMessage");
             } 
         }   
    } 
    
    protected void sendEmail(String toAddress, String fromAddress, String subject, String content) throws EmailDeliveryException {
        try {
            NotificationVO notifVO = new NotificationVO();

            notifVO.setMessageTOAddress(toAddress);
            notifVO.setMessageFromAddress(fromAddress);
            notifVO.setSMTPHost(ConfigurationUtil.getInstance().getFrpGlobalParm(ARConstants.MESSAGE_GENERATOR_CONFIG, ARConstants.SMTP_HOST_NAME));
            notifVO.setMessageSubject(subject);
            notifVO.setMessageContent(content);
            
            /* send e-mail */
            EmailUtil emaiUtil = new EmailUtil();
            emaiUtil.sendEmail(notifVO);
        } catch (Exception e) {
            throw new EmailDeliveryException(e.getMessage());
        }  
    } 
    
    
}
