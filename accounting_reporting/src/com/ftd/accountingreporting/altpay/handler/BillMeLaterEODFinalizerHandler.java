package com.ftd.accountingreporting.altpay.handler;

import com.ftd.accountingreporting.altpay.vo.BillMeLaterBillingDetailVO;
import com.ftd.accountingreporting.altpay.vo.BillMeLaterProfileVO;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.constant.AltPayConstants;

import com.ftd.accountingreporting.vo.EODMessageVO;
import com.ftd.accountingreporting.util.AccountingUtil;
import com.ftd.accountingreporting.util.FtpUtil;
import com.ftd.accountingreporting.util.SftpUtil;
import com.ftd.accountingreporting.util.security.FileProcessorPgp;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.File;

import java.sql.Connection;
import java.math.BigDecimal;

import java.util.Map;
import java.util.Calendar;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.io.StringWriter;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;


public class BillMeLaterEODFinalizerHandler extends AltPayEODHandlerBase {

    private Logger logger = new Logger("com.ftd.accountingreporting.altpay.handler.BillMeLaterEODFinalizerHandler");
    private BillMeLaterProfileVO bmProfileVO;
    private String  ftpServer = null;
    private String ftpSecondaryServer = null;
    private String  ftpInLocation = null;
    private String  ftpOutLocation = null;
    private String  ftpUsername = null;
    private String  ftpPassword = null;
    private String  ftpArchive = null;
    private String  ftpTimeLimit = null;
    private String  ftpRetryDelay = null;
    private String  pPid = null;
    private String  pPidPasswd = null;
    private String  pSid = null;
    private String  pSidPasswd = null;
    private String  pDivisionNum = null;
    private String  publicKeyPath = null;
    private int settlementRecCount = 0;
    private int settlementOrderCount = 0;
    private int settlementTotalAmount = 0;
    private int settlementSaleAmount = 0;
    private int settlementRefundAmount = 0;

    // Paymentech Settlement Record Format Statements
    //
    private static final String PAYMENTECH_HEADER_REC =
       "PID=%1$-6s %2$-8s SID=%3$-6s %4$-8s START  %5$ty%5$tm%5$td 3.0.0 %6$60s%n";
    private static final String PAYMENTECH_MERCHANT_REC =
       "MFTD.COM*FLOWERS/GIFTS     800-SENDFTD  %1$80s%n";
    private static final String PAYMENTECH_DETAIL_REC =
       "S%1$10s%2$-22s%3$sBL%4$-19s    %5$012d8401007 %6$ty%6$tm%6$td%7$-6s%8$28s%n";
    private static final String PAYMENTECH_BATCH_TOTAL_REC =
       "B RECS=%1$09d ORDS=%2$09d $TOT=%3$014d $SALE=%4$014d $REFUND=%5$014d%6$25s%n";
    private static final String PAYMENTECH_TOTAL_REC =
       "T RECS=%1$09d ORDS=%2$09d $TOT=%3$014d $SALE=%4$014d $REFUND=%5$014d%6$25s%n";
    private static final String PAYMENTECH_TRAILER_REC =
       "PID=%1$-6s %2$-8s SID=%3$-6s %4$-8s END  %5$ty%5$tm%5$td%6$69s%n";


    public BillMeLaterEODFinalizerHandler() {
    }

    private void reset() throws Throwable {
    	logger.info("Resetting trailer totals...");
        settlementRecCount = 0;
        settlementOrderCount = 0;
        settlementTotalAmount = 0;
        settlementSaleAmount = 0;
        settlementRefundAmount = 0;
    }

    /**
     * Primary access point for this handler
     */
    public void process(Connection conn) throws Throwable {
        super.process(conn);
        logger.info("BillMeLaterEODFinalizerHandler started");
        reset();
        // Load configs from DB
        bmProfileVO.createConfigsMap();

        // Get properties from global_parms/secure_config via Spring context
        //
        ftpServer      = (String)bmProfileVO.getConfigsMap().get(AltPayConstants.BM_FTP_URL);
        ftpSecondaryServer = (String)bmProfileVO.getConfigsMap().get(AltPayConstants.BM_FTP_SECONDARY_URL);
        ftpOutLocation = (String)bmProfileVO.getConfigsMap().get(AltPayConstants.BM_FTP_OUTBOUND_LOCATION);
        ftpInLocation  = (String)bmProfileVO.getConfigsMap().get(AltPayConstants.BM_FTP_INBOUND_LOCATION);
        ftpUsername    = (String)bmProfileVO.getConfigsMap().get(AltPayConstants.BM_FTP_USERNAME);
        ftpPassword    = (String)bmProfileVO.getConfigsMap().get(AltPayConstants.BM_FTP_PASSWORD);
        ftpArchive     = (String)bmProfileVO.getConfigsMap().get(AltPayConstants.BM_FTP_ARCHIVE_LOCATION);
        ftpTimeLimit   = (String)bmProfileVO.getConfigsMap().get(AltPayConstants.BM_FTP_RETRY_TIME_LIMIT);
        ftpRetryDelay  = (String)bmProfileVO.getConfigsMap().get(AltPayConstants.BM_FTP_RETRY_DELAY);
        pPid           = (String)bmProfileVO.getConfigsMap().get(AltPayConstants.BM_PAYMENTECH_PID);
        pPidPasswd     = (String)bmProfileVO.getConfigsMap().get(AltPayConstants.BM_PAYMENTECH_PID_PASSWORD);
        pSid           = (String)bmProfileVO.getConfigsMap().get(AltPayConstants.BM_PAYMENTECH_SID);
        pSidPasswd     = (String)bmProfileVO.getConfigsMap().get(AltPayConstants.BM_PAYMENTECH_SID_PASSWORD);
        pDivisionNum   = (String)bmProfileVO.getConfigsMap().get(AltPayConstants.BM_PAYMENTECH_DIVISION_NUM);
        publicKeyPath  = (String)bmProfileVO.getConfigsMap().get(AltPayConstants.BM_SECURITY_PUBLIC_KEY_PATH);

        // First let's get settlement response (from yesterdays file) and archive it locally
        //
        try {
        	getSettlementResponseFromServer(pPid);
        } 
        catch (Exception e) 
        {
            logger.error("Error attempting to retrieve BML response file: " + e.toString());        
            super.sendSystemMessage(con, e, false);
            delayAndRetry();
            return;
        }

        // Get BillMeLater detail records to be included in todays settlement file
        //
        List bmlList = altPayDAO.doGetBillingDetailBM(conn);

        // Loop over records and create settlement file image
        //
        BillMeLaterBillingDetailVO bdvo;
        StringBuffer fileImg = new StringBuffer();
        if (bmlList != null && bmlList.size() > 0) {
          fileImg.append(createSettlementHeader(pPid, pPidPasswd, pSid, pSidPasswd)); // Header record
          fileImg.append(createSettlementMerchant()); // Merchant record
          for (int x=0; x < bmlList.size(); x++) {
            bdvo = (BillMeLaterBillingDetailVO) bmlList.get(x);
            boolean isPayment = (AltPayConstants.PAYMENT_CODE_PAYMENT.equals(bdvo.getPaymentCode()))?true:false;
            fileImg.append(createSettlementDetail(pDivisionNum, bdvo.getAuthOrderNumber(),
                           isPayment, bdvo.getAccountNumber(), bdvo.getReqAmt(),
                           bdvo.getAuthDate(), bdvo.getAuthNumber()));  // Detail records
          }
          fileImg.append(createSettlementBatchTotal());  // Batch totals record
          fileImg.append(createSettlementTotal());       // Totals record
          fileImg.append(createSettlementTrailer(pPid, pPidPasswd, pSid, pSidPasswd));  // Trailer record

          // Send settlement file to remote host and archive a copy locally
          //
          try {
        	  logger.info("SFTP the settlment file to chase and copy to local server");
        	  sftpSettlementFileToServer(pPid, fileImg.toString());
          } catch (Exception e) {
              logger.error("Error attempting to send BML file: " + e.toString());
              super.sendSystemMessage(con, e, false);
              delayAndRetry();
              return;
          }
        }

        // Indicate we are done by setting the processed flag in the billing header table
        //
        altPayDAO.doUpdateProcessedFlag(conn, AltPayConstants.BM_PAYMENT_METHOD_ID);

    	logger.info("settlementRecCount is:" + settlementRecCount);
    	logger.info("settlementOrderCount is:" + settlementOrderCount);
    	logger.info("settlementTotalAmount is:" + settlementTotalAmount);
    	logger.info("settlementSaleAmount is:" + settlementSaleAmount);
    	logger.info("settlementRefundAmount is:" + settlementRefundAmount);
        logger.info("BillMeLaterEODFinalizerHandler completed");
    }



    /**
     * Creates string representing Paymentech settlement header record
     */
    private String createSettlementHeader(String pid, String pidPasswd, String sid, String sidPasswd) {
       Calendar todaysDate = Calendar.getInstance();
       String s = String.format(PAYMENTECH_HEADER_REC, pid, pidPasswd, sid, sidPasswd,
                                todaysDate, " ");
       settlementRecCount++;
       return s.toUpperCase();
    }

    /**
     * Creates string representing Paymentech settlement merchant record
     */
    private String createSettlementMerchant() {
       String s = String.format(PAYMENTECH_MERCHANT_REC, " ");
       settlementRecCount++;
       return s.toUpperCase();
    }

    /**
     * Creates string representing Paymentech settlement detail record
     */
    private String createSettlementDetail(String divisionNum, String authOrderNum,
                                         boolean isPayment, String accountNum,
                                         BigDecimal paymentAmt, Calendar authDate, String authNum) {
       int paymentAmount = (paymentAmt.multiply(new BigDecimal(100))).intValue();
       String s = String.format(PAYMENTECH_DETAIL_REC, divisionNum, authOrderNum,
                  ((isPayment==true)?"DP":"RF"), accountNum, paymentAmount, authDate, ((authNum!=null)?authNum:" "), " ");
       settlementRecCount++;
       settlementOrderCount++;
       settlementTotalAmount += paymentAmount;  // This is an absolute amount (i.e., refunds added not subtracted)
       if (isPayment) {
           settlementSaleAmount += paymentAmount;
       } else {
           settlementRefundAmount += paymentAmount;
       }
       return s.toUpperCase();
    }

    /**
     * Creates string representing Paymentech settlement batch total record
     */
    private String createSettlementBatchTotal() {
       String s = String.format(PAYMENTECH_BATCH_TOTAL_REC, settlementRecCount, settlementOrderCount,
                                settlementTotalAmount, settlementSaleAmount, settlementRefundAmount, " ");
       settlementRecCount++;
       logger.info("Batch Total Rec:" + s);
       return s.toUpperCase();
    }

    /**
     * Creates string representing Paymentech settlement batch total record
     */
    private String createSettlementTotal() {
       String s = String.format(PAYMENTECH_TOTAL_REC, settlementRecCount, settlementOrderCount,
                                settlementTotalAmount, settlementSaleAmount, settlementRefundAmount, " ");
       logger.info("Total Rec:" + s);
       return s.toUpperCase();
    }

    /**
     * Creates string representing Paymentech settlement trailer record
     */
    private String createSettlementTrailer(String pid, String pidPasswd, String sid, String sidPasswd) {
       Calendar todaysDate = Calendar.getInstance();
       String s = String.format(PAYMENTECH_TRAILER_REC, pid, pidPasswd, sid, sidPasswd,
                                todaysDate, " ");
       return s.toUpperCase();
    }


    
    /**
     * Re-enqueue JMS for ourself so we can try again.
     * This was added since FTP connections to Paymentech server get refused occasionally 
     * but retrying at a later time works.  
     */
    private void delayAndRetry() throws Throwable {
        List msgList  = new ArrayList();
        
        // Rather than a retry count, we keep retrying until a certain time of day
        //
        Calendar calendar = Calendar.getInstance();
        int currHour = calendar.get(Calendar.HOUR_OF_DAY);
        int timeLimitHour = Integer.parseInt(ftpTimeLimit);
        if (currHour < timeLimitHour) {
            logger.info("Re-enqueuing message for BillMeLater EOD Finalizer to try again");
            msgList.add(AltPayConstants.BILLMELATER_EOD_FINALIZER_PAYLOAD);
            AccountingUtil.dispatchJMSMessageList(AltPayConstants.ALTPAY_EOD_QUEUE, msgList, 0, Integer.parseInt(ftpRetryDelay));
        } else {
            String eMsg = "BillMeLater EOD Finalizer retry time limit was reached, so giving up";
            logger.error(eMsg);
            super.sendSystemMessage(con, new Exception(eMsg), true);
        }
    }

    
    // Obsolete - this was the beginning of XML needed if using Paymentech's Orbital Gateway
    //
    private void createXml(int aCount) {
      String ftdMerchantId = "123456";
      String ftdTerminalId = "001";
      String ftdBin = "000001";
      String countTotalStr = Integer.toString(aCount+1);  // Add one for closing record
      try {
        Document doc = DOMUtil.getDocumentBuilder().newDocument();
        Element eTransRequest   = (Element)doc.createElement("transRequest");
    	Element eBatchFileId    = (Element)doc.createElement("batchFileID");
    	Element eUserId         = (Element)doc.createElement("userID");
    	Element eFileDateTime   = (Element)doc.createElement("fileDateTime");
    	Element eFileId         = (Element)doc.createElement("fileId");
  	Element eMarkForCapture = null;
    	Element eTxRefNum       = null;
    	Element eAmount         = null;
    	Element eOrderId        = null;
    	Element eBin            = null;
    	Element eMerchantId     = null;
    	Element eTerminalId     = null;
    	Element eRetryTrace     = null;
    	Element eEndOfDay       = (Element)doc.createElement("endOfDay");
    	Element eBin2           = (Element)doc.createElement("bin");
    	Element eMerchantId2    = (Element)doc.createElement("merchantID");
    	Element eTerminalId2    = (Element)doc.createElement("terminalID");

        eTransRequest.setAttribute("RequestCount", countTotalStr);
        eTransRequest.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        eTransRequest.setAttribute("xsi:noNamespaceSchemaLocation", "C:\\OrbitalRequestSchema014.xsd");

        // Batch header (batchFileID)
        //
    	eUserId.appendChild(doc.createTextNode("123"));
    	eFileDateTime.appendChild(doc.createTextNode("200710121300"));
    	eFileId.appendChild(doc.createTextNode("ABC123456"));
        eBatchFileId.appendChild(eUserId);
        eBatchFileId.appendChild(eFileDateTime);
        eBatchFileId.appendChild(eFileId);
    	eTransRequest.appendChild(eBatchFileId);

        // Individual transactions (markForCapture)
        //
        for (int x=0; x<aCount; x++) {
      	  eMarkForCapture = (Element)doc.createElement("markForCapture");
    	  eTxRefNum       = (Element)doc.createElement("txRefNum");
    	  eAmount         = (Element)doc.createElement("amount");
    	  eOrderId        = (Element)doc.createElement("orderID");
    	  eBin            = (Element)doc.createElement("bin");
    	  eMerchantId     = (Element)doc.createElement("merchantID");
    	  eTerminalId     = (Element)doc.createElement("terminalID");
    	  eRetryTrace     = (Element)doc.createElement("retryTrace");

          eTxRefNum.appendChild(doc.createTextNode("???"));
          eAmount.appendChild(doc.createTextNode("10000"));
          eOrderId.appendChild(doc.createTextNode("xxx"));
          eBin.appendChild(doc.createTextNode(ftdBin));
          eMerchantId.appendChild(doc.createTextNode(ftdMerchantId));
          eTerminalId.appendChild(doc.createTextNode(ftdTerminalId));
          eRetryTrace.appendChild(doc.createTextNode("1234567890"));

          eMarkForCapture.setAttribute("BatchRequestNo", Integer.toString(x+1));
          eMarkForCapture.appendChild(eTxRefNum);
          eMarkForCapture.appendChild(eAmount);
          eMarkForCapture.appendChild(eOrderId);
          eMarkForCapture.appendChild(eBin);
          eMarkForCapture.appendChild(eMerchantId);
          eMarkForCapture.appendChild(eTerminalId);
          eMarkForCapture.appendChild(eRetryTrace);
          eTransRequest.appendChild(eMarkForCapture);
        }

        // Batch close (endOfDay)
        //
        eEndOfDay.setAttribute("BatchRequestNo", countTotalStr);
        eBin2.appendChild(doc.createTextNode(ftdBin));
        eMerchantId2.appendChild(doc.createTextNode(ftdMerchantId));
        eTerminalId2.appendChild(doc.createTextNode(ftdTerminalId));
        eEndOfDay.appendChild(eBin2);
        eEndOfDay.appendChild(eMerchantId2);
        eEndOfDay.appendChild(eTerminalId2);
        eTransRequest.appendChild(eEndOfDay);

        doc.appendChild(eTransRequest);

        Transformer t = TransformerFactory.newInstance().newTransformer();
        StringWriter sw = new StringWriter();
        t.transform(new DOMSource(doc), new StreamResult(sw));
        String xmlStr = sw.toString();
        System.out.println(xmlStr);
      } catch (Exception e) {
        System.out.println(e.toString());
      }
    }

    public boolean testConnection () {
        logger.info("entering testConnection");
        boolean result = true;
        // Does nothing right now
        logger.info("exiting testConnection returning:" + result);
        return result;
    }

    public void setBmProfileVO(BillMeLaterProfileVO bmProfileVO) {
        this.bmProfileVO = bmProfileVO;
    }

    public BillMeLaterProfileVO getBmProfileVO() {
        return bmProfileVO;
    }
    
    

    /**
     * Process settlement response file from Paymentech server.  There should only be
     * one file (yesterdays), but you never know, so assume there could be multiple.
     */
    private void getSettlementResponseFromServer(String pid) throws Exception 
    {

    	//pull all the response file from chase server	

    	logger.info("Get encrypted response file from Paymentech using Primary url and copy to local server path");
    	try
    	{
    	 	SftpUtil sftpUtil = new SftpUtil();
    		sftpUtil.downloadResponseFilesToLocal(ftpInLocation,ftpArchive, ftpServer, ftpUsername, ftpPassword);
    	}
    	catch(Exception e)
    	{
    		logger.info("Couldn't get the file from Primary trying to get encrypted response file from Paymentech using Secondary url and copy to local server path");
    		try{
    			SftpUtil sftpUtil = new SftpUtil();
    			sftpUtil.downloadResponseFilesToLocal(ftpInLocation,ftpArchive, ftpSecondaryServer, ftpUsername, ftpPassword);
    		}
    		catch(Exception ex)
    		{
    			throw new Exception("Not able to reterive the response file neither from primary nor from secondary url. Will retry it for 3 times if not NOC needs to contact the Chase.");
    		}
    	}    
    }

    /**
     * Send settlement file to Paymentech server and save encrypted copy to local filesystem
     */
    private void sftpSettlementFileToServer(String pid, String fileImgStr) throws Exception
    {
      // Determine filenames
      //
      String outSuffix = String.format(".%1$ty%1$tm%1$td", Calendar.getInstance());
      String outRename = pid + outSuffix+".asc";
            
      // encrypt the file    
      File settlementFile = new File(outRename);  
      FileProcessorPgp.encryptDataToFile(fileImgStr.getBytes(), settlementFile, publicKeyPath,false);
      
      //Sftp the file to remote server   
      
      try{
    	  SftpUtil sftpUtil = new SftpUtil();  
    	  sftpUtil.uploadFile(settlementFile, ftpOutLocation, ftpServer, ftpUsername, ftpPassword);
      }
      catch(Exception e)
      {
    	  logger.info("Uploading the settlement file to Paymentech using Secondary url as there was issue with Primary url");
    	  try{
    		  SftpUtil sftpUtil = new SftpUtil();  
    		  sftpUtil.uploadFile(settlementFile, ftpOutLocation, ftpSecondaryServer, ftpUsername, ftpPassword);
    	  }
    	  catch(Exception ex)
    	  {
    		  throw new Exception("Not able to upload the settlement file neither to primary nor to secondary url. Will retry it for 3 times if not NOC needs to contact the Chase.");
    	  }
      }
      
      //Sftp the file to local server
      logger.info("uploading the file to local server ");
      File localFile = new File(ftpArchive, outRename);
      FileProcessorPgp.encryptDataToFile(fileImgStr.getBytes(), localFile, publicKeyPath, false);
      
    }
    

}
