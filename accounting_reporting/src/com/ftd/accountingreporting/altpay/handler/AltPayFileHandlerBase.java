package com.ftd.accountingreporting.altpay.handler;

import com.enterprisedt.net.ftp.FTPException;

import com.ftd.accountingreporting.altpay.dao.AltPayDAO;
import com.ftd.accountingreporting.altpay.vo.AltPayProfileVOBase;
import com.ftd.accountingreporting.altpay.vo.BillMeLaterBillingDetailVO;
import com.ftd.accountingreporting.altpay.vo.BillMeLaterProfileVO;
import com.ftd.accountingreporting.altpay.vo.UAProfileVO;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.constant.AltPayConstants;

import com.ftd.accountingreporting.util.FileArchiveUtil;
import com.ftd.accountingreporting.vo.EODMessageVO;
import com.ftd.accountingreporting.util.FtpUtil;
import com.ftd.accountingreporting.util.security.FileProcessorPgp;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.File;

import java.sql.Connection;
import java.math.BigDecimal;

import java.util.Map;
import java.util.Calendar;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.io.StringWriter;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;


public abstract class AltPayFileHandlerBase extends AltPayHandlerBase {

    private Logger logger = new Logger("com.ftd.accountingreporting.altpay.handler.AltPayFileHandlerBase");
    protected Connection con;
    protected AltPayDAO altPayDAO;
    protected Calendar calToday;
    protected FtpUtil ftpUtil = null;
    protected int detailCount;
    protected int settlementRecCount = 0;
    
    protected String  ftpServer = null;
    protected String  ftpOutLocation = null;
    protected String  ftpUsername = null;
    protected String  ftpPassword = null;
    protected String  ftpOutArchive = null;    
    protected String  ftpOutFilename = null; 
    protected String  ftpInFilename = null; 
    
    protected String ftpInArchive = null;
    protected String ftpInLocation = null;
    
    public void process(Connection _con) throws Throwable {
        con = _con;
        calToday = Calendar.getInstance();
    }
    
    protected FtpUtil getFtpUtil() {
        if(ftpUtil == null) {
            ftpUtil = new FtpUtil();
        }
        return ftpUtil;
    }
    
    /**
     * Send settlement file to server and save encrypted copy to local filesystem
     */
    protected void sendSettlementFileToServer(String filename, String fileImgStr) throws Exception {

      // Send data to remote server.
      // Note we rename remote file after ftp'ing (per Paymentech requirements).
      //
      logger.info("Logging onto " + ftpServer + " as " + ftpUsername);
      ftpUtil = getFtpUtil();
      ftpUtil.login(ftpServer, ftpUsername, ftpPassword);
      ftpUtil.putFile(fileImgStr.getBytes(), filename, ftpOutLocation, false);
      ftpUtil.logout();
    }
    
    /**
     * Process settlement response file from Paymentech server.  There should only be
     * one file (yesterdays), but you never know, so assume there could be multiple.
     */
    protected List processSettlementResponseFromServer() throws Exception {

          List<String> respFiles = new ArrayList<String>();
          String inName;
          String reName;
          // Login to remote ftp server
          //
          ftpUtil = getFtpUtil();
          ftpUtil.login(ftpServer, ftpInLocation, ftpUsername, ftpPassword);
    
          // Get name of available response file(s) from remote server.
          // A response file will have a particular prefix, so get list of all files
          // then loop to pick file(s) with that prefix.
          //
          String[] allFiles = ftpUtil.getDirectoryList("."); // Get all filenames from server
           
          for (int x=0; x < allFiles.length; x++) {
                // retrieve files with matching start string
                if(allFiles[x].startsWith(ftpInFilename)) {
                    inName = allFiles[x];
                    reName = AltPayConstants.PROCESSED_FILE_PREFIX + inName;
                    respFiles.add(allFiles[x]);   // Found a response file
                    ftpUtil.getFile(ftpInArchive, allFiles[x]);
                    
                    // Delete file after processing so it won't be picked up next day.
                    ftpUtil.deleteFile(allFiles[x]);
                }
          }
          
          ftpUtil.logout();
          return respFiles;
    }    
    
    public static void maskLocalFile(String inputFile,
        int headerLineCount, int trailerLineCount, int column, String value) throws Exception {
        FileArchiveUtil.overwriteFileContents(inputFile, headerLineCount, trailerLineCount, column, value);
    }
    
    protected String fileListToString(List<String> fileList) throws Exception {
        String names = "";
        if(fileList != null) {
            for(int i = 0; i<fileList.size(); i++) {
                names = names + fileList.get(i);
                if(i < (fileList.size() - 1)) {
                    names += ",";
                }
            }
        }  
        return names;
    }
    
    public void setAltPayDAO(AltPayDAO altPayDAO) {
       this.altPayDAO = altPayDAO;
    }

    public AltPayDAO getAltPayDAO() {
       return altPayDAO;
    }    
}
