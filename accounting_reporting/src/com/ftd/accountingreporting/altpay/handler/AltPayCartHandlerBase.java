package com.ftd.accountingreporting.altpay.handler;

import java.math.BigDecimal;
import java.net.URL;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import com.ftd.accountingreporting.altpay.dao.AltPayDAO;
import com.ftd.accountingreporting.altpay.vo.AltPayBillingDetailVOBase;
import com.ftd.accountingreporting.altpay.vo.AltPayProfileVOBase;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.constant.AltPayConstants;
import com.ftd.accountingreporting.dao.CommonDAO;
import com.ftd.accountingreporting.vo.EODMessageVO;
import com.ftd.accountingreporting.vo.EODPaymentVO;
import com.ftd.osp.utilities.BillingRecordUtility;
import com.ftd.milespoints.webservice.MilesPointsRequest;
import com.ftd.milespoints.webservice.MilesPointsService;
import com.ftd.milespoints.webservice.MilesPointsServiceImplService;
import com.ftd.milespoints.webservice.PartnerResponse;
import com.ftd.milespoints.webservice.UpdateMilesResp;
import com.ftd.osp.utilities.ConfigurationUtil;

import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MembershipVO;


/**
 * This is the base class for Alternate Payment EOD processing for
 * the individual shopping cart.
 */
public abstract class AltPayCartHandlerBase extends AltPayHandlerBase implements IAltPayCartHandler {
    private static Logger logger  = new Logger("com.ftd.accountingreporting.altpay.handler.AltPayCartHandlerBase");
    protected Connection con;
    protected AltPayDAO altPayDAO;
    protected String paymentMethodId;
    protected static MilesPointsService milesPointsService;
    protected static URL milesPointsWsdlUrl;
    protected ConfigurationUtil util;
    
    public void process(Connection _con, EODMessageVO mvo) throws Throwable {
        con = _con;
        util = new ConfigurationUtil();
    }
    
    /**
     * Retrieves payment information for the given payment key.
     * @param pvo
     * @return
     * @throws Throwable
     */
    protected CachedResultSet getPaymentInfo(EODPaymentVO pvo) throws Throwable {
        return altPayDAO.doGetApPaymentInfo(con, pvo);
    }
    
    protected void incrementProcessedCount(long billingHeaderId) throws Throwable {
        altPayDAO.doIncrementProcessedCount(con, billingHeaderId);
    }

    protected boolean incrementAndCheckProcessedCount(long billingHeaderId) throws Throwable {
        return altPayDAO.doIncrementAndCheckProcessedCount(con, billingHeaderId);
    }
    
    /**
     * validates the list of payments and list of refunds for the shopping
     * cart. 
     * (createHistoryFlag and updateAcctgStatusFlag are false for all vo initially)
     * 
     * 1. Add up total of payments and billed payment amount
     * 2. Add up total of refunds and billed refund amount
     * 3. If grand total of payments < grand total of refund,
     *    3.1 Add up payment total and set that total to reqAmt on the first payment vo.
     *    3.2 Set createHistoryFlag for the first payment vo.
     *    3.3 Set updateAcctgStatusFlag for all payments.
     * 4. If total of payments plus billed payment amount  >= total of refunds plus refunded amount
     *    4.1 if total of payments = total of refunds
     *        4.1.1 set updateAcctgStatusFlag to true for all payments and all refunds
     *    4.2 if total of payments > total of refunds
     *        4.2.1 set reqAmt for the first payment vo. reqAmt = payment total - refund total.
     *        4.2.2 Set createHistoryFlag for the first payment vo.
     *        4.2.3 Set updateAcctgStatusFlag for all payments.
     *        4.2.4 Set updateAcctgStatusFlag for all refunds.
     *    4.3 if total of payments < total of refunds
     *        4.3.1 set reqAmt for the first refund vo. reqAmt = refund total - payment total.
     *        4.3.2 Set createHistoryFlag for the first refund vo.
     *        4.3.3 Set updateAcctgStatusFlag for all payments.
     *        4.3.4 Set updateAcctgStatusFlag for all refunds.
     *        
     *           
     * @param plist list of AltPayBillingDetailVOBase of payments
     * @param rlist list of AltPayBillingDetailVOBase of refunds
     * @return
     * @throws Throwable
     */
    protected void validateCart(String orderGuid, List plist, List rlist) throws Throwable {
        BigDecimal pTotal = this.getListTotal(plist);        // total of payments
        BigDecimal rTotal = this.getListTotal(rlist);        // total of refunds
        BigDecimal pBilled = this.getBilledPmtAmt(orderGuid);       // billed payment amount
        BigDecimal rBilled = this.getBilledRefundAmt(orderGuid);       // billed refund amount
        BigDecimal pGrandTotal = pTotal.add(pBilled);   // total of payments + billed payment amount
        BigDecimal rGrandTotal = rTotal.add(rBilled);   // total of refunds + billed refund amount
        int pMilesTotal = this.getListTotalMiles(plist);    // total payment miles 
        int rMilesTotal = this.getListTotalMiles(rlist);    // total refund miles
        
        logger.info("payment total=" + pTotal.toString());
        logger.info("refund total=" + rTotal.toString());
        logger.info("billed payment=" + pBilled.toString());
        logger.info("billed refund=" + rBilled.toString());
        // payments grand total < refund grand total
        if(pGrandTotal.compareTo(rGrandTotal) < 0) {
            logger.info("payment grand total < refund grand total");
            // Send no page notification. Bill the payments.
            super.sendSystemMessage(con, new Exception(AltPayConstants.AP_ERROR_REFUND_TOO_LARGE+orderGuid), false);
            //Add up payment total and set that total to reqAmt on the first payment vo.
             if (plist != null && plist.size() > 0) {
                AltPayBillingDetailVOBase firstObj = (AltPayBillingDetailVOBase)plist.get(0);
                firstObj.setReqAmt(pTotal);
                
                // Set createHistoryFlag for the first payment vo.
                firstObj.setCreateHistoryFlag(true);
            }

            // Set updateAcctgStatusFlag for all payments.
            this.setAcctgStatusFlag(plist);
        } else {
            logger.info("payment grand total >= refund grand total");
            if(pTotal.compareTo(rTotal) == 0) {
                logger.info("payment total = refund total");
                this.setAcctgStatusFlag(plist);
                this.setAcctgStatusFlag(rlist);
            } else if (pTotal.compareTo(rTotal) > 0) {
                logger.info("payment total > refund total");
                //set reqAmt for the first payment vo. reqAmt = payment total - refund total.
                 if (plist != null && plist.size() > 0) {
                     AltPayBillingDetailVOBase firstObj = (AltPayBillingDetailVOBase)plist.get(0);
                     firstObj.setReqAmt(pTotal.subtract(rTotal));
                     firstObj.setReqMilesAmt(pMilesTotal - rMilesTotal); 
                     // Set createHistoryFlag for the first payment vo.
                     firstObj.setCreateHistoryFlag(true);
                 }

                //Set updateAcctgStatusFlag for all payments.
                 this.setAcctgStatusFlag(plist);
                 
                 //Set updateAcctgStatusFlag for all refunds.
                 this.setAcctgStatusFlag(rlist);
            } else {
                logger.info("payment total < refund total");
                if (rlist != null && rlist.size() > 0) {
                    AltPayBillingDetailVOBase firstObj = (AltPayBillingDetailVOBase)rlist.get(0);
                    firstObj.setReqAmt(rTotal.subtract(pTotal));
                    firstObj.setReqMilesAmt(rMilesTotal - pMilesTotal);
                    
                    // Set createHistoryFlag for the first refund vo.
                    firstObj.setCreateHistoryFlag(true);
                    logger.debug("rlist is not null");
                    logger.debug("firstObj reqAmt is:" + firstObj.getReqAmt());
                    logger.debug("firstObj reqMilesAmt is:" + firstObj.getReqMilesAmt());
                    logger.debug("firstObj createHistoryFlag:" + firstObj.isCreateHistoryFlag());
                } else {
                    logger.debug("rlist is null");
                }
                
                //Set updateAcctgStatusFlag for all payments.
                //Set updateAcctgStatusFlag for all refunds.
                 this.setAcctgStatusFlag(plist);
                 this.setAcctgStatusFlag(rlist);
            }
        }
    }
    
    /**
     * Check auth and refund validity. If a payment and total refunds can net, net them.
     * If a payment is not netted and does not have auth, skip it. Refunds are "all or none".
     * If the total of all refunds exceeds billed total, then all of them will be skipped.
     * If the total of all refunds does not exceed billed total, they will be added together
     * on the first detail object. I
     * 
     * The difference between this procedure and validateCart is:
     * 1. validateCart adds up payment total; validateCartAndAuth does not.
     * 2. auth check is not included in validateCart. Caller calls processNoAuth first.
     * @param orderGuid
     * @param plist
     * @param rlist
     * @throws Throwable
     */
    protected void validateCartAndAuth(String orderGuid, List plist, List rlist) throws Throwable {    
        boolean refundNetted = false;
        BigDecimal rTotal = this.getListTotal(rlist);            // total of refunds

        if (plist != null && plist.size() > 0) {
                for (int i = 0; i < plist.size(); i++) {
                    if (refundNetted) {
                        rTotal = new BigDecimal(0);
                    }
                    AltPayBillingDetailVOBase bdvo = (AltPayBillingDetailVOBase)plist.get(i);
                    BigDecimal pTotal = bdvo.getReqAmt();                // total of payments

                    if(pTotal.compareTo(rTotal) == 0) {
                        // Full refund.
                        bdvo.setUpdateAcctgStatusFlag(true);;
                        this.setAcctgStatusFlag(rlist);
                        refundNetted = true;
                    } else {
                        // Refunds do not add up to payment. Check auth.
                        String authNumber = bdvo.getAuthNumber();
                        if(authNumber != null && authNumber.length() > 0) {
                            // Has auth. Net refund if refund has not been netted.
                            if (!refundNetted && pTotal.compareTo(rTotal) > 0) {
                                    refundNetted = true;
                                    bdvo.setUpdateAcctgStatusFlag(true);
                                    bdvo.setCreateHistoryFlag(true);
                                    this.setAcctgStatusFlag(rlist);
                                    bdvo.setReqAmt(pTotal.subtract(rTotal));
                            } else {
                                // If refunds have been netted with other payments or payment total is less than refund total, just bill the payments.
                                bdvo.setUpdateAcctgStatusFlag(true);
                                bdvo.setCreateHistoryFlag(true);
                            }
                        
                        } else {
                            // Do not have auth. Do not set flag to be processed.
                        }
                    } // end refunds do not add up to payment
                    
                } // end for loop for payment list
            } // end payment list if
            else if (!refundNetted && (rlist != null && rlist.size() > 0)) {
                // Refund has not bee netted. Add up the refund and see if biled payment covers it.
                BigDecimal pBilled = this.getBilledPmtAmt(orderGuid);       // billed payment amount
                BigDecimal rBilled = this.getBilledRefundAmt(orderGuid);    // billed refund amount
                BigDecimal netBilled = pBilled.subtract(rBilled);
                
                if(netBilled.compareTo(rTotal) >= 0) {
                    // Net Billed total enough to cover the refunds.
                    // Add up refund and put total on the first object.
                     AltPayBillingDetailVOBase bdvo = (AltPayBillingDetailVOBase)rlist.get(0);
                     bdvo.setCreateHistoryFlag(true);
                     bdvo.setReqAmt(rTotal);
                     this.setAcctgStatusFlag(rlist);
                } else {
                    // Net Billed total not enough to cover the refunds. Do not set flag to be processed.
                }
            }
    }
    
    
    /**
     * This method removes orders without auth and fully refunded from the list.
     * @param plist
     * @param rlist
     * @param mvo
     * @throws Throwable
     */
    protected void processNoAuth(List plist, List rlist) throws Throwable {
        BigDecimal pTotal = this.getListTotal(plist);        // total of payments
        BigDecimal rTotal = this.getListTotal(rlist);        // total of refunds

        String authNumber = null;
        BigDecimal paymentAmt = null;
        BigDecimal refundAmt = null;
        BigDecimal runningRefundTotal = new BigDecimal(0);
        ArrayList fullRefundRefundList = new ArrayList(); // Keeps a list of refund that fully net with a payment.
        ArrayList fullRefundPaymentList = new ArrayList();  // Keeps a list of refund that are available for evaluation.
        AltPayBillingDetailVOBase rvo = null;
        boolean fullyRefunded = false;
        
        logger.info("payment total=" + pTotal.toString());
        logger.info("refund total=" + rTotal.toString());
        
        // Check authorization. If not authroized and fully refunded, change status to Billed for both payment and refunds
        // If not authorized and not fully refunded, skip billing if billNoAuthFlag is off.
        if(plist != null && plist.size() > 0) {
            for(int i = 0; i < plist.size(); i++) {
                fullyRefunded = false;
                AltPayBillingDetailVOBase bdvo = (AltPayBillingDetailVOBase)plist.get(i);
                authNumber = bdvo.getAuthNumber();
                paymentAmt = bdvo.getReqAmt();
                
                if(authNumber == null || authNumber.length() == 0) {
                    //The payment has no authorization. Look and see if there is a full refund.
                    runningRefundTotal = new BigDecimal(0);
                    fullRefundRefundList.clear();
                    logger.debug("Payment has no auth:" + bdvo.getPaymentId());
                    logger.debug("Payment total:" + paymentAmt.doubleValue());
                    
                    if(rlist != null && rlist.size() > 0) {
                         for (int k = 0; k < rlist.size(); k++) {
                             rvo = (AltPayBillingDetailVOBase)rlist.get(k);
                             refundAmt = rvo.getReqAmt();
                             runningRefundTotal = runningRefundTotal.add(refundAmt);
                             fullRefundRefundList.add(rvo); 
                             
                             logger.debug("Refund amout:" + refundAmt);
                             logger.debug("Refund running total:" + runningRefundTotal.doubleValue());         
                             
                             if(paymentAmt.compareTo(runningRefundTotal) == 0) {
                                 // Refunds have incrementally added up to be equal to the payment amount.
                                 fullRefundPaymentList.clear();
                                 fullRefundPaymentList.add(bdvo);
                                 handleFullRefund(fullRefundPaymentList, fullRefundRefundList);
                                 
                                 // Update payment list
                                 plist.remove(i);
                                 i--;
                                 
                                 // Create remaining refund list.
                                 for (int m = k; m >=0; m--){
                                   rlist.remove(m);
                                 }
                                 
                                 fullyRefunded = true;
                                 break;
                             }
                         } // end for loop for refund list
                     }
                     
                     if(!fullyRefunded) {
                           //Payment has no auth and not fully refunded. Remove it.
                           plist.remove(i);
                           i--;

                     }

                    } // else - order has auth. Keep in the list for continued processing.
            }
        }
    }    
    
    /**
     * The input payment and refund lists are fully refunded. Update status to Billed.
     * @param pvoList
     * @param rvoList
     * @throws Throwable
     */
    protected void handleFullRefund(List pvoList, List rvoList) throws Throwable {
        long timeStamp = Calendar.getInstance().getTimeInMillis();
        setAcctgStatusFlag(pvoList);
        setAcctgStatusFlag(rvoList);
        this.updateOrderBillStatus(pvoList, timeStamp,ARConstants.ACCTG_STATUS_BILLED);
        this.updateRefundStatus(rvoList, timeStamp,ARConstants.ACCTG_STATUS_BILLED);
    }    
    
    /**
     * For each billing detail object in the list, flag the object
     * as required to updated accounting status after processing.
     * @param l
     * @throws Throwable
     */
    protected void setAcctgStatusFlag(List l) throws Throwable {
        if (l != null) {
            Iterator i = l.iterator();
            
            while(i.hasNext()) {
                AltPayBillingDetailVOBase bvo = (AltPayBillingDetailVOBase)i.next();
                bvo.setUpdateAcctgStatusFlag(true);
            }
        }        
    }
    
    /**
     * For the given shopping cart, retrieves the amount that was billed for the payment method id.
     * @param orderGuid
     * @return
     * @throws Throwable
     */
    protected BigDecimal getBilledPmtAmt(String orderGuid) throws Throwable {
        return altPayDAO.doGetBilledPmtAmt(con, orderGuid, paymentMethodId);
    }

    /**
     * For the given shopping cart, retrieves the amount that was refunded for the payment method id.
     * @param orderGuid
     * @return
     * @throws Throwable
     */
    protected BigDecimal getBilledRefundAmt(String orderGuid) throws Throwable {
        return altPayDAO.doGetBilledRefundAmt(con, orderGuid, paymentMethodId);
    }
    
    /**
     * Retrieves total amount of billing detail object in the list.
     * @param l
     * @return
     * @throws Throwable
     */
    protected BigDecimal getListTotal(List l) throws Throwable {
        BigDecimal total = new BigDecimal("0"); 
        if (l != null) {
            Iterator i = l.iterator();
            
            while(i.hasNext()) {
                AltPayBillingDetailVOBase bvo = (AltPayBillingDetailVOBase)i.next();
                
                total = total.add(bvo.getPaymentRecAmt());
            }
        }
        return total;
    }
    
    protected int getListTotalMiles(List l) throws Throwable {
        int total = 0; 
        if (l != null) {
            Iterator i = l.iterator();
            
            while(i.hasNext()) {
                AltPayBillingDetailVOBase bvo = (AltPayBillingDetailVOBase)i.next();
                if(bvo.getReqMilesAmt() > 0) {
                    total += bvo.getReqMilesAmt();
                }
            }
        }
        return total;
    }    
    
    /**
     * Updates order bill status for the list of billing details flagged to be updated.
     * @param plist
     * @param timestamp
     * @param status
     * @throws Throwable
     */
    protected void updateOrderBillStatus(List plist, long timestamp, String status) throws Throwable {      
        if(plist != null) {
            Iterator i = plist.iterator();
            while (i.hasNext()) {
                AltPayBillingDetailVOBase bvo = (AltPayBillingDetailVOBase)i.next();
                
                if (bvo.isUpdateAcctgStatusFlag()) {
                    this.updateOrderBillStatus(bvo, timestamp, status);
                }
            }
        }
    }
    
    /**
     * updates refund status for the list of billing details flagged to be updated.
     * @param rlist
     * @param timestamp
     * @param status
     * @throws Throwable
     */
    protected void updateRefundStatus(List rlist, long timestamp, String status) throws Throwable {      
        if(rlist != null) {
            Iterator i = rlist.iterator();
            while (i.hasNext()) {
                AltPayBillingDetailVOBase bvo = (AltPayBillingDetailVOBase)i.next();
                
                if (bvo.isUpdateAcctgStatusFlag()) {
                    this.updateRefundStatus(bvo, timestamp, status);
                }
            }
        }
    }
   
    /**
     * updates accounting status for the given payment.
     * @param bvo
     * @param timestamp
     * @param status
     * @throws Throwable
     */
    protected void updateOrderBillStatus(AltPayBillingDetailVOBase bvo, long timestamp, String status) throws Throwable {      
        CommonDAO cdao = new CommonDAO(con);
        cdao.doUpdatePaymentBillStatus(Long.valueOf(bvo.getPaymentId()), timestamp, status);
    }
    
    /**
     * updates accounting status for the given billing refund
     * @param bvo
     * @param timestamp
     * @param status
     * @throws Throwable
     */
    protected void updateRefundStatus(AltPayBillingDetailVOBase bvo, long timestamp, String status) throws Throwable {      
        CommonDAO cdao = new CommonDAO(con);
        cdao.doUpdatePaymentRefundStatus(bvo.getRefundId(), timestamp, status);
    }
    
    /**
     * Make a call to United to deduct miles. 
     * @param pvo
     * @param bvo
     * @throws Throwable
     */
    public String doDeductMiles(AltPayProfileVOBase pvo, AltPayBillingDetailVOBase bvo) throws Throwable {
        try {
            // Utilize miles points service client to update miles.        
            MilesPointsRequest mvo = new MilesPointsRequest();
            milesPointsService = getMilesPointsService();
            
            mvo.setClientUserName(util.getSecureProperty("SERVICE", "SVS_CLIENT"));
            mvo.setClientPassword(util.getSecureProperty("SERVICE", "SVS_HASHCODE"));
            mvo.setMembershipNumber(bvo.getMembershipNumber());
            mvo.setMembershipType(pvo.getPaymentMethodId()); 
            mvo.setPaymentId(bvo.getPaymentId());
            mvo.setMilesPointsRequested(bvo.getReqMilesAmt());
            UpdateMilesResp resp = milesPointsService.updateMiles(mvo);
            
            logger.info("deductMiles Miles requested: " + mvo.getMilesPointsRequested());
            logger.info("payment id: " + bvo.getPaymentId() + ". confirmation=" + resp.getExternalConfirmationNumber());
            
	        MembershipVO mpvo = new MembershipVO();
	        
	        mpvo.setMembershipNumber(bvo.getMembershipNumber());
	        mpvo.setMembershipType(pvo.getPaymentMethodId()); 
	        mpvo.setPaymentId(bvo.getPaymentId());
	        mpvo.setMilesPointsRequested(bvo.getReqMilesAmt());
	        mpvo.setReturnBooleanResult(resp.getReturnBooleanResult().equals("Y")? true : false);
	        
	        // Create billing info record
	        BillingRecordUtility billingRecordUtil = new BillingRecordUtility();
	        billingRecordUtil.createBillingRecord(con, mpvo);
	        
	        //save the confirmation number to the payment record as AP_CAPTURE_TXT
	        altPayDAO.doUpdateApCaptureTxt(con, bvo.getPaymentId(), resp.getExternalConfirmationNumber());
		    
	        if(resp.getReturnBooleanResult().equals("Y")) {   
		        return AltPayConstants.MILES_APPROVED;
            } else {
            	return AltPayConstants.MILES_DECLINED;
            }
        } catch (Throwable t) {
            logger.error(t);
            throw t;
        }
    }     
    
    public MilesPointsService getMilesPointsService() throws Exception {
		String mpsURL = util.getFrpGlobalParm("SERVICE", "MILESPOINTS_SERVICE_URL");
		
		if (milesPointsService == null ||
				(milesPointsWsdlUrl != null && !milesPointsWsdlUrl.toString().equals(mpsURL))) {
			
			milesPointsWsdlUrl = new URL(mpsURL);
			MilesPointsServiceImplService milesPointsService = new MilesPointsServiceImplService(milesPointsWsdlUrl);			
			this.milesPointsService = milesPointsService.getMilesPointsServiceImplPort();
		}
		
		return milesPointsService;
    }
    
    /**
     * Create a unique session id for locking.
     * @return
     * @throws Throwable
     */
    protected String getLockSessionId() throws Throwable {
        long current =  Calendar.getInstance().getTimeInMillis();
        return String.valueOf(current);
    }
     
    /* spring variables */
    public void setAltPayDAO(AltPayDAO altPayDAO) {
        this.altPayDAO = altPayDAO;
    }

    public AltPayDAO getAltPayDAO() {
        return altPayDAO;
    }

    public void setPaymentMethodId(String paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public String getPaymentMethodId() {
        return paymentMethodId;
    }
    
    public void setConfigurationUtil(ConfigurationUtil util) {
    	this.util = util;
    }
   
    /**
     * updates status for the given refund id
     * @param refundId
     * @param status
     * @throws Throwable
     */
    protected void updateRefundStatus(String refundId, String status) throws Throwable {      
        CommonDAO cdao = new CommonDAO(con);
        cdao.doUpdatePaymentRefundStatus(refundId, status);
    }

}
