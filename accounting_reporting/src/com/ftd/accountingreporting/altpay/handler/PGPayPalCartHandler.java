package com.ftd.accountingreporting.altpay.handler;

import java.io.IOException;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.ftd.accountingreporting.altpay.dao.PGAltPayDAO;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.constant.AltPayConstants;
import com.ftd.accountingreporting.dao.CommonDAO;
import com.ftd.accountingreporting.exception.EntityLockedException;
import com.ftd.accountingreporting.util.AccountingUtil;
import com.ftd.accountingreporting.util.LockUtil;
import com.ftd.accountingreporting.util.SystemMessager;
import com.ftd.accountingreporting.vo.EODMessageVO;
import com.ftd.accountingreporting.vo.EODPaymentVO;
import com.ftd.accountingreporting.vo.PGEODMessageVO;
import com.ftd.accountingreporting.vo.PGEODRecordVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.stats.ServiceResponseTrackingUtil;
import com.ftd.osp.utilities.vo.ServiceResponseTrackingVO;
import com.ftd.pg.client.PaypalClient;
import com.ftd.pg.client.paypal.refund.request.RefundRequest;
import com.ftd.pg.client.paypal.refund.response.RefundResponse;
import com.ftd.pg.client.paypal.settlement.request.SettlementRequest;
import com.ftd.pg.client.paypal.settlement.response.SettlementResponse;

public class PGPayPalCartHandler {

	private static final String APPROVED = "approved";
	private static final String SETTLED = "Settled";
	private static final String DECLINED = "Declined";

	public PGAltPayDAO pgAltPayDAO;
	public Connection conn;

	public static final Logger logger = new Logger("com.ftd.accountingreporting.altpay.handler.PGPayPalCartHandler");
	private static final String PAYMENT_GATEWAY_PP_SETTLEMENT = "PAYMENT_GATEWAY_PP_SETTLEMENT";
	private static final String PAYMENT_GATEWAY_SVC_RETRY_LIMIT = "PAYMENT_GATEWAY_SVC_RETRY_LIMIT";
	private static final String PAYMENT_GATEWAY_SVC_DELAY_TIME_IN_RETRY = "PAYMENT_GATEWAY_SVC_DELAY_TIME_IN_RETRY";

	/*private static final String INVALID_ERR_SATUS_CODE = "400";*/
	private final static String SERVICE_ERR_SATUS_CODE = "500";
	private static final String ERROR = "Error";
	/*private static final String INVALID = "Invalid";*/
	private static final String PG_PAYMENT_TRANSACTION_TYPE = "1";// Payment,
																	// ADD Bills
	private static final String PG_REFUND_TRANSACTION_TYPE = "3";// Refunds
	private static final String PG_PP_SETTLEMENT = "PG_PP_SETTLEMENT";

	public PGPayPalCartHandler(Connection conn) {

		this.conn = conn;
		pgAltPayDAO = new PGAltPayDAO(conn);
	}

	public void process(PGEODMessageVO mvo) throws Throwable {
		logger.debug("Entering process of PGPayPalCartHandler...");

		boolean lockObtained = false;
		LockUtil lockUtil = new LockUtil(this.conn);
		String orderGuid = pgAltPayDAO.doGetOrderGuid(mvo.getMasterOrderNumber());
		String sessionId = this.getLockSessionId();

		try {
			// attempt to lock shopping cart.
			lockObtained = lockUtil.obtainLock(AltPayConstants.PP_LOCK_ENTITY_TYPE, orderGuid,
					AltPayConstants.PG_PP_LOCK_CSR_ID, sessionId, AltPayConstants.PP_LOCK_LEVEL);

			if (lockObtained) {
				// for each payment id in the message, get payment info
				Map<String, List<PGEODRecordVO>> paymentRecordMap = getPGPaymentsMap(mvo.getPaymentIdList(),
						String.valueOf(mvo.getBatchNumber()), false);
				Map<String, List<PGEODRecordVO>> refundRecordMap = getPGPaymentsMap(mvo.getRefundIdList(),
						String.valueOf(mvo.getBatchNumber()), true);

				// Consolidating pre-settlements P1-R1, P2-R2 based on
				// AutTransaction ID etc..
				consolidatePreSettlements(paymentRecordMap, refundRecordMap, mvo);

				// Payments and Same Day Refunds - Pre Settlements
				if (!paymentRecordMap.isEmpty())
					processPGRecords(paymentRecordMap, mvo); // Only Payments

				if (!refundRecordMap.isEmpty())
					processPGRecords(refundRecordMap, mvo); // Only Refunds

			} else {
			
				StringBuffer sb = new StringBuffer(AltPayConstants.PP_ERROR_LOCK_FAILURE);
				sb.append("::");
				sb.append(mvo.getMasterOrderNumber());
				logger.error(new Exception(sb.toString()));
				sendSystemMessage(conn, new Exception(sb.toString()), false);
				
			}
		} catch (EntityLockedException e){
			
			StringBuffer sb = new StringBuffer(AltPayConstants.PP_ERROR_LOCK_FAILURE);
			sb.append("::");
			sb.append(mvo.getMasterOrderNumber());
			logger.error(new Exception(sb.toString()));
			sendSystemMessage(this.conn, new Exception(AltPayConstants.PP_ERROR_LOCK_FAILURE), false);
		}
		catch (Throwable t){
			try {
				logger.error(t);
				sendSystemMessage(this.conn, t, false);
			} catch (Throwable tt) {
				logger.error(tt);
			}
		}
		finally {
			
			// increment processed count regardless of result
			boolean allProcessedFlag = checkAndIncrementProcessedCount(mvo.getBatchNumber());
			if (allProcessedFlag){
				performPostPGEODTasks(mvo, this.conn);
			 }

			if (lockObtained) {
				try {
					lockUtil.releaseLock(AltPayConstants.PP_LOCK_ENTITY_TYPE, orderGuid,
							AltPayConstants.PG_PP_LOCK_CSR_ID, sessionId);
				} catch (Exception e) {
					logger.error(e);
					sendSystemMessage(this.conn, e, false);
				}
			}
		}

		logger.debug("Exiting process of PGPayPalCartHandler...");
	}
	

	/**
	 * @param mvo
	 * @param conn
	 * @throws Throwable
	 */
	private void performPostPGEODTasks(PGEODMessageVO mvo, Connection conn) throws Throwable {

		updateHeaderIndicator(mvo.getBatchNumber(), ARConstants.PAYPAL);

		long failedRecCounter = 0;
		List<PGEODRecordVO> billingDetailsList = getErroredBillingRecords(mvo.getBatchNumber(), ARConstants.PAYPAL);
		if (billingDetailsList != null)
			failedRecCounter = billingDetailsList.size();

		if (failedRecCounter > 0) {
			StringBuffer errorMessage = new StringBuffer(
					"PG EOD Settlements/Refunds were failed.. Please Reprocess Billing Details records using Scheduler Screen.");
			errorMessage.append("\n");
			errorMessage.append("No. of Failed Records:: " + failedRecCounter + " for the Batch Number::"
					+ mvo.getBatchNumber());
			errorMessage.append(" of Payment Type ::" + ARConstants.PAYPAL);
			errorMessage.append("\n\n");
			errorMessage.append("PayLoad is :: "+mvo.getBatchNumber()+"|"+ARConstants.PAYPAL);
			errorMessage.append("\n");
			errorMessage.append("***Note: Please wait until PG-EOD is completed before you proceed with Re-Process.***");
			errorMessage.append("\n");
			errorMessage.append("This is because errored records may retry.");
			logger.debug(errorMessage.toString()); // For Logging
			SystemMessager.send(errorMessage.toString(), null, ARConstants.SM_PAGE_SOURCE, SystemMessager.LEVEL_PRODUCTION,
			AltPayConstants.AP_PAGE_ERROR_TYPE, ARConstants.SM_PAGE_SUBJECT, conn);
			
		}
	}

	/**
	 * @param batchNumber
	 * @param paymentType
	 * @return
	 * @throws IOException
	 * @throws SQLException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 */
	private List<PGEODRecordVO> getErroredBillingRecords(long batchNumber, String paymentType) throws IOException,
			SQLException, ParserConfigurationException, SAXException {
		return pgAltPayDAO.getFailedPPBillingDetailsList(batchNumber, paymentType);
	}

	/**
	 * @param batchNumber
	 * @param paymentType
	 * @throws Throwable
	 */
	private void updateHeaderIndicator(long batchNumber, String paymentType) throws Throwable {

		pgAltPayDAO.updateHeaderIndicator(batchNumber, paymentType);
	}

	/**
	 * @param batchNumber
	 * @return
	 * @throws Throwable
	 */
	private boolean checkAndIncrementProcessedCount(long batchNumber) throws Exception {
		return pgAltPayDAO.checkAndIncrementProcessedCount(batchNumber);
	}

	/**
	 * @param paymentRecordMap
	 * @param refundRecordMap
	 * @param mvo
	 * @throws Exception
	 */
	private void consolidatePreSettlements(Map<String, List<PGEODRecordVO>> paymentRecordMap,
			Map<String, List<PGEODRecordVO>> refundRecordMap, PGEODMessageVO mvo) throws Exception {
		int c = 0;
		BigDecimal bgr = new BigDecimal(0);
		BigDecimal bgp = new BigDecimal(0);
		PGEODRecordVO paymentRecordVO = null;
		logger.debug("Before Consolidations Maps " + paymentRecordMap + "\n::" + refundRecordMap);
		Iterator<Entry<String, List<PGEODRecordVO>>> iterator = paymentRecordMap.entrySet().iterator();

		Map<String, List<PGEODRecordVO>> tempPaymentMap = new HashMap<String, List<PGEODRecordVO>>();// Temp
																										// Map
		while (iterator.hasNext()) {

			Entry<String, List<PGEODRecordVO>> entry = iterator.next();
			String authID = entry.getKey();

			List<PGEODRecordVO> prvoList = entry.getValue();
			List<PGEODRecordVO> rrVOList = refundRecordMap.get(authID);

			bgp = getListTotal(prvoList);
			bgr = getListTotal(rrVOList);

			paymentRecordVO = prvoList.get(0); // Since Auth ID is different for
												// each Payment

			c = bgp.compareTo(bgr);
			logger.debug("value of c is: " + c);
			if (c > 0) {

				// payment total is greater than refund total
				if (bgr.doubleValue() > 0)
					paymentRecordVO.setOrderAmount(bgp.subtract(bgr).doubleValue());

				List<PGEODRecordVO> list = new ArrayList<PGEODRecordVO>();
				list.add(paymentRecordVO);
				tempPaymentMap.put(authID, list);
				refundRecordMap.remove(authID);// Remove All refunds since those
												// were consolidated.

			} else if (c == 0) {

				// payment total is same as refund total
				billPaymentList(prvoList, mvo.getBatchTime());
				paymentRecordMap.remove(authID);

				// billRefundList(rrVOList,mvo.getBatchTime()); Commented Since
				// marking as Billed at the end
				refundRecordMap.remove(authID);

			} else {
				// Refund Total is greater than Payment Total
				refundRecordMap.remove(authID);
				logger.error("Payment is less than the Refund so ignoring refund & Continue with Payment ID List"
						+ prvoList + "Refund ID List ::" + rrVOList);
			}
			// Bill all refunds.
			billRefundList(rrVOList, mvo.getBatchTime());
		}
		paymentRecordMap.putAll(tempPaymentMap);
		logger.debug("After Consolidations Maps " + paymentRecordMap + "\n::" + refundRecordMap);
	}

	/**
	 * Mark all payments in the list Billed.
	* @param pList
	 * @param eodBatchTime
	 * @throws Exception
	 */
	private void billPaymentList(List<PGEODRecordVO> pList, long eodBatchTime) throws Exception {
		logger.debug("Entering billPaymentList...");
		if (pList != null) {
			logger.debug("pList size is: " + pList.size());
			for (PGEODRecordVO recordVO : pList) {
				pgAltPayDAO.doUpdatePaymentBillStatus(recordVO.getPaymentId(), eodBatchTime,
						ARConstants.ACCTG_STATUS_BILLED);
			}
		}
		logger.debug("Exiting billPaymentList...");
	}

	/**
	 * Mark all Refunds in the list Billed.
	 * @param rList
	 * @param eodBatchTime
	 * @throws Exception
	 */
	private void billRefundList(List<PGEODRecordVO> rList, long eodBatchTime) throws Exception {
		logger.debug("Entering billRefundList...");
		if (rList != null) {
			logger.debug("rList size is: " + rList.size());
			for (PGEODRecordVO recordVO : rList) {
				pgAltPayDAO.doUpdateRefundBillStatus(recordVO.getRefundId(), eodBatchTime,
						ARConstants.ACCTG_STATUS_BILLED);
			}
		}
		logger.debug("Exiting billRefundList...");
	}

	/**
	 * @param recordList
	 * @return
	 * @throws Exception
	 */
	private BigDecimal getListTotal(List<PGEODRecordVO> recordList) throws Exception {
		logger.debug("Entering getListTotal...");
		BigDecimal total = new BigDecimal(0);
		double amount = 0;

		if (recordList != null) {
			for (PGEODRecordVO recordVO : recordList) {
				amount = recordVO.getOrderAmount();
				logger.debug("amount is: " + amount);
				total = total.add(new BigDecimal(String.valueOf(amount)));
			}
		}
		logger.debug("returning total: " + total.doubleValue());
		logger.debug("Exiting getListTotal...");

		return total;
	}

	/**
	 * @param payments
	 * @param batchNumber
	 * @param isRefundId
	 * @return
	 * @throws Throwable
	 */
	private Map<String, List<PGEODRecordVO>> getPGPaymentsMap(List payments, String batchNumber, boolean isRefundId)
			throws Exception {

		if (logger.isDebugEnabled()) {
			logger.debug("Entering createPGPamentsMap. Size=" + (payments == null ? 0 : payments.size()));
		}
		Map<String, List<PGEODRecordVO>> paymentMap = new HashMap<String, List<PGEODRecordVO>>();
		List<PGEODRecordVO> paymentRecordList;

		paymentRecordList = loadPaymentInfoList(payments, batchNumber, isRefundId);

		List<PGEODRecordVO> paymentAuthList = null;
		List<String> authList = new ArrayList<String>();

		if (paymentRecordList != null) {
			logger.debug("Loaded payment list size: " + paymentRecordList.size());

			for (PGEODRecordVO pgEODRecordVO : paymentRecordList) {
				{

					String authID = pgEODRecordVO.getAuthTranId();
					paymentAuthList = paymentMap.get(authID);

					// if AuthID number does not exist in map, add it.
					if (paymentAuthList == null) {
						logger.debug("Create new payment entry list for Auth ID");
						paymentAuthList = new ArrayList<PGEODRecordVO>();
						authList.add(authID);
					}
					paymentAuthList.add(pgEODRecordVO);
					paymentMap.put(authID, paymentAuthList);
				}
			}
		}
		if (logger.isDebugEnabled()) {
			logger.debug("Loaded Map size:: " + paymentMap.size());
			logger.debug("Exiting getPGPaymentsMap.");
		}

		return paymentMap;

	}

	/**
	 * @param recordMap
	 * @param mvo
	 * @throws Throwable
	 */
	private void processPGRecords(Map<String, List<PGEODRecordVO>> recordMap, PGEODMessageVO mvo) throws Exception {

		// Consolidating Based on Auth-Code for Pre-Settlements Payments and
		// Refunds
		long billingDetailId = 0;
		String billingDetailIDStr = null;

		for (String paymentAuthID : recordMap.keySet()) {

			logger.debug("paymentAuthID ::" + paymentAuthID);
			List<PGEODRecordVO> paymentRecordVOList = recordMap.get(paymentAuthID);

			for (PGEODRecordVO paymentRecordVO : paymentRecordVOList) {

				// create billing detail
				billingDetailIDStr = pgAltPayDAO.createPGBillingDetail(paymentRecordVO, mvo.getBatchNumber());

				// Payment
				if (paymentRecordVO.getTransactionType().equals(PG_PAYMENT_TRANSACTION_TYPE))
					pgAltPayDAO.doUpdatePaymentBillStatus(paymentRecordVO.getPaymentId(), mvo.getBatchTime(),
							ARConstants.ACCTG_STATUS_BILLED); // Billed for

				// Refund
				if (paymentRecordVO.getTransactionType().equals(PG_REFUND_TRANSACTION_TYPE))
					pgAltPayDAO.doUpdateRefundBillStatus(paymentRecordVO.getRefundId(), mvo.getBatchTime(),
							ARConstants.ACCTG_STATUS_BILLED); // Billed for

				billingDetailId = Long.parseLong(billingDetailIDStr);

				paymentRecordVO.setBillingDetailId(billingDetailId);

				performSettlements(paymentRecordVO, mvo);

			}
		}
	}

	/**
	 * Create a unique session id for locking.
	 * 
	 * @return
	 * @throws Throwable
	 */
	protected String getLockSessionId() throws Throwable {
		long current = Calendar.getInstance().getTimeInMillis();
		return String.valueOf(current);
	}

	/**
	 * Retrieve payment information for the payment ids in the list.
	 * 
	 * @param list
	 * @param paymentCode
	 * @return
	 * @throws Throwable
	 */

	private List loadPaymentInfoList(List list, String batchNumber, boolean isRefund) throws Exception {
		logger.debug("entering loadPaymentInfoList");
		List infoList = null;
		if (list != null) {
			Iterator i = list.iterator();
			infoList = new ArrayList();
			while (i.hasNext()) {
				EODPaymentVO pvo = (EODPaymentVO) i.next();
				PGEODRecordVO ppvo = (PGEODRecordVO) this.loadPaymentInfo(pvo, isRefund);
				ppvo.setBillingHeaderId(Long.parseLong(batchNumber));
				infoList.add(ppvo);
			}
		}
		logger.debug("exiting loadPaymentInfoList");
		return infoList;
	}

	/**
	 * Retreives payment information specific to PayPal to prepare for the call.
	 * 
	 * @param pvo
	 * @return
	 * @throws Throwable
	 */

	private PGEODRecordVO loadPaymentInfo(EODPaymentVO pvo, boolean isRefund) throws Exception {

		logger.debug("entering loadPaymentInfo");
		
		BigDecimal amount = null;
		String authransactionId = null;
		long refundId;
		String authCode = null;
		String settlmentTransactionId = null;
		String refundTransactionId = null;
		String paymentType = null;

		PGEODRecordVO pgEODVO = new PGEODRecordVO();
		
		CachedResultSet rs = this.pgAltPayDAO.doGetPGApPaymentInfo(pvo);
		pgEODVO.setMasterOrderNumber(pvo.getMasterOrderNumber());
		pgEODVO.setPaymentId(Long.parseLong(pvo.getPaymentId()));

		if (rs != null && rs.next()) {

			if (isRefund) {
				pgEODVO.setTransactionType(ARConstants.TRANSACTION_TYPE_REFUND); // 3
				refundId = rs.getLong("refund_id");
				pgEODVO.setRefundId(refundId);

				if (rs.getString("debit_amount") == null) {
					throw new Exception("Amount is null for refund id: " + refundId);
				}
				amount = new BigDecimal(rs.getString("debit_amount"));
				pgEODVO.setRequestToken(rs.getString("request_token"));
				pgEODVO.setBillType(ARConstants.CONST_REFUND);

			} else {
				pgEODVO.setTransactionType(ARConstants.TRANSACTION_TYPE_PURCHASE); // 1
				if (rs.getString("credit_amount") == null) {
					throw new Exception("Amount is null for payment id: " + pgEODVO.getPaymentId());
				}
				amount = new BigDecimal(rs.getString("credit_amount"));
				if (rs.getString("additional_bill_id") != null && !rs.getString("additional_bill_id").isEmpty())
					pgEODVO.setBillType(ARConstants.CONST_ADD_BILL);
				else
					pgEODVO.setBillType(ARConstants.CONST_BILL); // Payment
			}

			authransactionId = rs.getString("AUTHORIZATION_TRANSACTION_ID");
			settlmentTransactionId = rs.getString("settlement_transaction_id");
			refundTransactionId = rs.getString("REFUND_TRANSACTION_ID");
			paymentType = rs.getString("payment_type");

			authCode = rs.getString("auth_number");
			pgEODVO.setOrderAmount(amount.doubleValue()); // amount in db
			pgEODVO.setVoiceAuthFlag(rs.getString("is_voice_auth"));
			pgEODVO.setAuthTranId(authransactionId);
			pgEODVO.setAuthCode(authCode);
			pgEODVO.setSettlmentTransactionId(settlmentTransactionId);
			pgEODVO.setRefundTransactionId(refundTransactionId);
			pgEODVO.setPaymentType(paymentType);
			pgEODVO.setMerchantRefId(rs.getString("merchant_ref_id"));
			pgEODVO.setRequestToken(rs.getString("request_id"));

		}
		logger.debug("exiting loadPaymentInfo");
		return pgEODVO;
	}

	/**
	 * Sends a system message. The paging system looks for source of 'End of
	 * Day'. For any messages with that souce, it will send a nopage email
	 * rather than paging or include in the no page summary.
	 * 
	 * @param con
	 *            DB connection
	 * @param t
	 *            Throwable object
	 * @param whether
	 *            to page or nopage
	 * @throws Throwable
	 */
	protected void sendSystemMessage(Connection con, Throwable t, boolean page) throws Throwable {
		try {
			logger.debug("Sending system message...");
			String source = "";
			String logMessage = t.getMessage();
			if (page) {
				source = ARConstants.SM_PAGE_SOURCE;
			} else {
				source = ARConstants.SM_NOPAGE_SOURCE;
			}
			SystemMessager.send(logMessage, t, source, SystemMessager.LEVEL_PRODUCTION,
					AltPayConstants.AP_PAGE_ERROR_TYPE, AltPayConstants.PG_AP_NOPAGE_SUBJECT, con);

		} catch (Exception ex) {
			logger.error(ex);
		} finally {
			if (logger.isDebugEnabled()) {
				logger.debug("Exiting sendSystemMessage");
			}
		}
	}

	/**
	 * @param pgEODRecVO
	 * @param mvo
	 * @throws Throwable
	 */
	private void performSettlements(PGEODRecordVO pgEODRecVO, PGEODMessageVO mvo) throws Exception {

		String pgSVCTimeOut = ConfigurationUtil.getInstance().getFrpGlobalParm(ARConstants.SERVICE,
				ARConstants.PAYMENT_GATEWAY_SVC_TIMEOUT);
		PaypalClient pc = new PaypalClient(pgSVCTimeOut);

		String settlementUrl = null;
		SettlementRequest settlmentRequest = new SettlementRequest();
		RefundRequest refundRequest = new RefundRequest();
		SettlementResponse settlmentResponse = new  SettlementResponse();
		RefundResponse refundResponse = new RefundResponse();

		try {

			settlementUrl = getSettlementURL(pgEODRecVO.getTransactionType());
			long startTime = new java.util.Date().getTime();
			mvo.setBillingDetailId(pgEODRecVO.getBillingDetailId());

			if (ARConstants.TRANSACTION_TYPE_PURCHASE.equals(pgEODRecVO.getTransactionType())) {
				settlmentRequest.setMerchantReferenceId(pgEODRecVO.getMerchantRefId());
				settlmentRequest.setAmount(Double.toString(pgEODRecVO.getOrderAmount()));
				settlmentRequest.setPayPalAuthorizationTransactionId(pgEODRecVO.getAuthTranId());
				settlmentRequest.setPaymentType(AltPayConstants.PAYPAL_PAYMENT_TYPE);

				if (mvo.getRetryCounter() > 0)
					settlmentRequest.setIsRetry(true);
				else
					settlmentRequest.setIsRetry(false);
				
				try {

					settlmentResponse = (SettlementResponse) pc.settlePaypalTrans(settlmentRequest, settlementUrl);

					logger.info("Payment Gateway Settlement Response for:: " + pgEODRecVO.getMerchantRefId() + "::"
							+ settlmentResponse);
				} catch (Exception e) {
					logger.error("Exception while Payment Gateway Settlement/Refund And it will re-processed:: re-process count::"
							+ mvo.getRetryCounter());
					logger.error(e.getMessage());
					// Error with Some invalid data or Exception
					settlmentResponse.setStatus(ERROR);
					settlmentResponse.setReasonCode(SERVICE_ERR_SATUS_CODE);
				}
				
				// Massage Settlement Msg
				if (checkAndProcessSettlementResponse(pgEODRecVO, settlmentResponse, mvo.getBatchTime())) {
					reProcessSettlement(pgEODRecVO, mvo);
				}
			}
			if (ARConstants.TRANSACTION_TYPE_REFUND.equals(pgEODRecVO.getTransactionType())) {
				refundRequest.setMerchantReferenceId(pgEODRecVO.getMerchantRefId());
				refundRequest.setAmount(Double.toString(pgEODRecVO.getOrderAmount()));
				refundRequest.setSettlementTransactionId((pgEODRecVO.getSettlmentTransactionId()));
				refundRequest.setPaymentType(AltPayConstants.PAYPAL_PAYMENT_TYPE);
				
				try{
				refundResponse = (RefundResponse) pc.refundPaypalTrans(refundRequest, settlementUrl);
				logger.info("PG PayPal Refund Response " + refundResponse);
				}
				catch (Exception e) {
					logger.error("Exception while Payment Gateway Settlement/Refund And it will re-processed:: re-process count::"+mvo.getRetryCounter());
					logger.error(e.getMessage());
					
					// Errored with Some invalid data
					refundResponse.setStatus(ERROR);
					settlmentResponse.setReasonCode(SERVICE_ERR_SATUS_CODE);
				}
				// Massage Settlement Msg
				if (checkAndProcessSettlementResponse(pgEODRecVO, populateSettlementResponse(refundResponse), mvo.getBatchTime())) {
					reProcessSettlement(pgEODRecVO, mvo);
				}

			}

			logServiceResponseTracking(String.valueOf(pgEODRecVO.getBillingDetailId()), PG_PP_SETTLEMENT, startTime);

		} catch (Exception e) {
			logger.error("Exception in Payment Gateway Settlement/Refund "+e.getMessage());
			throw e;
		}
	}

	/**
	 * @param refundResponse
	 * @return
	 */
	private SettlementResponse populateSettlementResponse(RefundResponse refundResponse) {

		SettlementResponse settlementResponse = new SettlementResponse();
		
		settlementResponse.setStatus(refundResponse.getStatus());
		settlementResponse.setSettlementDate(refundResponse.getRefundDate());
		settlementResponse.setSettlementTransactionId(refundResponse.getRefundTransactionId());
		settlementResponse.setMessage(refundResponse.getMessage());
		settlementResponse.setReasonCode(refundResponse.getReasonCode());
		
		return settlementResponse;
	}
	
	public void reProcessSettlement(PGEODRecordVO vo, PGEODMessageVO mvo) throws Exception {

		long timeToLive = 0;
		List list = new ArrayList();
		int count = 5;
		int delay = 0;

		// logger.info("PG PayPal EOD Re-Processing Starting with Count ::"+mvo.getRetryCounter()
		// );
		try {
			count = Integer.parseInt(ConfigurationUtil.getInstance().getFrpGlobalParm(ARConstants.SERVICE,
					PAYMENT_GATEWAY_SVC_RETRY_LIMIT));
			delay = Integer.parseInt(ConfigurationUtil.getInstance().getFrpGlobalParm(ARConstants.SERVICE,
					PAYMENT_GATEWAY_SVC_DELAY_TIME_IN_RETRY));
		} catch (NumberFormatException e) {
			logger.error("Exception while Parsing the Global Parameter.Continuing with Defaults");
		}

		if (mvo.getRetryCounter() <= count) {
			if (PG_PAYMENT_TRANSACTION_TYPE.equals(vo.getTransactionType())) // Payment
			{
				List pList = new ArrayList();
				EODPaymentVO eodPaymentVO = new EODPaymentVO(mvo.getMasterOrderNumber(), String.valueOf(vo
						.getPaymentId()), mvo.getPaymentMethodId());
				pList.add(eodPaymentVO);
				mvo.setPaymentIdList(pList);
			}
			if (PG_REFUND_TRANSACTION_TYPE.equals(vo.getTransactionType())) // Refund
			{
				List rList = new ArrayList();
				EODPaymentVO eodPaymentVO = new EODPaymentVO(mvo.getMasterOrderNumber(), String.valueOf(vo
						.getRefundId()), mvo.getPaymentMethodId());
				rList.add(eodPaymentVO);
				mvo.setRefundIdList(rList);
			}
			mvo.setBillingDetailId(vo.getBillingDetailId());
			mvo.setRetryCounter(mvo.getRetryCounter() + 1);
			list.add((mvo.toMessageXML()));
			logger.info("PG PayPal EOD Billing ID ::" + mvo.getBillingDetailId() + "::Reprocessing Number::"
					+ mvo.getRetryCounter());
			AccountingUtil.dispatchJMSMessageList(AltPayConstants.PG_ALTPAY_CART, list, timeToLive, delay);
		}

	}

	/**
	 * Construct a message to be sent to the cart processor.
	 * 
	 * @param paymentList
	 * @param refundList
	 * @return
	 * @throws java.lang.Exception
	 */
	protected String composePIDMessage(List pids, String masterOrderNumber, EODMessageVO eodMessageVO) throws Throwable {
		String message = null;

		if ((pids != null && pids.size() > 0)) {
			eodMessageVO.setMasterOrderNumber(masterOrderNumber);
			eodMessageVO.setPaymentIdList(pids);
			message = eodMessageVO.toMessageXML();
		}
		return message;
	}

	/**
	 * @param pgEODRecordVO
	 * @param settlmentResponse
	 * @param batchTime
	 * @return
	 * @throws Exception
	 */
	private boolean checkAndProcessSettlementResponse(PGEODRecordVO pgEODRecordVO,
			SettlementResponse settlmentResponse, long batchTime) throws Exception {
		boolean reProcess = false;
		if (settlmentResponse != null) {
			if (settlmentResponse.getStatus() != null) {
				logger.info("settlmentResponse.getStatus() " + settlmentResponse.getStatus());
				if (settlmentResponse.getStatus().equalsIgnoreCase(APPROVED)) {

					settlmentResponse.setStatus(SETTLED);

					settlmentResponse.setSettlementDate(massageSettlementDate(settlmentResponse.getSettlementDate()));

					// updates Settlement Response in Billing_Detail_pg_pp and
					// payment tables for Settlement ID and Status
					updateSettlmentResponse(settlmentResponse, pgEODRecordVO);

					// Updates Order Bills Status as Billed.
					// Will only mark it as 'Billed' as per existing behaviour.
					updatePaymentBillStatus(batchTime, pgEODRecordVO.getPaymentId(), ARConstants.ACCTG_STATUS_SETTLED);

				} else if (settlmentResponse.getStatus().equalsIgnoreCase(DECLINED)) {

					//settlmentResponse.setStatus(DECLINED);

					settlmentResponse.setSettlementDate(massageSettlementDate(settlmentResponse.getSettlementDate()));

					// updates Settlement Response in Billing_Detail_pg_PP and
					// payment tables
					updateSettlmentResponse(settlmentResponse, pgEODRecordVO);
					reProcess = false;

				}else if (settlmentResponse.getStatus().equalsIgnoreCase(ERROR)) {

					//settlmentResponse.setStatus(ERROR);
					settlmentResponse.setReasonCode(SERVICE_ERR_SATUS_CODE);// Errored with Some invalid data
					// updates Settlement Response in Billing_Detail_pg_cc and
					// payment tables
					updateSettlmentResponse(settlmentResponse, pgEODRecordVO);
					reProcess = true;
				}

			}
		} else {
			StringBuffer sb = new StringBuffer("PG EOD Settlment/Refund Process failed for " + "Payment ID:: ")
					.append(pgEODRecordVO.getPaymentId())
					.append(" Billing Deatil ID::" + pgEODRecordVO.getBillingDetailId())
					.append("for the Batch ID:: " + pgEODRecordVO.getBillingHeaderId()
							+ "Don't worry It will be retried....");
			logger.error(sb.toString());
		}

		return reProcess;
	}

	private void updatePaymentBillStatus(long batchTime, long paymentId, String acctgStatusSettled) throws Exception {

		CommonDAO cdao = new CommonDAO(this.conn);
		cdao.doUpdatePaymentBillStatus(paymentId, batchTime, acctgStatusSettled);

	}

	/**
	 * @param errorMessages
	 * @return
	 *//*
	private String getPreparedErrorMessagefromMap(List<Errors> errorMessages) {

		StringBuffer sb = new StringBuffer();
		for (Errors error : errorMessages) {
			sb.append(error.getMessage());
		}
		return sb.toString();
	}*/

	/** This function parses two different Date formats 
	 * @param jsonDate
	 * @return
	 * @throws Exception
	 */
	private Date getSQLDate(String jsonDate) throws Exception {
		
		//String jsonDate ="2018-09-05T01:38:18.45";
		//String jsonDate ="2018-09-05T02:01:18.0954874-07:00";
		
		SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSSZ");
		
		java.sql.Date  sqlDate = null;
		StringBuffer jsonDateBuffer = new StringBuffer(jsonDate);
		jsonDateBuffer.replace(jsonDate.lastIndexOf(":"), jsonDate.lastIndexOf(":") + 1, "");
		
		try {
			sqlDate = new java.sql.Date(DATE_FORMAT.parse(jsonDateBuffer.toString()).getTime());
		} catch (ParseException e) {
			logger.error("Excetpion while parsing the Date with Format:: "+ DATE_FORMAT + ":: " + e.getMessage());
			DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SS");
			logger.info("So going to parse with other Date Format:: "+ DATE_FORMAT);
			try {
				sqlDate = new java.sql.Date(DATE_FORMAT.parse(jsonDate.toString()).getTime());
			} catch (ParseException pe) {
				logger.error("Excetpion while parsing with Other Date Format:: "
						+ DATE_FORMAT + ":: " + e.getMessage());
				sqlDate = null;
				throw pe;
			}
		}
		return sqlDate;
				
	}

	/**
	 * @param requestId
	 * @param serviceMethod
	 * @param startTime
	 */
	public static void logServiceResponseTracking(String requestId, String serviceMethod, long startTime) {
		Connection connection = null;
		try {

			long responseTime = System.currentTimeMillis() - startTime;
			logger.debug("logServiceResponseTracking " + responseTime);
			ServiceResponseTrackingVO srtVO = new ServiceResponseTrackingVO();
			srtVO.setServiceName(PAYMENT_GATEWAY_PP_SETTLEMENT);
			srtVO.setServiceMethod(serviceMethod);
			srtVO.setTransactionId(requestId);
			srtVO.setResponseTime(responseTime);
			srtVO.setCreatedOn(new java.util.Date());

			connection = DataSourceUtil.getInstance().getConnection("ORDER SCRUB");
			ServiceResponseTrackingUtil srtUtil = new ServiceResponseTrackingUtil();
			srtUtil.insertWithRequest(connection, srtVO);
		} catch (Exception e) {
			logger.error("Exception occured while persisting PG Settlements Service response time lines.");
		}

	}

	private Date massageSettlementDate(Object settlementDate) throws Exception {
		Date massagedDate = new java.sql.Date(new java.util.Date().getTime());
		if (settlementDate != null)
			massagedDate = getSQLDate((String) settlementDate);

		return massagedDate;

	}

	private void updateSettlmentResponse(SettlementResponse settlementResponse, PGEODRecordVO pgEODRecordVO)
			throws Exception {

		pgAltPayDAO.doUpdatePGSettlementResponse(settlementResponse, pgEODRecordVO);

	}

	/**
	 * @param transactionType
	 * @return
	 * @throws Exception
	 */
	private String getSettlementURL(String transactionType) throws Exception {

		StringBuffer settlementWsUrl = new StringBuffer(ConfigurationUtil.getInstance().getFrpGlobalParm(
				ARConstants.SERVICE, ARConstants.PAYMENT_GATEWAY_SVC_URL));
		if (ARConstants.TRANSACTION_TYPE_PURCHASE.equals(transactionType)) {
			settlementWsUrl.append("paypal/settle");
		}
		if (ARConstants.TRANSACTION_TYPE_REFUND.equals(transactionType)) {
			settlementWsUrl.append("paypal/refund");
		}
		logger.debug("Payment Gateway SettlementWs Url::" + settlementWsUrl);
		return settlementWsUrl.toString();

	}

	/**
	 * @param eodMessageVO
	 * @throws Throwable
	 */
	public void reProcessPG(PGEODMessageVO eodMessageVO) throws Throwable {

		PGEODRecordVO pgEodRecordVO = getBillingDetailRecord(eodMessageVO.getBillingDetailId(),
				eodMessageVO.getBatchNumber());
		performSettlements(pgEodRecordVO, eodMessageVO);

	}

	/**
	 * @param billingId
	 * @param headerId
	 * @return
	 * @throws Throwable
	 */
	private PGEODRecordVO getBillingDetailRecord(long billingId, long headerId) throws Throwable {

		CachedResultSet rs = pgAltPayDAO.doGetBillingPPDetailsPG(billingId, headerId);
		PGEODRecordVO pgEodRecordVO = new PGEODRecordVO();
		if (rs.next()) {
			pgEodRecordVO.setBillingDetailId(rs.getLong("BILLING_DETAIL_ID"));
			pgEodRecordVO.setBillingHeaderId(rs.getLong("BILLING_HEADER_ID"));
			pgEodRecordVO.setOrderAmount(rs.getDouble("ORDER_AMOUNT"));
			pgEodRecordVO.setMasterOrderNumber(rs.getString("ORDER_NUMBER"));
			pgEodRecordVO.setTransactionType(rs.getString("TRANSACTION_TYPE"));
			pgEodRecordVO.setAuthTranId(rs.getString("AUTH_TRANSACTION_ID"));
			pgEodRecordVO.setSettlmentTransactionId(rs.getString("SETTLEMENT_TRANSACTION_ID"));
			pgEodRecordVO.setRequestToken(rs.getString("REQUEST_ID"));
			pgEodRecordVO.setMerchantRefId(rs.getString("merchant_ref_id"));
			pgEodRecordVO.setPaymentId(rs.getLong("payment_id"));
			pgEodRecordVO.setPaymentType(rs.getString("payment_type"));
		}

		return pgEodRecordVO;
	}

}
