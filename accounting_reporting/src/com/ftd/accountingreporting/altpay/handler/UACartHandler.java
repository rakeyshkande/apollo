package com.ftd.accountingreporting.altpay.handler;

import com.ftd.accountingreporting.altpay.vo.AltPayBillingDetailVOBase;
import com.ftd.accountingreporting.altpay.vo.UABillingDetailVO;
import com.ftd.accountingreporting.altpay.vo.UAProfileVO;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.constant.AltPayConstants;
import com.ftd.accountingreporting.exception.EntityLockedException;
import com.ftd.accountingreporting.util.AccountingUtil;
import com.ftd.accountingreporting.util.LockUtil;
import com.ftd.accountingreporting.vo.EODMessageVO;
import com.ftd.accountingreporting.vo.EODPaymentVO;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

import java.math.BigDecimal;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 * This class is responsible for processing the UA payments and refunds
 * the the shopping cart. Payments are settled through Web Service.
 * Refunds are settled by creating a file. If the netted total is positive,
 * it calls the web service to settle charges; if the netted total is
 * negative, the total is added to the refund file. 
 */
public class UACartHandler extends AltPayCartHandlerBase {

    Logger logger = new Logger("com.ftd.accountingreporting.altpay.handler.UACartHandler");
    UAProfileVO uaProfileVO;
    
    public UACartHandler() {}

    /**
     * This is the main method. It
     * 1. Attempts to lock the shopping cart. If a lock cannot be obtained, it sends a nopage notification.
     * 2. Loads payment information required to process.
     * 3. Validates the payments and refunds
     * 4. Process the payments and refunds.
     * @param conn
     * @param mvo
     * @throws Throwable
     */     
    public void process(Connection conn, EODMessageVO mvo) throws Throwable {
        logger.info("Entering process of UACartHandler...");
        uaProfileVO.createConfigsMap();

        super.process(conn, mvo); 
        
        boolean lockObtained = false;
        LockUtil lockUtil = new LockUtil(conn);
        String orderGuid = altPayDAO.doGetOrderGuid(conn, mvo.getMasterOrderNumber());
		logger.debug("orderGuid: " + orderGuid);
        String sessionId = this.getLockSessionId();
        List pvoList = null;
        List rvoList = null;

        try{
             // attempt to lock shopping cart.
             lockObtained = lockUtil.obtainLock(AltPayConstants.UA_LOCK_ENTITY_TYPE,
                                                orderGuid,
                                                AltPayConstants.UA_LOCK_CSR_ID,
                                                sessionId,
                                                AltPayConstants.UA_LOCK_LEVEL);
                                                
             if(lockObtained) {
                 // for each payment id in the message, get payment info
            	 
            	 logger.debug("mvo.getBatchNumber(): " + mvo.getBatchNumber());
                 pvoList = loadPaymentInfoList(mvo.getPaymentIdList(), String.valueOf(mvo.getBatchNumber()));
                 rvoList = loadPaymentInfoList(mvo.getRefundIdList(), String.valueOf(mvo.getBatchNumber()));
                                  
                 // Handle payments with no auth and fully refunded. If a payment has no auth and is not fully
                 // refunded, remove from list.
                 this.processNoAuth(pvoList, rvoList);
                 
                 logger.debug("pvoList: " + pvoList);
                 logger.debug("rvoList: " + rvoList);
                 
				 if(pvoList != null)
				 		logger.debug("pvoList.size(): " + pvoList.size());
				 if(rvoList != null)
				 		logger.debug("rvoList(): " + rvoList.size());
				 
                 if((pvoList != null && pvoList.size() > 0) || (rvoList != null && rvoList.size() > 0)) {
                     // Consolidates total if needed. Sets appropriate action flags.
                     this.validateCart(orderGuid, pvoList, rvoList);
                    	 
                     // Process cart 
                     this.processCart(pvoList, rvoList, mvo);
                 }
             } else {
                 super.sendSystemMessage(con, new Exception(AltPayConstants.UA_ERROR_LOCK_FAILURE), false);
             }
        } catch(EntityLockedException e) { 
            //send nopage notification
             super.sendSystemMessage(con, new Exception(AltPayConstants.UA_ERROR_LOCK_FAILURE), false);
        } catch(Throwable t) {
            try {
                logger.error(t);
                logger.info(mvo.toMessageXML());
                super.sendSystemMessage(con, t, false);
            } catch (Throwable tt) {
                logger.error(tt);
            }
        } finally {
            try {
                // Increment processed count regardless of result
                boolean areAllCartsProcessed = super.incrementAndCheckProcessedCount(mvo.getBatchNumber());
    
                // If all carts processed then dispatch to trigger UA EOD finalizer
				logger.debug("areAllCarts Processed: " + areAllCartsProcessed);
                if (areAllCartsProcessed == true) {
                    List msgList  = new ArrayList();
                    logger.info("Enqueuing message for UA EOD Finalizer");
                    msgList.add(AltPayConstants.UA_EOD_FINALIZER_PAYLOAD);
                    AccountingUtil.dispatchJMSMessageList
                            (AltPayConstants.ALTPAY_EOD_QUEUE, msgList, 0, 0);
                }
            } catch(Exception e) {
                logger.error(e);
                super.sendSystemMessage(con, e, false);
            }
            if(lockObtained) {
                try {
                    lockUtil.releaseLock(AltPayConstants.UA_LOCK_ENTITY_TYPE,orderGuid,AltPayConstants.UA_LOCK_CSR_ID,sessionId);
                } catch (Exception e){
                    logger.error(e);
                    super.sendSystemMessage(con, e, false);
                }
            }
        }
        
        logger.info("Exiting process of UACartHandler...");
    }
    
    /**
     * If payment, deduct miles. If refunds, consolidate refund miles and create a
     * billing detail.
     *        
     * @param pvoList
     * @throws Throwable
     */
    private void processCart(List pvoList, List rvoList, EODMessageVO mvo) throws Throwable {
        if(pvoList != null && pvoList.size() > 0) {

            // The net result is a payment. Deduct miles. 
            UABillingDetailVO uavo = (UABillingDetailVO)pvoList.get(0);
            if(uavo.isCreateHistoryFlag()) {
            
                logger.debug("Calling doDeductMiles for payment id: " + uavo.getPaymentId());
                String success = super.doDeductMiles(uaProfileVO, uavo);
                    
                if(AltPayConstants.MILES_APPROVED.equals(success)) 
                {
                    // update bill status
                    logger.debug("doDeductMiles successful");
                    super.updateOrderBillStatus(pvoList, mvo.getBatchTime(), ARConstants.ACCTG_STATUS_BILLED);
                    super.updateRefundStatus(rvoList, mvo.getBatchTime(),ARConstants.ACCTG_STATUS_BILLED);
                    
                } else if(AltPayConstants.MILES_DECLINED.equals(success)) {
                        logger.debug("doUpdateMiles failed");
                        // to do: Send email to LP
                        String toAddress = (String)uaProfileVO.getConfigsMap().get(ARConstants.LP_EMAIL_ADDRESS);
                        String fromAddress = ARConstants.EMAIL_FROM_ADDRESS;
                        super.sendEmail(toAddress, 
                                        fromAddress, 
                                        AltPayConstants.UA_LP_EMAIL_SUBJECT + uavo.getMasterOrderNumber(), 
                                        AltPayConstants.UA_LP_EMAIL_CONTENT);
                } // else - error. will retry next day.
            } else {
                // Full same day refund
                logger.debug("processing full same day refund");
                super.updateOrderBillStatus(pvoList, mvo.getBatchTime(),ARConstants.ACCTG_STATUS_BILLED);
                super.updateRefundStatus(rvoList, mvo.getBatchTime(),ARConstants.ACCTG_STATUS_BILLED);
            }
        } else if(rvoList != null && rvoList.size() > 0) {
        // refund
            logger.debug("processing refunds...");
            UABillingDetailVO uavo = (UABillingDetailVO)rvoList.get(0);
            
            logger.info("checking isCreateHistoryFlag: " + uavo.isCreateHistoryFlag());
            logger.info("refund id: " + uavo.getRefundId());
            
            if(uavo.isCreateHistoryFlag()) {
                // create billing detail
                altPayDAO.doCreateBillingDetailUA(con, uaProfileVO, uavo);
                      
                super.updateRefundStatus(rvoList, mvo.getBatchTime(), ARConstants.ACCTG_STATUS_BILLED);
                super.updateOrderBillStatus(pvoList, mvo.getBatchTime(),ARConstants.ACCTG_STATUS_BILLED);
            } // else - refunds are invalid.
        }
    } 
    
    
    /**
     * Retrieve payment information for the payment ids in the list.
     * @param list
     * @param batchNumber
     * @return
     * @throws Throwable
     */
     
    private List loadPaymentInfoList(List list, String batchNumber) throws Throwable {
        logger.info("entering loadPaymentInfoList");
        List infoList = null;
        if(list != null) {
            Iterator i = list.iterator();
            infoList = new ArrayList();
            while (i.hasNext()) {
                EODPaymentVO pvo = (EODPaymentVO)i.next();
                UABillingDetailVO uavo = (UABillingDetailVO)this.loadPaymentInfo(pvo);
                uavo.setBillingHeaderId(batchNumber);
                infoList.add(uavo);
            }
        }
        logger.info("exiting loadPaymentInfoList");
        return infoList;
    }
    
    /**
     * Retreives payment information to prepare for the call.
     * @param pvo
     * @return
     * @throws Throwable
     */
     
    private AltPayBillingDetailVOBase loadPaymentInfo(EODPaymentVO pvo) throws Throwable {
        logger.info("entering loadPaymentInfo:" + pvo.getPaymentInd());
        CachedResultSet rs = super.getPaymentInfo(pvo);
        String membershipNumber = null;
        int milesPointsAmt = 0;
        
        UABillingDetailVO uavo = new UABillingDetailVO();
        uavo.setMasterOrderNumber(pvo.getMasterOrderNumber());
        uavo.setPaymentCode(pvo.getPaymentInd());
        uavo.setPaymentId(pvo.getPaymentId());
        
        
        if(rs != null && rs.next()) {
            if(rs.getString("miles_points_amt") == null) {
                throw new Exception("Amount is null for payment id: " + uavo.getPaymentId());
            }
            
            membershipNumber = rs.getString("ap_account_txt");
            milesPointsAmt = rs.getInt("miles_points_amt");
            uavo.setReqAmt(new BigDecimal(rs.getString("amount")));
            uavo.setPaymentRecAmt(new BigDecimal(rs.getString("amount"))); 
            uavo.setMembershipNumber(membershipNumber);
            uavo.setReqMilesAmt(milesPointsAmt);
            uavo.setRefundId(rs.getString("refund_id"));
            uavo.setAuthNumber(rs.getString("auth_number"));
        }
        logger.info("exiting loadPaymentInfo");
        return uavo;
    }

    public void setUaProfileVO(UAProfileVO uaProfileVO) {
        this.uaProfileVO = uaProfileVO;
    }

    public UAProfileVO getUaProfileVO() {
        return uaProfileVO;
    }


}
