package com.ftd.accountingreporting.altpay.vo;

import java.math.BigDecimal;


public class AltPayBillingDetailVOBase {
    protected String billingDetailId;
    protected String billingHeaderId;
    protected String masterOrderNumber;
    protected String orderGuid;
    protected String paymentId;
    protected String refundId;
    protected String paymentCode;
    protected BigDecimal paymentRecAmt; // amount in db
    protected BigDecimal reqAmt;        // amount to send
    protected boolean updateAcctgStatusFlag;
    protected boolean createHistoryFlag;
    protected String membershipNumber;
    protected int reqMilesAmt;
    protected String authNumber;

    public void setBillingDetailId(String billingDetailId) {
        this.billingDetailId = billingDetailId;
    }

    public String getBillingDetailId() {
        return billingDetailId;
    }
    
    public void setMasterOrderNumber(String masterOrderNumber) {
        this.masterOrderNumber = masterOrderNumber;
    }

    public String getMasterOrderNumber() {
        return masterOrderNumber;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentCode(String paymentCode) {
        this.paymentCode = paymentCode;
    }

    public String getPaymentCode() {
        return paymentCode;
    }
    

    public void setUpdateAcctgStatusFlag(boolean updateAcctgStatusFlag) {
        this.updateAcctgStatusFlag = updateAcctgStatusFlag;
    }

    public boolean isUpdateAcctgStatusFlag() {
        return updateAcctgStatusFlag;
    }

    public void setCreateHistoryFlag(boolean createHistoryFlag) {
        this.createHistoryFlag = createHistoryFlag;
    }

    public boolean isCreateHistoryFlag() {
        return createHistoryFlag;
    }


    public void setPaymentRecAmt(BigDecimal paymentRecAmt) {
        this.paymentRecAmt = paymentRecAmt;
    }

    public BigDecimal getPaymentRecAmt() {
        return paymentRecAmt;
    }
    
    public void setReqAmt(BigDecimal reqAmt) {
        this.reqAmt = reqAmt;
    }

    public BigDecimal getReqAmt() {
        return reqAmt;
    }

    public void setOrderGuid(String orderGuid) {
        this.orderGuid = orderGuid;
    }

    public String getOrderGuid() {
        return orderGuid;
    }

    public void setRefundId(String refundId) {
        this.refundId = refundId;
    }

    public String getRefundId() {
        return refundId;
    }

    public void setBillingHeaderId(String billingHeaderId) {
        this.billingHeaderId = billingHeaderId;
    }

    public String getBillingHeaderId() {
        return billingHeaderId;
    }

    public void setReqMilesAmt(int reqMilesAmt) {
        this.reqMilesAmt = reqMilesAmt;
    }

    public int getReqMilesAmt() {
        return reqMilesAmt;
    }

    public void setMembershipNumber(String membershipNumber) {
        this.membershipNumber = membershipNumber;
    }

    public String getMembershipNumber() {
        return membershipNumber;
    }

    public void setAuthNumber(String authNumber) {
        this.authNumber = authNumber;
    }

    public String getAuthNumber() {
        return authNumber;
    }
}
