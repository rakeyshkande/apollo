package com.ftd.accountingreporting.altpay.vo;

import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.osp.utilities.ConfigurationUtil;

import java.math.BigDecimal;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class PayPalBillingDetailVO extends AltPayBillingDetailVOBase {

    protected String reqTransactionId;
    protected String resTransactionId;
    protected String resCorrelationId;
    protected String resAck;
    protected String resErrorMessage;
    protected BigDecimal resGrossAmt;
    protected BigDecimal resFeeAmt;
    protected BigDecimal resNetAmt;


    public void setReqTransactionId(String reqTransactionId) {
        this.reqTransactionId = reqTransactionId;
    }

    public String getReqTransactionId() {
        return reqTransactionId;
    }

    public void setResTransactionId(String resTransactionId) {
        this.resTransactionId = resTransactionId;
    }

    public String getResTransactionId() {
        return resTransactionId;
    }

    public void setResCorrelationId(String resCorrelationId) {
        this.resCorrelationId = resCorrelationId;
    }

    public String getResCorrelationId() {
        return resCorrelationId;
    }

    public void setResAck(String resAck) {
        this.resAck = resAck;
    }

    public String getResAck() {
        return resAck;
    }

    public void setResErrorMessage(String resErrorMessage) {
        this.resErrorMessage = resErrorMessage;
    }

    public String getResErrorMessage() {
        return resErrorMessage;
    }

    public void setResGrossAmt(BigDecimal resGrossAmt) {
        this.resGrossAmt = resGrossAmt;
    }

    public BigDecimal getResGrossAmt() {
        return resGrossAmt;
    }

    public void setResFeeAmt(BigDecimal resFeeAmt) {
        this.resFeeAmt = resFeeAmt;
    }

    public BigDecimal getResFeeAmt() {
        return resFeeAmt;
    }

    public void setResNetAmt(BigDecimal resNetAmt) {
        this.resNetAmt = resNetAmt;
    }

    public BigDecimal getResNetAmt() {
        return resNetAmt;
    }

    public String toString() {
        return "paymentId=" + this.paymentId 
            +  ",paymentCode=" + this.paymentCode
            +  ",reqTransactionId=" + this.reqTransactionId
            +  ",reqAmt=" + this.reqAmt.toString()
            +  ",resTransactionId=" + this.resTransactionId;
    }
}
