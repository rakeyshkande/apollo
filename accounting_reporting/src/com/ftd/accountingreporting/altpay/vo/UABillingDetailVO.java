package com.ftd.accountingreporting.altpay.vo;

import java.util.Date;


/**
 * Object representation for a record in clean.alt_pay_billing_detail_ua table.
 * In reality this class is only used for refunds as records for charges are
 * done in miles-points-services.
 */
public class UABillingDetailVO extends AltPayBillingDetailVOBase {

    protected Date reqRedemptionDate;
    protected String resSuccessFlag;
    protected String resErrorMessage;
    protected String manualEntryFlag;
    protected String reqBonusCode;
    protected String reqBonusType;
    protected String reqMileIndicator;
    protected String memberLastName;
    protected String externalOrderNumber;

    public void setManualEntryFlag(String manualEntryFlag) {
        this.manualEntryFlag = manualEntryFlag;
    }

    public String getManualEntryFlag() {
        return manualEntryFlag;
    }

    public void setReqBonusCode(String reqBonusCode) {
        this.reqBonusCode = reqBonusCode;
    }

    public String getReqBonusCode() {
        return reqBonusCode;
    }

    public void setReqBonusType(String reqBonusType) {
        this.reqBonusType = reqBonusType;
    }

    public String getReqBonusType() {
        return reqBonusType;
    }

    public void setReqMileIndicator(String reqMileIndicator) {
        this.reqMileIndicator = reqMileIndicator;
    }

    public String getReqMileIndicator() {
    
        return reqMileIndicator;
    }

    public void setReqRedemptionDate(Date reqRedemptionDate) {
        this.reqRedemptionDate = reqRedemptionDate;
    }

    public Date getReqRedemptionDate() {
        return reqRedemptionDate;
    }

    public void setResSuccessFlag(String resSuccessFlag) {
        this.resSuccessFlag = resSuccessFlag;
    }

    public String getResSuccessFlag() {
        return resSuccessFlag;
    }

    public void setResErrorMessage(String resErrorMessage) {
        this.resErrorMessage = resErrorMessage;
    }

    public String getResErrorMessage() {
        return resErrorMessage;
    }

	public String getMemberLastName() {
		return memberLastName;
	}

	public void setMemberLastName(String memberLastName) {
		this.memberLastName = memberLastName;
	}

	public String getExternalOrderNumber() {
		return externalOrderNumber;
	}

	public void setExternalOrderNumber(String externalOrderNumber) {
		this.externalOrderNumber = externalOrderNumber;
	}
    
}
