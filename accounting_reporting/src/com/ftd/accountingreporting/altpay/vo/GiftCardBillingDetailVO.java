package com.ftd.accountingreporting.altpay.vo;

import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.osp.utilities.ConfigurationUtil;

import java.math.BigDecimal;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class GiftCardBillingDetailVO extends AltPayBillingDetailVOBase {

    // Represents the users GiftCard number
    protected String accountNumber;

    // Represents the users PIN number
    protected String pin;

	// Represents date order was approved
    protected Calendar authDate;
    
    // Represents the actual transaction ID for calls to SVS (needed by SVS for dup-check processing)
    protected String captureTxt;

    // Status and any associated error text for transactions
    protected String status;
	protected String errorTxt;
    
    
	public void setAccountNumber(String accountNumber)
    {
      this.accountNumber = accountNumber;
    }
  
    public String getAccountNumber()
    {
      return accountNumber;
    }

    public Calendar getAuthDate() {
        return authDate;
    }
    
    public void setAuthDate(Calendar newAuthDate) {
        authDate = newAuthDate;
    }

    public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}
    
    public String getCaptureTxt() {
		return captureTxt;
	}

	public void setCaptureTxt(String captureTxt) {
		this.captureTxt = captureTxt;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getErrorTxt() {
		return errorTxt;
	}

	public void setErrorTxt(String error_txt) {
		this.errorTxt = error_txt;
	}


	public String toString() {
        return "paymentId=" + this.paymentId 
            +  ",refundId=" + this.refundId
            +  ",reqAmt=" + this.reqAmt
            +  ",paymentRecAmt" + this.paymentRecAmt
            +  ",paymentCode=" + this.paymentCode
            +  ",authNumber=" + this.authNumber
            +  ",captureTxt=" + this.captureTxt
            +  ",status=" + this.status
            +  ",errorTxt=" + this.errorTxt;
    }
}
