package com.ftd.accountingreporting.altpay.vo;

import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.constant.AltPayConstants;
import com.ftd.accountingreporting.vo.EODPaymentVO;
import com.ftd.osp.utilities.ConfigurationUtil;

import java.math.BigDecimal;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class AltPayReportDataVO {
    private int pmtErrorCount;
    private int refundErrorCount;
    private int authErrorCount;
    private boolean reportErrorFlag;
    private String reportText;

    public AltPayReportDataVO() {}
    
    public AltPayReportDataVO(int pcount, int rcount, int acount) {
        pmtErrorCount = pcount;
        refundErrorCount = rcount;
        authErrorCount = acount;
    }
    public void setPmtErrorCount(int pmtErrorCount) {
        this.pmtErrorCount = pmtErrorCount;
    }

    public int getPmtErrorCount() {
        return pmtErrorCount;
    }

    public void setRefundErrorCount(int refundErrorCount) {
        this.refundErrorCount = refundErrorCount;
    }

    public int getRefundErrorCount() {
        return refundErrorCount;
    }

    public void setAuthErrorCount(int authErrorCount) {
        this.authErrorCount = authErrorCount;
    }

    public int getAuthErrorCount() {
        return authErrorCount;
    }

    public void setReportErrorFlag(boolean reportErrorFlag) {
        this.reportErrorFlag = reportErrorFlag;
    }

    public boolean isReportErrorFlag() {
        reportErrorFlag = false;
        
        if (pmtErrorCount>0 || refundErrorCount>0 || authErrorCount>0) {
            reportErrorFlag = true;
        }
        return reportErrorFlag;
    }

    public void setReportText(String reportText) {
        this.reportText = reportText;
    }

    public String getReportText() {
        return reportText;
    }
}
