package com.ftd.accountingreporting.altpay.vo;


public class AafesBillingDetailVO extends AltPayBillingDetailVOBase {

    protected String facilityNumber;
    protected String ticketNumber;
    protected String returnCode;
    protected String returnMessage;
    protected String verifiedFlag;
    protected String refundId;
    protected String ccnumber;

    public String toString() {
        return "paymentId=" + this.paymentId 
            +  ",paymentCode=" + this.paymentCode
            +  ",ticketNumber=" + this.ticketNumber
            +  ",reqAmt=" + this.reqAmt.toString()
            +  ",authCode=" + this.authNumber;
    }

    public void setFacilityNumber(String facilityNumber) {
        this.facilityNumber = facilityNumber;
    }

    public String getFacilityNumber() {
        return facilityNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnMessage(String returnMessage) {
        this.returnMessage = returnMessage;
    }

    public String getReturnMessage() {
        return returnMessage;
    }

    public void setVerifiedFlag(String verifiedFlag) {
        this.verifiedFlag = verifiedFlag;
    }

    public String getVerifiedFlag() {
        return verifiedFlag;
    }

    public void setCcnumber(String ccnumber) {
        this.ccnumber = ccnumber;
    }

    public String getCcnumber() {
        return ccnumber;
    }
}
