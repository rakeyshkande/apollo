package com.ftd.accountingreporting.altpay.vo;

import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.osp.utilities.ConfigurationUtil;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;


public class AltPayProfileVOBase implements Serializable {
    
    protected String paymentMethodId;
    protected String runDateOffset;
    protected String stmtGetEODRecords;
    protected String stmtInsertBillingHeader;
    protected String stmtInsertBillingDetail;
    protected String stmtUpdateBillingDetail;
    protected List secureConfigs;
    protected List globalParms;
    protected Map configsMap;
    protected String dispatchToQueue;
    protected String updateDispatcheCountFlag;
    
    public void setStmtGetEODRecords(String stmtGetEODRecords) {
        this.stmtGetEODRecords = stmtGetEODRecords;
    }

    public String getStmtGetEODRecords() {
        return stmtGetEODRecords;
    }

    public void setPaymentMethodId(String paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public String getPaymentMethodId() {
        return paymentMethodId;
    }
    
    public void setSecureConfigs(List secureConfigs) {
        this.secureConfigs = secureConfigs;
    }

    public List getSecureConfigs() {
        return secureConfigs;
    }

    public void setGlobalParms(List globalParms) {
        this.globalParms = globalParms;
    }

    public List getGlobalParms() {
        return globalParms;
    }    
    
    public void setDispatchToQueue(String dispatchToQueue) {
        this.dispatchToQueue = dispatchToQueue;
    }

    public String getDispatchToQueue() {
        return dispatchToQueue;
    }     
    
    public void createConfigsMap () throws Throwable {
        if(configsMap == null) {
            configsMap = new HashMap();
        }
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        if(secureConfigs != null) {
            Iterator i = secureConfigs.iterator();
            while (i.hasNext()) {
                String name = (String)i.next();
                String value = null;
                if (name.contains(",")) {
                	// Context was specified
                	String[] contextAndName = StringUtils.split(name, ',');
                    value = configUtil.getSecureProperty(contextAndName[0], contextAndName[1]);                	                	
                } else {
                	// Use default context for accounting
                    value = configUtil.getSecureProperty(ARConstants.SECURE_CONFIG_CONTEXT, name);                	
                }
                configsMap.put(name, value);
            }
        }
        
        if(globalParms != null) {
            Iterator i = globalParms.iterator();
            while (i.hasNext()) {
                String name = (String)i.next();
                String value = null;
                if (name.contains(",")) {
                	// Context was specified
                	String[] contextAndName = StringUtils.split(name, ',');
                    value = configUtil.getFrpGlobalParm(contextAndName[0], contextAndName[1]);
                } else {
                	// Use default context for accounting
                    value = configUtil.getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, name);
                }
                configsMap.put(name, value);
            }
        }
    }
    
    public void initValues(String _paymentMethodId,
                          String _stmtGetEODRecords,
                          String _stmtInsertBillingDetail,
                          String _stmtUpdateBillingDetail,
                          Map _configsMap,
                          String _dispatchToQueue
                          )
    {
        paymentMethodId = _paymentMethodId;
        stmtGetEODRecords = _stmtGetEODRecords;
        stmtInsertBillingDetail = _stmtInsertBillingDetail;
        stmtUpdateBillingDetail = _stmtUpdateBillingDetail;
        configsMap = _configsMap;
        dispatchToQueue = _dispatchToQueue;
    }

    public void setStmtInsertBillingDetail(String stmtInsertBillingDetail) {
        this.stmtInsertBillingDetail = stmtInsertBillingDetail;
    }

    public String getStmtInsertBillingDetail() {
        return stmtInsertBillingDetail;
    }

    public void setStmtUpdateBillingDetail(String stmtUpdateBillingDetail) {
        this.stmtUpdateBillingDetail = stmtUpdateBillingDetail;
    }

    public String getStmtUpdateBillingDetail() {
        return stmtUpdateBillingDetail;
    }

    public Map getConfigsMap() {
        return configsMap;
    }

    public void setStmtInsertBillingHeader(String stmtInsertBillingHeader) {
        this.stmtInsertBillingHeader = stmtInsertBillingHeader;
    }

    public String getStmtInsertBillingHeader() {
        return stmtInsertBillingHeader;
    }

    public void setUpdateDispatcheCountFlag(String updateDispatcheCountFlag) {
        this.updateDispatcheCountFlag = updateDispatcheCountFlag;
    }

    public String getUpdateDispatcheCountFlag() {
        return updateDispatcheCountFlag;
    }
}
