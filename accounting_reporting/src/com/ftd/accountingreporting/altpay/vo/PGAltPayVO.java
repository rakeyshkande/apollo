package com.ftd.accountingreporting.altpay.vo;

public class PGAltPayVO {

	private String paymentTypeId;
	private String stmtGetEODRecords;
	private int altPayOffSet;
	public String getPaymentTypeId() {
		return paymentTypeId;
	}
	public void setPaymentTypeId(String paymentTypeId) {
		this.paymentTypeId = paymentTypeId;
	}
	public String getStmtGetEODRecords() {
		return stmtGetEODRecords;
	}
	public void setStmtGetEODRecords(String stmtGetEODRecords) {
		this.stmtGetEODRecords = stmtGetEODRecords;
	}
	public int getAltPayOffSet() {
		return altPayOffSet;
	}
	public void setAltPayOffSet(int altPayOffSet) {
		this.altPayOffSet = altPayOffSet;
	}
	@Override
	public String toString() {
		return "PGAltPayVO [paymentTypeId=" + paymentTypeId + ", stmtGetEODRecords=" + stmtGetEODRecords
				+ ", altPayOffSet=" + altPayOffSet + "]";
	}

	
	
	
	
	
}
