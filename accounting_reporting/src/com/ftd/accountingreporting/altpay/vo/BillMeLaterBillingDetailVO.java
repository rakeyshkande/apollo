package com.ftd.accountingreporting.altpay.vo;

import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.osp.utilities.ConfigurationUtil;

import java.math.BigDecimal;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class BillMeLaterBillingDetailVO extends AltPayBillingDetailVOBase {

    // Represents the users BML account number
    protected String accountNumber;

    // Represents the unique number Novator sends during authorization.
    // FYI, Novator couldn't send the master order number since they
    // don't create it until after authorization.
    protected String authOrderNumber;
    
    // Represents date order was approved
    protected Calendar authDate;
        
    public void setAccountNumber(String accountNumber)
    {
      this.accountNumber = accountNumber;
    }
  
    public String getAccountNumber()
    {
      return accountNumber;
    }

    public void setAuthOrderNumber(String authOrderNumber) {
        this.authOrderNumber = authOrderNumber;
    }

    public String getAuthOrderNumber() {
        return authOrderNumber;
    }

    public Calendar getAuthDate() {
        return authDate;
    }
    
    public void setAuthDate(Calendar newAuthDate) {
        authDate = newAuthDate;
    }

    public String toString() {
        return "paymentId=" + this.paymentId 
            +  ",paymentCode=" + this.paymentCode
            +  ",authNumber=" + this.authNumber
            +  ",authOrderNumber=" + this.authOrderNumber;
    }
}
