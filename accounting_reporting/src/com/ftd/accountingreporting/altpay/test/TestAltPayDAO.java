package com.ftd.accountingreporting.altpay.test;

/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

import com.ftd.accountingreporting.altpay.dao.AltPayDAO;
import com.ftd.accountingreporting.altpay.handler.IAltPayEODHandler;
import com.ftd.accountingreporting.altpay.handler.PayPalCartHandler;
import com.ftd.accountingreporting.altpay.handler.PayPalEODHandler;
import com.ftd.accountingreporting.altpay.vo.AltPayProfileVOBase;
import com.ftd.accountingreporting.altpay.vo.AltPayReportDataVO;
import com.ftd.accountingreporting.altpay.vo.PayPalProfileVO;
import com.ftd.accountingreporting.test.TestHelper;
import com.ftd.accountingreporting.util.EODUtil;
import com.ftd.accountingreporting.util.SystemMessager;
import com.ftd.accountingreporting.vo.EODMessageVO;
import com.ftd.osp.utilities.plugins.Logger;

import java.util.Calendar;

import com.paypal.sdk.exceptions.PayPalException;
import com.paypal.sdk.exceptions.TransactionException;
import com.paypal.sdk.profiles.APIProfile;
import com.paypal.sdk.profiles.ProfileFactory;
import com.paypal.sdk.services.CallerServices;
import com.paypal.soap.api.*;

import java.sql.Connection;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJBException;

import javax.jms.TextMessage;

import org.apache.commons.lang.StringUtils;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Tests AltPayDAO
 */ 
public class TestAltPayDAO {
    private static Connection con = TestHelper.getConnection();
    private static AltPayDAO altPayDAO = new AltPayDAO();
    private static PayPalProfileVO pvo = new PayPalProfileVO();
    private String message = "";
    private static Logger logger = new Logger("com.ftd.accountingreporting.altpay.test.TestPayPalCartHandler");
    

    public TestAltPayDAO() {
    }
    
    public static void main(String[] args) {
        try {
                TestAltPayDAO app =      new TestAltPayDAO();
                app.run();
        }
        catch (Exception e) {
                logger.error(e);
                System.out.println("ERROR: " + e.getMessage());
        }
    }
    
    public void run() throws PayPalException {
        
        try {
            AltPayDAO dao = new AltPayDAO();
            
            AltPayReportDataVO vo = dao.doGetReportDataPayPal(con, "Y");
            
            System.out.println(vo.getReportText());
        } catch (Throwable t) {
            
            logger.error(t);
            
            try {
                SystemMessager.sendSystemMessage(con, t);
            } catch (Exception e) {
                e.printStackTrace();
            }
            
        } 
        finally {
            logger.debug("End: onMessage()");
             
            try 
            {
                //if(con!=null) {
                //    con.close();
                //}
            }
            catch (Exception e)
            {
                logger.warn(e);
                e.printStackTrace();
            }
        }
        System.out.println("\nDone...");
    }
    
 
} // TestAltPayDAO