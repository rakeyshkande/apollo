package com.ftd.accountingreporting.altpay.test;

/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

import com.ftd.osp.utilities.plugins.Logger;

import java.util.Calendar;

import com.paypal.sdk.exceptions.PayPalException;
import com.paypal.sdk.exceptions.TransactionException;
import com.paypal.sdk.profiles.APIProfile;
import com.paypal.sdk.profiles.ProfileFactory;
import com.paypal.sdk.services.CallerServices;
import com.paypal.soap.api.*;

/**
 * PayPal Java SDK sample application
 * 1. Direct Payment of $20.00 response returns transaction id 13C20369NU563010P
 * 2. DoCapture of $20.00 sending 13C20369NU563010P in request as auth id. Response returns transaction id 7SU01111SE403773T
 * 3. RefundTransaction refunds $10.00 sending 7SU01111SE403773T in request as transaction id. Response returns refund transaction id 9N849461AE567733N
 * 4. RefundTransaction refunds $10.00 sending 7SU01111SE403773T in request as transaction id. Response returns refund transaction id 70U70976NC2422010
 * 
 * Notes:
 * 1. Can TransactionSearch ap_auth_txt to get ap_capture_txt if it's captured. type='Payment'
 * 2. If call doCapture with partial amount and Complete type, making another call for the
 *    remaining balance will result in error 10602.
 * ap_auth_txt=6EV12991K9430070X, ap_capture_txt=7A874433LW689913K
 * 3. Can call doCapture with full auth amount and type of NotComplete. This is the basis
 *    for setting all doCapture calls to NotComplete
 * ap_auth_txt=53P08993A3451970E, ap_capture_txt=0TL694762H836610N
 * 4. Can call RefundTransaction with full amount and type of Partial. this is the basis
 *    for settng all refundTransaction calls to Partial.
 * ap_capture_txt=0TL694762H836610N,refund_transaction_txt=4NM073011H0991013
 * Note: auto fix if doCapture call returns error code 10602 
 */ 
public class TestPayPal {
	CallerServices caller; 
        Logger logger = new Logger("com.ftd.accountingreporting.altpay.test.TestPayPal");
	
    public static void main(String[] args) {
    	try {
    		TestPayPal app =	new TestPayPal();
    		app.run();
		}
    	catch (Exception e) {
        e.printStackTrace();
    		System.out.println("ERROR: " + e.getMessage());
		}
    }
    
    public TestPayPal() throws PayPalException {
    	caller = new CallerServices();
    	
    	/*
    	 WARNING: Do not embed plaintext credentials in your application code.
    	 Doing so is insecure and against best practices.
    	 Your API credentials must be handled securely. Please consider 
    	 encrypting them for use in any production environment, and ensure
    	 that only authorized individuals may view or modify them.
    	 */
         //setSampleAppProfile();
         //setBuyerProfile();
         //setMerchantProfile();
         setNovatorTestProfile();
         
    }
    private void setBuyerProfile() throws PayPalException{
        APIProfile profile = ProfileFactory.createSSLAPIProfile();
        profile.setAPIUsername("buyer.hu_api1.ftdi.com");
        profile.setAPIPassword("E3NMF9QMBDA8NZHD");
        profile.setCertificateFile("C:\\chu\\release\\2_4_0\\1962\\sandbox\\paypal_cert_buyer.p12");
        profile.setPrivateKeyPassword("ftd2pbuyer");
        profile.setEnvironment("sandbox");
        profile.setTimeout(60000);
        caller.setAPIProfile(profile);
    }
    
    private void setMerchantProfile() throws PayPalException{
        APIProfile profile = ProfileFactory.createSSLAPIProfile();
        profile.setAPIUsername("christy.hu_api1.ftdi.com");
        profile.setAPIPassword("2PUZXH3ULWH5MSKH");
        profile.setCertificateFile("C:\\chu\\release\\2_4_0\\1962\\sandbox\\paypal_cert_sandbox.p12");
        profile.setPrivateKeyPassword("ftd2psandbox");
        profile.setEnvironment("sandbox");
        profile.setTimeout(60000);
        caller.setAPIProfile(profile);
    }
    
    private void setNovatorTestProfile() throws PayPalException{
        APIProfile profile = ProfileFactory.createSSLAPIProfile();
        profile.setAPIUsername("dwhalen_api1.ftdi.com");
        profile.setAPIPassword("TFQPS47NRNHF4U9W");
        profile.setCertificateFile("C:\\chu\\release\\2_4_0\\1962\\sandbox_novator\\paypal_cert_sandbox.p12");
        profile.setPrivateKeyPassword("ftd2psandbox");
        profile.setEnvironment("sandbox");
        profile.setTimeout(60000);
        caller.setAPIProfile(profile);
    }
    
    private void setSampleAppProfile() throws PayPalException{
        APIProfile profile = ProfileFactory.createSSLAPIProfile();
        profile.setAPIUsername("sdk-seller_api1.sdk.com");
        profile.setAPIPassword("12345678");
        profile.setCertificateFile("Cert/sdk-seller.p12");
        profile.setPrivateKeyPassword("password");
        profile.setEnvironment("sandbox");
        profile.setTimeout(60000);
        caller.setAPIProfile(profile);
    }    
    
    public void run() throws PayPalException {
    	//TransactionSearch();
	//GetTransactionDetails("4DK95773BJ871825M");
	//DoDirectPayment();
        //DoCaptureCall();
        //doRefundCall();
        GetTransactionDetails("54ce391317291");

	System.out.println("\nDone...");
    }
    
    public void TransactionSearch() throws PayPalException {
    	System.out.println("\n########## Transaction Search ##########\n");
		TransactionSearchRequestType request = new TransactionSearchRequestType();
		Calendar calendar = Calendar.getInstance();
		calendar.set(2005,2,25);
		request.setStartDate(calendar);
		request.setTransactionID("6EV12991K9430070X");
	
		TransactionSearchResponseType response = 
			(TransactionSearchResponseType) caller.call("TransactionSearch", request);
		System.out.println("Operation Ack: " + response.getAck());
		System.out.println("---------- Results ----------");
				
		// Check to see if any transactions were found
		PaymentTransactionSearchResultType[] ts = response.getPaymentTransactions();
		if (ts != null) 
		{					
			System.out.println("Found " + ts.length + " records");
					
			// Display the results of the first transaction returned
			for (int i = 0; i < ts.length; i++) 
			{	
				System.out.println("\nTransaction ID: " + ts[i].getTransactionID());
			        System.out.println("Transaction Type: " + ts[i].getType());
				System.out.println("Payer Name: " + ts[i].getPayerDisplayName());
				System.out.println("Gross Amount: " + ts[i].getGrossAmount().getCurrencyID() + " " + ts[i].getGrossAmount().get_value());
				System.out.println("Fee Amount: " + ts[i].getFeeAmount().getCurrencyID() + " " + ts[i].getFeeAmount().get_value());
				System.out.println("Net Amount: " + ts[i].getNetAmount().getCurrencyID() + " " + ts[i].getNetAmount().get_value());
			}
		}
		else 
		{
			System.out.println("Found 0 transaction");
		}
    }
   
    public void GetTransactionDetails() throws PayPalException {
    	System.out.println("\n########## Get Transaction Details ##########\n");
		GetTransactionDetailsRequestType request = new GetTransactionDetailsRequestType();
 	  	request.setTransactionID("7J110007888511720");
 	  	
 	  	GetTransactionDetailsResponseType response = (GetTransactionDetailsResponseType) caller.call("GetTransactionDetails", request);
 	  	System.out.println("Operation Ack: " + response.getAck());
 	  	System.out.println("---------- Results ----------");
 	  	
 	  	PaymentTransactionType ts = response.getPaymentTransactionDetails();
 	  	System.out.println("\nTransaction ID: " + ts.getPaymentInfo().getTransactionID());
 	  	System.out.println("Payer Name: " + ts.getPayerInfo().getPayer());
                System.out.println("chu Payer ID: " + ts.getPayerInfo().getPayerID());
 	  	System.out.println("Receiver Name: " + ts.getReceiverInfo().getReceiver());
 	  	//System.out.println("Gross Amount: " + ts.getPaymentInfo().getGrossAmount().getCurrencyID() + " " + ts.getPaymentInfo().getGrossAmount().get_value());
    }
    
    public void DoDirectPayment() throws PayPalException {
    	System.out.println("\n########## Do Direct Payment ##########\n");
    	
    	DoDirectPaymentRequestType request = new DoDirectPaymentRequestType();
		DoDirectPaymentRequestDetailsType details = new DoDirectPaymentRequestDetailsType();

		CreditCardDetailsType creditCard = new CreditCardDetailsType();
		creditCard.setCreditCardNumber("4721930402892796");
		creditCard.setCreditCardType(CreditCardTypeType.Visa);
		creditCard.setCVV2("000");
		creditCard.setExpMonth(11);
		creditCard.setExpYear(2007);
                
                creditCard.setCreditCardNumber("4859136234775209");
                creditCard.setCreditCardType(CreditCardTypeType.Visa);
		creditCard.setCVV2("000");
		creditCard.setExpMonth(11);
		creditCard.setExpYear(2007);
		
		PayerInfoType cardOwner = new PayerInfoType();
		cardOwner.setPayerCountry(CountryCodeType.US);
		
                /*
		AddressType address = new AddressType();
		address.setPostalCode("95101");
		address.setStateOrProvince("CA");
		address.setStreet1("123 Main St");
		address.setCountryName("US");
		address.setCountry(CountryCodeType.US);
		address.setCityName("San Jose");
		cardOwner.setAddress(address);
			
		PersonNameType payerName = new PersonNameType();
		payerName.setFirstName("SDK");
		payerName.setLastName("Buyer");
		cardOwner.setPayerName(payerName);
                */
                
                 AddressType address = new AddressType();
                 address.setPostalCode("60019");
                 address.setStateOrProvince("IL");
                 address.setStreet1("1647 Woodduck Lane");
                 address.setStreet2("Apt2");
                 address.setCountryName("US");
                 address.setCountry(CountryCodeType.US);
                 address.setCityName("Wheeling");
                 cardOwner.setAddress(address);
                         
                 PersonNameType payerName = new PersonNameType();
                 payerName.setFirstName("Buyer");
                 payerName.setLastName("Hu");
                 cardOwner.setPayerName(payerName);
		creditCard.setCardOwner(cardOwner);
		details.setCreditCard(creditCard);
		
		details.setIPAddress("12.36.5.78");
		details.setMerchantSessionId("456977");
		details.setPaymentAction(PaymentActionCodeType.Authorization);

		PaymentDetailsType payment = new PaymentDetailsType();
		
		BasicAmountType orderTotal = new BasicAmountType();
		orderTotal.setCurrencyID(CurrencyCodeType.USD);
		orderTotal.set_value("28.41");
		payment.setOrderTotal(orderTotal);
		
		details.setPaymentDetails(payment);
		request.setDoDirectPaymentRequestDetails(details);
		
		DoDirectPaymentResponseType response = (DoDirectPaymentResponseType) caller.call("DoDirectPayment", request);
    	
    	System.out.println("Operation Ack: " + response.getAck());
 	  	System.out.println("---------- Results ----------");
 	  	System.out.println("\nTransaction ID: " + response.getTransactionID());
                //4DK95773BJ871825M
 	  	System.out.println("CVV2: " + response.getCVV2Code());
 	  	System.out.println("AVS: " + response.getAVSCode());
 	  	System.out.println("Gross Amount: " + response.getAmount().getCurrencyID() 
			+ " " + response.getAmount().get_value());
    }
    
    public void GetTransactionDetails(String transactionID) throws PayPalException {
        System.out.println("\n########## Get Transaction Details for ##########" + transactionID + "\n");
                GetTransactionDetailsRequestType request = new GetTransactionDetailsRequestType();
                request.setTransactionID(transactionID);
                
                GetTransactionDetailsResponseType response = (GetTransactionDetailsResponseType) caller.call("GetTransactionDetails", request);
                System.out.println("Operation Ack: " + response.getAck());
                System.out.println("---------- Results ----------");
                
                PaymentTransactionType ts = response.getPaymentTransactionDetails();
                System.out.println("\nTransaction ID: " + ts.getPaymentInfo().getTransactionID());
                System.out.println("Payer Name: " + ts.getPayerInfo().getPayer());
                System.out.println("Receiver Name: " + ts.getReceiverInfo().getReceiver());
                //System.out.println("Gross Amount: " + ts.getPaymentInfo().getGrossAmount().getCurrencyID() + " " + ts.getPaymentInfo().getGrossAmount().get_value());
    }
    
    public void DoCaptureCall() throws PayPalException {
    
    try {
        DoCaptureRequestType request = new DoCaptureRequestType();
        BasicAmountType bat = new BasicAmountType("2.00");
        bat.setCurrencyID(CurrencyCodeType.USD);
        String authId = "2C978780UV755203B";

        request.setAmount(bat);
        request.setAuthorizationID(authId);
        request.setCompleteType(CompleteCodeType.Complete);
        //this.GetTransactionDetails(authId);
        DoCaptureResponseType response = (DoCaptureResponseType) caller.call("DoCapture", request);
        System.out.println("*********");
        //this.GetTransactionDetails(authId);
        System.out.println("%%%%%%%%%%");
        DoCaptureResponseDetailsType responseDetail = response.getDoCaptureResponseDetails();
        System.out.println("Operation Ack: " + response.getAck());
        System.out.println("---------- Results ----------");
        System.out.println("authorization id " + responseDetail.getAuthorizationID());
        System.out.println("capture id " + responseDetail.getPaymentInfo().getTransactionID());
        System.out.println("payment exchange rate " + responseDetail.getPaymentInfo().getExchangeRate());
        System.out.println("payment fee amount " + responseDetail.getPaymentInfo().getFeeAmount());
        System.out.println("payment gross amount " + responseDetail.getPaymentInfo().getGrossAmount());
        System.out.println("payment settle amount " + responseDetail.getPaymentInfo().getSettleAmount());
        System.out.println("payment tax amount " + responseDetail.getPaymentInfo().getTaxAmount());
        
    } catch (Exception e) {
        e.printStackTrace();
        throw new TransactionException (e.getMessage());
    }
    
    }
    //authid: 13C20369NU563010P
    //transid:7SU01111SE403773T
    
    private void doRefundCall() throws PayPalException {
        RefundTransactionRequestType request = new RefundTransactionRequestType();
        BasicAmountType bat = new BasicAmountType("15.55");
        bat.setCurrencyID(CurrencyCodeType.USD);
        String captureId = "7A874433LW689913K";

        request.setAmount(bat);
        request.setTransactionID(captureId);
        request.setRefundType(RefundPurposeTypeCodeType.Partial);
        //this.GetTransactionDetails(authId);
        RefundTransactionResponseType response = (RefundTransactionResponseType) caller.call("RefundTransaction", request);
        System.out.println("*********");
        //this.GetTransactionDetails(authId);
        System.out.println("Operation Ack: " + response.getAck());
        System.out.println("---------- Results ----------");
        System.out.println(response.getRefundTransactionID());
        System.out.println(response.getGrossRefundAmount());
        System.out.println(response.getNetRefundAmount());
        System.out.println(response.getFeeRefundAmount());
        System.out.println(response.getCorrelationID());

    }
} // SampleApp