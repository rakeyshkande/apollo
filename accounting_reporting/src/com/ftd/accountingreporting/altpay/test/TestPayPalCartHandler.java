package com.ftd.accountingreporting.altpay.test;

/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

import com.ftd.accountingreporting.altpay.dao.AltPayDAO;
import com.ftd.accountingreporting.altpay.handler.IAltPayEODHandler;
import com.ftd.accountingreporting.altpay.handler.PayPalCartHandler;
import com.ftd.accountingreporting.altpay.handler.PayPalEODHandler;
import com.ftd.accountingreporting.altpay.vo.AltPayProfileVOBase;
import com.ftd.accountingreporting.altpay.vo.PayPalProfileVO;
import com.ftd.accountingreporting.test.TestHelper;
import com.ftd.accountingreporting.util.EODUtil;
import com.ftd.accountingreporting.util.SystemMessager;
import com.ftd.accountingreporting.vo.EODMessageVO;
import com.ftd.osp.utilities.plugins.Logger;

import java.util.Calendar;

import com.paypal.sdk.exceptions.PayPalException;
import com.paypal.sdk.exceptions.TransactionException;
import com.paypal.sdk.profiles.APIProfile;
import com.paypal.sdk.profiles.ProfileFactory;
import com.paypal.sdk.services.CallerServices;
import com.paypal.soap.api.*;

import java.sql.Connection;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJBException;

import javax.jms.TextMessage;

import org.apache.commons.lang.StringUtils;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Tests PayPalCartHandler
 */ 
public class TestPayPalCartHandler {
    private static Connection con = TestHelper.getConnection();
    private static AltPayDAO altPayDAO = new AltPayDAO();
    private static PayPalProfileVO pvo = new PayPalProfileVO();
    private String message = "";
    private static Logger logger = new Logger("com.ftd.accountingreporting.altpay.test.TestPayPalCartHandler");
    

    private void init(String msg) throws Exception {
            this.message = msg;

            Map m = new HashMap();
            
            //m.put("pp-api-username","buyer.hu_api1.ftdi.com");
            //m.put("pp-api-password","E3NMF9QMBDA8NZHD");
            //m.put("pp-private-key-password","ftd2pbuyer");
            //m.put("pp-certificate-filename","paypal_cert_buyer.p12");
            m.put("pp-api-username","christy.hu_api1.ftdi.com");
            m.put("pp-api-password","2PUZXH3ULWH5MSKH");
            m.put("pp-private-key-password","ftd2psandbox");
            m.put("pp-certificate-filename","paypal_cert_sandbox.p12");
            m.put("pp-certificate-file-location","C:\\chu\\release\\2_4_0\\1962\\sandbox\\");
            m.put("pp-endpoint-environment","sandbox");
            m.put("pp-max-capture-amt","10000.00");
            m.put("ap-report-full-error-flag","Y");
            m.put("PAY_PAL_CONNECT_TIMER_FLAG","N");
            m.put("PAY_PAL_CONNECT_TIMER_MS","1800000");
            m.put("PAY_PAL_CONNECT_RETRY_COUNT","1");
            m.put("PAY_PAL_RES_ERROR_SIZE","2000");
            m.put("PAY_PAL_EOD_RUNDATE_OFFSET","-1");
            m.put("PAY_PAL_API_CONNECTION_TIMEOUT","60000");
            m.put("PAY_PAL_TEST_CONNECTION_FLAG","N");
            m.put("PAY_PAL_TEST_TRANSACTION_ID","123");
                            

            pvo.initValues("PP", 
                          "GET_PAY_PAL_EOD_RECORDS",
                          "INSERT_AP_BILLING_DETAIL_PP",
                          "UPDATE_AP_BILLING_DETAIL_PP",
                          m,
                          "ALTPAY_CART");
           
                          
    }
    
    public TestPayPalCartHandler(String message) throws PayPalException, Exception {
        init(message);
        
        run();
    }
    
    public void run() throws PayPalException {
        
        try {
            PayPalCartHandler handler = new PayPalCartHandler();
            EODMessageVO  mvo = EODMessageVO.xmlToMessageVO(this.message);
            handler.setPaymentMethodId("PP");

            handler.setAltPayDAO(altPayDAO);
            handler.setPpProfileVO(pvo);

            handler.process(con, mvo);
            
        } catch (Throwable t) {
            
            logger.error(t);
            
            try {
                SystemMessager.sendSystemMessage(con, t);
            } catch (Exception e) {
                e.printStackTrace();
            }
            
        } 
        finally {
            logger.debug("End: onMessage()");
             
            try 
            {
                //if(con!=null) {
                //    con.close();
                //}
            }
            catch (Exception e)
            {
                logger.warn(e);
                e.printStackTrace();
            }
        }
        System.out.println("\nDone...");
    }
    
 
} // TestPayPalEODHandler