package com.ftd.accountingreporting.altpay.test;

import com.ftd.accountingreporting.altpay.dao.AltPayDAO;
import com.ftd.accountingreporting.altpay.handler.AltPayReportHandler;
import com.ftd.accountingreporting.altpay.handler.IAltPayCartHandler;
import com.ftd.accountingreporting.altpay.handler.IAltPayEODHandler;
import com.ftd.accountingreporting.altpay.handler.PayPalCartHandler;
import com.ftd.accountingreporting.altpay.handler.PayPalEODHandler;
import com.ftd.accountingreporting.altpay.vo.AltPayProfileVOBase;
import com.ftd.accountingreporting.altpay.vo.PayPalProfileVO;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.constant.AltPayConstants;
import com.ftd.accountingreporting.test.TestHelper;
import com.ftd.accountingreporting.timer.PayPalConnectionTimerData;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

import com.paypal.sdk.exceptions.PayPalException;

import java.sql.Connection;

import java.util.Iterator;
import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */


/**
 * Tests PayPalCartHandler
 */ 
public class TestSpring {

    private static Logger logger = new Logger("com.ftd.accountingreporting.altpay.test.TestPayPalCartHandler");
    
    public TestSpring()  {

    }
    
    public static void main(String[] args) {
        try {
                TestSpring app =      new TestSpring();
                app.run();
        }
        catch (Exception e) {
                logger.error(e);
                System.out.println("ERROR: " + e.getMessage());
        }
    }    
    
    public void run() throws PayPalException {
        
        try {
            //this.testAltPayDAO();
            //this.testPpProfileVO();
            //this.testPp_eodHandler();
            //this.testPpCartHandler();
            this.testApReportHandler();
            //this.testPpConnectionTimerData();
        } catch (Throwable t) {
            
            logger.error(t);
            
            try {
                
            } catch (Exception e) {
                e.printStackTrace();
            }
            
        } 
        finally {
            logger.debug("End: onMessage()");
        }
        System.out.println("\nDone...");
    }
    
    private void detailAltPayDAO (AltPayDAO dao) {
        System.out.println(dao != null);
    }
    private void testAltPayDAO() {
        String beanName = "altPayDAO";
        logger.info("Asking Spring framework for bean " + beanName);
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("spring-ejb-context.xml");        
        AltPayDAO dao = (AltPayDAO)ctx.getBeanFactory().getBean(beanName);
        this.detailAltPayDAO(dao);
    }
    
    private void detailApProfileVO(AltPayProfileVOBase pvo) {
        System.out.println(pvo != null);
        //if(pvo instanceof PayPalProfileVO) {
            //System.out.println((String)(pvo.getConfigsMap().get(AltPayConstants.PP_API_CONNECTION_TIMEOUT)));
            //System.out.println((String)(pvo.getConfigsMap().get(AltPayConstants.PP_TEST_CONNECTION_FLAG)));
        //}
        
        System.out.println(pvo.getDispatchToQueue());
        System.out.println(pvo.getPaymentMethodId());
        //System.out.println((String)(pvo.getConfigsMap().get(AltPayConstants.PP_EOD_RUNDATE_OFFSET)));
        System.out.println(pvo.getStmtGetEODRecords());
        System.out.println(pvo.getStmtInsertBillingDetail());
        System.out.println(pvo.getStmtUpdateBillingDetail());
        //System.out.println((String)(pvo.getConfigsMap().get(AltPayConstants.PP_TEST_TRANSACTION_ID)));
        
        
        List gparm = pvo.getGlobalParms();
        List sparm = pvo.getSecureConfigs();
        
        if(gparm != null) {
            Iterator i = gparm.iterator();
            while(i.hasNext()) {
                System.out.println((String)i.next());
            }
        }
        
        if(sparm != null) {
            Iterator i = sparm.iterator();
            while(i.hasNext()) {
                System.out.println((String)i.next());
            }
        }
    }
    
    private void testPpProfileVO() {
        String beanName = "ppProfileVO";
        logger.info("Asking Spring framework for bean " + beanName);
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("spring-ejb-context.xml");        
        PayPalProfileVO pvo = (PayPalProfileVO)ctx.getBeanFactory().getBean(beanName);
        this.detailApProfileVO(pvo);

    }
    
    private void testPp_eodHandler() {
        String beanName = "pp_eodHandler";
        logger.info("Asking Spring framework for bean " + beanName);
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("spring-ejb-context.xml");        
        IAltPayEODHandler handler = (IAltPayEODHandler)ctx.getBeanFactory().getBean(beanName);
        PayPalEODHandler ph = (PayPalEODHandler)handler;
        System.out.println(ph != null);
        this.detailAltPayDAO(ph.getAltPayDAO());
        this.detailApProfileVO(ph.getPpProfileVO());
    }    

    private void testPpCartHandler() {
        String beanName = "ppCartHandler";
        logger.info("Asking Spring framework for bean " + beanName);
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("spring-ejb-context.xml");        
        IAltPayCartHandler handler = (IAltPayCartHandler)ctx.getBeanFactory().getBean(beanName);
        PayPalCartHandler ch = (PayPalCartHandler)handler;
        System.out.println(ch != null);
        System.out.println(ch.getPaymentMethodId());
        this.detailAltPayDAO(ch.getAltPayDAO());
        this.detailApProfileVO(ch.getPpProfileVO());
    }  
    
    private void testApReportHandler() {
        String beanName = "ap_reportHandler";
        logger.info("Asking Spring framework for bean " + beanName);
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("spring-ejb-context.xml");        
        AltPayReportHandler handler = (AltPayReportHandler)ctx.getBeanFactory().getBean(beanName);
        
        System.out.println(handler != null);

        this.detailAltPayDAO(handler.getAltPayDAO());
        this.detailApProfileVO(handler.getApProfileVO());
    }
    
    private void testPpConnectionTimerData() throws Exception {
        String beanName = "ppConnectionTimerData";
        logger.info("Asking Spring framework for bean " + beanName);
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("spring-ejb-context.xml");        
        PayPalConnectionTimerData data = (PayPalConnectionTimerData)ctx.getBeanFactory().getBean(beanName);
        
        System.out.println(data != null);
        System.out.println(data.getPpConnectInterval());
        System.out.println(data.getTimerName());
        this.detailApProfileVO(data.getPpProfileVO());
        
        //String timerMs = (ConfigurationUtil.getInstance()).getFrpGlobalParm(ARConstants.CONFIG_CONTEXT, data.getPpConnectInterval());
        //System.out.println(timerMs);
    }
} // TestSpring