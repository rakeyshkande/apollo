package com.ftd.accountingreporting.altpay.ejb;

import java.sql.Connection;


import javax.ejb.EJBException;
import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import com.ftd.accountingreporting.altpay.handler.PGPayPalCartHandler;
import com.ftd.accountingreporting.constant.AltPayConstants;
import com.ftd.accountingreporting.util.EODUtil;
import com.ftd.accountingreporting.util.SystemMessager;
import com.ftd.accountingreporting.vo.PGEODMessageVO;
import com.ftd.osp.utilities.plugins.Logger;

public class PGAltPayCartMDB implements MessageDrivenBean, MessageListener {

	public MessageDrivenContext context;
	private static Logger logger = new Logger("com.ftd.accountingreporting.altpay.ejb.PGAltPayCartMDB");

	
	public void setMessageDrivenContext(MessageDrivenContext ctx) {
		this.context = ctx;
	}

	public void ejbRemove() {
		
	}

	/** Instantiates the spring framework for this MDB */
	public void ejbCreate() {
	}

	/**
	 * 
	 * 
	 * @param message
	 *            containing passed in parameters
	 */
	public void onMessage(Message message) {
		logger.debug("Start: onMessage()");

		Connection conn = null;
		String payload = null;
		PGEODMessageVO mvo = null;

		try {
			payload = ((TextMessage) message).getText();
			logger.info("Received payload message in PGAltPayCartMDB ::" + payload);
			mvo = PGEODMessageVO.xmlToMessageVO(payload);
			
			conn = EODUtil.createDatabaseConnection();
			
            if(mvo.getPaymentMethodId().equals(AltPayConstants.PAYPAL))
            {
            	PGPayPalCartHandler handler = new  PGPayPalCartHandler(conn);
            	logger.debug("Executing process for handler type " + handler.getClass().getName());
            	if(mvo.getBillingDetailId()>0)
            	{	
            		handler.reProcessPG(mvo);
            	}else
            	{
            		handler.process(mvo);
            	}	
            }
	    

		} catch (Throwable t) {
			String errMsg = "Error while processing AltPayCartMDB event: " + payload
					+ ".  Manual intervention is required.";
			logger.error(errMsg, t);

			try {
				SystemMessager.sendSystemMessage(conn, t);
			} catch (Exception e) {
				errMsg = "Unable to send message to support pager.";
				logger.fatal(errMsg, e);
				throw new EJBException(errMsg, e);
			}

		} finally {
			logger.debug("End: onMessage()");

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(e);
			}
		}
	}
	
	protected void onEjbCreate() {
	}
}
