package com.ftd.accountingreporting.altpay.ejb;

import com.ftd.accountingreporting.altpay.dao.AltPayDAO;
import com.ftd.accountingreporting.altpay.handler.IAltPayCartHandler;
import com.ftd.accountingreporting.util.EODUtil;
import com.ftd.accountingreporting.util.SystemMessager;
import com.ftd.accountingreporting.vo.EODMessageVO;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import javax.ejb.EJBException;
import javax.ejb.MessageDrivenContext;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.commons.lang.StringUtils;

import org.springframework.ejb.support.AbstractJmsMessageDrivenBean;


/**
 * This Message Driven Bean listens on ALTPAY_CART JMS queue. When a message arrives,
 * it looks up a handler from the spring framework based on the message payload
 * and delegates processing to that handler.
 */ 
 public class AltPayCartMDB extends AbstractJmsMessageDrivenBean implements MessageListener 
 {
    private MessageDrivenContext context;
    private static Logger logger = 
        new Logger("com.ftd.accountingreporting.altpay.ejb.AltPayCartMDB");
        
    private AltPayDAO altPayDao;

    public void setMessageDrivenContext(MessageDrivenContext ctx) {
        this.context = ctx;
    }
    
    public void ejbRemove() {
        super.ejbRemove();
    }    

    /**Instantiates the spring framework for this MDB*/
    public void ejbCreate() {
        try {
            //setBeanFactoryLocatorKey(AltPayConstants.SPRING_APP_NAME);
            //setBeanFactoryLocator(ContextSingletonBeanFactoryLocator.getInstance());
            super.ejbCreate();
    
        } catch (Throwable t) {
            logger.error(t);
        }
    }
    

    /**
     * Retrieves resources from the spring framework
     **/
    protected void onEjbCreate() {
        altPayDao = (AltPayDAO)this.getBeanFactory().getBean("altPayDAO");
    }

    /**Bean Logic
    * 1.  Retrieves message text
    * 2.  Determines the appropriate handler based on payment method id
    * 3.  Gets an instance of the handler from the spring framework
    * 4.  Executes the performTask method for the handler
    * @param message containing passed in parameters
    */
    public void onMessage(Message message) {
        logger.info("Start: onMessage()");

        Connection conn = null;
        String payload = null;
        EODMessageVO mvo = null;
        
        try {
            payload = ((TextMessage)message).getText();
            logger.info("Received message: " + payload);
            mvo = EODMessageVO.xmlToMessageVO(payload);

            conn = EODUtil.createDatabaseConnection();
            
            String beanName = StringUtils.lowerCase(mvo.getPaymentMethodId()) + "CartHandler";
            logger.info("Asking Spring framework for bean " + beanName);
            IAltPayCartHandler handler = (IAltPayCartHandler)this.getBeanFactory().getBean(beanName);
            
            logger.info("Executing \"process\" for handler type " + handler.getClass().getName());
            handler.process(conn, mvo);
            
        } catch (Throwable t) {
            String errMsg = 
                "Error while processing AltPayCartMDB event: " + 
                payload + ".  Manual intervention is required.";
            logger.error(errMsg,t);

            //Send out system notification 
            
            try {
                SystemMessager.sendSystemMessage(conn, t);
            } catch (Exception e) {
                errMsg = "Unable to send message to support pager.";
                logger.fatal(errMsg,e);
                throw new EJBException(errMsg,e);
            }
            
        } 
        finally {
            logger.debug("End: onMessage()");
            
            try 
            {
                if(conn!=null) {
                    conn.close();
                }
            }
            catch (Exception e)
            {
                logger.warn(e);
            }
        }
    }

}