package com.ftd.accountingreporting.altpay.ejb;

import java.sql.Connection;

import javax.ejb.EJBException;
import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import com.ftd.accountingreporting.altpay.handler.PGPayPalEODHandler;
import com.ftd.accountingreporting.constant.AltPayConstants;
import com.ftd.accountingreporting.util.EODUtil;
import com.ftd.osp.utilities.plugins.Logger;

public class PGAltPayEODMDB implements MessageDrivenBean, MessageListener {

	

    public MessageDrivenContext context;
    private static Logger logger = new Logger("com.ftd.accountingreporting.altpay.ejb.PGAltPayEODMDB");
  
    
    public void setMessageDrivenContext(MessageDrivenContext ctx) {
        this.context = ctx;
    }
    
    public void ejbRemove() {
        
    }    

    /**Instantiates the spring framework for this MDB*/
    public void ejbCreate() {
        try {
            
    
        } catch (Throwable t) {
            logger.error(t);
        }
    }
    

    /**Retrieves resources from the spring framework
    */
    protected void onEjbCreate() {
       // altPayDao = (AltPayDAO)this.getBeanFactory().getBean("altPayDAO");
    }

    /**Bean Logic
    * 1.  Retrieves message text
    * 2.  Determines the appropriate handler based on passed in parameters 
    * 3.  Gets an instance of the handler from the spring framework
    * 4.  Executes the performTask method for the handler
    * @param message containing passed in parameters
    */
    public void onMessage(Message message) {
        logger.debug("Start: onMessage()");

        Connection conn = null;
        String payload = null;
        
        try {
            payload = ((TextMessage)message).getText();
            logger.info("payload"+payload);

            conn = EODUtil.createDatabaseConnection();
            
            if(payload.equals(AltPayConstants.PG_PAYPAL))
            {
            	PGPayPalEODHandler handler = new  PGPayPalEODHandler(conn);
            	logger.debug("Executing process for handler type " + handler.getClass().getName());
                handler.process();
            }	
            
            
            
        } catch (Throwable t) {
            String errMsg = 
                "Error while processing AltPayEODMDB event: " + 
                payload + ".  Manual intervention is required.";
            logger.error(errMsg,t);

            //Send out system notification 
            
            try {
               // SystemMessager.sendSystemMessage(conn, t); TODO
            } catch (Exception e) {
                errMsg = "Unable to send message to support pager.";
                logger.fatal(errMsg,e);
                throw new EJBException(errMsg,e);
            }
            
        } 
        finally {
            logger.debug("End: onMessage()");
            
            try 
            {
                if(conn!=null) {
                    conn.close();
                }
            }
            catch (Exception e)
            {
                logger.warn(e);
            }
        }
    }


}
