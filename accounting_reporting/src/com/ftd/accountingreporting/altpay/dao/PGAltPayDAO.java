package com.ftd.accountingreporting.altpay.dao;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.ftd.accountingreporting.altpay.vo.PGAltPayVO;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.constant.AltPayConstants;
import com.ftd.accountingreporting.util.AccountingUtil;
import com.ftd.accountingreporting.vo.EODPaymentVO;
import com.ftd.accountingreporting.vo.PGEODMessageVO;
import com.ftd.accountingreporting.vo.PGEODRecordVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pg.client.paypal.settlement.response.SettlementResponse;

public class PGAltPayDAO {
	
	Connection conn;
	 public PGAltPayDAO(Connection conn) {
		
		 this.conn =conn;
	}
	private static Logger logger = new Logger("com.ftd.accountingreporting.altpay.dao.PGAltPayDAO");
	 public Map doGetPGAltPayEODRecords(PGAltPayVO pvo) throws Throwable
	    {
	        logger.info("Entering doGetPGAltPayEODRecords");
	        
	        AccountingUtil au = new AccountingUtil();
	        Date billingDate = au.calculateOffSetDate(pvo.getAltPayOffSet());
	        
	        CachedResultSet prs = null;
	        CachedResultSet rrs = null;
	        String masterOrderNumber = null;
	        String paymentId = null;
	        String paymentInd = null;
	        EODPaymentVO paymentvo = null;
	        ArrayList paymentList = new ArrayList();
	        ArrayList refundList = new ArrayList();
	        Map outmap = null;
	        Map cartMap = new HashMap();
	        int paymentCount = 0;
	        int refundCount = 0;
	        
	        try{
	            DataRequest dataRequest = new DataRequest();
	            dataRequest.setConnection(this.conn);
	            dataRequest.setStatementID(AltPayConstants.GET_PG_ALTPAY_PAYMENT_EOD_RECS);
	            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	            
	            HashMap inputParams = new HashMap();
	            java.sql.Date sqlBillingDate = 
	                        new java.sql.Date(billingDate.getTime());
	                        
	            inputParams.put("IN_DATE", sqlBillingDate);
	            inputParams.put("IN_PAYMENT_TYPE", pvo.getPaymentTypeId());
	            logger.info("in_date="+sqlBillingDate);
	            dataRequest.setInputParams(inputParams);
	            
	            outmap = (Map)dataAccessUtil.execute(dataRequest);
	            dataRequest.reset();
	            prs = (CachedResultSet)outmap.get("OUT_PAYMENT_CUR");
	            rrs = (CachedResultSet)outmap.get("OUT_REFUND_CUR");
	            
	            if(prs != null) {
	                while(prs.next()) {
	                    masterOrderNumber = prs.getString("master_order_number");
	                    paymentId = prs.getString("payment_id");
	                    paymentInd = prs.getString("payment_indicator");
	                    paymentvo = new EODPaymentVO(masterOrderNumber,paymentId,paymentInd);
	                    paymentList.add(paymentvo);
	                    paymentCount++;
	                }
	            }
	            if (rrs != null) {
	                while(rrs.next()) {
	                    masterOrderNumber = rrs.getString("master_order_number");
	                    paymentId = rrs.getString("payment_id");
	                    paymentInd = rrs.getString("payment_indicator");
	                    paymentvo = new EODPaymentVO(masterOrderNumber, paymentId,paymentInd);
	                    refundList.add(paymentvo);
	                    refundCount++;
	                }   
	            }
	            
	            cartMap.put("PAYMENT", paymentList);
	            cartMap.put("REFUND", refundList);
	            logger.info("Found " + paymentCount + " payments and " + refundCount + " refunds");
	            logger.info("Exiting doGetPGAltPayEODRecords");
	        } finally {
	        } 
	        return  cartMap;
	    }        
	
	 public String doPGCreateBillingHeader(PGAltPayVO pvo)
	            throws Throwable
	    {
	      
	        logger.debug("Entering doPGCreateBillingHeader");
	           
	        AccountingUtil au = new AccountingUtil();
	        //int runDateOffset = Integer.parseInt((String)(pvo.getConfigsMap().get(AltPayConstants.AP_EOD_RUNDATE_OFFSET)));
	        Date billingDate = au.calculateOffSetDate(pvo.getAltPayOffSet());
	        String statement = AltPayConstants.CREATE_PG_ALTPAY_EOD_RECS;
	        String paymentMethodId  = pvo.getPaymentTypeId();
	       
	        DataRequest request = new DataRequest();
	        request.setConnection(this.conn);
	        
	       String batchNumber = "";
	       
	       try {
	           // setup store procedure input parameters 
	           HashMap inputParams = new HashMap();
	           java.sql.Date sqlBillingDate = 
	                       new java.sql.Date(billingDate.getTime());
	                       
	           inputParams.put("IN_PAYMENT_METHOD_ID", paymentMethodId);
	           inputParams.put("IN_BATCH_DATE", sqlBillingDate);
	           inputParams.put("IN_PROCESSED_FLAG", ARConstants.COMMON_VALUE_NO);
	           //inputParams.put("IN_DISPATCHED_COUNT", "-1");
	           //inputParams.put("IN_PROCESSED_COUNT", "0");//TODO

	          /* logger.info("in_payment_method_id=" + paymentMethodId);
	           logger.info("in_batch_date=" + sqlBillingDate);
	           logger.info("in_processed_flag=" + ARConstants.COMMON_VALUE_NO);*/
	           
	           request.setInputParams(inputParams);
	           request.setStatementID(statement);

	           // get data
	           DataAccessUtil dau = DataAccessUtil.getInstance();
	           Map outputs = (Map) dau.execute(request);
	           request.reset();
	           batchNumber = outputs.get("OUT_BILLING_HEADER_ID").toString();
	           //logger.info("out_billing_header_id=" + batchNumber);
	           logger.debug("Exiting doPGCreateBillingHeader");
	       } finally {

	       }
	       
	        return batchNumber;
	    }
	 
	 public String doGetOrderGuid(String masterOrderNumber) throws Throwable
	    {
	        if(logger.isDebugEnabled()) {
	            logger.debug("Entering doGetOrderGuid");
	        }
	        String orderGuid = null;
	        try{
	            DataRequest dataRequest = new DataRequest();
	            dataRequest.setConnection(this.conn);
	            dataRequest.setStatementID("GET_ORDER_GUID");
	            dataRequest.addInputParam("IN_MASTER_ORDER_NUMBER", masterOrderNumber);
	            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	            
	            logger.info("master_order_number=" + masterOrderNumber);
	            orderGuid = ((String)dataAccessUtil.execute(dataRequest));
	            dataRequest.reset();
	            logger.debug("Exiting doGetOrderGuid");
	        } finally {
	        }
	        return orderGuid;
	    }
	 
	 public CachedResultSet doGetPGApPaymentInfo(EODPaymentVO pvo) throws SQLException,Exception
	    {
	        logger.debug("Entering doGetPGApPaymentInfo");
	        CachedResultSet rs = null;

	        try{
	            DataRequest dataRequest = new DataRequest();
	            dataRequest.setConnection(this.conn);
	            dataRequest.setStatementID("GET_PG_AP_PAYMENT_INFO");
	            
	            HashMap inputParams = new HashMap();
	            inputParams.put("IN_PAYMENT_ID", pvo.getPaymentId());
	            inputParams.put("IN_PAYMENT_INDICATOR", pvo.getPaymentInd());
	            
	            logger.debug("in_payment_id=" + pvo.getPaymentId());
	            logger.debug("in_payment_indicator" + pvo.getPaymentInd()); 
	            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	            dataRequest.setInputParams(inputParams);
	            
	            rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
	            dataRequest.reset();
	            
	        } finally {
	        	logger.debug("Exiting doGetPGApPaymentInfo");
	        } 
	        return  rs;
	    }

	 
	 public String createPGBillingDetail(PGEODRecordVO vo,long batchNumber) throws Exception{
			
			if(logger.isDebugEnabled()) {
	            logger.debug("Entering createPGBillingDetail");
	        }
	        String retValue = "";
	        
	        
	        try{
	            DataRequest dataRequest = new DataRequest();
	            dataRequest.setConnection(this.conn);
	            dataRequest.setStatementID("CREATE_BILLING_DETAIL_PG_PP");
	            dataRequest.addInputParam("IN_BILLING_HEADER_ID", batchNumber);
	            dataRequest.addInputParam("IN_ORDER_NUMBER", vo.getMasterOrderNumber());
	            dataRequest.addInputParam("IN_PAYMENT_ID", new Long(vo.getPaymentId()));
	            //Rounding should be done because of BigDecimal may give more than 2 decimals
	            dataRequest.addInputParam("IN_ORDER_AMOUNT", BigDecimal.valueOf(vo.getOrderAmount()).setScale(2, BigDecimal.ROUND_DOWN).doubleValue());
	            dataRequest.addInputParam("IN_TRANSACTION_TYPE", vo.getTransactionType());
	            dataRequest.addInputParam("IN_BILL_TYPE", vo.getBillType());
	            dataRequest.addInputParam("IN_AUTH_TRANSACTION_ID", vo.getAuthTranId());
	            dataRequest.addInputParam("IN_MERCHANT_REF_ID", vo.getMerchantRefId());
	            dataRequest.addInputParam("IN_SETTLEMENT_TRANSACTION_ID", vo.getSettlmentTransactionId());
	            dataRequest.addInputParam("IN_REFUND_TRANSACTION_ID", vo.getRefundTransactionId());
	            dataRequest.addInputParam("IN_REQUEST_ID",vo.getRequestToken());
	            
	            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	                    
	            Map outputs = (Map) dataAccessUtil.execute(dataRequest); 
	            dataRequest.reset();
	            String status = (String) outputs.get(ARConstants.STATUS_PARAM);
	            if(status != null && status.equals(ARConstants.COMMON_VALUE_NO))
	            {
	                String message = (String) outputs.get(ARConstants.MESSAGE_PARAM);
	                throw new SQLException(message);
	            }
	            if(logger.isDebugEnabled()){
	                logger.debug("createPGBillingDetail on Billing Detail PG PP successfull. Status=" 
	                    + status);
	            }
	            retValue = outputs.get("OUT_SEQUENCE_NUMBER").toString(); 
	            //logger.debug(" retValue "+ retValue);
	        }
	        finally {
	            if(logger.isDebugEnabled()){
	                logger.debug("Exiting createPGBillingDetail");
	            } 
	        } 

	        return retValue; 
			
		}

	public void doUpdatePGSettlementResponse(SettlementResponse settlementResponse, PGEODRecordVO pgEODRecordVO) throws Exception {
		
			if (logger.isDebugEnabled()) {
				logger.debug("Entering PayPal doUpdatePGSettlementResponse");
			}
			try {
				DataRequest dataRequest = new DataRequest();
				dataRequest.setConnection(this.conn);
				dataRequest.setStatementID("UPDATE_BILLING_DTL_PG_PP_RES");
				dataRequest.addInputParam("IN_AUTH_TRANS_ID", pgEODRecordVO.getAuthTranId());
				dataRequest.addInputParam("IN_ORDER_NUMBER", pgEODRecordVO.getMasterOrderNumber());
				dataRequest.addInputParam("IN_BILLING_DETAIL_ID", pgEODRecordVO.getBillingDetailId());
				dataRequest.addInputParam("IN_PAYMENT_ID", pgEODRecordVO.getPaymentId());
				dataRequest.addInputParam("IN_REQUEST_ID", settlementResponse.getRequestId());
				
				if(pgEODRecordVO.getTransactionType().equals("1"))
				{	
					dataRequest.addInputParam("IN_SETTLEMENT_TRANSACTION_ID", settlementResponse.getSettlementTransactionId());
				}	
				else
				{	
					dataRequest.addInputParam("IN_SETTLEMENT_TRANSACTION_ID", pgEODRecordVO.getSettlmentTransactionId()); //Existing Settlement ID
					dataRequest.addInputParam("IN_REFUND_TRANSACTION_ID", settlementResponse.getSettlementTransactionId());//Refund Transaction ID
				}	
				
				dataRequest.addInputParam("IN_RESPONSE_CODE", settlementResponse.getReasonCode());
				dataRequest.addInputParam("IN_RESPONSE_MESSAGE", settlementResponse.getMessage());
				dataRequest.addInputParam("IN_STATUS", settlementResponse.getStatus());
				dataRequest.addInputParam("IN_SETTLEMENT_DATE",settlementResponse.getSettlementDate());
				dataRequest.addInputParam("IN_PROCESS_ACCOUNT",settlementResponse.getProcessorAccount());
				
				DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

				Map outputs = (Map) dataAccessUtil.execute(dataRequest);
				dataRequest.reset();
				String status = (String) outputs.get(ARConstants.STATUS_PARAM);
				if (status != null && status.equals(ARConstants.COMMON_VALUE_NO)) {
					String message = (String) outputs.get(ARConstants.MESSAGE_PARAM);
					throw new SQLException(message);
				}
				if (logger.isDebugEnabled()) {
					logger.debug("Updated PayPal  doUpdatePGSettlementResponse Details. Status=" + status);
				}

			} finally {
				if (logger.isDebugEnabled()) {
					logger.debug("Exiting PayPal doUpdatePGSettlementResponse");
				}
			}
		}

	public long getPGBillingDetailID(PGEODMessageVO mvo) throws Exception {

		long retValue = 0;
		if (logger.isDebugEnabled()) {
			logger.debug("Entering getPGBillingDetail");
		}

		try {
			DataRequest dataRequest = new DataRequest();
			dataRequest.setConnection(this.conn);
			dataRequest.setStatementID("GET_BILLING_DETAILS_PP_PG");
			dataRequest.addInputParam("IN_BILLING_DETAIL_ID", mvo.getBillingDetailId());
			dataRequest.addInputParam("IN_BILLING_HEADER_ID", mvo.getBatchNumber());
			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			retValue = ((BigDecimal) dataAccessUtil.execute(dataRequest)).longValue();
			//logger.debug("retValue" + retValue);
			dataRequest.reset();

		} finally {
			if (logger.isDebugEnabled()) {
				logger.debug("Exiting getPGBillingDetailID");
			}
		}
		return retValue;

	}

	   /**
	    * This method updates the status of the Bill to 'BILLED'
	    * @param payment_id long
	    * @param time_stamp long
	    * @param status String
	    * @throws java.lang.Exception
	    */
	    public boolean doUpdatePaymentBillStatus(long payment_id, long time_stamp, String upstatus) throws Exception
	    {
	        if(logger.isDebugEnabled()) {
	            logger.debug("Entering doUpdatePaymentBillStatus");
	        }
	        String status = "";
	        try{
	            DataRequest dataRequest = new DataRequest();
	            dataRequest.setConnection(this.conn);
	            dataRequest.setStatementID("UPDATE_BILL_REFUND_STATUS");
	            dataRequest.addInputParam("IN_PAYMENT_ID", new Long(payment_id).toString());
	            //dataRequest.addInputParam("IN_REFUND_ID", null);
	            dataRequest.addInputParam("IN_TIMESTAMP", new Timestamp(time_stamp));
	            dataRequest.addInputParam("IN_STATUS", upstatus);
	            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	            
	            logger.debug("Calling UPDATE_BILL_REFUND_STATUS: " + payment_id);
	            Map outputs = (Map) dataAccessUtil.execute(dataRequest);      
	            dataRequest.reset();
	            status = (String) outputs.get(ARConstants.STATUS_PARAM);
	            if(status != null && status.equals(ARConstants.COMMON_VALUE_NO))
	            {
	                String message = (String) outputs.get(ARConstants.MESSAGE_PARAM);
	                throw new SQLException(message);
	            }
	            logger.debug("Update Bill payment status  successfull. Status="+status);

	        } finally {
	            if(logger.isDebugEnabled()){
	                logger.debug("Exiting doUpdatePaymentBillStatus");
	            } 
	        } 
	        return (status).equals(ARConstants.COMMON_VALUE_YES)? true:false; 
	    }
	    /**
	     * This method updates the status of the Bill to 'BILLED'
	     * @param refund_id long
	     * @param time_stamp long
	     * @param status String
	     * @throws java.lang.Exception
	     */
	     public boolean doUpdateRefundBillStatus(long refund_id, long time_stamp, String upstatus) throws Exception
	     {
	         if(logger.isDebugEnabled()) {
	             logger.debug("Entering doUpdateRefundBillStatus");
	         }
	         String status = "";
	         try{
	             DataRequest dataRequest = new DataRequest();
	             dataRequest.setConnection(this.conn);
	             dataRequest.setStatementID("UPDATE_BILL_REFUND_STATUS");
	             //dataRequest.addInputParam("IN_PAYMENT_ID", null);
	             dataRequest.addInputParam("IN_REFUND_ID", new Long(refund_id).toString());
	             dataRequest.addInputParam("IN_TIMESTAMP", new Timestamp(time_stamp));
	             dataRequest.addInputParam("IN_STATUS", upstatus);
	             DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	             
	             logger.debug("Calling UPDATE_BILL_REFUND_STATUS: " + refund_id);
	             Map outputs = (Map) dataAccessUtil.execute(dataRequest);     
	             dataRequest.reset();
	             status = (String) outputs.get(ARConstants.STATUS_PARAM);
	             if(status != null && status.equals(ARConstants.COMMON_VALUE_NO))
	             {
	                 String message = (String) outputs.get(ARConstants.MESSAGE_PARAM);
	                 throw new SQLException(message);
	             }
	             if(logger.isDebugEnabled()){
	                 logger.debug("Update Bill Refund status  successfull. Status=" 
	                     + status);
	             }

	         } finally {
	             if(logger.isDebugEnabled()){
	                 logger.debug("Exiting doUpdateRefundBillStatus");
	             } 
	         } 
	         return (status).equals(ARConstants.COMMON_VALUE_YES)? true:false; 
	     }
		
	     /**
		    * This method calls CLEAN.XXX_PKG.SET_PROCESSED_INDICATOR to set the 
		    * completion_status of the billing_header.
		    * @param batchNumber
		    * @param status
		    * @throws java.lang.Exception
		    */
		    public void doUpdatePGProcessedIndicator(String batchNumbers, String inStatus) throws Exception
		    {
		        if(logger.isDebugEnabled()) {
		            logger.debug("Entering doUpdatePGProcessedIndicator");
		        }
		        try{
		            DataRequest dataRequest = new DataRequest();
		            dataRequest.setConnection(this.conn);
		            dataRequest.setStatementID("UPDATE_BILLING_HEADER_PG");
		            dataRequest.addInputParam("IN_BILLING_HEADER_IDS", batchNumbers);
		            dataRequest.addInputParam("IN_PROCESSED_INDICATOR", inStatus);
		            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
		            
		            logger.debug("Calling UPDATE_BILLING_HEADER_PG: " + batchNumbers);
		            Map outputs = (Map) dataAccessUtil.execute(dataRequest);     
		            dataRequest.reset();
		            String status = (String) outputs.get(ARConstants.STATUS_PARAM);
		            if(status != null && status.equals(ARConstants.COMMON_VALUE_NO))
		            {
		                String message = (String) outputs.get(ARConstants.MESSAGE_PARAM);
		                throw new SQLException(message);
		            }
		            if(logger.isDebugEnabled()){
		                logger.debug("Updated doUpdatePGProcessedIndicator Details. Status="+status);  
		            }
		        } finally {
		            if(logger.isDebugEnabled()){
		                logger.debug("Exiting doUpdatePGProcessedIndicator");
		            } 
		        }
		    }
	     
			public void doUpdatePGDispatchedCount(long batchNumber, int dispatchedCount) throws Throwable {
				logger.debug("Entering doUpdateDispatchedCount");

				DataRequest request = new DataRequest();
				String status = "";
				try {
					/* setup store procedure input parameters */
					HashMap inputParams = new HashMap();
					inputParams.put("IN_BILLING_HEADER_ID", String.valueOf(batchNumber));
					inputParams.put("IN_DISPATCHED_COUNT", String.valueOf(dispatchedCount));

					logger.debug("billing_header_id=" + batchNumber);
					logger.debug("dispatched_count=" + dispatchedCount);
					// build DataRequest object
					request.setConnection(this.conn);

					request.setInputParams(inputParams);
					request.setStatementID("UPDATE_PG_DISPATCHED_COUNT");

					// get data
					DataAccessUtil dau = DataAccessUtil.getInstance();
					Map outputs = (Map) dau.execute(request);
					request.reset();
					status = (String) outputs.get("OUT_STATUS");
					if (ARConstants.COMMON_VALUE_NO.equals(status)) {
						throw new SQLException((String) outputs.get("OUT_MESSAGE"));
					}
					logger.debug("Exiting doUpdateDispatchedCount");
				} finally {
				}
			}

			public CachedResultSet doGetBillingPPDetailsPG(long billingId, long headerId) throws Throwable{

				if (logger.isDebugEnabled()) {
					logger.debug("Entering doGetBillingPPDetailsPG");
				}
				CachedResultSet rs = null;
				try {
					DataRequest dataRequest = new DataRequest();
					dataRequest.setConnection(this.conn);
					dataRequest.setStatementID("GET_BILLING_DET_PG_PP_BY_ID");
					dataRequest.addInputParam("IN_BILLING_DETAIL_ID", billingId);
					dataRequest.addInputParam("IN_BILLING_HEADER_ID", headerId);
					DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
					rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
					dataRequest.reset();
					
				} finally {
					if (logger.isDebugEnabled()) {
						logger.debug("Exiting doGetBillingCCDetailsPG");
					}
				}
				return rs;
			
			}
			
			public List<PGEODRecordVO> getFailedPPBillingDetailsList(long billingHeaderId,String paymentType) throws IOException, SQLException, ParserConfigurationException, SAXException {
				
				if (logger.isDebugEnabled()) {
					logger.debug("Entering getFailedPPBillingDetailsIds");
				}
				 CachedResultSet ccbillingRecords = null;
				 List<PGEODRecordVO> billingDetList= new ArrayList<PGEODRecordVO>(); 
				 PGEODRecordVO pgEODRec = null;
				try {
					DataRequest dataRequest = new DataRequest();
					dataRequest.setConnection(this.conn);
					dataRequest.setStatementID("GET_FAILED_BILLING_DETAILS_PP");
					dataRequest.addInputParam("IN_BILLING_HEADER_ID", billingHeaderId);
					dataRequest.addInputParam("IN_PAYMENT_TYPE", paymentType);
					DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
					ccbillingRecords = (CachedResultSet)dataAccessUtil.execute(dataRequest);
					dataRequest.reset();
					
					while(ccbillingRecords.next())
					{
						pgEODRec = new PGEODRecordVO();
						pgEODRec.setBillingDetailId(ccbillingRecords.getLong("billing_detail_id"));
						pgEODRec.setBillingHeaderId(ccbillingRecords.getLong("billing_header_id"));
						pgEODRec.setStatus(ccbillingRecords.getString("status"));
						pgEODRec.setMasterOrderNumber(ccbillingRecords.getString("ORDER_NUMBER"));
						pgEODRec.setBillType(ccbillingRecords.getString("BILL_TYPE"));
						pgEODRec.setTransactionType(ccbillingRecords.getString("TRANSACTION_TYPE"));
						pgEODRec.setPaymentId(ccbillingRecords.getLong("PAYMENT_ID")); 
						billingDetList.add(pgEODRec);
					}	

				} finally {
					if (logger.isDebugEnabled()) {
						logger.debug("Exiting getFailedPPBillingDetailsIds");
					}
				}
				return billingDetList;
			}

			public void updateHeaderIndicator(long batchNumber,String paymentType)	throws Throwable {
				logger.debug("Entering updateHeaderIndicator");

				DataRequest request = new DataRequest();
				String status = "";
				try {
					
					HashMap inputParams = new HashMap();
					inputParams.put("IN_BILLING_HEADER_ID", String.valueOf(batchNumber));
					inputParams.put("IN_PAYMENT_TYPE", paymentType);

					
					request.setConnection(this.conn);

					request.setInputParams(inputParams);
					request.setStatementID("PG_UPDATE_HEADER_IND");

					
					DataAccessUtil dau = DataAccessUtil.getInstance();
					Map outputs = (Map) dau.execute(request);
					request.reset();
					status = (String) outputs.get("OUT_STATUS");
					if (ARConstants.COMMON_VALUE_NO.equals(status)) {
						throw new SQLException((String) outputs.get("OUT_MESSAGE"));
					}
					logger.debug("Exiting updateHeaderIndicator");
				} finally {
				}
		}

			public boolean checkAndIncrementProcessedCount(long batchNumber) throws SQLException,Exception {
				logger.debug("Entering checkAndIncrementProcessedCount");

				DataRequest request = new DataRequest();
				String status = "";
				boolean allProcessedFlag = false;
				String doneProcessingStr = "N";
				try {
					
					HashMap inputParams = new HashMap();
					inputParams.put("IN_BILLING_HEADER_ID", String.valueOf(batchNumber));

					logger.debug("billing_header_id=" + batchNumber);
					
					request.setConnection(this.conn);

					request.setInputParams(inputParams);
					request.setStatementID("PG_INCR_AND_CHECK_PROC_COUNT");

					
					DataAccessUtil dau = DataAccessUtil.getInstance();
					Map outputs = (Map) dau.execute(request);
					request.reset();
					status = (String) outputs.get("OUT_STATUS");
					doneProcessingStr = (String) outputs.get("OUT_IS_EQUAL");
					if("Y".equals(doneProcessingStr))
					{
						allProcessedFlag = true;
					}	
					if (ARConstants.COMMON_VALUE_NO.equals(status)) {
						throw new SQLException((String) outputs.get("OUT_MESSAGE"));
					}
					logger.debug("Exiting checkAndIncrementProcessedCount ::"+allProcessedFlag);
				} finally {
				}
				
				return allProcessedFlag;
			}

			
			
			
}	

