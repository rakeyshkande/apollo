package com.ftd.accountingreporting.altpay.dao;

import com.ftd.accountingreporting.altpay.vo.AltPayBillingDetailVOBase;
import com.ftd.accountingreporting.altpay.vo.AltPayProfileVOBase;
import com.ftd.accountingreporting.altpay.vo.AltPayReportDataVO;
import com.ftd.accountingreporting.altpay.vo.PayPalBillingDetailVO;
import com.ftd.accountingreporting.altpay.vo.BillMeLaterBillingDetailVO;
import com.ftd.accountingreporting.altpay.vo.GiftCardBillingDetailVO;
import com.ftd.accountingreporting.altpay.vo.UABillingDetailVO;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.constant.AltPayConstants;
import com.ftd.accountingreporting.util.AccountingUtil;
import com.ftd.accountingreporting.util.EODUtil;
import com.ftd.accountingreporting.vo.EODPaymentVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;
import java.math.BigDecimal;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;


public class AltPayDAO 
{
    private static Logger logger = 
        new Logger("com.ftd.accountingreporting.altpay.dao.AltPayDAO");
        
    public Map doGetAltPayEODRecords(Connection con, AltPayProfileVOBase pvo) throws Throwable
    {
        logger.info("Entering doGetAltPayEODRecords");
        
        AccountingUtil au = new AccountingUtil();
        int runDateOffset = Integer.parseInt((String)(pvo.getConfigsMap().get(AltPayConstants.AP_EOD_RUNDATE_OFFSET))); 
        Date billingDate = au.calculateOffSetDate(runDateOffset);
        
        CachedResultSet prs = null;
        CachedResultSet rrs = null;
        String masterOrderNumber = null;
        String paymentId = null;
        String paymentInd = null;
        EODPaymentVO paymentvo = null;
        ArrayList paymentList = new ArrayList();
        ArrayList refundList = new ArrayList();
        Map outmap = null;
        Map cartMap = new HashMap();
        int paymentCount = 0;
        int refundCount = 0;
        
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.setStatementID(pvo.getStmtGetEODRecords());
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            
            HashMap inputParams = new HashMap();
            java.sql.Date sqlBillingDate = 
                        new java.sql.Date(billingDate.getTime());
                        
            inputParams.put("IN_DATE", sqlBillingDate);
            inputParams.put("IN_PAYMENT_METHOD_ID", pvo.getPaymentMethodId());
            logger.info("in_date="+sqlBillingDate);
            dataRequest.setInputParams(inputParams);
            
            outmap = (Map)dataAccessUtil.execute(dataRequest);
            dataRequest.reset();
            prs = (CachedResultSet)outmap.get("OUT_PAYMENT_CUR");
            rrs = (CachedResultSet)outmap.get("OUT_REFUND_CUR");
            
            if(prs != null) {
                while(prs.next()) {
                    masterOrderNumber = prs.getString("master_order_number");
                    paymentId = prs.getString("payment_id");
                    paymentInd = prs.getString("payment_indicator");
                    paymentvo = new EODPaymentVO(masterOrderNumber,paymentId,paymentInd);
                    paymentList.add(paymentvo);
                    paymentCount++;
                }
            }
            if (rrs != null) {
                while(rrs.next()) {
                    masterOrderNumber = rrs.getString("master_order_number");
                    paymentId = rrs.getString("payment_id");
                    paymentInd = rrs.getString("payment_indicator");
                    paymentvo = new EODPaymentVO(masterOrderNumber, paymentId,paymentInd);
                    refundList.add(paymentvo);
                    refundCount++;
                }   
            }
            
            cartMap.put("PAYMENT", paymentList);
            cartMap.put("REFUND", refundList);
            logger.info("Found " + paymentCount + " payments and " + refundCount + " refunds");
            logger.info("Exiting doGetAltPayEODRecords");
        } finally {
        } 
        return  cartMap;
    }        
        

        
    public String doCreateBillingHeader(Connection con, AltPayProfileVOBase pvo)
            throws Throwable
    {
      
        logger.debug("Entering doCreateBillingHeader");
           
        AccountingUtil au = new AccountingUtil();
        int runDateOffset = Integer.parseInt((String)(pvo.getConfigsMap().get(AltPayConstants.AP_EOD_RUNDATE_OFFSET)));
        Date billingDate = au.calculateOffSetDate(runDateOffset);
        String statement = pvo.getStmtInsertBillingHeader();
        String paymentMethodId  = pvo.getPaymentMethodId();
       
        DataRequest request = new DataRequest();

       String batchNumber = "";
       
       try {
           // setup store procedure input parameters 
           HashMap inputParams = new HashMap();
           java.sql.Date sqlBillingDate = 
                       new java.sql.Date(billingDate.getTime());
                       
           inputParams.put("IN_PAYMENT_METHOD_ID", paymentMethodId);
           inputParams.put("IN_BATCH_DATE", sqlBillingDate);
           inputParams.put("IN_PROCESSED_FLAG", ARConstants.COMMON_VALUE_NO);
           inputParams.put("IN_DISPATCHED_COUNT", "-1");
           inputParams.put("IN_PROCESSED_COUNT", "0");

           logger.info("in_payment_method_id=" + paymentMethodId);
           logger.info("in_batch_date=" + sqlBillingDate);
           logger.info("in_processed_flag=" + ARConstants.COMMON_VALUE_NO);
           // build DataRequest object
           request.setConnection(con);
           
           request.setInputParams(inputParams);
           request.setStatementID(statement);

           // get data
           DataAccessUtil dau = DataAccessUtil.getInstance();
           Map outputs = (Map) dau.execute(request);
           request.reset();
           batchNumber = outputs.get("OUT_BILLING_HEADER_ID").toString();
           logger.info("out_billing_header_id=" + batchNumber);
           logger.debug("Exiting doCreateBillingHeader");
       } finally {

       }
       
        return batchNumber;
    }                  

    public void doCreateBillingDetailPP(Connection con, 
                                          AltPayProfileVOBase pvo,
                                          AltPayBillingDetailVOBase bvo)
       throws Throwable
    {

        logger.debug("Entering doCreateBillingDetailPP");
        PayPalBillingDetailVO ppvo = (PayPalBillingDetailVO)bvo;
        
        DataRequest request = new DataRequest();
        String status = "";
        try {
           /* setup store procedure input parameters */
           HashMap inputParams = new HashMap();
           inputParams.put("IN_BILLING_HEADER_ID", ppvo.getBillingHeaderId()); 
           inputParams.put("IN_PAYMENT_ID", ppvo.getPaymentId()); 
           inputParams.put("IN_PAYMENT_CODE", ppvo.getPaymentCode());
           inputParams.put("IN_MASTER_ORDER_NUMBER", ppvo.getMasterOrderNumber());
           inputParams.put("IN_REQ_TRANSACTION_TXT", ppvo.getReqTransactionId());
           inputParams.put("IN_REQ_AMT", ppvo.getReqAmt().toString());
    
           logger.info("in_billing_header_id=" + ppvo.getBillingHeaderId());
           logger.info("in_payment_id=" + ppvo.getPaymentId());
           logger.info("in_payment_code=" + ppvo.getPaymentCode());
           logger.info("in_master_order_number=" + ppvo.getMasterOrderNumber());
           logger.info("in_req_transaction_txt=" + ppvo.getReqTransactionId());
           logger.info("in_req_amt=" + ppvo.getReqAmt().toString());
           
           // build DataRequest object
           request.setConnection(con);
           request.setInputParams(inputParams);
           request.setStatementID(pvo.getStmtInsertBillingDetail());

           // get data
           DataAccessUtil dau = DataAccessUtil.getInstance();
           Map outputs = (Map) dau.execute(request);
           request.reset();
           status = (String) outputs.get("OUT_STATUS");
           if(!status.equals(ARConstants.COMMON_VALUE_YES))
           {
               String errorMessage = (String) outputs.get("OUT_MESSAGE");
               throw new Exception(errorMessage);
           }
           logger.debug("Exiting doCreateBillingDetail");
           bvo.setBillingDetailId(outputs.get("OUT_BILLING_DETAIL_ID").toString());
           
       } finally {
       }
       
    }
    
    public void doUpdateBillingDetailPP(Connection con, 
                                        AltPayProfileVOBase pvo,
                                        AltPayBillingDetailVOBase bvo)
       throws Throwable
    {
     
        logger.debug("Entering doUpdateBillingDetailPP");
           
        PayPalBillingDetailVO ppvo = (PayPalBillingDetailVO)bvo;
        DataRequest request = new DataRequest();
        String status = "";
        try {
           /* setup store procedure input parameters */
           HashMap inputParams = new HashMap();
           inputParams.put("IN_BILLING_DETAIL_ID", ppvo.getBillingDetailId());
           inputParams.put("IN_RES_ACK_TXT", ppvo.getResAck());
           inputParams.put("IN_RES_TRANSACTION_TXT", ppvo.getResTransactionId());
           inputParams.put("IN_RES_CORRELATION_TXT", ppvo.getResCorrelationId());
           inputParams.put("IN_ACK_ERROR_TXT", ppvo.getResErrorMessage());
           inputParams.put("IN_RES_GROSS_AMT", ppvo.getResGrossAmt().toString());
           inputParams.put("IN_RES_FEE_AMT", ppvo.getResFeeAmt().toString());
           inputParams.put("IN_RES_NET_AMT", ppvo.getResNetAmt().toString());

           logger.info("IN_BILLING_DETAIL_ID=" + ppvo.getBillingDetailId());
           logger.info("IN_RES_ACK_TXT=" + ppvo.getResAck());
           logger.info("IN_RES_TRANSACTION_TXT=" + ppvo.getResTransactionId());
           logger.info("IN_RES_CORRELATION_TXT=" + ppvo.getResCorrelationId());
           logger.info("IN_ACK_ERROR_TXT=" + ppvo.getResErrorMessage());
           logger.info("IN_RES_GROSS_AMT=" + ppvo.getResGrossAmt().toString());
           logger.info("IN_RES_FEE_AMT=" + ppvo.getResFeeAmt().toString());
           logger.info("IN_RES_NET_AMT=" + ppvo.getResNetAmt().toString());
           // build DataRequest object
           request.setConnection(con);
           
           request.setInputParams(inputParams);
           request.setStatementID(pvo.getStmtUpdateBillingDetail());

           // get data
           DataAccessUtil dau = DataAccessUtil.getInstance();
           Map outputs = (Map) dau.execute(request);
           request.reset();
           status = (String) outputs.get("OUT_STATUS");
           if (ARConstants.COMMON_VALUE_NO.equals(status)) {
               throw new SQLException((String)outputs.get("OUT_MESSAGE"));
           }
           logger.debug("Exiting doUpdateBillingDetailPP");
       } finally {
       }

    }

  /**
    * Creates billing detail record for BillMeLater
    */
    public void doCreateBillingDetailBM(Connection con, 
                                          AltPayProfileVOBase pvo,
                                          AltPayBillingDetailVOBase bvo)
       throws Throwable
    {

        logger.debug("Entering doCreateBillingDetailBM");
        BillMeLaterBillingDetailVO ppvo = (BillMeLaterBillingDetailVO)bvo;
        
        DataRequest request = new DataRequest();
        String status = "";
        try {
           /* setup store procedure input parameters */
           HashMap inputParams = new HashMap();
           inputParams.put("IN_BILLING_HEADER_ID", ppvo.getBillingHeaderId()); 
           inputParams.put("IN_PAYMENT_ID", ppvo.getPaymentId()); 
           inputParams.put("IN_PAYMENT_CODE", ppvo.getPaymentCode());
           inputParams.put("IN_MASTER_ORDER_NUMBER", ppvo.getMasterOrderNumber());
           inputParams.put("IN_ACCOUNT_NUMBER", ppvo.getAccountNumber());
           inputParams.put("IN_AUTH_NUMBER", ppvo.getAuthNumber());
           inputParams.put("IN_AUTH_ORDER_NUMBER", ppvo.getAuthOrderNumber());
           inputParams.put("IN_SETTLEMENT_AMT", ppvo.getReqAmt().toString());
           Date authDate = ppvo.getAuthDate().getTime();
           java.sql.Date sqlAuthDate = new java.sql.Date(authDate.getTime());
           inputParams.put("IN_AUTH_DATE", sqlAuthDate);
    
           logger.info("in_billing_header_id=" + ppvo.getBillingHeaderId());
           logger.info("in_payment_id=" + ppvo.getPaymentId());
           logger.info("in_payment_code=" + ppvo.getPaymentCode());
           logger.info("in_master_order_number=" + ppvo.getMasterOrderNumber());
           logger.info("in_auth_number=" + ppvo.getAuthNumber());
           logger.info("in_req_amt=" + ppvo.getReqAmt().toString());
           
           // build DataRequest object
           request.setConnection(con);
           request.setInputParams(inputParams);
           request.setStatementID(pvo.getStmtInsertBillingDetail());

           // get data
           DataAccessUtil dau = DataAccessUtil.getInstance();
           Map outputs = (Map) dau.execute(request);
           request.reset();
           status = (String) outputs.get("OUT_STATUS");
           if(!status.equals(ARConstants.COMMON_VALUE_YES))
           {
               String errorMessage = (String) outputs.get("OUT_MESSAGE");
               throw new Exception(errorMessage);
           }
           logger.debug("Exiting doCreateBillingDetailBM");
           bvo.setBillingDetailId(outputs.get("OUT_BILLING_DETAIL_ID").toString());
           
       } finally {
       }
       
    }


    /**
      * Creates billing detail record for Gift Card
      */
      public void doCreateBillingDetailGD(Connection con, 
                                            AltPayProfileVOBase pvo,
                                            AltPayBillingDetailVOBase bvo)
         throws Throwable
      {

          logger.debug("Entering doCreateBillingDetailGD");
          GiftCardBillingDetailVO ppvo = (GiftCardBillingDetailVO)bvo;
          
          DataRequest request = new DataRequest();
          String status = "";
          java.sql.Date sqlAuthDate = null;
          
          try {
             /* setup store procedure input parameters */
             HashMap inputParams = new HashMap();
             inputParams.put("IN_BILLING_HEADER_ID", ppvo.getBillingHeaderId()); 
             inputParams.put("IN_PAYMENT_ID", ppvo.getPaymentId()); 
             inputParams.put("IN_PAYMENT_CODE", ppvo.getPaymentCode());
             inputParams.put("IN_MASTER_ORDER_NUMBER", ppvo.getMasterOrderNumber());
             inputParams.put("IN_ACCOUNT_NUMBER", ppvo.getAccountNumber());
             inputParams.put("IN_PIN", ppvo.getPin());
             inputParams.put("IN_AUTH_NUMBER", ppvo.getAuthNumber());
             inputParams.put("IN_SETTLEMENT_AMT", ppvo.getReqAmt().toString());
             if (ppvo.getAuthDate() != null) {
            	 Date authDate = ppvo.getAuthDate().getTime();
                 sqlAuthDate = new java.sql.Date(authDate.getTime());
             }
             inputParams.put("IN_AUTH_DATE", sqlAuthDate);
             inputParams.put("IN_STATUS", ppvo.getStatus());
             inputParams.put("IN_ERROR_TXT", ppvo.getErrorTxt());
      
             logger.info("in_billing_header_id=" + ppvo.getBillingHeaderId());
             logger.info("in_payment_id=" + ppvo.getPaymentId());
             logger.info("in_payment_code=" + ppvo.getPaymentCode());
             logger.info("in_master_order_number=" + ppvo.getMasterOrderNumber());
             logger.info("in_auth_number=" + ppvo.getAuthNumber());
             logger.info("in_req_amt=" + ppvo.getReqAmt().toString());
             logger.info("in_status and in_error_txt =" + ppvo.getStatus() + " " + ppvo.getErrorTxt());
             
             // build DataRequest object
             request.setConnection(con);
             request.setInputParams(inputParams);
             request.setStatementID(pvo.getStmtInsertBillingDetail());

             // get data
             DataAccessUtil dau = DataAccessUtil.getInstance();
             Map outputs = (Map) dau.execute(request);
             request.reset();
             status = (String) outputs.get("OUT_STATUS");
             if(!status.equals(ARConstants.COMMON_VALUE_YES))
             {
                 String errorMessage = (String) outputs.get("OUT_MESSAGE");
                 throw new Exception(errorMessage);
             }
             logger.debug("Exiting doCreateBillingDetailGD");
             bvo.setBillingDetailId(outputs.get("OUT_BILLING_DETAIL_ID").toString());
             
         } finally {
         }
         
      }

      
  /**
    * Gets unprocessed billing detail records for BillMeLater
    */
    public List doGetBillingDetailBM(Connection con)
       throws Throwable
    {
        logger.debug("Entering doGetBillingDetailBM");
        DataRequest request = new DataRequest();
        ArrayList billingDetailVOs = new ArrayList();
        try {
          request.setConnection(con);
          request.reset();
          request.setStatementID("GET_AP_BILLING_DETAIL_BM");
          DataAccessUtil dau = DataAccessUtil.getInstance();
          CachedResultSet outputs = (CachedResultSet) dau.execute(request);
          while (outputs.next()) {
            BillMeLaterBillingDetailVO bvo = new BillMeLaterBillingDetailVO();
            // Note we don't set billingDetailId - this is because the query 
            // doesn't include it.  Since there may have been a failure in prior
            // EOD runs there may be multiple detail records representing the 
            // same orders - so by doing a "select distinct" without billingDetailId
            // we won't get duplicates.
            bvo.setBillingHeaderId(outputs.getString("alt_pay_billing_header_id"));
            bvo.setPaymentId(outputs.getString("payment_id"));
            bvo.setPaymentCode(outputs.getString("payment_code"));
            bvo.setMasterOrderNumber(outputs.getString("master_order_number"));
            bvo.setAccountNumber(outputs.getString("account_number"));
            bvo.setAuthNumber(outputs.getString("auth_number"));
            bvo.setAuthOrderNumber(outputs.getString("auth_order_number"));
            bvo.setReqAmt(new BigDecimal(outputs.getString("settlement_amt")));
            bvo.setPaymentRecAmt(bvo.getReqAmt());
            if (outputs.getDate("auth_date") != null) {
            	bvo.setAuthDate(EODUtil.convertSQLDate(outputs.getDate("auth_date")));
            }
            billingDetailVOs.add(bvo);
          }
       } finally {
           logger.debug("Exiting goGetBillingDetailBM");
       }
       return billingDetailVOs;       
    }

    
    /**
      * Gets unprocessed billing detail records for Gift Card
      */
      public List doGetBillingDetailGD(Connection con)
         throws Throwable
      {
          logger.debug("Entering doGetBillingDetailGD");
          DataRequest request = new DataRequest();
          ArrayList billingDetailVOs = new ArrayList();
          try {
            request.setConnection(con);
            request.reset();
            request.setStatementID("GET_AP_BILLING_DETAIL_GD");
            DataAccessUtil dau = DataAccessUtil.getInstance();
            CachedResultSet outputs = (CachedResultSet) dau.execute(request);
            while (outputs.next()) {
              GiftCardBillingDetailVO bvo = new GiftCardBillingDetailVO();
              // Note we don't set billingDetailId - this is because the query 
              // doesn't include it.  Since there may have been a failure in prior
              // EOD runs there may be multiple detail records representing the 
              // same orders - so by doing a "select distinct" without billingDetailId
              // we won't get duplicates.
              bvo.setBillingHeaderId(outputs.getString("alt_pay_billing_header_id"));
              bvo.setPaymentId(outputs.getString("payment_id"));
              bvo.setPaymentCode(outputs.getString("payment_code"));
              bvo.setMasterOrderNumber(outputs.getString("master_order_number"));
              bvo.setAccountNumber(outputs.getString("account_number"));
              bvo.setPin(outputs.getString("pin"));
              bvo.setAuthNumber(outputs.getString("auth_number"));
              bvo.setReqAmt(new BigDecimal(outputs.getString("settlement_amt")));
              bvo.setPaymentRecAmt(bvo.getReqAmt());
              bvo.setAuthDate(EODUtil.convertSQLDate(outputs.getDate("auth_date")));
              bvo.setStatus(outputs.getString("status"));
              bvo.setErrorTxt(outputs.getString("error_txt"));
              billingDetailVOs.add(bvo);
            }
         } finally {
             logger.debug("Exiting goGetBillingDetailGD");
         }
         return billingDetailVOs;       
      }
    
      
    /**
      * Gets unprocessed billing detail records for United
      */
      public List<UABillingDetailVO> doGetBillingDetailUA(Connection con)
         throws Throwable
      {
          logger.debug("Entering doGetBillingDetailUA");
          DataRequest request = new DataRequest();
          ArrayList<UABillingDetailVO> billingDetailVOs = new ArrayList<UABillingDetailVO>();
          try {
            request.setConnection(con);
            request.reset();
            request.setStatementID("GET_AP_BILLING_DETAIL_UA");
            DataAccessUtil dau = DataAccessUtil.getInstance();
            CachedResultSet outputs = (CachedResultSet) dau.execute(request);
            while (outputs.next()) {
              UABillingDetailVO bvo = new UABillingDetailVO();
              bvo.setMembershipNumber(outputs.getString("account_number"));
              bvo.setReqRedemptionDate(outputs.getDate("req_redemption_date"));
              bvo.setReqMilesAmt(outputs.getInt("req_miles_amt"));
              bvo.setMemberLastName(outputs.getString("last_name"));
              bvo.setExternalOrderNumber(outputs.getString("external_order_number"));
              billingDetailVOs.add(bvo);
            }
         } finally {
             logger.debug("Exiting goGetBillingDetailUA");
         }
         return billingDetailVOs;       
      }    

    public CachedResultSet doGetApPaymentInfo(Connection con, EODPaymentVO pvo) throws Throwable
    {
        logger.debug("Entering doGetApPaymentInfo");
        CachedResultSet rs = null;

        //Map outmap = null;

        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.setStatementID("GET_AP_PAYMENT_INFO");
            
            HashMap inputParams = new HashMap();
            inputParams.put("IN_PAYMENT_ID", pvo.getPaymentId());
            inputParams.put("IN_PAYMENT_INDICATOR", pvo.getPaymentInd());
            
            logger.info("in_payment_id=" + pvo.getPaymentId());
            logger.info("in_payment_indicator" + pvo.getPaymentInd()); 
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            dataRequest.setInputParams(inputParams);
            
            rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
            dataRequest.reset();
            logger.debug("Exiting doGetApPaymentInfo");
        } finally {

        } 
        return  rs;
    }    
    
    public void doUpdateDispatchedCount(Connection con, 
                                        long batchNumber,
                                        int dispatchedCount)
       throws Throwable
    {
        logger.debug("Entering doUpdateDispatchedCount");

        DataRequest request = new DataRequest();
        String status = "";
        try {
           /* setup store procedure input parameters */
           HashMap inputParams = new HashMap();
           inputParams.put("IN_BILLING_HEADER_ID", String.valueOf(batchNumber));
           inputParams.put("IN_DISPATCHED_COUNT", String.valueOf(dispatchedCount));

           logger.info("billing_header_id=" + batchNumber);
           logger.info("dispatched_count=" + dispatchedCount);
           // build DataRequest object
           request.setConnection(con);
           
           request.setInputParams(inputParams);
           request.setStatementID("UPDATE_DISPATCHED_COUNT");

           // get data
           DataAccessUtil dau = DataAccessUtil.getInstance();
           Map outputs = (Map) dau.execute(request);
           request.reset();
           status = (String) outputs.get("OUT_STATUS");
           if (ARConstants.COMMON_VALUE_NO.equals(status)) {
               throw new SQLException((String)outputs.get("OUT_MESSAGE"));
           }
           logger.debug("Exiting doUpdateDispatchedCount");
       } finally {
       }

    }    
    
    public void doIncrementProcessedCount(Connection con, 
                                        long batchNumber)
       throws Throwable
    {
        logger.debug("Entering doIncrementProcessedCount");

        DataRequest request = new DataRequest();
        String status = "";
        try {
           /* setup store procedure input parameters */
           HashMap inputParams = new HashMap();
           inputParams.put("IN_BILLING_HEADER_ID", String.valueOf(batchNumber));

           logger.info("billing_header_id=" + batchNumber);
           // build DataRequest object
           request.setConnection(con);
           
           request.setInputParams(inputParams);
           request.setStatementID("INCREMENT_PROCESSED_COUNT");

           // get data
           DataAccessUtil dau = DataAccessUtil.getInstance();
           Map outputs = (Map) dau.execute(request);
           request.reset();
           status = (String) outputs.get("OUT_STATUS");
           if (ARConstants.COMMON_VALUE_NO.equals(status)) {
               throw new SQLException((String)outputs.get("OUT_MESSAGE"));
           }
           logger.debug("Exiting doIncrementProcessedCount");
       } finally {
       }

    }      

    /**
     * Increments Alt Pay Billing Header processed count and checks if cart processing
     * is complete (processed_count = dispatched_count).  This is currently used
     * for BillMeLater only.
     * Returns true if processed_count = dispatched_count.
     */
    public boolean doIncrementAndCheckProcessedCount(Connection con, 
                                        long batchNumber)
       throws Throwable
    {
        logger.debug("Entering doIncrementAndCheckProcessedCount");

        DataRequest request = new DataRequest();
        String doneProcessingStr = "";
        String status = "";
        boolean doneProcessingFlag = false;
        try {
           HashMap inputParams = new HashMap();
           inputParams.put("IN_BILLING_HEADER_ID", String.valueOf(batchNumber));
           logger.info("billing_header_id=" + batchNumber);

           // Build DataRequest object
           request.setConnection(con);
           request.setInputParams(inputParams);
           request.setStatementID("INCREMENT_AND_CHECK_PROC_COUNT");

           // Execute statement and get results
           DataAccessUtil dau = DataAccessUtil.getInstance();
           Map outputs = (Map) dau.execute(request);
           request.reset();
           doneProcessingStr = (String) outputs.get("OUT_IS_EQUAL");
           status = (String) outputs.get("OUT_STATUS");
           if (ARConstants.COMMON_VALUE_NO.equals(status)) {
               throw new SQLException((String)outputs.get("OUT_MESSAGE"));
           }
           if ("Y".equals(doneProcessingStr)) {
             doneProcessingFlag = true;
           }
           logger.debug("Exiting doIncrementAndCheckProcessedCount");
       } finally {
       }
       return doneProcessingFlag;
    }      
    
    /**
     * Updates Alt Pay Billing Header processed flag to indicate processing complete.
     */
    public void doUpdateProcessedFlag(Connection con, String paymentMethodId)
       throws Throwable
    {
        logger.debug("Entering doUpdateProcessedFlag");

        DataRequest request = new DataRequest();
        String status = "";
        try {
           HashMap inputParams = new HashMap();
           inputParams.put("IN_PAYMENT_METHOD_ID", paymentMethodId);
           request.setConnection(con);
           request.setInputParams(inputParams);
           request.setStatementID("UPDATE_PROCESSED_FLAG");
           DataAccessUtil dau = DataAccessUtil.getInstance();
           Map outputs = (Map) dau.execute(request);
           request.reset();
           status = (String) outputs.get("OUT_STATUS");
           if (ARConstants.COMMON_VALUE_NO.equals(status)) {
               throw new SQLException((String)outputs.get("OUT_MESSAGE"));
           }
       } finally {
         logger.debug("Exiting doUpdateProcessedFlag");
       }
    }      
    
    public BigDecimal doGetBilledPmtAmt(Connection con, String orderGuid, String paymentMethodId) throws Throwable
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doGetBilledPmtAmt");
        }
        BigDecimal amt = new BigDecimal("0");
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.setStatementID("GET_BILLED_PMT_AMT");
            dataRequest.addInputParam("IN_ORDER_GUID", orderGuid);
            dataRequest.addInputParam("IN_PAYMENT_METHOD_ID", paymentMethodId);
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            
            logger.info("order_guid=" + orderGuid);
            logger.info("payment_method_id=" + paymentMethodId);
            amt = ((BigDecimal)dataAccessUtil.execute(dataRequest));
            dataRequest.reset();
            logger.debug("Exiting doGetBilledPmtAmt");
        } finally {
        }
        return amt;
    }     
    
    public BigDecimal doGetBilledRefundAmt(Connection con, String orderGuid, String paymentMethodId) throws Throwable
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doGetBilledRefundAmt");
        }
        BigDecimal amt = new BigDecimal("0");
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.setStatementID("GET_BILLED_REFUND_AMT");
            dataRequest.addInputParam("IN_ORDER_GUID", orderGuid);
            dataRequest.addInputParam("IN_PAYMENT_METHOD_ID", paymentMethodId);
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            
            logger.info("order_guid=" + orderGuid);
            logger.info("payment_method_id=" + paymentMethodId);
            amt = ((BigDecimal)dataAccessUtil.execute(dataRequest));
            dataRequest.reset();
            logger.debug("Exiting doGetBilledRefundAmt");
        } finally {
        }
        return amt;
    }    
    
    public void doUpdateApCaptureTxt(Connection con, 
                                        String paymentId,
                                        String apCaptureTxt)
       throws Throwable
    {
        logger.debug("Entering doUpdateApCaptureTxt");

        DataRequest request = new DataRequest();
        String status = "";
        try {
           /* setup store procedure input parameters */
           HashMap inputParams = new HashMap();
           inputParams.put("IN_PAYMENT_ID", paymentId);
           inputParams.put("IN_AP_CAPTURE_TXT", apCaptureTxt);
           inputParams.put("IN_UPDATED_BY", AltPayConstants.ALT_PAY_PROCESS_NAME);
           
           logger.info("in_payment_id=" + paymentId);
           logger.info("in_ap_catpure_txt" + apCaptureTxt);
           
           // build DataRequest object
           request.setConnection(con);
           
           request.setInputParams(inputParams);
           request.setStatementID("UPDATE_PMT_AP_CAPTURE_TXT");

           // get data
           DataAccessUtil dau = DataAccessUtil.getInstance();
           Map outputs = (Map) dau.execute(request);
           request.reset();
           status = (String) outputs.get("OUT_STATUS");
           if (ARConstants.COMMON_VALUE_NO.equals(status)) {
               throw new SQLException((String)outputs.get("OUT_MESSAGE"));
           }
           logger.debug("Exiting doUpdateApCaptureTxt");
       } finally {
       }
    }   
    
    public void doUpdateApAuthTxt(Connection con, 
                                        String paymentId,
                                        String apAuthTxt)
       throws Throwable
    {
        logger.debug("Entering doUpdateApAuthTxt");

        DataRequest request = new DataRequest();
        String status = "";
        try {
           /* setup store procedure input parameters */
           HashMap inputParams = new HashMap();
           inputParams.put("IN_PAYMENT_ID", paymentId);
           inputParams.put("IN_AP_AUTH_TXT", apAuthTxt);
           inputParams.put("IN_UPDATED_BY", AltPayConstants.ALT_PAY_PROCESS_NAME);
           
           logger.info("in_payment_id=" + paymentId);
           logger.info("in_ap_auth_txt" + apAuthTxt);
           
           // build DataRequest object
           request.setConnection(con);
           
           request.setInputParams(inputParams);
           request.setStatementID("UPDATE_PMT_AP_AUTH_TXT");

           // get data
           DataAccessUtil dau = DataAccessUtil.getInstance();
           Map outputs = (Map) dau.execute(request);
           request.reset();
           status = (String) outputs.get("OUT_STATUS");
           if (ARConstants.COMMON_VALUE_NO.equals(status)) {
               throw new SQLException((String)outputs.get("OUT_MESSAGE"));
           }
           logger.debug("Exiting doUpdateApAuthTxt");
       } finally {
       }
    }   
    
    public void doUpdateApRefundTransTxt(Connection con, 
                                        String paymentId,
                                        String apRefundTransTxt)
       throws Throwable
    {
        logger.debug("Entering doUpdateApRefundTransTxt");

        DataRequest request = new DataRequest();
        String status = "";
        try {
           /* setup store procedure input parameters */
           HashMap inputParams = new HashMap();
           inputParams.put("IN_PAYMENT_ID", paymentId);
           inputParams.put("IN_AP_REFUND_TRANS_TXT", apRefundTransTxt);
           inputParams.put("IN_UPDATED_BY", AltPayConstants.ALT_PAY_PROCESS_NAME);
           
           logger.info("in_payment_id=" + paymentId);
           logger.info("in_ap_refund_trans_txt" + apRefundTransTxt);
           // build DataRequest object
           request.setConnection(con);
           
           request.setInputParams(inputParams);
           request.setStatementID("UPDATE_AP_REFUND_TRANS_TXT");

           // get data
           DataAccessUtil dau = DataAccessUtil.getInstance();
           Map outputs = (Map) dau.execute(request);
           request.reset();
           status = (String) outputs.get("OUT_STATUS");
           if (ARConstants.COMMON_VALUE_NO.equals(status)) {
               throw new SQLException((String)outputs.get("OUT_MESSAGE"));
           }
           logger.debug("Exiting doUpdateApRefundTransTxt");
       } finally {
       }
    }    
    
    public String doGetOrderGuid(Connection con, String masterOrderNumber) throws Throwable
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doGetOrderGuid");
        }
        String orderGuid = null;
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.setStatementID("GET_ORDER_GUID");
            dataRequest.addInputParam("IN_MASTER_ORDER_NUMBER", masterOrderNumber);
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            
            logger.info("master_order_number=" + masterOrderNumber);
            orderGuid = ((String)dataAccessUtil.execute(dataRequest));
            dataRequest.reset();
            logger.debug("Exiting doGetOrderGuid");
        } finally {
        }
        return orderGuid;
    }        
    
    public AltPayReportDataVO doGetReportDataPayPal(Connection con, String fullErrorFlag) throws Throwable {
        logger.debug("Entering doGetReportDataPayPal");
        AltPayReportDataVO dvo = new AltPayReportDataVO();
        int pc = 0;
        int rc = 0;
        int ac = 0;
        String text = "";
        
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.setStatementID("GET_AP_ERROR_SUMMARY");
            dataRequest.addInputParam("IN_COMPLETE_SUMMARY_FLAG", fullErrorFlag);
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            Map outputs = (Map) dataAccessUtil.execute(dataRequest);
            dataRequest.reset();
            
            pc = Integer.parseInt(outputs.get("OUT_PMT_ERROR_COUNT").toString());
            rc = Integer.parseInt(outputs.get("OUT_REFUND_ERROR_COUNT").toString());
            ac = Integer.parseInt(outputs.get("OUT_AUTH_ERROR_COUNT").toString());
            
            logger.debug("pc=" + pc);
            logger.debug("rc=" + rc);
            logger.debug("ac=" + ac);
            
            dvo.setPmtErrorCount(pc);
            dvo.setRefundErrorCount(rc);
            dvo.setAuthErrorCount(ac);
            if(pc > 0 || rc > 0 || ac >0) {
                dvo.setReportErrorFlag(true);
                if(pc > 0) {
                    text = AltPayConstants.PP_REPORT_PMT_ERROR_COUNT + pc + "\n";
                }
                if(rc > 0) {
                    text = text + AltPayConstants.PP_REPORT_REFUND_ERROR_COUNT + rc + "\n";
                }
                if(ac > 0) {
                    text = text + AltPayConstants.PP_REPORT_AUTH_ERROR_COUNT + ac + "\n";
                }
                dvo.setReportText(text);
            }
            logger.debug("Exiting doGetReportDataPayPal");
        } finally {
        }
        return dvo;        
    }
    
    public AltPayReportDataVO doGetReportDataGiftCard(Connection con, String fullErrorFlag) throws Throwable {
        logger.debug("Entering doGetReportDataGiftCard");
        AltPayReportDataVO dvo = new AltPayReportDataVO();
        int pc = 0;
        int rc = 0;
        String text = "";
        
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.setStatementID("GET_GD_ERROR_SUMMARY");
            dataRequest.addInputParam("IN_COMPLETE_SUMMARY_FLAG", fullErrorFlag);
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            Map outputs = (Map) dataAccessUtil.execute(dataRequest);
            dataRequest.reset();
            
            pc = Integer.parseInt(outputs.get("OUT_PMT_ERROR_COUNT").toString());
            rc = Integer.parseInt(outputs.get("OUT_REFUND_ERROR_COUNT").toString());
            
            logger.debug("pc=" + pc);
            logger.debug("rc=" + rc);
            
            dvo.setPmtErrorCount(pc);
            dvo.setRefundErrorCount(rc);
            if(pc > 0 || rc > 0) {
                dvo.setReportErrorFlag(true);
                if(pc > 0) {
                    text = AltPayConstants.GD_REPORT_PMT_ERROR_COUNT + pc + "\n";
                }
                if(rc > 0) {
                    text = text + AltPayConstants.GD_REPORT_REFUND_ERROR_COUNT + rc + "\n";
                }
                dvo.setReportText(text);
            }
            logger.debug("Exiting doGetReportDataGiftCard");
        } finally {
        }
        return dvo;        
    }
    
    
    public CachedResultSet doGetReportDataAltPayEODErrors(Connection con) throws Exception
    {
        logger.debug("Entering doGetReportDataAltPayEODErrors");
        
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.reset();
            dataRequest.setStatementID("GET_ALT_PAY_EOD_ERROR_MESSAGES");
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            CachedResultSet outrs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

            return outrs;
        } finally {
            logger.debug("Exiting doGetReportDataAltPayEODErrors");
        }
    }

    
    public String doGetAltPayEODErrorEmailList(Connection con) throws Exception
    {
        logger.debug("Entering doGetAltPayEODErrorEmailList");
        StringBuffer text = new StringBuffer();
        String delimiter = ",";
        
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.reset();
            dataRequest.setStatementID("GET_EOD_ERROR_ALERT_LIST");
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            CachedResultSet outrs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

            while(outrs.next()) {
                text.append(outrs.getString("email_id"));
                text.append(";");
            }
        } finally {
            logger.debug("Exiting doGetAltPayEODErrorEmailList");
        }
        return text.toString();        
    }


    /**
      * Creates billing detail record for UA. Only apply to refunds.
      */
      public void doCreateBillingDetailUA(Connection con, 
                                            AltPayProfileVOBase pvo,
                                            AltPayBillingDetailVOBase bvo)
         throws Throwable
      {

          logger.debug("Entering doCreateBillingDetailBM");
          UABillingDetailVO uavo = (UABillingDetailVO)bvo;
          
          DataRequest request = new DataRequest();
          String status = "";
          try {
             /* setup store procedure input parameters */
             HashMap inputParams = new HashMap();
             inputParams.put("IN_BILLING_HEADER_ID", uavo.getBillingHeaderId()); 
             inputParams.put("IN_PAYMENT_ID", uavo.getPaymentId()); 
             inputParams.put("IN_MANUAL_ENTRY_FLAG", ARConstants.COMMON_VALUE_NO);
             inputParams.put("IN_REQ_ACCOUNT_NUMBER", uavo.getMembershipNumber());
             inputParams.put("IN_REQ_MILES_AMT", String.valueOf(uavo.getReqMilesAmt()));
             inputParams.put("IN_REQ_BONUS_CODE", (String)pvo.getConfigsMap().get(AltPayConstants.UA_BONUS_CODE));
             inputParams.put("IN_REQ_BONUS_TYPE", (String)pvo.getConfigsMap().get(AltPayConstants.UA_BONUS_TYPE));
             inputParams.put("IN_REQ_MILE_INDICATOR", AltPayConstants.UA_MILE_INDICATOR_CREDIT);
             inputParams.put("IN_RES_SUCCESS_FLAG", ARConstants.COMMON_VALUE_YES);
             inputParams.put("IN_RES_CODE", null);
             
             logger.info("IN_BILLING_HEADER_ID: " +  uavo.getBillingHeaderId()); 
             logger.info("IN_PAYMENT_ID: " +  uavo.getPaymentId()); 
             logger.info("IN_MANUAL_ENTRY_FLAG: " +  ARConstants.COMMON_VALUE_NO);
             //logger.info("IN_REQ_ACCOUNT_NUMBER: " +  uavo.getMembershipNumber());
             logger.info("IN_REQ_MILES_AMT: " +  String.valueOf(uavo.getReqMilesAmt()));
             logger.info("IN_REQ_BONUS_CODE: " +  (String)pvo.getConfigsMap().get(AltPayConstants.UA_BONUS_CODE));
             logger.info("IN_REQ_BONUS_TYPE: " +  (String)pvo.getConfigsMap().get(AltPayConstants.UA_BONUS_TYPE));
             logger.info("IN_REQ_MILE_INDICATOR: " +  AltPayConstants.UA_MILE_INDICATOR_CREDIT);
             logger.info("IN_RES_SUCCESS_FLAG: " +  ARConstants.COMMON_VALUE_YES);
             logger.info("IN_RES_CODE: " +  null);


             logger.info(uavo.toString());
             
             // build DataRequest object
             request.setConnection(con);
             request.setInputParams(inputParams);
             request.setStatementID(pvo.getStmtInsertBillingDetail());

             // get data
             DataAccessUtil dau = DataAccessUtil.getInstance();
             Map outputs = (Map) dau.execute(request);
             request.reset();
             status = (String) outputs.get("OUT_STATUS");
             if(!status.equals(ARConstants.COMMON_VALUE_YES))
             {
                 String errorMessage = (String) outputs.get("OUT_MESSAGE");
                 throw new Exception(errorMessage);
             }
             logger.debug("Exiting doCreateBillingDetail. payment_id:" + uavo.getPaymentId());
             
         } 
         finally {
         }
         
      }   


    public BigDecimal doGetNextUnitedRefundTranID(Connection con) throws Throwable
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering doGetNextUnitedRefundTranID");
        }
        BigDecimal transactionId = null;
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.setStatementID("GET_NEXT_UA_REFUND_TRANS_SEQ");
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            
            transactionId = ((BigDecimal) (dataAccessUtil.execute(dataRequest)));
			logger.debug("transactionId: " + transactionId);
            dataRequest.reset();
            logger.debug("Exiting doGetNextUnitedRefundTranID");
        } 
        catch (Exception e){
			logger.error("Error caught retrieving United refund transaction ID : " + e.getMessage());
			throw new Exception("Error caught retrieving United refund  transaction ID : " + e.getMessage());
        }
        finally {
        }
        return transactionId;
    }      
    
}