package com.ftd.accountingreporting.constant;

public class AltPayConstants 
{
  public static final String SPRING_APP_NAME = "arApp";
  
  public static final String ALTPAY_EOD_QUEUE = "ALTPAY_EOD";
  //public static final String ALTPAY_CART_QUEUE = "ALTPAY_CART";
  
  public static final String ALTPAY_REPORT_DELAY = "ALTPAY_REPORT_DELAY";
  
  public static final String PAYPAL_EOD_PAYLOAD = "pp_eod";
  public static final String PAYPAL_REPORT_PAYLOAD = "ap_report";
  
  public static final String PAYMENT_CODE_PAYMENT = "P";
  public static final String PAYMENT_CODE_REFUND = "R";
  
  public static final String PAY_PAL_ACK_SUCCESS = "Success";
  
  public static final String ALT_PAY_PROCESS_NAME = "ALT_PAY_EOD";
  
  public static final String PP_API_USERNAME = "pp-api-username";
  public static final String PP_API_PASSWORD = "pp-api-password";
  public static final String PP_PRIVATE_KEY_PASSWORD = "pp-private-key-password";
  
  public static final String PP_CERTIFICATE_FILE_LOCATION = "pp-certificate-file-location";
  public static final String PP_CERTIFICATE_FILENAME = "pp-certificate-filename";
  public static final String PP_ENDPOINT_ENVIRONMENT = "pp-endpoint-environment";
  public static final String PP_MAX_CAPTURE_AMT = "pp-max-capture-amt";
  public static final String AP_REPORT_FULL_ERROR_FLAG = "ap-report-full-error-flag";
  public static final String PP_CONNECT_TIMER_FLAG = "PAY_PAL_CONNECT_TIMER_FLAG";
  public static final String PP_CONNECT_TIMER_MS = "PAY_PAL_CONNECT_TIMER_MS";
  public static final String PP_CONNECT_RETRY_COUNT = "PAY_PAL_CONNECT_RETRY_COUNT";
  public static final String PP_RES_ERROR_SIZE = "PAY_PAL_RES_ERROR_SIZE";
  public static final String AP_EOD_RUNDATE_OFFSET = "ALT_PAY_EOD_RUNDATE_OFFSET";
  public static final String PP_API_CONNECTION_TIMEOUT = "PAY_PAL_API_CONNECTION_TIMEOUT";
  public static final String PP_TEST_CONNECTION_FLAG = "PAY_PAL_TEST_CONNECTION_FLAG";
  public static final String PP_TEST_TRANSACTION_ID = "PAY_PAL_TEST_TRANSACTION_ID";
  
  public static final String PP_ACK_SUCCESS = "Success";
  public static final String PP_ACK_FAILURE = "Failure";
  
  public static final String PP_ERROR_CODE = "ERROR_CODE:";
  public static final String PP_ERROR_SHORT_MSG = "***ERROR_SHORT_MESSAGE:";
  public static final String PP_ERROR_LONG_MSG = "***ERROR_LONG_MESSAGE:";
  public static final String PP_ERROR_DELIMITER = "~";
  
  public static final String PP_CALL_DO_CAPTURE = "DoCapture";
  public static final String PP_CALL_REFUND_REQUEST = "RefundTransaction";
  public static final String PP_CALL_GET_TRANS_DETAIL = "GetTransactionDetails";
  public static final String PP_CALL_TRANSACTION_SEARCH = "TransactionSearch";
  
  public static final String PP_TRANSACTION_SEARCH_TYPE_PAYMENT = "Payment";
  public static final String PP_TRANSACTION_SEARCH_TYPE_REFUND = "Refund";
  public static final String PP_TRANSACTION_SEARCH_TYPE_AUTHORIZATION = "Authorization";
  public static final String PP_TRANSACTION_SEARCH_MONTH_SPAN = "PP_TRANSACTION_SEARCH_MONTH_SPAN";
    
  
  //public static final String PP_RES_TRANSACTION_ID_FILLER = "000000000";
  public static final String PP_AUTO_FIX_CAPTURE_ERROR = "10602";
  public static final String PP_AUTO_FIX_REFUND_ERROR = "10009";
  public static final String PP_AUTO_FIX_RES_ERROR = "PP_AUTO_FIXED";
  
  public static final String PP_LOCK_ENTITY_TYPE = "PAYMENTS";
  public static final String PP_LOCK_LEVEL = "ORDERS";
  public static final String PP_LOCK_CSR_ID = "PP_EOD_BILLING";
  
  public static final String PP_ERROR_CONNECTION = "PayPal EOD could not run. PayPal site has been down for the past ";
  public static final String PP_ERROR_OVER_MAX_CAPTURE_AMT = "The following payment requested amount exceeds PayPal allowed capture amount:";
  public static final String PP_MSG_AUTO_FIX = "Alert: Pal Pal EOD has auto-fixed this payment:";
  public static final String PP_ERROR_LOCK_FAILURE = "Cannot obtain lock for PayPal order. Will reprocess next run. ";
  public static final String PP_REPORT_PMT_ERROR_COUNT = "Count of failed settlement payment calls to PayPal = ";
  public static final String PP_REPORT_REFUND_ERROR_COUNT = "Count of failed settlement refund calls to PayPal = ";
  public static final String PP_REPORT_AUTH_ERROR_COUNT = "Count of PayPal payments with no authorization = ";
  public static final String PP_ERR_SHORT_MSG_TRANS_REFUSED_PARAM = "PP_ERR_SHORT_MSG_TRANS_REFUSED";
  public static final String PP_ERR_LONG_MSG_FULLY_REFUNDED_PARAM = "PP_ERR_LONG_MSG_FULLY_REFUNDED";
  public static final String PP_ERR_LONG_MSG_CHARGE_BCK_FILED_PARAM = "PP_ERR_LONG_MSG_CHARGE_BCK_FILED";

  
  //public static final String AP_PAGE_SOURCE = "ALT PAY EOD";
  public static final String AP_PAGE_ERROR_TYPE = "System Exception";
  //public static final String AP_PAGE_SUBJECT = "SYSTEM MESSAGE";
  public static final String AP_NOPAGE_SUBJECT = "NOPAGE ALT PAY EOD System Message";
  public static final String AP_ERROR_REFUND_TOO_LARGE = "Refund total exceeds payment total. Please investigate. ";


  public static final String BILLMELATER_EOD_PAYLOAD = "bm_eod";
  public static final String BILLMELATER_EOD_FINALIZER_PAYLOAD = "bm_eod_finalizer";

  public static final String BM_PAYMENT_METHOD_ID = "BM";
  public static final String BM_LOCK_ENTITY_TYPE = "PAYMENTS";
  public static final String BM_LOCK_LEVEL = "ORDERS";
  public static final String BM_LOCK_CSR_ID = " BM_EOD_BILLING";
  
  public static final String BM_FTP_USERNAME = "bm-ftp-username";
  public static final String BM_FTP_PASSWORD = "bm-ftp-password";  
  public static final String BM_FTP_URL = "bm-ftp-url";
  public static final String BM_FTP_SECONDARY_URL = "bm-ftp-secondary-url";
  public static final String BM_FTP_INBOUND_LOCATION = "bm-ftp-inbound-location";
  public static final String BM_FTP_OUTBOUND_LOCATION = "bm-ftp-outbound-location";
  public static final String BM_FTP_ARCHIVE_LOCATION = "bm-ftp-archive-location";
  public static final String BM_FTP_INBOUND_SUFFIX = ".out.done";   // Filename suffix of response file
  public static final String BM_FTP_INBOUND_RENAME_SUFFIX = ".old"; // Filename suffix of renamed response file
  public static final String BM_FTP_OUTBOUND_PREFIX = "t";          // Filename prefix for settlement file
  public static final String BM_FTP_OUTBOUND_RENAME_PREFIX = "p";   // Filename prefix for renamed settlement file
  public static final String BM_FTP_RETRY_TIME_LIMIT = "bm-ftp-retry-time-limit";  // Hour at which we give up retrying to ftp
  public static final String BM_FTP_RETRY_DELAY = "bm-ftp-retry-delay";            // Delay in seconds for retrying ftp 
  public static final String BM_PAYMENTECH_PID = "bm-paymentech-pid"; 
  public static final String BM_PAYMENTECH_PID_PASSWORD = "bm-paymentech-pid-password"; 
  public static final String BM_PAYMENTECH_SID = "bm-paymentech-sid"; 
  public static final String BM_PAYMENTECH_SID_PASSWORD = "bm-paymentech-sid-password"; 
  public static final String BM_PAYMENTECH_DIVISION_NUM = "bm-paymentech-division-num";
  public static final String BM_SECURITY_PUBLIC_KEY_PATH = "securityPublicKeyPathBml";

  public static final String BM_ERROR_LOCK_FAILURE = "Cannot obtain lock for BML order. Will reprocess next run. ";
  
  //Unite Mileage Plus
  public static final String UA_EOD_PAYLOAD = "ua_eod";
  public static final String UA_EOD_FINALIZER_PAYLOAD = "ua_eod_finalizer";
  public static final String UA_LOCK_ENTITY_TYPE = "PAYMENTS";
  public static final String UA_LOCK_LEVEL = "ORDERS"; 
  public static final String UA_LOCK_CSR_ID = "UA_EOD_BILLING";
  public static final String UA_ERROR_LOCK_FAILURE = "Cannot obtain lock for PayPal order. Will reprocess next run. ";
    
  public static final String UA_RES_SUCCESS_FLAG = "Y";
  public static final String UA_BONUS_CODE = "UA_BONUS_CODE";
  public static final String UA_BONUS_TYPE = "UA_BONUS_TYPE";
  public static final String UA_LP_EMAIL_SUBJECT = "United Mileage Plus Order Settlement Failure Notice - ";
  public static final String UA_LP_EMAIL_CONTENT = "The system has failed to deduct miles from the customer's United Mileage Plus account. " +
                                                    "This is most likely because customer no longer has enough miles on account.";
  public static final String UA_REFUND_BONUS_CODE = "UA_REFUND_BONUS_CODE";
  public static final String UA_PARTNER_CODE = "UA_PARTNER_CODE";
  
  public static final String UA_FTP_URL = "ua-ftp-url"; 
  public static final String UA_FTP_USERNAME = "ua-ftp-username"; 
  public static final String UA_FTP_PASSWORD = "ua-ftp-password"; 
  public static final String UA_FTP_OUTBOUND_LOCATION = "ua-ftp-outbound-location";
  public static final String UA_FTP_OUT_ARCHIVE_LOCATION = "ua-ftp-out-archive-location";  
  public static final String UA_FTP_OUT_FILENAME = "ua-ftp-out-filename";
  public static final String UA_FTP_INBOUND_LOCATION = "ua-ftp-inbound-location"; 
  public static final String UA_FTP_IN_ARCHIVE_LOCATION = "ua-ftp-in-archive-location"; 
  public static final String UA_FTP_IN_FILENAME = "ua-ftp-in-filename";
  public static final String UA_MILE_INDICATOR_DEDUCT = "D";
  public static final String UA_MILE_INDICATOR_CREDIT = "C";
  public static final String UA_ACCOUNT_NUMBER_MASK = "XXXXXXXXXXX";
  
  
  public static final String PROCESSED_FILE_PREFIX = "processed_";
  public static final String MILES_APPROVED = "A";
  public static final String MILES_DECLINED = "D";
  
  public static final String AAFES_EOD_PAYLOAD = "ms_eod";
  public static final String AAFES_LOCK_ENTITY_TYPE = "PAYMENTS";
  public static final String AAFES_LOCK_LEVEL = "ORDERS";
  public static final String AAFES_LOCK_CSR_ID = " AAFES_EOD_BILLING";
  public static final String AAFES_ERROR_LOCK_FAILURE = "Cannot obtain lock for AAFES order. Will reprocess next run. ";
  
  // Gift Card Payment
  public static final String GIFT_CARD_EOD_PAYLOAD = "gd_eod";
  public static final String GD_LOCK_ENTITY_TYPE = "PAYMENTS";
  public static final String GD_LOCK_LEVEL = "ORDERS";
  public static final String GD_LOCK_CSR_ID = "GD_EOD_BILLING";
  public static final String GD_ERROR_LOCK_FAILURE = "Cannot obtain lock for Gift Card order. Will reprocess next run. ";
  public static final String GD_USERNAME = "SERVICE,SVS_CLIENT"; 
  public static final String GD_PASSWORD = "SERVICE,SVS_HASHCODE"; 
  public static final String GD_SERVICE_URL = "SERVICE,PAYMENT_SERVICE_URL";
  public static final String GD_SERVICE_SUCCESS = "Success";
  public static final String GD_SERVICE_FAILURE = "Failure";
  public static final String GD_SERVICE_REQUEST_TYPE = "SVS_TRANS_ID";
  public static final String GD_SERVICE_CLIENT_NAME = "APOLLO";
  public static final String GD_SERVICE_EXCEPTION = "GiftCard Service exception occurred";
  public static final String GD_SERVICE_ERR_LOGIN = "Invalid login";
  public static final String GD_SERVICE_ERR_AMOUNT = "SVS approved amount does not cover requested amount";
  public static final String GD_RETRY_COUNT = "GIFT_CARD_RETRY_COUNT";
  public static final String GD_ENQUEUE_DELAY = "GIFT_CARD_ENQUEUE_DELAY";
  public static final String GD_REFUND_NOT_ALLOWED = "GiftCard refund exceeds billed payment/refund total";
  public static final String GD_REPORT_PMT_ERROR_COUNT = "Count of failed GiftCard settlement payment calls = ";
  public static final String GD_REPORT_REFUND_ERROR_COUNT = "Count of failed GiftCard settlement refund calls = ";
  public static final int    GD_ENQUEUE_EXTRA_DELAY = 4;  // Extra delay in seconds

  //PG Alt Pay
  public static final String PG_ALTPAY_EOD_QUEUE =  "PG_ALTPAY_EOD";
  public static final String PG_PAYPAL = "PGPayPal";
  public static final String PAYPAL ="PP";
  public static final String PG_AP_EOD_RUNDATE_OFFSET = "PG_ALT_PAY_EOD_RUNDATE_OFFSET";
  public static final String GET_PG_ALTPAY_PAYMENT_EOD_RECS ="GET_PG_ALTPAY_PAYMENT_EOD_RECS";
  
  public static final String CREATE_PG_ALTPAY_EOD_RECS = "CREATE_PG_ALTPAY_EOD_RECS";

  public static final String PG_PP_LOCK_CSR_ID = "PG_PP_EOD_BILLING";
  
  public static final String PG_ALTPAY_CART = "PG_ALTPAY_CART";
  public static final String PG_AP_NOPAGE_SUBJECT = "PG ALT PAY EOD System Message";
  public static final String PAYPAL_PAYMENT_TYPE = "PAYPAL";
  
  
}
  