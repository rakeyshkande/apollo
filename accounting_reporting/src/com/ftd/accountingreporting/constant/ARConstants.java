package com.ftd.accountingreporting.constant;

public class ARConstants {
	// Configuration file
	public static final String CONFIG_FILE = "accountingreporting-config.xml";
	public static final String SECURE_CONFIG_CONTEXT = "accounting_reporting";
	public static final String EMAIL_CONFIG_FILE = "email_information.xml";
	public static final String CONFIG_CONTEXT = "ACCOUNTING_CONFIG";
	public static final String BASE_CONFIG = "BASE_CONFIG";
	public static final String AUTH_CONFIG = "AUTH_CONFIG";

	// DataSource name
	public static final String DATASOURCE_NAME = "DATASOURCE_NAME";

	// Error file
	public static final String ERROR_FILE = "security-errors.xml";
	// ENV
	public static final String ENV_KEY = "env";
	public static final String ENV_VAL_DEV = "DEV";
	public static final String ENV_VAL_PROD = "PROD";

	// Common output parameters
	public static final String STATUS = "status";
	public static final String MESSAGE = "message";

	// Name of the HashMap attribute for DataFilter
	public static final String PARAMETERS = "parameters";

	// currency format
	public static final String CURRENCY_PATTERN_NO_DOLLAR = "#,###,##0.00";

	// accounting status
	public static final String ACCTG_STATUS_UNBILLED = "Unbilled";
	public static final String ACCTG_STATUS_BILLED = "Billed";
	public static final String ACCTG_STATUS_SETTLED = "Settled";

	// constants for common events
	public static final String EVENT_CONTEXT = "ACCOUNTING";
	public static final String EVENT_GC = "GC-FEED-GC";
	public static final String EVENT_FF = "GC-FEED-FF";
	public static final String EVENT_RD = "GC-FEED-RD";
	public static final String EVENT_AAFES_EOD = "AAFES-EOD-PROCESS";

	/*********************************************************************************/
	/** constants for Gift Cert module **/
	/*********************************************************************************/
	// Gift cert coupon status
	public static final String GCC_STATUS_ISSUED_INACTIVE = "Issued Inactive";
	public static final String GCC_STATUS_ISSUED_ACTIVE = "Issued Active";
	public static final String GCC_STATUS_REDEEMED = "Redeemed";
	public static final String GCC_STATUS_REINSTATE = "Reinstate";
	public static final String GCC_STATUS_CANCELLED = "Cancelled";
	public static final String GCC_STATUS_ISSUED_EXPIRED = "Issued Expired";
	
	// Error
	public static final String RETRIEVE_BATCH_ERROR_RESPONSE = "Retrieve Batch API returned empty response";
	public static final String UPDATE_SOURCE_ERROR_RESPONSE = "Update Source API returned empty response";
	public static final String UPDATE_RECIPIENT_ERROR_RESPONSE = "Update Recipient API returned empty response";
	public static final String COMMENT_TEXT_CREDIT_QUEUE = "Gift Code is not Redeemable.  Sending order to the Credit Q.";

	// Common parameters
	public static final String COMMON_GCC_ACTION = "gcc_action";
	public static final String COMMON_GCC_USER = "gcc_user";
	public static final String COMMON_GCC_TYPE = "gcc_type";
	public static final String COMMON_PARM_CONTEXT = "context";
	public static final String COMMON_PARM_SEC_TOKEN = "securitytoken";
	public static final String COMMON_GCC_REQUEST_NUMBER = "gcc_request_number";

	// Common constant value
	public static final String COMMON_VALUE_YES = "Y";
	public static final String COMMON_VALUE_NO = "N";
	public static final String FORWARD_STATE = "forwardState";

	// Action parameters
	public static final String ACTION_PARM_DISPLAY_CREATE = "display_create";
	public static final String ACTION_PARM_DISPLAY_SEARCH = "display_search";
	public static final String ACTION_PARM_CREATE = "create";
	public static final String ACTION_PARM_MAINTAIN = "maintain";
	public static final String ACTION_PARM_DISPLAY_MAINTAIN = "display_maintain";
	public static final String ACTION_PARM_SEARCH = "search";
	public static final String ACTION_PARM_EXIT_SEARCH = "exit_search";
	public static final String ACTION_PARM_SAVE_EXIT_CREATE = "save_exit_create";
	public static final String ACTION_PARM_SAVE_EXIT_MAINTAIN = "save_exit_maintain";
	public static final String ACTION_PARM_EXIT_MAINTAIN = "exit_maintain";
	public static final String ACTION_PARM_EXIT_CREATE = "exit_create";
	public static final String ACTION_PARM_IS_MULTIPLE_GCC = "is_multiple_gcc";
	public static final String ACTION_PARM_DISPLAY_ADD = "display_add";
	public static final String ACTION_PARM_DISPLAY_EDIT = "display_edit";
	public static final String ACTION_PARM_ADD = "add";
	public static final String ACTION_PARM_EDIT = "edit";
	public static final String ACTION_PARM_EXIT_EDIT = "exit_edit";
	public static final String ACTION_PARM_EXIT_ADD = "exit_add";
	public static final String ACTION_LOGIN = "login";
	public static final String ACTION_ERROR = "error";

	public static final String XSL_GIFT_CERTIFICATE_COUPON_MASTER = "/xsl/giftCertificateCouponMaster.xsl";
	public static final String XSL_GIFT_CERTIFICATE_COUPON_SEARCH = "/xsl/giftCertificateCouponSearch.xsl";
	public static final String XSL_GIFT_CERTIFICATE_COUPON_RECIPIENT = "/xsl/giftCertificateCouponRecipient.xsl";
	public static final String XSL_OPERATIONS_MENU = "/xsl/operationsMenu.xsl";
	public static final String XSL_CUSTOMER_SERVICE_MENU = "/xsl/customerServiceMenu.xsl";
	public static final String XSL_SOURCE_CODE_LOOK_UP = "/xsl/sourceCodeLookup.xsl";

	public static final String CUSTOMER_SERVICE = "CS";
	public static final String OPERATIONAL = "OP";
	// XML ROOT element
	public static final String XML_ROOT = "ROOT";
	// XML param
	public static final String XML_FIELD_TOP = "FIELDS";
	public static final String XML_FIELD_BOTTOM = "FIELD";
	public static final String XML_ERROR_TOP = "ERRORS";
	public static final String XML_ERROR_BOTTOM = "ERROR";
	public static final String XML_ATTR_FIELD = "fieldname";
	public static final String XML_ATTR_VALUE = "value";
	public static final String XML_ATTR_DESCRIPTION = "description";
	public static final String XML_ATTR_SELECTED = "selected";
	public static final String XML_ATTR_ERROR = "error";
	public static final String XML_FIELD_PAGEDATA = "PAGEDATA";
	public static final String XML_FIELD_HEADER = "HEADER";
	public static final String XML_FIELD_SAVE_BUTTON = "SAVE_BUTTON";
	public static final String XML_FIELD_RECIPIENT_BUTTON = "RECIPIENT_BUTTON";

	public static final String DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss.S";
	public static final String DEFAULT_CONTEXT = "Order Proc";

	// custom parameters returned from database procedure
	public static final String STATUS_PARAM = "OUT_STATUS";
	public static final String MESSAGE_PARAM = "OUT_MESSAGE";

	public static final String SRC_CODE_VALID_MESSAGE = "VALIDATE_STATUS";
	public static final String SRC_CODE_EXISTS_MESSAGE = "EXIST_MESSAGE";
	public static final String SRC_CODE_EXPIRED_MESSAGE = "EXPIRED_MESSAGE";

	public static final String CONST_MAX_CS_ISSUE_AMOUNT = "COUPON_MAX_CS_ISSUE_AMOUNT";
	public static final String CONST_GRACE_PERIOD = "COUPON_EXPIRE_GRACE_PERIOD";
	public static final String CONST_GRACE_PERIOD_OFFSET = "GRACE_PERIOD_OFFSET";
	public static final String CONST_ELECTRONIC_FILE_PATH = "GIFT_CERT_ELECTRONIC_FILE_PATH";
	public static final String CONST_SUCCESS_MSG = "success_msg";
	public static final String CONST_USER_OP = "user_op";
	public static final String CONST_USER_CS = "user_cs";
	public static final String CONST_USER_SA = "user_sa";
	public static final String MESSAGE_SUCCESSFUL_CREATE = "Number Successfully Created";
	public static final String MESSAGE_SUCCESSFUL_UPDATE = "Number Successfully Updated";
	public static final String GC_MESSAGE_NOTIFICATION = "Your request is being processed and an email will be sent upon confirmation of completion";
	
	// Constants for format and redemption type lookup.
	public static final String FORMAT_PREFIX = "GIFT_CERT_FORMAT_";
	public static final String REDEEM_TYPE_PREFIX = "GIFT_CERT_REDEEM_TYPE_";
	public static final String SOURCE_CODE_EXISTS_ERROR_MSG = "src_code_exists_err_msg";
	public static final String SOURCE_CODE_EXISTS_ERROR_COUNT = "src_code_exists_count";
	public static final String SOURCE_CODE_EXPIRED_ERROR_MSG = "src_code_expired_err_msg";
	public static final String SOURCE_CODE_EXPIRED_ERROR_COUNT = "src_code_expired_count";
	
	// Constants for Novator feed
	public static final String Q_DELAY_PROPERTY_NAME = "JMS_OracleDelay";
	public static final String NOVATOR_GCQ_DELAY_SECONDS = "NOVATOR_GCQ_DELAY_SECONDS";
	public static final String Q_DELAY_PROPERTY_TYPE_LONG = "long";

	/*********************************************************************************/
	/** constants for EOD Start **/
	/*********************************************************************************/
	public static final String TRANSACTION_TYPE_PURCHASE = "1";
	public static final String TRANSACTION_TYPE_REFUND = "3";
	public static final String CONST_EOD_BILLING_FIRST_LINE = "eodBillingFileFirstLine";
	public static final String CONST_EOD_INSTANCE = "envInstance";
	public static final String CONST_EOD_HEADER_RECORD_TYPE = "eodHeaderRecordType";
	public static final String CONST_EOD_TRAILER_RECORD_TYPE = "eodTrailerRecordType";
	public static final String CONST_EOD_LINE_ITEM_TYPE = "eodLineItemRecordType";
	public static final String CONST_EOD_BATCH_DATE_OFFSET = "eodBatchDateOffset";
	public static final String CONST_EOD_FILE_LOCATION = "eodFileLocation";
	public static final String CONST_EOD_FILE_NAME = "eodFileName";
	public static final String CONST_EOD_EXCEPTION_FILE_NAME = "eodExceptionFile";
	public static final String CONST_EOD_FILE_PREFIX = "eodFilePrefix";
	public static final String CONST_EOD_EXCEPTION_FILE = "eodExceptionFile";
	public static final String CONST_EOD_ARCHIVE_LOCATION = "eodArchiveLocation";
	public static final String CONST_EOD_FILE_SUFFIX = "eodFileSuffix";
	public static final String CC_MASK = "XXXXXXXXXXXX";

	public static final String CONST_BILL = "Bill";
	public static final String CONST_ADD_BILL = "Add_Bill";
	public static final String CONST_REFUND = "Refund";

	public static final String LOCK_ENTITY_ID = "EOD_BILLING";
	public static final String LOCK_ENTITY_TYPE = "EOD_BILLING";
	public static final String LOCK_CSR_ID = "SYS";

	public static final String BAMS_CC_AUTH_PROVIDER = "BAMS";
	public static final String BAMS_LOCK_ENTITY_ID = "BAMS_EOD_BILLING";
	public static final String BAMS_LOCK_ENTITY_TYPE = "BAMS_EOD_BILLING";
	public static final String BAMS_LOCK_CSR_ID = "SYS";
	public static final String BAMS_BATCH_OFFSET_DATE = "BAMS_END_OF_DAY_BATCH_OFFSET_DATE";

	
	public static final String PAYMENT_GATEWAY_EOD = "PG-EOD";
	public static final String DISPATCH_PG_EOD_FLAG = "DISPATCH_PG_EOD_FLAG";
	public static final String DISPATCH_PG_PAYPAL_EOD_FLAG = "DISPATCH_PG_PAYPAL_EOD_FLAG";
	public static final String PG_EOD_BATCH_OFFSET_DATE = "PG_EOD_BATCH_OFFSET_DATE";
	public static final String CREDIT_CARD = "CC";
	public static final String PAYPAL= "PP";
	public static final String PG_LOCK_ENTITY_ID = "PG_EOD_BILLING";
	public static final String PG_LOCK_ENTITY_TYPE = "PG_EOD_BILLING";
	public static final String PG_EOD_PP = "PG_EOD_PP";
	public static final String PG_EOD_CC = "PG_EOD_CC";
	
	/*********************************************************************************/
	/** constants for AAFES Billing Process **/
	/*********************************************************************************/

	public static final String AAFES_LOCK_ENTITY_ID = "AAFES_BILLING"; // for
																		// locking
	public static final String AAFES_LOCK_ENTITY_TYPE = "AAFES_BILLING"; // for
																			// locking
	public static final String AAFES_LOCK_CSR_ID = "SYS"; // for locking

	/* Accounting Reporting config */
	public static final String AAFES_CC_SERVLET_URL = "AAFES_CC_SERVLET_URL";
	public static final String AAFES_FILE_LOCATION = "AAFES_file_location";
	public static final String AAFES_EXCEPTION_FILE = "AAFES_exception_file";
	public static final String AAFES_BATCH_DATE_OFFSET = "AafesBatchDateOffset";
	public static final String AAFES_FACILITY = "AAFES_facility";

	public static final String AAFES_BATCH_DATE_FORMAT = "MMddyy";

	public static final String AAFES_BILL_TYPE_BILL = "Bill";
	public static final String AAFES_BILL_TYPE_ADD_BILL = "Add_Bill";
	public static final String AAFES_BILL_TYPE_REFUND = "Refund";

	public static final String AAFES_TRANSACTION_TYPE_PAYMENT = "S";
	public static final String AAFES_TRANSACTION_TYPE_REFUND = "C";
	public static final String AAFES_RETURN_CODE_AUTHORIZED = "A";
	public static final String AAFES_BILLING_DETAIL_VERIFIED = "V";

	public static final String AAFES_READY_TO_BUILD_TYPE = "type";
	public static final String AAFES_READY_TO_BUILD_CC_NUMBER = "ccnumber";
	public static final String AAFES_READY_TO_BUILD_AMOUNT = "amount";
	public static final String AAFES_READY_TO_BUILD_FACILITY_NBR = "facnbr";
	public static final String AAFES_READY_TO_BUILD_AUTH_CODE = "authcode";
	public static final String AAFES_READY_TO_BUILD_TICKET = "ticket";

	/*********************************************************************************/
	/** constants for Amazon Billing Process **/
	/*********************************************************************************/

	//for locking
	public static final String AMAZON_LOCK_ENTITY_ID_REMITTANCE = "AMAZON_REMITTANCE";  
	public static final String AMAZON_LOCK_ENTITY_TYPE_REMITTANCE = "AMAZON_REMITTANCE";  
	public static final String AMAZON_LOCK_ENTITY_ID_EOD = "AMAZON_EOD"; 
	public static final String AMAZON_LOCK_ENTITY_TYPE_EOD = "AMAZON_EOD"; 
	public static final String AMAZON_LOCK_CSR_ID = "SYS"; 
	
	//for order adjustment
	public static final String AMAZON_ADJUSTMENT_SOURCE = "AMAZON_EOD"; 
	public static final String AMAZON_CSR_ID = "SYS"; 
	
	public static final String AMAZON_PARTNER_ID = "AMZNI";
	public static final String AMAZON_BATCH_DATE_OFFSET = "AmazonBatchDateOffset";
	public static final String AMAZON_EOD_REFUND_ADJUSTMENT_ID = "adjustment_id";

	/*********************************************************************************/
	/** constants for Wal-Mart Billing Process **/
	/*********************************************************************************/
	//for locking
	public static final String WALMART_LOCK_ENTITY_ID_REMITTANCE = "WALMART_REMITTANCE"; 
	public static final String WALMART_LOCK_ENTITY_TYPE_REMITTANCE = "WALMART_REMITTANCE"; 
	public static final String WALMART_LOCK_ENTITY_ID_EOD = "WALMART_EOD"; 
	public static final String WALMART_LOCK_ENTITY_TYPE_EOD = "WALMART_EOD"; 
	public static final String WALMART_LOCK_CSR_ID = "SYS"; 
	
	//for order adjustment
	public static final String WALMART_ADJUSTMENT_SOURCE = "WALMART_EOD"; 
	public static final String WALMART_CSR_ID = "SYS"; 
	public static final String WALMART_BATCH_DATE_OFFSET = "WalMartBatchDateOffset";
	public static final String WALMART_PARTNER_ID = "WLMTI";

	public static final String WALMART_EOD_REFUND_ADJUSTMENT_ID = "adjustment_id";

	public static final String WALMART_MSG_TYPE_STATUS = "STATUS";
	public static final String WALMART_MSG_TYPE_REFUND = "REFUND";
	
	/*********************************************************************************/
	/** System Messager Constants **/
	/*********************************************************************************/
	public static final String AMAZON_EOD_PROCESS = "Amazon EOD Billing Process";
	public static final String AMAZON_REMITTANCE_PROCESS = "Amazon Remittance Process";
	public static final String WALMART_EOD_PROCESS = "Wal-Mart EOD Billing Process";
	public static final String WALMART_REMITTANCE_PROCESS = "Wal-Mart Remittance Process";
	public static final String AAFES_BILLING_PROCESS = "AAFES Billing Processing";
	public static final String EOD_BILLING_PROCESS = "EOD Billing Processing";
	public static final String GIFT_CERT_NOVATOR_PROCESS = "Gift Certificate Novator Transmission";
	public static final String GC_INBOUND_STATUS_PROCESS = "Gift Certificate Inbound Status Process";
	public static final String FLORIST_RECON_PROCESS = "Mercury Reconciliation Process";
	public static final String VENDOR_RECON_PROCESS = "Venus Reconciliation Process";
	public static final String CC_RECON_PROCESS = "CC Reconciliation Process";
	public static final String RECON_MESSAGE_ARCHIVE_PROCESS = "Recon Message Archive Process";

	public static final String AMAZON_EOD_PROCESS_ERROR_TYPE = "System Exception";
	public static final String AMAZON_REMITTANCE_PROCESS_ERROR_TYPE = "System Exception";
	public static final String WALMART_EOD_PROCESS_ERROR_TYPE = "System Exception";
	public static final String WALMART_REMITTANCE_PROCESS_ERROR_TYPE = "System Exception";
	public static final String AAFES_BILLING_PROCESS_ERROR_TYPE = "System Exception";
	public static final String EOD_BILLING_PROCESS_ERROR_TYPE = "System Exception";
	public static final String GIFT_CERT_NOVATOR_PROCESS_ERROR_TYPE = "System Exception";
	public static final String FLORIST_RECON_PROCESS_ERROR_TYPE = "System Exception";
	public static final String VENDOR_RECON_PROCESS_ERROR_TYPE = "System Exception";
	public static final String CC_RECON_PROCESS_ERROR_TYPE = "System Exception";
	public static final String RECON_MESSAGE_ARCHIVE_PROCESS_ERROR_TYPE = "System Exception";
	public static final String GC_INBOUND_STATUS_PROCESS_ERROR_TYPE = "System Exception";

	/* JMS Constants */
	public static final String JMS_NOVATOR_GIFT_CERT = "NOVATOR_GIFT_CERT";

	/*********************************************************************************/
	/** Gift Certificate Novator Transmisson Processing Constants **/
	/*********************************************************************************/

	public static final String NOVATOR_TRANSMISSION_MAX_RETRY_COUNT = "MaxRetryCount";
	public static final String NOVATOR_GIFT_CERT_RESPONSE_XSD = "novator-gift-cert-response.xsd";
	public static final String NOVATOR_GIFT_CERT_URL = "NOVATOR_GIFT_CERT_URL";

	/*********************************************************************************/
	/** Recon Chargeback Constants **/
	/*********************************************************************************/
	public static final String COMMON_CBR_ACTION = "cbr_action";
	public static final String ACTION_CBR_LOAD = "load";
	public static final String ACTION_CBR_PRINT = "print";
	public static final String ACTION_CBR_VIEW = "view";
	public static final String ACTION_CBR_ADD = "add";
	public static final String ACTION_CBR_DISPLAY_ADD = "display_add";
	public static final String ACTION_CBR_EDIT = "edit";
	public static final String ACTION_CBR_DISPLAY_EDIT = "display_edit";
	public static final String ACTION_CBR_MAIN = "main";
	public static final String ACTION_CUST_HOLD_INFO = "cust_hold_info";
	public static final String ACTION_SHOPPING_CART_INFO = "shopping_cart_info";
	public static final String ACTION_RECIPIENT_INFO = "recipient_info";
	public static final String CB_REFUND_LOCK_ENTITY_TYPE = "PAYMENTS";
	public static final String CB_REFUND_LOCK_LEVEL = "ORDERS";

	public static final String MESG_ARCHIVE_NUM_MONTHS = "MESG_ARCHIVE_NUM_MONTHS";
	public static final String MESG_ARCHIVE_NUM_DAYS = "MESG_ARCHIVE_NUM_DAYS";

	public static final String FLORIST_RECON_ENTITY_ID_REMITTANCE = "FLORIST_RECONCILIATION";
	public static final String FLORIST_RECON_ENTITY_TYPE_REMITTANCE = "FLORIST_RECONCILIATION";
	public static final String MERCURY_RECON_ENTITY_ID_REMITTANCE = "MERCURY_RECONCILIATION"; 
	public static final String MERCURY_RECON_ENTITY_TYPE_REMITTANCE = "MERCURY_RECONCILIATION";
	public static final String VENUS_RECON_ENTITY_ID_REMITTANCE = "VENUS_RECONCILIATION"; 
	public static final String VENUS_RECON_ENTITY_TYPE_REMITTANCE = "VENUS_RECONCILIATION";
	public static final String CC_RECON_ENTITY_ID_REMITTANCE = "CC_RECONCILIATION"; 
	public static final String CC_RECON_ENTITY_TYPE_REMITTANCE = "CC_RECONCILIATION";
	
	//for order adjustment
	public static final String MERCURY_RECON_CSR_ID = "SYS"; 
	public static final String MERCURY_RECON_REPORT_ID_KEY = "mercReconReportId";

	
	public static final String VENUS_RECON_CSR_ID = "SYS"; 
	public static final String CC_RECON_CSR_ID = "SYS"; 

	/**
	 * Request info used by the DataFilter
	 **/
	// Header Data
	public static final String H_BRAND_NAME = "call_brand_name";
	public static final String H_CUSTOMER_ORDER_INDICATOR = "call_type_flag";
	public static final String H_CUSTOMER_SERVICE_NUMBER = "call_cs_number";
	public static final String H_DNIS_ID = "call_dnis";
	public static final String H_CALL_LOG_ID = "call_log_id";
	public static final String WORK_COMPLETE = "work_complete";
	
	// Search Criteria
	public static final String SC_CC_NUMBER = "sc_cc_number";
	public static final String SC_CUST_IND = "sc_cust_ind";
	public static final String SC_CUST_NUMBER = "sc_cust_number";
	public static final String SC_EMAIL_ADDRESS = "sc_email_address";
	public static final String SC_LAST_NAME = "sc_last_name";
	public static final String SC_MEMBERSHIP_NUMBER = "sc_rewards_number";
	public static final String SC_ORDER_NUMBER = "sc_order_number";
	public static final String SC_PHONE_NUMBER = "sc_phone_number";
	public static final String SC_RECIP_IND = "sc_recip_ind";
	public static final String SC_TRACKING_NUMBER = "sc_tracking_number";
	public static final String SC_ZIP_CODE = "sc_zip_code";
	
	// Timer Filter
	public static final String TIMER_CALL_LOG_ID = "t_call_log_id";
	public static final String TIMER_COMMENT_ORIGIN_TYPE = "t_comment_origin_type";
	public static final String TIMER_ENTITY_HISTORY_ID = "t_entity_history_id";
	
	// comments related
	public static final String START_ORIGIN = "start_origin";
	public static final String ADMIN_ACTION = "adminAction";
	public static final String CONTEXT = "context";
	public static final String SEC_TOKEN = "securitytoken";

	public static final String CBR_ACTION = "cbr_action";
	public static final String ORDER_GUID = "order_guid";
	public static final String MASTER_ORDER_NUMBER = "master_order_number";
	public static final String APPLICATIONCONTEXT = "applicationcontext";
	public static final String UO_DISPLAY_PAGE = "uo_display_page";
	public static final String ORDER_DETAIL_ID = "order_detail_id";
	public static final String EXTERNAL_ORDER_NUMBER = "external_order_number";

	/*********************************************************************************/
	/** Gift Cert Feed Constants **/
	/*********************************************************************************/
	public static final String GC_FEED_HANDLER_FILE = "gc-event-handlers.xml";
	public static final String HANDLER_DEFAULT = "DEFAULT";
	public static final String FILE_FIELD_NAME_CONST = "const";
	public static final String FILE_FIELD_NAME_NULL = "null";

	/*********************************************************************************/
	/** Reports Constants **/
	/*********************************************************************************/
	public static final String REPORT_MAIN_MENU = "report_main_menu";

	/*********************************************************************************/
	/** Vendor Batch Constants **/
	/*********************************************************************************/
	public static final String VENDOR_EOM_LOCK_ENTITY_TYPE = "VENDOR_EOM";
	public static final String VENDOR_EOM_LOCK_ENTITY_ID = "VENDOR_EOM";
	public static final String VENDOR_EOM_LOCK_CSR_ID = "SYS";
	public static final String VENDOR_EOM_RUNDATE_OFFSET = "VENDOR_EOM_RUNDATE_OFFSET";
	public static final String VENDOR_AUDIT_LOG_INSERTED_FLAG = "VENDOR_AUDIT_LOG_INSERTED_FLAG";
	public static final String VENDOR_EOM_VENDOR_CODE = "VENDOR_EOM_VENDOR_CODE";
	public static final String VENDOR_EOM_FTD_VENDOR_CODE_NO = "VENDOR_EOM_FTD_VENDOR_CODE_NO";
	public static final String VENDOR_EOM_LINE_TYPE = "VENDOR_EOM_LINE_TYPE";
	public static final String VENDOR_EOM_CREDIT_DESC = "VENDOR_EOM_CREDIT_DESC";
	public static final String VENDOR_EOM_ADJ_DESC = "VENDOR_EOM_ADJ_DESC";
	public static final String VENDOR_EOM_CREDIT_ITEM_NO = "VENDOR_EOM_CREDIT_ITEM_NO";
	public static final String VENDOR_EOM_ADJ_ITEM_NO = "VENDOR_EOM_ADJ_ITEM_NO";
	public static final String VENDOR_EOM_QUANTITY_SHIPPED = "VENDOR_EOM_QUANTITY_SHIPPED";
	public static final String VENDOR_JDE_FTP_NEXT_SERIAL_NO = "VENDOR_JDE_FTP_NEXT_SERIAL_NO";
	public static final String VENDOR_JDE_FTP_NEXT_FILENO = "VENDOR_JDE_FTP_NEXT_FILENO";
	public static final String VENDOR_EOM_FTP_SERVER = "VENDOR_EOM_FTP_SERVER";
	public static final String VENDOR_EOM_FTP_LOCATION = "VENDOR_EOM_FTP_LOCATION";
	public static final String VENDOR_EOM_FTP_USERNAME = "VENDOR_EOM_FTP_USERNAME";
	public static final String VENDOR_EOM_FTP_PASSWORD = "VENDOR_EOM_FTP_PASSWORD";
	public static final String VENDOR_EOM_LOCAL_LOCATION = "VENDOR_EOM_LOCAL_LOCATION";
	public static final String VENDOR_EOM_ARCHIVE_LOCATION = "VENDOR_EOM_ARCHIVE_LOCATION";
	
	/**********************************************************************************************************
	** DIPII-6 : Create a monthly job to populate west_vendor_jde_audit_log table, prepare and send files to JDE. 
	** West Vendor Batch Constants **
	**********************************************************************************************************/
	public static final String WEST_VENDOR_EOM_LOCK_ENTITY_TYPE = "WEST_VENDOR_EOM";
	public static final String WEST_VENDOR_EOM_LOCK_ENTITY_ID = "WEST_VENDOR_EOM";
	public static final String WEST_VENDOR_EOM_LOCK_CSR_ID = "SYS";
	public static final String WEST_VENDOR_EOM_RUNDATE_OFFSET = "WEST_VENDOR_EOM_RUNDATE_OFFSET";
	public static final String WEST_VENDOR_AUDIT_LOG_INSERTED_FLAG = "WEST_VENDOR_AUDIT_LOG_INSERTED_FLAG";
	public static final String WEST_VENDOR_EOM_VENDOR_CODE = "WEST_VENDOR_EOM_VENDOR_CODE";
	public static final String WEST_VENDOR_EOM_FTD_VENDOR_CODE_NO = "WEST_VENDOR_EOM_FTD_VENDOR_CODE_NO";
	public static final String WEST_VENDOR_EOM_LINE_TYPE = "WEST_VENDOR_EOM_LINE_TYPE";
	public static final String WEST_VENDOR_EOM_CREDIT_DESC = "WEST_VENDOR_EOM_CREDIT_DESC";
	public static final String WEST_VENDOR_EOM_ADJ_DESC = "WEST_VENDOR_EOM_ADJ_DESC";
	public static final String WEST_VENDOR_EOM_CREDIT_ITEM_NO = "WEST_VENDOR_EOM_CREDIT_ITEM_NO";
	public static final String WEST_VENDOR_EOM_ADJ_ITEM_NO = "WEST_VENDOR_EOM_ADJ_ITEM_NO";
	public static final String WEST_VENDOR_EOM_QUANTITY_SHIPPED = "WEST_VENDOR_EOM_QUANTITY_SHIPPED";
	/************************************************************************************************************
	** End of West Vendor Batch Constants **
	*************************************************************************************************************/

	public static final String BASE_URL = "BASE_URL";

	/*********************************************************************************/
	/** Partner Order Processing Constants **/
	/*********************************************************************************/
	public static final String POP_EVENT_CONTEXT = "PARTNER_ORDER_PROCESSING";

	public static final String DISPATCH_JDE_EOD_FLAG = "DISPATCH_JDE_EOD_FLAG";
	public static final String DISPATCH_BAMS_EOD_FLAG = "DISPATCH_BAMS_EOD_FLAG";
	public static final String DISPATCH_ALTPAY_EOD_FLAG = "DISPATCH_ALTPAY_EOD_FLAG";
	public static final String DISPATCH_PAYPAL_EOD_FLAG = "DISPATCH_PAYPAL_EOD_FLAG";
	public static final String DISPATCH_BILLMELATER_EOD_FLAG = "DISPATCH_BILLMELATER_EOD_FLAG";
	public static final String DISPATCH_UA_EOD_FLAG = "DISPATCH_UA_EOD_FLAG";
	public static final String DISPATCH_ALTPAY_REPORT_FLAG = "DISPATCH_ALTPAY_REPORT_FLAG";
	public static final String DISPATCH_UA_EOD_DELAY = "DISPATCH_UA_EOD_DELAY";
	public static final String DISPATCH_AAFES_EOD_FLAG = "DISPATCH_AAFES_EOD_FLAG";
	public static final String DISPATCH_GIFT_CARD_EOD_FLAG = "DISPATCH_GIFT_CARD_EOD_FLAG";

	// system messages
	public static final String SM_PAGE_SOURCE = "Accounting";
	public static final String SM_NOPAGE_SOURCE = "End of Day";
	public static final String SM_AP_PAGE_SUBJECT = "Alt Pay System Message";

	// timer
	public static final String TIMER_PP_CONNECTION_TESTER = "PayPalConnectionTester";

	/*********************************************************************************/
	/** Account Remittance / Outstanding Receivables **/
	/*********************************************************************************/
	public static final String DEFAULT_RECORDS_PER_PAGE = "25";
	public static final String DEFAULT_SORT_COLUMN = "ship_id_line_num";
	public static final String DEFAULT_SORT_DIRECTION = "asc";
	public static final String OUTSTANDING_RECEIVABLES_LOCK_ENTITY_TYPE = "OUTSTANDING_RECEIVABLES";

	// Global parms
	public static final String GLOBAL_OR_CONTEXT = "OUTSTANDING_RECEIVABLES";
	public static final String GLOBAL_OR_RECORDS_PER_PAGE = "RECORDS_PER_PAGE";

	public static final String MESSAGE_GENERATOR_CONFIG = "MESSAGE_GENERATOR_CONFIG";
	public static final String FTDAPPS_PARMS_CONFIG = "FTDAPPS_PARMS";
	public static final String SMTP_HOST_NAME = "SMTP_HOST_NAME";
	public static final String LP_EMAIL_ADDRESS = "LP_EMAIL_ADDRESS";
	public static final String EMAIL_FROM_ADDRESS = "no-reply@ftdi.com";
	public static final String BAMS_EMAIL_FROM_ADDRESS = "support.bams@ftdi.com";
	public static final String GIFT_CERT_MAINTENANCE_PROCESS = "Gift Certificate Maintenance Process";
	public static final String GIFT_CERT_MAINTENANCE_PROCESS_ERROR_TYPE = "System Exception";
	public static final String GIFT_CODE_MAINTENANCE_PROCESS = "Gift Certificate Maintenance Process";
	public static final String GIFT_CODE_MAINTENANCE_PROCESS_ERROR_TYPE = "System Exception";

	/**
	 ***************************************************************************************************
	 *** PTS Constants
	 ****************************************************************************************************
	 */

	public static final String MERCHANT_NAME = "MERCHANT_NAME_";
	public static final String MERCHANT_CITY = "MERCHANT_CITY_";
	public static final String MERCHANT_ACCOUNT = "MERCHANT_ACCOUNT_";
	public static final String M_RECORD_ID = "M";
	public static final String N_RECORD_ID = "N";
	public static final String D_RECORD_ID = "D";
	public static final String E_RECORD_ID = "E";
	public static final String T_RECORD_ID = "T";
	public static final String S_RECORD_ID = "S";
	public static final String XD05_RECORD_ID = "XD05";
	public static final String XD01_RECORD_ID = "XD01";
	public static final String XD02_RECORD_ID = "XD02";
	public static final String XE02_RECORD_ID = "XE02";
	public static final String XV01_RECORD_ID = "XV01";
	public static final String XV02_RECORD_ID = "XV02";
	public static final String XN01_RECORD_ID = "XN01";
	public static final String M_SUBMISSION_TYPE = "6";
	public static final String M_REFERENCE_ID = "RR";
	public static final String N_STATE = "IL";
	public static final String TRANSACTION_CODE_SALES = "5";
	public static final String TRANSACTION_CODE_CREDIT = "6";
	public static final String E_MAGNETIC_STRIPE_STATUS = "01";
	public static final String E_MAGNETIC_STRIPE_STATUS_MC = "81";
	public static final String E_POS_TERMINAL_CAPABILITY = "9";
	public static final String E_MERCHANT_ZIP_CODE = "60515";
	public static final String S_QUASI_CASH_INDICATOR = "N";
	public static final String PTS_SETTLEMENT_COMPANIES = "MERCHANT_ACCOUNTS";
	public static final String PARTIAL_SHIPMENT_INDICATOR = "N";
	public static final String CONST_PTS_FILE_LOCATION = "ptsFileLocation";
	public static final String CONST_PTS_FILE_NAME = "ptsFileName";
	public static final String CONST_PTS_ARCHIVE_LOCATION = "ptsArchiveLocation";
	public static final String ECOMM_SECURITY_LEVEL = "21";
	public static final String CARDINAL_COMMERCE_SPECIAL_CONDITION_INDICATOR_5 = "5";
	public static final String CARDINAL_COMMERCE_SPECIAL_CONDITION_INDICATOR_6 = "6";
	public static final String CARDINAL_COMMERCE_SPECIAL_CONDITION_INDICATOR_7 = "7";
	public static final String CARDINAL_COMMERCE_SPECIAL_CONDITION_INDICATOR_8 = "8";

	public final static String EOD_CART_CNT_CHK_INTERVAL = "eodCartCountCheckInterval";
	public final static String EOD_CART_CNT_CHANGE_CHK_INTERVAL = "eodCartCountChangeCheckInterval";
	public final static String EOD_FILE_SEND_RETRY_INTERVAL = "eodFileSendRetryInterval";
	public final static String EOD_FILE_SEND_RETRY_COUNT = "eodFileSendRetryCount";

	public final static String BAMS_PUBLIC_KEY_PATH = "bamspublickeypath";
	public final static String PTS_INTERNAL_FTP_SERVER = "ptsInternalFtpServer";
	public final static String PTS_INTERNAL_FTP_LOC = "ptsInternalFtpLocation";
	public final static String PTS_INTERNAL_FTP_USER = "ptsInternalFtpUsername";
	public final static String PTS_INTERNAL_FTP_PWD = "ptsInternalFtpPassword";

	public final static String FTD_PUBLIC_KEY_PATH = "FTD_PUBLIC_ENCRYPTION_KEY";
	public static final String BAMS_PTS_SETTLEMENT_FTDCOPY_LOC = "BAMS_PTS_SETTLEMENT_FTDCOPY_LOC";

	public static final String COMMA_SEPERATOR = ",";
	
	public static enum SettlementStatus { Settled, Failed, Sent };
	public static final String SERVICE = "SERVICE";
	
	public static final String FTD_COMPANY_ID = "ftd";
	
	public static final String PG_BATCH_DATE_FORMAT = "MMddyy";
	public static final String PG_CC_ERROR_LOCK_FAILURE = "Cannot obtain lock for CC order while running PG EOD. It will retry with some delay."; 
	public static final String PG_EOD_CART = "PG_EOD_CART";
	public static final String SM_PAGE_SUBJECT = "PG EOD System Message";
	public static final String PAYMENT_GATEWAY_SVC_URL  = "PAYMENT_GATEWAY_SVC_URL";
	public static final String PAYMENT_GATEWAY_SVC_TIMEOUT = "PAYMENT_GATEWAY_SVC_TIMEOUT";
	public static final String PAYMENT_GATEWAY_SVC_DELAY_TIME_IN_RETRY = "PAYMENT_GATEWAY_SVC_DELAY_TIME_IN_RETRY";
	public static final String PAYMENT_GATEWAY_SVC_RETRY_LIMIT = "PAYMENT_GATEWAY_SVC_RETRY_LIMIT";

	

}
