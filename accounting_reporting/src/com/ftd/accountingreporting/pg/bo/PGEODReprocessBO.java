package com.ftd.accountingreporting.pg.bo;

import java.util.ArrayList;
import java.util.List;

import com.ftd.accountingreporting.altpay.dao.PGAltPayDAO;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.dao.EODDAO;
import com.ftd.accountingreporting.vo.PGEODRecordVO;

public class PGEODReprocessBO {

	private EODDAO eodDAO;
	private PGAltPayDAO pgAltDAO;

	public PGEODReprocessBO(EODDAO dao,PGAltPayDAO pgAltDAO) {
		this.eodDAO = dao;
		this.pgAltDAO = pgAltDAO;
	}

	public long getDBBillingHeaderId(long billingHeaderId, String paymentType) throws Exception {
		return eodDAO.getDBBillingHeaderId(billingHeaderId, paymentType);
	}

	public List<PGEODRecordVO> getFailedCCBillingDetailsList(long billingHeaderId, String paymentMethodId) throws Exception {
		List<PGEODRecordVO> billingDetails = new ArrayList<PGEODRecordVO>();

		if(ARConstants.CREDIT_CARD.equalsIgnoreCase(paymentMethodId))
		{
			billingDetails = eodDAO.getFailedCCBillingDetailsList(billingHeaderId, paymentMethodId);
		}	
		else if(ARConstants.PAYPAL.equalsIgnoreCase(paymentMethodId))
		{
			billingDetails = pgAltDAO.getFailedPPBillingDetailsList(billingHeaderId, paymentMethodId);
		}

		return billingDetails;
	}

}
