package com.ftd.accountingreporting.pg.bo;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.dao.EODDAO;
import com.ftd.accountingreporting.exception.EntityLockedException;
import com.ftd.accountingreporting.util.AccountingUtil;
import com.ftd.accountingreporting.util.EODUtil;
import com.ftd.accountingreporting.util.LockUtil;
import com.ftd.accountingreporting.util.SystemMessager;
import com.ftd.accountingreporting.vo.EODPaymentVO;
import com.ftd.accountingreporting.vo.PGEODMessageVO;
import com.ftd.accountingreporting.vo.PGEODRecordVO;
import com.ftd.op.common.vo.jccasreq.CardTypeType;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.stats.ServiceResponseTrackingUtil;
import com.ftd.osp.utilities.vo.ServiceResponseTrackingVO;
import com.ftd.pg.client.CreditCardClient;
import com.ftd.pg.client.creditcard.refund.request.RefundRequest;
import com.ftd.pg.client.creditcard.refund.response.RefundResponse;
import com.ftd.pg.client.creditcard.settlement.request.SettlementRequest;
import com.ftd.pg.client.creditcard.settlement.response.SettlementResponse;

public class PGEODShoppingCartBO {

	private static Logger logger = new Logger("com.ftd.accountingreporting.bo.PGEODShoppingCartBO");

	private final static String APPROVED = "approved";
	private final static String SETTLED = "Settled";
	private final static String DECLINED = "Declined";
	private final static String ERROR = "Error";
	/*private final static String INVALID = "Invalid";
	private final static String INVALID_ERR_SATUS_CODE = "400";*/
	private final static String SERVICE_ERR_SATUS_CODE = "500";
	private static final String PG_CC_SETTLEMENT = "PG_CC_SETTLEMENT";
	private static final String PG_PAYMENT_TRANSACTION_TYPE = "1";// Payment,
																	// ADD Bills
	private static final String PG_REFUND_TRANSACTION_TYPE = "3";// Refunds
	
	

	private EODDAO dao;

	public PGEODShoppingCartBO(EODDAO dao) {
		this.dao = dao;
	}

	/**
	 * Process Starts from this method.
	 * 
	 * @param mvo
	 * @throws Throwable
	 */
	public void processPG(PGEODMessageVO mvo) throws Throwable {
		if (logger.isDebugEnabled()) {
			logger.debug("Entering process master order number " + mvo.getMasterOrderNumber());
		}

		boolean lockObtained = false;
		LockUtil lockUtil = null;
		String orderGuid = null;
		
		
		String sessionId = this.getLockSessionId();
		Connection conn = EODUtil.createDatabaseConnection();
		

		try {

			lockUtil = new LockUtil(conn);
			orderGuid = dao.doGetOrderGuid(mvo.getMasterOrderNumber());
			sessionId = this.getLockSessionId();

			lockObtained = lockUtil.obtainLock(ARConstants.CB_REFUND_LOCK_ENTITY_TYPE, orderGuid,
					ARConstants.PG_LOCK_ENTITY_ID, sessionId, ARConstants.CB_REFUND_LOCK_LEVEL);
			
			if (lockObtained) {

				// Have Payments and Add Bills
				Map<String, List<PGEODRecordVO>> paymentRecordMap = getPGPaymentsMap(mvo.getPaymentIdList(), false);
				// Have Refund
				Map<String, List<PGEODRecordVO>> refundRecordMap = getPGPaymentsMap(mvo.getRefundIdList(), true);

				// Consolidating pre-settlements P1-R1, P2-R2 based on
				// AutTransaction ID etc..
				consolidatePreSettlements(paymentRecordMap, refundRecordMap, mvo);

				// Payments and Same Day Refunds - Pre Settlements
				if (!paymentRecordMap.isEmpty())
					processPGRecords(paymentRecordMap, mvo); // Only Payments

				if (!refundRecordMap.isEmpty())
					processPGRecords(refundRecordMap, mvo); // Remaining Refunds

				if (logger.isDebugEnabled()) {
					logger.debug("Exiting process Master Order Number in ProcessPG " + mvo.getMasterOrderNumber());
				}
			} else {
				StringBuffer sb = new StringBuffer(ARConstants.PG_CC_ERROR_LOCK_FAILURE);
				sb.append("::");
				sb.append(mvo.getMasterOrderNumber());
				logger.error(new Exception(sb.toString()));
				sendSystemMessage(conn, new Exception(sb.toString()), false);
			}
		} catch (EntityLockedException e) {
			logger.error(e); 
			StringBuffer sb = new StringBuffer(ARConstants.PG_CC_ERROR_LOCK_FAILURE);
			sb.append("::");
			sb.append(mvo.getMasterOrderNumber());
			sendSystemMessage(conn, new Exception(sb.toString()), false);
		} catch (Exception t) {
			try {
				logger.error(t);
				sendSystemMessage(conn, t, false);
			} catch (Throwable tt) {
				logger.error(tt);
			}
		} finally {
			// increment processed count regardless of result
			boolean allProcessedFlag = checkAndIncrementProcessedCount(mvo.getBatchNumber());
			// Check the dispatched and Processed count and mark the header
			// Indicator as 'Y'
			logger.debug("PG EOD allProcessedFlag ::" + allProcessedFlag);
			if (allProcessedFlag) {
				performPostPGEODTasks(mvo, conn);
			}

			if (lockObtained) {
				try {
					lockUtil.releaseLock(ARConstants.CB_REFUND_LOCK_ENTITY_TYPE, orderGuid, ARConstants.PG_LOCK_ENTITY_ID,
							sessionId);
				} catch (Exception e) {
					logger.error(e);
					sendSystemMessage(conn, e, false);
				}
			}
		}

	}
	
	
	/**
	 * @param mvo
	 * @param conn
	 * @throws Throwable
	 */
	private void performPostPGEODTasks(PGEODMessageVO mvo, Connection conn) throws Throwable {

		updateHeaderIndicator(mvo.getBatchNumber(), ARConstants.CREDIT_CARD);
		long failedRecCounter = 0;
		List<PGEODRecordVO> billingDetailsList = getErroredBillingRecords(mvo.getBatchNumber(), ARConstants.CREDIT_CARD);
		if (billingDetailsList != null)
			failedRecCounter = billingDetailsList.size();

		if (failedRecCounter > 0) {
			StringBuffer errorMessage = new StringBuffer(
					"PG EOD Settlements/Refunds were failed. Please Reprocess Billing Details records using Scheduler Screen asap.");
			errorMessage.append("\n");
			errorMessage.append("No. of Failed Records:: " + failedRecCounter + " for the Batch Number::"+ mvo.getBatchNumber());
			errorMessage.append(" of Payment Type ::" + ARConstants.CREDIT_CARD);
			errorMessage.append("\n\n");
			errorMessage.append("PayLoad is :: "+mvo.getBatchNumber()+"|"+ARConstants.CREDIT_CARD);
			errorMessage.append("\n");
			errorMessage.append("***Note: Please wait until PG-EOD is completed before you proceed with Re-Process.***");
			errorMessage.append("\n");
			errorMessage.append("This is because errored records may retry.");
			logger.debug(errorMessage.toString()); // For Logging
			SystemMessager.send(errorMessage.toString(), null, ARConstants.SM_PAGE_SOURCE, SystemMessager.LEVEL_PRODUCTION,
					ARConstants.EOD_BILLING_PROCESS_ERROR_TYPE, ARConstants.SM_PAGE_SUBJECT, conn);
		}

	}

	/**
	 * @param batchNumber
	 * @param paymentType
	 * @return
	 * @throws IOException
	 * @throws SQLException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 */
	private List<PGEODRecordVO> getErroredBillingRecords(long batchNumber, String paymentType) throws IOException,
			SQLException, ParserConfigurationException, SAXException {
		return dao.getFailedCCBillingDetailsList(batchNumber, paymentType);
	}

	/**
	 * @param batchNumber
	 * @param paymentType
	 * @throws Throwable
	 */
	private void updateHeaderIndicator(long batchNumber, String paymentType) throws Throwable {

		dao.updateHeaderIndicator(batchNumber, paymentType);
	}

	/**
	 * @param batchNumber
	 * @return
	 * @throws Throwable
	 */
	private boolean checkAndIncrementProcessedCount(long batchNumber) throws Exception {
		return dao.checkAndIncrementProcessedCount(batchNumber);
	}

	/**
	 * @param paymentRecordMap
	 * @param refundRecordMap
	 * @param mvo
	 * @throws Exception
	 */
	private void consolidatePreSettlements(Map<String, List<PGEODRecordVO>> paymentRecordMap,
			Map<String, List<PGEODRecordVO>> refundRecordMap, PGEODMessageVO mvo) throws Exception {
		int c = 0;
		BigDecimal bgr = new BigDecimal(0);
		BigDecimal bgp = new BigDecimal(0);
		PGEODRecordVO paymentRecordVO = null;

		
		logger.debug("Before Consolidations Maps " + paymentRecordMap + "\n::" + refundRecordMap);
		Iterator<Entry<String, List<PGEODRecordVO>>> iterator = paymentRecordMap.entrySet().iterator();

		Map<String, List<PGEODRecordVO>> tempPaymentMap = new HashMap<String, List<PGEODRecordVO>>();
		while (iterator.hasNext()) {

			Entry<String, List<PGEODRecordVO>> entry = iterator.next();
			String authID = entry.getKey();
			List<PGEODRecordVO> prvoList = entry.getValue();
			List<PGEODRecordVO> rrVOList = refundRecordMap.get(authID);

			bgp = getListTotal(prvoList);
			bgr = getListTotal(rrVOList); // Summing up all the partial refunds
											// on the same Auth Trans ID

			if (prvoList != null)
				paymentRecordVO = prvoList.get(0); // Since Auth ID is different
													// for each Payment,
													// possibly only one Payment
													// Record

			c = bgp.compareTo(bgr);
			logger.debug("value of c is: " + c);
			if (c > 0) {

				// payment total is greater than refund total
				if (bgr.doubleValue() > 0)
					paymentRecordVO.setOrderAmount(bgp.subtract(bgr).doubleValue());

				List<PGEODRecordVO> list = new ArrayList<PGEODRecordVO>();
				list.add(paymentRecordVO);
				tempPaymentMap.put(authID, list);
				refundRecordMap.remove(authID);// Remove All refunds for Auth ID
												// since those were
												// consolidated.
			} else if (c == 0) {

				// payment total is same as refund total
				billPaymentList(prvoList, mvo.getBatchTime());
				iterator.remove();

				// billRefundList(rrVOList,mvo.getBatchTime()); Commented Since
				// marking as Billed at the end
				refundRecordMap.remove(authID);

			} else {
				// Refund Total is greater than Payment Total
				refundRecordMap.remove(authID);
				logger.error("Payment is less than the Refund so ignoring refund & Continue with Payment ID List"
						+ prvoList + "Refund ID List ::" + rrVOList);
			}
			// Bill all refunds.
			billRefundList(rrVOList, mvo.getBatchTime());
		}
		paymentRecordMap.putAll(tempPaymentMap);
		logger.debug("After Consolidations Maps " + paymentRecordMap + "\n::" + refundRecordMap);

	}

	/**
	 * Mark all payments in the list Billed.
	 */
	private void billPaymentList(List<PGEODRecordVO> pList, long eodBatchTime) throws Exception {
		logger.debug("Entering billPaymentList...");
		if (pList != null) {
			logger.debug("pList size is: " + pList.size());
			for (PGEODRecordVO recordVO : pList) {
				dao.doUpdatePaymentBillStatus(recordVO.getPaymentId(), eodBatchTime, ARConstants.ACCTG_STATUS_BILLED);
			}
		}
		logger.debug("Exiting billPaymentList...");
	}

	/**
	 * Mark all Refunds in the list Billed.
	 */
	private void billRefundList(List<PGEODRecordVO> rList, long eodBatchTime) throws Exception {
		logger.debug("Entering billRefundList...");
		if (rList != null) {
			logger.debug("rList size is: " + rList.size());
			for (PGEODRecordVO recordVO : rList) {
				dao.doUpdateRefundBillStatus(recordVO.getRefundId(), eodBatchTime, ARConstants.ACCTG_STATUS_BILLED);
			}
		}
		logger.debug("Exiting billRefundList...");
	}

	private BigDecimal getListTotal(List<PGEODRecordVO> recordList) throws Exception {
		logger.debug("Entering getListTotal...");
		BigDecimal total = new BigDecimal(0);
		double amount = 0;

		if (recordList != null) {
			for (PGEODRecordVO recordVO : recordList) {
				amount = recordVO.getOrderAmount();
				logger.debug("amount is: " + amount);
				total = total.add(new BigDecimal(String.valueOf(amount)));
			}
		}
		logger.debug("returning total: " + total.doubleValue());
		logger.debug("Exiting getListTotal...");

		return total;
	}

	private void sendSystemMessage(Connection conn, Exception t, boolean page) {

		try {
			logger.debug("Sending system message...");
			String source = "";
			String logMessage = t.getMessage();
			if (page) {
				source = ARConstants.SM_PAGE_SOURCE;
			} else {
				source = ARConstants.SM_NOPAGE_SOURCE;
			}
			SystemMessager.send(logMessage, t, source, SystemMessager.LEVEL_PRODUCTION,
					ARConstants.EOD_BILLING_PROCESS_ERROR_TYPE, ARConstants.SM_PAGE_SUBJECT, conn);

		} catch (Exception ex) {
			logger.error(ex);
		} finally {
			if (logger.isDebugEnabled()) {
				logger.debug("Exiting sendSystemMessage");
			}
		}

	}

	/**
	 * @param recordMap
	 * @param mvo
	 * @throws Throwable
	 */
	private void processPGRecords(Map<String, List<PGEODRecordVO>> recordMap, PGEODMessageVO mvo) throws Exception {

		// Consolidating Based on Auth-Code for Pre-Settlements Payments and
		// Refunds
		long billingDetailId = 0;
		String billingDetailIDStr = null;

		for (String paymentAuthID : recordMap.keySet()) {

			logger.debug("paymentAuthID ::" + paymentAuthID);
			List<PGEODRecordVO> paymentRecordVOList = recordMap.get(paymentAuthID);

			for (PGEODRecordVO paymentRecordVO : paymentRecordVOList) {

				// create billing detail
				billingDetailIDStr = dao.doCreatePGBillingDetail(paymentRecordVO, mvo.getBatchNumber());
				billingDetailId = Long.parseLong(billingDetailIDStr);
				paymentRecordVO.setBillingDetailId(billingDetailId);

				// Payment
				if (PG_PAYMENT_TRANSACTION_TYPE.equals(paymentRecordVO.getTransactionType()))
					dao.doUpdatePaymentBillStatus(paymentRecordVO.getPaymentId(), mvo.getBatchTime(),
							ARConstants.ACCTG_STATUS_BILLED); // Billed for
																// Payment

				// Refund
				if (PG_REFUND_TRANSACTION_TYPE.equals(paymentRecordVO.getTransactionType()))
					dao.doUpdateRefundBillStatus(paymentRecordVO.getRefundId(), mvo.getBatchTime(),
							ARConstants.ACCTG_STATUS_BILLED); // Billed for
																// Refund

				performSettlements(paymentRecordVO, mvo);

			}
		}
	}

	/**
	 * @param pgEODRecordVO
	 * @param mvo
	 * @throws Throwable
	 */
	private void performSettlements(PGEODRecordVO pgEODRecordVO, PGEODMessageVO mvo) throws Exception {

		String pgSVCTimeOut = ConfigurationUtil.getInstance().getFrpGlobalParm(ARConstants.SERVICE,
				"PAYMENT_GATEWAY_SVC_TIMEOUT");
		CreditCardClient ccClient = new CreditCardClient(pgSVCTimeOut);

		String settlementUrl = null;
		SettlementRequest settlmentRequest = new SettlementRequest();
		SettlementResponse  settlmentResponse  = new SettlementResponse();
		RefundRequest refundRequest = new RefundRequest();
		RefundResponse refundResponse = new RefundResponse();

		try {

			settlementUrl = getSettlementURL(pgEODRecordVO.getTransactionType());
			long startTime = new java.util.Date().getTime();

			if (PG_PAYMENT_TRANSACTION_TYPE.equals(pgEODRecordVO.getTransactionType())) {
				settlmentRequest.setMerchantReferenceId(pgEODRecordVO.getMerchantRefId());
				settlmentRequest.setAmount(Double.toString(pgEODRecordVO.getOrderAmount()));
				settlmentRequest.setAuthorizationTransactionId(pgEODRecordVO.getAuthTranId()); // Auth
				settlmentRequest.setPaymentType(getCreditCardType(pgEODRecordVO.getPaymentType()).value().toUpperCase());	
				if (mvo.getRetryCounter() > 0)
					settlmentRequest.setRetry(true);
				else
					settlmentRequest.setRetry(false);

			try{
				settlmentResponse = (SettlementResponse) ccClient.settleCC(settlmentRequest, settlementUrl);
				logger.info("Payment Gateway Settlement Response for:: "+pgEODRecordVO.getMerchantRefId()+"::" + settlmentResponse);
			}catch (Exception e) {
					logger.error("Exception while Payment Gateway Settlement/Refund And it will re-processed:: re-process count::"
							+ mvo.getRetryCounter());
					logger.error(e.getMessage());
					settlmentResponse.setStatus(ERROR); // Service Unavailable
					settlmentResponse.setReasonCode(SERVICE_ERR_SATUS_CODE);
					
			}
				
				if (checkAndProcessSettlementResponse(pgEODRecordVO, settlmentResponse, mvo.getBatchTime())) {
					reProcessSettlement(pgEODRecordVO, mvo);
				}
			}
			if (PG_REFUND_TRANSACTION_TYPE.equals(pgEODRecordVO.getTransactionType())) {
				refundRequest.setMerchantReferenceId(pgEODRecordVO.getMerchantRefId());
				refundRequest.setAmount(Double.toString(pgEODRecordVO.getOrderAmount()));
				refundRequest.setSettlementTransactionId(pgEODRecordVO.getSettlmentTransactionId()); // Settlement
				refundRequest.setPaymentType(getCreditCardType(pgEODRecordVO.getPaymentType()).value().toUpperCase());																						// Tran
																										// Id
				refundRequest.setRequestToken(pgEODRecordVO.getRequestToken());
				if (mvo.getRetryCounter() > 0)
					refundRequest.setRetry(true);
				else
					refundRequest.setRetry(false);
				
				try{
				
					refundResponse = (RefundResponse) ccClient.refundCC(refundRequest, settlementUrl);
					logger.info("Payment Gateway Refund Response for:: "+pgEODRecordVO.getMerchantRefId()+"::" + refundResponse);
				}catch (Exception e) {
					logger.error("Exception while Payment Gateway Settlement/Refund And it will re-processed:: re-process count::"+mvo.getRetryCounter());
					logger.error(e.getMessage());
					
					refundResponse.setStatus(ERROR); // Service Unavailable
					refundResponse.setReasonCode(SERVICE_ERR_SATUS_CODE);
				}	
				if (checkAndProcessSettlementResponse(pgEODRecordVO, populateSettlementResponse(refundResponse), mvo.getBatchTime())) {
					reProcessSettlement(pgEODRecordVO, mvo);
				}

			}

			logServiceResponseTracking(String.valueOf(pgEODRecordVO.getBillingDetailId()), PG_CC_SETTLEMENT, startTime);

		}  catch (Exception e) {
			logger.error("Exception in Payment Gateway Settlement/Refund "+e.getMessage());
			throw e;
		}
	}



	/**
	 * @param refundResponse
	 * @return
	 */
	private SettlementResponse populateSettlementResponse(RefundResponse refundResponse) {

		SettlementResponse settlementResponse = new SettlementResponse();
		
		settlementResponse.setStatus(refundResponse.getStatus());
		settlementResponse.setSettlementDate(refundResponse.getRefundDate());
		settlementResponse.setSettlementTransactionId(refundResponse.getRefundTransactionId());
		settlementResponse.setMessage(refundResponse.getMessage());
		settlementResponse.setReasonCode(refundResponse.getReasonCode());
		
		return settlementResponse;
	}



	private void updateSettlmentResponse(SettlementResponse settlementResponse, PGEODRecordVO pgEODRecordVO)
			throws Exception {

		this.dao.doUpdatePGSettlementResponse(settlementResponse, pgEODRecordVO);

	}

	private boolean checkAndProcessSettlementResponse(PGEODRecordVO pgEODRecordVO,
			SettlementResponse settlmentResponse, long batchTime) throws Exception {
		boolean reProcess = false;
		
		if (settlmentResponse != null) {
			if (settlmentResponse.getStatus() != null) {
				logger.info("settlmentResponse.getStatus() " + settlmentResponse.getStatus() + "for Billing Id "
						+ pgEODRecordVO.getBillingDetailId());
				if (settlmentResponse.getStatus().equalsIgnoreCase(APPROVED)) {
					settlmentResponse.setStatus(SETTLED);
					settlmentResponse.setSettlementDate(massageSettlementDate(settlmentResponse.getSettlementDate()));

					// updates Settlement Response in Billing_Detail_pg_cc and
					// payment tables
					updateSettlmentResponse(settlmentResponse, pgEODRecordVO);

					// Updates Order Bills Status as Settled.This will update
					// only Order Bills with Settled.
					updatePaymentBillStatus(batchTime, pgEODRecordVO.getPaymentId(), ARConstants.ACCTG_STATUS_SETTLED);

				} else if (settlmentResponse.getStatus().equalsIgnoreCase(DECLINED)) {

					//settlmentResponse.setStatus(DECLINED);
					settlmentResponse.setSettlementDate(massageSettlementDate(settlmentResponse.getSettlementDate()));
					
					// updates Settlement Response in Billing_Detail_pg_cc and
					// payment tables
					updateSettlmentResponse(settlmentResponse, pgEODRecordVO);
					reProcess = false;
				}  else if (settlmentResponse.getStatus().equalsIgnoreCase(ERROR)) { 
					// settlmentResponse.setStatus(ERROR); 
					settlmentResponse.setReasonCode(SERVICE_ERR_SATUS_CODE);
					updateSettlmentResponse(settlmentResponse, pgEODRecordVO);
					reProcess = true;
				}
			}
		} else {
			StringBuffer sb = new StringBuffer("Payment Gateway Settlment/Refund Process failed for " + "Payment ID:: ")
					.append(pgEODRecordVO.getPaymentId())
					.append(" Billing Deatil ID::").append(pgEODRecordVO.getBillingDetailId())
					.append(" for the Batch ID:: ").append(pgEODRecordVO.getBillingHeaderId())
					.append(" It will be retried....");
			logger.error(sb.toString());
			reProcess = true;
		}

		return reProcess;
	}

	/*private String getPreparedErrorMessagefromMap(List<Errors> errorMessages) {

		StringBuffer sb = new StringBuffer();
		for (Errors error : errorMessages) {
			sb.append(error.getMessage());
		}
		return sb.toString();
	}*/

	/** This function parses two different Date formats 
	 * @param jsonDate
	 * @return
	 * @throws Exception
	 */
	private static Date getSQLDate(String jsonDate) throws Exception {
		
		//String jsonDate ="2018-09-05T01:38:18.45";
		//String jsonDate ="2018-09-05T02:01:18.0954874-07:00";
		
		SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSSZ");
		
		java.sql.Date  sqlDate = null;
		StringBuffer jsonDateBuffer = new StringBuffer(jsonDate);
		jsonDateBuffer.replace(jsonDate.lastIndexOf(":"), jsonDate.lastIndexOf(":") + 1, "");
		
		try {
			sqlDate = new java.sql.Date(DATE_FORMAT.parse(jsonDateBuffer.toString()).getTime());
		} catch (ParseException e) {
			logger.error("Excetpion while parsing the Date with Format:: "+ DATE_FORMAT + ":: " + e.getMessage());
			DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SS");
			logger.info("So going to parse with other Date Format:: "+ DATE_FORMAT);
			try {
				sqlDate = new java.sql.Date(DATE_FORMAT.parse(jsonDate.toString()).getTime());
			} catch (ParseException pe) {
				logger.error("Excetpion while parsing with Other Date Format:: "
						+ DATE_FORMAT + ":: " + e.getMessage());
				sqlDate = null;
				throw pe;
			}
		}
		return sqlDate;
				
	}

	public static void logServiceResponseTracking(String requestId, String serviceMethod, long startTime) {
		Connection connection = null;
		try {

			long responseTime = System.currentTimeMillis() - startTime;
			logger.debug("logServiceResponseTracking " + responseTime);
			ServiceResponseTrackingVO srtVO = new ServiceResponseTrackingVO();
			srtVO.setServiceName(PG_CC_SETTLEMENT);
			srtVO.setServiceMethod(serviceMethod);
			srtVO.setTransactionId(requestId);
			srtVO.setResponseTime(responseTime);
			srtVO.setCreatedOn(new java.util.Date());

			connection = DataSourceUtil.getInstance().getConnection("ORDER SCRUB");
			ServiceResponseTrackingUtil srtUtil = new ServiceResponseTrackingUtil();
			srtUtil.insertWithRequest(connection, srtVO);
		} catch (Exception e) {
			logger.error("Exception occured while persisting PG Settlements Service response time lines.");
		}

	}

	private Date massageSettlementDate(Object settlementDate) throws Exception {
		Date massagedDate = new java.sql.Date(new java.util.Date().getTime());
		if (settlementDate != null)
			massagedDate = getSQLDate((String) settlementDate);

		return massagedDate;

	}

	/*
	 * Auth Trans ID ,Payment Object
	 */
	private Map<String, List<PGEODRecordVO>> getPGPaymentsMap(List<String> payments, boolean isRefundId)
			throws Exception {

		if (logger.isDebugEnabled()) {
			logger.debug("Entering createPGPamentsMap. Size=" + (payments == null ? 0 : payments.size()));
		}
		Map<String, List<PGEODRecordVO>> paymentMap = new HashMap<String, List<PGEODRecordVO>>();
		List<PGEODRecordVO> paymentRecordList = loadPGPaymentsById(payments, isRefundId);
		List<PGEODRecordVO> paymentAuthList = null;
		List<String> authList = new ArrayList<String>();

		if (paymentRecordList != null) {
			logger.debug("Loaded payment list size: " + paymentRecordList.size());

			for (PGEODRecordVO pgEODRecordVO : paymentRecordList) {
				{

					String authID = pgEODRecordVO.getAuthTranId();
					paymentAuthList = paymentMap.get(authID);

					// if AuthID number does not exist in map, add it.
					if (paymentAuthList == null) {
						logger.debug("Create new payment entry list for Auth ID");
						paymentAuthList = new ArrayList<PGEODRecordVO>();
						authList.add(authID);
					}
					paymentAuthList.add(pgEODRecordVO);
					paymentMap.put(authID, paymentAuthList);
				}
			}
		}
		if (logger.isDebugEnabled()) {
			logger.debug("Loaded Map size:: " + paymentMap.size());
			logger.debug("Exiting getPGPaymentsMap.");
		}

		return paymentMap;

	}

	private List<PGEODRecordVO> loadPGPaymentsById(List<String> paymentIds, boolean isRefundId) throws SQLException,
			Exception {

		if (logger.isDebugEnabled()) {
			logger.debug("Entering loadPGPaymentsById");
		}
		List<PGEODRecordVO> payments = null;
		String paymentId = null;
		PGEODRecordVO recordvo = null;

		try {
			if (paymentIds != null) {

				payments = new ArrayList<PGEODRecordVO>();

				for (int i = 0; i < paymentIds.size(); i++) {
					paymentId = (String) paymentIds.get(i);
					logger.debug("Loading payment with id: " + paymentId);
					CachedResultSet rs = dao.doGetPGPaymentById(paymentId);
					recordvo = new PGEODRecordVO();
					if (rs.next()) {

						recordvo.setPaymentId(rs.getLong("payment_id"));
						recordvo.setPaymentType(rs.getString("payment_type"));
						recordvo.setAuthCode(rs.getString("auth_number"));
						recordvo.setMasterOrderNumber(rs.getString("master_order_number"));
						String paymentIndicator = rs.getString("payment_indicator");

						if (paymentIndicator != null && paymentIndicator.equals("R")) {
							recordvo.setRefundId(rs.getLong("refund_id"));
							recordvo.setOrderAmount(rs.getDouble("debit_amount"));
							recordvo.setRequestToken(rs.getString("request_token"));
							recordvo.setTransactionType(ARConstants.TRANSACTION_TYPE_REFUND);

							recordvo.setBillType(ARConstants.CONST_REFUND);

						} else if (paymentIndicator != null && paymentIndicator.equals("P")) {
							recordvo.setOrderAmount(rs.getDouble("credit_amount"));
							recordvo.setTransactionType(ARConstants.TRANSACTION_TYPE_PURCHASE);

							String addBillingId = rs.getString("additional_bill_id");
							if (addBillingId != null && !addBillingId.isEmpty())
								recordvo.setBillType(ARConstants.CONST_ADD_BILL);
							else
								recordvo.setBillType(ARConstants.CONST_BILL); // Payment
						}
						recordvo.setAuthTranId(rs.getString("AUTHORIZATION_TRANSACTION_ID"));
						recordvo.setVoiceAuthFlag(rs.getString("is_voice_auth"));
						recordvo.setSettlmentTransactionId(rs.getString("settlement_transaction_id"));
						recordvo.setRefundTransactionId(rs.getString("REFUND_TRANSACTION_ID"));
						recordvo.setMerchantRefId(rs.getString("merchant_ref_id"));

					} else {
						String errorMsg = "No payment exists for payment id " + paymentId;
						logger.error(errorMsg);
						throw new Exception(errorMsg);
					}
					payments.add(recordvo);
				}
			}
		} finally {
			if (logger.isDebugEnabled()) {
				logger.debug("Exiting loadPaymentsByIds");
			}
		}

		return payments;
	}

	private void updatePaymentBillStatus(long timeStamp, long paymentId, String acctgStatusSettled) throws Exception {

		this.dao.doUpdatePaymentBillStatus(paymentId, timeStamp, acctgStatusSettled);
	}

	/**
	 * Create a unique session id for locking.
	 * 
	 * @return
	 * @throws Throwable
	 */
	protected String getLockSessionId() throws Throwable {
		long current = Calendar.getInstance().getTimeInMillis();
		return String.valueOf(current);
	}

	/**
	 * Sends a system message. The paging system looks for source of 'End of
	 * Day'. For any messages with that souce, it will send a nopage email
	 * rather than paging or include in the no page summary.
	 * 
	 * @param con
	 *            DB connection
	 * @param t
	 *            Throwable object
	 * @param whether
	 *            to page or nopage
	 * @throws Throwable
	 */
	protected void sendSystemMessage(Connection con, Throwable t, boolean page) throws Throwable {
		try {
			logger.debug("Sending system message...");
			String source = "";
			String logMessage = t.getMessage();
			if (page) {
				source = ARConstants.SM_PAGE_SOURCE;
			} else {
				source = ARConstants.SM_NOPAGE_SOURCE;
			}
			SystemMessager.send(logMessage, t, source, SystemMessager.LEVEL_PRODUCTION,
					ARConstants.EOD_BILLING_PROCESS_ERROR_TYPE, ARConstants.SM_PAGE_SUBJECT, con);

		} catch (Exception ex) {
			logger.error(ex);
		} finally {
			if (logger.isDebugEnabled()) {
				logger.debug("Exiting sendSystemMessage");
			}
		}
	}

	/**
	 * @param vo
	 * @param mvo
	 * @throws Throwable
	 */
	public void reProcessSettlement(PGEODRecordVO vo, PGEODMessageVO mvo) throws Exception {

		long timeToLive = 0;
		List<String> list = new ArrayList<String>();
		int count = 5;
		int delay = 0;

		try {
			count = Integer.parseInt(ConfigurationUtil.getInstance().getFrpGlobalParm(ARConstants.SERVICE,
					ARConstants.PAYMENT_GATEWAY_SVC_RETRY_LIMIT));
			delay = Integer.parseInt(ConfigurationUtil.getInstance().getFrpGlobalParm(ARConstants.SERVICE,
					ARConstants.PAYMENT_GATEWAY_SVC_DELAY_TIME_IN_RETRY));
		} catch (NumberFormatException e) {
			logger.error("Exception while Parsing the Global Parameter of PG.Continuing with Defaults..."
					+ e.getMessage());
		}

		if (mvo.getRetryCounter() < count) {
			if (PG_PAYMENT_TRANSACTION_TYPE.equals(vo.getTransactionType())) // Payment
			{
				List<EODPaymentVO> pList = new ArrayList<EODPaymentVO>();

				EODPaymentVO eodPaymentVO = new EODPaymentVO(mvo.getMasterOrderNumber(), String.valueOf(vo
						.getPaymentId()), mvo.getPaymentMethodId());
				pList.add(eodPaymentVO);
				mvo.setPaymentIdList(pList);
			}
			if (PG_REFUND_TRANSACTION_TYPE.equals(vo.getTransactionType())) // Refund
			{
				// List pList = new ArrayList();
				List<EODPaymentVO> rList = new ArrayList<EODPaymentVO>();
				EODPaymentVO eodPaymentVO = new EODPaymentVO(mvo.getMasterOrderNumber(), String.valueOf(vo
						.getRefundId()), mvo.getPaymentMethodId());
				rList.add(eodPaymentVO);
				mvo.setRefundIdList(rList);

			}
			mvo.setBillingDetailId(vo.getBillingDetailId());
			mvo.setRetryCounter(mvo.getRetryCounter() + 1);
			list.add((mvo.toMessageXML()));
			logger.info("PG EOD Billing ID ::" + mvo.getBillingDetailId() + "::Reprocessing Number::"
					+ mvo.getRetryCounter());
			AccountingUtil.dispatchJMSMessageList(ARConstants.PG_EOD_CART, list, timeToLive, delay);
		}

	}

	public String getSettlementURL(String transactionType) throws Exception {

		StringBuffer settlementWsUrl = new StringBuffer(ConfigurationUtil.getInstance().getFrpGlobalParm(
				ARConstants.SERVICE, ARConstants.PAYMENT_GATEWAY_SVC_URL));
		if (ARConstants.TRANSACTION_TYPE_PURCHASE.equals(transactionType)) {
			settlementWsUrl.append("creditcard/settle");
		}
		if (ARConstants.TRANSACTION_TYPE_REFUND.equals(transactionType)) {
			settlementWsUrl.append("creditcard/refund");
		}
		return settlementWsUrl.toString();

	}

	public void reProcessPG(PGEODMessageVO eodMessageVO) throws Throwable {

		PGEODRecordVO pgEodRecordVO = getBillingDetailRecord(eodMessageVO.getBillingDetailId(),
				eodMessageVO.getBatchNumber());
		performSettlements(pgEodRecordVO, eodMessageVO);

	}

	private PGEODRecordVO getBillingDetailRecord(long billingId, long headerId) throws Exception {

		CachedResultSet rs = dao.doGetBillingCCDetailsPG(billingId, headerId);
		PGEODRecordVO pgEodRecordVO = new PGEODRecordVO();
		if (rs.next()) {
			pgEodRecordVO.setBillingDetailId(rs.getLong("BILLING_DETAIL_ID"));
			pgEodRecordVO.setBillingHeaderId(rs.getLong("BILLING_HEADER_ID"));
			pgEodRecordVO.setOrderAmount(rs.getDouble("ORDER_AMOUNT"));
			pgEodRecordVO.setMasterOrderNumber(rs.getString("ORDER_NUMBER"));
			pgEodRecordVO.setTransactionType(rs.getString("TRANSACTION_TYPE"));
			pgEodRecordVO.setAuthTranId(rs.getString("AUTH_TRANSACTION_ID"));
			pgEodRecordVO.setSettlmentTransactionId(rs.getString("SETTLEMENT_TRANSACTION_ID"));
			pgEodRecordVO.setRequestToken(rs.getString("REQUEST_ID"));
			pgEodRecordVO.setMerchantRefId(rs.getString("merchant_ref_id"));
			pgEodRecordVO.setPaymentId(rs.getLong("payment_id"));
			pgEodRecordVO.setPaymentType(rs.getString("payment_type"));
		}

		return pgEodRecordVO;
	}
	
	private static CardTypeType getCreditCardType(String creditCardType) {
		// VISA, MASTERCARD, DISCOVER,DINERS,AMERICANEXPRESS
		if (creditCardType.equals("DC")) {
			return CardTypeType.DINERS;
		} else if (creditCardType.equals("DI")) {
			return CardTypeType.DISCOVER;
		} else if (creditCardType.equals("MC")) {
			return CardTypeType.MASTER_CARD;
		} else if (creditCardType.equals("VI")) {
			return CardTypeType.VISA;
		} else if (creditCardType.equals("AX")) {
			return CardTypeType.AMERICAN_EXPRESS;
		}
		return null;
	}

		public static void main(String[] args) {
			
				try
				{
					System.out.println(PGEODShoppingCartBO.getSQLDate("2018-09-05T03:46:47.66"));
					//System.out.println(PGEODShoppingCartBO.getSQLDate("2018-06-14TT01:57:14.7840981-07:00"));
				}catch(Exception e)
				{
					e.printStackTrace();
					
				}
				
			
		}
		
	
	
}
