package com.ftd.accountingreporting.filter;

import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.util.HashMap;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.xml.parsers.ParserConfigurationException;

import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

public class DataFilter implements Filter
{
    private FilterConfig _filterConfig = null;
    private Logger logger = new Logger("com.ftd.accountingreporting.filter.DataFilter");

    private static final String ADMIN_ACTION = "adminaction";
    private static final String SITE_NAME = "sitename";

    private static final String HEADER_CONTENT_TYPE = "CONTENT-TYPE";
    private static final String MULTPART_FORM_DATA = "multipart/form-data";

    private static final String PROPERTY_FILE = "security-config.xml";

    private static final String ERROR_PAGE = "error_page";


    public void init(FilterConfig filterConfig)
        throws ServletException
    {
        _filterConfig = filterConfig;
    }


    public void destroy()
    {
        _filterConfig = null;
    }


    /**
     * Get shared data.
     * 
     *   
     * @param ServletRequest  - input request object
     * @param ServletResponse - object used to respond to the user
     * @param FilterChain     - next program to be executed in the chain
     * 
     * @throws IOException
     * @throws ServletException
     */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
        throws IOException, ServletException
    {
        logger.debug("Entering DataFilter...");
        String adminAction = null,
               siteName = null;
               
        String gccAction = "";
        String isMultipleGcc = "";
        String gccUser = "";

        HashMap parameters  = new HashMap();
            
        if(request.getAttribute("parameters")!=null)
        {
          parameters=(HashMap)request.getAttribute("parameters");
        }
        if(isMultipartFormData(((HttpServletRequest)request )))
        {
           gccAction = (String) request.getAttribute(ARConstants.COMMON_GCC_ACTION);
           isMultipleGcc = (String) request.getAttribute(ARConstants.ACTION_PARM_IS_MULTIPLE_GCC);
           gccUser = (String) request.getAttribute(ARConstants.COMMON_GCC_USER);
        }
        else
        {
          gccAction = request.getParameter(ARConstants.COMMON_GCC_ACTION);
          isMultipleGcc = request.getParameter(ARConstants.ACTION_PARM_IS_MULTIPLE_GCC);
          gccUser = request.getParameter(ARConstants.COMMON_GCC_USER);
        }
        
        logger.debug("gcc_action is: " + gccAction);
        logger.debug("is_multiple_gcc is: " + isMultipleGcc);
        logger.debug("gccUser is: " + gccUser);
        parameters.put(ARConstants.COMMON_GCC_ACTION,gccAction==null?"":gccAction);
        parameters.put(ARConstants.ACTION_PARM_IS_MULTIPLE_GCC,isMultipleGcc==null?"":isMultipleGcc);
        parameters.put(ARConstants.COMMON_GCC_USER,gccUser==null?"":gccUser);
    
          if ((request.getParameter(ARConstants.SC_CUST_IND)) !=null)
      {
        if (!((request.getParameter(ARConstants.SC_CUST_IND)).equalsIgnoreCase("") )  )
          parameters.put(ARConstants.SC_CUST_IND, "Y");
        else
          parameters.put(ARConstants.SC_CUST_IND, "N");
      }
      else
      {
        parameters.put(ARConstants.SC_CUST_IND, "N");
      }
            
      if ((request.getParameter(ARConstants.SC_RECIP_IND)) !=null)
      {
        if (!((request.getParameter(ARConstants.SC_RECIP_IND)).equalsIgnoreCase("") )  )
          parameters.put(ARConstants.SC_RECIP_IND, "Y");
        else
          parameters.put(ARConstants.SC_RECIP_IND, "N");
      }
      else
      {
        parameters.put(ARConstants.SC_RECIP_IND, "N");
      }
        parameters.put(ARConstants.SC_ORDER_NUMBER, ((request.getParameter(ARConstants.SC_ORDER_NUMBER))               !=null?request.getParameter(ARConstants.SC_ORDER_NUMBER).toString().trim():""));
        parameters.put(ARConstants.SC_LAST_NAME, ((request.getParameter(ARConstants.SC_LAST_NAME))                  !=null?request.getParameter(ARConstants.SC_LAST_NAME).toString().trim():""));
        parameters.put(ARConstants.SC_PHONE_NUMBER,((request.getParameter(ARConstants.SC_PHONE_NUMBER))               !=null?request.getParameter(ARConstants.SC_PHONE_NUMBER).toString().trim():""));
        parameters.put(ARConstants.SC_EMAIL_ADDRESS,((request.getParameter(ARConstants.SC_EMAIL_ADDRESS))              !=null?request.getParameter(ARConstants.SC_EMAIL_ADDRESS).toString().trim():""));
        parameters.put(ARConstants.SC_ZIP_CODE, ((request.getParameter(ARConstants.SC_ZIP_CODE))                   !=null?request.getParameter(ARConstants.SC_ZIP_CODE).toString().trim():""));
        parameters.put(ARConstants.SC_CC_NUMBER,((request.getParameter(ARConstants.SC_CC_NUMBER))                  !=null?request.getParameter(ARConstants.SC_CC_NUMBER).toString().trim():""));
        parameters.put(ARConstants.SC_CUST_NUMBER,((request.getParameter(ARConstants.SC_CUST_NUMBER))                !=null?request.getParameter(ARConstants.SC_CUST_NUMBER).toString().trim():""));
        parameters.put(ARConstants.SC_TRACKING_NUMBER,((request.getParameter(ARConstants.SC_TRACKING_NUMBER))            !=null?request.getParameter(ARConstants.SC_TRACKING_NUMBER).toString().trim():""));
        parameters.put(ARConstants.SC_MEMBERSHIP_NUMBER,((request.getParameter(ARConstants.SC_MEMBERSHIP_NUMBER))          !=null?request.getParameter(ARConstants.SC_MEMBERSHIP_NUMBER).toString().trim():""));
        
        parameters.put(ARConstants.ADMIN_ACTION, ((request.getParameter(ARConstants.ADMIN_ACTION))!=null?request.getParameter(ARConstants.ADMIN_ACTION).toString().trim():""));
        
        parameters.put(ARConstants.CONTEXT,((request.getParameter(ARConstants.CONTEXT))          !=null?request.getParameter(ARConstants.CONTEXT).toString().trim():""));
        parameters.put(ARConstants.SEC_TOKEN,((request.getParameter(ARConstants.SEC_TOKEN))          !=null?request.getParameter(ARConstants.SEC_TOKEN).toString().trim():""));

        parameters.put(ARConstants.CBR_ACTION,((request.getParameter(ARConstants.CBR_ACTION))          !=null?request.getParameter(ARConstants.CBR_ACTION).toString().trim():""));
        parameters.put(ARConstants.ORDER_GUID,((request.getParameter(ARConstants.ORDER_GUID))          !=null?request.getParameter(ARConstants.ORDER_GUID).toString().trim():""));
        parameters.put(ARConstants.MASTER_ORDER_NUMBER,((request.getParameter(ARConstants.MASTER_ORDER_NUMBER))          !=null?request.getParameter(ARConstants.MASTER_ORDER_NUMBER).toString().trim():""));
        parameters.put(ARConstants.APPLICATIONCONTEXT,((request.getParameter(ARConstants.APPLICATIONCONTEXT))          !=null?request.getParameter(ARConstants.APPLICATIONCONTEXT).toString().trim():""));
        parameters.put(ARConstants.UO_DISPLAY_PAGE,((request.getParameter(ARConstants.UO_DISPLAY_PAGE))          !=null?request.getParameter(ARConstants.UO_DISPLAY_PAGE).toString().trim():""));
        parameters.put(ARConstants.ORDER_DETAIL_ID,((request.getParameter(ARConstants.ORDER_DETAIL_ID))          !=null?request.getParameter(ARConstants.ORDER_DETAIL_ID).toString().trim():""));    
        parameters.put(ARConstants.EXTERNAL_ORDER_NUMBER,((request.getParameter(ARConstants.EXTERNAL_ORDER_NUMBER))          !=null?request.getParameter(ARConstants.EXTERNAL_ORDER_NUMBER).toString().trim():""));    
        
        parameters.put(ARConstants.REPORT_MAIN_MENU,((request.getParameter(ARConstants.REPORT_MAIN_MENU))!=null?request.getParameter(ARConstants.REPORT_MAIN_MENU).trim():"accounting"));    
       
        
        //Timer Filter
        populateTimerInfo(request, parameters);
        
        //Header Info
        populateHeaderInfo(request, parameters);
       
        //Start Origin
        populateStartOriginInfo(request, parameters);
        
        request.setAttribute("parameters", parameters);
        chain.doFilter(request, response);
    }

    /**
     * Populate Start Origin data
     * 
     */
    private void populateStartOriginInfo(ServletRequest request, HashMap parameters)
    {
    
      parameters.put(ARConstants.START_ORIGIN,((request.getParameter(ARConstants.START_ORIGIN))             !=null?request.getParameter(ARConstants.START_ORIGIN).toString().trim():""));

    }


    /**
     * Populate Timer data
     * 
     */
    private void populateTimerInfo(ServletRequest request, HashMap parameters)
    {
    
      parameters.put(ARConstants.TIMER_CALL_LOG_ID,((request.getParameter(ARConstants.TIMER_CALL_LOG_ID))             !=null?request.getParameter(ARConstants.TIMER_CALL_LOG_ID).toString().trim():""));
      parameters.put(ARConstants.TIMER_COMMENT_ORIGIN_TYPE,((request.getParameter(ARConstants.TIMER_COMMENT_ORIGIN_TYPE))     !=null?request.getParameter(ARConstants.TIMER_COMMENT_ORIGIN_TYPE).toString().trim():""));
      parameters.put(ARConstants.TIMER_ENTITY_HISTORY_ID,((request.getParameter(ARConstants.TIMER_ENTITY_HISTORY_ID))       !=null?request.getParameter(ARConstants.TIMER_ENTITY_HISTORY_ID).toString().trim():""));

    }


    /**
     * Populate Header data
     * 
     */
    private void populateHeaderInfo(ServletRequest request, HashMap parameters)
    {
      parameters.put(ARConstants.H_BRAND_NAME,                   ((request.getParameter(ARConstants.H_BRAND_NAME))                  !=null?request.getParameter(ARConstants.H_BRAND_NAME).toString().trim():""));
      parameters.put(ARConstants.H_CUSTOMER_ORDER_INDICATOR,     ((request.getParameter(ARConstants.H_CUSTOMER_ORDER_INDICATOR))    !=null?request.getParameter(ARConstants.H_CUSTOMER_ORDER_INDICATOR).toString().trim():""));
      parameters.put(ARConstants.H_CUSTOMER_SERVICE_NUMBER,      ((request.getParameter(ARConstants.H_CUSTOMER_SERVICE_NUMBER))     !=null?request.getParameter(ARConstants.H_CUSTOMER_SERVICE_NUMBER).toString().trim():""));
      parameters.put(ARConstants.H_DNIS_ID,                      ((request.getParameter(ARConstants.H_DNIS_ID))                     !=null?request.getParameter(ARConstants.H_DNIS_ID).toString().trim():""));

    }


    /**
     * Returns true if request content type is multipart/form-data.
     * 
     * @param request HttpServlet request to be analyzed
     */
     public static boolean isMultipartFormData(HttpServletRequest request)
     {
        boolean isMultipartFormData = false;
        String headerContentType = request.getHeader(HEADER_CONTENT_TYPE);
        if(headerContentType != null  &&  headerContentType.startsWith(MULTPART_FORM_DATA))
        {
            isMultipartFormData = true;
        }

        return isMultipartFormData;
    }



}

