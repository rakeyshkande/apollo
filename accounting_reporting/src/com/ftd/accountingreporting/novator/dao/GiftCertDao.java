package com.ftd.accountingreporting.novator.dao;
import com.ftd.accountingreporting.dao.GiftCertificateCouponDAO;
import com.ftd.accountingreporting.novator.vo.GiftCertRestrictedSourceCodeVO;
import com.ftd.accountingreporting.novator.vo.GiftCertVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

/**
 * This is value object class which models the Gift response XML 
 * message from Novator. 
 * @author Charles Fox
 */
public class GiftCertDao 
{
    private Connection dbConnection = null;

    private Logger logger = 
        new Logger("com.ftd.accountingreporting.novator.dao.GiftCertDao");
        
    public GiftCertDao(Connection conn)
    {
        super();
        dbConnection = conn;
    }

    /**
     *  Call CLEAN.GIFT_CERTIFICATE_COUPON_PKG.GET_GCCREQ_BY_REQNUM_OR_GCCNUM(null, couponNumber)
     * @param couponNumber - String
     * @return GiftCertVO
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     */
    public GiftCertVO loadGiftCertData(String couponNumber)
        throws IOException, SAXException, SQLException, 
            ParserConfigurationException
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering loadGiftCertData");
            logger.debug("Coupon Number : " + couponNumber);
        }
        
        DataRequest request = new DataRequest();

        GiftCertVO retVO = new GiftCertVO();
        
        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_REQUEST_NUMBER", null);
            inputParams.put("IN_GC_COUPON_NUMBER", couponNumber);
            String requestNumber = new String();
            
            // build DataRequest object
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("GET_GCCREQ_BY_REQNUM_OR_GCCNUM");

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            CachedResultSet outputs = (CachedResultSet) dau.execute(request);
            
            while(outputs.next())
            { 
                retVO.setCouponNumber(couponNumber);
                retVO.setExpireDate(outputs.getDate("expiration_date"));
                retVO.setIssueAmount(outputs.getDouble("issue_amount"));
                retVO.setProgramID(outputs.getString("promotion_id"));
                requestNumber = outputs.getString("request_number");
            }
            retVO.setGiftCertRestrictedSourceCodeVO(loadRestrictedSourceCodes(requestNumber));
            
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting loadGiftCertData");
            } 
        } 
        return retVO;
    }
    
    private GiftCertRestrictedSourceCodeVO loadRestrictedSourceCodes(String requestNumber) throws
    IOException,SAXException,SQLException, ParserConfigurationException {
    	
    	GiftCertRestrictedSourceCodeVO restSourceCodeVO = new GiftCertRestrictedSourceCodeVO();    	
    	
    	List<String> srcCodesList = new ArrayList<String>();
    	DataRequest request = new DataRequest();
    	 try {
             /* setup store procedure input parameters */
             HashMap inputParams = new HashMap();
             inputParams.put("IN_REQUEST_NUMBER", requestNumber);
             
             // build DataRequest object
             request.setConnection(dbConnection);
             request.reset();
             request.setInputParams(inputParams);
             request.setStatementID("GET_RESTRICTED_SOURCE_CODES_BY_REQ_NUM_FOR_FEED");

             // get data
             DataAccessUtil srcCodeDao = DataAccessUtil.getInstance();
             CachedResultSet srcCodeOutputs = (CachedResultSet) srcCodeDao.execute(request);
             
             while(srcCodeOutputs.next())
             { 
                srcCodesList.add(srcCodeOutputs.getString("source_code"));
             }
             restSourceCodeVO.setRestrictedSourceCodes(srcCodesList);
             
         } finally {
             if(logger.isDebugEnabled()){
                logger.debug("Exiting loadGiftCertData");
             } 
         } 
    	return restSourceCodeVO; 
    }
}