package com.ftd.accountingreporting.novator.handler;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.exception.EmailDeliveryException;
import com.ftd.accountingreporting.novator.dao.GiftCertDao;
import com.ftd.accountingreporting.novator.vo.GiftCertRequestVO;
import com.ftd.accountingreporting.novator.vo.GiftCertResponseVO;
import com.ftd.accountingreporting.novator.vo.GiftCertVO;
import com.ftd.accountingreporting.novator.vo.GiftCertRestrictedSourceCodeVO;
import com.ftd.accountingreporting.util.EmailUtil;
import com.ftd.accountingreporting.util.HttpUtil;
import com.ftd.accountingreporting.util.SystemMessager;
import com.ftd.accountingreporting.util.XMLUtil;
import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.j2ee.ResourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.exception.SystemMessengerException;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.vo.NotificationVO;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.GiftCodeUtil;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.jms.JMSException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * This is the base class for the handlers.  
 * @author Charles Fox
 */
public class GiftCertMessageHandler 
{
    private Logger logger = 
        new Logger("com.ftd.accountingreporting.novator.handler.GiftCertMessageHandler");
    
    Connection dbConnection = null;

    private static final String JMS_PAYLOAD_OUTER = 
          "<?xml version=\"1.0\" encoding=\"UTF-8\"?><gift_cert_request><request_type>calyx</request_type><retry>0</retry>" +
          "<coupon_number>%s</coupon_number>%s</gift_cert_request>";
    private static final String JMS_PAYLOAD_INNER = 
          "<promo_id>%s</promo_id><issue_amount>%s</issue_amount><expire_date>%s</expire_date><sc_restriction>%s</sc_restriction>";
    
    public GiftCertMessageHandler(Connection conn)
    {
        super();
        dbConnection = conn;
    }

    /**
     *  �	lookup �Requeue Received Message� from ejb-jar.xml
     *  �	if the value for the entry is �true�, lookup 
     *      "Requeue Messages File Name" to get the filename for the 
     *      properties, and lookup "Requeue Message XPath" for the XPath of 
     *      the properties.
     *  �	select NodeList and return it. Do something like the following:
     * @param n/a
     * @return NodeList
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws NamingException
     * @throws XSLException
     */
     /*
    protected NodeList getRequeuePropertyList() 
        throws IOException, NamingException, SAXException, 
            ParserConfigurationException, XSLException
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering getRequeuePropertyList");
        }
        
        NodeList requeueMsgsNodeList=null;
        
        try{
            InitialContext initContext = new InitialContext();
            Context myenv = (Context) initContext.lookup("java:comp/env");
        
            // Requeue received message
            String requeueReceivedMsg = (String) myenv.lookup("Requeue Received Message");
            boolean requeueReceviedMessage = (requeueReceivedMsg != null && (requeueReceivedMsg.equals("true")))?true:false;
            if (requeueReceviedMessage) 
            {
                String requeueMsgsFileName = (String) myenv.lookup("Requeue Messages File Name");
                String requeueMsgsXPath = (String) myenv.lookup("Requeue Message XPath");
                URL url2 = ResourceUtil.getInstance().getResource(requeueMsgsFileName);
                if (url2 != null)
                {
                  File requeueMsgsFile = new File(new URLDecoder().decode(url2.getFile()));
                  XMLDocument requeueMsgsDoc = (XMLDocument)DOMUtil.getDocument(requeueMsgsFile);
                  requeueMsgsNodeList = requeueMsgsDoc.selectNodes(requeueMsgsXPath);
                }
                else
                {
                  throw new IOException(requeueMsgsFileName + " file could not be found.");
                }        
            }
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getRequeuePropertyList");
            } 
        } 
          return requeueMsgsNodeList;
    }
    */

    /**
     *  returns true if the retry is greater than a configurable 
     *  parameter value (initialize to 3). 
     * @param retry - int
     * @return boolean
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws TransformerException
     * @throws XSLException
     */
    protected boolean maxRetryReached(int retry)
        throws TransformerException, IOException, SAXException,
            ParserConfigurationException
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering maxRetryReached");
            logger.debug("retry : " + retry);
        }
        
        boolean maxReached = false;
        
        try {
            /* retrieve max retry count from configuration file */
            int maxRetry = Integer.parseInt(ConfigurationUtil.getInstance().getProperty(
                ARConstants.CONFIG_FILE,ARConstants.NOVATOR_TRANSMISSION_MAX_RETRY_COUNT));
                
            /* check if the max retry has been surpassed */
            if(retry > maxRetry)
            {
                maxReached = true;
            }
            
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting maxRetryReached");
            } 
        }
        return maxReached;
    }

    /**
     *  
     * @param couponNumber - String
     * @return GiftCertVO
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     * @todo : finish
     */
    protected void requeueJmsMessage(String textMsg)
        throws IOException, SAXException, ParserConfigurationException, 
            TransformerException, NamingException, 
            SystemMessengerException, SQLException, JMSException, Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering requeueJmsMessage");
            logger.debug("Text Message : " + textMsg);
        }

        try {
            /* Increment retry in the textMsg by 1. */
            String msgIncrRetry = getMessageWithIncrementedRetry(textMsg);
            Document responseXML = DOMUtil.getDocument(msgIncrRetry);
            Node retryNode = XPathAPI.selectSingleNode(responseXML,
                "/gift_cert_request/retry/text()");
            int retry = Integer.parseInt(retryNode.getNodeValue());
            /* Check if max retry has been reached. */
            boolean maxReached = maxRetryReached(retry);
            if(maxReached == false)
            {
                 MessageToken msgToken = new MessageToken();
                 // Set status to the dispatcher pipeline attribute value. 
                 msgToken.setStatus("NOVATOR_GIFT_CERT");
                 msgToken.setMessage(msgIncrRetry);
                 // Set JMSCorrelationID 
                 Node couponNode = XPathAPI.selectSingleNode(responseXML,
                   "/gift_cert_request/coupon_number/text()");
                 String jmsCorrelationID = "Gift Cert :: " + couponNode.getNodeValue() + " ::" + retry;
                 msgToken.setJMSCorrelationID(jmsCorrelationID);
                 
                 String propertyValue = ConfigurationUtil.getInstance().getProperty
                      (ARConstants.CONFIG_FILE, ARConstants.NOVATOR_GCQ_DELAY_SECONDS);
                 String propertyType = ARConstants.Q_DELAY_PROPERTY_TYPE_LONG;
                 msgToken.setProperty(ARConstants.Q_DELAY_PROPERTY_NAME, propertyValue, propertyType);                
        
                 /* Dispatch the message */
                 Dispatcher dispatcher = Dispatcher.getInstance();
                 dispatcher.dispatchTextMessage(new InitialContext(), msgToken);
                  
            }
            else
            {
                /* send system message */
                Exception newExcep = new Exception("Maxiumum retry has been reached");
                sendSystemMessage("The maximum number of retries has been reached for the following message " 
                    + textMsg,newExcep);
            }
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting requeueJmsMessage");
            } 
        }
    }

    /**
     *  
     * @param couponNumber - String
     * @return GiftCertVO
     * @throws Exception 
     * @throws SQLException
     */
    protected String getMessageWithIncrementedRetry(String messageText)
        throws Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering getMessageWithIncrementedRetry");
            logger.debug("Message Text : " + messageText);
        }

        String newMessageText = "";
        
        try {
        
            /* take messageText and replace node
             * �/gift_cert_message/retry� by incrementing the value by 1 */
            Document messageXML = DOMUtil.getDocument(messageText);
    
            Node retryNode = XPathAPI.selectSingleNode(messageXML,
                "/gift_cert_request/retry/text()");
            int retry = Integer.parseInt(retryNode.getNodeValue()) + 1;
            retryNode.setNodeValue(String.valueOf(retry));
            
            /* convert XML doc to String */
            XMLUtil xmlUtil = new XMLUtil();
            newMessageText= xmlUtil.convertDocumentToString(messageXML);
            
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getMessageWithIncrementedRetry");
            } 
        }
 
        return newMessageText;
    }
    
    /**
     *  parse the response xml and create GiftCertResponseVO.
     * @param couponNumber - String
     * @return GiftCertVO
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     */
    protected GiftCertResponseVO parseResponse(String messageText)
        throws IOException, SAXException, ParserConfigurationException, 
            TransformerException, ParseException
    {
            
        if(logger.isDebugEnabled()){
            logger.debug("Entering parseResponse");
            logger.debug("Message Text : " + messageText);
        }
        
        GiftCertResponseVO giftCertResponse = new GiftCertResponseVO();
        
        try {            
            Document responseXML = DOMUtil.getDocument(messageText);
            
            Node statusCodeNode = XPathAPI.selectSingleNode(responseXML,
                "/gift_cert_response/status_code/text()");
            giftCertResponse.setStatusCode(
                statusCodeNode!=null?statusCodeNode.getNodeValue():"");
            
            Node couponNumberNode = XPathAPI.selectSingleNode(responseXML,
                "/gift_cert_response/gift_cert/coupon_number/text()");
            giftCertResponse.setCouponNumber(
                couponNumberNode!=null?couponNumberNode.getNodeValue():"");
            
            Node promoIDNode = XPathAPI.selectSingleNode(responseXML,
                "/gift_cert_response/gift_cert/promotion_id/text()");
            giftCertResponse.setProgramID(
                promoIDNode!=null?promoIDNode.getNodeValue():"");  
            
            Node issueAmtNode = XPathAPI.selectSingleNode(responseXML,
                "/gift_cert_response/gift_cert/issue_amount/text()");
            giftCertResponse.setIssueAmount(
                Double.parseDouble(
                issueAmtNode!=null?issueAmtNode.getNodeValue():"0"));
            
            Node orderIDNode = XPathAPI.selectSingleNode(responseXML,
                "/gift_cert_response/gift_cert/order_id/text()");
            giftCertResponse.setOrderID(
                orderIDNode!=null?orderIDNode.getNodeValue():"");
            
            Node redeemDateNode = XPathAPI.selectSingleNode(responseXML,
                "/gift_cert_response/gift_cert/redeem_date/text()");
            SimpleDateFormat sdfOutput = 
                       new SimpleDateFormat ("MM/dd/yy"); 
            
            if(redeemDateNode!=null){
                Date redeemDate = sdfOutput.parse(redeemDateNode.getNodeValue());
                giftCertResponse.setRedeemDate(redeemDate);
            }
            
            Node expireDateNode = XPathAPI.selectSingleNode(responseXML,
                "/gift_cert_response/gift_cert/expire_date/text()");
            if(expireDateNode!=null){
                Date expireDate = sdfOutput.parse(expireDateNode.getNodeValue());
                giftCertResponse.setExpireDate(expireDate);
            }
            
            Node isPersistentNode = XPathAPI.selectSingleNode(responseXML,
                "/gift_cert_response/gift_cert/is_persistent/text()");
            giftCertResponse.setIsPersistent(
                isPersistentNode!=null?isPersistentNode.getNodeValue():"");

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting parseResponse");
            } 
        }
    
        return giftCertResponse;
    }

    /**
     * sends a system message.
     * @param systemMessage - String
     * @param systemException - Exception
     * @return n/a
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     */
    protected void sendSystemMessage(String systemMessage, Exception systemException) 
        throws SAXException, ParserConfigurationException, IOException, 
            SQLException, SystemMessengerException

    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering sendSystemMessage");
        }

        try {
            /* send a system message */
            SystemMessager sysMessager = new SystemMessager();
            sysMessager.send(systemMessage,systemException,
                ARConstants.GIFT_CERT_NOVATOR_PROCESS,SystemMessager.LEVEL_DEBUG,
                ARConstants.GIFT_CERT_NOVATOR_PROCESS_ERROR_TYPE,dbConnection);
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting sendSystemMessage");
            } 
        }
    }

    /**
     *  
     * @param couponNumber - String
     * @return GiftCertVO
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     */
    public void process(String textMessage)    
        throws TransformerException, IOException, SAXException,     
        ParserConfigurationException, SAXException, SQLException, 
        ClassNotFoundException, IllegalAccessException, ParseException,
        SystemMessengerException, Exception
    {
        boolean isCalyx = false;
        
        if(logger.isDebugEnabled()){
            logger.debug("Entering process");
            logger.debug("Text Message : " + textMessage);
        }
        
        try {
            /* extract coupon number off the messageText */
            Document responseXML = DOMUtil.getDocument(textMessage);
            Node couponNumberNode = XPathAPI.selectSingleNode(responseXML, "/gift_cert_request/coupon_number/text()");
            String couponNumber = couponNumberNode!=null?couponNumberNode.getNodeValue():"";
            /* create GiftCertRequestVO messageVo and set its messageType and giftCertVo. */
            GiftCertRequestVO giftCertRequest = new GiftCertRequestVO();
            Node requestTypeNode = XPathAPI.selectSingleNode(responseXML, "/gift_cert_request/request_type/text()");
            String requestType = requestTypeNode!=null?requestTypeNode.getNodeValue():"";
            if(requestType.toLowerCase().equals("issued active"))
            {
                giftCertRequest.setMessageType("add");
            }
            else if (requestType.toLowerCase().equals("reinstate") || requestType.toLowerCase().equals("none"))
            {
                giftCertRequest.setMessageType("update");
            }
            else if (requestType.toLowerCase().equals("cancelled"))
            {
                giftCertRequest.setMessageType("delete");
            }
            else if (requestType.toLowerCase().equals("calyxbatch"))
            {
               Node batchNumNode = XPathAPI.selectSingleNode(responseXML, "/gift_cert_request/batch_num/text()");
               String batchNum = batchNumNode!=null?batchNumNode.getNodeValue():"";
               initiateCalyxBatch(batchNum);
               return;
            }
            else if (requestType.toLowerCase().equals("calyx"))
            {
               isCalyx = true;
               giftCertRequest.setMessageType("add"); 
            }
            
            
            //
            // New logic for Calyx to get new coupon data from service
            //
            GiftCertVO giftCertVO = null;
            if (isCalyx == true) {
               boolean validParams = false;
               List<String> scList = null;
               GiftCertRestrictedSourceCodeVO gRestrictScVO = new GiftCertRestrictedSourceCodeVO();

               // Get promo info that was tucked inside JMS payload
               Node promoIdNode = XPathAPI.selectSingleNode(responseXML, "/gift_cert_request/promo_id/text()");
               String promoId = promoIdNode!=null?promoIdNode.getNodeValue():"";
               Node amountNode = XPathAPI.selectSingleNode(responseXML, "/gift_cert_request/issue_amount/text()");
               String amount = amountNode!=null?amountNode.getNodeValue():"";
               Node expDateNode = XPathAPI.selectSingleNode(responseXML, "/gift_cert_request/expire_date/text()");
               String expDate = expDateNode!=null?expDateNode.getNodeValue():"";
               Node sourceCodesNode = XPathAPI.selectSingleNode(responseXML, "/gift_cert_request/sc_restriction/text()");
               String sourceCodes = sourceCodesNode!=null?sourceCodesNode.getNodeValue():"";
               if (!sourceCodes.isEmpty() && !sourceCodes.equalsIgnoreCase("null")) {
                  String[] scs = sourceCodes.split(",");
                  scList = new ArrayList();
                  for (String sc : scs) {
                     scList.add(sc);
                  }
                  if (scList.size() > 0) {
                     gRestrictScVO.setRestrictedSourceCodes(scList);                      
                  }
               }
               if (couponNumber.isEmpty() || promoId.isEmpty() || amount.isEmpty()) {
                  logger.error("Invalid request for Calyx gift certs: " + couponNumber + " " + promoId + " " + amount);
                  return;                                    
               }
               
               // Populate VO
               giftCertVO = new GiftCertVO();
               giftCertVO.setCouponNumber(couponNumber);
               try {
                  if (!expDate.isEmpty() && !expDate.equalsIgnoreCase("null")) {
                     giftCertVO.setExpireDate(new SimpleDateFormat("MM/dd/yyyy").parse(expDate));             
                  }
               } catch(java.text.ParseException pe) {
                  // Log system and continue
                  sendSystemMessage("Expiration date not in valid format", pe);          
               }
               giftCertVO.setIssueAmount(Double.parseDouble(amount));
               giftCertVO.setProgramID(promoId);
               giftCertVO.setGiftCertRestrictedSourceCodeVO(gRestrictScVO);
               giftCertRequest.setGiftCertVo(giftCertVO);

               
            // Updates to existing certs
            //
            } else {
               //GiftCertDao giftCertDAO = new GiftCertDao(dbConnection);
               //giftCertVO = giftCertDAO.loadGiftCertData(couponNumber);
               giftCertVO = getCouponInfo(couponNumber);
               giftCertRequest.setGiftCertVo(giftCertVO);
            }
            
            /* Convert GiftCertRequestVO to xml. */
            Document requestDoc = DOMUtil.getDocument(giftCertRequest.toXML());
            XMLUtil xmlUtil = new XMLUtil();
            String xmlRequest = xmlUtil.convertDocumentToString(requestDoc);
            int nullPos = xmlRequest.indexOf("null");
            if(nullPos != -1) {
                String before = xmlRequest.substring(0, nullPos);
                String after = xmlRequest.substring(nullPos + 4);
                xmlRequest = before + after;
            }
            logger.debug("**************************************************************");
            logger.debug("Sending XML to website for Calyx GC change...");
            logger.debug(xmlRequest);
            logger.debug("**************************************************************");
            
            /* create HashMap and put the XML string on a 
             * key named �gift_cert_message� */
            HashMap requestHM = new HashMap();
            //requestHM.put("gift_cert_message",xmlRequest);
            requestHM.put("xml_feed",xmlRequest);
            
            /* send XML to Novator using HttpUtil�s send procedure. */
            String novatorURL = ConfigurationUtil.getInstance().getFrpGlobalParm(
                ARConstants.CONFIG_CONTEXT,ARConstants.NOVATOR_GIFT_CERT_URL);
            HttpUtil httpUtil = new HttpUtil();
            String response = httpUtil.send(novatorURL,requestHM);
            
            /* Process response object */
            processResponse(giftCertVO, response,textMessage);
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting process");
            } 
        } 
    }

    /**
     *  �	If the response cannot be parsed, requeue JMS message.
        �	If the response can be parsed, create GiftCertResponseVO.
            o	If status_code is not SUCCESS, log system message with the 
                response string.
     * @param response - String
     * @return n/a
     * @throws Exception
     */
    protected void processResponse(GiftCertVO gcVO, String response, String requestMessageText)
        throws IOException, SAXException, ParserConfigurationException, 
            TransformerException, ParseException, SQLException, 
            SystemMessengerException, Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering processResponse");
            logger.debug("Response : " + response);
        }

        try {
            /* check if Novator response is valid */
            XMLUtil xmlUtil = new XMLUtil();
            String validationFile = ARConstants.NOVATOR_GIFT_CERT_RESPONSE_XSD;
            boolean parseSuccess = true;
            try{
                xmlUtil.validateXML(response,validationFile);
            }            catch(SAXException ex){
                logger.warn("Unexpected error while parsing " + validationFile + ": "
                + ex.getMessage());
                parseSuccess = false;
            }
            if(parseSuccess == true){
                GiftCertResponseVO giftCertVO = parseResponse(response);
                String statusCode = giftCertVO.getStatusCode();
                if(!statusCode.equals("SUCCESS"))
                {
                    // send email to ops if promotion id is not found
                    if(statusCode.equals("ERROR_PROMOTION_NOT_FOUND")) {
                        this.sendEmail("Promotion id " + gcVO.getProgramID() 
                            + " not found in Novator system.\nCoupon Number " 
                            + gcVO.getCouponNumber());
                    }
                
                    /* status_code is not SUCCESS, 
                     * log system message with the response string */
                    Exception systemExcep = 
                        new Exception("Novator Processing error " + 
                        giftCertVO.getStatusCode());
                    sendSystemMessage(response,systemExcep);
                }
            }
            else
            {
                /* requeue JMS message. */
                requeueJmsMessage(requestMessageText);
            }
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting processResponse");
            } 
        }
    }
    
    public void sendEmail(String content) throws EmailDeliveryException {
        try {
            NotificationVO notifVO = new NotificationVO();
            /* set up e-mail */
            notifVO.setAddressSeparator(ConfigurationUtil.getInstance().getProperty(
                    ARConstants.EMAIL_CONFIG_FILE,"gc_feed_address_separator"));
            notifVO.setMessageTOAddress(ConfigurationUtil.getInstance().getFrpGlobalParm(
                    ARConstants.CONFIG_CONTEXT,"gc_feed_email_addresses"));
            notifVO.setMessageFromAddress(ConfigurationUtil.getInstance().getProperty(
                    ARConstants.EMAIL_CONFIG_FILE,"gc_feed_from_address"));
            notifVO.setSMTPHost(ConfigurationUtil.getInstance().getFrpGlobalParm(
                    ARConstants.CONFIG_CONTEXT,"gc_feed_mail_server"));
            notifVO.setMessageSubject("Novator Gift Certificate Feed Notification");
            notifVO.setMessageContent(content);
            
            /* send e-mail */
            EmailUtil emaiUtil = new EmailUtil();
            emaiUtil.sendEmail(notifVO);
        } catch (Exception e) {
            throw new EmailDeliveryException(e.getMessage());
        }  
    }   

    /**
     * Initiates the Calyx Gift Cert batch by retrieving data from Calyx services
     * and re-enqueuing to this MDB for each gift cert.  This is triggered by the
     * GiftCertServlet (and was created only to free the servlet to respond immediately).
     * 
     * @param batchNumber
     */
    private void initiateCalyxBatch(String batchNumber) throws Exception {
       String errStr = null;
       boolean gotBatch = false;
       logger.info("Enqueuing all Calyx gift certs for batch: " + batchNumber);
       try {
          if (batchNumber == null || batchNumber.isEmpty()) {
             errStr = "No Calyx Gift Cert Batch number specified";
          } else {
             String jmsInnerStr = getBatchInfo(batchNumber);
             gotBatch = true;
             if (jmsInnerStr == null || jmsInnerStr.isEmpty()) {
                errStr = "No Calyx Batch info found";
             } else {
                // Enqueue entry for each gift cert
                errStr = getAndEnqueueGiftCodes(batchNumber, jmsInnerStr);                
             }
          }
       } catch(Exception e) {
          if (gotBatch) {
             errStr = "Exception when attempting to get/enqueue gift codes for: " + batchNumber + " - " + e.toString();             
          } else {
             errStr = "Exception when attemptpig to get batch info for: " + batchNumber + " - " + e.toString();             
          }
       }
       if (errStr != null) {
          logger.error(errStr);
          /* send system message */
          Exception newExcep = new Exception("initiateCalyxBatch failed");
          sendSystemMessage(errStr, newExcep);          
       }
    }
    
    
    private String getAndEnqueueGiftCodes(String batchNum, String jmsInnerStr) throws Exception {
       String errStr = null;
       GiftCodeUtil gcu = new GiftCodeUtil();
       List<String[]> gcodeList = gcu.getGiftCodesOnly(batchNum);
       if (gcodeList != null && gcodeList.size() > 0) {
          logger.info("For GiftCode batch: " + batchNum + " enqueing " + gcodeList.size() + " gift certificates");
          for (String[] gcode : gcodeList) {
             String payLoad = String.format(JMS_PAYLOAD_OUTER, gcode[0], jmsInnerStr);
             //public static void insertJMSMessage(Connection connection, String payLoad, String queueName, String correlationId, long delaySec) throws Exception
             logger.info("Payload for Calyx gift cert feed: " + payLoad);
             FTDCommonUtils.insertJMSMessage(dbConnection, payLoad, "OJMS.NOVATOR_GIFTCERT", "", 3);   
          }         
       } else {
          errStr = "For GiftCode batch: " + batchNum + " no gift certificates to enqueue";
       }
       return errStr;
    }
       
    /**
     * Get Gift Certificate batch info and populate JMS payload template
     * with batch data.  This will be same for all gift certs in the batch.
     * 
     * @param batchNum
     * @return JMS payload
     */
    private String getBatchInfo(String batchNum) throws Exception {
          
       // Retrieve data from service
       //
       GiftCodeUtil gcu = new GiftCodeUtil();
       String xmlResp = gcu.searchGiftCertificate(batchNum, null, null);
       Document rd = GiftCodeUtil.getXMLDocument(xmlResp);
       String promotionId = GiftCodeUtil.getStringValueFromXML(rd, "gcc_promotion_id");
       String issueAmount = GiftCodeUtil.getStringValueFromXML(rd, "issue_amount");
       String expireDate = GiftCodeUtil.getStringValueFromXML(rd, "expiration_date");  // This is optional param
       logger.debug("Calyx gift cert batch: '" + batchNum + ",' " + promotionId + "', '" + issueAmount + "', '" + expireDate + "'");
       // Get any source code restrictions
       String scStr = getSourceCodesStr(rd, null);
       
       // Populate JMS payload
       //
       return String.format(JMS_PAYLOAD_INNER, promotionId, issueAmount, expireDate, (scStr != null ? scStr : ""));
    }
    
    
   /**
    * Get Gift Certificate info and populate VO.  This is used for coupon status updates only.
    * 
    * @param batchNum
    * @return
    * @throws Exception
    */
    private GiftCertVO getCouponInfo(String coupon) throws Exception {
       GiftCertRestrictedSourceCodeVO gRestrictScVO = new GiftCertRestrictedSourceCodeVO();
          
       // Retrieve data from service
       //
       GiftCodeUtil gcu = new GiftCodeUtil();
       String xmlResp = gcu.searchGiftCertificate(null, coupon, null);
       Document rd = GiftCodeUtil.getXMLDocument(xmlResp);
       String promotionId = GiftCodeUtil.getStringValueFromXML(rd, "gcc_promotion_id");
       String issueAmount = GiftCodeUtil.getStringValueFromXML(rd, "issue_amount");
       String expDate = GiftCodeUtil.getStringValueFromXML(rd, "expiration_date");  // This is optional param
       logger.debug("Calyx gift cert coupon status update for: '" + coupon + ",' " + promotionId + "', '" + issueAmount + "', '" + expDate + "'");
       // Get any source code restrictions
       List<String> srcCodesList = new ArrayList<String>();
       getSourceCodesStr(rd, srcCodesList);
       if (srcCodesList.size() > 0) {
          gRestrictScVO.setRestrictedSourceCodes(srcCodesList);                      
       }
            
       // Populate VO
       //
       GiftCertVO giftCertVO = new GiftCertVO();
       giftCertVO.setCouponNumber(coupon);
       try {
          if (expDate != null && !expDate.isEmpty() && !expDate.equalsIgnoreCase("null")) {
             giftCertVO.setExpireDate(new SimpleDateFormat("MM/dd/yyyy").parse(expDate));             
          }
       } catch(java.text.ParseException pe) {
          // Log system and continue
          sendSystemMessage("Expiration date not in valid format", pe);          
       }
       giftCertVO.setIssueAmount(Double.parseDouble(issueAmount));
       giftCertVO.setProgramID(promotionId);
       giftCertVO.setGiftCertRestrictedSourceCodeVO(gRestrictScVO);
       
       return giftCertVO;
    }

    
    private String getSourceCodesStr(Document parentElement, List<String> srcCodesList) {
       String srcCodesStr = null;
       NodeList nodeList = parentElement.getElementsByTagName("gcc_source_code");
       if (null != nodeList && nodeList.getLength() > 0) {
          Node sourceNode = nodeList.item(0);
          NodeList sourceNodeList = sourceNode.getChildNodes();
          srcCodesStr = "";
          for (int count = 0; count < sourceNodeList.getLength(); count++) {
             String value = new String();
             Node sourceInnerNode = sourceNodeList.item(count);
             if (sourceInnerNode.getChildNodes().getLength() > 0) {
                value = sourceInnerNode.getChildNodes().item(0).getNodeValue().trim();
                if (srcCodesStr.length() > 0) {
                   srcCodesStr += ",";
                }
                srcCodesStr += value.trim();
                if (srcCodesList != null) {
                   // Optional list set for caller if wanted
                   srcCodesList.add(value.trim());  
                }
             }
          }
       }
       return srcCodesStr;
    }
    
}