package com.ftd.accountingreporting.novator.vo;

import java.lang.reflect.Field;
import java.util.List;

import com.ftd.accountingreporting.common.XMLInterface;
import com.ftd.osp.utilities.xml.DOMUtil;

public class GiftCertRestrictedSourceCodeVO implements XMLInterface
{
    private List<String> source_code;  
     

    public GiftCertRestrictedSourceCodeVO()
    {
    }

    
    public List<String> getRestrictedSourceCodes() {
		return source_code;
	}


	public void setRestrictedSourceCodes(List<String> sourceCodes) {
		this.source_code = sourceCodes;
	}


	public String toXML() throws IllegalAccessException, ClassNotFoundException
	  {
	    StringBuffer sb = new StringBuffer();
	    try
	    {
	      
	      sb.append("<sc_restriction>"); 
	      
	      Field[] fields = this.getClass().getDeclaredFields();

	      appendFields(sb, fields);
	      sb.append("</sc_restriction>");
	    }

	    catch (Exception e)
	    {
	      e.printStackTrace();
	    }

	    return sb.toString();
	  }

	  
	  protected void appendFields(StringBuffer sb, Field[] fields) throws IllegalAccessException, ClassNotFoundException
	  {
	  	for (int i = 0; i < fields.length; i++)
	  	{
	  	  //if the field retrieved was a list of VO
	  	  if(fields[i].getType().equals(Class.forName("java.util.List")))
	  	  {
	  	    List list = (List)fields[i].get(this);
	  	    if(list != null)
	  	    {
	  	      for (int j = 0; j < list.size(); j++)
	  	      {
	  	    	sb.append("<" + fields[i].getName() + ">");
		  	      String value = "";

		          if(list.get(j) !=null){
		              value = list.get(j).toString();    
		              value = DOMUtil.encodeChars(value);
		          }
		          else
		          {
		              value = "";    
		          }
		  	          
		  	      sb.append(value);
		  	      sb.append("</" + fields[i].getName() + ">");
	  	      }
	  	    }
	  	  }
	  	  
	  	}
	  }

}