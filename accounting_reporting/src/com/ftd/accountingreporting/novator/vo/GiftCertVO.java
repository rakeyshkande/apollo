package com.ftd.accountingreporting.novator.vo;

import com.ftd.accountingreporting.common.XMLInterface;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.IOException;

import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;

import org.xml.sax.SAXException;

/**
 * This is a value object class which models the Gift Cert 
 * section of the XML message to be sent to Novator.
 * @author Charles Fox
 */
public class GiftCertVO implements XMLInterface
{
    public GiftCertVO()
    {
    }

    private String coupon_number = "";
    private String promotion_id = "";
    private double issue_amount;
    private Date expire_date;
    private GiftCertRestrictedSourceCodeVO giftCertRestrictedSourceCodeVO;

    /**
     * gets promotion_id
     * @return String promotion_id
     */
     public String getProgramID()
     {
        return promotion_id;
     }
     
    /**
     * sets promotion_id
     * @param newProgramID  String
     */
     public void setProgramID(String newProgramID)
     {
        promotion_id = trim(newProgramID);
     }

    /**
     * gets coupon_number
     * @return String coupon_number
     */
     public String getCouponNumber()
     {
        return coupon_number;
     }
     
    /**
     * sets coupon_number
     * @param newCouponNumber  String
     */
     public void setCouponNumber(String newCouponNumber)
     {
        coupon_number = trim(newCouponNumber);
     }

    /**
     * gets issue_amount
     * @return String issue_amount
     */
     public double getIssueAmount()
     {
        return issue_amount;
     }
     
    /**
     * sets issue_amount
     * @param newIssueAmount  double
     */
     public void setIssueAmount(double newIssueAmount)
     {
        issue_amount = newIssueAmount;
     }
    
    /**
     * gets expire_date
     * @return Date expire_date
     */
     public Date getExpireDate()
     {
        return expire_date;
     }
     
    /**
     * sets expire_date
     * @param newExpireDate  Date
     */
     public void setExpireDate(Date newExpireDate)
     {
        expire_date = newExpireDate;
     }
     
     
    public GiftCertRestrictedSourceCodeVO getGiftCertRestrictedSourceCodeVO() {
		return giftCertRestrictedSourceCodeVO;
	}

	public void setGiftCertRestrictedSourceCodeVO(
			GiftCertRestrictedSourceCodeVO giftCertRestrictedSourceCodeVO) {
		this.giftCertRestrictedSourceCodeVO = giftCertRestrictedSourceCodeVO;
	}
    
	
	private String trim(String str)
    {
        return (str != null)?str.trim():str;
    }  
     
  /**
    * This method uses the Reflection API to generate an XML string that will be
    * passed back to the calling module.
    * The XML string will contain all the fields within this VO, including the
    * variables as well as (a collection of) ValueObjects.
    *
    * @param  None
    * @return XML string
   **/
    public String toXML() throws IllegalAccessException, ClassNotFoundException
    {

        StringBuffer sb = new StringBuffer();
        try
        {
            sb.append("<gift_cert>");
            Field[] fields = this.getClass().getDeclaredFields();

            appendFields(sb, fields);
            sb.append("</gift_cert>");

        } finally {

        }

        return sb.toString();
    }

    protected void appendFields(StringBuffer sb, Field[] fields) 
        throws IllegalAccessException, ClassNotFoundException
    {

        try
        {
            for (int i = 0; i < fields.length; i++)
            {
              //if the field retrieved was a list of VO
              if(fields[i].getType().equals(Class.forName("java.util.List")))
              {
                List list = (List)fields[i].get(this);
                if(list != null)
                {
                  for (int j = 0; j < list.size(); j++)
                  {
                    XMLInterface xmlInt = (XMLInterface)list.get(j);
                    String sXmlVO = xmlInt.toXML();
                    sb.append(sXmlVO);
                  }
                }
              }
              else
              {
                //if the field retrieved was a VO
                if (fields[i].getType().toString().matches("(?i).*vo"))
                {
                  XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
                  String sXmlVO = xmlInt.toXML();
                  sb.append(sXmlVO);
                }
                //if the field retrieved was a Calendar object
                //else if (fields[i].getType().toString().matches("(?i).*calendar")
                else if (fields[i].getType().toString().matches("(?i).*date"))
                {
                  Date date;
                  String fDate = null;
                  if (fields[i].get(this) != null)
                  {
                    //date = (((GregorianCalendar)fields[i].get(this)).getTime());
                    date = (Date)fields[i].get(this);
                    //SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
                    SimpleDateFormat sdf = new SimpleDateFormat ("MM/dd/yyyy");
                    fDate = sdf.format(date).toString();
                  }
                  sb.append("<" + fields[i].getName() + ">");
                  sb.append(fDate);
                  sb.append("</" + fields[i].getName() + ">");
                }
                else
                {
                  
                  sb.append("<" + fields[i].getName() + ">");
                  String value = "";
        
                  if(fields[i].get(this)!=null){
                      value = fields[i].get(this).toString();    
                      value = DOMUtil.encodeChars(value);
                  }
                  else
                  {
                      value = "";    
                  }
                      
        
                  sb.append(value);
                  sb.append("</" + fields[i].getName() + ">");
                }
              }
            }
        } finally {

        }
    }
}