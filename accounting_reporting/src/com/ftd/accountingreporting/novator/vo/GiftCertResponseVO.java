package com.ftd.accountingreporting.novator.vo;
import com.ftd.accountingreporting.common.XMLInterface;
import java.util.Date;

/**
 * This is value object class which models the Gift response XML 
 * message from Novator. 
 * @author Charles Fox
 */
public class GiftCertResponseVO
{
    public GiftCertResponseVO()
    {
    }
    
    private String status_code = ""; //Status code of the response.
    private String response = ""; //This is the response XML.
    private String program_id = "";
    private String coupon_number = "";
    private double issue_amount;
    private String order_id = "";
    private Date redeem_date;
    private Date expire_date;
    private String is_persistent = "";


    /**
     * gets status_code
     * @return String status_code
     */
     public String getStatusCode()
     {
        return status_code;
     }
     
    /**
     * sets status_code
     * @param newStatusCode  String
     */
     public void setStatusCode(String newStatusCode)
     {
        status_code = trim(newStatusCode);
     }

    /**
     * gets response
     * @return String response
     */
     public String getResponse()
     {
        return response;
     }
     
    /**
     * sets response
     * @param newResponse  String
     */
     public void setResponse(String newResponse)
     {
        response = trim(newResponse);
     }

    /**
     * gets program_id
     * @return String program_id
     */
     public String getProgramID()
     {
        return program_id;
     }
     
    /**
     * sets program_id
     * @param newProgramID  String
     */
     public void setProgramID(String newProgramID)
     {
        program_id = trim(newProgramID);
     }

    /**
     * gets coupon_number
     * @return String coupon_number
     */
     public String getCouponNumber()
     {
        return coupon_number;
     }
     
    /**
     * sets coupon_number
     * @param newCouponNumber  String
     */
     public void setCouponNumber(String newCouponNumber)
     {
        coupon_number = trim(newCouponNumber);
     }   

    /**
     * gets issue_amount
     * @return double issue_amount
     */
     public double getIssueAmount()
     {
        return issue_amount;
     }
     
    /**
     * sets issue_amount
     * @param newIssueAmount  double
     */
     public void setIssueAmount(double newIssueAmount)
     {
        issue_amount = newIssueAmount;
     } 
     
    /**
     * gets order_id
     * @return String order_id
     */
     public String getOrderID()
     {
        return order_id;
     }
     
    /**
     * sets order_id
     * @param newOrderID  String
     */
     public void setOrderID(String newOrderID)
     {
        order_id = trim(newOrderID);
     }

    /**
     * gets redeem_date
     * @return Date redeem_date
     */
     public Date getRedeemDate()
     {
        return redeem_date;
     }
     
    /**
     * sets redeem_date
     * @param newRedeemDate  Date
     */
     public void setRedeemDate(Date newRedeemDate)
     {
        redeem_date = newRedeemDate;
     }

    /**
     * gets expire_date
     * @return Date expire_date
     */
     public Date getExpireDate()
     {
        return expire_date;
     }
     
    /**
     * sets expire_date
     * @param newExpireDate  Date
     */
     public void setExpireDate(Date newExpireDate)
     {
        expire_date = newExpireDate;
     }

    /**
     * gets is_persistent
     * @return String is_persistent
     */
     public String getIsPersistent()
     {
        return is_persistent;
     }
     
    /**
     * sets is_persistent
     * @param newIsPersistent  String
     */
     public void setIsPersistent(String newIsPersistent)
     {
        is_persistent = trim(newIsPersistent);
     }
    private String trim(String str)
    {
        return (str != null)?str.trim():str;
    } 
}