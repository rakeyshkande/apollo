package com.ftd.accountingreporting.novator.vo;

import com.ftd.accountingreporting.common.XMLInterface;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.IOException;

import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;

import org.xml.sax.SAXException;

/**
 * This is a value object class which models the XML message
 * to be sent to Novator
 * @author Charles Fox
 */
public class GiftCertRequestVO implements XMLInterface
{
    public GiftCertRequestVO()
    {
    }
    
        
    private String message_type = "";
    private GiftCertVO giftCertVo;
    
    /**
     * gets message_type
     * @return String message_type
     */
     public String getMessageType()
     {
        return message_type;
     }
     
    /**
     * sets message_type
     * @param newMessageType  String
     */
     public void setMessageType(String newMessageType)
     {
        message_type = trim(newMessageType);
     }

    /**
     * gets giftCertVo
     * @return GiftCertVO giftCertVo
     */
     public GiftCertVO getGiftCertVo()
     {
        return giftCertVo;
     }
     
    /**
     * sets giftCertVo
     * @param newGiftCertVo  GiftCertVO
     */
     public void setGiftCertVo(GiftCertVO newGiftCertVo)
     {
        giftCertVo = newGiftCertVo;
     }
  /**
    * This method uses the Reflection API to generate an XML string that will be
    * passed back to the calling module.
    * The XML string will contain all the fields within this VO, including the
    * variables as well as (a collection of) ValueObjects.
    *
    * @param  None
    * @return XML string
   **/
    public String toXML() throws IllegalAccessException, ClassNotFoundException
    {

        StringBuffer sb = new StringBuffer();
        try
        {
            sb.append("<?xml version='1.0' encoding='UTF-8'?>");
            sb.append("<gift_cert_message>");
            Field[] fields = this.getClass().getDeclaredFields();

            appendFields(sb, fields);
            sb.append("</gift_cert_message>");

        } finally {
 
        }

        return sb.toString();
    }

    protected void appendFields(StringBuffer sb, Field[] fields) 
        throws IllegalAccessException, ClassNotFoundException
    {

        try
        {
            for (int i = 0; i < fields.length; i++)
            {
              //if the field retrieved was a list of VO
              if(fields[i].getType().equals(Class.forName("java.util.List")))
              {
                List list = (List)fields[i].get(this);
                if(list != null)
                {
                  for (int j = 0; j < list.size(); j++)
                  {
                    XMLInterface xmlInt = (XMLInterface)list.get(j);
                    String sXmlVO = xmlInt.toXML();
                    sb.append(sXmlVO);
                  }
                }
              }
              else
              {
                //if the field retrieved was a VO
                if (fields[i].getType().toString().matches("(?i).*vo"))
                {
                  XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
                  String sXmlVO = xmlInt.toXML();
                  sb.append(sXmlVO);
                }
                //if the field retrieved was a Calendar object
                else if (fields[i].getType().toString().matches("(?i).*calendar"))
                {
                  Date date;
                  String fDate = null;
                  if (fields[i].get(this) != null)
                  {
                    date = (((GregorianCalendar)fields[i].get(this)).getTime());
                    SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
                    fDate = sdf.format(date).toString();
                    sb.append("<" + fields[i].getName() + ">");
                    sb.append(fDate);
                    sb.append("</" + fields[i].getName() + ">");
                  } else {
                    sb.append("<" + fields[i].getName() + "/>");
                  }
                }
                else
                {
                  
                  sb.append("<" + fields[i].getName() + ">");
                  String value = "";
        
                  if(fields[i].get(this)!=null){
                      value = fields[i].get(this).toString();    
                      value = DOMUtil.encodeChars(value);
                  }
                  else
                  {
                      value = "";    
                  }
                      
        
                  sb.append(value);
                  sb.append("</" + fields[i].getName() + ">");
                }
              }
            }
        } finally {

        }
    }
    
    private String trim(String str)
    {
        return (str != null)?str.trim():str;
    }  
}