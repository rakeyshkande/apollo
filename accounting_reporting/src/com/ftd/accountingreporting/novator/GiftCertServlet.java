package com.ftd.accountingreporting.novator;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.naming.InitialContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import com.ftd.osp.utilities.HttpServletResponseUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.novator.vo.GiftCertRestrictedSourceCodeVO;
import com.ftd.accountingreporting.novator.vo.GiftCertVO;
import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.GiftCodeUtil;

import org.apache.commons.lang.StringUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class GiftCertServlet extends HttpServlet {
   private Logger logger;
   private static final String BATCH_CREATE_PARAM = "batchCreate";
   private static final String BATCH_UPDATE_PARAM = "batchUpdate";
   private static final String BATCH_TYPE_PARAM   = "type";
   private static final int    SUCCESS_CODE       = 200;
   private static final int    CLIENT_ERROR_CODE  = 400;
   private static final int    SERVER_ERROR_CODE  = 500;
   private static final String JMS_PAYLOAD = 
         "<?xml version=\"1.0\" encoding=\"UTF-8\"?><gift_cert_request><request_type>calyxBatch</request_type><retry>0</retry>" +
         "<batch_num>%s</batch_num></gift_cert_request>";
   
      
   public void init(ServletConfig config) throws ServletException
   {
     logger = new Logger("com.ftd.accountingreporting.novator.GiftCertServlet");
     super.init(config);
   }

   public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
   {
      doPost(request, response);
   }

   public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
   {
      Connection conn = null;
      try {
         String batchCreate = request.getParameter(BATCH_CREATE_PARAM);
         String batchUpdate = request.getParameter(BATCH_UPDATE_PARAM);
         String dsname = ConfigurationUtil.getInstance().getProperty(ARConstants.CONFIG_FILE, ARConstants.DATASOURCE_NAME);
         conn = DataSourceUtil.getInstance().getConnection(dsname);
         
         if (batchCreate != null && !batchCreate.isEmpty()) {
            String payLoad = String.format(JMS_PAYLOAD, batchCreate);         
            logger.info("Enqueing Calyx batch gift cert create feed (all gift certs for batch will then be enqueued): " + payLoad);
            FTDCommonUtils.insertJMSMessage(conn, payLoad, "OJMS.NOVATOR_GIFTCERT", "", 3);   
            returnHttpResponse(response, SUCCESS_CODE, "");
         } else if (batchUpdate != null && !batchUpdate.isEmpty()) {
            String updateType = request.getParameter(BATCH_TYPE_PARAM);
            if (updateType == null || updateType.isEmpty()) {
               returnHttpResponse(response, CLIENT_ERROR_CODE, "Update request requires type parameter");               
            } else {
               logger.info("Enqueing Calyx batch gift cert update feed (all gift certs for batch will then be enqueued): " + batchUpdate + " " + updateType);            
               MessageToken messageToken = new MessageToken();
               messageToken.setStatus("GIFT_CERT_MAINT");
               messageToken.setJMSCorrelationID(updateType);
               messageToken.setMessage(batchUpdate);
               Dispatcher dispatcher = Dispatcher.getInstance();
               dispatcher.dispatchTextMessage(new InitialContext(), messageToken);
               returnHttpResponse(response, SUCCESS_CODE, "");
            }
         } else {
            returnHttpResponse(response, CLIENT_ERROR_CODE, "Empty request from client");
         }

      } catch(Exception e) {
         returnHttpResponse(response, SERVER_ERROR_CODE, e.toString());         
      } finally {
         try {
            if(conn != null && !conn.isClosed()) {
                conn.close();
            }           
         } catch (Exception e) {
            logger.error("Exception caught when closing DB connection!", e);
         }         
      }
   }

   
   private void returnHttpResponse(HttpServletResponse response, int statusCode, String statusReason)
   {
      try {
        HttpServletResponseUtil.sendError(response, statusCode, statusReason);
      } catch(Exception ex) {
         logger.error("GiftCertServlet failed when returning response: " + ex.toString());
      }
   }
     
}
