package com.ftd.accountingreporting.novator;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJBException;
import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.dao.GiftCertificateCouponDAO;

import com.ftd.accountingreporting.novator.handler.GiftCertMessageHandler;
import com.ftd.accountingreporting.util.SystemMessager;
import com.ftd.accountingreporting.vo.GiftCertificateCouponVO;
import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.novator.giftcert.util.MessageUtil;
import com.ftd.novator.giftcert.vo.GiftCertMessageVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.GiftCodeUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.xml.DOMUtil;

public class GiftCertificateMaintenanceMDB implements  MessageDrivenBean, MessageListener  {

	private Logger logger = new Logger("com.ftd.accountingreporting.novator.GiftCertificateMaintenanceMDB");
	private MessageDrivenContext context;	 	
	
	public void ejbCreate()
	    {
	    }
	 
	private void processMessage(Message msg) throws Exception
	    {
	        if(logger.isDebugEnabled()){
	            logger.debug("Entering processMessage");
	            logger.debug("Message : " + msg.toString());
	        }	        
	        Connection conn=null;
	        //GiftCertificateCouponDAO gccCouponDAO;
	        ArrayList<GiftCertificateCouponVO> couponsList = new ArrayList<GiftCertificateCouponVO>();
	        boolean success=false;
	        conn = createDatabaseConnection();
            //gccCouponDAO = new GiftCertificateCouponDAO(conn);
            boolean isSingleGc = false;
            String successMessage = null;
            String gcNumber = null;
            long dispatchCount = 0;
            GiftCertMessageHandler giftCertMessageHandler = new GiftCertMessageHandler(conn);
	            try
	            {
	                /* Extract text message from msg */
	                MessageToken token = new MessageToken();
	                TextMessage textMessage = (TextMessage) msg;
	                //For normal usage, message is request number.
	                //For single GC it is taken as coupon number
	                gcNumber = textMessage.getText();
	                
	                String correlationId = textMessage.getJMSCorrelationID();
                   GiftCodeUtil gcu = new GiftCodeUtil();
                   logger.info("Calyx GiftCertMaint on " + gcNumber + " " + correlationId);
	                if(correlationId.equals("MULTIPLE-GC-PROMOTION_ID_CHANGED")){	                	
	                  List<String[]> gcodeList = gcu.getGiftCodesOnly(gcNumber);
	                	//couponsList = getCouponDataFromXML(gccCouponDAO.doGetCouponByReqNumOrGccNum(gcNumber, null));
	                  if (gcodeList != null && gcodeList.size() > 0) {
	                     logger.info("Calyx Cert count: " + gcodeList.size());
	                     for (String[] coupon : gcodeList) {
	                        String coupNum  = coupon[0];
                           String coupStat = coupon[1];
                           if("Issued Active".equals(coupStat) || "Issued Expired".equals(coupStat)) {
                              dispatchNovatorMessage("Issued Active", coupNum);
                              dispatchCount++;
                            }
	                     }
	                  }
	                } else if(textMessage.getJMSCorrelationID().equals("MULTIPLE-GC-CANCEL-OR-REINSTATE")) {
	                     List<String[]> gcodeList = gcu.getGiftCodesOnly(gcNumber);
	                     //couponsList = getCouponDataFromXML(gccCouponDAO.doGetCouponByReqNumOrGccNum(gcNumber, null));
	                     if (gcodeList != null && gcodeList.size() > 0) {
	                        logger.info("Calyx Cert count: " + gcodeList.size());
	                        for (String[] coupon : gcodeList) {
	                           String coupNum  = coupon[0];
	                           String coupStat = coupon[1];
	                           dispatchNovatorMessage(coupStat, coupNum);
                              dispatchCount++;
	                        }
	                     }
	                } else if(textMessage.getJMSCorrelationID().equals("MULTIPLE-GC-SOURCE-CODE-MODIFIED")) {
	                	//The type is set to none because when source code is modified we get none value from UI 
	                	//as no drop down value would be selected. In gift certificates module, for "none",
	                	//status would be changed to "update"
                      List<String[]> gcodeList = gcu.getGiftCodesOnly(gcNumber);
                      //couponsList = getCouponDataFromXML(gccCouponDAO.doGetCouponByReqNumOrGccNum(gcNumber, null));
                      if (gcodeList != null && gcodeList.size() > 0) {
                         logger.info("Calyx Cert count: " + gcodeList.size());
                         for (String[] coupon : gcodeList) {
                            String coupNum  = coupon[0];
                            dispatchNovatorMessage("none", coupNum);
                            dispatchCount++;
                         }
                      }
	                } else if(textMessage.getJMSCorrelationID().equals("SINGLE-GC-SOURCE-CODE-MODIFIED")) {
	                	isSingleGc = true;
	                	dispatchCount = processSingleGC(gcNumber);
	                } else if(textMessage.getJMSCorrelationID().equals("GC-CREATE")){
                      List<String[]> gcodeList = gcu.getGiftCodesOnly(gcNumber);
                      //couponsList = getCouponDataFromXML(gccCouponDAO.doGetCouponByReqNumOrGccNum(gcNumber, null));
                      if (gcodeList != null && gcodeList.size() > 0) {
                         logger.info("Calyx Cert count: " + gcodeList.size());
                         for (String[] coupon : gcodeList) {
                            String coupNum  = coupon[0];
                            String coupStat = coupon[1];
                            dispatchNovatorMessage(coupStat, coupNum);
                            dispatchCount++;
                         }
                      }
	                }
                 logger.info("Dispatched count for Calyx Certs: " + dispatchCount);
	              if(isSingleGc) {
	            	  successMessage = "Gift Coupon Number is - ";
	              } else {
	            	  successMessage = "Gift Coupon Request Number is -";
	              }
	              giftCertMessageHandler.sendEmail("Gift Certificate request successfully processed." + successMessage + gcNumber);
	              success=true;
	            } catch(Exception e){
	                logger.error(e);
	                giftCertMessageHandler.sendEmail("Error occured while processing request." + successMessage + gcNumber);
	                SystemMessager.send("Error while sending JMS messages for gift certificates", e,
							ARConstants.GIFT_CERT_MAINTENANCE_PROCESS, SystemMessager.LEVEL_DEBUG, 
							ARConstants.GIFT_CERT_MAINTENANCE_PROCESS_ERROR_TYPE, conn);
	        }finally
	        {
	            if(!success)
	            {
	              logger.error("can not process the queue. roll back transaction");
	              
	              this.context.setRollbackOnly();//rollback transaction.
	            }
	            if(conn != null && !conn.isClosed()) {
	                  logger.debug("Closing connection...");
	                  conn.close();
	            }
	        
	            if(logger.isDebugEnabled()){
	            logger.debug("Exiting processMessage");
	            }
	        }        
	    }
	
	
	private long processSingleGC(String couponNumber) throws Exception {
	   long dispatchCount = 0;
      GiftCodeUtil gcu = new GiftCodeUtil();
      String xmlResp = gcu.searchGiftCertificate(null, couponNumber, null);
      Document rd = GiftCodeUtil.getXMLDocument(xmlResp);
      String batchNum = GiftCodeUtil.getStringValueFromXML(rd, "request_number");
      logger.info("Calyx GiftCertMaint - Checking all other gift certs on same batch (" + batchNum + ") as gift cert: " + couponNumber);
      List<String[]> gcodeList = gcu.getGiftCodesOnly(batchNum);
      if (gcodeList != null && gcodeList.size() > 0) {
         logger.info("Calyx Cert count: " + gcodeList.size());
         for (String[] coupon : gcodeList) {
            String coupNum  = coupon[0];
            String coupStat = coupon[1];
            if(!coupNum.equals(couponNumber) && coupStat.equals("Cancelled") || 
                  coupStat.equals("Issued Active") || coupStat.equals("Reinstate")) {
               dispatchNovatorMessage("none", coupNum);
               dispatchCount++;
            }
         }
      } 
      return dispatchCount;
	}
	
	private void dispatchNovatorMessage(String requestType,String gcCouponNumber ) throws Exception {
		
		MessageUtil msgUtil = new MessageUtil();
        GiftCertMessageVO giftCertMsg = new GiftCertMessageVO();
        giftCertMsg.setCouponNumber(gcCouponNumber);
        // For empty request type, it implies that the value from data base would be sent, as it is already saved.
        //For others, the request type is based on the legacy code conditions
        giftCertMsg.setRequestType(requestType);
                
        giftCertMsg.setRetry(0);
        String xmlMessage = giftCertMsg.toXML(); 
        logger.debug("*****Novator Message*****" + xmlMessage);
        msgUtil.dispatchMessage(xmlMessage);
	}
	
	@Override
	public void onMessage(Message msg) {

		if(logger.isDebugEnabled()){
	      logger.debug("Entering processMessage");
	      logger.debug("Message : " + msg.toString());
	    }
	    try {
	        processMessage(msg);
	    }catch(Exception e){
	        logger.error(e);
        }finally {
	        if(logger.isDebugEnabled()){
	           logger.debug("Exiting onMessageof Gift Certificate Request Receiver" + " Message Driven Bean");
	        }
	        if (context.getRollbackOnly())
	            	throw new RuntimeException("Rollback detected, exception thrown to force rollback.");
	        }

	}

	@Override
	public void ejbRemove() throws EJBException {		
	}

	public void setMessageDrivenContext(MessageDrivenContext ctx)
	{
	  this.context = ctx;
	}
	 	

	private Connection createDatabaseConnection() throws IOException, SAXException, ParserConfigurationException, 
        Exception
	{
	    if(logger.isDebugEnabled()){
	        logger.debug("Entering createDatabaseConnection");
	    }
	
	    Connection connection=null;
	    try{
	        /* create a connection to the database */
	        connection = DataSourceUtil.getInstance().
	            getConnection(ConfigurationUtil.getInstance().getProperty(
	            ARConstants.CONFIG_FILE,ARConstants.DATASOURCE_NAME));
	            
	    }finally{
	        if(logger.isDebugEnabled()){
	           logger.debug("Exiting createDatabaseConnection");
	        }
	    }
	    
	    return connection;
	}
	
	 private void logError(String message,Throwable excep, Connection con) throws IOException,
     SAXException,ParserConfigurationException,Exception
     {
	     if(logger.isDebugEnabled()){
	         logger.debug("Entering logError");
	     }
	 
	     
	      try{
	         
	         // log message to log4j log file 
	         logger.error(message,excep);
	         
	         // send a system message 
	         SystemMessager.send(message,excep, ARConstants.GIFT_CERT_MAINTENANCE_PROCESS,
	        		 SystemMessengerVO.LEVEL_PRODUCTION, ARConstants.GIFT_CERT_MAINTENANCE_PROCESS_ERROR_TYPE,con);
	     
	     } finally {
	         if(logger.isDebugEnabled()){
	            logger.debug("Exiting logError");
	         } 
	     }	
	     
     }
	 
	 private void closeConnection(Connection conn) throws SQLException, Exception
	    {
	        if(logger.isDebugEnabled()){
	            logger.debug("Entering closeConnection");
	        }

	        try
	        {
	          if(conn != null && !conn.isClosed()) {
	              logger.debug("Closing connection...");
	              conn.close();
	          }

	        }finally
	        {
	            if(logger.isDebugEnabled()){
	               logger.debug("Exiting closeConnection");
	            }
	        }    
	    }
	 
	 private ArrayList getCouponDataFromXML(Document couponDoc) throws Exception
	   {
	       ArrayList listCouponVo = new ArrayList();	       
	       NodeList listOfCoupons = couponDoc.getElementsByTagName("GC_COUPON");
	       for(int s=0; s < listOfCoupons.getLength(); s++)
	       {    
	            GiftCertificateCouponVO vo = new GiftCertificateCouponVO();
	            Node firstCouponNode = listOfCoupons.item(s);
	            if(firstCouponNode.getNodeType() == Node.ELEMENT_NODE)
	            {
	                Element firstRecipientElement = (Element)firstCouponNode;
	                
	                vo.setGcCouponNumber(getStringValueFromXML(firstRecipientElement,"gc_coupon_number"));
	                String recipId = getStringValueFromXML(firstRecipientElement,"gc_coupon_recip_id");
	                if (recipId!=null)
	                    vo.setGcCouponRecipId(new Long(recipId).longValue());
	                vo.setGcCouponStatus(getStringValueFromXML(firstRecipientElement,"gc_coupon_status"));
	                vo.setOrderSource(getStringValueFromXML(firstRecipientElement,"gc_coupon_number"));
	            }
	            listCouponVo.add(vo);
	       }	       
	       return listCouponVo;
	   }
	 
	 private String getStringValueFromXML(Element parentElement, String tagName) throws Exception
	   {
	       
	       NodeList nodeList = parentElement.getElementsByTagName(tagName);
	       if (nodeList.getLength() == 0) 
	          return null;
	       Element nodeElement = (Element)nodeList.item(0);
	       NodeList textNodeList = nodeElement.getChildNodes();
	       if (textNodeList.getLength() == 0) 
	          return null;
	       return ((Node)textNodeList.item(0)).getNodeValue().trim();
	   }
	
}
