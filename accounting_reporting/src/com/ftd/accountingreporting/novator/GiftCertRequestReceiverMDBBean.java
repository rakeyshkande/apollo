
package com.ftd.accountingreporting.novator;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.accountingreporting.novator.handler.GiftCertMessageHandler;
import com.ftd.accountingreporting.util.SystemMessager;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.vo.MessageToken;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import javax.ejb.MessageDrivenBean;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.ejb.MessageDrivenContext;
import javax.jms.TextMessage;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.xml.sax.SAXException;

public class GiftCertRequestReceiverMDBBean implements MessageDrivenBean, MessageListener 
{
    private MessageDrivenContext context;
    private Logger logger = 
        new Logger("com.ftd.accountingreporting.novator.GiftCertRequestReceiverMDBBean");
        
    public void ejbCreate()
    {
    }

    private void processMessage(Message msg) throws Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering processMessage");
            logger.debug("Message : " + msg.toString());
        }
        
        Connection conn=null;
        boolean success=false;
            try
            {
                /* Extract text message from msg */
                MessageToken token = new MessageToken();
                TextMessage textMessage = (TextMessage) msg;
                String textMsg = textMessage.getText();
                
                conn = createDatabaseConnection();
                
                /* Create GiftCertMessageHandler and process message */
                GiftCertMessageHandler giftCertMsgHandler = 
                    new GiftCertMessageHandler(conn);
                giftCertMsgHandler.process(textMsg);
                success=true;
            }catch(ParserConfigurationException pceException){
                logger.error(pceException);
            }catch(SAXException saxException){
                logger.error(saxException);           
            }catch(SQLException sqlException){
                logger.error(sqlException);
            }catch(TransformerException transException){
                logger.error(transException);
            }catch(JMSException jmsException){
                logger.error(jmsException);
            }catch(IOException ioException){
                logger.error(ioException);
            }catch(ClassNotFoundException cnfException){
                logger.error(cnfException);
            }catch(InstantiationException instException){
                logger.error(instException);
            }catch(IllegalAccessException iaException){
                logger.error(iaException);
            }catch(ParseException parseException){
                logger.error(parseException);
            }catch(Throwable e){
                logger.error(e);
                 
        }finally
        {
            if(!success)
            {
              logger.error("can not process the queue. roll back transaction");
              this.context.setRollbackOnly();//rollback transaction.
            }
            if(conn != null && !conn.isClosed()) {
                  logger.debug("Closing connection...");
                  conn.close();
            }
        
            if(logger.isDebugEnabled()){
            logger.debug("Exiting processMessage");
            }
        }        
    }
    public void onMessage(Message msg)
    {
        /*  �	Extract text message from msg
            �	Create GiftCertMessageHandler
            �	Call handler.process(textMsg)
            */
        if(logger.isDebugEnabled()){
            logger.debug("Entering processMessage");
            logger.debug("Message : " + msg.toString());
        }
        
            try
            {
                processMessage(msg);

            }catch(Exception e){
                logger.error(e);

        }finally
        {

            if(logger.isDebugEnabled()){
            logger.debug("Exiting onMessageof Gift Certificate Request Receiver" + 
                     " Message Driven Bean");
            }
            if (context.getRollbackOnly())
            	throw new RuntimeException("Rollback detected, exception thrown to force rollback.");
        }
    }

   
    /**
     * Create a connection to the database
     * @param n/a
     * @return java.sql.Connection
     * @throws Exception
     */
    private Connection createDatabaseConnection() 
        throws IOException, SAXException, ParserConfigurationException, 
            Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering createDatabaseConnection");
        }

        Connection connection=null;
        try{
            /* create a connection to the database */
            connection = DataSourceUtil.getInstance().
                getConnection(ConfigurationUtil.getInstance().getProperty(
                ARConstants.CONFIG_FILE,ARConstants.DATASOURCE_NAME));
                
        }finally{
            if(logger.isDebugEnabled()){
               logger.debug("Exiting createDatabaseConnection");
            }
        }
        
        return connection;
    }
    
    private void logError(String message,Throwable excep, Connection con) throws IOException,
        SAXException,ParserConfigurationException,Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering logError");
        }
    
        
         try{
            
            // log message to log4j log file 
            logger.error(message,excep);
            
            // send a system message 
            SystemMessager sysMessager = new SystemMessager();
            sysMessager.send(message,excep,
                ARConstants.GIFT_CERT_NOVATOR_PROCESS,SystemMessengerVO.LEVEL_PRODUCTION,
                ARConstants.GIFT_CERT_NOVATOR_PROCESS_ERROR_TYPE,con);
        
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting logError");
            } 
       }
    }
    
    private void closeConnection(Connection conn) throws SQLException, Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering closeConnection");
        }

        try
        {
          if(conn != null && !conn.isClosed()) {
              logger.debug("Closing connection...");
              conn.close();
          }

        }finally
        {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting closeConnection");
            }
        }    
    }
    public void ejbRemove()
    {
    }

    public void setMessageDrivenContext(MessageDrivenContext ctx)
    {
        this.context = ctx;
    }
}