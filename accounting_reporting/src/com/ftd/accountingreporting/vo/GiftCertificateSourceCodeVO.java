package com.ftd.accountingreporting.vo;

import java.util.List;
import java.util.Map;

public class GiftCertificateSourceCodeVO {

	private List<String> restrictedSourceCodes;
	
	private Map<String, List<String>> srcCodeMapForUpdate;

	
	
	public List<String> getRestrictedSourceCodes() {
		return restrictedSourceCodes;
	}

	public void setRestrictedSourceCodes(List<String> restrictedSourceCodes) {
		this.restrictedSourceCodes = restrictedSourceCodes;
	}


	public Map<String, List<String>> getSrcCodeMapForUpdate() {
		return srcCodeMapForUpdate;
	}

	public void setSrcCodeMapForUpdate(Map<String, List<String>> srcCodeMapForUpdate) {
		this.srcCodeMapForUpdate = srcCodeMapForUpdate;
	}

	
}
