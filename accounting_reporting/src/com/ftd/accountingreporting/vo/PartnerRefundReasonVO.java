package com.ftd.accountingreporting.vo;

/**
 * This is the value object that models a record in the REFUND_DISP_PARTNER_REF table.
 * @author Charles Fox
 */
public class PartnerRefundReasonVO 
{
    public PartnerRefundReasonVO()
    {
    }
    
    private String partnerId = "";
    private String refundDispCode = "";
    private String partnerRefundReason = "";
    /**
     * gets partnerId
     * This is the partner id that maps to clean.order.origin_id.
     * @return String partnerId
     */
     public String getPartnerId()
     {
        return partnerId;
     }
     
    /**
     * sets partnerId
     * @param newPartnerId  String
     */
     public void setPartnerId(String newPartnerId)
     {
        partnerId = trim(newPartnerId);
     }

    /**
     * gets refundDispCode
     * This is the refund disposition that maps to 
     * clean.refund_disposition_val.refund_disp_code.
     * @return String refundDispCode
     */
     public String getRefundDispCode()
     {
        return refundDispCode;
     }
     
    /**
     * sets refundDispCode
     * @param newRefundDispCode  String
     */
     public void setRefundDispCode(String newRefundDispCode)
     {
        refundDispCode = trim(newRefundDispCode);
     }
     
    /**
     * gets partnerRefundReason
     * This is the partner refund reason that maps to 
     * clean.refund_disp_partner_ref.refund_reason.
     * @return String partnerRefundReason
     */
     public String getPartnerRefundReason()
     {
        return partnerRefundReason;
     }
     
    /**
     * sets partnerRefundReason
     * @param newPartnerRefundReason  String
     */
     public void setPartnerRefundReason(String newPartnerRefundReason)
     {
        partnerRefundReason = trim(newPartnerRefundReason);
     }
     
    private String trim(String str)
    {
        return (str != null)?str.trim():str;
    } 
}