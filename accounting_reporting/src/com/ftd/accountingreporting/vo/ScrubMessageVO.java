package com.ftd.accountingreporting.vo;

/**
 * This is the value object that models a record in the 
 * POP.SCRUB_MESSAGES table. 
 * @author Charles Fox
 */
public class ScrubMessageVO 
{
    public ScrubMessageVO()
    {
    }
    
    private String partnerId = "";
    private String messageType = "";
    private String masterOrderNumber = "";
    private String externalOrderNumber = "";
    private String data;
    private String status = "";
    private String createdBy = "";
    private String errorDescription = "";
  
    /**
     * gets partnerId
     * This is the partner id.
     * @return String partnerId
     */
     public String getPartnerId()
     {
        return partnerId;
     }
     
    /**
     * sets partnerId
     * @param newPartnerId  String
     */
     public void setPartnerId(String newPartnerId)
     {
        partnerId = trim(newPartnerId);
     }

    /**
     * gets messageType
     * This is the message type.
     * @return String messageType
     */
     public String getMessageType()
     {
        return messageType;
     }
     
    /**
     * sets messageType
     * @param newMessageType  String
     */
     public void setMessageType(String newMessageType)
     {
        messageType = trim(newMessageType);
     }


    /**
     * gets masterOrderNumber
     * This is the master order number.
     * @return String masterOrderNumber
     */
     public String getMasterOrderNumber()
     {
        return masterOrderNumber;
     }
     
    /**
     * sets masterOrderNumber
     * @param newMasterOrderNumber  String
     */
     public void setMasterOrderNumber(String newMasterOrderNumber)
     {
        masterOrderNumber = trim(newMasterOrderNumber);
     }

    /**
     * gets externalOrderNumber
     * This is the external order number.
     * @return String externalOrderNumber
     */
     public String getExternalOrderNumber()
     {
        return externalOrderNumber;
     }
     
    /**
     * sets externalOrderNumber
     * @param newExternalOrderNumber  String
     */
     public void setExternalOrderNumber(String newExternalOrderNumber)
     {
        externalOrderNumber = trim(newExternalOrderNumber);
     }

    /**
     * gets data
     * This is the scub message data.
     * @return String data
     */
     public String getData()
     {
        return data;
     }
     
    /**
     * sets data
     * @param newData  String
     */
     public void setData(String newData)
     {
        data = newData;
     }

    /**
     * gets status
     * This is the message status.
     * @return String status
     */
     public String getStatus()
     {
        return status;
     }
     
    /**
     * sets status
     * @param newStatus  String
     */
     public void setStatus(String newStatus)
     {
        status = trim(newStatus);
     }

    /**
     * gets createdBy
     * This is the message source.
     * @return String createdBy
     */
     public String getCreatedBy()
     {
        return createdBy;
     }
     
    /**
     * sets createdBy
     * @param newCreatedBy  String
     */
     public void setCreatedBy(String newCreatedBy)
     {
        createdBy = trim(newCreatedBy);
     }

    /**
     * gets errorDescription
     * This is the error description for outbound error transmission. 
     * Null in this project.
     * @return String errorDescription
     */
     public String getErrorDescription()
     {
        return errorDescription;
     }
     
    /**
     * sets errorDescription
     * @param newErrorDescription  String
     */
     public void setErrorDescription(String newErrorDescription)
     {
        errorDescription = trim(newErrorDescription);
     }   
     
    private String trim(String str)
    {
        return (str != null)?str.trim():str;
    }
}