package com.ftd.accountingreporting.vo;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

/**
 * @author bsurimen
 *
 */
public class PGEODMessageVO {

	private long startTime;
	private long batchTime;
	private long batchNumber;
	private String masterOrderNumber;
	private String paymentMethodId;
	private List paymentIdList;
	private List refundIdList;
	private long retryCounter;
	private long billingDetailId;

	private Logger logger = new Logger("com.ftd.accountingreporting.vo.PGEODMessageVO");

	public PGEODMessageVO() {
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public long getStartTime() {
		return startTime;
	}

	public void setBatchTime(long batchTime) {
		this.batchTime = batchTime;
	}

	public long getBatchTime() {
		return batchTime;
	}

	public void setBatchNumber(long batchNumber) {
		this.batchNumber = batchNumber;
	}

	public long getBatchNumber() {
		return batchNumber;
	}

	public void setMasterOrderNumber(String masterOrderNumber) {
		this.masterOrderNumber = masterOrderNumber;
	}

	public String getMasterOrderNumber() {
		return masterOrderNumber;
	}

	public void setPaymentMethodId(String paymentMethodId) {
		this.paymentMethodId = paymentMethodId;
	}

	public String getPaymentMethodId() {
		return paymentMethodId;
	}

	public void setPaymentIdList(List paymentIdList) {
		this.paymentIdList = paymentIdList;
	}

	public List getPaymentIdList() {
		return paymentIdList;
	}

	public void setRefundIdList(List refundIdList) {
		this.refundIdList = refundIdList;
	}

	public List getRefundIdList() {
		return refundIdList;
	}

	public long getRetryCounter() {
		return retryCounter;
	}

	public void setRetryCounter(long retryCounter) {
		this.retryCounter = retryCounter;
	}

	public String toMessageXML() throws Exception {
		return "<ROOT>" + "<PMI>" + paymentMethodId + "</PMI>" + "<MON>" + masterOrderNumber + "</MON>"
				+ "<BATCH_TIME>" + batchTime + "</BATCH_TIME>" + "<BATCH_NUMBER>" + batchNumber + "</BATCH_NUMBER>"
				+ "<PIDS>" + getIdAsString(paymentIdList, "<PID>", "</PID>") + "</PIDS>" + "<RIDS>"
				+ getIdAsString(refundIdList, "<RID>", "</RID>") + "</RIDS>" + "<RETRY_COUNTER>" + getRetryCounter()
				+ "</RETRY_COUNTER>" + "<DETAIL_ID>" + getBillingDetailId() + "</DETAIL_ID></ROOT>";
	}

	public static PGEODMessageVO xmlToMessageVO(String messageXML) throws Exception {
		PGEODMessageVO eodMessageVO = new PGEODMessageVO();

		List paymentIds = null;
		List refundIds = null;

		Document cartMsgDoc = DOMUtil.getDocument(messageXML);
		eodMessageVO.setPaymentMethodId((((NodeList) cartMsgDoc.getElementsByTagName("PMI")).item(0).getFirstChild())
				.getNodeValue());
		eodMessageVO.setMasterOrderNumber((((NodeList) cartMsgDoc.getElementsByTagName("MON")).item(0).getFirstChild())
				.getNodeValue());
		eodMessageVO.setBatchTime((Long.valueOf((((NodeList) cartMsgDoc.getElementsByTagName("BATCH_TIME")).item(0)
				.getFirstChild()).getNodeValue())).longValue());
		eodMessageVO.setBatchNumber((Long.valueOf((((NodeList) cartMsgDoc.getElementsByTagName("BATCH_NUMBER")).item(0)
				.getFirstChild()).getNodeValue())).longValue());
		eodMessageVO.setRetryCounter((Long.valueOf((((NodeList) cartMsgDoc.getElementsByTagName("RETRY_COUNTER"))
				.item(0).getFirstChild()).getNodeValue())).longValue());

		eodMessageVO.setBillingDetailId((Long.valueOf((((NodeList) cartMsgDoc.getElementsByTagName("DETAIL_ID"))
				.item(0).getFirstChild()).getNodeValue())).longValue());

		NodeList paymentNL = (NodeList) cartMsgDoc.getElementsByTagName("PID");
		NodeList refundNL = (NodeList) cartMsgDoc.getElementsByTagName("RID");

		if (paymentNL != null) {
			if (paymentNL.getLength() > 0) {
				paymentIds = new ArrayList();
				for (int i = 0; i < paymentNL.getLength(); i++) {
					EODPaymentVO vo = new EODPaymentVO();
					vo.setPaymentId((paymentNL.item(i).getFirstChild()).getNodeValue());
					vo.setMasterOrderNumber(eodMessageVO.getMasterOrderNumber());
					vo.setPaymentInd("P");
					paymentIds.add(vo);
				}
				eodMessageVO.setPaymentIdList(paymentIds);
			}
		}
		if (refundNL != null) {
			if (refundNL.getLength() > 0) {
				refundIds = new ArrayList();
				for (int i = 0; i < refundNL.getLength(); i++) {
					EODPaymentVO vo = new EODPaymentVO();
					vo.setPaymentId((refundNL.item(i).getFirstChild()).getNodeValue());
					vo.setMasterOrderNumber(eodMessageVO.getMasterOrderNumber());
					vo.setPaymentInd("R");
					refundIds.add(vo);
				}
				eodMessageVO.setRefundIdList(refundIds);
			}
		}
		return eodMessageVO;
	}

	/**
	 * Returns a string of payment ids for the given list of EODPaymentVO with
	 * tags around the id.
	 * 
	 * @param idList
	 * @param sTag
	 * @param eTag
	 * @return
	 * @throws java.lang.Exception
	 */
	private String getIdAsString(List idList, String sTag, String eTag) throws Exception {
		String ids = "";
		EODPaymentVO vo = null;
		if (idList != null && idList.size() > 0) {
			for (int i = 0; i < idList.size(); i++) {

				vo = (EODPaymentVO) idList.get(i);
				ids += (sTag + vo.getPaymentId() + eTag);
			}
		}
		return ids;
	}

	public long getBillingDetailId() {
		return billingDetailId;
	}

	public void setBillingDetailId(long billingDetailId) {
		this.billingDetailId = billingDetailId;
	}

	@Override
	public String toString() {
		return "PGEODMessageVO [startTime=" + startTime + ", batchTime=" + batchTime + ", batchNumber=" + batchNumber
				+ ", masterOrderNumber=" + masterOrderNumber + ", paymentMethodId=" + paymentMethodId
				+ ", paymentIdList=" + paymentIdList + ", refundIdList=" + refundIdList + ", retryCounter="
				+ retryCounter + ", billingDetailId=" + billingDetailId + "]";
	}
	
	
}
