package com.ftd.accountingreporting.vo;
import java.util.Date;

/**
 * This is the reconciliation message value object. 
 * @author Charles Fox
 */
public class ReconMessageVO 
{

    private String messageId = "";
    private String system = "";
    private double amtToReconcile;
    private double amtReconciled;
    private String fillerId = "";
    private String reconDispCode = "";

    public ReconMessageVO()
    {
    }
    
    /**
     * gets messageId
     * This is the message order number.
     * @return String messageId
     */
     public String getMessageId()
     {
        return messageId;
     }
     
    /**
     * sets messageId
     * @param newMessageId  String
     */
    public void setMessageId(String newMessageId)
     {
        messageId = trim(newMessageId);
     }

    /**
     * gets system
     * This is either MERC or VENUS.
     * @return String system
     */
     public String getSystem()
     {
        return system;
     }
     
    /**
     * sets system
     * @param newSystem  String
     */
    public void setSystem(String newSystem)
     {
        system = trim(newSystem);
     }
     
    /**
     * gets amtToReconcile
     * This is the calculate amount to be reconciled.
     * @return double amtToReconcile
     */
     public double getAmtToReconcile()
     {
        return amtToReconcile;
     }
     
    /**
     * sets amtToReconcile
     * @param newAmtToReconcile  double
     */
    public void setAmtToReconcile (double newAmtToReconcile)
     {
        amtToReconcile = newAmtToReconcile;
     }
     

    /**
     * gets amtReconciled
     * This is the amount received in file.
     * @return double amtReconciled
     */
     public double getAmtReconciled()
     {
        return amtReconciled;
     }
     
    /**
     * sets amtReconciled
     * @param newAmtReconciled  double
     */
    public void setAmtReconciled(double newAmtReconciled)
     {
        amtReconciled = newAmtReconciled;
     }


    /**
     * gets fillerId
     * This is the filling florist or vendor id.
     * @return String fillerId
     */
     public String getFillerId()
     {
        return fillerId;
     }
     
    /**
     * sets fillerId
     * @param newFillerId  String
     */
    public void setFillerId(String newFillerId)
     {
        fillerId = trim(newFillerId);
     }

    /**
     * gets reconDispCode
     * This is the reconciliation disposition code.
     * @return String reconDispCode
     */
     public String getReconDispCode()
     {
        return reconDispCode;
     }
     
    /**
     * sets reconDispCode
     * @param newReconDispCode  String
     */
    public void setReconDispCode(String newReconDispCode)
     {
        reconDispCode = trim(newReconDispCode);
     }
     
    private String trim(String str)
    {
        return (str != null)?str.trim():str;
    } 
    
    /**
   * Overriding Object toString
   * @return 
   */
    public String toString() {
        return "messageId: " + this.messageId + "***" +
               "system: " + this.system + "***" +
               "amtToReconcile: " + this.amtToReconcile + "***" +
               "amtReconciled: " + this.amtReconciled + "***" +
               "fillerId: " + this.fillerId + "***" +
               "reconDispCode: " + this.reconDispCode;
        
    }
}