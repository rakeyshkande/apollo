package com.ftd.accountingreporting.vo;

/**
 * This is the value object that models the POP.SCRUB_MESSAGES.DATA field.  
 * @author Charles Fox
 */
public class ScrubMessageDataVO 
{
    public ScrubMessageDataVO()
    {
    }
    
    private String partnerPO = "";
    private double merchRefundAmount;
    private String shipRefundAmount = "";
    private String reasonCode = "";
   
    /**
     * gets partnerPO
     * This is the partner purchase order number.
     * @return String partnerPO
     */
     public String getPartnerPO()
     {
        return partnerPO;
     }
     
    /**
     * sets partnerPO
     * @param newPartnerPO  String
     */
     public void setPartnerPO(String newPartnerPO)
     {
        partnerPO = trim(newPartnerPO);
     }

    /**
     * gets merchRefundAmount
     * This is the merchandise refund amount
     * @return double merchRefundAmount
     */
     public double getMerchRefundAmount()
     {
        return merchRefundAmount;
     }
     
    /**
     * sets merchRefundAmount
     * @param newMerchRefundAmount  double
     */
     public void setMerchRefundAmount(double newMerchRefundAmount)
     {
        merchRefundAmount = newMerchRefundAmount;
     } 
     
    /**
     * gets shipRefundAmount
     * This is the ship refund amount.
     * @return String shipRefundAmount
     */
     public String getShipRefundAmount()
     {
        return shipRefundAmount;
     }
     
    /**
     * sets shipRefundAmount
     * @param newShipRefundAmount  String
     */
     public void setShipRefundAmount(String newShipRefundAmount)
     {
        shipRefundAmount = trim(newShipRefundAmount);
     }

    /**
     * gets reasonCode
     * This is the Wal-Mart refund reason code.
     * @return String reasonCode
     */
     public String getReasonCode()
     {
        return reasonCode;
     }
     
    /**
     * sets reasonCode
     * @param newReasonCode  String
     */
     public void setReasonCode(String newReasonCode)
     {
        reasonCode = trim(newReasonCode);
     }
     
    private String trim(String str)
    {
        return (str != null)?str.trim():str;
    }
    
    public String toXML()
    {
        String scrubMsgData = "<root><refund walmart-po-number='" + partnerPO + 
                    "' merchandise-refund-amount='" + merchRefundAmount  + "' " + 
                    "shipping-refund-amount='" + shipRefundAmount + "' " + 
                    "reason-code='" + reasonCode + "'/></root>";
        return scrubMsgData;
    }
}

   