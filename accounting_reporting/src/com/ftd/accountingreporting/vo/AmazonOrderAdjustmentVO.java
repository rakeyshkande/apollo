package com.ftd.accountingreporting.vo;

/**
 * This is the value object that models each line item on the bill file. 
 * @author Charles Fox
 */
public class AmazonOrderAdjustmentVO 
{
    public AmazonOrderAdjustmentVO()
    {
    }

    private String refundId = "";
    private String confirmationNumber = "";
    private String adjustmentReason = "";
    private Double principal;
    private Double shipping;
    private Double tax;
    private Double shippingTax;
    private String adjustmentSource = "";
    private String csrUserName = "";

    /**
     * gets refundId
     * This is the refund id 
     * @return String refundId
     */
     public String getRefundId()
     {
        return refundId;
     }
     
    /**
     * sets refundId
     * @param newRefundId  String
     */
    public void setRefundId(String newRefundId)
     {
        refundId = trim(newRefundId);
     }

    /**
     * gets confirmationNumber
     * This is the confirmation number. 
     * @return String confirmationNumber
     */
     public String getConfirmationNumber()
     {
        return confirmationNumber;
     }
     
    /**
     * sets confirmationNumber
     * @param newConfirmationNumber  String
     */
    public void setConfirmationNumber(String newConfirmationNumber)
     {
        confirmationNumber = trim(newConfirmationNumber);
     }
     
    /**
     * gets adjustmentReason
     * This is the adjustment reason. 
     * @return String adjustmentReason
     */
     public String getAdjustmentReason()
     {
        return adjustmentReason;
     }
     
    /**
     * sets adjustmentReason
     * @param newAdjustmentReason  String
     */
    public void setAdjustmentReason(String newAdjustmentReason)
     {
        adjustmentReason = trim(newAdjustmentReason);
     }


    /**
     * gets principal
     * This is the principal amount.
     * @return String principal
     */
     public Double getPrincipal()
     {
        return principal;
     }
     
    /**
     * sets principal
     * @param newPrincipal  String
     */
    public void setPrincipal(Double newPrincipal)
     {
        principal = newPrincipal;
     }

    /**
     * gets shipping
     * This is the shipping fee.
     * @return Double shipping
     */
     public Double getShipping()
     {
        return shipping;
     }
     
    /**
     * sets shipping
     * @param newShipping  Double
     */
    public void setShipping(Double newShipping)
     {
        shipping = newShipping;
     }
     
     
    /**
     * gets tax
     * This is the tax amount.
     * @return Double tax
     */
     public Double getTax()
     {
        return tax;
     }
     
    /**
     * sets tax
     * @param newTax  Double
     */
    public void setTax(Double newTax)
     {
        tax = newTax;
     }
     
    /**
     * gets shippingTax
     * This is the shipping tax. 
     * @return Double shippingTax
     */
     public Double getShippingTax()
     {
        return shippingTax;
     }
     
    /**
     * sets shippingTax
     * @param newShippingTax  Double
     */
    public void setShippingTax(Double newShippingTax)
     {
        shippingTax = newShippingTax;
     }    
    
    /**
     * gets adjustmentSource
     * This is the adjustment source. 
     * @return String adjustmentSource
     */
     public String getAdjustmentSource()
     {
        return adjustmentSource;
     }
     
    /**
     * sets adjustmentSource
     * @param newAdjustmentSource  String
     */
    public void setAdjustmentSource(String newAdjustmentSource)
     {
        adjustmentSource = trim(newAdjustmentSource);
     }
     
    /**
     * gets csrUserName
     * This is the csr user name.
     * @return String csrUserName
     */
     public String getCSRUserName()
     {
        return csrUserName;
     }
     
    /**
     * sets csrUserName
     * @param newCSRUserName  String
     */
    public void setCSRUserName(String newCSRUserName)
     {
        csrUserName = trim(newCSRUserName);
     }
     
    private String trim(String str)
    {
        return (str != null)?str.trim():str;
    } 
}
