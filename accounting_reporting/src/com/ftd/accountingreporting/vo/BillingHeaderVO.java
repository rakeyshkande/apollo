package com.ftd.accountingreporting.vo;
import java.math.BigDecimal;

public class BillingHeaderVO 
{
  private String processedIndicator;
  private BigDecimal batchNumber;

  public BillingHeaderVO(String _processedIndicator, BigDecimal _batchNumber)
  {
    this.processedIndicator = _processedIndicator;
    this.batchNumber = _batchNumber;
  }
  
  public String getProcessedIndicator() 
  {
    return this.processedIndicator;
  }
  
  public BigDecimal getBatchNumber() 
  {
    return this.batchNumber;
  }
}