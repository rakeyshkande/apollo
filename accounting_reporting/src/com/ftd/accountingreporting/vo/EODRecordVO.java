package com.ftd.accountingreporting.vo;

import java.util.Calendar;

import java.text.SimpleDateFormat;

import java.io.Serializable;
import java.math.BigDecimal;
  
/**
 * This class represents the value object for End of Day Detail Record.
 * 
 * @author Madhusudan Parab
 */
public class EODRecordVO
{
    private long billingHeaderId;
    private long billingDetailId;
    private long paymentId;
    private String lineItemType="";
    private String creditCardType="";
    private String creditCardNumber="";
    private String ccExpDate="";
    private double orderAmount;
    private Calendar authorizationDate;
    private String displayAuthorizationDate="";
    private String approvalCode="";
    private String orderNumber="";
    private String transactionType="";
    private String acqReferenceNumber = "";
    private String authCharIndicator="";
    private String responseCode="";
    private String AVSResultCode="";
    private String clearingMemberNumber="";
    private String originType="";
    private String cardHolderRefNumber="";
    private String salesTaxAmount="";
    private String addField1="";
    private String addField2="";
    private String addField3="";
    private String addField4="";
    private String recipientZip="";
    private long orderSequence;
    private String billType="";
    private String paymentMethod="";
    private String orderOrigin="";
    private boolean isSameDayCancel;
    private String orderGuid="";
    private String authTranId="";
    private String authValidationCode="";
    private String authSourceCode="";
    private Calendar cancelDate;
    private Calendar orderDate;
    private String companyId="";    
    private long orderDetailId;
    private long refundId;
    private String programName = "";
    private long walletIndicator;
    private String cscResponseCode = "";
	private double initialAuthAmount;
	private String voiceAuthFlag;
    private String cardinalVerifiedFlag;
    
    
   	/**
     * Constructor for class EODRecordVO
     */
     public EODRecordVO()
     {
     }
     
    public String getCscResponseCode() {
 		return cscResponseCode;
 	}

 	public void setCscResponseCode(String cscResponseCode) {
 		this.cscResponseCode = cscResponseCode;
 	}

	public long getWalletIndicator() {
		return walletIndicator;
	}
	public void setWalletIndicator(long walletIndicator) {
		this.walletIndicator = walletIndicator;
	}

	/**
     * gets programName
     * @return String programName
     */
     public String getProgramName()
     {
        return programName;
     }
     
    /**
     * sets programName
     * @param newProgramName  String
     */
     public void setProgramName(String newProgramName)
     {
        programName = newProgramName;
     }
     
    /**
     * gets lineItemType
     * @return String lineItemType
     */
     public String getLineItemType()
     {
        return lineItemType;
     }
     
    /**
     * sets lineItemType
     * @param lineItemType  String
     */
     public void setLineItemType(String newLineItemType)
     {
        lineItemType = newLineItemType;
     }
      
    /**
     * gets creditCardType
     * @return String creditCardType
     */
     public String getCreditCardType()
     {
        return creditCardType;
     }
       
    /**
     * sets creditCardType
     * @param creditCardType String
     */
     public void setCreditCardType(String newCreditCardType)
     {
        if("CB".equalsIgnoreCase(newCreditCardType)) {
            newCreditCardType = "DC";
        }
        creditCardType = newCreditCardType;
     }
        
    /**
     * gets creditCardNumber
     * @return creditCardNumber
     */
     public String getCreditCardNumber()
     {
        return creditCardNumber;
     }
     
    /**
     * sets creditCardNumber
     * @param String creditCardNumber
     */
     public void setCreditCardNumber(String newCreditCardNumber)
     {
        creditCardNumber = newCreditCardNumber;
     }
    
    /**
     * gets ccExpDate
     * @return ccExpDate
     */
     public String getCcExpDate()
     {
        return ccExpDate;
     }
     
    /**
     * sets ccExpDate
     * @param String ccExpDate
     */
     public void setCcExpDate(String newCcExpDate)
     {
        ccExpDate = newCcExpDate;
     }
     
    /**
     * gets orderAmount
     * @return orderAmount
     */
     public double getOrderAmount()
     {
        return orderAmount;
     }
     
    /**
     * sets orderAmount
     * @param orderAmount double
     */
     public void setOrderAmount(double newOrderAmount)
     {
        orderAmount = newOrderAmount;
     }
     
    public double getInitialAuthAmount() {
		return initialAuthAmount;
	}

	public void setInitialAuthAmount(double initialAuthAmount) {
		this.initialAuthAmount = initialAuthAmount;
	}
	
	

	public String getVoiceAuthFlag() {
		return voiceAuthFlag;
	}

	public void setVoiceAuthFlag(String voiceAuthFlag) {
		this.voiceAuthFlag = voiceAuthFlag;
	}

	/**
     * gets authorizationDate
     * @return authorizationDate
     */
     public Calendar getAuthorizationDate()
     {
        return authorizationDate;
     }
     
    /**
     * sets authorizationDate
     * @param authorizationDate Calendar
     */
     public void setAuthorizationDate(Calendar newAuthorizationDate)
     {
        authorizationDate = newAuthorizationDate;
        setDisplayAuthorizationDate(authorizationDate);
     }
    
    /**
     * gets displayAuthorizationDate
     * @return displayAuthorizationDate
     */
     public String getDisplayAuthorizationDate()
     {
       return displayAuthorizationDate;
     }
     
    /**
     * sets displayAuthorizationDate
     * @param authorizationDate Calendar
     */
     public void setDisplayAuthorizationDate(Calendar newAuthorizationDate)
     {
       SimpleDateFormat sdf = new SimpleDateFormat("MMddyy");
       displayAuthorizationDate = sdf.format(newAuthorizationDate.getTime());
     }
    
    /**
     * gets approvalCode
     * @return approvalCode
     */
     public String getApprovalCode()
     {
        return approvalCode;
     }
    
    /**
     * sets approvalCode
     * @param approvalCode String
     */
     public void setApprovalCode(String newApprovalCode)
     {
        approvalCode = newApprovalCode;
     }
     
    /**
     * gets orderNumber
     * @return orderNumber
     */
     public String getOrderNumber()
     {
        return orderNumber;
     }
    
    /**
     * sets orderNumber
     * @param orderNumber String
     */
     public void setOrderNumber(String newOrderNumber)
     {
        orderNumber = newOrderNumber;
     }
     
    /**
     * gets transactionType
     * @return transactionType
     */
     public String getTransactionType()
     {
        return transactionType;
     }
     
    /**
     * sets transactionType
     * @param transactionType String
     */
     public void setTransactionType(String newTransactionType)
     {
        transactionType = newTransactionType;
     }
    
    /**
     * gets acqReferenceNumber
     * @return acqReferenceNumber
     */
     public String getAcqReferenceNumber() {
        return acqReferenceNumber;
     }

    /**
     * sets acqReferenceNumber
     * @param newAcqReferenceNumber String
     */
     public void setAcqReferenceNumber(String newAcqReferenceNumber) {
        this.acqReferenceNumber = newAcqReferenceNumber;
     }

    /**
     * gets authCharIndicator
     * @return authCharIndicator
     */
     public String getAuthCharIndicator()
     {
        return authCharIndicator;
     }
     
    /**
     * sets authCharIndicator
     * @param authCharIndicator String
     */
     public void setAuthCharIndicator(String newAuthCharIndicator)
     {
        authCharIndicator = newAuthCharIndicator;
     }
     
    /**
     * gets responseCode
     * @return responseCode
     */
     public String getResponseCode()
     {
        return responseCode;
     }
    
    /**
     * sets responseCode
     * @param responseCode String
     */
     public void setResponseCode(String newResponseCode)
     {
        responseCode = newResponseCode;
     }
     
    /**
     * gets AVSResultCode
     * @return AVSResultCode
     */
     public String getAVSResultCode()
     {
        return AVSResultCode;
     }
     
    /**
     * sets AVSResultCode
     * @param AVSResultCode String
     */
     public void setAVSResultCode(String newAVSResultCode)
     {
        AVSResultCode = newAVSResultCode;
     }
     
    /**
     * gets clearingMemberNumber
     * @return clearingMemberNumber
     */
     public String getClearingMemberNumber()
     {
        return clearingMemberNumber;
     }
     
    /**
     * sets clearingMemberNumber
     * @param clearingMemberNumber String
     */
     public void setclearingMemberNumber(String newClearingMemberNumber)
     {
        clearingMemberNumber = newClearingMemberNumber;
     }
     
    /**
     * gets originType
     * @return originType
     */
     public String getOriginType()
     {
        return originType;
     }
     
    /**
     * sets originType
     * @param originType String
     */
     public void setOriginType(String newOriginType)
     {
        originType = newOriginType;
     }
    
    /**
     * gets cardHolderRefNumber
     * @return cardHolderRefNumber
     */
     public String getCardHolderRefNumber()
     {
        return cardHolderRefNumber;
     }
     
    /**
     * sets cardHolderRefNumber
     * @param cardHolderRefNumber String
     */
     public void setCardHolderRefNumber(String newCardHolderRefNumber)
     {
        cardHolderRefNumber = newCardHolderRefNumber;
     }
     
    /**
     * gets salesTaxAmount
     * @return salesTaxAmount
     */
     public String getSalesTaxAmount()
     {
        return salesTaxAmount;
     }
     
    /**
     * sets salesTaxAmount
     * @param salesTaxAmount double
     */
     public void setSalesTaxAmount(String newSalesTaxAmount)
     {
        salesTaxAmount = newSalesTaxAmount;
     }
     
    /**
     * gets addField1
     * @return addField1
     */
     public String getAddField1()
     {
        return addField1;
     }
     
    /**
     * sets addField1
     * @param addField1 String
     */
     public void setAddField1(String newAddField1)
     {
        addField1 = newAddField1;
     }

    /**
     * gets addField2
     * @return addField2
     */
     public String getAddField2()
     {
        return addField2;
     }
     
    /**
     * sets addField2
     * @param addField2 String
     */
     public void setAddField2(String newAddField2)
     {
        addField2 = newAddField2;
     }
     
    /**
     * gets addField3
     * @return addField3
     */
     public String getAddField3()
     {
        return addField3;
     }
     
    /**
     * sets addField3
     * @param addField3 String
     */
     public void setAddField3(String newAddField3)
     {
        addField3 = newAddField3;
     }
     
    /**
     * gets addField4
     * @return addField4
     */
     public String getAddField4()
     {
        return addField4;
     }
     
    /**
     * sets addField4
     * @param addField4 String
     */
     public void setAddField4(String newAddField4)
     {
        addField4 = newAddField4;
     }
     
    /**
     * gets recipientZip
     * @return recipientZip
     */
     public String getRecipientZip()
     {
        return recipientZip;
     }
     
    /**
     * sets recipientZip
     * @param recipientZip String
     */
     public void setRecipientZip(String newRecipientZip)
     {
        recipientZip = newRecipientZip;
     }
     
    /**
     * gets orderSequence
     * @return orderSequence
     */
     public long getOrderSequence()
     {
        return orderSequence;
     }
     
    /**
     * sets orderSequence
     * @param orderSequence long
     */
     public void setOrderSequence(long newOrderSequence)
     {
       orderSequence = newOrderSequence;
     }
     
    /**
     * gets billType
     * @return billType
     */
     public String getBillType()
     {
        return billType;
     }
     
    /**
     * sets billType
     * @param billType String
     */
     public void setBillType(String newBillType)
     {
        billType = newBillType;
     }
    
    /**
     * gets paymentMethod
     * @return paymentMethod
     */
     public String getPaymentMethod()
     {
        return paymentMethod;
     }
     
    /**
     * sets paymentMethod
     * @param paymentMethod String
     */
     public void setPaymentMethod(String newPaymentMethod)
     {
        paymentMethod = newPaymentMethod;
     }
     
    /**
     * gets orderOrigin
     * @return orderOrigin
     */
     public String getOrderOrigin()
     {
        return orderOrigin;
     }
     
    /**
     * sets orderOrigin
     * @param orderOrigin String
     */
     public void setOrderOrigin(String newOrderOrigin)
     {
        orderOrigin = newOrderOrigin;
     }
     
    /**
     * gets isSameDayCancel
     * @return isSameDayCancel
     */
     public boolean getIsSameDayCancel()
     {
        return isSameDayCancel;
     }
    
    /**
     * sets isSameDayCancel
     * @param isSameDayCancel boolean
     */
     public void setIsSameDayCancel(boolean newIsSameDayCancel)
     {
        isSameDayCancel = newIsSameDayCancel;
     }
    
    /**
     * gets orderDetailId
     * @return orderDetailId
     */
     /*public String getOrderDetailId()
     {
        return orderDetailId;
     }*/
     
    /**
     * sets orderDetailId
     * @param orderDetailId String
     */
     /*public void setOrderDetailId(String newOrderDetailId)
     {
        orderDetailId = newOrderDetailId;
     }*/
     
    /**
     * gets billingDetailId
     * @return billingDetailId
     */
     public long getBillingDetailId()
     {
       return billingDetailId;
     }
     
    /**
     * sets billingDetailId
     * @param billingDetailId
     */
     public void setBillingDetailId(long newBillingDetailId)
     {
       billingDetailId = newBillingDetailId;
     }

    /**
     * gets billingHeaderId
     * @return billingHeaderId
     */
     public long getBillingHeaderId()
     {
       return billingHeaderId;
     }
     
    /**
     * sets billingHeaderId
     * @param billingHeaderId
     */
     public void setBillingHeaderId(long newBillingHeaderId)
     {
       billingHeaderId = newBillingHeaderId;
     }

    /**
     * gets paymentId
     * @return paymentId
     */
     public long getPaymentId()
     {
       return paymentId;
     }
     
    /**
     * sets paymentId
     * @param paymentId
     */
     public void setPaymentId(long newPaymentId)
     {
       paymentId = newPaymentId;
     }
     
    /**
     * gets authTranId
     * @return authTranId
     */
     public String getAuthTranId()
     {
        return authTranId;
     }
     
    /**
     * sets authTranId
     * @param authTranId String
     */
     public void setAuthTranId(String newAuthTranId)
     {
        authTranId = newAuthTranId;
     }
     
    /**
     * gets authValidationCode
     * @return authValidationCode
     */
     public String getAuthValidationCode()
     {
        return authValidationCode;
     }
     
    /**
     * sets authValidationCode
     * @param authValidationCode String
     */
     public void setAuthValidationCode(String newAuthValidationCode)
     {
        authValidationCode = newAuthValidationCode;
     }

    /**
     * gets authSourceCode
     * @return authSourceCode
     */
     public String getAuthSourceCode()
     {
        return authSourceCode;
     }
     
    /**
     * sets authSourceCode
     * @param authSourceCode String
     */
     public void setAuthSourceCode(String newAuthSourceCode)
     {
        authSourceCode = newAuthSourceCode;
     }
     
    /**
     * gets cancelDate
     * @return cancelDate
     */
     public Calendar getCancelDate()
     {
        return cancelDate;
     }
     
    /**
     * sets cancelDate
     * @param cancelDate Calendar
     */
     public void setCancelDate(Calendar newCancelDate)
     {
        cancelDate = newCancelDate;
     }
     
    /**
     * gets orderDate
     * @return orderDate
     */
     public Calendar getOrderDate()
     {
        return orderDate;
     }
     
    /**
     * sets orderDate
     * @param orderlDate Calendar
     */
     public void setOrderDate(Calendar newOrderDate)
     {
        orderDate = newOrderDate;
     }

    /**
     * gets companyId
     * @return companyId
     */
     public String getCompanyId()
     {
        return companyId;
     }
     
    /**
     * sets companyId
     * @param companyId String
     */
     public void setCompanyId(String newCompanyId)
     {
        companyId = newCompanyId;
     }
     
    /**
     * gets orderDetailId
     * @return orderDetailId
     */
     public long getOrderDetailId()
     {
        return orderDetailId;
     }
     
    /**
     * sets orderDetailId
     * @param orderDetailId long
     */
     public void setOrderDetailId(long newOrderDetailId)
     {
        orderDetailId = newOrderDetailId;
     }
     
    /**
     * gets orderGuid
     * @return orderGuid
     */
     public String getOrderGuid()
     {
        return orderGuid;
     }
     
    /**
     * sets orderGuid
     * @param orderGuid String
     */
     public void setOrderGuid(String newOrderGuid)
     {
        orderGuid = newOrderGuid;
     }

    /**
     * gets refundId
     * @return refundId
     */
     public long getRefundId()
     {
        return refundId;
     }
     
    /**
     * sets refundId
     * @param refundId long
     */
     public void setRefundId(long newRefundId)
     {
        refundId = newRefundId;
     }
     
     public void reset() {
        lineItemType = null;
        creditCardType = null;
        creditCardNumber = null;
        ccExpDate = null;
        authorizationDate = null;
        displayAuthorizationDate = null;
        approvalCode = null;
        orderNumber = null;
        transactionType = null;
        authCharIndicator = null;
        responseCode = null;
        AVSResultCode = null;
        clearingMemberNumber = null;
        originType = null;
        cardHolderRefNumber = null;
        salesTaxAmount = null;
        addField1 = null;
        addField2 = null;
        addField3 = null;
        addField4 = null;
        recipientZip = null;
        billType = null;
        paymentMethod = null;
        orderOrigin = null;
        orderGuid = null;
        authTranId = null;
        authValidationCode = null;
        authSourceCode = null;
        cancelDate = null;
        orderDate = null;
        companyId = null;
        
     }

	

	public void setSameDayCancel(boolean isSameDayCancel) {
		this.isSameDayCancel = isSameDayCancel;
	}

	public String getCardinalVerifiedFlag() {
		return cardinalVerifiedFlag;
	}

	public void setCardinalVerifiedFlag(String cardinalVerifiedFlag) {
		this.cardinalVerifiedFlag = cardinalVerifiedFlag;
	}
     
     
}
