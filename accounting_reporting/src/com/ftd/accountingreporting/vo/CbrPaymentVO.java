package com.ftd.accountingreporting.vo;

public class CbrPaymentVO 
{
    public CbrPaymentVO()
    {
    }
    
    private String paymentId = "";
    private String paymentType = "";
    private String ccId = "";
    private String gcId = null;
    private double mpRedemptionRateAmt = 0;


    /**
     * gets paymentId, This is the payment id.
     * @return String paymentId
     */
     public String getPaymentID()
     {
        return paymentId;
     }
     
    /**
     * sets paymentId
     * @param newPaymentID  String
     */
     public void setPaymentID(String newPaymentID)
     {
        paymentId = trim(newPaymentID);
     }

    /**
     * gets paymentType, This is the payment type.
     * @return String paymentType
     */
     public String getPaymentType()
     {
        return paymentType;
     }
     
    /**
     * sets paymentType
     * @param newPaymentType  String
     */
     public void setPaymentType(String newPaymentType)
     {
        paymentType = trim(newPaymentType);
     }

    /**
     * gets ccId, This is the credit card id.
     * @return String ccId
     */
     public String getCCId()
     {
        return ccId;
     }
     
    /**
     * sets ccId
     * @param newCCId  String
     */
     public void setCCId(String newCCId)
     {
        ccId = trim(newCCId);
     }

    /**
     * gets gcId, This is the gift cert id. Null in this app.
     * @return String gcId
     */
     public String getGCId()
     {
        return gcId;
     }
     
    /**
     * sets gcId
     * @param newGCId  String
     */
     public void setGCId(String newGCId)
     {
        gcId = newGCId;
     }
     
    private String trim(String str)
    {
        return (str != null)?str.trim():str;
    }

  public void setMpRedemptionRateAmt(double mpRedemptionRateAmt)
  {
    this.mpRedemptionRateAmt = mpRedemptionRateAmt;
  }

  public double getMpRedemptionRateAmt()
  {
    return mpRedemptionRateAmt;
  }
}
