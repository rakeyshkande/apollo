package com.ftd.accountingreporting.vo;
import com.ftd.accountingreporting.common.XMLInterface;
import com.ftd.osp.utilities.xml.DOMUtil;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * This is the value object that models a Charge Back or Retrieval comment. 
 * @author Charles Fox
 */
public class CbrCommentVO implements XMLInterface
{
    public CbrCommentVO()
    {
    }

    private String comment_text = "";
    private String created_by = "";
    private Date created_on;

    /**
     * gets comment_text, This is the comment text.
     * @return String comment_text
     */
     public String getCommentText()
     {
        return comment_text;
     }
     
    /**
     * sets comment_text
     * @param newCommentText String
     */
     public void setCommentText(String newCommentText)
     {
        comment_text = trim(newCommentText);
     }
     
    /**
     * gets created_on, This is the date the comment is created.
     * @return String created_on
      */
     public Date getCreateOn()
     {
        return created_on;
     }
    
    /**
     * sets created_on
     * @param newCreateOn  Date
     */
     public void setCreateOn(Date newCreateOn)
     {
        created_on = newCreateOn;
     }

    /**
     * This is the user who creates the comment.
     * @return String created_by
     */
     public String getCreatedBy()
     {
        return created_by;
     }
     
    /**
     * sets created_by
     * @param newCreatedBy String
     */
     public void setCreatedBy(String newCreatedBy)
     {
        created_by = trim(newCreatedBy);
     }
     
    public String toXML()
        throws IllegalAccessException, ClassNotFoundException
    {
        StringBuffer sb = new StringBuffer();
     
        try
        {
            sb.append("<cbr_comments>");
            sb.append("<cbr_comment>");
            Field[] fields = this.getClass().getDeclaredFields();
            
            appendFields(sb, fields);
            sb.append("</cbr_comment>");
            sb.append("</cbr_comments>");
            
 
        } finally {

        }
            
        return sb.toString();
    }

    protected void appendFields(StringBuffer sb, Field[] fields) 
        throws IllegalAccessException, ClassNotFoundException
    {

        try
        {   
            for (int i = 0; i < fields.length; i++)
            {
              //if the field retrieved was a list of VO
              if(fields[i].getType().equals(Class.forName("java.util.List")))
              {
                List list = (List)fields[i].get(this);
                if(list != null)
                {
                  for (int j = 0; j < list.size(); j++)
                  {
                    XMLInterface xmlInt = (XMLInterface)list.get(j);
                    String sXmlVO = xmlInt.toXML();
                    sb.append(sXmlVO);
                  }
                }
              }
              else
              {
                //if the field retrieved was a VO
                if (fields[i].getType().toString().matches("(?i).*vo"))
                {
                  XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
                  String sXmlVO = xmlInt.toXML();
                  sb.append(sXmlVO);
                }
                //if the field retrieved was a Calendar object
                else if (fields[i].getType().toString().matches("(?i).*calendar"))
                {
                  Date date;
                  String fDate = null;
                  if (fields[i].get(this) != null)
                  {
                    date = (((GregorianCalendar)fields[i].get(this)).getTime());
                    SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
                    fDate = sdf.format(date).toString();
                  }
                  sb.append("<" + fields[i].getName() + ">");
                  sb.append(fDate);
                  sb.append("</" + fields[i].getName() + ">");
                }
                else if (fields[i].getType().toString().matches("(?i).*date"))
                {
                  Date date;
                  String fDate = null;
                  if (fields[i].get(this) != null)
                  {
                    date = (((Date)fields[i].get(this)));
                    SimpleDateFormat sdf = new SimpleDateFormat ("MM/dd/yyyy");
                    fDate = sdf.format(date).toString();
                  }
                  if(fDate!=null){
                      sb.append("<" + fields[i].getName() + ">");
                      sb.append(fDate);
                      sb.append("</" + fields[i].getName() + ">");
                  }
                  else
                  {
                        sb.append("<" + fields[i].getName() + "/>");
                  }
                }
                else
                {
                  
                  sb.append("<" + fields[i].getName() + ">");
                  String value = "";
        
                  if(fields[i].get(this)!=null){
                      value = fields[i].get(this).toString();    
                      value = DOMUtil.encodeChars(value);
                  }
                  else
                  {
                      value = "";    
                  }
                      
        
                  sb.append(value);
                  sb.append("</" + fields[i].getName() + ">");
                }
              }
            }

        } finally {

        }
    }
    
    private String trim(String str)
    {
        return (str != null)?str.trim():str;
    }
}