package com.ftd.accountingreporting.vo;
import java.lang.CloneNotSupportedException;

public class VendorJdeVO implements Cloneable
{
  String vendorCode;
  String FTDVendorCodeNo;
  String FTDMemberNo;
  String lineType;
  String billingDate;
  String shipDate;
  String vendorOrderNo;
  String memberRefNo;
  String vendorItemNo;
  String vendorItemDesc1;
  String vendorItemDesc2;
  String qtyShipped;
  String paymentTerms;
  String unitPriceUSD;
  String unitCostUSD;
  String unitPriceCAD;
  String unitCostCAD;
  String debitCreditInd;
  String serialNo;

  public VendorJdeVO()
  {
  }

  public String getVendorCode()
  {
    return vendorCode;
  }

  public void setVendorCode(String newVendorCode)
  {
    vendorCode = newVendorCode;
  }

  public String getFTDVendorCodeNo()
  {
    return FTDVendorCodeNo;
  }

  public void setFTDVendorCodeNo(String newFTDVendorCodeNo)
  {
    FTDVendorCodeNo = newFTDVendorCodeNo;
  }

  public String getFTDMemberNo()
  {
    return FTDMemberNo;
  }

  public void setFTDMemberNo(String newFTDMemberNo)
  {
    FTDMemberNo = newFTDMemberNo;
  }

  public String getLineType()
  {
    return lineType;
  }

  public void setLineType(String newLineType)
  {
    lineType = newLineType;
  }

  public String getBillingDate()
  {
    return billingDate;
  }

  public void setBillingDate(String newBillingDate)
  {
    billingDate = newBillingDate;
  }

  public String getShipDate()
  {
    return shipDate;
  }

  public void setShipDate(String newShipDate)
  {
    shipDate = newShipDate;
  }

  public String getVendorOrderNo()
  {
    return vendorOrderNo;
  }

  public void setVendorOrderNo(String newVendorOrderNo)
  {
    vendorOrderNo = newVendorOrderNo;
  }

  public String getMemberRefNo()
  {
    return memberRefNo;
  }

  public void setMemberRefNo(String newMemberRefNo)
  {
    memberRefNo = newMemberRefNo;
  }

  public String getVendorItemNo()
  {
    return vendorItemNo;
  }

  public void setVendorItemNo(String newVendorItemNo)
  {
    vendorItemNo = newVendorItemNo;
  }

  public String getVendorItemDesc1()
  {
    return vendorItemDesc1;
  }

  public void setVendorItemDesc1(String newVendorItemDesc1)
  {
    vendorItemDesc1 = newVendorItemDesc1;
  }

  public String getVendorItemDesc2()
  {
    return vendorItemDesc2;
  }

  public void setVendorItemDesc2(String newVendorItemDesc2)
  {
    vendorItemDesc2 = newVendorItemDesc2;
  }

  public String getQtyShipped()
  {
    return qtyShipped;
  }

  public void setQtyShipped(String newQtyShipped)
  {
    qtyShipped = newQtyShipped;
  }

  public String getPaymentTerms()
  {
    return paymentTerms;
  }

  public void setPaymentTerms(String newPaymentTerms)
  {
    paymentTerms = newPaymentTerms;
  }

  public String getUnitPriceUSD()
  {
    return unitPriceUSD;
  }

  public void setUnitPriceUSD(String newUnitPriceUSD)
  {
    unitPriceUSD = newUnitPriceUSD;
  }

  public String getUnitCostUSD()
  {
    return unitCostUSD;
  }

  public void setUnitCostUSD(String newUnitCostUSD)
  {
    unitCostUSD = newUnitCostUSD;
  }

  public String getUnitPriceCAD()
  {
    return unitPriceCAD;
  }

  public void setUnitPriceCAD(String newUnitPriceCAD)
  {
    unitPriceCAD = newUnitPriceCAD;
  }

  public String getUnitCostCAD()
  {
    return unitCostCAD;
  }

  public void setUnitCostCAD(String newUnitCostCAD)
  {
    unitCostCAD = newUnitCostCAD;
  }

  public String getDebitCreditInd()
  {
    return debitCreditInd;
  }

  public void setDebitCreditInd(String newDebitCreditInd)
  {
    debitCreditInd = newDebitCreditInd;
  }

  public String getSerialNo()
  {
    return serialNo;
  }

  public void setSerialNo(String newSerialNo)
  {
    serialNo = newSerialNo;
  }
  
  /**
   * Deep copy of unitCostUSD
   * @return 
   */
  public Object clone() {
      VendorJdeVO newVo = null;
      try {
          newVo = (VendorJdeVO)super.clone();
          newVo.setUnitCostUSD(new String(this.unitCostUSD));
      } catch(CloneNotSupportedException e) {
          newVo = new VendorJdeVO();
          newVo.setBillingDate(this.billingDate);
          newVo.setDebitCreditInd(this.debitCreditInd);
          newVo.setFTDMemberNo(this.FTDMemberNo);
          newVo.setFTDVendorCodeNo(this.FTDVendorCodeNo);
          newVo.setLineType(this.lineType);
          newVo.setMemberRefNo(this.memberRefNo);
          newVo.setPaymentTerms(this.paymentTerms);
          newVo.setQtyShipped(this.qtyShipped);
          newVo.setSerialNo(this.serialNo);
          newVo.setShipDate(this.shipDate);
          newVo.setUnitCostCAD(this.unitCostCAD);
          newVo.setUnitCostUSD(new String(this.unitCostUSD));
          newVo.setUnitPriceCAD(this.unitPriceCAD);
          newVo.setUnitPriceUSD(this.unitPriceUSD);
          newVo.setVendorCode(this.vendorCode);
          newVo.setVendorItemDesc1(this.vendorItemDesc1);
          newVo.setVendorItemDesc2(this.vendorItemDesc2);
          newVo.setVendorItemNo(this.vendorItemNo);
          newVo.setVendorOrderNo(this.vendorOrderNo);
      }
      return newVo;
  }
  
}