package com.ftd.accountingreporting.vo;

/**
 * This is the value object that models a record in the PARTNER_REFUND table.
 * @author Charles Fox
 */
public class PartnerRefundVO 
{
    public PartnerRefundVO()
    {
    }
    
    private String partnerId = "";
    private String refundId = "";
    private String fieldName = "";
    private String fieldValue = "";
    
    /**
     * gets partnerId
     * This is the partner id that maps to clean.order.origin_id. 
     * @return String partnerId
     */
     public String getPartnerId()
     {
        return partnerId;
     }
     
    /**
     * sets partnerId
     * @param newPartnerId  String
     */
    public void setPartnerId(String newPartnerId)
     {
        partnerId = trim(newPartnerId);
     }
     
    /**
     * gets refundId
     * This is the refund id that maps to clean.refund.refund_id. 
     * @return String refundId
     */
     public String getRefundId()
     {
        return refundId;
     }
     
    /**
     * sets refundId
     * @param newRefundId  String
     */
    public void setRefundId(String newRefundId)
     {
        refundId = trim(newRefundId);
     }

    /**
     * gets fieldName
     * This is the field name specific to a partnerís refund. 
     * @return String fieldName
     */
     public String getFieldName()
     {
        return fieldName;
     }
     
    /**
     * sets fieldName
     * @param newFieldName  String
     */
    public void setFieldName(String newFieldName)
     {
        fieldName = trim(newFieldName);
     }

    /**
     * gets fieldValue
     * This is the value of the partnerís refund field. 
     * @return String fieldValue
     */
     public String getFieldValue()
     {
        return fieldValue;
     }
     
    /**
     * sets fieldValue
     * @param newFieldValue  String
     */
    public void setFieldValue(String newFieldValue)
     {
        fieldValue = trim(newFieldValue);
     }
     
    private String trim(String str)
    {
        return (str != null)?str.trim():str;
    }
  
}