package com.ftd.accountingreporting.vo;

import java.io.Serializable;
/**
 * This class represents the value object for Order Details.
 * 
 * @author Madhusudan Parab
 */
public class OrderDetailVO implements Serializable
{
 
  
  private long orderDetailId;
  private long recipientId;
  private String aribaPONumber;
  private String productId;
  private String productLevel;
 
 /**
  * constructor for class Order Details Value Object
  */
  public OrderDetailVO()
  {
  }
  
 /**
  * sets orderDetailId
  */
  public void setOrderDetailId(long newOrderDetailId)
  {
    orderDetailId = newOrderDetailId;
  }
  
 /**
  * get orderDetailId
  */
  public long getOrderDetailId()
  {
    return orderDetailId;
  }
  
 /**
  * sets recipientId;
  */
  public void setRecipientId(long newRecipientId)
  {
    recipientId = newRecipientId;
  }
  
 /**
  * get recipientId
  */
  public long getRecipientId()
  {
    return recipientId;
  }
  
 /**
  * sets aribaPONumber
  */
  public void setAribaPONumber(String newAribaPONumber)
  {
    aribaPONumber = newAribaPONumber;
  }
  
 /**
  * get aribaPONumber
  */
  public String getAribaPONumber()
  {
    return aribaPONumber;
  }

 /**
  * sets productId
  */
  public void setProductId(String newProductId)
  {
    productId = newProductId;
  }
 
 /**
  * get productId
  */
  public String getProductId()
  {
    return productId;
  }
  
 /**
  * sets productLevel
  */
  public void setProductLevel(String newProductLevel)
  {
    productLevel = newProductLevel;
  }
  
 /**
  * get productLevel
  */
  public String getProductLevel()
  {
    return productLevel;
  }
} 