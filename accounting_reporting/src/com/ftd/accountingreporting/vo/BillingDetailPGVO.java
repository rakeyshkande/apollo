package com.ftd.accountingreporting.vo;

public class BillingDetailPGVO {

	private long billingHeaderId;
	private long billingDetailId;
	private String orderNumber;
	private long paymentId;
	private double orderAmount;
	private String transactionType = "";
	private String responseCode = "";
	private String status = "";
	private String responseMessage = "";
	private String authTranId = "";
	private String settlementTranId = "";
	private String refundTranId = "";
	private String billType = "";
	private String voiceAuthFlag = "";
	private String requestId = "";
	private String paymentMethod ="";
	
	public long getBillingHeaderId() {
		return billingHeaderId;
	}
	public void setBillingHeaderId(long billingHeaderId) {
		this.billingHeaderId = billingHeaderId;
	}
	public long getBillingDetailId() {
		return billingDetailId;
	}
	public void setBillingDetailId(long billingDetailId) {
		this.billingDetailId = billingDetailId;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public long getPaymentId() {
		return paymentId;
	}
	public void setPaymentId(long paymentId) {
		this.paymentId = paymentId;
	}
	public double getOrderAmount() {
		return orderAmount;
	}
	public void setOrderAmount(double orderAmount) {
		this.orderAmount = orderAmount;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	public String getAuthTranId() {
		return authTranId;
	}
	public void setAuthTranId(String authTranId) {
		this.authTranId = authTranId;
	}
	public String getSettlementTranId() {
		return settlementTranId;
	}
	public void setSettlementTranId(String settlementTranId) {
		this.settlementTranId = settlementTranId;
	}
	public String getRefundTranId() {
		return refundTranId;
	}
	public void setRefundTranId(String refundTranId) {
		this.refundTranId = refundTranId;
	}
	public String getBillType() {
		return billType;
	}
	public void setBillType(String billType) {
		this.billType = billType;
	}
	public String getVoiceAuthFlag() {
		return voiceAuthFlag;
	}
	public void setVoiceAuthFlag(String voiceAuthFlag) {
		this.voiceAuthFlag = voiceAuthFlag;
	}
	public String getRequestId() {
		return requestId;
	}
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	
	
}
