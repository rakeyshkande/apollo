package com.ftd.accountingreporting.vo;

import java.io.Serializable;
  
/**
 * This class represents the value object for a credit balance object.
 * 
 */
public class EODCreditBalanceVO
{
    private String ccNumber;
    private double balance;
    private boolean dipped; // whether a credit card refund has dipped for the balance.
    
    /**
     * Constructor for class EODPaymentVO
     */
     public EODCreditBalanceVO()
     {
     }


  public void setCcNumber(String ccNumber)
  {
    this.ccNumber = ccNumber;
  }


  public String getCcNumber()
  {
    return ccNumber;
  }


  public void setBalance(double balance)
  {
    this.balance = balance;
  }


  public double getBalance()
  {
    return balance;
  }


  public void setDipped(boolean dipped)
  {
    this.dipped = dipped;
  }


  public boolean isDipped()
  {
    return dipped;
  }

  public void add(double amt) {
    balance += amt;
  }
  
  public void substract(double amt) {
    balance -= amt;
  }

}