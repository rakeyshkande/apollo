package com.ftd.accountingreporting.vo;

public class PartnerPCardVO 
{
    public PartnerPCardVO()
    {
    }
    
    String programName = "";
    String fieldName = "";
    int sortOrder = 0;
    String sourceTable = "";
    String sourceField = "";
    String fieldPrefix = "";
    String configId = "";
    String fieldSuffix = "";
    String currencyDecimalInd = "";

    /**
     * gets programName
     * @return String programName
     */
     public String getProgramName()
     {
        return programName;
     }
     
    /**
     * sets programName
     * @param newProgramName  String
     */
     public void setProgramName(String newProgramName)
     {
        programName = trim(newProgramName);
     }

    /**
     * gets fieldName
     * @return String fieldName
     */
     public String getFieldName()
     {
        return fieldName;
     }
     
    /**
     * sets fieldName
     * @param newFieldName  String
     */
     public void setFieldName(String newFieldName)
     {
        fieldName = trim(newFieldName);
     }
     
    /**
     * gets sortOrder
     * @return int sortOrder
     */
     public int getSortOrder()
     {
        return sortOrder;
     }
     
    /**
     * sets sortOrder
     * @param newSortOrder  int
     */
     public void setSortOrder(int newSortOrder)
     {
        sortOrder = newSortOrder;
     }

    /**
     * gets sourceTable
     * @return String sourceTable
     */
     public String getSourceTable()
     {
        return sourceTable;
     }
     
    /**
     * sets sourceTable
     * @param newSourceTable  String
     */
     public void setSourceTable(String newSourceTable)
     {
        sourceTable = trim(newSourceTable);
     }
     
    /**
     * gets sourceField
     * @return String sourceField
     */
     public String getSourceField()
     {
        return sourceField;
     }
     
    /**
     * sets sourceField
     * @param newSourceField  String
     */
     public void setSourceField(String newSourceField)
     {
        sourceField = trim(newSourceField);
     }

    /**
     * gets fieldPrefix
     * @return String fieldPrefix
     */
     public String getFieldPrefix()
     {
        return fieldPrefix;
     }
     
    /**
     * sets fieldPrefix
     * @param newFieldPrefix  String
     */
     public void setFieldPrefix(String newFieldPrefix)
     {
        fieldPrefix = trim(newFieldPrefix);
     }  
     
     public String getConfigId(){
        return configId;
     }
     
     public void setConfigId(String newConfigId) {
        configId = newConfigId;
     }
     
    private String trim(String str)
    {
        return (str != null)?str.trim():str;
    } 


  public void setFieldSuffix(String fieldSuffix)
  {
    this.fieldSuffix = fieldSuffix;
  }


  public String getFieldSuffix()
  {
    return fieldSuffix;
  }


  public void setCurrencyDecimalInd(String currencyDecimalInd)
  {
    this.currencyDecimalInd = currencyDecimalInd;
  }


  public String getCurrencyDecimalInd()
  {
    return currencyDecimalInd;
  }
}