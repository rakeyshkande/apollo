package com.ftd.accountingreporting.vo;
import java.util.Date;

/**
 * This is the value object that models a record in the POP.ITEMS_SHIPPED table. 
 * @author Charles Fox
 */
public class ItemsShippedVO 
{
    public ItemsShippedVO()
    {
    }
    
    private String partnerId = "";
    private Date shipDate;
    private String masterOrderNumber = "";
    private String externalOrderNumber = "";
    private String shippedFlag = "";
    private String status = "";

    /**
     * gets partnerId
     * This is the partner id.
     * @return String partnerId
     */
     public String getPartnerId()
     {
        return partnerId;
     }
     
    /**
     * sets partnerId
     * @param newPartnerId  String
     */
     public void setPartnerId(String newPartnerId)
     {
        partnerId = trim(newPartnerId);
     }

    /**
     * gets shipDate
     * This is the ship date
     * @return String shipDate
     */
     public Date getShipDate()
     {
        return shipDate;
     }
     
    /**
     * sets shipDate
     * @param newShipDate  Date
     */
     public void setShipDate(Date newShipDate)
     {
        shipDate = newShipDate;
     }

    /**
     * gets masterOrderNumber
     * This is the master order number.
     * @return String masterOrderNumber
     */
     public String getMasterOrderNumber()
     {
        return masterOrderNumber;
     }
     
    /**
     * sets masterOrderNumber
     * @param newMasterOrderNumber  String
     */
     public void setMasterOrderNumber(String newMasterOrderNumber)
     {
        masterOrderNumber = trim(newMasterOrderNumber);
     }

    /**
     * gets externalOrderNumber
     * This is the external order number.
     * @return String externalOrderNumber
     */
     public String getExternalOrderNumber()
     {
        return externalOrderNumber;
     }
     
    /**
     * sets externalOrderNumber
     * @param newExternalOrderNumber  String
     */
     public void setExternalOrderNumber(String newExternalOrderNumber)
     {
        externalOrderNumber = trim(newExternalOrderNumber);
     }
     
    /**
     * gets shippedFlag
     * This is the shipped flag.
     * @return String shippedFlag
     */
     public String getShippedFlag()
     {
        return shippedFlag;
     }
     
    /**
     * sets shippedFlag
     * @param newShippedFlag  String
     */
     public void setShippedFlag(String newShippedFlag)
     {
        shippedFlag = trim(newShippedFlag);
     }

    /**
     * gets status
     * This is the message status.
     * @return String status
     */
     public String getStatus()
     {
        return status;
     }
     
    /**
     * sets status
     * @param newStatus  String
     */
     public void setStatus(String newStatus)
     {
        status = trim(newStatus);
     }
     
    private String trim(String str)
    {
        return (str != null)?str.trim():str;
    } 
}