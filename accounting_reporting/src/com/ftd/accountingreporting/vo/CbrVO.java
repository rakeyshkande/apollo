package com.ftd.accountingreporting.vo;
import com.ftd.accountingreporting.common.XMLInterface;
import com.ftd.accountingreporting.util.AccountingUtil;
import com.ftd.osp.utilities.xml.DOMUtil;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * This is the value object that models a Charge Back or Retrieval record. 
 * @author Charles Fox
 */
public class CbrVO implements XMLInterface
{
    public CbrVO()
    {
    }
    
    private String master_order_number = "";
    private String cbr_id = "";
    private String type_code = "";
    private Date received_date;
    private double amount;
    private String reason_code = "";
    private Date due_date;
    private Date reversal_date;
    private Date returned_date;
    private String credit_payment_id = "";
    private String comment = "";
    
    /**
     * gets master_order_number, This is the shopping cart number.
     * @return String master_order_number
     */
     public String getMasterOrderNumber()
     {
        return master_order_number;
     }
     
    /**
     * sets master_order_number
     * @param newMasterOrderNumber  String
     */
     public void setMasterOrderNumber(String newMasterOrderNumber)
     {
        master_order_number = trim(newMasterOrderNumber);
     }

    /**
     * gets cbr_id, This is the cbr_id.
     * @return String cbr_id
     */
     public String getCBRId()
     {
        return cbr_id;
     }
     
    /**
     * sets cbr_id
     * @param newCBRId  String
     */
     public void setCBRId(String newCBRId)
     {
        cbr_id = trim(newCBRId);
     }

    /**
     * gets type_code, This is either CB or RT.
     * @return String type_code
     */
     public String getTypeCode()
     {
        return type_code;
     }
     
    /**
     * sets type_code
     * @param newTypeCode  String
     */
     public void setTypeCode(String newTypeCode)
     {
        type_code = trim(newTypeCode);
     }

    /**
     * gets received_date, This is the date the Charge Back or Retrieval is received.
     * @return String received_date
      */
     public Date getReceivedDate()
     {
        return received_date;
     }
    
    /**
     * sets received_date
     * @param newReceivedDate  Date
     */
     public void setReceivedDate(Date newReceivedDate)
     {
        received_date = newReceivedDate;
     }
     
    /**
     * gets amount, This is the amount of the Charge Back or Retrieval.
     * @return double amount
      */
     public double getAmount()
     {
        return amount;
     }
    
    /**
     * sets amount
     * @param newAmount  double
     */
     public void setAmount(double newAmount)
     {
        amount = newAmount;
     }
     
    /**
     * gets reason_code, This is the reason code for the cbr.
     * @return String reason_code
     */
     public String getReasonCode()
     {
        return reason_code;
     }
     
    /**
     * sets reason_code
     * @param newReasonCode  String
     */
     public void setReasonCode(String newReasonCode)
     {
        reason_code = trim(newReasonCode);
     }

    /**
     * gets due_date, This is the due date
     * @return String due_date
      */
     public Date getDueDate()
     {
        return due_date;
     }
    
    /**
     * sets due_date
     * @param newDueDate  Date
     */
     public void setDueDate(Date newDueDate)
     {
        due_date = newDueDate;
     }
     
    /**
     * gets reversal_date, This is the reversal date of the charge back.
     * @return String reversal_date
      */
     public Date getReversalDate()
     {
        return reversal_date;
     }
    
    /**
     * sets reversal_date
     * @param newReversalDate  Date
     */
     public void setReversalDate(Date newReversalDate)
     {
        reversal_date = newReversalDate;
     }
     
    /**
     * gets returned_date, This is the returned date of the reversal.
     * @return Date returned_date
      */
     public Date getReturnedDate()
     {
        return returned_date;
     }
    
    /**
     * sets returned_date
     * @param newReturnedDate  Date
     */
     public void setReturnedDate(Date newReturnedDate)
     {
        returned_date = newReturnedDate;
     }
     
    /**
     * gets credit_payment_id, This is the payment_id for the payment against 
     * which the charge back / retrieval is created.
     * @return String credit_payment_id
     */
     public String getCreditPaymentID()
     {
        return credit_payment_id;
     }
     
    /**
     * sets credit_payment_id
     * @param newCreditPaymentID String
     */
     public void setCreditPaymentID(String newCreditPaymentID)
     {
        credit_payment_id = trim(newCreditPaymentID);
     }

    /**
     * gets comment, This is the comment to be added to the cbr.
     * @return String comment
     */
     public String getComment()
     {
        return comment;
     }
     
    /**
     * sets comment
     * @param newComment String
     */
     public void setComment(String newComment)
     {
        comment = trim(newComment);
     }

    public static CbrVO toObject(HttpServletRequest request)
        throws Exception
    {
        CbrVO cbr = new CbrVO();
        AccountingUtil acctgUtil = new AccountingUtil();

        if(request.getParameter("amount")!=null)
        {
          if(!request.getParameter("amount").toString().equalsIgnoreCase(""))
          {
            cbr.setAmount( 
              Double.valueOf(request.getParameter("amount")).doubleValue());
          }
        }
    
        if(request.getParameter("cbr_id")!=null)
          if(!request.getParameter("cbr_id").equalsIgnoreCase(""))
            cbr.setCBRId((String)request.getParameter("cbr_id"));
            
        if(request.getParameter("comments")!=null)
          if(!request.getParameter("comments").equalsIgnoreCase(""))
            cbr.setComment((String)request.getParameter("comments"));
            
        if(request.getParameter("payment_id")!=null)
          if(!request.getParameter("payment_id").equalsIgnoreCase(""))
            cbr.setCreditPaymentID((String)request.getParameter("payment_id"));
        
        String dueDate	= "";
        Calendar calDueDate = null;
          
        if(request.getParameter("date_due")!=null)
          if(!request.getParameter("date_due").equalsIgnoreCase(""))
          {
            dueDate = request.getParameter("date_due");
            if (dueDate!= null)
            {
              calDueDate = acctgUtil.formatDateStringToCal(dueDate);
              
            }
          }
        if(calDueDate!=null){
            cbr.setDueDate(calDueDate.getTime());
        }
        
        if(request.getParameter("master_order_number")!=null)
          if(!request.getParameter("master_order_number").equalsIgnoreCase(""))
            cbr.setMasterOrderNumber((String)request.getParameter("cart_number"));
            
        if(request.getParameter("reason")!=null)
          if(!request.getParameter("reason").equalsIgnoreCase(""))
            cbr.setReasonCode((String)request.getParameter("reason"));
        
        String receivedDate	= "";
        Calendar calReceivedDate = null;
        if(request.getParameter("date_rcvd")!=null)
          if(!request.getParameter("date_rcvd").equalsIgnoreCase(""))
          {
            receivedDate = request.getParameter("date_rcvd");
            if (receivedDate!= null)
            {
              calReceivedDate = acctgUtil.formatDateStringToCal(receivedDate);
            }
          }
        
        if(calReceivedDate!=null){  
            cbr.setReceivedDate(calReceivedDate.getTime());
        }
        
        String returnedDate	= "";
        Calendar calReturnedDate = null;
        if(request.getParameter("date_rtrn")!=null)
          if(!request.getParameter("date_rtrn").equalsIgnoreCase(""))
          {
            returnedDate = request.getParameter("date_rtrn");
            if (returnedDate!= null)
            {
              calReturnedDate = acctgUtil.formatDateStringToCal(returnedDate); 
            }
          }
          
        if(calReturnedDate!=null){
            cbr.setReturnedDate(calReceivedDate.getTime());
        }
        
        String reversalDate	= "";
        Calendar calReversalDate  = null;
        if(request.getParameter("date_rvsl")!=null)
          if(!request.getParameter("date_rvsl").equalsIgnoreCase(""))
          {
            reversalDate = request.getParameter("date_rvsl");
            if (reversalDate!= null)
            {
              calReversalDate = acctgUtil.formatDateStringToCal(reversalDate);
            }
          }
          
          if(calReversalDate!=null){
            cbr.setReversalDate(calReversalDate.getTime());
          }
          
        if(request.getParameter("type_code")!=null)
          if(!request.getParameter("type_code").equalsIgnoreCase(""))
            cbr.setTypeCode((String)request.getParameter("type_code"));
        
        return cbr;
    }

    public String toXML()
        throws IllegalAccessException, ClassNotFoundException
    {
        StringBuffer sb = new StringBuffer();
        Document cbrDoc = null;
        try
        {
            sb.append("<chargeback_retrieval>");
            Field[] fields = this.getClass().getDeclaredFields();
            
            appendFields(sb, fields);
            sb.append("</chargeback_retrieval>");
            

        } finally {

        }
            
        return sb.toString();
    }

    protected void appendFields(StringBuffer sb, Field[] fields) 
        throws IllegalAccessException, ClassNotFoundException
    {

        try
        {
            for (int i = 0; i < fields.length; i++)
            {
              //if the field retrieved was a list of VO
              if(fields[i].getType().equals(Class.forName("java.util.List")))
              {
                List list = (List)fields[i].get(this);
                if(list != null)
                {
                  for (int j = 0; j < list.size(); j++)
                  {
                    XMLInterface xmlInt = (XMLInterface)list.get(j);
                    String sXmlVO = xmlInt.toXML();
                    sb.append(sXmlVO);
                  }
                }
              }
              else
              {
                //if the field retrieved was a VO
                if (fields[i].getType().toString().matches("(?i).*vo"))
                {
                  XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
                  String sXmlVO = xmlInt.toXML();
                  sb.append(sXmlVO);
                }
                //if the field retrieved was a Calendar object
                else if (fields[i].getType().toString().matches("(?i).*calendar"))
                {
                  Date date;
                  String fDate = null;
                  if (fields[i].get(this) != null)
                  {
                    date = (((GregorianCalendar)fields[i].get(this)).getTime());
                    SimpleDateFormat sdf = new SimpleDateFormat ("MM/dd/yyyy");
                    fDate = sdf.format(date).toString();
                  }
                  if(fDate!=null){
                      sb.append("<" + fields[i].getName() + ">");
                      sb.append(fDate);
                      sb.append("</" + fields[i].getName() + ">");
                  }
                  else
                  {
                        sb.append("<" + fields[i].getName() + "/>");
                  }
                }
                else if (fields[i].getType().toString().matches("(?i).*date"))
                {
                  Date date;
                  String fDate = null;
                  if (fields[i].get(this) != null)
                  {
                    date = (((Date)fields[i].get(this)));
                    SimpleDateFormat sdf = new SimpleDateFormat ("MM/dd/yyyy");
                    fDate = sdf.format(date).toString();
                  }
                  if(fDate!=null){
                      sb.append("<" + fields[i].getName() + ">");
                      sb.append(fDate);
                      sb.append("</" + fields[i].getName() + ">");
                  }
                  else
                  {
                        sb.append("<" + fields[i].getName() + "/>");
                  }
                }
                else
                {
                  
                  sb.append("<" + fields[i].getName() + ">");
                  String value = "";
        
                  if(fields[i].get(this)!=null){
                      value = fields[i].get(this).toString();    
                      value = DOMUtil.encodeChars(value);
                  }
                  else
                  {
                      value = "";    
                  }
                      
        
                  sb.append(value);
                  sb.append("</" + fields[i].getName() + ">");
                }
              }
            }
        } finally {

        }
    }
    
    private String trim(String str)
    {
        return (str != null)?str.trim():str;
    }
}