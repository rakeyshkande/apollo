package com.ftd.accountingreporting.vo;

/**
 * This is the value object that models each line item on the bill file. 
 * @author Charles Fox
 */
public class AmazonRemittanceExceptionVO 
{
    public AmazonRemittanceExceptionVO()
    {
    }
    
    private String orderDetailId = "";
    private Double retailAmount;
    private Double wholesaleAmount;
    private String amazonOrderId = "";
    private String ftdAdjustmentId = "";
    private String settlementDate = "";
    private String settlementReference = "";
    private Double remittanceDiff;
     
    /**
     * gets orderDetailId
     * This is the order detail id.
     * @return String orderDetailId
     */
     public String getOrderDetailId()
     {
        return orderDetailId;
     }
     
    /**
     * sets orderDetailId
     * @param newOrderDetailId  String
     */
    public void setOrderDetailId(String newOrderDetailId)
     {
        orderDetailId = trim(newOrderDetailId);
     }

    /**
     * gets retailAmount
     * This is the retail amount
     * @return Double retailAmount
     */
     public Double getRetailAmount()
     {
        return retailAmount;
     }
     
    /**
     * sets retailAmount
     * @param newRetailAmount  Double
     */
    public void setRetailAmount(Double newRetailAmount)
     {
        retailAmount = newRetailAmount;
     }

    /**
     * gets wholesaleAmount
     * This is the wholesale amount.
     * @return Double wholesaleAmount
     */
     public Double getWholesaleAmount()
     {
        return wholesaleAmount;
     }
     
    /**
     * sets wholesaleAmount
     * @param newWholesaleAmount Double
     */
    public void setWholesaleAmount(Double newWholesaleAmount)
     {
        wholesaleAmount = newWholesaleAmount;
     }

    /**
     * gets amazonOrderId
     * This is the amazon order number
     * @return String amazonOrderId
     */
     public String getAmazonOrderId()
     {
        return amazonOrderId;
     }
     
    /**
     * sets amazonOrderId
     * @param newAmazonOrderId  String
     */
    public void setAmazonOrderId(String newAmazonOrderId)
     {
        amazonOrderId = trim(newAmazonOrderId);
     }

    /**
     * gets ftdAdjustmentId
     * This is the confirmation number.
     * @return String ftdAdjustmentId
     */
     public String getFTDAdjustmentId()
     {
        return ftdAdjustmentId;
     }
     
    /**
     * sets ftdAdjustmentId
     * @param newFTDAdjustmentId  String
     */
    public void setFTDAdjustmentId(String newFTDAdjustmentId)
     {
        ftdAdjustmentId = trim(newFTDAdjustmentId);
     }

    /**
     * gets settlementDate
     * This is the settlement date.
     * @return String settlementDate
     */
     public String getSettlementDate()
     {
        return settlementDate;
     }
     
    /**
     * sets settlementDate
     * @param newSettlementDate  String
     */
    public void setSettlementDate(String newSettlementDate)
     {
        settlementDate = trim(newSettlementDate);
     }

    /**
     * gets settlementReference
     * This is the settlement data id.
     * @return String settlementReference
     */
     public String getSettlementReference()
     {
        return settlementReference;
     }
     
    /**
     * sets settlementReference
     * @param newSettlementReference  String
     */
    public void setSettlementReference(String newSettlementReference)
     {
        settlementReference = trim(newSettlementReference);
     }

    /**
     * gets remittanceDiff
     * This is the amount difference between remitted total and order/refund total.
     * @return Double remittanceDiff
     */
     public Double getRemittanceDiff()
     {
        return remittanceDiff;
     }
     
    /**
     * sets remittanceDiff
     * @param newRemittanceDiff Double
     */
    public void setRemittanceDiff(Double newRemittanceDiff)
     {
        remittanceDiff = newRemittanceDiff;
     }  
     
    private String trim(String str)
    {
        return (str != null)?str.trim():str;
    } 
}