package com.ftd.accountingreporting.vo;

import java.math.BigDecimal;

public class BillingHeaderPGVO {

	  private String processedIndicator;
	  private BigDecimal batchNumber;

	  public BillingHeaderPGVO(String processIndicator, BigDecimal batchNumber)
	  {
	    this.processedIndicator = processIndicator;
	    this.batchNumber = batchNumber;
	  }
	  
	  public String getProcessedIndicator() 
	  {
	    return this.processedIndicator;
	  }
	  
	  public BigDecimal getBatchNumber() 
	  {
	    return this.batchNumber;
	  }
	
}
