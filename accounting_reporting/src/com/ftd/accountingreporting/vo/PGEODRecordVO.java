package com.ftd.accountingreporting.vo;


import java.util.Calendar;

public class PGEODRecordVO {

	private long billingHeaderId;
	private long billingDetailId;
	private long paymentId;
	private double orderAmount;
	private Calendar authorizationDate;
	private String approvalCode = "";
	private String authCode ="";
	//private String orderNumber = "";
	private String transactionType = "";
	private String billType = "";
	private String paymentType = "";
	private String orderOrigin = "";
	// private String orderGuid="";
	private String masterOrderNumber = "";
	private String authTranId = "";
	private Calendar orderDate;
	private String companyId = "";
	private String voiceAuthFlag;
	private String cardinalVerifiedFlag;
	private String settlmentTransactionId;
	private String refundTransactionId;
	private long refundId;
	private String processAccount;
	private String merchantRefId;
	private String requestToken;
	private String status;
	
	
	public PGEODRecordVO() {

	}

	public long getBillingHeaderId() {
		return billingHeaderId;
	}

	public void setBillingHeaderId(long billingHeaderId) {
		this.billingHeaderId = billingHeaderId;
	}

	public long getBillingDetailId() {
		return billingDetailId;
	}

	public void setBillingDetailId(long billingDetailId) {
		this.billingDetailId = billingDetailId;
	}

	public long getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(long paymentId) {
		this.paymentId = paymentId;
	}

	public double getOrderAmount() {
		return orderAmount;
	}

	public void setOrderAmount(double orderAmount) {
		this.orderAmount = orderAmount;
	}

	public Calendar getAuthorizationDate() {
		return authorizationDate;
	}

	public void setAuthorizationDate(Calendar authorizationDate) {
		this.authorizationDate = authorizationDate;
	}

	public String getApprovalCode() {
		return approvalCode;
	}

	public void setApprovalCode(String approvalCode) {
		this.approvalCode = approvalCode;
	}

	
	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getBillType() {
		return billType;
	}

	public void setBillType(String billType) {
		this.billType = billType;
	}

	

	public String getOrderOrigin() {
		return orderOrigin;
	}

	public void setOrderOrigin(String orderOrigin) {
		this.orderOrigin = orderOrigin;
	}

	public String getMasterOrderNumber() {
		return masterOrderNumber;
	}

	public void setMasterOrderNumber(String masterOrderNumber) {
		this.masterOrderNumber = masterOrderNumber;
	}

	public String getAuthTranId() {
		return authTranId;
	}

	public void setAuthTranId(String authTranId) {
		this.authTranId = authTranId;
	}

	public Calendar getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Calendar orderDate) {
		this.orderDate = orderDate;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getVoiceAuthFlag() {
		return voiceAuthFlag;
	}

	public void setVoiceAuthFlag(String voiceAuthFlag) {
		this.voiceAuthFlag = voiceAuthFlag;
	}

	public String getCardinalVerifiedFlag() {
		return cardinalVerifiedFlag;
	}

	public void setCardinalVerifiedFlag(String cardinalVerifiedFlag) {
		this.cardinalVerifiedFlag = cardinalVerifiedFlag;
	}

	public String getSettlmentTransactionId() {
		return settlmentTransactionId;
	}

	public void setSettlmentTransactionId(String settlmentTransactionId) {
		this.settlmentTransactionId = settlmentTransactionId;
	}

	public String getRefundTransactionId() {
		return refundTransactionId;
	}

	public void setRefundTransactionId(String refundTransactionId) {
		this.refundTransactionId = refundTransactionId;
	}

	public String getAuthCode() {
		return authCode;
	}

	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public long getRefundId() {
		return refundId;
	}

	public void setRefundId(long refundId) {
		this.refundId = refundId;
	}

	public String getProcessAccount() {
		return processAccount;
	}

	public void setProcessAccount(String processAccount) {
		this.processAccount = processAccount;
	}

	public String getMerchantRefId() {
		return merchantRefId;
	}

	public void setMerchantRefId(String merchantRefId) {
		this.merchantRefId = merchantRefId;
	}

	public String getRequestToken() {
		return requestToken;
	}

	public void setRequestToken(String requestToken) {
		this.requestToken = requestToken;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}
