package com.ftd.accountingreporting.vo;

import java.util.Calendar;

/**
 * This class represents the value object for table GC_COUPON_REQUEST and GC_COUPONS
 * 
 * @author Madhusudan Parab
 */
public class GiftCertificateCouponVO 
{
  private String requestNumber;
  private String gcCouponType;
  private double issueAmount;
  private double paidAmount;
  private Calendar issueDate;
  private Calendar expirationDate;
  private long quantity;
  private String format;
  private String fileName;
  private String requestedBy;
  private String orderSource;
  private String programName;
  private String programDesc;
  private String programComment;
  private String companyId;
  
  private String prefix;
  private String other;
  private Calendar createdOn;
  private String createdBy;
  private Calendar updatedOn;
  private String updatedBy;
  private String gcCouponNumber;
  private long gcCouponRecipId;
  private String gcCouponStatus;
  private Calendar gcRedemptionDate;
  private Calendar gcReinstateDate;
  private long gcOrderDetailId;
  private String gcOrderNumber;
  private String couponHistoryId;
  private String couponHistoryStatus;
  private String promotionID;
  private String gcPartnerProgramName;
  
  private GiftCertificateSourceCodeVO sourceCodeVO;
    /**
      * Constructor for class GiftCertificateCouponVO
      */
    public GiftCertificateCouponVO()
    {
    }

    /**
      * sets requestNumber
      * @param String newRequestNumber
      */
    public void setRequestNumber(String newRequestNumber)
    {
        requestNumber = newRequestNumber;
    }
    
   /**
     * gets requestNumber
     * @return String requestNumber
     */
    public String getRequestNumber()
    {
        return requestNumber;
    }
    
    /**
      * sets gcCouponType
      * @param String newGcCouponType
      */
    public void setGcCouponType(String newGcCouponType)
    {
        gcCouponType = newGcCouponType;
    }
    
   /**
     * gets gcCouponType
     * @return String gcCouponType
     */
    public String getGcCouponType()
    {
        return gcCouponType;
    }
    
    /**
      * sets issueAmount
      * @param double newIssueAmount
      */
    public void setIssueAmount(double newIssueAmount)
    {
        issueAmount = newIssueAmount;
    }
    
   /**
     * gets issueAmount
     * @return String issueAmount
     */
    public double getIssueAmount()
    {
        return issueAmount;
    }
    
    /**
      * sets paidAmount
      * @param double newPaidAmount
      */
    public void setPaidAmount(double newPaidAmount)
    {
        paidAmount = newPaidAmount;
    }
    
   /**
     * gets paidAmount
     * @return double paidAmount
     */
    public double getPaidAmount()
    {
        return paidAmount;
    }
    
    /**
      * sets issueDate
      * @param Calendar newIssueDate
      */
    public void setIssueDate(Calendar newIssueDate)
    {
        issueDate = newIssueDate;
    }
    
   /**
     * gets issueDate
     * @return Calendar issueDate
     */
    public Calendar getIssueDate()
    {
        return issueDate;
    }
    
    /**
      * sets expirationDate
      * @param Calendar newExpirationDate
      */
    public void setExpirationDate(Calendar newExpirationDate)
    {
        expirationDate = newExpirationDate;
    }
    
   /**
     * gets expirationDate
     * @return Calendar expirationDate
     */
    public Calendar getExpirationDate()
    {
        return expirationDate;
    }
    
    /**
      * sets quantity
      * @param long newQuantity
      */
    public void setQuantity(long newQuantity)
    {
        quantity = newQuantity;
    }
    
   /**
     * gets quantity
     * @return long quantity
     */
    public long getQuantity()
    {
        return quantity;
    }
    
    /**
      * sets format
      * @param String newFormat
      */
    public void setFormat(String newFormat)
    {
        format = newFormat;
    }
    
   /**
     * gets format
     * @return String format
     */
    public String getFormat()
    {
        return format;
    }
    
    /**
      * sets fileName
      * @param String newFileName
      */
    public void setFileName(String newFileName)
    {
        fileName = newFileName;
    }
    
   /**
     * gets fileName
     * @return String fileName
     */
    public String getFileName()
    {
        return fileName;
    }
    
    /**
      * sets requestedBy
      * @param String newRequestedBy
      */
    public void setRequestedBy(String newRequestedBy)
    {
        requestedBy = newRequestedBy;
    }
    
   /**
     * gets requestedBy
     * @return String requestedBy
     */
    public String getRequestedBy()
    {
        return requestedBy;
    }
    
    /**
      * sets orderSource
      * @param String newOrderSource
      */
    public void setOrderSource(String newOrderSource)
    {
        orderSource = newOrderSource;
    }
    
   /**
     * gets orderSource
     * @return String orderSource
     */
    public String getOrderSource()
    {
        return orderSource;
    }
    
    /**
      * sets programName
      * @param String newProgramName
      */
    public void setProgramName(String newProgramName)
    {
        programName = newProgramName;
    }
    
   /**
     * gets programName
     * @return String programName
     */
    public String getProgramName()
    {
        return programName;
    }
    
    /**
      * sets programDesc
      * @param String newProgramDesc
      */
    public void setProgramDesc(String newProgramDesc)
    {
        programDesc = newProgramDesc;
    }
    
   /**
     * gets programDesc
     * @return String programDesc
     */
    public String getProgramDesc()
    {
        return programDesc;
    }
    
    /**
      * sets programComment
      * @param String newProgramComment
      */
    public void setProgramComment(String newProgramComment)
    {
        programComment = newProgramComment;
    }
    
   /**
     * gets programComment
     * @return String programComment
     */
    public String getProgramComment()
    {
        return programComment;
    }
    
    /**
      * sets companyId
      * @param String newCompanyId
      */
    public void setCompanyId(String newCompanyId)
    {
        companyId = newCompanyId;
    }
    
   /**
     * gets companyId
     * @return String companyId
     */
    public String getCompanyId()
    {
        return companyId;
    }
        
    /**
      * sets prefix
      * @param String newPrefix
      */
    public void setPrefix(String newPrefix)
    {
        prefix = newPrefix;
    }
    
   /**
     * gets prefix
     * @return String prefix
     */
    public String getPrefix()
    {
        return prefix;
    }
    
    /**
      * sets other
      * @param String newOther
      */
    public void setOther(String newOther)
    {
        other = newOther;
    }
    
   /**
     * gets other
     * @return String other
     */
    public String getOther()
    {
        return other;
    }
  
    /**
      * sets createdOn
      * @param Calendar newCreatedOn
      */
    public void setCreatedOn(Calendar newCreatedOn)
    {
        createdOn = newCreatedOn;
    }
    
   /**
     * gets createdOn
     * @return Calendar createdOn
     */
    public Calendar getCreatedOn()
    {
        return createdOn;
    }
    
    /**
      * sets createdBy
      * @param String newCreatedBy
      */
    public void setCreatedBy(String newCreatedBy)
    {
        createdBy = newCreatedBy;
    }
    
   /**
     * gets createdBy
     * @return String createdBy
     */
    public String getCreatedBy()
    {
        return createdBy;
    }
    
    /**
      * sets updatedOn
      * @param Calendar newUpdatedOn
      */
    public void setUpdatedOn(Calendar newUpdatedOn)
    {
        updatedOn = newUpdatedOn;
    }
    
   /**
     * gets updatedOn
     * @return Calendar updatedOn
     */
    public Calendar getUpdatedOn()
    {
        return updatedOn;
    }
    
    /**
      * sets updatedBy
      * @param String newUpdatedBy
      */
    public void setUpdatedBy(String newUpdatedBy)
    {
        updatedBy = newUpdatedBy;
    }
    
   /**
     * gets updatedBy
     * @return String updatedBy
     */
    public String getUpdatedBy()
    {
        return updatedBy;
    }
    
    /**
      * sets gcCouponNumber
      * @param String newGcCouponNumber
      */
    public void setGcCouponNumber(String newGcCouponNumber)
    {
        gcCouponNumber = newGcCouponNumber;
    }
    
   /**
     * gets gcCouponNumber
     * @return String gcCouponNumber
     */
    public String getGcCouponNumber()
    {
        return gcCouponNumber;
    }
    
    /**
      * sets gcCouponRecipId
      * @param long newGcCouponRecipId
      */
    public void setGcCouponRecipId(long newGcCouponRecipId)
    {
        gcCouponRecipId = newGcCouponRecipId;
    }
    
   /**
     * gets gcCouponRecipId
     * @return long gcCouponRecipId
     */
    public long getGcCouponRecipId()
    {
        return gcCouponRecipId;
    }
    
    /**
      * sets gcCouponStatus
      * @param String newGcCouponStatus
      */
    public void setGcCouponStatus(String newGcCouponStatus)
    {
        gcCouponStatus = newGcCouponStatus;
    }
    
   /**
     * gets gcCouponStatus
     * @return String gcCouponStatus
     */
    public String getGcCouponStatus()
    {
        return gcCouponStatus;
    }
    
    
    /**
      * sets gcRedemptionDate
      * @param Calendar newGcRedemptionDate
      */
    public void setGcRedemptionDate(Calendar newGcRedemptionDate)
    {
        gcRedemptionDate = newGcRedemptionDate;
    }
    
   /**
     * gets gcRedemptionDate
     * @return Calendar gcRedemptionDate
     */
    public Calendar getGcRedemptionDate()
    {
        return gcRedemptionDate;
    }
    
    /**
      * sets gcReinstateDate
      * @param Calendar newGcReinstateDate
      */
    public void setGcReinstateDate(Calendar newGcReinstateDate)
    {
        gcReinstateDate = newGcReinstateDate;
    }
    
   /**
     * gets gcReinstateDate
     * @return Calendar gcReinstateDate
     */
    public Calendar getGcReinstateDate()
    {
        return gcReinstateDate;
    }
    
    /**
      * sets gcOrderDetailId
      * @param long newGcOrderDetailId
      */
    public void setGcOrderDetailId(long newGcOrderDetailId)
    {
        gcOrderDetailId = newGcOrderDetailId;
    }
    
   /**
     * gets gcOrderDetailId
     * @return long gcOrderDetailId
     */
    public long getGcOrderDetailId()
    {
        return gcOrderDetailId;
    }
    
    /**
      * sets gcOrderNumber
      * @param String newGcOrderNumber
      */
    public void setGcOrderNumber(String newGcOrderNumber)
    {
        gcOrderNumber = newGcOrderNumber;
    }
    
   /**
     * gets gcOrderNumber
     * @return String gcOrderNumber
     */
    public String getGcOrderNumber()
    {
        return gcOrderNumber;
    }
    
   /**
    * sets couponHistoryId
    * @param String newCouponHistoryId
    */
    public void setCouponHistoryId(String newCouponHistoryId)
    {
        couponHistoryId = newCouponHistoryId;
    }
    
   /**
    * gets couponHistoryId
    * @return String couponHistoryId
    */
    public String getCouponHistoryId()
    {
        return couponHistoryId;
    }
    
   /**
    * set couponHistoryStatus
    * @param String newCouponHistoryStatus
    */
    public void setCouponHistoryStatus(String newCouponHistoryStatus)
    {
        couponHistoryStatus = newCouponHistoryStatus;
    }
    
   /**
    * gets couponHistoryStatus
    * @return String couponHistoryStatus
    */
    public String getCouponHistoryStatus()
    {
        return couponHistoryStatus;
    }
    
    /**
      * sets promotionID
      * @param String newPromotionID
      */
    public void setPromotionID(String newPromotionID)
    {
        promotionID = newPromotionID;
    }
    
   /**
     * gets promotionID
     * @return String promotionID
     */
    public String getPromotionID()
    {
        return promotionID;
    }


  public void setGcPartnerProgramName(String gcPartnerProgramName)
  {
    this.gcPartnerProgramName = gcPartnerProgramName;
  }


  public String getGcPartnerProgramName()
  {
    return gcPartnerProgramName;
  }

public GiftCertificateSourceCodeVO getSourceCodeVO() {
	return sourceCodeVO;
}

public void setSourceCodeVO(GiftCertificateSourceCodeVO sourceCodeVO) {
	this.sourceCodeVO = sourceCodeVO;
}
  
  
}