package com.ftd.accountingreporting.vo;

import java.math.BigDecimal;

public class CbrOrderBillVO 
{
    public CbrOrderBillVO()
    {
    }
    
    private String orderDetailId = "";
    private double productAmount;
    private double addonAmount;
    private double discountAmount;
    private double serviceFee;
    private double shippingFee;
    private double shippingTax;
    private double tax;
    private double serviceFeeTax;
    private double adminFee;
    private double commissionAmount;
    private double wholesaleAmount;
    private int milesPointsAmount;
    private String      tax1Name;
    private String      tax1Description;
    private BigDecimal  tax1Rate;
    private BigDecimal  tax1Amount;
    private String      tax2Name;
    private String      tax2Description;
    private BigDecimal  tax2Rate;
    private BigDecimal  tax2Amount;
    private String      tax3Name;
    private String      tax3Description;
    private BigDecimal  tax3Rate;
    private BigDecimal  tax3Amount;
    private String      tax4Name;
    private String      tax4Description;
    private BigDecimal  tax4Rate;
    private BigDecimal  tax4Amount;
    private String      tax5Name;
    private String      tax5Description;
    private BigDecimal  tax5Rate;
    private BigDecimal  tax5Amount;

    
    /**
     * gets orderDetailId, This is the order detail id.
     * @return String orderDetailId
     */
     public String getOrderDetailId()
     {
        return orderDetailId;
     }
     
    /**
     * sets orderDetailId
     * @param newOrderDetailId  String
     */
     public void setOrderDetailId(String newOrderDetailId)
     {
        orderDetailId = trim(newOrderDetailId);
     }

    /**
     * gets addonAmount, This is the addon amount.
     * @return double addonAmount
     */
     public double getAddonAmount()
     {
        return addonAmount;
     }
     
    /**
     * sets addonAmount
     * @param newAddonAmount  double
     */
     public void setAddonAmount(double newAddonAmount)
     {
        addonAmount = newAddonAmount;
     }       


    /**
     * gets tax, This is the the tax amount.
     * @return double tax
     */
     public double getTax()
     {
        return tax;
     }
     
    /**
     * sets tax
     * @param newTax  double
     */
     public void setTax(double newTax)
     {
        tax = newTax;
     }
     
    private String trim(String str)
    {
        return (str != null)?str.trim():str;
    }


  public void setServiceFee(double serviceFee)
  {
    this.serviceFee = serviceFee;
  }


  public double getServiceFee()
  {
    return serviceFee;
  }


  public void setShippingFee(double shippingFee)
  {
    this.shippingFee = shippingFee;
  }


  public double getShippingFee()
  {
    return shippingFee;
  }

  public void setServiceFeeTax(double serviceFeeTax)
  {
    this.serviceFeeTax = serviceFeeTax;
  }


  public double getServiceFeeTax()
  {
    return serviceFeeTax;
  }


  public void setShippingTax(double shippingTax)
  {
    this.shippingTax = shippingTax;
  }


  public double getShippingTax()
  {
    return shippingTax;
  }

  public void setProductAmount(double productAmount)
  {
    this.productAmount = productAmount;
  }


  public double getProductAmount()
  {
    return productAmount;
  }


  public void setDiscountAmount(double discountAmount)
  {
    this.discountAmount = discountAmount;
  }


  public double getDiscountAmount()
  {
    return discountAmount;
  }


  public void setAdminFee(double adminFee)
  {
    this.adminFee = adminFee;
  }


  public double getAdminFee()
  {
    return adminFee;
  }


  public void setCommissionAmount(double commissionAmount)
  {
    this.commissionAmount = commissionAmount;
  }


  public double getCommissionAmount()
  {
    return commissionAmount;
  }


  public void setWholesaleAmount(double wholesaleAmount)
  {
    this.wholesaleAmount = wholesaleAmount;
  }


  public double getWholesaleAmount()
  {
    return wholesaleAmount;
  }

  public void setMilesPointsAmount(int milesPointsAmount)
  {
    this.milesPointsAmount = milesPointsAmount;
  }

  public int getMilesPointsAmount()
  {
    return milesPointsAmount;
  }

public String getTax1Name() {
	return tax1Name;
}

public void setTax1Name(String tax1Name) {
	this.tax1Name = tax1Name;
}

public String getTax1Description() {
	return tax1Description;
}

public void setTax1Description(String tax1Description) {
	this.tax1Description = tax1Description;
}

public BigDecimal getTax1Rate() {
	return tax1Rate;
}

public void setTax1Rate(BigDecimal tax1Rate) {
	this.tax1Rate = tax1Rate;
}

public BigDecimal getTax1Amount() {
	return tax1Amount;
}

public void setTax1Amount(BigDecimal tax1Amount) {
	this.tax1Amount = tax1Amount;
}

public String getTax2Name() {
	return tax2Name;
}

public void setTax2Name(String tax2Name) {
	this.tax2Name = tax2Name;
}

public String getTax2Description() {
	return tax2Description;
}

public void setTax2Description(String tax2Description) {
	this.tax2Description = tax2Description;
}

public BigDecimal getTax2Rate() {
	return tax2Rate;
}

public void setTax2Rate(BigDecimal tax2Rate) {
	this.tax2Rate = tax2Rate;
}

public BigDecimal getTax2Amount() {
	return tax2Amount;
}

public void setTax2Amount(BigDecimal tax2Amount) {
	this.tax2Amount = tax2Amount;
}

public String getTax3Name() {
	return tax3Name;
}

public void setTax3Name(String tax3Name) {
	this.tax3Name = tax3Name;
}

public String getTax3Description() {
	return tax3Description;
}

public void setTax3Description(String tax3Description) {
	this.tax3Description = tax3Description;
}

public BigDecimal getTax3Rate() {
	return tax3Rate;
}

public void setTax3Rate(BigDecimal tax3Rate) {
	this.tax3Rate = tax3Rate;
}

public BigDecimal getTax3Amount() {
	return tax3Amount;
}

public void setTax3Amount(BigDecimal tax3Amount) {
	this.tax3Amount = tax3Amount;
}

public String getTax4Name() {
	return tax4Name;
}

public void setTax4Name(String tax4Name) {
	this.tax4Name = tax4Name;
}

public String getTax4Description() {
	return tax4Description;
}

public void setTax4Description(String tax4Description) {
	this.tax4Description = tax4Description;
}

public BigDecimal getTax4Rate() {
	return tax4Rate;
}

public void setTax4Rate(BigDecimal tax4Rate) {
	this.tax4Rate = tax4Rate;
}

public BigDecimal getTax4Amount() {
	return tax4Amount;
}

public void setTax4Amount(BigDecimal tax4Amount) {
	this.tax4Amount = tax4Amount;
}

public String getTax5Name() {
	return tax5Name;
}

public void setTax5Name(String tax5Name) {
	this.tax5Name = tax5Name;
}

public String getTax5Description() {
	return tax5Description;
}

public void setTax5Description(String tax5Description) {
	this.tax5Description = tax5Description;
}

public BigDecimal getTax5Rate() {
	return tax5Rate;
}

public void setTax5Rate(BigDecimal tax5Rate) {
	this.tax5Rate = tax5Rate;
}

public BigDecimal getTax5Amount() {
	return tax5Amount;
}

public void setTax5Amount(BigDecimal tax5Amount) {
	this.tax5Amount = tax5Amount;
}
}
