package com.ftd.accountingreporting.vo;

import java.math.BigDecimal;

public class CbrRefundVO 
{
    public CbrRefundVO()
    {
    }

    private String refundDispCode = "";
    private String createdBy = "";
    private double refundProductAmount;
    private double refundAddonAmount;
    private double refundServiceFee;
    private double refundShippingFee;
    private double refundTax;
    private double refundAdminFee;
    private double refundServiceFeeTax;
    private double refundShippingTax;
    private double refundDiscountAmount;
    private double refundCommissionAmount;
    private double refundWholesaleAmount;
    private double refundWholesaleServiceFee;
    private String acctTransInd;
    private String refundStatus;
    private String orderDetailId = "";
    private String responsibleParty = "";
    private CbrPaymentVO cbrPayment;
    private String      tax1Name;
    private String      tax1Description;
    private BigDecimal  tax1Rate;
    private BigDecimal  tax1Amount;
    private String      tax2Name;
    private String      tax2Description;
    private BigDecimal  tax2Rate;
    private BigDecimal  tax2Amount;
    private String      tax3Name;
    private String      tax3Description;
    private BigDecimal  tax3Rate;
    private BigDecimal  tax3Amount;
    private String      tax4Name;
    private String      tax4Description;
    private BigDecimal  tax4Rate;
    private BigDecimal  tax4Amount;
    private String      tax5Name;
    private String      tax5Description;
    private BigDecimal  tax5Rate;
    private BigDecimal  tax5Amount;

    
    /**
     * gets refundDispCode, This is the refund disposition. E10 in this app.
     * @return String refundDispCode
     */
     public String getRefundDispCode()
     {
        return refundDispCode;
     }
     
    /**
     * sets refundDispCode
     * @param newRefundDispCode  String
     */
     public void setRefundDispCode(String newRefundDispCode)
     {
        refundDispCode = trim(newRefundDispCode);
     }

    /**
     * gets createdBy, This is the order bill id.
     * @return String createdBy
     */
     public String getCreatedBy()
     {
        return createdBy;
     }
     
    /**
     * sets createdBy
     * @param newCreatedBy  String
     */
     public void setCreatedBy(String newCreatedBy)
     {
        createdBy = trim(newCreatedBy);
     }
  
    /**
     * gets refundProductAmount, This is the refund product amount
     * @return double refundProductAmount
     */
     public double getRefundProductAmount()
     {
        return refundProductAmount;
     }
     
    /**
     * sets refundProductAmount
     * @param newRefundProductAmount  double
     */
     public void setRefundProductAmount(double newRefundProductAmount)
     {
        refundProductAmount = newRefundProductAmount;
     }   

    /**
     * gets refundAddonAmount, This is the refund addon amount.
     * @return double refundAddonAmount
     */
     public double getRefundAddonAmount()
     {
        return refundAddonAmount;
     }
     
    /**
     * sets refundAddonAmount
     * @param newRefundAddonAmount  double
     */
     public void setRefundAddonAmount(double newRefundAddonAmount)
     {
        refundAddonAmount = newRefundAddonAmount;
     }       

    /**
     * gets refundServiceFee, This is the refund service fee.
     * @return double refundServShipFee
     */
     public double getRefundServiceFee()
     {
        return refundServiceFee;
     }
     
    /**
     * sets refundServiceFee
     * @param newRefundServiceFee  double
     */
     public void setRefundServiceFee(double newRefundServiceFee)
     {
        refundServiceFee = newRefundServiceFee;
     }

    /**
     * gets refundShippingFee, This is the refund shipping fee.
     * @return double refundServShipFee
     */
     public double getRefundShippingFee()
     {
        return refundShippingFee;
     }
     
    /**
     * sets refundShippingFee
     * @param newRefundShippingFee  double
     */
     public void setRefundShippingFee(double newRefundShippingFee)
     {
        refundShippingFee = newRefundShippingFee;
     }

    /**
     * gets refundTax, This is the refund tax.
     * @return double refundTax
     */
     public double getRefundTax()
     {
        return refundTax;
     }
     
    /**
     * sets refundTax
     * @param newRefundTax  double
     */
     public void setRefundTax(double newRefundTax)
     {
        refundTax = newRefundTax;
     }
     
    /**
     * gets orderDetailId, This is the order detail id.
     * @return String orderDetailId
     */
     public String getOrderDetailId()
     {
        return orderDetailId;
     }
     
    /**
     * sets orderDetailId
     * @param newOrderDetailId  String
     */
     public void setOrderDetailId(String newOrderDetailId)
     {
        orderDetailId = trim(newOrderDetailId);
     }  

    /**
     * gets responsibleParty, This is the responsible party. �FTD� in this app.
     * @return String responsibleParty
     */
     public String getResponsibleParty()
     {
        return responsibleParty;
     }
     
    /**
     * sets responsibleParty
     * @param newResponsibleParty  String
     */
     public void setResponsibleParty(String newResponsibleParty)
     {
        responsibleParty = trim(newResponsibleParty);
     }     

    /**
     * gets cbrPayment, This is the payment object.
     * @return CbrPaymentVO cbrPayment
     */
     public CbrPaymentVO getCBRPaymentVO()
     {
        return cbrPayment;
     }
     
    /**
     * sets cbrPayment
     * @param newCBRPaymentVO  CbrPaymentVO
     */
     public void setCBRPaymentVO(CbrPaymentVO newCBRPaymentVO)
     {
        cbrPayment = newCBRPaymentVO;
     }   
     
    private String trim(String str)
    {
        return (str != null)?str.trim():str;
    }


  public void setRefundAdminFee(double refundAdminFee)
  {
    this.refundAdminFee = refundAdminFee;
  }


  public double getRefundAdminFee()
  {
    return refundAdminFee;
  }


  public void setRefundServiceFeeTax(double refundServiceFeeTax)
  {
    this.refundServiceFeeTax = refundServiceFeeTax;
  }


  public double getRefundServiceFeeTax()
  {
    return refundServiceFeeTax;
  }


  public void setRefundShippingTax(double refundShippingTax)
  {
    this.refundShippingTax = refundShippingTax;
  }


  public double getRefundShippingTax()
  {
    return refundShippingTax;
  }


  public void setRefundDiscountAmount(double refundDiscountAmount)
  {
    this.refundDiscountAmount = refundDiscountAmount;
  }


  public double getRefundDiscountAmount()
  {
    return refundDiscountAmount;
  }


  public void setRefundCommissionAmount(double refundCommissionAmount)
  {
    this.refundCommissionAmount = refundCommissionAmount;
  }


  public double getRefundCommissionAmount()
  {
    return refundCommissionAmount;
  }


  public void setRefundWholesaleAmount(double refundWholesaleAmount)
  {
    this.refundWholesaleAmount = refundWholesaleAmount;
  }


  public double getRefundWholesaleAmount()
  {
    return refundWholesaleAmount;
  }


  public void setAcctTransInd(String acctTransInd)
  {
    this.acctTransInd = acctTransInd;
  }


  public String getAcctTransInd()
  {
    return acctTransInd;
  }


  public void setRefundStatus(String billStatus)
  {
    this.refundStatus = refundStatus;
  }


  public String getRefundStatus()
  {
    return refundStatus;
  }


  public void setRefundWholesaleServiceFee(double refundWholesaleServiceFee)
  {
    this.refundWholesaleServiceFee = refundWholesaleServiceFee;
  }


  public double getRefundWholesaleServiceFee()
  {
    return refundWholesaleServiceFee;
  }

public CbrPaymentVO getCbrPayment() {
	return cbrPayment;
}

public void setCbrPayment(CbrPaymentVO cbrPayment) {
	this.cbrPayment = cbrPayment;
}

public String getTax1Name() {
	return tax1Name;
}

public void setTax1Name(String tax1Name) {
	this.tax1Name = tax1Name;
}

public String getTax1Description() {
	return tax1Description;
}

public void setTax1Description(String tax1Description) {
	this.tax1Description = tax1Description;
}

public BigDecimal getTax1Rate() {
	return tax1Rate;
}

public void setTax1Rate(BigDecimal tax1Rate) {
	this.tax1Rate = tax1Rate;
}

public BigDecimal getTax1Amount() {
	return tax1Amount;
}

public void setTax1Amount(BigDecimal tax1Amount) {
	this.tax1Amount = tax1Amount;
}

public String getTax2Name() {
	return tax2Name;
}

public void setTax2Name(String tax2Name) {
	this.tax2Name = tax2Name;
}

public String getTax2Description() {
	return tax2Description;
}

public void setTax2Description(String tax2Description) {
	this.tax2Description = tax2Description;
}

public BigDecimal getTax2Rate() {
	return tax2Rate;
}

public void setTax2Rate(BigDecimal tax2Rate) {
	this.tax2Rate = tax2Rate;
}

public BigDecimal getTax2Amount() {
	return tax2Amount;
}

public void setTax2Amount(BigDecimal tax2Amount) {
	this.tax2Amount = tax2Amount;
}

public String getTax3Name() {
	return tax3Name;
}

public void setTax3Name(String tax3Name) {
	this.tax3Name = tax3Name;
}

public String getTax3Description() {
	return tax3Description;
}

public void setTax3Description(String tax3Description) {
	this.tax3Description = tax3Description;
}

public BigDecimal getTax3Rate() {
	return tax3Rate;
}

public void setTax3Rate(BigDecimal tax3Rate) {
	this.tax3Rate = tax3Rate;
}

public BigDecimal getTax3Amount() {
	return tax3Amount;
}

public void setTax3Amount(BigDecimal tax3Amount) {
	this.tax3Amount = tax3Amount;
}

public String getTax4Name() {
	return tax4Name;
}

public void setTax4Name(String tax4Name) {
	this.tax4Name = tax4Name;
}

public String getTax4Description() {
	return tax4Description;
}

public void setTax4Description(String tax4Description) {
	this.tax4Description = tax4Description;
}

public BigDecimal getTax4Rate() {
	return tax4Rate;
}

public void setTax4Rate(BigDecimal tax4Rate) {
	this.tax4Rate = tax4Rate;
}

public BigDecimal getTax4Amount() {
	return tax4Amount;
}

public void setTax4Amount(BigDecimal tax4Amount) {
	this.tax4Amount = tax4Amount;
}

public String getTax5Name() {
	return tax5Name;
}

public void setTax5Name(String tax5Name) {
	this.tax5Name = tax5Name;
}

public String getTax5Description() {
	return tax5Description;
}

public void setTax5Description(String tax5Description) {
	this.tax5Description = tax5Description;
}

public BigDecimal getTax5Rate() {
	return tax5Rate;
}

public void setTax5Rate(BigDecimal tax5Rate) {
	this.tax5Rate = tax5Rate;
}

public BigDecimal getTax5Amount() {
	return tax5Amount;
}

public void setTax5Amount(BigDecimal tax5Amount) {
	this.tax5Amount = tax5Amount;
}
}