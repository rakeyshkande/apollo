package com.ftd.accountingreporting.vo;

import java.io.Serializable;
import java.util.Calendar;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import com.ftd.osp.utilities.xml.DOMUtil;

/**
 * This class represents the value object for table GC_COUPON_RECIPIENTS
 * 
 * @author Madhusudan Parab
 */
public class GccRecipientVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5968896627481060385L;
	private long gcCouponReceiptId;
	private String requestNumber;
	private String firstName;
	private String lastName;
	private String address1;
	private String address2;
	private String city;
	private String state;
	private String zipCode;
	private String country;
	private String phone;
	private String emailAddress;
	private Calendar createdOn;
	private String createdBy;
	private Calendar updatedOn;
	private String updatedBy;
	private String gcCouponNumber;
	private String issuedReferenceNumber;
	private String giftCertificateId;
	
	

	/**
	 * Constructor for class GccRecipientVO
	 */
	public GccRecipientVO() {
	}
	
	
	/**gets CertificateID associated with Recipient
	 * 
	 * 
	 * @return String
	 */
	public String getGiftCertificateId() {
		return giftCertificateId;
	}




	public void setGiftCertificateId(String giftCertificateId) {
		this.giftCertificateId = giftCertificateId;
	}




	/**
	 * gets gcCouponReceiptId
	 * 
	 * @return long gcCouponReceiptId
	 */
	public long getGcCouponReceiptId() {
		return gcCouponReceiptId;
	}

	/**
	 * sets gcCouponReceiptId
	 * 
	 * @param long newGcCouponReceiptId
	 */
	public void setGcCouponReceiptId(long newGcCouponReceiptId) {
		this.gcCouponReceiptId = newGcCouponReceiptId;
	}

	/**
	 * gets requestNumber
	 * 
	 * @return String requestNumber
	 */
	public String getRequestNumber() {
		return requestNumber;
	}

	/**
	 * sets requestNumber
	 * 
	 * @param String
	 *            newRequestNumber
	 */
	public void setRequestNumber(String newRequestNumber) {
		this.requestNumber = newRequestNumber;
	}

	/**
	 * gets firstName
	 * 
	 * @return String firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * sets firstName
	 * 
	 * @param String
	 *            newFirstName
	 */
	public void setFirstName(String newFirstName) {
		this.firstName = newFirstName;
	}

	/**
	 * gets lastName
	 * 
	 * @return String lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * sets lastName
	 * 
	 * @param String
	 *            newLastName
	 */
	public void setLastName(String newLastName) {
		this.lastName = newLastName;
	}

	/**
	 * gets address1
	 * 
	 * @return String address1
	 */
	public String getAddress1() {
		return address1;
	}

	/**
	 * sets address1
	 * 
	 * @param String
	 *            newAddress1
	 */
	public void setAddress1(String newAddress1) {
		this.address1 = newAddress1;
	}

	/**
	 * gets address2
	 * 
	 * @return String address2
	 */
	public String getAddress2() {
		return address2;
	}

	/**
	 * sets address2
	 * 
	 * @param String
	 *            newAddress2
	 */
	public void setAddress2(String newAddress2) {
		this.address2 = newAddress2;
	}

	/**
	 * gets city
	 * 
	 * @return String city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * sets city
	 * 
	 * @param String
	 *            newCity
	 */
	public void setCity(String newCity) {
		this.city = newCity;
	}

	/**
	 * gets state
	 * 
	 * @return String state
	 */
	public String getState() {
		return state;
	}

	/**
	 * sets state
	 * 
	 * @param String
	 *            newState
	 */
	public void setState(String newState) {
		this.state = newState;
	}

	/**
	 * gets zipCode
	 * 
	 * @return String zipCode
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * sets zipCode
	 * 
	 * @param String
	 *            newZipCode
	 */
	public void setZipCode(String newZipCode) {
		this.zipCode = newZipCode;
	}

	/**
	 * gets country
	 * 
	 * @return String country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * sets country
	 * 
	 * @param String
	 *            newCountry
	 */
	public void setCountry(String newCountry) {
		this.country = newCountry;
	}

	/**
	 * gets phone
	 * 
	 * @return String phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * sets phone
	 * 
	 * @param String
	 *            newPhone
	 */
	public void setPhone(String newPhone) {
		this.phone = newPhone;
	}

	/**
	 * gets emailAddress
	 * 
	 * @return String emailAddress
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * sets emailAddress
	 * 
	 * @param String
	 *            newEmailAddress
	 */
	public void setEmailAddress(String newEmailAddress) {
		this.emailAddress = newEmailAddress;
	}

	/**
	 * gets createdOn
	 * 
	 * @return Calendar createdOn
	 */
	public Calendar getCreatedOn() {
		return createdOn;
	}

	/**
	 * sets createdOn
	 * 
	 * @param Calendar
	 *            newCreatedOn
	 */
	public void setCreatedOn(Calendar newCreatedOn) {
		this.createdOn = newCreatedOn;
	}

	/**
	 * gets createdBy
	 * 
	 * @return String createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * sets createdBy
	 * 
	 * @param String
	 *            newCreatedBy
	 */
	public void setCreatedBy(String newCreatedBy) {
		this.createdBy = newCreatedBy;
	}

	/**
	 * gets updatedOn
	 * 
	 * @return Calendar updatedOn
	 */
	public Calendar getUpdatedOn() {
		return updatedOn;
	}

	/**
	 * sets updatedOn
	 * 
	 * @param Calendar
	 *            newUpdatedOn
	 */
	public void setUpdatedOn(Calendar newUpdatedOn) {
		this.updatedOn = newUpdatedOn;
	}

	/**
	 * gets updatedBy
	 * 
	 * @return String updatedBy
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * sets updatedBy
	 * 
	 * @param String
	 *            newUpdatedBy
	 */
	public void setUpdatedOn(String newUpdatedBy) {
		this.updatedBy = newUpdatedBy;
	}

	/**
	 * gets gcCouponNumber
	 * 
	 * @return String gcCouponNumber
	 */
	public String getGcCouponNumber() {
		return gcCouponNumber;
	}

	/**
	 * sets gcCouponNumber
	 * 
	 * @param String
	 *            newGcCouponNumber
	 */
	public void setGcCouponNumber(String newGcCouponNumber) {
		this.gcCouponNumber = newGcCouponNumber;
	}
	
	public String getIssuedReferenceNumber() {
		return issuedReferenceNumber;
	}

	public void setIssuedReferenceNumber(String issuedReferenceNumber) {
		this.issuedReferenceNumber = issuedReferenceNumber;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * Converts the value object to an XML string in the same format as the
	 * database table and column name.
	 */
	public Document toXML() throws Exception {
		Document rDoc = DOMUtil.getDefaultDocument();
		Element field = (Element) rDoc.createElement("GC_COUPON_RECIPIENT");
		rDoc.appendChild(field);
		Element fieldAttr = null;
		Text fieldText = null;

		fieldAttr = (Element) rDoc.createElement("gc_coupon_recip_id");
//		fieldText = rDoc.createTextNode(gcCouponReceiptId == 0 ? "" : String
//				.valueOf(gcCouponReceiptId));
		fieldText = rDoc.createTextNode(giftCertificateId == null ? "" :
			giftCertificateId);
		fieldAttr.appendChild(fieldText);
		field.appendChild(fieldAttr);
		fieldAttr = (Element) rDoc.createElement("request_number");
		fieldText = rDoc.createTextNode(requestNumber == null ? ""
				: requestNumber);
		field.appendChild(fieldAttr);
		fieldAttr.appendChild(fieldText);
		fieldAttr = (Element) rDoc.createElement("gc_coupon_number");
		fieldText = rDoc.createTextNode(gcCouponNumber == null ? ""
				: gcCouponNumber);
		fieldAttr.appendChild(fieldText);
		field.appendChild(fieldAttr);
		fieldAttr = (Element) rDoc.createElement("first_name");
		fieldText = rDoc.createTextNode(firstName == null ? "" : firstName);
		fieldAttr.appendChild(fieldText);
		field.appendChild(fieldAttr);
		fieldAttr = (Element) rDoc.createElement("last_name");
		fieldText = rDoc.createTextNode(lastName == null ? "" : lastName);
		fieldAttr.appendChild(fieldText);
		field.appendChild(fieldAttr);
		fieldAttr = (Element) rDoc.createElement("address_1");
		fieldText = rDoc.createTextNode(address1 == null ? "" : address1);
		fieldAttr.appendChild(fieldText);
		field.appendChild(fieldAttr);
		fieldAttr = (Element) rDoc.createElement("address_2");
		fieldText = rDoc.createTextNode(address2 == null ? "" : address2);
		fieldAttr.appendChild(fieldText);
		field.appendChild(fieldAttr);
		fieldAttr = (Element) rDoc.createElement("city");
		fieldText = rDoc.createTextNode(city == null ? "" : city);
		fieldAttr.appendChild(fieldText);
		field.appendChild(fieldAttr);
		fieldAttr = (Element) rDoc.createElement("state");
		fieldText = rDoc.createTextNode(state == null ? "" : state);
		fieldAttr.appendChild(fieldText);
		field.appendChild(fieldAttr);
		fieldAttr = (Element) rDoc.createElement("zip_code");
		fieldText = rDoc.createTextNode(zipCode == null ? "" : zipCode);
		fieldAttr.appendChild(fieldText);
		field.appendChild(fieldAttr);
		fieldAttr = (Element) rDoc.createElement("country");
		fieldText = rDoc.createTextNode(country == null ? "" : country);
		fieldAttr.appendChild(fieldText);
		field.appendChild(fieldAttr);
		fieldAttr = (Element) rDoc.createElement("phone");
		fieldText = rDoc.createTextNode(phone == null ? "" : phone);
		fieldAttr.appendChild(fieldText);
		field.appendChild(fieldAttr);
		fieldAttr = (Element) rDoc.createElement("email_address");
		fieldText = rDoc.createTextNode(emailAddress == null ? ""
				: emailAddress);
		fieldAttr.appendChild(fieldText);
		field.appendChild(fieldAttr);
		fieldAttr = (Element) rDoc.createElement("issuedReferenceNumber");
		fieldText = rDoc.createTextNode(issuedReferenceNumber == null ? ""
				: issuedReferenceNumber);
		fieldAttr.appendChild(fieldText);
		field.appendChild(fieldAttr);

		return rDoc;
	}

	/**
	 * An object is null if all fields are null.
	 * 
	 * @return
	 * @throws Exception
	 */
	public boolean isNullObject() throws Exception {
		if ((firstName == null || firstName.equals(""))
				&& (lastName == null || lastName.equals(""))
				&& (address1 == null || address1.equals(""))
				&& (address2 == null || address2.equals(""))
				&& (city == null || city.equals(""))
				&& (state == null || state.equals(""))
				&& (zipCode == null || zipCode.equals(""))
				&& (country == null || country.equals(""))
				&& (phone == null || phone.equals(""))
				&& (emailAddress == null || emailAddress.equals(""))
				&& (issuedReferenceNumber == null || issuedReferenceNumber.equals(""))) {
			return true;
		} else {
			return false;
		}
	}
}