package com.ftd.accountingreporting.vo;

import java.io.Serializable;

/**
 * This class represents the value object for Payment Totals.
 * 
 * @author Madhusudan Parab
 */
public class PaymentTotalsVO 
{
  
  private long paymentId;
  private double serviceFeeTotal;
  private double shippingFeeTotal;
  private double shippingTaxTotal;
  private double taxTotal;
  
 /**
  * constructor for class Payment Totals Value Object
  */
  public PaymentTotalsVO()
  {
  }
  
 /**
  * sets serviceFeeTotal
  * @param serviceFeeTotal double
  */
  public void setServiceFeeTotal(double newServiceFeeTotal)
  {
    serviceFeeTotal = newServiceFeeTotal;
  }
  
 /**
  * gets serviceFeeTotal
  * @return double serviceFeeTotal
  */
  public double getServiceFeeTotal()
  {
    return serviceFeeTotal;
  }

 /**
  * sets shippingFeeTotal
  * @param shippingFeeTotal double
  */
  public void setShippingFeeTotal(double newShippingFeeTotal)
  {
    shippingFeeTotal = newShippingFeeTotal;
  }
  
 /**
  * gets shippingFeeTotal
  * @return double shippingFeeTotal
  */
  public double getShippingFeeTotal()
  {
    return shippingFeeTotal;
  }
  
 /**
  * sets shippingTaxTotal
  * @param shippingTaxTotal double
  */
  public void setShippingTaxTotal(double newShippingTaxTotal)
  {
    shippingTaxTotal = newShippingTaxTotal;
  }
  
 /**
  * gets shippingTaxTotal
  * @return double shippingTaxTotal
  */
  public double getShippingTaxTotal()
  {
    return shippingTaxTotal;
  }

 /**
  * sets taxTotal
  * @param taxTotal double
  */
  public void setTaxTotal(double newTaxTotal)
  {
    taxTotal = newTaxTotal;
  }
  
 /**
  * gets taxTotal
  * @return double taxTotal
  */
  public double getTaxTotal()
  {
    return taxTotal;
  }

 /**
  * sets paymentId
  * @param paymentId long
  */
  public void setPaymentId(long newPaymentId)
  {
    paymentId = newPaymentId;
  }
  
 /**
  * gets paymentId
  * @return long paymentId
  */
  public long getPaymentId()
  {
    return paymentId;
  }
} 