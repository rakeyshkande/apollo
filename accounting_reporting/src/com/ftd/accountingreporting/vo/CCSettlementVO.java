package com.ftd.accountingreporting.vo;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This is the credit card settlement value object.  
 * @author Charles Fox
 */
public class CCSettlementVO 
{

    private String ccNumber = "";
    private double amount;
    private Date transactionDate;
    private String authNumber = "";
    private String reconDispCode = "";

    public CCSettlementVO()
    {
    }
    
    /**
     * gets ccNumber
     * This is the credit card number.
     * @return String ccNumber
     */
     public String getCCNumber()
     {
        return ccNumber;
     }
     
    /**
     * sets ccNumber
     * @param newCCNumber  String
     */
    public void setCCNumber(String newCCNumber)
     {
        ccNumber = trim(newCCNumber);
     }

    /**
     * gets amount
     * This is amount received in the file.
     * @return double amount
     */
     public double getAmount()
     {
        return amount;
     }
     
    /**
     * sets amount
     * @param newAmount  double
     */
    public void setAmount(double newAmount)
     {
        amount = newAmount;
     }
     
    /**
     * gets transactionDate
     * This is the transaction date.
     * @return Date transactionDate
     */
     public Date getTransactionDate()
     {
        return transactionDate;
     }
     
    /**
     * sets transactionDate
     * @param newTransactionDate  Date
     */
    public void setTransactionDate(Date newTransactionDate)
     {
        transactionDate = newTransactionDate;
     }

    /**
     * gets authNumber
     * This is the authorization number.
     * @return String authNumber
     */
     public String getAuthNumber()
     {
        return authNumber;
     }
     
    /**
     * sets authNumber
     * @param newAuthNumber  String
     */
    public void setAuthNumber(String newAuthNumber)
     {
        authNumber = trim(newAuthNumber);
     }

    /**
     * gets reconDispCode
     * This is the reconciliation disposition code.
     * @return String reconDispCode
     */
     public String getReconDispCode()
     {
        return reconDispCode;
     }
     
    /**
     * sets reconDispCode
     * @param newReconDispCode  String
     */
    public void setReconDispCode(String newReconDispCode)
     {
        reconDispCode = trim(newReconDispCode);
     }
       
    private String trim(String str)
    {
        return (str != null)?str.trim():str;
    } 
    
    private String getDisplayTransDate() throws Exception {
        String dateFormat = "MMddyy";
        SimpleDateFormat sdfOutput = new SimpleDateFormat (dateFormat);
            
        return sdfOutput.format(transactionDate);
    }
    public String toString() {
        String objectString = "";
        try {
            objectString =  "CC size: " + this.ccNumber.length() +
               "*** CC last 4 digits: " + this.ccNumber.substring(this.ccNumber.length() - 4) +
               "*** transaction date: " + this.getDisplayTransDate() +
               "*** auth number: " + this.authNumber +
               "*** credit amount: " + this.amount;      
            return objectString;
        } catch (Exception e) {
            return "";
        }
    }
}