package com.ftd.accountingreporting.vo;

import java.io.Serializable;
  
/**
 * This class represents the value object for End of Day Payment.
 * 
 */
public class EODPaymentVO
{
    private String paymentId;
    private String masterOrderNumber;
    private String paymentInd;
    
    /**
     * Constructor for class EODPaymentVO
     */
     public EODPaymentVO()
     {
     }

    /**
     * Constructor for class EODPaymentVO
     */
     public EODPaymentVO(String _masterOrderNumber, String _paymentId, String _paymentInd)
     {
          masterOrderNumber = _masterOrderNumber;
          paymentId = _paymentId;
          paymentInd = _paymentInd;
     }
     
      public void setPaymentId(String paymentId)
      {
        this.paymentId = paymentId;
      }
    
    
      public String getPaymentId()
      {
        return paymentId;
      }
    
    
      public void setMasterOrderNumber(String masterOrderNumber)
      {
        this.masterOrderNumber = masterOrderNumber;
      }
    
    
      public String getMasterOrderNumber()
      {
        return masterOrderNumber;
      }


  public void setPaymentInd(String paymentInd)
  {
    this.paymentInd = paymentInd;
  }


  public String getPaymentInd()
  {
    return paymentInd;
  }
    

}