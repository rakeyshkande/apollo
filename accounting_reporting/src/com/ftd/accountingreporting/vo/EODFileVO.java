package com.ftd.accountingreporting.vo;
import com.ftd.accountingreporting.constant.ARConstants;
import com.ftd.osp.utilities.ConfigurationUtil;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;


public class EODFileVO 
{
  private long startTime;
  private String eodFileName;
  private long batchNumber;
  private long batchTime;
  private String companyId;
  
  public EODFileVO()
  {
  }
  
  public void setStartTime(long _startTime) {
      startTime = _startTime;
  }
  
  
   /**
   * Returns the batch time in the format of MMddyy_HHmmss.
   * @return 
   * @throws java.lang.Exception
   */
   
   public String getRunDateTime() throws Exception
   {
     SimpleDateFormat sdf = new SimpleDateFormat("MMddyy_HHmmss");
     return sdf.format(new Date(this.startTime));
   } 
   
      /**
   * Retrieves name of the JDE file. JDE file name is jde_mmddyy_timemillis.
   * @param batchDatetime
   * @return 
   * @throws java.lang.Exception
   */
   public String getEodFileName() throws Exception {
      String eodFileName = ConfigurationUtil.getInstance().getProperty(ARConstants.CONFIG_FILE, ARConstants.CONST_EOD_FILE_LOCATION)
                       + ConfigurationUtil.getInstance().getProperty(ARConstants.CONFIG_FILE, ARConstants.CONST_EOD_FILE_NAME);
     
      return eodFileName;
   }
   
   /**
    * 
    */
   public String getPTSFileName() throws Exception {
	   String ptsFileName = ConfigurationUtil.getInstance().getProperty(ARConstants.CONFIG_FILE, ARConstants.CONST_PTS_FILE_LOCATION)
       + ConfigurationUtil.getInstance().getProperty(ARConstants.CONFIG_FILE, ARConstants.CONST_PTS_FILE_NAME)+"_"+getCompanyId();
	   return ptsFileName;
   }
   
  /**
   * sets the first line for the Billing File
   */
   public String getFirstLine() throws Exception
   {
     //set the variable from config file billingFileFirstLine
     String billingFileFirstLine = ConfigurationUtil.getInstance().getProperty(ARConstants.CONFIG_FILE, ARConstants.CONST_EOD_BILLING_FIRST_LINE);
     return billingFileFirstLine == null? "" : billingFileFirstLine;
   }  
   
  /**
   * gets the header record type from the config file
   */
   public String getHeaderRecordType() throws Exception
   {
     //sets the header record type
     String headerRecordType = ConfigurationUtil.getInstance().getProperty(ARConstants.CONFIG_FILE, ARConstants.CONST_EOD_HEADER_RECORD_TYPE);
     return headerRecordType == null? "" : headerRecordType;
   }

  /**
   * sets the trailer record type from the config file
   */
   public String getTrailerRecordType() throws Exception
   {
     String trailerRecordType = ConfigurationUtil.getInstance().getProperty(ARConstants.CONFIG_FILE, ARConstants.CONST_EOD_TRAILER_RECORD_TYPE);
     return trailerRecordType == null? "" : trailerRecordType;
   }
   
  public void setBatchNumber(long batchNumber)
  {
    this.batchNumber = batchNumber;
  }

  public long getBatchNumber()
  {
    return batchNumber;
  }
   
  public void setBatchTime(long batchTime)
  {
    this.batchTime = batchTime;
  }

  public long getBatchTime()
  {
    return batchTime;
  }  
  /**
   * Returns the batch date in the format of MMddyy.
   * @return 
   * @throws java.lang.Exception
   */
   public String getDisplayBatchDate() throws Exception
   {
     SimpleDateFormat sdf = new SimpleDateFormat("MMddyy");
     return sdf.format(getBatchDate().getTime());
   }
   
  /**
   * sets the batchDate by retrieving the eodBatchDateOffset from config file 
   * and calculate the date from current date in MMDDYY format
   */
   public Calendar getBatchDate() throws Exception
   {
     Calendar batchDate = Calendar.getInstance();
     batchDate.setTime(new Date(batchTime));
     return batchDate;
   }

	public String getCompanyId() {
		return companyId;
	}
	
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
   
   
}