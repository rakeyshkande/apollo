package com.ftd.accountingreporting.vo;

/**
 * This is the value object that models each line item on the bill file.
 * @author Charles Fox
 */
public class AafesRecordVO 
{
    public AafesRecordVO()
    {
    }
    
    private String billingDetailId = "";  
    private String paymentId = "";
	private String type = "";
    private String ccnumber = "";
	private String amount = "";
    private String facnbr = "";
    private String authcode = "";
    private String ticket = "";
    private String returnCode = "";
    private String returnMessage = "";
    private String verifiedFlag = "";
    private String refundId = "";
    /**
     * gets billingDetailId
     * This is the AAFES_BILLING_DETAIL.BILLING_DETAIL_ID 
     * @return String billingDetailId
     */
     public String getBillingDetailId()
     {
        return billingDetailId;
     }
     
    /**
     * sets billingDetailId
     * @param newBillingDetailId  String
     */
     public void setBillingDetailId(String newBillingDetailId)
     {
        billingDetailId = trim(newBillingDetailId);
     }

    /**
     * gets paymentId
     * This is the AAFES_BILLING_DETAIL.PAYMENT_ID 
     * @return String paymentId
     */
     public String getPaymentId()
     {
        return paymentId;
     }
     
    /**
     * sets paymentId
     * @param newPaymentId  String
     */
     public void setPaymentId(String newPaymentId)
     {
        paymentId = trim(newPaymentId);
     }
    /**
     * gets type
     * this is the credit or debit type. �S� for bill/add bill and �C� for refund
     * @return String billingDetailId
     */
     public String getType()
     {
        return type;
     }
     
    /**
     * sets type
     * @param newType  String
     */
     public void setType(String newType)
     {
        type = trim(newType);
     }
 
 
    /**
     * gets ccnumber
     * This is the credit card number.
     * @return String ccnumber
     */
     public String getCCNumber()
     {
        return ccnumber;
     }
     
    /**
     * sets ccnumber
     * @param newCCNumber  String
     */
     public void setCCNumber(String newCCNumber)
     {
        ccnumber = trim(newCCNumber);
     }    

    /**
     * gets amount
     * This is the amount to be settled.
     * @return String amount
     */
     public String getAmount()
     {
        return amount;
     }
     
    /**
     * sets amount
     * @param newAmount  String
     */
     public void setAmount(String newAmount)
     {
        amount = trim(newAmount);
     } 
     
     /**
     * gets facnbr
     * This is the facility number
     * @return String facnbr
     */
     public String getFacNbr()
     {
        return facnbr;
     }
     
    /**
     * sets facnbr
     * @param newFacNbr  String
     */
     public void setFacNbr(String newFacNbr)
     {
        facnbr = trim(newFacNbr);
     }     

     /**
     * gets authcode
     * This is the auth code.
     * @return String authcode
     */
     public String getAuthCode()
     {
        return authcode;
     }
     
    /**
     * sets authcode
     * @param newAuthCode  String
     */
     public void setAuthCode(String newAuthCode)
     {
        authcode = trim(newAuthCode);
     }  
     
     /**
     * gets ticket
     * This is the ticket number 
     * @return String ticket
     */
     public String getTicket()
     {
        return ticket;
     }
     
    /**
     * sets ticket
     * @param newTicket  String
     */
     public void setTicket(String newTicket)
     {
        ticket = trim(newTicket);
     }  

     /**
     * gets returnCode
     * This is the return code.
     * @return String returnCode
     */
     public String getReturnCode()
     {
        return returnCode;
     }
     
    /**
     * sets returnCode
     * @param newReturnCode  String
     */
     public void setReturnCode(String newReturnCode)
     {
        returnCode = trim(newReturnCode);
     }  

     /**
     * gets returnMessage
     * This is the return message.
     * @return String returnMessage
     */
     public String getReturnMessage()
     {
        return returnMessage;
     }
     
    /**
     * sets returnMessage
     * @param newReturnMessage  String
     */
     public void setReturnMessage (String newReturnMessage)
     {
        returnMessage = trim(newReturnMessage);
     }  
     
     /**
     * gets verifiedFlag
     * This is the verified flag.
     * @return String verifiedFlag
     */
     public String getVerifiedFlag()
     {
        return verifiedFlag;
     }
     
    /**
     * sets verifiedFlag
     * @param newVerifiedFlag  String
     */
     public void setVerifiedFlag(String newVerifiedFlag)
     {
        verifiedFlag = trim(newVerifiedFlag);
     }  

    /**
     * gets refundId
     * This is the AAFES_BILLING_DETAIL.REFUND_ID 
     * @return String refundId
     */
     public String getRefundId()
     {
        return refundId;
     }
     
    /**
     * sets refundId
     * @param newRefundId  String
     */
     public void setRefundId(String newRefundId)
     {
        refundId = trim(newRefundId);
     }
    private String trim(String str)
    {
        return (str != null)?str.trim():str;
    } 
    
    public String toString() {
        return "Payment Id: " + this.paymentId
               + "***Auth Code: " + this.authcode
               + "***Ticket Number: " + this.ticket
               + "***Amount: " + this.amount
               + "***Bill Type " + this.type;
    }
}