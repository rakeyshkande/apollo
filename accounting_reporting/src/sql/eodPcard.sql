insert into ftd_apps.pcard_config values (1,'CARD_HOLDER_REF_NUM',1,'ORDER_DETAILS','ARIBA_PO_NUMBER',null);
insert into ftd_apps.pcard_config values (2,'SALES_TAX',1,'ORDERS','TAX_TOTAL',null);
insert into ftd_apps.pcard_config values (3,'BILLINGINFO2',1,'CO_BRAND','INFO_NAME','id:');
insert into ftd_apps.pcard_config values (4,'BILLINGINFO2',2,'CO_BRAND','INFO_NAME','duns:');
insert into ftd_apps.pcard_config values (5,'BILLINGINFO2',3,'ORDERS','SHIPPING_FEE_TOTAL','frt:');
insert into ftd_apps.pcard_config values (6,'BILLINGINFO2',4,'ORDERS','TAX_TOTAL','tax:');
insert into ftd_apps.partner_pcard_config_ref values('J&J',1);
insert into ftd_apps.partner_pcard_config_ref values('J&J',2);
insert into ftd_apps.partner_pcard_config_ref values('J&J',3);
insert into ftd_apps.partner_pcard_config_ref values('J&J',4);
insert into ftd_apps.partner_pcard_config_ref values('J&J',5);
insert into ftd_apps.partner_pcard_config_ref values('J&J',6);

insert into ftd_apps.pcard_config values (7,'CARD_HOLDER_REF_NUM',1,'ORDER_DETAILS','ARIBA_PO_NUMBER',null);
insert into ftd_apps.pcard_config values (8,'BILLINGINFO4',1,'ORDERS','SHIPPING_FEE_TOTAL','frt:');
insert into ftd_apps.partner_pcard_config_ref values('ADP',7);
insert into ftd_apps.partner_pcard_config_ref values('ADP',8);


insert into ftd_apps.pcard_config values (9,'CARD_HOLDER_REF_NUM',1,'ORDER_DETAILS','ARIBA_PO_NUMBER',null);
insert into ftd_apps.pcard_config values (10,'SALES_TAX',1,'ORDERS','TAX_TOTAL',null);
insert into ftd_apps.pcard_config values (11,'BILLINGINFO2',1,'ORDER_DETAILS','ARIBA_PO_NUMBER','po:');
insert into ftd_apps.partner_pcard_config_ref values('BI',9);
insert into ftd_apps.partner_pcard_config_ref values('BI',10);
insert into ftd_apps.partner_pcard_config_ref values('BI',11);


insert into ftd_apps.pcard_config values (12,'SALES_TAX',1,'ORDERS','TAX_TOTAL',null);
insert into ftd_apps.pcard_config values (13,'BILLINGINFO2',1,'ORDER_DETAILS','ARIBA_PO_NUMBER','po:');
insert into ftd_apps.partner_pcard_config_ref values('MEDTR',12);
insert into ftd_apps.partner_pcard_config_ref values('MEDTR',13);


insert into ftd_apps.pcard_config values (14,'CARD_HOLDER_REF_NUM',1,'ORDER_DETAILS','ARIBA_PO_NUMBER',null);
insert into ftd_apps.pcard_config values (15,'BILLINGINFO2',1,null,null,'N/A');
insert into ftd_apps.partner_pcard_config_ref values('SCIATL',14);
insert into ftd_apps.partner_pcard_config_ref values('SCIATL',15);

insert into ftd_apps.pcard_config values (16,'CARD_HOLDER_REF_NUM',1,'ORDER_DETAILS','ARIBA_PO_NUMBER',null);
insert into ftd_apps.partner_pcard_config_ref values('ACS',16);

commit;