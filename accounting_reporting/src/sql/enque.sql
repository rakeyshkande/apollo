--RUN-EOD
DECLARE
enqueue_options dbms_aq.enqueue_options_t;
message_properties dbms_aq.message_properties_t;
message_handle raw(2000);
message sys.aq$_jms_text_message;
begin
      --Create a new JMS Message
      message := sys.aq$_jms_text_message.construct();

      --Set the properties in the message
      message.set_string_property(property_name => 'CONTEXT',
                                  property_value => 'ACCOUNTING');
      message.set_text(payload => '<?xml version="1.0" encoding="utf-8"?><event xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="event.xsd"><event-name>RUN-EOD</event-name><context>ACCOUNTING</context><payload></payload></event>' );

dbms_aq.enqueue (queue_name => 'OJMS.EM_ACCOUNTING',
enqueue_options => enqueue_options,
message_properties => message_properties,
payload => message,
msgid => message_handle);
commit;
end;

-- EOD-FINALIZER
DECLARE
enqueue_options dbms_aq.enqueue_options_t;
message_properties dbms_aq.message_properties_t;
message_handle raw(2000);
message sys.aq$_jms_text_message;
begin
      --Create a new JMS Message
      message := sys.aq$_jms_text_message.construct();

      --Set the properties in the message
      message.set_string_property(property_name => 'CONTEXT',
                                  property_value => 'ACCOUNTING');
      message.set_text(payload => '<?xml version="1.0" encoding="utf-8"?><event xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="event.xsd"><event-name>EOD-FINALIZER</event-name><context>ACCOUNTING</context><payload><![CDATA[<ROOT><BATCH_TIME>1143647996250</BATCH_TIME><START_TIME>1143734396250</START_TIME><BATCH_NUMBER>5468</BATCH_NUMBER></ROOT>]]></payload></event>' );

dbms_aq.enqueue (queue_name => 'OJMS.EM_ACCOUNTING',
enqueue_options => enqueue_options,
message_properties => message_properties,
payload => message,
msgid => message_handle);
commit;
end;

--RECON-MERCURY
DECLARE
enqueue_options dbms_aq.enqueue_options_t;
message_properties dbms_aq.message_properties_t;
message_handle raw(2000);
message sys.aq$_jms_text_message;
begin
      --Create a new JMS Message
      message := sys.aq$_jms_text_message.construct();

      --Set the properties in the message
      message.set_string_property(property_name => 'CONTEXT',
                                  property_value => 'ACCOUNTING');
      message.set_text(payload => '<?xml version="1.0" encoding="utf-8"?><event xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="event.xsd"><event-name>RECON-MERCURY</event-name><context>ACCOUNTING</context><payload></payload></event>' );

dbms_aq.enqueue (queue_name => 'OJMS.EM_ACCOUNTING',
enqueue_options => enqueue_options,
message_properties => message_properties,
payload => message,
msgid => message_handle);
commit;
end;

--RECON-VENUS
DECLARE
enqueue_options dbms_aq.enqueue_options_t;
message_properties dbms_aq.message_properties_t;
message_handle raw(2000);
message sys.aq$_jms_text_message;
begin
      --Create a new JMS Message
      message := sys.aq$_jms_text_message.construct();

      --Set the properties in the message
      message.set_string_property(property_name => 'CONTEXT',
                                  property_value => 'ACCOUNTING');
      message.set_text(payload => '<?xml version="1.0" encoding="utf-8"?><event xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="event.xsd"><event-name>RECON-VENUS</event-name><context>ACCOUNTING</context><payload></payload></event>' );

dbms_aq.enqueue (queue_name => 'OJMS.EM_ACCOUNTING',
enqueue_options => enqueue_options,
message_properties => message_properties,
payload => message,
msgid => message_handle);
commit;
end;

--RECON-CC
DECLARE
enqueue_options dbms_aq.enqueue_options_t;
message_properties dbms_aq.message_properties_t;
message_handle raw(2000);
message sys.aq$_jms_text_message;
begin
      --Create a new JMS Message
      message := sys.aq$_jms_text_message.construct();

      --Set the properties in the message
      message.set_string_property(property_name => 'CONTEXT',
                                  property_value => 'ACCOUNTING');
      message.set_text(payload => '<?xml version="1.0" encoding="utf-8"?><event xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="event.xsd"><event-name>RECON-CC</event-name><context>ACCOUNTING</context><payload></payload></event>' );

dbms_aq.enqueue (queue_name => 'OJMS.EM_ACCOUNTING',
enqueue_options => enqueue_options,
message_properties => message_properties,
payload => message,
msgid => message_handle);
commit;
end;

--RECON-ARCHIVE-MESSAGE
DECLARE
enqueue_options dbms_aq.enqueue_options_t;
message_properties dbms_aq.message_properties_t;
message_handle raw(2000);
message sys.aq$_jms_text_message;
begin
      --Create a new JMS Message
      message := sys.aq$_jms_text_message.construct();

      --Set the properties in the message
      message.set_string_property(property_name => 'CONTEXT',
                                  property_value => 'ACCOUNTING');
      message.set_text(payload => '<?xml version="1.0" encoding="utf-8"?><event xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="event.xsd"><event-name>RECON-ARCHIVE-MESSAGE</event-name><context>ACCOUNTING</context><payload></payload></event>' );

dbms_aq.enqueue (queue_name => 'OJMS.EM_ACCOUNTING',
enqueue_options => enqueue_options,
message_properties => message_properties,
payload => message,
msgid => message_handle);
commit;
end;

--AAFES-EOD-PROCESS
DECLARE
enqueue_options dbms_aq.enqueue_options_t;
message_properties dbms_aq.message_properties_t;
message_handle raw(2000);
message sys.aq$_jms_text_message;
begin
      --Create a new JMS Message
      message := sys.aq$_jms_text_message.construct();

      --Set the properties in the message
      message.set_string_property(property_name => 'CONTEXT',
                                  property_value => 'ACCOUNTING');
      message.set_text(payload => '<?xml version="1.0" encoding="utf-8"?><event xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="event.xsd"><event-name>AAFES-EOD-PROCESS</event-name><context>ACCOUNTING</context><payload></payload></event>' );

dbms_aq.enqueue (queue_name => 'OJMS.EM_ACCOUNTING',
enqueue_options => enqueue_options,
message_properties => message_properties,
payload => message,
msgid => message_handle);
commit;
end;

--AMAZON-EOD-PROCESS
DECLARE
enqueue_options dbms_aq.enqueue_options_t;
message_properties dbms_aq.message_properties_t;
message_handle raw(2000);
message sys.aq$_jms_text_message;
begin
      --Create a new JMS Message
      message := sys.aq$_jms_text_message.construct();

      --Set the properties in the message
      message.set_string_property(property_name => 'CONTEXT',
                                  property_value => 'ACCOUNTING');
      message.set_text(payload => '<?xml version="1.0" encoding="utf-8"?><event xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="event.xsd"><event-name>AMAZON-EOD-PROCESS</event-name><context>ACCOUNTING</context><payload></payload></event>' );

dbms_aq.enqueue (queue_name => 'OJMS.EM_ACCOUNTING',
enqueue_options => enqueue_options,
message_properties => message_properties,
payload => message,
msgid => message_handle);
commit;
end;

--GC-FEED-GC
DECLARE
enqueue_options dbms_aq.enqueue_options_t;
message_properties dbms_aq.message_properties_t;
message_handle raw(2000);
message sys.aq$_jms_text_message;
begin
      --Create a new JMS Message
      message := sys.aq$_jms_text_message.construct();

      --Set the properties in the message
      message.set_string_property(property_name => 'CONTEXT',
                                  property_value => 'ACCOUNTING');
      message.set_text(payload => '<?xml version="1.0" encoding="utf-8"?><event xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="event.xsd"><event-name>GC-FEED-GC</event-name><context>ACCOUNTING</context><payload><![CDATA[<root><event>GC-FEED-GC</event><partner-program>DISC2</partner-program></root>]]></payload></event>' );

dbms_aq.enqueue (queue_name => 'OJMS.EM_ACCOUNTING',
enqueue_options => enqueue_options,
message_properties => message_properties,
payload => message,
msgid => message_handle);
commit;
end;

--GC-FEED-FF
DECLARE
enqueue_options dbms_aq.enqueue_options_t;
message_properties dbms_aq.message_properties_t;
message_handle raw(2000);
message sys.aq$_jms_text_message;
begin
      --Create a new JMS Message
      message := sys.aq$_jms_text_message.construct();

      --Set the properties in the message
      message.set_string_property(property_name => 'CONTEXT',
                                  property_value => 'ACCOUNTING');
      message.set_text(payload => '<?xml version="1.0" encoding="utf-8"?><event xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="event.xsd"><event-name>GC-FEED-FF</event-name><context>ACCOUNTING</context><payload><![CDATA[<root><event>GC-FEED-FF</event><partner-program>DISC2</partner-program></root>]]></payload></event>' );

dbms_aq.enqueue (queue_name => 'OJMS.EM_ACCOUNTING',
enqueue_options => enqueue_options,
message_properties => message_properties,
payload => message,
msgid => message_handle);
commit;
end;

--GC-FEED-RD
DECLARE
enqueue_options dbms_aq.enqueue_options_t;
message_properties dbms_aq.message_properties_t;
message_handle raw(2000);
message sys.aq$_jms_text_message;
begin
      --Create a new JMS Message
      message := sys.aq$_jms_text_message.construct();

      --Set the properties in the message
      message.set_string_property(property_name => 'CONTEXT',
                                  property_value => 'ACCOUNTING');
      message.set_text(payload => '<?xml version="1.0" encoding="utf-8"?><event xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="event.xsd"><event-name>GC-FEED-RD</event-name><context>ACCOUNTING</context><payload><![CDATA[<root><event>GC-FEED-RD</event><partner-program>DISC2</partner-program></root>]]></payload></event>' );

dbms_aq.enqueue (queue_name => 'OJMS.EM_ACCOUNTING',
enqueue_options => enqueue_options,
message_properties => message_properties,
payload => message,
msgid => message_handle);
commit;
end;

--AMAZON SETTLEMENT-FEED
DECLARE
enqueue_options dbms_aq.enqueue_options_t;
message_properties dbms_aq.message_properties_t;
message_handle raw(2000);
message sys.aq$_jms_text_message;
begin
      --Create a new JMS Message
      message := sys.aq$_jms_text_message.construct();

      --Set the properties in the message
      message.set_string_property(property_name => 'CONTEXT',
                                  property_value => 'AMAZON');
      message.set_text(payload => '<?xml version="1.0" encoding="utf-8"?><event xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="event.xsd"><event-name>SETTLEMENT-FEED</event-name><context>AMAZON</context><payload></payload></event>' );
      message_properties.CORRELATION := 'AMAZON SETTLEMENT-FEED';

dbms_aq.enqueue (queue_name => 'OJMS.EM_AMAZON',
enqueue_options => enqueue_options,
message_properties => message_properties,
payload => message,
msgid => message_handle);
commit;
end;


--AMAZON-REMITTANCE-PROCESS
DECLARE
enqueue_options dbms_aq.enqueue_options_t;
message_properties dbms_aq.message_properties_t;
message_handle raw(2000);
message sys.aq$_jms_text_message;
begin
      --Create a new JMS Message
      message := sys.aq$_jms_text_message.construct();

      --Set the properties in the message
      message.set_string_property(property_name => 'CONTEXT',
                                  property_value => 'ACCOUNTING');
      message.set_text(payload => '<?xml version="1.0" encoding="utf-8"?><event xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="event.xsd"><event-name>AMAZON-REMITTANCE-PROCESS</event-name><context>ACCOUNTING</context><payload></payload></event>' );

dbms_aq.enqueue (queue_name => 'OJMS.EM_ACCOUNTING',
enqueue_options => enqueue_options,
message_properties => message_properties,
payload => message,
msgid => message_handle);
commit;
end;

--VENDOR-EOM
DECLARE
enqueue_options dbms_aq.enqueue_options_t;
message_properties dbms_aq.message_properties_t;
message_handle raw(2000);
message sys.aq$_jms_text_message;
begin
      --Create a new JMS Message
      message := sys.aq$_jms_text_message.construct();

      --Set the properties in the message
      message.set_string_property(property_name => 'CONTEXT',
                                  property_value => 'ACCOUNTING');
      message.set_text(payload => '<?xml version="1.0" encoding="utf-8"?><event xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="event.xsd"><event-name>VENUS-EOM</event-name><context>ACCOUNTING</context><payload></payload></event>' );

dbms_aq.enqueue (queue_name => 'OJMS.EM_ACCOUNTING',
enqueue_options => enqueue_options,
message_properties => message_properties,
payload => message,
msgid => message_handle);
commit;
end;