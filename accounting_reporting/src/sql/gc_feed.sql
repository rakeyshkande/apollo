INSERT INTO ftd_apps.program_type_val VALUES ('GC', 'Active', 'Gift Certificate Partner Program');

insert into clean.gc_coupon_status_val values ('Issued Inactive', 'An inactive gcc', 'Active', 'N');

insert into ftd_apps.partner_program(program_name, partner_name, program_type, email_exclude_flag, created_on, created_by, updated_on, updated_by,program_long_name)
values('DISC2','DISCOVER','GC','Y',SYSDATE,'SYS',SYSDATE,'SYS','DISCOVER GIFT CERT FEED');

-- Following was added to support Intuit GC program.
insert into ftd_apps.partner_program(program_name, partner_name, program_type, email_exclude_flag, created_on, created_by, updated_on, updated_by,program_long_name)
values('INTUIT','INTUIT','GC','Y',SYSDATE,'SYS',SYSDATE,'SYS','INTUIT GIFT CERT FEED');
insert into ftd_apps.partner_master(partner_name,created_on,created_by,updated_on,updated_by)
values ('APOLLO GENERIC PARTNER',sysdate, 'SYS',sysdate,'SYS');
insert into ftd_apps.partner_program(program_name, partner_name, program_type, email_exclude_flag, created_on, created_by, updated_on, updated_by,program_long_name)
values('GCGENERIC','APOLLO GENERIC PARTNER','GC','Y',SYSDATE,'SYS',SYSDATE,'SYS','GENERIC GIFT CERT FEED');

insert into clean.gc_config_parm (gc_config_parm_id, parm_name, parm_description)
values (1,'outbound_gc','name of outbound gift cert inventory file');
insert into clean.gc_config_parm (gc_config_parm_id, parm_name, parm_description)
values (2,'inbound_ff','name of inbound gift cert fulfullment file');
insert into clean.gc_config_parm (gc_config_parm_id, parm_name, parm_description)
values (3,'outbound_rd','name of outbound gift cert redemption file');
insert into clean.gc_config_parm (gc_config_parm_id, parm_name, parm_description)
values (4,'partner_sequence','partner sequence');

-- Following was added to support Intuit GC program.
insert into clean.gc_config_parm (gc_config_parm_id, parm_name, parm_description)
values (5,'inbound_act','name of outbound gift cert fulfillment response file');
insert into clean.gc_config_parm (gc_config_parm_id, parm_name, parm_description)
values (6,'outbound_act_resp','name of outbound gift cert fulfillment response file');
insert into clean.gc_config_parm (gc_config_parm_id, parm_name, parm_description)
values (7,'inbound_deact','name of inbound gift cert deactivation file');
insert into clean.gc_config_parm (gc_config_parm_id, parm_name, parm_description)
values (8,'outbound_deact_resp','name of outbound gift cert deactivation response file');

insert into clean.gc_program_config (gc_program_config_id, gc_config_parm_id, gc_partner_program_name, parm_value, template_name)
values (1, 1, 'DISC2', 'FTD.INVENTORY.TXT', 'gc-outbound-gc-1.xml');
insert into clean.gc_program_config (gc_program_config_id, gc_config_parm_id, gc_partner_program_name, parm_value, template_name)
values (2, 2, 'DISC2', 'notify.txt', 'gc-inbound-ff-1.xml');
insert into clean.gc_program_config (gc_program_config_id, gc_config_parm_id, gc_partner_program_name, parm_value, template_name)
values (3, 3, 'DISC2', 'FTD.REDEMPT.TXT', 'gc-outbound-rd-1.xml');
insert into clean.gc_program_config (gc_program_config_id, gc_config_parm_id, gc_partner_program_name, parm_value, template_name)
values (4, 4, 'INTUIT', 'Y', null);

-- Following was added to support Intuit GC program.
insert into clean.gc_program_config (gc_program_config_id, gc_config_parm_id, gc_partner_program_name, parm_value, template_name)
values (5, 5, 'INTUIT', 'CENVEO_FTD_ACT_', 'gc-inbound-intuit.xml');
insert into clean.gc_program_config (gc_program_config_id, gc_config_parm_id, gc_partner_program_name, parm_value, template_name)
values (6, 6, 'INTUIT', 'CENVEO_FTD_ACT_', 'Not Used');
insert into clean.gc_program_config (gc_program_config_id, gc_config_parm_id, gc_partner_program_name, parm_value, template_name)
values (7, 7, 'INTUIT', 'CENVEO_FTD_DEACT_', 'gc-inbound-intuit.xml');
insert into clean.gc_program_config (gc_program_config_id, gc_config_parm_id, gc_partner_program_name, parm_value, template_name)
values (8, 8, 'INTUIT', 'CENVEO_FTD_DEACT_', 'Not Used');

insert into clean.gc_program_config (gc_program_config_id, gc_config_parm_id, gc_partner_program_name, parm_value, template_name)
values (9, 4, 'GCGENERIC', 'Y', null);
insert into clean.gc_program_config (gc_program_config_id, gc_config_parm_id, gc_partner_program_name, parm_value, template_name)
values (10, 5, 'GCGENERIC', 'Not Used', 'gc-inbound-vanilla.xml');
insert into clean.gc_program_config (gc_program_config_id, gc_config_parm_id, gc_partner_program_name, parm_value, template_name)
values (11, 7, 'GCGENERIC', 'Not Used', 'gc-inbound-vanilla.xml');

commit;