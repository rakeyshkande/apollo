package com.ftd.accountingreporting.altpay.handler;

import org.easymock.EasyMock;

import com.ftd.accountingreporting.altpay.vo.AltPayBillingDetailVOBase;
import com.ftd.accountingreporting.altpay.vo.AltPayProfileVOBase;
import com.ftd.osp.utilities.ConfigurationUtil;

import junit.framework.Assert;
import junit.framework.TestCase;

public class AltPayCartHandlerBaseMilesPointsTests extends TestCase {
	
	ConfigurationUtil util;

	protected void setUp() throws Exception {
		
		util = EasyMock.createMock(ConfigurationUtil.class);
		EasyMock.expect(util.getSecureProperty("SERVICE", "SVS_CLIENT")).andReturn("BEARS");
		EasyMock.expect(util.getSecureProperty("SERVICE", "SVS_HASHCODE")).andReturn("RULES");
		EasyMock.expect(util.getFrpGlobalParm("SERVICE", "MILESPOINTS_SERVICE_URL")).andReturn("http://consumer-dev.ftdi.com/ps/services/MPS?wsdl");
		EasyMock.replay(util);
		
		super.setUp();
		
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	
	public void testMilesPointsCall() throws Throwable {
		
		UACartHandler uaCartHandler = new UACartHandler();
		uaCartHandler.util = util;
		AltPayProfileVOBase pvo = new AltPayProfileVOBase();
		pvo.setPaymentMethodId("UA");
		AltPayBillingDetailVOBase bvo = new AltPayBillingDetailVOBase();
		bvo.setPaymentId("123");
		bvo.setReqMilesAmt(1);
		bvo.setMembershipNumber("03129325380");
		
		String ret = uaCartHandler.doDeductMiles(pvo, bvo);
		
		Assert.assertTrue(ret.equals("A"));
	
	}		
}