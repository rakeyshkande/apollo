var CUSTOMER_COMMENTS = 1;
var ACCOUNT_HISTORY = 2;
var UPDATE_CUSTOMER = 3;
var HOLD = 4;
var LOSS_PREVENTION = 5;
var SEARCH = 6;
var BACK = 7;
var MAIN_MENU = 8;
var PRINTER = 9;
var EMAIL_LIST = 10;

var imgArray = new Array();
var imgPressedArray = new Array();
var imgGray = new Array();
var navigation = new Array("firstItem","backItem","nextItem","lastItem");

/*
   Calls helper methods to limit navigation options.
*/
function setNavigationHandlers()
{
   document.oncontextmenu = contextMenuHandler;
   document.onkeydown = backKeyHandler;
}

/*
   Disables the right button mouse click.
*/
function contextMenuHandler()
{
   return false;
}

/*
   Disables the various navigation keyboard commands.
*/
function backKeyHandler()
{
   // backspace
   if (window.event && window.event.keyCode == 8){
      var formElement = false;
      
      for(i = 0; i < document.forms[0].elements.length; i++){
         if(document.forms[0].elements[i].name == document.activeElement.name){
            formElement = true;
            break;
         }
      }
      
      if(formElement == false || (document.activeElement.type != "text" && document.activeElement.type != "textarea" && document.activeElement.type != "password")){
         window.event.cancelBubble = true;
         window.event.returnValue = false;
         return false;
      }
   }

   // F5 and F11
   if (window.event.keyCode == 122 || window.event.keyCode == 116){
      window.event.keyCode = 0;
      event.returnValue = false;
      return false;
   }

   // Alt + Left Arrow
   if(window.event.altLeft){
      window.event.returnValue = false;
      return false;
   }

   // Alt + Right Arrow
   if(window.event.altKey){
      window.event.returnValue = false;
      return false;
   }     
}

/*
   Returns the style object for the given element's field name.
*/
function getStyleObject(fieldName)
{
   if (document.getElementById && document.getElementById(fieldName))
      return document.getElementById(fieldName).style;
}

/*
   Limits input to digits only.      
*/
function digitOnlyListener()
{
   var input = event.keyCode;
   if (input < 48 || input > 57){
       event.returnValue = false;
   }
}

/*
   The limitTextarea will squelch any characters that exceed the limit value included in the method call.
   textarea - the textarea object
   limit - the number of characters allowed
*/
function limitTextarea(textarea, limit)
{
   if (textarea.value.length > limit)
      textarea.value = textarea.value.substring(0, limit);
}

/*
   The following function changes the control's style sheet to 'errorField'.
   elements - an Array of control names on the form
*/
function setErrorFields(elements)
{
    var element;
    for (var i = 0; i < elements.length; i++){
        element = document.getElementById(elements[i]);
        if (element != null){
            element.className = "errorField";
        }
    }
}
function setErrorField(element)
{
    var element = document.getElementById(element);
    if (element != null){
        element.className = "errorField";
    }
}

/*
    The following functions attached the 'onfocus' and 'onblur' events the the input
    elements.  When a control on the page gains focus, the control's border changes to red.
    When focus is lost, the control's border changes back to default.

    elements - an Array of control names on the form
    element - a control name on the form
*/
function addDefaultListenersArray(elements)
{
    var element;
    for (var i = 0; i < elements.length; i++){
        element = document.getElementById(elements[i]);
        if (element != null)
            addDefaultListenersSingle(elements[i]);
    }
}
function addDefaultListenersSingle(element)
{
    document.getElementById(element).attachEvent("onfocus", fieldFocus);
    document.getElementById(element).attachEvent("onblur", fieldBlur);
}
function fieldFocus()
{
    var element = event.srcElement;
    element.style.borderWidth = 2;
    element.style.borderColor = "red";
}
function fieldBlur()
{
    var element = event.srcElement;
    element.style.borderWidth = 2;
    element.style.borderColor = document.body.style.backgroundColor;
}

/*
    The following functions change the cursor UI when an image triggers a 'mouseover' event.

    elements - an Array of control names on the form
*/
function addImageCursorListener(elements)
{
    var element;
    for (var i = 0; i < elements.length; i++){
        element = document.getElementById(elements[i]);
        if (element != null){
            element.attachEvent("onmouseover", imageOver);
            element.attachEvent("onmouseout", imageOut);
        }
    }
}
function imageOver()
{
    document.forms[0].style.cursor = "hand";
}
function imageOut()
{
    document.forms[0].style.cursor = "default";
}

/*
    The following functions are used to dis/enable fields.

    elements - an Array of control names on the form
    access - boolean value used to set the disabled property
*/
function disableFields(elements)
{
    setFieldsAccess(elements, true)
}
function enableFields(elements)
{
    setFieldsAccess(elements, false)
}
function setFieldsAccess(elements, access)
{
    var element;
    for (var i = 0; i < elements.length; i++){
        element = document.getElementById(elements[i]);
        if (element != null){
            setFieldAccess(element, access)
        }
    }
}
function setFieldAccess(element, access)
{
   element.disabled = access;
}

/*
   The following functions are used to show and hide any 
   select boxes that would normally appear in front of a popup.
*/
function getAbsolutePos(element) {
   var r = { x: element.offsetLeft, y: element.offsetTop };
   if (element.offsetParent) {
      var tmp = getAbsolutePos(element.offsetParent);
      r.x += tmp.x;
      r.y += tmp.y;
   }
   return r;
}

function hideShowCovered(popup)
{
   function contains(a, b) {
      while (b.parentNode)
         if ((b = b.parentNode) == a)
            return true;
      return false;
   };

   var p = getAbsolutePos(popup);
   var EX1 = p.x;
   var EX2 = popup.offsetWidth + EX1;
   var EY1 = p.y;
   var EY2 = popup.offsetHeight + EY1;

   var tagsToHide = new Array("select");
   for (var k = 0; k < tagsToHide.length; k++) {
      var tags = document.getElementsByTagName(tagsToHide[k]);
      var tag = null;

      for (var i = 0; i < tags.length; i++) {
         tag = tags[i];
         if (!contains(this.div, tag)){
            p = getAbsolutePos(tag);
            var CX1 = p.x;
            var CX2 = tag.offsetWidth + CX1;
            var CY1 = p.y;
            var CY2 = tag.offsetHeight + CY1;

            if ( (CX1 > EX2) || (CX2 < EX1) || (CY1 > EY2) || (CY2 < EY1) ) {
               if (!tag.saveVisibility) {
                  tag.saveVisibility = tag.currentStyle["visibility"];
               }
               tag.style.visibility = tag.saveVisibility;
            } else {
               if (!tag.saveVisibility) {
                  tag.saveVisibility =  tag.currentStyle["visibility"];
               }
               tag.style.visibility = "hidden";
            }
         }
      }
   }
}

/*
  The following methods are used to display a wait message to the user
  while an action is being executed.  The following coding convention should
  be used for the wait div's table structure:

  waitDiv - id of the wait div
  waitMessage - id of the TD which will contain the message
  waitTD - id of the TD which will contain the dots (...)

  Parameters
  content - The div id containing the content to be hidden
  wait - The div id containing the wait message
  message - Optional, if provided the message to be displayed, otherwise, "Processing"
            will be the message
*/
function showWaitMessage(content, wait, message){

   var content = document.getElementById(content);
   var height = content.offsetHeight;
   var waitDiv = document.getElementById(wait + "Div").style;
   var waitMessage = document.getElementById(wait + "Message");
   _waitTD = document.getElementById(wait + "TD");

   content.style.display = "none";
   waitDiv.display = "block";
   waitDiv.height = height;
   waitMessage.innerHTML = (message) ? message : "Processing";
   clearWaitMessage();
   updateWaitMessage();
}

var _waitTD = "";
function clearWaitMessage(){
   _waitTD.innerHTML = "";
   setTimeout("clearWaitMessage()", 5000);
}
function updateWaitMessage(){
   setTimeout("updateWaitMessage()", 1000);
   setTimeout("showPeriod()", 1000);
}
function showPeriod(){
   _waitTD.innerHTML += "&nbsp;.";
}


/*
   This is a helper function to return the two security parameters
   required by the security framework.

   areOnlyParams -   is used to change the url string (? vs. &) depending if these
                     are the only parameters in the url
*/
function getSecurityParams(areOnlyParams){
   return   ((areOnlyParams) ? "?" : "&") +
            "securitytoken=" + document.getElementById("securitytoken").value +
            "&context=" + document.getElementById("context").value;
}
/*
  This function is used to clear all form fields on a page.

*/
function clearAllFields(form){
	
   for(var i = 0; i < form.elements.length; i++){
	if(form.elements[i].type == 'radio')
		form.elements[i].checked = false;
	if(form.elements[i].type == 'text')
		form.elements[i].value = "";
	if(form.elements[i].type == 'checkbox')
		form.elements[i].checked = false;		
   }
}

function doHandleNavigationButtons(page){
     if( document.forms[0].total_pages.value == 1){
     	
     	for(var i = 0; i < navigation.length; i++){
     	
     	   var button = document.getElementById(new String(navigation[i]));
     	   button.disabled = "true";
     	   button.src = imgGray[i].src;
     	}
	return;	
     }
     if( page < 2){
     	document.getElementById(new String(navigation[0])).src = imgGray[0].src;
	document.getElementById(new String(navigation[0])).disabled = "true";
	document.getElementById(new String(navigation[1])).src = imgGray[1].src;
	document.getElementById(new String(navigation[1])).disabled = "true";
	return;
     }
     if( page == document.forms[0].total_pages.value){	
	document.getElementById(new String(navigation[2])).src = imgGray[2].src;
	document.getElementById(new String(navigation[2])).disabled = "true";	
	document.getElementById(new String(navigation[3])).src = imgGray[3].src;
	document.getElementById(new String(navigation[3])).disabled = "true";
	document.getElementById(new String(navigation[0])).enabled = "true";
	document.getElementById(new String(navigation[1])).enabled = "true";
	return;
     }
}
/*
Sets the tabindexs for the 
Customer Account page
This is not the same as the indexes that 
will be set when the Customer Account page is
included into the Customer Shopping Cart page.
*/
var lastIndex = 0;

function setTabIndexes(){
	var startIndex = 5;
  var anchors = document.getElementsByName('master');
  var externalAnchors = document.getElementsByName('external');
  if((anchors != null && anchors.length > 0) && (externalAnchors != null && externalAnchors.length > 0)){
	for(var i = 0; i < anchors.length; i++){
		anchors[i].tabIndex = startIndex++;		
	}
	for(var k = 0; k < externalAnchors.length ; k++){
		externalAnchors[k].tabIndex = startIndex++;
	}
	lastIndex = startIndex;
  }
}
/*
This function is used specifically for the customer account page
sets the indexes on the buttons after the links have been set.
This way we know how many links there are and count up from there.

Makes use of the lastIndex variable defined above the setTabIndexes() function.
*/
function setButtonIndexs(){
   var buttons = document.getElementsByTagName('button');
   for(var i = 0; i < buttons.length; i++){
      if(buttons[i].id == 'customer_comments')
	buttons[i].tabIndex = lastIndex + CUSTOMER_COMMENTS;
      if(buttons[i].id == 'account_history')
	buttons[i].tabIndex = lastIndex + ACCOUNT_HISTORY;
      if(buttons[i].id == 'update_customer')
	buttons[i].tabIndex = lastIndex + UPDATE_CUSTOMER;
      if(buttons[i].id == 'hold')
	buttons[i].tabIndex = lastIndex + HOLD;
      if(buttons[i].id == 'loss_prevention')
	buttons[i].tabIndex = lastIndex + LOSS_PREVENTION;
      if(buttons[i].id == 'search')
	buttons[i].tabIndex = lastIndex + SEARCH;
      if(buttons[i].id == 'back')
	buttons[i].tabIndex = lastIndex + BACK;
      if(buttons[i].id == 'main_menu')
	buttons[i].tabIndex = lastIndex + MAIN_MENU;
  }
  var backHeader = document.getElementById('backButtonHeader');
 	if(backHeader != null)
 		backHeader.tabIndex = -1;
 	var viewing = document.getElementById('currentCSRs');
 	if(viewing != null)
 	 	viewing.tabIndex = -1;
  var printer = document.getElementById('printer');
  if(printer != null)
   printer.parentNode.tabIndex = lastIndex + 9;
}



/***********************************************************************************
* doAccountSearch()
************************************************************************************/
		 	function doAccountSearch(orderNumber){
		 		var guid = document.getElementById('guid_' + orderNumber).value;
		 		var url = "customerOrderSearch.do" +
		 				"?action=customer_account_search" + "&order_number=" + orderNumber
		 					 + "&order_guid=" + guid;
		 		performAction(url);
		 	}
/***********************************************************************************
* doReturnEmailAddresses()
************************************************************************************/
		 	function doReturnEmailAddresses(){
		 		var url_source = "customerOrderSearch.do" + "?action=more_customer_emails" +
		 				"&customer_id=" + cust_id + getSecurityParams(false);
	 		    var modal_dim = "dialogWidth:700px; dialogHeight:320px; center:yes; status=0; help:no; scroll:no";
		 		window.showModalDialog(url_source, "", modal_dim);
		 		//sourceCodeWindow = window.open("customerEmailAddresses.html","testPage","width=700,height=200,location=no,status=no,toolbar=no,top=130,left=40,titlebar=no,title=no");
		 	}

/*****************************************************
*
*	doAccountHistoryAction()
*
*****************************************************/
			function doAccountHistoryAction(){
				var url = "loadHistory.do?page_position=1" +
				"&history_type=Customer";
			
				performAction(url);
			}



/*
These following functions are used to dynamically change
the navigation image.
*/
function imgUnPressed(val){
    var img = event.srcElement;
    if(!img.disabled)
      img.src = imgArray[val].src;
}
function imgPressed(val){
    var img = event.srcElement;
    if(!img.disabled)
       img.src = imgPressedArray[val].src;
}


function Trim(value)
{
  return value.replace(/^\s+/,'').replace(/\s+$/,'');
}




/*
 functions used on the Customer Shopping Cart
 and Recipient Order Information pages

*/
/*The array in this function contains
  the list of elements which need to have 
  the tabIndex properly set. Modifying this
  will change the tab index sequence on the 
  Recipient/Order Information pages
*/
function setRecipTabIndexes(){
	var begIndex = findBeginIndex();
	var recipTabElements = new Array('sourceCodeLink','memberInfoLink',
			'altContactLink','recipNumberLink','orderPaymentInfoLink','updateOrderInfo',
			'updatePaymentInfo','updateFlorist','floristNameLink','orderComments','email','letters',
			'orderHistory','origView','floristInquiry','communiction','tagOrder',
			'reconciliation','refund','hold','search','back','printOrder','mainMenu');
			
	for(var i = 0; i < (tabsArray.length - 2) ; i++){
	  var z = (i + 1);
	  var count = 1;
	  for(var x = 0; x < recipTabElements.length; x++){
	  	var element = document.getElementById(recipTabElements[x] + z); 	  	
			setIndex(element,begIndex,count);
	  }
	  
	  var special = document.getElementById('specialIns' + z);
	  if(special != null)
	  	special.tabIndex = -1;
	  	
	  var cardMessage =  document.getElementById('cardMessage' + z);
	  if(cardMessage != null)
	  	cardMessage.tabIndex = -1;
	  	
		var currentCsrs = document.getElementById('currentCSRs');
		if(currentCsrs != null)
			currentCsrs.tabIndex = -1;
			
		var back = document.getElementById('backButtonHeader');
		if(back != null)
			back.tabIndex = -1;
			
		var printer = document.getElementById('printer');
		if(printer != null)
			printer.parentNode.tabIndex = 100;
			
	  var numberEntry = document.getElementById('numberEntry');
	  if(numberEntry != null)
	 		 numberEntry.tabIndex = 101;
	
	}
}
/*
	Sets the Customer Shopping Cart
	page elements tabIndex properites
*/
function setCartIndexes(){
	var begIndex = findBeginIndex();
	var cartElements = new Array('cartSourceCodeLink','cartPaymentInfoLink','cartUpdateCustomer',
					'cartCommentsAction','cartHistoryAction','cartHold','cartLossPrevention',
					'cartRefund','cartSearch','cartBack','cartMainMenu');
					
	for(var p = 0; p < cartElements.length; p++){
			var ele = document.getElementById(cartElements[p]);
			setCartElements(ele,begIndex);
			begIndex++;
	}	
}
/*
	Used with setCartIndexes()
*/
function setCartElements(element,index){
		if(element != null){
				element.tabIndex = (index);
		}
		else{
				return;
		}		
}
/* 
	Used with setRecipTabIndexes()
	to set all tabIndexes on Recip/Order
	page
*/
function setIndex(ele, beg,count){
	if(ele != null){
		ele.tabIndex = (beg + count++);
	}else{
		return;
	}
}
/*
	Returns the starting index
	after the last order tab
*/
function findBeginIndex(){
	var begIndex = 4;
	if(tabsArray.length == 5){
		begIndex = 6;
	}
	if(tabsArray.length == 4){
		begIndex = 5;
	}
	return begIndex;
}

function setCSCustomerAccountIndexes(){
	var endIndex;
	var begIndex = findBeginIndex();
	var caElements = new Array('email_address','caMoreLink','caDirectMail',
										'backItem','backItem2','nextItem','nextItem2','firstItem','firstItem2',
										'lastItem','lastItem2');
	var caButtonElems = new Array('customer_comments','account_history',
										'update_customer','hold','loss_prevention','search','back','main_menu');
										
	for(var a = 0; a < caElements.length; a++){
			var ele = document.getElementById(caElements[a]);
			setCSCustomerAccount(ele,begIndex);
			begIndex++;
	}
	
	var anchors = document.getElementsByName('master');
  var externalAnchors = document.getElementsByName('external');
  if((anchors != null && anchors.length > 0) && (externalAnchors != null && externalAnchors.length > 0)){
			for(var i = 0; i < anchors.length; i++){
				anchors[i].tabIndex = begIndex++;		
			}
			for(var k = 0; k < externalAnchors.length ; k++){
				externalAnchors[k].tabIndex = begIndex++;
			}			
	}	
	
	for(var r = 0; r < caButtonElems.length; r++){
			var ele = document.getElementById(caButtonElems[r]);
			setCSCustomerAccount(ele,begIndex);
			begIndex++;
	}
}

function setCSCustomerAccount(ele, index){
	if(ele != null){
				ele.tabIndex = (index);
		}
		else{
				return;
		}		
}