<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:import href="header_cbr.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:import href="securityAndData.xsl"/>
<xsl:output method="html" indent="no"/>
<xsl:output indent="yes"/>
<xsl:variable name="headername" select="'EOD Utilities'"/>
<xsl:template name="eodUtil" match="/">


<html>
<head>
    <title>FTD - EOD Utilities</title>
    <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
</head>

<script type="text/javascript" src="js/Tokenizer.js"></script>
<script type="text/javascript" src="js/prototype.js"></script>
<script type="text/javascript" src="js/rico.js"></script>
<script type="text/javascript" src="js/ftdajax.js"></script>
<script type="text/javascript">
<![CDATA[
function doMainMenuAction()
{
  window.location="MainMenuAction.do?gcc_user=user_sa" + "&securitytoken=" + document.forms[0].securitytoken.value + "&context=" + document.forms[0].context.value;
}
]]>


function doCheckPP()
{
  var newRequest = new FTD_AJAX.Request('EODUtil.do', FTD_AJAX_REQUEST_TYPE_POST, doResp, true);
  newRequest.addParam('action_type', 'checkPPConn');
  newRequest.send();
}

function doCheckUnited()
{
  var newRequest = new FTD_AJAX.Request('EODUtil.do', FTD_AJAX_REQUEST_TYPE_POST, doResp, true);
  newRequest.addParam('action_type', 'checkUnitedConn');
  newRequest.send();
}


function checkTimerName()
{
  if(document.getElementById("timerName").value == '')
  {
    alert("Please enter a Timer Name!");
    return false;
  }
  return true;  
}

function doResp(data)
{
  alert(data);
}


function doIsTimerRunning()
{
  if(checkTimerName())
  {
    var newRequest = new FTD_AJAX.Request('EODUtil.do', FTD_AJAX_REQUEST_TYPE_POST, doResp, true);
    newRequest.addParam('action_type', 'isTimerRunning');
    newRequest.addParam('timerName', document.getElementById("timerName").value);
    newRequest.send();
  }
}

function doStopTimer()
{
  if(checkTimerName())
  {
    var newRequest = new FTD_AJAX.Request('EODUtil.do', FTD_AJAX_REQUEST_TYPE_POST, doResp, true);
    newRequest.addParam('action_type', 'stopTimer');
    newRequest.addParam('timerName', document.getElementById("timerName").value);
    newRequest.send();
  }
}

function doViewTimers()
{
  var newRequest = new FTD_AJAX.Request('EODUtil.do', FTD_AJAX_REQUEST_TYPE_POST, doViewTimerResp, true);
  newRequest.addParam('action_type', 'viewTimers');
  newRequest.send();
}

function doViewTimerResp(data)
{
  if(data.length == 0) 
    data = "There are no Timers running.";
  
  document.getElementById("timer_info").value = data;
}

</script>

<body>
<xsl:call-template name="IncludeHeader"/>
<form name="AlternatePaymentUtilForm" action="return false;">
<xsl:call-template name="securityanddata"/>

<input type="hidden" id="action_type" name="action_type"/>
<input type="hidden" id="gcc_user" name="gcc_user" value="user_op"/>
<center>
<table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
<tr>
  <td>
    <table width="98%" border="0" cellpadding="2" cellspacing="2">
        <tr>
            <td width="49%" align="center" class="header">PayPal</td>
            <td width="49%" colspan="2" align="center" class="header">Timer</td>
        </tr>
        <tr>
            <td align="center"><button class="BlueButton" name="checkpp_but" tabindex="1" accesskey="C" onclick="doCheckPP();" type="button">(C)heck PayPal Connectivity</button></td>
            <td colspan="2" align="center">To manage the Timers use the accountingreporting TimerManager MBean via the Enterprise Manager.</td>
            <!--td colspan="2" align="center">Timer Name: <input tabindex="2" type="text" id="timerName" name="timerName" value="PayPalConnectionTester"/></td-->
        </tr>
        <tr>
            <td align="center"><button class="BlueButton" name="checkpp_but" tabindex="1" accesskey="U" onclick="doCheckUnited();" type="button">Check (U)nited Connectivity</button></td>
            <td colspan="2" align="center"></td>
            <!--td colspan="2" align="center">Timer Name: <input tabindex="2" type="text" id="timerName" name="timerName" value="PayPalConnectionTester"/></td-->
        </tr>
        <!--Timer access was built into this screen but functionality to access all machines within the cluster would need to be added for the Timer functionality to work.
        <tr>
            <td></td>
            <td colspan="2" align="center"><button class="BlueButton" name="isTimerRunning_but" tabindex="3" accesskey="I" onclick="doIsTimerRunning();">(I)s the Timer running?</button>
            &nbsp;&nbsp;&nbsp;&nbsp;       <button class="BlueButton" name="stopTimer_but" tabindex="4" accesskey="S" onclick="doStopTimer();">(S)top Timer</button></td>
        </tr>
        <tr height="10%">
            <td colspan="3"></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2" align="center">Timers:</td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2" align="center"><textarea style="width:100%" id="timer_info" name="timer_info"/></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2" align="center"><button class="BlueButton" name="viewTimers_but" tabindex="3" accesskey="V" onclick="doViewTimers();">(V)iew Timers</button></td>
        </tr>
        -->
        <tr height="10%">
            <td colspan="3"></td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td align="right">
              <button class="BlueButton" style="width:80;" name="mainMenu" tabindex="5" accesskey="M" onclick="javascript:doMainMenuAction();">(M)ain Menu </button>
            </td>
        </tr>
    </table>
  </td>
</tr>
</table>
</center>
</form>
<xsl:call-template name="footer"/>
</body>
</html>

 </xsl:template>

	<!--to get the header title-->
	<xsl:template name="IncludeHeader">
		<xsl:call-template name="header">
			<xsl:with-param name="headerName" select="$headername"/>
		</xsl:call-template>
	</xsl:template>
</xsl:stylesheet>

