<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" indent="yes"/>
	<xsl:template name="header" match="/">
		<xsl:param name="headerName"/>
		<xsl:param name="showRecipientHeader"/>
		<xsl:param name="showheader"/>
		
		<xsl:variable name="src_code_exists_err_msg" select="ROOT/@src_code_exists_err_msg"/>
		<xsl:variable name="src_code_expired_err_msg" select="ROOT/@src_code_expired_err_msg"/>
		<xsl:variable name="src_code_exists_count" select="ROOT/@src_code_exists_count"/>
		<xsl:variable name="src_code_expired_count" select="ROOT/@src_code_expired_count"/>
		<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
		<script type="text/javascript" src="js/clock.js"/>
		<script type="text/javascript" language="javascript"><![CDATA[
		
		function popupInvalidSourceCodes() {
			try {			
			var srcCode = ']]><xsl:value-of select="ROOT/@src_code_exists_err_msg"></xsl:value-of><![CDATA[';
			var modal_dim = "dialogWidth:500px; dialogHeight:350px; center:yes; status:0; help:no; scroll:no";
			var srcCodeObj = new Object();			
			srcCodeObj.srcCodeStr = srcCode;
			var results=showModalDialog('giftCertificateSourceCodesErrorPopUp.html', srcCodeObj, modal_dim);
		    if (results && results != null) {
		    	//initialising some variable in results - here gc_source_code	    	
		    }		
		} catch(e) {
			//alert(e.message);
		}
		}
		
		function popupExpiredSourceCodes() {
			try {			
			var srcCode = ']]><xsl:value-of select="ROOT/@src_code_expired_err_msg"></xsl:value-of><![CDATA[';
			var modal_dim = "dialogWidth:500px; dialogHeight:350px; center:yes; status:0; help:no; scroll:no";
			var srcCodeObj = new Object();			
			srcCodeObj.srcCodeStr = srcCode;
			var results=showModalDialog('giftCertificateSourceCodesErrorPopUp.html', srcCodeObj, modal_dim);
		    if (results && results != null) {
		    	//initialising some variable in results - here gc_source_code	    	
		    }		
		} catch(e) {
			//alert(e.message);
		}
		}
					
    ]]></script>
		<table width="98%" border="0" align="center" class="aligntop" cellpadding="0" cellspacing="1">
			<tr>
				<td rowspan="2" width="25%" align="left" >
					<img border="0" src="images/logo.gif" align="absmiddle"/>
				</td>
				<xsl:if test="$showheader">
				<td width="54%" align="center" colspan="1" class="header">
					<xsl:value-of select="$headerName"/>
				</td>
				</xsl:if>
				<xsl:if test="$showRecipientHeader">
					<xsl:if test="$gcc_action = 'display_edit'">
						<xsl:choose>
							<xsl:when test="$is_multiple_gcc = 'Y' or $is_multiple_gcc = 'y'">
								<td width="54%" align="center" colspan="1" class="header">Multiple Gift Certificates/Coupons Maintenance</td>
							</xsl:when>
							<xsl:otherwise>
								<td width="54%" align="center" colspan="1" class="header">Single Gift Certificate/Coupon Maintenance</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:if>
					<xsl:if test="$gcc_action = 'display_add'">
						<td width="54%" align="center" colspan="1" class="header">Create Gift Certificates / Coupons</td>
					</xsl:if>
				</xsl:if>
				<td width="21%" align="right" id="time" class="Label"></td>
				<script type="text/javascript">startClock();</script>
			</tr>
			<tr>
				<td align="center" class="Label"></td>
				<td align="right">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="3">
					<hr/>
				</td>
			</tr>
			<tr>
				<td colspan="3" align="center">
					<span id="span_success_error" class="SuccessMessage" style="display:block">
						<xsl:value-of select="ROOT/@exception_error"/>
						<xsl:value-of select="ROOT/@success_msg"/><br></br>
					</span>
					<span id="span_src_code_validate_message" class="SuccessMessage" style="display:block">
						<br></br><xsl:if test="$src_code_exists_err_msg != '' or $src_code_expired_err_msg != ''">
						The following source codes experienced a problem<br></br>													
						<xsl:if test="$src_code_exists_err_msg != ''">
							<xsl:choose>
							<xsl:when test="$src_code_exists_count != ' ' and $src_code_exists_count > 10">							   							   
								<xsl:value-of select='substring($src_code_exists_err_msg,1,50)'/>...
								<a href="javascript:popupInvalidSourceCodes()" tabindex="{position()}">									
									<font color="0000ff">Click to view more</font></a> 																				   
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="ROOT/@src_code_exists_err_msg"/>
							</xsl:otherwise>
							</xsl:choose>
						<br></br>
					    </xsl:if> 
					    <xsl:if test="$src_code_expired_err_msg != ''">
						<xsl:choose>
							<xsl:when test="$src_code_expired_count != ' ' and $src_code_expired_count > 10">							   							   
								<xsl:value-of select='substring($src_code_expired_err_msg,1,30)'/>...
								<a href="javascript:popupExpiredSourceCodes()" tabindex="{position()}">
									<font color="0000ff">Click to view more</font></a>							   
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="ROOT/@src_code_expired_err_msg"/>
							</xsl:otherwise>
						</xsl:choose>	
						</xsl:if>
					</xsl:if>											
					</span>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<xsl:if test="$gcc_action = 'display_maintain'">
						<p class="Label">
							<xsl:value-of select="ROOT/@sub_header_name"/>
							<xsl:value-of select="ROOT/@sub_header_value"/>
						</p>
					</xsl:if>
				</td>
				<td>
					<div align="right">
						<button class="BlueButton" name="gcc_submit_exit" tabindex="29" accesskey="E" onclick="javascript:exit();">(E)xit</button>
					</div>
				</td>
			</tr>
		</table>
	</xsl:template>
</xsl:stylesheet>