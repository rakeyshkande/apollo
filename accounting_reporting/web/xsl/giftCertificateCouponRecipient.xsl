<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
<!ENTITY ndash "&#8211;">
<!ENTITY Tab "&#x00009;" >
]>
<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
	<xsl:import href="security.xsl"/>
	<xsl:output method="html" indent="yes"/>
	<xsl:key name="error" match="ROOT/ERRORS/ERROR" use="@fieldname" />
	<xsl:key name="fields" match="ROOT/FIELDS/FIELD" use="@fieldname" />
	<xsl:variable name="gcc_type" select="key('fields','gcc_type')/@value" />
	<xsl:variable name="gcc_redemption_date" select="key('fields','gcc_redemption_date')/@value" />
	<xsl:variable name="gcc_company" select="key('fields','gcc_company')/@value" />
  <xsl:variable name="gcc_partner_program_name" select="key('fields','gcc_partner_program_name')/@value" />
	<xsl:variable name="gcc_format" select="key('fields','gcc_format')/@value" />
	<xsl:variable name="gcc_redemption_type" select="key('fields','gcc_redemption_type')/@value" />
	<xsl:template match="/">
		<html>
			<head>
				<META http-equiv="Content-Type" content="text/html"/>
				<title>
					<xsl:value-of select="ROOT/PAGEDATA/HEADER"/>
				</title>
				<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
				<script type="text/javascript" src="js/clock.js"></script>
				<script type="text/javascript" src="js/create.js"></script>
				<script type="text/javascript" src="js/common_functions_all.js"/>
				<script type="text/javascript" src="js/recipient.js"></script>
				<script type="text/javascript" src="js/FormChek.js"></script>
				<script type="text/javascript">

              var totalNumber = 0;

              var original;



<![CDATA[

 function test()
               {
                document.getElementById('number').value = totalNumber;
                return totalNumber;
               }



function init()

{
    original = new Array(document.forms[0].length);
    original = saveFormValue(original);
}


function saveFormValue(formValueArray) {



    for (var i=0;i<document.forms[0].length;i++)

    {
        current = document.forms[0].elements[i];
        if (current.type == "text" || current.type == "select-one")
        {
            formValueArray[i] = current.value;
        }
    }
    return formValueArray;

}

function testValueChange() {

    valueChanged = false;

    current = new Array(document.forms[0].length);

    if(original.length != current.length)

    {
        valueChanged = true;
    }

     current = saveFormValue(current);
     for (var i=0;i<current.length;i++)

    {
        if(original[i] != current[i])
        {
           valueChanged = true;
        }
    }

    return valueChanged;
}

      ]]></script>
			</head>
			<body onload="init()" onkeypress="enterkey();">
				<!-- Header-->
				<xsl:call-template name="IncludeHeader"/>
        <!--Buttons-->
        	<div style="position: relative; top: 365px">
						<table width="98%" border="0" align="center" cellpadding="0" cellspacing="1">
							<tr>
								<td width="100%">
									<div align="center">
										<button class="BlueButton" name="gcc_submit_save" accesskey="S" onClick="javascript:saverecipientpage();">(S)ave</button>
									</div>
								</td>
								<td>
									<div align="right">
										<button class="BlueButton" name="gcc_submit_exit" accesskey="E" onclick="javascript:exitrecipientpage();">(E)xit</button>
									</div>
								</td>
							</tr>
						</table>
          <!--Copyright bar-->
					<xsl:call-template name="footer"/>
          </div>
        <!--Footer-->
        
				<!-- Display table-->
				<form method="post">
					<div id="content" style="display:block">
						<xsl:call-template name="security"/>
						<input type="hidden" name="gcc_company_id" value="{key('fields','gcc_company')/@value}"/>
						<input type="hidden" name="saved_request_number" value="{ROOT/@saved_request_number}"/>
						<input type="hidden" name="request_number_for_edit_recipient" value="{ROOT/@request_number_for_edit_recipient}"/>
						<input type="hidden" name="success_msg" value="{ROOT/@success_msg}"/>
						<input type="hidden" name="exception_error" value="{ROOT/@exception_error}"/>
						<input type="hidden" name="gcc_request_number" value="{key('fields','gcc_request_number')/@value}"/>
						<input type="hidden" name="gcc_type" value="{key('fields','gcc_type')/@value}"/>
						<input type="hidden" name="gcc_issue_amount" value="{key('fields','gcc_issue_amount')/@value}"/>
						<input type="hidden" name="gcc_paid_amount" value="{key('fields','gcc_paid_amount')/@value}"/>
						<input type="hidden" name="gcc_issue_date" value="{key('fields','gcc_issue_date')/@value}"/>
						<input type="hidden" name="gcc_expiration_date" value="{key('fields','gcc_expiration_date')/@value}"/>
						<input type="hidden" name="gcc_quantity" value="{key('fields','gcc_quantity')/@value}"/>
						<input type="hidden" name="gcc_format" value="{key('fields','gcc_format')/@value}"/>
						<input type="hidden" name="gcc_filename" value="{key('fields','gcc_filename')/@value}"/>
						<input type="hidden" name="gcc_requestor_name" value="{key('fields','gcc_requestor_name')/@value}"/>
						<input type="hidden" name="gcc_redemption_type" value="{key('fields','gcc_redemption_type')/@value}"/>
						<input type="hidden" name="gcc_program_name" value="{key('fields','gcc_program_name')/@value}"/>
						<input type="hidden" name="gcc_program_description" value="{key('fields','gcc_program_description')/@value}"/>
						<input type="hidden" name="gcc_comments" value="{key('fields','gcc_comments')/@value}"/>
						<input type="hidden" name="gcc_company" value="{key('fields','gcc_company')/@value}"/>
						<input type="hidden" name="gcc_source_code" value="{key('fields','gcc_source_code')/@value}"/>
						<input type="hidden" name="gcc_prefix" value="{key('fields','gcc_prefix')/@value}"/>
						<input type="hidden" name="gcc_other" value="{key('fields','gcc_other')/@value}"/>
						<input type="hidden" name="gcc_status_single" value="{key('fields','gcc_status_single')/@value}"/>
						<input type="hidden" name="gcc_redemption_date" value="{key('fields','gcc_redemption_date')/@value}"/>
						<input type="hidden" name="gcc_order_number" value="{key('fields','gcc_order_number')/@value}"/>
						<input type="hidden" name="gcc_status_multiple" value="{key('fields','gcc_status_multiple')/@value}"/>
						<input type="hidden" name="gcc_status_active" value="{key('fields','gcc_status_active')/@value}"/>
						<input type="hidden" name="gcc_status_redeemed" value="{key('fields','gcc_status_redeemed')/@value}"/>
						<input type="hidden" name="gcc_status_cancelled" value="{key('fields','gcc_status_cancelled')/@value}"/>
						<input type="hidden" name="gcc_status_reinstated" value="{key('fields','gcc_status_reinstated')/@value}"/>
						<input type="hidden" name="gcc_status_expired" value="{key('fields','gcc_status_expired')/@value}"/>
						<input type="hidden" name="gcc_coupon_number" value="{key('fields','gcc_coupon_number')/@value}"/>
						<input type="hidden" name="gcc_promotion_id" value="{key('fields','gcc_promotion_id')/@value}"/>
						<input type="hidden" name="coupon_quantity" value=""/>
						<input type="hidden" name="gcc_enable_create_cs" value="{key('fields','gcc_enable_create_cs')/@value}"/>
						<input type="hidden" name="gcc_creation_date" value="{key('fields','gcc_creation_date')/@value}"/>
						<input type="hidden" name="gcc_partner_program_name" value="{key('fields','gcc_partner_program_name')/@value}"/>
						
            <div align="justify" class="floatingdiv">
								<table align="center" cellspacing="1">
									<tr>
										<td>
												<table align="center" class="innerTable">
													<tr class="Label">
														<td width="4%">
															<div align="left"></div>
														</td>
														<td colspan="5">
															<div align="left">
																<xsl:value-of select="ROOT/@sub_header_name" />
																<xsl:value-of select="ROOT/@sub_header_value" />
															</div>
														</td>
														<xsl:choose>
															<xsl:when test="$gcc_action = 'display_add'">
																<td colspan="5" align="left">
																	Creation Date:
                                             <script type="text/javascript"><![CDATA[

                var d = new Date()
                 var zero = '0';
                 var month = d.getMonth() + 1;
                 var date = d.getDate()
                 if (month < 10)
                 {
                  month = zero + month;
                 }

                 if (date < 10)
                 {
                  date = zero + date;
                 }
                 document.write(month)
                document.write("/")
                 document.write(date)
                 document.write("/")
                 document.write(d.getFullYear())
                  ]]></script>
																</td>
															</xsl:when>
															<xsl:otherwise>
																<td colspan="5">
																	<div align="left">Creation Date: 
																		<xsl:value-of select="key('fields','gcc_creation_date')/@value"/>
																	</div>
																</td>
															</xsl:otherwise>
														</xsl:choose>
													</tr>
													<tr>
														<td colspan="13" class="TotalLine">
															<div align="left">
																<strong>Recipient Information </strong>
																<br/>
															</div>
														</td>
													</tr>
													<tr class="Label">
														<td align="center"></td>
														<td></td>
														<td width="14%" align="center">First Name</td>
														<td width="10%" align="center">Last Name</td>
														<td width="9%" align="center">Address 1</td>
														<td width="9%" align="center">Address 2</td>
														<td width="9%" align="center">City</td>
														<td align="center">State</td>
														<td width="9%" align="center">Zip Code</td>
														<td width="9%" align="center">Country</td>
														<td width="7%" align="center">Phone</td>
														<td width="11%" align="center">Email Address</td>
														<td width="11%" align="center">Issued Reference Number</td>
													</tr>
													<xsl:for-each select="ROOT/GC_COUPON_RECIPIENTS/GC_COUPON_RECIPIENT">
														<script>

                       totalNumber ++;

														</script>
														<tr id="tr_{@num}" class="Label">
															<td align="center" id="i">
																<xsl:value-of select="@num"/>
															</td>
															<td class="Label">
																<span id="required_error_{@num}" class="ErrorMessage" style="display:none">Required</span>
															</td>
															<td align="center" valign="middle">
																<input type="text" size="20" maxlength="20" name="first_name_{@num}" id="first_name_{@num}" value="{first_name}"/>
															</td>
															<td align="center" valign="middle">
																<input type="text" size="20" maxlength="20" name="last_name_{@num}" id="last_name_{@num}" value="{last_name}"/>
															</td>
															<td align="center" valign="middle">
																<input type="text" size="45" maxlength="45" name="address_1_{@num}" id="address_1_{@num}" value="{address_1}"/>
															</td>
															<td align="center" valign="middle">
																<input type="text" size="45" maxlength="45" name="address_2_{@num}" id="address_2_{@num}" value="{address_2}"/>
															</td>
															<td align="center" valign="middle">
																<input type="text" size="30" maxlength="30" name="city_{@num}" id="city_{@num}" value="{city}"/>
															</td>
																<xsl:variable name="state" select="state" />
                                                                <td align="center" valign="middle">
																<select name="state_{@num}" id="state_{@num}">
																	<option value="none" selected="selected"></option>
																	<xsl:for-each select="/ROOT/STATE_MASTERS/STATE_MASTER">
																		<xsl:choose>
																			<xsl:when test="statemasterid = $state">
																				<option value="{statemasterid}" selected="selected">
																					<xsl:value-of select="statemasterid" />
																				</option>
																 			</xsl:when>
																			<xsl:otherwise>
																				<option value="{statemasterid}">
																					<xsl:value-of select="statemasterid" />
																				</option>
																			</xsl:otherwise>
																		</xsl:choose>
																	</xsl:for-each>
																</select>
															</td>
															<td align="center" valign="middle">
																<input type="text" size="12" maxlength="12" name="zip_{@num}" id="zip_{@num}" value="{zip_code}"/>
															</td>
															<xsl:variable name="country" select="country" />
															<td align="center" valign="middle">
																<select name="country_{@num}" id="country_{@num}">
																	<option value="none" selected="selected"></option>
																	<xsl:for-each select="/ROOT/COUNTRY_MASTERS/COUNTRY_MASTER">
																		<xsl:choose>
																			<xsl:when test="country_id = $country">
																				<option value="{country_id}" selected="selected">
																					<xsl:value-of select="name" />
																				</option>
																			</xsl:when>
																			<xsl:otherwise>
																				<option value="{country_id}">
																					<xsl:value-of select="name" />
																				</option>
																			</xsl:otherwise>
																		</xsl:choose>
																	</xsl:for-each>
																</select>
															</td>
															<td align="center" valign="middle">
																<input type="text" size="20" maxlength="20" name="phone_{@num}" id="phone_{@num}" value="{phone}"/>
															</td>
															<td align="center" valign="middle">
																<input type="text" size="55" maxlength="55" name="email_{@num}" id="email_{@num}" value="{email_address}" onchange="checkEmailAddress();" />
															</td>
															<td align="center" valign="middle">
																<input type="text" size="20" maxlength="20" name="issuedReferenceNumber_{@num}" id="issuedReferenceNumber_{@num}" value="{issuedReferenceNumber}"/>
															</td>
														</tr>
														<tr>
															<td></td>
															<td></td>
															<td></td>
															<td></td>
															<td></td>
															<td></td>
															<td></td>
															<td></td>
															<td></td>
															<td></td>
															<td></td>
															<td class="Label">
																<span id="error_email_{@num}" class="ErrorMessage" style="display:none">Incorrect Format</span>
															</td>
															<td></td>
														</tr>
														<input type="hidden" name="gc_coupon_recip_id_{@num}" value="{gc_coupon_recip_id}"/>
													</xsl:for-each>
													<input type="hidden" id="number" name="number" value="" />
													<script>

                      test();

													</script>
													<tr>
														<td colspan="11" align="center">
															<p>&nbsp;</p>
															<p>&nbsp;</p>
														</td>
													</tr>
												</table>
										</td>
									</tr>
								</table>
                </div><!--end of floatingdiv-->
                </div><!--end of contentdiv-->
					
					<!--  Used to dislay Processing... message to user-->
					<div id="waitDiv" style="display:none">
						<table id="waitTable" class="mainTable" width="98%" height="100%" align="center" cellpadding="30" cellspacing="1">
							<tr>
								<td width="100%">
									<table width="100%" cellspacing="0" cellpadding="0" border="0">
										<tr>
											<td id="waitMessage" align="right" width="50%" class="waitMessage" />
											<td id="waitTD" width="50%" class="waitMessage" />
										</tr>
									</table></td>
							</tr>
						</table>
					</div>
				</form>
			</body>
		</html>
	</xsl:template>
	<!--to get the header title-->
	<xsl:template name="IncludeHeader">
		<xsl:call-template name="header">
            <xsl:with-param name="showRecipientHeader" select="true()"/>
		</xsl:call-template>
	</xsl:template>
	<!--end-->
</xsl:stylesheet>