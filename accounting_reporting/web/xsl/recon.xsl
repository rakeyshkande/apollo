<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="header_cbr.xsl"/>
	<xsl:import href="footer.xsl"/>
	<xsl:import href="printRecon.xsl"/>
	<xsl:output method="html" indent="yes"/>
	<xsl:template match="/">
		<html>
			<head>
				<META http-equiv="Content-Type" content="text/html"/>
      	<title>Reconcillation Screen</title>
				<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
				<script type="text/javascript" src="js/clock.js"></script>
				<script type="text/javascript" src="js/recon.js"></script>
        <script type="text/javascript" src="js/common_cbr.js"></script>
			  <script type="text/javascript">

					<![CDATA[
function addclass()
{
document.getElementById('messagediv').style.height = '80px';
document.getElementById('messagediv').style.overflow = 'auto';
}				
]]></script>
			</head>
			<body onload="setFocus(), addclass();">
			<form name="form" method="post" action="">
				<!-- Header-->
				<xsl:call-template name="IncludeHeader"/>
        <table align="center" width="98%" border="0" cellpadding="0">
        		<tr>
        		<td>
              <input type="image" align="right" src="images/printer.jpg" width="30" height="30" onclick="printIt()"/>
            </td>
       			</tr>
        	</table>
				<xsl:call-template name="IncludeRecon"/>

				<table width="98%" border="0" align="center" cellpadding="0" cellspacing="1">
					<tr>
						<td width="91%" rowspan="2">
						</td>
						<td align="right">
							<button style="width:100px" name="back" class="BlueButton" accesskey="B" onclick="doBackAction();">(B)ack</button>
						</td>
					</tr>
					<tr>
						<td align="right">
							<button style="width:100px" name="main" class="BlueButton" accesskey="M" onclick="doMainMenu();">(M)ain Menu</button>
						</td>

					</tr>
				</table>
				<!--Copyright bar-->
				<xsl:call-template name="footer"/>
				</form>
			</body>
		</html>
	</xsl:template>
	<!--to get the header title-->
	<xsl:template name="IncludeHeader">
		<xsl:call-template name="header">
			<xsl:with-param name="headerName" select="'Reconciliation Screen'"/>
			<xsl:with-param name="showBackButton" select="true()"/>
		</xsl:call-template>
	</xsl:template>
	<!--to get the recon main screen-->
<xsl:template name="IncludeRecon">
<xsl:call-template name="recon">
<!--xsl:with-param name="showprinter" select="true()"/-->
</xsl:call-template>
</xsl:template>
	<!--end-->
</xsl:stylesheet>