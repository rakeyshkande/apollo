<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="securityAndData.xsl"/>
<xsl:output method="html" indent="yes"/>
<xsl:template name="recon" match="/">
<!--xsl:param name="showprinter"/-->
    <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
    <script type="text/javascript" src="js/recon.js"/>
    <script type="text/javascript" language="javascript">
        <![CDATA[

     function printPage()
     {
     window.print()
     window.close()
     }


    ]]>
    </script>
			<!-- Display table-->
			<xsl:call-template name="securityanddata"/>
			<body onload="highlightRow(), printPage()">

   <table width="98%" align="center" cellspacing="1" class="mainTable">
      <tr>
        <td>
            <table width="100%" align="center" cellspacing="0" class="innerTable">
               <tr>
                 <td colspan="7" align="center">
                 <div align="left"><span class="Label">Order Number :</span>&nbsp;<xsl:value-of select="root/external_order_number"/></div></td>
              </tr>
               <tr>
                  <td colspan="7" align="center" class="TotalLine"><div align="center"><strong>Order Messages (Final Price Displayed) </strong><br/>
                   </div></td>
                 <div align="left"></div></tr>
               <tr class="Label">
                 <td width="11%" align="center">Message<br/>Type</td>
                 <td width="11%" align="center">Message<br/>Date</td>
                 <td width="10%" align="center">Price</td>
                 <td width="15%" align="center">Filler ID</td>
                 <td width="14%" align="center">Mercury or<br/>Venus ID</td>
                 <td width="10%" align="center">Reconciled Date</td>
                 <td width="17%" align="center">Reconciled<br/>Amount</td>
               </tr>
               <tr class="Label">
                 <td colspan="7" align="center"><hr/></td>
              </tr>
               <tr class="Label">
                 <td colspan="7" align="center">
			<div id="messagediv" align="center" class="innerTable">
			<table id="Merc" width="99%" cellpadding="0" cellspacing="0">
			<xsl:for-each select="root/messages/message">
            <tr>
            <td width="11%"><div align="center"><xsl:value-of select="msg_type"/></div></td>
            <td width="11%"><div align="center"><xsl:value-of select="created_on"/></div></td>
            <td width="10%"><div align="center"><xsl:value-of select="price"/></div></td>
            <td width="15%"><div align="center"><xsl:value-of select="florist_vendor_id"/></div></td>
            <td width="14%"><div align="center"><xsl:value-of select="merc_venus_id"/></div></td>
            <td width="10%"><div align="center"><xsl:value-of select="reconciled_date"/></div></td>
            <td width="17%"><div align="center">$<xsl:value-of select="format-number(amount_reconciled, '#,###,##0.00')"/></div></td>
            </tr>
            </xsl:for-each>
              </table>
			</div>
                </td> </tr>
               <tr class="Label">
                 <td colspan="7" align="center"/> </tr>
               <tr>
                 <td colspan="7" align="center">
                   <p>&nbsp;</p>
               
              <!--xsl:if test="$showprinter">
				<p align="right">
				<img src="images/printer.jpg" width="30" height="30" onclick="printIt()"/>
				</p>
				</xsl:if-->
              </td>
               </tr>
		  </table>
  		</td>
     </tr>
</table>
 </body>
</xsl:template>
</xsl:stylesheet>