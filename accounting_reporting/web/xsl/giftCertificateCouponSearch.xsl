
<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
	<xsl:import href="security.xsl"/>
	<xsl:key name="field" match="ROOT/FIELDS/FIELD" use="@fieldname" />
	<xsl:key name="error" match="ROOT/ERRORS/ERROR" use="@fieldname" />
	<xsl:variable name="error" select="ROOT/ERRORS/ERROR/@fieldname" />
    <xsl:output method="html" indent="yes"/>
    <xsl:template match="/">
	<html>
   <head>
      <META http-equiv="Content-Type" content="text/html"/>
      <title>Gift Certificate Coupons Maintenance</title>
      <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
      <script type="text/javascript" src="js/clock.js"></script>
      <script type="text/javascript" src="js/util.js"/>
      <script type="text/javascript" src="js/common_functions_all.js"/>
      <script type="text/javascript">
      var keypress = "0";
      
<![CDATA[

/************************
initial function
************************/

function init()
{
document.getElementById('gcc_request_number').focus();
document.getElementById('gcc_submit_search').disabled= true;
}

/**********************************************************************
Search Action when the Enter key is pressed
**********************************************************************/

function doSearchAction()
{
var requestField = document.getElementById("gcc_request_number").value;
var couponField = document.getElementById("gcc_coupon_number").value;
var keypress = window.event.keyCode;

  if (keypress != 13)
  return false;
  else
    {
      if(requestField == '' && couponField == '')
      return false;
      else
      {
      search()
      }
    }
}

function search()
{
var requestField = document.getElementById("gcc_request_number").value;
var couponField = document.getElementById("gcc_coupon_number").value;

if ( requestField == '' && couponField == '')
return false;
       else
       {
       document.getElementById("gcc_action").value = 'search';
       doSearchGiftCertAction();
       }
}

/*****************************
The common function
********************************/

function doSearchGiftCertAction()
{
document.forms[0].action = "SearchGiftCertAction.do?gcc_user=" + document.forms[0].gcc_user.value + "&securitytoken=" + document.forms[0].securitytoken.value + "&context=" + document.forms[0].context.value;
document.forms[0].submit();
}

/****************************************************
the function for disabling and enabling of search button
*****************************************************/
function enableSearch()
{
var requestField = document.getElementById("gcc_request_number").value;
var couponField = document.getElementById("gcc_coupon_number").value;

if ( (requestField != null && requestField != '') || (couponField != null && couponField != '') )
  {
  document.getElementById('gcc_submit_search').disabled= false;
  }
  else if ( requestField == '' && couponField == '')
  {
  document.getElementById('gcc_submit_search').disabled= true;
  }
}

/**********************************************************************
 the function for disabling and enabling of fields
**********************************************************************/

function disableField()
{
var requestField = document.getElementById("gcc_request_number").value;
var couponField = document.getElementById("gcc_coupon_number").value;

     if ( (requestField == null || requestField == '' ) && (couponField == null || couponField == '' ) )
     {
       document.getElementById('gcc_coupon_number').style.backgroundColor= '#FFFFFF';
       document.getElementById('gcc_coupon_number').disabled= false;
       document.getElementById('gcc_request_number').style.backgroundColor= '#FFFFFF';
       document.getElementById('gcc_request_number').disabled= false;
     }
     else if (requestField != null && requestField != '' )
     {
       document.getElementById('gcc_coupon_number').disabled= true;
       document.getElementById('gcc_coupon_number').style.backgroundColor= '#eeeeee';
     }
     else if (couponField != null && couponField != '' )
     {
      document.getElementById('gcc_request_number').disabled= true;
      document.getElementById('gcc_request_number').style.backgroundColor= '#eeeeee';
     }

}


]]>
</script>
   </head>
<body onload="init();" onkeyup = "doSearchAction(), enableSearch(), disableField();">

<!-- Header-->

<xsl:call-template name="IncludeHeader"/>
<!-- Display table-->
<form method="post">
<div id="content" style="display:block">
<xsl:call-template name="security"/>
   <table width="98%" align="center" cellspacing="1" class="mainTable">
        <td><table width="100%" align="center" class="innerTable">
 
                       <tr>
                  <td colspan="5" align="center" class="TotalLine"><div align="left"><strong>Gift Certificate / Coupon Information</strong><br/></div></td>
                  <div align="left"></div>
               </tr>
               <tr class="Label">
                 <td colspan="5" align="center">
                 <p>&nbsp;</p>
                 <p>&nbsp;</p>
                 </td>
               </tr>
               
               <tr class="Label">
                 <td width="20%">
                   <div align="right">Request Number:</div></td>
                   <td width="15%"><div align="left">
                   <input type="text" value="" tabindex="1" size="20" maxlength="20" id="gcc_request_number" name="gcc_request_number" />
                   </div>
                   </td>
                 
                 <td width="30%"><div align="right">Gift Certificate / Coupon Number  :</div></td>
                 <td width="15%"><div align="left" class="Label">
                  <input type="text" value="" tabindex="2" size="20" maxlength="20" id="gcc_coupon_number" name="gcc_coupon_number" />
                   </div>
                   </td>
                  </tr>
                   <tr class="Label">
                   <td></td>
                   <td>
                   <xsl:if test="$error = 'gcc_request_number'">
						<div align="left">
							<span id="server_error" class="ErrorMessage" style="display:block">
								<xsl:value-of select="key('error','gcc_request_number')/@value" />
							</span>
						</div>
					</xsl:if>
                   </td>
                   <td></td>
                   <td><xsl:if test="$error = 'gcc_coupon_number'">
						<div align="left">
							<span id="server_error" class="ErrorMessage" style="display:block">
								<xsl:value-of select="key('error','gcc_coupon_number')/@value" />
							</span>
						</div>
					</xsl:if>
					</td>

                    </tr>
               <tr class="Label">
                 <td colspan="5" align="center">
                   <p>&nbsp;</p>
                   <p>&nbsp;</p>
                   <p>&nbsp;</p>
                   <p>&nbsp;</p>
                   <p>&nbsp;</p>
                   <p>&nbsp;</p>
                   <p>&nbsp;</p>
                   </td>
               </tr>
               <tr class="Label">
                 <td colspan="5" align="center">
                  <button class="BlueButton" name="gcc_submit_search" tabindex="3" accesskey="S" onclick="search();">(S)earch</button>
                 </td></tr>

		  </table>
  		</td>

   </table>

   <table width="98%" border="0" align="center" cellpadding="0" cellspacing="1">
      <tr>
         <td width="91%"><div align="center">
         </div></td>
         <td width="9%" align="right">
         <button class="BlueButton" name="gcc_submit_exit" tabindex="4" accesskey="E" onclick="exit();">(E)xit</button>
         </td>
      </tr>
   </table>
   </div>
   <!--  Used to dislay Processing... message to user-->
					<div id="waitDiv" style="display:none">
						<table id="waitTable" class="mainTable" width="98%" height="100%" align="center" cellpadding="30" cellspacing="1">
							<tr>
								<td width="100%">
									<table width="100%" cellspacing="0" cellpadding="0" border="0">
										<tr>
											<td id="waitMessage" align="right" width="50%" class="waitMessage" />
											<td id="waitTD" width="50%" class="waitMessage" />
										</tr>
									</table></td>
							</tr>
						</table>
					</div>

<!--Copyright bar-->
<xsl:call-template name="footer"/>
</form>
</body>
</html>
	</xsl:template>
	<!--to get the header title-->

<xsl:template name="IncludeHeader">
<xsl:call-template name="header">
<xsl:with-param name="showheader" select="true()"/>
<xsl:with-param name="headerName" select="'Gift Certificates / Coupons Maintenance'"/>
</xsl:call-template>
</xsl:template>
<!--end-->
</xsl:stylesheet>