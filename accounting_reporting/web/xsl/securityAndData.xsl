
<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:param name="securitytoken"/>
	<xsl:param name="context"/>
	<xsl:param name="cbr_action"/>
	<xsl:param name="order_guid"/>
	<xsl:param name="order_detail_id"/>
	<xsl:param name="external_order_number"/>
	<xsl:param name="master_order_number"/>
	<xsl:param name="adminAction"/>
	<xsl:param name="applicationcontext"/>
	<xsl:param name="sc_cust_ind"/>
	<xsl:param name="sc_tracking_number"/>
	<xsl:param name="sc_cust_number"/>
	<xsl:param name="sc_phone_number"/>
	<xsl:param name="sc_rewards_number"/>
	<xsl:param name="sc_cc_number"/>
	<xsl:param name="sc_recip_ind"/>
	<xsl:param name="sc_order_number"/>
	<xsl:param name="sc_email_address"/>
	<xsl:param name="sc_zip_code"/>
	<xsl:param name="sc_last_name"/>
	<xsl:param name="call_cs_number"/>
	<xsl:param name="call_type_flag"/>
	<xsl:param name="call_brand_name"/>
	<xsl:param name="call_dnis"/>
	<xsl:param name="t_call_log_id"/>
	<xsl:param name="t_entity_history_id"/>
	<xsl:param name="t_comment_origin_type"/>
	<xsl:param name="start_origin"/>
	<xsl:param name="uo_display_page"/>
	
	<xsl:output method="html" indent="yes"/>
	<xsl:template name="securityanddata">
	
		<!--recon charge back-->
		<input type="hidden" name="master_order_number" value="{$master_order_number}"/>
		<input type="hidden" name="cbr_action" value="{$cbr_action}"/>
		<input type="hidden" name="order_guid" value="{$order_guid}"/>
		<input type="hidden" name="order_detail_id" value="{$order_detail_id}"/>
		<input type="hidden" name="external_order_number" value="{$external_order_number}"/>
		<!--security-->
		<input type="hidden" name="securitytoken" id="securitytoken" value="{$securitytoken}"/>
		<input type="hidden" name="context" id="context" value="{$context}"/>
		<input type="hidden" name="applicationcontext" id="applicationcontext" value="{$applicationcontext}"/>
		<input type="hidden" name="adminAction" id="adminAction" value="{$adminAction}"/>
		<!-- searchCriteria -->
		<input type="hidden" name="sc_cust_ind" 			      id="sc_cust_ind" 		        value="{$sc_cust_ind}" />
		<input type="hidden" name="sc_tracking_number"      id="sc_tracking_number" 	  value="{$sc_tracking_number}" />
		<input type="hidden" name="sc_cust_number" 			    id="sc_cust_number" 		    value="{$sc_cust_number}" />
		<input type="hidden" name="sc_phone_number" 		    id="sc_phone_number" 		    value="{$sc_phone_number}" />
		<input type="hidden" name="sc_rewards_number" 	    id="sc_rewards_number" 		  value="{$sc_rewards_number}" />
		<input type="hidden" name="sc_cc_number" 			      id="sc_cc_number" 		      value="{$sc_cc_number}" />
		<input type="hidden" name="sc_recip_ind" 			      id="sc_recip_ind" 		      value="{$sc_recip_ind}" />
		<input type="hidden" name="sc_order_number" 		    id="sc_order_number" 		    value="{$sc_order_number}" />
		<input type="hidden" name="sc_email_address" 		    id="sc_email_address" 		  value="{$sc_email_address}" />
		<input type="hidden" name="sc_zip_code" 			      id="sc_zip_code" 		        value="{$sc_zip_code}" />
		<input type="hidden" name="sc_last_name" 			      id="sc_last_name" 		      value="{$sc_last_name}" />
		<!-- header.xsl -->
		<input type="hidden" name="call_cs_number" 			    id="call_cs_number" 		    value="{$call_cs_number}"/>
		<input type="hidden" name="call_type_flag" 			    id="call_type_flag" 		    value="{$call_type_flag}"/>
		<input type="hidden" name="call_brand_name" 		    id="call_brand_name" 		    value="{$call_brand_name}"/>
		<input type="hidden" name="call_dnis" 				      id="call_dnis" 			        value="{$call_dnis}"/>
		<!-- call log-->
		<input type="hidden" name="t_call_log_id" 			    id="t_call_log_id" 		      value="{$t_call_log_id}"/>
		<!-- timer -->
		<input type="hidden" name="t_entity_history_id" id="t_entity_history_id" value="{$t_entity_history_id}"/>
		<input type="hidden" name="t_comment_origin_type" id="t_comment_origin_type" value="{$t_comment_origin_type}"/>
		<!-- start origin -->
		<input type="hidden" name="start_origin" id="start_origin" value="{$start_origin}"/>
		<!-- Update Order info -->
		<input type="hidden" name="uo_display_page" id="uo_display_page" value="{$uo_display_page}"/>
	</xsl:template>
</xsl:stylesheet>