<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:param name="securitytoken"/>
	<xsl:param name="context"/>
	<xsl:param name="is_multiple_gcc"/>
	<xsl:param name="gcc_action"/>
	<xsl:param name="gcc_user"/>
	<xsl:param name="coupon_quantity"/>
	<xsl:param name="report_main_menu"/>
  	<xsl:output method="html" indent="yes"/>
	<xsl:template name="security" match="/">
	    <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
	    <input type="hidden" name="context" value="{$context}"/>
		<input type="hidden" name="is_multiple_gcc" value="{$is_multiple_gcc}"/>
		<input type="hidden" name="gcc_action" value="{$gcc_action}"/>
		<input type="hidden" name="gcc_user" value="{$gcc_user}"/>
		<input type="hidden" name="report_main_menu" id="report_main_menu" value="{$report_main_menu}"/>
	</xsl:template>	
</xsl:stylesheet>