<?xml version='1.0' encoding='windows-1252'?>
<!DOCTYPE FTD[
    <!ENTITY space "&#160;">
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="header_tax.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:param name="securitytoken"/>
<xsl:param name="context"/>
<xsl:key name="pagedata" match="root/pageData/data" use="@name" />
<xsl:variable name="taxAdminURL" select="root/pageData/data[@name='taxAdminURL']/@value"/>
<xsl:variable name="siteName" select="root/pageData/data[@name='siteName']/@value"/>
<xsl:template match="/">
<html>
<head>
<title>
        Tax Admin Settings
</title>
<META http-equiv="Content-Type" content="text/html"/>
<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
<!-- <style type="text/css">
#sourceTypeListAllDiv {display: block;}
#sourceTypeListTempDiv {display: none;}
select.fixedWidthSelect { width:300px } 
</style> -->

<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/clock.js"></script>

</head>
<body OnLoad="javascript:init()">
<xsl:call-template name="header">
        <xsl:with-param name="headerName" select="'Tax Admin Settings'" />
        <xsl:with-param name="showTime" select="true()"/>
        <xsl:with-param name="showBackButton" select="false()"/>
        <xsl:with-param name="showPrinter" select="false()"/>
        <xsl:with-param name="showSearchBox" select="false()"/>
      </xsl:call-template>
	<form target="taxRules" name="theform" method="post" action="">
        <!-- <div id="content" style="display:block">
		   <div id="resultMsgDiv">	   
       		</div>
        </div> -->
        <table align="center" width="98%" border="3" cellpadding="1" cellspacing="1" bordercolor="#006699">
        <tr>
        	<td>
        	<table align = "center" width="80%">
				<xsl:if test="key('pagedata','taxAdminURL')/@value"> 
					<iframe name = "taxRules" src="{key('pagedata','taxAdminURL')/@value}" width="100%" height="500" scrolling="auto"></iframe>					
				 </xsl:if>		        	
                </table>
        	</td>
        </tr>
    </table>
 	<table width="98%" border="0" align="center" cellpadding="2" cellspacing="2">
      <tr>
        <td width="10%"></td>
        <td width="80%" align="center">
        <br />
        <a class="BlueButton" href="http://{$siteName}/secadmin/security/Main.do?action=load&amp;context={$context}&amp;adminAction=accounting&amp;securitytoken={$securitytoken}" style="padding:3px;color:#000">Back</a>&nbsp;
        <a class="BlueButton" href="http://{$siteName}/secadmin/security/Main.do?context={$context}&amp;securitytoken={$securitytoken}" style="padding:3px;color:#000">Main Menu</a>&nbsp;
        </td>
      </tr>
    </table>        
      </form>

<xsl:call-template name="footer"></xsl:call-template>
</body>
</html>  
</xsl:template>
  </xsl:stylesheet>