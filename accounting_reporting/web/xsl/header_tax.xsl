
<!DOCTYPE ACDemo [
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" indent="yes"/>
	<xsl:template name="header" match="/">
		<xsl:param name="headerName"/>
		<xsl:param name="buttonName"/>


		<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
		<script type="text/javascript" src="js/clock.js"/>
		<script type="text/javascript" src="js/reporting.js"/>
		<script type="text/javascript" src="js/util.js"/>
		<script type="text/javascript" language="javascript"><![CDATA[
    ]]>
		</script>
		<table width="98%" border="0" align="center" cellpadding="0" cellspacing="1">
			<tr>
				<td rowspan="2" width="25%" align="left">
					<img border="0" src="images/logo.gif" align="absmiddle"/>
				</td>
				<td width="54%" align="center" colspan="1" class="header">
					<xsl:value-of select="$headerName"/>
				</td>
				<td width="21%" align="right" id="time" class="Label"></td>
				<script type="text/javascript">startClock();</script>
			</tr>
			<tr>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td colspan="3">
					<hr/>
				</td>
			</tr>
			<tr>
				<td colspan="3" align="right">
				    <xsl:if test="$buttonName !=''">
						<xsl:choose>
							<xsl:when test="$buttonName='back_button'">
								<button class="BlueButton" name="back_button" id="back_button" accesskey="B" onclick="javascript:doReportingBack();">(B)ack</button>
							</xsl:when>
							<xsl:when test="$buttonName='back_confirm'">
								<button class="BlueButton" name="back_button" id="back_button" accesskey="B" onclick="javascript:doReportingConfirmBack();">(B)ack</button>
							</xsl:when>
							<xsl:otherwise>
								<button class="BlueButton" name="main_menu" accesskey="M" onclick="javascript:doReportingMainMenu();">(M)ain Menu</button>
							
							</xsl:otherwise>
						</xsl:choose>
					</xsl:if>
				</td>
			</tr>
		</table>
	</xsl:template>
</xsl:stylesheet>