<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="header_cbr.xsl"/>
	<xsl:import href="footer.xsl"/>
	<xsl:import href="printBilling.xsl"/>
	<xsl:output method="html" indent="yes"/>
	<xsl:key name="buttontype" match="root/buttons/button" use="@name" />
	<xsl:template match="/">
		<html>
			<head>
				<META http-equiv="Content-Type" content="text/html"/>
				<title>Billing Transactions Screen</title>
				<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
				<script type="text/javascript" src="js/clock.js"></script>
				<script type="text/javascript" src="js/billing.js"></script>
        <script type="text/javascript" src="js/common_cbr.js"></script>
			    <script type="text/javascript">
var popup = '<xsl:value-of select="root/popup/@display"/>';
var ord_num = '<xsl:value-of select="root/master_order_number"/>';
					<![CDATA[
function addclass()
{
 document.getElementById('billingdiv').style.height = '80px';
 document.getElementById('billingdiv').style.overflow = 'auto';
}


]]></script>
			</head>
			<body onload="setFocus(), highlightRow(),addclass(), displaypopup();">
			<form name="form" method="post" action="">
			<!-- Header-->
				<xsl:call-template name="IncludeHeader"/>
        	<table align="center" width="98%" border="0" cellpadding="0">
        		<tr>
        		<td>
            		<input type="image" align="right" src="images/printer.jpg" width="30" height="30" onclick="printIt()"/>
       			</td>
       			</tr>
        	</table>
        	
        
				<xsl:call-template name="IncludeBilling"/>
        <xsl:variable name="addCBMsg" select="key('buttontype','add_chargeback')/@message"/>
        <xsl:variable name="addRTMsg" select="key('buttontype','add_retrieval')/@message"/>
        
				<table width="98%" border="0" align="center" cellpadding="0" cellspacing="1">
					<tr>
						<td width="91%" rowspan="2">
              
							<button style="width:100px"  name="{key('buttontype','add_chargeback')/@name}" id="add_chargeback" class="bigBlueButton" accesskey="C" onclick="addChargeBack('{$addCBMsg}');">Add
								<br/>(C)harge Back
							</button>
							<button style="width:100px" name="{key('buttontype','add_retrieval')/@name}" id="add_retrieval" class="bigBlueButton" accesskey="R" onclick="addRetrieval('{$addRTMsg}');">Add
								<br/>(R)etrieval
							</button>
							<div align="center"></div>
						</td>
						<td align="right">
							<button style="width:100px" name="back" id="back" class="BlueButton" accesskey="B" onclick="doBackAction();">(B)ack</button>
						</td>
					</tr>
					<tr>
						<td align="right">
							<button style="width:100px" name="main" id="main" class="BlueButton" accesskey="M" onclick="doMainMenu();">(M)ain Menu</button>
						</td>

					</tr>
				</table>
				<!--Copyright bar-->
				<xsl:call-template name="footer"/>
				</form>
			</body>
		</html>
	</xsl:template>
	<!--to get the header title-->
	<xsl:template name="IncludeHeader">
		<xsl:call-template name="header">
			<xsl:with-param name="headerName" select="'Billing Transactions Screen'"/>
			<xsl:with-param name="showBackButton" select="true()"/>
		</xsl:call-template>
	</xsl:template>
	<!--to get the recon main screen-->
<xsl:template name="IncludeBilling">
<xsl:call-template name="billing">
<!--xsl:with-param name="showprinter" select="'true'"/-->
</xsl:call-template>
</xsl:template>
	<!--end-->
</xsl:stylesheet>