<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="header_cbr.xsl"/>
	<xsl:import href="footer.xsl"/>
	<xsl:import href="securityAndData.xsl"/>
	<xsl:output method="html" indent="yes"/>
	<xsl:variable name="cbrreason" select="root/chargeback_retrieval/reason_code"/>
	<xsl:variable name="cbrtype" select="root/chargeback_retrieval/type_code"/>
	<xsl:template match="/">
		<html>
			<head>
				<META http-equiv="Content-Type" content="text/html"/>
				<title>
					<xsl:value-of select="root/header"/>
				</title>
				<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
				<link rel="stylesheet" type="text/css" href="css/calendar.css"/>
				<script type="text/javascript" src="js/clock.js"/>
				<script type="text/javascript" src="js/FormChek.js"/>
				<script type="text/javascript" src="js/calendar.js"/>
				<script type="text/javascript" src="js/date.js"/>
				<script type="text/javascript" src="js/cb_ret.js"/>
				<script type="text/javascript" src="js/common.js"/>
				<script type="text/javascript">
	var popup = '<xsl:value-of select="root/@displayMessage"/>';
	var cbrtype = '<xsl:value-of select="root/chargeback_retrieval/type_code"/>';
	var cbrid = '<xsl:value-of select="root/chargeback_retrieval/cbr_id"/>';
    var amdis = '<xsl:value-of select="root/chargeback_retrieval/amount/@disabled"/>';
    var revdis = '<xsl:value-of select="root/chargeback_retrieval/reversal_date/@disabled"/>';
 <![CDATA[

function init()
{
    original = new Array(document.forms[0].length);
    original = saveFormValue(original);
}

function saveFormValue(formValueArray)
{
    for (var i=0;i<document.forms[0].length;i++)
    {
        current = document.forms[0].elements[i];
        if (current.type == "text" || current.type == "select-one")
        {
            formValueArray[i] = current.value;
        }
    }
     return formValueArray;
}

// Call this when Exit button is clicked.
// Returns true if any value on the form has changed.
// Returns false otherwise.

function testValueChange()
{
    valueChanged = false;
    current = new Array(document.forms[0].length);
    if(original.length != current.length)
    {
        valueChanged = true;
    }
     current = saveFormValue(current);
     for (var i=0;i<current.length;i++)

    {
        if(original[i] != current[i])
        {
            valueChanged = true;
        }
    }
    return valueChanged;
}

  ]]>
				</script>
			</head>
			<body onload="init(), highlightRow(), disableAmount(), disableRevDate();">
				<form name="form" method="post">
					<input type="hidden" name="cbr_id" id="cbr_id" value=""/>
					<!-- Header-->
					<xsl:call-template name="IncludeHeader"/>
					<xsl:call-template name="securityanddata"/>
					<!-- Display table-->
					<table width="98%" align="center" cellspacing="1" class="mainTable">
						<tr>
							<td>
								<table align="center" width="100%" class="innerTable">
									<tr>
										<td colspan="5">
											<div align="left">
												<span class="Label">Shopping Cart Number:</span>&nbsp;
												<xsl:value-of select="$master_order_number"/>
											</div>
										</td>
									</tr>
									<tr>
										<td colspan="5" align="center" class="TotalLine">
											<b>
												<xsl:value-of select="root/subheader"/>
											</b>
										</td>
									</tr>
									<tr class="Label">
										<td width="15%">
											<div align="right">Date Received: </div>
										</td>
										<td width="25%">
											<div align="left">
												<input type="text" id="date_rcvd" name="date_rcvd" size="25" maxlength="10" tabindex="1" value="{root/chargeback_retrieval/received_date}"/>
												<img id="date_rcvd_img" src="images/calendar.gif" width="16" height="16" tabindex="2"/>
											</div>
											<div>
												<span id="span_date_rcvd_error" class="ErrorMessage" style="display:none">Invalid Date</span>
											</div>
											<div>
												<span id="span_date_rcvd" class="ErrorMessage" style="display:none">Required</span>
											</div>
										</td>
										<td colspan="2" width="30%">
											<div align="right">Due Date : </div>
										</td>
										<td width="25%">
											<div align="left">
												<input type="text" id="date_due" name="date_due" size="25" maxlength="10" tabindex="5" value="{root/chargeback_retrieval/due_date}"/>
												<img id="date_due_img" name="date_due_img" src="images/calendar.gif" width="16" height="16" tabindex="6"/>
											</div>
											<div>
												<span id="span_date_due_error" class="ErrorMessage" style="display:none">Invalid Date</span>
											</div>
										</td>
									</tr>
									<tr class="Label">
										<td>
											<div align="right">Amount: </div>
										</td>
										<td>
											<div align="left">
												<input type="text" id="amount" name="amount" size="25" maxlength="8" tabindex="3" value="{root/chargeback_retrieval/amount}"/>
											</div>
										</td>
										<xsl:choose>
											<xsl:when test="$cbrtype = 'CB'">
												<td colspan="2">
													<div align="right">Reversal  Date : </div>
												</td>
												<td>
													<div align="left">
														<input type="text" id="date_rvsl" name="date_rvsl" size="25" maxlength="10" tabindex="7" value="{root/chargeback_retrieval/reversal_date}"/>
														<img id="date_rvsl_img" name="date_rvsl_img" src="images/calendar.gif" width="16" height="16" tabindex="8"/>
													</div>
												</td>
												<script type="text/javascript">
  Calendar.setup(
    {
      inputField  : "date_rvsl",  // ID of the input field
      ifFormat    : "mm/dd/y",  // the date format
      button      : "date_rvsl_img"  // ID of the button
    }
  );
												</script>
											</xsl:when>
											<xsl:otherwise>
												<td colspan="2">
													<div align="right">Date Returned : </div>
												</td>
												<td>
													<div align="left">
														<input type="text" id="date_rtrn" name="date_rtrn" size="25" maxlength="10" tabindex="7" value="{root/chargeback_retrieval/returned_date}"/>
														<img id="date_rtrn_img" name="date_rtrn_img" src="images/calendar.gif" width="16" height="16" tabindex="8"/>
													</div>
												</td>
												<script type="text/javascript">
  Calendar.setup(
    {
      inputField  : "date_rtrn",  // ID of the input field
      ifFormat    : "mm/dd/y",  // the date format
      button      : "date_rtrn_img"  // ID of the button
    }
  );
												</script>
											</xsl:otherwise>
										</xsl:choose>
									</tr>
									<tr class="Label">
									<td></td>
									<td colspan="3">
									<div><span id="span_amount_error" class="ErrorMessage" style="display:none">Incorrect Format</span>
									</div>
                                    <div>
									<span id="span_amount_error1" class="ErrorMessage" style="display:none">Amount cannot exceed the Bill/Refund Amount you selected</span>
									</div>
									<div>
									<span id="span_amount" class="ErrorMessage" style="display:none">Required</span>
									</div>
									</td>
									<td>
									<xsl:choose>
									<xsl:when test="$cbrtype = 'CB'">
									<div>
									<span id="span_date_rvsl_error" class="ErrorMessage" style="display:none">Invalid Date</span>
									</div>
									</xsl:when>
									<xsl:otherwise>
									<div>
									<span id="span_date_rtrn_error" class="ErrorMessage" style="display:none">Invalid Date</span>
									</div>
									</xsl:otherwise>
									</xsl:choose>
									</td>
									</tr>
									<tr class="Label">
										<td>
											<div align="right">Reason Given:</div>
										</td>
										<td colspan="3">
											<div align="left">
												<select id="reason" name="reason" class="select" tabindex="4">
													<option value="none"></option>
													<xsl:for-each select="root/reasons/reason">
														<xsl:choose>
															<xsl:when test=" reason_code = $cbrreason">
																<option value="{reason_code}" selected="selected">
																	<xsl:value-of select="reason_code" />
																</option>
															</xsl:when>
															<xsl:otherwise>
																<option value="{reason_code}">
																	<xsl:value-of select="reason_code" />
																</option>
															</xsl:otherwise>
														</xsl:choose>
													</xsl:for-each>
												</select>
											</div>
											<div>
												<span id="span_reason" class="ErrorMessage" style="display:none">Required</span>
											</div>
										</td>
										<td> </td>
									</tr>
									<tr class="Label">
										<td>
											<div align="right">Recourse/Comments: </div>
										</td>
										<td colspan="4" valign="middle">
											<div align="left">
												<textarea name="comments" id="comments" cols="50" rows="5" tabindex="9">
													<xsl:value-of select="root/chargeback_retrieval/comment"/>
												</textarea>
											</div>
										</td>
									</tr>
									<tr class="Label">
										<td colspan="5" align="center">
											<table width="100%"  border="0" cellpadding="0" cellspacing="0">
												<tr valign="bottom" class="Label">
													<xsl:choose>
												    <xsl:when test="$cbr_action = 'add'">
													<td width="9%">
														<div align="center">Select</div>
													</td>
													</xsl:when>
												    <xsl:otherwise>
												    <td width="9%">
														<div align="center"></div>
													</td>
													</xsl:otherwise>
											        </xsl:choose>
													<td width="14%">
														<div align="center">Type</div>
													</td>
													<td width="24%">
														<div align="center">Bill / Refund <br/>Amount
														</div>
													</td>
													<td width="27%">
														<div align="center">Payment Method </div>
													</td>
													<td width="26%">
														<p align="center">Credit Card<br/> Number
														</p>
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr class="Label">
										<td colspan="5" align="center">
											<hr size="2"/>
										</td>
									</tr>
									<tr class="Label">
										<td colspan="5" align="center">
											<xsl:choose>
												<xsl:when test="$cbr_action = 'add' or $cbr_action = 'display_add'">
													<div align="center" class="floatingdiv innerTable">
														<table id="payment" width="100%"  border="0" cellpadding="0" cellspacing="0">
															<xsl:for-each select="root/payments_cb/payment">
																<xsl:variable name="radioselect" select="@num" />
																<tr valign="top">
																	<td width="9%">
																		<div align="center">
																			<input name="radiobutton" type="radio" tabindex="10" onclick="setFields('{amount}','{payment_id}')"/>
																		        <input type="hidden" name="payment_amount"  id="payment_amount" value=""/>
																		        	<input type="hidden" name="payment_id"  id="payment_id" value=""/>
																		        
																		</div>
																	</td>
																	<td width="14%">
																		<div align="center">Payment</div>
																	</td>
																	<td width="24%">
																		<div align="center">
																			<xsl:value-of select="amount"/>
																		</div>
																	</td>
																	<td width="27%">
																		<div align="center">
																			<xsl:value-of select="payment_type"/>
																		</div>
																	</td>
																	<td width="26%">
																		<div align="center">
																			<xsl:value-of select="cc_number"/>
																		</div>
																	</td>
																</tr>
															</xsl:for-each>
														</table>
													</div>
												</xsl:when>
												<xsl:otherwise>
													<div align="center" class="floatdiv innerTable">
														<table id="payment" width="100%"  border="0" cellpadding="0" cellspacing="0">
															<xsl:for-each select="root/payments_not_cb/payment">
																<tr valign="top">
																	<td width="9%"></td>
																	<td width="14%">
																		<div align="center">
																			<xsl:value-of select="type"/>
																		</div>
																	</td>
																	<td width="24%">
																		<div align="center">
																			<xsl:value-of select="amount"/>
																		</div>
																	</td>
																	<td width="27%">
																		<div align="center">
																			<xsl:value-of select="payment_type"/>
																		</div>
																	</td>
																	<td width="26%">
																		<div align="center">
																			<xsl:value-of select="cc_number"/>
																		</div>
																	</td>
																</tr>
															</xsl:for-each>
														</table>
													</div>
												</xsl:otherwise>
											</xsl:choose></td>
									</tr>
									<tr class="Label">
										<td colspan="5" align="center"></td>
									</tr>
									<tr class="Label">
										<td colspan="5" align="left">
										<div>
									      <span id="span_payment_error" class="ErrorMessage" style="display:none">Please Select a Payment</span>
									      </div>
										</td>
									</tr>
									<tr class="Label">
										<td align="center"></td>
										<td colspan="4" align="center">
											<table width="100%"  border="0" class="Label">
												<tr>
													<td width="10%">
														<div align="center">Date/Time</div>
													</td>
													<td width="10%">
														<div align="center">User</div>
													</td>
													<td width="75%">
														<div align="left">Recourse/ Comment </div>
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr class="Label">
										<td>
											<div align="right" id="prev_comments">Previous Comments: </div>
										</td>
										<td colspan="4">
											<div align="center" class="floatingdiv innerTable">
												<table width="100%" border="0">
													<xsl:for-each select="root/cbr_comments/cbr_comment">
														<tr>
															<td width="10%">
																<div align="center">
																	<xsl:value-of select="created_on"/>
																</div>
															</td>
															<td width="10%">
																<div align="center">
																	<xsl:value-of select="created_by"/>
																</div>
															</td>
															<td width="75%">
																<div align="left">
																	<xsl:value-of select="comment_text"/>
																</div>
															</td>
														</tr>
													</xsl:for-each>
												</table>
											</div>
										</td>
									</tr>
									<tr>
										<td colspan="5"></td>
									</tr>
								</table></td>
						</tr>
					</table>
					<table width="98%" border="0" align="center" cellpadding="0" cellspacing="1">
						<tr>
							<td>
								<div align="center">
									<input type="button" class="blueButton" name="button_save" id="button_save" value="(S)ave" onClick="javascript:saveCBR();" tabindex="11" accesskey="S"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<input type="button" class="blueButton" name="button_cancel" id="button_cancel" value="(C)ancel" onClick="javascript:cancelCBR();" tabindex="12" accesskey="C"/>
								</div>
							</td>
						</tr>
					</table>
					<!--Copyright bar-->
					<xsl:call-template name="footer"/>
				</form>
			</body>
		</html>
		<script type="text/javascript">
  Calendar.setup(
    {
      inputField  : "date_rcvd",  // ID of the input field
      ifFormat    : "mm/dd/y",  // the date format
      button      : "date_rcvd_img"  // ID of the button
    }
  );
		</script>
		<script type="text/javascript">
  Calendar.setup(
    {
      inputField  : "date_due",  // ID of the input field
      ifFormat    : "mm/dd/y",  // the date format
      button      : "date_due_img"  // ID of the button
    }
  );
		</script>
	</xsl:template>
	<!--to get the header title-->
	<xsl:variable name="headername" select="root/header"/>
	<xsl:template name="IncludeHeader">
		<xsl:call-template name="header">
			<xsl:with-param name="headerName" select="$headername"/>
		</xsl:call-template>
	</xsl:template>
	<!--end-->
</xsl:stylesheet>