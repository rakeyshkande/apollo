<!DOCTYPE ACDemo [
  <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- Keys -->
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>

<!-- Variables -->
<xsl:variable name="OK" select="'O'"/>
<xsl:variable name="CONFIRM" select="'C'"/>
<xsl:variable name="CURRENT_PAGE" select="key('pageData', 'current_page')/value"/>
<xsl:variable name="ORIG_PARTNER_SELECTED" select="key('pageData', 'orig_partner_selected')/value"/>
<xsl:variable name="ORIG_SORT_COLUMN" select="key('pageData', 'orig_sort_column')/value"/>
<xsl:variable name="ORIG_SORT_DIRECTION" select="key('pageData', 'orig_sort_direction')/value"/>
<xsl:variable name="REMIT_DETAILS_UPDATE_COUNT" select="key('pageData', 'remit_details_update_count')/value"/>
<xsl:variable name="SHOW_FIRST" select="key('pageData', 'show_first')/value"/>
<xsl:variable name="SHOW_PREVIOUS" select="key('pageData', 'show_previous')/value"/>
<xsl:variable name="SHOW_NEXT" select="key('pageData', 'show_next')/value"/>
<xsl:variable name="SHOW_LAST" select="key('pageData', 'show_last')/value"/>
<xsl:variable name="TOTAL_PAGES" select="key('pageData', 'total_pages')/value"/>


	<xsl:output method="html" indent="yes"/>
	<xsl:template match="/root">
	<html>
		<head>
			<title>FTD - Outstanding Receivables</title>
			<link href="css/ftd.css"      type="text/css" rel="stylesheet"/>
			<script type="text/javascript" src="js/copyright.js"></script>
			
			<style>
				th
				{
					font-size: 8pt;
					font-family: Arial;
				}

				<style type="text/css">
					table.remit
					{
						border:1px solid black;
						border-collapse:collapse;
					}
					table.remit th, table.remit td
					{
						border:1px solid #aaaaaa;
						padding: 2px 15px 2px 15px;
					}
					table.remit tr.alternate
					{
						background-color:#ffffcc;
					}
					table.remit thead th
					{
						background-color:#ccccff;
					}
					table.remit tfoot td
					{
						background-color:#ffccff;
					}
					table.remit th.sortable
					{
						cursor:pointer;
					}
					table.remit th.TableSortedAscending
					{
						background-image:url("images/sorted_up.gif");
						background-position:center left;
						background-repeat:no-repeat;
						cursor:pointer;
					}
					table.remit th.TableSortedDescending
					{
						background-image:url("images/sorted_down.gif");
						background-position:center left;
						background-repeat:no-repeat;
						cursor:pointer;
					}
				</style>
		  </style>

			<script language="javascript" src="js/receivables.js"/>
			<script language="javascript" src="js/FormChek.js"/>
			<script type="text/javascript" src="js/util.js"/>

			<script language="javascript">

var okError = '<xsl:value-of select="key('pageData', 'OK_MESSAGE')/value"/>';
var origPartnerSelected = '<xsl:value-of select="key('pageData', 'orig_partner_selected')/value"/>';
var remitDetailsUpdateCount = '<xsl:value-of select="key('pageData', 'remit_details_update_count')/value"/>';
var remitDetailsUpdateCountNumeric = remitDetailsUpdateCount - 0;
var recordsDisplayedOnPage = 0;

			</script>
		</head>

		<!-- Body -->
		<body onload="javascript:init();" link="blue" vlink="blue">

			<!-- Content div -->
			<div class="mainContent" style="display:block">

				<!-- Form -->
				<form name="receivablesForm" method="post" action="">

					<input type="hidden" name="context"             				value="{key('pageData', 'context')/value}"/>
					<input type="hidden" name="current_page"        				value="{key('pageData', 'current_page')/value}"/>
					<input type="hidden" name="records_displayed_on_page"		value="0"/>
					<input type="hidden" name="remit_details_update_count"	value="{key('pageData', 'remit_details_update_count')/value}"/>
					<input type="hidden" name="securitytoken"       				value="{key('pageData', 'securitytoken')/value}"/>
					<input type="hidden" name="total_pages"         				value="{key('pageData', 'total_pages')/value}"/>


					<!-- Origin -->
					<input type="hidden" name="new_origin_selected"/>
					<input type="hidden" name="orig_origin_selected"  value="{key('pageData', 'orig_origin_selected')/value}"/>

					<!-- Partner -->
					<input type="hidden" name="new_partner_selected"/>
					<input type="hidden" name="orig_partner_selected" value="{key('pageData', 'orig_partner_selected')/value}"/>

					<!-- Sort Column -->
					<input type="hidden" name="new_sort_column"/>
					<input type="hidden" name="orig_sort_column"      value="{key('pageData', 'orig_sort_column')/value}"/>

					<!-- Sort Direction -->
					<input type="hidden" name="new_sort_direction"/>
					<input type="hidden" name="orig_sort_direction"   value="{key('pageData', 'orig_sort_direction')/value}"/>


					<div style="text-align: center;">
						<table style="width: 98%;" cellpadding="0" cellspacing="0" width="98%">
							<tbody>
								<tr>
									<td style="padding: 0in; width: 30%;" width="30%">
										<p>
										<span style="font-size: 8pt; font-family: Arial;">
											<img alt="FTD.COM" height="32" width="131" src="images/logo.gif" ></img>
										</span>
										</p>
									</td>
									<td style="padding: 0in; background: white none repeat scroll 0%; width: 40%; -moz-background-clip: -moz-initial; -moz-background-origin: -moz-initial; -moz-background-inline-policy: -moz-initial;" width="40%">
										<p style="text-align: center;" align="center">
											<b><span style="font-family: Arial; font-size: 10pt;  color: rgb(0, 0, 153);">Outstanding Receivables</span></b>
										</p>
									</td>
									<td style="padding: 0in; width: 20%;" id="time" width="20%">
										<p style="text-align: center;" align="center">
											<b><span style="font-size: 8pt; font-family: Arial;">&nbsp;</span></b>
										</p>
									</td>
									<td align="right">
										<input name="exitButton" accesskey="E" value="(E)xit" onclick="javascript:exit();" type="button"/>
									</td>
									<td width="15px">&nbsp;</td>
								</tr>
								<tr>
									<td colspan="5">&nbsp;</td>
								</tr>
								<tr>
									<td colspan="5" style="padding: 0in;">
										<div style="text-align: center;">
											<!-- Border surrounding the entire data, excluding the FTD logo and Exit buttons -->
											<table style="border: 2.25pt outset rgb(0, 102, 153); width: 98%;" cellpadding="0" cellspacing="1" rules="none" width="98%">
												<tbody>
													<tr>
														<!-- Border surrounding the partner and remit header data -->
														<td style="border: 1pt inset rgb(0, 102, 153); padding: 0.75pt;">
															<table style="width: 100%;" cellpadding="0" width="100%">
																<tbody>
																	<tr>
																		<td width="50px" align="right">Partner &nbsp;</td>
																		<td width="75px" align="left">
																			&nbsp;
																			<select name="partnerDropDown">
																				<xsl:for-each select="OUT_PARTNERS/OUT_PARTNER">
																					<option value="{partner_id}" origin="{origin_id}">
																						<xsl:if test="partner_id = $ORIG_PARTNER_SELECTED"><xsl:attribute name="selected"/></xsl:if>
																						<xsl:value-of select="partner_name"/>
																					</option>
																				</xsl:for-each>
																			</select>
																		</td>
																		<td colspan="5">&nbsp;</td>
																		<td id="difference_label" name="difference_label">Difference</td>
																		<td id="difference_value" name="difference_value">
																			<xsl:choose>
																				<xsl:when test="OUT_TOTALS/OUT_TOTAL/difference">
																					<xsl:value-of select="OUT_TOTALS/OUT_TOTAL/difference"/>
																				</xsl:when>
																				<xsl:otherwise>
																					0.00
																				</xsl:otherwise>
																			</xsl:choose>
																		</td>
																	</tr>
																	<tr>
																		<td colspan="2" align="center" valign="middle">
																			<input name="refreshButton" accesskey="R" value="(R)efresh Data" onclick="javascript:refresh();" type="button"/>
																		</td>
																		<td width="50px">&nbsp;</td>
																		<td id="remit_date_label" name="remit_date_label">Remittance Date</td>
																		<td>
																			<input type="text" name="remit_date_text" size="10" maxlength="10">
																				<xsl:if test="OUT_HEADERS/OUT_HEADER">
																					<xsl:attribute name="value">
																						<xsl:value-of select="OUT_HEADERS/OUT_HEADER/remittance_date"/>
																					</xsl:attribute>
																				</xsl:if>
																			</input>
																		</td>
																		<td id="remit_amount_label" name="remit_amount_label">Remittance Amount</td>
																		<td>
																			<input type="text" name="remit_amount_text" size="10" maxlength="10" onblur="javascript:updateDifference();">
																				<xsl:if test="OUT_HEADERS/OUT_HEADER">
																					<xsl:attribute name="value">
																						<xsl:value-of select="format-number(OUT_HEADERS/OUT_HEADER/remittance_amt, '#,###,##0.00')"/>
																					</xsl:attribute>
																				</xsl:if>
																			</input>
																		</td>
																		<td id="remit_number_label" name="remit_number_label">Remittance Number</td>
																		<td>
																			<input type="text" name="remit_number_text" size="20" maxlength="20">
																				<xsl:if test="OUT_HEADERS/OUT_HEADER">
																					<xsl:attribute name="value">
																						<xsl:value-of select="OUT_HEADERS/OUT_HEADER/remittance_number"/>
																					</xsl:attribute>
																				</xsl:if>
																			</input>
																		</td>
																	</tr>
																</tbody>
															</table>
														</td>
													</tr>
													<tr>
														<!-- Border surrounding the column header, detail info, and buttons -->
														<td style="border: 1pt inset rgb(0, 102, 153); padding: 0.75pt;">
															<div style="text-align: center;">
																<table class="remit" style="width: 98%;" cellpadding="0" width="98%" cellspacing="0">
																	<caption style="background: rgb(54, 105, 172);">
																		<b><span style="font-size: 11pt; font-family: Arial; color: white;">Unsettled Orders</span></b>
																	</caption>
																	<THEAD>
																		<tr>

																			<!-- Checkbox -->
																			<th>Select</th>

																			<!-- Ship Id - Line # -->
																			<th title="Click here to sort" onclick="javascript:sort('ship_id_line_num');">
																				<xsl:choose>
																					<xsl:when test="$ORIG_SORT_COLUMN = 'ship_id_line_num'">
																						<xsl:choose>
																							<xsl:when test="$ORIG_SORT_DIRECTION = 'asc'">
																								<xsl:attribute name="class">TableSortedAscending</xsl:attribute>
																							</xsl:when>
																							<xsl:otherwise>
																								<xsl:attribute name="class">TableSortedDescending</xsl:attribute>
																							</xsl:otherwise>
																						</xsl:choose>
																					</xsl:when>
																					<xsl:otherwise>
																						<xsl:attribute name="class">sortable</xsl:attribute>
																					</xsl:otherwise>
																				</xsl:choose>
																				PO # / Ship ID
																			</th>

																			<!-- External Order Number -->
																			<th title="Click here to sort" onclick="javascript:sort('external_order_number');">
																				<xsl:choose>
																					<xsl:when test="$ORIG_SORT_COLUMN = 'external_order_number'">
																						<xsl:choose>
																							<xsl:when test="$ORIG_SORT_DIRECTION = 'asc'">
																								<xsl:attribute name="class">TableSortedAscending</xsl:attribute>
																							</xsl:when>
																							<xsl:otherwise>
																								<xsl:attribute name="class">TableSortedDescending</xsl:attribute>
																							</xsl:otherwise>
																						</xsl:choose>
																					</xsl:when>
																					<xsl:otherwise>
																						<xsl:attribute name="class">sortable</xsl:attribute>
																					</xsl:otherwise>
																				</xsl:choose>
																				Apollo Order Number
																			</th>

																			<!-- Billing Date -->
																			<th title="Click here to sort" onclick="javascript:sort('billing_date');">
																				<xsl:choose>
																					<xsl:when test="$ORIG_SORT_COLUMN = 'billing_date'">
																						<xsl:choose>
																							<xsl:when test="$ORIG_SORT_DIRECTION = 'asc'">
																								<xsl:attribute name="class">TableSortedAscending</xsl:attribute>
																							</xsl:when>
																							<xsl:otherwise>
																								<xsl:attribute name="class">TableSortedDescending</xsl:attribute>
																							</xsl:otherwise>
																						</xsl:choose>
																					</xsl:when>
																					<xsl:otherwise>
																						<xsl:attribute name="class">sortable</xsl:attribute>
																					</xsl:otherwise>
																				</xsl:choose>
																				Billing Date
																			</th>

																			<!-- Delivery Date -->
																			<th title="Click here to sort" onclick="javascript:sort('delivery_date');">
																				<xsl:choose>
																					<xsl:when test="$ORIG_SORT_COLUMN = 'delivery_date'">
																						<xsl:choose>
																							<xsl:when test="$ORIG_SORT_DIRECTION = 'asc'">
																								<xsl:attribute name="class">TableSortedAscending</xsl:attribute>
																							</xsl:when>
																							<xsl:otherwise>
																								<xsl:attribute name="class">TableSortedDescending</xsl:attribute>
																							</xsl:otherwise>
																						</xsl:choose>
																					</xsl:when>
																					<xsl:otherwise>
																						<xsl:attribute name="class">sortable</xsl:attribute>
																					</xsl:otherwise>
																				</xsl:choose>
																				Delivery Date
																			</th>

																			<!-- Days Aged -->
																			<th title="Click here to sort" onclick="javascript:sort('days_aged');">
																				<xsl:choose>
																					<xsl:when test="$ORIG_SORT_COLUMN = 'days_aged'">
																						<xsl:choose>
																							<xsl:when test="$ORIG_SORT_DIRECTION = 'asc'">
																								<xsl:attribute name="class">TableSortedAscending</xsl:attribute>
																							</xsl:when>
																							<xsl:otherwise>
																								<xsl:attribute name="class">TableSortedDescending</xsl:attribute>
																							</xsl:otherwise>
																						</xsl:choose>
																					</xsl:when>
																					<xsl:otherwise>
																						<xsl:attribute name="class">sortable</xsl:attribute>
																					</xsl:otherwise>
																				</xsl:choose>
																				Days Aged
																			</th>

																			<!-- Expected Wholesale Amount -->
																			<th title="Click here to sort" onclick="javascript:sort('expected_wholesale_amt');">
																				<xsl:choose>
																					<xsl:when test="$ORIG_SORT_COLUMN = 'expected_wholesale_amt'">
																						<xsl:choose>
																							<xsl:when test="$ORIG_SORT_DIRECTION = 'asc'">
																								<xsl:attribute name="class">TableSortedAscending</xsl:attribute>
																							</xsl:when>
																							<xsl:otherwise>
																								<xsl:attribute name="class">TableSortedDescending</xsl:attribute>
																							</xsl:otherwise>
																						</xsl:choose>
																					</xsl:when>
																					<xsl:otherwise>
																						<xsl:attribute name="class">sortable</xsl:attribute>
																					</xsl:otherwise>
																				</xsl:choose>
																				Expected Wholesale Amount
																			</th>

																			<!-- Prior Remittance -->
																			<th title="Click here to sort" onclick="javascript:sort('prior_rem_applied_amt');">
																				<xsl:choose>
																					<xsl:when test="$ORIG_SORT_COLUMN = 'prior_rem_applied_amt'">
																						<xsl:choose>
																							<xsl:when test="$ORIG_SORT_DIRECTION = 'asc'">
																								<xsl:attribute name="class">TableSortedAscending</xsl:attribute>
																							</xsl:when>
																							<xsl:otherwise>
																								<xsl:attribute name="class">TableSortedDescending</xsl:attribute>
																							</xsl:otherwise>
																						</xsl:choose>
																					</xsl:when>
																					<xsl:otherwise>
																						<xsl:attribute name="class">sortable</xsl:attribute>
																					</xsl:otherwise>
																				</xsl:choose>
																				Prior Remittance(s) Applied
																			</th>

																			<!-- Outstanding Balance -->
																			<th title="Click here to sort" onclick="javascript:sort('outstanding_bal_due_amt');">
																				<xsl:choose>
																					<xsl:when test="$ORIG_SORT_COLUMN = 'outstanding_bal_due_amt'">
																						<xsl:choose>
																							<xsl:when test="$ORIG_SORT_DIRECTION = 'asc'">
																								<xsl:attribute name="class">TableSortedAscending</xsl:attribute>
																							</xsl:when>
																							<xsl:otherwise>
																								<xsl:attribute name="class">TableSortedDescending</xsl:attribute>
																							</xsl:otherwise>
																						</xsl:choose>
																					</xsl:when>
																					<xsl:otherwise>
																						<xsl:attribute name="class">sortable</xsl:attribute>
																					</xsl:otherwise>
																				</xsl:choose>
																				Outstanding Balance Due
																			</th>

																			<!-- Cash Received -->
																			<th title="Click here to sort" onclick="javascript:sort('cash_received_amt');">
																				<xsl:choose>
																					<xsl:when test="$ORIG_SORT_COLUMN = 'cash_received_amt'">
																						<xsl:choose>
																							<xsl:when test="$ORIG_SORT_DIRECTION = 'asc'">
																								<xsl:attribute name="class">TableSortedAscending</xsl:attribute>
																							</xsl:when>
																							<xsl:otherwise>
																								<xsl:attribute name="class">TableSortedDescending</xsl:attribute>
																							</xsl:otherwise>
																						</xsl:choose>
																					</xsl:when>
																					<xsl:otherwise>
																						<xsl:attribute name="class">sortable</xsl:attribute>
																					</xsl:otherwise>
																				</xsl:choose>
																				Cash Received
																			</th>

																			<!-- Write Off -->
																			<th title="Click here to sort" onclick="javascript:sort('write_off_amt');">
																				<xsl:choose>
																					<xsl:when test="$ORIG_SORT_COLUMN = 'write_off_amt'">
																						<xsl:choose>
																							<xsl:when test="$ORIG_SORT_DIRECTION = 'asc'">
																								<xsl:attribute name="class">TableSortedAscending</xsl:attribute>
																							</xsl:when>
																							<xsl:otherwise>
																								<xsl:attribute name="class">TableSortedDescending</xsl:attribute>
																							</xsl:otherwise>
																						</xsl:choose>
																					</xsl:when>
																					<xsl:otherwise>
																						<xsl:attribute name="class">sortable</xsl:attribute>
																					</xsl:otherwise>
																				</xsl:choose>
																				Write Off
																			</th>

																			<!-- Remaining Balance Due -->
																			<th title="Click here to sort" onclick="javascript:sort('remaining_bal_due_amt');">
																				<xsl:choose>
																					<xsl:when test="$ORIG_SORT_COLUMN = 'remaining_bal_due_amt'">
																						<xsl:choose>
																							<xsl:when test="$ORIG_SORT_DIRECTION = 'asc'">
																								<xsl:attribute name="class">TableSortedAscending</xsl:attribute>
																							</xsl:when>
																							<xsl:otherwise>
																								<xsl:attribute name="class">TableSortedDescending</xsl:attribute>
																							</xsl:otherwise>
																						</xsl:choose>
																					</xsl:when>
																					<xsl:otherwise>
																						<xsl:attribute name="class">sortable</xsl:attribute>
																					</xsl:otherwise>
																				</xsl:choose>
																				Remaining Balance Due
																			</th>
																		</tr>
																	</THEAD>
																	<tbody>
																		<xsl:for-each select="OUT_DETAILS/OUT_DETAIL">
																			<script>
																				recordsDisplayedOnPage++;
																			</script>
																			<tr border="2">

																				<!-- Checkbox -->
																				<td align="center">
																					<input type="checkbox" id="checkbox_{position()}" name="checkbox_{position()}" onclick="javascript:checkUncheck({position()});">
																						<xsl:if test="settle_flag = 'Y'"><xsl:attribute name="CHECKED"/></xsl:if>
																					</input>
																				</td>

																				<!-- Ship Id - Line # -->
																				<td id="td_ship_id_line_num_{position()}" name="td_ship_id_line_num_{position()}" align="center">
																					<xsl:if test="settle_flag = 'N'"><xsl:attribute name="style">color:gray</xsl:attribute></xsl:if>
																					<xsl:value-of select="ship_id_line_num"/>
																				</td>

																				<!-- External Order Number -->
																				<td id="td_external_order_number_{position()}" name="td_external_order_number_{position()}" align="center">
																					<xsl:if test="settle_flag = 'N'"><xsl:attribute name="style">color:gray</xsl:attribute></xsl:if>
																					<xsl:value-of select="external_order_number"/>
																				</td>

																				<!-- Billing Date -->
																				<td id="td_billing_date_{position()}" name="td_billing_date_{position()}" align="center">
																					<xsl:if test="settle_flag = 'N'"><xsl:attribute name="style">color:gray</xsl:attribute></xsl:if>
																					<xsl:value-of select="billing_date"/>
																				</td>

																				<!-- Delivery Date -->
																				<td id="td_delivery_date_{position()}" name="td_delivery_date_{position()}" align="center">
																					<xsl:if test="settle_flag = 'N'"><xsl:attribute name="style">color:gray</xsl:attribute></xsl:if>
																					<xsl:value-of select="delivery_date"/>
																				</td>

																				<!-- Days Aged -->
																				<td id="td_days_aged_{position()}" name="td_days_aged_{position()}" align="center">
																					<xsl:if test="settle_flag = 'N'"><xsl:attribute name="style">color:gray</xsl:attribute></xsl:if>
																					<xsl:value-of select="days_aged"/>
																				</td>

																				<!-- Expected Wholesale Amount -->
																				<td id="td_expected_wholesale_amt_{position()}" name="td_expected_wholesale_amt_{position()}" align="center">
																					<xsl:if test="settle_flag = 'N'"><xsl:attribute name="style">color:gray</xsl:attribute></xsl:if>
																					<xsl:value-of select="format-number(expected_wholesale_amt, '#,###,##0.00')"/>
																				</td>

																				<!-- Prior Remittance -->
																				<td id="td_prior_rem_applied_amt_{position()}" name="td_prior_rem_applied_amt_{position()}" align="center">
																					<xsl:if test="settle_flag = 'N'"><xsl:attribute name="style">color:gray</xsl:attribute></xsl:if>
																					<xsl:value-of select="format-number(prior_rem_applied_amt, '#,###,##0.00')"/>
																				</td>

																				<!-- Outstanding Balance -->
																				<td id="td_outstanding_bal_due_amt_{position()}" name="td_outstanding_bal_due_amt_{position()}" align="center">
																					<xsl:if test="settle_flag = 'N'"><xsl:attribute name="style">color:gray</xsl:attribute></xsl:if>
																					<xsl:value-of select="format-number(outstanding_bal_due_amt, '#,###,##0.00')"/>
																				</td>

																				<!-- Cash Received -->
																				<td align="center" >
																					<input type="text" id="cash_received_amt_{position()}" name="cash_received_amt_{position()}" maxlength="8" size="6" onblur="javascript:updateRemaining({position()});">
																						<xsl:if test="settle_flag = 'N'"><xsl:attribute name="DISABLED"/></xsl:if>
																						<xsl:attribute name="value"><xsl:value-of select="cash_received_amt"/></xsl:attribute>
																					</input>
																				</td>

																				<!-- Write Off -->
																				<td id="td_write_off_amt_{position()}" name="td_write_off_amt_{position()}" align="center" onclick="javascript:toggleRemaining({position()});">
																					<xsl:choose>
																						<xsl:when test="settle_flag = 'N'">
																							<xsl:attribute name="style">cursor:pointer; color: gray</xsl:attribute>
																						</xsl:when>
																						<xsl:otherwise>
																							<xsl:attribute name="style">cursor:pointer</xsl:attribute>
																						</xsl:otherwise>
																					</xsl:choose>
																					<u>
																						<xsl:value-of select="format-number(write_off_amt, '#,###,##0.00')"/>
																					</u>
																				</td>

																				<!-- Remaining Balance Due -->
																				<td id="td_remaining_bal_due_amt_{position()}" name="td_remaining_bal_due_amt_{position()}" align="center">
																					<xsl:if test="settle_flag = 'N'"><xsl:attribute name="style">color:gray</xsl:attribute></xsl:if>
																					<xsl:value-of select="format-number(remaining_bal_due_amt, '#,###,##0.00')"/>
																				</td>

																				<!-- Hidden variables for all the above -->
																				<input type="hidden" name="settle_flag_{position()}" id="settle_flag_{position()}" value="{settle_flag}"/>
																				<input type="hidden" name="ship_id_line_num_{position()}"	id="ship_id_line_num_{position()}"	value="{ship_id_line_num}" />
																				<input type="hidden" name="external_order_number_{position()}"	id="external_order_number_{position()}"	value="{external_order_number}" />
																				<input type="hidden" name="order_detail_id_{position()}"	id="order_detail_id_{position()}"	value="{order_detail_id}" />
																				<input type="hidden" name="billing_date_{position()}"	id="billing_date_{position()}"	value="{billing_date}" />
																				<input type="hidden" name="delivery_date_{position()}"	id="delivery_date_{position()}"	value="{delivery_date}" />
																				<input type="hidden" name="days_aged_{position()}"	id="days_aged_{position()}"	value="{days_aged}" />
																				<input type="hidden" name="expected_wholesale_amt_{position()}"	id="expected_wholesale_amt_{position()}"	value="{expected_wholesale_amt}" />
																				<input type="hidden" name="prior_rem_applied_amt_{position()}"	id="prior_rem_applied_amt_{position()}"	value="{prior_rem_applied_amt}" />
																				<input type="hidden" name="outstanding_bal_due_amt_{position()}"	id="outstanding_bal_due_amt_{position()}"	value="{outstanding_bal_due_amt}" />
																				<input type="hidden" name="write_off_amt_{position()}"	id="write_off_amt_{position()}"	value="{write_off_amt}" />
																				<input type="hidden" name="remaining_bal_due_amt_{position()}"	id="remaining_bal_due_amt_{position()}"	value="{remaining_bal_due_amt}" />

																			</tr>
																		</xsl:for-each>
																	</tbody>

<!-- ************************************************************************************************************ -->
<!-- ************************************************************************************************************ -->
																	<TFOOT>
																		<tr>
																			<td id="total_label" name="total_label" colspan="3" align="left">Totals</td>
																			<td>&nbsp;</td>
																			<td>&nbsp;</td>
																			<td>&nbsp;</td>
																			<td id="tot_expected_wholesale_amt_all" name="tot_expected_wholesale_amt_all" align="center">
																				<xsl:choose>
																					<xsl:when test="OUT_TOTALS/OUT_TOTAL/tot_expected_wholesale_amt">
																						<xsl:value-of select="format-number(OUT_TOTALS/OUT_TOTAL/tot_expected_wholesale_amt, '#,###,##0.00')"/>
																					</xsl:when>
																					<xsl:otherwise>
																						0.00
																					</xsl:otherwise>
																				</xsl:choose>
																			</td>
																			<td id="tot_prior_rem_applied_amt_all" name="tot_prior_rem_applied_amt_all" align="center">
																				<xsl:choose>
																					<xsl:when test="OUT_TOTALS/OUT_TOTAL/tot_prior_rem_applied_amt">
																						<xsl:value-of select="format-number(OUT_TOTALS/OUT_TOTAL/tot_prior_rem_applied_amt, '#,###,##0.00')"/>
																					</xsl:when>
																					<xsl:otherwise>
																						0.00
																					</xsl:otherwise>
																				</xsl:choose>
																			</td>
																			<td id="tot_outstanding_bal_due_amt_all" name="tot_outstanding_bal_due_amt_all" align="center">
																				<xsl:choose>
																					<xsl:when test="OUT_TOTALS/OUT_TOTAL/tot_outstanding_bal_due_amt">
																						<xsl:value-of select="format-number(OUT_TOTALS/OUT_TOTAL/tot_outstanding_bal_due_amt, '#,###,##0.00')"/>
																					</xsl:when>
																					<xsl:otherwise>
																						0.00
																					</xsl:otherwise>
																				</xsl:choose>
																			</td>
																			<td id="tot_cash_received_amt_all" name="tot_cash_received_amt_all" align="center">
																				<xsl:choose>
																					<xsl:when test="OUT_TOTALS/OUT_TOTAL/tot_cash_received_amt">
																						<xsl:value-of select="format-number(OUT_TOTALS/OUT_TOTAL/tot_cash_received_amt, '#,###,##0.00')"/>
																					</xsl:when>
																					<xsl:otherwise>
																						0.00
																					</xsl:otherwise>
																				</xsl:choose>
																			</td>
																			<td id="tot_write_off_amt_all" name="tot_write_off_amt_all" align="center">
																				<xsl:choose>
																					<xsl:when test="OUT_TOTALS/OUT_TOTAL/tot_write_off_amt">
																						<xsl:value-of select="format-number(OUT_TOTALS/OUT_TOTAL/tot_write_off_amt, '#,###,##0.00')"/>
																					</xsl:when>
																					<xsl:otherwise>
																						0.00
																					</xsl:otherwise>
																				</xsl:choose>
																			</td>
																			<td id="tot_remaining_bal_due_amt_all" name="tot_remaining_bal_due_amt_all" align="center">
																				<xsl:choose>
																					<xsl:when test="OUT_TOTALS/OUT_TOTAL/tot_remaining_bal_due_amt">
																						<xsl:value-of select="format-number(OUT_TOTALS/OUT_TOTAL/tot_remaining_bal_due_amt, '#,###,##0.00')"/>
																					</xsl:when>
																					<xsl:otherwise>
																						0.00
																					</xsl:otherwise>
																				</xsl:choose>
																			</td>
																		</tr>
																	</TFOOT>
<!-- ************************************************************************************************************ -->
<!-- ************************************************************************************************************ -->


																</table>
															</div>
															<div>
																<table border="0" cellpadding="3" cellspacing="0">
																	<caption/>
																	<tr>
																		<td>
																			<xsl:choose>
																				<xsl:when test="$ORIG_PARTNER_SELECTED != '' and $REMIT_DETAILS_UPDATE_COUNT > 0">
																					<input type="button" name="selectAllButton" style="width: 90px;" accesskey="A" value="Select (A)ll" onclick="javascript:checkAll();"/>
																				</xsl:when>
																				<xsl:otherwise>
																					<input type="button" name="selectAllButton" style="width: 90px;" accesskey="A" value="Select (A)ll">
																						<xsl:attribute name="DISABLED"/>
																					</input>
																				</xsl:otherwise>
																			</xsl:choose>

																		</td>
																		<td>
																			<xsl:choose>
																				<xsl:when test="$ORIG_PARTNER_SELECTED != '' and $REMIT_DETAILS_UPDATE_COUNT > 0">
																					<input type="button" name="unselectAllButton" style="width: 90px;" accesskey="U" value="(U)nselect All" onclick="javascript:uncheckAll();"/>
																				</xsl:when>
																				<xsl:otherwise>
																					<input type="button" name="unselectAllButton" style="width: 90px;" accesskey="U" value="(U)nselect All">
																						<xsl:attribute name="DISABLED"/>
																					</input>
																				</xsl:otherwise>
																			</xsl:choose>
																		</td>
																		<td>
																			<xsl:choose>
																				<xsl:when test="$ORIG_PARTNER_SELECTED != '' and $REMIT_DETAILS_UPDATE_COUNT > 0">
																					<input type="button" name="settleButton" style="width: 90px;" accesskey="S" value="(S)ettle" onclick="javascript:settle();"/>
																				</xsl:when>
																				<xsl:otherwise>
																					<input type="button" name="settleButton" style="width: 90px;" accesskey="S" value="(S)ettle">
																						<xsl:attribute name="DISABLED"/>
																					</input>
																				</xsl:otherwise>
																			</xsl:choose>
																		</td>
																		<td>
																			<xsl:choose>
																				<xsl:when test="$ORIG_PARTNER_SELECTED != '' and $REMIT_DETAILS_UPDATE_COUNT > 0">
																					<input type="button" name="cancelButton" style="width: 140px;" accesskey="C" value="(C)ancel Settlement" onclick="javascript:cancel();"/>
																				</xsl:when>
																				<xsl:otherwise>
																					<input type="button" name="cancelButton" style="width: 140px;" accesskey="C" value="(C)ancel Settlement">
																						<xsl:attribute name="DISABLED"/>
																					</input>
																				</xsl:otherwise>
																			</xsl:choose>
																		</td>
																		<td>
																			<xsl:choose>
																				<xsl:when test="$ORIG_PARTNER_SELECTED != '' and $REMIT_DETAILS_UPDATE_COUNT > 0">
																					<input type="button" name="downloadButton" style="width: 90px;" accesskey="D" value="(D)ownload" onclick="javascript:download();"/>
																				</xsl:when>
																				<xsl:otherwise>
																					<input type="button" name="downloadButton" style="width: 90px;" accesskey="D" value="(D)ownload">
																						<xsl:attribute name="DISABLED"/>
																					</input>
																				</xsl:otherwise>
																			</xsl:choose>
																		</td>
																		<td>
																			<input type="button" name="reportButton" style="width: 140px;" accesskey="P" value="Re(p)ort Submission" onclick="javascript:reportSubmission();"/>
																		</td>
																		<td width="100%">&nbsp;</td>
																		<td>
																			<xsl:choose>
																				<xsl:when test="$SHOW_FIRST = 'y'">
																					<a href="#"><img src="images/firstItem.gif" border="0" name="firstItem" id="firstItem" onclick="javascript:pageAction('first_page');"/></a>
																				</xsl:when>
																				<xsl:otherwise>
																					<img src="images/firstItemGray.gif" border="0" name="firstItem" id="firstItem"/>
																				</xsl:otherwise>
																			</xsl:choose>
																		</td>
																		<td>
																			<xsl:choose>
																				<xsl:when test="$SHOW_PREVIOUS = 'y'">
																					<a href="#"><img src="images/previousItem.gif" border="0" name="previousItem" id="previousItem" onclick="javascript:pageAction('previous_page');"/></a>
																				</xsl:when>
																				<xsl:otherwise>
																					<img src="images/previousItemGray.gif" border="0" name="previousItem" id="previousItem"/>
																				</xsl:otherwise>
																			</xsl:choose>
																		</td>
																		<td>
																			<xsl:choose>
																				<xsl:when test="$SHOW_NEXT = 'y'">
																					<a href="#"><img src="images/nextItem.gif" border="0" name="nextItem" id="nextItem" onclick="javascript:pageAction('next_page');"/></a>
																				</xsl:when>
																				<xsl:otherwise>
																					<img src="images/nextItemGray.gif" border="0" name="nextItem" id="nextItem"/>
																				</xsl:otherwise>
																			</xsl:choose>
																		</td>
																		<td>
																			<xsl:choose>
																				<xsl:when test="$SHOW_LAST = 'y'">
																					<a href="#"><img src="images/lastItem.gif" border="0" name="lastItem" id="lastItem" onclick="javascript:pageAction('last_page');"/></a>
																				</xsl:when>
																				<xsl:otherwise>
																					<img src="images/lastItemGray.gif" border="0" name="lastItem" id="lastItem"/>
																				</xsl:otherwise>
																			</xsl:choose>
																		</td>
																		<td width="100%">&nbsp;</td>
																	</tr>
																</table>
															</div>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<p><span style="display: none;">&nbsp;</span></p>
					<div style="text-align: center;">
						<table style="width: 96%;" border="0" cellpadding="0" width="96%">
							<tbody>
								<tr>
									<td valign="top"><p>&nbsp;</p></td>
									<td style="padding: 0.75pt; width: 45pt;" align="right" width="60">
										Page&nbsp;<xsl:value-of select="$CURRENT_PAGE"/>&nbsp;of&nbsp;<xsl:value-of select="$TOTAL_PAGES"/>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<p><span style="display: none;">&nbsp;</span></p>
					<div style="text-align: center;">
						<table style="width: 96%;" border="0" cellpadding="0" width="96%">
							<tbody>
								<tr>
									<td valign="top"><p>&nbsp;</p></td>
									<td style="padding: 0.75pt; width: 45pt;" align="right" width="60">
										<input name="exitButton" accesskey="E" value="(E)xit" onclick="javascript:exit();" type="button"/>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<p><span style="display: none;">&nbsp;</span></p>
					<div style="text-align: center;">
						<table style="width: 98%;" border="0" cellpadding="0" cellspacing="0" width="98%">
							<tbody>
								<tr>
									<td style="padding: 0in;">
										<p><span style="font-size: 8pt; font-family: Arial;">&nbsp;</span></p>
									</td>
								</tr>
								<tr>									
									<td style="padding: 0in; background: rgb(0, 51, 102) none repeat scroll 0%; -moz-background-clip: -moz-initial; -moz-background-origin: -moz-initial; -moz-background-inline-policy: -moz-initial;" class="disclaimer"></td>
									<script>showCopyright();</script>
								</tr>
							</tbody>
						</table>
					</div>
				</form>
			<!-- end main content div -->
			</div>
		</body>
	</html>
</xsl:template>
</xsl:stylesheet>