<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	
	<xsl:output method="html" indent="yes"/>
	<xsl:template name="VENDOR_PRODUCT_TEMPLATE" match="parameterValueList/list-item/displayText[. = 'VENDOR_PRODUCT_TEMPLATE']">
		<xsl:param name="oracleRptParm"/>
		<xsl:param name="displayText"/>
			<script type="text/javascript">
			<![CDATA[
			var vendorProductIdArray=new Array();
			function addVendorProductId(){
				var vendorProductId=document.getElementById("product_id").value;
				
				if(validatePresent("product_id", "product_id_error")){
					var url = "AddVendorProductId.do?product_id="+vendorProductId+getSecurityParams(false);			
					//alert(url);
					vendorProductIdIFrame.location.href=url;
				}
			}

			]]>
				
			
		 	
		 </script>
   			<iframe id="vendorProductIdIFrame" name="vendorProductIdIFrame" border="0" width="0" height="0"/>
            <input type="hidden" id="{$oracleRptParm}" name="{$oracleRptParm}"/>
			<table>
               <tr>
                 <td colspan="3" align="center"></td>
              </tr>
               <tr>
                 <td colspan="3" align="center"></td>
              </tr>
               
               <tr>
              
              </tr>
               <tr>
                 <td width="45%" class="LabelRight">Vendor Product ID :</td>
                 <td width="30%" align="left"><input id="product_id" name="product_id" type="text" size="25"/><span id="product_id_error" class="ErrorMessage" style="display:none">Invalid Vendor Product ID</span></td>
                 <td width="25%" align="left"><a href="javascript:addVendorProductId();">Add</a>&nbsp;&nbsp;&nbsp;</td>
                 
              </tr>
               <tr>
                 <td width="45%" class="LabelRight">Vendor Product ID Selected:</td>
                 <td width="30%" align="left">
                  
					    <select name="product_id_selected" multiple="true"></select>
						<br/><span id="{$oracleRptParm}_error" class="ErrorMessage" style="display:none">Required.</span>	 	
                   </td>
                 <td width="25%" align="left"><a href="javascript:removeSelected('product_id_selected', vendorProductIdArray, 'p_product_id');">Remove Selected</a><br/><a href="javascript:removeAll('product_id_selected', vendorProductIdArray, 'p_product_id');">Clear All </a></td>
              </tr>
               <tr>
                 <td colspan="3" align="center"></td>
              </tr>
               
               <tr>
                 <td colspan="3" align="center"></td>
              </tr>
		  </table>
  	</xsl:template>         
</xsl:stylesheet>