<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	
	<xsl:output method="html" indent="yes"/>
	<xsl:template name="USER_ID_TEMPLATE" match="parameterValueList/list-item/displayText[. = 'USER_ID_TEMPLATE']">
		<xsl:param name="oracleRptParm"/>
		<xsl:param name="displayText"/>
			<script type="text/javascript">
			<![CDATA[
			var userIdArray=new Array();
			function addUserId(){
				var userId=document.getElementById("user_id").value;
				
				if(validatePresent("user_id", "user_id_error")){
					var url = "AddUserId.do?user_id="+userId+getSecurityParams(false);			
					//alert(url);
					userIdIFrame.location.href=url;
				}
			}

			function lookupUserId(){
				var args = new Array();
				args.action = "LookupUserId.do";
				args[0] = new Array("start_page", "1");
				args[1] = new Array("user_id", document.forms[0].user_id.value);
				args[2] = new Array("securitytoken", document.getElementById("securitytoken").value);
				args[3] = new Array("context", document.getElementById("context").value);
				var modal_dim = "dialogWidth:570px; dialogHeight:400px; dialogLeft:125; dialogTop: 190; center:yes; status=0; help:no; scroll:yes";

				var userId = window.showModalDialog("html/wait.html", args, modal_dim);
			]]>
				addCodeToOptionList(userId, userIdArray, '<xsl:value-of select="$oracleRptParm"/>', 'user_id_selected');
			}
		 	
		 </script>
   			<iframe id="userIdIFrame" name="userIdIFrame" border="0" width="0" height="0"/>
            <input type="hidden" id="{$oracleRptParm}" name="{$oracleRptParm}"/>
			<table>
               <tr>
                 <td colspan="3" align="center"></td>
              </tr>
               <tr>
                 <td colspan="3" align="center"></td>
              </tr>
               
               <tr>
              
              </tr>
               <tr>
                 <td width="45%" class="LabelRight">User ID :</td>
                 <td width="30%" align="left"><input id="user_id" name="user_id" type="text" size="25"/><span id="user_id_error" class="ErrorMessage" style="display:none">Invalid User ID</span></td>
                 <td width="25%" align="left"><a href="javascript:addUserId();">Add</a>&nbsp;&nbsp;&nbsp;<a href="javascript:lookupUserId();">Lookup User ID</a></td>
                 
              </tr>
               <tr>
                 <td width="45%" class="LabelRight">User ID Selected:</td>
                 <td width="30%" align="left">
                  
					    <select name="user_id_selected" multiple="true"></select>
						<br/><span id="{$oracleRptParm}_error" class="ErrorMessage" style="display:none">Required.</span>	 	
                   </td>
                 <td width="25%" align="left"><a href="javascript:removeSelected('user_id_selected', userIdArray, 'p_user_id');">Remove Selected</a><br/><a href="javascript:removeAll('user_id_selected', userIdArray, 'p_user_id');">Clear All </a></td>
              </tr>
               <tr>
                 <td colspan="3" align="center"></td>
              </tr>
               
               <tr>
                 <td colspan="3" align="center"></td>
              </tr>
		  </table>
  	</xsl:template>         
</xsl:stylesheet>