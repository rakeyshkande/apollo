<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	
	<xsl:output method="html" indent="yes"/>
	<xsl:template name="SOURCE_TYPE_TEMPLATE" match="parameterValueList/list-item/displayText[. = 'SOURCE_TYPE_TEMPLATE']">
		<xsl:param name="oracleRptParm"/>
		<xsl:param name="displayText"/>
			<script type="text/javascript">
			<![CDATA[
			var sourceTypeArray=new Array();
			function addSourceType(){
				var sourceType=document.getElementById("source_type").value;
				
				if(validatePresent("source_type", "source_type_error")){
					var url = "AddSourceType.do?source_type="+sourceType+getSecurityParams(false);			
					//alert(url);
					sourceTypeIFrame.location.href=url;
				}
			}

			function lookupSourceType(){
				var args = new Array();
				args.action = "LookupSourceType.do";
				args[0] = new Array("start_page", "1");
				args[1] = new Array("source_type", document.forms[0].source_type.value);
				args[2] = new Array("securitytoken", document.getElementById("securitytoken").value);
				args[3] = new Array("context", document.getElementById("context").value);
				var modal_dim = "dialogWidth:570px; dialogHeight:400px; dialogLeft:125; dialogTop: 190; center:yes; status=0; help:no; scroll:yes";

				var sourceType = window.showModalDialog("html/wait.html", args, modal_dim);
			]]>
				addCodeToOptionList(sourceType, sourceTypeArray, '<xsl:value-of select="$oracleRptParm"/>', 'source_type_selected');
			}
		 	
		 </script>
   			<iframe id="sourceTypeIFrame" name="sourceTypeIFrame" border="0" width="0" height="0"/>
            <input type="hidden" id="{$oracleRptParm}" name="{$oracleRptParm}"/>
			<table>
               <tr>
                 <td colspan="3" align="center"></td>
              </tr>
               <tr>
                 <td colspan="3" align="center"></td>
              </tr>
               
               <tr>
              
              </tr>
               <tr>
                 <td width="45%" class="LabelRight">Source Type :</td>
                 <td width="30%" align="left"><input id="source_type" name="source_type" type="text" size="25"/><span id="source_type_error" class="ErrorMessage" style="display:none">Invalid Source Type</span></td>
                 <td width="25%" align="left"><a href="javascript:addSourceType();">Add</a>&nbsp;&nbsp;&nbsp;<a href="javascript:lookupSourceType();">Lookup Source Type</a></td>
                 
              </tr>
               <tr>
                 <td width="45%" class="LabelRight">Source Type Selected:</td>
                 <td width="30%" align="left">
                  
					    <select name="source_type_selected" multiple="true"></select>
						<br/><span id="{$oracleRptParm}_error" class="ErrorMessage" style="display:none">Required.</span>	 	
                   </td>
                 <td width="25%" align="left"><a href="javascript:removeSelected('source_type_selected', sourceTypeArray, 'p_source_type');">Remove Selected</a><br/><a href="javascript:removeAll('source_type_selected', sourceTypeArray, 'p_source_type');">Clear All </a></td>
              </tr>
               <tr>
                 <td colspan="3" align="center"></td>
              </tr>
               
               <tr>
                 <td colspan="3" align="center"></td>
              </tr>
		  </table>
  	</xsl:template>         
</xsl:stylesheet>