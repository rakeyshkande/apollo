
<!DOCTYPE ACDemo [
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
	<xsl:import href="../security.xsl"/>
	<xsl:import href="startEndDateTemplate.xsl"/>
	<xsl:import href="singleDateTemplate.xsl"/>
	<xsl:import href="sourceCodeTemplate.xsl"/>
	<xsl:import href="sourceCodesTemplate.xsl"/>
	<xsl:import href="sourceTypesTemplate.xsl"/>
	<xsl:import href="sourceTypeTemplate.xsl"/>
	<xsl:import href="vendorIdTemplate.xsl"/>
	<xsl:import href="dnisIdTemplate.xsl"/>
	<xsl:import href="userIdTemplate.xsl"/>
	<xsl:import href="vendorProductIdTemplate.xsl"/>
        <xsl:import href="floristInputTemplate.xsl"/>
        <xsl:import href="vendorIdListTemplate.xsl"/>
        <xsl:import href="productIdTemplate.xsl"/>        
	<xsl:output method="html" indent="yes"/>
	<xsl:template match="/">
		<html>
			<head>
				<META http-equiv="Content-Type" content="text/html"/>
				<title>Report Submission Form</title>
				<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
				<link rel="stylesheet" type="text/css" href="css/calendar.css"/>
				<script type="text/javascript" src="js/calendar.js"/>
				<script type="text/javascript" src="js/date.js"/>
				<script type="text/javascript" src="js/FormChek.js"/>
				<script type="text/javascript" src="js/clock.js"/>
				<script type="text/javascript" src="js/dateval.js"/>
				<script type="text/javascript" src="js/reporting.js"/>
				<script type="text/javascript">/**********************************
		Reset the fields
		************************************/
		function init(){
			untabHeader();
		
		}
					<xsl:text>var reqFields = new Array(</xsl:text>
					<xsl:for-each select="root/requiredParmList/list-item">
						<xsl:variable name="required1" select="substring-before(., ',')"/>
						<xsl:variable name="required2" select="substring-after(., ',')"/>
						<xsl:choose>
							<xsl:when test="$required1!=''">'<xsl:value-of select="$required1"/>','<xsl:value-of select="$required2"/>'</xsl:when>
							<xsl:otherwise>'<xsl:value-of select="."/>'</xsl:otherwise>
						</xsl:choose>
						<xsl:if test="position() != last()">,</xsl:if>
					</xsl:for-each>
					<xsl:text>);</xsl:text><![CDATA[
	
		function validateOnSubmit(){
			var errors=0;
			var stringArray=reqFields.toString();
      setErrorCount("0");
                        for(var i=0; i<reqFields.length; i++){
				var field=reqFields[i];
				if(field.indexOf(",")!=-1){
					var tokens=field.split(",");
					for(var j=0; j<tokens.length; j++){
					    var token=trim(tokens[j]);
						//reset error field
						//alert(token+"_error");
						document.getElementById(token+"_error").style.display = "none";
						if(!validatePresent(token, token+"_error")){
							errors +=1;
              setErrorCount(errors);
						}
					}					

				}else{
					//alert(field+"_error");
					document.getElementById(field+"_error").style.display = "none";
					if(!validatePresent(field, field+"_error")){
						errors +=1;
            setErrorCount(errors);
					}

				}
			}
			
		]]>//other validations
					<xsl:for-each select="root/parameterList/list-item">
						<xsl:if test="validation !='' ">
							<xsl:variable select="oracleReportParameterName" name="parmName"/>
							<xsl:variable select="id" name="parmId"/>
							var parm='<xsl:value-of select="$parmName"/>';
							var needValidation<xsl:value-of select="$parmId"/>=true;
							if(parm.indexOf(",")!=-1){
								var parmArray=parm.split(",");
								<![CDATA[
								for(var i=0; i<	parmArray.length; i++)
								]]>
								{
								    if(!noError(parmArray[i])){
										needValidation<xsl:value-of select="$parmId"/>=false;
										break;
									}
								}							
								
							}else{
								needValidation<xsl:value-of select="$parmId"/>=noError(parm);
							}
							if(needValidation<xsl:value-of select="$parmId"/>)
							{    
									 var isOK=validate<xsl:value-of select="validation"/>('<xsl:value-of select="$parmName"/>', '<xsl:value-of select="$parmName"/>_error');
									 
									 if(!isOK){
                        errors +=1;
                        setErrorCount(errors);
									 }
								
								
							}
						</xsl:if>
					</xsl:for-each><![CDATA[

      setErrorCount(errors);
			return (errors==0);
		
		}
    
    function setErrorCount(val) {
        document.getElementById("error_count").value = val;
    }

		function noError(fieldId){
			var elm=document.getElementById(fieldId+"_error");
			return (elm==null||elm.style.display=='none');
		}

	 function doSubmission(){
		updateEdit();
		if(validateOnSubmit()){
			submitForm();
		}

	 }
   
   function submitForm() {
      document.forms[0].submit();
   }

	 
		 ]]>
				</script>
			</head>
			<body onload="init();">
				<!-- Header-->
				<xsl:call-template name="IncludeHeader"/>
				<!-- Display table-->
				<form name="form" method="post" action="SubmitReportForm.do">
					<xsl:call-template name="security"/>
					<input type="hidden" name="report_id" value="{(normalize-space(root/id))}"/>
					<input type="hidden" name="output_type" value="{(normalize-space(root/outputType))}"/>
					<input type="hidden" name="report_server" value="{(normalize-space(root/serverName))}"/>
					<input type="hidden" name="file_prefix" value="{(normalize-space(root/filePrefix))}"/>
					<input type="hidden" name="oracle_rdf" value="{(normalize-space(root/oracleRDFName))}"/>
          <input type="hidden" name="error_count" value="0"/>

					<table width="98%" class="mainTable">
						<tr>
							<td>
								<table width="100%" align="center" class="innerTable">
									<tr>
										<td colspan="4" align="center" class="PopupHeader">
											<xsl:value-of select="normalize-space(root/name)"/>
										</td>
									</tr>
									<tr>
										<td colspan="4" align="center" class="TotalLine">
											<strong>Report Description</strong>
										</td>
									</tr>
									<tr>
										<td colspan="4" align="center" class="bluetext">
											<xsl:value-of select="normalize-space(root/description)"/>
										</td>
									</tr>
									<tr>
										<td colspan="4" align="center"></td>
									</tr>
									<tr>
										<td colspan="4" align="center" class="TotalLine">
											<strong>Selection Criteria</strong>
										</td>
									</tr>
									<tr>
										<td colspan="4" align="center" class="LabelRight">
											<p>&nbsp;</p>
										</td>
									</tr>
									<xsl:for-each select="root/parameterList/list-item">
										
											<xsl:variable name="parmType" select="normalize-space(type)"/>
											<xsl:choose>
												<xsl:when test="$parmType = 'TE'">
                        <tr>
													<td align="center">
														<xsl:apply-templates select="parameterValueList/list-item/displayText">
															<xsl:with-param name="oracleRptParm" select="oracleReportParameterName"/>
															<xsl:with-param name="displayText" select="displayName"/>
															<xsl:with-param name="allowFutureDate" select="allowFutureDate"/>
														</xsl:apply-templates>
													</td>
                        </tr>
												</xsl:when>
												
												<xsl:otherwise>
                                                                                                    <tr>
													<xsl:apply-templates select="type">
														<xsl:with-param name="oracleParm" select="normalize-space(oracleReportParameterName)"/>
													</xsl:apply-templates>
                                                                                                    </tr>
												</xsl:otherwise>
											</xsl:choose>
										
									</xsl:for-each>


									<tr class="Label">
										<td colspan="4" align="center">
											<p>&nbsp;</p>
										</td>
									</tr>
									<tr class="Label">
										<td colspan="4" align="center">
											<p>&nbsp;</p>
										</td>
									</tr>
									<tr class="Label">
										<td colspan="4" align="center">
											<p>
												<xsl:if test="normalize-space(root/notes)">Note: <xsl:value-of select="root/notes"/></xsl:if>
											</p>
										</td>
									</tr>
									<tr class="Label">
										<td colspan="4" align="center">
											<p>&nbsp;</p>
										</td>
									</tr>
									<tr>
										<td colspan="4" align="center"></td>
									</tr>
									<tr>
										<td colspan="4" align="center">
											<input type="button" class="blueButton" name="submitButton" accesskey="S" value="(S)ubmit" onClick="doSubmission();"/>
										</td>
									</tr>
									<tr>
										<td colspan="3" align="center"></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<table width="98%" border="0" align="center" cellpadding="0" cellspacing="1">
						<tr>
							<td width="100%">
								<div align="center">
								</div>
							</td>
							<td width="9%" align="right">
								<button class="BlueButton" name="main_menu" accesskey="M" onclick="javascript:doReportingConfirmMainMenu();">(M)ain Menu</button>
							</td>
						</tr>
					</table>

					<!--Copyright bar-->
					<xsl:call-template name="footer"/>
				</form>
			</body>
		</html>
	</xsl:template>
	<!--to get the header title-->
	<xsl:template name="IncludeHeader">
		<xsl:call-template name="header">
			<xsl:with-param name="headerName" select="'Report Submission Form'"/>
			<xsl:with-param name="buttonName" select="'back_confirm'"/>
		</xsl:call-template>
	</xsl:template>
	<!--end-->

	<xsl:template name="Radio" match="type[. ='R']">
		<xsl:param name="oracleParm"/>
		<td align="center">
			<table>
				<tr>
					<td align="left" class="LabelRight" width="40%">
						<xsl:value-of select="normalize-space(../displayName)"/>:</td>
					<td align="left" width="30%">

						<xsl:for-each select="../parameterValueList/list-item">
							<input type="radio" class="select" name="{$oracleParm}" value="{normalize-space(defaultFormValue)}"  onKeyPress="return noEnter();" onclick="updateEdit()"/>
							<xsl:value-of select="displayText"/>
						</xsl:for-each>
					</td>
					<td align="left" width="30%">
						<div>
							<span id="{$oracleParm}_error" class="ErrorMessage" style="display:none">Invalid</span>
						</div>
					</td>
				</tr>
			</table>
		</td>
	</xsl:template>
	<xsl:template name="TextField" match="type[. ='T']">
		<xsl:param name="oracleParm"/>
		<td align="center">
			<table>
				<tr>
					<td align="left" class="LabelRight" width="48%">
						<xsl:value-of select="normalize-space(../displayName)"/>:</td>
					<td align="left" width="22%">
						<input type="text" name="{$oracleParm}" value="{../parameterValueList/list-item/defaultFormValue}" onfocus="updateEdit();" onblur="updateEdit();" onKeyPress="return noEnter();"/>
					</td>
					<td align="left" width="30%">
						<div>
							<span id="{$oracleParm}_error" class="ErrorMessage" style="display:none">Invalid</span>
						</div>
					</td>
				</tr>
			</table>
		</td>
	</xsl:template>


	<xsl:template name="ListBox" match="type[. ='MS']">
		<xsl:param name="oracleParm"/>
		<td align="center">
			<table>
				<tr>
					<td align="left" class="LabelRight" width="48%">
						<xsl:value-of select="normalize-space(../displayName)"/>:</td>
					<td align="left" width="22%">
						<select multiple="true" name="{$oracleParm}" class="select" onchange="updateEdit()">
							<xsl:choose>
								<xsl:when test="../parameterValueList/list-item/optionMap !=''">
									<option value="{../parameterValueList/list-item/defaultFormValue}" selected="selected">
										<xsl:value-of select="../parameterValueList/list-item/defaultFormValue"/>
									</option>
									<xsl:for-each select="../parameterValueList/list-item/optionMap/map-item">

										<option value="{normalize-space(name)}">
											<xsl:value-of select="normalize-space(value)"/>
										</option>
									</xsl:for-each>
								</xsl:when>
								<xsl:otherwise>
									<xsl:for-each select="../parameterValueList/list-item">
										<option value="{normalize-space(defaultFormValue)}">
											<xsl:value-of select="normalize-space(displayText)"/>
										</option>
									</xsl:for-each>
								</xsl:otherwise>
							</xsl:choose>
						</select>
					</td>
					<td align="left" width="30%">
						<div>
							<span id="{$oracleParm}_error" class="ErrorMessage" style="display:none">Invalid</span>
						</div>
					</td>
				</tr>
			</table>
		</td>
	</xsl:template>


	<xsl:template name="DropDown" match="type[. ='S']">
		<xsl:param name="oracleParm"/>
		<td align="center">
			<table>
				<tr>
					<td align="left" class="LabelRight" width="48%">
						<xsl:value-of select="normalize-space(../displayName)"/>:</td>
					<td align="left" width="22%">
						<select class="select" name="{$oracleParm}" onchange="updateEdit()">
							<xsl:choose>
								<xsl:when test="../parameterValueList/list-item/optionMap !=''">
                                                                    <xsl:if test="../parameterValueList/list-item/defaultFormValue !=''">
									<option value="{../parameterValueList/list-item/defaultFormValue}" selected="selected">
										<xsl:value-of select="../parameterValueList/list-item/defaultFormValue"/>
									</option>
                                                                    </xsl:if>
									<xsl:for-each select="../parameterValueList/list-item/optionMap/map-item">

										<option value="{normalize-space(name)}">
											<xsl:value-of select="normalize-space(value)"/>
										</option>
									</xsl:for-each>
								</xsl:when>
								<xsl:otherwise>
									<xsl:for-each select="../parameterValueList/list-item">
										<option value="{normalize-space(defaultFormValue)}">
											<xsl:value-of select="normalize-space(displayText)"/>
										</option>
									</xsl:for-each>
								</xsl:otherwise>
							</xsl:choose>
						</select>
					</td>
					<td align="left" width="30%">
						<div>
							<span id="{$oracleParm}_error" class="ErrorMessage" style="display:none">Invalid</span>
						</div>
					</td>
				</tr>
			</table>
		</td>
	</xsl:template>
</xsl:stylesheet>