
<!DOCTYPE ACDemo [
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output method="html" indent="yes"/>
	<xsl:template name="FLORIST_INPUT_TEMPLATE" match="parameterValueList/list-item/displayText[. = 'FLORIST_INPUT_TEMPLATE']">
                <xsl:param name="oracleRptParm"/>
		<xsl:param name="displayText"/>
		<xsl:param name="allowFutureDate"/>
		<script type="text/javascript">
		 <![CDATA[
      function changeTextboxSize() {
          document.getElementById("p_data").value = "";

          var optionBox = document.getElementById("p_option").options;
          var selectedValue = optionBox[optionBox.selectedIndex].value;
          var newMaxLength = 9;

          if (selectedValue == 'P') {
              newMaxLength = 7;
          } else if(selectedValue == 'A') {
              newMaxLength = 4;
          }

          document.getElementById("p_data").setAttribute('maxLength', newMaxLength);
          document.getElementById("p_data").focus();
      }

      function validateFloristInputFormat(option, data) {
          var validFlorist = false;
          var regExp;

          if(option == "N") {
              regExp = /^\d{2}-?\d{4}[a-zA-Z]{2}$/;
          } else if(option == "P") {
              regExp = /^\d{2}-?\d{4}$/;
          } else if(option == "A") {
              regExp = /^\d{4}$/;
          }

          return regExp.test(data);

      }

      function validateFloristInput(){

				if(validatePresent("p_data", "p_data_error")){
          var optionBox = document.getElementById("p_option").options;
          var v_option = optionBox[optionBox.selectedIndex].value;
          var v_data = document.getElementById("p_data").value;
          var v_error_count = document.getElementById("error_count").value;
          
					v_data = v_data.toUpperCase();
          document.getElementById("p_data").value = v_data;
          var url = "FloristAction.do?p_option=" + v_option +"&p_data="+v_data+"&error_count="+v_error_count + getSecurityParams(false);
          if(validateFloristInputFormat(v_option, v_data)) {
              document.getElementById("p_data_error").firstChild.nodeValue="Not Found.";
              floristInputIFrame.location.href=url;
          } else {
              document.getElementById("p_data_error").firstChild.nodeValue="Invalid Format.";
				  	  document.getElementById("p_data_error").style.display = "block";
          }

				}
        
      }
      ]]>
    </script>
   			<iframe id="floristInputIFrame" name="floristInputIFrame" border="0" width="0" height="0"/>
    <table>
			<tr align="center">
				<td class="LabelRight" width="47%">Option:</td>
				<td align="left">
          <select class="select" id="p_option" name="p_option" onChange="javascript:updateEdit();changeTextboxSize()">
            <option value="N">By Florist Number</option>
            <option value="P">By Florist Prefix Number</option>
            <option value="A">By Central Account Number</option>
          </select>
        </td>
        <td align="left" width="30%">
					<div>
						<span id="p_option_error" class="ErrorMessage" style="display:none">Invalid Option</span>
					</div>
				</td>
      </tr>
      <tr>
        <td class="LabelRight" width="47%">Florist:</td>
				<td align="left"><input type="text" id="p_data" name="p_data" maxlength="9" onfocus="updateEdit()"  onKeyPress="return noEnter();"/>
				</td>
        <td align="left" width="30%">
					<div>
						<span id="p_data_error" class="ErrorMessage" style="display:none">Not Found</span>
					</div>
				</td>
			</tr>

		</table>

	</xsl:template>

</xsl:stylesheet>