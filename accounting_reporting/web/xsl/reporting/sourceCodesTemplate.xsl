<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	
	<xsl:output method="html" indent="yes"/>
	<xsl:template name="SOURCE_CODES_TEMPLATE" match="parameterValueList/list-item/displayText[. = 'SOURCE_CODES_TEMPLATE']">
		<xsl:param name="oracleRptParm"/>
		<xsl:param name="displayText"/>
			<script type="text/javascript">
			<![CDATA[
			var sourceCodeArray=new Array();
			function addSourceCode(){
				var sourceCode=document.getElementById("source_code").value;
				var sourceCodes = sourceCode.split(" ");
				sourceCodes = getUnique(sourceCodes);
				var selectedSourceCodes = document.getElementById("source_code_selected");
				var tempSourceCodes = [];
				var exists = "F";
				
				if(selectedSourceCodes.length == 0){
					for(var i=0; i<sourceCodes.length;i++){
						tempSourceCodes.push(sourceCodes[i]);
					}
								
				}
				else{
					for(var i=0;i<sourceCodes.length;i++){
						for(var j=0;j<selectedSourceCodes.length;j++){
								if(selectedSourceCodes[j].value == sourceCodes[i]){
									exists = "T";
								}
								
							}
							if(exists != "T"){
							 tempSourceCodes.push(sourceCodes[i]);
							}
							exists = "F";
					}
				}
				
				
				for(var i=0;i<tempSourceCodes.length;i++){
					var option = document.createElement('option');
        			option.text = option.value = tempSourceCodes[i];
        			selectedSourceCodes.add(option);
        		}
				
				setSourceCodes();
					
				
			}
			
			function getUnique(input) {
    			var a = [];
    			for ( i = 0; i < input.length; i++ ) {
        			var current = input[i];
        			if (!contains(a,current)) a.push(current);
    			}
			    return a;
			}
			
			function contains(a, obj) {
				for (var i = 0; i < a.length; i++) {
					if (a[i] === obj) {
						return true;
					}
    			}
    			return false;
			}
			
			function setSourceCodes(){
				var vc = document.getElementById("source_code_selected");
  				var sourceCodesChosen = ""; 
  
  					for (var i = 0; i < vc.length; i++)
  						{
	  						sourceCodesChosen += vc[i].value + ","; 
  						}

  					sourceCodesChosen = sourceCodesChosen.substring(0,sourceCodesChosen.length - 1);
  					sourceCodeArray = sourceCodesChosen;
  					document.getElementById("p_source_codes").value = sourceCodesChosen;
  					
  					]]>
			}
			
			
			
		 </script>
   			<iframe id="sourceCodeIFrame" name="sourceCodeIFrame" border="0" width="0" height="0"/>
            <input type="hidden" id="{$oracleRptParm}" name="{$oracleRptParm}"/>
			<table>
               <tr>
                 <td colspan="3" align="center"></td>
              </tr>
               <tr>
                 <td colspan="3" align="center"></td>
              </tr>
               
               <tr>
              
              </tr>
               <tr>
                 <td width="45%" class="LabelRight">Source Code :</td>
                 <td width="30%" align="left"><input id="source_code" name="source_code" type="text" size="25"/><span id="source_code_error" class="ErrorMessage" style="display:none">Invalid Source Code</span></td>
                 <td width="25%" align="left"><a href="javascript:addSourceCode();">Add</a></td>
                 
              </tr>
               <tr>
                 <td width="45%" class="LabelRight">Source Code Selected:</td>
                 <td width="30%" align="left">
                  
					    <select id="source_code_selected" name="source_code_selected" multiple="true"></select>
						<br/><span id="{$oracleRptParm}_error" class="ErrorMessage" style="display:none">Required.</span>	 	
                   </td>
                 <td width="25%" align="left"><a href="javascript:removeSelected('source_code_selected', sourceCodeArray, 'p_source_codes');">Remove Selected</a><br/><a href="javascript:removeAll('source_code_selected', sourceCodeArray, 'p_source_codes');">Clear All </a></td>
              </tr>
               <tr>
                 <td colspan="3" align="center"></td>
              </tr>
               
               <tr>
                 <td colspan="3" align="center"></td>
              </tr>
		  </table>
  	</xsl:template>         
</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2005. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition></MapperMetaTag>
</metaInformation>
-->