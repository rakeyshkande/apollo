<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	
	<xsl:output method="html" indent="yes"/>
	<xsl:template name="SOURCE_TYPES_TEMPLATE" match="parameterValueList/list-item/displayText[. = 'SOURCE_TYPES_TEMPLATE']">
		<xsl:param name="oracleRptParm"/>
                <xsl:param name="displayText"/>
                <script type="text/javascript">
                	var sourceTypeArray=new Array();
                </script>
            <!--p_source_types-->
            <input type="hidden" id="{$oracleRptParm}" name="{$oracleRptParm}"/>
            <table>     
                <tr>
                    <td width="45%" class="LabelRight"><xsl:value-of select="$displayText"/>:</td>
                    <td width="30%" align="left"><select name="sourceTypesAvailable" id="sourceTypesAvailable">
                                        <xsl:attribute name="multiple"/>
                                        <xsl:for-each select="../optionMap/map-item">
                                            <option value="{name}">
                                                <xsl:value-of select="value"/>
                                            </option>
                                        </xsl:for-each>
                                    </select>
                                    <br/><span id="{$oracleRptParm}_error" class="ErrorMessage" style="display:none">Required.</span>
                    	</td>	
                 	<td width="25%" align="left"><a href="javascript:moveSourceTypesSelected();">Add</a></td>
                </tr>
                 <tr>
                 <td width="45%" class="LabelRight">Source Type Selected:</td>
                 <td width="30%" align="left">
                  
					    <select name="source_type_selected" id="source_type_selected">
                                         <xsl:attribute name="multiple"/>
                                      </select>
							 	
                   </td>
                 <td width="25%" align="left"><a href="javascript:removeSelected('source_type_selected', sourceTypeArray, 'p_source_types');">Remove Selected</a><br/><a href="javascript:removeAll('source_type_selected', sourceTypeArray, 'p_source_types');">Clear All </a></td>
                 
              </tr>
                
                    
            </table>
            
            <table>
            <tr>
            	<td width="45%" class="LabelRight">&nbsp;</td>
                <td width="60%" align="left"><span id="source_type_error" class="ErrorMessage" style="display:none">You can only select 15 Source Types when generating this report. You have exceeded this number.</span></td>
            </tr>
            </table>
  	</xsl:template>         
</xsl:stylesheet>