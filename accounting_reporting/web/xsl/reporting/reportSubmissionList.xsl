
<!DOCTYPE ACDemo [
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
	<xsl:import href="../security.xsl"/>
	<xsl:output method="html" indent="yes"/>
	<xsl:template match="/">
		<html>
			<head>
				<META http-equiv="Content-Type" content="text/html"/>
				<title>Report Submission List</title>
				<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
				<link rel="stylesheet" type="text/css" href="css/calendar.css"/>
				<script type="text/javascript" src="js/calendar.js"/>
				<script type="text/javascript" src="js/date.js"/>
				<script type="text/javascript" src="js/FormChek.js"/>
				<script type="text/javascript" src="js/clock.js"/>
				<script type="text/javascript" src="js/dateval.js"/>
				<script type="text/javascript" src="js/util.js"/>
				<script type="text/javascript"><![CDATA[
		function openForm(reportId){
		  
		  var url="BuildReportForm.do?report_id="+reportId+getSecurityParams(false);
		  document.forms[0].action=url;
		  document.forms[0].submit();
		
		}
		
			function init(){
				untabHeader();
		
			}
 	
 	]]>
				</script>
			</head>
			<body onload="init();">
				<!-- Header-->
				<xsl:call-template name="IncludeHeader"/>
				<!-- Display table-->
				<form method="post">
					<xsl:call-template name="security"/>

					<table width="98%" align="center" cellspacing="1" class="mainTable">
						<tr>
							<td>

								<table width="100%" align="center" class="innerTable">
									<xsl:for-each select="reportList/category">
										<xsl:if test="normalize-space(.)">
											<tr>
												<td width="93%" align="center">
													<div class="PopupHeader size10">
														<strong>
															<xsl:value-of select="normalize-space(@category_desc)"/>
														</strong>
													</div>
												</td>
											</tr>
											<tr>
												<td align="center"></td>
											</tr>
											<xsl:for-each select="reports/report">
												<xsl:variable select="report_id" name="reportId"/>
												<tr>
													<td align="center">
														<a href="javascript:openForm({$reportId});" class="size9">
															<xsl:value-of select="name"/>
														</a>
													</td>
												</tr>
											</xsl:for-each>
											<tr>
												<td align="center"></td>
											</tr>
										</xsl:if>
									</xsl:for-each>
								</table>
							</td>
						</tr>
					</table>
					<table width="98%" border="0" align="center" cellpadding="0" cellspacing="1">
						<tr>
							<td width="100%" align="center"></td>
							<td width="9%" align="right">
								<button class="BlueButton" name="main_menu" accesskey="M" onclick="javascript:doReportingMainMenu();">(M)ain Menu</button>
							</td>
						</tr>
					</table>
				</form>
				<!--Copyright bar-->
				<xsl:call-template name="footer"/>
			</body>
		</html>
	</xsl:template>

	<!--to get the header title-->
	<xsl:template name="IncludeHeader">
		<xsl:call-template name="header">
			<xsl:with-param name="headerName" select="'Report Submission List'"/>
			<xsl:with-param name="buttonName" select="''"/>
		</xsl:call-template>
	</xsl:template>
	<!--end-->
</xsl:stylesheet>