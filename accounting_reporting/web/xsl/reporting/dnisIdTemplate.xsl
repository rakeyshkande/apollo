<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	
	<xsl:output method="html" indent="yes"/>
	<xsl:template name="DNIS_ID_TEMPLATE" match="parameterValueList/list-item/displayText[. = 'DNIS_ID_TEMPLATE']">
		<xsl:param name="oracleRptParm"/>
		<xsl:param name="displayText"/>
			<script type="text/javascript">
			<![CDATA[
			var dnisIdArray=new Array();
			function addDnisId(){
				var dnisId=document.getElementById("dnis_id").value;
				
				if(validatePresent("dnis_id", "dnis_id_error")){
					var url = "AddDnisId.do?dnis_id="+dnisId+getSecurityParams(false);			
					//alert(url);
					dnisIdIFrame.location.href=url;
				}
			}

			function lookupDnisId(){
				var args = new Array();
				args.action = "LookupDnisId.do";
				args[0] = new Array("start_page", "1");
				args[1] = new Array("dnis_id", document.forms[0].dnis_id.value);
				args[2] = new Array("securitytoken", document.getElementById("securitytoken").value);
				args[3] = new Array("context", document.getElementById("context").value);
				var modal_dim = "dialogWidth:570px; dialogHeight:400px; dialogLeft:125; dialogTop: 190; center:yes; status=0; help:no; scroll:yes";

				var dnisId = window.showModalDialog("html/wait.html", args, modal_dim);
			]]>
				addCodeToOptionList(dnisId, dnisIdArray, '<xsl:value-of select="$oracleRptParm"/>', 'dnis_id_selected');
			}
		 	
		 </script>
   			<iframe id="dnisIdIFrame" name="dnisIdIFrame" border="0" width="0" height="0"/>
            <input type="hidden" id="{$oracleRptParm}" name="{$oracleRptParm}"/>
			<table>
               <tr>
                 <td colspan="3" align="center"></td>
              </tr>
               <tr>
                 <td colspan="3" align="center"></td>
              </tr>
               
               <tr>
              
              </tr>
               <tr>
                 <td width="45%" class="LabelRight">DNIS ID :</td>
                 <td width="30%" align="left"><input id="dnis_id" name="dnis_id" type="text" size="25"/><span id="dnis_id_error" class="ErrorMessage" style="display:none">Invalid DNIS ID</span></td>
                 <td width="25%" align="left"><a href="javascript:addDnisId();">Add</a>&nbsp;&nbsp;&nbsp;<a href="javascript:lookupDnisId();">Lookup DNIS ID</a></td>
                 
              </tr>
               <tr>
                 <td width="45%" class="LabelRight">DNIS ID Selected:</td>
                 <td width="30%" align="left">
                  
					    <select name="dnis_id_selected" multiple="true"></select>
						<br/><span id="{$oracleRptParm}_error" class="ErrorMessage" style="display:none">Required.</span>	 	
                   </td>
                 <td width="25%" align="left"><a href="javascript:removeSelected('dnis_id_selected', dnisIdArray, 'p_dnis_id');">Remove Selected</a><br/><a href="javascript:removeAll('dnis_id_selected', dnisIdArray, 'p_dnis_id');">Clear All </a></td>
              </tr>
               <tr>
                 <td colspan="3" align="center"></td>
              </tr>
               
               <tr>
                 <td colspan="3" align="center"></td>
              </tr>
		  </table>
  	</xsl:template>         
</xsl:stylesheet>