<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" indent="yes"/>

<xsl:template match="/root">

  <html>
    <head>
      <script type="text/javascript" language="javascript">
        forwardAction();
	
		function forwardAction()
		{
			//get status
			var status = "<xsl:value-of select='OUT_STATUS'/>";
				
			if(status=='SUCCESS')
			{
				//open success page.
			}
			else
			{
				//parent.hideWaitMessage("content", "wait");
				//display error message on the parent page.
				<xsl:for-each select="errors">
				
				</xsl:for-each>	

			} 
        }
      </script>
    </head>
    <body></body>
  </html>
</xsl:template>

</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2005. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\..\xml\iframe.xml" htmlbaseurl="" outputurl="" processortype="internal" profilemode="0" profiledepth="" profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition></MapperMetaTag>
</metaInformation>
-->