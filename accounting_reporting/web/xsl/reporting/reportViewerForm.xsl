
<!DOCTYPE ACDemo [
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
	<xsl:import href="../security.xsl"/>
	<xsl:output method="html" indent="yes"/>
	<xsl:template match="/">
		<html>
			<head>
				<META http-equiv="Content-Type" content="text/html"/>
				<title>Report Viewer Form</title>
				<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
				<link rel="stylesheet" type="text/css" href="css/calendar.css"/>
				<script type="text/javascript" src="js/calendar.js"/>
				<script type="text/javascript" src="js/date.js"/>
				<script type="text/javascript" src="js/FormChek.js"/>
				<script type="text/javascript" src="js/clock.js"/>
				<script type="text/javascript" src="js/dateval.js"/>
				<script type="text/javascript" src="js/reporting.js"/>
				<script type="text/javascript"><![CDATA[

		function init(){
			untabHeader();
		
		}

var dataFields = new Array('start_date','end_date');

var spans = new Array('start_date_error','end_date_error');

function resetErrorFields()
{
		for(var i = 0; i < spans.length; i++)
		{
				var spanField = spans[i];
				var dateField = dataFields[i];
				document.getElementById(dateField).style.backgroundColor = "#FFFFFF";
				document.getElementById(spanField).style.display = "none";

		}
}
/**********************************
The SUBMIT function
************************************/

function doFormSubmit()
{
	    resetErrorFields();
        if(validatePresent("start_date","start_date_error")&&validatePresent("end_date","end_date_error"))
		{
			if (checkDateFormat("start_date")&&checkDateFormat("end_date")&&checkDateFormat("start_date", "end_date",''))
			{
		
				document.forms[0].submit();
	        }
		}
        
}      		
 ]]>
				</script>
			</head>
			<body onload="init();">
				<!-- Header-->
				<xsl:call-template name="IncludeHeader"/>
				<!-- Display table-->
				<form name="form" action="GetReportViewerList.do">
					<xsl:call-template name="security"/>
					<table width="98%" align="center" cellspacing="1" class="mainTable">
						<tr>
							<td>
								<table width="100%" align="center" class="innerTable">
									<tr>
										<td colspan="4" align="center"/>
									</tr>
									<tr>
										<td colspan="4" align="center" class="TotalLine">
											<strong>Selection Criteria</strong>
										</td>
									</tr>
									<tr>
										<td colspan="4" align="center" class="LabelRight">
											<p>&nbsp;</p>
										</td>
									</tr>
									<tr>
										<td width="48%" class="LabelRight">Start Date :</td>
										<td width="16%" align="left">
											<input type="text" id="start_date" name="start_date" maxlength="10" value="{root/start_date}" onKeyPress="return noEnter();"/>
										</td>
										<td width="6%" align="left">
											<input type="image" id="start_date_img" src="images/calendar.gif" width="16" height="16"/>
										</td>
										<td width="30%" align="left">
											<div>
												<span id="start_date_error" class="ErrorMessage" style="display:none">Invalid Date</span>
											</div>
										</td>
									</tr>
									<tr>
										<td class="LabelRight">End Date :</td>
										<td align="left">
											<input type="text" id="end_date" name="end_date" maxlength="10" value="{root/end_date}" onKeyPress="return noEnter();"/>
										</td>
										<td align="left">
											<input type="image" id="end_date_img" src="images/calendar.gif" width="16" height="16"/>
										</td>
										<td>
											<div>
												<span id="end_date_error" class="ErrorMessage" style="display:none">Invalid Date</span>
											</div>
										</td>
									</tr>
									<tr class="Label">
										<td colspan="4" align="center">
											<p>&nbsp;</p>
										</td>
									</tr>
									<tr class="Label">
										<td colspan="4" align="center">
											<p>&nbsp;</p>
										</td>
									</tr>
									<tr>
										<td colspan="4" align="center">
											<input type="button" class="blueButton" name="submitButton" accesskey="S" value="(S)ubmit" onClick="doFormSubmit();"/>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<table width="98%" border="0" align="center" cellpadding="0" cellspacing="1">
						<tr>
							<td width="100%">
								<div align="center"></div>
							</td>
							<td width="9%" align="right">
								<button class="BlueButton" name="main_menu" accesskey="M" onclick="javascript:doReportingMainMenu();">(M)ain Menu</button>
							</td>
						</tr>
					</table>
					<!--Copyright bar-->
					<xsl:call-template name="footer"/>
				</form>
			</body>
		</html>
		<script type="text/javascript">Calendar.setup(
    {
      inputField  : "start_date",  // ID of the input field
      ifFormat    : "mm/dd/y",  // the date format
      button      : "start_date_img"  // ID of the button
    }
  );</script>
		<script type="text/javascript">Calendar.setup(
    {
      inputField  : "end_date",  // ID of the input field
      ifFormat    : "mm/dd/y",  // the date format
      button      : "end_date_img"  // ID of the button
    }
  );</script>
	</xsl:template>
	<!--to get the header title-->
	<xsl:template name="IncludeHeader">
		<xsl:call-template name="header">
			<xsl:with-param name="headerName" select="'Search Report'"/>
			<xsl:with-param name="buttonName" select="''"/>
		</xsl:call-template>
	</xsl:template>
	<!--end-->
</xsl:stylesheet>