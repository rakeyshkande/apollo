
<!DOCTYPE ACDemo [
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output method="html" indent="yes"/>
	<xsl:template name="START_END_DATE_TEMPLATE" match="parameterValueList/list-item/displayText[. = 'START_END_DATE_TEMPLATE']">
		<xsl:param name="allowFutureDate"/>
		<table>
			<tr>
				<td class="LabelRight" width="48%">Start Date:</td>
				<td align="left" width="16%">
					<input type="text" id="p_start_date" name="p_start_date" maxlength="10" value="{/root/default-date}" onfocus="updateEdit()" onblur="updateEdit()" onKeyPress="return noEnter();"/>
				</td>
				<td align="left" width="6%">
					<input type="image" id="start_date_img" src="images/calendar.gif" width="16" height="16"/>
				</td>
				<td align="left" width="30%">
					<div>
						<span id="p_start_date_error" class="ErrorMessage" style="display:none">Invalid Date</span>
					</div>
				</td>
				<script type="text/javascript">Calendar.setup(
				    {
				      inputField  : "p_start_date",  // ID of the input field
				      ifFormat    : "mm/dd/y",  // the date format
				      button      : "start_date_img"  // ID of the button
				    }
				  );</script>
			</tr>
			<tr>
				<td class="LabelRight" width="48%">End Date:</td>
				<td align="left" width="16%">
					<input type="text" id="p_end_date" name="p_end_date" maxlength="10" value="{/root/default-date}" onfocus="updateEdit()" onblur="updateEdit()" onKeyPress="return noEnter();"/>
				</td>
				<td align="left" width="6%">
					<input type="image" id="end_date_img" src="images/calendar.gif" width="16" height="16" onclick="updateEdit();"/>
				</td>
				<td width="30%">
					<div>
						<span id="p_end_date_error" class="ErrorMessage" style="display:none">Invalid Date</span>
					</div>
				</td>
				<script type="text/javascript">Calendar.setup(
				    {
				      inputField  : "p_end_date",  // ID of the input field
				      ifFormat    : "mm/dd/y",  // the date format
				      button      : "end_date_img"  // ID of the button
				    }
				  );</script>
			</tr>
		</table>
		<script type="text/javascript">
    var days='<xsl:value-of select="/root/daysSpan"/>';
    <![CDATA[
                        // Dates are optional
                        function validateStartDateEndDateOpt(){
                                  var validDt = true;
                                  var entry = document.getElementById("p_start_date").value;
                                  
                                  if(entry != null && !checkDateFormat("p_start_date")){
                                        validDt = false;
                                  }
                                  entry = document.getElementById("p_end_date").value;
				  if(entry != null && !checkDateFormat("p_end_date")){
                                        validDt= false;
                                  }

                                return validDt;
			}
                        
			function validateStartDateEndDate(){
			
				    var validDt=true;
					if(!checkDateFormat("p_start_date")){
						validDt= false;
					}

					if(!checkDateFormat("p_end_date")){
						validDt= false;
					}

				//	alert(validDt);
					if(validDt){
							if(!checkenddate("p_start_date", "p_end_date", days)){
								validDt= false;
							}
					
					
			]]>
					
					<xsl:if test="$allowFutureDate ='false'">
			<![CDATA[
							//alert("eod required ");
			          
							if(isPastYesterday("p_start_date")){
								validDt= false;
							}
							
							if(isPastYesterday("p_end_date")){
								
								validDt= false;
							}
			]]>
						</xsl:if>
					  }

				//alert(validDt);
				return validDt;
		
			}
      
			function validateStartDateEndDateCS02(){
				  var validDt = validateStartDateEndDate();
          
                                  if(validDt) {
                                      //check if start date is more than 2 year back
                                      validDt = isDateTooFarBack("p_start_date", "M", 24);
                                  }

                                return validDt;
			}    
                        

		 </script>
	</xsl:template>
</xsl:stylesheet>