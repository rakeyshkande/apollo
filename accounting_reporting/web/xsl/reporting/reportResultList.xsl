
<!DOCTYPE ACDemo [
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
	<xsl:import href="../security.xsl"/>
	<xsl:output method="html" indent="yes"/>
	<xsl:template match="/">
		<html>
			<head>
				<META http-equiv="Content-Type" content="text/html"/>
				<title>Report Submission List</title>
				<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
				<link rel="stylesheet" type="text/css" href="css/calendar.css"/>
				<script type="text/javascript" src="js/calendar.js"/>
				<script type="text/javascript" src="js/date.js"/>
				<script type="text/javascript" src="js/FormChek.js"/>
				<script type="text/javascript" src="js/clock.js"/>
				<script type="text/javascript" src="js/util.js"/>
				<script type="text/javascript"><![CDATA[
			
			function init(){
				untabHeader();
		
			}
			
			function getReportFile(submissionId){
				var outType="";
				//get type value for the give submission id.
				var typeName="output_type_"+submissionId;
				var fileName="report_name_"+submissionId;
				var reportName=document.forms[0].elements[fileName].value;
				//alert(name);
				var outTypeElm=document.forms[0].elements[typeName];
				if(outTypeElm.length==null){
					outType=outTypeElm.value;
				}else{
					for(var i=0; i<outTypeElm.length; i++){
						if(outTypeElm[i].checked){
							outType=outTypeElm[i].value;
							break;
						}
					}
				}
				//alert(outType);
				
				var url="GetGeneratedReport.do?submission_id="+submissionId+"&output_type="+outType+"&report_name="+reportName+getSecurityParams(false);
				document.forms[0].action = url;
				document.forms[0].submit();
			}
			
			
			
 		]]>
				</script>
			</head>
			<body onload="init();">
				<!-- Header-->
				<xsl:call-template name="IncludeHeader"/>
				<!-- Display table-->
				<form name="form" method="post">
					<xsl:call-template name="security"/>
					<input type="hidden" name="start_date" value="{report-result-list/start_date}"/>
					<input type="hidden" id="end_date" name="end_date" value="{report-result-list/end_date}"/>
					<table width="98%" align="center" cellspacing="1" class="mainTable">
						<tr>
							<td>
								<table width="100%" align="center" class="innerTable">
									<tr>
										<td colspan="8" align="center" class="TotalLine">
											<strong>Report List</strong>
										</td>
									</tr>
									<tr class="Label">
										<td width="14%" align="center">Category Name</td>
										<td width="22%" align="center">Format</td>
										<td width="14%" align="center">Report Name</td>
										<td width="13%" align="center">Submission Time</td>
										<td width="13%" align="center">Completion Time</td>
										<td width="13%" align="center">Last Accessed Time</td>
										<td width="10%" align="center">Status</td>
										<td width="14%" align="center">Error</td>
									</tr>
									<tr>
										<td colspan="8" align="center"></td>
									</tr>
									<xsl:for-each select="report-result-list/report">
										<xsl:variable name="type" select="submitted_output_type"/>
										<xsl:variable name="status" select="report_status"/>
										<input type="hidden" name="report_name_{report_submission_id}" value="{report_file_name}"/>
										<tr>
											<td align="center">
												<span class="size9">
													<xsl:value-of select="category_description"/>
												</span>
											</td>
											<td align="center" class="size9">
												<xsl:choose>
													<xsl:when test="$type='Both'">
														<input name="output_type_{report_submission_id}" type="radio" value="Char" checked="checked">Excel</input>
														<input name="output_type_{report_submission_id}" type="radio" value="Bin">PDF</input>
													</xsl:when>
													<xsl:when test="$type='Bin'">
														<input name="output_type_{report_submission_id}" type="radio" value="Bin" checked="checked">PDF</input>
													</xsl:when>
													<xsl:otherwise>
														<input name="output_type_{report_submission_id}" type="radio" value="Char" checked="checked">Excel</input>
													</xsl:otherwise>
												</xsl:choose>
											</td>
											<td align="center" class="size9">
												<xsl:choose>
													<xsl:when test="$status = 'C'">
														<a href="javascript:getReportFile({report_submission_id});">
															<xsl:value-of select="normalize-space(report_name)"/>
														</a>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="normalize-space(report_name)"/>
													</xsl:otherwise>
												</xsl:choose>
											</td>
											<td align="center" class="size9">
												<xsl:value-of select="normalize-space(submitted_on)"/>
											</td>
											<td align="center" class="size9">
												<xsl:value-of select="normalize-space(completed_on)"/>
											</td>
											<td align="center" class="size9">
												<xsl:value-of select="normalize-space(last_accessed_date)"/>
											</td>
											<td align="center" class="size9">
												<xsl:value-of select="normalize-space(status_description)"/>
											</td>
											<td align="center" class="RequiredFieldTxt">
												<xsl:value-of select="normalize-space(error_message)"/>
											</td>
										</tr>
									</xsl:for-each>

									<tr>
										<td colspan="7" align="center"></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<table width="98%" border="0" align="center" cellpadding="0" cellspacing="1">
						<tr>
							<td width="100%">
								<div align="center"><button class="BlueButton" name="refresh_page" accesskey="R" onclick="javascript:getReportResultList();">(R)efresh</button></div>
							</td>
							<td width="9%" align="right">
								<button class="BlueButton" name="main_menu" accesskey="M" onclick="javascript:doReportingMainMenu();">(M)ain Menu</button>
							</td>
						</tr>
					</table>
					<!--Copyright bar-->
					<xsl:call-template name="footer"/>
				</form>
			</body>
		</html>
	</xsl:template>
	<!--to get the header title-->
	<xsl:template name="IncludeHeader">
		<xsl:call-template name="header">
			<xsl:with-param name="headerName" select="'Report Result List'"/>
			<xsl:with-param name="buttonName" select="'back_button'"/>
		</xsl:call-template>
	</xsl:template>
	<!--end-->
</xsl:stylesheet>