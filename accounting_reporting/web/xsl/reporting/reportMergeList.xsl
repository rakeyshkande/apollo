<?xml version='1.0'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	
	<xsl:output method="xml" indent="yes"/>
    <xsl:template match="/" name="REPORT_LIST">
	<xsl:element name="reportList">
		<xsl:apply-templates select="root/category"/>
   </xsl:element>
   </xsl:template>
  <xsl:template match="root/category">
  	
	<xsl:variable name="category-code"><xsl:value-of select="@report_category"/></xsl:variable>
	  <xsl:copy>
	  	   <xsl:attribute name="category_desc"><xsl:value-of select="@report_category_desc"/></xsl:attribute>
		   <xsl:element name="reports">
		   <xsl:apply-templates select="/root/report[child::report_category/text() = $category-code]"/>
		   </xsl:element>
	  </xsl:copy>
  </xsl:template>
  <xsl:template match="//report">
  
  <xsl:copy-of select="."/>
   
  
 </xsl:template>

</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2005. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\..\xml\ReportUserList.xml" htmlbaseurl="" outputurl="" processortype="internal" profilemode="0" profiledepth="" profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition></MapperMetaTag>
</metaInformation>
-->