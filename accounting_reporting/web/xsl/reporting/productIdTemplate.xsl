
<!DOCTYPE ACDemo [
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output method="html" indent="yes"/>
	<xsl:template name="PRODUCT_ID_TEMPLATE" match="parameterValueList/list-item/displayText[. = 'PRODUCT_ID_TEMPLATE']">
		<xsl:param name="oracleRptParm"/>
		<xsl:param name="displayText"/>
                
                <!--p_product_id-->
                <input type="hidden" id="{$oracleRptParm}" name="{$oracleRptParm}"/>
		<table  width="68%" align="center">
                          <tr>
                            <td class="LabelRight" width="20%"><xsl:value-of select="$displayText"/>:</td>
                            <td align="left" rowspan="3">
                              <textarea name="taProductIds" id="taProductIds" rows="3" cols="20" onBlur="javascript:setInputProductIds();">
                              <xsl:value-of select="../defaultFormValue"/>
                              </textarea>
                            </td>
                            <td>
                                <div>
                                    <span id="taProductIds_error" class="ErrorMessage" style="display:none">Invalid Product ID</span>
                                </div>
                            </td>
                          </tr>
                          <tr>
                            <td class="LabelRight" width="20%">(line delimited)</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                          </tr>
                          <td>&nbsp;</td>
		</table>
		
	</xsl:template>
</xsl:stylesheet>