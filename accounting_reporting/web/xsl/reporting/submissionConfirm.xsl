
<!DOCTYPE ACDemo [
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
	<xsl:import href="../security.xsl"/>
	<xsl:output method="html" indent="yes"/>
	<xsl:template match="/">
		<html>
			<head>
				<META http-equiv="Content-Type" content="text/html"/>
				<title>Report Submissioni Confirmation</title>
				<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
				<link rel="stylesheet" type="text/css" href="css/calendar.css"/>
				<script type="text/javascript" src="js/calendar.js"/>
				<script type="text/javascript" src="js/date.js"/>
				<script type="text/javascript" src="js/FormChek.js"/>
				<script type="text/javascript" src="js/clock.js"/>
				<script type="text/javascript" src="js/dateval.js"/>
				<script type="text/javascript" src="js/reporting.js"/>
				<script type="text/javascript" src="js/util.js"/>
				<script type="text/javascript"><![CDATA[
			function openReportViewer(){
				document.forms[0].action="GetReportViewerForm.do"+getSecurityParams(true);
				document.forms[0].submit();
				
			}
 		]]>
				</script>
			</head>
			<body>
				<xsl:variable select="root/OUT_STATUS" name="status"/>
				<!-- Header-->
				<xsl:call-template name="IncludeHeader"/>
				<!-- Display table-->
				<form method="post">
					<xsl:call-template name="security"/>
					<table width="98%" align="center" cellspacing="1" class="mainTable">
						<tr>
							<td>
								<table width="100%" align="center" class="innerTable">
									<tr>
										<td align="center" class="TotalLine">
											<strong>Confirmation</strong>
										</td>
									</tr>
									<tr>
										<td align="center">
											<div>
												<xsl:choose>
													<xsl:when test="$status='SUCCESS'">Your request to execute <xsl:value-of select="root/report_name"/> has been received. Please check the Report Viewer screen to see the progress of your report.						                   				to view the report.</xsl:when>
													<xsl:otherwise>Your request can not be submitted. Error: <xsl:value-of select="$status"/></xsl:otherwise>
												</xsl:choose>
											</div>
										</td>
									</tr>
									<tr>
										<td align="center">
											<div>
												
											</div>
										</td>
									</tr>
									<tr>
										<td align="center" class="TotalLine">
											<strong>Navigation Options</strong>
										</td>
									</tr>
									<tr>

										<td>
											<div align="center">
												<table>
													<tr>
														<td>
															<button class="BlueButton" name="report_viewer" accesskey="R" onclick="javascript:openReportViewer();">(R)eport Viewer</button>
														</td>
														<td>
															<button class="BlueButton" name="main_menu" accesskey="M" onclick="javascript:doReportingMainMenu();">(M)ain Menu</button>
														</td>
														<td>
															<button class="BlueButton" name="log_off" accesskey="L" onclick="javascript:doReportingLogoff();">(L)ogoff</button>
														</td>
													</tr>
												</table>
											</div>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</form>
				<!--Copyright bar-->
				<xsl:call-template name="footer"/>
			</body>
		</html>
	</xsl:template>
	<!--to get the header title-->
	<xsl:template name="IncludeHeader">
		<xsl:call-template name="header">
			<xsl:with-param name="headerName" select="'Report Submission Confirmation'"/>
			<xsl:with-param name="buttonName" select="''"/>
		</xsl:call-template>
	</xsl:template>
	<!--end-->
</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2005. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition></MapperMetaTag>
</metaInformation>
-->