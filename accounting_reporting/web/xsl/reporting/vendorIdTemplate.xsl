<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	
	<xsl:output method="html" indent="yes"/>
	<xsl:template name="VENDOR_ID_TEMPLATE" match="parameterValueList/list-item/displayText[. = 'VENDOR_ID_TEMPLATE']">
		<xsl:param name="oracleRptParm"/>
		<xsl:param name="displayText"/>
			<script type="text/javascript">
			<![CDATA[
			var vendorIdArray=new Array();
			function addVendorId(){
				var vendorId=document.getElementById("vendor_id").value;
				
				if(validatePresent("vendor_id", "vendor_id_error")){
					var url = "AddVendorId.do?vendor_id="+vendorId+getSecurityParams(false);			
					//alert(url);
					vendorIdIFrame.location.href=url;
				}
			}

			function lookupVendorId(){
				var args = new Array();
				args.action = "LookupVendorId.do";
				args[0] = new Array("start_page", "1");
				args[1] = new Array("vendor_id", document.forms[0].vendor_id.value);
				args[2] = new Array("securitytoken", document.getElementById("securitytoken").value);
				args[3] = new Array("context", document.getElementById("context").value);
				var modal_dim = "dialogWidth:570px; dialogHeight:400px; dialogLeft:125; dialogTop: 190; center:yes; status=0; help:no; scroll:yes";

				var vendorId = window.showModalDialog("html/wait.html", args, modal_dim);
			]]>
				addCodeToOptionList(vendorId, vendorIdArray, '<xsl:value-of select="$oracleRptParm"/>', 'vendor_id_selected');
			}
		 	
		 </script>
   			<iframe id="vendorIdIFrame" name="vendorIdIFrame" border="0" width="0" height="0"/>
            <input type="hidden" id="{$oracleRptParm}" name="{$oracleRptParm}"/>
			<table>
               <tr>
                 <td colspan="3" align="center"></td>
              </tr>
               <tr>
                 <td colspan="3" align="center"></td>
              </tr>
               
               <tr>
              
              </tr>
               <tr>
                 <td width="45%" class="LabelRight">Vendor ID :</td>
                 <td width="30%" align="left"><input id="vendor_id" name="vendor_id" type="text" size="25"/><span id="vendor_id_error" class="ErrorMessage" style="display:none">Invalid Vendor ID</span></td>
                 <td width="25%" align="left"><a href="javascript:addVendorId();">Add</a>&nbsp;&nbsp;&nbsp;<a href="javascript:lookupVendorId();">Lookup Vendor ID</a></td>
                 
              </tr>
               <tr>
                 <td width="45%" class="LabelRight">Vendor ID Selected:</td>
                 <td width="30%" align="left">
                  
					    <select name="vendor_id_selected" multiple="true"></select>
						<br/><span id="{$oracleRptParm}_error" class="ErrorMessage" style="display:none">Required.</span>	 	
                   </td>
                 <td width="25%" align="left"><a href="javascript:removeSelected('vendor_id_selected', vendorIdArray, 'p_vendor_id');">Remove Selected</a><br/><a href="javascript:removeAll('vendor_id_selected', vendorIdArray, 'p_vendor_id');">Clear All </a></td>
              </tr>
               <tr>
                 <td colspan="3" align="center"></td>
              </tr>
               
               <tr>
                 <td colspan="3" align="center"></td>
              </tr>
		  </table>
  	</xsl:template>         
</xsl:stylesheet>