
<!DOCTYPE ACDemo [
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output method="html" indent="yes"/>
	<xsl:template name="SINGLE_DATE_TEMPLATE" match="parameterValueList/list-item/displayText[. = 'SINGLE_DATE_TEMPLATE']">
		<xsl:param name="oracleRptParm"/>
		<xsl:param name="displayText"/>
		<xsl:param name="allowFutureDate"/>
		<table>
			<tr>
				<td class="LabelRight" width="48%"><xsl:value-of select="$displayText"/>:</td>
				<td align="left" width="16%">
					<input type="text" id="{$oracleRptParm}" name="{$oracleRptParm}" maxlength="10" value="{/root/default-date}" onfocus="updateEdit()" onblur="updateEdit()" onKeyPress="return noEnter();"/>
				</td>
				<td align="left" width="6%">
					<input type="image" id="date_img" src="images/calendar.gif" width="16" height="16"/>
				</td>
				<td align="left" width="30%">
					<div>
						<span id="{$oracleRptParm}_error" class="ErrorMessage" style="display:none">Invalid Date</span>
					</div>
				</td>
				<script type="text/javascript">Calendar.setup(
				    {
				      inputField  : "<xsl:value-of select='$oracleRptParm'/>",  // ID of the input field
				      ifFormat    : "mm/dd/y",  // the date format
				      button      : "date_img"  // ID of the button
				    }
				  );</script>
			</tr>
		</table>
		<script type="text/javascript">
		
			function validateSingleDate(){
			
				     var validDt= true;
					if(checkDateFormat("<xsl:value-of select='$oracleRptParm'/>")){
						
						
					<xsl:if test="$allowFutureDate ='false'">
			
							//alert("eod required ");
							if(isPastYesterday("<xsl:value-of select='$oracleRptParm'/>")){
								validDt= false;
							}
							
							
		
					</xsl:if>
			
					}else{
						validDt= false;
					}
				
				    return validDt;
		
			}
		 
		 </script>
	</xsl:template>
</xsl:stylesheet>