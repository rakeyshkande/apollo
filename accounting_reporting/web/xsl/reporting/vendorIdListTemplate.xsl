<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	
	<xsl:output method="html" indent="yes"/>
	<xsl:template name="VENDOR_ID_LIST_TEMPLATE" match="parameterValueList/list-item/displayText[. = 'VENDOR_ID_LIST_TEMPLATE']">
		<xsl:param name="oracleRptParm"/>
                <xsl:param name="displayText"/>
            <!--p_vendor_id-->
            <input type="hidden" id="{$oracleRptParm}" name="{$oracleRptParm}"/>
            <table width="68%" align="center">     
                <tr>
                    <td class="LabelRight" width="20%"><xsl:value-of select="$displayText"/>:</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="20%">&nbsp;</td>
                    <td>
                        <table align="left">
                            <tr>
                                <td>Available Vendors:</td>
                                <td>&nbsp;</td>
                                <td>Selected Vendors:</td>
                            </tr>
                            <tr>
                                <td>
                                    <select name="sVendorsAvailable" id="sVendorsAvailable" ondblclick="moveSelected();" size="6" style="width:300;" tabindex="1">
                                        <xsl:attribute name="multiple"/>
                                        <xsl:for-each select="../optionMap/map-item">
                                            <option value="{name}">
                                                <xsl:value-of select="value"/>&nbsp;(<xsl:value-of select="name"/>)
                                            </option>
                                        </xsl:for-each>
                                    </select>
                                </td>
                                <td>
                                    <table>
                                        <tr><td><button name="bMoveSelectedAll"   id="bMoveSelectedAll"   class="BlueButton" style="width:20;" onclick="moveSelectedAll();"   tabindex="2">&gt;&gt;</button></td></tr>
                                        <tr><td><button name="bMoveSelected"      id="bMoveSelected"      class="BlueButton" style="width:20;" onclick="moveSelected();"      tabindex="3">&gt;</button></td></tr>
                                        <tr><td><button name="bMoveUnSelected"    id="bMoveUnSelected"    class="BlueButton" style="width:20;" onclick="moveUnSelected();"    tabindex="4" >&lt;</button></td></tr>
                                        <tr><td><button name="bMoveUnSelectedAll" id="bMoveUnSelectedAll" class="BlueButton" style="width:20;" onclick="moveUnSelectedAll();" tabindex="5">&lt;&lt;</button></td></tr>
                                    </table>
                                </td>
                                <td>
                                     <select name="sVendorsChosen" id="sVendorsChosen" ondblclick="moveUnSelected();" size="6" style="width:300;" tabindex="6">
                                         <xsl:attribute name="multiple"/>
                                      </select>
                                 </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                    
            </table>
  	</xsl:template>         
</xsl:stylesheet>