<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
	<xsl:output method="html" indent="yes"/>
	<xsl:template match="/">
<html>
   <head>
      <META http-equiv="Content-Type" content="text/html"/>
      <title>Template 1</title>
      <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
      <link rel="stylesheet" type="text/css" href="css/calendar.css"/>
      <script type="text/javascript" src="js/calendar.js"/>
      <script type="text/javascript" src="js/date.js"/>
      <script type="text/javascript" src="js/FormChek.js"/>
      <script type="text/javascript" src="js/clock.js"/>
      <script type="text/javascript" src="js/dateval.js"/>
      <script type="text/javascript">
      	<![CDATA[

 ]]>
	</script>
   </head>
<body>
<!-- Header-->
	<xsl:call-template name="IncludeHeader"/>
<!-- Display table-->
   <table width="98%" align="center" cellspacing="1" class="mainTable">
      <tr>
        <td>
            <table width="100%" align="center" class="innerTable">
               <tr>
                 <td colspan="4" align="center"></td>
              </tr>
               <tr>
                 <td colspan="4" align="center"></td>
              </tr>
               <tr>
                 <td colspan="4" align="center" class="TotalLine"><strong>Sample Template 1 </strong></td>
               </tr>
               <tr>
              <td colspan="4" align="center" class="LabelRight"> <p>&nbsp;</p></td>

              </tr>
               <tr>
                 <td width="33%" align="center" class="LabelRight">Product ID  :</td>
                 <td width="21%" align="left"><input name="product_id" type="text" size="25"/></td>
                 <td width="18%" align="left"><a href="about:blank">Add</a></td>
                 <td width="28%"></td>
              </tr>
               <tr>
                 <td align="center" class="LabelRight"></td>
                 <td colspan="2">
                   <table width="100%"  border="0" cellpadding="0" cellspacing="0" class="textbox">
                     <tr>
                       <td width="9%"><input type="checkbox" name="checkbox" value="checkbox"/></td>
                       <td width="91%">&nbsp;GC01 </td>
                     </tr>
                     <tr>
                       <td><input name="checkbox" type="checkbox" value="checkbox" checked="checked"/></td>
                       <td>&nbsp;W200</td>
                     </tr>
                     <tr>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                     </tr>
                   </table>
                   </td>
                 <td align="left"><a href="about:blank">Remove Checked<br/>Clear All </a></td>
              </tr>
               <tr>
                 <td colspan="4" align="center"></td>
              </tr>
               <tr>
                 <td colspan="4" align="center"><input type="button" class="blueButton" name="submit" value="(S)ubmit" onClick=""/></td>
              </tr>
               <tr>
                 <td colspan="4" align="center"></td>
              </tr>
		  </table>
  		</td>
     </tr>
</table>
         <table width="98%" border="0" align="center" cellpadding="0" cellspacing="1">
           <tr>
             <td width="100%"></td>
             <td width="9%" align="right"><input type="button" class="blueButton" name="main" value="(M)ain Menu" onClick=""/></td>
           </tr>
         </table>
<!--Copyright bar-->
<xsl:call-template name="footer"/>
</body>
</html>
</xsl:template>
<!--to get the header title-->
		<xsl:template name="IncludeHeader">
		<xsl:call-template name="header">
		   <xsl:with-param name="headerName" select="'Template 1'"/>
		</xsl:call-template>
	</xsl:template>
<!--end-->
</xsl:stylesheet>