<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" indent="yes"/>

<xsl:template match="/root">

  <html>
    <head>
      <script type="text/javascript" language="javascript">
        addCodeAction();
	
		function addCodeAction()
		{
			//alert("in addCodeAction");
			//get status
			var status = "<xsl:value-of select='indicator'/>";
			var code="";
			var errorFieldId="";
			var codeArray;
			var fieldId="";
			var selectField="";
			<xsl:choose>
				<xsl:when test="source_type[. !='']">
					code="<xsl:value-of select='source_type'/>";
					errorFieldId="source_type_error";
					codeArray=parent.sourceTypeArray;
					fieldId="p_source_type";
					selectField="source_type_selected";
				</xsl:when>
				<xsl:when test="vendor_id[. !='']">
					code="<xsl:value-of select='vendor_id'/>";
					//alert(code);
					errorFieldId="vendor_id_error";
					codeArray=parent.vendorIdArray;
					fieldId="p_vendor_id";
					selectField="vendor_id_selected";
				</xsl:when>
				<xsl:when test="source_code[. !='']">
					code="<xsl:value-of select='source_code'/>";
					errorFieldId="source_code_error";
					codeArray=parent.sourceCodeArray;
					fieldId="p_source_code";
					selectField="source_code_selected";
				</xsl:when>
				<xsl:when test="dnis_id[. !='']">
					code="<xsl:value-of select='dnis_id'/>";
					errorFieldId="dnis_id_error";
					codeArray=parent.dnisIdArray;
					fieldId="p_dnis_id";
					selectField="dnis_id_selected";
				</xsl:when>
				<xsl:when test="user_id[. !='']">
					code="<xsl:value-of select='user_id'/>";
					errorFieldId="user_id_error";
					codeArray=parent.userIdArray;
					fieldId="p_user_id";
					selectField="user_id_selected";
				</xsl:when>
				<xsl:when test="product_id[. !='']">
					code="<xsl:value-of select='product_id'/>";
					errorFieldId="product_id_error";
					codeArray=parent.vendorProductIdArray;
					fieldId="p_product_id";
					selectField="product_id_selected";
				</xsl:when>
        <xsl:when test="florist_id[. !='']">
					errorFieldId="p_data_error";
				</xsl:when>
			</xsl:choose>
      

            if(status=='Y')
            {		
              parent.document.getElementById(errorFieldId).style.display = "none";
              <xsl:choose>
                <xsl:when test="florist_id[. !='']">
                  var errorCount = "<xsl:value-of select='error_count'/>";
                  if(errorCount == "0") {
                      parent.submitForm();
                  }
                </xsl:when>
                <xsl:otherwise>
                  //add source type to the checkbox array.
                  parent.addCodeToOptionList(code, codeArray, fieldId, selectField);
                </xsl:otherwise>
              </xsl:choose>
              
            }
            else
            {
              //parent.hideWaitMessage("content", "wait");
              parent.document.getElementById(errorFieldId).style.display = "block";
      
            } 
        }
      </script>
    </head>
    <body>
	
	</body>
  </html>
</xsl:template>

</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2005. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition></MapperMetaTag>
</metaInformation>
-->