<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	
	<xsl:output method="html" indent="yes"/>
	<xsl:template name="SOURCE_CODE_TEMPLATE" match="parameterValueList/list-item/displayText[. = 'SOURCE_CODE_TEMPLATE']">
		<xsl:param name="oracleRptParm"/>
		<xsl:param name="displayText"/>
			<script type="text/javascript">
			<![CDATA[
			var sourceCodeArray=new Array();
			function addSourceCode(){
				var sourceCode=document.getElementById("source_code").value;
				
				if(validatePresent("source_code", "source_code_error")){
					var url = "AddSourceCode.do?source_code="+sourceCode+getSecurityParams(false);			
					//alert(url);
					sourceCodeIFrame.location.href=url;
				}
			}

			function lookupSourceCode(){
				var args = new Array();
				args.action = "LookupSourceCode.do";
				args[0] = new Array("start_page", "1");
				args[1] = new Array("source_code", document.forms[0].source_code.value);
				args[2] = new Array("securitytoken", document.getElementById("securitytoken").value);
				args[3] = new Array("context", document.getElementById("context").value);
				var modal_dim = "dialogWidth:570px; dialogHeight:400px; dialogLeft:125; dialogTop: 190; center:yes; status=0; help:no; scroll:yes";

				var sourceCode = window.showModalDialog("html/wait.html", args, modal_dim);
			]]>
				addCodeToOptionList(sourceCode, sourceCodeArray, '<xsl:value-of select="$oracleRptParm"/>', 'source_code_selected');
			}
		 	
		 </script>
   			<iframe id="sourceCodeIFrame" name="sourceCodeIFrame" border="0" width="0" height="0"/>
            <input type="hidden" id="{$oracleRptParm}" name="{$oracleRptParm}"/>
			<table>
               <tr>
                 <td colspan="3" align="center"></td>
              </tr>
               <tr>
                 <td colspan="3" align="center"></td>
              </tr>
               
               <tr>
              
              </tr>
               <tr>
                 <td width="45%" class="LabelRight">Source Code :</td>
                 <td width="30%" align="left"><input id="source_code" name="source_code" type="text" size="25"/><span id="source_code_error" class="ErrorMessage" style="display:none">Invalid Source Code</span></td>
                 <td width="25%" align="left"><a href="javascript:addSourceCode();">Add</a>&nbsp;&nbsp;&nbsp;<a href="javascript:lookupSourceCode();">Lookup Source Code</a></td>
                 
              </tr>
               <tr>
                 <td width="45%" class="LabelRight">Source Code Selected:</td>
                 <td width="30%" align="left">
                  
					    <select name="source_code_selected" multiple="true"></select>
						<br/><span id="{$oracleRptParm}_error" class="ErrorMessage" style="display:none">Required.</span>	 	
                   </td>
                 <td width="25%" align="left"><a href="javascript:removeSelected('source_code_selected', sourceCodeArray, 'p_source_code');">Remove Selected</a><br/><a href="javascript:removeAll('source_code_selected', sourceCodeArray, 'p_source_code');">Clear All </a></td>
              </tr>
               <tr>
                 <td colspan="3" align="center"></td>
              </tr>
               
               <tr>
                 <td colspan="3" align="center"></td>
              </tr>
		  </table>
  	</xsl:template>         
</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2005. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition></MapperMetaTag>
</metaInformation>
-->