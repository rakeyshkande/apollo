
<!DOCTYPE ACDemo [
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="html" indent="no"/>
	<xsl:output indent="yes"/>
	<xsl:param name="securitytoken"/>
	<xsl:param name="context"/>
	<xsl:template match="/root">

		<html>
			<head>
				<title>FTD - DNIS Lookup</title>
				<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
				<script language="javascript" src="js/FormChek.js"/>
				<script language="javascript" src="js/util.js"/>
				<script type="text/javascript" src="js/reporting.js"/>
				<script language="javascript">var fieldNames = new Array("dnisIdInput");
        var images = new Array("dnisIdSearch", "dnisIdCloseTop", "dnisIdCloseBottom", "dnisIdSelectNone");<![CDATA[
	    function init()
        {
            addDefaultListenersArray(fieldNames);
            addImageCursorListener(images);
            window.name = "VIEW_DNIS_ID_LOOKUP";
            window.focus();
            untabHeader();
            //document.forms[0].dnisIdInput.focus();
        }

        function onKeyDown()
        {
          if (window.event.keyCode == 13)
            reopenPopup();
        }

        function enterReOpenDnisIdPopup()
        {
          if (window.event.keyCode == 13)
            reopenPopup();
        }

        function reopenPopup()
        {
          var form = document.forms[0];
         
          
          //First validate the Source Code input
          var dnisId = document.forms[0].dnisIdInput.value;
          dnisId = stripWhitespace(dnisId);

          
            form.target = window.name;
            
            var url_source="LookupDnisId.do?dnis_id=" + dnisId+"&start_page="+document.forms[0].start_page.value + getSecurityParams(false);
            form.action = url_source;
            form.submit();
          
        }

        function closeDnisIdLookup(id)
        {
          if (window.event.keyCode == 13)
            populatePage(id);
        }

        function populatePage(id)
        {
          var sc, desc, part;
          if (id > -1){
            sc = document.getElementById("dnisId" + id).innerHTML;
            //desc = document.getElementById("desc" + id).innerHTML;
            //part = document.getElementById("partner_id" + id).value;

          }

          window.returnValue = sc;
          window.close();
        }
    ]]></script>
			</head>

			<body onLoad="javascript:init(), buttonDis(dnisIdLookupForm.start_page.value, dnisIdLookupForm);">
				<form name="dnisIdLookupForm" method="post" action="">
					<input type="hidden" name="securitytoken" value="{$securitytoken}"/>
					<input type="hidden" name="context" value="{$context}"/>
					<input type="hidden" name="start_page" value="{start_page}"/>
					<input type="hidden" name="total_page" value="{total_page}"/>
					<input type="hidden" name="dnisIdInput" value="{dnis_id_lookup}"/>
					<center>

						<table width="98%" border="0" cellpadding="2" cellspacing="2">
							<tr>
								<td align="right">
									<img id="dnisIdCloseTop" tabindex="3" onkeydown="javascript:closeDnisIdLookup(-1)" src="images/button_close.gif" alt="Close screen" border="0" onclick="javascript:populatePage(-1)"/>
								</td>
							</tr>
							<tr>
								<td>
									<table width="100%" border="1" class="mainTable" cellpadding="2" cellspacing="2">
										<tr>
											<td width="5%" class="label">&nbsp;</td>
											<td class="label" align="center" valign="bottom">DNIS<br/>ID</td>
											<td class="label" align="center" valign="bottom">DNIS<br/>Description</td>
											<td>
												
												<xsl:choose>
													<xsl:when test="dnis-code-list[. !='']">
														<xsl:for-each select="dnis-code-list/item">
															<tr>
																<td>
																	<img onclick="javascript:populatePage({@num})" onkeydown="javascript:closeDnisIdLookup({@num})" tabindex="4" src="images/selectButtonRight.gif"/>
																</td>

																<td id="dnisId{@num}" align="center">
																	<xsl:value-of select="dnis_id"/>
																</td>
																
																<td id="dnisDesc{@num}" align="center">
																	<xsl:value-of select="description"/>
																</td>
															</tr>
														</xsl:for-each>
													</xsl:when>
													<xsl:otherwise>
														<span class="ErrorMessage">No DNIS has been found.</span>
													</xsl:otherwise>
												</xsl:choose>
												
											</td>
										</tr>
										<tr>
											<td width="5%" valign="center">
											</td>
											<td colspan="7" valign="top" align="right">
												<span class="LabelRight">Page&nbsp;<xsl:value-of select="start_page"/> &nbsp;of&nbsp;<xsl:value-of select="total_page"/></span>
											</td>
										</tr>
										
											<tr>
												<td width="5%" valign="center">
												</td>
												<td colspan="7" valign="top" align="right">
													<button class="arrowButton" tabindex="2" name="firstpage" onClick="goToFirstPage();">7</button>
													<button class="arrowButton" tabindex="3" name="previouspage" onClick="goToPreviousPage();">3</button>
													<button class="arrowButton" tabindex="4" name="nextpage" onClick="goToNextPage();">4</button>
													<button class="arrowButton" tabindex="5" name="lastpage" onClick="goToLastPage();">8</button>
												</td>
											</tr>
										
									</table>
								</td>
							</tr>
							<tr>
								<td align="right">
									<img id="dnisIdCloseBottom" tabindex="5" onkeydown="javascript:closeDnisIdLookup(-1)" src="images/button_close.gif" alt="Close screen" border="0" onclick="javascript:populatePage(-1)"/>
								</td>
							</tr>
						</table>
					</center>
				</form>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>