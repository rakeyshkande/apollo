<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
	<xsl:import href="security.xsl"/>
	<xsl:output method="html" indent="yes"/>
	<xsl:key name="error" match="ROOT/ERRORS/ERROR" use="@fieldname" />
	<xsl:key name="fields" match="ROOT/FIELDS/FIELD" use="@fieldname" />
	<xsl:key name="status" match="ROOT/GC_COUPONS/GC_COUPON" use="gc_coupon_status" />
	<xsl:variable name="error" select="ROOT/ERRORS/ERROR/@fieldname" />
	<xsl:variable name="gcc_type" select="key('fields','gcc_type')/@value" />
	<xsl:variable name="gcc_redemption_date" select="key('fields','gcc_redemption_date')/@value" />
	<xsl:variable name="gcc_company" select="key('fields','gcc_company')/@value" />
	<xsl:variable name="gcc_status_single" select="key('fields','gcc_status_single')/@value" />
	<xsl:variable name="gcc_status_multiple" select="key('fields','gcc_status_multiple')/@value" />
	<xsl:variable name="saved_request_number" select="ROOT/@saved_request_number" />
	<xsl:template match="/">
		<html>
			<head>
				<META http-equiv="Content-Type" content="text/html"/>
				<title>
					Single Gift Certificate / Coupon Maintenance
				</title>
				<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
				<link rel="stylesheet" type="text/css" href="css/calendar.css"/>
				<script type="text/javascript" src="js/clock.js"/>
				<script type="text/javascript" src="js/util.js"/>
				<script type="text/javascript" src="js/FormChek.js"/>
				<script type="text/javascript" src="js/calendar_single.js"/>
				<script type="text/javascript" src="js/date.js"/>
				<script type="text/javascript" src="js/single.js"/>
				<script type="text/javascript" src="js/common_functions_all.js"/>
				<script type="text/javascript" src="js/dateval.js"/>
				<script type="text/javascript">
				var gccstatus = '<xsl:value-of select="key('fields','gcc_status_single')/@value"/>';
				var original;

<![CDATA[

function init() {

    original = new Array(document.forms[0].length);
    original = saveFormValue(original);
    // Defect Fix : #14619
    if(document.form.gcc_source_code_old.value == '') {                                               
    	document.form.gcc_source_code_old.value = document.form.gcc_source_code.value;                                                                     
    }
}

function saveFormValue(formValueArray) {

    for (var i=0;i<document.forms[0].length;i++)

    {
        current = document.forms[0].elements[i];

        if (current.type == "text" || current.type == "select-one") {

            formValueArray[i] = current.value;

        }
    }
     return formValueArray;
}



// Call this when Exit button is clicked.

// Returns true if any value on the form has changed.

// Returns false otherwise.

function testValueChange() {

    valueChanged = false;

    current = new Array(document.forms[0].length);
    if(original.length != current.length)
    {
        valueChanged = true;
    }

     current = saveFormValue(current);
     for (var i=0;i<current.length;i++)

    {
        if(original[i] != current[i])
        {
            valueChanged = true;
        }
    }

    return valueChanged;
}


function defaultstatus()
{
			if (gccstatus != 'Redeemed')
				{
				document.getElementById('gcc_redemption_date').disabled = true;
				document.getElementById('gcc_redemption_date').style.backgroundColor="#eeeeee";
				document.getElementById('redemp_date_img').disabled = true;
				document.getElementById('gcc_order_number').disabled = true;
				document.getElementById('gcc_order_number').style.backgroundColor="#eeeeee";
				}
				else
				{
				document.getElementById('gcc_redemption_date').disabled = false;
				document.getElementById('gcc_redemption_date').style.backgroundColor="#FFFFFF";
				document.getElementById('redemp_date_img').disabled = false;
				document.getElementById('gcc_order_number').disabled = false;
				document.getElementById('gcc_order_number').style.backgroundColor="#FFFFFF";
				}

}
               
      ]]>
				</script>
			</head>
			<body onload="init(), defaultstatus();" onkeypress="enterkey();">
				<!-- Header-->
				<xsl:call-template name="IncludeHeader"/>
				<!-- Display table-->
				<form name="form" method="post">
					<div id="content" style="display:block">
						<xsl:call-template name="security"/>
						<input type="hidden" name="gcc_company_id" value="{key('fields','gcc_company')/@value}"/>
						<input type="hidden" name="saved_request_number" value="{ROOT/@saved_request_number}"/>
						<input type="hidden" name="request_number_for_edit_recipient" value="{ROOT/@request_number_for_edit_recipient}"/>
						<input type="hidden" name="success_msg" value="{ROOT/@success_msg}"/>
						<input type="hidden" name="exception_error" value="{ROOT/@exception_error}"/>
						<input type="hidden" name="status_multiple_count" value="{ROOT/@status_multiple_count}"/>
						<input type="hidden" name="gcc_enable_create_cs" value="{key('fields','gcc_enable_create_cs')/@value}"/>
						<input type="hidden" name="gc_coupon_recip_id" value="{key('fields','gcc_coupon_recip_id')/@value}"/>
						<input type="hidden" name="gcc_creation_date" value="{key('fields','gcc_creation_date')/@value}"/>
						<input type="hidden" name="gcc_coupon_number" value="{key('fields','gcc_coupon_number')/@value}"/>
						<input type="hidden" name="gcc_source_code_old" value=""/>
						                <!-- hidden fields for the recipient data-->
                                         <xsl:for-each select="ROOT/GC_COUPON_RECIPIENTS/GC_COUPON_RECIPIENT">
                                <input type="hidden" name="gc_coupon_recip_id_{@num}" value="{gc_coupon_recip_id}"/>
																<input type="hidden" name="first_name_{@num}" id="first_name_{@num}" value="{first_name}"/>
																<input type="hidden" name="last_name_{@num}" id="last_name_{@num}" value="{last_name}"/>
																<input type="hidden" name="address_1_{@num}" id="address_1_{@num}" value="{address_1}"/>
																<input type="hidden" name="address_2_{@num}" id="address_2_{@num}" value="{address_2}"/>
																<input type="hidden" name="city_{@num}" id="city_{@num}" value="{city}"/>
															    <input type="hidden" name="state_{@num}" id="state_{@num}" value="{state}"/>
																<input type="hidden" name="zip_{@num}" id="zip_{@num}" value="{zip_code}"/>
										                        <input type="hidden" name="country_{@num}" id="country_{@num}" value="{country}"/>
																<input type="hidden" name="phone_{@num}" id="phone_{@num}" value="{phone}"/>
																<input type="hidden" name="email_{@num}" id="email_{@num}" value="{email_address}"/>
																<input type="hidden" name="issuedReferenceNumber_{@num}" id="issuedReferenceNumber_{@num}" value="{issuedReferenceNumber}"/>
                                            </xsl:for-each>
											<!-- END OF HIDDEN FIELDS-->
						
						<table width="98%" align="center" cellspacing="1" class="mainTable">
							<tr>
								<td>
									<table width="100%" align="center" class="innerTable">
										<tr>
											<td colspan="10" align="center" class="TotalLine">
												<div align="left">
													<!--Gift Certificate / Coupon Information-->
													<strong>Gift Certificate / Coupon Information</strong>
													<br/>
												</div>
											</td>
											<div align="left"></div>
										</tr>
										<tr class="Label">
											<td colspan="2" align="center" valign="top">
												<div align="right">Request Number: </div>
											</td>
											<td colspan="2" align="center">
												<div align="left">
													<input type="text" value="{key('fields','gcc_request_number')/@value}" size="25" maxlength="12" id="gcc_request_number" name="gcc_request_number"  disabled="true" class="disabledfield" />
												</div>
											</td>
											<td colspan="2" align="center">
												<div align="right">Creation Date : </div>
											</td>
											<td colspan="2" align="center" class="Label">
												<div align="left">
													<xsl:value-of select="key('fields','gcc_creation_date')/@value"/>
												</div>
											</td>
											<td colspan="2" align="center"></td>
										</tr>
										<tr class="Label">
											<td colspan="2" align="center" valign="top">
												<div align="right">Type: </div>
											</td>
											<td colspan="2" align="center">
												<div align="left">
													<select id="gcc_type" name="gcc_type" disabled="true" class="disabledfield">
																<option value="{key('fields','gcc_type')/@value}" selected="selected">
																	<xsl:value-of select="key('fields','gcc_type')/@value" />
																</option>
													</select>
												</div>
											</td>
											<td colspan="2" align="center" valign="top">
												<div align="right">Issue Date : </div>
											</td>
											<td colspan="2" align="center" valign="top">
												<div align="left">
													<input type="text" value="{key('fields','gcc_issue_date')/@value}" size="25" maxlength="10" id="gcc_issue_date" name="gcc_issue_date"  disabled="true" class="disabledfield" />
												</div>
											</td>
											<td colspan="2" align="center">
												<div align="left"></div>
											</td>
										</tr>
										<tr class="Label">
											<td colspan="2" align="center" valign="top">
												<div align="right">Issue Amount:</div>
											</td>
											<td colspan="2" align="center">
												<div align="left">
													<input type="text" value="{key('fields','gcc_issue_amount')/@value}" size="25" maxlength="5" id="gcc_issue_amount" name="gcc_issue_amount"  disabled="true" class="disabledfield" />
												</div>
											</td>
											<td colspan="2" align="center" valign="top">
												<div align="right">Expiration Date : </div>
											</td>
											<td colspan="2" align="center">
												<div align="left">
													<input type="text" value="{key('fields','gcc_expiration_date')/@value}" size="25" maxlength="10" id="gcc_expiration_date" name="gcc_expiration_date" disabled="true" class="disabledfield" />
												</div>
											</td>
											<td colspan="2" align="center">
												<div align="left"></div>
											</td>
										</tr>
										<tr class="Label">
											<td colspan="2" align="center" valign="top">
												<div align="right">Paid Amount: </div>
											</td>
											<td colspan="2" align="center">
												<div align="left">
													<input type="text" value="{key('fields','gcc_paid_amount')/@value}" size="25" maxlength="5" id="gcc_paid_amount" name="gcc_paid_amount" disabled="true" class="disabledfield" />
												</div>
											</td>
											<td colspan="2" align="center" valign="top">
												<div align="right">Quantity : </div>
											</td>
											<td colspan="2" align="center">
												<div align="left">
													<input type="text" value="{key('fields','gcc_quantity')/@value}" size="25" maxlength="6" id="gcc_quantity" name="gcc_quantity" disabled="true" class="disabledfield" />
												</div>
											</td>
											<td colspan="2" align="center"></td>
										</tr>
										<tr class="Label">
											<td width="38" align="center"></td>
											<td width="40" align="center">
												<div align="left"></div>
											</td>
											<td colspan="2" align="center">
												<div align="left"></div>
											</td>
											<td colspan="2" align="center" valign="top">
												<div align="right">Format :</div>
											</td>
											<td colspan="2" align="center">
												<div align="left">
													<select name="gcc_format" id="gcc_format" disabled="true" class="disabledfield">
														<option value="{key('fields','gcc_format')/@value}"><xsl:value-of select="key('fields','gcc_format')/@value" /></option>
														</select>
												</div>
											</td>
											<td colspan="2" align="center"></td>
										</tr>
										<tr class="Label">
											<td align="center"></td>
											<td align="center">
												<div align="left"></div>
											</td>
											<td colspan="2" align="center">
												<div align="left"></div>
											</td>
											<td colspan="2" align="center" valign="top" id="gcc_filename_name">
												<div align="right">Filename : </div>
											</td>
											<td colspan="2" align="center">
												<div align="left">
													<input type="text" value="{key('fields','gcc_filename')/@value}" size="25" maxlength="64" id="gcc_filename" name="gcc_filename" disabled="true" class="disabledfield"  />
												</div>
											</td>
										</tr>
										<tr class="Label">
											<td height="22" colspan="10" align="center">
												<div align="left"></div>
												<!--Program Information-->
												<div align="left" class="TotalLine">Program Information </div>
											</td>
										</tr>
										<tr class="Label">
											<td colspan="2" align="center" valign="top">
												<div align="right">Requestor Name: </div>
											</td>
											<td colspan="2" align="center">
												<div align="left">
													<input type="text" value="{key('fields','gcc_requestor_name')/@value}" size="25" maxlength="32" id="gcc_requestor_name" name="gcc_requestor_name" disabled="true" class="disabledfield" />
												</div>
											</td>
											<td colspan="2" align="center" valign="top">
												<div align="right">Company:</div>
											</td>
											<td colspan="2" align="center">
												<div align="left">
													<select id="gcc_company" name="gcc_company" disabled="true" class="disabledfield">
																<option value="{key('fields','gcc_company')/@value}" selected="selected">
																	<xsl:value-of select="key('fields','gcc_company')/@value" />
																</option>
													</select>
												</div>
											</td>
											<td colspan="2" align="center"></td>
										</tr>
										<tr class="Label">
											<td colspan="2" align="center" valign="top">
												<div align="right">Redemption Type: </div>
											</td>
											<td colspan="2" align="center" valign="top">
												<div align="left">
													<select id="gcc_redemption_type" name="gcc_redemption_type" disabled="true" class="disabledfield">
														<option value="{key('fields','gcc_redemption_type')/@value}"><xsl:value-of select="key('fields','gcc_redemption_type')/@value" /></option>
													</select>
												</div>
											</td>
											<td colspan="2" align="center" valign="top">
												<div align="right">Restricted Source Code(s): </div>
											</td>
											<td colspan="2" align="center">
												<div align="left">
													
													<textarea cols="30" rows="3" id="gcc_source_code" name="gcc_source_code" disabled="true">
														<xsl:value-of select="key('fields','gcc_source_code')/@value" />
													</textarea>
													</div>
													<div align="left">
													<a href="javascript:popSourceCodes()" tabindex="{position()}">
													<font color="0000ff">Click to edit source codes</font></a>
												</div>
												<div>
													<span id="span_source_code_format_error" class="ErrorMessage" style="display:none">Invalid Format</span>
												</div>
											</td>												
										</tr>
										<tr class="Label">
											<td colspan="2" align="center" valign="top">
												<div align="right">Program Name: </div>
											</td>
											<td colspan="2" align="center">
												<div align="left">
													<input type="text" value="{key('fields','gcc_program_name')/@value}" size="25" maxlength="32" id="gcc_program_name" name="gcc_program_name" disabled="true" class="disabledfield" />
												</div>
											</td>
                                             <td colspan="2" align="center" valign="top">
                                             <div align="right">Promotion Id:</div>
                                             </td>
                                             <td colspan="2" align="center"><div align="left">
                                              <input type="text" value="{key('fields','gcc_promotion_id')/@value}" size="25" maxlength="20" id="gcc_promotion_id" name="gcc_promotion_id" disabled="true" class="disabledfield" />
                                              </div>
                                              </td>
											<td colspan="2" align="center"></td>
										</tr>
                    <tr class="Label">
													<td colspan="2" align="center" valign="top">
														<div align="right">Partner Program Name: </div>
													</td>
													<td colspan="2" align="center">
														<div align="left">
                              <select id="gcc_partner_program_name" name="gcc_partner_program_name" disabled="true" class="disabledfield">
                                  <option value="{key('fields','gcc_partner_program_name')/@value}" selected="selected">
                                    <xsl:value-of select="key('fields','gcc_partner_program_name')/@value" />
                                  </option>
                              </select>
                            </div>
                          </td>
                    </tr>                        
										<tr class="Label">
											<td colspan="2" align="center" valign="top">
												<div align="right">Program Description: </div>
											</td>
											<td colspan="8" align="center">
												<div align="left">
													<input type="text" value="{key('fields','gcc_program_description')/@value}" size="100" maxlength="64" id="gcc_program_description" name="gcc_program_description" disabled="true" class="disabledfield" />
												</div>
											</td>
										</tr>
										<tr class="Label">
											<td colspan="2" align="center" valign="top">
												<div align="right">Comments: </div>
											</td>
											<td colspan="8" align="center">
												<div align="left">
													<input type="text" value="{key('fields','gcc_comments')/@value}" size="100" maxlength="320" id="gcc_comments" name="gcc_comments" disabled="true" class="disabledfield" />
												</div>
											</td>
										</tr>
										<tr class="Label">
											<td colspan="10" align="center" class="TotalLine">
												<div align="left">Additional Information </div>
											</td>
										</tr>
										<tr>
											<td colspan="2" align="center" class="Label" valign="top">
												<div align="right">Prefix: </div>
											</td>
											<td colspan="2" align="center">
												<div align="left">
													<input type="text" value="{key('fields','gcc_prefix')/@value}" size="25" maxlength="4" id="gcc_prefix" name="gcc_prefix" disabled="true" class="disabledfield" />
												</div>
											</td>
											<td colspan="2" align="center">
												<div align="left" class="Label">
													<div align="right" valign="top">Other:</div></div>
											</td>
											<td colspan="4" align="center">
												<div align="left">
													<input type="text" value="{key('fields','gcc_other')/@value}" size="45" maxlength="32" id="gcc_other" name="gcc_other" disabled="true" class="disabledfield" />
												</div>
											</td>
										</tr>
                                               <tr class="Label">
														<td colspan="2" align="center" class="Label" valign="top">
															<div align="right">Status: </div>
														</td>
														<td colspan="2" align="center">
															<div align="left">
																<select tabindex="1" id="gcc_status_single" name="gcc_status_single" onchange="onStatusChangeSingle();">
																	<option value="none"></option>
																	<xsl:for-each select="ROOT/GC_COUPON_STATUSS/GC_COUPON_STATUS">
																		<xsl:choose>
																			<xsl:when test="gc_coupon_status = $gcc_status_single">
																				<option value="{gc_coupon_status}" selected="selected">
																					<xsl:value-of select="gc_coupon_status" />
																				</option>
																			</xsl:when>
																			<xsl:otherwise>
																				<option value="{gc_coupon_status}">
																					<xsl:value-of select="gc_coupon_status" />
																				</option>
																			</xsl:otherwise>
																		</xsl:choose>
																	</xsl:for-each>
																</select>
															</div>
														</td>
											<td colspan="2" align="center" valign="top">
												<div align="right">Order Number</div>
											</td>
											<td colspan="4" align="center">
												<div align="left">
													<input type="text" value="{key('fields','gcc_order_number')/@value}" tabindex="2" size="25" maxlength="20" id="gcc_order_number" name="gcc_order_number" disabled="true" class="disabledfield" />
												</div>
												<div align="left">
													<span id="span_order_number" class="ErrorMessage" style="display:none">Required</span>
															</div>
															<xsl:if test="$error = 'gcc_order_number'">
														<div align="left">
															<span id="server_error" class="ErrorMessage" style="display:block">
																<xsl:value-of select="key('error','gcc_order_number')/@value" />
															</span>
														</div>
														</xsl:if>
											</td>
											
										</tr>
										<tr>
											<td colspan="2" align="center">
												<div align="left"></div>
											</td>
											<td align="center">
												<div align="left" class="string">
													<xsl:value-of select="key('fields','gcc_status_single_display')/@value" />
												</div>
											</td>
											<td colspan="3" align="center"></td>
											<td colspan="1" align="center">
												<div align="left" class="string">
													<xsl:value-of select="ROOT/GC_COUPON_HISTORYS/GC_COUPON_HISTORY/order_detail_id"/>
												</div>
											</td>
										</tr>
										<tr align="center" class="Label">
											<td colspan="2" valign="top">
												<div align="right">Redemption Date:</div>
											</td>
											<td>
												<div align="left">
													<input type="text" value="{key('fields','gcc_redemption_date')/@value}" tabindex="3" size="25" maxlength="10" id="gcc_redemption_date" name="gcc_redemption_date" disabled="true" class="disabledfield" />
																<div>
																<span id="span_redemption_date_error" class="ErrorMessage" style="display:none">Invalid Date</span>
																</div>
																<div>
																	<span id="span_redemption_date" class="ErrorMessage" style="display:none">Required</span>
																</div>
												</div>
												
											</td>
											<td>
												<div align="left">
													<img id="redemp_date_img" name="redemp_date_img" tabindex="4" src="images/calendar.gif" />
												</div>
											</td>
											<td colspan="4"></td>
										</tr>
										<tr align="center">
											<td colspan="2"></td>
											<td colspan="6">
												<div align="left" class="string">
													<xsl:value-of select="ROOT/GC_COUPON_HISTORYS/GC_COUPON_HISTORY/redemption_date"/>
												</div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					
					<!--buttons-->
					<table width="98%" border="0" align="center" cellpadding="0" cellspacing="1">
						<tr>
							<td width="50%" align="right">
								<button class="BlueButton" name="gcc_submit_create_save" tabindex="5" accesskey="S" onclick="javascript:savesingleGC();">(S)ave</button>
							</td>
							<td width="38%" align="left">
								<button class="BlueButton" name="gcc_submit_display_recipient" tabindex="6" accesskey="R" onclick="javascript:editSingleRecipient();">Edit (R)ecipient</button>
							</td>
							<td width="12%" align="right">
								<button class="BlueButton" name="gcc_submit_exit" tabindex="7" accesskey="E" onclick="javascript:exit();">(E)xit</button>
							</td>
						</tr>
					</table>
					</div>
					<!--  Used to dislay Processing... message to user-->
					<div id="waitDiv" style="display:none">
						<table id="waitTable" class="mainTable" width="98%" height="100%" align="center" cellpadding="30" cellspacing="1">
							<tr>
								<td width="100%">
									<table width="100%" cellspacing="0" cellpadding="0" border="0">
										<tr>
											<td id="waitMessage" align="right" width="50%" class="waitMessage" />
											<td id="waitTD" width="50%" class="waitMessage" />
										</tr>
									</table></td>
							</tr>
						</table>
					</div>
					<!--Copyright bar-->
					<xsl:call-template name="footer"/>
					<table>
					<tr>
					<td><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p></td>
					
					</tr>
					</table>
				</form>
			</body>
		</html>
		<script type="text/javascript">
  Calendar.setup(
    {
      inputField  : "gcc_issue_date",  // ID of the input field
      ifFormat    : "mm/dd/y",  // the date format
      button      : "issue_date_img"  // ID of the button
    }
  );
		</script>
		<script type="text/javascript">
  Calendar.setup(
    {
      inputField  : "gcc_expiration_date",  // ID of the input field
      ifFormat    : "mm/dd/y",  // the date format
      button      : "exp_date_img"  // ID of the button
    }
  );
		</script>
		<script type="text/javascript">
  Calendar.setup(
    {
      inputField  : "gcc_redemption_date",  // ID of the input field
      ifFormat    : "mm/dd/y",  // the date format
      button      : "redemp_date_img"  // ID of the button
    }
  );
										</script>
			</xsl:template>
	<!--to get the header title-->
	<xsl:template name="IncludeHeader">
		<xsl:call-template name="header">
		    <xsl:with-param name="showheader" select="true()"/>
			<xsl:with-param name="headerName" select="'Single Gift Certificate / Coupon Maintenance'"/>
		</xsl:call-template>
	</xsl:template>
	<!--end-->
</xsl:stylesheet>