<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="securityAndData.xsl"/>
<xsl:output method="html" indent="yes"/>
<xsl:template name="billing" match="/">
<!--xsl:param name="showprinter"/-->
    <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
    <script type="text/javascript" src="js/billing.js" />
    <script type="text/javascript" language="javascript">
        <![CDATA[

     function printPage()
     {
     window.print()
     window.close()
     }
   ]]>
    </script>
			<!-- Display table-->
			<xsl:call-template name="securityanddata"/>
			<body onload="highlightRow(), printPage();">
			<form name="printform">

   <table width="98%" align="center" cellspacing="1" class="mainTable">
      <tr>
        <td>
        <input type="hidden" id="cbr_id" name="cbr_id" value=""/>
            <table width="100%" align="center" cellspacing="0" class="innerTable">
               <tr>
                 <td colspan="9" align="center">
                 <div align="left"><span class="Label">Order Number :</span>&nbsp;<xsl:value-of select="root/master_order_number"/></div></td>
              </tr>
               <tr>
                  <td colspan="9" align="center" class="TotalLine"><div align="center"><strong>Billing Transactions</strong><br/>
                   </div></td>
                </tr>
                 		<tr class="Label">
									<td width="10%" rowspan="2" align="center">Type</td>
									<td width="11%" rowspan="2" align="center">Billed<br/>Amount</td>
									<td width="11%" rowspan="2" align="center">Payment<br/>Method</td>
									<td width="12%" rowspan="2" align="center">Refund<br/>Code</td>
									<td width="14%" rowspan="2" align="center">Billed<br/>Date</td>
									<td width="11%" rowspan="2" align="center">Settled<br/>Date</td>
									<td colspan="3" align="center">(Charge Back/Retrieval Info)</td>
								</tr>
                                <tr class="Label">
                                   <td width="11%" align="center">Date Rec'd</td>
                                   <td width="11%" align="center">Date Due</td>
                                   <td width="9%" align="center">Amount</td>
                                </tr>
								<tr class="Label">
									<td colspan="9" align="center"><hr/></td>
								</tr>
								<tr class="Label">
									<td colspan="9" align="center">
										<div id="billingdiv" align="center" class="innerTable">
											<table id="Bill" width="100%" cellpadding="0" cellspacing="0">
												<xsl:for-each select="root/billing_transactions/billing_transaction">
												<tr>
												<xsl:choose>
												<xsl:when test="@is_link = 'true'">
													<td width="11%">
														<div align="center"><a href="javascript:typeAction('{cbr_id}');"><xsl:value-of select="type"/></a></div>
													</td>
													</xsl:when>
													<xsl:otherwise>
													<td width="11%">
														<div align="center"><xsl:value-of select="type"/></div>
													</td>
													</xsl:otherwise>
													</xsl:choose>
													<td width="10%">
														<div align="center"><xsl:value-of select="amount"/></div>
													</td>
													<td width="11%">
														<div align="center"><xsl:value-of select="payment_type"/></div>
													</td>
													<td width="12%">
														<div align="center"><xsl:value-of select="refund_code"/></div>
													</td>
													<td width="14%">
														<div align="center"><xsl:value-of select="billed_date"/></div>
													</td>
													<td width="11%">
														<div align="center"><xsl:value-of select="settled_date"/></div>
													</td>
													<td width="11%">
														<div align="center"><xsl:value-of select="cbr_received_date"/></div>
													</td>
													<td width="11%">
														<div align="center"><xsl:value-of select="cbr_due_date"/></div>
													</td>
													<td width="9%">
														<div align="center"><xsl:value-of select="reversal"/></div>
													</td>
												</tr>
												</xsl:for-each>
              </table>
			</div>
                </td> </tr>
               <tr class="Label">
                 <td colspan="9" align="right"/> </tr>
               <tr>
                 <td colspan="9" align="center">
                   <p>&nbsp;</p>
                   

              <!--xsl:if test="'true' = $showprinter">
				<p align="right">
				<img src="images/printer.jpg" width="30" height="30" onclick="printIt()"/>
				</p>
				</xsl:if-->
        </td>
               </tr>
		  </table>
  		</td>
     </tr>
</table>
</form>
 </body>
 </xsl:template>
</xsl:stylesheet>