<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" indent="no"/>
<xsl:output indent="yes"/>
<xsl:param name="securitytoken"/>
<xsl:param name="context"/>
<xsl:template match="/root">

<html>
<head>
    <title>FTD - Source Code Lookup</title>
    <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
    <script language="javascript" src="js/FormChek.js"/>
    <script language="javascript" src="js/util.js"/>
    <script language="javascript">
    <![CDATA[
        var fieldNames = new Array("sourceCodeInput");
        var images = new Array("sourceCodeSearch", "sourceCodeCloseTop", "sourceCodeCloseBottom", "sourceCodeSelectNone");

        function init()
        {
            addDefaultListenersArray(fieldNames);
            addImageCursorListener(images);
            window.name = "VIEW_SOURCE_CODE_LOOKUP";
            window.focus();
            document.forms[0].sourceCodeInput.focus();
        }

        function onKeyDown()
        {
          if (window.event.keyCode == 13)
            reopenPopup();
        }

        function enterReOpenSourceCodePopup()
        {
          if (window.event.keyCode == 13)
            reopenPopup();
        }

        function reopenPopup()
        {
          var form = document.forms[0];
         
          
          //First validate the Source Code input
          var sourceCode = document.forms[0].sourceCodeInput.value;
          sourceCode = stripWhitespace(sourceCode);

          if(sourceCode == "" || sourceCode.length < 2)
          {
            form.sourceCodeInput.focus();
            form.sourceCodeInput.style.backgroundColor = 'pink';
            alert("Please correct the marked fields");
          }
          else 
          {
            form.target = window.name;
            form.dateFlag.value = "Y";
            var url_source="SourceCodeAction.do?sourceCodeInput=" + sourceCode + "&gcc_action=" + 'lookup_source_code' + "&securitytoken=" + document.forms[0].securitytoken.value + "&context=" + document.forms[0].context.value + "&company_id=" + document.SourceCodeLookupForm.gcc_company.value;
            form.action = url_source;
            form.submit();
          }
        }

        function closeSourceLookup(id)
        {
          if (window.event.keyCode == 13)
            populatePage(id);
        }

        function populatePage(id)
        {
          var sc, desc, part;
          if (id > -1){
            sc = document.getElementById("sourceCode" + id).innerHTML;
            desc = document.getElementById("desc" + id).innerHTML;
            part = document.getElementById("partner_id" + id).value;

          }

          window.returnValue = sc;
          window.close();
        }
    ]]>
    </script>
</head>

<body onLoad="javascript:init();">
<form name="SourceCodeLookupForm" method="post" action="">
<input type="hidden" name="securitytoken" value="{$securitytoken}"/>
<input type="hidden" name="context" value="{$context}"/>
<input type="hidden" name="gcc_company" value="{@company_id}"/>
<input type="hidden" name="dateFlag" value="Y"/>
<center>
    <table width="98%" border="0" cellpadding="2" cellspacing="2">
        <tr>
            <td align="center" class="header">Source Code Lookup</td>
        </tr>
        <tr>
            <td nowrap="true" colspan="3" align="left">
                <span class="instruction">Enter Source Code or Description value</span>&nbsp;
                <input type="text" name="sourceCodeInput" tabindex="1" size="20" maxlength="50" value="" onkeypress="javascript:enterReOpenSourceCodePopup();"/>
                <span class="instruction"> and press </span>
                <img id="sourceCodeSearch" tabindex="2" onkeydown="javascript:onKeyDown();" src="images/button_search.gif" alt="Search" border="0" onclick="javascript:reopenPopup()"/>
            </td>
        </tr>
        <tr>
            <td><hr/></td>
        </tr>
    </table>
    <table width="98%" border="0" cellpadding="2" cellspacing="2">
        <tr>
            <td align="right">
                <img id="sourceCodeCloseTop" tabindex="3" onkeydown="javascript:closeSourceLookup(-1)" src="images/button_close.gif" alt="Close screen" border="0" onclick="javascript:populatePage(-1)"/>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%" border="1" class="LookupTable" cellpadding="2" cellspacing="2">
                    <tr>
                        <td width="5%" class="label">&nbsp;</td>
                        <td class="label" valign="bottom">Name/Description</td>
                        <td class="label" valign="bottom">Offer</td>
                        <td class="label" align="center" valign="bottom">Service<br/>Charge</td>
                        <td class="label" align="center" valign="bottom">Expiration<br/>Date</td>
                        <td class="label" align="center" valign="bottom">Order<br/>Source</td>
                        <td class="label" align="center" valign="bottom">Source<br/>Code</td>
                        <td>
                            <xsl:for-each select="sourceCodeList/sourceCode">
                            <tr>
                                <td>
                                    <xsl:choose>
                                       <xsl:when test="expiredflag='Y'">
                                            <img alt="disabled" src="images/selectButtonRight_disabled.gif"/>
                                        </xsl:when>
                                        <xsl:when test="expiredflag='N'">
                                            <img onclick="javascript:populatePage({@num})" onkeydown="javascript:closeSourceLookup({@num})" tabindex="4" src="images/selectButtonRight.gif"/>
                                        </xsl:when>
                                    </xsl:choose>
                                </td>
                                <td id="desc{@num}" align="left"><xsl:value-of disable-output-escaping="yes" select="sourcedescription"/></td>
                                <td align="left"><xsl:value-of select="offerdescription"/></td>
                                <td align="center"><xsl:value-of select="servicecharge"/></td>
                                <td align="right"><xsl:value-of select="expirationdate"/></td>
                                <td align="center"><xsl:value-of select="ordersource"/></td>
                                <td id="sourceCode{@num}" align="center"><xsl:value-of select="sourcecode"/></td>
                                <input type="hidden" name="partner_id{@num}" value="{partner_id}"/>
                            </tr>
                            </xsl:for-each>
                        </td>
                    </tr>
                    <tr>
                        <td width="5%" valign="center">
                            <img id="sourceCodeSelectNone" tabindex="4" onkeydown="javascript:closeSourceLookup(-1)" src="images/selectButtonRight.gif" border="0" onclick="javascript:populatePage(-1);"/>
                        </td>
                        <td colspan="7" valign="top">None of the above</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="right">
                <img id="sourceCodeCloseBottom" tabindex="5" onkeydown="javascript:closeSourceLookup(-1)" src="images/button_close.gif" alt="Close screen" border="0" onclick="javascript:populatePage(-1)"/>
            </td>
        </tr>
    </table>
</center>
</form>
</body>
</html>

</xsl:template>
</xsl:stylesheet>