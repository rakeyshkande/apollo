<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
	<xsl:import href="security.xsl"/>
	<xsl:output method="html" indent="yes"/>
	<xsl:key name="error" match="ROOT/ERRORS/ERROR" use="@fieldname" />
	<xsl:key name="fields" match="ROOT/FIELDS/FIELD" use="@fieldname" />
	<xsl:key name="status" match="ROOT/GC_COUPONS/GC_COUPON" use="gc_coupon_status" />
	<xsl:variable name="error" select="ROOT/ERRORS/ERROR/@fieldname" />
	<xsl:variable name="gcc_type" select="key('fields','gcc_type')/@value" />
	<xsl:variable name="gcc_redemption_date" select="key('fields','gcc_redemption_date')/@value" />
	<xsl:variable name="gcc_company" select="key('fields','gcc_company')/@value" />
  <xsl:variable name="gcc_partner_program_name" select="key('fields','gcc_partner_program_name')/@value" />
	<xsl:variable name="gcc_status_single" select="key('fields','gcc_status_single')/@value" />
	<xsl:variable name="gcc_status_multiple" select="key('fields','gcc_status_multiple')/@value" />
	<xsl:variable name="saved_request_number" select="ROOT/@saved_request_number" />
	<xsl:template match="/">
		<html>
			<head>
				<META http-equiv="Content-Type" content="text/html"/>
				<title>
					Create Gift Certificates / Coupons
				</title>
				<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
				<link rel="stylesheet" type="text/css" href="css/calendar.css"/>
				<script type="text/javascript" src="js/clock.js"/>
				<script type="text/javascript" src="js/util.js"/>
				<script type="text/javascript" src="js/FormChek.js"/>
				<script type="text/javascript" src="js/calendar.js"/>
				<script type="text/javascript" src="js/date.js"/>
				<script type="text/javascript" src="js/create.js"/>
				<script type="text/javascript" src="js/common_functions_all.js"/>
				<script type="text/javascript" src="js/dateval.js"/>
				<script type="text/javascript">
				
				var reqNum = '<xsl:value-of select="key('fields','gcc_request_number')/@value"/>';
                var typ = '<xsl:value-of select="key('fields','gcc_type')/@value"/>';
                var formatvalue = '<xsl:value-of select="key('fields','gcc_format')/@value"/>';
				var redemtype = '<xsl:value-of select="key('fields','gcc_redemption_type')/@value"/>';
                var original;

<![CDATA[
function EnterToSourcePopup()
	{
	   if (window.event.keyCode == 13)
		{
		    openSourceCodePopup();
		}
	}

function CloseSourceLookup()
	{
	   if (window.event.keyCode == 13)
		{
		    goCancelSourceCode();
		}
	}


function init() {

    original = new Array(document.forms[0].length);

    original = saveFormValue(original);
    
    // Defect Fix : #14619
    if(document.form.src_code_exists_err_msg.value == '') {                                               
    	document.form.src_code_exists_err_msg.value = ']]><xsl:value-of select="ROOT/@src_code_exists_err_msg"></xsl:value-of><![CDATA[';                                                                     
    }
   
    if(document.form.src_code_expired_err_msg.value == '') {                                               
    	document.form.src_code_expired_err_msg.value = ']]><xsl:value-of select="ROOT/@src_code_expired_err_msg"></xsl:value-of><![CDATA[';                                                                     
    }
    
    
    

}

function saveFormValue(formValueArray) {

    for (var i=0;i<document.forms[0].length;i++)

    {
        current = document.forms[0].elements[i];

        if (current.type == "text" || current.type == "select-one") {

            formValueArray[i] = current.value;

        }
    }
     return formValueArray;
}



// Call this when Exit button is clicked.

// Returns true if any value on the form has changed.

// Returns false otherwise.

function testValueChange() {

    valueChanged = false;

    current = new Array(document.forms[0].length);
    if(original.length != current.length)
    {
        valueChanged = true;
    }

     current = saveFormValue(current);
     for (var i=0;i<current.length;i++)

    {
        if(original[i] != current[i])
        {
            valueChanged = true;
        }
    }
    return valueChanged;
}

               
      ]]>
				</script>
			</head>
			<body onload="displayDefault(), disableFields(), init();" onkeypress="enterkey();">
				<!-- Header-->
				<xsl:call-template name="IncludeHeader"/>
				<!-- Display table-->
				<form name="form" method="post">
					<div id="content" style="display:block">
						<xsl:call-template name="security"/>
						<input type="hidden" name="gcc_company_id" value="{key('fields','gcc_company')/@value}"/>
						<input type="hidden" name="saved_request_number" value="{ROOT/@saved_request_number}"/>
						<input type="hidden" name="request_number_for_edit_recipient" value="{ROOT/@request_number_for_edit_recipient}"/>
						<input type="hidden" name="success_msg" value="{ROOT/@success_msg}"/>
						<input type="hidden" name="src_code_exists_err_msg" value=""/>
						<input type="hidden" name="src_code_expired_err_msg" value=""/>
						<input type="hidden" name="exception_error" value="{ROOT/@exception_error}"/>
						<input type="hidden" name="status_multiple_count" value="{ROOT/@status_multiple_count}"/>
                        <input type="hidden" name="gcc_enable_create_cs" value="{key('fields','gcc_enable_create_cs')/@value}"/>
                                             <!-- hidden fields for the recipient data-->
                                         <xsl:for-each select="ROOT/GC_COUPON_RECIPIENTS/GC_COUPON_RECIPIENT">
                                <input type="hidden" name="gc_coupon_recip_id_{@num}" value="{gc_coupon_recip_id}"/>
																<input type="hidden" name="first_name_{@num}" id="first_name_{@num}" value="{first_name}"/>
																<input type="hidden" name="last_name_{@num}" id="last_name_{@num}" value="{last_name}"/>
																<input type="hidden" name="address_1_{@num}" id="address_1_{@num}" value="{address_1}"/>
																<input type="hidden" name="address_2_{@num}" id="address_2_{@num}" value="{address_2}"/>
																<input type="hidden" name="city_{@num}" id="city_{@num}" value="{city}"/>
															    <input type="hidden" name="state_{@num}" id="state_{@num}" value="{state}"/>
																<input type="hidden" name="zip_{@num}" id="zip_{@num}" value="{zip_code}"/>
										                        <input type="hidden" name="country_{@num}" id="country_{@num}" value="{country}"/>
																<input type="hidden" name="phone_{@num}" id="phone_{@num}" value="{phone}"/>
																<input type="hidden" name="email_{@num}" id="email_{@num}" value="{email_address}"/>
																<input type="hidden" name="issuedReferenceNumber_{@num}" id="issuedReferenceNumber_{@num}" value="{issuedReferenceNumber}"/>
                                            </xsl:for-each>
											<!-- END OF HIDDEN FIELDS-->
						<table width="98%" align="center" cellspacing="1" class="mainTable">
							<tr>
								<td>
									<table width="100%" align="center" class="innerTable">
										<tr>
											<td colspan="10" align="center" class="TotalLine">
												<div align="left">
													<!--Gift Certificate / Coupon Information-->
													<strong>Gift Certificate / Coupon Information</strong>
													<br/>
												</div>
											</td>
											<div align="left"></div>
										</tr>
										<tr class="Label">
											<td colspan="2" align="center" valign="top">
												<div align="right">Request Number: </div>
											</td>
											<td colspan="2" align="center">
												<div align="left">
															<input type="text" value="{key('fields','gcc_request_number')/@value}" tabindex="1" size="25" maxlength="12" id="gcc_request_number" name="gcc_request_number" />
															<div>
																<span id="span_request_number" class="ErrorMessage" style="display:none">Required</span>
															</div>
															<xsl:if test="$error = 'gcc_request_number'">
																<div>
																	<span id="request_server_error" class="ErrorMessage" style="display:block">
																		<xsl:value-of select="key('error','gcc_request_number')/@value" />
																	</span>
																</div>
															</xsl:if>
												</div>
											</td>
											<td colspan="2" align="center">
												<div align="right">Creation Date : </div>
											</td>
											  <td colspan="2" align="center" class="Label">
												<div align="left">
													<script type="text/javascript"><![CDATA[
                var d = new Date()
                 var zero = '0';
                 var month = d.getMonth() + 1;
                 var date = d.getDate()
                 if (month < 10)
                 {
                  month = zero + month;
                 }
                 if (date < 10)
                 {
                  date = zero + date;
                 }
                 document.write(month)
                 document.write("/")
                 document.write(date)
                 document.write("/")
                 document.write(d.getFullYear())
                  ]]></script></div>
											</td>
												<td colspan="2" align="center"></td>
										</tr>
										<tr class="Label">
											<td colspan="2" align="center" valign="top">
												<div align="right">Type: </div>
											</td>
											<td colspan="2" align="center">
												<div align="left">
													<select tabindex="2" id="gcc_type" name="gcc_type" onchange="onGccTypeChange(), disableFields();">
															<option value="none"></option>
															<xsl:for-each select="ROOT/GC_COUPON_TYPES/GC_COUPON_TYPE">
															<xsl:choose>
															<xsl:when test="gc_coupon_type = $gcc_type">
																	<option value="{gc_coupon_type}" selected="selected">
																		<xsl:value-of select="gc_coupon_type" />
																	</option>

															</xsl:when>
															<xsl:otherwise>
															<option value="{gc_coupon_type}">
																		<xsl:value-of select="gc_coupon_type" />
																	</option>

														</xsl:otherwise>
														</xsl:choose>
														</xsl:for-each>
													</select>
													<div>
														<span id="span_coupon_type" class="ErrorMessage" style="display:none">Required</span>
													</div>
												</div>
											</td>
											<td colspan="2" align="center" valign="top">
												<div align="right">Issue Date : </div>
											</td>
											<td colspan="2" align="center" valign="top">
												<div align="left">
													<input type="text" value="{key('fields','gcc_issue_date')/@value}" tabindex="5" size="25" maxlength="10" id="gcc_issue_date" name="gcc_issue_date" onchange="setExpireDate();" />
													<div>
														<span id="span_issue_date_error1" class="ErrorMessage" style="display:none">Invalid Date</span>
													</div>
													<div>
														<span id="span_issue_date_error2" class="ErrorMessage" style="display:none">Date has passed</span>
													</div>
													<div>
														<span id="span_issue_date" class="ErrorMessage" style="display:none">Required</span>
													</div>
												</div>
											</td>
												<td colspan="2" align="center">
														<div align="left">
															<img id="issue_date_img" name="DateCalendar" src="images\calendar.gif" tabindex="6"></img>
														</div>
													</td>
												</tr>
										<tr class="Label">
											<td colspan="2" align="center" valign="top">
												<div align="right">Issue Amount:</div>
											</td>
											<td colspan="2" align="center">
												<div align="left">
													<input type="text" value="{key('fields','gcc_issue_amount')/@value}" tabindex="3" size="25" maxlength="5" id="gcc_issue_amount" name="gcc_issue_amount" />
													<div>
														<span id="span_issue_amount" class="ErrorMessage" style="display:none">Required</span>
													</div>
													<div>
														<span id="span_issue_amount_error" class="ErrorMessage" style="display:none">Invalid Format</span>
													</div>
													<div>
														<span id="span_issue_amount_error1" class="ErrorMessage" style="display:none">No Cents Allowed</span>
													</div>
													<xsl:if test="$error = 'gcc_issue_amount'">
														<div>
															<span id="issue_server_error" class="ErrorMessage" style="display:block">
																<xsl:value-of select="key('error','gcc_issue_amount')/@value" />
															</span>
														</div>
													</xsl:if>
												</div>
											</td>
											<td colspan="2" align="center" valign="top">
												<div align="right">Expiration Date : </div>
											</td>
											<td colspan="2" align="center">
												<div align="left">
													<input type="text" value="{key('fields','gcc_expiration_date')/@value}" tabindex="7" size="25" maxlength="10" id="gcc_expiration_date" name="gcc_expiration_date" />
													<div>
														<span id="span_expiration_date_error1" class="ErrorMessage" style="display:none">Invalid Date</span>
													</div>
													<div>
														<span id="span_expiration_date_error2" class="ErrorMessage" style="display:none">Date has passed</span>
													</div>
													<div>
														<span id="span_expiration_date" class="ErrorMessage" style="display:none">Required</span>
													</div>
												</div>
											</td>
													<td colspan="2" align="center">
														<div align="left">
															<img id="exp_date_img" name="expireDateCalendar" tabindex="8" src="images/calendar.gif" />
														</div>
													</td>
												</tr>
										<tr class="Label">
											<td colspan="2" align="center" valign="top">
												<div align="right">Paid Amount: </div>
											</td>
											<td colspan="2" align="center">
												<div align="left">
													<input type="text" value="{key('fields','gcc_paid_amount')/@value}" tabindex="4" size="25" maxlength="5" id="gcc_paid_amount" name="gcc_paid_amount" />
													<div>
														<span id="span_paid_amount" class="ErrorMessage" style="display:none">Required</span>
													</div>
													<div>
														<span id="span_paid_amount_error" class="ErrorMessage" style="display:none">Invalid Format</span>
													</div>
												</div>
											</td>
											<td colspan="2" align="center" valign="top">
												<div align="right">Quantity : </div>
											</td>
											<td colspan="2" align="center">
												<div align="left">
													<xsl:choose>
														<xsl:when test="saved_request_number = ''">
															<input type="text" value="{key('fields','gcc_quantity')/@value}" tabindex="9" size="25" maxlength="6" id="gcc_quantity" name="gcc_quantity" disabled="true"/>
														</xsl:when>
														<xsl:otherwise>
															<input type="text" value="{key('fields','gcc_quantity')/@value}" tabindex="9" size="25" maxlength="6" id="gcc_quantity" name="gcc_quantity" onchange="addRecipientButton();"/>
															<div>
																<span id="span_quantity" class="ErrorMessage" style="display:none">Required</span>
															</div>
															<div>
																<span id="span_quantity_error" class="ErrorMessage" style="display:none">Invalid Format</span>
															</div>
														</xsl:otherwise>
													</xsl:choose>
												</div>
											</td>
											<td colspan="2" align="center"></td>
										</tr>
										<tr class="Label">
											<td width="38" align="center"></td>
											<td width="40" align="center">
												<div align="left"></div>
											</td>
											<td colspan="2" align="center">
												<div align="left"></div>
											</td>
											<td colspan="2" align="center" valign="top">
												<div align="right">Format :</div>
											</td>
											<td colspan="2" align="center">
												<div align="left">
													<select tabindex="10" name="gcc_format" id="gcc_format">
														<option value="E">E - Electronic</option>
														<option value="P" selected="true">P - Paper</option>
													</select>
													<div>
														<span id="span_format" class="ErrorMessage" style="display:none">Required</span>
													</div>
												</div>
											</td>
											<td colspan="2" align="center"></td>
										</tr>
										<tr class="Label">
											<td align="center"></td>
											<td align="center">
												<div align="left"></div>
											</td>
											<td colspan="2" align="center">
												<div align="left"></div>
											</td>
											<td colspan="2" align="center" valign="top" id="gcc_filename_name">
												<div align="right">Filename : </div>
											</td>
											<td colspan="2" align="center">
												<div align="left">
													<input type="text" value="{key('fields','gcc_filename')/@value}" tabindex="11" size="25" maxlength="64" id="gcc_filename" name="gcc_filename" disabled="true" class="disabledfield" />
												</div>
											</td>
											</tr>
										<tr class="Label">
											<td height="22" colspan="10" align="center">
												<div align="left"></div>
												<!--Program Information-->
												<div align="left" class="TotalLine">Program Information </div>
											</td>
										</tr>
										<tr class="Label">
											<td colspan="2" align="center" valign="top">
												<div align="right">Requestor Name: </div>
											</td>
											<td colspan="2" align="center">
												<div align="left">
													<input type="text" value="{key('fields','gcc_requestor_name')/@value}" tabindex="12" size="25" maxlength="32" id="gcc_requestor_name" name="gcc_requestor_name" />
													<div>
														<span id="span_requestor_name" class="ErrorMessage" style="display:none">Required</span>
													</div>
												</div>
											</td>
											<td colspan="2" align="center" valign="top">
												<div align="right">Company:</div>
											</td>
											<td colspan="2" align="center">
												<div align="left">
													<select tabindex="18" id="gcc_company" name="gcc_company">
														<xsl:for-each select="ROOT/COMPANYS/COMPANY">
														<xsl:choose>
																<xsl:when test="company_id = $gcc_company">
																   <option value="{company_id}" selected="selected">
																		<xsl:value-of select="company_name" />
																	</option>
																	</xsl:when>
																<xsl:otherwise>
																<option value="{company_id}">
																		<xsl:value-of select="company_name" />
																	</option>
															</xsl:otherwise>
															</xsl:choose>
															</xsl:for-each>
														</select>
												</div>
											</td>
											<td colspan="2" align="center"></td>
										</tr>
										<tr class="Label">
											<td colspan="2" align="center" valign="top">
												<div align="right">Redemption Type: </div>
											</td>
											<td colspan="2" align="center" valign="top">
												<div align="left">
													<select tabindex="13" id="gcc_redemption_type" name="gcc_redemption_type">
														<option value="I">I - Internet</option>
														<option value="P" selected="true">P - Phone</option>
													</select>
													<div>
														<span id="span_redemption_type" class="ErrorMessage" style="display:none">Required</span>
													</div>
												</div>
											</td>
											<td colspan="2" align="center" valign="top">
												<div align="right">Restricted Source Code(s): </div>
											</td>
											<td colspan="2" align="center">
												<div align="left">
													<textarea cols="30" rows="3" id="gcc_source_code" name="gcc_source_code" tabindex="19">
														<xsl:value-of select="key('fields','gcc_source_code')/@value" />
													</textarea>													
													<div>
														<span id="span_source_code_format_error" class="ErrorMessage" style="display:none">Invalid Format</span>
													</div>
													<xsl:if test="$error = 'gcc_source_code'">
														<div>
															<span id="sourcecode_server_error" class="ErrorMessage" style="display:block">
																<xsl:value-of select="key('error','gcc_source_code')/@value" />
															</span>
														</div>
													</xsl:if>
												</div>
											</td>											
										</tr>
										<tr class="Label">
													<td colspan="2" align="center" valign="top">
														<div align="right">Program Name: </div>
													</td>
													<td colspan="2" align="center">
														<div align="left">
															<input type="text" value="{key('fields','gcc_program_name')/@value}" tabindex="14" size="25" maxlength="32" id="gcc_program_name" name="gcc_program_name" />
															<div>
																<span id="span_program_name" class="ErrorMessage" style="display:none">Required</span>
															</div>
														</div>
													</td>
                                             <td colspan="2" align="center" valign="top">
                                             <div align="right">Promotion Id:</div>
                                             </td>
                                             <td colspan="2" align="center"><div align="left">
                                              <input type="text" value="{key('fields','gcc_promotion_id')/@value}" tabindex="19" size="25" maxlength="20" id="gcc_promotion_id" name="gcc_promotion_id"/>
                                              <div>
                                                    <span id="span_promotion_id" class="ErrorMessage" style="display:none">Required</span>
                                              </div>
                                              <div>
                                                    <span id="span_promotion_id_with_space" class="ErrorMessage" style="display:none">No Space Allowed</span>
                                              </div>
                                              </div>
                                              </td>
											<td colspan="2" align="center"></td>
										</tr>
                    <tr class="Label">
													<td colspan="2" align="center" valign="top">
														<div align="right">Partner Program Name: </div>
													</td>
													<td colspan="2" align="center">
														<div align="left">
                              <select tabindex="15" id="gcc_partner_program_name" name="gcc_partner_program_name" disabled="true" class="disabledfield">
                                <option value="none"></option>
                                <xsl:for-each select="ROOT/GC_PROGRAM_NAMES/GC_PROGRAM_NAME">
                                  <xsl:choose>
                                    <xsl:when test="program_name = $gcc_partner_program_name">
                                        <option value="{program_name}" selected="selected">
                                          <xsl:value-of select="program_name" />
                                        </option>
                                    </xsl:when>
                                    <xsl:otherwise>
                                      <option value="{program_name}">
                                          <xsl:value-of select="program_name" />
                                      </option>
                                    </xsl:otherwise>
                                  </xsl:choose>
                                </xsl:for-each>
                              </select>
                            </div>
                          </td>
                    </tr>
										<tr class="Label">
													<td colspan="2" align="center" valign="top">
														<div align="right">Program Description: </div>
													</td>
													<td colspan="8" align="center">
														<div align="left">
															<input type="text" value="{key('fields','gcc_program_description')/@value}" tabindex="16" size="100" maxlength="64" id="gcc_program_description" name="gcc_program_description" />
															<div>
																<span id="span_program_description" class="ErrorMessage" style="display:none">Required</span>
															</div>
														</div>
													</td>
										</tr>
										<tr class="Label">
											<td colspan="2" align="center" valign="top">
												<div align="right">Comments: </div>
											</td>
											<td colspan="8" align="center">
												<div align="left">
													<input type="text" value="{key('fields','gcc_comments')/@value}" tabindex="17" size="100" maxlength="320" id="gcc_comments" name="gcc_comments" />
												</div>
											</td>
										</tr>
											<tr class="Label">
												<td colspan="10" align="center" class="TotalLine">
													<div align="left">Additional Information </div>
												</td>
											</tr>
											<tr>
												<td colspan="2" align="center" class="Label" valign="top">
													<div align="right">Prefix: </div>
												</td>
												<td colspan="2" align="center">
													<div align="left">
														<input type="text" value="{key('fields','gcc_prefix')/@value}" tabindex="21" size="25" maxlength="4" id="gcc_prefix" name="gcc_prefix"/>
													</div>
												</td>
												<td colspan="2" align="center" valign="top">
													<div align="left" class="Label">
														<div align="right">Other:</div></div>
												</td>
												<td colspan="4" align="center">
													<div align="left">
														<input type="text" value="{key('fields','gcc_other')/@value}" tabindex="22" size="45" maxlength="32" id="gcc_other" name="gcc_other" />
													</div>
												</td>
											</tr>
											<tr class="Label">
												<td colspan="10" align="center" class="Label">
													<div align="right">
														<p>&nbsp;</p>
														<p>&nbsp;</p>
													</div>
												</td>
											</tr>
                                            
									</table>
									<!-- used to display the look up source code action pop up window-->
<DIV id="sourceCodeLookup" style="VISIBILITY: hidden; position: absolute; bottom:125px; left: 350px ;border: 1px solid; border-color: 006699; background:#C1D1EC;">
<center>
<DIV id="sourceCodeLookupSearching" style="VISIBILITY: hidden;">
	<font face="Verdana" color="#FF0000">Please wait ... searching!</font>
</DIV>
<DIV id="updateWin" style="VISIBILITY: hidden; position: absolute; border: 1px solid; border-color: 006699; background:#C1D1EC;">
	<br/><br/><p align="center"> <font face="Verdana">Please wait ... updating!</font> </p>
</DIV>
</center>
<TABLE width="98%" border="0" cellpadding="2" cellspacing="2">
	        <TR>
	            <TD class="label" colspan="3" align="left">
					<font face="Verdana"><strong>Source Code Lookup </strong></font>
	            </TD>
	        </TR>
	        <TR>
	            <TD nowrap="true" colspan="3" align="left">
	            	<SPAN class="label"> <font face="Verdana">Source Code or Description:</font> </SPAN> &nbsp;&nbsp;
	            	<INPUT tabindex="31" onkeydown="javascript:EnterToSourcePopup()" type="text" name="sourceCodeInput" size="30" maxlength="50" onFocus="javascript:fieldFocus('sourceCodeInput')" onblur="javascript:fieldBlur('sourceCodeInput')"/> &nbsp;&nbsp;
	            </TD>
	        </TR>
	        <TR>
				<TD colspan="3" align="right">
				<IMG onkeydown="javascript:EnterToSourcePopup()" tabindex="32" src="images/button_search.gif" alt="Search" border="0" onclick="javascript:goSearchSourceCode()"/>
             			<IMG onkeydown="javascript:CloseSourceLookup()" tabindex="32" src="images/button_close.gif" alt="Close screen" border="0" onclick="javascript:goCancelSourceCode()"/>
	         	</TD>
	        </TR>
	        <TR>
	            <TD colspan="3"></TD>
	        </TR>
	    </TABLE>
</DIV>
								</td>
							</tr>
						</table>
						<!--buttons-->
							<table width="98%" border="0" align="center" cellpadding="0" cellspacing="1">
								<tr>
									<td width="91%" align="center">
										<button class="BlueButton" id="gcc_submit_create_save" name="gcc_submit_create_save" tabindex="23" accesskey="C" onclick="javascript:createGC();">(C)reate</button>
										<button class="BlueButton" id="gcc_submit_display_recipient" name="gcc_submit_display_recipient" tabindex="24" accesskey="A" onclick="javascript:addRecipient();">(A)dd Recipient</button>
										<button class="BlueButton" id="gcc_reset_button" name="gcc_reset_button"  tabindex="26" accesskey="F" onclick="javascript:doClearFields();">Clear (F)ields</button>
									</td>
									<td width="9%" align="right">
										<button class="BlueButton" id="gcc_reset_button" name="gcc_submit_exit" tabindex="27" accesskey="E" onClick="javascript:exit();">(E)xit</button>
									</td>
								</tr>
							</table>
						</div>
					<!--  Used to dislay Processing... message to user-->
					<div id="waitDiv" style="display:none">
						<table id="waitTable" class="mainTable" width="98%" height="100%" align="center" cellpadding="30" cellspacing="1">
							<tr>
								<td width="100%">
									<table width="100%" cellspacing="0" cellpadding="0" border="0">
										<tr>
											<td id="waitMessage" align="right" width="50%" class="waitMessage" />
											<td id="waitTD" width="50%" class="waitMessage" />
										</tr>
									</table></td>
							</tr>
						</table>
					</div>
					

<!--Copyright bar-->
<xsl:call-template name="footer"/>
</form>
</body>
</html>
		<script type="text/javascript">
  Calendar.setup(
    {
      inputField  : "gcc_issue_date",  // ID of the input field
      ifFormat    : "mm/dd/y",  // the date format
      button      : "issue_date_img"  // ID of the button
    }
  );
		</script>
		<script type="text/javascript">
  Calendar.setup(
    {
      inputField  : "gcc_expiration_date",  // ID of the input field
      ifFormat    : "mm/dd/y",  // the date format
      button      : "exp_date_img"  // ID of the button
    }
  );
		</script>


<SCRIPT language="JavaScript">

<![CDATA[
	function openSourceCodeLookup()
	{
		//In case the DIV is opened a second time
		document.forms[0].sourceCodeInput.style.backgroundColor='white';

		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers

		//cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		sourcecodesearch = (dom)?document.getElementById("sourceCodeLookup").style : ie? document.all.sourceCodeLookup : document.sourceCodeLookup
		//scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		//sourcecodesearch.top=scroll_top+document.body.clientHeight/2 -10

		sourcecodesearch.width=190
		sourcecodesearch.height=100
		//sourcecodesearch.left=document.body.clientWidth/2 - 100
		sourcecodesearch.visibility=(dom||ie)? "visible" : "show"

		//Populate the source code input with the source code on the page and set the focus
		document.all.sourceCodeInput.value = document.all.gcc_source_code.value;
		document.all.sourceCodeInput.focus();

	}

	function goCancelSourceCode()
	{
		sourcecodesearch.visibility= "hidden";
		document.forms[0].gcc_source_code.focus();
	}

	function goSearchSourceCode()
	{
		//First validate the Source Code input
		var check = true;
		var sourceCode = document.forms[0].sourceCodeInput.value;
		var sourceCode = stripWhitespace(sourceCode);

		if(sourceCode == "" || sourceCode.length < 2)
		{
		   document.forms[0].sourceCodeInput.focus();
           document.forms[0].sourceCodeInput.style.backgroundColor='pink'
           check = false;
           alert("Please correct the marked fields");
		}

		if (check)
           openSourceCodePopup();
	}

	function openSourceCodePopup()
	{
		
		var val = document.all.sourceCodeInput.value;
		

		//First validate the sourcd code input
		var check = true;
		var val = stripWhitespace(val);

	   	//Check to make sure the code exists
		if(val == "")
		{
			if (check == true)
			{
			  check = false;
			}
		}

		if (check == false)
		{
			document.all.sourceCodeInput.style.backgroundColor='pink'
			document.all.sourceCodeInput.focus();
			alert("Please correct the marked fields");
			return false;
		}

		//Every is valid, so open the popup
		document.all.sourceCodeLookupSearching.style.visibility = "visible";

		var form = document.forms[0];

	    var url_source="SourceCodeAction.do?POPUP_ID=LOOKUP_SOURCE_CODE&sourceCodeInput=" + val + "&gcc_action=" + 'lookup_source_code' + "&securitytoken=" + document.forms[0].securitytoken.value + "&context=" + document.forms[0].context.value + "&company_id=" + document.forms[0].gcc_company.value;
	    var modal_dim="dialogWidth:800px; dialogHeight:650px; center:yes; status=0";
	    var ret =  window.showModalDialog(url_source,"", modal_dim);
	    //var ret =  window.open(url_source,"", modal_dim);

 		//in case the X icon is clicked
 		if (!ret)
 		{
                    var ret = new Array();
                    ret[0] = '';
 		}

 	 	//populate the text box
   if (ret[0] != '')
    {
			document.all.gcc_source_code.value = ret;
    }

		//Set the focus on the next field
		document.forms[0].gcc_source_code.focus();

		sourcecodesearch.visibility= "hidden";
		document.all.sourceCodeLookupSearching.style.visibility = "hidden";
	}

]]>
</SCRIPT>
	</xsl:template>
	<!--to get the header title-->
		<xsl:template name="IncludeHeader">
		<xsl:call-template name="header">
		    <xsl:with-param name="showheader" select="true()"/>
			<xsl:with-param name="headerName" select="'Create Gift Certificates / Coupons'"/>
		</xsl:call-template>
	</xsl:template>
	<!--end-->
</xsl:stylesheet>