<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes"/>
<xsl:template name="header" match="/">

<xsl:param name="showBackButton"/>
<xsl:param name="headerName"/>

    <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
    <script type="text/javascript" src="js/clock.js"/>
    <script type="text/javascript" language="javascript">
       <![CDATA[
    ]]>
    </script>
       <table width="98%" border="0" align="center" cellpadding="0" cellspacing="1">
      <tr>
         <td rowspan="2" width="20%" align="left" >
            <img border="0" src="images/logo.gif" align="absmiddle"/>

         </td>
         <td width="62%" align="center" colspan="1" class="header"><xsl:value-of select="$headerName"/></td>
         <xsl:if test="$showBackButton">
         <td align="right"><button style="width:100" name="back" id="top_back" class="BlueButton" accesskey="B" onclick="doBackAction();">(B)ack</button></td>
         </xsl:if>
	 </tr>
      <tr>
		 <td align="center" class="Label"></td>
         <td width="18%" align="right" id="time" class="Label"></td><script type="text/javascript">startClock();</script>
      </tr>
	  <tr>
         <td colspan="3">
            <hr/>
         </td>
      </tr>
      <tr>
		<td colspan="3" align="center">
			<span id="span_display_message" class="SuccessMessage" style="display:block">
					<xsl:value-of select="root/@displayMessage"/>
				</span>
			</td>
		</tr>
   </table>
       </xsl:template>
   </xsl:stylesheet>