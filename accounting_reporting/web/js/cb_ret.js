/************************************
required fields
**************************************/
var reqFields = new Array('date_rcvd','amount','reason');

var errorspansreq = new Array('span_date_rcvd','span_amount','span_reason');


function checkReqFields()
{
			var rLength = reqFields.length;
			var flag = 'true';

				for (var i = 0; i < rLength; i++)
				{
					var reqType = reqFields[i];

					if (document.getElementById(reqType).value != null)
							{
							var entry = document.getElementById(reqType).value;
							var reqerror = errorspansreq[i];

							fieldlength=entry.length;
										if (entry == '' || entry == 'none')
										{
												document.getElementById(reqerror).style.display = "block";
												document.getElementById(reqType).style.backgroundColor ='pink';
												flag = 'false';
										}
							}
				}

			return flag;
}


/************************************
to check the date fields
**************************************/

var dateFields = new Array('date_rcvd','date_due','date_rvsl','date_rtrn');
var errorspansdate = new Array('span_date_rcvd_error','span_date_due_error','span_date_rvsl_error','span_date_rtrn_error');


function checkDates()
{
			var dLength = dateFields.length;
			var flag = 'true';

				for (var i = 0; i < dLength; i++)
				{
					var dateType = dateFields[i];

					if (document.getElementById(dateType) != null)
							{
							var entry = document.getElementById(dateType).value;
							var dateerror = errorspansdate[i];

								if(entry != '' && !ValidateDate(dateType))
										{
										document.getElementById(dateerror).style.display = "block";
										document.getElementById(dateType).style.backgroundColor='pink';
										flag = 'false';
										}
							}

				}
			return flag;
}

/************************************
to check the amount field
**************************************/
function checkcbrAmount()
{
var cbr_action = document.getElementById('cbr_action').value;
var flag = 'true';
var entry = document.getElementById('amount').value;
var payment = entry.split('.');

if (document.getElementById('amount') != null)
{
		 	if(!isInteger(payment[0]) || (payment.length > 1 && (!isInteger(payment[1]) || (payment[1].length > 2))))
		 	{
		 	document.getElementById('span_amount_error').style.display = "block";
			document.getElementById('amount').style.backgroundColor='pink';
			flag = 'false';
			}

			if (payment.length > 2)
			{
					document.getElementById('span_amount_error').style.display = "block";
					document.getElementById('amount').style.backgroundColor='pink';
					flag = 'false';
			}
			if(entry != '' && flag == 'true')
			{
				if (cbr_action == 'add' || cbr_action == 'display_add')
				{

				var amt = document.getElementById('payment_amount').value;
				var stripdollar = '$()'
				var nodollaramt = stripCharsInBag (amt, stripdollar)

					if (scltCompareDouble(nodollaramt,entry) == -1)
					{
					document.getElementById('span_amount_error1').style.display = "block";
					document.getElementById('amount').style.backgroundColor='pink';
					flag = 'false';
					}
					else
					{
					}
				}
			}
	}
	    return flag;
}


/**********************************
Reset the error fields
************************************/
var spans = new Array('span_amount_error','span_amount_error1','span_payment_error','span_date_rcvd_error','span_date_due_error','span_date_rvsl_error','span_date_rcvd','span_amount','span_reason')
function resetErrorFields()
{
		for(var i = 0; i < spans.length; i++)
		{
				var spanField = spans[i];
				if (document.getElementById(spanField)!=null)
				{
				document.getElementById(spanField).style.display = "none";
				}
		}
}


/***********************************
The Save function
**************************************/
function saveCBR()
{
  document.getElementById('button_save').disabled = true;
var cbr_action = document.getElementById('cbr_action').value;

if(cbr_action == 'add' || cbr_action == 'display_add')
		{
               if (document.getElementById('payment_amount').value == '')
               {
                document.getElementById('span_payment_error').style.display = "block";
                document.getElementById('button_save').disabled = false;
                return false;
               }
        }
      resetErrorFields();
      defaultColor();

  	var flag1 = 'true';
		var flag = 'true'

    
		flag = checkReqFields();
		if (flag == 'false' && flag1 == 'true')
			flag1 = 'false';
        
		flag = checkcbrAmount();
  
		if (flag == 'false' && flag1 == 'true')
			flag1 = 'false';
  
		flag = checkDates();
		if (flag == 'false' && flag1 == 'true')
			flag1 = 'false';


		if (flag1 == 'true')
		{
			doSaveAction();
		}
    else
    {
      document.getElementById('button_save').disabled = false;
    }
}

function doAddAction()
{
    if ( cbrtype == 'CB')	{
        document.forms[0].action = "AddChargeBack.do" + "?cbr_action=add";
        document.forms[0].submit();
    }  else {
        // cbrtype == 'RT'
        document.forms[0].action = "AddRetrieval.do" + "?cbr_action=add";
        document.forms[0].submit();
    }
}

/*************************************************
This is when you click the Add Charge back button
**************************************************/
function doSaveAction()
{
        var cbr_action = document.getElementById('cbr_action').value;
        
        if( cbr_action == 'add' || cbr_action == 'display_add')
        {
          
          var enteredAmt = document.getElementById('amount').value;
          var paymentAmt = document.getElementById('payment_amount').value;
          var stripdollar = '$()';
          var nodollaramt = stripCharsInBag (paymentAmt, stripdollar)
        
          if (scltCompareDouble(nodollaramt,enteredAmt) == 1) {
            // alert if entered amount is smaller than payment amount.
            if(displaycomfirm()) {
                doAddAction();
            } 
          } else {
              doAddAction();
          }
        }
        // cbr_action == 'edit' or cbr_action == 'display_edit'
        else
        {
            document.getElementById('cbr_id').value = cbrid;
            document.forms[0].action = "EditCBR.do" + "?cbr_action=edit" + "&cbr_id =" + document.getElementById('cbr_id').value;
            document.forms[0].submit();
        }    


/*
	if ( cbrtype == 'CB')
	{
		if( cbr_action == 'add' || cbr_action == 'display_add')
		{
		document.forms[0].action = "AddChargeBack.do" + "?cbr_action=add";
		document.forms[0].submit();
		}
		else
		{
		document.getElementById('cbr_id').value = cbrid;
		document.forms[0].action = "EditCBR.do" + "?cbr_action=edit" + "&cbr_id =" + document.getElementById('cbr_id').value;
		document.forms[0].submit();
		}
	}
	else
	{
		if( cbr_action == 'add' || cbr_action == 'display_add')
		{
		document.forms[0].action = "AddRetrieval.do" + "?cbr_action=add";
		document.forms[0].submit();
		}
		else
		{
		document.getElementById('cbr_id').value = cbrid;
		document.forms[0].action = "EditCBR.do" + "?cbr_action=edit" + "&cbr_id =" + document.getElementById('cbr_id').value;
		document.forms[0].submit();
		}

	}
*/
}

/****************************************************
This is displayed when the entered amount is smaller 
than the payment amount selected.
*****************************************************/
function displaycomfirm()
{
 		var returnval = false;
    var modalUrl = "confirmstatus.html";
    var modalArguments = new Object();
    modalArguments.modalText = 'The amount you entered is smaller than the payment amount selected. Are you sure you want to proceed?';
    var modalFeatures  =  'dialogWidth:500px; dialogHeight:175px; center:yes; status=no; help=no;';
    modalFeatures +=  'resizable:no; scroll:no';
    var ret = window.showModalDialog(modalUrl, modalArguments, modalFeatures);
    if(ret)
    {
 			returnval = true;
 		}
    
    return returnval;
}

/****************************************************
This is when you click on the Cancel Button
*****************************************************/

function cancelCBR()
{

	if (testValueChange() == true)
			{
        var modalUrl = "confirmstatus.html";
        var modalArguments = new Object();
        modalArguments.modalText = 'Are you sure you want to exit without saving your changes?';
        var modalFeatures  =  'dialogWidth:250px; dialogHeight:175px; center:yes; status=no; help=no;';
        modalFeatures +=  'resizable:no; scroll:no';
        var ret = window.showModalDialog(modalUrl, modalArguments, modalFeatures);
         if(ret)
            {
						document.forms[0].action = "ViewBilling.do"+ "?cbr_action=view";
						document.forms[0].submit();
						}
            else
            {
            return false;
            }
			}
			else
			{
			document.forms[0].action = "ViewBilling.do"+ "?cbr_action=view";
			document.forms[0].submit();
			}
}



/****************************************************
This function is used to highlight alternate rows
****************************************************/
function alternateRows(tableObject,startRow) {
var TABLE_ROW_COLOR_DEFAULT = "#FFFFFF";
var TABLE_ROW_COLOR_HILIGHT = "#C0D7ED";


	 var currentRowColor = "";
	 for (var i = startRow; i < tableObject.rows.length; i++) {

		currentRowColor = TABLE_ROW_COLOR_DEFAULT;

		if (i % 2 == 0) {
			currentRowColor = TABLE_ROW_COLOR_HILIGHT;
			}

	   tableObject.rows[i].style.backgroundColor = currentRowColor;

	   }

}

function highlightRow()
{
	 alternateRows(document.getElementById("payment"),0);

}
/******************************************************
This function is to disable the amount field on the page
******************************************************/

function disableAmount()
{
	if (amdis == 'true')
	{
	document.getElementById('amount').disabled = true;
	document.getElementById('amount').style.backgroundColor = '#eeeeee';
	}
	else
	{
	document.getElementById('amount').disabled = false;
	document.getElementById('amount').style.backgroundColor = '#ffffff';
	}
}

/******************************************************
This function is to disable the reversal date field on the page
******************************************************/

function disableRevDate()
{
	if ( cbrtype == 'CB')
	{
			if (revdis == 'true')
			{
			document.getElementById('date_rvsl').disabled = true;
			document.getElementById('date_rvsl').style.backgroundColor = '#eeeeee';
            document.getElementById('date_rvsl_img').disabled = true;
			}
			else
			{
			document.getElementById('date_rvsl').disabled = false;
			document.getElementById('date_rvsl').style.backgroundColor = '#ffffff';
			}
	}
}



/******************************************************
This function is set the select the amount field on the page
******************************************************/
function setFields(amt, id)
{
document.getElementById('payment_amount').value = amt;
document.getElementById('payment_id').value = id;
}
/***********************************************************
set colour from pink back to white once the form is refreshed
************************************************************/
function defaultColor()
{
var NumElements=document.form.elements.length;
      for (i=0; i<NumElements;i++)
			{
					if (document.form.elements[i].type=="text" || document.form.elements[i].type == "select-one")
					{
					document.form.elements[i].style.backgroundColor='#FFFFFF';
					}
			}
}