/****************************************************
This function is used to highlight alternate rows
****************************************************/
function alternateRows(tableObject,startRow) {
var TABLE_ROW_COLOR_DEFAULT = "#FFFFFF";
var TABLE_ROW_COLOR_HILIGHT = "#C0D7ED";


	 var currentRowColor = "";
	 for (var i = startRow; i < tableObject.rows.length; i++) {

		currentRowColor = TABLE_ROW_COLOR_DEFAULT;

		if (i % 2 == 0) {
			currentRowColor = TABLE_ROW_COLOR_HILIGHT;
			}

	   tableObject.rows[i].style.backgroundColor = currentRowColor;

	   }

}

function highlightRow()
{
	 alternateRows(document.getElementById("Bill"),0);
}

/***************************************
this is when u click the printer button
***********************************/
function printIt()
{
url = "PrintBilling.do" + "?cbr_action=print"
+"&securitytoken=" + document.forms[0].securitytoken.value
+"&context=" + document.forms[0].context.value
+"&master_order_number=" + document.forms[0].master_order_number.value
+"&order_guid=" + document.forms[0].order_guid.value;
var sFeatures="dialogHeight:500px;dialogWidth:800px;";
 sFeatures +=  "status:1;scroll:0;resizable:0";

window.showModalDialog(url,"" , sFeatures);


}
/*************************************************
This is when you click the Add Charge back button
**************************************************/
function addChargeBack(msg)
{
  if(msg == "") {
    document.forms[0].action = "AddChargeBack.do" + "?cbr_action=display_add";
    document.forms[0].submit();
  } else {
    alert(msg);
  }
}
/*************************************************
This is when you click the Add Retrieval button
**************************************************/
function addRetrieval(msg)
{
  if(msg == "") {
    document.forms[0].action = "AddRetrieval.do" + "?cbr_action=display_add";
    document.forms[0].submit();
  } else {
    alert(msg);
  }
}

/*************************************************
This is when you click the link of the type field
**************************************************/
function typeAction(id)
{
document.getElementById('cbr_id').value = id
document.forms[0].action = "EditCBR.do" + "?cbr_action=display_edit" +"&cbr_id=" + document.getElementById('cbr_id').value;
document.forms[0].submit();
}

/*************************************************
This is when you click the Back button
**************************************************/
function doBackAction()
{
document.forms[0].action = "CBRMenu.do" + "?cbr_action=shopping_cart_info";
document.forms[0].submit();
}

/*************************************************
This is when you click the Main Menu button
**************************************************/
function doMainMenu()
{
        //document.forms[0].action = "CBRMenu.do" + "?cbr_action=main";
        //document.forms[0].submit();
        var url = '';
 				var modalUrl = "confirmstatus.html";
        var modalArguments = new Object();
        modalArguments.modalText = 'Have you completed your work on this order?';
        var modalFeatures  =  'dialogWidth:200px; dialogHeight:150px; center:yes; status=no; help=no;';
        modalFeatures +=  'resizable:no; scroll:no';
        var ret = window.showModalDialog(modalUrl, modalArguments, modalFeatures);
         
        if(ret)
        {
 							url = "CBRMenu.do" + "?cbr_action=main" +	"&work_complete=y" 
                  +"&securitytoken=" + document.forms[0].securitytoken.value
                  +"&context=" + document.forms[0].context.value;
 				} else
        {
              url = "CBRMenu.do" + "?cbr_action=main" +	"&work_complete=n" 
                  +"&securitytoken=" + document.forms[0].securitytoken.value
                  +"&context=" + document.forms[0].context.value;
        }
        document.forms[0].action = url;
        document.forms[0].submit();
}

/****************************************************
This is to display the pop-up on load
*****************************************************/
function displaypopup()
{
     if (popup == 'true')
     {
 				var modalUrl = "confirmstatus.html";
         var modalArguments = new Object();
         modalArguments.modalText = 'Would you like to view Customer Hold Information for the customer assosiated with shopping cart ' + ord_num + ' ?';
         var modalFeatures  =  'dialogWidth:500px; dialogHeight:175px; center:yes; status=no; help=no;';
         modalFeatures +=  'resizable:no; scroll:no';
         var ret = window.showModalDialog(modalUrl, modalArguments, modalFeatures);
        
         if(ret)
         {
 							document.forms[0].action = "CBRMenu.do" +"?cbr_action=cust_hold_info";
 							document.forms[0].submit();
 				 }

 			}

}