function isEmptyString(testString)
{
   if (testString == null) {
	return true;
   }

   var newString = testString.replace(/\s/g, "");
   if ((newString == null) || (newString.length == 0)) {
      return true;
   }
   return false;
}


function check_date(field, lengthOfYearField)
{
     if (lengthOfYearField == "BOTH")
          return check_date(field,"2") || check_date(field,"4");

     var seperator = "/";
     
     var returnValue = false;
     var counter;
     
     var yearLengthAsInt = parseInt(lengthOfYearField,10);
 
     // If there is more than one of this field on the form,
     // we only want the first one.
     var DateField;     
     if (field.length)
          DateField = field[0];
     else
          DateField = field;

     // Is the field present on the form?
     var undefined;
     if (field != undefined)
     {
          if (!isEmpty(field))
          {
               var DateValue = DateField.value;
               
               if (checkDateChars(DateValue))
               {
/*               
                    DateValue = extractNumsFromString(DateValue);
                    if (DateValue != "")
                    {
                         // Date should be MMDD and a year of the specified length
                         if (DateValue.length == (4 + yearLengthAsInt))
                         {
                              month = DateValue.substr(0,2);
                              day   = DateValue.substr(2,2);
                              year  = DateValue.substr(4,yearLengthAsInt);
*/
                    if (DateValue != "")
                    {
                    
                         var DateArray = DateValue.split(seperator);

                         var month = DateArray[0];
                         var day = DateArray[1];
                         var year = DateArray[2];

                         if ( (month != undefined) && (day != undefined) && (year != undefined) )
                         {
                              if (year.length == yearLengthAsInt &&
                                  month.length == 2 && day.length == 2) {
                                   var passYear = year;
                                   if (yearLengthAsInt == 2)
                                   {
                                        if(year < 50)
                                             passYear = "20" + year;
                                        else
                                             passYear = "19" + year;
                                   }

                                   returnValue = validateDate(month, day, passYear);
                              }
                         }
                    }
               }
          }

          if (returnValue)
          {
               DateValue = month + seperator + day + seperator + year;
               field.value = DateValue;               
          }
          
          return returnValue;
     }
}

function checkDateChars(dateString)
{
     var returnVal = true;
     
     var validSeparators = "/-";
     var allCharsValid = true;
     for (counter = 0; counter < dateString.length; counter++)
     {
          var currentChar = dateString.charAt(counter);
          if (isNaN(currentChar))
               if (validSeparators.indexOf(currentChar) == -1)
                    returnVal = false;
     }
     
     return returnVal;
     
}

function extractNumsFromString(originalString)
{
     var newString = "";
     var currentChar;
     
     for (var i = 0; i < originalString.length; i++)
     {
          currentChar = originalString.charAt(i);
          if (!isNaN(currentChar))
               newString += currentChar;
     }

    
     return newString;
}

function validateDate(month, day, year)
{
     returnVal = false;

     //if(year != 0)
    
     {
               var daysInMonth;
               switch(parseInt(month,10))
               {
                    case 1:
                    case 3:
                    case 5:
                    case 7:
                    case 8:
                    case 10:
                    case 12:
                         daysInMonth = 31;
                         break;
                         
                    // Thirty days hath September, April, June, and November...                         
                    case 4:
                    case 6:
                    case 9:
                    case 11:
                         daysInMonth = 30;
                         break;
                         
                    case 2:
                         var calcYear = parseInt(year,10);
                         var leap = ( !(calcYear % 4) );
                         daysInMonth = 28 + leap;
                         break;
                         
                    default:
                         returnVal = false;
               }
               
               if( (day >= 1) && (day <= daysInMonth) )
                    returnVal = true;
     }
    
     return returnVal;
}


function validateDateFormat(dateValue)
{
    var objRegExp = /^\d{2}\/\d{2}\/\d{4}$/
 
    //check to see if in correct format
    if(!objRegExp.test(dateValue)) {   
        alert("Date must be in mm/dd/yyyy format");
        return false; //doesn't match pattern, bad date
    }
    else {
        return true;
    }
}

function compareDates(fromDate, toDate, lengthOfYearField)
{
var flag = 'true'
     var checkstr = "0123456789";
     var toDateField;

     var fromDateField;
     if(fromDate.length)
          fromDateField = fromDate[0];
     else
          fromDateField = fromDate;

     var toDateField;
     if(toDate.length)
          toDateField = toDate[0];
     else
          toDateField = toDate;

     //var fromDateField = fromDate;
     //var toDateField = toDate;
     var fromDateValue = "";
     var toDateValue = "";
     var fromDateTemp = "";
     var toDateTemp = "";
     var seperator = "/";
     var fromDay;
     var toDay;
     var fromMonth;
     var toMonth;
     var fromYear;
     var toYear;
     var i;

     var undefined;
     if((fromDate != undefined) && (toDate != undefined)) {

          check_date(fromDate,lengthOfYearField);
          fromDateValue = fromDateField.value;

          if (isEmpty(toDate)){
               // make the dates equal
               toDateValue = fromDateField.value;
          } else {
               check_date(toDate,lengthOfYearField);
               toDateValue = toDateField.value;
          }

          /* Date 1*/
          /* Delete all chars except 0..9 */
          for (var i = 0; i < fromDateValue.length; i++) {
               if (checkstr.indexOf(fromDateValue.substr(i,1)) >= 0) {
               fromDateTemp = fromDateTemp + fromDateValue.substr(i,1);
          }
     }

     fromDateValue = fromDateTemp;

     /* year */
     fromYear = fromDateValue.substr(4,4);
     /* month*/
     fromMonth = fromDateValue.substr(0,2);
     /*  day*/
     fromDay = fromDateValue.substr(2,2);

     /* Date 2 */
     /* Delete all chars except 0..9 */
     for (var i = 0; i < toDateValue.length; i++) {
          if (checkstr.indexOf(toDateValue.substr(i,1)) >= 0) {
               toDateTemp = toDateTemp + toDateValue.substr(i,1);
          }
     }
     toDateValue = toDateTemp;

     /* year */
     toYear = toDateValue.substr(4,4);
     /* Validation of month*/
     toMonth = toDateValue.substr(0,2);
     /* Validation of day*/
     toDay = toDateValue.substr(2,2);

     /* Compare Dates */
     if (toYear > fromYear){
       return true;
     } else if ((toYear == fromYear) && (toMonth > fromMonth)){
       flag = 'true';
     } else if ((toYear == fromYear) && (toMonth == fromMonth) && (toDay >= fromDay)){
       flag = 'true';
     } else {
       alert("The Date cannot be before the Issue Date");
       flag = 'false';
     }
   
  }
 return flag;
}

function doDateErrorMsg(field, lengthOfYearField)
{
     var dateErrorMsg = "";
     if (lengthOfYearField == "BOTH")
          dateErrorMsg = "Please enter a valid date (mm/dd/yyyy) or (mm/dd/yy)";
     else if (lengthOfYearField == "2")
          dateErrorMsg = "Please enter a valid date (mm/dd/yy)";
     else if (lengthOfYearField == "4")
          dateErrorMsg = "Please enter a valid date (mm/dd/yyyy)";
     else
          dateErrorMsg = "Please enter a valid date";
       
     field.select();
     field.focus();
     alert(dateErrorMsg);
}

function checkDaysPast(checkDate, allowFuture, numDays)
{
  var retVal = true;

  var checkDateArray = checkDate.split('/');
  var checkDateMonth = checkDateArray[0];
  var checkDateDay = checkDateArray[1];
  var checkDateYear = checkDateArray[2];
  var checkDateObj = new Date();
  checkDateObj.setFullYear(checkDateYear);
  checkDateObj.setMonth(checkDateMonth-1);
  checkDateObj.setDate(checkDateDay);

  var todayDate = new Date();
  todayDateMonth = todayDate.getMonth();
  todayDateDay = todayDate.getDate();
  todayDateYear = todayDate.getFullYear();

  var tempDate = new Date();
  var tempDateMonth = tempDate.getMonth();
  var tempDateDay = tempDate.getDate();
  var tempDateYear = tempDate.getFullYear();

  for(var i = 0; i < numDays; i++)
  {
      tempDateDay -= 1;
      tempDate.setFullYear(tempDateYear);
      tempDate.setMonth(tempDateMonth);
      tempDate.setDate(tempDateDay);
      tempDateYear = tempDate.getFullYear();
      tempDateMonth = tempDate.getMonth();
      tempDateDay = tempDate.getDate();
  } // end of for loop

  if ( allowFuture == false )
  {
    if(checkDateObj > todayDate) {
      alert("Future dates are not allowed");
      retVal = false;
    }
  }
  
  if(tempDate > checkDateObj) {
    alert("Requested date must be within " + numDays + " days of current date.");
    retVal = false;
  }

  return retVal;
}
