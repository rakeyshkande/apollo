/********************************************************
Common Exit function for the common header
********************************************************/

function exit()
{

	var action = document.getElementById("gcc_action").value;

	if (action == 'display_add' || action == 'display_edit')
	{
	  exitrecipientpage();
	}

	if (action == 'display_maintain')
	{
			if (testValueChange() == true)
			{
        var modalUrl = "confirmstatus.html";
        var modalArguments = new Object();
        modalArguments.modalText = 'Your request has not been saved. Are you sure you want to exit the screen?';
        var modalFeatures  =  'dialogWidth:250px; dialogHeight:175px; center:yes; status=no; help=no;';
        modalFeatures +=  'resizable:no; scroll:no';
        var ret = window.showModalDialog(modalUrl, modalArguments, modalFeatures);
         if(ret)
            {
						showWaitMessage("content", "wait", "In Progress");
						document.getElementById("gcc_action").value = 'display_search'
						document.forms[0].action = "SearchGiftCertAction.do?gcc_user=" + document.forms[0].gcc_user.value + "&securitytoken=" + document.forms[0].securitytoken.value + "&context=" + document.forms[0].context.value;
						document.forms[0].submit();
						}
            else
            {
            return false;
            }
			}
			else
			{
			showWaitMessage("content", "wait", "In Progress");
			document.getElementById("gcc_action").value = 'display_search'
			document.forms[0].action = "SearchGiftCertAction.do?gcc_user=" + document.forms[0].gcc_user.value + "&securitytoken=" + document.forms[0].securitytoken.value + "&context=" + document.forms[0].context.value;
			document.forms[0].submit();
			}
	}

 	if (action == 'display_create')
	{

		if (testValueChange() == true)
		{
				var modalUrl = "confirmstatus.html";
        var modalArguments = new Object();
        modalArguments.modalText = 'Your Request has not been created. Are you sure you want to exit the screen?';
        var modalFeatures  =  'dialogWidth:250px; dialogHeight:175px; center:yes; status=no; help=no;';
        modalFeatures +=  'resizable:no; scroll:no';
        var ret = window.showModalDialog(modalUrl, modalArguments, modalFeatures);
         if(ret)
            {
						showWaitMessage("content", "wait", "In Progress");
						document.forms[0].action = "MainMenuAction.do?gcc_user=" + document.forms[0].gcc_user.value + "&securitytoken=" + document.forms[0].securitytoken.value + "&context=" + document.forms[0].context.value;
						document.forms[0].submit();
						}
            else
            {
            return false;
            }
		}
		else
		{
		showWaitMessage("content", "wait", "In Progress");
		document.forms[0].action = "MainMenuAction.do?gcc_user=" + document.forms[0].gcc_user.value + "&securitytoken=" + document.forms[0].securitytoken.value + "&context=" + document.forms[0].context.value;
		document.forms[0].submit();
		}
	}
	if (action == 'display_search')
			{
				showWaitMessage("content", "wait", "In Progress");
				document.forms[0].action = "MainMenuAction.do?gcc_user=" + document.forms[0].gcc_user.value + "&securitytoken=" + document.forms[0].securitytoken.value + "&context=" + document.forms[0].context.value;
				document.forms[0].submit();
			}
}

/*********************************************
To show a 'In process' message to the user
*********************************************/

function showWaitMessage(content, wait, message)
{
   var content = document.getElementById(content);
   var height = content.offsetHeight;
   var waitDiv = document.getElementById(wait + "Div").style;
   var waitMessage = document.getElementById(wait + "Message");
   _waitTD = document.getElementById(wait + "TD");

   content.style.display = "none";
   waitDiv.display = "block";
   waitDiv.height = height;
   waitMessage.innerHTML = (message) ? message : "Processing";
   clearWaitMessage();
   updateWaitMessage();
}

var _waitTD = "";
function clearWaitMessage()
{
   _waitTD.innerHTML = "";
   setTimeout("clearWaitMessage()", 5000);
}

function updateWaitMessage()
{
   setTimeout("updateWaitMessage()", 1000);
   setTimeout("showPeriod()", 1000);
}

function showPeriod()
{
   _waitTD.innerHTML += "&nbsp;.";
}


/**********************************************
 The common functions
**********************************************/
function doCreateCertificateCouponAction()
{
showWaitMessage("content", "wait", "In Progress");
document.forms[0].action = "CreateGiftCertAction.do";
document.forms[0].submit();
}

function doSingleCertificateCouponAction()
{
showWaitMessage("content", "wait", "In Progress");
document.forms[0].action = "SingleGiftCertAction.do";
document.forms[0].submit();
}

function doMultipleCertificateCouponAction()
{
showWaitMessage("content", "wait", "In Progress");
document.forms[0].action = "MultipleGiftCertAction.do";
document.forms[0].submit();
}


function doGccRecipientAction()
{

showWaitMessage("content", "wait", "In Progress");
document.forms[0].action = "RecipientAction.do";
document.forms[0].submit();

}

/**************************************
Highlighting the field
*******************************************/
function fieldFocus()
{
    var element = event.srcElement;
    element.style.borderWidth = 2;
    element.style.borderColor = 'red';
}

/**************************************
to check if the number entered is integer
*************************************/

function isInteger(s)
{

	for (var i = 0; i < s.length; i++)
	{
		var c = s.charAt(i);
		if (!((c >= "0") && (c <= "9")))
		{
			return false;
		}
	}
	return true;
}

function validateSrcCodeAlphaNumeric() {
	//alert("starting");
	var sourceCode = document.getElementById("gcc_source_code").value;
	//alert(sourceCode);
	var flag = 'true';
	if(sourceCode != null) {
	
		var reg = /[0-9a-zA-Z, \r\n/]/;
		var arry = new Array(sourceCode.length);
		for(var i=0; i< sourceCode.length; i++) {
			arry[i] = sourceCode.charAt(i);
			//alert(arry[i]);
	       if(!(arry[i].search(reg) != -1)) {
	    	  // alert("special special");
	    	   document.getElementById("span_source_code_format_error").style.display = "block";
	    		document.getElementById("gcc_source_code").style.backgroundColor ='pink';
	    		flag = 'false';
	    		return flag;
	        }
	       document.getElementById("span_source_code_format_error").style.display = "none";
   		   document.getElementById("gcc_source_code").style.backgroundColor ='#FFFFFF';
	     }
	}
     return flag;
}


function popSourceCodes() {
	try {		
		var modal_dim = "dialogWidth:500px; dialogHeight:350px; center:yes; status:0; help:no; scroll:no";
		var srcCodeObj = new Object();
		
		srcCodeObj.srcCodeStr = document.form.gcc_source_code.value;		
		var results=showModalDialog('giftCertificateSourceCodes.html', srcCodeObj, modal_dim);
	    if (results && results != null) {
	    	//initialising some variable in results - here gc_source_code
	    	document.form.gcc_source_code.value = results.gc_source_code;
	    }		   
	} catch(e) {
		//alert(e.message);
	}
}
	
function popupInvalidSourceCodes() {
	try {
		
		var srcCode = '<xsl:value-of select="ROOT/@src_code_exists_err_msg"></xsl:value-of>';
		var modal_dim = "dialogWidth:500px; dialogHeight:350px; center:yes; status:0; help:no; scroll:no";
		var srcCodeObj = new Object();
		
		srcCodeObj.srcCodeStr = srcCode;
		var results=showModalDialog('giftCertificateSourceCodesErrorPopUp.html', srcCodeObj, modal_dim);
	    if (results && results != null) {
	    	//initialising some variable in results - here gc_source_code	    	
	    }		
	} catch(e) {
		//alert(e.message);
	}
}
