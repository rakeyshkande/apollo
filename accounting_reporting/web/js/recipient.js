/**********************************************
 The common functions
**********************************************/
function doGccRecipientAction()

{
showWaitMessage("content", "wait", "In Progress");
document.forms[0].action = "RecipientAction.do";
document.forms[0].submit();
}

/*******************************************************************
Showing Modal Pop Up on Exit if users have changed any values on the form
*******************************************************************/

function exitrecipientpage()
{
	var action = document.getElementById("gcc_action").value;

	if (testValueChange() == true)
	 	{
        var modalUrl = "confirm.html";
  			var modalArguments = new Object();
  			modalArguments.modalText = 'Do you want to save the data you have entered?';
  			var modalFeatures  =  'dialogWidth:250px; dialogHeight:200px; center:yes; status=no; help=no;';
  			modalFeatures +=  'resizable:no; scroll:no';
  			var ret = window.showModalDialog(modalUrl, modalArguments, modalFeatures);


								if (ret == 'Yes')
								{
									 saverecipientpage();
								}
								else if (ret == 'No')
								{
									 exitrecipient();
								}
								else if (ret== 'Cancel')
								{
								return false;
								}

			}
			else
			{
			exitrecipient()
			}
}

/*************************************
Exit Function
*************************************/

function exitrecipient()
{
var action = document.getElementById("gcc_action").value;
var multiple = document.getElementById("is_multiple_gcc").value;

   if (action == 'display_add')
   {
          document.getElementById("gcc_action").value = 'exit_add';
    }

   else if (action == 'display_edit' && multiple == 'Y')
   {
          document.getElementById("gcc_action").value = 'exit_edit';
          document.getElementById("is_multiple_gcc").value = 'Y';
   }

   else if (action == 'display_edit' && multiple != 'Y')
   {
          document.getElementById("gcc_action").value = 'exit_edit';
          document.getElementById("is_multiple_gcc").value = 'N';
   }

   showWaitMessage("content", "wait", "In Progress");
   doGccRecipientAction();
}

/*************************************
Save Function
*************************************/

function saverecipientpage()

{
       resetFields();
       var flag1 = 'true';
       var flag = 'true';

      flag = checkForEmptyFields();
       if (flag == 'false' && flag1 == 'true')
        flag1 = 'false';

      flag = checkEmailAddress()
       if (flag == 'false' && flag1 == 'true')
       flag1 = 'false';

   	if (flag1 == 'true')
    {
      var action = document.getElementById("gcc_action").value;

        if (action =='display_add')
        {
        document.getElementById("gcc_action").value = 'add';
        document.getElementById("coupon_quantity").value = document.getElementById("gcc_quantity").value;
        doGccRecipientAction();
        }

        else if (action =='display_edit')

        {
        document.getElementById("gcc_action").value = 'edit';
        document.getElementById("coupon_quantity").value = document.getElementById("gcc_quantity").value;
        doGccRecipientAction();
        }
    }
    else
    {
    return false;
    }
}
/**********************************************
when user presses the enter key without focus on any button
***********************************************/

function enterkey()

{
    if (window.event.keyCode == 13)
    return false;
}

/*******************************************************
checkEmailAddress()
********************************************************/

     function checkEmailAddress()
     {
     var flag='true';
     var num= document.getElementById('number').value;
            for (var x = 1 ; x <= num ; x++)

           {
                if(isEmail(document.getElementById('email_' + x).value,true))
                {
                       document.getElementById("email_"+x).style.backgroundColor='#FFFFFF';
                       document.getElementById("error_email_" + x).style.display = "none";
                 }

                else{
                       document.getElementById("email_"+x).style.backgroundColor='pink';
                       document.getElementById("error_email_"+x).style.display = "block";
                       flag = 'false'
                }
           }
           return flag;
     }

/*************************************************************************************
Iterate through fields in each row to ensure that no row is completely empty
**************************************************************************************/

 function checkForEmptyFields()

 {
   var flag='true';
   var num= document.getElementById('number').value

   for (var x = 1 ; x <= num ; x++)
  {
      if(   			  document.getElementById('first_name_'+x).value != ""         				 ||
                    document.getElementById('last_name_'+x).value != ""                  ||
                    document.getElementById('address_1_'+x).value != ""                  ||
                    document.getElementById('address_2_'+x).value != ""                  ||
                    document.getElementById('city_'+x).value != ""                       ||
                    document.getElementById('state_'+x).value != "none"                  ||
                    document.getElementById('country_'+x).value != "none"         			 ||
                    document.getElementById('zip_'+x).value != ""                        ||
                    document.getElementById('phone_'+x).value != ""                      ||
                    document.getElementById('email_'+x).value != "")

      {
             document.getElementById('required_error_'+x).style.display = "none";
      }

      else

      {
             document.getElementById('required_error_'+x).style.display = "block";
             flag = 'false'
      }
   }
   return flag;
 }

/**************************************************
Reset error fields
***************************************************/

function resetFields()
{
  var num= document.getElementById('number').value
    for (var x = 1 ; x <= num ; x++)
          {
          document.getElementById('required_error_'+x).style.display = "none";
          document.getElementById("error_email_" + x).style.display = "none";
          }
}
