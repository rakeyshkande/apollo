/*********************************************
default colour for all the fields on the page
**********************************************/
function defaultcolour()
{
      var NumElements=document.form.elements.length;
      for (i=0; i<NumElements;i++)
			{
					if (document.form.elements[i].type=="text" || document.form.elements[i].type=="select-one" || document.form.elements[i].type=="textarea")
					{
						document.form.elements[i].style.backgroundColor='#FFFFFF';
					}
			}
}
/************************************************
greying out the disabled fields on the page
*************************************************/
var disfields = new Array("gcc_quantity","gcc_format","gcc_filename","gcc_expiration_date","gcc_requestor_name","gcc_promotion_id","gcc_program_description","gcc_program_name","gcc_request_number","gcc_type","gcc_redemption_type","gcc_paid_amount");
function disableFields()
{
		for(var i = 0; i < disfields.length; i++)
		{
				var disableField = disfields[i];

				if (document.getElementById(disableField).disabled == true)
				{
				document.getElementById(disableField).style.backgroundColor='#eeeeee';
				}
				else
				{
				document.getElementById(disableField).style.backgroundColor='';
				}
		}
}


/************************************************
Create Function..clicking on the create button
*************************************************/
function createGC()
{
resetErrorFields();
defaultcolour();
disableFields();

  	var flag1 = 'true';
		var flag = 'true'

		flag = checknumericFields();
		if (flag == 'false' && flag1 == 'true')
			flag1 = 'false';

		flag = checkAlpha();
		if (flag == 'false' && flag1 == 'true')
			flag1 = 'false';

		flag = checkDD();
		if (flag == 'false' && flag1 == 'true')
			flag1 = 'false';

		flag = checkissuedate();
		if (flag == 'false' && flag1 == 'true')
			flag1 = 'false';

		flag = checkexpirationdate();
		if (flag == 'false' && flag1 == 'true')
			flag1 = 'false';

		flag = checkIssueAmount();
		if (flag == 'false' && flag1 == 'true')
			flag1 = 'false';

		flag = checkPaidAmount();
		if (flag == 'false' && flag1 == 'true')
			flag1 = 'false';

		flag = checkcsAlpha();
		if (flag == 'false' && flag1 == 'true')
			flag1 = 'false';

		flag = checkRequestNumber();
		if (flag == 'false' && flag1 == 'true')
			flag1 = 'false';

		flag = checkPromotionId();
		if (flag == 'false' && flag1 == 'true')
			flag1 = 'false';
		
		flag = validateSrcCodeAlphaNumeric();
		
		if (flag == 'false' && flag1 == 'true') 		
			flag1 = 'false';			
		
		
		if (flag1 == 'true')
		{
			document.getElementById("gcc_action").value = 'create';
			document.getElementById("gcc_request_number").disabled = false;
			document.getElementById("gcc_format").disabled = false;
      document.getElementById("gcc_type").disabled = false;

			doCreateCertificateCouponAction();
		}

}

/************************************************
Add Recipient Button
*************************************************/
function addRecipient()
{
document.getElementById("gcc_action").value = 'display_add';
document.getElementById("gcc_request_number").disabled = false;
document.getElementById("gcc_format").disabled = false;
document.getElementById("gcc_type").disabled = false;
doGccRecipientAction();
}

/***********************************************
Disabling & Enabling of the add recipient button
************************************************/

function addRecipientButton()
{
var quantity = document.getElementById("gcc_quantity").value;

		if (!isInteger(quantity) || quantity == '0' || quantity == '')
		{
		document.getElementById("gcc_submit_display_recipient").disabled = true;
		}
		else
		{
		document.getElementById("gcc_submit_display_recipient").disabled = false;
		}
}

/**********************************************
clear the form fields..the clearfields button
************************************************/

function doClearFields()
{
     			document.getElementById("gcc_type").disabled = false;
      		document.getElementById("gcc_action").value = 'display_create';
      		doCreateCertificateCouponAction();
}

/**********************************************
when user presses the enter key without focus on any button
***********************************************/

function enterkey()
{
 		if(window.event.keyCode == 13)
        return false
}

/*******************************************
gets the current date
********************************************/
 function getCurrentDate()
 {
       var d = new Date()
       var zero = '0';
       var month = d.getMonth() + 1;
       var date = d.getDate()
       if (month < 10)
       {
        month = zero + month;
       }
       if (date < 10)
       {
        date = zero + date;
       }
       var returndate = month+"/"+date+"/"+d.getFullYear();
       return returndate;
  }

/*******************************************
Set ExpireDate to one year from the issue date
********************************************/
        function setExpireDate()
        {
        var type = document.getElementById('gcc_type').value;

						if ( type != 'GC')
						{
                var issueDate = document.forms[0].gcc_issue_date.value;
                var expDate = new Date(document.forms[0].gcc_issue_date.value);

                var zero = '0';
                var month = d.getMonth() + 1;
    						var date = d.getDate()

                var offset = 365 * 24 * 60 * 60 * 1000;
                var expDay = new Date(expDate.getTime() + offset);
                var zero = '0';
                var month = expDay.getMonth() + 1;
    var date = expDay.getDate();
    var year = expDay.getFullYear();
    if (month < 10)
                       {
                        month = zero + month;
                       }
                       if (date < 10)
                       {
                        date = zero + date;
       									}
          var dateText = month + "/" + date + "/" + year;
                document.forms[0].gcc_expiration_date.value = dateText;

                                if (issueDate == '')
                                document.forms[0].gcc_expiration_date.value = '';

                                if(!ValidateDate('gcc_issue_date'))
                                document.forms[0].gcc_expiration_date.value = '';

        		}
        		else
        		{
        		document.forms[0].gcc_expiration_date.value = "";
        		}
			}


/*****************************************************************************************************************
The functions below are to check each an every form field and validate it according to its type(numeric/alpha etc)
*****************************************************************************************************************/
/********************************************
to check numeric fields
*******************************************/


var numericFields = new Array('gcc_quantity');
var errorspansnum1 = new Array('span_quantity');
var errorspansnum2 = new Array('span_quantity_error');


function checknumericFields()
{
	var aLength = numericFields.length;
	var flag = 'true';

	for (var i = 0; i < aLength; i++)
	{
		var fieldType = numericFields[i];

		if (document.getElementById(fieldType).value != null)
				{
					var entry = document.getElementById(fieldType).value;
					var reqerror1 = errorspansnum1[i];
					var reqerror2 = errorspansnum2[i];



								if (entry == null || entry == "" )
								{
								document.getElementById(reqerror1).style.display = "block";
								document.getElementById(fieldType).style.backgroundColor='pink';
								flag = 'false';
								}

								else if (!isPositiveInteger(entry))
								{
								document.getElementById(reqerror2).style.display = "block";
								document.getElementById(fieldType).style.backgroundColor='pink';
								flag = 'false';
								}

								else
								{
								document.getElementById(reqerror1).style.display = "none";
								document.getElementById(reqerror2).style.display = "none";
								document.getElementById(fieldType).style.backgroundColor='#FFFFFF';
								}
				}
		}
	return flag;
}

/*************************************
to check and  validate issue date
**************************************/

function checkissuedate()
{
var flag = 'true';
var d = new Date();
var currentDate = (d.getMonth()+1) + "/" + d.getDate() + "/" + d.getFullYear();
var today = new Date()
var yesterday = new Date()
yesterday.setDate(today.getDate()-1);

					var entry = document.getElementById("gcc_issue_date").value;
					var ent = new Date(entry)
					var x = ent.getTime()

		fieldlength=entry.length;

		if (fieldlength < 1)
		{
			document.getElementById("span_issue_date").style.display = "block";
			document.getElementById("gcc_issue_date").style.backgroundColor='pink';
			flag = 'false';
		}

		else if(!ValidateDate("gcc_issue_date"))
		{
			document.getElementById("span_issue_date_error1").style.display = "block";
	    document.getElementById("gcc_issue_date").style.backgroundColor='pink';
			flag = 'false';
		}

		else if (x < yesterday)
		{
			document.getElementById("span_issue_date_error2").style.display = "block";
			document.getElementById("gcc_issue_date").style.backgroundColor='pink';
			flag = 'false';
		}

		else
		{
			document.getElementById("gcc_issue_date").style.backgroundColor='#FFFFFF';
			document.getElementById("span_issue_date").style.display = "none";
			document.getElementById("span_issue_date_error1").style.display = "none";
			document.getElementById("span_issue_date_error2").style.display = "none";
		}
	return flag;
}

/*************************************
to check and  validate expiration date
**************************************/

function checkexpirationdate()
{
var type = document.getElementById("gcc_type").value;

if (type != 'GC')
{
	var flag = 'true';
	var d = new Date();
	var currentDate = (d.getMonth()+1) + "/" + d.getDate() + "/" + d.getFullYear();
	var today = new Date()
	var yesterday = new Date()
	yesterday.setDate(today.getDate()-1);


					var entry = document.getElementById("gcc_expiration_date").value;
					var ent = new Date(entry)
					var x = ent.getTime()

		fieldlength=entry.length;

		if (fieldlength < 1)
		{
			document.getElementById("span_expiration_date").style.display = "block";
			document.getElementById("gcc_expiration_date").style.backgroundColor='pink';
			flag = 'false';
		}
		else if(!ValidateDate('gcc_expiration_date'))
		{
			document.getElementById("span_expiration_date_error1").style.display = "block";
	    document.getElementById("gcc_expiration_date").style.backgroundColor='pink';
			flag = 'false';
		}
		else if (x < yesterday)
		{
			document.getElementById("span_expiration_date_error2").style.display = "block";
			document.getElementById("gcc_expiration_date").style.backgroundColor='pink';
			flag = 'false';
		}
    else if(compareDates(form.gcc_issue_date, form.gcc_expiration_date, 4) == 'false')
    {
			document.getElementById("span_expiration_date_error1").style.display = "block";
	    document.getElementById("gcc_expiration_date").style.backgroundColor='pink';
			flag = 'false';
		}
		else
		{
			document.getElementById("gcc_expiration_date").style.backgroundColor='#FFFFFF';
			document.getElementById("span_expiration_date").style.display = "none";
			document.getElementById("span_expiration_date_error1").style.display = "none";
			document.getElementById("span_expiration_date_error2").style.display = "none";
		}
	}

return flag;
}

/************************************
to check the alpha fields
**************************************/

var alphaFields = new Array('gcc_requestor_name');
var errorspansalpha1 = new Array('span_requestor_name');


function checkAlpha()
{
var type = document.getElementById("gcc_type").value;

			var cLength = alphaFields.length;
			var flag = 'true';

				for (var i = 0; i < cLength; i++)
				{
					var alphaType = alphaFields[i];

					if (document.getElementById(alphaType).value != null)
							{
							var entry = document.getElementById(alphaType).value;
							var alphaerror1 = errorspansalpha1[i];
							fieldlength=entry.length;
										if (fieldlength < 1)
										{
												document.getElementById(alphaerror1).style.display = "block";
												document.getElementById(alphaType).style.backgroundColor ='pink';
												flag = 'false';
										}

								}
				}

			return flag;
}
/****************************************************************
to check the promotion id field when the redemption type is "I"
******************************************************************/

function checkPromotionId()
{
 var flag = 'true';

			if (document.getElementById('gcc_promotion_id')!=null)
			{
			var entry = document.getElementById('gcc_promotion_id').value;
			}


 var redemtype = document.getElementById("gcc_redemption_type").value;

 if (redemtype == 'I')
	{
	fieldlength=entry.length;

			if (fieldlength < 1)
			{
        document.getElementById('span_promotion_id').style.display = "block";
        document.getElementById('gcc_promotion_id').style.backgroundColor='pink';
        flag = 'false';
			} 
      else if (entry.indexOf(' ') != -1)
			{
        document.getElementById('span_promotion_id_with_space').style.display = "block";
        document.getElementById('gcc_promotion_id').style.backgroundColor='pink';
        flag = 'false';
			}
      
	}
  

	return flag;
}


/***************************************************
to check the remaining fields if type is not 'CS'
*****************************************************/

var csalphaFields = new Array("gcc_program_description","gcc_program_name");
var cserrorspansalpha1 = new Array("span_program_description","span_program_name");


function checkcsAlpha()
{
var type = document.getElementById("gcc_type").value;

	if (type != 'CS')
	{
			var cLength = csalphaFields.length;
			var flag = 'true';

				for (var i = 0; i < cLength; i++)
				{
					var csalphaType = csalphaFields[i];

					if (document.getElementById(csalphaType).value != null)
							{
							var entry = document.getElementById(csalphaType).value;
							var csalphaerror1 = cserrorspansalpha1[i];
							fieldlength=entry.length;
										if (fieldlength < 1)
										{
												document.getElementById(csalphaerror1).style.display = "block";
												document.getElementById(csalphaType).style.backgroundColor ='pink';
												flag = 'false';
										}
								}
				}
		}

			return flag;
}

/***************************************************
to check the request number field
*****************************************************/

function checkRequestNumber()
{
var flag = 'true';

			if (document.getElementById('gcc_request_number')!=null)
			{
			var entry = document.getElementById('gcc_request_number').value;
			}

var user = document.getElementById("gcc_user").value;
var type = document.getElementById("gcc_type").value;
if (user == 'user_op')
	{
	fieldlength=entry.length;

			if (fieldlength < 1)
			{
			document.getElementById('span_request_number').style.display = "block";
			document.getElementById('gcc_request_number').style.backgroundColor='pink';
			flag = 'false';
			}
	}

if (user == 'user_cs' && type != 'CS')
	{
	fieldlength=entry.length;

			if (fieldlength < 1)
			{
			document.getElementById('span_request_number').style.display = "block";
			document.getElementById('gcc_request_number').style.backgroundColor='pink';
			flag = 'false';
			}
	}
			return flag;
}

/*************************************
to check the dropdown fields
**************************************/

var DDFields = new Array("gcc_type","gcc_redemption_type");
var errorspansDD = new Array("span_coupon_type","span_redemption_type");


function checkDD()
{
var dLength = DDFields.length;
var flag = 'true';

	for (var i = 0; i < dLength; i++)
	{
		var DDType = DDFields[i];

			if (document.getElementById(DDType).value != null)
			{
			var entry = document.getElementById(DDType).value;
			var DDerror = errorspansDD[i];
			}


		if (entry == 'none' || entry == '')
			{
			document.getElementById(DDerror).style.display = "block";
			document.getElementById(DDType).style.backgroundColor='pink'
			flag = 'false';
			}
			else
			{
			document.getElementById(DDerror).style.display = "none";
			document.getElementById(DDType).style.backgroundColor='#FFFFFF'
			}

	}
	return flag;
}

/***********************************************
To check the entry for the issueamount.
if value is anything but ".00" it will reject it
*************************************************/
function checkIssueAmount()
{

var flag = 'true';

			if (document.getElementById('gcc_issue_amount')!=null)
			{
			var entry = document.getElementById('gcc_issue_amount').value;
			}

		fieldlength=entry.length;

			if (fieldlength < 1)
			{
			document.getElementById('span_issue_amount').style.display = "block";
			document.getElementById('gcc_issue_amount').style.backgroundColor='pink';
			flag = 'false';
			}

			validChar='0123456789. ';

			for (var i=0; i < fieldlength; i++ )
				{
						if (validChar.indexOf(entry.charAt(i)) < 0)

						{
						document.getElementById('span_issue_amount_error').style.display = "block";
						document.getElementById('gcc_issue_amount').style.backgroundColor='pink';
						flag = 'false';
						}
				}


 			var payment = entry.split('.');
			 for (var j = 1; j<=payment.length ; j++)
			 {
			    if(payment[j] == null || payment[j] == 0 || payment[j] == '' )
			    {
			      document.getElementById('span_issue_amount_error1').style.display = "none";
			   		flag = 'true';
			    }
			   	else
			   	{
			      document.getElementById('span_issue_amount_error').style.display = "block";
						document.getElementById('gcc_issue_amount').style.backgroundColor='pink';
						flag = 'false';
			   		break;
			 		}
				}

    return flag;
}

/***********************************************
To check the entry for the paidamount.
*************************************************/

function checkPaidAmount()
{

var flag = 'true';

	if (document.getElementById('gcc_paid_amount')!=null)
			{
			var entry = document.getElementById('gcc_paid_amount').value;
			}

		fieldlength=entry.length;

				if (fieldlength < 1)
				{
				document.getElementById('span_paid_amount').style.display = "block";
				document.getElementById('gcc_paid_amount').style.backgroundColor='pink';
				flag = 'false';
				}
				else
				{
				document.getElementById('span_paid_amount').style.display = "none";
				document.getElementById('gcc_paid_amount').style.backgroundColor='#FFFFFF';
				}

			var payment = entry.split('.');

		 	if(!isInteger(payment[0]) || (payment.length > 1 && (!isInteger(payment[1]) || (payment[1].length > 2))))
		 	{
		 	document.getElementById('span_paid_amount_error').style.display = "block";
			document.getElementById('gcc_paid_amount').style.backgroundColor='pink';
			flag = 'false';
			}

			if (payment.length > 2)
			{
					document.getElementById('span_paid_amount_error').style.display = "block";
					document.getElementById('gcc_paid_amount').style.backgroundColor='pink';
					flag = 'false';
			}


    return flag;
}

/**********************************
Reset the error fields
************************************/
var spans = new Array("span_quantity","span_quantity_error","span_source_code_format_error",
"span_issue_date","span_expiration_date","span_issue_date_error1","span_expiration_date_error1",
"span_issue_date_error2","span_expiration_date_error2","span_request_number",
"span_program_description","span_program_name","span_requestor_name","span_request_number_error",
"span_program_description_error","span_program_name_error","span_requestor_name_error",
"span_coupon_type","span_format","span_redemption_type","span_issue_amount","span_paid_amount",
"span_issue_amount_error","span_paid_amount_error","span_issue_amount_error1","span_paid_amount_error1",
"request_server_error","issue_server_error","sourcecode_server_error","span_promotion_id","span_promotion_id_with_space");

function resetErrorFields()
{
		for(var i = 0; i < spans.length; i++)
		{
				var spanField = spans[i];
				if (document.getElementById(spanField)!=null)
				{
				document.getElementById(spanField).style.display = "none";
				}
		}
}


/******************************************
to populate the value selected for gcc_type
*******************************************/

function onGccTypeChange()

{
resetErrorFields();
defaultcolour();
disableFields();

var type = document.getElementById('gcc_type').value;
var paidAmount = document.getElementById('gcc_paid_amount').value;
var prefix = document.getElementById('gcc_prefix').value;
document.getElementById('gcc_prefix').value = type;
var user = document.getElementById("gcc_user").value;

                 var expdate = document.getElementById('gcc_expiration_date').value
                		if (expdate == '' && type != 'GC')
                		{
                		setExpireDate();
                		}


			if (type == 'CS')
			{
					if (user == 'user_cs')
					{
					document.getElementById('gcc_request_number').disabled = true;
					document.getElementById('gcc_paid_amount').value = 0;
					document.getElementById('gcc_paid_amount').disabled = true;

					}
					if (user == 'user_op')
					{
					document.getElementById('gcc_paid_amount').value = 0;
          document.getElementById('gcc_format').disabled = true;
          document.getElementById('gcc_paid_amount').disabled = true;
					}
                				var crebut = document.getElementById("gcc_enable_create_cs").value;
                				if (crebut == 'Not Added')
                					{
                					document.getElementById('gcc_submit_create_save').disabled = true
                					}
               						else if (crebut == 'Added')
                					{
                 					document.getElementById('gcc_submit_create_save').disabled = false
                					}

			}
			else
			{
			document.getElementById('gcc_request_number').disabled = false;
			document.getElementById('gcc_paid_amount').disabled = false;
			document.getElementById('gcc_program_name').disabled = false;
			document.getElementById('gcc_program_description').disabled = false;
			document.getElementById('gcc_submit_create_save').disabled = false;
      document.getElementById('gcc_format').disabled = false;
     	}

			if (type == 'CP')
			{
				document.getElementById('gcc_paid_amount').value = 0;
				document.getElementById('gcc_paid_amount').disabled = true;
			}

			if (type == 'GC')
			{
      document.getElementById('gcc_expiration_date').value = '';
			document.getElementById('gcc_expiration_date').disabled = true;
			document.getElementById('exp_date_img').style.display = "none";
			}
			else
			{
			document.getElementById('gcc_expiration_date').disabled = false;
			document.getElementById('exp_date_img').style.display = "block";
			}

			if (prefix == '')
			{
			document.getElementById('gcc_prefix').value = type;
			}

			if (type == 'none')
			{
			document.getElementById('gcc_prefix').value = '';
			}
}


/*****************************************
set default functions
********************************************/

function displayDefault()
{
var issueDate = document.getElementById("gcc_issue_date").value;
var type = document.getElementById("gcc_type").value;
var action = document.getElementById("gcc_action").value;
var multiple = document.getElementById("is_multiple_gcc").value;
var user = document.getElementById("gcc_user").value;
var d = new Date()
var currentDate = getCurrentDate();
var quantity = document.getElementById("gcc_quantity").value;
var reqnumber = document.getElementById("saved_request_number").value
var successmsg = document.getElementById("success_msg").value;
var srcCodeExistsErrMsg = document.getElementById("src_code_exists_err_msg").value;
var srcCodeExpiredErrMsg = document.getElementById("src_code_expired_err_msg").value;


var gccformat =         document.getElementById("gcc_format").value;
var redemptiontype = document.getElementById("gcc_redemption_type").value;

if (formatvalue != 'P' && formatvalue != '')
                   {
                    document.getElementById("gcc_format").value = formatvalue;
                   }
                   if (redemtype != 'P' && redemtype != '')
                   {
                    document.getElementById("gcc_redemption_type").value = redemtype;
                   }

 	if (action == 'display_create')
 	{
				document.getElementById('gcc_request_number').value = reqNum;
        		document.getElementById('gcc_type').value = typ;
                addRecipientButton();

                if (issueDate == '')
                {
                 document.forms[0].gcc_issue_date.value = currentDate;
                }
								if (document.getElementById('gcc_expiration_date')!= null)
                {
                 var expdate = document.getElementById('gcc_expiration_date').value
                		if (expdate == '' && type != 'GC')
                		{
                		setExpireDate();
                		}

                }

           if (user == 'user_cs')
       		{
                   document.getElementById('gcc_format').disabled = true;
                   document.getElementById('gcc_type').value = 'CS';
                   document.getElementById('gcc_type').disabled = true;
                   document.getElementById('gcc_request_number').disabled = true;
                   document.getElementById('gcc_paid_amount').value = '0';
                   document.getElementById('gcc_paid_amount').disabled = true;
          }
          else
          {
           document.getElementById('gcc_request_number').disabled = false;
           document.getElementById('gcc_paid_amount').disabled = false;

          }

                   if (user == 'user_op' && type == 'CS')
                   {
                   document.getElementById('gcc_format').disabled = true;
                   document.getElementById('gcc_paid_amount').value = '0';
                   }
                   else if (user == 'user_op' && type != 'CS')
                   {
                   document.getElementById('gcc_format').disabled = false;
                   }

              if(type == 'CS')
              {
                var crebut = document.getElementById("gcc_enable_create_cs").value;
                if (crebut == 'Not Added')
                {
                document.getElementById('gcc_submit_create_save').disabled = true
                }
                else if (crebut == 'Added')
                {
                 document.getElementById('gcc_submit_create_save').disabled = false
                }
              }

                   if (type == 'GC')
                   {
                   document.getElementById('gcc_expiration_date').disabled = true;
                   document.getElementById('exp_date_img').style.display = "none";
                   }
                   else
                   {
                    document.getElementById('gcc_expiration_date').disabled = false;
                    document.getElementById('exp_date_img').style.display = "block";
                   }

                   if (type == 'CS' || type == 'CP')
                   {
                   document.getElementById('gcc_paid_amount').value = '0';
                   document.getElementById('gcc_paid_amount').disabled = true;
                   }
                   else
                   {
                   document.getElementById('gcc_paid_amount').disabled = false;
                   }
                   if (successmsg != '')
									 		{
									 		document.getElementById('span_success_error').style.display = "block";
									 		document.getElementById('gcc_submit_create_save').disabled = true;
									 		document.getElementById('gcc_submit_display_recipient').disabled = true;
									 		document.getElementById('gcc_reset_button').disabled = false;
									 		}
									 		else if (successmsg == '')
									 		{
									 		document.getElementById('gcc_submit_display_recipient').disabled = true;
									 		document.getElementById('gcc_reset_button').disabled = true;
											}
                   if(srcCodeExistsErrMsg != '' || srcCodeExistsErrMsg !=''){
                	   document.getElementById('span_src_code_validate_message').style.display = "block";
                   }
		}


                if (action == 'create')
                {
                        document.getElementById('gcc_submit_create_save').disabled = true;
                        document.getElementById('gcc_submit_display_recipient').disabled = true;
                        document.getElementById('gcc_reset_button').disabled = false;
								}

}


