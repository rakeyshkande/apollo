
/*************************************
Edit Function for Multiple Recipient
*************************************/
function editMultipleRecipient()
{
document.getElementById("gcc_action").value = 'display_edit';
enableAllMultiple();
doGccRecipientAction();
}

/*************************************
Save Function for Multiple
*************************************/
function savemultipleGC()
{
resetErrorFields();

  var flag1 = 'true';
	var flag = 'true'
	flag = checkAlpha();
		if (flag == 'false' && flag1 == 'true')
	flag1 = 'false';

	flag = checkPromotionId();
		if (flag == 'false' && flag1 == 'true')
	flag1 = 'false';
	
	flag = validateSrcCodeAlphaNumeric();
		if (flag == 'false' && flag1 == 'true')
	flag1 = 'false';
		
  if (flag1 == 'true')
	{
  	checkstatus()
	}

}

/**/
function checkstatus()
{
var status = document.getElementById('gcc_status_multiple').value;
if (status != 'none')
  	{
  	confirmStatus()
  	}
  	else
 	 	{
		document.getElementById("gcc_action").value = 'maintain'
		enableAllMultiple();
		doMultipleCertificateCouponAction();
		}
}
/*******************************************
Confirmation window for the status count for multiple
*******************************************/
function confirmStatus()
{
	var url = '';

  //Modal_URL
  var modalUrl = "confirmstatus.html";
  //Modal_Arguments
  var modalArguments = new Object();

	 var cnt = document.getElementById('status_multiple_count').value;

      modalArguments.modalText = 'Change will affect ' + cnt + ' gift certificates/coupons. Are you sure you want to proceed?';

      //Modal_Features
      var modalFeatures  =  'dialogWidth:250px; dialogHeight:175px; center:yes; status=no; help=no;';
      modalFeatures +=  'resizable:no; scroll:no';

      //get a true/false value from the modal dialog.
      var ret = window.showModalDialog(modalUrl, modalArguments, modalFeatures);

      if(ret)
      {
         	document.getElementById("gcc_action").value = 'maintain'
				 	enableAllMultiple();
					doMultipleCertificateCouponAction();
      }
      else
      {
      return false;
      }

}

/**********************************************
when user presses the enter key without focus on any button
***********************************************/

function enterkey()

{
    if (window.event.keyCode == 13)
    return false;
}


/*********************************************************
To enable the fields on multiple coupon Maintainance screen
*********************************************************/

var mfields = new Array("gcc_request_number","gcc_issue_amount","gcc_type","gcc_issue_date","gcc_paid_amount","gcc_expiration_date","gcc_quantity","gcc_format","gcc_filename","gcc_redemption_type","gcc_company","gcc_source_code","gcc_other","gcc_prefix","gcc_partner_program_name");

function enableAllMultiple()
{
	var mfieldlength = mfields.length;

		for (var i = 0; i < mfieldlength; i++)
				{

				var action = document.getElementById("gcc_action").value;
				var multiple = document.getElementById("is_multiple_gcc").value;
				var mfield = mfields[i];

						if(document.getElementById(mfield).type == "select-one")
						document.getElementById(mfield).disabled = false;
						else
						document.getElementById(mfield).disabled = false;

				}
}

/*****************************
check promotion id
*******************************/
function checkPromotionId()
{
 var flag = 'true';

			if (document.getElementById('gcc_promotion_id')!=null)
			{
			var entry = document.getElementById('gcc_promotion_id').value;
			}


var redemtype = document.getElementById("gcc_redemption_type").value;
var redemptypefirst = redemtype.charAt(0);

 if (redemptypefirst == 'I')
	{
	fieldlength=entry.length;

			if (fieldlength < 1)
			{
			document.getElementById('span_promotion_id').style.display = "block";
			document.getElementById('gcc_promotion_id').style.backgroundColor='pink';
			flag = 'false';
			}
      else if (entry.indexOf(' ') != -1)
			{
        document.getElementById('span_promotion_id_with_space').style.display = "block";
        document.getElementById('gcc_promotion_id').style.backgroundColor='pink';
        flag = 'false';
			}
	}

	return flag;
}

/***************************************************
to check the required fields
*****************************************************/

var reqFields = new Array("gcc_requestor_name","gcc_program_description","gcc_program_name");
var errorspansreq = new Array("span_requestor_name","span_program_description","span_program_name");


function checkAlpha()
{
			var rLength = reqFields.length;
			var flag = 'true';

				for (var i = 0; i < rLength; i++)
				{
					var reqType = reqFields[i];

					if (document.getElementById(reqType).value != null)
							{
							var entry = document.getElementById(reqType).value;
							var errorspans = errorspansreq[i];
							fieldlength=entry.length;
										if (fieldlength < 1)
										{
												document.getElementById(errorspans).style.display = "block";
												document.getElementById(reqType).style.backgroundColor ='pink';
												flag = 'false';
										}
								}
				}

			return flag;
}

/**********************************
Reset the error fields
************************************/
var fields = new Array("gcc_requestor_name","gcc_program_description","gcc_program_name","gcc_promotion_id","gcc_promotion_id");
var spans = new Array("span_requestor_name","span_program_description","span_program_name","span_promotion_id","span_promotion_id_with_space");

function resetErrorFields()
{
		for(var i = 0; i < spans.length; i++)
		{
				var Field = fields[i];
				var spanField = spans[i];
				if (document.getElementById(spanField)!=null)
				{
				document.getElementById(spanField).style.display = "none";
				document.getElementById(Field).style.backgroundColor='#FFFFFF';
				}
		}
}



