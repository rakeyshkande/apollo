
/*
Global variables
*/

var cashReceivedTotalAll = 0;
var writeOffTotalAll = 0;
var remainingBalTotalAll = 0;
var cashReceivedTotalPageOrig = 0;
var writeOffTotalPageOrig = 0;
var remainingBalTotalPageOrig = 0;


/*********************************************************************************************************
 *  Initialization
 *********************************************************************************************************/
function init()
{
  document.oncontextmenu = contextMenuHandler;
  document.onkeydown = backKeyHandler;
  initErrorMessages();

	if (origPartnerSelected != null && origPartnerSelected.length > 0 && remitDetailsUpdateCountNumeric > 0)
	{
		var remitDate = document.getElementById('remit_date_text').value;
		if (remitDate == null || remitDate.length == 0)
		{
			var today = new Date();
			var month = today.getMonth() + 1;
			var date = today.getDate();
			var year = today.getFullYear();
			document.getElementById('remit_date_text').value = month + '/' + date + '/' + year;
		}

		saveVariables();
		updateDifference();
	}
	else
	{
		document.getElementById('difference_label').style.color = 'gray';
		document.getElementById('difference_value').style.color = 'gray';

		document.getElementById('remit_date_text').disabled = true;
		document.getElementById('remit_date_label').style.color = 'gray';

		document.getElementById('remit_amount_text').disabled = true;
		document.getElementById('remit_amount_label').style.color = 'gray';

		document.getElementById('remit_number_text').disabled = true;
		document.getElementById('remit_number_label').style.color = 'gray';

	}

}


/**********************************************************************************************************
 * Called when the page loads, this function displays any error messages
 * generated on load or from validation failures.
 **********************************************************************************************************/
function initErrorMessages()
{
	if (okError !=null && okError.length > 0)
	{
		displayOkError(okError);
	}
}


/**********************************************************************************************************
 * Displays the ok error
 **********************************************************************************************************/
function displayOkError(errorMessage)
{
	//Modal_URL
	var modalUrl = "html/alert.html";

	// modal arguments
	var modalArguments = new Object();
	modalArguments.modalText = errorMessage;

	// modal features
	var modalFeatures = 'dialogWidth:300px; dialogHeight:200px; center:yes; status=no; help=no; resizable:no; scroll:yes';

	// show the message
	window.showModalDialog(modalUrl, modalArguments, modalFeatures);

}


/**********************************************************************************************************
 * Displays the confirm error
 **********************************************************************************************************/
function displayConfirmError(errorMessage)
{
	//Modal_URL
	var modalUrl = "html/confirm.html";

	// modal arguments
	var modalArguments = new Object();
	modalArguments.modalText = errorMessage;

	// modal features
	var modalFeatures = 'dialogWidth:300px; dialogHeight:200px; center:yes; status=no; help=no; resizable:no; scroll:yes';

	// returned value
	var returnValue = window.showModalDialog(modalUrl, modalArguments, modalFeatures);

	return returnValue;

}


/*********************************************************************************************************
 * This function will save the original totals in JS variables for later use
 *********************************************************************************************************/
function saveVariables()
{

	cashReceivedTotalAll = document.getElementById('tot_cash_received_amt_all').innerText - 0;
	writeOffTotalAll = document.getElementById('tot_write_off_amt_all').innerText - 0;
	remainingBalTotalAll = document.getElementById('tot_remaining_bal_due_amt_all').innerText - 0;

	var moreRecordsExist = true;
	var cashReceivedLine = 0;
	var writeOffLine = 0;
	var remainingBalLine = 0;

	for (var i = 1; i < 26 && moreRecordsExist; i++)
	{
		var checkbox = document.getElementById('checkbox_' + i);
		if (checkbox != null)
		{
			cashReceivedLine = document.getElementById('cash_received_amt_' + i).value - 0;
			cashReceivedTotalPageOrig += cashReceivedLine;

			writeOffLine = document.getElementById('write_off_amt_' + i).value - 0;
			writeOffTotalPageOrig += writeOffLine;


			remainingBalLine = document.getElementById('remaining_bal_due_amt_' + i).value - 0;
			remainingBalTotalPageOrig += remainingBalLine;
		}
		else
			moreRecordsExist = false;
	}

}


/*********************************************************************************************************
 * This function will be invoked when the user
 *  1) checks/unchecks the checkbox
 *  2) hits 'select all' or 'unselect all' buttons.
 * It will update the cash received, remaining balance, write offs, and will enable/disable/gray out
 * records.  It will also update the different and totals
 *********************************************************************************************************/
function checkUncheck(lineNum)
{
	var checkbox = document.getElementById('checkbox_' + lineNum);
	var outstanding = document.getElementById('outstanding_bal_due_amt_' + lineNum).value - 0;
	document.getElementById('cash_received_amt_' + lineNum).style.backgroundColor = 'white';

	if(checkbox.checked)
	{
		//**set the settled flag
		document.getElementById('settle_flag_' + lineNum).value = 'Y';

		//**update the cash received, remaining balance, and write off
		document.getElementById('cash_received_amt_' + lineNum).disabled = false;
		document.getElementById('cash_received_amt_' + lineNum).value = outstanding.toFixed(2);

		document.getElementById('td_remaining_bal_due_amt_' + lineNum).innerText = '0.00';
		document.getElementById('remaining_bal_due_amt_' + lineNum).value = '0.00';

		document.getElementById('td_write_off_amt_' + lineNum).innerText = '0.00';
		document.getElementById('write_off_amt_' + lineNum).value = '0.00';

		//**update the underline
		document.getElementById('td_write_off_amt_' + lineNum).style.textDecoration = "underline";

		//update the color
		document.getElementById('td_ship_id_line_num_' + lineNum).style.color = 'black';
		document.getElementById('td_external_order_number_' + lineNum).style.color = 'black';
		document.getElementById('td_billing_date_' + lineNum).style.color = 'black';
		document.getElementById('td_delivery_date_' + lineNum).style.color = 'black';
		document.getElementById('td_expected_wholesale_amt_' + lineNum).style.color = 'black';
		document.getElementById('td_prior_rem_applied_amt_' + lineNum).style.color = 'black';
		document.getElementById('td_outstanding_bal_due_amt_' + lineNum).style.color = 'black';
		document.getElementById('td_days_aged_' + lineNum).style.color = 'black';
		document.getElementById('td_write_off_amt_' + lineNum).style.color = 'black';
		document.getElementById('td_remaining_bal_due_amt_' + lineNum).style.color = 'black';

	}
	else
	{
		//**set the settled flag
		document.getElementById('settle_flag_' + lineNum).value = 'N';

		//**update the cash received, remaining balance, and write off
		document.getElementById('cash_received_amt_' + lineNum).disabled = true;
		document.getElementById('cash_received_amt_' + lineNum).value = '0.00';

		document.getElementById('td_remaining_bal_due_amt_' + lineNum).innerText = outstanding.toFixed(2);
		document.getElementById('remaining_bal_due_amt_' + lineNum).value = outstanding.toFixed(2);

		document.getElementById('td_write_off_amt_' + lineNum).innerText = '0.00';
		document.getElementById('write_off_amt_' + lineNum).value = '0.00';

		//**update the color
		document.getElementById('td_ship_id_line_num_' + lineNum).style.color = 'gray';
		document.getElementById('td_external_order_number_' + lineNum).style.color = 'gray';
		document.getElementById('td_billing_date_' + lineNum).style.color = 'gray';
		document.getElementById('td_delivery_date_' + lineNum).style.color = 'gray';
		document.getElementById('td_expected_wholesale_amt_' + lineNum).style.color = 'gray';
		document.getElementById('td_prior_rem_applied_amt_' + lineNum).style.color = 'gray';
		document.getElementById('td_outstanding_bal_due_amt_' + lineNum).style.color = 'gray';
		document.getElementById('td_days_aged_' + lineNum).style.color = 'gray';
		document.getElementById('td_write_off_amt_' + lineNum).style.color = 'gray';
		document.getElementById('td_remaining_bal_due_amt_' + lineNum).style.color = 'gray';

	}

	updateTotals();
	updateDifference();

}



/*********************************************************************************************************
 * This function will be called whenever the user enters in a 'cash received' and takes the control off
 * of that input field.
 * It will update the remaining balance, the difference and the totals.
 *********************************************************************************************************/
function updateRemaining(lineNum)
{
	document.getElementById('cash_received_amt_' + lineNum).style.backgroundColor = 'white';

	if (validateCashReceived(lineNum, false))
	{
		var cashReceived = document.getElementById('cash_received_amt_' + lineNum).value - 0;
		var outstanding = document.getElementById('outstanding_bal_due_amt_' + lineNum).value - 0;
		var difference = outstanding - cashReceived;

		document.getElementById('td_write_off_amt_' + lineNum).innerText = '0.00';
		document.getElementById('write_off_amt_' + lineNum).value = '0.00';
		document.getElementById('td_write_off_amt_' + lineNum).style.textDecoration = "underline";

		difference = difference.toFixed(2);
		document.getElementById('td_remaining_bal_due_amt_' + lineNum).innerText = difference;
		document.getElementById('remaining_bal_due_amt_' + lineNum).value = difference;

		updateTotals();
		updateDifference();
	}

}


/*********************************************************************************************************
 * This funtion is invoked when the user clicks on the write-off link.  It is used to swap the values
 * between the write-off and the remaining balance fields.
 *********************************************************************************************************/
function toggleRemaining(lineNum)
{
	var checkbox = document.getElementById('checkbox_' + lineNum);
	if (checkbox.checked)
	{
		var remainingBal = document.getElementById('remaining_bal_due_amt_' + lineNum).value - 0;
		var writeOff = document.getElementById('write_off_amt_' + lineNum).value - 0;

		document.getElementById('remaining_bal_due_amt_' + lineNum).value = writeOff.toFixed(2);
		document.getElementById('td_remaining_bal_due_amt_' + lineNum).innerText = writeOff.toFixed(2);

		document.getElementById('write_off_amt_' + lineNum).value = remainingBal.toFixed(2);
		document.getElementById('td_write_off_amt_' + lineNum).innerText = remainingBal.toFixed(2);
		document.getElementById('td_write_off_amt_' + lineNum).style.textDecoration = "underline";

		updateTotals();
		updateDifference();
	}

}


/*********************************************************************************************************
 * This function will update the totals displayed, using the original totals and the sum of the line
 * records.
 *********************************************************************************************************/
function updateTotals()
{

	var moreRecordsExist = true;

	var cashReceivedLine = 0;
	var cashReceivedLineTotal = 0;
	var writeOffLine = 0;
	var writeOffLineTotal = 0;
	var remainingBalLine = 0;
	var remainingBalLineTotal = 0;

	for (var i = 1; i < 26 && moreRecordsExist; i++)
	{
		var checkbox = document.getElementById('checkbox_' + i);
		if (checkbox != null)
		{
			cashReceivedLine = document.getElementById('cash_received_amt_' + i).value - 0;
			cashReceivedLineTotal += cashReceivedLine;

			writeOffLine = document.getElementById('write_off_amt_' + i).value - 0;
			writeOffLineTotal += writeOffLine;


			remainingBalLine = document.getElementById('remaining_bal_due_amt_' + i).value - 0;
			remainingBalLineTotal += remainingBalLine;
		}
		else
			moreRecordsExist = false;
	}

	var cashReceivedTotalPageNew = cashReceivedTotalAll - cashReceivedTotalPageOrig + cashReceivedLineTotal;
	var writeOffTotalPageNew = writeOffTotalAll - writeOffTotalPageOrig + writeOffLineTotal;
	var remainingBalTotalPageNew = remainingBalTotalAll - remainingBalTotalPageOrig + remainingBalLineTotal;

	document.getElementById('tot_cash_received_amt_all').innerText =  cashReceivedTotalPageNew.toFixed(2);
	document.getElementById('tot_write_off_amt_all').innerText = writeOffTotalPageNew.toFixed(2);
	document.getElementById('tot_remaining_bal_due_amt_all').innerText = remainingBalTotalPageNew.toFixed(2);

}


/*********************************************************************************************************
 * This function will update the difference based off of the remittance amount entered and the total cash
 * received. Note that the updateTotals() function must be invoked prior to calling this function
 *********************************************************************************************************/
function updateDifference()
{
	var remitAmountText = document.getElementById('remit_amount_text').value;
	if (validateRemitAmount(true, false))
	{
		var cashReceivedTotal = document.getElementById('tot_cash_received_amt_all').innerText - 0;
		var newRemitAmountText = remitAmountText - cashReceivedTotal;

		var diffValue = document.getElementById('difference_value');

		if (diffValue != null)
		{
			document.getElementById('difference_value').innerText = newRemitAmountText.toFixed(2);
			if (newRemitAmountText < 0)
			{
				document.getElementById('difference_value').style.color = 'red';
				document.getElementById('difference_label').style.color = 'red';
			}
			else
			{
				document.getElementById('difference_value').style.color = 'black';
				document.getElementById('difference_label').style.color = 'black';
			}
		}
	}

}

/*********************************************************************************************************
 * This function is invoked when the user hits the 'uncheck all' button.  It will update remaining balance,
 * write off, and cash received at line level, followed by totals, and the difference
 *********************************************************************************************************/
function uncheckAll()
{
	var moreRecordsExist = true;

	for (var i = 1; i < 26 && moreRecordsExist; i++)
	{
		var checkbox = document.getElementById('checkbox_' + i);
		if (checkbox != null)
		{
			document.getElementById('checkbox_' + i).checked = false;
			checkUncheck(i);
			updateRemaining(i);
		}
		else
			moreRecordsExist = false;
	}

	updateTotals();
	updateDifference();

}


/*********************************************************************************************************
 * This function is invoked when the user hits the 'check all' button.  It will update remaining balance,
 * write off, and cash received at line level, followed by totals, and the difference
 *********************************************************************************************************/
function checkAll()
{
	var moreRecordsExist = true;

	for (var i = 1; i < 26 && moreRecordsExist; i++)
	{
		var checkbox = document.getElementById('checkbox_' + i);
		if (checkbox != null)
		{
			document.getElementById('checkbox_' + i).checked = true;
			checkUncheck(i);
			updateRemaining(i);
		}
		else
			moreRecordsExist = false;
	}

	updateTotals();
	updateDifference();

}


/*********************************************************************************************************
 * This function is called to ensure that the user has entered valid values for all the enterable fields
 *********************************************************************************************************/
function validateAll()
{
	var valid = true;

	valid = validateRemitDate(false, true);

	if (valid)
		valid = validateRemitAmount(false, true);

	if (valid)
		valid = validateRemitNumber();

	if (valid)
		valid = validateCashReceivedAll();

	if (valid)
		valid = validateRemitAmountvsCashReceived();

	return valid;
}


//*********************************************************************************************************
//*********************************************************************************************************
function validateRemitDate(nullOK, displayAlert)
{
	var remitDate = document.getElementById('remit_date_text').value;
	document.getElementById('remit_date_text').style.backgroundColor = 'white';

	if (nullOK && (remitDate == null || remitDate.length == 0) )
	{
		return true;
	}
	else
	{
		if (remitDate == null || remitDate.length == 0)
		{
			document.getElementById('remit_date_text').style.backgroundColor = 'pink';
			if (displayAlert)
				displayOkError('Please enter a remittance date');
			return false;
		}
		else
		{
			var dateRegex = /^([1-9]|0[1-9]|1[012])[- /.]([1-9]|0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d$/;


			if(!dateRegex.test(remitDate))
			{
				document.getElementById('remit_date_text').style.backgroundColor = 'pink';
				if (displayAlert)
					displayOkError("Please enter a valid remittance date.  The correct format is mm/dd/yyyy");
				return false;
			}
		}
	}

	return true;

}

//*********************************************************************************************************
//*********************************************************************************************************
function validateRemitAmount(nullOK, displayAlert)
{
	var remitAmount = document.getElementById('remit_amount_text').value;
	document.getElementById('remit_amount_text').style.backgroundColor = 'white';

	if (nullOK && (remitAmount == null || remitAmount.length == 0) )
	{
		return true;
	}
	else
	{
		if (isDigit(remitAmount) || isFloat(remitAmount))
		{
			remitAmount = remitAmount - 0;
			if (remitAmount < 0)
			{
				document.getElementById('remit_amount_text').style.backgroundColor = 'pink';
				if (displayAlert)
					displayOkError("Please enter a valid remittance amount that is greater than 0.");
				return false;
			}
		}
		else
		{
			document.getElementById('remit_amount_text').style.backgroundColor = 'pink';
			if (displayAlert)
				displayOkError("Please enter a numeric remittance amount.");
			return false;
		}
	}

	return true;
}

//*********************************************************************************************************
//*********************************************************************************************************
function validateRemitNumber()
{
	var remitNumber = document.getElementById('remit_number_text').value;
	document.getElementById('remit_number_text').style.backgroundColor = 'white';

	if (remitNumber == null || remitNumber.length <= 0)
	{
		document.getElementById('remit_number_text').style.backgroundColor = 'pink';
		displayOkError("Please enter a valid remittance number.");
		return false;
	}

	return true;

}


//*********************************************************************************************************
//*********************************************************************************************************
function validateCashReceivedAll()
{
	var moreRecordsExist = true;
	var valid = true;
	var cashReceivedLine;
	var checkbox;

	for (var i = 1; i < 26 && moreRecordsExist && valid; i++)
	{
		checkbox = document.getElementById('checkbox_' + i);
		if (checkbox != null)
		{
			valid = validateCashReceived(i, true);
		}
		else
			moreRecordsExist = false;
	}

	return valid;
}


//*********************************************************************************************************
//*********************************************************************************************************
function validateCashReceived(lineNum, displayAlert)
{
	document.getElementById('cash_received_amt_' + lineNum).style.backgroundColor = 'white';
	var cashReceived = document.getElementById('cash_received_amt_' + lineNum).value;
	var outstanding = document.getElementById('outstanding_bal_due_amt_' + lineNum).value - 0;

	if (isDigit(cashReceived) || isFloat(cashReceived))
	{
		cashReceived = cashReceived - 0;
		var difference = outstanding - cashReceived;
		if (difference < 0)
		{
			document.getElementById('cash_received_amt_' + lineNum).style.backgroundColor = 'pink';
			if (displayAlert)
				displayOkError("Cash Received must be less than or equal to outstanding balance.  Please correct");
			return false;
		}
	}
	else
	{
		document.getElementById('cash_received_amt_' + lineNum).style.backgroundColor = 'pink';
		if (displayAlert)
			displayOkError("Please enter a numeric value for the Cash Received");
		return false;
	}

	return true;
}



//*********************************************************************************************************
//*********************************************************************************************************
function validateRemitAmountvsCashReceived()
{
	var remitAmount = document.getElementById('remit_amount_text').value - 0;
	var totalCashReceived = document.getElementById('tot_cash_received_amt_all').innerText - 0;

	if (remitAmount == totalCashReceived)
	{
		return true;
	}
	else
	{
		var errorMessage = 'The remittance amount does not match the total cash received.  Do you want to continue and settle?';

		if (displayConfirmError(errorMessage))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

}


/*************************************************************************************
* Using the specified URL submit the form
**************************************************************************************/
function performAction(url)
{
  //alert("performAction - url = " + url);
  //showWaitMessage("mainContent", "wait", "Validating information please wait"); /* show wait message */
	document.forms[0].action = url;
	document.forms[0].submit();
}


//*********************************************************************************************************
//*********************************************************************************************************
function settle()
{
	var valid = validateAll();
	if (valid)
	{
		var selectedPartnerSelected = document.getElementById("partnerDropDown").selectedIndex;
		var selectedPartnerId = document.getElementById("partnerDropDown")[selectedPartnerSelected].value;
		var selectedPartnerOrigin = document.getElementById("partnerDropDown")[selectedPartnerSelected].origin;
		document.getElementById("new_partner_selected").value = selectedPartnerId;
		document.getElementById("new_origin_selected").value = selectedPartnerOrigin;
		document.getElementById("records_displayed_on_page").value = recordsDisplayedOnPage;
		var url	=	"ReceivablesAction.do" + "?action_type=settle"
						+	"&securitytoken=" + document.forms[0].securitytoken.value
						+	"&context=" + document.forms[0].context.value;

		performAction(url);
	}

}


//*********************************************************************************************************
//*********************************************************************************************************
function refresh()
{
	if (validateRemitDate(true, true) && validateRemitAmount(true, true) && validateCashReceivedAll())
	{
		var selectedPartnerSelected = document.getElementById("partnerDropDown").selectedIndex;
		var selectedPartnerId = document.getElementById("partnerDropDown")[selectedPartnerSelected].value;
		var selectedPartnerOrigin = document.getElementById("partnerDropDown")[selectedPartnerSelected].origin;
		document.getElementById("new_partner_selected").value = selectedPartnerId;
		document.getElementById("new_origin_selected").value = selectedPartnerOrigin;
		document.getElementById("records_displayed_on_page").value = recordsDisplayedOnPage;

		var url	=	"ReceivablesAction.do" + "?action_type=load"
						+	"&securitytoken=" + document.forms[0].securitytoken.value
						+	"&context=" + document.forms[0].context.value;

		performAction(url);
	}

}


//*********************************************************************************************************
//*********************************************************************************************************
function cancel()
{

	var discard = displayConfirmError("Are you sure you want to discard all your changes?");
	if (discard)
	{
		var url	=	"ReceivablesAction.do" + "?action_type=cancel"
						+	"&securitytoken=" + document.forms[0].securitytoken.value
						+	"&context=" + document.forms[0].context.value;

		performAction(url);
	}

}


//*********************************************************************************************************
//*********************************************************************************************************
function download()
{
	if (validateRemitDate(true, true) && validateRemitAmount(true, true) && validateCashReceivedAll())
	{
		var selectedPartnerSelected = document.getElementById("partnerDropDown").selectedIndex;
		var selectedPartnerId = document.getElementById("partnerDropDown")[selectedPartnerSelected].value;
		var selectedPartnerOrigin = document.getElementById("partnerDropDown")[selectedPartnerSelected].origin;
		var origPartnerId = document.getElementById("orig_partner_selected").value;

		if (selectedPartnerId != origPartnerId)
		{
			displayOkError('Please reselect the partner for whom the data is currently being displayed before using the DOWNLOAD feature.');
		}
		else
		{
			document.getElementById("new_partner_selected").value = selectedPartnerId;
			document.getElementById("new_origin_selected").value = selectedPartnerOrigin;
			document.getElementById("records_displayed_on_page").value = recordsDisplayedOnPage;

			var url	=	"ReceivablesAction.do" + "?action_type=download"
							+	"&securitytoken=" + document.forms[0].securitytoken.value
							+	"&context=" + document.forms[0].context.value;

			performAction(url);
		}
	}

}


//*********************************************************************************************************
//*********************************************************************************************************
function reportSubmission()
{
	if (validateRemitDate(true, true) && validateRemitAmount(true, true) && validateCashReceivedAll())
	{
		document.getElementById("records_displayed_on_page").value = recordsDisplayedOnPage;
		var url	=	"ReceivablesAction.do" + "?action_type=report_submission"
						+	"&securitytoken=" + document.forms[0].securitytoken.value
						+	"&context=" + document.forms[0].context.value;

		performAction(url);
	}

}


//*********************************************************************************************************
//*********************************************************************************************************
function pageAction(navigateTo)
{
	if (validateRemitDate(true, true) && validateRemitAmount(true, true) && validateCashReceivedAll())
	{
		var selectedPartnerSelected = document.getElementById("partnerDropDown").selectedIndex;
		var selectedPartnerId = document.getElementById("partnerDropDown")[selectedPartnerSelected].value;
		var selectedPartnerOrigin = document.getElementById("partnerDropDown")[selectedPartnerSelected].origin;
		document.getElementById("new_partner_selected").value = selectedPartnerId;
		document.getElementById("new_origin_selected").value = selectedPartnerOrigin;
		document.getElementById("records_displayed_on_page").value = recordsDisplayedOnPage;

		var url	=	"ReceivablesAction.do" + "?action_type=" + navigateTo
						+	"&securitytoken=" + document.forms[0].securitytoken.value
						+	"&context=" + document.forms[0].context.value;

		performAction(url);
	}

}


//*********************************************************************************************************
//*********************************************************************************************************
function exit()
{
	if (validateRemitDate(true, true) && validateRemitAmount(true, true) && validateCashReceivedAll())
	{
		document.getElementById("records_displayed_on_page").value = recordsDisplayedOnPage;
		var url	=	"ReceivablesAction.do" + "?action_type=exit"
						+	"&securitytoken=" + document.forms[0].securitytoken.value
						+	"&context=" + document.forms[0].context.value;

		performAction(url);
	}

}


//*********************************************************************************************************
//*********************************************************************************************************
function sort(newSortColumn)
{
	if (origPartnerSelected != null && origPartnerSelected.length > 0 && remitDetailsUpdateCountNumeric > 0 &&
			validateRemitDate(true, true) && validateRemitAmount(true, true) && validateCashReceivedAll())
	{
		var selectedPartnerSelected = document.getElementById("partnerDropDown").selectedIndex;
		var selectedPartnerId = document.getElementById("partnerDropDown")[selectedPartnerSelected].value;
		var selectedPartnerOrigin = document.getElementById("partnerDropDown")[selectedPartnerSelected].origin;
		document.getElementById("new_partner_selected").value = selectedPartnerId;
		document.getElementById("new_origin_selected").value = selectedPartnerOrigin;
		document.getElementById("records_displayed_on_page").value = recordsDisplayedOnPage;

		var origSortColumn = document.getElementById('orig_sort_column').value;
		var origSortDirection = document.getElementById('orig_sort_direction').value;

		var newSortDirection = 'asc';

		if (newSortColumn == origSortColumn && origSortDirection == 'asc')
		{
			newSortDirection = 'desc';
		}

		document.getElementById("new_sort_column").value = newSortColumn;
		document.getElementById("new_sort_direction").value = newSortDirection;

		//alert("origColumn = " + origSortColumn + " newSortColumn = " + newSortColumn + " origSortDirection = " + origSortDirection + " newSortDirection = " + newSortDirection);

		var url	=	"ReceivablesAction.do" + "?action_type=sort"
						+	"&securitytoken=" + document.forms[0].securitytoken.value
						+	"&context=" + document.forms[0].context.value;

		performAction(url);
	}

}






