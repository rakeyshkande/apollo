var emptyString = /^\s*$/
var node_text = 3; // DOM text node-type
var edited=0;


function noEnter()
{
	if(window.event && window.event.keyCode == 13)
	{
		window.event.returnValue = false;
		return false;
	}
	else
	{
		return true;
	}
}


function trim(str)
{
  return str.replace(/^\s+|\s+$/g, '')
}

function checkDateFormat(dateVal)
{
	var error=dateVal+"_error";
	var entry = document.getElementById(dateVal).value;
					
		fieldlength=entry.length;

		if(fieldlength>1&&!ValidateDate(dateVal))
		{
			document.getElementById(error).firstChild.nodeValue="Invalid Date.";
			document.getElementById(error).style.display = "block";
		    	
			return false;
		}

	return true;
}




function isPastYesterday(dateField)
{
	//alert(dateField);
	var today=new Date();
	var yesterday=today-1;
	var error=dateField+"_error";
	var entry = document.getElementById(dateField).value;
	var entryDate=parseAndCreateDate(entry);
	//alert("entryDate="+entryDate+"\t yesterday="+yesterday);				
	var timeDiff=	getDateTimeDiff(entryDate, yesterday);
        //alert("timeDiff="+timeDiff);
		if(timeDiff<=0)
		{
			document.getElementById(error).firstChild.nodeValue="The date must  be prior to today's date.";
			document.getElementById(error).style.display = "block";
		    	
			return true;
		}

	return false;
}



		function checkenddate(startDate, endDate, days)
		{
			var valid=true;
	    		
	    		var end = document.getElementById(endDate).value;
	    		var start=document.getElementById(startDate).value;
			var error=endDate+"_error";
			if(end.length>0&&start.length>0&&(end.indexOf("/")!=-1)&&(start.indexOf("/")!=-1)){
			
				var timeDiff=getTimeDiff(start, end);
				if(timeDiff<0){
					document.getElementById(error).firstChild.nodeValue="The start date must be before the end date";
					document.getElementById(error).style.display = "block";
				   	//document.getElementById(endDate).style.backgroundColor='pink';
					valid= false;
					
				}else if(days!=null&&days!=''&&days!=0){
					var timeDiff=getTimeDiff(start, end);
					
					if(timeDiff>days){
					    document.getElementById(error).firstChild.nodeValue="The date range can not exceed "+days+" days";
					    document.getElementById(error).style.display = "block";
					    
					    //document.getElementById(endDate).style.backgroundColor='pink';
					    valid= false;
					}
				}
			
			}
			
	        	
			
			if(valid)
			{
				document.getElementById(endDate).style.backgroundColor='#FFFFFF';
				document.getElementById(error).style.display = "none";
				
			}
		
			return valid;
	       }

/*****************************
 Check if the date object is prior to today by more than the 
 given number of year/month/days. Scale is 'Y', 'M', or 'D'.
********************************/
function isDateTooFarBack(startDate, scale, number)
{
    //create date object from entry date
    var start = document.getElementById(startDate).value;
    var entryDate=parseAndCreateDate(start);
    var valid = true;
    
    //calculate the bountry date based on scale and number
    var boundryDate = new Date();
    
    if(scale == "Y") {
            scaleDesc = " years";
            boundryDate.setYear(boundryDate.getFullYear() - number);
    } else if(scale == "M") {
            scaleDesc = " months";
            boundryDate.setMonth(boundryDate.getMonth() - number);
    } else { // scale == "D"
            scaleDesc = " days";
            boundryDate.setDate(boundryDate.getDate() - number);
    }
    
    valid = entryDate > boundryDate;
    
    if(!valid) {
        document.getElementById(startDate + "_error").firstChild.nodeValue="The start date can not exceed "+ number + scaleDesc + " from current date";
				document.getElementById(startDate + "_error").style.display = "block";
    }

    return valid;
}
		
/*****************************
Converts a date object to the format of mm/dd/yyyy.
********************************/  
/*
function dateToString(dateObj) {
        var yr = dateObj.getFullYear();
        
        var mo = dateObj.getMonth() + 1;
        var dt = dateObj.getDate();      
        var dateStr = mo + "/" + dt + "/" + yr;
        
        return dateStr;
        
}			
*/
		
		
					
		
		// -----------------------------------------
		//            validatePresent
		// Validate if something has been entered
		// Returns true if so 
		// -----------------------------------------
		
		function validatePresent(vfld,   // element to be validated
		                         ifld )  // id of element to receive info/error msg
		{
		  
		  var elmType=document.forms[0][vfld].type;
		  //alert("elmType="+elmType);
		  if(elmType=='radio'||elmType=='checkbox'||typeof elmType=='undefined'){
		  	
		  	return validateMultiple(vfld,ifld);
		  }else{
			  var value=document.forms[0][vfld].value;
			  if(value==null||trim(value)==''){
			  	document.getElementById(ifld).firstChild.nodeValue="Required.";
			  	document.getElementById(ifld).style.display = "block";
			  	return false;
			  }else{
			  	document.getElementById(ifld).style.display = "none";
			  	return true;
			  }
	           }
		 
		}
		
		
		
		function validateNumber(vfld,ifld){
		
			var regex = /^\d*$/;
			var value=document.forms[0][vfld].value;
			  if(value!=null&&trim(value)!=''){
			  	if(!regex.test(trim(value))){
				  	document.getElementById(ifld).firstChild.nodeValue="Invaid Number.";
				  	document.getElementById(ifld).style.display = "block";
				  	return false;
				}else{
				  document.getElementById(ifld).style.display = "none";
			  	  return true;
				}
			  }else{
			  	document.getElementById(ifld).style.display = "none";
			  	return true;
			  }
		
		}
		
		
		function validateMultiple(vfld,ifld){
		    var valid=false;
		    
		    for(var i=0; i<document.forms[0][vfld].length; i++){
		        //alert("length="+i+"checked"+document.forms[0][vfld][i].checked);
		    	if(document.forms[0][vfld][i].checked==true){
		    		
		  		valid= true;
		  		break;
		    	}
		    }
		    
		    if(valid==true){
		    	
		    	document.getElementById(ifld).style.display = "none";
		    }else{
		        document.getElementById(ifld).firstChild.nodeValue="Required.";
		  	document.getElementById(ifld).style.display = "block";
		    }
		
		    return valid;
		
		} 
		
		
		function getTimeDiff(start, end){
		   var s, startTime, endTime, diffTime, startArray, endArray; //Declare variables.
		   var MinMilli = 1000 * 60;       //Initialize variables.
		   var HrMilli = MinMilli * 60;
		   var DyMilli = HrMilli * 24;
		   
		   startTime = parseAndCreateDate(start);  
		   endTime=parseAndCreateDate(end);  
		   diffTime=getDateTimeDiff(startTime, endTime);
		   
		   return(diffTime);                      //Return results.
		}
		
		
		function getDateTimeDiff(startTime, endTime){
		   var s, startTime, endTime, diffTime, startArray, endArray; //Declare variables.
		   var MinMilli = 1000 * 60;       //Initialize variables.
		   var HrMilli = MinMilli * 60;
		   var DyMilli = HrMilli * 24;
		   
		   
		   diffTime=Math.round(Math.abs(endTime / DyMilli))-Math.round(Math.abs(startTime / DyMilli));
		   
		   return(diffTime);                      //Return results.
		}
		
		
		function parseAndCreateDate(dateStr){
		   var dateArray=dateStr.split("/");
		   return createDate( dateArray[0], dateArray[1], dateArray[2]);  
		}
		
		
		function createDate(month, day, year){
		
		   var dateVal=new Date();
		   var mon=parseInt(month,10)-1;//January starts with 0.
		   
		   var d=parseInt(day,10);
		   if(year.length==2){
		   	year ='20'+year;
		   }
		   var yr=parseInt(year);
		   //alert("m="+mon+"\tday="+d+"\tyear="+yr);
		   dateVal.setMonth(mon);
		   dateVal.setDate(d);
		   dateVal.setYear(yr);
		   //alert("date="+dateVal.getTime());
		   return dateVal;
		}
		
		
		function doConfirm()
		{
			var url = '';
		
			//Modal_URL
			var modalUrl = "html/confirm.html";
		
			//Modal_Arguments
			var modalArguments = new Object();
			modalArguments.modalText = 'Are you sure that you want to leave page without submitting the request?';
		
			//Modal_Features
			var modalFeatures = 'dialogWidth:200px; dialogHeight:180px; center:yes; status=no; help=no;';
			modalFeatures += 'resizable:no; scroll:no';
		
			//get a true/false value from the modal dialog.
			return window.showModalDialog(modalUrl, modalArguments, modalFeatures);
		
			
		}
		
		
		function doReportingMainMenu(){
		  
			  var url="ReportingMainMenu.do?report_main_menu="+document.getElementById("report_main_menu").value+getSecurityParams(false);
			  document.forms[0].action=url;
			  document.forms[0].target="_self";
			  document.forms[0].submit();
	           		
	       }
	       
	       
	       
	       function doReportingConfirmMainMenu(){
		  var gotoMainmenu=true;
		  if(edited>0){
		  	gotoMainmenu=doConfirm();
		  }
		  if(gotoMainmenu){
			  doReportingMainMenu();
	           }		
	       }
	       
	       
	       
	       function doReportingLogoff(){
		
		  var url="ReportingMainMenu.do?report_main_menu=logoff"+getSecurityParams(false);
		  document.forms[0].action=url;
		  document.forms[0].target="_self";
		  document.forms[0].submit();		
	       }
	       
	       
	       
	       function getReportResultList(){
		
		  var url="GetReportViewerList.do?start_date="+document.forms[0].start_date.value+"&end_date="+document.forms[0].end_date.value+getSecurityParams(false);
		  document.forms[0].action=url;
		  document.forms[0].target="_self";
		  document.forms[0].submit();		
	       }
	       
	       
	       
	       function updateEdit(){
			//alert("edited="+edited);
			edited +=1;
	       }
	       
	       function doReportingBack(){
	       
	       		history.back();
	       
	       }
	       
	       
	       function doReportingConfirmBack(){
	          
	       	  var goBack=true;
		  if(edited>0){
		  	goBack=doConfirm();
		  }
		  if(goBack){
			  doReportingBack();
	          }	
	       
	       }
	       
	       
	       function addCodeToOptionList(val, sourceCodeArray, hiddenField, fieldName){
	       	if(typeof val !="undefined"){
	       		//alert("fieldName="+hiddenField);
	       		sourceCodeArray[sourceCodeArray.length]=val;
	       		
	       		var codeOptions=eval('document.forms[0].' + fieldName); 
	       		//alert(codeOptions.type);
	       		codeOptions.options.length = 0
	       		for(var i=0; i<sourceCodeArray.length; i++){
	       			//alert(sourceCodeArray[i]);
	       			codeOptions.options[i]=new Option(sourceCodeArray[i], sourceCodeArray[i]);
	       		}
	       		setSelectedCodes(sourceCodeArray, hiddenField);
	       		
	        }
	       }
	       
	       
	       function setSelectedCodes(sourceCodeArray, hiddenField){
	       		var selectedCodes=eval('document.forms[0].' + hiddenField);
	       		selectedCodes.value='';
	       		for(var i=0; i<sourceCodeArray.length; i++){
	       			
	       			selectedCodes.value +=sourceCodeArray[i];
	       			if(i!=sourceCodeArray.length-1){
	       				selectedCodes.value +=",";
	       			}
	       		}
	       		//alert("total selected="+selectedCodes.value);
	       
	       }
	      
	       
	       function removeSelected(fieldId, sourceCodeArray, hiddenField){
	       		//remove from array.
	       		
	       		var codeOptions=eval('document.forms[0].' + fieldId);
			
	       		
	       		for( var i=0; i<codeOptions.length; i++){
	       			if(codeOptions.options[i].selected){
	       				//alert("selected="+i);
	       				codeOptions.options[i]=null;
	       				removeFromArray(sourceCodeArray, i);
	       				//selCount += 1;
	       				i--;
	       			}
	       		}
	       		setSelectedCodes(sourceCodeArray, hiddenField);
	       
	       }
	       
	       function removeFromArray(array, delindex){
	       
	       		var size = array.length;
	       		
	       		for (var i=0; i<=size; i++)
				array[i] = ((i == delindex) ? "delete" : array[i]);
				for (var j=delindex; j<size-1; j++)
					if (j != size)
						array[j] = array[j+1];
					
				
			array.length = size-1;
		        
	       
	       
	       }
	       
	       
	       function removeAll(fieldId, sourceCodeArray, hiddenField){
	       		//remove from array.
	       		sourceCodeArray.length = 0;
	       		//alert(sourceCodeArray.length);
	       		var codeOptions=document.forms[0][fieldId];
	       		var selectedCodes=eval('document.forms[0].' + hiddenField);
	       		selectedCodes.value='';
	       		codeOptions.length=0;
	       		
	       
	       }
	       
	      
/*****************************
 this will take you to the first page
********************************/
function goToFirstPage()
{
document.forms[0].start_page.value = 1;
reopenPopup();

}

/************************
this will take u to one previous page
****************************************/

function goToPreviousPage()
{
document.forms[0].start_page.value--;
reopenPopup();

}

/**************************
this will take u to one next page
**************************************/

function goToNextPage()
{
document.forms[0].start_page.value++;
reopenPopup();

}


/*************************
this will take u to the last page
******************************************/

function goToLastPage()
{
document.forms[0].start_page.value = document.forms[0].total_page.value;
reopenPopup();

}


/*************************************
this is to disable the navigation buttons
********************************/

     function buttonDis(page, form)
    {
	//setNavigationHandlers();
	
   	if (form.total_page.value <= 1)
   	{
   	document.getElementById("firstpage").disabled= "true";
   	document.getElementById("previouspage").disabled= "true";
   	document.getElementById("nextpage").disabled= "true";
   	document.getElementById("lastpage").disabled= "true";
   	return;}
   	if (page < 2)
   	{
   	document.getElementById("firstpage").disabled= "true";
   	document.getElementById("previouspage").disabled= "true";
   	return;}
	if (page == form.total_page.value)
	{
	document.getElementById("nextpage").disabled= "true";
   	document.getElementById("lastpage").disabled= "true";
	return;}

}


					function untabHeader()
					{
											
						var backButton = document.getElementById("back_button");
						if(backButton != null)
						{
							backButton.tabIndex = -1;
						}
						
						var inputElements = document.getElementsByTagName("input");
						var type;
						for(var i = 0; i < inputElements.length; i++)
						{
							type = inputElements[i].getAttribute("type");
							// Untab any hidden input fields on the page
							if(type == "hidden")
							{
								inputElements[i].setAttribute("tabindex", "-1");
							}
						}
					}
                                        

//****************************************************************************************************
//* moveSelectedAll
//****************************************************************************************************
function moveSelectedAll()
{
  var vendorsAvailableDropDown = document.getElementById("sVendorsAvailable");

  for (var i = 0; i < vendorsAvailableDropDown.length; i++)
  {
		vendorsAvailableDropDown[i].selected = true; 
	}
	moveSelected(); 
        document.getElementById("p_vendor_id").value=getVendorsChosen();
}


//****************************************************************************************************
//* moveSelected
//****************************************************************************************************
function moveSelected()
{
  var vendorsAvailableDropDown = document.getElementById("sVendorsAvailable");
  var vendorsChosenDropDown  = document.getElementById("sVendorsChosen");

	var newVendorsAvailableArrayList = new Array();
	var newVendorsChosenArrayList = new Array();

	//create existing Chosen list
  for (var i = 0; i < vendorsChosenDropDown.length; i++)
  {
		newVendorsChosenArrayList[i] = new Array(vendorsChosenDropDown[i].value, vendorsChosenDropDown[i].innerText)
	}

	//define index positions for the arrays - new data will be added according to these positions
	var newVendorsChosenIndex = newVendorsChosenArrayList.length - 1; 
	var newVendorsAvailableIndex = -1; 

	//(1) create new Available ArrayList 
	//(2) append to Chosen ArrayList
  for (var i = 0; i < vendorsAvailableDropDown.length; i++)
  {
		if (vendorsAvailableDropDown[i].selected)
		{
			newVendorsChosenIndex++; 
			newVendorsChosenArrayList[newVendorsChosenIndex] = new Array(vendorsAvailableDropDown[i].value, vendorsAvailableDropDown[i].innerText)
		}
		else
		{
			newVendorsAvailableIndex++; 
			newVendorsAvailableArrayList[newVendorsAvailableIndex] = new Array(vendorsAvailableDropDown[i].value, vendorsAvailableDropDown[i].innerText)
		}
	}

	//sort Available ArrayList
	bubbleSortAsc(newVendorsAvailableArrayList, newVendorsAvailableArrayList.length, 1); 

	//sort Chosen ArrayList
	bubbleSortAsc(newVendorsChosenArrayList, newVendorsChosenArrayList.length, 1); 

	//remove options from existing Available select dropdown
  for (var i = vendorsAvailableDropDown.length - 1; i >= 0 ; i--)
  {
		vendorsAvailableDropDown.remove(i); 
	}  	

	//remove options from existing Chosen select dropdown
  for (var i = vendorsChosenDropDown.length - 1; i >= 0 ; i--)
  {
		vendorsChosenDropDown.remove(i); 
	}  	


	//add items from existing Available select/options
  for (var i = 0; i < newVendorsAvailableArrayList.length; i++)
  {
		//Create a new option  - definition = new Option(text, value, defaultSelected, selected)  
		var newVendorsAvailableOption = new Option(newVendorsAvailableArrayList[i][1], newVendorsAvailableArrayList[i][0], false, false);

		//and add it to the drop down
		vendorsAvailableDropDown.add(newVendorsAvailableOption); 
	}  	

	//add items from existing Available select/options
  for (var i = 0; i < newVendorsChosenArrayList.length; i++)
  {
		//Create a new option  - definition = new Option(text, value, defaultSelected, selected)  
		var newVendorsChosenOption = new Option(newVendorsChosenArrayList[i][1], newVendorsChosenArrayList[i][0], false, false);

		//and add it to the drop down
		vendorsChosenDropDown.add(newVendorsChosenOption); 
	}  	
        document.getElementById("p_vendor_id").value=getVendorsChosen();
}

//****************************************************************************************************
//* moveUnSelectedAll
//****************************************************************************************************
function moveUnSelectedAll()
{
  var vendorsChosenDropDown = document.getElementById("sVendorsChosen");

  for (var i = 0; i < vendorsChosenDropDown.length; i++)
  {
		vendorsChosenDropDown[i].selected = true; 
	}
	moveUnSelected(); 
        document.getElementById("p_vendor_id").value=getVendorsChosen();
}


//****************************************************************************************************
//* moveUnSelected
//****************************************************************************************************
function moveUnSelected()
{
  var vendorsChosenDropDown = document.getElementById("sVendorsChosen");
  var vendorsAvailableDropDown  = document.getElementById("sVendorsAvailable");

	var newVendorsChosenArrayList = new Array();
	var newVendorsAvailableArrayList = new Array();

	//create existing Available list
  for (var i = 0; i < vendorsAvailableDropDown.length; i++)
  {
		newVendorsAvailableArrayList[i] = new Array(vendorsAvailableDropDown[i].value, vendorsAvailableDropDown[i].innerText)
	}

	//define index positions for the arrays - new data will be added according to these positions
	var newVendorsAvailableIndex = newVendorsAvailableArrayList.length - 1; 
	var newVendorsChosenIndex = -1; 

	//(1) create new Chosen ArrayList 
	//(2) append to Available ArrayList
  for (var i = 0; i < vendorsChosenDropDown.length; i++)
  {
		if (vendorsChosenDropDown[i].selected)
		{
			newVendorsAvailableIndex++; 
			newVendorsAvailableArrayList[newVendorsAvailableIndex] = new Array(vendorsChosenDropDown[i].value, vendorsChosenDropDown[i].innerText)
		}
		else
		{
			newVendorsChosenIndex++; 
			newVendorsChosenArrayList[newVendorsChosenIndex] = new Array(vendorsChosenDropDown[i].value, vendorsChosenDropDown[i].innerText)
		}
	}

	//sort Chosen ArrayList
	bubbleSortAsc(newVendorsChosenArrayList, newVendorsChosenArrayList.length, 1); 

	//sort Available ArrayList
	bubbleSortAsc(newVendorsAvailableArrayList, newVendorsAvailableArrayList.length, 1); 

	//remove options from existing Chosen select dropdown
  for (var i = vendorsChosenDropDown.length - 1; i >= 0 ; i--)
  {
		vendorsChosenDropDown.remove(i); 
	}  	

	//remove options from existing Available select dropdown
  for (var i = vendorsAvailableDropDown.length - 1; i >= 0 ; i--)
  {
		vendorsAvailableDropDown.remove(i); 
	}  	


	//add items from existing Chosen select/options
  for (var i = 0; i < newVendorsChosenArrayList.length; i++)
  {
		//Create a new option  - definition = new Option(text, value, defaultSelected, selected)  
		var newVendorsChosenOption = new Option(newVendorsChosenArrayList[i][1], newVendorsChosenArrayList[i][0], false, false);

		//and add it to the drop down
		vendorsChosenDropDown.add(newVendorsChosenOption); 
	}  	

	//add items from existing Chosen select/options
  for (var i = 0; i < newVendorsAvailableArrayList.length; i++)
  {
		//Create a new option  - definition = new Option(text, value, defaultSelected, selected)  
		var newVendorsAvailableOption = new Option(newVendorsAvailableArrayList[i][1], newVendorsAvailableArrayList[i][0], false, false);

		//and add it to the drop down
		vendorsAvailableDropDown.add(newVendorsAvailableOption); 
	}  	
        document.getElementById("p_vendor_id").value=getVendorsChosen();
}



//****************************************************************************************************
//* bubbleSortAsc
//****************************************************************************************************
function bubbleSortAsc(arrayName,length,sortBy) 
{
	for (var i=0; i<(length-1); i++)
	{
		for (var j=i+1; j<length; j++)
		{
			if (arrayName[j][sortBy] < arrayName[i][sortBy]) 
			{
				var dummy = arrayName[i];
				arrayName[i] = arrayName[j];
				arrayName[j] = dummy;
			}
		}
	}
}


//***************************************************************************************************
// getVendorsChosen()
//***************************************************************************************************/
function getVendorsChosen()
{
  var vc = document.getElementById("sVendorsChosen");
  var vendorsChosen = ""; 
  
  for (var i = 0; i < vc.length; i++)
  {
    vendorsChosen += vc[i].value + ","; 
  }

  vendorsChosen = vendorsChosen.substring(0,vendorsChosen.length - 1)
  return vendorsChosen; 
  
}

//Format the new line delimited product ids into a comma delimited string.
function setInputProductIds()
{
    var p = document.getElementById("taProductIds").value;
    var productsInput = "";
    productsInput = p.split("\r\n").join(",");
    document.getElementById("p_product_id").value = productsInput;
}

function validateProductId()
{
    var ret = true;
    var fieldName = "taProductIds";
    var fieldError = fieldName + "_error";
    var errorMessage = "";
    var p = document.getElementById(fieldName).value;
    var regExp = new RegExp("\n","g");
    var regExp1 = new RegExp("\r","g");
    
    p = p.replace(regExp,"");
    p = p.replace(regExp1,"");

    if (p.length > 2975)
    {
        errorMessage = "You can only enter 3000 characters in Product Ids.";
        document.getElementById(fieldError).firstChild.nodeValue=errorMessage;
	document.getElementById(fieldError).style.display = "block";
        ret = false;
    }
    return ret;
}

function moveSourceTypesSelected()
{
  var vendorsAvailableDropDown = document.getElementById("sourceTypesAvailable");
  var vendorsChosenDropDown  = document.getElementById("source_type_selected");
  
  	
  	if(vendorsChosenDropDown.length >= 15){
  		document.getElementById("source_type_error").style.display = "block";
  		return;
  	}
  	else{
  		document.getElementById("source_type_error").style.display = "none";
  	}
  	
	var newVendorsAvailableArrayList = new Array();
	var newVendorsChosenArrayList = new Array();

	//create existing Chosen list
  for (var i = 0; i < vendorsChosenDropDown.length; i++)
  {
		newVendorsChosenArrayList[i] = new Array(vendorsChosenDropDown[i].value)
	}

	//define index positions for the arrays - new data will be added according to these positions
	var newVendorsChosenIndex = newVendorsChosenArrayList.length - 1; 
	var newVendorsAvailableIndex = -1; 

	//(1) create new Available ArrayList 
	//(2) append to Chosen ArrayList
	//available dropdown - {1,2,3,4,5}
	//chosen - {2,4}
	var exists = "F";
  for (var i = 0; i < vendorsAvailableDropDown.length; i++)
  {
		if (vendorsAvailableDropDown[i].selected)
		{
			for(var j = 0; j < vendorsChosenDropDown.length; j++){
				if(vendorsChosenDropDown[j].value == vendorsAvailableDropDown[i].value){
					exists = "T";
				}
			}
			if(exists != "T"){
				newVendorsChosenIndex++; 
				newVendorsChosenArrayList[newVendorsChosenIndex] = new Array(vendorsAvailableDropDown[i].value)
			}
			exists = "F";
		}
  }
  
  	var chosenLength = parseInt(vendorsChosenDropDown.length);
  	var newChosenLength = parseInt(newVendorsChosenArrayList.length);
  	
  	if(parseInt(newChosenLength) > 15){
  		//alert("Total length is : "+parseInt(newChosenLength));
  		document.getElementById("source_type_error").style.display = "block";
  		return;
  	}
  
	
	//sort Chosen ArrayList
	bubbleSortAsc(newVendorsChosenArrayList, newVendorsChosenArrayList.length, 1); 
	sourceTypeArray = newVendorsChosenArrayList;
	
	//remove options from existing Chosen select dropdown
	for (var i = vendorsChosenDropDown.length - 1; i >= 0 ; i--)
  	{
		vendorsChosenDropDown.remove(i); 
	}  	

	//add items from existing Available select/options
	for (var i = 0; i < newVendorsChosenArrayList.length; i++)
	{
		//Create a new option  - definition = new Option(text, value, defaultSelected, selected)  
		var newVendorsChosenOption = new Option(newVendorsChosenArrayList[i][0], newVendorsChosenArrayList[i][0], false, false);

		//and add it to the drop down
		vendorsChosenDropDown.add(newVendorsChosenOption); 
	}  	
  		
        document.getElementById("p_source_types").value=getSourceTypesChosen();
        
}

function getSourceTypesChosen()
{
  var vc = document.getElementById("source_type_selected");
  var sourceTypesChosen = ""; 
  
  for (var i = 0; i < vc.length; i++)
  {
	  var stype = vc[i].value.replace('&','%26');
	  sourceTypesChosen += stype + ","; 
  }
  
  sourceTypesChosen = sourceTypesChosen.substring(0,sourceTypesChosen.length - 1)
  return sourceTypesChosen; 
  
}
                                        