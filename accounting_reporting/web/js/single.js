
/*************************************
Edit Function for Single Recipient
*************************************/
function editSingleRecipient()
{
document.getElementById("gcc_action").value = 'display_edit';
enableAllSingle();
doGccRecipientAction();
}

/*************************************
Save Function for Single
*************************************/
function savesingleGC()
{
var flag1 = 'true';
var flag = 'true'
flag = checkRedemptionDate();
	if (flag == 'false' && flag1 == 'true')
	flag1 = 'false';

flag = checkOrderNumber();
if (flag == 'false' && flag1 == 'true')
	flag1 = 'false';

flag = validateSrcCodeAlphaNumeric();
if (flag == 'false' && flag1 == 'true')
flag1 = 'false';

	if (flag1 == 'true')
	{
	document.getElementById("gcc_action").value = 'maintain';
	enableAllSingle();
	doSingleCertificateCouponAction();
	}
}

/***************************************************
the on status change function
***************************************************/

function onStatusChangeSingle()

{
var status = document.getElementById("gcc_status_single").value;


			if (status != 'Redeemed')
				{
				document.getElementById('gcc_redemption_date').value = '';
				document.getElementById('gcc_redemption_date').disabled = true;
				document.getElementById('gcc_redemption_date').style.backgroundColor="#eeeeee";
				document.getElementById('redemp_date_img').disabled = true;
				document.getElementById('gcc_order_number').value = '';
				document.getElementById('gcc_order_number').disabled = true;
				document.getElementById('gcc_order_number').style.backgroundColor="#eeeeee";
				}
				else
				{
				document.getElementById('gcc_redemption_date').disabled = false;
				document.getElementById('gcc_redemption_date').style.backgroundColor="#FFFFFF";
				document.getElementById('redemp_date_img').disabled = false;
				document.getElementById('gcc_order_number').disabled = false;
				document.getElementById('gcc_order_number').style.backgroundColor="#FFFFFF";
				}

}


/************************************************************
To check the dropdown for status
*************************************************************/

function checkRedemptionDate()
{
	var status = document.getElementById("gcc_status_single").value;
	var flag = 'true';

if (status == 'Redeemed')
{
	rDate = document.getElementById("gcc_redemption_date").value
		issueDate = document.getElementById("gcc_issue_date").value

				if (rDate == '')
				{
					document.getElementById('gcc_redemption_date').style.backgroundColor='pink';
					document.getElementById("span_redemption_date").style.display = "block";
					document.getElementById("span_redemption_date_error").style.display = "none";
					flag = 'false';
				}
                                else if(!validateDateFormat(rDate))
                                {
                                        document.getElementById('gcc_redemption_date').style.backgroundColor='pink';
					document.getElementById("span_redemption_date").style.display = "none";
					document.getElementById("span_redemption_date_error").style.display = "block";
					flag = 'false';
                                  
                                }
				else if(!ValidateDate('gcc_redemption_date'))
				{
					document.getElementById('gcc_redemption_date').style.backgroundColor='pink';
					document.getElementById("span_redemption_date").style.display = "none";
					document.getElementById("span_redemption_date_error").style.display = "block";
					flag = 'false';

				}
				else if(compareDates(form.gcc_issue_date, form.gcc_redemption_date, 4) == 'false')
				{
					document.getElementById('gcc_redemption_date').style.backgroundColor='pink';
					document.getElementById("span_redemption_date").style.display = "none";
					document.getElementById("span_redemption_date_error").style.display = "block";
					flag = 'false';

				}
				else
				{
				document.getElementById('gcc_redemption_date').style.backgroundColor='#FFFFFF';
				document.getElementById("span_redemption_date").style.display = "none";
				document.getElementById("span_redemption_date_error").style.display = "none";
				flag = 'true';
				}

	}
	return flag;
}

/*****************************
check order number
*******************************/
function checkOrderNumber()
{
ordnum = document.getElementById("gcc_order_number").value
var status = document.getElementById("gcc_status_single").value;
var flag = 'true';

if (status == 'Redeemed')
{

				if (ordnum == '' || ordnum == null)
				{
				document.getElementById("span_order_number").style.display = "block";
				document.getElementById('gcc_order_number').style.backgroundColor='pink';
				flag = 'false';
				}
				else
				{
				document.getElementById("span_order_number").style.display = "none";
				document.getElementById('gcc_order_number').style.backgroundColor='#FFFFFF';
				flag = 'true';
				}
}

return flag;
}


/**********************************************
when user presses the enter key without focus on any button
***********************************************/

function enterkey()

{
    if (window.event.keyCode == 13)
    return false;
}

/*********************************************************
To enable all fields on single coupon Maintainance screen
*********************************************************/

var sfields = new Array("gcc_request_number","gcc_issue_amount","gcc_type","gcc_issue_date","gcc_paid_amount","gcc_expiration_date","gcc_quantity","gcc_format","gcc_filename","gcc_requestor_name","gcc_redemption_type","gcc_program_name","gcc_program_description","gcc_comments","gcc_company","gcc_source_code","gcc_other","gcc_prefix","gcc_promotion_id","gcc_partner_program_name");

function enableAllSingle()
{
	var sfieldlength = sfields.length;

		for (var i = 0; i < sfieldlength; i++)
				{

				var action = document.getElementById("gcc_action").value;
				var multiple = document.getElementById("is_multiple_gcc").value;
				var sfield = sfields[i];

				if(document.getElementById(sfield).type == "select-one")
				document.getElementById(sfield).disabled = false;
				else
				document.getElementById(sfield).disabled = false;
				}

}