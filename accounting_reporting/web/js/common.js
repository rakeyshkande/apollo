// Return 1 if lhs > rhs, 0 if lhs = rhs and -1 if lhs < rhs or -2 if invalid

function scltCompareDouble(lhsValueStr, rhsValueStr)

{

	if (!scltIsDouble(lhsValueStr) || !scltIsDouble(rhsValueStr))
    {
		return -2;
    }

	var val = parseFloat(lhsValueStr);

   var val2 = parseFloat(rhsValueStr);

   if (val == val2) return 0;

   if (val < val2) return -1;

   return 1;

}

function scltIsDouble(value)

{

    var str = new String(value)

    if (isEmptyString(str) || scltIsNaN(str)) {

        return false;

    }

    // var pattern = new RegExp("[\d\s]+(\.\d{2})?");

    // The pattern specified, look for a character that is not a

	// digit, space, plus, or minus sign.  If we find any char that

	// is not one of those, we will assume that value is not a double.

	// TODO: We need to improve the reg. expression but we need to be

	// careful and only use the charaters we are using at the moment

	// otherwise we might introduce anoth i18n problem, for example

	// checking a-z is not acceptable.

	var pattern = new RegExp("[^0-9 -+\.]");

    m = str.search(pattern);

    if (m == -1){

        return true

    }

    else{

        return false;

    }

}

function isEmptyString(testString)

{

	if (testString == null) {

		return true;

	}



   var newString = testString.replace(/\s/g, "");

   if ((newString == null) || (newString.length == 0)) {

      return true;

   }



   return false;

}

function scltIsNaN(value)

{

//   return scltIsDouble(value) == false;

    return isNaN(value);

}