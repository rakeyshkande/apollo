INSERT INTO FRP.GLOBAL_PARMS VALUES ('ACCOUNTING', 'COUPON_EXPIRE_GRACE_PERIOD', '30');
INSERT INTO FRP.GLOBAL_PARMS VALUES ('ACCOUNTING', 'COUPON_MAX_CS_ISSUE_AMOUNT', '50');
INSERT INTO FRP.GLOBAL_PARMS VALUES ('ACCOUNTING', 'GIFT_CERT_LAST_SYNCH_DATE', SYSDATE);

--defect 895
INSERT INTO FRP.GLOBAL_PARMS VALUES ('ACCOUNTING', 'VENDOR_JDE_FTP_NEXT_SERIAL_NO', '100','chu',sysdate,'chu',sysdate);
INSERT INTO FRP.GLOBAL_PARMS VALUES ('ACCOUNTING', 'VENDOR_JDE_FTP_NEXT_FILENO', 'u00046','chu',sysdate,'chu',sysdate);

--Defect 8276 Iteration 1
INSERT INTO FRP.GLOBAL_PARMS (context,name,value,created_on,created_by,updated_on,updated_by,description) values 
('FTDAPPS_PARMS','SAME_DAY_UPCHARGE','Y',sysdate,'Defect_8276_Release_5_4_0',sysdate,'Defect_8276_Release_5_4_0','This parm is used to turn ON or OFF the Same Day Upcharge functionality.');
INSERT INTO FRP.GLOBAL_PARMS (context,name,value,created_on,created_by,updated_on,updated_by,description) values 
('FTDAPPS_PARMS','SAME_DAY_UPCHARGE_DISPLAY','Y',sysdate ,'Defect_8276_Release_5_4_0',sysdate,'Defect_8276_Release_5_4_0','This parm is used to determine whether or not to display the Same Day Upcharge as a separate line.');