CREATE OR REPLACE PACKAGE BODY CLEAN.JCPENNY_EOD_PKG
AS


PROCEDURE INSERT_JCP_BILLING_FORM
(
 IN_BILLING_DATE_TIME        IN JCP_BILLING_FORM.BILLING_DATE_TIME%TYPE, 
 IN_JCP_BILLING_FORM_TYPE_ID IN JCP_BILLING_FORM.JCP_BILLING_FORM_TYPE_ID%TYPE, 
 IN_FORM_ID                  IN JCP_BILLING_FORM.FORM_ID%TYPE, 
 IN_COMPLETION_FLAG          IN JCP_BILLING_FORM.COMPLETION_FLAG%TYPE,
 OUT_JCP_BILLING_FORM_ID    OUT JCP_BILLING_FORM.JCP_BILLING_FORM_ID%TYPE,
 OUT_BATCH_DATE_TIMESTAMP   OUT JCP_BILLING_FORM.CREATED_ON%TYPE,
 OUT_STATUS                 OUT VARCHAR2,
 OUT_ERROR_MESSAGE          OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure creates a record in the jcp_billing_form table 
          of the specified type return the jcp_billing_form_id.

Input:
        jcp_billing_form_id       NUMBER
	billing_date_time         DATE
	jcp_billing_form_type_id  VARCHAR2
        form_id                   VARCHAR2
        completion_flag           CHAR

Output:
        jcp_billing_form_id
        status
        error message

-----------------------------------------------------------------------------*/

BEGIN

  SELECT jcp_billing_form_id_sequence.NEXTVAL 
    INTO out_jcp_billing_form_id 
    FROM DUAL;
  
  out_batch_date_timestamp := SYSDATE;

  INSERT INTO jcp_billing_form
     (jcp_billing_form_id, 
      billing_date_time,
      jcp_billing_form_type_id,
      form_id,
      completion_flag,
      created_on)
    VALUES
      (out_jcp_billing_form_id,
       NVL(in_billing_date_time,SYSDATE),
       in_jcp_billing_form_type_id,
       in_form_id,
       in_completion_flag,
       out_batch_date_timestamp);
	
  out_status := 'Y';
  
  EXCEPTION WHEN OTHERS THEN
  BEGIN
    ROLLBACK;
    out_status := 'N';
    out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END INSERT_JCP_BILLING_FORM;


PROCEDURE INSERT_JCP_BILL_DETAILS
(
 IN_ORDER_OUT_BATCH_NUM   IN JCP_BILL_DETAILS.ORDER_OUT_BATCH_NUM%TYPE,
 IN_ORDER_IN_BATCH_NUM    IN JCP_BILL_DETAILS.ORDER_IN_BATCH_NUM%TYPE,
 IN_INVOICE_OUT_BATCH_NUM IN JCP_BILL_DETAILS.INVOICE_OUT_BATCH_NUM%TYPE,
 IN_INVOICE_IN_BATCH_NUM  IN JCP_BILL_DETAILS.INVOICE_IN_BATCH_NUM%TYPE,
 IN_FTD_COM_ID            IN JCP_BILL_DETAILS.FTD_COM_ID%TYPE,
 IN_BILL_TO_NAME          IN JCP_BILL_DETAILS.BILL_TO_NAME%TYPE,
 IN_BILL_TO_ADDRESS_1     IN JCP_BILL_DETAILS.BILL_TO_ADDRESS_1%TYPE,
 IN_BILL_TO_ADDRESS_2     IN JCP_BILL_DETAILS.BILL_TO_ADDRESS_2%TYPE,
 IN_BILL_TO_CITY          IN JCP_BILL_DETAILS.BILL_TO_CITY%TYPE,
 IN_BILL_TO_STATE         IN JCP_BILL_DETAILS.BILL_TO_STATE%TYPE,
 IN_BILL_TO_ZIP           IN JCP_BILL_DETAILS.BILL_TO_ZIP%TYPE,
 IN_BILL_TO_PHONE         IN JCP_BILL_DETAILS.BILL_TO_PHONE%TYPE,
 IN_BILL_TO_EMAIL	  IN JCP_BILL_DETAILS.BILL_TO_EMAIL%TYPE,		    
 IN_RECIPIENT_NAME	  IN JCP_BILL_DETAILS.RECIPIENT_NAME%TYPE,	    
 IN_RECIPIENT_ADDRESS_1	  IN JCP_BILL_DETAILS.RECIPIENT_ADDRESS_1%TYPE,	    
 IN_RECIPIENT_ADDRESS_2	  IN JCP_BILL_DETAILS.RECIPIENT_ADDRESS_2%TYPE,	    
 IN_RECIPIENT_CITY	  IN JCP_BILL_DETAILS.RECIPIENT_CITY%TYPE,	    
 IN_RECIPIENT_STATE       IN JCP_BILL_DETAILS.RECIPIENT_STATE%TYPE,    
 IN_RECIPIENT_ZIP	  IN JCP_BILL_DETAILS.RECIPIENT_ZIP%TYPE,	    
 IN_MASTER_ORDER_NUMBER   IN JCP_BILL_DETAILS.MASTER_ORDER_NUMBER%TYPE,		    
 IN_ORDER_DETAIL_ID	  IN JCP_BILL_DETAILS.ORDER_DETAIL_ID%TYPE,	    
 IN_CC_NUMBER             IN JCP_BILL_DETAILS.CC_NUMBER%TYPE,
 IN_AUTH_CODE             IN JCP_BILL_DETAILS.AUTH_CODE%TYPE,
 IN_LOT_NUMBER            IN JCP_BILL_DETAILS.LOT_NUMBER%TYPE,  
 IN_PRODUCT_DESC	  IN JCP_BILL_DETAILS.PRODUCT_DESC%TYPE,	    
 IN_PRODUCT_PRICE 	  IN JCP_BILL_DETAILS.PRODUCT_PRICE%TYPE,	    
 IN_QUANTITY	          IN JCP_BILL_DETAILS.QUANTITY%TYPE,
 IN_JCP_PON		  IN JCP_BILL_DETAILS.JCP_PON%TYPE,		    
 IN_SUPPLIER_NUMBER	  IN JCP_BILL_DETAILS.SUPPLIER_NUMBER%TYPE,
 IN_PO_REQ_DATETIME	  IN JCP_BILL_DETAILS.PO_REQ_DATETIME%TYPE,	
 IN_FTD_PON		  IN JCP_BILL_DETAILS.FTD_PON%TYPE,		    
 IN_MEDIA_TYPE		  IN JCP_BILL_DETAILS.MEDIA_TYPE%TYPE,		    
 IN_TAX	                  IN JCP_BILL_DETAILS.TAX%TYPE,	    
 IN_OTHER_CHARGES	  IN JCP_BILL_DETAILS.OTHER_CHARGES%TYPE,		    
 IN_INVOICE_NUMBER	  IN JCP_BILL_DETAILS.INVOICE_NUMBER%TYPE,		    
 IN_INVOICE_AMOUNT	  IN JCP_BILL_DETAILS.LINE_NUMBER%TYPE,		    
 IN_LINE_NUMBER		  IN JCP_BILL_DETAILS.LINE_NUMBER%TYPE,
 IN_STORE_NUMBER          IN JCP_BILL_DETAILS.STORE_NUMBER%TYPE,
 IN_REMITTANCE_AMOUNT     IN JCP_BILL_DETAILS.REMITTANCE_AMOUNT%TYPE,
 IN_REMITTANCE_DATE       IN JCP_BILL_DETAILS.REMITTANCE_DATE%TYPE, 
 IN_REMITTANCE_TIME       IN JCP_BILL_DETAILS.REMITTANCE_TIME%TYPE,
 IN_CHECK_REF_NUMBER      IN JCP_BILL_DETAILS.CHECK_REF_NUMBER%TYPE,
 IN_ERROR_INDICATOR       IN JCP_BILL_DETAILS.ERROR_INDICATOR%TYPE,
 IN_COMPLETION_STATE_ID   IN JCP_BILL_DETAILS.JCP_BILL_COMPLETION_STATE_ID%TYPE,
 OUT_STATUS              OUT VARCHAR2,
 OUT_MESSAGE             OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
   This procedure creates record in the jcp_bill_details table.

Input:
   order_out_batch_num		NUMBER
   order_in_batch_num		NUMBER
   invoice_out_batch_num	NUMBER
   invoice_in_batch_num		NUMBER
   ftd_com_id			VARCHAR2
   bill_to_name			VARCHAR2
   bill_to_address_1		VARCHAR2
   bill_to_address_2		VARCHAR2
   bill_to_city			VARCHAR2
   bill_to_state		VARCHAR2
   bill_to_zip			VARCHAR2
   bill_to_phone		VARCHAR2
   bill_to_email		VARCHAR2		    
   recipient_name		VARCHAR2
   recipient_address_1		VARCHAR2
   recipient_address_2		VARCHAR2
   recipient_city		VARCHAR2
   recipient_state		VARCHAR2
   recipient_zip		VARCHAR2
   master_order_number		VARCHAR2
   order_detail_id		NUMBER
   cc_number			VARCHAR2
   auth_code			VARCHAR2
   lot_number			VARCHAR2
   product_desc			VARCHAR2
   product_price		NUMBER
   quantity			VARCHAR2
   jcp_pon			VARCHAR2
   supplier_number		VARCHAR2
   po_req_datetime		DATE
   ftd_pon			VARCHAR2
   media_type			VARCHAR2
   tax				NUMBER
   other_charges		NUMBER
   invoice_number		NUMBER
   invoice_amount		NUMBER
   line_number			VARCHAR2
   store_number			VARCHAR2
   remittance_amount		NUMBER
   remittance_date		VARCHAR2
   remittance_time		VARCHAR2
   check_ref_number		VARCHAR2
   error_indicator              CHAR
   completion_state_id          VARCHAR2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

  INSERT INTO jcp_bill_details
  (   
   jcp_bill_details_id,
   order_out_batch_num,
   order_in_batch_num,
   invoice_out_batch_num,
   invoice_in_batch_num,
   ftd_com_id,
   bill_to_name,
   bill_to_address_1,
   bill_to_address_2,
   bill_to_city,
   bill_to_state,
   bill_to_zip,
   bill_to_phone,
   bill_to_email,		    
   recipient_name,
   recipient_address_1,
   recipient_address_2,
   recipient_city,
   recipient_state,
   recipient_zip,
   master_order_number,
   order_detail_id,
   cc_number,
   auth_code,
   lot_number,
   product_desc,
   product_price,
   quantity,
   jcp_pon,
   supplier_number,
   po_req_datetime,
   ftd_pon,
   media_type,
   tax,
   other_charges,
   invoice_number,
   invoice_amount,
   line_number,
   store_number,
   remittance_amount,
   remittance_date,
   remittance_time,
   check_ref_number,
   error_indicator,
   jcp_bill_completion_state_id
  )
  VALUES
  (     
   jcp_billing_detail_id_sequence.NEXTVAL,
   in_order_out_batch_num,
   in_order_in_batch_num,
   in_invoice_out_batch_num,
   in_invoice_in_batch_num,
   in_ftd_com_id,
   in_bill_to_name,
   in_bill_to_address_1,
   in_bill_to_address_2,
   in_bill_to_city,
   in_bill_to_state,
   in_bill_to_zip,
   in_bill_to_phone,
   in_bill_to_email,		    
   in_recipient_name,
   in_recipient_address_1,
   in_recipient_address_2,
   in_recipient_city,
   in_recipient_state,
   in_recipient_zip,
   in_master_order_number,
   in_order_detail_id,
   in_cc_number,
   in_auth_code,
   in_lot_number,
   in_product_desc,
   in_product_price,
   in_quantity,
   in_jcp_pon,
   in_supplier_number,
   in_po_req_datetime,
   in_ftd_pon,
   in_media_type,
   in_tax,
   in_other_charges,
   in_invoice_number,
   in_invoice_amount,
   in_line_number,
   in_store_number,
   in_remittance_amount,
   in_remittance_date,
   in_remittance_time,
   in_check_ref_number,
   in_error_indicator,
   in_completion_state_id
  );

  OUT_STATUS := 'Y';

  EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_JCP_BILL_DETAILS;


PROCEDURE INSERT_JCP_SINGLE_REJECTS
(
 IN_JCP_BILLING_FORM_ID IN JCP_SINGLE_REJECTS.JCP_BILLING_FORM_ID%TYPE,
 IN_FORM_ID             IN JCP_SINGLE_REJECTS.FORM_ID%TYPE,
 IN_XREF_ID             IN JCP_SINGLE_REJECTS.XREF_ID%TYPE,
 IN_ERROR_CODE          IN JCP_SINGLE_REJECTS.ERROR_CODE%TYPE,
 IN_ERROR_MSG_ID        IN JCP_SINGLE_REJECTS.ERROR_MSG_ID%TYPE,
 OUT_STATUS            OUT VARCHAR2,
 OUT_MESSAGE           OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
   This procedure creates record in the jcp_single_rejects table.

Input:
        jcp_billing_form_id        NUMBER
	form_id                    VARCHAR2
	xref_id                    VARCHAR2
	error_code                 VARCHAR2
	error_msg_id               VARCHAR2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

  INSERT INTO jcp_single_rejects
  (   
   jcp_single_rejects_id,
   jcp_billing_form_id,
   form_id,
   xref_id,
   error_code,
   error_msg_id
  )
  VALUES
  (     
   jcp_single_reject_id_sequence.NEXTVAL,
   in_jcp_billing_form_id,
   in_form_id,
   in_xref_id,
   in_error_code,
   in_error_msg_id
  );

  OUT_STATUS := 'Y';

  EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_JCP_SINGLE_REJECTS;


PROCEDURE UPDATE_JCP_BILL_COMP_STATE
(
 IN_JCP_BILL_DETAILS_ID  IN JCP_BILL_DETAILS.JCP_BILL_DETAILS_ID%TYPE,
 IN_COMPLETION_STATE_ID  IN JCP_BILL_DETAILS.JCP_BILL_COMPLETION_STATE_ID%TYPE,
 OUT_STATUS             OUT VARCHAR2,
 OUT_MESSAGE            OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure updates the jcp_bill_details record's 
        completion state.

Input:
        billing_detail_id          number
        completion_state_id        varchar2

Output:
        status                     varchar2 (Y or N)
        message                    varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

  UPDATE jcp_bill_details
     SET jcp_bill_completion_state_id  = in_completion_state_id             
   WHERE jcp_bill_details_id = in_jcp_bill_details_id;

  IF SQL%FOUND THEN
       out_status := 'Y';
  ELSE
       out_status := 'N';
       out_message := 'WARNING: No jcp_bill_details records updated for jcp_bill_details_id ' || in_jcp_bill_details_id ||'.';
  END IF;
  
  EXCEPTION WHEN OTHERS THEN
    BEGIN
      out_status := 'N';
      out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END UPDATE_JCP_BILL_COMP_STATE;


PROCEDURE UPDATE_JCP_BILL_DETL_ERR_STAT
(
 IN_ORDER_DETAIL_ID IN JCP_BILL_DETAILS.ORDER_DETAIL_ID%TYPE,
 IN_ERROR_INDICATOR IN JCP_BILL_DETAILS.ERROR_INDICATOR%TYPE,
 OUT_STATUS        OUT VARCHAR2,
 OUT_MESSAGE       OUT VARCHAR2
)
 AS
 /*-----------------------------------------------------------------------------
 Description:
         This procedure updates jcp_bill_details' error_indicator to 
         status for the given order_detail_id passed in.
 
 Input:
         order_detail_id       number
         error_indicator       varchar2
 
 Output:
         status                          varchar2 (Y or N)
         message                         varchar2 (error message)
 -----------------------------------------------------------------------------*/
  
 BEGIN
 
   UPDATE jcp_bill_details
      SET error_indicator = in_error_indicator            
    WHERE order_detail_id = in_order_detail_id;
 
   IF SQL%FOUND THEN
      out_status := 'Y';
   ELSE
      out_status := 'N';
      out_message := 'WARNING: No jcp_bill_details records updated for order_detail_id ' || in_order_detail_id ||'.';
   END IF;
     

   EXCEPTION 
     WHEN NO_DATA_FOUND THEN
     BEGIN
      out_status := 'N';
      out_message := 'WARNING: No records found for jcp_bill_details with an order_detail_id of ' || in_order_detail_id ||'.';
     END;
 
     WHEN OTHERS THEN
     BEGIN
       out_status := 'N';
       out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
     END;
 
END UPDATE_JCP_BILL_DETL_ERR_STAT;


PROCEDURE UPDATE_JCP_BILL_BATCH_NUMBERS
(
 IN_JCP_BILL_DETAILS_ID   IN JCP_BILL_DETAILS.JCP_BILL_DETAILS_ID%TYPE,
 IN_ORDER_IN_BATCH_NUM    IN JCP_BILL_DETAILS.ORDER_IN_BATCH_NUM%TYPE,
 IN_INVOICE_OUT_BATCH_NUM IN JCP_BILL_DETAILS.INVOICE_OUT_BATCH_NUM%TYPE,
 IN_INVOICE_IN_BATCH_NUM  IN JCP_BILL_DETAILS.INVOICE_IN_BATCH_NUM%TYPE,
 OUT_STATUS    OUT VARCHAR2,
 OUT_MESSAGE   OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure receives one of the three batch numbers in the 
        parameter list and updates the jcp_billing_detail record with 
        the specified billing_detail_id.

Input:
        jcp_bill_details_id             NUMBER
        order_in_batch_num		NUMBER
        invoice_out_batch_num		NUMBER
        invoice_in_batch_num		NUMBER

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

  IF in_order_in_batch_num IS NOT NULL THEN
      UPDATE jcp_bill_details
         SET order_in_batch_num  = in_order_in_batch_num
       WHERE jcp_bill_details_id = in_jcp_bill_details_id;
  ELSIF in_invoice_out_batch_num IS NOT NULL THEN
      UPDATE jcp_bill_details
         SET invoice_out_batch_num = in_invoice_out_batch_num
       WHERE jcp_bill_details_id = in_jcp_bill_details_id;
  ELSIF in_invoice_in_batch_num IS NOT NULL THEN
      UPDATE jcp_bill_details
         SET invoice_in_batch_num = in_invoice_in_batch_num
       WHERE jcp_bill_details_id = in_jcp_bill_details_id;
  END IF;

  IF SQL%FOUND THEN
       out_status := 'Y';
  ELSE
       out_status := 'N';
       out_message := 'WARNING: No jcp_bill_details records updated for jcp_bill_details_id ' || in_jcp_bill_details_id ||'.';
  END IF;
  
  EXCEPTION WHEN OTHERS THEN
    BEGIN
      out_status := 'N';
      out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END UPDATE_JCP_BILL_BATCH_NUMBERS;


PROCEDURE GET_JCP_LOT
(
 IN_PRICE_POINT  IN JCP_LOT.PRICE_POINT%TYPE, 
 IN_JCP_CATEGORY IN JCP_LOT.JCP_CATEGORY%TYPE,
 OUT_CURSOR     OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
 Description:
    This procedure returns the lot number and cost for the 
      given price point and category.
 
 Input:
         price_point
         category
 
 Output:
         cursor containing jcp_lot information
 
-----------------------------------------------------------------------------*/
 
 BEGIN
 
   OPEN out_cursor FOR
       SELECT lot_number,
              cost
         FROM jcp_lot
        WHERE price_point = in_price_point
          AND jcp_category = in_jcp_category;
 
 
 END GET_JCP_LOT;
 
 
 FUNCTION GET_JCP_RECP_NAME_BY_PAY_ID
 (
  IN_PAYMENT_ID    IN NUMBER
 )
 RETURN VARCHAR2
 IS
 /*-----------------------------------------------------------------------------
 Description:
         This stored procedure the first and last name of the recipient
         related to the payment id passed in.
         
         Note an assumption is made that JCPenny orders will always only
         have one recipient per order.
 
 Input:
         payment_id             	NUMBER
 
 Output:
         varchar2
 
 -----------------------------------------------------------------------------*/
 v_first_name customer.first_name%TYPE;
 v_last_name customer.last_name%TYPE;
 v_name VARCHAR2(70);
 
 BEGIN
 
   BEGIN
     SELECT c.first_name,
            c.last_name
       INTO v_first_name,
            v_last_name
       FROM customer c,
            order_details od,
            payments p
      WHERE p.payment_id = in_payment_id
        AND od.order_guid = p.order_guid
        AND od.recipient_id = c.customer_id;
        
      v_name := v_first_name || ' ' || v_last_name;
   
     EXCEPTION WHEN NO_DATA_FOUND THEN v_name := NULL;
   END;
   
   RETURN v_name;
 
END GET_JCP_RECP_NAME_BY_PAY_ID;


PROCEDURE GET_JCP_BILL_DETAILS
(
 IN_COMPLETION_STATE_ID  IN JCP_BILL_DETAILS.JCP_BILL_COMPLETION_STATE_ID%TYPE,
 OUT_CURSOR             OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
   This procedure retrieves all records from the jcp_bill_details table 
     that are in the specified completion state and not in error.

Input:
        completion_state_id  VARCHAR2

Output:
        cursor  containing the jcp_bill_details records

-----------------------------------------------------------------------------*/

BEGIN

  OPEN out_cursor FOR
       SELECT jcp_bill_details_id,
              order_out_batch_num,
              order_in_batch_num,
              invoice_out_batch_num,
              invoice_in_batch_num,
              ftd_com_id,
              bill_to_name,
              bill_to_address_1,
              bill_to_address_2,
              bill_to_city,
              bill_to_state,
              bill_to_zip,
              bill_to_phone,    
              bill_to_email,		
              recipient_name,
              recipient_address_1,
              recipient_address_2,
              recipient_city,
              recipient_state,
              recipient_zip,
              master_order_number,
              order_detail_id,
              cc_number,
              auth_code,
              lot_number,
              product_desc,
              product_price,
              quantity,
              jcp_pon,
              supplier_number,
              po_req_datetime,
              ftd_pon,
              media_type,
              tax,
              other_charges,
              invoice_number,
              invoice_amount,
              line_number,
	      store_number,
	      remittance_amount,
	      remittance_date,
              remittance_time,
              check_ref_number,
	      error_indicator,
	      jcp_bill_completion_state_id
         FROM jcp_bill_details
        WHERE jcp_bill_completion_state_id = in_completion_state_id
          AND error_indicator <> 'Y';


END GET_JCP_BILL_DETAILS;


PROCEDURE GET_JCP_BILL_DETAILS_BY_ORDER
(
 IN_ORDER_DETAIL_ID  IN JCP_BILL_DETAILS.ORDER_DETAIL_ID%TYPE,
 OUT_CURSOR         OUT TYPES.REF_CURSOR
)
AS
 /*-----------------------------------------------------------------------------
 Description:
    This procedure retrieves all records from the jcp_bill_details 
    table for the given order.
 
 Input:
         order_detail_id  VARCHAR2
 
 Output:
         cursor  containing the jcp_bill_details records
 
 -----------------------------------------------------------------------------*/
 
 BEGIN
 
   OPEN out_cursor FOR
       SELECT jcp_bill_details_id,
              order_out_batch_num,
              order_in_batch_num,
              invoice_out_batch_num,
              invoice_in_batch_num,
              ftd_com_id,
              bill_to_name,
              bill_to_address_1,
              bill_to_address_2,
              bill_to_city,
              bill_to_state,
              bill_to_zip,
              bill_to_phone,    
              bill_to_email,		
              recipient_name,
              recipient_address_1,
              recipient_address_2,
              recipient_city,
              recipient_state,
              recipient_zip,
              master_order_number,
              order_detail_id,
              cc_number,
              auth_code,
              lot_number,
              product_desc,
              product_price,
              quantity,
              jcp_pon,
              supplier_number,
              po_req_datetime,
              ftd_pon,
              media_type,
              tax,
              other_charges,
              invoice_number,
              invoice_amount,
              line_number,
	      store_number,
	      remittance_amount,
	      remittance_date,
              remittance_time,
              check_ref_number,
	      error_indicator,
	      jcp_bill_completion_state_id
         FROM jcp_bill_details
        WHERE order_detail_id = in_order_detail_id;
 
 
END GET_JCP_BILL_DETAILS_BY_ORDER;


PROCEDURE UPDATE_JCP_BILL_DET_BY_ORDER
(
 IN_ORDER_DETAIL_ID   IN JCP_BILL_DETAILS.ORDER_DETAIL_ID%TYPE,
 IN_REMITTANCE_DATE   IN JCP_BILL_DETAILS.REMITTANCE_DATE%TYPE, 
 IN_REMITTANCE_TIME   IN JCP_BILL_DETAILS.REMITTANCE_TIME%TYPE,
 IN_REMITTANCE_AMOUNT IN JCP_BILL_DETAILS.REMITTANCE_AMOUNT%TYPE,
 IN_CHECK_REF_NUMBER  IN JCP_BILL_DETAILS.CHECK_REF_NUMBER%TYPE,
 OUT_STATUS          OUT VARCHAR2,
 OUT_MESSAGE         OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure updates four columns in table jcp_bill_details
          of all records for the specified order_detail_id.

Input:
        order_detail_id         NUMBER
        remittance_date         VARCHAR2  
        remittance_time         VARCHAR2
        remittance_amount	NUMBER       
        check_ref_number	VARCHAR2

Output:
        status                     varchar2 (Y or N)
        message                    varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

  UPDATE jcp_bill_details
     SET remittance_date   = in_remittance_date,
         remittance_time   = in_remittance_time,
         remittance_amount = in_remittance_amount,
         check_ref_number  = in_check_ref_number             
   WHERE order_detail_id = in_order_detail_id;

  IF SQL%FOUND THEN
       out_status := 'Y';
  ELSE
       out_status := 'N';
       out_message := 'WARNING: No jcp_bill_details records updated for order_detail_id ' || in_order_detail_id ||'.';
  END IF;
  
  EXCEPTION WHEN OTHERS THEN
    BEGIN
      out_status := 'N';
      out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END UPDATE_JCP_BILL_DET_BY_ORDER;


PROCEDURE GET_JCP_RECEIVED_ORDERS
(
 OUT_JCP_ORDERS_CURSOR   OUT TYPES.REF_CURSOR,
 OUT_ORDER_BILLS_CURSOR  OUT TYPES.REF_CURSOR,
 OUT_ADDON_CURSOR        OUT TYPES.REF_CURSOR
) 
AS	
/*-----------------------------------------------------------------------------
Description:
   This procedure retrieves three cursors for all JC Penney orders that 
   are ready to be billed on or before the specified date.

Input:
        N/A        

Output:
        cursor containing JC Penney orders
        cursor containing JC Penney order_bills
        cursor containing JC Penney addon

-----------------------------------------------------------------------------*/

BEGIN

  INSERT INTO temp_jcp_orders
          SELECT o.master_order_number,
                 o.order_guid,
                 o.customer_id,
                 o.email_id,
                 od.order_detail_id,
                 od.product_id,
                 od.recipient_id,
                 s.order_source
            FROM orders o, 
                 ftd_apps.source s,
                 order_details od
           WHERE s.source_code = o.source_code
             AND s.jcpenney_flag = 'Y'
             AND od.order_guid = o.order_guid
             AND od.order_disp_code = 'Processed';

  OPEN out_jcp_orders_cursor FOR
          SELECT tjo.master_order_number,
                 tjo.order_detail_id,
                 global.encryption.decrypt_it(cc.cc_number) cc_number,
                 billing_end_of_day_pkg.get_cust_name_by_payment_id(p.payment_id),
                 buy.address_1,
                 buy.address_2,
                 buy.city,
                 buy.state,
                 buy.zip_code,
                 buy_cp.phone_number,
                 buy_e.email_address,
                 get_jcp_recp_name_by_pay_id(p.payment_id),
                 rec.address_1,
                 rec.address_2,
                 rec.city,
                 rec.state,
                 rec.zip_code,
                 rec_cp.phone_number,
                 p.payment_id,
                 pm.product_name,
                 pm.jcp_category,
                 tjo.order_source
            FROM customer_phones rec_cp, 
                 customer rec,
                 ftd_apps.product_master pm,
                 credit_cards cc,
                 payments p,
	         email buy_e,
                 customer_phones buy_cp,
                 customer buy,
                 temp_jcp_orders tjo                 
           WHERE buy.customer_id = tjo.customer_id
             AND buy_cp.customer_id (+) = tjo.customer_id
             AND buy_cp.phone_type = 'Day'
             AND buy_e.email_id (+) = tjo.email_id
             AND p.order_guid = tjo.order_guid
             AND cc.cc_id (+) = p.cc_id
             AND pm.product_id (+) = tjo.product_id
             AND rec.customer_id (+) = tjo.recipient_id
             AND rec_cp.customer_id (+) = tjo.recipient_id
             AND rec_cp.phone_type = 'Day';


  OPEN out_order_bills_cursor FOR
          SELECT ob.additional_bill_indicator,
                 ob.product_amount,
                 ob.add_on_amount,
                 ob.discount_amount,
                 ob.service_fee,
                 ob.shipping_fee,
                 ob.same_day_fee
            FROM order_bills ob
           WHERE ob.order_detail_id IN (SELECT order_detail_id FROM temp_jcp_orders)
	     AND ob.bill_status = 'Unbilled';


  OPEN out_addon_cursor FOR
          SELECT aot.description, 
                 ao.description,
                 ao.price
            FROM ftd_apps.addon_type aot,
                 ftd_apps.addon ao,
                 order_add_ons oao
           WHERE oao.order_detail_id IN (SELECT order_detail_id FROM temp_jcp_orders)
	     AND ao.addon_id = oao.add_on_code
	     AND ao.addon_type = aot.addon_type_id;



END GET_JCP_RECEIVED_ORDERS;


				    			  
				    			  
END;
.
/