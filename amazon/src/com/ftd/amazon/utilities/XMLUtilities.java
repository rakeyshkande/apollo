package com.ftd.amazon.utilities;

import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.JAXPException;
import com.ftd.osp.utilities.xml.JAXPUtil;

import java.io.File;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.lang.StringUtils;

import org.apache.xpath.XPathAPI;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

public class XMLUtilities {
    /**
     * Returns the XML file as Document object.
     * @param instc
     * @param file
     * @return
     * @throws Exception
     */
    public static Document getFileDocument(Object instc, String file) throws Exception {
        Document doc = JAXPUtil.parseDocument(instc.getClass().getClassLoader().getResourceAsStream(file));
        return doc;
    }

    /**
     * Returns the XML file as Document object.
     * @param file
     * @return
     * @throws Exception
     */
    public static Document getFileDocument(String file) throws Exception {
        Class c = Class.forName("com.ftd.amazon.api.utilities.XMLUtilities");
        Document doc = JAXPUtil.parseDocument(c.getClassLoader().getResourceAsStream(file));
        return doc;
    }
    
    /**
     * Creates an Element with the input tagName and nodeValue to the input doc.
     * @param doc
     * @param tagName
     * @param nodeValue
     * @return Elment
     * @throws Exception
     */
    public static Element createElement(Document doc, String tagName, String nodeValue) throws Exception{
        Element e = doc.createElement(tagName);
        Text t = doc.createTextNode(nodeValue);
        e.appendChild(t);
        return e;
    }
    
    /**
     * Creates a Document and its root element with the given tagName 
     * @param tagName
     * @return Document
     * @throws Exception
     */
    public static Document createDocumentWithRoot(String tagName) throws Exception {
        try {
            Document doc = JAXPUtil.createDocument();
            Element root = doc.createElement(tagName);
            doc.appendChild(root);
            return doc;
        } catch (JAXPException je) {
            throw new Exception(je.getMessage());
        }
       
    }     
    
    /**
     * Retrieves a single node text for the given node and x-path string.
     * @param n
     * @param xpathString
     * @return String 
     * @throws Exception
     */
    public static String selectSingleNodeText(Node n, String xpathString ) throws Exception
    {
       String retval = "";
       Node tElement = (Node)XPathAPI.selectSingleNode(n,xpathString);
       if( tElement!=null ) 
       {
            Text t = (Text)tElement.getFirstChild();
            if(t != null) {
                retval = StringUtils.trimToEmpty(t.getNodeValue());
            }
       }
       
       return retval;
    }  
    
    /**
     * Retrieve the NodeList from the input Node for the given x-path string
     * @param n
     * @param xpathString
     * @return
     * @throws Exception
     */
    public static NodeList selectXPathNodes(Node n, String xpathString ) throws Exception
    {
       return XPathAPI.selectNodeList(n, xpathString);
       
    }    
    
    /**
     * Select single node from the input node with the given x-path string.
     * @param n
     * @param xpathString
     * @return
     * @throws Exception
     */
    public static Element selectSingleNode(Node n, String xpathString ) throws Exception
    {
 
       Element tElement = (Element)XPathAPI.selectSingleNode(n,xpathString);
       
       return tElement;
    }      
        
}
