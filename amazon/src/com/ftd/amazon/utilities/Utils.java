package com.ftd.amazon.utilities;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.commons.lang.StringUtils;

/**
 * This is a class of static utility methods
 */
public class Utils 
{   
  /**
   * Converts an ISO date string to a java.util.Date object
   * The date formatter in jdk 1.4 interprets the timezone 'Z' formatter as
   * -hh:mm.  The ISO standared which Amazon uses sends the timezone over as
   * -hhmm.
   * @param azDateString string to parse
   * @return converted date object
   * @throws java.text.ParseException
   */
     public static java.sql.Date azDateString2Date(String azDateString) throws ParseException
     {
        String strDate = StringUtils.substringBeforeLast(azDateString,":") + StringUtils.substringAfterLast(azDateString,":");
        
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        java.util.Date date = format.parse(strDate);
        return new java.sql.Date(date.getTime());
     }
    
  /**
   * Converts a java.sql.Date to an ISO date string.
   * The date formatter in jdk 1.4 interprets the timezone 'Z' formatter as
   * -hhmm.  The ISO standared which Amazon uses sends the timezone over as
   * -hh:mm.
   * @param date date to parse
   * @return converted string object
   * @throws java.text.ParseException
   */
    public static String sqlDate2AzDateString(java.sql.Date date) throws ParseException
    {
      String retval=null;
      SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
      if( format!=null )
      {
      	java.util.Date testDate = new java.util.Date(date.getTime());
      	if( testDate!=null )
      	{
			String strTmp = format.format(testDate);
			retval = StringUtils.substring(strTmp,0,strTmp.length()-2) + ":" + StringUtils.right(strTmp,2);
      	}
      }
      
      return retval;
    }
    
  /**
   * Tests a string for boolean true.  Valid true strings are:
   * "T", "TRUE", "Y", "YES", and "1".  Passed string of null returns false;
   * Evaluation ignores case.
   * @param strBool string to evaluate
   * @return evaluation results
   */
    public static boolean isTrue( String strBool ) 
    {
      if( strBool==null )
        return false;
      
      if( strBool.equalsIgnoreCase("T") || strBool.equalsIgnoreCase("TRUE") ||
          strBool.equalsIgnoreCase("Y") || strBool.equalsIgnoreCase("YES") ||
          strBool.equalsIgnoreCase("1") )
        return true;
        
      return false;
    }

    public static String removeAllSpecialChars(String inStr)
    {
        StringBuffer sb = new StringBuffer();
        for(int i = 0; i < inStr.length(); i++)
        {
            if(Character.isLetterOrDigit(inStr.charAt(i)))
            {
                sb.append(inStr.charAt(i));
            }
        }

        return sb.toString();
    }

}