/**
 * 
 */
package com.ftd.amazon.vo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.amazonaws.mws.model.FeedProcessingStatus;
import com.amazonaws.mws.model.FeedType;
import com.amazonaws.mws.model.IdList;
import com.amazonaws.mws.model.StatusList;
import com.amazonaws.mws.model.TypeList;

/**
 * @author skatam
 *
 */
public class FeedStatusRequestVO 
{
	private Set<String> feedSubmissionIdList = new HashSet<String>();
	private Set<FeedProcessingStatus> statusTypes= new HashSet<FeedProcessingStatus>();
	private Set<FeedType> feedTypes= new HashSet<FeedType>();
	
	public Set<FeedProcessingStatus> getStatusTypes() {
		return statusTypes;
	}
	public void addStatusType(FeedProcessingStatus statusType) {
			this.getStatusTypes().add(statusType);
	}
	public Set<FeedType> getFeedTypes() {
		return feedTypes;
	}
	
	public void addFeedType(FeedType feedType) {
			this.getFeedTypes().add(feedType);
	}
	
	public Set<String> getFeedSubmissionIdList() {
		return feedSubmissionIdList;
	}
		
	public void addFeedSubmissionId(String feedSubmissionId) {
		this.getFeedSubmissionIdList().add(feedSubmissionId);
	}
	
	public StatusList getStatusList() 
	{
		List<String> statusTypesList = null;
		if(statusTypes != null && !statusTypes.isEmpty()) {
			statusTypesList = new ArrayList<String>();
			for (FeedProcessingStatus statusType : statusTypes) {
				//if(!statusTypesList.contains(statusType.value()))
				{
					statusTypesList.add(statusType.value());
				}
			}			
		}
		return new StatusList(statusTypesList);
	}
	
	public TypeList getTypeList() 
	{
		List<String> feedTypesList = null;
		if(feedTypes != null && !feedTypes.isEmpty()) {
			feedTypesList = new ArrayList<String>();
			for (FeedType feedType : feedTypes) {
				//if(!feedTypesList.contains(feedType.value()))
				{
					feedTypesList.add(feedType.value());
				}
			}
		}
		return new TypeList(feedTypesList);
	}
	
	public IdList getIdList() {
		List<String> feedIdList = null;
		if(feedSubmissionIdList != null && !feedSubmissionIdList.isEmpty()){
			feedIdList = new ArrayList<String>();
		}
		for (String	id : feedSubmissionIdList) {
			//if(!feedIdList.contains(id))
			{
				feedIdList.add(id);
			}
		}
		return new IdList(feedIdList);
		
	}

}
