/**
 * 
 */
package com.ftd.amazon.vo;

import java.math.BigDecimal;
import java.util.Date;

public class SettlementDetailVO 
{
	private String settlementId;
	private String amazonOrderNumber;
	private String amazonItemNumber;
	private String transactionType;
	private String transactionName;
	private BigDecimal transactionAmount;
	private Date postedDate;

	public String getSettlementId() {
		return settlementId;
	}
	public void setSettlementId(String settlementId) {
		this.settlementId = settlementId;
	}

	public String getAmazonOrderNumber() {
		return amazonOrderNumber;
	}
	public void setAmazonOrderNumber(String amazonOrderNumber) {
		this.amazonOrderNumber = amazonOrderNumber;
	}
	
	public String getAmazonItemNumber() {
		return amazonItemNumber;
	}
	public void setAmazonItemNumber(String amazonItemNumber) {
		this.amazonItemNumber = amazonItemNumber;
	}
	
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	
	public void setPostedDate(Date postedDate) {
		this.postedDate = postedDate;
	}
	public Date getPostedDate() {
		return postedDate;
	}
	
	public void setTransactionName(String transactionName) {
		this.transactionName = transactionName;
	}
	public String getTransactionName() {
		return transactionName;
	}
	
	public void setTransactionAmount(BigDecimal transactionAmount) {
		this.transactionAmount = transactionAmount;
	}
	public BigDecimal getTransactionAmount() {
		return transactionAmount;
	}
	
}
