/**
 * 
 */
package com.ftd.amazon.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author skatam
 *
 */
public class InventoryFeedVO 
{
	private List<InventoryFeedDetailVO> feedDetails = new ArrayList<InventoryFeedDetailVO>();

	public List<InventoryFeedDetailVO> getFeedDetails() {
		if(feedDetails == null){
			this.feedDetails = new ArrayList<InventoryFeedDetailVO>();
		}
		return feedDetails;
	}

	public void setFeedDetails(List<InventoryFeedDetailVO> feedDetails) {
		this.feedDetails = feedDetails;
	}
	public void addFeedDetail(InventoryFeedDetailVO feedDetail) {
		this.feedDetails.add(feedDetail);
	}
	
	
}
