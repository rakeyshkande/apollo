/**
 * 
 */
package com.ftd.amazon.vo;

/**
 * @author skatam
 *
 */
public class OrderAckFeedVO extends BaseFeedDetailVO
{
	private String azOrderID;
	private String masterOrderNumber;
	private String confirmationNumber;
	private String azOrderAckId;
	private String azOrderItemNumber;
	private String statusCode;
	private String cancelReason;
	
	public String getAzOrderAckId() {
		return azOrderAckId;
	}
	public void setAzOrderAckId(String azOrderAckId) {
		this.azOrderAckId = azOrderAckId;
	}
	public String getAzOrderItemNumber() {
		return azOrderItemNumber;
	}
	public void setAzOrderItemNumber(String azOrderItemNumber) {
		this.azOrderItemNumber = azOrderItemNumber;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getCancelReason() {
		return cancelReason;
	}
	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}
	public String getAzOrderID() {
		return azOrderID;
	}
	public void setAzOrderID(String azOrderID) {
		this.azOrderID = azOrderID;
	}
	public String getConfirmationNumber() {
		return confirmationNumber;
	}
	public void setConfirmationNumber(String confirmationNumber) {
		this.confirmationNumber = confirmationNumber;
	}
	public String getMasterOrderNumber() {
		return masterOrderNumber;
	}
	public void setMasterOrderNumber(String masterOrderNumber) {
		this.masterOrderNumber = masterOrderNumber;
	}
	
}
