package com.ftd.amazon.vo;

import java.math.BigDecimal;

public class PriceFeedDetailVO extends BaseFeedDetailVO{

	private String priceFeedId;
	private BigDecimal standardPrice;
	private BigDecimal deluxePrice;
	private BigDecimal premiumPrice;
	private ProductMasterVO productFeedStatus;
	
	public void setPriceFeedId(String priceFeedId) {
		this.priceFeedId = priceFeedId;
	}
	public String getPriceFeedId() {
		return priceFeedId;
	}
	public ProductMasterVO getProductFeedStatus() {
		return productFeedStatus;
	}
	public void setProductFeedStatus(ProductMasterVO productFeedStatus) {
		this.productFeedStatus = productFeedStatus;
	}
	public void setStandardPrice(BigDecimal standardPrice) {
		this.standardPrice = standardPrice;
	}
	public BigDecimal getStandardPrice() {
		return standardPrice;
	}
	public void setDeluxePrice(BigDecimal deluxePrice) {
		this.deluxePrice = deluxePrice;
	}
	public BigDecimal getDeluxePrice() {
		return deluxePrice;
	}
	public void setPremiumPrice(BigDecimal premiumPrice) {
		this.premiumPrice = premiumPrice;
	}
	public BigDecimal getPremiumPrice() {
		return premiumPrice;
	}
}
