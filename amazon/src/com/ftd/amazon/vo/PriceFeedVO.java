package com.ftd.amazon.vo;

import java.util.ArrayList;
import java.util.List;

public class PriceFeedVO {
	
	private List<PriceFeedDetailVO> feedDetails = new ArrayList<PriceFeedDetailVO>();

	public void setFeedDetails(List<PriceFeedDetailVO> feedDetails) {
		this.feedDetails = feedDetails;
	}

	public List<PriceFeedDetailVO> getFeedDetails() {
		return feedDetails;
	}
	
	public void addFeedDetail(PriceFeedDetailVO feedDetail){
		this.feedDetails.add(feedDetail);
	}
	
}
