/**
 * 
 */
package com.ftd.amazon.vo;


/**
 * @author skatam
 *
 */
public class ImageFeedDetailVO extends BaseFeedDetailVO
{
	private String imageFeedId;
	private String sku;
	private String imageType;
	private String imageLocation;
	private String novatorId;
	private ProductMasterVO productFeedStatus;
	
	public String getImageFeedId() {
		return imageFeedId;
	}
	public void setImageFeedId(String imageFeedId) {
		this.imageFeedId = imageFeedId;
	}
	public String getImageType() {
		return imageType;
	}
	public void setImageType(String imageType) {
		this.imageType = imageType;
	}
	public String getImageLocation() {
		return imageLocation;
	}
	public void setImageLocation(String imageLocation) {
		this.imageLocation = imageLocation;
	}
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public void setNovatorId(String novatorId) {
		this.novatorId = novatorId;
	}
	public String getNovatorId() {
		return novatorId;
	}
	public void setProductFeedStatus(ProductMasterVO productFeedStatus) {
		this.productFeedStatus = productFeedStatus;
	}
	public ProductMasterVO getProductFeedStatus() {
		return productFeedStatus;
	}
}
