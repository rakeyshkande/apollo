package com.ftd.amazon.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author sbring
 *
 */
public class OrderFulfillmentFeedVO {
	
	private List<OrderFulfillmentFeedDetailVO> feedDetails = new ArrayList<OrderFulfillmentFeedDetailVO>();
	
	public void addFeedDetail(OrderFulfillmentFeedDetailVO feedDetail){
		this.feedDetails.add(feedDetail);
	}

	public void setFeedDetails(List<OrderFulfillmentFeedDetailVO> feedDetails) {
		this.feedDetails = feedDetails;
	}

	public List<OrderFulfillmentFeedDetailVO> getFeedDetails() {
		return feedDetails;
	}
	
}
