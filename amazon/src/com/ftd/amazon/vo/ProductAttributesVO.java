/**
 * 
 */
package com.ftd.amazon.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author skatam
 * 
 */
public class ProductAttributesVO {

	protected List<String> bulletPoint;
	protected List<String> searchTerms;
	protected List<String> usedFor;
	protected List<String> otherItemAttributes;
	protected List<String> targetAudience;
	protected List<String> subjectContent;

	public List<String> getBulletPoint() {
		if (bulletPoint == null) {
			bulletPoint = new ArrayList<String>();
		}
		return this.bulletPoint;
	}

	public List<String> getSearchTerms() {
		if (searchTerms == null) {
			searchTerms = new ArrayList<String>();
		}
		return this.searchTerms;
	}

	public List<String> getUsedFor() {
		if (usedFor == null) {
			usedFor = new ArrayList<String>();
		}
		return this.usedFor;
	}

	public List<String> getTargetAudience() {
		if (targetAudience == null) {
			targetAudience = new ArrayList<String>();
		}
		return this.targetAudience;
	}

	public List<String> getOtherItemAttributes() {
		if (otherItemAttributes == null) {
			otherItemAttributes = new ArrayList<String>();
		}
		return otherItemAttributes;
	}

	public void setOtherItemAttributes(List<String> otherItemAttributes) {
		this.otherItemAttributes = otherItemAttributes;
	}

	public List<String> getSubjectContent() {
		if (subjectContent == null) {
			subjectContent = new ArrayList<String>();
		}
		return subjectContent;
	}

	public void setSubjectContent(List<String> subjectContent) {
		this.subjectContent = subjectContent;
	}

}
