package com.ftd.amazon.vo;

import java.sql.Date;

/**
 * @author sbring
 *
 */
public class OrderFulfillmentFeedDetailVO extends BaseFeedDetailVO{

	private String orderFulfillmentId;
	private String amazonOrderId;
	private String orderItemNumber;
	private Date fulfillmentDate;
	private String shippingMethod;
	private String carrierName; 
	private String trackingNumber; 

	public void setOrderFulfillmentId(String orderFulfillmentId) {
		this.orderFulfillmentId = orderFulfillmentId;
	}
	public String getOrderFulfillmentId() {
		return orderFulfillmentId;
	}
	public void setAmazonOrderId(String amazonOrderId) {
		this.amazonOrderId = amazonOrderId;
	}
	public String getAmazonOrderId() {
		return amazonOrderId;
	}
	public void setOrderItemNumber(String orderItemNumber) {
		this.orderItemNumber = orderItemNumber;
	}
	public String getOrderItemNumber() {
		return orderItemNumber;
	}
	public Date getFulfillmentDate() {
		return fulfillmentDate;
	}
	public void setFulfillmentDate(Date fulfillmentDate) {
		this.fulfillmentDate = fulfillmentDate;
	}
	public void setShippingMethod(String shippingMethod) {
		this.shippingMethod = shippingMethod;
	}
	public String getShippingMethod() {
		return shippingMethod;
	}
	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}
	public String getCarrierName() {
		return carrierName;
	}
	public void setTrackingNumber(String trackingNumber) {
		this.trackingNumber = trackingNumber;
	}
	public String getTrackingNumber() {
		return trackingNumber;
	}

}
