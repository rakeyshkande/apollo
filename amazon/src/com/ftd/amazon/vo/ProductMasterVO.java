/**
 * 
 */
package com.ftd.amazon.vo;

import java.sql.Date;

import com.ftd.amazon.common.AmazonConstants;

/**
 * @author skatam
 *
 */
public class ProductMasterVO 
{
	private String productId;
	private String catalogStatusStandard;
	private boolean sentToAmazonStandardFlag;
	private String catalogStatusDeluxe;
	private boolean sentToAmazonDeluxeFlag;
	private String catalogStatusPremium;
	private boolean sentToAmazonPremiumFlag;
	private String productMasterStatus;
	
	private String productNameStandard;
	private String productDescriptionStandard;
	private String productNameDeluxe;
	private String productDescriptionDeluxe;
	private String productNamePremium;
	private String productDescriptionPremium;
	private String itemType;
	
	private Date createdOn;
	
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getCatalogStatusStandard() {
		return catalogStatusStandard;
	}
	public void setCatalogStatusStandard(String catalogStatusStandard) {
		this.catalogStatusStandard = catalogStatusStandard;
	}
	public boolean isSentToAmazonStandardFlag() {
		return sentToAmazonStandardFlag;
	}
	public void setSentToAmazonStandardFlag(boolean sentToAmazonStandardFlag) {
		this.sentToAmazonStandardFlag = sentToAmazonStandardFlag;
	}
	public String getCatalogStatusDeluxe() {
		return catalogStatusDeluxe;
	}
	public void setCatalogStatusDeluxe(String catalogStatusDeluxe) {
		this.catalogStatusDeluxe = catalogStatusDeluxe;
	}
	public boolean isSentToAmazonDeluxeFlag() {
		return sentToAmazonDeluxeFlag;
	}
	public void setSentToAmazonDeluxeFlag(boolean sentToAmazonDeluxeFlag) {
		this.sentToAmazonDeluxeFlag = sentToAmazonDeluxeFlag;
	}
	public String getCatalogStatusPremium() {
		return catalogStatusPremium;
	}
	public void setCatalogStatusPremium(String catalogStatusPremium) {
		this.catalogStatusPremium = catalogStatusPremium;
	}
	public boolean isSentToAmazonPremiumFlag() {
		return sentToAmazonPremiumFlag;
	}
	public void setSentToAmazonPremiumFlag(boolean sentToAmazonPremiumFlag) {
		this.sentToAmazonPremiumFlag = sentToAmazonPremiumFlag;
	}
	
	public boolean isCatalogStatusStandardActive(){
		return AmazonConstants.CATALOG_STATUS_ACTIVE_VALUE.equalsIgnoreCase(catalogStatusStandard);
	}
	public boolean isCatalogStatusDeluxeActive(){
		return AmazonConstants.CATALOG_STATUS_ACTIVE_VALUE.equalsIgnoreCase(catalogStatusDeluxe);
	}
	public boolean isCatalogStatusPremiumActive(){
		return AmazonConstants.CATALOG_STATUS_ACTIVE_VALUE.equalsIgnoreCase(catalogStatusPremium);
	}
	public String getProductNameStandard() {
		return productNameStandard;
	}
	public void setProductNameStandard(String productNameStandard) {
		this.productNameStandard = productNameStandard;
	}
	public String getProductDescriptionStandard() {
		return productDescriptionStandard;
	}
	public void setProductDescriptionStandard(String productDescriptionStandard) {
		this.productDescriptionStandard = productDescriptionStandard;
	}
	public String getProductNameDeluxe() {
		return productNameDeluxe;
	}
	public void setProductNameDeluxe(String productNameDeluxe) {
		this.productNameDeluxe = productNameDeluxe;
	}
	public String getProductDescriptionDeluxe() {
		return productDescriptionDeluxe;
	}
	public void setProductDescriptionDeluxe(String productDescriptionDeluxe) {
		this.productDescriptionDeluxe = productDescriptionDeluxe;
	}
	public String getProductNamePremium() {
		return productNamePremium;
	}
	public void setProductNamePremium(String productNamePremium) {
		this.productNamePremium = productNamePremium;
	}
	public String getProductDescriptionPremium() {
		return productDescriptionPremium;
	}
	public void setProductDescriptionPremium(String productDescriptionPremium) {
		this.productDescriptionPremium = productDescriptionPremium;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public void setProductMasterStatus(String productMasterStatus) {
		this.productMasterStatus = productMasterStatus;
	}
	public String getProductMasterStatus() {
		return productMasterStatus;
	}
	public void setItemType(String itemType) {
		this.itemType = itemType;
	}
	public String getItemType() {
		return itemType;
	}
	
	/*public boolean isCatalogStatusStandardReadyToSend(){
		return isCatalogStatusStandardActive() && isSentToAmazonStandardFlag();
	}
	public boolean isCatalogStatusDeluxeReadyToSend(){
		return isCatalogStatusDeluxeActive() && isSentToAmazonDeluxeFlag();
	}
	public boolean isCatalogStatusPremiumReadyToSend(){
		return isCatalogStatusPremiumActive() && isSentToAmazonPremiumFlag();
	}*/
}
