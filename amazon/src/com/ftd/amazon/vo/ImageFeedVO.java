package com.ftd.amazon.vo;

import java.util.ArrayList;
import java.util.List;

public class ImageFeedVO {
	private List<ImageFeedDetailVO> feedDetails = new ArrayList<ImageFeedDetailVO>();

	public List<ImageFeedDetailVO> getFeedDetails() {
		if(feedDetails == null){
			this.feedDetails = new ArrayList<ImageFeedDetailVO>();
		}
		return feedDetails;
	}

	public void setFeedDetails(List<ImageFeedDetailVO> feedDetails) {
		this.feedDetails = feedDetails;
	}
	public void addFeedDetail(ImageFeedDetailVO feedDetail) {
		this.feedDetails.add(feedDetail);
	}
}
