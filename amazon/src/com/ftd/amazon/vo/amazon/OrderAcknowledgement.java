//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2012.07.25 at 02:22:26 PM IST 
//


package com.ftd.amazon.vo.amazon;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.NormalizedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "amazonOrderID",
    "merchantOrderID",
    "statusCode",
    "item"
})
@XmlRootElement(name = "OrderAcknowledgement")
public class OrderAcknowledgement {

    @XmlElement(name = "AmazonOrderID", required = true)
    protected String amazonOrderID;
    @XmlElement(name = "MerchantOrderID")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    protected String merchantOrderID;
    @XmlElement(name = "StatusCode", required = true)
    protected String statusCode;
    @XmlElement(name = "Item")
    protected List<OrderAcknowledgement.Item> item;

    public String getAmazonOrderID() {
        return amazonOrderID;
    }

    public void setAmazonOrderID(String value) {
        this.amazonOrderID = value;
    }

    public String getMerchantOrderID() {
        return merchantOrderID;
    }

    public void setMerchantOrderID(String value) {
        this.merchantOrderID = value;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String value) {
        this.statusCode = value;
    }

    public List<OrderAcknowledgement.Item> getItem() {
        if (item == null) {
            item = new ArrayList<OrderAcknowledgement.Item>();
        }
        return this.item;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "amazonOrderItemCode",
        "merchantOrderItemID",
        "cancelReason"
    })
    public static class Item {

        @XmlElement(name = "AmazonOrderItemCode", required = true)
        protected String amazonOrderItemCode;
        @XmlElement(name = "MerchantOrderItemID")
        @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
        protected String merchantOrderItemID;
        @XmlElement(name = "CancelReason")
        protected String cancelReason;

        public String getAmazonOrderItemCode() {
            return amazonOrderItemCode;
        }

        public void setAmazonOrderItemCode(String value) {
            this.amazonOrderItemCode = value;
        }

        public String getMerchantOrderItemID() {
            return merchantOrderItemID;
        }

        public void setMerchantOrderItemID(String value) {
            this.merchantOrderItemID = value;
        }

        public String getCancelReason() {
            return cancelReason;
        }

        public void setCancelReason(String value) {
            this.cancelReason = value;
        }

    }

}
