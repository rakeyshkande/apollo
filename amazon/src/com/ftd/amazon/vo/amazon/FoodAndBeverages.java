//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2012.07.25 at 01:19:11 PM IST 
//


package com.ftd.amazon.vo.amazon;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "productType"
})
@XmlRootElement(name = "FoodAndBeverages")
public class FoodAndBeverages 
{

    @XmlElement(name = "ProductType", required = true)
    protected FoodAndBeverages.ProductType productType;

    public FoodAndBeverages.ProductType getProductType() 
    {
        return productType;
    }
    public void setProductType(FoodAndBeverages.ProductType value) {
        this.productType = value;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "householdSupplies"
    })
    public static class ProductType 
    {
       
        @XmlElement(name = "HouseholdSupplies")
        protected HouseholdSupplies householdSupplies;
        
        public HouseholdSupplies getHouseholdSupplies() {
            return householdSupplies;
        }
        
        public void setHouseholdSupplies(HouseholdSupplies value) {
            this.householdSupplies = value;
        }

    }

}
