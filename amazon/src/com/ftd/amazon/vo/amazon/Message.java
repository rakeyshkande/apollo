package com.ftd.amazon.vo.amazon;

import java.math.BigInteger;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "messageID",
    "operationType",
    "processingReport",
    "inventory",
    "productImage",
    "product",
    "orderAcknowledgement",
    "orderAdjustment",
    "price",
    "orderFulfillment"
})
public class Message 
{

    @XmlElement(name = "MessageID", required = true)
    protected BigInteger messageID;
    @XmlElement(name = "OperationType")
    protected String operationType;
    @XmlElement(name = "ProcessingReport")
    protected ProcessingReport processingReport;
    @XmlElement(name = "Inventory")
    protected Inventory inventory;
    @XmlElement(name = "ProductImage")
    protected ProductImage productImage;
    @XmlElement(name = "Product")
    protected Product product;
    @XmlElement(name = "OrderAcknowledgement")
    protected OrderAcknowledgement orderAcknowledgement;
    @XmlElement(name = "OrderAdjustment")
    protected OrderAdjustment orderAdjustment;
    @XmlElement(name = "Price")
    protected Price price;
    @XmlElement(name = "OrderFulfillment")
    protected OrderFulfillment orderFulfillment;
    
    public BigInteger getMessageID() {
        return messageID;
    }

    public void setMessageID(BigInteger value) {
        this.messageID = value;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String value) {
        this.operationType = value;
    }
    public ProcessingReport getProcessingReport() {
        return processingReport;
    }

    public void setProcessingReport(ProcessingReport value) {
        this.processingReport = value;
    }
    
    public Inventory getInventory() {
        return inventory;
    }

    public void setInventory(Inventory value) {
        this.inventory = value;
    }

    public ProductImage getProductImage() {
        return productImage;
    }

    public void setProductImage(ProductImage value) {
        this.productImage = value;
    }
    public Product getProduct() {
        return product;
    }
    public void setProduct(Product value) {
        this.product = value;
    }
    public OrderAcknowledgement getOrderAcknowledgement() {
        return orderAcknowledgement;
    }
    public void setOrderAcknowledgement(OrderAcknowledgement value) {
        this.orderAcknowledgement = value;
    }

	public OrderAdjustment getOrderAdjustment() {
		return orderAdjustment;
	}

	public void setOrderAdjustment(OrderAdjustment orderAdjustment) {
		this.orderAdjustment = orderAdjustment;
	}
	public Price getPrice() {
		return price;
	}

	public void setPrice(Price price) {
		this.price = price;
	}

	public OrderFulfillment getOrderFulfillment() {
		return orderFulfillment;
	}

	public void setOrderFulfillment(OrderFulfillment orderFulfillment) {
		this.orderFulfillment = orderFulfillment;
	}

}
