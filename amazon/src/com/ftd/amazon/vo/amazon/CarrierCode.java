//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.2-27 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2012.08.17 at 11:12:48 AM IST 
//


package com.ftd.amazon.vo.amazon;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CarrierCode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CarrierCode">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="USPS"/>
 *     &lt;enumeration value="UPS"/>
 *     &lt;enumeration value="FedEx"/>
 *     &lt;enumeration value="DHL"/>
 *     &lt;enumeration value="Fastway"/>
 *     &lt;enumeration value="GLS"/>
 *     &lt;enumeration value="GO!"/>
 *     &lt;enumeration value="Hermes Logistik Gruppe"/>
 *     &lt;enumeration value="Royal Mail"/>
 *     &lt;enumeration value="Parcelforce"/>
 *     &lt;enumeration value="City Link"/>
 *     &lt;enumeration value="TNT"/>
 *     &lt;enumeration value="Target"/>
 *     &lt;enumeration value="SagawaExpress"/>
 *     &lt;enumeration value="NipponExpress"/>
 *     &lt;enumeration value="YamatoTransport"/>
 *     &lt;enumeration value="DHL Global Mail"/>
 *     &lt;enumeration value="UPS Mail Innovations"/>
 *     &lt;enumeration value="FedEx SmartPost"/>
 *     &lt;enumeration value="OSM"/>
 *     &lt;enumeration value="OnTrac"/>
 *     &lt;enumeration value="Streamlite"/>
 *     &lt;enumeration value="Newgistics"/>
 *     &lt;enumeration value="Canada Post"/>
 *     &lt;enumeration value="Blue Package"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CarrierCode")
@XmlEnum
public enum CarrierCode {

    USPS("USPS"),
    UPS("UPS"),
    @XmlEnumValue("FedEx")
    FED_EX("FedEx"),
    DHL("DHL"),
    @XmlEnumValue("Fastway")
    FASTWAY("Fastway"),
    GLS("GLS"),
    @XmlEnumValue("GO!")
    GO("GO!"),
    @XmlEnumValue("Hermes Logistik Gruppe")
    HERMES_LOGISTIK_GRUPPE("Hermes Logistik Gruppe"),
    @XmlEnumValue("Royal Mail")
    ROYAL_MAIL("Royal Mail"),
    @XmlEnumValue("Parcelforce")
    PARCELFORCE("Parcelforce"),
    @XmlEnumValue("City Link")
    CITY_LINK("City Link"),
    TNT("TNT"),
    @XmlEnumValue("Target")
    TARGET("Target"),
    @XmlEnumValue("SagawaExpress")
    SAGAWA_EXPRESS("SagawaExpress"),
    @XmlEnumValue("NipponExpress")
    NIPPON_EXPRESS("NipponExpress"),
    @XmlEnumValue("YamatoTransport")
    YAMATO_TRANSPORT("YamatoTransport"),
    @XmlEnumValue("DHL Global Mail")
    DHL_GLOBAL_MAIL("DHL Global Mail"),
    @XmlEnumValue("UPS Mail Innovations")
    UPS_MAIL_INNOVATIONS("UPS Mail Innovations"),
    @XmlEnumValue("FedEx SmartPost")
    FED_EX_SMART_POST("FedEx SmartPost"),
    OSM("OSM"),
    @XmlEnumValue("OnTrac")
    ON_TRAC("OnTrac"),
    @XmlEnumValue("Streamlite")
    STREAMLITE("Streamlite"),
    @XmlEnumValue("Newgistics")
    NEWGISTICS("Newgistics"),
    @XmlEnumValue("Canada Post")
    CANADA_POST("Canada Post"),
    @XmlEnumValue("Blue Package")
    BLUE_PACKAGE("Blue Package"),
    @XmlEnumValue("Other")
    OTHER("Other");
    
    private final String value;

    CarrierCode(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CarrierCode fromValue(String v) {
        for (CarrierCode c: CarrierCode.values()) {
            if (c.value.equalsIgnoreCase(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
