
package com.ftd.amazon.vo.amazon;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.NormalizedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "amazonOrderID",
    "merchantOrderID",
    "adjustedItem"
})
@XmlRootElement(name = "OrderAdjustment")
public class OrderAdjustment {

    @XmlElement(name = "AmazonOrderID")
    protected String amazonOrderID;
    @XmlElement(name = "MerchantOrderID")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    protected String merchantOrderID;
    @XmlElement(name = "AdjustedItem", required = true)
    protected List<OrderAdjustment.AdjustedItem> adjustedItem;

    public String getAmazonOrderID() {
        return amazonOrderID;
    }

    public void setAmazonOrderID(String value) {
        this.amazonOrderID = value;
    }

    public String getMerchantOrderID() {
        return merchantOrderID;
    }

    public void setMerchantOrderID(String value) {
        this.merchantOrderID = value;
    }

    public List<OrderAdjustment.AdjustedItem> getAdjustedItem() {
        if (adjustedItem == null) {
            adjustedItem = new ArrayList<OrderAdjustment.AdjustedItem>();
        }
        return this.adjustedItem;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "amazonOrderItemCode",
        "merchantOrderItemID",
        "merchantAdjustmentItemID",
        "adjustmentReason",
        "itemPriceAdjustments",
        "promotionAdjustments"
    })
    public static class AdjustedItem {

        @XmlElement(name = "AmazonOrderItemCode")
        protected String amazonOrderItemCode;
        @XmlElement(name = "MerchantOrderItemID")
        @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
        protected String merchantOrderItemID;
        @XmlElement(name = "MerchantAdjustmentItemID")
        @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
        protected String merchantAdjustmentItemID;
        @XmlElement(name = "AdjustmentReason", required = true)
        protected String adjustmentReason;
        @XmlElement(name = "ItemPriceAdjustments")
        protected BuyerPrice itemPriceAdjustments;
        @XmlElement(name = "PromotionAdjustments")
        protected List<OrderAdjustment.AdjustedItem.PromotionAdjustments> promotionAdjustments;

        public String getAmazonOrderItemCode() {
            return amazonOrderItemCode;
        }

        public void setAmazonOrderItemCode(String value) {
            this.amazonOrderItemCode = value;
        }

        public String getMerchantOrderItemID() {
            return merchantOrderItemID;
        }
        public void setMerchantOrderItemID(String value) {
            this.merchantOrderItemID = value;
        }

        public String getMerchantAdjustmentItemID() {
            return merchantAdjustmentItemID;
        }

        public void setMerchantAdjustmentItemID(String value) {
            this.merchantAdjustmentItemID = value;
        }

        public String getAdjustmentReason() {
            return adjustmentReason;
        }

        public void setAdjustmentReason(String value) {
            this.adjustmentReason = value;
        }

        public BuyerPrice getItemPriceAdjustments() {
        	if(itemPriceAdjustments == null){
        		itemPriceAdjustments = new BuyerPrice();
        	}
            return itemPriceAdjustments;
        }

        public void setItemPriceAdjustments(BuyerPrice value) {
            this.itemPriceAdjustments = value;
        }

        
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "promotionClaimCode",
            "merchantPromotionID",
            "component"
        })
        public static class PromotionAdjustments
        {

            @XmlElement(name = "PromotionClaimCode")
            protected String promotionClaimCode;
            @XmlElement(name = "MerchantPromotionID")
            @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
            protected String merchantPromotionID;
            @XmlElement(name = "Component", required = true)
            protected List<OrderAdjustment.AdjustedItem.PromotionAdjustments.Component> component;

            
            public String getPromotionClaimCode() {
                return promotionClaimCode;
            }

           
            public void setPromotionClaimCode(String value) {
                this.promotionClaimCode = value;
            }

            
            public String getMerchantPromotionID() {
                return merchantPromotionID;
            }

            
            public void setMerchantPromotionID(String value) {
                this.merchantPromotionID = value;
            }

            
            public List<OrderAdjustment.AdjustedItem.PromotionAdjustments.Component> getComponent() {
                if (component == null) {
                    component = new ArrayList<OrderAdjustment.AdjustedItem.PromotionAdjustments.Component>();
                }
                return this.component;
            }


            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "type",
                "amount"
            })
            public static class Component {

                @XmlElement(name = "Type", required = true)
                protected PromotionApplicationType type;
                @XmlElement(name = "Amount", required = true)
                protected CurrencyAmount amount;

                public PromotionApplicationType getType() {
                    return type;
                }

                public void setType(PromotionApplicationType value) {
                    this.type = value;
                }
                public CurrencyAmount getAmount() {
                    return amount;
                }

                public void setAmount(CurrencyAmount value) {
                    this.amount = value;
                }

            }

        }

    }

}
