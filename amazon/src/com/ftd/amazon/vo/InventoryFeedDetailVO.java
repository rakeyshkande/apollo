/**
 * 
 */
package com.ftd.amazon.vo;

/**
 * @author skatam
 *
 */
public class InventoryFeedDetailVO extends BaseFeedDetailVO
{
	private String inventoryFeedId;

	public String getInventoryFeedId() {
		return inventoryFeedId;
	}
	public void setInventoryFeedId(String productFeedId) {
		this.inventoryFeedId = productFeedId;
	}
	
	
	
}
