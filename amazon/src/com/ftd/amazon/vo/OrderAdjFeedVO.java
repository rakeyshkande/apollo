/**
 * 
 */
package com.ftd.amazon.vo;

/**
 * @author skatam
 *
 */
public class OrderAdjFeedVO extends BaseFeedDetailVO
{
	private String azOrderID;
	private String masterOrderNumber;
	private String confirmationNumber;
	private String azOrderAdjId;
	private String azOrderItemNumber;
	private String adjustmentReason;
	private String principalAmt;
	private String shippingAmt;
	private String taxAmt;
	private String shippingTaxAmt;
	
	public String getAzOrderAdjId() {
		return azOrderAdjId;
	}
	public void setAzOrderAdjId(String azOrderAdjId) {
		this.azOrderAdjId = azOrderAdjId;
	}
	public String getAzOrderItemNumber() {
		return azOrderItemNumber;
	}
	public void setAzOrderItemNumber(String azOrderItemNumber) {
		this.azOrderItemNumber = azOrderItemNumber;
	}
	public String getAzOrderID() {
		return azOrderID;
	}
	public void setAzOrderID(String azOrderID) {
		this.azOrderID = azOrderID;
	}
	public String getConfirmationNumber() {
		return confirmationNumber;
	}
	public void setConfirmationNumber(String confirmationNumber) {
		this.confirmationNumber = confirmationNumber;
	}
	public String getMasterOrderNumber() {
		return masterOrderNumber;
	}
	public void setMasterOrderNumber(String masterOrderNumber) {
		this.masterOrderNumber = masterOrderNumber;
	}
	public String getAdjustmentReason() {
		return adjustmentReason;
	}
	public void setAdjustmentReason(String adjustmentReason) {
		this.adjustmentReason = adjustmentReason;
	}
	public String getPrincipalAmt() {
		return principalAmt;
	}
	public void setPrincipalAmt(String principalAmt) {
		this.principalAmt = principalAmt;
	}
	public String getShippingAmt() {
		return shippingAmt;
	}
	public void setShippingAmt(String shippingAmt) {
		this.shippingAmt = shippingAmt;
	}
	public String getTaxAmt() {
		return taxAmt;
	}
	public void setTaxAmt(String taxAmt) {
		this.taxAmt = taxAmt;
	}
	public String getShippingTaxAmt() {
		return shippingTaxAmt;
	}
	public void setShippingTaxAmt(String shippingTaxAmt) {
		this.shippingTaxAmt = shippingTaxAmt;
	}
	
}
