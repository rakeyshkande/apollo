/**
 * 
 */
package com.ftd.amazon.vo;

import java.math.BigDecimal;
import java.util.Date;

public class AzOrderDetailVO 
{
	private String amazonItemNumber;
	private String amazonOrderNumber;
	private String confirmationNumber;
	private String amazonProductId;
	private BigDecimal principalAmount;
	private BigDecimal shippingAmount;
	private BigDecimal taxAmount;
	private BigDecimal shippingTaxAmount;

	public String getAmazonOrderNumber() {
		return amazonOrderNumber;
	}
	public void setAmazonOrderNumber(String amazonOrderNumber) {
		this.amazonOrderNumber = amazonOrderNumber;
	}
	
	public String getAmazonItemNumber() {
		return amazonItemNumber;
	}
	public void setAmazonItemNumber(String amazonItemNumber) {
		this.amazonItemNumber = amazonItemNumber;
	}
	public String getConfirmationNumber() {
		return confirmationNumber;
	}
	public void setConfirmationNumber(String confirmationNumber) {
		this.confirmationNumber = confirmationNumber;
	}
	public String getAmazonProductId() {
		return amazonProductId;
	}
	public void setAmazonProductId(String amazonProductId) {
		this.amazonProductId = amazonProductId;
	}
	public BigDecimal getPrincipalAmount() {
		return principalAmount;
	}
	public void setPrincipalAmount(BigDecimal principalAmount) {
		this.principalAmount = principalAmount;
	}
	public BigDecimal getShippingAmount() {
		return shippingAmount;
	}
	public void setShippingAmount(BigDecimal shippingAmount) {
		this.shippingAmount = shippingAmount;
	}
	public BigDecimal getTaxAmount() {
		return taxAmount;
	}
	public void setTaxAmount(BigDecimal taxAmount) {
		this.taxAmount = taxAmount;
	}
	public BigDecimal getShippingTaxAmount() {
		return shippingTaxAmount;
	}
	public void setShippingTaxAmount(BigDecimal shippingTaxAmount) {
		this.shippingTaxAmount = shippingTaxAmount;
	}
	
}
