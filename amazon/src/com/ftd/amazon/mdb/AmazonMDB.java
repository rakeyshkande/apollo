package com.ftd.amazon.mdb;

import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import com.ftd.amazon.bo.AmazonFeedBO;
import com.ftd.amazon.bo.AmazonOrderBO;
import com.ftd.amazon.bo.AmazonReportBO;
import com.ftd.amazon.bo.FloristFulfilledOrderBO;
import com.ftd.amazon.bo.ImageFeedBO;
import com.ftd.amazon.bo.InventoryFeedBO;
import com.ftd.amazon.bo.OrderAcknowledgementFeedBO;
import com.ftd.amazon.bo.OrderAdjustmentFeedBO;
import com.ftd.amazon.bo.OrderFulfillmentFeedBO;
import com.ftd.amazon.bo.PriceFeedBO;
import com.ftd.amazon.bo.ProductFeedBO;
import com.ftd.amazon.common.AmazonConstants;
import com.ftd.osp.utilities.plugins.Logger;

public class AmazonMDB implements MessageDrivenBean, MessageListener 
{
	private static final long serialVersionUID = 1L;
	private MessageDrivenContext context;
	private Logger logger;
	private static String logContext = "com.ftd.amazon.mdb.AmazonMDB";
  
	public void ejbCreate() {
		
	}

  public void onMessage(Message msg) {
      
    try {
      
        //get message and extract order detail_id
        TextMessage textMessage = (TextMessage)msg;
        String msgText = textMessage.getText();
        String action = msg.getJMSCorrelationID();
        logger.info("action: " + action);
        logger.info("msgText: " + msgText);
        
        if (action == null || action.equals("")) 
        {
        	logger.error("Invalid action");
        } 
        else if (action.equalsIgnoreCase(AmazonConstants.SCHEDULED_REPORTS)) 
        {
        	AmazonReportBO reportBO = new AmazonReportBO();
        	reportBO.processScheduledReports();
        } 
        else if (action.equalsIgnoreCase(AmazonConstants.PROCESS_INBOUND_ORDER)) 
        {
        	AmazonOrderBO orderBO = new AmazonOrderBO();
        	orderBO.processInboundOrder(msgText);
        }  
        else if (action.equalsIgnoreCase(AmazonConstants.INVENTORY_FEED)) 
        {
        	InventoryFeedBO feedBO = new InventoryFeedBO();
        	feedBO.processInventoryFeed();
        } 
        else if (action.equalsIgnoreCase(AmazonConstants.IMAGE_FEED)){
        	ImageFeedBO feedBO = new ImageFeedBO();
        	feedBO.processImageURLFeed();
        }
        else if (action.equalsIgnoreCase(AmazonConstants.PRODUCT_FEED))
        {
        	ProductFeedBO productFeedBO = new ProductFeedBO();
        	productFeedBO.processProductFeed();
        }
        else if (action.equalsIgnoreCase(AmazonConstants.ORDER_ACK_FEED))
        {
        	OrderAcknowledgementFeedBO ordAckFeedBO = new OrderAcknowledgementFeedBO();
        	ordAckFeedBO.processOrderAcknowledgementFeed();
        }
        else if (action.equalsIgnoreCase(AmazonConstants.ORDER_ADJ_FEED))
        {
        	OrderAdjustmentFeedBO ordAdjFeedBO = new OrderAdjustmentFeedBO();
        	ordAdjFeedBO.processOrderAdjustmentFeed();
        }
        else if (action.equalsIgnoreCase(AmazonConstants.PRICE_FEED)){
        	PriceFeedBO feedBO = new PriceFeedBO();
        	feedBO.processPriceFeed();
        }
        else if (action.equalsIgnoreCase(AmazonConstants.ORDER_FULFILL_FEED)){
        	OrderFulfillmentFeedBO orderFulfillmentFeedBO = new OrderFulfillmentFeedBO();
        	orderFulfillmentFeedBO.processOrderFulfillmentFeed();
        }
        else if (action.equalsIgnoreCase(AmazonConstants.FEED_STATUS)) 
        {
        	AmazonFeedBO feedBO = new AmazonFeedBO();
        	feedBO.updateFeedStatus();
        }
        else if (action.equalsIgnoreCase(AmazonConstants.REFUND_FEED))
        {
        	logger.debug("Entered Refund Feed");
        	AmazonFeedBO feedBO = new AmazonFeedBO();
        	feedBO.processRefundFeed();
        	logger.debug("Done Refund Feed");
        }
        else if (action.equalsIgnoreCase(AmazonConstants.PROCESS_SETTLEMENT_REPORT)) 
        {
        	AmazonReportBO reportBO = new AmazonReportBO();
        	reportBO.processSettlementReport(msgText);
        }
        else if (action.equalsIgnoreCase(AmazonConstants.INSERT_FLORIST_FULFILLED_ORDERS)) 
        {
        	FloristFulfilledOrderBO floristConfirmationOrdersBO = new FloristFulfilledOrderBO();
        	floristConfirmationOrdersBO.processFloristFulfilledOrders();
        }
        else if (action.equalsIgnoreCase(AmazonConstants.CHECK_PDB_EXCEPTION_DATES))
        {
        	AmazonFeedBO feedBO = new AmazonFeedBO();
        	feedBO.checkPDBExceptionDates();
        }
        else 
        {
        	logger.error("Invalid action: " + action);
        }

        logger.info("Finished");
    } catch (Exception e) { 
        logger.error(e);
    } catch (Throwable t) {
        logger.error("Thrown error: " + t);
    }
  }

  public void ejbRemove() 
  {
  }

  public void setMessageDrivenContext(MessageDrivenContext ctx) 
  {
      this.context = ctx;
      logger = new Logger(logContext);  
  }
}