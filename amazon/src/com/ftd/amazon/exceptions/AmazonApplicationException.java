/**
 * 
 */
package com.ftd.amazon.exceptions;

/**
 * @author skatam
 *
 */
public class AmazonApplicationException extends RuntimeException
{
	private static final long serialVersionUID = 1L;

	public AmazonApplicationException() {
		super();
		
	}

	public AmazonApplicationException(String message, Throwable cause) {
		super(message, cause);
		
	}

	public AmazonApplicationException(String message) {
		super(message);
		
	}

	public AmazonApplicationException(Throwable cause) {
		super(cause);
		
	}
	
}
