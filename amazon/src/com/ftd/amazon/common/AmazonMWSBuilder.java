
package com.ftd.amazon.common;

import com.amazonaws.mws.MarketplaceWebService;
import com.amazonaws.mws.MarketplaceWebServiceClient;
import com.amazonaws.mws.MarketplaceWebServiceConfig;
import com.amazonaws.mws.mock.MarketplaceWebServiceMock;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * @author skatam
 *
 */
public class AmazonMWSBuilder
{
	private static Logger logger = new Logger("com.ftd.amazon.common.AmazonMWSBuilder");
	public MarketplaceWebService getMarketplaceWebServiceMock() 
	{
		return new MarketplaceWebServiceMock();
	}
	
	public static MarketplaceWebService getMarketplaceWebService
						(MarketplaceWebServiceConfig config) 
	{
		MarketplaceWebService mws = null;
		
		try 
		{
			String accessKeyId = AmazonUtils.getAccessKeyID();
			String secretAccessKey = AmazonUtils.getSecretAccessKey();
			String appName = AmazonUtils.getApplicationName();
			String appVersion = AmazonUtils.getApplicationVersion();
			if(config == null){
				config = new MarketplaceWebServiceConfig();
			}
			if(!config.isSetServiceURL()){
				String serviceURL = AmazonUtils.getServiceURL();			
				config.setServiceURL(serviceURL);
			}
			
			configureProxySettings(config);
			
			mws = new MarketplaceWebServiceClient
						(accessKeyId, secretAccessKey, 
						appName, appVersion, config);
		
		} 
		catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		
        return mws;		
	}

	private static void configureProxySettings(MarketplaceWebServiceConfig config) 
	{
		logger.debug("Needs Proxy Configuration :"+AmazonConstants.NEEDS_PROXY_CONFIG);
		if(AmazonConstants.NEEDS_PROXY_CONFIG)
		{
			config.setProxyHost(AmazonConstants.PROXY_HOST);
			config.setProxyPort(AmazonConstants.PROXY_PORT);		
		}
	}

}
