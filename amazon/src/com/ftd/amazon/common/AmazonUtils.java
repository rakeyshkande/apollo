package com.ftd.amazon.common;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.naming.InitialContext;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang.StringUtils;
import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.Text;

import com.amazonaws.mws.MarketplaceWebServiceException;
import com.ftd.amazon.dao.AmazonDAO;
import com.ftd.amazon.exceptions.AmazonApplicationException;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.CacheUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.xml.DOMUtil;

public class AmazonUtils
{ 
  
    /**
    * Logger instance
    */
    private static Logger logger = new Logger("com.ftd.amazon.common.AmazonUtils");
    
    public static boolean sendJMSMessage(String status, String corrId, String message)
    {

      boolean success = true;

      try
      {
        MessageToken messageToken = new MessageToken();
        messageToken.setStatus(status);
        messageToken.setJMSCorrelationID(corrId);
        messageToken.setMessage(message);
        Dispatcher dispatcher = Dispatcher.getInstance();
        dispatcher.dispatchTextMessage(new InitialContext(), messageToken);
      }
      catch (Exception e)
      {
        logger.error(e);
        success = false;
      }

      return success;

    }

    /**
     * Get a new database connection.
     * 
     * @return
     * @throws Exception
     */
    public static Connection getNewConnection() throws Exception
    {
      // get database connection
      ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
      Connection conn = null;

      String datasource = configUtil.getPropertyNew(AmazonConstants.PROPERTY_FILE, AmazonConstants.DATASOURCE_NAME);
      conn = DataSourceUtil.getInstance().getConnection(datasource);

      return conn;
    }

    
    
    public static String getFrpGlobalParm(String context, String name) //throws CacheException, Exception
    {
      CacheUtil cacheUtil = CacheUtil.getInstance();
      try {
		return cacheUtil.getGlobalParm(context, name);
		} catch (Exception e) {
			logger.error(e);
			throw new RuntimeException(e);
		}
    }

    
    
    public static String getSecureGlobalParm(String context, String name) //throws CacheException, Exception
    {
      try {
		ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
		  return configUtil.getSecureProperty(context, name);
	   } catch (Exception e) {
		   logger.error(e);
			throw new RuntimeException(e);
	   }
    }

    
    public static void sendPageSystemMessage(String logMessage)
    {
      Connection conn = null;
      try
      {
        conn = getNewConnection();
        String appSource = AmazonConstants.SM_PAGE_SOURCE;
        String errorType = AmazonConstants.SM_TYPE;
        String subject = AmazonConstants.SM_PAGE_SUBJECT;
        int pageLevel = SystemMessengerVO.LEVEL_PRODUCTION;

        SystemMessengerVO systemMessengerVO = new SystemMessengerVO();
        systemMessengerVO.setLevel(pageLevel);
        systemMessengerVO.setSource(appSource);
        systemMessengerVO.setType(errorType);
        systemMessengerVO.setSubject(subject);
        systemMessengerVO.setMessage(logMessage);
        String result = SystemMessenger.getInstance().send(systemMessengerVO, conn, false);

      }
      catch (Exception ex)
      {
        // Do not attempt to send system message it requires obtaining a
        // connection
        // and may end up in an infinite loop.
        logger.error(ex);
      }
      finally
      {
        if (conn != null)
        {
          try
          {
            conn.close();
          }
          catch (Exception e)
          {
            logger.error("Unable to close connection: " + e);
          }
        }
      }

    }

    public static void sendNoPageSystemMessage(String logMessage)
    {
      Connection conn = null;
      try
      {
        conn = getNewConnection();
        String appSource = AmazonConstants.SM_PAGE_SOURCE;
        String errorType = AmazonConstants.SM_TYPE;
        String subject = AmazonConstants.SM_NOPAGE_SUBJECT;
        int pageLevel = SystemMessengerVO.LEVEL_PRODUCTION;

        SystemMessengerVO systemMessengerVO = new SystemMessengerVO();
        systemMessengerVO.setLevel(pageLevel);
        systemMessengerVO.setSource(appSource);
        systemMessengerVO.setType(errorType);
        systemMessengerVO.setSubject(subject);
        systemMessengerVO.setMessage(logMessage);
        String result = SystemMessenger.getInstance().send(systemMessengerVO, conn, false);

      }
      catch (Exception ex)
      {
        // Do not attempt to send system message it requires obtaining a
        // connection
        // and may end up in an infinite loop.
        logger.error(ex);
      }
      finally
      {
        if (conn != null)
        {
          try
          {
            conn.close();
          }
          catch (Exception e)
          {
            logger.error("Unable to close connection: " + e);
          }
        }
      }

    }
    
    public static String getServiceURL() {
		return getFrpGlobalParm(AmazonConstants.AMZ_GLOBAL_CONTEXT, AmazonConstants.AMZ_NAME_MWS_URL);
	}
	
    public static List<String> getMarketPlaceIds() {
		List<String> marketPlaceIds = new ArrayList<String>();
		String id = getFrpGlobalParm(AmazonConstants.AMZ_GLOBAL_CONTEXT, AmazonConstants.AMZ_NAME_MARKETPLACE_ID);
		marketPlaceIds.add(id);
		return marketPlaceIds;
	}
	
    public static String getMerchantId() {
		return getFrpGlobalParm(AmazonConstants.AMZ_GLOBAL_CONTEXT, AmazonConstants.AMZ_NAME_MERCHANT_ID);
	}

    public static String getApplicationVersion() {
		return AmazonConstants.APP_VERSION;
	}

    public static String getApplicationName() {
		return AmazonConstants.APP_NAME;
	}

	public static String getSecretAccessKey() {
		return getSecureGlobalParm(AmazonConstants.AMZ_SECURE_CONTEXT, AmazonConstants.AMZ_NAME_SECRET_ACCESS_KEY);
	}

	public static String getAccessKeyID() {
		return getSecureGlobalParm(AmazonConstants.AMZ_SECURE_CONTEXT, AmazonConstants.AMZ_NAME_ACCESS_KEY_ID);
	}
	
	/**
	* Calculate content MD5 header values for feeds stored on disk.
	*/
	public static String computeContentMD5HeaderValue( FileInputStream fis )
	{
		String md5Content = null;
		
		try 
		{
			DigestInputStream dis = new DigestInputStream( fis,
			MessageDigest.getInstance( "MD5" ));
			byte[] buffer = new byte[8192];
			while( dis.read( buffer ) > 0 );
			md5Content = new String(
			org.apache.commons.codec.binary.Base64.encodeBase64(dis.getMessageDigest().digest())
			);
			// Effectively resets the stream to be beginning of the file via a FileChannel.
			fis.getChannel().position( 0 );
		} 
		catch (NoSuchAlgorithmException e) {
			throw new AmazonApplicationException(e);
		} 
		catch (IOException e) {
			throw new AmazonApplicationException(e);		
		}
		return md5Content;
	}
	
	/**
     * Get the name of the server that the PI application is running on
     * @return String name of local host or blank if it can't be determined.
     */
    public static String getLocalHostName() 
    {
        String retVal = "";
        try {
            InetAddress addr = InetAddress.getLocalHost();
            retVal = addr.getHostName();
        } catch (UnknownHostException e) {
            retVal = "Unknown";
        }
        return retVal;
    }
    
    public static String getAmazonLocalFeedDirectory() {
		return getFrpGlobalParm(AmazonConstants.AMZ_GLOBAL_CONTEXT, "LOCAL_FEED_DIRECTORY");
	}
    
    public static String getAmazonLocalFeedArchiveDirectory() {
			return getFrpGlobalParm(AmazonConstants.AMZ_GLOBAL_CONTEXT, "ARCHIVE_FEED_DIRECTORY");
	}
    
	public static void log(MarketplaceWebServiceException ex)
	{
		logger.error("Caught Exception: " + ex.getMessage());
        logger.error("Response Status Code: " + ex.getStatusCode());
        logger.error("Error Code: " + ex.getErrorCode());
        logger.error("Error Type: " + ex.getErrorType());
        logger.error("Request ID: " + ex.getRequestId());
        logger.error("XML: " + ex.getXML());
	}
	    
	public static Calendar fromXMLGregorianCalendar(XMLGregorianCalendar xc) //throws DatatypeConfigurationException 
	{
	 Calendar c = Calendar.getInstance();
	 c.setTimeInMillis(xc.toGregorianCalendar().getTimeInMillis());
	 return c;
	}

	public static XMLGregorianCalendar toXMLGregorianCalendar(Date date) //throws DatatypeConfigurationException 
	{
	 GregorianCalendar gc = new GregorianCalendar();
	 gc.setTimeInMillis(date.getTime());
	 XMLGregorianCalendar xc;
	try {
		xc = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
	} catch (DatatypeConfigurationException e) {
		logger.error(e);
		throw new RuntimeException(e.getMessage());
	}
	 return xc;
	}

	public static String deriveFeedName(String feedNamePrefix) {
		String directory = AmazonUtils.getAmazonLocalFeedDirectory();
		return directory + feedNamePrefix + System.currentTimeMillis()+".xml";
	}
	
	public static String getSourceCode(){
		return getFrpGlobalParm(AmazonConstants.AMZ_GLOBAL_CONTEXT, AmazonConstants.AMZ_NAME_DEFAULT_SOURCE_CODE);
	}

}