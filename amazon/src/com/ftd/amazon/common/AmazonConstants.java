package com.ftd.amazon.common;

public class AmazonConstants
{ 
  
    public AmazonConstants() {
    }

    //Feeds
    public final static String PRODUCT_FEED = "PRODUCT-FEED";
    public final static String PRICE_FEED = "PRICE-FEED";
    public final static String INVENTORY_FEED = "INVENTORY-FEED";
    public final static String IMAGE_FEED = "IMAGE-FEED";
    public final static String PRODUCT_OVERRIDE_FEED = "PRODUCT-OVERRIDE-FEED";
    public final static String ORDER_ACK_FEED = "ORDER-ACK-FEED";
    public final static String ORDER_ADJ_FEED = "ORDER-ADJ-FEED";
    public final static String ORDER_FULFILL_FEED = "ORDER-FULFILL-FEED";
    public final static String FEED_STATUS = "FEED-STATUS";
    public final static String REFUND_FEED = "PROCESS-REFUNDS";
    public final static String INSERT_FLORIST_FULFILLED_ORDERS =  "FLORIST-FULFILLED-ORDERS";
    public final static String CHECK_PDB_EXCEPTION_DATES = "CHECK-PDB-EXCEPTION-DATES";

    //Reports
    public final static String SCHEDULED_REPORTS = "SCHEDULED-REPORTS";
    public final static String ORDER_REPORT = "_GET_ORDERS_DATA_";
    public final static String DEFAULT_REPORT_ID = "0";
    public final static String SETTLEMENT_REPORT = "_GET_PAYMENT_SETTLEMENT_DATA_"; //settlement report
    public final static String FEED_SUMMARY_REPORT = "FeedSummaryReport";
    public final static String V2_SETTLEMENT_REPORT = "_GET_V2_SETTLEMENT_REPORT_DATA_";
    
    //Order
    public final static String PROCESS_INBOUND_ORDER = "PROCESS-INBOUND-ORDER";
    public final static String PROCESS_SETTLEMENT_REPORT = "PROCESS-SETTLEMENT-REPORT"; //settlement report
    
    //JMS
    public final static String JMS_PIPELINE_FOR_EM_AMAZON = "SUCCESS";

    //Errors
    public final static int ERROR_CODE = 600;
    public final static int SUCCESS_CODE = 200;
    public final static String EMPTY_XML_ERROR = "Received empty xml transmission.";

    public final static String APP_NAME = "FTD.COM";
    public final static String APP_VERSION = "5.8";

    public final static String DEFAULT_COMPANY_ID = "FTD";
    public final static String ORDER_STATUS_RECEIVED = "RECEIVED";
    public static final String ORDER_STATUS_TRANSFORMED = "TRANSFORMED";
    public final static String ORDER_STATUS_DONE = "DONE";
    public final static String ORDER_STATUS_GATHERER_ERROR = "GATHERER ERROR";
    public final static String PROPERTY_FILE = "amazon_config.xml";
    public final static String DATASOURCE_NAME = "DATASOURCE";

    // System messaging
    public static final String SM_PAGE_SOURCE = "AMAZON_PAGE";
    public static final String SM_NOPAGE_SOURCE = "AMAZON_NOPAGE";
    public static final String SM_PAGE_SUBJECT = "Amazon Message";
    public static final String SM_NOPAGE_SUBJECT = "NOPAGE Amazon Message";
    public static final String SM_TYPE = "System Exception";

    // Global parameters
    public static final String AMZ_GLOBAL_CONTEXT            = "AMAZON_CONFIG";
    public static final String AMZ_NAME_APP_NAME             = "app_name";
    public static final String AMZ_NAME_APP_VERSION          = "app_version";
    public static final String AMZ_NAME_DEFAULT_SOURCE_CODE  = "default_source_code";
    public static final String AMZ_NAME_MARKETPLACE_ID       = "marketplace_id";
    public static final String AMZ_NAME_MERCHANT_ID          = "merchant_id";
    public static final String AMZ_NAME_MWS_URL              = "mws_url";
    public static final String AMZ_ORDER_GATHERER_URL        = "ORDER_GATHERER_URL";

    // Secure configuration parameters
    public static final String AMZ_SECURE_CONTEXT            = "amazon";
    public static final String AMZ_NAME_ACCESS_KEY_ID        = "accessKeyId";
    public static final String AMZ_NAME_SECRET_ACCESS_KEY    = "secretAccessKey";
    
    public static final boolean NEEDS_PROXY_CONFIG = false;
    public static final String 	PROXY_HOST    = "monitoring01.hyd.int.untd.com";
    public static final int 	PROXY_PORT    = 3128;
    
    public static final String 	CATALOG_SUFFIX_STANDARD    = "";
    public static final String 	CATALOG_SUFFIX_DELUXE    = "_deluxe";
    public static final String 	CATALOG_SUFFIX_PREMIUM    = "_premium";
    
    public static final String 	CATALOG_STATUS_ACTIVE_VALUE    = "A";
    public static final String 	CATALOG_STATUS_INACTIVE_VALUE    = "I";
    
    public static final String 	FEED_STATUS_NEW    = "NEW";    
    public static final String 	FEED_STATUS_SENT    = "SENT";    
    public static final String 	FEED_STATUS_CONFIRMED    = "CONFIRMED";    
    
    public static final String AMZ_IMAGE_TYPE				 = "image_type";
    public static final String AMZ_IMAGE_SIZE				 = "image_size";
    public static final String AMZ_IMAGE_URL				 = "image_url";
    public static final String AMZ_IMAGE_FORMAT				 = "image_format";
    
    public static final String DEFAULT_END_DATE		         = "default_end_date";
    
    public static final String INVENTORY_FEED_NAME_PREFIX 	= "amazon_inv_feed_";
    public static final String PRODUCT_FEED_NAME_PREFIX 	= "amazon_product_feed_";
    public static final String ORDER_ACK_FEED_NAME_PREFIX 	= "amazon_ord_ack_feed_";
    public static final String IMAGE_FEED_NAME_PREFIX 		= "amazon_img_feed_";
    public static final String PRICE_FEED_NAME_PREFIX 		= "amazon_price_feed_";
    public static final String ORDER_FUL_FEED_NAME_PREFIX   = "amazon_ord_ful_feed_";
    public static final String ORDER_ADJ_FEED_NAME_PREFIX 	= "amazon_ord_adj_feed_";
		
}