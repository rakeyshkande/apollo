package com.ftd.amazon.common;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.HashMap;

import javax.naming.InitialContext;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ftd.amazon.bo.AmazonReportBO;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.HttpServletResponseUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.xml.JAXPUtil;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;

public class AmazonOrderGatherer extends HttpServlet
{ 
  
    /**
    * Logger instance
    */
    private Logger logger = new Logger("com.ftd.amazon.common.AmazonOrderGatherer");
    
    public void init(ServletConfig config) throws ServletException
    {
      super.init(config);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
      PrintWriter out = response.getWriter();
      out.println("The Amazon Order Gatherer!");
      out.close();
      
    }

  /**
   *
   *  DoPost method receives and process the xml file
   *
   * @param request HttpServletRequest
   * @param response HttpServletResponse
   * @exception ServletException
   * @exception IOException
   */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
      logger.info("BEGIN TRANSACTION");

      String orderXML = request.getParameter("Order");
      logger.info(orderXML);

      //check to see if anything was received in the transmission
      if(orderXML == null || orderXML.equals(""))
      {
        this.returnError(response, AmazonConstants.EMPTY_XML_ERROR);
        return;
      }
      Connection conn = null;
      
      try {
          AmazonUtils amzUtils = new AmazonUtils();
          //establish connection
          conn = amzUtils.getNewConnection();

          AmazonReportBO reportBO = new AmazonReportBO();
          HashMap orderHash = (HashMap) reportBO.splitOrderReport(orderXML);
          
          String reportId = "0";
          reportBO.insertOrderSendJMS(conn, reportId, orderHash);
          
          this.returnSuccess(response);
          
      } catch (Exception e) {
    	  logger.error(e);
    	  this.returnError(response, "Couldn't process Order XML");
      } finally {
          if (conn != null) {
              try {
                  conn.close();
              } catch (Exception e) {
                  logger.error("Unable to close connection: " + e);
              }
          }
      }

    }
 
    /**
    *
    * Set and return success code
    *
    * @param response HttpServletResponse
    */
   private void returnSuccess(HttpServletResponse response) {
       try {
           HttpServletResponseUtil.sendError(response, AmazonConstants.SUCCESS_CODE, "Success");
       } catch(Exception ex) {
           logger.error(ex.toString());
       }
   }

   /**
    *
    * Set and return error code
    *
    * @param response HttpServletResponse
    */
   private void returnError(HttpServletResponse response, String errorReason) {
       try {
           HttpServletResponseUtil.sendError(response, AmazonConstants.ERROR_CODE, errorReason);
       } catch(Exception ex) {
           logger.error("XML file transmission failed: " + ex.toString());
       }
   }

}