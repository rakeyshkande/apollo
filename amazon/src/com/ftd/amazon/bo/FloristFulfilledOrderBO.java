package com.ftd.amazon.bo;

import java.sql.Connection;
import java.sql.SQLException;

import com.ftd.amazon.common.AmazonUtils;
import com.ftd.amazon.dao.AmazonDAO;
import com.ftd.amazon.dao.OrderFulfillmentFeedDAO;
import com.ftd.amazon.exceptions.AmazonApplicationException;
import com.ftd.osp.utilities.plugins.Logger;

public class FloristFulfilledOrderBO {

	private Logger logger = new Logger("com.ftd.amazon.bo.FloristFulfilledOrderBO");

	/**
	 * Default public constructor
	 */
	public FloristFulfilledOrderBO(){

	}

	public void processFloristFulfilledOrders() throws Exception{
		// TODO Auto-generated method stub
		logger.info("insertFloristFulfilledOrders");
		Connection conn = null;
		try
		{
			conn = AmazonUtils.getNewConnection();
			AmazonDAO feedDAO = new AmazonDAO(conn);
			feedDAO.insertFloristFulfilledOrders();
		}
		catch (Exception e) 
		{
			logger.error(e);
			throw new AmazonApplicationException(e.getMessage());
		}
		finally
		{
			if(conn!= null){ try { conn.close(); } catch (SQLException e) {} }
		}
	}

}
