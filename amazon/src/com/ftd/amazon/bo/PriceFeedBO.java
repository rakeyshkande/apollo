package com.ftd.amazon.bo;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import com.amazonaws.mws.model.FeedSubmissionInfo;
import com.amazonaws.mws.model.FeedType;
import com.amazonaws.mws.model.SubmitFeedResponse;
import com.amazonaws.mws.model.SubmitFeedResult;
import com.ftd.amazon.common.AmazonConstants;
import com.ftd.amazon.common.AmazonUtils;
import com.ftd.amazon.dao.PriceFeedDAO;
import com.ftd.amazon.exceptions.AmazonApplicationException;
import com.ftd.amazon.vo.PriceFeedDetailVO;
import com.ftd.amazon.vo.PriceFeedVO;
import com.ftd.amazon.vo.ProductMasterVO;
import com.ftd.amazon.vo.amazon.AmazonEnvelope;
import com.ftd.amazon.vo.amazon.BaseCurrencyCodeWithDefault;
import com.ftd.amazon.vo.amazon.Header;
import com.ftd.amazon.vo.amazon.Message;
import com.ftd.amazon.vo.amazon.OverrideCurrencyAmount;
import com.ftd.amazon.vo.amazon.Price;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

public class PriceFeedBO {

	private Logger logger = new Logger("com.ftd.amazon.bo.PriceFeedBO");

	public PriceFeedBO() {

	}
	public void processPriceFeed() 
	{
		logger.info("Processing Price Feed..");

		Connection conn = null;
		try
		{
			conn = AmazonUtils.getNewConnection();
			PriceFeedDAO feedDAO = new PriceFeedDAO(conn);
			PriceFeedVO priceFeedData = feedDAO.getPriceFeedData();
			if(priceFeedData.getFeedDetails()==null || priceFeedData.getFeedDetails().isEmpty()){
				logger.info("No price Feed Updates to Send to Amazon");
				return;
			}
			AmazonEnvelope amazonEnvelope = this.buildPriceFeedEnvelope(priceFeedData,conn);
			if(amazonEnvelope.getMessages().isEmpty()){
				logger.info("No price Feed Updates to Send to Amazon");
				return;
			}
			String feedContent = amazonEnvelope.marshallAsXML();
			logger.debug("FeedContent XML :"+feedContent);
			String fileName = AmazonFeedBO.saveFeedXML(feedContent, AmazonConstants.PRICE_FEED_NAME_PREFIX);

			SubmitFeedResponse response = AmazonFeedBO.submitFeed(FeedType.POST_PRODUCT_PRICING_DATA ,fileName);
			// add response handling code
			SubmitFeedResult submitFeedResult = response.getSubmitFeedResult();
			FeedSubmissionInfo feedSubmissionInfo = submitFeedResult.getFeedSubmissionInfo();
			List<PriceFeedDetailVO> feedDetails = priceFeedData.getFeedDetails();
			for (PriceFeedDetailVO pricevo : feedDetails) 
			{
				//vo.setFeedStatus(feedSubmissionInfo.getFeedProcessingStatus());
				pricevo.setFeedStatus(AmazonConstants.FEED_STATUS_SENT);
				pricevo.setServer(AmazonUtils.getLocalHostName());
				pricevo.setFilename(fileName);
				pricevo.setTransactionId(feedSubmissionInfo.getFeedSubmissionId());
				feedDAO.saveFeedStatus(pricevo);
			}
		}
		catch (Exception e) 
		{
			logger.error(e);
			throw new AmazonApplicationException(e.getMessage());
		}
		finally
		{
			if(conn!= null){ try { conn.close(); } catch (SQLException e) {} }
		}
	}
	private AmazonEnvelope buildPriceFeedEnvelope(PriceFeedVO priceFeedVO,Connection conn) throws Exception {
		AmazonEnvelope amazonEnvelope = new AmazonEnvelope();
		Header header = new Header();
		header.setDocumentVersion("1.01");
		header.setMerchantIdentifier(AmazonUtils.getMerchantId());
		amazonEnvelope.setHeader(header );
		amazonEnvelope.setMessageType("Price");
		int counter = 1;
		List<PriceFeedDetailVO> PriceFeedDetails = priceFeedVO.getFeedDetails();
		for(PriceFeedDetailVO priceFeedDetail : PriceFeedDetails)
		{
			List<Price> priceList = this.buildPriceList(priceFeedDetail,conn);
			for(Price price: priceList){
				Message msg = new Message();
				msg.setMessageID(new BigInteger(""+counter));
				msg.setOperationType("Update");	
				msg.setPrice(price);
				amazonEnvelope.getMessages().add(msg);
				counter++;
			}
		}
		return amazonEnvelope;
	}

	protected List<Price> buildPriceList(PriceFeedDetailVO priceFeedDetailVO, Connection conn) throws Exception 
	{
		logger.info("buildingPriceList");
		List<Price> priceList = new ArrayList<Price>();
		ProductMasterVO productFeedStatus = priceFeedDetailVO.getProductFeedStatus();

		PriceFeedDAO feedDAO = new PriceFeedDAO(conn);
		String sourceCode = AmazonUtils.getSourceCode();
		Calendar cal = null;

		if(productFeedStatus.isCatalogStatusStandardActive() 
				&& productFeedStatus.isSentToAmazonStandardFlag())
		{
			Price price = new Price();
			price.setSKU(priceFeedDetailVO.getProductId()+AmazonConstants.CATALOG_SUFFIX_STANDARD);
			OverrideCurrencyAmount overrideCurrencyAmount = new OverrideCurrencyAmount();
			overrideCurrencyAmount.setValue(priceFeedDetailVO.getStandardPrice());
			overrideCurrencyAmount.setCurrency(BaseCurrencyCodeWithDefault.fromValue(BaseCurrencyCodeWithDefault.USD.value()));
			price.setStandardPrice(overrideCurrencyAmount);
			
			Map priceSaleMap =  feedDAO.getPriceFeedSaleData(sourceCode, priceFeedDetailVO.getProductId(),
					priceFeedDetailVO.getStandardPrice());

			BigDecimal salePrice = priceFeedDetailVO.getStandardPrice();
			boolean hasSale = false;
			Date startDate = null;
			Date endDate = null;

			Double sourceDiscountAmt = (Double) priceSaleMap.get("sourceDiscountAmt");
			String sourceDiscountType = (String) priceSaleMap.get("sourceDiscountType");
			if (sourceDiscountType != null && sourceDiscountAmt > 0) {
				BigDecimal sourcePrice = calculateSalePrice(sourceDiscountAmt, sourceDiscountType,
						priceFeedDetailVO.getStandardPrice());
                if (sourcePrice.compareTo(priceFeedDetailVO.getStandardPrice()) < 0) {
                	salePrice = sourcePrice;
                	startDate = (Date) priceSaleMap.get("sourceStartDate");
                	hasSale = true;
                	logger.debug("Source code sale price: " + salePrice);
                }
			}
			
			Double iotwDiscountAmt = (Double) priceSaleMap.get("iotwDiscountAmt");
			String iotwDiscountType = (String) priceSaleMap.get("iotwDiscountType");
			if (iotwDiscountType != null && iotwDiscountAmt > 0) {
				BigDecimal iotwPrice = calculateSalePrice(iotwDiscountAmt, iotwDiscountType,
						priceFeedDetailVO.getStandardPrice());
                if (iotwPrice.compareTo(salePrice) < 0) {
                	salePrice = iotwPrice;
                	startDate = (Date) priceSaleMap.get("iotwStartDate");
                	endDate = (Date) priceSaleMap.get("iotwEndDate");
                	hasSale = true;
                	logger.debug("IOTW sale price: " + salePrice);
                }
			}

			// If a record exists for IOTW source code, then <sale> node should be constructed.
			if (hasSale) {
				Price.Sale sale = new Price.Sale();
				OverrideCurrencyAmount overrideCurrencyAmountSale = new OverrideCurrencyAmount();
				overrideCurrencyAmountSale.setValue(salePrice);
				overrideCurrencyAmountSale.setCurrency(BaseCurrencyCodeWithDefault.fromValue(BaseCurrencyCodeWithDefault.USD.value()));
				sale.setSalePrice(overrideCurrencyAmountSale);
				sale.setStartDate(AmazonUtils.toXMLGregorianCalendar(startDate));
				if (endDate!=null){
					sale.setEndDate(AmazonUtils.toXMLGregorianCalendar(endDate));
				} else {
					ConfigurationUtil cu = ConfigurationUtil.getInstance();
			        String defaultEndDate = cu.getFrpGlobalParm(AmazonConstants.AMZ_GLOBAL_CONTEXT,
		        		AmazonConstants.DEFAULT_END_DATE);
					cal = Calendar.getInstance();
					SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
					cal.setTime(sdf.parse(defaultEndDate));
					sale.setEndDate(AmazonUtils.toXMLGregorianCalendar(cal.getTime()));
				}
				price.setSale(sale);
			}
			priceList.add(price);
		}

		if(productFeedStatus.isCatalogStatusDeluxeActive() 
				&& productFeedStatus.isSentToAmazonDeluxeFlag())
		{
			Price price = new Price();
			price.setSKU(priceFeedDetailVO.getProductId()+AmazonConstants.CATALOG_SUFFIX_DELUXE);
			OverrideCurrencyAmount overrideCurrencyAmount = new OverrideCurrencyAmount();
			overrideCurrencyAmount.setValue(priceFeedDetailVO.getDeluxePrice());
			overrideCurrencyAmount.setCurrency(BaseCurrencyCodeWithDefault.fromValue(BaseCurrencyCodeWithDefault.USD.value()));
			price.setStandardPrice(overrideCurrencyAmount);

			Map priceSaleMap =  feedDAO.getPriceFeedSaleData(sourceCode, priceFeedDetailVO.getProductId(),
					priceFeedDetailVO.getDeluxePrice());

			BigDecimal salePrice = priceFeedDetailVO.getDeluxePrice();
			boolean hasSale = false;
			Date startDate = null;
			Date endDate = null;

			Double sourceDiscountAmt = (Double) priceSaleMap.get("sourceDiscountAmt");
			String sourceDiscountType = (String) priceSaleMap.get("sourceDiscountType");
			if (sourceDiscountType != null && sourceDiscountAmt > 0) {
				BigDecimal sourcePrice = calculateSalePrice(sourceDiscountAmt, sourceDiscountType,
						priceFeedDetailVO.getDeluxePrice());
                if (sourcePrice.compareTo(priceFeedDetailVO.getDeluxePrice()) < 0) {
                	salePrice = sourcePrice;
                	startDate = (Date) priceSaleMap.get("sourceStartDate");
                	hasSale = true;
                	logger.debug("Source code sale price: " + salePrice);
                }
			}
			
			Double iotwDiscountAmt = (Double) priceSaleMap.get("iotwDiscountAmt");
			String iotwDiscountType = (String) priceSaleMap.get("iotwDiscountType");
			if (iotwDiscountType != null && iotwDiscountAmt > 0) {
				BigDecimal iotwPrice = calculateSalePrice(iotwDiscountAmt, iotwDiscountType,
						priceFeedDetailVO.getDeluxePrice());
                if (iotwPrice.compareTo(salePrice) < 0) {
                	salePrice = iotwPrice;
                	startDate = (Date) priceSaleMap.get("iotwStartDate");
                	endDate = (Date) priceSaleMap.get("iotwEndDate");
                	hasSale = true;
                	logger.debug("IOTW sale price: " + salePrice);
                }
			}

			// If a record exists for IOTW source code, then <sale> node should be constructed.
			if (hasSale){
				Price.Sale sale = new Price.Sale();
				OverrideCurrencyAmount overrideCurrencyAmountSale = new OverrideCurrencyAmount();
				overrideCurrencyAmountSale.setValue(salePrice);
				overrideCurrencyAmountSale.setCurrency(BaseCurrencyCodeWithDefault.fromValue(BaseCurrencyCodeWithDefault.USD.value()));
				sale.setSalePrice(overrideCurrencyAmountSale);
				sale.setStartDate(AmazonUtils.toXMLGregorianCalendar(startDate));
				if (endDate!=null){
					sale.setEndDate(AmazonUtils.toXMLGregorianCalendar(endDate));
				} else {
					ConfigurationUtil cu = ConfigurationUtil.getInstance();
			        String defaultEndDate = cu.getFrpGlobalParm(AmazonConstants.AMZ_GLOBAL_CONTEXT,
		        		AmazonConstants.DEFAULT_END_DATE);
					cal = Calendar.getInstance();
					SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
					cal.setTime(sdf.parse(defaultEndDate));
					sale.setEndDate(AmazonUtils.toXMLGregorianCalendar(cal.getTime()));
				}
				price.setSale(sale);
			}
			priceList.add(price);
		}

		if(productFeedStatus.isCatalogStatusPremiumActive() 
				&& productFeedStatus.isSentToAmazonPremiumFlag())
		{
			Price price = new Price();
			price.setSKU(priceFeedDetailVO.getProductId()+AmazonConstants.CATALOG_SUFFIX_PREMIUM);
			OverrideCurrencyAmount overrideCurrencyAmount = new OverrideCurrencyAmount();
			overrideCurrencyAmount.setValue(priceFeedDetailVO.getPremiumPrice());
			overrideCurrencyAmount.setCurrency(BaseCurrencyCodeWithDefault.fromValue(BaseCurrencyCodeWithDefault.USD.value()));
			price.setStandardPrice(overrideCurrencyAmount);

			Map priceSaleMap =  feedDAO.getPriceFeedSaleData(sourceCode, priceFeedDetailVO.getProductId(),
					priceFeedDetailVO.getPremiumPrice());

			BigDecimal salePrice = priceFeedDetailVO.getPremiumPrice();
			boolean hasSale = false;
			Date startDate = null;
			Date endDate = null;

			Double sourceDiscountAmt = (Double) priceSaleMap.get("sourceDiscountAmt");
			String sourceDiscountType = (String) priceSaleMap.get("sourceDiscountType");
			if (sourceDiscountType != null && sourceDiscountAmt > 0) {
				BigDecimal sourcePrice = calculateSalePrice(sourceDiscountAmt, sourceDiscountType,
						priceFeedDetailVO.getPremiumPrice());
                if (sourcePrice.compareTo(priceFeedDetailVO.getPremiumPrice()) < 0) {
                	salePrice = sourcePrice;
                	startDate = (Date) priceSaleMap.get("sourceStartDate");
                	hasSale = true;
                	logger.debug("Source code sale price: " + salePrice);
                }
			}
			
			Double iotwDiscountAmt = (Double) priceSaleMap.get("iotwDiscountAmt");
			String iotwDiscountType = (String) priceSaleMap.get("iotwDiscountType");
			if (iotwDiscountType != null && iotwDiscountAmt > 0) {
				BigDecimal iotwPrice = calculateSalePrice(iotwDiscountAmt, iotwDiscountType,
						priceFeedDetailVO.getPremiumPrice());
                if (iotwPrice.compareTo(salePrice) < 0) {
                	salePrice = iotwPrice;
                	startDate = (Date) priceSaleMap.get("iotwStartDate");
                	endDate = (Date) priceSaleMap.get("iotwEndDate");
                	hasSale = true;
                	logger.debug("IOTW sale price: " + salePrice);
                }
			}

			// If a record exists for IOTW source code, then <sale> node should be constructed.
			if (hasSale){
				Price.Sale sale = new Price.Sale();
				OverrideCurrencyAmount overrideCurrencyAmountSale = new OverrideCurrencyAmount();
				overrideCurrencyAmountSale.setValue(salePrice);
				overrideCurrencyAmountSale.setCurrency(BaseCurrencyCodeWithDefault.fromValue(BaseCurrencyCodeWithDefault.USD.value()));
				sale.setSalePrice(overrideCurrencyAmountSale);
				sale.setStartDate(AmazonUtils.toXMLGregorianCalendar(startDate));
				if (endDate!=null){
					sale.setEndDate(AmazonUtils.toXMLGregorianCalendar(endDate));
				} else {
					ConfigurationUtil cu = ConfigurationUtil.getInstance();
			        String defaultEndDate = cu.getFrpGlobalParm(AmazonConstants.AMZ_GLOBAL_CONTEXT,
		        		AmazonConstants.DEFAULT_END_DATE);
					cal = Calendar.getInstance();
					SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
					cal.setTime(sdf.parse(defaultEndDate));
					sale.setEndDate(AmazonUtils.toXMLGregorianCalendar(cal.getTime()));
				}
				price.setSale(sale);
			}
			priceList.add(price);
		}

		return priceList;
	}

	private BigDecimal calculateSalePrice(Double discount, String discountType, BigDecimal itemPrice) throws Exception {
		logger.info("calculatingSalePrice");
		Double itemPriceBeforeDiscount;
		Double itemPriceAfterDiscount;
		Double itemDiscount;
		if( discount == 0.0 ) 
		{
			itemPriceBeforeDiscount = Double.valueOf(bigDecimalToString(itemPrice));
			return (new BigDecimal(Double.toString(itemPriceBeforeDiscount)));
		}
		else
		{
			if( StringUtils.equals("P",discountType) ) //Percentage discount
			{
				itemPriceBeforeDiscount = Double.valueOf(bigDecimalToString(itemPrice));
				itemDiscount = ((double)itemPriceBeforeDiscount*discount/100);
				itemPriceAfterDiscount = itemPriceBeforeDiscount - itemDiscount;
				DecimalFormat twoDecimalForm = new DecimalFormat("#.##");
				return new BigDecimal(twoDecimalForm.format(itemPriceAfterDiscount));
			}
			else if( StringUtils.equals("D",discountType) ) //Dollar discount
			{
				itemPriceBeforeDiscount = Double.valueOf(bigDecimalToString(itemPrice));
				itemPriceAfterDiscount = itemPriceBeforeDiscount - discount;
				DecimalFormat twoDecimalForm = new DecimalFormat("#.##");
				return new BigDecimal(twoDecimalForm.format(itemPriceAfterDiscount));
			}
			else 
			{
				throw new Exception("Amazon source code is configured with an unsupported discount type of " + discountType );
			}
		}
	}

	private String bigDecimalToString(BigDecimal val)
	{
		if(val == null)
		{
			return null;
		}
		return val.toPlainString();
	}


}
