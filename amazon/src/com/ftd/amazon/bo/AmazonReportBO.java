package com.ftd.amazon.bo;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.amazonaws.mws.MarketplaceWebService;
import com.amazonaws.mws.MarketplaceWebServiceClient;
import com.amazonaws.mws.MarketplaceWebServiceConfig;
import com.amazonaws.mws.MarketplaceWebServiceException;
import com.amazonaws.mws.model.GetReportListRequest;
import com.amazonaws.mws.model.GetReportListResponse;
import com.amazonaws.mws.model.GetReportListResult;
import com.amazonaws.mws.model.GetReportRequest;
import com.amazonaws.mws.model.GetReportResponse;
import com.amazonaws.mws.model.IdList;
import com.amazonaws.mws.model.ReportInfo;
import com.amazonaws.mws.model.UpdateReportAcknowledgementsRequest;
import com.amazonaws.mws.model.UpdateReportAcknowledgementsResponse;
import com.ftd.amazon.common.AmazonConstants;
import com.ftd.amazon.common.AmazonUtils;
import com.ftd.amazon.dao.AmazonDAO;
import com.ftd.amazon.utilities.Utils;
import com.ftd.amazon.utilities.XMLUtilities;
import com.ftd.amazon.vo.SettlementDetailVO;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.JAXPUtil;

public class AmazonReportBO
{

  /**
   * Logger instance
   */
  private Logger logger = new Logger("com.ftd.amazon.bo.AmazonReportBO");

  /**
   * Default constructor
   */
  public AmazonReportBO()
  {
  }

  public void processScheduledReports()
  {
    logger.info("processScheduledReports()");
    boolean result = true;
    boolean hasNext = true;
    String nextToken = null;
    Connection conn = null;

    try
    {
      //instantiate the common utility
      AmazonUtils amzUtils = new AmazonUtils();

      //establish connection
      conn = amzUtils.getNewConnection();

      // retrieve the string values below from frp.global_parms and frp.secure_config
      String appName = amzUtils.getFrpGlobalParm(AmazonConstants.AMZ_GLOBAL_CONTEXT, AmazonConstants.AMZ_NAME_APP_NAME);
      String appVersion = amzUtils.getFrpGlobalParm(AmazonConstants.AMZ_GLOBAL_CONTEXT, AmazonConstants.AMZ_NAME_APP_VERSION);
      String url = amzUtils.getFrpGlobalParm(AmazonConstants.AMZ_GLOBAL_CONTEXT, AmazonConstants.AMZ_NAME_MWS_URL);
      String accessKeyId = amzUtils.getSecureGlobalParm(AmazonConstants.AMZ_SECURE_CONTEXT, AmazonConstants.AMZ_NAME_ACCESS_KEY_ID);
      String secretAccessKey = amzUtils.getSecureGlobalParm(AmazonConstants.AMZ_SECURE_CONTEXT, AmazonConstants.AMZ_NAME_SECRET_ACCESS_KEY);
      String merchantId = amzUtils.getFrpGlobalParm(AmazonConstants.AMZ_GLOBAL_CONTEXT, AmazonConstants.AMZ_NAME_MERCHANT_ID);

      MarketplaceWebServiceConfig config = new MarketplaceWebServiceConfig();
      config.setServiceURL(url);

      //instantiate service
      MarketplaceWebService service = new MarketplaceWebServiceClient(accessKeyId, secretAccessKey, appName, appVersion, config);

      /** REQUEST **/
      //instantiate request and set parms
      GetReportListRequest request = new GetReportListRequest();
      request.setMerchant(merchantId);
      request.setAcknowledged(false);

      /** RESPONSE **/
      //define response
      GetReportListResponse response = null;

      //response will consist of report list
      GetReportListResult getReportListResult = null;

      //report list will consist of a list report info
      java.util.List<ReportInfo> reportInfoList = null;
      List reportIdList;

      //report can only have 10 ReportInfoList nodes max.  If more exist, hasNext = true
      while (hasNext)
      {
        logger.info("Calling getReportList");
        response = service.getReportList(request);
        logger.info("getReportList - Response = " + response.toXML());

        //check if the response contains non-null getReportListResult  
        if (response.isSetGetReportListResult())
        {
          //instantiate the reportIdList
          reportIdList = new ArrayList();

          //get the getReportListResult
          getReportListResult = response.getGetReportListResult();

          //retrieve the HasNext value
          if (getReportListResult.isSetHasNext())
          {
            hasNext = getReportListResult.isHasNext();
          }

          //retrieve the NextToken value
          if (getReportListResult.isSetNextToken())
          {
            nextToken = getReportListResult.getNextToken();
          }

          //retrieve all the ReportInfoList in a list
          reportInfoList = getReportListResult.getReportInfoList();

          logger.info("ReportInfoList has " + reportInfoList.size() + " reports");

          //Loop thru each ReportInfo
          for (ReportInfo reportInfo : reportInfoList)
          {
            logger.info("getReportId = " + reportInfo.getReportId());
            logger.info("isSetReportType = " + reportInfo.isSetReportType());
            logger.info("getReportType has " + reportInfo.getReportType());

            //reportIdList.add(reportInfo.getReportId());

            if (reportInfo.isSetReportType()) {
            	if (reportInfo.getReportType().equalsIgnoreCase(AmazonConstants.ORDER_REPORT)) {
                    //call method to get order report
                    processOrderReport(service, conn, reportInfo, merchantId);
                    reportIdList.add(reportInfo.getReportId());
                } else if (reportInfo.getReportType().equalsIgnoreCase(AmazonConstants.SETTLEMENT_REPORT)) {
                    //call method to get order report
                    saveSettlementReport(service, conn, reportInfo, merchantId);
                    reportIdList.add(reportInfo.getReportId());
                } else if (reportInfo.getReportType().equalsIgnoreCase(AmazonConstants.FEED_SUMMARY_REPORT)) {
                	reportIdList.add(reportInfo.getReportId());
                } else if (reportInfo.getReportType().equalsIgnoreCase(AmazonConstants.V2_SETTLEMENT_REPORT)) {
                	reportIdList.add(reportInfo.getReportId());
                } else {
                	String errorMsg = "Invalid Amazon report: " + reportInfo.getReportId() +
                	        " " + reportInfo.getReportType();
                	logger.error(errorMsg);
                	amzUtils.sendNoPageSystemMessage(errorMsg);
                    reportIdList.add(reportInfo.getReportId());
                }
            }
          }

          //and confirm report
          if (reportIdList.size() > 0)
            confirmReport(service, reportIdList, merchantId);
        }
        else
          hasNext = false;
      }
    }
    catch (MarketplaceWebServiceException ex)
    {
      logger.error("Caught Exception: " + ex.getMessage());
      logger.error("Response Status Code: " + ex.getStatusCode());
      logger.error("Error Code: " + ex.getErrorCode());
      logger.error("Error Type: " + ex.getErrorType());
      logger.error("Request ID: " + ex.getRequestId());
      logger.error("XML: " + ex.getXML());
    }
    catch (Exception e)
    {
      logger.error(e);
      result = false;
    }
    finally
    {
      if (conn != null)
      {
        try
        {
          conn.close();
        }
        catch (Exception e)
        {
          logger.error("Unable to close connection: " + e);
        }
      }
    }

    logger.info("result: " + result);
  }

  /**
   * This method receives ReportInfo -> its a VO that contains the report id.
   * The method will retrieve this report id, and call GetReport using this
   * report id. GetReport will populate the order in report.xml
   * stream. This method will then read the stream, transform, store order
   * report, parse order report, store inidividual orders, and send
   * JMS for each order.
   * 
   * @param service
   *          - Amazon Marketplace Web Service
   * @param conn
   *          - Database Connection
   * @param reportIdList
   *          - list of report ids
   * @param merchantId
   *          - FTD merchant id
   * @return void
   * @throws Exception
   */
  private void processOrderReport(MarketplaceWebService service, Connection conn, ReportInfo reportInfo, String merchantId) throws Exception
  {

    //get order report
    GetReportRequest request = new GetReportRequest();
    OutputStream reportStream = new FileOutputStream("report.xml");
    request.setReportOutputStream(reportStream);
    request.setMerchant(merchantId);
    request.setReportId(reportInfo.getReportId());

    logger.info("Calling getReport");
    GetReportResponse response = service.getReport(request);
    logger.info("getReport - Response = " + response.toXML());

    BufferedReader br = new BufferedReader(new FileReader("report.xml"));

    String line;
    String reportString;
    StringBuffer sb = new StringBuffer();
    while ((line = br.readLine()) != null)
    {
      sb.append(line);
    }

    reportString = sb.toString();
    logger.info(reportString);

    if (reportStream != null)
      reportStream.close();
    if (br != null)
      br.close();

    //store order report in AMAZON -> AZ_ORDER_REPORT
    AmazonDAO amzDAO = new AmazonDAO(conn);

    //insert the amazon report in the database
    amzDAO.insertAmazonOrderReport(reportInfo.getReportId(), reportString);

    //parse thru the <Message> and for each MessageID
    HashMap orderHash = (HashMap) splitOrderReport(reportString);

    //store in AMAZON -> AZ_ORDER and enqueue EM_AMAZON.  Action = PROCESS-INBOUND-ORDER, corrid = ??
    insertOrderSendJMS(conn, reportInfo.getReportId(), orderHash);

  }

  /**
   * This method receives a list of report ids, transforms, and acknowldeges the
   * reports to Amazon
   * 
   * @param service
   *          - Amazon Marketplace Web Service
   * @param reportIdList
   *          - list of report ids
   * @param merchantId
   *          - FTD merchant id
   * @return void
   * @throws Exception
   */
  private void confirmReport(MarketplaceWebService service, List reportIdList, String merchantId) throws Exception
  {

    IdList idList = new IdList();
    idList.setId(reportIdList);

    UpdateReportAcknowledgementsRequest request = new UpdateReportAcknowledgementsRequest();
    request.setMerchant(merchantId);
    request.setAcknowledged(true);
    request.setReportIdList(idList);

    logger.info("Calling updateReportAcknowledgements");
    UpdateReportAcknowledgementsResponse response = service.updateReportAcknowledgements(request);
    logger.info("updateReportAcknowledgements - Response = " + response.toXML());

  }

  /**
   * Splits an order report up into individual orders. This method will:
   * 1. Loop through the order report and retrieve the individual orders.
   * 2. Return a HashMap of individual orders.
   * 
   * @param reportString
   *          - String representation of report stream
   * @return orderHash
   *         - hashmap of orders. key = amazon order number, value = string
   *         representation of <OrderReport>
   * @throws Exception
   */
  public HashMap splitOrderReport(String reportString) throws Exception
  {
    //document that will contain the order report.  note that the raw order report can have 1 to many <OrderReport>
    Document orderReportDocument = DOMUtil.getDocument(reportString);

    //hashmap where key = amazon order number and value = individual order report - this is XML document in string format
    HashMap orderHash = new HashMap();

    //Create an XPath object to parse the XML document:
    String orderReportXPath = "//Message/OrderReport";
    String orderIdXPath = "//OrderReport/AmazonOrderID";

    //Node for each <OrderReport>
    Node orderNode = null;

    //xml document for each order
    Document orderDocument;

    //xml document for each order in string format - to be stored in hashmap
    String orderString;

    //amazon order number - key in the hashmap
    String amazonOrderNumber;

    //get all <OrderReport> nodes
    NodeList nl = DOMUtil.selectNodes(orderReportDocument, orderReportXPath);

    //and iterate thru them
    for (int i = 0; i < nl.getLength(); i++)
    {
      //retrieve each <OrderReport> node
      orderNode = (Node) nl.item(i);

      if (orderNode != null)
      {
        //create a document that will contain the order document
        orderDocument = (Document) DOMUtil.getDefaultDocument();

        //create a parent level element <OrderReport>
        Element orderReportElement = (Element) orderDocument.createElement("OrderReport");

        //append parennt
        orderDocument.appendChild(orderReportElement);

        //and order node to the order document
        DOMUtil.addSection(orderDocument, orderNode.getChildNodes());

        //retrieve the amazon order number
        amazonOrderNumber = (DOMUtil.selectSingleNode(orderDocument, orderIdXPath)).getFirstChild().getNodeValue();

        //convert order document to string
        orderString = DOMUtil.convertToString(orderDocument);
        logger.info("amazonOrderNumber = " + amazonOrderNumber);
        logger.info("orderString = " + orderString);

        //and save String representation of order dcument in a hashmap.... the key = amazon order number
        orderHash.put(amazonOrderNumber, orderString);
      }
    }

    return orderHash;
  }

  /**
   * This method receives a hashmap of orders. It inserts them in AZ_ORDER table
   * and then spawn a JMS message for each order
   * 
   * @param conn
   *          - Database Connection
   * @param reportId
   *          - Report id associated with orders in this hashmap
   * @param orderHash
   *          - hashmap of orders. key = amazon order number, value = string
   *          representation of <OrderReport>
   * @return void
   * @throws Exception
   */
  public void insertOrderSendJMS(Connection conn, String reportId, HashMap orderHash) throws Exception
  {

    //store order report in AMAZON -> AZ_ORDER_REPORT
    AmazonDAO amzDAO = new AmazonDAO(conn);

    Set ks = orderHash.keySet();
    Iterator iter = ks.iterator();
    String amazonOrderNumber;
    String masterOrderNumber;
    String orderStatus = AmazonConstants.ORDER_STATUS_RECEIVED;
    boolean success;

    //Iterate thru the hashmap returned from the business object using the keyset
    while (iter.hasNext())
    {
      success = false;
      amazonOrderNumber = iter.next().toString();
      masterOrderNumber = "A" + StringUtils.replaceChars(amazonOrderNumber, "-", "");
      String orderString = (String) orderHash.get(amazonOrderNumber);

      //insert record in AZ_ORDER
      amzDAO.insertAmazonOrder(amazonOrderNumber, masterOrderNumber, reportId, orderString, AmazonConstants.ORDER_STATUS_RECEIVED);

      success = AmazonUtils.sendJMSMessage(AmazonConstants.JMS_PIPELINE_FOR_EM_AMAZON, AmazonConstants.PROCESS_INBOUND_ORDER, amazonOrderNumber);

      if (!success)
      {
        throw new Exception("Unable to send JMS message for amazon order number = " + amazonOrderNumber);
      }

    }

  }

  //SettlementReport
  private void saveSettlementReport(MarketplaceWebService service, Connection conn, ReportInfo reportInfo, String merchantId) throws Exception
  {
    //get settlement report
      
    GetReportRequest request = new GetReportRequest();
    OutputStream reportStream = new FileOutputStream("report.xml");
    request.setReportOutputStream( reportStream );
    request.setMerchant(merchantId);
    request.setReportId(reportInfo.getReportId());

    logger.info("Calling getReport");
    GetReportResponse response = service.getReport(request);
    logger.info("getReport - Response = " + response.toXML());
    logger.info("getReport - Response = " + response.getGetReportResult().getMD5Checksum());
    logger.info("request.getReportOutputStream().toString() = " + request.getReportOutputStream().toString() );
    
    BufferedReader br = new BufferedReader(new FileReader("report.xml"));

    String line;
    String reportString;
    StringBuffer sb = new StringBuffer();
    while ((line = br.readLine()) != null)
    {
      sb.append(line);
    }

    reportString = sb.toString();
    logger.info(reportString);

    if (reportStream != null)
      reportStream.close();
    if (br != null)
      br.close();

    String settlementXML = reportString;
    if(settlementXML == null || settlementXML.equals("")) {
    	logger.error("Settlement Report XML is null");
    } else {
    
    try {
  	  Document settlementDoc = JAXPUtil.parseDocument(settlementXML);
  	  
  	  String azReportId = XMLUtilities.selectSingleNodeText(settlementDoc, "/AmazonEnvelope/Message/SettlementReport/SettlementData/AmazonSettlementID");
	    if (azReportId == null || azReportId.equals("")) {
	    	logger.error("Missing Amazon Settlement Report Id");	    	
	    } else {	        
	
	        AmazonDAO amazonDAO = new AmazonDAO(conn);
	        amazonDAO.insertAmazonSettlementReport(azReportId, DOMUtil.domToString(settlementDoc));
	
		    AmazonUtils.sendJMSMessage(AmazonConstants.JMS_PIPELINE_FOR_EM_AMAZON,
				    AmazonConstants.PROCESS_SETTLEMENT_REPORT, azReportId);
		    
	    }
  	    	  
    } catch (Exception e) {
  	  logger.error(e);
    }
    }
  }
  //SettlementReport
  
  public void processSettlementReport(String settlementReportId) {
	logger.info("processSettlementReport(" + settlementReportId + ")");

    Connection conn = null;
	try {
	    conn = DataSourceUtil.getInstance().getConnection("ORDER SCRUB");
		AmazonDAO amazonDAO = new AmazonDAO(conn);	        
		String amazonSettlementXML = amazonDAO.getAmazonSettlementXML(settlementReportId);
		amazonSettlementXML = amazonSettlementXML.replaceAll("\\r|\\n", ""); 
		Document settlementDoc = JAXPUtil.parseDocument(amazonSettlementXML);

	    //Create an XPath object to parse the XML document:
		String settlementDataXPath = "//Message/SettlementReport/SettlementData";
	    String orderReportXPath = "//Message/SettlementReport/Order";
	    String adjustmentReportXPath = "//Message/SettlementReport/Adjustment";
	    String otherReportXPath = "//Message/SettlementReport/OtherTransaction";
	    String orderIdXPath = "//Order/AmazonOrderID";
	    String adjustmentIdXPath = "//Adjustment/AmazonOrderID";
	    
	    logger.info("Settlement data");
	    String settlementId = "";
	    NodeList nl = DOMUtil.selectNodes(settlementDoc, settlementDataXPath);
	    if (nl != null && nl.getLength() > 0) {
	    	Document tempDoc = (Document) DOMUtil.getDefaultDocument();
	        Element orderElement = (Element) tempDoc.createElement("SettlementData");
	        tempDoc.appendChild(orderElement);
	        
	        BigDecimal totalAmount = new BigDecimal(0);
	        Date startDate = null;
	        Date endDate = null;
	        Date depositDate = null;
	    	
	        Node settlementNode = (Node) nl.item(0);
	        DOMUtil.addSection(tempDoc, settlementNode.getChildNodes());
	        logger.info(JAXPUtil.toString(tempDoc));

			String strTmp = XMLUtilities.selectSingleNodeText(tempDoc,"/SettlementData/AmazonSettlementID");
			logger.info(strTmp);
			settlementId = strTmp;

			strTmp = XMLUtilities.selectSingleNodeText(tempDoc,"/SettlementData/TotalAmount");
			logger.info(strTmp);
			if (strTmp != null && !strTmp.equals("")) {
				totalAmount = new BigDecimal(strTmp);
			}

			strTmp = XMLUtilities.selectSingleNodeText(tempDoc,"/SettlementData/StartDate");
			logger.info(strTmp);
			if (strTmp != null && !strTmp.equals("")) {
			    startDate = Utils.azDateString2Date(strTmp);
			}

			strTmp = XMLUtilities.selectSingleNodeText(tempDoc,"/SettlementData/EndDate");
			logger.info(strTmp);
			if (strTmp != null && !strTmp.equals("")) {
			    endDate = Utils.azDateString2Date(strTmp);
			}

			strTmp = XMLUtilities.selectSingleNodeText(tempDoc,"/SettlementData/DepositDate");
			logger.info(strTmp);
			if (strTmp != null && !strTmp.equals("")) {
			    depositDate = Utils.azDateString2Date(strTmp);
			}

			amazonDAO.insertSettlementData(settlementId, totalAmount, startDate, endDate, depositDate);
	    }
	    
	    //get all <Order> nodes
	    nl = DOMUtil.selectNodes(settlementDoc, orderReportXPath);

	    for (int i = 0; i < nl.getLength(); i++) {
	    	Document tempDoc = (Document) DOMUtil.getDefaultDocument();
	        Element orderElement = (Element) tempDoc.createElement("Order");
	        tempDoc.appendChild(orderElement);
	        
	        Node orderNode = (Node) nl.item(i);
	        DOMUtil.addSection(tempDoc, orderNode.getChildNodes());
	        logger.debug(JAXPUtil.toString(tempDoc));
	    	String amazonOrderNumber = DOMUtil.selectSingleNode(tempDoc, orderIdXPath).getFirstChild().getNodeValue();
	    	logger.debug("amazonOrderNumber: " + amazonOrderNumber);
	    	String amazonItemNumber = "";

			String strTmp = XMLUtilities.selectSingleNodeText(tempDoc,"/Order/Fulfillment/PostedDate");
			logger.debug("postedDate: " + strTmp);
	        Date postedDate = null;
			if (strTmp != null && !strTmp.equals("")) {
			    postedDate = Utils.azDateString2Date(strTmp);
			}

	        NodeList nlItem = XMLUtilities.selectXPathNodes(tempDoc,"/Order/Fulfillment/Item");

	        if(nlItem != null) {
	            for(int j=0; j<nlItem.getLength(); j++) {
	                Element itemElement = (Element)nlItem.item(j);

	                NodeList nlOrder = itemElement.getChildNodes();
	                for (int k=0; k<nlOrder.getLength(); k++) {
	                    Element rec = (Element) nlOrder.item(k);
	                    String nodeName = rec.getNodeName();
	                    //logger.debug(nodeName);
                        if (nodeName.equalsIgnoreCase("AmazonOrderItemCode")) {
                            Node thisNode = rec.getFirstChild();
                            String value = thisNode.getNodeValue();
                            logger.debug("azOrderItemCode: " + value);
                            amazonItemNumber = value;
                        } else if (nodeName.equalsIgnoreCase("ItemPrice")) {
                        	logger.debug("ItemPrice");
        	                SettlementDetailVO sdVO = new SettlementDetailVO();
        	                sdVO.setSettlementId(settlementId);
        	                sdVO.setAmazonOrderNumber(amazonOrderNumber);
        	                sdVO.setTransactionType("Order");
        	                sdVO.setPostedDate(postedDate);
                            sdVO.setAmazonItemNumber(amazonItemNumber);
                            NodeList nlComponents = rec.getChildNodes();
                            for (int a=0; a<nlComponents.getLength(); a++) {
                                Element components = (Element) nlComponents.item(a);
                                NodeList thisComponent = components.getChildNodes();
                                String cType = "";
                                String cAmount = "";
                                for (int b=0; b<thisComponent.getLength(); b++) {
                                    Element cDetails = (Element) thisComponent.item(b);
                                    String cName = cDetails.getNodeName();
                                    String cValue = "";
                                    if (cDetails.hasChildNodes()) {
                                        Node nn = cDetails.getFirstChild();
                                        cValue = nn.getNodeValue();
                                    }
                                    if (cName != null & cName.equalsIgnoreCase("Type")) {
                                    	cType = cValue;
                                    } else if (cName != null & cName.equalsIgnoreCase("Amount")) {
                                    	cAmount = cValue;
                                    }
                                }
                                logger.debug("  " + cType + " " + cAmount);
                                sdVO.setTransactionName(cType);
                                if (cAmount != null && !cAmount.equals("")) {
                                	sdVO.setTransactionAmount(new BigDecimal(cAmount));
                                } else {
                                	sdVO.setTransactionAmount(new BigDecimal("0"));
                                }
                                amazonDAO.insertSettlementDetail(sdVO);
                            }
                        } else if (nodeName.equalsIgnoreCase("ItemFees")) {
                        	logger.debug("ItemFees");
        	                SettlementDetailVO sdVO = new SettlementDetailVO();
        	                sdVO.setSettlementId(settlementId);
        	                sdVO.setAmazonOrderNumber(amazonOrderNumber);
        	                sdVO.setTransactionType("Order");
        	                sdVO.setPostedDate(postedDate);
                            sdVO.setAmazonItemNumber(amazonItemNumber);
                            NodeList nlFees = rec.getChildNodes();
                            for (int a=0; a<nlFees.getLength(); a++) {
                                Element fees = (Element) nlFees.item(a);
                                NodeList thisFee = fees.getChildNodes();
                                String cType = "";
                                String cAmount = "";
                                for (int b=0; b<thisFee.getLength(); b++) {
                                    Element cDetails = (Element) thisFee.item(b);
                                    String cName = cDetails.getNodeName();
                                    String cValue = "";
                                    if (cDetails.hasChildNodes()) {
                                        Node nn = cDetails.getFirstChild();
                                        cValue = nn.getNodeValue();
                                    }
                                    if (cName != null & cName.equalsIgnoreCase("Type")) {
                                    	cType = cValue;
                                    } else if (cName != null & cName.equalsIgnoreCase("Amount")) {
                                    	cAmount = cValue;
                                    }
                                }
                                logger.debug("  " + cType + " " + cAmount);
                                sdVO.setTransactionName(cType);
                                if (cAmount != null && !cAmount.equals("")) {
                                	sdVO.setTransactionAmount(new BigDecimal(cAmount));
                                } else {
                                	sdVO.setTransactionAmount(new BigDecimal("0"));
                                }
                                amazonDAO.insertSettlementDetail(sdVO);
                            }
                        }
	                }

	            }
	        }

	    }
	    
	    //get all <Adjustment> nodes
	    nl = DOMUtil.selectNodes(settlementDoc, adjustmentReportXPath);

	    for (int i = 0; i < nl.getLength(); i++) {
	    	Document tempDoc = (Document) DOMUtil.getDefaultDocument();
	        Element orderElement = (Element) tempDoc.createElement("Adjustment");
	        tempDoc.appendChild(orderElement);
	        
	        Node orderNode = (Node) nl.item(i);
	        DOMUtil.addSection(tempDoc, orderNode.getChildNodes());
	        logger.debug(JAXPUtil.toString(tempDoc));
	    	String amazonOrderNumber = DOMUtil.selectSingleNode(tempDoc, adjustmentIdXPath).getFirstChild().getNodeValue();
	    	logger.debug("amazonOrderNumber: " + amazonOrderNumber);
	    	String amazonItemNumber = "";

			String strTmp = XMLUtilities.selectSingleNodeText(tempDoc,"/Adjustment/Fulfillment/PostedDate");
			logger.debug("postedDate: " + strTmp);
	        Date postedDate = null;
			if (strTmp != null && !strTmp.equals("")) {
			    postedDate = Utils.azDateString2Date(strTmp);
			}

	        NodeList nlItem = XMLUtilities.selectXPathNodes(tempDoc,"/Adjustment/Fulfillment/AdjustedItem");

	        if(nlItem != null) {
	            for(int j=0; j<nlItem.getLength(); j++) {
	                Element itemElement = (Element)nlItem.item(j);

	                NodeList nlOrder = itemElement.getChildNodes();
	                for (int k=0; k<nlOrder.getLength(); k++) {
	                    Element rec = (Element) nlOrder.item(k);
	                    String nodeName = rec.getNodeName();
	                    //logger.debug(nodeName);
                        if (nodeName.equalsIgnoreCase("AmazonOrderItemCode")) {
                            Node thisNode = rec.getFirstChild();
                            String value = thisNode.getNodeValue();
                            logger.debug("azOrderItemCode: " + value);
                            amazonItemNumber = value;
                        } else if (nodeName.equalsIgnoreCase("ItemPriceAdjustments")) {
                        	logger.debug("ItemPriceAdjustments");
        	                SettlementDetailVO sdVO = new SettlementDetailVO();
        	                sdVO.setSettlementId(settlementId);
        	                sdVO.setAmazonOrderNumber(amazonOrderNumber);
        	                sdVO.setTransactionType("Adjustment");
        	                sdVO.setPostedDate(postedDate);
                            sdVO.setAmazonItemNumber(amazonItemNumber);
                            NodeList nlComponents = rec.getChildNodes();
                            for (int a=0; a<nlComponents.getLength(); a++) {
                                Element components = (Element) nlComponents.item(a);
                                NodeList thisComponent = components.getChildNodes();
                                String cType = "";
                                String cAmount = "";
                                for (int b=0; b<thisComponent.getLength(); b++) {
                                    Element cDetails = (Element) thisComponent.item(b);
                                    String cName = cDetails.getNodeName();
                                    String cValue = "";
                                    if (cDetails.hasChildNodes()) {
                                        Node nn = cDetails.getFirstChild();
                                        cValue = nn.getNodeValue();
                                    }
                                    if (cName != null & cName.equalsIgnoreCase("Type")) {
                                    	cType = cValue;
                                    } else if (cName != null & cName.equalsIgnoreCase("Amount")) {
                                    	cAmount = cValue;
                                    }
                                }
                                logger.debug("  " + cType + " " + cAmount);
                                sdVO.setTransactionName(cType);
                                if (cAmount != null && !cAmount.equals("")) {
                                	sdVO.setTransactionAmount(new BigDecimal(cAmount));
                                } else {
                                	sdVO.setTransactionAmount(new BigDecimal("0"));
                                }
                                amazonDAO.insertSettlementDetail(sdVO);
                            }
                        } else if (nodeName.equalsIgnoreCase("ItemFeeAdjustments")) {
                        	logger.debug("ItemFeeAdjustments");
        	                SettlementDetailVO sdVO = new SettlementDetailVO();
        	                sdVO.setSettlementId(settlementId);
        	                sdVO.setAmazonOrderNumber(amazonOrderNumber);
        	                sdVO.setTransactionType("Adjustment");
        	                sdVO.setPostedDate(postedDate);
                            sdVO.setAmazonItemNumber(amazonItemNumber);
                            NodeList nlFees = rec.getChildNodes();
                            for (int a=0; a<nlFees.getLength(); a++) {
                                Element fees = (Element) nlFees.item(a);
                                NodeList thisFee = fees.getChildNodes();
                                String cType = "";
                                String cAmount = "";
                                for (int b=0; b<thisFee.getLength(); b++) {
                                    Element cDetails = (Element) thisFee.item(b);
                                    String cName = cDetails.getNodeName();
                                    String cValue = "";
                                    if (cDetails.hasChildNodes()) {
                                        Node nn = cDetails.getFirstChild();
                                        cValue = nn.getNodeValue();
                                    }
                                    if (cName != null & cName.equalsIgnoreCase("Type")) {
                                    	cType = cValue;
                                    } else if (cName != null & cName.equalsIgnoreCase("Amount")) {
                                    	cAmount = cValue;
                                    }
                                }
                                logger.debug("  " + cType + " " + cAmount);
                                sdVO.setTransactionName(cType);
                                if (cAmount != null && !cAmount.equals("")) {
                                	sdVO.setTransactionAmount(new BigDecimal(cAmount));
                                } else {
                                	sdVO.setTransactionAmount(new BigDecimal("0"));
                                }
                                amazonDAO.insertSettlementDetail(sdVO);
                            }
                        }
	                }

	            }
	        }

	    }

	    //get all <OtherTransaction> nodes
	    nl = DOMUtil.selectNodes(settlementDoc, otherReportXPath);

	    for (int i = 0; i < nl.getLength(); i++) {
	    	Document tempDoc = (Document) DOMUtil.getDefaultDocument();
	        Element orderElement = (Element) tempDoc.createElement("OtherTransaction");
	        tempDoc.appendChild(orderElement);
	        
	        Node orderNode = (Node) nl.item(i);
	        DOMUtil.addSection(tempDoc, orderNode.getChildNodes());
	        logger.debug(JAXPUtil.toString(tempDoc));
	    	String transactionType = XMLUtilities.selectSingleNodeText(tempDoc,"/OtherTransaction/TransactionType");
	    	logger.debug("transactionType: " + transactionType);

			String strTmp = XMLUtilities.selectSingleNodeText(tempDoc,"/OtherTransaction/PostedDate");
			logger.debug("postedDate: " + strTmp);
	        Date postedDate = null;
			if (strTmp != null && !strTmp.equals("")) {
			    postedDate = Utils.azDateString2Date(strTmp);
			}

			strTmp = XMLUtilities.selectSingleNodeText(tempDoc,"/OtherTransaction/Amount");
			logger.debug("amount: " + strTmp);

            SettlementDetailVO sdVO = new SettlementDetailVO();
            sdVO.setSettlementId(settlementId);
            sdVO.setTransactionType("OtherTransaction");
            sdVO.setPostedDate(postedDate);
            sdVO.setTransactionName(transactionType);
            if (strTmp != null && !strTmp.equals("")) {
            	sdVO.setTransactionAmount(new BigDecimal(strTmp));
            } else {
            	sdVO.setTransactionAmount(new BigDecimal("0"));
            }
            amazonDAO.insertSettlementDetail(sdVO);

	    }

/*
	    //get all <Order> nodes
	    nl = DOMUtil.selectNodes(settlementDoc, orderReportXPath);

	    //and iterate thru them
	    for (int i = 0; i < nl.getLength(); i++) {
	    	Document tempDoc = (Document) DOMUtil.getDefaultDocument();
	        Element orderElement = (Element) tempDoc.createElement("Order");
	        tempDoc.appendChild(orderElement);
	        
	        Node orderNode = (Node) nl.item(i);
	        DOMUtil.addSection(tempDoc, orderNode.getChildNodes());
	        logger.info(JAXPUtil.toString(tempDoc));
	    	String amazonOrderNumber = DOMUtil.selectSingleNode(tempDoc, orderIdXPath).getFirstChild().getNodeValue();
	    	logger.info("amazonOrderNumber: " + amazonOrderNumber);

			String strTmp = XMLUtilities.selectSingleNodeText(tempDoc,"/Order/Fulfillment/PostedDate");
			logger.info(strTmp);
	        Date postedDate = null;
			if (strTmp != null && !strTmp.equals("")) {
			    postedDate = Utils.azDateString2Date(strTmp);
			}

	        NodeList nlItem = XMLUtilities.selectXPathNodes(tempDoc,"/Order/Fulfillment/Item");

	        if(nlItem != null) {
	            for(int j=0; j<nlItem.getLength(); j++) {
	                Element itemElement = (Element)nlItem.item(j);
	                SettlementDetailVO sdVO = new SettlementDetailVO();
	                sdVO.setSettlementId(settlementId);
	                sdVO.setAmazonOrderNumber(amazonOrderNumber);
	                sdVO.setTransactionType("Order");
	                sdVO.setPostedDate(postedDate);

                    String azOrderItemCode = XMLUtilities.selectSingleNodeText(itemElement,"./AmazonOrderItemCode");
                    logger.info("azOrderItemCode: " + azOrderItemCode);
                    sdVO.setAmazonOrderItemNumber(azOrderItemCode);

		            strTmp = XMLUtilities.selectSingleNodeText(itemElement,"./ItemPrice/Component[Type='Principal']/Amount");
		            logger.info("Principal: " + strTmp);
		            if (strTmp != null && !strTmp.equals("")) {
		            	sdVO.setPrincipalAmount(new BigDecimal(strTmp));
		            }
		              
		            strTmp = XMLUtilities.selectSingleNodeText(itemElement,"./ItemPrice/Component[Type='Shipping']/Amount");
		            logger.info("Shipping: " + strTmp);
		            if (strTmp != null && !strTmp.equals("")) {
		            	sdVO.setShippingAmount(new BigDecimal(strTmp));
		            }
		              
		            strTmp = XMLUtilities.selectSingleNodeText(itemElement,"./ItemPrice/Component[Type='Tax']/Amount");
		            logger.info("Tax: " + strTmp);
		            if (strTmp != null && !strTmp.equals("")) {
		            	sdVO.setTaxAmount(new BigDecimal(strTmp));
		            }
		              
		            strTmp = XMLUtilities.selectSingleNodeText(itemElement,"./ItemPrice/Component[Type='ShippingTax']/Amount");
		            logger.info("ShippingTax: " + strTmp);
		            if (strTmp != null && !strTmp.equals("")) {
		            	sdVO.setShippingTaxAmount(new BigDecimal(strTmp));
		            }

		            strTmp = XMLUtilities.selectSingleNodeText(itemElement,"./ItemFees/Fee[Type='Commission']/Amount");
		            logger.info("Commission: " + strTmp);
		            if (strTmp != null && !strTmp.equals("")) {
		            	sdVO.setCommissionAmount(new BigDecimal(strTmp));
		            }

		            amazonDAO.insertSettlementDetail(sdVO);
	            }
	        }

	    }
	    
	    //get all <Order> nodes
	    nl = DOMUtil.selectNodes(settlementDoc, adjustmentReportXPath);

	    //and iterate thru them
	    for (int i = 0; i < nl.getLength(); i++) {
	    	Document tempDoc = (Document) DOMUtil.getDefaultDocument();
	        Element orderElement = (Element) tempDoc.createElement("Adjustment");
	        tempDoc.appendChild(orderElement);
	        
	        Node orderNode = (Node) nl.item(i);
	        DOMUtil.addSection(tempDoc, orderNode.getChildNodes());
	        logger.info(JAXPUtil.toString(tempDoc));
	    	String amazonOrderNumber = DOMUtil.selectSingleNode(tempDoc, adjustmentIdXPath).getFirstChild().getNodeValue();
	    	logger.info("amazonOrderNumber: " + amazonOrderNumber);

			String strTmp = XMLUtilities.selectSingleNodeText(tempDoc,"/Adjustment/Fulfillment/PostedDate");
			logger.info(strTmp);
	        Date postedDate = null;
			if (strTmp != null && !strTmp.equals("")) {
			    postedDate = Utils.azDateString2Date(strTmp);
			}

	        NodeList nlItem = XMLUtilities.selectXPathNodes(tempDoc,"/Adjustment/Fulfillment/AdjustedItem");

	        if(nlItem != null) {
	            for(int j=0; j<nlItem.getLength(); j++) {
	                Element itemElement = (Element)nlItem.item(j);
	                SettlementDetailVO sdVO = new SettlementDetailVO();
	                sdVO.setSettlementId(settlementId);
	                sdVO.setAmazonOrderNumber(amazonOrderNumber);
	                sdVO.setTransactionType("Adjustment");
	                sdVO.setPostedDate(postedDate);
	                
                    String azOrderItemCode = XMLUtilities.selectSingleNodeText(itemElement,"./AmazonOrderItemCode");
                    logger.info("azOrderItemCode: " + azOrderItemCode);
                    sdVO.setAmazonOrderItemNumber(azOrderItemCode);

		            strTmp = XMLUtilities.selectSingleNodeText(itemElement,"./ItemPrice/Component[Type='Principal']/Amount");
		            logger.info("Principal: " + strTmp);
		            if (strTmp != null && !strTmp.equals("")) {
		            	sdVO.setPrincipalAmount(new BigDecimal(strTmp));
		            }
		              
		            strTmp = XMLUtilities.selectSingleNodeText(itemElement,"./ItemPrice/Component[Type='Shipping']/Amount");
		            logger.info("Shipping: " + strTmp);
		            if (strTmp != null && !strTmp.equals("")) {
		            	sdVO.setShippingAmount(new BigDecimal(strTmp));
		            }
		              
		            strTmp = XMLUtilities.selectSingleNodeText(itemElement,"./ItemPrice/Component[Type='Tax']/Amount");
		            logger.info("Tax: " + strTmp);
		            if (strTmp != null && !strTmp.equals("")) {
		            	sdVO.setTaxAmount(new BigDecimal(strTmp));
		            }
		              
		            strTmp = XMLUtilities.selectSingleNodeText(itemElement,"./ItemPrice/Component[Type='ShippingTax']/Amount");
		            logger.info("ShippingTax: " + strTmp);
		            if (strTmp != null && !strTmp.equals("")) {
		            	sdVO.setShippingTaxAmount(new BigDecimal(strTmp));
		            }

		            strTmp = XMLUtilities.selectSingleNodeText(itemElement,"./ItemFees/Fee[Type='Commission']/Amount");
		            logger.info("Commission: " + strTmp);
		            if (strTmp != null && !strTmp.equals("")) {
		            	sdVO.setCommissionAmount(new BigDecimal(strTmp));
		            }

		            amazonDAO.insertSettlementDetail(sdVO);

	            }
	        }
	    }

	    //get all <OtherTransaction> nodes
	    nl = DOMUtil.selectNodes(settlementDoc, otherReportXPath);

	    //and iterate thru them
	    for (int i = 0; i < nl.getLength(); i++) {
	    	Document tempDoc = (Document) DOMUtil.getDefaultDocument();
	        Element orderElement = (Element) tempDoc.createElement("OtherTransaction");
	        tempDoc.appendChild(orderElement);
	        
	        Node orderNode = (Node) nl.item(i);
	        DOMUtil.addSection(tempDoc, orderNode.getChildNodes());
	        logger.info(JAXPUtil.toString(tempDoc));

			String strTmp = XMLUtilities.selectSingleNodeText(tempDoc,"/OtherTransaction/PostedDate");
			logger.info(strTmp);
	        Date postedDate = null;
			if (strTmp != null && !strTmp.equals("")) {
			    postedDate = Utils.azDateString2Date(strTmp);
			}

            SettlementDetailVO sdVO = new SettlementDetailVO();
            sdVO.setSettlementId(settlementId);
			strTmp = XMLUtilities.selectSingleNodeText(tempDoc,"/OtherTransaction/TransactionType");
			logger.info(strTmp);
            sdVO.setTransactionType(strTmp);
            sdVO.setPostedDate(postedDate);
            
            strTmp = XMLUtilities.selectSingleNodeText(tempDoc,"./OtherTransaction/Amount");
            logger.info("Amount: " + strTmp);
            if (strTmp != null && !strTmp.equals("")) {
            	sdVO.setPrincipalAmount(new BigDecimal(strTmp));
            }

            amazonDAO.insertSettlementDetail(sdVO);

	    }
*/

	} catch (Exception e) {			
	    e.printStackTrace();
	} finally {
        if (conn != null) {
            try {
                conn.close();
            } catch (Exception e) {
                logger.error("Unable to close connection: " + e);
            }
        }
    }

  }
  
}
