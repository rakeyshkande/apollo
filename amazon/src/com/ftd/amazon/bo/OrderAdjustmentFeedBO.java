/**
 * 
 */
package com.ftd.amazon.bo;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.amazonaws.mws.model.FeedSubmissionInfo;
import com.amazonaws.mws.model.FeedType;
import com.amazonaws.mws.model.SubmitFeedResponse;
import com.amazonaws.mws.model.SubmitFeedResult;
import com.ftd.amazon.common.AmazonConstants;
import com.ftd.amazon.common.AmazonUtils;
import com.ftd.amazon.dao.OrderAdjFeedDAO;
import com.ftd.amazon.exceptions.AmazonApplicationException;
import com.ftd.amazon.vo.OrderAdjFeedVO;
import com.ftd.amazon.vo.amazon.AmazonEnvelope;
import com.ftd.amazon.vo.amazon.BaseCurrencyCode;
import com.ftd.amazon.vo.amazon.BuyerPrice;
import com.ftd.amazon.vo.amazon.CurrencyAmount;
import com.ftd.amazon.vo.amazon.Header;
import com.ftd.amazon.vo.amazon.Message;
import com.ftd.amazon.vo.amazon.OrderAdjustment;
import com.ftd.amazon.vo.amazon.OrderAdjustment.AdjustedItem;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * @author skatam
 *
 */
public class OrderAdjustmentFeedBO 
{
	private static Logger logger = new Logger("com.ftd.amazon.bo.OrderAdjustmentFeedBO");
	
	public void processOrderAdjustmentFeed() 
	{
    	logger.debug("*********** processOrderAdjustmentFeed() **********");
    	Connection conn = null;
    	try
    	{
    		conn = AmazonUtils.getNewConnection();
    		OrderAdjFeedDAO feedDAO = new OrderAdjFeedDAO(conn);
	    	List<OrderAdjFeedVO> orderAdjFeedData = feedDAO.getOrderAdjustmentFeedData(AmazonConstants.FEED_STATUS_NEW);
	    	if(orderAdjFeedData==null || orderAdjFeedData.isEmpty()){
	    		logger.info("No Order Adjustment Feed Updates to Send to Amazon");
	    		return;
	    	}
	    	AmazonEnvelope amazonEnvelope = this.buildOrderAdjustmentFeedEnvelope(orderAdjFeedData);
	    	
	    	String feedContent = amazonEnvelope.marshallAsXML();
	    	logger.debug("FeedContent XML :"+feedContent);
	    	String fileName = AmazonFeedBO.saveFeedXML(feedContent, AmazonConstants.ORDER_ADJ_FEED_NAME_PREFIX);
	    	
	    	SubmitFeedResponse response = AmazonFeedBO.submitFeed(FeedType.POST_PAYMENT_ADJUSTMENT_DATA ,fileName);
			
	        // add response handling code
	        SubmitFeedResult submitFeedResult = response.getSubmitFeedResult();
	        FeedSubmissionInfo feedSubmissionInfo = submitFeedResult.getFeedSubmissionInfo();
	    	for (OrderAdjFeedVO vo : orderAdjFeedData) 
	        {
				vo.setFeedStatus(AmazonConstants.FEED_STATUS_SENT);
				vo.setServer(AmazonUtils.getLocalHostName());
				vo.setFilename(fileName);
				vo.setTransactionId(feedSubmissionInfo.getFeedSubmissionId());
				feedDAO.saveFeedStatus(vo);
	        }
    	}
    	catch (Exception e) {
			logger.error(e);
			throw new AmazonApplicationException(e.getMessage());
		}
        finally
        {
        	if(conn!= null){ try { conn.close(); } catch (SQLException e) {} }
        }
    	
}

	private AmazonEnvelope buildOrderAdjustmentFeedEnvelope(List<OrderAdjFeedVO> orderAdjFeedData) 
	{
		
    	AmazonEnvelope amazonEnvelope = new AmazonEnvelope();
    	Header header = new Header();
    	header.setDocumentVersion("1.01");
    	header.setMerchantIdentifier(AmazonUtils.getMerchantId());
		amazonEnvelope.setHeader(header );
		amazonEnvelope.setMessageType("OrderAdjustment");
		
    	
		Map<String, List<OrderAdjFeedVO>> orderAdjMap = new HashMap<String, List<OrderAdjFeedVO>>();
		
		for (OrderAdjFeedVO feedDetail : orderAdjFeedData) 
    	{
    		if(!orderAdjMap.containsKey(feedDetail.getAzOrderID())){
    			orderAdjMap.put(feedDetail.getAzOrderID(), new ArrayList<OrderAdjFeedVO>());
    		}
    		orderAdjMap.get(feedDetail.getAzOrderID()).add(feedDetail);
		}
		int counter = 1;
		Set<String> keySet = orderAdjMap.keySet();
		for (String key : keySet) 
		{
			List<OrderAdjFeedVO> feedDetailList = orderAdjMap.get(key);
			OrderAdjFeedVO adjFeedVO = feedDetailList.get(0);
			Message message = new Message();
    		message.setMessageID(new BigInteger(""+counter));
    		
			OrderAdjustment orderAdjustment = new OrderAdjustment();
			//orderAdjustment.setMerchantOrderID(adjFeedVO.getMasterOrderNumber());
			orderAdjustment.setAmazonOrderID(adjFeedVO.getAzOrderID());
			
			for (OrderAdjFeedVO orderAdjFeedVO : feedDetailList) 
			{
				AdjustedItem adjustedItem = new AdjustedItem();
				adjustedItem.setMerchantOrderItemID(orderAdjFeedVO.getConfirmationNumber());
				//adjustedItem.setMerchantAdjustmentItemID(orderAdjFeedVO.getAzOrderAdjId());
				adjustedItem.setAdjustmentReason(orderAdjFeedVO.getAdjustmentReason());
				
				BuyerPrice itemPriceAdjustments = adjustedItem.getItemPriceAdjustments();
				if(!isNullOrZero(orderAdjFeedVO.getPrincipalAmt()))
				{
					BuyerPrice.Component principalAmt = new BuyerPrice.Component();
					CurrencyAmount principalAmtValue = new CurrencyAmount();
					principalAmtValue.setCurrency(BaseCurrencyCode.USD);
					principalAmtValue.setValue(new BigDecimal(orderAdjFeedVO.getPrincipalAmt()));
					principalAmt.setAmount(principalAmtValue );
					principalAmt.setType("Principal");				
					itemPriceAdjustments.getComponent().add(principalAmt);
				}
				if(!isNullOrZero(orderAdjFeedVO.getShippingAmt()))
				{
					BuyerPrice.Component shippingAmt = new BuyerPrice.Component();
					CurrencyAmount shippingAmtValue = new CurrencyAmount();
					shippingAmtValue.setCurrency(BaseCurrencyCode.USD);
					shippingAmtValue.setValue(new BigDecimal(orderAdjFeedVO.getShippingAmt()));
					shippingAmt.setAmount(shippingAmtValue );
					shippingAmt.setType("Shipping");				
					itemPriceAdjustments.getComponent().add(shippingAmt);
				}
				if(!isNullOrZero(orderAdjFeedVO.getTaxAmt() ))
				{
					BuyerPrice.Component taxAmt = new BuyerPrice.Component();
					CurrencyAmount taxAmtValue = new CurrencyAmount();
					taxAmtValue.setCurrency(BaseCurrencyCode.USD);
					taxAmtValue.setValue(new BigDecimal(orderAdjFeedVO.getTaxAmt()));
					taxAmt.setAmount(taxAmtValue );
					taxAmt.setType("Tax");				
					itemPriceAdjustments.getComponent().add(taxAmt);
				}
				if(!isNullOrZero(orderAdjFeedVO.getShippingTaxAmt()))
				{
					BuyerPrice.Component shippingTaxAmt = new BuyerPrice.Component();
					CurrencyAmount shippingTaxAmtValue = new CurrencyAmount();
					shippingTaxAmtValue.setCurrency(BaseCurrencyCode.USD);
					shippingTaxAmtValue.setValue(new BigDecimal(orderAdjFeedVO.getShippingTaxAmt()));
					shippingTaxAmt.setAmount(shippingTaxAmtValue );
					shippingTaxAmt.setType("ShippingTax");
					itemPriceAdjustments.getComponent().add(shippingTaxAmt);
				}
				orderAdjustment.getAdjustedItem().add(adjustedItem );
			}
			message.setOrderAdjustment(orderAdjustment);
    		amazonEnvelope.getMessages().add(message);
    		counter++;
		}
		
    	return amazonEnvelope;
    
    }
	
	private static boolean isNullOrZero(String value) {
		if(value==null || value.trim().length()==0){
			return true;
		}
		try {
			double val = Double.parseDouble(value.trim());
			return (val==0.0);
		} catch (NumberFormatException e) {
			logger.error(e.getMessage());
			return true;
		}
	}
	
}
