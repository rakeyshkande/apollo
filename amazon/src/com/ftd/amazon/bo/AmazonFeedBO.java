package com.ftd.amazon.bo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import com.amazonaws.mws.MarketplaceWebService;
import com.amazonaws.mws.MarketplaceWebServiceConfig;
import com.amazonaws.mws.MarketplaceWebServiceException;
import com.amazonaws.mws.model.FeedProcessingStatus;
import com.amazonaws.mws.model.FeedSubmissionInfo;
import com.amazonaws.mws.model.FeedType;
import com.amazonaws.mws.model.GetFeedSubmissionListRequest;
import com.amazonaws.mws.model.GetFeedSubmissionListResponse;
import com.amazonaws.mws.model.GetFeedSubmissionListResult;
import com.amazonaws.mws.model.GetFeedSubmissionResultRequest;
import com.amazonaws.mws.model.GetFeedSubmissionResultResponse;
import com.amazonaws.mws.model.IdList;
import com.amazonaws.mws.model.StatusList;
import com.amazonaws.mws.model.SubmitFeedRequest;
import com.amazonaws.mws.model.SubmitFeedResponse;
import com.amazonaws.mws.model.TypeList;
import com.ftd.amazon.common.AmazonConstants;
import com.ftd.amazon.common.AmazonMWSBuilder;
import com.ftd.amazon.common.AmazonUtils;
import com.ftd.amazon.dao.AmazonFeedDAO;
import com.ftd.amazon.exceptions.AmazonApplicationException;
import com.ftd.amazon.vo.BaseFeedDetailVO;
import com.ftd.amazon.vo.FeedStatusRequestVO;
import com.ftd.amazon.vo.amazon.AmazonEnvelope;
import com.ftd.amazon.vo.amazon.Message;
import com.ftd.amazon.vo.amazon.ProcessingReport;
import com.ftd.amazon.vo.amazon.ProcessingReport.Result;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;

/**
 * AmazonFeedBO contains logic common to all types of Feeds.
 * Any Feed specific logic should be in Feed Specific BO.
 * For ex: Inventory Feed specific logic should be in InventoryFeedBO.
 * 
 * @author skatam
 *
 */
public class AmazonFeedBO 
{ 
  
    /**
    * Logger instance
    */
    private static Logger logger = new Logger("com.ftd.amazon.bo.AmazonFeedBO");
    
    /**
    * Default private constructor
    */
    public AmazonFeedBO() throws Exception {
    }

    public void processOverrideFeed() {
    	logger.info("processOverrideFeed");
    	
    }
    
    
    public void updateFeedStatus() throws Exception
    {
    	logger.debug("Updating Feed Status..");
    	Connection conn=null;
		try
		{
			conn = AmazonUtils.getNewConnection();
			AmazonFeedDAO amazonFeedDAO = new AmazonFeedDAO(conn);
			List<BaseFeedDetailVO> feedData = amazonFeedDAO.getFeedDataSentToAmazon();
			if(feedData == null || feedData.isEmpty())
			{
				logger.info("No feed found with status 'SENT'");
				return;
			}
			
			FeedStatusRequestVO feedStatusRequestVO = new FeedStatusRequestVO();
			
			for (BaseFeedDetailVO baseFeedDetailVO : feedData) 
			{
				feedStatusRequestVO.addFeedSubmissionId(baseFeedDetailVO.getTransactionId());
			}
			
			List<FeedSubmissionInfo> feedSubmissionInfoList = getFeedStatus(feedStatusRequestVO);
			if(feedSubmissionInfoList != null)
			{
				for (FeedSubmissionInfo feedSubmissionInfo : feedSubmissionInfoList) 
				{
					String feedType = feedSubmissionInfo.getFeedType();
					String status = feedSubmissionInfo.getFeedProcessingStatus();
					logger.info("FeedSubmissionId:"+feedSubmissionInfo.getFeedSubmissionId());
					logger.info("FeedType:"+feedType);
					logger.info("FeedStatus:"+status);
					if(FeedProcessingStatus.DONE.value().equalsIgnoreCase(status))
					{
						this.processFeedProcessingReport(conn, feedSubmissionInfo);
						
						BaseFeedDetailVO feedDetailVO = new BaseFeedDetailVO();
						//feedDetailVO.setFeedStatus(feedSubmissionInfo.getFeedProcessingStatus());
						feedDetailVO.setFeedStatus(AmazonConstants.FEED_STATUS_CONFIRMED);					
						feedDetailVO.setServer(AmazonUtils.getLocalHostName());
						feedDetailVO.setTransactionId(feedSubmissionInfo.getFeedSubmissionId());
						AmazonFeedDAO feedDAO = new AmazonFeedDAO(conn);
						
						String internalFeedType = this.getInternalFeedType(FeedType.fromValue(feedType));
						feedDetailVO.setFeedType(internalFeedType);
						
						feedDAO.updateFeedSubmissionStatus(feedDetailVO);
						
						
					}
					else
					{
						logger.info("TransactionId:"+feedSubmissionInfo.getFeedSubmissionId()+", FeedType :"+
						feedType+", FeedStatus:"+status);
					}
				}
				
			}			
		}
		catch (AmazonApplicationException e) {
			throw e;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
		finally
		{
			if(conn!= null){ try { conn.close(); } catch (SQLException e) {} }
		}		
    }
    
	private void processFeedProcessingReport(Connection conn, FeedSubmissionInfo feedSubmissionInfo) 
	{
		GetFeedSubmissionResultRequest request = new GetFeedSubmissionResultRequest();
        request.setMerchant( AmazonUtils.getMerchantId() );
        request.setFeedSubmissionId(feedSubmissionInfo.getFeedSubmissionId());
        String amzFeedType = feedSubmissionInfo.getFeedType();
        String internalFeedType = this.getInternalFeedType(FeedType.fromValue(amzFeedType));
        
        try {
        	String feedDir = AmazonUtils.getAmazonLocalFeedDirectory();
        	String fileName =  feedDir+"feedSubmissionResult_"+System.currentTimeMillis()+".xml" ;
            OutputStream processingResult = new FileOutputStream(fileName);
            request.setFeedSubmissionResultOutputStream( processingResult );
            MarketplaceWebServiceConfig config = new MarketplaceWebServiceConfig();
			MarketplaceWebService service = AmazonMWSBuilder.getMarketplaceWebService(config );
            GetFeedSubmissionResultResponse response = service.getFeedSubmissionResult(request);
            //String processingReportXml = request.getFeedSubmissionResultOutputStream().toString();
            logger.debug("GetFeedSubmissionResultResponse XML :"+response.toXML());
            InputStream is = new FileInputStream(new File(fileName));
            JAXBContext context = JAXBContext.newInstance(AmazonEnvelope.class);
    		Unmarshaller um = context.createUnmarshaller();
    		AmazonEnvelope amazonEnvelope = (AmazonEnvelope) um.unmarshal(is);
    		List<Message> messages = amazonEnvelope.getMessages();
    		for (Message message : messages) 
    		{
    			ProcessingReport processingReport = message.getProcessingReport();
    			this.reportErrorsIfAny(conn, processingReport, internalFeedType, feedSubmissionInfo.getFeedSubmissionId());
    		}
    		
    		archiveFeedFile(fileName);
        } 
        catch (MarketplaceWebServiceException ex) {
        	AmazonUtils.log(ex);
        	throw new AmazonApplicationException(ex);
        }
        catch (Exception e) {
        	throw new AmazonApplicationException(e);
        }
	}
	
	public void processRefundFeed() throws Exception
	{
		Connection conn = null;	
		try
		{
			logger.debug("Entered Refund BO");
			conn = AmazonUtils.getNewConnection();
			AmazonFeedDAO amazonFeedDAO = new AmazonFeedDAO(conn);
			amazonFeedDAO.processRefunds();
			logger.debug("End Refund BO");
		}
		catch (Exception e) {
			logger.error("Error occured while calling processRefunds method from AmazonFeedDAO", e);
			throw e;
			
		}
		finally
		{
			if(conn!= null)
			{ 
				try 
				{ 
					conn.close(); 
				} 
				catch (SQLException e) 
				{
					logger.error("Error occured while closing the connection", e);
				} 
			}
		}
	}
	
	private void reportErrorsIfAny(Connection conn, ProcessingReport processingReport, 
									String internalFeedType, String transactionId) throws Exception
	{
		if(processingReport.getProcessingSummary().getMessagesWithError().intValue() > 0 
				|| processingReport.getProcessingSummary().getMessagesWithWarning().intValue() > 0)
		{
			
			String feedFileName = new AmazonFeedDAO(conn).getFeedFileNameByTxnId(internalFeedType, transactionId);
			List<Result> results = processingReport.getResult();
			for (Result result : results) 
			{
				SystemMessengerVO systemMessageVO = new SystemMessengerVO();
		     	systemMessageVO.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
		     	systemMessageVO.setSource("Amazon Feed["+internalFeedType+"]");
		     	systemMessageVO.setType(result.getResultCode().toUpperCase());
		     	systemMessageVO.setMessage(result.getResultDescription()+" FeedXML :"+feedFileName);
		     	systemMessageVO.setSubject("Amazon Feed[ "+internalFeedType+"] "+result.getResultCode());
		     	
		     	SystemMessenger sysMessenger = SystemMessenger.getInstance();
		     	try {
		     		logger.debug("Sending SystemMessage :"+result.getResultDescription());
					sysMessenger.send(systemMessageVO, conn, false);
				} catch (Exception e) {
					logger.error(e);
				}
			}
		}
		else
		{
			logger.debug("No Errors/Warnings in ProcessingReport XML.");
		}


	}
	public static List<FeedSubmissionInfo> getFeedStatus(FeedStatusRequestVO requestVO) 
	{
		GetFeedSubmissionListRequest request = new GetFeedSubmissionListRequest();
        String merchantId = AmazonUtils.getMerchantId();
		request.setMerchant( merchantId );
		
		IdList idList = requestVO.getIdList();
		StatusList statusList = requestVO.getStatusList();
		TypeList typeList = requestVO.getTypeList();
		GetFeedSubmissionListResponse response = getFeedSubmissionListResponse(idList, statusList, typeList);
		
		if (response.isSetGetFeedSubmissionListResult()) 
		{
            GetFeedSubmissionListResult  getFeedSubmissionListResult = response.getGetFeedSubmissionListResult();
            java.util.List<FeedSubmissionInfo> feedSubmissionInfoList = getFeedSubmissionListResult.getFeedSubmissionInfoList();
            return feedSubmissionInfoList;
        }
		return null;
	}
	
	public static  GetFeedSubmissionListResponse getFeedSubmissionListResponse
					(IdList idList, StatusList statusList, TypeList typeList) 
	{
		GetFeedSubmissionListRequest request = new GetFeedSubmissionListRequest();
        String merchantId = AmazonUtils.getMerchantId();
		request.setMerchant(merchantId);
		if(statusList != null && statusList.isSetStatus()){
			request.setFeedProcessingStatusList(statusList);
		}
		if(typeList != null && typeList.isSetType()){
			request.setFeedTypeList(typeList);
		}
		if(idList != null && idList.isSetId()){
			request.setFeedSubmissionIdList(idList);
		}
		MarketplaceWebServiceConfig config = new MarketplaceWebServiceConfig();
		
		MarketplaceWebService service = AmazonMWSBuilder.getMarketplaceWebService(config);
		
		GetFeedSubmissionListResponse response = null;
		try 
		{
            response = service.getFeedSubmissionList(request);
		}
		catch (MarketplaceWebServiceException ex) 
		{
			AmazonUtils.log(ex);
	    	throw new AmazonApplicationException(ex.getMessage());
        }
		return response;
		
	}
	
	public static  String saveFeedXML(String feedContent, String fileNamePrefix)
    {
		String fileName = AmazonUtils.deriveFeedName(fileNamePrefix); 
    	try 
    	{
    		File file = new File(fileName);
    		if(!file.exists()){
					file.createNewFile();
    		}
			OutputStream os = new FileOutputStream(new File(fileName));
			os.write(feedContent.getBytes());
			os.close();
		}
    	catch (IOException e) {
			throw new AmazonApplicationException(e.getMessage());
		}
    	logger.debug("Feed File saved to :"+fileName);
		return fileName;
    }
    
	public static void archiveFeedFile(String fileName)
	{
		logger.debug("Archiving File :"+fileName);
		File feedFile = new File(fileName);
		if(!feedFile.exists())
		{
			logger.info("Feed file"+fileName+" doesn't exist.");
			return;
		}
		String archiveDirName = AmazonUtils.getAmazonLocalFeedArchiveDirectory();
		File archiveDir = new File(archiveDirName);
		if(!archiveDir.exists()) {
			archiveDir.mkdirs();
			logger.info("Created Feed Archive Directory.");
		}		
		
		File movedFile = new File(archiveDir, feedFile.getName());
		logger.debug("Archiving File to :"+movedFile);
		
		boolean moved = feedFile.renameTo(movedFile);
		if(moved) {
			logger.debug("File moved to :"+movedFile.getAbsolutePath());
		}
		else{
			logger.error("Unable to archive Feed XML file.");
		}
	}
	
	public static  SubmitFeedRequest buildFeedRequest(FeedType feedType, String fileName)
    {
    	SubmitFeedRequest request = buildGenericFeedRequest();
    	request.setFeedType(feedType.value());
    	
    	File file = new File(fileName);
    	FileInputStream fis = null;
		try {
			fis = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new AmazonApplicationException(e.getMessage());
		}
		request.setFeedContent(fis);    	
		String contentMD5 = AmazonUtils.computeContentMD5HeaderValue(fis);
		request.setContentMD5(contentMD5);
		
		return request;
    }
    
    public static  SubmitFeedRequest buildGenericFeedRequest()
    {
    	SubmitFeedRequest request = new SubmitFeedRequest();
    	request.setMerchant(AmazonUtils.getMerchantId());
        request.setMarketplaceIdList(new IdList(AmazonUtils.getMarketPlaceIds()));
        return request;
    }
    
    public static SubmitFeedResponse submitFeed(FeedType feedType, String fileName) 
    {
    	SubmitFeedResponse response = null;

    	try 
    	{
    		MarketplaceWebServiceConfig config = new MarketplaceWebServiceConfig();
    		
    		MarketplaceWebService mws = AmazonMWSBuilder.getMarketplaceWebService(config);
    		SubmitFeedRequest request = buildFeedRequest(feedType ,fileName);
            response = mws.submitFeed(request);
            
            AmazonFeedBO.archiveFeedFile(fileName);
			
        } 
    	catch (MarketplaceWebServiceException ex) 
        {
    		AmazonUtils.log(ex);
    		throw new AmazonApplicationException(ex.getMessage());
    	} 
    	/*catch (Exception e) {
    		logger.error(e);
    		throw new AmazonApplicationException(e.getMessage());
    	}*/
    	return response;
    }
    
    private String getInternalFeedType(FeedType feedType)
    {
    	if(feedType==FeedType.POST_INVENTORY_AVAILABILITY_DATA)
		{
			return ("INVENTORY_FEED");
		}
		else if(feedType==FeedType.POST_PRODUCT_IMAGE_DATA)
		{
			return("IMAGE_FEED");
		} 
		else if(feedType==FeedType.POST_PRODUCT_DATA)
		{
			return("PRODUCT_FEED");
		}else if(feedType==FeedType.POST_PRODUCT_PRICING_DATA)
		{
			return("PRICE_FEED");
		}else if(feedType==FeedType.POST_ORDER_ACKNOWLEDGEMENT_DATA)
		{
			return("ORDER_ACK_FEED");
		}else if(feedType==FeedType.POST_PAYMENT_ADJUSTMENT_DATA)
		{
			return("ORDER_ADJ_FEED");
		}else if(feedType==FeedType.POST_ORDER_FULFILLMENT_DATA)
		{
			return("ORDER_FULFILL_FEED");
		}		
		else {
			return null;
		}
    }
    
	public void checkPDBExceptionDates() throws Exception {
		Connection conn = null;	
		try {
			logger.debug("checkPDBExceptionDates()");
			conn = AmazonUtils.getNewConnection();
			AmazonFeedDAO amazonFeedDAO = new AmazonFeedDAO(conn);
			amazonFeedDAO.checkPDBExceptionDates();
		} catch (Exception e) {
			logger.error("Error occured while calling checkPDBExceptionDates", e);
			throw e;
		} finally {
			if(conn!= null) { 
				try { 
					conn.close(); 
				} catch (SQLException e) {
					logger.error("Error occured while closing the connection", e);
				} 
			}
		}
	}

}
