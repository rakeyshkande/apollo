package com.ftd.amazon.bo;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.amazonaws.mws.model.FeedSubmissionInfo;
import com.amazonaws.mws.model.FeedType;
import com.amazonaws.mws.model.SubmitFeedResponse;
import com.amazonaws.mws.model.SubmitFeedResult;
import com.ftd.amazon.common.AmazonConstants;
import com.ftd.amazon.common.AmazonUtils;
import com.ftd.amazon.dao.OrderFulfillmentFeedDAO;
import com.ftd.amazon.exceptions.AmazonApplicationException;
import com.ftd.amazon.vo.OrderFulfillmentFeedDetailVO;
import com.ftd.amazon.vo.OrderFulfillmentFeedVO;
import com.ftd.amazon.vo.amazon.AmazonEnvelope;
import com.ftd.amazon.vo.amazon.CarrierCode;
import com.ftd.amazon.vo.amazon.Header;
import com.ftd.amazon.vo.amazon.Message;
import com.ftd.amazon.vo.amazon.OrderFulfillment;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * @author sbring
 *
 */
public class OrderFulfillmentFeedBO {

	private Logger logger = new Logger("com.ftd.amazon.bo.OrderFulfillmentFeedBO");

	/**
	 * Default public constructor
	 */
	public OrderFulfillmentFeedBO(){

	}

	public void processOrderFulfillmentFeed() throws Exception 
	{
		logger.info("processOrderFulfillmentFeed");

		Connection conn = null;
		try
		{
			conn = AmazonUtils.getNewConnection();
			OrderFulfillmentFeedDAO feedDAO = new OrderFulfillmentFeedDAO(conn);

			OrderFulfillmentFeedVO fulfillFeedData = feedDAO.getOrderFulfillmentFeedData(AmazonConstants.FEED_STATUS_NEW);
			if(fulfillFeedData.getFeedDetails()==null || fulfillFeedData.getFeedDetails().isEmpty()){
				logger.info("No Order Fulfillment Feed Updates to Send to Amazon");
				return;
			}

			AmazonEnvelope ae = buildOrderFulfillFeedEnvelope(fulfillFeedData);
			if(ae.getMessages().isEmpty()){
				logger.info("No Order Fulfillment Feed Updates to Send to Amazon");
				return;
			}

			String feedContent = ae.marshallAsXML();
			logger.debug("FeedContent XML :"+feedContent);
			String fileName = AmazonFeedBO.saveFeedXML(feedContent, AmazonConstants.ORDER_FUL_FEED_NAME_PREFIX);
			SubmitFeedResponse response = AmazonFeedBO.submitFeed(FeedType.POST_ORDER_FULFILLMENT_DATA,fileName);
			// add response handling code
			SubmitFeedResult submitFeedResult = response.getSubmitFeedResult();
			FeedSubmissionInfo feedSubmissionInfo = submitFeedResult.getFeedSubmissionInfo();
			List<OrderFulfillmentFeedDetailVO> feedDetails = fulfillFeedData.getFeedDetails();
			for (OrderFulfillmentFeedDetailVO orderFulfillment : feedDetails) 
			{	
				orderFulfillment.setFeedStatus(AmazonConstants.FEED_STATUS_SENT);
				orderFulfillment.setServer(AmazonUtils.getLocalHostName());
				orderFulfillment.setFilename(fileName);
				orderFulfillment.setTransactionId(feedSubmissionInfo.getFeedSubmissionId());
				feedDAO.saveFeedStatus(orderFulfillment); 
			}
		}
		catch (Exception e) 
		{
			logger.error(e);
			throw new AmazonApplicationException(e.getMessage());
		}
		finally
		{
			if(conn!= null){ try { conn.close(); } catch (SQLException e) {} }
		}
	}

	private AmazonEnvelope buildOrderFulfillFeedEnvelope(OrderFulfillmentFeedVO orderFulfillFeedVO) throws CacheException, Exception
	{
		AmazonEnvelope amazonEnvelope = new AmazonEnvelope();
		Header header = new Header();
		header.setDocumentVersion("1.01");
		header.setMerchantIdentifier(AmazonUtils.getMerchantId());
		amazonEnvelope.setHeader(header );
		amazonEnvelope.setMessageType("OrderFulfillment");
		int counter = 1;
		List<OrderFulfillmentFeedDetailVO> feedDetails = orderFulfillFeedVO.getFeedDetails();
		for (OrderFulfillmentFeedDetailVO feedDetail : feedDetails) 
		{
			Message msg = new Message();
			msg.setMessageID(new BigInteger(""+counter));

			OrderFulfillment orderFulfillment = new OrderFulfillment();
			//orderFulfillment.setMerchantFulfillmentID(new BigInteger(feedDetail.getOrderFulfillmentId()));
			orderFulfillment.setAmazonOrderID(feedDetail.getAmazonOrderId());
			orderFulfillment.setFulfillmentDate(AmazonUtils.toXMLGregorianCalendar(feedDetail.getFulfillmentDate()));
			OrderFulfillment.FulfillmentData fulfillmentData = new OrderFulfillment.FulfillmentData();
			try{
				fulfillmentData.setCarrierCode(CarrierCode.fromValue(feedDetail.getCarrierName()));
			}
			catch (Exception e){
				if (feedDetail.getCarrierName().equalsIgnoreCase("Florist"))
				{
					fulfillmentData.setCarrierName("FTD Florist");
				}
				else
				{
					logger.error("Invalid Carrier Name: " + feedDetail.getCarrierName());
					throw new AmazonApplicationException(e.getMessage());
				}
			}
			if (!feedDetail.getCarrierName().equalsIgnoreCase("Florist"))
			{
				fulfillmentData.setShipperTrackingNumber(feedDetail.getTrackingNumber());
			}
			fulfillmentData.setShippingMethod(feedDetail.getShippingMethod());
			orderFulfillment.setFulfillmentData(fulfillmentData);

			OrderFulfillment.Item item = new OrderFulfillment.Item();
			item.setAmazonOrderItemCode(feedDetail.getOrderItemNumber());
			orderFulfillment.getItem().add(item);

			msg.setOrderFulfillment(orderFulfillment);
			amazonEnvelope.getMessages().add(msg);
			counter++;
		}
		return amazonEnvelope;
	}

}
