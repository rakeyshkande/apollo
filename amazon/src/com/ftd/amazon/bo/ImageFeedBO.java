package com.ftd.amazon.bo;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.amazonaws.mws.model.FeedSubmissionInfo;
import com.amazonaws.mws.model.FeedType;
import com.amazonaws.mws.model.SubmitFeedResponse;
import com.amazonaws.mws.model.SubmitFeedResult;
import com.ftd.amazon.common.AmazonConstants;
import com.ftd.amazon.common.AmazonUtils;
import com.ftd.amazon.dao.ImageFeedDAO;
import com.ftd.amazon.exceptions.AmazonApplicationException;
import com.ftd.amazon.vo.ImageFeedDetailVO;
import com.ftd.amazon.vo.ImageFeedVO;
import com.ftd.amazon.vo.ProductMasterVO;
import com.ftd.amazon.vo.amazon.AmazonEnvelope;
import com.ftd.amazon.vo.amazon.Header;
import com.ftd.amazon.vo.amazon.Message;
import com.ftd.amazon.vo.amazon.ProductImage;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.plugins.Logger;

public class ImageFeedBO 
{ 

	/**
	 * Logger instance
	 */
	private Logger logger = new Logger("com.ftd.amazon.bo.AmazonFeedBO");

	/**
	 * Default public constructor
	 */
	public ImageFeedBO(){
		
	}

	public void processImageURLFeed() throws Exception 
	{
		logger.info("processImageURLFeed");

		Connection conn = null;
		try
		{
			conn = AmazonUtils.getNewConnection();
			ImageFeedDAO feedDAO = new ImageFeedDAO(conn );

			ImageFeedVO imageFeedData = feedDAO.getImageFeedData();
			if(imageFeedData.getFeedDetails()==null || imageFeedData.getFeedDetails().isEmpty()){
				logger.info("No Image Feed Updates to Send to Amazon");
				return;
			}

			AmazonEnvelope ae = buildImageFeedEnvelope(imageFeedData);
			if(ae.getMessages().isEmpty()){
				logger.info("No Image Feed Updates to Send to Amazon");
				return;
			}

			String feedContent = ae.marshallAsXML();
			logger.debug("FeedContent XML :"+feedContent);
			String fileName = AmazonFeedBO.saveFeedXML(feedContent, AmazonConstants.IMAGE_FEED_NAME_PREFIX);

			SubmitFeedResponse response = AmazonFeedBO.submitFeed(FeedType.POST_PRODUCT_IMAGE_DATA ,fileName);
			
			// add response handling code
			SubmitFeedResult submitFeedResult = response.getSubmitFeedResult();
			FeedSubmissionInfo feedSubmissionInfo = submitFeedResult.getFeedSubmissionInfo();
			List<ImageFeedDetailVO> feedDetails = imageFeedData.getFeedDetails();
			for (ImageFeedDetailVO productImage : feedDetails) 
			{	
				productImage.setFeedStatus(AmazonConstants.FEED_STATUS_SENT);
				productImage.setServer(AmazonUtils.getLocalHostName());
				productImage.setFilename(fileName);
				productImage.setTransactionId(feedSubmissionInfo.getFeedSubmissionId());
				feedDAO.saveFeedStatus(productImage); 
			}
		}
		catch (Exception e) 
		{
			logger.error(e);
			throw new AmazonApplicationException(e.getMessage());
		}
		finally
		{
			if(conn!= null){ try { conn.close(); } catch (SQLException e) {} }
		}
	}

	private AmazonEnvelope buildImageFeedEnvelope(ImageFeedVO imageFeedVO) throws CacheException, Exception
	{
		AmazonEnvelope amazonEnvelope = new AmazonEnvelope();
		Header header = new Header();
		header.setDocumentVersion("1.01");
		header.setMerchantIdentifier(AmazonUtils.getMerchantId());
		amazonEnvelope.setHeader(header );
		amazonEnvelope.setMessageType("ProductImage");
		int counter = 1;
		List<ImageFeedDetailVO> feedDetails = imageFeedVO.getFeedDetails();
		for (ImageFeedDetailVO feedDetail : feedDetails) 
		{
			List<ProductImage> productImageList = this.buildImageList(feedDetail);
			for (ProductImage productImage : productImageList) 
			{
				Message msg = new Message();
				msg.setMessageID(new BigInteger(""+counter));
				msg.setOperationType("Update");	
				msg.setProductImage(productImage);
				amazonEnvelope.getMessages().add(msg);
				counter++;
			}
		}
		return amazonEnvelope;
	}

	protected List<ProductImage> buildImageList(ImageFeedDetailVO imageFeedDetailVO) 
	{
		List<ProductImage> imageList = new ArrayList<ProductImage>();

		ProductMasterVO productFeedStatus = imageFeedDetailVO.getProductFeedStatus();
		if(productFeedStatus.isCatalogStatusStandardActive() 
				&& productFeedStatus.isSentToAmazonStandardFlag())
		{
			ProductImage productImage = new ProductImage();
			productImage.setImageType(AmazonUtils.getFrpGlobalParm(AmazonConstants.AMZ_GLOBAL_CONTEXT, AmazonConstants.AMZ_IMAGE_TYPE));
			productImage.setSKU(imageFeedDetailVO.getSku()+AmazonConstants.CATALOG_SUFFIX_STANDARD);
			productImage.setImageLocation(buildImageLocation(imageFeedDetailVO.getNovatorId())+ AmazonUtils.getFrpGlobalParm(AmazonConstants.AMZ_GLOBAL_CONTEXT, AmazonConstants.AMZ_IMAGE_FORMAT));
			imageList.add(productImage);
		}

		if(productFeedStatus.isCatalogStatusDeluxeActive() 
				&& productFeedStatus.isSentToAmazonDeluxeFlag())
		{
			ProductImage productImage = new ProductImage();
			productImage.setImageType(AmazonUtils.getFrpGlobalParm(AmazonConstants.AMZ_GLOBAL_CONTEXT, AmazonConstants.AMZ_IMAGE_TYPE));
			productImage.setSKU(imageFeedDetailVO.getSku()+AmazonConstants.CATALOG_SUFFIX_DELUXE);
			productImage.setImageLocation(buildImageLocation(imageFeedDetailVO.getNovatorId()) + AmazonConstants.CATALOG_SUFFIX_DELUXE + AmazonUtils.getFrpGlobalParm(AmazonConstants.AMZ_GLOBAL_CONTEXT, AmazonConstants.AMZ_IMAGE_FORMAT));
			imageList.add(productImage);
		}

		if(productFeedStatus.isCatalogStatusPremiumActive() 
				&& productFeedStatus.isSentToAmazonPremiumFlag())
		{
			ProductImage productImage = new ProductImage();
			productImage.setImageType(AmazonUtils.getFrpGlobalParm(AmazonConstants.AMZ_GLOBAL_CONTEXT, AmazonConstants.AMZ_IMAGE_TYPE));
			productImage.setSKU(imageFeedDetailVO.getSku()+AmazonConstants.CATALOG_SUFFIX_PREMIUM);
			productImage.setImageLocation(buildImageLocation(imageFeedDetailVO.getNovatorId()) + AmazonConstants.CATALOG_SUFFIX_PREMIUM + AmazonUtils.getFrpGlobalParm(AmazonConstants.AMZ_GLOBAL_CONTEXT, AmazonConstants.AMZ_IMAGE_FORMAT));
			imageList.add(productImage);
		}
		return imageList;
	}

	private String buildImageLocation(String novatorId) {
		String imageSize = AmazonUtils.getFrpGlobalParm(AmazonConstants.AMZ_GLOBAL_CONTEXT, AmazonConstants.AMZ_IMAGE_SIZE);
		String imageUrl = AmazonUtils.getFrpGlobalParm(AmazonConstants.AMZ_GLOBAL_CONTEXT, AmazonConstants.AMZ_IMAGE_URL);
		return (imageUrl + novatorId + "_" + imageSize);
	}


}