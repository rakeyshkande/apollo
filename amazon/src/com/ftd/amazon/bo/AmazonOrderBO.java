package com.ftd.amazon.bo;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.ftd.amazon.common.AmazonConstants;
import com.ftd.amazon.common.AmazonUtils;
import com.ftd.amazon.dao.AmazonDAO;
import com.ftd.amazon.utilities.Utils;
import com.ftd.amazon.utilities.XMLUtilities;
import com.ftd.amazon.vo.AzOrderDetailVO;
import com.ftd.osp.utilities.cacheMgr.CacheUtil;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.StateMasterVO;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.pas.common.vo.ProductAvailVO;
import com.ftd.pas.server.ProductAvailabilityBO;

public class AmazonOrderBO 
{ 
  
    /**
    * Logger instance
    */
    private Logger logger = new Logger("com.ftd.amazon.bo.AmazonOrderBO");
    private NumberFormat fmt = NumberFormat.getNumberInstance();
    /**
    * Default constructor
    */
    public AmazonOrderBO() throws Exception {
    }

    public void processInboundOrder(String amazonOrderNumber) {
    	logger.info("processInboundOrder");
    	logger.info(amazonOrderNumber);
    	
    	// read record from AMAZON.AZ_ORDER
    	// create FTD_XML and update AMAZON.AZ_ORDER
    	// create AMAZON.AZ_DETAIL ??
    	// send to Order Gatherer
    	
    	Connection conn = null;
    	try{
			conn = DataSourceUtil.getInstance().getConnection("ORDER SCRUB");
			//conn = testConnection();
			
	    	Document amazonOrderDoc = getAmazonOrderXML(amazonOrderNumber, conn);
	    	Document ftdOrderDoc = transformToFTDXml(amazonOrderDoc, conn);
	    	saveFTDOrderXml(ftdOrderDoc, amazonOrderNumber, AmazonConstants.ORDER_STATUS_TRANSFORMED,conn);    	
	    	saveAmazonOrderDetailsInfo(ftdOrderDoc, amazonOrderNumber, conn);
	    	sendToOrderGatherer(ftdOrderDoc, amazonOrderNumber, conn);
    	
    	} catch (Exception e) {			
			e.printStackTrace();
			logger.error("processInboundOrder Exception: " + e);
		}
        finally
        {
          if (conn != null)
          {
            try
            {
              conn.close();
            }
            catch (Exception e)
            {
              logger.error("Unable to close connection: " + e);
            }
          }
        }
    }
    
    /**
     * Get Amazon order xml (az_xml) from PTN_AMAZON.AZ_ORDER table
     * for the given amazon order number
     * @param amazonOrderNumber
     * @param conn
     * @return Document
     * @throws Exception
     */
    private Document getAmazonOrderXML(String amazonOrderNumber, Connection conn) throws Exception {
		AmazonDAO amazonDAO = new AmazonDAO(conn);
        
		String amazonOrderXML = amazonDAO.getAmazonOrderXML(amazonOrderNumber);
	    if(amazonOrderXML == null || amazonOrderXML.equals(""))
	    {
	    	logger.error("Amazon order XML is null");
	    }
	        
	    return JAXPUtil.parseDocument(amazonOrderXML);
	}

	/**
	 * Transforms Amazon order xml (AZ_XML) to FTD order xml (FTD_XML)
	 * @param amazonOrderDoc
	 * @param conn
	 * @return Document 
	 */
    private Document transformToFTDXml(Document amazonOrderDoc, Connection conn) {			
		Document ftdOrderDoc = null;
		AmazonDAO amazonDAO = new AmazonDAO(conn);
        
		try {  
			ftdOrderDoc = DOMUtil.getDefaultDocument();
			String strTmp = XMLUtilities.selectSingleNodeText(amazonOrderDoc,"/OrderReport/OrderDate");
			java.sql.Date orderDate = Utils.azDateString2Date(strTmp);
			
			logger.debug("Getting source code");
			ConfigurationUtil cu = ConfigurationUtil.getInstance();
	        String sourceCode = cu.getFrpGlobalParm(AmazonConstants.AMZ_GLOBAL_CONTEXT,
        		AmazonConstants.AMZ_NAME_DEFAULT_SOURCE_CODE);
	        BigDecimal dDiscount = new BigDecimal("0");
	        String discountType = "";
			
			String azOrderId = XMLUtilities.selectSingleNodeText(amazonOrderDoc,"/OrderReport/AmazonOrderID");
	        
	        //Generate the master order number
	        String masterOrderNumber = "A" + StringUtils.replaceChars(azOrderId,"-","");
	        
	        //Start building the FTD Order
	        Element ftdRoot = ftdOrderDoc.createElement("order");
	        
	        //Master order number
	        Element element1 = ftdOrderDoc.createElement("header");
	        element1.appendChild(XMLUtilities.createElement(ftdOrderDoc,"amazon-order-number",azOrderId));
	        element1.appendChild(XMLUtilities.createElement(ftdOrderDoc,"master-order-number",masterOrderNumber));
	        
	        //Source Code
	        element1.appendChild(XMLUtilities.createElement(ftdOrderDoc,"source-code",sourceCode));
	        
	        //Origin
	        element1.appendChild(XMLUtilities.createElement(ftdOrderDoc,"origin",ConfigurationUtil.getInstance().getProperty("amazon_config.xml", "origin")));
	        
	        //Order count (will fill in the value later)
	        Element orderCountElement = ftdOrderDoc.createElement("order-count");
	        element1.appendChild(orderCountElement);
	        int orderCounter=0;
	        
	        //Order Amount (will fill in the value later)
	        Element orderAmountElement = ftdOrderDoc.createElement("order-amount");
	        element1.appendChild(orderAmountElement);
	        
	        //Discount Total (will fill in the value later)
	        Element discountTotal = ftdOrderDoc.createElement("discount-total");
	        element1.appendChild(discountTotal);
	        
	        //Transaction Date (from order date set above)
	        SimpleDateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
	        Element element2 = XMLUtilities.createElement(ftdOrderDoc,"transaction-date",format.format(orderDate));
	        element1.appendChild(element2);
	        
	        //Socket Timestamp
	        element1.appendChild(XMLUtilities.createElement(ftdOrderDoc,"socket-timestamp",format.format(new java.util.Date())));
	        
	        String buyerName = XMLUtilities.selectSingleNodeText(amazonOrderDoc,"/OrderReport/BillingData/BuyerName");
	        if (buyerName == null) buyerName = "";
	        //Buyer First Name
	        String buyerFirstName = StringUtils.substringBefore(buyerName," ");
	        if (buyerFirstName == null) buyerFirstName = "";
	        if (buyerFirstName == "" || buyerFirstName.isEmpty()){ buyerFirstName = "NA"; }
	        if (buyerFirstName.length() > 20) {
                AmazonUtils.sendNoPageSystemMessage("Amazon Order Number: " + azOrderId + 
                		"\nBuyer First Name too long: " + buyerFirstName);
                buyerFirstName = buyerFirstName.substring(0, 20);
	        }
	        element1.appendChild(XMLUtilities.createElement(ftdOrderDoc,"buyer-first-name", buyerFirstName)); 

	        //Buyer Last Name is updated with the system date 
	        Calendar cal = Calendar.getInstance();
	        DateFormat dateFormat = new SimpleDateFormat("MMddyyyy");
	        String currentDate = dateFormat.format(cal.getTime());	        
	        String buyerLastName = StringUtils.substringAfter(buyerName," ");
	        if (buyerLastName == null) buyerLastName = "";
	        if (buyerLastName == "" || buyerLastName.isEmpty()){ buyerLastName = "NA"; }
	        buyerLastName = buyerLastName.concat("-");
	        buyerLastName = buyerLastName.concat(currentDate);
	        logger.info(" *****buyerLastName: "+buyerLastName);
	        if (buyerLastName != null && buyerLastName.length() > 20) {
                AmazonUtils.sendNoPageSystemMessage("Amazon Order Number: " + azOrderId + 
                		"\nBuyer Last Name too long: " + buyerLastName);
                buyerLastName = buyerLastName.substring(0, 20);
	        }
	        element1.appendChild(XMLUtilities.createElement(ftdOrderDoc,"buyer-last-name", buyerLastName)); 
	        
	        //Buyer Daytime Phone
	        String buyerPhoneNumber = XMLUtilities.selectSingleNodeText(amazonOrderDoc,"/OrderReport/BillingData/BuyerPhoneNumber");
	        if (buyerPhoneNumber == null) buyerPhoneNumber = "";
	        buyerPhoneNumber = Utils.removeAllSpecialChars(buyerPhoneNumber);
	        if (buyerPhoneNumber.length() > 10) {
                AmazonUtils.sendNoPageSystemMessage("Amazon Order Number: " + azOrderId + 
                		"\nBuyer Phone Number too long: " + buyerPhoneNumber);
	        	buyerPhoneNumber = buyerPhoneNumber.substring(0, 10);
	        } else if (buyerPhoneNumber.length() < 10) {
	        	buyerPhoneNumber = "0000000000" + buyerPhoneNumber;
	        	buyerPhoneNumber = buyerPhoneNumber.substring(buyerPhoneNumber.length()-10);
	        	logger.debug("buyerPhoneNumber too short, changed to: " + buyerPhoneNumber);
	        }
	        element1.appendChild(XMLUtilities.createElement(ftdOrderDoc,"buyer-primary-phone", buyerPhoneNumber));
	        
	        //Buyer Address 1 is updated with the sequence number from database.
	        String buyerSequence = amazonDAO.getAmazonBuyerSequence();
	        StringBuffer buyerAddress1 = new StringBuffer(buyerSequence);
	        buyerAddress1.append("-");
	        buyerAddress1.append(cu.getContentWithFilter(conn, "AMAZON", "DEFAULT_BUYER_ADDRESS", null, null));	 
	        logger.info(" ***** buyerAddress1: "+buyerAddress1.toString());
	        element1.appendChild(XMLUtilities.createElement(ftdOrderDoc,"buyer-address1",buyerAddress1.toString()));
	        
	        //Buyer Address 2
	        /*StringBuffer sb = new StringBuffer();
	        sb.append(XMLUtilities.selectSingleNodeText(amazonOrderDoc,"/OrderReport/BillingData/Address/AddressFieldTwo"));
	        sb.append("\r");
	        sb.append(XMLUtilities.selectSingleNodeText(amazonOrderDoc,"/OrderReport/BillingData/Address/AddressFieldThree"));
	        element1.appendChild(XMLUtilities.createElement(ftdOrderDoc,"buyer-address2",StringUtils.trimToEmpty(sb.toString())));*/
	        
	        //Buyer City
	        element1.appendChild(XMLUtilities.createElement(ftdOrderDoc,"buyer-city",cu.getContentWithFilter(conn, "AMAZON", "DEFAULT_BUYER_CITY", null, null)));
	        
	        //Buyer State
	        element1.appendChild(XMLUtilities.createElement(ftdOrderDoc,"buyer-state",cu.getContentWithFilter(conn, "AMAZON", "DEFAULT_BUYER_STATE", null, null)));
	        
	        //Buyer Postal
	        element1.appendChild(XMLUtilities.createElement(ftdOrderDoc,"buyer-postal-code",cu.getContentWithFilter(conn, "AMAZON", "DEFAULT_BUYER_POSTAL_CODE", null, null)));
	        
	        //Buyer Country
	        element1.appendChild(XMLUtilities.createElement(ftdOrderDoc,"buyer-country",cu.getContentWithFilter(conn, "AMAZON", "DEFAULT_BUYER_COUNTRY", null, null)));
	        
	        //Buyer Daytime Phone
	        //element1.addContent(new Element("buyer-daytime-phone").setText(XMLUtilities.selectSingleNodeText(azOrder,"/OrderReport/BillingData/Address/PhoneNumber")));
	        
	        //Buyer Email
	        element1.appendChild(XMLUtilities.createElement(ftdOrderDoc,"buyer-email-address",XMLUtilities.selectSingleNodeText(amazonOrderDoc,"/OrderReport/BillingData/BuyerEmailAddress")));
	        
	        //News Letter Flag
	        element1.appendChild(XMLUtilities.createElement(ftdOrderDoc,"news-letter-flag",ConfigurationUtil.getInstance().getProperty("amazon_config.xml", "news_letter_flag")));
	        
	        //Payment Type
	        element1.appendChild(XMLUtilities.createElement(ftdOrderDoc,"cc-type",ConfigurationUtil.getInstance().getProperty("amazon_config.xml", "cc_type")));
	        
	        //Add header to the root
	        ftdRoot.appendChild(element1);
	        
	        //Now do the order items
	        BigDecimal totalComputedPrice = new BigDecimal("0");
	        BigDecimal totalDiscount = new BigDecimal("0");
	        BigDecimal totalShipping = new BigDecimal("0");
	        BigDecimal totalTax = new BigDecimal("0");
	        BigDecimal totalShippingTax = new BigDecimal("0");
	        Element itemElement;
	        
	        Element ftdItemsElement = ftdOrderDoc.createElement("items");
	        NodeList nl = XMLUtilities.selectXPathNodes(amazonOrderDoc,"/OrderReport/Item");

	        if(nl != null) {
	            for(int i=0; i<nl.getLength(); i++) 
	            {
	              itemElement = (Element)nl.item(i);
	              Element ftdItemElement = ftdOrderDoc.createElement("item");
	              
	              //Now save the AmazonOrderItemCode for use later on down the read
	              Element idElement = (Element) XMLUtilities.selectSingleNode(itemElement,"./AmazonOrderItemCode");
	              if( idElement==null || idElement.getFirstChild()==null || idElement.getFirstChild().getNodeValue()==null) 
	              {
	                throw new Exception("No Amazon order item code can be found for this item");  
	              }
	              ftdItemElement.appendChild(XMLUtilities.createElement(ftdOrderDoc,"amazon-order-item-code",StringUtils.trimToEmpty(idElement.getFirstChild().getNodeValue())));
	              
	              //IOTW source code
	              String sku = XMLUtilities.selectSingleNodeText(itemElement,"./SKU");
	              String skuDeluxePremium = sku;
	              String amazonSku = sku;
	              if(sku.indexOf(AmazonConstants.CATALOG_SUFFIX_DELUXE) >= 0 || sku.indexOf(AmazonConstants.CATALOG_SUFFIX_PREMIUM) >= 0){
	            	  sku = sku.substring(0, 4);
	              }
	              String dateReceived = new SimpleDateFormat("dd-MMM-yy").format(orderDate);
	              Map iotwMap = amazonDAO.getIOTWSourceCode(sourceCode, sku, dateReceived);
	              
	              sourceCode = (String) iotwMap.get("iotwSourceCode");
	              dDiscount = (BigDecimal) iotwMap.get("discount");
	              discountType = (String) iotwMap.get("discountType");
	              logger.debug("sourceCode: " + sourceCode + " discountType: " + discountType +
	            		  " discount: " + dDiscount);
	              //End IOTW source code
	              
	              //Source Code
	              ftdItemElement.appendChild(XMLUtilities.createElement(ftdOrderDoc,"item-source-code",sourceCode));
	              
	              //look up the price in FTD_APPS.PRODUCT_MASTER
	              logger.debug("Getting pdb data");
	              BigDecimal pdb_price = new BigDecimal("0");
	              Map pdbPriceMap = amazonDAO.getPdbPriceData(sku);
	              String shipMethodFlorist = (String) pdbPriceMap.get("shipMethodFlorist");
	              if (shipMethodFlorist == null) shipMethodFlorist = "N";
	              String shipMethodCarrier = (String) pdbPriceMap.get("shipMethodCarrier");
	              if (shipMethodCarrier == null) shipMethodCarrier = "N";

	              if(skuDeluxePremium.indexOf(AmazonConstants.CATALOG_SUFFIX_DELUXE) >= 0){
		              if(pdbPriceMap.get("deluxePrice")!=null){
		            	  		pdb_price = (BigDecimal) pdbPriceMap.get("deluxePrice");
		            	  }
		          }
		          else if(skuDeluxePremium.indexOf(AmazonConstants.CATALOG_SUFFIX_PREMIUM) >= 0){
		           	  if(pdbPriceMap.get("premiumPrice")!=null){
		           		  		pdb_price = (BigDecimal) pdbPriceMap.get("premiumPrice");
		            	  }
		          }
		          else{
		           	  if(pdbPriceMap.get("standardPrice")!=null){
		           		  		pdb_price = (BigDecimal) pdbPriceMap.get("standardPrice");
		            	  }
		          }
	              
	              if(pdb_price.compareTo(BigDecimal.ZERO) ==0){ //get historical pdb data
		              Map productPriceMap = amazonDAO.getProductByTimestamp(sku, format.parse(format.format(orderDate)));
		              if(skuDeluxePremium.indexOf(AmazonConstants.CATALOG_SUFFIX_DELUXE) >= 0){
		            	  if(productPriceMap.get("deluxePrice")!=null){
		            		  pdb_price = (BigDecimal) productPriceMap.get("deluxePrice");
		            	  }
		              }
		              else if(skuDeluxePremium.indexOf(AmazonConstants.CATALOG_SUFFIX_PREMIUM) >= 0){
		            	  if(productPriceMap.get("premiumPrice")!=null){
		            		  pdb_price = (BigDecimal) productPriceMap.get("premiumPrice");
		            	  }
		              }
		              else{
		            	  if(productPriceMap.get("standardPrice")!=null){
		            		  pdb_price = (BigDecimal) productPriceMap.get("standardPrice");
		            	  }
		              }
	              }
	              logger.debug("##pdb_price: "+pdb_price);
	              
	              BigDecimal itemComputedPrice = new BigDecimal("0");
	              BigDecimal itemDiscount = new BigDecimal("0");
	              BigDecimal itemPrincipal = new BigDecimal("0");
	              BigDecimal itemShipping = new BigDecimal("0");
	              BigDecimal itemTax = new BigDecimal("0");
	              BigDecimal itemShippingTax = new BigDecimal("0");
	              BigDecimal commission = new BigDecimal("0");
	              boolean invalidPricePoint = false;
	              //Get Delivery date
	              String shipMethod = "";
	              strTmp = XMLUtilities.selectSingleNodeText(itemElement,"./CustomizationInfo[Type='SD-StartTime']/Data");
	              if( (strTmp!= null) && (!strTmp.isEmpty()) && (strTmp!="") ){
	            	  long sdStartTime = Long.parseLong(strTmp);
	            	  Date deliveryDate = new Date(sdStartTime * 1000);
	            	  String zipCode = XMLUtilities.selectSingleNodeText(amazonOrderDoc,"/OrderReport/FulfillmentData/Address/PostalCode");
	            	  shipMethod = getShipMethod(conn, sku, deliveryDate, zipCode, shipMethodFlorist, shipMethodCarrier);
	            	  ftdItemElement.appendChild(XMLUtilities.createElement(ftdOrderDoc,"delivery-date",new SimpleDateFormat("MM/dd/yyyy").format(deliveryDate)));
	              }
	              else{
	      	        String zipCode = XMLUtilities.selectSingleNodeText(amazonOrderDoc,"/OrderReport/FulfillmentData/Address/PostalCode");
                    if(zipCode.length() > 5) {
                        zipCode = zipCode.substring(0,5);
                    }
                	Date nextAvailableDate = getNextAvailableDate(conn, sku, zipCode, 30, shipMethodFlorist, shipMethodCarrier);
                	if (nextAvailableDate != null) {
                	shipMethod = getShipMethod(conn, sku, nextAvailableDate, zipCode, shipMethodFlorist, shipMethodCarrier);
                	}

                    logger.info("Delivery date from pas service: "  + nextAvailableDate);
                    if (nextAvailableDate != null) {
	    	            ftdItemElement.appendChild(XMLUtilities.createElement(ftdOrderDoc,"delivery-date",new SimpleDateFormat("MM/dd/yyyy").format(nextAvailableDate)));
	            	} else {
	            	      ftdItemElement.appendChild(XMLUtilities.createElement(ftdOrderDoc,"delivery-date",strTmp));
	            	}
	              }
	              
	              //shipping-method
	              logger.info("shipMethod: " + shipMethod + " shipMethodFlorist: " + shipMethodFlorist +
	            		  " shipMethodCarrier: " + shipMethodCarrier);
	              if (shipMethod != null) {
		              ftdItemElement.appendChild(XMLUtilities.createElement(ftdOrderDoc,"shipping-method",shipMethod));	            	  
	              } else {
		              ftdItemElement.appendChild(XMLUtilities.createElement(ftdOrderDoc,"shipping-method",""));	            	  	            	  
	              }
	              //Get the dollar amounts of the four price elements
	              strTmp = XMLUtilities.selectSingleNodeText(itemElement,"./ItemPrice/Component[Type='Principal']/Amount");
	              if( strTmp.length()>0 ) {
	                try{
	            	  itemPrincipal = new BigDecimal(strTmp);
	                }catch(NumberFormatException e){
	                	itemPrincipal = new BigDecimal("0");
	                	invalidPricePoint = true;
	                }
	              }
	              
	              strTmp = XMLUtilities.selectSingleNodeText(itemElement,"./ItemPrice/Component[Type='Shipping']/Amount");
	              if( strTmp.length()>0 ) {
	                try{
	            	  itemShipping = new BigDecimal(strTmp);
	              	}catch(NumberFormatException e){
	              		itemShipping = new BigDecimal("0");
	                }
	              }
	              
	              strTmp = XMLUtilities.selectSingleNodeText(itemElement,"./ItemPrice/Component[Type='Tax']/Amount");
	              if( strTmp.length()>0 ) {
	                try{
	            	  itemTax = new BigDecimal(strTmp);
	              	}catch(NumberFormatException e){
	              		itemTax = new BigDecimal("0");
	                }
	              }
	              
	              strTmp = XMLUtilities.selectSingleNodeText(itemElement,"./ItemPrice/Component[Type='ShippingTax']/Amount");
	              if( strTmp.length()>0 ) {
	                try{
	            	  itemShippingTax = new BigDecimal(strTmp);
	              	}catch(NumberFormatException e){
	              		itemShippingTax = new BigDecimal("0");
	                }
	              }
	              
	              //Back out the discount if there is any
	              boolean isDiscountApplied = false;
	              if( dDiscount.compareTo(BigDecimal.ZERO)==0.0 ) 
	              {
	                itemComputedPrice = itemPrincipal;
	                itemDiscount= new BigDecimal("0");
	              }
	              else
	              {
	                if( StringUtils.equals("P",discountType) ) //Percentage discount
	                {
	                  //Compute the undiscounted price.  What the next line does:
	                  //1.  Subtract the discount from 100
	                  //2.  Divide the pricicipal by the result of #1 by 100 for the undiscounted percentage 
	                  //3.  Multiply the amount of the principal by the undiscounted percentage giving you total price unrounded
	                  //4.  Multiply it by 100 to get the 1/100 position to the right of the decimal point
	                  //5.  Round off to the nearest decimal (aka 5/4 rounding)
	                  //6.  Divide the result of #5 by 100
	                  
	                  //Need to do this to get rid of the trailing non-significant digits
	                	BigDecimal hundreths = new BigDecimal(0.01);
	                	BigDecimal productPrice = pdb_price.setScale(2, BigDecimal.ROUND_DOWN);
	                	BigDecimal discountPrice = productPrice.multiply(dDiscount).multiply(hundreths);
	                	discountPrice = discountPrice.setScale(2, BigDecimal.ROUND_DOWN);
	                	
	                  itemComputedPrice = itemPrincipal.add(discountPrice);
	                  itemDiscount = discountPrice;
	                  logger.debug("##for discount type P - itemComputedPrice: "+itemComputedPrice+" - itemDiscount: "+itemDiscount);
	                  isDiscountApplied = true;
	                }
	                else if( StringUtils.equals("D",discountType) ) //Dollar discount
	                {
	                  itemDiscount = dDiscount;
	                  itemComputedPrice = itemPrincipal.add(dDiscount);
	                  logger.debug("##for discount type D - itemComputedPrice: "+itemComputedPrice+" - itemDiscount: "+itemDiscount);
	                  isDiscountApplied = true;
	                }
	                else 
	                {
	                  throw new Exception("Amazon source code " + sourceCode + "is configured with an unsupported discount type of " + discountType );
	                }
	              }
	              
	              //discount computation price does not equal pdb price then take amazon price
	              if( isDiscountApplied && itemComputedPrice.compareTo(pdb_price)!=0 ){
	                itemComputedPrice = itemPrincipal;
	                itemDiscount=new BigDecimal("0");
	                logger.debug("Prices do not match, setting discount to 0");
	              }
	              //Increment the totals
	              totalComputedPrice = totalComputedPrice.add(itemComputedPrice);
	              //totalComputedPrice+=itemPrincipal;
	              totalDiscount = totalDiscount.add(itemDiscount);
	              totalShipping = totalShipping.add(itemShipping);
	              totalTax = totalTax.add(itemTax);
	              totalShippingTax = totalShippingTax.add(itemShippingTax);
	              
	              //Add the item amounts
	              ftdItemElement.appendChild(XMLUtilities.createElement(ftdOrderDoc,"order-total",fmt.format((itemComputedPrice.subtract(itemDiscount)).add(itemShipping).add(itemTax).add(itemShippingTax))));
	              ftdItemElement.appendChild(XMLUtilities.createElement(ftdOrderDoc,"tax-amount",fmt.format(itemTax)));
	              ftdItemElement.appendChild(XMLUtilities.createElement(ftdOrderDoc,"shipping-tax-amount",fmt.format(itemShippingTax)));
	              ftdItemElement.appendChild(XMLUtilities.createElement(ftdOrderDoc,"service-fee",fmt.format(itemShipping)));
	              
	              //Product id (always send the novator id)
	              ftdItemElement.appendChild(XMLUtilities.createElement(ftdOrderDoc,"productid",sku));
	              ftdItemElement.appendChild(XMLUtilities.createElement(ftdOrderDoc,"amazon-productid",amazonSku));
	              
	              //Quantity (will be validated at the validator)
	              ftdItemElement.appendChild(XMLUtilities.createElement(ftdOrderDoc,"quantity",XMLUtilities.selectSingleNodeText(itemElement,"./Quantity")));
	              
	              //Commission (grabs first entry only)
	              strTmp = XMLUtilities.selectSingleNodeText(itemElement,"./ItemFees/Fee[Type='Commission']/Amount");
	              if( strTmp.length()>0 ) {
	                //Comes in as a negative amount, change to positive
	                commission = new BigDecimal(strTmp).multiply(new BigDecimal(-1));
	              }
	              
	              //Commission
	              ftdItemElement.appendChild(XMLUtilities.createElement(ftdOrderDoc,"commission",fmt.format(commission)));
	              
	              //Product price
	              ftdItemElement.appendChild(XMLUtilities.createElement(ftdOrderDoc,"product-price",fmt.format(itemComputedPrice)));
	              logger.debug("XML product-price:" + fmt.format(itemComputedPrice));
	              
		            ////////////////////////////////BEGIN FULFILLMENT DATA     ///////////////////////////////
		  	        String recipientName = XMLUtilities.selectSingleNodeText(amazonOrderDoc,"/OrderReport/FulfillmentData/Address/Name");
		  	        if (recipientName == null) recipientName = "";
		  	        String recipientFirstName = StringUtils.substringBefore(recipientName," ");
		  	        if (recipientFirstName == null) recipientFirstName = "";
		  	      if (recipientFirstName == "" || recipientFirstName.isEmpty()){ recipientFirstName = "NA"; }
			        if (recipientFirstName.length() > 20) {
		                AmazonUtils.sendNoPageSystemMessage("Amazon Order Number: " + azOrderId + 
		                		"\nRecipient First Name too long: " + recipientFirstName);
		                recipientFirstName = recipientFirstName.substring(0, 20);
			        }
		  	        String recipientLastName = StringUtils.substringAfter(recipientName," ");
		  	        if (recipientLastName == null) recipientLastName = "";
		  	        if (recipientLastName == "" || recipientLastName.isEmpty()){ recipientLastName = "NA"; }
			        if (recipientLastName.length() > 20) {
		                AmazonUtils.sendNoPageSystemMessage("Amazon Order Number: " + azOrderId + 
		                		"\nRecipient Last Name too long: " + recipientLastName);
		                recipientLastName = recipientLastName.substring(0, 20);
			        }
		  	        //Recipient First Name
		  	        Element rFirstName = XMLUtilities.createElement(ftdOrderDoc,"recip-first-name", recipientFirstName); 
		  	        //Recipient Last Name
		  	        Element rLastName = XMLUtilities.createElement(ftdOrderDoc,"recip-last-name", recipientLastName); 
		  	        
		  	        //Recipient Address 1
		  	        String addressOne = XMLUtilities.selectSingleNodeText(amazonOrderDoc,"/OrderReport/FulfillmentData/Address/AddressFieldOne");
		  	        String addressTwo = XMLUtilities.selectSingleNodeText(amazonOrderDoc,"/OrderReport/FulfillmentData/Address/AddressFieldTwo");
		  	        String addressThree = XMLUtilities.selectSingleNodeText(amazonOrderDoc,"/OrderReport/FulfillmentData/Address/AddressFieldThree");
		  	        String newAddressOne = addressOne;
		  	        if (newAddressOne == null) newAddressOne = "";
		  	        String newAddressTwo = addressTwo;
		  	        if (newAddressTwo == null) newAddressTwo = "";
		  	        if (addressThree != null) {
		  	        	newAddressTwo = newAddressTwo.concat(addressThree);
		  	        }
		  	        if (newAddressOne.length() > 45) {
		  	        	String tempAddress = newAddressOne.substring(45, newAddressOne.length());
		  	        	if (newAddressTwo.length() > 0) tempAddress = tempAddress.concat("; ");
		  	        	newAddressTwo = tempAddress.concat(newAddressTwo);
		  	        	newAddressOne = newAddressOne.substring(0, 45);
		  	        }
		  	        if (newAddressTwo.length() > 45) {
		                AmazonUtils.sendNoPageSystemMessage("Amazon Order Number: " + azOrderId + 
		                		"\nRecipient Address too long: " + newAddressOne + "\n" + newAddressTwo);
		  	        	newAddressTwo = newAddressTwo.substring(0, 45);
		  	        }
		  	        Element rAddress1 = XMLUtilities.createElement(ftdOrderDoc,"recip-address1", newAddressOne);
		  	        
		  	        Element rAddress2 = XMLUtilities.createElement(ftdOrderDoc,"recip-address2", newAddressTwo);
		  	        
		  	        //Recipient City
		  	        String recipientCity = XMLUtilities.selectSingleNodeText(amazonOrderDoc,"/OrderReport/FulfillmentData/Address/City");
		  	        if (recipientCity == null) recipientCity = "";
			        if (recipientCity.length() > 30) {
		                AmazonUtils.sendNoPageSystemMessage("Amazon Order Number: " + azOrderId + 
		                		"\nRecipient City too long: " + recipientCity);
		                recipientCity = recipientCity.substring(0, 30);
			        }
		  	        Element rCity = XMLUtilities.createElement(ftdOrderDoc,"recip-city", recipientCity);
		  	        
		  	        //Recipient State
		  	        String recipientState = XMLUtilities.selectSingleNodeText(amazonOrderDoc,"/OrderReport/FulfillmentData/Address/StateOrRegion");
		  	        if (recipientState == null) recipientState = "";
			        CacheUtil cacheUtil = CacheUtil.getInstance();
			        logger.info("checking recipientState: " + recipientState);
			        StateMasterVO stateVO = cacheUtil.getStateById(recipientState);
			        if (stateVO == null) {
			        	logger.info("Not in cache, checking by name");
			        	stateVO = amazonDAO.getStateByName(recipientState);
			        	if (stateVO != null && stateVO.getStateMasterId() != null) {
			        		logger.debug("found recipientState: " + stateVO.getStateMasterId());
			        		recipientState = stateVO.getStateMasterId();
			        	}
			        }
			        if (recipientState.length() > 10) {
		                AmazonUtils.sendNoPageSystemMessage("Amazon Order Number: " + azOrderId + 
		                		"\nRecipient State too long: " + recipientState);
		                recipientState = recipientState.substring(0, 10);
			        }
		  	        Element rState = XMLUtilities.createElement(ftdOrderDoc,"recip-state", recipientState);
		  	        
		  	        //Recipient Postal
		  	        String recipientZip = XMLUtilities.selectSingleNodeText(amazonOrderDoc,"/OrderReport/FulfillmentData/Address/PostalCode");
		  	        if (recipientZip == null) recipientZip = "";
		  	        recipientZip = recipientZip.replaceAll(" ", "");
		  	        recipientZip = recipientZip.replaceAll("-", "");
			        if (recipientZip.length() > 9) {
		                AmazonUtils.sendNoPageSystemMessage("Amazon Order Number: " + azOrderId + 
		                		"\nRecipient Zip too long: " + recipientZip);
		                recipientZip = recipientZip.substring(0, 9);
			        }
		  	        Element rPostal = XMLUtilities.createElement(ftdOrderDoc,"recip-postal-code", recipientZip);
		  	        
		  	        //Recipient Country
		  	        String recipientCountry = XMLUtilities.selectSingleNodeText(amazonOrderDoc,"/OrderReport/FulfillmentData/Address/CountryCode");
		  	        if (recipientCountry == null) recipientCountry = "";
		  	        Element rCountry = XMLUtilities.createElement(ftdOrderDoc,"recip-country", recipientCountry);
		  	        
		  	        //Recipient Daytime Phone
		  	        String recipientPhone = XMLUtilities.selectSingleNodeText(amazonOrderDoc,"/OrderReport/FulfillmentData/Address/PhoneNumber");
		  	        if (recipientPhone == null) recipientPhone = "";
		  	        recipientPhone = Utils.removeAllSpecialChars(recipientPhone);
			        if (recipientPhone.length() > 10) {
		                AmazonUtils.sendNoPageSystemMessage("Amazon Order Number: " + azOrderId + 
		                		"\nRecipient Phone too long: " + recipientPhone);
		                recipientPhone = recipientPhone.substring(0, 10);
			        } else if (recipientPhone.length() < 10) {
			        	recipientPhone = "0000000000" + recipientPhone;
			        	recipientPhone = recipientPhone.substring(recipientPhone.length()-10);
			        	logger.debug("recipientPhone too short, changed to: " + recipientPhone);
			        }
		  	        Element rPhone = XMLUtilities.createElement(ftdOrderDoc,"recip-phone", recipientPhone);
		  	        ////////////////////////////////    END FULFILLMENT DATA     ///////////////////////////////
		  	        
	              //Recipient Info from above
	              ftdItemElement.appendChild(rFirstName);
	              ftdItemElement.appendChild(rLastName);
	              ftdItemElement.appendChild(rAddress1);
	              ftdItemElement.appendChild(rAddress2);
	              ftdItemElement.appendChild(rCity);
	              ftdItemElement.appendChild(rState);
	              ftdItemElement.appendChild(rPostal);
	              ftdItemElement.appendChild(rCountry );
	              ftdItemElement.appendChild(rPhone );
	              
	              //Ship To Type
	              ftdItemElement.appendChild(XMLUtilities.createElement(ftdOrderDoc,"ship-to-type",ConfigurationUtil.getInstance().getProperty("amazon_config.xml", "ship_to_type")));
	              
	              //Occasion
	              ftdItemElement.appendChild(XMLUtilities.createElement(ftdOrderDoc,"occasion",ConfigurationUtil.getInstance().getProperty("amazon_config.xml", "occasion")));
	              
	              //Card Message
	              ftdItemElement.appendChild(XMLUtilities.createElement(ftdOrderDoc,"card-message",XMLUtilities.selectSingleNodeText(itemElement,"./GiftMessageText")));
	              
	              //FOL Indicator
	              ftdItemElement.appendChild(XMLUtilities.createElement(ftdOrderDoc,"fol-indicator",ConfigurationUtil.getInstance().getProperty("amazon_config.xml", "fol_indicator")));
	              
	              //Sunday Delivery
	              ftdItemElement.appendChild(XMLUtilities.createElement(ftdOrderDoc,"sunday-delivery-flag",ConfigurationUtil.getInstance().getProperty("amazon_config.xml", "sunday_delivery_flag")));
	              
	              //Sender Release
	              ftdItemElement.appendChild(XMLUtilities.createElement(ftdOrderDoc,"sender-release-flag",ConfigurationUtil.getInstance().getProperty("amazon_config.xml", "sender_release_flag")));
	              
	              //Product Substitution Acknowledgement
	              ftdItemElement.appendChild(XMLUtilities.createElement(ftdOrderDoc,"product-substitution-acknowledgement",ConfigurationUtil.getInstance().getProperty("amazon_config.xml", "product_sub_ack")));
	              
	              //PDB price
	              ftdItemElement.appendChild(XMLUtilities.createElement(ftdOrderDoc,"pdb-price",fmt.format(pdb_price)));
	              logger.debug("XML pdb-price: " + fmt.format(pdb_price));
	              
	              //Transaction
	              ftdItemElement.appendChild(XMLUtilities.createElement(ftdOrderDoc,"transaction",fmt.format(itemPrincipal.add(itemShipping).add(itemTax).add(itemShippingTax))));
	              
	              //Wholesale
	              ftdItemElement.appendChild(XMLUtilities.createElement(ftdOrderDoc,"wholesale",fmt.format( itemPrincipal.add(itemShipping).add(itemTax).add(itemShippingTax).subtract(commission) )));
	              
	              //Item Discount
	              if( itemDiscount.compareTo(BigDecimal.ZERO) >0 )
	              {
	                //Item discount amount
	                ftdItemElement.appendChild(XMLUtilities.createElement(ftdOrderDoc,"discount-amount",fmt.format(itemDiscount)));
		            logger.debug("XML discount-amount: " + fmt.format(itemDiscount));
	              
	                //Item Discount Type
	                ftdItemElement.appendChild(XMLUtilities.createElement(ftdOrderDoc,"discount-type",discountType));
		            logger.debug("XML discount-type: " + discountType);
	                
	                //Discounted product price
	                ftdItemElement.appendChild(XMLUtilities.createElement(ftdOrderDoc,"discounted-product-price",fmt.format(itemPrincipal)));
		            logger.debug("XML discounted-product-price: " + fmt.format(itemPrincipal));
	              }
	              
	              //Amazon principal amount
	              ftdItemElement.appendChild(XMLUtilities.createElement(ftdOrderDoc,"amazon-principal-amount",fmt.format(itemPrincipal)));
	              
	              //Add to the items element
	              ftdItemsElement.appendChild(ftdItemElement);
	              
	              orderCounter++;
	              
	            }  //End Item loop
	        }
	        
	        //Add the order count in
	        orderCountElement.appendChild(ftdOrderDoc.createTextNode(String.valueOf(orderCounter)));
	        
	        //Add the order amount
	        orderAmountElement.appendChild(ftdOrderDoc.createTextNode(fmt.format( (totalComputedPrice.subtract(totalDiscount)).add(totalShipping).add(totalTax).add(totalShippingTax) )));
	        
	        //Add the discount total
	        if( totalDiscount.compareTo(BigDecimal.ZERO) >0 )
	        {
	          discountTotal.appendChild(ftdOrderDoc.createTextNode(fmt.format(totalDiscount)));
	        }
	        
	        //Finally put in the confirmation numbers
	        //We hold off on doing this till now to prevent gaps in sequence numbers because of errors
	        NodeList itemnl = XMLUtilities.selectXPathNodes(ftdItemsElement,"./item");
	        
	        if(itemnl != null) {
	            for(int i=0; i<itemnl.getLength(); i++) 
	            {
	              itemElement = (Element)itemnl.item(i);
	              Element idElement = XMLUtilities.selectSingleNode(itemElement,"./amazon-order-item-code");
	              
	              String confNumber = amazonDAO.getConfirmationNumber();
	              
	              //Add the confirmation number
	              itemElement.appendChild(XMLUtilities.createElement(ftdOrderDoc,"order-number",confNumber));
	            } //End item
	        }
	        
	        //Add items element to root
	        ftdRoot.appendChild(ftdItemsElement);
	        
	        //Make it a document
	        ftdOrderDoc.appendChild(ftdRoot);
	        
		}catch(Exception e){
			logger.error(e);
		}
		return ftdOrderDoc;
	}

	/**
	 * Saves FTD order xml (FTD_XML) into PTN_AMAZON.AZ_ORDER table
	 * @param ftdOrderDoc
	 * @param amazonOrderNumber
	 * @param orderStatusTransformed 
	 * @param conn
	 * @throws Exception
	 */
    private void saveFTDOrderXml(Document ftdOrderDoc, String amazonOrderNumber, String orderStatusTransformed, Connection conn) throws Exception {
		AmazonDAO amazonDAO = new AmazonDAO(conn);
        String ftd_xml = DOMUtil.domToString(ftdOrderDoc);
		amazonDAO.saveFTDOrderXml(amazonOrderNumber, ftd_xml, orderStatusTransformed);
                
	}

	/**
	 * Saves Amazon order details info to PTN_AMAZON.AZ_ORDER_DETAIL table
	 * @param ftdOrderDoc
	 * @param amazonOrderNumber
	 * @param conn
	 * @throws Exception
	 */
    private void saveAmazonOrderDetailsInfo(Document ftdOrderDoc, String amazonOrderNumber, Connection conn) throws Exception {
    	logger.info("AmazonOrderBO : saveAmazonOrderDetailsInfo()");
		AmazonDAO amazonDAO = new AmazonDAO(conn);
		
        NodeList ftdList = XMLUtilities.selectXPathNodes(ftdOrderDoc.getDocumentElement(),"/order/items/item");
		String amazonOrderItemNumber = null;
		String confirmationNumber = null;
        try {
        	if(ftdList != null) {
        		for (int i=0; i<ftdList.getLength(); i++) {
        			Element itemElement = (Element)ftdList.item(i);
        			amazonOrderItemNumber = XMLUtilities.selectSingleNodeText(itemElement,"./amazon-order-item-code");
        			confirmationNumber = XMLUtilities.selectSingleNodeText(itemElement,"./order-number");
        			String amazonProductId = XMLUtilities.selectSingleNodeText(itemElement, "./amazon-productid");

  	                String principal = XMLUtilities.selectSingleNodeText(itemElement,"./amazon-principal-amount");
  	                String shipping = XMLUtilities.selectSingleNodeText(itemElement,"./service-fee");
  	                String tax = XMLUtilities.selectSingleNodeText(itemElement,"./tax-amount");
  	                String shippingTax = XMLUtilities.selectSingleNodeText(itemElement,"./shipping-tax-amount");
  	                logger.info(principal + " " + shipping + " " + tax + " " + shippingTax);

  	                AzOrderDetailVO azVO = new AzOrderDetailVO();
  	                azVO.setAmazonItemNumber(amazonOrderItemNumber);
  	                azVO.setAmazonOrderNumber(amazonOrderNumber);
  	                azVO.setConfirmationNumber(confirmationNumber);
  	                azVO.setAmazonProductId(amazonProductId);
  	                if (principal == null || principal.equals("")) {
  	                	azVO.setPrincipalAmount(new BigDecimal("0"));
  	                } else {
  	                	azVO.setPrincipalAmount(new BigDecimal(principal));
  	                }
  	                if (shipping == null || shipping.equals("")) {
  	                	azVO.setShippingAmount(new BigDecimal("0"));
  	                } else {
  	                	azVO.setShippingAmount(new BigDecimal(shipping));
  	                }
  	                if (tax == null || tax.equals("")) {
  	                	azVO.setTaxAmount(new BigDecimal("0"));
  	                } else {
  	                	azVO.setTaxAmount(new BigDecimal(tax));
  	                }
  	                if (shippingTax == null || shippingTax.equals("")) {
  	                	azVO.setShippingTaxAmount(new BigDecimal("0"));
  	                } else {
  	                	azVO.setShippingTaxAmount(new BigDecimal(shippingTax));
  	                }
  	                amazonDAO.saveAmazonOrderDetailsInfo(azVO);
        		}
        	}
        } catch (Exception exec) {
        	StringBuilder errorMess = new StringBuilder(" AmazonItemNumber: ");
        	errorMess.append(amazonOrderItemNumber);
        	errorMess.append(", ConfirmationNumber: ");
        	errorMess.append(confirmationNumber);
        	errorMess.append(", ");
        	errorMess.append(exec.getMessage());
        	logger.error("ERROR in saveAmazonOrderDetailsInfo : "+errorMess.toString());   
        	logger.error(exec.fillInStackTrace());
        	AmazonUtils.sendPageSystemMessage(errorMess.toString());
        }
	}

	/**
	 * Sends FTD order xml to OrderGatherer
	 * @param ftdOrderDoc
	 * @param amazonOrderNumber 
	 * @param conn
	 */
    private void sendToOrderGatherer(Document ftdOrderDoc, String amazonOrderNumber, Connection conn) {
    	logger.debug("sendOrderToOrderGatherer()");

        boolean success = false;

        try {

            ConfigurationUtil cu = ConfigurationUtil.getInstance();
            String orderGathererURL = cu.getFrpGlobalParm(AmazonConstants.AMZ_GLOBAL_CONTEXT,
            		AmazonConstants.AMZ_ORDER_GATHERER_URL);
            logger.debug("orderGathererURL: " + orderGathererURL);

            String strXml = DOMUtil.domToString(ftdOrderDoc);
            String response;
            int result;
            
            PostMethod post = new PostMethod(orderGathererURL);
            NameValuePair nvPair = null;
            NameValuePair[] nvPairArray = new NameValuePair[1];
            String name, value;
            
            name = "Order";
            value = strXml;
            nvPair = new NameValuePair(name, value);
            nvPairArray[0] = nvPair;

            post.setRequestBody(nvPairArray);
            
            HttpClient httpclient = new HttpClient();
            // Execute request
            try {
                result = httpclient.executeMethod(post);
                response = post.getResponseBodyAsString();
            } finally {
                // Release current connection to the connection pool once you are done
                post.releaseConnection();
            }
            String message = "Http Response Code is " + result;
            logger.debug(message);
            
            /*if (result == 200) {
                success = true;
                System.out.println(success);
            }*/
            if( result==200 )
            {
            	success = true;
                updateOrderStatus(conn,amazonOrderNumber,AmazonConstants.ORDER_STATUS_DONE);
            }
            else
            {   
                //convert the order object to a string
                //String strOrder=XMLUtilities.domToString(ftdOrder);
                if( result==600 ) 
                {
                	success = false;
                	//log the error and send email to IT and marketing
                    logger.error(response);
                    updateOrderStatus(conn,amazonOrderNumber,AmazonConstants.ORDER_STATUS_GATHERER_ERROR);
                    //Send to the error table
                    //sendToAmazonErrorTable(connection,response,strOrder);*/
                    
                    //send no-page email
                    AmazonUtils.sendNoPageSystemMessage("Amazon-Order-Number: "+amazonOrderNumber+"\n ERROR-Details: "+response);
                }
                else
                {
                    //throw new AmazonException("OrderInfuserHandler received error from order gatherer: " + response); 
                }
             }
        } catch (Exception e) {
            logger.error(e);
            success = false;
        }

        //return success;
	}
	
	private void updateOrderStatus(Connection conn,
			String amazonOrderNumber, String orderStatus) throws Exception {
		AmazonDAO amazonDAO = new AmazonDAO(conn);
		amazonDAO.updateOrderStatus(amazonOrderNumber, orderStatus);
	}

	public static void main(String[] args) {
		try{
			AmazonOrderBO orderBO = new AmazonOrderBO();		
			orderBO.processInboundOrder("002-4436726-4265844");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private Connection testConnection() throws Exception {
		String driver_ = "oracle.jdbc.driver.OracleDriver";
        String database_ = "jdbc:oracle:thin:@adonis.ftdi.com:1521:animal";
        String user_ = "";
        String password_ = "";
                
        Class.forName(driver_);                               
	        
	    return DriverManager.getConnection(database_, user_, password_);     
	}
	
	private Date getNextAvailableDate(Connection conn, String itemSku, String zipCode, int days,
			String shipMethodFlorist, String shipMethodCarrier) throws Exception {
      Date nextAvailableDate = null;
      try {
    	  ProductAvailabilityBO paBO = new ProductAvailabilityBO();
	      ProductAvailVO[] paVOs = paBO.getProductAvailableDates(conn,itemSku, zipCode,null, days, null);
	      for (ProductAvailVO paVO : paVOs) {
	    	  if (paVO.getIsAvailable()) {
	    		  
    		      Calendar availableDate = paVO.getFloristCutoffDate();
                  if (availableDate != null && shipMethodFlorist.equalsIgnoreCase("Y")) {	  
    	    	      Calendar todayDate = Calendar.getInstance();
    		          //Don't allow same-day deliveries
    		          if (availableDate.after(todayDate)) {
                          if (availableDate.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
            	   	          if ( (ConfigurationUtil.getInstance().getFrpGlobalParm("AMAZON_CONFIG", "SAT_DELIVERY_ALLOWED_FLORAL")).equalsIgnoreCase("N") ) {
              	                  continue;
        		              }
            	   	      //Don't allow Sunday delivery
                          } else if (availableDate.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
                        	  continue;
                          }
            	          nextAvailableDate = availableDate.getTime();
            	          break;
    	    	      } else {
    		        	  logger.info("Skipping same-day delivery");
    		          }
                  } else {
                	  if (shipMethodCarrier.equalsIgnoreCase("Y")) {
                		  Calendar deliveryDate = paVO.getDeliveryDate();
                	      Calendar shipND = paVO.getShipDateND();
                	      if (shipND != null) {
                	          nextAvailableDate = deliveryDate.getTime();
                   	    	  break;  
                	      } else {
                    	      Calendar ship2D = paVO.getShipDate2D();
                    	      if (ship2D != null) {
                    	          nextAvailableDate = deliveryDate.getTime();
                       	    	  break;
                	          } else {
                    	          Calendar shipGR = paVO.getShipDateGR();
                    	          if (shipGR != null) {
                    	              nextAvailableDate = deliveryDate.getTime();
                       	    	      break;  
                	              } else {
                    	              Calendar shipSA = paVO.getShipDateSA();
                    	              if (shipSA != null) {
            	                          if ( (ConfigurationUtil.getInstance().getFrpGlobalParm("AMAZON_CONFIG", "SAT_DELIVERY_ALLOWED_DROPSHIP")).equalsIgnoreCase("N") ) {
          	                                  continue;
          	                              }        	              
                   	                      nextAvailableDate = deliveryDate.getTime();
                       	    	          break;
                    	              }
                	              }
                	          }
                	      }
            	      }
                  }
	    	  }
          }
      }
      catch(Exception e) {
    	  e.printStackTrace();
    	  logger.error("getNextAvailableDate Exception: " + e);
      }
	  return nextAvailableDate;
	}
	
	private String getShipMethod(Connection conn, String itemSku, Date deliveryDate, String zipCode,
			String shipMethodFlorist, String shipMethodCarrier) throws Exception {
		String shipMethod = null;
		boolean isShipMethodSet = false;
        SimpleDateFormat sdfTime = new SimpleDateFormat("yyyyMMddHHmm");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		try {  
			
			ProductAvailabilityBO paBO = new ProductAvailabilityBO();
	      
			if(zipCode.length() > 5) {
				logger.info("Zip Code length is too long, using substring of 5 digits in - " + zipCode);
				zipCode = zipCode.substring(0,5);
			}
	      
		    ProductAvailVO paVO = paBO.getProductAvailability(conn,itemSku, deliveryDate, zipCode, null,null);
		    if (paVO.getIsAvailable()) {
    		    Calendar floristDate = paVO.getFloristCutoffDate();
		  
    		    if (shipMethodFlorist.equalsIgnoreCase("Y")) {
                    if (floristDate != null) {
        	            shipMethod = "SD";
        	            isShipMethodSet = true;
                    }
                }
    		    
    		    if (shipMethodCarrier.equalsIgnoreCase("Y")) {
                    Date today = new Date();
                    String todayString = sdfTime.format(today);
                    logger.debug("today: " + todayString);

                    if(paVO.getShipDateGR() != null && !isShipMethodSet){
                        Date shipDateGR = paVO.getShipDateGR().getTime();
                        String cutoffGR = paVO.getShipDateGRCutoff();
                        String cutoffString = sdf.format(shipDateGR) + cutoffGR;
                        logger.debug("GR cutoff: " + cutoffString);
                        if (todayString.compareTo(cutoffString) <= 0) {
            	            shipMethod = "GR";
            	            isShipMethodSet = true;
                        }
                    }
                    if(paVO.getShipDate2D() != null && !isShipMethodSet){
                        Date shipDate2D = paVO.getShipDate2D().getTime();
                        String cutoff2D = paVO.getShipDate2DCutoff();
                        String cutoffString = sdf.format(shipDate2D) + cutoff2D;
                        logger.debug("2F cutoff: " + cutoffString);
                        if (todayString.compareTo(cutoffString) <= 0) {
            	            shipMethod = "2F";
            	            isShipMethodSet = true;
                        }
                    }
                    if(paVO.getShipDateND() != null && !isShipMethodSet){
                        Date shipDateND = paVO.getShipDateND().getTime();
                        String cutoffND = paVO.getShipDateNDCutoff();
                        String cutoffString = sdf.format(shipDateND) + cutoffND;
                        logger.debug("ND cutoff: " + cutoffString);
                        if (todayString.compareTo(cutoffString) <= 0) {
            	            shipMethod = "ND";
            	            isShipMethodSet = true;
                        }
                    }
                    if(paVO.getShipDateSA() != null && !isShipMethodSet){
                        Date shipDateSA = paVO.getShipDateSA().getTime();
                        String cutoffSA = paVO.getShipDateSACutoff();
                        String cutoffString = sdf.format(shipDateSA) + cutoffSA;
                        logger.debug("SA cutoff: " + cutoffString);
                        if (todayString.compareTo(cutoffString) <= 0) {
            	            shipMethod = "SA";
            	            isShipMethodSet = true;
                        }
                    }
                    
                    if(paVO.getShipDateSU() != null && !isShipMethodSet){
                        Date shipDateSU = paVO.getShipDateSU().getTime();
                        String cutoffSU = paVO.getShipDateSUCutoff();
                        String cutoffString = sdf.format(shipDateSU) + cutoffSU;
                        logger.debug("SU cutoff: " + cutoffString);
                        if (todayString.compareTo(cutoffString) <= 0) {
            	            shipMethod = "SU";
            	            isShipMethodSet = true;
                        }
                    }
    		    }
		    }
		} catch(Exception e) {
	    	e.printStackTrace();
	    	logger.error("getShipMethod Exception: " + e);
	    }
		return shipMethod;
	}
}