package com.ftd.amazon.bo;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.amazonaws.mws.model.FeedSubmissionInfo;
import com.amazonaws.mws.model.FeedType;
import com.amazonaws.mws.model.SubmitFeedResponse;
import com.amazonaws.mws.model.SubmitFeedResult;
import com.ftd.amazon.common.AmazonConstants;
import com.ftd.amazon.common.AmazonUtils;
import com.ftd.amazon.dao.InventoryFeedDAO;
import com.ftd.amazon.exceptions.AmazonApplicationException;
import com.ftd.amazon.vo.InventoryFeedDetailVO;
import com.ftd.amazon.vo.InventoryFeedVO;
import com.ftd.amazon.vo.ProductMasterVO;
import com.ftd.amazon.vo.amazon.AmazonEnvelope;
import com.ftd.amazon.vo.amazon.Header;
import com.ftd.amazon.vo.amazon.Inventory;
import com.ftd.amazon.vo.amazon.Message;
import com.ftd.osp.utilities.plugins.Logger;

public class InventoryFeedBO 
{ 
  
    private Logger logger = new Logger("com.ftd.amazon.bo.InventoryFeedBO");
    
    public InventoryFeedBO() 
    {
    }
   
    public void processInventoryFeed() 
    {
    	logger.info("Processing Inventory Feed..");
    	
    	Connection conn = null;
    	try
    	{
    		conn = AmazonUtils.getNewConnection();
    		InventoryFeedDAO feedDAO = new InventoryFeedDAO(conn);
	    	InventoryFeedVO inventoryFeedData = feedDAO.getInventoryFeedData(AmazonConstants.FEED_STATUS_NEW);
	    	if(inventoryFeedData.getFeedDetails()==null || inventoryFeedData.getFeedDetails().isEmpty()){
	    		logger.info("No Inventory Feed Updates to Send to Amazon");
	    		return;
	    	}
	    	AmazonEnvelope amazonEnvelope = this.buildInventoryFeedEnvelope(inventoryFeedData);
	    	if(amazonEnvelope.getMessages().isEmpty()){
	    		logger.info("No Inventory Feed Updates to Send to Amazon");
	    		return;
	    	}
	    	String feedContent = amazonEnvelope.marshallAsXML();
	    	logger.debug("FeedContent XML :"+feedContent);
	    	
	    	String fileName = AmazonFeedBO.saveFeedXML(feedContent, AmazonConstants.INVENTORY_FEED_NAME_PREFIX);
	    	
	    	SubmitFeedResponse response = 
	    		AmazonFeedBO.submitFeed(FeedType.POST_INVENTORY_AVAILABILITY_DATA ,fileName);
	    	
	        // add response handling code
	        SubmitFeedResult submitFeedResult = response.getSubmitFeedResult();
	        FeedSubmissionInfo feedSubmissionInfo = submitFeedResult.getFeedSubmissionInfo();
	        
	        List<InventoryFeedDetailVO> feedDetails = inventoryFeedData.getFeedDetails();
	        for (InventoryFeedDetailVO vo : feedDetails) 
	        {
				//vo.setFeedStatus(feedSubmissionInfo.getFeedProcessingStatus());
				vo.setFeedStatus(AmazonConstants.FEED_STATUS_SENT);
				vo.setServer(AmazonUtils.getLocalHostName());
				vo.setFilename(fileName);
				vo.setTransactionId(feedSubmissionInfo.getFeedSubmissionId());
				feedDAO.saveFeedStatus(vo);
	        }
    	}
        catch (Exception e) 
        {
			logger.error(e);
			throw new AmazonApplicationException(e.getMessage());
		}
        finally
        {
        	if(conn!= null){ try { conn.close(); } catch (SQLException e) {} }
        }
    }
 
    private AmazonEnvelope buildInventoryFeedEnvelope(InventoryFeedVO inventoryFeedVO)
    {
    	AmazonEnvelope amazonEnvelope = new AmazonEnvelope();
    	Header header = new Header();
    	header.setDocumentVersion("1.01");
    	header.setMerchantIdentifier(AmazonUtils.getMerchantId());
		amazonEnvelope.setHeader(header );
		amazonEnvelope.setMessageType("Inventory");
		int counter = 1;
		List<InventoryFeedDetailVO> feedDetails = inventoryFeedVO.getFeedDetails();
    	for (InventoryFeedDetailVO feedDetail : feedDetails) 
    	{
    		List<Inventory> inventoryList = this.buildInventoryList(feedDetail);
    		for (Inventory inventory : inventoryList) 
    		{
    			Message message = new Message();
        		message.setMessageID(new BigInteger(String.valueOf(counter)));
        		message.setOperationType("Update");
        		message.setInventory(inventory);
        		amazonEnvelope.getMessages().add(message);
        		counter++;
			}
		}
    	return amazonEnvelope;
    }
    
    protected List<Inventory> buildInventoryList(InventoryFeedDetailVO inventoryFeedDetailVO) 
    {
    	List<Inventory> inventoryList = new ArrayList<Inventory>();
    	
    	ProductMasterVO productFeedStatus = inventoryFeedDetailVO.getProductFeedStatus();
		if(productFeedStatus.isCatalogStatusStandardActive() 
				&& productFeedStatus.isSentToAmazonStandardFlag())
		{
			Inventory inventory = new Inventory();
    		inventory.setSKU(inventoryFeedDetailVO.getProductId()+AmazonConstants.CATALOG_SUFFIX_STANDARD);
    		if (productFeedStatus.getProductMasterStatus() != null && productFeedStatus.getProductMasterStatus().equals("A")) {
    		    inventory.setAvailable(true);
    		} else {
    			inventory.setAvailable(false);
    		}
    		inventoryList.add(inventory);
		}
		
		if(productFeedStatus.isCatalogStatusDeluxeActive() 
				&& productFeedStatus.isSentToAmazonDeluxeFlag())
		{
			Inventory inventory = new Inventory();
    		inventory.setSKU(inventoryFeedDetailVO.getProductId()+AmazonConstants.CATALOG_SUFFIX_DELUXE);
    		if (productFeedStatus.getProductMasterStatus() != null && productFeedStatus.getProductMasterStatus().equals("A")) {
    		    inventory.setAvailable(true);
    		} else {
    			inventory.setAvailable(false);
    		}
    		
    		inventoryList.add(inventory);
		}
		
		if(productFeedStatus.isCatalogStatusPremiumActive() 
				&& productFeedStatus.isSentToAmazonPremiumFlag())
		{
			Inventory inventory = new Inventory();
    		inventory.setSKU(inventoryFeedDetailVO.getProductId()+AmazonConstants.CATALOG_SUFFIX_PREMIUM);
    		if (productFeedStatus.getProductMasterStatus() != null && productFeedStatus.getProductMasterStatus().equals("A")) {
    		    inventory.setAvailable(true);
    		} else {
    			inventory.setAvailable(false);
    		}
    		
    		inventoryList.add(inventory);
		}
		return inventoryList;
	}
    
}