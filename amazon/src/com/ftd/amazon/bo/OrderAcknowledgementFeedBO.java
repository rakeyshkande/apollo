/**
 * 
 */
package com.ftd.amazon.bo;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.amazonaws.mws.model.FeedSubmissionInfo;
import com.amazonaws.mws.model.FeedType;
import com.amazonaws.mws.model.SubmitFeedResponse;
import com.amazonaws.mws.model.SubmitFeedResult;
import com.ftd.amazon.common.AmazonConstants;
import com.ftd.amazon.common.AmazonUtils;
import com.ftd.amazon.dao.OrderAckFeedDAO;
import com.ftd.amazon.exceptions.AmazonApplicationException;
import com.ftd.amazon.vo.OrderAckFeedVO;
import com.ftd.amazon.vo.amazon.AmazonEnvelope;
import com.ftd.amazon.vo.amazon.Header;
import com.ftd.amazon.vo.amazon.Message;
import com.ftd.amazon.vo.amazon.OrderAcknowledgement;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * @author skatam
 *
 */
public class OrderAcknowledgementFeedBO 
{
	private static Logger logger = new Logger("com.ftd.amazon.bo.OrderAcknowledgementFeedBO");
	
	public void processOrderAcknowledgementFeed() 
	{
    	logger.debug("******* processOrderAcknowledgementFeed() *********");
    	
    	Connection conn = null;
    	try
    	{
    		conn = AmazonUtils.getNewConnection();
    		OrderAckFeedDAO feedDAO = new OrderAckFeedDAO(conn);
	    	List<OrderAckFeedVO> orderAcknowledgements = feedDAO.getOrderAckFeedData(AmazonConstants.FEED_STATUS_NEW);
	    	if(orderAcknowledgements==null || orderAcknowledgements.isEmpty()){
	    		logger.info("No Order Acknowledgement Feed Updates to Send to Amazon");
	    		return;
	    	}
	    	AmazonEnvelope amazonEnvelope = this.buildOrderAckFeedEnvelope(feedDAO,orderAcknowledgements);
	    	
	    	String feedContent = amazonEnvelope.marshallAsXML();
	    	logger.debug("FeedContent XML :"+feedContent);
	    	String fileName = AmazonFeedBO.saveFeedXML(feedContent, AmazonConstants.ORDER_ACK_FEED_NAME_PREFIX);
	    	
	    	SubmitFeedResponse response = AmazonFeedBO.submitFeed(FeedType.POST_ORDER_ACKNOWLEDGEMENT_DATA ,fileName);
			
	        // add response handling code
	        SubmitFeedResult submitFeedResult = response.getSubmitFeedResult();
	        FeedSubmissionInfo feedSubmissionInfo = submitFeedResult.getFeedSubmissionInfo();
	        
	        for (OrderAckFeedVO vo : orderAcknowledgements) 
	        {
				vo.setFeedStatus(AmazonConstants.FEED_STATUS_SENT);
				vo.setServer(AmazonUtils.getLocalHostName());
				vo.setFilename(fileName);
				vo.setTransactionId(feedSubmissionInfo.getFeedSubmissionId());
				feedDAO.saveFeedStatus(vo);
	        }
	        
    	}
        catch (Exception e) {
			logger.error(e);
			throw new AmazonApplicationException(e.getMessage());
		}
        finally
        {
        	if(conn!= null){ try { conn.close(); } catch (SQLException e) {} }
        }
    	
    }

	private AmazonEnvelope buildOrderAckFeedEnvelope(OrderAckFeedDAO feedDAO, 
			List<OrderAckFeedVO> orderAcknowledgements) 
	{
    	AmazonEnvelope amazonEnvelope = new AmazonEnvelope();
    	Header header = new Header();
    	header.setDocumentVersion("1.01");
    	header.setMerchantIdentifier(AmazonUtils.getMerchantId());
		amazonEnvelope.setHeader(header );
		amazonEnvelope.setMessageType("OrderAcknowledgement");
		int counter = 1;
    	
		Map<String, List<OrderAckFeedVO>> orderAckMap = new HashMap<String, List<OrderAckFeedVO>>();
		
		for (OrderAckFeedVO feedDetail : orderAcknowledgements) 
    	{
    		if(!orderAckMap.containsKey(feedDetail.getAzOrderID())){
    			orderAckMap.put(feedDetail.getAzOrderID(), new ArrayList<OrderAckFeedVO>());
    		}
    		orderAckMap.get(feedDetail.getAzOrderID()).add(feedDetail);
		}
		
		Set<String> keySet = orderAckMap.keySet();
		for (String key : keySet) 
		{
			List<OrderAckFeedVO> feedDetailList = orderAckMap.get(key);
			OrderAckFeedVO ackFeedVO = feedDetailList.get(0);
			Message message = new Message();
    		message.setMessageID(new BigInteger(""+counter));
    		
			OrderAcknowledgement orderAcknowledgement = new OrderAcknowledgement();
			orderAcknowledgement.setAmazonOrderID(ackFeedVO.getAzOrderID());
			orderAcknowledgement.setMerchantOrderID(ackFeedVO.getMasterOrderNumber());
			orderAcknowledgement.setStatusCode(ackFeedVO.getStatusCode());
			
			for (OrderAckFeedVO orderAckFeedVO : feedDetailList) 
			{
				OrderAcknowledgement.Item item = new OrderAcknowledgement.Item();
				item.setAmazonOrderItemCode(orderAckFeedVO.getAzOrderItemNumber());
				item.setMerchantOrderItemID(orderAckFeedVO.getConfirmationNumber());
				
				if("Failure".equalsIgnoreCase(ackFeedVO.getStatusCode())){
					item.setCancelReason(ackFeedVO.getCancelReason());
				}
				
				orderAcknowledgement.getItem().add(item);
			}
			message.setOrderAcknowledgement(orderAcknowledgement);
    		amazonEnvelope.getMessages().add(message);
    		counter++;
		}
		
    	return amazonEnvelope;
    }
}
