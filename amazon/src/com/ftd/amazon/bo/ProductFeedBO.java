/**
 * 
 */
package com.ftd.amazon.bo;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.amazonaws.mws.model.FeedSubmissionInfo;
import com.amazonaws.mws.model.FeedType;
import com.amazonaws.mws.model.SubmitFeedResponse;
import com.amazonaws.mws.model.SubmitFeedResult;
import com.ftd.amazon.common.AmazonConstants;
import com.ftd.amazon.common.AmazonUtils;
import com.ftd.amazon.dao.ProductFeedDAO;
import com.ftd.amazon.exceptions.AmazonApplicationException;
import com.ftd.amazon.vo.BaseFeedDetailVO;
import com.ftd.amazon.vo.ProductAttributesVO;
import com.ftd.amazon.vo.ProductMasterVO;
import com.ftd.amazon.vo.amazon.AmazonEnvelope;
import com.ftd.amazon.vo.amazon.ConditionInfo;
//import com.ftd.amazon.vo.amazon.FoodAndBeverages;
//import com.ftd.amazon.vo.amazon.FoodAndBeverages.ProductType;
import com.ftd.amazon.vo.amazon.Header;
//import com.ftd.amazon.vo.amazon.HouseholdSupplies;
import com.ftd.amazon.vo.amazon.Message;
import com.ftd.amazon.vo.amazon.Miscellaneous;
import com.ftd.amazon.vo.amazon.Product;
import com.ftd.amazon.vo.amazon.Product.DescriptionData;
import com.ftd.amazon.vo.amazon.Product.ProductData;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * @author skatam
 *
 */
public class ProductFeedBO 
{
	private static Logger logger = new Logger("com.ftd.amazon.bo.ProductFeedBO");
	
	public void processProductFeed() 
	{
    	logger.info("processProductFeed");
    	Connection conn = null;
    	try
    	{
    		conn = AmazonUtils.getNewConnection();
    		
    		ProductFeedDAO feedDAO = new ProductFeedDAO(conn);
    		
    		List<BaseFeedDetailVO> productFeedData = feedDAO.getProductMasterData(AmazonConstants.FEED_STATUS_NEW);
	    	if(productFeedData.isEmpty())
	    	{
	    		logger.info("No Product Feed Updates to Send to Amazon");
	    		return;
	    	}

	    	AmazonEnvelope amazonEnvelope = this.buildProductFeedEnvelope(productFeedData, feedDAO);
	    	if(amazonEnvelope.getMessages().isEmpty()){
	    		logger.info("No Product Feed Updates to Send to Amazon");
	    		return;
	    	}
	    	String feedContent = amazonEnvelope.marshallAsXML();
	    	logger.debug("FeedContent XML :"+feedContent);
	    	String fileName = AmazonFeedBO.saveFeedXML(feedContent, AmazonConstants.PRODUCT_FEED_NAME_PREFIX);
	    	
	    	SubmitFeedResponse response = AmazonFeedBO.submitFeed(FeedType.POST_PRODUCT_DATA ,fileName);
			
	        // add response handling code
	        SubmitFeedResult submitFeedResult = response.getSubmitFeedResult();
	        FeedSubmissionInfo feedSubmissionInfo = submitFeedResult.getFeedSubmissionInfo();
	        
	        for (BaseFeedDetailVO vo : productFeedData) 
	        {
				//vo.setFeedStatus(feedSubmissionInfo.getFeedProcessingStatus());
				vo.setFeedStatus(AmazonConstants.FEED_STATUS_SENT);
				vo.setServer(AmazonUtils.getLocalHostName());
				vo.setFilename(fileName);
				vo.setTransactionId(feedSubmissionInfo.getFeedSubmissionId());
                                // updateProductPriceSendFlags has to be first so that another feed is not triggered
				feedDAO.updateProductPriceSentFlags(vo);
				feedDAO.saveFeedStatus(vo);
	        }
	        
    	}
        catch (Exception e) {
        	e.printStackTrace();
			logger.error(e);
			throw new AmazonApplicationException(e.getMessage());
		}
        finally
        {
        	if(conn!= null){ try { conn.close(); } catch (SQLException e) {} }
        }
    }

	private AmazonEnvelope buildProductFeedEnvelope(List<BaseFeedDetailVO> productFeedData, ProductFeedDAO feedDAO) 
	{
		logger.info("buildProductFeedEnvelope() size: " + productFeedData.size());
    	AmazonEnvelope amazonEnvelope = new AmazonEnvelope();
    	Header header = new Header();
    	header.setDocumentVersion("1.01");
    	header.setMerchantIdentifier(AmazonUtils.getMerchantId());
		amazonEnvelope.setHeader(header );
		amazonEnvelope.setMessageType("Product");
		int counter = 1;

		try {
			
        	for (BaseFeedDetailVO baseFeedDetailVO : productFeedData) 
        	{
    	    	ProductMasterVO productMasterVO = baseFeedDetailVO.getProductFeedStatus();
    		    List<Product> productList = this.buildProductList(feedDAO, baseFeedDetailVO);
    	
                for (Product product : productList) {
            		Message message = new Message();
            		message.setMessageID(new BigInteger(""+counter));
    	    	
        	    	if (product.getActiveFlag() != null && product.getActiveFlag().equals("Y")) {
    		            message.setOperationType("PartialUpdate");
            		} else {
            			message.setOperationType("Delete");
            		}
    		
    		        message.setProduct(product);
    		
        		    amazonEnvelope.getMessages().add(message);
    		
            		counter++;
                }
        	}
		} catch (Exception e) {
        	e.printStackTrace();
			logger.error(e);
		}
		
    	return amazonEnvelope;
    }
	
	protected List<Product> buildProductList(ProductFeedDAO feedDAO, BaseFeedDetailVO feedDetailVO) throws Exception
    {
		logger.info("buildProductList()");
		ProductMasterVO productFeedStatus = feedDetailVO.getProductFeedStatus();
		
    	List<Product> productList = new ArrayList<Product>();
    	
    	ProductAttributesVO productAttributes = 
    		feedDAO.getProductAttributes(feedDetailVO.getFeedId(), feedDetailVO.getProductId());
    	
    	if(productFeedStatus.isCatalogStatusStandardActive())
		{
    		Product stdProduct = new Product();
    		this.populateProductBaseDetails(stdProduct, productFeedStatus);
			stdProduct.setSKU(feedDetailVO.getProductId()+AmazonConstants.CATALOG_SUFFIX_STANDARD);
			stdProduct.getDescriptionData().setTitle(productFeedStatus.getProductNameStandard());
			stdProduct.getDescriptionData().setDescription(productFeedStatus.getProductDescriptionStandard());
			stdProduct.setActiveFlag("Y");
			
			this.populateProductAttributes(stdProduct, productAttributes);
    		productList.add(stdProduct);
		} else if (productFeedStatus.isSentToAmazonStandardFlag()) {
    		Product stdProduct = new Product();
    		//this.populateProductBaseDetails(stdProduct, productFeedStatus);
			stdProduct.setSKU(feedDetailVO.getProductId()+AmazonConstants.CATALOG_SUFFIX_STANDARD);
			stdProduct.setActiveFlag("N");
    		productList.add(stdProduct);
		}

    	if(productFeedStatus.isCatalogStatusDeluxeActive())
		{
    		Product deluxeProduct = new Product();
    		this.populateProductBaseDetails(deluxeProduct, productFeedStatus);
			deluxeProduct.setSKU(feedDetailVO.getProductId()+AmazonConstants.CATALOG_SUFFIX_DELUXE);
    		deluxeProduct.getDescriptionData().setTitle(productFeedStatus.getProductNameDeluxe());
    		deluxeProduct.getDescriptionData().setDescription(productFeedStatus.getProductDescriptionDeluxe());
			deluxeProduct.setActiveFlag("Y");
   		
    		this.populateProductAttributes(deluxeProduct, productAttributes);
    		productList.add(deluxeProduct);
		} else if (productFeedStatus.isSentToAmazonDeluxeFlag()) {
    		Product deluxeProduct = new Product();
    		//this.populateProductBaseDetails(deluxeProduct, productFeedStatus);
			deluxeProduct.setSKU(feedDetailVO.getProductId()+AmazonConstants.CATALOG_SUFFIX_DELUXE);
			deluxeProduct.setActiveFlag("N");
    		productList.add(deluxeProduct);
		}
		
    	if(productFeedStatus.isCatalogStatusPremiumActive())
		{
    		Product premiumProduct = new Product();
    		this.populateProductBaseDetails(premiumProduct, productFeedStatus);
			premiumProduct.setSKU(feedDetailVO.getProductId()+AmazonConstants.CATALOG_SUFFIX_PREMIUM);
			premiumProduct.getDescriptionData().setTitle(productFeedStatus.getProductNamePremium());
			premiumProduct.getDescriptionData().setDescription(productFeedStatus.getProductDescriptionPremium());
			premiumProduct.setActiveFlag("Y");
   		
			this.populateProductAttributes(premiumProduct, productAttributes);
    		productList.add(premiumProduct);
		} else if (productFeedStatus.isSentToAmazonPremiumFlag()) {
    		Product premiumProduct = new Product();
    		//this.populateProductBaseDetails(premiumProduct, productFeedStatus);
			premiumProduct.setSKU(feedDetailVO.getProductId()+AmazonConstants.CATALOG_SUFFIX_PREMIUM);
			premiumProduct.setActiveFlag("N");
   		productList.add(premiumProduct);
		}
		return productList;
	}
	
	private void populateProductAttributes(Product product, ProductAttributesVO attributes) 
	{
		DescriptionData descriptionData = product.getDescriptionData();
		if(descriptionData == null)
		{
			descriptionData = new DescriptionData();
			product.setDescriptionData(descriptionData);
		}		
		List<String> bulletPoints = attributes.getBulletPoint();
		for (String bulletPoint : bulletPoints) {
			descriptionData.getBulletPoint().add(bulletPoint);
		}
		List<String> otherItemAttributes = attributes.getOtherItemAttributes();
		for (String otherItemAttr : otherItemAttributes) {
			descriptionData.getOtherItemAttributes().add(otherItemAttr);
		}
		List<String> searchTerms = attributes.getSearchTerms();
		for (String searchTerm : searchTerms) {
			descriptionData.getSearchTerms().add(searchTerm);
		}
		List<String> subjectContent = attributes.getSubjectContent();
		for (String content : subjectContent) {
			descriptionData.getSubjectContent().add(content);
		}
		List<String> targetAudience = attributes.getTargetAudience();
		for (String audience : targetAudience) {
			descriptionData.getTargetAudience().add(audience);
		}
		List<String> usedFor = attributes.getUsedFor();
		for (String used4 : usedFor) {
			descriptionData.getUsedFor().add(used4);
		}
	}

	private void populateProductBaseDetails(Product product, ProductMasterVO productFeedStatus)
	{
		product.setProductTaxCode("A_GEN_TAX");
		
		product.setLaunchDate(AmazonUtils.toXMLGregorianCalendar(productFeedStatus.getCreatedOn()));
		
		ConditionInfo condition = new ConditionInfo();
		condition.setConditionType("New");
		product.setCondition(condition);
		
		ProductData productData = new ProductData();
		Miscellaneous miscellaneous = new Miscellaneous();
		miscellaneous.setProductType("Flowers");
		productData.setMiscellaneous(miscellaneous );
		
		product.setProductData(productData);
		
		DescriptionData descriptionData = new DescriptionData();
		descriptionData.setBrand("FTD");
		
		descriptionData.setMaxOrderQuantity(new BigInteger("1"));
		descriptionData.setManufacturer("FTD");
		descriptionData.setItemType(productFeedStatus.getItemType());
		
		descriptionData.setIsGiftMessageAvailable(true);
		descriptionData.setIsGiftWrapAvailable(false);
		
		product.setDescriptionData(descriptionData);
	}
}
