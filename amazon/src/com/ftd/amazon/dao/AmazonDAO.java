package com.ftd.amazon.dao;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.ftd.amazon.vo.AzOrderDetailVO;
import com.ftd.amazon.vo.SettlementDetailVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.StateMasterVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

public class AmazonDAO
{

  /**
   * Logger instance
   */
  private Logger logger = new Logger("com.ftd.amazon.dao.AmazonDAO");
  private Connection conn;

  /**
   * Default constructor
   */
  public AmazonDAO(Connection conn)
  {
    this.conn = conn;
  }

  public void insertAmazonOrder(String amazonOrderNumber, String masterOrderNumber, String reportId, String amazonOrderString, String orderStatus) throws Exception
  {

    logger.debug("insertAmazonOrder()");

    DataRequest request = new DataRequest();
    request.reset();
    request.setConnection(conn);
    HashMap inputParams = new HashMap();
    inputParams.put("IN_AZ_ORDER_NUMBER", amazonOrderNumber);
    inputParams.put("IN_MASTER_ORDER_NUMBER", masterOrderNumber);
    inputParams.put("IN_AZ_REPORT_ID", reportId);
    inputParams.put("IN_AZ_XML", amazonOrderString);
    inputParams.put("IN_FTD_XML", null);
    inputParams.put("IN_ORDER_STATUS", orderStatus);
    request.setInputParams(inputParams);
    request.setStatementID("INSERT_AZ_ORDER");

    DataAccessUtil dau = DataAccessUtil.getInstance();

    Map result = (Map) dau.execute(request);

    String status = (String) result.get("OUT_STATUS");
    logger.info("status: " + status);
    if (status == null || status.equalsIgnoreCase("N"))
    {
      String message = (String) result.get("OUT_MESSAGE");
      throw new Exception(message);
    }

  }

  public void insertAmazonOrderReport(String orderReportId, String orderReport) throws Exception
  {

    logger.debug("insertAmazonOrderReport()");

    HashMap inputParams = new HashMap();
    inputParams.put("IN_ORDER_REPORT_ID", orderReportId);
    inputParams.put("IN_ORDER_REPORT", orderReport);

    DataRequest request = new DataRequest();
    request.reset();
    request.setConnection(conn);
    request.setInputParams(inputParams);
    request.setStatementID("INSERT_AZ_ORDER_REPORT");

    DataAccessUtil dau = DataAccessUtil.getInstance();

    Map result = (Map) dau.execute(request);

    String status = (String) result.get("OUT_STATUS");
    logger.info("status: " + status);
    if (status == null || status.equalsIgnoreCase("N"))
    {
      String message = (String) result.get("OUT_MESSAGE");
      throw new Exception(message);
    }

  }

  public String getAmazonOrderXML(String amazonOrderNumber) throws Exception {
	  logger.debug("getAmazonOrderXML()");

      DataRequest request = new DataRequest();
      request.reset();
      request.setConnection(conn);
      HashMap inputParams = new HashMap();
      inputParams.put("IN_AZ_ORDER_NUMBER", amazonOrderNumber);        
      request.setInputParams(inputParams);
      request.setStatementID("GET_AZ_ORDER_XML");

      CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(request);
      String amazonXML = "";
      while ( results != null && results.next() ) 
      {
    	  Clob xmlCLOB = results.getClob("AZ_XML");
    	  amazonXML = xmlCLOB.getSubString(1L, (int)xmlCLOB.length());
      }
      logger.info("amazonXML: " + amazonXML);
      
      return amazonXML;
	}

  public void saveFTDOrderXml(String amazonOrderNumber, String ftd_xml, String orderStatusTransformed) throws Exception {
		logger.debug("saveFTDOrderXml()");
	
	    HashMap inputParams = new HashMap();
	    inputParams.put("IN_AZ_ORDER_NUMBER", amazonOrderNumber);
	    inputParams.put("IN_FTD_XML", ftd_xml);
	    inputParams.put("IN_ORDER_STATUS", orderStatusTransformed);
	
	    DataRequest request = new DataRequest();
	    request.reset();
	    request.setConnection(conn);
	    request.setInputParams(inputParams);
	    request.setStatementID("SAVE_FTD_ORDER_XML");
	
	    DataAccessUtil dau = DataAccessUtil.getInstance();
	
	    Map result = (Map) dau.execute(request);
	
	    String status = (String) result.get("OUT_STATUS");
	    logger.info("status: " + status);
	    if (status == null || status.equalsIgnoreCase("N"))
	    {
	      String message = (String) result.get("OUT_MESSAGE");
	      throw new Exception(message);
	    }
		
	}

  public void saveAmazonOrderDetailsInfo(AzOrderDetailVO azVO) throws Exception {
		logger.debug("saveAmazonOrderDetailsInfo()");
		
	    HashMap inputParams = new HashMap();
	    inputParams.put("IN_AZ_ORDER_ITEM_NUMBER", azVO.getAmazonItemNumber());
	    inputParams.put("IN_AZ_ORDER_NUMBER", azVO.getAmazonOrderNumber());
	    inputParams.put("IN_CONFIRMATION_NUMBER", azVO.getConfirmationNumber());
	    inputParams.put("IN_AMAZON_PRODUCT_ID", azVO.getAmazonProductId());
	    inputParams.put("IN_PRINCIPAL_AMT", azVO.getPrincipalAmount());
	    inputParams.put("IN_SHIPPING_AMT", azVO.getShippingAmount());
	    inputParams.put("IN_TAX_AMT", azVO.getTaxAmount());
	    inputParams.put("IN_SHIPPING_TAX_AMT", azVO.getShippingTaxAmount());
	
	    DataRequest request = new DataRequest();
	    request.reset();
	    request.setConnection(conn);
	    request.setInputParams(inputParams);
	    request.setStatementID("SAVE_AZ_ORDER_DETAILS");
	
	    DataAccessUtil dau = DataAccessUtil.getInstance();
	
	    Map result = (Map) dau.execute(request);
	
	    String status = (String) result.get("OUT_STATUS");
	    logger.info("AmazonDAO:saveAmazonOrderDetailsInfo status: " + status);
	    if (status == null || status.equalsIgnoreCase("N"))
	    {
	    	StringBuilder message = new StringBuilder("Unable to insert AmazonOrderDetails into PTN_AMAZON.AZ_ORDER_DETAIL, "); 
	    	message.append((String) result.get("OUT_MESSAGE"));
	    	throw new Exception(message.toString());
	    }
	}

	public String getConfirmationNumber() throws Exception {
			logger.debug("getConfirmationNumber()");

		  DataRequest request = new DataRequest();
	      request.reset();
	      request.setConnection(conn);
	      request.setStatementID("GET_ORDER_CONFIRMATION_NUMBER");

	      CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(request);
	      String confNumber = "";
	      while ( results != null && results.next() ) 
	      {
	    	  confNumber = results.getString("confirmation_number");
	      }
	      logger.info("confNumber: " + confNumber);
		return confNumber;
	}

	public void updateOrderStatus(String amazonOrderNumber, String orderStatus) throws Exception {
		logger.debug("updateOrderStatus()");
		
	    HashMap inputParams = new HashMap();
	    inputParams.put("IN_AZ_ORDER_NUMBER", amazonOrderNumber);
	    inputParams.put("IN_ORDER_STATUS", orderStatus);
	
	    DataRequest request = new DataRequest();
	    request.reset();
	    request.setConnection(conn);
	    request.setInputParams(inputParams);
	    request.setStatementID("UPDATE_AZ_ORDER_STATUS");
	
	    DataAccessUtil dau = DataAccessUtil.getInstance();
	
	    Map result = (Map) dau.execute(request);
	
	    String status = (String) result.get("OUT_STATUS");
	    logger.info("status: " + status);
	    if (status == null || status.equalsIgnoreCase("N"))
	    {
	      String message = (String) result.get("OUT_MESSAGE");
	      throw new Exception(message);
	    }
		
	}

	public Map getPdbPriceData(String sku) throws Exception {
		  logger.debug("getPdbPriceData()");

	      DataRequest request = new DataRequest();
	      request.reset();
	      request.setConnection(conn);
	      HashMap inputParams = new HashMap();
	      inputParams.put("IN_PRODUCT_ID", sku);        
	      request.setInputParams(inputParams);
	      request.setStatementID("GET_PDB_DATA");

	      CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(request);
	      HashMap pdbPriceMap = new HashMap();
	      while ( results != null && results.next() ) 
	      {
	    	  pdbPriceMap.put("standardPrice", results.getBigDecimal("STANDARD_PRICE"));
	    	  pdbPriceMap.put("deluxePrice", results.getBigDecimal("DELUXE_PRICE"));
	    	  pdbPriceMap.put("premiumPrice", results.getBigDecimal("PREMIUM_PRICE"));
	    	  pdbPriceMap.put("shipMethodFlorist", results.getString("SHIP_METHOD_FLORIST"));
	    	  pdbPriceMap.put("shipMethodCarrier", results.getString("SHIP_METHOD_CARRIER"));
	      }
	      
	      return pdbPriceMap;
	}

	//SettlementReport
	public void insertAmazonSettlementReport(String azReportId, String settlementReport) throws Exception {
    	
    	logger.debug("insertAmazonSettlementReport()");

        DataRequest request = new DataRequest();
        request.reset();
        request.setConnection(conn);
        HashMap inputParams = new HashMap();
        inputParams.put("IN_AZ_REPORT_ID", azReportId);
        inputParams.put("IN_REPORT", settlementReport);
        request.setInputParams(inputParams);
        request.setStatementID("INSERT_AZ_SETTLEMENT_REPORT");

        DataAccessUtil dau = DataAccessUtil.getInstance();

        Map result = (Map) dau.execute(request);
    	
	    String status = (String) result.get("OUT_STATUS");
	    logger.info("status: " + status);
	    if (status == null || status.equalsIgnoreCase("N"))
	    {
	      String message = (String) result.get("OUT_MESSAGE");
	      throw new Exception(message);
	    }
	}
	//SettlementReport

	public Map getIOTWSourceCode(String sourceCode, String itemSku, String dateReceived) throws Exception {
		logger.debug("getIOTWSourceCode()");

	      DataRequest request = new DataRequest();
	      request.reset();
	      request.setConnection(conn);
	      HashMap inputParams = new HashMap();
	      inputParams.put("IN_SOURCE_CODE", sourceCode);
	      inputParams.put("IN_PRODUCT_ID", itemSku);
	      inputParams.put("IN_TIME_RECEIVED", dateReceived);
	      request.setInputParams(inputParams);
	      request.setStatementID("GET_IOTW_SOURCE_CODE");

	      CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(request);
	      
	      HashMap iotwMap = new HashMap();
	      String iotwSourceCode = sourceCode;
	      BigDecimal discount = new BigDecimal("0");
	      String discountType = "";
	      while ( results != null && results.next() ) 
	      {
	    	  iotwSourceCode = results.getString("IOTW_SOURCE");
	    	  discount = results.getBigDecimal("DISCOUNT");
	    	  discountType = results.getString("DISCOUNT_TYPE");
	      }
	      iotwMap.put("iotwSourceCode", iotwSourceCode);
	      iotwMap.put("discount", discount);
	      iotwMap.put("discountType", discountType);
	      logger.info("iotwSourceCode: " + iotwSourceCode);
		return iotwMap;
	}

	  public String getAmazonSettlementXML(String settlementId) throws Exception {
		  logger.debug("getAmazonOrderXML()");

	      DataRequest request = new DataRequest();
	      request.reset();
	      request.setConnection(conn);
	      HashMap inputParams = new HashMap();
	      inputParams.put("IN_SETTLEMENT_REPORT_ID", settlementId);        
	      request.setInputParams(inputParams);
	      request.setStatementID("GET_AZ_SETTLEMENT_XML");

	      CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(request);
	      String amazonXML = "";
	      while ( results != null && results.next() ) 
	      {
	    	  Clob xmlCLOB = results.getClob("REPORT");
	    	  if (xmlCLOB != null) {
	    	      amazonXML = xmlCLOB.getSubString(1L, (int)xmlCLOB.length());
	    	  }
	      }
	      logger.info("amazonXML: " + amazonXML);
	      
	      return amazonXML;
		}

	  public void insertSettlementData(String settlementId, BigDecimal totalAmount,
			  Date startDate, Date endDate, Date depositDate) throws Exception {
		  logger.info("insertSettlementData()");

          DataRequest request = new DataRequest();
	      request.reset();
	      request.setConnection(conn);
	      HashMap inputParams = new HashMap();
	      inputParams.put("IN_SETTLEMENT_DATA_ID", settlementId);
	      inputParams.put("IN_TOTAL_AMT", totalAmount);
	      inputParams.put("IN_START_DATE", new java.sql.Date(startDate.getTime()));
	      inputParams.put("IN_END_DATE", new java.sql.Date(endDate.getTime()));
	      inputParams.put("IN_DEPOSIT_DATE", new java.sql.Date(depositDate.getTime()));
	      request.setInputParams(inputParams);
	      request.setStatementID("INSERT_SETTLEMENT_DATA");

	      DataAccessUtil dau = DataAccessUtil.getInstance();

	      Map result = (Map) dau.execute(request);
	    	
		  String status = (String) result.get("OUT_STATUS");
		  logger.info("status: " + status);
		  if (status == null || status.equalsIgnoreCase("N")) {
		      String message = (String) result.get("OUT_MESSAGE");
		      throw new Exception(message);
		  }

	  }
	  
	  public void insertSettlementDetail(SettlementDetailVO detailVO) throws Exception {
		  logger.info("insertSettlementDetail(" + 
				  detailVO.getAmazonOrderNumber() + " " +
				  detailVO.getAmazonItemNumber() + " " +
				  detailVO.getTransactionType() + " " +
				  detailVO.getTransactionName() + ")");
		  
          DataRequest request = new DataRequest();
	      request.reset();
	      request.setConnection(conn);
	      HashMap inputParams = new HashMap();
	      inputParams.put("IN_SETTLEMENT_DATA_ID", detailVO.getSettlementId());
	      inputParams.put("IN_AZ_ORDER_NUMBER", detailVO.getAmazonOrderNumber());
	      inputParams.put("IN_AZ_ORDER_ITEM_NUMBER", detailVO.getAmazonItemNumber());
	      inputParams.put("IN_TRANSACTION_TYPE", detailVO.getTransactionType());
	      inputParams.put("IN_TRANSACTION_NAME", detailVO.getTransactionName());
	      inputParams.put("IN_TRANSACTION_AMT", detailVO.getTransactionAmount());
	      inputParams.put("IN_POSTED_DATE", new java.sql.Date(detailVO.getPostedDate().getTime()));
	      request.setInputParams(inputParams);
	      request.setStatementID("INSERT_SETTLEMENT_DETAIL");

	      DataAccessUtil dau = DataAccessUtil.getInstance();

	      Map result = (Map) dau.execute(request);
	    	
		  String status = (String) result.get("OUT_STATUS");
		  logger.info("status: " + status);
		  if (status == null || status.equalsIgnoreCase("N")) {
		      String message = (String) result.get("OUT_MESSAGE");
		      throw new Exception(message);
		  }
		  
	  }
	
	/**
	 * This method calls the GET_AMAZON_BUYER_SEQUENCE stored procedure
	 * from PTN_AMAZON schema to get the sequence to update it to buyer address.
	 * @return	String	sequence number.
	 * @exception Exception
	 */
	public String getAmazonBuyerSequence() throws Exception {
		logger.debug("getAmazonBuyerSequence()");
		
		DataRequest request = new DataRequest();
		request.reset();
	    request.setConnection(conn);		
		HashMap inputParams = new HashMap();
		request.setInputParams(inputParams);
		request.setStatementID("GET_AMAZON_BUYER_SEQUENCE");
		DataAccessUtil dau = DataAccessUtil.getInstance();
        Map result = (Map) dau.execute(request);

		String sequence = (String) result.get("OUT_BUYER_SEQUENCE");
		String status = (String) result.get("OUT_STATUS");
	    logger.info(" ****BuyerSquence StoreProc Status: " + status);
	    if (status == null || status.equalsIgnoreCase("N"))
	    {
	    	String message = (String) result.get("OUT_MESSAGE");
	    	throw new Exception(message);
	    }					
		return sequence;
	}
	
	public void insertFloristFulfilledOrders() throws Exception {
		logger.debug("insertFloristFulfilledOrders()");

		DataRequest request = new DataRequest();
		request.reset();
		request.setConnection(conn);
		request.setStatementID("INSERT_FLORIST_FULFILLED_ORDERS");
		DataAccessUtil dau = DataAccessUtil.getInstance();
		Map result = (Map) dau.execute(request);

		String status = (String) result.get("OUT_STATUS");
		logger.info("status: " + status);
		if (status == null || status.equalsIgnoreCase("N"))
		{
			String message = (String) result.get("OUT_MESSAGE");
			throw new Exception(message);
		}
	}
	
	public Map getProductByTimestamp(String productId, Date priceDateTime) throws Exception {		
		
        // Query the historical product data.
		logger.debug("getProductByTimestamp()");

		DataRequest request = new DataRequest();
		request.setConnection(conn);
		request.setStatementID("GET_PRODUCT_DETAILS_BY_TIMESTAMP");
		request.addInputParam("IN_PRODUCT_ID", productId);
		request.addInputParam("IN_DATE_TIME", new java.sql.Timestamp(priceDateTime.getTime()));
        DataAccessUtil dau = DataAccessUtil.getInstance();
        CachedResultSet rs = (CachedResultSet)dau.execute(request);
        rs.reset();
        request.reset();
        HashMap productPriceMap = new HashMap();
        // Only one record (or none) should return.
        if (rs.next()) {
            // Record the historical standard price.
        	BigDecimal standardPrice = rs.getBigDecimal("standard_price");
        	productPriceMap.put("standardPrice", standardPrice);
            // Historical premium and deluxe prices.
        	BigDecimal deluxePrice = rs.getBigDecimal("deluxe_price");
        	productPriceMap.put("deluxePrice", deluxePrice);
        	BigDecimal premiumPrice = rs.getBigDecimal("premium_price");
        	productPriceMap.put("premiumPrice", premiumPrice);                        
        }
        
        return productPriceMap;
	}
	
	public StateMasterVO getStateByName(String state) throws Exception {
		logger.debug("getStateByName(" + state + ")");
		StateMasterVO stateVO = new StateMasterVO();

	    DataRequest request = new DataRequest();
	    request.reset();
	    request.setConnection(conn);
	    HashMap inputParams = new HashMap();
	    inputParams.put("IN_STATE_NAME", state);        
	    request.setInputParams(inputParams);
	    request.setStatementID("GET_STATE_BY_NAME");

	    CachedResultSet rs = (CachedResultSet)DataAccessUtil.getInstance().execute(request);
	    while (rs.next()) {
	    	stateVO = new StateMasterVO();
	    	stateVO.setStateMasterId(rs.getString("state_master_id"));
	    	stateVO.setStateName(rs.getString("state_name"));
	    	stateVO.setCountryCode(rs.getString("country_code"));
	    }

		return stateVO;
	}
}