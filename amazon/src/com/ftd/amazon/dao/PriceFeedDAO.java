/**
 * 
 */
package com.ftd.amazon.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import com.ftd.amazon.vo.PriceFeedDetailVO;
import com.ftd.amazon.vo.PriceFeedVO;
import com.ftd.amazon.vo.ProductMasterVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * @author sbring
 *
 */
public class PriceFeedDAO 
{
	private Logger logger = new Logger("com.ftd.amazon.dao.PriceFeedDAO");

	private Connection conn;
	public PriceFeedDAO(Connection conn) {
		this.conn = conn;
	}

	public PriceFeedVO getPriceFeedData() throws Exception{
		DataRequest dataRequest = new DataRequest();
		dataRequest.setConnection(this.conn);
		dataRequest.setStatementID("GET_NEW_PRICE_FEED_DATA");

		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
		CachedResultSet crs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
		PriceFeedVO feed = new PriceFeedVO();
		while(crs.next()){
			PriceFeedDetailVO priceFeedDetail = new PriceFeedDetailVO();
			priceFeedDetail.setPriceFeedId(crs.getString("AZ_PRICE_FEED_ID"));
			priceFeedDetail.setProductId(crs.getString("PRODUCT_ID"));
			priceFeedDetail.setStandardPrice(crs.getBigDecimal("STANDARD_PRICE"));
			priceFeedDetail.setDeluxePrice(crs.getBigDecimal("DELUXE_PRICE"));
			priceFeedDetail.setPremiumPrice(crs.getBigDecimal("PREMIUM_PRICE"));
			
			ProductMasterVO productFeedStatusVO = new ProductMasterVO();
			productFeedStatusVO.setProductId(crs.getString("PRODUCT_ID"));
			productFeedStatusVO.setCatalogStatusStandard(crs.getString("CATALOG_STATUS_STANDARD"));
			productFeedStatusVO.setSentToAmazonStandardFlag("Y".equalsIgnoreCase(crs.getString("SENT_TO_AMAZON_STANDARD_FLAG")));
			productFeedStatusVO.setCatalogStatusDeluxe(crs.getString("CATALOG_STATUS_DELUXE"));
			productFeedStatusVO.setSentToAmazonDeluxeFlag("Y".equalsIgnoreCase(crs.getString("SENT_TO_AMAZON_DELUXE_FLAG")));
			productFeedStatusVO.setCatalogStatusPremium(crs.getString("CATALOG_STATUS_PREMIUM"));
			productFeedStatusVO.setSentToAmazonPremiumFlag("Y".equalsIgnoreCase(crs.getString("SENT_TO_AMAZON_PREMIUM_FLAG")));

			priceFeedDetail.setProductFeedStatus(productFeedStatusVO);
			feed.addFeedDetail(priceFeedDetail);
		}
		return feed;
	}

	public void saveFeedStatus(PriceFeedDetailVO pvo) throws Exception {
		HashMap<String,Object> inputParams = new HashMap<String,Object>();
		inputParams.put("IN_AZ_PRICE_FEED_ID", pvo.getPriceFeedId());
		inputParams.put("IN_PRODUCT_ID", pvo.getProductId());
		inputParams.put("IN_FEED_STATUS", pvo.getFeedStatus());
		inputParams.put("IN_SERVER", pvo.getServer());
		inputParams.put("IN_FILENAME", pvo.getFilename());
		inputParams.put("IN_AZ_TRANSACTION_ID", pvo.getTransactionId());
		inputParams.put("IN_CREATED_BY", "_SYSTEM_");
		inputParams.put("IN_UPDATED_BY", "_SYSTEM_");

		DataRequest dataRequest = new DataRequest();
		dataRequest.setConnection(this.conn);
		dataRequest.setStatementID("SAVE_PRICE_FEED_STATUS");
		dataRequest.setInputParams(inputParams);

		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
		Map outputs = (Map) dataAccessUtil.execute(dataRequest);

		String status = (String) outputs.get("OUT_STATUS");
		if(status != null && status.equalsIgnoreCase("N"))
		{
			String message = (String) outputs.get("OUT_MESSAGE");
			throw new SQLException(message);
		}

	}

	public Map getPriceFeedSaleData(String sourceCode, String productId, BigDecimal productPrice) throws Exception {
		logger.info("getPriceFeedSaleData(" + sourceCode + ", " + productId + ", " +
				productPrice.toString() + ")");
		HashMap<String,Object> inputParams = new HashMap<String,Object>();
		inputParams.put("IN_SOURCE_CODE", sourceCode);
		inputParams.put("IN_PRODUCT_ID", productId);
		inputParams.put("IN_PRODUCT_PRICE", productPrice);

		DataRequest dataRequest = new DataRequest();
		dataRequest.setConnection(this.conn);
		dataRequest.setStatementID("GET_PRICE_FEED_SALE_DATA");
		dataRequest.setInputParams(inputParams);

		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
		CachedResultSet crs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
		HashMap priceSaleMap = new HashMap();
		double sourceDiscountAmt = 0.0;
		String sourceDiscountType = "";
		Date sourceStartDate = null;
		double iotwDiscountAmt = 0.0;
		String iotwDiscountType = "";
		Date iotwStartDate = null;
		Date iotwEndDate = null;
		while(crs != null && crs.next())
		{
			sourceDiscountAmt = crs.getDouble("source_discount_amt");
			sourceDiscountType = crs.getString("source_discount_type");
			sourceStartDate = crs.getDate("source_start_date");
			iotwDiscountAmt = crs.getDouble("iotw_discount_amt");
			iotwDiscountType = crs.getString("iotw_discount_type");
			iotwStartDate = crs.getDate("iotw_start_date");
			iotwEndDate = crs.getDate("iotw_end_date");
		}
		priceSaleMap.put("sourceDiscountAmt", sourceDiscountAmt);
		priceSaleMap.put("sourceDiscountType", sourceDiscountType);
		priceSaleMap.put("sourceStartDate", sourceStartDate);
		priceSaleMap.put("iotwDiscountAmt", iotwDiscountAmt);
		priceSaleMap.put("iotwDiscountType", iotwDiscountType);
		priceSaleMap.put("iotwStartDate", iotwStartDate);
		priceSaleMap.put("iotwEndDate", iotwEndDate);

		logger.debug("sourceDiscountType: " + sourceDiscountType);
		logger.debug("sourceDiscountAmt: " + sourceDiscountAmt);
		logger.debug("iotwDiscountType: " + iotwDiscountType);
		logger.debug("iotwDiscountAmt: " + iotwDiscountAmt);

		return priceSaleMap;
	}
}
