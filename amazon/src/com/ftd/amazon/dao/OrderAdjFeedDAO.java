/**
 * 
 */
package com.ftd.amazon.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ftd.amazon.vo.OrderAdjFeedVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * @author skatam
 *
 */
public class OrderAdjFeedDAO 
{
	private Logger logger = new Logger("com.ftd.amazon.dao.OrderAdjFeedDAO");
	
	private Connection conn;
	public OrderAdjFeedDAO(Connection conn) {
		this.conn = conn;
	}
		
	public List<OrderAdjFeedVO> getOrderAdjustmentFeedData(String feedStatus)  throws Exception
	{
		List<OrderAdjFeedVO> feedList = new ArrayList<OrderAdjFeedVO>();
		
	    DataRequest dataRequest = new DataRequest();
	    dataRequest.setConnection(this.conn);
	    dataRequest.setStatementID("GET_ORDER_ADJ_FEED_BY_STATUS");
	    
	    HashMap<String,Object> inputParams = new HashMap<String,Object>();
	    inputParams.put("IN_FEED_STATUS", feedStatus);
	    dataRequest.setInputParams(inputParams);
	      
	    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	    CachedResultSet crs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
	    while(crs.next())
	    {
	    	OrderAdjFeedVO feedDetail = new OrderAdjFeedVO();
	    	feedDetail.setFeedType("ORDER_ADJ_FEED");
	    	feedDetail.setFeedId(crs.getString("AZ_ORDER_ADJUSTMENT_ID"));
	    	feedDetail.setAzOrderAdjId(crs.getString("AZ_ORDER_ADJUSTMENT_ID"));	    	
	    	feedDetail.setAzOrderItemNumber(crs.getString("AZ_ORDER_ITEM_NUMBER"));
	    	feedDetail.setAzOrderID(crs.getString("AZ_ORDER_NUMBER"));
	    	feedDetail.setMasterOrderNumber(crs.getString("MASTER_ORDER_NUMBER"));
	    	feedDetail.setConfirmationNumber(crs.getString("CONFIRMATION_NUMBER"));
	    	
	    	feedDetail.setAdjustmentReason(crs.getString("ADJUSTMENT_REASON"));
	    	feedDetail.setPrincipalAmt(crs.getString("PRINCIPAL_AMT"));
	    	feedDetail.setShippingAmt(crs.getString("SHIPPING_AMT"));
	    	feedDetail.setTaxAmt(crs.getString("TAX_AMT"));
	    	feedDetail.setShippingTaxAmt(crs.getString("SHIPPING_TAX_AMT"));
	    	
	    	feedList.add(feedDetail);
	    }
	    return feedList;
		
	}
	
	public void saveFeedStatus(OrderAdjFeedVO vo) throws Exception
	{
		logger.debug("Saving FeedStatus. AZ_ORDER_ADJUSTMENT_ID=["+vo.getFeedId()+"], " +
				"TRANSACTION_ID=["+vo.getTransactionId()+"], FEED_STATUS=["+vo.getFeedStatus()+"]");
		
	      HashMap<String,Object> inputParams = new HashMap<String,Object>();
	      inputParams.put("IN_AZ_ORDER_ADJ_FEED_ID", vo.getAzOrderAdjId());
	      inputParams.put("IN_FEED_STATUS", vo.getFeedStatus());
	      inputParams.put("IN_SERVER", vo.getServer());
	      inputParams.put("IN_FILENAME", vo.getFilename());
	      inputParams.put("IN_AZ_TRANSACTION_ID", vo.getTransactionId());
	      inputParams.put("IN_CREATED_BY", "_SYSTEM_");
	      inputParams.put("IN_UPDATED_BY", "_SYSTEM_");
	      
	      DataRequest dataRequest = new DataRequest();
	      dataRequest.setConnection(this.conn);
	      dataRequest.setStatementID("SAVE_ORDER_ADJ_FEED_STATUS");
	      dataRequest.setInputParams(inputParams);

	      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	      Map outputs = (Map) dataAccessUtil.execute(dataRequest);
	      
	      String status = (String) outputs.get("OUT_STATUS");
	      if(status != null && status.equalsIgnoreCase("N"))
	      {
	        String message = (String) outputs.get("OUT_MESSAGE");
	        throw new SQLException(message);
	      }
	    
	}
	
}
