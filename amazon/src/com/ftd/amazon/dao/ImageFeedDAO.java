/**
 * 
 */
package com.ftd.amazon.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.ftd.amazon.vo.ImageFeedDetailVO;
import com.ftd.amazon.vo.ImageFeedVO;
import com.ftd.amazon.vo.ProductMasterVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * @author sbringu
 *
 */
public class ImageFeedDAO {

	private Logger logger = new Logger("com.ftd.amazon.dao.ImageFeedDAO");
	
	private Connection conn;
	public ImageFeedDAO(Connection conn) {
		this.conn = conn;
	}
		
	public ImageFeedVO getImageFeedData() throws Exception
	{
		logger.debug("********getImageFeedData() ******");
	    DataRequest dataRequest = new DataRequest();
	    dataRequest.setConnection(this.conn);
	    dataRequest.setStatementID("GET_NEW_IMAGE_FEED_DATA");
	    
	    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	    CachedResultSet crs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
	    ImageFeedVO feed = new ImageFeedVO();
	    while(crs.next())
	    {
	    	ImageFeedDetailVO imgFeedDetail = new ImageFeedDetailVO();
	    	imgFeedDetail.setImageFeedId(crs.getString("AZ_IMAGE_FEED_ID"));
	    	imgFeedDetail.setSku(crs.getString("PRODUCT_ID"));
	    	imgFeedDetail.setNovatorId(crs.getString("NOVATOR_ID"));
	    	//imgFeedDetail.setFeedStatus(crs.getString("FEED_STATUS"));
	    	//imgFeedDetail.setServer(crs.getString("SERVER"));
	    	//imgFeedDetail.setFilename(crs.getString("FILENAME"));
	    	ProductMasterVO productFeedStatusVO = new ProductMasterVO();
	    	productFeedStatusVO.setProductId(crs.getString("PRODUCT_ID"));
	    	productFeedStatusVO.setCatalogStatusStandard(crs.getString("CATALOG_STATUS_STANDARD"));
	    	productFeedStatusVO.setSentToAmazonStandardFlag("Y".equalsIgnoreCase(crs.getString("SENT_TO_AMAZON_STANDARD_FLAG")));
	    	productFeedStatusVO.setCatalogStatusDeluxe(crs.getString("CATALOG_STATUS_DELUXE"));
	    	productFeedStatusVO.setSentToAmazonDeluxeFlag("Y".equalsIgnoreCase(crs.getString("SENT_TO_AMAZON_DELUXE_FLAG")));
	    	productFeedStatusVO.setCatalogStatusPremium(crs.getString("CATALOG_STATUS_PREMIUM"));
	    	productFeedStatusVO.setSentToAmazonPremiumFlag("Y".equalsIgnoreCase(crs.getString("SENT_TO_AMAZON_PREMIUM_FLAG")));
	    	
	    	imgFeedDetail.setProductFeedStatus(productFeedStatusVO);
			feed.addFeedDetail(imgFeedDetail);
	    }
	    return feed;
	  
	}
	
	public void saveFeedStatus(ImageFeedDetailVO ivo) throws Exception {
	      HashMap<String,Object> inputParams = new HashMap<String,Object>();
	      inputParams.put("IN_AZ_IMAGE_FEED_ID", ivo.getImageFeedId());
	      inputParams.put("IN_PRODUCT_ID", ivo.getSku());
	      inputParams.put("IN_FEED_STATUS", ivo.getFeedStatus());
	      inputParams.put("IN_SERVER", ivo.getServer());
	      inputParams.put("IN_FILENAME", ivo.getFilename());
	      inputParams.put("IN_AZ_TRANSACTION_ID", ivo.getTransactionId());
	      inputParams.put("IN_CREATED_BY", "_SYSTEM_");
	      inputParams.put("IN_UPDATED_BY", "_SYSTEM_");
	      
	      DataRequest dataRequest = new DataRequest();
	      dataRequest.setConnection(this.conn);
	      dataRequest.setStatementID("SAVE_IMAGE_FEED_STATUS");
	      dataRequest.setInputParams(inputParams);

	      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	      Map outputs = (Map) dataAccessUtil.execute(dataRequest);
	      
	      String status = (String) outputs.get("OUT_STATUS");
	      if(status != null && status.equalsIgnoreCase("N"))
	      {
	        String message = (String) outputs.get("OUT_MESSAGE");
	        throw new SQLException(message);
	      }
		
	}
}
