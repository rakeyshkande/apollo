/**
 * 
 */
package com.ftd.amazon.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ftd.amazon.vo.BaseFeedDetailVO;
import com.ftd.amazon.vo.ProductAttributesVO;
import com.ftd.amazon.vo.ProductMasterVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * @author skatam
 *
 */
public class ProductFeedDAO 
{
	private Logger logger = new Logger("com.ftd.amazon.dao.ProductFeedDAO");
	
	private Connection conn;
	public ProductFeedDAO(Connection conn) {
		this.conn = conn;
	}
	
	public List<BaseFeedDetailVO> getProductMasterData(String feedStatus) throws Exception
	{
		List<BaseFeedDetailVO> feed = new ArrayList<BaseFeedDetailVO>();
		
	    DataRequest dataRequest = new DataRequest();
	    dataRequest.setConnection(this.conn);
	    dataRequest.setStatementID("GET_PRODUCT_FEED_BY_STATUS");
	    
	    HashMap<String,Object> inputParams = new HashMap<String,Object>();
	    inputParams.put("IN_FEED_STATUS", feedStatus);
	    dataRequest.setInputParams(inputParams);
	      
	    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	    CachedResultSet crs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
	    
	    while(crs.next())
	    {
	    	BaseFeedDetailVO feedDetail = new BaseFeedDetailVO();
	    	feedDetail.setFeedType("PRODUCT_FEED");
	    	feedDetail.setFeedId(crs.getString("AZ_PRODUCT_FEED_ID"));
	    	feedDetail.setProductId(crs.getString("PRODUCT_ID"));
	    	
	    	ProductMasterVO productFeedStatusVO = new ProductMasterVO();
	    	productFeedStatusVO.setProductId(crs.getString("PRODUCT_ID"));
	    	productFeedStatusVO.setCatalogStatusStandard(crs.getString("catalog_status_standard"));
	    	productFeedStatusVO.setSentToAmazonStandardFlag("Y".equalsIgnoreCase(crs.getString("sent_to_amazon_standard_FLAG")));
	    	productFeedStatusVO.setCatalogStatusDeluxe(crs.getString("catalog_status_deluxe"));
	    	productFeedStatusVO.setSentToAmazonDeluxeFlag("Y".equalsIgnoreCase(crs.getString("sent_to_amazon_deluxe_FLAG")));
	    	productFeedStatusVO.setCatalogStatusPremium(crs.getString("catalog_status_premium"));
	    	productFeedStatusVO.setSentToAmazonPremiumFlag("Y".equalsIgnoreCase(crs.getString("SENT_TO_AMAZON_PREMIUM_FLAG")));
	    	
	    	productFeedStatusVO.setProductNameStandard(crs.getString("PRODUCT_NAME_STANDARD"));
	    	productFeedStatusVO.setProductDescriptionStandard(crs.getString("PRODUCT_DESCRIPTION_STANDARD"));
	    	
	    	productFeedStatusVO.setProductNameDeluxe(crs.getString("PRODUCT_NAME_DELUXE"));
	    	productFeedStatusVO.setProductDescriptionDeluxe(crs.getString("PRODUCT_DESCRIPTION_DELUXE"));
	    	
	    	productFeedStatusVO.setProductNamePremium(crs.getString("PRODUCT_NAME_PREMIUM"));
	    	productFeedStatusVO.setProductDescriptionPremium(crs.getString("PRODUCT_DESCRIPTION_PREMIUM"));
	    	
	    	productFeedStatusVO.setCreatedOn(crs.getDate("CREATED_ON"));
	    	productFeedStatusVO.setProductMasterStatus(crs.getString("pm_status"));
	    	productFeedStatusVO.setItemType(crs.getString("ITEM_TYPE"));
	    	
	    	feedDetail.setProductFeedStatus(productFeedStatusVO);
			feed.add(feedDetail);
	    }
	    return feed;
	  
	}
	
	public ProductAttributesVO getProductAttributes(String productFeedId, String sku) throws Exception
	{
		logger.info("Get Attributes for ProductId :"+sku);
		ProductAttributesVO productAttributes = new ProductAttributesVO();
						
		DataRequest dataRequest = new DataRequest();
	    dataRequest.setConnection(this.conn);
	    dataRequest.setStatementID("GET_ATTRS_BY_PROD");
	    
	    HashMap<String,Object> inputParams = new HashMap<String,Object>();
	    inputParams.put("IN_PRODUCT_ID", sku);
	    inputParams.put("IN_AZ_PRODUCT_FEED_ID", productFeedId);
	    dataRequest.setInputParams(inputParams);
	    
	    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	    Map outMap = (Map) dataAccessUtil.execute(dataRequest);
	    
	    CachedResultSet BULLET_POINTS_CUR = (CachedResultSet) outMap.get("OUT_BULLET_POINTS_CUR");
	    CachedResultSet SEARCH_TERMS_CUR = (CachedResultSet) outMap.get("OUT_SEARCH_TERMS_CUR");
	    CachedResultSet INTENDED_USE_CUR = (CachedResultSet) outMap.get("OUT_INTENDED_USE_CUR");
	    CachedResultSet TARGET_AUDIENCE_CUR = (CachedResultSet) outMap.get("OUT_TARGET_AUDIENCE_CUR");
	    CachedResultSet OTHER_ATTRIBUTES_CUR = (CachedResultSet) outMap.get("OUT_OTHER_ATTRIBUTES_CUR");
	    CachedResultSet SUBJECT_MATTER_CUR = (CachedResultSet) outMap.get("OUT_SUBJECT_MATTER_CUR");
	    
	    while(BULLET_POINTS_CUR.next())
	    {
	    	productAttributes.getBulletPoint().add(BULLET_POINTS_CUR.getString("product_attribute_value"));
	    }
	    while(SEARCH_TERMS_CUR.next())
	    {
	    	productAttributes.getSearchTerms().add(SEARCH_TERMS_CUR.getString("product_attribute_value"));
	    }
	    while(INTENDED_USE_CUR.next())
	    {
	    	productAttributes.getUsedFor().add(INTENDED_USE_CUR.getString("product_attribute_value"));
	    }
	    while(TARGET_AUDIENCE_CUR.next())
	    {
	    	productAttributes.getTargetAudience().add(TARGET_AUDIENCE_CUR.getString("product_attribute_value"));
	    }
	    while(OTHER_ATTRIBUTES_CUR.next())
	    {
	    	productAttributes.getOtherItemAttributes().add(OTHER_ATTRIBUTES_CUR.getString("product_attribute_value"));
	    }
	    while(SUBJECT_MATTER_CUR.next())
	    {
	    	productAttributes.getSubjectContent().add(SUBJECT_MATTER_CUR.getString("product_attribute_value"));
	    }
	    
	    return productAttributes;
	}

	public void saveFeedStatus(BaseFeedDetailVO vo) throws Exception
	{
			logger.debug("Saving FeedStatus. PRODUCT_FEED_ID=["
					+vo.getFeedId()+
					"], TRANSACTION_ID=["+vo.getTransactionId()
					+"], FEED_STATUS=["+vo.getFeedStatus()+"]");
	      HashMap<String,Object> inputParams = new HashMap<String,Object>();
	      inputParams.put("IN_AZ_PRODUCT_FEED_ID", vo.getFeedId());
	      inputParams.put("IN_PRODUCT_ID", vo.getProductId());
	      inputParams.put("IN_FEED_STATUS", vo.getFeedStatus());
	      inputParams.put("IN_SERVER", vo.getServer());
	      inputParams.put("IN_FILENAME", vo.getFilename());
	      inputParams.put("IN_AZ_TRANSACTION_ID", vo.getTransactionId());
	      inputParams.put("IN_CREATED_BY", "_SYSTEM_");
	      inputParams.put("IN_UPDATED_BY", "_SYSTEM_");
	      
	      DataRequest dataRequest = new DataRequest();
	      dataRequest.setConnection(this.conn);
	      dataRequest.setStatementID("SAVE_PRODUCT_FEED_STATUS");
	      dataRequest.setInputParams(inputParams);

	      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	      Map outputs = (Map) dataAccessUtil.execute(dataRequest);
	      
	      String status = (String) outputs.get("OUT_STATUS");
	      if(status != null && status.equalsIgnoreCase("N"))
	      {
	        String message = (String) outputs.get("OUT_MESSAGE");
	        throw new SQLException(message);
	      }
	}

	public void updateProductPriceSentFlags(BaseFeedDetailVO vo) throws Exception 
	{
		logger.debug("updateProductPriceSentFlags. PRODUCT_ID=["+vo.getProductId()+"]");
		 HashMap<String,Object> inputParams = new HashMap<String,Object>();
	      inputParams.put("IN_PRODUCT_ID", vo.getProductId());
	      ProductMasterVO feedStatus = vo.getProductFeedStatus();
	      inputParams.put("IN_STANDARD_FLAG", feedStatus.isCatalogStatusStandardActive()?"Y":"N");
	      inputParams.put("IN_DELUXE_FLAG", feedStatus.isCatalogStatusDeluxeActive()?"Y":"N");
	      inputParams.put("IN_PREMIUM_FLAG", feedStatus.isCatalogStatusPremiumActive()?"Y":"N");
	      
	      DataRequest dataRequest = new DataRequest();
	      dataRequest.setConnection(this.conn);
	      dataRequest.setStatementID("UPDATE_PROD_FEED_SUBM_STATUS");
	      dataRequest.setInputParams(inputParams);

	      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	      Map outputs = (Map) dataAccessUtil.execute(dataRequest);
	      
	      String status = (String) outputs.get("OUT_STATUS");
	      if(status != null && status.equalsIgnoreCase("N"))
	      {
	        String message = (String) outputs.get("OUT_MESSAGE");
	        throw new SQLException(message);
	      }
	}
}
