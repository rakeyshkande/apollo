package com.ftd.amazon.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import com.ftd.amazon.vo.OrderFulfillmentFeedDetailVO;
import com.ftd.amazon.vo.OrderFulfillmentFeedVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * @author sbring
 *
 */
public class OrderFulfillmentFeedDAO {
	private Logger logger = new Logger("com.ftd.amazon.dao.OrderFulfillmentFeedDAO");

	private Connection conn;
	public OrderFulfillmentFeedDAO(Connection conn) {
		this.conn = conn;
	}

	public OrderFulfillmentFeedVO getOrderFulfillmentFeedData(String feedStatus) throws Exception{

		DataRequest dataRequest = new DataRequest();
		dataRequest.setConnection(this.conn);
		dataRequest.setStatementID("GET_NEW_ORDER_FULFILLMENT_FEED_DATA");
		
		HashMap<String,Object> inputParams = new HashMap<String,Object>();
	    inputParams.put("IN_FEED_STATUS", feedStatus);
	    dataRequest.setInputParams(inputParams);
		
		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
		CachedResultSet crs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
		OrderFulfillmentFeedVO feed = new OrderFulfillmentFeedVO();
		while(crs.next()){

			OrderFulfillmentFeedDetailVO feedDetail = new OrderFulfillmentFeedDetailVO();
			feedDetail.setOrderFulfillmentId(crs.getString("AZ_ORDER_FULFILLMENT_ID"));
			feedDetail.setAmazonOrderId(crs.getString("AZ_ORDER_NUMBER"));
			feedDetail.setOrderItemNumber(crs.getString("AZ_ORDER_ITEM_NUMBER"));
			feedDetail.setShippingMethod(crs.getString("SHIPPING_METHOD"));
			feedDetail.setCarrierName(crs.getString("CARRIER_NAME"));
			feedDetail.setTrackingNumber(crs.getString("TRACKING_NUMBER"));
			feedDetail.setFulfillmentDate((crs.getDate("FULFILLMENT_DATE")));
			feed.addFeedDetail(feedDetail);
		}
		return feed;
	}
	
	public void saveFeedStatus(OrderFulfillmentFeedDetailVO ofvo) throws Exception {
	      HashMap<String,Object> inputParams = new HashMap<String,Object>();
	      inputParams.put("IN_AZ_ORDER_FULFILLMENT_ID", ofvo.getOrderFulfillmentId());
	      inputParams.put("IN_FEED_STATUS", ofvo.getFeedStatus());
	      inputParams.put("IN_SERVER", ofvo.getServer());
	      inputParams.put("IN_FILENAME", ofvo.getFilename());
	      inputParams.put("IN_AZ_TRANSACTION_ID", ofvo.getTransactionId());
	      inputParams.put("IN_CREATED_BY", "_SYSTEM_");
	      inputParams.put("IN_UPDATED_BY", "_SYSTEM_");
	      
	      DataRequest dataRequest = new DataRequest();
	      dataRequest.setConnection(this.conn);
	      dataRequest.setStatementID("SAVE_ORDER_FULFILLMENT_FEED_STATUS");
	      dataRequest.setInputParams(inputParams);

	      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	      Map outputs = (Map) dataAccessUtil.execute(dataRequest);
	      
	      String status = (String) outputs.get("OUT_STATUS");
	      if(status != null && status.equalsIgnoreCase("N"))
	      {
	        String message = (String) outputs.get("OUT_MESSAGE");
	        throw new SQLException(message);
	      }
		
	}

}

