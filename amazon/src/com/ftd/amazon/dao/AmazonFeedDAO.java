/**
 * 
 */
package com.ftd.amazon.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ftd.amazon.vo.BaseFeedDetailVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * @author skatam
 *
 */
public class AmazonFeedDAO 
{
	private Logger logger = new Logger("com.ftd.amazon.dao.AmazonFeedDAO");
	
	private Connection conn;
	public AmazonFeedDAO(Connection conn) {
		this.conn = conn;
	}

	public void updateFeedSubmissionStatus(BaseFeedDetailVO vo)throws Exception
	{
		  logger.info("updateFeedSubmissionStatus(" + vo.getFeedType() + ", " + vo.getFeedStatus()+ ")");
	      HashMap<String,Object> inputParams = new HashMap<String,Object>();
	      inputParams.put("IN_FEED_TYPE", vo.getFeedType());
	      inputParams.put("IN_FEED_STATUS", vo.getFeedStatus());
	      inputParams.put("IN_SERVER", vo.getServer());
	      inputParams.put("IN_AZ_TRANSACTION_ID", vo.getTransactionId());
	      inputParams.put("IN_UPDATED_BY", "_SYSTEM_");
	      
	      DataRequest dataRequest = new DataRequest();
	      dataRequest.setConnection(this.conn);
	      dataRequest.setStatementID("UPDATE_FEED_SUBM_STATUS");
	      dataRequest.setInputParams(inputParams);

	      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	      Map outputs = (Map) dataAccessUtil.execute(dataRequest);
	      
	      String status = (String) outputs.get("OUT_STATUS");
	      if(status != null && status.equalsIgnoreCase("N"))
	      {
	        String message = (String) outputs.get("OUT_MESSAGE");
	        throw new SQLException(message);
	      }
	}
	
	public List<BaseFeedDetailVO> getFeedDataSentToAmazon() throws Exception
	{
	    DataRequest dataRequest = new DataRequest();
	    dataRequest.setConnection(this.conn);
	    dataRequest.setStatementID("GET_FEED_SENT_TO_AMAZON");
	    
	    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	    CachedResultSet crs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
	    List<BaseFeedDetailVO> feed = new ArrayList<BaseFeedDetailVO>();
	    while(crs.next())
	    {
	    	BaseFeedDetailVO feedDetail = new BaseFeedDetailVO();
	    	feedDetail.setFeedId(crs.getString("FEED_ID"));
	    	feedDetail.setTransactionId(crs.getString("AZ_TRANSACTION_ID"));
	    	feedDetail.setFeedType(crs.getString("FEED_TYPE"));
	    	feed.add(feedDetail);
	    }
	    return feed;
	  
	}
	

	public String getFeedFileNameByTxnId(String internalFeedType, String transactionId) throws Exception
	{
		  logger.info("getFeedFileNameByTxnId(" + internalFeedType + ", " + transactionId+ ")");
	      HashMap<String,Object> inputParams = new HashMap<String,Object>();
	      inputParams.put("IN_FEED_TYPE", internalFeedType);
	      inputParams.put("IN_AZ_TRANSACTION_ID", transactionId);
	      
	      DataRequest dataRequest = new DataRequest();
	      dataRequest.setConnection(this.conn);
	      dataRequest.setStatementID("GET_FEED_FILENAME_BY_TXN_ID");
	      dataRequest.setInputParams(inputParams);

	      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	      Map outputs = (Map) dataAccessUtil.execute(dataRequest);
	      
	      String status = (String) outputs.get("OUT_STATUS");
	      if(status != null && status.equalsIgnoreCase("N"))
	      {
	        String message = (String) outputs.get("OUT_MESSAGE");
	        throw new SQLException(message);
	      }
	      return  (String) outputs.get("OUT_FILENAME");
	}
	
	public void processRefunds() throws Exception
	{
		logger.debug("Entered RefundFeedDAO");
		DataRequest dataRequest = new DataRequest();
		dataRequest.setConnection(this.conn);
		HashMap<String,Object> inputParams = new HashMap<String,Object>();
	    inputParams.put("IN_USER_NAME", "SYS");
		dataRequest.setStatementID("PROCESS_AMAZON_REFUNDS");
		dataRequest.setInputParams(inputParams);
		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
		Map outputs = (Map) dataAccessUtil.execute(dataRequest);
		  
		String status = (String) outputs.get("OUT_STATUS");
		if(status != null && status.equalsIgnoreCase("N"))
		{
			String message = (String) outputs.get("OUT_MESSAGE");
		    throw new SQLException(message);
		}
		logger.debug("End RefundFeedDAO");
	}

	public void checkPDBExceptionDates() throws Exception
	{
		logger.debug("checkPDBExceptionDates()");
		DataRequest dataRequest = new DataRequest();
		dataRequest.setConnection(this.conn);
		dataRequest.setStatementID("CHECK_PDB_EXCEPTION_DATES");
		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
		Map outputs = (Map) dataAccessUtil.execute(dataRequest);
		  
		String status = (String) outputs.get("OUT_STATUS");
		if(status != null && status.equalsIgnoreCase("N"))
		{
			String message = (String) outputs.get("OUT_MESSAGE");
		    throw new SQLException(message);
		}
		logger.debug("Finished");
	}

}
