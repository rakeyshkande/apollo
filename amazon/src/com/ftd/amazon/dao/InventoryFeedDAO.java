/**
 * 
 */
package com.ftd.amazon.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.ftd.amazon.vo.InventoryFeedDetailVO;
import com.ftd.amazon.vo.InventoryFeedVO;
import com.ftd.amazon.vo.ProductMasterVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * @author skatam
 *
 */
public class InventoryFeedDAO {

	private Logger logger = new Logger("com.ftd.amazon.dao.InventoryFeedDAO");
	
	private Connection conn;
	public InventoryFeedDAO(Connection conn) {
		this.conn = conn;
	}
		
	public InventoryFeedVO getInventoryFeedData(String feedStatus) throws Exception
	{
	    DataRequest dataRequest = new DataRequest();
	    dataRequest.setConnection(this.conn);
	    dataRequest.setStatementID("GET_INVENTORY_FEED_BY_STATUS");
	    
	    HashMap<String,Object> inputParams = new HashMap<String,Object>();
	    inputParams.put("IN_FEED_STATUS", feedStatus);
	    dataRequest.setInputParams(inputParams);
	      
	    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	    CachedResultSet crs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
	    InventoryFeedVO feed = new InventoryFeedVO();
	    while(crs.next())
	    {
	    	InventoryFeedDetailVO feedDetail = new InventoryFeedDetailVO();
	    	feedDetail.setInventoryFeedId(crs.getString("AZ_INVENTORY_FEED_ID"));
	    	feedDetail.setProductId(crs.getString("PRODUCT_ID"));
	    	//feedDetail.setFeedStatus(crs.getString("FEED_STATUS"));
	    	//feedDetail.setServer(crs.getString("SERVER"));
	    	//feedDetail.setFilename(crs.getString("FILENAME"));
	    	ProductMasterVO productFeedStatusVO = new ProductMasterVO();
	    	productFeedStatusVO.setProductId(crs.getString("PRODUCT_ID"));
	    	productFeedStatusVO.setCatalogStatusStandard(crs.getString("catalog_status_standard"));
	    	productFeedStatusVO.setSentToAmazonStandardFlag("Y".equalsIgnoreCase(crs.getString("sent_to_amazon_standard_FLAG")));
	    	productFeedStatusVO.setCatalogStatusDeluxe(crs.getString("catalog_status_deluxe"));
	    	productFeedStatusVO.setSentToAmazonDeluxeFlag("Y".equalsIgnoreCase(crs.getString("sent_to_amazon_deluxe_FLAG")));
	    	productFeedStatusVO.setCatalogStatusPremium(crs.getString("catalog_status_premium"));
	    	productFeedStatusVO.setSentToAmazonPremiumFlag("Y".equalsIgnoreCase(crs.getString("SENT_TO_AMAZON_PREMIUM_FLAG")));
	    	productFeedStatusVO.setProductMasterStatus(crs.getString("pm_status"));
	    	
	    	feedDetail.setProductFeedStatus(productFeedStatusVO);
			feed.addFeedDetail(feedDetail);
	    }
	    return feed;
	  
	}
	
	public void saveFeedStatus(InventoryFeedDetailVO vo) throws Exception
	{
		logger.debug("Saving FeedStatus. INVENTORY_FEED_ID=["
				+vo.getInventoryFeedId()+
				"], TRANSACTION_ID=["+vo.getTransactionId()
				+"], FEED_STATUS=["+vo.getFeedStatus()+"]");
	      HashMap<String,Object> inputParams = new HashMap<String,Object>();
	      inputParams.put("IN_AZ_INVENTORY_FEED_ID", vo.getInventoryFeedId());
	      inputParams.put("IN_PRODUCT_ID", vo.getProductId());
	      inputParams.put("IN_FEED_STATUS", vo.getFeedStatus());
	      inputParams.put("IN_SERVER", vo.getServer());
	      inputParams.put("IN_FILENAME", vo.getFilename());
	      inputParams.put("IN_AZ_TRANSACTION_ID", vo.getTransactionId());
	      inputParams.put("IN_CREATED_BY", "_SYSTEM_");
	      inputParams.put("IN_UPDATED_BY", "_SYSTEM_");
	      
	      DataRequest dataRequest = new DataRequest();
	      dataRequest.setConnection(this.conn);
	      dataRequest.setStatementID("SAVE_INVENTORY_FEED_STATUS");
	      dataRequest.setInputParams(inputParams);

	      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	      Map outputs = (Map) dataAccessUtil.execute(dataRequest);
	      
	      String status = (String) outputs.get("OUT_STATUS");
	      if(status != null && status.equalsIgnoreCase("N"))
	      {
	        String message = (String) outputs.get("OUT_MESSAGE");
	        throw new SQLException(message);
	      }
	    
	}
}
