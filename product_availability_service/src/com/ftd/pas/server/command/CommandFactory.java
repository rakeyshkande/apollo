package com.ftd.pas.server.command;

import com.ftd.pas.server.exception.InvalidPASCommandArgumentException;
import com.ftd.pas.server.exception.InvalidPASCommandException;

import java.sql.Connection;

import java.util.Date;
import java.util.List;

/**
 * Factory for the creation of commands.  The factory will take 
 * the name of the command and create the appropriate command class.
 */
public class CommandFactory
{
    /**
     * Basic factory method for creating a Command object.  This method is used from an input
     * that is purely text based.  The command is responsible for parsing the commandArguments
     * into something useful.
     * 
     * @param con Database connection
     * @param commandName Name of the command.  Must be defined in the Command interface
     * @param commandArguments Command arguments, varies by command
     * @return
     * @throws InvalidPASCommandException
     * @throws InvalidPASCommandArgumentException
     */
    public static Command createCommand(Connection con, String commandName, List<String> commandArguments)
            throws InvalidPASCommandException, InvalidPASCommandArgumentException
    {
        AbstractCommand command = createCommandByName(commandName, con);
        
        command.setArguments(commandArguments);
        
        return command;
    }

    /**
     * Basic factory method for creating a Command object.  This method is used from an input
     * that is purely text based.  The command is responsible for parsing the commandArguments
     * into something useful.
     * 
     * @param con Database connection
     * @param commandName Name of the command.  Must be defined in the Command interface
     * @param commandArguments Command arguments, varies by command
     * @param performInvalidation command should perform invalidation
     * @return
     * @throws InvalidPASCommandException
     * @throws InvalidPASCommandArgumentException
     */
    public static Command createCommand(Connection con, String commandName, List<String> commandArguments, boolean performInvalidation)
            throws InvalidPASCommandException, InvalidPASCommandArgumentException
    {
        AbstractCommand command = createCommandByName(commandName, con);
        
        command.setArguments(commandArguments);
        command.setPerformInvalidation(performInvalidation);
        
        return command;
    }
    
    /**
     * Factory method for creating a command with the delivery date and a single parameters.  This is
     * generally called from within a Command to execute another Command.
     * @param con Database connection
     * @param commandName Name of the command
     * @param deliveryDate Delivery Date for the command
     * @param param Single parameter for the command
     * @return
     * @throws InvalidPASCommandException
     * @throws InvalidPASCommandArgumentException
     */
    public static Command createCommand(Connection con, String commandName, Date deliveryDate, String param)
            throws InvalidPASCommandException, InvalidPASCommandArgumentException
    {
        AbstractCommand command = createCommandByName(commandName, con);
        
        command.setArguments(deliveryDate, param);
        
        return command;
        
    }

    /**
     * Factory method for creating a command with the delivery date and a single object parameters.  This is
     * generally called from within a Command to execute another Command.
     * @param con Database connection
     * @param commandName Name of the command
     * @param deliveryDate Delivery Date for the command
     * @param param Single Object parameter for the command
     * @return
     * @throws InvalidPASCommandException
     * @throws InvalidPASCommandArgumentException
     */
    public static Command createCommand(Connection con, String commandName, Date deliveryDate, Object param)
            throws InvalidPASCommandException, InvalidPASCommandArgumentException
    {
        AbstractCommand command = createCommandByName(commandName, con);
        
        command.setArguments(deliveryDate, param);
        
        return command;
        
    }
    
    /**
     * Create the appropriate Command Object based on the command name.
     * @param commandName
     * @return
     * @throws InvalidPASCommandException
     */
    protected static AbstractCommand createCommandByName(String commandName, Connection con)
            throws InvalidPASCommandException
    {
        AbstractCommand command = null; 
        
        if (commandName != null)
        {
            if (commandName.equals(Command.COMMAND_ADD_COUNTRY_DATE))
            {
                command = new AddCountryDateCommand();        
            }
            else if (commandName.equals(Command.COMMAND_ADD_DELIVERY_DATE))
            {
                command = new AddDeliveryDateCommand();        
            }
            else if (commandName.equals(Command.COMMAND_ADD_PRODUCT_DATE))
            {
                command = new AddProductDateCommand();        
            }
            else if (commandName.equals(Command.COMMAND_ADD_PRODUCT_STATE_DATE))
            {
                command = new AddProductStateDateCommand();        
            }
            else if (commandName.equals(Command.COMMAND_ADD_VENDOR_DATE))
            {
                command = new AddVendorDateCommand();        
            }
            else if (commandName.equals(Command.COMMAND_ADD_FLORIST_ZIPCODE_DATE))
            {
                command = new AddFloristZipcodeDateCommand();
            }
            else if (commandName.equals(Command.COMMAND_ADD_COUNTRY_PRODUCT_DATE))
            {
                command = new AddCountryProductDateCommand();        
            }
            else if (commandName.equals(Command.COMMAND_ADD_TIME_ZONE_DATE))
            {
                command = new AddTimeZoneDateCommand();        
            }
            else if (commandName.equals(Command.COMMAND_ADD_VENDOR_PRODUCT_STATE_DATE))
            {
                command = new AddVendorProductStateDateCommand();        
            }
            else if (commandName.equals(Command.COMMAND_ADD_NEXT_DELIVERY_DATE))
            {
                command = new AddNextDeliveryDateCommand();        
            }
            else if (commandName.equals(Command.COMMAND_REMOVE_DELIVERY_DATE))
            {
                command = new RemoveDeliveryDateCommand();        
            }
            else if (commandName.equals(Command.COMMAND_MODIFIED_COUNTRY))
            {
                command = new ModifiedCountryCommand();        
            }
            else if (commandName.equals(Command.COMMAND_MODIFIED_COUNTRY_PRODUCT))
            {
                command = new ModifiedCountryProductCommand();        
            }
            else if (commandName.equals(Command.COMMAND_MODIFIED_PRODUCT))
            {
                command = new ModifiedProductCommand();        
            }
            else if (commandName.equals(Command.COMMAND_MODIFIED_PRODUCT_STATE))
            {
                command = new ModifiedProductStateCommand();        
            }
            else if (commandName.equals(Command.COMMAND_MODIFIED_VENDOR))
            {
                command = new ModifiedVendorCommand();        
            }
            else if (commandName.equals(Command.COMMAND_MODIFIED_VENDOR_PRODUCT))
            {
                command = new ModifiedVendorProductCommand();        
            }
            else if (commandName.equals(Command.COMMAND_MODIFIED_CUTOFF_SATURDAY))
            {
                command = new ModifiedCutoffSaturdayCommand();        
            }
            else if (commandName.equals(Command.COMMAND_MODIFIED_CUTOFF_SUNDAY))
            {
                command = new ModifiedCutoffSundayCommand();        
            }
            else if (commandName.equals(Command.COMMAND_MODIFIED_CUTOFF_EXOTIC))
            {
                command = new ModifiedCutoffExoticCommand();        
            }
            else if (commandName.equals(Command.COMMAND_MODIFIED_CUTOFF_LATEST))
            {
                command = new ModifiedCutoffLatestCommand();        
            }
            else if (commandName.equals(Command.COMMAND_MODIFIED_CUTOFF_LASTEST_VENDOR))
            {
                command = new ModifiedCutoffLastestVendorCommand();        
            }
            else if (commandName.equals(Command.COMMAND_MODIFIED_FLORIST))
            {
                command = new ModifiedFloristCommand();        
            }
            else if (commandName.equals(Command.COMMAND_MODIFIED_STATE))
            {
                command = new ModifiedStateCommand();        
            }
            else if (commandName.equals(Command.COMMAND_RECALC))
            {
                command = new RecalcCommand();        
            }
            else if (commandName.equals((Command.COMMAND_RECALC_FLORIST_ZIP))) {
                command = new RecalcFloristZipCommand();
            }
            else if (commandName.equals(Command.COMMAND_MODIFIED_FLORIST_ZIP))
            {
                command = new ModifiedFloristZipCommand();
            }
            else if (commandName.equals(Command.COMMAND_RECALC_TIME_ZONES)) {
            	command = new RecalcTimeZonesCommand();
            }
        }
        
        if (command != null)
        {
            command.setConnection(con);
            command.setCommandName(commandName);
        }
        else
        {
            throw new InvalidPASCommandException("Invalid Command " + commandName);
        }
        
        return command;
        
    }
}
