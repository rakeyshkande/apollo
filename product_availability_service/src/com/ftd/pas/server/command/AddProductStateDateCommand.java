package com.ftd.pas.server.command;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pas.server.exception.InvalidPASCommandArgumentException;
import com.ftd.pas.server.vo.PASProductStateVO;
import com.ftd.pas.server.vo.ProductStateVO;

import java.text.ParseException;

import java.util.Calendar;
import java.util.Date;
import java.util.List;


/**
 * Command for processing the adding of a new product state exclusion for a delivery date to the
 * product availability.  All of the processing needed to add a new date is performed.
 */
public class AddProductStateDateCommand extends AbstractLoopingCommand
{
    protected Logger logger = new Logger(this.getClass().getName());

    protected String productId;
    protected String stateId;

    /**
     * Create a new AddProductStateDateCommand object.
     */
    public AddProductStateDateCommand()
    {
        super();
    }

    /**
     * Set the list of arguments.
     * expected is:
     * DeliveryDate in MMddYYYY format
     * ProductId 
     * StateId
     * @param arguments
     * @throws InvalidPASCommandArgumentException if the arguments are not correct.
     */
    public void setArguments(List<String> arguments) throws InvalidPASCommandArgumentException

    {
        super.setArguments(arguments);

        if (arguments.size() != 3)
        {
            throw new InvalidPASCommandArgumentException("Wrong number of Arguments, need 3 got " + arguments.size());
        }

        // Should have first argument, a delivery Date.
        String deliveryDateString = arguments.get(0);
        try
        {
            setDeliveryDate(deliveryDateString);
        } catch (ParseException pe)
        {
            logger.error("Invalid Delivery Date " + deliveryDateString, pe);
            throw new InvalidPASCommandArgumentException("Invalid Delivery Date");
        }
        if (arguments.get(1).equals(ALL))
        {
            productId = null;
        }
        else
        {
            productId = arguments.get(1);
        }
        if (arguments.get(2).equals(ALL))
        {
            stateId = null;
        }
        else
        {
            stateId = arguments.get(2);
        }
    }

    /**
     * Set the arguments for the command.
     * @param deliveryDate
     * @param productId
     * @param stateId
     * @throws InvalidPASCommandArgumentException
     */
    public void setArguments(Date deliveryDate, String productId, String stateId) throws InvalidPASCommandArgumentException
    {
        setDeliveryDate(deliveryDate);
        this.productId = productId;
        this.stateId = stateId;
    }

    /**
     * Set the arguments for the command.
     * @param deliveryDate
     * @param productStateVO Must be a ProductStateVO or exception is thrown
     * @throws InvalidPASCommandArgumentException
     */
    public void setArguments(Date deliveryDate, Object productStateVO) throws InvalidPASCommandArgumentException
    {
        setDeliveryDate(deliveryDate);

        if (productStateVO instanceof ProductStateVO)
        {
            this.paramObject = productStateVO;
        } 
        else
        {
            throw new InvalidPASCommandArgumentException("Expecting ProductStateVO, got " + productStateVO.getClass().getName());
        }

    }


    /**
     * Get the list of country products to process.
     * @return
     * @throws Exception
     */
    protected List getObjects() throws Exception
    {
        if (isPerformInvalidation())
        {
            getMaintDAO().invalidatePASProductState(getConnection(),deliveryDate,productId, stateId);
        }

        List<ProductStateVO> details = getQueryDAO().getProductStateExclusions(getConnection(), productId, stateId);
        return details;        
    }

    /**
     * Process a singly Product State combination, insert into PAS_PRODUCT_STATE_DT.
     * @param object
     * @throws Exception
     */
    protected void executeSingleUnit(Object object) throws Exception
    {
        ProductStateVO productStateVO = (ProductStateVO) object;
        logger.debug("Processing " + productStateVO.getProductId() + " " + productStateVO.getStateId());
        
        // Build up the PASProductVO
        PASProductStateVO productState = new PASProductStateVO();
        productState.setProductId(productStateVO.getProductId());
        productState.setStateCode(productStateVO.getStateId());
        productState.setDeliveryDate(deliveryDate);

        // Check the day of the week for the product.  
        int dayOfWeek = deliveryDateCalendar.get(Calendar.DAY_OF_WEEK);
        String productDayOfWeekFlag = NO;
        switch (dayOfWeek)
        {
        case Calendar.SUNDAY:
            productDayOfWeekFlag = productStateVO.getSunday();
            break;
        case Calendar.MONDAY:
            productDayOfWeekFlag = productStateVO.getMonday();
            break;
        case Calendar.TUESDAY:
            productDayOfWeekFlag = productStateVO.getTuesday();
            break;
        case Calendar.WEDNESDAY:
            productDayOfWeekFlag = productStateVO.getWednesday();
            break;
        case Calendar.THURSDAY:
            productDayOfWeekFlag = productStateVO.getThursday();
            break;
        case Calendar.FRIDAY:
            productDayOfWeekFlag = productStateVO.getFriday();
            break;
        case Calendar.SATURDAY:
            productDayOfWeekFlag = productStateVO.getSaturday();
            break;
        }
        if (productDayOfWeekFlag != null)
        {
            // Switch the flag around
            if (productDayOfWeekFlag.equals(YES))
            {
                productDayOfWeekFlag = NO;
            }
            else
            {
                productDayOfWeekFlag = YES;
            }
            productState.setDeliveryAvailable(productDayOfWeekFlag);
        }

        getMaintDAO().insertPASProductState(getConnection(), productState);
    }
}
