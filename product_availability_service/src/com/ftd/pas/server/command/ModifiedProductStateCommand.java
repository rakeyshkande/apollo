package com.ftd.pas.server.command;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pas.server.exception.InvalidPASCommandArgumentException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Command for processing the modification to a product state.  This could
 * be triggered in multiple different methods.  This will reset just the 
 * data for the product state.  This command will cascade updates to the vendor
 * product state table.
 */
public class ModifiedProductStateCommand extends AbstractDateLoopingCommand
{
    protected Logger logger = new Logger(this.getClass().getName());

    protected String productId;
    protected String stateCode;

    /**
     * Create a new object.
     */
    public ModifiedProductStateCommand()
    {
        super();
    }

    /**
     * Set the list of arguments.
     * expected is:
     * productId 
     * stateCode
     * @param arguments
     * @throws InvalidPASCommandArgumentException if the arguments are not correct.
     */
    public void setArguments(List<String> arguments) throws InvalidPASCommandArgumentException

    {
        super.setArguments(arguments);

        if (arguments.size() != 2)
        {
            throw new InvalidPASCommandArgumentException("Wrong number of Arguments, need 2 got " + arguments.size());
        }

        // one argument, a product.
        productId = arguments.get(0);
        stateCode = arguments.get(1);

    }

    /**
     * Execute the command.  Just update the base product data.
     */
    protected void executeSingleUnit(Date processDate) throws Exception
    {

        logger.debug("Processing " + processDate);
        addProductState(processDate, productId, stateCode);

        addVendorProductState(processDate, ALL, productId, stateCode, true);
    }


}
