package com.ftd.pas.server.command;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pas.server.exception.InvalidPASCommandArgumentException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Command for processing the modification to country products
 * in product_index or product_index_xref.  Loops through all the dates in
 * product availability and changes each.
 */
public class ModifiedCountryProductCommand extends AbstractDateLoopingCommand
{
    protected Logger logger = new Logger(this.getClass().getName());

    protected String countryId;
    protected String productId;

    /**
     * Create a new object.
     */
    public ModifiedCountryProductCommand()
    {
        super();
    }

    /**
     * Set the list of arguments.
     * expected is:
     * countryId 
     * productId 
     * @param arguments
     * @throws InvalidPASCommandArgumentException if the arguments are not correct.
     */
    public void setArguments(List<String> arguments) throws InvalidPASCommandArgumentException

    {
        super.setArguments(arguments);

        if (arguments.size() != 2)
        {
            throw new InvalidPASCommandArgumentException("Wrong number of Arguments, need 1 got " + arguments.size());
        }

        // Should have one argument, a delivery Date.
        countryId = arguments.get(0);
        productId = arguments.get(1);

    }

    /**
     * Execute the command.  Since the country has changed it could affect availability
     * for all products in the country.  Need to do a mass update of all the country
     * products for all dates.
     */
    protected void executeSingleUnit(Date processDate) throws Exception
    {
        logger.debug("execute called");

        logger.debug("Processing " + processDate);
        invalidateCountryProduct(processDate);
        addCountryProduct(processDate, countryId, productId);

    }

    /**
     * Make all the data for the specified country and product invalid 
     * for the purpose of PAS.
     * @param processDate
     * @throws Exception
     */
    protected void invalidateCountryProduct(Date processDate) throws Exception
    {
        getMaintDAO().invalidatePASCountryProduct(getConnection(),processDate, countryId, productId);
    }
}
