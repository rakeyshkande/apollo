package com.ftd.pas.server.command;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pas.server.exception.InvalidPASCommandArgumentException;

import com.ftd.pas.server.vo.ProductDetailVO;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Command for processing the modification to the exotic cutoff.
 * Loops through all the dates in
 * product availability and changes each.
 */
public class ModifiedCutoffExoticCommand extends AbstractDateLoopingCommand
{
    protected Logger logger = new Logger(this.getClass().getName());

    protected List<ProductDetailVO> allExoticProducts;
    
    /**
     * Create a new object.
     */
    public ModifiedCutoffExoticCommand()
    {
        super();
    }

    /**
     * Set the list of arguments.
     * expected is:
     * None 
     * @param arguments
     * @throws InvalidPASCommandArgumentException if the arguments are not correct.
     */
    public void setArguments(List<String> arguments) throws InvalidPASCommandArgumentException

    {
        super.setArguments(arguments);

        if (arguments.size() != 0)
        {
            throw new InvalidPASCommandArgumentException("Wrong number of Arguments, need 0 got " + arguments.size());
        }


    }

    /**
     * Override the method to force an update of the cache before perfomring the looping.
     * @throws Exception
     */
    protected void executeCommand() throws Exception
    {
        refreshGlobalParmCache();
        super.executeCommand();
    }

    /**
     * Execute the command.  Since the exotic cutoff has changed, change all the products
     * that are exotic
     */
    protected void executeSingleUnit(Date processDate) throws Exception
    {
        logger.debug("Processing " + processDate);

        List<ProductDetailVO> exoticProducts = getExoticProducts();
        addProducts(processDate, exoticProducts);

    }

    /**
     * Add all of the exotic products for recalc.
     * @throws Exception
     */
    protected void addProducts(Date processDate, List<ProductDetailVO> exoticProducts) throws Exception
    {
        for (int i=0; i < exoticProducts.size();i++)
        {
            String productId = exoticProducts.get(i).getProductId();
            addProduct(processDate,productId);
            addVendorProductState(processDate, ALL, productId, ALL, false);
        }
    }

    /**
     * Lazily initialize the list of exotic products.
     * @return
     */
    protected List<ProductDetailVO> getExoticProducts() throws Exception
    {
        if (allExoticProducts == null)
        {
            allExoticProducts = getQueryDAO().getActiveExoticProducts(getConnection());
        }
        return allExoticProducts;
    }
}
