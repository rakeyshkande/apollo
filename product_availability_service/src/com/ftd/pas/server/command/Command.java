package com.ftd.pas.server.command;

public interface Command
{
    public final static String COMMAND_ADD_COUNTRY_DATE                = "ADD_COUNTRY_DATE";
    public final static String COMMAND_ADD_COUNTRY_PRODUCT_DATE        = "ADD_COUNTRY_PRODUCT_DATE";
    public final static String COMMAND_ADD_DELIVERY_DATE               = "ADD_DELIVERY_DATE";
    public final static String COMMAND_ADD_FLORIST_DATE                = "ADD_FLORIST_DATE";
    public final static String COMMAND_ADD_NEXT_DELIVERY_DATE          = "ADD_NEXT_DELIVERY_DATE";
    public final static String COMMAND_ADD_PRODUCT_DATE                = "ADD_PRODUCT_DATE";
    public final static String COMMAND_ADD_PRODUCT_STATE_DATE          = "ADD_PRODUCT_STATE_DATE";
    public final static String COMMAND_ADD_TIME_ZONE_DATE              = "ADD_TIME_ZONE_DATE";
    public final static String COMMAND_ADD_VENDOR_DATE                 = "ADD_VENDOR_DATE";
    public final static String COMMAND_ADD_VENDOR_PRODUCT_STATE_DATE   = "ADD_VENDOR_PRODUCT_STATE_DATE";
    public final static String COMMAND_MODIFIED_COUNTRY                = "MODIFIED_COUNTRY";
    public final static String COMMAND_MODIFIED_COUNTRY_PRODUCT        = "MODIFIED_COUNTRY_PRODUCT";
    public final static String COMMAND_MODIFIED_CUTOFF_EXOTIC          = "MODIFIED_CUTOFF_EXOTIC";
    public final static String COMMAND_MODIFIED_CUTOFF_LASTEST_VENDOR  = "MODIFIED_CUTOFF_LASTEST_VENDOR";
    public final static String COMMAND_MODIFIED_CUTOFF_LATEST          = "MODIFIED_CUTOFF_LATEST";
    public final static String COMMAND_MODIFIED_CUTOFF_SATURDAY        = "MODIFIED_CUTOFF_SATURDAY";
    public final static String COMMAND_MODIFIED_CUTOFF_SUNDAY          = "MODIFIED_CUTOFF_SUNDAY";
    public final static String COMMAND_MODIFIED_FLORIST                = "MODIFIED_FLORIST";
    public final static String COMMAND_MODIFIED_PRODUCT                = "MODIFIED_PRODUCT";
    public final static String COMMAND_MODIFIED_PRODUCT_STATE          = "MODIFIED_PRODUCT_STATE";
    public final static String COMMAND_MODIFIED_STATE                  = "MODIFIED_STATE";
    public final static String COMMAND_MODIFIED_VENDOR                 = "MODIFIED_VENDOR";
    public final static String COMMAND_MODIFIED_VENDOR_PRODUCT         = "MODIFIED_VENDOR_PRODUCT";
    public final static String COMMAND_RECALC                          = "RECALC";
    public final static String COMMAND_REMOVE_DELIVERY_DATE            = "REMOVE_DELIVERY_DATE";
    public final static String COMMAND_ADD_FLORIST_ZIPCODE_DATE        = "ADD_FLORIST_ZIPCODE_DATE";
    public final static String COMMAND_RECALC_FLORIST_ZIP              = "RECALC_FLORIST_ZIP";
    public final static String COMMAND_MODIFIED_FLORIST_ZIP            = "MODIFIED_FLORIST_ZIP";
    public final static String COMMAND_RECALC_TIME_ZONES               = "RECALC_TIME_ZONES";
    
    public final static String YES = "Y";
    public final static String NO  = "N";
    public final static String ALL = "ALL";

    public final static String PROCESS_INVALIDATION = "PROCESS_INVALIDATION";
    
    public void execute() throws Exception;
}
