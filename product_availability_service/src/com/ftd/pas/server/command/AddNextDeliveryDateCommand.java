package com.ftd.pas.server.command;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pas.server.exception.InvalidPASCommandArgumentException;

import com.ftd.pas.server.vo.PASCountryVO;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Command for processing the adding of a new date to the
 * product availability and removing old dates.  All of the processing needed
 * to add a new date is performed and remove an old one.
 */
public class AddNextDeliveryDateCommand extends AbstractCommand
{
    protected Logger logger = new Logger(this.getClass().getName());

    /**
     * Create a new AddNextDeliveyrDateCommand object.
     */
    public AddNextDeliveryDateCommand()
    {
        super();
    }

    /**
     * Set the list of arguments.
     * expected is:
     * None
     * @param arguments
     * @throws InvalidPASCommandArgumentException if the arguments are not correct.
     */
    public void setArguments(List<String> arguments) throws InvalidPASCommandArgumentException

    {
        super.setArguments(arguments);

        if (arguments.size() != 0)
        {
            throw new InvalidPASCommandArgumentException("Wrong number of Arguments, need 0 got " + arguments.size());
        }

    }

    /**
     * Execute the command.  Get the number of days that are supposed to be stored in PAS.
     * AddDeliveryDateCommand for each date not already processed.
     */
    protected void executeCommand() throws Exception
    {
        logger.debug("execute called");
        
        addNewDays();
        removeOldDays();
    }
    
    protected void addNewDays() throws Exception
    {

        int maxDays = getMaxDays();
        
        Calendar lastDay = Calendar.getInstance();
        lastDay.add(Calendar.DAY_OF_YEAR,maxDays);
        
        List<Date> processDays = getDaysToProcess(lastDay);

        for (int i=processDays.size() - 1; i >= 0; i--)
        {
            addDeliveryDate(processDays.get(i));
        }

    }
    
    protected void removeOldDays() throws Exception
    {
        int maxDaysBack = getMaxDaysBack() * -1;
        
        Calendar lastDay = Calendar.getInstance();
        lastDay.add(Calendar.DAY_OF_YEAR, maxDaysBack);
        
        List<Date> processDays = getDaysToRemove(lastDay);

        for (int i=processDays.size() - 1; i >= 0; i--)
        {
            removeDeliveryDate(processDays.get(i));
        }
    }

    /**
     * Add a delivery date to PAS.
     * @throws Exception
     */
    protected void addDeliveryDate(Date deliveryDate) throws Exception
    {
        // process the days forward
        List<String> args = new ArrayList<String>();
        String deliveryDateString = getSDF().format(deliveryDate);
        args.add(deliveryDateString);
        Command command = CommandFactory.createCommand(getConnection(), COMMAND_ADD_DELIVERY_DATE, args);
        logger.info("Adding Delivery Date for " + deliveryDateString);
        command.execute();
    }
    
    /**
     * Remove the data for a delivery date from PAS.
     * @param deliveryDate
     * @throws Exception
     */
    protected void removeDeliveryDate(Date deliveryDate) throws Exception
    {
        // Process the days back
        List<String> args = new ArrayList<String>();
        String deliveryDateString = getSDF().format(deliveryDate);
        args.add(deliveryDateString);
        Command command = CommandFactory.createCommand(getConnection(), COMMAND_REMOVE_DELIVERY_DATE, args);
        logger.info("Removing Delivery Date for " + deliveryDateString);
        command.execute();
    }
    
    

    /**
     * Figure out the days to process.  The PAS data is checked to determine all the
     * days that are not populated from the lastDay backwards.
     * 
     * @param lastDay
     * @return
     */
    protected List<Date> getDaysToProcess(Calendar lastDay) throws Exception
    {
        Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DAY_OF_YEAR, -1);
        boolean hasData = false;
        List<Date> datesToProcess = new ArrayList<Date>();
        
        do
        {
            hasData = isProductAvailabilityDataAvailable(lastDay);
            
            if (!hasData)
            {
                datesToProcess.add(lastDay.getTime());
            }
            lastDay.add(Calendar.DAY_OF_YEAR,-1);
            
        } while (lastDay.after(yesterday) && !hasData);
        
        return datesToProcess;
    }

    /**
     * Figure out the days to process.  The PAS data is checked to determine all the
     * days that are populated from the lastDay backwards.
     * 
     * @param lastDay
     * @return
     */
    protected List<Date> getDaysToRemove(Calendar lastDay) throws Exception
    {
        boolean hasData = false;
        List<Date> datesToProcess = new ArrayList<Date>();
        
        do
        {
            logger.debug("Remove date = " + lastDay.getTime());
            hasData = isProductAvailabilityDataAvailable(lastDay);
            logger.debug("hasData = " + hasData);            
            if (hasData)
            {
                datesToProcess.add(lastDay.getTime());
            }
            lastDay.add(Calendar.DAY_OF_YEAR,-1);
            
        } while (hasData);
        
        return datesToProcess;
    }
    
    /**
     * Check to see if there is data for the day in product availability.
     * This is done by looking for country data for the day for US.
     * @param day
     * @return
     */
    protected boolean isProductAvailabilityDataAvailable(Calendar day) throws Exception
    {
        PASCountryVO vo = getQueryDAO().getPASCountry(getConnection(),"US",day.getTime());
        
        if (vo != null)
        {
            return true;
        }
        else
        {
            return false;
        }
        
    }
}
