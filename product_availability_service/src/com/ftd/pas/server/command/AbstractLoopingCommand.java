package com.ftd.pas.server.command;

import com.ftd.pas.server.exception.PASCommandException;
import com.ftd.pas.server.util.SystemMessage;

import java.util.List;

/**
 * Base class for a command that loops through a group of objects to 
 * perform commands on.  Errors are trapped and handled on the processing
 * of a single object.  
 */
public abstract class AbstractLoopingCommand extends AbstractCommand
{
    private int  errorCounter = 0;
    private int  maxErrors    = 100;
    

    /**
     * Abstract method used for getting the list of objects to loop on.
     * @return
     * @throws Exception
     */
    protected abstract List getObjects() throws Exception;

    /**
     * Execute the command.
     * 
     * Get the list of details from the base class and loop through
     * them, executing a unit of work at a time
     */
    protected void executeCommand() throws Exception
    {
        //Check to see what we have been sent
        if (paramObject != null)
        {
            executeSingle(paramObject);
        }
        else
        {
            List details = getObjects();
            for (int i=0; i < details.size(); i++)
            {
                Object vo = details.get(i);
                executeSingle(vo);
            }
        }
    }

    /**
     * Process a single unit of work for the command.  These will be grouped
     * to allow for limiting of system messaging and errors.
     * @param param
     * @throws Exception
     */
    protected abstract void executeSingleUnit(Object param) throws Exception;

    /**
     * The purpose of this method is to handle exceptions that happen on a
     * unit of work within a command.  The exception will be captured and a
     * system message written.  The number of exceptions is counted and after
     * a set number of exceptions this method will escalate the exception out 
     * out of the method.
     * @param param
     * @throws Exception
     */
    protected void executeSingle(Object param) throws Exception
    {
        try
        {
            executeSingleUnit(param);
        }
        catch (Exception e)
        {
            errorCounter++;
            logger.error("Error processing command ",e);
            SystemMessage.sendSystemMessage(getConnection(),"PAS Command Failed",e.getMessage());
            
            if (errorCounter > getMaxErrorsAllowed())
            {
                throw new PASCommandException("PAS Command exceeed the number of max errors, " + getMaxErrorsAllowed());
            }
        }
        
    }
    
    /**
     * Get the maximum number of errors allowed for this command before the 
     * errors will terminate the execution of the whole command.  This method
     * can be overridden by the extending class.
     * @return
     */
    protected int getMaxErrorsAllowed()
    {
        return maxErrors;
    }

}
