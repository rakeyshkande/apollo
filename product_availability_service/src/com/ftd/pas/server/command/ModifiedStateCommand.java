package com.ftd.pas.server.command;

import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pas.server.exception.InvalidPASCommandArgumentException;

import java.util.Date;
import java.util.List;

/**
 * Command for processing the modification to a state.  This could
 * be triggered in multiple different methods.  This will reset the 
 * data for the product state.  This command will cascade updates to the vendor
 * product state table.
 */
public class ModifiedStateCommand extends AbstractDateLoopingCommand
{
    protected Logger logger = new Logger(this.getClass().getName());

    protected String stateCode;

    /**
     * Create a new object.
     */
    public ModifiedStateCommand()
    {
        super();
    }

    /**
     * Set the list of arguments.
     * expected is:
     * stateCode
     * @param arguments
     * @throws InvalidPASCommandArgumentException if the arguments are not correct.
     */
    public void setArguments(List<String> arguments) throws InvalidPASCommandArgumentException

    {
        super.setArguments(arguments);

        if (arguments.size() != 1)
        {
            throw new InvalidPASCommandArgumentException("Wrong number of Arguments, need 1 got " + arguments.size());
        }

        // one argument, a product.
        stateCode = arguments.get(0);

    }

    /**
     * Override the method to force an update of the cache before perfomring the looping.
     * @throws Exception
     */
    protected void executeCommand() throws Exception
    {
        CacheManager.getInstance().getHandler("CACHE_NAME_STATE_MASTER", true);
        super.executeCommand();
    }

    /**
     * Execute the command.  Just update the base product data.
     */
    protected void executeSingleUnit(Date processDate) throws Exception
    {

        logger.debug("Processing " + processDate);
        addProductState(processDate, ALL, stateCode);

        addVendorProductState(processDate, ALL, ALL, stateCode, true);
    }


}
