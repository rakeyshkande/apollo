package com.ftd.pas.server.command;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pas.server.exception.InvalidPASCommandArgumentException;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Command for processing the modification to a vendor.  This could
 * be triggered in multiple different methods.  This will reset all of the 
 * data for the vendor.
 */
public class ModifiedVendorCommand extends AbstractDateLoopingCommand
{
    protected Logger logger = new Logger(this.getClass().getName());

    protected String vendorId;

    /**
     * Create a new object.
     */
    public ModifiedVendorCommand()
    {
        super();
    }

    /**
     * Set the list of arguments.
     * expected is:
     * vendorId 
     * @param arguments
     * @throws InvalidPASCommandArgumentException if the arguments are not correct.
     */
    public void setArguments(List<String> arguments) throws InvalidPASCommandArgumentException

    {
        super.setArguments(arguments);

        if (arguments.size() < 1 || arguments.size() > 2)
        {
            throw new InvalidPASCommandArgumentException("Wrong number of Arguments, need 1 or 2 got " + arguments.size());
        }

        // Should have one argument, a vendor.
        vendorId = arguments.get(0);

        if (arguments.size() == 2) {
            String deliveryDateString = arguments.get(1);
            logger.info("deliveryDate: " + deliveryDateString);
            try {
                setDeliveryDate(deliveryDateString);
            } catch (ParseException pe) {
                logger.error("Invalid Delivery Date " + deliveryDateString, pe);
                throw new InvalidPASCommandArgumentException("Invalid Delivery Date");
            }
        }
    }

    protected void executeCommand() throws Exception
    {
    	if (deliveryDate == null) {
            int maxDays = getMaxDays();

            Calendar processDateCalendar = Calendar.getInstance();
            for (int i=0; i <= maxDays; i++) {
                executeSingle(processDateCalendar.getTime());
                processDateCalendar.add(Calendar.DAY_OF_YEAR,1);
            }
        } else {
    		executeSingle(deliveryDate);
    	}
   }
   
    /**
     * Execute the command.  Since the vendor has changed it could affect availability
     * for all products for the vendor.  Need to do a mass update of all the vendor
     * products for all dates.
     */
    protected void executeSingleUnit(Date processDate) throws Exception
    {
        logger.debug("execute called");

        addVendor(processDate, vendorId);
        addVendorProductState(processDate, vendorId, ALL, ALL, true);

    }

}
