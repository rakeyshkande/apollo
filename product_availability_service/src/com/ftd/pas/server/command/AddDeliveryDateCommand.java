package com.ftd.pas.server.command;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pas.server.exception.InvalidPASCommandArgumentException;

import java.text.ParseException;

import java.util.List;


/**
 * Command for processing the adding of a new date to the
 * product availability.  All of the processing needed
 * to add a new date is performed.
 */
public class AddDeliveryDateCommand extends AbstractCommand
{
    protected Logger logger = new Logger(this.getClass().getName());
    
    /**
     * Create a new AddDeliveyrDateCommand object.
     */
    public AddDeliveryDateCommand()
    {
        super();
    }

    /**
     * Set the list of arguments.
     * expected is:
     *     DeliveryDate in MMddYYYY format
     * @param arguments
    * @throws InvalidPASCommandArgumentException if the arguments are not correct.
     */
    public void setArguments(List<String> arguments)
            throws InvalidPASCommandArgumentException
    
    {
        super.setArguments(arguments);

        if (arguments.size() != 1)
        {
            throw new InvalidPASCommandArgumentException("Wrong number of Arguments, need 1 got " + arguments.size());
        }
        
        // Should have one argument, a delivery Date.
        String deliveryDateString = arguments.get(0);
        try
        {
            deliveryDate = getSDF().parse(deliveryDateString);
        }
        catch (ParseException pe)
        {
            logger.error("Invalid Delivery Date " + deliveryDateString, pe);
            throw new InvalidPASCommandArgumentException("Invalid Delivery Date");
        }
    }
    
    /**
     * Execute the command.
     * Foreach country
     *    AddCountryCommand
     *
     * Foreach product
     *    AddProductCommand
     *
     * Foreach Florist
     *    FloristActivateCommand
     *
     * Foreach Vendor
     *    VendorActivateCommand
     *    
     * Foreach Country Not Domestic
     *    AddIntCountryCommand
     */
    protected void executeCommand() throws Exception
    {
        logger.debug("execute called");

        addTimeZone(deliveryDate);
        
        addProduct(deliveryDate, ALL);
        
        addProductState(deliveryDate, ALL, ALL);

        addFloristZipCode(deliveryDate);
        
        addVendor(deliveryDate, ALL);
        
        addCountry(deliveryDate, ALL);
        
        // Add all the products for a country
        addCountryProduct(deliveryDate, ALL, ALL);
        
        addVendorProductState(deliveryDate, ALL, ALL, ALL, false);
    }
    
    

}
