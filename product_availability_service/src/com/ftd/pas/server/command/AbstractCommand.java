package com.ftd.pas.server.command;

import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.CountryMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.StateMasterHandler;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.pas.server.constants.PASConstants;
import com.ftd.pas.server.dao.PASMaintDAO;
import com.ftd.pas.server.dao.PASQueryDAO;
import com.ftd.pas.server.exception.InvalidPASCommandArgumentException;

import java.sql.Connection;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;

/**
 * Abstract base class for Commands.  Contains common methods,
 * logic and variables.
 * 
 */
public abstract class AbstractCommand implements Command
{
    protected Logger logger = new Logger(this.getClass().getName());
    protected List<String> commandArguments;
    protected String       commandName;
    
    protected Date         deliveryDate;
    protected Calendar     deliveryDateCalendar;
    
    private SimpleDateFormat sdf;
    
    private Connection con;
    
    private PASQueryDAO queryDAO;
    private PASMaintDAO maintDAO;
    private ConfigurationUtil cu;
    
    protected Object paramObject = null;
    
    protected boolean performInvalidation = false;
    
    public AbstractCommand()
    {
    }
    
    /**
     * Execute the command.  This method is for performing the real command processing.
     * @throws Exception
     */
    protected abstract void executeCommand() throws Exception;
    
    /**
     * Get the SDF object for parsing dates.  This is lazily initialized.
     * @return
     */
    protected SimpleDateFormat getSDF()
    {
        if (sdf == null)
        {
            sdf = new SimpleDateFormat("MMddyyyy");
        }
        return sdf;
    }
    
    /**
     * Set the database connection.
     * @param con
     */
    public void setConnection(Connection con)
    {
        this.con = con;
    }

    /**
     * Set the list of arguments for the command.
     * @param commandArguments
     */
    public void setArguments(List<String> commandArguments)
            throws InvalidPASCommandArgumentException
    {
        this.commandArguments = commandArguments;
    }

    /**
     * Set the command arguments.  This must be overridded by the
     * extending class or it will throw an exception.
     * @param deliveryDate
     * @param param
     */
    public void setArguments(Date deliveryDate, String param)
            throws InvalidPASCommandArgumentException
    {
        // Subclass must implement this to be valid.
        throw new InvalidPASCommandArgumentException("Invalid argument combination (Date,String) for this command");
    }
    
    /**
     * Set the name for the command.  This is for ease of use for
     * the base classes.
     * @param commandName
     */
    public void setCommandName(String commandName)
    {
        this.commandName = commandName;
    }
    
    /**
     * Set the command arguments.  This must be overridden by the extending class or it
     * will throw and exception.
     * @param deliveryDate
     * @param param
     * @throws InvalidPASCommandArgumentException
     */
    public void setArguments(Date deliveryDate, Object param)
            throws InvalidPASCommandArgumentException
    {
        // Subclass must implement this to be valid
        throw new InvalidPASCommandArgumentException("Invalid argument combination (Date,Object) for this command");
    }
    
    /**
     * Get the Country Handler for the Country cache.
     * @return
     */
    protected CountryMasterHandler getCountryHandler()
    {
        CountryMasterHandler countryHandler = (CountryMasterHandler) CacheManager.getInstance().getHandler("CACHE_NAME_COUNTRY_MASTER");
        return countryHandler;
    }

    /**
     * Get the State Handler for the State cache.
     * @return
     */
    protected StateMasterHandler getStatesHandler()
    {
        StateMasterHandler stateHandler = (StateMasterHandler) CacheManager.getInstance().getHandler("CACHE_NAME_STATE_MASTER");
        return stateHandler;
    }
    
    /**
     * Get the PAS Query DAO object.  This object is lazily initialized.
     * @return
     */
    protected PASQueryDAO getQueryDAO()
    {
        if (queryDAO == null)
        {
            queryDAO = new PASQueryDAO();
        }
        return queryDAO;
    }

    /**
     * Get the PAS Maint DAO object.  This object is lazily initialized.
     * @return
     */
    protected PASMaintDAO getMaintDAO()
    {
        if (maintDAO == null)
        {
            maintDAO = new PASMaintDAO();
        }
        return maintDAO;
    }
    
    /**
     * Get the active database Connection.
     * @return
     */
    protected Connection getConnection()
    {
        return con;
    }
    
    /**
     * Set the deliveryDate for the command.  The string is parsed and the Date and Calendar
     * deliveryDate objects are set.
     * @param deliveryDateString
     * @throws ParseException
     */
    protected void setDeliveryDate(String deliveryDateString) throws ParseException
    {
        this.deliveryDate = getSDF().parse(deliveryDateString);
        deliveryDateCalendar = Calendar.getInstance();
        deliveryDateCalendar.setTime(deliveryDate);
    }

    /**
     * Set the deliveryDate for the command.  The date and Calendar date objects are set
     * @param deliveryDate
     */
    protected void setDeliveryDate(Date deliveryDate) 
    {
        this.deliveryDate = deliveryDate;
        deliveryDateCalendar = Calendar.getInstance();
        deliveryDateCalendar.setTime(deliveryDate);
    }
    
    /**
     * Get the configurationUtil instance.  This is lazily initialized.
     * @return
     * @throws Exception
     */
    protected ConfigurationUtil getConfigurationUtil() throws Exception
    {
        if (cu == null)
        {
            cu = ConfigurationUtil.getInstance();
        }
        return cu;
    }
    
    /**
     * Wrapper around the executeCommand method.
     * @throws Exception
     */
    public void execute() throws Exception
    {
        Date startTime = new Date();
        executeCommand();
        Date endTime = new Date();
        
        long difference = endTime.getTime() - startTime.getTime();
        logger.info("Command " + commandName + " took " + difference + " milliseconds to execute");
    }
    
    /**
     * Get the number of days of maximum data to keep for PAS.
     * @return
     * @throws Exception
     */
    protected int getMaxDays() throws Exception
    {
        int maxDays = 0;
        String maxDaysString = getConfigurationUtil().getFrpGlobalParm(PASConstants.PAS_CONFIG_CONTEXT,PASConstants.MAX_DAYS_PARM);
        if (maxDaysString != null)
        {
            try
            {
                maxDays = Integer.valueOf(maxDaysString);
            }
            catch (NumberFormatException e)
            {
                maxDays = 0;
            }
        }
        if (maxDays == 0)
        {
            logger.error("Error getting the max days for PAS, using 120");
            maxDays = 120;
        }
        return maxDays;
    }
    
    /**
     * Get the number of days of back to keep data for PAS.
     * @return
     * @throws Exception
     */
    protected int getMaxDaysBack() throws Exception
    {
        int maxDays = 0;
        String maxDaysString = getConfigurationUtil().getFrpGlobalParm(PASConstants.PAS_CONFIG_CONTEXT,PASConstants.MAX_DAYS_BACK_PARM);
        if (maxDaysString != null)
        {
            try
            {
                maxDays = Integer.valueOf(maxDaysString);
            }
            catch (NumberFormatException e)
            {
                maxDays = 0;
            }
        }
        if (maxDays == 0)
        {
            logger.error("Error getting the max days for PAS, using 120");
            maxDays = 120;
        }
        return maxDays;
    }
    
    protected void spawnNewJMSCommand(String commandName, List<String> args, int priority) throws Exception
    {
        StringBuilder sb = new StringBuilder();
        sb.append(commandName);
        for (int i=0; i < args.size(); i++)
        {
            sb.append(" ");
            sb.append(args.get(i));
        }
        logger.debug("Inserting JMS message with payload " + sb.toString());

        sendJMSMessage(sb.toString(),commandName, PASConstants.PAS_COMMAND_QUEUE, priority);        
    }
    
    protected void sendJMSMessage(String payload, String correlationId, String jmsQueueName, int priority) throws Exception
    {
        MessageToken token = new MessageToken();
        Context context = new InitialContext();
        token.setMessage(payload);
        token.setStatus(jmsQueueName);
        token.setJMSCorrelationID(correlationId);
        token.setJMSPriority(priority);
        Dispatcher dispatcher = Dispatcher.getInstance();
        dispatcher.dispatchTextMessage(context, token);
    }

    protected void setPerformInvalidation(boolean param)
    {
        this.performInvalidation = param;
    }

    protected boolean isPerformInvalidation()
    {
        return this.performInvalidation;
    }
    
    protected void refreshGlobalParmCache() throws Exception
    {
        CacheManager.getInstance().getHandler("CACHE_NAME_GLOBAL_PARM", true);
    }

    /**
     * Add the products for the delivery date to PAS.
     * @throws Exception
     */
    protected void addProduct(Date processDate, String productId) throws Exception
    {
        List<String> args = new ArrayList<String>();
        String deliveryDateString = getSDF().format(processDate);
        args.add(deliveryDateString);
        args.add(productId);
        Command command = CommandFactory.createCommand(getConnection(), COMMAND_ADD_PRODUCT_DATE, args, true);
        command.execute();
    }


    /**
     * Add the product state exclusions for the delivery date to PAS.
     * @throws Exception
     */
    protected void addProductState(Date processDate, String productId, String stateCode) throws Exception
    {
        List<String> args = new ArrayList<String>();
        String deliveryDateString = getSDF().format(processDate);
        args.add(deliveryDateString);
        args.add(productId);
        args.add(stateCode);
        Command command = CommandFactory.createCommand(getConnection(), COMMAND_ADD_PRODUCT_STATE_DATE, args, true);
        command.execute();
    }

    /**
     * Add all of the vendors for the delivery date to PAS.  These are
     * all spawned as separate JMS messages to improve the performance.
     * @throws Exception
     */
    protected void addVendor(Date processDate, String vendorId) throws Exception
    {
        List<String> args = new ArrayList<String>();
        String deliveryDateString = getSDF().format(processDate);
        args.add(deliveryDateString);
        args.add(vendorId);
        Command command = CommandFactory.createCommand(getConnection(), COMMAND_ADD_VENDOR_DATE, args);
        command.execute();

    }

    /**
     * Add all of the product vendor combos for the delivery date to PAS.
     * @throws Exception
     */
    protected void addVendorProductState(Date processDate, String vendorId, String productId, String stateCode, boolean invalidate) throws Exception
    {
        // Add country and products
        List<String> args = new ArrayList<String>();
        String deliveryDateString = getSDF().format(processDate);
        args.add(deliveryDateString);
        args.add(vendorId);
        args.add(productId);
        args.add(stateCode);
        if (invalidate)
        {
            args.add(PROCESS_INVALIDATION);
        }
        Date today = getSDF().parse(getSDF().format(new Date()));
        int priority = (int)( (processDate.getTime() - today.getTime()) / (1000 * 60 * 60 * 24) );
        if (priority < 0) priority = 0;
        logger.info(getSDF().format(processDate) + " priority: " + priority);
        spawnNewJMSCommand(COMMAND_ADD_VENDOR_PRODUCT_STATE_DATE, args, priority);
    }

    /**
     * Add all of the zip codes for the delivery date to PAS.  These are
     * all spawned as separate JMS messages to improve the performance.
     * @throws Exception
     */
    protected void addFloristZipCode(Date processDate) throws Exception
    {
        List<String> args = new ArrayList<String>();
        String deliveryDateString = getSDF().format(processDate);
        args.add(deliveryDateString);
        args.add(ALL);
        Date today = getSDF().parse(getSDF().format(new Date()));
        int priority = (int)( (processDate.getTime() - today.getTime()) / (1000 * 60 * 60 * 24) );
        if (priority < 0) priority = 0;
        logger.info(getSDF().format(processDate) + " priority: " + priority);
        spawnNewJMSCommand(COMMAND_ADD_FLORIST_ZIPCODE_DATE, args, priority);
    }
    
    protected void recalcFloristZipCode(String zipcode, Date processDate) throws Exception {
        List<String> args = new ArrayList<String>();
        args.add(zipcode);
        String processDateString = getSDF().format(processDate);
        args.add(processDateString);
        //spawnNewJMSCommand(COMMAND_RECALC_FLORIST_ZIP, args);
        Command command = CommandFactory.createCommand(getConnection(), COMMAND_RECALC_FLORIST_ZIP, args);
        command.execute();
    }

    /**
     * Add all of the countries for the delivery date to PAS.
     * @throws Exception
     */
    protected void addCountry(Date processDate, String countryId) throws Exception
    {
        List<String> args = new ArrayList<String>();
        String deliveryDateString = getSDF().format(processDate);
        args.add(deliveryDateString);
        args.add(countryId);
        Command command = CommandFactory.createCommand(getConnection(), COMMAND_ADD_COUNTRY_DATE, args);
        command.execute();
    }

    /**
     * Add all of the country product combos for the delivery date to PAS.
     * @throws Exception
     */
    protected void addCountryProduct(Date processDate, String countryId, String productId) throws Exception
    {
        // Add country and products
        List<String> args = new ArrayList<String>();
        String deliveryDateString = getSDF().format(processDate);
        args.add(deliveryDateString);
        args.add(countryId);
        args.add(productId);
        Command command = CommandFactory.createCommand(getConnection(), COMMAND_ADD_COUNTRY_PRODUCT_DATE, args);
        command.execute();
    }
    
    /**
     * Add all of the time zones for the delivery date to PAS.
     * @throws Exception
     */
    protected void addTimeZone(Date processDate) throws Exception
    {
        List<String> args = new ArrayList<String>();
        String deliveryDateString = getSDF().format(processDate);
        args.add(deliveryDateString);
        Command command = CommandFactory.createCommand(getConnection(), COMMAND_ADD_TIME_ZONE_DATE, args);
        command.execute();
    }
  
}
