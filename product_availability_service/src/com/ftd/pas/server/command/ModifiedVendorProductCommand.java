package com.ftd.pas.server.command;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pas.server.exception.InvalidPASCommandArgumentException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Command for processing the modification to a vendor product.  This could
 * be triggered in multiple different methods.  This will reset all of the 
 * data for the vendor.
 */
public class ModifiedVendorProductCommand extends AbstractDateLoopingCommand
{
    protected Logger logger = new Logger(this.getClass().getName());

    protected String vendorId;
    protected String productId;

    /**
     * Create a new object.
     */
    public ModifiedVendorProductCommand()
    {
        super();
    }

    /**
     * Set the list of arguments.
     * expected is:
     * vendorId 
     * productId
     * @param arguments
     * @throws InvalidPASCommandArgumentException if the arguments are not correct.
     */
    public void setArguments(List<String> arguments) throws InvalidPASCommandArgumentException

    {
        super.setArguments(arguments);

        if (arguments.size() != 2)
        {
            throw new InvalidPASCommandArgumentException("Wrong number of Arguments, need 2 got " + arguments.size());
        }

        // Should have one argument, a vendor.
        vendorId = arguments.get(0);
        productId = arguments.get(1);

    }

    /**
     * Execute the command.  Since the vendor has changed it could affect availability
     * for all products for the vendor.  Need to do a mass update of all the vendor
     * products for all dates.
     */
    protected void executeSingleUnit(Date processDate) throws Exception
    {
        logger.debug("execute called");

        addVendorProductState(processDate, vendorId, productId, ALL, true);

    }


}
