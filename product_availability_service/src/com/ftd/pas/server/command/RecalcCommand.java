package com.ftd.pas.server.command;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pas.server.exception.InvalidPASCommandArgumentException;

import java.util.Date;
import java.util.List;

/**
 * Command for processing a total recalculation of the product availability data.
 */
public class RecalcCommand extends AbstractDateLoopingCommand
{
    protected Logger logger = new Logger(this.getClass().getName());


    /**
     * Create a new object.
     */
    public RecalcCommand()
    {
        super();
    }

    /**
     * Set the list of arguments.
     * expected is:
     *   None
     * @param arguments
     * @throws InvalidPASCommandArgumentException if the arguments are not correct.
     */
    public void setArguments(List<String> arguments) throws InvalidPASCommandArgumentException

    {
        super.setArguments(arguments);

        if (arguments.size() != 0)
        {
            throw new InvalidPASCommandArgumentException("Wrong number of Arguments, need 0 got " + arguments.size());
        }

    }

    /**
     * Execute the command.  Just update the base product data.
     */
    protected void executeSingleUnit(Date processDate) throws Exception
    {
        logger.debug("Recalc Processing " + processDate);

        // PAS_TIMEZONE
        addTimeZone(processDate);
        // PAS_VENDOR
        addVendor(processDate, ALL);
        // PAS_PRODUCT
        addProduct(processDate, ALL);
        // PAS_COUNTRY
        addCountry(processDate, ALL);
        // PAS_FLORIST_ZIPCODE
        addFloristZipCode(processDate);
        // PAS_PRODUCT_STATE
        addProductState(processDate, ALL, ALL);
        // PAS_VENDOR_PRODUCT_STATE
        addVendorProductState(processDate, ALL, ALL, ALL, true);
        // PAS_COUNTRY_PRODUCT
        addCountryProduct(processDate, ALL, ALL);
    }



}
