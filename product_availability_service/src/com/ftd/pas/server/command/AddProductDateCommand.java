package com.ftd.pas.server.command;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pas.server.constants.PASConstants;
import com.ftd.pas.server.exception.InvalidPASCommandArgumentException;
import com.ftd.pas.server.vo.PASProductVO;
import com.ftd.pas.server.vo.ProductDetailVO;


/**
 * Command for processing the adding of a new product for a delivery date to the
 * product availability.  All of the processing needed to add a new date is performed.
 */
public class AddProductDateCommand extends AbstractLoopingCommand
{
    protected Logger logger = new Logger(this.getClass().getName());

    protected String          productId;

    /**
     * Create a new AddProductDateCommand object.
     */
    public AddProductDateCommand()
    {
        super();
    }

    /**
     * Set the list of arguments.
     * expected is:
     * DeliveryDate in MMddYYYY format
     * ProductId 
     * @param arguments
     * @throws InvalidPASCommandArgumentException if the arguments are not correct.
     */
    public void setArguments(List<String> arguments) throws InvalidPASCommandArgumentException

    {
        super.setArguments(arguments);

        if (arguments.size() != 2)
        {
            throw new InvalidPASCommandArgumentException("Wrong number of Arguments, need 2 got " + arguments.size());
        }

        // Should have first argument, a delivery Date.
        String deliveryDateString = arguments.get(0);
        try
        {
            setDeliveryDate(deliveryDateString);
        } catch (ParseException pe)
        {
            logger.error("Invalid Delivery Date " + deliveryDateString, pe);
            throw new InvalidPASCommandArgumentException("Invalid Delivery Date");
        }
        if (arguments.get(1).equals(ALL))
        {
            productId = null;
        }
        else
        {
            productId = arguments.get(1);
        }
    }

    /**
     * Set the arguments for the command.
     * @param deliveryDate
     * @param productId
     * @throws InvalidPASCommandArgumentException
     */
    public void setArguments(Date deliveryDate, String productId) throws InvalidPASCommandArgumentException
    {
        setDeliveryDate(deliveryDate);
        this.productId = productId;
    }

    /**
     * Set the arguments for the command.
     * @param deliveryDate
     * @param productDetailVO Must be a ProductDetailVO or exception is thrown
     * @throws InvalidPASCommandArgumentException
     */
    public void setArguments(Date deliveryDate, Object productDetailVO) throws InvalidPASCommandArgumentException
    {
        setDeliveryDate(deliveryDate);
        
        if (productDetailVO instanceof ProductDetailVO)
        {
            this.paramObject = productDetailVO;
        }
        else
        {
            throw new InvalidPASCommandArgumentException("Expecting ProductDetailVO, got " + productDetailVO.getClass().getName());
        }
    }


    /**
     * Get the list of products to process.
     * @return
     * @throws Exception
     */
    protected List getObjects() throws Exception
    {
        if (isPerformInvalidation())
        {
            getMaintDAO().invalidatePASProduct(getConnection(),deliveryDate,productId);
        }

        List<ProductDetailVO> details = getQueryDAO().getActiveProducts(getConnection(), productId, deliveryDate); 
        return details;        
    }

    /**
     * Process a product and insert the results into PAS_PRODUCT_DT.
     * @param object
     * @throws Exception
     */
    protected void executeSingleUnit(Object object) throws Exception
    {
        ProductDetailVO productDetailVO = (ProductDetailVO) object;
        logger.debug("Processing " + productDetailVO.getProductId());
        
        // Build up the PASProductVO
        PASProductVO product = new PASProductVO();
        product.setProductId(productDetailVO.getProductId());
        product.setProductType(productDetailVO.getProductType());
        product.setDeliveryDate(deliveryDate);
        product.setAddonDays(0L);
        product.setTransitDays(0L);
        product.setDeliveryAvailable(YES);
        product.setShipAllowed(YES);
        product.setShipRestricted(YES);
        product.setCodificationId(productDetailVO.getCodificationId());
        
        if (productDetailVO.getExoticFlag() != null && productDetailVO.getExoticFlag().equals(YES))
        {
            // Add a day to addonDays
            //SP-121 - Get the configured exotic lead days //product.setAddonDays(1L);
        	long exoticLeadDays = 1L;
        	try {
        		exoticLeadDays = Long.parseLong(getConfigurationUtil().getFrpGlobalParm(PASConstants.FTDAPPS_CONFIG_CONTEXT, "exotic_lead_days"));
        	} catch(Exception e) {
        		logger.error("Unable to get the lead days configured, exotic_lead_days. Setting to default value 1. " + e);
        		exoticLeadDays = 1L;
        	}         	       	
        	if(exoticLeadDays > 0) {
        		product.setAddonDays(exoticLeadDays);
        	}
        	
            String exoticCutoff = getConfigurationUtil().getFrpGlobalParm(PASConstants.FTDAPPS_CONFIG_CONTEXT, "EXOTIC_CUTOFF");
            product.setCutoffTime(exoticCutoff);
        }
        
        if (productDetailVO.getPersonalizationLeadDays() != null && productDetailVO.getPersonalizationLeadDays() > 0)
        {
            product.setAddonDays(productDetailVO.getPersonalizationLeadDays());

            String perAddonDaysString = getConfigurationUtil().getFrpGlobalParm(PASConstants.SHIPPING_CONFIG_CONTEXT, "PERSONAL_CREATIONS_DAYS_IN_TRANSIT");
            Long perAddonDays = Long.valueOf(perAddonDaysString);
            product.setTransitDays(perAddonDays);
        }
        

        // Check the day of the week for the product.  
        int dayOfWeek = deliveryDateCalendar.get(Calendar.DAY_OF_WEEK);
        String productDayOfWeekFlag = NO;
        switch (dayOfWeek)
        {
        case Calendar.SUNDAY:
            productDayOfWeekFlag = productDetailVO.getSundayFlag();
            break;
        case Calendar.MONDAY:
            productDayOfWeekFlag = productDetailVO.getMondayFlag();
            break;
        case Calendar.TUESDAY:
            productDayOfWeekFlag = productDetailVO.getTuesdayFlag();
            break;
        case Calendar.WEDNESDAY:
            productDayOfWeekFlag = productDetailVO.getWednesdayFlag();
            break;
        case Calendar.THURSDAY:
            productDayOfWeekFlag = productDetailVO.getThursdayFlag();
            break;
        case Calendar.FRIDAY:
            productDayOfWeekFlag = productDetailVO.getFridayFlag();
            break;
        case Calendar.SATURDAY:
            productDayOfWeekFlag = productDetailVO.getSaturdayFlag();
            break;
        }
        if (productDayOfWeekFlag != null)
        {
            // Reverse the value
            if (productDayOfWeekFlag.equals(YES))
            {
                product.setShipRestricted(NO);
            }
            else
            {
                product.setShipRestricted(YES);
            }
        }
        
        if (productDetailVO.getExceptionCode() != null && productDetailVO.getExceptionCode().equals("A"))
        {
            // Check for exception black out dates
            if (deliveryDate.before(productDetailVO.getExceptionStartDate()))
            {
                product.setDeliveryAvailable(NO);
            }
            if (deliveryDate.after(productDetailVO.getExceptionEndDate()))
            {
                product.setDeliveryAvailable(NO);
            }
        }

        String cutoffParmContext = PASConstants.FTDAPPS_CONFIG_CONTEXT;
        String cutoffParmName = PASConstants.LATEST_CUTOFF_PARM;
        if (productDetailVO.getShipMethodFlorist() != null && productDetailVO.getShipMethodFlorist().equalsIgnoreCase(YES)) {
            product.setShipMethodFlorist(YES);
        }
        if (productDetailVO.getShipMethodCarrier() != null && productDetailVO.getShipMethodCarrier().equalsIgnoreCase(YES)) {
            product.setShipMethodCarrier(YES);
            cutoffParmContext = PASConstants.SHIPPING_CONFIG_CONTEXT;
            cutoffParmName = PASConstants.LASTEST_VENDOR_CUTOFF_PARM;
        }
        String latestCutoff = getConfigurationUtil().getFrpGlobalParm(cutoffParmContext, cutoffParmName);
        product.setCutoffTime(latestCutoff);
        if (product.getProductType() != null &&
        		!product.getProductType().equalsIgnoreCase("SERVICES") &&
        		(product.getShipMethodFlorist() == null || product.getShipMethodFlorist().equals(NO)) &&
        		(product.getShipMethodCarrier() == null || product.getShipMethodCarrier().equals(NO))) {
        	product.setDeliveryAvailable(NO);
        }

        getMaintDAO().insertPASProduct(getConnection(),product);
    }
}
