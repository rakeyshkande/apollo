package com.ftd.pas.server.command;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pas.server.exception.InvalidPASCommandArgumentException;

import com.ftd.pas.server.vo.PASFloristZipcodeVO;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Command for processing the modification to a Florist.
 * Loops through all the dates in product availability and changes each.
 */
public class ModifiedFloristCommand extends AbstractLoopingCommand
{
    protected Logger logger = new Logger(this.getClass().getName());

    protected String floristId;
    protected Date startDate;
    protected Date endDate;
    protected boolean dayFlag;
    protected String dayValue;
    
    /**
     * Create a new object.
     */
    public ModifiedFloristCommand()
    {
        super();
    }

    /**
     * Set the list of arguments.
     * expected is:
     * None 
     * @param arguments
     * @throws InvalidPASCommandArgumentException if the arguments are not correct.
     */
    public void setArguments(List<String> arguments) throws InvalidPASCommandArgumentException

    {
        super.setArguments(arguments);

        if (arguments.size() < 1 || arguments.size() > 3)
        {
            throw new InvalidPASCommandArgumentException("Wrong number of Arguments, need 1, 2, or 3 got " + arguments.size());
        }

        floristId = arguments.get(0);
        dayFlag = false;
        dayValue = null;
        startDate = new Date();
        endDate = new Date();
        try {
            Calendar cal = Calendar.getInstance();   
            cal.setTime(startDate);   
            cal.add(Calendar.DATE, getMaxDays());  
            endDate = cal.getTime();
            endDate = getSDF().parse(getSDF().format(endDate));
        } catch (Exception e) {
            logger.error("Date error: " + e);
        }
        
        // Process the second optional argument
        String secondArg = "";
        String thirdArg = "";
        if (arguments.size() > 1)
        {
            secondArg = arguments.get(1);
            if (secondArg != null && secondArg.indexOf("DAY") > -1) {
                dayFlag = true;
                dayValue = secondArg;
            } else {
                try {
                    Date tempDate = getSDF().parse(secondArg);
                    if (tempDate.getTime() > startDate.getTime()) {
                        startDate = tempDate;
                    }
                } catch (Exception e) {
                    logger.error("parse exception: " + e);
                }
            }
            if (arguments.size() == 3) {
                thirdArg = arguments.get(2);
                try {
                    Date tempDate = getSDF().parse(thirdArg);
                    if (tempDate.getTime() < endDate.getTime()) {
                        endDate = tempDate;
                    }
                } catch (Exception e) {
                    logger.error("parse exception: " + e);
                }
            }
        }
        logger.info("floristId: " + floristId + " " + secondArg + " " + thirdArg);
        logger.info("start: " + getSDF().format(startDate) + " end: " + getSDF().format(endDate));

    }

    /**
     * Get the list of objects to loop through.  Setup a couple objects that won't change
     * during the process.
     * @return
     * @throws Exception
     */
    protected List getObjects() throws Exception {
        List details = null;
        return details;
    }
    
    protected void executeCommand() throws Exception
    {
        SimpleDateFormat sdfE = new SimpleDateFormat("EEE");
        List details = getQueryDAO().getFloristZipsByFlorist(getConnection(), floristId);
        Calendar startCal = Calendar.getInstance();   
        Calendar endCal = Calendar.getInstance();   
        endCal.setTime(endDate);
        endCal.add(Calendar.DATE, 1);
        for (int i=0; i < details.size(); i++) {
            String zipCode = (String) details.get(i);
            startCal.setTime(startDate);
            while (startCal.before(endCal)) {
                if (!dayFlag || sdfE.format(startCal.getTime()).equalsIgnoreCase(dayValue.substring(0,3))) {
                    PASFloristZipcodeVO zipVO = new PASFloristZipcodeVO();
                    zipVO.setZipCode(zipCode);
                    zipVO.setDeliveryDate(startCal.getTime());
                    executeSingle(zipVO);
                }
                startCal.add(Calendar.DAY_OF_YEAR, 1);
            }
        }

    }
    /**
     * Execute the command.  Since the exotic cutoff has changed, change all the products
     * that are exotic
     */
    protected void executeSingleUnit(Object obj) throws Exception
    {
        PASFloristZipcodeVO zipVO = (PASFloristZipcodeVO) obj;
        //logger.info("executeSingleUnit() " + zipVO.getZipCode() + " " + zipVO.getDeliveryDate());
        recalcFloristZipCode(zipVO.getZipCode(), zipVO.getDeliveryDate());

    }

}
