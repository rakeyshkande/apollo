package com.ftd.pas.server.command;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.pas.server.constants.PASConstants;
import com.ftd.pas.server.exception.PASCommandException;
import com.ftd.pas.server.util.SystemMessage;

import java.sql.Connection;

import java.sql.SQLException;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.naming.InitialContext;

import javax.transaction.UserTransaction;

/**
 * Base class for a command that needs to process through all of the days
 * setup in product availability.  
 */
public abstract class AbstractDateLoopingCommand extends AbstractCommand
{
    Connection utConn = null;

    /**
     * Run the command for each Date that is in the set of data for
     * product availablility.  Each day of the set is done in a
     * separate transaction.
     */
     protected void executeCommand() throws Exception
     {
         int maxDays = getMaxDays();

         Calendar processDateCalendar = Calendar.getInstance();
         for (int i=0; i <= maxDays; i++)
         {
             executeSingle(processDateCalendar.getTime());
             processDateCalendar.add(Calendar.DAY_OF_YEAR,1);
         }
    }
    
    protected void executeCommand2() throws Exception
    {
        int maxDays = getMaxDays();

        InitialContext context = null;
        UserTransaction userTransaction = null;
        Connection utConn = null;
        
        // Store off the original connection
        Connection mainConn = getConnection();
        if (mainConn != null)
        {
            mainConn.close();
        }

        Calendar processDateCalendar = Calendar.getInstance();
        for (int i=0; i <= maxDays; i++)
        {
            try
            {
                context = new InitialContext();
                userTransaction = (UserTransaction) context.lookup("java:comp/UserTransaction");
                userTransaction.begin();

                ConfigurationUtil cu = ConfigurationUtil.getInstance();
                String datasource = cu.getPropertyNew(PASConstants.PROPERTY_FILE, PASConstants.DATASOURCE_NAME);

                utConn = DataSourceUtil.getInstance().getConnection(datasource);

                // Switch the connection to the ut one
                setConnection(utConn);
                
                executeSingle(processDateCalendar.getTime());
                processDateCalendar.add(Calendar.DAY_OF_YEAR,1);
                
                // Commit the transaction
                userTransaction.commit();
            }
            finally
            {
                // Make sure the transaction is closed
                if( utConn!=null ) 
                {
                    try 
                    {
                        if( !utConn.isClosed() ) 
                        {
                            utConn.close();
                        }
                    } 
                    catch (SQLException sqle) 
                    {
                        logger.warn("Error closing database connection",sqle);
                    }
                }
                
            }
        }
        // Reset the initial connection
        setConnection(mainConn);
    }

    /**
     * Process a single unit of work for the command.  These will be grouped
     * to allow for limiting of system messaging and errors.
     * @param processDate
     * @throws Exception
     */
    protected abstract void executeSingleUnit(Date processDate) throws Exception;

    /**
     * The purpose of this method is to handle exceptions that happen on a
     * unit of work within a command.  The exception will be captured and a
     * system message written.  The number of exceptions is counted and after
     * a set number of exceptions this method will escalate the exception out 
     * out of the method.
     * @param processDate
     * @throws Exception
     */
    protected void executeSingle(Date processDate) throws Exception
    {
        try
        {
            executeSingleUnit(processDate);
        }
        catch (Exception e)
        {
            logger.error("Error processing command ", e);
            SystemMessage.sendSystemMessage(getConnection(), "PAS Command Failed", e.getMessage());
        }

    }

}
