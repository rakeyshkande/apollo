package com.ftd.pas.server.command;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pas.server.exception.InvalidPASCommandArgumentException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Command for processing the modification to a product.  This could
 * be triggered in multiple different methods.  This will reset just the 
 * data for the product.  This command will not cascade updates to the vendor
 * product state table.
 */
public class ModifiedProductCommand extends AbstractDateLoopingCommand
{
    protected Logger logger = new Logger(this.getClass().getName());

    protected String productId;
    protected boolean cascadeUpdate;

    /**
     * Create a new object.
     */
    public ModifiedProductCommand()
    {
        super();
    }

    /**
     * Set the list of arguments.
     * expected is:
     * productId 
     * cascade Update Flag
     * @param arguments
     * @throws InvalidPASCommandArgumentException if the arguments are not correct.
     */
    public void setArguments(List<String> arguments) throws InvalidPASCommandArgumentException

    {
        super.setArguments(arguments);

        if (arguments.size() != 2 && arguments.size() != 3)
        {
            throw new InvalidPASCommandArgumentException("Wrong number of Arguments, need 2 or 3 got " + arguments.size());
        }

        // one argument, a product.
        productId = arguments.get(0);

        String argument = arguments.get(1);
        if (argument != null && argument.equals(NO))
        {
            cascadeUpdate = false;
        }
        else
        {
            cascadeUpdate = true;
        }
        
        // If invalidationFlag is passed from caller, use that flag passed. Otherwise always perform invalidation.
        
        String invalidationFlag = null;
        
        if (arguments.size() == 3){
        	invalidationFlag = arguments.get(2);
        } else {
        	logger.debug("Invalidation flag is not passed. Will default to true.");
        	invalidationFlag = YES;
        }

        boolean invalidation = (invalidationFlag.equals(YES)) ? true : false;
        super.setPerformInvalidation(invalidation);

    }

    /**
     * Execute the command.  Just update the base product data.
     */
    protected void executeSingleUnit(Date processDate) throws Exception
    {
        logger.debug("Processing " + processDate);
        addProduct(processDate, productId);
        
        if (cascadeUpdate)
        {
            addProductState(processDate, productId, ALL);
            logger.info("perform invalidation:" + super.isPerformInvalidation());
            addVendorProductState(processDate, ALL, productId, ALL, super.isPerformInvalidation());
        }

    }


}
