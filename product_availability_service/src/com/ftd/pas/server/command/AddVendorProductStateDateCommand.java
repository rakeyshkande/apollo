package com.ftd.pas.server.command;

import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.ShipMethodHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ShipMethodVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.StateMasterVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pas.server.constants.PASConstants;
import com.ftd.pas.server.exception.InvalidPASCommandArgumentException;

import com.ftd.pas.server.vo.PASCountryVO;
import com.ftd.pas.server.vo.PASProductStateVO;
import com.ftd.pas.server.vo.PASProductVO;
import com.ftd.pas.server.vo.PASVendorProductStateVO;
import com.ftd.pas.server.vo.PASVendorVO;
import com.ftd.pas.server.vo.VendorProductStateDetailVO;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Command for processing the adding of a new country's products for a delivery date to the
 * product availability.  All of the processing needed
 * to add a new date is performed.
 */
public class AddVendorProductStateDateCommand extends AbstractLoopingCommand
{
    protected Logger logger = new Logger(this.getClass().getName());

    protected String vendorId;
    protected String productId;
    protected String stateCode;
    
    protected List<StateMasterVO> dropShipStates = null;
       
    protected Map<String,Map>  pasCountryDateMap = new HashMap<String,Map>();
    protected Map<String,Map>  pasVendorDateMap = new HashMap<String,Map>();
    protected Map<String,Map>  pasProductDateMap = new HashMap<String,Map>();
    
    /**
     * Create a new AddVendorProductStateDateCommand object.
     */
    public AddVendorProductStateDateCommand()
    {
        super();
    }

    /**
     * Set the list of arguments.
     * expected is:
     * DeliveryDate in MMddYYYY format
     * vendorId (ALL means all)
     * productId (ALL means all)
     * stateCode (ALL means all)
     * @param arguments
     * @throws InvalidPASCommandArgumentException if the arguments are not correct.
     */
    public void setArguments(List<String> arguments) throws InvalidPASCommandArgumentException

    {
        super.setArguments(arguments);

        if (arguments.size() < 4 || arguments.size() > 5)
        {
            throw new InvalidPASCommandArgumentException("Wrong number of Arguments, need 4 or 5 got " + arguments.size());
        }

        // Should have first argument, a delivery Date.
        String deliveryDateString = arguments.get(0);
        try
        {
            setDeliveryDate(deliveryDateString);
        } catch (ParseException pe)
        {
            logger.error("Invalid Delivery Date " + deliveryDateString, pe);
            throw new InvalidPASCommandArgumentException("Invalid Delivery Date");
        }

        // Convert ALL to a null argument
        String argument = arguments.get(1);
        if (argument.equals(ALL))
        {
            vendorId = null;
        } else
        {
            vendorId = argument;
        }
        argument = arguments.get(2);
        if (argument.equals(ALL))
        {
            productId = null;
        } else
        {
            productId = argument;
        }
        argument = arguments.get(3);
        if (argument.equals(ALL))
        {
            stateCode = null;
        } else
        {
            stateCode = argument;
        }

        // Process the optional argument
        if (arguments.size() == 5)
        {
            String processInvalString = arguments.get(4);
            if (PROCESS_INVALIDATION.equals(processInvalString))
            {
                setPerformInvalidation(true);
            }
        }

    }

    /**
     * Set the arguments for the command.
     * @param deliveryDate
     * @param vendorId vendor id, null means all
     * @param productId product id, null means all
     * @param stateCode state code, null means all
     * @throws InvalidPASCommandArgumentException
     */
    public void setArguments(Date deliveryDate, String vendorId, String productId, String stateCode) throws InvalidPASCommandArgumentException
    {
        setDeliveryDate(deliveryDate);
        this.vendorId = vendorId;
        this.productId = productId;
        this.stateCode = stateCode;
    }

    /**
     * Set the arguments for the command.
     * @param deliveryDate
     * @param vendorProductStateDetailVO Must be a VendorProductStateDetailVO or exception is thrown
     * @throws InvalidPASCommandArgumentException
     */
    public void setArguments(Date deliveryDate, Object vendorProductStateDetailVO) throws InvalidPASCommandArgumentException
    {
        setDeliveryDate(deliveryDate);

        if (vendorProductStateDetailVO instanceof VendorProductStateDetailVO)
        {
            this.paramObject = vendorProductStateDetailVO;
        } else
        {
            throw new InvalidPASCommandArgumentException("Expecting VendorProductStateDetailVO, got " + vendorProductStateDetailVO.getClass().getName());
        }

    }

    /**
     * Override the base class implementation.  If this command is for all vendors, then 
     * spawn JMS messages to process each vendor.  It the command is for a specific, then
     * execute the normal looping command of the base class.
     * @throws Exception
     */
    protected void executeCommand() throws Exception
    {
        if (vendorId == null && productId == null)
        {
            logger.info("sendVendorCommands");
        	sendVendorCommands();
        }
        else
        {
        	logger.info("executeCommand");
        	super.executeCommand();
        }
    }

    /**
     * Get the list of vendor products to process.
     * @return
     * @throws Exception
     */
    protected List getObjects() throws Exception
    {
        if (isPerformInvalidation())
        {
            getMaintDAO().invalidatePASVendorProductState(getConnection(),deliveryDate,vendorId,productId,stateCode);
        }

        List<VendorProductStateDetailVO> details = getQueryDAO().getVendorProductDetails(getConnection(), vendorId, productId);
        return details;
    }


    /**
     * Process a single Vendor Product combination.  The looping of states is handled in the method.
     * @throws Exception
     */
    protected void executeSingleUnit(Object param) throws Exception
    {
        VendorProductStateDetailVO details = (VendorProductStateDetailVO)param;

        if (logger.isDebugEnabled()) {
            logger.debug("Processing " + details.getVendorId() + " " + details.getProductId() + " " + details.getProductSubcode());
        }
        
        String productId = details.getProductId();
        if (productId == null)
        {
            productId = details.getProductSubcode();
        }
        
        if (logger.isDebugEnabled()) {
            logger.debug("Using productId of " + productId);
        }
        PASProductVO pasProductVO = getPASProduct(productId, deliveryDate);
        PASVendorVO pasVendorVO = getPASVendor(details.getVendorId(), deliveryDate);
                
        if (pasVendorVO == null || pasProductVO == null ||
            pasProductVO.getShipMethodCarrier() == null ||
            !pasProductVO.getShipMethodCarrier().equals(YES))
        {
            if (logger.isDebugEnabled()) {
                logger.debug("No Delivery - " + pasVendorVO + " " + pasProductVO);
                if (pasProductVO != null) {
                    logger.debug("getShipMethodCarrier: " + pasProductVO.getShipMethodCarrier());
                }
            }
        } 
        else 
        {
    
            if(pasVendorVO.getVendorType().equals("FTD WEST")){
               	logger.info("skipping vendor product date calc");
            }
            else{
	        	calculateTransitDays(details, pasProductVO);
	            
	            HashMap<Date, String> invMap = new HashMap<Date, String>();
	            
	            // Get the list of available drop ship states
	            List<StateMasterVO> states = getDropShipStates(stateCode);
	            for (int i = 0; i < states.size(); i++)
	            {
	                StateMasterVO stateVO = states.get(i);
	                if (logger.isDebugEnabled()) {
	                    logger.debug("Processing state " + stateVO.getStateMasterId());
	                }
	
	    
	                PASVendorProductStateVO pasVendorProductStateVO = new PASVendorProductStateVO();
	                pasVendorProductStateVO.setDeliveryDate(deliveryDate);
	                pasVendorProductStateVO.setVendorId(details.getVendorId());
	                pasVendorProductStateVO.setProductId(details.getProductSubcode());
	                
	                // Start out nulling all deliveries
	                pasVendorProductStateVO.setShipNDDate(null);
	                pasVendorProductStateVO.setShip2DDate(null);
	                pasVendorProductStateVO.setShipGRDate(null);
	                pasVendorProductStateVO.setShipSatDate(null);
	                pasVendorProductStateVO.setShipNDCutoffTime("0000");
	                pasVendorProductStateVO.setShip2DCutoffTime("0000");
	                pasVendorProductStateVO.setShipGRCutoffTime("0000");
	                pasVendorProductStateVO.setShipSatCutoffTime("0000");
	    
	                pasVendorProductStateVO.setStateCode(stateVO.getStateMasterId());
	    
	                // Get the country for the state
	                String countryId = getCountryIdForState(stateVO);
	                PASCountryVO pasCountryVO = getPASCountry( countryId, deliveryDate, "ND");
	
	                // PASProductState will only have a row if there is an exclusion, not required
	                PASProductStateVO pasProductStateVO = getQueryDAO().getPASProductState(getConnection(), productId, stateVO.getStateMasterId(), deliveryDate);
	    
	                if (logger.isDebugEnabled()) {
	                    logger.debug(pasCountryVO.getDeliveryAvailable() + " " +pasVendorVO.getDeliveryAvailable()+" " +pasProductVO.getDeliveryAvailable() );
	                }
	                    
	                // If not available that day, puke
	                if (pasCountryVO == null || 
	                    pasCountryVO.getDeliveryAvailable().equals(NO) ||
	                    pasVendorVO.getDeliveryAvailable().equals(NO) || 
	                    pasProductVO.getDeliveryAvailable().equals(NO) ||
	                    (pasProductStateVO != null && pasProductStateVO.getDeliveryAvailable().equals(NO))
	                    )
	                {
	                    if (logger.isDebugEnabled()) {
	                        logger.debug("No Delivery - " + pasCountryVO + " " + pasProductStateVO);
	                    }
	                } 
	                else
	                {
	                    // It if is a Saturday, process Saturday delivery rules
	                    if (deliveryDateCalendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY)
	                    {
	                        if (logger.isDebugEnabled()) {
	                            logger.debug("Saturday delivery");
	                        }
	                        // Check for the saturday delivery flag
	                        if (details.isSaturdayDeliveryAvailable() && !isSaturdayExcludedState(pasVendorProductStateVO))
	                        {
	                            // Calculate Saturday Delivery
	                            calculateSaturdayShipDate(details,pasVendorProductStateVO, countryId, productId);
	                        } 
	                        else
	                        {
	                            if (logger.isDebugEnabled()) {
	                                logger.debug("Not Saturday delivery");
	                            }
	                            // If no saturday delivery, puke
	                        }
	                    } 
	                    else
	                    {
	                        // Not on saturday
	                        calculateShipDates(details,pasVendorProductStateVO, countryId, productId);
	                    }
	                }
	                
	                if (pasVendorVO.getVendorType() == null || pasVendorVO.getVendorType().equals("FTP")) {
	                    if (logger.isDebugEnabled()) {
	                	    logger.debug("FTP Vendor, don't check Inventory Tracking");
	                    }
	                } else if (pasVendorVO.getVendorType().equals("FTD WEST")) {
	                    if (logger.isDebugEnabled()) {
	                	    logger.debug("FTD WEST Vendor, don't check Inventory Tracking");
	                    }
	                } else {
	                    if (logger.isDebugEnabled()) {
	                	    logger.debug("Check Inventory Tracking for availability");
	                    }
	                	if (pasVendorProductStateVO.getShipNDDate() != null) {
	                		String shutdownReached = invMap.get(pasVendorProductStateVO.getShipNDDate());
	                		if (shutdownReached == null) {
	                		    HashMap results = getQueryDAO().getInventoryTrackingAvailability(getConnection(),
	                				pasVendorProductStateVO.getProductId(),
	                				pasVendorProductStateVO.getVendorId(),
	                				pasVendorProductStateVO.getShipNDDate());
	                		    if (results != null) {
	                			    shutdownReached = (String) results.get("SHUTDOWN_LVL_REACHED");
	                			    invMap.put(pasVendorProductStateVO.getShipNDDate(), shutdownReached);
	                		    }
	                        }
	              			if (shutdownReached != null && shutdownReached.equalsIgnoreCase("Y")) {
	               			    pasVendorProductStateVO.setShipNDDate(null);
	               			    pasVendorProductStateVO.setShipNDCutoffTime("0000");
	                		}
	                	}
	                	if (pasVendorProductStateVO.getShip2DDate() != null) {
	                		String shutdownReached = invMap.get(pasVendorProductStateVO.getShip2DDate());
	                		if (shutdownReached == null) {
	                		    HashMap results = getQueryDAO().getInventoryTrackingAvailability(getConnection(),
	                				pasVendorProductStateVO.getProductId(),
	                				pasVendorProductStateVO.getVendorId(),
	                				pasVendorProductStateVO.getShip2DDate());
	                		    if (results != null) {
	                			    shutdownReached = (String) results.get("SHUTDOWN_LVL_REACHED");
	                			    invMap.put(pasVendorProductStateVO.getShip2DDate(), shutdownReached);
	                		    }
	                        }
	                		if (shutdownReached != null && shutdownReached.equalsIgnoreCase("Y")) {
	                		    pasVendorProductStateVO.setShip2DDate(null);
	                		    pasVendorProductStateVO.setShip2DCutoffTime("0000");
	                		}
	                	}
	                	if (pasVendorProductStateVO.getShipGRDate() != null) {
	                		String shutdownReached = invMap.get(pasVendorProductStateVO.getShipGRDate());
	                		if (shutdownReached == null) {
	                		    HashMap results = getQueryDAO().getInventoryTrackingAvailability(getConnection(),
	                				pasVendorProductStateVO.getProductId(),
	                				pasVendorProductStateVO.getVendorId(),
	                				pasVendorProductStateVO.getShipGRDate());
	                		    if (results != null) {
	                			    shutdownReached = (String) results.get("SHUTDOWN_LVL_REACHED");
	                			    invMap.put(pasVendorProductStateVO.getShipGRDate(), shutdownReached);
	                		    }
	                        }
	                		if (shutdownReached != null && shutdownReached.equalsIgnoreCase("Y")) {
	                		    pasVendorProductStateVO.setShipGRDate(null);
	                		    pasVendorProductStateVO.setShipGRCutoffTime("0000");
	                		}
	                	}
	                	if (pasVendorProductStateVO.getShipSatDate() != null) {
	                		String shutdownReached = invMap.get(pasVendorProductStateVO.getShipSatDate());
	                		if (shutdownReached == null) {
	                		    HashMap results = getQueryDAO().getInventoryTrackingAvailability(getConnection(),
	                				pasVendorProductStateVO.getProductId(),
	                				pasVendorProductStateVO.getVendorId(),
	                				pasVendorProductStateVO.getShipSatDate());
	                		    if (results != null) {
	                			    shutdownReached = (String) results.get("SHUTDOWN_LVL_REACHED");
	                			    invMap.put(pasVendorProductStateVO.getShipSatDate(), shutdownReached);
	                		    }
	                        }
	               			if (shutdownReached != null && shutdownReached.equalsIgnoreCase("Y")) {
	               			    pasVendorProductStateVO.setShipSatDate(null);
	               			    pasVendorProductStateVO.setShipSatCutoffTime("0000");
	                		}
	                	}
	                }
	                // Insert the row into PAS_VENDOR_PRODUCT_STATE_DT for
	               	getMaintDAO().insertPASVendorProductState(getConnection(), pasVendorProductStateVO);
	            }
            }
        }

    }

    /**
     * Get a list of all the states that allow for drop ship.
     * @param stateCode State to filter by, optional
     * @return
     */
    protected List<StateMasterVO> getDropShipStates(String stateCode)
    {
        if (dropShipStates == null)
        {
            List<StateMasterVO> stateMasterList = null;
            try 
            {
                stateMasterList = getStatesHandler().getStateMasterList();
            } catch (Exception ce)
            {
                throw new RuntimeException(ce);
            }
            List<StateMasterVO> stateList = new ArrayList<StateMasterVO>();
    
            Iterator<StateMasterVO> iterator = stateMasterList.iterator();
            while (iterator.hasNext())
            {
                StateMasterVO state = (StateMasterVO)iterator.next();
                if (stateCode == null || stateCode.equals(state.getStateMasterId()))
                {
                    if (state.getDropShipAvailableFlag().equals(YES))
                    {
                        stateList.add(state);
                    }
                }
            }
            dropShipStates = stateList;
        }

        return dropShipStates;
    }

    /**
     * Get the countryId for the given state.  Assume if the country is
     * null it is US.  This returns the 3 char countryId
     * @param stateVO
     * @return
     */
    protected String getCountryIdForState(StateMasterVO stateVO)
    {
        String countryId = stateVO.getCountryCode();
        if (countryId == null)
        {
            countryId = "US";
        }

        return countryId;
    }

    /**
     * Calculate the number of transit days for each type of shipping.  The results
     * are put back in the VendorProductStateDetailVO object.  Addon days are added to
     * the transit days.
     * @param vo The current vendor product state details
     * @param productVO The current product
     * @return
     */
    protected void calculateTransitDays(VendorProductStateDetailVO vo, PASProductVO productVO)
    {
        
        if (vo.isGroundShipAvailable())
        {
            // If the product has transit days for it, then use that for ground.
            if (productVO.getTransitDays() > 0)
            {
                vo.setGroundTransitDays(productVO.getTransitDays().intValue() + productVO.getAddonDays().intValue());
            }
            else
            {
                
                vo.setGroundTransitDays(getShipMethodGR().getMaxTransitDays().intValue() + productVO.getAddonDays().intValue());
            }
        }
        // 2 days for two day
        if (vo.isTwoDayShipAvailable())
        {
            vo.setTwoDayTransitDays(getShipMethod2D().getMaxTransitDays().intValue() + productVO.getAddonDays().intValue());
        }
        // 1 day for next day
        if (vo.isNextDayShipAvailable())
        {
            vo.setNextDayTransitDays(getShipMethodND().getMaxTransitDays().intValue() + productVO.getAddonDays().intValue());
        }
        // 1 or 2 days for Sat (ND or 2D available)
        if (vo.isSaturdayDeliveryAvailable())
        {
            if (vo.isNextDayShipAvailable())
            {
                vo.setSaturdayTransitDays(getShipMethodND().getMaxTransitDays().intValue() + productVO.getAddonDays().intValue());
            }
            else if (vo.isTwoDayShipAvailable())
            {
                vo.setSaturdayTransitDays(getShipMethod2D().getMaxTransitDays().intValue() + productVO.getAddonDays().intValue());
            }
        }

        
        if (logger.isDebugEnabled()) {
            logger.debug("Transit Days are " + 
                     vo.getNextDayTransitDays() + " " +
                     vo.getTwoDayTransitDays() + " " +
                     vo.getGroundTransitDays() + " " +
                     vo.getSaturdayTransitDays() 
                     );
        }
        
    }
    
    /**
     * Calculate the ship dates for each of the ship methods.  Only the methods that are
     * available will be calculated.  The results of the calculation will be put into
     * the PASVendorProductStateVO object.
     * @param detailVO 
     * @param pasVO Results of the calculation will be stored in this object
     * @return
     */
    protected void calculateShipDates(VendorProductStateDetailVO detailVO, PASVendorProductStateVO pasVO, String countryId, String baseProductId)
            throws Exception
    {
        if (logger.isDebugEnabled()) {
            logger.debug("Calculating ship Dates");
        }

        if (detailVO.isNextDayShipAvailable() && !isNextDayExcludedState(pasVO))        
        {
            pasVO.setShipNDDate(calculateShipDate(detailVO.getNextDayTransitDays(),detailVO.getVendorId(),countryId,baseProductId,false, "ND", detailVO));
            pasVO.setShipNDCutoffTime(calcCutoffTimeForShipDate(pasVO.getShipNDDate(),baseProductId, pasVO.getVendorId()));
        }
        
        if (detailVO.isTwoDayShipAvailable())
        {
            pasVO.setShip2DDate(calculateShipDate(detailVO.getTwoDayTransitDays(),detailVO.getVendorId(),countryId,baseProductId,false, "2F", detailVO));
            pasVO.setShip2DCutoffTime(calcCutoffTimeForShipDate(pasVO.getShip2DDate(),baseProductId, pasVO.getVendorId()));
        }

        if (detailVO.isGroundShipAvailable() && !isGroundExcludedState(pasVO))
        {
            pasVO.setShipGRDate(calculateShipDate(detailVO.getGroundTransitDays(),detailVO.getVendorId(),countryId,baseProductId,false, "GR", detailVO));
            pasVO.setShipGRCutoffTime(calcCutoffTimeForShipDate(pasVO.getShipGRDate(),baseProductId, pasVO.getVendorId()));
        }

    }

    /**
     * Calculate the ship date for saturday.  The results of the calculation will be put into
     * the PASVendorProductStateVO object.
     * @param detailVO 
     * @param pasVO Results of the calculation will be stored in this object
     * @return
     */
    protected void calculateSaturdayShipDate(VendorProductStateDetailVO detailVO, PASVendorProductStateVO pasVO, String countryId, String baseProductId)
            throws Exception
    {
        if (logger.isDebugEnabled()) {
            logger.debug("Calculating Saturday delivery ship date");
        }
        
        pasVO.setShipSatDate(calculateShipDate(detailVO.getSaturdayTransitDays(),detailVO.getVendorId(),countryId,baseProductId,true, "SA", detailVO));
        pasVO.setShipSatCutoffTime(calcCutoffTimeForShipDate(pasVO.getShipSatDate(),baseProductId, pasVO.getVendorId()));
        
    }

    /**
     * Calculate the ship date based on the number of transit days.
     * @param transitDays
     * @return shipDate
     */
    protected Date calculateShipDate(int transitDays, String vendorId, String countryId, String productId,
                                     boolean allowRestricted, String shipMethod, VendorProductStateDetailVO detailVO)
            throws Exception
    {
        if (logger.isDebugEnabled()) {
            logger.debug("Calculating ship date for transit days of " + transitDays + " product " + productId);
        }
        Calendar shipCalendar = (Calendar) deliveryDateCalendar.clone();
        
        // Calculate the ship date
        Date shipDate = null;
        Date shipDateCurrent = null;
        
        do
        {
            shipCalendar.add(Calendar.DAY_OF_YEAR, -1);
            shipDateCurrent = shipCalendar.getTime();
            // Get Country for the day
            PASCountryVO pasCountryVO = getPASCountry(countryId, shipDateCurrent, shipMethod);
            
            if (pasCountryVO == null)
            {
                // This shouldn't happen, but it might.  This means we should puke out of this
                break;
            }
            
        	if (pasCountryVO.getHolidayFlag() != null && pasCountryVO.getHolidayFlag().equalsIgnoreCase("Y") && shipMethod != null &&
        			(shipMethod.equalsIgnoreCase("ND") || shipMethod.equalsIgnoreCase("2F") || shipMethod.equalsIgnoreCase("SA"))) {
        		boolean twoDayAllowed = detailVO.isTwoDayShipAvailable();
        		boolean groundAllowed = detailVO.isGroundShipAvailable();
        		if (((shipMethod.equalsIgnoreCase("ND") || shipMethod.equalsIgnoreCase("SA")) && !twoDayAllowed) || 
        				(shipMethod.equalsIgnoreCase("2F") && !groundAllowed)) {
        		    return null;
        		}
        	}
            if (pasCountryVO.getTransitAllowed().equals(YES))
            {
                transitDays--;
            }
            
        } while (transitDays > 0);
        
        // We have a ship date, now make sure it is a valid ship date
        // Get Vendor for the ship day
        PASVendorVO pasVendorVO = getPASVendor(vendorId, shipDateCurrent);
        // Get Product for the ship day
        PASProductVO pasProductVO = getPASProduct(productId, shipDateCurrent);
        // Get Country for the day
        PASCountryVO pasCountryVO = getPASCountry(countryId, shipDateCurrent, shipMethod);
        if (pasVendorVO == null || pasCountryVO == null || pasProductVO == null)
        {
            // Ahhh, puke, no data to check
            if (logger.isDebugEnabled()) {
                logger.debug("vendor " + pasVendorVO + " country" + pasCountryVO + " product " + pasProductVO);
            }
        }
        else
        {
            // check for valid ship date
            if (pasVendorVO.getShipAllowed().equals(YES) && 
                pasCountryVO.getShippingAllowed().equals(YES))
            {
                
                if (pasProductVO.getShipAllowed().equals(YES) &&
                    ((pasProductVO.getShipRestricted().equals(NO)) ||
                    (pasProductVO.getShipRestricted().equals(YES) && allowRestricted)))
                {
                    shipDate = shipCalendar.getTime();
                }
            }
        }
        
        
        if (logger.isDebugEnabled()) {
            logger.debug("Ship Date Working is " + shipDateCurrent + " Ship Date is " + shipDate);
        }
        
        return shipDate;
    }
    
    /**
     * Get the cutoff time for the ship date.
     * @param shipDate Date for the cutoff
     * @param productId Product for cutoff time
     * @param vendorId Vendor for cutoff time
     * @return
     */
    protected String calcCutoffTimeForShipDate(Date shipDate, String productId, String vendorId)
            throws Exception
    {
        if (shipDate == null)
        {
            return "0000";    
        }
        
        PASVendorVO pasVendor = getPASVendor(vendorId, shipDate);
        PASProductVO pasProduct = getPASProduct(productId, shipDate);        
        
        String cutoffTime;
        if (pasVendor.getCutoffTime().compareTo(pasProduct.getCutoffTime()) < 0)
        {
            cutoffTime = pasVendor.getCutoffTime();
        }
        else
        {
            cutoffTime = pasProduct.getCutoffTime();
        }
        
        return cutoffTime;
    }
    
    /**
     * Get the ship method from the cache.
     * @return
     */
    protected ShipMethodVO getShipMethodND()
    {
        return getShipMethod(PASConstants.SHIP_METHOD_ND);
    }
    /**
     * Get the ship method from the cache.
     * @return
     */
    protected ShipMethodVO getShipMethod2D()
    {
        return getShipMethod(PASConstants.SHIP_METHOD_2D);
    }
    /**
     * Get the ship method from the cache.
     * @return
     */
    protected ShipMethodVO getShipMethodSA()
    {
        return getShipMethod(PASConstants.SHIP_METHOD_SA);
    }
    /**
     * Get the ship method from the cache.
     * @return
     */
    protected ShipMethodVO getShipMethodGR()
    {
        return getShipMethod(PASConstants.SHIP_METHOD_GR);
    }
    
    /**
     * Get the ship method from the cache.
     * @param shipMethod
     * @return
     */
    protected ShipMethodVO getShipMethod(String shipMethod)
    {
        ShipMethodHandler shipMethodHandler = (ShipMethodHandler) CacheManager.getInstance().getHandler("CACHE_NAME_SHIP_METHODS");
        
        try
        {
            ShipMethodVO shipMethodVO = shipMethodHandler.getShipMethodById(shipMethod);
            return shipMethodVO;
        } catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }
    
    /**
     * Get a country from the cache.  If the country is not found in the cache, it is 
     * retrieved from the database.  This case is local to the command.
     * @param countryId
     * @param deliveryDate
     * @return
     * @throws Exception
     */
    protected PASCountryVO getPASCountry(String countryId, Date deliveryDate, String shipMethod) throws Exception
    {
    	String mapKey = countryId + "*" + getSDF().format(deliveryDate) + "*" + shipMethod;
        Map dateMap = pasCountryDateMap.get(mapKey);

        if (dateMap == null)
        {
            dateMap = new HashMap();
            pasCountryDateMap.put(mapKey,dateMap);            
        }
        
        PASCountryVO vo = (PASCountryVO) dateMap.get(mapKey);
        if (vo == null)
        {
            vo = getQueryDAO().getPASCountry(getConnection(), countryId, deliveryDate);
            dateMap.put(mapKey,vo);
        }
            
        return vo;
    }

    /**
     * Get a vendor from the cache.  If the vendor is not found in the cache, it is 
     * retrieved from the database.  This case is local to the command.
     * @param vendorId
     * @param deliveryDate
     * @return
     * @throws Exception
     */
    protected PASVendorVO getPASVendor(String vendorId, Date deliveryDate) throws Exception
    {
        Map dateMap = pasVendorDateMap.get(vendorId);

        if (dateMap == null)
        {
            dateMap = new HashMap();
            pasVendorDateMap.put(vendorId,dateMap);            
        }
        
        PASVendorVO vo = (PASVendorVO) dateMap.get(deliveryDate);
        if (vo == null)
        {
            List<PASVendorVO> voList = getQueryDAO().getPASVendor(getConnection(),vendorId, deliveryDate);
            if (voList.size() > 0)
            {
                vo = voList.get(0);
                dateMap.put(deliveryDate,vo); 
            }
        }
            
        return vo;
    }
  
    /**
     * Get a product from the cache.  If the product is not found in the cache, it is 
     * retrieved from the database.  This case is local to the command.
     * @param productId
     * @param deliveryDate
     * @return
     * @throws Exception
     */
    protected PASProductVO getPASProduct(String productId, Date deliveryDate) throws Exception
    {
        Map dateMap = pasProductDateMap.get(productId);

        if (dateMap == null)
        {
            dateMap = new HashMap();
            pasProductDateMap.put(productId,dateMap);            
        }
        
        PASProductVO vo = (PASProductVO) dateMap.get(deliveryDate);
        if (vo == null)
        {
            vo = getQueryDAO().getPASProduct(getConnection(), productId, deliveryDate);
            dateMap.put(deliveryDate,vo); 
        }
            
        return vo;
    }


    /**
     * Send a JMS command to process each vendor.  This is called
     * when a request is made to process all vendors.
     * @throws Exception
     */
    protected void sendVendorCommands() throws Exception
    {
        String productId = this.productId;
        String stateCode = this.stateCode;
        if (productId == null)
        {
            productId = ALL;
        }
        if (stateCode == null)
        {
            stateCode = ALL;
        }
                                  
        // Get the list of vendors
        List<PASVendorVO> vendorList = getQueryDAO().getAllPASVendor(getConnection(), deliveryDate);
        for (int i=0; i < vendorList.size(); i++)
        {
            PASVendorVO vo = vendorList.get(i);
            List<String> args = new ArrayList<String>();
            args.add(getSDF().format(deliveryDate));
            args.add(vo.getVendorId());
            args.add(productId);
            args.add(stateCode);
            if (isPerformInvalidation())
            {
                args.add(PROCESS_INVALIDATION);
            }
            Date today = getSDF().parse(getSDF().format(new Date()));
            int priority = (int)( (deliveryDate.getTime() - today.getTime()) / (1000 * 60 * 60 * 24) );
            if (priority < 0) priority = 0;
            logger.info(getSDF().format(deliveryDate) + " priority: " + priority);
            if (priority < 0) priority = 0;
            if(vo != null && !vo.getVendorType().equalsIgnoreCase("FTD WEST")){
            	spawnNewJMSCommand(COMMAND_ADD_VENDOR_PRODUCT_STATE_DATE, args, priority);
            }
            else{
            	logger.info("Skipping COMMAND_ADD_VENDOR_PRODUCT_STATE_DATE");
            };
        }
        
    }
    
    /**
     * Check to see if the state is excluded from Next Day delivery.
     * @param vo
     * @return
     */
    protected boolean isNextDayExcludedState(PASVendorProductStateVO vo)
    {
        if (vo.getStateCode().equals("HI") || vo.getStateCode().equals("AK"))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Check to see if the state is excluded from ground delivery.
     * @param vo
     * @return
     */
    protected boolean isGroundExcludedState(PASVendorProductStateVO vo)
    {
        if (vo.getStateCode().equals("HI") || vo.getStateCode().equals("AK"))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    /**
     * Check to see if the state is excluded from saturday delivery.
     * @param vo
     * @return
     */
    protected boolean isSaturdayExcludedState(PASVendorProductStateVO vo)
    {
        if (vo.getStateCode().equals("HI") || vo.getStateCode().equals("AK"))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
