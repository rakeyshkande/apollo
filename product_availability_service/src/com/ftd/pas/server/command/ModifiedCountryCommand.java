package com.ftd.pas.server.command;

import com.ftd.osp.utilities.cacheMgr.handlers.vo.CountryMasterVO;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pas.server.exception.InvalidPASCommandArgumentException;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Command for processing the modification to a country
 * in country_master.  Loops through all the dates in
 * product availability and changes each.
 */
public class ModifiedCountryCommand extends AbstractDateLoopingCommand
{
    protected Logger logger = new Logger(this.getClass().getName());

    protected String countryId;
    
    /**
     * Create a new object.
     */
    public ModifiedCountryCommand()
    {
        super();
    }

    /**
     * Set the list of arguments.
     * expected is:
     * CountryId 
     * @param arguments
     * @throws InvalidPASCommandArgumentException if the arguments are not correct.
     */
    public void setArguments(List<String> arguments) throws InvalidPASCommandArgumentException

    {
        super.setArguments(arguments);

        if (arguments.size() < 1 || arguments.size() > 2)
        {
            throw new InvalidPASCommandArgumentException("Wrong number of Arguments, need 1 or 2 got " + arguments.size());
        }

        // Should have one argument, a delivery Date.
        countryId = arguments.get(0);
        
        if (arguments.size() == 2) {
            String deliveryDateString = arguments.get(1);
            logger.info("deliveryDate: " + deliveryDateString);
            try {
                setDeliveryDate(deliveryDateString);
            } catch (ParseException pe) {
                logger.error("Invalid Delivery Date " + deliveryDateString, pe);
                throw new InvalidPASCommandArgumentException("Invalid Delivery Date");
            }
        }
    }

    /**
     * Override the method to force an update of the cache before perfomring the looping.
     * @throws Exception
     */
    protected void executeCommand() throws Exception
    {
        CacheManager.getInstance().getHandler("CACHE_NAME_COUNTRY_MASTER", true);
    	if (deliveryDate == null) {
            int maxDays = getMaxDays();

            Calendar processDateCalendar = Calendar.getInstance();
            for (int i=0; i <= maxDays; i++) {
                executeSingle(processDateCalendar.getTime());
                processDateCalendar.add(Calendar.DAY_OF_YEAR,1);
            }
        } else {
    		executeSingle(deliveryDate);
    	}
    }



    /**
     * Execute the command.  Since the country has changed it could affect availability
     * for all products in the country.  Need to do a mass update of all the country
     * products for all dates.
     */
    protected void executeSingleUnit(Date processDate) throws Exception
    {
        logger.debug("execute called");
        
        logger.debug("Processing " + processDate);
        getMaintDAO().invalidatePASCountry(getConnection(),processDate,countryId);
        getMaintDAO().invalidatePASCountryProduct(getConnection(),processDate,countryId,ALL);
        addCountry(processDate, countryId);
        
        CountryMasterVO country = getCountryHandler().getCountryById(countryId);
        
        if (country.getCountryType().equals("I"))
        {
            addCountryProduct(processDate, countryId, ALL);
        }
        else
        {
            if (countryId.equals("US"))
            {
                addVendorProductState(processDate, ALL, ALL, ALL, false);
            }
        }
        
    }


}
