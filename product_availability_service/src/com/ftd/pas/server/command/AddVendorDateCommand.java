package com.ftd.pas.server.command;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pas.server.constants.PASConstants;
import com.ftd.pas.server.exception.InvalidPASCommandArgumentException;
import com.ftd.pas.server.vo.PASVendorVO;
import com.ftd.pas.server.vo.VendorDetailVO;

import java.text.ParseException;

import java.util.Calendar;
import java.util.Date;
import java.util.List;


/**
 * Command for processing the adding of a new vendor for a delivery date to the
 * product availability.  All of the processing needed to add a new date is performed.
 */
public class AddVendorDateCommand extends AbstractLoopingCommand
{
    protected Logger logger = new Logger(this.getClass().getName());

    protected String vendorId;

    protected String lastestVendorCutoffTime;
    
    /**
     * Create a new AddProductDateCommand object.
     */
    public AddVendorDateCommand()
    {
        super();
    }

    /**
     * Set the list of arguments.
     * expected is:
     * DeliveryDate in MMddYYYY format
     * ProductId 
     * @param arguments
     * @throws InvalidPASCommandArgumentException if the arguments are not correct.
     */
    public void setArguments(List<String> arguments) throws InvalidPASCommandArgumentException

    {
        super.setArguments(arguments);

        if (arguments.size() != 2)
        {
            throw new InvalidPASCommandArgumentException("Wrong number of Arguments, need 2 got " + arguments.size());
        }

        // Should have first argument, a delivery Date.
        String deliveryDateString = arguments.get(0);
        try
        {
            setDeliveryDate(deliveryDateString);
        } catch (ParseException pe)
        {
            logger.error("Invalid Delivery Date " + deliveryDateString, pe);
            throw new InvalidPASCommandArgumentException("Invalid Delivery Date");
        }
        if (arguments.get(1).equals(ALL))
        {
            vendorId = null;
        }
        else
        {
            vendorId = arguments.get(1);
        }
    }

    /**
     * Set the arguments for the command.
     * @param deliveryDate
     * @param vendorId
     * @throws InvalidPASCommandArgumentException
     */
    public void setArguments(Date deliveryDate, String vendorId) throws InvalidPASCommandArgumentException
    {
        setDeliveryDate(deliveryDate);
        this.vendorId = vendorId;
    }

    /**
     * Set the arguments for the command.
     * @param deliveryDate
     * @param vendorDetailVO Must be a VendorDetailVO or exception is thrown
     * @throws InvalidPASCommandArgumentException
     */
    public void setArguments(Date deliveryDate, Object vendorDetailVO) throws InvalidPASCommandArgumentException
    {
        setDeliveryDate(deliveryDate);

        if (vendorDetailVO instanceof VendorDetailVO)
        {
            this.paramObject = vendorDetailVO;
        } else
        {
            throw new InvalidPASCommandArgumentException("Expecting VendorDetailVO, got " + vendorDetailVO.getClass().getName());
        }

    }

    
    /**
     * Get the list of vendors to process.
     * @return
     * @throws Exception
     */
    protected List getObjects() throws Exception
    {
        if (isPerformInvalidation())
        {
            getMaintDAO().invalidatePASVendor(getConnection(),deliveryDate,vendorId);
        }

        List<VendorDetailVO> details = getQueryDAO().getVendorDetails(getConnection(), vendorId, deliveryDate);
        lastestVendorCutoffTime = getConfigurationUtil().getFrpGlobalParm(PASConstants.SHIPPING_CONFIG_CONTEXT,PASConstants.LASTEST_VENDOR_CUTOFF_PARM);
        return details;        
    }
    
    /**
     * Execute a single unit of work for the command.
     * @param object VendorDetailVO object
     * @throws Exception
     */
    protected void executeSingleUnit(Object object) throws Exception
    {
        VendorDetailVO vendorDetailVO = (VendorDetailVO) object;
        
        // Build up the PASVendorVO
        PASVendorVO vendor = new PASVendorVO();
        vendor.setVendorId(vendorDetailVO.getVendorId());
        vendor.setDeliveryDate(deliveryDate);

        vendor.setCutoffTime(vendorDetailVO.getCutoffTime());

        if (lastestVendorCutoffTime.compareTo(vendor.getCutoffTime()) < 0)
        {
            vendor.setCutoffTime(lastestVendorCutoffTime);
        }
        
        String deliveryAvailable = YES;
        String shipAllowed = YES;
        
        // Check the day of the week for the product.  
        int dayOfWeek = deliveryDateCalendar.get(Calendar.DAY_OF_WEEK);
        switch (dayOfWeek)
        {
        case Calendar.SUNDAY:
            deliveryAvailable = NO;
            shipAllowed = NO;
            break;
        }
        if (vendorDetailVO.getDeliveryRestriction() != null && vendorDetailVO.getDeliveryRestriction().equals(YES))
        {
            deliveryAvailable = NO;
        }
        if (vendorDetailVO.getShippingRestriction() != null && vendorDetailVO.getShippingRestriction().equals(YES))
        {
            shipAllowed = NO;
        }

        vendor.setDeliveryAvailable(deliveryAvailable);
        vendor.setShipAllowed(shipAllowed);

        getMaintDAO().insertPASVendor(getConnection(), vendor);
    }
}
