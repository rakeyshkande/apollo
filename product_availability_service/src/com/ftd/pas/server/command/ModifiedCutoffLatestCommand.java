package com.ftd.pas.server.command;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pas.server.exception.InvalidPASCommandArgumentException;

import java.util.Date;
import java.util.List;

/**
 * Command for processing the modification to the latest cutoff.
 * Loops through all the dates in product availability and changes each.
 */
public class ModifiedCutoffLatestCommand extends AbstractDateLoopingCommand
{
    protected Logger logger = new Logger(this.getClass().getName());

    /**
     * Create a new object.
     */
    public ModifiedCutoffLatestCommand()
    {
        super();
    }

    /**
     * Set the list of arguments.
     * expected is:
     * None 
     * @param arguments
     * @throws InvalidPASCommandArgumentException if the arguments are not correct.
     */
    public void setArguments(List<String> arguments) throws InvalidPASCommandArgumentException

    {
        super.setArguments(arguments);

        if (arguments.size() != 0)
        {
            throw new InvalidPASCommandArgumentException("Wrong number of Arguments, need 0 got " + arguments.size());
        }


    }

    /**
     * Override the method to force an update of the cache before perfomring the looping.
     * @throws Exception
     */
    protected void executeCommand() throws Exception
    {
        refreshGlobalParmCache();
        super.executeCommand();
    }

    /**
     * Execute the command.  Since the exotic cutoff has changed, change all the products
     * that are exotic
     */
    protected void executeSingleUnit(Date processDate) throws Exception
    {
        logger.debug("Processing " + processDate);

        addFloristZipCode(processDate);

    }


}
