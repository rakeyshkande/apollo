package com.ftd.pas.server.command;

import com.ftd.osp.utilities.cacheMgr.handlers.vo.CountryMasterVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pas.server.constants.PASConstants;
import com.ftd.pas.server.exception.InvalidPASCommandArgumentException;
import com.ftd.pas.server.util.DateUtils;
import com.ftd.pas.server.vo.HolidayCountryVO;
import com.ftd.pas.server.vo.PASCountryVO;

import java.text.ParseException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


/**
 * Command for processing the adding of a new country for a delivery date to the
 * product availability.  All of the processing needed
 * to add a new date is performed.
 */
public class AddCountryDateCommand extends AbstractLoopingCommand
{
    protected Logger logger = new Logger(this.getClass().getName());
    
    protected String countryId;

    /**
     * Create a new AddDeliveyrDateCommand object.
     */
    public AddCountryDateCommand()
    {
        super();
    }

    /**
     * Set the list of arguments.
     * expected is:
     *   DeliveryDate in MMddYYYY format
     *   CountryId 
     * @param arguments
     * @throws InvalidPASCommandArgumentException if the arguments are not correct.
     */
    public void setArguments(List<String> arguments) throws InvalidPASCommandArgumentException

    {
        super.setArguments(arguments);

        if (arguments.size() != 2)
        {
            throw new InvalidPASCommandArgumentException("Wrong number of Arguments, need 2 got " + arguments.size());
        }

        // Should have first argument, a delivery Date.
        String deliveryDateString = arguments.get(0);
        try
        {
            setDeliveryDate(deliveryDateString);
        } catch (ParseException pe)
        {
            logger.error("Invalid Delivery Date " + deliveryDateString, pe);
            throw new InvalidPASCommandArgumentException("Invalid Delivery Date");
        }
        countryId = arguments.get(1);
        if (countryId != null && countryId.equals("ALL"))
        {
            countryId = null;
        }
    }

    /**
     * Set the arguments for the command.
     * @param deliveryDate 
     * @param countryId
     * @throws InvalidPASCommandArgumentException
     */
    public void setArguments(Date deliveryDate, String countryId)
            throws InvalidPASCommandArgumentException
    {
        setDeliveryDate(deliveryDate);
        this.countryId = countryId;
    }


    /**
     * Get the list of countries to loop on.
     * @return
     * @throws Exception
     */
    protected List getObjects() throws Exception
    {
        if (isPerformInvalidation())
        {
            getMaintDAO().invalidatePASCountry(getConnection(),deliveryDate,countryId);
        }

        List countries = null;    
        if (countryId == null)
        {
            countries = getCountryHandler().getCountryList();    
        }
        else
        {
            countries = new ArrayList();
            Object country = getCountryHandler().getCountryById(countryId);
            if (country != null)
            {
                countries.add(country);
            }
        }
        return countries;        
    }
        
    /**
     * Execute a single unit of work for the command.  Each unit of work
     * is a country and populates the PAS_COUNTRY_DT table.
     * @param object  The country id
     * @throws Exception
     */
    protected void executeSingleUnit(Object object) throws Exception
    {
        CountryMasterVO country = (CountryMasterVO) object;

        logger.debug("country = " + country.getCountryId() + " " + country.getSaturdayClosed() + " " +
                     country.getStatus() + " " + deliveryDate);
        
        // First check to see if the country is active.  Cruddy cache has inactive too
        if (!country.getStatus().equals("Active"))
        {
            return;
        }
        
        PASCountryVO pasCountryVO = new PASCountryVO();
        pasCountryVO.setDeliveryDate(deliveryDate);
        pasCountryVO.setCountryId(country.getCountryId());

        //CountryVO country = getCountryHandler().getCountryById(countryId);
        
        // Set the addon days from the country
        pasCountryVO.setAddonDays(country.getAddonDays());

        if (country.getCountryType().equals("I"))
        {
            String intlAddonDaysString = getConfigurationUtil().getFrpGlobalParm(PASConstants.FTDAPPS_CONFIG_CONTEXT, "INTL_OVERRIDE_DAYS");
            Long intAddonDays = Long.valueOf(intlAddonDaysString);
            if (intAddonDays > pasCountryVO.getAddonDays())
            {
                pasCountryVO.setAddonDays(intAddonDays);
            }
        }

        // Check the day of the week for the country.  
        int dayOfWeek = deliveryDateCalendar.get(Calendar.DAY_OF_WEEK);
        String countryDayOfWeekFlag = "N";
        String transitAllowed = "N";
        switch (dayOfWeek)
        {
            case Calendar.SUNDAY:
                countryDayOfWeekFlag = country.getSundayClosed();
                transitAllowed = country.getSundayTransit();
                break;
            case Calendar.MONDAY:
                countryDayOfWeekFlag = country.getMondayClosed();
                transitAllowed = country.getMondayTransit();
                break;
            case Calendar.TUESDAY:
                countryDayOfWeekFlag = country.getTuesdayClosed();
                transitAllowed = country.getTuesdayTransit();
                break;
            case Calendar.WEDNESDAY:
                countryDayOfWeekFlag = country.getWednesdayClosed();
                transitAllowed = country.getWednesdayTransit();
                break;
            case Calendar.THURSDAY:
                countryDayOfWeekFlag = country.getThursdayClosed();
                transitAllowed = country.getThursdayTransit();
                break;
            case Calendar.FRIDAY:
                countryDayOfWeekFlag = country.getFridayClosed();
                transitAllowed = country.getFridayTransit();
                break;
            case Calendar.SATURDAY:
                countryDayOfWeekFlag = country.getSaturdayClosed();
                transitAllowed = country.getSaturdayTransit();
                break;
        }
        logger.debug("countryDayOfWeekFlag = " + countryDayOfWeekFlag);
        if (countryDayOfWeekFlag.equals("N") || countryDayOfWeekFlag.equals("H"))
        {
            pasCountryVO.setDeliveryAvailable("Y");
            pasCountryVO.setShippingAllowed("Y");
        }
        else
        {
            pasCountryVO.setDeliveryAvailable("N");
            pasCountryVO.setShippingAllowed("N");
        }
        pasCountryVO.setTransitAllowed(transitAllowed);
        
        pasCountryVO.setCutoffTime(country.getCutoffTime());
        
        List<HolidayCountryVO> holidays = getQueryDAO().getCountryHolidays(getConnection(),country.getCountryId());
        logger.debug("Got " + holidays.size() + " holidays");
        
        // Check the list of holidays for any on the delivery date
        for (int i=0; i < holidays.size(); i++)
        {
            HolidayCountryVO holiday = holidays.get(i);

            logger.debug("Checking " + holiday.getHolidayDate());
            if (DateUtils.isSameDay(holiday.getHolidayDate(), deliveryDateCalendar))
            {
                logger.debug("They are the same day");
                if (!holiday.getDeliverableFlag().equals("Y")) 
                {
                    pasCountryVO.setDeliveryAvailable(holiday.getDeliverableFlag());
                }
                if (!holiday.getShippingAllowed().equals("Y")) 
                {
                    pasCountryVO.setShippingAllowed(holiday.getShippingAllowed());
                }
                pasCountryVO.setTransitAllowed("N");
            }
        }

        // Insert the row into PAS_COUNTRY_DT
        getMaintDAO().insertPASCountry(getConnection(), pasCountryVO);

    }
}
