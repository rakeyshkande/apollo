package com.ftd.pas.server.command;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pas.server.exception.InvalidPASCommandArgumentException;

import com.ftd.pas.server.vo.PASFloristZipcodeVO;

import java.util.Date;
import java.util.List;


/**
 * Command for processing the adding of a new florist for a delivery date to the
 * product availability.  All of the processing needed to add a new date is performed.
 */
public class RecalcFloristZipCommand extends AbstractLoopingCommand
{
    protected Logger logger = new Logger(this.getClass().getName());

    protected String zipCode;
    protected Date processDate;

    /**
     * Create a new AddProductDateCommand object.
     */
    public RecalcFloristZipCommand()
    {
        super();
    }

    /**
     * Set the list of arguments.
     * expected is:
     * DeliveryDate in MMddYYYY format
     * FloristId
     * @param arguments
     * @throws InvalidPASCommandArgumentException if the arguments are not correct.
     */
    public void setArguments(List<String> arguments) throws InvalidPASCommandArgumentException

    {
        super.setArguments(arguments);

        if (arguments.size() != 2)
        {
            throw new InvalidPASCommandArgumentException("Wrong number of Arguments, need 2 got " + arguments.size());
        }

        // Should have first argument, a delivery Date.
        zipCode = arguments.get(0);
        String arg1 = arguments.get(1);
        try {
            processDate = getSDF().parse(arg1);
        } catch (Exception e) {
            logger.error("parse exception: " + e);
        }

    }

    /**
     * Get the list of objects to loop through.  Setup a couple objects that won't change
     * during the process.
     * @return
     * @throws Exception
     */
    protected List getObjects() throws Exception
    {
        List<PASFloristZipcodeVO> details = getQueryDAO().getFloristZipsByZip(getConnection(), zipCode, processDate);
        return details;
    }

    /**
     * Build a single zip code object to insert into PAS_ZIPCODE_DT.
     * @param object PASFloristZipcodeVO object
     * @throws Exception
     */
    protected void executeSingleUnit(Object object) throws Exception
    {
        PASFloristZipcodeVO zipVO = (PASFloristZipcodeVO) object;
        logger.info(zipVO.getZipCode() + " " + getSDF().format(processDate) + " " + zipVO.getStatusCode() + " " +
                zipVO.getCutoffTime() + " " + zipVO.getFitStatusCode());
        getMaintDAO().insertPASFloristZipcode(getConnection(), zipVO);
    }
}
