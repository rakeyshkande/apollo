package com.ftd.pas.server.command;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pas.server.exception.InvalidPASCommandArgumentException;

import java.text.ParseException;

import java.util.Date;
import java.util.List;


/**
 * Command for processing the adding of a new florist for a delivery date to the
 * product availability.  All of the processing needed to add a new date is performed.
 */
public class AddFloristZipcodeDateCommand extends AbstractLoopingCommand
{
    protected Logger logger = new Logger(this.getClass().getName());

    protected String zipCode;


    /**
     * Create a new AddFloristZipcodeDateCommand object.
     */
    public AddFloristZipcodeDateCommand()
    {
        super();
    }

    /**
     * Set the list of arguments.
     * expected is:
     * DeliveryDate in MMddYYYY format
     * zipCode
     * @param arguments
     * @throws InvalidPASCommandArgumentException if the arguments are not correct.
     */
    public void setArguments(List<String> arguments) throws InvalidPASCommandArgumentException

    {
        super.setArguments(arguments);

        if (arguments.size() != 2)
        {
            throw new InvalidPASCommandArgumentException("Wrong number of Arguments, need 2 got " + arguments.size());
        }

        // Should have first argument, a delivery Date.
        String deliveryDateString = arguments.get(0);
        try
        {
            setDeliveryDate(deliveryDateString);
        } catch (ParseException pe)
        {
            logger.error("Invalid Delivery Date " + deliveryDateString, pe);
            throw new InvalidPASCommandArgumentException("Invalid Delivery Date");
        }
        if (arguments.get(1).equals(ALL))
        {
            zipCode = null;
        }
        else
        {
            zipCode = arguments.get(1);
        }

    }

    /**
     * Set the arguments for the command.
     * @param deliveryDate
     * @param zipCode
     * @throws InvalidPASCommandArgumentException
     */
    public void setArguments(Date deliveryDate, String zipCode) throws InvalidPASCommandArgumentException
    {
        setDeliveryDate(deliveryDate);
        this.zipCode = zipCode;
    }

    /**
     * Get the list of objects to loop through.  Setup a couple objects that won't change
     * during the process.
     * @return
     * @throws Exception
     */
    protected List getObjects() throws Exception
    {
        List details = getQueryDAO().getAllFloristZips(getConnection(), zipCode);
        return details;
    }

    /**
     * Build a single florist object to insert into PAS_FLORIST_DT.
     * @param object zipCode
     * @throws Exception
     */
    protected void executeSingleUnit(Object object) throws Exception
    {
        String zipCode = (String) object;
        logger.info("executeSingleUnit() - zipCode: " + zipCode + " deliveryDate: " + getSDF().format(deliveryDate));
        recalcFloristZipCode(zipCode, deliveryDate);
    }
}
