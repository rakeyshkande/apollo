package com.ftd.pas.server.command;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pas.server.exception.InvalidPASCommandArgumentException;

import com.ftd.pas.server.vo.PASTimeZoneVO;

import java.text.ParseException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * Command for processing the adding of a time zone for a delivery date to the
 * product availability.  All of the processing needed to add a new date is performed.
 */
public class AddTimeZoneDateCommand extends AbstractCommand
{
    protected Logger logger = new Logger(this.getClass().getName());

    protected String floristId;


    /**
     * Create a new AddProductDateCommand object.
     */
    public AddTimeZoneDateCommand()
    {
        super();
    }

    /**
     * Set the list of arguments.
     * expected is:
     * DeliveryDate in MMddYYYY format
     * @param arguments
     * @throws InvalidPASCommandArgumentException if the arguments are not correct.
     */
    public void setArguments(List<String> arguments) throws InvalidPASCommandArgumentException

    {
        super.setArguments(arguments);

        if (arguments.size() != 1)
        {
            throw new InvalidPASCommandArgumentException("Wrong number of Arguments, need 1 got " + arguments.size());
        }

        // Should have first argument, a delivery Date.
        String deliveryDateString = arguments.get(0);
        try
        {
            setDeliveryDate(deliveryDateString);
        }
        catch (ParseException pe)
        {
            logger.error("Invalid Delivery Date " + deliveryDateString, pe);
            throw new InvalidPASCommandArgumentException("Invalid Delivery Date");
        }

    }

    /**
     * Execute the command.  Put a row in PAS_TME_ZONE_DT for every
     * time zone listed in state_master with a delta to CST for the day.
     * @throws Exception
     */
    protected void executeCommand() throws Exception
    {
        
        List<PASTimeZoneVO> zones = getTimeZones();
        
        calculateOffsets(zones);
        
        saveZones(zones);
            
    }

    /**
     * Get a list of all the time zones to process.  
     * @return
     */
    protected List<PASTimeZoneVO> getTimeZones()
    {
        List<PASTimeZoneVO> list = new ArrayList<PASTimeZoneVO>();
        
        for (int i=1; i <= 6; i ++)
        {
            PASTimeZoneVO vo = new PASTimeZoneVO();
            vo.setTimeZoneCode(i);
            vo.setDeliveryDate(deliveryDate);
            vo.setDaylightSavingsFlag(YES);
            list.add(vo);
        }
        
        return list;
    }
    
    /**
     * Calculate the offsets from the times zones to central.
     * @param zones
     */
    protected void calculateOffsets(List<PASTimeZoneVO> zones)
    {
        Calendar cstCalendar = Calendar.getInstance();
        Date effectiveDate = cstCalendar.getTime();
        long time = effectiveDate.getTime();

        int cstHoursOffset = getHoursOffset("America/Chicago",time);

        for (int i=0; i < zones.size();i++)
        {
            calculateZoneOffset(zones.get(i), cstHoursOffset, time);
        }
    }
    
    /**
     * Calculate the offset time from central time for this time zone.
     * @param vo
     * @param cstHoursOffset
     * @param time
     */
    protected void calculateZoneOffset(PASTimeZoneVO vo, int cstHoursOffset, long time)
    {
        int hoursOffset = 0;
        if (vo.getTimeZoneCode() == 1)
        {
            hoursOffset = getHoursOffset("America/New_York",time);
        }
        if (vo.getTimeZoneCode() == 2)
        {
            hoursOffset = getHoursOffset("America/Chicago",time);
        }
        if (vo.getTimeZoneCode() == 3)
        {
            hoursOffset = getHoursOffset("America/Denver",time);
        }
        if (vo.getTimeZoneCode() == 4)
        {
            hoursOffset = getHoursOffset("America/Los_Angeles",time);
        }
        if (vo.getTimeZoneCode() == 5)
        {
            hoursOffset = getHoursOffset("US/Alaska",time);
        }
        if (vo.getTimeZoneCode() == 6)
        {
            hoursOffset = getHoursOffset("Pacific/Honolulu",time);
        }

        int difference = cstHoursOffset - hoursOffset;
        vo.setCstDelta(difference);
    }

    /**
     * Get the number of hours difference between the time zone and the time
     * passed in.
     * @param timeZoneName
     * @param time
     * @return offset represented as hours and two zeros
     */
    protected int getHoursOffset(String timeZoneName, long time)
    {
        TimeZone timeZone = TimeZone.getTimeZone(timeZoneName);

        int millisecondsOffset = timeZone.getOffset(time);
        int hoursOffset = millisecondsOffset / (10 * 60 * 60);
        
        return hoursOffset;
    }
    
    /**
     * Save the data to the database.
     * @param zones
     */
    protected void saveZones(List<PASTimeZoneVO> zones) throws Exception
    {
        for (int i=0; i < zones.size();i++)
        {
            PASTimeZoneVO vo = zones.get(i);
            getMaintDAO().insertPASTimeZone(getConnection(),vo);
            logger.debug("PASTimeZone " + vo.getDeliveryDate() + " " + vo.getTimeZoneCode() + " " + vo.getCstDelta());
        }
    }
}
