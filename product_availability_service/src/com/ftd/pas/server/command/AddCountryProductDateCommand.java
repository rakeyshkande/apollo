package com.ftd.pas.server.command;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pas.server.constants.PASConstants;
import com.ftd.pas.server.exception.InvalidPASCommandArgumentException;
import com.ftd.pas.server.vo.CountryProductDetailVO;
import com.ftd.pas.server.vo.PASCountryProductVO;

import com.ftd.pas.server.vo.PASCountryVO;

import java.text.ParseException;

import java.util.Calendar;
import java.util.Date;
import java.util.List;


/**
 * Command for processing the adding of a new country's products for a delivery date to the
 * product availability.  All of the processing needed
 * to add a new date is performed.
 */
public class AddCountryProductDateCommand extends AbstractLoopingCommand
{
    protected Logger logger = new Logger(this.getClass().getName());

    protected String countryId;
    protected String productId;
    
    protected Integer internationalAddonDays;
    
    /**
     * Create a new AddDeliveyrDateCommand object.
     */
    public AddCountryProductDateCommand()
    {
        super();
    }

    /**
     * Set the list of arguments.
     * expected is:
     * DeliveryDate in MMddYYYY format
     * CountryId (ALL means all)
     * ProductId (ALL means all)
     * @param arguments
     * @throws InvalidPASCommandArgumentException if the arguments are not correct.
     */
    public void setArguments(List<String> arguments) throws InvalidPASCommandArgumentException

    {
        super.setArguments(arguments);

        if (arguments.size() != 3)
        {
            throw new InvalidPASCommandArgumentException("Wrong number of Arguments, need 3 got " + arguments.size());
        }

        // Should have first argument, a delivery Date.
        String deliveryDateString = arguments.get(0);
        try
        {
            setDeliveryDate(deliveryDateString);
        } catch (ParseException pe)
        {
            logger.error("Invalid Delivery Date " + deliveryDateString, pe);
            throw new InvalidPASCommandArgumentException("Invalid Delivery Date");
        }
        
        // Convert ALL to a null argument
        String argument = arguments.get(1);
        if (argument.equals(ALL))
        {
            countryId = null;
        }
        else
        {
            countryId = argument;
        }
        argument = arguments.get(2);
        if (argument.equals(ALL))
        {
            productId = null;
        }
        else
        {
            productId = argument;
        }
    }

    /**
     * Set the arguments for the command.
     * @param deliveryDate
     * @param countryId country id, null means all
     * @param productId product id, null means all
     * @throws InvalidPASCommandArgumentException
     */
    public void setArguments(Date deliveryDate, String countryId, String productId) throws InvalidPASCommandArgumentException
    {
        setDeliveryDate(deliveryDate);
        this.countryId = countryId;
        this.productId = productId;
    }

    /**
     * Set the arguments for the command.
     * @param deliveryDate
     * @param countryProductDetailVO Must be a CountryProductDetailVO or exception is thrown
     * @throws InvalidPASCommandArgumentException
     */
    public void setArguments(Date deliveryDate, Object countryProductDetailVO) throws InvalidPASCommandArgumentException
    {
        setDeliveryDate(deliveryDate);
        
        if (countryProductDetailVO instanceof CountryProductDetailVO)
        {
            this.paramObject = countryProductDetailVO;
        }
        else
        {
            throw new InvalidPASCommandArgumentException("Expecting CountryProductDetailVO, got " + countryProductDetailVO.getClass().getName());
        }
        
    }

    
    /**
     * Get the list of country products to process.
     * @return
     * @throws Exception
     */
    protected List getObjects() throws Exception
    {
        if (isPerformInvalidation())
        {
            getMaintDAO().invalidatePASCountryProduct(getConnection(),deliveryDate,countryId,productId);
        }
        List<CountryProductDetailVO> details = getQueryDAO().getCountryProductDetails(getConnection(),countryId,productId);
        return details;        
    }

    /**
     * Process a single country product combination and insert into PAS_COUNTRY_PRODUCT_DT.
     * @param param
     * @throws Exception
     */
    protected void executeSingleUnit(Object param) throws Exception
    {
        CountryProductDetailVO details = (CountryProductDetailVO) param;

        logger.debug("Processing " + details.getCountryId() + " " + details.getProductId());
        PASCountryProductVO pasCountryProductVO = new PASCountryProductVO();
        pasCountryProductVO.setDeliveryDate(deliveryDate);
        pasCountryProductVO.setCountryId(details.getCountryId());
        pasCountryProductVO.setProductId(details.getProductId());

        // First check if delivery is at all available on this day.
        // Check the PAS_COUNTRY to see if delivery is available
        PASCountryVO pasCountryVO = getQueryDAO().getPASCountry(getConnection(),
                                                                pasCountryProductVO.getCountryId(),
                                                                deliveryDate);
        if (pasCountryVO == null || pasCountryVO.getDeliveryAvailable().equals(NO))
        {
            logger.debug("pasCountryVO is null or flag is NO " + pasCountryVO);
            pasCountryProductVO.setCutoffDate(deliveryDate);
            pasCountryProductVO.setCutoffTime("0000");
            pasCountryProductVO.setDeliveryAvailable(NO);
        }
        else
        {
            int addonDays = (int) pasCountryVO.getAddonDays();
            if (getIntAddonDays() > addonDays)
            {
                addonDays = getIntAddonDays();
            }
            
            // Switch addonDays to be negative
            addonDays *= -1;

            logger.debug("Addon days is " + addonDays);            
            // Start with the delivery date
            Calendar cutoffDateCalendar = (Calendar) deliveryDateCalendar.clone();
            // Subtract the addonDays
            cutoffDateCalendar.add(Calendar.DAY_OF_YEAR,addonDays);
                
            // Loop back to find the cutoff date
            while (pasCountryProductVO.getCutoffDate() == null)
            {
                Date cutoffDate = cutoffDateCalendar.getTime();
                logger.debug("processing cutoff date for " + cutoffDate);
                PASCountryVO pasCountryCutoffVO = getQueryDAO().getPASCountry(getConnection(),
                                                                              pasCountryProductVO.getCountryId(),
                                                                              cutoffDate);
                if (pasCountryCutoffVO == null)
                {
                    logger.error("Error processing cutoff date, VO null for " + cutoffDate);
                    // AAAhhh, hosed, make delivery not available 
                    pasCountryProductVO.setCutoffDate(deliveryDate);
                    pasCountryProductVO.setCutoffTime("0000");
                    pasCountryProductVO.setDeliveryAvailable(NO);
                }
                else if (pasCountryCutoffVO.getShippingAllowed().equals(YES))
                {
                    logger.debug("Shipping is allowed");
                    // Found the date to allow
                    pasCountryProductVO.setCutoffDate(cutoffDate);
                    pasCountryProductVO.setCutoffTime(pasCountryCutoffVO.getCutoffTime());
                    pasCountryProductVO.setDeliveryAvailable(YES);
                    
                }
                else
                {
                    logger.debug("Shipping is not allowed, go to previous day");
                    // Check the previous day
                    cutoffDateCalendar.add(Calendar.DAY_OF_YEAR,-1);                    
                }
            }
            
        }

        // Insert the row into PAS_COUNTRY_DT
        getMaintDAO().insertPASCountryProduct(getConnection(), pasCountryProductVO);

    }
    
    protected Integer getIntAddonDays() throws Exception
    {
        if (internationalAddonDays == null)
        {
            String addonDaysString = getConfigurationUtil().getFrpGlobalParm(PASConstants.FTDAPPS_CONFIG_CONTEXT,
                                                                             PASConstants.INTL_OVERRIDE_DAYS_PARM);
            internationalAddonDays = Integer.parseInt(addonDaysString);
        }
        
        return internationalAddonDays;
    }
}
