package com.ftd.pas.server.vo;

import java.util.Date;

public class ProductDetailVO
{
    private String productId;
    private String productType;
    private String exceptionCode;
    private Date   exceptionStartDate;
    private Date   exceptionEndDate;
    private String exoticFlag;
    private Long    personalizationLeadDays;
    private String sundayFlag;
    private String mondayFlag;
    private String tuesdayFlag;
    private String wednesdayFlag;
    private String thursdayFlag;
    private String fridayFlag;
    private String saturdayFlag;
    private String codificationId;
    private String shipMethodFlorist;
    private String shipMethodCarrier;
    private String pquadProductId;
    
    public ProductDetailVO()
    {
    }

    public void setProductId(String param)
    {
        this.productId = param;
    }

    public String getProductId()
    {
        return productId;
    }

    public void setProductType(String param)
    {
        this.productType = param;
    }

    public String getProductType()
    {
        return productType;
    }

    public void setExceptionCode(String param)
    {
        this.exceptionCode = param;
    }

    public String getExceptionCode()
    {
        return exceptionCode;
    }

    public void setExceptionStartDate(Date param)
    {
        this.exceptionStartDate = param;
    }

    public Date getExceptionStartDate()
    {
        return exceptionStartDate;
    }

    public void setExceptionEndDate(Date param)
    {
        this.exceptionEndDate = param;
    }

    public Date getExceptionEndDate()
    {
        return exceptionEndDate;
    }

    public void setExoticFlag(String param)
    {
        this.exoticFlag = param;
    }

    public String getExoticFlag()
    {
        return exoticFlag;
    }

    public void setPersonalizationLeadDays(Long param)
    {
        this.personalizationLeadDays = param;
    }

    public Long getPersonalizationLeadDays()
    {
        return personalizationLeadDays;
    }

    public void setSundayFlag(String param)
    {
        this.sundayFlag = param;
    }

    public String getSundayFlag()
    {
        return sundayFlag;
    }

    public void setMondayFlag(String param)
    {
        this.mondayFlag = param;
    }

    public String getMondayFlag()
    {
        return mondayFlag;
    }

    public void setTuesdayFlag(String param)
    {
        this.tuesdayFlag = param;
    }

    public String getTuesdayFlag()
    {
        return tuesdayFlag;
    }

    public void setWednesdayFlag(String param)
    {
        this.wednesdayFlag = param;
    }

    public String getWednesdayFlag()
    {
        return wednesdayFlag;
    }

    public void setThursdayFlag(String param)
    {
        this.thursdayFlag = param;
    }

    public String getThursdayFlag()
    {
        return thursdayFlag;
    }

    public void setFridayFlag(String param)
    {
        this.fridayFlag = param;
    }

    public String getFridayFlag()
    {
        return fridayFlag;
    }

    public void setSaturdayFlag(String param)
    {
        this.saturdayFlag = param;
    }

    public String getSaturdayFlag()
    {
        return saturdayFlag;
    }

    public void setCodificationId(String param)
    {
        this.codificationId = param;
    }

    public String getCodificationId()
    {
        return codificationId;
    }

    public void setShipMethodFlorist(String shipMethodFlorist) {
        this.shipMethodFlorist = shipMethodFlorist;
    }

    public String getShipMethodFlorist() {
        return shipMethodFlorist;
    }

    public void setShipMethodCarrier(String shipMethodCarrier) {
        this.shipMethodCarrier = shipMethodCarrier;
    }

    public String getShipMethodCarrier() {
        return shipMethodCarrier;
    }

	public String getPquadProductId() {
		return pquadProductId;
	}

	public void setPquadProductId(String pquadProductId) {
		this.pquadProductId = pquadProductId;
	}
}
