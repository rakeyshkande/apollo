package com.ftd.pas.server.vo;

import java.util.Date;

/**
 * Value Object for holding the data for product information used by the
 * product availability.
 */
public class PASProductVO
{
    private String productId;
    private Date   deliveryDate;
    private String cutoffTime;
    private Long   addonDays;
    private Long   transitDays;
    private String shipAllowed;
    private String shipRestricted;
    private String deliveryAvailable;
    private String codificationId;
    private String productType;
    private String shipMethodFlorist;
    private String shipMethodCarrier;
    
    public PASProductVO()
    {
    }

    public void setProductId(String param)
    {
        this.productId = param;
    }

    public String getProductId()
    {
        return productId;
    }

    public void setDeliveryDate(Date param)
    {
        this.deliveryDate = param;
    }

    public Date getDeliveryDate()
    {
        return deliveryDate;
    }

    public void setCutoffTime(String param)
    {
        this.cutoffTime = param;
    }

    public String getCutoffTime()
    {
        return cutoffTime;
    }

    public void setAddonDays(Long param)
    {
        this.addonDays = param;
    }

    public Long getAddonDays()
    {
        return addonDays;
    }

    public void setTransitDays(Long param)
    {
        this.transitDays = param;
    }

    public Long getTransitDays()
    {
        return transitDays;
    }

    public void setShipAllowed(String param)
    {
        this.shipAllowed = param;
    }

    public String getShipAllowed()
    {
        return shipAllowed;
    }

    public void setShipRestricted(String param)
    {
        this.shipRestricted = param;
    }

    public String getShipRestricted()
    {
        return shipRestricted;
    }

    public void setDeliveryAvailable(String param)
    {
        this.deliveryAvailable = param;
    }

    public String getDeliveryAvailable()
    {
        return deliveryAvailable;
    }

    public void setCodificationId(String param)
    {
        this.codificationId = param;
    }

    public String getCodificationId()
    {
        return codificationId;
    }

    public void setProductType(String param)
    {
        this.productType = param;
    }

    public String getProductType()
    {
        return productType;
    }

    public void setShipMethodFlorist(String shipMethodFlorist) {
        this.shipMethodFlorist = shipMethodFlorist;
    }

    public String getShipMethodFlorist() {
        return shipMethodFlorist;
    }

    public void setShipMethodCarrier(String shipMethodCarrier) {
        this.shipMethodCarrier = shipMethodCarrier;
    }

    public String getShipMethodCarrier() {
        return shipMethodCarrier;
    }
}
