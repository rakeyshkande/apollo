package com.ftd.pas.server.vo;

public class CountryProductDetailVO
{
    private String countryId;
    private String productId;
    
    public CountryProductDetailVO()
    {
    }

    public void setCountryId(String param)
    {
        this.countryId = param;
    }

    public String getCountryId()
    {
        return countryId;
    }

    public void setProductId(String param)
    {
        this.productId = param;
    }

    public String getProductId()
    {
        return productId;
    }
}
