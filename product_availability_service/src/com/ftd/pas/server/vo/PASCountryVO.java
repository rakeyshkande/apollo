package com.ftd.pas.server.vo;

import java.util.Date;

/**
 * Value Object for the PAS Country table
 */
public class PASCountryVO
{
    private String countryId;
    private Date deliveryDate;
    private String cutoffTime;
    private long addonDays;
    private String deliveryAvailable;
    private String shippingAllowed;
    private String transitAllowed;
    private String holidayFlag;

    public PASCountryVO()
    {
    }

    public void setCountryId(String param)
    {
        this.countryId = param;
    }

    public String getCountryId()
    {
        return countryId;
    }

    public void setDeliveryAvailable(String param)
    {
        this.deliveryAvailable = param;
    }

    public String getDeliveryAvailable()
    {
        return deliveryAvailable;
    }

    public void setShippingAllowed(String param)
    {
        this.shippingAllowed = param;
    }

    public String getShippingAllowed()
    {
        return shippingAllowed;
    }

    public void setDeliveryDate(Date param)
    {
        this.deliveryDate = param;
    }

    public Date getDeliveryDate()
    {
        return deliveryDate;
    }

    public void setCutoffTime(String param)
    {
        this.cutoffTime = param;
    }

    public String getCutoffTime()
    {
        return cutoffTime;
    }

    public void setAddonDays(long param)
    {
        this.addonDays = param;
    }

    public long getAddonDays()
    {
        return addonDays;
    }

    public void setTransitAllowed(String param)
    {
        this.transitAllowed = param;
    }

    public String getTransitAllowed()
    {
        return transitAllowed;
    }

	public void setHolidayFlag(String holidayFlag) {
		this.holidayFlag = holidayFlag;
	}

	public String getHolidayFlag() {
		return holidayFlag;
	}
}
