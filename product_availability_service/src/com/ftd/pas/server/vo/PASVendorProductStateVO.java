package com.ftd.pas.server.vo;

import java.util.Date;

public class PASVendorProductStateVO
{
    private String vendorId;
    private String productId;
    private String stateCode;
    private Date   deliveryDate;
    private Date   shipNDDate;
    private Date   ship2DDate;
    private Date   shipGRDate;
    private Date   shipSatDate;
    private String shipNDCutoffTime;
    private String ship2DCutoffTime;
    private String shipGRCutoffTime;
    private String shipSatCutoffTime;
    
    public PASVendorProductStateVO()
    {
    }

    public void setVendorId(String param)
    {
        this.vendorId = param;
    }

    public String getVendorId()
    {
        return vendorId;
    }

    public void setProductId(String param)
    {
        this.productId = param;
    }

    public String getProductId()
    {
        return productId;
    }

    public void setStateCode(String param)
    {
        this.stateCode = param;
    }

    public String getStateCode()
    {
        return stateCode;
    }

    public void setDeliveryDate(Date param)
    {
        this.deliveryDate = param;
    }

    public Date getDeliveryDate()
    {
        return deliveryDate;
    }

    public void setShipNDDate(Date param)
    {
        this.shipNDDate = param;
    }

    public Date getShipNDDate()
    {
        return shipNDDate;
    }

    public void setShip2DDate(Date param)
    {
        this.ship2DDate = param;
    }

    public Date getShip2DDate()
    {
        return ship2DDate;
    }

    public void setShipGRDate(Date param)
    {
        this.shipGRDate = param;
    }

    public Date getShipGRDate()
    {
        return shipGRDate;
    }

    public void setShipSatDate(Date param)
    {
        this.shipSatDate = param;
    }

    public Date getShipSatDate()
    {
        return shipSatDate;
    }

    public void setShipNDCutoffTime(String param)
    {
        this.shipNDCutoffTime = param;
    }

    public String getShipNDCutoffTime()
    {
        return shipNDCutoffTime;
    }

    public void setShip2DCutoffTime(String param)
    {
        this.ship2DCutoffTime = param;
    }

    public String getShip2DCutoffTime()
    {
        return ship2DCutoffTime;
    }

    public void setShipGRCutoffTime(String param)
    {
        this.shipGRCutoffTime = param;
    }

    public String getShipGRCutoffTime()
    {
        return shipGRCutoffTime;
    }

    public void setShipSatCutoffTime(String param)
    {
        this.shipSatCutoffTime = param;
    }

    public String getShipSatCutoffTime()
    {
        return shipSatCutoffTime;
    }
}
