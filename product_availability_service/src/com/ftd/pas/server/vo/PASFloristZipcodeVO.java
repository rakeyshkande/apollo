package com.ftd.pas.server.vo;

import java.util.Date;

public class PASFloristZipcodeVO
{
    private String zipCode;
    private Date   deliveryDate;
    private String cutoffTime;
    private String statusCode;
    private String fitStatusCode;

    public PASFloristZipcodeVO()
    {
    }

    public void setZipCode(String param)
    {
        this.zipCode = param;
    }

    public String getZipCode()
    {
        return zipCode;
    }

    public void setDeliveryDate(Date param)
    {
        this.deliveryDate = param;
    }

    public Date getDeliveryDate()
    {
        return deliveryDate;
    }

    public void setCutoffTime(String param)
    {
        this.cutoffTime = param;
    }

    public String getCutoffTime()
    {
        return cutoffTime;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

	public String getFitStatusCode() {
		return fitStatusCode;
	}

	public void setFitStatusCode(String fitStatusCode) {
		this.fitStatusCode = fitStatusCode;
	}
}
