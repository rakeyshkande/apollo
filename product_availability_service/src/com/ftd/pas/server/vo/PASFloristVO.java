package com.ftd.pas.server.vo;

import java.util.Date;

public class PASFloristVO
{
    private String floristId;
    private Date   deliveryDate;
    private String cutoffTime;
    private String statusCode;
    
    public PASFloristVO()
    {
    }

    public void setFloristId(String param)
    {
        this.floristId = param;
    }

    public String getFloristId()
    {
        return floristId;
    }

    public void setDeliveryDate(Date param)
    {
        this.deliveryDate = param;
    }

    public Date getDeliveryDate()
    {
        return deliveryDate;
    }

    public void setCutoffTime(String param)
    {
        this.cutoffTime = param;
    }

    public String getCutoffTime()
    {
        return cutoffTime;
    }

    public void setStatusCode(String param)
    {
        this.statusCode = param;
    }

    public String getStatusCode()
    {
        return statusCode;
    }
}
