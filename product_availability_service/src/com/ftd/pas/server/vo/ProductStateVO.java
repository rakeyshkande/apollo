package com.ftd.pas.server.vo;

public class ProductStateVO
{
    private String productId;
    private String stateId;
    private String sunday;
    private String monday;
    private String tuesday;
    private String wednesday;
    private String thursday;
    private String friday;
    private String saturday;
    private String stateDeliveryExclusion;
    
        
    public ProductStateVO()
    {
    }

    public void setProductId(String param)
    {
        this.productId = param;
    }

    public String getProductId()
    {
        return productId;
    }

    public void setStateId(String param)
    {
        this.stateId = param;
    }

    public String getStateId()
    {
        return stateId;
    }

    public void setSunday(String param)
    {
        this.sunday = param;
    }

    public String getSunday()
    {
        return sunday;
    }

    public void setMonday(String param)
    {
        this.monday = param;
    }

    public String getMonday()
    {
        return monday;
    }

    public void setTuesday(String param)
    {
        this.tuesday = param;
    }

    public String getTuesday()
    {
        return tuesday;
    }

    public void setWednesday(String param)
    {
        this.wednesday = param;
    }

    public String getWednesday()
    {
        return wednesday;
    }

    public void setThursday(String param)
    {
        this.thursday = param;
    }

    public String getThursday()
    {
        return thursday;
    }

    public void setFriday(String param)
    {
        this.friday = param;
    }

    public String getFriday()
    {
        return friday;
    }

    public void setSaturday(String param)
    {
        this.saturday = param;
    }

    public String getSaturday()
    {
        return saturday;
    }

    public void setStateDeliveryExclusion(String param)
    {
        this.stateDeliveryExclusion = param;
    }

    public String getStateDeliveryExclusion()
    {
        return stateDeliveryExclusion;
    }
}
