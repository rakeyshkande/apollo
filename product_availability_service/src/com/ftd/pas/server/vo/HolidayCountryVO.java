package com.ftd.pas.server.vo;

import java.util.Date;

/**
 * Value Object for the Holiday Country table
 */
public class HolidayCountryVO
{
    private String countryId;
    private Date   holidayDate;
    private String deliverableFlag;
    private String shippingAllowed;
    
    public HolidayCountryVO()
    {
    }

    public void setCountryId(String param)
    {
        this.countryId = param;
    }

    public String getCountryId()
    {
        return countryId;
    }

    public void setHolidayDate(Date param)
    {
        this.holidayDate = param;
    }

    public Date getHolidayDate()
    {
        return holidayDate;
    }

    public void setDeliverableFlag(String param)
    {
        this.deliverableFlag = param;
    }

    public String getDeliverableFlag()
    {
        return deliverableFlag;
    }

    public void setShippingAllowed(String param)
    {
        this.shippingAllowed = param;
    }

    public String getShippingAllowed()
    {
        return shippingAllowed;
    }
}
