package com.ftd.pas.server.vo;

public class VendorDetailVO
{
    private String vendorId;
    private String cutoffTime;
    private String deliveryRestriction;
    private String shippingRestriction;
    private String vendorType;
    
    
    public VendorDetailVO()
    {
    }

    public void setVendorId(String param)
    {
        this.vendorId = param;
    }

    public String getVendorId()
    {
        return vendorId;
    }

    public void setCutoffTime(String param)
    {
        this.cutoffTime = param;
    }

    public String getCutoffTime()
    {
        return cutoffTime;
    }

    public void setDeliveryRestriction(String param)
    {
        this.deliveryRestriction = param;
    }

    public String getDeliveryRestriction()
    {
        return deliveryRestriction;
    }

    public void setShippingRestriction(String param)
    {
        this.shippingRestriction = param;
    }

    public String getShippingRestriction()
    {
        return shippingRestriction;
    }

	public String getVendorType() {
		return vendorType;
	}

	public void setVendorType(String vendorType) {
		this.vendorType = vendorType;
	}
}
