package com.ftd.pas.server.vo;

import java.util.Date;

public class PASCountryProductVO
{
    private String countryId;
    private String productId;
    private Date   deliveryDate;
    private Date   cutoffDate;
    private String cutoffTime;
    private String deliveryAvailable;
    
    public PASCountryProductVO()
    {
    }

    public void setCountryId(String param)
    {
        this.countryId = param;
    }

    public String getCountryId()
    {
        return countryId;
    }

    public void setProductId(String param)
    {
        this.productId = param;
    }

    public String getProductId()
    {
        return productId;
    }

    public void setDeliveryDate(Date param)
    {
        this.deliveryDate = param;
    }

    public Date getDeliveryDate()
    {
        return deliveryDate;
    }

    public void setCutoffDate(Date param)
    {
        this.cutoffDate = param;
    }

    public Date getCutoffDate()
    {
        return cutoffDate;
    }

    public void setCutoffTime(String param)
    {
        this.cutoffTime = param;
    }

    public String getCutoffTime()
    {
        return cutoffTime;
    }

    public void setDeliveryAvailable(String param)
    {
        this.deliveryAvailable = param;
    }

    public String getDeliveryAvailable()
    {
        return deliveryAvailable;
    }
}
