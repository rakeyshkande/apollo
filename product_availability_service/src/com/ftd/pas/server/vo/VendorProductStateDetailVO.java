package com.ftd.pas.server.vo;

public class VendorProductStateDetailVO
{
    private String vendorId;
    private String productId;
    private String stateCode;
    private String productSubcode;
    private boolean nextDayShipAvailable;
    private boolean twoDayShipAvailable;
    private boolean groundShipAvailable;
    private boolean saturdayDeliveryAvailable;
    
    // Some fields used in the calculations, handy to put them here
    private int nextDayTransitDays = 0;
    private int twoDayTransitDays = 0;
    private int groundTransitDays = 0;
    private int saturdayTransitDays = 0;
    private String vendorType;
    

    public VendorProductStateDetailVO()
    {
    }

    public void setVendorId(String param)
    {
        this.vendorId = param;
    }

    public String getVendorId()
    {
        return vendorId;
    }

    public void setProductId(String param)
    {
        this.productId = param;
    }

    public String getProductId()
    {
        return productId;
    }


    public void setStateCode(String param)
    {
        this.stateCode = param;
    }

    public String getStateCode()
    {
        return stateCode;
    }

    public void setProductSubcode(String param)
    {
        this.productSubcode = param;
    }

    public String getProductSubcode()
    {
        return productSubcode;
    }

    public void setNextDayShipAvailable(boolean param)
    {
        this.nextDayShipAvailable = param;
    }

    public boolean isNextDayShipAvailable()
    {
        return nextDayShipAvailable;
    }

    public void setTwoDayShipAvailable(boolean param)
    {
        this.twoDayShipAvailable = param;
    }

    public boolean isTwoDayShipAvailable()
    {
        return twoDayShipAvailable;
    }

    public void setGroundShipAvailable(boolean param)
    {
        this.groundShipAvailable = param;
    }

    public boolean isGroundShipAvailable()
    {
        return groundShipAvailable;
    }

    public void setSaturdayDeliveryAvailable(boolean param)
    {
        this.saturdayDeliveryAvailable = param;
    }

    public boolean isSaturdayDeliveryAvailable()
    {
        return saturdayDeliveryAvailable;
    }

    public void setNextDayTransitDays(int param)
    {
        this.nextDayTransitDays = param;
    }

    public int getNextDayTransitDays()
    {
        return nextDayTransitDays;
    }

    public void setTwoDayTransitDays(int param)
    {
        this.twoDayTransitDays = param;
    }

    public int getTwoDayTransitDays()
    {
        return twoDayTransitDays;
    }

    public void setGroundTransitDays(int param)
    {
        this.groundTransitDays = param;
    }

    public int getGroundTransitDays()
    {
        return groundTransitDays;
    }

    public void setSaturdayTransitDays(int param)
    {
        this.saturdayTransitDays = param;
    }

    public int getSaturdayTransitDays()
    {
        return saturdayTransitDays;
    }

	public String getVendorType() {
		return vendorType;
	}

	public void setVendorType(String vendorType) {
		this.vendorType = vendorType;
	}
}
