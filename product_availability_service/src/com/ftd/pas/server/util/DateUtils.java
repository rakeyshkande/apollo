package com.ftd.pas.server.util;

import java.util.Calendar;
import java.util.Date;

/**
 * Utility functions for use on Date or Calendar objects.  
 */
public class DateUtils
{
    
    /**
     * Determine if the objects have the same day.  The time (hour,minute,second) elements
     * are ignored in the comparison.  
     * @param day1
     * @param day2
     * @return
     */
    public static boolean isSameDay(Date day1, Date day2)
    {
        Calendar day2Calendar = Calendar.getInstance();
        day2Calendar.setTime(day2);
        Calendar day1Calendar = Calendar.getInstance();
        day1Calendar.setTime(day1);
        
        return isSameDay(day1Calendar, day2Calendar);
    }
    
    /**
     * Determine if the objects have the same day.  The time (hour,minute,second) elements
     * are ignored in the comparison.  
     * @param day1Calendar
     * @param day2
     * @return
     */
    public static boolean isSameDay(Calendar day1Calendar, Date day2)
    {
        Calendar day2Calendar = Calendar.getInstance();
        day2Calendar.setTime(day2);
        
        return isSameDay(day1Calendar, day2Calendar);
    }
    
    /**
     * Determine if the objects have the same day.  The time (hour,minute,second) elements
     * are ignored in the comparison.  
     * @param day1
     * @param day2Calendar
     * @return
     */
    public static boolean isSameDay(Date day1, Calendar day2Calendar)
    {
        Calendar day1Calendar = Calendar.getInstance();
        day1Calendar.setTime(day1);
        
        return isSameDay(day1Calendar, day2Calendar);
    }
    
    /**
     * Determine if the objects have the same day.  The time (hour,minute,second) elements
     * are ignored in the comparison.  
     * @param day1
     * @param day2
     * @return
     */
    public static boolean isSameDay(Calendar day1, Calendar day2)
    {
        boolean sameDay = false;
        
        if (day1.get(Calendar.YEAR) == day2.get(Calendar.YEAR))
        {
            if (day1.get(Calendar.DAY_OF_YEAR) == day2.get(Calendar.DAY_OF_YEAR))
            {
                sameDay = true;
            }
        }
        
        return sameDay;
    }
    
}
