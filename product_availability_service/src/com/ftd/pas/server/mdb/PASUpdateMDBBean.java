package com.ftd.pas.server.mdb;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.pas.server.command.Command;

import com.ftd.pas.server.command.CommandFactory;

import com.ftd.pas.server.constants.PASConstants;

import com.ftd.pas.server.util.SystemMessage;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.ejb.EJBException;
import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

public class PASUpdateMDBBean implements MessageDrivenBean, MessageListener
{
    protected Logger logger = new Logger(this.getClass().getName());

    private MessageDrivenContext context;

    public void ejbCreate()
    {
    }

    public void setMessageDrivenContext(MessageDrivenContext context) throws EJBException
    {
        this.context = context;
    }

    public void ejbRemove() throws EJBException
    {
    }

    /**
     * process the JMS message.  This should take the parameters and create
     * a command and arguements from them.
     * @param message
     */
    public void onMessage(Message message)
    {
        Connection conn = null;
        
        try
        {
            //  Called the MDB, do something
            if (!(message instanceof TextMessage))
            {
                throw new IllegalArgumentException("Invalid Message type " + message.getClass().getName() + " expecting TextMessage");
            }
            TextMessage textMessage = (TextMessage) message;
            
            String messageText = textMessage.getText();
            
            // The messageText holds the command to run
            logger.info("message = [" + messageText + "]");
            StringTokenizer tokens = new StringTokenizer(messageText);
            String commandName = "fubar";
            List<String> commandArguments = new ArrayList<String>();
            // First is the command
            if (tokens.hasMoreTokens())
            {
                commandName = tokens.nextToken();
            }
            // Second is the command argument
            while (tokens.hasMoreTokens())
            {
                // First is the command
                commandArguments.add(tokens.nextToken());
            }
            
            if (commandName != null)
            {
                ConfigurationUtil cu = ConfigurationUtil.getInstance();
                String datasource = cu.getPropertyNew(PASConstants.PROPERTY_FILE, PASConstants.DATASOURCE_NAME);
                conn = DataSourceUtil.getInstance().getConnection(datasource);
                // conn.setAutoCommit(true); // Removed, this is a Container Managed Transaction

                Command command = CommandFactory.createCommand(conn,commandName,commandArguments);
                command.execute();
            }
            else
            {
                throw new IllegalArgumentException("Invalid Message  [" + messageText + "]" );
            }

        } catch (Throwable t)
        {
            SystemMessage.sendSystemMessage(conn,"PAS Command Failed",t.getMessage());
            logger.error("Exception processing",t);
        } finally
        {
            //close the connection
            try
            {
                conn.close();
            } catch (Exception e)
            {
                logger.error("Exception processing",e);
            }
        }
    }
}
