package com.ftd.pas.server.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pas.server.vo.CountryProductDetailVO;
import com.ftd.pas.server.vo.HolidayCountryVO;

import com.ftd.pas.server.vo.PASCountryVO;
import com.ftd.pas.server.vo.PASProductStateVO;
import com.ftd.pas.server.vo.PASProductVO;
import com.ftd.pas.server.vo.PASVendorVO;
import com.ftd.pas.server.vo.PASFloristZipcodeVO;
import com.ftd.pas.server.vo.ProductDetailVO;

import com.ftd.pas.server.vo.ProductStateVO;

import com.ftd.pas.server.vo.VendorDetailVO;

import com.ftd.pas.server.vo.VendorProductStateDetailVO;

import java.math.BigDecimal;

import java.sql.Connection;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

/**
 * DAO for handling the queries for Product Availability.
 */
public class PASQueryDAO
{
    protected Logger logger = new Logger(this.getClass().getName());

    public PASQueryDAO()
    {
    }

    /**
     * Get the holidays for a country.
     * @param conn
     * @param countryId
     * @return
     * @throws Exception
     */
    public List<HolidayCountryVO> getCountryHolidays(Connection conn,
                                             String countryId) throws Exception 
    {
        HolidayCountryVO vo;
        List<HolidayCountryVO> holidayList = new ArrayList<HolidayCountryVO>();

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_COUNTRY_ID", countryId);

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_HOLIDAYS_BY_COUNTRY");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        
        while ( results != null && results.next() ) 
        {
            vo = new HolidayCountryVO();

            vo.setCountryId(countryId);
            vo.setHolidayDate(sdf.parse(results.getString("holidayDate")));
            vo.setDeliverableFlag(results.getString("deliverableFlag"));
            vo.setShippingAllowed(results.getString("shippingAllowed"));
            
            holidayList.add(vo);
        }

        return holidayList;
    }
    
    /**
     * Get the list of active products.  
     * @param conn
     * @param productId The product to filter by, optional
     * @return
     * @throws Exception
     */
    public List<ProductDetailVO> getActiveProducts(Connection conn, String productId, Date deliveryDate) throws Exception 
    {
        ProductDetailVO vo;
        List<ProductDetailVO> productList = new ArrayList<ProductDetailVO>();

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_PRODUCT_ID", productId);
        java.sql.Date sqlDate = new java.sql.Date(deliveryDate.getTime());
        inputParms.put("IN_DELIVERY_DATE", sqlDate);
        
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_ACTIVE_PRODUCTS");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        while ( results != null && results.next() ) 
        {
            vo = new ProductDetailVO();

            vo.setProductId(results.getString("PRODUCT_ID"));
            vo.setProductType(results.getString("PRODUCT_TYPE"));
            vo.setExceptionCode(results.getString("EXCEPTION_CODE"));
            vo.setExceptionStartDate(results.getDate("EXCEPTION_START_DATE"));
            vo.setExceptionEndDate(results.getDate("EXCEPTION_END_DATE"));
            vo.setExoticFlag(results.getString("EXOTIC_FLAG"));
            BigDecimal personalizationLeadDays = results.getBigDecimal("PERSONALIZATION_LEAD_DAYS");
            if (personalizationLeadDays != null)
            {
                vo.setPersonalizationLeadDays(personalizationLeadDays.longValue());
            }
            else
            {
                vo.setPersonalizationLeadDays(0L);
            }
            vo.setMondayFlag(results.getString("MONDAY_FLAG"));
            vo.setTuesdayFlag(results.getString("TUESDAY_FLAG"));
            vo.setWednesdayFlag(results.getString("WEDNESDAY_FLAG"));
            vo.setThursdayFlag(results.getString("THURSDAY_FLAG"));
            vo.setFridayFlag(results.getString("FRIDAY_FLAG"));
            vo.setSaturdayFlag(results.getString("SATURDAY_FLAG"));
            vo.setSundayFlag(results.getString("SUNDAY_FLAG"));
            vo.setCodificationId(results.getString("CODIFICATION_ID"));
            vo.setShipMethodFlorist(results.getString("SHIP_METHOD_FLORIST"));
            vo.setShipMethodCarrier(results.getString("SHIP_METHOD_CARRIER"));
            vo.setPquadProductId(results.getString("PQUAD_PRODUCT_ID"));
            
            productList.add(vo);
        }

        return productList;
    }

    /**
     * Get the list of active exotic products.  
     * @param conn
     * @return
     * @throws Exception
     */
    public List<ProductDetailVO> getActiveExoticProducts(Connection conn) throws Exception 
    {
        ProductDetailVO vo;
        List<ProductDetailVO> productList = new ArrayList<ProductDetailVO>();

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_ACTIVE_EXOTIC_PRODUCTS");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        while ( results != null && results.next() ) 
        {
            vo = new ProductDetailVO();

            vo.setProductId(results.getString("PRODUCT_ID"));
            vo.setProductType(results.getString("PRODUCT_TYPE"));
            vo.setExceptionCode(results.getString("EXCEPTION_CODE"));
            vo.setExceptionStartDate(results.getDate("EXCEPTION_START_DATE"));
            vo.setExceptionEndDate(results.getDate("EXCEPTION_END_DATE"));
            vo.setExoticFlag(results.getString("EXOTIC_FLAG"));
            BigDecimal personalizationLeadDays = results.getBigDecimal("PERSONALIZATION_LEAD_DAYS");
            if (personalizationLeadDays != null)
            {
                vo.setPersonalizationLeadDays(personalizationLeadDays.longValue());
            }
            else
            {
                vo.setPersonalizationLeadDays(0L);
            }
            vo.setMondayFlag(results.getString("MONDAY_FLAG"));
            vo.setTuesdayFlag(results.getString("TUESDAY_FLAG"));
            vo.setWednesdayFlag(results.getString("WEDNESDAY_FLAG"));
            vo.setThursdayFlag(results.getString("THURSDAY_FLAG"));
            vo.setFridayFlag(results.getString("FRIDAY_FLAG"));
            vo.setSaturdayFlag(results.getString("SATURDAY_FLAG"));
            vo.setSundayFlag(results.getString("SUNDAY_FLAG"));
            vo.setCodificationId(results.getString("CODIFICATION_ID"));
            
            productList.add(vo);
        }

        return productList;
    }

    /**
     * Get the details for a single product.
     * @param conn
     * @param productId
     * @return
     * @throws Exception
     */
    public ProductDetailVO getProduct(Connection conn, String productId) throws Exception 
    {
        ProductDetailVO vo = null;

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_PRODUCT_ID",productId);

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_PRODUCT");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        if ( results != null && results.next() ) 
        {
            vo = new ProductDetailVO();

            vo.setProductId(results.getString("PRODUCT_ID"));
            vo.setProductType(results.getString("PRODUCT_TYPE"));
            vo.setExceptionCode(results.getString("EXCEPTION_CODE"));
            vo.setExceptionStartDate(results.getDate("EXCEPTION_START_DATE"));
            vo.setExceptionEndDate(results.getDate("EXCEPTION_END_DATE"));
            vo.setExoticFlag(results.getString("EXOTIC_FLAG"));
            vo.setPersonalizationLeadDays(results.getBigDecimal("PERSONALIZATION_LEAD_DAYS").longValue());
            vo.setMondayFlag(results.getString("MONDAY_FLAG"));
            vo.setTuesdayFlag(results.getString("TUESDAY_FLAG"));
            vo.setWednesdayFlag(results.getString("WEDNESDAY_FLAG"));
            vo.setThursdayFlag(results.getString("THURSDAY_FLAG"));
            vo.setFridayFlag(results.getString("FRIDAY_FLAG"));
            vo.setSaturdayFlag(results.getString("SATURDAY_FLAG"));
            vo.setSundayFlag(results.getString("SUNDAY_FLAG"));
            vo.setCodificationId(results.getString("CODIFICATION_ID"));
            
        }

        return vo;
    }

    /**
     * Get the list of all product state exclusions.  
     * @param conn
     * @param productId Filter by product id, Optional
     * @param stateCode Filter by state code, Optional
     * @return
     * @throws Exception
     */
    public List<ProductStateVO> getProductStateExclusions(Connection conn, String productId, String stateCode) 
            throws Exception 
    {
        ProductStateVO vo;
        List<ProductStateVO> productStateList = new ArrayList<ProductStateVO>();

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_PRODUCT_ID", productId);
        inputParms.put("IN_STATE_CODE", stateCode);
        

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_PRODUCT_STATE_EXCLUSIONS");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        while ( results != null && results.next() ) 
        {
            vo = new ProductStateVO();

            vo.setProductId(results.getString("PRODUCT_ID"));
            vo.setStateId(results.getString("EXCLUDED_STATE"));
            vo.setSunday(results.getString("SUN"));
            vo.setMonday(results.getString("MON"));
            vo.setTuesday(results.getString("TUE"));
            vo.setWednesday(results.getString("WED"));
            vo.setThursday(results.getString("THU"));
            vo.setFriday(results.getString("FRI"));
            vo.setSaturday(results.getString("SAT"));
            vo.setStateDeliveryExclusion(results.getString("DELIVERY_EXCLUSION"));
            
            productStateList.add(vo);
        }

        return productStateList;
    }

    /**
     * Get the list of all vendor details and restrictions.  
     * @param conn
     * @param vendorId Filter by vendorr id, Optional
     * @param deliveryDate delivery date to get restrictions for
     * @return
     * @throws Exception
     */
    public List<VendorDetailVO> getVendorDetails(Connection conn, String vendorId, Date deliveryDate) 
            throws Exception 
    {
        VendorDetailVO vo;
        List<VendorDetailVO> vendorDetailList = new ArrayList<VendorDetailVO>();

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_VENDOR_ID", vendorId);
        java.sql.Date sqlDate = new java.sql.Date(deliveryDate.getTime());
        inputParms.put("IN_DELIVERY_DATE", sqlDate);
        

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_VENDORS_WITH_RESTRICTIONS");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        while ( results != null && results.next() ) 
        {
            vo = new VendorDetailVO();

            vo.setVendorId(results.getString("VENDOR_ID"));
            vo.setCutoffTime(results.getString("CUTOFF_TIME"));
            vo.setShippingRestriction(results.getString("SHIP_RESTRICTION"));
            vo.setDeliveryRestriction(results.getString("DELIVERY_RESTRICTION"));
            vo.setVendorType(results.getString("VENDOR_TYPE"));
            
            vendorDetailList.add(vo);
        }

        return vendorDetailList;
    }

    /**
     * Get the list of all country products.  
     * @param conn
     * @param countryId Filter by country id, Optional
     * @param productId Filter by product id, Optional
     * @return
     * @throws Exception
     */
    public List<CountryProductDetailVO> getCountryProductDetails(Connection conn, String countryId, String productId) 
            throws Exception 
    {
        CountryProductDetailVO vo;
        List<CountryProductDetailVO> list = new ArrayList<CountryProductDetailVO>();

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_COUNTRY_ID", countryId);
        inputParms.put("IN_PRODUCT_ID", productId);

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_COUNTRY_PRODUCTS");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        while ( results != null && results.next() ) 
        {
            vo = new CountryProductDetailVO();

            vo.setCountryId(results.getString("COUNTRY_ID"));
            vo.setProductId(results.getString("PRODUCT_ID"));
            
            list.add(vo);
        }

        return list;
    }
    
    /**
     * Get the list of all vendor product state details.  
     * @param conn
     * @param vendorId Filter by vendor id, Optional
     * @param productId Filter by product id, Optional
     * @return
     * @throws Exception
     */
    public List<VendorProductStateDetailVO> getVendorProductDetails(Connection conn, 
                                                                 String vendorId, 
                                                                 String productId) 
            throws Exception 
    {
        VendorProductStateDetailVO vo;
        List<VendorProductStateDetailVO> list = new ArrayList<VendorProductStateDetailVO>();

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_VENDOR_ID", vendorId);
        inputParms.put("IN_PRODUCT_ID", productId);

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_VENDOR_PRODUCTS");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        while ( results != null && results.next() ) 
        {
            vo = new VendorProductStateDetailVO();

            vo.setVendorId(results.getString("VENDOR_ID"));
            vo.setProductId(results.getString("PRODUCT_ID"));
            vo.setProductSubcode(results.getString("PRODUCT_SUBCODE_ID"));
            vo.setNextDayShipAvailable(StringUtils.equals("Y",results.getString("NEXT_DAY_SHIP_AVAILABLE")));
            vo.setTwoDayShipAvailable(StringUtils.equals("Y",results.getString("TWO_DAY_SHIP_AVAILABLE")));
            vo.setGroundShipAvailable(StringUtils.equals("Y",results.getString("GROUND_SHIP_AVAILABLE")));
            vo.setSaturdayDeliveryAvailable(StringUtils.equals("Y",results.getString("SATURDAY_SHIP_AVAILABLE")));
            vo.setVendorType(results.getString("VENDOR_TYPE"));
            
            list.add(vo);
        }

        return list;
    }

    /**
     * Get the PAS country data from PAS_COUNTRY_DT.
     * @param conn
     * @param countryId
     * @return
     * @throws Exception
     */
    public PASCountryVO getPASCountry(Connection conn,
                                      String countryId,
                                      Date   deliveryDate) throws Exception 
    {
        PASCountryVO vo = null;

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_COUNTRY_ID", countryId);
        java.sql.Date sqlDeliveryDate = new java.sql.Date(deliveryDate.getTime());
        inputParms.put("IN_DELIVERY_DATE", sqlDeliveryDate);

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_PAS_COUNTRY");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        if ( results != null && results.next() ) 
        {
            vo = new PASCountryVO();

            vo.setCountryId(results.getString("COUNTRY_ID"));
            vo.setDeliveryDate(results.getDate("DELIVERY_DATE"));
            vo.setCutoffTime(results.getString("CUTOFF_TIME"));
            vo.setAddonDays(results.getBigDecimal("ADDON_DAYS_QTY").longValue());
            vo.setShippingAllowed(results.getString("SHIP_ALLOWED_FLAG"));
            vo.setDeliveryAvailable(results.getString("DELIVERY_AVAILABLE_FLAG"));
            vo.setTransitAllowed(results.getString("TRANSIT_ALLOWED_FLAG"));
            vo.setHolidayFlag(results.getString("deliverable_flag") == null ? "N" : "Y");
        }

        return vo;
    }

    /**
     * Get All the PAS vendor data from PAS_VENDOR_DT.
     * @param conn
     * @return
     * @throws Exception
     */
    public List<PASVendorVO> getAllPASVendor(Connection conn,
                                     Date   deliveryDate) throws Exception 
    {
        List<PASVendorVO> voList = new ArrayList<PASVendorVO>();
        PASVendorVO vo = null;

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        java.sql.Date sqlDeliveryDate = new java.sql.Date(deliveryDate.getTime());
        inputParms.put("IN_DELIVERY_DATE", sqlDeliveryDate);

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_ALL_PAS_VENDOR");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        while ( results != null && results.next() ) 
        {
            vo = new PASVendorVO();

            vo.setVendorId(results.getString("VENDOR_ID"));
            vo.setDeliveryDate(results.getDate("DELIVERY_DATE"));
            vo.setCutoffTime(results.getString("CUTOFF_TIME"));
            vo.setShipAllowed(results.getString("SHIP_ALLOWED_FLAG"));
            vo.setDeliveryAvailable(results.getString("DELIVERY_AVAILABLE_FLAG"));
            vo.setVendorType(results.getString("VENDOR_TYPE"));
            
            voList.add(vo);
        }

        return voList;
    }

    /**
     * Get the PAS vendor data from PAS_VENDOR_DT.
     * @param conn
     * @param vendorId
     * @return
     * @throws Exception
     */
    public List<PASVendorVO> getPASVendor(Connection conn,
                                     String vendorId,
                                     Date   deliveryDate) throws Exception 
    {
        List<PASVendorVO> voList = new ArrayList<PASVendorVO>();
        PASVendorVO vo = null;

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_VENDOR_ID", vendorId);
        java.sql.Date sqlDeliveryDate = new java.sql.Date(deliveryDate.getTime());
        inputParms.put("IN_DELIVERY_DATE", sqlDeliveryDate);

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_PAS_VENDOR");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        while ( results != null && results.next() ) 
        {
            vo = new PASVendorVO();

            vo.setVendorId(results.getString("VENDOR_ID"));
            vo.setDeliveryDate(results.getDate("DELIVERY_DATE"));
            vo.setCutoffTime(results.getString("CUTOFF_TIME"));
            vo.setShipAllowed(results.getString("SHIP_ALLOWED_FLAG"));
            vo.setDeliveryAvailable(results.getString("DELIVERY_AVAILABLE_FLAG"));
            vo.setVendorType(results.getString("VENDOR_TYPE"));
            
            voList.add(vo);
        }

        return voList;
    }

    /**
     * Get the PAS product state data from PAS_PRODUCT_STATE_DT.
     * @param conn
     * @param productId
     * @param stateCode
     * @return
     * @throws Exception
     */
    public PASProductStateVO getPASProductState(Connection conn,
                                                String productId,
                                                String stateCode,
                                                Date   deliveryDate) throws Exception 
    {
        PASProductStateVO vo = null;

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_PRODUCT_ID", productId);
        inputParms.put("IN_STATE_CODE", stateCode);
        java.sql.Date sqlDeliveryDate = new java.sql.Date(deliveryDate.getTime());
        inputParms.put("IN_DELIVERY_DATE", sqlDeliveryDate);

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_PAS_PRODUCT_STATE");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        if ( results != null && results.next() ) 
        {
            vo = new PASProductStateVO();

            vo.setProductId(results.getString("PRODUCT_ID"));
            vo.setStateCode(results.getString("STATE_CODE"));
            vo.setDeliveryDate(results.getDate("DELIVERY_DATE"));
            vo.setDeliveryAvailable(results.getString("DELIVERY_AVAILABLE_FLAG"));
            
        }

        return vo;
    }
    
    /**
     * Get the PAS product data from PAS_PRODUCT_DT.
     * @param conn
     * @param productId
     * @return
     * @throws Exception
     */
    public PASProductVO getPASProduct(Connection conn,
                                      String productId,
                                      Date   deliveryDate) throws Exception 
    {
        PASProductVO vo = null;

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_PRODUCT_ID", productId);
        java.sql.Date sqlDeliveryDate = new java.sql.Date(deliveryDate.getTime());
        inputParms.put("IN_DELIVERY_DATE", sqlDeliveryDate);

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_PAS_PRODUCT");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        if ( results != null && results.next() ) 
        {
            vo = new PASProductVO();

            vo.setProductId(results.getString("PRODUCT_ID"));
            vo.setDeliveryDate(results.getDate("DELIVERY_DATE"));
            vo.setCutoffTime(results.getString("CUTOFF_TIME"));
            vo.setAddonDays(results.getBigDecimal("ADDON_DAYS_QTY").longValue());
            vo.setTransitDays(results.getBigDecimal("TRANSIT_DAYS_QTY").longValue());
            vo.setShipAllowed(results.getString("SHIP_ALLOWED_FLAG"));
            vo.setShipRestricted(results.getString("SHIP_RESTRICTED_FLAG"));
            vo.setDeliveryAvailable(results.getString("DELIVERY_AVAILABLE_FLAG"));
            vo.setCodificationId(results.getString("CODIFICATION_ID"));
            vo.setProductType(results.getString("PRODUCT_TYPE"));
            vo.setShipMethodFlorist(results.getString("SHIP_METHOD_FLORIST"));
            vo.setShipMethodCarrier(results.getString("SHIP_METHOD_CARRIER"));
            
        }

        return vo;
    }

    /**
     * Get the list of all florist zips.  
     * @param conn
     * @param floristId
     * @return
     * @throws Exception
     */
    public List getFloristZipsByFlorist(Connection conn, String floristId) throws Exception 
    {
        List list = new ArrayList();

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_FLORIST_ID", floristId);

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_FLORIST_ZIPS_BY_FLORIST");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        while ( results != null && results.next() ) 
        {
            String zipCode = results.getString("ZIP_CODE");
            list.add(zipCode);
        }

        return list;
    }

    /**
     * Get the list of all florist details.  
     * @param conn
     * @param zipCode Filter by florist id, Optional
     * @return
     * @throws Exception
     */
    public List<PASFloristZipcodeVO> getFloristZipsByZip(Connection conn, String zipCode, Date processDate) 
            throws Exception 
    {
        List<PASFloristZipcodeVO> list = new ArrayList<PASFloristZipcodeVO>();

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_ZIP_CODE", zipCode);
        java.sql.Date sqlDeliveryDate = new java.sql.Date(processDate.getTime());
        inputParms.put("IN_DELIVERY_DATE", sqlDeliveryDate);

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_FLORIST_ZIPS_BY_ZIP");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        PASFloristZipcodeVO vo = new PASFloristZipcodeVO();
        vo.setZipCode(zipCode);
        vo.setDeliveryDate(processDate);

        if (results != null && results.next()) {
        	// if any results are returned, the zip code is available for FIT
            vo.setFitStatusCode("Y");
            // check EROS availability for Apollo zip code availability
        	String openInEROS = results.getString("open_in_eros");
        	if (openInEROS != null && openInEROS.equalsIgnoreCase("Y")) {
                vo.setCutoffTime(results.getString("cutoff_time"));
                vo.setStatusCode("Y");
        	} else {
                vo.setCutoffTime("0000");
                vo.setStatusCode("N");
        	}
        } else {
            vo.setCutoffTime("0000");
            vo.setStatusCode("N");
            vo.setFitStatusCode("N");
        }
        
        list.add(vo);

        return list;
    }

    /**
     * Get the list of all florist_zip zip codes.  
     * @param conn
     * @return
     * @throws Exception
     */
    public List getAllFloristZips(Connection conn, String zipCode) throws Exception {
        List list = new ArrayList();

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_ZIP_CODE", zipCode);

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_ALL_FLORIST_ZIPS");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        while ( results != null && results.next() ) {
            list.add(results.getString("ZIP_CODE"));
        }

        return list;
    }

    public HashMap getInventoryTrackingAvailability(Connection conn, String productId, String vendorId,
    		Date shipDate) throws Exception {
    	
    	HashMap params = new HashMap();
    	
        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_PRODUCT_ID", productId);
        inputParms.put("IN_VENDOR_ID", vendorId);
        java.sql.Date sqlDate = new java.sql.Date(shipDate.getTime());
        inputParms.put("IN_SHIP_DATE", sqlDate);

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("CHECK_VENDOR_SHUTDOWN");
        dataRequest.setInputParams(inputParms);
  
        Map outputs = (Map)DataAccessUtil.getInstance().execute(dataRequest);

        String status = (String)outputs.get("OUT_STATUS");
        String message = "";
        if (status != null && status.equalsIgnoreCase("N"))
        {
            message = (String)outputs.get("OUT_MESSAGE");
            logger.error(message);
        } else {
        	String shutdownReached = (String)outputs.get("OUT_SHUTDOWN_LVL_REACHED");
        	String shutdownType = (String)outputs.get("OUT_SHUTDOWN_TYPE");
        	params.put("SHUTDOWN_LVL_REACHED", shutdownReached);
        	params.put("SHUTDOWN_TYPE", shutdownType);
        }
    	return params;
    	
    }
    
}
