package com.ftd.pas.server.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pas.server.constants.PASConstants;
import com.ftd.pas.server.vo.PASCountryProductVO;
import com.ftd.pas.server.vo.PASCountryVO;

import com.ftd.pas.server.vo.PASFloristVO;
import com.ftd.pas.server.vo.PASFloristZipcodeVO;
import com.ftd.pas.server.vo.PASProductStateVO;
import com.ftd.pas.server.vo.PASProductVO;

import com.ftd.pas.server.vo.PASStateVO;
import com.ftd.pas.server.vo.PASTimeZoneVO;
import com.ftd.pas.server.vo.PASVendorProductStateVO;
import com.ftd.pas.server.vo.PASVendorVO;

import java.math.BigDecimal;

import java.sql.Connection;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Data Access Object for maintaing the tables in the PAS Schema.
 */
public class PASMaintDAO
{
    protected Logger logger = new Logger(this.getClass().getName());

    public PASMaintDAO()
    {
    }

    /**
     * Insert or Update a row in PAS_COUNTRY_DT.
     * @param con
     * @param vo
     * @throws Exception
     */
    public void insertPASCountry(Connection con, PASCountryVO vo) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(con);
        dataRequest.setStatementID("PAS_INSERT_COUNTRY");

        HashMap inputParams = new HashMap();
        inputParams.put("IN_COUNTRY_ID", vo.getCountryId());
        java.sql.Date deliveryDate = new java.sql.Date(vo.getDeliveryDate().getTime());
        inputParams.put("IN_DELIVERY_DATE", deliveryDate);
        inputParams.put("IN_CUTOFF_TIME", vo.getCutoffTime());
        inputParams.put("IN_ADDON_DAYS_QTY", new BigDecimal(vo.getAddonDays()));
        inputParams.put("IN_SHIP_ALLOWED_FLAG", vo.getShippingAllowed());
        inputParams.put("IN_DELIVERY_AVAILABLE_FLAG", vo.getDeliveryAvailable());
        inputParams.put("IN_TRANSIT_ALLOWED_FLAG", vo.getTransitAllowed());
        dataRequest.setInputParams(inputParams);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map)dataAccessUtil.execute(dataRequest);

        String status = (String)outputs.get("OUT_STATUS");
        if (status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String)outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }

    }
    
    /**
     * Insert or Update a row in PAS_PRODUCT_DT.
     * @param con
     * @param vo
     * @throws Exception
     */
    public void insertPASProduct(Connection con, PASProductVO vo) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(con);
        dataRequest.setStatementID("PAS_INSERT_PRODUCT");

        HashMap inputParams = new HashMap();
        inputParams.put("IN_PRODUCT_ID", vo.getProductId());
        java.sql.Date deliveryDate = new java.sql.Date(vo.getDeliveryDate().getTime());
        inputParams.put("IN_DELIVERY_DATE", deliveryDate);
        inputParams.put("IN_CUTOFF_TIME", vo.getCutoffTime());
        inputParams.put("IN_ADDON_DAYS_QTY", new BigDecimal(vo.getAddonDays()));
        inputParams.put("IN_TRANSIT_DAYS_QTY", new BigDecimal(vo.getTransitDays()));
        inputParams.put("IN_SHIP_ALLOWED_FLAG", vo.getShipAllowed());
        inputParams.put("IN_SHIP_RESTRICTED_FLAG", vo.getShipRestricted());
        inputParams.put("IN_DELIVERY_AVAILABLE_FLAG", vo.getDeliveryAvailable());
        inputParams.put("IN_CODIFICATION_ID", vo.getCodificationId());
        inputParams.put("IN_PRODUCT_TYPE", vo.getProductType());
        inputParams.put("IN_SHIP_METHOD_FLORIST", vo.getShipMethodFlorist());
        inputParams.put("IN_SHIP_METHOD_CARRIER", vo.getShipMethodCarrier());
        dataRequest.setInputParams(inputParams);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map)dataAccessUtil.execute(dataRequest);

        String status = (String)outputs.get("OUT_STATUS");
        if (status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String)outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }

    }

    /**
     * Insert or Update a row in PAS_PRODUCT_STATE_DT.
     * @param con
     * @param vo
     * @throws Exception
     */
    public void insertPASProductState(Connection con, PASProductStateVO vo) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(con);
        dataRequest.setStatementID("PAS_INSERT_PRODUCT_STATE");

        HashMap inputParams = new HashMap();
        inputParams.put("IN_PRODUCT_ID", vo.getProductId());
        inputParams.put("IN_STATE_CODE", vo.getStateCode());
        java.sql.Date deliveryDate = new java.sql.Date(vo.getDeliveryDate().getTime());
        inputParams.put("IN_DELIVERY_DATE", deliveryDate);
        inputParams.put("IN_DELIVERY_AVAILABLE_FLAG", vo.getDeliveryAvailable());
        dataRequest.setInputParams(inputParams);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map)dataAccessUtil.execute(dataRequest);

        String status = (String)outputs.get("OUT_STATUS");
        if (status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String)outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }

    }

    /**
     * Insert or Update a row in PAS_VENDOR_DT.
     * @param con
     * @param vo
     * @throws Exception
     */
    public void insertPASVendor(Connection con, PASVendorVO vo) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(con);
        dataRequest.setStatementID("PAS_INSERT_VENDOR");

        HashMap inputParams = new HashMap();
        inputParams.put("IN_VENDOR_ID", vo.getVendorId());
        java.sql.Date deliveryDate = new java.sql.Date(vo.getDeliveryDate().getTime());
        inputParams.put("IN_DELIVERY_DATE", deliveryDate);
        inputParams.put("IN_CUTOFF_TIME", vo.getCutoffTime());
        inputParams.put("IN_SHIP_ALLOWED_FLAG", vo.getShipAllowed());
        inputParams.put("IN_DELIVERY_AVAILABLE_FLAG", vo.getDeliveryAvailable());
        dataRequest.setInputParams(inputParams);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map)dataAccessUtil.execute(dataRequest);

        String status = (String)outputs.get("OUT_STATUS");
        if (status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String)outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }

    }

    /**
     * Insert or Update a row in PAS_STATE_DT.
     * @param con
     * @param vo
     * @throws Exception
     */
    public void insertPASState(Connection con, PASStateVO vo) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(con);
        dataRequest.setStatementID("PAS_INSERT_STATE");

        HashMap inputParams = new HashMap();
        inputParams.put("IN_STATE_CODE", vo.getStateCode());
        java.sql.Date deliveryDate = new java.sql.Date(vo.getDeliveryDate().getTime());
        inputParams.put("IN_DELIVERY_DATE", deliveryDate);
        inputParams.put("IN_CUTOFF_TIME", vo.getCutoffTime());
        dataRequest.setInputParams(inputParams);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map)dataAccessUtil.execute(dataRequest);

        String status = (String)outputs.get("OUT_STATUS");
        if (status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String)outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }

    }

    /**
     * Insert or Update a row in PAS_FLORIST_DT.
     * @param con
     * @param vo
     * @throws Exception
     */
    public void insertPASFlorist(Connection con, PASFloristVO vo) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(con);
        dataRequest.setStatementID("PAS_INSERT_FLORIST");

        HashMap inputParams = new HashMap();
        inputParams.put("IN_FLORIST_ID", vo.getFloristId());
        java.sql.Date deliveryDate = new java.sql.Date(vo.getDeliveryDate().getTime());
        inputParams.put("IN_DELIVERY_DATE", deliveryDate);
        inputParams.put("IN_CUTOFF_TIME", vo.getCutoffTime());
        inputParams.put("IN_STATUS_CODE", vo.getStatusCode());
        dataRequest.setInputParams(inputParams);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map)dataAccessUtil.execute(dataRequest);

        String status = (String)outputs.get("OUT_STATUS");
        if (status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String)outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }

    }

    /**
     * Insert or Update a row in PAS_VENDOR_PRODUCT_STATE_DT.
     * @param con
     * @param vo
     * @throws Exception
     */
    public void insertPASVendorProductState(Connection con, PASVendorProductStateVO vo) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(con);
        dataRequest.setStatementID("PAS_INSERT_VENDOR_PRODUCT_STATE");

        HashMap inputParams = new HashMap();
        inputParams.put("IN_VENDOR_ID", vo.getVendorId());
        inputParams.put("IN_PRODUCT_ID", vo.getProductId());
        inputParams.put("IN_STATE_CODE", vo.getStateCode());
        java.sql.Date deliveryDate = new java.sql.Date(vo.getDeliveryDate().getTime());
        inputParams.put("IN_DELIVERY_DATE", deliveryDate);

        if (vo.getShipNDDate() == null)
            deliveryDate = null;
        else
            deliveryDate = new java.sql.Date(vo.getShipNDDate().getTime());
        inputParams.put("IN_SHIP_ND_DATE", deliveryDate);

        if (vo.getShip2DDate() == null)
            deliveryDate = null;
        else
            deliveryDate = new java.sql.Date(vo.getShip2DDate().getTime());
        inputParams.put("IN_SHIP_2D_DATE", deliveryDate);

        if (vo.getShipGRDate() == null)
            deliveryDate = null;
        else
            deliveryDate = new java.sql.Date(vo.getShipGRDate().getTime());
        inputParams.put("IN_SHIP_GR_DATE", deliveryDate);
        if (vo.getShipSatDate() == null)
            deliveryDate = null;
        else
            deliveryDate = new java.sql.Date(vo.getShipSatDate().getTime());
        inputParams.put("IN_SHIP_SAT_DATE", deliveryDate);

        inputParams.put("IN_SHIP_ND_CUTOFF_TIME", vo.getShipNDCutoffTime());
        inputParams.put("IN_SHIP_2D_CUTOFF_TIME", vo.getShip2DCutoffTime());
        inputParams.put("IN_SHIP_GR_CUTOFF_TIME", vo.getShipGRCutoffTime());
        inputParams.put("IN_SHIP_SAT_CUTOFF_TIME", vo.getShipSatCutoffTime());
        
        dataRequest.setInputParams(inputParams);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map)dataAccessUtil.execute(dataRequest);

        String status = (String)outputs.get("OUT_STATUS");
        if (status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String)outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }

    }

    /**
     * Insert or Update a row in PAS_COUNTRY_PRODUCT_DT.
     * @param con
     * @param vo
     * @throws Exception
     */
    public void insertPASCountryProduct(Connection con, PASCountryProductVO vo) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(con);
        dataRequest.setStatementID("PAS_INSERT_COUNTRY_PRODUCT");

        HashMap inputParams = new HashMap();
        inputParams.put("IN_COUNTRY_ID", vo.getCountryId());
        inputParams.put("IN_PRODUCT_ID", vo.getProductId());
        java.sql.Date deliveryDate = new java.sql.Date(vo.getDeliveryDate().getTime());
        inputParams.put("IN_DELIVERY_DATE", deliveryDate);
        deliveryDate = new java.sql.Date(vo.getCutoffDate().getTime());
        inputParams.put("IN_CUTOFF_DATE", deliveryDate);
        inputParams.put("IN_CUTOFF_TIME", vo.getCutoffTime());
        inputParams.put("IN_DELIVERY_AVAILABLE_FLAG", vo.getDeliveryAvailable());
        
        dataRequest.setInputParams(inputParams);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map)dataAccessUtil.execute(dataRequest);

        String status = (String)outputs.get("OUT_STATUS");
        if (status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String)outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }

    }

    /**
     * Insert or Update a row in PAS_TIMEZONE_DT.
     * @param con
     * @param vo
     * @throws Exception
     */
    public void insertPASTimeZone(Connection con, PASTimeZoneVO vo) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(con);
        dataRequest.setStatementID("INSERT_PAS_TIMEZONE_DT");

        HashMap inputParams = new HashMap();
        inputParams.put("IN_TIME_ZONE_CODE", Integer.toString(vo.getTimeZoneCode()));
        inputParams.put("IN_DELTA_TO_CST_HHMM", Integer.toString(vo.getCstDelta()));
        java.sql.Date deliveryDate = new java.sql.Date(vo.getDeliveryDate().getTime());
        inputParams.put("IN_DELIVERY_DATE", deliveryDate);
        inputParams.put("IN_DAYLIGHT_SAVINGS_FLAG", vo.getDaylightSavingsFlag());
        dataRequest.setInputParams(inputParams);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map)dataAccessUtil.execute(dataRequest);

        String status = (String)outputs.get("OUT_STATUS");
        if (status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String)outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }

    }

    /**
     * Insert a message into the PAS JMS queue.
     * @param con
     * @param payload message payload
     * @throws Exception
     */
    public void insertPASJMSMessage(Connection con, String payload) 
            throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(con);
        dataRequest.setStatementID("PAS_POST_A_MESSAGE");

        HashMap inputParams = new HashMap();
        inputParams.put("IN_QUEUE_NAME", PASConstants.PAS_COMMAND_QUEUE);
        inputParams.put("IN_CORRELATION_ID", payload);
        inputParams.put("IN_PAYLOAD", payload);
        inputParams.put("IN_DELAY_SECONDS", 0L);
        dataRequest.setInputParams(inputParams);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map)dataAccessUtil.execute(dataRequest);

        String status = (String)outputs.get("OUT_STATUS");
        if (status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String)outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }

    }

    /**
     * Delete all the rows for a particular deliveryDate.
     * @param con
     * @param deliveryDate
     * @throws Exception
     */
    public void deleteDeliveryDate(Connection con, Date deliveryDate) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(con);
        dataRequest.setStatementID("REMOVE_DELIVERY_DATE");

        HashMap inputParams = new HashMap();
        java.sql.Date deliveryDateSql = new java.sql.Date(deliveryDate.getTime());
        inputParams.put("IN_DELIVERY_DATE", deliveryDateSql);
        dataRequest.setInputParams(inputParams);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map)dataAccessUtil.execute(dataRequest);

        String status = (String)outputs.get("OUT_STATUS");
        if (status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String)outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }

    }

    /**
     * Make all the data invalid for the purpose of PAS.     * @param con
     * @param deliveryDate
     * @param countryId
     * @param productId
     * @throws Exception
     */
    public void invalidatePASCountryProduct(Connection con, Date deliveryDate, String countryId, String productId) 
            throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(con);
        dataRequest.setStatementID("INVAL_PAS_COUNTRY_PRODUCT_DT");

        HashMap inputParams = new HashMap();
        inputParams.put("IN_COUNTRY_ID", countryId);
        inputParams.put("IN_PRODUCT_ID", productId);
        java.sql.Date deliveryDateSql = new java.sql.Date(deliveryDate.getTime());
        inputParams.put("IN_DELIVERY_DATE", deliveryDateSql);
        
        dataRequest.setInputParams(inputParams);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map)dataAccessUtil.execute(dataRequest);

        String status = (String)outputs.get("OUT_STATUS");
        if (status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String)outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }

    }

    /**
     * Make all the data invalid for the purpose of PAS.     * @param con
     * @param deliveryDate
     * @param vendorId
     * @param productId
     * @param stateCode
     * @throws Exception
     */
    public void invalidatePASVendorProductState(Connection con, Date deliveryDate, 
                                                String vendorId, String productId, String stateCode) 
            throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(con);
        dataRequest.setStatementID("INVAL_PAS_VENDOR_PRODUCT_ST_DT");

        HashMap inputParams = new HashMap();
        inputParams.put("IN_VENDOR_ID", vendorId);
        inputParams.put("IN_PRODUCT_ID", productId);
        inputParams.put("IN_STATE_CODE", stateCode);
        java.sql.Date deliveryDateSql = new java.sql.Date(deliveryDate.getTime());
        inputParams.put("IN_DELIVERY_DATE", deliveryDateSql);
        
        dataRequest.setInputParams(inputParams);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map)dataAccessUtil.execute(dataRequest);

        String status = (String)outputs.get("OUT_STATUS");
        if (status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String)outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }

    }

    /**
     * Make all the data invalid for the purpose of PAS.     * @param con
     * @param deliveryDate
     * @param vendorId
     * @throws Exception
     */
    public void invalidatePASVendor(Connection con, Date deliveryDate, 
                                    String vendorId) 
            throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(con);
        dataRequest.setStatementID("INVAL_PAS_VENDOR_DT");

        HashMap inputParams = new HashMap();
        inputParams.put("IN_VENDOR_ID", vendorId);
        java.sql.Date deliveryDateSql = new java.sql.Date(deliveryDate.getTime());
        inputParams.put("IN_DELIVERY_DATE", deliveryDateSql);
        
        dataRequest.setInputParams(inputParams);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map)dataAccessUtil.execute(dataRequest);

        String status = (String)outputs.get("OUT_STATUS");
        if (status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String)outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }

    }

    /**
     * Make all the data invalid for the purpose of PAS.     * @param con
     * @param deliveryDate
     * @param floristId
     * @throws Exception
     */
    public void invalidatePASFlorist(Connection con, Date deliveryDate, 
                                                String floristId) 
            throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(con);
        dataRequest.setStatementID("INVAL_FLORIST_DT");

        HashMap inputParams = new HashMap();
        inputParams.put("IN_FLORIST_ID", floristId);
        java.sql.Date deliveryDateSql = new java.sql.Date(deliveryDate.getTime());
        inputParams.put("IN_DELIVERY_DATE", deliveryDateSql);
        
        dataRequest.setInputParams(inputParams);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map)dataAccessUtil.execute(dataRequest);

        String status = (String)outputs.get("OUT_STATUS");
        if (status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String)outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }

    }

    /**
     * Make all the data invalid for the purpose of PAS.     * @param con
     * @param deliveryDate
     * @param productId
     * @throws Exception
     */
    public void invalidatePASProduct(Connection con, Date deliveryDate, 
                                                String productId) 
            throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(con);
        dataRequest.setStatementID("INVAL_PAS_PRODUCT_DT");

        HashMap inputParams = new HashMap();
        inputParams.put("IN_PRODUCT_ID", productId);
        java.sql.Date deliveryDateSql = new java.sql.Date(deliveryDate.getTime());
        inputParams.put("IN_DELIVERY_DATE", deliveryDateSql);
        
        dataRequest.setInputParams(inputParams);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map)dataAccessUtil.execute(dataRequest);

        String status = (String)outputs.get("OUT_STATUS");
        if (status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String)outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }

    }

    /**
     * Make all the data invalid for the purpose of PAS.     * @param con
     * @param deliveryDate
     * @param countryId
     * @throws Exception
     */
    public void invalidatePASCountry(Connection con, Date deliveryDate, 
                                                String countryId) 
            throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(con);
        dataRequest.setStatementID("INVAL_PAS_COUNTRY_DT");

        HashMap inputParams = new HashMap();
        inputParams.put("IN_COUNTRY_ID", countryId);
        java.sql.Date deliveryDateSql = new java.sql.Date(deliveryDate.getTime());
        inputParams.put("IN_DELIVERY_DATE", deliveryDateSql);
        
        dataRequest.setInputParams(inputParams);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map)dataAccessUtil.execute(dataRequest);

        String status = (String)outputs.get("OUT_STATUS");
        if (status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String)outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }

    }

    /**
     * Make all the data invalid for the purpose of PAS.
     * @param con
     * @param deliveryDate
     * @param productId
     * @param stateCode
     * @throws Exception
     */
    public void invalidatePASProductState(Connection con, Date deliveryDate, 
                                                String productId, String stateCode) 
            throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(con);
        dataRequest.setStatementID("INVAL_PAS_PRODUCT_STATE_DT");

        HashMap inputParams = new HashMap();
        inputParams.put("IN_PRODUCT_ID", productId);
        inputParams.put("IN_STATE_CODE", stateCode);
        java.sql.Date deliveryDateSql = new java.sql.Date(deliveryDate.getTime());
        inputParams.put("IN_DELIVERY_DATE", deliveryDateSql);
        
        dataRequest.setInputParams(inputParams);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map)dataAccessUtil.execute(dataRequest);

        String status = (String)outputs.get("OUT_STATUS");
        if (status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String)outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }

    }

   /**
     * Test method used to dump out the parms from a map.
     * @param map
     */
    protected void dumpParams(Map map)
    {
        Iterator iterator = map.keySet().iterator();
        while (iterator.hasNext())
        {
            String key = (String) iterator.next();
            logger.debug("Param Dump:  " + key + " " + map.get(key));
        }
    }

    /**
     * Insert or Update a row in PAS_FLORIST_ZIPCODE_DT.
     * @param con
     * @param vo
     * @throws Exception
     */
    public void insertPASFloristZipcode(Connection con, PASFloristZipcodeVO vo) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(con);
        dataRequest.setStatementID("INSERT_PAS_FLORIST_ZIPCODE_DT");

        HashMap inputParams = new HashMap();
        inputParams.put("IN_ZIP_CODE_ID", vo.getZipCode());
        java.sql.Date deliveryDate = new java.sql.Date(vo.getDeliveryDate().getTime());
        inputParams.put("IN_DELIVERY_DATE", deliveryDate);
        inputParams.put("IN_CUTOFF_TIME", vo.getCutoffTime());
        inputParams.put("IN_STATUS_CODE", vo.getStatusCode());
        inputParams.put("IN_FIT_STATUS_CODE", vo.getFitStatusCode());
        dataRequest.setInputParams(inputParams);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map)dataAccessUtil.execute(dataRequest);

        String status = (String)outputs.get("OUT_STATUS");
        if (status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String)outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }

    }

}
