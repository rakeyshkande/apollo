package com.ftd.pas.server.constants;

public interface PASConstants
{
    public final static String PROPERTY_FILE = "pas-config.xml";
    public final static String DATASOURCE_NAME = "DATASOURCE";

    public final static String FTDAPPS_CONFIG_CONTEXT  = "FTDAPPS_PARMS";
    public final static String SHIPPING_CONFIG_CONTEXT = "SHIPPING_PARMS";
    public final static String PAS_CONFIG_CONTEXT      = "PAS_CONFIG";
    
    public final static String INTL_OVERRIDE_DAYS_PARM    = "INTL_OVERRIDE_DAYS";
    public final static String LASTEST_VENDOR_CUTOFF_PARM = "LASTEST_CUTOFF_VENDOR";
    public final static String LATEST_CUTOFF_PARM         = "LATEST_CUTOFF";
    public final static String MAX_DAYS_PARM              = "MAX_DAYS"; 
    public final static String MAX_DAYS_BACK_PARM         = "MAX_DAYS_BACK"; 
    
    public final static String PAS_COMMAND_QUEUE = "OJMS.PAS_COMMAND";
    
    public final static String SHIP_METHOD_ND = "ND";
    public final static String SHIP_METHOD_2D = "2F";
    public final static String SHIP_METHOD_GR = "GR";
    public final static String SHIP_METHOD_SA = "SA";
    
}
