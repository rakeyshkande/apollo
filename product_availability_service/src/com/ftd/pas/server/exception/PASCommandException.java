package com.ftd.pas.server.exception;

public class PASCommandException extends Exception
{
    public PASCommandException()
    {
        super();
    }
    
    public PASCommandException(String message)
    {
        super(message);
    }
}
