package com.ftd.pas.server.exception;

/**
 * Exception meaning the command sent to PAS is invalid.
 */
public class InvalidPASCommandException extends java.lang.Exception
{
    public InvalidPASCommandException()
    {
        super();
    }
    
    public InvalidPASCommandException(String message)
    {
        super(message);
    }
}
