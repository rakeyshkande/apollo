package com.ftd.pas.server.exception;

/**
 * Exception meaning the command arguments sent to PAS are invalid.
 */
public class InvalidPASCommandArgumentException extends Exception
{
    public InvalidPASCommandArgumentException()
    {
        super();
    }

    public InvalidPASCommandArgumentException(String message)
    {
        super(message);
    }
}
