package com.ftd.deployer.client;

import com.ftd.deployer.client.config.EnvironmentConfigJPanel;
import com.ftd.deployer.client.deployer.EnvironmentDeployerJPanel;

import com.ftd.deployer.server.ServerFactory;

import java.awt.*;
import java.awt.event.*;

import java.io.File;

import java.io.IOException;

import java.util.Iterator;
import java.util.Set;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;

import org.apache.log4j.Logger;

public class DeployerFrame extends JFrame
{
    private static Logger logger  = Logger.getLogger(DeployerFrame.class);
    
    private BorderLayout layoutMain = new BorderLayout();
    private JMenuBar menuBar = new JMenuBar();
    private JMenu menuFile = new JMenu();
    private JMenuItem menuFileExit = new JMenuItem();
    private JMenu menuDeploy = new JMenu();
    private JMenuItem menuDeployDeploy = new JMenuItem();
    private JMenuItem menuDeployConfig = new JMenuItem();
    private JMenu menuHelp = new JMenu();
    private JMenuItem menuHelpAbout = new JMenuItem();
    private JLabel statusBar = new JLabel();
    
    private JTabbedPane centerJTabbedPane = null;
    
    private EnvironmentConfigJPanel configJPanel = null;
    
    private ClientProperties clientProperties;

    public DeployerFrame()
    {
        try
        {
            jbInit();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void jbInit() throws Exception
    {
        this.clientProperties = ClientProperties.getInstance();
        this.setJMenuBar( menuBar );
        this.getContentPane().setLayout( layoutMain );
        

        this.setSize( new Dimension(800, 600) );
        this.setTitle("Deployer");
        menuFile.setText( "File" );
        menuFileExit.setText( "Exit" );
        menuFileExit.addActionListener( new ActionListener() { public void actionPerformed( ActionEvent ae ) { fileExit_ActionPerformed( ae ); } } );
        menuDeploy.setText( "Deploy" );
        menuDeployDeploy.setText( "Deploy" );
        menuDeployDeploy.addActionListener( new ActionListener() 
        { public void actionPerformed( ActionEvent ae ) { deployDeploy_ActionPerformed( ae ); } } );
        menuDeployConfig.setText( "Config" );
        menuDeployConfig.addActionListener( new ActionListener() { public void actionPerformed( ActionEvent ae ) { deployConfig_ActionPerformed( ae ); } } );
        menuHelp.setText( "Help" );
        menuHelpAbout.setText( "About" );
        menuHelpAbout.addActionListener( new ActionListener() { public void actionPerformed( ActionEvent ae ) { helpAbout_ActionPerformed( ae ); } } );
        statusBar.setText( "" );
        menuFile.add( menuFileExit );
        menuBar.add( menuFile );
        menuDeploy.add(menuDeployDeploy);
        menuDeploy.add(menuDeployConfig);
        menuBar.add(menuDeploy);
        menuHelp.add( menuHelpAbout );
        menuBar.add( menuHelp );
        this.getContentPane().add( statusBar, BorderLayout.SOUTH );

        centerJTabbedPane = new JTabbedPane();
        this.getContentPane().add(centerJTabbedPane, BorderLayout.CENTER );
        
        selectConfigFile();
    }

    protected void fileExit_ActionPerformed(ActionEvent e)
    {
        System.exit(0);
    }

    protected void helpAbout_ActionPerformed(ActionEvent e)
    {
        JOptionPane.showMessageDialog(this, new DeployerFrame_AboutBoxPanel1(), "About", JOptionPane.PLAIN_MESSAGE);
    }

    protected void deployDeploy_ActionPerformed(ActionEvent e)
    {
        String[] environments;
        Set environmentSet = ServerFactory.getConfigAPI().getEnvironmentList();
        environments = new String[environmentSet.size()];
        Iterator iterator = environmentSet.iterator();
        int i =0;
        while (iterator.hasNext())
        {
            String envName = (String) iterator.next();
            environments[i++] = envName;
        }
        
        // Select a configuration
        String envPicked = (String) JOptionPane.showInputDialog(null,
                                                     "Select an environment",
                                                     "Environment Selection",
                                                     JOptionPane.QUESTION_MESSAGE,
                                                     null,
                                                     environments,
                                                     null);
         
         File earFileLocation = selectEarFileLocation(envPicked);
         
         // Put the deploy screen on the front page
         EnvironmentDeployerJPanel envDeployerJPanel = new EnvironmentDeployerJPanel(envPicked,earFileLocation);

         centerJTabbedPane.add("Deploy " + envPicked,envDeployerJPanel);
         this.validate();
    }

    protected void deployConfig_ActionPerformed(ActionEvent e)
    {
        // Put the config screen on the front page
        if (configJPanel == null)
        {
            configJPanel = new EnvironmentConfigJPanel();
            centerJTabbedPane.add("Config",configJPanel);
        }
        else
        {
            JOptionPane.showMessageDialog(this, "Config Panel is already open.  Click on the config Tab.", "Config Already Open", JOptionPane.PLAIN_MESSAGE);
        }
    }

    /**
     * Select the configuration file to be used to read the config data.
     */
    protected void selectConfigFile()
    {
        JFileChooser chooser = new JFileChooser();
        FileFilter filter = new FileFilter()
        {
                public boolean accept(File file)
                {
                    if (file.isDirectory())
                    {
                        return true;
                    }
                    else if (file.getName().endsWith(".xml"))
                    {
                        return true;
                    }
                    return false;
                }

                public String getDescription()
                {
                    return "XML files";
                }
            };
        chooser.setFileFilter(filter);
        chooser.setDialogTitle("Select Configuration File");
        String defaultDir = ClientProperties.getInstance().getDefaultConfigFileLocation();
        File defaultLocation = new File(defaultDir);
        chooser.setCurrentDirectory(defaultLocation);
        int returnVal = chooser.showOpenDialog(null);
        File selectedFile = null;
        if(returnVal == JFileChooser.APPROVE_OPTION) 
        {
            selectedFile = chooser.getSelectedFile();
            logger.info("You chose to open this file: " +
                selectedFile.getName());
            if (!selectedFile.equals(defaultLocation))
            {
                ClientProperties.getInstance().setDefaultConfigFileLocation(selectedFile.getAbsolutePath());
            }
        }
        else
        {
            System.exit(0);
        }
        
        try
        {
            ServerFactory.getConfigAPI().initialize(selectedFile);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.exit(0);
        }
    }
    
    /**
     * Select the location to be used to retrieve the ear files.
     */
    protected File selectEarFileLocation(String envPicked)
    {
        JFileChooser chooser = new JFileChooser();
        chooser.setDialogTitle("Select ear directory");
        String defaultDir = ClientProperties.getInstance().getDefaultEarFileLocation(envPicked);
        File defaultLocation = new File(defaultDir);
        chooser.setCurrentDirectory(defaultLocation);
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int returnVal = chooser.showOpenDialog(null);
        File earFileDirectory = null;
        if(returnVal == JFileChooser.APPROVE_OPTION) 
        {
            earFileDirectory = chooser.getSelectedFile();
            if (!defaultDir.equals(earFileDirectory))
            {
                ClientProperties.getInstance().setDefaultEarFileLocation(earFileDirectory.getAbsolutePath(), envPicked);
            }
            return earFileDirectory;
        }
        else
        {
            System.exit(0);
        }
        
        return null;
    }

}
