package com.ftd.deployer.client;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import java.io.IOException;

import java.util.Properties;

import org.apache.log4j.Logger;


public class ClientProperties
{
    private static Logger logger  = Logger.getLogger(ClientProperties.class);

    private static ClientProperties instance;
    private Properties properties;
    private String homeLocation;
    private File   propertiesFile;
    
    private final static String DEFAULT_CONFIG_FILE_LOCATION_PROPERTY = "default.config.file.location";
    private final static String DEFAULT_EAR_FILE_LOCATION_PROPERTY    = "default.ear.file.location";
    
    private final static String FILENAME = "deploytool.properties";

    
    
    private ClientProperties()
    {
        init();
    }
    
    public synchronized static ClientProperties getInstance()
    {
        if (instance == null)
        {
            instance = new ClientProperties();
        }
        return instance;
    }
    
    public String getDefaultConfigFileLocation()
    {
        String propertyValue = properties.getProperty(DEFAULT_CONFIG_FILE_LOCATION_PROPERTY,"/FTD_PROJECTS/FTD-Current/FTD/deploy_tool");
        return propertyValue;
    }

    public void setDefaultConfigFileLocation(String configFileLocation)
    {
        properties.setProperty(DEFAULT_CONFIG_FILE_LOCATION_PROPERTY,configFileLocation);
        saveProperties();
    }
    
    public String getDefaultEarFileLocation(String environment)
    {
        String key = DEFAULT_EAR_FILE_LOCATION_PROPERTY + "." + environment;
        String propertyValue = properties.getProperty(key,"/FTD_PROJECTS/FTD-Current/FTD/dist");
        return propertyValue;
    }

    public void setDefaultEarFileLocation(String earFileLocation,String environment)
    {
        String key = DEFAULT_EAR_FILE_LOCATION_PROPERTY + "." + environment;
        properties.setProperty(key,earFileLocation);
        saveProperties();
    }
    
    /**
     * Read the properties from the default location.  If none exists, create one.
     */
    private void init()
    {
        String homeLocation = getHomeLocation();
        propertiesFile = new File(homeLocation);
        properties = new Properties();
        readProperties();
    }

    private void saveProperties()
    {   
        try
        {
            FileOutputStream fos = new FileOutputStream(propertiesFile);
            properties.store(fos, "Default Properties");    
        }
        catch (IOException e)
        {
            logger.error("Error saving properties file",e);
        }
    }

    private void readProperties()
    {   
        try
        {
            if (propertiesFile.exists())
            {
                FileInputStream fis = new FileInputStream(propertiesFile);
                properties.load(fis);    
            }
        }
        catch (IOException e)
        {
            logger.error("Error reading properties file",e);
        }
    }
    
    /**
     * Get the home location where we are going to store the properties.
     * This is retrieved from the environment variables.
     * @return
     */
    private String getHomeLocation()
    {
        if (homeLocation == null)
        {
                
            homeLocation = "./";
            String userProfile = System.getenv("USERPROFILE");
            if (userProfile != null && !userProfile.equals(""))
            {
                homeLocation = userProfile;
            }
            else
            {  
                String userHome = System.getenv("HOME");
                if (userHome != null && !userHome.equals(""))
                {
                    homeLocation = userHome;
                }
            }
            homeLocation += "/" + FILENAME;
        }        
        logger.debug("Home location is [" + homeLocation + "]");
        return homeLocation;
    }

    
}
