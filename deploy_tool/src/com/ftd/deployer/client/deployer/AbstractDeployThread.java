package com.ftd.deployer.client.deployer;

import com.ftd.deployer.server.DeployAPI;
import com.ftd.deployer.server.ServerFactory;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.deploy.spi.status.ProgressListener;
import javax.enterprise.deploy.spi.status.ProgressObject;

import org.apache.log4j.Logger;

public abstract class AbstractDeployThread extends Thread
{
    private static Logger logger  = Logger.getLogger(AbstractDeployThread.class);

    protected List deployments;
    protected List listeners;
    protected List progressListeners;
    protected ProgressEventDispatcher dispatcher;

    public AbstractDeployThread()
    {
        deployments = new ArrayList();
        listeners = new ArrayList();
        progressListeners = new ArrayList();
        dispatcher = ProgressEventDispatcher.getInstance();
    }

    public void addDeploy(String serverName, String instanceName, String password, String earFilePath, String deploymentName, 
                          String parentDeploymentName, ProgressListener listener, String serverType)
    {
        ProgressEventListenerAdapter listenerAdapter = new ProgressEventListenerAdapter(serverName,deploymentName);
        dispatcher.addListener(serverName,deploymentName,listener);
        DeployObject deployObject = 
            new DeployObject(serverName, instanceName, password, earFilePath, deploymentName, parentDeploymentName, listenerAdapter, serverType);
        deployments.add(deployObject);
        
        // Set the Thread's name so the logs are separated
        // Assumes this thread always works against the same serverName as that is what getHostname does anyway
        setName(getClass().getSimpleName() + "-" + getHostname());
    }

    public int getNumberOfDeploymentSteps()
    {
        // There 1 deployment step, and 2 pre and 2 post deployments
        return deployments.size() + deployments.size()* 4; // We want progress to reflect Pre and Post Deploy steps as well
    }
    
    public List getDeploymentList()
    {
        return deployments;
    }

    public void addDeployListener(DeployThreadListener threadListener)
    {
        listeners.add(threadListener);
    }

    public String getHostname()
    {
        DeployObject deployObject = (DeployObject)deployments.get(0);
        return deployObject.serverName;
    }

    public void run()
    {
        try
        {
            preDeploy();
            
            for (int i = 0; i < deployments.size(); i++)
            {
                DeployObject dep = (DeployObject)deployments.get(i);
                try
                {
                    DeployAPI deployAPI = ServerFactory.getDeployAPI(dep.getServerType());
                    ProgressObject progress = executeDeploy(dep,deployAPI);
                    dep.addProgressObject(progress);
    
                    while (stillRunning(progress))
                    {
                        this.sleep(500);
                    }
    
                }
                catch (javax.enterprise.deploy.spi.exceptions.DeploymentManagerCreationException e)
                {
                    logger.error("Error Deploying ",e);
                    logger.error("PASSWORD FAILURE");
                    try
                    {
                      logger.error("Firing deployment failed event");
                      fireDeploymentFailedEvent(dep.getServerName(),dep.getDeploymentName(),e);
                    }
                    catch (Exception f)
                    {
                      logger.error("Could not fire event ",f);
                    }
                }
                catch (javax.naming.AuthenticationException e)
                {
                    logger.error("Error Deploying ",e);
                    logger.error("PASSWORD FAILURE");
                    fireDeploymentFailedEvent(dep.getServerName(),dep.getDeploymentName(),e);
                }
                catch (Exception e)
                {
                    logger.error("Error Deploying ",e);
                    fireDeploymentFailedEvent(dep.getServerName(),dep.getDeploymentName(),e);
                } finally
                {
                    // Finished a deploy, fire an event
                    fireDeploymentCompleteEvent(dep.getServerName(),dep.getDeploymentName());
                }
            }
            
            postDeploy();

        } catch (Exception e)
        {
            logger.error("Error Deploying ",e);
        }
        fireThreadCompleteEvent(getHostname());

    }

    public boolean stillRunning(ProgressObject progress)
    {
        if (progress.getDeploymentStatus().isRunning())
        {
            return true;
        } else
        {
            return false;
        }
    }

    protected void fireDeploymentCompleteEvent(String server, String deploymentName)
    {
        fireEvent(new ThreadDeployEvent(ThreadDeployEvent.STATUS_DEPLOYMENT_COMPLETE, server, deploymentName));
    }

    protected void fireDeploymentFailedEvent(String server, String deploymentName, Exception e)
    {
        fireEvent(new ThreadDeployEvent(ThreadDeployEvent.STATUS_DEPLOYMENT_FAILED, server,deploymentName,e));
    }

    protected void fireThreadCompleteEvent(String server)
    {
        fireEvent(new ThreadDeployEvent(ThreadDeployEvent.STATUS_THREAD_COMPLETE, server));
    }

    protected void fireEvent(ThreadDeployEvent event)
    {
        for (int i = 0; i < listeners.size(); i++)
        {
            DeployThreadListener listener = (DeployThreadListener)listeners.get(i);
            listener.threadDeployEvent(event);
        }

    }
    
    /**
     * Kicks off the Pre Deployment Process. This is a linear process.
     *  We need to handle pre-deploy for shutting down container's etc.
     * By design, make this a linear process as we want to shut down
     * All containers up front for the server.
     * This initiates the shutdown process then checks for completion
     * First, executePreDeploy is called on all of the items.
     * Then, verifyPreDeploy is called on all of the items.
     */
    protected void preDeploy()
    {
        for (int i = 0; i < deployments.size(); i++)
        {
            DeployObject dep = (DeployObject)deployments.get(i);
            try
            {
                DeployAPI deployAPI = ServerFactory.getDeployAPI(dep.getServerType());
                executePreDeploy(dep,deployAPI);
            } catch (Exception e)
            {
                logger.error("Error during Pre-Deployment ",e);
                fireDeploymentFailedEvent(dep.getServerName(),dep.getDeploymentName(),e);
            } finally
            {
                // Finished a pre-deploy, fire an event to increment the progress
                fireDeploymentCompleteEvent(dep.getServerName(),dep.getDeploymentName());
            }
        }
        
        for (int i = 0; i < deployments.size(); i++)
        {
            DeployObject dep = (DeployObject)deployments.get(i);
            try
            {
                DeployAPI deployAPI = ServerFactory.getDeployAPI(dep.getServerType());
                verifyPreDeploy(dep,deployAPI);
            } catch (Exception e)
            {
                logger.error("Error during Pre-Deployment ",e);
                fireDeploymentFailedEvent(dep.getServerName(),dep.getDeploymentName(),e);
            } finally
            {
                // Finished a pre-deploy, fire an event to increment the progress
                fireDeploymentCompleteEvent(dep.getServerName(),dep.getDeploymentName());
            }
        }        
    }
    

    /**
     * Kicks off the PostDeployment Process. This is a linear process.
     * We need to handle post-deploy for restarting container's etc.
     * By design, make this a linear process as we want to start up
     * All applicable containers .
     * This initiates the startup process then checks for completion
     * First, executePostDeploy is called on all of the items.
     * Then, verifyPostDeploy is called on all of the items.
     */
    protected void postDeploy()
    {
        for (int i = 0; i < deployments.size(); i++)
        {
            DeployObject dep = (DeployObject)deployments.get(i);
            try
            {
                DeployAPI deployAPI = ServerFactory.getDeployAPI(dep.getServerType());
                executePostDeploy(dep,deployAPI);
            } catch (Exception e)
            {
                logger.error("Error during Post-Deployment startup",e);
                fireDeploymentFailedEvent(dep.getServerName(),dep.getDeploymentName(),e);
            } finally
            {
                // Finished a post-deploy, fire an event to increment the progress
                fireDeploymentCompleteEvent(dep.getServerName(),dep.getDeploymentName());
            }
        }
        
        for (int i = 0; i < deployments.size(); i++)
        {
            DeployObject dep = (DeployObject)deployments.get(i);
            try
            {
                DeployAPI deployAPI = ServerFactory.getDeployAPI(dep.getServerType());
                verifyPostDeploy(dep,deployAPI);
            } catch (Exception e)
            {
                logger.error("Error during Post-Deployment startup",e);
                fireDeploymentFailedEvent(dep.getServerName(),dep.getDeploymentName(),e);
            } finally
            {
                // Finished a post-deploy, fire an event to increment the progress
                   fireDeploymentCompleteEvent(dep.getServerName(),dep.getDeploymentName());
               }
        }        
    }    
    
    
    protected abstract ProgressObject executeDeploy(DeployObject dep,DeployAPI deployAPI) throws Exception;      
   
    protected void executePreDeploy(DeployObject dep,DeployAPI deployAPI) throws Exception
    {
        // By default, this is a NOP
    }
    
    protected void executePostDeploy(DeployObject dep,DeployAPI deployAPI) throws Exception
    {
        // By default, this is a NOP
    }
    
    protected void verifyPreDeploy(DeployObject dep,DeployAPI deployAPI) throws Exception
    {
        // By default, this is a NOP
    }
    
    protected void verifyPostDeploy(DeployObject dep,DeployAPI deployAPI) throws Exception
    {
        // By default, this is a NOP
    }    

}
