package com.ftd.deployer.client.deployer;

public interface DeployThreadListener
{
    void threadDeployEvent(ThreadDeployEvent event);
}
