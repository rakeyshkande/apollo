package com.ftd.deployer.client.deployer;

import com.ftd.deployer.server.DeployAPI;

import javax.enterprise.deploy.spi.status.ProgressObject;

/**
 * Extends GetStatusThread so that it does a status after the startup completes.
 */
public class StartupContainersThread extends GetStatusThread
{
    
    protected void executePreDeploy(DeployObject dep,DeployAPI deployAPI) throws Exception
    {
        deployAPI.invokeStartup(dep.serverName, dep.instanceName, dep.password, dep.listener);                
    }
    
    protected ProgressObject executeDeploy(DeployObject dep,DeployAPI deployAPI) throws Exception
    {
        deployAPI.verifyStartup(dep.serverName, dep.instanceName, dep.password, dep.listener);
        return super.executeDeploy(dep,deployAPI);
    }

}
