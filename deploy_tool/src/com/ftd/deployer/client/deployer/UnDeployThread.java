package com.ftd.deployer.client.deployer;

import com.ftd.deployer.server.DeployAPI;

import javax.enterprise.deploy.spi.status.ProgressObject;

public class UnDeployThread extends AbstractDeployThread
{
    protected ProgressObject executeDeploy(DeployObject dep,DeployAPI deployAPI) throws Exception
    {
        ProgressObject progress = deployAPI.unDeploy(dep.serverName,dep.instanceName,dep.password,dep.deploymentName,dep.listener, dep.getEarFileName());
        return progress;
    }
    
    protected void executePreDeploy(DeployObject dep,DeployAPI deployAPI) throws Exception
    {
        deployAPI.preUnDeployStart(dep.serverName,dep.instanceName,dep.password,dep.earFilePath,dep.deploymentName,dep.parentDeploymentName, dep.listener);
    }
    
    protected void verifyPreDeploy(DeployObject dep,DeployAPI deployAPI) throws Exception
    {
        deployAPI.preUnDeployComplete(dep.serverName,dep.instanceName,dep.password,dep.earFilePath,dep.deploymentName,dep.parentDeploymentName, dep.listener);
    }    

    protected void executePostDeploy(DeployObject dep,DeployAPI deployAPI) throws Exception
    {
        deployAPI.postUnDeployStart(dep.serverName,dep.instanceName,dep.password,dep.earFilePath,dep.deploymentName,dep.parentDeploymentName, dep.listener);
    }    
    
    protected void verifyPostDeploy(DeployObject dep,DeployAPI deployAPI) throws Exception
    {
        deployAPI.postUnDeployComplete(dep.serverName,dep.instanceName,dep.password,dep.earFilePath,dep.deploymentName,dep.parentDeploymentName, dep.listener);
    }        

}
