package com.ftd.deployer.client.deployer;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTabbedPane;

public class DeployThreadManagerJPanel extends JPanel
{
    protected List threads;
    protected JTabbedPane threadJTabbedPane;
    
    protected JPanel progressBarJPanel;
    
    public DeployThreadManagerJPanel()
    {
        init();
    }
    
    public void addDeployThread(DeployThread thread)
    {
        threads.add(thread);
        addThreadPanel(thread);
    }
    
    protected void init()
    {
        threads = new ArrayList();
        
        this.setLayout(new BorderLayout());
        
        progressBarJPanel = new JPanel();
        progressBarJPanel.setLayout(new GridLayout(0,2));
        this.add(progressBarJPanel, BorderLayout.NORTH);
        
        threadJTabbedPane = new JTabbedPane();
        this.add(threadJTabbedPane,BorderLayout.CENTER);        
        
    }
    
    protected void addThreadPanel(AbstractDeployThread thread)
    {
        JProgressBar progressBar = new JProgressBar();
        addProgressBar(thread.getHostname(), progressBar);
        
        DeployThreadJPanel threadPanel = new DeployThreadJPanel(thread, progressBar);
        threadJTabbedPane.add(threadPanel, threadPanel.getTabName());
        
    }
    
    protected void addProgressBar(String hostName, JProgressBar progressBar)
    {
        progressBarJPanel.add(new JLabel(hostName));
        progressBarJPanel.add(progressBar);
    }
}
