package com.ftd.deployer.client.deployer;

import java.io.File;

import javax.enterprise.deploy.spi.status.ProgressListener;
import javax.enterprise.deploy.spi.status.ProgressObject;

public class DeployObject
{
    protected String serverName;
    protected String instanceName;
    protected String password;
    protected String earFilePath;
    protected String deploymentName;
    protected String parentDeploymentName;
    protected ProgressListener listener;
    protected ProgressObject progress;
    protected String serverType;

    protected DeployObject(String serverName, String instanceName, String password, String earFilePath, 
                           String deploymentName, ProgressListener listener, String serverType)
    {
        this.serverName = serverName;
        this.instanceName = instanceName;
        this.password = password;
        this.earFilePath = earFilePath;
        this.deploymentName = deploymentName;
        this.listener = listener;
        this.serverType = serverType;
    }

    protected DeployObject(String serverName, String instanceName, String password, String earFilePath, 
                           String deploymentName, String parentDeploymentName, ProgressListener listener, String serverType)
    {
        this.serverName = serverName;
        this.instanceName = instanceName;
        this.password = password;
        this.earFilePath = earFilePath;
        this.deploymentName = deploymentName;
        this.parentDeploymentName = parentDeploymentName;
        this.listener = listener;
        this.serverType = serverType;
    }

    protected void addProgressObject(ProgressObject progress)
    {
        this.progress = progress;
    }

    public void setServerName(String serverName)
    {
        this.serverName = serverName;
    }

    public String getServerName()
    {
        return serverName;
    }

    public void setInstanceName(String instanceName)
    {
        this.instanceName = instanceName;
    }

    public String getInstanceName()
    {
        return instanceName;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getPassword()
    {
        return password;
    }

    public void setEarFilePath(String earFilePath)
    {
        this.earFilePath = earFilePath;
    }

    public String getEarFilePath()
    {
        return earFilePath;
    }
    
    /**
     * @return The name portion of the ear file Path
     */
    public String getEarFileName()
    {
        File earFile = new File(earFilePath);
        return earFile.getName();        
    }

    public void setDeploymentName(String deploymentName)
    {
        this.deploymentName = deploymentName;
    }

    public String getDeploymentName()
    {
        return deploymentName;
    }
    public String getParentDeploymentName()
    {
        return parentDeploymentName;
    }
    public boolean hasParent()
    {
        return (parentDeploymentName != null);
    }

    public String getServerType()
    {
        return serverType;
    }

    public void setServerType(String serverType)
    {
        this.serverType = serverType;
    }
}
