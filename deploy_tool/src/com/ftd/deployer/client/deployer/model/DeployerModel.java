package com.ftd.deployer.client.deployer.model;

import com.ftd.deployer.vo.ApplicationConfigVO;
import com.ftd.deployer.vo.EnvironmentConfigVO;

import com.ftd.deployer.vo.ServerConfigVO;

import java.io.File;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DeployerModel
{
    protected Map  applicationMap;
    
    protected EnvironmentConfigVO environmentConfig;
    protected File                earFileLocation;
    
    public DeployerModel(EnvironmentConfigVO environmentConfig, File earFileLocation)
    {
        this.earFileLocation = earFileLocation;
        applicationMap = new HashMap();
        setData(environmentConfig);
    }
    
    /**
     * Dump the current model and replace it with a new environment Config
     * @param environmentConfig
     */
    public void setData(EnvironmentConfigVO environmentConfig)
    {
        this.environmentConfig = environmentConfig;
        
        List servers = environmentConfig.getServers();
        for (int i=0; i < servers.size();i++)
        {
            ServerConfigVO server = (ServerConfigVO) servers.get(i);  
            addServer(server);
        }
    }
    
    
    public Collection getApplicationSet()
    {
        return applicationMap.values();
    }
    
    public List getServerListByApplication(String applicationName)
    {
        return new ArrayList();
    }
    
    
    protected void addServer(ServerConfigVO server)
    {
        List applications = server.getApplications();
        for (int i=0; i < applications.size();i++)
        {
            ApplicationConfigVO application = (ApplicationConfigVO) applications.get(i);
            DeploymentDetailModel detailModel = new DeploymentDetailModel();
            detailModel.setEarFileName(application.getEarFileName());
            detailModel.setDeployName(application.getDeployName());
            detailModel.setContainerName(application.getContainerName());
            detailModel.setHasParent(application.isHasParent());
            detailModel.setParentApplication(application.getParentApplication());
            detailModel.setName(server.getName());
            detailModel.setHostname(server.getHostname());
            detailModel.setEarFileLocation(earFileLocation);
            detailModel.setServerType(server.getServerType());
            
            if (!applicationMap.containsKey(detailModel.getEarFileName()))
            {
                ApplicationDetailModel applicationModel = new ApplicationDetailModel();
                applicationModel.setName(detailModel.getEarFileName());
                applicationMap.put(applicationModel.getName(),applicationModel);
            }
            ApplicationDetailModel applicationModel = (ApplicationDetailModel) applicationMap.get(detailModel.getEarFileName());
            applicationModel.addServer(detailModel);
        }
        
    }
}
