package com.ftd.deployer.client.deployer.model;

import java.util.ArrayList;
import java.util.List;

public class ApplicationDetailModel implements Comparable
{
    protected List   serverList;
    protected String name;
    
    public ApplicationDetailModel()
    {
        serverList = new ArrayList();
    }
    
    public void addServer(DeploymentDetailModel serverModel)
    {
        serverList.add(serverModel);
    }
    
    public List getServerList()
    {
        return serverList;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public int compareTo(Object o)
    {
        if (o instanceof ApplicationDetailModel)
        {
            ApplicationDetailModel otherModel = (ApplicationDetailModel) o;
            return this.name.compareTo(otherModel.name);
        }
        
        return 0;
    }
}
