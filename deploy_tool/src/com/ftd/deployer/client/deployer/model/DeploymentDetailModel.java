package com.ftd.deployer.client.deployer.model;

import java.io.File;

public class DeploymentDetailModel implements Comparable
{
    protected String  earFileName;
    protected String  deployName;
    protected String  containerName;
    protected boolean hasParent;
    protected String  parentApplication;
    protected String  name;
    protected String  hostname;
    protected File    earFileLocation;
    protected String  serverType;
    
    public DeploymentDetailModel()
    {
    }


    public void setEarFileName(String earFileName)
    {
        this.earFileName = earFileName;
    }

    public String getEarFileName()
    {
        return earFileName;
    }

    public void setDeployName(String deployName)
    {
        this.deployName = deployName;
    }

    public String getDeployName()
    {
        return deployName;
    }

    public void setContainerName(String containerName)
    {
        this.containerName = containerName;
    }

    public String getContainerName()
    {
        return containerName;
    }

    public void setHasParent(boolean hasParent)
    {
        this.hasParent = hasParent;
    }

    public boolean isHasParent()
    {
        return hasParent;
    }

    public void setParentApplication(String parentApplication)
    {
        this.parentApplication = parentApplication;
    }

    public String getParentApplication()
    {
        return parentApplication;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public void setHostname(String hostname)
    {
        this.hostname = hostname;
    }

    public String getHostname()
    {
        return hostname;
    }

    public void setEarFileLocation(File earFileLocation)
    {
        this.earFileLocation = earFileLocation;
    }

    public File getEarFileLocation()
    {
        return earFileLocation;
    }
    
    public String getFullEarFilePath()
    {
        StringBuffer fullPath = new StringBuffer();
        fullPath.append(earFileLocation.getAbsolutePath());
        fullPath.append(File.separator);
        fullPath.append(earFileName);

        return fullPath.toString();
    }
    
    public int compareTo(Object o)
    {
        if (o instanceof DeploymentDetailModel)
        {
            DeploymentDetailModel otherModel = (DeploymentDetailModel) o;
            return this.deployName.compareTo(otherModel.deployName);
        }
        
        return 0;
    }


    public String getServerType()
    {
        return serverType;
    }


    public void setServerType(String serverType)
    {
        this.serverType = serverType;
    }
}
