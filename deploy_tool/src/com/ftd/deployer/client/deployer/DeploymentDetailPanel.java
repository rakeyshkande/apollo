package com.ftd.deployer.client.deployer;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import com.ftd.deployer.client.deployer.model.DeploymentDetailModel;

public class DeploymentDetailPanel 
{
    protected static String oraclePassword;
    protected DeploymentDetailModel model;
    protected DeploymentListener    deploymentListener;
    
    protected JCheckBox deployJCheckBox;
    protected JLabel    hostJLabel;
    protected JLabel    containerJLabel;
    protected JButton   statusJButton;
    protected JButton   deployNowJButton;
    protected JButton   unDeployNowJButton;
    private final JPasswordField passwordField = new JPasswordField(30);
    protected JTextField statusInfoField;

    public DeploymentDetailPanel(DeploymentDetailModel model)
    {
        this.model = model;
        init();
    }
        
    /**
     * This is an event from the deployment Listener telling us to update our status.
     * This needs to be put on the swing thread and run from there.
     */
    public void updateDeploymentStatus()
    {
        
        Runnable doUpdateStatus = new Runnable() 
        {
            public void run() 
            {
                Color statusColor = Color.GRAY;
                if (deploymentListener != null)
                {
                    if (deploymentListener.isFailed())
                    {
                        statusColor = Color.RED;
                    }
                    else if (deploymentListener.isInProgess())
                    {
                        statusColor = Color.ORANGE;
                    }
                    else if (deploymentListener.isComplete())
                    {
                        statusColor = Color.GREEN;
                    }
                    else
                    {
                        statusColor = Color.PINK;
                    }
                    
                    statusInfoField.setText(deploymentListener.getDeploymentInfo());
                }
                else
                {
                    statusColor = Color.PINK;
                }
                statusJButton.setForeground(statusColor);
            }
         };

         SwingUtilities.invokeLater(doUpdateStatus);
    }
    
    /**
     * Reset all the coloring for the status.  It is assumed this is off the swing thread.
     */
    public void resetDeploymentStatus()
    {
        
        Runnable doUpdateStatus = new Runnable() 
        {
            public void run() 
            {
                Color statusColor = Color.BLACK;
                statusJButton.setForeground(statusColor);
            }
         };

         SwingUtilities.invokeLater(doUpdateStatus);
    }
    
    protected void init()
    {
        deployJCheckBox = new JCheckBox();
        hostJLabel = new JLabel(model.getHostname());
        containerJLabel = new JLabel(model.getServerType() + ":" + model.getContainerName());
        statusJButton = new JButton("Status");
        deployNowJButton = new JButton("Deploy Now");
        unDeployNowJButton = new JButton("UnDeploy Now");
        statusInfoField = new JTextField("Deploy Info: Unknown                             ");
        statusInfoField.setEditable(false);
        statusInfoField.setColumns(80);
        
        deployNowJButton.addActionListener(new ActionListener()
        {

            public void actionPerformed(ActionEvent e)
            {
                deployButtonPressed();
            }
        });
        statusJButton.addActionListener(new ActionListener()
        {

            public void actionPerformed(ActionEvent e)
            {
                statusButtonPressed();
            }
        });
        unDeployNowJButton.addActionListener(new ActionListener()
        {

            public void actionPerformed(ActionEvent e)
            {
                unDeployButtonPressed();
            }
        });
    }
    
    protected void deployButtonPressed()
    {
        int confirmation = JOptionPane.showConfirmDialog(null,"Confirm Deployment");

        if (confirmation == JOptionPane.YES_OPTION)
        {
            executeSingleDeployment();
        }
    }

    protected void unDeployButtonPressed()
    {
        int confirmation = JOptionPane.showConfirmDialog(null,"Confirm UnDeployment");

        if (confirmation == JOptionPane.YES_OPTION)
        {
            executeSingleUnDeployment();
        }
    }
    
    protected void executeSingleDeployment()
    {            
        // Move off the swing thread
        DeployThread deployThread = new DeployThread();
        if(addDeployment(deployThread))
        {
          deployThread.start();
        }
    }

    protected void executeSingleUnDeployment()
    {       
        // Move off the swing thread
        UnDeployThread deployThread = new UnDeployThread();
        if(addDeployment(deployThread))
        {
          deployThread.start();
        }
    }
    
    public Boolean addDeployment(AbstractDeployThread deployThread)
    {
        if (this.oraclePassword == "" || this.oraclePassword == null)
        {
            String errorMessage = "";
            if(!setOraclePasswordDialog(model.getContainerName(), model.getHostname(), errorMessage))
            {
                return false;
            }
        }
        deploymentListener = new DeploymentListener(this);
        deployThread.addDeploy(model.getHostname(),model.getContainerName(),this.oraclePassword,model.getFullEarFilePath(),model.getDeployName(),model.getParentApplication(),deploymentListener, model.getServerType());
        return true;
    }
    
    protected void statusButtonPressed()
    {
        if (deploymentListener != null)
        {
            String deploymentLog = deploymentListener.getDeploymentLog();
            //JOptionPane.showMessageDialog(null,deploymentLog,"Deployment Log",JOptionPane.INFORMATION_MESSAGE);
            MessageDialog.showMessageDialog(null,deploymentLog,"Deployment Log",JOptionPane.INFORMATION_MESSAGE);
        }
    }

    public void setDeployJCheckBox(JCheckBox deployJCheckBox)
    {
        this.deployJCheckBox = deployJCheckBox;
    }

    public JCheckBox getDeployJCheckBox()
    {
        return deployJCheckBox;
    }

    public void setHostJLabel(JLabel hostJLabel)
    {
        this.hostJLabel = hostJLabel;
    }

    public JLabel getHostJLabel()
    {
        return hostJLabel;
    }

    public void setContainerJLabel(JLabel containerJLabel)
    {
        this.containerJLabel = containerJLabel;
    }

    public JLabel getContainerJLabel()
    {
        return containerJLabel;
    }

    public void setStatusJButton(JButton statusJButton)
    {
        this.statusJButton = statusJButton;
    }

    public JButton getStatusJButton()
    {
        return statusJButton;
    }

    public void setDeployNowJButton(JButton deployNowJButton)
    {
        this.deployNowJButton = deployNowJButton;
    }

    public JButton getDeployNowJButton()
    {
        return deployNowJButton;
    }
    public JButton getUnDeployNowJButton()
    {
        return unDeployNowJButton;
    }
    public Boolean setOraclePasswordDialog(String applicationName, String environmentName, String errorMessage)
    {
        char [] password = null;
        Boolean passwordEntered = false;

        passwordField.setText(this.oraclePassword);
        JPanel messagePanel = new JPanel();
        messagePanel.setLayout(new BoxLayout(messagePanel, BoxLayout.Y_AXIS));
        if (errorMessage != "" && errorMessage != null)
        {
            JLabel errorLabel = new JLabel(errorMessage);
            errorLabel.setForeground(Color.red);
            messagePanel.add(errorLabel);
        }
        messagePanel.add(new JLabel("Enter oc4jadmin/oracle user password to deploy "+ applicationName  + " to " + environmentName));
        messagePanel.add(passwordField);
        JOptionPane jop = new JOptionPane(messagePanel, JOptionPane.QUESTION_MESSAGE, JOptionPane.OK_CANCEL_OPTION);
        JDialog dialog = jop.createDialog(null,"Password:");
        dialog.addComponentListener(new ComponentAdapter()
        {
            @Override
            public void componentShown(ComponentEvent e)
            {
                passwordField.requestFocusInWindow();
            }
        });
        dialog.setVisible(true);
        int result = (Integer)jop.getValue();
        if (result == JOptionPane.OK_OPTION)
        {
            password = passwordField.getPassword();
            passwordEntered = true;
        }
        else
        {
            passwordEntered = false;
        }
        if (password != null)
        {
            this.oraclePassword = new String(password);
        }
        else
        {
            this.oraclePassword = "";
        }
        return passwordEntered;
    }
    public void setOraclePassword(String oraclePassword)
    {
        this.oraclePassword = oraclePassword;
    }
    public String getOraclePassword()
    {
        return this.oraclePassword;
    }
    
    public String getShortDescription()
    {
        String shortDescription;
        
        shortDescription = model.getHostname() + " " + model.getContainerName(); 
        
        return shortDescription;
    }
 
    public String getApplicationDescription()
    {
        String shortDescription;
        
        shortDescription =  model.getEarFileName() + " -> " + model.getHostname() + "::" + model.getServerType() + "-" + model.getContainerName(); 
        
        return shortDescription;        
        
        
    }

    public JTextField getStatusInfoField()
    {
        return statusInfoField;
    }

    public void setStatusInfoField(JTextField statusInfoField)
    {
        this.statusInfoField = statusInfoField;
    }


    public boolean isSameContainer(DeploymentDetailPanel other)
    {
        if(other.model.getHostname().equals(model.getHostname()) &&
           other.model.getContainerName().equals(model.getContainerName()))
        {
            return true;
        }

        return false;
    }
}
