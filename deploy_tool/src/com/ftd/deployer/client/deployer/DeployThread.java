package com.ftd.deployer.client.deployer;

import com.ftd.deployer.server.DeployAPI;
import javax.enterprise.deploy.spi.status.ProgressObject;

public class DeployThread extends AbstractDeployThread
{
    protected ProgressObject executeDeploy(DeployObject dep,DeployAPI deployAPI) throws Exception
    {
        ProgressObject progress = deployAPI.deploy(dep.serverName,dep.instanceName,dep.password,dep.earFilePath,dep.deploymentName,dep.parentDeploymentName, dep.listener);
        return progress;
    }
    
    protected void executePreDeploy(DeployObject dep,DeployAPI deployAPI) throws Exception
    {
        deployAPI.preDeployStart(dep.serverName,dep.instanceName,dep.password,dep.earFilePath,dep.deploymentName,dep.parentDeploymentName, dep.listener);
    }
    
    protected void verifyPreDeploy(DeployObject dep,DeployAPI deployAPI) throws Exception
    {
        deployAPI.preDeployComplete(dep.serverName,dep.instanceName,dep.password,dep.earFilePath,dep.deploymentName,dep.parentDeploymentName, dep.listener);
    }    

    protected void executePostDeploy(DeployObject dep,DeployAPI deployAPI) throws Exception
    {
        deployAPI.postDeployStart(dep.serverName,dep.instanceName,dep.password,dep.earFilePath,dep.deploymentName,dep.parentDeploymentName, dep.listener);
    }    

    protected void verifyPostDeploy(DeployObject dep,DeployAPI deployAPI) throws Exception
    {
        deployAPI.postDeployComplete(dep.serverName,dep.instanceName,dep.password,dep.earFilePath,dep.deploymentName,dep.parentDeploymentName, dep.listener);
    }        
    
    
}
