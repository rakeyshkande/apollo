package com.ftd.deployer.client.deployer;

import java.awt.Component;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class MessageDialog
{
    public static void showMessageDialog(Component parentComponent, String message, String title, int messageType)
    {
        // create a JTextArea
        JTextArea textArea = new JTextArea(20, 60);
        textArea.setText(message);
        textArea.setEditable(false);
        
        // wrap a scrollpane around it
        JScrollPane scrollPane = new JScrollPane(textArea);
        
        // display them in a message dialog
        JOptionPane.showMessageDialog(parentComponent, scrollPane, title, messageType);

    }
    
    
    public static int showConfirmDialog(Component parentComponent, String message, String title, int optionType)
    {
        // create a JTextArea
        JTextArea textArea = new JTextArea(20, 60);
        textArea.setText(message);
        textArea.setEditable(false);
        
        // wrap a scrollpane around it
        JScrollPane scrollPane = new JScrollPane(textArea);
                
        return JOptionPane.showConfirmDialog(parentComponent, scrollPane, title, optionType);
       
    }
}
