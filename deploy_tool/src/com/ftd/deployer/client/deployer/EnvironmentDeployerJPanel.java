package com.ftd.deployer.client.deployer;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;

import org.apache.log4j.Logger;

import com.ftd.deployer.client.deployer.model.ApplicationDetailModel;
import com.ftd.deployer.client.deployer.model.DeployerModel;
import com.ftd.deployer.client.deployer.model.DeploymentDetailModel;
import com.ftd.deployer.server.ConfigAPI;
import com.ftd.deployer.server.ServerFactory;
import com.ftd.deployer.util.WindowUtils;
import com.ftd.deployer.vo.EnvironmentConfigVO;

public class EnvironmentDeployerJPanel extends JPanel
{
    private static Logger logger  = Logger.getLogger(EnvironmentDeployerJPanel.class);

    protected JPanel        mainJPanel;
    protected ConfigAPI     configAPI;
    protected DeployerModel model;
    protected String        environmentName;
    protected File          earFileLocation;
    
    protected JLabel        earFileLocationJLabel;
    
    protected JButton       deployAllJButton;
    protected JButton       unDeployAllJButton;
    protected JButton       selectAllJButton;
    protected JButton       deSelectAllJButton;
    protected JButton       closeJButton;
    protected JButton       resetStatusJButton;
    protected JButton       getDeploymentStatusJButton;
    protected JButton       shutdownContainerJButton;
    protected JButton       startupContainerJButton;
    
    protected List<DeploymentDetailPanel> detailPanels;
    protected Map           applicationDetailPanels;
    
    public EnvironmentDeployerJPanel(String environmentName, File earFileLocation)
    {
        this.environmentName = environmentName;
        this.earFileLocation = earFileLocation;
        init();
    }

    protected void init()
    {
        detailPanels = new ArrayList<DeploymentDetailPanel>();
        applicationDetailPanels = new HashMap();
        
        this.setLayout(new BorderLayout());
         
        // Add the top piece
        JLabel earJLabel = getEarFileLocationJLabel();
        setEarFileLabel();
        this.add(earJLabel,BorderLayout.NORTH);
        
        JPanel mainJPanel = getMainJPanel();
        mainJPanel.setLayout(new BorderLayout());
        JScrollPane scrollPane = new JScrollPane(mainJPanel);
        this.add(scrollPane,BorderLayout.CENTER);
        EnvironmentConfigVO environmentConfig = getConfigAPI().getEnvironmentConfiguration(environmentName);
        DeployerModel model = new DeployerModel(environmentConfig, earFileLocation);
        
        JPanel centerJPanel = new JPanel();
        centerJPanel.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        // Add some padding
        gbc.insets = new Insets(2,2,2,2);
        
        Collection applications = model.getApplicationSet();
        List sortedAppList = new ArrayList();
        sortedAppList.addAll(applications);
        Collections.sort(sortedAppList);
        for (int i=0; i < sortedAppList.size(); i++)
        {
            ApplicationDetailModel appModel = (ApplicationDetailModel) sortedAppList.get(i);
            layoutApplicationPanel(appModel,centerJPanel,gbc);
            gbc.gridy++;
            gbc.gridx = 0;
            gbc.gridwidth = 99;
            centerJPanel.add(new JSeparator(),gbc);
            gbc.gridy++;
            gbc.gridwidth = 1;
        }
        
        mainJPanel.add(centerJPanel, BorderLayout.CENTER);
        
        JPanel buttonJPanel = new JPanel();
        // buttonJPanel.setLayout(new FlowLayout());
        buttonJPanel.setLayout(new GridLayout(2,4));
        
        deployAllJButton = new JButton("Deploy Checked");
        deployAllJButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                deployAllButtonPressed();
            }
        });
        buttonJPanel.add(deployAllJButton);

        unDeployAllJButton = new JButton("UnDeploy Checked");
        unDeployAllJButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                unDeployAllButtonPressed();
            }
        });
        buttonJPanel.add(unDeployAllJButton);

        selectAllJButton = new JButton("Select All");
        selectAllJButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                selectAllButtonPressed();
            }
        });
        buttonJPanel.add(selectAllJButton);

        deSelectAllJButton = new JButton("De-Select All");
        deSelectAllJButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                deSelectAllButtonPressed();
            }
        });
        buttonJPanel.add(deSelectAllJButton);

        resetStatusJButton = new JButton("Reset Status");
        resetStatusJButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                resetStatusButtonPressed();
            }
        });
        buttonJPanel.add(resetStatusJButton);
        
        
        getDeploymentStatusJButton = new JButton("Get Deploy Status/Info");
        getDeploymentStatusJButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                getDeployStatusButtonPressed();
            }
        });
        buttonJPanel.add(getDeploymentStatusJButton);        
        
        shutdownContainerJButton = new JButton("Stop Containers");
        shutdownContainerJButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                shutdownContainerButtonPressed();
            }
        });
        buttonJPanel.add(shutdownContainerJButton); 
        
        startupContainerJButton = new JButton("Start Containers");
        startupContainerJButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                startupContainerButtonPressed();
            }
        });
        buttonJPanel.add(startupContainerJButton);         
        
        
        
        closeJButton = new JButton("Close");
        closeJButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                closeButtonPressed();
            }
        });
        
        // This doesn't do anything anyway
        // buttonJPanel.add(closeJButton);

        this.add(buttonJPanel,BorderLayout.SOUTH);
        
        this.validate();
    }
    
    
    protected JPanel getMainJPanel()
    {
        if (mainJPanel == null)
        {
            mainJPanel = new JPanel();
        }
        return mainJPanel;
    }

    protected ConfigAPI getConfigAPI()
    {
        if (configAPI == null)
        {
            configAPI = ServerFactory.getConfigAPI();
        }
        return configAPI;
    }
  
    protected void selectAllButtonPressed()
    {
        setCheckBoxes(false);
        setCheckBoxes(true);
    }
    
    protected void setCheckBoxes(boolean setValue)
    {
        Iterator iterator = applicationDetailPanels.keySet().iterator();
        while (iterator.hasNext())
        {
            JCheckBox appCheckBox = (JCheckBox) iterator.next();
            appCheckBox.setSelected(setValue);;
        }
    }

    protected void deSelectAllButtonPressed()
    {
        setCheckBoxes(true);
        setCheckBoxes(false);
    }
    
    protected int showConfirmationDialog(String type, List<DeploymentDetailPanel> detailPanelList)
    {
        StringBuilder sb = new StringBuilder();
        
        sb.append("Confirm ");
        sb.append(type);
        sb.append("\n");
        
        for (int i=0; i < detailPanelList.size();i++)
        {
            sb.append("\n");
            sb.append(detailPanelList.get(i).getApplicationDescription());
        }
        
        int confirmation = MessageDialog.showConfirmDialog(this,sb.toString(), "Confirm: " + type, JOptionPane.YES_NO_CANCEL_OPTION);
        
        return confirmation;
    }
    
    protected void deployAllButtonPressed()
    {
        // Get a list of all the deployments to do
        List<DeploymentDetailPanel> deploymentList = getCheckedDeployments();
            
        int confirmation = showConfirmationDialog("Deploy", deploymentList);
        
        if (confirmation == JOptionPane.YES_OPTION)
        {
            massDeploy(deploymentList);
        }
    }
    
    protected void getDeployStatusButtonPressed()
    {
        // Get a list of all the deployments to do
        List<DeploymentDetailPanel> deploymentList = getCheckedDeployments();
        
        int confirmation = showConfirmationDialog("Get Deploy Status", deploymentList);

        if (confirmation == JOptionPane.YES_OPTION)
        {
        
            massGetStatus(deploymentList);
        }
    }
    
    protected void shutdownContainerButtonPressed()
    {
        // Get a list of all the deployments to do
        List<DeploymentDetailPanel> deploymentList = getCheckedDeploymentsAndOverlappingContainers();
        
        int confirmation = showConfirmationDialog("Shutdown Containers", deploymentList);

        if (confirmation == JOptionPane.YES_OPTION)
        {
        
            massShutdownContainers(deploymentList);
        }
    }    



    protected void startupContainerButtonPressed()
    {
        // Get a list of all the deployments to do
        List<DeploymentDetailPanel> deploymentList = getCheckedDeploymentsAndOverlappingContainers();
        
        int confirmation = showConfirmationDialog("Startup Containers", deploymentList);

        if (confirmation == JOptionPane.YES_OPTION)
        {
        
            massStartUpContainers(deploymentList);
        }
    }    
    
    protected void unDeployAllButtonPressed()
    {
        // Get a list of all the deployments to do
        List<DeploymentDetailPanel> deploymentList = getCheckedDeployments();
        
        int confirmation = showConfirmationDialog("UnDeploy", deploymentList);

        if (confirmation == JOptionPane.YES_OPTION)
        {
        
            massUnDeploy(deploymentList);
        }
    }

    protected void massDeploy(List<DeploymentDetailPanel> panels)
    {
        Map<String,DeployThread> threads = new HashMap<String,DeployThread>();
        
        DeployThreadManagerJPanel managerJPanel = new DeployThreadManagerJPanel();
        
        for (int i=0; i < panels.size(); i++)
        {
            DeploymentDetailPanel panel = panels.get(i);
            String hostname = panel.getHostJLabel().getText();
            DeployThread deployThread = threads.get(hostname);
            if (deployThread == null)
            {
                deployThread = new DeployThread();
                if (panel.addDeployment(deployThread))
                {
                threads.put(hostname,deployThread);
                }
                logger.debug("Setting up Thread for host " + hostname);
            }
            else 
            {
                panel.addDeployment(deployThread);
            }
        }
        
        Iterator threadIterator = threads.values().iterator();
        while (threadIterator.hasNext())
        {
            DeployThread deployThread = (DeployThread) threadIterator.next();
            managerJPanel.addThreadPanel(deployThread);
            deployThread.start();
        }
        
        showManagerDialog(managerJPanel);
    }
    
    protected void massGetStatus(List<DeploymentDetailPanel> panels)
    {
        massInvokeCommandThread(panels, GetStatusThread.class, "Get Status");
    }
    
    
    protected void massShutdownContainers(List<DeploymentDetailPanel> panels)
    {
        massInvokeCommandThread(panels, ShutdownContainersThread.class, "Get Status");
    }
    
    protected void massStartUpContainers(List<DeploymentDetailPanel> panels)
    {
        massInvokeCommandThread(panels, StartupContainersThread.class, "Get Status");
    }
    
    protected void massInvokeCommandThread(List<DeploymentDetailPanel> panels, Class<? extends AbstractDeployThread> deployThreadClass, String deployType)
    {
        Map<String,AbstractDeployThread> threads = new HashMap<String,AbstractDeployThread>();
        
        DeployThreadManagerJPanel managerJPanel = new DeployThreadManagerJPanel();
        
        for (int i=0; i < panels.size(); i++)
        {
            DeploymentDetailPanel panel = panels.get(i);
            String hostname = panel.getHostJLabel().getText();
            AbstractDeployThread deployThread = threads.get(hostname);
            if (deployThread == null)
            {
               try
               {
                   deployThread = deployThreadClass.newInstance();
               } catch (Exception e)
               {
                   // NOP Actually
                   throw new RuntimeException(e);
               }
               
               if (panel.addDeployment(deployThread))
               {
                   threads.put(hostname,deployThread);
                   logger.debug("Setting up Thread for host " + hostname);
               }
            }
            else
            {
                panel.addDeployment(deployThread);
            }
        }
        
        Iterator threadIterator = threads.values().iterator();
        while (threadIterator.hasNext())
        {
            AbstractDeployThread getStatusThread = (AbstractDeployThread) threadIterator.next();
            managerJPanel.addThreadPanel(getStatusThread);
            getStatusThread.start();
        }
        
        showManagerDialog(managerJPanel);        
        
        
    }

    protected void massUnDeploy(List<DeploymentDetailPanel> panels)
    {
        Map<String,UnDeployThread> threads = new HashMap<String,UnDeployThread>();
        
        DeployThreadManagerJPanel managerJPanel = new DeployThreadManagerJPanel();
        
        for (int i=0; i < panels.size(); i++)
        {
            DeploymentDetailPanel panel = panels.get(i);
            String hostname = panel.getHostJLabel().getText();
            UnDeployThread undeployThread = threads.get(hostname);
            if (undeployThread == null)
            {
               undeployThread = new UnDeployThread();
               if (panel.addDeployment(undeployThread))
               {
                   threads.put(hostname,undeployThread);
                   logger.debug("Setting up Thread for host " + hostname);

               }
            }
            else
            {
                panel.addDeployment(undeployThread);
            }
        }
        
        Iterator threadIterator = threads.values().iterator();
        while (threadIterator.hasNext())
        {
            UnDeployThread undeployThread = (UnDeployThread) threadIterator.next();
            managerJPanel.addThreadPanel(undeployThread);
            undeployThread.start();
        }
        
        showManagerDialog(managerJPanel);
    }
    
    protected void resetStatusButtonPressed()
    {
        int confirmation = JOptionPane.showConfirmDialog(null,"Are you sure you want to clear all status colors?");
        
        if (confirmation == JOptionPane.YES_OPTION)
        {
            // Reset all Status
            resetStatus();
        }
    }
    
    protected void resetStatus()
    {
        for (int i=0; i < detailPanels.size();i++)
        {
            DeploymentDetailPanel panel = detailPanels.get(i);    
            panel.resetDeploymentStatus();
        }
    }

    /**
     * Open a dialog window showing the manager thread panel.
     * @param managerJPanel
     */
    protected void showManagerDialog(DeployThreadManagerJPanel managerJPanel)
    {
        JDialog managerJDialog = new JDialog();
        
        managerJDialog.add(managerJPanel);
        managerJDialog.setTitle("Deployment Manager");
        managerJDialog.setSize(800,400);
        managerJDialog.setLocationRelativeTo(this);
        managerJDialog.setVisible(true);
    }
    
    
    /**
     * Parse through the detail panel check boxes to get the ones that
     * are checked.
     * @return
     */
    protected List<DeploymentDetailPanel> getCheckedDeployments()
    {
        List<DeploymentDetailPanel> panels = new ArrayList<DeploymentDetailPanel>();
        
        for (int i=0; i < detailPanels.size();i++)
        {
           DeploymentDetailPanel panel = detailPanels.get(i); 
           if (panel.getDeployJCheckBox().isSelected())
           {
                panels.add(panel);
           }
        }
        
        return panels;
    }

     /**
      * Close the deploy panel.  First check to ensure there are no active deployments.
      */
     protected void closeButtonPressed()
     {
         System.out.println("Ha Ha Made you click");
         
         // Check for active deployments
         
         // Close the panel
     }
    
    protected void layoutApplicationPanel(ApplicationDetailModel appModel,JPanel centerJPanel,GridBagConstraints gbc)
    {
        JCheckBox deployApplicationJCheckBox;
        JLabel applicationNameJLabel;
        
        deployApplicationJCheckBox = new JCheckBox();
        deployApplicationJCheckBox.addItemListener(new ItemListener()
        {
            public void itemStateChanged(ItemEvent event)
            {
                applicationCheckBoxChanged(event);
            }
        });
        
        // If the application is not found or is not case matched, use a colored/italic file name
        File applicationEarFile = new File(this.earFileLocation, appModel.getName());
        if(checkFileExists(applicationEarFile))
        {
            applicationNameJLabel = new JLabel(appModel.getName());
        } else
        {
            applicationNameJLabel = new JLabel("<html><FONT COLOR='RED'><I>"+ appModel.getName() + "</I></FONT></html>");
            
            if(applicationEarFile.exists())
            {
                try
                {
                    applicationNameJLabel.setToolTipText("Filename does not Match Expected Case: Found->" + applicationEarFile.getCanonicalFile().getName() + ", Expected: " + appModel.getName());
                } catch (Exception e)
                {
                    applicationNameJLabel.setToolTipText("Filename does not Match Expected Case: " + applicationEarFile.getAbsolutePath());
                }
            } else
            {
                applicationNameJLabel.setToolTipText("File not found: " + applicationEarFile.getAbsolutePath());
            }
        }
        
        centerJPanel.add(deployApplicationJCheckBox,gbc);
        gbc.gridx++;
        centerJPanel.add(applicationNameJLabel,gbc);
        gbc.gridx++;

        java.util.List serverList = appModel.getServerList();
        
        List panelList = new ArrayList();
        applicationDetailPanels.put(deployApplicationJCheckBox,panelList);

        List sortedDetailList = new ArrayList();
        sortedDetailList.addAll(serverList);
        Collections.sort(sortedDetailList);
        for (int i = 0; i < sortedDetailList.size(); i++)
        {
            if (i > 0)
            {
                gbc.gridwidth = 99;
                centerJPanel.add(new JSeparator(),gbc);
                gbc.gridy++;
                gbc.gridwidth = 1;
            }
            DeploymentDetailModel detailModel = (DeploymentDetailModel)sortedDetailList.get(i);
            layoutDetailPanel(detailModel,centerJPanel,gbc, panelList);
            gbc.gridy++;
            gbc.gridx = 2;
        }

    }
    
    protected boolean checkFileExists(File applicationEarFile)
    {
        if(!applicationEarFile.exists())
        {
            return false;
        }
        
        try
        {
            String actualName = applicationEarFile.getCanonicalFile().getName();
            if(!actualName.equals(applicationEarFile.getName()))
            {
                return false;
            }
        } catch (Exception e)
        {
            // NOP. We already checked for File Exists above
        }
        
        return true;
        
    }
    
    protected void layoutDetailPanel(DeploymentDetailModel model, JPanel centerJPanel, GridBagConstraints gbc, List panelList)
    {
        DeploymentDetailPanel detailPanel = new DeploymentDetailPanel(model);
        
        centerJPanel.add(detailPanel.getDeployJCheckBox(),gbc);
        gbc.gridx++;
        centerJPanel.add(detailPanel.getHostJLabel(),gbc);
        gbc.gridx++;
        centerJPanel.add(detailPanel.getContainerJLabel(),gbc);
        gbc.gridx++;
        centerJPanel.add(detailPanel.getStatusJButton(),gbc);
        // AR -- remove this functionality. For now just don't add the buttons unless someone wants to resurrect
        // This functionality, and figure out the Threading Model.
        // gbc.gridx++;
        // centerJPanel.add(detailPanel.getDeployNowJButton(),gbc);
        // gbc.gridx++;
        // centerJPanel.add(detailPanel.getUnDeployNowJButton(),gbc);
        gbc.gridx++;
        centerJPanel.add(detailPanel.getStatusInfoField(),gbc);
    
        panelList.add(detailPanel);    
        detailPanels.add(detailPanel);
    }
    
    /**
     * Change the server check boxes to correspond to the new value
     * of the application check box.
     * @param event
     */
    protected void applicationCheckBoxChanged(ItemEvent event)
    {
        Object source = event.getItemSelectable();
        
        List detailPanels = (List) applicationDetailPanels.get(source);
        
        boolean newState = true;
        if (event.getStateChange() == ItemEvent.DESELECTED)
        {
            newState = false;
        }
        
        for (int i=0; i < detailPanels.size(); i++)
        {
            DeploymentDetailPanel panel = (DeploymentDetailPanel) detailPanels.get(i);
            panel.getDeployJCheckBox().setSelected(newState);
        }

    }

    protected JLabel getEarFileLocationJLabel() 
    { 
        if (earFileLocationJLabel == null) 
        {
            earFileLocationJLabel = new JLabel();
        }
        return earFileLocationJLabel;
    }
    
    private void setEarFileLabel()
    {
        JLabel earFileJLabel = getEarFileLocationJLabel();
        StringBuilder sb = new StringBuilder();
        
        sb.append("Ear file location is: ");
        sb.append(earFileLocation.getAbsolutePath());
        earFileJLabel.setText(sb.toString());
    }
    
    
    /**
     * Get the containers that are checked. Then add and deployment detail panels
     * that are bound to the same containers and ensure they are checked as well
     * @return
     */
    private List<DeploymentDetailPanel> getCheckedDeploymentsAndOverlappingContainers()
    {
        List<DeploymentDetailPanel> deploymentList = getCheckedDeployments();
        
        boolean selectionModified = false;
        
        // Now make a list of all overlapping containers with the same Server/Container        
        for (int i=0; i < detailPanels.size();i++)
        {
           DeploymentDetailPanel panel = detailPanels.get(i); 
           
           for(int j=0; j < deploymentList.size(); j++)
           {
               DeploymentDetailPanel selectedPanel = deploymentList.get(j); 
               if(selectedPanel.isSameContainer(panel))
               {
                   if (!panel.getDeployJCheckBox().isSelected())
                   {
                       panel.getDeployJCheckBox().setSelected(true);
                       selectionModified = true;
                   }
               }
           }
        }
        
        if(selectionModified)
        {
            JOptionPane.showMessageDialog(this,"The selection was updated\nto include other applications residing\nin the same containers\nAs they are affected by\nthis operation", "Selection Updated", JOptionPane.INFORMATION_MESSAGE);            
        }
        
        //Retrieve the list again
        deploymentList = getCheckedDeployments();
        
        return deploymentList;
    }
}
