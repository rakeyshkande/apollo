package com.ftd.deployer.client.deployer;

public class ThreadDeployEvent
{
    public final static int STATUS_THREAD_COMPLETE = 0;
    public final static int STATUS_DEPLOYMENT_COMPLETE = 1;
    public final static int STATUS_DEPLOYMENT_FAILED = 2;
    
    protected int status;
    protected Exception failedException;
    protected String server;
    protected String application;
    
    public ThreadDeployEvent(int status,String server, String application)
    {
        this.server = server;
        this.application = application;
        this.status = status;
    }

    public ThreadDeployEvent(int status,String server)
    {
        this.server = server;
        this.status = status;
    }

    public ThreadDeployEvent(int status, String server, String application, Exception e)
    {
        this.server = server;
        this.application = application;
        this.failedException = e;
        this.status = status;
    }
    
    public Exception getException()
    {
        return failedException;
    }
    public int getStatus()
    {
        return status;
    }
    public String getServer()
    {
        return server;
    }
    public String getApplication()
    {
        return application;
    }
}
