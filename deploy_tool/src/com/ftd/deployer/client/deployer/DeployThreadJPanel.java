package com.ftd.deployer.client.deployer;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.FlowLayout;

import java.awt.Font;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import java.awt.GridLayout;

import java.awt.Insets;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.util.List;

import javax.enterprise.deploy.spi.status.DeploymentStatus;
import javax.enterprise.deploy.spi.status.ProgressEvent;
import javax.enterprise.deploy.spi.status.ProgressListener;

import javax.swing.*;

public class DeployThreadJPanel extends JPanel implements DeployThreadListener, ProgressListener
{
    protected static String        NEWLINE= "\n";
    protected AbstractDeployThread deployThread;
    protected String               server;
    protected JProgressBar         progressBar;
    protected JProgressBar         parentProgressBar;
    
    protected JPanel               buttonJPanel;
    
    protected JPanel               logJPanel;
    protected JTextArea            logJTextArea;
    protected CardLayout           cardLayout;
    
    protected int deploymentsComplete = 0;
    
    protected ProgressEventDispatcher dispatcher;    
    
    public DeployThreadJPanel(AbstractDeployThread deployThread, JProgressBar parentProgressBar)
    {
        this.deployThread = deployThread;
        this.parentProgressBar = parentProgressBar;
        init();
    }
    
    protected void init()
    {
        dispatcher = ProgressEventDispatcher.getInstance();
        
        deployThread.addDeployListener(this);
        server = deployThread.getHostname();
        dispatcher.addListener(server,this);
    
        this.setLayout(new BorderLayout());
        
        JPanel progressJPanel = new JPanel();
        progressJPanel.setLayout(new FlowLayout());
        progressJPanel.add(new JLabel(deployThread.getHostname()));
        progressBar = new JProgressBar();
        progressBar.setMaximum(deployThread.getNumberOfDeploymentSteps());
        progressBar.setStringPainted(true);
        parentProgressBar.setMaximum(deployThread.getNumberOfDeploymentSteps());
        parentProgressBar.setStringPainted(true);
        progressJPanel.add(progressBar);
        
        this.add(progressJPanel,BorderLayout.NORTH);
        
        logJTextArea = new JTextArea();
        logJTextArea.setFont(new Font("Tahoma", Font.PLAIN, 10));
        JScrollPane logJScrollPane = new JScrollPane(logJTextArea);
        logJTextArea.append("Deployment Log\n");

        logJPanel = new JPanel();
        cardLayout = new CardLayout();
        logJPanel.setLayout(cardLayout);
        logJPanel.add(logJScrollPane,"ALL");

        buttonJPanel = new JPanel();
        buttonJPanel.setLayout(new GridLayout(0,1));

        // Add the All button
        JButton allJButton = new JButton("ALL");
        buttonJPanel.add(allJButton);
        allJButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                appButtonPressed(e);
            }
        });

        List deploymentList = deployThread.getDeploymentList();
        
        for (int i=0; i < deploymentList.size();i++)
        {
            DeployObject deploy = (DeployObject) deploymentList.get(i);
            String application = deploy.getDeploymentName();
            JButton appJButton = new JButton(application);
            buttonJPanel.add(appJButton);
            appJButton.addActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent e)
                {
                    appButtonPressed(e);
                }
            });
                        
            // Add a text field and tie them together                        
            JTextArea appJTextArea = new JTextArea();
            appJTextArea.setFont(new Font("Tahoma", Font.PLAIN, 10));
            JScrollPane appJScrollPane = new JScrollPane(appJTextArea);
            appJScrollPane.setBackground(Color.BLUE);
            appJTextArea.append("Deployment Log\n");
            logJPanel.add(appJScrollPane,application);
            new AppListenerAdapter(application,server,appJTextArea, appJButton);
        }
        
        JPanel centerJPanel = new JPanel();
        centerJPanel.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 0.1;
        gbc.weighty = 0.1;
        gbc.insets = new Insets(2,2,2,2);
        
        gbc.fill = GridBagConstraints.NONE;
        centerJPanel.add(buttonJPanel,gbc);
        gbc.gridx++;
        gbc.fill = GridBagConstraints.BOTH;
        gbc.weightx = 0.9;
        gbc.weighty = 0.9;
        centerJPanel.add(logJPanel,gbc);

        this.add(centerJPanel,BorderLayout.CENTER);
        
    }
    
    public void threadDeployEvent(ThreadDeployEvent event)
    {
        if (event.getStatus() == ThreadDeployEvent.STATUS_DEPLOYMENT_COMPLETE)
        {
            deploymentsComplete++;
            setProgressBar(deploymentsComplete);
        }
        else if (event.getStatus() == ThreadDeployEvent.STATUS_DEPLOYMENT_FAILED)
        {
            Exception failedException = event.getException();
            StringBuffer message = new StringBuffer();
            message.append("****Failed Deployment****\n");
            message.append("****");
            message.append(event.getApplication());
            message.append("****\n");
            if (failedException != null)
            {
                message.append(failedException.getMessage());
            }
            logJTextArea.append(message.toString());
            logJTextArea.append(NEWLINE);
        }
        else if (event.getStatus() == ThreadDeployEvent.STATUS_THREAD_COMPLETE)
        {
            setProgressBar(deployThread.getNumberOfDeploymentSteps());
        }
    }
    
    
    public String getTabName()
    {
        return deployThread.getHostname();    
    }

    public void handleProgressEvent(ProgressEvent progressEvent)
    {
        String message = progressEvent.getDeploymentStatus().getMessage();
        logJTextArea.append(message);
        logJTextArea.append(NEWLINE);
    }
    
    public void removeNotify()
    {
        dispatcher.removeListener(this);
        super.removeNotify();
    }
    
    protected void appButtonPressed(ActionEvent event)
    {
        String appName = ((JButton) event.getSource()).getText();
        cardLayout.show(logJPanel,appName);
    }
    
    protected void setProgressBar(int value)
    {
        progressBar.setValue(value);
        parentProgressBar.setValue(value);
    }
    
    class AppListenerAdapter implements ProgressListener
    {
        String application;
        String server;
        JTextArea appJTextArea;
        JButton appJButton;
        
        AppListenerAdapter(String application, String server, JTextArea appJTextArea, JButton appJButton)
        {
            this.application = application;
            this.server = server;
            this.appJTextArea = appJTextArea;
            this.appJButton = appJButton;
            
            dispatcher.addListener(server,application,this);
        }

        public void handleProgressEvent(ProgressEvent progressEvent)
        {
            UpdateThread doUpdateStatus = new UpdateThread(appJTextArea, appJButton, progressEvent);
            SwingUtilities.invokeLater(doUpdateStatus);
        }
    }
    
    class UpdateThread extends Thread
    {
        protected JTextArea appJTextArea;
        protected JButton   appJButton;
        protected ProgressEvent progressEvent;
        
        public UpdateThread(JTextArea appJTextArea, JButton appJButton, ProgressEvent progressEvent)
        {
            this.appJButton = appJButton;
            this.appJTextArea = appJTextArea;
            this.progressEvent = progressEvent;
        }
        
        public void run()
        {
            DeploymentStatus status = progressEvent.getDeploymentStatus();
            String message = status.getMessage();
            appJTextArea.append(message);
            appJTextArea.append(NEWLINE);

            Color statusColor = Color.GRAY;
            if (status != null)
            {
                if (status.isFailed())
                {
                    statusColor = Color.RED;
                }
                if (status.isRunning())
                {
                    statusColor = Color.ORANGE;
                }
                if (status.isCompleted())
                {
                    statusColor = Color.GREEN;
                }
            }
            appJButton.setForeground(statusColor);
        }
        
    }
}
