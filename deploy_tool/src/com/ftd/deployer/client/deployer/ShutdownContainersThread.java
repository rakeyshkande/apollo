package com.ftd.deployer.client.deployer;

import javax.enterprise.deploy.spi.status.ProgressObject;

import com.ftd.deployer.server.DeployAPI;

/**
 * Extends GetStatusThread so that it does a status after the shutdown completes.
 */
public class ShutdownContainersThread extends GetStatusThread
{
 
    protected void executePreDeploy(DeployObject dep,DeployAPI deployAPI) throws Exception
    {
        deployAPI.invokeShutdown(dep.serverName, dep.instanceName, dep.password, dep.listener);                
    }
    
    /**
     * Just invoke getDeployStatus
     */
    protected ProgressObject executeDeploy(DeployObject dep,DeployAPI deployAPI) throws Exception
    {
        deployAPI.verifyShutdown(dep.serverName, dep.instanceName, dep.password, dep.listener);
         
        return super.executeDeploy(dep,deployAPI);
    }

}
