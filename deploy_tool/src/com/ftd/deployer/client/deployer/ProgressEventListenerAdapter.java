package com.ftd.deployer.client.deployer;

import javax.enterprise.deploy.spi.status.ProgressEvent;
import javax.enterprise.deploy.spi.status.ProgressListener;

/**
 * This Adapter listens to progress events for a specific
 * server and application.  The extra payload for the server
 * and application are sent to the dispatcher to dispatch.
 */
public class ProgressEventListenerAdapter implements ProgressListener
{
    protected String server;
    protected String application;
    
    protected ProgressEventDispatcher dispatcher;
    
    public ProgressEventListenerAdapter(String server, String application)
    {
        this.server = server;
        this.application = application;
        
        dispatcher = ProgressEventDispatcher.getInstance();
    }

    public void handleProgressEvent(ProgressEvent progressEvent)
    {
        dispatcher.handleProgressEvent(server,application,progressEvent);
    }
}
