package com.ftd.deployer.client.deployer;

import com.ftd.deployer.server.DeployAPI;

import javax.enterprise.deploy.spi.status.ProgressObject;

public class GetStatusThread extends AbstractDeployThread
{
    
    /**
     * Just invoke getDeployStatus
     */
    protected ProgressObject executeDeploy(DeployObject dep,DeployAPI deployAPI) throws Exception
    {
         ProgressObject progress = deployAPI.getDeployStatus(dep.serverName,dep.instanceName,dep.password,dep.deploymentName,dep.listener, dep.getEarFileName());
         return progress;
    }

}
