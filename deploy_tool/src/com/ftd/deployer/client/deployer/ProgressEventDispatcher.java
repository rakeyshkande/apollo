package com.ftd.deployer.client.deployer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import java.util.Map;

import javax.enterprise.deploy.spi.status.ProgressEvent;
import javax.enterprise.deploy.spi.status.ProgressListener;

/**
 * The purpose of this class is to dispatch ProgressEvents to the
 * appropriate subscribers.  Subscription can be done for all events,
 * events for a particular server, or a particular server and application.
 */
public class ProgressEventDispatcher 
{
    private static ProgressEventDispatcher instance;
    
    private List globalListeners;
    private Map  serverListeners;
    private Map  applicationListeners;
    
    private ProgressEventDispatcher()
    {
        globalListeners = new ArrayList();
        serverListeners = new HashMap();
        applicationListeners = new HashMap();
    }
    
    public static ProgressEventDispatcher getInstance()
    {
        if (instance == null)
        {
            instance = new ProgressEventDispatcher();
        }
        return instance;
    }
    
    public void handleProgressEvent(String server, String application, ProgressEvent event)
    {
        fireProgressEvent(event);
        fireProgressEvent(server,event);
        fireProgressEvent(server,application,event);
    }
    
    public void addListener(ProgressListener listener)
    {
        globalListeners.add(listener);
    }

    public void removeListener(ProgressListener listener)
    {
        globalListeners.remove(listener);
    }
    
    public void addListener(String server, ProgressListener listener)
    {
        List listenerList = (List) serverListeners.get(server);
        if (listenerList == null)
        {
            listenerList = new ArrayList();
            serverListeners.put(server,listenerList);
        }
        listenerList.add(listener);
    }

    public void removeListener(String server, ProgressListener listener)
    {
        List listenerList = (List) serverListeners.get(server);
        if (listenerList != null)
        {
            listenerList.remove(listener);
        }
    }
    
    public void addListener(String server, String application, ProgressListener listener)
    {
        String key = server + "X" + application;
        List listenerList = (List) applicationListeners.get(key);
        if (listenerList == null)
        {
            listenerList = new ArrayList();
            applicationListeners.put(key,listenerList);
        }
        listenerList.add(listener);
    }

    public void removeListener(String server, String application, ProgressListener listener)
    {
        String key = server + "X" + application;
        List listenerList = (List) applicationListeners.get(key);
        if (listenerList != null)
        {
            listenerList.remove(listener);
        }
    }
    
    protected void fireProgressEvent(ProgressEvent event)
    {
        for (int i=0; i < globalListeners.size();i++)
        {
            ProgressListener listener = (ProgressListener) globalListeners.get(i);
            listener.handleProgressEvent(event);
        }
    }
    
    protected void fireProgressEvent(String server, ProgressEvent event)
    {
        List listenerList = (List) serverListeners.get(server);
        
        if (listenerList != null) 
        {
            for (int i=0; i < listenerList.size();i++)
            {
                ProgressListener listener = (ProgressListener) listenerList.get(i);
                listener.handleProgressEvent(event);
            }
        }
        
    }
    
    protected void fireProgressEvent(String server, String application, ProgressEvent event)
    {
        String key = server + "X" + application;
        List listenerList = (List) applicationListeners.get(key);
        
        if (listenerList != null) 
        {
            for (int i=0; i < listenerList.size();i++)
            {
                ProgressListener listener = (ProgressListener) listenerList.get(i);
                listener.handleProgressEvent(event);
            }
        }
        
    }
}
