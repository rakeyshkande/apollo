package com.ftd.deployer.client.deployer;

import javax.enterprise.deploy.spi.TargetModuleID;
import javax.enterprise.deploy.spi.status.DeploymentStatus;
import javax.enterprise.deploy.spi.status.ProgressEvent;

import org.apache.log4j.Logger;

/**
 * This represent a significant Status/Progress Event
 */
public class DeploymentStatusProgressEvent extends ProgressEvent
{
    private static final long serialVersionUID = -3734420335866455730L;

    private static Logger logger = Logger.getLogger(DeploymentStatusProgressEvent.class);
    

    public DeploymentStatusProgressEvent(Object source, TargetModuleID targetModuleID, DeploymentStatus sCode)
    {
        super(source, targetModuleID, sCode);
        
        // logger.debug("Progress Event: " + sCode.getMessage(), new Exception());
    }

    
   
    
}
