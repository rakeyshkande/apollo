package com.ftd.deployer.client.deployer;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.deploy.spi.status.DeploymentStatus;
import javax.enterprise.deploy.spi.status.ProgressEvent;
import javax.enterprise.deploy.spi.status.ProgressListener;

import org.apache.log4j.Logger;

public class DeploymentListener implements ProgressListener
{
    private static Logger logger  = Logger.getLogger(DeploymentListener.class);

    protected List<ProgressEvent> progressEventList;
    protected DeploymentDetailPanel panel;
    
    String deploymentInfo = "Deploy Info: Unknown                             ";
        
    public DeploymentListener(DeploymentDetailPanel panel) 
    {
        this.panel = panel;
        progressEventList = new ArrayList<ProgressEvent>();
    }

    public void handleProgressEvent(ProgressEvent progressEvent)
    {
        String message = progressEvent.getDeploymentStatus().getMessage();
        logger.debug(message);          
        
        if(progressEvent instanceof DeploymentStatusProgressEvent)
        {
            // Capture this separately
            deploymentInfo = progressEvent.getDeploymentStatus().getMessage();
        } else 
        {
            progressEventList.add(progressEvent);  
        }
        
        panel.updateDeploymentStatus();
    }
    
    public List getProgressList()
    {
        return progressEventList;
    }

    public String getDeploymentLog()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("Deployment Log:\n");
        boolean gotUploading = false;
        for (int i=0; i < progressEventList.size();i++)
        {
            ProgressEvent event = progressEventList.get(i);
            String message = event.getDeploymentStatus().getMessage();
            if (message.startsWith("Uploading file"))
            {
                if (!gotUploading)
                {
                    sb.append(event.getDeploymentStatus().getMessage());
                    sb.append("\n");
                }
                gotUploading = true;
            }
            else
            {
                sb.append(event.getDeploymentStatus().getMessage());
                sb.append("\n");
            }
        }
        return sb.toString();
    }
    
    public boolean isComplete()
    {
        ProgressEvent event = progressEventList.get(progressEventList.size() -1);
        
        DeploymentStatus status = event.getDeploymentStatus();
        return status.isCompleted();
    }
    
    public boolean isFailed()
    {
        ProgressEvent event = progressEventList.get(progressEventList.size() -1);

        DeploymentStatus status = event.getDeploymentStatus();
        return status.isFailed();
    }
    
    public boolean isInProgess()
    {
        ProgressEvent event = progressEventList.get(progressEventList.size() -1);    
        
        DeploymentStatus status = event.getDeploymentStatus();
        return status.isRunning();
    }
    
    
    public String getDeploymentInfo()
    {
        return deploymentInfo;
    }
}
