package com.ftd.deployer.client.config;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

public abstract class ContentEditJPanel extends JPanel
{
    DefaultMutableTreeNode node;
    DefaultTreeModel treeModel;
    
    JPanel buttonJPanel;
    JButton applyChangesJButton;
    
    public ContentEditJPanel(DefaultMutableTreeNode node, DefaultTreeModel treeModel) 
    {
        this.node = node;
        this.treeModel = treeModel;
    }

    public abstract void applyChanges();
}
