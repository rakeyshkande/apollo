package com.ftd.deployer.client.config;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import com.ftd.deployer.server.ServerTypes;
import com.ftd.deployer.vo.ApplicationConfigVO;
import com.ftd.deployer.vo.ServerConfigVO;

public class TreeServerJPanel extends ContentEditJPanel
{
    protected JButton addApplicationJButton;

    protected JTextField nameJTextField;
    protected JTextField hostnameJTextField;
    protected JLabel nameJLabel = new JLabel("Name:");
    protected JLabel hostnameJLabel = new JLabel("Host Name:");
    protected JLabel serverTypeJLabel = new JLabel("Server Type:");    
    protected JComboBox serverTypeComboBox;

    public TreeServerJPanel(DefaultMutableTreeNode node, DefaultTreeModel treeModel)
    {
        super(node, treeModel);
        init();
    }

    public void applyChanges()
    {
        ServerConfigVO vo = (ServerConfigVO) node.getUserObject();
        vo.setName(nameJTextField.getText());
        vo.setHostname(hostnameJTextField.getText());
        
        String previousType = vo.getServerType();
        vo.setServerType((String) serverTypeComboBox.getSelectedItem());
        String newType = vo.getServerType();
        
        if(!previousType.equals(newType))
        {
            JOptionPane.showMessageDialog(this, "Application container Names will be\nUpdated for the Server Type: " + newType);
            // Server Type changed, updated container names
            vo.updateContainerNamesByServerType();
        }
        
    }

    protected void init()
    {
        this.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;

        nameJTextField = new JTextField(30);
        hostnameJTextField = new JTextField(30);
        
        serverTypeComboBox = new JComboBox(ServerTypes.SERVER_TYPES);
        
        ServerConfigVO vo = (ServerConfigVO) node.getUserObject();
        nameJTextField.setText(vo.getName());
        hostnameJTextField.setText(vo.getHostname());
        
        serverTypeComboBox.setSelectedItem(vo.getServerType());
        serverTypeComboBox.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                serverTypeChanged();
            }
        });

        buttonJPanel = new JPanel();
        buttonJPanel.setLayout(new FlowLayout());
        applyChangesJButton = new JButton("Apply Changes");
        applyChangesJButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                applyChanges();
            }
        });
        buttonJPanel.add(applyChangesJButton);
        addApplicationJButton = new JButton("Add Application");
        addApplicationJButton.addActionListener(new ActionListener()
                {
                    public void actionPerformed(ActionEvent e)
                    {
                        addApplication();
                    }
                });
        buttonJPanel.add(addApplicationJButton);
        
        this.add(nameJLabel,gbc);
        gbc.gridx++;
        this.add(nameJTextField,gbc);
        gbc.gridx = 0;
        gbc.gridy++;
        this.add(hostnameJLabel,gbc);
        gbc.gridx++;
        this.add(hostnameJTextField,gbc);
        
        gbc.gridx = 0;
        gbc.gridy++;
        this.add(serverTypeJLabel,gbc);
        gbc.gridx++;
        this.add(serverTypeComboBox,gbc);        
        
        gbc.gridx = 0;
        gbc.gridy++;
        gbc.gridwidth = 2;
        this.add(buttonJPanel,gbc);

    }

    protected void addApplication()
    {
        ApplicationConfigVO vo = new ApplicationConfigVO();
        vo.setDeployName("New Application");
        DefaultMutableTreeNode envNode = new DefaultMutableTreeNode(vo);

        node.add(envNode);
        treeModel.nodeStructureChanged(node);
    }

    
    
    protected void serverTypeChanged()
    {
        String serverType = (String) serverTypeComboBox.getSelectedItem();
        
        
        
    }
}
