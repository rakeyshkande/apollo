package com.ftd.deployer.client.config;

import com.ftd.deployer.vo.EnvironmentConfigVO;

import com.ftd.deployer.vo.ServerConfigVO;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

public class TreeEnvironmentJPanel extends ContentEditJPanel
{
    protected JButton addServerJButton;

    protected JTextField nameJTextField;
    protected JLabel nameJLabel = new JLabel("Name:");

    public TreeEnvironmentJPanel(DefaultMutableTreeNode node, DefaultTreeModel treeModel)
    {
        super(node, treeModel);
        init();
    }

    public void applyChanges()
    {
        EnvironmentConfigVO vo = (EnvironmentConfigVO) node.getUserObject();
        vo.setName(nameJTextField.getText());
    }

    protected void init()
    {
        this.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;

        nameJTextField = new JTextField(30);
        
        EnvironmentConfigVO vo = (EnvironmentConfigVO) node.getUserObject();
        nameJTextField.setText(vo.getName());

        buttonJPanel = new JPanel();
        buttonJPanel.setLayout(new FlowLayout());
        applyChangesJButton = new JButton("Apply Changes");
        applyChangesJButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                applyChanges();
            }
        });
        buttonJPanel.add(applyChangesJButton);
        addServerJButton = new JButton("Add Server");
        addServerJButton.addActionListener(new ActionListener()
                {
                    public void actionPerformed(ActionEvent e)
                    {
                        addServer();
                    }
                });
        buttonJPanel.add(addServerJButton);
        
        this.add(nameJLabel,gbc);
        gbc.gridx++;
        this.add(nameJTextField,gbc);
        gbc.gridx = 0;
        gbc.gridy++;
        gbc.gridwidth = 2;
        this.add(buttonJPanel,gbc);
    }

    protected void addServer()
    {
        ServerConfigVO vo = new ServerConfigVO();
        vo.setName("New Server");
        DefaultMutableTreeNode envNode = new DefaultMutableTreeNode(vo);

        node.add(envNode);
        treeModel.nodeStructureChanged(node);
    }
}
