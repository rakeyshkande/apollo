package com.ftd.deployer.client.config;

import com.ftd.deployer.server.ConfigAPI;

import com.ftd.deployer.server.ServerFactory;

import com.ftd.deployer.vo.ApplicationConfigVO;
import com.ftd.deployer.vo.EnvironmentConfigVO;

import com.ftd.deployer.vo.ServerConfigVO;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.FlowLayout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeSelectionModel;

import org.apache.log4j.Logger;

public class EnvironmentConfigJPanel extends JPanel implements TreeSelectionListener
{
    private static Logger logger  = Logger.getLogger(EnvironmentConfigJPanel.class);

    protected JPanel contentJPanel;
    protected JPanel buttonJPanel;    
    protected ContentEditJPanel contentEditJPanel;
    
    protected JButton saveJButton;
    protected JButton resetJButton;
    
    protected JTree  tree;
    protected DefaultMutableTreeNode rootNode;
    protected DefaultTreeModel treeModel;
    
    protected ConfigAPI configAPI;
    
    public EnvironmentConfigJPanel()
    {
        init();
    }
    
    protected void init()
    {
        configAPI = ServerFactory.getConfigAPI();
        
        this.setLayout(new BorderLayout());
        
        contentJPanel = new JPanel();
        buttonJPanel = new JPanel();

        rootNode = new DefaultMutableTreeNode("Root");
        buildTreeModel(rootNode);
        
        tree = new JTree(rootNode);
        JScrollPane treeJScrollPane = new JScrollPane(tree);
        treeModel = (DefaultTreeModel) tree.getModel();

        tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        //Listen for when the selection changes.
        tree.addTreeSelectionListener(this);


        saveJButton = new JButton("Save");
        resetJButton = new JButton("Reset");
        saveJButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                saveButtonPressed();
            }
        });
        resetJButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                resetButtonPressed();
            }
        });
        
        buttonJPanel.setLayout(new FlowLayout());
        buttonJPanel.add(saveJButton);
        buttonJPanel.add(resetJButton);

        displayRoot(rootNode);

        JSplitPane topJSplitPane = new JSplitPane();
        topJSplitPane.setLeftComponent(treeJScrollPane);
        topJSplitPane.setRightComponent(contentJPanel);
        topJSplitPane.setDividerLocation(300);        
        
        this.add(topJSplitPane,BorderLayout.CENTER);
        this.add(buttonJPanel,BorderLayout.SOUTH);
    }
    
    public void valueChanged(TreeSelectionEvent e)
    {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode)tree.getLastSelectedPathComponent();

        if (node == null)
            return;

        Object nodeInfo = node.getUserObject();
        
        if (nodeInfo instanceof ApplicationConfigVO)
        {
            // Add an application config edit panel to the content panel
            displayApplication(node);
        }
        else if (nodeInfo instanceof ServerConfigVO)
        {
            // Add an application config edit panel to the content panel
            displayServer(node);
        }
        else if (nodeInfo instanceof EnvironmentConfigVO)
        {
            // Add an application config edit panel to the content panel
            displayEnvironment(node);
        }
        else 
        {
            // Add an application config edit panel to the content panel
            displayRoot(node);
        }
    }


    /**
     * Build a tree model from the environment configuration.  The configuration
     * is read from the ConfigAPI and is cloned for use on the screen.
     * @param root
     */
    protected void buildTreeModel(DefaultMutableTreeNode root)
    {
        Set environments = configAPI.getEnvironmentList();
        Iterator envIterator = environments.iterator();
        while (envIterator.hasNext())
        {
            String envName = (String) envIterator.next();
            EnvironmentConfigVO environmentNonMutable = configAPI.getEnvironmentConfiguration(envName);
            EnvironmentConfigVO environment = environmentNonMutable.fullClone();
            
            DefaultMutableTreeNode envNode = new DefaultMutableTreeNode(environment);
            root.add(envNode);
            
            List<ServerConfigVO> servers = environment.getServers();
            for (int i=0; i < servers.size();i++)
            {
                ServerConfigVO server = servers.get(i);
                
                DefaultMutableTreeNode serverNode = new DefaultMutableTreeNode(server);
                envNode.add(serverNode);
                
                List applications = server.getApplications();
                List sortedAppList = new ArrayList();
                sortedAppList.addAll(applications);
                Collections.sort(sortedAppList);
                for (int j=0; j < sortedAppList.size();j++)
                {
                    ApplicationConfigVO application = (ApplicationConfigVO) sortedAppList.get(j);
                    
                    DefaultMutableTreeNode applicationNode = new DefaultMutableTreeNode(application);
                    serverNode.add(applicationNode);
                }
                
            }
        }
        
    }
    
    protected void saveButtonPressed()
    {
        int confirmation = JOptionPane.showConfirmDialog(null,"Do you really want to save all changes?");

        if (confirmation == JOptionPane.YES_OPTION)
        {
            // TODO this really should be off the swing thread with a progress meter
             Cursor hourglassCursor = new Cursor(Cursor.WAIT_CURSOR);
             setCursor(hourglassCursor);
            // First save the current panels changes
            if (contentEditJPanel != null)
            {
                contentEditJPanel.applyChanges();
            }
            try
            {
                // Then convert the nodes back to the proper data model.
                Map<String,EnvironmentConfigVO> config = convertModelToVOModel();
                // Save the file to disk
                configAPI.saveEnvironmentConfiguration(config);
            }
            catch (Exception e)
            {
                JOptionPane.showMessageDialog(null,"Error saving Configuration. " + e.getMessage(),"Error in Save",JOptionPane.ERROR_MESSAGE);
                logger.error("Error Saving Configuration",e);
            }
            Cursor normalCursor = new Cursor(Cursor.DEFAULT_CURSOR);
            setCursor(normalCursor);
        }        
    }
    
    protected void resetButtonPressed()
    {
        int confirmation = JOptionPane.showConfirmDialog(null,"Do you really want to reset the configuration and lose all changes?");

        if (confirmation == JOptionPane.YES_OPTION)
        {
            rootNode.removeAllChildren();
            buildTreeModel(rootNode);
            treeModel.nodeStructureChanged(rootNode);
        }
        
    }
    
    protected void displayApplication(DefaultMutableTreeNode node)
    {
        TreeApplicationJPanel panel = new TreeApplicationJPanel(node, treeModel);
        replaceEditPanel(panel);
    }
    protected void displayServer(DefaultMutableTreeNode node)
    {
        TreeServerJPanel panel = new TreeServerJPanel(node, treeModel);
        replaceEditPanel(panel);
    }
    protected void displayEnvironment(DefaultMutableTreeNode node)
    {
        TreeEnvironmentJPanel panel = new TreeEnvironmentJPanel(node, treeModel);
        replaceEditPanel(panel);
    }
    protected void displayRoot(DefaultMutableTreeNode node)
    {
        TreeRootJPanel panel = new TreeRootJPanel(node, treeModel);
        replaceEditPanel(panel);
    }
    
    protected void replaceEditPanel(ContentEditJPanel panel)
    {
        if (contentEditJPanel != null)
        {
            contentJPanel.remove(contentEditJPanel);
        }
        contentEditJPanel = panel;
        contentJPanel.add(contentEditJPanel);
        this.validate();
        this.repaint();
    }
    
    protected Map<String,EnvironmentConfigVO> convertModelToVOModel()
    {
        Map<String,EnvironmentConfigVO> config = new HashMap<String,EnvironmentConfigVO>();
        
        // Start with the root node and build down
        Enumeration environmentEnum = rootNode.children();
        while (environmentEnum.hasMoreElements())
        {
            DefaultMutableTreeNode envNode = (DefaultMutableTreeNode) environmentEnum.nextElement();
            EnvironmentConfigVO envUserObject = (EnvironmentConfigVO) envNode.getUserObject();
            EnvironmentConfigVO env = new EnvironmentConfigVO();
            env.setName(envUserObject.getName());
            Enumeration serverEnum = envNode.children();
            while (serverEnum.hasMoreElements())
            {
                DefaultMutableTreeNode serverNode = (DefaultMutableTreeNode) serverEnum.nextElement();
                ServerConfigVO serverUserObject = (ServerConfigVO) serverNode.getUserObject();
                ServerConfigVO server = new ServerConfigVO();
                server.setName(serverUserObject.getName());
                server.setHostname(serverUserObject.getHostname());
                server.setServerType(serverUserObject.getServerType());
                
                Enumeration appEnum = serverNode.children();
                while (appEnum.hasMoreElements())
                {
                    DefaultMutableTreeNode appNode = (DefaultMutableTreeNode) appEnum.nextElement();
                    ApplicationConfigVO appUserObject = (ApplicationConfigVO) appNode.getUserObject();
                    ApplicationConfigVO app = new ApplicationConfigVO();
                    app.setContainerName(appUserObject.getContainerName());
                    app.setDeployName(appUserObject.getDeployName());
                    app.setEarFileName(appUserObject.getEarFileName());
                    app.setParentApplication(appUserObject.getParentApplication());
                    
                    server.addApplication(app);
                }
                
                env.addServer(server);
            }
            config.put(env.getName(),env);
        }
        
        return config;
    }
}
