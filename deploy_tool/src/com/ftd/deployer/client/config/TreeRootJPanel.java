package com.ftd.deployer.client.config;

import com.ftd.deployer.vo.EnvironmentConfigVO;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

public class TreeRootJPanel extends ContentEditJPanel
{
    protected JButton addEnvironmentJButton;
    
    public TreeRootJPanel(DefaultMutableTreeNode node, DefaultTreeModel treeModel)
    {
        super(node, treeModel);
        init();
    }
    
    public void applyChanges()
    {
        // Do nothing with this, nothing to change
    }
    
    protected void init()
    {
        addEnvironmentJButton = new JButton("Add Environment");
        addEnvironmentJButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                addEnvironment();
            }
        });
        this.add(addEnvironmentJButton);
    }
    
    protected void addEnvironment()
    {
        EnvironmentConfigVO vo = new EnvironmentConfigVO();
        vo.setName("New Environment");
        DefaultMutableTreeNode envNode = new DefaultMutableTreeNode(vo);
        
        node.add(envNode);
        treeModel.nodeStructureChanged(node);
    }
}
