package com.ftd.deployer.client.config;

import com.ftd.deployer.vo.ApplicationConfigVO;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import java.awt.event.ActionEvent;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

public class TreeApplicationJPanel extends ContentEditJPanel
{
    protected JTextField earJTextField;
    protected JTextField deployNameJTextField;
    protected JTextField containerJTextField;
    protected JTextField parentApplicationJTextField;
    protected JLabel earJLabel = new JLabel("Ear File Name:");
    protected JLabel deployNameJLabel = new JLabel("Deploy Name:");
    protected JLabel containerJLabel = new JLabel("Container Name:");
    protected JLabel parentApplicationJLabel = new JLabel("Parent Application Name:");
    
    public TreeApplicationJPanel(DefaultMutableTreeNode node, DefaultTreeModel treeModel)
    {
        super(node, treeModel);
        init();
    }

    /**
     * Take the fields from the screen and put them in the tree node.  
     */
    public void applyChanges()
    {
        ApplicationConfigVO vo = (ApplicationConfigVO) node.getUserObject();
        vo.setContainerName(containerJTextField.getText());
        vo.setDeployName(deployNameJTextField.getText());
        vo.setEarFileName(earJTextField.getText());
        vo.setParentApplication(parentApplicationJTextField.getText());
    }

    protected void init()
    {
        this.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        
        earJTextField = new JTextField(30);
        deployNameJTextField = new JTextField(30);
        containerJTextField = new JTextField(30);
        parentApplicationJTextField = new JTextField(30);
        
        ApplicationConfigVO vo = (ApplicationConfigVO) node.getUserObject();
        earJTextField.setText(vo.getEarFileName());
        deployNameJTextField.setText(vo.getDeployName());
        containerJTextField.setText((vo.getContainerName()));;
        parentApplicationJTextField.setText(vo.getParentApplication());
        
        buttonJPanel = new JPanel();
        buttonJPanel.setLayout(new FlowLayout());
        applyChangesJButton = new JButton("Apply Changes");
        applyChangesJButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                applyChanges();
            }
        });
        buttonJPanel.add(applyChangesJButton);
        
        this.add(deployNameJLabel,gbc);
        gbc.gridx++;
        this.add(deployNameJTextField,gbc);
        gbc.gridx = 0;
        gbc.gridy++;
        this.add(containerJLabel,gbc);
        gbc.gridx++;
        this.add(containerJTextField,gbc);
        gbc.gridx = 0;
        gbc.gridy++;
        this.add(earJLabel,gbc);
        gbc.gridx++;
        this.add(earJTextField,gbc);
        gbc.gridx = 0;
        gbc.gridy++;
        this.add(parentApplicationJLabel,gbc);
        gbc.gridx++;
        this.add(parentApplicationJTextField,gbc);
        gbc.gridx = 0;
        gbc.gridy++;
        gbc.gridwidth = 2;
        this.add(buttonJPanel,gbc);
    }

}
