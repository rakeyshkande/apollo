package com.ftd.deployer.server;

import com.ftd.deployer.server.impl.ConfigAPIImpl;
import com.ftd.deployer.server.impl.DeployAPIImpl;
import com.ftd.deployer.server.impl.jboss.DeployAPIJboss51Impl;

public class ServerFactory
{
    protected static ConfigAPI configAPI = null;
    
    public static ConfigAPI getConfigAPI()
    {
        if (configAPI == null)
        {
            configAPI = new ConfigAPIImpl();
        }
        return configAPI;
    }
    
    /**
     * Returns an appropriate Deployment API Implementation for the 
     * given Server Type
     * @param serverType
     * @return
     */
    public static DeployAPI getDeployAPI(String serverType)
    {
        if(ServerTypes.SERVER_TYPE_JBOSS_5_1.equalsIgnoreCase(serverType))
        {
            return new DeployAPIJboss51Impl();
        } else if (ServerTypes.SERVER_TYPE_OC4J.equalsIgnoreCase(serverType))
        {
            return new DeployAPIImpl();
        }
        
        throw new RuntimeException("Unrecognized Server Type: " + serverType);
    }
}
