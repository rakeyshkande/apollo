package com.ftd.deployer.server;

import javax.enterprise.deploy.spi.status.ProgressListener;
import javax.enterprise.deploy.spi.status.ProgressObject;

/**
 * API for doing deploys.  This hides the implementation details of the
 * deployment.
 */
public interface DeployAPI
{
    /**
     * Prepares for the deployment of an ear file to a server.
     * @param serverName
     * @param instanceName
     * @param password
     * @param earFilePath
     * @param deploymentName
     * @param parentDeploymentName
     * @param listener
     */
     public void preDeployStart(String serverName,
                        String instanceName,
                        String password,
                        String earFilePath,
                        String deploymentName,
                        String parentDeploymentName,
                        ProgressListener listener) throws Exception;    
    
     /**
      * Prepares for the deployment of an ear file to a server.
      * @param serverName
      * @param instanceName
      * @param password
      * @param earFilePath
      * @param deploymentName
      * @param parentDeploymentName
      * @param listener
      */
      public void preDeployComplete(String serverName,
                         String instanceName,
                         String password,
                         String earFilePath,
                         String deploymentName,
                         String parentDeploymentName,
                         ProgressListener listener) throws Exception;         
    
    /**
     * Perform a deploy of an ear file to a server.
     * @param serverName
     * @param instanceName
     * @param password
     * @param earFilePath
     * @param deploymentName
     * @param parentDeploymentName
     * @param listener
     */
     public ProgressObject deploy(String serverName,
                        String instanceName,
                        String password,
                        String earFilePath,
                        String deploymentName,
                        String parentDeploymentName,
                        ProgressListener listener) throws Exception;
      
      /**
       * Completes for the deployment of an ear file to a server.
       * @param serverName
       * @param instanceName
       * @param password
       * @param earFilePath
       * @param deploymentName
       * @param parentDeploymentName
       * @param listener
       */
       public void postDeployStart(String serverName,
                          String instanceName,
                          String password,
                          String earFilePath,
                          String deploymentName,
                          String parentDeploymentName,
                          ProgressListener listener) throws Exception;    
       
       /**
        * Completes for the deployment of an ear file to a server.
        * @param serverName
        * @param instanceName
        * @param password
        * @param earFilePath
        * @param deploymentName
        * @param parentDeploymentName
        * @param listener
        */
        public void postDeployComplete(String serverName,
                           String instanceName,
                           String password,
                           String earFilePath,
                           String deploymentName,
                           String parentDeploymentName,
                           ProgressListener listener) throws Exception;          
       
        /**
         * Prepares for the deployment of an ear file to a server.
         * @param serverName
         * @param instanceName
         * @param password
         * @param earFilePath
         * @param deploymentName
         * @param parentDeploymentName
         * @param listener
         */
         public void preUnDeployStart(String serverName,
                            String instanceName,
                            String password,
                            String earFilePath,
                            String deploymentName,
                            String parentDeploymentName,
                            ProgressListener listener) throws Exception;    
        
         /**
          * Prepares for the deployment of an ear file to a server.
          * @param serverName
          * @param instanceName
          * @param password
          * @param earFilePath
          * @param deploymentName
          * @param parentDeploymentName
          * @param listener
          */
          public void preUnDeployComplete(String serverName,
                             String instanceName,
                             String password,
                             String earFilePath,
                             String deploymentName,
                             String parentDeploymentName,
                             ProgressListener listener) throws Exception;        
          
    /**
     * Perform an undeploy of an ear file to a server.
     * @param serverName
     * @param instanceName
     * @param password
     * @param deploymentName - The deployment name in Oracle. Not used in JBOSS
     * @param listener
     * @param earFileName - name of the .ear artifact. Only used in jboss
     */
     public ProgressObject unDeploy(String serverName,
                        String instanceName,
                        String password,
                        String deploymentName,
                        ProgressListener listener,
                        String earFileName) throws Exception;

     /**
      * Prepares for the deployment of an ear file to a server.
      * @param serverName
      * @param instanceName
      * @param password
      * @param earFilePath
      * @param deploymentName
      * @param parentDeploymentName
      * @param listener
      */
      public void postUnDeployStart(String serverName,
                         String instanceName,
                         String password,
                         String earFilePath,
                         String deploymentName,
                         String parentDeploymentName,
                         ProgressListener listener) throws Exception;    
     
      /**
       * Prepares for the deployment of an ear file to a server.
       * @param serverName
       * @param instanceName
       * @param password
       * @param earFilePath
       * @param deploymentName
       * @param parentDeploymentName
       * @param listener
       */
       public void postUnDeployComplete(String serverName,
                          String instanceName,
                          String password,
                          String earFilePath,
                          String deploymentName,
                          String parentDeploymentName,
                          ProgressListener listener) throws Exception;        

     /**
      * Shutdowns the specified server
      * @param serverName
      * @param instanceName
      * @param password
      * @param listener
      * @throws Exception
      */
     public ProgressObject invokeShutdown(String serverName,
             String instanceName,
             String password,
             ProgressListener listener) throws Exception;
     
     
     /**
      * Shutdowns the specified server
      * @param serverName
      * @param instanceName
      * @param password
      * @param listener
      * @throws Exception
      */
     public ProgressObject verifyShutdown(String serverName,
             String instanceName,
             String password,
             ProgressListener listener) throws Exception;     
     
     /**
      * Starts up the specified server
      * @param serverName
      * @param instanceName
      * @param password
      * @param listener
      * @throws Exception
      */
     public ProgressObject invokeStartup(String serverName,
             String instanceName,
             String password,
             ProgressListener listener) throws Exception;     
     
     /**
      * Starts up the specified server
      * @param serverName
      * @param instanceName
      * @param password
      * @param listener
      * @throws Exception
      */
     public ProgressObject verifyStartup(String serverName,
             String instanceName,
             String password,
             ProgressListener listener) throws Exception;         
     
     
     
     
     /**
      * Perform an undeploy of an ear file to a server.
      * @param serverName
      * @param instanceName
      * @param password
      * @param deploymentName - The deployment name in Oracle. Not used in JBOSS
      * @param listener
      * @param earFileName - name of the .ear artifact. Only used in jboss
      */
      public ProgressObject getDeployStatus(String serverName,
                                            String instanceName,
                                            String password,
                                            String deploymentName,
                                            ProgressListener listener,
                                            String earFileName) throws Exception;
}
