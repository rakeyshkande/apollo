package com.ftd.deployer.server;

import com.ftd.deployer.vo.EnvironmentConfigVO;

import java.io.File;
import java.io.IOException;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * API for dealing with environment configurations.
 */
public interface ConfigAPI
{
    /**
     * Iniialize the config API with a configuration file
     * @param configFile
     * @throws IOException
     */
    public void initialize(File configFile) throws Exception;

    /**
     * Get a list of all the environments in the current configuration.
     * @return
     */
    public Set getEnvironmentList();
    
    
    public EnvironmentConfigVO getEnvironmentConfiguration(String configurationName);
    public void saveEnvironmentConfiguration(Map<String,EnvironmentConfigVO> config) throws Exception;
    
}
