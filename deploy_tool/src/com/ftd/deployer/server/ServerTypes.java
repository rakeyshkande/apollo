package com.ftd.deployer.server;

/**
 * Represents the various Server Types supported by the Deploy Tool
 */
public class ServerTypes
{
    /**
     * The OC4J Server Type. This is the default Type
     */
    public static final String SERVER_TYPE_OC4J = "oc4j";
    
    /**
     * The JBOSS 5.1 Server Type.
     */
    public static final String SERVER_TYPE_JBOSS_5_1 = "jboss_5.1";
    
    /**
     * The default Server Type if not specified in the config file
     */
    public static final String SERVER_DEFAULT_TYPE = SERVER_TYPE_OC4J;
    
    
    public static final String[] SERVER_TYPES = {SERVER_TYPE_JBOSS_5_1, SERVER_TYPE_OC4J};


    public static String updateContainerName(String containerName, String serverType)
    {
        if(SERVER_TYPE_OC4J.equals(serverType))
        {
            containerName = containerName.toUpperCase();
            if(!containerName.startsWith("OC4J_"))
            {
                containerName = "OC4J_" + containerName;
            }
        } else if(SERVER_TYPE_JBOSS_5_1.equals(serverType))
        {
            containerName = containerName.toLowerCase();
            if(containerName.startsWith("oc4j_"))
            {
                containerName = containerName.substring(5);
            }            
        }
        
        return containerName;
    }
}
