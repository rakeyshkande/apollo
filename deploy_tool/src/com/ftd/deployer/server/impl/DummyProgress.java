package com.ftd.deployer.server.impl;

import javax.enterprise.deploy.shared.ActionType;
import javax.enterprise.deploy.shared.CommandType;
import javax.enterprise.deploy.shared.StateType;
import javax.enterprise.deploy.spi.TargetModuleID;
import javax.enterprise.deploy.spi.status.ClientConfiguration;
import javax.enterprise.deploy.spi.status.DeploymentStatus;
import javax.enterprise.deploy.spi.status.ProgressEvent;
import javax.enterprise.deploy.spi.status.ProgressListener;
import javax.enterprise.deploy.spi.status.ProgressObject;

public class DummyProgress implements ProgressObject
{
    ProgressListener listener;
    DeploymentStatus status;
    
    public DummyProgress(ProgressListener listener)
    {
        this.listener = listener;
        status = new DeploymentStatusImpl("Start", true);
        fireEvent();
        status = new DeploymentStatusImpl("Complete", true);
        fireEvent();
        
    }
    
    protected void fireEvent()
    {
        ProgressEvent event = new ProgressEvent(this, null, status);
        
        listener.handleProgressEvent(event);
    }


    public DeploymentStatus getDeploymentStatus()
    {
        return status;
    }

    public TargetModuleID[] getResultTargetModuleIDs()
    {
        return new TargetModuleID[0];
    }

    public ClientConfiguration getClientConfiguration(TargetModuleID targetModuleID)
    {
        return null;
    }

    public boolean isCancelSupported()
    {
        return false;
    }

    public void cancel()
    {
    }

    public boolean isStopSupported()
    {
        return false;
    }

    public void stop()
    {
    }

    public void addProgressListener(ProgressListener progressListener)
    {
    }

    public void removeProgressListener(ProgressListener progressListener)
    {
    }
    
    class DeploymentStatusImpl implements DeploymentStatus
    {
        String message;
        boolean complete;
        
        DeploymentStatusImpl(String message, boolean complete)
        {
            this.message = message;
            this.complete = complete;
        }
        
        public StateType getState()
        {
            return null;
        }

        public CommandType getCommand()
        {
            return null;
        }

        public ActionType getAction()
        {
            return null;
        }

        public String getMessage()
        {
            return message;
        }

        public boolean isCompleted()
        {
            return complete;
        }

        public boolean isFailed()
        {
            return false;
        }

        public boolean isRunning()
        {
            return !complete;
        }
    }
}
