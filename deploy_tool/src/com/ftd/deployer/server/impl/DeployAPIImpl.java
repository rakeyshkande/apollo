package com.ftd.deployer.server.impl;

import com.ftd.deployer.server.DeployAPI;

import java.io.File;

import java.util.HashMap;
import java.util.Map;

import javax.enterprise.deploy.shared.ModuleType;
import javax.enterprise.deploy.shared.factories.DeploymentFactoryManager;
import javax.enterprise.deploy.spi.Target;
import javax.enterprise.deploy.spi.TargetModuleID;
import javax.enterprise.deploy.spi.factories.DeploymentFactory;
import javax.enterprise.deploy.spi.status.ProgressListener;
import javax.enterprise.deploy.spi.status.ProgressObject;

import oracle.oc4j.admin.deploy.shared.util.DeploymentUtils;
import oracle.oc4j.admin.deploy.spi.ProprietaryConnectedDeploymentManager;
import oracle.oc4j.admin.deploy.spi.factories.Oc4jDeploymentFactory;

import org.apache.log4j.Logger;

public class DeployAPIImpl extends AbstractDeployAPI implements DeployAPI
{
    private static Logger logger  = Logger.getLogger(DeployAPIImpl.class);

    String START_MODULE_ID = "oc4j:j2eeType=J2EEApplication,name=";
    String END_MODULE_ID   = ",J2EEServer=standalone";

    public DeployAPIImpl()
    {
    }

    public ProgressObject deploy(String serverName,
                       String instanceName,
                       String password,
                       String earFilePath,
                       String deploymentName,
                       String parentDeploymentName,
                       ProgressListener listener) throws Exception
    {
        if (serverName.equals("fe1.ftdi.com") || serverName.equals("fe2.ftdi.com") || serverName.equals("fe3.ftdi.com"))
        {
            return new DummyProgress(listener);    
        }
        
        String usr = "oc4jadmin";        
        String uri = "deployer:oc4j:opmn://" + serverName + ":6003/" + instanceName;
        DeploymentFactoryManager dfm = DeploymentFactoryManager.getInstance();
        DeploymentFactory df = new Oc4jDeploymentFactory(); 
        dfm.registerDeploymentFactory(df);
        ProprietaryConnectedDeploymentManager dm = (ProprietaryConnectedDeploymentManager) dfm.getDeploymentManager(uri, usr, password);
        Target[] targets = dm.getTargets();

        File earFile = new File(earFilePath);
        
        // Build a deployment plan
        oracle.oc4j.admin.management.shared.SharedModuleType type = DeploymentUtils.getSharedModuleType(earFilePath);
        Map extraParams = new HashMap();
        extraParams.put("bindWebApp", "default-web-site");
        extraParams.put("undeployPrevious","true");  
        if (parentDeploymentName != null && !(parentDeploymentName.equals("")))
        {
            extraParams.put("parent",parentDeploymentName);            
        }
        java.io.ByteArrayInputStream deploymentPlanIn = DeploymentUtils.getDeploymentPlan(deploymentName, extraParams, type);
        
        logger.info("Deploying " + deploymentName + " to " + instanceName + " " + serverName);
        ProgressObject progress = dm.distribute(targets,earFile,deploymentPlanIn,listener);
        
        return progress;
    }

    public ProgressObject unDeploy(String serverName,
                       String instanceName,
                       String password,
                       String deploymentName,
                       ProgressListener listener,
                       String earFileName) throws Exception
    {
        String usr = "oc4jadmin";        
        String uri = "deployer:oc4j:opmn://" + serverName + ":6003/" + instanceName;
        DeploymentFactoryManager dfm = DeploymentFactoryManager.getInstance();
        DeploymentFactory df = new Oc4jDeploymentFactory(); 
        dfm.registerDeploymentFactory(df);
        ProprietaryConnectedDeploymentManager dm = (ProprietaryConnectedDeploymentManager) dfm.getDeploymentManager(uri, usr, password);
        Target[] targets = dm.getTargets();
        ModuleType type = ModuleType.EAR;
        TargetModuleID[] availableTargetModuleIDs = dm.getAvailableModules(type,targets);
        TargetModuleID[] targetModuleIDs = new TargetModuleID[1];
        String targetModuleID = buildModuleID(deploymentName);
        for (int i=0; i < availableTargetModuleIDs.length;i++)
        {
            logger.debug("availableTargetModuleID is " + availableTargetModuleIDs[i].getModuleID());
            if (availableTargetModuleIDs[i].getModuleID().equals(targetModuleID))
            {
                targetModuleIDs[0] = availableTargetModuleIDs[i];
            }
        }

        logger.info("Undeploying " + deploymentName + " from " + instanceName + " " + serverName);
        ProgressObject progress = null;
        if (targetModuleIDs[0] == null)
        {
            // nothing to undeploy
            logger.info("Nothing to undeploy");
        }
        else
        {
            progress = dm.undeploy(targetModuleIDs,listener);
        }
        
        return progress;
    }
    
    public Boolean deployTest(String serverName,
                     String instanceName,
                     String password)
    {
      try
      {
        if (serverName.equals("fe1.ftdi.com") || serverName.equals("fe2.ftdi.com") || serverName.equals("fe3.ftdi.com"))
        {
            return true;    
        }
        
        String usr = "oc4jadmin";        
        String uri = "deployer:oc4j:opmn://" + serverName + ":6003/" + instanceName;
        DeploymentFactoryManager dfm = DeploymentFactoryManager.getInstance();
        DeploymentFactory df = new Oc4jDeploymentFactory(); 
        dfm.registerDeploymentFactory(df);
        ProprietaryConnectedDeploymentManager dm = (ProprietaryConnectedDeploymentManager) dfm.getDeploymentManager(uri, usr, password);
        return true;
      }
      catch (Exception e)
      {
        logger.error(e);
        return false;
      }
    }

    protected String buildModuleID(String appName)
    {
        return START_MODULE_ID + appName + END_MODULE_ID;
    }
    
    
}
