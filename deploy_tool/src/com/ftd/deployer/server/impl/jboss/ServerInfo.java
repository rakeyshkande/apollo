package com.ftd.deployer.server.impl.jboss;

/**
 * Provides information for a server
 */
public class ServerInfo
{
    String pid;
    String serverName;
    int portOffset = -1;
    ServerStatus serverStatus = ServerStatus.Inactive;
    String serverURL;
    
    
    public enum ServerStatus {
        Active,             // Process is Active and Web Interface is Up
        Inactive,           // Process is Inactive
        Unknown             // Process is Active, but Web Interface is not accessible
    }

    public String getPid()
    {
        return pid;
    }

    public void setPid(String pid)
    {
        this.pid = pid;
    }

    public String getServerName()
    {
        return serverName;
    }

    public void setServerName(String serverName)
    {
        this.serverName = serverName;
    }

    public ServerStatus getServerStatus()
    {
        return serverStatus;
    }

    public void setServerStatus(ServerStatus serverStatus)
    {
        this.serverStatus = serverStatus;
    }

    public int getPortOffset()
    {
        return portOffset;
    }

    public void setPortOffset(String portOffsetStr)
    {
        try
        {
            this.portOffset = Integer.parseInt(portOffsetStr);
        } catch (Exception e)
        {
            this.portOffset = -1;
        }
    }
    
    
    
    public String toString()
    {
        return pid + "\t" + serverName + ":" + portOffset + "\t" + serverStatus;
    }

    public String getServerURL()
    {
        return serverURL;
    }

    public void setServerURL(String serverURL)
    {
        this.serverURL = serverURL;
    }

    public void setPortOffset(int portOffset)
    {
        this.portOffset = portOffset;
    }
}
