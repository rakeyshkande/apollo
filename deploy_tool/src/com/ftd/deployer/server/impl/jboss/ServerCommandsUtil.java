package com.ftd.deployer.server.impl.jboss;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

import com.ftd.deployer.server.impl.jboss.ServerInfo.ServerStatus;
import com.ftd.deployer.util.SchUtil;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * Executes Server Commands. This is a helper class
 * @author Arajoo
 *
 */
public class ServerCommandsUtil
{
    Logger logger = new Logger(ServerCommandsUtil.class.getName());
    SchUtil schUtil;
    JBossConfig jBossConfig;
    String serverHost;
    
    public ServerCommandsUtil(SchUtil schUtil, JBossConfig jBossConfig, String serverHost)
    {
        this.schUtil = schUtil;
        this.jBossConfig = jBossConfig;
        this.serverHost = serverHost;
    }
    
   
    /**
     * Sample Output of this command is: 
     * oracle    3788  0.1  2.2407032346096 ?        S   Jun 24 166:36 /usr/jdk/jdk1.6.0_20/bin/java -Dprogram.name=run.sh -server -Xms128m -Xmx256m -XX:MaxPermSize=128m -verbose:gc -XX:+UseParallelGC -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -Dorg.jboss.resolver.warning=true -Dsun.rmi.dgc.client.gcInterval=3600000 -Dsun.rmi.dgc.server.gcInterval=3600000 -Djava.endorsed.dirs=/u01/app/jboss-5.1.0.GA/lib/endorsed -classpath /u01/app/jboss-5.1.0.GA/bin/run.jar:/usr/jdk/jdk1.6.0_20/lib/tools.jar org.jboss.Main -b 0.0.0.0 -P /u01/app/jboss-5.1.0.GA/config/apollo.properties -c florist_maint -Djboss.bind.port.offset=4
     * Column 2 = PID
     * Container Name = -c florist_maint
     * Port Offset = -Djboss.bind.port.offset=4
     * @return
     * @throws Exception
     */
    public Map<String, ServerInfo> getServerStatus() throws Exception
    {
        return getServerStatusImpl(null);
    }
    
    /**
     * If specificContainer is not null, only information for that container is retrieved
     * 
     * Sample Output of this command is: 
     * oracle    3788  0.1  2.2407032346096 ?        S   Jun 24 166:36 /usr/jdk/jdk1.6.0_20/bin/java -Dprogram.name=run.sh -server -Xms128m -Xmx256m -XX:MaxPermSize=128m -verbose:gc -XX:+UseParallelGC -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -Dorg.jboss.resolver.warning=true -Dsun.rmi.dgc.client.gcInterval=3600000 -Dsun.rmi.dgc.server.gcInterval=3600000 -Djava.endorsed.dirs=/u01/app/jboss-5.1.0.GA/lib/endorsed -classpath /u01/app/jboss-5.1.0.GA/bin/run.jar:/usr/jdk/jdk1.6.0_20/lib/tools.jar org.jboss.Main -b 0.0.0.0 -P /u01/app/jboss-5.1.0.GA/config/apollo.properties -c florist_maint -Djboss.bind.port.offset=4
     * Column 2 = PID
     * Container Name = -c florist_maint
     * Port Offset = -Djboss.bind.port.offset=4
     * @return
     * @throws Exception
     */
    private Map<String, ServerInfo> getServerStatusImpl(String specificContainer) throws Exception
    {
        if(specificContainer != null)
        {
            logger.info("Retrieving Server Status: " + specificContainer);
        } else
        {
            logger.info("Retrieving Server Status");
        }
        
        Map<String, ServerInfo> retVal = new HashMap<String, ServerInfo>();
        
        String command = jBossConfig.getProperty("command.list.server.all.process");
        
        StringBuffer response = new StringBuffer();
        schUtil.executeCommand(command, response, true);
        
        String[] items = response.toString().split("\n"); // Process each line
        
        for(String item: items)
        {
            String[] itemParts = item.split("\\s+");
            
            String pid = itemParts[1];
            String serverName = "";
            String portOffset = "";
            for(int iLoop = 0; iLoop < itemParts.length; iLoop ++)
            {
                if(itemParts[iLoop].equals("-c"))
                {
                    serverName = itemParts[iLoop + 1];
                } else if(itemParts[iLoop].contains("-Djboss.bind.port.offset="))
                {
                    portOffset = itemParts[iLoop].split("=")[1];
                }
            }
            
            if(StringUtils.isEmpty(portOffset)|| StringUtils.isEmpty(serverName))
            {
                continue;
            }
            
            ServerInfo serverInfo = new ServerInfo();
            serverInfo.setPid(pid);
            serverInfo.setServerName(serverName);
            serverInfo.setPortOffset(portOffset);
            serverInfo.setServerStatus(ServerStatus.Unknown); // For now

            if(specificContainer == null)
            {
                retVal.put(serverName, serverInfo);
            } else if(specificContainer.equals(serverName))
            {
                retVal.put(serverName, serverInfo);
                break;                
            }
        }
        
        // At this time, the server's are in 'Unknown Status'. 
        // Check to see if the web port is available
        for(String serverName: retVal.keySet())
        {
            ServerInfo serverInfo = retVal.get(serverName);
            
            int baseHttpPort = jBossConfig.getPropertyAsInt("base.port.http", -1);
            if(baseHttpPort == -1)
            {
                throw new RuntimeException("System Error, base.port.http not valid");
            }
            
            int httpPort = baseHttpPort + serverInfo.getPortOffset();
            
            if(httpPort < baseHttpPort) // We got a -1 which is invalid
            {
                System.out.println("Invalid Port Offset for Server: " + serverName);
                continue;
            }
            
            logger.info("Server Process is Running: " + serverHost + "/" + serverInfo.serverName + "; PID: " + serverInfo.getPid());
            
            String url = "http://" + serverHost + ":" + httpPort + "/";
            if(isReachableHttpURL(url))
            {
                serverInfo.setServerStatus(ServerStatus.Active);
                serverInfo.setServerURL(url);
            }
        }
        
        return retVal;
    }
    
    /**
     * Checks if the specified server/port is reachable
     * @param url
     * @return
     * @throws Exception
     */
    private boolean isReachableHttpURL(String urlString)
    {        

        HttpURLConnection conn = null;
        InputStream input = null;
        
        try
        {
            URL httpUrl = new URL(urlString);
            logger.info("Checking Server Web Port: " + urlString);
            
            conn = (HttpURLConnection) httpUrl.openConnection();
            conn.setConnectTimeout(15 * 1000); // SET CONNECTION TIMEOUT TO 15
            conn.setReadTimeout(60 * 1000); // SET I/O READ TIMEOUT TO 60 seconds
            conn.setRequestMethod("GET");        
            conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 (.NET CLR 3.5.30729)");
            conn.connect();        
            input = conn.getInputStream(); 
            
            String page = IOUtils.toString(input);
            
            if(page.length() > 0) 
            {
                logger.info("Server Web Port is Up: " + urlString);
                return true;
            }
        } catch (Exception e)
        {
            logger.info("Server Web Port is not available: " + urlString + "->" + e.getMessage());
        } finally
        {
            IOUtils.closeQuietly(input);
            
            try 
            {
                conn.disconnect();
            } catch (Exception ex)
            {
                // NOP
            }
        }
        
        return false;
    }

    
    /**
     * Executes commands to check if the server is running
     * Note: This reuses the getServerStatus command so its not as efficient as it can be
     * @param containerName
     * @return
     */
    public ServerInfo getServerInfo(String containerName) throws Exception
    {
        Map<String, ServerInfo> serverContainerStatus = getServerStatusImpl(containerName);

        ServerInfo server = serverContainerStatus.get(containerName);
        
        if(server == null)
        {
            server = new ServerInfo();
            server.setServerStatus(ServerStatus.Inactive);
        }

        return server;
    }    
    
    /**
     * Executes commands to check if the server is running
     * Note: This reuses the getServerStatus command so its not as efficient as it can be
     * @param containerName
     * @return
     */
    public ServerStatus getServerStatus(String containerName) throws Exception
    {
        Map<String, ServerInfo> serverContainerStatus = getServerStatusImpl(containerName);

        ServerInfo server = serverContainerStatus.get(containerName);
        
        if(server == null)
        {
            return ServerStatus.Inactive;
        }

        return server.getServerStatus();
    }
    
    /**
     * Initiates the server shutdown command
     * @param instanceName
     * @return
     */
    public boolean stopServer(String instanceName) throws Exception
    {
        Map<String, String> commandParameters = new HashMap<String, String>();
        commandParameters.put("serverName", instanceName);
        
        String command = jBossConfig.getProperty("command.server.stop", commandParameters);
        
        StringBuffer response = new StringBuffer();
        return schUtil.executeCommand(command, response);
    }
    
    /**
     * Initiates the server shutdown command
     * @param instanceName
     * @return
     */
    public boolean startServer(String instanceName) throws Exception
    {
        Map<String, String> commandParameters = new HashMap<String, String>();
        commandParameters.put("serverName", instanceName);
        
        String command = jBossConfig.getProperty("command.server.start", commandParameters);
        
        StringBuffer response = new StringBuffer();
        return schUtil.executeCommand(command, response);
    }
    
    
    /**
     * Returns a Map of Servers/Containers as the .ear files deployed to them
     * The key is the container, the set is the list of ear files
     * 
     * @return
     * @throws Exception
     */
    
    public Map<String, Set<String>> getServerDeployments() throws Exception
    {
        return getServerDeployments(null);
    }
    
    /**
     * Returns a Map of Servers/Containers as well as the .ear files deployed to them
     * 
     * If serverContainer is not null, only that server is returned.
     * 
     * The command returns a list of items of the following form:
     *  <container>/deploy/<application.ear>
     *  accounting/deploy/accountingreporting.ear 
     * @return
     * @throws Exception
     */    
    public Map<String, Set<String>> getServerDeployments(String instanceName) throws Exception
    {
        Map<String, Set<String>> retVal = new HashMap<String, Set<String>>();
        
        String command = jBossConfig.getProperty("command.list.server.all.deployments");
        
        StringBuffer response = new StringBuffer();
        schUtil.executeCommand(command, response, true);
        
        String[] items = response.toString().split("\n"); // Process each line
        
        // each line is an .ear file
        for(String item: items)
        {
            String[] deployment = item.split("/");

            if(deployment.length != 3)
            {
                continue;
            }
            
            String container = deployment[0];
            String application = deployment[2];
            
            if(instanceName != null && container.equals(instanceName) == false)
            {
                continue; // Skip, we are only interested in a single container
            }
            
            Set<String> apps = retVal.get(container);
            if(apps == null)
            {
                apps = new HashSet<String>();
                retVal.put(container, apps);
            }
            
            
           apps.add(application);
       
        }
        
        return retVal;
    }
    
}
