package com.ftd.deployer.server.impl.jboss;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Set;

import javax.enterprise.deploy.spi.status.ProgressListener;
import javax.enterprise.deploy.spi.status.ProgressObject;

import org.apache.log4j.Logger;

import com.ftd.deployer.server.DeployAPI;
import com.ftd.deployer.server.impl.AbstractDeployAPI;
import com.ftd.deployer.server.impl.LocalProgress;
import com.ftd.deployer.server.impl.jboss.ServerInfo.ServerStatus;
import com.ftd.deployer.util.SchUtil;
import com.ftd.deployer.util.SystemOutProgressLogger;

/**
 * Implements the Deploy API For JBOSS 5.1
 */
public class DeployAPIJboss51Impl extends AbstractDeployAPI implements DeployAPI
{
    private static Logger logger = Logger.getLogger(DeployAPIJboss51Impl.class);

    private static JBossConfig jBossConfig = new JBossConfig("server_jboss_5.1.xml");
    
    private static final int VERIFY_TIMEOUT  = 1000 * 60 * 20; // 20 minutes
    private static final int RETRY_TIMEOUT  = 1000 * 10; // 10 seconds
    private static final int INVOKE_TIMEOUT = 1000 * 2; // 2 seconds

    /**
     * serverName = host instanceName = container password = login for user deploymentName = destination .ear file name
     */
    public ProgressObject deploy(String serverName, String instanceName, String password, String earFilePath,
            String deploymentName, String parentDeploymentName, ProgressListener listener) throws Exception
    {
        LocalProgress progress = new LocalProgress(listener);

        SchUtil schUtil = null;
        
        try
        {
            
            // Ensure the server is in shut-down status
            verifyShutdownImpl(serverName, instanceName, password, listener);            
            
            progress.setDeploymentStatusInfo("Deploying Application");      
            
            File localFile = new File(earFilePath);
            if(!checkDeployArtifact(progress, localFile))
            {
                return progress;
            }

            String remoteFilePath = jBossConfig.getRemoteEarFilePath(instanceName, localFile.getName());
            
            schUtil = new SchUtil(progress, serverName, "oracle", password);
            
            ServerCommandsUtil serverCommandsUtil = new ServerCommandsUtil(schUtil, DeployAPIJboss51Impl.jBossConfig, serverName);
            ServerStatus serverStatus = serverCommandsUtil.getServerStatus(instanceName);            
            
            if(ServerStatus.Inactive != serverStatus)
            {
                progress.failed("The server has to be shutdown prior to any deployments");
                return progress;
                
            }
                        
            progress.logProgress("Sending File: " + earFilePath + " to: " + remoteFilePath);
            
            if(!schUtil.sendFileSCP(earFilePath, remoteFilePath))
            {
                progress.failed("Error during File Transfer");
            } else
            {
                progress.completed();
            }
            
        }
        catch (Throwable t)
        {
            logger.error("Failed to deploy: " + t.getMessage(), t);
            progress.logProgress("Deployment Failed: " + t.getMessage(), t);
            progress.failed("Failed to deploy: " + t.getMessage());
        }
        finally
        {
            if(schUtil != null)
            {
                schUtil.endSession();
            }
        }

        return progress;
    }

    /**
     * Ensures the deploy artifact exists, has the correct name etc.
     * @param progress
     * @param artifact
     * @return
     * @throws IOException
     */
    private boolean checkDeployArtifact(LocalProgress progress, File artifact) throws IOException
    {
        if (!artifact.exists())
        {
            progress.failed("Local File Not Found");
            return false;
        }
        if(!artifact.getCanonicalFile().getName().equals(artifact.getName()))
        {
            progress.failed("Local File Name has case Mismatch with Configured ear Name. This must be corrected to ensure name is consistent on server");
            return false;                
        }
        
        return true;
    }

    /**
     * serverName = host instanceName = container password = login for user deploymentName = destination .ear file name
     */
    public ProgressObject unDeploy(String serverName, String instanceName, String password, String deploymentName,
            ProgressListener listener, String earFileName) throws Exception
    {
        LocalProgress progress = new LocalProgress(listener);

        SchUtil schUtil = null;
        
        try
        {
            // Ensure the server is in shut-down status
            verifyShutdownImpl(serverName, instanceName, password, listener);        
            
            progress.setDeploymentStatusInfo("Undeploying Application");                  
            
            String remoteFilePath = jBossConfig.getRemoteEarFilePath(instanceName, earFileName);
            
            schUtil = new SchUtil(progress, serverName, "oracle", password);
            
            ServerCommandsUtil serverCommandsUtil = new ServerCommandsUtil(schUtil, DeployAPIJboss51Impl.jBossConfig, serverName);
            ServerStatus serverStatus = serverCommandsUtil.getServerStatus(instanceName);            
            
            if(ServerStatus.Inactive != serverStatus)
            {
                progress.failed("The server has to be shutdown prior to any undeployments");
                return progress;
            }            
            
            progress.logProgress("Removing File: " + remoteFilePath);
            
            StringBuffer sb = new StringBuffer();
            
            if(!schUtil.executeCommand("rm -f " + remoteFilePath, sb))
            {
                progress.failed("Failed to remove file: " + remoteFilePath);
            } else
            {
                progress.completed();
            }
        }
        catch (Throwable t)
        {
            logger.error("Failed to undeploy: " + t.getMessage(), t);
            progress.logProgress("Undeploy Failed: " + t.getMessage(), t);
            progress.failed("Undeploy Failed");
        }
        finally
        {
            if(schUtil != null)
            {
                schUtil.endSession();
            }
        }

        return progress;
    }
    
    /**
     * Invokes shutdown of the container
     */
    public void preDeployStart(String serverName, String instanceName, String password, String earFilePath,
            String deploymentName, String parentDeploymentName, ProgressListener listener) throws Exception
    {
        invokeShutdown(serverName, instanceName, password, listener);
    }

    public void preDeployComplete(String serverName, String instanceName, String password, String earFilePath,
            String deploymentName, String parentDeploymentName, ProgressListener listener) throws Exception
    {
        // NOP. The deploy command will wait for the shutdown to complete
    }

    // Initiates Startup of the JBOSS Container
    public void postDeployStart(String serverName, String instanceName, String password, String earFilePath,
            String deploymentName, String parentDeploymentName, ProgressListener listener) throws Exception
    {
        invokeStartup(serverName, instanceName, password, listener);
    }

    // Verifies Startup of the JBOSS Container
    public void postDeployComplete(String serverName, String instanceName, String password, String earFilePath,
            String deploymentName, String parentDeploymentName, ProgressListener listener) throws Exception
    {
        verifyStartupImpl(serverName, instanceName, password, listener);
        
        File earName = new File(earFilePath);
        LocalProgress progress = new LocalProgress(listener);
        
        DeployStatus deployStatus = updateDeployStatus(serverName, instanceName, password,deploymentName, progress, earName.getName());
        
        if(deployStatus.applicationDeployed && deployStatus.isActive)
        {
            progress.completed();
        } else
        {
            progress.failed("Application Not Active");
        }
    }
    
    
    public void preUnDeployStart(String serverName, String instanceName, String password, String earFilePath,
            String deploymentName, String parentDeploymentName, ProgressListener listener) throws Exception
    {   
        invokeShutdown(serverName, instanceName, password, listener);
    }

    public void preUnDeployComplete(String serverName, String instanceName, String password, String earFilePath,
            String deploymentName, String parentDeploymentName, ProgressListener listener) throws Exception
    {
        // NOP. The undeploy will wait for the shutdown to complete
    }    

    public void postUnDeployComplete(String serverName, String instanceName, String password, String earFilePath,
            String deploymentName, String parentDeploymentName, ProgressListener listener) throws Exception
    {
        File earName = new File(earFilePath);
        LocalProgress progress = new LocalProgress(listener);
        
        DeployStatus deployStatus = updateDeployStatus(serverName, instanceName, password,deploymentName, progress, earName.getName());
        
        if(!deployStatus.applicationDeployed)
        {
            progress.completed();
        } else
        {
            progress.failed("Application Not Undeployed");
        }    
    }
    
    @Override
    public ProgressObject invokeShutdown(String serverName, String instanceName, String password, ProgressListener listener)
            throws Exception
    {
        LocalProgress progress = new LocalProgress(listener);
        SchUtil schUtil = null;
        
        try
        {
            progress.setDeploymentStatusInfo("Shutting Down Container");            
            progress.logProgress("Shutting down container/instance: " + instanceName);
            
            schUtil = new SchUtil(progress, serverName, "oracle", password);
            ServerCommandsUtil serverCommandsUtil = new ServerCommandsUtil(schUtil, DeployAPIJboss51Impl.jBossConfig, serverName);
            
            ServerInfo serverInfo = serverCommandsUtil.getServerInfo(instanceName);
            ServerStatus serverStatus = serverInfo.getServerStatus();
            
            if(ServerStatus.Inactive == serverStatus)
            {    
                return progress; // Nothing to Do
            } else if (ServerStatus.Unknown == serverStatus)
            {
                progress.logProgress("The server is in an unknown state. Generally, this means the process is in the process of shutting down/starting up. Check the Server Status");
            } else
            {
                // Invoke the shutdown command
                if(!serverCommandsUtil.stopServer(instanceName))
                {
                    progress.logProgress("Failed to initiate server shutdown");
                } 
                
                SchUtil.sleepQuietly(INVOKE_TIMEOUT);
            }
        } finally
        {
            if(schUtil != null)
            {
                schUtil.endSession();
            }
        }
        
        return progress;
        
    }


    @Override
    public ProgressObject invokeStartup(String serverName, String instanceName, String password, ProgressListener listener)
            throws Exception
    {
        LocalProgress progress = new LocalProgress(listener);
        SchUtil schUtil = null;
        
        try
        {
            progress.setDeploymentStatusInfo("Starting Up Container");                    
            progress.logProgress("Starting up container/instance: " + instanceName);
            
            schUtil = new SchUtil(progress, serverName, "oracle", password);
            ServerCommandsUtil serverCommandsUtil = new ServerCommandsUtil(schUtil, DeployAPIJboss51Impl.jBossConfig, serverName);
            
            ServerInfo serverInfo = serverCommandsUtil.getServerInfo(instanceName);
            ServerStatus serverStatus = serverInfo.getServerStatus();
            
            if(ServerStatus.Active == serverStatus)
            {               
                return progress; // Nothing to Do
            } else if (ServerStatus.Unknown == serverStatus)
            {
                // This means the server is in the process of starting up.
                return progress; // Nothing to Do
            } else
            {
                // Invoke the startup command
                if(!serverCommandsUtil.startServer(instanceName))
                {
                    progress.logProgress("Failed to initiate server start up");
                } 
                
                SchUtil.sleepQuietly(INVOKE_TIMEOUT);
            }
        } finally
        {
            if(schUtil != null)
            {
                schUtil.endSession();
            }
        }
                
        return progress;
    }

    @Override
    public ProgressObject verifyStartup(String serverName, String instanceName, String password, ProgressListener listener)
    throws Exception
    {
        LocalProgress progress = new LocalProgress(listener);
        
        if(!verifyStartupImpl(serverName, instanceName, password, listener))
        {
            progress.failed("Server did not startup within the startup period");   
            return progress;
        } else
        {
            progress.completed();
        }
        
        return progress;
    }

    private boolean verifyStartupImpl(String serverName, String instanceName, String password, ProgressListener listener)
            throws Exception
    {
        long startTime = System.currentTimeMillis();
        long endTime = startTime + VERIFY_TIMEOUT;
        
        LocalProgress progress = new LocalProgress(listener);
        SchUtil schUtil = null;
        
        try
        {
            progress.logProgress("Checking to see if container/instance is up: " + instanceName);
            
            schUtil = new SchUtil(progress, serverName, "oracle", password);
            ServerCommandsUtil serverCommandsUtil = new ServerCommandsUtil(schUtil, DeployAPIJboss51Impl.jBossConfig, serverName);
            
            
            while(true)
            {
                ServerInfo serverInfo= serverCommandsUtil.getServerInfo(instanceName);
                ServerStatus serverStatus = serverInfo.getServerStatus();

                if(ServerStatus.Active == serverStatus)
                {
                    progress.logProgress("Server is Active: " + instanceName);
                    return true; // Success
                } else if (ServerStatus.Unknown == serverStatus)
                {
                    progress.logProgress("Server starting up: " + instanceName);
                } else
                {
                    progress.failed("Server is Inactive, startup failed: " + instanceName);
                    break;
                }
                
                long currentTime = System.currentTimeMillis();
                
                if(currentTime > endTime)
                {
                    // Timed out
                    progress.failed("Server did not start within Timeout period. Check Server Logs/Status");
                    break;
                }
                
                // Wait before checking again
                try
                {
                    Thread.sleep(RETRY_TIMEOUT);
                } catch (Exception e)
                {
                    // NOP
                }
            }
            
        } finally
        {
            if(schUtil != null)
            {
                schUtil.endSession();
            }
        }
        
        return false;
    }

    public ProgressObject verifyShutdown(String serverName, String instanceName, String password, ProgressListener listener)
    throws Exception    
    {
        LocalProgress progress = new LocalProgress(listener);
        
        if(!verifyShutdownImpl(serverName, instanceName, password, listener))
        {
            progress.failed("Server did not shutdown within the shutdown period");            
        } else
        {
            progress.completed();
        }
        
        return progress;        
    }

    private boolean verifyShutdownImpl(String serverName, String instanceName, String password, ProgressListener listener)
            throws Exception
    {
        long startTime = System.currentTimeMillis();
        long endTime = startTime + VERIFY_TIMEOUT;
        
        LocalProgress progress = new LocalProgress(listener);
        SchUtil schUtil = null;
        
        try
        {
            progress.logProgress("Checking to see if container/instance is down: " + instanceName);
            
            schUtil = new SchUtil(progress, serverName, "oracle", password);
            ServerCommandsUtil serverCommandsUtil = new ServerCommandsUtil(schUtil, DeployAPIJboss51Impl.jBossConfig, serverName);
            
            while(true)
            {
                ServerInfo serverInfo= serverCommandsUtil.getServerInfo(instanceName);
                ServerStatus serverStatus = serverInfo.getServerStatus();
                                
                if(ServerStatus.Inactive == serverStatus)
                {
                    progress.logProgress("Server is Inactive: " + instanceName);       
                    return true; // Success
                } else if (ServerStatus.Unknown == serverStatus)
                {
                    // Shutdown may still be pending
                } else
                {
                    // Shutdown may still be pending
                }
                
                long currentTime = System.currentTimeMillis();
                
                if(currentTime > endTime)
                {                    
                    // Timed out
                    progress.failed("Server did not shutdown within Timeout period. Check Server Logs/Status");
                    break;
                }
                
                // Wait before checking again
                try
                {
                    Thread.sleep(RETRY_TIMEOUT);
                } catch (Exception e)
                {
                    // NOP
                }                
            }
            
        } finally
        {
            if(schUtil != null)
            {
                schUtil.endSession();
            }
        }
        
        return false;
    }

    
    /**
     * Returns the Deploy Status for an the passed in item
     */
    public ProgressObject getDeployStatus(String serverName, String instanceName, String password,
            String deploymentName, ProgressListener listener, String earFileName) throws Exception
    {
        LocalProgress progress = new LocalProgress(listener);

        updateDeployStatus(serverName, instanceName, password, deploymentName, progress, earFileName);
        progress.completed();
        
        return progress;
    }    
    
    
    private DeployStatus updateDeployStatus(String serverName, String instanceName, String password,
            String deploymentName, LocalProgress progress, String earFileName) throws Exception
    {
        SchUtil schUtil = null;
        
        String deployStatusText = "";
        
        DeployStatus appDeployStatus = new  DeployStatus();
        
        try
        {            
            schUtil = new SchUtil(progress, serverName, "oracle", password);
            
            ServerCommandsUtil serverCommandsUtil = new ServerCommandsUtil(schUtil, DeployAPIJboss51Impl.jBossConfig, serverName);
            
            // See if the app is deployed
            Set<String> applications = serverCommandsUtil.getServerDeployments(instanceName).get(instanceName);
            if(applications == null || !applications.contains(earFileName))
            {
                deployStatusText = "Not deployed;";
            } else
            {
                deployStatusText = "Deployed;";
                appDeployStatus.applicationDeployed = true;
            }

            // Get the status of the server and append to deploy status
            appDeployStatus.serverInfo = serverCommandsUtil.getServerInfo(instanceName);
            
            
            deployStatusText = deployStatusText + getServerStatusString(appDeployStatus);
            
            progress.setDeploymentStatusInfo(deployStatusText);

        }finally
        {
            if(schUtil != null)
            {
                schUtil.endSession();
            }
        }        
        
        return appDeployStatus;
    }
    
    private String getServerStatusString(DeployStatus appDeployStatus)
    {
        String statusText;
        if(appDeployStatus.serverInfo == null)
        {
            statusText = "Unknown";
        }
        else if(ServerStatus.Inactive == appDeployStatus.serverInfo.getServerStatus())
        {
            statusText = "Down";
        } else if (ServerStatus.Active == appDeployStatus.serverInfo.getServerStatus())
        {
            statusText = "Up;PID=" + appDeployStatus.serverInfo.getPid() + ";" + appDeployStatus.serverInfo.getServerURL();
            appDeployStatus.isActive= true; 
        } else
        {
            statusText = "Unknown;PID=" + appDeployStatus.serverInfo.getPid() + ";Http is Down";
        }        
        
        appDeployStatus.statusText = statusText;
        
        return statusText;
    }
    
    private class DeployStatus
    {
        public boolean applicationDeployed = false;
        public ServerInfo serverInfo;
        public String statusText;
        public boolean isActive = false;
    }
    
    public static void main(String[] args) throws Exception
    {
       SchUtil schUtil = new SchUtil(new SystemOutProgressLogger(), "brass1.ftdi.com", "oracle", "lli7tst");
       //SchUtil schUtil = new SchUtil(new SystemOutProgressLogger(), "copper1.ftdi.com", "oracle", "lli7tst");
       ServerCommandsUtil serverCommandsUtil = new ServerCommandsUtil(schUtil, DeployAPIJboss51Impl.jBossConfig, "brass1.ftdi.com");
        
       Map<String, ServerInfo> serverStatus = serverCommandsUtil.getServerStatus();
       System.out.println(serverStatus.toString());       
       
       Map<String, Set<String>> serverDeployments = serverCommandsUtil.getServerDeployments();
       System.out.println(serverDeployments.toString());       
       schUtil.endSession();
       

       
        
    }


}
