package com.ftd.deployer.server.impl.jboss;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.ftd.osp.utilities.j2ee.ResourceUtil;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.JAXPUtil;

/**
 * Maintains Configuration Information for JBOSS
 */
public class JBossConfig
{
 
    /**
     * Contains all the Properties in the Config File
     */
    Map<String, String> properties = new HashMap<String, String>();
    
    /**
     * Contains the JBOSS Home property
     */
    String jbossHome;
    
    /**
     * Instantiates the config using the passed in config file resource Path
     * @param configResourcePath
     */
    public JBossConfig(String configResourcePath)
    {
        try
        {
            loadConfig(configResourcePath);
        } catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    /**
     * Loads the passed in config resource
     * @param configResourcePath
     * @throws Exception
     */
    private void loadConfig(String configResourcePath) throws Exception
    {
        InputStream resrc = ResourceUtil.getInstance().getResourceFileAsStream(configResourcePath);
        Document configDoc = DOMUtil.getDocument(resrc);
        
        NodeList propertyList = configDoc.getElementsByTagName("property");
        for(int iLoop = 0; iLoop < propertyList.getLength(); iLoop ++)
        {
            Element propNode = (Element) propertyList.item(iLoop);
            
            String propName = propNode.getAttribute("name");
            String value = JAXPUtil.getTextValue(propNode);
            
            properties.put(propName, value);
        }
        
        jbossHome = properties.get("jboss.home");
    }

    
    /**
     * Returns the remote File path based on the container, and deployment Name
     * @param container
     * @param earFilePath The name of the .ear file
     * @param deploymentName
     * @return
     */
    public String getRemoteEarFilePath(String container, String earFilename)
    {
        String remotePath = jbossHome + "/server/" + container + "/deploy/" + earFilename;
        
        if(remotePath.endsWith(".ear") == false)
        {
            remotePath += ".ear";
        }

        return remotePath;
    }
    
    public static void main(String[] args)
    {
        new JBossConfig("server_jboss_5.1.xml");
    }

    /**
     * Returns a property from the config file
     * @param name
     * @return
     */
    public String getProperty(String name)
    {
        return getProperty(name, new HashMap<String, String>());
    }
    
    /**
     * Retrieves the passed in property name, and performs property substitutions within the command
     * if they are embedded as ${propertyName}=value in the propertySubstitutions map
     * @param name
     * @param propertySubstitutions
     * @return
     */
    public String getProperty(String name, Map<String, String> propertySubstitutions)
    {
        String retVal = properties.get(name);
        
        if(retVal == null)
        {
            return null;
        }
        
        for(String prop: propertySubstitutions.keySet())
        {
            String val = propertySubstitutions.get(prop);
            String key = "${" + prop + "}";

            retVal = retVal.replace(key, val);
        }
        
        return retVal;
    }

    /**
     * Returns the property as integer or the defaultValue if there is a parse exception
     * or the property is not found
     * @param name
     * @param defaultValue
     * @return
     */
    public int getPropertyAsInt(String name, int defaultValue)
    {
        String strProp = properties.get(name);
        
        if(strProp == null)
        {
            return defaultValue;
        }
        
        try
        {
            return Integer.parseInt(strProp);
        } catch (Exception e)
        {
            // NOP
        }
        
        return defaultValue;
    }
    
    
    public String getJBOSSHome()
    {
        return jbossHome;
    }
}
