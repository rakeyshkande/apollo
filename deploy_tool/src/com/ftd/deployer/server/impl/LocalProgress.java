package com.ftd.deployer.server.impl;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.enterprise.deploy.shared.ActionType;
import javax.enterprise.deploy.shared.CommandType;
import javax.enterprise.deploy.shared.StateType;
import javax.enterprise.deploy.spi.TargetModuleID;
import javax.enterprise.deploy.spi.status.ClientConfiguration;
import javax.enterprise.deploy.spi.status.DeploymentStatus;
import javax.enterprise.deploy.spi.status.ProgressEvent;
import javax.enterprise.deploy.spi.status.ProgressListener;
import javax.enterprise.deploy.spi.status.ProgressObject;

import com.ftd.deployer.client.deployer.DeploymentListener;
import com.ftd.deployer.client.deployer.DeploymentStatusProgressEvent;
import com.ftd.deployer.util.ProgressLogger;

/**
 * Implements a Local/Inline Progress Object where all the status updates occur
 * in the application.
 * @author Arajoo
 *
 */
public class LocalProgress implements ProgressObject, ProgressLogger
{
    ProgressListener listener;
    DeploymentStatusImpl status;
    
    public LocalProgress(ProgressListener listener)
    {
        this.listener = listener;
        logProgress("Started");
    }
    
    /**
     * Logs an intermediate progress
     * @param message
     */
    public void logProgress(String message)
    {
        logProgress(message, false);
    }
    
    /**
     * Logs an intermediate progress
     * @param message
     */
    public void logProgress(String message, Throwable t)
    {
        StringWriter stringWriter = new StringWriter();
        t.printStackTrace(new PrintWriter(stringWriter));
        
        logProgress(message + "\n" + stringWriter.toString(), false);
    }
    
    /**
     * Logs a new Progress to the Progress
     * @param message
     * @param completed
     */
    private void logProgress(String message, boolean completed)
    {
        DeploymentStatusImpl newStatus = new DeploymentStatusImpl(message, completed);
        
        if(newStatus.isRunning() && status != null)
        {
            if(status.isCompleted() || status.isFailed())
            {
                // Keep the same status
                newStatus.failed = status.failed;
                newStatus.complete = status.complete;
            }
        }
        
        ProgressEvent event = new ProgressEvent(this, null, newStatus);
        listener.handleProgressEvent(event);
        status = newStatus;
    }

    /**
     * Logs a deployment failure
     * @param message
     */
    private void logFailure(String message)
    {
        DeploymentStatusImpl newStatus = new DeploymentStatusImpl(message, false);
        newStatus.setFailed(true);
        ProgressEvent event = new ProgressEvent(this, null, newStatus);
        listener.handleProgressEvent(event);
        status = newStatus;
    }
    
    /**
     * deployement completed
     */
    public void completed()
    {
        logProgress("Complete", true);
    }
    
    /**
     * deployment failed
     */
    public void failed(String message)
    {
        logFailure(message);
    }

    public DeploymentStatus getDeploymentStatus()
    {
        return status;
    }

    public TargetModuleID[] getResultTargetModuleIDs()
    {
        return new TargetModuleID[0];
    }

    public ClientConfiguration getClientConfiguration(TargetModuleID targetModuleID)
    {
        return null;
    }

    public boolean isCancelSupported()
    {
        return false;
    }

    public void cancel()
    {
    }

    public boolean isStopSupported()
    {
        return false;
    }

    public void stop()
    {
    }

    public void addProgressListener(ProgressListener progressListener)
    {
    }

    public void removeProgressListener(ProgressListener progressListener)
    {
    }
    
    class DeploymentStatusImpl implements DeploymentStatus
    {
        String message;
        boolean complete;
        boolean failed = false;
        
        DeploymentStatusImpl(String message, boolean complete)
        {
            this.message = message;
            this.complete = complete;
        }
        
        public StateType getState()
        {
            return null;
        }

        public CommandType getCommand()
        {
            return null;
        }

        public ActionType getAction()
        {
            return null;
        }

        public String getMessage()
        {
            return message;
        }

        public boolean isCompleted()
        {
            return complete;
        }

        public boolean isFailed()
        {
            return failed;
        }
        
        public void setFailed(boolean bFailed)
        {
            failed = bFailed;
            if(failed)
            {
                complete = true;
            }
        }

        public boolean isRunning()
        {
            return !complete;
        }
    }
    
    public void setDeploymentStatusInfo(String deploymentInfo)
    {
        DeploymentStatusImpl newStatus = new DeploymentStatusImpl(deploymentInfo, false);
        
        if(status != null)
        {
            if(status.isCompleted() || status.isFailed())
            {
                // Keep the same status
                newStatus.failed = status.failed;
                newStatus.complete = status.complete;
            }
        }
        
        ProgressEvent event = new DeploymentStatusProgressEvent(this, null, newStatus);
        listener.handleProgressEvent(event);
    }
}
