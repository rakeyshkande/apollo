package com.ftd.deployer.server.impl;

public interface ConfigFileConstants
{
    public final static String NODE_ROOT_PATH          = "/root";
    public final static String NODE_ENVIRONMENT_PATH   = "/root/environment";

    public final static String NODE_ROOT               = "root";
    public final static String NODE_ENVIRONMENT        = "environment";
    public final static String NODE_SERVER             = "server";
    public final static String NODE_APPLICATION        = "application";
    public final static String NODE_EAR                = "ear";
    public final static String NODE_DEPLOY_NAME        = "deployName";
    public final static String NODE_CONTAINER          = "container";
    public final static String NODE_PARENT_APPLICATION = "parentApplication";
    public final static String ATTRIBUTE_NAME          = "name";
    public final static String ATTRIBUTE_HOST          = "host";
    public final static String ATTRIBUTE_SERVER_TYPE   = "type";
}
