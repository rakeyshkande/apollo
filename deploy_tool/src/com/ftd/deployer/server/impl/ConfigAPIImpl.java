package com.ftd.deployer.server.impl;

import com.ftd.deployer.server.ConfigAPI;
import com.ftd.deployer.vo.EnvironmentConfigVO;

import java.io.File;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ConfigAPIImpl implements ConfigAPI
{
    protected Map<String,EnvironmentConfigVO> environmentConfig;
    protected File configFile;
    
    public ConfigAPIImpl()
    {
        environmentConfig = new HashMap<String, EnvironmentConfigVO>();
    }

    public void initialize(File configFile) throws Exception
    {
        this.configFile = configFile;
        environmentConfig = readConfigFile(configFile);
    }
    
    public Set getEnvironmentList()
    {
        return environmentConfig.keySet();
    }

    public EnvironmentConfigVO getEnvironmentConfiguration(String configurationName)
    {
        return environmentConfig.get(configurationName);
    }

    public void saveEnvironmentConfiguration(Map<String,EnvironmentConfigVO> config) throws Exception
    {
        writeConfigFile(config);
    }
    
    private Map<String,EnvironmentConfigVO> readConfigFile(File configFile) throws Exception
    {
        ConfigFileReader reader = new ConfigFileReader(configFile);
        Map<String,EnvironmentConfigVO> map = reader.readFile();
        return map;
    }
    
    private void writeConfigFile(Map<String, EnvironmentConfigVO> config) throws Exception
    {
        ConfigFileWriter writer = new ConfigFileWriter(configFile);
        writer.writeFile(config);
    }

}
