package com.ftd.deployer.server.impl;

import javax.enterprise.deploy.spi.status.ProgressListener;
import javax.enterprise.deploy.spi.status.ProgressObject;

import com.ftd.deployer.server.DeployAPI;

/**
 * Provides NOP stubs for optional APIs
 */
public abstract class AbstractDeployAPI implements DeployAPI
{

    public void preDeployStart(String serverName, String instanceName, String password, String earFilePath,
            String deploymentName, String parentDeploymentName, ProgressListener listener) throws Exception
    {
        // NOP
    }

    public void preDeployComplete(String serverName, String instanceName, String password, String earFilePath,
            String deploymentName, String parentDeploymentName, ProgressListener listener) throws Exception
    {
        // NOP
    }

    public void postDeployStart(String serverName, String instanceName, String password, String earFilePath,
            String deploymentName, String parentDeploymentName, ProgressListener listener) throws Exception
    {
        // NOP
    }

    public void postDeployComplete(String serverName, String instanceName, String password, String earFilePath,
            String deploymentName, String parentDeploymentName, ProgressListener listener) throws Exception
    {
        // NOP
    }

    public ProgressObject invokeShutdown(String serverName, String instanceName, String password, ProgressListener listener)
            throws Exception
    {
        return new DummyProgress(listener);
    }
    
    
    
    

    public ProgressObject verifyShutdown(String serverName, String instanceName, String password, ProgressListener listener)
            throws Exception
    {
        return new DummyProgress(listener);
    }

    public ProgressObject invokeStartup(String serverName, String instanceName, String password, ProgressListener listener)
            throws Exception
    {
        return new DummyProgress(listener);
    }

    public ProgressObject verifyStartup(String serverName, String instanceName, String password, ProgressListener listener)
            throws Exception
    {
        return new DummyProgress(listener);
    }

    public void preUnDeployStart(String serverName, String instanceName, String password, String earFilePath,
            String deploymentName, String parentDeploymentName, ProgressListener listener) throws Exception
    {
        // NOP
        
    }

    public void preUnDeployComplete(String serverName, String instanceName, String password, String earFilePath,
            String deploymentName, String parentDeploymentName, ProgressListener listener) throws Exception
    {
        // NOP
        
    }

    public void postUnDeployStart(String serverName, String instanceName, String password, String earFilePath,
            String deploymentName, String parentDeploymentName, ProgressListener listener) throws Exception
    {
        // NOP
        
    }

    public void postUnDeployComplete(String serverName, String instanceName, String password, String earFilePath,
            String deploymentName, String parentDeploymentName, ProgressListener listener) throws Exception
    {
        // NOP
        
    }

    public ProgressObject getDeployStatus(String serverName,
            String instanceName,
            String password,
            String deploymentName,
            ProgressListener listener,
            String earFileName) throws Exception
    {
        return new DummyProgress(listener);
    }    
    

}
