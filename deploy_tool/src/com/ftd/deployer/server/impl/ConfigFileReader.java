package com.ftd.deployer.server.impl;

import com.ftd.deployer.server.ServerTypes;
import com.ftd.deployer.vo.ApplicationConfigVO;
import com.ftd.deployer.vo.EnvironmentConfigVO;

import com.ftd.deployer.vo.ServerConfigVO;

import com.ftd.osp.utilities.xml.JAXPUtil;

import java.io.BufferedReader;
import java.io.File;

import java.io.FileReader;
import java.io.IOException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ConfigFileReader implements ConfigFileConstants
{
    private static Logger logger  = Logger.getLogger(ConfigFileReader.class);
    protected File configFile;
    
    public ConfigFileReader(File configFile)
    {
        this.configFile = configFile;
    }
    
    
    public Map<String,EnvironmentConfigVO> readFile() throws Exception
    {
        if (!configFile.exists())
        {
            throw new IOException("File does not exist");
        }
        
        String fileContents = readFile(configFile);

        Document configDocument = JAXPUtil.parseDocument(fileContents);

        return parseConfigDocument(configDocument);
    }
    
    protected String readFile(File configFile) throws IOException
    {
        FileReader fileReader = new FileReader(configFile);
        BufferedReader bufferReader = new BufferedReader(fileReader);
        
        StringBuffer fileContents = new StringBuffer(1024);
        String line = bufferReader.readLine();
        while (line != null)
        {
            fileContents.append(line);
            line = bufferReader.readLine();
        }
        fileReader.close();
        
        return fileContents.toString();
    }
    

    private Map<String,EnvironmentConfigVO> parseConfigDocument(Document doc) throws Exception
    {
        Map<String,EnvironmentConfigVO> environments = new HashMap<String,EnvironmentConfigVO>();
        
        NodeList environmentNodes = JAXPUtil.selectNodes(doc,NODE_ENVIRONMENT_PATH);
        
        for (int i=0; i < environmentNodes.getLength();i++)
        {
            Node environmentNode = environmentNodes.item(i);
            EnvironmentConfigVO environmentConfig = new EnvironmentConfigVO();
            
            NamedNodeMap attributeMap = environmentNode.getAttributes();
            String name = attributeMap.getNamedItem(ATTRIBUTE_NAME).getTextContent();

            environmentConfig.setName(name);

            List<ServerConfigVO> serverList = getServerForEnvironment(environmentNode);
            environmentConfig.setServers(serverList);
            
            environments.put(environmentConfig.getName(),environmentConfig);
        }
        return environments;
    }
    
    protected List<ServerConfigVO> getServerForEnvironment(Node environmentNode) throws Exception
    {
        List<ServerConfigVO> servers = new ArrayList<ServerConfigVO>();
        NodeList serverNodes = JAXPUtil.selectNodes((Element)environmentNode,NODE_SERVER);
        
        for (int i=0; i < serverNodes.getLength();i++)
        {
            Node serverNode = serverNodes.item(i);
            ServerConfigVO serverConfig = new ServerConfigVO();
            NamedNodeMap attributeMap = serverNode.getAttributes();
            
            String name = attributeMap.getNamedItem(ATTRIBUTE_NAME).getTextContent();
            String host = attributeMap.getNamedItem(ATTRIBUTE_HOST).getTextContent();

            serverConfig.setName(name);
            serverConfig.setHostname(host);
            
            String serverType = ServerTypes.SERVER_DEFAULT_TYPE;
            if(attributeMap.getNamedItem(ATTRIBUTE_SERVER_TYPE) != null)
            {
                serverType = attributeMap.getNamedItem(ATTRIBUTE_SERVER_TYPE).getTextContent().trim();
            }
            
            serverConfig.setServerType(serverType);
            
            List<ApplicationConfigVO> applications = getApplicationsForServer(serverNode);
            serverConfig.setApplications(applications);
            
            servers.add(serverConfig);
        }
        
        return servers;
    }

    protected List<ApplicationConfigVO> getApplicationsForServer(Node serverNode) throws Exception
    {
        List<ApplicationConfigVO> applications = new ArrayList<ApplicationConfigVO>();
        NodeList applicationNodes = JAXPUtil.selectNodes((Element)serverNode,NODE_APPLICATION);
        
        for (int i=0; i < applicationNodes.getLength();i++)
        {
            Node applicationNode = applicationNodes.item(i);
            ApplicationConfigVO applicationConfig = getApplicationConfigFromNode(applicationNode);

            applications.add(applicationConfig);
        }
        
        return applications;
    }
    
    protected ApplicationConfigVO getApplicationConfigFromNode(Node applicationNode)
    {
        Element applicationElement = (Element) applicationNode;
        ApplicationConfigVO ac = new ApplicationConfigVO();

        ac.setContainerName(JAXPUtil.getFirstChildNodeTextByTagName(applicationElement,NODE_CONTAINER));
        ac.setDeployName(JAXPUtil.getFirstChildNodeTextByTagName(applicationElement,NODE_DEPLOY_NAME));
        ac.setEarFileName(JAXPUtil.getFirstChildNodeTextByTagName(applicationElement,NODE_EAR));
        ac.setParentApplication(JAXPUtil.getFirstChildNodeTextByTagName(applicationElement,NODE_PARENT_APPLICATION));
        
        return ac;
    }
        
}
