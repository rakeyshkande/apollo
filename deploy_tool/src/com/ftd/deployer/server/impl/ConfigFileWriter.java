package com.ftd.deployer.server.impl;

import com.ftd.deployer.vo.ApplicationConfigVO;
import com.ftd.deployer.vo.EnvironmentConfigVO;

import com.ftd.deployer.vo.ServerConfigVO;
import com.ftd.osp.utilities.xml.JAXPUtil;

import java.io.File;

import java.io.FileWriter;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class ConfigFileWriter implements ConfigFileConstants
{
    protected File configFile;
    
    public ConfigFileWriter(File configFile)
    {
        this.configFile = configFile;
    }

    public void writeFile(Map<String, EnvironmentConfigVO> config) throws Exception
    {
        makeBackupCopy();
        writeFileContents(configFile,config);        
    }
    
    /**
     * Write the contents of the config object to the file.
     */
    protected void writeFileContents(File configFile, Map<String, EnvironmentConfigVO> config) throws Exception
    {
        String fileContents = convertToXMLString(config);
        FileWriter writer = new FileWriter(configFile);
        writer.write(fileContents);
        writer.close();
    }
    
    /**
     * Convert the object model to an xml string.
     */
    protected String convertToXMLString(Map<String, EnvironmentConfigVO> config) throws Exception
    {
        Document document = JAXPUtil.createDocument();    
        
        Element rootNode = document.createElement(NODE_ROOT);
        document.appendChild(rootNode);
        
        addEnvironments(document,rootNode,config);
        
        return JAXPUtil.toString(document);
    }

    protected void addEnvironments(Document document, Element parentNode, Map<String, EnvironmentConfigVO> config) throws Exception
    {
        Iterator<String> envIterator = config.keySet().iterator();
        while (envIterator.hasNext())
        {
            String name = envIterator.next();
            Element envNode = document.createElement(NODE_ENVIRONMENT);
            envNode.setAttribute(ATTRIBUTE_NAME,name);
            
            EnvironmentConfigVO vo = config.get(name);
            List<ServerConfigVO> servers = vo.getServers();
            
            for (int i=0; i < servers.size(); i++)
            {
                ServerConfigVO serverConfigVO = servers.get(i);
                addServer(document,envNode,serverConfigVO);
            }
            parentNode.appendChild(envNode); 
        }
    }

    protected void addServer(Document document, Element parentNode, ServerConfigVO server) throws Exception
    {
        Element serverNode = document.createElement(NODE_SERVER);
        serverNode.setAttribute(ATTRIBUTE_NAME, server.getName());
        serverNode.setAttribute(ATTRIBUTE_HOST, server.getHostname());
        serverNode.setAttribute(ATTRIBUTE_SERVER_TYPE, server.getServerType());

        List<ApplicationConfigVO> applications =  server.getApplications();

        for (int i = 0; i < applications.size(); i++)
        {
            ApplicationConfigVO applicationConfigVO = applications.get(i);
            addApplication(document, serverNode, applicationConfigVO);
        }
        parentNode.appendChild(serverNode);
    }

    protected void addApplication(Document document, Element parentNode, ApplicationConfigVO application) throws Exception
    {
        Element applicationNode = document.createElement(NODE_APPLICATION);
        Element earNode = JAXPUtil.buildSimpleXmlNode(document, NODE_EAR,application.getEarFileName());
        applicationNode.appendChild(earNode);
        Element deployNode = JAXPUtil.buildSimpleXmlNode(document, NODE_DEPLOY_NAME,application.getDeployName());
        applicationNode.appendChild(deployNode);
        Element containerNode = JAXPUtil.buildSimpleXmlNode(document, NODE_CONTAINER,application.getContainerName());
        applicationNode.appendChild(containerNode);
        if (application.isHasParent())
        {
            Element parentAppNode = JAXPUtil.buildSimpleXmlNode(document, NODE_PARENT_APPLICATION,application.getParentApplication());
            applicationNode.appendChild(parentAppNode);
        }

        parentNode.appendChild(applicationNode);
    }
    
    /**
     * Make a backup copy of the current config file.
     */
    protected void makeBackupCopy() throws Exception
    {
        String backupFilename = configFile.getAbsolutePath() + ".bak";
        File backupFile = new File(backupFilename);
        backupFile.delete();
        configFile.renameTo(backupFile);
    }
    
}
