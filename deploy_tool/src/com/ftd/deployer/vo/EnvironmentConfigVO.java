package com.ftd.deployer.vo;

import java.util.ArrayList;
import java.util.List;

public class EnvironmentConfigVO
{
    protected String name;
    protected List   servers;
    
    public EnvironmentConfigVO()
    {
        servers = new ArrayList();
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public void setServers(List servers)
    {
        this.servers = servers;
    }

    public List getServers()
    {
        return servers;
    }
    public void addServer(ServerConfigVO server)
    {
        servers.add(server);
    }
    
    public String toString()
    {
        return name;
    }

    /**
     * Produce a full clone of the object.  The cloning is recursive
     * to all the child objects.
     * @return
     */
    public EnvironmentConfigVO fullClone()
    {
        EnvironmentConfigVO clone = new EnvironmentConfigVO();
        clone.name = name;
        List serversClone = new ArrayList();
        for (int i=0; i < servers.size(); i++)
        {
            ServerConfigVO serverNonMutable = (ServerConfigVO) servers.get(i);
            ServerConfigVO serverClone = serverNonMutable.fullClone();
            serversClone.add(serverClone);
        }
        clone.setServers(serversClone);
        return clone;
    }
}
