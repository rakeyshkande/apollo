package com.ftd.deployer.vo;

public class ApplicationConfigVO implements Comparable
{
    protected String  earFileName;
    protected String  deployName;
    protected String  containerName;
    protected boolean hasParent;
    protected String  parentApplication;
    
    public ApplicationConfigVO()
    {
        hasParent = false;
    }

    public void setEarFileName(String earFileName)
    {
        this.earFileName = earFileName;
    }

    public String getEarFileName()
    {
        return earFileName;
    }

    public void setDeployName(String deployName)
    {
        this.deployName = deployName;
    }

    public String getDeployName()
    {
        return deployName;
    }

    public void setContainerName(String containerName)
    {
        this.containerName = containerName;
    }

    public String getContainerName()
    {
        return containerName;
    }

    public void setHasParent(boolean hasParent)
    {
        this.hasParent = hasParent;
    }

    public boolean isHasParent()
    {
        return hasParent;
    }

    public void setParentApplication(String parentApplication)
    {
        this.parentApplication = parentApplication;
        if (parentApplication != null && !parentApplication.equals(""))
        {
            hasParent = true;
        }
        else
        {
            hasParent = false;
        }
    }

    public String getParentApplication()
    {
        return parentApplication;
    }
    
    public String toString()
    {
        return deployName;
    }
    
    /**
     * Produce a full clone of the object.  The cloning is recursive
     * to all the child objects.
     * @return
     */
    public ApplicationConfigVO fullClone()
    {
        ApplicationConfigVO clone = new ApplicationConfigVO();
        clone.earFileName = earFileName;
        clone.deployName = deployName;
        clone.containerName = containerName;
        clone.hasParent = hasParent;
        clone.parentApplication = parentApplication;
        
        return clone;
    }

    public int compareTo(Object o)
    {
        if (o instanceof ApplicationConfigVO)
        {
            ApplicationConfigVO vo = (ApplicationConfigVO) o;
            return deployName.compareTo(vo.deployName);
        }
        
        return 1;
    }
}
