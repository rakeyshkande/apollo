package com.ftd.deployer.vo;

import java.util.ArrayList;
import java.util.List;

import com.ftd.deployer.server.ServerTypes;

public class ServerConfigVO
{
    protected String name;
    protected String hostname;
    protected List   applications;
    protected String serverType;
    
    public ServerConfigVO()
    {
        applications = new ArrayList();
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public void setHostname(String hostname)
    {
        this.hostname = hostname;
    }

    public String getHostname()
    {
        return hostname;
    }

    public void setApplications(List applications)
    {
        this.applications = applications;
    }

    public List getApplications()
    {
        return applications;
    }
    public void addApplication(ApplicationConfigVO app)
    {
        applications.add(app);
    }
    
    public String toString()
    {
        return name;
    }
    
    /**
     * Produce a full clone of the object.  The cloning is recursive
     * to all the child objects.
     * @return
     */
    public ServerConfigVO fullClone()
    {
        ServerConfigVO clone = new ServerConfigVO();
        clone.name = name;
        clone.hostname = hostname;
        clone.serverType = serverType;
        
        List applications = new ArrayList();
        for (int i=0; i < this.applications.size(); i++)
        {
            ApplicationConfigVO appNonMutable = (ApplicationConfigVO) this.applications.get(i);
            ApplicationConfigVO appClone = appNonMutable.fullClone();
            applications.add(appClone);
        }
        clone.setApplications(applications);
        
        return clone;
    }

    public String getServerType()
    {
        return serverType;
    }

    public void setServerType(String serverType)
    {
        this.serverType = serverType;
    }

    
    
    public void updateContainerNamesByServerType()
    {
        List applications = new ArrayList();
        for (int i=0; i < this.applications.size(); i++)
        {
            ApplicationConfigVO appVO = (ApplicationConfigVO) this.applications.get(i);
            
            String containerName = appVO.getContainerName();
            containerName = ServerTypes.updateContainerName(containerName, serverType);
            appVO.setContainerName(containerName);
 
        }        
        
    }
}
