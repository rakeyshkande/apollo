package com.ftd.deployer.util;

import com.jcraft.jsch.UserInfo;

/**
 * Class containing information on an SSH user.
 */
public class SSHUserInfo implements UserInfo
{
    String name;
    String password;
    
    boolean bPrompt = true;
    
    private ProgressLogger logger;

    
    public SSHUserInfo(ProgressLogger logger, String name, String password)
    {
        this.logger = logger;
        this.name = name;
        this.password = password;
    }

    public String getPassphrase()
    {
        return null;
    }

    public boolean promptPassphrase(String arg0)
    {
        return bPrompt;
    }

    public String getPassword()
    {
        return password;
    }
    
    public boolean promptPassword(String arg0)
    {
        return bPrompt;
    }

    public boolean promptYesNo(String arg0)
    {
        return true;
    }

    public void showMessage(String message)
    {
        logger.logProgress(message);
    }

}
