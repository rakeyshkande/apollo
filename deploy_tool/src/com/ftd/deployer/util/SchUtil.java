package com.ftd.deployer.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelShell;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

/**
 * Provides Utility methods wrapping the JSch Library for performing SSH and SCP commands.
 * References the Ant SCP task Source Code and the JSch examples
 */
public class SchUtil
{
    private static Logger logger  = Logger.getLogger(SchUtil.class);
    
    private ProgressLogger progressLogger;
    
    Session session = null;
    
    String sessionKey;
    
    Thread commandThread = null;
    
    static final int RETRY_INTERVAL = 500;
    static final int MAX_WAIT = 1000 * 60 * 10; // 10 minutes
    static final int CMD_PAUSE_INTERVAL = 100; 
    
    static final String COMMAND_PROMPT = "sshexeccmd:";
    /**
     * Initiates an Sch Session
     * @param logger
     */
    public SchUtil(ProgressLogger progressLogger, String hostName, String userName, String password) throws Exception
    {
        // this.progressLogger = progressLogger;
        // Don't send to the status progress that is passed in, that messes
        // with status (completion/etc)
        this.progressLogger = new SystemOutProgressLogger(logger);
        
        startSession(hostName, userName, password, 22);
    }
    
    /**
     * Starts the session with the given host, user, password and port
     * @param hostName
     * @param userName
     * @param password
     * @param port
     * @throws Exception
     */
    private void startSession(String hostName, String userName, String password, int port) throws Exception
    {
        JSch jsch = new JSch();
        session = jsch.getSession(userName, hostName, port);
        
        sessionKey = userName + "@" + hostName + ":" + port;
        progressLogger.logProgress("Connecting to: " + sessionKey);

        SSHUserInfo ui = new SSHUserInfo(progressLogger, userName, password);
        session.setUserInfo(ui);
        session.connect();
        session.setTimeout(0); // Wait forever
        
        progressLogger.logProgress("Session Connected: " + userName + "@" + hostName + ":" + port);
    }
    
    /**
     * Ends the session
     */
    public void endSession()
    {
        if(session != null)
        {
            progressLogger.logProgress("Closing Session: " + sessionKey);
        }
        disconnect(session);
        session = null;
    }
    
    /**
     * Sends the Local File to the Remote File with the passed in SSH port
     * @param localFilePath
     * @param remoteFilePath
     * @return <code>true</code> on success, else <code>false</code>
     * @throws Exception
     */
    public boolean sendFileSCP(String localFilePath, String remoteFilePath) throws Exception
    {
        FileInputStream fis = null;
        Channel channel = null;        
        OutputStream out = null;
        InputStream in = null;
        
        logger.info(sessionKey + ": Sending File via SCP: " + localFilePath + "->" + remoteFilePath);
        
        try
        {
            // exec 'scp -t remoteFilePath' remotely
            String command = "scp -p -t " + remoteFilePath;
            channel = session.openChannel("exec");
            ((ChannelExec) channel).setCommand(command);

            // get I/O streams for remote scp
            out = channel.getOutputStream();
            in = channel.getInputStream();

            channel.connect();

            if (checkAck(in) != 0)
            {
                return false;
            }

            // send "C0644 filesize filename", where filename should not include '/'
            long filesize = (new File(localFilePath)).length();
            command = "C0644 " + filesize + " ";
            if (localFilePath.lastIndexOf('/') > 0)
            {
                command += localFilePath.substring(localFilePath.lastIndexOf('/') + 1);
            }
            else
            {
                command += localFilePath;
            }
            command += "\n";
            out.write(command.getBytes());
            out.flush();
            if (checkAck(in) != 0)
            {
                return false;
            }

            // send a content of localFilePath
            fis = new FileInputStream(localFilePath);
            
            IOUtils.copy(fis, out);
            
            byte[] buf = new byte[1024];
 
            fis.close();
            fis = null;
            // send '\0'
            buf[0] = 0;
            out.write(buf, 0, 1);
            out.flush();
            if (checkAck(in) != 0)
            {
                return false;
            }

            progressLogger.logProgress("File Transfer Completed: " + localFilePath + " to: " + remoteFilePath);
            return true;

        }
        finally
        {
            IOUtils.closeQuietly(in);
            IOUtils.closeQuietly(out);
            
            disconnect(channel);
        }
    }

    /**
     * Disconnects the channel with null and exception handling
     * @param channel
     */
    private static void disconnect(Channel channel)
    {
        if(channel == null)
        {
            return;
        }
        
        try
        {
            channel.disconnect();
        } catch (Throwable t)
        {
            logger.warn(t);
        }
    }
    
    /**
     * Disconnects the session with null and exception handling
     * @param session
     */
    private static void disconnect(Session session)
    {
        if(session == null)
        {
            return;
        }
        
        try
        {
            session.disconnect();
        } catch (Throwable t)
        {
            logger.warn(t);
        }
    }    
    
    /**
     * Check Ack Status, references from ScpTo.java sample
     * 
     * @param in
     * @return
     * @throws Exception
     */
    private int checkAck(InputStream in) throws Exception
    {
        int b = in.read();
        // b may be 0 for success,
        // 1 for error,
        // 2 for fatal error,
        // -1
        if (b == 0)
            return b;
        if (b == -1)
            return b;

        if (b == 1 || b == 2)
        {
            StringBuffer sb = new StringBuffer();
            int c;
            do
            {
                c = in.read();
                sb.append((char) c);
            } while (c != '\n');
            if (b == 1)
            { // error
                progressLogger.logProgress("SSH/SCP Ack Error: " +  sb.toString());
            }
            if (b == 2)
            { // fatal error
                progressLogger.logProgress("SSH/SCP Ack Fatal Error: " +  sb.toString());
            }
        }
        return b;
    }

    /**
     * Executes the passed in command and logs all output to sb
     * @param command
     * @param sb
     * @return
     */
    public boolean executeCommand(String command, StringBuffer output) throws Exception
    {
        return executeCommand(command, output, false);
    }
    
//    public boolean executeCommand(String command, StringBuffer output, boolean silentMode) throws Exception
//    {
//        Channel theChannel = null;
//        ByteArrayOutputStream bos = new ByteArrayOutputStream();
//        
//        ProgressOutputStream out = new ProgressOutputStream(progressLogger, bos, output, silentMode);
//        
//        try
//        {
//            final ChannelExec channel = (ChannelExec) session.openChannel("exec");
//            theChannel = channel;
//            channel.setCommand(command);
//            channel.setOutputStream(out);
//            channel.setExtOutputStream(out);
//            channel.setErrStream(out);
//            channel.connect();
//            
//            // wait for it to finish
//            commandThread =
//                new Thread() {
//                    public void run() {
//                        while (!channel.isClosed()) {
//                            if (commandThread == null) {
//                                return;
//                            }
//                            try {
//                                sleep(RETRY_INTERVAL);
//                            } catch (Exception e) {
//                                // ignored
//                            }
//                        }
//                    }
//                };         
//                
//            commandThread.start();
//            
//            try
//            {
//                commandThread.join(MAX_WAIT); 
//            } catch (Exception e)
//            {
//            }
//            
//            boolean bRetVal = false;
//            if (commandThread.isAlive()) {
//                commandThread = null; // Force the command thread to exit
//                progressLogger.logProgress("Command Execution Timed out");
//            } else
//            {
//                // this is the wrong test if the remote OS is OpenVMS,
//                // but there doesn't seem to be a way to detect it.
//                int ec = channel.getExitStatus();
//                if (ec != 0) {
//                    progressLogger.logProgress("Remote command failed with exit status " + ec);
//                } else
//                {
//                    bRetVal = true;
//                }
//            }
//
//            if(bRetVal)
//            {
//                if(!silentMode)
//                {
//                    progressLogger.logProgress("Command Completed Succesfully");
//                }
//            }
//            
//            out.logRemainingProgress();
//            
//            return bRetVal;
//        }
//        finally
//        {
//            disconnect(theChannel);
//        }
//    }
    
    /**
     * Opens a shell to the target session and executes the passed in command and logs all output to output String Buffer
     * @param command
     * @param sb
     * @return
     */    
    public boolean executeCommand(String command, StringBuffer output, boolean silentMode) throws Exception
    {
        Channel theChannel = null;
        
        final SchResponseStreamProcessor responseProcessor = new SchResponseStreamProcessor(progressLogger, output, silentMode);

        logger.info(sessionKey + ": Sending SSH Command: " + command);
        try
        {
            final ChannelShell channel = (ChannelShell) session.openChannel("shell");
            theChannel = channel;
            
            PipedOutputStream commandWriter = new PipedOutputStream();
            PipedInputStream commandInputStream = new PipedInputStream(commandWriter);
            channel.setInputStream(commandInputStream);
            channel.setOutputStream(responseProcessor);
            channel.setExtOutputStream(responseProcessor);
            channel.setPty(true); // Don't echo
            channel.setPtyType("xtermc");
            channel.setPtySize(200, 200, 200, 200); // Allow commands up to 200 col's and responses up to 200 rows
            channel.connect();
            
            // wait for it to finish
            commandThread =
                new Thread() {
                    public void run() {
                        while (!channel.isClosed() && !channel.isEOF() && !responseProcessor.isExitFilterFound()) {
                            if (commandThread == null) {
                                return;
                            }
                            
                            sleepQuietly(RETRY_INTERVAL);
                        }
                    }
                };         
                
            responseProcessor.setStartCommandFilter(COMMAND_PROMPT + command); // The line that initiates the command
            responseProcessor.setEndCommandFilter(COMMAND_PROMPT + "exit"); // The exit command
            
            // This is because in some versions of solaris, exit does not seems to terminate the channel
            // So we have to watch for the text after exit is issued
            responseProcessor.addChannelExitFilter("logout");
                
            commandThread.start();
            
            // Set up a predictable prompt so we can parse the command and its output
            commandWriter.write( ("PS1=" + COMMAND_PROMPT + "\n").getBytes()); // bash and kshell       
            commandWriter.write( ("set prompt=" + COMMAND_PROMPT + "\n").getBytes()); // for c-shell
            
            sleepQuietly(CMD_PAUSE_INTERVAL);
            
            // Send the Command
            commandWriter.write(command.getBytes());
            commandWriter.write("\n".getBytes());
            
            sleepQuietly(CMD_PAUSE_INTERVAL);
            
            // Send the Exit Command
            commandWriter.write("exit\n".getBytes());
            
            sleepQuietly(CMD_PAUSE_INTERVAL);
            
            try
            {
                commandThread.join(MAX_WAIT); 
            } catch (Exception e)
            {
            }
            
            boolean bRetVal = false;
            if (commandThread.isAlive()) {
                commandThread = null; // Force the command thread to exit
                progressLogger.logProgress("Command Execution Timed out");
            } else
            {
                // this is the wrong test if the remote OS is OpenVMS,
                // but there doesn't seem to be a way to detect it.
                int ec = channel.getExitStatus();
                if (ec != 0) {
                    progressLogger.logProgress("Remote command failed with exit status " + ec);
                } else
                {
                    bRetVal = true;
                }
            }

            if(bRetVal)
            {
                if(!silentMode)
                {
                    progressLogger.logProgress("Command Completed Succesfully");
                }
            }
            
            responseProcessor.logRemainingProgress();
            
            return bRetVal;
        }
        finally
        {
            disconnect(theChannel);
        }
    }    
    
    
    public static void sleepQuietly(long sleepTime)
    {
        try
        {
            Thread.sleep(sleepTime);
        } catch (Exception e)
        {
        }
    }
    
    /**
     * Sample of using the SchUtil
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception
    {
        //SchUtil util = new SchUtil(new SystemOutProgressLogger(), "brass1.ftdi.com", "oracle", "lli7tst");
        SchUtil util = new SchUtil(new SystemOutProgressLogger(), "copper1.ftdi.com", "oracle", "lli7tst");
        StringBuffer output = new StringBuffer();
        
        //util.executeCommand("env", output);
        // util.executeCommand("echo $PS1", output);
        System.out.println(util.executeCommand("/u01/app/jboss-5.1.0.GA/start_arvtest.sh", output));
 
        util.endSession();
        System.out.println("Output: ");
        System.out.println(output.toString());
        
    }
}
