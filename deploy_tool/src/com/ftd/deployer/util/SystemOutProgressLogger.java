package com.ftd.deployer.util;

import org.apache.log4j.Logger;

public class SystemOutProgressLogger implements ProgressLogger
{
    private Logger logger;
    
    public SystemOutProgressLogger()
    {
    }
    
    public SystemOutProgressLogger(Logger logger)
    {
        this.logger = logger;
    }

    public void logProgress(String message)
    {
        System.out.println(message);
        
        if(logger != null)
        {
            logger.info(message);
        }

    }

    public void logProgress(String message, Throwable t)
    {
        System.out.println(message);
        t.printStackTrace();
        
        if(logger != null)
        {
            logger.warn(message, t);
        }        
    }

}
