package com.ftd.deployer.util;

/**
 * Provides an interface for logging progress
 */
public interface ProgressLogger
{
    /**
     * Logs the specific progress message
     * @param message
     */
    public void logProgress(String message);
    
    
    /**
     * Logs the specific progress message and the Throwable call stack
     * @param message
     * @param t
     */
    public void logProgress(String message, Throwable t);
}
