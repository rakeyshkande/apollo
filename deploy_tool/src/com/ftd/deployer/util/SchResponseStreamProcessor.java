package com.ftd.deployer.util;

import java.io.ByteArrayOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

/**
 * Processes the Responses from the Sch Command
 * Only captures output that exist between the startCommandFilter
 * and the End Command Filter
 */
public class SchResponseStreamProcessor extends FilterOutputStream
{
    
    private static Logger logger  = Logger.getLogger(SchResponseStreamProcessor.class);    

    private ProgressLogger progressLogger;
    
    private StringBuffer currentProgressMessage = new StringBuffer();
    private StringBuffer allOutput;
    private boolean silentMode;
        
    private String startCommandFilter = "";
    private String endCommandFilter = "";
    
    private boolean startCommandFilterFound = false;
    private boolean completedCommandFilterFound = false;
    private boolean exitFilterFound = false;
    private Set<String> channelExitFilters = new HashSet<String>();
    
    /**
     * @param out
     * @param progressLogger
     */
    public SchResponseStreamProcessor(ProgressLogger progressLogger, StringBuffer allOutput, boolean silentMode)
    {
        super(new ByteArrayOutputStream());
        this.progressLogger = progressLogger;
        this.allOutput = allOutput;
        this.silentMode = silentMode;
    }
    
    public void setStartCommandFilter(String val)
    {
        startCommandFilter = val;
    }
    
    public void setEndCommandFilter(String val)
    {
        endCommandFilter = val;
    }    
    

    public void write(int b) throws IOException
    {
        // Don't bother upstream
        
        // Log the Progress/Capture the Output
        char nextChar = (char) b;
        
        if(nextChar == '\n' || nextChar == '\r')
        {
            logProgressMessage();
        } else
        {
            currentProgressMessage.append(nextChar);
        }
    }
    
    
    public void logRemainingProgress()
    {
        logProgressMessage();
    }
    
    public void addChannelExitFilter(String filter)
    {
        channelExitFilters.add(filter);
    }
    
    public boolean isExitFilterFound()
    {
        return exitFilterFound;
    }
    
    private void logProgressMessage()
    {
        if(currentProgressMessage.length() > 0)
        {
            String nextOutput = currentProgressMessage.toString();
            currentProgressMessage = new StringBuffer();
            
            logger.debug(nextOutput);
            
            if(startCommandFilter.equals(nextOutput))
            {
                startCommandFilterFound = true;
                
                if(!silentMode)
                {
                    progressLogger.logProgress(nextOutput);
                }                
                
                return;
            } else if(endCommandFilter.equals(nextOutput))
            {
                completedCommandFilterFound = true;
            } else if(completedCommandFilterFound && channelExitFilters.contains(nextOutput))
            {
                exitFilterFound = true; // Mark that we found the filter after the exit command
            }
            
            // Skip if not started, or after completed
            if(!startCommandFilterFound || completedCommandFilterFound)
            {
                return;
            }
            
            if(!silentMode)
            {
                progressLogger.logProgress(nextOutput);
            }

            if(allOutput.length() > 0)
            {
                allOutput.append("\n");
            }
            allOutput.append(nextOutput);

        }        
        
    }




}
