package com.ftd.deployer.util;

import java.awt.Component;
import java.awt.Frame;

public class WindowUtils
{
    /**
     * Returns the parent Frame of a component or null if there is no parent
     * @param component
     * @return
     */
    public static Frame getParentFrame(Component component)
    {
        Component frame = component;
        while ((frame != null) && !(frame instanceof Frame))
        {
            frame = frame.getParent();
        }
        
        return (Frame) frame;
    }

}
