echo off

set classpath=dist/deploy-tool.jar
set classpath=%classpath%;lib/adminclient.jar
set classpath=%classpath%;lib/jmxcluster.jar
set classpath=%classpath%;lib/jmxri.jar
set classpath=%classpath%;lib/jmx_remote_api.jar
set classpath=%classpath%;lib/oc4j-internal.jar
set classpath=%classpath%;lib/optic.jar
set classpath=%classpath%;lib/jsch-0.1.54.jar
set classpath=%classpath%;lib/commons-io-2.0.1.jar
set classpath=%classpath%;../lib/ftdutilities.jar
set classpath=%classpath%;../lib/utilities.jar
set classpath=%classpath%;../lib/log4j-1.2.5.jar
set classpath=%classpath%;../lib/ejb.jar
set classpath=%classpath%;../lib/javax77.jar
set classpath=%classpath%;../lib/javax88.jar
set classpath=%classpath%;../lib/commons-lang-2.2.jar
set classpath=%classpath%;../lib/xmlparserv2.jar


java -Xmx512m com.ftd.deployer.client.Deployer
