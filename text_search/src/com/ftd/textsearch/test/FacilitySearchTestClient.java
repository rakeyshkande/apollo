package com.ftd.textsearch.test;

import com.ftd.textsearch.client.FacilitySearchClient;

public class FacilitySearchTestClient extends FacilitySearchClient
{
    public FacilitySearchTestClient()
    {
    }


    protected String getGlobalParm(String parm)
    {
        String param_value = null;

        if (parm.equals("SOURCE_CODE_SEARCH_URL"))
        {
            param_value = "http://zinc2:7778/textsearchsourcecode/";
        } else if (parm.equals("PRODUCT_SEARCH_URL"))
        {
            param_value = "http://zinc2:7778/textsearchproduct/";
        } else if (parm.equals("FACILITY_SEARCH_URL"))
        {
            param_value = "http://zinc2:7778/textsearchfacility/";
        } else if (parm.equals("IOTW_SEARCH_URL"))
        {
            param_value = "http://zinc2:7778/textsearchiotw/";
        } else if (parm.equals("PRODUCT_SEARCH_FUZZY"))
        {
            param_value = "0.5";
        }

        return param_value;
    }

}
