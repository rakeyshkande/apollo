package com.ftd.textsearch.test;

import com.ftd.textsearch.client.SourceCodeSearchClient;

public class SourceCodeSearchTestClient extends SourceCodeSearchClient
{
    public SourceCodeSearchTestClient()
    {
    }


    protected String getGlobalParm(String parm)
    {
        String param_value = null;

        if (parm.equals("SOURCE_CODE_SEARCH_URL"))
        {
            param_value = "http://iron3:7778/textsearchsourcecode/";
        }
        else if (parm.equals("SOURCE_CODE_SEARCH_FUZZY"))
        {
            param_value = "0.3";
        }

        return param_value;
    }


}
