package com.ftd.textsearch.test;

import com.ftd.textsearch.client.IOTWSearchClient;

public class IOTWSearchTestClient extends IOTWSearchClient
{
    public IOTWSearchTestClient()
    {
    }


    protected String getGlobalParm(String parm)
    {
        String param_value = null;

        if (parm.equals("SOURCE_CODE_SEARCH_URL"))
        {
            param_value = "http://zinc2:7778/textsearchsourcecode/";
        } else if (parm.equals("PRODUCT_SEARCH_URL"))
        {
            param_value = "http://zinc2:7778/textsearchproduct/";
        } else if (parm.equals("FACILITY_SEARCH_URL"))
        {
            param_value = "http://zinc2:7778/textsearchfacility/";
        } else if (parm.equals("IOTW_SEARCH_URL"))
        {
            param_value = "http://zinc3:7778/textsearchiotw/";
        } else if (parm.endsWith("FUZZY_SEARCH"))
        {
            param_value = "0.5";
        }

        return param_value;
    }

    /**
     * Override to get the version of PAS that won't use GLOBALPARMS.
     * @return
     */
    /*protected PASRequestService getPAS() {
        PASRequestService pas = 
            PASRequestServiceFactory.createPASRequestService();
        ((PASRequestServiceRemoteImpl)pas).setServerAddress("http://zinc2:7778/pasquery/services/ProductAvailability");

        return pas;
    }*/

}
