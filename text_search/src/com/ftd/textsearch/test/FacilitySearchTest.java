package com.ftd.textsearch.test;

import com.ftd.textsearch.client.FacilitySearchClient;
import com.ftd.textsearch.client.ProductSearchClient;
import com.ftd.textsearch.client.SourceCodeSearchClient;
import com.ftd.textsearch.vo.FacilitySearchVO;
import com.ftd.textsearch.vo.ProductSearchVO;
import com.ftd.textsearch.vo.SourceCodeSearchVO;

import java.util.Hashtable;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;

public class FacilitySearchTest
{
    public FacilitySearchTest()
    {
    }

    public static void main(String[] args)
    {
        try
        {
            //System.setProperty(Context.INITIAL_CONTEXT_FACTORY, "com.evermind.server.rmi.RMIInitialContextFactory");
            //System.setProperty(Context.SECURITY_PRINCIPAL, "oc4jadmin");
            //System.setProperty(Context.SECURITY_CREDENTIALS, "welcome");
            //System.setProperty(Context.PROVIDER_URL, "opmn:ormi://zinc1:6003:OC4J_ORDER_PROCESSING/orderprocessing");

            FacilitySearchClient client = new FacilitySearchTestClient();

            List<FacilitySearchVO> voList = client.searchFacilities("HOME", 5, "", "IL", "N");

            System.out.println("Search Returned " + voList.size() + " results");
            for (int i = 0; i < voList.size(); i++)
            {
                FacilitySearchVO vo = voList.get(i);
                System.out.println("  Business Id : " + vo.getBusinessId());
                System.out.println("  Business Nm : " + vo.getBusinessName());
                System.out.println("  Business Ty : " + vo.getBusinessType());
                System.out.println("  Address     : " + vo.getAddress());
                System.out.println("  City        : " + vo.getCity());
                System.out.println("  State       : " + vo.getState());
                System.out.println("  Zip Code    : " + vo.getZipCode());
                System.out.println("  Phone #     : " + vo.getPhoneNumber());
                System.out.println("  Score       : " + vo.getRelevancy());
                System.out.println(" ");

            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }

    }
}
