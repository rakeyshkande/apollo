package com.ftd.textsearch.test;

import com.ftd.textsearch.vo.ProductSearchVO;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import junit.framework.TestCase;

public class ProductSearchClientTest extends TestCase
{
    protected Date tomorrow;
    protected Date today;
//    protected String searchTerm = "tulip";
//    protected String searchTerm = "MKZ";
    protected String searchTerm = "happiness bouquet";
    

    public ProductSearchClientTest(String sTestName)
    {
        super(sTestName);
    }

    protected void setUp() throws Exception
    {
        super.setUp();

        Calendar deliveryDateCalendar = Calendar.getInstance();
        today = deliveryDateCalendar.getTime();
        deliveryDateCalendar.add(Calendar.DAY_OF_YEAR,1);
        tomorrow = deliveryDateCalendar.getTime();
    }

    /**
     * @see ProductSearchTestClient#searchProducts(String,int,java.util.Date,String,String,String,boolean)
     */
    public void testSearchProductsNoAvail()
    {
        try
        {
            ProductSearchTestClient client = new ProductSearchTestClient();

            List<ProductSearchVO> voList = client.searchProducts(searchTerm, 30, null, null, "", "FTD", false, false, "ftd", null, null);

            assertEquals(true, voList.size() > 0);
            System.out.println("Search Returned " + voList.size() + " results");
            for (int i = 0; i < voList.size(); i++)
            {
                ProductSearchVO vo = voList.get(i);
                System.out.println("  Results : " + vo.getProductId() + " " + vo.getRelevancy() + " " + vo.getProductName() + " " + vo.getPopularity());

            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }


    /**
     * @see ProductSearchTestClient#searchProducts(String,int,java.util.Date,String,String,String,boolean)
     */
    public void testSearchProductsToday()
    {
        try
        {
            ProductSearchTestClient client = new ProductSearchTestClient();

            List<ProductSearchVO> voList = client.searchProducts(searchTerm, 30, today, "60148", "US", "FTD", true, false, "ftd", null, null);

            assertEquals(true, voList.size() > 0);
            System.out.println("Search Returned " + voList.size() + " results");
            for (int i = 0; i < voList.size(); i++)
            {
                ProductSearchVO vo = voList.get(i);
                System.out.println("  Results : " + vo.getProductId() + " " + vo.getRelevancy() + " " + vo.getProductName() + " " + vo.getPopularity());

            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    /**
     * @see ProductSearchTestClient#searchProducts(String,int,java.util.Date,String,String,String,boolean)
     */
    public void testSearchProductsTomorrow()
    {
        try
        {
            ProductSearchTestClient client = new ProductSearchTestClient();

            List<ProductSearchVO> voList = client.searchProducts(searchTerm, 30, tomorrow, "60148", "US", "FTD", true, false, "ftd", null, null);

            assertEquals(true, voList.size() > 0);
            System.out.println("Search Returned " + voList.size() + " results");
            for (int i = 0; i < voList.size(); i++)
            {
                ProductSearchVO vo = voList.get(i);
                System.out.println("  Results : " + vo.getProductId() + " " + vo.getRelevancy() + " " + vo.getProductName() + " " + vo.getPopularity());

            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    public void testSearchProductsInternational()
    {
        try
        {
            ProductSearchTestClient client = new ProductSearchTestClient();

            List<ProductSearchVO> voList = client.searchProducts(null, 30, tomorrow, "", "AR", "FTD", false, false, "AR", "ftd", null);
            assertEquals(true, voList.size() > 0);

            System.out.println("Search Returned " + voList.size() + " results");
            for (int i = 0; i < voList.size(); i++)
            {
                ProductSearchVO vo = voList.get(i);
                System.out.println("  Results : " + vo.getProductId() + " " + vo.getRelevancy() + " " + vo.getProductName() + " " + vo.getPopularity());

            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

}
