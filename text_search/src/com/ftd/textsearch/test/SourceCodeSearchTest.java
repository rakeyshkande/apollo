package com.ftd.textsearch.test;

import com.ftd.textsearch.client.SourceCodeSearchClient;

import com.ftd.textsearch.vo.SourceCodeSearchVO;

import java.util.Hashtable;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;

import junit.framework.TestCase;

public class SourceCodeSearchTest extends TestCase
{

    public SourceCodeSearchTest(String sTestName)
    {
        super(sTestName);
    }

    protected void setUp() throws Exception
    {
        super.setUp();
    }
    
    public void testSearchSourceCodes()
    {
        try
        {
            SourceCodeSearchTestClient client = new SourceCodeSearchTestClient();
            
            List<SourceCodeSearchVO> voList = client.searchSourceCodes("Advantage","FTD");
            
            System.out.println("Search Returned " + voList.size() + " results");
            for (int i=0; i < voList.size(); i++)
            {
                SourceCodeSearchVO vo = voList.get(i);
                System.out.println("  Source Code: " + vo.getSourceCode());
                System.out.println("  Description: " + vo.getDescription());
                System.out.println("  Source Type: " + vo.getSourceType());
                System.out.println("  Company Id : " + vo.getCompanyId());
                System.out.println("  Score      : " + vo.getRelevancy());
                
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        
    }
}
