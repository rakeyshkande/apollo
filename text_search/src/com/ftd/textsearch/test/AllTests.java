package com.ftd.textsearch.test;

import junit.framework.Test;
import junit.framework.TestSuite;
import com.clarkware.junitperf.*;


public class AllTests
{
    private static    int maxElapsedMilliseconds = 1000000;
    private static    int maxUsers = 1;
    
    public static Test suite()
    {
        TestSuite suite;
        suite = new TestSuite("AllTests");
        
        buildMethodTestCase(new ProductSearchClientTest("testSearchProductsToday"), suite);
        buildMethodTestCase(new ProductSearchClientTest("testSearchProductsTomorrow"), suite);
        buildMethodTestCase(new ProductSearchClientTest("testSearchProductsInternational"), suite);
        buildMethodTestCase(new SourceCodeSearchTest("testSearchSourceCodes"), suite);

        return suite;
    }
    
    private static void buildMethodTestCase(Test testCase, TestSuite suite)
    {
        Test loadTest = new LoadTest(testCase, maxUsers);
        Test timedTest = new TimedTest(loadTest, maxElapsedMilliseconds);
        suite.addTest(timedTest);
        
    }
}
