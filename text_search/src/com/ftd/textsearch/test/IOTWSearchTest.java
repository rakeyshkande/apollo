package com.ftd.textsearch.test;

import com.ftd.textsearch.client.IOTWSearchClient;
import com.ftd.textsearch.client.ProductSearchClient;
import com.ftd.textsearch.client.SourceCodeSearchClient;
import com.ftd.textsearch.vo.IOTWSearchVO;
import com.ftd.textsearch.vo.ProductSearchVO;
import com.ftd.textsearch.vo.SourceCodeSearchVO;

import java.util.Hashtable;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;

public class IOTWSearchTest
{
    public IOTWSearchTest()
    {
    }

    public static void main(String[] args)
    {
        try
        {
            IOTWSearchTestClient client = new IOTWSearchTestClient();

            List<IOTWSearchVO> voList = client.searchIOTW("", "FTD", "", "", "", "");

            System.out.println("Search Returned " + voList.size() + " results");
            for (int i = 0; i < voList.size(); i++)
            {
                IOTWSearchVO vo = voList.get(i);
                System.out.println("  Product Id : " + vo.getProductId());
                System.out.println("  Product Nm : " + vo.getProductName());
                System.out.println("  Long Desc  : " + vo.getProductLongDescription());
                System.out.println("  Std Price  : " + vo.getStandardPrice());
                System.out.println("  Src Code   : " + vo.getSourceCode());
                System.out.println("  Src Code   : " + vo.getSourceCodeDescription());
                System.out.println("  Src Code   : " + vo.getSourceType());
                System.out.println("  Src PHI    : " + vo.getSourceCodePriceHeaderId());
                System.out.println("  IOTW Src C : " + vo.getIotwSourceCode());
                System.out.println("  IOTW Src C : " + vo.getIotwSourceCodeDescription());
                System.out.println("  IOTW Src C : " + vo.getIotwSourceType());
                System.out.println("  IOTW PHI   : " + vo.getIotwSourceCodePriceHeaderId());
                System.out.println("  Score      : " + vo.getRelevancy());
                System.out.println(" ");

            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }

    }
}
