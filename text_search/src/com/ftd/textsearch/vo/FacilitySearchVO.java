package com.ftd.textsearch.vo;

public class FacilitySearchVO extends TextSearchVO
{
    private String businessId;
    private String businessType;
    private String businessName;
    private String address;
    private String city;
    private String state;
    private String zipCode;
    private String phoneNumber;
    
    public FacilitySearchVO()
    {
    }

    public void setBusinessId(String param)
    {
        this.businessId = param;
    }

    public String getBusinessId()
    {
        return businessId;
    }

    public void setBusinessType(String param)
    {
        this.businessType = param;
    }

    public String getBusinessType()
    {
        return businessType;
    }

    public void setBusinessName(String param)
    {
        this.businessName = param;
    }

    public String getBusinessName()
    {
        return businessName;
    }

    public void setAddress(String param)
    {
        this.address = param;
    }

    public String getAddress()
    {
        return address;
    }

    public void setCity(String param)
    {
        this.city = param;
    }

    public String getCity()
    {
        return city;
    }

    public void setState(String param)
    {
        this.state = param;
    }

    public String getState()
    {
        return state;
    }

    public void setZipCode(String param)
    {
        this.zipCode = param;
    }

    public String getZipCode()
    {
        return zipCode;
    }

    public void setPhoneNumber(String param)
    {
        this.phoneNumber = param;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }
}
