package com.ftd.textsearch.vo;

public class ProductSearchVO extends TextSearchVO
{
    private String productId;
    private String novatorId;
    private String productName;
    private String novatorName;
    private String standardPrice;
    private String deluxePrice;
    private String premiumPrice;
    private String shortDescription;
    private String longDescription;
    private String ShipMethodCarrier;
    private String shipMethodFlorist;
    private String companyId;
    private String discountAllowedFlag;
    private String popularity;
    private String over21Flag;
    private String sourceCodes;
    private String allowFreeShippingFlag;
    
    public ProductSearchVO()
    {
    }

    public void setProductId(String param)
    {
        this.productId = param;
    }

    public String getProductId()
    {
        return productId;
    }

    public void setNovatorId(String param)
    {
        this.novatorId = param;
    }

    public String getNovatorId()
    {
        return novatorId;
    }

    public void setProductName(String param)
    {
        this.productName = param;
    }

    public String getProductName()
    {
        return productName;
    }

    public void setStandardPrice(String param)
    {
        this.standardPrice = param;
    }

    public String getStandardPrice()
    {
        return standardPrice;
    }

    public void setDeluxePrice(String param)
    {
        this.deluxePrice = param;
    }

    public String getDeluxePrice()
    {
        return deluxePrice;
    }

    public void setPremiumPrice(String param)
    {
        this.premiumPrice = param;
    }

    public String getPremiumPrice()
    {
        return premiumPrice;
    }

    public void setShortDescription(String param)
    {
        this.shortDescription = param;
    }

    public String getShortDescription()
    {
        return shortDescription;
    }

    public void setLongDescription(String param)
    {
        this.longDescription = param;
    }

    public String getLongDescription()
    {
        return longDescription;
    }

    public void setShipMethodCarrier(String shipMethodCarrier)
    {
        this.ShipMethodCarrier = shipMethodCarrier;
    }

    public String getShipMethodCarrier()
    {
        return ShipMethodCarrier;
    }

    public void setShipMethodFlorist(String param)
    {
        this.shipMethodFlorist = param;
    }

    public String getShipMethodFlorist()
    {
        return shipMethodFlorist;
    }

    public void setCompanyId(String param)
    {
        this.companyId = param;
    }

    public String getCompanyId()
    {
        return companyId;
    }

    public void setDiscountAllowedFlag(String param)
    {
        this.discountAllowedFlag = param;
    }

    public String getDiscountAllowedFlag()
    {
        return discountAllowedFlag;
    }

    public void setNovatorName(String param)
    {
        this.novatorName = param;
    }

    public String getNovatorName()
    {
        return novatorName;
    }

    public void setPopularity(String param)
    {
        this.popularity = param;
    }

    public String getPopularity()
    {
        return popularity;
    }

    public void setOver21Flag(String param)
    {
        this.over21Flag = param;
    }

    public String getOver21Flag()
    {
        return over21Flag;
    }

    public void setSourceCodes(String param)
    {
        this.sourceCodes = param;
    }

    public String getSourceCodes()
    {
        return sourceCodes;
    }


    public void setAllowFreeShippingFlag(String allowFreeShippingFlag) {
        this.allowFreeShippingFlag = allowFreeShippingFlag;
    }

    public String getAllowFreeShippingFlag() {
        return allowFreeShippingFlag;
    }
}
