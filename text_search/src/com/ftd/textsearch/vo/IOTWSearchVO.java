package com.ftd.textsearch.vo;

public class IOTWSearchVO extends TextSearchVO
{
    private String productId;
    private String productName;
    private String productLongDescription;
    private String standardPrice;
    private String sourceCode;
    private String sourceCodeDescription;
    private String sourceType;
    private String sourceCodePriceHeaderId;
    private String iotwSourceCode;
    private String iotwSourceCodeDescription;
    private String iotwSourceType;
    private String iotwSourceCodePriceHeaderId;
    private String companyId;
    private String productPageMessage;


    public void setSourceCode(String param)
    {
        this.sourceCode = param;
    }

    public String getSourceCode()
    {
        return sourceCode;
    }

    public void setSourceType(String param)
    {
        this.sourceType = param;
    }

    public String getSourceType()
    {
        return sourceType;
    }

    public void setCompanyId(String param)
    {
        this.companyId = param;
    }

    public String getCompanyId()
    {
        return companyId;
    }

    public void setProductId(String param)
    {
        this.productId = param;
    }

    public String getProductId()
    {
        return productId;
    }

    public void setProductName(String param)
    {
        this.productName = param;
    }

    public String getProductName()
    {
        return productName;
    }

    public void setProductLongDescription(String param)
    {
        this.productLongDescription = param;
    }

    public String getProductLongDescription()
    {
        return productLongDescription;
    }

    public void setSourceCodeDescription(String param)
    {
        this.sourceCodeDescription = param;
    }

    public String getSourceCodeDescription()
    {
        return sourceCodeDescription;
    }

    public void setIotwSourceCode(String param)
    {
        this.iotwSourceCode = param;
    }

    public String getIotwSourceCode()
    {
        return iotwSourceCode;
    }

    public void setIotwSourceCodeDescription(String param)
    {
        this.iotwSourceCodeDescription = param;
    }

    public String getIotwSourceCodeDescription()
    {
        return iotwSourceCodeDescription;
    }

    public void setIotwSourceType(String param)
    {
        this.iotwSourceType = param;
    }

    public String getIotwSourceType()
    {
        return iotwSourceType;
    }

    public void setProductPageMessage(String param)
    {
        this.productPageMessage = param;
    }

    public String getProductPageMessage()
    {
        return productPageMessage;
    }

    public void setStandardPrice(String standardPrice)
    {
        this.standardPrice = standardPrice;
    }

    public String getStandardPrice()
    {
        return standardPrice;
    }

    public void setIotwSourceCodePriceHeaderId(String iotwSourceCodePriceHeaderId)
    {
        this.iotwSourceCodePriceHeaderId = iotwSourceCodePriceHeaderId;
    }

    public String getIotwSourceCodePriceHeaderId()
    {
        return iotwSourceCodePriceHeaderId;
    }

    public void setSourceCodePriceHeaderId(String sourceCodePriceHeaderId)
    {
        this.sourceCodePriceHeaderId = sourceCodePriceHeaderId;
    }

    public String getSourceCodePriceHeaderId()
    {
        return sourceCodePriceHeaderId;
    }
}
