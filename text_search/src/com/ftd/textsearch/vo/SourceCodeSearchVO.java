package com.ftd.textsearch.vo;

/**
 * Value object representing the Source Code.  
 */
public class SourceCodeSearchVO extends TextSearchVO
{
    private String sourceCode;
    private String description;
    private String sourceType;
    private String companyId;


    /**
     * Set the source code.
     * @param param Source Code
     */
    public void setSourceCode(String param)
    {
        this.sourceCode = param;
    }

    /**
     * Get the source code
     * @return
     */
    public String getSourceCode()
    {
        return sourceCode;
    }

    /**
     * Set the source code description
     * @param param Source code description
     */
    public void setDescription(String param)
    {
        this.description = param;
    }

    /**
     * Get the description
     * @return description
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * Set the source type
     * @param param
     */
    public void setSourceType(String param)
    {
        this.sourceType = param;
    }

    /**
     * Get the source type
     * @return
     */
    public String getSourceType()
    {
        return sourceType;
    }

    /**
     * Set the company id
     * @param param company id
     */
    public void setCompanyId(String param)
    {
        this.companyId = param;
    }

    /**
     * Get the company Id
     * @return
     */
    public String getCompanyId()
    {
        return companyId;
    }
}
