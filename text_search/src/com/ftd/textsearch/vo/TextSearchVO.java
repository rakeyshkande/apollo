package com.ftd.textsearch.vo;

/**
 * Base class for the text search value objects.  
 */
public class TextSearchVO
{
    private double relevancy;
    
    public TextSearchVO()
    {
    }

    /**
     * set the relevancy
     * @param param  The relevancy
     */
    public void setRelevancy(double param)
    {
        this.relevancy = param;
    }

    /**
     * Get the relevancy
     * @return
     */
    public double getRelevancy()
    {
        return relevancy;
    }
}
