package com.ftd.textsearch.client;

import com.ftd.textsearch.vo.FacilitySearchVO;
import com.ftd.textsearch.vo.ProductSearchVO;
import com.ftd.textsearch.vo.SourceCodeSearchVO;
import com.ftd.textsearch.vo.TextSearchVO;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.solr.client.ResultDoc;

/**
 * Client for the facility text search.  See the base class
 * SearchClient for details.
 */
public class FacilitySearchClient extends SearchClient
{
    private static final String FACILITY_SERVLET_GLOBAL_PARM = "FACILITY_SEARCH_URL";
    private static final String SEARCH_FUZZY_GLOBAL_PARM     = "FACILITY_FUZZY_SEARCH";

    private static final String BUSINESS_ID_FIELD   = "business_id";
    private static final String BUSINESS_TYPE_FIELD = "business_type";
    private static final String BUSINESS_NAME_FIELD = "business_name";
    private static final String ADDRESS_FIELD       = "address";
    private static final String CITY_FIELD          = "city";
    private static final String STATE_FIELD         = "state";
    private static final String ZIP_CODE_FIELD      = "zip_code";
    private static final String PHONE_NUMBER_FIELD  = "phone_number";
    
    protected String searchText;
    protected String city;
    protected String state;
    protected String businessType;

    public FacilitySearchClient()
    {
    }

    /**
     * Perform a text based search of facilities by the criteria given.
     * @param searchText Text to search for
     * @param numberOfResults Number of results to return
     * @param city City to filter by, somewhat fuzzy match
     * @param state State to filter by, two letter code
     * @param businessType Business type to filter by, one letter code
     * @return List of FacilitySearchVO that match the criteria
     */
    public List<FacilitySearchVO> searchFacilities(String searchText, 
                                                   int numberOfResults, 
                                                   String city, 
                                                   String state,
                                                   String businessType) 
    {
        this.maxRows = numberOfResults;
        this.searchText = searchText;
        this.city = city;
        this.state = state;
        this.businessType = businessType;

        List resultList = executeSearch();

        return resultList;
    }

    /**
     * Get the name of the global parm for the facility text search server.
     * @return
     */
    protected String getGlobalParmName()
    {
        return FACILITY_SERVLET_GLOBAL_PARM;
    }
    /**
     * Get the name of the global parm that is used to the the URL for the search service.
     * @return
     */
    protected String getFuzzyGlobalParmName()
    {
        return SEARCH_FUZZY_GLOBAL_PARM;
    }

    /**
     * Get the formatted text search string.  The search text is made up of
     * the searchText, optional city, optional state, and business type.
     * @return
     */
    protected String getSearchText()
    {
        StringBuilder sb = new StringBuilder();

        if (searchText != null && !searchText.equals(""))
        {
            sb.append("(");
            sb.append(searchText);
            sb.append(FUZZY_SEARCH);
            sb.append(getFuzzyValue());
            sb.append(" OR ");
            sb.append(searchText);
            sb.append(")");
            sb.append(" AND ");
        }
        
        if (city != null && !city.equals(""))
        {
            sb.append(CITY_FIELD);
            sb.append(":(\"");
            sb.append(city);
            sb.append("\")");
            sb.append(" AND ");
        }

        if (state != null && !state.equals(""))
        {
            sb.append(STATE_FIELD);
            sb.append(":");
            sb.append(state);
            sb.append(" AND ");
        }

        sb.append(BUSINESS_TYPE_FIELD);
        sb.append(":(+\"");
        sb.append(businessType);
        sb.append("\")");

        return sb.toString();
    }

    /**
     * Map the results of the search results document in to the
     * FacilitySearchVO.
     * @param doc Text Search results document
     * @return FacilitySearchVO
     */
    protected TextSearchVO mapResultsToVO(ResultDoc doc)
    {
        FacilitySearchVO vo = new FacilitySearchVO();

        Map<String, Object> fieldMap = doc.getField();
        
        vo.setBusinessId((String)fieldMap.get(BUSINESS_ID_FIELD));
        vo.setBusinessName((String)fieldMap.get(BUSINESS_NAME_FIELD));
        vo.setBusinessType((String)fieldMap.get(BUSINESS_TYPE_FIELD));
        vo.setAddress((String)fieldMap.get(ADDRESS_FIELD));
        vo.setCity((String)fieldMap.get(CITY_FIELD));
        vo.setState((String)fieldMap.get(STATE_FIELD));
        vo.setZipCode((String)fieldMap.get(ZIP_CODE_FIELD));
        vo.setPhoneNumber((String)fieldMap.get(PHONE_NUMBER_FIELD));

        return vo;
    }
}
