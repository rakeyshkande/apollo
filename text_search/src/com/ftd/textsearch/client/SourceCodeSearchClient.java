package com.ftd.textsearch.client;

import com.ftd.textsearch.vo.SourceCodeSearchVO;

import com.ftd.textsearch.vo.TextSearchVO;

import java.util.List;

import java.util.Map;

import org.apache.solr.client.ResultDoc;

/**
 * Client for the Source Code text search.  
 */
public class SourceCodeSearchClient extends SearchClient
{
    private final static String SEARCH_SERVLET_GLOBAL_PARM = "SOURCE_CODE_SEARCH_URL";
    private final static String SEARCH_FUZZY_GLOBAL_PARM   = "SOURCE_CODE_FUZZY_SEARCH";
    
    private final static String SOURCE_CODE_FIELD = "source_code";
    private final static String SOURCE_TYPE_FIELD = "source_type";
    private final static String DESCRIPTION_FIELD = "description";
    private final static String COMPANY_ID_FIELD  = "company_id";

    
    protected String companyId;
    protected String searchText;
    
    public SourceCodeSearchClient()
    {
    }
    
    /**
     * Perform a text search for source codes that match the criteria.
     * @param searchText The text to search with
     * @param companyId The companyId for filtering the results
     * @return A list of source codes that match the critera, sorted by relevance
     */
    public List<SourceCodeSearchVO> searchSourceCodes(String searchText, String companyId)
    {
        this.companyId = companyId;
        this.searchText = searchText;
        
        List resultList = executeSearch();
        
        return resultList;
    }


    /**
     * Get the name of the global parm that is used to the the URL for the search service
     * @return
     */
    protected String getGlobalParmName()
    {
        return SEARCH_SERVLET_GLOBAL_PARM;
    }

    /**
     * Get the name of the global parm that is used to the the URL for the search service.
     * @return
     */
    protected String getFuzzyGlobalParmName()
    {
        return SEARCH_FUZZY_GLOBAL_PARM;
    }

    /**
     * Build the search text used to execute the search.  This search is built using the searchText
     * and filtering the search on the company id.  The company id is set as an exact match on the
     * company id field.
     * @return formatted search text
     */
    protected String getSearchText()
    {
        StringBuilder sb = new StringBuilder();
        
        sb.append("(");
        sb.append(searchText);
        sb.append(FUZZY_SEARCH);
        sb.append(getFuzzyValue());
        sb.append(" OR ");
        sb.append(searchText);
        sb.append(")");
        sb.append(" AND ");
        sb.append(COMPANY_ID_FIELD);
        sb.append(":(+");
        sb.append(companyId);
        sb.append(")");
        
        
        return sb.toString();
    }

    /**
     * Map the search results to the SourceCodeSearchVO.
     * @param doc Search results document
     * @return source code search vo representing the results
     */
    protected TextSearchVO mapResultsToVO(ResultDoc doc)
    {
        SourceCodeSearchVO vo = new SourceCodeSearchVO();
        
        Map<String,Object> fieldMap = doc.getField();
        vo.setSourceCode((String)fieldMap.get(SOURCE_CODE_FIELD));
        vo.setCompanyId((String)fieldMap.get(COMPANY_ID_FIELD));
        vo.setDescription((String)fieldMap.get(DESCRIPTION_FIELD));
        vo.setSourceType((String)fieldMap.get(SOURCE_TYPE_FIELD));
        
        return vo;
    }
}
