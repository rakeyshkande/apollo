package com.ftd.textsearch.client;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.textsearch.vo.TextSearchVO;

import java.net.URL;

import java.util.ArrayList;
import java.util.List;

import org.apache.solr.client.QueryResults;
import org.apache.solr.client.ResultDoc;
import org.apache.solr.client.SolrClient;
import org.apache.solr.client.SolrQuery;
import org.apache.solr.client.impl.SolrClientImpl;

/**
 * Base class for the search client.  This class holds all the common code
 * for performing a text search.  
 */
public abstract class SearchClient
{
    protected Logger logger = new Logger(this.getClass().getName());

    protected final static String CONFIG_CONTEXT = "TEXT_SEARCH";
    protected final static String SCORE_FIELD  = "score";
    protected final static String FUZZY_SEARCH = "~";

    protected ConfigurationUtil cu;
    
    protected String         baseUrl;
    protected String         searchText;
    protected int            startRow;
    protected int            maxRows;
    protected String         indent;
    protected String         specialInstructions;
    protected List<String>   fieldsToReturn;
    protected List           sortFieldList;

    private String           fuzzyValue;

    /**
     * Get the name of the global parm for the text server URL.
     * @return
     */
    protected abstract String  getGlobalParmName();
    /**
     * Get the name of the global parm for the text server fuzzy value.
     * @return
     */
    protected abstract String  getFuzzyGlobalParmName();
    /**
     * Get the text for performing the specific search.  The text
     * will be in Solr format.
     * @return
     */
    protected abstract String  getSearchText();
    /**
     * Map the results from the document to the value object.
     * @param doc
     * @return
     */
    protected abstract TextSearchVO mapResultsToVO(ResultDoc doc);
    
    public SearchClient()
    {
        startRow = 0;
        maxRows = 999;
        indent = "on";
        specialInstructions = "";
    }

    /**
     * Execute a search.  The search will:
     *   1) get the URL for the specific text search 
     *   2) build up the appropriate search text
     *   3) build the full URL
     *   4) call the text search server to execute the results
     *   5) format the results of the search in a VO object
     * @return
     */
    protected List executeSearch()    
    {
        List searchResults = new ArrayList();
        
        // Get the servlet location from global_parms
        setupBaseURL();
        
        // Transform the data to the proper search url
        searchText = getSearchText();
        
        // Setup the fields to return
        this.fieldsToReturn = getFieldsToReturn();
        
        this.sortFieldList = getSortFieldList();
        
        try
        {
            SolrClient client = new SolrClientImpl(new URL(baseUrl));
            SolrQuery solrQuery = new SolrQuery();
            solrQuery.setFields(this.fieldsToReturn);
            solrQuery.setStart(startRow);
            solrQuery.setRows(maxRows);
            solrQuery.setQuery(searchText);
            //solrQuery.setSort(sortFieldList);
            solrQuery.setIncludeScore(true);
            
            logger.debug("queryString = [" + searchText + " ]");
            logger.debug("query = [" + solrQuery.toString() + " ]");

            QueryResults queryResults = client.query(solrQuery);
            
            // Parse the response into a List of VO
            logger.info( "docs returned = " + queryResults.getDocs().size() + 
                          " of " + queryResults.getNumFound() + 
                          " in " + queryResults.getQTime() + " QTime");
            for( ResultDoc doc : queryResults.getDocs() ) 
            {
                TextSearchVO vo = mapResultsToVO(doc);
                vo.setRelevancy(doc.getScore());
                searchResults.add(vo);        
            }


        }
        catch (Exception e)
        {
            logger.error("Exception performing Solr Search ",e);
        }
        
        return searchResults;
    }

    /**
     * Get the list of fields to return from the search.  This can
     * be overriden by the extending classes.
     * @return
     */
    protected List<String> getFieldsToReturn()
    {
        List<String> fieldList = new ArrayList<String>();
        fieldList.add("*");
        
        return fieldList;
    }

    /**
     * Get the sort order for the search.  This can be overridden
     * by the extending class.  The default this method gives is by 
     * score (relevance).
     * @return
     */
    protected List<String> getSortFieldList()
    {
        List<String> sortList = new ArrayList<String>();
        sortList.add("score");
        
        return sortList;
    }
    
    
    /**
     * Get the base url for the server from global parameters.
     */
    protected void setupBaseURL()
    {
        String globalParm = getGlobalParmName();
        
        this.baseUrl = getGlobalParm(globalParm);
    }

    /**
     * Get the fuzzy search value from global parameters.
     */
    protected String getFuzzyValue()
    {
        if (fuzzyValue == null)
        {
            String globalParm = getFuzzyGlobalParmName();
        
            this.fuzzyValue = getGlobalParm(globalParm);
        }
        return fuzzyValue;
    }
    
    
    /**
     * Get a value from global paramters.  
     * @param parm name of the value to get
     * @return
     */
    protected String getGlobalParm(String parm)
    {
        String param_value = null;

        try
        {
            if (cu == null)
            {
                cu = ConfigurationUtil.getInstance();
            }
            param_value = cu.getFrpGlobalParm(CONFIG_CONTEXT, parm);
        }
        catch (Exception e)
        {
            logger.error("Error getting Global Parm " + parm,e);
        }
        
        /** Hardcoded way for unit testing        
        if (parm.equals("SOURCE_CODE_SEARCH_URL"))
        {
            param_value = "http://zinc2:7778/textsearchsourcecode/";
        }
        else if (parm.equals("PRODUCT_SEARCH_URL"))
        {
            param_value = "http://zinc2:7778/textsearchproduct/";
        }
        else if (parm.equals("FACILITY_SEARCH_URL"))
        {
            param_value = "http://zinc2:7778/textsearchfacility/";
        }
        else if (parm.equals("IOTW_SEARCH_URL"))
        {
            param_value = "http://zinc2:7778/textsearchiotw/";
        }
        **/
        
        
        
        return param_value;
    }

}
