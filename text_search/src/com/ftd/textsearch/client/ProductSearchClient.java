package com.ftd.textsearch.client;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.solr.client.ResultDoc;

import com.ftd.osp.utilities.PASServiceUtil;
import com.ftd.osp.utilities.visitor.OrderEntryElementVO;
import com.ftd.osp.utilities.visitor.SourceProductVisitorImpl;
import com.ftd.textsearch.vo.ProductSearchVO;
import com.ftd.textsearch.vo.TextSearchVO;

/**
 * Client for the product text search.  This client will perform
 * a text search on products.
 */
public class ProductSearchClient extends SearchClient
{
    private static final String PRODUCT_SERVLET_GLOBAL_PARM = "PRODUCT_SEARCH_URL";
    private final static String SEARCH_FUZZY_GLOBAL_PARM    = "PRODUCT_FUZZY_SEARCH";
    
    private static final String PRODUCT_ID_FIELD           = "product_id";
    private static final String NOVATOR_ID_FIELD           = "novator_id";
    private static final String PRODUCT_NAME_FIELD         = "product_name";
    private static final String NOVATOR_NAME_FIELD         = "novator_name";
    private static final String STANDARD_PRICE_FIELD       = "standard_price";
    private static final String DELUXE_PRICE_FIELD         = "deluxe_price";
    private static final String PREMIUM_PRICE_FIELD        = "premium_price";
    private static final String SHORT_DESCRIPTION_FIELD    = "short_description";
    private static final String LONG_DESCRIPTION_FIELD     = "long_description";
    //private static final String DOMINANT_FLOWERS_FIELD     = "dominant_flowers";
    private static final String SHIP_METHOD_CARRIER_FIELD  = "ship_method_carrier";
    private static final String SHIP_METHOD_FLORIST_FIELD  = "ship_method_florist";
    private static final String COUNTRY_ID_FIELD           = "country_id";
    private static final String COMPANY_ID_FIELD           = "company_id";
    //private static final String ORDER_ENTRY_KEYWORD_FIELD  = "order_entry_keyword";
    //private static final String INDEX_NAME_FIELD           = "index_name";
    private static final String DISCOUNT_ALLOWED_FLAG_FIELD= "discount_allowed_flag";
    private static final String POPULARITY_FIELD           = "popularity";
    private static final String OVER_21_FLAG               = "over_21";
    private static final String SOURCE_CODES_FIELD         = "source_codes";
    private static final String ALLOW_FREE_SHIPPING_FLAG   = "allow_free_shipping_flag";

    protected String searchText;
    protected int    numberOfResults;
    protected Date   deliveryDate;
    protected String zipCode;
    protected String countryId;
    protected String companyId;
    protected boolean filterByAvailability;
    protected boolean includeOver21Products;
    protected String sourceCode;
    protected String defaultDomain;

    private static final String US_COUNTRY_CODE = "US";
    private static final String CANADA_COUNTRY_CODE = "CA";

    public ProductSearchClient()
    {
    }

    /**
     * Perform a text based search on the products.
     * @param searchText Text to search on
     * @param numberOfResults The number of results to return
     * @param deliveryDate The delivery date used for product availability.  Required only if filterByAvailability is on.
     * @param zipCode The Zip Code used for product availability.  Required only if filterByAvailability is on.
     * @param countryId The country used for product availabiliyt.  Required only if filterByAvailability is on.
     * @param companyId The company id to filter results for.  Required.
     * @param filterByAvailability Flag to determine if the results should be filter by product availability.  
     * @return
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
	public List<ProductSearchVO> searchProducts(String searchText, 
                                                int numberOfResults,
                                                Date deliveryDate,
                                                String zipCode,
                                                String countryId,
                                                String companyId,
                                                boolean filterByAvailability,
                                                boolean includeOver21Products,
                                                String sourceCode,
                                                String defaultDomain,
                                                OrderEntryElementVO elem)
    {
        this.numberOfResults = numberOfResults;
        this.deliveryDate = deliveryDate;
        this.zipCode = zipCode;
        this.countryId = countryId;
        this.companyId = companyId;
        this.filterByAvailability = filterByAvailability;
        this.searchText = searchText;
        this.maxRows = numberOfResults;
        this.includeOver21Products = includeOver21Products;
        this.sourceCode = sourceCode;
        this.defaultDomain = defaultDomain;
        SourceProductVisitorImpl visitor = new SourceProductVisitorImpl();
        List resultList = null;
        int numFetches = 0;
        if(logger.isDebugEnabled()) {
            logger.debug("filterByAvailability: " + filterByAvailability);
            logger.debug("elem == null? " + (elem == null));
        }
        // elem represents the SourceProductUtility check here.
        if (filterByAvailability || elem != null)
        {
            List<ProductSearchVO> filteredList = new ArrayList<ProductSearchVO>();
            boolean isDomestic = true;
            
            if (countryId != null && !(countryId.equalsIgnoreCase(US_COUNTRY_CODE) || 
                    countryId.equalsIgnoreCase(CANADA_COUNTRY_CODE)))
            {
                isDomestic = false;
            }
            do
            {
                if(logger.isDebugEnabled()) {
                    logger.debug("start do ");
                }
                resultList = executeSearch();
                
                for (int i=0; i < resultList.size() && filteredList.size() < numberOfResults; i++)
                {
                    if(logger.isDebugEnabled()) {
                        logger.debug("start for");
                    }
                    ProductSearchVO vo = (ProductSearchVO) resultList.get(i);
                    try
                    {
                        boolean isAvailable = true;
                        // First check if the product is allowed on source code.
                        if(elem != null) {
                            elem.setProductId(vo.getProductId());
                            try {
                                if(!elem.accept(visitor)) {
                                    if(logger.isDebugEnabled()) {
                                        logger.debug("count: Product filtered by source product utility:" + i + ":" + vo.getProductId());
                                    }
                                    // Failed the source product utility check. Product not allowed on source code.
                                    continue;
                                }
                            } catch (Exception ve) {
                                // Product should be included if it does not pass the check.
                                logger.error(ve);
                                logger.error("Exception in source product filtering.");
                            }
                        }
                    
                        if(filterByAvailability) {
                        
                            if (isDomestic)
                            {
                                isAvailable = PASServiceUtil.isProductAvailable(vo.getProductId(),deliveryDate,zipCode);
                            }
                            else
                            {
                                isAvailable = PASServiceUtil.isInternationalProductAvailable(vo.getProductId(),deliveryDate,countryId);
                            }
                            if(logger.isDebugEnabled()) {
                                logger.debug("Checking avail for " + vo.getProductId() + " " + deliveryDate + " " + zipCode + " " + isAvailable);
                            }
                        }
                        
                        if(logger.isDebugEnabled()) {
                            logger.debug("count: productid: isAvailable -- " + i + ":" + vo.getProductId() + ":" + isAvailable);
                        }
                        if (isAvailable)
                        {
                            filteredList.add(vo);
                        }
                    } catch (Exception e) {
                        logger.error (e);
                        logger.error("Exception using PAS",e);                    
                    } 
                }
                // Increment the start
                startRow += numberOfResults;
                numFetches++;
            } while (resultList.size() > 0 && filteredList.size() < numberOfResults);
            if(logger.isDebugEnabled()) {
                logger.debug("numFetches:" + numFetches);
            }
            resultList = filteredList;
        }
        
        return resultList;
    }

    /**
     * Get the name of the global parameter used for the product text search global parm url.
     * @return
     */
    protected String getGlobalParmName()
    {
        return PRODUCT_SERVLET_GLOBAL_PARM;
    }
    /**
     * Get the name of the global parm that is used to the the URL for the search service.
     * @return
     */
    protected String getFuzzyGlobalParmName()
    {
        return SEARCH_FUZZY_GLOBAL_PARM;
    }

    /**
     * Get the formatted text for searching for a product.  The searchText
     * is used along with optional companyId and optional countryId.
     * @return
     */
    protected String getSearchText()
    {
        StringBuilder sb = new StringBuilder();

        if (searchText != null && !searchText.equals(""))
        {
            sb.append("((");
            sb.append(searchText);
            sb.append(FUZZY_SEARCH);
            sb.append(getFuzzyValue());
            sb.append(" OR ");
            sb.append(searchText);
            sb.append(")");

            sb.append(" OR ");
            sb.append(PRODUCT_NAME_FIELD);
            sb.append(":(");
            sb.append(searchText);
            sb.append("))");

        }
        
        if (countryId != null && !countryId.equals(""))
        {
            if (sb.length() > 0)
            {
                sb.append(" AND ");
            }
            sb.append(COUNTRY_ID_FIELD);
            sb.append(":(+");
            sb.append(countryId);
            sb.append(")");
        }
        else
        {
            if (sb.length() > 0)
            {
                sb.append(" AND ");
            }
            sb.append(COUNTRY_ID_FIELD);
            sb.append(":(+US)");
        }
        if (companyId != null && !companyId.equals(""))
        {
            if (sb.length() > 0)
            {
                sb.append(" AND ");
            }
            sb.append(COMPANY_ID_FIELD);
            sb.append(":(+");
            sb.append(companyId);
            sb.append(")");
        }
        if (!includeOver21Products) {
            if (sb.length() > 0)
            {
                sb.append(" AND ");
            }
            sb.append(OVER_21_FLAG);
            sb.append(":(+N)");
        }

        if (sourceCode != null && !sourceCode.equals(""))
        {
            if (sb.length() > 0)
            {
                sb.append(" AND ");
            }
            sb.append(" ( ");
            sb.append(SOURCE_CODES_FIELD);
            sb.append(":(+");
            sb.append(sourceCode);
            sb.append(")");

            if (defaultDomain != null && defaultDomain.length() > 0) 
            {
                sb.append(" OR ");
                sb.append(SOURCE_CODES_FIELD);
                sb.append(":(+");
                sb.append(defaultDomain);
                sb.append(")");
            }
            sb.append(")");
        }
        
        return sb.toString();
    }

    /**
     * Map the results document of the search to the ProductSearchVO.
     * @param doc results document
     * @return ProductSearchVO
     */
    protected TextSearchVO mapResultsToVO(ResultDoc doc)
    {
        ProductSearchVO vo = new ProductSearchVO();

        Map<String, Object> fieldMap = doc.getField();
        vo.setProductId((String)fieldMap.get(PRODUCT_ID_FIELD));
        vo.setProductName((String)fieldMap.get(PRODUCT_NAME_FIELD));
        vo.setNovatorName((String)fieldMap.get(NOVATOR_NAME_FIELD));
        vo.setShortDescription((String)fieldMap.get(SHORT_DESCRIPTION_FIELD));
        vo.setLongDescription((String)fieldMap.get(LONG_DESCRIPTION_FIELD));

        vo.setCompanyId((String)fieldMap.get(COMPANY_ID_FIELD));
        vo.setDeluxePrice((String)fieldMap.get(DELUXE_PRICE_FIELD));
        vo.setDiscountAllowedFlag((String)fieldMap.get(DISCOUNT_ALLOWED_FLAG_FIELD));
        vo.setNovatorId((String)fieldMap.get(NOVATOR_ID_FIELD));
        vo.setPremiumPrice((String)fieldMap.get(PREMIUM_PRICE_FIELD));
        vo.setShipMethodCarrier((String)fieldMap.get(SHIP_METHOD_CARRIER_FIELD));
        vo.setShipMethodFlorist((String)fieldMap.get(SHIP_METHOD_FLORIST_FIELD));
        vo.setStandardPrice((String)fieldMap.get(STANDARD_PRICE_FIELD));
        vo.setPopularity((String)fieldMap.get(POPULARITY_FIELD));
        vo.setOver21Flag((String)fieldMap.get(OVER_21_FLAG));
        vo.setSourceCodes((String)fieldMap.get(SOURCE_CODES_FIELD));
        vo.setAllowFreeShippingFlag((String)fieldMap.get(ALLOW_FREE_SHIPPING_FLAG));

        return vo;
    }
    
}
