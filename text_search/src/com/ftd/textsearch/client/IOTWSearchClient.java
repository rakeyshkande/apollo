package com.ftd.textsearch.client;

import com.ftd.textsearch.vo.IOTWSearchVO;
import com.ftd.textsearch.vo.TextSearchVO;

import java.util.List;
import java.util.Map;

import org.apache.solr.client.ResultDoc;

/**
 * Client for the Item of the Week (IOTW) text search.  See base class
 * SearchClient for details on the search.
 */
public class IOTWSearchClient extends SearchClient
{
    private static final String SEARCH_SERVLET_GLOBAL_PARM = "IOTW_SEARCH_URL";
    private static final String SEARCH_FUZZY_GLOBAL_PARM   = "IOTW_FUZZY_SEARCH";
    
    private static final String PRODUCT_ID_FIELD               = "product_id";
    private static final String NOVATOR_ID_FIELD               = "novator_id";
    private static final String PRODUCT_NAME_FIELD             = "product_name";
    private static final String PRODUCT_LONG_DESCRIPTION_FIELD = "long_description";
    private static final String STANDARD_PRICE_FIELD           = "standard_price";
    private static final String SOURCE_CODE_FIELD              = "source_code";
    private static final String SOURCE_TYPE_FIELD              = "source_type";
    private static final String SOURCE_CODE_DESCRIPTION_FIELD  = "source_code_description";
    private static final String SOURCE_CODE_PRICE_HEADER_ID_FIELD  = "source_code_price_header_id";
    private static final String IOTW_SOURCE_CODE_FIELD         = "iotw_source_code";
    private static final String IOTW_SOURCE_TYPE_FIELD         = "iotw_source_type";
    private static final String IOTW_SOURCE_CODE_DESCRIPTION_FIELD = "iotw_source_code_description";
    private static final String IOTW_SOURCE_CODE_PRICE_HEADER_ID_FIELD = "iotw_source_code_price_header_id";
    private static final String COMPANY_ID_FIELD               = "company_id";
    private static final String PRODUCT_PAGE_MESSAGE_FIELD     = "product_page_message";


    protected String companyId;
    protected String searchText;
    protected String iotwSourceCode;
    protected String sourceCode;
    protected String productId;
    protected String priceHeaderId;

    public IOTWSearchClient()
    {
    }

    /**
     * Perform a search for Item of the Weeks that match the criteria.
     * @param searchText Text to search for
     * @param companyId Company to filter the results by
     * @return
     */
    public List<IOTWSearchVO> searchIOTW(String searchText, 
                                         String companyId,
                                         String iotwSourceCode, 
                                         String sourceCode, 
                                         String productId,
                                         String priceHeaderId)
    {
        this.companyId = companyId;
        this.searchText = searchText;
        this.iotwSourceCode = iotwSourceCode;
        this.sourceCode = sourceCode;
        this.productId = productId;
        this.priceHeaderId = priceHeaderId;

        List resultList = executeSearch();

        return resultList;
    }

    /**
     * Get the name of the global parameter for the IOTW text search server.
     * @return
     */
    protected String getGlobalParmName()
    {
        return SEARCH_SERVLET_GLOBAL_PARM;
    }
    /**
     * Get the name of the global parm that is used to the the URL for the search service.
     * @return
     */
    protected String getFuzzyGlobalParmName()
    {
        return SEARCH_FUZZY_GLOBAL_PARM;
    }

    
    /**
     * Get the formatted search text for the IOTW search.  The search
     * text will contain the searchText and the company_id.
     * @return
     */
    protected String getSearchText()
    {
        StringBuilder sb = new StringBuilder();

        sb.append(COMPANY_ID_FIELD);
        sb.append(":(+");
        sb.append(companyId);
        sb.append(")");

        if (this.searchText != null && !this.searchText.equals(""))
        {
            sb.append(" AND ");
            sb.append("(");
            sb.append(searchText);
            sb.append(FUZZY_SEARCH);
            sb.append(getFuzzyValue());
            sb.append(" OR ");
            sb.append(searchText);
            sb.append(")");
        }

        if (this.iotwSourceCode != null && !this.iotwSourceCode.equals(""))
        {
            sb.append(" AND ");
            sb.append(IOTW_SOURCE_CODE_FIELD);
            sb.append(":(+");
            sb.append(iotwSourceCode);
            sb.append(")");
        }
        if (this.sourceCode != null && !this.sourceCode.equals(""))
        {
            sb.append(" AND ");
            sb.append(SOURCE_CODE_FIELD);
            sb.append(":(+");
            sb.append(sourceCode);
            sb.append(")");
        }
        if (this.productId != null && !this.productId.equals(""))
        {
            sb.append(" AND (");
            sb.append(PRODUCT_ID_FIELD);
            sb.append(":(+");
            sb.append(productId);
            sb.append(")");
            sb.append(" OR ");
            sb.append(NOVATOR_ID_FIELD);
            sb.append(":(+");
            sb.append(productId);
            sb.append("))");
        }
        if (this.priceHeaderId != null && !this.priceHeaderId.equals(""))
        {
            sb.append(" AND ");
            sb.append(IOTW_SOURCE_CODE_PRICE_HEADER_ID_FIELD);
            sb.append(":(+");
            sb.append(priceHeaderId);
            sb.append(")");
        }


        return sb.toString();
    }

    /**
     * Map the results of the search results document to the IOTWSearchVO.
     * @param doc results document
     * @return TextSearchVO
     */
    protected TextSearchVO mapResultsToVO(ResultDoc doc)
    {
        IOTWSearchVO vo = new IOTWSearchVO();

        Map<String, Object> fieldMap = doc.getField();
        vo.setCompanyId((String)fieldMap.get(COMPANY_ID_FIELD));

        vo.setProductId((String)fieldMap.get(PRODUCT_ID_FIELD));
        vo.setProductLongDescription((String)fieldMap.get(PRODUCT_LONG_DESCRIPTION_FIELD));
        vo.setProductName((String)fieldMap.get(PRODUCT_NAME_FIELD));
        vo.setStandardPrice((String)fieldMap.get(STANDARD_PRICE_FIELD));

        vo.setSourceCode((String)fieldMap.get(SOURCE_CODE_FIELD));
        vo.setSourceCodeDescription((String)fieldMap.get(SOURCE_CODE_DESCRIPTION_FIELD));
        vo.setSourceType((String)fieldMap.get(SOURCE_TYPE_FIELD));
        vo.setSourceCodePriceHeaderId((String)fieldMap.get(SOURCE_CODE_PRICE_HEADER_ID_FIELD));

        vo.setIotwSourceCode((String)fieldMap.get(IOTW_SOURCE_CODE_FIELD));
        vo.setIotwSourceCodeDescription((String)fieldMap.get(IOTW_SOURCE_CODE_DESCRIPTION_FIELD));
        vo.setIotwSourceType((String)fieldMap.get(IOTW_SOURCE_TYPE_FIELD));
        vo.setIotwSourceCodePriceHeaderId((String)fieldMap.get(IOTW_SOURCE_CODE_PRICE_HEADER_ID_FIELD));

        vo.setProductPageMessage((String)fieldMap.get(PRODUCT_PAGE_MESSAGE_FIELD));

        return vo;
    }
}
