/**
 * 
 */
package com.ftd.textsearch.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import com.ftd.osp.utilities.plugins.Logger;

/**
 * The only purpose of this class is to instantiate a logger instance
 * so we will use the common Logger facilities.
 * 
 * @author cjohnson
 *
 */
public class InitLoggerServlet extends HttpServlet {
	
	private static Logger logger = new Logger(InitLoggerServlet.class.getName());

	@Override
	public void init() throws ServletException {
		logger.info("Text Search logging initialized");
		logger.debug("Debug level logging");
		super.init();
	}

}
