package com.ftd.b2b.ariba.common.utils;

import com.ftd.b2b.common.utils.BusinessObject;
import com.ftd.b2b.ariba.integration.dao.IRequestDataMiner;
import com.ftd.b2b.ariba.common.valueobjects.AribaRequestVO;
import com.ftd.b2b.ariba.common.valueobjects.URLLookupVO;
import com.ftd.b2b.ariba.integration.dao.IURLLookupDAO;
import com.ftd.framework.common.exceptions.ResourceNotFoundException;
import com.ftd.framework.common.exceptions.FTDApplicationException;
import com.ftd.framework.common.utilities.ResourceManager;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.b2b.constants.B2BConstants;
import com.ftd.b2b.common.utils.XMLParseException;
import com.ftd.b2b.common.utils.AuthenticationException;
import com.ftd.b2b.common.utils.InvalidClientException;
import com.ftd.b2b.common.utils.InvalidDUNSException;
import com.ftd.b2b.utils.mail.B2BMailer;

import java.util.List;
import java.util.Iterator;

/**
 * Implementaion class for obtaining and validating cXML data shared across
 * all B2B use cases.
 *
 * @author York Davis
 * @version 1.0 
 **/
public class RequestAuthenticatorImpl extends BusinessObject implements IRequestAuthenticator, 
                                                                        B2BConstants
{
  private ResourceManager resourceManager;
  
  public RequestAuthenticatorImpl()
  {
    super(LOG_CATEGORY_REQUEST_AUTHENTICATOR);
    resourceManager = ResourceManager.getInstance();    
  }

  /**
   * Obtain the URLLookupVO object based on the ASN Number.
   * 
   * @param   ASN Nmber.
   * @return  URLLookup Object containing PunchOut Client data.
   * @throws  InvalidClientException if the ASN Number does not exist on the URLxref table.
   * @throws  FTDApplicationException when an unrecoverable application error is thrown.   
   *
   * @author York Davis
   **/    
  public URLLookupVO getPunchOutClientData(String asnNumber) throws InvalidClientException,
                                                                    FTDApplicationException
  {
    IURLLookupDAO urlLookupDAO = null;
    try
    {
      urlLookupDAO = (IURLLookupDAO) resourceManager.getImplementation(INTERFACE_URLLOOKUPDAO);
    }
    catch (ResourceNotFoundException rnfe)
    {
      getLogManager().error("" + ERROR_CODE_UNABLETOLOADSERVICE +  rnfe.toString());        
      B2BMailer.getInstance().send(this, "getPunchOutClientData()", rnfe.toString(), B2BMailer.ACTION2);      
      throw new FTDApplicationException(ERROR_CODE_UNRECOVERABLE_EXCEPTION, rnfe);
    }

    List list = null;
    try
    {
        list = urlLookupDAO.retrieve(asnNumber);        
    }
    catch(Throwable t)
    {
        getLogManager().error("" + ERROR_CODE_UNRECOVERABLE_EXCEPTION +  t.toString());    
        B2BMailer.getInstance().send(this, "getPunchOutClientData()", t.toString(), B2BMailer.ACTION2);              
        throw new FTDApplicationException(ERROR_CODE_UNRECOVERABLE_EXCEPTION, t);
    }

    if ((list == null) || (list.size() == 0))
    {
        throw new InvalidClientException(asnNumber + " not a valid PunchOut Client");
    }

    Iterator iterator = list.listIterator();

    return (URLLookupVO) iterator.next();
  }

  /**
   * Obtain the AribaRequestVO object which contains common
   * properties across all B2B Ariba use cases.
   * 
   * @param   cXML document.
   * @return  ArbiaRequestVO object.
   * @throws  XMLParseException when the specified fields can not be parsed.
   * @throws  FTDApplicationException when an unrecoverable application error is thrown.
   *
   * @author York Davis
   **/
  public AribaRequestVO getXMLHeaderData(String cXML) throws XMLParseException,
                                                             FTDApplicationException
  {
        synchronized(this.getClass())
        {
          IRequestDataMiner requestDataMiner = null;

          try
          {
              requestDataMiner = (IRequestDataMiner) resourceManager.getImplementation(INTERFACE_REQUESTDATAMINER);
          }
          catch(ResourceNotFoundException rnfe)
          {
              getLogManager().error("" + ERROR_CODE_UNABLETOLOADSERVICE +  rnfe.toString());                
              B2BMailer.getInstance().send(this, "getXMLHeaderData()", rnfe.toString(), B2BMailer.ACTION2);                          
              throw new FTDApplicationException(ERROR_CODE_UNRECOVERABLE_EXCEPTION, rnfe);
          }

          try
          {
              return requestDataMiner.mine(cXML);
          }
          catch (XMLParseException xmle)
          {
              getLogManager().error("" + ERROR_CODE_XMLPARSEEXCEPTION +  xmle.toString());     
              // Do not send the XML document to the pager
              B2BMailer.getInstance().send(this, "getXMLHeaderData()", xmle.toString(), B2BMailer.ACTION2);                
              String msg = xmle.toString() + "\n\n XML document : " + cXML;            
              B2BMailer.getInstance().send(this, "getXMLHeaderData()", msg, B2BMailer.ACTION1);                            
              throw xmle;
          }
      }        
  }


  /**
   * Validates the document fields obtained from the CXML document
   * against values in the properties file.  This is an overloaded method.
   * This method assumes that both duns and shared secret should be validated.
   * 
   * @param   cXML Shared Secret.
   * @throws  AuthencationException when the SharedSecret is not correct.
   * @throws  InvalidDUNSException when the DUNS is not correct.
   *
   * @author Ed Mueller 6/16/03
   **/ 
  public void validateDocument(AribaRequestVO vo) throws AuthenticationException,
                                                         InvalidDUNSException
  {
        validateDocument(vo,true,true);
  }

  /**
   * Validates the document fields obtained from the CXML document
   * against values in the properties file.
   * 
   * @param   cXML Shared Secret.
   * @throws  AuthencationException when the SharedSecret is not correct.
   * @throws  InvalidDUNSException when the DUNS is not correct.
   *
   * @author York Davis
   *
   *  6/16/03 Ed Mueller.  Add boolean parameters so that calling function can determine what needs to be validated.
   **/ 
  public void validateDocument(AribaRequestVO vo, boolean checkSecret, boolean checkDUNS) throws AuthenticationException,
                                                         InvalidDUNSException
  {
        // Shared Secret
        if (checkSecret){
                    //trap any parsing errors related to shared secret
                    String sharedSecret = "";
                    try{
                            sharedSecret = (String) vo.getParams().get(CONSTANT_SHARED_SECRET);
                            }
                    catch(Throwable t){
                            String msg = "Could not find shared secret in request:" + CONSTANT_SHARED_SECRET;                    
                            getLogManager().error("" + ERROR_CODE_INVALIDSHAREDSECRET_EXCEPTION +  msg);                        
                            B2BMailer.getInstance().send(this, "validateDocument()", msg, B2BMailer.ACTION2);                                      
                            throw new AuthenticationException(msg);
                            }                    
                    String propertiesSharedSecret =  resourceManager.getProperty(PROPERTY_B2B_PROPERTY_FILE, 
                                                                                 PROPERTY_SHARED_SECRET);       
                    if (sharedSecret == null || propertiesSharedSecret == null || !sharedSecret.equals(propertiesSharedSecret))
                    {
                        String msg = "Invalid SharedSecret '" + sharedSecret + "'";                    
                        getLogManager().error("" + ERROR_CODE_INVALIDSHAREDSECRET_EXCEPTION +  msg);                        
                        B2BMailer.getInstance().send(this, "validateDocument()", msg, B2BMailer.ACTION2);                                      
                        throw new AuthenticationException(msg);
                    }
        }
        
        // DUNS number
        if (checkDUNS) {
                    //trap any parsing errors related to shared secret
                    String dunsNumber = "";
                    try{        
                        dunsNumber = (String) vo.getParams().get(CONSTANT_FTD_DUNS_NUMBER);
                        }
                    catch(Throwable t){
                            String msg = "Could not find DUNS Number in request:" + CONSTANT_FTD_DUNS_NUMBER;                    
                            getLogManager().error("" + ERROR_CODE_INVALIDSHAREDSECRET_EXCEPTION + msg);                        
                            B2BMailer.getInstance().send(this, "validateDocument()", msg, B2BMailer.ACTION2);                                      
                            throw new InvalidDUNSException(msg);
                            }                    
                            
                        String ftdDUNSNumber =  "";
                        String ftdASNNumber = "";
                        try {
                            ConfigurationUtil cu = ConfigurationUtil.getInstance();
                            ftdDUNSNumber = cu.getFrpGlobalParm(B2B_ARIBA_CONFIG_CONTEXT, PROPERTY_FTD_DUNS_NUMBER);
                            ftdASNNumber = cu.getFrpGlobalParm(B2B_ARIBA_CONFIG_CONTEXT, PROPERTY_FTD_ASN_NUMBER);
                        } catch(Exception e) {
                            String msg = "Can't get DUNS number from global_parms";                    
                            getLogManager().error("" + ERROR_CODE_UNRECOVERABLE_EXCEPTION +  msg);                        
                            B2BMailer.getInstance().send(this, "validateDocument()", msg, B2BMailer.ACTION2);                                      
                            throw new InvalidDUNSException(msg);
                        }
                        if (ftdASNNumber != null && ftdDUNSNumber != null && 
                           (dunsNumber.equalsIgnoreCase(ftdDUNSNumber) || dunsNumber.equalsIgnoreCase(ftdASNNumber)))
                        {
                        }
                        else
                        {
                            String msg = "Invalid DUNS number '" + dunsNumber + "'";                    
                            getLogManager().error("" + ERROR_CODE_INVALIDSHAREDSECRET_EXCEPTION +  msg);                        
                            B2BMailer.getInstance().send(this, "validateDocument()", msg, B2BMailer.ACTION2);                                      
                            throw new InvalidDUNSException(msg);
                        }
            }
   }

   /**
    * Converts this class into a string.
    * 
    * @author York Davis
    **/
   public String toString()
  {
    return this.getClass().toString();
  }

  /**
    * Determine equality of this object to another.
    * 
    * @param  The object to be compared.
    *
    * @author York Davis
    **/
  public boolean equals(Object o)
  {
    return this == o;
  }

  /**
    * Saves state. Currently unused.
    * 
    * @author York Davis
    **/
  public void save()
  {
  }

  /**
    * Builds state. Currently unused.
    * 
    * @author York Davis
    **/
  public void create()
  {
  }

}