package com.ftd.b2b.ariba.common.utils;



import com.ftd.framework.common.exceptions.FTDApplicationException;
import com.ftd.b2b.common.utils.XMLParseException;
import com.ftd.b2b.common.utils.AuthenticationException;
import com.ftd.b2b.common.utils.InvalidClientException;
import com.ftd.b2b.common.utils.InvalidDUNSException;
import com.ftd.b2b.ariba.common.valueobjects.AribaRequestVO;
import com.ftd.b2b.ariba.common.valueobjects.URLLookupVO;

/**
 * Interface for obtaining and validating cXML data shared across
 * all B2B use cases.
 *
 * @author York Davis
 * @version 1.0 
 **/
public interface IRequestAuthenticator 
{
  /**
   * Obtain the URLLookupVO object based on the ASN Number.
   * 
   * @param   ASN Nmber.
   * @return  URLLookup Object containing PunchOut Client data.
   * @throws  InvalidClientException if the ASN Number does not exist on the URLxref table.
   * @throws  FTDApplicationException when an unrecoverable application error is thrown.   
   *
   * @author York Davis
   **/   
  public URLLookupVO getPunchOutClientData(String asnNumber) throws InvalidClientException,
                                                                    FTDApplicationException;

   /**
   * Obtain the AribaRequestVO object which contains common
   * properties across all B2B Ariba use cases.
   * 
   * @param   cXML document.
   * @return  ArbiaRequestVO object.
   * @throws  XMLParseException when the specified fields can not be parsed.
   * @throws  FTDApplicationException when an unrecoverable application error is thrown.
   *
   * @author York Davis
   **/ 
  public AribaRequestVO getXMLHeaderData(String cXML) throws XMLParseException,       
                                                             FTDApplicationException;

  /**
   * Validates the document fields obtained from the CXML document
   * against values in the properties file.
   * 
   * @param   cXML Shared Secret.
   * @throws  AuthencationException when the values do not match.
   *
   * @author York Davis
   **/    
  public void validateDocument(AribaRequestVO vo) throws AuthenticationException,
                                                         InvalidDUNSException;

  /**
   * Validates the document fields obtained from the CXML document
   * against values in the properties file.
   * 
   * @param   cXML Shared Secret.
   * @throws  AuthencationException when the values do not match.
   *
   * @author Ed Mueller 6/16/03
   **/    
  public void validateDocument(AribaRequestVO vo, boolean checkSecret, boolean checkDuns) throws AuthenticationException,
                                                         InvalidDUNSException;                                                         
}