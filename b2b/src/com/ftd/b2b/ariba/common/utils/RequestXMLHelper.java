package com.ftd.b2b.ariba.common.utils;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.*;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.ftd.b2b.common.utils.XMLHelper;
import com.ftd.b2b.common.utils.XMLParseException;
import com.ftd.b2b.constants.B2BConstants;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.sun.org.apache.xerces.internal.parsers.DOMParser;

/**
 * Implementation class to extract values from an XML document
 * for the PunchOutRequest.
 *
 * @author York Davis
 * @version 1.0 
 **/
public class RequestXMLHelper extends XMLHelper implements B2BConstants
{
  public static final String HEADER      = "Header";
  public static final String FROM        = "From";
  public static final String TO          = "To";  
  public static final String CREDENTIAL  = "Credential";
  public static final String IDENTITY    = "Identity";
 
  /**
    * Retrieve the SharedSecret
    * 
    * @param  The cXML document
    * @throws XMLParseException - when an error occurs parsing the XML
    *
    * @author York Davis
    *
    * Ed Mueller 6/16/03 - Added try block
    **/
  public String getSharedSecret(String sXML) throws XMLParseException
  {
    String value = "";
    try{       
        value = getTextValueByTagName(sXML, CONSTANT_SHARED_SECRET);
        }
    catch(Throwable t){
       throw new XMLParseException("Node " + CONSTANT_SHARED_SECRET + " Not Found.");
        }
    return value;
  }

  /**
    * Retrieve the PayloadId
    * 
    * @param  The cXML document
    * @throws XMLParseException - when an error occurs parsing the XML
    *
    * @author York Davis
    * Ed Mueller 6/16/03 - Added try block
    **/
  public String getPayloadId(String sXML) throws XMLParseException
  {
   String value = "";
    try{       
       value = getAttributeByTagName(sXML, CONSTANT_CXML, CONSTANT_PAYLOAD_ID);
        }
    catch(Throwable t){
       throw new XMLParseException("Node " + CONSTANT_PAYLOAD_ID + " Not Found.");
        }
    return value;       
  }

  /**
    * Retrieve the ASN Number
    * 
    * @param  The cXML document
    * @return The ASN Buyer Number
    * @throws XMLParseException - when an error occurs parsing the XML
    *
    * @author Ed Mueller
    **/
  public String getAsnBuyerNumber(String sXML) throws XMLParseException
  {
    try
    {
	  	Document doc = DOMUtil.getDocument(sXML);
	  	return DOMUtil.getNodeText(doc, "/cXML/Header/From/Credential/Identity");
  	}
  	catch (Throwable ioe) {
  		throw new XMLParseException(ioe.toString());
  	}

    
  }

  /**
   * Retrieve the DUNS Number OR The NetworkId is DUNS is not available
   * 
   * @param  The cXML document
   * @return The DUNS Number
   * @throws XMLParseException - when an error occurs parsing the XML
   *
   * @author York Davis
   **/
  public String getDUNSNumber(String sXML) throws XMLParseException
  {
	  
	try {
		Document doc = DOMUtil.getDocument(sXML);
		
		String domain =  DOMUtil.getNodeText(doc, "/cXML/Header/To/Credential/@domain");
		
		if (domain.equals("DUNS")) {
			return DOMUtil.getNodeText(doc, "/cXML/Header/To/Credential[@domain='DUNS']/Identity");
		}else if (domain.equals("NetworkId")) {
			return DOMUtil.getNodeText(doc, "/cXML/Header/To/Credential[@domain='NetworkId']/Identity");
		}else {
			throw new Exception("DUNS Number or Network ID Not Found");
		}
	}   	
	catch (Throwable ioe) {
		throw new XMLParseException(ioe.toString());
	}
    
  }

    /**
   * Retrieve the Internal Supplier ID
   * 
   * @param  The cXML document
   * @return The Internal Supplier ID
   * @throws XMLParseException - when an error occurs parsing the XML
   *
   * @author Kristyn Angelo
   **/
  public String getInternalSupplierID(String sXML) throws XMLParseException
  {
	    try {
			Document doc = DOMUtil.getDocument(sXML);
			return DOMUtil.getNodeText(doc, "/cXML/Header/To/Credential[@domain='internalsupplierid']/Identity");
		}   	
		catch (Throwable ioe) {
			throw new XMLParseException(ioe.toString());
		}    
  }
}