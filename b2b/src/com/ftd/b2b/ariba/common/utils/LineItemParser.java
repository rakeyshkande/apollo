package com.ftd.b2b.ariba.common.utils;

import com.ftd.b2b.ariba.integration.dao.ICountryCodeDAO;
import com.ftd.b2b.ariba.common.utils.ClientOptions;
import com.ftd.b2b.ariba.common.valueobjects.ClientOptionsVO;
import java.util.ArrayList;
import java.util.StringTokenizer;
import com.ftd.b2b.ariba.common.valueobjects.ShoppingCartOrderVO;
import com.ftd.b2b.ariba.common.valueobjects.ShoppingCartLineItemVO;
import com.ftd.b2b.constants.B2BConstants;
import java.text.SimpleDateFormat;
import com.ftd.b2b.utils.mail.B2BMailer;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.framework.common.exceptions.FTDApplicationException;
import com.ftd.framework.common.utilities.ResourceManager;
import com.ftd.framework.common.exceptions.ResourceNotFoundException;


/** 
 * A utility class for converting a Novator tilde-delimitted
 * string into objects.
 */

/* Ed Mueller 6/25/03
    Added parsing for shipping info, card message and shipping instructions.*/
 
public class LineItemParser implements B2BConstants
{

    private String data;
    private int currentStartPos = 0;
    private int currentEndPos = 0;    
    private int delimiterLength;
    private String delimiter;
    
    /**
     * Constructor
     */
    public LineItemParser()
    {

    }

    public LineItemParser(String dataValue,String delimiterValue)
    {
       data = dataValue;
       delimiter = delimiterValue;
       delimiterLength = delimiter.length();

    }


    /**
     This method removes HTML tags (if any) from a string.
     @params String Input string (potentially) containing HTML tags
     @returns String Input string with HTML tags removed
     */
    private static String stripHtml(String a_rawStr) {
        String outStr = a_rawStr;
        if (a_rawStr.indexOf('<') > -1) {
            StringBuffer outStrBuf = new StringBuffer(a_rawStr.length());
            boolean isText = true;
            char curChar;
            for (int x=0; x < a_rawStr.length(); x++) {
                curChar = a_rawStr.charAt(x);
                if (curChar == '<') {
                    isText = false;
                } else if (curChar == '>') {
                    isText = true;
                } else if (isText) {
                    outStrBuf.append(curChar);
                }
            } // end for
            outStr = outStrBuf.toString();
        }
        return(outStr);
    }


    /**
     This method takes in a line of data and delimits each double tilde with 
     a dash.  This will allow the Sting tokenizer to tokenize each element.
     @params String the raw data
     @returns String the data with dashes
     */
    private static String addDashes(String rawData){
        StringBuffer newValue = new StringBuffer("");
        
        char[] charValues = rawData.toCharArray();
        int length = charValues.length;
        int delimCount = 0;


        
        for(int i=0;i<length;i++){
            char currentChar = charValues[i];
            if (currentChar == CONSTANT_CATALOG_FILE_DELIMITER_SINGLE){
                delimCount++;//increment counter

                //determine what to do based on number of tildes in a row encountered
                switch (delimCount)
                    {
                    case 1:
                        //first delimiter in possible series
                        newValue.append(currentChar);
                        break;
                    case 2:
                        //second delimiter in series;
                        newValue.append(currentChar);
                        break;
                    case 3:
                        //third delimiter in series, must add seperator and set tilde count
                        newValue.append(CONSTANT_RAW_DATA_EMPTY_FIELD);
                        newValue.append(currentChar);
                        delimCount = 1;
                        break;
                    }
                }
            else{
                delimCount = 0;
                newValue.append(currentChar);        
                }
            }


    return newValue.toString();

    }

 /**
  * Builds an Order VO from a string of raw socket data.
  *
  * @params String, tilda delimited line item
  * @returns ShoppingCartOrderVO, VO containing line and order data.
  */
  public static ShoppingCartOrderVO parseData(String rawLineItem) 
                    throws FTDApplicationException
  {



    ShoppingCartOrderVO voOrder = new ShoppingCartOrderVO();

    //first add dashes between field delimiters so string tokenizer can be used
    rawLineItem = addDashes(rawLineItem);

    try{

    //create tokenizer to parse raw data
    StringTokenizer tokenizer = new StringTokenizer(rawLineItem, CONSTANT_RAW_DATA_FIELD_DELIMITER);

    voOrder.setLineItems(new ArrayList());
    ShoppingCartLineItemVO voLineItem = new ShoppingCartLineItemVO();

    //data formatter
    SimpleDateFormat sdf = new SimpleDateFormat( CONSTANT_SHOPPING_CART_DATE_FORMAT ); 

    String temp = null;

    String readValue = null;

    //Make sure line contains tokens
    if (tokenizer.hasMoreElements()) {    
        //parse through entire line
        readValue=tokenizer.nextToken(); //Field not used : voLineItem.setCountOfCharectorsInTrans(tokenizer.nextToken()); //1
        voLineItem.setOrderNumber(tokenizer.nextToken());//2
        readValue=tokenizer.nextToken(); //Field not used : voLineItem.setSourceCode(tokenizer.nextToken());//3
        readValue=tokenizer.nextToken(); 
        if (!(readValue == null || readValue.equalsIgnoreCase(CONSTANT_RAW_DATA_EMPTY_FIELD))) { voLineItem.setBillToFirstName(readValue);}     //4
        readValue=tokenizer.nextToken(); 
        if (!(readValue == null || readValue.equalsIgnoreCase(CONSTANT_RAW_DATA_EMPTY_FIELD))) { voLineItem.setBillToLastName(readValue);}     //5
        readValue=tokenizer.nextToken();
        if (!(readValue == null || readValue.equalsIgnoreCase(CONSTANT_RAW_DATA_EMPTY_FIELD))) { voLineItem.setBillToAddressLine1(readValue);}     //6
        readValue=tokenizer.nextToken(); 
        if (!(readValue == null || readValue.equalsIgnoreCase(CONSTANT_RAW_DATA_EMPTY_FIELD))) { voLineItem.setBillToAddressLine2(readValue);}     //7
        readValue=tokenizer.nextToken(); 
        if (!(readValue == null || readValue.equalsIgnoreCase(CONSTANT_RAW_DATA_EMPTY_FIELD))) { voLineItem.setBillToCity(readValue);}     //8
        readValue=tokenizer.nextToken(); 
        if (!(readValue == null || readValue.equalsIgnoreCase(CONSTANT_RAW_DATA_EMPTY_FIELD))) { voLineItem.setBillToState(readValue);}     //9
        readValue=tokenizer.nextToken(); 
        if (!(readValue == null || readValue.equalsIgnoreCase(CONSTANT_RAW_DATA_EMPTY_FIELD))) { voLineItem.setBillToZipCode(readValue);}     //10
        readValue=tokenizer.nextToken(); //Field not used : voLineItem.setBillToHomePhone(tokenizer.nextToken());//11
        readValue=tokenizer.nextToken(); 
        if (!(readValue == null || readValue.equalsIgnoreCase(CONSTANT_RAW_DATA_EMPTY_FIELD))) { voLineItem.setBillToWorkPhone(readValue);}     //12
        readValue=tokenizer.nextToken(); //Field not used : voLineItem.setBillToFaxNumber(tokenizer.nextToken());//13
        readValue=tokenizer.nextToken();
        if (!(readValue == null || readValue.equalsIgnoreCase(CONSTANT_RAW_DATA_EMPTY_FIELD))) { voLineItem.setBillToEmail(readValue);}     //14
        readValue=tokenizer.nextToken(); 
        if (!(readValue == null || readValue.equalsIgnoreCase(CONSTANT_RAW_DATA_EMPTY_FIELD))) { voLineItem.setBillToCountry(readValue);}     //15
        readValue=tokenizer.nextToken(); //Field not used : voLineItem.setCCType(tokenizer.nextToken());//16
        readValue=tokenizer.nextToken(); //Field not used : voLineItem.setCCNumber(tokenizer.nextToken());//17
        readValue=tokenizer.nextToken(); //Field not used : voLineItem.setCCExpirationDate(tokenizer.nextToken());     //18       
        readValue=tokenizer.nextToken(); //Field not used : voLineItem.setApprovalActionCode(tokenizer.nextToken());//19
        readValue=tokenizer.nextToken(); //Field not used : voLineItem.setCCAVSResult(tokenizer.nextToken());//20
        readValue=tokenizer.nextToken(); //Field not used : voLineItem.setCCApprovalAmount(tokenizer.nextToken());//21
        readValue=tokenizer.nextToken(); //Field not used : voLineItem.setACQreferenceDate(tokenizer.nextToken());//22
        readValue=tokenizer.nextToken(); //Field not used : voLineItem.setApprovalVerbiage(tokenizer.nextToken());//23
        readValue=tokenizer.nextToken(); //Field not used : voLineItem.setApprovalActionCode(tokenizer.nextToken());//24

        temp = tokenizer.nextToken();//25
        if (!(temp == null || temp.equalsIgnoreCase(CONSTANT_RAW_DATA_EMPTY_FIELD))) { voLineItem.setTaxAmount(new Float(temp).floatValue()); }

        
        temp = tokenizer.nextToken();//26
        if (!(temp == null || temp.equalsIgnoreCase(CONSTANT_RAW_DATA_EMPTY_FIELD))) { voLineItem.setOrderTotal(new Float(temp).floatValue());}//26

        tokenizer.nextToken(); //Field not used : voLineItem.setCoBrandedInfo(tokenizer.nextToken());//27

       
        temp = tokenizer.nextToken();//28
        if (!(temp == null || temp.equalsIgnoreCase(CONSTANT_RAW_DATA_EMPTY_FIELD))) { voOrder.setCreatedDate(sdf.parse(temp));}//28
        
        readValue=tokenizer.nextToken(); //Field not used : voLineItem.setGiftCertificateCount(tokenizer.nextToken());//29
        readValue=tokenizer.nextToken(); //Field not used : voLineItem.setGiftCertificateDetail(tokenizer.nextToken());   //30     
        readValue=tokenizer.nextToken(); //Field not used : voLineItem.setNewsletterFlag(tokenizer.nextToken());//31

        readValue=tokenizer.nextToken(); //32
        if (!(readValue == null || readValue.equalsIgnoreCase(CONSTANT_RAW_DATA_EMPTY_FIELD))) { voLineItem.setShipToFirstName(readValue);}     //32

        readValue=tokenizer.nextToken(); //33
        if (!(readValue == null || readValue.equalsIgnoreCase(CONSTANT_RAW_DATA_EMPTY_FIELD))) { voLineItem.setShipToLastName(readValue);}     //33
        
        readValue=tokenizer.nextToken(); //34 
        if (!(readValue == null || readValue.equalsIgnoreCase(CONSTANT_RAW_DATA_EMPTY_FIELD))) { voLineItem.setShipToAddressLine1(readValue);}     //34
        
        readValue=tokenizer.nextToken(); //35
        if (!(readValue == null || readValue.equalsIgnoreCase(CONSTANT_RAW_DATA_EMPTY_FIELD))) { voLineItem.setShipToAddressLine2(readValue);}     //35
      
        readValue=tokenizer.nextToken(); //36
        if (!(readValue == null || readValue.equalsIgnoreCase(CONSTANT_RAW_DATA_EMPTY_FIELD))) { voLineItem.setShipToCity(readValue);}     //36
        
        readValue=tokenizer.nextToken(); //37
        if (!(readValue == null || readValue.equalsIgnoreCase(CONSTANT_RAW_DATA_EMPTY_FIELD))) { voLineItem.setShipToState(readValue);}     //37
        
        readValue=tokenizer.nextToken(); //38
        if (!(readValue == null || readValue.equalsIgnoreCase(CONSTANT_RAW_DATA_EMPTY_FIELD))) { voLineItem.setShipToZipCode(readValue);}     //38
        
        readValue=tokenizer.nextToken(); //39
        if (!(readValue == null || readValue.equalsIgnoreCase(CONSTANT_RAW_DATA_EMPTY_FIELD))) { voLineItem.setShipToCountry(readValue);}     //39
        
        readValue=tokenizer.nextToken(); //Field not used : voLineItem.setShipToInternational(tokenizer.nextToken());//40
        readValue=tokenizer.nextToken(); //Field not used : voLineItem.setQMSAddressOne(tokenizer.nextToken());         //41
        readValue=tokenizer.nextToken(); //Field not used : voLineItem.setQMSAddressTwo(tokenizer.nextToken());//42
        readValue=tokenizer.nextToken(); //Field not used : voLineItem.setQMSCity(tokenizer.nextToken());//43
        readValue=tokenizer.nextToken(); //Field not used : voLineItem.setQMSState(tokenizer.nextToken());//44
        readValue=tokenizer.nextToken(); //Field not used : voLineItem.setQMSZip(tokenizer.nextToken());//45
        readValue=tokenizer.nextToken(); //Field not used : voLineItem.setQMSResultCode(tokenizer.nextToken());//46
        readValue=tokenizer.nextToken(); //Field not used : voLineItem.setQMSOverrideFlag(tokenizer.nextToken());//47
        readValue=tokenizer.nextToken(); //Field not used : voLineItem.setQMSFirmName(tokenizer.nextToken());//48
        readValue=tokenizer.nextToken(); //Field not used : voLineItem.setQMSLatitude(tokenizer.nextToken());//49
        readValue=tokenizer.nextToken(); //Field not used : voLineItem.setQMSLongitude(tokenizer.nextToken());//50
        readValue=tokenizer.nextToken(); //Field not used : voLineItem.setQMSUSPSRangeRecordType(tokenizer.nextToken());//51        

        readValue=tokenizer.nextToken(); //52
        if (!(readValue == null || readValue.equalsIgnoreCase(CONSTANT_RAW_DATA_EMPTY_FIELD))) { voLineItem.setShipToWorkPhone(readValue);}     //52
        
        readValue=tokenizer.nextToken(); //53
        if (!(readValue == null || readValue.equalsIgnoreCase(CONSTANT_RAW_DATA_EMPTY_FIELD))) { voLineItem.setShipToWorkExtension(readValue);}     //53
        
        readValue=tokenizer.nextToken(); //54
        if (!(readValue == null || readValue.equalsIgnoreCase(CONSTANT_RAW_DATA_EMPTY_FIELD))) { voLineItem.setShipToBusinessType(readValue);}     //54

        readValue=tokenizer.nextToken(); //55
        if (!(readValue == null || readValue.equalsIgnoreCase(CONSTANT_RAW_DATA_EMPTY_FIELD))) { voLineItem.setShipToBusinessName(readValue);}     //55

        readValue=tokenizer.nextToken(); //Field not used : voLineItem.setShipToTypeInfo(tokenizer.nextToken());//56

        voLineItem.setOccasion(tokenizer.nextToken());         //57
        readValue = voLineItem.getOccasion();
        
        voLineItem.setItemNumber(tokenizer.nextToken());//58
        readValue = voLineItem.getItemNumber();

        temp = tokenizer.nextToken();//59
        if (!(temp == null || temp.equalsIgnoreCase(CONSTANT_RAW_DATA_EMPTY_FIELD))) { voLineItem.setRetailPrice(new Float(temp).floatValue());}//59
        
        tokenizer.nextToken(); //Field not used : voLineItem.setColorSize(tokenizer.nextToken());//60        

        temp = tokenizer.nextToken();//61
        if (!(temp == null || temp.equalsIgnoreCase(CONSTANT_RAW_DATA_EMPTY_FIELD))) { voLineItem.setAddOnCount(new Integer(temp).intValue());}//61
        
        temp = tokenizer.nextToken();//62
        if (!(temp == null || temp.equalsIgnoreCase(CONSTANT_RAW_DATA_EMPTY_FIELD))) { voLineItem.setAddOnDetail(temp);}     //63       

        temp = tokenizer.nextToken();     //63       
        if (!(temp == null || temp.equalsIgnoreCase(CONSTANT_RAW_DATA_EMPTY_FIELD))) { voLineItem.setServiceFee(new Float(temp).floatValue());}     //63       
        
        temp = tokenizer.nextToken(); //64
        if (!(temp == null || temp.equalsIgnoreCase(CONSTANT_RAW_DATA_EMPTY_FIELD))) { voLineItem.setExtraShippingFee(new Float(temp).floatValue());} //64

        temp = tokenizer.nextToken();//65
//        if (!(temp == null || temp.equalsIgnoreCase(CONSTANT_RAW_DATA_EMPTY_FIELD))) { voLineItem.setDeliveryDate(sdf.parse(temp));}//65
        if (!(temp == null || temp.equalsIgnoreCase(CONSTANT_RAW_DATA_EMPTY_FIELD_FOR_DATE) || temp.equalsIgnoreCase(CONSTANT_RAW_DATA_EMPTY_FIELD))) { voLineItem.setDeliveryDate(sdf.parse(temp));}//65

        // Append Signature field (if any) to Card Message field.  Currently no distinction between these fields is necessary (WRT B2B application).
        StringBuffer cardMsg = new StringBuffer();
        readValue = tokenizer.nextToken();                    // 66 Card Message
        if (!(readValue == null || readValue.equalsIgnoreCase(CONSTANT_RAW_DATA_EMPTY_FIELD))) { cardMsg.append(readValue);}          // 66
        readValue = tokenizer.nextToken();                    // 67 Card Signature
        if (!(readValue == null || readValue.equalsIgnoreCase(CONSTANT_RAW_DATA_EMPTY_FIELD))) { 
          if (cardMsg.length() > 0) {cardMsg.append("  ");}   // Add space between message and signature
          cardMsg.append(readValue);                          // Now add signature
        }
        if (!(cardMsg.toString().equalsIgnoreCase(CONSTANT_RAW_DATA_EMPTY_FIELD))) { voLineItem.setCardMessage(cardMsg.toString());}  // 66 & 67

        readValue = tokenizer.nextToken(); //68
        if (!(readValue == null || readValue.equalsIgnoreCase(CONSTANT_RAW_DATA_EMPTY_FIELD))) { voLineItem.setSpecialInstructions(readValue);}     //68
        
        temp = tokenizer.nextToken(); //Field not used : voLineItem.setShippingMethod(tokenizer.nextToken());         //69
        temp = tokenizer.nextToken(); //70
        if (!(temp == null || temp.equalsIgnoreCase(CONSTANT_RAW_DATA_EMPTY_FIELD))) { voLineItem.setDropShipCharges(new Float(temp).floatValue());} //70
        readValue=tokenizer.nextToken(); //71
        if (!(readValue == null || readValue.equalsIgnoreCase(CONSTANT_RAW_DATA_EMPTY_FIELD))) { voLineItem.setBillToWorkPhoneExt(readValue);}     //71
        temp = tokenizer.nextToken(); //Field not used : voLineItem.setSecondDeliveryDate(tokenizer.nextToken());//72
        temp = tokenizer.nextToken(); //Field not used : voLineItem.setLastMinuteGiftFlag(tokenizer.nextToken());//73
        temp = tokenizer.nextToken(); //Field not used : voLineItem.setFOLindicator(tokenizer.nextToken());//74
        temp = tokenizer.nextToken(); //Field not used : voLineItem.setSocketDateTimeStamp(tokenizer.nextToken());//75                    
        temp = tokenizer.nextToken(); //Field not used : voLineItem.setLastMinuteGiftEmailAddress(tokenizer.nextToken());//76
        temp = tokenizer.nextToken(); //Field not used : voLineItem.setLastMinuteGiftEmailSignature(tokenizer.nextToken());  //77

        temp = tokenizer.nextToken();//78        
        if (!(temp == null || temp.equalsIgnoreCase(CONSTANT_RAW_DATA_EMPTY_FIELD))) {voLineItem.setMasterOrderNumber(temp);}       
        
        tokenizer.nextToken(); //Field not used : voLineItem.setSundayDelivery(tokenizer.nextToken());//79

        temp = tokenizer.nextToken();//80
        if (!(temp == null || temp.equalsIgnoreCase(CONSTANT_RAW_DATA_EMPTY_FIELD))) {voLineItem.setItemUNSPSCCode(temp);}
        
        temp = tokenizer.nextToken();//81
        if (!(temp == null || temp.equalsIgnoreCase(CONSTANT_RAW_DATA_EMPTY_FIELD))) {
            voLineItem.setItemDescription(stripHtml(temp)); //  Item Description - get rid of any HTML tags first
        }           
        
        voOrder.setPayloadID(tokenizer.nextToken());//82
        voOrder.setOriginalBuyerCookie(tokenizer.nextToken());//83 Original Buyer cookie
        voLineItem.setOriginalBuyerCookie(voOrder.getOriginalBuyerCookie());
        voOrder.setAsnNumber(tokenizer.nextToken());//84

        // Certain customers want a default UNSPSC code for all products.  If this is 
        // one of those customers, get the default and override the value from Novator.
        //
        ClientOptionsVO covo = ClientOptions.getClientOptionsForAsn(voOrder.getAsnNumber());
        if ((covo != null) && (covo.getDefaultUnspsc() != null)) {
            voLineItem.setItemUNSPSCCode(covo.getDefaultUnspsc());
            new Logger(LOG_CATEGORY_LINEITEMAPARSER).debug("Using default UNSPSC code: '" + covo.getDefaultUnspsc() +
                                                               "' for client: " + voOrder.getAsnNumber());
        // Otherwise check for other UNSPSC processing
        } else {

            //6/24/03 - K. Angelo
            //Added code to truncate the UNSPSC code for products if the customers requests only 8 digits.
            //The list of customers are in the B2B.xml file

            ResourceManager resourceManager = ResourceManager.getInstance();
            String UNSPSCTruncateList=  resourceManager.getProperty(PROPERTY_B2B_PROPERTY_FILE, PROPERTY_8_DIGIT_UNSPSC_CODE_LIST);    
            if (UNSPSCTruncateList.indexOf(voOrder.getAsnNumber()) >= 0)
            {
                //Get UNSPSC code
                String code = voLineItem.getItemUNSPSCCode();
                    try{
                    String TruncatedUNSPSCCode = (voLineItem.getItemUNSPSCCode()).substring(0,8);
                    voLineItem.setItemUNSPSCCode(TruncatedUNSPSCCode);
                            }
                    catch(Throwable t) {
                               String[] msg = new String[] {"Could not truncate UNSPSC Code.",rawLineItem};                                                                                            
                               new Logger(LOG_CATEGORY_LINEITEMAPARSER).error("Could not truncate UNSPSC Code: " + rawLineItem);
                    }
            }
        } 

        voOrder.setSupplierURL(tokenizer.nextToken());//85
        voLineItem.setAribaBuyerCookie(tokenizer.nextToken());//86 - Current Buyer Cookie of Punchout Session
        voOrder.setBuyerCookie(voLineItem.getAribaBuyerCookie());

        temp = tokenizer.nextToken(); //Field not used : 87
        temp = tokenizer.nextToken(); //Field not used : 88
        temp = tokenizer.nextToken(); //Field not used : 89
        temp = tokenizer.nextToken(); //Field not used : 90
        temp = tokenizer.nextToken(); //Field not used : 91
        temp = tokenizer.nextToken(); //Field not used : 92
        temp = tokenizer.nextToken(); //Field not used : 93
        temp = tokenizer.nextToken(); //Field not used : 94
        temp = tokenizer.nextToken(); //Field not used : 95
        temp = tokenizer.nextToken(); //Field not used : 96
        temp = tokenizer.nextToken(); //Field not used : 97
        temp = tokenizer.nextToken(); //Field not used : 98
        temp = tokenizer.nextToken(); //Field not used : 99
        temp = tokenizer.nextToken(); //Field not used : 100

        temp = tokenizer.nextToken();//101 >>>> This is the discount price!!
        if (!(temp == null || temp.equalsIgnoreCase(CONSTANT_RAW_DATA_EMPTY_FIELD))) { voLineItem.setItemPrice(new Float(temp).floatValue());}//101

       
        //Set quantity to one
        voLineItem.setItemQuantity(1);


        //if a country was obtained from Novator then convert the name into a country code
        if(voLineItem.getShipToCountry()  != null && voLineItem.getShipToCountry().length() > 2){
                            ResourceManager rm = ResourceManager.getInstance();
                            ICountryCodeDAO daoCountryCodes = null;
                            try{
                                daoCountryCodes = 
                                        (ICountryCodeDAO) rm.getImplementation(INTERFACE_COUNTRYCODEDAO);
                                }
                            catch(ResourceNotFoundException e){
                                    String[] msg = new String[] {"COUNTRYCODEDAO:" + e.toString()};                                                                                            
                                    new Logger(LOG_CATEGORY_LINEITEMAPARSER).error("parseData: " + e.toString());
                                    B2BMailer.getInstance().send(new LineItemParser(),"parseData()",ERROR_CODE_UNABLETOLOADSERVICE,msg,B2BMailer.ACTION2);
                                    throw new FTDApplicationException(ERROR_CODE_UNABLETOLOADSERVICE, e);    
                                }        
                            try{            
                                voLineItem.setShipToCountry(daoCountryCodes.retrieveCountryCodeByName(voLineItem.getShipToCountry().toUpperCase()));       

                               }
                            catch(Exception e){
                                    String[] msg = new String[] {"COUNTRYCODEDAO:" + e.toString()};                                                                                            
                                    new Logger(LOG_CATEGORY_LINEITEMAPARSER).error("parseData: " + e.toString());
                                    B2BMailer.getInstance().send(new LineItemParser(),"parseData()",ERROR_CODE_UNRECOVERABLE_EXCEPTION,msg,B2BMailer.ACTION2);
                                    throw new FTDApplicationException(ERROR_CODE_UNRECOVERABLE_EXCEPTION, e);    
                                }
            }//end if country code found

        //add item to order

        voOrder.addLineItem(voLineItem);
        
    }

    }
    catch(java.text.ParseException e){
       String[] msg = new String[] {e.toString(),rawLineItem};                                                                                            
       new Logger(LOG_CATEGORY_LINEITEMAPARSER).error("parseData: " + e.toString());
       B2BMailer.getInstance().send(new LineItemParser(), "sendToAriba()",ERROR_CODE_PARSINGSHOPPINGCARTDATA, msg,B2BMailer.ACTION2);
       throw new FTDApplicationException(ERROR_CODE_UNRECOVERABLE_EXCEPTION, e);
        }
    catch(Throwable e){
       String[] msg = new String[] {e.toString(),rawLineItem};                                                                                            
       new Logger(LOG_CATEGORY_LINEITEMAPARSER).error("parseData: " + e.toString());
       e.printStackTrace();
       B2BMailer.getInstance().send(new LineItemParser(), "sendToAriba()",ERROR_CODE_UNRECOVERABLE_EXCEPTION, msg,B2BMailer.ACTION2);
       throw new FTDApplicationException(ERROR_CODE_UNRECOVERABLE_EXCEPTION, e);
        }
        
    
    return voOrder;
}

}
