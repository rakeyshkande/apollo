package com.ftd.b2b.ariba.common.utils;

import com.ftd.framework.common.exceptions.ResourceNotFoundException;
import com.ftd.framework.common.exceptions.FTDApplicationException;
import com.ftd.framework.common.utilities.ResourceManager;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.b2b.constants.B2BConstants;
import com.ftd.b2b.ariba.common.valueobjects.ClientOptionsVO;
import com.ftd.b2b.ariba.integration.dao.IClientOptionsDAO;
import com.ftd.b2b.utils.mail.B2BMailer;

import java.util.Map;
import java.util.List;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Singleton class for retrieving client processing options from
 * the database.  The first access to getClientOptionsForAsn()
 * will create the singleton and populate the client options hash.
 */
public class ClientOptions implements B2BConstants {

  private static ClientOptions clientOptionsSo = null;
  private static Map clientOptionsHash = null;
  private ResourceManager resourceManager;

  
  /**
   * Private constructor for this singleton class.  The client options
   * are retrieved from the database and stored in a static hash to
   * avoid repeated queries.
   **/
  private ClientOptions() throws FTDApplicationException {
    resourceManager = ResourceManager.getInstance();    
    loadClientOptions();
  }

  
  /**
   * Public method for obtaining processing options for a particular client (ASN).
   * The singleton is created if necessary.
   */
  public static ClientOptionsVO getClientOptionsForAsn(String a_asn) throws FTDApplicationException {
    ClientOptions lco = ClientOptions.getInstance(); 
    return((ClientOptionsVO) lco.clientOptionsHash.get(a_asn));
  }

  
  /**
   * Public method for reloading client options from database.
   * The singleton is created if necessary.
   */
  public static void reloadClientOptions() throws FTDApplicationException {
    ClientOptions lco = ClientOptions.getInstance(); 
    lco.loadClientOptions();
  }


  /**
   * Private method for loading actual client options from database
   * into our static hash.
   **/
  private void loadClientOptions() throws FTDApplicationException {
    clientOptionsHash = new HashMap();
    IClientOptionsDAO clientOptionsDAO = null;
    Logger logManager = new Logger(LOG_CATEGORY_CLIENTOPTIONS_DAO); 
    logManager.debug("Loading/reloading client options from database");
    
    try {
      clientOptionsDAO = (IClientOptionsDAO) resourceManager.getImplementation(INTERFACE_CLIENTOPTIONSDAO);
    } catch (ResourceNotFoundException rnfe) {
      logManager.error("loadClientOptions: " + rnfe.toString());        
      B2BMailer.getInstance().send(this, "ClientOptions()", rnfe.toString(), B2BMailer.ACTION2);      
      throw new FTDApplicationException(ERROR_CODE_UNRECOVERABLE_EXCEPTION, rnfe);
    }

    List list = null;
    try {
      list = clientOptionsDAO.retrieve();        
    } catch(Throwable t) {
      logManager.error("loadClientOptions: " + t.toString());    
      B2BMailer.getInstance().send(this, "ClientOptions()", t.toString(), B2BMailer.ACTION2);              
      throw new FTDApplicationException(ERROR_CODE_UNRECOVERABLE_EXCEPTION, t);
    }

    if ((list != null) && (list.size() > 0)) {
      Iterator it = list.listIterator();
      while (it.hasNext()) {
        ClientOptionsVO covo = (ClientOptionsVO)it.next();
        if (covo != null) {
          clientOptionsHash.put(covo.getAsnNumber(), covo);
        }
      }
    }
  }


  /**
   * Method for obtaining a reference to this singleton class.
   * Note that this method does NOT have to be invoked directly since
   * getClientOptionsForAsn will invoke it.
   * @return ClientOptions object reference.
   **/
  public static ClientOptions getInstance() throws FTDApplicationException {
    if (clientOptionsSo == null) {
      clientOptionsSo = new ClientOptions();
    }
    return clientOptionsSo;
  }
  
}
