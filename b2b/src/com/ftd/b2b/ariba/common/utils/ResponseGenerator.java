package com.ftd.b2b.ariba.common.utils;

import java.util.GregorianCalendar;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;
import java.util.StringTokenizer;
import java.text.SimpleDateFormat;
import java.util.SimpleTimeZone;

import com.ftd.framework.common.utilities.ResourceManager;
import com.ftd.b2b.ariba.common.valueobjects.AribaRequestVO;
import com.ftd.b2b.constants.B2BConstants;

/**
 * Base class for generating a cXML document.
 * This class also contains methods for generating error responses.
 *
 * @author York Davis
 * @version 1.0 
 **/
public class ResponseGenerator implements B2BConstants
{

  private static final String GMT          = "GMT";
  private static final String CHICAGO_TIME = "America/Chicago";
  private ResourceManager resourceManager;
  
  public ResponseGenerator()
  {
      resourceManager = ResourceManager.getInstance();  
  }

  /**
   * Generates an error response.
   *
   * @param  A string indicating the error type.
   * @param  The AribaRequestVO object from which response parameters are taken.
   * @return The formatted cXML error response document.
   * @author York Davis
   **/
  public String generateErrorResponse(String errorType, AribaRequestVO aribaRequestVO)
  {
      StringBuffer sb = new StringBuffer(getXMLHeaders());
      sb.append("<cXML");

      if (aribaRequestVO != null)
      {
        HashMap params = (HashMap) aribaRequestVO.getParams();      
        String payloadID = (String) params.get(CONSTANT_PAYLOAD_ID);
        if (payloadID != null)
        {
          sb.append("  payloadID=\"");
          sb.append(payloadID);
          sb.append("\"");
        }
      }

      String error =  resourceManager.getProperty(PROPERTY_B2B_PROPERTY_FILE, errorType);  
      StringTokenizer tokenizer = new StringTokenizer(error, "|");
      String errorCode = tokenizer.nextToken();
      String errorText = tokenizer.nextToken();
      String errorMsg  = tokenizer.nextToken();
      
      sb.append(" timestamp=\"");
      sb.append(getTimestamp());
      sb.append("\">");
      sb.append("<Response>");
      sb.append("<Status code=\"");
      sb.append(errorCode);
      sb.append("\" text=\"");
      sb.append(errorText);
      sb.append("\">");
      sb.append(errorMsg);
      sb.append("</Status>");
      sb.append("</Response>");
      sb.append("</cXML>");
      return sb.toString();  
  }

  /**
   * Construct the Ariba timestamp.
   *
   * @return Timestamp string.
   * @author York Davis
   **/
  protected String getTimestamp()
  {
      GregorianCalendar cal = new GregorianCalendar();
      cal.setTime(new Date());

      StringBuffer sb = new StringBuffer();

      sb.append(Integer.toString(cal.get(Calendar.YEAR)));
      sb.append('-');
      sb.append(zeroPad(cal.get(Calendar.MONTH)+1));      
      sb.append('-');      
      sb.append(zeroPad(cal.get(Calendar.DATE)));            
      sb.append('T');
      sb.append(zeroPad(cal.get(Calendar.HOUR_OF_DAY)));                  
      sb.append(':');
      sb.append(zeroPad(cal.get(Calendar.MINUTE)));                        
      sb.append(':');
      sb.append(zeroPad(cal.get(Calendar.SECOND)));                        
      sb.append('-');
      sb.append(zeroPad(getHourDifferenceBetweenGMTandChicagoTime()));
      sb.append(":00");
      
      return sb.toString();
  }

  /**
   * Generates the XML Headers
   *
   * @return XML Headers
   * @author York Davis
   **/  
  protected String getXMLHeaders()
  {
       return  resourceManager.getProperty(PROPERTY_B2B_PROPERTY_FILE, 
                                           PROPERTY_CXMLHEADER);   
  }

  /**
   * Add a leading zeroes to field if one does not exist.
   *
   * @param  The integer value to be padded with a leading zero.
   * @return The integer converted to a String with a leading zero.
   * @author York Davis
   **/
  private String zeroPad(int i)
  {
      String val = Integer.toString(i);
      return i < 10 ? "0" + val : val;
  }

 /**
   * Calculates the number of hours that GreenwichMeanTime is ahead
   * of Chicago time.
   *
   * @return Hour difference.
   * @author York Davis
   **/
  private int getHourDifferenceBetweenGMTandChicagoTime()
  {
   	Calendar calGmt     = new GregorianCalendar(TimeZone.getTimeZone(GMT));
    Calendar calChicago = new GregorianCalendar(TimeZone.getTimeZone(CHICAGO_TIME));

		int result = (calGmt.get(Calendar.HOUR)) - calChicago.get(Calendar.HOUR);
		return result < 0 ? result + 12 : result;
  }

}