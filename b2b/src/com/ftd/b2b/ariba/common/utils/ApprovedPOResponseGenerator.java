package com.ftd.b2b.ariba.common.utils;

import com.ftd.b2b.ariba.common.valueobjects.OrderVO;
import com.ftd.b2b.ariba.common.valueobjects.LineItemVO;
import java.util.*;

/**
 * This class generates the Approved PR Response cXML
 * document to be sent to Ariba.
 */
public class ApprovedPOResponseGenerator extends ResponseGenerator
{

    


    /**
        Constructor
        */
    public ApprovedPOResponseGenerator()
    {
    }

    /**
     * Using the OrderVO, generates a cXML document 
     * in response to receiving an approved PO.
     * @params OrderVO containing order data
     */
     public String generateResponse(OrderVO voOrder)
     {

        //Encode data
        voOrder.encode();

        //Get line items
        List lines = voOrder.getLineItems();

        StringBuffer cxml = new StringBuffer("");;

        cxml.append(getXMLHeaders());
        cxml.append("<cXML payloadID=\"");
        cxml.append(voOrder.getPayloadID());
        cxml.append("\" timestamp=\"");
        cxml.append(getTimestamp());
        cxml.append("\">");
        
        cxml.append("<Response>");
        cxml.append("<Status code=\"");
        cxml.append(CONSTANT_PO_RECEIVED_SUCCESS_CODE);
        cxml.append("\" text=\"");
        cxml.append(CONSTANT_PO_RECEIVED_SUCCESS_TEXT);
        cxml.append("\"></Status>");
        cxml.append("</Response>");

        cxml.append("</cXML>");        
           

        return cxml.toString();
     }//end generate response
}