package com.ftd.b2b.ariba.common.utils;

import java.util.*;
import com.ftd.b2b.constants.B2BConstants;
import com.ftd.framework.common.utilities.ResourceManager;

public class Ping implements  B2BConstants
{
    
public String  doPing(boolean a_reloadClientOpts){

   // This used to open a socket and check if the B2B threads were running, but since they have
   // been removed, we simply return success (and reload client options if specified).
   //
   if (a_reloadClientOpts == true) {
      try {
         ClientOptions.reloadClientOptions();
         return "Client options were reloaded";
      } catch (Exception e) {
         e.printStackTrace();
         return "Error loading client options";         
      }
   } else 
   {
      return "The dude abides";
   }

}

public String doPing() {
    return doPing(false);  
}

}