package com.ftd.b2b.ariba.common.utils;



/**
 * Periodically wake up and look for rows on the Change_Cancel_Transmission table.
 * If rows are found, send them to the HP socket and delete each row from the table
 * after it is sent.
 *
 * @author York Davis
 * @version 1.0 
 **/
public interface IChangeCancelSender 
{
    /**
     * Called at server shutdown time to stop the monitor thread of this class.
     *
     * @author York Davis     
     */
     public void stopThread();
}
