package com.ftd.b2b.ariba.common.utils;

import java.util.List;
import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;
import java.util.StringTokenizer;

import com.ftd.b2b.constants.B2BConstants;
import com.ftd.b2b.ariba.common.valueobjects.OrderVO;
import com.ftd.b2b.ariba.common.valueobjects.LineItemVO;

/**
 * Utility class for generating Collection objects keyed by various
 * values.
 *
 * @author York Davis
 * @version 1.0 
 **/
public class MapGenerator implements B2BConstants
{
  /**
   * Build a Collection object from a String of Add-Ons.
   * AddOns generally have the format of "A:1,B:2". From this,
   * the "A" and "B" will be the keys while the "1" and "2" will
   * be the values.
   *
   * @param Add on String
   * @return Map of AddOns
   * @author York Davis
   **/
  public static Map buildAddOnMap(String addOnDetail)
  {
      String addOn    = "";
      String quantity = "";
      Map addOnMap = new HashMap();
      StringTokenizer itemTokenizer = new StringTokenizer(addOnDetail, CONSTANT_ADDON_ITEM_DELIMITER);
      while (itemTokenizer.hasMoreElements())
      {
          String addOnData = itemTokenizer.nextToken();
          StringTokenizer idTokenizer = new StringTokenizer(addOnData, CONSTANT_ADDON_QUANTITY_DELIMITER);
          addOn = idTokenizer.nextToken();
          quantity = idTokenizer.nextToken();
          addOnMap.put(addOn, quantity);
      }
      return addOnMap;
  }

  /**
   * Build a Collection object of LineItem data for an Order
   * The Collection will be keyed by Order Number.
   *
   * @param OrderVO object from which to build the Map of LineItems.
   * @return Map of Line Items keyed by Order Number.
   * @author York Davis
   **/
  public static Map getLineItemMapKeyedByOrderNumber(OrderVO orderVO)
  {
    Map map = new HashMap();

    Iterator iterator = orderVO.getLineItems().listIterator();
    LineItemVO lineItemVO = null;
    while (iterator.hasNext())
    {
        lineItemVO = (LineItemVO) iterator.next();
        map.put(lineItemVO.getOrderNumber(), lineItemVO);
    }
    return map;
  }

  /**
   * Build a Collection object of LineItem data for an Order
   * The Collection will be keyed by Order Number concatenated
   * with Item Number.
   *
   * @param OrderVO object from which to build the Map of LineItems.
   * @return Map of Line Items keyed by Order Number and Item Number.
   * @author York Davis
   **/
  public static Map getLineItemMapKeyedByOrderNumberAndItemNumber(OrderVO orderVO)
  {
    Map map = new HashMap();

    Iterator iterator = orderVO.getLineItems().listIterator();
    LineItemVO lineItemVO = null;
    while (iterator.hasNext())
    {
        lineItemVO = (LineItemVO) iterator.next();
        map.put(lineItemVO.getOrderNumber()+lineItemVO.getItemNumber(), lineItemVO);
    }
    return map;
  }
}