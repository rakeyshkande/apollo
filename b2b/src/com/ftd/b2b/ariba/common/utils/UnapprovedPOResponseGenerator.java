package com.ftd.b2b.ariba.common.utils;

import com.ftd.b2b.ariba.common.valueobjects.ShoppingCartOrderVO;
import com.ftd.b2b.ariba.common.valueobjects.ShoppingCartLineItemVO;
import java.util.List;
import com.ftd.b2b.common.utils.DataFormatter;
import com.ftd.b2b.utils.mail.B2BMailer;
import com.ftd.framework.common.exceptions.FTDApplicationException;
import com.ftd.framework.common.utilities.ResourceManager;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * This class generates the PunchOutOrderMessage cXML
 * document to be sent to Ariba.
 */
public class UnapprovedPOResponseGenerator extends ResponseGenerator
{
    protected static Logger logger = new Logger(UnapprovedPOResponseGenerator.class.getName());

    private static final String OPERATIONALLOWED = "edit";
    private static final String CURRENCY = "USD";
    private static final String QUANTITY = "1";
    private static final String LANG = "en";
    private static final String UNITOFMEASURE = "EA";
    private static final String DOMAIN = "UNSPSC";
    private static final String FROM_DOMAIN = "www.ftd.com";
    private static final String SENDER_DOMAIN = "www.ftd.com";
    private static final String SENDER_IDENTITY = "PunchoutResponse";
    private static final String SENDER_USER_AGENT = "FTD Buyer Services";
    private static final String NETWORK_ID = "NetworkId";
    /**
        Constructor
        */
    public UnapprovedPOResponseGenerator()
    {
    }

    /**
     * Using the OrderVO, generates a cXML document of
     * unapproved Purchase Orders.
     * @params OrderVO containing order data
     */
     public String generateResponse(ShoppingCartOrderVO voOrder)
     {

        //Encode data
        voOrder.encode();

        //Get line items
        List lines = voOrder.getLineItems();

        //Get FTD DUNS number
        ResourceManager rm = ResourceManager.getInstance();        
        String ftdDUNSNumber = "";
        try {
            ConfigurationUtil cu = ConfigurationUtil.getInstance();
            ftdDUNSNumber = cu.getFrpGlobalParm(B2B_ARIBA_CONFIG_CONTEXT, PROPERTY_FTD_DUNS_NUMBER);
        } catch(Exception e) {
          logger.error("send - Can't get DUNS property: " + e.toString());
        }

        // Get ASNs that we should include ship-to extrinsic info for
        String shipToExtrinsicsForAsn = rm.getProperty(PROPERTY_B2B_PROPERTY_FILE, PROPERTY_SHOPPING_RESP_HAS_SHIP_TO_INFO);

        //get one item from list...some values are consistent for all lines items
        ShoppingCartLineItemVO voLineItem;
        if (lines.size() > 0) {
            voLineItem = (ShoppingCartLineItemVO)lines.get(0);
            }
        else {
            voLineItem = new ShoppingCartLineItemVO();
            }

        StringBuffer cxml = new StringBuffer("");;

        cxml.append(getXMLHeaders());
        cxml.append("<cXML payloadID=\"");
        cxml.append(voOrder.getPayloadID());
        cxml.append("\" timestamp=\"");
        cxml.append(getTimestamp());
        cxml.append("\">");
        
        cxml.append("<Header>");
        cxml.append("<From>");
        cxml.append("<Credential domain = \"");
        cxml.append(FROM_DOMAIN);
        cxml.append("\">");
        cxml.append("<Identity>");
        cxml.append(ftdDUNSNumber);
        cxml.append("</Identity>");
        cxml.append("</Credential>");
        cxml.append("</From>");
        cxml.append("<To>");
        cxml.append("<Credential domain = \"");
        cxml.append(NETWORK_ID);
        cxml.append("\">");
        cxml.append("<Identity>");
        cxml.append(voOrder.getAsnNumber());
        cxml.append("</Identity>");
        cxml.append("</Credential>");
        cxml.append("</To>");
        cxml.append("<Sender>");
        cxml.append("<Credential domain = \"");
        cxml.append(SENDER_DOMAIN);
        cxml.append("\">");
        cxml.append("<Identity>");
        cxml.append(SENDER_IDENTITY);
        cxml.append("</Identity>");
        cxml.append("</Credential>");
        cxml.append("<UserAgent>");
        cxml.append(SENDER_USER_AGENT);
        cxml.append("</UserAgent>");
        cxml.append("</Sender>");
        cxml.append("</Header>");
        
        cxml.append("<Message>");
        cxml.append("<PunchOutOrderMessage>");
        cxml.append("<BuyerCookie>");
        cxml.append(voOrder.getBuyerCookie());
        cxml.append("</BuyerCookie>");
        cxml.append("<PunchOutOrderMessageHeader operationAllowed=\"");
        cxml.append(OPERATIONALLOWED);
        cxml.append("\">");
        cxml.append("<Total>");
        cxml.append("<Money currency=\"");
        cxml.append(CURRENCY);
        cxml.append("\">");
        cxml.append(voLineItem.getOrderTotal());
        cxml.append("</Money>");
        cxml.append("</Total>");
        cxml.append("</PunchOutOrderMessageHeader>");

        //for each line in the order add xml
        for (int i = 0; i <lines.size(); i++){

            voLineItem = (ShoppingCartLineItemVO)lines.get(i);
        
            cxml.append("<ItemIn quantity=\"");
            cxml.append(voLineItem.getItemQuantity());
            cxml.append("\">");
            cxml.append("<ItemID>");
            cxml.append("<SupplierPartID>");
            cxml.append(voLineItem.getItemNumber());
            cxml.append("</SupplierPartID>");
            cxml.append("<SupplierPartAuxiliaryID>");
            cxml.append(voLineItem.getOrderNumber());
            cxml.append("</SupplierPartAuxiliaryID>");
            cxml.append("</ItemID>");
            cxml.append("<ItemDetail>");
            cxml.append("<UnitPrice>");
            cxml.append("<Money currency=\"");
            cxml.append(CURRENCY);
            cxml.append("\">");
            cxml.append(DataFormatter.formatPrice(voLineItem.getItemPrice()));
            cxml.append(" </Money>");
            cxml.append("</UnitPrice>");
            cxml.append("<Description xml:lang=\"");
            cxml.append(LANG+"\">");
            cxml.append(voLineItem.getItemDescription());
            cxml.append("</Description>");
            cxml.append("<UnitOfMeasure>");
            cxml.append(UNITOFMEASURE);
            cxml.append("</UnitOfMeasure>");
            cxml.append("<Classification domain=\"");
            cxml.append(DOMAIN);
            cxml.append("\">");
            cxml.append(voLineItem.getItemUNSPSCCode());
            cxml.append("</Classification>");
            cxml.append("<ManufacturerName>");
            cxml.append("</ManufacturerName>");
            cxml.append("</ItemDetail>");
            
            // Logic to include ship-to information in extrinsic fields for 
            // certain clients (e.g., EDS)
            if (shipToExtrinsicsForAsn.indexOf(voOrder.getAsnNumber().toUpperCase()) > -1) {
                voLineItem.encode();  
                cxml.append("<Extrinsic name=\"ShipToFirstName\">");
                if (voLineItem.getShipToFirstName() != null) cxml.append(voLineItem.getShipToFirstName());
                cxml.append("</Extrinsic>");
                cxml.append("<Extrinsic name=\"ShipToLastName\">");
                if (voLineItem.getShipToLastName() != null) cxml.append(voLineItem.getShipToLastName());
                cxml.append("</Extrinsic>");
                cxml.append("<Extrinsic name=\"ShipToAddressLine1\">");
                if (voLineItem.getShipToAddressLine1() != null) cxml.append(voLineItem.getShipToAddressLine1());
                cxml.append("</Extrinsic>");
                cxml.append("<Extrinsic name=\"ShipToAddressLine2\">");
                if (voLineItem.getShipToAddressLine2() != null) cxml.append(voLineItem.getShipToAddressLine2());
                cxml.append("</Extrinsic>");
                cxml.append("<Extrinsic name=\"ShipToCity\">");
                if (voLineItem.getShipToCity() != null) cxml.append(voLineItem.getShipToCity());
                cxml.append("</Extrinsic>");
                cxml.append("<Extrinsic name=\"ShipToState\">");
                if (voLineItem.getShipToState() != null) cxml.append(voLineItem.getShipToState());
                cxml.append("</Extrinsic>");
                cxml.append("<Extrinsic name=\"ShipToZipCode\">");
                if (voLineItem.getShipToZipCode() != null) cxml.append(voLineItem.getShipToZipCode());
                cxml.append("</Extrinsic>");
                cxml.append("<Extrinsic name=\"ShipToCountry\">");
                if (voLineItem.getShipToCountry() != null) cxml.append(voLineItem.getShipToCountry());
                cxml.append("</Extrinsic>");
                cxml.append("<Extrinsic name=\"ShipToHomePhone\">");
                if (voLineItem.getShipToHomePhone() != null) cxml.append(voLineItem.getShipToHomePhone());
                cxml.append("</Extrinsic>");
                cxml.append("<Extrinsic name=\"ShipToWorkPhone\">");
                if (voLineItem.getShipToWorkPhone() != null) cxml.append(voLineItem.getShipToWorkPhone());
                cxml.append("</Extrinsic>");
                cxml.append("<Extrinsic name=\"ShipToWorkExtension\">");
                if (voLineItem.getShipToWorkExtension() != null) cxml.append(voLineItem.getShipToWorkExtension());
                cxml.append("</Extrinsic>");
                cxml.append("<Extrinsic name=\"ShipToEmail\">");
                if (voLineItem.getShipToEmail() != null) cxml.append(voLineItem.getShipToEmail());
                cxml.append("</Extrinsic>");
                cxml.append("<Extrinsic name=\"ShipToType\">");
                if (voLineItem.getShipToBusinessType() != null) cxml.append(voLineItem.getShipToBusinessType());
                cxml.append("</Extrinsic>");
                cxml.append("<Extrinsic name=\"ShipToTypeName\">");
                if (voLineItem.getShipToBusinessName() != null) cxml.append(voLineItem.getShipToBusinessName());
                cxml.append("</Extrinsic>");
            }

            cxml.append("</ItemIn>");    
        }//end for

        cxml.append("</PunchOutOrderMessage>");
        cxml.append("</Message>");     
        cxml.append("</cXML>");        
           

        return cxml.toString();
     }//end generate response
}