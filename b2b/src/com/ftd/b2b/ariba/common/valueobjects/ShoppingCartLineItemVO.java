package com.ftd.b2b.ariba.common.valueobjects;

/**
 * This VO contains the line items for the shopping cart
 * order VO.
 *
 * !! Currently there is no extra data in the shopping
 *    cart VO.  This VO was built in case extra fields are
 *    needed and to improve readability of the code.
 */ 
public class ShoppingCartLineItemVO extends LineItemVO
{
    private int itemQuantity;

    public int getItemQuantity(){
        return itemQuantity;
        }

    public void setItemQuantity(int newValue){
        itemQuantity = newValue;
        }        
}