package com.ftd.b2b.ariba.common.valueobjects;

/**
 * Value object class to encapsulate all information required to insert
 * to the Order Entry tables.
 *
 * @author York Davis
 * @version 1.0
 */
public class OrderEntryVO 
{
  private ApprovedLineItemVO lineItemVO;
  private String             asnNumber;
  private String             sourceCode;

  /**  
   * Return LineItemVO.
   *
   * @author York Davis
   */
  public ApprovedLineItemVO getLineItemVO()
  {
    return lineItemVO;
  }

  /**  
   * Set LineItemVO.
   *
   * @author York Davis
   */
  public void setLineItemVO(ApprovedLineItemVO newLineItemVO)
  { 
    lineItemVO = newLineItemVO;
  }

  /**  
   * Return ASN Number
   *
   * @author York Davis
   */
  public String getASNNumber()
  {
    return asnNumber;
  }

  /**  
   * Set ASN Number
   *
   * @author York Davis
   */
  public void setASNNumber(String newASNNumber)
  {
    asnNumber = newASNNumber;
  }

  /**  
   * Return Source Code
   *
   * @author York Davis
   */
  public String getSourceCode()
  {
    return sourceCode;
  }

  /**  
   * Set Source Code
   *
   * @author York Davis
   */
  public void setSourceCode(String newSourceCode)
  {
    sourceCode = newSourceCode;
  }
  
}