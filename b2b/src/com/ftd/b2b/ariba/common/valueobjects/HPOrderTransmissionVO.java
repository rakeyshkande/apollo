package com.ftd.b2b.ariba.common.valueobjects;

public class HPOrderTransmissionVO 
{
  int HPSequenceNumber;
  String HPSocketValue;

  public HPOrderTransmissionVO()
  {
  }

  public int getHPSequenceNumber()
  {
    return HPSequenceNumber;
  }

  public void setHPSequenceNumber(int newHPSequenceNumber)
  {
    HPSequenceNumber = newHPSequenceNumber;
  }

  public String getHPSocketValue()
  {
    return HPSocketValue;
  }

  public void setHPSocketValue(String newHPSocketValue)
  {
    HPSocketValue = newHPSocketValue;
  }
}