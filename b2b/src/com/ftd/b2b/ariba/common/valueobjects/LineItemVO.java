package com.ftd.b2b.ariba.common.valueobjects;
import java.util.Date;
import java.util.StringTokenizer;
import com.ftd.b2b.constants.B2BConstants;
import com.ftd.b2b.common.utils.DataEncoder;

/**
 * A value object for line items.
 */

/* 6/24/03
    Ed Mueller
    Added Shipping, Card Message, Special Instructions.  They are coming from Novator now. */
    
public class LineItemVO extends DataEncoder 
        implements B2BConstants, Cloneable, ILineItemVO
{

    private String orderNumber;
    private String aribaBuyerCookie;
    private String OriginalBuyerCookie;
    private float taxAmount;
    private float orderTotal;
    private String occasion;    
    private String itemNumber;
    private String itemDescription;
    private float itemPrice;
    private int addOnCount;
    private String addOnDetail;
    private float serviceFee;
    private Date deliveryDate;
    private String masterOrderNumber;
    private String itemUNSPSCCode;

/* 6/25/03 Ed Mueller
    These fields added to table. */
 private String ShipToFirstName;
 private   String ShipToLastName;
 private   String ShipToAddressLine1;
 private   String ShipToAddressLine2;
 private   String ShipToCity;
 private   String ShipToState;
 private   String ShipToZipCode;
 private   String ShipToCountry;
 private   String ShipToHomePhone;
 private   String ShipToWorkPhone;
 private   String ShipToWorkExtension;
 private   String ShipToFaxNumber;
 private   String ShipToEmail;
 private   String ShipToBusinessType;
 private   String ShipToBusinessName;
 private   String CardMessage;
 private   String SpecialInstructions;    
    private float discountPrice;
    private float RetailPrice;
/* 6/25/03..end*/

    // BillTo added for Spec #2255
    private String billToFirstName;
		private String billToLastName;
		private String billToAddressLine1;
		private String billToAddressLine2;
		private String billToCity;
		private String billToState;
		private String billToZipCode;
		private String billToWorkPhone;
		private String billToWorkPhoneExt;
		private String billToEmail;
		private String billToCountry;

    private float extraShippingFee;
    private float dropShipCharges;

    public LineItemVO() 
    {
    }




    /** 
     * Get number of line items.
     * @returns int
     */
    public int getNumberOfLineItems(){

     try{
       //parse order number for line ocunt
       String lineCount = "0";
       StringTokenizer tokenizer = new StringTokenizer(masterOrderNumber, CONSTANT_ORDER_NUMBER_DIVIDER);
       tokenizer.nextToken();//order number
       if (tokenizer.hasMoreElements()) {
           lineCount = tokenizer.nextToken();//line count
        }
                       
        return Integer.parseInt(lineCount);
        }
     catch(Throwable t){
        return -1;
        }
    }
    /** 
     * Get order Number value.
     * @returns String order Number
     */
    public String getOrderNumber(){
        return orderNumber;
        }
        
    /** Set value.
     * @parms String order Number value
     */
    public void setOrderNumber(String newValue){
        orderNumber = newValue;
        }

    /** 
     * Get value.
     * @returns String
     */
    public float getTaxAmount(){
        return taxAmount;
        }
        
    /** Set value.
     * @parms String value
     */
    public void setTaxAmount(float newValue){
        taxAmount = newValue;
        }

    /** 
     * Get value.
     * @returns String
     */
    public float getOrderTotal(){
        return orderTotal;
        }
        
    /** Set value.
     * @parms String value
     */
    public void setOrderTotal(float newValue){
        orderTotal= newValue;
        }

    /** 
     * Get value.
     * @returns String
     */
    public String getOccasion(){
        return occasion;
        }
        
    /** Set value.
     * @parms String value
     */
    public void setOccasion(String newValue){
        occasion= newValue;
        }

    /** 
     * Get value.
     * @returns String
     */
    public String getItemNumber(){
        return itemNumber;
        }
        
    /** Set value.
     * @parms String value
     */
    public void setItemNumber(String newValue){
        itemNumber= newValue;
        }

    /** 
     * Get value.
     * @returns float
     */
    public float getItemPrice(){
        return itemPrice;
        }
        
    /** Set value.
     * @parms float value
     */
    public void setItemPrice(float newValue){
       itemPrice = newValue;
        }

    /** 
     * Get value.
     * @returns Integer
     */
    public int getAddOnCount(){
        return addOnCount;
        }
        
    /** Set value.
     * @parms Integer value
     */
    public void setAddOnCount(int newValue){
        addOnCount= newValue;
        }

      /** 
     * Get value.
     * @returns String
     */
    public String getAddOnDetail(){
        return addOnDetail;
        }
        
    /** Set value.
     * @parms String value
     */
    public void setAddOnDetail(String newValue){
       addOnDetail = newValue;
        }

    /** 
     * Get value.
     * @returns String
     */
    public float getServiceFee(){
        return serviceFee;
        }
        
    /** Set value.
     * @parms String value
     */
    public void setServiceFee(float newValue){
       serviceFee = newValue;
        }

    /** 
     * Get value.
     * @returns Date
     */
    public Date getDeliveryDate(){
        return deliveryDate;
        }
        
    /** Set value.
     * @parms Date value
     */
    public void setDeliveryDate(Date newValue){
        deliveryDate= newValue;
        }
 
    /** 
     * Get value.
     * @returns String
     */
    public String getMasterOrderNumber(){
        return masterOrderNumber;
        }
        
    /** Set value.
     * @parms String value
     */
    public void setMasterOrderNumber(String newValue){
        masterOrderNumber= newValue;
        }

    /** 
     * Get value.
     * @returns String
     */
    public String getItemUNSPSCCode(){
        return itemUNSPSCCode;
        }
        
    /** Set value.
     * @parms String value
     */
    public void setItemUNSPSCCode(String newValue){
        itemUNSPSCCode= newValue;
        }

    /** 
     * Get value.
     * @returns String
     */
    public String getItemDescription(){
        return itemDescription;
        }
        
    /** Set value.
     * @parms String value
     */
    public void setItemDescription(String newValue){
        itemDescription= newValue;
        }

    /** 
     * Get value.
     * @returns String
     */
    public String getAribaBuyerCookie(){
        return aribaBuyerCookie;
        }
        
    /** Set value.
     * @parms String value
     */
    public void setAribaBuyerCookie(String newValue){
        aribaBuyerCookie= newValue;
        }

  /** 
     * Get value.
     * @returns String
     */
    public String getOriginalBuyerCookie(){
        return OriginalBuyerCookie;
        }
        
    /** Set value.
     * @parms String value
     */
    public void setOriginalBuyerCookie(String newValue){
        OriginalBuyerCookie= newValue;
        }

  
/**
 * Create a copy of this object.
 */
    public Object copy() throws java.lang.CloneNotSupportedException  {
        return super.clone();
    }

  /** Get Ship To First Name
     *@returns String containing Ship To First Name
     */
  public String getShipToFirstName()
  {
    return ShipToFirstName;
  }

  /** Set Ship To First Name
     *@parms string containing Ship To First Name
     */
    public void setShipToFirstName(String newShipToFirstName)
  {
    ShipToFirstName = newShipToFirstName;
  }

  /** Get Ship To Last Name
     *@returns String containing Ship To Last Name
     */
  public String getShipToLastName()
  {
    return ShipToLastName;
  }

  /** Set Ship To Last Name
     *@parms string containing Ship To Last Name
     */
    public void setShipToLastName(String newShipToLastName)
  {
    ShipToLastName = newShipToLastName;
  }

  /** Get Ship To Address Line 1
     *@returns String containing Ship To Address Line 1
     */
  public String getShipToAddressLine1()
  {
    return ShipToAddressLine1;
  }

  /** Set Ship To Address Line 1
     *@parms string containing Ship To Address Line 1
     */
    public void setShipToAddressLine1(String newShipToAddressLine1)
  {
    ShipToAddressLine1 = newShipToAddressLine1;
  }

  /** Get Ship To Address Line 2
     *@returns String containing Ship To Address Line 2
     */
  public String getShipToAddressLine2()
  {
    return ShipToAddressLine2;
  }

  /** Set Ship To Address Line 2
     *@parms string containing Ship To Address Line 2
     */
    public void setShipToAddressLine2(String newShipToAddressLine2)
  {
    ShipToAddressLine2 = newShipToAddressLine2;
  }

  /** Get Ship To City
     *@returns String containing Ship To City
     */
  public String getShipToCity()
  {
    return ShipToCity;
  }

  /** Set Ship To City
     *@parms string containing Ship To City
     */
    public void setShipToCity(String newShipToCity)
  {
    ShipToCity = newShipToCity;
  }

  /** Get Ship To State
     *@returns String containing Ship To State
     */
  public String getShipToState()
  {
    return ShipToState;
  }

  /** Set Ship To State
     *@parms string containing Ship To State
     */
    public void setShipToState(String newShipToState)
  {
    ShipToState = newShipToState;
  }

  /** Get Ship To Zip Code
     *@returns String containing Ship To Zip Code
     */
  public String getShipToZipCode()
  {
    return ShipToZipCode;
  }

  /** Set Ship To Zip Code
     *@parms string containing Ship To Zip Code
     */
    public void setShipToZipCode(String newShipToZipCode)
  {
    ShipToZipCode = newShipToZipCode;
  }

  /** Get Ship To Country
     *@returns String containing Ship To Country
     */
  public String getShipToCountry()
  {
    return ShipToCountry;
  }

  /** Set Ship To Country
     *@parms string containing Ship To Country
     */
    public void setShipToCountry(String newShipToCountry)
  {
    ShipToCountry = newShipToCountry;
  }

  /** Get Ship To Home Phone
     *@returns String containing Ship To Home Phone
     */
  public String getShipToHomePhone()
  {
    return ShipToHomePhone;
  }

  /** Set Ship To Home Phone
     *@parms string containing Ship To Home Phone
     */
    public void setShipToHomePhone(String newShipToHomePhone)
  {
    ShipToHomePhone = newShipToHomePhone;
  }

  /** Get Ship To Work Phone
     *@returns String containing Ship To Work Phone
     */
  public String getShipToWorkPhone()
  {
    return ShipToWorkPhone;
  }

  /** Set Ship To Work Phone
     *@parms string containing Ship To Work Phone
     */
    public void setShipToWorkPhone(String newShipToWorkPhone)
  {
    ShipToWorkPhone = newShipToWorkPhone;
  }

  /** Get Ship To Work Extension
     *@returns String containing Ship To Work Extension
     */
  public String getShipToWorkExtension()
  {
    return ShipToWorkExtension;
  }

  /** Set Ship To Work Extension
     *@parms string containing Ship To Work Extension
     */
    public void setShipToWorkExtension(String newShipToWorkExtension)
  {
    ShipToWorkExtension = newShipToWorkExtension;
  }

  /** Get Ship To Fax Number
     *@returns String containing Ship To Fax Number
     */
  public String getShipToFaxNumber()
  {
    return ShipToFaxNumber;
  }

  /** Set Ship To Fax Number
     *@parms string containing Ship To Fax Number
     */
    public void setShipToFaxNumber(String newShipToFaxNumber)
  {
    ShipToFaxNumber = newShipToFaxNumber;
  }

  /** Get Ship To Email
     *@returns String containing Ship To Email
     */
  public String getShipToEmail()
  {
    return ShipToEmail;
  }

  /** Set Ship To Email
     *@parms string containing Ship To Email
     */
    public void setShipToEmail(String newShipToEmail)
  {
    ShipToEmail = newShipToEmail;
  }

  /** Get Card Message
     *@returns String containing Card Message
     */
  public String getCardMessage()
  {
    return CardMessage;
  }

  /** Set Card Message
     *@parms string containing Card Message
     */
    public void setCardMessage(String newCardMessage)
  {
    CardMessage = newCardMessage;
  }

  /** Get Special Instructions
     *@returns String containing Special Instructions
     */
  public String getSpecialInstructions()
  {
    return SpecialInstructions;
  }

  /** Set Special Instructions
     *@parms string containing Special Instructions
     */
    public void setSpecialInstructions(String newSpecialInstructions)
  {
    SpecialInstructions = newSpecialInstructions;
  }

    public float getDiscountPrice()
    {
        return discountPrice;
    }

    public void setDiscountPrice(float newDiscountPrice)
    {
        discountPrice = newDiscountPrice;
    }

    public float getRetailPrice()
    {
        return RetailPrice;
    }

    public void setRetailPrice(float newRetailPrice)
    {
        RetailPrice = newRetailPrice;
    }

    public String getBillToFirstName() {return billToFirstName;}
		public String getBillToLastName() {return billToLastName;}
		public String getBillToAddressLine1() {return billToAddressLine1;}
		public String getBillToAddressLine2() {return billToAddressLine2;}
		public String getBillToCity() {return billToCity;}
		public String getBillToState() {return billToState;}
		public String getBillToZipCode() {return billToZipCode;}
		public String getBillToWorkPhone() {return billToWorkPhone;}
		public String getBillToWorkPhoneExt() {return billToWorkPhoneExt;}
		public String getBillToEmail() {return billToEmail;}
		public String getBillToCountry() {return billToCountry;}

		public void setBillToFirstName(String a_str) {billToFirstName = a_str;}
		public void setBillToLastName(String a_str) {billToLastName = a_str;}
		public void setBillToAddressLine1(String a_str) {billToAddressLine1 = a_str;}
		public void setBillToAddressLine2(String a_str) {billToAddressLine2 = a_str;}
		public void setBillToCity(String a_str) {billToCity = a_str;}
		public void setBillToState(String a_str) {billToState = a_str;}
		public void setBillToZipCode(String a_str) {billToZipCode = a_str;}
		public void setBillToWorkPhone(String a_str) {billToWorkPhone = a_str;}
		public void setBillToWorkPhoneExt(String a_str) {billToWorkPhoneExt = a_str;}
		public void setBillToEmail(String a_str) {billToEmail = a_str;}
		public void setBillToCountry(String a_str) {billToCountry = a_str;}

    public float getExtraShippingFee() {return extraShippingFee;}
    public void setExtraShippingFee(float a_float) {extraShippingFee = a_float;}
    public float getDropShipCharges() {return dropShipCharges;}
    public void setDropShipCharges(float a_float) {dropShipCharges = a_float;}

    public void setShipToBusinessType(String shipToType) {
        this.ShipToBusinessType = shipToType;
    }

    public String getShipToBusinessType() {
        return ShipToBusinessType;
    }

    public void setShipToBusinessName(String shipToBusinessName) {
        this.ShipToBusinessName = shipToBusinessName;
    }

    public String getShipToBusinessName() {
        return ShipToBusinessName;
    }
}
