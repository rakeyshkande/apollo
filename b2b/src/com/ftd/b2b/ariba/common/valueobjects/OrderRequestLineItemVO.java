package com.ftd.b2b.ariba.common.valueobjects;
import java.util.*;
import java.util.Date;

/**
 * VO which contains data from sent to FTD from an Ariba
 * approved order transmission.
 */
public class OrderRequestLineItemVO extends LineItemVO implements Cloneable
{
  private String comments;
  private int quantity;
  private String BillToFirstName;
  private String BillToLastName;  
  private String BillToAddressLine1;
  private String BillToAddressLine2;
  private String BillToCity;
  private String BillToState;
  private String BillToZipCode;
  private String BillToCountry;
  private String BillToHomePhone;
  private String BillToWorkPhone;
  private String BillToWorkExtension;
  private String BillToFaxNumber;
  private String BillToEmail;

  private String aribaPONumber;
  private String approvedDate;
  private String partId;
  private int    aribaLineNumber = 0;
  private String aribaCostCenter;
  private String AMSProjectCode;
  public String PCardNumber;
  public Date PCardExpirationDate;
  public String PCardName;

  private String costCntrAccount;
  private String costCntrLocation;
  private String costCntrExpense;

  // Hash for optional/extra client-specific order info
  private HashMap lineItemExtensions;


  public String getAribaCostCenter()
  {
      return aribaCostCenter;
  }

  public void setAribaCostCenter(String newAribaCostCenter)
  {
      aribaCostCenter = newAribaCostCenter;
  }

  public String getCostCntrAccount()
  {
      return costCntrAccount;
  }

  public void setCostCntrAccount(String a_value)
  {
      costCntrAccount = a_value;
  }

  public String getCostCntrLocation()
  {
      return costCntrLocation;
  }

  public void setCostCntrLocation(String a_value)
  {
      costCntrLocation = a_value;
  }

  public String getCostCntrExpense()
  {
      return costCntrExpense;
  }

  public void setCostCntrExpense(String a_value)
  {
      costCntrExpense = a_value;
  }

  public String getAMSProjectCode()
  {
      return AMSProjectCode;
  }

  public void setAMSProjectCode(String newAMSProjectCode)
  {
      AMSProjectCode = newAMSProjectCode;
  }
  
  public int getAribaLineNumber()
  {
      return aribaLineNumber;
  }

  public void setAribaLineNumber(int newAribaLineNumber)
  {
      aribaLineNumber = newAribaLineNumber;
  }
  
  public String getPartId()
  {
    return partId;
  }

  public void setPartId(String newPartId)
  {
    partId = newPartId;
  }
  
  public String getAribaPONumber()
  {
      return aribaPONumber;
  }

  public void setAribaPONumber(String newAribaPONumber)
  {
      aribaPONumber = newAribaPONumber;
  }
  
  public String getApprovedDate()
  {
      return approvedDate;
  }

  public void setApprovedDate(String newApprovedDate)
  {
      approvedDate = newApprovedDate;
  }
  
  public String getBillToFirstName()
  {
    return BillToFirstName;
  }

  public void setBillToFirstName(String newBillToFirstName)
  {
    BillToFirstName = newBillToFirstName;
  }

  public String getBillToLastName()
  {
    return BillToLastName;
  }

  public void setBillToLastName(String newBillToLastName)
  {
    BillToLastName = newBillToLastName;
  }

  public String getBillToAddressLine1()
  {
    return BillToAddressLine1;
  }

  public void setBillToAddressLine1(String newBillToAddressLine1)
  {
    BillToAddressLine1 = newBillToAddressLine1;
  }

  public String getBillToAddressLine2()
  {
    return BillToAddressLine2;
  }

  public void setBillToAddressLine2(String newBillToAddressLine2)
  {
    BillToAddressLine2 = newBillToAddressLine2;
  }

  public String getBillToCity()
  {
    return BillToCity;
  }

  public void setBillToCity(String newBillToCity)
  {
    BillToCity = newBillToCity;
  }

  public String getBillToState()
  {
    return BillToState;
  }

  public void setBillToState(String newBillToState)
  {
    BillToState = newBillToState;
  }

  public String getBillToZipCode()
  {
    return BillToZipCode;
  }

  public void setBillToZipCode(String newBillToZipCode)
  {
    BillToZipCode = newBillToZipCode;
  }

  public String getBillToCountry()
  {
    return BillToCountry;
  }

  public void setBillToCountry(String newBillToCountry)
  {
    BillToCountry = newBillToCountry;
  }

  public String getBillToHomePhone()
  {
    return BillToHomePhone;
  }

  public void setBillToHomePhone(String newBillToHomePhone)
  {
    BillToHomePhone = newBillToHomePhone;
  }

  public String getBillToWorkPhone()
  {
    return BillToWorkPhone;
  }

  public void setBillToWorkPhone(String newBillToWorkPhone)
  {
    BillToWorkPhone = newBillToWorkPhone;
  }

  public String getBillToWorkExtension()
  {
    return BillToWorkExtension;
  }

  public void setBillToWorkExtension(String newBillToWorkExtension)
  {
    BillToWorkExtension = newBillToWorkExtension;
  }

  public String getBillToFaxNumber()
  {
    return BillToFaxNumber;
  }

  public void setBillToFaxNumber(String newBillToFaxNumber)
  {
    BillToFaxNumber = newBillToFaxNumber;
  }

  public String getBillToEmail()
  {
    return BillToEmail;
  }

  public void setBillToEmail(String newBillToEmail)
  {
    BillToEmail = newBillToEmail;
  }


  /** 
   * Get Value
   * @returns value
   */
  public String getComments(){
    return comments;
    }

  /** 
   * Set Value
   * @params new value
   */
  public void setComments(String newValue){
     comments = newValue;
    }    



  /** 
   * Get Value
   * @returns value
   */
  public int getQuantity(){
    return quantity;
    }

  /** 
   * Set Value
   * @params new value
   */
  public void setQuantity(int newValue){
     quantity = newValue;
    } 
    
  public Object copy() throws CloneNotSupportedException
  {
      return this.clone();
  }

  public String getPCardNumber()
  {
    return PCardNumber;
  }

  public void setPCardNumber(String newPCardNumber)
  {
    PCardNumber = newPCardNumber;
  }

  public Date getPCardExpirationDate()
  {
    return PCardExpirationDate;
  }

  public void setPCardExpirationDate(Date newPCardExpirationDate)
  {
    PCardExpirationDate = newPCardExpirationDate;
  }

  public String getPCardName()
  {
    return PCardName;
  }

  public void setPCardName(String newPCardName)
  {
    PCardName = newPCardName;
  }

  /** Get Line-Item Extensions hash.
   *  This is used for optional/extra client-specific order info.
   */
  public HashMap getLineItemExtensions() {
    return lineItemExtensions;
  }

  /** Set Line-Item Extensions hash.
   *  This is used for optional/extra client-specific order info.
   */
  public void setLineItemExtensions(HashMap a_val) {
    lineItemExtensions = a_val;
  }
  
}