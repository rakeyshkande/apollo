package com.ftd.b2b.ariba.common.valueobjects;

/**
 * Value object for storing the data for the ChangeCancelTransmission
 * table.
 *
 * @author York Davis
 * @version 1.0 
 **/
public class ChangeCancelTransmissionVO 
{
  private String changeCancelMsg;
  private int    sequence;

  /**
   * Returns the ChangeCancel message.
   *    
   * @return Change/Cancel Message
   * @author York Davis
   **/  
  public String getChangeCancelMsg()
  {
      return changeCancelMsg;
  }

  /**
   * Sets the ChangeCancel message.
   *    
   * @param Change/Cancel Message
   * @author York Davis
   **/  
  public void setChangeCancelMsg(String newChangeCancelMsg)
  {
      changeCancelMsg = newChangeCancelMsg;
  }
  
  /**
   * Returns the sequence number.
   *    
   * @return sequence number.
   * @author York Davis
   **/  
  public int getSequence()
  {
      return sequence;
  }

  /**
   * Sets the sequence number.
   *    
   * @param sequence number
   * @author York Davis
   **/  
  public void setSequence(int newSequence)
  {
      sequence = newSequence;
  }
  
}