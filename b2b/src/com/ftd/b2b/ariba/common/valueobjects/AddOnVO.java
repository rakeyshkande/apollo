package com.ftd.b2b.ariba.common.valueobjects;

/**
 * A value object for Add Ons
 */
public class AddOnVO
{
  String AddOnId;
  String AddOnType;
  String AddOnDescription;
  float AddOnPrice;
  String AddOnUNSPSC;

  public AddOnVO()
  {
  }

  /** Get Add On Id
   * @returns String containing Add On Id
   */
  public String getAddOnId()
  {
    return AddOnId;
  }

  /** Set Add On Id
   * @parms String containing Add On Id
   */
  public void setAddOnId(String newAddOnId)
  {
    AddOnId = newAddOnId;
  }

  /** Get Add On Type
   * @returns String containing Add On Type
   */
   public String getAddOnType()
  {
    return AddOnType;
  }

  /** Set Add On Type
   * @parms String containing Add On Type
   */
  public void setAddOnType(String newAddOnType)
  {
    AddOnType = newAddOnType;
  }

  /** Get Add On Description
   * @returns String containing Add On Description
   */
  public String getAddOnDescription()
  {
    return AddOnDescription;
  }

  /** Set Add On Description
   * @parms String containing Add On Description
   */
  public void setAddOnDescription(String newAddOnDescription)
  {
    AddOnDescription = newAddOnDescription;
  }

  /** Get Add On Price
   * @returns float containing Add On Price
   */
  public float getAddOnPrice()
  {
    return AddOnPrice;
  }

  /** Set Add On Price
   * @parms float containing Add On Price
   */
  public void setAddOnPrice(float newAddOnPrice)
  {
    AddOnPrice = newAddOnPrice;
  }
  
    /** Get Add On UNSPSC
   * @returns String containing Add On UNSPSC
   */
  public String getAddOnUNSPSC()
  {
    return AddOnUNSPSC;
  }

  /** Set Add On UNSPSC
   * @parms String containing Add On UNSPSC
   */
  public void setAddOnUNSPSC(String newAddOnUNSPSC)
  {
    AddOnUNSPSC = newAddOnUNSPSC;
  }
}