package com.ftd.b2b.ariba.common.valueobjects;

/**
 * A value object for Client Options.
 * This object represents special processing options (if any) for
 * clients.  Values originate from the b2b.client_options database table.
 */
 
public class ClientOptionsVO 
{
  private String asnNumber;
  private String defaultUnspsc;
  private String serviceFeeUnspsc;
  private String taxUnspsc;

  public ClientOptionsVO() {
  }

  public String getAsnNumber() {
    return asnNumber;
  }
  public void setAsnNumber(String a_value) {
    asnNumber = a_value;
  }

  public String getDefaultUnspsc() {
    return defaultUnspsc;
  }
  public void setDefaultUnspsc(String a_value) {
    defaultUnspsc = a_value;
  }

  public String getServiceFeeUnspsc() {
    return serviceFeeUnspsc;
  }
  public void setServiceFeeUnspsc(String a_value) {
    serviceFeeUnspsc = a_value;
  }

  public String getTaxUnspsc() {
    return taxUnspsc;
  }
  public void setTaxUnspsc(String a_value) {
    taxUnspsc = a_value;
  }
}