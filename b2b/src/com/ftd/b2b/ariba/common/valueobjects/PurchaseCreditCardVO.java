package com.ftd.b2b.ariba.common.valueobjects;
import java.util.Date;

/**
 * A value object for Purchase Credit Cards
 */
 
public class PurchaseCreditCardVO 
{
  public String CreditCardNumber;
  public Date ExpirationDate;
  public String CreditCardName;

  public PurchaseCreditCardVO()
  {
  }

  /** Get Credit Card Number
   * @returns String containing Credit Card Number
   */
  public String getCreditCardNumber()
  {
    return CreditCardNumber;
  }

  /** Set Credit Card Number
   * @parms String containing Credit Card Number
   */
  public void setCreditCardNumber(String newCreditCardNumber)
  {
    CreditCardNumber = newCreditCardNumber;
  }

  /** Get Expiration Date
   * @returns String containing Expiration Date
   */
  public Date getExpirationDate()
  {
    return ExpirationDate;
  }

  /** Set Expiration Date
   * @parms String containing Expiration Date
   */
  public void setExpirationDate(Date newExpirationDate)
  {
    ExpirationDate = newExpirationDate;
  }

  /** Get Credit Card Name
   * @returns String containing Credit Card Name
   */
  public String getCreditCardName()
  {
    return CreditCardName;
  }

  /** Set CreditCardName
   * @parms String containing Credit Card Name
   */
  public void setCreditCardName(String newCreditCardName)
  {
    CreditCardName = newCreditCardName;
  }
}