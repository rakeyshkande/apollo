package com.ftd.b2b.ariba.common.valueobjects;

/**
 *  Value object for Phone numbers.
 */
public class PhoneVO 
{
  private String homePhone = "";
  private String workPhone = "";
  private String faxPhone  = "";

  public String getHomePhone()
  {
    return homePhone;
  }

  public void setHomePhone(String newHomePhone)
  {
    homePhone = newHomePhone;
  }

  public String getWorkPhone()
  {
    return workPhone;
  }

  public void setWorkPhone(String newWorkPhone)
  {
    workPhone = newWorkPhone;
  }

  public String getFaxPhone()
  {
    return faxPhone;
  }

  public void setFaxPhone(String newFaxPhone)
  {
    faxPhone = newFaxPhone;
  }
}