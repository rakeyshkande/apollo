package com.ftd.b2b.ariba.common.valueobjects;

/**
 * Value object used to hold data for parsing a name into
 * a first name, last name component.
 *
 * @author York Davis
 * @version 1.0 
 **/
public class ParsedNameVO
{
  private String firstName;
  private String lastName;

  /**
   * Sets the first name.
   *
   * @param First name
   * @author York Davis
   **/
  public void setFirstName(String firstName)
  {
      this.firstName = firstName;
  }

  /**
   * Sets the last name.
   *
   * @param Last name
   * @author York Davis
   **/
  public void setLastName(String lastName)
  {
      this.lastName = lastName;
  }

  /**
   * Returns the first name.
   *
   * @return First name.
   * @author York Davis
   **/
  public String getFirstName()
  {
      return firstName;
  }

  /**
   * Returns the last name.
   *
   * @return Last name.
   * @author York Davis
   **/
  public String getLastName()
  {
      return lastName;
  }
}


