package com.ftd.b2b.ariba.common.valueobjects;
import java.util.*;

/**
 * VO which contains data from sent to FTD from an Ariba
 * shopping cart request.
 */
public class ShoppingCartOrderVO extends OrderVO implements Cloneable
{
  private String supplierURL;


  /** 
   * Get Value
   * @returns value
   */
  public String getSupplierURL(){
    return supplierURL;
    }

  /** 
   * Set Value
   * @params new value
   */
  public void setSupplierURL(String newValue){
     supplierURL = newValue;
    } 

    /**
 * Create a copy of this object.
 */
    public Object copy() throws java.lang.CloneNotSupportedException  {
        return super.clone();
    }

}