package com.ftd.b2b.ariba.common.valueobjects;

import java.util.*;

/**
 * A value object for approved order line items
 * This value object extends the base line item value object
 */

/* 6/24/03
    Ed Mueller
    Removed Shipping, Card Message, Special Instructions.  These now exist on the 
    LineItemVO...they are coming from Novator now. */

public class ApprovedLineItemVO extends LineItemVO
{
  String AribaPONumber;
  int AribaLineNumber;
  String BillToFirstName;
  String BillToLastName;
  String BillToAddressLine1;
  String BillToAddressLine2;
  String BillToCity;
  String BillToState;
  String BillToZipCode;
  String BillToCountry;
  String BillToHomePhone;
  String BillToWorkPhone;
  String BillToWorkExtension;
  String BillToFaxNumber;
  String BillToEmail;

  String AribaCostCenter;
  String AMSProjectCode;
  String PCardNumber;
  Date PCardExpirationDate;
  String PCardName;
  
  private String costCntrAccount;
  private String costCntrLocation;
  private String costCntrExpense;

  // Hash for optional/extra client-specific order info
  private HashMap lineItemExtensions;

  
  public ApprovedLineItemVO()
  {
  }

  /** Get Ariba PO Number
     *@returns String containing Ariba PO Number
     */
  public String getAribaPONumber()
  {
    return AribaPONumber;
  }

  /** Set Ariba PO Number
     *@parms string containing Ariba PO  Number
     */
    public void setAribaPONumber(String newAribaPONumber)
  {
    AribaPONumber = newAribaPONumber;
  }

  /** Get Ariba Line Number
     *@returns Integer containing Ariba Line Number
     */
  public int getAribaLineNumber()
  {
    return AribaLineNumber;
  }

  /** Set Ariba Line Number
     *@parms string containing Ariba Line Number
     */
    public void setAribaLineNumber(int newAribaLineNumber)
  {
    AribaLineNumber = newAribaLineNumber;
  }

  /** Get Bill To First Name
     *@returns String containing Bill To First Name
     */
  public String getBillToFirstName()
  {
    return BillToFirstName;
  }

  /** Set Bill To First Name
     *@parms string containing Bill To First Name
     */
    public void setBillToFirstName(String newBillToFirstName)
  {
    BillToFirstName = newBillToFirstName;
  }

  /** Get Bill To Last Name
     *@returns String containing Bill To Last Name
     */
  public String getBillToLastName()
  {
    return BillToLastName;
  }

  /** Set Bill To Last Name
     *@parms string containing Bill To Last Name
     */
    public void setBillToLastName(String newBillToLastName)
  {
    BillToLastName = newBillToLastName;
  }

  /** Get Bill To Address Line 1
     *@returns String containing Bill To Address Line 1
     */
  public String getBillToAddressLine1()
  {
    return BillToAddressLine1;
  }

  /** Set Bill To Address Line 1
     *@parms string containing Bill To Address Line 1
     */
    public void setBillToAddressLine1(String newBillToAddressLine1)
  {
    BillToAddressLine1 = newBillToAddressLine1;
  }

  /** Get Bill To Address Line 2
     *@returns String containing Bill To Address Line 2
     */
    public String getBillToAddressLine2()
  {
    return BillToAddressLine2;
  }

  /** Set Bill To Address Line 2
     *@parms string containing Bill To Address Line 2
     */
    public void setBillToAddressLine2(String newBillToAddressLine2)
  {
    BillToAddressLine2 = newBillToAddressLine2;
  }

  /** Get Bill To City
     *@returns String containing Bill To City
     */
  public String getBillToCity()
  {
    return BillToCity;
  }

  /** Set Bill To City
     *@parms string containing Bill To City
     */
    public void setBillToCity(String newBillToCity)
  {
    BillToCity = newBillToCity;
  }

  /** Get Bill To State
     *@returns String containing Bill To State
     */
  public String getBillToState()
  {
    return BillToState;
  }

  /** Set Bill To State
     *@parms string containing Bill To State
     */
    public void setBillToState(String newBillToState)
  {
    BillToState = newBillToState;
  }

  /** Get Bill To Zip Code
     *@returns String containing Bill To Zip Code
     */
  public String getBillToZipCode()
  {
    return BillToZipCode;
  }

  /** Set Bill To Zip Code
     *@parms string containing Bill To Zip Code
     */
    public void setBillToZipCode(String newBillToZipCode)
  {
    BillToZipCode = newBillToZipCode;
  }

  /** Get Bill To Country
     *@returns String containing Bill To Country
     */
  public String getBillToCountry()
  {
    return BillToCountry;
  }

  /** Set Bill To Country
     *@parms string containing Bill To Country
     */
    public void setBillToCountry(String newBillToCountry)
  {
    BillToCountry = newBillToCountry;
  }

  /** Get Bill To Home Phone
     *@returns String containing Bill To Phone
     */
  public String getBillToHomePhone()
  {
    return BillToHomePhone;
  }

  /** Set Bill To Home Phone
     *@parms string containing Bill To Home Phone
     */
    public void setBillToHomePhone(String newBillToHomePhone)
  {
    BillToHomePhone = newBillToHomePhone;
  }

  /** Get Bill To Work Phone
     *@returns String containing Bill To Work Phone
     */
  public String getBillToWorkPhone()
  {
    return BillToWorkPhone;
  }

  /** Set Bill To Work Phone
     *@parms string containing Bill To Work Phone
     */
    public void setBillToWorkPhone(String newBillToWorkPhone)
  {
    BillToWorkPhone = newBillToWorkPhone;
  }

  /** Get Bill To Work Extension
     *@returns String containing Bill To Work Extension
     */
  public String getBillToWorkExtension()
  {
    return BillToWorkExtension;
  }

  /** Set Bill To Work Extension
     *@parms string containing Bill To Work Extension
     */
    public void setBillToWorkExtension(String newBillToWorkExtension)
  {
    BillToWorkExtension = newBillToWorkExtension;
  }

  /** Get Bill To Fax Number
     *@returns String containing Bill To Fax Number
     */
  public String getBillToFaxNumber()
  {
    return BillToFaxNumber;
  }

  /** Set Bill To Fax Number
     *@parms string containing Bill To Fax Number
     */
    public void setBillToFaxNumber(String newBillToFaxNumber)
  {
    BillToFaxNumber = newBillToFaxNumber;
  }

  /** Get Bill To Email
     *@returns String containing Bill To Email
     */
  public String getBillToEmail()
  {
    return BillToEmail;
  }

  /** Set Bill To Email
     *@parms string containing Bill To Email
     */
    public void setBillToEmail(String newBillToEmail)
  {
    BillToEmail = newBillToEmail;
  }



  /** Get Ariba Cost Center
     *@returns String containing Ariba Cost Center
     */
  public String getAribaCostCenter()
  {
    return AribaCostCenter;
  }

  /** Set Ariba Cost Center
     *@parms string containing Ariba Cost Center
     */
    public void setAribaCostCenter(String newAribaCostCenter)
  {
    AribaCostCenter = newAribaCostCenter;
  }

    /** Get AMS Project Code
     *@returns String containing AMS Project Code
     */
  public String getAMSProjectCode()
  {
    return AMSProjectCode;
  }

  public String getCostCntrAccount()
  {
      return costCntrAccount;
  }

  public void setCostCntrAccount(String a_value)
  {
      costCntrAccount = a_value;
  }

  public String getCostCntrLocation()
  {
      return costCntrLocation;
  }

  public void setCostCntrLocation(String a_value)
  {
      costCntrLocation = a_value;
  }

  public String getCostCntrExpense()
  {
      return costCntrExpense;
  }

  public void setCostCntrExpense(String a_value)
  {
      costCntrExpense = a_value;
  }

  /** Set AMS Project Code
     *@parms string containing AMS Project Code
     */
    public void setAMSProjectCode(String newAMSProjectCode)
  {
    AMSProjectCode = newAMSProjectCode;
  }

    public String getPCardNumber()
  {
    return PCardNumber;
  }

  public void setPCardNumber(String newPCardNumber)
  {
    PCardNumber = newPCardNumber;
  }

  public Date getPCardExpirationDate()
  {
    return PCardExpirationDate;
  }

  public void setPCardExpirationDate(Date newPCardExpirationDate)
  {
    PCardExpirationDate = newPCardExpirationDate;
  }

  public String getPCardName()
  {
    return PCardName;
  }

  public void setPCardName(String newPCardName)
  {
    PCardName = newPCardName;
  }


  /** Get Line-Item Extensions hash.
   *  This is used for optional/extra client-specific order info.
   */
  public HashMap getLineItemExtensions() {
    return lineItemExtensions;
  }

  /** Set Line-Item Extensions hash.
   *  This is used for optional/extra client-specific order info.
   */
  public void setLineItemExtensions(HashMap a_val) {
    lineItemExtensions = a_val;
  }
 
}