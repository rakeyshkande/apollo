package com.ftd.b2b.ariba.common.valueobjects;
import java.util.*;

/**
 * A value object for orders.
 */
public interface IOrderVO 
{
    /**
     * Get line items.
     * @returns List of line items
     */
     public ArrayList getLineItems();

    /**
     * Set line items.
     * @params List of line items
     */
     public void setLineItems(ArrayList newValue);

    /**
     * Add a line item to list.
     * @returns List of line items
     */
     public void addLineItem(LineItemVO vo);


  public String getBuyerCookie();
  
  public void setBuyerCookie(String newBuyerCookie);

  public String getOriginalBuyerCookie();

  public void setOriginalBuyerCookie(String newOriginalBuyerCookie);

  public String getAsnNumber();

  public void setAsnNumber(String newAsnNumber);

  public String getPayloadID();
  
  public void setPayloadID(String newPayloadID);
  
  public Date getCreatedDate();

  public void setCreatedDate(Date newCreatedDate);


}