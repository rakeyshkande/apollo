package com.ftd.b2b.ariba.common.valueobjects;

import java.util.Map;
import java.util.HashMap;

/**
 * Value object for storing specific values obtained
 * from the cXML document.
 *
 * @author York Davis
 * @version 1.0 
 **/
public class AribaRequestVO 
{
  private Map params;
  
  /**
   * Default Constructor. Instantiates the HashMap collection object.
   * 
   * @author York Davis
   **/   
  public AribaRequestVO()
  {
      params = new HashMap();
  }

  /**
   * Returns the collection object of values.
   * 
   * @author York Davis
   **/   
  public Map getParams()
  {
    return params;
  }

  /**
   * Adds a specific name/value pair to the collection object.
   * 
   * @author York Davis
   **/   
  public void addParam(String name, String value)
  {
      params.put(name, value);
  }
  
}