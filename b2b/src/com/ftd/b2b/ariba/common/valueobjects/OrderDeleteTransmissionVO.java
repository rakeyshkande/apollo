package com.ftd.b2b.ariba.common.valueobjects;
import java.util.Date;

/**
 * A value object for Order Delete Transmission table
 */
 
public class OrderDeleteTransmissionVO 
{
  String BuyerCookie;
  Date SendDate;

  public OrderDeleteTransmissionVO()
  {
  }

  /** Get Buyer Cookie
   * @returns String containing Buyer Cookie
   */
  public String getBuyerCookie()
  {
    return BuyerCookie;
  }

  /** Set Buyer Cookie
   * @parms String containing Buyer Cookie
   */
  public void setBuyerCookie(String newBuyerCookie)
  {
    BuyerCookie = newBuyerCookie;
  }

  /** Get Send Date
   * @returns Date containing Send Date
   */
  public Date getSendDate()
  {
    return SendDate;
  }

  /** Set Send Date
   * @parms Date containing Send Date
   */
  public void setSendDate(Date newSendDate)
  {
    SendDate = newSendDate;
  }
}
