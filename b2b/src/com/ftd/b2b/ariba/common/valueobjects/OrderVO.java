package com.ftd.b2b.ariba.common.valueobjects;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.ListIterator;
import com.ftd.b2b.common.utils.DataEncoder;

/**
 * A value object for orders.
 */
public class OrderVO extends DataEncoder implements IOrderVO
{
  private ArrayList lineItems = new ArrayList();
  private String BuyerCookie;
  private String OriginalBuyerCookie;
  private String AsnNumber;
  private String PayloadID;
  private Date CreatedDate;


    /**
     * Get line items.
     * @returns List of line items
     */
     public ArrayList getLineItems() {
     return lineItems;
     }

    /**
    * Return array of line items from the list. 
    * This is done so JOX can be used to serialize this object.
    */
    public ShoppingCartLineItemVO[] getJoxLineItems()
    {
        ShoppingCartLineItemVO[] vo = new ShoppingCartLineItemVO[lineItems.size()];
        ListIterator li = lineItems.listIterator();
        int i = 0;
        while (li.hasNext())
        {
            vo[i] = (ShoppingCartLineItemVO) li.next();
	    i++;
        }
        return vo;
    }
    /**
     * Set line items.
     * @params List of line items
     */
     public void setLineItems(ArrayList newValue) {
      lineItems = newValue;
     }     

    /**
     * Populate the line list based on the passed in array of lines.
     * This is done so JOX can be used to serialize this object.
     * @params ShoppingCartLineItemVO 
     */
   public void setJoxLineItems(ShoppingCartLineItemVO[] vo)
    {
        lineItems.clear();
        int size = vo.length;
        for (int i=0; i<size; i++)
        {
            lineItems.add(vo[i]);
        }
    }
 
    /**
     * Add a line item to list.
     * @returns List of line items
     */
     public void addLineItem(LineItemVO vo) {
       lineItems.add(vo);
     }


  /**
   * Get Buyer Cookie.
   * @returns String containing Buyer Cookie
   */
  public String getBuyerCookie()
  {
    return BuyerCookie;
  }

  /**
   * Set Buyer Cookie
   * @params String containing Buyer Cookie
   */
  public void setBuyerCookie(String newBuyerCookie)
  {
    BuyerCookie = newBuyerCookie;
  }

/**
   * Get Original Buyer Cookie.
   * @returns String containing Original Buyer Cookie
   */
  public String getOriginalBuyerCookie()
  {
    return OriginalBuyerCookie;
  }

  /**
   * Set Original Buyer Cookie
   * @params String containing Original Buyer Cookie
   */
  public void setOriginalBuyerCookie(String newOriginalBuyerCookie)
  {
    OriginalBuyerCookie = newOriginalBuyerCookie;
  }

  /**
   * Get ASN Number
   * @returns String containing ASN Number
   */
  public String getAsnNumber()
  {
    return AsnNumber;
  }

  /**
   * Set ASN Number
   * @params String containing ASN Number
   */
  public void setAsnNumber(String newAsnNumber)
  {
    AsnNumber = newAsnNumber;
  }

  /**
   * Get Payload ID
   * @returns String containing Payload ID
   */
  public String getPayloadID()
  {
    return PayloadID;
  }

  /**
   * Set Payload ID
   * @params String containing Payload ID
   */
  public void setPayloadID(String newPayloadID)
  {
    PayloadID = newPayloadID;
  }

  /**
   * Get Created Date
   * @returns Date containing Created Date
   */
  public Date getCreatedDate()
  {
    return CreatedDate;
  }

  /**
   * Set Created Date
   * @params Date containing Created Date
   */
  public void setCreatedDate(Date newCreatedDate)
  {
    CreatedDate = newCreatedDate;
  }


 
}