package com.ftd.b2b.ariba.common.valueobjects;
import java.util.Date;

public class FTDOrderTransmissionVO 
{
    private long FTDSequenceNumber;
    private String FTDTransmission;
    private String FTDSent;
    private Date sendDate;

  public FTDOrderTransmissionVO()
  {
  }

  public long getFTDSequenceNumber()
  {
    return FTDSequenceNumber;
  }

  public void setFTDSequenceNumber(long newFTDSequenceNumber)
  {
    FTDSequenceNumber = newFTDSequenceNumber;
  }

  public String getFTDTransmission()
  {
    return FTDTransmission;
  }

  public void setFTDTransmission(String newValue)
  {
    FTDTransmission = newValue;
  }

    public String getFTDSent()
    {
        return FTDSent;
    }

    public void setFTDSent(String newFtdSent)
    {
        FTDSent = newFtdSent;
    }

    public Date getSendDate()
    {
        return sendDate;
    }

    public void setSendDate(Date newSendDate)
    {
        sendDate = newSendDate;
    }
}