package com.ftd.b2b.ariba.common.valueobjects;
import java.util.*;

/**
 * A value object for line items.
 */
public interface ILineItemVO 
{
    /** 
     * Get number of line items.
     * @returns int
     */
    public int getNumberOfLineItems();
 
    /** 
     * Get order Number value.
     * @returns String order Number
     */
    public String getOrderNumber();
        
    /** Set value.
     * @parms String order Number value
     */
    public void setOrderNumber(String newValue);

    /** 
     * Get value.
     * @returns String
     */
    public float getTaxAmount();
        
    /** Set value.
     * @parms String value
     */
    public void setTaxAmount(float newValue);

    /** 
     * Get value.
     * @returns String
     */
    public float getOrderTotal();
        
    /** Set value.
     * @parms String value
     */
    public void setOrderTotal(float newValue);

    /** 
     * Get value.
     * @returns String
     */
    public String getOccasion();
        
    /** Set value.
     * @parms String value
     */
    public void setOccasion(String newValue);

    /** 
     * Get value.
     * @returns String
     */
    public String getItemNumber();
        
    /** Set value.
     * @parms String value
     */
    public void setItemNumber(String newValue);

    /** 
     * Get value.
     * @returns float
     */
    public float getItemPrice();
        
    /** Set value.
     * @parms float value
     */
    public void setItemPrice(float newValue);

    /** 
     * Get value.
     * @returns Integer
     */
    public int getAddOnCount();
        
    /** Set value.
     * @parms Integer value
     */
    public void setAddOnCount(int newValue);

      /** 
     * Get value.
     * @returns String
     */
    public String getAddOnDetail();
        
    /** Set value.
     * @parms String value
     */
    public void setAddOnDetail(String newValue);

    /** 
     * Get value.
     * @returns String
     */
    public float getServiceFee();
        
    /** Set value.
     * @parms String value
     */
    public void setServiceFee(float newValue);

    /** 
     * Get value.
     * @returns Date
     */
    public Date getDeliveryDate();
        
    /** Set value.
     * @parms Date value
     */
    public void setDeliveryDate(Date newValue);
 
    /** 
     * Get value.
     * @returns String
     */
    public String getMasterOrderNumber();
        
    /** Set value.
     * @parms String value
     */
    public void setMasterOrderNumber(String newValue);

    /** 
     * Get value.
     * @returns String
     */
    public String getItemUNSPSCCode();
        
    /** Set value.
     * @parms String value
     */
    public void setItemUNSPSCCode(String newValue);

    /** 
     * Get value.
     * @returns String
     */
    public String getItemDescription();
        
    /** Set value.
     * @parms String value
     */
    public void setItemDescription(String newValue);

    /** 
     * Get value.
     * @returns String
     */
    public String getAribaBuyerCookie();
        
    /** Set value.
     * @parms String value
     */
    public void setAribaBuyerCookie(String newValue);

    public float getExtraShippingFee();
    public void setExtraShippingFee(float newValue);
    public float getDropShipCharges();
    public void setDropShipCharges(float newValue);

  
/**
 * Create a copy of this object.
 */
    public Object copy() throws java.lang.CloneNotSupportedException;

 
}