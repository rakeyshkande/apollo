package com.ftd.b2b.ariba.common.valueobjects;
import java.util.*;

/**
 * A value object for approved orders
 * This value object extends the base Order value object
 */
public class ApprovedOrderVO extends OrderVO
{
  Date ApprovedDate;
  String InternalSupplierID;
  

  public ApprovedOrderVO()
  {
  }

  /** Get Approved Date
     *@returns Date containing Approved Date
     */
    public Date getApprovedDate()
  {
    return ApprovedDate;
  }

  /** Set Approved Date
     *@parms Date containing Approved Date
     */
    public void setApprovedDate(Date newApprovedDate)
  {
    ApprovedDate = newApprovedDate;
  }

    /** Get Internal Supplier ID
     *@returns String containing Internal Supplier ID
     */
    public String getInternalSupplierID()
  {
    return InternalSupplierID;
  }

  /** Set Internal Supplier ID
     *@parms String containing Internal Supplier ID
     */
    public void setInternalSupplierID(String newInternalSupplierID)
  {
    InternalSupplierID = newInternalSupplierID;
  }

}