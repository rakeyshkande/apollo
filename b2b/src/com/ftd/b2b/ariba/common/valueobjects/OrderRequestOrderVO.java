package com.ftd.b2b.ariba.common.valueobjects;
import java.util.*;

/**
 * VO which contains data from sent to FTD from an Ariba
 * approved order transmission.
 */
public class OrderRequestOrderVO extends OrderVO
{
  private String comments;
  private String orderType;
  private Date approvedDate;
  public String InternalSupplierID;
  
  /** 
   * Get Value
   * @returns value
   */
  public Date getApprovedDate(){
    return approvedDate;
    }

  /** 
   * Set Value
   * @params new value
   */
  public void setApprovedDate(Date newApprovedDate){
     approvedDate = newApprovedDate;
    }    

  /** 
   * Get Value
   * @returns value
   */
  public String getComments(){
    return comments;
    }

  /** 
   * Set Value
   * @params new value
   */
  public void setComments(String newValue){
     comments = newValue;
    }    

  /** 
   * Get Value
   * @returns value
   */
  public String getOrderType(){
    return orderType;
    }

  /** 
   * Set Value
   * @params new value
   */
  public void setOrderType(String newValue){
     orderType = newValue;
    } 

  /** 
   * Get Value
   * @returns value
   */
  public String getInternalSupplierID()
  {
    return InternalSupplierID;
  }

  /** 
   * Set Value
   * @params new value
   */
  public void setInternalSupplierID(String newInternalSupplierID)
  {
    InternalSupplierID = newInternalSupplierID;
  }

}