package com.ftd.b2b.ariba.common.valueobjects;

/**
 * A value object for Client URL Lookup
 */
 
public class URLLookupVO 
{
  private String AsnNumber;
  private String ClientName;
  private String URL;

  public URLLookupVO()
  {
  }

  /** Get ASN Number
   * @returns String containing ASN Number
   */
  public String getAsnNumber()
  {
    return AsnNumber;
  }

  /** Set ASN Number
   * @parms String containing ASN Number
   */
  public void setAsnNumber(String newAsnNumber)
  {
    AsnNumber = newAsnNumber;
  }

  /** Get Client Name
   * @returns String containing Client Name
   */
  public String getClientName()
  {
    return ClientName;
  }

  /** Set Client Name 
   * @parms String containing Client Name
   */
  public void setClientName(String newClientName)
  {
    ClientName = newClientName;
  }

  /** Get Punchout URL
   * @returns String containing Punchout URL
   */
  public String getURL()
  {
    return URL;
  }

  /** Set Punchout URL
   * @parms String containing Punchout URL
   */
  public void setURL(String newURL)
  {
    URL = newURL;
  }
}