package com.ftd.b2b.ariba.integration;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

import com.ftd.b2b.constants.B2BConstants;
import com.ftd.framework.common.utilities.ResourceManager;
import com.ftd.b2b.utils.mail.B2BMailer;
import com.ftd.osp.utilities.plugins.Logger;
//import com.ftd.framework.services.ServiceLocator;

import com.ftd.b2b.ariba.businessobjects.punchout.*;

/**
 * Servlet which receives PunchOutRequests and responds with
 * a PunchOutResponse.
 *
 * @author York Davis
 * @version 1.0 
 **/
public class PunchOutRequestServlet extends HttpServlet implements B2BConstants 
{
  private static final String CONTENT_TYPE = "text/xml";

  /**
   * Servlet init() method.
   *
   * @param  ServletConfig object.
   * @throws ServletException
   * @author York Davis
   **/
   public void init(ServletConfig config) throws ServletException
   {
     super.init(config);
     String msg = this.getClass().toString() + " successfully started.";
     // B2BMailer.getInstance().send(this, "init()",  msg, B2BMailer.ACTION1);                
     new Logger(LOG_CATEGORY_PUNCHOUT_SERVLET).info(msg);
   }

  /**
   * Code in this doGet() method serves to funnel all requests
   * through one common processRequest() method.
   *
   * @param  HttpServletRequest  object.
   * @param  HttpServletResponse object.   
   * @throws ServletException
   * @throws IOException
   * @author York Davis
   **/
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    processRequest(request, response);
  }

  /**
   * Code in this doPost() method serves to funnel all requests
   * through one common processRequest() method.
   *
   * @param  HttpServletRequest  object.
   * @param  HttpServletResponse object.   
   * @throws ServletException
   * @throws IOException
   * @author York Davis
   **/
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    processRequest(request, response);
  }

  /**
   * Pull in the PunchOutRequest document from the ServletInputStream.
   * Call methods of the PunchOutRequest interface to process the
   * request and generate a PunchOutResponse document.
   *
   * @param  HttpServletRequest  object.
   * @param  HttpServletResponse object.   
   * @throws ServletException
   * @throws IOException
   * @author York Davis
   **/
  public void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    StringBuffer cXML = new StringBuffer();
    String sLine      = "";
    
    PrintWriter out = response.getWriter();   

    BufferedReader reader = request.getReader();

    while ((sLine = reader.readLine()) != null)
    {
        cXML.append(sLine);
    }

   //check if this a ping request to verify the system is up
/*   String ping = (String)request.getParameter(B2BConstants.CONSTANT_PING);
    if(ping != null && !ping.equals("")){
        out.println("B2B Application(PunchOutRequestServlet) is running. Ping received:" + ping);
        Ping pingObject = new Ping();
        out.println(pingObject.doPing());             
        return;
        } */

//    ServiceLocator serviceLocator =  ServiceLocator.getInstance();
    IPunchOutRequest punchOutRequest = null;
    ResourceManager resourceManager = ResourceManager.getInstance();

    try
    {
//        IPunchOutRequestHome punchOutRequestHome = (IPunchOutRequestHome) serviceLocator.getServiceHome(INTERFACE_PUNCHOUTREQUEST);
//        punchOutRequest = (IPunchOutRequest) punchOutRequestHome.create();
        punchOutRequest = (IPunchOutRequest) resourceManager.getImplementation(INTERFACE_PUNCHOUTREQUEST);
        out.println(punchOutRequest.processPunchOutRequest(cXML.toString()));
    }
    catch (Throwable throwable)
    {
        Logger logManager = new Logger(LOG_CATEGORY_PUNCHOUT_SERVLET);
        logManager.error("punchout processRequest: " + throwable.toString());            
        B2BMailer.getInstance().send(this, "processRequest()", throwable.toString(), B2BMailer.ACTION2);            
        out.println(new PunchOutResponseGenerator().generateErrorResponse(PROPERTY_THROWABLEEXCEPTION, null));      
        return;       
    }
    finally
    {
        out.close();
    }
    
  }
}
