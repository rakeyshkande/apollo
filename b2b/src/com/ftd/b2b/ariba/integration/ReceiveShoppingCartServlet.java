package com.ftd.b2b.ariba.integration;
import com.ftd.b2b.ariba.common.utils.Ping;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import com.ftd.framework.common.exceptions.*;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.framework.common.utilities.ResourceManager;

import com.ftd.b2b.utils.mail.B2BMailer;
import com.ftd.b2b.common.utils.XMLHelper;
import com.ftd.b2b.common.utils.XMLParseException;
import com.ftd.b2b.constants.B2BConstants;
import com.ftd.b2b.ariba.common.valueobjects.*;
import com.ftd.b2b.ariba.businessobjects.receiveshoppingcart.IOrderProcessor;

import com.ftd.b2b.ariba.businessobjects.receiveshoppingcart.OrderAssemblerImpl;
import com.ftd.b2b.ariba.common.utils.LineItemParser;

import java.util.StringTokenizer;

import javax.naming.InitialContext;
import javax.transaction.UserTransaction;

/**
 * Servlet which receives assembled Orders (in XML format) from the socket listener
 * and processes the Order.
 *
 * @author York Davis
 * @version 1.0
 **/
public class ReceiveShoppingCartServlet extends HttpServlet implements B2BConstants 
{
  private static String SUCCESS  = " SUCCESS";
  private static String FAILURE  = " FAILURE";
  private static String COMPLETE =  "complete";
  private static final String CONTENT_TYPE        = "text/xml";
  private ResourceManager resourceManager;
  private Logger lm;
  
  /**
   * Servlet init() method.
   *
   * @param  ServletConfig object.
   * @throws ServletException
   * @author York Davis
   **/
   public void init(ServletConfig config) throws ServletException
   {
     super.init(config);
     lm = new Logger(LOG_CATEGORY_ORDER_PROCESSOR);

     resourceManager =  ResourceManager.getInstance();
     
     String msg = this.getClass().toString() + " successfully started.";
     // B2BMailer.getInstance().send(this, "init()", msg, B2BMailer.ACTION1);  
     lm.info(msg);
   }

  /**
   * Code in this doGet() method serves to funnel all requests
   * through one common processRequest() method.
   *
   * @param  HttpServletRequest  object.
   * @param  HttpServletResponse object.   
   * @throws ServletException
   * @throws IOException
   * @author York Davis
   **/
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    try
    {
       processRequest(request, response);
    }
    catch (Throwable t)
    {
       String[] msg = new String[]{t.toString()};
       lm.error("shopping cart servlet doGet: " + t.toString());
       B2BMailer.getInstance().send(this, "doGet()", ERROR_CODE_UNRECOVERABLE_EXCEPTION, msg, B2BMailer.ACTION2);    
   }
  }

  /**
   * Code in this doPost() method serves to funnel all requests
   * through one common processRequest() method.
   *
   * @param  HttpServletRequest  object.
   * @param  HttpServletResponse object.   
   * @throws ServletException
   * @throws IOException
   * @author York Davis
   **/
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    try
    {
       processRequest(request, response);
    }
    catch (Throwable t)
    {
       String[] msg = new String[]{t.toString()};
       lm.error("shopping cart servlet doPost: " + t.toString());
       B2BMailer.getInstance().send(this, "doPost()", ERROR_CODE_UNRECOVERABLE_EXCEPTION, msg, B2BMailer.ACTION2);    
   }
  }

  /**
   * Pull in the Shopping Cart in the form of XML, demarshall
   * it into a value object and process the order.
   *
   * @param  HttpServletRequest  object.
   * @param  HttpServletResponse object.   
   * @throws ServletException
   * @throws IOException
   * @author York Davis
   **/
  public void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
      ShoppingCartOrderVO voOrder = null;
      ShoppingCartLineItemVO voLineItem = null;
      OrderAssemblerImpl assembler = null;
      UserTransaction userTransaction = null;
      InitialContext iContext = null;
  
      // Pull the Shopping Cart from the input stream   
      StringBuffer rawOrder = new StringBuffer();
      String xmlOrderForPending = null;
      String xmlOrderForNovator = null;
      String orderSegment = "";
      String latestLineItem = null;
    
      // Get raw order from Novator
      //
      BufferedReader reader = request.getReader();
      while ((orderSegment = reader.readLine()) != null)
      {
          rawOrder.append(orderSegment);
      }
      lm.debug("processRequest - Shopping cart received: " + rawOrder);

      try {
          // Tokenize each line item from raw data
          String[] tokenizer = rawOrder.toString().split(CONSTANT_RAW_LINE_ITEM_DELIMITER);
          
          // Loop over each line item and assemble the order
          //
          for (int x=0; x<tokenizer.length; x++) {
              String lineItem = tokenizer[x]; 
    
              //Parse input string into an order object
              voOrder = LineItemParser.parseData(lineItem);
    
              //Obtain the line VO that was just parsed
              voLineItem = (ShoppingCartLineItemVO)voOrder.getLineItems().get(0);
              latestLineItem = voLineItem.getOrderNumber();
              
              // Assemble all line items of order
              if (assembler == null) {
                  assembler = new OrderAssemblerImpl(voOrder.getBuyerCookie(), voLineItem.getNumberOfLineItems(), voOrder);
              } else {
                  assembler.addLineItem(voLineItem);
              }
          }
    
          // Make sure all line items were received
          //
          if (! assembler.isOrderComplete()) {
            String[] msg = new String[] {voOrder.getBuyerCookie()}; 
            lm.error("processRequest - All line items not received");
            B2BMailer.getInstance().send(this, "processRequest()", ERROR_CODE_ALLLINEITEMSNOTRECEIVED, msg, B2BMailer.ACTION1);
            sendNovatorResponse(false, response, latestLineItem, null);  // Send error response back to Novator
            return; 
          }
          
          // Convert complete order to XML (for saving to Pending tables) and to cXML (for Novator)
          //
          xmlOrderForPending = assembler.formatXML();
          xmlOrderForNovator = createAribaXML(assembler.getOrder());

      } catch (Exception e) {
          String[] msg = new String[] {"Uncaught Exception : " + e.toString()};
          lm.error("processRequest: " + msg[0]);
          e.printStackTrace();
          B2BMailer.getInstance().send(this, "processRequest()", ERROR_CODE_UNRECOVERABLE_EXCEPTION, msg, B2BMailer.ACTION2);
          sendNovatorResponse(false, response, latestLineItem, null);  // Send error response back to Novator
          return; 
      }

/*   String ping = (String)request.getParameter(B2BConstants.CONSTANT_PING);
   //check if this a ping request to verify the system is up
    if(rawOrder != null && !ping.equals("")){
        PrintWriter out = response.getWriter();  
        out.println("B2B Application(ReceiveShoppingCartServlet) is running. Ping received:" + ping);
        Ping pingObject = new Ping();
        out.println(pingObject.doPing());             
        return;
        }
*/

      // Convert the order XML into value object.
      //
      // NOTE: If the assembly into xml and conversion back to an object here seems silly,
      // well, it is.  It's a legacy from when b2b had thread groups assembling orders
      // and posting xml to this servlet.  During the conversion we left this logic intact
      // since we didn't want to stir the pot too much.  Ideally it would be further simplified.
      //
      if (lm.isDebugEnabled()) {
        lm.debug("processRequest - shopping cart data that will be saved to database : " + xmlOrderForPending);
      }  
      ShoppingCartOrderVO voShoppingCartOrder = null;
      try
      {
//         String orderData = extractAndValidate(rawOrder.toString());
         voShoppingCartOrder = (ShoppingCartOrderVO) new XMLHelper().demarshall(xmlOrderForPending);
      } catch (XMLParseException xmlpe) {
         String[] msg = new String[]{"Error parsing XML document : " + xmlpe.toString() + "\n\n" + xmlOrderForPending};
         lm.error("processRequest: " + msg[0]);
         B2BMailer.getInstance().send(this, "processRequest()", ERROR_CODE_UNRECOVERABLE_EXCEPTION, msg, B2BMailer.ACTION1);    
         sendNovatorResponse(false, response, latestLineItem, null);  // Send error response back to Novator
         return;
      } catch (Throwable e) {
         String[] msg = new String[]{"Demarshall error : " + e.toString()};
         lm.error("processRequest: " + msg[0]);
         B2BMailer.getInstance().send(this, "processRequest()", ERROR_CODE_UNRECOVERABLE_EXCEPTION, msg, B2BMailer.ACTION2);    
         sendNovatorResponse(false, response, latestLineItem, null);  // Send error response back to Novator
         return;
      }

      // Persist order to database
      //
      IOrderProcessor orderProcessor = null;
      boolean persistSuccess = false;
      try
      {           
         iContext = new InitialContext();
         userTransaction = (UserTransaction)iContext.lookup("java:comp/UserTransaction");
         int transactionTimeout = Integer.parseInt(resourceManager.getProperty(PROPERTY_B2B_PROPERTY_FILE, PROPERTY_TRANSACTION_TIMEOUT));
         userTransaction.setTransactionTimeout(transactionTimeout);
         userTransaction.begin();

         orderProcessor = (IOrderProcessor) resourceManager.getImplementation(INTERFACE_ORDERPROCESSOR);                
         synchronized (this.getClass())
         {
           orderProcessor.persistData(voShoppingCartOrder);
           persistSuccess = true;
         }
      } catch (FTDApplicationException ftdae) {
          // No need to log since the exception was already logged and emailed
      } catch (Throwable e) {
         String[] msg = new String[] {"Uncaught Exception : " + e.toString()};
         lm.error("processRequest: " + msg[0]);
         e.printStackTrace();
         B2BMailer.getInstance().send(this, "processRequest()", ERROR_CODE_UNRECOVERABLE_EXCEPTION, msg, B2BMailer.ACTION2);
      }

      // If success, then commit and return http response to Novator, 
      // else rollback and return error response to Novator.
      //
      try {
         if (persistSuccess) {
            lm.debug("Committing DB changes and sending response to Novator");
            userTransaction.commit();
            sendNovatorResponse(true, response, latestLineItem, xmlOrderForNovator);  // Send success response
         } else {
            lm.error("Rollback DB changes and sending error response to Novator");
            userTransaction.rollback();
            sendNovatorResponse(false, response, latestLineItem, null);  // Send error response
         }
      } catch (Exception e) {
         String[] msg = new String[] {"Uncaught Exception : " + e.toString()};
         lm.error("Sending error response to Novator since exception occurred: " + msg[0]);
         e.printStackTrace();
         sendNovatorResponse(false, response, latestLineItem, null);  // Send error response
         B2BMailer.getInstance().send(this, "processRequest()", ERROR_CODE_UNRECOVERABLE_EXCEPTION, msg, B2BMailer.ACTION2);
      }
      
  }

  
    /**
     * Create and send an http response to Novator
     * 
     * @param successResponse True to send successful response, false for failure
     * @param response 
     * @param orderNumber
     * @param msg
     */
    private void sendNovatorResponse(boolean successResponse, HttpServletResponse response, String orderNumber, String msg) {
        PrintWriter out = null;   
        try {
            out = response.getWriter();   
            if (successResponse) {
                // These are required by Novator to send the 'received' confirmation back to Ariba
                out.println(orderNumber + SUCCESS);
                out.println(COMPLETE); 
                out.println(msg);
            } else {
                out.println(orderNumber + FAILURE);
                out.println(COMPLETE); 
                out.println(msg);
            }
        } catch (Exception e) {
            String[] err = new String[] {"Error sending response to Novator: " + e.toString()};
            lm.error("sendNovatorResponse - " + err[0]);
            B2BMailer.getInstance().send(this, "sendNovatorResponse()", ERROR_CODE_UNRECOVERABLE_EXCEPTION, err, B2BMailer.ACTION2);
        } finally {
          out.close();    
        }
    }

  
    /**
     * Build the XML to send back to Novator.
     *
     * @param ShoppingCartOrderVO The order to be converted to XML.
     * @return The cXML response document.
     * @author York Davis
     **/
     private String createAribaXML(ShoppingCartOrderVO order) throws FTDApplicationException
     {
       // Process the Order
       IOrderProcessor orderProcessor = null;
       try
       {           
          orderProcessor = (IOrderProcessor) resourceManager.getImplementation(INTERFACE_ORDERPROCESSOR);                
          return orderProcessor.buildOrder(order);
       }
       catch (ResourceNotFoundException e)
       {
          String[] msg = new String[]{ INTERFACE_ORDERPROCESSOR + " could not be found : " + e.toString()};
          lm.error(msg[0]);
          B2BMailer.getInstance().send(this, "createAribaXML()", ERROR_CODE_UNABLETOLOADSERVICE,msg, B2BMailer.ACTION2);
          throw new FTDApplicationException(ERROR_CODE_UNRECOVERABLE_EXCEPTION, e);
       }
     }
  
}
