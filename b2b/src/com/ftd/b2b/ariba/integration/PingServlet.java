package com.ftd.b2b.ariba.integration;
import com.ftd.b2b.ariba.common.utils.Ping;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

import com.ftd.b2b.constants.B2BConstants;
import com.ftd.b2b.integration.dao.DAO;
import com.ftd.framework.common.utilities.ResourceManager;
import com.ftd.b2b.utils.mail.B2BMailer;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;


import com.ftd.b2b.ariba.businessobjects.punchout.*;

/**
 * Servlet which returns a ping response.
 *
 * @author York Davis
 * @version 1.0 
 **/
public class PingServlet extends HttpServlet implements B2BConstants 
{
  private static final String CONTENT_TYPE = "text/xml";
  protected static Logger logger = new Logger(PingServlet.class.getName());

  /**
   * Servlet init() method.
   *
   * @param  ServletConfig object.
   * @throws ServletExceEd Mueller
   **/
   public void init(ServletConfig config) throws ServletException
   {
     super.init(config);
   }

  /**
   * Code in this doGet() method serves to funnel all requests
   * through one common processRequest() method.
   *
   * @param  HttpServletRequest  object.
   * @param  HttpServletResponse object.   
   * @throws ServletException
   * @throws IOException
   * @author York Davis
   **/
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    processRequest(request, response);
  }

  /**
   * Code in this doPost() method serves to funnel all requests
   * through one common processRequest() method.
   *
   * @param  HttpServletRequest  object.
   * @param  HttpServletResponse object.   
   * @throws ServletException
   * @throws IOException
   * @author Ed Mueller
   **/
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    processRequest(request, response);
  }

  /**
   * Pull in the PunchOutRequest document from the ServletInputStream.
   * Call methods of the PunchOutRequest interface to process the
   * request and generate a PunchOutResponse document.
   *
   * @param  HttpServletRequest  object.
   * @param  HttpServletResponse object.   
   * @throws ServletException
   * @throws IOException
   * @author Ed Mueller
   **/
  public void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    logger.debug("Ping Servlet here");
    StringBuffer cXML = new StringBuffer();
    String sLine      = "";
    
    PrintWriter out = response.getWriter();   

    String gathererUrl = "";
    try {
        ConfigurationUtil cu = ConfigurationUtil.getInstance();
        gathererUrl = cu.getFrpGlobalParm(B2B_ARIBA_CONFIG_CONTEXT, PROPERTY_ORDER_PROCESSING_ADDRESS);
    } catch (Exception e) {
        out.println("Ping error while getting global parm: " + e.toString());   
        e.printStackTrace(out);
    }
    
    //get ping object
    Ping pingObject = new Ping();

    //display ping response
    out.println(new java.util.Date());
    out.println("The B2B application is alive and configured to submit to: " + gathererUrl);
    if (request.getParameter(CONSTANT_RELOAD_CLIENT_OPTIONS) != null) {
      // If reload parameter set, then force reload of client options
      out.println(pingObject.doPing(true));
    } else {
      out.println(pingObject.doPing());             
    }
    return;
  }
}
