package com.ftd.b2b.ariba.integration.dao;

import java.sql.SQLException;
import java.util.List;

import com.ftd.b2b.ariba.common.valueobjects.ApprovedLineItemVO;
import com.ftd.framework.common.exceptions.BadConnectionException;
import com.ftd.framework.common.exceptions.FTDSystemException;

/**
 * Interface class for the working with the 
 * Approved Line Item table.  
 *
 * @author Kristyn Angelo
 * @version 1.0
 */

public interface IApprovedLineDAO 
{
  /** Retrieves records from the Approved Line Item table
   *  based on the Buyer Cookie
   * @parm String containing the buyer cookie
   * @return List of Approved line item value objects
   * @throws SQL Exception if retrieve call to database fails.
   *
   * @author Kristyn Angelo
   */
  public List retrieveLines(String BuyerCookie) throws SQLException, 
                                                  BadConnectionException,
                                                  FTDSystemException;                                                  
  /** Inserts a new Approved Line into the Approved Line Item table
   * @parm Approved Line Item Value Object and String containing Buyer Cookie
   * @return String with a value of true or false
   * @throws SQL Exception if retrieve call to database fails.
   *
   * @author Kristyn Angelo
   */
  public String insertLine(ApprovedLineItemVO voApprovedLine, String BuyerCookie) throws SQLException, 
                                                                     BadConnectionException,
                                                                     FTDSystemException; 

    /** Retrieves Buyer Cookie from Approved Line Item table
   *  based on the FTD Order Number
   * @parm String containing FTD Order Number
   * @return String containing Buyer Cookie
   * @throws SQL Exception if retrieve call to database fails.
   *
   * @author Kristyn Angelo
   */
  public String retrieveBuyerCookie(String FTDOrderNumber) throws SQLException, 
                                                  BadConnectionException,
                                                 FTDSystemException;                                                                     
}