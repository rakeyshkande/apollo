package com.ftd.b2b.ariba.integration.dao;

import java.sql.SQLException;
import java.util.List;

import com.ftd.framework.common.exceptions.BadConnectionException;
import com.ftd.framework.common.exceptions.FTDSystemException;

/**
 * Interface for working with Client URL table
 *
 * @author Kristyn Angelo
 * @version 1.0 
 **/
 
public interface IURLLookupDAO
{
  /** Retrieves records from the Client URL table
   * based on the ASN Number.
   * @parm String containing the ASN Number
   * @return List of Client URL  value objects
   * @throws SQL Exception if retrieve call to database fails.
   *
   * @author Kristyn Angelo
   */
    public List retrieve(String ASNNumber) throws SQLException, 
                                                  BadConnectionException,
                                                  FTDSystemException;
}