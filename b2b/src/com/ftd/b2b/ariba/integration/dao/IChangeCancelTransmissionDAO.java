package com.ftd.b2b.ariba.integration.dao;

import java.sql.SQLException;
import java.util.List;

import com.ftd.framework.common.exceptions.BadConnectionException;
import com.ftd.framework.common.exceptions.FTDSystemException;

/**
 * Data Access interface for the ChangeCancelTransmission table.
 *
 * @author York Davis
 * @version 1.0
 */
public interface IChangeCancelTransmissionDAO
{
  /**
   * Deletes records based on the sequence.
   *
   * @parm Sequence number.
   * @return String saying whether the delete was successful.
   * @throws SQLException if delete call to database fails.
   * @throws BadConnectionException if database connection is in error.
   * @throws FTDSystemException if a framework error occurs.
   *
   * @author York Davis
   */
   public String delete(int sequence) throws SQLException, 
                                            BadConnectionException,
                                            FTDSystemException;
  /**
   * Inserts a Change/Cancel msg to the table.
   *
   * @parm Change/Cancel message.
   * @return String saying whether the delete was successful.
   * @throws SQLException if delete call to database fails.
   * @throws BadConnectionException if database connection is in error.
   * @throws FTDSystemException if a framework error occurs.
   *
   * @author York Davis
   */
   public String insert(String changeCancelMsg) throws SQLException, 
                                                     BadConnectionException,
                                                     FTDSystemException;
  /**
   * Retrieves all Change/Cancel messages from the table.
   *
   * @return Collection object of current Change/Cancel messages.
   * @throws SQLException if delete call to database fails.
   * @throws BadConnectionException if database connection is in error.
   * @throws FTDSystemException if a framework error occurs.
   *
   * @author York Davis
   */
   public List retrieve() throws SQLException, 
                                BadConnectionException,
                                FTDSystemException;                                                    
 }