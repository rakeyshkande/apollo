package com.ftd.b2b.ariba.integration.dao;

import com.ftd.framework.common.exceptions.BadConnectionException;
import com.ftd.framework.common.exceptions.FTDSystemException;

import com.ftd.b2b.ariba.common.valueobjects.ChangeCancelTransmissionVO;
import com.ftd.b2b.integration.dao.DAO;
import com.ftd.b2b.constants.*;

import java.util.*;
import java.sql.*;
import javax.sql.*;

/**
 * Data Access implementation class for the ChangeCancelTransmission table.
 *
 * @author York Davis
 * @version 1.0
 */
public class ChangeCancelTransmissionDAOImpl extends DAO implements IChangeCancelTransmissionDAO,
                                                                    B2BConstants,
                                                                    B2BDatabaseConstants
{

  private static final String insertSQL = "{? = call B2B.CHANGECANCELTRANSINSERT_FUNC(?) }";
  private static final String deleteSQL = "{? = call B2B.CHANGECANCELTRANSDELETE_FUNC(?) }";
  private static final String selectSQL = "{? = call B2B.CHANGECANCELTRANSSELECT_FUNC() }";

  public ChangeCancelTransmissionDAOImpl()
  {
      super(LOG_CATEGORY_CHANGECANCELTRANSMISSION_DAO);
  }

  /**
   * Deletes records based on the sequence.
   *
   * @parm Sequence number.
   * @return String saying whether the delete was successful.
   * @throws SQLException if delete call to database fails.
   * @throws BadConnectionException if database connection is in error.
   * @throws FTDSystemException if a framework error occurs.
   *
   * @author York Davis
   */
  public String delete(int sequence) throws SQLException, 
                                            BadConnectionException,
                                            FTDSystemException
  {
      return super.execute(deleteSQL, new Object[] {new Integer(sequence)});
  }

  /**
   * Inserts a Change/Cancel msg to the table.
   *
   * @parm Change/Cancel message.
   * @return String saying whether the delete was successful.
   * @throws SQLException if delete call to database fails.
   * @throws BadConnectionException if database connection is in error.
   * @throws FTDSystemException if a framework error occurs.
   *
   * @author York Davis
   */
  public String insert(String changeCancelMsg) throws SQLException, 
                                                      BadConnectionException,
                                                      FTDSystemException
  {
      return super.execute(insertSQL, new Object[] {changeCancelMsg} );
  }

  /**
   * Retrieves all Change/Cancel messages from the table.
   *
   * @return Collection object of current Change/Cancel messages.
   * @throws SQLException if delete call to database fails.
   * @throws BadConnectionException if database connection is in error.
   * @throws FTDSystemException if a framework error occurs.
   *
   * @author York Davis
   */
  public List retrieve() throws SQLException, 
                                BadConnectionException,
                                FTDSystemException
  {
      return super.retrieve(selectSQL);
  }

  /**  
   * Builds the ChangeCancelTransmissionVO object.
   *
   * @parm Result Set
   * @return Constructed object.
   * @throws SQLException if delete call to database fails.
   * @throws BadConnectionException if database connection is in error.
   * @throws FTDSystemException if a framework error occurs.
   *
   * @author York Davis
   */
  public Object set(ResultSet rsResults) throws SQLException
  {
      ChangeCancelTransmissionVO cctVO = new ChangeCancelTransmissionVO();
      cctVO.setChangeCancelMsg(rsResults.getString(B2B_CHANGE_CANCEL_MSG));
      cctVO.setSequence(rsResults.getInt(B2B_CHANGE_CANCEL_SEQUENCE));  
      return cctVO;
  }
}