package com.ftd.b2b.ariba.integration.dao;

import java.util.ArrayList;

import com.ftd.framework.common.exceptions.BadConnectionException;
import com.ftd.framework.common.exceptions.FTDSystemException;

import com.ftd.b2b.integration.dao.*;
import com.ftd.b2b.constants.*;

import java.util.*;
import java.sql.*;
import javax.sql.*;

/**
 * Implementation class for the working with the Client URL table
 *
 * @author Kristyn Angelo
 * @version 1.0
 */

public class BuyerSourceCodeLookupDAOImpl extends DAO implements IBuyerSourceCodeLookupDAO, 
                                                                 B2BConstants,
                                                                 B2BDatabaseConstants
{
  private static final String SQL = "{? = call B2B.SOURCECODELOOKUP_FUNC(?) }";
  
  public BuyerSourceCodeLookupDAOImpl()
  {
    super(LOG_CATEGORY_BUYERSOURCECODELOOKUP_DAO);
  }

  /** Retrieve source code value from the Buyer_Source_Code table
   * based on the ASN Number.
   * @parm String containing the ASN Number
   * @return String containing the Buyer Source Code
   * @throws SQL Exception if retrieve call to database fails.
   *
   * @author Kristyn Angelo
   */
  public String retrieveByASNNumber(String ASNNumber) throws SQLException, 
                                                  BadConnectionException,
                                                  FTDSystemException
  {
          return super.execute(SQL, new Object[] {ASNNumber});
  }

  /**  Sets the Buyer Source Code value object fields 
   * @parm Result Set
   * @return Buyer Source Code value object
   * @throws SQL Exception if the fields are not returned from the
   *         database call.
   *
   * @author Kristyn Angelo
   */
  public Object set(ResultSet rsResults) throws SQLException
  {
    return null;
  }

}