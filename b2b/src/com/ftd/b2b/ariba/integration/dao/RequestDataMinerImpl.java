package com.ftd.b2b.ariba.integration.dao;

import com.ftd.b2b.common.utils.BusinessObject;
import com.ftd.b2b.ariba.common.utils.RequestXMLHelper;
import com.ftd.b2b.ariba.common.valueobjects.AribaRequestVO;
import com.ftd.b2b.constants.B2BConstants;
import com.ftd.b2b.common.utils.XMLParseException;

/**
 * Implementation class for mining data from a cXML document.
 * Should be resusable all Ariba B2B use cases.
 *
 * @author York Davis
 * @version 1.0 
 **/
public class RequestDataMinerImpl extends BusinessObject implements IRequestDataMiner, B2BConstants
{

  public static final String NOT_FOUND      = "[NOT FOUND]";

  public RequestDataMinerImpl()
  {
    super(LOG_CATEGORY_REQUEST_DATA_MINER);
  }

  /**
    * Based on the cXML document passed, extract the parameters and return
    * a value object.
    * 
    * @param  cXML document.
    * @return AribaRequest value object.
    * @throws XMLParseException if the data cannot be parsed.
    *
    * @author York Davis
    *
    * Added error checking and defaulting of values for shared secret and duns number
    **/
  public AribaRequestVO mine(String cXML) throws XMLParseException
  {
        RequestXMLHelper requestXMLHelper = new RequestXMLHelper();

        AribaRequestVO aribaRequestVO = new AribaRequestVO();
        aribaRequestVO.addParam(CONSTANT_ASN_NUMBER, requestXMLHelper.getAsnBuyerNumber(cXML));
        aribaRequestVO.addParam(CONSTANT_PAYLOAD_ID, requestXMLHelper.getPayloadId(cXML));

        String secretValue = "";
        try{
                secretValue = requestXMLHelper.getSharedSecret(cXML);
             }
        catch(Throwable t){
                secretValue = NOT_FOUND;
        }
        finally{
                aribaRequestVO.addParam(CONSTANT_SHARED_SECRET, secretValue);  
        }
        
        String dunsValue = "";
        try{
                dunsValue = requestXMLHelper.getDUNSNumber(cXML);
            }
        catch(Throwable t){
                dunsValue = NOT_FOUND;
        }
        finally{
                aribaRequestVO.addParam(CONSTANT_FTD_DUNS_NUMBER, dunsValue);                
            }

        String internalSupplierIDValue = "";
        try{
                internalSupplierIDValue = requestXMLHelper.getInternalSupplierID(cXML);
            }
        catch(Throwable t){
                internalSupplierIDValue = NOT_FOUND;
        }
        finally{
                aribaRequestVO.addParam(CONSTANT_FTD_INTERNAL_SUPPLIER_ID, internalSupplierIDValue);                
            }            
            
        return aribaRequestVO;
  }

   /**
    * Converts this class into a string.
    * 
    * @return String 
    **/
   public String toString()
  {
    return this.getClass().toString();
  }

  /**
    * Determine equality of this object to another.
    * 
    * @param  The object to be compared.
    * @return Result of equality comparison
    *
    **/
  public boolean equals(Object o)
  {
    return this == o;
  }

  /**
    * Saves state. Currently unused.
    * 
    * @author York Davis
    **/
  public void save()
  {
  }

  /**
    * Builds state. Currently unused.
    * 
    * @author York Davis
    **/
  public void create()
  {
  }

}