package com.ftd.b2b.ariba.integration.dao;

import com.ftd.framework.common.exceptions.BadConnectionException;
import com.ftd.framework.common.exceptions.FTDSystemException;

import com.ftd.b2b.ariba.common.valueobjects.OrderVO;
import com.ftd.b2b.ariba.common.valueobjects.IOrderVO;
import com.ftd.b2b.ariba.common.valueobjects.LineItemVO;
import com.ftd.b2b.ariba.integration.dao.PendingLineDAOImpl;
import com.ftd.b2b.integration.dao.DAO;
import com.ftd.b2b.utils.mail.B2BMailer;
import com.ftd.b2b.constants.*;
import com.ftd.b2b.common.utils.DuplicateApprovalException;

import java.util.ArrayList;
import java.util.ListIterator;
import java.util.List;
import java.sql.SQLException;
import java.sql.ResultSet;

/**
 * Implementation class for the working with the Pending 
 * and Pending Line Item tables.  Functions include retrieve,
 * delete and insert into the tables.
 *
 * @author Kristyn Angelo
 * @version 1.0
 */

public class PendingDAOImpl extends DAO implements IPendingDAO,
                                                   B2BConstants,
                                                   B2BDatabaseConstants
{

  private static final String selectSQL = "{? = call B2B.PENDINGINFO_FUNC(?) }";
  private static final String insertSQL = 
    "{? = call B2B.PENDINGINSERT_FUNC(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }";
  private static final String deleteSQL = "{? = call B2B.PENDINGDELETE_FUNC(?) }";
  private static final String moveSQL = "{? = call B2B.PENDINGMOVE_FUNC(?) }";

  public PendingDAOImpl()
  {
      super(LOG_CATEGORY_PENDING_DAO);
  }

  public PendingDAOImpl(boolean selfManagedTransaction)
  {
      super(LOG_CATEGORY_PENDING_DAO, selfManagedTransaction);
  }

  /** Retrieves records from the Pending Line Item table
   * based on the Buyer Cookie.
   * @parm String containing the buyer cookie
   * @return List of Pending Line Item value objects
   * @throws SQL Exception if retrieve call to database fails.
   *
   * @author Kristyn Angelo
   */
  public List retrieveLineItems(String BuyerCookie) throws SQLException,
                                                      BadConnectionException,
                                                      FTDSystemException
  {
      PendingLineDAOImpl pendingLine = new PendingLineDAOImpl();
      return pendingLine.retrieveLines(BuyerCookie);
  }

  /** Retrieves records from the Pending and Pending Line Item
   * tables based on the Buyer Cookie
   * @parm String containing the buyer cookie
   * @return List of Pending Order value objects
   * @throws SQL Exception if retrieve call to database fails.
   *
   * @author Kristyn Angelo
   */
  public Object retrieveByOrderNumber(String OrderNumber) throws SQLException, 
                                                  BadConnectionException,
                                                 FTDSystemException, 
                                                 DuplicateApprovalException
  {
      OrderVO vo = new OrderVO();
      List orderList = super.retrieve(selectSQL, new Object[] {OrderNumber});
                
      if ( orderList == null )
      {          
          // No match found, so let's check if it was already approved just for fun
          ApprovedDAOImpl apoDao = new ApprovedDAOImpl();
          if (apoDao.retrieveByOrderNumber(OrderNumber) != null) {
              logger.error("Customer tried to approve an order that has already been approved: " + OrderNumber);
              throw new DuplicateApprovalException("Order was already approved: " + OrderNumber);
          }
          return null;
      }
      else if (orderList.size() > 1)
      {
          throw new SQLException ("Multiple Lists of Pending Line Items were returned for Order Number: " + OrderNumber);
      }
      else
      {
          vo = (OrderVO)orderList.get(0);
          String BuyerCookie = vo.getBuyerCookie();
          List lineList = retrieveLineItems(BuyerCookie);
          if (lineList.isEmpty())
          {
            throw new SQLException("No Pending Line Items were returned for Buyer Cookie:  " + BuyerCookie);
          }
          vo.setLineItems((ArrayList)lineList);
      }
      return vo;
 }

    /**  Deletes records from the Pending and Pending Line Item
   * tables based on the Buyer Cookie
   * @parm Pending Order value object
   * @return String saying whether the delete was successful
   * @throws SQL Exception if delete call to database fails.
   *
   * @author Kristyn Angelo
   */
  public String delete(IOrderVO vo) throws SQLException, 
                                          BadConnectionException,
                                          FTDSystemException
  {

      //Force delete to be self managed
      //setSelfManagedTransaction(true);  No longer needed since using UserTransaction at higher level
      
      String BuyerCookie = vo.getOriginalBuyerCookie();
      String returnVal = "";

      try
      {
        returnVal = super.execute(deleteSQL, new Object[] {BuyerCookie} );
      } 
      catch (SQLException sqle)
      {
        rollback(); // This should no longer do anything - remove this someday
        throw sqle;
      }
      commit(); // This should no longer do anything - remove this someday
      return returnVal;     
  }

  /** Moves an order in pending instead of deleting.
   * There are cases where website erroneously returns us edited cart with new order number instead of original,
   * so created this hack to save original cart, allowing approval logic to find order if necessary.
   */
  public String moveOrder(IOrderVO vo) throws SQLException, 
                                          BadConnectionException,
                                          FTDSystemException
  {
      String BuyerCookie = vo.getOriginalBuyerCookie();
      String returnVal = "";

      try
      {
        returnVal = super.execute(moveSQL, new Object[] {BuyerCookie} );
      } 
      catch (SQLException sqle)
      {
        rollback(); // This should no longer do anything - remove this someday
        throw sqle;
      }
      commit(); // This should no longer do anything - remove this someday
      return returnVal;     
  }

  
  /**  Inserts records into the Pending and Pending Line Item
   * tables based on the Buyer Cookie
   * @parm Pending Order value object
   * @return String saying whether the insert was successful
   * @throws SQL Exception if insert call to database fails.
   *
   * @author Kristyn Angelo
   */          
  public String insert(OrderVO vo) throws SQLException, 
                                          BadConnectionException,
                                          FTDSystemException
  {
      wasAnyFieldTruncated = false;
      
      //Force insert to be self managed
      //setSelfManagedTransaction(true);  No longer needed since using UserTransaction at higher level  
      
      String returnVal = "";
           
      String BuyerCookie = vo.getOriginalBuyerCookie();
      String ASNNumber = vo.getAsnNumber();
      String PayloadID = vo.getPayloadID();
      java.util.Date CreatedDate = vo.getCreatedDate();
         

      List list = new ArrayList();
      list = vo.getLineItems();

      ListIterator i = list.listIterator();
      while (i.hasNext())
      {
          LineItemVO lineVO = (LineItemVO)i.next();
          String OrderNumber = truncIt(lineVO.getOrderNumber(), MAX_LEN_ORDERNUM);
          float TaxAmount = lineVO.getTaxAmount();
          float OrderTotal = lineVO.getOrderTotal();
          String Occasion = truncIt(lineVO.getOccasion(), MAX_LEN_OCCASION);
          String ItemNumber = truncIt(lineVO.getItemNumber(), MAX_LEN_ITEMNUM);
          float ItemPrice = lineVO.getItemPrice();
          int AddOnCount = lineVO.getAddOnCount();
          String AddOnDetail = lineVO.getAddOnDetail();
          float ServiceFee = lineVO.getServiceFee();
          java.util.Date DeliveryDate = lineVO.getDeliveryDate();
          String MasterOrderNumber = truncIt(lineVO.getMasterOrderNumber(), MAX_LEN_MASTER);
          String ItemUNSPSCCode = truncIt(lineVO.getItemUNSPSCCode(), MAX_LEN_UNSPSC);
          String ItemDescription = truncIt(encode(lineVO.getItemDescription()), MAX_LEN_DESCRIPTION);
          float ExtraShippingFee = lineVO.getExtraShippingFee();
          float DropShipCharges = lineVO.getDropShipCharges();

        /* 6/25/03 Ed Mueller
            These fields were added to the pending and archive tables. */
        String ShipToFirstName = truncIt(lineVO.getShipToFirstName(), MAX_LEN_NAME);
        String ShipToLastName = truncIt(lineVO.getShipToLastName(), MAX_LEN_NAME);
        String ShipToAddressLine1 = truncIt(lineVO.getShipToAddressLine1(), MAX_LEN_ADDRESS);
        String ShipToAddressLine2 = truncIt(lineVO.getShipToAddressLine2(), MAX_LEN_ADDRESS);      
        String ShipToCity = truncIt(lineVO.getShipToCity(), MAX_LEN_CITY);
        String ShipToState = truncIt(lineVO.getShipToState(), MAX_LEN_STATE);
        String ShipToZipCode = truncIt(lineVO.getShipToZipCode(), MAX_LEN_ZIP);
        String ShipToCountry = truncIt(lineVO.getShipToCountry(), MAX_LEN_COUNTRY);
        String ShipToPhone = truncIt(lineVO.getShipToWorkPhone(), MAX_LEN_PHONE);
        String ShipToExtension = truncIt(lineVO.getShipToWorkExtension(), MAX_LEN_EXTENSION);
        String ShipToBusinessType = truncIt(lineVO.getShipToBusinessType(), CONSTANT_MAX_SHIP_TO_BUS_TYPE);
        String ShipToBusinessName = truncIt(lineVO.getShipToBusinessName(), CONSTANT_MAX_SHIP_TO_BUS_NAME);
        String CardMessage = truncIt(lineVO.getCardMessage(), MAX_LEN_CARD);
        String SpecialInstructions = truncIt(lineVO.getSpecialInstructions(), MAX_LEN_CARD);
        /* 6/25/03...end */
        
        /* 8/4/03 Ed Mueller field added for retail vs discount price changes */
        float retailPrice = lineVO.getRetailPrice();
        /* 8/4/03...end*/

          // BillTo added for Spec #2255
          String BillToFirstName = truncIt(lineVO.getBillToFirstName(), MAX_LEN_NAME);
          String BillToLastName = truncIt(lineVO.getBillToLastName(), MAX_LEN_NAME);
          String BillToAddressLine1 = truncIt(lineVO.getBillToAddressLine1(), MAX_LEN_ADDRESS);
          String BillToAddressLine2 = truncIt(lineVO.getBillToAddressLine2(), MAX_LEN_ADDRESS);      
          String BillToCity = truncIt(lineVO.getBillToCity(), MAX_LEN_CITY);
          String BillToState = truncIt(lineVO.getBillToState(), MAX_LEN_STATE);
          String BillToZipCode = truncIt(lineVO.getBillToZipCode(), MAX_LEN_ZIP);
          String BillToCountry = truncIt(lineVO.getBillToCountry(), MAX_LEN_COUNTRY);
          String BillToWorkPhone = truncIt(lineVO.getBillToWorkPhone(), MAX_LEN_PHONE);
          String BillToWorkPhoneExt = truncIt(lineVO.getBillToWorkPhoneExt(), MAX_LEN_EXTENSION);
          String BillToEmail = truncIt(lineVO.getBillToEmail(), MAX_LEN_EMAIL);
          
          try
          {
              if (logger.isDebugEnabled())  
             {
               logger.debug("Inserting into Pending Tables");
             }   
              returnVal = super.execute(insertSQL, new Object[] {OrderNumber, new Float(TaxAmount), 
                                                                        new Float(OrderTotal), Occasion, 
                                                                        ItemNumber, new Float(ItemPrice), 
                                                                        new Integer(AddOnCount), AddOnDetail, 
                                                                        new Float(ServiceFee), DeliveryDate,
                                                                        MasterOrderNumber, ItemUNSPSCCode, 
                                                                        ItemDescription, PayloadID, 
                                                                        BuyerCookie, ASNNumber, CreatedDate,
                                                                        ShipToFirstName, ShipToLastName,
                                                                        ShipToAddressLine1, ShipToAddressLine2,
                                                                        ShipToCity, ShipToState,
                                                                        ShipToZipCode, ShipToCountry,
                                                                        ShipToPhone, 
                                                                        ShipToExtension,
                                                                        ShipToBusinessType,
                                                                        ShipToBusinessName,
                                                                        CardMessage, SpecialInstructions,new Float(retailPrice),
                                                                        BillToFirstName, BillToLastName,
                                                                        BillToAddressLine1, BillToAddressLine2,
                                                                        BillToCity, BillToState,
                                                                        BillToZipCode, BillToCountry,
                                                                        BillToWorkPhone, BillToWorkPhoneExt, BillToEmail,
                                                                        new Float(ExtraShippingFee), new Float(DropShipCharges)}
                                                                        );
          } 
          catch (SQLException sqle)
          {
              rollback();  // This should no longer do anything - remove this someday
              throw sqle;
          }
      } // end while
      commit(); // This should no longer do anything - remove this someday
      
      // Send alert if any field was truncated
      try {
          if (wasAnyFieldTruncated == true) {
              String[] msg = new String[] {((BuyerCookie!=null)?BuyerCookie:" ")};  
              B2BMailer.getInstance().send(this,"insert",ERROR_CODE_VALUE_TOO_LARGE_FOR_COLUMN,msg,B2BMailer.ACTION2);          
          }
      } catch (Exception e) {
          logger.error("Failed to send alert that fields were truncated, but continuing anyhow.  Exception was: " + e);
      }

      return returnVal;     
    }

  /**  Sets the Pending order value object fields 
   * @parm Result Set
   * @return Pending order value object
   * @throws SQL Exception if the fields are not returned from the
   *         database call.
   *
   * @author Kristyn Angelo
   */
  public Object set(ResultSet rsResults) throws SQLException
  {
      OrderVO vo = new OrderVO();
      vo.setBuyerCookie(rsResults.getString(B2B_BUYER_COOKIE));
      vo.setAsnNumber(rsResults.getString(B2B_ASN_NUMBER));
      vo.setPayloadID(rsResults.getString(B2B_PAYLOAD_ID));
      vo.setCreatedDate(rsResults.getDate(B2B_CREATED_DATE));

      return vo;
  }

}