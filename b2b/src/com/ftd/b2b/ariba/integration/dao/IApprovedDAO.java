package com.ftd.b2b.ariba.integration.dao;

import java.sql.SQLException;
import java.util.List;

import com.ftd.b2b.ariba.common.valueobjects.ApprovedOrderVO;
import com.ftd.framework.common.exceptions.BadConnectionException;
import com.ftd.framework.common.exceptions.FTDSystemException;



/**
 * Interface for working with Approved and Approved Line Item tables
 *
 * @author Kristyn Angelo
 * @version 1.0 
 **/



public interface IApprovedDAO
{

  /**  Deletes records from the Approved and Approved Line Item
   * tables based on the Buyer Cookie
   * @parm Approved Order value object
   * @return String saying whether the delete was successful
   * @throws SQL Exception if delete call to database fails.
   *
   * @author Kristyn Angelo
   */
  public String delete(ApprovedOrderVO vo) throws SQLException,
                                                  BadConnectionException,
                                                  FTDSystemException;

  /**  Inserts records into the Approved and Approved Line Item
   * tables based on the Buyer Cookie
   * @parm Approved Order value object
   * @return String saying whether the insert was successful
   * @throws SQL Exception if insert call to database fails.
   *
   * @author Kristyn Angelo
   */
  public String insert(ApprovedOrderVO vo) throws SQLException,
                                                  BadConnectionException,
                                                  FTDSystemException;

  /** Retrieves records from the Approved and Approved Line Item
   * tables based on the Order Number
   * @parm String containing the Order Number
   * @return An Approved Order value object
   * @throws SQL Exception if retrieve call to database fails.
   *
   * @author Kristyn Angelo
   */
  public Object retrieveByOrderNumber(String OrderNumber) throws SQLException,
                                                  BadConnectionException,
                                                  FTDSystemException;

  /** Retrieves records from the Approved Line Item table
   * based on the Buyer Cookie.
   * @parm String containing the buyer cookie
   * @return List of Approved Line Item value objects
   * @throws SQL Exception if retrieve call to database fails.
   *
   * @author Kristyn Angelo
   */
  public List retrieveLineItems(String BuyerCookie) throws SQLException,
                                                           BadConnectionException,
                                                           FTDSystemException;
  
}