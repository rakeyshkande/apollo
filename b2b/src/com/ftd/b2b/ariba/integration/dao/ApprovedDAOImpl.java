package com.ftd.b2b.ariba.integration.dao;

import com.ftd.framework.common.exceptions.BadConnectionException;
import com.ftd.framework.common.exceptions.FTDSystemException;

import com.ftd.b2b.ariba.common.valueobjects.ApprovedOrderVO;
import com.ftd.b2b.ariba.common.valueobjects.ApprovedLineItemVO;
import com.ftd.b2b.integration.dao.*;
import com.ftd.b2b.utils.mail.B2BMailer;
import com.ftd.b2b.constants.*;

import java.util.*;
import java.sql.SQLException;
import java.sql.ResultSet;

/**
 * Implementation class for the working with the Approved
 * and Approved Line Item tables.  Functions include retrieve,
 * delete and insert into the tables.
 *
 * @author Kristyn Angelo
 * @version 1.0
 */

public class ApprovedDAOImpl extends DAO implements IApprovedDAO, 
                                                    B2BConstants,
                                                    B2BDatabaseConstants
{
  private static final String selectSQL = "{ ? = call B2B.APPROVEDINFO_FUNC(?) }";
  private static final String insertSQL = 
  "{ ? = call B2B.APPROVEDINSERT_FUNC(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }";
  private static final String deleteSQL = "{ ? = call B2B.APPROVEDDELETE_FUNC(?) }";
  
  private static final int ITEM_DESCRIPTION_SAFE_LENGTH = 180;
  
  public ApprovedDAOImpl()
  {
    super(LOG_CATEGORY_APPROVED_DAO);
  }

  public ApprovedDAOImpl(boolean selfManagedTransaction)
  {
    super(LOG_CATEGORY_APPROVED_DAO, selfManagedTransaction);
  }

  /**  Sets the Approved order value object fields 
   * @parm Result Set
   * @return Approved order value object
   * @throws SQL Exception if the fields are not returned from the
   *         database call.
   *
   * @author Kristyn Angelo
   */
  public Object set(ResultSet rsResults) throws SQLException
  {
    ApprovedOrderVO vo = new ApprovedOrderVO();
    vo.setBuyerCookie(rsResults.getString(B2B_BUYER_COOKIE));
    vo.setAsnNumber(rsResults.getString(B2B_ASN_NUMBER));
    vo.setPayloadID(rsResults.getString(B2B_PAYLOAD_ID));
    vo.setCreatedDate(rsResults.getDate(B2B_CREATED_DATE));
    vo.setApprovedDate(rsResults.getDate(B2B_APPROVED_DATE));

    return vo;
  }

  /**  Deletes records from the Approved and Approved Line Item
   * tables based on the Buyer Cookie
   * @parm Approved Order value object
   * @return String saying whether the delete was successful
   * @throws SQL Exception if delete call to database fails.
   *
   * @author Kristyn Angelo
   */
  public String delete(ApprovedOrderVO vo) throws SQLException,
                                                  BadConnectionException,
                                                  FTDSystemException
  {

    if (!isSelfManagedTransaction())
    {
      throw new SQLException("Approved delete database call must manage transactions");
    }
    
    String BuyerCookie = vo.getBuyerCookie();
    String returnVal = "";

    try
    {
      returnVal = super.execute(deleteSQL, new Object[] {BuyerCookie} );
    }
    catch (SQLException sqle)
    {
      rollback();  // This should no longer do anything - remove this someday
      throw sqle;
    }
    commit();  // This should no longer do anything - remove this someday

    return returnVal;
  }

  /**  Inserts records into the Approved and Approved Line Item
   * tables based on the Buyer Cookie
   * @parm Approved Order value object
   * @return String saying whether the insert was successful
   * @throws SQL Exception if insert call to database fails.
   *
   * @author Kristyn Angelo
   */
  public String insert(ApprovedOrderVO vo) throws SQLException,
                                                  BadConnectionException,
                                                  FTDSystemException
  {
    //Force insert to be self managed
    //setSelfManagedTransaction(true);    

    wasAnyFieldTruncated = false;
    String returnVal = "";
    String BuyerCookie = vo.getBuyerCookie();
    String ASNNumber = vo.getAsnNumber();
    String PayloadID = vo.getPayloadID();
    java.util.Date CreatedDate = vo.getCreatedDate();
    java.util.Date ApprovedDate = vo.getApprovedDate();

    List lineItemList = vo.getLineItems();
    ListIterator i = lineItemList.listIterator();
    while (i.hasNext())
    {
      ApprovedLineItemVO lineVO = (ApprovedLineItemVO)i.next();
      String FTDOrderNumber = truncIt(lineVO.getOrderNumber(), MAX_LEN_ORDERNUM);
      String AribaPONumber = lineVO.getAribaPONumber();
      int AribaLineNumber = lineVO.getAribaLineNumber();
      float TaxAmount = lineVO.getTaxAmount();
      float OrderTotal = lineVO.getOrderTotal();
      String BillToFirstName = truncIt(lineVO.getBillToFirstName(), MAX_LEN_NAME);
      String BillToLastName = truncIt(lineVO.getBillToLastName(), MAX_LEN_NAME);
      String BillToAddressLine1 = truncIt(lineVO.getBillToAddressLine1(), MAX_LEN_ADDRESS);
      String BillToAddressLine2 = truncIt(lineVO.getBillToAddressLine2(), MAX_LEN_ADDRESS);      
      String BillToCity = truncIt(lineVO.getBillToCity(), MAX_LEN_CITY);
      String BillToState = truncIt(lineVO.getBillToState(), MAX_LEN_STATE);
      String BillToZipCode = truncIt(lineVO.getBillToZipCode(), MAX_LEN_ZIP);
      String BillToCountry = truncIt(lineVO.getBillToCountry(), MAX_LEN_COUNTRY);
      String BillToHomePhone = truncIt(lineVO.getBillToHomePhone(), MAX_LEN_PHONE);
      String BillToWorkPhone = truncIt(lineVO.getBillToWorkPhone(), MAX_LEN_PHONE);
      String BillToWorkExtension = truncIt(lineVO.getBillToWorkExtension(), MAX_LEN_EXTENSION);
      String BillToFaxNumber = truncIt(lineVO.getBillToFaxNumber(), MAX_LEN_PHONE);
      String BillToEmail = truncIt(lineVO.getBillToEmail(), MAX_LEN_EMAIL);
      String ShipToFirstName = truncIt(lineVO.getShipToFirstName(), MAX_LEN_NAME);
      String ShipToLastName = truncIt(lineVO.getShipToLastName(), MAX_LEN_NAME);
      String ShipToAddressLine1 = truncIt(lineVO.getShipToAddressLine1(), MAX_LEN_ADDRESS);
      String ShipToAddressLine2 = truncIt(lineVO.getShipToAddressLine2(), MAX_LEN_ADDRESS);      
      String ShipToCity = truncIt(lineVO.getShipToCity(), MAX_LEN_CITY);
      String ShipToState = truncIt(lineVO.getShipToState(), MAX_LEN_STATE);
      String ShipToZipCode = truncIt(lineVO.getShipToZipCode(), MAX_LEN_ZIP);
      String ShipToCountry = truncIt(lineVO.getShipToCountry(), MAX_LEN_COUNTRY);
      String ShipToHomePhone = truncIt(lineVO.getShipToHomePhone(), MAX_LEN_PHONE);
      String ShipToWorkPhone = truncIt(lineVO.getShipToWorkPhone(), MAX_LEN_PHONE);
      String ShipToWorkExtension = truncIt(lineVO.getShipToWorkExtension(), MAX_LEN_EXTENSION);
      String ShipToFaxNumber = truncIt(lineVO.getShipToFaxNumber(), MAX_LEN_PHONE);
      String ShipToEmail = truncIt(lineVO.getShipToEmail(), MAX_LEN_EMAIL);
      String ShipToType = truncIt(lineVO.getShipToBusinessType(), CONSTANT_MAX_SHIP_TO_BUS_TYPE);
      String ShipToTypeName = truncIt(lineVO.getShipToBusinessName(), CONSTANT_MAX_SHIP_TO_BUS_NAME);
      String Occasion = truncIt(lineVO.getOccasion(), MAX_LEN_OCCASION);
      String ItemNumber = truncIt(lineVO.getItemNumber(), MAX_LEN_ITEMNUM);
      String ItemDescription = truncIt(encode(lineVO.getItemDescription()), MAX_LEN_DESCRIPTION);
      float ItemPrice = lineVO.getItemPrice();
      int AddOnCount = lineVO.getAddOnCount();
      String AddOnDetail = lineVO.getAddOnDetail();
      float ServiceFee = lineVO.getServiceFee();
      java.util.Date DeliveryDate = lineVO.getDeliveryDate();
      String CardMessage = truncIt(lineVO.getCardMessage(), MAX_LEN_CARD);
      String SpecialInstructions = truncIt(lineVO.getSpecialInstructions(), MAX_LEN_CARD);
      String MasterOrderNumber = truncIt(lineVO.getMasterOrderNumber(), MAX_LEN_MASTER);
      String UNSPSCCode = truncIt(lineVO.getItemUNSPSCCode(), MAX_LEN_UNSPSC);
      String AribaCostCenter = truncIt(lineVO.getAribaCostCenter(), MAX_LEN_CCENTER);
      String AMSProjectCode = truncIt(lineVO.getAMSProjectCode(), MAX_LEN_AMSCODE);
      float ExtraShippingFee = lineVO.getExtraShippingFee();
      float DropShipCharges = lineVO.getDropShipCharges();

      /* 8/4/03 ed mueller, new field */
      float retailPrice = lineVO.getRetailPrice();
      /* 8/4/03 end */

      try
      {
        returnVal = super.execute(insertSQL, new Object[] {BuyerCookie, FTDOrderNumber,
                                                          AribaPONumber, new Integer(AribaLineNumber),
                                                          new Float(TaxAmount), new Float(OrderTotal),
                                                          BillToFirstName, BillToLastName,
                                                          BillToAddressLine1, BillToAddressLine2,
                                                          BillToCity, BillToState,
                                                          BillToZipCode, BillToCountry,
                                                          BillToHomePhone, BillToWorkPhone,
                                                          BillToWorkExtension, BillToFaxNumber,
                                                          BillToEmail, ShipToFirstName, ShipToLastName,
                                                          ShipToAddressLine1, ShipToAddressLine2,
                                                          ShipToCity, ShipToState,
                                                          ShipToZipCode, ShipToCountry,
                                                          ShipToHomePhone, ShipToWorkPhone,
                                                          ShipToWorkExtension, ShipToFaxNumber,
                                                          ShipToEmail, ShipToType, ShipToTypeName, Occasion, ItemNumber,
                                                          ItemDescription, new Float(ItemPrice),
                                                          new Integer(AddOnCount), AddOnDetail,
                                                          new Float(ServiceFee), DeliveryDate,
                                                          CardMessage, SpecialInstructions, 
                                                          MasterOrderNumber, UNSPSCCode,
                                                          AribaCostCenter, AMSProjectCode, ASNNumber,
                                                          PayloadID, CreatedDate, ApprovedDate,new Float(retailPrice),
                                                          new Float(ExtraShippingFee), new Float(DropShipCharges)}
                                                          );
      }
      catch (SQLException sqle)
      {
        rollback();  // This should no longer do anything - remove this someday
        throw sqle;
      }
    }
    commit();  // This should no longer do anything - remove this someday

    // Send alert if any field was truncated
    try {
        if (wasAnyFieldTruncated == true) {
            String[] msg = new String[] {((BuyerCookie!=null)?BuyerCookie:" ")};  
            B2BMailer.getInstance().send(this,"insert",ERROR_CODE_VALUE_TOO_LARGE_FOR_COLUMN,msg,B2BMailer.ACTION2);          
        }
    } catch (Exception e) {
        logger.error("Failed to send alert that fields were truncated, but continuing anyhow.  Exception was: " + e);
    }
    return returnVal;
  }
  

  /** Retrieves records from the Approved and Approved Line Item
   * tables based on the Order Number
   * @parm String containing the Order Number
   * @return An Approved Order value object
   * @throws SQL Exception if retrieve call to database fails.
   *
   * @author Kristyn Angelo
   */
  public Object retrieveByOrderNumber(String OrderNumber) throws SQLException,
                                                  BadConnectionException,
                                                  FTDSystemException
  {
    ApprovedOrderVO approvedVO = new ApprovedOrderVO();
    List orderList = super.retrieve(selectSQL, new Object[] {OrderNumber});
    
    if (orderList == null)
    {
      return null;
    }
    else if (orderList.size() > 1)
    {
      throw new SQLException ("Multiple Lists of Approved Line Items were returned for Order Number: " + OrderNumber);
    }
    else
    {
      approvedVO = (ApprovedOrderVO)orderList.get(0);
      String BuyerCookie = approvedVO.getBuyerCookie();
      List lineList = retrieveLineItems(BuyerCookie);
      if ( lineList.isEmpty() )
      {
        throw new SQLException("No Approved Line Items were returned for BuyerCookie: " + BuyerCookie);
      }
      approvedVO.setLineItems((ArrayList)lineList);
    }
    return approvedVO;
  }

  /** Retrieves records from the Approved Line Item table
   * based on the Buyer Cookie.
   * @parm String containing the buyer cookie
   * @return List of Approved Line Item value objects
   * @throws SQL Exception if retrieve call to database fails.
   *
   * @author Kristyn Angelo
   */
  public List retrieveLineItems(String BuyerCookie) throws SQLException,
                                                           BadConnectionException,
                                                           FTDSystemException
  {
      ApprovedLineDAOImpl approvedLine = new ApprovedLineDAOImpl();
      return approvedLine.retrieveLines(BuyerCookie);  
  }

}