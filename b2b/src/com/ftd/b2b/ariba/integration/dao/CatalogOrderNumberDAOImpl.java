package com.ftd.b2b.ariba.integration.dao;

import com.ftd.framework.common.exceptions.BadConnectionException;
import com.ftd.framework.common.exceptions.FTDSystemException;
import com.ftd.framework.common.utilities.ResourceManager;

import com.ftd.b2b.integration.dao.*;
import com.ftd.b2b.constants.B2BConstants;

import java.util.*;
import java.sql.*;
import javax.sql.*;

/**
 * Implementation class for the working with the Catalog Order Number sequence.
 *
 * @author Kristyn Angelo
 * @version 1.0
 */

public class CatalogOrderNumberDAOImpl extends DAO implements ICatalogOrderNumberDAO,
                                                              B2BConstants
{
  private static String CATALOG_ORDER_PREFIX;
  private static final String SQL = "{? = call B2B.CATALOGORDERNUMBER_FUNC() }";

  public CatalogOrderNumberDAOImpl()
  {
    super(LOG_CATEGORY_CATALOGORDERNUMBER_DAO);

    ResourceManager resourceManager = ResourceManager.getInstance();  
    CATALOG_ORDER_PREFIX = resourceManager.getProperty(PROPERTY_B2B_PROPERTY_FILE, 
                                               PROPERTY_CATALOG_ORDER_PREFIX);
  }

  /** Retrieve the next available Catalog Order Number from the Database.
   * @parm no parameters required
   * @return String containg catalog order number
   * @throws SQL Exception if retrieve call to database fails.
   *
   * @author Kristyn Angelo
   */
   public String retrieveCatalogNumber() throws SQLException, 
                                   BadConnectionException,
                                   FTDSystemException
  {
      return ( CATALOG_ORDER_PREFIX.concat(super.execute(SQL, null)) );
  }

  /**  Object implemented but not used
   * @parm Result Set
   * @return null
   * @throws SQL Exception if the fields are not returned from the
   *         database call.
   *
   * @author Kristyn Angelo
   */
  public Object set(ResultSet rsResults) throws SQLException
  {
    return null;
  }
}