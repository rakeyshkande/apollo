package com.ftd.b2b.ariba.integration.dao;

import com.ftd.framework.common.exceptions.BadConnectionException;
import com.ftd.framework.common.exceptions.FTDSystemException;

import com.ftd.b2b.ariba.common.valueobjects.LineItemVO;
import com.ftd.b2b.integration.dao.*;
import com.ftd.b2b.constants.*;

import java.util.*;
import java.sql.*;
import javax.sql.*;

/**
 * Implementation class for the working with the Pending Line Item table
 *
 * @author Kristyn Angelo
 * @version 1.0
 */
 
public class PendingLineDAOImpl extends DAO implements IPendingLineDAO, 
                                                       B2BConstants,
                                                       B2BDatabaseConstants
{
  private static final String selectSQL = "{? = call B2B.PENDINGLINEINFO_FUNC(?) }";
  private static final String singleSelectSQL = "{? = call B2B.PENDINGSINGLELINEINFO_FUNC(?,?) }";
  private static final String buyerCookieSQL = "{? = call B2B.PENDINGLINEBUYERCOOKIE_FUNC(?) }";

  public PendingLineDAOImpl()
  {
      super(LOG_CATEGORY_PENDING_LINE_DAO);
  }

  /** Retrieves records from the Pending Line Item table
   * based on the Buyer Cookie.
   * @parm String containing the buyer cookie
   * @return List of Pending Line Item value objects
   * @throws SQL Exception if retrieve call to database fails.
   *
   * @author Kristyn Angelo
   */
  public List retrieveLines(String BuyerCookie) throws SQLException, 
                                                  BadConnectionException,
                                                 FTDSystemException
  {
          return super.retrieve(selectSQL, new Object[] {BuyerCookie});
  }

    /** Retrieves a record from the Pending Line Item table
   * based on the FTD Order Number and Item Number
   * @parm String containing the FTD Order Number and a String containing Item Number
   * @return List of Pending Line Item value objects
   * @throws SQL Exception if retrieve call to database fails.
   *
   * @author Kristyn Angelo
   */
  public List retrieveSingleLine(String FTDOrderNumber, String ItemNumber) throws SQLException, 
                                                                           BadConnectionException,
                                                                           FTDSystemException
  {
          return super.retrieve(singleSelectSQL, new Object[] {FTDOrderNumber, ItemNumber});
  }

 /** Retrieves Buyer Cookie for an FTD Order fromPending Line Item table
   * @parm String containing the FTD Order Number 
   * @return String containing Buyer Cookier
   * @throws SQL Exception if retrieve call to database fails.
   *
   * @author Kristyn Angelo
   */
  public String retrieveBuyerCookie(String FTDOrderNumber) throws SQLException, 
                                                                  BadConnectionException,
                                                                  FTDSystemException
  {
          return super.execute(buyerCookieSQL, new Object[] {FTDOrderNumber});
  }



  /**  Sets the Pending Line Item value object fields 
   * @parm Result Set
   * @return Pending Line Item value object
   * @throws SQL Exception if the fields are not returned from the
   *         database call.
   *
   * @author Kristyn Angelo
   */
  public Object set(ResultSet rsResults) throws SQLException
  {
      LineItemVO lvo = new LineItemVO();
      lvo.setAribaBuyerCookie(rsResults.getString(B2B_BUYER_COOKIE));
      lvo.setOrderNumber(rsResults.getString(B2B_FTD_ORDER_NUMBER));
      lvo.setTaxAmount(rsResults.getFloat(B2B_TAX_AMOUNT));
      lvo.setOrderTotal(rsResults.getFloat(B2B_ORDER_TOTAL));
      lvo.setOccasion(rsResults.getString(B2B_OCCASION));
      lvo.setItemNumber(rsResults.getString(B2B_ITEM_NUMBER));
      lvo.setItemDescription(rsResults.getString(B2B_ITEM_DESCRIPTION));
      lvo.setItemPrice(rsResults.getFloat(B2B_ITEM_PRICE));
      lvo.setAddOnCount(rsResults.getInt(B2B_ADDON_COUNT));
      lvo.setAddOnDetail(rsResults.getString(B2B_ADDON_DETAIL));
      lvo.setServiceFee(rsResults.getFloat(B2B_SERVICE_FEE));
      lvo.setDeliveryDate(rsResults.getDate(B2B_DELIVERY_DATE));
      lvo.setMasterOrderNumber(rsResults.getString(B2B_MASTER_ORDER_NUMBER));
      lvo.setItemUNSPSCCode(rsResults.getString(B2B_UNSPSC_CODE));

    /* 6/25/03 Ed Mueller
        This chunk was added because these fields were added to the Pending & Arhicve table. */
    lvo.setShipToFirstName(rsResults.getString(B2B_SHIP_TO_FIRST_NAME));
    lvo.setShipToLastName(rsResults.getString(B2B_SHIP_TO_LAST_NAME));
    lvo.setShipToAddressLine1(rsResults.getString(B2B_SHIP_TO_ADDRESS_LINE1));
    lvo.setShipToAddressLine2(rsResults.getString(B2B_SHIP_TO_ADDRESS_LINE2));
    lvo.setShipToCity(rsResults.getString(B2B_SHIP_TO_CITY));
    lvo.setShipToState(rsResults.getString(B2B_SHIP_TO_STATE));
    lvo.setShipToZipCode(rsResults.getString(B2B_SHIP_TO_ZIP_CODE));
    lvo.setShipToCountry(rsResults.getString(B2B_SHIP_TO_COUNTRY));
    lvo.setShipToWorkPhone(rsResults.getString(B2B_SHIP_TO_PHONE));
    lvo.setShipToWorkExtension(rsResults.getString(B2B_SHIP_TO_EXT));
    lvo.setCardMessage(rsResults.getString(B2B_CARD_MESSAGE));
    lvo.setSpecialInstructions(rsResults.getString(B2B_SPECIAL_INSTRUCTIONS));
    /* 6/25/03....end */
    lvo.setShipToBusinessType(rsResults.getString(B2B_SHIP_TO_BUSINESS_TYPE));
    lvo.setShipToBusinessName(rsResults.getString(B2B_SHIP_TO_BUSINESS_NAME));

    /* 8/4/03 Ed Mueller field added for retail vs discount price changes */
     lvo.setRetailPrice(rsResults.getFloat(B2B_RETAIL_PRICE));    
    /* 8/4/03....end */

      // BillTo added for Spec #2255
      lvo.setBillToFirstName(rsResults.getString(B2B_BILL_TO_FIRST_NAME));
      lvo.setBillToLastName(rsResults.getString(B2B_BILL_TO_LAST_NAME));
      lvo.setBillToAddressLine1(rsResults.getString(B2B_BILL_TO_ADDRESS_LINE1));
      lvo.setBillToAddressLine2(rsResults.getString(B2B_BILL_TO_ADDRESS_LINE2));
      lvo.setBillToCity(rsResults.getString(B2B_BILL_TO_CITY));
      lvo.setBillToState(rsResults.getString(B2B_BILL_TO_STATE));
      lvo.setBillToZipCode(rsResults.getString(B2B_BILL_TO_ZIP_CODE));
      lvo.setBillToCountry(rsResults.getString(B2B_BILL_TO_COUNTRY));
      lvo.setBillToWorkPhone(rsResults.getString(B2B_BILL_TO_WORK_PHONE));
      lvo.setBillToWorkPhoneExt(rsResults.getString(B2B_BILL_TO_WORK_EXT));
      lvo.setBillToEmail(rsResults.getString(B2B_BILL_TO_EMAIL));

      return lvo;     
  }
}