package com.ftd.b2b.ariba.integration.dao;

import com.ftd.framework.common.exceptions.BadConnectionException;
import com.ftd.framework.common.exceptions.FTDSystemException;
import com.ftd.framework.common.utilities.ResourceManager;

import com.ftd.b2b.integration.dao.*;
import com.ftd.b2b.constants.B2BConstants;

import java.util.*;
import java.sql.*;
import javax.sql.*;

/**
 * Implementation class for the working with the Punchout Order Number sequence.
 *
 * @author Kristyn Angelo
 * @version 1.0
 */

public class PunchoutOrderNumberDAOImpl extends DAO implements IPunchoutOrderNumberDAO,
                                                               B2BConstants
{
  private static String PUNCHOUT_ORDER_PREFIX;
  private static final String SQL = "{? = call B2B.PUNCHOUTORDERNUMBER_FUNC() }";

  public PunchoutOrderNumberDAOImpl()
  {
    super(LOG_CATEGORY_PUNCHOUTORDERNUMBER_DAO);

    ResourceManager resourceManager = ResourceManager.getInstance();  
    PUNCHOUT_ORDER_PREFIX = resourceManager.getProperty(PROPERTY_B2B_PROPERTY_FILE, 
                                               PROPERTY_PUNCHOUT_ORDER_PREFIX);

  }

  /** Retrieve the next available Punchout Order Number from the Database.
   * @parm no parameters required
   * @return String containg catalog order number
   * @throws SQL Exception if retrieve call to database fails.
   *
   * @author Kristyn Angelo
   */
   public String retrievePunchoutNumber() throws SQLException, 
                                   BadConnectionException,
                                   FTDSystemException
  {
      return ( PUNCHOUT_ORDER_PREFIX.concat(super.execute(SQL, null)) );
  }

  /**  Object implemented but not used
   * @parm Result Set
   * @return null
   * @throws SQL Exception if the fields are not returned from the
   *         database call.
   *
   * @author Kristyn Angelo
   */
  public Object set(ResultSet rsResults) throws SQLException
  {
    return null;
  }
}