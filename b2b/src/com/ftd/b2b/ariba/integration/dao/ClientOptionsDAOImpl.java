package com.ftd.b2b.ariba.integration.dao;

import java.util.ArrayList;

import com.ftd.framework.common.exceptions.BadConnectionException;
import com.ftd.framework.common.exceptions.FTDSystemException;

import com.ftd.b2b.ariba.common.valueobjects.ClientOptionsVO;
import com.ftd.b2b.integration.dao.*;
import com.ftd.b2b.constants.*;

import java.util.*;
import java.sql.*;
import javax.sql.*;

/**
 * Implementation class for working with the Client Options table
 *
 */

public class ClientOptionsDAOImpl extends DAO implements IClientOptionsDAO, 
                                                     B2BConstants,
                                                     B2BDatabaseConstants
{
  private static final String SQL = "{? = call B2B.CLIENTOPTIONS_FUNC() }";
  
  public ClientOptionsDAOImpl()
  {
    super(LOG_CATEGORY_CLIENTOPTIONS_DAO);
  }

  /** Retrieves all records from the Client Options table.
   * @return List of Client Option value objects
   * @throws SQL Exception if retrieve call to database fails.
   */
  public List retrieve() throws SQLException, BadConnectionException, FTDSystemException {
    return super.retrieve(SQL);
  }

  /**  Sets the client options value object fields 
   * @parm Result Set
   * @return Client options value object
   * @throws SQL Exception if the fields are not returned from the
   *         database call.
   */
  public Object set(ResultSet rsResults) throws SQLException
  {
      ClientOptionsVO cvo = new ClientOptionsVO();
      cvo.setAsnNumber(rsResults.getString(B2B_CLIENT_OPTS_ASN));
      cvo.setDefaultUnspsc(rsResults.getString(B2B_CLIENT_OPTS_UNSPSC));
      cvo.setServiceFeeUnspsc(rsResults.getString(B2B_CLIENT_OPTS_SVC_FEE_UNSPSC));
      cvo.setTaxUnspsc(rsResults.getString(B2B_CLIENT_OPTS_TAX_UNSPSC));
      return cvo;
  }

}