package com.ftd.b2b.ariba.integration.dao;

import java.util.ArrayList;

import com.ftd.framework.common.exceptions.BadConnectionException;
import com.ftd.framework.common.exceptions.FTDSystemException;

import com.ftd.b2b.ariba.common.valueobjects.URLLookupVO;
import com.ftd.b2b.integration.dao.*;
import com.ftd.b2b.constants.*;

import java.util.*;
import java.sql.*;
import javax.sql.*;

/**
 * Implementation class for the working with the Client URL table
 *
 * @author Kristyn Angelo
 * @version 1.0
 */

public class URLLookupDAOImpl extends DAO implements IURLLookupDAO, 
                                                     B2BConstants,
                                                     B2BDatabaseConstants
{
  private static final String SQL = "{? = call B2B.CLIENTURL_FUNC(?) }";
  
  public URLLookupDAOImpl()
  {
    super(LOG_CATEGORY_URLLOOKUP_DAO);
  }

  /** Retrieves records from the Client URL table
   * based on the ASN Number.
   * @parm String containing the ASN Number
   * @return List of Client URL  value objects
   * @throws SQL Exception if retrieve call to database fails.
   *
   * @author Kristyn Angelo
   */
  public List retrieve(String ASNNumber) throws SQLException, 
                                                  BadConnectionException,
                                                  FTDSystemException
  {
          return super.retrieve(SQL, new Object[] {ASNNumber});
  }

  /**  Sets the Client URL value object fields 
   * @parm Result Set
   * @return Client URL value object
   * @throws SQL Exception if the fields are not returned from the
   *         database call.
   *
   * @author Kristyn Angelo
   */
  public Object set(ResultSet rsResults) throws SQLException
  {
      URLLookupVO vo = new URLLookupVO();
      vo.setAsnNumber(rsResults.getString(B2B_ASN_NUMBER));
      vo.setClientName(rsResults.getString(B2B_CLIENT_NAME));
      vo.setURL(rsResults.getString(B2B_URL));
      return vo;
  }

}