package com.ftd.b2b.ariba.integration.dao;

import com.ftd.b2b.common.utils.XMLParseException;
import com.ftd.b2b.ariba.common.valueobjects.AribaRequestVO;



/**
 * Interface for mining data from a cXML document.
 * Shoud be resusable all Ariba B2B use cases.
 *
 * @author York Davis
 * @version 1.0 
 **/
 public interface IRequestDataMiner 
{
   /**
    * Based on the cXML document passed, extract the parameters and return
    * a value object.
    * 
    * @param  cXML document.
    * @return AribaRequest value object.
    * @throws XMLParseException if the data cannot be parsed.
    *
    * @author York Davis
    **/ 
    public AribaRequestVO mine(String cXML) throws XMLParseException;
}