package com.ftd.b2b.ariba.integration.dao;

import java.sql.SQLException;
import java.util.List;

import com.ftd.framework.common.exceptions.BadConnectionException;
import com.ftd.framework.common.exceptions.FTDSystemException;

/**
 * Interface for working with Add On tables in the Product Database
 *
 * @author Kristyn Angelo
 * @version 1.0 
 **/
 
public interface IAddOnDAO 
{
    
  /** Retrieves records from the Add On tables
   * based on the Add On Type.
   * @parm String containing the Add On Type
   * @return List of Add On value objects
   * @throws SQL Exception if retrieve call to database fails.
   *
   * @author Kristyn Angelo
   */
    public List retrieveByAddOnType(String AddOnType) throws SQLException, 
                                                  BadConnectionException,
                                                  FTDSystemException;
}