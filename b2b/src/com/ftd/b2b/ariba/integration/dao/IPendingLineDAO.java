package com.ftd.b2b.ariba.integration.dao;

import java.sql.SQLException;
import java.util.List;

import com.ftd.framework.common.exceptions.BadConnectionException;
import com.ftd.framework.common.exceptions.FTDSystemException;

/**
 * Interface for working with Pending Line Item tables
 *
 * @author Kristyn Angelo
 * @version 1.0 
 **/
 
public interface IPendingLineDAO
{
  /** Retrieves records from the Pending Line Item table
   * based on the Buyer Cookie.
   * @parm String containing the buyer cookie
   * @return List of Pending Line Item value objects
   * @throws SQL Exception if retrieve call to database fails.
   *
   * @author Kristyn Angelo
   */
    public List retrieveLines(String BuyerCookie) throws SQLException, 
                                                  BadConnectionException,
                                                  FTDSystemException;  

 /** Retrieves a record from the Pending Line Item table
   * based on the FTD Order Number and Item Number
   * @parm String containing the FTD Order Number and a String containing Item Number
   * @return List of Pending Line Item value objects
   * @throws SQL Exception if retrieve call to database fails.
   *
   * @author Kristyn Angelo
   */
  public List retrieveSingleLine(String FTDOrderNumber, String ItemNumber) throws SQLException, 
                                                                           BadConnectionException,
                                                                           FTDSystemException;                                                  

 /** Retrieves Buyer Cookie for an FTD Order fromPending Line Item table
   * @parm String containing the FTD Order Number 
   * @return String containing Buyer Cookier
   * @throws SQL Exception if retrieve call to database fails.
   *
   * @author Kristyn Angelo
   */
  public String retrieveBuyerCookie(String FTDOrderNumber) throws SQLException, 
                                                                  BadConnectionException,
                                                                  FTDSystemException;                                                                           
}