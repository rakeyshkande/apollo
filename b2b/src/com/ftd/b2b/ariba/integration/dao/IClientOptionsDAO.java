package com.ftd.b2b.ariba.integration.dao;

import java.sql.SQLException;
import java.util.List;

import com.ftd.framework.common.exceptions.BadConnectionException;
import com.ftd.framework.common.exceptions.FTDSystemException;

/**
 * Interface for working with Client Options table
 **/
 
public interface IClientOptionsDAO
{
  /** Retrieves records from the Client Options table
   * based on the ASN Number.
   * @parm String containing the ASN Number
   * @return List of Client Option value objects
   * @throws SQL Exception if retrieve call to database fails.
   */
  public List retrieve() throws SQLException, BadConnectionException, FTDSystemException;

}