package com.ftd.b2b.ariba.integration.dao;

import java.sql.SQLException;

import com.ftd.framework.common.exceptions.BadConnectionException;
import com.ftd.framework.common.exceptions.FTDSystemException;

/**
 * Interface for working with the catalog order number sequence.
 *
 * @author Kristyn Angelo
 * @version 1.0 
 **/
 
public interface ICatalogOrderNumberDAO
{
    /** Retrieve the next available Catalog Order Number from the Database.
   * @parm no parameters required
   * @return String containg catalog order number
   * @throws SQL Exception if retrieve call to database fails.
   *
   * @author Kristyn Angelo
   */
   public String retrieveCatalogNumber() throws SQLException, 
                                   BadConnectionException,
                                   FTDSystemException;
}