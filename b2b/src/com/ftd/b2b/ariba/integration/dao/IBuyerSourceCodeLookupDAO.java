package com.ftd.b2b.ariba.integration.dao;

import java.sql.SQLException;

import com.ftd.framework.common.exceptions.BadConnectionException;
import com.ftd.framework.common.exceptions.FTDSystemException;

/**
 * Interface for working with Buyer Source Code table
 *
 * @author Kristyn Angelo
 * @version 1.0 
 **/
 
public interface IBuyerSourceCodeLookupDAO
{
  /** Retrieve source code value from the Buyer_Source_Code table
   * based on the ASN Number.
   * @parm String containing the ASN Number
   * @return String containing the Buyer Source Code
   * @throws SQL Exception if retrieve call to database fails.
   *
   * @author Kristyn Angelo
   */
  public String retrieveByASNNumber(String ASNNumber) throws SQLException, 
                                                  BadConnectionException,
                                                  FTDSystemException;
}