package com.ftd.b2b.ariba.integration.dao;

import com.ftd.framework.common.exceptions.BadConnectionException;
import com.ftd.framework.common.exceptions.FTDSystemException;

import com.ftd.b2b.integration.dao.*;
import com.ftd.b2b.constants.B2BConstants;

import java.util.*;
import java.sql.*;
import javax.sql.*;

/**
 * Implementation class for the working with the country cross-reference table.
 *
 * @author Kristyn Angelo
 * @version 1.0
 */

public class CountryCodeDAOImpl extends DAO implements ICountryCodeDAO,
                                                       B2BConstants
{

  private static final String SQL = "{? = call B2B.COUNTRYXREF_FUNC(?) }";
  private static final String SQLByName = "{? = call B2B.COUNTRYXREFBYNAME_FUNC(?) }";
  
  public CountryCodeDAOImpl()
  {
    super(LOG_CATEGORY_COUNTRYCODE_DAO);
  }

  /** Retrieve the 2 character country code from the country cross-reference table
   * @parm String containing country code received on the Approved PO
   * @return String containg 2 character country code
   * @throws SQL Exception if retrieve call to database fails.
   *
   * @author Kristyn Angelo
   */
   public String retrieveCountryCode(String countryCode) throws SQLException, 
                                                                BadConnectionException,
                                                                FTDSystemException
  {
          return super.execute(SQL, new Object[] {countryCode} );
  }

  /** Retrieve the 2 character country code from the country cross-reference table
   * @parm String containing country code received on the Approved PO
   * @return String containg 2 character country code
   * @throws SQL Exception if retrieve call to database fails.
   *
   * @author Kristyn Angelo
   */
   public String retrieveCountryCodeByName(String name) throws SQLException, 
                                                                BadConnectionException,
                                                                FTDSystemException
  {
          return super.execute(SQLByName, new Object[] {name} );
  }
  
  /**  Object implemented but not used
   * @parm Result Set
   * @return null
   * @throws SQL Exception if the fields are not returned from the
   *         database call.
   *
   * @author Kristyn Angelo
   */
  public Object set(ResultSet rsResults) throws SQLException
  {
    return null;
  }

}