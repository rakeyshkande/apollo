package com.ftd.b2b.ariba.integration.dao;

import java.sql.SQLException;

import com.ftd.framework.common.exceptions.BadConnectionException;
import com.ftd.framework.common.exceptions.FTDSystemException;

/**
 * Interface for working with the country cross-reference table.
 *
 * @author Kristyn Angelo
 * @version 1.0 
 **/
 
public interface ICountryCodeDAO
{
    /** Retrieve the 2 character country code from the country cross-reference table
   * @parm String containing country code received on the Approved PO
   * @return String containg 2 character country code
   * @throws SQL Exception if retrieve call to database fails.
   *
   * @author Kristyn Angelo
   */
   public String retrieveCountryCode(String countryCode) throws SQLException, 
                                                                BadConnectionException,
                                                                FTDSystemException;

    /** Retrieve the 2 character country code from the country cross-reference table
   * @parm String containing country name
   * @return String containg 2 character country code
   * @throws SQL Exception if retrieve call to database fails.
   *
   * @author Ed Mueller
   */
   public String retrieveCountryCodeByName(String name) throws SQLException, 
                                                                BadConnectionException,
                                                                FTDSystemException;                                                                
}