package com.ftd.b2b.ariba.integration.dao;

import com.ftd.framework.common.exceptions.BadConnectionException;
import com.ftd.framework.common.exceptions.FTDSystemException;

import com.ftd.b2b.ariba.common.valueobjects.ApprovedLineItemVO;
import com.ftd.b2b.integration.dao.*;
import com.ftd.b2b.utils.mail.B2BMailer;
import com.ftd.b2b.constants.*;

import java.util.List;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Implementation class for the working with the 
 * Approved Line Item table.  
 *
 * @author Kristyn Angelo
 * @version 1.0
 */

public class ApprovedLineDAOImpl extends DAO implements IApprovedLineDAO, 
                                                        B2BConstants,
                                                        B2BDatabaseConstants
{
  private static final String selectSQL = "{? = call B2B.APPROVEDLINEINFO_FUNC(?) }";
  private static final String insertSQL = 
    "{? = call B2B.APPROVEDLINEINSERT_FUNC(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }";  
  private static final String buyerCookieSQL = "{? = call B2B.APPROVEDLINEBUYERCOOKIE_FUNC(?) }";   

  public ApprovedLineDAOImpl()
  {
      super(LOG_CATEGORY_APPROVED_LINE_DAO);
  }

  /** Inserts a new Approved Line into the Approved Line Item table
   * @parm Approved Line Item Value Object
   * @return String with a value of true or false
   * @throws SQL Exception if retrieve call to database fails.
   *
   * @author Kristyn Angelo
   */
  public String insertLine(ApprovedLineItemVO voApprovedLine, String BuyerCookie) throws SQLException, 
                                                                                 BadConnectionException,
                                                                                 FTDSystemException
  {
      wasAnyFieldTruncated = false;
      String FTDOrderNumber = truncIt(voApprovedLine.getOrderNumber(), MAX_LEN_ORDERNUM);
      String AribaPONumber = voApprovedLine.getAribaPONumber();
      int AribaLineNumber = voApprovedLine.getAribaLineNumber();
      float TaxAmount = voApprovedLine.getTaxAmount();
      float OrderTotal = voApprovedLine.getOrderTotal();
      String BillToFirstName = truncIt(voApprovedLine.getBillToFirstName(), MAX_LEN_NAME);
      String BillToLastName = truncIt(voApprovedLine.getBillToLastName(), MAX_LEN_NAME);
      String BillToAddressLine1 = truncIt(voApprovedLine.getBillToAddressLine1(), MAX_LEN_ADDRESS);
      String BillToAddressLine2 = truncIt(voApprovedLine.getBillToAddressLine2(), MAX_LEN_ADDRESS);      
      String BillToCity = truncIt(voApprovedLine.getBillToCity(), MAX_LEN_CITY);
      String BillToState = truncIt(voApprovedLine.getBillToState(), MAX_LEN_STATE);
      String BillToZipCode = truncIt(voApprovedLine.getBillToZipCode(), MAX_LEN_ZIP);
      String BillToCountry = truncIt(voApprovedLine.getBillToCountry(), MAX_LEN_COUNTRY);
      String BillToHomePhone = truncIt(voApprovedLine.getBillToHomePhone(), MAX_LEN_PHONE);
      String BillToWorkPhone = truncIt(voApprovedLine.getBillToWorkPhone(), MAX_LEN_PHONE);
      String BillToWorkExtension = truncIt(voApprovedLine.getBillToWorkExtension(), MAX_LEN_EXTENSION);
      String BillToFaxNumber = truncIt(voApprovedLine.getBillToFaxNumber(), MAX_LEN_PHONE);
      String BillToEmail = truncIt(voApprovedLine.getBillToEmail(), MAX_LEN_EMAIL);
      String ShipToFirstName = truncIt(voApprovedLine.getShipToFirstName(), MAX_LEN_NAME);
      String ShipToLastName = truncIt(voApprovedLine.getShipToLastName(), MAX_LEN_NAME);
      String ShipToAddressLine1 = truncIt(voApprovedLine.getShipToAddressLine1(), MAX_LEN_ADDRESS);
      String ShipToAddressLine2 = truncIt(voApprovedLine.getShipToAddressLine2(), MAX_LEN_ADDRESS);      
      String ShipToCity = truncIt(voApprovedLine.getShipToCity(), MAX_LEN_CITY);
      String ShipToState = truncIt(voApprovedLine.getShipToState(), MAX_LEN_STATE);
      String ShipToZipCode = truncIt(voApprovedLine.getShipToZipCode(), MAX_LEN_ZIP);
      String ShipToCountry = truncIt(voApprovedLine.getShipToCountry(), MAX_LEN_COUNTRY);
      String ShipToHomePhone = truncIt(voApprovedLine.getShipToHomePhone(), MAX_LEN_PHONE);
      String ShipToWorkPhone = truncIt(voApprovedLine.getShipToWorkPhone(), MAX_LEN_PHONE);
      String ShipToWorkExtension = truncIt(voApprovedLine.getShipToWorkExtension(), MAX_LEN_EXTENSION);
      String ShipToFaxNumber = truncIt(voApprovedLine.getShipToFaxNumber(), MAX_LEN_PHONE);
      String ShipToEmail = truncIt(voApprovedLine.getShipToEmail(), MAX_LEN_EMAIL);
      String ShipToBusinessType = truncIt(voApprovedLine.getShipToBusinessType(), CONSTANT_MAX_SHIP_TO_BUS_TYPE);
      String ShipToBusinessName = truncIt(voApprovedLine.getShipToBusinessName(), CONSTANT_MAX_SHIP_TO_BUS_NAME);
      String Occasion = truncIt(voApprovedLine.getOccasion(), MAX_LEN_OCCASION);
      String ItemNumber = truncIt(voApprovedLine.getItemNumber(), MAX_LEN_ITEMNUM);
      String ItemDescription = truncIt(encode(voApprovedLine.getItemDescription()), MAX_LEN_DESCRIPTION);
      float ItemPrice = voApprovedLine.getItemPrice();
      int AddOnCount = voApprovedLine.getAddOnCount();
      String AddOnDetail = voApprovedLine.getAddOnDetail();
      float ServiceFee = voApprovedLine.getServiceFee();
      java.util.Date DeliveryDate = voApprovedLine.getDeliveryDate();
      String CardMessage = truncIt(voApprovedLine.getCardMessage(), MAX_LEN_CARD);
      String SpecialInstructions = truncIt(voApprovedLine.getSpecialInstructions(), MAX_LEN_CARD);
      String MasterOrderNumber = truncIt(voApprovedLine.getMasterOrderNumber(), MAX_LEN_MASTER);
      String UNSPSCCode = truncIt(voApprovedLine.getItemUNSPSCCode(), MAX_LEN_UNSPSC);
      String AribaCostCenter = truncIt(voApprovedLine.getAribaCostCenter(), MAX_LEN_CCENTER);
      String AMSProjectCode = truncIt(voApprovedLine.getAMSProjectCode(), MAX_LEN_AMSCODE);
      float ExtraShippingFee = voApprovedLine.getExtraShippingFee();
      float DropShipCharges = voApprovedLine.getDropShipCharges();

      /* 8/4/03 ed mueller, new field */
      float retailPrice = voApprovedLine.getRetailPrice();
      /* 8/4/03 end */

      String retStr = super.execute(insertSQL, new Object[] {BuyerCookie, FTDOrderNumber,
                                                    AribaPONumber, new Integer(AribaLineNumber),
                                                    new Float(TaxAmount), new Float(OrderTotal),
                                                    BillToFirstName, BillToLastName,
                                                    BillToAddressLine1, BillToAddressLine2,
                                                    BillToCity, BillToState,
                                                    BillToZipCode, BillToCountry,
                                                    BillToHomePhone, BillToWorkPhone,
                                                    BillToWorkExtension, BillToFaxNumber,
                                                    BillToEmail, ShipToFirstName, ShipToLastName,
                                                    ShipToAddressLine1, ShipToAddressLine2,
                                                    ShipToCity, ShipToState,
                                                    ShipToZipCode, ShipToCountry,
                                                    ShipToHomePhone, ShipToWorkPhone,
                                                    ShipToWorkExtension, ShipToFaxNumber,
                                                    ShipToEmail, ShipToBusinessType, ShipToBusinessName, 
                                                    Occasion, ItemNumber,
                                                    ItemDescription, new Float(ItemPrice),
                                                    new Integer(AddOnCount), AddOnDetail,
                                                    new Float(ServiceFee), DeliveryDate,
                                                    CardMessage, SpecialInstructions, 
                                                    MasterOrderNumber, UNSPSCCode,
                                                    AribaCostCenter, AMSProjectCode,new Float(retailPrice),
                                                    new Float(ExtraShippingFee), new Float(DropShipCharges)}
                                                    );

      // Send alert if any field was truncated
      try {
          if (wasAnyFieldTruncated == true) {
              String[] msg = new String[] {((BuyerCookie!=null)?BuyerCookie:" ")};  
              B2BMailer.getInstance().send(this,"insert",ERROR_CODE_VALUE_TOO_LARGE_FOR_COLUMN,msg,B2BMailer.ACTION2);          
          }
      } catch (Exception e) {
          logger.error("Failed to send alert that fields were truncated, but continuing anyhow.  Exception was: " + e);
      }
      
      return retStr;
  }
                                                                     
  /** Retrieves records from the Approved Line Item table
   *  based on the Buyer Cookie
   * @parm String containing the buyer cookie
   * @return List of Approved line item value objects
   * @throws SQL Exception if retrieve call to database fails.
   *
   * @author Kristyn Angelo
   */
  public List retrieveLines(String BuyerCookie) throws SQLException, 
                                                  BadConnectionException,
                                                 FTDSystemException
  {
    return super.retrieve(selectSQL, new Object[] {BuyerCookie});
  }

    /** Retrieves Buyer Cookie from Approved Line Item table
   *  based on the FTD Order Number
   * @parm String containing FTD Order Number
   * @return String containing Buyer Cookie
   * @throws SQL Exception if retrieve call to database fails.
   *
   * @author Kristyn Angelo
   */
  public String retrieveBuyerCookie(String FTDOrderNumber) throws SQLException, 
                                                  BadConnectionException,
                                                 FTDSystemException
  {
    return super.execute(buyerCookieSQL, new Object[] {FTDOrderNumber});
  }

  /**  Sets the Approved line item value object fields 
   * @parm Result Set
   * @return Approved line item value object
   * @throws SQL Exception if the fields are not returned from the
   *         database call.
   *
   * @author Kristyn Angelo
   */
  public Object set(ResultSet rsResults) throws SQLException
  {
    ApprovedLineItemVO lineVO = new ApprovedLineItemVO();
    lineVO.setOrderNumber(rsResults.getString(B2B_FTD_ORDER_NUMBER));
    lineVO.setAribaPONumber(rsResults.getString(B2B_ARIBA_PO_NUMBER));
    lineVO.setAribaLineNumber(rsResults.getInt(B2B_ARIBA_LINE_NUMBER));
    lineVO.setTaxAmount(rsResults.getFloat(B2B_TAX_AMOUNT));
    lineVO.setOrderTotal(rsResults.getFloat(B2B_ORDER_TOTAL));
    lineVO.setBillToFirstName(rsResults.getString(B2B_BILL_TO_FIRST_NAME));
    lineVO.setBillToLastName(rsResults.getString(B2B_BILL_TO_LAST_NAME));
    lineVO.setBillToAddressLine1(rsResults.getString(B2B_BILL_TO_ADDRESS_LINE1));
    lineVO.setBillToAddressLine2(rsResults.getString(B2B_BILL_TO_ADDRESS_LINE2));
    lineVO.setBillToCity(rsResults.getString(B2B_BILL_TO_CITY));
    lineVO.setBillToState(rsResults.getString(B2B_BILL_TO_STATE));
    lineVO.setBillToZipCode(rsResults.getString(B2B_BILL_TO_ZIP_CODE));
    lineVO.setBillToCountry(rsResults.getString(B2B_BILL_TO_COUNTRY));
    lineVO.setBillToHomePhone(rsResults.getString(B2B_BILL_TO_HOME_PHONE));
    lineVO.setBillToWorkPhone(rsResults.getString(B2B_BILL_TO_WORK_PHONE));
    lineVO.setBillToWorkExtension(rsResults.getString(B2B_BILL_TO_WORK_EXT));
    lineVO.setBillToFaxNumber(rsResults.getString(B2B_BILL_TO_FAX_NUMBER));
    lineVO.setBillToEmail(rsResults.getString(B2B_BILL_TO_EMAIL));
    lineVO.setShipToFirstName(rsResults.getString(B2B_SHIP_TO_FIRST_NAME));
    lineVO.setShipToLastName(rsResults.getString(B2B_SHIP_TO_LAST_NAME));
    lineVO.setShipToAddressLine1(rsResults.getString(B2B_SHIP_TO_ADDRESS_LINE1));
    lineVO.setShipToAddressLine2(rsResults.getString(B2B_SHIP_TO_ADDRESS_LINE2));
    lineVO.setShipToCity(rsResults.getString(B2B_SHIP_TO_CITY));
    lineVO.setShipToState(rsResults.getString(B2B_SHIP_TO_STATE));
    lineVO.setShipToZipCode(rsResults.getString(B2B_SHIP_TO_ZIP_CODE));
    lineVO.setShipToCountry(rsResults.getString(B2B_SHIP_TO_COUNTRY));
    lineVO.setShipToHomePhone(rsResults.getString(B2B_SHIP_TO_HOME_PHONE));
    lineVO.setShipToWorkPhone(rsResults.getString(B2B_SHIP_TO_WORK_PHONE));
    lineVO.setShipToWorkExtension(rsResults.getString(B2B_SHIP_TO_WORK_EXT));
    lineVO.setShipToFaxNumber(rsResults.getString(B2B_SHIP_TO_FAX_NUMBER));
    lineVO.setShipToEmail(rsResults.getString(B2B_SHIP_TO_EMAIL));
    lineVO.setShipToBusinessType(rsResults.getString(B2B_SHIP_TO_BUSINESS_TYPE));
    lineVO.setShipToBusinessName(rsResults.getString(B2B_SHIP_TO_BUSINESS_NAME));
    lineVO.setOccasion(rsResults.getString(B2B_OCCASION));
    lineVO.setItemNumber(rsResults.getString(B2B_ITEM_NUMBER));
    lineVO.setItemDescription(rsResults.getString(B2B_ITEM_DESCRIPTION));
    lineVO.setItemPrice(rsResults.getFloat(B2B_ITEM_PRICE));
    lineVO.setAddOnCount(rsResults.getInt(B2B_ADDON_COUNT));
    lineVO.setAddOnDetail(rsResults.getString(B2B_ADDON_DETAIL));
    lineVO.setServiceFee(rsResults.getFloat(B2B_SERVICE_FEE));
    lineVO.setDeliveryDate(rsResults.getDate(B2B_DELIVERY_DATE));
    lineVO.setCardMessage(rsResults.getString(B2B_CARD_MESSAGE));
    lineVO.setSpecialInstructions(rsResults.getString(B2B_SPECIAL_INSTRUCTIONS));
    lineVO.setMasterOrderNumber(rsResults.getString(B2B_MASTER_ORDER_NUMBER));
    lineVO.setItemUNSPSCCode(rsResults.getString(B2B_UNSPSC_CODE));
    lineVO.setAribaCostCenter(rsResults.getString(B2B_ARIBA_COST_CENTER));
    lineVO.setAMSProjectCode(rsResults.getString(B2B_AMS_PROJECT_CODE));
    lineVO.setRetailPrice(rsResults.getFloat(B2B_RETAIL_PRICE));
    
    return lineVO;     
  }
   
}