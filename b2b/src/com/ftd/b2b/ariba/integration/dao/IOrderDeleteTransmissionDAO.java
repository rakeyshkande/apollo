package com.ftd.b2b.ariba.integration.dao;

import java.sql.SQLException;
import java.util.List;

import com.ftd.b2b.ariba.common.valueobjects.OrderDeleteTransmissionVO;
import com.ftd.framework.common.exceptions.BadConnectionException;
import com.ftd.framework.common.exceptions.FTDSystemException;

/**
 * Interface for working with the Order Delete Transmission table
 *
 * @author Kristyn Angelo
 * @version 1.0 
 **/
 
public interface IOrderDeleteTransmissionDAO
{
  /**  
   * Deletes records from the Order Delete Transmission
   * table based on the number of days
   *
   * @parm Number of days to base deletion on.
   * @return String saying whether the delete was successful
   * @throws SQL Exception if delete call to database fails.
   *
   * @author Kristyn Angelo
   */
  public String delete(int holdDays) throws SQLException, 
                                            BadConnectionException,
                                            FTDSystemException;

  /**
   * Deletes records from the Order Delete Transmission
   * table based on the Buyer Cookie
   *
   * @parm Number of days to base deletion on.
   * @return String saying whether the delete was successful
   * @throws SQL Exception if delete call to database fails.
   *
   * @author Kristyn Angelo
   */
  public String deleteByBuyerCookie(String BuyerCookie) throws SQLException, 
                                                                BadConnectionException,
                                                                FTDSystemException;

  /**  Inserts records into the Order Delete Transmission 
   * table 
   * @parm String containing Buyer Cookie and Date containg Send Date
   * @return String saying whether the insert was successful
   * @throws SQL Exception if insert call to database fails.
   *
   * @author Kristyn Angelo
   */          
  public String insert(OrderDeleteTransmissionVO voODT) throws SQLException, 
                                                                 BadConnectionException,
                                                                 FTDSystemException;

  /** Retrieves records from the Order Delete Transmission
   * table based on the Send Date 
   * @parm Number containing the number of days to go back from the current date
   * @return List of Orders to send to Novator
   * @throws SQL Exception if retrieve call to database fails.
   *
   * @author Kristyn Angelo
   */
  public List retrieve(int holdDays) throws SQLException, 
                                  BadConnectionException,
                                  FTDSystemException;  

  /** Retrieves a single record based on the buyer cookie
   * table based on the Send Date 
   * @parm Buyer Cookie of order
   * @return Order Delete Transmission value object
   * @throws SQL Exception if retrieve call to database fails.
   *
   * @author Kristyn Angelo
   */
  public Object retrieveSingleOrder(String BuyerCookie) throws SQLException, 
                                                               BadConnectionException,
                                                               FTDSystemException;                                  
}