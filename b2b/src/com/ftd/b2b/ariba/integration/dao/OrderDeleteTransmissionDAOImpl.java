package com.ftd.b2b.ariba.integration.dao;

import com.ftd.framework.common.exceptions.BadConnectionException;
import com.ftd.framework.common.exceptions.FTDSystemException;

import com.ftd.b2b.ariba.common.valueobjects.OrderDeleteTransmissionVO;
import com.ftd.b2b.integration.dao.*;
import com.ftd.b2b.constants.*;

import java.util.*;
import java.sql.*;
import javax.sql.*;

/**
 * Implementation class for the working with the Pending 
 * and Pending Line Item tables.  Functions include retrieve,
 * delete and insert into the tables.
 *
 * @author Kristyn Angelo
 * @version 1.0
 */

public class OrderDeleteTransmissionDAOImpl extends DAO implements IOrderDeleteTransmissionDAO,
                                                                   B2BConstants,
                                                                   B2BDatabaseConstants
{

  private static final String insertSQL = "{? = call B2B.ORDERDELETETRANSINSERT_FUNC(?,?) }";
  private static final String deleteSQL = "{? = call B2B.ORDERDELETETRANSDELETE_FUNC(?) }";
  private static final String selectSQL = "{? = call B2B.ORDERDELETETRANSSELECT_FUNC(?) }";
  private static final String infoSQL = "{? = call B2B.ORDERDELETETRANSINFO_FUNC(?) }";  
  private static final String deleteBCSQL = "{? = call B2B.ORDERDELETETRANSDELETEBC_FUNC(?) }";

  public OrderDeleteTransmissionDAOImpl()
  {
      super(LOG_CATEGORY_ORDERDELETETRANS_DAO);
  }

  /**
   * Deletes records from the Order Delete Transmission
   * table based on the number of hold days
   *
   * @parm Number of days to base deletion on.
   * @return String saying whether the delete was successful
   * @throws SQL Exception if delete call to database fails.
   *
   * @author Kristyn Angelo
   */
  public String delete(int holdDays) throws SQLException, 
                                            BadConnectionException,
                                            FTDSystemException
  {
      return super.execute(deleteSQL, new Object[] {new Integer(holdDays)} );
  }

  /**
   * Deletes records from the Order Delete Transmission
   * table based on the Buyer Cookie
   *
   * @parm Number of days to base deletion on.
   * @return String saying whether the delete was successful
   * @throws SQL Exception if delete call to database fails.
   *
   * @author Kristyn Angelo
   */
  public String deleteByBuyerCookie(String BuyerCookie) throws SQLException, 
                                                                BadConnectionException,
                                                                FTDSystemException
  {
        return super.execute(deleteBCSQL, new Object[] {BuyerCookie} );
  }

  /**  Inserts records into the Order Delete Transmission 
   * table 
   * @parm String containing Buyer Cookie and Date containg Send Date
   * @return String saying whether the insert was successful
   * @throws SQL Exception if insert call to database fails.
   *
   * @author Kristyn Angelo
   */          
  public String insert(OrderDeleteTransmissionVO voODT) throws SQLException, 
                                                                 BadConnectionException,
                                                                 FTDSystemException
  {
      return super.execute(insertSQL, new Object[] {
                                                      voODT.getBuyerCookie(),
                                                      voODT.getSendDate()
                                                   } );
  }

  /** 
   * Retrieves records from the Order Delete Transmission
   * table based on the Send Date 
   * @parm Number containing the number of days to go back from the current date
   * @return List of Orders to send to Novator
   * @throws SQL Exception if retrieve call to database fails.
   *
   * @author Kristyn Angelo
   */
  public List retrieve(int holdDays) throws SQLException, 
                                            BadConnectionException,
                                            FTDSystemException
  {
      return super.retrieve(selectSQL, new Object[] {new Integer(holdDays)});
  }

  /** Retrieves a single record based on the buyer cookie
   * table based on the Send Date 
   * @parm Buyer Cookie of order
   * @return Order Delete Transmission value object
   * @throws SQL Exception if retrieve call to database fails.
   *
   * @author Kristyn Angelo
   */
  public Object retrieveSingleOrder(String BuyerCookie) throws SQLException, 
                                                                BadConnectionException,
                                                                FTDSystemException
  {
      OrderDeleteTransmissionVO voOrderDelete = new OrderDeleteTransmissionVO();
      List orderList = super.retrieve(infoSQL, new Object[] {BuyerCookie});
                
      if ( orderList == null )
      {
          return null;
      }
      else if (orderList.size() > 1)
      {
        throw new SQLException ("Multiple Order Delete Transmission entries were returned for Buyer Cookie: " + BuyerCookie);
      }
      else
      {
        voOrderDelete = (OrderDeleteTransmissionVO)orderList.get(0);
      }
      return voOrderDelete;

  }

  /**  Sets the Order Delete Transmission order value object fields 
   * @parm Result Set
   * @return Pending order value object
   * @throws SQL Exception if the fields are not returned from the
   *         database call.
   *
   * @author Kristyn Angelo
   */
  public Object set(ResultSet rsResults) throws SQLException
  {
      OrderDeleteTransmissionVO voODT = new OrderDeleteTransmissionVO();
      voODT.setBuyerCookie(rsResults.getString(B2B_BUYER_COOKIE));
      voODT.setSendDate(rsResults.getDate(B2B_SEND_DATE));  

      return voODT;
  }

}