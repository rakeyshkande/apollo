package com.ftd.b2b.ariba.integration.dao;

import java.util.ArrayList;

import com.ftd.framework.common.exceptions.BadConnectionException;
import com.ftd.framework.common.exceptions.FTDSystemException;

import com.ftd.b2b.ariba.common.valueobjects.AddOnVO;
import com.ftd.b2b.integration.dao.*;
import com.ftd.b2b.constants.*;


import java.util.*;
import java.sql.*;
import javax.sql.*;

/**
 * Implementation class for the working with the Add On tables
 * in the Product Database.  
 *
 * @author Kristyn Angelo
 * @version 1.0
 */

public class AddOnDAOImpl extends DAO implements IAddOnDAO, 
                                                 B2BConstants,
                                                 B2BDatabaseConstants
{

  private static final String SQL = "{? = call B2B.ADDONINFO_FUNC(?) }";

  public AddOnDAOImpl()
  {
    super(LOG_CATEGORY_ADDON_DAO);
  }

  /** Retrieves records from the Add On table in the Product Database
   * @parm String containing the Add On Type (i.e. 'A', 'B', 'C')
   * @return List of Add On infomation 
   * @throws SQL Exception if retrieve call to database fails.
   *
   * @author Kristyn Angelo
   */
   public List retrieveByAddOnType(String AddOnType) throws SQLException, 
                                                  BadConnectionException,
                                                  FTDSystemException
  {
          return super.retrieve(SQL, new Object[] {AddOnType});
  }

  /**  Sets the Add On value object fields
   * @parm Result Set
   * @return Add On value object
   * @throws SQL Exception if the fields are not returned from the
   *         database call.
   *
   * @author Kristyn Angelo
   */
  public Object set(ResultSet rsResults) throws SQLException
  {
      AddOnVO vo = new AddOnVO();
      vo.setAddOnId(rsResults.getString(B2B_ADDON_ID));
      vo.setAddOnType(rsResults.getString(B2B_ADDON_TYPE));
      vo.setAddOnDescription(rsResults.getString(B2B_ADDON_DESCRIPTION));
      vo.setAddOnPrice(rsResults.getFloat(B2B_ADDON_PRICE));
      vo.setAddOnUNSPSC(rsResults.getString(B2B_ADDON_UNSPSC));
      return vo;
  }


}