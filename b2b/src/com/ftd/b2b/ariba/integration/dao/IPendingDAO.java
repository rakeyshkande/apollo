package com.ftd.b2b.ariba.integration.dao;

import java.sql.SQLException;
import java.util.List;

import com.ftd.b2b.ariba.common.valueobjects.IOrderVO;
import com.ftd.b2b.ariba.common.valueobjects.OrderVO;
import com.ftd.framework.common.exceptions.BadConnectionException;
import com.ftd.framework.common.exceptions.FTDSystemException;
import com.ftd.b2b.common.utils.DuplicateApprovalException;
/**
 * Interface for working with Pending and Pending Line Item tables
 *
 * @author Kristyn Angelo
 * @version 1.0 
 **/
 
public interface IPendingDAO
{
  /** Retrieves records from the Pending and Pending Line Item
   * tables based on the Order Number
   * @parm String containing the Order Number
   * @return A Pending Order value object
   * @throws SQL Exception if retrieve call to database fails.
   *
   * @author Kristyn Angelo
   */
    public Object retrieveByOrderNumber(String OrderNumber) throws SQLException, 
                                                  BadConnectionException,
                                                  FTDSystemException,
                                                  DuplicateApprovalException;                                                  


  /**  Inserts records into the Pending and Pending Line Item
   * tables based on the Buyer Cookie
   * @parm Pending Order value object
   * @return String saying whether the insert was successful
   * @throws SQL Exception if insert call to database fails.
   *
   * @author Kristyn Angelo
   */  
    public String insert(OrderVO vo) throws SQLException, 
                                                  BadConnectionException,
                                                 FTDSystemException;

    /**  Deletes records from the Pending and Pending Line Item
   * tables based on the Buyer Cookie
   * @parm Pending Order value object
   * @return String saying whether the delete was successful
   * @throws SQL Exception if delete call to database fails.
   *
   * @author Kristyn Angelo
   */
    public String delete(IOrderVO vo) throws SQLException, 
                                          BadConnectionException,
                                          FTDSystemException;

    public String moveOrder(IOrderVO vo) throws SQLException, 
    BadConnectionException,
    FTDSystemException;
    
  /** Retrieves records from the Pending Line Item table
   * based on the Buyer Cookie.
   * @parm String containing the buyer cookie
   * @return List of Pending Line Item value objects
   * @throws SQL Exception if retrieve call to database fails.
   *
   * @author Kristyn Angelo
   */                                          
    public List retrieveLineItems(String BuyerCookie) throws SQLException,
                                                      BadConnectionException,
                                                      FTDSystemException;

}