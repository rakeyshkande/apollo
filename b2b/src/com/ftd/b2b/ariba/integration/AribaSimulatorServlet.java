package com.ftd.b2b.ariba.integration;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
//import javax.servlet.ServletContext;
//import javax.servlet.RequestDispatcher;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.*;

import com.ftd.osp.utilities.plugins.Logger;

public class AribaSimulatorServlet extends HttpServlet {
    
    // TODO ??? Prevent from running on production 
    
    protected static Logger logger = new Logger(AribaSimulatorServlet.class.getName());
    private static final String CXML_INPUT = "cxmlInput";
    private static final String SIM_TYPE = "simType";
    private static final String PUNCHOUT_SERVLET = "http://%server%/ariba/PunchOutRequest";
    private static final String APPROVAL_SERVLET = "http://%server%/ariba/ReceiveApprovedPO";
    private static final String SHOPPING_SERVLET = "http://%server%/ariba/ReceiveShoppingCart";
    private static final String ARIBA_SERVLET = "https://service.ariba.com/service/transaction/cxml.asp";
    private static final String PUNCHOUT_RESPONSE_HTML = 
        "<html><body><h2>Punchout Response</h2><textarea rows=\"8\" cols=\"110\">%raw%</textarea><br/><br/><a href=\"%link%\">Punchout to Website</a><body></html>";

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        processRequest(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) {
        processRequest(request, response);        
    }
    
    public void processRequest(HttpServletRequest request, HttpServletResponse response) {
        logger.info("Ariba Simulator here");
        if (request.getParameter(CXML_INPUT) != null) {
            try {
                URL url = null;
                HttpURLConnection con = null;
                String cxml = request.getParameter(CXML_INPUT);
                String sname = request.getServerName();
                String respStr = null;

                if (!sname.startsWith("b2b")){
                    sname += ":" + request.getServerPort();
                }                
                    
                // PUNCHOUT SIMULATION
                //
                if (request.getParameter(SIM_TYPE).equals("punchout") ) {
                    logger.info("PunchOut request (that would come from Ariba): " + cxml);
                        
                    // Call our B2B punchout servlet with the passed cxml
                    //
                    url = new URL(PUNCHOUT_SERVLET.replaceAll("%server%", sname));
                    logger.info("Punchout request being sent to: " + url);
                    con = (HttpURLConnection) url.openConnection();
                    sendToServlet(con, cxml);
    
                    // Get response from punchout servlet
                    //
                    String rawResp = responseFromServlet(con);
                    logger.info("Response from B2B punchout servlet): " + rawResp);
                    
                    // Format response
                    //
                    String poUrl = "";
                    //String decResp = URLDecoder.decode(sb.toString(), "UTF-8");
                    Pattern p = Pattern.compile("<URL>(.*)</URL>");
                    Matcher m = p.matcher(rawResp);
                    if (m.find()) {
                       poUrl = m.group(1);
                    }
                    respStr = PUNCHOUT_RESPONSE_HTML.replaceAll("%raw%", rawResp);
                    respStr = respStr.replaceAll("%link%", poUrl);


                // APPROVAL SIMULATION
                //
                } else if (request.getParameter(SIM_TYPE).equals("approval") ) {
                    
                    logger.info("Approval request (that would come from Ariba): " + cxml);
                    
                    // Call our B2B punchout servlet with the passed cxml
                    //
                    url = new URL(APPROVAL_SERVLET.replaceAll("%server%", sname));
                    logger.info("Approval request being sent to: " + url);
                    con = (HttpURLConnection) url.openConnection();
                    sendToServlet(con, cxml);
    
                    // Get response from approval servlet
                    //
                    respStr = responseFromServlet(con);
                    logger.info("Response from B2B Approval servlet): " + respStr);                                                        

                    
                // SHOPPING CART SUBMISSION SIMULATION
                //
                } else if (request.getParameter(SIM_TYPE).equals("cart") ) {
                    
                    logger.info("Shopping cart request (that would come from Website): " + cxml);
                    
                    // Call our B2B shopping cart servlet with the passed (tilde delimted) data
                    //
                    url = new URL(SHOPPING_SERVLET.replaceAll("%server%", sname));
                    logger.info("Shopping cart being sent to: " + url);
                    con = (HttpURLConnection) url.openConnection();
                    sendToServlet(con, cxml);
    
                    // Get response from shopping cart servlet
                    //
                    respStr = responseFromServlet(con);
                    logger.info("Response from B2B Shopping Cart servlet): " + respStr);                                                        
                
               // REQUEST TO ARIBA
               //
               } else if (request.getParameter(SIM_TYPE).equals("ariba")) {
                  url = new URL(ARIBA_SERVLET);
                  con = (HttpURLConnection) url.openConnection();
                  sendToServlet(con, cxml);
                  
                  // Get response from Ariba
                  //
                  respStr = responseFromServlet(con);
                  logger.info("Response from Ariba servlet: " + respStr);
               }

                // Return servlet response as response from this servlet 
                //
                PrintWriter responseFromPunchOut = response.getWriter();
                responseFromPunchOut.println(respStr);
                responseFromPunchOut.close();
                
            } catch (Exception e) {
                logger.error("Simulator can't route to Servlet - " + e);
            }
        }
    }

    
    // Send data to Servlet
    //
    private void sendToServlet(HttpURLConnection con, String data) throws Exception {
        OutputStream outStream = null;
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-type", "text/xml");
        con.setRequestProperty("Connection", "Keep-Alive");
        con.setDoInput(true);
        con.setDoOutput(true);
        con.setUseCaches(false);
        con.setDefaultUseCaches(false);
        outStream = con.getOutputStream();
        outStream.write(data.getBytes());
        outStream.flush();
        outStream.close();
    }

    
    // Get response from Servlet
    //
    private String responseFromServlet(HttpURLConnection con) throws Exception {
        // Get response from shopping cart servlet
        //
        BufferedReader br = null;
        InputStream is = con.getInputStream();
        br = new BufferedReader(new InputStreamReader(is));
        StringBuffer sb = new StringBuffer();
        String currentLine = null;
        while( (currentLine = br.readLine())!=null) {
            sb.append(currentLine);
        }
        br.close();
        return sb.toString();
    }
}
