package com.ftd.b2b.ariba.businessobjects.receiveshoppingcart;


import com.ftd.b2b.ariba.common.valueobjects.ShoppingCartOrderVO;
import com.ftd.framework.common.exceptions.*;

/**
 * Controls the processing of an Order once the order is 
 * fully received.
 */
public interface IOrderProcessor 
{
/**
 * Contains logic to:
 *  Add line items to the order collection for AddOns.
 *  Generate Cxml response
 *
 * @params OrderVO A complete order
 */
 public String buildOrder(ShoppingCartOrderVO order) throws FTDApplicationException;
 
/**
 * Contains logic to:
 *  Persist the data to the pending table.
 *
 * @params OrderVO A complete order
 */ 
 public void persistData(ShoppingCartOrderVO order) throws FTDApplicationException;

}