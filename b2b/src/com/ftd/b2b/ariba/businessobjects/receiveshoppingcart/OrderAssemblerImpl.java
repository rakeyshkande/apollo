package com.ftd.b2b.ariba.businessobjects.receiveshoppingcart;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.framework.common.utilities.ResourceManager;
import com.ftd.framework.common.exceptions.ResourceNotFoundException;
import com.ftd.framework.common.exceptions.FTDApplicationException;

import com.ftd.b2b.utils.mail.B2BMailer;
import com.ftd.b2b.ariba .common.valueobjects.ShoppingCartOrderVO;
import com.ftd.b2b.ariba .common.valueobjects.ShoppingCartLineItemVO;
import com.ftd.b2b.constants.B2BConstants;
import com.ftd.b2b.common.utils.XMLHelper;

/**
 * Each instance of OrderAssemblerImpl represents an Order containing
 * line items.  This used to be a thread process that ran
 * continuously until it had received all of its Line items or 
 * the time out had been reached.
 */
public class OrderAssemblerImpl implements IOrderAssembler, B2BConstants
{

    //class variables
    private boolean loop = true;    
    private int lineItemCount = 0;
    private int waitcount = 0;
    private Logger lm;
    private ResourceManager rm;
    private ShoppingCartOrderVO order;
    private ShoppingCartOrderVO orderNoAddOns;    
    
    public OrderAssemblerImpl(String id, int lineItems, ShoppingCartOrderVO newOrder)
    {
        this.lineItemCount = lineItems;    

        order         = new ShoppingCartOrderVO();
        order.setSupplierURL(newOrder.getSupplierURL());
        order.setBuyerCookie(newOrder.getBuyerCookie());
        order.setOriginalBuyerCookie(newOrder.getOriginalBuyerCookie());
        order.setAsnNumber(newOrder.getAsnNumber());
        order.setPayloadID(newOrder.getPayloadID());
        order.setCreatedDate(newOrder.getCreatedDate());
        order.addLineItem((ShoppingCartLineItemVO) newOrder.getLineItems().get(0));
        
        orderNoAddOns = new ShoppingCartOrderVO();
        orderNoAddOns.setSupplierURL(newOrder.getSupplierURL());
        orderNoAddOns.setBuyerCookie(newOrder.getBuyerCookie());
        orderNoAddOns.setOriginalBuyerCookie(newOrder.getOriginalBuyerCookie());
        orderNoAddOns.setAsnNumber(newOrder.getAsnNumber());
        orderNoAddOns.setPayloadID(newOrder.getPayloadID());
        orderNoAddOns.setCreatedDate(newOrder.getCreatedDate());
        orderNoAddOns.addLineItem((ShoppingCartLineItemVO) newOrder.getLineItems().get(0));

        //setup logging
        lm = new Logger(LOG_CATEGORY_ORDER_ASSEMBLER);
        if (lm.isDebugEnabled()){
            String errormsg = "OrderAssemblerImpl - " + id + " should contain " + lineItemCount + " line items (one already received)";
            lm.debug(errormsg);
        }

        //get resource manager
        rm = ResourceManager.getInstance();    
    }

/**
 * Adds a LineItemVO to the List object of line items.
 * @params LineItemVO containing order line 
 */
  public void addLineItem(ShoppingCartLineItemVO voLineItem){
    if (lm.isDebugEnabled()) {
      int count = order.getLineItems().size() + 1; // Add one since zero-relative
      String errormsg = "OrderAssemblerImpl - " + count + " out of " + lineItemCount + " items received";
      lm.debug(errormsg);
    }
    order.addLineItem(voLineItem);
    orderNoAddOns.addLineItem(voLineItem);

  }//end addLineItem


    /**
     * This method will run continuously until one of several conditions
     * occur:
     *   1. All of the line items in the order have been received.
     *   2. A period of time has expired
     */
     /*
      public void run(){

            IOrderProcessor orderProcessor = null;
            int waitCount = 0;

            try
            {           
              orderProcessor = (IOrderProcessor) rm.getImplementation(INTERFACE_ORDERPROCESSOR);                
            }
            catch (ResourceNotFoundException e)
            {
               String[] msg = new String[]{ INTERFACE_ORDERPROCESSOR + " could not be found : " + e.toString()};
               lm.error(msg[0]);
               B2BMailer.getInstance().send(this, "run()",ERROR_CODE_UNABLETOLOADSERVICE,msg, B2BMailer.ACTION2);
               return;
            }
      
            //catch all unhandle exceptions
            try{
                while (loop) {
                        //if all the line items have been received
                        if (isOrderComplete()) {
                                if(debugEnabled) {
                                    lm.debug("All ("+order.getLineItems().size()+")lines received for ");  //??? threadID
                                }                                                 
                                loop = false;
                        }
                        else {
                                //check for time out
                                if (waitCount >= threadTimeOut) 
                                {
                                   String[] msg = new String[] {Integer.toString(order.getLineItems().size()), Integer.toString(lineItemCount), "???"}; //??? threadID
                                   lm.error("All line items not received");
                                   B2BMailer.getInstance().send(this, "run()", ERROR_CODE_ALLLINEITEMSNOTRECEIVED, msg, B2BMailer.ACTION1);
          
                                   loop = false;
                                   return; // Exit this method, there is no other action we can
                                           // take other than sending an email notification that
                                           // the shopping cart timed out.
                                }
                                else {
                                      try
                                      {
                                         //sleep to prevent overburdening the processor
                                         waitCount += threadSleep;
                                         sleep(threadSleep);
                                      }
                                      catch (Exception e) 
                                      {
                                         //ignore this exception, keep processing
                                      }
                                } // end if
                        } // end if
                } // end while
            //Send order to servlet for processing
            String url =  rm.getProperty(PROPERTY_B2B_PROPERTY_FILE, PROPERTY_RECEIVE_SHOPPING_CART_URL); 
            //send message
            try
            {
                HTTPProxy.send(url, formatXML(orderNoAddOns).toString(), CONSTANT_ARIBA_URL_CONTENT_TYPE);
            }
            catch(HTTPProxyException e)
            {
               String[] msg = new String[] {e.toString()};
               lm.error("" + ERROR_CODE_SENDSHOPPINGTOSERVLET +  msg);
               B2BMailer.getInstance().send(this, "run()", ERROR_CODE_SENDSHOPPINGCARTTOARIBA,
                                            msg, B2BMailer.ACTION2);
            }            
        }
        catch(Throwable e)
        {
           String[] msg = new String[] {e.toString()};
           lm.error(e.toString());
           B2BMailer.getInstance().send(this, "run()",ERROR_CODE_UNRECOVERABLE_EXCEPTION ,msg,B2BMailer.ACTION2);
        }
        
      }//end run
      */

    /**
     * Wraps the marshalled ShoppingCartOrderVO object in an XML header
     * which will including the SharedSecret and DUNS number. This is done
     * for authentication purposes between this class and the servlet that
     * receives the data.
     *
     * @params ShoppingCartOrderVO The Order VO
     * @return StringBuffer of XML
     * @throws Exception if an error occurs marshalling the value object.
     */
     public String formatXML() throws Exception
     {
         return new XMLHelper().marshall(orderNoAddOns);
     }

    /**
     * Determine if the order is complete based on whether
     * the total line items assembled equals the total expected.
     *
     * @return boolean
     * @author York Davis
     **/
    public boolean isOrderComplete()
    {
        return lineItemCount <= order.getLineItems().size();
    }

    /**
     * Returns the ShoppingCartOrder VO Object.
     *
     * @return ShoppingCartOrderVO
     * @author York Davis
     **/
    public ShoppingCartOrderVO getOrder()
    {
        return order;
    }
    /**
     * Not Used
     */
     public void create(){ }

    /**
     * Not Used
     */
     public void save(){ }

    /**
    * Determins if objects are the same.  
    * Note, a deep compare is not done.
    * @parms object to cmpare to
    * @returns True of objects equal, else False
    */
    public boolean equals(Object obj){
     return (obj == this); }

    /**
    * Returns the class name.
    * @returns Class Name
    */
    public String toString(){
        return this.getClass().toString(); }
     
}