package com.ftd.b2b.ariba.businessobjects.receiveshoppingcart;

import java.net.Socket;
import java.net.ServerSocket;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.IOException;
import java.io.InputStreamReader;

import com.ftd.b2b.common.utils.BusinessObject;
import com.ftd.framework.common.utilities.ResourceManager;
import com.ftd.framework.common.exceptions.FTDApplicationException;
import com.ftd.framework.common.exceptions.ResourceNotFoundException;

import com.ftd.b2b.ariba.common.utils.LineItemParser;
import com.ftd.b2b.ariba.common.utils.ClientOptions;
import com.ftd.b2b.ariba.common.valueobjects.ShoppingCartLineItemVO;
import com.ftd.b2b.ariba.common.valueobjects.ShoppingCartOrderVO;
import com.ftd.b2b.constants.B2BConstants;
import com.ftd.b2b.utils.mail.B2BMailer;

/** 
 *  !!! OBSOLETE CLASS - THREADS NO LONGER USED !!!
 *
 * LineItemReceiverImpl receives socket transmissions from Novator and is the
 * starting point for the processing of this use case.  Each socket transmission
 * represents a line item for a particular order.  LineItemReceiverImpl 
 * serves as the starting point for building an Order from individual
 * line items.
 */
public class LineItemReceiverImpl extends BusinessObject 
                            implements ILineItemReceiver, Runnable, B2BConstants
{
    //class variables
    private boolean debugEnabled = false;
    private boolean loop = true;
    private LineItemParser lineItemParser;

    protected Socket         socket       = null;
    protected ServerSocket   serverSocket = null;
    protected BufferedReader in           = null;
    protected PrintWriter    out          = null;

    private static ThreadGroup group = new ThreadGroup("FTDThreads");
    private ResourceManager resourceManager;

    private static String SUCCESS  = " SUCCESS";
    private static String COMPLETE =  "complete";

    /**
     *  Constructor 
     */
    public LineItemReceiverImpl()
    {
        super(LOG_CATEGORY_LINE_ITEM_RECEIVER);

        resourceManager = ResourceManager.getInstance();

        if ( super.getLogManager().isDebugEnabled() )
        {
            debugEnabled = true;
        }
    }


    /**
     * This method opens a java socket and listens endlessly for line item
     * transactions.  When a transmission is received it calls LineItemParserImpl
     * to parese the data.  The data is then placed into a new OrderAssembler
     * thread and added to existing thread if an order has already been created
     * for the received line.   
     */
    public void run() {
    /*
                ShoppingCartOrderVO voOrder = null;
                ShoppingCartLineItemVO voLineItem = null;
                OrderAssemblerImpl assembler = null;
                
                //obtain values from property file
                String port =  resourceManager.getProperty(PROPERTY_B2B_PROPERTY_FILE, PROPERTY_RECEIVE_SHOPPING_CART_PORT);    
                try{
                    //open socket
                    serverSocket = new ServerSocket(Integer.parseInt(port));
                    if(debugEnabled) {
                        super.getLogManager().debug("FTD Socket listening on port" + port);
                        }            

                }//end, open socket try
                catch (IOException e){
                   String[] msg = new String[]{"Shopping Cart Socket Error:" + e.toString()};
                   getLogManager().error(e.toString());
                   B2BMailer.getInstance().send(this, "run()", ERROR_CODE_SOCKETOPENSHOPPINGCART,msg,B2BMailer.ACTION3);
                   return;
                }

                  getLogManager().info("Shopping Cart Thread Started.");
                  B2BMailer.getInstance().send(this, "run()", "Shopping Cart Started.", B2BMailer.ACTION1);
          
                //continue looping/listening until flagged to stop
                while (loop) {
                        //This process cannot stop running if an exception occurs
                        try{

                        //clear out line variables
                        String wholeLine = "";

                        //    if(debugEnabled) {
                        //        super.getLogManager().debug("Shoppingcart about to accept transmission. " + wholeLine);
                        //    }                 
                                    
                            //get data from socket
                            socket = serverSocket.accept();
                            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                            out = new PrintWriter(socket.getOutputStream(), true);

                            //read the entire input stream
                            while ((wholeLine = in.readLine()) != null){

                                    if(debugEnabled) {
                                        super.getLogManager().debug("Order Received" + wholeLine);
                                    }                                    

                                    // If this is a request to reload client options, then do so, then go wait for other lines
                                    if ((wholeLine.length() <= CONSTANT_RELOAD_CLIENT_OPTIONS.length()) && 
                                        (wholeLine.startsWith(CONSTANT_RELOAD_CLIENT_OPTIONS))) {
                                        super.getLogManager().debug("Request for threads to reload client options");
                                        ClientOptions.reloadClientOptions();
                                        continue;
                                    }

                                    //Parse input string into an order object
                                    voOrder = LineItemParser.parseData(wholeLine);

                                    //Obtain the line the line VO that was just parsed
                                    voLineItem = (ShoppingCartLineItemVO)voOrder.getLineItems().get(0);

                                    // Determine if the Thread is already active by looking for it in the
                                    // ThreadGroup. If it is there, add the new line item to the existing thread.
                                    // If it is not there, that means that this is the first line item for this
                                    // Shopping Cart - create a new Thread in the ThreadGroup, add the line item
                                    // and start the Thread.
                                    //
                                    // Synchronize on the ThreadGroup as it cannot change during the duration
                                    // of processing an individual line item.
                                    // Note: It may not be necessary to synchronize as only this Thread of execution
                                    // should be (will be?) in this code at any one time.
                                    synchronized (group)
                                    {
                                            try{
                                                    assembler = (OrderAssemblerImpl) findThreadInGroup(voOrder.getBuyerCookie());
                                                
                                                    //We already have an order created, so add line to existing order
                                                    assembler.addLineItem(voLineItem);
                                        
                                                    if(debugEnabled) {
                                                        super.getLogManager().debug("Adding to existing car thread" + voOrder.getBuyerCookie());
                                                    }                                                         
                                            }
                                            catch (ThreadNotFoundException tnfe) {

                                                    //Order does not exist, create new one
                                                    assembler = new OrderAssemblerImpl(group, voOrder.getBuyerCookie(), voLineItem.getNumberOfLineItems(),voOrder);
                                                    assembler.start();
                                                    if(debugEnabled) {
                                                        super.getLogManager().debug("Started new cart thread" + voOrder.getBuyerCookie());
                                                    }                                    
                                            }
                                    } // end synchronized
                                  
                                    // The following two lines of code are required by Novator to 
                                    // send the 'received' confirmation back to Novator.
                                    out.println(voLineItem.getOrderNumber() + SUCCESS);
                                    out.println(COMPLETE);                                    
                                    if (assembler.isOrderComplete())
                                    {
                                      out.println(createAribaXML(assembler.getOrder()));
                                    }
                        }//end while loop that is reading from socket

                        // Close resources.
                        out.close();
                        in.close();
                        socket.close();
                        
                 //       if(debugEnabled) {
                 //            super.getLogManager().debug("Shopping cart end loop to read data.");
                 //       }    
                            
                  } //end, try that prevent method from ending
                  // If FTDApplicationException is thrown, do nothing. 
                  // This is because the exception was already logged and emailed and
                  // we do not want to do so again.
                  catch (FTDApplicationException ftdae)
                  {
                  }
                  catch (Throwable e)
                  {
                      String[] msg = new String[]{e.toString()};
                      getLogManager().error(e.toString());
                      B2BMailer.getInstance().send(this, "run()", ERROR_CODE_UNRECOVERABLE_EXCEPTION, msg, B2BMailer.ACTION2);
                  }
                } // end while

                //close socket
                try{
                    serverSocket.close();
                    }
                catch(Throwable t){
                    //We do not care if an exception was thrown during close
                }

                    if(debugEnabled) {
                        super.getLogManager().debug("Shopping cart listener stopping.");
                    } 
        */
        }//end run()

        /**
         *
         * Attempt to find the Thread in the ThreadGroup based on 
         * the Order Id. Order Id is the name that is given to
         * the Thread.
         *
         * @params Name of thread to find
         * @returns Found thread
         */
         /*
        private static final Runnable findThreadInGroup(String threadName) throws ThreadNotFoundException
        {
                //create array to hold enumerated threads, array should be
                //be larger then activeCount because activeCount may not be
                //accureate.
                Thread[] aThread = new Thread[(group.activeCount() * 2)];
                int iThreadsInGroup = group.enumerate(aThread, false);

                //loop through each thread in enumeration
                for (int j=0; j<iThreadsInGroup; j++)
                {
                                //if match found return thread
                                if  (aThread[j].getName().equalsIgnoreCase(threadName)) {
                                        return aThread[j];
                                }
                }
                //This exception may be thrown, but it is not an error
                throw new ThreadNotFoundException();
        }
        */

    /**
     * Stops the thread.
     */
    public void stopThread()
    {
        if(debugEnabled) 
        {
            super.getLogManager().debug("Thread Stop for Shopping Cart.");
        }                 
        loop = false;
    }

   /**
    * Build the XML to send back to Novator.
    *
    * @param ShoppingCartOrderVO The order to be converted to XML.
    * @return The cXML response document.
    * @author York Davis
    **/
    /*
    private String createAribaXML(ShoppingCartOrderVO order) throws FTDApplicationException
    {
      // Process the Order
      IOrderProcessor orderProcessor = null;
      try
      {           
         orderProcessor = (IOrderProcessor) resourceManager.getImplementation(INTERFACE_ORDERPROCESSOR);                
         return orderProcessor.buildOrder(order);
      }
      catch (ResourceNotFoundException e)
      {
         String[] msg = new String[]{ INTERFACE_ORDERPROCESSOR + " could not be found : " + e.toString()};
         getLogManager().error(msg[0]);
         B2BMailer.getInstance().send(this, "processRequest()", ERROR_CODE_UNABLETOLOADSERVICE,msg, B2BMailer.ACTION2);
         throw new FTDApplicationException(ERROR_CODE_UNRECOVERABLE_EXCEPTION, e);
      }
    }
    */

    /**
     * Not Used
     */
     public void create(){ }

    /**
     * Not Used
     */
     public void save(){ }

    /**
    * Determins if objects are the same.  
    * Note, a deep compare is not done.
    * @parms object to cmpare to
    * @returns True of objects equal, else False
    */
    public boolean equals(Object obj){
     return (obj == this); }

    /**
    * Returns the class name.
    * @returns Class Name
    */
    public String toString(){
        return this.getClass().toString(); }    

}