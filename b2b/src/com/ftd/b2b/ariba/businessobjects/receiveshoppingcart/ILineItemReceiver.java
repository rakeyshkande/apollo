package com.ftd.b2b.ariba.businessobjects.receiveshoppingcart;



/** 
 * LineItemReceiverImpl receives socket transmissions from Novator and is the
 * starting point for the processing of this use case.  Each socket transmission
 * represents a line item for a particular order.  LineItemReceiverImpl 
 * serves as the starting point for building an Order from individual
 * line items.
 */
public interface ILineItemReceiver  
{
    /**
     * Starts the Thread
     */
    public void run();

    /**
     * Stops the Thread from executing
     */
    public void stopThread();
}