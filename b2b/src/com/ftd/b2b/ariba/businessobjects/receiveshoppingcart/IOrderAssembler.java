package com.ftd.b2b.ariba.businessobjects.receiveshoppingcart;


import com.ftd.b2b.ariba.common.valueobjects.ShoppingCartLineItemVO;
import com.ftd.b2b.ariba.common.valueobjects.ShoppingCartOrderVO;

/**
 * Each instance of OrderAssemblerImpl represents an Order containing
 * line items.  
 */
public interface IOrderAssembler 
{

/**
 * Adds a LineItemVO to the List object of line items.
 * @params LineItemVO containing order line 
 */
  public void addLineItem(ShoppingCartLineItemVO voLineItem);
      
    /**
     * Returns the ShoppingCartOrder VO Object.
     *
     * @return ShoppingCartOrder
     * @author York Davis
     **/
     public ShoppingCartOrderVO getOrder();

     /**
      * Determine if the order is complete based on whether
      * the total line items assembled equals the total expected.
      *
      * @return boolean
      * @author York Davis
      **/
     public boolean isOrderComplete();
     
    /**
     * Not Used
     */
     public void create();

    /**
     * Not Used
     */
     public void save();

    /**
    * Determins if objects are the same.  
    * Note, a deep compare is not done.
    * @parms object to cmpare to
    * @returns True of objects equal, else False
    */
    public boolean equals(Object obj);

    /**
    * Returns the class name.
    * @returns Class Name
    */
    public String toString();
 
}