package com.ftd.b2b.ariba.businessobjects.receiveshoppingcart;

import com.ftd.b2b.common.utils.BusinessObject;
import com.ftd.framework.common.utilities.ResourceManager;
import com.ftd.framework.common.exceptions.FTDApplicationException;
import com.ftd.framework.common.exceptions.ResourceNotFoundException;
import com.ftd.b2b.ariba.common.valueobjects.ShoppingCartOrderVO;
import com.ftd.b2b.ariba.common.valueobjects.ShoppingCartLineItemVO;
import com.ftd.b2b.ariba.common.valueobjects.AddOnVO;
import com.ftd.b2b.ariba.common.valueobjects.ClientOptionsVO;
import com.ftd.b2b.ariba.integration.dao.IAddOnDAO;
import com.ftd.b2b.ariba.integration.dao.IPendingDAO;
import com.ftd.b2b.ariba.common.utils.UnapprovedPOResponseGenerator;
import com.ftd.b2b.ariba.common.utils.ClientOptions;
import com.ftd.b2b.constants.B2BConstants;
import com.ftd.b2b.common.utils.HTTPProxy;
import com.ftd.b2b.common.utils.HTTPProxyException;
import com.ftd.b2b.utils.mail.B2BMailer;

import java.util.*;

/**
 * Controls the processing of an Order once the order is fully received.
 *
 * 10/15/2002 (YWD) - get instance of ResourceManager in constructor.
 *
 */
public class OrderProcessorImpl extends BusinessObject implements B2BConstants, IOrderProcessor
{

    private boolean debugEnabled = false;
    private ResourceManager rm;
    private ShoppingCartLineItemVO voServiceFeeLineItem = null;
    private ShoppingCartLineItemVO voTaxLineItem = null;
    
/**
 * Constructor
 */
    public OrderProcessorImpl()
    {
        super(LOG_CATEGORY_ORDER_PROCESSOR);

        if ( super.getLogManager().isDebugEnabled() )  {
            debugEnabled = true;
        }                    

        rm = ResourceManager.getInstance();
    }

/**
 * Constructor
 */
    public OrderProcessorImpl(String category)
    {
        super(category);

        if ( super.getLogManager().isDebugEnabled() )  {
            debugEnabled = true;
        }                    
    }

    /**
     * Contains logic to:
     *  Add line items to the order collection for AddOns.
     *  Generate Cxml response
     *
     * @params OrderVO A complete order
     */
     public String buildOrder(ShoppingCartOrderVO order) throws FTDApplicationException
     {
        //Add line items to the order collection for Addons
        order = addExtraLineItems(order);
        
        //Generate Cxml response
        StringBuffer cxmlResp = new StringBuffer();
        cxmlResp.append(order.getSupplierURL());
        cxmlResp.append("|");
        cxmlResp.append(order.getBuyerCookie());
        cxmlResp.append("|");
        cxmlResp.append(new UnapprovedPOResponseGenerator().generateResponse(order));
        if (debugEnabled) {
          getLogManager().debug("cXML response: " + cxmlResp.toString());
        }
        return cxmlResp.toString();
     }//end process order



    /**
     * Contains logic to:
     *  Persist the data to the pending table.
     *
     * @params OrderVO A complete order
     */ 
    public void persistData(ShoppingCartOrderVO order) throws FTDApplicationException
    {
        //get DAO
        IPendingDAO daoPending = null;
        try{
            daoPending = (IPendingDAO) rm.getImplementation(INTERFACE_PENDINGDAO);                
            }
        catch(ResourceNotFoundException e){
           String[] msg = new String[] {e.toString()};            
           getLogManager().error("persistData: " + e.toString());
           B2BMailer.getInstance().send(this, "persistData()",ERROR_CODE_UNABLETOLOADSERVICE ,msg,B2BMailer.ACTION2);
           throw new FTDApplicationException(ERROR_CODE_UNRECOVERABLE_EXCEPTION, e);
            }
            

        try{
            // First move data if it already exists.
            // We do this instead of deleting since there are cases where website erroneously returns us 
            // edited cart with new order number instead of original, so we save original cart, allowing  
            // approval logic to find original order if necessary.            
            String dataFound = daoPending.moveOrder(order);
        
            //insert data into pending database
            getLogManager().debug("Buyer Cookie: " + order.getBuyerCookie());
            getLogManager().debug("Original BuyerCookier: " + order.getOriginalBuyerCookie());
            daoPending.insert(order);

            }//end, try
        catch(Exception e){
           String[] msg = new String[] {e.toString()};                    
           getLogManager().error("persistData: " + e.toString());
           e.printStackTrace();
           B2BMailer.getInstance().send(this, "persistData()",ERROR_CODE_DAO_EXCEPTION ,msg,B2BMailer.ACTION2);
           throw new FTDApplicationException(ERROR_CODE_UNRECOVERABLE_EXCEPTION, e);
        }

    }


    /**
     * Break off certain aspects of order into separate line items.  Addons will always
     * be split into line items, but service fee and tax will be treated as line items
     * only for certain clients.
     * 
     * @params OrderVo original order
     * @returns OrderVO order with addons on seperate lines
     */
    private ShoppingCartOrderVO addExtraLineItems(ShoppingCartOrderVO order) throws FTDApplicationException {

        //get dao
        IAddOnDAO daoAddOn = null;
        try{
            daoAddOn = (IAddOnDAO) rm.getImplementation(INTERFACE_ADDONDAO);                
            }
        catch(ResourceNotFoundException e){
           String[] msg = new String[] {e.toString()};                            
           getLogManager().error("addExtraLineItems: " + e.toString());
           B2BMailer.getInstance().send(this, "persistData()",ERROR_CODE_UNABLETOLOADSERVICE ,msg,B2BMailer.ACTION2);
           throw new FTDApplicationException(ERROR_CODE_UNRECOVERABLE_EXCEPTION, e);
            }

        //get list items from order
        List lineItems = order.getLineItems();

        //for each line in the order
        int numberOfItems = lineItems.size();
        for (int i=0; i < numberOfItems; i++) {

            //get line item from order
            ShoppingCartLineItemVO voExistingLine = (ShoppingCartLineItemVO)lineItems.get(i);

            // If there is a service fee or tax, include it as line item (if client
            // is configured as such).  Note that service fees are totaled into
            // single line item.  Ditto for tax.
            //
            addServiceFee(voExistingLine, order.getAsnNumber());
            addTax(voExistingLine, order.getAsnNumber());
            
            // Check for Addons and add as line items
            //
            String addonDetail = voExistingLine.getAddOnDetail();
            if (addonDetail != null && addonDetail.length() > 0 && !addonDetail.equalsIgnoreCase(CONSTANT_RAW_DATA_EMPTY_FIELD)){
                //parse out addon types and counts for each item
                StringTokenizer itemTokenizer = new StringTokenizer(addonDetail, CONSTANT_ADDON_ITEM_DELIMITER);
                while (itemTokenizer.hasMoreElements()){
                
                    String addonData = itemTokenizer.nextToken();

                    //copy existing line item
                    ShoppingCartLineItemVO voLineItem = null;
                    try{
                        voLineItem = (ShoppingCartLineItemVO)voExistingLine.copy();
                        }
                    catch (java.lang.CloneNotSupportedException e){
                           String[] msg = new String[] {e.toString()};                                                
                           getLogManager().error("addExtraLineItems: " + e.toString());
                           B2BMailer.getInstance().send(this, "addExtraLineItems()",ERROR_CODE_CLONENOTSUPPORTED ,msg,B2BMailer.ACTION2);
                           throw new FTDApplicationException(ERROR_CODE_UNRECOVERABLE_EXCEPTION, e);
                        } //end, catch

                    //Obtain item id and quantity
                    StringTokenizer idTokenizer = new StringTokenizer(addonData, CONSTANT_ADDON_QUANTITY_DELIMITER);                
                    String addon = idTokenizer.nextToken();
                    String quantity = idTokenizer.nextToken();

                    //look up addon values
                    AddOnVO voAddon = null;
                    try{
                        List addons = daoAddOn.retrieveByAddOnType(addon);
                        if (addons == null){
                           String[] msg = new String[] {addon};                                                                        
                           getLogManager().error("addExtraLineItems - addon value not found: " +  addon);
                           B2BMailer.getInstance().send(this, "addExtraLineItems()",ERROR_CODE_ADDONVALUENOTFOUND,msg,B2BMailer.ACTION2);
                           continue;//continue at beginning of for loop without adding this line
                            }
                        voAddon = (AddOnVO)addons.get(0);                                                                  
                    
                       }
                    catch(Exception e){
                       String[] msg = new String[] {e.toString()};                                                                                            
                       getLogManager().error("addExtraLineItems: " + e.toString());
                       B2BMailer.getInstance().send(this, "addExtraLineItems()",ERROR_CODE_DAO_EXCEPTION ,msg,B2BMailer.ACTION2);
                       throw new FTDApplicationException(ERROR_CODE_UNRECOVERABLE_EXCEPTION, e);
                        }

                    //format addong price


                    //Insert item into lineitems
                    voLineItem.setItemNumber(CONSTANT_ADDONFLAG + voAddon.getAddOnId());
                    voLineItem.setItemPrice(voAddon.getAddOnPrice());
                    voLineItem.setItemDescription(voAddon.getAddOnDescription());
                    voLineItem.setItemQuantity(Integer.parseInt(quantity));
                    
                    //some fields need to be cleared out for new line 
                    voLineItem.setAddOnCount(0);
                    voLineItem.setAddOnDetail(null);                    
                    voLineItem.setServiceFee(0);
                    voLineItem.setTaxAmount(0);
                    voLineItem.setOrderTotal(0);


                    // Certain customers want a default UNSPSC code for all products.  If this is 
                    // one of those customers, get the default and override the value from Novator.
                    //
                    ClientOptionsVO covo = ClientOptions.getClientOptionsForAsn(order.getAsnNumber());
                    if ((covo != null) && (covo.getDefaultUnspsc() != null)) {
                        voLineItem.setItemUNSPSCCode(covo.getDefaultUnspsc());
                        getLogManager().debug("Using default UNSPSC code: '" +
                                       covo.getDefaultUnspsc() + "' for client: " + order.getAsnNumber());
                                       
                    // Otherwise check for other UNSPSC processing
                    } else {

                        /* 6/3/2003 Ed Mueller
                            The UNSPSC number for addons will be set to a default for
                            certain clients.  Originally this code was added for Aegon, 
                            but could be used by other clients. */
                        //Get asn's which need their Addon UNSPSC value defaulted and the default value
                        ResourceManager resourceManager = ResourceManager.getInstance();
                        String asnDefaulters =  resourceManager.getProperty(PROPERTY_B2B_PROPERTY_FILE, PROPERTY_ASN_ADDON_UNSPSC_DEFAULTERS);    
                        String asnDefault =  resourceManager.getProperty(PROPERTY_B2B_PROPERTY_FILE, PROPERTY_ASN_ADDON_UNSPSC_DEFAULT);    
                    
                        //if this ASN needs the value defaulted
                        if (asnDefaulters.indexOf(order.getAsnNumber()) >= 0){
                                //default value
                                voLineItem.setItemUNSPSCCode(asnDefault);                            
                                }
                        //else use value from database                        
                        else{
                            voLineItem.setItemUNSPSCCode(voAddon.getAddOnUNSPSC());
                            }
                        
                        //6/20/03 - K. Angelo
                        //Added code to truncate the UNSPSC code for addons if the customers requests only 8 digits.
                        //The list of customers are in the B2B.xml file
                        String UNSPSCTruncateList=  resourceManager.getProperty(PROPERTY_B2B_PROPERTY_FILE, PROPERTY_8_DIGIT_UNSPSC_CODE_LIST);    
                        if (UNSPSCTruncateList.indexOf(order.getAsnNumber()) >= 0)
                        {
                            String TruncatedUNSPSCCode = (voLineItem.getItemUNSPSCCode()).substring(0,8);
                            voLineItem.setItemUNSPSCCode(TruncatedUNSPSCCode);
                        }
                    } 

                    lineItems.add(voLineItem);

                }//end, while
            }//end if have addons
        }//end for

        // Add service fee total as line item (variable will be non-null
        // if client configured as such and service fees exist).
        // Ditto for tax.
        //
        if (voServiceFeeLineItem != null) {
          lineItems.add(voServiceFeeLineItem);
        }
        if (voTaxLineItem != null) {
          lineItems.add(voTaxLineItem);
        }

        // Place all line items into order
        //
        order.setLineItems((ArrayList)lineItems);
            
    return order;    
    
    }//end addExtraLineItems


    /**
     * Create new line item if service fee exists and client is configured as such.
     * If multiple order line items have service fees, they are totaled in a single line item.
     * @params ShoppingCartLineItemVO Existing line item
     * @params String ASN number
     */
    private void addServiceFee(ShoppingCartLineItemVO voExistingLine, String asnNumber) 
    throws FTDApplicationException {

        // Only bother doing this for clients with this option enabled 
        // and only when service-fee, drop-ship charges, or extra fee are non-zero.
        //
        ClientOptionsVO covo = ClientOptions.getClientOptionsForAsn(asnNumber);
        if ((covo != null) && (covo.getServiceFeeUnspsc() != null) &&
            ((voExistingLine.getServiceFee() > 0) ||
             (voExistingLine.getExtraShippingFee() > 0) ||
             (voExistingLine.getDropShipCharges() > 0))
             ) {

          // Determine service fee for this item.  Either the service fee should
          // be set (florist delivery) or the drop-ship charge (plus possible extra
          // fee) should set, but not both.  Since that's the case, we can just
          // add them all together and not worry about it.
          //
          float itemServiceFee = voExistingLine.getServiceFee() +
                                 voExistingLine.getExtraShippingFee() +
                                 voExistingLine.getDropShipCharges();

          // If this is first service fee encountered, create new line item based on existing one
          //
          if (voServiceFeeLineItem == null) {
            try {
              voServiceFeeLineItem = (ShoppingCartLineItemVO)voExistingLine.copy();
            } catch (java.lang.CloneNotSupportedException e) {
              String[] msg = new String[] {e.toString()};
              getLogManager().error("addServiceFee: " + e.toString());
              B2BMailer.getInstance().send(this, "addServiceFee()",ERROR_CODE_CLONENOTSUPPORTED ,msg,B2BMailer.ACTION2);
              throw new FTDApplicationException(ERROR_CODE_UNRECOVERABLE_EXCEPTION, e);
            }
            // Insert item into lineitems
            voServiceFeeLineItem.setItemNumber(CONSTANT_SERVICE_FEE_LINE_ITEM_NUM);
            voServiceFeeLineItem.setItemPrice(itemServiceFee);
            voServiceFeeLineItem.setItemDescription(CONSTANT_SERVICE_FEE_LINE_ITEM_DESC);
            voServiceFeeLineItem.setItemQuantity(1);
            // Some fields need to be cleared out for new line 
            voServiceFeeLineItem.setAddOnCount(0);
            voServiceFeeLineItem.setAddOnDetail(null);                    
            voServiceFeeLineItem.setServiceFee(0);
            voServiceFeeLineItem.setTaxAmount(0);
            voServiceFeeLineItem.setOrderTotal(0);
            voServiceFeeLineItem.setExtraShippingFee(0);
            voServiceFeeLineItem.setDropShipCharges(0);
            // Set UNSPSC to service fee UNSPSC for this client
            voServiceFeeLineItem.setItemUNSPSCCode(covo.getServiceFeeUnspsc());

          // Otherwise, use our service fee line item and just add to service fee total
          //
          } else {
            voServiceFeeLineItem.setItemPrice(voServiceFeeLineItem.getItemPrice() + itemServiceFee);            
          }
        }
    
    }//end addServiceFee


    /**
     * Create new line item if tax exists and client is configured as such.
     * If multiple order line items have tax, they are totaled in a single line item.
     * @params ShoppingCartLineItemVO Existing line item
     * @params String ASN number
     */
    private void addTax(ShoppingCartLineItemVO voExistingLine, String asnNumber) 
    throws FTDApplicationException {

        // Only bother doing this for clients with this option enabled 
        // and only when tax is non-zero.
        //
        ClientOptionsVO covo = ClientOptions.getClientOptionsForAsn(asnNumber);
        if ((covo != null) && (covo.getTaxUnspsc() != null) &&
            (voExistingLine.getTaxAmount() > 0)) {

          // If this is first tax encountered, create new line item based on existing one
          //
          if (voTaxLineItem == null) {
            try {
              voTaxLineItem = (ShoppingCartLineItemVO)voExistingLine.copy();
            } catch (java.lang.CloneNotSupportedException e) {
              String[] msg = new String[] {e.toString()};
              getLogManager().error("addTax: " + e.toString());
              B2BMailer.getInstance().send(this, "addTax()",ERROR_CODE_CLONENOTSUPPORTED ,msg,B2BMailer.ACTION2);
              throw new FTDApplicationException(ERROR_CODE_UNRECOVERABLE_EXCEPTION, e);
            }
            // Insert item into lineitems
            voTaxLineItem.setItemNumber(CONSTANT_TAX_LINE_ITEM_NUM);
            voTaxLineItem.setItemPrice(voExistingLine.getTaxAmount());
            voTaxLineItem.setItemDescription(CONSTANT_TAX_LINE_ITEM_DESC);
            voTaxLineItem.setItemQuantity(1);
            // Some fields need to be cleared out for new line 
            voTaxLineItem.setAddOnCount(0);
            voTaxLineItem.setAddOnDetail(null);                    
            voTaxLineItem.setServiceFee(0);
            voTaxLineItem.setTaxAmount(0);
            voTaxLineItem.setOrderTotal(0);
            // Set UNSPSC to tax UNSPSC for this client
            voTaxLineItem.setItemUNSPSCCode(covo.getTaxUnspsc());
            //??? other fields?

          // Otherwise, use our tax line item and just add to tax total
          //
          } else {
            voTaxLineItem.setItemPrice(voTaxLineItem.getItemPrice() + voExistingLine.getTaxAmount());            
          }
        }
    
    }//end addTax


    /**
     * Not Used
     */
     public void create(){ }

    /**
     * Not Used
     */
     public void save(){ }

    /**
    * Determins if objects are the same.  
    * Note, a deep compare is not done.
    * @parms object to cmpare to
    * @returns True of objects equal, else False
    */
    public boolean equals(Object obj){
     return (obj == this); }

    /**
    * Returns the class name.
    * @returns Class Name
    */
    public String toString(){
        return this.getClass().toString(); }     
}