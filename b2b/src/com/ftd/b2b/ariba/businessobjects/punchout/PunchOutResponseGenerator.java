package com.ftd.b2b.ariba.businessobjects.punchout;

import com.ftd.b2b.ariba.common.utils.ResponseGenerator;
import com.ftd.b2b.ariba.common.valueobjects.AribaRequestVO;
import com.ftd.b2b.ariba.common.valueobjects.URLLookupVO;
import com.ftd.b2b.constants.B2BConstants;
import java.util.HashMap;

/**
 * Generates the PunchOutResponse cXML document.
 *
 * @author York Davis
 * @version 1.0 
 **/
public class PunchOutResponseGenerator extends ResponseGenerator implements B2BConstants
{
  public PunchOutResponseGenerator()
  {
  }

  /**
   * Generates the PunchOutResponse cXML document.
   *
   * @param The URLLookupVO object.
   * @param The AribaRequestVO object.   
   * @return The cXML response document.
   * @author York Davis
   **/
  public String generateResponse(URLLookupVO urlLookupVO, AribaRequestVO aribaRequestVO)
  {
      HashMap params = (HashMap) aribaRequestVO.getParams();

      StringBuffer sb = new StringBuffer(getXMLHeaders());
      sb.append("<cXML payloadID=\"");
      sb.append(params.get(CONSTANT_PAYLOAD_ID));
      sb.append("\" timestamp=\"");
      sb.append(getTimestamp());
      sb.append("\">");
      sb.append("<Response>");
      sb.append("<Status code=\"200\" text=\"success\" />");
      sb.append("<PunchOutSetupResponse>");
      sb.append("<StartPage>");
      sb.append("<URL>");
      sb.append(urlLookupVO.getURL());
      sb.append("?payloadid=");
      sb.append(java.net.URLEncoder.encode((String)params.get(CONSTANT_PAYLOAD_ID)));
      sb.append("&#x26;currentbuyercookie=");
      sb.append(java.net.URLEncoder.encode((String)params.get(CONSTANT_BUYER_COOKIE)));      
      sb.append("&#x26;asnbuyer=");
      sb.append(java.net.URLEncoder.encode((String)params.get(CONSTANT_ASN_NUMBER)));      
      sb.append("&#x26;ordermessageurl=");
      sb.append(java.net.URLEncoder.encode((String)params.get(CONSTANT_URL)));  
      sb.append("&#x26;operation=");
      sb.append(java.net.URLEncoder.encode((String)params.get(CONSTANT_OPERATION))); 
      sb.append("&#x26;buyercookie=");
      sb.append(java.net.URLEncoder.encode((String)params.get(CONSTANT_ORIGINAL_BUYER_COOKIE))); 
      sb.append("</URL>");
      sb.append("</StartPage>");
      sb.append("</PunchOutSetupResponse>");
      sb.append("</Response>");
      sb.append("</cXML>");
      return sb.toString();
  }
}