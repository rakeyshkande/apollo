package com.ftd.b2b.ariba.businessobjects.punchout;

import com.ftd.b2b.common.utils.XMLHelper;
import com.ftd.b2b.common.utils.XMLParseException;
import com.ftd.b2b.constants.B2BConstants;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.sun.org.apache.xerces.internal.parsers.DOMParser;

import org.xml.sax.*;
import org.xml.sax.helpers.*;
import java.io.*;
import org.w3c.dom.*;

/**
 * Parses XML data specific to the B2B PunchOutRequest Use Case
 *
 * @author York Davis
 * @version 1.0 
 **/
public class PunchOutRequestXMLHelper extends XMLHelper implements B2BConstants
{
  private static final String OPERATION = "operation";

  public PunchOutRequestXMLHelper()
  {
    super();
  }

  /**
   * Extract the BuyerCookie field from the cXML document.
   * 
   * @param   The cXML document
   * @return  The Buyer Cookie field
   *
   * @author York Davis
   **/  
  public String getBuyerCookie(String cXML) throws XMLParseException
  {
      return getTextValueByTagName(cXML, CONSTANT_BUYER_COOKIE);
  }

   /**
    * Extract the OrderMessage URL from the cXML document.
    * 
    * @param   The cXML document
    * @return  The OrderMessage URL
    *
    * @author York Davis
    **/  
  public String getOrderMsgURL(String cXML) throws XMLParseException 
  {
	  
	  try {
		  Document doc = DOMUtil.getDocument(cXML);
		  return DOMUtil.getNodeText(doc, "/cXML/Request/PunchOutSetupRequest/BrowserFormPost/URL");
	  }
		  catch(Throwable t){
		  throw new XMLParseException("Error locating an " + OPERATION + " value.");    
	  }

}

   /**
    * Extract the Operation value from the cXML document.
    * 
    * @param   The cXML document
    * @return  The operation value
    *
    * @author Kristyn Angelo
    **/  
  public String getOperation(String cXML) throws XMLParseException 
  {
	  try {
		  Document doc = DOMUtil.getDocument(cXML);
		  return DOMUtil.getNodeText(doc, "/cXML/Request/PunchOutSetupRequest/@operation");
	  }
		  catch(Throwable t){
		  throw new XMLParseException("Error locating an " + OPERATION + " value.");    
	  }

  }

  
   /**
    * Extract the Supplier Part Auxilary ID value from the cXML document.
    * 
    * @param   The cXML document
    * @return  The Supplier Part Auxilary ID value
    *
    * @author Kristyn Angelo
    **/  
  public String getSupplierAuxID(String cXML) throws XMLParseException 
  {
	  
	  try {
		  Document doc = DOMUtil.getDocument(cXML);
		  return DOMUtil.getNodeText(doc, "/cXML/Request/PunchOutSetupRequest/ItemOut/ItemID/SupplierPartAuxiliaryID");
	  }
		  catch(Throwable t){
		  throw new XMLParseException("Error locating an " + OPERATION + " value.");    
	  }
		  
  }

}