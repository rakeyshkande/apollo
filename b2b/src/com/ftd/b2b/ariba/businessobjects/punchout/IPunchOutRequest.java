package com.ftd.b2b.ariba.businessobjects.punchout;



/**
 * Interface for building a PunchOutResponse document from
 * a PunchOutRequest document.
 *
 * @author York Davis
 * @version 1.0 
 **/
public interface IPunchOutRequest 
{
   /**
   * This method will be called to begin the workflow processing.
   * This method exists so that any uncaught exceptions will be
   * intercepted and handled.
   *
   * FTDApplicationException is thrown in this UseCase as a general error
   * from which this UseCase cannot recover - but which still needs to return
   * an XML response document to the requester. Emails are sent and logging
   * is performed at the point at which the exception is taken. So when 
   * FTDApplicationException is caught in this method, all that needs
   * to be performed is the XML error response. Catching Throwable is an
   * exceptional condition which means that some other uncaught exception 
   * was thrown. Even for Throwable, however, an XML response document still
   * needs to be sent.
   *   
   * @param  The cXML document.
   * @return The cXML response document.
   * @author York Davis
   **/
  public String processPunchOutRequest(String cXML);
}