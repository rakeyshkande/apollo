package com.ftd.b2b.ariba.businessobjects.punchout;

import com.ftd.b2b.ariba.common.utils.IRequestAuthenticator;
import com.ftd.b2b.ariba.common.valueobjects.AribaRequestVO;
import com.ftd.b2b.ariba.integration.dao.*;
import com.ftd.b2b.common.utils.*;
import com.ftd.b2b.constants.B2BConstants;
import com.ftd.b2b.utils.mail.B2BMailer;
import com.ftd.framework.common.exceptions.FTDApplicationException;
import com.ftd.framework.common.exceptions.ResourceNotFoundException;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.framework.common.utilities.ResourceManager;
import java.util.Map;

// Referenced classes of package com.ftd.b2b.ariba.businessobjects.punchout:
//            PunchOutResponseGenerator, PunchOutRequestXMLHelper, IPunchOutRequest

public class PunchOutRequestImpl
    implements IPunchOutRequest, B2BConstants
{

    private IRequestAuthenticator requestAuthenticator;
    private ResourceManager resourceManager;
    private PunchOutResponseGenerator response;
    private Logger logManager;
    private AribaRequestVO aribaRequestVO;

    public PunchOutRequestImpl()
    {
        resourceManager = ResourceManager.getInstance();
    }

    public String processPunchOutRequest(String cXML)
    {
        response = new PunchOutResponseGenerator();
        logManager = new Logger("com.ftd.b2b.Punchout");
        String msg[];
        try
        {
            if(logManager.isDebugEnabled())
            {
                logManager.debug("PunchOutRequest was processed. " + cXML);
            }
            //due to certificate issues at Ariba, take cxml string and convert https in the DOCTYPE tag 
            //and change to http
            cXML = ChangeHTTPS(cXML);
            
            String s = process(cXML);
            return s;
        }
        catch(StringIndexOutOfBoundsException sobe) 
        {
            logManager.debug("PunchOutRequest was not https, so returned error response");
            String s0 = response.generateErrorResponse("ThrowableException", aribaRequestVO);
            return s0;
        }
        catch(FTDApplicationException ftdae)
        {
            String s1 = response.generateErrorResponse("ThrowableException", aribaRequestVO);
            return s1;
        }
        catch(Throwable t)
        {
            msg = (new String[] {
                t.toString()
            });
            logManager.error("processPunchOutRequest: " + t.toString());
            t.printStackTrace();
        }
        B2BMailer.getInstance().send(this, "processPunchOutRequest()", 102, msg, 2);
        String s2 = response.generateErrorResponse("ThrowableException", aribaRequestVO);
        return s2;
    }

    private String process(String cXML)
        throws FTDApplicationException
    {
        com.ftd.b2b.ariba.common.valueobjects.URLLookupVO urlLookupVO = null;
        try
        {
            requestAuthenticator = (IRequestAuthenticator)resourceManager.getImplementation("RequestAuthenticator");
        }
        catch(ResourceNotFoundException rnfe)
        {
            String msg[] = {
                rnfe.toString()
            };
            logManager.error("process: " + rnfe.toString());
            rnfe.printStackTrace();
            B2BMailer.getInstance().send(this, "process()", 1, msg, 2);
            throw new FTDApplicationException(102, rnfe);
        }
        try
        {
            aribaRequestVO = getXMLParams(cXML);
        }
        catch(XMLParseException xmle)
        {
            String s = response.generateErrorResponse("XMLParseException", aribaRequestVO);
            return s;
        }
        catch(DuplicateApprovalException xmle)
        {
            String s = response.generateErrorResponse("OrderNotFoundException", aribaRequestVO);
            return s;
        }
        try
        {
            requestAuthenticator.validateDocument(aribaRequestVO,true,false);
        }
        catch(AuthenticationException ae)
        {
            String s1 = response.generateErrorResponse("AuthenticationException", aribaRequestVO);
            return s1;
        }
        catch(InvalidDUNSException ae)
        {
            String s2 = response.generateErrorResponse("InvalidDUNSException", aribaRequestVO);
            return s2;
        }
        String asnNumber = (String)aribaRequestVO.getParams().get("Identity");
        try
        {
            synchronized(getClass())
            {
                urlLookupVO = requestAuthenticator.getPunchOutClientData(asnNumber);
            }
        }
        catch(InvalidClientException ice)
        {
            logManager.error("process - not a valid punchout client: " + asnNumber);
            String msg = asnNumber + " not a valid PunchOut Client";
            B2BMailer.getInstance().send(this, "getPunchOutClientData()", msg, 2);
            String s3 = response.generateErrorResponse("InvalidClientException", aribaRequestVO);
            return s3;
        }
        return response.generateResponse(urlLookupVO, aribaRequestVO);
    }

    private AribaRequestVO getXMLParams(String cXML)
        throws FTDApplicationException, XMLParseException, DuplicateApprovalException
    {
        String OrigBuyerCookie = null;
        String buyerCookie = null;
        AribaRequestVO aribaRequestVO = requestAuthenticator.getXMLHeaderData(cXML);
        try
        {
            PunchOutRequestXMLHelper xmlHelper = new PunchOutRequestXMLHelper();
            String Operation = xmlHelper.getOperation(cXML);
            logManager.info("Punchout operation: " + Operation);
            if(Operation.equals("edit") || Operation.equals("inspect"))
            {
                String SupplierPartAuxID = xmlHelper.getSupplierAuxID(cXML);
                OrigBuyerCookie = getOriginalBuyerCookie(SupplierPartAuxID, false);  // Only look in pending (not approved) for edit/inspect
                aribaRequestVO.addParam("OrigBuyerCookie", OrigBuyerCookie);
                if (OrigBuyerCookie == null || OrigBuyerCookie.length() < 1) {
                    // We don't want to allow edit of approved order (or inspect if order not found)
                    throw new DuplicateApprovalException();
                }
            } else
            {
                buyerCookie = xmlHelper.getBuyerCookie(cXML);
                aribaRequestVO.addParam("OrigBuyerCookie", buyerCookie);
            }
            buyerCookie = xmlHelper.getBuyerCookie(cXML);
            aribaRequestVO.addParam("BuyerCookie", buyerCookie);
            String URL = xmlHelper.getOrderMsgURL(cXML);
            aribaRequestVO.addParam("URL", URL);
            aribaRequestVO.addParam("operation", Operation);
        }
        catch(XMLParseException xmle)
        {
            logManager.error("getXMLParams - XML parse exception: " +  xmle.toString());
            String msg = xmle.toString() + "\n\n XML document : " + cXML;
            B2BMailer.getInstance().send(this, "getXMLParams()", msg, 1);
            B2BMailer.getInstance().send(this, "getXMLParams()", xmle.toString(), 2);
            throw xmle;
        }
        return aribaRequestVO;
    }

    private String getOriginalBuyerCookie(String SupplierPartAuxID, boolean lookInApproved)
        throws FTDApplicationException
    {
        IPendingLineDAO daoPendingLine = null;
        IApprovedLineDAO daoApprovedLine = null;
        String BuyerCookie = null;
        try
        {
            daoPendingLine = (IPendingLineDAO)resourceManager.getImplementation("PendingLineDAO");
        }
        catch(ResourceNotFoundException e)
        {
            logManager.error("getOriginalBuyerCookie 1: " +  e.toString());
            B2BMailer.getInstance().send(this, "getOriginalBuyerCookie", "PendingLineDAO:" + e.toString(), 1);
            throw new FTDApplicationException(102, e);
        }
        try
        {
            BuyerCookie = daoPendingLine.retrieveBuyerCookie(SupplierPartAuxID);
            if(!BuyerCookie.equals("False"))
            {
                String s = BuyerCookie;
                return s;
            }
        }
        catch(Throwable t)
        {
            logManager.error("getOriginalBuyerCookie 2: " + t.toString());
            t.printStackTrace();
            B2BMailer.getInstance().send(this, "getOriginalBuyerCookie", t.toString(), 2);
            throw new FTDApplicationException(102, t);
        }
        if(BuyerCookie.equals("False") && lookInApproved)
        {
            logManager.info("Looking in approved tables for order: " + SupplierPartAuxID);
            try
            {
                daoApprovedLine = (IApprovedLineDAO)resourceManager.getImplementation("ApprovedLineDAO");
            }
            catch(ResourceNotFoundException e)
            {
                logManager.error("getOriginalBuyerCookie 3: " + e.toString());
                B2BMailer.getInstance().send(this, "getOriginalBuyerCookie", "ApprovedLineDAO:" + e.toString(), 1);
                throw new FTDApplicationException(102, e);
            }
            try
            {
                BuyerCookie = daoApprovedLine.retrieveBuyerCookie(SupplierPartAuxID);
                if(!BuyerCookie.equals("False"))
                {
                    String s2 = BuyerCookie;
                    return s2;
                }
            }
            catch(Throwable t)
            {
                logManager.error("getOriginalBuyerCookie 4: " + t.toString());
                t.printStackTrace();
                B2BMailer.getInstance().send(this, "getOriginalBuyerCookie", t.toString(), 2);
                throw new FTDApplicationException(102, t);
            }
        }
        if(BuyerCookie.equals("False"))
        {
            logManager.error("getOriginalBuyerCookie - No buyer cookie found for edit/inspect punchout: " + SupplierPartAuxID);
            B2BMailer.getInstance().send(this, "getOriginalBuyerCookie()", "No Buyer Cookie Found For Edit/Inspect Punchout Request.", 2);
        }
        return "";
    }

    private String ChangeHTTPS(String incomingCXML) throws FTDApplicationException
    {
        String newCXMLTransmissionBeginning = null;
        String newCXMLTransmissionEnding = null;
        String newCXMLTransmission = null;
        int httpsLocation = -1;

        httpsLocation = incomingCXML.indexOf("https://");

        if (httpsLocation < 100)
        {
            newCXMLTransmissionBeginning = incomingCXML.substring(0, (httpsLocation+4));
            newCXMLTransmissionEnding = incomingCXML.substring( (httpsLocation+5) );
            newCXMLTransmission = newCXMLTransmissionBeginning + newCXMLTransmissionEnding;
            return(newCXMLTransmission);
        }
        else
        {
            return(incomingCXML);
        }
    }
}
