package com.ftd.b2b.ariba.businessobjects.receiveapprovedpo.variations;

import com.ftd.b2b.ariba.businessobjects.receiveapprovedpo.ApoXmlHelper;

import com.ftd.b2b.ariba.common.valueobjects.AribaRequestVO;
import com.ftd.b2b.ariba.common.valueobjects.OrderRequestLineItemVO;
import com.ftd.b2b.ariba.common.valueobjects.OrderRequestOrderVO;
import com.ftd.b2b.ariba.common.valueobjects.ParsedNameVO;
import com.ftd.b2b.ariba.common.valueobjects.PhoneVO;
import com.ftd.b2b.ariba.common.valueobjects.PurchaseCreditCardVO;
import com.ftd.b2b.common.utils.XMLParseException;
import com.ftd.b2b.constants.B2BConstants;

import java.io.ByteArrayInputStream;
import java.io.StringReader;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Iterator;
import java.util.StringTokenizer;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.xpath.*;

/**
 * Top level utility class to convert an XML document into an Order Value Object for
 * EDS.  This class should contain only the EDS specific logic - the bulk 
 * of the logic is common to all clients and resides in the superclass.
 * 
 * @see ApoXmlHelper
 * @see ReceiveApprovedPOImpl#processApprovedPO
 */
public class ApoXmlEds extends ApoXmlHelper {

    private static final String EDS_MOC_RC = "MOC / RC Information";
    private static final String EDS_BLC    = "BLC7";
    private static final String EDS_NUMBER = "number";


  /**
   * Special line item detail processing for EDS
   */
  protected void detailLineItemClientSpecifics() throws XMLParseException, JDOMException {

    try {
        // EDS's BLOC number resides in Payment/BLC7 within OrderRequestHeader
        //
        Element paymentNode = (Element)XPath.selectSingleNode(orderRequestHeaderNode,PAYMENT);
        Element blcNode = (Element)XPath.selectSingleNode(paymentNode,EDS_BLC);
        detailLineVo.setCostCntrExpense(getAttributeValue(blcNode, EDS_NUMBER).trim());
    
        // EDS's MOC/RC number resides in ItemOut/Distribution/Segment within OrderRequest
        //
        Element distributionNode = (Element)XPath.selectSingleNode(itemOutNode,DISTRIBUTION);
        Element accountingNode = (Element)XPath.selectSingleNode(distributionNode,ACCOUNTING);
        List segmentList = XPath.selectNodes(accountingNode,SEGMENT);
        Iterator its = segmentList.listIterator();
        while (its.hasNext()) {
          Element segmentElem = (Element)its.next();
          if ((getAttributeValue(segmentElem, TYPE).equals(EDS_MOC_RC))) {
            String idStr = getAttributeValue(segmentElem, ID).trim();
            // First 3 digits is MOC, last 7 are RC
                detailLineVo.setCostCntrLocation(idStr.substring(0,3));
                detailLineVo.setCostCntrAccount(idStr.substring(3));
          }
        }
    } catch (Exception e) {
       throw new XMLParseException("Could not parse EDS RC/MOC/BLOC information.");
    }
   }
}

