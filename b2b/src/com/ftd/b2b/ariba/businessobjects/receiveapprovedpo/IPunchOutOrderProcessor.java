package com.ftd.b2b.ariba.businessobjects.receiveapprovedpo;
import com.ftd.b2b.ariba.common.valueobjects.OrderRequestOrderVO;
import com.ftd.b2b.ariba.common.valueobjects.IOrderVO;
import com.ftd.framework.common.exceptions.FTDApplicationException;


/**
 * Processes a PunchOut Order
 */
public interface IPunchOutOrderProcessor  
{
    /**
     *  Process order.
     *
     *  @params OrderRequestVO request order
     *  @IOrderVO The existing order as found on the archive or pending table
     */
     public boolean processOrder(OrderRequestOrderVO voOrderRequest, IOrderVO voExistingOrder)  
                    throws FTDApplicationException;



}