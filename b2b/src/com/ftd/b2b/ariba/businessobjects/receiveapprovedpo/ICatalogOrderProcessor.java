package com.ftd.b2b.ariba.businessobjects.receiveapprovedpo;

import com.ftd.b2b.ariba.common.valueobjects.OrderRequestOrderVO;
import com.ftd.framework.common.exceptions.FTDApplicationException;



/**
 * Processes a Catalog Order
 */
public interface ICatalogOrderProcessor 
{
    /**
     *  Process order.
     *
     *  @params OrderRequestVO request order
     */
     public boolean processOrder(OrderRequestOrderVO voOrderRequest)  
                    throws FTDApplicationException;
}