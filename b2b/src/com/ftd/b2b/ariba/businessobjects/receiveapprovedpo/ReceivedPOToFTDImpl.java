package com.ftd.b2b.ariba.businessobjects.receiveapprovedpo;

import com.ftd.b2b.common.utils.BusinessObject;
import com.ftd.framework.common.utilities.ResourceManager;
import com.ftd.framework.common.exceptions.ResourceNotFoundException;
import com.ftd.framework.common.exceptions.FTDApplicationException;
import com.ftd.osp.utilities.ConfigurationUtil;

import java.text.DecimalFormat;

import com.ftd.b2b.ariba.common.valueobjects.ApprovedOrderVO;
import com.ftd.b2b.ariba.common.valueobjects.ApprovedLineItemVO;
import com.ftd.b2b.ariba.integration.dao.IBuyerSourceCodeLookupDAO;
import com.ftd.b2b.common.utils.HTTPProxy;
import com.ftd.b2b.constants.B2BConstants;
import com.ftd.b2b.utils.mail.B2BMailer;

import java.util.*;
import java.text.SimpleDateFormat;
import java.io.StringWriter;
import java.io.IOException;

import org.jdom.*;
import org.jdom.output.*;

public class ReceivedPOToFTDImpl extends BusinessObject implements IReceivedPOToFTD, B2BConstants
{

  private ResourceManager rm;
  private boolean debugEnabled = false;
  private static String asnEds = null;

  /**
    * Constructor
    */
  public ReceivedPOToFTDImpl()
  {
   super(LOG_CATEGORY_RECEIVE_APPROVED_PO);
   if ( super.getLogManager().isDebugEnabled() )  {
     debugEnabled = true;
   }
   rm = ResourceManager.getInstance();
   if (asnEds == null) {
     asnEds = rm.getProperty(PROPERTY_B2B_PROPERTY_FILE, PROPERTY_ASN_EDS);   
   }
  }

  /**
    * Constructor
    */
  public ReceivedPOToFTDImpl(String category)
  {
    super(category);

    if ( super.getLogManager().isDebugEnabled() ) 
    {
      debugEnabled = true;
    }
    rm = ResourceManager.getInstance();
  }  

  /**
     * Convert order to XML and send to order_gatherer
     * 
     * @param voApprovedOrder
     * @throws FTDApplicationException
     */
  public void send(ApprovedOrderVO voApprovedOrder) throws FTDApplicationException
  {
    IBuyerSourceCodeLookupDAO daoBuyerSourceCode = null;
    String sourceCode = null;
    String sDeliveryDate = null;
    String gathererResponse = null;
    SimpleDateFormat sdfOrder = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
    SimpleDateFormat sdfDelivery = new SimpleDateFormat(CONSTANT_HP_DELIVERY_DATE_FORMAT);
    SimpleDateFormat sdfExpiration = new SimpleDateFormat(CONSTANT_HP_CC_EXPIRATION_DATE_FORMAT);
    boolean isAsnEds = false;  // Assume not an EDS order
    String httpAddress = "";
    
    try
    {
      daoBuyerSourceCode = 
              (IBuyerSourceCodeLookupDAO) rm.getImplementation(INTERFACE_BUYERSOURCECODELOOKUPDAO);
    }
    catch(ResourceNotFoundException e)
    {
      getLogManager().error("send 1: " + e.toString());
      B2BMailer.getInstance().send(this, "send()", INTERFACE_BUYERSOURCECODELOOKUPDAO + 
                                                 ":" + e.toString(), B2BMailer.ACTION1);
      throw new FTDApplicationException(ERROR_CODE_UNRECOVERABLE_EXCEPTION,e);
    }
        
    try
    {
      ConfigurationUtil cu = ConfigurationUtil.getInstance();
      httpAddress = cu.getFrpGlobalParm(B2B_ARIBA_CONFIG_CONTEXT, PROPERTY_ORDER_PROCESSING_ADDRESS);
      sourceCode = daoBuyerSourceCode.retrieveByASNNumber(voApprovedOrder.getAsnNumber());
      if (sourceCode == null)
      {
        sourceCode = rm.getProperty(PROPERTY_B2B_PROPERTY_FILE, PROPERTY_DEFAULT_SOURCE_CODE);
      }
    }
    catch(Throwable t)
    {
      getLogManager().error("send 2: " + t.toString());
      B2BMailer.getInstance().send(this, "send()", t.toString(), B2BMailer.ACTION2);
      throw new FTDApplicationException(ERROR_CODE_UNRECOVERABLE_EXCEPTION, t);
    }

    //create dollar amount formattor
    DecimalFormat amountFormatter = new DecimalFormat(DOLLAR_AMOUNT_FORMAT);

    //get line items from order
    List approvedLines = voApprovedOrder.getLineItems();
    
    if (voApprovedOrder.getAsnNumber() != null && voApprovedOrder.getAsnNumber().startsWith(asnEds)) {
        isAsnEds = true;                    // This is an EDS order
    }

    //get first line item
    ApprovedLineItemVO voApprovedLineItem = (ApprovedLineItemVO)approvedLines.get(0);

    //Get credit card values
    String sExpirationDate = "";
    if (voApprovedLineItem.getPCardExpirationDate() != null)
    {
      sExpirationDate = sdfExpiration.format(voApprovedLineItem.getPCardExpirationDate());
    }    

    //format  delivery date
    java.util.Date deliveryDate = voApprovedLineItem.getDeliveryDate();
    if (deliveryDate != null)
    {
      sDeliveryDate = sdfDelivery.format(deliveryDate);
    }

    //get & format transaction date
    String test = CONSTANT_FTD_TRANSACTION_DATE_FORMAT;
    SimpleDateFormat sdfTranDate = new SimpleDateFormat(test);
    String transactionDate = sdfTranDate.format(new java.util.Date());


    //calculate order total
    Iterator totalIterator = approvedLines.listIterator();
    double orderTotal = 0;
    while(totalIterator.hasNext()){
            ApprovedLineItemVO voLine = (ApprovedLineItemVO)totalIterator.next();
            orderTotal = orderTotal + voLine.getTaxAmount() + voLine.getServiceFee() + voLine.getItemPrice();
            }
    orderTotal = Double.parseDouble(amountFormatter.format(orderTotal));                

    //determine the orgin based on order number...catalog & punchout have different sounce codes
    ResourceManager resourceManager = ResourceManager.getInstance();  
    String catalogPrefix = resourceManager.getProperty(PROPERTY_B2B_PROPERTY_FILE, 
                                               PROPERTY_CATALOG_ORDER_PREFIX);
   String orgin = "";                                               
    if (voApprovedLineItem.getOrderNumber().startsWith(catalogPrefix)){
            //Catalog order
            orgin = CATALOG_SOURCE_CODE;
    } else {
            //punchout order
            orgin = PUNCHOUT_SOURCE_CODE;
    }

    //Remove item count from mastser order number
    String masterOrder = "";
    if(voApprovedLineItem.getMasterOrderNumber() != null){
            StringTokenizer idTokenizer = new StringTokenizer(voApprovedLineItem.getMasterOrderNumber(), "/");                
            masterOrder= idTokenizer.nextToken();
    }
    
    //start buiding the xml
    Element eRoot = new Element("order");
    Element e2, e3, e4, e5, e6;
    e2 = new Element("header");
    e3 = new Element("master-order-number");
    e3.setText(checkNull(masterOrder));
    e2.addContent(e3);
    
    e3 = new Element("source-code");
    e3.setText(checkNull(sourceCode));
    e2.addContent(e3);
        
    e3 = new Element("origin");
    e3.setText(checkNull(orgin));
    e2.addContent(e3);

    e3 = new Element("order-count");
    e3.setText(String.valueOf(approvedLines.size()));
    e2.addContent(e3);

    e3 = new Element("order-amount");
    e3.setText(String.valueOf(orderTotal));
    e2.addContent(e3);

    e3 = new Element("transaction-date");
    e3.setText(transactionDate);
    e2.addContent(e3);

    e3 = new Element("socket-timestamp");
    e2.addContent(e3);

    e3 = new Element("contact-first-name");
    e2.addContent(e3);

    e3 = new Element("contact-last-name");
    e2.addContent(e3);

    e3 = new Element("contact-phone");
    e2.addContent(e3);

    e3 = new Element("contact-ext");
    e2.addContent(e3);

    e3 = new Element("contact-email-address");
    e2.addContent(e3);

    e3 = new Element("buyer-first-name");
    e3.setText(checkNull(voApprovedLineItem.getBillToFirstName()));
    e2.addContent(e3);

    e3 = new Element("buyer-last-name");
    e3.setText(checkNull(voApprovedLineItem.getBillToLastName()));
    e2.addContent(e3);

    e3 = new Element("buyer-business");
    e2.addContent(e3);

    e3 = new Element("buyer-address1");
    e3.setText(checkNull(voApprovedLineItem.getBillToAddressLine1()));
    e2.addContent(e3);

    e3 = new Element("buyer-address2");
    e3.setText(checkNull(voApprovedLineItem.getBillToAddressLine2()));
    e2.addContent(e3);

    e3 = new Element("buyer-city");
    e3.setText(checkNull(voApprovedLineItem.getBillToCity()));
    e2.addContent(e3);

    e3 = new Element("buyer-state");
    e3.setText(checkNull(voApprovedLineItem.getBillToState()));
    e2.addContent(e3);

    e3 = new Element("buyer-postal-code");
    e3.setText(checkNull(voApprovedLineItem.getBillToZipCode()));
    e2.addContent(e3);

    e3 = new Element("buyer-country");
    e3.setText(checkNull(voApprovedLineItem.getBillToCountry()));
    e2.addContent(e3);

    e3 = new Element("buyer-primary-phone");
    e3.setText(checkNull(voApprovedLineItem.getBillToWorkPhone()));
    e2.addContent(e3);

    e3 = new Element("buyer-secondary-phone");
    e3.setText(checkNull(voApprovedLineItem.getBillToHomePhone()));
    e2.addContent(e3);

    e3 = new Element("buyer-primary-phone-ext");
    e3.setText(checkNull(voApprovedLineItem.getBillToWorkExtension()));
    e2.addContent(e3);

    e3 = new Element("buyer-fax");
    e3.setText(checkNull(voApprovedLineItem.getBillToFaxNumber()));
    e2.addContent(e3);

    e3 = new Element("buyer-email-address");
    e3.setText(checkNull(voApprovedLineItem.getBillToEmail()));
    e2.addContent(e3);

    e3 = new Element("ariba-buyer-cookie");
    e3.setText(checkNull(voApprovedOrder.getBuyerCookie()));
    e2.addContent(e3);

    e3 = new Element("ariba-asn-buyer-number");
    e3.setText(checkNull(voApprovedOrder.getAsnNumber()));
    e2.addContent(e3);

    e3 = new Element("ariba-payload");
    e3.setText(checkNull(voApprovedOrder.getPayloadID()));
    e2.addContent(e3);

    e3 = new Element("co-brand-credit-card-code");
    e3.setText("N");
    e2.addContent(e3);

    e3 = new Element("news-letter-flag");
    e2.addContent(e3);

    e3 = new Element("cc-type");
    e2.addContent(e3);

    e3 = new Element("cc-number");
    e3.setText(checkNull(voApprovedLineItem.getPCardNumber()));
    e2.addContent(e3);

    e3 = new Element("cc-exp-date");
    e3.setText(sExpirationDate);
    e2.addContent(e3);

    e3 = new Element("cc-approval-code");
    e2.addContent(e3);

    e3 = new Element("cc-approval-amt");
    e2.addContent(e3);

    e3 = new Element("cc-approval-verbage");
    e2.addContent(e3);

    e3 = new Element("cc-approval-action-code");
    e2.addContent(e3);

    e3 = new Element("cc-avs-result");
    e2.addContent(e3);

    e3 = new Element("cc-acq-data");
    e2.addContent(e3);

    e3 = new Element("aafes-ticket-number");
    e2.addContent(e3);

    e3 = new Element("gift-certificates");
    e2.addContent(e3);
            
    e3 = new Element("co-brands");
    //if PCard add this co-brand section.  
    //Note we don't want to set this for EDS since not needed 
    if(voApprovedLineItem.getPCardNumber() != null && voApprovedLineItem.getPCardNumber() != "" && isAsnEds == false){
        String propertiesDunsNumber = "";
        try {
            ConfigurationUtil cu = ConfigurationUtil.getInstance();
            propertiesDunsNumber = cu.getFrpGlobalParm(B2B_ARIBA_CONFIG_CONTEXT, PROPERTY_FTD_DUNS_NUMBER);
        } catch(Exception e) {
          getLogManager().error("send - Can't get DUNS property: " + e.toString());
          B2BMailer.getInstance().send(this, "sendPOToFTD()", e.toString(), B2BMailer.ACTION2);
          throw new FTDApplicationException(ERROR_CODE_UNRECOVERABLE_EXCEPTION,e);
        }
        
        e4 = new Element("co-brand");    

        e5 = new Element("name");
        e5.setText("ID");
        e4.addContent(e5);  
        e5 = new Element("data");
        e5.setText(voApprovedOrder.getInternalSupplierID());
        e4.addContent(e5);
                    
        e5 = new Element("name");
        e5.setText("DUNS");
        e4.addContent(e5);  
        e5 = new Element("data");
        e5.setText(propertiesDunsNumber);
        e4.addContent(e5);
        e3.addContent(e4);
    }
    // If Cost Center Accounting values are present (currently used by Target and EDS only)
    // add this co-brand section.
    String costCntrAccount = voApprovedLineItem.getCostCntrAccount();
    if ((costCntrAccount != null) && (costCntrAccount.trim().length() > 0)){       
        e4 = new Element("co-brand");    

        e5 = new Element("name");
        if (isAsnEds) {
            e5.setText("RC");
        } else {
            e5.setText("act");
        }
        e4.addContent(e5);  
        e5 = new Element("data");
        e5.setText(costCntrAccount);
        e4.addContent(e5);
                    
        e5 = new Element("name");
        if (isAsnEds) {
            e5.setText("MOC");
        } else {
            e5.setText("loc");
        }
        e4.addContent(e5);  
        e5 = new Element("data");
        e5.setText(voApprovedLineItem.getCostCntrLocation());
        e4.addContent(e5);
                    
        e5 = new Element("name");
        if (isAsnEds) {
            e5.setText("BLC");
        } else {
            e5.setText("exp");
        }
        e4.addContent(e5);  
        e5 = new Element("data");
        e5.setText(voApprovedLineItem.getCostCntrExpense());
        e4.addContent(e5);
        e3.addContent(e4);
    }
    
    e2.addContent(e3);
    eRoot.addContent(e2);
            
    e2 = new Element("items");

    //do this part for each item
    Iterator i = approvedLines.listIterator();
    while(i.hasNext()){
    
         voApprovedLineItem = (ApprovedLineItemVO)i.next();    

        //calculate line total
        double lineTotal = voApprovedLineItem.getTaxAmount() + voApprovedLineItem.getServiceFee() + voApprovedLineItem.getItemPrice();
        lineTotal = Double.parseDouble(amountFormatter.format(lineTotal));    

        e3 = new Element("item");

        e4 = new Element("order-number");
        e4.setText(checkNull(voApprovedLineItem.getOrderNumber()));
        e3.addContent(e4);

        e4 = new Element("item-source-code");
        e4.setText(checkNull(sourceCode));
        e3.addContent(e4);

        e4 = new Element("item-of-the-week-flag");
        e4.setText(CONSTANT_ITEM_OF_WEEK_FLAG_DEFAULT);   
        e3.addContent(e4);

        e4 = new Element("order-total");
        e4.setText(String.valueOf(lineTotal));
        e3.addContent(e4);

        e4 = new Element("tax-amount");
        e4.setText(String.valueOf(voApprovedLineItem.getTaxAmount()));
        e3.addContent(e4);

        e4 = new Element("service-fee");
        e4.setText(String.valueOf(voApprovedLineItem.getServiceFee()));
        e3.addContent(e4);

        e4 = new Element("extra-shipping-fee");
        e3.addContent(e4);

        e4 = new Element("drop-ship-charges");
        e3.addContent(e4);

        e4 = new Element("retail-variable-price");
        e4.setText(String.valueOf(voApprovedLineItem.getItemPrice()));
        e3.addContent(e4);

        e4 = new Element("productid");
        e4.setText(checkNull(voApprovedLineItem.getItemNumber()));
        e3.addContent(e4);

        e4 = new Element("product-price");
        e4.setText(String.valueOf(voApprovedLineItem.getRetailPrice()));
        e3.addContent(e4);

        e4 = new Element("first-color-choice");
        e3.addContent(e4);

        e4 = new Element("second-color-choice");
        e3.addContent(e4);

        //**** ADDON SECTION -start *****
        e4 = new Element("add-ons");
        //if addons are on the line 
        String addonDetail = voApprovedLineItem.getAddOnDetail();
        if (addonDetail != null && addonDetail.length() > 0){

            //parse out addon types and counts for each item
            StringTokenizer itemTokenizer = new StringTokenizer(addonDetail, CONSTANT_ADDON_ITEM_DELIMITER);
            while (itemTokenizer.hasMoreElements()){

                //get addon detail & quantity string
                String addonData = itemTokenizer.nextToken();

                //parse out item id and quantity from string
                StringTokenizer idTokenizer = new StringTokenizer(addonData, CONSTANT_ADDON_QUANTITY_DELIMITER);                
                String addon = idTokenizer.nextToken();
                String quantity = idTokenizer.nextToken();            
                    
                    
                e5 = new Element("add-on");
                e6 = new Element("id");
                e6.setText(addon);
                e5.addContent(e6);
                    
                e6 = new Element("quantity");
                e6.addContent(quantity);
                e5.addContent(e6);

                e4.addContent(e5);
            }//end  while token has elements....line contains more addons
        }//end if line contains addons
        e3.addContent(e4);
       //**** ADDON SECTION -end *****

        e4 = new Element("recip-first-name");
        e4.setText(voApprovedLineItem.getShipToFirstName());
        e3.addContent(e4);

        e4 = new Element("recip-last-name");
        e4.setText(voApprovedLineItem.getShipToLastName());
        e3.addContent(e4);

        e4 = new Element("recip-address1");
        e4.setText(voApprovedLineItem.getShipToAddressLine1());
        e3.addContent(e4);

        e4 = new Element("recip-address2");
        e4.setText(voApprovedLineItem.getShipToAddressLine2());
        e3.addContent(e4);

        e4 = new Element("recip-city");
        e4.setText(voApprovedLineItem.getShipToCity());
        e3.addContent(e4);

        e4 = new Element("recip-state");
        e4.setText(voApprovedLineItem.getShipToState());
        e3.addContent(e4);

        e4 = new Element("recip-postal-code");
        e4.setText(voApprovedLineItem.getShipToZipCode());
        e3.addContent(e4);

        e4 = new Element("recip-country");
        e4.setText(voApprovedLineItem.getShipToCountry());
        e3.addContent(e4);

        e4 = new Element("recip-international");
        e3.addContent(e4);

        e4 = new Element("recip-phone");
        e4.setText(voApprovedLineItem.getShipToWorkPhone());
        e3.addContent(e4);

        e4 = new Element("recip-phone-ext");
        e4.setText(voApprovedLineItem.getShipToWorkExtension());
        e3.addContent(e4);

        e4 = new Element("ship-to-type");
        e4.setText(checkNull(voApprovedLineItem.getShipToBusinessType()));
        e3.addContent(e4);

        e4 = new Element("ship-to-type-name");
        e4.setText(checkNull(voApprovedLineItem.getShipToBusinessName()));
        e3.addContent(e4);

        e4 = new Element("ship-to-type-info");
        e3.addContent(e4);

        e4 = new Element("occassion");
        e4.setText(checkNull(voApprovedLineItem.getOccasion()));
        e3.addContent(e4);

        e4 = new Element("delivery-date");
        e4.setText(sDeliveryDate);
        e3.addContent(e4);

        e4 = new Element("second-delivery-date");
        e3.addContent(e4);

        e4 = new Element("card-message");
        e4.setText(checkNull(voApprovedLineItem.getCardMessage()));
        e3.addContent(e4);

        e4 = new Element("card-signature");
        e3.addContent(e4);

        e4 = new Element("special-instructions");
        e4.setText(checkNull(voApprovedLineItem.getSpecialInstructions()));
        e3.addContent(e4);

        e4 = new Element("shipping-method");
        e3.addContent(e4);

        e4 = new Element("lmg-flag");
        e3.addContent(e4);

        e4 = new Element("lmg-email-address");
        e3.addContent(e4);

        e4 = new Element("lmg-email-signature");
        e3.addContent(e4);

        e4 = new Element("fol-indicator");
        e3.addContent(e4);

        e4 = new Element("sunday-delivery-flag");
        e3.addContent(e4);

        e4 = new Element("sender-release-flag");
        e3.addContent(e4);

        e4 = new Element("qms-address1");
        e3.addContent(e4);

        e4 = new Element("qms-address2");
        e3.addContent(e4);

        e4 = new Element("qms-city");
        e3.addContent(e4);

        e4 = new Element("qms-state");
        e3.addContent(e4);

        e4 = new Element("qms-postal-code");
        e3.addContent(e4);

        e4 = new Element("qms-firm-name");
        e3.addContent(e4);

        e4 = new Element("qms-latitude");
        e3.addContent(e4);

        e4 = new Element("qms-longitude");
        e3.addContent(e4);

        e4 = new Element("qms-override-flag");
        e3.addContent(e4);

        e4 = new Element("qms-usps-range-record-type");
        e3.addContent(e4);

        e4 = new Element("ariba-po-number");
        e4.setText(checkNull(voApprovedLineItem.getAribaPONumber()));
        e3.addContent(e4);

        e4 = new Element("ariba-cost-center");
        e4.setText(checkNull(voApprovedLineItem.getAribaCostCenter()));
        e3.addContent(e4);

        e4 = new Element("ariba-ams-project-code");
        e4.setText(checkNull(voApprovedLineItem.getAMSProjectCode()));
        e3.addContent(e4);

        e4 = new Element("ariba-unspsc-code");
        e4.setText(checkNull(voApprovedLineItem.getItemUNSPSCCode()));
        e3.addContent(e4);

        e4 = new Element("product-substitution-acknowledgement");
        e3.addContent(e4);

        // Item Extensions are optional client-specific name/value pairs
        HashMap lineItemExt = voApprovedLineItem.getLineItemExtensions();
        if (lineItemExt != null) {
          e4 = new Element("item-extensions");
          for (Iterator it=lineItemExt.entrySet().iterator(); it.hasNext();) {
            Map.Entry et = (Map.Entry) it.next();
            e5 = new Element((String) "extension");
            e6 = new Element((String) "name");
            e6.setText(checkNull((String) et.getKey()));
            e5.addContent(e6);
            e6 = new Element((String) "data");
            e6.setText(checkNull((String) et.getValue()));
            e5.addContent(e6);
            e4.addContent(e5);  
          }
          e3.addContent(e4);
        }
        e2.addContent(e3);  //close of item tag
    }
    eRoot.addContent(e2);

    try
    {
        // Convert to XML
        //
        StringWriter writer = new StringWriter();
        XMLOutputter out = new XMLOutputter();
        try {
            out.output(eRoot,writer);
        } catch (IOException e) {
            e.printStackTrace();
            getLogManager().error("send - error converting order to XML: " + e.toString());
            B2BMailer.getInstance().send(this, "sendPOToFTD()", e.toString(), B2BMailer.ACTION2);
            throw new FTDApplicationException(ERROR_CODE_UNRECOVERABLE_EXCEPTION, e);	
        }		

        // Actual send to order_gatherer
        //
        getLogManager().info("send - sending order to order_gatherer: " + masterOrder);
        Map orderMap = new HashMap();
        orderMap.put(ORDER_PROCESSING_ORDER_PARAM,writer.toString());
        gathererResponse = HTTPProxy.send(httpAddress,orderMap);
 //???       
    }
    catch(Throwable t)
    {
        t.printStackTrace();
        getLogManager().error("send - error sending XML to order_gatherer: " + t.toString());
        B2BMailer.getInstance().send(this, "sendPOToFTD()", t.toString(), B2BMailer.ACTION2);
        throw new FTDApplicationException(ERROR_CODE_UNRECOVERABLE_EXCEPTION, t);
    }//end catch throwable
  
}

   /**
    * Check to see if a String is null.  If so, return an empty string
    *
    * @parm String containing value to check
    * @returns Empty String if null, parameter string if not
    * @throws FTD Application Exception when an unexcepted error occurs
    * @author Kristyn Angelo
    */
  private String checkNull(String data)
  {
      if (data == null)
      {
          return "";
      }
      else
      {
          return data;
      }
  }   

   /**
   * Not Used
   */
   public void create(){ }

 /**
   * Not Used
   */
   public void save(){ }

 /**
   * Determine if objects are the same .
   * Note a deep compare is not done
   * @parms object to compare to
   * @returns True if objects are equal, else false
   */
   public boolean equals(Object obj)
   { 
      return(obj == this);
   }

 /**
   * Returns the class name
   * @returns class name
   */
   public String toString()
   { 
      return this.getClass().toString();
   }

}