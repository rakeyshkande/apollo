package com.ftd.b2b.ariba.businessobjects.receiveapprovedpo;

import com.ftd.b2b.ariba.common.valueobjects.ApprovedOrderVO;
import com.ftd.b2b.ariba.common.valueobjects.OrderRequestLineItemVO;
import com.ftd.b2b.ariba.common.valueobjects.ApprovedLineItemVO;
import com.ftd.b2b.ariba.common.valueobjects.OrderRequestOrderVO;
import com.ftd.b2b.utils.mail.B2BMailer;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import com.ftd.b2b.ariba.integration.dao.ICatalogOrderNumberDAO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.framework.common.utilities.ResourceManager;
import com.ftd.framework.common.exceptions.ResourceNotFoundException;
import com.ftd.framework.common.exceptions.FTDApplicationException;


/**
 * Processes a Catalog Order
 */


 /**
  * The Addon logic was commented out.  Catalog order cannot contain addons
  * because we do not have a way of matching addons with orders...catalog
  * order do not have an FTD Order Number.  Code was placed in here to send
  * in email by chance a catalog order with an Addon was received.
  */
public class CatalogOrderProcessorImpl extends OrderProcessorImpl 
        implements IOrderProcessor, ICatalogOrderProcessor
{

        
    /**
     * Constructor
     */
    public CatalogOrderProcessorImpl()
    {
        super(LOG_CATEGORY_CATALOG_ORDER_PROCESSOR );

        //setup logging
        lm = new Logger(LOG_CATEGORY_RECEIVE_APPROVED_PO);
        if ( lm.isDebugEnabled() )  {
            debugEnabled = true;

        rm = ResourceManager.getInstance();
        }   

    }//end Constructor

    /**
     *  Process order.
     *
     *  @params OrderRequestVO request order
     *  @returns String Servlet response
     */
     public boolean processOrder(OrderRequestOrderVO voOrderRequest)  
                    throws FTDApplicationException{


        if (debugEnabled){
            lm.debug("Processing catalog request");
            }

        //build the order
        ApprovedOrderVO voApprovedOrder = buildOrder(voOrderRequest);

        //Save order to Approved and OrderEntry tables
        persistOrder(voApprovedOrder);

         // If we made it to here then all went well
         return true;
         
     }//end, process order



/**
 * Builds an Approved OrderVO based on the passed in order request.
 *
 * @params OrderRequestOrderVO  request order
 * @returns ApprovedOrderVO  the approved order object
 */
private ApprovedOrderVO buildOrder(OrderRequestOrderVO voOrderRequest)
                    throws FTDApplicationException {


        int approvedLineCount = 0;
        List approvedLineItems = null;

        //VO to be inserted into the Approved table
        ApprovedOrderVO voApprovedOrder= new ApprovedOrderVO();        

        //obtain master order number
        String masterOrderNumber = getNextMasterOrderNumber();

        //populate the approved vo ORDER info based on REQUEST data       
        populateHeader(voApprovedOrder, voOrderRequest);
        voApprovedOrder.setCreatedDate(voApprovedOrder.getApprovedDate());
        voApprovedOrder.setBuyerCookie(masterOrderNumber);
        
        //List containing all lines with addon information
        List addonList = new ArrayList();

        //Get Service Fee
        String serviceFee =  rm.getProperty(PROPERTY_B2B_PROPERTY_FILE, PROPERTY_CATALOG_SERVICE_FEE);          

        //Add all line items to approved order
        List requestLines = voOrderRequest.getLineItems();
        int numberOfLines = requestLines.size();
        for(int i=0; i < numberOfLines; i++){
        
            //get line item
            OrderRequestLineItemVO voRequestLine = 
                                    (OrderRequestLineItemVO)requestLines.get(i);          
            String ftdOrderNumber = voRequestLine.getOrderNumber();
            String supplierPartID = voRequestLine.getPartId();

            //Is this line an addon..if so yell, scream, and send emails.
            if (supplierPartID.startsWith(CONSTANT_ADDONFLAG)){
                //addonList.add(voRequestLine);
                String[] msg = new String[] {masterOrderNumber};                                                                                            
                lm.error("buildOrder - catalog contains addons: " + masterOrderNumber);
                B2BMailer.getInstance().send(this, "buildOrder().",ERROR_CODE_CATALOGCONTAINSADDONS,msg , B2BMailer.ACTION2);      

                }
            else{
                //Add line to order
                ApprovedLineItemVO voApprovedLine = new ApprovedLineItemVO();
                moveRequestDataToApproveLine(voApprovedLine,voRequestLine,voOrderRequest);
                populateCatalogValues(voApprovedLine,serviceFee);
                voApprovedLine.setAribaBuyerCookie(masterOrderNumber);
                voApprovedLine.setItemPrice(voRequestLine.getItemPrice());
                voApprovedOrder.addLineItem(voApprovedLine);
               
                //split lines if neccessary
                splitLines(voApprovedOrder,voApprovedLine,voRequestLine);                   
                }
                
        } // end for loop through each order request line 

        //REMOVED BECAUSE CURRENTLY CATALOG ORDERS CANNOT HAVE ADDONS
        //Process addon lines...this must be processed after all line items
        //have been added to the order.
        //numberOfLines = addonList.size();
        //for (int i = 0; i < numberOfLines ; i++){
        //    updateAddonFields(voApprovedOrder,(OrderRequestLineItemVO)addonList.get(i));
        //}      


        approvedLineItems = voApprovedOrder.getLineItems();
        approvedLineCount = approvedLineItems.size();

        //create master order number..append line count to master order number
        masterOrderNumber = masterOrderNumber + 
                                   CONSTANT_MASTER_ORDER_NUMBER_SEPERATOR +
                                   approvedLineCount;

        //Set master order number for all lines
        for (int i=0;i<approvedLineCount;i++){
            ApprovedLineItemVO line = (ApprovedLineItemVO)approvedLineItems.get(i);
            line.setMasterOrderNumber(masterOrderNumber);
        }

        return voApprovedOrder;

}



/**
 * Populates the passed in approved line VO using business logic
 * related to catalog order processing.
 *
 * @params ApprovedLineItemVO Popuate approved line item VO
 */
 private void populateCatalogValues(ApprovedLineItemVO voApprovedLine,String serviceFee)
    throws FTDApplicationException{
    
    //Occasion
    //if (voApprovedLine.getItemNumber().startsWith(CONSTANT_STYPE_PREFIX)){
    //    voApprovedLine.setOccasion(CONSTANT_STYPE_OCCASION_CODE);
    //    }
    //else{
    // Just default to "other"
    voApprovedLine.setOccasion(CONSTANT_NONSTYPE_OCCASION_CODE);
    //    }

    //Service Fee
    try{
    voApprovedLine.setServiceFee(new Float(serviceFee).floatValue());
    }    
    catch (NumberFormatException e)
        {
            String[] msg = new String[] {"Service fee in property file not numeric." + e.toString()};                                                                                                    
            lm.error(msg[0]);
            B2BMailer.getInstance().send(this, "populateCatalogValues",ERROR_CODE_VALUENOTNUMERIC,msg, B2BMailer.ACTION2);      
            throw new FTDApplicationException(ERROR_CODE_VALUENOTNUMERIC, e);
        }    

    //Delivery Date
    if (voApprovedLine.getDeliveryDate() == null){
        voApprovedLine.setDeliveryDate(new Date());
        }

    //set order number
    voApprovedLine.setOrderNumber(getNextCatalogOrderNumber());
    
 }

/**
 * Get the next order number from the database.
 *
 * @returns String order number to use for next order
 */
protected String getNextCatalogOrderNumber() throws FTDApplicationException{
        ICatalogOrderNumberDAO daoOrderNumber = null;
        String newOrderNumber = null;
        try{            

            //first check pending table
            daoOrderNumber = 
                (ICatalogOrderNumberDAO) rm.getImplementation(INTERFACE_CATALOGORDERNUMBERDAO); 
            newOrderNumber =  daoOrderNumber.retrieveCatalogNumber();
            }
        catch(ResourceNotFoundException e){
            String[] msg = new String[] {e.toString()};                                                                                                    
            lm.error("getNextCatalogOrderNumber: " + e.toString());
            B2BMailer.getInstance().send(this,"getNextCatalogOrderNumber()",ERROR_CODE_UNABLETOLOADSERVICE,msg,B2BMailer.ACTION2);
            throw new FTDApplicationException(ERROR_CODE_UNRECOVERABLE_EXCEPTION, e);
            }
        catch(Exception e){
            String[] msg = new String[] {e.toString()};                                                                                                    
            lm.error("getNextCatalogOrderNumber: " + e.toString());
            B2BMailer.getInstance().send(this,"getNextOrderNumber()",ERROR_CODE_UNRECOVERABLE_EXCEPTION,msg,B2BMailer.ACTION2);
            throw new FTDApplicationException(ERROR_CODE_UNRECOVERABLE_EXCEPTION, e);
            }

        return newOrderNumber;
}



    /**
     * Not Used
     */
     public void create(){ }

    /**
     * Not Used
     */
     public void save(){ }

    /**
    * Determins if objects are the same.  
    * Note, a deep compare is not done.
    * @parms object to cmpare to
    * @returns True of objects equal, else False
    */
    public boolean equals(Object obj){
     return (obj == this); }

    /**
    * Returns the class name.
    * @returns Class Name
    */
    public String toString(){
        return this.getClass().toString(); }


 /**
  * This method takes in approved order object and a lineitem.  The method will
  * determine if the line needs to be split into mulitple lines (quantity > 1).
  * If the line needs to be broken up in will copy the existing line item and
  * add it to the order.  If the line does not need to broken apart this function
  * will not change the order object.
  *
  *@params ApprovedOrderVO approved order object
  *@params ApprovedLineItemVO Line item to be copied if needed
  *@params OrderRequestLineItemVO line item used to determine if row needs to copied
  */
  protected ApprovedOrderVO splitLines(ApprovedOrderVO voApprovedOrder,
                                    ApprovedLineItemVO voApprovedLine,
                                    OrderRequestLineItemVO voRequestLine)
                                    throws FTDApplicationException{


                //Break apart lines which has a quantity greater then one,
                //this means the quantity has changed
                int quantity = voRequestLine.getQuantity();
                for(int n = 2; n <= quantity; n++){

                    //copy existing approved line item
                    ApprovedLineItemVO voNewLineItem = null;
                    try{
                        voNewLineItem = 
                                (ApprovedLineItemVO)voApprovedLine.copy();
                        }
                    catch (java.lang.CloneNotSupportedException e){
                        String[] msg = new String[] {e.toString()};                                                                                            
                        lm.error("splitLines: "  + e.toString());
                        B2BMailer.getInstance().send(this,"splitLines()",ERROR_CODE_CLONENOTSUPPORTED,msg,B2BMailer.ACTION2);
                        throw new FTDApplicationException(ERROR_CODE_CLONENOTSUPPORTED, e);
                        } //end, catch                
    
                    //clear fields
                    voNewLineItem.setAddOnCount(0);
                    voNewLineItem.setAddOnDetail("");
                    voNewLineItem.setTaxAmount(0);
                    voNewLineItem.setOrderTotal(0);

                    //set new order number
                    voNewLineItem.setOrderNumber(getNextCatalogOrderNumber());
                    
                    voApprovedOrder.addLineItem(voNewLineItem);
            
                }//end for to break apart lines


    return voApprovedOrder;
  }  

}