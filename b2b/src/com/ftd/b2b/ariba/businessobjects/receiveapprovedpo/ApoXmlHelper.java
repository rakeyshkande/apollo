package com.ftd.b2b.ariba.businessobjects.receiveapprovedpo;

import com.ftd.b2b.ariba.common.valueobjects.AribaRequestVO;
import com.ftd.b2b.ariba.common.valueobjects.OrderRequestLineItemVO;
import com.ftd.b2b.ariba.common.valueobjects.OrderRequestOrderVO;
import com.ftd.b2b.ariba.common.valueobjects.ParsedNameVO;
import com.ftd.b2b.ariba.common.valueobjects.PhoneVO;
import com.ftd.b2b.ariba.common.valueobjects.PurchaseCreditCardVO;
import com.ftd.b2b.common.utils.XMLParseException;
import com.ftd.b2b.constants.B2BConstants;

import java.io.ByteArrayInputStream;
import java.io.StringReader;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Iterator;
import java.util.StringTokenizer;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.xpath.*;

/////////////////////////////////////////////
// Unresolved Issues:
//    1) LineItem comments?
/////////////////////////////////////////////

/**
 * Abstract utility class to convert an XML document into an Order Value Object.
 * This class uses the Oracle DOM parser to perform the extraction.
 * Client specific variations should use this as a base class.
 **/
public abstract class ApoXmlHelper implements B2BConstants
{
   protected static final String[] suffixes = {" JR ", " SR ", " III ", " MD "};
   protected static final String[] prefixes = {" DR ", " MR ", " MS ",  " MRS ", " MISS "};
   protected static final String[] special  = {".", ",", "&"};

   protected static final String ISOCOUNTRYCODE        = "isoCountryCode";
   protected static final String COUNTRY               = "Country";
   protected static final String REQUEST               = "Request";
   protected static final String ORDERREQUEST          = "OrderRequest";
   protected static final String ORDERREQUESTHEADER    = "OrderRequestHeader";
   protected static final String TYPE                  = "type";
   protected static final String ORDERDATE             = "orderDate";
   protected static final String ORDERID               = "orderID";
   protected static final String COMMENTS              = "Comments";
   protected static final String ITEMOUT               = "ItemOut";
   protected static final String QUANTITY              = "quantity";
   protected static final String COSTCENTER            = "Cost Center";
   protected static final String PROJECT_CODE          = "Project Code";
   protected static final String ID                    = "id";
   protected static final String REQUESTEDDELIVERYDATE = "requestedDeliveryDate";
   protected static final String PHONE                 = "Phone";   
   protected static final String FAX                   = "Fax";      
   protected static final String NAME                  = "name";      
   protected static final String ADDRESS_NAME          = "Name";      
   protected static final String WORK                  = "work";      
   protected static final String HOME                  = "home";  
   protected static final String DEFAULT_PHONE_NUMBER  = "0000000000";
   protected static final String DISTRIBUTION          = "Distribution";
   protected static final String EMAIL                 = "Email";
   protected static final String DELIVERTO             = "DeliverTo";
   protected static final String STREET                = "Street";
   protected static final String SHIPTO                = "ShipTo";
   protected static final String BILLTO                = "BillTo";
   protected static final String STATE                 = "State";
   protected static final String PAYMENT               = "Payment";
   protected static final String PCARD                 = "PCard";
   protected static final String PCARDNUMBER           = "number";
   protected static final String PCARDEXPIRE           = "expiration";
   protected static final String PCARDNAME             = "name";
   protected static final String EXTRINSIC             = "Extrinsic";
   protected static final String TOTAL                 = "Total";
   protected static final String ADDRESS               = "Address";
   protected static final String POSTALADDRESS         = "PostalAddress";
   protected static final String POSTALCODE            = "PostalCode";
   protected static final String CITY                  = "City";
   protected static final String TELEPHONENUMBER       = "TelephoneNumber";
   protected static final String NUMBER                = "Number";
   protected static final String AREAORCITYCODE        = "AreaOrCityCode";
   protected static final String SUPPLIERPARTID        = "SupplierPartID";
   protected static final String SUPPLIERPARTAUXID     = "SupplierPartAuxiliaryID";
   protected static final String ITEMID                = "ItemID";
   protected static final String ITEMDETAIL            = "ItemDetail";
   protected static final String UNITPRICE             = "UnitPrice";
   protected static final String MONEY                 = "Money";
   protected static final String ACCOUNTING            = "Accounting";
   protected static final String SEGMENT               = "Segment";
   protected static final String DESCRIPTION           = "Description";
   protected static final String CLASSIFICATION        = "Classification";
   protected static final int    COST_CENTER_MAX_LEN   = 40;
   
   protected String  orderComments = "";
   protected String  orderType     = "";
   protected String  approvedDate  = "";
   protected PhoneVO phoneVO;
   protected int ShipToLocation = CONSTANT_LINE_SHIP_TO;

   protected Element orderRequestHeaderNode;

   protected OrderRequestLineItemVO baseLineVo;
   protected Element baseLineBillToNode;
   protected Element baseLineShipToNode;

   protected OrderRequestLineItemVO detailLineVo;
   protected Element itemOutNode;
   protected Element itemDetailNode;


  /**
   * Workflow method to begin the process of creating an OrderRequestOrderVO
   * from the XML document.
   *
   * @param AribaRequestVO. This object has already been mined in order to
   *        perform the validation of the Ariba Buyer.
   * @param XML document
   * @return Constructed Order VO
   * @throws XMLParseException when an unexpected error occurs parsing the document.   
   * @author York Davis
   **/
   public OrderRequestOrderVO buildOrder(AribaRequestVO requestVO, String xml)
                              throws XMLParseException
   {                              
         try
         {
           return extractOrder(requestVO, xml);
         }
         catch (Throwable throwable)
         {
            throw new XMLParseException(throwable.toString());
         }
   }

  /**
   * Create an OrderRequestOrderVO from the XML document.
   *
   * @param AribaRequestVO. This object has already been mined in order to
   *        perform the validation of the Ariba Buyer.
   * @param XML document
   * @return Constructed Order VO
   * @throws XMLParseException when an unexpected error occurs parsing the document.   
   * @author York Davis
   **/
   private OrderRequestOrderVO extractOrder(AribaRequestVO requestVO, String xml) throws XMLParseException
   {
         HashMap map = (HashMap) requestVO.getParams();
         String asnNum = (String) map.get(CONSTANT_ASN_NUMBER);
         OrderRequestLineItemVO lineItemVO = buildBaseLineItemVO(xml, asnNum);
         OrderRequestOrderVO    orderVO = extractLineItemDetail(lineItemVO, xml, asnNum);

         orderVO.setAsnNumber(asnNum);
         orderVO.setPayloadID((String)map.get(CONSTANT_PAYLOAD_ID));
         orderVO.setInternalSupplierID((String)map.get(CONSTANT_FTD_INTERNAL_SUPPLIER_ID));
         orderVO.setComments(orderComments);
         orderVO.setOrderType(orderType);
         orderVO.setApprovedDate(buildApprovedDate(approvedDate));
         return orderVO;
   }


  /** 
   * Abstract method which should be implemented to handle any client-specific
   * XML processing for line item details.  This method is invoked only by 
   * extractLineItemDetail.
   * 
   * @see #extractLineItemDetail
   */
  protected abstract void detailLineItemClientSpecifics() throws XMLParseException, JDOMException;


  /**
   * Build a base Line Item VO from the XML document. This is a base Object that contains
   * common BillTo and ShipTo information. From this object all other Line Item Objects
   * will be cloned. This is due to the fact that the XML document contains only one section
   * of BillTo and ShipTo data that is common to all Line Items - even though all Line Item
   * objects must contain this same data. This is a requirement for the FTD order processing
   * system.
   *
   * @param  XML document
   * @param  ASN number 
   * @return Constructed Line Item VO
   * @throws XMLParseException when an unexpected error occurs parsing the document.   
   * @author York Davis
   **/
  private OrderRequestLineItemVO buildBaseLineItemVO(String cXML, String asnNum) throws XMLParseException 
  {
      SAXBuilder builder;
      Document doc;
      Element root;
      Element requestNode;
      String shipToStreetAddress = "";

      // Start with empty slate
      baseLineVo = new OrderRequestLineItemVO();  
      baseLineShipToNode = null;
      baseLineBillToNode = null;

      StringReader reader = new StringReader(cXML);
      try
      {
        builder = new SAXBuilder();
        doc = builder.build(reader);
        root = doc.getRootElement();
    	}
    	catch (Throwable ioe)
    	{
    		throw new XMLParseException(ioe.toString());
    	}

      try {
        requestNode = (Element)XPath.selectSingleNode(root,REQUEST);
        List nl = XPath.selectNodes(requestNode,ORDERREQUEST);
        if (nl.size() != 1)
        {
          throw new XMLParseException("Unexpected number of 'OrderRequest' elements found.");
        }    

        Element orderRequestNode = (Element) nl.get(0);
        orderRequestHeaderNode = (Element)XPath.selectSingleNode(orderRequestNode,ORDERREQUESTHEADER);
        orderType = getAttributeValue(orderRequestHeaderNode, TYPE);
        approvedDate = getAttributeValue(orderRequestHeaderNode, ORDERDATE);
        baseLineVo.setAribaPONumber(getAttributeValue(orderRequestHeaderNode, ORDERID));
        Element totalNode = (Element)XPath.selectSingleNode(orderRequestHeaderNode,TOTAL); 
        baseLineShipToNode = (Element)XPath.selectSingleNode(orderRequestHeaderNode,SHIPTO);
        baseLineBillToNode = (Element)XPath.selectSingleNode(orderRequestHeaderNode,BILLTO);

        // Ship-To info
        //
        if (baseLineShipToNode != null) {
            Element shipAddressNode = (Element)XPath.selectSingleNode(baseLineShipToNode,ADDRESS);
            //Element shipName = (Element)XPath.selectSingleNode(shipAddressNode,ADDRESS_NAME);

            // <PostalAddress>...
            Element shipPostalAddressNode = (Element)XPath.selectSingleNode(shipAddressNode,POSTALADDRESS);
            List deliverToList = XPath.selectNodes(shipPostalAddressNode,DELIVERTO);
            if (deliverToList != null && deliverToList.size() > 0) {
                Element deliverTo = (Element) deliverToList.get(0);
                ParsedNameVO shipParsedName = parseName(getTextValue(deliverTo));
                baseLineVo.setShipToFirstName(shipParsedName.getFirstName());
                baseLineVo.setShipToLastName(shipParsedName.getLastName());
                if (deliverToList.size() > 1) {
                  deliverTo = (Element) deliverToList.get(1);
                  baseLineVo.setShipToLastName(shipParsedName.getLastName() + " " + getTextValue(deliverTo));
                }
            }
            List streetList = XPath.selectNodes(shipPostalAddressNode,STREET);
            Iterator it = streetList.listIterator();
            while (it.hasNext()) {
              Element streetElem = (Element)it.next();
              shipToStreetAddress = shipToStreetAddress + " "  + getTextValue(streetElem);
            }
            baseLineVo.setShipToAddressLine1(shipToStreetAddress.trim());
            baseLineVo.setShipToAddressLine2("");  //Set Ship To Address Line 2 to empty
            baseLineVo.setShipToCity(getTextValue((Element)XPath.selectSingleNode(shipPostalAddressNode,CITY)));
            Element stateElem = (Element)XPath.selectSingleNode(shipPostalAddressNode,STATE); 
            if (stateElem == null) {
              baseLineVo.setShipToState("NA");
            } else {
              baseLineVo.setShipToState(getTextValue(stateElem));
            }
            baseLineVo.setShipToZipCode(getTextValue((Element)XPath.selectSingleNode(shipPostalAddressNode,POSTALCODE)));
            Element countryElem = (Element)XPath.selectSingleNode(shipPostalAddressNode,COUNTRY);
            baseLineVo.setShipToCountry(getAttributeValue(countryElem, ISOCOUNTRYCODE));
            // ...</PostalAddress>
            baseLineVo.setShipToEmail(getTextValue((Element)XPath.selectSingleNode(shipAddressNode,EMAIL)));
            phoneVO = extractPhone(shipAddressNode);
            baseLineVo.setShipToWorkPhone(phoneVO.getWorkPhone());          
            baseLineVo.setShipToHomePhone(phoneVO.getHomePhone());
            baseLineVo.setShipToFaxNumber(phoneVO.getFaxPhone());
            ShipToLocation = CONSTANT_HEADER_SHIP_TO;
        }

        // Copy Bill-To info
        baseLineBillToCopy();

        Element orderCommentsElem = (Element)XPath.selectSingleNode(orderRequestHeaderNode,COMMENTS);
        // Order-level Comments are optional and should be null if not found.
        orderComments = null; 
        if (orderCommentsElem != null) {
          orderComments = getTextValue(orderCommentsElem);
        }
        Element paymentNode = (Element)XPath.selectSingleNode(orderRequestHeaderNode,PAYMENT);
        if (paymentNode != null) {
          Element pcardNode = (Element)XPath.selectSingleNode(paymentNode,PCARD);
          if (pcardNode != null) {
            PurchaseCreditCardVO voPCard = extractPCard(pcardNode);
            baseLineVo.setPCardNumber(voPCard.getCreditCardNumber());
            baseLineVo.setPCardExpirationDate(voPCard.getExpirationDate());
            baseLineVo.setPCardName(voPCard.getCreditCardName());
          }
        }

      } catch (JDOMException e) {
        throw new XMLParseException("JDOM exception: " + e);
      }

      return baseLineVo;
  }


  /**
   * Copy Bill-To information for base line-item.  This is separate method
   * (instead of within buildBaseLineItemVO) so it can be overwritten by 
   * client implementations (e.g., Target) that need different functionality.
   */
  protected void baseLineBillToCopy() throws JDOMException
  {
      String billToStreetAddress = "";
      if (baseLineBillToNode != null) {
          Element billAddressNode = (Element)XPath.selectSingleNode(baseLineBillToNode,ADDRESS);
          Element billName = (Element)XPath.selectSingleNode(billAddressNode,ADDRESS_NAME);
          ParsedNameVO billParsedName = parseName(getTextValue(billName));
          baseLineVo.setBillToFirstName(billParsedName.getFirstName());
          baseLineVo.setBillToLastName(billParsedName.getLastName());

          // <PostalAddress>...
          Element billPostalAddressNode = (Element)XPath.selectSingleNode(billAddressNode,POSTALADDRESS);
          List streetList = XPath.selectNodes(billPostalAddressNode,STREET);
          Iterator it = streetList.listIterator();
          while (it.hasNext()) {
            Element streetElem = (Element)it.next();
            billToStreetAddress = billToStreetAddress + " "  + getTextValue(streetElem);
          }
          baseLineVo.setBillToAddressLine1(billToStreetAddress.trim());
          baseLineVo.setBillToAddressLine2("");  //Set Bill To Address Line 2 to empty
          baseLineVo.setBillToCity(getTextValue((Element)XPath.selectSingleNode(billPostalAddressNode,CITY))); 
          Element stateElem = (Element)XPath.selectSingleNode(billPostalAddressNode,STATE); 
          if (stateElem == null) {
            baseLineVo.setBillToState("NA");
          } else {
            baseLineVo.setBillToState(getTextValue(stateElem));
          }
          baseLineVo.setBillToZipCode(getTextValue((Element)XPath.selectSingleNode(billPostalAddressNode,POSTALCODE))); 
          Element countryElem = (Element)XPath.selectSingleNode(billPostalAddressNode,COUNTRY); 
          baseLineVo.setBillToCountry(getAttributeValue(countryElem, ISOCOUNTRYCODE));
          // ...</PostalAddress>

          baseLineVo.setBillToEmail(getTextValue((Element)XPath.selectSingleNode(billAddressNode,EMAIL))); 
          phoneVO = extractPhone(billAddressNode);
          baseLineVo.setBillToWorkPhone(phoneVO.getWorkPhone());          
          baseLineVo.setBillToHomePhone(phoneVO.getHomePhone());
          baseLineVo.setBillToFaxNumber(phoneVO.getFaxPhone());
      }
  }


  /**
   * Extract detailed Line Item information from the section of the XML document
   * containing unique information across differing Line Items within an Order.
   * This method will use the base Line Item object (the one which contains
   * common BillTo and ShipTo data for all Line Items) and clone that object.
   * The cloned object will then be set with data that is unique per Line Item.
   *
   * @param  Cloneable, "base" Line Item VO
   * @param  XML document
   * @param  ASN number 
   * @return Constructed Order VO
   * @throws XMLParseException when an unexpected error occurs parsing the document.
   * @author York Davis
   **/
   private OrderRequestOrderVO extractLineItemDetail(OrderRequestLineItemVO vo, String cXML, String asnNum) 
                              throws XMLParseException 
   {
      OrderRequestOrderVO orderVO = new OrderRequestOrderVO();
      String shipToStreetAddress = "";
      SAXBuilder builder;
      Document doc;
      Element root;
      Element requestNode;
      Element orderRequestNode;

      StringReader reader = new StringReader(cXML);
      try
      {
        builder = new SAXBuilder();
        doc = builder.build(reader);
        root = doc.getRootElement();
    	}
    	catch (Throwable ioe)
    	{
    		throw new XMLParseException(ioe.toString());
    	}

      try {
        requestNode = (Element)XPath.selectSingleNode(root,REQUEST);
        orderRequestNode = (Element)XPath.selectSingleNode(requestNode,ORDERREQUEST);
        List itemOutList = XPath.selectNodes(orderRequestNode,ITEMOUT);
        Iterator it = itemOutList.listIterator();
        
        // Loop for each line item
        //
        int itemCnt = 0;
        while (it.hasNext()) {
          itemOutNode = (Element)it.next();
          itemCnt++;

          try {
            detailLineVo = (OrderRequestLineItemVO) vo.copy();
          } catch (CloneNotSupportedException cnse) {
            throw new XMLParseException(cnse.toString());
          }
          detailLineVo.setAribaLineNumber(itemCnt);

          double dQuantity = Double.parseDouble(getAttributeValue(itemOutNode, QUANTITY));
          detailLineVo.setQuantity((int) dQuantity);
          String reqDeliveryDate = itemOutNode.getAttributeValue(REQUESTEDDELIVERYDATE);
          if (reqDeliveryDate != null) {
            detailLineVo.setDeliveryDate(buildDate(reqDeliveryDate));
          } // Will be null otherwise

          Element itemIdNode = (Element)XPath.selectSingleNode(itemOutNode,ITEMID);
          detailLineVo.setPartId(getTextValue((Element)XPath.selectSingleNode(itemIdNode,SUPPLIERPARTID)));
          detailLineVo.setOrderNumber(getTextValue((Element)XPath.selectSingleNode(itemIdNode,SUPPLIERPARTAUXID)));

          itemDetailNode = (Element)XPath.selectSingleNode(itemOutNode,ITEMDETAIL);
          Element unitPriceNode = (Element)XPath.selectSingleNode(itemDetailNode,UNITPRICE);
          detailLineVo.setItemPrice(Float.parseFloat(getTextValue((Element)XPath.selectSingleNode(unitPriceNode,MONEY))));
          detailLineVo.setItemDescription(getTextValue((Element)XPath.selectSingleNode(itemDetailNode,DESCRIPTION)));
          detailLineVo.setItemUNSPSCCode(getTextValue((Element)XPath.selectSingleNode(itemDetailNode,CLASSIFICATION)));

          //Check to see if Ship To is at the line level
          if (ShipToLocation == CONSTANT_LINE_SHIP_TO) {
            Element shipToNode = (Element)XPath.selectSingleNode(itemOutNode,SHIPTO);
            if (shipToNode != null) {
                Element shipAddressNode = (Element)XPath.selectSingleNode(shipToNode,ADDRESS);
                //Element shipName = (Element)XPath.selectSingleNode(shipAddressNode,ADDRESS_NAME);

                // <PostalAddress>...
                Element shipPostalAddressNode = (Element)XPath.selectSingleNode(shipAddressNode,POSTALADDRESS);
                List deliverToList = XPath.selectNodes(shipPostalAddressNode,DELIVERTO);
                if (deliverToList != null && deliverToList.size() > 0) {
                    Element deliverTo = (Element) deliverToList.get(0);
                    ParsedNameVO shipParsedName = parseName(getTextValue(deliverTo));
                    detailLineVo.setShipToFirstName(shipParsedName.getFirstName());
                    detailLineVo.setShipToLastName(shipParsedName.getLastName());
                    if (deliverToList.size() > 1) {
                      deliverTo = (Element) deliverToList.get(1);
                      detailLineVo.setShipToLastName(shipParsedName.getLastName() + " " + getTextValue(deliverTo));
                    }
                }
                List streetList = XPath.selectNodes(shipPostalAddressNode,STREET);
                Iterator itl = streetList.listIterator();
                while (itl.hasNext()) {
                  Element streetElem = (Element)itl.next();
                  shipToStreetAddress = shipToStreetAddress + " "  + getTextValue(streetElem);
                }
                detailLineVo.setShipToAddressLine1(shipToStreetAddress.trim());
                detailLineVo.setShipToAddressLine2("");  //Set Ship To Address Line 2 to empty
                detailLineVo.setShipToCity(getTextValue((Element)XPath.selectSingleNode(shipPostalAddressNode,CITY))); 
                Element stateElem = (Element)XPath.selectSingleNode(shipPostalAddressNode,STATE); 
                if (stateElem == null) {
                  detailLineVo.setShipToState("NA");
                } else {
                  detailLineVo.setShipToState(getTextValue(stateElem));
                }
                detailLineVo.setShipToZipCode(getTextValue((Element)XPath.selectSingleNode(shipPostalAddressNode,POSTALCODE))); 
                Element countryElem = (Element)XPath.selectSingleNode(shipPostalAddressNode,COUNTRY); 
                detailLineVo.setShipToCountry(getAttributeValue(countryElem, ISOCOUNTRYCODE));
                // ...</PostalAddress>

                detailLineVo.setShipToEmail(getTextValue((Element)XPath.selectSingleNode(shipAddressNode,EMAIL))); 
                phoneVO = extractPhone(shipAddressNode);
                detailLineVo.setShipToWorkPhone(phoneVO.getWorkPhone());          
                detailLineVo.setShipToHomePhone(phoneVO.getHomePhone());
                detailLineVo.setShipToFaxNumber(phoneVO.getFaxPhone());
            }
          } // end if ship to is at line level

          detailLineVo.setAribaCostCenter("");
          Element distributionNode = (Element)XPath.selectSingleNode(itemOutNode,DISTRIBUTION);
          Element accountingNode = (Element)XPath.selectSingleNode(distributionNode,ACCOUNTING);

          // Get Cost Center and Project Code.  Note that 
          // Project Codes are currently used only by AMS.
          //
          detailLineVo.setAMSProjectCode("");  // Assume no project code
          List segmentList = XPath.selectNodes(accountingNode,SEGMENT);
          Iterator its = segmentList.listIterator();
          while (its.hasNext()) {
            Element segmentElem = (Element)its.next();
            if ((getAttributeValue(segmentElem, TYPE).equals(PROJECT_CODE))) {
              detailLineVo.setAMSProjectCode(getAttributeValue(segmentElem, ID));
            } else if ((getAttributeValue(segmentElem, TYPE).equals(COSTCENTER))) {
              String ccenter = getAttributeValue(segmentElem, ID);
              if (ccenter != null && (ccenter.length() > COST_CENTER_MAX_LEN)) {
                  ccenter = ccenter.substring(0,COST_CENTER_MAX_LEN);
              }
              detailLineVo.setAribaCostCenter(ccenter);
            }
          }

          // Handle any client-specific processing
          detailLineItemClientSpecifics();          

          // Include this line item in overall order
          orderVO.addLineItem(detailLineVo);        
        } // end loop for each line item

      } catch (JDOMException e) {
        throw new XMLParseException("JDOM exception: " + e);
      }
      return orderVO;
  }


  /**
   * Returns string value of an element. If one does not exist,
   * it returns an empty string.
   * 
   * @param Element Element to extract string value from
   *
   **/
  protected String getTextValue(Element a_element)
  {
      return a_element == null ? "" : a_element.getTextTrim();
  }

  /**
   * Extract an attribute value given an Element and Attribute name.
   * If one does not exist it returns an empty string.
   * 
   * @param Element Element containing attribute
   * @param String Attribute name
   *
   **/
  protected String getAttributeValue(Element a_element, String a_attrName)
  {
      return a_element == null ? "" : a_element.getAttributeValue(a_attrName, "");
  }
    
  /**
   * Builds a java.util.Date object based on a yyyy/mm/dd String
   *
   * @param yyyy/mm/dd String
   * @return java.util.Date
   * @author York Davis
   **/
  protected Date buildDate(String date) throws XMLParseException
  {
    try {
      Calendar cal = new GregorianCalendar();
      cal.set(Calendar.YEAR, Integer.parseInt(date.substring(0, 4)));
      cal.set(Calendar.MONTH, ((Integer.parseInt(date.substring(5, 7))-1)));
      cal.set(Calendar.DATE, Integer.parseInt(date.substring(8, 10)));
      return cal.getTime();
    } catch (Throwable t) {
      throw new XMLParseException("Incorrect date format, expected yyyy/mm/dd, but got: " + date);
    }
  }
  
  /**
   * Builds a java.util.Date object based on a mm/dd/yyyy String
   *
   * @param mm/dd/yyyy String
   * @return java.util.Date
   **/
  protected Date buildDateNormal(String date) throws XMLParseException
  {
    try {
      Calendar cal = new GregorianCalendar();
      cal.set(Calendar.MONTH, ((Integer.parseInt(date.substring(0, 2))-1)));
      cal.set(Calendar.DATE, Integer.parseInt(date.substring(3, 5)));
      cal.set(Calendar.YEAR, Integer.parseInt(date.substring(6, 10)));
      return cal.getTime();
    } catch (Throwable t) {
      throw new XMLParseException("Incorrect date format, expected mm/dd/yyyy, but got: " + date);
    }
  }

  /**
   * Extracts the home, work  and fax phone numbers.
   * Generally, all orders will have a 'work' phone at the Order-level.
   * However, 'home' phone may or may not exist. If it is not there,
   * they will simply be set to ten zeroes. 
   * This method is used for both 'shipto' and 'billto' phone number
   * processing.  
   *
   * @param Element  The BillTo or ShipTo element.
   * @return PhoneVO
   **/
  private PhoneVO extractPhone(Element a_addrElement) throws JDOMException
  {
      // <Phone name="...">
      //    <TelephoneNumber>
      //       <CountryCode ...>...</CountryCode>
      //       <AreaOrCityCode>...</AreaOrCityCode>
      //       <Number>...</Number>
      //    </TelephoneNumber>
      // </Phone>
      //
      PhoneVO vo = new PhoneVO();
      vo.setWorkPhone(DEFAULT_PHONE_NUMBER);
      vo.setHomePhone(DEFAULT_PHONE_NUMBER);
      vo.setFaxPhone("");
      if (a_addrElement == null) {
        return vo;
      }
      List phoneList = XPath.selectNodes(a_addrElement,PHONE);
      Iterator it = phoneList.listIterator();
      if (it == null) {
        return vo;
      }
      while (it.hasNext()) {
        Element phoneElem = (Element)it.next();
        String phoneType = getAttributeValue(phoneElem,NAME);
        if (WORK.equals(phoneType)) {
          vo.setWorkPhone(getPhoneInfo((Element)XPath.selectSingleNode(phoneElem,TELEPHONENUMBER)));
          continue;
        }
        if (HOME.equals(phoneType)) {
          vo.setHomePhone(getPhoneInfo((Element)XPath.selectSingleNode(phoneElem,TELEPHONENUMBER)));
          continue;
        }
      }
      Element faxElem = ((Element)XPath.selectSingleNode(a_addrElement,FAX));
      if (faxElem != null) {
        vo.setFaxPhone(getPhoneInfo(faxElem));
      }
      return vo;
  }

  /**
   * Extracts the phone number into a common format from a given element.
   *
   * @param The Element object containing the phone info.
   * @return area code and telephone number string.
   **/
  private String getPhoneInfo(Element a_phoneNumElem) throws JDOMException
  {
      String areaCode = getTextValue((Element)XPath.selectSingleNode(a_phoneNumElem,AREAORCITYCODE));
      String number   = getTextValue((Element)XPath.selectSingleNode(a_phoneNumElem,NUMBER));
      return areaCode + number;
  }

  /**
   * Extracts the PCard Information into a common format from a given element.
   *
   * @param The Element object containing the PCard Information.
   * @return PurchaseCreditCard VO containing information.
   **/
  private PurchaseCreditCardVO extractPCard(Element a_paymentElem) throws XMLParseException
  {
      PurchaseCreditCardVO voPCard = new PurchaseCreditCardVO();
      voPCard.setCreditCardNumber(getAttributeValue(a_paymentElem, PCARDNUMBER));
      String PCardExpireDate = getAttributeValue(a_paymentElem, PCARDEXPIRE);
      voPCard.setExpirationDate(buildApprovedDate(PCardExpireDate));
      voPCard.setCreditCardName(getAttributeValue(a_paymentElem, PCARDNAME));

      return voPCard;
  }

  /**
   * Builds an Approved Date object from an input date string.
   *
   * @param String containing date.
   * @return Approved java.util.Date object
   * @author York Davis
   **/
  private Date buildApprovedDate(String date) throws XMLParseException
  {
        try{
            /*Date comes in the following format:YYYY-MM-DDTHH:MM:SS+HH:MM
              Example, October 31, 2002 at 9:13 would be:2002-10-31T09:13:00+??:??*/
            StringTokenizer tokenizer = new StringTokenizer(date, CONSTANT_APPROVEDPO_DATE_TOKENS);
            String theDate = tokenizer.nextToken();
            String theTime = "";
            try {
                theTime = tokenizer.nextToken();
            }    //  if we can't obtain the time, set the time to midnight
            catch (Throwable throwable)
            {
                 theTime = "00:00:00";
            }
            SimpleDateFormat sdf = new SimpleDateFormat(CONSTANT_APPROVEDPO_DATE_FORMAT);
            return sdf.parse(theDate + " " + theTime);
        }
        catch (Throwable throwable)
        {
            throw new XMLParseException("Error parsing Approved Date.");
        }  
  }

  /**
   * Remove special characters from a name and parse out the
   * resulting string into firstname, lastname components.
   * If only one name (like 'Cher' or 'Madonna') can be parsed out,
   * it will be palced in the last name and first name will be left blank.
   *
   * @param Name String
   * @return ParsedNameVO object containing first and last name elements.
   * @author York Davis
   **/
  protected ParsedNameVO parseName(String s)
  {
      StringBuffer sb = new StringBuffer(s.toUpperCase());
      sb = removeSpecial(sb, special);
      sb = removePrefix(sb, prefixes);
      sb = removeSuffix(sb, suffixes);
      return getParsedName(sb);
  }

  /**
   * Parse a name string into first and last name components.
   *
   * @param Name string with special characters removed.
   * @return ParsedNameVO object.
   * @author York Davis
   **/
  private ParsedNameVO getParsedName(StringBuffer sb)
  {
    	String s = sb.toString().trim();
    	int i = s.lastIndexOf(' ');
	
    	ParsedNameVO pn = new ParsedNameVO();
     	try
      {
    		pn.setLastName(s.substring(i + 1));
    		pn.setFirstName(s.substring(0, i));
    	}
    	catch (Throwable throwable)
    	{
    		pn.setLastName(sb.toString());
    		pn.setFirstName("");
    	}
    	return pn;	
  }

  /**
   * Removes a String array of prefixes from a StringBuffer.
   *
   * @param StringBuffer containing name.
   * @param String array of token values to be removed.
   * @return StringBuffer of name with tokens removed.
   * @author York Davis
   **/
  private StringBuffer removePrefix(StringBuffer sb, String[] tokens)
  {
      // First remove, for example all " DR ".
      StringBuffer sbx = remove(sb, tokens);

      // Check to see if the suffix is at the beginning. For example
      // "DR " at the beginning instead of " DR ".
      int loc = 0;
      int tokenCount = tokens.length;

      for (int i=0; i<tokenCount; i++)
      {
        loc = sbx.toString().indexOf(tokens[i].substring(1));
      	if (loc == 0)
        {
          sbx.replace(loc, loc+(tokens[i].length()-1), " ");
        }
      }
      return sb;
  }

  /**
   * Removes a String array of suffixes from a StringBuffer.
   *
   * @param StringBuffer containing name.
   * @param String array of token values to be removed.
   * @return StringBuffer of name with tokens removed.
   * @author York Davis
   **/
  private StringBuffer removeSuffix(StringBuffer sb, String[] tokens)
  {
      // First remove, for example all " JR ".
      StringBuffer sbx = remove(sb, tokens);

      // Check to see if the suffix is at the end. For example
      // " JR" at the end instead of " JR ".
      int tokenCount = tokens.length;
      int len = 0;
      int lenx = 0;

      for (int i=0; i<tokenCount; i++)
      {
      	len = tokens[i].length();
        if (sbx.toString().endsWith(tokens[i].substring(0, len-1)))
        {
      	  lenx = sbx.length() + 1;
      	  sbx = new StringBuffer(sbx.substring(0, lenx-len));
        }
      }
      return sbx;
  }

  /**
   * Removes special characters from a StringBuffer
   *
   * @param StringBuffer containing name.
   * @param String array of token values to be removed.
   * @return StringBuffer of name with tokens removed.
   * @author York Davis
   **/
  private StringBuffer removeSpecial(StringBuffer sb, String[] tokens)
  {
    	return remove(sb, tokens);
  }

  /**
   * Removes a String array of tokens from a StringBuffer.
   *
   * @param StringBuffer containing name.
   * @param String array of token values to be removed.
   * @return StringBuffer of name with tokens removed.
   * @author York Davis
   **/
  private StringBuffer remove(StringBuffer sb, String[] tokens)
  {
      int loc = 0;
      int tokenCount = tokens.length;

      for (int i=0; i<tokenCount; i++)
      {
      	while (loc > -1)
      	{
	        loc = sb.toString().indexOf(tokens[i]);
      		if (loc > -1)
	        {
	          sb.replace(loc, loc+(tokens[i].length()), " ");
	        }
      	}
      	loc = 0;
      }
      return sb;
  }
}

