package com.ftd.b2b.ariba.businessobjects.receiveapprovedpo;

import com.ftd.b2b.ariba.common.valueobjects.ApprovedOrderVO;
import com.ftd.b2b.ariba.common.valueobjects.OrderRequestLineItemVO;
import com.ftd.b2b.ariba.common.valueobjects.LineItemVO;
import com.ftd.b2b.ariba.common.valueobjects.IOrderVO;
import com.ftd.b2b.ariba.common.valueobjects.ApprovedLineItemVO;
import com.ftd.b2b.ariba.common.valueobjects.OrderRequestOrderVO;
import com.ftd.b2b.ariba.common.valueobjects.OrderDeleteTransmissionVO;
import com.ftd.b2b.ariba.integration.dao.IOrderDeleteTransmissionDAO;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import com.ftd.b2b.constants.B2BConstants;
import com.ftd.b2b.ariba.integration.dao.IPendingDAO;
import com.ftd.b2b.ariba.integration.dao.IPunchoutOrderNumberDAO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.framework.common.exceptions.FTDApplicationException;
import com.ftd.framework.common.exceptions.ResourceNotFoundException;
import com.ftd.b2b.utils.mail.B2BMailer;
import com.ftd.b2b.ariba.common.utils.MapGenerator;
/**
 * Processes a PunchOut PO
 * @author Ed Mueller
 */

 /* 6/25/03 Ed Mueller
     Add new fields to copy from existing order...see below. */
     
public class PunchOutOrderProcessorImpl extends OrderProcessorImpl
        implements  B2BConstants,IPunchOutOrderProcessor
{

       
    /**
     * Constructor
     */
    public PunchOutOrderProcessorImpl()
    {
        super(LOG_CATEGORY_PUNCHOUT_ORDER_PROCESSOR );

        //setup logging
        lm = new Logger(LOG_CATEGORY_RECEIVE_APPROVED_PO);
        if ( lm.isDebugEnabled() )  {
            debugEnabled = true;
        }   

    }//end Constructor


    /**
     *  Process order.
     *
     *  @params OrderRequestVO request order
     *  @IOrderVO The existing order as found on the archive or pending table
     */
     public boolean processOrder(OrderRequestOrderVO voOrderRequest, IOrderVO voExistingOrder)  
                    throws FTDApplicationException{


        if (debugEnabled){
            lm.debug("Processing punchout APO request");
            }

        //build the order
        ApprovedOrderVO voApprovedOrder = buildOrder(voOrderRequest, voExistingOrder);

        //Save order to Approved and OrderEntry tables
        lm.debug("Save order to approved tables");
        persistOrder(voApprovedOrder);

        //Generate CXML response to Ariba
        //String response = new ApprovedPOResponseGenerator().generateResponse(voApprovedOrder);
        
        //log Novator transmission to database
        logTransmissions(voApprovedOrder);

        //remove pending data
         lm.debug("Removing order from pending tables");
        removeOrder(voExistingOrder);

        // If we made it to here then all went well
        return true;
     }//end, process order



    /**
     * Builds an OrderVO:
     *  -Break apart line with quantity > 1
     *  -Null out certain fields
     *
     * @params OrderRequestVO VO before processing
     * @params IOrderVO order object that exists on the existing or pending table
     * @returns ApprovedOrderVO 
     */
    private ApprovedOrderVO buildOrder(OrderRequestOrderVO voOrderRequest, IOrderVO voExistingOrder) 
                    throws FTDApplicationException {


        //confirm that the number of lines in the existing vo and the
        //request vo are the same.
        // !! REMOVED THIS CHECK....DUE TO ADDONS LINE COUNTS MAY NOT BE EQUAL
        /*if (voOrderRequest.getLineItems().size() != voExistingOrder.getLineItems().size()){
            Logger logManager = new Logger(LOG_CATEGORY_ORDER_PROCESSOR);
            logManager.error("" + ERROR_CODE_REQUESTLINESNOTEQUALEXISTING + new String[] {voOrderRequest.getBuyerCookie()});
            String msg = "(Buyer cookie " + voOrderRequest.getBuyerCookie() + "):Number of line items in order request not the same as pending table.";
            B2BMailer.getInstance().send(this,"updateAddonFields()",msg,B2BMailer.ACTION2);
            throw new FTDApplicationException(ERROR_CODE_REQUESTLINESNOTEQUALEXISTING);        
        }*/
        if (voOrderRequest.getLineItems().size() != voExistingOrder.getLineItems().size()){
          lm.debug("buildOrder: Number of line items in approval is not what expected - this could just be due to addons, " +
                  "or the dreaded split APO");
        }

        //VO to be inserted into the Approved table
        ApprovedOrderVO voApprovedOrder= new ApprovedOrderVO();        

        //populate the approved vo ORDER info based on EXISTING data
        voApprovedOrder.setCreatedDate(voExistingOrder.getCreatedDate());

        //populate the approved vo ORDER info based on REQUEST data
        populateHeader(voApprovedOrder, voOrderRequest);

        //Add all line items from existing order into approved order
        voApprovedOrder = addLineItems(voApprovedOrder, voExistingOrder);
        Map approvedLineMap = MapGenerator.getLineItemMapKeyedByOrderNumber(voApprovedOrder);
    
        //Update each line in approved vo with data from request vo
        List requestLines = voOrderRequest.getLineItems();
        int numberOfLines = requestLines.size();
        for(int i=0; i < numberOfLines; i++){
        
            //get line item
            OrderRequestLineItemVO voRequestLine = 
                                    (OrderRequestLineItemVO)requestLines.get(i);          
            String ftdOrderNumber = voRequestLine.getOrderNumber();
            String supplierPartID = voRequestLine.getPartId();

            //If this line is an addon
            if (supplierPartID.startsWith(CONSTANT_ADDONFLAG)){
                updateAddonFields(voApprovedOrder,voRequestLine);
                }
            //If this line is a service fee line item
            else if (supplierPartID.startsWith(CONSTANT_SERVICE_FEE_LINE_ITEM_NUM)) {
                // Don't need to do anything since service fee already in Pending tables
                }
            //If this line is a tax line item
            else if (supplierPartID.startsWith(CONSTANT_TAX_LINE_ITEM_NUM)) {
                // Don't need to do anything since tax already in Pending tables
                }
            else{
                //Find corresponding line in approved vo
                ApprovedLineItemVO voApprovedLine = (ApprovedLineItemVO)approvedLineMap.get(ftdOrderNumber);
                if (voApprovedLine == null){
                    if (numberOfLines == 1 && (voOrderRequest.getLineItems().size() == voExistingOrder.getLineItems().size())) {
                        // No match but only one line item so we can just use what we have.  This is to help cover
                        // the erroneous cases where we had same cookie in pending twice but different order numbers.
                        // If there had been more than one line item it would be harder to match the orders, so we'll
                        // let that throw the exception in the else clause instead.
                        voApprovedLine = (ApprovedLineItemVO)voApprovedOrder.getLineItems().get(0);
                        lm.error("We (most likely) have order where cookie in pending twice but different order numbers. Order approved: " 
                                 + ftdOrderNumber + " Most recent cart for same cookie (so using it): " + voApprovedLine.getOrderNumber());
                    } else {
                        String[] msg = new String[] {voRequestLine.getAribaBuyerCookie(),ftdOrderNumber};
                        lm.error("buildOrder order line not found: " + voRequestLine.getAribaBuyerCookie() + " " + ftdOrderNumber);
                        B2BMailer.getInstance().send(this,"updateAddonFields()",ERROR_CODE_ORDERLINENOTFOUND,msg,B2BMailer.ACTION2);
                        throw new FTDApplicationException(ERROR_CODE_ORDERLINENOTFOUND);
                    }
                }

                //Populate approved vo with data from requst po
                moveRequestDataToApproveLine(voApprovedLine,voRequestLine,voOrderRequest);

                //if CXML contains a date place it in line, otherwise
                //leave the date from the existing order in there
                if (voRequestLine.getDeliveryDate() != null){
                    voApprovedLine.setDeliveryDate(voRequestLine.getDeliveryDate());
                    }
                else{
                    //make sure exsiting order does not have null date
                    if (voApprovedLine.getDeliveryDate() == null){
                        voApprovedLine.setDeliveryDate(new Date()); 
                        }
                }

                /* It has been determined that New POs will not have
                   a line item quantity greater then one, so the lines
                   will not have to be split.  The following line of code
                   and corresponding stored procedure was left here in case
                   that business rule changes.  The actual function exists
                   in the CatalogProcessor object because that is the only 
                   processing using that method now. */
                //split lines if neccessary
                //splitLines(voApprovedOrder,voApprovedLine,voRequestLine);   
                } 
                
        } // end for loop through each line 




        return voApprovedOrder;
        
    }//end buildOrder







/**
 * Add all the line items from the existing order VO to the approved VO, 
 * only the required field values will be copied to the apporved VO.
 *
 * @params ApprovedOrderVO The approved order to update
 * @params IOrderVO the existing order from the pending or archive table
 * @returns The update approved order object
 */
  private ApprovedOrderVO addLineItems(ApprovedOrderVO voApprovedOrder, 
                                       IOrderVO voExistingOrder){

    voApprovedOrder.setLineItems(new ArrayList());
    List existingLineItems = voExistingOrder.getLineItems();
    
    //for each line in the existing order
    int numberOfLines = existingLineItems.size();
    for (int i=0; i < numberOfLines; i++){

        LineItemVO existingLine = (LineItemVO)existingLineItems.get(i);
        ApprovedLineItemVO newLine = new ApprovedLineItemVO();

        //populate fields which are not being sent in the PO request
        newLine.setOccasion(existingLine.getOccasion());
        newLine.setServiceFee(existingLine.getServiceFee());
        newLine.setDeliveryDate(existingLine.getDeliveryDate());
        newLine.setMasterOrderNumber(existingLine.getMasterOrderNumber());
        newLine.setOrderNumber(existingLine.getOrderNumber());
        newLine.setExtraShippingFee(existingLine.getExtraShippingFee());
        newLine.setDropShipCharges(existingLine.getDropShipCharges());

        /*  Ed 6/25/03
        These field are now obtained from the existing order when they exist there. */        
        newLine.setShipToFirstName(existingLine.getShipToFirstName());
        newLine.setShipToLastName(existingLine.getShipToLastName());
        newLine.setShipToAddressLine1(existingLine.getShipToAddressLine1());
        newLine.setShipToAddressLine2(existingLine.getShipToAddressLine2());
        newLine.setShipToCity(existingLine.getShipToCity());
        newLine.setShipToState(existingLine.getShipToState());
        newLine.setShipToZipCode(existingLine.getShipToZipCode());
        newLine.setShipToCountry(existingLine.getShipToCountry());
        newLine.setShipToWorkPhone(existingLine.getShipToWorkPhone());
        newLine.setShipToWorkExtension(existingLine.getShipToWorkExtension());
        newLine.setShipToBusinessType(existingLine.getShipToBusinessType());
        newLine.setShipToBusinessName(existingLine.getShipToBusinessName());
        newLine.setCardMessage(existingLine.getCardMessage());
        newLine.setSpecialInstructions(existingLine.getSpecialInstructions());
        //end, 6/25/03

        /* Ed 8/4/03 Retail price has been added to the novator transmission. */
        newLine.setRetailPrice(existingLine.getRetailPrice());
        newLine.setItemPrice(existingLine.getItemPrice());
        /* 8/4/03....end*/

        // BillTo added for Spec #2255
        newLine.setBillToFirstName(existingLine.getBillToFirstName());
        newLine.setBillToLastName(existingLine.getBillToLastName());
        newLine.setBillToAddressLine1(existingLine.getBillToAddressLine1());
        newLine.setBillToAddressLine2(existingLine.getBillToAddressLine2());
        newLine.setBillToCity(existingLine.getBillToCity());
        newLine.setBillToState(existingLine.getBillToState());
        newLine.setBillToZipCode(existingLine.getBillToZipCode());
        newLine.setBillToCountry(existingLine.getBillToCountry());
        newLine.setBillToWorkPhone(existingLine.getBillToWorkPhone());
        newLine.setBillToWorkExtension(existingLine.getBillToWorkPhoneExt());
        newLine.setBillToEmail(existingLine.getBillToEmail());

        voApprovedOrder.addLineItem(newLine);
            
        }

    return voApprovedOrder;
    
    }




    /**
      * Logs a Novator tranmission to the database.
      *
      * @params ApprovedOrderVO The approved order to transmit
      */
    private void logTransmissions(ApprovedOrderVO voApprovedOrder)
            throws FTDApplicationException{

        List lineItems = voApprovedOrder.getLineItems();
       
        //get the latest delivery date
        Date latestDate = ((LineItemVO)lineItems.get(0)).getDeliveryDate();
        Date deliveryDate = null;
        int numberOfLines = lineItems.size();
        for(int i=1;i<numberOfLines;i++){
            LineItemVO line = (LineItemVO)lineItems.get(i);
            deliveryDate = ((LineItemVO)lineItems.get(i)).getDeliveryDate();
            if (latestDate.before(deliveryDate)){
                latestDate = deliveryDate;
                }
        }//end for    

        OrderDeleteTransmissionVO voTran = new OrderDeleteTransmissionVO();
        voTran.setBuyerCookie(voApprovedOrder.getBuyerCookie());
        voTran.setSendDate(latestDate);

        try{            

            IOrderDeleteTransmissionDAO daoTransmission = 
                        (IOrderDeleteTransmissionDAO) rm.getImplementation(INTERFACE_ORDERDELETETRANSMISSIONDAO); 
            daoTransmission.insert(voTran);
            }
        catch(ResourceNotFoundException e){
            String[] msg = new String[] {e.toString()};
            lm.error("logTransmissions: " + e.toString());
            B2BMailer.getInstance().send(this,"logTransmissions()",ERROR_CODE_UNABLETOLOADSERVICE,msg,B2BMailer.ACTION2);
            throw new FTDApplicationException(ERROR_CODE_UNABLETOLOADSERVICE, e);            
            }
        catch(Exception e){
            String[] msg = new String[] {e.toString()};
            lm.error("logTransmissions: " + e.toString());
            B2BMailer.getInstance().send(this,"logTransmissions()",ERROR_CODE_UNRECOVERABLE_EXCEPTION,msg,B2BMailer.ACTION2);
            throw new FTDApplicationException(ERROR_CODE_UNRECOVERABLE_EXCEPTION, e);
            }
            
      }//end transmit()
      
    /**
     * Remove the existing order from pending table
     *
     * @params IOrderVO the order to delete from the database
     */
     private void removeOrder(IOrderVO voOrder) throws FTDApplicationException{

        try{    

            IPendingDAO daoPending = 
                (IPendingDAO) rm.getImplementation(INTERFACE_PENDINGDAO);
            voOrder.setOriginalBuyerCookie(voOrder.getBuyerCookie());    
            daoPending.delete(voOrder);

            }//end try
        catch(ResourceNotFoundException e){
            String[] msg = new String[] {e.toString()};
            lm.error("removeOrder: " + e.toString());
            B2BMailer.getInstance().send(this,"removeOrder()",ERROR_CODE_UNABLETOLOADSERVICE,msg,B2BMailer.ACTION2);
            throw new FTDApplicationException(ERROR_CODE_UNABLETOLOADSERVICE, e);            
            }
        catch(Exception e){
            String[] msg = new String[] {e.toString()};
            lm.error("removeOrder: " + e.toString());
            B2BMailer.getInstance().send(this,"removeOrder()",ERROR_CODE_UNRECOVERABLE_EXCEPTION,msg,B2BMailer.ACTION2);
            throw new FTDApplicationException(ERROR_CODE_UNRECOVERABLE_EXCEPTION, e);
            }            
        }//end removeData()


/**
 * Get the next order number from the database.
 *
 * @returns String order number to use for next order
 */
protected String getNextPunchOutOrderNumber() throws FTDApplicationException{
        IPunchoutOrderNumberDAO daoOrderNumber = null;
        String newOrderNumber = null;
        try{            

            //first check pending table
            daoOrderNumber = 
                (IPunchoutOrderNumberDAO) rm.getImplementation(INTERFACE_PUNCHOUTORDERNUMBERDAO); 
            newOrderNumber =  daoOrderNumber.retrievePunchoutNumber();
            }
        catch(ResourceNotFoundException e){
            String[] msg = new String[] {e.toString()};
            lm.error("getNextPunchOutOrderNumber: " + e.toString());
            B2BMailer.getInstance().send(this,"getNextOrderNumber()",ERROR_CODE_UNABLETOLOADSERVICE,msg,B2BMailer.ACTION2);
            throw new FTDApplicationException(ERROR_CODE_UNRECOVERABLE_EXCEPTION, e);
            }
        catch(Exception e){
            String[] msg = new String[] {e.toString()};
            lm.error("getNextPunchOutOrderNumber: " + e.toString());
            B2BMailer.getInstance().send(this,"getNextOrderNumber()",ERROR_CODE_UNRECOVERABLE_EXCEPTION,msg,B2BMailer.ACTION2);
            throw new FTDApplicationException(ERROR_CODE_UNRECOVERABLE_EXCEPTION, e);
            }

        return newOrderNumber;
}
   

    /**
     * Not Used
     */
     public void create(){ }

    /**
     * Not Used
     */
     public void save(){ }

    /**
    * Determins if objects are the same.  
    * Note, a deep compare is not done.
    * @parms object to cmpare to
    * @returns True of objects equal, else False
    */
    public boolean equals(Object obj){
     return (obj == this); }

    /**
    * Returns the class name.
    * @returns Class Name
    */
    public String toString(){
        return this.getClass().toString(); }

}