package com.ftd.b2b.ariba.businessobjects.receiveapprovedpo;


import com.ftd.framework.common.exceptions.*;

import com.ftd.b2b.ariba.common.valueobjects.ApprovedOrderVO;


public interface IReceivedPOToFTD 
{

  public void send(ApprovedOrderVO voApprovedOrder) throws FTDApplicationException;

}