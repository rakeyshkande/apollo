package com.ftd.b2b.ariba.businessobjects.receiveapprovedpo.variations;

import com.ftd.b2b.ariba.businessobjects.receiveapprovedpo.ApoXmlHelper;

import com.ftd.b2b.ariba.common.valueobjects.AribaRequestVO;
import com.ftd.b2b.ariba.common.valueobjects.OrderRequestLineItemVO;
import com.ftd.b2b.ariba.common.valueobjects.OrderRequestOrderVO;
import com.ftd.b2b.ariba.common.valueobjects.ParsedNameVO;
import com.ftd.b2b.ariba.common.valueobjects.PhoneVO;
import com.ftd.b2b.ariba.common.valueobjects.PurchaseCreditCardVO;
import com.ftd.b2b.common.utils.XMLParseException;
import com.ftd.b2b.constants.B2BConstants;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.ByteArrayInputStream;
import java.io.StringReader;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Iterator;
import java.util.StringTokenizer;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.xpath.*;

/**
 * Top level utility class to convert an XML document into an Order Value Object for
 * Charles Schwab.  This class should contain only Charles Schwab specific logic - the bulk 
 * of the logic is common to all clients and resides in the superclass.
 * 
 * @see ApoXmlHelper
 * @see ReceiveApprovedPOImpl#processApprovedPO
 */
public class ApoXmlCSchwab extends ApoXmlHelper {

  private static final String SCHWAB_INVOICE_NAME      = "reqName";
  private static final String SCHWAB_INVOICE_PO_NUM    = "poNum";
  private static final String SCHWAB_INVOICE_COST_CNTR = "costCntr";
  private static final String DEPARTMENT = "Department";


  /**
   * Special line item detail processing for Charles Schwab.  The order ID, billTo name,
   * and cost center number are to be treated as line item extensions (so they will show
   * up in the monthly revenue reports from the HP).
   */
  protected void detailLineItemClientSpecifics() throws XMLParseException, JDOMException {

    HashMap lie = new HashMap();

    lie.put(SCHWAB_INVOICE_PO_NUM, detailLineVo.getAribaPONumber());
    String iname = detailLineVo.getBillToFirstName() + " " + detailLineVo.getBillToLastName();
    lie.put(SCHWAB_INVOICE_NAME, iname.trim());

    // Cost center resides within line item distribution/accounting/segment
    //
    Element distributionNode = (Element)XPath.selectSingleNode(itemOutNode,DISTRIBUTION);
    Element accountingNode = (Element)XPath.selectSingleNode(distributionNode,ACCOUNTING);
    List segmentList = XPath.selectNodes(accountingNode,SEGMENT);
    Iterator its = segmentList.listIterator();
    while (its.hasNext()) {
      Element segmentElem = (Element)its.next();
      if ((getAttributeValue(segmentElem, TYPE).equals(DEPARTMENT))) {
        String idStr = getAttributeValue(segmentElem, ID);
        // Cost center is set of chars before first semi-colon
        int i = idStr.indexOf(":");
        if (i > 0) {
          lie.put(SCHWAB_INVOICE_COST_CNTR, idStr.substring(0, i));
        }
      }
    }
    detailLineVo.setLineItemExtensions(lie);
  }
  
}
