package com.ftd.b2b.ariba.businessobjects.receiveapprovedpo;


import com.ftd.framework.common.exceptions.*;

import com.ftd.b2b.ariba.common.valueobjects.ApprovedOrderVO;
import com.ftd.b2b.ariba.common.valueobjects.OrderRequestOrderVO;

import java.sql.*;
import java.io.*;

public interface IChangeCancelProcessor 
{
  /**
   *  Process Ariba Purchase Order Cancel request.
   *  Send a socket transmission for each line item in the canceled order.
   * @parm Order Request value object
   * @return 
   * @throws 
   *
   * @author Kristyn Angelo
   */
  public void processCancel(OrderRequestOrderVO voOrderRequest) throws FTDApplicationException;

  /**
   *  Process Ariba Purchase Order Change request.
   *  Send a socket transmission for each change that is found on each line item.
   * @parm Order Request value object
   * @return 
   * @throws 
   *
   * @author Kristyn Angelo
   */
  public void processChange(OrderRequestOrderVO voOrderRequest, int customerType) throws FTDApplicationException;

}