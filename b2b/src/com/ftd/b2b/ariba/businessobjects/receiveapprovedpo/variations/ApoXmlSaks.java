package com.ftd.b2b.ariba.businessobjects.receiveapprovedpo.variations;

import com.ftd.b2b.ariba.businessobjects.receiveapprovedpo.ApoXmlHelper;

import com.ftd.b2b.ariba.common.valueobjects.AribaRequestVO;
import com.ftd.b2b.ariba.common.valueobjects.OrderRequestLineItemVO;
import com.ftd.b2b.ariba.common.valueobjects.OrderRequestOrderVO;
import com.ftd.b2b.ariba.common.valueobjects.ParsedNameVO;
import com.ftd.b2b.ariba.common.valueobjects.PhoneVO;
import com.ftd.b2b.ariba.common.valueobjects.PurchaseCreditCardVO;
import com.ftd.b2b.common.utils.XMLParseException;
import com.ftd.b2b.constants.B2BConstants;

import java.io.ByteArrayInputStream;
import java.io.StringReader;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Iterator;
import java.util.StringTokenizer;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.xpath.*;

/**
 * Top level utility class to convert an XML document into an Order Value Object for
 * Saks.  This class should contain only Saks specific logic - the bulk 
 * of the logic is common to all clients and resides in the superclass.
 * 
 * @see ApoXmlHelper
 * @see ReceiveApprovedPOImpl#processApprovedPO
 */
public class ApoXmlSaks extends ApoXmlHelper {

  private static final String SAKS_XML_EXP_CNTR = "Expense Center";
  private static final String SAKS_XML_GL_ACCT  = "GL Account";
  private static final String SAKS_XML_LOCATION = "Location";
  private static final String SAKS_XML_DESC = "ID";
  private static final String SAKS_XML_DESC_TAG = "description";
  private static final String SAKS_INVOICE_EXP_CNTR = "expCntr";
  private static final String SAKS_INVOICE_GL_ACCT  = "acct";
  private static final String SAKS_INVOICE_LOCATION = "loc";

  /** 
   * We override superclass method here since for Saks, ShipTo should
   * always be used as BillTo.
   * 
   * @see ApoXmlHelper#buildBaseLineItemVO
   */
  protected void baseLineBillToCopy() throws JDOMException {
      // For Saks, ShipTo should be used as BillTo
      //
      if (baseLineShipToNode != null) {
        baseLineVo.setBillToFirstName(baseLineVo.getShipToFirstName());
        baseLineVo.setBillToLastName(baseLineVo.getShipToLastName());
        baseLineVo.setBillToAddressLine1(baseLineVo.getShipToAddressLine1());
        baseLineVo.setBillToAddressLine2(baseLineVo.getShipToAddressLine2());
        baseLineVo.setBillToCity(baseLineVo.getShipToCity());
        baseLineVo.setBillToState(baseLineVo.getShipToState());
        baseLineVo.setBillToZipCode(baseLineVo.getShipToZipCode());
        baseLineVo.setBillToCountry(baseLineVo.getShipToCountry());
        baseLineVo.setBillToWorkPhone(baseLineVo.getShipToWorkPhone());
        baseLineVo.setBillToHomePhone(baseLineVo.getShipToHomePhone());
        baseLineVo.setBillToFaxNumber(baseLineVo.getShipToFaxNumber());
        baseLineVo.setBillToEmail(baseLineVo.getShipToEmail());
        //baseLineVo.setBillToEmail("");  // Temporary change to always clear email address
      }
  }

  /**
   * Special line item detail processing for Saks.  The Expense Center, GL Account,
   * and Location are to be treated as line item extensions (so they will show
   * up in the monthly revenue reports from the HP).
   */
  protected void detailLineItemClientSpecifics() throws XMLParseException, JDOMException {

    HashMap lie = new HashMap();

    // Values reside within line item distribution/accounting/segment's
    //
    Element distributionNode = (Element)XPath.selectSingleNode(itemOutNode,DISTRIBUTION);
    Element accountingNode = (Element)XPath.selectSingleNode(distributionNode,ACCOUNTING);
    List segmentList = XPath.selectNodes(accountingNode,SEGMENT);
    Iterator its = segmentList.listIterator();
    while (its.hasNext()) {
      Element segmentElem = (Element)its.next();
      String descStr = getAttributeValue(segmentElem, SAKS_XML_DESC_TAG);
      if (SAKS_XML_DESC.equals(descStr)) {
        String typeStr = getAttributeValue(segmentElem, TYPE);
        String idStr = getAttributeValue(segmentElem, ID);
        if ((SAKS_XML_EXP_CNTR.equalsIgnoreCase(typeStr)) && (idStr != null)) {
          lie.put(SAKS_INVOICE_EXP_CNTR, idStr);
        } else if ((SAKS_XML_GL_ACCT.equalsIgnoreCase(typeStr)) && (idStr != null)) {
          lie.put(SAKS_INVOICE_GL_ACCT, idStr);
        } else if ((SAKS_XML_LOCATION.equalsIgnoreCase(typeStr)) && (idStr != null)) {
          lie.put(SAKS_INVOICE_LOCATION, idStr);
        }
      }
    }
    detailLineVo.setLineItemExtensions(lie);
  }
  
}
