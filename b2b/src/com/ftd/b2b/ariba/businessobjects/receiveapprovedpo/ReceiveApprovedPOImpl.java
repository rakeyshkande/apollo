package com.ftd.b2b.ariba.businessobjects.receiveapprovedpo;

import com.ftd.b2b.ariba.businessobjects.receiveapprovedpo.variations.*;
import com.ftd.b2b.ariba.common.utils.ApprovedPOResponseGenerator;
import com.ftd.b2b.ariba.common.utils.IRequestAuthenticator;
import com.ftd.b2b.ariba.common.valueobjects.AribaRequestVO;
import com.ftd.b2b.ariba.common.valueobjects.ILineItemVO;
import com.ftd.b2b.ariba.common.valueobjects.IOrderVO;
import com.ftd.b2b.ariba.common.valueobjects.LineItemVO;
import com.ftd.b2b.ariba.common.valueobjects.OrderRequestOrderVO;
import com.ftd.b2b.ariba.common.valueobjects.URLLookupVO;
import com.ftd.b2b.ariba.integration.dao.IPendingDAO;
import com.ftd.b2b.common.utils.AuthenticationException;
import com.ftd.b2b.common.utils.InvalidClientException;
import com.ftd.b2b.common.utils.InvalidDUNSException;
import com.ftd.b2b.common.utils.DuplicateApprovalException;
import com.ftd.b2b.common.utils.XMLParseException;
import com.ftd.b2b.constants.B2BConstants;
import com.ftd.b2b.utils.mail.B2BMailer;
import com.ftd.framework.common.exceptions.FTDApplicationException;
import com.ftd.framework.common.exceptions.ResourceNotFoundException;
import com.ftd.framework.common.utilities.ResourceManager;
import com.ftd.osp.utilities.plugins.Logger;

import java.util.List;
import java.util.HashMap;

import javax.naming.InitialContext;
import javax.transaction.UserTransaction;

/**
 * This class contains the processing logic for receiving
 * and responding to an approved order request.
 * @author Ed Mueller
 */
public class ReceiveApprovedPOImpl implements IReceiveApprovedPO, B2BConstants 
{

    private boolean debugEnabled = false;
    private Logger lm = null;
    private ResourceManager rm = null;
    private static String asnTarget  = null;
    private static String asnCSchwab = null;
    private static String asnMLynch  = null;
    private static String asnSaks    = null;
    private static String asnNsc     = null;
    private static String asnEds     = null;

    
  /**
   * Method used by J2EE container called on bean creation.
   * and building the response document.
   *
   * @author York Davis
   **/
  public ReceiveApprovedPOImpl() {
    //setup logging
    lm = new Logger(LOG_CATEGORY_RECEIVE_APPROVED_PO);
    if ( lm.isDebugEnabled() )  {
        debugEnabled = true;
    }   
    rm = ResourceManager.getInstance();  
    if (asnTarget == null) {  // Only need to check one since all will be set
       asnTarget  = rm.getProperty(PROPERTY_B2B_PROPERTY_FILE, PROPERTY_ASN_TARGET);   
       asnCSchwab = rm.getProperty(PROPERTY_B2B_PROPERTY_FILE, PROPERTY_ASN_CSCHWAB);
       asnMLynch  = rm.getProperty(PROPERTY_B2B_PROPERTY_FILE, PROPERTY_ASN_MLYNCH);   
       asnSaks    = rm.getProperty(PROPERTY_B2B_PROPERTY_FILE, PROPERTY_ASN_SAKS);   
       asnNsc     = rm.getProperty(PROPERTY_B2B_PROPERTY_FILE, PROPERTY_ASN_NSC);   
       asnEds     = rm.getProperty(PROPERTY_B2B_PROPERTY_FILE, PROPERTY_ASN_EDS);   
    }
  }

    /**
     * Controls the flow of processing an Approved PO
     *
     * @params String Cxml message to process
     * @params AribeRequestVO aribe request
     * @returns String Error response, if empty no error response sent to Ariba
    */    
    public String processApprovedPO(String cxml){

        AribaRequestVO voAribaRequest = null;
        IRequestAuthenticator authenticator = null;
        OrderRequestOrderVO voOrderRequestOrder = null;
        ApoXmlHelper apoXmlHelper = null;
        String errorResponse = "";
        int customerType = 0;
        boolean processSuccess = false;
        boolean duplicateApo = false;
        UserTransaction userTransaction = null;
        InitialContext iContext = null;

        if(debugEnabled && (cxml != null)) {
            // Mask out CC info before writing to log
            String cxmlSafe = cxml.replaceAll("<PCard[^>]*>", "<PCard xxxx>");
            lm.debug("Approved PO Received:" + cxmlSafe);
        }

        ApprovedPOResponseGenerator responseGenerator = new ApprovedPOResponseGenerator();        

        try{  // ??? Temporary nesting of try clauses to catch elusive exception
        try{        
            //due to certificate issues at Ariba, take cxml string and convert https in the DOCTYPE tag 
            //and change to http
            cxml = ChangeHTTPS(cxml);

            //Validate message
            authenticator = 
                (IRequestAuthenticator) rm.getImplementation(INTERFACE_REQUESTAUTHENTICATOR);              
            voAribaRequest = authenticator.getXMLHeaderData(cxml);
            authenticator.validateDocument(voAribaRequest);

            // Certain clients require specific APO parsing, so instantiate proper 
            // APO XML helper class based on client.  Otherwise use default class.
            // ??? Change this! Instantiation should be driven dynamically instead of via conditionals.
            //
            HashMap paramMap = (HashMap) voAribaRequest.getParams();
            String asnNum = (String) paramMap.get(CONSTANT_ASN_NUMBER);
            if (asnNum.startsWith(asnTarget)) {
              apoXmlHelper = (ApoXmlHelper) new ApoXmlTarget();
            } else if (asnNum.startsWith(asnCSchwab)) {
              apoXmlHelper = (ApoXmlHelper) new ApoXmlCSchwab();
            } else if (asnNum.startsWith(asnMLynch)) {
              apoXmlHelper = (ApoXmlHelper) new ApoXmlMLynch();
            } else if (asnNum.startsWith(asnSaks)) {
              apoXmlHelper = (ApoXmlHelper) new ApoXmlSaks();
            } else if (asnNum.startsWith(asnNsc)) {
              apoXmlHelper = (ApoXmlHelper) new ApoXmlNsc();
            } else if (asnNum.startsWith(asnEds)) {
              apoXmlHelper = (ApoXmlHelper) new ApoXmlEds();
            } else {
              apoXmlHelper = (ApoXmlHelper) new ApoXmlDefault();
            }

            //Build the Order
            voOrderRequestOrder = apoXmlHelper.buildOrder(voAribaRequest,cxml);    

        }
        catch(AuthenticationException e){
            //exeception has been logged in utility class
            lm.debug("processApprovedPO 1: " + e.toString());
            e.printStackTrace();
            return responseGenerator.generateErrorResponse(PROPERTY_AUTHENTICATIONEXCEPTION,voAribaRequest);
            }
        catch(InvalidDUNSException e){
            //exeception has been logged in utility class
            lm.debug("processApprovedPO 2: " + e.toString());
            e.printStackTrace();
            return responseGenerator.generateErrorResponse(PROPERTY_INVALIDDUNSEXCEPTION,voAribaRequest);
            }
        catch(XMLParseException e){
            //exeception has been logged in utility class
            String[] msg = new String[] {e.toString()};
            lm.error("processApprovedPO 3: " + e.toString());
            e.printStackTrace();
            return responseGenerator.generateErrorResponse(PROPERTY_XMLPARSEEXCEPTION,voAribaRequest);
            }
        catch(ResourceNotFoundException e){
            String[] msg = new String[] {e.toString()};
            lm.error("processApprovedPO 4: "  + e.toString());
            e.printStackTrace();
            B2BMailer.getInstance().send(this,"proccessApprovedPO()",ERROR_CODE_UNABLETOLOADSERVICE,msg,B2BMailer.ACTION2);           
            return responseGenerator.generateErrorResponse(PROPERTY_THROWABLEEXCEPTION,voAribaRequest);
            }
        catch(Throwable e){
            String[] msg = new String[] {e.toString()};
            lm.error("processApprovedPO 5: " + e.toString());
            e.printStackTrace();
            B2BMailer.getInstance().send(this,"proccessApprovedPO()",ERROR_CODE_UNRECOVERABLE_EXCEPTION,msg,B2BMailer.ACTION2);           
            return responseGenerator.generateErrorResponse(PROPERTY_THROWABLEEXCEPTION,voAribaRequest);
            }
        } // ??? end temporary try clause    
        catch(Exception eTemp){
         // catching exception is not really needed since catch throwable below, but added anyhow
         lm.debug("processApprovedPO 6: " + eTemp.toString());
         return null;
         }
        catch(Throwable eTemp){
         lm.debug("processApprovedPO 7: " + eTemp.toString());
         eTemp.printStackTrace();
         return null;
         } // ??? end temporary try clause catches
         
        lm.debug("Approved PO was authenticated and validated successfully");

        //determine if punchout or catalog customer
        try{            
            //query URL table for Buyers ASN number..excpetion thrown if not on table
            URLLookupVO voURLLookup = null;
            synchronized (this.getClass())
            {
              voURLLookup = authenticator.getPunchOutClientData(voOrderRequestOrder.getAsnNumber());
            }
            customerType = CONSTANT_PUNCHOUT_CUSTOMER;                
            }
        catch(InvalidClientException e){
            //if exception thrown then this is catalog customer
            //customerType = CONSTANT_CATALOG_CUSTOMER;
            // We no longer allow Catalog customers so return error response to Ariba
            String[] msg = new String[] {voOrderRequestOrder.getAsnNumber()};
            lm.error("processApprovedPO - No ASN found: " + voOrderRequestOrder.getAsnNumber());
            e.printStackTrace();
            B2BMailer.getInstance().send(this,"proccessApprovedPO()",ERROR_CODE_INVALIDCLIENT_EXCEPTION,msg,B2BMailer.ACTION2);           
            return responseGenerator.generateErrorResponse(PROPERTY_INVALIDCLIENTEXCEPTION,voAribaRequest);            
        }        
        catch(Throwable e){
            String[] msg = new String[] {e.toString()};
            lm.error("processApprovedPO 8: " + e.toString());
            e.printStackTrace();
            B2BMailer.getInstance().send(this,"proccessApprovedPO()",ERROR_CODE_UNRECOVERABLE_EXCEPTION,msg,B2BMailer.ACTION2);           
            return responseGenerator.generateErrorResponse(PROPERTY_THROWABLEEXCEPTION,voAribaRequest);
            }       

        
        //Determine which process to execute
        try{
            iContext = new InitialContext();
            userTransaction = (UserTransaction)iContext.lookup("java:comp/UserTransaction");
            int transactionTimeout = Integer.parseInt(rm.getProperty(PROPERTY_B2B_PROPERTY_FILE, PROPERTY_TRANSACTION_TIMEOUT));
            userTransaction.setTransactionTimeout(transactionTimeout);
            userTransaction.begin();
                
            String orderType = voOrderRequestOrder.getOrderType();
            if (orderType.equalsIgnoreCase(CONSTANT_NEW_PO_FLAG)){
                lm.debug("Approved PO being processed as type: New");
                processSuccess = processNewPO(voOrderRequestOrder,voAribaRequest,customerType);
                }
            else{
                String[] msg = new String[] {orderType};
                lm.error("processApprovedPO - invalid PO type: "  + orderType);
                B2BMailer.getInstance().send(this, "processApprovedPO()", ERROR_CODE_INVALIDPOTYPE,msg,B2BMailer.ACTION1);
                throw new FTDApplicationException(ERROR_CODE_UNRECOVERABLE_EXCEPTION);
                }//end, else not new po
            }
        catch(FTDApplicationException e){
            //error already logged
            }
        catch(DuplicateApprovalException e){
            //error already logged
            duplicateApo = true;
            }
        catch(Throwable e){
            String[] msg = new String[] {e.toString()};
            lm.error("processApprovedPO 9: " + e.toString());
            B2BMailer.getInstance().send(this,"proccessApprovedPO()",ERROR_CODE_UNRECOVERABLE_EXCEPTION,msg,B2BMailer.ACTION2);           
            }
        finally{
        
           // Commit database changes
           if (processSuccess) {
               try {
                 if (userTransaction != null) {userTransaction.commit();}
               } catch (Exception re) {
                 lm.error("Commit exception: " + re);
                 processSuccess = false;
               }
           } 
           
           // If all went well, send success response back to Ariba, otherwise
           // rollback database changes and send error response.
           //
           if (processSuccess) {
               lm.debug("processApprovedPO - Returning success status to Ariba");
               return responseGenerator.generateResponse(voOrderRequestOrder);
           } else {
               lm.debug("processApprovedPO - Rolling back database changes and returning error status to Ariba");
               try {
                 if (userTransaction != null) {userTransaction.rollback();}
               } catch (Exception re) {
                 lm.error("Rollback exception ignored: " + re);
               }
               if (duplicateApo) {
                   return responseGenerator.generateErrorResponse(PROPERTY_DUPLICATEAPPROVALEXCEPTION,voAribaRequest);                   
               } else {
                   return responseGenerator.generateErrorResponse(PROPERTY_THROWABLEEXCEPTION,voAribaRequest);                   
               }
           }
        }
    }




     

    /**
     * Contains the logic for processing a new PO.
     *
     * @params OrderRequestOrderVO OrderRequest to process
     */
    private boolean processNewPO(OrderRequestOrderVO voOrderRequestOrder, AribaRequestVO voAribaRequest,int customerType)
                throws FTDApplicationException, Exception {

        IRequestAuthenticator authenticator = null;
        String orderNumber = null;
        IOrderVO voExistingOrder = null;
        String response = null;
        boolean isSuccess = false;
           

            List lines = voOrderRequestOrder.getLineItems();
            orderNumber = ((LineItemVO)lines.get(0)).getOrderNumber();


            //look for existing order if punchout request            
            if (customerType == CONSTANT_PUNCHOUT_CUSTOMER){
                voExistingOrder = getExistingOrder(orderNumber);
            }

            if (voExistingOrder == null){
                // Catalog order processing used to be here, but we no longer allow Catalog requests, 
                // so error response should be returned to Ariba
                String[] msg = new String[] {orderNumber};
                lm.error("processApprovedPO - Order number not found in pending tables: " + orderNumber);
                B2BMailer.getInstance().send(this,"proccessApprovedPO()",ERROR_CODE_APPROVEDORDERNOTFOUND,msg,B2BMailer.ACTION2);           
                throw new FTDApplicationException(ERROR_CODE_APPROVEDORDERNOTFOUND);
            } else {
                voOrderRequestOrder = setBuyerCookie(voOrderRequestOrder,voExistingOrder.getBuyerCookie());
                IPunchOutOrderProcessor processor = 
                    (IPunchOutOrderProcessor) rm.getImplementation(INTERFACE_PUNCHOUTORDERPROCESSOR);  
                synchronized (this.getClass())
                {
                  isSuccess = processor.processOrder(voOrderRequestOrder,voExistingOrder);
                }
                }
            return isSuccess;
            }
            

    /**
     * Get existing data from either the pending table.
     *
     * @params String orderNumber
     * @returns IOrderVO found order, or null if none found
     */
     private IOrderVO getExistingOrder(String orderNumber)
        throws FTDApplicationException, Exception{

        IOrderVO voExistingOrder = null;
        lm.debug("getExistingOrder - Looking for existing order in pending tables: " + orderNumber);

        try{

            //Get daos to retrieve pending data
            IPendingDAO daoPending = 
                (IPendingDAO) rm.getImplementation(INTERFACE_PENDINGDAO);                

            synchronized (this.getClass())
            {
              voExistingOrder = (IOrderVO)daoPending.retrieveByOrderNumber(orderNumber);
            }
        }
        catch(ResourceNotFoundException e){
            String[] msg = new String[] {e.toString()};
            lm.error("getExistingOrder: " + e.toString());
            B2BMailer.getInstance().send(this,"getExistingOrder()",ERROR_CODE_UNABLETOLOADSERVICE,msg,B2BMailer.ACTION2);
            throw new FTDApplicationException(ERROR_CODE_UNABLETOLOADSERVICE, e);            
            }
        catch(DuplicateApprovalException e){
            String[] msg = new String[] {e.toString()};
            lm.error("getExistingOrder : " + e.toString());
            B2BMailer.getInstance().send(this,"getExistingOrder()",ERROR_CODE_UNABLETOLOADSERVICE,msg,B2BMailer.ACTION2);
            throw new DuplicateApprovalException();            
            }
        catch(Throwable e){
            String[] msg = new String[] {e.toString()};
            lm.error("getExistingOrder  : " + e.toString());
            B2BMailer.getInstance().send(this,"getExistingOrder()",ERROR_CODE_UNRECOVERABLE_EXCEPTION,msg,B2BMailer.ACTION2);
            throw new FTDApplicationException(ERROR_CODE_UNRECOVERABLE_EXCEPTION);
        }
        
        return voExistingOrder;
     }
    
    
    /**
     * Set the buyer cookie to the passed in value for the passed in order object.
     * This method sets the buyer cookie on both the order and line level.
     *
     * @params OrderRequestOrder Order obect to be udpated
     * @params String cookie to be set in order object
     */
    private OrderRequestOrderVO setBuyerCookie(OrderRequestOrderVO voOrder,String cookie){

        List lines = voOrder.getLineItems();

        voOrder.setBuyerCookie(cookie);

        //set each line 
        for(int i =0; i < lines.size() ; i++){
            LineItemVO line = (LineItemVO)lines.get(i);
            line.setAribaBuyerCookie(cookie);            
        }

        return voOrder;
    }
    
    private String ChangeHTTPS(String incomingCXML)
    {
        String newCXMLTransmissionBeginning = null;
        String newCXMLTransmissionEnding = null;
        String newCXMLTransmission = null;
        int httpsLocation = -1;

        httpsLocation = incomingCXML.indexOf("https://");

        if (httpsLocation < 100)
        {
            newCXMLTransmissionBeginning = incomingCXML.substring(0, (httpsLocation+4));
            newCXMLTransmissionEnding = incomingCXML.substring( (httpsLocation+5) );
            newCXMLTransmission = newCXMLTransmissionBeginning + newCXMLTransmissionEnding;
            return(newCXMLTransmission);
        }
        else
        {
            return(incomingCXML);
        }
    }
}