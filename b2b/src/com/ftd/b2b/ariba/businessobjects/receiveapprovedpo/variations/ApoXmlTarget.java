package com.ftd.b2b.ariba.businessobjects.receiveapprovedpo.variations;

import java.util.Iterator;
import java.util.List;

import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.xpath.XPath;

import com.ftd.b2b.ariba.businessobjects.receiveapprovedpo.ApoXmlHelper;
import com.ftd.b2b.ariba.businessobjects.receiveapprovedpo.ReceiveApprovedPOImpl;
import com.ftd.b2b.ariba.common.valueobjects.ParsedNameVO;
import com.ftd.b2b.common.utils.XMLParseException;

/**
 * Top level utility class to convert an XML document into an Order Value Object for
 * Target.  This class should contain only the Target specific logic - the bulk 
 * of the logic is common to all clients and resides in the superclass.
 * 
 * @see ApoXmlHelper
 * @see ReceiveApprovedPOImpl#processApprovedPO
 */
public class ApoXmlTarget extends ApoXmlHelper {

   private static final String TARGET_ACCOUNT_STRING = "AccountingString";
   private static final String TARGET_DESTINATION    = "Form.Destination";
   private static final String TARGET_RECIP_NAME     = "Form.Recipient Name:";
   private static final String TARGET_REQ_PHONE      = "Form.Requestor Phone Number:";
   private static final String TARGET_MESSAGE1       = "Form.Message1:";
   private static final String TARGET_DATE_NEEDED    = "Form.Date Needed:";
   private static final String TARGET_BUSINESS_NAME  = "Form.Business Name";
   private static final String TARGET_STREET         = "Form.Street";
   private static final String TARGET_CITY           = "Form.City";
   private static final String TARGET_STATE          = "Form.State";
   private static final String TARGET_ZIP            = "Form.Zip";
   private static final String TARGET_RECIP_PHONE    = "Form.Phone";
   private static final String TARGET_ATTENTION      = "Form.Attention";
   private static final String TARGET_DELIVERY_NOTES = "Form.Delivery Notes";

  /** 
   * We override superclass method here since for Target, ShipTo should
   * always be used as BillTo.
   * 
   * @see ApoXmlHelper#buildBaseLineItemVO
   */
  protected void baseLineBillToCopy() throws JDOMException {
    // For Target, ShipTo should be used as BillTo
    //
    if (baseLineShipToNode != null) {
      baseLineVo.setBillToFirstName(baseLineVo.getShipToFirstName());
      baseLineVo.setBillToLastName(baseLineVo.getShipToLastName());
      baseLineVo.setBillToAddressLine1(baseLineVo.getShipToAddressLine1());
      baseLineVo.setBillToAddressLine2(baseLineVo.getShipToAddressLine2());
      baseLineVo.setBillToCity(baseLineVo.getShipToCity());
      baseLineVo.setBillToState(baseLineVo.getShipToState());
      baseLineVo.setBillToZipCode(baseLineVo.getShipToZipCode());
      baseLineVo.setBillToCountry(baseLineVo.getShipToCountry());
      //baseLineVo.setBillToWorkPhone(baseLineVo.getShipToWorkPhone()); This will be via extrinsic instead
      baseLineVo.setBillToHomePhone(baseLineVo.getShipToHomePhone());
      baseLineVo.setBillToFaxNumber(baseLineVo.getShipToFaxNumber());
      //baseLineVo.setBillToEmail(baseLineVo.getShipToEmail());
      baseLineVo.setBillToEmail("");  // Temporary change to always clear email address
    }
  }


  /**
   * Special line item detail processing for Target Stores
   */
  protected void detailLineItemClientSpecifics() throws XMLParseException, JDOMException {
  
      List extList = XPath.selectNodes(itemDetailNode,EXTRINSIC);
      Iterator extIt = extList.listIterator();
      String specialInstructions = "";
      String tStreet = "";
      String tCity = "";
      String tState = "";
      String tZip = "";
      String tBusinessName = "";
      String tBusinessType = "";
      
      // Extract data from all Extrinsic tags
      //
      while (extIt.hasNext()) {
        Element extElem = (Element)extIt.next();
        String extType = getAttributeValue(extElem,NAME);
        if (TARGET_ACCOUNT_STRING.equals(extType)) {
          String acctStr = getTextValue(extElem);
          try {
            detailLineVo.setCostCntrAccount(acctStr.substring(0,6));
            detailLineVo.setCostCntrLocation(acctStr.substring(6,10));
            detailLineVo.setCostCntrExpense(acctStr.substring(10,14));
          } catch (Exception e) {
            throw new XMLParseException("Invalid Accounting String detected.");
          }
        } else if (TARGET_DESTINATION.equals(extType)) {
          tBusinessType = getTextValue(extElem); 
        } else if (TARGET_RECIP_NAME.equals(extType)) {
          ParsedNameVO shipParsedName = parseName(getTextValue(extElem));
          detailLineVo.setShipToFirstName(shipParsedName.getFirstName());
          detailLineVo.setShipToLastName(shipParsedName.getLastName());
        } else if (TARGET_REQ_PHONE.equals(extType)) {
          detailLineVo.setBillToWorkPhone(getTextValue(extElem)); 
        } else if (TARGET_MESSAGE1.equals(extType)) {
          detailLineVo.setCardMessage(getTextValue(extElem)); 
        } else if (TARGET_DATE_NEEDED.equals(extType)) {
          detailLineVo.setDeliveryDate(buildDateNormal(getTextValue(extElem))); 
        } else if (TARGET_BUSINESS_NAME.equals(extType)) {
          tBusinessName = getTextValue(extElem); 
        } else if (TARGET_STREET.equals(extType)) {
          tStreet = getTextValue(extElem); 
        } else if (TARGET_CITY.equals(extType)) {
          tCity = getTextValue(extElem); 
        } else if (TARGET_STATE.equals(extType)) {
          tState = getTextValue(extElem); 
        } else if (TARGET_ZIP.equals(extType)) {
          tZip = getTextValue(extElem); 
        } else if (TARGET_RECIP_PHONE.equals(extType)) {
          detailLineVo.setShipToWorkPhone(getTextValue(extElem)); 
        } else if (TARGET_ATTENTION.equals(extType)) {
          specialInstructions += getTextValue(extElem) + " " ; 
        } else if (TARGET_DELIVERY_NOTES.equals(extType)) {
          specialInstructions += getTextValue(extElem) + " "; 
        }
      } // end while
      
      detailLineVo.setSpecialInstructions(specialInstructions); 

      // Use extrinsic address (and business name/type) if any of: 
      // street, city, state, or zip are entered
      //
      if ((tStreet.length() > 0) || (tCity.length() > 0) || 
          (tState.length() > 0) || (tZip.length() > 0)) {
        // Scrub is not happy with empty street, city, or zip so put in dummy if empty
        detailLineVo.setShipToAddressLine1(tStreet.length()==0?"Unknown":tStreet);  // approved_line_item has non-null constraint
        detailLineVo.setShipToCity(tCity.length()==0?"Unknown":tCity);  // approved_line_item has non-null constraint
        detailLineVo.setShipToZipCode(tZip.length()==0?"99999":tZip); 
        detailLineVo.setShipToState(tState); 
        detailLineVo.setShipToBusinessType(tBusinessType); 
        detailLineVo.setShipToBusinessName(tBusinessName); 
      }
   }
}

