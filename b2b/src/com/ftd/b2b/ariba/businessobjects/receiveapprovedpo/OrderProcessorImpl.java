package com.ftd.b2b.ariba.businessobjects.receiveapprovedpo;

import com.ftd.b2b.ariba.common.valueobjects.OrderRequestOrderVO;
import com.ftd.b2b.ariba.common.valueobjects.OrderRequestLineItemVO;
import com.ftd.b2b.ariba.common.valueobjects.ApprovedLineItemVO;
import com.ftd.b2b.ariba.common.valueobjects.ApprovedOrderVO;
import com.ftd.b2b.ariba.common.valueobjects.OrderEntryVO;

import java.util.StringTokenizer;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.io.*;

import com.ftd.b2b.constants.B2BConstants;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.b2b.ariba.integration.dao.IBuyerSourceCodeLookupDAO;
import com.ftd.b2b.ariba.integration.dao.ICountryCodeDAO;
import com.ftd.b2b.ariba.integration.dao.IApprovedDAO;
import com.ftd.b2b.ariba.integration.dao.IMasterOrderNumberDAO;
import com.ftd.framework.common.utilities.ResourceManager;


import com.ftd.framework.common.exceptions.ResourceNotFoundException;
import com.ftd.framework.common.exceptions.FTDApplicationException;

import com.ftd.b2b.utils.mail.B2BMailer;
import com.ftd.b2b.common.utils.BusinessObject;

import com.ftd.b2b.ariba.common.utils.MapGenerator;

/**
 * Parent class for CatalogOrderProcessor and 
 * PunchOutOrderProcessor.
 */

public abstract class OrderProcessorImpl extends BusinessObject 
            implements IOrderProcessor,B2BConstants
{

    protected boolean debugEnabled = false;
    protected ResourceManager rm;
    protected Logger lm;
    private static String asnTarget = null;
    
    /**
      *  Constructor
      */
    public OrderProcessorImpl(String category){
      super(category);
      lm = new Logger(LOG_CATEGORY_ORDER_PROCESSOR);
      if ( super.getLogManager().isDebugEnabled() )  {
         debugEnabled = true;
      }       
      rm = ResourceManager.getInstance();   
      if (asnTarget == null) {
        asnTarget = rm.getProperty(PROPERTY_B2B_PROPERTY_FILE, PROPERTY_ASN_TARGET);   
      }
    }




/**
 * Populate order level detail of approved order with data from 
 * order requests.
 *
 * @params ApprovedOrderVO, Order object to populate
 * @params OrderRequestOrder Order object containt request data
 */
 protected void populateHeader(ApprovedOrderVO voApprovedOrder, 
                            OrderRequestOrderVO voOrderRequest)
                            throws FTDApplicationException {
                            
        //populate the approved vo ORDER info based on data from PO REQUEST
        voApprovedOrder.setBuyerCookie(voOrderRequest.getBuyerCookie());
        voApprovedOrder.setAsnNumber(voOrderRequest.getAsnNumber());
        voApprovedOrder.setPayloadID(voOrderRequest.getPayloadID());
        voApprovedOrder.setInternalSupplierID(voOrderRequest.getInternalSupplierID());
        voApprovedOrder.setApprovedDate(voOrderRequest.getApprovedDate());
 }





/**
 * Get the next mater order number from the database.
 *
 * @returns String master order number to use for next order
 */
protected String getNextMasterOrderNumber() throws FTDApplicationException{
        IMasterOrderNumberDAO daoMasterOrderNumber = null;
        String newOrderNumber = null;
        try{            
            daoMasterOrderNumber = 
                (IMasterOrderNumberDAO) rm.getImplementation(INTERFACE_MASTERORDERNUMBERDAO); 
            newOrderNumber = daoMasterOrderNumber.retrieveMasterOrderNumber();
            }
        catch(ResourceNotFoundException e){
            String[] msg = new String[] {e.toString()};                                                                                            
            lm.error("getNextMasterOrderNumber: " +  e.toString());
            B2BMailer.getInstance().send(this,"getNextMasterOrderNumber()",ERROR_CODE_UNABLETOLOADSERVICE,msg,B2BMailer.ACTION2);
            throw new FTDApplicationException(ERROR_CODE_UNRECOVERABLE_EXCEPTION, e);
            }
        catch(Exception e){
            String[] msg = new String[] {e.toString()};                                                                                            
            lm.error("getNextMasterOrderNumber: " + e.toString());
            B2BMailer.getInstance().send(this,"getNextMasterOrderNumber()",ERROR_CODE_UNRECOVERABLE_EXCEPTION,msg,B2BMailer.ACTION2);
            throw new FTDApplicationException(ERROR_CODE_UNRECOVERABLE_EXCEPTION, e);
            }

        return newOrderNumber;
}

/**
 * Move data from the request po (i.e., cXML) to the approved vo line items
 *
 * @params ApprovedLineItemVO approved order line item to be populate
 * @params OrderRequestLineItemVO request order line item to obtain data from
 * @params OrderRequestOrderVO order object to obtain data from
 */
protected void moveRequestDataToApproveLine(ApprovedLineItemVO voApprovedLine,
                        OrderRequestLineItemVO voRequestLine,
                        OrderRequestOrderVO voRequestOrder)
                        throws FTDApplicationException{
    boolean shipInfoSetByNovator = true;                        
    voApprovedLine.setAribaBuyerCookie(voRequestOrder.getBuyerCookie());
    voApprovedLine.setAribaLineNumber(voRequestLine.getAribaLineNumber());

    /* 6/25/03 Ed Mueller
        Only use shipping information from APO cXML if the information was not found in the database. */
    if(( voApprovedLine.getShipToAddressLine1() == null || voApprovedLine.getShipToAddressLine1().equals("")) &&
        ( voApprovedLine.getShipToAddressLine2() == null || voApprovedLine.getShipToAddressLine2().equals(""))){
                    // Copy from APO cXML
                    voApprovedLine.setShipToFirstName(voRequestLine.getShipToFirstName());
                    voApprovedLine.setShipToLastName(voRequestLine.getShipToLastName());
                    voApprovedLine.setShipToAddressLine1(voRequestLine.getShipToAddressLine1());
                    voApprovedLine.setShipToAddressLine2(voRequestLine.getShipToAddressLine2());
                    voApprovedLine.setShipToState(voRequestLine.getShipToState());
                    voApprovedLine.setShipToCity(voRequestLine.getShipToCity());
                    voApprovedLine.setShipToZipCode(voRequestLine.getShipToZipCode());
                    voApprovedLine.setShipToCountry(voRequestLine.getShipToCountry());
                    voApprovedLine.setShipToHomePhone(voRequestLine.getShipToHomePhone());
                    voApprovedLine.setShipToWorkPhone(voRequestLine.getShipToWorkPhone());
                    voApprovedLine.setShipToWorkExtension(voRequestLine.getShipToWorkExtension());
                    voApprovedLine.setShipToFaxNumber(voRequestLine.getShipToFaxNumber());
                    voApprovedLine.setShipToEmail(voRequestLine.getShipToEmail());

                    shipInfoSetByNovator = false;
                }
        //6/25/03...end

    // Business Name and Destination values.  Use APO cXML only if not found in 
    // database.  Note that only Target catalog orders currently set this info in the APO.
    // Normal orders will pass this info during shopping cart checkout.
    if ((voApprovedLine.getShipToBusinessName() == null || voApprovedLine.getShipToBusinessName().equals("")) && 
         (voApprovedLine.getShipToBusinessType() == null || voApprovedLine.getShipToBusinessType().equals(""))) {
        // Copy from APO cXML
        voApprovedLine.setShipToBusinessName(voRequestLine.getShipToBusinessName());
        voApprovedLine.setShipToBusinessType(voRequestLine.getShipToBusinessType());
    }

    // BillTo added for Spec #2255
    // Certain clients specify requestor info during shopping cart checkout.  This
    // info will be saved in Pending BillTo fields, so if we find billTo info is defined here, 
    // we can assume it's one of those clients and cXML billTo info should be ignored.
    if(( voApprovedLine.getBillToLastName() == null || voApprovedLine.getBillToLastName().equals("")) &&
        ( voApprovedLine.getBillToAddressLine1() == null || voApprovedLine.getBillToAddressLine1().equals(""))){
        voApprovedLine.setBillToFirstName(voRequestLine.getBillToFirstName());
        voApprovedLine.setBillToLastName(voRequestLine.getBillToLastName());
        voApprovedLine.setBillToAddressLine1(voRequestLine.getBillToAddressLine1());
        voApprovedLine.setBillToAddressLine2(voRequestLine.getBillToAddressLine2());
        voApprovedLine.setBillToState(voRequestLine.getBillToState());
        voApprovedLine.setBillToCity(voRequestLine.getBillToCity());
        voApprovedLine.setBillToZipCode(voRequestLine.getBillToZipCode());
        voApprovedLine.setBillToHomePhone(voRequestLine.getBillToHomePhone());
        voApprovedLine.setBillToWorkPhone(voRequestLine.getBillToWorkPhone());
        voApprovedLine.setBillToWorkExtension(voRequestLine.getBillToWorkExtension());
        voApprovedLine.setBillToFaxNumber(voRequestLine.getBillToFaxNumber());
        voApprovedLine.setBillToEmail(voRequestLine.getBillToEmail());
    }
                
    voApprovedLine.setItemNumber(voRequestLine.getPartId());
    voApprovedLine.setItemDescription(voRequestLine.getItemDescription());
    
    voApprovedLine.setItemUNSPSCCode(voRequestLine.getItemUNSPSCCode());
    voApprovedLine.setAribaPONumber(voRequestLine.getAribaPONumber());
    voApprovedLine.setOrderNumber(voRequestLine.getOrderNumber());
    voApprovedLine.setAribaCostCenter(voRequestLine.getAribaCostCenter());
    voApprovedLine.setAMSProjectCode(voRequestLine.getAMSProjectCode());
    voApprovedLine.setPCardNumber(voRequestLine.getPCardNumber());
    voApprovedLine.setPCardExpirationDate(voRequestLine.getPCardExpirationDate());
    voApprovedLine.setPCardName(voRequestLine.getPCardName());

    // Cost Center accounting values (currently used for Target only)
    voApprovedLine.setCostCntrAccount(voRequestLine.getCostCntrAccount());
    voApprovedLine.setCostCntrLocation(voRequestLine.getCostCntrLocation());
    voApprovedLine.setCostCntrExpense(voRequestLine.getCostCntrExpense());
    
    // ??? Temporary hack inserted on 10/19/04 - this must be replaced in next release!!!
    // Intent was to override APO data with punchout data for "reqName" extension
    // data since needed for Charles Schwab implementation.  This should be replaced
    // with client specific extensions ala ApoXmlHelper.java.
    // 
    HashMap lext = voRequestLine.getLineItemExtensions();
    if ((lext != null) && (lext.containsKey("reqName") == true)) {
      String fullname = voApprovedLine.getBillToFirstName() + " " + 
                        voApprovedLine.getBillToLastName();
      lext.put("reqName", (String) fullname.trim());
    }
    // ??? End temporary hack
    // Hash for optional/extra client-specific order info
    voApprovedLine.setLineItemExtensions(lext);
    
    //Set comments
    /* 6/25/03 Ed Mueller
        Only set the comments and special instructions if they were not passed in the Novator\shopping cart transmission.*/
    if((voApprovedLine.getSpecialInstructions() == null ||  voApprovedLine.getSpecialInstructions().equals("")) &&
        (voApprovedLine.getCardMessage() == null ||  voApprovedLine.getCardMessage().equals(""))){
        String orderComments = 
            (voRequestOrder.getComments() == null ? "" : voRequestOrder.getComments());
        String lineComments = 
            (voRequestLine.getComments() == null ? "" : voRequestLine.getComments());
        if (orderComments.equalsIgnoreCase("") && lineComments.equalsIgnoreCase("")){
            voApprovedLine.setSpecialInstructions("");
            voApprovedLine.setCardMessage(CONSTANT_DEFAULT_CARD_MSG_TO
                                                +voRequestLine.getShipToFirstName()+ 
                                                 CONSTANT_DEFAULT_CARD_MSG_SPACE +
                                                 voRequestLine.getShipToLastName()+                                              
                                                 CONSTANT_DEFAULT_CARD_MSG_FROM  + 
                                                 voRequestLine.getBillToFirstName()+
                                                 CONSTANT_DEFAULT_CARD_MSG_SPACE +
                                                 voRequestLine.getBillToLastName());
            }
        else{
            voApprovedLine.setSpecialInstructions(orderComments + " " + lineComments);
            voApprovedLine.setCardMessage("");
            }

        // Card Message and Special Instructions may be set for Target Catalog requests
        //
        String asnNum = voRequestOrder.getAsnNumber();
        if ((asnNum != null) && (asnNum.startsWith(asnTarget))) {
          String cardMessage = 
            (voRequestLine.getCardMessage() == null ? "" : voRequestLine.getCardMessage());
          String specialInst = 
            (voRequestLine.getSpecialInstructions() == null ? "" : voRequestLine.getSpecialInstructions());
          if ((cardMessage.length() > 0) || (specialInst.length() > 0)) {
            voApprovedLine.setSpecialInstructions(specialInst);
            voApprovedLine.setCardMessage(cardMessage);
          }
          // Also, delivery date may be set for Target.  
          // ??? Might be able to do this for everyone instead of just for Target. 
          voApprovedLine.setDeliveryDate(voRequestLine.getDeliveryDate());
        }
    }//end card mesage or instructions are blank or null
    //6/25/03....end

    
    //Set values to empty/zero/null
    voApprovedLine.setTaxAmount(0);
    voApprovedLine.setOrderTotal(0);

    //Convert country codes from 3 char code to a 2 char code
    ICountryCodeDAO daoCountryCodes = null;
    try{
        daoCountryCodes = 
                (ICountryCodeDAO) rm.getImplementation(INTERFACE_COUNTRYCODEDAO);
        }
    catch(ResourceNotFoundException e){
            String[] msg = new String[] {"COUNTRYCODEDAO:" + e.toString()};                                                                                            
            lm.error("moveRequestDataToApproveLine: " + e.toString());
            B2BMailer.getInstance().send(this,"moveData()",ERROR_CODE_UNABLETOLOADSERVICE,msg,B2BMailer.ACTION2);
            throw new FTDApplicationException(ERROR_CODE_UNABLETOLOADSERVICE, e);    
        }
    try{            
        //only do this if the ship info was not set by Novator
        if(!shipInfoSetByNovator){
                voApprovedLine.setShipToCountry(daoCountryCodes.retrieveCountryCode(voRequestLine.getShipToCountry()));       
            }
       }
    catch(Exception e){
            String[] msg = new String[] {"COUNTRYCODEDAO:" + e.toString()};                                                                                            
            lm.error("moveRequestDataToApproveLine: " + e.toString());
            B2BMailer.getInstance().send(this,"moveData()",ERROR_CODE_UNRECOVERABLE_EXCEPTION,msg,B2BMailer.ACTION2);
            throw new FTDApplicationException(ERROR_CODE_UNRECOVERABLE_EXCEPTION, e);    
        }

    //Convert country codes from 3 char code to a 2 char code
    try{            
        voApprovedLine.setBillToCountry(daoCountryCodes.retrieveCountryCode(voRequestLine.getBillToCountry()));
       }
    catch(Exception e){
            String[] msg = new String[] {"BillToCountryCode:" + e.toString()};                                                                                            
            lm.error("moveRequestDataToApproveLine: " + e.toString());
            B2BMailer.getInstance().send(this,"moveData()-bill to country",ERROR_CODE_UNRECOVERABLE_EXCEPTION,msg,B2BMailer.ACTION2);
            throw new FTDApplicationException(ERROR_CODE_UNRECOVERABLE_EXCEPTION, e);    
        }

}

/**
 * This function takes in a pased in ApprovedOrder vo which needs to have addon
 * information update for a line item.  This function updates the addon information
 * for the lineitem that corresponds to the passed in PO request line item.
 *
 * @params ApprovedOrderVo Approved order VO to update
 * @params LineItemVO The PO Request line item which contains an Addon
 * @returns ApprovedOrderVO The update approved order object
 */
 protected ApprovedOrderVO updateAddonFields(ApprovedOrderVO voApprovedOrder, 
                                           OrderRequestLineItemVO voRequestLine)
                            throws FTDApplicationException{


    boolean addonFound = false;
    StringBuffer newAddonDetail = new StringBuffer("");

    Map approvedLineMap = MapGenerator.getLineItemMapKeyedByOrderNumber(voApprovedOrder);
    
    //get the approved line that goes with this request line item z
    String orderNumber = voRequestLine.getOrderNumber();
    ApprovedLineItemVO voApprovedLine = (ApprovedLineItemVO)approvedLineMap.get(orderNumber);
    if (voApprovedLine == null){
            String[] msg = new String[] {voRequestLine.getAribaBuyerCookie(),orderNumber};
            lm.error("updateAddonFields - order line not found: "  + voRequestLine.getAribaBuyerCookie() + " " + orderNumber);
            B2BMailer.getInstance().send(this,"updateAddonFields()",ERROR_CODE_ORDERLINENOTFOUND,msg,B2BMailer.ACTION2);
            throw new FTDApplicationException(ERROR_CODE_ORDERLINENOTFOUND);
        }


    //Remove the ADDON prefix form the addon ID
    String requestAddonID = voRequestLine.getPartId();
    requestAddonID = requestAddonID.substring(CONSTANT_ADDONFLAG.length(),
                                              requestAddonID.length());
                                              
    int requestAddonQuantity = voRequestLine.getQuantity();

    //get addon information from approved order
    String approvedAddonDetail = voApprovedLine.getAddOnDetail();
    int approvedTotalAddonCount = voApprovedLine.getAddOnCount();
    
    //if the approved order line does not contain addon data then we are almost done
    if (approvedAddonDetail == null || approvedAddonDetail.equalsIgnoreCase("")){
        addonFound = false;
        }
    else{
        //break apart the approved addon data
        StringTokenizer itemTokenizer = 
                             new StringTokenizer(approvedAddonDetail, 
                                                 CONSTANT_ADDON_ITEM_DELIMITER);
        while (itemTokenizer.hasMoreElements()){                

            String addonData = itemTokenizer.nextToken();    

            //Obtain item id and quantity
            StringTokenizer idTokenizer = new StringTokenizer(addonData, 
                                            CONSTANT_ADDON_QUANTITY_DELIMITER);                
            String approvedAddonID = idTokenizer.nextToken();
            int approvedQuantity = Integer.parseInt(idTokenizer.nextToken());

            //check if this is the addon we are looking for
            if (approvedAddonID.equalsIgnoreCase(requestAddonID)){

                addonFound = true;

                //increment counts
                approvedQuantity += requestAddonQuantity;
                approvedTotalAddonCount += requestAddonQuantity;

              }//end if addon found

                //append to new addon detail, add delimiter if needed
                if (!newAddonDetail.toString().equalsIgnoreCase("")){
                    newAddonDetail.append(CONSTANT_ADDON_ITEM_DELIMITER);
                    }
                 newAddonDetail.append(approvedAddonID + 
                                  CONSTANT_ADDON_QUANTITY_DELIMITER + 
                                  approvedQuantity);

                
            } //end while more tokens                
        }//end else, no current addons

    //if an addon match was not found then add it
    if (!addonFound){
       approvedTotalAddonCount += requestAddonQuantity;
        if (!newAddonDetail.toString().equalsIgnoreCase("")){
            newAddonDetail.append(CONSTANT_ADDON_ITEM_DELIMITER);
            }
        newAddonDetail.append(requestAddonID + 
                              CONSTANT_ADDON_QUANTITY_DELIMITER + 
                              requestAddonQuantity);
    }

    //update the approved line item
    voApprovedLine.setAddOnDetail(newAddonDetail.toString());
    voApprovedLine.setAddOnCount(approvedTotalAddonCount);

    return voApprovedOrder;
 }

    /**
     * Save the order to the approved tables and send to order_gatherer
     *
     * @params ApprovedOrderVO The approved order to save to the database.
     */
    protected void persistOrder(ApprovedOrderVO voApprovedOrder) 
                 throws FTDApplicationException{
                 
        IApprovedDAO daoApproved = null;
        String newOrderNumber = null;
        
        // Save to approved tables
        //
        try{
            daoApproved = (IApprovedDAO) rm.getImplementation(INTERFACE_APPROVEDDAO); 
            daoApproved.insert(voApprovedOrder);
            }
        catch(ResourceNotFoundException e){
            String[] msg = new String[] {"APPROVEDINSERT:" + e.toString()};
            lm.error("Approval - persistOrder: " + e.toString());
            B2BMailer.getInstance().send(this,"persistOrder()",ERROR_CODE_UNABLETOLOADSERVICE,msg,B2BMailer.ACTION2);
            throw new FTDApplicationException(ERROR_CODE_UNABLETOLOADSERVICE, e);            
            }
        catch(Exception e){
            String[] msg = new String[] {"APPROVEDINSERT:" + e.toString()};
            lm.error("Approval - persistOrder: " + e.toString());
            e.printStackTrace();
            B2BMailer.getInstance().send(this,"persistOrder()",ERROR_CODE_UNRECOVERABLE_EXCEPTION,msg,B2BMailer.ACTION2);
            throw new FTDApplicationException(ERROR_CODE_UNRECOVERABLE_EXCEPTION, e);
            }

        // Send to order_gatherer
        //
        try{
            new ReceivedPOToFTDImpl().send(voApprovedOrder);
            }
        catch(Throwable e){
            final Writer result = new StringWriter();
            final PrintWriter printWriter = new PrintWriter( result );
            e.printStackTrace( printWriter );
            String[] msg = new String[] {"send: " + result.toString()};
            lm.error("Approval - persistOrder: " + msg[0]);
            B2BMailer.getInstance().send(this,"persistOrder()",ERROR_CODE_UNRECOVERABLE_EXCEPTION,msg,B2BMailer.ACTION2);
            throw new FTDApplicationException(ERROR_CODE_UNRECOVERABLE_EXCEPTION, e);
        }//end catch
       
    }

    /**
     * Inserts the approved order into the Order Entry tables.  OLD CODE - NEVER CALLED
     */
   /*
   private void insertOrderEntry(ApprovedOrderVO voApprovedOrder)
        throws FTDApplicationException{
        IOrderEntryDAO daoOrderEntry = null;
        IBuyerSourceCodeLookupDAO daoSourceCode = null;
        String sourceCode = null;

        try{            
            daoOrderEntry = (IOrderEntryDAO) rm.getImplementation(INTERFACE_ORDERENTRYDAO);
            daoSourceCode = (IBuyerSourceCodeLookupDAO) rm.getImplementation(INTERFACE_BUYERSOURCECODELOOKUPDAO);

            //obtain source code
            sourceCode = daoSourceCode.retrieveByASNNumber(voApprovedOrder.getAsnNumber());

            if (sourceCode == null){
                sourceCode =  rm.getProperty(PROPERTY_B2B_PROPERTY_FILE, PROPERTY_DEFAULT_SOURCE_CODE);                
            }
            
            }
        catch(ResourceNotFoundException e){
            String[] msg = new String[] {e.toString()};
            lm.error("insertOrderEntry: " + e.toString());
            B2BMailer.getInstance().send(this,"insertOrderEntry()",ERROR_CODE_UNABLETOLOADSERVICE,msg,B2BMailer.ACTION2);
            throw new FTDApplicationException(ERROR_CODE_UNABLETOLOADSERVICE, e);            
            }          
        catch(Throwable e){
            String[] msg = new String[] {e.toString()};
            lm.error("insertOrderEntry: " + e.toString());
            B2BMailer.getInstance().send(this,"insertOrderEntry()",ERROR_CODE_DAO_EXCEPTION,msg,B2BMailer.ACTION2);
            throw new FTDApplicationException(ERROR_CODE_DAO_EXCEPTION, e);            
            }          

        OrderEntryVO voOrderEntry = new OrderEntryVO();
        voOrderEntry.setSourceCode(sourceCode);
        voOrderEntry.setASNNumber(voApprovedOrder.getAsnNumber());        

        //Insert each line into order entry, if error occurs log\email it and
        //keep going.
        List lines = voApprovedOrder.getLineItems();
        int numberOfLines = lines.size();
        for (int i = 0; i < numberOfLines ; i++){
            ApprovedLineItemVO approvedLine = (ApprovedLineItemVO)lines.get(i);
            voOrderEntry.setLineItemVO(approvedLine);
            try{
                daoOrderEntry.insert(voOrderEntry);
                }
            catch(Throwable e){
                String[] msg = new String[] {approvedLine.getOrderNumber(),e.toString()};
                lm.error("insertOrderEntry - order entry insert error: " + approvedLine.getOrderNumber() + " " + e.toString());
                B2BMailer.getInstance().send(this,"insertOrderEntry()",ERROR_CODE_ORDERENTRYINSERT,msg,B2BMailer.ACTION2);
                }
        }// end for
    }//end method
    */

}