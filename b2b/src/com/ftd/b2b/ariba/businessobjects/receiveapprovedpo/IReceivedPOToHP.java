package com.ftd.b2b.ariba.businessobjects.receiveapprovedpo;


import com.ftd.framework.common.exceptions.*;

import com.ftd.b2b.ariba.common.valueobjects.ApprovedOrderVO;

import java.sql.*;
import java.io.*;

public interface IReceivedPOToHP 
{

  public void sendPOToHP(ApprovedOrderVO voApprovedOrder) throws FTDApplicationException;

}