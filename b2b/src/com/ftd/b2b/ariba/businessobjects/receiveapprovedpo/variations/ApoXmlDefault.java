package com.ftd.b2b.ariba.businessobjects.receiveapprovedpo.variations;

import com.ftd.b2b.ariba.businessobjects.receiveapprovedpo.ApoXmlHelper;

import com.ftd.b2b.ariba.common.valueobjects.AribaRequestVO;
import com.ftd.b2b.ariba.common.valueobjects.OrderRequestLineItemVO;
import com.ftd.b2b.ariba.common.valueobjects.OrderRequestOrderVO;
import com.ftd.b2b.ariba.common.valueobjects.ParsedNameVO;
import com.ftd.b2b.ariba.common.valueobjects.PhoneVO;
import com.ftd.b2b.ariba.common.valueobjects.PurchaseCreditCardVO;
import com.ftd.b2b.common.utils.XMLParseException;
import com.ftd.b2b.constants.B2BConstants;

import java.io.ByteArrayInputStream;
import java.io.StringReader;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Iterator;
import java.util.StringTokenizer;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.xpath.*;

/**
 * Top level utility class to convert an XML document into an Order Value Object. 
 * This is the default for clients - any clients with specific parsing needs should
 * implement their own class (extending ApoXmlHelper).  The proper class is instantiated
 * in ReceiveApprovedPOImpl.processApprovedPO.
 * 
 * @see ApoXmlHelper
 * @see ReceiveApprovedPOImpl#processApprovedPO
 */
public class ApoXmlDefault extends ApoXmlHelper
{
  protected void detailLineItemClientSpecifics() throws XMLParseException, JDOMException {
    // Nothing extra to be done     
  }
}

