package com.ftd.b2b.ariba.businessobjects.receiveapprovedpo.variations;

import com.ftd.b2b.ariba.businessobjects.receiveapprovedpo.ApoXmlHelper;

import com.ftd.b2b.ariba.common.valueobjects.AribaRequestVO;
import com.ftd.b2b.ariba.common.valueobjects.OrderRequestLineItemVO;
import com.ftd.b2b.ariba.common.valueobjects.OrderRequestOrderVO;
import com.ftd.b2b.ariba.common.valueobjects.ParsedNameVO;
import com.ftd.b2b.ariba.common.valueobjects.PhoneVO;
import com.ftd.b2b.ariba.common.valueobjects.PurchaseCreditCardVO;
import com.ftd.b2b.common.utils.XMLParseException;
import com.ftd.b2b.constants.B2BConstants;

import java.io.ByteArrayInputStream;
import java.io.StringReader;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Iterator;
import java.util.StringTokenizer;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.xpath.*;

/**
 * Top level utility class to convert an XML document into an Order Value Object for
 * Merrill Lynch.  This class should contain only Merrill Lynch specific logic - the bulk 
 * of the logic is common to all clients and resides in the superclass.
 * 
 * @see ApoXmlHelper
 * @see ReceiveApprovedPOImpl#processApprovedPO
 */
public class ApoXmlMLynch extends ApoXmlHelper {

  private static final String LYNCH_INVOICE_PO_NUM   = "poNum";
  private static final String LYNCH_INVOICE_LINE_NUM = "poLineNum";


  /**
   * Special line item detail processing for Merrill Lynch.  The Order ID, 
   * and line item number are to be treated as line item extensions (so they will show
   * up in the monthly revenue reports from the HP).
   */
  protected void detailLineItemClientSpecifics() throws XMLParseException, JDOMException {

    HashMap lie = new HashMap();

    lie.put(LYNCH_INVOICE_PO_NUM, detailLineVo.getAribaPONumber());
    lie.put(LYNCH_INVOICE_LINE_NUM, String.valueOf(detailLineVo.getAribaLineNumber()));
    detailLineVo.setLineItemExtensions(lie);
  }
  
}
