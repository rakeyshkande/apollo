package com.ftd.b2b.ariba.businessobjects.receiveapprovedpo;

import com.ftd.framework.common.utilities.ResourceManager;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.b2b.common.utils.SocketSender;
import com.ftd.b2b.utils.mail.B2BMailer;
import com.ftd.b2b.constants.B2BConstants;

import java.net.*;
import java.io.*;
import java.util.*;

/**
 * Singleton class for sending messages to the Novator socket.
 * The 'getInstance()' method of this class should be called in a
 * startup servlet to create the singleton intially.
 *
 * @author York Davis
 * @version 1.0 
 **/
public class NovatorPurgeShoppingCartSocketSender extends SocketSender implements B2BConstants
{
  private static NovatorPurgeShoppingCartSocketSender socketSender = null;
  
  /**
   * Private constructor for Singleton. All startup processing
   * is performed here so that the 'send()' method can
   * simply send messages.
   *
   * @author York Davis
   **/
  private NovatorPurgeShoppingCartSocketSender()
  {
      ResourceManager  resourceManager = ResourceManager.getInstance();
      String ip      =  resourceManager.getProperty(PROPERTY_B2B_PROPERTY_FILE, 
                                                    PROPERTY_NOVATOR_PURGE_SHOPPING_CART_SOCKET_IP);   
      String port    =  resourceManager.getProperty(PROPERTY_B2B_PROPERTY_FILE, 
                                                    PROPERTY_NOVATOR_PURGE_SHOPPING_CART_SOCKET_PORT);   
      this.ip = processIp(ip);
      this.iport = processPort(port);
      logManager.info(this.getClass() + " started using IP=" + ip + " port=" + Integer.toString(iport) + ".");
  }

  /**
   * Method for obtaining a reference to this singleton class.
   *
   * @return NovatorPurgeShoppingCartSocketSender object reference.
   * @author York Davis
   **/
  public static NovatorPurgeShoppingCartSocketSender getInstance()
  {
      if (socketSender == null)
      {
          socketSender = new NovatorPurgeShoppingCartSocketSender();
      }
      return socketSender;
  }
}