package com.ftd.b2b.integration.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import oracle.jdbc.OracleTypes;

import com.ftd.b2b.constants.B2BConstants;
import com.ftd.framework.common.exceptions.BadConnectionException;
import com.ftd.framework.common.exceptions.FTDSystemException;
import com.ftd.framework.common.utilities.ResourceManager;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;


/**
 * Abstract, DAO super-class for all implementation DAOs.
 * Creates connection to database, handles commits and rollbacks,
 * send SQL calls to database and builds result sets.
 *
 * @author York Davis
 * @version 1.0
 */
public abstract class DAO implements B2BConstants
{
  private Connection  con = null;
  private boolean selfManagedTransaction = false;
  private ResourceManager resourceManager;
  protected static Logger logger = new Logger(DAO.class.getName());
  protected boolean wasAnyFieldTruncated;

  protected DAO(String sLogLoc)
  {

    resourceManager = ResourceManager.getInstance();
    selfManagedTransaction = false;
  }

  protected DAO(String sLogLoc, boolean selfManagedTransaction)
  {
    resourceManager = ResourceManager.getInstance();
    this.selfManagedTransaction = selfManagedTransaction;    
  }

  /**
   * Sets the Transaction self-managed flag.
   *
   * @param Self-managed flag.
   * @author York Davis
   **/  
  protected void setSelfManagedTransaction(boolean b)
  {
      selfManagedTransaction = b;
      try {
        if (con != null) {
          con.setAutoCommit(!b);
        }
      } catch (java.sql.SQLException e) {
          // No big deal
      }
  }

  /**
   * Returns the self-managed transaction flag.
   *
   * @return The self-managed flag.
   * @author York Davis
   **/  
  protected boolean isSelfManagedTransaction()
  {
      return selfManagedTransaction;
  }

  /**
   * Obtain a database connection.
   *
   * 10/15/2002 (YWD) - Added try/catch for throwable. It appears that if the
   * framework cannot obtain a db connection it throws a throwable. We want 
   * to wrap this and rethrow BadConnectionException.
   *
   * @return Database connection.
   * @author York Davis
   **/   
  private Connection getConnection() throws SQLException, BadConnectionException, FTDSystemException
  {
    try
    {
      String dataSource = resourceManager.getProperty("ftd.framework.configuration", "application.db.datasource");
      con = DataSourceUtil.getInstance().getConnection(dataSource);
    }
    catch (Throwable t)
    {
      String msg = this.getClass().toString() + " took an error obtaining a database connection : " + t.toString();
      t.printStackTrace();
      throw new BadConnectionException(ERROR_CODE_UNABLETOCONNECTTODB, new String[] {msg});
    }      

  	if (con == null)
  	{
  		throw new SQLException("Database connection is null.");
  	}
    if (selfManagedTransaction)
    {
        con.setAutoCommit(false);
    }
 
    return con;    
  }

  /**
   * Close the database connection.
   *
   * @author York Davis
   **/   
  private void closeConnection() throws SQLException
  {
    if(con != null && !con.isClosed())
    {
      try
      {
              con.close();
      }
      catch (SQLException sqle)
      {
      }
      con = null;      
    }
  }

  /** 
   * Used to send SQL calls to database for such things as insert and deletes.
   * Returns only a single value
   * @parm SQL call statement, List of values that will be used in SQL call statement
   * @return String 
   * @throws SQL Exception or BadConnection if there is a problem with the call to
   *         the database.
   *
   * @author York Davis
   */
  protected String execute(String SQL, Object[] param) throws SQLException, 
                                                              BadConnectionException,
                                                              FTDSystemException
  {
    String result     = "";
  	int iRowsEffected = 0;

    try
    {
    if ((selfManagedTransaction) && con == null)
    {
      if (logger.isDebugEnabled())  
      {
    	  logger.debug("Self Managed and getting Connection");
      }     
      con = getConnection();
     }
     else
        if (con == null)
        {
          if (logger.isDebugEnabled())  
          {
            logger.debug("Not Self Managed and getting Connection");
          }         
          con = getConnection();
        }

      CallableStatement callableStatement = con.prepareCall(SQL);
  		callableStatement.registerOutParameter(1, java.sql.Types.VARCHAR);

  		// Set params if not null.
  		if (param != null)
  		{
  			// set the parameters
  			int iNumParams = param.length;
  			for (int i=0; i<iNumParams; i++)
  			{
          Object parmObject = param[i];
          if (parmObject instanceof java.lang.String)
          {
            String s = (String) parmObject;
      			callableStatement.setString(i+2, s);
            continue;
          }
          if (parmObject instanceof Integer)
          {
            int j = ((Integer) parmObject).intValue();
      			callableStatement.setInt(i+2, j);   
            continue;
          }
          if (parmObject instanceof Long)
          {
            long j = ((Long) parmObject).longValue();
      			callableStatement.setLong(i+2, j);   
            continue;
          }
          if (parmObject instanceof Float)
          {
            float f = ((Float) parmObject).floatValue();
      			callableStatement.setFloat(i+2, f);                        
            continue;
          }
          if (parmObject instanceof Double)
          {
            double d = ((Double) parmObject).doubleValue();
      			callableStatement.setDouble(i+2, d);                        
            continue;
          }
          if (parmObject instanceof java.util.Date)
          {
            java.util.Date date = ((java.util.Date) parmObject);
      			callableStatement.setDate(i+2, new java.sql.Date(date.getTime()));                        
            continue;
          }
          if (parmObject == null)
          {
            callableStatement.setString(i+2, null);
            continue;
          }
  			}
  		}

  		callableStatement.execute();
    	result = (String) callableStatement.getObject(1);
      callableStatement.close();
  	}
  	catch (SQLException sqle)
  	{
    	throw sqle;
  	}
  	finally
    {
      if ( !isSelfManagedTransaction() )
      {
        closeConnection();
     	}
    }
  	return result;	
  }

  /**
   * Overloaded execute() method when there are no parameters.
   *
   * @return String 
   * @throws SQL Exception or BadConnection if there is a problem with the call to
   *         the database.
   *
   * @author York Davis
   */
  protected String execute(String SQL) throws SQLException, 
                                              BadConnectionException,
                                              FTDSystemException
  {
      return execute(SQL, null);
  }

  /**
   * Used to send SQL calls to database for retrieving multiple values
   * @parm SQL call statement, List of values that will be used in SQL call statement
   * @return List of records returned from the database
   * @throws SQL Exception or BadConnection if there is a problem with the call to
   *         the database.
   *
   * @author York Davis
   */
  protected List retrieve(String SQL,  Object[] param)  throws SQLException, 
                                                               BadConnectionException,
                                                               FTDSystemException
  {
  	List list = new ArrayList();
	
    try
  	{
  		// get the connection and check if null
  		con = getConnection();

  		CallableStatement callableStatement = con.prepareCall(SQL);
  		callableStatement.registerOutParameter(1, OracleTypes.CURSOR);

  		// Set params if not null.
      if (param != null)
      {
  			int iNumParams = param.length;
  			for (int i=0; i<iNumParams; i++)
  			{
          Object parmObject = param[i];        
          if (parmObject instanceof java.lang.String)
          {
            String s = (String) parmObject;
      			callableStatement.setString(i+2, s);
            continue;
          }
          if (parmObject instanceof Integer)
          {
            int j = ((Integer) parmObject).intValue();
      			callableStatement.setInt(i+2, j);            
            continue;
          }
          if (parmObject instanceof Float)
          {
            float f = ((Float) parmObject).floatValue();
      			callableStatement.setFloat(i+2, f);                        
            continue;
          }
          if (parmObject instanceof Double)
          {
            double d = ((Double) parmObject).doubleValue();
      			callableStatement.setDouble(i+2, d);                        
            continue;
          }
          if (parmObject instanceof java.util.Date)
          {
            java.util.Date date = ((java.util.Date) parmObject);
      			callableStatement.setDate(i+2, new java.sql.Date(date.getTime()));                        
            continue;
          }
          if (parmObject == null)
          {
            callableStatement.setString(i+2, null);
            continue;
          }
  			}
      }

    	callableStatement.execute();
  		ResultSet rs = (ResultSet) callableStatement.getObject(1);
  		while (rs.next())
    	{
  			list.add(set(rs));
  		}
      callableStatement.close();
		  rs.close();
    }
  	catch (SQLException sqle)
  	{
  		throw sqle;
  	}
  	finally
  	{
        closeConnection();
  	}
    
  	return list.size() == 0 ? null : list;
  }

  /**
   * Overloaded retrieve() method when there are no parameters.
   *
   * @return List of records returned from the database
   * @throws SQL Exception or BadConnection if there is a problem with the call to
   *         the database.
   *
   * @author York Davis
   */
  protected List retrieve(String SQL)  throws SQLException, 
                                              BadConnectionException,
                                              FTDSystemException
  {
      return retrieve(SQL, null);
  }

  /** 
   * OBSOLETE - ACTUAL ROLLBACKS SHOULD NO LONGER OCCUR - THIS CODE SHOULD BE
   * REMOVED ONCE ITS CONFIRMED NO ATTEMPTS ARE BEING MADE.
   * 
   * Rollback transactions that have occur after the last commit
   * @throws SQL Exception if there is a problem with rollback the database.
   *
   * @author York Davis
   */
  protected void rollback() throws SQLException
  {
    if (!selfManagedTransaction) return;
	logger.error("Attempting to rollback - this should no longer be invoked");
      try
      {
        con.rollback();
      }
      catch (SQLException e)
      {
          throw e;
      }
      finally
      {
        closeConnection();
      }
  }

  /** 
   * OBSOLETE - ACTUAL COMMITS SHOULD NOT OCCUR AT THIS LEVEL - THIS CODE SHOULD BE
   * REMOVED ONCE ITS CONFIRMED NO ATTEMPTS ARE BEING MADE.
   * 
   * Commit all work since last commit
   * @parm None
   * @return None
   * @throws SQL Exception if there is a problem with commit to the database.
   *
   * @author York Davis
   */
  protected void commit() throws SQLException
  {
    if (!selfManagedTransaction)  return;
      logger.error("Attempting to commit from DAO - this should no longer be invoked");
      try
      {
        con.commit();
      }
      catch (SQLException e)
      {
          throw e;
      }
      finally
      {
        closeConnection();
      }
  }

/**
 * Encode characters to be inserted into database.
 *
 * @return java.lang.String
 */
public String encode(String val) 
{
	if(val != null) 
	{ 
		StringBuffer buf = new StringBuffer(""); 
		char c; 
		for(int i = 0; i < val.length(); i++) 
		{
			c = val.charAt(i); 
			switch(c) 
			{ 
				case '\u00ae': 
					buf.append("&reg;"); 
					break; 

				case '\u00a9': 
					buf.append("&copy;"); 
					break;

				case '\u2122': 
					buf.append("&trade;"); 
					break;                    
 
				default: 
					buf.append(c); 
					break; 
			} 
		} 
		return buf.toString(); 
	} 
	else 
	{ 
		return new String(""); 
	} 
}


protected String truncIt(String inStr, int maxLen) throws SQLException {
    String returnStr = inStr;
    if (returnStr != null && returnStr.length() > maxLen) {
        wasAnyFieldTruncated = true;
        int origLen = returnStr.length();
        returnStr = returnStr.substring(0,maxLen);
        logger.warn("Value too large for DB column, so truncated from " + origLen +  
                    " to " + maxLen + " chars: " + returnStr);
    }
    return returnStr;
}

  
  /** 
   * Abstract method which encapsulates the creation of a VO object from
   * a ResultSet object. The implemenation for this method will be deferred 
   * until this class is sub-classed at which point the code will create a VO,
   * populate it and return it.
   *
   * @parm ResultSet object.
   * @return created VO.
   * @throws SQL Exception if there is a problem with commit to the database.
   *
   * @author York Davis
   */
  protected abstract Object set(ResultSet rsResults) throws SQLException;
 
}
