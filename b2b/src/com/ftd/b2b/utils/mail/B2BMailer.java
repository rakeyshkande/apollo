package com.ftd.b2b.utils.mail;

import java.net.InetAddress;

import com.ftd.b2b.ariba.integration.PingServlet;
import com.ftd.b2b.constants.B2BConstants;
import com.ftd.framework.common.utilities.ErrorCodeStore;
import com.ftd.framework.common.utilities.ResourceManager;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * This class is used by the B2B application to send emails or pages
 * to distribution lists when the B2B application detects an error condition.
 * These distribution lists and other associated properties are elements of the properties file.
 * This class is a singleton which should be started at server startup time.
 *
 * @author York Davis
 * @version 1.0 
 **/
public class B2BMailer implements B2BConstants 
{
    private static B2BMailer mailer   = null;
    protected static Logger logger = new Logger(B2BMailer.class.getName());

    private MailMessageVO    mailMessage = null;
    private MailManager      mailManager;
    private ResourceManager  resourceManager;
    private ConfigurationUtil cu;

    // ACTIONs are arbitrary in that they define an action to be 
    // taken but do not define the action itself. This was devised
    // so that later enhancements would not be tied to a specific
    // mail implementation such as 'setEmail(true)' or 'setPage(true)', etc.
    public static final int ACTION1 = 1; // Email notification
    public static final int ACTION2 = 2; // Pager notification   
    public static final int ACTION3 = 3; // Email and Pager notification 

    private static String emailTo;
    private static String emailFrom;
    private static String emailSubject;
    private static String pageFrom;
    private static String pageSubject;
    private static String localHost;


    /**
     * Private constructor for this singleton class.
     * This constructor encapsulates all processing that can be performed
     * at server start up time and does not need to be performed for 
     * each mail send.
     *
     * @author York Davis
     **/
    private B2BMailer()
    {
      resourceManager = ResourceManager.getInstance();  
      
      String mailServer = "";
      try {
          cu = ConfigurationUtil.getInstance();
          mailServer = cu.getFrpGlobalParm(B2B_ARIBA_CONFIG_CONTEXT, PROPERTY_MAIL_SERVER);
      } catch (Exception e) {
          logger.error("Failed to get ConfigurationUtil. " + e);
      }

    //Get local host name
    try{
        InetAddress inet = InetAddress.getLocalHost();
        localHost = inet.toString();   
    }
    catch(java.net.UnknownHostException e){
        localHost = "UNKNOWN LOCALHOST:" + e;
        System.out.println("Error accessing local host name");
    }

      mailManager = new MailManager(LOG_CATEGORY_JAVAMAIL, mailServer);        
    }
    
    /**
     * Obtain the singleton reference to the B2BMailer.
     *
     * @return The B2BMail object.
     *
     * @author York Davis
     **/
   public static synchronized B2BMailer getInstance()
   {
        try 
        {
           if (mailer == null)
           {
             mailer = new B2BMailer();
           }
        
        } catch (Exception ex) 
        {
            ex.printStackTrace();
        }
    
          return mailer;  
   }

    /**
     * Based on the ACTION code, determine how to process the email request.
     *
     * @param Object class in which the condition was detected.
     * @param String method in which the condition was detected.
     * @param msg    message to be sent regarding the condition.
     * @param action An arbitrary 'action' code which indicates how to
     *               process the request.
     *
     * @author York Davis
     **/
    public synchronized void send(Object object, String method, String msg, int action) 
    {
      getProperties();
			switch(action) 
			{ 
				case ACTION1: 
					performAction1(object, method, msg); 
					break; 
				case ACTION2: 
					performAction2(object, method, msg); 
					break; 
				case ACTION3: 
					performAction1(object, method, msg); 
          performAction2(object, method, msg); 
					break; 
			} 
    }

    /**
     * Based on the ACTION code, determine how to process the email request.
     *
     * @param Object class in which the condition was detected.
     * @param String method in which the condition was detected.
     * @param int the error code
     * @param String[] the error message parameters
     * @param action An arbitrary 'action' code which indicates how to
     *               process the request.
     *
     * @author York Davis
     **/
    public synchronized void send(Object object, String method, int errorCode, String[] errorString, int action) 
    {
      send(object,
           method,
           ErrorCodeStore.getInstance().findValue(errorCode, errorString),
           action);
    }
    
    /**
     * Currently, action '1' send an email.
     *
     * @param Object class in which the condition was detected.
     * @param String method in which the condition was detected.
     * @param msg    message to be sent regarding the condition.
     *
     * @author York Davis
     **/
    private void performAction1(Object object, String method, String msg)
    {
      String msgtext =  "Class   : " + getObject(object) + "\n" +
                        "method  : " + method + "\n" + 
                        "time    : " + new java.util.Date().toString() + "\n" +
                        "server  : " + localHost + "\n\n" +
                        "message : " + msg;
                        
      mailMessage = new MailMessageVO();
      mailMessage.setTo(emailTo);
      mailMessage.setFrom(emailFrom);
      mailMessage.setSubject(emailSubject);
      mailMessage.setMsg(msgtext);

      try
      {
        mailManager.sendMsg(mailMessage);
      }
      catch (Throwable t)
      {
      }
    }

    /**
     * Currently, action '2' sends a page.
     *
     * @param Object class in which the condition was detected.
     * @param String method in which the condition was detected.
     * @param msg    message to be sent regarding the condition.
     *
     * @author York Davis
     **/
    private void performAction2(Object object, String method, String msg)
    {
      String msgtext =  "Class   : " + getObject(object) + "\n" +
                        "method  : " + method + "\n" + 
                        "time    : " + new java.util.Date().toString() + "\n" +
                        "server  : " + localHost + "\n\n" +
                        "message : " + msg;
                        
      mailMessage = new MailMessageVO();
      mailMessage.setTo(emailTo);
      mailMessage.setFrom(pageFrom);
      mailMessage.setSubject(pageSubject);
      mailMessage.setMsg(msgtext);

      try
      {
        mailManager.sendMsg(mailMessage);
      }
      catch (Throwable t)
      {
      }
    }

    /**
     * Converts the class into a string representation of itself.
     * Unless the object is an instance of java.lang.String in which
     * case it just returns the String value.
     *
     * @return The B2BMail object.
     *
     * @author York Davis
     **/
     private String getObject(Object object)
     {
      if (object instanceof java.lang.String)
      {
          return (String) object;
      }
      else
      {
        return object.getClass().toString().substring(6);
      }
    }

  /**
   * Get properties from properties file. This is done each time the
   * send() method is called so that if the properties change, the
   * latest properties will be used.
   *
   * @author York Davis
   **/
  private void getProperties()
  {
      try {
          emailTo        = cu.getFrpGlobalParm(B2B_ARIBA_CONFIG_CONTEXT, PROPERTY_EMAIL_TO);
          
          emailFrom      =  resourceManager.getProperty(PROPERTY_B2B_PROPERTY_FILE, 
                                                        PROPERTY_EXCEPTION_EMAIL_FROM);       
          emailSubject   =  resourceManager.getProperty(PROPERTY_B2B_PROPERTY_FILE, 
                                                        PROPERTY_EXCEPTION_EMAIL_TITLE);   
          pageFrom       =  resourceManager.getProperty(PROPERTY_B2B_PROPERTY_FILE, 
                                                        PROPERTY_EXCEPTION_PAGE_FROM);       
          pageSubject    =  resourceManager.getProperty(PROPERTY_B2B_PROPERTY_FILE, 
                                                        PROPERTY_EXCEPTION_PAGE_TITLE);
      } catch (Exception e) {
          logger.error("Failed to get properties. " + e);
      }
  }
}