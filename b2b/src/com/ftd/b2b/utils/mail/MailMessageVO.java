package com.ftd.b2b.utils.mail;

/**
 * Value object for sending mail messages.
 *
 * @author York Davis
 * @version 1.0 
 **/
public class MailMessageVO 
 {
  private String from               = null;
  private String to                 = null;
  private String cc                 = null;
  private String bcc                = null;
  private String subject            = null;
  private String msg                = null;
  private String attachmentLocation = null;
  
  public MailMessageVO()
  {
  }

  public String getFrom()
  {
    return from;
  }

  public void setFrom(String from)
  {
    this.from = from;
  }

  public String getTo()
  {
    return to;
  }

  public void setTo(String to)
  {
    this.to = to;
  }

  public String getCc()
  {
    return cc;
  }


  public void setCc(String cc)
  {
    this.cc = cc;
  }

  public String getBcc()
  {
    return bcc;
  }

  public void setBcc(String bcc)
  {
    this.bcc = bcc;
  }

  public String getSubject()
  {
    return subject;
  }

  public void setSubject(String subject)
  {
    this.subject = subject;
  }

 public String getMsg()
  {
    return msg;
  }

  public void setMsg(String msg)
  {
    this.msg = msg;
  }

  public String getAttachmentLocation()
  {
    return attachmentLocation;
  }

  public void setAttachmentLocation(String attachmentLocation)
  {
    this.attachmentLocation = attachmentLocation;
  }

}