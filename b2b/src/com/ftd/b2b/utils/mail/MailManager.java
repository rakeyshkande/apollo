package com.ftd.b2b.utils.mail;

import com.ftd.b2b.utils.mail.MailMessageVO;
import javax.activation.*;
import java.io.*;
import java.net.InetAddress;
import java.util.Properties;
import java.util.Date;
import javax.mail.*;
import javax.mail.internet.*;
import com.ftd.b2b.constants.B2BConstants;

import com.ftd.osp.utilities.plugins.Logger;

/**
 * Implementation class to send a JavaMail message.
 *
 * @author York Davis
 * @version 1.0 
 **/
public class MailManager implements B2BConstants
{
  private static final String HOST         = "mail.smtp.host";
  private static final String MAILER_TYPE  = "X-Mailer";
  private static final String MAILER_VALUE = "MailManagerImpl"; 
  private String logLocation;
  private Message msg;
  private Logger logManager;

  /**
    * MailManagerImpl constructor. In order that this class might be shared across
    * multiple projects, this class passes in two parameters to the constructor.
    * Otherwise, these params would normally be pulled from properties files.
    * 
    * @param  The Log descriptor
    * @param  The Mail server location.
    *
    * @author York Davis
    **/
  public MailManager(String logLocation, String sMailServer) 
  {


      logManager = new Logger(logLocation);
      logManager.debug("New instance of MailManager Created.");

  
    try{
        //Properties props = System.getProperties();
        Properties props = new Properties();
        props.put(HOST, sMailServer);
        Session session = Session.getDefaultInstance(props, null);
        msg = new MimeMessage(session);
        System.out.println("Fine");
      }
      catch(Throwable t){
        System.out.println("Error Creating Session");
        logManager.error("MailManger()-OLDCODE:" + t.toString());            
      }


  }

  /**
    * Send the mail message. This method essentially wraps the call
    * to the send() method to capture any exceptional conditions.
    * 
    * @param  The Mail message value object.
    *
    * @author York Davis
    * @throws Exception if any error condition occurs.
    **/
  public synchronized void sendMsg(MailMessageVO mailMsg) throws Exception
  {
      try
      {
        send(mailMsg);
      }
      catch (Throwable throwable)
      {
          logManager.error("Mail send failed in MailManager.send() : " + throwable.toString());
          throw new Exception(throwable.toString());
      }
  }

  /**
    * Send the mail message. 
    * 
    * @param  The Mail message value object.
    *
    * @author York Davis
    * @throws Exception if any error condition occurs.
    **/
  private void send(MailMessageVO mailMsg) throws Exception
  {
    msg.setFrom(new InternetAddress(mailMsg.getFrom()));
    msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mailMsg.getTo(), false));
    msg.setSubject(mailMsg.getSubject());    
    msg.setText(mailMsg.getMsg());
    msg.setHeader(MAILER_TYPE, MAILER_VALUE);
    msg.setSentDate(new Date());    

    String cc = mailMsg.getCc();
    if (cc != null)
    {
     	msg.setRecipients(Message.RecipientType.CC, InternetAddress.parse(cc, false));  
    }
    
    String bcc = mailMsg.getBcc();
    if (bcc != null)
    {
    	msg.setRecipients(Message.RecipientType.BCC, InternetAddress.parse(bcc, false));
    }

    String attachmentLocation = mailMsg.getAttachmentLocation();
    if (attachmentLocation != null)
    {
        processAttachment(attachmentLocation, mailMsg.getMsg());
    }

    Transport.send(msg);
  }

  /**
    * Process the attachment to the mail.
    * 
    * @param  The attachment location.
    * @param  The attachment message text.
    *
    * @author York Davis
    **/
  private void processAttachment(String attachmentLocation, String sMsgText) throws MessagingException
  {
     // create and fill the first message part
     MimeBodyPart mbp1 = new MimeBodyPart();
     mbp1.setText(sMsgText);
     // create the second message part
     MimeBodyPart mbp2 = new MimeBodyPart();
     // attach the file to the message
     FileDataSource fds = new FileDataSource(attachmentLocation);
     mbp2.setDataHandler(new DataHandler(fds));
     mbp2.setFileName(attachmentLocation);

     // create the Multipart
     //and its parts to it
     Multipart mp = new MimeMultipart();
     mp.addBodyPart(mbp1);
     mp.addBodyPart(mbp2);
     // add the Multipart to the message
     msg.setContent(mp);
  }

  /**
    * Converts this class into a string.
    * 
    * @author York Davis
    **/
   public String toString()
  {
    return this.getClass().toString();
  }

  /**
    * Determine equality of this object to another.
    * 
    * @param  The object to be compared.
    *
    * @author York Davis
    **/
  public boolean equals(Object o)
  {
    return this == o;
  }

  /**
    * Saves state. Currently unused.
    * 
    * @author York Davis
    **/
  public void save()
  {
  }

  /**
    * Builds state. Currently unused.
    * 
    * @author York Davis
    **/
  public void create()
  {
  }
  
}