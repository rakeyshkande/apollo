package com.ftd.b2b.utils.misc;

import com.ftd.b2b.constants.B2BConstants;
import com.ftd.b2b.common.utils.HTTPProxy;
import java.net.*;
import java.io.*;
import java.util.*;

// Forces B2B threads and server to reload client options singleton from DB.
// Since the B2B standalone threads and server application have their own 
// singleton for client options, both must be signalled to reload.  For the
// threads, simply send reload message to host/port.  For the server, invoke
// the Ping servlet with parameter to force reload.
//
public class ReloadClientOptions implements B2BConstants
{
	public static void main(String[] args) throws Exception {
    if (args.length != 3) {
      System.out.println("Syntax: java ReloadClientOptions [host] [port] [url]");
      System.out.println("    host - Host that thread is running on");
      System.out.println("    port - Port that thread is listening to");
      System.out.println("    url  - URL of Ping servlet (e.g., http://b2btest.ftdi.com/aribatest/Ping)");
      return;
    }
    String host = args[0];
    int port    = Integer.parseInt(args[1]);
    String url  = args[2];
    new ReloadClientOptions().doIt(host, port, url);
	}

	private void doIt(String host, int port, String url) throws Exception {
    System.out.println("Starting");
    Socket socket = null;
    PrintWriter out = null;

    System.out.println("Connecting to thread socket");
    socket = new Socket(host, port);
    out = new PrintWriter(socket.getOutputStream(), true);

    System.out.println("Signalling B2B threads to reload client options");
    out.println(CONSTANT_RELOAD_CLIENT_OPTIONS);
    out.close();
    socket.close();

    System.out.println("Signalling B2B server to reload client options");
    Map params = new HashMap();
    params.put(CONSTANT_RELOAD_CLIENT_OPTIONS, "true");
    HTTPProxy.send(url, params);
    
    System.out.println("Send Successful!");
  }
}