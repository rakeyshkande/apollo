package com.ftd.b2b.constants;

public interface B2BDatabaseConstants 
{

  // Common Columns
  public static final String B2B_BUYER_COOKIE = "Buyer_Cookie";
  public static final String B2B_ASN_NUMBER = "ASN_Number";
  public static final String B2B_PAYLOAD_ID = "Payload_ID";
  public static final String B2B_CREATED_DATE = "Created_Date";
  public static final String B2B_FTD_ORDER_NUMBER = "FTD_Order_Number";
  public static final String B2B_TAX_AMOUNT = "Tax_Amount";
  public static final String B2B_ORDER_TOTAL = "Order_Total";
  public static final String B2B_OCCASION = "Occasion";
  public static final String B2B_ITEM_NUMBER = "Item_Number";
  public static final String B2B_ITEM_DESCRIPTION = "Item_Description";
  public static final String B2B_ITEM_PRICE = "Item_Price";
  public static final String B2B_ADDON_COUNT = "Add_On_Count";
  public static final String B2B_ADDON_DETAIL = "Add_On_Detail";
  public static final String B2B_SERVICE_FEE = "Service_Fee";
  public static final String B2B_DELIVERY_DATE = "Delivery_Date";
  public static final String B2B_MASTER_ORDER_NUMBER = "Master_Order_Number";
  public static final String B2B_UNSPSC_CODE = "UNSPSC_Code";
  public static final String B2B_COLOR_SIZE = "Color_Size";

  //FTD Transmission table
  public static final String B2B_FTD_TRANSMISSION_SEQ_NUMBER = "FTD_SEQUENCE_NUMBER";
  public static final String B2B_FTD_TRANSMISSION  = "FTD_TRANSMISSION";
  public static final String B2B_FTD_TRANSMISSION_SENT  = "FTD_SENT";
  public static final String B2B_FTD_TRANSMISSION_SENDDATE    = "SEND_DATE";
  
  // Approved Tables Only Columns
  public static final String B2B_APPROVED_DATE = "Approved_Date";
  public static final String B2B_ARIBA_PO_NUMBER = "Ariba_PO_Number";
  public static final String B2B_ARIBA_LINE_NUMBER = "Ariba_Line_Number";
  public static final String B2B_BILL_TO_FIRST_NAME = "Bill_To_First_Name";
  public static final String B2B_BILL_TO_LAST_NAME = "Bill_To_Last_Name";
  public static final String B2B_BILL_TO_ADDRESS_LINE1 = "Bill_To_Address_Line1";
  public static final String B2B_BILL_TO_ADDRESS_LINE2 = "Bill_To_Address_Line2";
  public static final String B2B_BILL_TO_CITY = "Bill_To_City";
  public static final String B2B_BILL_TO_STATE = "Bill_To_State";
  public static final String B2B_BILL_TO_ZIP_CODE = "Bill_To_Zip_Code";
  public static final String B2B_BILL_TO_COUNTRY = "Bill_To_Country";
  public static final String B2B_BILL_TO_HOME_PHONE = "Bill_To_Home_Phone";
  public static final String B2B_BILL_TO_WORK_PHONE = "Bill_To_Work_Phone";
  public static final String B2B_BILL_TO_WORK_EXT = "Bill_To_Work_Ext";
  public static final String B2B_BILL_TO_FAX_NUMBER = "Bill_To_Fax_Number";
  public static final String B2B_BILL_TO_EMAIL = "Bill_To_Email";
  public static final String B2B_SHIP_TO_FIRST_NAME = "Ship_To_First_Name";
  public static final String B2B_SHIP_TO_LAST_NAME = "Ship_To_Last_Name";
  public static final String B2B_SHIP_TO_ADDRESS_LINE1 = "Ship_To_Address_Line1";
  public static final String B2B_SHIP_TO_ADDRESS_LINE2 = "Ship_To_Address_Line2";
  public static final String B2B_SHIP_TO_CITY = "Ship_To_City";
  public static final String B2B_SHIP_TO_STATE = "Ship_To_State";
  public static final String B2B_SHIP_TO_ZIP_CODE = "Ship_To_Zip_Code";
  public static final String B2B_SHIP_TO_COUNTRY = "Ship_To_Country";
  public static final String B2B_SHIP_TO_HOME_PHONE = "Ship_To_Home_Phone";
  public static final String B2B_SHIP_TO_WORK_PHONE = "Ship_To_Work_phone";
  public static final String B2B_SHIP_TO_WORK_EXT = "Ship_To_Work_Ext";
  public static final String B2B_SHIP_TO_FAX_NUMBER = "Ship_To_Fax_Number";
  public static final String B2B_SHIP_TO_EMAIL = "Ship_To_Email";  
  public static final String B2B_SHIP_TO_BUSINESS_TYPE = "Ship_To_Business_Type";  
  public static final String B2B_SHIP_TO_BUSINESS_NAME = "Ship_To_Business_Name";  
  public static final String B2B_CARD_MESSAGE = "Card_Message";
  public static final String B2B_SPECIAL_INSTRUCTIONS = "Special_Instructions";
    public static final String B2B_RETAIL_PRICE = "Retail_Price";
  public static final String B2B_ARIBA_COST_CENTER = "Ariba_Line_Cost_Center";
  public static final String B2B_AMS_PROJECT_CODE = "AMS_Project_Code";

  public static final String B2B_SHIP_TO_PHONE = "Ship_To_phone";
  public static final String B2B_SHIP_TO_EXT = "Ship_To_Ext";

  // Archive Tables Only Columns
  public static final String B2B_ARCHIVED_DATE = "Archived_Date";

  // Client URL Columns
  public static final String B2B_CLIENT_NAME = "Client_Name";
  public static final String B2B_URL = "URL";
  public static final String B2B_SOURCE_CODE = "Source_Code";

  // Add On Columns
  public static final String B2B_ADDON_ID = "AddOn_ID";
  public static final String B2B_ADDON_TYPE = "AddOn_Type";
  public static final String B2B_ADDON_DESCRIPTION = "Description";
  public static final String B2B_ADDON_PRICE = "Price";
  public static final String B2B_ADDON_UNSPSC = "UNSPSC";

  // Order Delete Transmission Columns and Constants
  public static final String B2B_SEND_DATE = "Send_Date";

  // Change/Cancel Transmssion Columns
  public static final String B2B_CHANGE_CANCEL_SEQUENCE = "socket_sequence_number";
  public static final String B2B_CHANGE_CANCEL_MSG      = "socket_value";

  //HP Order Transmission Columns
  public static final String B2B_HP_SOCKET_VALUE = "HP_Socket_Value";
  public static final String B2B_HP_SEQ_NUMBER = "HP_Sequence_Number";

  // Client Options Columns
  public static final String B2B_CLIENT_OPTS_ASN = "asn_number";
  public static final String B2B_CLIENT_OPTS_UNSPSC = "unspsc";
  public static final String B2B_CLIENT_OPTS_SVC_FEE_UNSPSC = "service_fee_unspsc";
  public static final String B2B_CLIENT_OPTS_TAX_UNSPSC = "tax_unspsc";
 
 }