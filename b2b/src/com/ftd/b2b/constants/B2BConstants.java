package com.ftd.b2b.constants;

public interface B2BConstants 
{
//  log categories
  public static final String LOG_CATEGORY_JAVAMAIL = "com.ftd.b2b.EMail";
  public static final String LOG_CATEGORY_LINE_ITEM_RECEIVER = "com.ftd.b2b.ReceiveShoppingCart";  
  public static final String LOG_CATEGORY_ORDER_ASSEMBLER = "com.ftd.b2b.ReceiveShoppingCart";  
  public static final String LOG_CATEGORY_ORDER_PROCESSOR = "com.ftd.b2b.ReceiveShoppingCart";    
  public static final String LOG_CATEGORY_PUNCH_OUT_REQUEST = "com.ftd.b2b.ReceiveShoppingCart";    
  public static final String LOG_CATEGORY_REQUEST_AUTHENTICATOR = "com.ftd.b2b.RequestAuthenticator";      
  public static final String LOG_CATEGORY_REQUEST_DATA_MINER = "com.ftd.b2b.CXMLDataMinder";        
  public static final String LOG_CATEGORY_RECEIVE_APPROVED_PO = "com.ftd.b2b.ReceiveApprovedPO";  
  public static final String LOG_CATEGORY_PUNCHOUT_ORDER_PROCESSOR = "com.ftd.b2b.Punchout";
  public static final String LOG_CATEGORY_ADDON_DAO = "com.ftd.b2b.DataAccessObject";
  public static final String LOG_CATEGORY_PENDING_DAO = "com.ftd.b2b.DataAccessObject";  
  public static final String LOG_CATEGORY_PENDING_LINE_DAO = "com.ftd.b2b.DataAccessObject";  
  public static final String LOG_CATEGORY_URLLOOKUP_DAO = "com.ftd.b2b.DataAccessObject";    
  public static final String LOG_CATEGORY_APPROVED_DAO = "com.ftd.b2b.DataAccessObject";  
  public static final String LOG_CATEGORY_APPROVED_LINE_DAO = "com.ftd.b2b.DataAccessObject";  
  public static final String LOG_CATEGORY_COUNTRYCODE_DAO = "com.ftd.b2b.DataAccessObject";
  public static final String LOG_CATEGORY_CATALOGORDERNUMBER_DAO = "com.ftd.b2b.DataAccessObject";
  public static final String LOG_CATEGORY_MASTERORDERNUMBER_DAO = "com.ftd.b2b.DataAccessObject";  
  public static final String LOG_CATEGORY_PUNCHOUTORDERNUMBER_DAO = "com.ftd.b2b.DataAccessObject";
  public static final String LOG_CATEGORY_PENDING_BUYERCOOKIE_DAO = "com.ftd.b2b.DataAccessObject";
  public static final String LOG_CATEGORY_LINEITEMAPARSER = "com.ftd.b2b.ReceiveShoppingCart";
  public static final String LOG_CATEGORY_PUNCHOUT_SERVLET = "com.ftd.b2b.Punchout";  
  public static final String LOG_CATEGORY_RECEIEVEAPPROVEDPO_SERVLET = "com.ftd.b2b.ReceiveApprovedPO";    
  public static final String LOG_CATEGORY_SOCKET_SENDER = "com.ftd.b2b.SocketSender";    
  public static final String LOG_CATEGORY_CATALOG_ORDER_PROCESSOR = "com.ftd.b2b.ReceiveApprovedPO";    
  public static final String LOG_CATEGORY_ORDERDELETETRANS_DAO = "com.ftd.b2b.DataAccessObject";
  public static final String LOG_CATEGORY_ORDERENTRY_DAO = "com.ftd.b2b.DataAccessObject";  
  public static final String LOG_CATEGORY_BUYERSOURCECODELOOKUP_DAO = "com.ftd.b2b.DataAccessObject";
  public static final String LOG_CATEGORY_UNSPSCLOOKUP_DAO = "com.ftd.b2b.DataAccessObject";
  public static final String LOG_CATEGORY_ORDERDATAPURGER = "com.ftd.b2b.DataPurge";
  public static final String LOG_CATEGORY_CHANGECANCELTRANSMISSION_DAO = "com.ftd.b2b.DataAccessObject";  
  public static final String LOG_CATEGORY_CHANGECANCELSENDER = "com.ftd.b2b.ChangeCancelSender";    
  public static final String LOG_CATEGORY_LINE_ITEM_SENDER = "com.ftd.b2b.ReceiveShoppingCart";      
  public static final String LOG_CATEGORY_B2B_STARTER = "com.ftd.b2b.ReceiveShoppingCart";      
  public static final String LOG_CATEGORY_HPORDERTRANS_DAO = "com.ftd.b2b.DataAccessObject";      
  public static final String LOG_CATEGORY_HPSENDER = "com.ftd.b2b.HPOrderSender";
  public static final String LOG_CATEGORY_FTDORDERTRANS_DAO = "com.ftd.b2b.DataAccessObject";   
  public static final String LOG_CATEGORY_FTDSENDER = "com.ftd.b2b.ReceiveApprovedPO"; 
  public static final String LOG_CATEGORY_CLIENTOPTIONS_DAO = "com.ftd.b2b.DataAccessObject";

// properties
  public static final String B2B_ARIBA_CONFIG_CONTEXT = "B2B_ARIBA_CONFIG";
  public static final String PROPERTY_B2B_PROPERTY_FILE       = "b2b.configuration";
  public static final String PROPERTY_XMLPARSEEXCEPTION       = "XMLParseException";
  public static final String PROPERTY_INVALIDCLIENTEXCEPTION  = "InvalidClientException";  
  public static final String PROPERTY_INVALIDDUNSEXCEPTION    = "InvalidDUNSException";    
  public static final String PROPERTY_DUPLICATEAPPROVALEXCEPTION    = "DuplicateApprovalException";    
  public static final String PROPERTY_AUTHENTICATIONEXCEPTION = "AuthenticationException";
  public static final String PROPERTY_THROWABLEEXCEPTION = "ThrowableException";  
  public static final String PROPERTY_MAIL_SERVER                           = "mailServer";
  public static final String PROPERTY_SHARED_SECRET                         = "sharedSecret";  
  public static final String PROPERTY_RECEIVE_SHOPPING_CART_PORT            = "receiveShoppingCartPort";
  public static final String PROPERTY_RECEIVE_SHOPPING_CART_URL            = "receiveShoppingCartURL";
  public static final String PROPERTY_EMAIL_TO    = "EmailTo";
  public static final String PROPERTY_EXCEPTION_EMAIL_FROM  = "ExceptionEmailFrom";
  public static final String PROPERTY_EXCEPTION_EMAIL_TITLE = "ExceptionEmailTitle";
  public static final String PROPERTY_EXCEPTION_PAGE_FROM  = "ExceptionPageFrom";
  public static final String PROPERTY_EXCEPTION_PAGE_TITLE  = "ExceptionPageTitle";
  public static final String PROPERTY_RECEIVE_SHOPPING_THREAD_TIMEOUT = "ShoppingCartThreadTimeOutMS";
  public static final String PROPERTY_RECEIVE_SHOPPING_THREAD_SLEEP = "ShoppingCartThreadSleepMS";
  public static final String PROPERTY_ORDER_CANCEL_CHANGE_SOCKET_IP = "OrderCancelChangeSocketIP";
  public static final String PROPERTY_ORDER_CANCEL_CHANGE_SOCKET_PORT = "OrderCancelChangeSocketPort";  
  public static final String PROPERTY_NOVATOR_PURGE_SHOPPING_CART_SOCKET_IP = "NovatorPurgeShoppingCartSocketIP";    
  public static final String PROPERTY_NOVATOR_PURGE_SHOPPING_CART_SOCKET_PORT = "NovatorPurgeShoppingCartSocketPort"; 
  public static final String PROPERTY_CATALOG_SERVICE_FEE = "CatalogServiceFee";     
  public static final String PROPERTY_CXMLHEADER = "cXMLHeader";       
  public static final String PROPERTY_ORDER_ENTRY_USERID = "OrderEntryUserId";
  public static final String PROPERTY_ORDER_ENTRY_ADDRESS_TYPE = "OrderEntryAddressType";
  public static final String PROPERTY_ORDER_ENTRY_STATUS = "OrderEntryStatus";  
  public static final String PROPERTY_SHOPPING_CART_HOLD_DAYS = "ShoppingCartHoldDays";
  public static final String PROPERTY_DEFAULT_SOURCE_CODE = "DefaultSourceCode";
  public static final String PROPERTY_CHANGE_CANCEL_SENDER_SLEEP_INTERVAL = "ChangeCancelSenderSleepInterval";
  public static final String PROPERTY_ERROR_NOTIFIER_SLEEP_INTERVAL = "ErrorNotifierSleepInterval";  
  public static final String PROPERTY_MASTER_ORDER_PREFIX = "MasterOrderPrefix";
  public static final String PROPERTY_PUNCHOUT_ORDER_PREFIX = "PunchoutOrderPrefix";
  public static final String PROPERTY_CATALOG_ORDER_PREFIX = "CatalogOrderPrefix";
  public static final String PROPERTY_FTD_DUNS_NUMBER = "DUNS";
  public static final String PROPERTY_RECEIVE_SHOPPING_CART_SERVER_URL = "ShoppingCartURL";
  public static final String PROPERTY_FTD_ASN_NUMBER = "ASNNumber";
  public static final String PROPERTY_SSL_CLASS_NAME = "SSLClassName";
  public static final String PROPERTY_PROXY_SERVER = "ProxyServer";
  public static final String PROPERTY_PROXY_SERVER_PORT = "ProxyServerPort";
  public static final String PROPERTY_ASN_ADDON_UNSPSC_DEFAULTERS = "ASNADDONDEFAULT";
  public static final String PROPERTY_ASN_ADDON_UNSPSC_DEFAULT = "ADDONDEFAULT"; 
  public static final String PROPERTY_8_DIGIT_UNSPSC_CODE_LIST = "8DIGITUNSPSCCODE";
  public static final String PROPERTY_ORDER_TRANSMISSION_METHOD ="OrderTransmissionMethod";
  public static final String PROPERTY_ORDER_PROCESSING_ADDRESS ="OrderProcessingAddress";
  public static final String PROPERTY_TRANSACTION_TIMEOUT = "UserTransactionTimeout";
  public static final String PROPERTY_SHOPPING_RESP_HAS_SHIP_TO_INFO = "ShoppingResponseHasShipToInfo";

  public static final String PROPERTY_ASN_TARGET  = "ASN_TARGET";  // Target Stores
  public static final String PROPERTY_ASN_ACS     = "ASN_ACS";     // American Cancer Society
  public static final String PROPERTY_ASN_CSCHWAB = "ASN_CSCHWAB"; // Charles Schwab
  public static final String PROPERTY_ASN_MLYNCH  = "ASN_MLYNCH";  // Merrill Lynch
  public static final String PROPERTY_ASN_SAKS    = "ASN_SAKS";    // Saks
  public static final String PROPERTY_ASN_NSC     = "ASN_NSC";     // National Semiconductor
  public static final String PROPERTY_ASN_EDS     = "ASN_EDS";     // EDS
  
// interfaces
  public static final String INTERFACE_FTDORDERTRANSMISSIONDAO = "FTDOrderTransmissionDAO";
  public static final String INTERFACE_PUNCHOUTREQUEST          = "PunchOutRequest";
  public static final String INTERFACE_RECEIVEAPPROVEDPO          = "ReceiveApprovedPO";  
  public static final String INTERFACE_REQUESTDATAMINER   = "RequestDataMiner";
  public static final String INTERFACE_REQUESTAUTHENTICATOR   = "RequestAuthenticator";  
  public static final String INTERFACE_ORDERPROCESSOR =       "OrderProcessor";
  public static final String INTERFACE_ADDONDAO =       "AddOnDAO";
  public static final String INTERFACE_PENDINGDAO =       "PendingDAO";
  public static final String INTERFACE_PENDINGLINEDAO =       "PendingLineDAO";  
  public static final String INTERFACE_APPROVEDDAO =       "ApprovedDAO";
  public static final String INTERFACE_APPROVEDLINEDAO =       "ApprovedLineDAO";
  public static final String INTERFACE_URLLOOKUPDAO   = "URLLookupDAO";
  public static final String INTERFACE_APPROVEDRESPONSEGENERATOR   = "ApprovedResponseGenerator";
  public static final String INTERFACE_CATALOGORDERPROCESSOR   = "CatalogOrderProcessor";
  public static final String INTERFACE_PUNCHOUTORDERPROCESSOR   = "PunchOutOrderProcessor";
  public static final String INTERFACE_CHANGECANCELPROCESSOR = "ChangeCancelProcessor";
  public static final String INTERFACE_PUNCHOUTORDERNUMBERDAO = "PunchoutOrderNumberDAO";
  public static final String INTERFACE_CATALOGORDERNUMBERDAO = "CatalogOrderNumberDAO";
  public static final String INTERFACE_MASTERORDERNUMBERDAO = "MasterOrderNumberDAO";
  public static final String INTERFACE_COUNTRYCODEDAO = "CountryCodeDAO";
  public static final String INTERFACE_ORDERDELETETRANSMISSIONDAO = "OrderDeleteTransmissionDAO";
  public static final String INTERFACE_BUYERSOURCECODELOOKUPDAO   = "BuyerSourceCodeLookupDAO";
  public static final String INTERFACE_CHANGECANCELDAO = "ChangeCancelTransmissionDAO";
  public static final String INTERFACE_UNSPSCLOOKUPDAO = "UnspscLookupDAO";
  public static final String INTERFACE_CLIENTOPTIONSDAO = "ClientOptionsDAO";

// constants
  public static final String CONSTANT_PING_VALUE = "PONG";
  public static final String CONSTANT_PING = "PING";
  public static final String CONSTANT_SHARED_SECRET = "SharedSecret";
  public static final String CONSTANT_FTD_DUNS_NUMBER = "DUNS";
  public static final String CONSTANT_FTD_INTERNAL_SUPPLIER_ID = "internalsupplierid";
  public static final String CONSTANT_PAYLOAD_ID    = "payloadID";  
  public static final String CONSTANT_ASN_NUMBER    = "Identity";  
  public static final String CONSTANT_CXML          = "cXML";  
  public static final String CONSTANT_BUYER_COOKIE  = "BuyerCookie"; 
  public static final String CONSTANT_ORIGINAL_BUYER_COOKIE = "OrigBuyerCookie";
  public static final String CONSTANT_BROWSER_POST_FORM  = "BrowserFormPost";
  public static final String CONSTANT_URL  = "URL";  
  public static final String CONSTANT_OPERATION = "operation";
  public static final String CONSTANT_PUNCHOUT_SETUP_REQUEST = "PunchOutSetupRequest";
  public static final String CONSTANT_REQUEST = "Request";
  public static final String CONSTANT_SUPPLIER_PART_AUXILARY_ID = "SupplierPartAuxiliaryID";
  public static final String CONSTANT_ITEM_OUT = "ItemOut";

  public static final String CONSTANT_FTD_ORDER_PROCSSING_HTTP = "HTTP";
  public static final String CONSTANT_SOCKET = "SOCKET";
  public static final String SUCCESS_FTD_RESPONSE = "SUCCESS";
  public static final String ERROR_FTD_RESPONSE = "ERROR";
  public static final String ORDER_TRANSMISSION_SUCCESS = "Y";
  public static final String ORDER_TRANSMISSION_ERROR = "E";
  public static final String ORDER_TRANSMISSION_NOT_SENT = "N";
  public static final String ORDER_PROCESSING_ORDER_PARAM = "Order";
  public static final String CONSTANT_FTD_URL_CONTENT_TYPE = "application/x-www-form-urlencoded";
  public static final String DOLLAR_AMOUNT_FORMAT = "0.00";
  public static final String CATALOG_SOURCE_CODE = "CAT";
  public static final String PUNCHOUT_SOURCE_CODE = "ARI";
  
  public static final String CONSTANT_RAW_DATA_EMPTY_FIELD_FOR_DATE = "//";
  public static final String CONSTANT_SHOPPING_CART_DATE_FORMAT = "EEE MMM d hh:mm:ss z yyyy";
  public static final char CONSTANT_CATALOG_FILE_DELIMITER_SINGLE = '~';
  public static final String CONSTANT_RAW_DATA_FIELD_DELIMITER = "~~";
  public static final String CONSTANT_RAW_LINE_ITEM_DELIMITER = "LINE_ITEM_DELIMITER";
  public static final String CONSTANT_ORDER_NUMBER_DIVIDER = "/";
  public static final String CONSTANT_RAW_DATA_EMPTY_FIELD = "-";
  public static final String CONSTANT_DEFAULT_FOL_INDICATOR = "FTD";
  public static final String CONSTANT_HP_ORDER_DATE_FORMAT = "EEE MMM d hh:mm:ss z yyyy";
  public static final String CONSTANT_HP_DELIVERY_DATE_FORMAT = "MM/dd/yyyy";
  public static final String CONSTANT_HP_CC_EXPIRATION_DATE_FORMAT = "MM/yyyy";
  public static final String CONSTANT_RECIPIENT_CHANGE_SUBTYPE = "RN";
  public static final String CONSTANT_DELIVERYDATE_CHANGE_SUBTYPE = "DD";
  public static final String CONSTANT_PRODUCT_CHANGE_SUBTYPE = "PR";
  public static final String CONSTANT_OTHER_CHANGE_SUBTYPE = "OT";
  public static final String CONSTANT_CHANGE_CANCEL_SOCKET_BEGIN_IDENTIFIER = "CSERV";
  public static final String CONSTANT_CHANGE_SOCKET_TYPE = "MO";
  public static final String CONSTANT_CANCEL_SOCKET_TYPE = "CX";
  public static final String CONSTANT_CHANGE_CANCEL_SOCKET_END_IDENTIFIER = "EOF";
  public static final String CONSTANT_CHANGE_CANCEL_DATE_FORMAT = "yyyy/MM/dd";
  public static final String CONSTANT_ITEM_CHANGE_ORIGINAL_COMMENT = "Original Item Quantity: 1  ";
  public static final String CONSTANT_ITEM_CHANGE_NEW_COMMENT = "New Item Quantity: ";
  public static final String CONSTANT_ADDON_CHANGE_ORIGINAL_COMMENT = "Original Add-On Quantity: ";
  public static final String CONSTANT_ADDON_CHANGE_NEW_COMMENT = "  New Add-On Quantity: ";
  public static final String CONSTANT_NEWADDON_CODE_CHANGE_COMMENT = "  New Add-On Code: ";
  public static final String CONSTANT_NEWADDON_QUANTITY_CHANGE_COMMENT = "  New Add-On Quantity: ";
  public static final String CONSTANT_UNATTACHED_CHANGE_COMMENT = "Unattached change order from Ariba for PO Number: ";
  public static final String CONSTANT_DEFAULT_CARD_MSG_TO = "TO: ";
  public static final String CONSTANT_DEFAULT_CARD_MSG_FROM = "  FROM: ";
  public static final String CONSTANT_DEFAULT_CARD_MSG_SPACE = " ";
  public static final String CONSTANT_TRANSACTION_DATE_FORMAT="yyyyMMddHHmmsszzz";
  public static final String CONSTANT_FTD_TRANSACTION_DATE_FORMAT="EEE MMM dd HH:mm:ss zzz yyyy";

  public static final String HP_XML_HEADER = "<?xml version='1.0' encoding='utf-8'?>";

  public static final String CONSTANT_ARIBA_PARAMETER_KEY = "cxml-urlencoded";
  public static final String CONSTANT_ARIBA_URL_CONTENT_TYPE = "application/x-www-form-urlencoded";

  public static final String CONSTANT_ADDONFLAG = "ADDON";                        
  public static final String CONSTANT_ADDON_QUANTITY_DELIMITER = ":";
  public static final String CONSTANT_ADDON_ITEM_DELIMITER = ",";
  public static final String CONSTANT_DATABASE_TRUE = "True";
  public static final String CONSTANT_PUNCHOUT_ORDER_PREFIX = "ARS";
  public static final String CONSTANT_PO_RECEIVED_SUCCESS_CODE = "200";
  public static final String CONSTANT_PO_RECEIVED_SUCCESS_TEXT = "OK";

  public static final String CONSTANT_NEW_PO_FLAG = "New";
  public static final String CONSTANT_CHANGE_PO_FLAG = "Edit";
  public static final String CONSTANT_CANCEL_PO_FLAG = "Cancel";
  public static final String CONSTANT_UPDATE_PO_FLAG = "Update";
  public static final String CONSTANT_APPROVEDPO_DATE_TOKENS = "T+";
  public static final String CONSTANT_APPROVEDPO_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
  public static final String CONSTANT_STYPE_PREFIX = "S";
  // Next two should match entries in ftd_apps.occasion table
  public static final String CONSTANT_STYPE_OCCASION_CODE = "1";    // Funeral
  public static final String CONSTANT_NONSTYPE_OCCASION_CODE = "8"; // Other
  
  public static final int CONSTANT_MAX_SHIP_TO_BUS_TYPE = 20;
  public static final int CONSTANT_MAX_SHIP_TO_BUS_NAME = 50;

  public static final int CONSTANT_PUNCHOUT_CUSTOMER = 1;
  public static final int CONSTANT_CATALOG_CUSTOMER = 2;

  public static final int CONSTANT_HEADER_SHIP_TO = 1;
  public static final int CONSTANT_LINE_SHIP_TO = 2;

  public static final String CONSTANT_ITEM_OF_WEEK_FLAG_DEFAULT = "N";

  public static final String CONSTANT_MASTER_ORDER_NUMBER_SEPERATOR = "/";
  public static final String CONSTANT_SHOPPING_CART = "shoppingCart";  
  public static final String CONSTANT_CDATA_START = "<![CDATA[";
  public static final String CONSTANT_CDATA_END   = "]]>";
  public static final String CONSTANT_SERVICE_FEE_LINE_ITEM_NUM = "SVCFEE";
  public static final String CONSTANT_SERVICE_FEE_LINE_ITEM_DESC = "Service Fee";
  public static final String CONSTANT_TAX_LINE_ITEM_NUM = "TAXAMT";
  public static final String CONSTANT_TAX_LINE_ITEM_DESC = "Tax Amount";

  public static final String CONSTANT_RELOAD_CLIENT_OPTIONS = "ReloadClientOptions";

// error codes
  public static final int ERROR_CODE_UNABLETOLOADSERVICE = 1;
  public static final int ERROR_CODE_UNABLETOCONNECTTODB = 7;  
  public static final int ERROR_CODE_XMLPARSEEXCEPTION = 100;
  public static final int ERROR_CODE_INVALIDCLIENTEXCEPTION = 101;  
  public static final int ERROR_CODE_UNRECOVERABLE_EXCEPTION = 102;    
  public static final int ERROR_CODE_INVALIDCLIENT_EXCEPTION = 103;      
  public static final int ERROR_CODE_INVALIDSHAREDSECRET_EXCEPTION = 104;        
  public static final int ERROR_CODE_SENDSHOPPINGCARTTOARIBA = 105;        
  public static final int ERROR_CODE_DAO_EXCEPTION = 106;
  public static final int ERROR_CODE_CLONENOTSUPPORTED = 107;
  public static final int ERROR_CODE_ALLLINEITEMSNOTRECEIVED = 108;
  public static final int ERROR_CODE_SOCKETOPENSHOPPINGCART = 109;
  public static final int ERROR_CODE_SOCKETREADSHOPPINGCART  = 110;
  public static final int ERROR_CODE_PARSINGSHOPPINGCARTDATA = 111;
  public static final int ERROR_CODE_ADDONVALUENOTFOUND = 112;
  public static final int ERROR_CODE_INVALIDPOTYPE = 113;
  public static final int ERROR_CODE_SOCKETERROR = 114;
  public static final int ERROR_CODE_PARSINGORDERREQUEST = 115;
  public static final int ERROR_CODE_ORDERLINENOTFOUND = 116;
  public static final int ERROR_CODE_APPROVEDORDERNOTFOUND = 117;
  public static final int ERROR_CODE_ORDERENTRYINSERT = 118;
  public static final int ERROR_CODE_REQUESTLINESNOTEQUALEXISTING = 119;
  public static final int ERROR_CODE_VALUENOTNUMERIC = 120;
  public static final int ERROR_CODE_MASTERORDERNUMBERPARSE= 121;
  public static final int ERROR_CODE_CATALOGCONTAINSADDONS=122;
  public static final int ERROR_CODE_SENDSHOPPINGTOSERVLET=123;
  public static final int ERROR_CODE_SHOPPINGCART_BUYERCOOKIE_NOTFOUND=124;
  public static final int ERROR_CODE_VALUE_TOO_LARGE_FOR_COLUMN=125;

  // Clean column lengths
  public static final int MAX_LEN_ORDERNUM = 20;
  public static final int MAX_LEN_OCCASION = 30;
  public static final int MAX_LEN_ITEMNUM = 10;
  public static final int MAX_LEN_MASTER = 100;
  public static final int MAX_LEN_UNSPSC = 40;
  public static final int MAX_LEN_DESCRIPTION = 255;
  public static final int MAX_LEN_NAME = 100;
  public static final int MAX_LEN_ADDRESS = 45;
  public static final int MAX_LEN_CITY = 30;
  public static final int MAX_LEN_STATE = 20;
  public static final int MAX_LEN_ZIP = 10;
  public static final int MAX_LEN_COUNTRY = 3;
  public static final int MAX_LEN_PHONE = 20;
  public static final int MAX_LEN_EXTENSION = 10;
  public static final int MAX_LEN_EMAIL = 55;
  public static final int MAX_LEN_CARD = 1000;
  public static final int MAX_LEN_CCENTER = 40;
  public static final int MAX_LEN_AMSCODE = 20;

    }