package com.ftd.b2b.common.utils;

/**
 * This exception class is thrown in the condition where the ASN number
 * passed in with the cXML document is not a valid PunchOut Client.
 * Normally, this means that the ASN number is not on the URL Lookup table.
 *
 * @author York Davis
 * @version 1.0 
 **/
public class InvalidClientException extends Exception
{

   public InvalidClientException()
  {
    super();
  }
  
  public InvalidClientException(String e)
  {
    super(e);
  }

}