package com.ftd.b2b.common.utils;

import com.ftd.b2b.utils.mail.B2BMailer;
import com.ftd.b2b.constants.B2BConstants;
import com.ftd.osp.utilities.plugins.Logger;

import java.net.*;
import java.io.*;
import java.util.*;

/**
 * This abstract class can be used as a base class for any object
 * that needs to send String messages to a socket.
 *
 * @author York Davis
 * @version 1.0 
 **/
public abstract class SocketSender implements B2BConstants
{
  protected Logger logManager = new Logger(LOG_CATEGORY_SOCKET_SENDER); 
  private static final String IP_DEFAULT         = "localhost";
  private static final int    PORT_DEFAULT       = 4447;  

  protected Socket socket;
  protected String ip;
  protected int iport;
  
  public SocketSender()
  {
  }
  
  /**
   * Send a String message to the socket.
   *
   * @param Message to be sent.
   * @throws IOException if a socket error occurs.
   * @author York Davis
   **/
  public void send(String msg) throws IOException
  {
      socket = new Socket(ip, iport);      
      PrintWriter out = new PrintWriter(socket.getOutputStream(), true);  
      out.println(msg);
      out.close();
  }

  /**
   * Translate the String port from the properties file into an int
   * and handle any error conditions.
   *
   * @param  String port from properties.
   * @return int port to be used with the socket.
   * @author York Davis
   **/
  protected int processPort(String port)
  {
      int iport = PORT_DEFAULT;
      try
      {
          iport = Integer.parseInt(port);
      }
      catch (NumberFormatException nfe)
      {
           String msg = "Port address could not be extracted from properties or was not numeric. Using default of '" + PORT_DEFAULT + "'.";
           logManager.error("processPort: " + msg);            
           B2BMailer.getInstance().send(this, "processPort()", msg, B2BMailer.ACTION2);            
           iport = PORT_DEFAULT;
      }
      return iport;
  }

  /**
   * Translate the String IP from the properties file
   * and handle any error conditions.
   *
   * @param  String IP from properties.
   * @return IP to be used with the socket.
   * @author York Davis
   **/
  protected String processIp(String ip)
  {
      if (ip == null)
      {
           String msg = "IP could not be extracted from properties. Using default of '" + IP_DEFAULT + "'.";
           logManager.error("processPort: " + msg);            
           B2BMailer.getInstance().send(this, "processIp()", msg, B2BMailer.ACTION2);            
           ip = IP_DEFAULT;
      }
      return ip;
  }

}