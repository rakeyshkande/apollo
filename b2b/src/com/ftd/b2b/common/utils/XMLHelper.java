package com.ftd.b2b.common.utils;

import org.xml.sax.*;
import org.xml.sax.helpers.*;
import java.io.*;
import org.w3c.dom.*;

import com.sun.org.apache.xerces.internal.parsers.DOMParser;
import com.wutka.jox.*;

/**
 * Base class for interfacing with DOM Parser.
 *
 * @author York Davis
 * @version 1.0 
 **/
public class XMLHelper
{
  protected DOMParser parser = null;

 /**
  * Extract the first Text Node associated with a Tag name
  * for a given XML string.
  * 
  * @return The Text Node Value
  * @param  The XML string
  * @param  The Tag name
  * @throws XMLParseException if an error occurs parsing the document.
  *
  * @author York Davis
  **/
 public final String getTextValueByTagName(String sXML,
                                           String sTagName) throws XMLParseException
  {
    if (sXML == null || sTagName == null)
    {
        throw new XMLParseException("Input parameters are in error");      
    }

    parser = new DOMParser();

    try
    {
        parser.parse(new org.xml.sax.InputSource(new java.io.ByteArrayInputStream(sXML.getBytes())));
    }
    catch (Throwable ioe)
    {
        throw new XMLParseException("Error parsing XML document : " + ioe.toString());
    }

    NodeList nl = parser.getDocument().getElementsByTagName(sTagName);    
    if (nl == null) 
    {
      throw new XMLParseException("Could not locate '" + sTagName + "' in XML document");
    }

    Node node = nl.item(0);
    if (node == null) 
    {
      throw new XMLParseException("Could not locate '" + sTagName + "' in XML document");
    }

    if (node.getFirstChild().getNodeType() == Node.TEXT_NODE)
    {
      return node.getFirstChild().getNodeValue();
    }
    throw new XMLParseException("Node error finding " + sTagName);
  }
 /**
  * Extract the Attribute value associated with a Tag name
  * for a given XML string.
  * 
  * @return The Attribute Value
  * @param  The XML string
  * @param  The Tag name
  * @param  The Attribute name
  * @throws XMLParseException if an error occurs parsing the document.  
  *
  * @author York Davis
  **/
  public final String getAttributeByTagName(String sXML,
                                               String sTagName,
                      											   String sAttributeName) throws XMLParseException
  {
    if (sXML == null || sTagName == null || sAttributeName == null)
    {
  		throw new XMLParseException("Input parameters are in error");      
    }
  
  	parser = new DOMParser();

  	try
  	{
  	  	parser.parse(new org.xml.sax.InputSource(new java.io.ByteArrayInputStream(sXML.getBytes())));
  	}
  	catch (Throwable ioe)
  	{
  		throw new XMLParseException(ioe.toString());
  	}

  	NodeList nl = parser.getDocument().getElementsByTagName(sTagName);
    if (nl == null) 
    {
      throw new XMLParseException("Could not locate '" + sTagName + "' in XML document");
    }

    Node node = nl.item(0);
    if (node == null) 
    {
      throw new XMLParseException("Could not locate '" + sTagName + "' in XML document");
    }

  	NamedNodeMap nnm = node.getAttributes();
    if (nnm == null)
    {
  		throw new XMLParseException("Error parsing XML document.");    
    }

    node = nnm.getNamedItem(sAttributeName);
    if (node == null)
    {
      throw new XMLParseException("Could not locate '" + sAttributeName + "' in '" + sTagName + "'");      
    }
  	return node.getNodeValue();
  }
  
 /**
  * Translate all invalid XML characters.
  *
  * Creation date: (05/17/2001 1:21:05 PM)
  * @return java.lang.String
  */
  public static String encode(String val) 
  {
  	if(val != null) 
  	{ 
  		StringBuffer buf = new StringBuffer(val.length() + 8); 
  		char c; 
  		for(int i = 0; i < val.length(); i++) 
  		{
  			c = val.charAt(i); 
  			switch(c) 
  			{ 
  				case '<': 
  					buf.append("&#60;"); 
  					break; 
  				case '>': 
  					buf.append("&#62;"); 
  					break; 
  				case '&': 
  					buf.append("&#38;"); 
  					break; 
	  			case '\"': 
  					buf.append("&#034;"); 
  					break; 
  				case '\'': 
  					buf.append("&#039;"); 
  					break; 
  				case '\u00a9': 
  					buf.append("&#169;"); 
  					break; 
  				case '\u00ae': 
  					buf.append("&#174;"); 
  					break;                     
  				case '\u2122': 
  					buf.append("<SUP>TM</SUP>"); 
  					break;                    
  				default: 
  					buf.append(c); 
  					break; 
  			} 
  		} 
  		return buf.toString(); 
  	} 
  	else 
  	{ 
  		return new String(""); 
  	} 
  }

 /**
  * Creates an object from XML. The main document tag of the
  * XML must contain the fully qualified class name.
  *
  * Creation date: (07/12/2001 3:05:44 PM)
  * @return java.lang.Object
  * @param  String The XML to convert to an object
  */
  public Object demarshall(String sXML) throws Exception
  {
	  Object    oObject   = null;
  	Class     cClass    = null;

    DOMParser parser = new DOMParser();
  	parser.parse(new org.xml.sax.InputSource(new java.io.ByteArrayInputStream(sXML.getBytes())));

		// get the class name string from the XML document and instantiate the class
  	cClass = Class.forName(parser.getDocument().getDocumentElement().getTagName());	

  	// use JOX to create the object from the XML input stream
  	ByteArrayInputStream in = new ByteArrayInputStream(sXML.getBytes());
    JOXBeanInputStream joxIn = new JOXBeanInputStream(in);	
  	oObject = joxIn.readObject(cClass);		
    joxIn.close();
    in.close();

  	return oObject;
  }

 /**
  * Convert the input object to an XML string.
  *
  * Creation date: (07/12/2001 2:05:11 PM)
  * @return java.lang.String
  * @param Object The Object to be converted to XML
  */
  public String marshall(Object object) throws Exception
  {
  	ByteArrayOutputStream fileOut = new ByteArrayOutputStream();
  	JOXBeanOutputStream   joxOut  = new JOXBeanOutputStream(fileOut);
  	String sXML = "";

  	joxOut.writeObject(object.getClass().toString().substring(6), object);
  	sXML = fileOut.toString();
  	joxOut.close();
    fileOut.close();

	  return sXML;
  }
}
