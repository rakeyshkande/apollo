package com.ftd.b2b.common.utils;

public class DuplicateApprovalException extends Exception {

    public DuplicateApprovalException()
    {
      super();
    }
    
    public DuplicateApprovalException(String e)
    {
      super(e);
    }

}
