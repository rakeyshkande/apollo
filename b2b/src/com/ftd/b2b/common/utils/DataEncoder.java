package com.ftd.b2b.common.utils;

import java.awt.*;
import java.util.*;
import java.lang.reflect.*;

/**
 * This class should be used as a base class for Value Objects that
 * wish to have string data XML encoded. Using reflection, this class
 * will encode any String, Strings in a List object or Strings in a
 * HashMap. This class uses recursion to accomplish this.
 *
 * Creation date: (09/23/2002 10:56:21 AM)
 * @author: York Davis (SST)
 */
public abstract class DataEncoder {
	private static final String HASHMAP   = "java.util.HashMap";
	private static final String LIST      = "java.util.List";
	private static final String STRING    = "java.lang.String";
	private static final String GET       = "get";
	private static final String SET       = "set";

   /**
    * Encode String data.
    * Creation date: (07/01/2002 9:52:04 AM)
    */
   public void encode()
   {
    	Class c = this.getClass();

      Method[] theMethods = c.getMethods();
      for (int i = 0; i < theMethods.length; i++)
      {
    		String methodString = theMethods[i].getName();
    		String returnString = theMethods[i].getReturnType().getName();

    		if (methodString.startsWith(GET) && returnString.equalsIgnoreCase(STRING))  
    		{
    		  	Class[] parameterTypes = new Class[] {String.class};
    		    try
    		    {
      				Method concatMethod = c.getMethod(methodString, null);
  		        String result       = XMLHelper.encode((String) concatMethod.invoke(this, null));
  		        concatMethod        = c.getMethod(SET + methodString.substring(3), parameterTypes);
  		        concatMethod.invoke(this, new Object[] {result});
       			}
      			catch (Throwable e)
      			{
    			}
    		}
    	}
    	processArrayLists();
    	processHashMaps();
    }


    /**
     * Encode String data in Lists.
     * Creation date: (07/01/2002 10:44:58 AM)
     */
    private void processArrayLists()
    {
      	Class c = this.getClass();
        Method[] theMethods = c.getMethods();
        for (int i = 0; i < theMethods.length; i++)
        {
            if (theMethods[i].getReturnType().getName().equalsIgnoreCase(LIST))
            {
        			String methodString    = theMethods[i].getName();
      		    Class[] parameterTypes = new Class[] {String.class};
    	        try
      		    {
        				Method concatMethod = c.getMethod(methodString, null);
      			    ArrayList al = (ArrayList) concatMethod.invoke(this, null);

        				Iterator iterator = al.listIterator();
        				DataEncoder encoder;
        				while (iterator.hasNext())
        				{
        					try
        					{
        						encoder = (DataEncoder) iterator.next();
        						encoder.encode();
        					}
        					catch (Throwable t)
        					{
        					}
        				}
            	}
        			catch (Throwable e)
        			{
        			}
          }
    	}
  }

  /**
   * Encode String data in HashMaps.
   * Creation date: (07/01/2002 10:44:58 AM)
   */
  private void processHashMaps()
  {
      Class c = this.getClass();
     	Method[] theMethods = c.getMethods();
     	for (int i = 0; i < theMethods.length; i++)
     	{
      		if (theMethods[i].getReturnType().getName().equalsIgnoreCase(HASHMAP))
        	{
      			String methodString    = theMethods[i].getName();
    		    Class[] parameterTypes = new Class[] {String.class};
    		    try
    		    {
      				Method concatMethod = c.getMethod(methodString, null);
  		        HashMap al = (HashMap) concatMethod.invoke(this, null);

      				Iterator iterator = al.values().iterator();
      				DataEncoder encoder;
      				while (iterator.hasNext())
      				{
        					try
        					{
        						encoder = (DataEncoder) iterator.next();
        						encoder.encode();
        					}
          				catch (Throwable t)
        					{
        					}
      				}
    			}
  		    catch (Throwable t)
  		    {
          }
        }
    }
  }
}
