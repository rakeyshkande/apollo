package com.ftd.b2b.common.utils;

/**
 * This interface can be implemented by any class wishing to be 
 * notified of a callback event.
 *
 * @author York Davis
 * @version 1.0 
 **/
public interface IGenericCallback 
{
  /**
   * A generic callback method whose implementation can be 
   * changed as needed in the implementing class.
   *
   * @author York Davis
   **/
  public void callback();
}