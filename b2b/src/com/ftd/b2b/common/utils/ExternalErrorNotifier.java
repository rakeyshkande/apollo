package com.ftd.b2b.common.utils;

import com.ftd.b2b.constants.B2BConstants;
import com.ftd.b2b.utils.mail.B2BMailer;
import com.ftd.b2b.common.utils.BusinessObject;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.framework.common.utilities.ResourceManager;

/**
 * Email notification class for those instances where a repetitive error
 * may occur and an email should not be sent every time this error happens.
 * By using this class, email and log notifications will be limited to 
 * a period of time specified in the properties file. Otherwise, a potential
 * error condition could exist where emails and logs are sent in a tight loop
 * that could, for instance, overload the email server, etc.
 *
 * @author York Davis
 * @version 1.0 
 **/
public class ExternalErrorNotifier implements IGenericCallback, B2BConstants
{
  private boolean oktosend = true;
  
  /**
   * Implementation of the callback method used to set a flag indicating
   * that it is ok to send the email. This method is called after the thread
   * has expired which causes the wait period.
   *
   * @author York Davis
   **/
  public void callback()
  {
    oktosend = true;
  }

  /**
   * Attempt to log the error and send the email. However, if this activity
   * has occured recently and the thread that monitors the period of time
   * has not expired, skip the sending of the email and log until the time
   * has expired.
   *
   * @param Log category
   * @param Object generating the error
   * @param Method generating the error
   * @param Error message
   * @param Action code
   * @author York Davis
   **/
  public void sendError(String logCategory, Object originator, String method, String msg, int action)
  {
      if (oktosend)
      {
        Logger logManager = new Logger(logCategory);    
        logManager.error("sendError: " + msg);        
        B2BMailer.getInstance().send(originator, method, msg, action);          

        oktosend = false;
        // launch the thread to start the timer.
        Thread thread = new Thread(new CallbackThread(this));
        thread.start();
      }
  }
}

