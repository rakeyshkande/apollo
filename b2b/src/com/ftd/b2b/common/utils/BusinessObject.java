package com.ftd.b2b.common.utils;

import com.ftd.osp.utilities.plugins.Logger;
import java.io.Serializable;

public abstract class BusinessObject
    implements Serializable
{

    public BusinessObject(String loggerCategory)
    {
        lm = new Logger(loggerCategory);
    }

    public Logger getLogManager()
    {
        return lm;
    }

    public abstract void create();

    public abstract void save();

    public abstract boolean equals(Object obj);

    public abstract String toString();

    private Logger lm;
}