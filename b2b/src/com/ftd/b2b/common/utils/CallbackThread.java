package com.ftd.b2b.common.utils;

import com.ftd.framework.common.utilities.ResourceManager;
import com.ftd.b2b.constants.B2BConstants;

/**
 * This class serves to notify any class implementing the IGenericCallback
 * interface of the elapse of a period of time. This was developed primarily 
 * for use with the ExternalErrorNotifier class but could be used with other
 * classes implementing the IGenericCallback interface as well.
 *
 * @author York Davis
 * @version 1.0 
 **/
public class CallbackThread implements Runnable, B2BConstants
{
    private IGenericCallback notifier;
    private int milliSecondsToWait;
    private ResourceManager resourceManager;
    
    public CallbackThread(IGenericCallback notifier)
    {
        this.notifier = notifier;

        resourceManager = ResourceManager.getInstance();
        try
        {
             milliSecondsToWait =  Integer.parseInt(resourceManager.getProperty(PROPERTY_B2B_PROPERTY_FILE, 
                                                                                PROPERTY_ERROR_NOTIFIER_SLEEP_INTERVAL));   
        }
        catch (NumberFormatException nfe)
        {
             milliSecondsToWait = 300000; // five minutes
        }
        
    }

  /**
   * Wait for a specific period of time and then perform the callback.
   *
   * @author York Davis
   **/
   public void run()
    {
        try
        {
          Thread.sleep(milliSecondsToWait);
        }
        catch (InterruptedException ie)
        {
        }
        notifier.callback();
    }
}