package com.ftd.b2b.common.utils;

/**
 * This exception class is thrown in the condition where the SharedSecret
 * passed in with the cXML document is not the same as the SharedSecret
 * stored in the properties file.
 *
 * @author York Davis
 * @version 1.0 
 **/
public class AuthenticationException extends Exception
{

  public AuthenticationException()
  {
    super();
  }

  public AuthenticationException(String e)
  {
    super(e);
  }

}