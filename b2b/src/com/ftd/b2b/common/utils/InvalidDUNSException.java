package com.ftd.b2b.common.utils;

/**
 * This exception class is thrown in the condition where the DUNS number
 * passed in with the cXML document is not a valid. 
 *
 * @author York Davis
 * @version 1.0 
 **/
public class InvalidDUNSException extends Exception
{

   public InvalidDUNSException()
  {
    super();
  }
  
  public InvalidDUNSException(String e)
  {
    super(e);
  }

}