package com.ftd.b2b.common.utils;

/**
 * Defines an exception which occurs while parsing the XML document.
 *
 * @author York Davis
 * @version 1.0 
 **/
public class XMLParseException extends Exception 
{
  public XMLParseException()
  {
    super();
  }

  public XMLParseException(String e)
  {
    super(e);
  }
}