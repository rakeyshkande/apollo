package com.ftd.b2b.common.utils;

import java.text.NumberFormat;

/*
 * This class contains static methods to aid in formatting data.
 * @auther Ed Mueller
 */
public class DataFormatter 
{

/**
 * Formats price field to insure it contains 2 digits after the decimal.
 *
 * @params value String value to format
 * @returns A formatted string.
 *
 * @author Ed Mueller
 **/
    public static String formatPrice(double value)
    {
      //price needs to display 2 values after the decimal
      NumberFormat formatter = NumberFormat.getNumberInstance();
      formatter.setMinimumIntegerDigits(1);
      formatter.setMinimumFractionDigits(2);

      String formattedValue = formatter.format(value);

      return formattedValue;
    }

}