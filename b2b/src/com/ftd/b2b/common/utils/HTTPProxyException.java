package com.ftd.b2b.common.utils;

public class HTTPProxyException extends Exception 
{

  public HTTPProxyException()
  {
    super();
  }
  
  public HTTPProxyException(String e)
  {
    super(e);
  }
}