package com.ftd.b2b.test;

import java.net.*;
import java.io.*;
import java.util.*;
import com.sun.net.ssl.*;
import java.security.*;



// Sends contents of a file to a URL. ywd 10/10/2002
// !!! NOTE: It appears that if file is greater than 4269 characters, the send will not work !!! 
//
public class URLSender
{
	public static void main(String[] args) throws Exception
	{
			if (args.length != 2)
			{
				System.out.println("format: java URLSender [url] [input file]");
				return;
			}
			new URLSender().sendFileToUrl(args[0], args[1]);
	}

	public String sendFileToUrl(String theURL, String file) throws Exception
	{
	    File f = new File(file);
	    FileInputStream fis = new FileInputStream(f);
		BufferedReader br = new BufferedReader(new InputStreamReader(fis));

	    StringBuffer sb = new StringBuffer();
	    String line = "";
		while ((line = br.readLine()) != null)
	    {
			sb.append(line);
	    }

      System.setProperty("java.protocol.handler.pkgs","com.sun.net.ssl.internal.www.protocol");

	    URL url = new URL(theURL);
	    URLConnection con = url.openConnection();

		con.setRequestProperty("Content-Type", "text/xml");
		con.setDoInput(true);
		con.setDoOutput(true);
		con.setUseCaches(false);
		con.setDefaultUseCaches(false);

		OutputStream out = con.getOutputStream();
		System.out.println("Sent:\n\n" + sb.toString()+"\n\n\n");
		out.write(sb.toString().getBytes());

		InputStream is = con.getInputStream();
		br = new BufferedReader(new InputStreamReader(is));
		line=null;
		//reuse sb for the response
		sb = new StringBuffer();
		System.out.println("Got:\n\n");
		//String fn = "c:\\Work\\Workspaces\\B2B\\testapps\\" + buildOutputFileName();
		//File fileout = new File(fn);
		//FileWriter writer = new FileWriter(fileout);
		while( (line = br.readLine())!=null)
		{
			System.out.println(line);
			sb.append(line);
			//writer.write(line);
		}
		System.out.println("\n\nDone.\n");

		br.close();
		out.flush();
		out.close();
		//writer.close();
		
		return sb.toString();
	 }

	 private String buildOutputFileName()
	 {
		 Calendar cal = new GregorianCalendar();
		 cal.setTime(new Date());
		 return cal.get(Calendar.YEAR) + zeroPad(cal.get(Calendar.MONTH)) +
		   	    zeroPad(cal.get(Calendar.DATE))  + "-" + zeroPad(cal.get(Calendar.HOUR))   +
		 		zeroPad(cal.get(Calendar.MINUTE)) +  zeroPad(cal.get(Calendar.SECOND)) + ".xml";

	 }

	 private String zeroPad(int i)
	 {
		 String s = Integer.toString(i);
		 return s.length() == 1 ? "0" + s : s;
 	 }
}
