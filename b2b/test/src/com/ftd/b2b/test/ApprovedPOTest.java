package com.ftd.b2b.test;

import junit.framework.TestCase;

/**
 * This class will test the Approved PO Scenario.
 * 
 * It will submit a message to the ApprovedPO Servlet and then test for the expected response.
 * @author cjohnson
 *
 */
public class ApprovedPOTest extends TestCase {

	
	public void testSendApprovedPO() {
		
		URLSender urlSender = new URLSender();
		String response = null;
		try {
			response = urlSender.sendFileToUrl("b2btest.ftdi.com:7978/ariba/ReceiveApprovedPO", "/b2btest/data/approved.xml");
		} catch (Exception e) {
			// whatev.
			assertTrue("The test failed.  response: " + response ,false);
			
			e.printStackTrace();
		}
	}

	
	//ariba/PunchOutRequest
}
