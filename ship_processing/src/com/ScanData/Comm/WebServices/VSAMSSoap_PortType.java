/**
 * VSAMSSoap_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WebServices;

public interface VSAMSSoap_PortType extends java.rmi.Remote {

    /**
     * Returns the SessionID (default 0).
     */
    public int getSessionID() throws java.rmi.RemoteException;

    /**
     * Returns the version of this WebService.
     */
    public java.lang.String getWebServiceVersion() throws java.rmi.RemoteException;

    /**
     * Returns the version of the web.config file for this WebService.
     */
    public java.lang.String getWebServiceConfigVersion() throws java.rmi.RemoteException;

    /**
     * Accepts XML stream as input, creates ShipUnit record in ScanData's
     * VSAMS database, returns ShipUnit info including Label fields in XML
     * stream to caller.
     */
    public java.lang.String createShipment(java.lang.String strDSCreate) throws java.rmi.RemoteException;

    /**
     * Accepts XML Document as input stream, creates ShipUnit record
     * in ScanData's VSAMS database, returns ShipUnit info including Label
     * fields in XML Document format to caller.
     */
    public com.ScanData.Comm.WebServices.CreateShipmentXMLResponseCreateShipmentXMLResult createShipment(com.ScanData.Comm.WebServices.CreateShipmentXMLXdICSU xdICSU) throws java.rmi.RemoteException;

    /**
     * Accepts XML stream as input, Cancels ShipUnit record in ScanData's
     * VSAMS database, returns transaction status in XML stream to caller.
     */
    public java.lang.String cancelShipment(java.lang.String strDSCancel) throws java.rmi.RemoteException;

    /**
     * Accepts XML document as input, Cancels ShipUnit record in ScanData's
     * VSAMS database, returns transaction status in XML document to caller.
     */
    public com.ScanData.Comm.WebServices.CancelShipmentXMLResponseCancelShipmentXMLResult cancelShipment(com.ScanData.Comm.WebServices.CancelShipmentXMLXdReqCancel xdReqCancel) throws java.rmi.RemoteException;

    /**
     * Accepts XML stream as input stream, Closes shipment records
     * in ScanData's VSAMS database, returns transaction status in XML stream
     * to caller.
     */
    public java.lang.String close(java.lang.String strDSClose) throws java.rmi.RemoteException;

    /**
     * Accepts XML Document as input, Closes shipment records in ScanData's
     * VSAMS database, returns transaction status in XML Document to caller.
     */
    public com.ScanData.Comm.WebServices.CloseXMLResponseCloseXMLResult close(com.ScanData.Comm.WebServices.CloseXMLXdReqClose xdReqClose) throws java.rmi.RemoteException;

    /**
     * Accepts XML stream as input stream, Closes shipment records
     * in ScanData's VSAMS database, returns transaction status in XML stream
     * to caller.
     */
    public java.lang.String closeTrailer(java.lang.String strDSCloseTrailer) throws java.rmi.RemoteException;

    /**
     * Accepts XML Document as input, Closes shipment records in ScanData's
     * VSAMS database, returns transaction status in XML Document to caller.
     */
    public com.ScanData.Comm.WebServices.CloseTrailerXMLResponseCloseTrailerXMLResult closeTrailer(com.ScanData.Comm.WebServices.CloseTrailerXMLXdReqCloseTrailer xdReqCloseTrailer) throws java.rmi.RemoteException;

    /**
     * Ends current WebService session by closing out session statistics
     * tables in ScanData database.
     */
    public void endSession() throws java.rmi.RemoteException;

    /**
     * Accepts XML stream as input, returns ShipUnit info including
     * Label fields in XML stream to caller.
     */
    public java.lang.String getShipmentInfo(java.lang.String strDSCartonInfo) throws java.rmi.RemoteException;

    /**
     * Accepts XML Document as input stream, returns ShipUnit info
     * including Label fields in XML Document format to caller.
     */
    public com.ScanData.Comm.WebServices.GetShipmentInfoXMLResponseGetShipmentInfoXMLResult getShipmentInfo(com.ScanData.Comm.WebServices.GetShipmentInfoXMLXdReqInfo xdReqInfo) throws java.rmi.RemoteException;

    /**
     * Accepts XML stream as input, returns rating fields in XML stream
     * to caller.
     */
    public java.lang.String rateShipment(java.lang.String strDSRate) throws java.rmi.RemoteException;

    /**
     * Accepts XML Document as input stream, returns rating fields
     * in XML Document format to caller.
     */
    public com.ScanData.Comm.WebServices.RateShipmentXMLResponseRateShipmentXMLResult rateShipment(com.ScanData.Comm.WebServices.RateShipmentXMLXdICSU xdICSU) throws java.rmi.RemoteException;
}
