/**
 * CreateShipmentXMLResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WebServices;

public class CreateShipmentXMLResponse  implements java.io.Serializable {
    private com.ScanData.Comm.WebServices.CreateShipmentXMLResponseCreateShipmentXMLResult createShipmentXMLResult;

    public CreateShipmentXMLResponse() {
    }

    public CreateShipmentXMLResponse(
           com.ScanData.Comm.WebServices.CreateShipmentXMLResponseCreateShipmentXMLResult createShipmentXMLResult) {
           this.createShipmentXMLResult = createShipmentXMLResult;
    }


    /**
     * Gets the createShipmentXMLResult value for this CreateShipmentXMLResponse.
     * 
     * @return createShipmentXMLResult
     */
    public com.ScanData.Comm.WebServices.CreateShipmentXMLResponseCreateShipmentXMLResult getCreateShipmentXMLResult() {
        return createShipmentXMLResult;
    }


    /**
     * Sets the createShipmentXMLResult value for this CreateShipmentXMLResponse.
     * 
     * @param createShipmentXMLResult
     */
    public void setCreateShipmentXMLResult(com.ScanData.Comm.WebServices.CreateShipmentXMLResponseCreateShipmentXMLResult createShipmentXMLResult) {
        this.createShipmentXMLResult = createShipmentXMLResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CreateShipmentXMLResponse)) return false;
        CreateShipmentXMLResponse other = (CreateShipmentXMLResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.createShipmentXMLResult==null && other.getCreateShipmentXMLResult()==null) || 
             (this.createShipmentXMLResult!=null &&
              this.createShipmentXMLResult.equals(other.getCreateShipmentXMLResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCreateShipmentXMLResult() != null) {
            _hashCode += getCreateShipmentXMLResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CreateShipmentXMLResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">CreateShipmentXMLResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createShipmentXMLResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "CreateShipmentXMLResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">>CreateShipmentXMLResponse>CreateShipmentXMLResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
