/**
 * GetShipmentInfoXML.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WebServices;

public class GetShipmentInfoXML  implements java.io.Serializable {
    private com.ScanData.Comm.WebServices.GetShipmentInfoXMLXdReqInfo xdReqInfo;

    public GetShipmentInfoXML() {
    }

    public GetShipmentInfoXML(
           com.ScanData.Comm.WebServices.GetShipmentInfoXMLXdReqInfo xdReqInfo) {
           this.xdReqInfo = xdReqInfo;
    }


    /**
     * Gets the xdReqInfo value for this GetShipmentInfoXML.
     * 
     * @return xdReqInfo
     */
    public com.ScanData.Comm.WebServices.GetShipmentInfoXMLXdReqInfo getXdReqInfo() {
        return xdReqInfo;
    }


    /**
     * Sets the xdReqInfo value for this GetShipmentInfoXML.
     * 
     * @param xdReqInfo
     */
    public void setXdReqInfo(com.ScanData.Comm.WebServices.GetShipmentInfoXMLXdReqInfo xdReqInfo) {
        this.xdReqInfo = xdReqInfo;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetShipmentInfoXML)) return false;
        GetShipmentInfoXML other = (GetShipmentInfoXML) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.xdReqInfo==null && other.getXdReqInfo()==null) || 
             (this.xdReqInfo!=null &&
              this.xdReqInfo.equals(other.getXdReqInfo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getXdReqInfo() != null) {
            _hashCode += getXdReqInfo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetShipmentInfoXML.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">GetShipmentInfoXML"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("xdReqInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "xdReqInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">>GetShipmentInfoXML>xdReqInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
