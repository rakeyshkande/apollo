/**
 * VSAMS.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WebServices;

public interface VSAMS extends javax.xml.rpc.Service {

/**
 * WebService that communicates with ScanData's VSAMS backend server.
 */
    public java.lang.String getVSAMSSoap12Address();

    public com.ScanData.Comm.WebServices.VSAMSSoap_PortType getVSAMSSoap12() throws javax.xml.rpc.ServiceException;

    public com.ScanData.Comm.WebServices.VSAMSSoap_PortType getVSAMSSoap12(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    public java.lang.String getVSAMSSoapAddress();

    public com.ScanData.Comm.WebServices.VSAMSSoap_PortType getVSAMSSoap() throws javax.xml.rpc.ServiceException;

    public com.ScanData.Comm.WebServices.VSAMSSoap_PortType getVSAMSSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
