/**
 * VSAMSLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WebServices;

public class VSAMSLocator extends org.apache.axis.client.Service implements com.ScanData.Comm.WebServices.VSAMS {

/**
 * WebService that communicates with ScanData's VSAMS backend server.
 */

    public VSAMSLocator() {
    }


    public VSAMSLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public VSAMSLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for VSAMSSoap12
    private java.lang.String VSAMSSoap12_address = "http://localhost:2542/VSAMSService/VSAMS.asmx";

    public java.lang.String getVSAMSSoap12Address() {
        return VSAMSSoap12_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String VSAMSSoap12WSDDServiceName = "VSAMSSoap12";

    public java.lang.String getVSAMSSoap12WSDDServiceName() {
        return VSAMSSoap12WSDDServiceName;
    }

    public void setVSAMSSoap12WSDDServiceName(java.lang.String name) {
        VSAMSSoap12WSDDServiceName = name;
    }

    public com.ScanData.Comm.WebServices.VSAMSSoap_PortType getVSAMSSoap12() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(VSAMSSoap12_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getVSAMSSoap12(endpoint);
    }

    public com.ScanData.Comm.WebServices.VSAMSSoap_PortType getVSAMSSoap12(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.ScanData.Comm.WebServices.VSAMSSoap12Stub _stub = new com.ScanData.Comm.WebServices.VSAMSSoap12Stub(portAddress, this);
            _stub.setPortName(getVSAMSSoap12WSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setVSAMSSoap12EndpointAddress(java.lang.String address) {
        VSAMSSoap12_address = address;
    }


    // Use to get a proxy class for VSAMSSoap
    private java.lang.String VSAMSSoap_address = "http://localhost:2542/VSAMSService/VSAMS.asmx";

    public java.lang.String getVSAMSSoapAddress() {
        return VSAMSSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String VSAMSSoapWSDDServiceName = "VSAMSSoap";

    public java.lang.String getVSAMSSoapWSDDServiceName() {
        return VSAMSSoapWSDDServiceName;
    }

    public void setVSAMSSoapWSDDServiceName(java.lang.String name) {
        VSAMSSoapWSDDServiceName = name;
    }

    public com.ScanData.Comm.WebServices.VSAMSSoap_PortType getVSAMSSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(VSAMSSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getVSAMSSoap(endpoint);
    }

    public com.ScanData.Comm.WebServices.VSAMSSoap_PortType getVSAMSSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.ScanData.Comm.WebServices.VSAMSSoap_BindingStub _stub = new com.ScanData.Comm.WebServices.VSAMSSoap_BindingStub(portAddress, this);
            _stub.setPortName(getVSAMSSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setVSAMSSoapEndpointAddress(java.lang.String address) {
        VSAMSSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     * This service has multiple ports for a given interface;
     * the proxy implementation returned may be indeterminate.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.ScanData.Comm.WebServices.VSAMSSoap_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.ScanData.Comm.WebServices.VSAMSSoap12Stub _stub = new com.ScanData.Comm.WebServices.VSAMSSoap12Stub(new java.net.URL(VSAMSSoap12_address), this);
                _stub.setPortName(getVSAMSSoap12WSDDServiceName());
                return _stub;
            }
            if (com.ScanData.Comm.WebServices.VSAMSSoap_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.ScanData.Comm.WebServices.VSAMSSoap_BindingStub _stub = new com.ScanData.Comm.WebServices.VSAMSSoap_BindingStub(new java.net.URL(VSAMSSoap_address), this);
                _stub.setPortName(getVSAMSSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("VSAMSSoap12".equals(inputPortName)) {
            return getVSAMSSoap12();
        }
        else if ("VSAMSSoap".equals(inputPortName)) {
            return getVSAMSSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "VSAMS");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "VSAMSSoap12"));
            ports.add(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "VSAMSSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("VSAMSSoap12".equals(portName)) {
            setVSAMSSoap12EndpointAddress(address);
        }
        else 
if ("VSAMSSoap".equals(portName)) {
            setVSAMSSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
