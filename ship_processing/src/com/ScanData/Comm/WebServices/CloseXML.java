/**
 * CloseXML.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WebServices;

public class CloseXML  implements java.io.Serializable {
    private com.ScanData.Comm.WebServices.CloseXMLXdReqClose xdReqClose;

    public CloseXML() {
    }

    public CloseXML(
           com.ScanData.Comm.WebServices.CloseXMLXdReqClose xdReqClose) {
           this.xdReqClose = xdReqClose;
    }


    /**
     * Gets the xdReqClose value for this CloseXML.
     * 
     * @return xdReqClose
     */
    public com.ScanData.Comm.WebServices.CloseXMLXdReqClose getXdReqClose() {
        return xdReqClose;
    }


    /**
     * Sets the xdReqClose value for this CloseXML.
     * 
     * @param xdReqClose
     */
    public void setXdReqClose(com.ScanData.Comm.WebServices.CloseXMLXdReqClose xdReqClose) {
        this.xdReqClose = xdReqClose;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CloseXML)) return false;
        CloseXML other = (CloseXML) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.xdReqClose==null && other.getXdReqClose()==null) || 
             (this.xdReqClose!=null &&
              this.xdReqClose.equals(other.getXdReqClose())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getXdReqClose() != null) {
            _hashCode += getXdReqClose().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CloseXML.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">CloseXML"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("xdReqClose");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "xdReqClose"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">>CloseXML>xdReqClose"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
