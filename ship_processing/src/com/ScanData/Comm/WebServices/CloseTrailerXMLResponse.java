/**
 * CloseTrailerXMLResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WebServices;

public class CloseTrailerXMLResponse  implements java.io.Serializable {
    private com.ScanData.Comm.WebServices.CloseTrailerXMLResponseCloseTrailerXMLResult closeTrailerXMLResult;

    public CloseTrailerXMLResponse() {
    }

    public CloseTrailerXMLResponse(
           com.ScanData.Comm.WebServices.CloseTrailerXMLResponseCloseTrailerXMLResult closeTrailerXMLResult) {
           this.closeTrailerXMLResult = closeTrailerXMLResult;
    }


    /**
     * Gets the closeTrailerXMLResult value for this CloseTrailerXMLResponse.
     * 
     * @return closeTrailerXMLResult
     */
    public com.ScanData.Comm.WebServices.CloseTrailerXMLResponseCloseTrailerXMLResult getCloseTrailerXMLResult() {
        return closeTrailerXMLResult;
    }


    /**
     * Sets the closeTrailerXMLResult value for this CloseTrailerXMLResponse.
     * 
     * @param closeTrailerXMLResult
     */
    public void setCloseTrailerXMLResult(com.ScanData.Comm.WebServices.CloseTrailerXMLResponseCloseTrailerXMLResult closeTrailerXMLResult) {
        this.closeTrailerXMLResult = closeTrailerXMLResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CloseTrailerXMLResponse)) return false;
        CloseTrailerXMLResponse other = (CloseTrailerXMLResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.closeTrailerXMLResult==null && other.getCloseTrailerXMLResult()==null) || 
             (this.closeTrailerXMLResult!=null &&
              this.closeTrailerXMLResult.equals(other.getCloseTrailerXMLResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCloseTrailerXMLResult() != null) {
            _hashCode += getCloseTrailerXMLResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CloseTrailerXMLResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">CloseTrailerXMLResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("closeTrailerXMLResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "CloseTrailerXMLResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">>CloseTrailerXMLResponse>CloseTrailerXMLResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
