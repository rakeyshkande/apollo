/**
 * VSAMSSoap_BindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WebServices;

public class VSAMSSoap_BindingStub extends org.apache.axis.client.Stub implements com.ScanData.Comm.WebServices.VSAMSSoap_PortType {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[16];
        _initOperationDesc1();
        _initOperationDesc2();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetSessionID");
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        oper.setReturnClass(int.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "GetSessionIDResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetWebServiceVersion");
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        oper.setReturnClass(java.lang.String.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "GetWebServiceVersionResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetWebServiceConfigVersion");
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        oper.setReturnClass(java.lang.String.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "GetWebServiceConfigVersionResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("CreateShipment");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "strDSCreate"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        oper.setReturnClass(java.lang.String.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "CreateShipmentStringResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("CreateShipment");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "xdICSU"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">>CreateShipmentXML>xdICSU"), com.ScanData.Comm.WebServices.CreateShipmentXMLXdICSU.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">>CreateShipmentXMLResponse>CreateShipmentXMLResult"));
        oper.setReturnClass(com.ScanData.Comm.WebServices.CreateShipmentXMLResponseCreateShipmentXMLResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "CreateShipmentXMLResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("CancelShipment");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "strDSCancel"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        oper.setReturnClass(java.lang.String.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "CancelShipmentStringResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("CancelShipment");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "xdReqCancel"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">>CancelShipmentXML>xdReqCancel"), com.ScanData.Comm.WebServices.CancelShipmentXMLXdReqCancel.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">>CancelShipmentXMLResponse>CancelShipmentXMLResult"));
        oper.setReturnClass(com.ScanData.Comm.WebServices.CancelShipmentXMLResponseCancelShipmentXMLResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "CancelShipmentXMLResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[6] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Close");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "strDSClose"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        oper.setReturnClass(java.lang.String.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "CloseStringResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[7] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Close");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "xdReqClose"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">>CloseXML>xdReqClose"), com.ScanData.Comm.WebServices.CloseXMLXdReqClose.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">>CloseXMLResponse>CloseXMLResult"));
        oper.setReturnClass(com.ScanData.Comm.WebServices.CloseXMLResponseCloseXMLResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "CloseXMLResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[8] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("CloseTrailer");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "strDSCloseTrailer"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        oper.setReturnClass(java.lang.String.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "CloseTrailerStringResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[9] = oper;

    }

    private static void _initOperationDesc2(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("CloseTrailer");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "xdReqCloseTrailer"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">>CloseTrailerXML>xdReqCloseTrailer"), com.ScanData.Comm.WebServices.CloseTrailerXMLXdReqCloseTrailer.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">>CloseTrailerXMLResponse>CloseTrailerXMLResult"));
        oper.setReturnClass(com.ScanData.Comm.WebServices.CloseTrailerXMLResponseCloseTrailerXMLResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "CloseTrailerXMLResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[10] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("EndSession");
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[11] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetShipmentInfo");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "strDSCartonInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        oper.setReturnClass(java.lang.String.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "GetShipmentInfoStringResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[12] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetShipmentInfo");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "xdReqInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">>GetShipmentInfoXML>xdReqInfo"), com.ScanData.Comm.WebServices.GetShipmentInfoXMLXdReqInfo.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">>GetShipmentInfoXMLResponse>GetShipmentInfoXMLResult"));
        oper.setReturnClass(com.ScanData.Comm.WebServices.GetShipmentInfoXMLResponseGetShipmentInfoXMLResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "GetShipmentInfoXMLResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[13] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("RateShipment");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "strDSRate"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        oper.setReturnClass(java.lang.String.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "RateShipmentStringResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[14] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("RateShipment");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "xdICSU"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">>RateShipmentXML>xdICSU"), com.ScanData.Comm.WebServices.RateShipmentXMLXdICSU.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">>RateShipmentXMLResponse>RateShipmentXMLResult"));
        oper.setReturnClass(com.ScanData.Comm.WebServices.RateShipmentXMLResponseRateShipmentXMLResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "RateShipmentXMLResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[15] = oper;

    }

    public VSAMSSoap_BindingStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public VSAMSSoap_BindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public VSAMSSoap_BindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">>CancelShipmentXML>xdReqCancel");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.CancelShipmentXMLXdReqCancel.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">>CancelShipmentXMLResponse>CancelShipmentXMLResult");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.CancelShipmentXMLResponseCancelShipmentXMLResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">>CloseTrailerXML>xdReqCloseTrailer");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.CloseTrailerXMLXdReqCloseTrailer.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">>CloseTrailerXMLResponse>CloseTrailerXMLResult");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.CloseTrailerXMLResponseCloseTrailerXMLResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">>CloseXML>xdReqClose");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.CloseXMLXdReqClose.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">>CloseXMLResponse>CloseXMLResult");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.CloseXMLResponseCloseXMLResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">>CreateShipmentXML>xdICSU");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.CreateShipmentXMLXdICSU.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">>CreateShipmentXMLResponse>CreateShipmentXMLResult");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.CreateShipmentXMLResponseCreateShipmentXMLResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">>GetShipmentInfoXML>xdReqInfo");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.GetShipmentInfoXMLXdReqInfo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">>GetShipmentInfoXMLResponse>GetShipmentInfoXMLResult");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.GetShipmentInfoXMLResponseGetShipmentInfoXMLResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">>RateShipmentXML>xdICSU");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.RateShipmentXMLXdICSU.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">>RateShipmentXMLResponse>RateShipmentXMLResult");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.RateShipmentXMLResponseRateShipmentXMLResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">CancelShipmentString");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.CancelShipmentString.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">CancelShipmentStringResponse");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.CancelShipmentStringResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">CancelShipmentXML");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.CancelShipmentXML.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">CancelShipmentXMLResponse");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.CancelShipmentXMLResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">CloseString");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.CloseString.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">CloseStringResponse");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.CloseStringResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">CloseTrailerString");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.CloseTrailerString.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">CloseTrailerStringResponse");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.CloseTrailerStringResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">CloseTrailerXML");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.CloseTrailerXML.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">CloseTrailerXMLResponse");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.CloseTrailerXMLResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">CloseXML");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.CloseXML.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">CloseXMLResponse");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.CloseXMLResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">CreateShipmentXMLResponse");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.CreateShipmentXMLResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">EndSession");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.EndSession.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">EndSessionResponse");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.EndSessionResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">GetShipmentInfoString");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.GetShipmentInfoString.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">GetShipmentInfoStringResponse");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.GetShipmentInfoStringResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">GetShipmentInfoXML");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.GetShipmentInfoXML.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">GetShipmentInfoXMLResponse");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.GetShipmentInfoXMLResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">RateShipmentString");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.RateShipmentString.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">RateShipmentStringResponse");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.RateShipmentStringResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">RateShipmentXML");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.RateShipmentXML.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">RateShipmentXMLResponse");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.RateShipmentXMLResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public int getSessionID() throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/Comm/WebServices/GetSessionID");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "GetSessionID"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return ((java.lang.Integer) _resp).intValue();
            } catch (java.lang.Exception _exception) {
                return ((java.lang.Integer) org.apache.axis.utils.JavaUtils.convert(_resp, int.class)).intValue();
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public java.lang.String getWebServiceVersion() throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/Comm/WebServices/GetWebServiceVersion");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "GetWebServiceVersion"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (java.lang.String) _resp;
            } catch (java.lang.Exception _exception) {
                return (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.String.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public java.lang.String getWebServiceConfigVersion() throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/Comm/WebServices/GetWebServiceConfigVersion");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "GetWebServiceConfigVersion"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (java.lang.String) _resp;
            } catch (java.lang.Exception _exception) {
                return (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.String.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public java.lang.String createShipment(java.lang.String strDSCreate) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/Comm/WebServices/CreateShipmentString");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "CreateShipmentString"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {strDSCreate});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (java.lang.String) _resp;
            } catch (java.lang.Exception _exception) {
                return (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.String.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.ScanData.Comm.WebServices.CreateShipmentXMLResponseCreateShipmentXMLResult createShipment(com.ScanData.Comm.WebServices.CreateShipmentXMLXdICSU xdICSU) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/Comm/WebServices/CreateShipmentXML");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "CreateShipmentXML"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {xdICSU});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.ScanData.Comm.WebServices.CreateShipmentXMLResponseCreateShipmentXMLResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.ScanData.Comm.WebServices.CreateShipmentXMLResponseCreateShipmentXMLResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.ScanData.Comm.WebServices.CreateShipmentXMLResponseCreateShipmentXMLResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public java.lang.String cancelShipment(java.lang.String strDSCancel) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/Comm/WebServices/CancelShipmentString");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "CancelShipmentString"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {strDSCancel});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (java.lang.String) _resp;
            } catch (java.lang.Exception _exception) {
                return (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.String.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.ScanData.Comm.WebServices.CancelShipmentXMLResponseCancelShipmentXMLResult cancelShipment(com.ScanData.Comm.WebServices.CancelShipmentXMLXdReqCancel xdReqCancel) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/Comm/WebServices/CancelShipmentXML");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "CancelShipmentXML"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {xdReqCancel});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.ScanData.Comm.WebServices.CancelShipmentXMLResponseCancelShipmentXMLResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.ScanData.Comm.WebServices.CancelShipmentXMLResponseCancelShipmentXMLResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.ScanData.Comm.WebServices.CancelShipmentXMLResponseCancelShipmentXMLResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public java.lang.String close(java.lang.String strDSClose) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[7]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/Comm/WebServices/CloseString");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "CloseString"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {strDSClose});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (java.lang.String) _resp;
            } catch (java.lang.Exception _exception) {
                return (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.String.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.ScanData.Comm.WebServices.CloseXMLResponseCloseXMLResult close(com.ScanData.Comm.WebServices.CloseXMLXdReqClose xdReqClose) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[8]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/Comm/WebServices/CloseXML");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "CloseXML"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {xdReqClose});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.ScanData.Comm.WebServices.CloseXMLResponseCloseXMLResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.ScanData.Comm.WebServices.CloseXMLResponseCloseXMLResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.ScanData.Comm.WebServices.CloseXMLResponseCloseXMLResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public java.lang.String closeTrailer(java.lang.String strDSCloseTrailer) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[9]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/Comm/WebServices/CloseTrailerString");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "CloseTrailerString"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {strDSCloseTrailer});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (java.lang.String) _resp;
            } catch (java.lang.Exception _exception) {
                return (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.String.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.ScanData.Comm.WebServices.CloseTrailerXMLResponseCloseTrailerXMLResult closeTrailer(com.ScanData.Comm.WebServices.CloseTrailerXMLXdReqCloseTrailer xdReqCloseTrailer) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[10]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/Comm/WebServices/CloseTrailerXML");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "CloseTrailerXML"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {xdReqCloseTrailer});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.ScanData.Comm.WebServices.CloseTrailerXMLResponseCloseTrailerXMLResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.ScanData.Comm.WebServices.CloseTrailerXMLResponseCloseTrailerXMLResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.ScanData.Comm.WebServices.CloseTrailerXMLResponseCloseTrailerXMLResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public void endSession() throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[11]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/Comm/WebServices/EndSession");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "EndSession"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public java.lang.String getShipmentInfo(java.lang.String strDSCartonInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[12]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/Comm/WebServices/GetShipmentInfoString");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "GetShipmentInfoString"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {strDSCartonInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (java.lang.String) _resp;
            } catch (java.lang.Exception _exception) {
                return (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.String.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.ScanData.Comm.WebServices.GetShipmentInfoXMLResponseGetShipmentInfoXMLResult getShipmentInfo(com.ScanData.Comm.WebServices.GetShipmentInfoXMLXdReqInfo xdReqInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[13]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/Comm/WebServices/GetShipmentInfoXML");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "GetShipmentInfoXML"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {xdReqInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.ScanData.Comm.WebServices.GetShipmentInfoXMLResponseGetShipmentInfoXMLResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.ScanData.Comm.WebServices.GetShipmentInfoXMLResponseGetShipmentInfoXMLResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.ScanData.Comm.WebServices.GetShipmentInfoXMLResponseGetShipmentInfoXMLResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public java.lang.String rateShipment(java.lang.String strDSRate) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[14]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/Comm/WebServices/RateShipmentString");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "RateShipmentString"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {strDSRate});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (java.lang.String) _resp;
            } catch (java.lang.Exception _exception) {
                return (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.String.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.ScanData.Comm.WebServices.RateShipmentXMLResponseRateShipmentXMLResult rateShipment(com.ScanData.Comm.WebServices.RateShipmentXMLXdICSU xdICSU) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[15]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/Comm/WebServices/RateShipmentXML");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "RateShipmentXML"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {xdICSU});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.ScanData.Comm.WebServices.RateShipmentXMLResponseRateShipmentXMLResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.ScanData.Comm.WebServices.RateShipmentXMLResponseRateShipmentXMLResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.ScanData.Comm.WebServices.RateShipmentXMLResponseRateShipmentXMLResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
