/**
 * GetShipmentInfoXMLResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WebServices;

public class GetShipmentInfoXMLResponse  implements java.io.Serializable {
    private com.ScanData.Comm.WebServices.GetShipmentInfoXMLResponseGetShipmentInfoXMLResult getShipmentInfoXMLResult;

    public GetShipmentInfoXMLResponse() {
    }

    public GetShipmentInfoXMLResponse(
           com.ScanData.Comm.WebServices.GetShipmentInfoXMLResponseGetShipmentInfoXMLResult getShipmentInfoXMLResult) {
           this.getShipmentInfoXMLResult = getShipmentInfoXMLResult;
    }


    /**
     * Gets the getShipmentInfoXMLResult value for this GetShipmentInfoXMLResponse.
     * 
     * @return getShipmentInfoXMLResult
     */
    public com.ScanData.Comm.WebServices.GetShipmentInfoXMLResponseGetShipmentInfoXMLResult getGetShipmentInfoXMLResult() {
        return getShipmentInfoXMLResult;
    }


    /**
     * Sets the getShipmentInfoXMLResult value for this GetShipmentInfoXMLResponse.
     * 
     * @param getShipmentInfoXMLResult
     */
    public void setGetShipmentInfoXMLResult(com.ScanData.Comm.WebServices.GetShipmentInfoXMLResponseGetShipmentInfoXMLResult getShipmentInfoXMLResult) {
        this.getShipmentInfoXMLResult = getShipmentInfoXMLResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetShipmentInfoXMLResponse)) return false;
        GetShipmentInfoXMLResponse other = (GetShipmentInfoXMLResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getShipmentInfoXMLResult==null && other.getGetShipmentInfoXMLResult()==null) || 
             (this.getShipmentInfoXMLResult!=null &&
              this.getShipmentInfoXMLResult.equals(other.getGetShipmentInfoXMLResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetShipmentInfoXMLResult() != null) {
            _hashCode += getGetShipmentInfoXMLResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetShipmentInfoXMLResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">GetShipmentInfoXMLResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getShipmentInfoXMLResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "GetShipmentInfoXMLResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">>GetShipmentInfoXMLResponse>GetShipmentInfoXMLResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
