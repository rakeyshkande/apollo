/**
 * CloseTrailerStringResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WebServices;

public class CloseTrailerStringResponse  implements java.io.Serializable {
    private java.lang.String closeTrailerStringResult;

    public CloseTrailerStringResponse() {
    }

    public CloseTrailerStringResponse(
           java.lang.String closeTrailerStringResult) {
           this.closeTrailerStringResult = closeTrailerStringResult;
    }


    /**
     * Gets the closeTrailerStringResult value for this CloseTrailerStringResponse.
     * 
     * @return closeTrailerStringResult
     */
    public java.lang.String getCloseTrailerStringResult() {
        return closeTrailerStringResult;
    }


    /**
     * Sets the closeTrailerStringResult value for this CloseTrailerStringResponse.
     * 
     * @param closeTrailerStringResult
     */
    public void setCloseTrailerStringResult(java.lang.String closeTrailerStringResult) {
        this.closeTrailerStringResult = closeTrailerStringResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CloseTrailerStringResponse)) return false;
        CloseTrailerStringResponse other = (CloseTrailerStringResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.closeTrailerStringResult==null && other.getCloseTrailerStringResult()==null) || 
             (this.closeTrailerStringResult!=null &&
              this.closeTrailerStringResult.equals(other.getCloseTrailerStringResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCloseTrailerStringResult() != null) {
            _hashCode += getCloseTrailerStringResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CloseTrailerStringResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">CloseTrailerStringResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("closeTrailerStringResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "CloseTrailerStringResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
