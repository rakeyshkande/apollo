/**
 * RateShipmentXML.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WebServices;

public class RateShipmentXML  implements java.io.Serializable {
    private com.ScanData.Comm.WebServices.RateShipmentXMLXdICSU xdICSU;

    public RateShipmentXML() {
    }

    public RateShipmentXML(
           com.ScanData.Comm.WebServices.RateShipmentXMLXdICSU xdICSU) {
           this.xdICSU = xdICSU;
    }


    /**
     * Gets the xdICSU value for this RateShipmentXML.
     * 
     * @return xdICSU
     */
    public com.ScanData.Comm.WebServices.RateShipmentXMLXdICSU getXdICSU() {
        return xdICSU;
    }


    /**
     * Sets the xdICSU value for this RateShipmentXML.
     * 
     * @param xdICSU
     */
    public void setXdICSU(com.ScanData.Comm.WebServices.RateShipmentXMLXdICSU xdICSU) {
        this.xdICSU = xdICSU;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RateShipmentXML)) return false;
        RateShipmentXML other = (RateShipmentXML) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.xdICSU==null && other.getXdICSU()==null) || 
             (this.xdICSU!=null &&
              this.xdICSU.equals(other.getXdICSU())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getXdICSU() != null) {
            _hashCode += getXdICSU().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RateShipmentXML.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">RateShipmentXML"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("xdICSU");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "xdICSU"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">>RateShipmentXML>xdICSU"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
