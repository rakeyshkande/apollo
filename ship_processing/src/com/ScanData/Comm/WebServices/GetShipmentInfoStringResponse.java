/**
 * GetShipmentInfoStringResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WebServices;

public class GetShipmentInfoStringResponse  implements java.io.Serializable {
    private java.lang.String getShipmentInfoStringResult;

    public GetShipmentInfoStringResponse() {
    }

    public GetShipmentInfoStringResponse(
           java.lang.String getShipmentInfoStringResult) {
           this.getShipmentInfoStringResult = getShipmentInfoStringResult;
    }


    /**
     * Gets the getShipmentInfoStringResult value for this GetShipmentInfoStringResponse.
     * 
     * @return getShipmentInfoStringResult
     */
    public java.lang.String getGetShipmentInfoStringResult() {
        return getShipmentInfoStringResult;
    }


    /**
     * Sets the getShipmentInfoStringResult value for this GetShipmentInfoStringResponse.
     * 
     * @param getShipmentInfoStringResult
     */
    public void setGetShipmentInfoStringResult(java.lang.String getShipmentInfoStringResult) {
        this.getShipmentInfoStringResult = getShipmentInfoStringResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetShipmentInfoStringResponse)) return false;
        GetShipmentInfoStringResponse other = (GetShipmentInfoStringResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getShipmentInfoStringResult==null && other.getGetShipmentInfoStringResult()==null) || 
             (this.getShipmentInfoStringResult!=null &&
              this.getShipmentInfoStringResult.equals(other.getGetShipmentInfoStringResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetShipmentInfoStringResult() != null) {
            _hashCode += getGetShipmentInfoStringResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetShipmentInfoStringResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">GetShipmentInfoStringResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getShipmentInfoStringResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "GetShipmentInfoStringResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
