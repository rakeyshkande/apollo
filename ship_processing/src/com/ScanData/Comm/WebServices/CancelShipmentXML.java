/**
 * CancelShipmentXML.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WebServices;

public class CancelShipmentXML  implements java.io.Serializable {
    private com.ScanData.Comm.WebServices.CancelShipmentXMLXdReqCancel xdReqCancel;

    public CancelShipmentXML() {
    }

    public CancelShipmentXML(
           com.ScanData.Comm.WebServices.CancelShipmentXMLXdReqCancel xdReqCancel) {
           this.xdReqCancel = xdReqCancel;
    }


    /**
     * Gets the xdReqCancel value for this CancelShipmentXML.
     * 
     * @return xdReqCancel
     */
    public com.ScanData.Comm.WebServices.CancelShipmentXMLXdReqCancel getXdReqCancel() {
        return xdReqCancel;
    }


    /**
     * Sets the xdReqCancel value for this CancelShipmentXML.
     * 
     * @param xdReqCancel
     */
    public void setXdReqCancel(com.ScanData.Comm.WebServices.CancelShipmentXMLXdReqCancel xdReqCancel) {
        this.xdReqCancel = xdReqCancel;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CancelShipmentXML)) return false;
        CancelShipmentXML other = (CancelShipmentXML) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.xdReqCancel==null && other.getXdReqCancel()==null) || 
             (this.xdReqCancel!=null &&
              this.xdReqCancel.equals(other.getXdReqCancel())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getXdReqCancel() != null) {
            _hashCode += getXdReqCancel().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CancelShipmentXML.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">CancelShipmentXML"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("xdReqCancel");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "xdReqCancel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">>CancelShipmentXML>xdReqCancel"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
