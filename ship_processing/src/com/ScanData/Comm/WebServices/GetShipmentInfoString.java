/**
 * GetShipmentInfoString.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WebServices;

public class GetShipmentInfoString  implements java.io.Serializable {
    private java.lang.String strDSCartonInfo;

    public GetShipmentInfoString() {
    }

    public GetShipmentInfoString(
           java.lang.String strDSCartonInfo) {
           this.strDSCartonInfo = strDSCartonInfo;
    }


    /**
     * Gets the strDSCartonInfo value for this GetShipmentInfoString.
     * 
     * @return strDSCartonInfo
     */
    public java.lang.String getStrDSCartonInfo() {
        return strDSCartonInfo;
    }


    /**
     * Sets the strDSCartonInfo value for this GetShipmentInfoString.
     * 
     * @param strDSCartonInfo
     */
    public void setStrDSCartonInfo(java.lang.String strDSCartonInfo) {
        this.strDSCartonInfo = strDSCartonInfo;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetShipmentInfoString)) return false;
        GetShipmentInfoString other = (GetShipmentInfoString) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.strDSCartonInfo==null && other.getStrDSCartonInfo()==null) || 
             (this.strDSCartonInfo!=null &&
              this.strDSCartonInfo.equals(other.getStrDSCartonInfo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getStrDSCartonInfo() != null) {
            _hashCode += getStrDSCartonInfo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetShipmentInfoString.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">GetShipmentInfoString"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("strDSCartonInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "strDSCartonInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
