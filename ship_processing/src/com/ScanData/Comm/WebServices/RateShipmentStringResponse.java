/**
 * RateShipmentStringResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WebServices;

public class RateShipmentStringResponse  implements java.io.Serializable {
    private java.lang.String rateShipmentStringResult;

    public RateShipmentStringResponse() {
    }

    public RateShipmentStringResponse(
           java.lang.String rateShipmentStringResult) {
           this.rateShipmentStringResult = rateShipmentStringResult;
    }


    /**
     * Gets the rateShipmentStringResult value for this RateShipmentStringResponse.
     * 
     * @return rateShipmentStringResult
     */
    public java.lang.String getRateShipmentStringResult() {
        return rateShipmentStringResult;
    }


    /**
     * Sets the rateShipmentStringResult value for this RateShipmentStringResponse.
     * 
     * @param rateShipmentStringResult
     */
    public void setRateShipmentStringResult(java.lang.String rateShipmentStringResult) {
        this.rateShipmentStringResult = rateShipmentStringResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RateShipmentStringResponse)) return false;
        RateShipmentStringResponse other = (RateShipmentStringResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.rateShipmentStringResult==null && other.getRateShipmentStringResult()==null) || 
             (this.rateShipmentStringResult!=null &&
              this.rateShipmentStringResult.equals(other.getRateShipmentStringResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRateShipmentStringResult() != null) {
            _hashCode += getRateShipmentStringResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RateShipmentStringResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">RateShipmentStringResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rateShipmentStringResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "RateShipmentStringResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
