/**
 * CancelShipmentStringResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WebServices;

public class CancelShipmentStringResponse  implements java.io.Serializable {
    private java.lang.String cancelShipmentStringResult;

    public CancelShipmentStringResponse() {
    }

    public CancelShipmentStringResponse(
           java.lang.String cancelShipmentStringResult) {
           this.cancelShipmentStringResult = cancelShipmentStringResult;
    }


    /**
     * Gets the cancelShipmentStringResult value for this CancelShipmentStringResponse.
     * 
     * @return cancelShipmentStringResult
     */
    public java.lang.String getCancelShipmentStringResult() {
        return cancelShipmentStringResult;
    }


    /**
     * Sets the cancelShipmentStringResult value for this CancelShipmentStringResponse.
     * 
     * @param cancelShipmentStringResult
     */
    public void setCancelShipmentStringResult(java.lang.String cancelShipmentStringResult) {
        this.cancelShipmentStringResult = cancelShipmentStringResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CancelShipmentStringResponse)) return false;
        CancelShipmentStringResponse other = (CancelShipmentStringResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.cancelShipmentStringResult==null && other.getCancelShipmentStringResult()==null) || 
             (this.cancelShipmentStringResult!=null &&
              this.cancelShipmentStringResult.equals(other.getCancelShipmentStringResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCancelShipmentStringResult() != null) {
            _hashCode += getCancelShipmentStringResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CancelShipmentStringResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", ">CancelShipmentStringResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cancelShipmentStringResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/Comm/WebServices/", "CancelShipmentStringResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
