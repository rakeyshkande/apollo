/**
 * GetMultipleOrderDataSearchResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WTMServices;

public class GetMultipleOrderDataSearchResponse  implements java.io.Serializable {
    private com.ScanData.Comm.WTMServices.GetMultipleOrderDataSearchResponseGetMultipleOrderDataSearchResult getMultipleOrderDataSearchResult;

    public GetMultipleOrderDataSearchResponse() {
    }

    public GetMultipleOrderDataSearchResponse(
           com.ScanData.Comm.WTMServices.GetMultipleOrderDataSearchResponseGetMultipleOrderDataSearchResult getMultipleOrderDataSearchResult) {
           this.getMultipleOrderDataSearchResult = getMultipleOrderDataSearchResult;
    }


    /**
     * Gets the getMultipleOrderDataSearchResult value for this GetMultipleOrderDataSearchResponse.
     * 
     * @return getMultipleOrderDataSearchResult
     */
    public com.ScanData.Comm.WTMServices.GetMultipleOrderDataSearchResponseGetMultipleOrderDataSearchResult getGetMultipleOrderDataSearchResult() {
        return getMultipleOrderDataSearchResult;
    }


    /**
     * Sets the getMultipleOrderDataSearchResult value for this GetMultipleOrderDataSearchResponse.
     * 
     * @param getMultipleOrderDataSearchResult
     */
    public void setGetMultipleOrderDataSearchResult(com.ScanData.Comm.WTMServices.GetMultipleOrderDataSearchResponseGetMultipleOrderDataSearchResult getMultipleOrderDataSearchResult) {
        this.getMultipleOrderDataSearchResult = getMultipleOrderDataSearchResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetMultipleOrderDataSearchResponse)) return false;
        GetMultipleOrderDataSearchResponse other = (GetMultipleOrderDataSearchResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getMultipleOrderDataSearchResult==null && other.getGetMultipleOrderDataSearchResult()==null) || 
             (this.getMultipleOrderDataSearchResult!=null &&
              this.getMultipleOrderDataSearchResult.equals(other.getGetMultipleOrderDataSearchResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetMultipleOrderDataSearchResult() != null) {
            _hashCode += getGetMultipleOrderDataSearchResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetMultipleOrderDataSearchResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/", ">GetMultipleOrderDataSearchResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getMultipleOrderDataSearchResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/", "GetMultipleOrderDataSearchResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/", ">>GetMultipleOrderDataSearchResponse>GetMultipleOrderDataSearchResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
