/**
 * GetAggregateCounts.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WTMServices;

public class GetAggregateCounts  implements java.io.Serializable {
    private java.lang.String distributionCenter;

    private java.lang.String lineOfBusiness;

    private java.lang.String status;

    private java.util.Calendar datePlannedShipment;

    private int numberOfDays;

    public GetAggregateCounts() {
    }

    public GetAggregateCounts(
           java.lang.String distributionCenter,
           java.lang.String lineOfBusiness,
           java.lang.String status,
           java.util.Calendar datePlannedShipment,
           int numberOfDays) {
           this.distributionCenter = distributionCenter;
           this.lineOfBusiness = lineOfBusiness;
           this.status = status;
           this.datePlannedShipment = datePlannedShipment;
           this.numberOfDays = numberOfDays;
    }


    /**
     * Gets the distributionCenter value for this GetAggregateCounts.
     * 
     * @return distributionCenter
     */
    public java.lang.String getDistributionCenter() {
        return distributionCenter;
    }


    /**
     * Sets the distributionCenter value for this GetAggregateCounts.
     * 
     * @param distributionCenter
     */
    public void setDistributionCenter(java.lang.String distributionCenter) {
        this.distributionCenter = distributionCenter;
    }


    /**
     * Gets the lineOfBusiness value for this GetAggregateCounts.
     * 
     * @return lineOfBusiness
     */
    public java.lang.String getLineOfBusiness() {
        return lineOfBusiness;
    }


    /**
     * Sets the lineOfBusiness value for this GetAggregateCounts.
     * 
     * @param lineOfBusiness
     */
    public void setLineOfBusiness(java.lang.String lineOfBusiness) {
        this.lineOfBusiness = lineOfBusiness;
    }


    /**
     * Gets the status value for this GetAggregateCounts.
     * 
     * @return status
     */
    public java.lang.String getStatus() {
        return status;
    }


    /**
     * Sets the status value for this GetAggregateCounts.
     * 
     * @param status
     */
    public void setStatus(java.lang.String status) {
        this.status = status;
    }


    /**
     * Gets the datePlannedShipment value for this GetAggregateCounts.
     * 
     * @return datePlannedShipment
     */
    public java.util.Calendar getDatePlannedShipment() {
        return datePlannedShipment;
    }


    /**
     * Sets the datePlannedShipment value for this GetAggregateCounts.
     * 
     * @param datePlannedShipment
     */
    public void setDatePlannedShipment(java.util.Calendar datePlannedShipment) {
        this.datePlannedShipment = datePlannedShipment;
    }


    /**
     * Gets the numberOfDays value for this GetAggregateCounts.
     * 
     * @return numberOfDays
     */
    public int getNumberOfDays() {
        return numberOfDays;
    }


    /**
     * Sets the numberOfDays value for this GetAggregateCounts.
     * 
     * @param numberOfDays
     */
    public void setNumberOfDays(int numberOfDays) {
        this.numberOfDays = numberOfDays;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetAggregateCounts)) return false;
        GetAggregateCounts other = (GetAggregateCounts) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.distributionCenter==null && other.getDistributionCenter()==null) || 
             (this.distributionCenter!=null &&
              this.distributionCenter.equals(other.getDistributionCenter()))) &&
            ((this.lineOfBusiness==null && other.getLineOfBusiness()==null) || 
             (this.lineOfBusiness!=null &&
              this.lineOfBusiness.equals(other.getLineOfBusiness()))) &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.datePlannedShipment==null && other.getDatePlannedShipment()==null) || 
             (this.datePlannedShipment!=null &&
              this.datePlannedShipment.equals(other.getDatePlannedShipment()))) &&
            this.numberOfDays == other.getNumberOfDays();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDistributionCenter() != null) {
            _hashCode += getDistributionCenter().hashCode();
        }
        if (getLineOfBusiness() != null) {
            _hashCode += getLineOfBusiness().hashCode();
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getDatePlannedShipment() != null) {
            _hashCode += getDatePlannedShipment().hashCode();
        }
        _hashCode += getNumberOfDays();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetAggregateCounts.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/", ">GetAggregateCounts"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("distributionCenter");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/", "DistributionCenter"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lineOfBusiness");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/", "LineOfBusiness"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/", "Status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datePlannedShipment");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/", "DatePlannedShipment"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numberOfDays");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/", "NumberOfDays"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
