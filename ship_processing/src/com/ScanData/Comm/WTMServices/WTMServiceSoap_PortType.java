/**
 * WTMServiceSoap_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WTMServices;

public interface WTMServiceSoap_PortType extends java.rmi.Remote {
    public java.lang.String helloWorld() throws java.rmi.RemoteException;
    public java.lang.String getCartonLabelImage(java.lang.String cartonNumber) throws java.rmi.RemoteException;
    public java.lang.String getCartonLabelURL(java.lang.String cartonNumber) throws java.rmi.RemoteException;
    public com.ScanData.Comm.WTMServices.GetDistributionCenterListResponseGetDistributionCenterListResult getDistributionCenterList() throws java.rmi.RemoteException;
    public com.ScanData.Comm.WTMServices.GetLineOfBusinessListResponseGetLineOfBusinessListResult getLineOfBusinessList(java.lang.String distributionCenter) throws java.rmi.RemoteException;
    public com.ScanData.Comm.WTMServices.GetAggregateCountsResponseGetAggregateCountsResult getAggregateCounts(java.lang.String distributionCenter, java.lang.String lineOfBusiness, java.lang.String status, java.util.Calendar datePlannedShipment, int numberOfDays) throws java.rmi.RemoteException;
    public com.ScanData.Comm.WTMServices.GetAggregateCountsSearchResponseGetAggregateCountsSearchResult getAggregateCountsSearch(java.lang.String distributionCenter, java.lang.String lineOfBusiness, java.lang.String status, com.ScanData.Comm.WTMServices.GetAggregateCountsSearchParams params) throws java.rmi.RemoteException;
    public com.ScanData.Comm.WTMServices.GetMultipleOrderDataResponseGetMultipleOrderDataResult getMultipleOrderData(java.lang.String distributionCenter, java.lang.String lineOfBusiness, java.lang.String status, java.util.Calendar datePlannedShipment, int numberOfDays, int maxRows) throws java.rmi.RemoteException;
    public com.ScanData.Comm.WTMServices.GetMultipleOrderDataSearchResponseGetMultipleOrderDataSearchResult getMultipleOrderDataSearch(java.lang.String distributionCenter, java.lang.String lineOfBusiness, java.lang.String status, com.ScanData.Comm.WTMServices.GetMultipleOrderDataSearchParams params, com.ScanData.Comm.WTMServices.GetMultipleOrderDataSearchSort sort, int maxRows) throws java.rmi.RemoteException;
    public com.ScanData.Comm.WTMServices.CreateBatchResponseCreateBatchResult createBatch(java.lang.String distributionCenter, java.lang.String lineOfBusiness, com.ScanData.Comm.WTMServices.CreateBatchParams params, com.ScanData.Comm.WTMServices.CreateBatchSort sort, int maxRows) throws java.rmi.RemoteException;
    public com.ScanData.Comm.WTMServices.GetOrderDataResponseGetOrderDataResult getOrderData(java.lang.String distributionCenter, java.lang.String lineOfBusiness, java.lang.String orderKey) throws java.rmi.RemoteException;
    public com.ScanData.Comm.WTMServices.RejectOrderResponseRejectOrderResult rejectOrder(java.lang.String distributionCenter, java.lang.String lineOfBusiness, com.ScanData.Comm.WTMServices.RejectOrderRejectOrderParams rejectOrderParams) throws java.rmi.RemoteException;
    public com.ScanData.Comm.WTMServices.GetPrintBatchOrdersResponseGetPrintBatchOrdersResult getPrintBatchOrders(java.lang.String distributionCenter, java.lang.String lineOfBusiness, java.lang.String printBatchID) throws java.rmi.RemoteException;
    public com.ScanData.Comm.WTMServices.PrintOrderResponsePrintOrderResult printOrder(java.lang.String distributionCenter, java.lang.String lineOfBusiness, java.lang.String orderKey) throws java.rmi.RemoteException;
    public com.ScanData.Comm.WTMServices.PrintBatchResponsePrintBatchResult printBatch(java.lang.String distributionCenter, java.lang.String lineOfBusiness, java.lang.String printBatchID, int startSequence, int endSequence) throws java.rmi.RemoteException;
    public com.ScanData.Comm.WTMServices.GetBatchHistoryResponseGetBatchHistoryResult getBatchHistory(java.lang.String distributionCenter, java.lang.String lineOfBusiness, java.util.Calendar requestDate) throws java.rmi.RemoteException;
    public com.ScanData.Comm.WTMServices.GetVendorStatusResponseGetVendorStatusResult getVendorStatus(java.lang.String requestedDate) throws java.rmi.RemoteException;
    public com.ScanData.Comm.WTMServices.GetPLDUpdateResponseGetPLDUpdateResult getPLDUpdate(java.lang.String lineOfBusiness, java.lang.String status, java.lang.String startDateTime, java.lang.String endDateTime) throws java.rmi.RemoteException;
    public com.ScanData.Comm.WTMServices.GetOrderStatusResponseGetOrderStatusResult getOrderStatus(com.ScanData.Comm.WTMServices.GetOrderStatusOrderStatusParams orderStatusParams) throws java.rmi.RemoteException;
}
