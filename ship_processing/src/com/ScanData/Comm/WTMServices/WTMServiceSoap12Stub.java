/**
 * WTMServiceSoap12Stub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WTMServices;

public class WTMServiceSoap12Stub extends org.apache.axis.client.Stub implements com.ScanData.Comm.WTMServices.WTMServiceSoap_PortType {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[19];
        _initOperationDesc1();
        _initOperationDesc2();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("HelloWorld");
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        oper.setReturnClass(java.lang.String.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/", "HelloWorldResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetCartonLabelImage");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "CartonNumber"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        oper.setReturnClass(java.lang.String.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/", "GetCartonLabelImageResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetCartonLabelURL");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "CartonNumber"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        oper.setReturnClass(java.lang.String.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/", "GetCartonLabelURLResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetDistributionCenterList");
        oper.setReturnType(new javax.xml.namespace.QName("http://ScanData.com/", ">>GetDistributionCenterListResponse>GetDistributionCenterListResult"));
        oper.setReturnClass(com.ScanData.Comm.WTMServices.GetDistributionCenterListResponseGetDistributionCenterListResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/", "GetDistributionCenterListResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetLineOfBusinessList");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "DistributionCenter"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ScanData.com/", ">>GetLineOfBusinessListResponse>GetLineOfBusinessListResult"));
        oper.setReturnClass(com.ScanData.Comm.WTMServices.GetLineOfBusinessListResponseGetLineOfBusinessListResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/", "GetLineOfBusinessListResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetAggregateCounts");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "DistributionCenter"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "LineOfBusiness"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "Status"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "DatePlannedShipment"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"), java.util.Calendar.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "NumberOfDays"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ScanData.com/", ">>GetAggregateCountsResponse>GetAggregateCountsResult"));
        oper.setReturnClass(com.ScanData.Comm.WTMServices.GetAggregateCountsResponseGetAggregateCountsResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/", "GetAggregateCountsResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetAggregateCountsSearch");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "DistributionCenter"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "LineOfBusiness"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "Status"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "Params"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ScanData.com/", ">>GetAggregateCountsSearch>Params"), com.ScanData.Comm.WTMServices.GetAggregateCountsSearchParams.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ScanData.com/", ">>GetAggregateCountsSearchResponse>GetAggregateCountsSearchResult"));
        oper.setReturnClass(com.ScanData.Comm.WTMServices.GetAggregateCountsSearchResponseGetAggregateCountsSearchResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/", "GetAggregateCountsSearchResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[6] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetMultipleOrderData");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "DistributionCenter"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "LineOfBusiness"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "Status"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "DatePlannedShipment"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"), java.util.Calendar.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "NumberOfDays"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "MaxRows"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ScanData.com/", ">>GetMultipleOrderDataResponse>GetMultipleOrderDataResult"));
        oper.setReturnClass(com.ScanData.Comm.WTMServices.GetMultipleOrderDataResponseGetMultipleOrderDataResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/", "GetMultipleOrderDataResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[7] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetMultipleOrderDataSearch");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "DistributionCenter"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "LineOfBusiness"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "Status"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "Params"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ScanData.com/", ">>GetMultipleOrderDataSearch>Params"), com.ScanData.Comm.WTMServices.GetMultipleOrderDataSearchParams.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "Sort"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ScanData.com/", ">>GetMultipleOrderDataSearch>Sort"), com.ScanData.Comm.WTMServices.GetMultipleOrderDataSearchSort.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "MaxRows"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ScanData.com/", ">>GetMultipleOrderDataSearchResponse>GetMultipleOrderDataSearchResult"));
        oper.setReturnClass(com.ScanData.Comm.WTMServices.GetMultipleOrderDataSearchResponseGetMultipleOrderDataSearchResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/", "GetMultipleOrderDataSearchResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[8] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("CreateBatch");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "DistributionCenter"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "LineOfBusiness"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "Params"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ScanData.com/", ">>CreateBatch>Params"), com.ScanData.Comm.WTMServices.CreateBatchParams.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "Sort"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ScanData.com/", ">>CreateBatch>Sort"), com.ScanData.Comm.WTMServices.CreateBatchSort.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "MaxRows"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ScanData.com/", ">>CreateBatchResponse>CreateBatchResult"));
        oper.setReturnClass(com.ScanData.Comm.WTMServices.CreateBatchResponseCreateBatchResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/", "CreateBatchResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[9] = oper;

    }

    private static void _initOperationDesc2(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetOrderData");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "DistributionCenter"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "LineOfBusiness"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "OrderKey"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ScanData.com/", ">>GetOrderDataResponse>GetOrderDataResult"));
        oper.setReturnClass(com.ScanData.Comm.WTMServices.GetOrderDataResponseGetOrderDataResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/", "GetOrderDataResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[10] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("RejectOrder");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "DistributionCenter"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "LineOfBusiness"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "RejectOrderParams"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ScanData.com/", ">>RejectOrder>RejectOrderParams"), com.ScanData.Comm.WTMServices.RejectOrderRejectOrderParams.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ScanData.com/", ">>RejectOrderResponse>RejectOrderResult"));
        oper.setReturnClass(com.ScanData.Comm.WTMServices.RejectOrderResponseRejectOrderResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/", "RejectOrderResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[11] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetPrintBatchOrders");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "DistributionCenter"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "LineOfBusiness"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "PrintBatchID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ScanData.com/", ">>GetPrintBatchOrdersResponse>GetPrintBatchOrdersResult"));
        oper.setReturnClass(com.ScanData.Comm.WTMServices.GetPrintBatchOrdersResponseGetPrintBatchOrdersResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/", "GetPrintBatchOrdersResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[12] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("PrintOrder");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "DistributionCenter"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "LineOfBusiness"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "OrderKey"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ScanData.com/", ">>PrintOrderResponse>PrintOrderResult"));
        oper.setReturnClass(com.ScanData.Comm.WTMServices.PrintOrderResponsePrintOrderResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/", "PrintOrderResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[13] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("PrintBatch");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "DistributionCenter"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "LineOfBusiness"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "PrintBatchID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "StartSequence"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "EndSequence"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ScanData.com/", ">>PrintBatchResponse>PrintBatchResult"));
        oper.setReturnClass(com.ScanData.Comm.WTMServices.PrintBatchResponsePrintBatchResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/", "PrintBatchResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[14] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetBatchHistory");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "DistributionCenter"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "LineOfBusiness"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "RequestDate"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"), java.util.Calendar.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ScanData.com/", ">>GetBatchHistoryResponse>GetBatchHistoryResult"));
        oper.setReturnClass(com.ScanData.Comm.WTMServices.GetBatchHistoryResponseGetBatchHistoryResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/", "GetBatchHistoryResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[15] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetVendorStatus");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "RequestedDate"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ScanData.com/", ">>GetVendorStatusResponse>GetVendorStatusResult"));
        oper.setReturnClass(com.ScanData.Comm.WTMServices.GetVendorStatusResponseGetVendorStatusResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/", "GetVendorStatusResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[16] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetPLDUpdate");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "LineOfBusiness"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "Status"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "StartDateTime"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "EndDateTime"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ScanData.com/", ">>GetPLDUpdateResponse>GetPLDUpdateResult"));
        oper.setReturnClass(com.ScanData.Comm.WTMServices.GetPLDUpdateResponseGetPLDUpdateResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/", "GetPLDUpdateResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[17] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetOrderStatus");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/", "OrderStatusParams"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ScanData.com/", ">>GetOrderStatus>OrderStatusParams"), com.ScanData.Comm.WTMServices.GetOrderStatusOrderStatusParams.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ScanData.com/", ">>GetOrderStatusResponse>GetOrderStatusResult"));
        oper.setReturnClass(com.ScanData.Comm.WTMServices.GetOrderStatusResponseGetOrderStatusResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/", "GetOrderStatusResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[18] = oper;

    }

    public WTMServiceSoap12Stub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public WTMServiceSoap12Stub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public WTMServiceSoap12Stub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">>CreateBatch>Params");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.CreateBatchParams.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">>CreateBatch>Sort");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.CreateBatchSort.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">>CreateBatchResponse>CreateBatchResult");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.CreateBatchResponseCreateBatchResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">>GetAggregateCountsResponse>GetAggregateCountsResult");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.GetAggregateCountsResponseGetAggregateCountsResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">>GetAggregateCountsSearch>Params");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.GetAggregateCountsSearchParams.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">>GetAggregateCountsSearchResponse>GetAggregateCountsSearchResult");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.GetAggregateCountsSearchResponseGetAggregateCountsSearchResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">>GetBatchHistoryResponse>GetBatchHistoryResult");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.GetBatchHistoryResponseGetBatchHistoryResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">>GetDistributionCenterListResponse>GetDistributionCenterListResult");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.GetDistributionCenterListResponseGetDistributionCenterListResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">>GetLineOfBusinessListResponse>GetLineOfBusinessListResult");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.GetLineOfBusinessListResponseGetLineOfBusinessListResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">>GetMultipleOrderDataResponse>GetMultipleOrderDataResult");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.GetMultipleOrderDataResponseGetMultipleOrderDataResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">>GetMultipleOrderDataSearch>Params");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.GetMultipleOrderDataSearchParams.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">>GetMultipleOrderDataSearch>Sort");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.GetMultipleOrderDataSearchSort.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">>GetMultipleOrderDataSearchResponse>GetMultipleOrderDataSearchResult");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.GetMultipleOrderDataSearchResponseGetMultipleOrderDataSearchResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">>GetOrderDataResponse>GetOrderDataResult");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.GetOrderDataResponseGetOrderDataResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">>GetOrderStatus>OrderStatusParams");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.GetOrderStatusOrderStatusParams.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">>GetOrderStatusResponse>GetOrderStatusResult");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.GetOrderStatusResponseGetOrderStatusResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">>GetPLDUpdateResponse>GetPLDUpdateResult");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.GetPLDUpdateResponseGetPLDUpdateResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">>GetPrintBatchOrdersResponse>GetPrintBatchOrdersResult");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.GetPrintBatchOrdersResponseGetPrintBatchOrdersResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">>GetVendorStatusResponse>GetVendorStatusResult");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.GetVendorStatusResponseGetVendorStatusResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">>PrintBatchResponse>PrintBatchResult");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.PrintBatchResponsePrintBatchResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">>PrintOrderResponse>PrintOrderResult");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.PrintOrderResponsePrintOrderResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">>RejectOrder>RejectOrderParams");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.RejectOrderRejectOrderParams.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">>RejectOrderResponse>RejectOrderResult");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.RejectOrderResponseRejectOrderResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">CreateBatch");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.CreateBatch.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">CreateBatchResponse");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.CreateBatchResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">GetAggregateCounts");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.GetAggregateCounts.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">GetAggregateCountsResponse");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.GetAggregateCountsResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">GetAggregateCountsSearch");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.GetAggregateCountsSearch.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">GetAggregateCountsSearchResponse");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.GetAggregateCountsSearchResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">GetBatchHistory");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.GetBatchHistory.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">GetBatchHistoryResponse");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.GetBatchHistoryResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">GetLineOfBusinessList");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.GetLineOfBusinessList.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">GetLineOfBusinessListResponse");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.GetLineOfBusinessListResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">GetMultipleOrderData");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.GetMultipleOrderData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">GetMultipleOrderDataResponse");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.GetMultipleOrderDataResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">GetMultipleOrderDataSearch");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.GetMultipleOrderDataSearch.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">GetMultipleOrderDataSearchResponse");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.GetMultipleOrderDataSearchResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">GetOrderData");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.GetOrderData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">GetOrderDataResponse");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.GetOrderDataResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">GetOrderStatus");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.GetOrderStatus.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">GetOrderStatusResponse");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.GetOrderStatusResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">GetPLDUpdate");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.GetPLDUpdate.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">GetPLDUpdateResponse");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.GetPLDUpdateResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">GetPrintBatchOrders");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.GetPrintBatchOrders.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">GetPrintBatchOrdersResponse");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.GetPrintBatchOrdersResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">GetVendorStatus");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.GetVendorStatus.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">GetVendorStatusResponse");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.GetVendorStatusResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">PrintBatch");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.PrintBatch.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">PrintBatchResponse");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.PrintBatchResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">PrintOrder");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.PrintOrder.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">PrintOrderResponse");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.PrintOrderResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">RejectOrder");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.RejectOrder.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/", ">RejectOrderResponse");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WTMServices.RejectOrderResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public java.lang.String helloWorld() throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/HelloWorld");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/", "HelloWorld"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (java.lang.String) _resp;
            } catch (java.lang.Exception _exception) {
                return (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.String.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public java.lang.String getCartonLabelImage(java.lang.String cartonNumber) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/GetCartonLabelImage");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/", "GetCartonLabelImage"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {cartonNumber});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (java.lang.String) _resp;
            } catch (java.lang.Exception _exception) {
                return (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.String.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public java.lang.String getCartonLabelURL(java.lang.String cartonNumber) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/GetCartonLabelURL");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/", "GetCartonLabelURL"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {cartonNumber});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (java.lang.String) _resp;
            } catch (java.lang.Exception _exception) {
                return (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.String.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.ScanData.Comm.WTMServices.GetDistributionCenterListResponseGetDistributionCenterListResult getDistributionCenterList() throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/GetDistributionCenterList");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/", "GetDistributionCenterList"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.ScanData.Comm.WTMServices.GetDistributionCenterListResponseGetDistributionCenterListResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.ScanData.Comm.WTMServices.GetDistributionCenterListResponseGetDistributionCenterListResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.ScanData.Comm.WTMServices.GetDistributionCenterListResponseGetDistributionCenterListResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.ScanData.Comm.WTMServices.GetLineOfBusinessListResponseGetLineOfBusinessListResult getLineOfBusinessList(java.lang.String distributionCenter) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/GetLineOfBusinessList");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/", "GetLineOfBusinessList"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {distributionCenter});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.ScanData.Comm.WTMServices.GetLineOfBusinessListResponseGetLineOfBusinessListResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.ScanData.Comm.WTMServices.GetLineOfBusinessListResponseGetLineOfBusinessListResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.ScanData.Comm.WTMServices.GetLineOfBusinessListResponseGetLineOfBusinessListResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.ScanData.Comm.WTMServices.GetAggregateCountsResponseGetAggregateCountsResult getAggregateCounts(java.lang.String distributionCenter, java.lang.String lineOfBusiness, java.lang.String status, java.util.Calendar datePlannedShipment, int numberOfDays) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/GetAggregateCounts");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/", "GetAggregateCounts"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {distributionCenter, lineOfBusiness, status, datePlannedShipment, new java.lang.Integer(numberOfDays)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.ScanData.Comm.WTMServices.GetAggregateCountsResponseGetAggregateCountsResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.ScanData.Comm.WTMServices.GetAggregateCountsResponseGetAggregateCountsResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.ScanData.Comm.WTMServices.GetAggregateCountsResponseGetAggregateCountsResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.ScanData.Comm.WTMServices.GetAggregateCountsSearchResponseGetAggregateCountsSearchResult getAggregateCountsSearch(java.lang.String distributionCenter, java.lang.String lineOfBusiness, java.lang.String status, com.ScanData.Comm.WTMServices.GetAggregateCountsSearchParams params) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/GetAggregateCountsSearch");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/", "GetAggregateCountsSearch"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {distributionCenter, lineOfBusiness, status, params});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.ScanData.Comm.WTMServices.GetAggregateCountsSearchResponseGetAggregateCountsSearchResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.ScanData.Comm.WTMServices.GetAggregateCountsSearchResponseGetAggregateCountsSearchResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.ScanData.Comm.WTMServices.GetAggregateCountsSearchResponseGetAggregateCountsSearchResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.ScanData.Comm.WTMServices.GetMultipleOrderDataResponseGetMultipleOrderDataResult getMultipleOrderData(java.lang.String distributionCenter, java.lang.String lineOfBusiness, java.lang.String status, java.util.Calendar datePlannedShipment, int numberOfDays, int maxRows) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[7]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/GetMultipleOrderData");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/", "GetMultipleOrderData"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {distributionCenter, lineOfBusiness, status, datePlannedShipment, new java.lang.Integer(numberOfDays), new java.lang.Integer(maxRows)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.ScanData.Comm.WTMServices.GetMultipleOrderDataResponseGetMultipleOrderDataResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.ScanData.Comm.WTMServices.GetMultipleOrderDataResponseGetMultipleOrderDataResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.ScanData.Comm.WTMServices.GetMultipleOrderDataResponseGetMultipleOrderDataResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.ScanData.Comm.WTMServices.GetMultipleOrderDataSearchResponseGetMultipleOrderDataSearchResult getMultipleOrderDataSearch(java.lang.String distributionCenter, java.lang.String lineOfBusiness, java.lang.String status, com.ScanData.Comm.WTMServices.GetMultipleOrderDataSearchParams params, com.ScanData.Comm.WTMServices.GetMultipleOrderDataSearchSort sort, int maxRows) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[8]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/GetMultipleOrderDataSearch");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/", "GetMultipleOrderDataSearch"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {distributionCenter, lineOfBusiness, status, params, sort, new java.lang.Integer(maxRows)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.ScanData.Comm.WTMServices.GetMultipleOrderDataSearchResponseGetMultipleOrderDataSearchResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.ScanData.Comm.WTMServices.GetMultipleOrderDataSearchResponseGetMultipleOrderDataSearchResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.ScanData.Comm.WTMServices.GetMultipleOrderDataSearchResponseGetMultipleOrderDataSearchResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.ScanData.Comm.WTMServices.CreateBatchResponseCreateBatchResult createBatch(java.lang.String distributionCenter, java.lang.String lineOfBusiness, com.ScanData.Comm.WTMServices.CreateBatchParams params, com.ScanData.Comm.WTMServices.CreateBatchSort sort, int maxRows) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[9]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/CreateBatch");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/", "CreateBatch"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {distributionCenter, lineOfBusiness, params, sort, new java.lang.Integer(maxRows)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.ScanData.Comm.WTMServices.CreateBatchResponseCreateBatchResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.ScanData.Comm.WTMServices.CreateBatchResponseCreateBatchResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.ScanData.Comm.WTMServices.CreateBatchResponseCreateBatchResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.ScanData.Comm.WTMServices.GetOrderDataResponseGetOrderDataResult getOrderData(java.lang.String distributionCenter, java.lang.String lineOfBusiness, java.lang.String orderKey) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[10]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/GetOrderData");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/", "GetOrderData"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {distributionCenter, lineOfBusiness, orderKey});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.ScanData.Comm.WTMServices.GetOrderDataResponseGetOrderDataResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.ScanData.Comm.WTMServices.GetOrderDataResponseGetOrderDataResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.ScanData.Comm.WTMServices.GetOrderDataResponseGetOrderDataResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.ScanData.Comm.WTMServices.RejectOrderResponseRejectOrderResult rejectOrder(java.lang.String distributionCenter, java.lang.String lineOfBusiness, com.ScanData.Comm.WTMServices.RejectOrderRejectOrderParams rejectOrderParams) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[11]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/RejectOrder");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/", "RejectOrder"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {distributionCenter, lineOfBusiness, rejectOrderParams});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.ScanData.Comm.WTMServices.RejectOrderResponseRejectOrderResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.ScanData.Comm.WTMServices.RejectOrderResponseRejectOrderResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.ScanData.Comm.WTMServices.RejectOrderResponseRejectOrderResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.ScanData.Comm.WTMServices.GetPrintBatchOrdersResponseGetPrintBatchOrdersResult getPrintBatchOrders(java.lang.String distributionCenter, java.lang.String lineOfBusiness, java.lang.String printBatchID) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[12]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/GetPrintBatchOrders");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/", "GetPrintBatchOrders"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {distributionCenter, lineOfBusiness, printBatchID});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.ScanData.Comm.WTMServices.GetPrintBatchOrdersResponseGetPrintBatchOrdersResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.ScanData.Comm.WTMServices.GetPrintBatchOrdersResponseGetPrintBatchOrdersResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.ScanData.Comm.WTMServices.GetPrintBatchOrdersResponseGetPrintBatchOrdersResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.ScanData.Comm.WTMServices.PrintOrderResponsePrintOrderResult printOrder(java.lang.String distributionCenter, java.lang.String lineOfBusiness, java.lang.String orderKey) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[13]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/PrintOrder");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/", "PrintOrder"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {distributionCenter, lineOfBusiness, orderKey});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.ScanData.Comm.WTMServices.PrintOrderResponsePrintOrderResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.ScanData.Comm.WTMServices.PrintOrderResponsePrintOrderResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.ScanData.Comm.WTMServices.PrintOrderResponsePrintOrderResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.ScanData.Comm.WTMServices.PrintBatchResponsePrintBatchResult printBatch(java.lang.String distributionCenter, java.lang.String lineOfBusiness, java.lang.String printBatchID, int startSequence, int endSequence) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[14]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/PrintBatch");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/", "PrintBatch"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {distributionCenter, lineOfBusiness, printBatchID, new java.lang.Integer(startSequence), new java.lang.Integer(endSequence)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.ScanData.Comm.WTMServices.PrintBatchResponsePrintBatchResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.ScanData.Comm.WTMServices.PrintBatchResponsePrintBatchResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.ScanData.Comm.WTMServices.PrintBatchResponsePrintBatchResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.ScanData.Comm.WTMServices.GetBatchHistoryResponseGetBatchHistoryResult getBatchHistory(java.lang.String distributionCenter, java.lang.String lineOfBusiness, java.util.Calendar requestDate) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[15]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/GetBatchHistory");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/", "GetBatchHistory"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {distributionCenter, lineOfBusiness, requestDate});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.ScanData.Comm.WTMServices.GetBatchHistoryResponseGetBatchHistoryResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.ScanData.Comm.WTMServices.GetBatchHistoryResponseGetBatchHistoryResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.ScanData.Comm.WTMServices.GetBatchHistoryResponseGetBatchHistoryResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.ScanData.Comm.WTMServices.GetVendorStatusResponseGetVendorStatusResult getVendorStatus(java.lang.String requestedDate) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[16]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/GetVendorStatus");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/", "GetVendorStatus"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {requestedDate});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.ScanData.Comm.WTMServices.GetVendorStatusResponseGetVendorStatusResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.ScanData.Comm.WTMServices.GetVendorStatusResponseGetVendorStatusResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.ScanData.Comm.WTMServices.GetVendorStatusResponseGetVendorStatusResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.ScanData.Comm.WTMServices.GetPLDUpdateResponseGetPLDUpdateResult getPLDUpdate(java.lang.String lineOfBusiness, java.lang.String status, java.lang.String startDateTime, java.lang.String endDateTime) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[17]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/GetPLDUpdate");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/", "GetPLDUpdate"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {lineOfBusiness, status, startDateTime, endDateTime});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.ScanData.Comm.WTMServices.GetPLDUpdateResponseGetPLDUpdateResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.ScanData.Comm.WTMServices.GetPLDUpdateResponseGetPLDUpdateResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.ScanData.Comm.WTMServices.GetPLDUpdateResponseGetPLDUpdateResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.ScanData.Comm.WTMServices.GetOrderStatusResponseGetOrderStatusResult getOrderStatus(com.ScanData.Comm.WTMServices.GetOrderStatusOrderStatusParams orderStatusParams) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[18]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/GetOrderStatus");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/", "GetOrderStatus"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {orderStatusParams});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.ScanData.Comm.WTMServices.GetOrderStatusResponseGetOrderStatusResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.ScanData.Comm.WTMServices.GetOrderStatusResponseGetOrderStatusResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.ScanData.Comm.WTMServices.GetOrderStatusResponseGetOrderStatusResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
