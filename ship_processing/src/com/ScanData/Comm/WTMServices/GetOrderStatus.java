/**
 * GetOrderStatus.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WTMServices;

public class GetOrderStatus  implements java.io.Serializable {
    private com.ScanData.Comm.WTMServices.GetOrderStatusOrderStatusParams orderStatusParams;

    public GetOrderStatus() {
    }

    public GetOrderStatus(
           com.ScanData.Comm.WTMServices.GetOrderStatusOrderStatusParams orderStatusParams) {
           this.orderStatusParams = orderStatusParams;
    }


    /**
     * Gets the orderStatusParams value for this GetOrderStatus.
     * 
     * @return orderStatusParams
     */
    public com.ScanData.Comm.WTMServices.GetOrderStatusOrderStatusParams getOrderStatusParams() {
        return orderStatusParams;
    }


    /**
     * Sets the orderStatusParams value for this GetOrderStatus.
     * 
     * @param orderStatusParams
     */
    public void setOrderStatusParams(com.ScanData.Comm.WTMServices.GetOrderStatusOrderStatusParams orderStatusParams) {
        this.orderStatusParams = orderStatusParams;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetOrderStatus)) return false;
        GetOrderStatus other = (GetOrderStatus) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.orderStatusParams==null && other.getOrderStatusParams()==null) || 
             (this.orderStatusParams!=null &&
              this.orderStatusParams.equals(other.getOrderStatusParams())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getOrderStatusParams() != null) {
            _hashCode += getOrderStatusParams().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetOrderStatus.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/", ">GetOrderStatus"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orderStatusParams");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/", "OrderStatusParams"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/", ">>GetOrderStatus>OrderStatusParams"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
