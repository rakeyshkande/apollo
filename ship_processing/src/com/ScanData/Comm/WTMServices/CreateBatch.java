/**
 * CreateBatch.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WTMServices;

public class CreateBatch  implements java.io.Serializable {
    private java.lang.String distributionCenter;

    private java.lang.String lineOfBusiness;

    private com.ScanData.Comm.WTMServices.CreateBatchParams params;

    private com.ScanData.Comm.WTMServices.CreateBatchSort sort;

    private int maxRows;

    public CreateBatch() {
    }

    public CreateBatch(
           java.lang.String distributionCenter,
           java.lang.String lineOfBusiness,
           com.ScanData.Comm.WTMServices.CreateBatchParams params,
           com.ScanData.Comm.WTMServices.CreateBatchSort sort,
           int maxRows) {
           this.distributionCenter = distributionCenter;
           this.lineOfBusiness = lineOfBusiness;
           this.params = params;
           this.sort = sort;
           this.maxRows = maxRows;
    }


    /**
     * Gets the distributionCenter value for this CreateBatch.
     * 
     * @return distributionCenter
     */
    public java.lang.String getDistributionCenter() {
        return distributionCenter;
    }


    /**
     * Sets the distributionCenter value for this CreateBatch.
     * 
     * @param distributionCenter
     */
    public void setDistributionCenter(java.lang.String distributionCenter) {
        this.distributionCenter = distributionCenter;
    }


    /**
     * Gets the lineOfBusiness value for this CreateBatch.
     * 
     * @return lineOfBusiness
     */
    public java.lang.String getLineOfBusiness() {
        return lineOfBusiness;
    }


    /**
     * Sets the lineOfBusiness value for this CreateBatch.
     * 
     * @param lineOfBusiness
     */
    public void setLineOfBusiness(java.lang.String lineOfBusiness) {
        this.lineOfBusiness = lineOfBusiness;
    }


    /**
     * Gets the params value for this CreateBatch.
     * 
     * @return params
     */
    public com.ScanData.Comm.WTMServices.CreateBatchParams getParams() {
        return params;
    }


    /**
     * Sets the params value for this CreateBatch.
     * 
     * @param params
     */
    public void setParams(com.ScanData.Comm.WTMServices.CreateBatchParams params) {
        this.params = params;
    }


    /**
     * Gets the sort value for this CreateBatch.
     * 
     * @return sort
     */
    public com.ScanData.Comm.WTMServices.CreateBatchSort getSort() {
        return sort;
    }


    /**
     * Sets the sort value for this CreateBatch.
     * 
     * @param sort
     */
    public void setSort(com.ScanData.Comm.WTMServices.CreateBatchSort sort) {
        this.sort = sort;
    }


    /**
     * Gets the maxRows value for this CreateBatch.
     * 
     * @return maxRows
     */
    public int getMaxRows() {
        return maxRows;
    }


    /**
     * Sets the maxRows value for this CreateBatch.
     * 
     * @param maxRows
     */
    public void setMaxRows(int maxRows) {
        this.maxRows = maxRows;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CreateBatch)) return false;
        CreateBatch other = (CreateBatch) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.distributionCenter==null && other.getDistributionCenter()==null) || 
             (this.distributionCenter!=null &&
              this.distributionCenter.equals(other.getDistributionCenter()))) &&
            ((this.lineOfBusiness==null && other.getLineOfBusiness()==null) || 
             (this.lineOfBusiness!=null &&
              this.lineOfBusiness.equals(other.getLineOfBusiness()))) &&
            ((this.params==null && other.getParams()==null) || 
             (this.params!=null &&
              this.params.equals(other.getParams()))) &&
            ((this.sort==null && other.getSort()==null) || 
             (this.sort!=null &&
              this.sort.equals(other.getSort()))) &&
            this.maxRows == other.getMaxRows();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDistributionCenter() != null) {
            _hashCode += getDistributionCenter().hashCode();
        }
        if (getLineOfBusiness() != null) {
            _hashCode += getLineOfBusiness().hashCode();
        }
        if (getParams() != null) {
            _hashCode += getParams().hashCode();
        }
        if (getSort() != null) {
            _hashCode += getSort().hashCode();
        }
        _hashCode += getMaxRows();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CreateBatch.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/", ">CreateBatch"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("distributionCenter");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/", "DistributionCenter"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lineOfBusiness");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/", "LineOfBusiness"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("params");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/", "Params"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/", ">>CreateBatch>Params"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sort");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/", "Sort"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/", ">>CreateBatch>Sort"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("maxRows");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/", "MaxRows"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
