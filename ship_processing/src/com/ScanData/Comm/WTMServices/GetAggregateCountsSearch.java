/**
 * GetAggregateCountsSearch.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WTMServices;

public class GetAggregateCountsSearch  implements java.io.Serializable {
    private java.lang.String distributionCenter;

    private java.lang.String lineOfBusiness;

    private java.lang.String status;

    private com.ScanData.Comm.WTMServices.GetAggregateCountsSearchParams params;

    public GetAggregateCountsSearch() {
    }

    public GetAggregateCountsSearch(
           java.lang.String distributionCenter,
           java.lang.String lineOfBusiness,
           java.lang.String status,
           com.ScanData.Comm.WTMServices.GetAggregateCountsSearchParams params) {
           this.distributionCenter = distributionCenter;
           this.lineOfBusiness = lineOfBusiness;
           this.status = status;
           this.params = params;
    }


    /**
     * Gets the distributionCenter value for this GetAggregateCountsSearch.
     * 
     * @return distributionCenter
     */
    public java.lang.String getDistributionCenter() {
        return distributionCenter;
    }


    /**
     * Sets the distributionCenter value for this GetAggregateCountsSearch.
     * 
     * @param distributionCenter
     */
    public void setDistributionCenter(java.lang.String distributionCenter) {
        this.distributionCenter = distributionCenter;
    }


    /**
     * Gets the lineOfBusiness value for this GetAggregateCountsSearch.
     * 
     * @return lineOfBusiness
     */
    public java.lang.String getLineOfBusiness() {
        return lineOfBusiness;
    }


    /**
     * Sets the lineOfBusiness value for this GetAggregateCountsSearch.
     * 
     * @param lineOfBusiness
     */
    public void setLineOfBusiness(java.lang.String lineOfBusiness) {
        this.lineOfBusiness = lineOfBusiness;
    }


    /**
     * Gets the status value for this GetAggregateCountsSearch.
     * 
     * @return status
     */
    public java.lang.String getStatus() {
        return status;
    }


    /**
     * Sets the status value for this GetAggregateCountsSearch.
     * 
     * @param status
     */
    public void setStatus(java.lang.String status) {
        this.status = status;
    }


    /**
     * Gets the params value for this GetAggregateCountsSearch.
     * 
     * @return params
     */
    public com.ScanData.Comm.WTMServices.GetAggregateCountsSearchParams getParams() {
        return params;
    }


    /**
     * Sets the params value for this GetAggregateCountsSearch.
     * 
     * @param params
     */
    public void setParams(com.ScanData.Comm.WTMServices.GetAggregateCountsSearchParams params) {
        this.params = params;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetAggregateCountsSearch)) return false;
        GetAggregateCountsSearch other = (GetAggregateCountsSearch) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.distributionCenter==null && other.getDistributionCenter()==null) || 
             (this.distributionCenter!=null &&
              this.distributionCenter.equals(other.getDistributionCenter()))) &&
            ((this.lineOfBusiness==null && other.getLineOfBusiness()==null) || 
             (this.lineOfBusiness!=null &&
              this.lineOfBusiness.equals(other.getLineOfBusiness()))) &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.params==null && other.getParams()==null) || 
             (this.params!=null &&
              this.params.equals(other.getParams())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDistributionCenter() != null) {
            _hashCode += getDistributionCenter().hashCode();
        }
        if (getLineOfBusiness() != null) {
            _hashCode += getLineOfBusiness().hashCode();
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getParams() != null) {
            _hashCode += getParams().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetAggregateCountsSearch.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/", ">GetAggregateCountsSearch"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("distributionCenter");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/", "DistributionCenter"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lineOfBusiness");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/", "LineOfBusiness"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/", "Status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("params");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/", "Params"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/", ">>GetAggregateCountsSearch>Params"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
