/**
 * GetOrderDataResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WTMServices;

public class GetOrderDataResponse  implements java.io.Serializable {
    private com.ScanData.Comm.WTMServices.GetOrderDataResponseGetOrderDataResult getOrderDataResult;

    public GetOrderDataResponse() {
    }

    public GetOrderDataResponse(
           com.ScanData.Comm.WTMServices.GetOrderDataResponseGetOrderDataResult getOrderDataResult) {
           this.getOrderDataResult = getOrderDataResult;
    }


    /**
     * Gets the getOrderDataResult value for this GetOrderDataResponse.
     * 
     * @return getOrderDataResult
     */
    public com.ScanData.Comm.WTMServices.GetOrderDataResponseGetOrderDataResult getGetOrderDataResult() {
        return getOrderDataResult;
    }


    /**
     * Sets the getOrderDataResult value for this GetOrderDataResponse.
     * 
     * @param getOrderDataResult
     */
    public void setGetOrderDataResult(com.ScanData.Comm.WTMServices.GetOrderDataResponseGetOrderDataResult getOrderDataResult) {
        this.getOrderDataResult = getOrderDataResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetOrderDataResponse)) return false;
        GetOrderDataResponse other = (GetOrderDataResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getOrderDataResult==null && other.getGetOrderDataResult()==null) || 
             (this.getOrderDataResult!=null &&
              this.getOrderDataResult.equals(other.getGetOrderDataResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetOrderDataResult() != null) {
            _hashCode += getGetOrderDataResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetOrderDataResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/", ">GetOrderDataResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getOrderDataResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/", "GetOrderDataResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/", ">>GetOrderDataResponse>GetOrderDataResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
