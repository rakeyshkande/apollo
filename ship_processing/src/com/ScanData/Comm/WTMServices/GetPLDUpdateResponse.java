/**
 * GetPLDUpdateResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WTMServices;

public class GetPLDUpdateResponse  implements java.io.Serializable {
    private com.ScanData.Comm.WTMServices.GetPLDUpdateResponseGetPLDUpdateResult getPLDUpdateResult;

    public GetPLDUpdateResponse() {
    }

    public GetPLDUpdateResponse(
           com.ScanData.Comm.WTMServices.GetPLDUpdateResponseGetPLDUpdateResult getPLDUpdateResult) {
           this.getPLDUpdateResult = getPLDUpdateResult;
    }


    /**
     * Gets the getPLDUpdateResult value for this GetPLDUpdateResponse.
     * 
     * @return getPLDUpdateResult
     */
    public com.ScanData.Comm.WTMServices.GetPLDUpdateResponseGetPLDUpdateResult getGetPLDUpdateResult() {
        return getPLDUpdateResult;
    }


    /**
     * Sets the getPLDUpdateResult value for this GetPLDUpdateResponse.
     * 
     * @param getPLDUpdateResult
     */
    public void setGetPLDUpdateResult(com.ScanData.Comm.WTMServices.GetPLDUpdateResponseGetPLDUpdateResult getPLDUpdateResult) {
        this.getPLDUpdateResult = getPLDUpdateResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetPLDUpdateResponse)) return false;
        GetPLDUpdateResponse other = (GetPLDUpdateResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getPLDUpdateResult==null && other.getGetPLDUpdateResult()==null) || 
             (this.getPLDUpdateResult!=null &&
              this.getPLDUpdateResult.equals(other.getGetPLDUpdateResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetPLDUpdateResult() != null) {
            _hashCode += getGetPLDUpdateResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetPLDUpdateResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/", ">GetPLDUpdateResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getPLDUpdateResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/", "GetPLDUpdateResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/", ">>GetPLDUpdateResponse>GetPLDUpdateResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
