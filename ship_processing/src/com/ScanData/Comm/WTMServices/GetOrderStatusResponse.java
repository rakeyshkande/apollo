/**
 * GetOrderStatusResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WTMServices;

public class GetOrderStatusResponse  implements java.io.Serializable {
    private com.ScanData.Comm.WTMServices.GetOrderStatusResponseGetOrderStatusResult getOrderStatusResult;

    public GetOrderStatusResponse() {
    }

    public GetOrderStatusResponse(
           com.ScanData.Comm.WTMServices.GetOrderStatusResponseGetOrderStatusResult getOrderStatusResult) {
           this.getOrderStatusResult = getOrderStatusResult;
    }


    /**
     * Gets the getOrderStatusResult value for this GetOrderStatusResponse.
     * 
     * @return getOrderStatusResult
     */
    public com.ScanData.Comm.WTMServices.GetOrderStatusResponseGetOrderStatusResult getGetOrderStatusResult() {
        return getOrderStatusResult;
    }


    /**
     * Sets the getOrderStatusResult value for this GetOrderStatusResponse.
     * 
     * @param getOrderStatusResult
     */
    public void setGetOrderStatusResult(com.ScanData.Comm.WTMServices.GetOrderStatusResponseGetOrderStatusResult getOrderStatusResult) {
        this.getOrderStatusResult = getOrderStatusResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetOrderStatusResponse)) return false;
        GetOrderStatusResponse other = (GetOrderStatusResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getOrderStatusResult==null && other.getGetOrderStatusResult()==null) || 
             (this.getOrderStatusResult!=null &&
              this.getOrderStatusResult.equals(other.getGetOrderStatusResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetOrderStatusResult() != null) {
            _hashCode += getGetOrderStatusResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetOrderStatusResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/", ">GetOrderStatusResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getOrderStatusResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/", "GetOrderStatusResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/", ">>GetOrderStatusResponse>GetOrderStatusResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
