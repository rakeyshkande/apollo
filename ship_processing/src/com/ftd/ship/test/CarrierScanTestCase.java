package com.ftd.ship.test;

import com.ftd.ship.bo.CarrierScansBO;

import com.ftd.ship.bo.DHLScansBO;
import com.ftd.ship.bo.FedExScansBO;
import com.ftd.ship.bo.UPSScansBO;
import com.ftd.ship.common.JMSPipeline;
import com.ftd.ship.common.framework.util.CommonUtils;
import com.ftd.ship.dao.ShipDAO;
import com.ftd.ship.vo.CarrierScanVO;

import com.ftd.ship.vo.DHLScanVO;

import java.io.File;

import java.io.FileInputStream;
import java.io.IOException;

import java.math.BigDecimal;

import java.sql.Connection;

import java.util.HashMap;
import java.util.List;

import javax.naming.InitialContext;

import junit.framework.TestSuite;

import org.apache.commons.io.FileUtils;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class CarrierScanTestCase extends SDSTestCase {
    Connection conn;
    ApplicationContext context;
    public CarrierScanTestCase(String sTestName) {
        super(sTestName);
    }

    public void setUp() throws Exception
    {
        //Start up spring framework
        context = new FileSystemXmlApplicationContext("C:\\_RLSE_2.3\\FTD\\ship_processing\\web\\WEB-INF\\spring-ship-servlet.xml");
        this.conn = resourceProvider.getDatabaseConnection();
    }
    
    public void tearDown() throws Exception
    {
        if( conn!=null && !conn.isClosed() ) {
            conn.close();    
        }
    }
    
    public static TestSuite suite()
    { 
        TestSuite suite = null;
        
        try 
        {
            suite = new TestSuite(ShipRequestTestCase.class);
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        
        return suite;
    }
    
    public void testPopulateDHLScanVO() {
        String scanLine = "\"061101\",\"1023\",\"00001\",\"62094478294\",\"00968053004\",\"00790696504\",\"061030\",\"1607\",\"LD\",\"061101\",\"1200\",\"00001\",\"LD FD -4011 13TH       RD     \",\"MXG\",\"TOP\",\"A\",\"CD\",\"O\",\"15631533493540           \"";
        DHLScanVO vo = new DHLScanVO(scanLine);
        
        System.out.println("batchDate: "+vo.getBatchDate());
        System.out.println("batchTime: "+vo.getBatchTime());
        System.out.println("batchRecordSequence: "+vo.getBatchRecordSequence());
        System.out.println("trackingNumber: "+vo.getTrackingNumber());
        System.out.println("masterInvoiceNumber: "+vo.getMasterInvoiceNumber());
        System.out.println("customerNumber: "+vo.getCustomerNumber());
        System.out.println("pickupDate: "+vo.getPickupDate());
        System.out.println("pickupTime: "+vo.getPickupTime());
        System.out.println("shipmentStatusEventCode: "+vo.getShipmentStatusEventCode());
        System.out.println("eventDate: "+vo.getEventDate());
        System.out.println("eventTime: "+vo.getEventTime());
        System.out.println("numberOfPieces: "+vo.getNumberOfPieces());
        System.out.println("comment: "+vo.getComment());
        System.out.println("originStation: "+vo.getOriginStation());
        System.out.println("destinationStation: "+vo.getDestinationStation());
        System.out.println("serviceLevelInd: "+vo.getServiceLevelInd());
        System.out.println("productType: "+vo.getProductType());
        System.out.println("packagingType: "+vo.getPackagingType());
        System.out.println("customerReferenceNumber: "+vo.getCustomerReferenceNumber());
    }
    
    /**
     * Tests the parsing of a "DHL" file
     */
    public void testParseDHLFile() throws Exception {
//        String fileName = "SSIFMT04P.1954";
//        String localDirectory = "C:\\Documents and Settings\\tpeterson\\My Documents\\_scans\\DHL\\";
        String fileName = "scanfile.txt";
        String localDirectory = "C:\\temp\\work";
        
        DHLScansBO bo = (DHLScansBO)context.getBean("dhlScansBO");
        List<CarrierScanVO> list = bo.parseScanFile(fileName, localDirectory);
        HashMap<String,String> map = new HashMap<String,String>();
        
        for( int idx=0; idx < list.size(); idx++ ) {
            CarrierScanVO vo = list.get(idx);
//            StringBuilder sb = new StringBuilder();
//            sb.append("Carrier Id: ");
//            sb.append(vo.getCarrierId());
//            sb.append("\r\nFile Name: ");
//            sb.append(vo.getFileName());
//            sb.append("\r\nId: ");
//            sb.append(vo.getId());
//            sb.append("\r\nOrder Id: ");
//            sb.append(vo.getOrderId());
//            sb.append("\r\nScan Type: ");
//            sb.append(vo.getScanType());
//            sb.append("\r\nTimestamp: ");
//            sb.append(vo.getTimestamp());
//            sb.append("\r\nTracking Number: ");
//            sb.append(vo.getTrackingNumber());
//            
//            System.out.println(sb.toString());
//            System.out.println();

            if( map.containsKey(vo.getOrderId()) ) {
                System.out.println("Duplicate of "+vo.getOrderId()+ " found");
            }
        }
        
        System.out.println("Processed "+list.size()+" records");
    }
    
    /**
     * Tests the parsing of a FedEx scan file
     * @throws Exception
     */
    public void testParseFedexFile() throws Exception {
        String fileName = "200611080158.t";
        String localDirectory = "C:\\Documents and Settings\\tpeterson\\My Documents\\_scans\\FEDEX";
        
        FedExScansBO bo = (FedExScansBO)context.getBean("fedexScansBO");
        List<CarrierScanVO> list = bo.parseScanFile(fileName, localDirectory);
        
        for( int idx=0; idx < list.size(); idx++ ) {
            CarrierScanVO vo = list.get(idx);
            StringBuilder sb = new StringBuilder();
            sb.append("Carrier Id: ");
            sb.append(vo.getCarrierId());
            sb.append("\r\nFile Name: ");
            sb.append(vo.getFileName());
            sb.append("\r\nId: ");
            sb.append(vo.getId());
            sb.append("\r\nOrder Id: ");
            sb.append(vo.getOrderId());
            sb.append("\r\nScan Type: ");
            sb.append(vo.getScanType());
            sb.append("\r\nTimestamp: ");
            sb.append(vo.getTimestamp());
            sb.append("\r\nTracking Number: ");
            sb.append(vo.getTrackingNumber());
            
            System.out.println(sb.toString());
            System.out.println();
        }
    }
    
    public void testGetFilesLocally() {
        ShipDAO shipDAO = shipDAO = (ShipDAO)context.getBean("shipDAO");
        File scansDir = new File("C:/temp/scans/work");
        File archiveDir = new File("C:/temp/scans/archive");
        String carrierId = "DHL";
        
        File[] files = scansDir.listFiles();
        int ctr = 0;
            
        for (int i = 0; i < files.length; i++) {
            File scanFile = files[i];
            String fileName = scanFile.getName();
            
            try {
                if( scanFile.length()>0 ) {
                    //Upload the file to the server
                    System.out.println("Uploading file: " + fileName);

                    long length = scanFile.length();
                    FileInputStream fis = new FileInputStream(scanFile);
                    int readSize;
                    StringBuilder sb = new StringBuilder();
                    
                    do {
                     if (length > Integer.MAX_VALUE) {
                         readSize = Integer.MAX_VALUE;
                     } else {
                         readSize = (int)length;
                     }    
                     
                     // Create the byte array to hold the data
                     byte[] bytes = new byte[(int)length];
                    
                     // Read in the bytes
                     int offset = 0;
                     int numRead = 0;
                     while (offset < bytes.length && (numRead=fis.read(bytes, offset, bytes.length-offset)) >= 0) {
                         offset += numRead;
                     }
                    
                     // Ensure all the bytes have been read in
                     if (offset < bytes.length) {
                         throw new IOException("Could not completely read file "+fileName);
                     }
                     
                     //Add to the string builder
                     sb.append(new String(bytes));
                    
                     length = length - offset;
                     
                    } while(length > 0);
                    fis.close();
                    
                    shipDAO.insertScanFile(conn,carrierId,fileName,"SP",sb.toString());
                    
                    //Send the JMS message to process
                    System.out.println("Sending JMS message to process file "+fileName);
                    String payload = carrierId+" "+fileName;
                    shipDAO.postAMessage(conn,"OJMS.SDS_PROCESS_SCANS","PROCESSSCANS",payload,300);
                    //CommonUtils.sendJMSMessage(new InitialContext(),carrierId+" "+fileName,JMSPipeline.PROCESSSCANS);
                    System.out.println("Sent JMS message to process file "+fileName);
                                         
                    System.out.println("File uploaded: " + fileName);
                } else {
                    System.out.println("File "+fileName+" is empty.  It will not be moved to the server.");
                }
                
                System.out.println("Copying file "+fileName+" to archive "+archiveDir.getCanonicalPath());
                FileUtils.copyFileToDirectory(scanFile, archiveDir);
                
                System.out.println("Removing "+fileName+" from file system.");
                FileUtils.forceDelete(scanFile);
                
                ++ctr;
                
            } catch (Exception e) {
                System.out.println("Error while archiving file "+fileName+".  Will continue processing...");
                e.printStackTrace();
            } finally {
                System.out.println("Processed "+String.valueOf(ctr)+" files");
            }
        }
    }
    
    /**
     * Tests the parsing of a FedEx scan file
     * @throws Exception
     */
    public void testParseUPSFile() throws Exception {
        String fileName = "Delivery_Notification061221_152146005.xml";
        String localDirectory = "C:\\Documents and Settings\\tpeterson\\My Documents\\_scans\\UPS\\Examples";
        
        UPSScansBO bo = (UPSScansBO)context.getBean("upsScansBO");
        List<CarrierScanVO> list = bo.parseScanFile(fileName, localDirectory);
        
        for( int idx=0; idx < list.size(); idx++ ) {
            CarrierScanVO vo = list.get(idx);
            StringBuilder sb = new StringBuilder();
            sb.append("Carrier Id: ");
            sb.append(vo.getCarrierId());
            sb.append("\r\nFile Name: ");
            sb.append(vo.getFileName());
            sb.append("\r\nId: ");
            sb.append(vo.getId());
            sb.append("\r\nOrder Id: ");
            sb.append(vo.getOrderId());
            sb.append("\r\nScan Type: ");
            sb.append(vo.getScanType());
            sb.append("\r\nTimestamp: ");
            sb.append(vo.getTimestamp());
            sb.append("\r\nTracking Number: ");
            sb.append(vo.getTrackingNumber());
            
            System.out.println(sb.toString());
            System.out.println();
        }
    }
    
    /**
     * Tests the parsing of a "DHL" file and the placing of the scans into
     * the database.
     */
    public void testProcessDHLFile() throws Exception {
        String carrierId = "DHL";
//        String fileName = "SSIFMT04P.16317";
//        String localDirectory = "C:\\Documents and Settings\\tpeterson\\My Documents\\_scans\\DHL\\";

        String fileName = "scanfile.txt";
        String localDirectory = "/tmp";
        
        //CarrierScansBO bo = (CarrierScansBO)context.getBean("carrierScansBO");
        DHLScansBO bo = (DHLScansBO)context.getBean("dhlScansBO");
        
        bo.processScanFile(conn,carrierId,fileName,localDirectory);
    }
    
    public void testProcessScan() throws Exception {
        DHLScansBO bo = (DHLScansBO)context.getBean("dhlScansBO");
        bo.processScan(conn, new BigDecimal(35311));
    }
    
    /**
     * Tests the parsing of a "Fedex" file and the placing of the scans into
     * the database.
     */
    public void testProcessFedexFile() throws Exception {
        String carrierId = "FEDEX";
//        String fileName = "200611080158.t";
        String fileName = "200611080301.t";
        String localDirectory = "C:\\Documents and Settings\\tpeterson\\My Documents\\_scans\\FEDEX";
        
        CarrierScansBO bo = (CarrierScansBO)context.getBean("carrierScansBO");
        
        bo.processScanFile(conn,carrierId,fileName,localDirectory);
    }

    public static void main(String[] args) {
        try {
//            CarrierScanTestCase carrierScanTestCase = new CarrierScanTestCase("testParseDHLFile");
//            CarrierScanTestCase carrierScanTestCase = new CarrierScanTestCase("testParseFedexFile");
//            CarrierScanTestCase carrierScanTestCase = new CarrierScanTestCase("testParseUPSFile");
//            CarrierScanTestCase carrierScanTestCase = new CarrierScanTestCase("testProcessDHLFile");
//            CarrierScanTestCase carrierScanTestCase = new CarrierScanTestCase("testProcessFedexFile");
//            CarrierScanTestCase carrierScanTestCase = new CarrierScanTestCase("testPopulateDHLScanVO");
            CarrierScanTestCase carrierScanTestCase = new CarrierScanTestCase("testGetFilesLocally");
//              CarrierScanTestCase carrierScanTestCase = new CarrierScanTestCase("testProcessScan");
            carrierScanTestCase.runBare();
        } catch (Throwable e) {
            e.printStackTrace();
        } 
    }
}
