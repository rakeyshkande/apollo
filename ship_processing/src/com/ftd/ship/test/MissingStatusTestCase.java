package com.ftd.ship.test;

import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.ship.bo.SDSMissingStatusBO;
import com.ftd.ship.vo.SDSMessageVO;

import java.sql.Connection;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestSuite;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import org.w3c.dom.Document;


public class MissingStatusTestCase extends SDSTestCase {
    Connection conn;
    ApplicationContext context;
    private static final SimpleDateFormat SDS_DATE_TIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
    private static final SimpleDateFormat SDS_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    
    public MissingStatusTestCase(String testCase) {
        super(testCase);
    }
    
    public void setUp() throws Exception
    {
        //Start up spring framework
        context = new FileSystemXmlApplicationContext(SPRING_CONFIG_FILE);
        this.conn = resourceProvider.getDatabaseConnection();
    }
    
    public void tearDown() throws Exception
    {
        if( conn!=null && !conn.isClosed() ) {
            conn.close();    
        }
    }
    
    public static TestSuite suite()
    { 
        TestSuite suite = null;
        
        try 
        {
            suite = new TestSuite(InboundTestCase.class);
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        
        return suite;
    }
    
    /**
     * Test the creation of a  "GetOrdersStatus" request xml document by calling
     * SDSMissingStatusBO.buildRequest
     */
    public void testBuildRequest() throws Throwable {
        List<String> cartonNumbers = new ArrayList<String>();
        cartonNumbers.add("E7019533");
        cartonNumbers.add("E7019534");
        cartonNumbers.add("E7019535");
        cartonNumbers.add("E7019536");
        cartonNumbers.add("E7019537");
        cartonNumbers.add("E7019538");
        SDSMissingStatusBO bo = (SDSMissingStatusBO)context.getBean("sdsMissingStatusBO");
        Document doc = bo.buildRequest(cartonNumbers);
        System.out.println(JAXPUtil.toString(doc));
    }

    /**
     * Test the retrieval of missing status's by making a call to
     * ShipDAO.getMissingStatus
     * @throws Throwable
     */
    public void testMissingStatusDAO() throws Throwable {
        List<String> cartonNumbers = shipDAO.getMissingStatus(conn,-1);
        
        for( int idx=0; idx<cartonNumbers.size(); idx++) {
            System.out.println(cartonNumbers.get(idx));
        }
    }
    
    public void testResponse() throws Throwable {
//        String strDoc ="<ORDER_DATA_DOC "+
//            "xmlns=\"http://ScanData.com/FTD/WTM/DataSetSchemas/WTM_XMLSchema.xsd\">"+
//            "<ORDER_DATA_ROW>"+
//            "<CartonNumber>00872092OD001000</CartonNumber>"+
//            "<OrderKey>00872092OD001000</OrderKey>"+
//            "<DateCreated>2006-11-20T00:00:00</DateCreated>"+
//            "<DatePacked></DatePacked>"+
//            "<DateShipped></DateShipped>"+
//            "<PrintBatchID></PrintBatchID>"+
//            "<PrintBatchSequence></PrintBatchSequence>"+
//            "<TrackingNumber>1Z2374412345678</TrackingNumber>"+
//            "<Status>CANCELED_NOT_LABELED</Status>"+
//            "</ORDER_DATA_ROW>"+
//            "<ORDER_DATA_ROW>"+
//            "<CartonNumber> E123456</CartonNumber>"+
//            "<OrderKey> E123456</OrderKey>"+
//            "<DateCreated>2006-11-20T00:00:00</DateCreated>"+
//            "<DatePacked></DatePacked>"+
//            "<DateShipped></DateShipped>"+
//            "<PrintBatchID></PrintBatchID>"+
//            "<PrintBatchSequence></PrintBatchSequence>"+
//            "<TrackingNumber>1Z2374412345678</TrackingNumber>"+
//            "<Status>CANCELED_NOT_LABELED</Status>"+
//            "</ORDER_DATA_ROW>"+
//            "<ORDER_DATA_ROW>"+
//            "<CartonNumber> E123456</CartonNumber>"+
//            "<OrderKey> E123456</OrderKey>"+
//            "<DateCreated>2006-11-20T00:00:00</DateCreated>"+
//            "<DatePacked></DatePacked>"+
//            "<DateShipped>2006-11-20T00:00:00</DateShipped>"+
//            "<PrintBatchID>AAAA</PrintBatchID>"+
//            "<PrintBatchSequence>1</PrintBatchSequence>"+
//            "<TrackingNumber>1Z2374412345678</TrackingNumber>"+
//            "<Status>SHIPPED</Status>"+
//            "</ORDER_DATA_ROW>"+
//            "</ORDER_DATA_DOC>";

        String strDoc ="<ORDER_STATUS_DATA_DOC "+
            "xmlns=\"http://ScanData.com/FTD/WTM/DataSetSchemas/WTM_XMLSchema.xsd\">"+
            "  <ORDER_STATUS_DATA_ROW> "+
            "    <CartonNumber>00872092OD001000</CartonNumber> "+
            "    <OrderKey>00872092OD001000</OrderKey> "+
            "    <DateCreated>2006-11-14T17:19:32</DateCreated> "+
            "    <DatePacked>2007-03-14T15:28:31</DatePacked> "+
            "    <PrintBatchID>20070314-1</PrintBatchID> "+
            "    <PrintBatchSequence>1</PrintBatchSequence> "+ 
            "    <TrackingNumber>918906103229</TrackingNumber> "+
            "    <Status>LABELED</Status> "+
            "  </ORDER_STATUS_DATA_ROW> "+
            "</ORDER_STATUS_DATA_DOC>";
            
        System.out.println(strDoc);
        SDSMissingStatusBO bo = (SDSMissingStatusBO)context.getBean("sdsMissingStatusBO");  
        List<SDSMessageVO> list = bo.parseResponse(JAXPUtil.parseDocument(strDoc,false,false));
        System.out.println("Status update found = "+String.valueOf(list.size()));
        SDSMessageVO vo;
        StringBuilder sb;
            
        for( int idx=0; idx<list.size(); idx++) {
            vo = list.get(idx);
            sb = new StringBuilder();
            sb.append("Carton Number: ");
            sb.append(vo.getCartonNumber());
            sb.append("\r\nDirection: ");
            sb.append(vo.getDirection());
            sb.append("\r\nStatus Date: ");
            sb.append(vo.getTrackingNumber());
            sb.append("\r\nBatch Number: ");
            sb.append(vo.getPrintBatchNumber());
            sb.append("\r\nBatch Sequence: ");
            sb.append(vo.getPrintBatchSequence());
            sb.append("\r\nShip Date Date: ");
            sb.append(SDS_DATE_FORMAT.format(vo.getShipDate()));
            sb.append("\r\nStatus: ");
            sb.append(vo.getStatus());
            sb.append("\r\nStatus Date: ");
            sb.append(SDS_DATE_TIME_FORMAT.format(vo.getShipDate()));
            sb.append("\r\n");
            
            System.out.println(sb.toString());
        }
    }

    /**
     * Test the entire SDSMissingStatusBO processRequest logic
     * @throws Exception
     */
    public void testProcessRequest() throws Exception {
        List<String> cartonNumbers = new ArrayList<String>();
//        cartonNumbers.add("E0118868");
//        cartonNumbers.add("E0118869");
//        cartonNumbers.add("E0118870");
//        cartonNumbers.add("E0118871");
//        cartonNumbers.add("E0118872");
//        cartonNumbers.add("E0118873");
//        cartonNumbers.add("E0118874");
//        cartonNumbers.add("E0118875");
//        cartonNumbers.add("E0118877");
//        cartonNumbers.add("E0118878");
//        cartonNumbers.add("E0118879");
//        cartonNumbers.add("E0118880");
//        cartonNumbers.add("E0118881");
//        cartonNumbers.add("E0118882");
//        cartonNumbers.add("E0118884");
//        cartonNumbers.add("E0118885");
//        cartonNumbers.add("E0118886");
//        cartonNumbers.add("E0118888");
//        cartonNumbers.add("E0118889");
//        cartonNumbers.add("E0118890");
//        cartonNumbers.add("E0118891");
//        cartonNumbers.add("E0118892");
//        cartonNumbers.add("E0118893");
//        cartonNumbers.add("E0118894");
//        cartonNumbers.add("E0118895");
//        cartonNumbers.add("E0118897");
//        cartonNumbers.add("E0118919");
//        cartonNumbers.add("E0118921");
//        cartonNumbers.add("E0118923");
//        cartonNumbers.add("E0118924");
//        cartonNumbers.add("E0118925");
//        cartonNumbers.add("E0118920");
//        cartonNumbers.add("E0118922");
//        cartonNumbers.add("E0118914");
//        cartonNumbers.add("E0118917");
//        cartonNumbers.add("E0118918");
//        cartonNumbers.add("E0118915");
//        cartonNumbers.add("E0118916");
        cartonNumbers.add("E0118925");
        cartonNumbers.add("E0118927");
        cartonNumbers.add("E0118928");
        cartonNumbers.add("E0118929");
        cartonNumbers.add("E0118930");
        cartonNumbers.add("E0118931");
        cartonNumbers.add("E0118932");
        
        SDSMissingStatusBO bo = (SDSMissingStatusBO)context.getBean("sdsMissingStatusBO");
        bo.processRequest(conn,cartonNumbers,null,new Long(5),null);
        conn.commit();
    }

    public static void main(String[] args) {
        try {
//            MissingStatusTestCase testCase = new MissingStatusTestCase("testBuildRequest");
//            MissingStatusTestCase testCase = new MissingStatusTestCase("testMissingStatusDAO");
//            MissingStatusTestCase testCase = new MissingStatusTestCase("testResponse");
            MissingStatusTestCase testCase = new MissingStatusTestCase("testProcessRequest");
            testCase.runBare();
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }
}
