package com.ftd.ship.test;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.*;
import javax.servlet.http.*;

import org.apache.commons.lang.StringUtils;

public class ShipRequestTestServlet extends HttpServlet {
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    /**Process the HTTP doGet request.
     */
    public void doPost(HttpServletRequest request, 
                      HttpServletResponse response) throws ServletException, IOException {response.setContentType(CONTENT_TYPE);
        String testString = request.getParameter("testChoice");
//        PrintWriter out = response.getWriter();
//        out.println("<html>");
//        out.println("<head><title>ShipRequestTestServlet</title></head>");
//        out.println("<body>");
//        out.println("<p>Test to be performed: "+testString+"</p>");
//        out.println("</body></html>");
//        out.close();

        if( StringUtils.equals("testNextAvailableDeliveryDate",testString) ) {
            
        }
        
        try {
            ShipRequestTestCase shipRequestTestCase = new ShipRequestTestCase(testString);
            shipRequestTestCase.runBare();
        } catch (Throwable t) {
            PrintWriter out = response.getWriter();
            t.printStackTrace(out);
        }
    }
}
