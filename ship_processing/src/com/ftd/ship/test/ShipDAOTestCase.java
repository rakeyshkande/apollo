package com.ftd.ship.test;

import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.ship.vo.CarrierVO;
import com.ftd.ship.vo.ManifestVO;
import com.ftd.ship.vo.ProductNotificationVO;
import com.ftd.ship.vo.QueueVO;
import com.ftd.ship.vo.SDSErrorVO;
import com.ftd.ship.vo.SDSMessageVO;
import com.ftd.ship.vo.SDSTransactionVO;
import com.ftd.ship.vo.ShipMethodVO;
import com.ftd.ship.vo.ShipVendorProductVO;
import com.ftd.ship.vo.ShipmentOptionVO;
import com.ftd.ship.vo.VenusMessageVO;

import java.math.BigDecimal;

import java.sql.Connection;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import junit.framework.TestSuite;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import org.w3c.dom.Document;


public class ShipDAOTestCase extends SDSTestCase {
    Connection conn;
    ApplicationContext context;
    private static SimpleDateFormat MMDDYYYY_FORMAT = new SimpleDateFormat("MM/dd/yyyy");
    private static final SimpleDateFormat SDS_DATE_TIME_FORMAT_NO_MILLISECONDS = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    
    /**
     * Tests ShipDAO.loadVendorCarriers retrieval of the specific data
     */
    public void testLoadVendorCarriers() {
        try {
            List<CarrierVO> list = shipDAO.loadVendorCarriers(conn,"25065");
            
            for( int idx=0; idx<list.size(); idx++ ) {
                CarrierVO vo = list.get(idx);
                StringBuilder sb = new StringBuilder();
                sb.append("Carrier id: ");
                sb.append(vo.getCarrierId());
                sb.append("\r\nName: ");
                sb.append(vo.getCarrierName());
                
                System.out.println(sb.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Tests ShipDAO.loadQueueItem retrieval of the specified data
     */
    public void testLoadQueueItem() {
        try {
            QueueVO vo = shipDAO.loadQueueItem(conn,"25065");  
            System.out.println(vo.getExternalOrderNumber());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
//    public void testLoadVenusMessage() {
//        try {
//            VenusMessageVO vo = shipDAO.loadVenusMessage(conn,"25065");
//            System.out.println(vo.getVenusOrderNumber());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
    
    /**
     * Tests ShipDAO.getVenusMessage to retrieve a record from the venus.venus table
     */
    public void testGetVenusMessage() {
        try {
            VenusMessageVO vo = shipDAO.getVenusMessage(conn,"25065");
            System.out.println(vo.getVenusOrderNumber());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Tests ShipDAO.getVenusMessage to retrieve a record from the venus.venus table
     */
    public void testGetVenusMessage2() {
        try {
            List<VenusMessageVO> list = shipDAO.getVenusMessage(conn,"E0114011","FTD");
            VenusMessageVO vo = list.get(0);
            System.out.println(vo.getVenusOrderNumber());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Tests ShipDAO.decrementProductInventory to reduce the inventory count by 1
     */
    public void testDecrementProductInventory() {
        try {
            boolean result = shipDAO.decrementProductInventory(conn,"B500","00060",1);
            System.out.println("Has inventory level been hit? "+result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Tests ShipDAO.incrementProductInventory to increase the inventory count by 1
     */
    public void testIecrementProductInventory() {
        try {
            shipDAO.incrementProductInventory(conn,"B500","00060",1);
            System.out.println("Success");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Tests ShipDAO.getInventoryNotifications to retrieve email addresses for a 
     * specifice product/vendor combination
     */
    public void testGetInventoryNotifications() {
        try {
            List<ProductNotificationVO> list = shipDAO.getInventoryNotifications(conn,"B500","00060");
            ProductNotificationVO vo = list.get(0);
            System.out.println(vo.getEmailAddress());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Tests shipDAO.getInventoryRecordXML to return inventory data in an xml format
     */
    public void testInventoryRecordXML() {
        try {
            Document doc = shipDAO.getInventoryRecordXML(conn,"B500","00060");
            System.out.println(JAXPUtil.toString(doc));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Tests shipDAO.getShippingVendors to get a list of vendors that can ship a product based on the ship/delivery dates for a recipient postal code
     */
    public void testGetShippingVendors() {
        try {
            List<ShipVendorProductVO> list = shipDAO.getShippingVendors(conn,"F104",MMDDYYYY_FORMAT.parse("08/30/2006"),MMDDYYYY_FORMAT.parse("08/29/2006"),"54880");
            //List<ShipVendorProductVO> list = shipDAO.getShippingVendors(conn,"F104",MMDDYYYY_FORMAT.parse("08/30/2006"),MMDDYYYY_FORMAT.parse("08/29/2006"),"31067");
            for( int idx=0; idx<list.size(); idx++ ) {
               ShipVendorProductVO vo = list.get(idx);
               System.out.println("ID: "+vo.getVendorId());
               System.out.println("Code: "+vo.getVendorCode());
               System.out.println("Sku: "+vo.getVendorSku());
               System.out.println("Cost: "+vo.getVendorCost());
               System.out.println("Subcode Description: "+vo.getSubcodeDescription());
               System.out.println();
               System.out.println("Carriers for vendor:");
               for( int idx2=0; idx2<vo.getCarrierIds().size(); idx2++) {
                   System.out.println(vo.getCarrierIds().get(idx2));
               }
           }
           System.out.println();
           
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Tests ShipDAO.canVendorShipOnShipDate to determine if a vendor can ship a product on a specific date.
     */
    public void testCanVendorShipOnShipDate(){
        try {
            boolean result = shipDAO.canVendorShipOnShipDate(conn,"00050",MMDDYYYY_FORMAT.parse("08/16/2006"));
            System.out.println(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Tests ShipDAO.getCarrierList to get a list of active carriers.
     */
    public void testGetCarrierList() {
        try {
            List<CarrierVO> list = shipDAO.getCarrierList(conn);
            
            for( int idx=0; idx<list.size(); idx++ ) {
                CarrierVO vo = list.get(idx);
                StringBuilder sb = new StringBuilder();
                sb.append("Carrier id: ");
                sb.append(vo.getCarrierId());
                sb.append("\r\nName: ");
                sb.append(vo.getCarrierName());
                
                System.out.println(sb.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Tests ShipDAO.getVendorsToClose to get the vendors where they are past 
     * cutoff and the associated carriers
     */
    public void testGetVendorsPastCutoff() {
        try {
            List<ManifestVO> list = shipDAO.getVendorsToClose(conn,null,null,false,"0");
            
            for( int idx=0; idx<list.size(); idx++ ) {
                ManifestVO vo = list.get(idx);
                StringBuilder sb = new StringBuilder();
                sb.append("Vendor id: ");
                sb.append(vo.getVendorCode());
                sb.append("\r\nCarrier id: ");
                sb.append(vo.getCarrierId());
                
                System.out.println(sb.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Tests ShipDAO.getErrorProcessing to get the error processing information 
     * for the passed in error id and system
     */
    public void testGetErrorProcessing() {
        try {
            //SDSErrorVO vo = shipDAO.getErrorProcessing(conn,"-1234","SDS");
            SDSErrorVO vo = shipDAO.getErrorProcessing(conn,"-101001","SDS");
             
            System.out.println("Error ID: ");
            System.out.println(vo.getErrorId());
            System.out.println("\r\nSource: ");
            System.out.println(vo.getSource());
            System.out.println("\r\nDisposition: ");
            System.out.println(vo.getDisposition());
            System.out.println("\r\nComments: ");
            System.out.println(vo.getOrderComment());
            System.out.println("\r\nPage: ");
            System.out.println(vo.isPageSupport());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Tests shipDAO.getShipMethodFromCarrierShipMethod to get the ftd ship method that corresponds to the ftd ship method specified
     */
    public void testGetShipMethodFromCarrierShipMethod() {
        try {
            String shipMethod = shipDAO.getShipMethodFromCarrierShipMethod(conn,"FEDEX","PRIO OVNT");
            System.out.println(shipMethod);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    

    /**
     * Tests ShipDAO.getSDSTransaction ability to insert a record into the venus.sds_transaction table
     */
    public void testSDSTransaction() {
        try {
            SDSTransactionVO vo = new SDSTransactionVO();
            vo.setVenusId("10000");
            vo.setRequest("This is the request");
            vo.setResponse("This is the response");
            shipDAO.insertSDSTransaction(conn,vo);
            
            vo = shipDAO.getSDSTransaction(conn,vo.getVenusId());
            System.out.println("ID: "+String.valueOf(vo.getId()));
            System.out.println("VENUS ID: "+vo.getVenusId());
            System.out.println("REQUEST: "+vo.getRequest());
            System.out.println("RESPONSE: "+vo.getResponse());
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }
    
    /**
     * Tests ShipDAO.getFtdShipMethodData ability to convert the carrier ship method to an FTD ship method by calling venus.ship_pkg.GET_FTD_SHIP_DATA
     */
    public void testShipMethodConvert() {
        try {
            ShipMethodVO vo = shipDAO.getFtdShipMethodData(conn,"UPS","UPSSTDCAN");
            System.out.println("Carrier: "+vo.getCarrierId());
            System.out.println("Ship Method: "+vo.getShipMethod());
            System.out.println("Carrier Delivery: "+vo.getCarrierDelivery());
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }
    
    public void testGetSDSMessage() {
        try {
            SDSMessageVO vo = shipDAO.getSDSMessage(conn,684L);
            System.out.println("Carton Number: "+vo.getCartonNumber());
            System.out.println("Direction: "+vo.getDirection().toString());
            System.out.println("Message Id: "+vo.getMsgId().toString());
            System.out.println("Message Text: "+vo.getMsgText());
            System.out.println("Msg Type: "+vo.getMsgType().toString());
            System.out.println("Batch Number: "+vo.getPrintBatchNumber());
            System.out.println("Batch Sequence: "+vo.getPrintBatchSequence());
            System.out.println("Ship Date: "+SDS_DATE_TIME_FORMAT_NO_MILLISECONDS.format(vo.getShipDate()));
            System.out.println("Status: "+vo.getStatus());
            System.out.println("Status Code: "+vo.getStatusCode());
            System.out.println("Status Date: "+SDS_DATE_TIME_FORMAT_NO_MILLISECONDS.format(vo.getStatusDate()));
            System.out.println("Tracking number: "+vo.getTrackingNumber());
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }
    
    public void testInsertShipmentOption() {
        Date today = Calendar.getInstance().getTime();
        int rank = 1;
        
        try {
            ShipmentOptionVO vo = new ShipmentOptionVO();
            vo.setVenusOrderNumber("E102802");
            vo.setCarrierId("FEDEX");
            vo.setShipMethodId("GROUND COM");
            vo.setZoneCode(rank);
            vo.setRateWeight(new BigDecimal(6.5500));
            vo.setRateCharge(new BigDecimal(2.34000));
            //vo.setHandlingCharge(1.40000);
            vo.setTotalCharge(new BigDecimal(7.89000));
            vo.setFreightCharge(new BigDecimal(2.22));
            vo.setRateCalcType("Standard");
            vo.setDateShipped(today);
            vo.setExpectedDeliveryDate(today);
            vo.setDaysInTransit(rank);
            vo.setDistributionCenter(null);
            vo.setShipPointId("ZFDEDALLTX");
            vo.setErrorTxt("Cannot ship");
            vo.setZoneJumpFlag("Y");
            vo.setIdealRankNumber(rank);
            vo.setActualRankNumber(rank);

            shipDAO.insertShipmentOption(this.conn, vo);
            
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }    
    
    public void setUp() throws Exception
    {
        //Start up spring framework
        context = new FileSystemXmlApplicationContext("C:\\FTD_NEW\\FTD\\ship_processing\\web\\WEB-INF\\spring-ship-servlet.xml");
        this.conn = resourceProvider.getDatabaseConnection();
    }
    
    public void tearDown() throws Exception
    {
        if( conn!=null && !conn.isClosed() ) {
            conn.close();    
        }
    }
    
    public static TestSuite suite()
    { 
        TestSuite suite = null;
        
        try 
        {
            suite = new TestSuite(ShipDAOTestCase.class);
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        
        return suite;
    }
    
    public ShipDAOTestCase(String sTestName) {
        super(sTestName);
    }

    public static void main(String[] args) {
        //ShipDAOTestCase shipDAOTestCase = new ShipDAOTestCase("testGetPartnerForOrigin");
         ShipDAOTestCase shipDAOTestCase = new ShipDAOTestCase("testInsertShipmentOption");
        try {
            shipDAOTestCase.runBare();
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }
}
