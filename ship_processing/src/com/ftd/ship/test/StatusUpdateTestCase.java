package com.ftd.ship.test;

import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.ship.bo.SDSMsgRetrievalProcessingBO;
import com.ftd.ship.bo.communications.sds.SDSCommunications;
import com.ftd.ship.common.SDSConstants;
import com.ftd.ship.common.resources.SDSStatusParm;
import com.ftd.ship.vo.SDSMessageVO;

import java.sql.Connection;

import java.text.SimpleDateFormat;

import java.util.List;

import junit.framework.TestSuite;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;


public class StatusUpdateTestCase extends SDSTestCase {
    Connection conn;
    ApplicationContext context;
    private static final SimpleDateFormat SDS_DATE_TIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
    private static final SimpleDateFormat SDS_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    
    //<PLD_DOC xmlns="http://ScanData.com/FTD/WTM/DataSetSchemas/WTM_XMLSchema.xsd">
    //    <PLD_ROW>
    //       <CartonNumber>E0114265</CartonNumber>
    //       <TrackingNumber>1ZJ123450100011242    </TrackingNumber>
    //       <PrintBatchID>20060911-1</PrintBatchID>
    //       <PrintBatchSequence>1</PrintBatchSequence>
    //       <DatePlannedShipment>2006-09-07</DatePlannedShipment>
    //       <Status>LABELED</Status>
    //       <DateStatus>2006-09-11T17:39:10</DateStatus>
    //    </PLD_ROW>
    //    <PLD_ROW>
    //       <CartonNumber>E0114266</CartonNumber>
    //       <TrackingNumber>1ZJ123450100011251    </TrackingNumber>
    //       <PrintBatchID>20060911-1</PrintBatchID>
    //       <PrintBatchSequence>2</PrintBatchSequence>
    //       <DatePlannedShipment>2006-09-06</DatePlannedShipment>
    //       <Status>LABELED</Status>
    //       <DateStatus>2006-09-11T17:39:10</DateStatus>
    //    </PLD_ROW>
    //    <PLD_ROW>
    //       <CartonNumber>E0114267</CartonNumber>
    //       <TrackingNumber>1ZJ123450100011260    </TrackingNumber>
    //       <PrintBatchID>20060912-1</PrintBatchID>
    //       <PrintBatchSequence>1</PrintBatchSequence>
    //       <DatePlannedShipment>2006-09-11</DatePlannedShipment>
    //       <Status>LABELED</Status>
    //       <DateStatus>2006-09-12T15:28:58</DateStatus>
    //    </PLD_ROW>
    //    <PLD_ROW>
    //       <CartonNumber>E0114278</CartonNumber>
    //       <TrackingNumber>1ZF123450100009099    </TrackingNumber>
    //       <PrintBatchID>20060912-1</PrintBatchID>
    //       <PrintBatchSequence>1</PrintBatchSequence>
    //       <DatePlannedShipment>2006-09-11</DatePlannedShipment>
    //       <Status>LABELED</Status>
    //       <DateStatus>2006-09-12T15:34:45</DateStatus>
    //    </PLD_ROW>
    //    <PLD_ROW>
    //       <CartonNumber>E0114279</CartonNumber>
    //       <TrackingNumber>1ZF123450100009106    </TrackingNumber>
    //       <PrintBatchID>20060912-1</PrintBatchID>
    //       <PrintBatchSequence>2</PrintBatchSequence>
    //       <DatePlannedShipment>2006-09-11</DatePlannedShipment>
    //       <Status>LABELED</Status>
    //       <DateStatus>2006-09-12T15:34:45</DateStatus>
    //    </PLD_ROW>
    //    <PLD_ROW>
    //       <CartonNumber>E0114279</CartonNumber>
    //       <TrackingNumber>1ZF123450100009106    </TrackingNumber>
    //       <PrintBatchID>20060908-1</PrintBatchID>
    //       <PrintBatchSequence>1</PrintBatchSequence>
    //       <DatePlannedShipment>2006-09-11</DatePlannedShipment>
    //       <Status>LABELED</Status>
    //       <DateStatus>2006-09-08T15:19:35</DateStatus>
    //    </PLD_ROW>
    //    <PLD_ROW>
    //       <CartonNumber>E0114278</CartonNumber>
    //       <TrackingNumber>1ZF123450100009099    </TrackingNumber>
    //       <PrintBatchID>20060908-2</PrintBatchID>
    //       <PrintBatchSequence>1</PrintBatchSequence>
    //       <DatePlannedShipment>2006-09-11</DatePlannedShipment>
    //       <Status>LABELED</Status>
    //       <DateStatus>2006-09-08T15:38:52</DateStatus>
    //    </PLD_ROW>
    //    <PLD_ROW>
    //       <CartonNumber>E0114277</CartonNumber>
    //       <TrackingNumber>1ZF123450100009080    </TrackingNumber>
    //       <PrintBatchID>20060908-3</PrintBatchID>
    //       <PrintBatchSequence>1</PrintBatchSequence>
    //       <DatePlannedShipment>2006-09-11</DatePlannedShipment>
    //       <Status>LABELED</Status>
    //       <DateStatus>2006-09-08T15:42:18</DateStatus>
    //    </PLD_ROW>
    // </PLD_DOC>


    /**
     * Tests the parsing of a manually created "CANCELED" status request response
     */
    public void testParseResponse() {
        StringBuilder sb = new StringBuilder();
        sb.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n");
        sb.append("<PLD_DOC xmlns=\"http://ScanData.com/FTD/WTM/DataSetSchemas/WTM_XMLSchema.xsd\">");
        sb.append("<PLD_ROW>");
        sb.append("<CartonNumber>FTD0000000001</CartonNumber>");
        sb.append("<TrackingNumber>1ZV69W420200066740</TrackingNumber>");
        sb.append("<PrintBatchID>20060818-7</PrintBatchID>");
        sb.append("<PrintBatchSequence>1</PrintBatchSequence>");
        sb.append("<DatePlannedShipment>2006-08-18</DatePlannedShipment>");
        sb.append("<Status>CANCELED</Status>");
        sb.append("<DateStatus>2006-08-18T15:20:30</DateStatus>");
        sb.append("</PLD_ROW>");
        sb.append("<PLD_ROW>");
        sb.append("<CartonNumber>FTD0000000002</CartonNumber>");
        sb.append("<TrackingNumber>1ZV69W420200066759</TrackingNumber>");
        sb.append("<PrintBatchID>20060818-7</PrintBatchID>");
        sb.append("<PrintBatchSequence>2</PrintBatchSequence>");
        sb.append("<DatePlannedShipment>2006-08-18</DatePlannedShipment>");
        sb.append("<Status>CANCELED</Status>");
        sb.append("<DateStatus>2006-08-18T15:20:30</DateStatus>");
        sb.append("</PLD_ROW>");
        sb.append("<PLD_ROW>");
        sb.append("<CartonNumber>FTD0000000003</CartonNumber>");
        sb.append("<TrackingNumber>1ZV69W420200066768</TrackingNumber>");
        sb.append("<PrintBatchID>20060823-1</PrintBatchID>");
        sb.append("<PrintBatchSequence>1</PrintBatchSequence>");
        sb.append("<DatePlannedShipment>2006-08-18</DatePlannedShipment>");
        sb.append("<Status>SHIPPED</Status>");
        sb.append("<DateStatus>2006-08-25T11:10:54</DateStatus>");
        sb.append("</PLD_ROW>");
        sb.append("<PLD_ROW>");
        sb.append("<CartonNumber>FTD0000000004</CartonNumber>");
        sb.append("<TrackingNumber>1ZV69W420200066777</TrackingNumber>");
        sb.append("<PrintBatchID>20060823-1</PrintBatchID>");
        sb.append("<PrintBatchSequence>2</PrintBatchSequence>");
        sb.append("<DatePlannedShipment>2006-08-18</DatePlannedShipment>");
        sb.append("<Status>SHIPPED</Status>");
        sb.append("<DateStatus>2006-08-25T11:10:54</DateStatus>");
        sb.append("</PLD_ROW>");
        sb.append("<PLD_ROW>");
        sb.append("<CartonNumber>FTD0000000005</CartonNumber>");
        sb.append("<TrackingNumber>1ZV69W420200066786</TrackingNumber>");
        sb.append("<PrintBatchID>20060823-1</PrintBatchID>");
        sb.append("<PrintBatchSequence>3</PrintBatchSequence>");
        sb.append("<DatePlannedShipment>2006-08-18</DatePlannedShipment>");
        sb.append("<Status>SHIPPED</Status>");
        sb.append("<DateStatus>2006-08-25T11:10:54</DateStatus>");
        sb.append("</PLD_ROW>");
        sb.append("<PLD_ROW>");
        sb.append("<CartonNumber>FTD0000000006</CartonNumber>");
        sb.append("<TrackingNumber>1ZV69W420200066795</TrackingNumber>");
        sb.append("<DatePlannedShipment>2006-08-18</DatePlannedShipment>");
        sb.append("<Status>SHIPPED</Status>");
        sb.append("<DateStatus>2006-08-30T14:59:22</DateStatus>");
        sb.append("</PLD_ROW>");
        sb.append("<PLD_ROW>");
        sb.append("<CartonNumber>FTD0000000007</CartonNumber>");
        sb.append("<TrackingNumber>1ZV69W420200066802</TrackingNumber>");
        sb.append("<PrintBatchID>20060831-1</PrintBatchID>");
        sb.append("<PrintBatchSequence>1</PrintBatchSequence>");
        sb.append("<DatePlannedShipment>2006-08-18</DatePlannedShipment>");
        sb.append("<Status>LABELED</Status>");
        sb.append("<DateStatus>2006-08-31T14:02:01</DateStatus>");
        sb.append("</PLD_ROW>");
        sb.append("<PLD_ROW>");
        sb.append("<CartonNumber>FTD0000000008</CartonNumber>");
        sb.append("<TrackingNumber>1ZV69W420200066811</TrackingNumber>");
        sb.append("<PrintBatchID>20060831-1</PrintBatchID>");
        sb.append("<PrintBatchSequence>2</PrintBatchSequence>");
        sb.append("<DatePlannedShipment>2006-08-18</DatePlannedShipment>");
        sb.append("<Status>LABELED</Status>");
        sb.append("<DateStatus>2006-08-31T14:02:01</DateStatus>");
        sb.append("</PLD_ROW>");
        sb.append("<PLD_ROW>");
        sb.append("<CartonNumber>FTD0000000011</CartonNumber>");
        sb.append("<TrackingNumber>1ZV69W421200066847</TrackingNumber>");
        sb.append("<DatePlannedShipment>2006-08-18</DatePlannedShipment>");
        sb.append("<Status>CANCELED</Status>");
        sb.append("<DateStatus>2006-08-28T13:47:10</DateStatus>");
        sb.append("</PLD_ROW>");
        sb.append("<PLD_ROW>");
        sb.append("<CartonNumber>FTD0000000012</CartonNumber>");
        sb.append("<TrackingNumber>1ZV69W421200066856</TrackingNumber>");
        sb.append("<DatePlannedShipment>2006-08-18</DatePlannedShipment>");
        sb.append("<Status>CANCELED</Status>");
        sb.append("<DateStatus>2006-08-28T13:47:10</DateStatus>");
        sb.append("</PLD_ROW>");
        sb.append("<PLD_ROW>");
        sb.append("<CartonNumber>FTD0000000013</CartonNumber>");
        sb.append("<TrackingNumber>1ZV69W421200066865</TrackingNumber>");
        sb.append("<DatePlannedShipment>2006-08-18</DatePlannedShipment>");
        sb.append("<Status>CANCELED</Status>");
        sb.append("<DateStatus>2006-08-28T13:47:10</DateStatus>");
        sb.append("</PLD_ROW>");
        sb.append("<PLD_ROW>");
        sb.append("<CartonNumber>FTD0000000014</CartonNumber>");
        sb.append("<TrackingNumber>1ZV69W421200066874</TrackingNumber>");
        sb.append("<DatePlannedShipment>2006-08-18</DatePlannedShipment>");
        sb.append("<Status>CANCELED</Status>");
        sb.append("<DateStatus>2006-08-28T13:47:10</DateStatus>");
        sb.append("</PLD_ROW>");
        sb.append("<PLD_ROW>");
        sb.append("<CartonNumber>FTD0000000016</CartonNumber>");
        sb.append("<TrackingNumber>1ZV69W420300066891</TrackingNumber>");
        sb.append("<DatePlannedShipment>2006-08-18</DatePlannedShipment>");
        sb.append("<Status>CANCELED</Status>");
        sb.append("<DateStatus>2006-08-28T13:46:02</DateStatus>");
        sb.append("</PLD_ROW>");
        sb.append("</PLD_DOC>");
        
        try {
            Document doc = JAXPUtil.parseDocument(sb.toString(),false,true);
            SDSMsgRetrievalProcessingBO bo = (SDSMsgRetrievalProcessingBO)context.getBean("sdsMsgRetrievalProcessingBO");
            List<SDSMessageVO> list = bo.parseResponse(doc);
            SDSMessageVO vo;
            
            for( int idx=0; idx<list.size(); idx++) {
                vo = list.get(idx);
                sb = new StringBuilder();
                sb.append("Carton Number: ");
                sb.append(vo.getCartonNumber());
                sb.append("\r\nDirection: ");
                sb.append(vo.getDirection());
                sb.append("\r\nStatus Date: ");
                sb.append(vo.getTrackingNumber());
                sb.append("\r\nBatch Number: ");
                sb.append(vo.getPrintBatchNumber());
                sb.append("\r\nBatch Sequence: ");
                sb.append(vo.getPrintBatchSequence());
                sb.append("\r\nShip Date Date: ");
                sb.append(SDS_DATE_FORMAT.format(vo.getShipDate()));
                sb.append("\r\nStatus: ");
                sb.append(vo.getStatus());
                sb.append("\r\nStatus Date: ");
                sb.append(SDS_DATE_TIME_FORMAT.format(vo.getShipDate()));
                sb.append("\r\n");
                
                System.out.println(sb.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Tests the parsing of a manually created "LABELED" status request response
     */
    public void testParseResponse2() {
        StringBuilder sb = new StringBuilder();
        sb.append("<GetPLDUpdateResponse xmlns=\"http://ScanData.com/\">");
        sb.append("    <GetPLDUpdateResult>");
        sb.append("    <PLD_DOC xmlns=\"http://ScanData.com/FTD/WTM/DataSetSchemas/WTM_XMLSchema.xsd\">");
        sb.append("        <PLD_ROW>");
        sb.append("           <CartonNumber>E0114265</CartonNumber>");
        sb.append("           <TrackingNumber>1ZJ123450100011242    </TrackingNumber>");
        sb.append("           <PrintBatchID>20060911-1</PrintBatchID>");
        sb.append("           <PrintBatchSequence>1</PrintBatchSequence>");
        sb.append("           <DatePlannedShipment>2006-09-07</DatePlannedShipment>");
        sb.append("           <Status>LABELED</Status>");
        sb.append("           <DateStatus>2006-09-11T17:39:10</DateStatus>");
        sb.append("        </PLD_ROW>");
        sb.append("        <PLD_ROW>");
        sb.append("           <CartonNumber>E0114266</CartonNumber>");
        sb.append("           <TrackingNumber>1ZJ123450100011251    </TrackingNumber>");
        sb.append("           <PrintBatchID>20060911-1</PrintBatchID>");
        sb.append("           <PrintBatchSequence>2</PrintBatchSequence>");
        sb.append("           <DatePlannedShipment>2006-09-06</DatePlannedShipment>");
        sb.append("           <Status>LABELED</Status>");
        sb.append("           <DateStatus>2006-09-11T17:39:10</DateStatus>");
        sb.append("        </PLD_ROW>");
        sb.append("        <PLD_ROW>");
        sb.append("           <CartonNumber>E0114267</CartonNumber>");
        sb.append("           <TrackingNumber>1ZJ123450100011260    </TrackingNumber>");
        sb.append("           <PrintBatchID>20060912-1</PrintBatchID>");
        sb.append("           <PrintBatchSequence>1</PrintBatchSequence>");
        sb.append("           <DatePlannedShipment>2006-09-11</DatePlannedShipment>");
        sb.append("           <Status>LABELED</Status>");
        sb.append("           <DateStatus>2006-09-12T15:28:58</DateStatus>");
        sb.append("        </PLD_ROW>");
        sb.append("        <PLD_ROW>");
        sb.append("           <CartonNumber>E0114278</CartonNumber>");
        sb.append("           <TrackingNumber>1ZF123450100009099    </TrackingNumber>");
        sb.append("           <PrintBatchID>20060912-1</PrintBatchID>");
        sb.append("           <PrintBatchSequence>1</PrintBatchSequence>");
        sb.append("           <DatePlannedShipment>2006-09-11</DatePlannedShipment>");
        sb.append("           <Status>LABELED</Status>");
        sb.append("           <DateStatus>2006-09-12T15:34:45</DateStatus>");
        sb.append("        </PLD_ROW>");
        sb.append("        <PLD_ROW>");
        sb.append("           <CartonNumber>E0114279</CartonNumber>");
        sb.append("           <TrackingNumber>1ZF123450100009106    </TrackingNumber>");
        sb.append("           <PrintBatchID>20060912-1</PrintBatchID>");
        sb.append("           <PrintBatchSequence>2</PrintBatchSequence>");
        sb.append("           <DatePlannedShipment>2006-09-11</DatePlannedShipment>");
        sb.append("           <Status>LABELED</Status>");
        sb.append("           <DateStatus>2006-09-12T15:34:45</DateStatus>");
        sb.append("        </PLD_ROW>");
        sb.append("        <PLD_ROW>");
        sb.append("           <CartonNumber>E0114279</CartonNumber>");
        sb.append("           <TrackingNumber>1ZF123450100009106    </TrackingNumber>");
        sb.append("           <PrintBatchID>20060908-1</PrintBatchID>");
        sb.append("           <PrintBatchSequence>1</PrintBatchSequence>");
        sb.append("           <DatePlannedShipment>2006-09-11</DatePlannedShipment>");
        sb.append("           <Status>LABELED</Status>");
        sb.append("           <DateStatus>2006-09-08T15:19:35</DateStatus>");
        sb.append("        </PLD_ROW>");
        sb.append("        <PLD_ROW>");
        sb.append("           <CartonNumber>E0114278</CartonNumber>");
        sb.append("           <TrackingNumber>1ZF123450100009099    </TrackingNumber>");
        sb.append("           <PrintBatchID>20060908-2</PrintBatchID>");
        sb.append("           <PrintBatchSequence>1</PrintBatchSequence>");
        sb.append("           <DatePlannedShipment>2006-09-11</DatePlannedShipment>");
        sb.append("           <Status>LABELED</Status>");
        sb.append("           <DateStatus>2006-09-08T15:38:52</DateStatus>");
        sb.append("        </PLD_ROW>");
        sb.append("        <PLD_ROW>");
        sb.append("           <CartonNumber>E0114277</CartonNumber>");
        sb.append("           <TrackingNumber>1ZF123450100009080    </TrackingNumber>");
        sb.append("           <PrintBatchID>20060908-3</PrintBatchID>");
        sb.append("           <PrintBatchSequence>1</PrintBatchSequence>");
        sb.append("           <DatePlannedShipment>2006-09-11</DatePlannedShipment>");
        sb.append("           <Status>LABELED</Status>");
        sb.append("           <DateStatus>2006-09-08T15:42:18</DateStatus>");
        sb.append("        </PLD_ROW>");
        sb.append("     </PLD_DOC>");
        sb.append("</GetPLDUpdateResult>"); 
        sb.append("</GetPLDUpdateResponse>");
        
        try {
            Document doc = JAXPUtil.parseDocument(sb.toString(),false,true);
            System.out.println(JAXPUtil.toString(doc));
            NodeList list = JAXPUtil.selectNodes(doc,"//sd:PLD_ROW","sd","http://ScanData.com/FTD/WTM/DataSetSchemas/WTM_XMLSchema.xsd");
 
            System.out.println("Records selected= "+String.valueOf(list.getLength()));
            for( int idx=0; idx<list.getLength(); idx++ ) 
            {
                Element row = (Element)list.item(idx);
                String cartonNumber = JAXPUtil.getFirstChildNodeTextByTagName(row,"CartonNumber");
                System.out.println("CartonNumber: "+cartonNumber);   
                String TrackingNumber = JAXPUtil.getFirstChildNodeTextByTagName(row,"TrackingNumber");
                System.out.println("TrackingNumber: "+TrackingNumber);   
                String PrintBatchID = JAXPUtil.getFirstChildNodeTextByTagName(row,"PrintBatchID");
                System.out.println("PrintBatchID: "+PrintBatchID);   
                String PrintBatchSequence = JAXPUtil.getFirstChildNodeTextByTagName(row,"PrintBatchSequence");
                System.out.println("PrintBatchSequence: "+PrintBatchSequence);   
                String DatePlannedShipment = JAXPUtil.getFirstChildNodeTextByTagName(row,"DatePlannedShipment");
                System.out.println("DatePlannedShipment: "+DatePlannedShipment);   
                String Status = JAXPUtil.getFirstChildNodeTextByTagName(row,"Status");
                System.out.println("Status: "+Status);   
                String DateStatus = JAXPUtil.getFirstChildNodeTextByTagName(row,"DateStatus");
                System.out.println("DateStatus: "+DateStatus);   
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Tests the parsing of a manually created "LABELED" status request response
     */
    public void testParseResponse3() {
        StringBuffer sb = new StringBuffer();
        sb.append("<GetPLDUpdateResponse xmlns=\"http://ScanData.com/\">");
        sb.append("    <GetPLDUpdateResult>");
        sb.append("    <PLD_DOC xmlns=\"http://ScanData.com/FTD/WTM/DataSetSchemas/WTM_XMLSchema.xsd\">");
        sb.append("        <PLD_ROW>");
        sb.append("           <CartonNumber>E0114265</CartonNumber>");
        sb.append("           <TrackingNumber>1ZJ123450100011242    </TrackingNumber>");
        sb.append("           <PrintBatchID>20060911-1</PrintBatchID>");
        sb.append("           <PrintBatchSequence>1</PrintBatchSequence>");
        sb.append("           <DatePlannedShipment>2006-09-07</DatePlannedShipment>");
        sb.append("           <Status>LABELED</Status>");
        sb.append("           <DateStatus>2006-09-11T17:39:10</DateStatus>");
        sb.append("        </PLD_ROW>");
        sb.append("        <PLD_ROW>");
        sb.append("           <CartonNumber>E0114266</CartonNumber>");
        sb.append("           <TrackingNumber>1ZJ123450100011251    </TrackingNumber>");
        sb.append("           <PrintBatchID>20060911-1</PrintBatchID>");
        sb.append("           <PrintBatchSequence>2</PrintBatchSequence>");
        sb.append("           <DatePlannedShipment>2006-09-06</DatePlannedShipment>");
        sb.append("           <Status>LABELED</Status>");
        sb.append("           <DateStatus>2006-09-11T17:39:10</DateStatus>");
        sb.append("        </PLD_ROW>");
        sb.append("        <PLD_ROW>");
        sb.append("           <CartonNumber>E0114267</CartonNumber>");
        sb.append("           <TrackingNumber>1ZJ123450100011260    </TrackingNumber>");
        sb.append("           <PrintBatchID>20060912-1</PrintBatchID>");
        sb.append("           <PrintBatchSequence>1</PrintBatchSequence>");
        sb.append("           <DatePlannedShipment>2006-09-11</DatePlannedShipment>");
        sb.append("           <Status>LABELED</Status>");
        sb.append("           <DateStatus>2006-09-12T15:28:58</DateStatus>");
        sb.append("        </PLD_ROW>");
        sb.append("        <PLD_ROW>");
        sb.append("           <CartonNumber>E0114278</CartonNumber>");
        sb.append("           <TrackingNumber>1ZF123450100009099    </TrackingNumber>");
        sb.append("           <PrintBatchID>20060912-1</PrintBatchID>");
        sb.append("           <PrintBatchSequence>1</PrintBatchSequence>");
        sb.append("           <DatePlannedShipment>2006-09-11</DatePlannedShipment>");
        sb.append("           <Status>LABELED</Status>");
        sb.append("           <DateStatus>2006-09-12T15:34:45</DateStatus>");
        sb.append("        </PLD_ROW>");
        sb.append("        <PLD_ROW>");
        sb.append("           <CartonNumber>E0114279</CartonNumber>");
        sb.append("           <TrackingNumber>1ZF123450100009106    </TrackingNumber>");
        sb.append("           <PrintBatchID>20060912-1</PrintBatchID>");
        sb.append("           <PrintBatchSequence>2</PrintBatchSequence>");
        sb.append("           <DatePlannedShipment>2006-09-11</DatePlannedShipment>");
        sb.append("           <Status>LABELED</Status>");
        sb.append("           <DateStatus>2006-09-12T15:34:45</DateStatus>");
        sb.append("        </PLD_ROW>");
        sb.append("        <PLD_ROW>");
        sb.append("           <CartonNumber>E0114279</CartonNumber>");
        sb.append("           <TrackingNumber>1ZF123450100009106    </TrackingNumber>");
        sb.append("           <PrintBatchID>20060908-1</PrintBatchID>");
        sb.append("           <PrintBatchSequence>1</PrintBatchSequence>");
        sb.append("           <DatePlannedShipment>2006-09-11</DatePlannedShipment>");
        sb.append("           <Status>LABELED</Status>");
        sb.append("           <DateStatus>2006-09-08T15:19:35</DateStatus>");
        sb.append("        </PLD_ROW>");
        sb.append("        <PLD_ROW>");
        sb.append("           <CartonNumber>E0114278</CartonNumber>");
        sb.append("           <TrackingNumber>1ZF123450100009099    </TrackingNumber>");
        sb.append("           <PrintBatchID>20060908-2</PrintBatchID>");
        sb.append("           <PrintBatchSequence>1</PrintBatchSequence>");
        sb.append("           <DatePlannedShipment>2006-09-11</DatePlannedShipment>");
        sb.append("           <Status>LABELED</Status>");
        sb.append("           <DateStatus>2006-09-08T15:38:52</DateStatus>");
        sb.append("        </PLD_ROW>");
        sb.append("        <PLD_ROW>");
        sb.append("           <CartonNumber>E0114277</CartonNumber>");
        sb.append("           <TrackingNumber>1ZF123450100009080    </TrackingNumber>");
        sb.append("           <PrintBatchID>20060908-3</PrintBatchID>");
        sb.append("           <PrintBatchSequence>1</PrintBatchSequence>");
        sb.append("           <DatePlannedShipment>2006-09-11</DatePlannedShipment>");
        sb.append("           <Status>LABELED</Status>");
        sb.append("           <DateStatus>2006-09-08T15:42:18</DateStatus>");
        sb.append("        </PLD_ROW>");
        sb.append("     </PLD_DOC>");
        sb.append("</GetPLDUpdateResult>"); 
        sb.append("</GetPLDUpdateResponse>");
        
        try {
            Document doc = JAXPUtil.parseDocument(sb.toString(),false,true);
            SDSMsgRetrievalProcessingBO bo = (SDSMsgRetrievalProcessingBO)context.getBean("sdsMsgRetrievalProcessingBO");
            List<SDSMessageVO> list = bo.parseResponse(doc);
            SDSMessageVO vo;
            
            for( int idx=0; idx<list.size(); idx++) {
                vo = list.get(idx);
                sb = new StringBuffer();
                sb.append("Carton Number: ");
                sb.append(vo.getCartonNumber());
                sb.append("\r\nDirection: ");
                sb.append(vo.getDirection());
                sb.append("\r\nStatus Date: ");
                sb.append(vo.getTrackingNumber());
                sb.append("\r\nBatch Number: ");
                sb.append(vo.getPrintBatchNumber());
                sb.append("\r\nBatch Sequence: ");
                sb.append(vo.getPrintBatchSequence());
                sb.append("\r\nShip Date Date: ");
                sb.append(SDS_DATE_FORMAT.format(vo.getShipDate()));
                sb.append("\r\nStatus: ");
                sb.append(vo.getStatus());
                sb.append("\r\nStatus Date: ");
                sb.append(SDS_DATE_TIME_FORMAT.format(vo.getShipDate()));
                sb.append("\r\n");
                
                System.out.println(sb.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
  
    public StatusUpdateTestCase(String sTestName) {
        super(sTestName);
    }

    public void setUp() throws Exception
    {
        //Start up spring framework
        context = new FileSystemXmlApplicationContext("C:\\jdev_1013\\jdev\\mywork\\ship_processing\\FTD\\ship_processing\\web\\WEB-INF\\spring-ship-servlet.xml");
        this.conn = resourceProvider.getDatabaseConnection();
    }
    
    public void tearDown() throws Exception
    {
        if( conn!=null && !conn.isClosed() ) {
            conn.close();    
        }
    }
    
    public static TestSuite suite()
    { 
        TestSuite suite = null;
        
        try 
        {
            suite = new TestSuite(StatusUpdateTestCase.class);
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        
        return suite;
    }
    
//    public void testUpdateBuild() {
//        SDSMsgRetrievalProcessingBO bo = (SDSMsgRetrievalProcessingBO)context.getBean("sdsMsgRetrievalProcessingBO");
//        try {
//            Document doc = bo.buildRequest(SDSStatusParm.SHIPPED);
//            System.out.println(JAXPUtil.toString(doc));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
    
    /**
     * Tests the build, transmitting, and parsing of a "SHIPPED" status update 
     * by manually calling SDSCommunications.getStatusUpdates
     */
    public void testGetShippedUpdate() {
        try {
            SDSCommunications comm = (SDSCommunications)context.getBean("sdsCommunications");
            Document doc = comm.getStatusUpdates(conn, SDSConstants.LOB_FTDCOM,SDSStatusParm.SHIPPED,"2006-09-01","2006-09-24");
            System.out.println(JAXPUtil.toString(doc));
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
     /**
      * Tests the build, transmitting, and parsing of a "SHIPPED" status update 
      * by manually calling SDSMsgRetrievalProcessingBO.processRequest
      */
     public void testGetShippedUpdate2() {
        try {
            SDSMsgRetrievalProcessingBO bo = (SDSMsgRetrievalProcessingBO)context.getBean("sdsMsgRetrievalProcessingBO");
            bo.processRequest(conn,SDSStatusParm.SHIPPED,SDS_DATE_FORMAT.parse("2006-09-01"),SDS_DATE_FORMAT.parse("2006-09-24"));
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Tests the build, transmitting, and parsing of a "CANCELED" status update 
     * by manually calling SDSCommunications.getStatusUpdates
     */
    public void testGetCancelUpdate() {
        try {
            SDSCommunications comm = (SDSCommunications)context.getBean("sdsCommunications");
            Document doc = comm.getStatusUpdates(conn, SDSConstants.LOB_FTDCOM,SDSStatusParm.CANCELED,"2006-09-01","2006-09-24");
            System.out.println(JAXPUtil.toString(doc));
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
     /**
      * Tests the build, transmitting, and parsing of a "CANCELED" status update 
      * by manually calling SDSMsgRetrievalProcessingBO.processRequest
      */
     public void testGetCancelUpdate2() {
        try {
            SDSMsgRetrievalProcessingBO bo = (SDSMsgRetrievalProcessingBO)context.getBean("sdsMsgRetrievalProcessingBO");
            bo.processRequest(conn,SDSStatusParm.CANCELED,SDS_DATE_FORMAT.parse("2006-09-01"),SDS_DATE_FORMAT.parse("2006-09-24"));
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Tests the build, transmitting, and parsing of a "LABELED" status update 
     * by manually calling SDSCommunications.getStatusUpdates
     */
    public void testGetLabeledUpdate() {
        try {
            SDSCommunications comm = (SDSCommunications)context.getBean("sdsCommunications");
            Document doc = comm.getStatusUpdates(conn, SDSConstants.LOB_FTDCOM,SDSStatusParm.LABELED,"2006-09-01","2006-09-24");
            System.out.println(JAXPUtil.toString(doc));
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Tests the build, transmitting, and parsing of a "LABELED" status update 
     * by manually calling SDSMsgRetrievalProcessingBO.processRequest
     */
    public void testGetLabeledUpdate2() {
        try {
            SDSMsgRetrievalProcessingBO bo = (SDSMsgRetrievalProcessingBO)context.getBean("sdsMsgRetrievalProcessingBO");
            bo.processRequest(conn,SDSStatusParm.LABELED,SDS_DATE_FORMAT.parse("2006-09-01"),SDS_DATE_FORMAT.parse("2006-10-18"));
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Tests the build, transmitting, and parsing of a "REJECTED" status update 
     * by manually calling SDSCommunications.getStatusUpdates
     */
    public void testGetRejectedUpdate() {
        try {            
            SDSCommunications comm = (SDSCommunications)context.getBean("sdsCommunications");
            Document doc = comm.getStatusUpdates(conn, SDSConstants.LOB_FTDCOM,SDSStatusParm.REJECTED,"2006-09-01","2006-09-24");
            System.out.println(JAXPUtil.toString(doc));
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Tests the build, transmitting, and parsing of a "REJECTED" status update 
     * by manually calling SDSMsgRetrievalProcessingBO.processRequest
     */
    public void testGetRejectedUpdate2() {
        try {
            SDSMsgRetrievalProcessingBO bo = (SDSMsgRetrievalProcessingBO)context.getBean("sdsMsgRetrievalProcessingBO");
            bo.processRequest(conn,SDSStatusParm.REJECTED,SDS_DATE_FORMAT.parse("2006-09-01"),SDS_DATE_FORMAT.parse("2006-09-24"));
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Test the conversion of a SDS date/time string to a date object and back again
     */
    public void testTimeConversion() {
        try {
            SimpleDateFormat SDS_DATE_TIME_FORMAT_NO_MILLISECONDS = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            java.util.Date testDate = SDS_DATE_TIME_FORMAT_NO_MILLISECONDS.parse("2006-09-15T13:06:32");
            System.out.println("Success: "+SDS_DATE_TIME_FORMAT_NO_MILLISECONDS.format(testDate));
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
    
    public static void main(String[] args) {
        //junit.textui.TestRunner.run( suite() );
        
        try {
            //StatusUpdateTestCase test = new StatusUpdateTestCase("testGetShippedUpdate2");
            //StatusUpdateTestCase test = new StatusUpdateTestCase("testGetCancelUpdate2");
            //StatusUpdateTestCase test = new StatusUpdateTestCase("testGetLabeledUpdate2");
            //StatusUpdateTestCase test = new StatusUpdateTestCase("testGetRejectedUpdate2");
            StatusUpdateTestCase test = new StatusUpdateTestCase("testParseResponse3");
            //StatusUpdateTestCase test = new StatusUpdateTestCase("testTimeConversion");
            test.runBare();
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }
    
    /*
     <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
             <soap:Body>
                     <GetPLDUpdateResponse xmlns="http://ScanData.com/">
                             <GetPLDUpdateResult>
                                     <PLD_DOC xmlns="http://ScanData.com/FTD/WTM/DataSetSchemas/WTM_XMLSchema.xsd">
                                             <PLD_ROW>
                                                     <CartonNumber>E0114280</CartonNumber>
                                                     <TrackingNumber>471735758171          </TrackingNumber>
                                                     <DatePlannedShipment>2006-09-14</DatePlannedShipment>
                                                     <Status>REJECTED</Status>
                                                     <DateStatus>2006-09-14T14:24:12</DateStatus>
                                             </PLD_ROW>
                                             <PLD_ROW>
                                                     <CartonNumber>E0114280</CartonNumber>
                                                     <TrackingNumber>471735758171          </TrackingNumber>
                                                     <DatePlannedShipment>2006-09-14</DatePlannedShipment>
                                                     <Status>REJECTED</Status>
                                                     <DateStatus>2006-09-14T14:07:14</DateStatus>
                                             </PLD_ROW>
                                             <PLD_ROW>
                                                     <CartonNumber>E0000014</CartonNumber>
                                                     <TrackingNumber>1ZF123450100009062    </TrackingNumber>
                                                     <DatePlannedShipment>2006-09-05</DatePlannedShipment>
                                                     <Status>REJECTED</Status>
                                                     <DateStatus>2006-09-18T18:10:36</DateStatus>
                                             </PLD_ROW>
                                             <PLD_ROW>
                                                     <CartonNumber>E0114279</CartonNumber>
                                                     <TrackingNumber>1ZF123450100009106    </TrackingNumber>
                                                     <DatePlannedShipment>2006-09-11</DatePlannedShipment>
                                                     <Status>REJECTED</Status>
                                                     <DateStatus>2006-09-15T09:35:15</DateStatus>
                                             </PLD_ROW>
                                     </PLD_DOC>
                             </GetPLDUpdateResult>
                     </GetPLDUpdateResponse>
             </soap:Body>
     </soap:Envelope>
     */
}
