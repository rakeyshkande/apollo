package com.ftd.ship.test;

import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.ship.bo.SDSManifestProcessingBO;

import com.ftd.ship.vo.SDSResponseVO;

import java.sql.Connection;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import junit.framework.TestSuite;

import org.apache.commons.lang.StringUtils;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import org.w3c.dom.Document;

public class ManifestRequestTestCase extends SDSTestCase {
    Connection conn;
    ApplicationContext context;
    private static final Pattern CODE_PATTERN = Pattern.compile("\\d\\d-\\d\\d\\d\\d[A-Z][A-Z]");
    private static final int VENDOR_CODE_LENGTH = 9;
    private static final Pattern ID_PATTERN = Pattern.compile("\\d\\d\\d\\d\\d");
    private static final int VENDOR_ID_LENGTH = 5;
    
    /**
     * Test building a manifesting request per SDS specifications
     */
    public void testBuildRequest() {
        SDSManifestProcessingBO bo = new SDSManifestProcessingBO();
        
        try {
            Document doc = bo.buildRequest("FEDEX","90-0001AA");
            System.out.println(JAXPUtil.toString(doc));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Test parsing a SDS response to a manifesting request
     */
    public void testParseRequest() {
        StringBuilder sb = new StringBuilder();
        sb.append("<?xml version = '1.0' encoding = 'UTF-8'?>\r\n");
        sb.append("<CloseXMLResponse xmlns=\"http://ScanData.com/Comm/WebServices/\">");
        sb.append("<SD_COMM_WS_RES_CLOSE xmlns=\"http://ScanData.com/Comm/WebServices/DataSets/SD_COMM_WS_RES_CLOSE.xsd\">");
        sb.append("<REQUEST>");
        sb.append("<CarrierID>FEDEX</CarrierID>");
        sb.append("<LocationID>90-0001AA</LocationID>");
        sb.append("</REQUEST>");
        sb.append("<STATUS>");
        sb.append("<StatusCode xmlns=\"\">-103012</StatusCode>");
        sb.append("<StatusDescription xmlns=\"\">No shipment to be manifested for &lt;Carrier&gt; at &lt;Location&gt;</StatusDescription>");
        sb.append("</STATUS>");
        sb.append("</SD_COMM_WS_RES_CLOSE>");
        sb.append("</CloseXMLResponse>");
        
        System.out.println(sb.toString());
        
        SDSManifestProcessingBO bo = new SDSManifestProcessingBO();
        
        try {
            Document doc = JAXPUtil.parseDocument(sb.toString(),false,false);
            SDSResponseVO vo = bo.parseResponse(doc);
            System.out.println("Status code: "+vo.getStatusCode());
            System.out.println("Message text: "+vo.getMsgText());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Test the building, transmitting, and parsing of a manifest request/response 
     * by using the SDSManifestProcessingBO
     */
    public void testCloseRequest() {
        try {
            SDSManifestProcessingBO bo = (SDSManifestProcessingBO)context.getBean("sdsManifestProcessingBO");
            bo.processRequest(conn,"FEDEX");
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }
    
    /**
     * Test several strings to determine if the succeed in being matched against 
     * patterns for florist id and carrier codes
     */
    public void testRegExMatch() {
        testRegExMatch("");
        testRegExMatch("91-0001AA");
        testRegExMatch("91-0001AA fedex");
        testRegExMatch("00050");
        testRegExMatch("00061 DHL");
        testRegExMatch("DHL");
    }
    
    /**
     * Test a specific string to determine if the passed string contains a florist id, carrier code, or both
     * @param arg
     */
    private void testRegExMatch(String arg) {
        //What type of request is this
        String vendorId = null;
        String vendorCode = null;
        String carrierId = null;
        try {
            arg = StringUtils.upperCase(StringUtils.trimToEmpty(arg));
            
            if( StringUtils.isBlank(arg) ) {
                vendorId = null;
                vendorCode = null;
                carrierId = null;
            } else {
                
                //Test to see if you got an id
                String testStr = StringUtils.substring(arg,0,VENDOR_ID_LENGTH);
                Matcher m = ID_PATTERN.matcher(testStr);
                
                if( m.matches() ) {
                    vendorId = testStr;    
                    vendorCode = null;
                } else {
                    //Test for a vendor (florist) code
                    testStr = StringUtils.substring(arg,0,VENDOR_CODE_LENGTH);
                    m = CODE_PATTERN.matcher(testStr);
                    
                    if( m.matches() ) {
                        vendorId = null;
                        vendorCode = testStr;
                    } else {
                        //It's not a vendor id or a vendor code so assume that it's a carrier id
                        carrierId = arg;
                        vendorCode = null;
                        vendorId = null;
                    }
                }
                    
                //Check to see if we got a vendor code/id and a carrier id
                if( vendorCode !=null && arg.length()>VENDOR_CODE_LENGTH ) {
                    carrierId = arg.substring(VENDOR_CODE_LENGTH);
                } else if( vendorId !=null && arg.length()>VENDOR_ID_LENGTH ) {
                    carrierId = arg.substring(VENDOR_ID_LENGTH);
                }
            }
            
            StringBuilder sb = new StringBuilder();
            sb.append("arg: ");
            sb.append(arg);
            sb.append("\r\nVendor id: ");
            sb.append(vendorId);
            sb.append("\r\nVendor code: ");
            sb.append(vendorCode);
            sb.append("\r\nCarrier id: ");
            sb.append(carrierId);
            sb.append("\r\n");
            
            System.out.println(sb.toString());
        } catch (Throwable t) {
            t.printStackTrace();   
        }
    }
    
    public ManifestRequestTestCase(String sTestName) {
        super(sTestName);
    }

    public void setUp() throws Exception
    {
        //Start up spring framework
        context = new FileSystemXmlApplicationContext("C:\\jdev_1013\\jdev\\mywork\\ship_processing\\FTD\\ship_processing\\web\\WEB-INF\\spring-ship-servlet.xml");
        this.conn = resourceProvider.getDatabaseConnection();
    }
    
    public void tearDown() throws Exception
    {
        if( conn!=null && !conn.isClosed() ) {
            conn.close();    
        }
    }
    
    public static TestSuite suite()
    { 
        TestSuite suite = null;
        
        try 
        {
            suite = new TestSuite(ManifestRequestTestCase.class);
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        
        return suite;
    }

    public static void main(String[] args) {
        ManifestRequestTestCase manifestRequestTestCase = new ManifestRequestTestCase("testCloseRequest");
        manifestRequestTestCase.run();
    }
}
