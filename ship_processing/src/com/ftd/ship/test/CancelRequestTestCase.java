package com.ftd.ship.test;

import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.ship.bo.SDSOutboundMsgProcessingBO;
import com.ftd.ship.bo.communications.sds.SDSCommunications;
import com.ftd.ship.dao.ShipDAO;
import com.ftd.ship.vo.SDSResponseVO;
import com.ftd.ship.vo.VenusMessageVO;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.URLEncoder;
import java.net.UnknownHostException;

import java.sql.Connection;

import java.text.SimpleDateFormat;

import java.util.Date;

import junit.framework.TestSuite;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import org.w3c.dom.Document;


public class CancelRequestTestCase extends SDSTestCase {
    Connection conn;
    ApplicationContext context;
    protected static final SimpleDateFormat SDS_DATE_TIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");

    /**
     * Test building a well formed cancel document per SDS specifications
     */
    public void testBuildCancelDocument() {
        VenusMessageVO vmVO = new VenusMessageVO();
        vmVO.setVenusOrderNumber("E0000009");
        
        SDSOutboundMsgProcessingBO bo = (SDSOutboundMsgProcessingBO)context.getBean("sdsOutProcessingBO");
        try {
            Document doc = bo.buildCancelRequest(vmVO);
            String xmlString = JAXPUtil.toString(doc);
            System.out.println(xmlString);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Test parsing a manually constructed responsed for a "cancel request" 
     */
    public void testParseCancelResponse() {
        StringBuilder sb = new StringBuilder();
        sb.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
        sb.append("<CancelShipmentXMLResponse xmlns=\"http://ScanData.com/Comm/WebServices/\">");
        sb.append("<CancelShipmentXMLResult>");
        sb.append("<SD_COMM_WS_RES_CANCEL xmlns=\"http://ScanData.com/Comm/WebServices/DataSets/SD_COMM_WS_RES_CANCEL.xsd\">");
        sb.append("<REQUEST>");
        sb.append("<CartonNumber>1234567815</CartonNumber>");
        sb.append("</REQUEST>");
        sb.append("<STATUS>");
        sb.append("<StatusCode xmlns=\"\">102012</StatusCode>");
        sb.append("<StatusDescription xmlns=\"\">Carton&lt;1234567815&gt; has been CANCELED already.|</StatusDescription>");
        sb.append("</STATUS>");
        sb.append("</SD_COMM_WS_RES_CANCEL>");
        sb.append("</CancelShipmentXMLResult>");
        sb.append("</CancelShipmentXMLResponse>");
        
        SDSOutboundMsgProcessingBO bo = new SDSOutboundMsgProcessingBO();
        try {
            System.out.println(sb.toString());
            Document doc = JAXPUtil.parseDocument(sb.toString(),false,false);
            
            SDSResponseVO responseVO = bo.parseCancelResponse(doc);
            System.out.println("Cancel response code: "+responseVO.getStatusCode());
            System.out.println("Response text: "+responseVO.getMsgText());
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Test creating, transmitting, and parsing a cancel request via the SDSOutboundMsgBrocessBO class 
     */
    public void testManualCancel() {
        VenusMessageVO vmVO = new VenusMessageVO();
        vmVO.setVenusOrderNumber("E0114278");
        vmVO.setCancelReasonCode("SHP");

        try {
            SDSOutboundMsgProcessingBO bo = (SDSOutboundMsgProcessingBO)context.getBean("sdsOutProcessingBO");
            Document doc = bo.buildCancelRequest(vmVO);
            String xmlString = JAXPUtil.toString(doc);
            System.out.println(xmlString);
            
            SDSCommunications comm = (SDSCommunications)context.getBean("sdsCommunications");
            System.out.println("Start: "+SDS_DATE_TIME_FORMAT.format(new Date()));
            Document response = comm.cancelShipmentXML(conn, doc);
            System.out.println("  End: "+SDS_DATE_TIME_FORMAT.format(new Date()));
            System.out.println("Response: "+JAXPUtil.toString(response));
            SDSResponseVO rVO = bo.parseCancelResponse(response);
            System.out.println("Success: "+rVO.isSuccess());
            System.out.println("Status code: "+rVO.getStatusCode());
            System.out.println("Msg Text: "+rVO.getMsgText());
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Test building, sending, and parsing the response for a ship request by calling SDSOutboundMsgProcessingBO.processCancelRequest
     */
    public void testCancel() {
        try {
        ShipDAO shipDAO = (ShipDAO)context.getBean("shipDAO");
        SDSOutboundMsgProcessingBO bo = (SDSOutboundMsgProcessingBO)context.getBean("sdsOutProcessingBO");
        VenusMessageVO vmVO = shipDAO.getVenusMessage(conn,"26353");
        bo.processCancelRequest(conn,vmVO);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
  
    public CancelRequestTestCase(String sTestName) {
        super(sTestName);
    }

    public void setUp() throws Exception
    {   
        //Start up spring framework
        context = new FileSystemXmlApplicationContext("C:\\jdev_1013\\jdev\\mywork\\ship_processing\\FTD\\ship_processing\\web\\WEB-INF\\spring-ship-servlet.xml");
        this.conn = resourceProvider.getDatabaseConnection();
    }
    
    public void tearDown() throws Exception
    {
        if( conn!=null && !conn.isClosed() ) {
            conn.close();    
        }
    }
    
    public static TestSuite suite()
    { 
        TestSuite suite = null;
        
        try 
        {
            suite = new TestSuite(CancelRequestTestCase.class);
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        
        return suite;
    }
    
    public static void main(String[] args) {
        //junit.textui.TestRunner.run( suite() );
//         System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.SimpleLog");
//         System.setProperty("org.apache.commons.logging.simplelog.showdatetime", "true");
//         System.setProperty("org.apache.commons.logging.simplelog.log.httpclient.wire", "debug");
//         System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.commons.httpclient", "debug");
        try {
            //CancelRequestTestCase cancelRequestTestCase = new CancelRequestTestCase("testBuildCancelDocument");
            //CancelRequestTestCase cancelRequestTestCase = new CancelRequestTestCase("testParseCancelResponse");
            CancelRequestTestCase cancelRequestTestCase = new CancelRequestTestCase("testManualCancel");
            
            
            //CancelRequestTestCase cancelRequestTestCase = new CancelRequestTestCase("testConnection2");
            cancelRequestTestCase.runBare();
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    /**
     * Test connectivity to the dev SDS ship server
     */
    public void testSocketConnection() {
        String response = "";
        Socket socket = null;
        PrintWriter out = null;
        BufferedReader in = null;
        //http://70.61.53.77:5130/vsams.asmx
        String serverName = "70.61.53.77";
        int portNumber = 5130;
        int timeout = 30000;
        String requestString ="<?xml version=\"1.0\" encoding=\"UTF-8\"?><soapenv:Envelope xmlns:soapenv=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><soapenv:Body><CancelShipmentXML xmlns=\"http://ScanData.com/Comm/WebServices/\"><xdReqCancel><SD_COMM_WS_REQ_CANCEL xmlns=\"http://ScanData.com/Comm/WebServices/DataSets/SD_COMM_WS_REQ_CANCEL.xsd\"><REQUEST><CartonNumber>E0000001</CartonNumber><CancelReason>SHP</CancelReason></REQUEST></SD_COMM_WS_REQ_CANCEL></xdReqCancel></CancelShipmentXML></soapenv:Body></soapenv:Envelope>";
        //System.out.println(requestString);

        try 
        {
          // JDK 1.4
          InetSocketAddress address = new InetSocketAddress(serverName, portNumber);
          //socket = new Socket(serverName, portNumber);
          socket = new Socket();
          socket.setSoTimeout(timeout);
          socket.connect(address, timeout);

          out = new PrintWriter(socket.getOutputStream(), true);
          in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            String encodedString = URLEncoder.encode(requestString,"utf-8");
            //System.out.println(encodedString);
          
            out.println("POST /vsams.asmx HTTP/1.1");
            out.println("Host: 70.61.53.77");
//            out.println("Content-Type: application/soap+xml; charset=utf-8");
            out.println("Content-Type: text/xml; charset=utf-8");
            out.println("Content-Length: "+encodedString.length());
            out.println("SOAPAction: \"http://ScanData.com/Comm/WebServices/CancelShipmentXML\"");
            out.println();

            out.println(encodedString);
                        out.println();
                        out.flush();
                        
            StringBuilder sb = new StringBuilder();
            response = in.readLine();
            while( response!=null && response.length()>0 ) 
            {
                sb.append(response);
                response = in.readLine();
            }
            response = sb.toString();
            System.out.println(response);
        }
        catch (UnknownHostException e)
        {            
          e.printStackTrace();
        }
        catch (IOException e)
        {
          e.printStackTrace();
        }

//        out.println(transmitData);
//        out.flush();
//          
//        logger.debug("SendUpdateFeed: Data sent");
//        
//        try
//        {
//          logger.debug("SendUpdateFeed: Waiting for response");
//          StringBuffer sb = new StringBuffer();
//          response = in.readLine();
//          while( response!=null && response.length()>0 ) 
//          {
//              sb.append(response);
//              response = in.readLine();
//          }
//          response = sb.toString();
//          logger.debug("Novator Response:" + response);
//        } 
//        catch(SocketTimeoutException stoe)
//        {
//          logger.error(stoe);
//          throw new Exception(stoe);
//        }
//        catch(IOException ioe)
//        {
//          logger.error(ioe);
//          throw new Exception(ioe);   
//        } 
        finally 
        {
          try
          {
            out.close();
            in.close();
            socket.close();
          }
          catch(IOException ioe)
          {
            ioe.printStackTrace();   
          }
        }
          
    }

    /**
     * Test connectivity to the dev SDS ship server
     */
    public void testConnection2() {
        Document response = null;
        PostMethod post = new PostMethod("http://70.61.53.77:5130/vsams.asmx");
        
        try {
            String requestString ="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<soapenv:Envelope xmlns:soapenv=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><soapenv:Body><CancelShipmentXML xmlns=\"http://ScanData.com/Comm/WebServices/\"><xdReqCancel><SD_COMM_WS_REQ_CANCEL xmlns=\"http://ScanData.com/Comm/WebServices/DataSets/SD_COMM_WS_REQ_CANCEL.xsd\"><REQUEST><CartonNumber>E0000001</CartonNumber><CancelReason>SHP</CancelReason></REQUEST></SD_COMM_WS_REQ_CANCEL></xdReqCancel></CancelShipmentXML></soapenv:Body></soapenv:Envelope>";
            RequestEntity entity = new StringRequestEntity(requestString);
            post.setRequestEntity(entity);
            post.setRequestHeader("SOAPAction", "http://ScanData.com/Comm/WebServices/CancelShipmentXML");
            post.setRequestHeader("Content-Type", "text/xml; charset=utf-8");
            HttpClient httpclient = new HttpClient();
            
            int result = httpclient.executeMethod(post);
            //Do we need to worry about any other response codes?
            if( result!=200 ) {
                System.out.println("Unexpected http result code of "+result+" from update status request to SDS server");
            }
        
            String strResponse = post.getResponseBodyAsString();
            System.out.println(strResponse);
            
            response = JAXPUtil.parseDocument(strResponse,false,false);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if( post!=null ) {
                post.releaseConnection();
            }
        }
    }
}
