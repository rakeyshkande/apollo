package com.ftd.ship.bo;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.JAXPUtil;

import com.ftd.ship.bo.communications.sds.SDSCommunications;
import com.ftd.ship.common.JMSPipeline;
import com.ftd.ship.common.MsgDirection;
import com.ftd.ship.common.MsgType;
import com.ftd.ship.common.SDSConstants;
import com.ftd.ship.common.ShipConstants;
import com.ftd.ship.common.resources.SDSStatusParm;
import com.ftd.ship.vo.SDSMessageVO;

import java.math.BigDecimal;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.Date;

import java.util.List;
import java.text.SimpleDateFormat;

import org.apache.commons.lang.StringUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class SDSMissingStatusBO extends SDSMsgRetrievalProcessingBO {
    private static final Logger LOGGER = new Logger("com.ftd.ship.bo.SDSMissingStatusBO"); 
    private static final String CONFIG_MISSING_STATUS_DAYS = "MISSING_STATUS_DAYS";
    private static final Long DEFAULT_MISSING_STATUS_DAYS = 7L;
    private static final String CONFIG_MAX_MISSING_STATUS_REQUESTS = "MISSING_STATUS_MAX_REQUESTS";
    private static final Long DEFAULT_MAX_REQUEST_REQUESTS = 100L;
    private static final String CONFIG_MISSING_STATUS_QUEUE_DELAY = "MISSING_STATUS_QUEUE_DELAY_SECONDS";
    private static final Long DEFAULT_MAX_QUEUE_DELAY = 600L; //Ten minutes
    
    public SDSMissingStatusBO() {
    }
    
    public void processRequest(Connection connection) throws Exception {
        processRequest(connection,null,null,null,null);
    }
    
    public void processRequest(Connection connection, 
                               List<String> cartonNumbers, 
                               Long missingStatusDays, 
                               Long maxRecords, 
                               Long queueDelay) throws Exception {  
        
        if( cartonNumbers==null ) {
            if( missingStatusDays==null ) {
                try {
                    String strTmp = 
                      getGlobalParameter(connection,ShipConstants.GLOBAL_PARMS_CONTEXT,
                      CONFIG_MISSING_STATUS_DAYS,String.valueOf(DEFAULT_MISSING_STATUS_DAYS));
                    missingStatusDays = new Long(strTmp);
                } catch (Throwable t) {
                    LOGGER.error("Failed to retrieve "+CONFIG_MISSING_STATUS_DAYS+
                      " from global parms.  Defaulting to "+String.valueOf(DEFAULT_MISSING_STATUS_DAYS));
                    missingStatusDays = DEFAULT_MISSING_STATUS_DAYS;
                }
            }
            
            cartonNumbers = shipDAO.getMissingStatus(connection,missingStatusDays);
        }
        
        if(cartonNumbers.size()==0 ) {
            LOGGER.debug("No missing status to process");
            return;
        }
        
        if( maxRecords==null ) {
            try {
                String strTmp = 
                  getGlobalParameter(connection,ShipConstants.GLOBAL_PARMS_CONTEXT,
                  CONFIG_MAX_MISSING_STATUS_REQUESTS,String.valueOf(DEFAULT_MAX_REQUEST_REQUESTS));
                maxRecords = new Long(strTmp);
            } catch (Throwable t) {
                LOGGER.error("Failed to retrieve "+CONFIG_MAX_MISSING_STATUS_REQUESTS+
                  " from global parms.  Defaulting to "+String.valueOf(DEFAULT_MAX_REQUEST_REQUESTS));
                maxRecords = DEFAULT_MAX_REQUEST_REQUESTS;
            }
        }
        
        for( int idx=0; idx<cartonNumbers.size(); ) {
            List<String> submittedCartonNumbers = new ArrayList<String>();
            for( int idx2=0; idx2<maxRecords && idx<cartonNumbers.size(); idx2++) {
                submittedCartonNumbers.add(cartonNumbers.get(idx++));
            }
        
            Document requestDoc = buildRequest(cartonNumbers);
            LOGGER.debug(JAXPUtil.toString(requestDoc));
            
            Document responseDoc = sdsCommunications.getOrderStatus(connection,requestDoc);
            LOGGER.debug(JAXPUtil.toString(responseDoc));
            
            List<SDSMessageVO> messages = this.parseResponse(responseDoc);
            processResponse(connection,messages);
        }
    }
    
    public Document buildRequest(List<String> cartonNumbers) throws Exception {
        Document doc = JAXPUtil.createDocument(); 
        Element docRoot = doc.createElement("GET_ORDER_STATUS_PARAMS");
        docRoot.setAttribute("xmlns","");
        doc.appendChild(docRoot);
        
        for( int idx=0; idx<cartonNumbers.size(); idx++) {
            docRoot.appendChild(JAXPUtil.buildSimpleXmlNode(doc,"OrderKey",cartonNumbers.get(idx)));
        }
        
        return doc;
    }
    
    public List<SDSMessageVO> parseResponse(Document doc) throws Exception {
        List<SDSMessageVO> response = new ArrayList<SDSMessageVO>();

        NodeList list = JAXPUtil.selectNodes(doc,"//*[local-name()='"+SDSConstants.TAG_STATUS_MISSING_ORDER_STATUS_ROW+"']");
        LOGGER.info("Status update found = "+String.valueOf(list.getLength()));
        
        SDSMessageVO msgVO;
        for( int idx=0; idx<list.getLength(); idx++ ) {
            Element row = (Element)list.item(idx);
             
            //Status
            String strValue = JAXPUtil.getFirstChildNodeTextByTagName(row,SDSConstants.TAG_STATUS_TYPE);
            if( StringUtils.equals(strValue,"INITIAL") ) {
                //Don't care about them (equivalent to the venus.sds_status "AVAILABLE" state
                //Defect 3552
                continue;
            }
            
            msgVO = new SDSMessageVO();
             
            if( StringUtils.equals(strValue,MsgType.CANCELED_NOT_LABELED.toString()) ) {
                LOGGER.debug("SDS status CANCELED_NOT_LABELED being changed to CANCELED");
                msgVO.setStatus(MsgType.CANCEL.toString());
                msgVO.setMsgType(MsgType.CANCEL);
            } else {
                msgVO.setStatus(strValue);
                msgVO.setMsgType(MsgType.sdsToMsgType(strValue));
            }
            
            if( StringUtils.equals(strValue,SDSStatusParm.CANCELED.toString()) ) {
                msgVO.setDirection(MsgDirection.OUTBOUND);    
            } else {
                msgVO.setDirection(MsgDirection.INBOUND);
            }
            
            //Carton number
            strValue = JAXPUtil.getFirstChildNodeTextByTagName(row,SDSConstants.TAG_SHIP_CARTON_NUMBER);
            msgVO.setCartonNumber(strValue);
            
            //Tracking number
            strValue = JAXPUtil.getFirstChildNodeTextByTagName(row,SDSConstants.TAG_SHIP_TRACKING_NUMBER);
            msgVO.setTrackingNumber(strValue);
            
            //Print Number
            strValue = JAXPUtil.getFirstChildNodeTextByTagName(row,SDSConstants.TAG_STATUS_BATCH_ID);
            if( StringUtils.isNotBlank(strValue) ) {
                msgVO.setPrintBatchNumber(strValue);
            }
            
            //Batch Sequence
            strValue = JAXPUtil.getFirstChildNodeTextByTagName(row,SDSConstants.TAG_STATUS_BATCH_SEQUENCE);
            try {
                BigDecimal batchSequence = new BigDecimal(strValue);
                msgVO.setPrintBatchSequence(batchSequence);
            } catch (Exception e) {
                msgVO.setPrintBatchSequence(null);
            }
            
            //Ship Date
            strValue = JAXPUtil.getFirstChildNodeTextByTagName(row,SDSConstants.TAG_STATUS_MISSING_SHIP_DATE);
            try {
                 msgVO.setShipDate((new SimpleDateFormat(SDS_DATE_FORMAT)).parse(strValue));
            } catch (Exception e) {
                 LOGGER.error("Error converting ship date "+strValue+".  Setting to current date");
                 msgVO.setShipDate(new Date());
            }
             
            //Status Date
            if( MsgType.PRINT.equals(msgVO.getMsgType())  ) {
                strValue = JAXPUtil.getFirstChildNodeTextByTagName(row,SDSConstants.TAG_STATUS_MISSING_PACKED_DATE);
            } else if( MsgType.SHIP.equals(msgVO.getMsgType()) ) {
                strValue = JAXPUtil.getFirstChildNodeTextByTagName(row,SDSConstants.TAG_STATUS_MISSING_SHIP_DATE);
            } else {
                strValue = (new SimpleDateFormat(SDS_DATE_TIME_FORMAT_NO_MILLISECONDS)).format(new Date());
            }
            
            try {
                msgVO.setStatusDate((new SimpleDateFormat(SDS_DATE_TIME_FORMAT_NO_MILLISECONDS)).parse(strValue));
            } catch (Exception e) {
                LOGGER.error("Error converting status date "+strValue+".  Setting to current date");
                msgVO.setStatusDate(new Date());
            }
            
            msgVO.setProcessed(false);
            
            if( msgVO.getMsgType()==null ) {
                //Invalid status received so return the element to report on
                msgVO.setValue(JAXPUtil.toString(row));
            }
            response.add(msgVO);
        }
        
        return response;
    }
}
