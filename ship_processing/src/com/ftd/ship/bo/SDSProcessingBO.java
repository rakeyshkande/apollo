package com.ftd.ship.bo;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.ship.bo.communications.sds.SDSApplicationException;
import com.ftd.ship.bo.communications.sds.SDSCommunications;
import com.ftd.ship.common.JMSPipeline;
import com.ftd.ship.common.MsgType;
import com.ftd.ship.common.ShipConstants;
import com.ftd.ship.common.framework.util.CommonUtils;
import com.ftd.ship.common.resources.ResourceProviderBase;
import com.ftd.ship.dao.OrderDAO;
import com.ftd.ship.dao.QueueDAO;
import com.ftd.ship.dao.ShipDAO;
import com.ftd.ship.vo.CommentsVO;
import com.ftd.ship.vo.GlobalParameterVO;
import com.ftd.ship.vo.OrderDetailVO;
import com.ftd.ship.vo.OrderVO;
import com.ftd.ship.vo.QueueVO;
import com.ftd.ship.vo.SDSTransactionVO;
import com.ftd.ship.vo.VenusMessageVO;

import java.sql.Connection;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;


public class SDSProcessingBO {
    private static final Logger logger  = new Logger("com.ftd.ship.bo.SDSProcessingBO");
    protected static final String MMDDYYYY_FORMAT = "MM/dd/yyyy";
    protected static final String EMAIL_FORMAT = "EEE, d MMMMM, yyyy";
    protected static final String SDS_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS";
    protected static final String SDS_DATE_TIME_FORMAT_NO_MILLISECONDS = "yyyy-MM-dd'T'HH:mm:ss";
    protected static final String SDS_DATE_FORMAT = "yyyy-MM-dd";
    protected ResourceProviderBase resourceProvider;
    protected ShipDAO shipDAO;
    protected OrderDAO orderDAO;
    protected QueueDAO queueDAO;
    protected SDSCommunications sdsCommunications;
    
    public SDSProcessingBO() {
    }
    
    /*
     * Insert a comment into the DB
     * Consume all exceptions (i.e. don't cause a rollback if it fails
     */
    protected void createComment(Connection conn, String comment, OrderDetailVO orderDetailVO)
    {   
        try {
            CommentsVO commentsVO = new CommentsVO();
            commentsVO.setComment(comment);
            commentsVO.setCommentOrigin(ShipConstants.SHIP_PROCESSING_COMMENT_ORIGIN);
            commentsVO.setCommentType(ShipConstants.COMMENT_TYPE_ORDER);
            commentsVO.setCreatedBy(ShipConstants.SHIP_PROCESSING_COMMENT_ORIGIN);
            commentsVO.setCustomerId(Long.toString(orderDetailVO.getRecipientId()));
            commentsVO.setOrderDetailId(Long.toString(orderDetailVO.getOrderDetailId()));
            commentsVO.setOrderGuid(orderDetailVO.getOrderGuid());
            orderDAO.insertComment(conn,commentsVO);
        } catch (Throwable t) {
            logger.warn("Adding comment to the database has failed.  Continuing with the transaction.",t);
        }
    }

    protected void sendToQueue(Connection connection, VenusMessageVO venusMessageVO, OrderDetailVO orderDetailVO, OrderVO orderVO ) throws SDSApplicationException {
        try {
            //insert into queue
            QueueVO queueVO = new QueueVO();
            queueVO.setQueueType(venusMessageVO.getMsgType().getFtdMsgType());
            queueVO.setMessageTimestamp(new java.util.Date());
            queueVO.setMessageType(venusMessageVO.getMsgType().getFtdMsgType());
            queueVO.setSystem(ShipConstants.ARGO_SYSTEM);
            queueVO.setMercuryNumber(venusMessageVO.getVenusOrderNumber());
            queueVO.setMasterOrderNumber(orderVO.getMasterOrderNumber());
            queueVO.setOrderGuid(orderDetailVO.getOrderGuid());
            queueVO.setOrderDetailId(Long.toString(orderDetailVO.getOrderDetailId()));
            queueVO.setExternalOrderNumber(orderDetailVO.getExternalOrderNumber());
            queueVO.setMercuryId(venusMessageVO.getVenusId());
            queueDAO.insertQueueRecord(connection,queueVO);      
        } catch (Exception e) {
            throw new SDSApplicationException("Error while trying to send msg to queue",e);
        }
    }

    protected void sendToQueue(Connection connection, VenusMessageVO venusMessageVO, OrderDetailVO orderDetailVO ) throws SDSApplicationException {
    
        OrderVO orderVO = null;
        
        try {
            orderVO = orderDAO.getOrder(connection,orderDetailVO.getOrderGuid());
        } catch (Exception e) {
            throw new SDSApplicationException("Error while trying to lookup related associated order data to send to queue");
        }
        
        sendToQueue(connection,venusMessageVO,orderDetailVO,orderVO);
    }
    
    protected void enqueueJmsMessage(Connection conn, JMSPipeline pipeline, String message) throws SDSApplicationException {
         enqueueJmsMessage(conn, pipeline, message, 0L);
    }
    
    protected void enqueueJmsMessage(Connection conn, JMSPipeline pipeline, String message, long millisecondsDelay) throws SDSApplicationException {
        try {
            long secondsDelay = 0;
            
            if( millisecondsDelay > 0 ) {
                secondsDelay = (long)millisecondsDelay/1000;

                if ( secondsDelay > Integer.MAX_VALUE ) {
                    secondsDelay = Integer.MAX_VALUE;
                }
            }
            
//            InitialContext context = new InitialContext();
//            CommonUtils.sendJMSMessage(conn,context,message,pipeline,(int)secondsDelay);
             CommonUtils.sendJMSMessage(conn,message,message,pipeline,(int)secondsDelay);
        } catch (Exception e) {
            logger.error("Error while trying to enqueue JMS message");
            throw new SDSApplicationException("Error while trying to enqueue JMS message",e);
        }
    }

    /*
     * Get the order message assocated with the past in venus messageVO
     */
    protected VenusMessageVO getAssociatedOrder(Connection conn,String externalOrderNumber) throws SDSApplicationException
    {
        //Retrieve venus FTD order message related to this message 
        List venusList = null;
        try {
            venusList = shipDAO.getVenusMessage(conn, externalOrderNumber, MsgType.ORDER.getFtdMsgType());
        } catch (Exception e) {
            throw new SDSApplicationException("Error retrieving associated order for "+externalOrderNumber);
        }
        if(venusList == null || venusList.size() == 0)
        {
            throw new SDSApplicationException("Corresponding order message could not be found for Venus order " + externalOrderNumber);
        }
        else
        {
            if(venusList.size() > 1)
            {
                throw new SDSApplicationException("Multiple FTDs were found for the for Venus order " + externalOrderNumber);                        
            }
        }
        VenusMessageVO foundOrder = (VenusMessageVO)venusList.get(0);    
        
        return foundOrder;
    }
    
    protected void insertSDSTransaction(Connection conn, String venusId, String request, String response) throws Exception {
        SDSTransactionVO vo = new SDSTransactionVO();
        vo.setVenusId(venusId);
        vo.setRequest(request);
        vo.setResponse(response);
        
        shipDAO.insertSDSTransaction(conn,vo);
    }
    
    protected String getGlobalParameter(Connection connection, String context, String paramName, String defaultValue ) {
        String retVal = "";
        GlobalParameterVO vo = null;
        try {
            vo = CommonUtils.getGlobalParameter(connection,context,paramName,defaultValue);
        } catch (Exception e) {
            logger.warn("Global parameter "+context+"/"+paramName+" not found.  Will use default value of "+defaultValue,e);
        }
        
        if( vo==null ) {
            if( StringUtils.isNotBlank(defaultValue) ) {
                retVal = defaultValue;
            }
        } else {
            retVal = vo.getValue();
        }
        
        return retVal;
    }
    
    public static Date tzDateTimeToDate(String tzDateTimeString) throws Exception {
        String strDate = StringUtils.substring(tzDateTimeString,0,10);
        return (new SimpleDateFormat(SDS_DATE_FORMAT)).parse(strDate);
    }

    public void setShipDAO(ShipDAO shipDAO) {
        this.shipDAO = shipDAO;
    }

    public ShipDAO getShipDAO() {
        return shipDAO;
    }

    public void setOrderDAO(OrderDAO orderDAO) {
        this.orderDAO = orderDAO;
    }

    public OrderDAO getOrderDAO() {
        return orderDAO;
    }

    public void setQueueDAO(QueueDAO queueDAO) {
        this.queueDAO = queueDAO;
    }

    public QueueDAO getQueueDAO() {
        return queueDAO;
    }

    public void setResourceProvider(ResourceProviderBase resourceProvider) {
        this.resourceProvider = resourceProvider;
    }

    public ResourceProviderBase getResourceProvider() {
        return resourceProvider;
    }

    public void setSdsCommunications(SDSCommunications sdsCommunications) {
        this.sdsCommunications = sdsCommunications;
    }

    public SDSCommunications getSdsCommunications() {
        return sdsCommunications;
    }
    
    protected SimpleDateFormat getSdsDateTimeFormatLenient() {
        SimpleDateFormat sdf = new SimpleDateFormat(SDS_DATE_TIME_FORMAT);
        sdf.setLenient(true);
        return sdf;
    }

}
