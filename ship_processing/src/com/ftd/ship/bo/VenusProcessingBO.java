package com.ftd.ship.bo;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.ship.common.VenusStatus;
import com.ftd.ship.dao.ShipDAO;
import com.ftd.ship.vo.CarrierVO;

import java.sql.Connection;

import java.util.List;
import java.util.Random;


public class VenusProcessingBO 
{
    private static Logger logger  = new Logger("com.ftd.ship.dao.VenusProcessingBO");
    private ShipDAO shipDAO;
    private QueueBO queueBO;
    
    private static final Random random = new Random();
    
    public VenusProcessingBO()
    {
    }
    
    /**
    * This is the processVenusOrder() which is the primary method of execution
    * for processing the new carrier delivery method
    * 
    * @param connection database connection
    * @param venusId The String which contains the venus Id for the order
    * @throws Exception
    */
    public void processVenusOrder(Connection connection, String venusId) throws Exception
    {
        logger.debug("Start: processVenusOrder(" + venusId + ")");
        
        try
        {
            // Select carrier for order using ratios
            List carrierList = shipDAO.loadVendorCarriers(connection, venusId);
            
            // Determine the cap for the random number generator and remove any 
            // carriers with a ratio of 0
            int randomCap = 0;
            for (int i = 0; i < carrierList.size(); i++) 
            {
                int carrierRatio = ((CarrierVO) carrierList.get(i)).getRatio().intValue();
                if(carrierRatio == 0)
                {
                    carrierList.remove(i);
                    i--;
                }
                
                randomCap += carrierRatio;
            }
            
            // Throw exception if carrier list is empty or null
            if(carrierList == null || carrierList.size() == 0)
            {
                throw new Exception("Error in configuration of carriers for Venus ID: " + venusId);
            }
            
            // Determine correct carrier to use for order
            CarrierVO carrier = this.processCarrierRatio(venusId, carrierList, randomCap);
            
            // Switch FTD ship method to carrier ship method
            logger.debug("Switching ship method for " + carrier.getCarrierName());
            shipDAO.updateCarrierDeliveryMethod(connection, carrier.getReferenceNumber(), carrier.getCarrierId());
                 
            // Update venus status to open so it will be picked up by venus scheduled process
            shipDAO.updateVenusStatus(connection, venusId, VenusStatus.OPEN);
        }
        catch(Exception e)
        {
            // Send message to reject queue
            queueBO.sendRejectMessageToQueue(connection, venusId);
            
            // Pass exception up
            throw e;
        }
        
        logger.debug("End: processVenusOrder()");
    }
    
    /**
    * Determines carrier from the ratios stored in the database for the carriers
    * of the specific vendor
    * 
    * @param venusId The String which contains the venus Id for the order
    * @param carrierList The list of carriers
    * @param randomCap The max number that will be randomized
    * @throws Exception
    * @return CarrierVO The selected carrier from the ratio processing 
    */
    private CarrierVO processCarrierRatio(String venusId, List carrierList, int randomCap) throws Exception
    {
        logger.debug("Start: processCarrierRatio(" + venusId + ", " + carrierList.size() + "," + randomCap + ")");
        
        // Pull random number
        int randomNumber = random.nextInt(randomCap);
        logger.debug("processCarrierRatio randomNumber: " + randomNumber);
        
        // Determine carrier based on ratios
        int currentStart = 0;
        CarrierVO carrier = null;
        for (int i = 0; i < carrierList.size(); i++) 
        {
            carrier = (CarrierVO) carrierList.get(i);
            if((randomNumber >= currentStart) && (randomNumber < (currentStart + carrier.getRatio().intValue())))
            {
                logger.debug("End: processVenusOrder() returning - " + carrier.getCarrierName());
                return carrier;
            }
            
            currentStart = currentStart + carrier.getRatio().intValue();
        }
        
        // If no carrier selected (should never get here)
        logger.debug("End: processVenusOrder() returning null");
        return null;
    }

    public void setShipDAO(ShipDAO shipDAO) {
        this.shipDAO = shipDAO;
    }

    public ShipDAO getShipDAO() {
        return shipDAO;
    }

    public void setQueueBO(QueueBO queueBO) {
        this.queueBO = queueBO;
    }

    public QueueBO getQueueBO() {
        return queueBO;
    }
}
