package com.ftd.ship.bo;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;

import com.enterprisedt.net.ftp.FTPClient;
import com.enterprisedt.net.ftp.FTPConnectMode;
import com.enterprisedt.net.ftp.FTPTransferType;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.ship.bo.communications.sds.SDSApplicationException;
import com.ftd.ship.common.JMSPipeline;
import com.ftd.ship.common.ShipConstants;
import com.ftd.ship.common.framework.util.CommonUtils;
import com.ftd.ship.vo.CarrierScanVO;


public abstract class CarrierScansBO extends SDSProcessingBO {
    private static final Logger logger = new Logger("com.ftd.ship.bo.CarrierScansBO");
    public static final String DHL_FORMAT = "DHL";
    public static final String FEDEX_FORMAT = "FEDEX";
    public static final String UPS_FORMAT = "UPS";
    public static final String APPLICATION_CONTEXT = "ship_processing";
    private static final String PARAM_LOGIN = "NOSCAN_FTP_LOGIN_";
    private static final String PARAM_PASSWORD = "NOSCAN_FTP_PASSWORD_";
    private static final String PARAM_SERVER = "NOSCAN_FTP_SERVER_";
    private static final String PARAM_REMOTE_DIRECTORY = "NOSCAN_FTP_REMOTE_DIRECTORY_";
    private static final String PARAM_LOCAL_DIRECTORY = "NOSCAN_FTP_LOCAL_DIRECTORY_";
    private static final String PARAM_DELETE_REMOTE_FILE = "NOSCAN_FTP_DELETE_REMOTE_FILE_";
    protected static final String PARAM_ARCHIVE_DIRECTORY = "NOSCAN_ARCHIVE_ROOT_DIRECTORY";
    protected static final String DEFAULT_ARCHIVE_ROOT = "/u02/apollo/noscan";
    protected static final String ARCHIVE_FORMAT = "yyyyMM";
    private static final String PARAM_WORKING_DIRECTORY = "NOSCAN_WORKING_DIRECTORY";
    private static final String DEFAULT_WORKING_DIRECTORY = "/tmp";
    protected String carrierId = "";
    
    public CarrierScansBO() {
    }
    
    public String getCarrierId() {
        return carrierId;
    }
    
    public void getScans(Connection connection) throws SDSApplicationException {
        getScans(connection, carrierId);
    }
    
    protected void getScans(Connection connection, String carrierId) throws SDSApplicationException {            
        boolean hasErrors = false;
        try {
            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
            String host = getGlobalParameter(connection,ShipConstants.GLOBAL_PARMS_CONTEXT,PARAM_SERVER+carrierId,"localhost");
            String userId = configUtil.getSecureProperty(APPLICATION_CONTEXT,PARAM_LOGIN+carrierId);
            String credentials = configUtil.getSecureProperty(APPLICATION_CONTEXT,PARAM_PASSWORD+carrierId);
            String localDirectory = getGlobalParameter(connection,ShipConstants.GLOBAL_PARMS_CONTEXT,PARAM_LOCAL_DIRECTORY+carrierId,"./");
            String remoteDirectory = getGlobalParameter(connection,ShipConstants.GLOBAL_PARMS_CONTEXT,PARAM_REMOTE_DIRECTORY+carrierId,"./");
            boolean deleteRemoteFile = BooleanUtils.toBoolean(getGlobalParameter(connection,ShipConstants.GLOBAL_PARMS_CONTEXT,PARAM_DELETE_REMOTE_FILE+carrierId,"true"));
            
            logger.debug("host: "+host);
            logger.debug("userId: "+userId);
            logger.debug("credentials: ##########");
            logger.debug("localDirectory: "+localDirectory);
            logger.debug("remoteDirectory: "+remoteDirectory);
            logger.debug("delete file: "+deleteRemoteFile);
            
            FTPClient ftpClient = new FTPClient(host);
            ftpClient.login(userId, credentials);
            ftpClient.setType(FTPTransferType.ASCII);
            ftpClient.setConnectMode(FTPConnectMode.PASV);
            if( remoteDirectory!=null && remoteDirectory.length()>0) {
                ftpClient.chdir(remoteDirectory);
            }

            String[] fileNames = null;
            try {
                logger.info("Getting the list of files");
                fileNames = ftpClient.dir("*");
                if( fileNames.length==0 ) {
                    logger.info("Nothing to download");
                    return;
                }
            } catch (Exception e) {
                logger.info("Nothing to download");
                return;
            }
                
            //Check to see if the download directory exists
            File fLocal = new File(localDirectory);
            if( !fLocal.exists() ) {
                FileUtils.forceMkdir(fLocal);
            }
            
            for (int i = 0; i < fileNames.length; i++) {
                String fileName = fileNames[i];
                
                try {
                    
                    String remoteFileName;
                    if( remoteDirectory!=null && remoteDirectory.length()>0) {
                        remoteFileName = remoteDirectory + "/" + fileName;
                    } else {
                        remoteFileName = fileName;
                    }
                    
                    //Download the file from the server
                    logger.info("Downloading file: " + fileName);
                    ftpClient.get(localDirectory + "/" + fileName,remoteFileName);                
                    logger.info("File downloaded: " + fileName);
                    
                    //Save to the database
                    File scanFile = new File(localDirectory,fileName);
                    long length = scanFile.length();
                    FileInputStream fis = new FileInputStream(scanFile);
                    int readSize;
                    StringBuilder sb = new StringBuilder();
                    
                    do {
                        if (length > Integer.MAX_VALUE) {
                            readSize = Integer.MAX_VALUE;
                        } else {
                            readSize = (int)length;
                        }    
                        
                        // Create the byte array to hold the data
                        byte[] bytes = new byte[(int)length];
                    
                        // Read in the bytes
                        int offset = 0;
                        int numRead = 0;
                        while (offset < bytes.length && (numRead=fis.read(bytes, offset, bytes.length-offset)) >= 0) {
                            offset += numRead;
                        }
                    
                        // Ensure all the bytes have been read in
                        if (offset < bytes.length) {
                            throw new IOException("Could not completely read file "+fileName);
                        }
                        
                        //Add to the string builder
                        sb.append(new String(bytes));
    
                        length = length - offset;
                        
                    } while(length > 0);
                    fis.close();
                    
                    shipDAO.insertScanFile(connection,carrierId,fileName,"SP",sb.toString());
                    
                    //Send the JMS message to process
                    logger.debug("Sending JMS message to process file "+fileName);
//                    CommonUtils.sendJMSMessage(new InitialContext(),carrierId+" "+fileName,JMSPipeline.PROCESSSCANS);
                    CommonUtils.sendJMSMessage(connection,carrierId+" "+fileName,carrierId+" "+fileName,JMSPipeline.PROCESSSCANS);
                    logger.debug("Sent JMS message to process file "+fileName);
                    
                    //Delete the file off of the remote file system
                    logger.debug("Removing file "+remoteFileName+" from FTP server.");
                    if( deleteRemoteFile ) {
                        ftpClient.delete(remoteFileName);
                    }
                
                    //Move to archive directory
                    String archiveDirectory = getGlobalParameter(connection,ShipConstants.GLOBAL_PARMS_CONTEXT,PARAM_ARCHIVE_DIRECTORY,DEFAULT_ARCHIVE_ROOT)+"/"+carrierId+"/"+(new SimpleDateFormat(ARCHIVE_FORMAT)).format(new Date());
                        
                    //Check to see if the download directory exists
                    File archiveDir = new File(archiveDirectory);
                    if( !archiveDir.exists() ) {
                        logger.debug("Creating archive directory "+archiveDir.getCanonicalPath());
                        FileUtils.forceMkdir(archiveDir);
                    }
                    
                    logger.debug("Moving file "+fileName+" to archive "+archiveDirectory);
                    FileUtils.copyFileToDirectory(scanFile,archiveDir);
                    
                    logger.debug("Removing "+fileName+" from file system.");
                    FileUtils.forceDelete(scanFile);
                    
                } catch (IOException e) {
                    logger.error("Error while archiving scan file "+fileName+".  Will continue processing...",e);
                    hasErrors = true;
                }
            }
            
            logger.info("Download process completed.");
            
        } catch (Exception e) {
            throw new SDSApplicationException("Unable to ftp scan files for carrier "+carrierId,e);
        } finally {
            if ( hasErrors ) {
                throw new SDSApplicationException("Errors were found while FTPing scan files for carrier "+carrierId+".  See ship processing log for details.");
            }
        }
    }

    public void processScanFile(Connection connection, String carrierId, String fileName) throws SDSApplicationException {
        String localDirectory;
        try {
            localDirectory = getGlobalParameter(connection,ShipConstants.GLOBAL_PARMS_CONTEXT,PARAM_WORKING_DIRECTORY+carrierId,DEFAULT_WORKING_DIRECTORY);
        } catch (Exception e) {
            throw new SDSApplicationException("Error while trying to retreive local directory parameter",e);
        }
        
        processScanFile(connection,carrierId,fileName,localDirectory);
    }

    public void processScanFile(Connection connection, String carrierId, String fileName, String localDirectory) throws SDSApplicationException {
        
        String scanFormat;
        File scanFile = null;            
        boolean hasErrors = false;
        
        logger.debug("Processing file "+fileName+" for carrier "+carrierId);
        
        try {
            scanFormat = shipDAO.getCarrierScanFormat(connection,carrierId);
        } catch (Exception e) {
            throw new SDSApplicationException("Error while retrieving scan format for carrier "+carrierId,e);
        }
        
        if( StringUtils.isEmpty(scanFormat) ) {
            throw new SDSApplicationException("Scan processing is not available for carrier "+carrierId);
        }
        
        if( !StringUtils.equals(scanFormat,DHL_FORMAT) && !StringUtils.equals(scanFormat,FEDEX_FORMAT) && !StringUtils.equals(scanFormat,UPS_FORMAT) ) {
            throw new SDSApplicationException("Carrier "+carrierId+" has an unsupported scan format of "+scanFormat);
        }
        
        //Get the file from the database
        String fileContents = null;
        try {
            fileContents = shipDAO.getScanFile(connection,carrierId,fileName);
        } catch (Exception e) {
            throw new SDSApplicationException("Error retrieving file contents from database for "+carrierId+"/"+fileName,e);
        }
        
        if( StringUtils.isBlank(fileContents) ) {
            throw new SDSApplicationException("Empty file contents retrieved from database for "+carrierId+"/"+fileName+".  Check 'contents' clob in database.");
        }
        
        try {        
            //Write to the file system
            try {
                scanFile = new File(localDirectory+"/"+fileName);
                BufferedWriter out = new BufferedWriter(new FileWriter(scanFile));
                out.write(fileContents);
                out.close();
            } catch (Exception e) {
                throw new SDSApplicationException("Error writing "+fileName+" to "+localDirectory);
            }
            
            List<CarrierScanVO> scans = parseScanFile(fileName,localDirectory);
            
            for( int idx=0; idx<scans.size(); idx++ ) {
                try {
                    CarrierScanVO vo = scans.get(idx);
                    BigDecimal id = shipDAO.insertScan(connection,vo);
                    
                    logger.debug("Inserted scan id "+ String.valueOf(id.longValue())+" for order "+vo.getOrderId());
                    
                    enqueueJmsMessage(connection,JMSPipeline.PROCESSSCANS,String.valueOf(id.longValue()));
                } catch (Exception e) {
                    hasErrors = true;
                    logger.error(e);
                }
            }
        } finally {
            try {
                if( scanFile!=null && scanFile.exists() ) {
                    FileUtils.forceDelete(scanFile);
                }
            } catch (Exception e) {
                logger.warn("Failed to delete file "+scanFile.getName());
            }
        }
        
        if( hasErrors ) {
            throw new SDSApplicationException("Errors were found while processing scan file "+fileName+".  See logs for details.");
        }
    }
    
    public abstract List<CarrierScanVO> parseScanFile(String fileName, String localDirectory) throws SDSApplicationException;
    
    public void processScan(Connection connection, BigDecimal scanId) throws SDSApplicationException {
        try {
            logger.info("Processing scan id "+scanId);
            shipDAO.processScan(connection, scanId);
        } catch (SDSApplicationException se) {
            throw se;
        } catch (Exception e) {

            String message = e.getMessage();
            if( message!=null && message.startsWith("VENUS order record not found for scan id") ) {
                logger.warn(message);
            } else {
                throw new SDSApplicationException(e);
            }
        }
    }
}
