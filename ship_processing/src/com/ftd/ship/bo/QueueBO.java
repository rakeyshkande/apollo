package com.ftd.ship.bo;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.ship.common.ExternalSystemStatus;
import com.ftd.ship.common.MsgDirection;
import com.ftd.ship.common.MsgType;
import com.ftd.ship.common.ShipConstants;
import com.ftd.ship.common.VenusStatus;
import com.ftd.ship.dao.QueueDAO;
import com.ftd.ship.dao.ShipDAO;
import com.ftd.ship.vo.QueueVO;
import com.ftd.ship.vo.VenusMessageVO;

import java.sql.Connection;

import java.util.Date;


public class QueueBO 
{
    private static Logger logger  = new Logger("com.ftd.ship.dao.QueueBO");
    
    private ShipDAO shipDAO;    
    private QueueDAO queueDAO;
    private static final String QUEUE_TYPE  = "REJ";
    private static final String QUEUE_MESSAGE_TYPE  = "REJI";
    private static final String SYSTEM  = "Venus";
    private static final String QUEUE_REJ_MESSAGE  = "QUEUE_REJ_MESSAGE";
    
    public QueueBO()
    {
    }
    
   /**
    * Place a given order in the passed in customer service queue
    * @param connection database connection
    * @param venusId
    */
    public void sendRejectMessageToQueue(Connection connection, String venusId) throws Exception 
    {
        try 
        {
            // Load data to build venus reject message
            logger.debug("Loading venus message...");
            VenusMessageVO venusMessage = shipDAO.getVenusMessage(connection,venusId);
            //venusMessage.setMsgType(QUEUE_TYPE);
            venusMessage.setMsgType(MsgType.REJECT);
            venusMessage.setVenusStatus(VenusStatus.VERIFIED);
            venusMessage.setMessageDirection(MsgDirection.INBOUND);
            venusMessage.setExternalSystemStatus(ExternalSystemStatus.VERIFIED);
            venusMessage.setMessageText(ConfigurationUtil.getInstance().getProperty(ShipConstants.CONFIG_FILE, QUEUE_REJ_MESSAGE));
            venusMessage.setComments(ConfigurationUtil.getInstance().getProperty(ShipConstants.CONFIG_FILE, QUEUE_REJ_MESSAGE));
            
            // Insert venus reject message
            logger.debug("Inserting venus message...");
            shipDAO.insertVenusMessage(connection, venusMessage);
        
            // Load queue data out of database to build a queue item
            logger.debug("Loading queue item...");
            QueueVO queueItem = shipDAO.loadQueueItem(connection,venusId);
            queueItem.setQueueType(QUEUE_TYPE);
            queueItem.setMessageType(QUEUE_MESSAGE_TYPE);
            queueItem.setSystem(SYSTEM);
              
            // Insert queue item into table
            logger.debug("Saving queue item...");
            queueDAO.insertQueueRecord(connection, queueItem);
        } 
        catch (Exception e) 
        {
            // Exception on reject queue insert will cause a generic queue message
            this.sendUnassociatedMercuryMessageToQueue(connection, venusId);
            logger.error("Could not associate message to an order.  Unassociated Queue message inserted.", e);
        }
    }
    
    /** 
    * Used to deal with GEN messages and other messages for which there 
    * seems to be no associated order
    * 
    * @param venusId
    */
    private void sendUnassociatedMercuryMessageToQueue(Connection connection, String venusId) throws Exception 
    {
        // Create generic queue object
        QueueVO queueItem = new QueueVO();
        queueItem.setMessageTimestamp(new Date());
        queueItem.setQueueType(QUEUE_TYPE);
        queueItem.setMessageType(QUEUE_MESSAGE_TYPE);
        queueItem.setSystem(SYSTEM);
        queueItem.setMercuryId(venusId);
        
        // Insert into queue table
        queueDAO.insertQueueRecord(connection, queueItem);
    }

    public void setShipDAO(ShipDAO shipDAO) {
        this.shipDAO = shipDAO;
    }

    public ShipDAO getShipDAO() {
        return shipDAO;
    }

    public void setQueueDAO(QueueDAO queueDAO) {
        this.queueDAO = queueDAO;
    }

    public QueueDAO getQueueDAO() {
        return queueDAO;
    }
}
