package com.ftd.ship.bo;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.ship.bo.communications.sds.SDSApplicationException;
import com.ftd.ship.bo.communications.sds.SDSCommunications;
import com.ftd.ship.common.ExternalSystemStatus;
import com.ftd.ship.common.MsgDirection;
import com.ftd.ship.common.MsgType;
import com.ftd.ship.common.SDSConstants;
import com.ftd.ship.common.ShipConstants;
import com.ftd.ship.common.VenusStatus;
import com.ftd.ship.common.framework.util.CommonUtils;
import com.ftd.ship.vo.OrderDetailVO;
import com.ftd.ship.vo.SDSResponseVO;
import com.ftd.ship.vo.VenusMessageVO;

import java.sql.Connection;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

import org.apache.commons.lang.time.DateUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


public class SDSOutboundMsgProcessingBO extends SDSProcessingBO {
    private static final Logger LOGGER = new Logger("com.ftd.ship.bo.SDSOutboundMsgProcessingBO");
    
    public SDSOutboundMsgProcessingBO() {
    }
    
    public void processCancelRequest(Connection connection, VenusMessageVO venusMessageVO) throws Exception {
        Document doc = buildCancelRequest(venusMessageVO);
//        SDSCommunications comm = new SDSCommunications();
        LOGGER.debug(JAXPUtil.toString(doc));
        doc = sdsCommunications.cancelShipmentXML(connection, doc);
        LOGGER.debug(JAXPUtil.toString(doc));
        
        SDSResponseVO responseVO = parseCancelResponse(doc);
        
        //Set the status on the original order and mark cancelled
        VenusMessageVO assocOrderVO = getAssociatedOrder(connection,venusMessageVO.getVenusOrderNumber());

        if( responseVO.isSuccess() ) {
            assocOrderVO.setCancelledStatusDate(new java.util.Date());
            shipDAO.markVenusMsgCancelled(connection,assocOrderVO);
                    
            //Mark the can as being successful
            venusMessageVO.setVenusStatus(VenusStatus.VERIFIED);
            venusMessageVO.setExternalSystemStatus(ExternalSystemStatus.NOTVERIFIED); 
            shipDAO.updateVenusSDS(connection,venusMessageVO);
            LOGGER.info("Successfully cancelled tracking number for "+venusMessageVO.getVenusOrderNumber());
            
            OrderDetailVO orderDetailVO = orderDAO.getOrderDetail(connection, venusMessageVO.getReferenceNumber());
            if (orderDetailVO == null)
            {
                throw new SDSApplicationException("No order detail record for the given order detail id: "+venusMessageVO.getReferenceNumber());
            }
            
            if (orderDetailVO.getSubcode() != null && orderDetailVO.getSubcode().length() > 0) {
                shipDAO.incrementProductInventory( connection,
                                             orderDetailVO.getSubcode(), 
                                             assocOrderVO.getVendorId(), 
                                             1);
            } else {
                shipDAO.incrementProductInventory( connection,
                                             orderDetailVO.getProductId(), 
                                             assocOrderVO.getVendorId(), 
                                             1);
            }
            
            // Defect 2619 - Inventory Tracking. Increment CAN message count against assigend product inventory.
            // Please note, orders cancelled after being printed won't be added back.  This is true for
            //regular vendor delivered orders as well as Zone Jump orders.
            Calendar cTodaysDate = Calendar.getInstance();
            //set today's date time to 00:00:00 too
            cTodaysDate.set(Calendar.HOUR, 0); 
            cTodaysDate.set(Calendar.MINUTE, 0); 
            cTodaysDate.set(Calendar.SECOND, 0); 
            cTodaysDate.set(Calendar.MILLISECOND, 0);
            cTodaysDate.set( Calendar.AM_PM, Calendar.AM );
             
            Calendar shipDateCalendar = Calendar.getInstance();
            shipDateCalendar.setTime(assocOrderVO.getShipDate());
             
            //Create a Calendar Object
            Calendar zjDate = Calendar.getInstance();
            //Define the format
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            String zjLabelDate = null;
            
            if(assocOrderVO.getZoneJumpLabelDate() != null)
                zjLabelDate = assocOrderVO.getZoneJumpLabelDate().toString();
            if(zjLabelDate != null)
            {
                zjLabelDate = zjLabelDate.substring(5,7) + "/" +zjLabelDate.substring(8,10) + "/" + zjLabelDate.substring(0,4);      
                zjDate.setTime(sdf.parse(zjLabelDate));
            }
            boolean cancelOrder = false;
            if(assocOrderVO.isZoneJumpFlag() 
               && !assocOrderVO.getSdsStatus().equalsIgnoreCase(ShipConstants.PRINTED_DISPOSITION)
               && (getDiffInDays(cTodaysDate.getTime(), zjDate.getTime()) >= 0)) 
               
            {
                cancelOrder = true;
            }
            //only update inventory counts if non Zone Jump order is cancelled before printed
            //status and it's on or before planned ship date
            if (assocOrderVO.getPrintedStatusDate() == null && getDiffInDays(cTodaysDate.getTime(), shipDateCalendar.getTime()) >= 0)
            {
                cancelOrder = true;
            }
            if(cancelOrder)
            {
                if(venusMessageVO.getInvTrkCountDate() == null) {
                  try {
                       shipDAO.updateInvTrkCount(connection, venusMessageVO);
                   } catch (Exception e) {
                       LOGGER.error(e);
                       CommonUtils.getInstance().sendSystemMessage("Product inventory deduction failed for venus id:" + 
                                   venusMessageVO.getVenusId() + ".\nReason:" + e.getMessage());
                   }
                }
                if(venusMessageVO.getInvTrkAssignedCountDate() == null) {
                   try {
                       shipDAO.updateInvTrkAssignedCount(connection, venusMessageVO);
                   } catch (Exception e) {
                       LOGGER.error(e);
                       CommonUtils.getInstance().sendSystemMessage("Product vendor inventory deduction failed for venus id:" + 
                                   venusMessageVO.getVenusId() + ".\nReason:" + e.getMessage());
                   }
                }
            }            
        } else if (StringUtils.equals(responseVO.getStatusCode(),SDSConstants.STATUS_CANCEL_ERROR_ALREADY_CANCELLED)) {
//            assocOrderVO.setCancelledStatusDate(new java.util.Date());
            assocOrderVO.setVenusStatus(VenusStatus.VERIFIED);
            assocOrderVO.setExternalSystemStatus(ExternalSystemStatus.VERIFIED);
            shipDAO.updateVenusSDS(connection,assocOrderVO);
            
            //Create order comment
            String comment = "Cancel failed because tracking number for "+venusMessageVO.getVenusOrderNumber()+" because it was already cancelled.";
            OrderDetailVO orderDetailVO = orderDAO.getOrderDetail(connection,assocOrderVO.getReferenceNumber());
            createComment(connection,comment,orderDetailVO);
                    
            //Mark the can as failing
            venusMessageVO.setVenusStatus(VenusStatus.ERROR);
            venusMessageVO.setExternalSystemStatus(ExternalSystemStatus.VERIFIED);
            venusMessageVO.setMessageText(responseVO.getMsgText());
            venusMessageVO.setErrorDescription(responseVO.getStatusCode());
            shipDAO.updateVenusSDS(connection,venusMessageVO);
            LOGGER.info(comment);
            
        } else if (StringUtils.equals(responseVO.getStatusCode(),SDSConstants.STATUS_CANCEL_ERROR_TOO_LATE)) {
            //Create order comment
            String comment = "This shipment cannot be cancelled because it has already been shipped.";
            OrderDetailVO orderDetailVO = orderDAO.getOrderDetail(connection,assocOrderVO.getReferenceNumber());
            createComment(connection,comment,orderDetailVO);
                    
            //Mark the can as failing
            venusMessageVO.setVenusStatus(VenusStatus.ERROR);
            venusMessageVO.setExternalSystemStatus(ExternalSystemStatus.VERIFIED);
            venusMessageVO.setMessageText(responseVO.getMsgText());
            venusMessageVO.setErrorDescription(responseVO.getStatusCode());
            shipDAO.updateVenusSDS(connection,venusMessageVO);
            LOGGER.info(comment);
            
            // Oct. 10, 2006
            //Per A. Brandimore, create a reject message and queue it.
            VenusMessageVO rejectMsg = CommonUtils.copyVenusMessage(venusMessageVO,MsgType.REJECT);
            rejectMsg.setOperator(ShipConstants.SDS_USER);
            rejectMsg.setMessageDirection(MsgDirection.INBOUND);
            rejectMsg.setVenusStatus(VenusStatus.ERROR);
            rejectMsg.setExternalSystemStatus(ExternalSystemStatus.VERIFIED);
            rejectMsg.setTransmissionTime(new Date());
            rejectMsg.setMessageText(comment);
            rejectMsg.setComments(comment);
            String rejectId = shipDAO.insertVenusMessage(connection,rejectMsg);
            rejectMsg.setVenusId(rejectId);
            
            //queue it
            sendToQueue(connection,rejectMsg,orderDetailVO);
            
        } else {
            StringBuilder errString = new StringBuilder();
            errString.append("Error processing cancel request\r\n");
            errString.append("Error code: ");
            errString.append(responseVO.getStatusCode());
            errString.append("\r\n");
            errString.append("Error message: ");
            errString.append(responseVO.getMsgText());
            
            //Create order comment
            String comment = "Cancel tracking number failed for "+venusMessageVO.getVenusOrderNumber()+": "+responseVO.getMsgText();
            OrderDetailVO orderDetailVO = orderDAO.getOrderDetail(connection,assocOrderVO.getReferenceNumber());
            createComment(connection,comment,orderDetailVO);
                    
            //Mark the can as failing
            venusMessageVO.setVenusStatus(VenusStatus.ERROR);
            venusMessageVO.setExternalSystemStatus(ExternalSystemStatus.VERIFIED);
            venusMessageVO.setMessageText(responseVO.getMsgText());
            venusMessageVO.setErrorDescription(responseVO.getStatusCode());
            shipDAO.updateVenusSDS(connection,venusMessageVO);
            LOGGER.info(comment);
            
            // Oct. 10, 2006
            //Per A. Brandimore, create a reject message and queue it.
            VenusMessageVO rejectMsg = CommonUtils.copyVenusMessage(venusMessageVO,MsgType.REJECT);
            rejectMsg.setOperator(ShipConstants.SDS_USER);
            rejectMsg.setMessageDirection(MsgDirection.INBOUND);
            rejectMsg.setVenusStatus(VenusStatus.ERROR);
            rejectMsg.setExternalSystemStatus(ExternalSystemStatus.VERIFIED);
            rejectMsg.setTransmissionTime(new Date());
            rejectMsg.setMessageText(comment);
            rejectMsg.setComments(comment);
            String rejectId = shipDAO.insertVenusMessage(connection,rejectMsg);
            rejectMsg.setVenusId(rejectId);
            
            //queue it
            sendToQueue(connection,rejectMsg,orderDetailVO);
                        
            //Send out system notification 
            try {
                LOGGER.error(errString.toString());
                CommonUtils.getInstance().sendSystemMessage(errString.toString());
            } catch (Exception e) {
                LOGGER.error("Unable to send message to support pager.");
                LOGGER.fatal(e);
            }
        }
    }
    
    public Document buildCancelRequest(VenusMessageVO venusMessageVO) throws Exception {
         Document doc = JAXPUtil.createDocument();
         Element root = doc.createElement(SDSConstants.TAG_CANCEL_ROOT);
         root.setAttribute("xmlns",SDSConstants.TAG_CANCEL_ROOT_NS_URI);
         doc.appendChild(root);
         
         Element request = doc.createElement(SDSConstants.TAG_CANCEL_REQUEST);
         root.appendChild(request);
         
         Element carton = JAXPUtil.buildSimpleXmlNode(doc,SDSConstants.TAG_CANCEL_CARTON_NUMBER,venusMessageVO.getVenusOrderNumber());
         request.appendChild(carton);
         
         Element reason = JAXPUtil.buildSimpleXmlNode(doc,SDSConstants.TAG_CANCEL_REASON_CODE,venusMessageVO.getCancelReasonCode());
         request.appendChild(reason);
         
         return doc;
    }
    
    public SDSResponseVO parseCancelResponse(Document responseDoc) throws Exception {
        SDSResponseVO responseVO = new SDSResponseVO();
        
        String statusCode;
        try {
            statusCode = JAXPUtil.selectSingleNodeText(responseDoc,SDSConstants.XPATH_CANCEL_STATUS_CODE_XPATH,SDSConstants.XPATH_CANCEL_NAMESPACE_PREFIX,SDSConstants.XPATH_CANCEL_NAMESPACE_URI);
        } catch (Exception e) {
            throw new SDSApplicationException("Failed to retrieve status code in cancel response.",e);
        }
        
        if( StringUtils.isBlank(statusCode) ) {
            throw new SDSApplicationException("No status code found in cancel response.");
        }
        responseVO.setStatusCode(statusCode);

        String statusDesc;
        try { 
            statusDesc = JAXPUtil.selectSingleNodeText(responseDoc,SDSConstants.XPATH_CANCEL_STATUS_DESC_XPATH,SDSConstants.XPATH_CANCEL_NAMESPACE_PREFIX,SDSConstants.XPATH_CANCEL_NAMESPACE_URI);
            responseVO.setMsgText(statusDesc);
        } catch (Exception e) {
            LOGGER.info("Failed to retrieve status description in cancel response.",e);
        }
        
        responseVO.setSuccess(statusCode.equals(SDSConstants.STATUS_OK));
        
        return responseVO;
    }
    
    /**
     * Returns the difference in days between the startDate and endDate.
     * 
     * @param startDate
     * @param endDate
     * @return The difference in days between the startDate and endDate
     * @throws IllegalArgumentException if either startDate or endDate are null
     */
    public int getDiffInDays(Date startDate, Date endDate) {

      // null check inputs
      if ( startDate == null || endDate == null ) {
        throw new IllegalArgumentException("Invalid parameter: one or both of startDate='" + startDate + "', endDate='" + endDate + "' were null.");
      }

      long diffInMillis = endDate.getTime() - startDate.getTime();

      int diffInDays = (int)(diffInMillis / DateUtils.MILLIS_IN_DAY);
      return diffInDays;
    }

}
