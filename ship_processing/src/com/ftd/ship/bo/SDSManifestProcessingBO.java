package com.ftd.ship.bo;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.ship.bo.communications.sds.SDSApplicationException;
import com.ftd.ship.common.SDSConstants;
import com.ftd.ship.vo.ManifestVO;
import com.ftd.ship.vo.SDSResponseVO;

import java.sql.Connection;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


public class SDSManifestProcessingBO extends SDSProcessingBO {
    private static final Logger LOGGER = new Logger("com.ftd.ship.bo.SDSManifestProcessingBO");
    private static final Pattern CODE_PATTERN = Pattern.compile("\\d\\d-\\d\\d\\d\\d[A-Z][A-Z]");
    private static final int VENDOR_CODE_LENGTH = 9;
    private static final Pattern ID_PATTERN = Pattern.compile("\\d\\d\\d\\d\\d");
    private static final int VENDOR_ID_LENGTH = 5;
    private static final String ZONE_JUMP_PATTERN = "ZJ";
    
    public SDSManifestProcessingBO() {
    }
             
    public void processRequest(Connection connection, String arg) throws Exception {
        LOGGER.debug("Entering processRequest");

        //What type of request is this
        String vendorId = null;
        String vendorCode = null;
        String carrierId = null;
        String vendor = null;
        boolean isZoneJump = false;
        String daysInTransitQty = null; 
        try {
            arg = StringUtils.upperCase(StringUtils.trimToEmpty(arg));
            
            if( StringUtils.isBlank(arg) ) {
                vendorId = null;
                vendorCode = null;
                carrierId = null;
            } else {
                // If this is for ZoneJump then argument string will end in ZoneJump characters
                if ((arg.substring(0,(arg.length() - 1))).endsWith(ZONE_JUMP_PATTERN)) {
                    daysInTransitQty = arg.substring(arg.length() - 1, arg.length()); 
                    arg = arg.substring(0,(arg.length() - 1));
                    arg = StringUtils.trimToEmpty( arg.substring(0, (arg.length() - ZONE_JUMP_PATTERN.length())) );
                    isZoneJump = true;
                    LOGGER.info("Manifest processing for ZoneJump");
                }
                
                //Test to see if you got an id
                String testStr = StringUtils.substring(arg,0,VENDOR_ID_LENGTH);
                Matcher m = ID_PATTERN.matcher(testStr);
                
                if( m.matches() ) {
                    vendorId = testStr;    
                    vendorCode = null;
                } else {
                    //Test for a vendor (florist) code
                    testStr = StringUtils.substring(arg,0,VENDOR_CODE_LENGTH);
                    m = CODE_PATTERN.matcher(testStr);
                    
                    if( m.matches() ) {
                        vendorId = null;
                        vendorCode = testStr;
                    } else {
                        //It's not a vendor id or a vendor code so assume that it's a carrier id
                        carrierId = arg;
                        vendorCode = null;
                        vendorId = null;
                    }
                }
                    
                //Check to see if we got a vendor code/id and a carrier id
                if( vendorCode !=null && arg.length()>VENDOR_CODE_LENGTH ) {
                    carrierId = arg.substring(VENDOR_CODE_LENGTH);
                } else if( vendorId !=null && arg.length()>VENDOR_ID_LENGTH ) {
                    carrierId = arg.substring(VENDOR_ID_LENGTH);
                }
            }
        } catch (Throwable t) {
            if (isZoneJump) {
                // Go no further if ZoneJump
                throw new SDSApplicationException("Error during ZoneJump Manifest processing: " + t);
            }
            LOGGER.error("Error while parsing passed arguement of "+arg+".  Will process with default values.",t);
            vendorId = null;
            vendorCode = null;
            carrierId = null;
        }

        // ZoneJump only allowed for carrierId
        if (isZoneJump && (carrierId == null || vendorId != null || vendorCode != null)  ) {
            throw new SDSApplicationException("Invalid Manifest arguments for ZoneJump: " + arg);
        }
        
        if( vendorId!=null ) {
            vendor = vendorId;
        } else if( vendorCode!=null ) {
            vendor = vendorCode;
        }
        
        try {
            if( LOGGER.isDebugEnabled() ) {
                StringBuilder sb = new StringBuilder();
                sb.append("Vendor: ");
                sb.append(vendor);
                sb.append(" and Carrier: ");
                sb.append(carrierId);
                sb.append(" and ZoneJump flag: ");
                sb.append(isZoneJump?"Y":"N");
                sb.append(" and daysInTransit: ");
                sb.append(daysInTransitQty);
                LOGGER.debug(sb.toString());
            }
            //SDSCommunications comm = new SDSCommunications();

            List<ManifestVO> manifestList = shipDAO.getVendorsToClose(connection, vendor, carrierId, isZoneJump, daysInTransitQty);
            StringBuffer errString = new StringBuffer();
            
            LOGGER.debug("Manifest query returned : " + manifestList.size() + " records.");
            
            for( int idx=0; idx<manifestList.size(); idx++ ) {
                
                ManifestVO manifestVO = manifestList.get(idx);
                Document doc;        
                SDSResponseVO responseVO = null; 
                
                try {

                    if (isZoneJump)
                        {
                          doc = buildRequestZJ(manifestVO.getCarrierId(),manifestVO.getVendorCode(), manifestVO.getZoneJumpTrailerNumber());
                          LOGGER.debug("Close Request for ZoneJump sent to ScanData is : " + JAXPUtil.toString(doc));
                          doc = sdsCommunications.closeTrailerXML(connection, doc);        
                          LOGGER.debug("Close Response for ZoneJump received from ScanData is : " + JAXPUtil.toString(doc));
                        }
                        else
                        {
                          doc = buildRequest(manifestVO.getCarrierId(),manifestVO.getVendorCode());
                          LOGGER.debug("Close Request sent to ScanData is : " + JAXPUtil.toString(doc));
                          doc = sdsCommunications.closeXML(connection, doc);        
                          LOGGER.debug("Close Response received from ScanData is : " + JAXPUtil.toString(doc));
                        } 
                        
                        if (isZoneJump)
                          responseVO = parseResponseZJ(doc);        
                        else
                          responseVO = parseResponse(doc);        
                        
                        LOGGER.debug("ResponseVO is : " + responseVO.toString());
                        LOGGER.debug("ResponseVO - status code is : " + responseVO.getStatusCode());
                        
                        if( responseVO.isSuccess() ) 
                        {
                            if (isZoneJump)
                              shipDAO.updateTripStatus(connection, manifestVO.getZoneJumpTrailerNumber(), SDSConstants.ZJ_STATUS_TRIP_CLOSED);
                        
                            LOGGER.info("Successfully closed "+manifestVO.getCarrierId()+" for "+manifestVO.getVendorCode() + " and trailer = " + manifestVO.getZoneJumpTrailerNumber());
                         
                        } else {
                            errString.append("Failed to close ");
                            errString.append(manifestVO.getCarrierId());
                            errString.append("\r\n");
                            errString.append("Error code: ");
                            errString.append(responseVO.getStatusCode());
                            errString.append("\r\n");
                            errString.append("Error message: ");
                            errString.append(responseVO.getMsgText());
                            errString.append("\r\n");
                            LOGGER.info("Failed to close - SDSApplicationException will be thrown after processing remaining vendors.  Error message was: " + responseVO.getMsgText());
                        }
                }
            
                catch (Throwable t) {
                    
                    errString.append("Caught an exception ");
                    errString.append("\r\n");
                    errString.append(t.getMessage());
                    errString.append("Manifest VO Carrier ID:");
                    errString.append("\r\n");
                    errString.append(manifestVO.getCarrierId());
                    errString.append("\r\n");
                    if (responseVO != null) {
                        errString.append("Response VO Status code: ");
                        errString.append(responseVO.getStatusCode());
                        errString.append("\r\n");
                        errString.append("Response VO Error message: ");
                        errString.append(responseVO.getMsgText());
                        errString.append("\r\n");
                    }
                    LOGGER.error("Error while processing manifest: " + idx + "\n");
                }
                
            }
            
            if( errString.length()>0 ) {
                throw new SDSApplicationException("Errors found while processing SDS close:\r\n"+errString.toString().trim());
            }
        
        } finally {
            LOGGER.debug("Leaving processRequest");
        }
    }
     
    public Document buildRequest(String carrierId, String locationId) throws Exception {
        
        Document doc = JAXPUtil.createDocument();
        Element root = doc.createElementNS(SDSConstants.TAG_MANIFEST_ROOT_NS_URI,SDSConstants.TAG_MANIFEST_ROOT);
        root.setPrefix("sd");
        doc.appendChild(root);
        
        Element request = doc.createElementNS(SDSConstants.TAG_MANIFEST_ROOT_NS_URI,SDSConstants.TAG_MANIFEST_REQUEST);
        root.appendChild(request);
        
        Element element = doc.createElementNS(SDSConstants.TAG_MANIFEST_ROOT_NS_URI,SDSConstants.TAG_MANIFEST_CARRIER);
        element.appendChild(doc.createTextNode(carrierId));
        request.appendChild(element);
        
        element = doc.createElementNS(SDSConstants.TAG_MANIFEST_ROOT_NS_URI,SDSConstants.TAG_MANIFEST_LOCATION);
        element.appendChild(doc.createTextNode(locationId));
        request.appendChild(element);
        
        return doc;
     }
          
    public Document buildRequestZJ(String carrierId, String locationId, String zoneJumpTrailer) throws Exception {
        
        Document doc = JAXPUtil.createDocument();
        Element root = doc.createElementNS(SDSConstants.TAG_MANIFEST_ROOT_NS_URI_ZJ,SDSConstants.TAG_MANIFEST_ROOT_ZJ);
        root.setPrefix("sd");
        doc.appendChild(root);
        
        Element request = doc.createElementNS(SDSConstants.TAG_MANIFEST_ROOT_NS_URI_ZJ,SDSConstants.TAG_MANIFEST_REQUEST);
        root.appendChild(request);
        
        Element element = doc.createElementNS(SDSConstants.TAG_MANIFEST_ROOT_NS_URI_ZJ,SDSConstants.TAG_MANIFEST_CARRIER);
        element.appendChild(doc.createTextNode(carrierId));
        request.appendChild(element);
        
        element = doc.createElementNS(SDSConstants.TAG_MANIFEST_ROOT_NS_URI_ZJ,SDSConstants.TAG_MANIFEST_LOCATION);
        element.appendChild(doc.createTextNode(locationId));
        request.appendChild(element);
        
        element = doc.createElementNS(SDSConstants.TAG_MANIFEST_ROOT_NS_URI_ZJ, SDSConstants.TAG_MANIFEST_ZONE_JUMP_TRAILERNUMBER);
        element.appendChild(doc.createTextNode(zoneJumpTrailer));
        request.appendChild(element);
        
        return doc;
     }
     


     public SDSResponseVO parseResponse(Document responseDoc) throws Exception {
         SDSResponseVO responseVO = new SDSResponseVO();
        
        String statusCode;
        try {
            statusCode = JAXPUtil.selectSingleNodeText(responseDoc,SDSConstants.XPATH_MANIFEST_STATUS_CODE_XPATH,SDSConstants.XPATH_MANIFEST_NAMESPACE_PREFIX,SDSConstants.XPATH_MANIFEST_NAMESPACE_URI);
        } catch (Exception e) {
            throw new SDSApplicationException("Failed to retrieve status code in cancel response.",e);
        }
        
        if( StringUtils.isBlank(statusCode) ) {
            throw new SDSApplicationException("No status code found in cancel response.");
        }
        responseVO.setStatusCode(statusCode);

        String statusDesc;
        try { 
            statusDesc = JAXPUtil.selectSingleNodeText(responseDoc,SDSConstants.XPATH_MANIFEST_STATUS_DESC_XPATH,SDSConstants.XPATH_MANIFEST_NAMESPACE_PREFIX,SDSConstants.XPATH_MANIFEST_NAMESPACE_URI);
            responseVO.setMsgText(statusDesc);
        } catch (Exception e) {
            LOGGER.info("Failed to retrieve status description in cancel response.",e);
        }
        
        if( StringUtils.equals(statusCode,SDSConstants.STATUS_OK) || StringUtils.equals(statusCode,SDSConstants.STATUS_MANIFEST_NOTHING_TO_DO)) {
            responseVO.setSuccess(true);
        } else {
            responseVO.setSuccess(false);
        }
        //responseVO.setSuccess(statusCode.equals(SDSConstants.STATUS_OK)||statusCode.equals(SDSConstants.STATUS_MANIFEST_NOTHING_TO_DO));
        
        return responseVO;
     }

     public SDSResponseVO parseResponseZJ(Document responseDoc) throws Exception {
         SDSResponseVO responseVO = new SDSResponseVO();
        
        String statusCode;
        try {
            statusCode = JAXPUtil.selectSingleNodeText(responseDoc,SDSConstants.XPATH_MANIFEST_STATUS_CODE_XPATH_ZJ,SDSConstants.XPATH_MANIFEST_NAMESPACE_PREFIX,SDSConstants.XPATH_MANIFEST_NAMESPACE_URI_ZJ);
        } catch (Exception e) {
            throw new SDSApplicationException("Failed to retrieve status code in cancel response.",e);
        }
        
        if( StringUtils.isBlank(statusCode) ) {
            throw new SDSApplicationException("No status code found in cancel response.");
        }
        responseVO.setStatusCode(statusCode);

        String statusDesc;
        try { 
            statusDesc = JAXPUtil.selectSingleNodeText(responseDoc,SDSConstants.XPATH_MANIFEST_STATUS_DESC_XPATH_ZJ,SDSConstants.XPATH_MANIFEST_NAMESPACE_PREFIX,SDSConstants.XPATH_MANIFEST_NAMESPACE_URI_ZJ);
            responseVO.setMsgText(statusDesc);
        } catch (Exception e) {
            LOGGER.info("Failed to retrieve status description in cancel response.",e);
        }
        
        if( StringUtils.equals(statusCode,SDSConstants.STATUS_OK) || StringUtils.equals(statusCode,SDSConstants.STATUS_MANIFEST_NOTHING_TO_DO)) {
            responseVO.setSuccess(true);
        } else {
            responseVO.setSuccess(false);
        }
        //responseVO.setSuccess(statusCode.equals(SDSConstants.STATUS_OK)||statusCode.equals(SDSConstants.STATUS_MANIFEST_NOTHING_TO_DO));
        
        return responseVO;
     }



}
