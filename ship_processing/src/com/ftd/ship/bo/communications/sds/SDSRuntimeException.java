package com.ftd.ship.bo.communications.sds;

public class SDSRuntimeException extends RuntimeException {
    public SDSRuntimeException(Throwable t) {
        super(t);
    }
    public SDSRuntimeException(String message) {
        super(message);
    }
    public SDSRuntimeException(String message, Throwable t) {
        super(message,t);
    }
}
