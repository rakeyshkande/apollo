package com.ftd.ship.bo.communications.sds;

import com.ScanData.Comm.WTMServices.GetOrderStatusOrderStatusParams;
import com.ScanData.Comm.WTMServices.GetOrderStatusResponseGetOrderStatusResult;
import com.ScanData.Comm.WTMServices.GetPLDUpdateResponseGetPLDUpdateResult;
import com.ScanData.Comm.WTMServices.WTMServiceLocator;
import com.ScanData.Comm.WTMServices.WTMServiceSoap_BindingStub;
import com.ScanData.Comm.WebServices.CancelShipmentXMLResponseCancelShipmentXMLResult;
import com.ScanData.Comm.WebServices.CancelShipmentXMLXdReqCancel;
import com.ScanData.Comm.WebServices.CloseTrailerXMLResponseCloseTrailerXMLResult;
import com.ScanData.Comm.WebServices.CloseTrailerXMLXdReqCloseTrailer;
import com.ScanData.Comm.WebServices.CloseXMLResponseCloseXMLResult;
import com.ScanData.Comm.WebServices.CloseXMLXdReqClose;
import com.ScanData.Comm.WebServices.CreateShipmentXMLResponseCreateShipmentXMLResult;
import com.ScanData.Comm.WebServices.CreateShipmentXMLXdICSU;
import com.ScanData.Comm.WebServices.GetShipmentInfoXMLResponseGetShipmentInfoXMLResult;
import com.ScanData.Comm.WebServices.GetShipmentInfoXMLXdReqInfo;
import com.ScanData.Comm.WebServices.RateShipmentXMLResponseRateShipmentXMLResult;
import com.ScanData.Comm.WebServices.RateShipmentXMLXdICSU;
import com.ScanData.Comm.WebServices.VSAMSLocator;
import com.ScanData.Comm.WebServices.VSAMSSoap12Stub;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.JAXPException;
import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.ship.common.ShipConstants;
import com.ftd.ship.common.framework.util.CommonUtils;
import com.ftd.ship.common.resources.ResourceProviderBase;
import com.ftd.ship.common.resources.SDSStatusParm;
import com.ftd.ship.vo.GlobalParameterVO;

import java.net.MalformedURLException;
import java.net.URL;

import java.rmi.RemoteException;

import java.sql.Connection;

import javax.xml.rpc.ServiceException;

import org.apache.axis.message.MessageElement;
import org.apache.commons.lang.StringUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


/**
 * Public API to the ScanData web service
 */
public class SDSCommunications {
    private static Logger logger = 
        new Logger("com.ftd.ship.bo.communications.SDSCommunications");
    private ResourceProviderBase resourceProvider;
    private static final int DEFAULT_TIMEOUT = 900; //fifteen minutes

    private static final String SHIP_PROCESSING_CONFIG_CONTEXT = "SHIP_PROCESSING_CONFIG";
    private static final String SDS_URL = "SDS_URL";
    private static final String WTM_URL = "WTM_URL";
    private static final String SDS_TIMEOUT_SECONDS = "SDS_TIMEOUT_SECONDS";
    private String sdsUrl = null;    
    private String wtmUrl = null;

    /**
     * Default constructor
     * @throws Exception
     */
    public SDSCommunications() {
    }
    
    /**
     * Sends a rate shipment request for the passed in parameters
     * @param connection database connection
     * @param rateDocument contains the close parameters
     * @return
     * @throws SDSCommunicationsException
     * @throws SDSApplicationException
     */
    public Document rateShipmentXML(Connection connection, Document rateDocument) throws SDSCommunicationsException, SDSApplicationException {
                
        logger.debug("Entering rateShipmentXML");
        Document responseDoc = null;
        
        try {
            VSAMSSoap12Stub binding;
            try {
                URL url = new URL(getSdsUrl());
                binding = (VSAMSSoap12Stub)new VSAMSLocator().getVSAMSSoap12(url);
            } catch (MalformedURLException mue) {
                logger.error("Error setting SDS url",mue);
                throw new SDSCommunicationsException("Error setting SDS url",mue);
            } catch (ServiceException jre) {
                if (jre.getLinkedCause() != null)
                    logger.error(jre.getLinkedCause());
                throw new SDSCommunicationsException("JAX-RPC ServiceException caught: " + jre);
            }
            
            // Time out value
             binding.setTimeout(getSDSTimeout(connection));
            
             MessageElement[] msgs = new MessageElement[1];
             msgs[0] = new MessageElement((Element)rateDocument.getFirstChild());  
             RateShipmentXMLXdICSU csu = new RateShipmentXMLXdICSU(msgs);
            
            // Send Document
            try {
                logger.debug("Sending rate shipment request to "+getSdsUrl());            
                RateShipmentXMLResponseRateShipmentXMLResult x = binding.rateShipment(csu);
                MessageElement[] responses = x.get_any();
                responseDoc = responses[0].getAsDocument();
            } 
            catch (RemoteException re) {
                logger.error(re);
                throw new SDSCommunicationsException("rateShipment exception caught: " + re);
            } 
            catch (Exception e) {
                logger.error(e);
                throw new SDSApplicationException("Error retrieving results from SDS server",e);
            }
        } finally {
            logger.debug("Leaving rateShipmentXML");
        }
        
        return responseDoc;
    }    
    
    /**
     * Sends a create shipment request for the passed in parameters
     * @param connection database connection
     * @param shipDocument contains the close parameters
     * @return
     * @throws SDSCommunicationsException
     * @throws SDSApplicationException
     */
    public Document createShipment(Connection connection,  String shipDocument) throws SDSCommunicationsException, SDSApplicationException {
        
        logger.debug("Entering createShipment");
        VSAMSSoap12Stub binding;
        try {
            URL url = new URL(getSdsUrl());
            binding = (VSAMSSoap12Stub)new VSAMSLocator().getVSAMSSoap12(url);
        } catch (MalformedURLException mue) {
            logger.error("Error setting SDS url",mue);
            throw new SDSCommunicationsException("Error setting SDS url",mue);
        } catch (ServiceException jre) {
            if (jre.getLinkedCause() != null)
                logger.error(jre.getLinkedCause());
            throw new SDSCommunicationsException("JAX-RPC ServiceException caught: " + jre);
        }

        // Time out value
        binding.setTimeout(this.getSDSTimeout(connection));

        // Send Document
         String strResponse;
        try {
            strResponse = binding.createShipment(shipDocument);
        } 
        catch (RemoteException re) {
            logger.error(re);
            throw new SDSCommunicationsException("createShipment exception caught: " + re);
        } 
        catch (Exception e) {
            logger.error(e);
            throw new SDSApplicationException("Error retrieving results from SDS server",e);
        }
         
         Document responseDoc = null;
         try {
            responseDoc = JAXPUtil.parseDocument(strResponse,false,false);
         } catch (JAXPException e) {
            logger.error(e);
            throw new SDSApplicationException("Error while parsing response document from SDS server.",e);
         }
         
        logger.debug("Leaving createShipment");
        
        return responseDoc;
    }
    
    /**
     * Sends a create shipment request for the passed in parameters
     * @param connection database connection
     * @param shipDocument contains the close parameters
     * @return
     * @throws SDSCommunicationsException
     * @throws SDSApplicationException
     */
    public Document createShipmentXML(Connection connection, Document shipDocument) throws SDSCommunicationsException, SDSApplicationException {
                
        logger.debug("Entering createShipmentXML");
        Document responseDoc = null;
        
        try {
            VSAMSSoap12Stub binding;
            try {
                URL url = new URL(getSdsUrl());
                binding = (VSAMSSoap12Stub)new VSAMSLocator().getVSAMSSoap12(url);
            } catch (MalformedURLException mue) {
                logger.error("Error setting SDS url",mue);
                throw new SDSCommunicationsException("Error setting SDS url",mue);
            } catch (ServiceException jre) {
                if (jre.getLinkedCause() != null)
                    logger.error(jre.getLinkedCause());
                throw new SDSCommunicationsException("JAX-RPC ServiceException caught: " + jre);
            }
            
            // Time out value
             binding.setTimeout(getSDSTimeout(connection));
            
             MessageElement[] msgs = new MessageElement[1];
             msgs[0] = new MessageElement((Element)shipDocument.getFirstChild());  
             CreateShipmentXMLXdICSU csu = new CreateShipmentXMLXdICSU(msgs);
            
            // Send Document
            try {
                logger.debug("Sending shipment request to "+getSdsUrl());            
                CreateShipmentXMLResponseCreateShipmentXMLResult x = binding.createShipment(csu);
                MessageElement[] responses = x.get_any();
                responseDoc = responses[0].getAsDocument();
            } 
            catch (RemoteException re) {
                logger.error(re);
                throw new SDSCommunicationsException("createShipment exception caught: " + re);
            } 
            catch (Exception e) {
                logger.error(e);
                throw new SDSApplicationException("Error retrieving results from SDS server",e);
            }
        } finally {
            logger.debug("Leaving createShipmentXML");
        }
        
        return responseDoc;
    }
    
    /**
     * Sends a cancel request for the passed in parameters
     * @param connection database connection
     * @param cancelDocument contains the close parameters
     * @return
     * @throws SDSCommunicationsException
     * @throws SDSApplicationException
     */
    public Document cancelShipmentXML(Connection connection, Document cancelDocument) throws SDSCommunicationsException, SDSApplicationException {

        logger.debug("Entering cancelShipmentXML");    
        Document response = null;
        
        try {
            MessageElement[] msgs = new MessageElement[1];
            msgs[0] = new MessageElement((Element)cancelDocument.getFirstChild());
            CancelShipmentXMLXdReqCancel request = new CancelShipmentXMLXdReqCancel(msgs);
            
    
            VSAMSSoap12Stub binding;
            try {
                URL url = new URL(getSdsUrl());
                binding = (VSAMSSoap12Stub)new VSAMSLocator().getVSAMSSoap12(url);
            } catch (MalformedURLException mue) {
                logger.error("Error setting SDS url",mue);
                throw new SDSCommunicationsException("Error setting SDS url",mue);
            } catch (ServiceException jre) {
                if (jre.getLinkedCause() != null)
                    logger.error(jre.getLinkedCause());
                throw new SDSCommunicationsException("JAX-RPC ServiceException caught: " + jre);
            }
    
            // Time out value
            binding.setTimeout(getSDSTimeout(connection));
    
            // Send Document
            try {
                CancelShipmentXMLResponseCancelShipmentXMLResult results = binding.cancelShipment(request);
                msgs = results.get_any();
                logger.debug("Received "+msgs.length+" responses from SDS.");
                response = msgs[0].getAsDocument();
            } 
            catch (RemoteException re) {
                logger.error(re);
                throw new SDSCommunicationsException("cancelShipment exception caught: " + re);
            } 
            catch (Exception e) {
                logger.error(e);
                throw new SDSApplicationException("Error retrieving results from SDS server (cancelShipment)",e);
            }
        } finally {
            logger.debug("Leaving cancelShipmentXML");
        }
        
        return response;
    }
    
    /**
     * Sends a cancel request for the passed in parameters
     * @param connection database connection
     * @param cancelDocument contains the close parameters
     * @return
     * @throws SDSCommunicationsException
     * @throws SDSApplicationException
     */
    public Document cancelShipment(Connection connection, String cancelDocument) throws SDSCommunicationsException, SDSApplicationException {

        logger.debug("Entering cancelShipment");  
        Document responseDoc = null;

        try {
            VSAMSSoap12Stub binding;
            try {
                URL url = new URL(getSdsUrl());
                binding = (VSAMSSoap12Stub)new VSAMSLocator().getVSAMSSoap12(url);
            } catch (MalformedURLException mue) {
                logger.error("Error setting SDS url",mue);
                throw new SDSCommunicationsException("Error setting SDS url",mue);
            } catch (ServiceException jre) {
                if (jre.getLinkedCause() != null)
                    logger.error(jre.getLinkedCause());
                throw new SDSCommunicationsException("JAX-RPC ServiceException caught: " + jre);
            }
            
            // Time out value
            binding.setTimeout(getSDSTimeout(connection));
            
            // Send Document
            String strResponse;
            try {
                strResponse = binding.cancelShipment(cancelDocument);
            } 
            catch (RemoteException re) {
                logger.error(re);
                throw new SDSCommunicationsException("cancelShipment exception caught: " + re);
            } 
            catch (Exception e) {
                logger.error(e);
                throw new SDSApplicationException("Error retrieving results from SDS server (cancelShipment)",e);
            }
             
            try {
               responseDoc = JAXPUtil.parseDocument(strResponse,false,false);
            } catch (JAXPException e) {
                logger.error(e);
                throw new SDSApplicationException("Error while parsing response document from SDS server.",e);
            }
        } finally {
            logger.debug("Leaving cancelShipment");
        }
        
        return responseDoc;
    }

    /**
     * Sends a close request for the passed in parameters
     * @param connection database connection
     * @param closeDocument contains the close parameters
     * @return
     * @throws SDSCommunicationsException
     * @throws SDSApplicationException
     */
    public Document closeXML(Connection connection, Document closeDocument) throws SDSCommunicationsException, SDSApplicationException {
    
        Document response = null;
        logger.debug("Entering closeXML");    
        
        try {
            MessageElement[] msgs = new MessageElement[1];
            msgs[0] = new MessageElement((Element)closeDocument.getFirstChild());
            CloseXMLXdReqClose csu = new CloseXMLXdReqClose(msgs);
    
            VSAMSSoap12Stub binding;
            try {
                URL url = new URL(getSdsUrl());
                binding = (VSAMSSoap12Stub)new VSAMSLocator().getVSAMSSoap12(url);
            } catch (MalformedURLException mue) {
                logger.error("Error setting SDS url",mue);
                throw new SDSCommunicationsException("Error setting SDS url",mue);
            } catch (ServiceException jre) {
                if (jre.getLinkedCause() != null)
                    logger.error(jre.getLinkedCause());
                throw new SDSCommunicationsException("JAX-RPC ServiceException caught: " + jre);
            }
    
            // Time out value
            binding.setTimeout(getSDSTimeout(connection));
    
            // Send Document
            try {
                CloseXMLResponseCloseXMLResult results = binding.close(csu);
                msgs = results.get_any();
                logger.debug("Received "+msgs.length+" responses from SDS.");
                response = msgs[0].getAsDocument();
            } 
            catch (RemoteException re) {
                logger.error(re);
                throw new SDSCommunicationsException("close exception caught: " + re);
            } 
            catch (Exception e) {
                logger.error(e);
                throw new SDSApplicationException("Error retrieving results from SDS server (close)",e);
            }
        } finally {
            logger.debug("Leaving closeXML");   
        }
        
        return response;
    }

    /**
     * Sends a close request for the passed in parameters
     * @param connection database connection
     * @param closeDocument contains the close parameters
     * @return
     * @throws SDSCommunicationsException
     * @throws SDSApplicationException
     */
    public Document closeTrailerXML(Connection connection, Document closeDocument) throws SDSCommunicationsException, SDSApplicationException {
    
        Document response = null;
        logger.debug("Entering closeTrailerXML");    
        
        try {
            MessageElement[] msgs = new MessageElement[1];
            msgs[0] = new MessageElement((Element)closeDocument.getFirstChild());
            CloseTrailerXMLXdReqCloseTrailer csu = new CloseTrailerXMLXdReqCloseTrailer(msgs);
    
            VSAMSSoap12Stub binding;
            try {
                URL url = new URL(getSdsUrl());
                binding = (VSAMSSoap12Stub)new VSAMSLocator().getVSAMSSoap12(url);
            } catch (MalformedURLException mue) {
                logger.error("Error setting SDS url",mue);
                throw new SDSCommunicationsException("Error setting SDS url",mue);
            } catch (ServiceException jre) {
                if (jre.getLinkedCause() != null)
                    logger.error(jre.getLinkedCause());
                throw new SDSCommunicationsException("JAX-RPC ServiceException caught: " + jre);
            }
    
            // Time out value
            binding.setTimeout(getSDSTimeout(connection));
    
            // Send Document
            try {
                CloseTrailerXMLResponseCloseTrailerXMLResult results = binding.closeTrailer(csu);
                msgs = results.get_any();
                logger.debug("Received "+msgs.length+" responses from SDS.");
                response = msgs[0].getAsDocument();
            } 
            catch (RemoteException re) {
                logger.error(re);
                throw new SDSCommunicationsException("close exception caught: " + re);
            } 
            catch (Exception e) {
                logger.error(e);
                throw new SDSApplicationException("Error retrieving results from SDS server (close)",e);
            }
        } finally {
            logger.debug("Leaving closeTrailerXML");   
        }
        
        return response;
    }

    /**
     * Sends a close request for the passed in parameters
     * @param connection database connection
     * @param closeDocument contains the close parameters
     * @return
     * @throws SDSCommunicationsException
     * @throws SDSApplicationException
     */
    public Document close(Connection connection, String closeDocument) throws SDSCommunicationsException, SDSApplicationException {
        Document responseDoc = null;
        logger.debug("Entering close");
        
        try {
            VSAMSSoap12Stub binding;
        
            try {
                URL url = new URL(getSdsUrl());
                binding = (VSAMSSoap12Stub)new VSAMSLocator().getVSAMSSoap12(url);
            } catch (MalformedURLException mue) {
                logger.error("Error setting SDS url",mue);
                throw new SDSCommunicationsException("Error setting SDS url",mue);
            } catch (ServiceException jre) {
                if (jre.getLinkedCause() != null)
                    logger.error(jre.getLinkedCause());
                throw new SDSCommunicationsException("JAX-RPC ServiceException caught: " + jre);
            }
    
            // Time out value
            binding.setTimeout(getSDSTimeout(connection));
    
            // Send Document
            String strResponse;
            try {
                strResponse = binding.close(closeDocument);
            } 
            catch (RemoteException re) {
                logger.error(re);
                throw new SDSCommunicationsException("close exception caught: " + re);
            } 
            catch (Exception e) {
                logger.error(e);
                throw new SDSApplicationException("Error retrieving results from SDS server (close)",e);
            }
            
            try {
                responseDoc = JAXPUtil.parseDocument(strResponse,false,false);
            } catch (JAXPException e) {
                throw new SDSApplicationException("Error while parsing response document from SDS server.",e);
            }
        } finally {        
            logger.debug("Leaving close");
        }
        
        return responseDoc;
    }

    /**
     * This method will send a request for status updates to the SDS ship server.
     * @param connection database connection
     * @param lineOfBusiness should always be "AA"
     * @param statusParm what type of status is being requested
     * @param startDate starting date of the inquiry.  May be null for new status only
     * @param endDate ending date of the inquiry.  May be null for new status only
     * @return
     * @throws SDSCommunicationsException
     * @throws SDSApplicationException
     */
     public Document getStatusUpdates(Connection connection, String lineOfBusiness, SDSStatusParm statusParm, String startDate, String endDate) throws SDSCommunicationsException, SDSApplicationException {

        logger.debug("Entering getStatusUpdates");

        Document response = null;
        try {
            WTMServiceSoap_BindingStub binding;
            MessageElement[] msgs;
             
            try {
                URL url = new URL(getWtmUrl());
                binding = (WTMServiceSoap_BindingStub)new WTMServiceLocator().getWTMServiceSoap(url);
            } catch (MalformedURLException mue) {
                logger.error("Error setting SDS url",mue);
                throw new SDSCommunicationsException("Error setting SDS url",mue);
            } catch (ServiceException jre) {
                if (jre.getLinkedCause() != null)
                    logger.error(jre.getLinkedCause());
                throw new SDSCommunicationsException("JAX-RPC ServiceException caught: " + jre);
            }
    
            // Time out value
            binding.setTimeout(getSDSTimeout(connection));
    
            // Send Document
            GetPLDUpdateResponseGetPLDUpdateResult results;
            try {
                results = binding.getPLDUpdate(lineOfBusiness,statusParm==null?null:statusParm.toString(),startDate,endDate);
                msgs = results.get_any();
                logger.debug("Received "+msgs.length+" responses from SDS.");
                response = msgs[0].getAsDocument();
            } 
            catch (RemoteException re) {
                logger.error(re);
                throw new SDSCommunicationsException("close exception caught: " + re);
            } 
            catch (Exception e) {
                logger.error(e);
                throw new SDSApplicationException("Error retrieving results from SDS server (close)",e);
            }
        } finally {
            logger.debug("Leaving getStatusUpdates");
        }
        
        return response;
    }

    /**
     * Request previous ship units for the passed in parameters
     * @param connection database connection
     * @param request document containing the parameters needed to request the 
     *        ship unit document.
     * @return
     * @throws SDSCommunicationsException
     * @throws SDSApplicationException
     */
    public Document getShipUnitInfo(Connection connection, Document request) throws SDSCommunicationsException, SDSApplicationException {
        logger.debug("Entering getShipUnitInfo");
        Document response = null;
        
        try {
            VSAMSSoap12Stub binding;
            try {
                URL url = new URL(getSdsUrl());
                binding = (VSAMSSoap12Stub)new VSAMSLocator().getVSAMSSoap12(url);
            } catch (MalformedURLException mue) {
                logger.error("Error setting SDS url",mue);
                throw new SDSCommunicationsException("Error setting SDS url",mue);
            } catch (ServiceException jre) {
                if (jre.getLinkedCause() != null)
                    logger.error(jre.getLinkedCause());
                throw new SDSCommunicationsException("JAX-RPC ServiceException caught: " + jre);
            }
            
            // Time out value
            binding.setTimeout(getSDSTimeout(connection));
            
            MessageElement[] msgs = new MessageElement[1];
            msgs[0] = new MessageElement((Element)request.getFirstChild()); 
            GetShipmentInfoXMLXdReqInfo params = new GetShipmentInfoXMLXdReqInfo(msgs);
            
            // Send Document
            try {
                GetShipmentInfoXMLResponseGetShipmentInfoXMLResult results = binding.getShipmentInfo(params);
                msgs = results.get_any();
                response = msgs[0].getAsDocument();
            } 
            catch (RemoteException re) {
                logger.error(re);
                throw new SDSCommunicationsException("close exception caught: " + re);
            } 
            catch (Exception e) {
                logger.error(e);
                throw new SDSApplicationException("Error retrieving results from SDS server (close)",e);
            }
        } finally {
            logger.debug("Leaving getShipUnitInfo");
        }
        
        return response;
     }
     
     public Document getOrderStatus(Connection connection, Document request) throws SDSCommunicationsException, SDSApplicationException {
        logger.debug("Entering getOrderStatus");
        Document response = null;
        
        try {
            WTMServiceSoap_BindingStub binding;            
            MessageElement[] msgs = new MessageElement[1];
             
            try {
                URL url = new URL(getWtmUrl());
                binding = (WTMServiceSoap_BindingStub)new WTMServiceLocator().getWTMServiceSoap(url);
            } catch (MalformedURLException mue) {
                logger.error("Error setting SDS url",mue);
                throw new SDSCommunicationsException("Error setting SDS url",mue);
            } catch (ServiceException jre) {
                if (jre.getLinkedCause() != null)
                    logger.error(jre.getLinkedCause());
                throw new SDSCommunicationsException("JAX-RPC ServiceException caught: " + jre);
            }
            
            // Time out value
            binding.setTimeout(getSDSTimeout(connection));
            
            msgs[0] = new MessageElement((Element)request.getFirstChild()); 
            GetOrderStatusOrderStatusParams params = new GetOrderStatusOrderStatusParams(msgs);
            
            // Send Document
            try {
                GetOrderStatusResponseGetOrderStatusResult results = binding.getOrderStatus(params);
                msgs = results.get_any();
                response = msgs[0].getAsDocument();
            } 
            catch (RemoteException re) {
                logger.error(re);
                throw new SDSCommunicationsException("close exception caught: " + re);
            } 
            catch (Exception e) {
                logger.error(e);
                throw new SDSApplicationException("Error retrieving results from SDS server (close)",e);
            }
        } finally {
            logger.debug("Leaving getOrderStatus");
        }
        
        return response;
     }

    /**
     * Get a value from global parms.  Will return the default value if not found or any error occurs.
     * @param connection database connection
     * @param context global param context
     * @param paramName parameter name
     * @param defaultValue default value to return
     * @return
     */
    private String getGlobalParameter(Connection connection, String context, String paramName, String defaultValue ) {
        String retVal = "";
        GlobalParameterVO vo = null;
        try {
            vo = CommonUtils.getGlobalParameter(connection,context,paramName,defaultValue);
        } catch (Exception e) {
            logger.warn("Global parameter "+context+"/"+paramName+" not found.  Will use default value of "+defaultValue,e);
        }
        
        if( vo==null ) {
            if( StringUtils.isNotBlank(defaultValue) ) {
                retVal = defaultValue;
            }
        } else {
            retVal = vo.getValue();
        }
        
        return retVal;
    }

    /**
     * Get the timeout value, stored in global parms
     * @param connection database connection
     * @return Integer containing the timeout value.  Returned value will never be null.
     */
    private Integer getSDSTimeout(Connection connection) {
        Integer timeout = new Integer(DEFAULT_TIMEOUT*1000);
        try {
            String strTimeout = this.getGlobalParameter(connection,ShipConstants.GLOBAL_PARMS_CONTEXT,SDS_TIMEOUT_SECONDS,String.valueOf(DEFAULT_TIMEOUT));
            timeout = Integer.parseInt(strTimeout)*1000;
        } catch (Exception e) {
            logger.error(SDS_TIMEOUT_SECONDS+" does not appear to be configured correctly in global parms.  Defaulting to "+DEFAULT_TIMEOUT+" seconds.",e);
            timeout = DEFAULT_TIMEOUT*1000;
        }
        
        return timeout;
    }

    public void setResourceProvider(ResourceProviderBase resourceProvider) {
        this.resourceProvider = resourceProvider;
    }

    public ResourceProviderBase getResourceProvider() {
        return resourceProvider;
    }

    public String getSdsUrl() throws SDSApplicationException {
        if( sdsUrl==null ) {
            try {
                sdsUrl = resourceProvider.getGlobalParameter(SHIP_PROCESSING_CONFIG_CONTEXT,SDS_URL);
            } catch (Exception e) {
                logger.fatal("Unable to get sds url for ship server",e);
                throw new SDSApplicationException(e);
            }
        
        }
        return sdsUrl;
    }

    public String getWtmUrl() throws SDSApplicationException {
        if( wtmUrl==null ) {
            try {
                wtmUrl = resourceProvider.getGlobalParameter(SHIP_PROCESSING_CONFIG_CONTEXT,WTM_URL);  
            } catch (Exception e) {
                logger.fatal("Unable to get wtm url for ship server",e);
                throw new SDSApplicationException(e);
            }
        }
        return wtmUrl;
    }
}
