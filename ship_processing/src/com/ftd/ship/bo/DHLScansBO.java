package com.ftd.ship.bo;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.ship.vo.CarrierScanVO;
import com.ftd.ship.vo.DHLScanVO;

import java.io.IOException;
import java.io.RandomAccessFile;

import java.sql.Timestamp;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;


public class DHLScansBO extends CarrierScansBO {
    private static final Logger LOGGER = new Logger("com.ftd.ship.bo.DHLScansBO");
    private static final String DHL_EVENT_FORMAT = "yyMMddHHmm";

    public DHLScansBO() {
        carrierId = "DHL";
    }
    
    public List<CarrierScanVO> parseScanFile(String fileName, String localDirectory) {
        ArrayList<CarrierScanVO> list = new ArrayList<CarrierScanVO>();
        
        RandomAccessFile raf = null;
        try {
            raf = new RandomAccessFile(localDirectory+"/"+fileName, "r");
            String line = "";
    
            while ((line = raf.readLine())!= null) {
                
                if( StringUtils.isBlank(line) ) {
                    continue;
                }
                
                DHLScanVO dhlVO = new DHLScanVO(line);
                CarrierScanVO vo = new CarrierScanVO();
                vo.setCarrierId(carrierId);
                vo.setFileName(fileName);
                vo.setOrderId(dhlVO.getCustomerReferenceNumber());
                vo.setProcessed(false);
                vo.setScanType(dhlVO.getShipmentStatusEventCode());
                vo.setTrackingNumber(dhlVO.getTrackingNumber());
                String dateString = dhlVO.getEventDate()+dhlVO.getEventTime();
                Date eventDate;
                try {
                    eventDate = (new SimpleDateFormat(DHL_EVENT_FORMAT)).parse(dateString);
                } catch (Exception e) {
                    LOGGER.warn("Error parsing event date for order/tracking number "+vo.getOrderId()+"/"+vo.getTrackingNumber()+".  Will use current timestamp instead.");
                    eventDate = new Date();
                }
                vo.setTimestamp(new Timestamp(eventDate.getTime()));
                
                list.add(vo);
            }
        } catch (Exception e) {
            LOGGER.error("Error while reading scan file "+fileName,e);
        } finally {
            if( raf!=null ) {
                try {
                    raf.close();
                } catch (IOException ioe) {
                    LOGGER.warn("Failed to closing noscan file "+fileName,ioe);
                }
            }
        }
        
        return list;
    }
}
