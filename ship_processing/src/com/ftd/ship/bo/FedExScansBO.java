package com.ftd.ship.bo;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.ship.vo.CarrierScanVO;
import com.ftd.ship.vo.FedexDetailScanVO;
import com.ftd.ship.vo.FedexHeaderScanVO;

import java.io.IOException;
import java.io.RandomAccessFile;

import java.sql.Timestamp;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;


public class FedExScansBO extends CarrierScansBO {
    private static final Logger LOGGER = new Logger("com.ftd.ship.bo.FedExScansBO");
    private static final String FEDEX_EVENT_FORMAT = "yyyyMMddhhmm";
    
    public FedExScansBO() {
        carrierId = "FEDEX";
    }
    
    public List<CarrierScanVO> parseScanFile(String fileName, String localDirectory) {
        ArrayList<CarrierScanVO> list = new ArrayList<CarrierScanVO>();
        
        RandomAccessFile raf = null;
        try {
            raf = new RandomAccessFile(localDirectory+"/"+fileName, "r");
            String line = "";
            FedexHeaderScanVO headerVO = null;
    
            while ((line = raf.readLine()) != null) {
            
                if( StringUtils.isBlank(line) ) {
                    continue;
                }
                
                String recordType = line.substring(0, 1);
                if (StringUtils.equals("H",recordType)) {
                    headerVO = new FedexHeaderScanVO(line);
                } else if (StringUtils.equals("D",recordType)) {
                    FedexDetailScanVO detailVO = new FedexDetailScanVO(line);
                    CarrierScanVO vo = new CarrierScanVO();
                    vo.setCarrierId(carrierId);
                    vo.setFileName(fileName);
                    vo.setOrderId(headerVO.getReference());
                    vo.setProcessed(false);
                    vo.setScanType(detailVO.getScanType());
                    vo.setTrackingNumber(headerVO.getAirbillNumber());
                    
                    //Format 200610311630
                    String dateString = detailVO.getScanDate() + detailVO.getScanTime();
                    Date eventDate;
                    try {
                        eventDate = (new SimpleDateFormat(FEDEX_EVENT_FORMAT)).parse(dateString);
                    } catch (Exception e) {
                        LOGGER.warn("Error parsing event date for order/tracking number "+vo.getOrderId()+"/"+vo.getTrackingNumber()+".  Will use current timestamp instead.");
                        eventDate = new Date();
                    }
                    vo.setTimestamp(new Timestamp(eventDate.getTime()));
                    
                    list.add(vo);
                }
            }
        } catch (Exception e) {
            LOGGER.error("Error while reading scan file "+fileName,e);
        } finally {
            if( raf!=null ) {
                try {
                    raf.close();
                } catch (IOException ioe) {
                    LOGGER.warn("Failed to closing noscan file "+fileName,ioe);
                }
            }
        }
        
        return list;
    }
}
