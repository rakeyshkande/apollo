package com.ftd.ship.mdb;

import java.sql.Connection;

import javax.ejb.EJBException;
import javax.ejb.MessageDrivenContext;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.springframework.context.access.ContextSingletonBeanFactoryLocator;
import org.springframework.ejb.support.AbstractJmsMessageDrivenBean;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.ship.bo.CarrierZipsBO;
import com.ftd.ship.bo.communications.sds.SDSApplicationException;
import com.ftd.ship.common.framework.util.CommonUtils;
import com.ftd.ship.common.resources.ResourceProviderBase;
import com.ftd.ship.dao.ShipDAO;

public class RetrieveCarrierZipsMDB extends AbstractJmsMessageDrivenBean implements MessageListener {
    private MessageDrivenContext context;
    private ResourceProviderBase resourceProvider;
    private ShipDAO shipDAO;
    private static Logger logger = new Logger("com.ftd.ship.mdb.RetrieveCarrierZipsMDB");
    
    public RetrieveCarrierZipsMDB() {
    }
    
    public void ejbCreate() {
        setBeanFactoryLocatorKey("shipProcessingApp");
        setBeanFactoryLocator(ContextSingletonBeanFactoryLocator.getInstance());
        super.ejbCreate();
    }

    protected void onEjbCreate() {
        resourceProvider = (ResourceProviderBase)this.getBeanFactory().getBean("resourceProvider");        
        shipDAO          = (ShipDAO)this.getBeanFactory().getBean("shipDAO");
    }

    /**
    * This is the onMessage() which execute when the message is consumed
    * 
    * @param message The Message which contains the payload of the JMS message
    */
    public void onMessage(Message message) {
        logger.debug("Start: onMessage()");
        
        Connection connection = null;
        String carrierId = null;
        
        try {
            carrierId = ((TextMessage)message).getText();
            connection = resourceProvider.getDatabaseConnection();
            CarrierZipsBO carrierZipsBO = new CarrierZipsBO();
            
            if(carrierId.equalsIgnoreCase("CARRIERFILEMISSING")){
            	carrierZipsBO.checkCarrierZipFiles(connection);
            }
            else{
	            try {
	            	carrierId = shipDAO.getCarrierScanFormat(connection,carrierId);
	            	logger.debug("RetrieveCarrierZipsMDB - carrierId: "+carrierId);
	            } catch (Exception e) {
	                throw new SDSApplicationException("RetrieveCarrierZipsMDB - Error while retrieving scan format for carrier "+carrierId,e);
	            }
	            	                        
	            logger.debug("### ### Message: "+carrierId);
	            carrierZipsBO.getCarrierZips(connection, carrierId);
            }            
            
        } catch (SDSApplicationException appE) {
            String errMsg = 
                "RetrieveCarrierZipsMDB - Error while retrieving zips for carrier " + carrierId + 
                ".  " +
                appE.getMessage();
            logger.error(errMsg,appE);

            //Send out system notification 
            try {
                CommonUtils.getInstance().sendSystemMessage(errMsg);
            } catch (Exception e) {
                errMsg = "Unable to send message to support pager.";
                logger.fatal(errMsg,e);
                throw new EJBException(errMsg,e);
            }
        } catch (Throwable t) {
            logger.error("RetrieveCarrierZipsMDB failed.",t);
            try {
                CommonUtils.getInstance().sendSystemMessage("RetrieveCarrierZipsMDB", "RetrieveCarrierZipsMDB failed" + t.getMessage());
            } catch (Exception e) {
                String errMsg = "Unable to send message to support pager.";
                logger.fatal(errMsg,e);
            }
        } finally {
            try 
            {
                if(connection!=null) {
                    connection.close();
                }
            }
            catch (Exception e)
            {
                logger.warn(e);
            }
        }
    }

    public void setMessageDrivenContext(MessageDrivenContext MessageDrivenContext) throws EJBException {
        this.context = MessageDrivenContext;
    }
}
