package com.ftd.ship.mdb;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.ship.bo.SDSInboundMsgProcessingBO;
import com.ftd.ship.bo.communications.sds.SDSApplicationException;
import com.ftd.ship.common.framework.util.CommonUtils;
import com.ftd.ship.common.resources.ResourceProviderBase;

import java.sql.Connection;

import javax.ejb.EJBException;
import javax.ejb.MessageDrivenContext;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.springframework.context.access.ContextSingletonBeanFactoryLocator;
import org.springframework.ejb.support.AbstractJmsMessageDrivenBean;


public class SDSInboundMDB extends AbstractJmsMessageDrivenBean implements MessageListener {
    private MessageDrivenContext context;
    private ResourceProviderBase resourceProvider;
    private SDSInboundMsgProcessingBO sdsInProcessingBO;
    private static Logger logger = new Logger("com.ftd.ship.mdb.SDSInboundMDB");
    
    public SDSInboundMDB() {
    }
    
    public void ejbCreate() {
        setBeanFactoryLocatorKey("shipProcessingApp");
        setBeanFactoryLocator(ContextSingletonBeanFactoryLocator.getInstance());
        super.ejbCreate();
    }

    protected void onEjbCreate() {
        resourceProvider = (ResourceProviderBase)this.getBeanFactory().getBean("resourceProvider");
        sdsInProcessingBO = (SDSInboundMsgProcessingBO)this.getBeanFactory().getBean("sdsInProcessingBO");
    }

    /**
    * This is the onMessage() which execute when the message is consumed
    * 
    * @param message The Message which contains the payload of the JMS message
    */
    public void onMessage(Message message) {
        logger.debug("Start: onMessage()");
        
        Connection connection = null;
        String msgId = null;
        
        try {
            msgId = ((TextMessage)message).getText();
            connection = resourceProvider.getDatabaseConnection();
            sdsInProcessingBO.processMessage(connection, ((TextMessage)message).getText());
        } catch (SDSApplicationException appE) {
            String errMsg = 
                "Error from Argo while processing sds_status_message id: " + msgId + 
                ".  Manual intervention is required.";
            logger.error(errMsg,appE);

            //Send out system notification 
            try {
                CommonUtils.getInstance().sendSystemMessage(errMsg);
            } catch (Exception e) {
                errMsg = "Unable to send message to support pager.";
                logger.fatal(errMsg,e);
                throw new EJBException(errMsg,e);
            }  
        } catch (Throwable t) {
            logger.error("SDSInboundMDB failed.",t);
            try {
                CommonUtils.getInstance().sendSystemMessage("SDSInboundMDB", "SDSInboundMDB failed" + t.getMessage());
            } catch (Exception e) {
                String errMsg = "Unable to send message to support pager.";
                logger.fatal(errMsg,e);
            }
        } finally {
            try 
            {
                if(connection!=null) {
                    connection.close();
                }
            }
            catch (Exception e)
            {
                logger.warn(e);
            }
        }
    }

    public void setMessageDrivenContext(MessageDrivenContext MessageDrivenContext) throws EJBException {
        this.context = MessageDrivenContext;
    }
}
