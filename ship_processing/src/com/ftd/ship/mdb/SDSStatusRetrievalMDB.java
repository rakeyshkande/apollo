package com.ftd.ship.mdb;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.ship.bo.SDSMissingStatusBO;
import com.ftd.ship.bo.SDSMsgRetrievalProcessingBO;
import com.ftd.ship.bo.communications.sds.SDSApplicationException;
import com.ftd.ship.bo.communications.sds.SDSCommunicationsException;
import com.ftd.ship.common.SDSConstants;
import com.ftd.ship.common.framework.util.CommonUtils;
import com.ftd.ship.common.resources.ResourceProviderBase;
import com.ftd.ship.common.resources.SDSStatusParm;

import java.sql.Connection;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;

import java.util.GregorianCalendar;

import javax.ejb.MessageDrivenContext;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.commons.lang.StringUtils;

import org.springframework.context.access.ContextSingletonBeanFactoryLocator;
import org.springframework.ejb.support.AbstractJmsMessageDrivenBean;


public class SDSStatusRetrievalMDB extends AbstractJmsMessageDrivenBean implements MessageListener {
    private MessageDrivenContext context;
    private ResourceProviderBase resourceProvider;
    private SDSMsgRetrievalProcessingBO sdsMsgRetrievalProcessingBO;
    private SDSMissingStatusBO sdsMissingStatusBO;
    private static Logger logger = new Logger("com.ftd.ship.mdb.SDSStatusRetrievalMDB");
    private static String SDF = "MM/dd/yyyy";
    
    public SDSStatusRetrievalMDB() {
    }
    
    public void ejbCreate() {
        setBeanFactoryLocatorKey("shipProcessingApp");
        setBeanFactoryLocator(ContextSingletonBeanFactoryLocator.getInstance());
        super.ejbCreate();
    }

    protected void onEjbCreate() {
        resourceProvider = (ResourceProviderBase)this.getBeanFactory().getBean("resourceProvider");
        sdsMsgRetrievalProcessingBO = (SDSMsgRetrievalProcessingBO)this.getBeanFactory().getBean("sdsMsgRetrievalProcessingBO");
        sdsMissingStatusBO = (SDSMissingStatusBO)this.getBeanFactory().getBean("sdsMissingStatusBO");
    }

    /**
    * This is the onMessage() which execute when the message is consumed
    * 
    * @param message The Message which contains the payload of the JMS message
    */
    public void onMessage(Message message) {
        logger.debug("Start: onMessage()");
        //TODO: 2.4 complete
        //Add logic to ask for status requests for specific cartons
        
        Connection connection = null;
        String statusParmStr;
        
        try { 
            connection = resourceProvider.getDatabaseConnection();            
            statusParmStr = StringUtils.trimToEmpty(((TextMessage)message).getText());
            String[] parms = StringUtils.split(statusParmStr,' ');
            
            logger.debug("Parameters passed: ["+statusParmStr+"]");
            
            if( StringUtils.equals(parms[0],SDSConstants.MISSING_STATUS_REQUEST) ) {
                sdsMissingStatusBO.processRequest(connection);
            } else {
                //Don't worry about time portion, bo will remove it when it's converted to a string
                Date startDate = null;
                Date endDate = null;
                SDSStatusParm statusType = null;
                Calendar cal = GregorianCalendar.getInstance();
                
                if( parms.length>2 ) {
                    try {
                    startDate = (new SimpleDateFormat(SDF)).parse(parms[1]);
                    endDate = (new SimpleDateFormat(SDF)).parse(parms[2]);
                    } catch (ParseException pe) {
                        throw new SDSApplicationException("Error processing parameters.  Expecting <statusType> <start date (mm/dd/yyyy))> <end date (mm/dd/yyyy)>",pe);
                    }
                }
                else if( parms.length>1 ) {
                    if( StringUtils.equalsIgnoreCase("TODAY",parms[1]) ) {
                        startDate = cal.getTime();
                        cal.add(Calendar.DATE,1);
                        endDate = cal.getTime();
                    } else if( StringUtils.equalsIgnoreCase("YESTERDAY",parms[1]) ) {
                        endDate = cal.getTime();
                        cal.add(Calendar.DAY_OF_MONTH,-1);
                        startDate = cal.getTime();
                    } else {
                        int daysToProcess = 0;
                        
                        //Was a number passed in?
                        try {
                            daysToProcess = Integer.parseInt(parms[1]);
                            if( daysToProcess < 0 ) {
                                daysToProcess = 0;
                            }
                        } catch (NumberFormatException nfe) {
                            throw new SDSApplicationException("Error processing parameters.  Expecting <statusType> <days to process>",nfe);
                        }
                        
                        if( daysToProcess > 0 ) {
                            try {
                                cal.add(Calendar.DATE,1);
                                endDate = cal.getTime();
                                
                                if( daysToProcess==1 ) {
                                    startDate = new Date();
                                } else {
                                    cal.setTime(new Date());
                                    cal.add(Calendar.DATE,(daysToProcess-1)*-1);
                                    startDate = cal.getTime();
                                }
                            } catch (Exception e) {
                                logger.error("Error generating start and end dates.  Will only get new status.");
                                startDate = null;
                                endDate = null;
                            }
                        }
                    }
                }
                
                if( parms.length>0 ) {
                    statusType = SDSStatusParm.toSDSStatusParm(parms[0]);
                }
                
                if( statusType==null ) {
                    //Get them all
                    sdsMsgRetrievalProcessingBO.processRequest(connection,SDSStatusParm.CANCELED,startDate,endDate);
                    sdsMsgRetrievalProcessingBO.processRequest(connection,SDSStatusParm.CANCELED_NOT_LABELED,startDate,endDate);
                    sdsMsgRetrievalProcessingBO.processRequest(connection,SDSStatusParm.LABELED,startDate,endDate);
                    sdsMsgRetrievalProcessingBO.processRequest(connection,SDSStatusParm.REJECTED,startDate,endDate);
                    sdsMsgRetrievalProcessingBO.processRequest(connection,SDSStatusParm.SHIPPED,startDate,endDate);
                } else {
                    //Get the requested status update
                    sdsMsgRetrievalProcessingBO.processRequest(connection,statusType,startDate,endDate);
                }
            }
            
        } catch (SDSCommunicationsException commE) {
            //Really don't care since the process is not transactional
            logger.warn("Comm error with SDS ship server.  Will try again later.",commE);
        } catch (SDSApplicationException appE) {
            logger.error(appE);

            //Send out system notification 
            try {
                CommonUtils.getInstance().sendSystemMessage(appE.getMessage());
            } catch (Exception e) {
                logger.fatal("Unable to send message to support pager.",e);
            }
        } catch (Throwable t) {
            logger.error("SDSStatusRetrievalMDB failed.",t);
            try {
                CommonUtils.getInstance().sendSystemMessage("SDSStatusRetrievalMDB", "SDSStatusRetrievalMDB failed" + t.getMessage());
            } catch (Exception e) {
                String errMsg = "Unable to send message to support pager.";
                logger.fatal(errMsg,e);
            }
        } finally {
            try 
            {
                if(connection!=null) {
                    connection.close();
                }
            }
            catch (Exception e)
            {
                logger.warn(e);
            }
        }
    }

    public void setMessageDrivenContext(MessageDrivenContext messageDrivenContext) {
        this.context = messageDrivenContext;
        this.setBeanFactoryLocator(ContextSingletonBeanFactoryLocator.getInstance());
    }
}
