package com.ftd.ship.vo;

public class ShipMethodVO {
    private String carrierId;
    private String shipMethod;
    private String carrierDelivery;
    
    public ShipMethodVO() {
    }

    public void setCarrierId(String carrierId) {
        this.carrierId = carrierId;
    }

    public String getCarrierId() {
        return carrierId;
    }

    public void setShipMethod(String shipMethod) {
        this.shipMethod = shipMethod;
    }

    public String getShipMethod() {
        return shipMethod;
    }

    public void setCarrierDelivery(String carrierDelivery) {
        this.carrierDelivery = carrierDelivery;
    }

    public String getCarrierDelivery() {
        return carrierDelivery;
    }
}
