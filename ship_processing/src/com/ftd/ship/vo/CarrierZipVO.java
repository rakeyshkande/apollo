package com.ftd.ship.vo;

public class CarrierZipVO {
	String carrierId;
	String zipCode;
    String weekdayByNoonDelFlag;
    String saturdayByNoonDelFlag;
    String userId;
    String saturdayDelAvailable;
    
    public String getSaturdayDelAvailable() {
		return saturdayDelAvailable;
	}
	public void setSaturdayDelAvailable(String saturdayDelAvailable) {
		this.saturdayDelAvailable = saturdayDelAvailable;
	}
	public String getCarrierId() {
		return carrierId;
	}
	public void setCarrierId(String carrierId) {
		this.carrierId = carrierId;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getWeekdayByNoonDelFlag() {
		return weekdayByNoonDelFlag;
	}
	public void setWeekdayByNoonDelFlag(String weekdayByNoonDelFlag) {
		this.weekdayByNoonDelFlag = weekdayByNoonDelFlag;
	}
	public String getSaturdayByNoonDelFlag() {
		return saturdayByNoonDelFlag;
	}
	public void setSaturdayByNoonDelFlag(String saturdayByNoonDelFlag) {
		this.saturdayByNoonDelFlag = saturdayByNoonDelFlag;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}	
}
