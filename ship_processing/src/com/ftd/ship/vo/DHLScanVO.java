package com.ftd.ship.vo;

import org.apache.commons.lang.StringUtils;

public class DHLScanVO {
    private String line;
    private String batchDate;
    private String batchTime;
    private String batchRecordSequence;
    private String trackingNumber;
    private String masterInvoiceNumber;
    private String customerNumber;
    private String pickupDate;
    private String pickupTime;
    private String shipmentStatusEventCode;
    private String eventDate;
    private String eventTime;
    private String numberOfPieces;
    private String comment;
    private String originStation;
    private String destinationStation;
    private String serviceLevelInd;
    private String productType;
    private String packagingType;
    private String customerReferenceNumber;
    
    public DHLScanVO() {
    }
    
    public DHLScanVO(String line) {
        this.line = line;
        this.batchDate = StringUtils.trimToEmpty(StringUtils.substring(line,1,7));
        this.batchTime = StringUtils.trimToEmpty(StringUtils.substring(line,10,14));
        this.batchRecordSequence = StringUtils.trimToEmpty(StringUtils.substring(line,17,22));
        this.trackingNumber = StringUtils.trimToEmpty(StringUtils.substring(line,25,36));
        this.masterInvoiceNumber = StringUtils.trimToEmpty(StringUtils.substring(line,39,50));
        this.customerNumber = StringUtils.trimToEmpty(StringUtils.substring(line,53,64));
        this.pickupDate = StringUtils.trimToEmpty(StringUtils.substring(line,67,73));
        this.pickupTime = StringUtils.trimToEmpty(StringUtils.substring(line,76,80));
        this.shipmentStatusEventCode = StringUtils.trimToEmpty(StringUtils.substring(line,83,85));
        this.eventDate = StringUtils.trimToEmpty(StringUtils.substring(line,88,94));
        this.eventTime = StringUtils.trimToEmpty(StringUtils.substring(line,97,101));
        this.numberOfPieces = StringUtils.trimToEmpty(StringUtils.substring(line,104,109));
        this.comment = StringUtils.trimToEmpty(StringUtils.substring(line,112,142));
        this.originStation = StringUtils.trimToEmpty(StringUtils.substring(line,145,148));
        this.destinationStation = StringUtils.trimToEmpty(StringUtils.substring(line,151,154));
        this.serviceLevelInd = StringUtils.trimToEmpty(StringUtils.substring(line,157,158));
        this.productType = StringUtils.trimToEmpty(StringUtils.substring(line,161,163));
        this.packagingType = StringUtils.trimToEmpty(StringUtils.substring(line,166,167));
        this.customerReferenceNumber = StringUtils.trimToEmpty(StringUtils.substring(line,170,195));
    }
    
    public void setLine(String line) {
        this.line = line;
    }
    
    public String getLine() {
        return line;
    }

    public void setBatchDate(String batchDate) {
        this.batchDate = batchDate;
    }

    public String getBatchDate() {
        return batchDate;
    }

    public void setBatchTime(String batchTime) {
        this.batchTime = batchTime;
    }

    public String getBatchTime() {
        return batchTime;
    }

    public void setBatchRecordSequence(String batchRecordSequence) {
        this.batchRecordSequence = batchRecordSequence;
    }

    public String getBatchRecordSequence() {
        return batchRecordSequence;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setMasterInvoiceNumber(String masterInvoiceNumber) {
        this.masterInvoiceNumber = masterInvoiceNumber;
    }

    public String getMasterInvoiceNumber() {
        return masterInvoiceNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setPickupDate(String pickupDate) {
        this.pickupDate = pickupDate;
    }

    public String getPickupDate() {
        return pickupDate;
    }

    public void setPickupTime(String pickupTime) {
        this.pickupTime = pickupTime;
    }

    public String getPickupTime() {
        return pickupTime;
    }

    public void setShipmentStatusEventCode(String shipmentStatusEventCode) {
        this.shipmentStatusEventCode = shipmentStatusEventCode;
    }

    public String getShipmentStatusEventCode() {
        return shipmentStatusEventCode;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventTime(String eventTime) {
        this.eventTime = eventTime;
    }

    public String getEventTime() {
        return eventTime;
    }

    public void setNumberOfPieces(String numberOfPieces) {
        this.numberOfPieces = numberOfPieces;
    }

    public String getNumberOfPieces() {
        return numberOfPieces;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getComment() {
        return comment;
    }

    public void setOriginStation(String originStation) {
        this.originStation = originStation;
    }

    public String getOriginStation() {
        return originStation;
    }

    public void setDestinationStation(String destinationStation) {
        this.destinationStation = destinationStation;
    }

    public String getDestinationStation() {
        return destinationStation;
    }

    public void setServiceLevelInd(String serviceLevelInd) {
        this.serviceLevelInd = serviceLevelInd;
    }

    public String getServiceLevelInd() {
        return serviceLevelInd;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getProductType() {
        return productType;
    }

    public void setPackagingType(String packagingType) {
        this.packagingType = packagingType;
    }

    public String getPackagingType() {
        return packagingType;
    }

    public void setCustomerReferenceNumber(String customerReferenceNumber) {
        this.customerReferenceNumber = customerReferenceNumber;
    }

    public String getCustomerReferenceNumber() {
        return customerReferenceNumber;
    }
}
