package com.ftd.ship.vo;

public class PickTicketVO {
    private String confirmationNumber; //order
    private String originatorName;     //orign name
    private String originatorPhone;    //origin phone
    private String recipientContactName; //recipient name
    private String recipientCompany;     //recipient company
    private String recipientAddress1;    //recipient address1
    private String recipientAddress2;    //recipient address1
    private String recipientCity;    //recipient city
    private String recipientState;    //recipient state
    private String recipientPostalCode;    //recipient postal
    private String productId;              //product id
    private String productDescription;     //product noun
    private String cardMessage;            //card
    private String custom;                 //custom
    
    public PickTicketVO() {
    }

    public void setConfirmationNumber(String confirmationNumber) {
        this.confirmationNumber = confirmationNumber;
    }

    public String getConfirmationNumber() {
        return confirmationNumber;
    }

    public void setOriginatorName(String originatorName) {
        this.originatorName = originatorName;
    }

    public String getOriginatorName() {
        return originatorName;
    }

    public void setOriginatorPhone(String originatorPhone) {
        this.originatorPhone = originatorPhone;
    }

    public String getOriginatorPhone() {
        return originatorPhone;
    }

    public void setRecipientContactName(String recipientContactName) {
        this.recipientContactName = recipientContactName;
    }

    public String getRecipientContactName() {
        return recipientContactName;
    }

    public void setRecipientCompany(String recipientCompany) {
        this.recipientCompany = recipientCompany;
    }

    public String getRecipientCompany() {
        return recipientCompany;
    }

    public void setRecipientAddress1(String recipientAddress1) {
        this.recipientAddress1 = recipientAddress1;
    }

    public String getRecipientAddress1() {
        return recipientAddress1;
    }

    public void setRecipientAddress2(String recipientAddress2) {
        this.recipientAddress2 = recipientAddress2;
    }

    public String getRecipientAddress2() {
        return recipientAddress2;
    }

    public void setRecipientCity(String recipientCity) {
        this.recipientCity = recipientCity;
    }

    public String getRecipientCity() {
        return recipientCity;
    }

    public void setRecipientState(String recipientState) {
        this.recipientState = recipientState;
    }

    public String getRecipientState() {
        return recipientState;
    }

    public void setRecipientPostalCode(String recipientPostalCode) {
        this.recipientPostalCode = recipientPostalCode;
    }

    public String getRecipientPostalCode() {
        return recipientPostalCode;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setCardMessage(String cardMessage) {
        this.cardMessage = cardMessage;
    }

    public String getCardMessage() {
        return cardMessage;
    }

    public void setCustom(String custom) {
        this.custom = custom;
    }

    public String getCustom() {
        return custom;
    }
}
