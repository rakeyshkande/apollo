package com.ftd.ship.vo;

import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;

public class ProductVO extends BaseVO
{
  private String productId;
  private String novatorId;
  private String productName;
  private String novatorName;
  private String status;
  private String deliveryType;
  private String category;
  private String productType;
  private String productSubType;
  private String colorSizeFlag;
  private double standardPrice;
  private double deluxePrice;
  private double premiumPrice;
  private double preferredPricePoint;
  private double variablePriceMax;
  private String shortDescription;
  private String longDescription;
  private String floristReferenceNumber;
  private String mercuryDescription;
  private String itemComments;
  private String addOnBalloonsFlag;
  private String addOnBearsFlag;
  private String addOnCardsFlag;
  private String addOnFuneralFlag;
  private String codifiedFlag;
  private String exceptionCode;
  private Date exceptionStartDate; 
  private Date exceptionEndDate;
  private String exceptionMessage;
  private String secondChoiceCode;
  private String holidaySecondChoiceCode;
  private String dropshipCode;
  private String discountAllowedFlag;
  private String deliveryIncludedFlag;
  private String taxFlag;
  private String serviceFeeFlag;
  private String exoticFlag;
  private String egiftFlag;
  private String countryId;
  private String arrangementSize;
  private String arrangementColors;
  private String dominantFlowers;
  private String searchPriority;
  private String recipe;
  private String subcodeFlag;
  private String dimWeight;
  private String nextDayUpgradeFlag;
  private String corporateSite;
  private String unspscCode;
  private String priceRank1;
  private String priceRank2;
  private String priceRank3;
  private String shipMethodCarrier;
  private String shipMethodFlorist;
  private String shippingKey;
  private String variablePriceFlag;
  private String holidaySku;
  private double holidayPrice;
  private String catalogFlag;
  private double holidayDeluxePrice;
  private double holidayPremiumPrice;
  private Date holidayStartDate;
  private Date holidayEndDate;
  private String holdUntilAvailable;
  private String mondayDeliveryFreshcut;
  private String twoDaySatFreshcut;
  private String shippingSystem;
  private boolean over21;
  private boolean expressOnly;
  
  //????
  private String companyId;
  private long inventoryLevel;
    
  public ProductVO()
  {
  }
  
  public void setProductId(String productId)
  {
    if(valueChanged(this.productId, productId))
    {
      setChanged(true);
    }
    this.productId = productId;
  }


  public String getProductId()
  {
    return productId;
  }


  public void setNovatorId(String novatorId)
  {
    if(valueChanged(this.novatorId, novatorId))
    {
      setChanged(true);
    }
    this.novatorId = novatorId;
  }


  public String getNovatorId()
  {
    return novatorId;
  }


  public void setProductName(String productName)
  {
    if(valueChanged(this.productName, productName))
    {
      setChanged(true);
    }
    this.productName = productName;
  }


  public String getProductName()
  {
    return productName;
  }


  public void setNovatorName(String novatorName)
  {
    if(valueChanged(this.novatorName, novatorName))
    {
      setChanged(true);
    }
    this.novatorName = novatorName;
  }


  public String getNovatorName()
  {
    return novatorName;
  }


  public void setStatus(String status)
  {
    if(valueChanged(this.status, status))
    {
      setChanged(true);
    }
    this.status = status;
  }


  public String getStatus()
  {
    return status;
  }


  public void setDeliveryType(String deliveryType)
  {
    if(valueChanged(this.deliveryType, deliveryType))
    {
      setChanged(true);
    }
    this.deliveryType = deliveryType;
  }


  public String getDeliveryType()
  {
    return deliveryType;
  }


  public void setCategory(String category)
  {
    if(valueChanged(this.category, category))
    {
      setChanged(true);
    }
    this.category = category;
  }


  public String getCategory()
  {
    return category;
  }


  public void setProductType(String productType)
  {
    if(valueChanged(this.productType, productType))
    {
      setChanged(true);
    }
    this.productType = productType;
  }


  public String getProductType()
  {
    return productType;
  }


  public void setProductSubType(String productSubType)
  {
    if(valueChanged(this.productSubType, productSubType))
    {
      setChanged(true);
    }
    this.productSubType = productSubType;
  }


  public String getProductSubType()
  {
    return productSubType;
  }


  public void setColorSizeFlag(String colorSizeFlag)
  {
    if(valueChanged(this.colorSizeFlag, colorSizeFlag))
    {
      setChanged(true);
    }
    this.colorSizeFlag = colorSizeFlag;
  }


  public String getColorSizeFlag()
  {
    return colorSizeFlag;
  }


  public void setStandardPrice(double standardPrice)
  {
    this.standardPrice = standardPrice;
  }


  public double getStandardPrice()
  {
    return standardPrice;
  }


  public void setDeluxePrice(double deluxePrice)
  {
    this.deluxePrice = deluxePrice;
  }


  public double getDeluxePrice()
  {
    return deluxePrice;
  }


  public void setPremiumPrice(double premiumPrice)
  {
    this.premiumPrice = premiumPrice;
  }


  public double getPremiumPrice()
  {
    return premiumPrice;
  }


  public void setPreferredPricePoint(double preferredPricePoint)
  {
    this.preferredPricePoint = preferredPricePoint;
  }


  public double getPreferredPricePoint()
  {
    return preferredPricePoint;
  }


  public void setVariablePriceMax(double variablePriceMax)
  {
    this.variablePriceMax = variablePriceMax;
  }


  public double getVariablePriceMax()
  {
    return variablePriceMax;
  }


  public void setShortDescription(String shortDescription)
  {
    if(valueChanged(this.shortDescription, shortDescription))
    {
      setChanged(true);
    }
    this.shortDescription = shortDescription;
  }


  public String getShortDescription()
  {
    return shortDescription;
  }


  public void setLongDescription(String longDescription)
  {
    if(valueChanged(this.longDescription, longDescription))
    {
      setChanged(true);
    }
    this.longDescription = longDescription;
  }


  public String getLongDescription()
  {
    return longDescription;
  }


  public void setFloristReferenceNumber(String floristReferenceNumber)
  {
    if(valueChanged(this.floristReferenceNumber, floristReferenceNumber))
    {
      setChanged(true);
    }
    this.floristReferenceNumber = floristReferenceNumber;
  }


  public String getFloristReferenceNumber()
  {
    return floristReferenceNumber;
  }


  public void setMercuryDescription(String mercuryDescription)
  {
    if(valueChanged(this.mercuryDescription, mercuryDescription))
    {
      setChanged(true);
    }
    this.mercuryDescription = mercuryDescription;
  }


  public String getMercuryDescription()
  {
    return mercuryDescription;
  }


  public void setItemComments(String itemComments)
  {
    if(valueChanged(this.itemComments, itemComments))
    {
      setChanged(true);
    }
    this.itemComments = itemComments;
  }


  public String getItemComments()
  {
    return itemComments;
  }


  public void setAddOnBalloonsFlag(String addOnBalloonsFlag)
  {
    if(valueChanged(this.addOnBalloonsFlag, addOnBalloonsFlag))
    {
      setChanged(true);
    }
    this.addOnBalloonsFlag = addOnBalloonsFlag;
  }


  public String getAddOnBalloonsFlag()
  {
    return addOnBalloonsFlag;
  }


  public void setAddOnBearsFlag(String addOnBearsFlag)
  {
    if(valueChanged(this.addOnBearsFlag, addOnBearsFlag))
    {
      setChanged(true);
    }
    this.addOnBearsFlag = addOnBearsFlag;
  }


  public String getAddOnBearsFlag()
  {
    return addOnBearsFlag;
  }


  public void setAddOnCardsFlag(String addOnCardsFlag)
  {
    if(valueChanged(this.addOnCardsFlag, addOnCardsFlag))
    {
      setChanged(true);
    }
    this.addOnCardsFlag = addOnCardsFlag;
  }


  public String getAddOnCardsFlag()
  {
    return addOnCardsFlag;
  }


  public void setAddOnFuneralFlag(String addOnFuneralFlag)
  {
    if(valueChanged(this.addOnFuneralFlag, addOnFuneralFlag))
    {
      setChanged(true);
    }
    this.addOnFuneralFlag = addOnFuneralFlag;
  }


  public String getAddOnFuneralFlag()
  {
    return addOnFuneralFlag;
  }


  public void setCodifiedFlag(String codifiedFlag)
  {
    if(valueChanged(this.codifiedFlag, codifiedFlag))
    {
      setChanged(true);
    }
    this.codifiedFlag = codifiedFlag;
  }


  public String getCodifiedFlag()
  {
    return codifiedFlag;
  }


  public void setExceptionCode(String exceptionCode)
  {
    if(valueChanged(this.exceptionCode, exceptionCode))
    {
      setChanged(true);
    }
    this.exceptionCode = exceptionCode;
  }


  public String getExceptionCode()
  {
    return exceptionCode;
  }


  public void setExceptionStartDate(Date exceptionStartDate)
  {
    if(valueChanged(this.exceptionStartDate, exceptionStartDate))
    {
      setChanged(true);
    }
    this.exceptionStartDate = exceptionStartDate;
  }


  public Date getExceptionStartDate()
  {
    return exceptionStartDate;
  }


  public void setExceptionEndDate(Date exceptionEndDate)
  {
    if(valueChanged(this.exceptionEndDate, exceptionEndDate))
    {
      setChanged(true);
    }
    this.exceptionEndDate = exceptionEndDate;
  }


  public Date getExceptionEndDate()
  {
    return exceptionEndDate;
  }


  public void setExceptionMessage(String exceptionMessage)
  {
    if(valueChanged(this.exceptionMessage, exceptionMessage))
    {
      setChanged(true);
    }
    this.exceptionMessage = exceptionMessage;
  }


  public String getExceptionMessage()
  {
    return exceptionMessage;
  }


  public void setSecondChoiceCode(String secondChoiceCode)
  {
    if(valueChanged(this.secondChoiceCode, secondChoiceCode))
    {
      setChanged(true);
    }
    this.secondChoiceCode = secondChoiceCode;
  }


  public String getSecondChoiceCode()
  {
    return secondChoiceCode;
  }


  public void setDropshipCode(String dropshipCode)
  {
    if(valueChanged(this.dropshipCode, dropshipCode))
    {
      setChanged(true);
    }
    this.dropshipCode = dropshipCode;
  }


  public String getDropshipCode()
  {
    return dropshipCode;
  }


  public void setDiscountAllowedFlag(String discountAllowedFlag)
  {
    if(valueChanged(this.discountAllowedFlag, discountAllowedFlag))
    {
      setChanged(true);
    }
    this.discountAllowedFlag = discountAllowedFlag;
  }


  public String getDiscountAllowedFlag()
  {
    return discountAllowedFlag;
  }


  public void setDeliveryIncludedFlag(String deliveryIncludedFlag)
  {
    if(valueChanged(this.deliveryIncludedFlag, deliveryIncludedFlag))
    {
      setChanged(true);
    }
    this.deliveryIncludedFlag = deliveryIncludedFlag;
  }


  public String getDeliveryIncludedFlag()
  {
    return deliveryIncludedFlag;
  }


  public void setServiceFeeFlag(String serviceFeeFlag)
  {
    if(valueChanged(this.serviceFeeFlag, serviceFeeFlag))
    {
      setChanged(true);
    }
    this.serviceFeeFlag = serviceFeeFlag;
  }


  public String getServiceFeeFlag()
  {
    return serviceFeeFlag;
  }


  public void setExoticFlag(String exoticFlag)
  {
    if(valueChanged(this.exoticFlag, exoticFlag))
    {
      setChanged(true);
    }
    this.exoticFlag = exoticFlag;
  }


  public String getExoticFlag()
  {
    return exoticFlag;
  }


  public void setCountryId(String countryId)
  {
    if(valueChanged(this.countryId, countryId))
    {
      setChanged(true);
    }
    this.countryId = countryId;
  }


  public String getCountryId()
  {
    return countryId;
  }


  public void setArrangementSize(String arrangementSize)
  {
    if(valueChanged(this.arrangementSize, arrangementSize))
    {
      setChanged(true);
    }
    this.arrangementSize = arrangementSize;
  }


  public String getArrangementSize()
  {
    return arrangementSize;
  }


  public void setArrangementColors(String arrangementColors)
  {
    if(valueChanged(this.arrangementColors, arrangementColors))
    {
      setChanged(true);
    }
    this.arrangementColors = arrangementColors;
  }


  public String getArrangementColors()
  {
    return arrangementColors;
  }


  public void setDominantFlowers(String dominantFlowers)
  {
    if(valueChanged(this.dominantFlowers, dominantFlowers))
    {
      setChanged(true);
    }
    this.dominantFlowers = dominantFlowers;
  }


  public String getDominantFlowers()
  {
    return dominantFlowers;
  }


  public void setSearchPriority(String searchPriority)
  {
    if(valueChanged(this.searchPriority, searchPriority))
    {
      setChanged(true);
    }
    this.searchPriority = searchPriority;
  }


  public String getSearchPriority()
  {
    return searchPriority;
  }


  public void setRecipe(String recipe)
  {
    if(valueChanged(this.recipe, recipe))
    {
      setChanged(true);
    }
    this.recipe = recipe;
  }


  public String getRecipe()
  {
    return recipe;
  }


  public void setSubcodeFlag(String subcodeFlag)
  {
    if(valueChanged(this.subcodeFlag, subcodeFlag))
    {
      setChanged(true);
    }
    this.subcodeFlag = subcodeFlag;
  }


  public String getSubcodeFlag()
  {
    return subcodeFlag;
  }


  public void setDimWeight(String dimWeight)
  {
    if(valueChanged(this.dimWeight, dimWeight))
    {
      setChanged(true);
    }
    this.dimWeight = dimWeight;
  }


  public String getDimWeight()
  {
    return dimWeight;
  }


  public void setNextDayUpgradeFlag(String nextDayUpgradeFlag)
  {
    if(valueChanged(this.nextDayUpgradeFlag, nextDayUpgradeFlag))
    {
      setChanged(true);
    }
    this.nextDayUpgradeFlag = nextDayUpgradeFlag;
  }


  public String getNextDayUpgradeFlag()
  {
    return nextDayUpgradeFlag;
  }


  public void setCorporateSite(String corporateSite)
  {
    if(valueChanged(this.corporateSite, corporateSite))
    {
      setChanged(true);
    }
    this.corporateSite = corporateSite;
  }


  public String getCorporateSite()
  {
    return corporateSite;
  }


  public void setUnspscCode(String unspscCode)
  {
    if(valueChanged(this.unspscCode, unspscCode))
    {
      setChanged(true);
    }
    this.unspscCode = unspscCode;
  }


  public String getUnspscCode()
  {
    return unspscCode;
  }


  public void setPriceRank1(String priceRank1)
  {
    if(valueChanged(this.priceRank1, priceRank1))
    {
      setChanged(true);
    }
    this.priceRank1 = priceRank1;
  }


  public String getPriceRank1()
  {
    return priceRank1;
  }


  public void setPriceRank2(String priceRank2)
  {
    if(valueChanged(this.priceRank2, priceRank2))
    {
      setChanged(true);
    }
    this.priceRank2 = priceRank2;
  }


  public String getPriceRank2()
  {
    return priceRank2;
  }


  public void setPriceRank3(String priceRank3)
  {
    if(valueChanged(this.priceRank3, priceRank3))
    {
      setChanged(true);
    }
    this.priceRank3 = priceRank3;
  }


  public String getPriceRank3()
  {
    return priceRank3;
  }


  public void setShipMethodCarrier(String shipMethodCarrier)
  {
    if(valueChanged(this.shipMethodCarrier, shipMethodCarrier))
    {
      setChanged(true);
    }
    this.shipMethodCarrier = shipMethodCarrier;
  }


  public String getShipMethodCarrier()
  {
    return shipMethodCarrier;
  }


  public void setShipMethodFlorist(String shipMethodFlorist)
  {
    if(valueChanged(this.shipMethodFlorist, shipMethodFlorist))
    {
      setChanged(true);
    }
    this.shipMethodFlorist = shipMethodFlorist;
  }


  public String getShipMethodFlorist()
  {
    return shipMethodFlorist;
  }


  public void setShippingKey(String shippingKey)
  {
    if(valueChanged(this.shippingKey, shippingKey))
    {
      setChanged(true);
    }
    this.shippingKey = shippingKey;
  }


  public String getShippingKey()
  {
    return shippingKey;
  }


  public void setCompanyId(String companyId)
  {
    if(valueChanged(this.companyId, companyId))
    {
      setChanged(true);
    }
    this.companyId = companyId;
  }


  public String getCompanyId()
  {
    return companyId;
  }


  public void setHoldUntilAvailable(String holdUntilAvailable)
  {
    if(valueChanged(this.holdUntilAvailable, holdUntilAvailable))
    {
      setChanged(true);
    }
    this.holdUntilAvailable = holdUntilAvailable;
  }


  public String getHoldUntilAvailable()
  {
    return holdUntilAvailable;
  }


  public void setMondayDeliveryFreshcut(String mondayDeliveryFreshcut)
  {
    if(valueChanged(this.mondayDeliveryFreshcut, mondayDeliveryFreshcut))
    {
      setChanged(true);
    }
    this.mondayDeliveryFreshcut = mondayDeliveryFreshcut;
  }


  public String getMondayDeliveryFreshcut()
  {
    return mondayDeliveryFreshcut;
  }


  public void setTwoDaySatFreshcut(String twoDaySatFreshcut)
  {
    if(valueChanged(this.twoDaySatFreshcut, twoDaySatFreshcut))
    {
      setChanged(true);
    }
    this.twoDaySatFreshcut = twoDaySatFreshcut;
  }


  public String getTwoDaySatFreshcut()
  {
    return twoDaySatFreshcut;
  }


  public void setInventoryLevel(long inventoryLevel)
  {
    if(valueChanged(this.inventoryLevel, inventoryLevel))
    {
      setChanged(true);
    }
    this.inventoryLevel = inventoryLevel;
  }


  public long getInventoryLevel()
  {
    return inventoryLevel;
  }
  
  public void setHolidaySecondChoiceCode(String holidaySecondChoiceCode)
  {
    if(valueChanged(this.holidaySecondChoiceCode, holidaySecondChoiceCode))
    {
      setChanged(true);
    }
    this.holidaySecondChoiceCode = holidaySecondChoiceCode;
  }


  public String getHolidaySecondChoiceCode()
  {
    return holidaySecondChoiceCode;
  }
  
  public void setTaxFlag(String taxFlag)
  {
    if(valueChanged(this.taxFlag, taxFlag))
    {
      setChanged(true);
    }
    this.taxFlag = taxFlag;
  }


  public String getTaxFlag()
  {
    return taxFlag;
  }
  
  public void setEgiftFlag(String egiftFlag)
  {
    if(valueChanged(this.egiftFlag, egiftFlag))
    {
      setChanged(true);
    }
    this.egiftFlag = egiftFlag;
  }


  public String getEgiftFlag()
  {
    return egiftFlag;
  }
  
  public void setVariablePriceFlag(String variablePriceFlag)
  {
    if(valueChanged(this.variablePriceFlag, variablePriceFlag))
    {
      setChanged(true);
    }
    this.variablePriceFlag = variablePriceFlag;
  }


  public String getVariablePriceFlag()
  {
    return variablePriceFlag;
  }
  
  public void setHolidaySku(String holidaySku)
  {
    if(valueChanged(this.holidaySku, holidaySku))
    {
      setChanged(true);
    }
    this.holidaySku = holidaySku;
  }


  public String getHolidaySku()
  {
    return holidaySku;
  }
  
  public void setHolidayPrice(double holidayPrice)
  {
    this.holidayPrice = holidayPrice;
  }


  public double getHolidayPrice()
  {
    return holidayPrice;
  }
  
  public void setCatalogFlag(String catalogFlag)
  {
    if(valueChanged(this.holidaySku, catalogFlag))
    {
      setChanged(true);
    }
    this.catalogFlag = catalogFlag;
  }


  public String getCatalogFlag()
  {
    return catalogFlag;
  }
  
  public void setHolidayDeluxePrice(double holidayDeluxePrice)
  {
    this.holidayDeluxePrice = holidayDeluxePrice;
  }


  public double getHolidayDeluxePrice()
  {
    return holidayDeluxePrice;
  }
  
  public void setHolidayPremiumPrice(double holidayPremiumPrice)
  {
    this.holidayPremiumPrice = holidayPremiumPrice;
  }


  public double getHolidayPremiumPrice()
  {
    return holidayPremiumPrice;
  }
  
  public void setHolidayStartDate(Date holidayStartDate)
  {
    if(valueChanged(this.holidayStartDate, holidayStartDate))
    {
      setChanged(true);
    }
    this.holidayStartDate = holidayStartDate;
  }


  public Date getHolidayStartDate()
  {
    return holidayStartDate;
  }
  
  public void setHolidayEndDate(Date holidayEndDate)
  {
    if(valueChanged(this.holidayEndDate, holidayEndDate))
    {
      setChanged(true);
    }
    this.holidayEndDate = holidayEndDate;
  }


  public Date getHolidayEndDate()
  {
    return holidayEndDate;
  }
  
  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }

    public void setShippingSystem(String shippingSystem) {
        if(valueChanged(this.shippingSystem, shippingSystem))
        {
          setChanged(true);
        }
        this.shippingSystem = shippingSystem;
    }

    public String getShippingSystem() {
        return shippingSystem;
    }

    public void setOver21(boolean over21) {
        this.over21 = over21;
    }

    public boolean isOver21() {
        return over21;
    }

    public void setExpressOnly(boolean expressOnly) {
        this.expressOnly = expressOnly;
    }

    public boolean isExpressOnly() {
        return expressOnly;
    }
}
