package com.ftd.ship.vo;

public class VendorProductVO extends BaseVO {
    private String vendorId;
    private String vendorCode;
    private String productSkuId;
    private double vendorCost;
    private String vendorSku;
    private boolean available;
    private boolean removed;
    
    public VendorProductVO() {
    }

    public void setVendorId(String vendorId) {
        if (valueChanged(this.vendorId, vendorId)) {
            setChanged(true);
        }
        this.vendorId = vendorId;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setProductSkuId(String productSkuId) {
        if (valueChanged(this.productSkuId, productSkuId)) {
            setChanged(true);
        }
        this.productSkuId = productSkuId;
    }

    public String getProductSkuId() {
        return productSkuId;
    }

    public void setVendorCost(double vendorCost) {
        if (valueChanged(this.vendorCost, vendorCost)) {
            setChanged(true);
        }
        this.vendorCost = vendorCost;
    }

    public double getVendorCost() {
        return vendorCost;
    }

    public void setVendorSku(String vendorSku) {
        if (valueChanged(this.vendorSku, vendorSku)) {
            setChanged(true);
        }
        this.vendorSku = vendorSku;
    }

    public String getVendorSku() {
        return vendorSku;
    }

    public void setAvailable(boolean available) {
        if (valueChanged(this.available, available)) {
            setChanged(true);
        }
        this.available = available;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    public String getVendorCode() {
        return vendorCode;
    }

  public void setRemoved(boolean removed)
  {
    if (valueChanged(this.removed, removed)) 
      setChanged(true);

    this.removed = removed;
  }

  public boolean isRemoved()
  {
    return removed;
  }
  
}
