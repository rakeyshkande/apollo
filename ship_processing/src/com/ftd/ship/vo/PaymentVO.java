package com.ftd.ship.vo;

import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class PaymentVO extends BaseVO
{

  private	long	    paymentId;
  private	String	  orderGuid;
  private	String    additionalBillId;
  private	String	  paymentType;
  private	long	    ccId;
  private	String	  authResult;
  private	String	  authNumber;
  private String    avsCode;
  private String    acqReferenceNumber;
  private String    gcCouponNumber;
  private String    authOverrideFlag;
  private	double	  creditAmount;
  private	double	  debitAmount;
  private String    paymentIndicator;
  private String    refundId;  
  private String    authFlag;
  // new fields for getCCPayments //
  private Date      authDate;
  private String    authCharIndicator;
  private String    transactionId;
  private String    validationCode;
  private String    authSourceCode;
  private String    responseCode;
  private String    aafesTicketNumber;
  
  public PaymentVO()
  {
  }

  public void setPaymentId(long paymentId)
  {
    if(valueChanged(this.paymentId, paymentId))
    {
      setChanged(true);
    }
    this.paymentId = paymentId;
  }

  public long getPaymentId()
  {
    return paymentId;
  }

  public void setOrderGuid(String orderGuid)
  {
    if(valueChanged(this.orderGuid, orderGuid))
    {
      setChanged(true);
    }
    this.orderGuid = trim(orderGuid);
  }

  public String getOrderGuid()
  {
    return orderGuid;
  }

  public void setAdditionalBillId(String additionalBillId)
  {
    if(valueChanged(this.additionalBillId, additionalBillId))
    {
      setChanged(true);
    }
    this.additionalBillId = additionalBillId;
  }

  public String getAdditionalBillId()
  {
    return additionalBillId;
  }

  public void setPaymentType(String paymentType)
  {
    if(valueChanged(this.paymentType, paymentType))
    {
      setChanged(true);
    }
    this.paymentType = trim(paymentType);
  }

  public String getPaymentType()
  {
    return paymentType;
  }

  public void setCcId(long ccId)
  {
    if(valueChanged(this.ccId, ccId))
    {
      setChanged(true);
    }
    this.ccId = ccId;
  }

  public long getCcId()
  {
    return ccId;
  }

  public void setCreditAmount(double creditAmount)
  {
    if(valueChanged(new Double(this.creditAmount), new Double(creditAmount)))
    {
      setChanged(true);
    }
    this.creditAmount = creditAmount;
  }

  public double getCreditAmount()
  {
    return creditAmount;
  }
  
  public void setDebitAmount(double debitAmount)
  {
    if(valueChanged(new Double(this.debitAmount), new Double(debitAmount)))
    {
      setChanged(true);
    }
    this.debitAmount = debitAmount;
  }

  public double getDebitAmount()
  {
    return debitAmount;
  }

  public void setAuthResult(String authResult)
  {
    if(valueChanged(this.authResult, authResult))
    {
      setChanged(true);
    }
    this.authResult = trim(authResult);
  }

  public String getAuthResult()
  {
    return authResult;
  }

  public void setAuthNumber(String authNumber)
  {
    if(valueChanged(this.authNumber, authNumber))
    {
      setChanged(true);
    }
    this.authNumber = trim(authNumber);
  }

  public String getAuthNumber()
  {
    return authNumber;
  }

  public void setAvsCode(String avsCode)
  {
    if(valueChanged(this.avsCode, avsCode))
    {
      setChanged(true);
    }
    this.avsCode = trim(avsCode);
  }

  public String getAvsCode()
  {
    return avsCode;
  }
  
  public void setAcqReferenceNumber(String acqReferenceNumber)
  {
    if(valueChanged(this.acqReferenceNumber, acqReferenceNumber))
    {
      setChanged(true);
    }
    this.acqReferenceNumber = trim(acqReferenceNumber);
  }

  public String getAcqReferenceNumber()
  {
    return acqReferenceNumber;
  }
  
  public void setGcCouponNumber(String gcCouponNumber)
  {
    if(valueChanged(this.gcCouponNumber, gcCouponNumber))
    {
      setChanged(true);
    }
    this.gcCouponNumber = trim(gcCouponNumber);
  }

  public String getGcCouponNumber()
  {
    return gcCouponNumber;
  }
  
  public void setAuthOverrideFlag(String authOverrideFlag)
  {
    if(valueChanged(this.authOverrideFlag, authOverrideFlag))
    {
      setChanged(true);
    }
    this.authOverrideFlag = trim(authOverrideFlag);
  }

  public String getAuthOverrideFlag()
  {
    return authOverrideFlag;
  }
  
  public void setAuthFlag(String authFlag)
  {
    if(valueChanged(this.authFlag, authFlag))
    {
      setChanged(true);
    }
    this.authFlag = trim(authFlag);
  }

  public String getAuthFlag()
  {
    return authFlag;
  }
  
  public void setPaymentIndicator(String paymentIndicator)
  {
    if(valueChanged(this.paymentIndicator, paymentIndicator))
    {
      setChanged(true);
    }
    this.paymentIndicator = trim(paymentIndicator);
  }

  public String getPaymentIndicator()
  {
    return paymentIndicator;
  }
  
  public void setRefundId(String refundId)
  {
    if(valueChanged(this.refundId, refundId))
    {
      setChanged(true);
    }
    this.refundId = refundId;
  }

  public String getRefundId()
  {
    return refundId;
  }

  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }


  public void setAuthDate(Date authDate)
  {
    this.authDate = authDate;
  }


  public Date getAuthDate()
  {
    return authDate;
  }


  public void setAuthCharIndicator(String authCharIndicator)
  {
    this.authCharIndicator = authCharIndicator;
  }


  public String getAuthCharIndicator()
  {
    return authCharIndicator;
  }


  public void setTransactionId(String transactionId)
  {
    this.transactionId = transactionId;
  }


  public String getTransactionId()
  {
    return transactionId;
  }


  public void setValidationCode(String validationCode)
  {
    this.validationCode = validationCode;
  }


  public String getValidationCode()
  {
    return validationCode;
  }


  public void setAuthSourceCode(String authSourceCode)
  {
    this.authSourceCode = authSourceCode;
  }


  public String getAuthSourceCode()
  {
    return authSourceCode;
  }


  public void setResponseCode(String responseCode)
  {
    this.responseCode = responseCode;
  }


  public String getResponseCode()
  {
    return responseCode;
  }


  public void setAafesTicketNumber(String aafesTicketNumber)
  {
    this.aafesTicketNumber = aafesTicketNumber;
  }


  public String getAafesTicketNumber()
  {
    return aafesTicketNumber;
  }

}
