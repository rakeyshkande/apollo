package com.ftd.ship.vo;

import java.math.BigDecimal;

import java.sql.Timestamp;

public class CarrierScanVO {
    BigDecimal id;
    String carrierId;
    String scanType;
    String fileName;
    Timestamp timestamp;
    Boolean processed;
    String orderId;
    String trackingNumber;
    
    public CarrierScanVO() {
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setCarrierId(String carrierId) {
        this.carrierId = carrierId;
    }

    public String getCarrierId() {
        return carrierId;
    }

    public void setScanType(String scanType) {
        this.scanType = scanType;
    }

    public String getScanType() {
        return scanType;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setProcessed(Boolean processed) {
        this.processed = processed;
    }

    public Boolean isProcessed() {
        return processed;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }
}
