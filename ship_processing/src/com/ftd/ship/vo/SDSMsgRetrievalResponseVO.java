package com.ftd.ship.vo;


import java.util.ArrayList;
import java.util.List;

public class SDSMsgRetrievalResponseVO extends SDSResponseVO {
    List<VenusMessageVO>  messages;
    
    public SDSMsgRetrievalResponseVO() {
        messages = new ArrayList<VenusMessageVO>();
    }

    public void setMessages(List<VenusMessageVO> messages) {
        this.messages = messages;
    }

    public List<VenusMessageVO> getMessages() {
        return messages;
    }
}
