package com.ftd.ship.vo;

import java.util.Date;
import java.util.List;

public class SDSRateResponseVO extends SDSResponseVO {
    List<SDSZoneJumpTripVO> zoneJumpTrips;
    List<ShipmentOptionVO> shipmentOptions;
    
    public SDSRateResponseVO() {
    }

    public void setZoneJumpTrips(List<SDSZoneJumpTripVO> zoneJumpTrips) {
        this.zoneJumpTrips = zoneJumpTrips;
    }

    public List<SDSZoneJumpTripVO> getZoneJumpTrips() {
        return zoneJumpTrips;
    }

    public void setShipmentOptions(List<ShipmentOptionVO> shipmentOptions) {
        this.shipmentOptions = shipmentOptions;
    }

    public List<ShipmentOptionVO> getShipmentOptions() {
        return this.shipmentOptions;
    }
}
