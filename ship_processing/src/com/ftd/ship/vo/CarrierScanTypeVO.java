package com.ftd.ship.vo;

public class CarrierScanTypeVO {
    private String carrierId;
    private String scanType;
    private Boolean collectScan;
    private Boolean deliveryScan;
    
    public CarrierScanTypeVO() {
    }

    public void setCarrierId(String carrierId) {
        this.carrierId = carrierId;
    }

    public String getCarrierId() {
        return carrierId;
    }

    public void setScanType(String scanType) {
        this.scanType = scanType;
    }

    public String getScanType() {
        return scanType;
    }

    public void setCollectScan(Boolean collectScan) {
        this.collectScan = collectScan;
    }

    public Boolean isCollectScan() {
        return collectScan;
    }

    public void setDeliveryScan(Boolean deliveryScan) {
        this.deliveryScan = deliveryScan;
    }

    public Boolean isDeliveryScan() {
        return deliveryScan;
    }
}
