package com.ftd.ship.vo;

import java.util.Date;

public class QueueItemVO 
{
    private String queueType;
    private String messageType;
    private Date messageTimestamp;
    private String system;
    private String mercuryNumber;
    private String masterOrderNumber;
    private String orderGuid;
    private String orderDetailId;
    private String externalOrderNumber;
    private String mercuryId;
    
    public QueueItemVO()
    {
    }
    
    
    public void setQueueType(String queueType)
    {
        this.queueType = queueType;
    }
    
    
    public String getQueueType()
    {
        return queueType;
    }
    
    
    public void setMessageType(String messageType)
    {
        this.messageType = messageType;
    }
    
    
    public String getMessageType()
    {
        return messageType;
    }
    
    
    public void setMessageTimestamp(Date messageTimestamp)
    {
        this.messageTimestamp = (Date)messageTimestamp.clone();
    }
    
    
    public Date getMessageTimestamp()
    {
        return messageTimestamp;
    }
    
    
    public void setSystem(String system)
    {
        this.system = system;
    }
    
    
    public String getSystem()
    {
        return system;
    }
    
    
    public void setMercuryNumber(String mercuryNumber)
    {
        this.mercuryNumber = mercuryNumber;
    }
    
    
    public String getMercuryNumber()
    {
        return mercuryNumber;
    }
    
    
    public void setMasterOrderNumber(String masterOrderNumber)
    {
        this.masterOrderNumber = masterOrderNumber;
    }
    
    
    public String getMasterOrderNumber()
    {
        return masterOrderNumber;
    }
    
    
    public void setOrderDetailId(String orderDetailId)
    {
        this.orderDetailId = orderDetailId;
    }
    
    
    public String getOrderDetailId()
    {
        return orderDetailId;
    }
    
    
    public void setExternalOrderNumber(String externalOrderNumber)
    {
        this.externalOrderNumber = externalOrderNumber;
    }
    
    
    public String getExternalOrderNumber()
    {
        return externalOrderNumber;
    }
    
    
    public void setOrderGuid(String orderGuid)
    {
        this.orderGuid = orderGuid;
    }
    
    
    public String getOrderGuid()
    {
        return orderGuid;
    }
    
    
    public void setMercuryId(String mercuryId)
    {
        this.mercuryId = mercuryId;
    }
    
    
    public String getMercuryId()
    {
        return mercuryId;
    }
}