package com.ftd.ship.vo;

import java.util.Date;

public class VendorProductOverrideVO {
    String vendorId;
    String productSubcodeId; 
    String carrierId;
    Date deliveryOverrideDate;
    String sdsShipMethod; 
    
    public VendorProductOverrideVO() {
    }

  public void setVendorId(String vendorId)
  {
    this.vendorId = vendorId;
  }

  public String getVendorId()
  {
    return vendorId;
  }

  public void setProductSubcodeId(String productSubcodeId)
  {
    this.productSubcodeId = productSubcodeId;
  }

  public String getProductSubcodeId()
  {
    return productSubcodeId;
  }

  public void setCarrierId(String carrierId)
  {
    this.carrierId = carrierId;
  }

  public String getCarrierId()
  {
    return carrierId;
  }

  public void setDeliveryOverrideDate(Date deliveryOverrideDate)
  {
    this.deliveryOverrideDate = deliveryOverrideDate;
  }

  public Date getDeliveryOverrideDate()
  {
    return deliveryOverrideDate;
  }

  public void setSdsShipMethod(String sdsShipMethod)
  {
    this.sdsShipMethod = sdsShipMethod;
  }

  public String getSdsShipMethod()
  {
    return sdsShipMethod;
  }
}
