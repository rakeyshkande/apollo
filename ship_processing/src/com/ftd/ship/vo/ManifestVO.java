package com.ftd.ship.vo;

public class ManifestVO {
    private String vendorCode;
    private String carrierId;
    private String zoneJumpTrailerNumber;
    
    public ManifestVO() {
    }

    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    public String getVendorCode() {
        return vendorCode;
    }

    public void setCarrierId(String carrierId) {
        this.carrierId = carrierId;
    }

    public String getCarrierId() {
        return carrierId;
    }

    public void setZoneJumpTrailerNumber(String zoneJumpTrailerNumber) {
        this.zoneJumpTrailerNumber = zoneJumpTrailerNumber;
    }

    public String getZoneJumpTrailerNumber() {
        return zoneJumpTrailerNumber;
    }
}
