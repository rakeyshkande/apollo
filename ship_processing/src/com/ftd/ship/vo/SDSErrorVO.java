package com.ftd.ship.vo;

import com.ftd.ship.common.SDSErrorDisposition;

public class SDSErrorVO {
    long errorId;
    String source; //SDS or FEDEX
    SDSErrorDisposition disposition; //REPROCESS OR QUEUE
    boolean pageSupport; //Y OR N
    String orderComment;
    
    public SDSErrorVO() {
    }

    public void setErrorId(long errorId) {
        this.errorId = errorId;
    }

    public long getErrorId() {
        return errorId;
    }

    public void setPageSupport(boolean pageSupport) {
        this.pageSupport = pageSupport;
    }

    public boolean isPageSupport() {
        return pageSupport;
    }

    public void setOrderComment(String orderComment) {
        this.orderComment = orderComment;
    }

    public String getOrderComment() {
        return orderComment;
    }

    public void setDisposition(SDSErrorDisposition disposition) {
        this.disposition = disposition;
    }

    public SDSErrorDisposition getDisposition() {
        return disposition;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSource() {
        return source;
    }
}
