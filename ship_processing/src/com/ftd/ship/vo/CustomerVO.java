package com.ftd.ship.vo;

import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class CustomerVO extends BaseVO {
    private long customerId;
    private String concatId;
    private String firstName;
    private String lastName;
    private String businessName;
    private String address1;
    private String address2;
    private String city;
    private String state;
    private String zipCode;
    private String country;
    private String county;
    private String addressType;
    private char preferredCustomer;
    private char buyerIndicator;
    private char recipientIndicator;
    private String origin;
    private Date firstOrderDate;
    private Date birthday;

    private List customerPhoneVOList;
    private List commentsVOList;
    private List emailVOList;
    private List customerHistoryVOList;
    private List membershipVOList;
    private List directMailVOList;
    private List altContactVOList;
    private List orderVOList;

    public CustomerVO() {
        customerPhoneVOList = new ArrayList();
        commentsVOList = new ArrayList();
        emailVOList = new ArrayList();
        customerHistoryVOList = new ArrayList();
        membershipVOList = new ArrayList();
        directMailVOList = new ArrayList();
        altContactVOList = new ArrayList();
        orderVOList = new ArrayList();
    }

    public void setCustomerId(long customerId) {
        if (valueChanged(this.customerId, customerId)) {
            setChanged(true);
        }
        this.customerId = customerId;
    }

    public long getCustomerId() {
        return customerId;
    }

    public void setConcatId(String concatId) {
        if (valueChanged(this.concatId, concatId)) {
            setChanged(true);
        }
        this.concatId = trim(concatId);
    }

    public String getConcatId() {
        return concatId;
    }

    public void setFirstName(String firstName) {
        if (valueChanged(this.firstName, firstName)) {
            setChanged(true);
        }
        this.firstName = trim(firstName);
    }

    public String getFirstName() {
        return firstName;
    }

    public void setLastName(String lastName) {
        if (valueChanged(this.lastName, lastName)) {
            setChanged(true);
        }
        this.lastName = trim(lastName);
    }

    public String getLastName() {
        return lastName;
    }

    public void setBusinessName(String businessName) {
        if (valueChanged(this.businessName, businessName)) {
            setChanged(true);
        }
        this.businessName = trim(businessName);
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setAddress1(String address1) {
        if (valueChanged(this.address1, address1)) {
            setChanged(true);
        }
        this.address1 = trim(address1);
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress2(String address2) {
        if (valueChanged(this.address2, address2)) {
            setChanged(true);
        }
        this.address2 = trim(address2);
    }

    public String getAddress2() {
        return address2;
    }

    public void setCity(String city) {
        if (valueChanged(this.city, city)) {
            setChanged(true);
        }
        this.city = trim(city);
    }

    public String getCity() {
        return city;
    }

    public void setState(String state) {
        if (valueChanged(this.state, state)) {
            setChanged(true);
        }
        this.state = trim(state);
    }

    public String getState() {
        return state;
    }

    public void setZipCode(String zipCode) {
        if (valueChanged(this.zipCode, zipCode)) {
            setChanged(true);
        }
        this.zipCode = trim(zipCode);
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setCountry(String country) {
        if (valueChanged(this.country, country)) {
            setChanged(true);
        }
        this.country = trim(country);
    }

    public String getCountry() {
        return country;
    }

    public void setCounty(String county) {
        if (valueChanged(this.county, county)) {
            setChanged(true);
        }
        this.county = trim(county);
    }

    public String getCounty() {
        return county;
    }

    public void setAddressType(String addressType) {
        if (valueChanged(this.addressType, addressType)) {
            setChanged(true);
        }
        this.addressType = trim(addressType);
    }

    public String getAddressType() {
        return addressType;
    }

    public void setPreferredCustomer(char preferredCustomer) {
        if (valueChanged(this.preferredCustomer, preferredCustomer)) {
            setChanged(true);
        }
        this.preferredCustomer = preferredCustomer;
    }

    public char getPreferredCustomer() {
        return preferredCustomer;
    }

    public void setBuyerIndicator(char buyerIndicator) {
        if (valueChanged(this.buyerIndicator, buyerIndicator)) {
            setChanged(true);
        }
        this.buyerIndicator = buyerIndicator;
    }

    public char getBuyerIndicator() {
        return buyerIndicator;
    }

    public void setRecipientIndicator(char recipientIndicator) {
        if (valueChanged(this.recipientIndicator, recipientIndicator)) {
            setChanged(true);
        }
        this.recipientIndicator = recipientIndicator;
    }

    public char getRecipientIndicator() {
        return recipientIndicator;
    }

    public void setOrigin(String origin) {
        if (valueChanged(this.origin, origin)) {
            setChanged(true);
        }
        this.origin = trim(origin);
    }

    public String getOrigin() {
        return origin;
    }

    public void setFirstOrderDate(Date firstOrderDate) {
        if (valueChanged(this.firstOrderDate, firstOrderDate)) {
            setChanged(true);
        }
        this.firstOrderDate = firstOrderDate;
    }

    public Date getFirstOrderDate() {
        return firstOrderDate;
    }

    public void setBirthday(Date birthday) {
        if (valueChanged(this.birthday, birthday)) {
            setChanged(true);
        }
        this.birthday = birthday;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setCustomerPhoneVOList(List customerPhoneVOList) {
        if (customerPhoneVOList != null) {
            Iterator it = customerPhoneVOList.iterator();
            while (it.hasNext()) {
                CustomerPhoneVO customerPhoneVO = (CustomerPhoneVO)it.next();
                customerPhoneVO.setChanged(true);
            }
        }
        this.customerPhoneVOList = customerPhoneVOList;
    }

    public List getCustomerPhoneVOList() {
        return customerPhoneVOList;
    }

    public void setCommentsVOList(List commentsVOList) {
        if (commentsVOList != null) {
            Iterator it = commentsVOList.iterator();
            while (it.hasNext()) {
                CommentsVO commentsVO = (CommentsVO)it.next();
                commentsVO.setChanged(true);
            }
        }
        this.commentsVOList = commentsVOList;
    }

    public List getCommentsVOList() {
        return commentsVOList;
    }

    public void setEmailVOList(List emailVOList) {
        if (emailVOList != null) {
            Iterator it = emailVOList.iterator();
            while (it.hasNext()) {
                EmailVO emailVO = (EmailVO)it.next();
                emailVO.setChanged(true);
            }
        }
        this.emailVOList = emailVOList;
    }

    public List getEmailVOList() {
        return emailVOList;
    }

    public void setCustomerHistoryVOList(List customerHistoryVOList) {
        if (customerHistoryVOList != null) {
            Iterator it = customerHistoryVOList.iterator();
            while (it.hasNext()) {
                CustomerHistoryVO customerHistoryVO = 
                    (CustomerHistoryVO)it.next();
                customerHistoryVO.setChanged(true);
            }
        }
        this.customerHistoryVOList = customerHistoryVOList;
    }

    public List getCustomerHistoryVOList() {
        return customerHistoryVOList;
    }

    public void setMembershipVOList(List membershipVOList) {
        if (membershipVOList != null) {
            Iterator it = membershipVOList.iterator();
            while (it.hasNext()) {
                MembershipVO membershipVO = (MembershipVO)it.next();
                membershipVO.setChanged(true);
            }
        }
        this.membershipVOList = membershipVOList;
    }

    public List getMembershipVOList() {
        return membershipVOList;
    }

    public void setDirectMailVOList(List directMailVOList) {
        if (directMailVOList != null) {
            Iterator it = directMailVOList.iterator();
            while (it.hasNext()) {
                DirectMailVO directMailVO = (DirectMailVO)it.next();
                directMailVO.setChanged(true);
            }
        }
        this.directMailVOList = directMailVOList;
    }

    public List getDirectMailVOList() {
        return directMailVOList;
    }

    public void setAltContactVOList(List altContactVOList) {
        if (altContactVOList != null) {
            Iterator it = altContactVOList.iterator();
            while (it.hasNext()) {
                AltContactVO altContactVO = (AltContactVO)it.next();
                altContactVO.setChanged(true);
            }
        }
        this.altContactVOList = altContactVOList;
    }

    public List getAltContactVOList() {
        return altContactVOList;
    }

    public void setOrderVOList(List orderVOList) {
        if (orderVOList != null) {
            Iterator it = orderVOList.iterator();
            while (it.hasNext()) {
                OrderVO orderVO = (OrderVO)it.next();
                orderVO.setChanged(true);
            }
        }
        this.orderVOList = orderVOList;
    }

    public List getOrderVOList() {
        return orderVOList;
    }

    private String trim(String str) {
        return (str != null) ? str.trim() : str;
    }

}
