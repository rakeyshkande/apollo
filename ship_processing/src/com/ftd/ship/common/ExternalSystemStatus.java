package com.ftd.ship.common;

import org.apache.commons.lang.StringUtils;

public enum ExternalSystemStatus { 
    NOTVERIFIED, 
    VERIFIED;

    public static ExternalSystemStatus toExternalSystemStatus(String strValue) {
        if( StringUtils.equals(NOTVERIFIED.toString(),strValue)) {
            return NOTVERIFIED;
        }
        else if( StringUtils.equals(VERIFIED.toString(),strValue)) {
            return VERIFIED;
        }
        
        return null;
    }
}
