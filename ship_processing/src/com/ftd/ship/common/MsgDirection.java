package com.ftd.ship.common;

import org.apache.commons.lang.StringUtils;

public enum MsgDirection { 
    INBOUND, 
    OUTBOUND;
    
    public static MsgDirection toMsgDirection(String strValue) {
        if( StringUtils.equals(MsgDirection.INBOUND.toString(),strValue) ) {
            return MsgDirection.INBOUND;
        } else if( StringUtils.equals(MsgDirection.OUTBOUND.toString(),strValue) ) {
            return MsgDirection.OUTBOUND;
        } 
        
        return null;
    }
}
