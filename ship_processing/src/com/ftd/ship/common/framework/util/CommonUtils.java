package com.ftd.ship.common.framework.util;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.ship.common.JMSPipeline;
import com.ftd.ship.common.MsgType;
import com.ftd.ship.common.ShipConstants;
import com.ftd.ship.common.resources.J2EEResourceProvider;
import com.ftd.ship.common.resources.ResourceProviderBase;
import com.ftd.ship.common.resources.TestResourceProvider;
import com.ftd.ship.dao.OrderDAO;
import com.ftd.ship.dao.ShipDAO;
import com.ftd.ship.vo.GlobalParameterVO;
import com.ftd.ship.vo.VenusMessageVO;

import java.sql.Connection;

import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.NamingException;


/**
 *
 * @author Jason Weiss
 */
public class CommonUtils {
    private static final String CONFIG_FILE = "ship_config.xml";
    private static OrderDAO orderDAO = new OrderDAO();
    private static ShipDAO shipDAO = new ShipDAO();
    private static Logger logger = 
        new Logger("com.ftd.ship.common.framework.util.CommonUtilites");
        
    private static final CommonUtils INSTANCE = new CommonUtils();

    private CommonUtils() {
    }
    
    public static CommonUtils getInstance() {
        return INSTANCE;
    }

    /**
   * This method sends a message to the System Messenger.
   * @param message
   * @return message id
   */
    public String sendSystemMessage(String message) throws Exception {

            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
            String messageSource = 
                configUtil.getProperty(CONFIG_FILE, "MESSAGE_SOURCE");
    
            return sendSystemMessage(messageSource, message);
    }
    
    /**
     * This method sends a message to the System Messenger.
     * @param message
     * @return message id
     */
      public String sendSystemMessage(String source, String message) throws Exception {
          Connection conn = null;
          String messageID = "";
          
          try {
              logger.error("Sending System Message: " + message);
              conn = getResourceProvider().getDatabaseConnection();

              //build system vo
              SystemMessengerVO sysMessage = new SystemMessengerVO();
              sysMessage.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
              sysMessage.setSource(source);
              sysMessage.setType("ERROR");
              sysMessage.setMessage(message);
              sysMessage.setSubject("SHIP PROCESSING ERROR");
      
              SystemMessenger sysMessenger = SystemMessenger.getInstance();
              messageID = sysMessenger.send(sysMessage, conn);
      
              if (messageID == null) {
                  String msg = 
                      "Error occured while attempting to send out a system message. Msg not sent: " + 
                      message;
                  logger.error(msg);
              }
          } finally {
              if( conn!=null ) {
                  try {
                      if( !conn.isClosed() ) {
                          conn.close();
                      }
                  } catch (SQLException sqle) {
                      logger.warn("Error while closing database conneciton.",sqle);
                  }
              }
          }

          return messageID;
      }    

      
      /**
       * This method sends a message to the System Messenger depending on ACTION_TYPE.
       * @param message
       * @return message id
       */
        public String sendSystemMessage(String source, String message, String action_type) throws Exception {
            Connection conn = null;
            String messageID = "";
            
            try {
                logger.error("Sending System Message: " + message);
                conn = getResourceProvider().getDatabaseConnection();

                //build system vo
                SystemMessengerVO sysMessage = new SystemMessengerVO();
                sysMessage.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
                sysMessage.setSource(source);
                sysMessage.setType(action_type);
                sysMessage.setMessage(message);
                //sysMessage.setSubject("SHIP PROCESSING ERROR");
                sysMessage.setSubject(null);
        
                SystemMessenger sysMessenger = SystemMessenger.getInstance();
                messageID = sysMessenger.send(sysMessage, conn);
        
                if (messageID == null) {
                    String msg = 
                        "Error occured while attempting to send out a system message. Msg not sent: " + 
                        message;
                    logger.error(msg);
                }
            } finally {
                if( conn!=null ) {
                    try {
                        if( !conn.isClosed() ) {
                            conn.close();
                        }
                    } catch (SQLException sqle) {
                        logger.warn("Error while closing database conneciton.",sqle);
                    }
                }
            }

            return messageID;
        }
        
    /**
    * Returns a transactional resource from the EJB container.
    *
    * @param jndiName
    * @return
    * @throws javax.naming.NamingException
    */
    public static Object lookupResource(String jndiName) throws NamingException {
        InitialContext initContext = null;
        try {
            initContext = new InitialContext();

            return initContext.lookup(jndiName);
        } finally {
            try {
                initContext.close();
            } catch (Exception ex) {
                logger.error("Error looking up resource.",ex);
            }
        }
    }

     /**
      * Place a text message into a JMS queue
      * @param conn database connection
      * @param message message text
      * @param corrId correlation id
      * @param destination queue table
      * @throws Exception
      */
     public static void sendJMSMessage(Connection conn, 
                                      String message, 
                                      String corrId,
                                      JMSPipeline destination) throws Exception {
        sendJMSMessage(conn, message, corrId, destination, 0);                         
    }

    /**
     * Place a text message into a JMS queue
     * @param conn database connection
     * @param message message text
     * @param corrId correlation id
     * @param destination queue table
     * @param secondsDelay seconds to delay
     * @throws Exception
     */
    public static void sendJMSMessage(Connection conn, 
                                      String message, 
                                      String corrId,
                                      JMSPipeline destination, 
                                      int secondsDelay) throws Exception {
        shipDAO.postAMessage(conn,destination.getTableName(),corrId,message,secondsDelay);
    }
    

    public static GlobalParameterVO getGlobalParameter(Connection connection, 
                                                       String context, 
                                                       String name, 
                                                       String defaultValue) {
        GlobalParameterVO vo = null;
        
        try {
            vo = 
                orderDAO.getGlobalParameter(connection, context, name);
        } catch (Exception e) {
            logger.error("Error while retrieving global parameter " + context + 
                         "/" + name + ".  Will use default value of " + 
                         defaultValue);
            vo = new GlobalParameterVO();
            vo.setContext(context);
            vo.setName(name);
            vo.setValue(defaultValue);
        }
        
        return vo;
    }
    
    public static VenusMessageVO copyVenusMessage(VenusMessageVO sourceMessage, MsgType newMsgType) {
        VenusMessageVO retval = (VenusMessageVO)sourceMessage.clone();
        retval.setMsgType(newMsgType);
        retval.setOperator(null);
        retval.setMessageDirection(null);
        retval.setVenusStatus(null);
        retval.setExternalSystemStatus(null);
        retval.setTrackingNumber(null);
        retval.setPrintedStatusDate(null);
        retval.setShippedStatusDate(null);
        retval.setCancelledStatusDate(null);
        retval.setRejectedStatusDate(null);
        retval.setFinalCarrier(null);
        retval.setFinalCarrier(null);
        retval.setSdsStatus(null);
        retval.setForcedShipment(false);
        retval.setRatedShipment(false);
//        retval.setBatchNumber(null);
//        retval.setBatchSequence(null);
        retval.setLastScanReceived(null);
        retval.setDeliveryScan(null);
        retval.setGetSdsResponse(false);
        
        if( !sourceMessage.getMsgType().equals(MsgType.ORDER) || !newMsgType.equals(MsgType.ORDER) ) {
            retval.setPrice(0.00);
            retval.setOverUnderCharge(0.00);
        }
        
        return retval;
    }
    
    public ResourceProviderBase getResourceProvider() {
        ResourceProviderBase retval;
        if( this.getClass().getClassLoader().getResource(ShipConstants.TEST_PROPERTY_FILE)!=null ) {
            retval = new TestResourceProvider();
        } else {
            retval = new J2EEResourceProvider();
        }
        
        return retval;
    }
}
