package com.ftd.ship.common;

public class ShipConstants {

    public static final String CONFIG_FILE = "ship_config.xml";
    public static final String TEST_PROPERTY_FILE = "ship_test_config.xml";
    public static final String DATASOURCE_NAME = "DATASOURCE";
    public static final String SDS_USER = "SP";
    public static final String ARGO_SYSTEM = "Argo";
    public static final String SHIPPING_METHOD_NEXTDAY = "ND";
    public static final String POC_EMAIL_TYPE = "Email";
    public static final String PRINTED_DISPOSITION = "Printed";  
    public static final String SHIPPED_DISPOSITION = "Shipped";  
    public static final String PROCESSED_DISPOSITION = "Processed";  
    public final static String UPDATED_BY_VL = "SP";
    public static final String PARTNER_MAX_SYSTEM_REJECTS = "MAX_SYSTEM_REJECTS_ON_ORDERS";
    public static final String PARTNER_MAX_VENDOR_REJECTS = "MAX_VENDOR_REJECTS_ON_ORDERS";
    public static final String PARTNER_MAX_CARRIER_REJECTS = "MAX_CARRIER_REJECTS_ON_ORDERS";
    
    /* Pick Ticket xml tags */
    public static final String TICKET_ROOT = "InvoiceData";
    public static final String TICKET_ORDER = "order";
    public static final String TICKET_ORIGINATOR = "origin";
    public static final String TICKET_ORIGINATOR_NAME = "name";
    public static final String TICKET_ORIGINATOR_PHONE = "phone";
    public static final String TICKET_RECIPIENT = "recipient";
    public static final String TICKET_RECIPIENT_NAME = "name";
    public static final String TICKET_RECIPIENT_COMPANY = "company";
    public static final String TICKET_RECIPIENT_ADDR1 = "address1";
    public static final String TICKET_RECIPIENT_ADDR2 = "address2";
    public static final String TICKET_RECIPIENT_CITY = "city";
    public static final String TICKET_RECIPIENT_STATE = "state";
    public static final String TICKET_RECIPIENT_POSTAL = "postal";
    public static final String TICKET_PRODUCT = "product";
    public static final String TICKET_PRODUCT_ID = "id";
    public static final String TICKET_PRODUCT_DESC = "noun";
    public static final String TICKET_SHIP_DATE = "shipDate";
    public static final String TICKET_CARD_MESSAGE = "card";
    public static final String TICKET_CUSTOM = "custom";
    public static final String TICKET_GUARANTEE = "guarantee";
    public static final String TICKET_PERSONAL_GREETING_ID = "personalGreetingID";
    public static final String TICKET_NOTE = "note";

    /* custom parameters returned from database procedure */
    public static final String OUT_STATUS_PARAM = "OUT_STATUS";
    public static final String OUT_MESSAGE_PARAM = "OUT_MESSAGE";
    
    /* global parms */
    public static final String GLOBAL_PARMS_CONTEXT = "SHIPPING_PARMS";
    public static final String GLOBAL_PARMS_MAX_SHIP_DAYS_OUT = "MAX_SHIP_DAYS_OUT";
    public static final String GLOBAL_PARMS_DELAYED_ORDER_DEQUEUE_START_HOUR = "DELAYED_ORDER_DEQUEUE_START_HOUR";
    
    /* misc */
    public final static String VL_NO = "N";
    public final static String VL_YES = "Y";
    public static final String SHIP_PROCESSING_COMMENT_ORIGIN = "ShipProc";
    public static final String COMMENT_TYPE_ORDER = "Order";
    
    //common
    public final static String ORDER_GUID = "ORDER_GUID";
    public final static String SOURCE_CODE = "SOURCE_CODE";
    public final static String COMPANY_ID = "COMPANY_ID";
    public final static String ENABLE_LP_PROCESSING = "ENABLE_LP_PROCESSING";
    public final static String PHONE_NUMBER = "PHONE_NUMBER";
    public final static String ORDER_DETAIL_ID = "ORDER_DETAIL_ID";  
    public final static String DELIVERY_DATE = "DELIVERY_DATE";
    public final static String PRODUCT_ID = "PRODUCT_ID";
    public final static String FLORIST_ID = "FLORIST_ID";
    public final static String CUSTOMER_ID = "CUSTOMER_ID";
    public final static String URL = "URL";
    public final static String BRAND = "SDS_BRAND";
  
    //SP_UPDATE_ORDER_DETAIL
    public final static String RECIPIENT_ID = "RECIPIENT_ID";  
    public final static String QUANTITY = "QUANTITY";
    public final static String COLOR_1 = "COLOR_1";
    public final static String COLOR_2 = "COLOR_2";
    public final static String SUBSTITUTION_INDICATOR = "SUBSTITUTION_INDICATOR";
    public final static String SAME_DAY_GIFT = "SAME_DAY_GIFT";
    public final static String OCCASION = "OCCASION";
    public final static String CARD_MESSAGE = "CARD_MESSAGE";
    public final static String CARD_SIGNATURE = "CARD_SIGNATURE";
    public final static String SPECIAL_INSTRUCTIONS = "SPECIAL_INSTRUCTIONS";
    public final static String RELEASE_INFO_INDICATOR = "RELEASE_INFO_INDICATOR";  
    public final static String SHIP_METHOD = "SHIP_METHOD";
    public final static String SHIP_DATE = "SHIP_DATE";
    public final static String ORDER_DISP_CODE = "ORDER_DISP_CODE";
    public final static String ORDER_HELD = "ORDER_HELD";
    public final static String SECOND_CHOICE_PRODUCT = "SECOND_CHOICE_PRODUCT";
    public final static String ZIP_QUEUE_COUNT = "ZIP_QUEUE_COUNT";
    public final static String RANDOM_WEIGHT_DIST_FAILURES = "RANDOM_WEIGHT_DIST_FAILURES";
    public final static String UPDATED_BY = "UPDATED_BY";
    public final static String DELIVERY_DATE_RANGE_END = "DELIVERY_DATE_RANGE_END";
    public final static String SCRUBBED_ON = "SCRUBBED_ON";
    public final static String SCRUBBED_ON_DATE = "SCRUBBED_ON_DATE";
    public final static String SCRUBBED_BY = "SCRUBBED_BY";
    public final static String USER_ID = "USER_ID";
    public final static String ARIBA_UNSPSC_CODE = "ARIBA_UNSPSC_CODE";
    public final static String ARIBA_PO_NUMBER = "ARIBA_PO_NUMBER";
    public final static String ARIBA_AMS_PROJECT_CODE = "ARIBA_AMS_PROJECT_CODE";
    public final static String ARIBA_COST_CENTER = "ARIBA_COST_CENTER";
    public final static String SIZE_INDICATOR = "SIZE_INDICATOR";
    public final static String MILES_POINTS = "MILES_POINTS";
    public final static String SUBCODE = "SUBCODE";
 
    //SP_GET_ORDER_BY_GUID
    public final static String MASTER_ORDER_NUMBER = "MASTER_ORDER_NUMBER";
    public final static String MEMBERSHIP_ID = "MEMBERSHIP_ID";
    public final static String ORIGIN_ID = "ORIGIN_ID";
    public final static String ORDER_DATE = "ORDER_DATE";
    public final static String ORDER_TOTAL = "ORDER_TOTAL";
    public final static String PRODUCT_TOTAL = "PRODUCT_TOTAL";
    public final static String ADD_ON_TOTAL = "ADD_ON_TOTAL";
    public final static String SERVICE_FEE_TOTAL = "SERVICE_FEE_TOTAL";
    public final static String SHIPPING_FEE_TOTAL = "SHIPPING_FEE_TOTAL";
    public final static String DISCOUNT_TOTAL = "DISCOUNT_TOTAL";
    public final static String TAX_TOTAL = "TAX_TOTAL";
    public final static String LOSS_PREVENTION_INDICATOR = "LOSS_PREVENTION_INDICATOR";
    
    //SP_GET_CUSTOMER  
    public final static String CONCAT_ID = "CONCAT_ID";
    public final static String CUSTOMER_FIRST_NAME = "CUSTOMER_FIRST_NAME";
    public final static String CUSTOMER_LAST_NAME = "CUSTOMER_LAST_NAME";
    public final static String BUSINESS_NAME = "BUSINESS_NAME";
    public final static String CUSTOMER_ADDRESS_1 = "CUSTOMER_ADDRESS_1";
    public final static String CUSTOMER_ADDRESS_2 = "CUSTOMER_ADDRESS_2";
    public final static String CUSTOMER_CITY = "CUSTOMER_CITY";
    public final static String CUSTOMER_STATE = "CUSTOMER_STATE";
    public final static String CUSTOMER_ZIP_CODE = "CUSTOMER_ZIP_CODE";
    public final static String CUSTOMER_COUNTRY = "CUSTOMER_COUNTRY";
  
    //SP_GET_COMPANY  
    public final static String COMPANY_NAME = "COMPANY_NAME";
    public final static String INTERNET_ORIGIN = "INTERNET_ORIGIN";
    public final static String DEFAULT_PROGRAM_ID = "DEFAULT_PROGRAM_ID";
    public final static String CLEARING_MEMBER_NUMBER = "CLEARING_MEMBER_NUMBER";
    public final static String LOGO_FILE_NAME = "LOGO_FILENAME";  
  
    //SP_GET_GLOBAL_PARAMETER  
    public final static String CONTEXT = "CONTEXT";
    public final static String NAME = "NAME";
    
    //SP_GET_PRODUCT_BY_ID
    public final static String PRODUCT_ID_LC = "productId";
    public final static String NOVATOR_ID_LC = "novatorId";
    public final static String PRODUCT_NAME_LC = "productName";
    public final static String NOVATOR_NAME_LC = "novatorName";
    public final static String STATUS_LC = "status";
    public final static String DELIVERY_TYPE_LC = "deliveryType";
    public final static String CATEGORY_LC = "category";
    public final static String PRODUCT_TYPE_LC = "productType";
    public final static String PRODUCT_SUB_TYPE_LC = "productSubType";
    public final static String COLOR_SIZE_FLAG_LC = "colorSizeFlag";
    public final static String STANDARD_PRICE_LC = "standardPrice";
    public final static String DELUXE_PRICE_LC = "deluxePrice";
    public final static String PREMUIM_PRICE_LC = "premiumPrice";
    public final static String PREFERRED_PRICE_POINT_LC = "preferredPricePoint";
    public final static String VARIABLE_PRICE_MAX_LC = "variablePriceMax";
    public final static String SHORT_DESCRIPTION_LC = "shortDescription";
    public final static String LONG_DESCRIPTION_LC = "longDescription";
    public final static String FLORIST_REFERENCE_NUMBER_LC = "floristReferenceNumber";
    public final static String MERCURY_DESCRIPTION_LC = "mercuryDescription";
    public final static String ITEM_COMMENTS_LC = "itemsComments";
    public final static String ADD_ON_BALLOONS_FLAG_LC = "addOnBallonsFlag";
    public final static String ADD_ON_BEARS_FLAG_LC = "addOnBearsFlag";
    public final static String ADD_ON_CARDS_FLAG_LC = "addOnCardsFlag";
    public final static String ADD_ON_FUNERAL_FLAG_LC = "addOnFuneralFlag";
    public final static String CODIFIED_FLAG_LC = "codifiedFlag";
    public final static String EXECPTION_CODE_LC = "codifiedFlag";
    public final static String EXCEPTION_START_DATE_LC = "exceptionStartDate";
    public final static String EXECPTION_END_DATE_LC = "exceptionEndDate";
    public final static String EXCEPTION_MESSAGE_LC = "exceptionMessage";
    public final static String VENDOR_ID_LC = "vendorId";
    public final static String VENDOR_COST_LC = "vendorCost";
    public final static String VENDOR_SKU_LC = "vendorSku";
    public final static String SECOND_CHOICE_CODE_LC = "secondChoiceCode";
    public final static String HOLIDAY_SECOND_CHOICE_CODE_LC = "holidaySecondChoiceCode";
    public final static String DROPSHIP_CODE_LC = "dropshipCode";
    public final static String DISCOUNT_ALLOWED_FLAG_LC = "discountAllowedFlag";
    public final static String DELIVERY_INCLUDED_FLAG_LC = "discountAllowedFlag";
    public final static String TAX_FLAG_LC = "taxFlag";
    public final static String SERVICE_FEE_FLAG_LC = "serviceFeeFlag";
    public final static String EXOTIC_FLAG_LC = "exoticFlag";
    public final static String EGIFT_FLAG_LC = "egiftFlag";
    public final static String COUNTRY_ID_LC = "countryId";
    public final static String ARRANGEMENT_SIZE_LC = "arrangementSize";
    public final static String ARRANGEMENT_COLORS_LC = "arrangementColors";
    public final static String DOMINANT_FLOWERS_LC = "dominantFlowers";
    public final static String SEARCH_PRIORITY_LC = "searchPriority";
    public final static String RECIPE_LC = "recipe";
    public final static String SUBCODE_FLAG_LC = "subcodeFlag";
    public final static String DIM_WEIGHT_LC = "dimWeight";
    public final static String NEXT_DAT_UPGRADE_FLAG_LC = "nextDayUpgradeFlag";
    public final static String CORPORATE_SITE_LC = "corporateSite";
    public final static String UNSPSC_CODE_LC = "unspscCode";
    public final static String PRICE_RANK1_LC = "priceRank1";
    public final static String PRICE_RANK2_LC = "priceRank1";
    public final static String PRICE_RANK3_LC = "priceRank3";
    public final static String SHIP_METHOD_CARRIER_LC = "shipMethodCarrier";
    public final static String SHIP_METHOD_FLORIST_LC = "shipMethodFlorist";
    public final static String SHIPPING_KEY_LC = "shippingKey";
    public final static String VARIABLE_PRICE_FLAG_LC = "variablePriceFlag";
    public final static String HOLIDAY_SKU_LC = "holidaySku";
    public final static String HOLIDAY_PRICE_LC = "holidayPrice";
    public final static String CATALOG_FLAG_LC = "catalogFlag";
    public final static String HOLIDAY_DELUXE_PRICE_LC = "holidayDeluxePrice";
    public final static String HOLIDAY_PREMIUM_PRICE_LC = "holidayPremiumPrice";
    public final static String HOLIDAY_START_DATE_LC = "holidayStartDate";
    public final static String HOLIDAY_END_DATE_LC = "holidayEndDate";
    public final static String DEFAULT_CARRIER_LC = "defaultCarrier";
    public final static String HOLD_UNTIL_AVAILABLE_LC = "holdUntilAvailable";
    public final static String MONDAY_DELIVERY_FRESHCUT_LC = "mondayDeliveryFreshcut";
    public final static String TWO_DAY_SAT_FRESHCUT_LC = "twoDayShipSatFreshcut";
    public final static String VENDOR_TYPE_LC = "vendorType";
    public final static String OVER_21_LC = "over21";
    public final static String EXPRESS_ONLY_LC = "expressShippingOnly";
    public final static String VENDOR_CANCEL_REASON_CODE = "VCN";
    public final static String USE_PARTNER_CARRIER_ACCOUNTS = "USE_PARTNER_CARRIER_ACCOUNTS";
    
    // Email confirmation status
    public final static String DCON_STATUS_PENDING = "Pending";
    public final static String DCON_STATUS_CONFIRMED = "Confirmed";
    
    // Content constants
    public final static String CONTENT_CONTEXT_PREFERRED_PARTNER = "PREFERRED_PARTNER";
    public final static String CONTENT_NAME_PREFERRED_PARTNER = "PHONE";

    public ShipConstants() {
    }
}
