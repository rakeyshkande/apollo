package com.ftd.ship.common.resources;

import org.apache.commons.lang.StringUtils;

public enum SDSStatusParm { 
    CANCELED, LABELED, REJECTED, SHIPPED, CANCELED_NOT_LABELED;

        
    public static SDSStatusParm toSDSStatusParm(String type) {
        if( StringUtils.equals(CANCELED.toString(),type)) {
            return CANCELED;
        }
        if( StringUtils.equals(LABELED.toString(),type)) {
            return LABELED;
        }
        if( StringUtils.equals(REJECTED.toString(),type)) {
            return REJECTED;
        }
        if( StringUtils.equals(SHIPPED.toString(),type)) {
            return SHIPPED;
        }
        if( StringUtils.equals(CANCELED_NOT_LABELED.toString(),type)) {
            return CANCELED_NOT_LABELED;
        }
        
        return null;
    }
}
