package com.ftd.ship.common.resources;


import java.sql.Connection;

import org.apache.commons.lang.StringUtils;

public abstract class ResourceProviderBase {

    /**
     * Create a database connection
     * @return database connection
     * @throws Exception thrown on all errors and exceptions
     */
    public abstract Connection getDatabaseConnection() 
                        throws Exception;

     /** Return the value of the record for the frp.global_parms table
      * @param context filter
      * @param name filter
      * @param defaultValue value to return if no data is found
      * @return the value column returned from the database or the default value
      * @throws Exception for all errors and exceptions
      */
     public String getGlobalParameter( String context, String name, String defaultValue) throws Exception {        
         String retval;
         
         if( StringUtils.isBlank(context) ) {
             throw new Exception("Passed in context is blank or null");
         }

         if( StringUtils.isBlank(name) ) {
             throw new Exception("Passed in name is blank or null");
         }
         
         try {
             retval = getGlobalParameter(context,name);
             
             if( retval==null ) {
                 retval = defaultValue;
             }
         } catch (Exception e) {
             retval = defaultValue;
         }
         
         return retval;
     }

    /**
     * Returns the value column from the frp.global_parms table
     * @param context filter
     * @param name filter
     * @return value of record returned from database or null
     * @throws Exception for all errors and exceptions
     */
    public abstract String getGlobalParameter(String context, 
                                     String name) throws Exception;
}
