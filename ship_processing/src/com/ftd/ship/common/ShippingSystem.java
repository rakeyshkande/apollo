package com.ftd.ship.common;

import org.apache.commons.lang.StringUtils;

public enum ShippingSystem {
    ESCALATE,FTP,SDS;
    
    public static ShippingSystem toShippingSystem(String shippingSystem) {
        if( StringUtils.equals(ESCALATE.toString(),shippingSystem)) {
            return ESCALATE;
        }
        else if( StringUtils.equals(FTP.toString(),shippingSystem)) {
            return FTP;
        }
        else if( StringUtils.equals(SDS.toString(),shippingSystem)) {
            return SDS;
        }
        
        return null;
    }
}
