package com.ftd.ship.common;

import org.apache.commons.lang.StringUtils;

public enum MsgType {
    CANCEL("CAN","CANCELED"),
    PRINT("ANS","LABELED"),
    SHIP("ANS","SHIPPED"),
    ORDER("FTD","shipRequest"),
    REJECT("REJ","REJECTED"),
    ANSWER("ANS","NA"),
    CANCELED_NOT_LABELED("CAN","CANCELED_NOT_LABELED");

    private String ftdMsgType;
    private String sdsMsgType;
    
    private MsgType(String ftdMsgType, String sdsMsgType) {
        this.ftdMsgType=ftdMsgType; 
        this.sdsMsgType=sdsMsgType; 
    }
    
    public String getFtdMsgType(){
        return ftdMsgType;
    }
    
    public String getSdsMsgType() {
        return sdsMsgType;
    }
    
    public boolean equals(MsgType msgType) {
        return ( msgType!=null && 
            StringUtils.equals(this.getFtdMsgType(),msgType.getFtdMsgType()) && 
            StringUtils.equals(this.getSdsMsgType(),msgType.getSdsMsgType()) );
    }
    
    public static MsgType ftdToMsgType(String msgType) {
        if( StringUtils.equals(CANCEL.getFtdMsgType(),msgType)) {
            return CANCEL;
        }
        else if( StringUtils.equals(PRINT.getFtdMsgType(),msgType)) {
            return PRINT;
        }
        else if( StringUtils.equals(SHIP.getFtdMsgType(),msgType)) {
            return SHIP;
        }
        else if( StringUtils.equals(ORDER.getFtdMsgType(),msgType)) {
            return ORDER;
        }
        else if( StringUtils.equals(REJECT.getFtdMsgType(),msgType)) {
            return REJECT;
        }
        else if( StringUtils.equals(ANSWER.getFtdMsgType(),msgType)) {
            return ANSWER;
        }
        
        return null;
    }
    
    public static MsgType sdsToMsgType(String msgType) {
        if( StringUtils.equals(CANCEL.getSdsMsgType(),msgType)) {
            return CANCEL;
        }
        else if( StringUtils.equals(PRINT.getSdsMsgType(),msgType)) {
            return PRINT;
        }
        else if( StringUtils.equals(SHIP.getSdsMsgType(),msgType)) {
            return SHIP;
        }
        else if( StringUtils.equals(REJECT.getSdsMsgType(),msgType)) {
            return REJECT;
        }
        else if( StringUtils.equals(CANCELED_NOT_LABELED.getSdsMsgType(),msgType)) {
            return CANCELED_NOT_LABELED;
        }
        
        return null;
    }
}
