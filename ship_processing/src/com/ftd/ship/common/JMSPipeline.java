package com.ftd.ship.common;

import org.apache.commons.lang.StringUtils;

public enum JMSPipeline { 
    PROCESSSHIP("OJMS.SHIP_PROCESSING"),
    GETSTATUS("OJMS.SDS_GET_STATUS"),
    PROCESSINBOUND("OJMS.SDS_INBOUND_PROCESSING"),
    MANIFEST("OJMS.SDS_MANIFEST"),
    GETSCANS("OJMS.SDS_GET_SCANS"),
    PROCESSSCANS("OJMS.SDS_PROCESS_SCANS"),
    DELIVERYCONFIRM("OJMS.SDS_DELIVERY_CONFIRM"),
    REJECTREVIEW("OJMS.SDS_REVIEW_REJECT"),
    REFUND("OJMS.EM_COM"),
    MY_BUYS("OJMS.MY_BUYS"),
    GETCARRIERZIPS("OJMS.CARRIER_GET_ZIPS"),
    PROCESSCARRIERZIPS("OJMS.CARRIER_PROCESS_ZIPS");
    
    private String tableName;
    
    private JMSPipeline(String tableName) {
        this.tableName = tableName;
    }
    
    public String getTableName() {
        return tableName;
    }
        
    public static JMSPipeline toJMSPipeline(String strValue) {
        if( StringUtils.equals(PROCESSSHIP.toString(),strValue)) {
            return PROCESSSHIP;
        }
        else if( StringUtils.equals(GETSTATUS.toString(),strValue)) {
            return GETSTATUS;
        }
        else if( StringUtils.equals(PROCESSINBOUND.toString(),strValue)) {
            return PROCESSINBOUND;
        }
        else if( StringUtils.equals(MANIFEST.toString(),strValue)) {
            return MANIFEST;
        }
        else if( StringUtils.equals(GETSCANS.toString(),strValue)) {
            return GETSCANS;
        }
        else if( StringUtils.equals(PROCESSSCANS.toString(),strValue)) {
            return PROCESSSCANS;
        }
        else if( StringUtils.equals(DELIVERYCONFIRM.toString(),strValue)) {
            return DELIVERYCONFIRM;
        }
        else if( StringUtils.equals(REJECTREVIEW.toString(),strValue)) {
            return REJECTREVIEW;
        }
        else if( StringUtils.equals(REFUND.toString(),strValue)) {
            return REFUND;
        }
        else if( StringUtils.equals(MY_BUYS.toString(),strValue)) {
            return MY_BUYS;
        }
        else if( StringUtils.equals(GETCARRIERZIPS.toString(),strValue)) {
            return GETCARRIERZIPS;
        }
        else if( StringUtils.equals(PROCESSCARRIERZIPS.toString(),strValue)) {
            return PROCESSCARRIERZIPS;
        }
        
        return null;
    }
}
