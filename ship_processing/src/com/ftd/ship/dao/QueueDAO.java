package com.ftd.ship.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.ship.vo.QueueVO;

import java.sql.Connection;

import java.util.Map;

public class QueueDAO 
{
    private Logger logger;
    
    private static final String LOGGING_CONTEXT = "com.ftd.ship.dao.QueueDAO";
  
    public QueueDAO()
    {
        logger = new Logger(LOGGING_CONTEXT);
    }
  
    public void insertQueueRecord(Connection connection, QueueVO queueRecord) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("INSERT_QUEUE_RECORD");
        
        dataRequest.addInputParam("IN_QUEUE_TYPE", new String(queueRecord.getQueueType()));
        dataRequest.addInputParam("IN_MESSAGE_TYPE", new String(queueRecord.getMessageType()));
        
        java.sql.Timestamp sqlTimeStamp = new java.sql.Timestamp(queueRecord.getMessageTimestamp().getTime());      
        dataRequest.addInputParam("IN_MESSAGE_TIMESTAMP", sqlTimeStamp);
        
        dataRequest.addInputParam("IN_SYSTEM", queueRecord.getSystem() != null ? queueRecord.getSystem() : null);
        dataRequest.addInputParam("IN_MERCURY_NUMBER", queueRecord.getMercuryNumber() != null ? queueRecord.getMercuryNumber() : null);
        dataRequest.addInputParam("IN_MASTER_ORDER_NUMBER", queueRecord.getMasterOrderNumber() != null ? queueRecord.getMasterOrderNumber() : null);
        dataRequest.addInputParam("IN_ORDER_GUID", queueRecord.getOrderGuid() != null ? queueRecord.getOrderGuid() : null);
        dataRequest.addInputParam("IN_ORDER_DETAIL_ID", queueRecord.getOrderDetailId() != null ? queueRecord.getOrderDetailId() : null);
        dataRequest.addInputParam("IN_EXTERNAL_ORDER_NUMBER", queueRecord.getExternalOrderNumber() != null ? queueRecord.getExternalOrderNumber() : null);
        dataRequest.addInputParam("IN_MERCURY_ID", queueRecord.getMercuryId());
        dataRequest.addInputParam("IN_POINT_OF_CONTACT_ID", null);
        dataRequest.addInputParam("IN_EMAIL_ADDRESS", null);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
    }
}
