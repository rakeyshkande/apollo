package com.ftd.ship.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.ship.common.ExternalSystemStatus;
import com.ftd.ship.common.MsgDirection;
import com.ftd.ship.common.MsgType;
import com.ftd.ship.common.SDSErrorDisposition;
import com.ftd.ship.common.ShippingSystem;
import com.ftd.ship.common.VenusStatus;
import com.ftd.ship.vo.CarrierScanTypeVO;
import com.ftd.ship.vo.CarrierScanVO;
import com.ftd.ship.vo.CarrierVO;
import com.ftd.ship.vo.CarrierZipVO;
import com.ftd.ship.vo.ManifestVO;
import com.ftd.ship.vo.ProductNotificationVO;
import com.ftd.ship.vo.QueueVO;
import com.ftd.ship.vo.SDSErrorVO;
import com.ftd.ship.vo.SDSMessageVO;
import com.ftd.ship.vo.SDSRateResponseVO;
import com.ftd.ship.vo.SDSTransactionVO;
import com.ftd.ship.vo.SDSZoneJumpTripVO;
import com.ftd.ship.vo.ShipMethodVO;
import com.ftd.ship.vo.ShipVendorProductVO;
import com.ftd.ship.vo.ShipmentOptionVO;
import com.ftd.ship.vo.VenusMessageVO;

import java.io.IOException;

import java.math.BigDecimal;

import java.sql.Clob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import org.xml.sax.SAXException;


/**
 * VenusDAO
 * This is the DAO that handles loading and updating Venus information
 */

public class ShipDAO 
{
    //private Connection connection;
    private static Logger logger  = new Logger("com.ftd.ship.dao.ShipDAO");
    
    /**
    * Constructor
    */
    public ShipDAO()
    {
    }
    
    /**
    * Returns the carriers asscociated to the vendor for the venus order
    * @param connection database connection
    * @param venusId The String which contains the venus Id for the order
    * @throws Exception
    * @return list of CarrierVOs for the vendor of the order 
    */
    public List<CarrierVO> loadVendorCarriers(Connection connection, String venusId) throws Exception
    {
        CarrierVO carrier = null;
        List<CarrierVO> carrierList = new ArrayList<CarrierVO>();
        
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        
        dataRequest.setStatementID("SHIP_VIEW_VNDR_CARRIER_BY_VENUS_ID");
        dataRequest.addInputParam("IN_VENUS_ID", venusId);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        
        while(rs.next())
        {
            carrier = new CarrierVO();
            carrier.setCarrierId(rs.getObject(2).toString());
            carrier.setRatio(rs.getObject(3) != null?new Integer(rs.getObject(3).toString()):new Integer(0));
            carrier.setCarrierName(rs.getObject(4).toString());
            carrier.setGroundFlag(rs.getObject(5).toString());
            carrier.setReferenceNumber(rs.getObject(6).toString());
            
            carrierList.add(carrier);
        }
        
        return carrierList;    
    }

    /**
    * Updates the venus status
    * 
    * @param connection Connection
    * @param venusId The String which contains the venus Id for the order
    * @param venusStatus The String which contains the updated status
    * @throws Exception 
    */
    public void updateVenusStatus(Connection connection, String venusId, VenusStatus venusStatus) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("SHIP_UPDATE_VENUS_STATUS");
        dataRequest.addInputParam("IN_VENUS_ID", venusId);
        dataRequest.addInputParam("IN_VENUS_STATUS", venusStatus.toString());
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
    }

    /**
    * Updates the venus status
    * 
    * @param connection Connection
    * @param venusMessageVO Object containing the status changes
    * @throws Exception 
    */
    public void updateVenusStatus(Connection connection, VenusMessageVO venusMessageVO) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("SHIP_UPDATE_VENUS_EXTERNAL_STATUS");
        dataRequest.addInputParam("IN_VENUS_ID", venusMessageVO.getVenusId());
        dataRequest.addInputParam("IN_VENUS_STATUS", venusMessageVO.getVenusStatus().toString());
        dataRequest.addInputParam("IN_EXTERNAL_SYSTEM_STATUS", venusMessageVO.getExternalSystemStatus().toString());
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
    }

    /**
    * Updates the venus status
    * 
    * @param connection Connection
    * @param venusMessageVO Value object containing the values to be updated
    * @throws Exception 
    */
    public void updateVenusSDS(Connection connection, VenusMessageVO venusMessageVO) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("SHIP_UPDATE_VENUS_SDS");
        dataRequest.addInputParam("IN_VENUS_ID", venusMessageVO.getVenusId());
        dataRequest.addInputParam("IN_VENUS_STATUS", venusMessageVO.getVenusStatus().toString());
        dataRequest.addInputParam("IN_FILLING_VENDOR", venusMessageVO.getFillingVendor());
        dataRequest.addInputParam("IN_PRICE", new BigDecimal(venusMessageVO.getPrice()));
        dataRequest.addInputParam("IN_MESSAGE_TEXT", venusMessageVO.getMessageText());
        dataRequest.addInputParam("IN_ORDER_PRINTED", venusMessageVO.getOrderPrinted() == null ? null : StringUtils.equals(venusMessageVO.getOrderPrinted(),"Y")?"Y":"N");
        dataRequest.addInputParam("IN_ERROR_DESCRIPTION", venusMessageVO.getErrorDescription());
        dataRequest.addInputParam("IN_CANCEL_REASON_CODE", venusMessageVO.getCancelReasonCode());
        dataRequest.addInputParam("IN_VENDOR_SKU", venusMessageVO.getVendorSKU());
        dataRequest.addInputParam("IN_EXTERNAL_SYSTEM_STATUS", venusMessageVO.getExternalSystemStatus().toString());
        dataRequest.addInputParam("IN_VENDOR_ID", venusMessageVO.getVendorId());
        dataRequest.addInputParam("IN_TRACKING_NUMBER", venusMessageVO.getTrackingNumber());
        dataRequest.addInputParam("IN_PRINTED_DATE", venusMessageVO.getPrintedStatusDate() == null ? null : new java.sql.Timestamp(venusMessageVO.getPrintedStatusDate().getTime()));
        dataRequest.addInputParam("IN_SHIPPED_DATE", venusMessageVO.getShippedStatusDate() == null ? null : new java.sql.Timestamp(venusMessageVO.getShippedStatusDate().getTime()));
        dataRequest.addInputParam("IN_CANCELLED_DATE", venusMessageVO.getCancelledStatusDate() == null ? null : new java.sql.Timestamp(venusMessageVO.getCancelledStatusDate().getTime()));
        dataRequest.addInputParam("IN_REJECTED_DATE", venusMessageVO.getRejectedStatusDate() == null ? null : new java.sql.Timestamp(venusMessageVO.getRejectedStatusDate().getTime()));
        dataRequest.addInputParam("IN_FINAL_CARRIER", venusMessageVO.getFinalCarrier());
        dataRequest.addInputParam("IN_FINAL_SHIP_METHOD", venusMessageVO.getFinalShipMethod());
        dataRequest.addInputParam("IN_FORCED_SHIPMENT", venusMessageVO.isForcedShipment()?"Y":"N");
        dataRequest.addInputParam("IN_RATED_SHIPMENT", venusMessageVO.isRatedShipment()?"Y":"N");
        dataRequest.addInputParam("IN_GET_SDS_RESPONSE", venusMessageVO.isGetSdsResponse()?"Y":"N");
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
    }
    
    /**
    * Updates the carrier delivery method on order details to the carrier delivery
    * method asssociated to the apollo delivery method
    * 
    * @param connection Connection
    * @param orderDetailId The String containing the order detail id
    * @param carrier The String containing the carrier id
    * @throws Exception
    */
    public void updateCarrierDeliveryMethod(Connection connection, String orderDetailId, String carrier) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("SHIP_UPDATE_CARRIER_DELIVERY");
        dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);
        dataRequest.addInputParam("IN_CARRIER", carrier);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
    }
    
    /**
    * Returns a QueueItemVO for the specific venus id for passing to a queue
    * 
    * @param connection Connection
    * @param venusId The String which contains the venus Id for the order
    * @throws Exception
    * @return QueueItemVO Populated queue item to insert into queue 
    */
    public QueueVO loadQueueItem(Connection connection, String venusId) throws Exception
    {
        QueueVO queueItem = null;
        
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        
        dataRequest.setStatementID("SHIP_VIEW_Q_ITEM_DATA_BY_VENUS_ID");
        dataRequest.addInputParam("IN_VENUS_ID", venusId);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        while(rs.next())
        {
            queueItem = new QueueVO();
            queueItem.setMessageTimestamp(new Date());
            queueItem.setMercuryNumber(rs.getObject(1).toString());
            queueItem.setOrderDetailId(rs.getObject(2).toString());
            queueItem.setOrderGuid(rs.getObject(3).toString());
            queueItem.setExternalOrderNumber(rs.getObject(4).toString());
            queueItem.setMasterOrderNumber(rs.getObject(5).toString());
            queueItem.setMercuryId(venusId);
        }
        
        return queueItem;    
    }
    
    /**
    * Returns a VenusMessageVO for the specific venus id
    * 
    * @param connection Connection
    * @param venusId The String which contains the venus Id for the order
    * @throws Exception
    * @return VenusMessageVO Populated venus message 
    */
    public VenusMessageVO getVenusMessage(Connection connection, String venusId) throws Exception
    {
        VenusMessageVO venusMessage = null;
        
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        
        dataRequest.setStatementID("SHIP_GET_VENUS_MESSAGE_BY_VENUS_ID");
        dataRequest.addInputParam("IN_VENUS_ID", venusId);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        while(rs.next())
        {
            venusMessage = new VenusMessageVO();
            int fieldCtr = 0;
            venusMessage.setVenusId(rs.getString(++fieldCtr));
            venusMessage.setVenusOrderNumber(rs.getString(++fieldCtr));
            venusMessage.setVenusStatus(VenusStatus.toVenusStatus(rs.getString(++fieldCtr)));
            venusMessage.setMsgType(MsgType.ftdToMsgType(rs.getString(++fieldCtr)));
            venusMessage.setSendingVendor(rs.getString(++fieldCtr));
            venusMessage.setFillingVendor(rs.getString(++fieldCtr));
            venusMessage.setOrderDate(rs.getDate(++fieldCtr));
            venusMessage.setRecipient(rs.getString(++fieldCtr));
            venusMessage.setBusinessName(rs.getString(++fieldCtr));
            venusMessage.setAddress1(rs.getString(++fieldCtr));
            venusMessage.setAddress2(rs.getString(++fieldCtr));
            venusMessage.setCity(rs.getString(++fieldCtr));
            venusMessage.setState(rs.getString(++fieldCtr));
            venusMessage.setZip(rs.getString(++fieldCtr));
            venusMessage.setCountry(rs.getString(++fieldCtr));
            venusMessage.setPhoneNumber(rs.getString(++fieldCtr));
            venusMessage.setDeliveryDate(rs.getDate(++fieldCtr));
            venusMessage.setFirstChoice(rs.getString(++fieldCtr));
            BigDecimal decimalValue = rs.getBigDecimal(++fieldCtr);
            if( decimalValue==null ) {
                venusMessage.setPrice(0.00);
            } else {
                venusMessage.setPrice(decimalValue.doubleValue());
            }
            venusMessage.setCardMessage(rs.getString(++fieldCtr));
            venusMessage.setShipDate(rs.getDate(++fieldCtr));
            venusMessage.setOperator(rs.getString(++fieldCtr));
            venusMessage.setComments(rs.getString(++fieldCtr));
            venusMessage.setTransmissionTime(rs.getDate(++fieldCtr));
            venusMessage.setReferenceNumber(rs.getString(++fieldCtr));
            venusMessage.setProductId(rs.getString(++fieldCtr));
            venusMessage.setCombinedReportNumber(rs.getString(++fieldCtr));
            venusMessage.setRofNumber(rs.getString(++fieldCtr));
            venusMessage.setAdjReasonCode(rs.getString(++fieldCtr));
            decimalValue = rs.getBigDecimal(++fieldCtr);
            if( decimalValue==null ) {
                venusMessage.setOverUnderCharge(0.00);
            } else {
                venusMessage.setOverUnderCharge(decimalValue.doubleValue());
            }
            venusMessage.setProcessedIndicator(rs.getString(++fieldCtr));
            venusMessage.setMessageText(rs.getString(++fieldCtr));
            venusMessage.setSendToVenus(rs.getString(++fieldCtr));
            venusMessage.setOrderPrinted(rs.getString(++fieldCtr));
            venusMessage.setSubType(rs.getString(++fieldCtr));
            venusMessage.setErrorDescription(rs.getString(++fieldCtr));
            venusMessage.setCancelReasonCode(rs.getString(++fieldCtr));
            venusMessage.setCompOrder(rs.getString(++fieldCtr));
            decimalValue = rs.getBigDecimal(++fieldCtr);
            if( decimalValue==null ) {
                venusMessage.setOldPrice(0.00);
            } else {
                venusMessage.setOldPrice(decimalValue.doubleValue());
            }
            venusMessage.setShipMethod(rs.getString(++fieldCtr));
            venusMessage.setShippingSystem(ShippingSystem.toShippingSystem(rs.getString(++fieldCtr)));
            venusMessage.setTrackingNumber(rs.getString(++fieldCtr));
            venusMessage.setPrintedStatusDate(rs.getDate(++fieldCtr));
            venusMessage.setShippedStatusDate(rs.getDate(++fieldCtr));
            venusMessage.setCancelledStatusDate(rs.getDate(++fieldCtr));
            venusMessage.setFinalCarrier(rs.getString(++fieldCtr));
            venusMessage.setFinalShipMethod(rs.getString(++fieldCtr));
            venusMessage.setSdsStatus(rs.getString(++fieldCtr));
            venusMessage.setForcedShipment(StringUtils.equals("Y",rs.getString(++fieldCtr)));
            venusMessage.setRatedShipment(StringUtils.equals("Y",rs.getString(++fieldCtr)));
            venusMessage.setCountry(rs.getString(++fieldCtr));
            venusMessage.setProductDescription(rs.getString(++fieldCtr));
            venusMessage.setProductWeight(rs.getString(++fieldCtr));
            venusMessage.setRejectedStatusDate(rs.getDate(++fieldCtr));
            venusMessage.setVendorSKU(rs.getString(++fieldCtr));
            venusMessage.setExternalSystemStatus(ExternalSystemStatus.toExternalSystemStatus(rs.getString(++fieldCtr)));
            venusMessage.setVendorId(rs.getString(++fieldCtr));
            venusMessage.setGetSdsResponse(StringUtils.equals("Y",rs.getString(++fieldCtr)));
            venusMessage.setOrderOrigin(rs.getString(++fieldCtr));
            venusMessage.setZoneJumpFlag(rs.getString("ZONE_JUMP_FLAG") == null ? false : true);
            venusMessage.setZoneJumpLabelDate(getUtilDate(rs.getObject("ZONE_JUMP_LABEL_DATE")));
            venusMessage.setZoneJumpTrailerNumber(rs.getString("ZONE_JUMP_TRAILER_NUMBER"));
            venusMessage.setInvTrkCountDate(rs.getDate("INV_TRK_COUNT_DATE"));
            venusMessage.setInvTrkAssignedCountDate(rs.getDate("INV_TRK_ASSIGNED_COUNT_DATE"));
            venusMessage.setPersonalGreetingID(rs.getString("PERSONAL_GREETING_ID"));
        }
        
        return venusMessage;    
    }
    
    /**
    * Updates the venus comment
    * 
    * @param connection Connection
    * @param venusId The String containing the venus id
    * @param comment The String containing the carrier id
    * @throws Exception
    */
    public void updateVenusComment(Connection connection, String venusId, String comment) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("SHIP_UPDATE_VENUS_COMMENT");
        dataRequest.addInputParam("IN_VENUS_ID", venusId);
        dataRequest.addInputParam("IN_COMMENT", comment);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
    }


     

       /**
      * This method serves as a wrapper for the VENUS.VENUS_PKG.INSERT_VENUS_MESSAGE
      * stored procedure. (Taken from com.ftd.op.venus.dao.VenusDAO) 
      * 
      * @param venusMessage
      * @throws Exception
      */
    public String insertVenusMessage(Connection conn, VenusMessageVO venusMessage) throws Exception
    {
        logger.debug("insertVenusMessage(VenusMessageVO venusMessage) :: String ");
        DataRequest dataRequest = new DataRequest();
        
        dataRequest.setStatementID("SHIP_INSERT_VENUS_MESSAGE");
        dataRequest.setConnection(conn);
        
        dataRequest.addInputParam("IN_VENUS_ORDER_NUMBER", venusMessage.getVenusOrderNumber());
        dataRequest.addInputParam("IN_VENUS_STATUS", venusMessage.getVenusStatus().toString());
        dataRequest.addInputParam("IN_MSG_TYPE", venusMessage.getMsgType().getFtdMsgType());
        dataRequest.addInputParam("IN_SENDING_VENDOR", venusMessage.getSendingVendor());
        dataRequest.addInputParam("IN_FILLING_VENDOR", venusMessage.getFillingVendor());
        dataRequest.addInputParam("IN_ORDER_DATE", venusMessage.getOrderDate() == null ? null : new java.sql.Timestamp(venusMessage.getOrderDate().getTime()));
        dataRequest.addInputParam("IN_RECIPIENT", venusMessage.getRecipient());
        dataRequest.addInputParam("IN_BUSINESS_NAME", venusMessage.getBusinessName());
        dataRequest.addInputParam("IN_ADDRESS_1", venusMessage.getAddress1());
        dataRequest.addInputParam("IN_ADDRESS_2", venusMessage.getAddress2());
        dataRequest.addInputParam("IN_CITY", venusMessage.getCity());
        dataRequest.addInputParam("IN_STATE", venusMessage.getState());
        dataRequest.addInputParam("IN_ZIP", venusMessage.getZip());
        dataRequest.addInputParam("IN_PHONE_NUMBER", venusMessage.getPhoneNumber());
        dataRequest.addInputParam("IN_DELIVERY_DATE", venusMessage.getDeliveryDate() == null ? null : new java.sql.Date(venusMessage.getDeliveryDate().getTime()));
        dataRequest.addInputParam("IN_FIRST_CHOICE", venusMessage.getFirstChoice());
        dataRequest.addInputParam("IN_PRICE", venusMessage.getPrice() == null? null: new BigDecimal(venusMessage.getPrice()));
        dataRequest.addInputParam("IN_CARD_MESSAGE", venusMessage.getCardMessage());
        dataRequest.addInputParam("IN_SHIP_DATE", venusMessage.getShipDate()== null ? null :new java.sql.Date(venusMessage.getShipDate().getTime()));
        dataRequest.addInputParam("IN_OPERATOR", venusMessage.getOperator());
        dataRequest.addInputParam("IN_COMMENTS", venusMessage.getComments());
        dataRequest.addInputParam("IN_TRANSMISSION_TIME", venusMessage.getTransmissionTime() == null ? null : new java.sql.Timestamp(venusMessage.getTransmissionTime().getTime()));
        dataRequest.addInputParam("IN_REFERENCE_NUMBER", venusMessage.getReferenceNumber());
        dataRequest.addInputParam("IN_PRODUCT_ID", venusMessage.getProductId());
        dataRequest.addInputParam("IN_COMBINED_REPORT_NUMBER", venusMessage.getCombinedReportNumber());
        dataRequest.addInputParam("IN_ROF_NUMBER", venusMessage.getRofNumber());
        dataRequest.addInputParam("IN_ADJ_REASON_CODE", venusMessage.getAdjReasonCode());
        dataRequest.addInputParam("IN_OVER_UNDER_CHARGE", venusMessage.getOverUnderCharge() == null? null: new BigDecimal(venusMessage.getOverUnderCharge()));
        dataRequest.addInputParam("IN_PROCESSED_INDICATOR", venusMessage.getProcessedIndicator());    
        dataRequest.addInputParam("IN_MESSAGE_TEXT", venusMessage.getMessageText());
        dataRequest.addInputParam("IN_SEND_TO_VENUS", venusMessage.getSendToVenus());
        dataRequest.addInputParam("IN_ORDER_PRINTED", getBoolean(venusMessage.isPrinted()));
        dataRequest.addInputParam("IN_SUB_TYPE", venusMessage.getSubType());
        dataRequest.addInputParam("IN_ERROR_DESCRIPTION", venusMessage.getErrorDescription());
        dataRequest.addInputParam("IN_COUNTRY", venusMessage.getCountry());
        dataRequest.addInputParam("IN_CANCEL_REASON_CODE", venusMessage.getCancelReasonCode());
        dataRequest.addInputParam("IN_COMP_ORDER", venusMessage.getCompOrder());  
        dataRequest.addInputParam("IN_OLD_PRICE", venusMessage.getOldPrice() == null ? null : new BigDecimal(venusMessage.getOldPrice()));
        dataRequest.addInputParam("IN_SHIP_METHOD", venusMessage.getShipMethod());
        dataRequest.addInputParam("IN_VENDOR_SKU", venusMessage.getVendorSKU());
        dataRequest.addInputParam("IN_PRODUCT_DESCRIPTION", venusMessage.getProductDescription());
        dataRequest.addInputParam("IN_PRODUCT_WEIGHT", venusMessage.getProductWeight());    
        dataRequest.addInputParam("IN_MESSAGE_DIRECTION", venusMessage.getMessageDirection().toString());
        dataRequest.addInputParam("IN_EXTERNAL_SYSTEM_STATUS", venusMessage.getExternalSystemStatus().toString());  
        dataRequest.addInputParam("IN_SHIPPING_SYSTEM", venusMessage.getShippingSystem().toString()); 
        dataRequest.addInputParam("IN_TRACKING_NUMBER", venusMessage.getTrackingNumber()); 
        dataRequest.addInputParam("IN_PRINTED", venusMessage.getPrintedStatusDate() == null ? null : new java.sql.Timestamp(venusMessage.getPrintedStatusDate().getTime()));
        dataRequest.addInputParam("IN_SHIPPED", venusMessage.getShippedStatusDate() == null ? null : new java.sql.Timestamp(venusMessage.getShippedStatusDate().getTime()));
        dataRequest.addInputParam("IN_CANCELLED", venusMessage.getCancelledStatusDate() == null ? null : new java.sql.Timestamp(venusMessage.getCancelledStatusDate().getTime()));
        dataRequest.addInputParam("IN_FINAL_CARRIER", venusMessage.getFinalCarrier()); 
        dataRequest.addInputParam("IN_FINAL_SHIP_METHOD", venusMessage.getFinalShipMethod()); 
        dataRequest.addInputParam("IN_REJECTED", venusMessage.getRejectedStatusDate() == null ? null : new java.sql.Timestamp(venusMessage.getRejectedStatusDate().getTime()));
        dataRequest.addInputParam("IN_VENDOR_ID", venusMessage.getVendorId()); 
        dataRequest.addInputParam("IN_PERSONAL_GREETING_ID", venusMessage.getPersonalGreetingID()); 
        dataRequest.addInputParam("IN_NEW_SHIPPING_SYSTEM", venusMessage.getNewShippingSystem());
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
         String message = (String) outputs.get("OUT_MESSAGE");
         throw new Exception(message);
        }
        
        return ((String)outputs.get("OUT_VENUS_ID"));
    }

      /**
     *  This method serves as a wrapper for the VENUS.VENUS_PKG.UPDATE_VENUS
     *  stored procedure. Updates the venus record. 
     *  (Taken from com.ftd.op.venus.dao.VenusDAO)
     * 
     * @param conn database connection
     * @param venusMessageVO to be saved
     * @throws Exception
     */

    /**
     * Returns the number of times a message has been sent
     * (Taken from com.ftd.op.venus.dao.VenusDAO)
     * 
     * @param conn database connection
     * @param externalOrderNumber referenced order
     * @param messageType type of message
     * @return
     * @throws Exception
     */
    public int getNumberOfTimesSent(Connection conn, String externalOrderNumber, String messageType) throws Exception
    {
        logger.debug("getNumberOfTimesSent(String externalOrderNumber ("+externalOrderNumber+"), String messageType("+messageType+"))");
        DataRequest dataRequest = null;
        int retVal = 0;
        
        dataRequest = new DataRequest();
        dataRequest.reset();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("SHIP_GET_NUM_OF_TIMES_MESSAGE_SENT");
        dataRequest.addInputParam("IN_EXTERNAL_ORDER_NUMBER", externalOrderNumber);
        dataRequest.addInputParam("IN_MESSAGE_TYPE", messageType);
        
        retVal = Integer.parseInt(DataAccessUtil.getInstance().execute(dataRequest).toString());
        
        return retVal;
    }
  
  /**
     This method the master product id for the passed in product id/sub code.
     * (Taken from com.ftd.op.venus.dao.VenusDAO)
     * 
     * @param conn database connection
     * @param productId to look up.
     * @return product id from product master table
     * @throws java.lang.Exception
     */
    public String getProductMasterId(Connection conn, String productId) throws Exception
    {
        String masterProductId = productId;
        CachedResultSet results = null;
        
        DataRequest dataRequest = new DataRequest();
        dataRequest.reset();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("SHIP_GET_PRODUCT_BY_ANY_ID");
        dataRequest.addInputParam("IN_PRODUCT_ID", productId);
        
        //Map outputs = (Map) DataAccessUtil.getInstance().execute(dataRequest);
        
        //results = (CachedResultSet)outputs.get("OUT_CUR");
        results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);
        
        if ( results != null && results.next() ) {
            masterProductId = results.getString("productId");
        }
        
        return masterProductId;
    }
    
  /**
   * This method serves as a wrapper for the VENUS.VENUS_PKG.GET_VENUS_MESSAGE
   * stored procedure. 
   * (Taken from com.ftd.op.venus.dao.VenusDAO)
   * 
   * @param conn database connection
   * @param venusOrderNumber filter
   * @param messageType filter
   * @return List messages
   * @throws Exception
   */
  
  public List<VenusMessageVO> getVenusMessage(Connection conn, String venusOrderNumber, String messageType) throws Exception
  {
    CachedResultSet crs = null;  
    ArrayList<VenusMessageVO> messages = null;
  
    VenusMessageVO msgVO = null;

    logger.debug("VenusDAO.getVenusMessage(String venusOrderNumber ("+ venusOrderNumber +"), String messageType ("+ messageType +"))");
    DataRequest dataRequest = new DataRequest();
    dataRequest.reset();
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("SHIP_GET_VENUS_MESSAGE");
    dataRequest.addInputParam("IN_VENUS_ORDER_NUMBER", venusOrderNumber);
    dataRequest.addInputParam("IN_MESSAGE_TYPE", messageType);
    crs = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);

    if( crs != null )
    {
      messages = new ArrayList<VenusMessageVO>();
      
      while ( crs.next() )
      {
        msgVO = new VenusMessageVO();
        msgVO.setVenusId((String)crs.getObject("VENUS_ID"));
        msgVO.setVenusOrderNumber((String)crs.getObject("VENUS_ORDER_NUMBER"));
        msgVO.setVenusStatus(VenusStatus.toVenusStatus((String)crs.getObject("VENUS_STATUS")));
        msgVO.setMsgType(MsgType.ftdToMsgType((String)crs.getObject("MSG_TYPE")));
        msgVO.setSendingVendor((String)crs.getObject("SENDING_VENDOR"));
        msgVO.setFillingVendor((String)crs.getObject("FILLING_VENDOR"));
        msgVO.setOrderDate(getUtilDate(crs.getObject("ORDER_DATE")));
        msgVO.setRecipient((String)crs.getObject("RECIPIENT"));
        msgVO.setBusinessName((String)crs.getObject("BUSINESS_NAME"));
        msgVO.setAddress1((String)crs.getObject("ADDRESS_1"));
        msgVO.setAddress2((String)crs.getObject("ADDRESS_2"));
        msgVO.setCity((String)crs.getObject("CITY"));
        msgVO.setState((String)crs.getObject("STATE"));
        msgVO.setZip((String)crs.getObject("ZIP"));
        msgVO.setCountry((String)crs.getObject("COUNTRY"));
        msgVO.setPhoneNumber((String)crs.getObject("PHONE_NUMBER"));
        msgVO.setDeliveryDate(getUtilDate(crs.getObject("DELIVERY_DATE")));
        msgVO.setFirstChoice((String)crs.getObject("FIRST_CHOICE"));
        msgVO.setPrice(new Double(crs.getDouble("PRICE")));
        msgVO.setCardMessage((String)crs.getObject("CARD_MESSAGE"));
        msgVO.setShipDate(getUtilDate(crs.getObject("SHIP_DATE")));
        msgVO.setOperator((String)crs.getObject("OPERATOR"));
        msgVO.setComments((String)crs.getObject("COMMENTS"));
        msgVO.setTransmissionTime(getUtilDate(crs.getObject("TRANSMISSION_TIME")));
        msgVO.setReferenceNumber((String)crs.getObject("REFERENCE_NUMBER"));
        msgVO.setProductId((String)crs.getObject("PRODUCT_ID"));
        msgVO.setCombinedReportNumber((String)crs.getObject("COMBINED_REPORT_NUMBER"));
        msgVO.setRofNumber((String)crs.getObject("ROF_NUMBER"));
        msgVO.setAdjReasonCode((String)crs.getObject("ADJ_REASON_CODE"));
        msgVO.setOverUnderCharge(new Double(crs.getDouble("OVER_UNDER_CHARGE")));
        msgVO.setProcessedIndicator((String)crs.getObject("PROCESSED_INDICATOR"));
        msgVO.setMessageText((String)crs.getObject("MESSAGE_TEXT"));
        msgVO.setSendToVenus((String)crs.getObject("SEND_TO_VENUS"));
        msgVO.setOrderPrinted((String)crs.getObject("ORDER_PRINTED"));
        msgVO.setSubType((String)crs.getObject("SUB_TYPE"));
        msgVO.setErrorDescription((String)crs.getObject("ERROR_DESCRIPTION"));
        msgVO.setCancelReasonCode((String)crs.getObject("CANCEL_REASON_CODE"));
        msgVO.setCompOrder((String)crs.getObject("COMP_ORDER"));
        msgVO.setOldPrice(new Double(crs.getDouble("OLD_PRICE")));
        msgVO.setShipMethod(crs.getString("SHIP_METHOD"));
        msgVO.setVendorSKU(crs.getString("VENDOR_SKU"));
        msgVO.setProductDescription(crs.getString("PRODUCT_DESCRIPTION"));
        msgVO.setProductWeight(crs.getString("PRODUCT_WEIGHT"));
        msgVO.setShippingSystem(ShippingSystem.toShippingSystem(crs.getString("SHIPPING_SYSTEM")));
        msgVO.setTrackingNumber(crs.getString("TRACKING_NUMBER"));
        msgVO.setPrintedStatusDate(getUtilDate(crs.getObject("PRINTED")));
        msgVO.setShippedStatusDate(getUtilDate(crs.getObject("SHIPPED")));
        msgVO.setCancelledStatusDate(getUtilDate(crs.getObject("CANCELLED")));
        msgVO.setFinalCarrier(crs.getString("FINAL_CARRIER"));
        msgVO.setFinalShipMethod(crs.getString("FINAL_SHIP_METHOD"));
        msgVO.setSdsStatus(crs.getString("SDS_STATUS"));
        msgVO.setVendorId(crs.getString("VENDOR_ID"));
        msgVO.setRejectedStatusDate(getUtilDate(crs.getObject("REJECTED")));
        msgVO.setGetSdsResponse(StringUtils.equals(crs.getString("GET_SDS_RESPONSE"),"Y"));
        msgVO.setExternalSystemStatus(ExternalSystemStatus.toExternalSystemStatus(crs.getString("EXTERNAL_SYSTEM_STATUS")));
        msgVO.setZoneJumpFlag(crs.getString("ZONE_JUMP_FLAG") == null ? false : true);
        msgVO.setZoneJumpLabelDate(getUtilDate(crs.getObject("ZONE_JUMP_LABEL_DATE")));
        msgVO.setZoneJumpTrailerNumber(crs.getString("ZONE_JUMP_TRAILER_NUMBER"));
        msgVO.setPersonalGreetingID(crs.getString("PERSONAL_GREETING_ID"));
        
        messages.add(msgVO);
      }
    }
    return messages;
  }
 
    /**
   * This method serves as a wrapper for the GLOBAL.INVENTORY_MAINT_PKG.DECREMENT_PRODUCT_INVENTORY
   * stored procedure. 
   * (Taken from com.ftd.op.venus.dao.VenusDAO)
   * 
   * @param conn databasse connection
   * @param productId 
   * @param vendorId
   * @param decreaseAmount
   * @throws Exception
   * 
   */
    public boolean decrementProductInventory(Connection conn, String productId, String vendorId,  int decreaseAmount)
        throws Exception
    {
        logger.debug("decrementProductInventory(String productId ("+productId+"), String vendorId (" +vendorId+ ") int decreaseAmount ("+decreaseAmount+"))");
        
        boolean invLevelReached = false;
        
        DataRequest dataRequest = new DataRequest();
        dataRequest.reset();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("SHIP_DECREMENT_PRODUCT_INVENTORY");
        dataRequest.addInputParam("IN_PRODUCT_ID", productId);
        dataRequest.addInputParam("IN_VENDOR_ID", vendorId);
        dataRequest.addInputParam("IN_DECREASE_AMOUNT", new Integer(decreaseAmount).toString());
        HashMap results = (HashMap) DataAccessUtil.getInstance().execute(dataRequest);
        if ( results != null )
        {
            
            String levelFlag = (String) results.get("OUT_INVENTORY_LEVEL_REACHED");
            if(levelFlag != null && levelFlag.equalsIgnoreCase("Y"))
            {
                invLevelReached = true;
            }
        
            if (((String) results.get("OUT_STATUS")).equalsIgnoreCase("N"))
            {
              throw new Exception((String) results.get("OUT_ERROR_MESSAGE"));
            }
        }
        else throw new Exception("DECREMENT_PRODUCT_INVENTORY did not return any rows");  
        
        return invLevelReached;
    
    } 
         
   /**
    * This method serves as a wrapper for the GLOBAL.INVENTORY_MAINT_PKG.INCREMENT_PRODUCT_INVENTORY
    * stored procedure. 
    * (Taken from com.ftd.op.venus.dao.VenusDAO)
    * 
    * @param conn database connection
    * @param productId
    * @param vendorId
    * @param increaseAmount
    * @throws SAXException, ParserConfigurationException, IOException, SQLException, Exception
    * 
    */ 
    public void incrementProductInventory(Connection conn, String productId, String vendorId, int increaseAmount)
        throws Exception
    {
        logger.debug("incrementProductInventory(String productId ("+productId+"), int increaseAmount ("+increaseAmount+"), vendor id "+vendorId);
        DataRequest dataRequest = new DataRequest();
        dataRequest.reset();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("SHIP_INCREMENT_PRODUCT_INVENTORY");
        dataRequest.addInputParam("IN_PRODUCT_ID", productId);
        dataRequest.addInputParam("IN_VENDOR_ID", vendorId);
        dataRequest.addInputParam("IN_INCREASE_AMOUNT", new Integer(increaseAmount).toString());
        HashMap results = (HashMap) DataAccessUtil.getInstance().execute(dataRequest);
        if ( results != null )
        {
            if (((String) results.get("OUT_STATUS")).equalsIgnoreCase("N"))
            {
              throw new Exception((String) results.get("OUT_ERROR_MESSAGE"));
            }
        }
        else throw new Exception("INCREMENT_PRODUCT_INVENTORY did not return any rows");      
    }


    /**
     * Retruns a list of ProductNotificationVO's for the parameters passed
     * (Taken from com.ftd.op.venus.dao.VenusDAO)
     * 
     * @param conn
     * @param productId
     * @param vendorId
     * @return
     * @throws Exception
     */
    public List<ProductNotificationVO> getInventoryNotifications(Connection conn, String productId, String vendorId) throws Exception
    {
        CachedResultSet results = null;
    
        ArrayList<ProductNotificationVO> invList = new ArrayList<ProductNotificationVO>();
        
        DataRequest dataRequest = new DataRequest();
        dataRequest.reset();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("SHIP_GET_INV_CTRL_NOTIFICATIONS");

        HashMap inputParams = new HashMap();
        inputParams.put("IN_PRODUCT_ID", productId);
        inputParams.put("IN_VENDOR_ID", vendorId);
        dataRequest.setInputParams(inputParams);      

        Map outputs = (Map) DataAccessUtil.getInstance().execute(dataRequest);
      

        results = (CachedResultSet)outputs.get("OUT_INV_NOTIFICATIONS_CURSOR");

        if ( results != null )
        {
            while ( results.next() )
            {
                ProductNotificationVO notificationVO = new ProductNotificationVO();
          
                notificationVO.setEmailAddress(results.getString("email_address"));
                invList.add(notificationVO);
            }
        }
        return invList;
    }
    
      /**
       * Return inventory data in an xml format 
       * (Taken from com.ftd.op.venus.dao.VenusDAO)
       * 
       * @param conn database connection
       * @param productId
       * @param vendorId
       * @return 
       * @throws java.lang.Exception
       */
      public Document getInventoryRecordXML(Connection conn, String productId, String vendorId) throws Exception
      {
          DataRequest dataRequest = new DataRequest();
          dataRequest.reset();
          dataRequest.setConnection(conn);
          dataRequest.setStatementID("SHIP_GET_INVENTORY_DATA");
          
          HashMap inputParams = new HashMap();
          inputParams.put("IN_PRODUCT_ID", productId);
          inputParams.put("IN_VENDOR_ID", vendorId);
          dataRequest.setInputParams(inputParams);      
          
          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
          HashMap map = (HashMap)dataAccessUtil.execute(dataRequest);
          
          Document mainDoc = JAXPUtil.createDocument();
          Element mainRoot = mainDoc.createElement("inventorydata");
          mainDoc.appendChild(mainRoot);      

          Document inventoryDoc = JAXPUtil.createDocument();
          Element invRoot = inventoryDoc.createElement("inventory");
          inventoryDoc.appendChild(invRoot);      
          
          Document emailDoc = JAXPUtil.createDocument();
          Element emailRoot = emailDoc.createElement("notfications");
          emailDoc.appendChild(emailRoot);              
          
          DOMUtil.addSection(inventoryDoc,((Document)map.get("OUT_INVENTORY_CURSOR")).getChildNodes());
          DOMUtil.addSection(emailDoc,((Document)map.get("OUT_INV_NOTIFICATIONS_CURSOR")).getChildNodes());
          DOMUtil.addSection(mainDoc,inventoryDoc.getChildNodes());        
          DOMUtil.addSection(mainDoc,emailDoc.getChildNodes());                

          return mainDoc;
      }

    /**
     * Get a list of vendors that can ship a product based on the ship/delivery dates for a recipient postal code
     * (new)
     * 
     * @param conn database connection
     * @param productSubcodeId product id or product subcode id
     * @param deliveryDate filter
     * @param shipDate filter
     * @param postalCode filter
     * @return list of ShipVendorProductVO
     * @throws Exception
     */
    public List<ShipVendorProductVO> getShippingVendors(Connection conn, String productSubcodeId, Date deliveryDate, Date shipDate, String postalCode) throws Exception {
        ArrayList<ShipVendorProductVO> list = new ArrayList<ShipVendorProductVO>();
        
        CachedResultSet results = null;
        CachedResultSet results2 = null;
        
        DataRequest dataRequest = new DataRequest();
        dataRequest.reset();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("SHIP_GET_SHIPPING_VENDORS");
        
        HashMap inputParams = new HashMap();
        inputParams.put("IN_PRODUCT_SUBCODE_ID", productSubcodeId);
        inputParams.put("IN_DELIVERY_DATE", new java.sql.Date(deliveryDate.getTime()));
        inputParams.put("IN_SHIP_DATE", new java.sql.Date(shipDate.getTime()));
        dataRequest.setInputParams(inputParams);
        
        results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        if ( results != null )
        {
            while ( results.next() )
            {
                ShipVendorProductVO vo = new ShipVendorProductVO();                    
                List<CarrierVO> carrierList = new ArrayList<CarrierVO>();
                String vendorId = results.getString("VENDOR_ID");
                vo.setVendorId(vendorId);
                vo.setVendorCode(results.getString("MEMBER_NUMBER"));
                vo.setProductSkuId(results.getString("PRODUCT_SUBCODE_ID"));
                BigDecimal vendorCost = results.getBigDecimal("VENDOR_COST");
                if( vendorCost==null ) {
                 vendorCost = new BigDecimal(0.00);
                }
                vo.setVendorCost(vendorCost.doubleValue());
                vo.setVendorSku(results.getString("VENDOR_SKU"));
                vo.setAvailable(StringUtils.equals("Y",results.getString("AVAILABLE")));
                vo.setSubcodeDescription(results.getString("SUBCODE_DESCRIPTION"));
                
                //Now call venus.ship_pkg.get_carriers_for_product
                DataRequest dataRequest2 = new DataRequest();
                dataRequest2.reset();
                dataRequest2.setConnection(conn);
                dataRequest2.setStatementID("SHIP_GET_SDS_VENDOR_CARRIERS");
                  
                inputParams = new HashMap();
                inputParams.put("IN_VENDOR_ID", vendorId);
                inputParams.put("IN_POSTAL_CODE", postalCode);
                dataRequest2.setInputParams(inputParams);
                  
                results2 = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest2);
                  
                if( results2 != null ) {
                    while(results2.next()) {
                        String returnCarrierId = results2.getString(1);
                        if( StringUtils.isNotBlank(returnCarrierId) ) {
                            CarrierVO cvo = new CarrierVO();
                            cvo.setCarrierId(returnCarrierId);
                            carrierList.add(cvo);   
                        }
                    }
                }
                
                //Don't add vendors that don't have assigned carriers
                if( carrierList.size()>0 ) {
                    vo.setCarrierIds(carrierList);
                    list.add(vo);
                }
             }
         }
        
        return list;
    }

    /**
     * Can the vendor ship on the specified ship date
     * @param connection database conneciton
     * @param vendorId to validate
     * @param shipDate to validate
     * @return true if vendor can ship
     * @throws Exception
     */
    public boolean canVendorShipOnShipDate( Connection connection, String vendorId, java.util.Date shipDate) throws Exception {        
        DataRequest dataRequest = new DataRequest();
        dataRequest.reset();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("SHIP_CAN_VENDOR_SHIP_ON_DATE");
        
        HashMap inputParams = new HashMap();
        inputParams.put("IN_VENDOR_ID", vendorId);
        inputParams.put("IN_SHIP_DATE", new java.sql.Date(shipDate.getTime()));
        dataRequest.setInputParams(inputParams);
        
        String result = (String)DataAccessUtil.getInstance().execute(dataRequest);
        
        return StringUtils.equals(result,"Y");
    }

    /**
     * Get a list of active carriers
     * @param connection database connection
     * @return
     * @throws Exception
     */
    public List<CarrierVO> getCarrierList(Connection connection) throws Exception {
        List<CarrierVO> list = new ArrayList<CarrierVO>();
        
        CachedResultSet results = null;
        
        DataRequest dataRequest = new DataRequest();
        dataRequest.reset();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("SHIP_GET_CARRIER_LIST");
        
        HashMap inputParams = new HashMap();
        dataRequest.setInputParams(inputParams);
        
        results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        if ( results != null )
        {
            while ( results.next() )
            {
                CarrierVO vo = new CarrierVO();
                vo.setCarrierId(results.getString(1));
                vo.setCarrierName(results.getString(2));
                list.add(vo);
            }
        }
        
        return list;
    }
    
    public BigDecimal insertSDSMessage(Connection connection, SDSMessageVO sdsMessageVO) throws Exception {
        //calls venus.ship_pkg.insertSDSMessage 

        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("SHIP_INSERT_SDS_STATUS_MESSAGE");
         
        dataRequest.addInputParam("IN_CARTON_NUMBER",sdsMessageVO.getCartonNumber());
        dataRequest.addInputParam("IN_TRACKING_NUMBER",sdsMessageVO.getTrackingNumber());
        dataRequest.addInputParam("IN_BATCH_NUMBER",sdsMessageVO.getPrintBatchNumber());
        dataRequest.addInputParam("IN_BATCH_SEQUENCE",sdsMessageVO.getPrintBatchSequence()==null ? null : sdsMessageVO.getPrintBatchSequence());
        dataRequest.addInputParam("IN_SHIP_DATE", sdsMessageVO.getShipDate() == null ? null : new java.sql.Date(sdsMessageVO.getShipDate().getTime()));
        dataRequest.addInputParam("IN_STATUS",sdsMessageVO.getMsgType().getSdsMsgType());
        dataRequest.addInputParam("IN_STATUS_DATE",sdsMessageVO.getStatusDate() == null ? null : new java.sql.Timestamp(sdsMessageVO.getStatusDate().getTime()));
        dataRequest.addInputParam("IN_PROCESSED",sdsMessageVO.isProcessed()?"Y":"N");
        dataRequest.addInputParam("IN_DIRECTION",sdsMessageVO.getDirection().toString());
         
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
        
        BigDecimal id = (BigDecimal) outputs.get("OUT_MSG_ID");
        
        return id;
    }
    
    public SDSMessageVO getSDSMessage(Connection connection, Long msgId) throws Exception {
        
        ResultSet rs = null;
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("SHIP_GET_SDS_STATUS_MESSAGE");
          
        dataRequest.addInputParam("IN_ID", new BigDecimal(msgId));
         
        rs = (ResultSet)DataAccessUtil.getInstance().execute(dataRequest);
        
        SDSMessageVO vo = null;
        if( rs.next() ) {
            vo = new SDSMessageVO();
            vo.setMsgId(rs.getBigDecimal("ID")); 
            vo.setCartonNumber(rs.getString("CARTON_NUMBER"));
            vo.setTrackingNumber(rs.getString("TRACKING_NUMBER")); 
            vo.setPrintBatchNumber(rs.getString("PRINT_BATCH_NUMBER")); 
            vo.setPrintBatchSequence(rs.getBigDecimal("PRINT_BATCH_SEQUENCE"));
            Timestamp tempTime = rs.getTimestamp("SHIP_DATE");
            vo.setShipDate(tempTime == null ? null : new Date(tempTime.getTime()));
            tempTime = rs.getTimestamp("STATUS_DATE");
            vo.setStatusDate(tempTime == null ? new Date() : new Date(tempTime.getTime()));
            vo.setProcessed(StringUtils.equals(rs.getString("PROCESSED"),"Y"));
            vo.setStatus(rs.getString("STATUS"));
            vo.setMsgType(MsgType.sdsToMsgType(vo.getStatus()));
            vo.setDirection(MsgDirection.toMsgDirection(rs.getString("MESSAGE_DIRECTION")));
        }
        
        return vo;
    }
    
    public void markSDSMessageProcessed(Connection connection, BigDecimal msgId) throws Exception {
    
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("SHIP_MARK_SDS_MESSAGE_PROCESSED");
         
        dataRequest.addInputParam("IN_ID", msgId);
         
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
    }

    /**
     * Get the vendors where they are past cutoff and the associated carriers
     * @param connection database connection
     * @return
     * @throws Exception
     */
    public List<ManifestVO> getVendorsToClose(Connection connection, String vendor, String carrier, boolean zoneJumpFlag, 
                                              String daysInTransitQty)
      throws Exception
    {
        List<ManifestVO> list = new ArrayList<ManifestVO>();

        CachedResultSet results = null;
        
        DataRequest dataRequest = new DataRequest();
        dataRequest.reset();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("SHIP_GET_VENDOR_CARRIERS_TO_CLOSE");
        
        HashMap inputParams = new HashMap();
        inputParams.put("IN_VENDOR",vendor);
        inputParams.put("IN_CARRIER_ID",carrier);
        inputParams.put("IN_ZJ_FLAG",zoneJumpFlag?"Y":"N");
        inputParams.put("IN_DAYS_IN_TRANSIT_QTY",daysInTransitQty);
        dataRequest.setInputParams(inputParams);
        
        results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        if ( results != null )
        {
          String vendorCode = null; 
          String carrierId = null; 
          String zoneJumpTrailerNumber = null; 
          
            while ( results.next() )
            {
                ManifestVO vo = new ManifestVO();
                
                //retrieve the fields
                vendorCode = results.getString("filling_vendor"); 
                carrierId = results.getString("final_carrier"); 
                zoneJumpTrailerNumber = results.getString("zone_jump_trailer_number"); 
                
                //if vendorCode was returned, remove the "-" from the vendorCode
                if (StringUtils.isNotBlank(vendorCode))
                  vendorCode = StringUtils.remove(vendorCode, "-"); 
                
                //set the VO
                vo.setVendorCode(vendorCode);
                vo.setCarrierId(carrierId);
                vo.setZoneJumpTrailerNumber(zoneJumpTrailerNumber);
                
                //add the vo to the list to be passed back. 
                list.add(vo);
            }
        }
        
        return list;
    }

    /**
     * Get the error processing information for the passed in error id and system
     * 
     * @param connection database connection
     * @param errorId to look up
     * @param system SDS or FEDEX
     * @return SDSErrorVO populated with processing instructions for the passed in parameter
     * @throws Exception
     */
    public SDSErrorVO getErrorProcessing(Connection connection, String errorId, String system) throws Exception {
        
        CachedResultSet results = null;
        
        DataRequest dataRequest = new DataRequest();
        dataRequest.reset();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("SHIP_GET_SDS_ERROR_PROCESSING");
        
        HashMap inputParams = new HashMap();
        inputParams.put("IN_ERROR_ID", errorId);
        inputParams.put("IN_SOURCE", system);
        dataRequest.setInputParams(inputParams);
        
        results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        SDSErrorVO errorVO = new SDSErrorVO();
        if ( results != null && results.next() )
        {
            errorVO = new SDSErrorVO();
            errorVO.setErrorId( results.getBigDecimal(1).longValue() );
            errorVO.setSource(results.getString(2));
            errorVO.setDisposition(StringUtils.equals(SDSErrorDisposition.REPROCESS.toString(),results.getString(3))?SDSErrorDisposition.REPROCESS:SDSErrorDisposition.QUEUE);
            errorVO.setPageSupport(StringUtils.equals("Y",results.getString(4)));
            errorVO.setOrderComment(results.getString(5));
        } else {
            errorVO.setErrorId(Long.parseLong(errorId));
            errorVO.setSource(system);
            errorVO.setDisposition(SDSErrorDisposition.QUEUE);
            errorVO.setPageSupport(true);
            errorVO.setOrderComment("An unknown error was received from the Argo server.");
        }
        
        return errorVO;
    }

    /**
     * Get the ftd ship method that corresponds to the ftd ship method specified
     * 
     * @param connection database connection
     * @param carrierId for the passed ship method
     * @param carrierShipMethod to look up
     * @return the carrier specific ship method for the passed in ftd ship method
     * @throws Exception
     */
    public String getShipMethodFromCarrierShipMethod(Connection connection, String carrierId, String carrierShipMethod) throws Exception {      
        DataRequest dataRequest = new DataRequest();
        dataRequest.reset();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("SHIP_GET_FTD_SHIP_METHOD");
        
        HashMap inputParams = new HashMap();
        inputParams.put("IN_CARRIER_ID", carrierId);
        inputParams.put("IN_SHIP_METHOD", carrierShipMethod);
        dataRequest.setInputParams(inputParams);
        
        String result = (String)DataAccessUtil.getInstance().execute(dataRequest);
        
        return result;
    }
    
    public String getShipVia(Connection connection, String shipMethod, boolean airOnly) throws Exception {      
        DataRequest dataRequest = new DataRequest();
        dataRequest.reset();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("SHIP_GET_SHIP_VIA");
        
        HashMap inputParams = new HashMap();
        inputParams.put("IN_SHIP_METHOD", shipMethod);
        inputParams.put("IN_AIR_ONLY", airOnly?"Y":"N");
        dataRequest.setInputParams(inputParams);
        
        String result = (String)DataAccessUtil.getInstance().execute(dataRequest);
        
        return result;
    }
    
    public void markVenusMsgCancelled(Connection connection, VenusMessageVO venusMessageVO) throws Exception {
    
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("SHIP_MARK_VENUS_MSG_CANCELLED");
         
        dataRequest.addInputParam("IN_VENUS_ID", venusMessageVO.getVenusId());         
        dataRequest.addInputParam("IN_CANCELLED_TIME", new java.sql.Timestamp(venusMessageVO.getCancelledStatusDate().getTime()));
         
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
    }
    
    public void markVenusMsgRejected(Connection connection, VenusMessageVO venusMessageVO) throws Exception {
    
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("SHIP_MARK_VENUS_MSG_REJECTED");
         
        dataRequest.addInputParam("IN_VENUS_ID", venusMessageVO.getVenusId());         
        dataRequest.addInputParam("IN_REJECTED_TIME", new java.sql.Timestamp(venusMessageVO.getRejectedStatusDate().getTime()));
         
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
    }
    
    public void markVenusMsgShipped(Connection connection, VenusMessageVO venusMessageVO) throws Exception {
    
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("SHIP_MARK_VENUS_MSG_SHIPPED");
         
        dataRequest.addInputParam("IN_VENUS_ID", venusMessageVO.getVenusId());         
        dataRequest.addInputParam("IN_SHIPPED_TIME", new java.sql.Timestamp(venusMessageVO.getShippedStatusDate().getTime()));
         
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
    }
    
    public void markVenusMsgPrinted(Connection connection, VenusMessageVO venusMessageVO) throws Exception {
    
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("SHIP_MARK_VENUS_MSG_PRINTED");
         
        dataRequest.addInputParam("IN_VENUS_ID", venusMessageVO.getVenusId());         
        dataRequest.addInputParam("IN_PRINTED_TIME", new java.sql.Timestamp(venusMessageVO.getPrintedStatusDate().getTime()));
        dataRequest.addInputParam("IN_BATCH_NUMBER", venusMessageVO.getPrintBatchNumber());
        dataRequest.addInputParam("IN_BATCH_SEQUENCE", venusMessageVO.getPrintBatchSequence());
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
    }

    /**
     * Inserts a record into the venus.sds_transaction table
     * @param conn database connection
     * @param transactionVO record to insert into the venus.SDS_TRANSACTION table
     * @throws Exception
     */
    public void insertSDSTransaction(Connection conn, SDSTransactionVO transactionVO) throws Exception
    {
        logger.debug("insertSDSTransaction(Connection conn, SDSTransactionVO transactionVO)");
        DataRequest dataRequest = new DataRequest();
        
        dataRequest.setStatementID("SHIP_INSERT_SDS_TRANSACTION");
        dataRequest.setConnection(conn);
        
        dataRequest.addInputParam("IN_VENUS_ID", transactionVO.getVenusId());        
        dataRequest.addInputParam("IN_REQUEST", transactionVO.getRequest());        
        dataRequest.addInputParam("IN_RESPONSE", transactionVO.getResponse());
        
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
         String message = (String) outputs.get("OUT_MESSAGE");
         throw new Exception(message);
        }
    }

    /**
     * Get the SDS transaction for the passed in venus id
     * @param connection database connection
     * @param venusId to get transaction for
     * @return
     * @throws Exception
     */
    public SDSTransactionVO getSDSTransaction(Connection connection, String venusId) throws Exception {
        SDSTransactionVO vo = new SDSTransactionVO();
        
        CachedResultSet results = null;
        
        DataRequest dataRequest = new DataRequest();
        dataRequest.reset();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("SHIP_GET_SDS_TRANS_BY_VENUS_ID");
        dataRequest.addInputParam("IN_VENUS_ID",venusId);
        
        DataAccessUtil dau = DataAccessUtil.getInstance();
        
        results = (CachedResultSet)dau.execute(dataRequest);

        if ( results!=null && results.next() )
        {
            vo.setId(results.getBigDecimal(1));
            vo.setVenusId(results.getString(2));
            Clob clob = results.getClob(3);
            vo.setRequest(clob.getSubString(1L,(int)clob.length()));
            clob = results.getClob(4);
            vo.setResponse(clob.getSubString(1L,(int)clob.length()));
        }
        
        return vo;
    }

    /**
     * Convert the carrier ship method to an FTD ship method by calling 
     * venus.ship_pkg.GET_FTD_SHIP_DATA
     * @param connection database connection
     * @param carrierId carrier id
     * @param carrierShipMethod carrier's ship method
     * @return
     * @throws Exception
     */
    public ShipMethodVO getFtdShipMethodData(Connection connection, String carrierId, String carrierShipMethod) throws Exception {
        ShipMethodVO vo = null;
        
        CachedResultSet rs = null;
        
        DataRequest dataRequest = new DataRequest();
        dataRequest.reset();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("SHIP_GET_FTD_SHIP_DATA");
        dataRequest.addInputParam("IN_CARRIER_ID",carrierId);
        dataRequest.addInputParam("IN_SHIP_METHOD",carrierShipMethod);
        
        DataAccessUtil dau = DataAccessUtil.getInstance();
        
        rs = (CachedResultSet)dau.execute(dataRequest);

        if ( rs!=null && rs.next() )
        {
            vo = new ShipMethodVO();
            vo.setCarrierId(rs.getString(1));
            vo.setShipMethod(rs.getString(2));
            vo.setCarrierDelivery(rs.getString(3));
        }
        
        return vo;
    }

    /**
     * Get the file format name for the downloaded carrier scans file 
     * @param connection database connection
     * @param carrierId filter
     * @return a string representation of the carrier format
     * @throws Exception
     */
    public String getCarrierScanFormat(Connection connection, String carrierId) throws Exception {
        DataRequest dataRequest = new DataRequest();
        dataRequest.reset();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("SHIP_GET_CARRIER_SCAN_FORMAT");
        
        HashMap inputParams = new HashMap();
        inputParams.put("IN_CARRIER_ID", carrierId);
        dataRequest.setInputParams(inputParams);
        
        return (String)DataAccessUtil.getInstance().execute(dataRequest);
    }

    /**
     * Get a list of active scans for the passed in carrier id
     * @param connection database connection
     * @param carrierId filter
     * @return HashMap of CarrierScanVO value objects
     * @throws Exception
     */
    public HashMap<String,CarrierScanTypeVO> getActiveCarrierScansMap(Connection connection, String carrierId) throws Exception {
        HashMap<String,CarrierScanTypeVO> list = new HashMap<String,CarrierScanTypeVO>();
        DataRequest dataRequest = new DataRequest();
        dataRequest.reset();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("SHIP_GET_ACTIVE_CARRIER_SCANS");
        
        HashMap inputParams = new HashMap();
        inputParams.put("IN_CARRIER_ID", carrierId);
        dataRequest.setInputParams(inputParams);
        
        CachedResultSet rs = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);
        
        while( rs.next() ) {
            CarrierScanTypeVO vo = new CarrierScanTypeVO();
            vo.setCarrierId(rs.getString(1));
            vo.setScanType(rs.getString(2));
            vo.setCollectScan(StringUtils.equals("Y",rs.getString(3)));
            vo.setDeliveryScan(StringUtils.equals("Y",rs.getString(4)));
            
            list.put(vo.getScanType(),vo);
        }
        
        return list;
    }

    /**
     * Insert a record into the venus.scans table
     * @param connection database connection
     * @param carrierScanVO contains values to insert
     * @return primary key on new record
     * @throws Exception
     */
    public BigDecimal insertScan(Connection connection, CarrierScanVO carrierScanVO) throws Exception {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("SHIP_INSERT_SCAN");
         
        dataRequest.addInputParam("IN_CARRIER_ID",carrierScanVO.getCarrierId());
        dataRequest.addInputParam("IN_SCAN_TYPE",carrierScanVO.getScanType());
        dataRequest.addInputParam("IN_FILE_NAME",carrierScanVO.getFileName());
        dataRequest.addInputParam("IN_SCAN_TIMESTAMP",carrierScanVO.getTimestamp() == null ? null : new java.sql.Timestamp(carrierScanVO.getTimestamp().getTime()));
        dataRequest.addInputParam("IN_PROCESSED",carrierScanVO.isProcessed()?"Y":"N");
        dataRequest.addInputParam("IN_ORDER_ID",carrierScanVO.getOrderId());
        dataRequest.addInputParam("IN_TRACKING_NUMBER",carrierScanVO.getTrackingNumber());
         
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
        
        BigDecimal id = (BigDecimal) outputs.get("OUT_ID");
        
        return id;
    }

    /**
     * Process a record from the venus.scans table
     * @param connection database connection
     * @param id primary key 
     * @return CarrierScanVO
     * @throws Exception
     */
    public void processScan(Connection connection, BigDecimal id) throws Exception {
        
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("SHIP_PROCESS_SCAN");
          
        dataRequest.addInputParam("IN_SCAN_ID", id);
         
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
    }

    /**
     * Insert the contents of a carrier scan file into the database
     * @param connection database conneciton
     * @param carrierId scan file origin
     * @param fileName from original file
     * @param contents of file
     */
    public void insertScanFile(Connection connection, String carrierId, String fileName, String userId, String contents) throws Exception {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("SHIP_INSERT_SCAN_FILE");
         
        dataRequest.addInputParam("IN_CARRIER_ID",carrierId);
        dataRequest.addInputParam("IN_FILE_NAME",fileName);
        dataRequest.addInputParam("IN_USER_ID",userId);
        dataRequest.addInputParam("IN_CONTENTS",contents);
         
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
    }

    /**
     * Get the venus.scan_file record for the passed in parameters
     * @param connection database conneciton
     * @param carrierId scan file origin
     * @param fileName original scan file name
     * @return String representation of the scan file
     */
    public String getScanFile(Connection connection, String carrierId, String fileName) throws Exception {
        DataRequest dataRequest = new DataRequest();
        dataRequest.reset();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("SHIP_GET_LAST_SCAN_FILE");
        
        HashMap inputParams = new HashMap();
        inputParams.put("IN_CARRIER_ID", carrierId);
        inputParams.put("IN_FILE_NAME", fileName);
        dataRequest.setInputParams(inputParams);
        String result = "";
        
        CachedResultSet rs = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);
        
        if( rs!=null && rs.next() ) {
            Clob tClob = rs.getClob(1);
            
            if( tClob!=null ) {
                result = tClob.getSubString(1, (int) tClob.length());
            }
        }
        
        return result;
        
    }

    /**
     * Get a list of carton numbers with missing status
     * @param connection database connection
     * @param daysToProcess how many days do we check for
     * @return list of carton numbers (e numbers) that have missing status
     * @throws Exception
     */
    public List<String> getMissingStatus(Connection connection, long daysToProcess) throws Exception {
        List<String> cartonNumbers = new ArrayList<String>();
        DataRequest dataRequest = new DataRequest();
        dataRequest.reset();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("SHIP_GET_MISSING_STATUS");
        
        HashMap inputParams = new HashMap();
        inputParams.put("IN_DAYS_TO_PROCESS", new Long(daysToProcess));
        dataRequest.setInputParams(inputParams);
        
        CachedResultSet rs = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);
        
        while( rs.next() ) {
            String cartonNumber = rs.getString(1);
            
            if( cartonNumber!=null ) {
                cartonNumbers.add(cartonNumber);
            }
        }
        
        return cartonNumbers;
    }
    
    public void postAMessage(Connection conn, String queueName, String correlationId, String payload, long delaySeconds ) throws Exception {
        
        DataRequest dataRequest = new DataRequest();
        
        /* build DataRequest object */
        HashMap inputParams = new HashMap();
        inputParams.put("IN_QUEUE_NAME",queueName);
        inputParams.put("IN_CORRELATION_ID",correlationId);
        inputParams.put("IN_PAYLOAD",payload);
        inputParams.put("IN_DELAY_SECONDS",new Long(delaySeconds));
        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("SHIP_POST_A_MESSAGE");
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
    }

    /**
     * Retrieves a record from the FRP.GLOBAL_PARMS table
     * Calls FRP.MISC_PKG.GET_GLOBAL_PARM_VALUE.
     *
     * @param conn database connection
     * @param context filter
     * @param name filter
     * @return String value of record retrieved.  Null is returned if no 
     * matching record was found.
     */
    public String getGlobalParameter(Connection conn, String context, 
                                     String name) throws Exception {
        DataRequest dataRequest = new DataRequest();
        boolean isEmpty = true;
        String retval = null;

        try {
            logger.debug("getGlobalParameter (String context, String name)");
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("CONTEXT", context);
            inputParams.put("NAME", name);

            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("SHIP_GET_GLOBAL_PARM_VALUE");
            dataRequest.setInputParams(inputParams);

            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            retval = (String)dataAccessUtil.execute(dataRequest);
            if (retval != null)
                isEmpty = false;
        } catch (Exception e) {
            logger.error(e);
            throw e;
        }
        if (isEmpty)
            return null;
        else
            return retval;
    }

    /**
     * This method serves as a wrapper for the VENUS.UTIL_PKG.GET_NEXT_VENUS_ORDER_NUMBER_SQ
     * stored procedure.
     * 
     * @param conn database connection
     * @return
     * @throws Exception
     */
    public long generateVenusOrderNumber(Connection conn) throws Exception{
        DataRequest dataRequest = new DataRequest();
        long idNum = 0;
        try
        {
            logger.info("generateVenusOrderNumber :: String ");
            /* setup stored procedure input parameters */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("SHIP_GET_NEXT_VENUS_ORDER_NUMBER_SQ");
        
            /* execute the stored prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            BigDecimal output =(BigDecimal) dataAccessUtil.execute(dataRequest);
            idNum = output.longValue();
        
        } catch (Exception e) {
          logger.error(e);
          throw e;
        }
        return idNum;
    }

    private String getBoolean(boolean bool)
    {
        return bool ? "Y" : "N";
    }
    
    /*
     * Converts a database timestamp to a java.util.Date.
     */
    private java.util.Date getUtilDate(Object time)
    {
        java.util.Date theDate = null;
        boolean test  = false;
        if(time != null)
        {
            if(test || time instanceof Timestamp)
            {
                Timestamp timestamp = (Timestamp) time;
                theDate = new java.util.Date(timestamp.getTime());
            }
            else
            {
                theDate = (java.util.Date) time;            
            }
        }
        
        return theDate;
    }

    /**
     * Places a message in a JMS queue
     * Calls events.post_a_message_flex
     * 
     * @param conn database connection
     * @param queueName name of JMS queue table
     * @param correlationId to save
     * @param payload to save
     * @throws Exception for all errors and exceptions received
     */
    public void postAMessage(Connection conn, String queueName, 
                             String correlationId, 
                             String payload) throws Exception {

        /**
        IN_QUEUE_NAME     IN  VARCHAR2,
        IN_CORRELATION_ID IN  VARCHAR2,
        IN_PAYLOAD        IN  VARCHAR2,
        IN_DELAY_SECONDS  IN  NUMBER,
        OUT_STATUS        OUT VARCHAR2,
        OUT_MESSAGE       OUT VARCHAR2
        */

        StringBuilder sb = new StringBuilder();
        sb.append("Calling postamessage");
        sb.append("\r\nqueueName: ");
        sb.append(queueName);
        sb.append("\r\ncorrelationId: ");
        sb.append(correlationId);
        sb.append("\r\npayload: ");
        sb.append(payload);
        DataRequest dataRequest = new DataRequest();

        /* build DataRequest object */
        HashMap inputParams = new HashMap();
        inputParams.put("IN_QUEUE_NAME", queueName);
        inputParams.put("IN_CORRELATION_ID", correlationId);
        inputParams.put("IN_PAYLOAD", payload);
        inputParams.put("IN_DELAY_SECONDS", new Long(0));
        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("PI_POST_A_MESSAGE");

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map)dataAccessUtil.execute(dataRequest);

        String status = (String)outputs.get("OUT_STATUS");
        if (status != null && status.equalsIgnoreCase("N")) {
            String message = (String)outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
    }

    /**
     * Defect 1809
     * Get the number of vendor initiated rejects for an order.  Calls
     * VENUS.SHIP_PKG.GET_VENDOR_ORDER_REJECT_COUNT
     * @param conn database connection
     * @param vo order value object
     * @return count of rejects
     * @throws Exception
     */
    public Long getVendorOrderRejectCount(Connection conn, VenusMessageVO vo) throws Exception {
        Long retval = null;
        
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("SHIP_GET_VENDOR_ORDER_REJECT_COUNT");
        dataRequest.addInputParam("IN_REFERENCE_NUMBER",vo.getReferenceNumber());
        
        BigDecimal result = (BigDecimal)DataAccessUtil.getInstance().execute(dataRequest);
        
        if( result != null ) {
            retval = result.longValue();
        }
        return retval;
    }

    /**
     * Defect 1809
     * Get the number of vendor initiated rejects for an order.  Calls
     * VENUS.SHIP_PKG.GET_SYSTEM_ORDER_REJECT_COUNT
     * @param conn database connection
     * @param vo order value object
     * @return count of rejects
     * @throws Exception
     */
    public Long getSystemOrderRejectCount(Connection conn, VenusMessageVO vo) throws Exception {
        Long retval = null;
        
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("SHIP_GET_SYSTEM_ORDER_REJECT_COUNT");
        dataRequest.addInputParam("IN_REFERENCE_NUMBER",vo.getReferenceNumber());
        
        BigDecimal result = (BigDecimal)DataAccessUtil.getInstance().execute(dataRequest);
        
        if( result != null ) {
            retval = result.longValue();
        }
        return retval;
    }

    /**
     * Defect 1809
     * Get the number of vendor initiated rejects for an order.  Calls
     * VENUS.SHIP_PKG.GET_CARRIER_ORDER_REJECT_COUNT
     * @param conn database connection
     * @param vo order value object
     * @return count of rejects
     * @throws Exception
     */
    public Long getCarrierOrderRejectCount(Connection conn, VenusMessageVO vo) throws Exception {
        Long retval = null;
        
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("SHIP_GET_CARRIER_ORDER_REJECT_COUNT");
        dataRequest.addInputParam("IN_REFERENCE_NUMBER",vo.getReferenceNumber());
        
        BigDecimal result = (BigDecimal)DataAccessUtil.getInstance().execute(dataRequest);
        
        if( result != null ) {
            retval = result.longValue();
        }
        return retval;
    }

    /**
     * Defect 1809
     * Get the billing account number from the ftd_apps.partner_shipping_accounts
     * table for the passed in parameters.  Returns an empty string if the data
     * cannot be found.
     * @param conn database connection
     * @param vendorId filter
     * @param partnerId filter
     * @param carrierId filter
     * @return
     * @throws Exception
     */
    public String getPartnerShippingAccount(Connection conn, String vendorId, String partnerId, String carrierId) throws Exception {
        
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("SHIP_GET_PARTNER_SHIPPING_ACCOUNT");
        dataRequest.addInputParam("IN_VENDOR_ID", vendorId);
        dataRequest.addInputParam("IN_PARTNER_ID", partnerId);
        dataRequest.addInputParam("IN_CARRIER_ID", carrierId);
        
        return (String)DataAccessUtil.getInstance().execute(dataRequest);
    }

    /**
    * This method will remove a previously assigned Zone Jump order from the vendor it was assigned to and  
    * remove that box from the vendor's pallet count so the space can be used for another package.
    *
    * @param conn
    * @param venusId
    * @throws SAXException, ParserConfigurationException, IOException, SQLException, Exception
    */

    public void removeOrderFromVendor(Connection conn, String venusId)throws SAXException, ParserConfigurationException, IOException, SQLException, Exception
    {
       logger.debug("removeOrderFromVendor(String venusId ("+venusId+"))");
       DataRequest dataRequest = new DataRequest();
       dataRequest.reset();
       dataRequest.setConnection(conn);
       dataRequest.setStatementID("REMOVE_ORDER_FROM_VENDOR");
       dataRequest.addInputParam("IN_VENUS_ID", venusId);
       dataRequest.addInputParam("IN_UPDATED_BY", "SHIP_PROCESSING");

       Map outputs = (Map) DataAccessUtil.getInstance().execute(dataRequest);
       String status = (String)outputs.get("OUT_STATUS");

       if(status.equals("N") )
       {
        String message = (String)outputs.get("OUT_MESSAGE");
        throw new Exception(message.toString());
       }
    }
    
    /**
     * Get a list of Zone Jump carrier hubs based on product and order delivery date.
     * Valid carrier and hubs need to meet these conditions:
     *      Product is available.
     *      Zone Jump trip can accommodate order delivery date.
     *      Order detail has not being assigned to the vendor previously.
     *      Pass cap block check.
     * @param conn
     * @param productSubcodeId
     * @param deliveryDate
     * @param orderDetailId
     * @return
     * @throws Exception 
     */
    public List<SDSZoneJumpTripVO> getZoneJumpHubs(Connection conn, String productSubcodeId, Date deliveryDate, String orderDetailId) throws Exception {
        logger.debug("getZoneJumpHubs(String productSubcodeId ("+productSubcodeId+"), Date deliveryDate ("+deliveryDate+"), String orderDetailId ("+orderDetailId+")");
        
        ArrayList<SDSZoneJumpTripVO> list = new ArrayList<SDSZoneJumpTripVO>();
        
        CachedResultSet results = null;
        
        DataRequest dataRequest = new DataRequest();
        dataRequest.reset();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("SHIP_GET_ZONE_JUMP_HUBS");
        
        HashMap inputParams = new HashMap();
        inputParams.put("IN_PRODUCT_SUBCODE_ID", productSubcodeId);
        inputParams.put("IN_DELIVERY_DATE", new java.sql.Date(deliveryDate.getTime()));
        inputParams.put("IN_ORDER_DETAIL_ID", orderDetailId);
        dataRequest.setInputParams(inputParams);
        
        results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        if ( results != null )
        {
            while ( results.next() )
            {
                int ctr = 0;
                SDSZoneJumpTripVO vo = new SDSZoneJumpTripVO();
                
                vo.setCarrierId(results.getString(++ctr));
                vo.setShipPointId(results.getString(++ctr));
                vo.setZoneJumpDate(results.getDate(++ctr));
                vo.setShipDate(results.getDate(++ctr));
                list.add(vo);
             }
         }
        
        return list;
    }

    /**
     * Retrieves vendors based on hub information.
     * @param conn
     * @param productSubcodeId
     * @param responseVO
     * @return
     * @throws Exception
     */
     public List<ShipVendorProductVO> getShippingVendorsByHub(Connection conn, String productSubcodeId, SDSRateResponseVO responseVO, int index) throws Exception {
         logger.debug("getShippingVendorsByHub(String productSubcodeId ("+productSubcodeId+")");
         ArrayList<ShipVendorProductVO> list = new ArrayList<ShipVendorProductVO>();
         List hubs = (ArrayList<SDSZoneJumpTripVO>)responseVO.getZoneJumpTrips();
         CachedResultSet results = null;
         CachedResultSet results2 = null;
         DataRequest dataRequest = new DataRequest();
         dataRequest.reset();
         dataRequest.setConnection(conn);
         dataRequest.setStatementID("SHIP_GET_SHIPPING_VENDORS_BY_HUB");
         
         SDSZoneJumpTripVO hub = (SDSZoneJumpTripVO)hubs.get(index);
         ShipmentOptionVO soVO = hub.getShipmentOption();

        //if there was an error, we don't care about any vendor(s) (if any) associated with the hub.
         if(StringUtils.isBlank(soVO.getErrorTxt()))
         {
           String shipPointId = hub.getShipPointId();
           String carrierId = hub.getCarrierId();
           String sdsShipMethodId = hub.getShipMethod();
           Date zoneJumpDate = hub.getZoneJumpDate();   
           Date deliveryDate = hub.getDeliveryDate(); 
           Date shipDate = hub.getShipDate();            
           
           HashMap inputParams = new HashMap();
           inputParams.put("IN_PRODUCT_SUBCODE_ID", productSubcodeId);
           inputParams.put("IN_SHIP_DATE", new java.sql.Date(shipDate.getTime()));
           inputParams.put("IN_CARRIER_ID", carrierId);
           inputParams.put("IN_SHIP_POINT_ID", shipPointId);
           inputParams.put("IN_DELIVERY_DATE", new java.sql.Date(deliveryDate.getTime()));
           inputParams.put("IN_SDS_SHIP_METHOD", sdsShipMethodId);
  
           SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
           String zoneJumpDateString = null; 
           String shipDateString = null; 
           String deliveryDateString = null; 
           
           try
           {
             if (zoneJumpDate != null)
              zoneJumpDateString = sdf.format(zoneJumpDate);
             if (shipDate != null)
              shipDateString = sdf.format(shipDate);
             if (deliveryDate != null) 
              deliveryDateString = sdf.format(deliveryDate);
           }
           catch (Exception e)
           {
             logger.debug(e.toString());
           }
           logger.debug("getShippingVendorsByHub -- " +
                        " shipPointId = " + shipPointId + 
                        " and carrierId = " + carrierId +
                        " and sdsShipMethodId = " + sdsShipMethodId + 
                        " and zoneJumpDateString = " + zoneJumpDateString +
                        " and shipDateString = " + shipDateString + 
                        " and deliveryDateString = " + deliveryDateString);
               
           dataRequest.setInputParams(inputParams);
  
           // For each available hub, find the available vendor and add to list.
           results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);
                          
           if ( results != null )
           {
               while ( results.next() )
               {
                   ShipVendorProductVO vo = new ShipVendorProductVO();                    
                   SDSZoneJumpTripVO trip = new SDSZoneJumpTripVO();
                   String vendorId = results.getString("VENDOR_ID");
                   vo.setVendorId(vendorId);
                   vo.setVendorCode(results.getString("MEMBER_NUMBER"));
                   vo.setProductSkuId(results.getString("PRODUCT_SUBCODE_ID"));
                   BigDecimal vendorCost = results.getBigDecimal("VENDOR_COST");
                   if( vendorCost==null ) {
                       vendorCost = new BigDecimal(0.00);
                   }
                   vo.setVendorCost(vendorCost.doubleValue());
                   vo.setVendorSku(results.getString("VENDOR_SKU"));
                   vo.setAvailable(StringUtils.equals("Y",results.getString("AVAILABLE")));
                   vo.setSubcodeDescription(results.getString("SUBCODE_DESCRIPTION"));
                      
                   trip.setShipPointId(shipPointId);
                   trip.setCarrierId(carrierId);                          
                   if (results.getString("DEPARTURE_DATE") != null)
                     trip.setZoneJumpDate(sdf.parse(results.getString("DEPARTURE_DATE")));
                   trip.setShipDate(shipDate);
                   trip.setShipMethod(sdsShipMethodId);
                   trip.setCarrierId(results.getString("OVERRIDE_CARRIER"));
                   trip.setTripId(results.getString("TRIP_ID"));                       
                   trip.setTrailerNumber(results.getString("ZONE_JUMP_TRAILER_NUMBER"));
                   trip.setBillingAccount(results.getString("BILLING_ACCOUNT"));
                   vo.setZoneJumpTrip(trip);
                   list.add(vo);
               }
           }     
         }
         
         logger.debug("getShippingVendorsByHub -- total records returned = " + list.size()); 
         
         return list;
     }

    /**
     * Updates the Zone Jump field for the venus record.
     * @param connection
     * @param venusMessageVO
     * @throws Exception
     */
    public void updateVenusZJ(Connection connection, VenusMessageVO venusMessageVO, SDSZoneJumpTripVO trip) throws Exception
    {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        String zoneJumpDateString = null; 
         
        try
        {
          if (trip.getZoneJumpDate() != null)
           zoneJumpDateString = sdf.format(trip.getZoneJumpDate());
        }
        catch (Exception e)
        {
          logger.debug(e.toString());
        }

        logger.debug("updateVenusZJ -- " +
                     " venusId = " + venusMessageVO.getVenusId() + 
                     " and zoneJumpDateString = " + zoneJumpDateString +
                     " and zoneJumpTrailerNumber = " + trip.getTrailerNumber());

        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("SHIP_UPDATE_VENUS_ZJ");
        dataRequest.addInputParam("IN_VENUS_ID", venusMessageVO.getVenusId());
        dataRequest.addInputParam("IN_ZONE_JUMP_FLAG", "Y");
        dataRequest.addInputParam("IN_ZONE_JUMP_LABEL_DATE", trip.getZoneJumpDate()== null ? null :new java.sql.Date(trip.getZoneJumpDate().getTime()));
        dataRequest.addInputParam("IN_ZONE_JUMP_TRAILER_NUMBER", trip.getTrailerNumber());
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
    }


    /**
     * Updates the Zone Jump status on the ZJ_TRIP table
     * @param connection
     * @param zoneJumpTrailer
     * @param tripStatus
     * @throws Exception
     */
    public void updateTripStatus(Connection connection, String zoneJumpTrailer, String tripStatus) throws Exception
    {
        logger.debug("updateTripStatus");
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("UPDATE_ZJ_TRIP_STATUS");
        dataRequest.addInputParam("IN_TRIP_ID", null);
        dataRequest.addInputParam("IN_TRIP_STATUS_CODE", tripStatus);
        dataRequest.addInputParam("IN_ZONE_JUMP_TRAILER_NUMBER", zoneJumpTrailer);
        dataRequest.addInputParam("IN_UPDATED_BY", "SHIP_PROCESSING_MANIFEST_ZJ");
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
    }
    
    
    /**
     * Assigns order to vendor. Decrement pallet count.
     * @param connection
     * @param venusMessageVO
     * @throws Exception
     */
    public void assignOrderToVendor(Connection connection, VenusMessageVO venusMessageVO, SDSZoneJumpTripVO trip) throws Exception
    {
        logger.debug("assignOrderToVendor");
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("SHIP_ASSIGN_ORDER_TO_VENDOR");
        dataRequest.addInputParam("IN_TRIP_ID", trip.getTripId());
        dataRequest.addInputParam("IN_VENDOR_ID", venusMessageVO.getVendorId());
        dataRequest.addInputParam("IN_ORDER_DETAIL_ID", venusMessageVO.getReferenceNumber());
        dataRequest.addInputParam("IN_VENUS_ID", venusMessageVO.getVenusId());
        dataRequest.addInputParam("IN_CREATED_BY", "SHIP_PROCESSING");
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
    }    
    
    public void updateInvTrkCount(Connection connection, VenusMessageVO venusMessageVO) throws Exception
    {
        updateInvTrkCount(connection, venusMessageVO.getVenusId(), venusMessageVO.getMsgType().getFtdMsgType());
    }    


    public void updateInvTrkCount(Connection connection, String venusId, String msgType) throws Exception
    {
        logger.debug("updateInvTrkCount");
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("SHIP_UPDATE_INV_TRK_COUNT");
        dataRequest.addInputParam("IN_UPDATE_AMOUNT", "1");
        dataRequest.addInputParam("IN_VENUS_ID", venusId);
        dataRequest.addInputParam("IN_MSG_TYPE", msgType);
    
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
    }    
    
    public void updateInvTrkAssignedCount(Connection connection, VenusMessageVO venusMessageVO) throws Exception
    {
        logger.debug("updateInvTrkAssignedCount");
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("SHIP_UPDATE_INV_TRK_ASSIGNED_COUNT");
        dataRequest.addInputParam("IN_UPDATE_AMOUNT", "1");
        dataRequest.addInputParam("IN_VENUS_ID", venusMessageVO.getVenusId());
        dataRequest.addInputParam("IN_MSG_TYPE", venusMessageVO.getMsgType().getFtdMsgType());
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
    }      
    
    public void insertShipmentOption(Connection connection, ShipmentOptionVO sovo) throws Exception
    {
        logger.debug("insertShipmentOption");
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("SHIP_INSERT_SHIPMENT_OPTION");
        dataRequest.addInputParam("IN_SDS_VENUS_ORDER_NUMBER", sovo.getVenusOrderNumber());
        dataRequest.addInputParam("IN_SDS_CARRIER_ID", sovo.getCarrierId());
        dataRequest.addInputParam("IN_SDS_SHIP_METHOD_ID", sovo.getShipMethodId());
        dataRequest.addInputParam("IN_SDS_ZONE_CODE", sovo.getZoneCode());
        dataRequest.addInputParam("IN_SDS_RATE_WEIGHT", sovo.getRateWeight());
        dataRequest.addInputParam("IN_SDS_RATE_CHARGE", sovo.getRateCharge());
        dataRequest.addInputParam("IN_SDS_HANDLING_CHARGE", sovo.getHandlingCharge());
        dataRequest.addInputParam("IN_SDS_TOTAL_CHARGE", sovo.getTotalCharge());
        dataRequest.addInputParam("IN_SDS_FREIGHT_CHARGE", sovo.getFreightCharge());
        dataRequest.addInputParam("IN_SDS_RATE_CALC_TYPE", sovo.getRateCalcType());
        dataRequest.addInputParam("IN_SDS_DATE_SHIPPED", sovo.getDateShipped() == null ? null : new java.sql.Date(sovo.getDateShipped().getTime()));
        dataRequest.addInputParam("IN_SDS_EXPECTED_DELIVERY_DATE", sovo.getExpectedDeliveryDate() == null ? null : new java.sql.Date(sovo.getExpectedDeliveryDate().getTime()));
        dataRequest.addInputParam("IN_SDS_DAYS_IN_TRANSIT", sovo.getDaysInTransit());
        dataRequest.addInputParam("IN_SDS_DISTRIBUTION_CENTER", sovo.getDistributionCenter());
        dataRequest.addInputParam("IN_SDS_SHIP_POINT_ID", sovo.getShipPointId());
        dataRequest.addInputParam("IN_SDS_ERROR_TXT", sovo.getErrorTxt());
        dataRequest.addInputParam("IN_ZONE_JUMP_FLAG", sovo.getZoneJumpFlag());
        dataRequest.addInputParam("IN_IDEAL_RANK_NUMBER", Integer.valueOf(sovo.getIdealRankNumber()));
        dataRequest.addInputParam("IN_ACTUAL_RANK_NUMBER", Integer.valueOf(sovo.getActualRankNumber()));

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
    }


  /**
   * Get a list of vendors that can ship a product based on the ship/delivery dates for a recipient postal code
   * (new)
   *
   * @param conn database connection
   * @param productSubcodeId product id or product subcode id
   * @param deliveryDate filter
   * @param shipDate filter
   * @param postalCode filter
   * @return list of ShipVendorProductVO
   * @throws Exception
   */
  public List<ShipVendorProductVO> getShippingVendorsAll(Connection conn, String productSubcodeId, Date deliveryDate, Date shipDate, String postalCode) throws Exception
  {
    ArrayList<ShipVendorProductVO> list = new ArrayList<ShipVendorProductVO>();

    CachedResultSet results = null;
    CachedResultSet results2 = null;

    DataRequest dataRequest = new DataRequest();
    dataRequest.reset();
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("SHIP_GET_SHIPPING_VENDORS_ALL");

    HashMap inputParams = new HashMap();
    inputParams.put("IN_PRODUCT_SUBCODE_ID", productSubcodeId);
    inputParams.put("IN_DELIVERY_DATE", new java.sql.Date(deliveryDate.getTime()));
    inputParams.put("IN_SHIP_DATE", new java.sql.Date(shipDate.getTime()));
    dataRequest.setInputParams(inputParams);

    results = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);

    if (results != null)
    {
      while (results.next())
      {
        ShipVendorProductVO vo = new ShipVendorProductVO();
        List<CarrierVO> carrierList = new ArrayList<CarrierVO>();
        String vendorId = results.getString("vendor_id");

        vo.setVendorId(vendorId);
        vo.setVendorCode(results.getString("member_number"));
        vo.setProductSkuId(results.getString("product_subcode_id"));

        BigDecimal vendorCost = results.getBigDecimal("vendor_cost");
        if (vendorCost == null)
          vendorCost = new BigDecimal(0.00);

        vo.setVendorCost(vendorCost.doubleValue());
        vo.setVendorSku(results.getString("vendor_sku"));
        vo.setAvailable(StringUtils.equalsIgnoreCase("Y", results.getString("available")));
        vo.setRemoved(StringUtils.equalsIgnoreCase("Y", results.getString("removed")));
        vo.setSubcodeDescription(results.getString("subcode_description"));

        //Now call venus.ship_pkg.get_carriers_for_product
        DataRequest dataRequest2 = new DataRequest();
        dataRequest2.reset();
        dataRequest2.setConnection(conn);
        dataRequest2.setStatementID("SHIP_GET_SDS_VENDOR_CARRIERS");

        inputParams = new HashMap();
        inputParams.put("IN_VENDOR_ID", vendorId);
        inputParams.put("IN_POSTAL_CODE", postalCode);
        dataRequest2.setInputParams(inputParams);

        results2 = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest2);

        if (results2 != null)
        {
          while (results2.next())
          {
            String returnCarrierId = results2.getString("CARRIER_ID");
            if (StringUtils.isNotBlank(returnCarrierId))
            {
              CarrierVO cvo = new CarrierVO();
              cvo.setCarrierId(returnCarrierId);
              carrierList.add(cvo);
            }
          }
        }

        //Don't add vendors that don't have assigned carriers
        if (carrierList.size() > 0)
        {
          vo.setCarrierIds(carrierList);
          list.add(vo);
        }
      }
    }

    return list;
  }


  /**
     * This method receives all the parms for FTD_APPS.VENDOR_PRODUCT_OVERRIDE table, and determines if
     * a block exist for the data provided.
     *
     * @param conn database connection
     * @param vendorId
     * @param productId
     * @param deliveryDate
     * @param carrierId
     * @param sdsShipMethodID
     *
     * @return true if a block exists; false if a block does not exist
     *
     * @throws java.lang.Exception
     */
  public boolean shipMethodBlockExists(Connection conn, String vendorId, String productId, Date deliveryDate, String carrierId, String sdsShipMethodId)
         throws Exception
  {
    DataRequest dataRequest = new DataRequest();

    /* build DataRequest object */
    dataRequest.reset();
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("SHIP_SHIP_METHOD_BLOCK_EXISTS");

    /* setup store procedure input parameters */
    dataRequest.addInputParam("IN_VENDOR_ID", vendorId);
    dataRequest.addInputParam("IN_PRODUCT_SUBCODE_ID", productId);
    dataRequest.addInputParam("IN_DELIVERY_OVERRIDE_DATE", deliveryDate == null ? null : new java.sql.Date(deliveryDate.getTime()));
    dataRequest.addInputParam("IN_CARRIER_ID", carrierId);
    dataRequest.addInputParam("IN_SDS_SHIP_METHOD", sdsShipMethodId);

    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    String sSDSShipMethodBlockExists = (String) dataAccessUtil.execute(dataRequest);

    boolean bSDSShipMethodBlockExists = sSDSShipMethodBlockExists != null && StringUtils.equalsIgnoreCase(sSDSShipMethodBlockExists, "Y") ? true : false; 

    return bSDSShipMethodBlockExists;

  }
  
    /**
    * Updates the venus add on sku and price
    * 
    * @param connection Connection
    * @param String venusID
    * @param String vendorAddOnId
    * @param String vendorPrice
    * @throws Exception 
    */
    public void updateVenusAddOns(Connection connection, String venusId, String vendorAddOnSku, String vendorPrice, String addOnId) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("SHIP_UPDATE_VENUS_ADD_ONS");
        dataRequest.addInputParam("IN_VENUS_ID", venusId);
        dataRequest.addInputParam("IN_ADDON_ID", addOnId);
        dataRequest.addInputParam("IN_VENDOR_SKU", vendorAddOnSku);
        dataRequest.addInputParam("IN_PER_ADDON_PRICE", new BigDecimal(vendorPrice));
        dataRequest.addInputParam("IN_UPDATED_BY", "SHIP_PROCESSING");
                
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
    }

    /**
     * Insert the contents of a carrier zip code into the database
     * @param connection database conneciton
     * @param CarrierZipVO
     */
    public void insertUpdateCarrierZips(Connection connection, CarrierZipVO carrierZipVO) throws Exception {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("SHIP_INSERT_CARRIER_ZIP_CODE");
        
        dataRequest.addInputParam("IN_ZIP_CODE",carrierZipVO.getZipCode());
        dataRequest.addInputParam("IN_CARRIER_ID",carrierZipVO.getCarrierId());
        dataRequest.addInputParam("IN_WEEKDAY_BY_NOON_DEL_FLAG",carrierZipVO.getWeekdayByNoonDelFlag());
        dataRequest.addInputParam("IN_SATURDAY_BY_NOON_DEL_FLAG",carrierZipVO.getSaturdayByNoonDelFlag());
        dataRequest.addInputParam("IN_SATURDAY_DEL_AVAILABLE_FLAG",carrierZipVO.getSaturdayDelAvailable());
        dataRequest.addInputParam("IN_USER_ID",carrierZipVO.getUserId());
         
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }                
    }
    
    /**
     * Deletes unprocessed zip codes
     * @param connection database connection
     * @param carrierId 
     */
    public void deleteCarrierZips(Connection connection, String carrierId) throws Exception {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("SHIP_DELETE_CARRIER_ZIP_CODE");
         
        dataRequest.addInputParam("IN_CARRIER_ID",carrierId);
         
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
    }
    
    /**
     * Get email list for sending zip file not received alert
     * @param connection database connection
     */
    public String doGetZipCodeReportEmailList(Connection con) throws Exception
    {
        logger.debug("Entering doGetZipCodeReportEmailList");
        StringBuffer text = new StringBuffer();
        String delimiter = ",";
        
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.reset();
            dataRequest.setStatementID("GET_ZIP_CODE_REPORT_EMAIL_LIST");
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            CachedResultSet outrs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

            while(outrs.next()) {
                text.append(outrs.getString("email_id"));
                text.append(";");
            }
        } finally {
            logger.debug("Exiting doGetZipCodeReportEmailList");
        }
        return text.toString();        
    }
    
    /**
     * Get date for when the last zip code file was received
     * @param connection database connection
     * @param carrierId
     */
    public String doGetLastZipFileReceivedDate(Connection con, String carrierId) throws Exception
    {
        logger.debug("Entering doGetLastZipFileReceivedDate..");
        String zip_file_date = "";        
        
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.reset();
            dataRequest.setStatementID("GET_LAST_ZIP_FILE_RECEIVED_DATE");
            dataRequest.addInputParam("IN_CARRIER_ID",carrierId);
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            CachedResultSet outrs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

            while(outrs.next()) {
            	zip_file_date = outrs.getString("created_on");                
            }
        } finally {
            logger.debug("Exiting doGetLastZipFileReceivedDate..");
        }
        return zip_file_date;        
    }
    
    /**
     * Defect 12761
     * Insert a "shipment notification" record into the ptn_amazon.az_fulfillmemnt table for the associated order
     * Calls pi.pi_maint_pkg.insert_outbound_message
     * @param conn database connection
     * @param associatedOrderVO representing partner order in venus.venus
     * @param partnersVO partner record
     * @throws Exception for all errors and exceptions
     */
    public void insertAmazonPartnerShipment(Connection conn,VenusMessageVO associatedOrderVO) throws Exception {
        Long retval = null;
        
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("SHIP_INSERT_AMAZON_SHIP_MESSAGE");
        
        dataRequest.addInputParam("IN_VENUS_ORDER_NUM",associatedOrderVO.getVenusOrderNumber());
        dataRequest.addInputParam("IN_SHIP_DATE", associatedOrderVO.getShippedStatusDate() == null ? null : new java.sql.Timestamp(associatedOrderVO.getShippedStatusDate().getTime()));
        //dataRequest.addInputParam("IN_SHIPPING_METHOD", associatedOrderVO.getFinalShipMethod());
        dataRequest.addInputParam("IN_SHIPPING_METHOD", associatedOrderVO.getShipMethod());
        dataRequest.addInputParam("IN_CARRIER_NAME", associatedOrderVO.getFinalCarrier());
        //dataRequest.addInputParam("IN_CARRIER_NAME", associatedOrderVO.getShippingSystem());
        dataRequest.addInputParam("IN_TRACKING_NUMBER", associatedOrderVO.getTrackingNumber());
        //dataRequest.addInputParam("IN_USER",associatedOrderVO.getVenusOrderNumber());
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
    }
    
    public void insertMercentPartnerShipment(Connection conn,VenusMessageVO associatedOrderVO) throws Exception {
        Long retval = null;
        
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("INSERT_MERCENT_SHIP_MESSAGE");
        
        dataRequest.addInputParam("IN_VENUS_ORDER_NUM",associatedOrderVO.getVenusOrderNumber());
        dataRequest.addInputParam("IN_SHIP_DATE", associatedOrderVO.getShippedStatusDate() == null ? null : new java.sql.Timestamp(associatedOrderVO.getShippedStatusDate().getTime()));
        dataRequest.addInputParam("IN_SHIPPING_METHOD", associatedOrderVO.getShipMethod());
        dataRequest.addInputParam("IN_CARRIER_NAME", associatedOrderVO.getFinalCarrier());
        dataRequest.addInputParam("IN_TRACKING_NUMBER", associatedOrderVO.getTrackingNumber());
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
    }

}
