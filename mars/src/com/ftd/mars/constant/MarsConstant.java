/**
 * 
 */
package com.ftd.mars.constant;



/**
 * Constants that are used by all the classes
 * 
 * @author kdatchan
 *
 */
public interface MarsConstant {

	static String MARS_CONTEXT = "MARS_CONFIG";
	static String PHOENIX_CONTEXT = "PHOENIX";
	static String FTDAPPS_PARMS_CONTEXT = "FTDAPPS_PARMS";
	static String ORDER_PROCESSING_CONTEXT = "ORDER_PROCESSING";
	static String LMD_CONTEXT = "LOAD_MEMBER_DATA";

	static String PHOENIX_ENABLED = "PHOENIX_ENABLED";
	static String ARTIFACT_ID = "ARTIFACT_ID";
	static String GROUP_ID = "GROUP_ID";
	static String VERSION = "VERSION";
	static String SCAN_INTERVAL = "SCAN_INTERVAL";
	static String FLORIST_SOFT_BLOCK_DAYS = "FLORIST_SOFT_BLOCK_DAYS";
	static String PROPERTY_FILE = "mars_config.xml";
	static String MESSAGE_SOURCE = "MESSAGE_SOURCE";
	static String DATASOURCE = "MARS_DS";
	static String ERROR = "ERROR";
	static String SUBJECT = "MARS";
	static String PAGE_SOURCE = "MARS_PAGE";	
	static String NOPAGE_SOURCE = "MARS_NO_PAGE";

	static String MERCURY_INBOUND_QUEUE = "MERCURYINBOUND";
	static String MARS_MERCURY_QUEUE = "MARSMERCURY";
	static String JMS_DELAY_PROPERTY = "JMS_OracleDelay";
	static String Y_STRING = "Y";
	static String N_STRING = "N";

	static String GLOBAL_MONDAY_CUTOFF = "MONDAY_CUTOFF";
	static String GLOBAL_TUESDAY_CUTOFF = "TUESDAY_CUTOFF";
	static String GLOBAL_WEDNESDAY_CUTOFF = "WEDNESDAY_CUTOFF";
	static String GLOBAL_THURSDAY_CUTOFF = "THURSDAY_CUTOFF";
	static String GLOBAL_FRIDAY_CUTOFF = "FRIDAY_CUTOFF";
	static String GLOBAL_SATURDAY_CUTOFF = "SATURDAY_CUTOFF";
	static String GLOBAL_SUNDAY_CUTOFF = "SUNDAY_CUTOFF";
	static String GLOBAL_REJECT_RETRIES = "REJECT_RETRIES";
	static String GLOBAL_MARS_ORDER_BOUNCE_COUNT = "MARS_ORDER_BOUNCE_COUNT";
	static String AUTO_RESP_BEFORE_CUTOFF = "AUTO_RESPONSE_OFF_BEFORE_CUTOFF_MINUTES";
	static String WEB_SERVICE_URL = "WEB_SERVICE_URL";
	static String REPORT_WS_CACHE = "REPORT_WS_CACHE";
	static String XLS = "XLS";
	static String MARS = "MARS";
	static String MARS_ACTION_DETAIL = "MARS_Action_Detail_Report";
	static String FLORIST_SUMMARY = "Florist_Message_Summary_Report";
	static String SUCCESS = "SUCCESS";
	static String BRE_TIMEOUT = "BRE_TIMEOUT";

	static String VIEW_QUEUE_HOLIDAY_DATE = "VIEW_QUEUE_HOLIDAY_DATE";
	static String VIEW_QUEUE_SWITCH = "VIEW_QUEUE_SWITCH";

	static String US_COUNTRY_CODE = "US";
	static String CA_COUNTRY_CODE = "CA";
	
	static String SKIP_MESSAGES_BY_MARS = "SKIP_MESSAGES_BY_MARS";

}


