/**
 * 
 */
package com.ftd.mars.exception;

/**
 * @author kdatchan
 *
 */
public class MarsException extends Exception
{
	private static final long serialVersionUID = 1L;

	public MarsException() {
		super();
	}

	public MarsException(String message, Throwable cause) {
		super(message, cause);
	}

	public MarsException(String message) {
		super(message);
	}

	public MarsException(Throwable cause) {
		super(cause);
	}
	
}
