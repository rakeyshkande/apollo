/**
 * 
 */
package com.ftd.mars.exception;

/**
 * @author vgutala
 * This Customized exception is used to throw when a particular message type skip from mars processing.
 */
public class MercuryMessageTypeException extends Exception
{
	private static final long serialVersionUID = 1L;

	public MercuryMessageTypeException() {
		super();
	}

	public MercuryMessageTypeException(String message, Throwable cause) {
		super(message, cause);
	}

	public MercuryMessageTypeException(String message) {
		super(message);
	}

	public MercuryMessageTypeException(Throwable cause) {
		super(cause);
	}
	
}
