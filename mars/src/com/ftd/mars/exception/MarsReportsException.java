/**
 * 
 */
package com.ftd.mars.exception;

/**
 * @author kdatchan
 *
 */
public class MarsReportsException extends Exception
{
	private static final long serialVersionUID = 1L;

	public MarsReportsException() {
		super();
	}

	public MarsReportsException(String message, Throwable cause) {
		super(message, cause);
	}

	public MarsReportsException(String message) {
		super(message);
	}

	public MarsReportsException(Throwable cause) {
		super(cause);
	}
	
}
