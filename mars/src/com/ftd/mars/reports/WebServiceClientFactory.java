package com.ftd.mars.reports;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.xml.ws.WebServiceException;

import com.ftd.mars.constant.MarsConstant;
import com.ftd.mars.exception.MarsException;
import com.ftd.mars.util.MarsUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.report.service.ReportWebService;
import com.ftd.report.service.ReportWebService_Service;

/**
 * CXF implementation for getting Report Service's WS instance
 * 
 * @author kdatchan
 * 
 */
public class WebServiceClientFactory {

	private static final Logger logger = new Logger(
			WebServiceClientFactory.class.getName());
	private static Map<String, ReportWebService> threadInstancesMap = new HashMap<String, ReportWebService>();

	public ReportWebService getReportService() throws Exception {
		StringBuffer errorMessage = null;
		ReportWebService reportService = null;
		String key = null;

		if (logger.isDebugEnabled()) {
			logger.debug("Getting the Report Web Service Intance");
		}

		try {
			key = MarsConstant.REPORT_WS_CACHE;

			logger.debug("Key to get the web service instance: " + key);

			if (threadInstancesMap.containsKey(key)) {
				logger.debug("Getting the cached instance.");
				reportService = threadInstancesMap.get(key);
			} else {

				if (logger.isDebugEnabled()) {
					logger.debug(" Creating the Report web service instance.");
				}

				String svsUrlString = MarsUtil.getReportWebServiceUrl();
				ReportWebService_Service ems = new ReportWebService_Service(new URL(
						svsUrlString));
				reportService = ems.getReportWebServicePort();

				if (reportService == null) {
					errorMessage = new StringBuffer();
					errorMessage.append(" Report Web Service instance is null.");
					logger.error(errorMessage.toString());
					throw new MarsException(errorMessage.toString());

				}

				threadInstancesMap.put(key, reportService);
			}

			if (logger.isDebugEnabled()) {
				logger.debug("Size of the cache:" + threadInstancesMap.size());
			}

			return reportService;
		} catch (MalformedURLException e1) {

			errorMessage = new StringBuffer();
			errorMessage
					.append(" Unable to create URL object for report service wsdl url string.");
			logger.error(errorMessage.toString());
			throw new MarsException(e1);

		} catch (WebServiceException wsException) {

			errorMessage = new StringBuffer();
			errorMessage.append(" web service exception occured.");
			logger.error(errorMessage.toString());
			throw new MarsException(wsException);

		} catch (Exception e) {

			errorMessage = new StringBuffer();
			errorMessage.append(" Exception occured.");
			logger.error(errorMessage.toString());
			throw new MarsException(e);
		}
	}

	public void removeThreadInstance() {
		threadInstancesMap.clear();
	}
}
