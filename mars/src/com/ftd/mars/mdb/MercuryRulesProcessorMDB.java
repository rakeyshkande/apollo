package com.ftd.mars.mdb;

import javax.ejb.EJBException;
import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import com.ftd.mars.processor.MercuryRulesAppContext;
import com.ftd.mars.processor.MercuryRulesProcessor;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * MDB that consumes the mercury inbound messages from
 * ojms.mars_mercury queue and processes the requests
 * 
 * @author kdatchan
 *
 */
public class MercuryRulesProcessorMDB implements MessageDrivenBean, MessageListener {

	private static final long serialVersionUID = 1L;

	MessageDrivenContext messageDrivenContext;
	
  private static Logger LOGGER = new Logger(MercuryRulesProcessorMDB.class.getName());

  @Override
  public void onMessage(Message message) {

    // declare and initialize the variables
    String mercuryId = null;
    MercuryRulesProcessor rulesProcessor;

    try {
    	long startTime = System.currentTimeMillis();
      TextMessage textMessage = (TextMessage) message;
      mercuryId = textMessage.getText();
      rulesProcessor = MercuryRulesAppContext.getMercuryRulesProcessor();
      rulesProcessor.process(mercuryId);
      LOGGER.info(new StringBuffer("Total time taken to process request in millis: ").append((System.currentTimeMillis() - startTime)).toString());
    }

    catch (Throwable e) {
    	LOGGER.error("Exception caught while trying to execute the rules. Putting the message to MARS_MERCURY queue. Exception message: " + e.getMessage(), e);
			throw new RuntimeException();
    }

  }

  public void ejbRemove() throws EJBException {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(".ejbRemove():: ***In MercuryRulesProcessorMDB remove()***");
    }
  }

  public void ejbCreate() {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(".ejbCreate():: ***In MercuryRulesProcessorMDB create()***");
    }
  }

  @Override
  public void setMessageDrivenContext(MessageDrivenContext ctx) throws EJBException {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(".setMessageDrivenContext():: ***In MercuryRulesProcessorMDB setMessageDrivenContext()**");
    }
    this.messageDrivenContext = ctx;
  }

}
