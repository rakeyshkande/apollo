package com.ftd.mars.mdb;

import javax.ejb.EJBException;
import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import com.ftd.mars.processor.MarsReportsProcessor;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * MDB that consumes the message from
 * ojms.mars_reports queue and processes the requests
 * 
 * @author kdatchan
 *
 */
public class MarsReportsMDB implements MessageDrivenBean, MessageListener {

	private static final long serialVersionUID = 1L;

	MessageDrivenContext messageDrivenContext;
	
  private static Logger LOGGER = new Logger(MarsReportsMDB.class.getName());

  @Override
  public void onMessage(Message message) {

    // declare and initialize the variables
    String messageId = null;

    try {
    	long startTime = System.currentTimeMillis();
      TextMessage textMessage = (TextMessage) message;
      messageId = textMessage.getText();
      MarsReportsProcessor mrp = new MarsReportsProcessor();
      mrp.processReports(messageId);
      LOGGER.info(new StringBuffer("Total time taken to process request in millis: ").append((System.currentTimeMillis() - startTime)).toString());
    }

    catch (Throwable e) {
    	LOGGER.error("Exception caught while trying to submit the report. Putting the message to MARS_REPORTS queue. Exception message: " + e.getMessage());
			throw new RuntimeException();
    }

  }

  public void ejbRemove() throws EJBException {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(".ejbRemove()::" + "***In MarsReportsMDB remove()***");
    }
  }

  public void ejbCreate() {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(".ejbCreate()::" + "***In MarsReportsMDB create()***");
    }
  }

  @Override
  public void setMessageDrivenContext(MessageDrivenContext ctx) throws EJBException {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(".setMessageDrivenContext()::"
        + "***In MarsReportsMDB setMessageDrivenContext()**");
    }
    this.messageDrivenContext = ctx;
  }

}
