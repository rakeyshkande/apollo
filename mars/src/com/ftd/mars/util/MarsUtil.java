/**
 * 
 */
package com.ftd.mars.util;

import java.io.IOException;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.naming.InitialContext;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import com.ftd.constant.MercuryConstants;
import com.ftd.constant.OrderConstants;
import com.ftd.core.domain.Customer;
import com.ftd.core.domain.Mercury;
import com.ftd.core.domain.MercuryRulesTracker;
import com.ftd.core.domain.OrderDetails;
import com.ftd.core.domain.Orders;
import com.ftd.core.domain.request.MarsVO;
import com.ftd.core.domain.response.RulesResponse;
import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.mars.constant.MarsConstant;
import com.ftd.mars.exception.MarsException;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.PartnerVO;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.report.service.ReportRequest;
import com.ftd.rulesaction.constant.RulesActionConstants;
import com.ftd.rulesaction.util.RulesActionUtil;
import com.ftd.rulesexecutor.core.domain.RuleExecutionContext;
import com.ftd.rulesexecutor.core.domain.RuleModuleConfig;


/**
 * An utility class that helps in construction, preparing etc. objects
 * that are required by the core classes
 * 
 * @author kdatchan
 *
 */
public class MarsUtil {
	
	private static final Logger LOGGER = Logger.getLogger(MarsUtil.class);
	
	/**
	 * Gets the preferredPartnerId from the cache for the given source code
	 * 
	 * @param sourceCode
	 * @return preferredPartnerId
	 * @throws MarsException
	 */
	public static String getPreferredPartnerId(String sourceCode) throws MarsException {
		String preferredPartnerId = null;

    try {
      PartnerVO partnerVO = FTDCommonUtils.getPreferredPartnerBySource(sourceCode);
      if (partnerVO != null) {
      	preferredPartnerId = partnerVO.getPartnerName();	
      }
      
    }
    catch (Exception e) {
    	LOGGER.error("Exception caught when getting the preferredPartnerId", e);
    }
    
		return preferredPartnerId;
	}
	
	/**
	 * Constructs the RuleModuleConfig object that is required by the
	 * Rules processor class inorder to get the Rule engine object
	 * 
	 * @return RuleModuleConfig
	 * @throws MarsException
	 */
	public static RuleModuleConfig getRuleModuleConfig() throws MarsException {
		RuleModuleConfig ruleModuleConfig = new RuleModuleConfig();
		ruleModuleConfig.setArtifactId(getArtifactId());
		ruleModuleConfig.setGroupId(getGroupId());
		ruleModuleConfig.setVersion(getVersion());
		ruleModuleConfig.setScanInterval(getScanInterval());
		return ruleModuleConfig;
	}

	/**
	 * Constructs the RuleExecutionContext object for the given 
	 * rule domain objects
	 * 
	 * @return RuleExecutionContext
	 * @throws Exception
	 */
	public static RuleExecutionContext getRuleFacts(MarsVO marsVO, RulesResponse response) throws Exception {
		return new RuleExecutionContext(marsVO, response);
	}
	
	/**
	 * Constructs/prepares MarVO object using all the dependent objects.
	 * 
	 * @param mercuryMessage
	 * @param associatedMessage
	 * @param orderDetails
	 * @param orders
	 * @param recipient
	 * @param customer
	 * @param timeZone
	 * @param orderStatus
	 * @param phoenixEligible
	 * @param marsOrderBounceCount
	 * @return MarsVO
	 * @throws MarsException
	 */
	public static MarsVO getMarsVO(Mercury mercuryMessage, Mercury associatedMessage, OrderDetails orderDetails, Orders orders, Customer recipient, Customer customer, String timeZone, boolean orderStatus, String phoenixEligible, boolean citySearchOrder, long marsOrderBounceCount) throws MarsException {
		MarsVO mars = new MarsVO();
		if (mercuryMessage.getPrice() != null) {
			mars.setAskpThreshold(mercuryMessage.getPrice().subtract(associatedMessage.getPrice()));
		}
		
		if (customer.getAddress1() != null) {
			mars.setCustomerAddress(customer.getAddress1());	
		}
		if (customer.getAddress2() != null) {
			if (mars.getCustomerAddress() != null) {
				mars.setCustomerAddress(mars.getCustomerAddress() + " " + customer.getAddress2());
			}
			else {
				mars.setCustomerAddress(customer.getAddress2());
			}
		}
		
		mars.setCustomerCity(customer.getCity());
		mars.setCustomerCountry(customer.getCountry());
		mars.setCustomerFirstName(customer.getFirstName());
		mars.setCustomerLastName(customer.getLastName());
		if (customer.getCustomerPhone() != null) {
			mars.setCustomerPhoneNumber(customer.getCustomerPhone().getPhoneNumber());
		}
		mars.setCustomerState(customer.getState());
		mars.setCustomerZipCode(customer.getZipCode());
		mars.setDeliveryDate(orderDetails.getDeliveryDate());
		mars.setDirection(mercuryMessage.getMessageDirection());
		mars.setOrderDetailId(orderDetails.getOrderDetailId());
		mars.setMercuryId(mercuryMessage.getMercuryId());
		mars.setMercuryNumber(mercuryMessage.getMercuryMessageNumber());
		mars.setExternalOrderNumber(orderDetails.getExternalOrderNumber());
		//Q4SP16-16 : MARS | Multiple lines in the message text
		if (mercuryMessage.getComments() != null) {
			String mercMsgComments = mercuryMessage.getComments();
			mercMsgComments = mercMsgComments.replaceAll("[\\t\\n\\r]", " ");
			mercMsgComments = mercMsgComments.replaceAll(" +", " ");
			mars.setMessageText(mercMsgComments.toUpperCase());
		}
		mars.setMessageType(mercuryMessage.getMessageType());
		if (MercuryConstants.ASK.equalsIgnoreCase(mercuryMessage.getMessageType()) || MercuryConstants.ANS.equalsIgnoreCase(mercuryMessage.getMessageType())) {
			mars.setMessageType(mercuryMessage.getMessageType() + (mercuryMessage.getAskAnswerCode() != null ? mercuryMessage.getAskAnswerCode() : ""));
		}
		mars.setAskAnswerCode(mercuryMessage.getAskAnswerCode());
		mars.setNewMercuryPrice(mercuryMessage.getPrice());
		mars.setOccasion(associatedMessage.getOccasion());
		mars.setOrderBounceCount(orderDetails.getRejectRetryCount());
		mars.setOrderDate(orders.getOrderDate());
		mars.setOrderOrigin(orders.getOriginId());
		mars.setOriginalMercuryPrice(associatedMessage.getPrice());
		mars.setSourceCode(orders.getSourceCode());
		mars.setPreferredPartnerId(getPreferredPartnerId(mars.getSourceCode()));
		mars.setProductId(orderDetails.getProductId());
		if (recipient.getAddress1() != null) {
			mars.setRecipientAddress(recipient.getAddress1());	
		}
		if (recipient.getAddress2() != null) {
			if (mars.getRecipientAddress() != null) {
				mars.setRecipientAddress(mars.getRecipientAddress() + " " + recipient.getAddress2());
			}
			else {
				mars.setRecipientAddress(recipient.getAddress2());
			}
		}
		
		if (recipient.getCity() != null) {
			mars.setDeliveryCity(recipient.getCity().toUpperCase());
			mars.setRecipientCity(recipient.getCity().toUpperCase());
		}
		if (recipient.getState() != null) {
			mars.setDeliveryState(recipient.getState().toUpperCase());
			mars.setRecipientState(recipient.getState().toUpperCase());
		}
		if (recipient.getZipCode() != null) {
			mars.setDeliveryZipCode(recipient.getZipCode().toUpperCase());
			if (recipient.getCountry() != null) {
				if (MarsConstant.US_COUNTRY_CODE.equalsIgnoreCase(recipient.getCountry())) {
					mars.setDeliveryZipCode(recipient.getZipCode().trim().toUpperCase().substring(0, 5));
				}
				else if (MarsConstant.CA_COUNTRY_CODE.equalsIgnoreCase(recipient.getCountry())) {
					mars.setDeliveryZipCode(recipient.getZipCode().trim().toUpperCase().substring(0, 3));
				}
			}
			mars.setRecipientZipCode(recipient.getZipCode().toUpperCase());
		}
		if (recipient.getCountry() != null) {
			mars.setRecipientCountry(recipient.getCountry().toUpperCase());
		}
		
		mars.setRecipientFirstName(recipient.getFirstName());
		mars.setRecipientLastName(recipient.getLastName());
		if (recipient.getCustomerPhone() != null) {
			mars.setRecipientPhoneNumber(recipient.getCustomerPhone().getPhoneNumber());
		}
		mars.setSakText(mercuryMessage.getSakText());
		mars.setSourceCode(orders.getSourceCode());
		try {
			mars.setFloristSoftBlockDays(RulesActionUtil.getSoftBlockFloristDays());
		} 
		catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
		mars.setGlobalMondayCutoff(getGlobalMondayCutoff());
		mars.setGlobalTuesdayCutoff(getGlobalTuesdayCutoff());
		mars.setGlobalWednesdayCutoff(getGlobalWednesdayCutoff());
		mars.setGlobalThursdayCutoff(getGlobalThursdayCutoff());
		mars.setGlobalFridayCutoff(getGlobalFridayCutoff());
		mars.setGlobalSaturdayCutoff(getGlobalSaturdayCutoff());
		mars.setGlobalSundayCutoff(getGlobalSundayCutoff());
		mars.setAutoResponseCutoffStop(getAutoResponseCutoffStop());
		mars.setGlobalRejectRetryLimit(getGlobalRejectRetryLimit());
		mars.setGlobalMarsOrderCount(getGlobalMarsOrderCount());
		mars.setTimeZone(timeZone);
		mars.setOrderStatus(MarsUtil.getOrderStatus(orderStatus));
		mars.setPhoenixEligible(MarsUtil.getPhoenixEligible(phoenixEligible));
		mars.setMarsOrderBounceCount(marsOrderBounceCount);
		mars.setCutOffTime(getCutoff(mars));
		mars.setViewQueueHolidayDate(MarsUtil.getViewQueueHolidayDate());
		mars.setViewQueueControl(MarsUtil.getViewQueueFlag());
		mars.setViewQueue(mercuryMessage.getViewQueue());
		mars.setCitySearchOrder(MarsUtil.getCitySearchOrder(citySearchOrder));
		
		return mars;
	}

	private static Date getViewQueueHolidayDate() throws MarsException {
		String dateString = getGlobalParam(MarsConstant.MARS_CONTEXT, MarsConstant.VIEW_QUEUE_HOLIDAY_DATE);
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		if (dateString != null) {
			try {
				return df.parse(dateString);
			} 
			catch (ParseException e) {
				LOGGER.error("Unable to parse the View Queue Holiday Date param. But the process will still continue");
			}
		}
		return null;
	}

	/**
	 * 
	 * 
	 * @param orderStatus
	 * @return String
	 */
	public static String getOrderStatus(boolean orderStatus) {

		if (orderStatus) {
			return OrderConstants.ORDER_STATUS_CANCELLED;
		}
		return null;
	}
	
	/**
	 * A wrapper method that sends system message
	 * 
	 * @param message
	 * @param messageSource
	 * @param type
	 */
	public static void sendSystemMessageWrapper(String message, String messageSource, String type) throws MarsException {

		LOGGER.error(message);
		Connection conn = null;

		try {
			String messageId = "";
			SystemMessengerVO sysMessage = new SystemMessengerVO();
			sysMessage.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
			sysMessage.setSource(messageSource);
			sysMessage.setType(type);
			sysMessage.setSubject(MarsConstant.SUBJECT);
			sysMessage.setMessage(message);
			SystemMessenger sysMessenger = SystemMessenger.getInstance();
			conn = getConnection();

			messageId = sysMessenger.send(sysMessage, conn);

			if (messageId == null) {
				String msg = "Error occured while attempting to send out a system message. Msg not sent: " + message;
				LOGGER.error(msg);
			}
		} 
		catch (Exception e) {
			LOGGER.error("Could not send system message: " + e.getMessage(), e);
			throw new MarsException(e);
		} 
		finally {
			try {
				if (conn != null) {
					conn.close();	
				}
			} catch (Exception e) {
				LOGGER.error(e);
			}
		}

	}
  
	/**
	 * Get the DB connection based on the datasource value
	 * 
	 * @return Connection
	 * @throws MarsException
	 */
  public static Connection getConnection() throws MarsException {

    ConfigurationUtil config;
		try {
			config = ConfigurationUtil.getInstance();
	    String dbConnection = config.getProperty(MarsConstant.PROPERTY_FILE,MarsConstant.DATASOURCE);
	    return DataSourceUtil.getInstance().getConnection(dbConnection);
		} 
		catch (IOException e) {
			LOGGER.error("Exception caught while trying to get the connection. Exception message: " + e.getMessage(), e);
			throw new MarsException(e);
		} 
		catch (SAXException e) {
			LOGGER.error("Exception caught while trying to get the connection. Exception message: " + e.getMessage(), e);
			throw new MarsException(e);
		} 
		catch (ParserConfigurationException e) {
			LOGGER.error("Exception caught while trying to get the connection. Exception message: " + e.getMessage(), e);
			throw new MarsException(e);
		} catch (Exception e) {
			LOGGER.error("Exception caught while trying to get the connection. Exception message: " + e.getMessage(), e);
			throw new MarsException(e);
		}

  }
  
  /**
   * An utility method that sends JMS message to the appropriate queue
   * 
   * @param context
   * @param messageToken
   * @param queue
   * @throws MarsException
   */
  private static void sendJMSMessage(InitialContext context, MessageToken messageToken, String queue) throws MarsException {
    messageToken.setJMSCorrelationID((String) messageToken.getMessage());

    Dispatcher dispatcher;
		try {
			if (context == null) {
				context = new InitialContext();
			}
			dispatcher = Dispatcher.getInstance();
			messageToken.setStatus(queue);
	    dispatcher.dispatchTextMessage(context, messageToken);
		} 
		catch (Exception e) {
			LOGGER.error("Exception caught while trying to send the message to queue: " + queue);
			throw new MarsException(e);
		}
  }
  
  /*
   * see the above method sendJMSMessage(InitialContext context, MessageToken messageToken, String queue)
   */
  public static void sendJMSMessage(InitialContext context, String message, String queue, String delayTime)
      throws MarsException {

	  MessageToken token = new MessageToken();
	  token.setMessage(message);
	  if (delayTime != null) {
	  	token.setProperty(MarsConstant.JMS_DELAY_PROPERTY, delayTime, "int");	
	  }
	  
	  sendJMSMessage(context, token, queue);
  }

  /**
   * A method to prepare the mercury rules tracker object for exception scenarios
   * 
   * @param mercuryId
   * @param exceptionMessage
   * @param status
   * @return MercuryRulesTracker
   */
  public static MercuryRulesTracker prepareMercuryRulesTracker(MarsVO mars, String mercuryId, String exceptionMessage, String status) {
  	MercuryRulesTracker rulesTracker = new MercuryRulesTracker();
  	rulesTracker.setMercuryId(mercuryId);
  	if (exceptionMessage != null && !exceptionMessage.isEmpty()) {
  		rulesTracker.setExceptionMessage(exceptionMessage.length() >= 200 ? exceptionMessage.substring(0, 200) : exceptionMessage);	
  	}
  	rulesTracker.setActionStatus(status);
  	if (mars != null) {
  		rulesTracker = new MercuryRulesTracker();
			rulesTracker.setAskpThreshold(mars.getAskpThreshold());
			rulesTracker.setAutoRespBeforeCutoff(mars.getAutoResponseCutoffStop());
			rulesTracker.setActionStatus(RulesActionConstants.ACTION_FAILED);
			rulesTracker.setCustomerAddress(mars.getCustomerAddress());
			rulesTracker.setCustomerCity(mars.getCustomerCity());
			rulesTracker.setCustomerCountry(mars.getCustomerCountry());
			rulesTracker.setCustomerFirstName(mars.getCustomerFirstName());
			rulesTracker.setCustomerLastName(mars.getCustomerLastName());
			rulesTracker.setCustomerPhoneNumber(mars.getCustomerPhoneNumber());
			rulesTracker.setCustomerState(mars.getCustomerState());
			rulesTracker.setCustomerZipCode(mars.getCustomerZipCode());
			rulesTracker.setCreatedBy(RulesActionConstants.SYS);
			rulesTracker.setDeliveryCity(mars.getDeliveryCity());
			rulesTracker.setDeliveryState(mars.getDeliveryState());
			rulesTracker.setDeliveryDate(mars.getDeliveryDate());
			rulesTracker.setDeliveryZipCode(mars.getDeliveryZipCode());
			rulesTracker.setFloristBlockDays(mars.getFloristSoftBlockDays());
			rulesTracker.setGlobalRejectRetryLimit(mars.getGlobalRejectRetryLimit());
			rulesTracker.setMarsOrderBounceCount(mars.getMarsOrderBounceCount());
			rulesTracker.setMercuryId(mars.getMercuryId());
			rulesTracker.setMercuryNumber(mars.getMercuryNumber());
			rulesTracker.setMessageDirection(mars.getDirection());
			rulesTracker.setMessageText(mars.getMessageText());
			rulesTracker.setMessageType(mars.getMessageType());
			rulesTracker.setNewMercuryPrice(mars.getNewMercuryPrice());
			rulesTracker.setOccasion(mars.getOccasion());
			rulesTracker.setOrderBounceCount(mars.getOrderBounceCount());
			rulesTracker.setOrderDate(mars.getOrderDate());
			rulesTracker.setOrderDetailId(mars.getOrderDetailId());
			rulesTracker.setOrderOrigin(mars.getOrderOrigin());
			rulesTracker.setOriginalMercuryPrice(mars.getOriginalMercuryPrice());
			rulesTracker.setPhoenixEligibleFlag(mars.getPhoenixEligible());
			rulesTracker.setPreferredPartnerId(mars.getPreferredPartnerId());
			rulesTracker.setProductId(mars.getProductId());
			rulesTracker.setRecipientAddress(mars.getRecipientAddress());
			rulesTracker.setRecipientCity(mars.getRecipientCity());
			rulesTracker.setRecipientCountry(mars.getRecipientCountry());
			rulesTracker.setRecipientFirstName(mars.getRecipientFirstName());
			rulesTracker.setRecipientLastName(mars.getRecipientLastName());
			rulesTracker.setRecipientPhoneNumber(mars.getRecipientPhoneNumber());
			rulesTracker.setRecipientState(mars.getRecipientState());
			rulesTracker.setRecipientZipCode(mars.getRecipientZipCode());
			rulesTracker.setRuleType(RulesActionConstants.MERCURY_INBOUND);
			rulesTracker.setSakText(mars.getSakText());
			rulesTracker.setSourceCode(mars.getSourceCode());
			rulesTracker.setTimeZone(mars.getTimeZone());
			rulesTracker.setUpdatedBy(RulesActionConstants.SYS);
			rulesTracker.setCutOff(mars.getCutOffTime());
  	}
  	return rulesTracker;
  }
  

  /**
   * Gets the groupId from the global params
   * 
   * @return GroupId
   * @throws MarsException
   */
	public static String getGroupId() throws MarsException {
    return getGlobalParam(MarsConstant.MARS_CONTEXT, MarsConstant.GROUP_ID);
	}
	
  /**
   * Gets the ArtifactId from the global params
   * 
   * @return ArtifactId
   * @throws MarsException
   */
	public static String getArtifactId() throws MarsException {
    return getGlobalParam(MarsConstant.MARS_CONTEXT, MarsConstant.ARTIFACT_ID);
	}
	
  /**
   * Gets the Version from the global params
   * 
   * @return Version
   * @throws MarsException
   */
	public static String getVersion() throws MarsException {
    return getGlobalParam(MarsConstant.MARS_CONTEXT, MarsConstant.VERSION);
	}
	
  /**
   * Gets the PhoenixEligible flag based on the global param value and 
   * phoenix eligibility criteria 
   * 
   * @return PhoenixEligible
   * @throws MarsException
   */
	public static String getPhoenixEligible(String phoenixEligible) throws MarsException {
    String phoenixEnabled = getPhoenixEligible();
    
    if (phoenixEnabled != null && MarsConstant.Y_STRING.equalsIgnoreCase(phoenixEnabled)) {
    	return phoenixEligible;
    }
    
		return null;
	}
	
  /**
   * Gets the PhoenixEligible from the global params
   * 
   * @return PhoenixEligible
   * @throws MarsException
   */
	private static String getPhoenixEligible() throws MarsException {
    return getGlobalParam(MarsConstant.PHOENIX_CONTEXT, MarsConstant.PHOENIX_ENABLED);
	}
	
	  /**
	   * Gets the CitySearchOrder flag based on city search criteria 
	   * 
	   * @return citySearchOrder
	   * @throws MarsException
	   */
	public static String getCitySearchOrder(boolean citySearchOrder) throws MarsException {
	    
	    if (citySearchOrder) {
	    	return "Y";
	    }
	    
			return "N";
		}

  /**
   * Gets the ScanInterval from the global params
   * 
   * @return ScanInterval
   * @throws MarsException
   */
	public static long getScanInterval() throws MarsException {
		return Long.valueOf(getGlobalParam(MarsConstant.MARS_CONTEXT, MarsConstant.SCAN_INTERVAL));
	}
	
  /**
   * Gets the MondayCutoff from the global params
   * 
   * @return MondayCutoff
   * @throws MarsException
   */
	public static long getGlobalMondayCutoff() throws MarsException {
		return Long.valueOf(getGlobalParam(MarsConstant.FTDAPPS_PARMS_CONTEXT, MarsConstant.GLOBAL_MONDAY_CUTOFF));
	}
	
  /**
   * Gets the TuesdayCutoff from the global params
   * 
   * @return TuesdayCutoff
   * @throws MarsException
   */
	public static long getGlobalTuesdayCutoff() throws MarsException {
		return Long.valueOf(getGlobalParam(MarsConstant.FTDAPPS_PARMS_CONTEXT, MarsConstant.GLOBAL_TUESDAY_CUTOFF));
	}
	
  /**
   * Gets the WednesdayCutoff from the global params
   * 
   * @return WednesdayCutoff
   * @throws MarsException
   */
	public static long getGlobalWednesdayCutoff() throws MarsException {
    return Long.valueOf(getGlobalParam(MarsConstant.FTDAPPS_PARMS_CONTEXT, MarsConstant.GLOBAL_WEDNESDAY_CUTOFF));
	}
	
  /**
   * Gets the ThursdayCutoff from the global params
   * 
   * @return ThursdayCutoff
   * @throws MarsException
   */
	public static long getGlobalThursdayCutoff() throws MarsException {
    return Long.valueOf(getGlobalParam(MarsConstant.FTDAPPS_PARMS_CONTEXT, MarsConstant.GLOBAL_THURSDAY_CUTOFF));
	}
	
  /**
   * Gets the FridayCutoff from the global params
   * 
   * @return FridayCutoff
   * @throws MarsException
   */
	public static long getGlobalFridayCutoff() throws MarsException {
	  return Long.valueOf(getGlobalParam(MarsConstant.FTDAPPS_PARMS_CONTEXT, MarsConstant.GLOBAL_FRIDAY_CUTOFF));
	}
	
  /**
   * Gets the SaturdayCutoff from the global params
   * 
   * @return SaturdayCutoff
   * @throws MarsException
   */
	public static long getGlobalSaturdayCutoff() throws MarsException {
		return Long.valueOf(getGlobalParam(MarsConstant.FTDAPPS_PARMS_CONTEXT, MarsConstant.GLOBAL_SATURDAY_CUTOFF));
	}
	
  /**
   * Gets the SundayCutoff from the global params
   * 
   * @return SundayCutoff
   * @throws MarsException
   */
	public static long getGlobalSundayCutoff() throws MarsException {
	  return Long.valueOf(getGlobalParam(MarsConstant.FTDAPPS_PARMS_CONTEXT, MarsConstant.GLOBAL_SUNDAY_CUTOFF));
	}
	
  /**
   * Gets the AutoResponseCutoffStop from the global params
   * 
   * @return AutoResponseCutoffStop
   * @throws MarsException
   */
	public static long getAutoResponseCutoffStop() throws MarsException {
    return Long.valueOf(getGlobalParam(MarsConstant.ORDER_PROCESSING_CONTEXT, MarsConstant.AUTO_RESP_BEFORE_CUTOFF));
	}
	
  /**
   * Gets the RejectRetryLimit from the global params
   * 
   * @return RejectRetryLimit
   * @throws MarsException
   */
	public static long getGlobalRejectRetryLimit() throws MarsException {
    return Long.valueOf(getGlobalParam(MarsConstant.ORDER_PROCESSING_CONTEXT, MarsConstant.GLOBAL_REJECT_RETRIES));
	}
	
  /**
   * Gets the GlobalMarsOrderCount from the global params
   * 
   * @return GlobalMarsOrderCount
   * @throws MarsException
   */
	public static long getGlobalMarsOrderCount() throws MarsException {
		return Long.valueOf(getGlobalParam(MarsConstant.MARS_CONTEXT, MarsConstant.GLOBAL_MARS_ORDER_BOUNCE_COUNT));
	}

  /**
   * Gets the ReportWebServiceUrl from the global params
   * 
   * @return ReportWebServiceUrl
   * @throws MarsException
   */
	public static String getReportWebServiceUrl() throws MarsException {
		return getGlobalParam(MarsConstant.MARS_CONTEXT, MarsConstant.WEB_SERVICE_URL);
	}
	
  /**
   * Gets the BreTimeout from the global params
   * 
   * @return BreTimeout
   * @throws MarsException
   */
	public static long getBreTimeout() throws MarsException {
		return Long.valueOf(getGlobalParam(MarsConstant.MARS_CONTEXT, MarsConstant.BRE_TIMEOUT));
	}
	
  /**
   * Gets the value of the global param based on the name and context
   * 
   * @return Global param value
   * @throws MarsException
   */
	private static String getGlobalParam(String context, String name) throws MarsException {
		String paramValue = null;

    ConfigurationUtil configUtil;
    try {
      configUtil = ConfigurationUtil.getInstance();
      paramValue = configUtil.getFrpGlobalParm(context,
      		name);
    }
    catch (Exception e) {
    	LOGGER.error("Exception caught when getting the value for global param:  " + name, e);
    	throw new MarsException(e);
    }
    
		return paramValue;
	}
	
	
	public static String[] tokenizeString(String payload) {
		String delimiter = ";";
		String[] arr = null;
		if (payload != null) {
			arr = payload.split(delimiter);
		}
		return arr;
	}
	
	/**
	 * Prepares the report request object
	 * 
	 * @param reportName
	 * @param timeInterval
	 * @return ReportRequest
	 */
	public static ReportRequest getReportRequest(String reportName, String timeInterval) {
		ReportRequest reportRequest = new ReportRequest();
		
		reportRequest.setFormat(MarsConstant.XLS);
		reportRequest.setName(reportName);
		reportRequest.setRequestedSystem(MarsConstant.MARS);
		reportRequest.setParams(timeInterval);
		
		return reportRequest;
	}
	
	/**
	 * Gets the cutoff time in hhmm format
	 * @param mars
	 * @return
	 */
	private static long getCutoff(MarsVO mars) {
		long cutoffTime = 1400;
		Calendar date = Calendar.getInstance();
		switch (date.get(Calendar.DAY_OF_WEEK)) {
		case Calendar.MONDAY:
			cutoffTime = mars.getGlobalMondayCutoff();
			break;

		case Calendar.TUESDAY:
			cutoffTime = mars.getGlobalTuesdayCutoff();
			break;

		case Calendar.WEDNESDAY:
			cutoffTime = mars.getGlobalWednesdayCutoff();
			break;

		case Calendar.THURSDAY:
			cutoffTime = mars.getGlobalThursdayCutoff();
			break;

		case Calendar.FRIDAY:
			cutoffTime = mars.getGlobalFridayCutoff();
			break;

		case Calendar.SATURDAY:
			cutoffTime = mars.getGlobalSaturdayCutoff();
			break;

		case Calendar.SUNDAY:
			cutoffTime = mars.getGlobalSundayCutoff();
			break;
		}
		return cutoffTime;
	}
	
	private static String getViewQueueFlag() {
		try {
			return getGlobalParam(MarsConstant.LMD_CONTEXT, MarsConstant.VIEW_QUEUE_SWITCH);
		}
		catch (MarsException e) {
			return MarsConstant.N_STRING;
		}
	}
	/**
	 * Gets the Message Types to Skip MARS processing from the global params
	 * 
	 * @return Message Types String
	 * @throws MarsException
	 */
	public static String getMessageTypesToSkip() throws MarsException {
		return getGlobalParam(MarsConstant.MARS_CONTEXT, MarsConstant.SKIP_MESSAGES_BY_MARS);
	}
}
