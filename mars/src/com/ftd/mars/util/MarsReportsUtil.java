package com.ftd.mars.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class MarsReportsUtil {
	
	public static String REPORT_TIME_FORMAT = "MM/dd/yyyy:HH:mm";
	public static String REPORT_TIME_CUMULATIVE = "C";
	public static String REPORT_TIME_HOUR = "H";
	public static String REPORT_TIME_DAY = "D";
	public static String REPORT_TIME_MONTH = "M";
	
		
	public static String getDateString(String duration) {
		Calendar startDate = Calendar.getInstance();
		Calendar endDate = Calendar.getInstance();
		StringBuffer dateString = new StringBuffer("P_START_TIME=");
		dateString.append(getStartDate(startDate, duration));
		dateString.append(" P_END_TIME=");
		dateString.append(getEndDate(endDate, duration));
		
		return dateString.toString();
	}
	
	public static String getDateString(String duration, String reportGroup) {
		Calendar startDate = Calendar.getInstance();
		Calendar endDate = Calendar.getInstance();
		StringBuffer dateString = new StringBuffer("P_START_TIME=");
		dateString.append(getStartDate(startDate, duration));
		dateString.append(" P_END_TIME=");
		dateString.append(getEndDate(endDate, duration));
		dateString.append(" P_GROUP=");
		dateString.append(reportGroup);
		
		return dateString.toString();
	}
	
	private static String getStartDate(Calendar calendar, String duration) {
		
		if (calendar != null && duration != null && !duration.isEmpty()) {
			if (duration.endsWith(REPORT_TIME_HOUR)) {
				calendar.add(Calendar.HOUR_OF_DAY, -Integer.valueOf(duration.replace(REPORT_TIME_HOUR, "")));
				calendar.set(Calendar.MINUTE, 0);
			}
			else if (duration.endsWith(REPORT_TIME_CUMULATIVE)) {
				calendar.set(Calendar.HOUR_OF_DAY, 0);
				calendar.set(Calendar.MINUTE, 0);
			}
			else if (duration.endsWith(REPORT_TIME_DAY)) {
				calendar.add(Calendar.DAY_OF_MONTH, -Integer.valueOf(duration.replace(REPORT_TIME_DAY, "")));
				calendar.set(Calendar.HOUR_OF_DAY, 0);
				calendar.set(Calendar.MINUTE, 0);
			}
			else if (duration.endsWith(REPORT_TIME_MONTH)) {
				calendar.add(Calendar.MONTH, -Integer.valueOf(duration.replace(REPORT_TIME_MONTH, "")));
				calendar.set(Calendar.DAY_OF_MONTH, 1);
				calendar.set(Calendar.HOUR_OF_DAY, 0);
				calendar.set(Calendar.MINUTE, 0);
			}	
		}
		DateFormat df = new SimpleDateFormat(REPORT_TIME_FORMAT);
		
		return df.format(calendar.getTime());
	}
	
	private static String getEndDate(Calendar calendar, String duration) {
		
		if (calendar != null && duration != null && !duration.isEmpty()) {
			if (duration.endsWith(REPORT_TIME_HOUR)) {
				calendar.set(Calendar.MINUTE, 0);
			}
			else if (duration.endsWith(REPORT_TIME_CUMULATIVE)) {
				calendar.set(Calendar.MINUTE, 0);
			}
			else if (duration.endsWith(REPORT_TIME_DAY)) {
				calendar.set(Calendar.HOUR_OF_DAY, 0);
				calendar.set(Calendar.MINUTE, 0);
			}
			else if (duration.endsWith(REPORT_TIME_MONTH)) {
				calendar.set(Calendar.DAY_OF_MONTH, 0);
				calendar.set(Calendar.HOUR_OF_DAY, 0);
				calendar.set(Calendar.MINUTE, 0);
			}	
		}
		DateFormat df = new SimpleDateFormat(REPORT_TIME_FORMAT);
		
		return df.format(calendar.getTime());
	}
	
	public static void main(String[] args) {
		int count = 0;
		int displayCount = 1;
		int i = 0;
		while (i < 100) {
  		if (++count > displayCount && displayCount != 0) {
    		break;
    	}
  		System.out.println(i);
  		i++;
		}
	}

}
