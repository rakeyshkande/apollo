package com.ftd.mars.processor;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MercuryRulesAppContext {
	
	private static final  ApplicationContext ctx = new ClassPathXmlApplicationContext("mars-context.xml");
	
	private MercuryRulesAppContext() {
		
	}
	
	public static MercuryRulesProcessor getMercuryRulesProcessor() {
		return (MercuryRulesProcessor) ctx.getBean("mercuryRulesProcessor");
	}
	
	public static Object getMercuryRulesProcessor1() {
		return ctx;
	}

}
