package com.ftd.mars.processor;

import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.naming.InitialContext;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ftd.core.domain.MercuryRulesTracker;
import com.ftd.core.domain.OrderDetails;
import com.ftd.core.domain.request.MarsVO;
import com.ftd.core.domain.response.RulesResponse;
import com.ftd.mars.cache.RuleEngineCache;
import com.ftd.mars.constant.MarsConstant;
import com.ftd.mars.exception.MarsException;
import com.ftd.mars.exception.MercuryMessageTypeException;
import com.ftd.mars.util.MarsUtil;
import com.ftd.mercuryservice.exception.MercuryServiceException;
import com.ftd.mercuryservice.service.MercuryService;
import com.ftd.orderservice.exception.OrderServiceException;
import com.ftd.orderservice.service.OrderService;
import com.ftd.rulesaction.constant.RulesActionConstants;
import com.ftd.rulesaction.exception.RulesActionException;
import com.ftd.rulesexecutor.RuleEngineException;
import com.ftd.rulesexecutor.core.domain.RuleExecutionContext;
import com.ftd.rulesexecutor.engine.RuleEngine;
import com.ftd.rulesexecutor.engine.RuleEngineFactory;
import com.ftd.rulesexecutor.engine.RuleExecutor;

/**
 * Core class that processes the Inbound mercury message requests.
 * Gets all the dependent objects in a map by invoking the 
 * Pre-processor class.
 * Requests Rule Engine to execute the rules for the Inbound mercury request.
 * Handles the action codes returned by the Rule engine by invoking the 
 * Post process class.  
 * 
 * @author kdatchan
 *
 */
@Component
public class MercuryRulesProcessor {

	@Autowired
	@Qualifier(value="mercuryService")
	private MercuryService mercuryService;

	@Autowired
	@Qualifier(value="orderService")
	private OrderService orderService;
	
	@Autowired
	private MercuryRulesPreProcessor mercuryRulesPreProcessor;
	
	@Autowired
	private MercuryRulesPostProcessor mercuryRulesPostProcessor;
	
	private static final Logger LOGGER = Logger.getLogger(MercuryRulesProcessor.class);

	/**
	 * 1. Gets all the dependent objects, by invoking pre-processor class, in a map.
	 * <br/>
	 * 2. Calls Rule engine to execute the rules for the input request.
	 * <br/>
	 * 3. Processes the action codes returned by the Rule engine
	 * <br/>
	 * 4. Handles all the exceptions and sends System messages. Failed requests will be 
	 * re-processed by the existing system
	 * <br/>
	 * 5. Throws unhandled exception back to the MDB
	 * <br/>
	 * 6. Exception messages are inserted into Mercury rules tracker table in the finally block 
	 * 
	 * @param mercuryId
	 * @throws MarsException
	 */
	public void process(String mercuryId) throws Exception {

		StringBuffer sb = null;
		RulesResponse response = new RulesResponse();
		MercuryRulesTracker mercuryRulesTracker = null;
		boolean persistRulesTracker = false;
		Map<String, Object> dependentObjects = null;
		try {

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(new StringBuffer("Pre-processing the request for mercury id : ").append(mercuryId));
			}
			// preprocess
			dependentObjects = mercuryRulesPreProcessor.preProcess(mercuryId);

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(new StringBuffer("Sending the request to Rule Engine for mercury id : ").append(mercuryId));
			}
			// execute rules
			RuleExecutionContext executionContext = executeRules(dependentObjects, response);
			
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(new StringBuffer("Post-processing the request to Rule Engine for mercury id : ").append(mercuryId));
			}
			//post process
			mercuryRulesPostProcessor.postProcess(executionContext.getRuleFact(), dependentObjects);
			
		} 
		catch (MercuryMessageTypeException e) {
			sb = new StringBuffer(" We can ignore this error ,");
			sb.append("This message is not processed by Mars as per the global param(SKIP_MASSAGES_BY_MARS) configuration. Putting the message to MERCURY_INBOUND queue and ");
			sb.append(" request will be processed by the existing system(Order Processing) for this mercury Id "+mercuryId);
			LOGGER.error(sb.toString());
			this.sendJMSMessage(null, mercuryId, MarsConstant.MERCURY_INBOUND_QUEUE, null);
		} 
		catch (MercuryServiceException e) {
			sb = new StringBuffer("Exception caught while trying to execute the rules. Putting the message to MERCURY_INBOUND queue. Exception message: ");
			sb.append(e.getMessage());
			LOGGER.error(sb.toString(), e);
			sb = new StringBuffer("Unable to process the request with mercury id: ");
			sb.append(mercuryId);
			sb.append(" due to exception: ");
			sb.append(e.getMessage());
			sb.append(". Request will be processed by the existing system(Order Processing)");
			this.sendSystemMessageWrapper(sb.toString(), MarsConstant.NOPAGE_SOURCE, MarsConstant.ERROR);
			this.sendJMSMessage(null, mercuryId, MarsConstant.MERCURY_INBOUND_QUEUE, null);
			persistRulesTracker = true;
		} 
		catch (NumberFormatException e) {
			sb = new StringBuffer("Exception caught while trying to execute the rules. Putting the message to MERCURY_INBOUND queue. Exception message: ");
			sb.append(e.getMessage());
			LOGGER.error(sb.toString(), e);
			sb = new StringBuffer("Unable to process the request with mercury id: ");
			sb.append(mercuryId);
			sb.append(" due to exception: ");
			sb.append(e.getMessage());
			sb.append(". Request will be processed by the existing system(Order Processing)");
			this.sendSystemMessageWrapper(sb.toString(), MarsConstant.NOPAGE_SOURCE, MarsConstant.ERROR);
			this.sendJMSMessage(null, mercuryId, MarsConstant.MERCURY_INBOUND_QUEUE, null);
			persistRulesTracker = true;
		} 
		catch (OrderServiceException e) {
			sb = new StringBuffer("Exception caught while trying to execute the rules. Putting the message to MERCURY_INBOUND queue. Exception message: ");
			sb.append(e.getMessage());
			LOGGER.error(sb.toString(), e);
			sb = new StringBuffer("Unable to process the request with mercury id: ");
			sb.append(mercuryId);
			sb.append(" due to exception: ");
			sb.append(e.getMessage());
			sb.append(". Request will be processed by the existing system(Order Processing)");
			this.sendSystemMessageWrapper(sb.toString(), MarsConstant.NOPAGE_SOURCE, MarsConstant.ERROR);
			this.sendJMSMessage(null, mercuryId, MarsConstant.MERCURY_INBOUND_QUEUE, null);
			persistRulesTracker = true;
		}
		catch (RuleEngineException e) {
			sb = new StringBuffer("Exception caught while trying to execute the rules. Putting the message to MERCURY_INBOUND queue. Exception message: ");
			sb.append(e.getMessage());
			LOGGER.error(sb.toString(), e);
			sb = new StringBuffer("Rule engine is unable to process the request with mercury id: ");
			sb.append(mercuryId);
			sb.append(" due to exception: ");
			sb.append(e.getMessage());
			sb.append(". Request will be processed by the existing system(Order Processing)");
			this.sendSystemMessageWrapper(sb.toString(), MarsConstant.PAGE_SOURCE, MarsConstant.ERROR);
			this.sendJMSMessage(null, mercuryId, MarsConstant.MERCURY_INBOUND_QUEUE, null);
			persistRulesTracker = true;
		}
		catch (RulesActionException e) {
			sb = new StringBuffer("Exception caught while trying to execute the rules. Putting the message to MARS_MERCURY queue. Exception message: ");
			sb.append(e.getMessage());
			LOGGER.error(sb.toString(), e);
			sb = new StringBuffer("Error occurred when trying to execute the actions returned by Rule engine for mercury id: ");
			sb.append(mercuryId);
			sb.append(" due to exception: ");
			sb.append(e.getMessage());
			sb.append(". Request will be re-tried");
			this.sendSystemMessageWrapper(sb.toString(), MarsConstant.PAGE_SOURCE, MarsConstant.ERROR);
			this.sendJMSMessage(null, mercuryId, MarsConstant.MARS_MERCURY_QUEUE, "300");
			persistRulesTracker = true;
		}
		catch (MarsException e) {
			sb = new StringBuffer("Exception caught while trying to execute the rules. Putting the message to MERCURY_INBOUND queue. Exception message: ");
			sb.append(e.getMessage());
			LOGGER.error(sb.toString(), e);
			sb = new StringBuffer("Unable to process the request with mercury id: ");
			sb.append(mercuryId);
			sb.append(" due to exception: ");
			sb.append(e.getMessage());
			sb.append(". Request will be processed by the existing system(Order Processing)");
			this.sendSystemMessageWrapper(sb.toString(), MarsConstant.NOPAGE_SOURCE, MarsConstant.ERROR);
			this.sendJMSMessage(null, mercuryId, MarsConstant.MERCURY_INBOUND_QUEUE, null);
			persistRulesTracker = true;
		}
		catch (TimeoutException e) {
			sb = new StringBuffer("Exception caught while trying to execute the rules. Putting the message to MARS_QUEUE queue. Exception message: ");
			sb.append(e.getMessage());
			LOGGER.error(sb.toString(), e);
			sb = new StringBuffer("Rule engine failed to process the request within the configured time limit for mercury id: ");
			sb.append(mercuryId);
			sb.append(". Request will be re-tried");
			this.sendSystemMessageWrapper(sb.toString(), MarsConstant.NOPAGE_SOURCE, MarsConstant.ERROR);
			this.sendJMSMessage(null, mercuryId, MarsConstant.MARS_MERCURY_QUEUE, "300");
			persistRulesTracker = true;
		}
		catch (Exception e) {
			sb = new StringBuffer("Exception caught while trying to execute the rules. Putting the message to MERCURY_INBOUND queue. Exception message: ");
			sb.append(e.getMessage());
			LOGGER.error(sb.toString(), e);
			sb = new StringBuffer("Unable to process the request with mercury id: ");
			sb.append(mercuryId);
			sb.append(" due to exception: ");
			sb.append(e.getMessage());
			sb.append(". Request will be processed by the existing system(Order Processing)");
			this.sendSystemMessageWrapper(sb.toString(), MarsConstant.NOPAGE_SOURCE, MarsConstant.ERROR);
			this.sendJMSMessage(null, mercuryId, MarsConstant.MERCURY_INBOUND_QUEUE, null);
			persistRulesTracker = true;
		}
		finally {
			if (persistRulesTracker) {
				if (dependentObjects != null) {
  				mercuryRulesTracker = (MercuryRulesTracker) dependentObjects.get(RulesActionConstants.MERCURY_RULES_TRACKER_OBJ);
  				OrderDetails orderDetails  = (OrderDetails) dependentObjects.get(RulesActionConstants.ORDER_DETAILS_OBJ);
  				MarsVO mars  = (MarsVO) dependentObjects.get(RulesActionConstants.MARS_OBJ);
  				if (mercuryRulesTracker != null) {
  					mercuryRulesTracker.setMarsOrderBounceCount(mercuryRulesTracker.getMarsOrderBounceCount() + 1);
  					persistRulesTracker(mercuryRulesTracker);
  				}
  				else {
  					mercuryRulesTracker = MarsUtil.prepareMercuryRulesTracker(mars, mercuryId, (sb != null ? sb.toString() : null), RulesActionConstants.ACTION_FAILED);
  					mercuryRulesTracker.setMarsOrderBounceCount(1);
  					if (orderDetails != null) {
  						mercuryRulesTracker.setOrderDetailId(orderDetails.getOrderDetailId());
  						mercuryRulesTracker.setExternalOrderNumber(orderDetails.getExternalOrderNumber());
  						mercuryRulesTracker.setFloristId(orderDetails.getFloristId());
  					}
  					persistRulesTracker(mercuryRulesTracker);
  				}
				}
			}
		}

	}

	/**
	 * 1. Gets the Rule engine object from the cache
	 * <br/>
	 * 2. Calls Rule engine to execute the rules.
	 * <br/>
	 * 3. Terminates the request if Rule engine has taken more time
	 * 
	 * @param map
	 * @param response
	 * @return RuleExecutionContext
	 * @throws MarsException
	 * @throws RuleEngineException
	 * @throws TimeoutException
	 */
	private RuleExecutionContext executeRules(Map<String, Object> map, RulesResponse response) throws MarsException, RuleEngineException, TimeoutException {
		
		final RuleExecutionContext executionContext = new RuleExecutionContext(map.get(RulesActionConstants.MARS_OBJ), response);
		
		try {
			RuleEngine ruleEngine = getRuleEngine();
			final RuleExecutor ruleExecutor = new RuleExecutor(ruleEngine);
			ExecutorService executorService = Executors.newSingleThreadExecutor();
			long time = System.currentTimeMillis();
		  
			LOGGER.info("Calling Rule Engine to apply rules");
	    Future<Boolean> future = executorService.submit(
	          new Callable<Boolean>(){
	              public Boolean call() throws RuleEngineException {
	              	return ruleExecutor.process(executionContext);
	              }
	             });
	    future.get(MarsUtil.getBreTimeout(),TimeUnit.MILLISECONDS);
	    LOGGER.info(new StringBuffer("Total time taken by rule engine to prcoess the request: ").append((System.currentTimeMillis() - time)).toString());
		} 
		catch (TimeoutException e) {
			throw new TimeoutException("Rule engine failed to process the request within the configured time limit");
		}
		catch (MarsException e) {
			throw new RuleEngineException(e);
		}
		catch (Exception e) {
			throw new RuleEngineException(e);
		}
		
		return executionContext;
	}
	
	/**
	 * 1. Checks if the Rule engine object is present in cache.
	 * <br/>
	 * 2. Returns the object if its present in the cache.
	 * <br/>
	 * 3. Else creates a new Rule engine object, puts it in cache
	 * and returns that object.
	 * 
	 * @return RuleEngine
	 * @throws MarsException
	 */
	private RuleEngine getRuleEngine() throws MarsException, Exception {
		
		RuleEngine ruleEngine = RuleEngineCache.getInstance().getRuleEngine(MarsUtil.getArtifactId() + MarsUtil.getVersion());
		
		if (ruleEngine != null) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Getting the Rule Engine object from cache");
			}
			return ruleEngine; 
		}
		
		else {
			LOGGER.info(new StringBuffer("Rule engine object is not present in the cache. So the object will be created and cached for id : ").append(MarsUtil.getArtifactId() + MarsUtil.getVersion()));
			ruleEngine = RuleEngineFactory.getRuleEngine(
					MarsUtil.getGroupId(),
					MarsUtil.getArtifactId(),
					MarsUtil.getVersion(),
					MarsUtil.getScanInterval());
			
			if (ruleEngine == null) {
				throw new MarsException("Rule engine object is null which is unexpected. Throwing an exception");
			}
			RuleEngineCache.getInstance().setRuleEngine(MarsUtil.getArtifactId() + MarsUtil.getVersion(), ruleEngine);
		}
		
		return ruleEngine;
	}
	
	/**
	 * Persists the Mercury inbound request into the traking table.
	 * 
	 * @param mercuryRulesTracker
	 */
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	private void persistRulesTracker(MercuryRulesTracker mercuryRulesTracker) {
		String mercuryId = null;
		try {
			if (mercuryRulesTracker != null) {
				mercuryId = mercuryRulesTracker.getMercuryId();
				mercuryService.saveMercuryRulesTracker(mercuryRulesTracker);
			}
		}
		catch (Exception e) {
			try {
				this.sendSystemMessageWrapper("Exception caught while trying to persit tracking information in DB for mercury id: " + mercuryId, MarsConstant.NOPAGE_SOURCE, MarsConstant.ERROR);
			} 
			catch (MarsException e1) {
				LOGGER.error("Exception caught while trying to send system message", e);
			}
			LOGGER.error("Exception caught while trying to persit tracking information in DB", e);
		}
	}
	
	/**
	 * Creates a new spring transaction so that sending system message won't
	 * be affected due to the outcome of the existing transaction.  
	 * 
	 * @param context
	 * @param mercuryId
	 * @param queue
	 * @param delayTime
	 * @throws MarsException
	 */
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	private void sendJMSMessage(InitialContext context, String mercuryId,
			String queue, String delayTime) throws MarsException {
		MarsUtil.sendJMSMessage(context, mercuryId, queue, delayTime);
	}

	/**
	 * Creates a new spring transaction so that sending system message won't
	 * be affected due to the outcome of the existing transaction.  
	 * 
	 * @param message
	 * @param messageSource
	 * @param type
	 */
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	private void sendSystemMessageWrapper(String message, String messageSource,
			String type) throws MarsException {
		MarsUtil.sendSystemMessageWrapper(message, messageSource, type);
	}
}
