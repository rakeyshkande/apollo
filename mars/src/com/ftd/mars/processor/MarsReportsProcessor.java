package com.ftd.mars.processor;

import org.apache.log4j.Logger;

import com.ftd.mars.constant.MarsConstant;
import com.ftd.mars.exception.MarsReportsException;
import com.ftd.mars.reports.WebServiceClientFactory;
import com.ftd.mars.util.MarsReportsUtil;
import com.ftd.mars.util.MarsUtil;
import com.ftd.report.service.ReportResponse;
import com.ftd.report.service.ReportWebService;

public class MarsReportsProcessor {

	private static final Logger LOGGER = Logger.getLogger(MarsReportsProcessor.class);
			
	public void processReports(String payload) {
		StringBuffer sb = null;
    try {
    	String[] arr = MarsUtil.tokenizeString(payload);
    	    	
    	if (MarsConstant.MARS_ACTION_DETAIL.equalsIgnoreCase(arr[0])) {
    		submitReport(arr[0], MarsReportsUtil.getDateString(arr[1], arr[2]));
    	}
    	else if (MarsConstant.FLORIST_SUMMARY.equalsIgnoreCase(arr[0])) {
    		submitReport(arr[0], MarsReportsUtil.getDateString(arr[1]));
    	}
    	else {
    		throw new MarsReportsException("Invalid report name. Process will exit");
    	}
		}
    catch (Exception e) {
    	sb = new StringBuffer("Failed to submit report due to exception: ");
    	sb.append(e.getMessage());
    	LOGGER.error("Failed to submit report due to exception: ", e);
    	WebServiceClientFactory clientFactory = new WebServiceClientFactory();
    	clientFactory.removeThreadInstance();
    	try {
    		MarsUtil.sendSystemMessageWrapper(sb.toString(), MarsConstant.NOPAGE_SOURCE, MarsConstant.ERROR);	
    	}
    	catch (Exception ex) {
    		LOGGER.error("Unable to send system message", ex);
    	}
    	
		}
	}
	
	private void submitReport(String reportName, String timeInterval) throws Exception {
		WebServiceClientFactory clientFactory = new WebServiceClientFactory();
  	ReportWebService reportService = clientFactory.getReportService();
		ReportResponse reportResponse = reportService.submitReport(MarsUtil.getReportRequest(reportName, timeInterval));
		if (reportResponse == null) {
			throw new MarsReportsException("Report service failed to process the reports.");
		}
		if (!MarsConstant.SUCCESS.equalsIgnoreCase(reportResponse.getStatus())) {
			throw new MarsReportsException(new StringBuffer("Report service failed to process the reports. Exception message: ").append(reportResponse.getErrorDescription()).toString());
		}
	}
	

}
