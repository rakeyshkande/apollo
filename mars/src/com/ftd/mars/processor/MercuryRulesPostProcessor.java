package com.ftd.mars.processor;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ftd.core.domain.Comments;
import com.ftd.core.domain.Mercury;
import com.ftd.core.domain.request.MarsVO;
import com.ftd.core.domain.response.RulesResponse;
import com.ftd.mars.exception.MarsException;
import com.ftd.rulesaction.MercuryRulesActionHandler;
import com.ftd.rulesaction.ViewQueueRulesActionHandler;
import com.ftd.rulesaction.constant.RulesActionConstants;
import com.ftd.rulesaction.exception.RulesActionException;
import com.ftd.rulesexecutor.core.domain.RuleFact;

/**
 * The purpose of this class is to send the updated rule facts/objects
 * to Rules Action handler class to process the action codes returned 
 * by the rule engine
 * <br/>
 * 1. Gets the updated rule facts/objects from the Rule fact object.
 * <br/>
 * 2. Constructs the new objects, which will be required by Rules Action
 * module, and put them into the map 
 * <br/>
 * 3. Invokes Rules action handler to process the action codes returned by
 * the rule engine  
 * 
 * 
 * @author kdatchan
 *
 */
@Component
public class MercuryRulesPostProcessor {

	@Autowired
	private MercuryRulesActionHandler rulesActionHandler;
	
	@Autowired
	private ViewQueueRulesActionHandler viewQueueRulesActionHandler;
	
	private static final Logger LOGGER = Logger.getLogger(MercuryRulesPostProcessor.class);
	
	/**
	 * 1. Gets the updated rule facts/objects from the Rule fact object.
	 * <br/>
	 * 2. Constructs the new objects, which will be required by Rules Action
	 * module, and put them into the map.
	 * <br/> 
	 * 3. Invokes Rules action handler 
	 * to process the action codes returned by the rule engine.
	 * <br/> 
	 * 4. Throws appropriate exception to the caller
	 * 
	 * @param ruleFact
	 * @param map
	 * @throws MarsException
	 * @throws RulesActionException
	 */
	public void postProcess(RuleFact ruleFact, Map<String, Object> map) throws MarsException, RulesActionException {
		
		if (ruleFact == null) {
			LOGGER.error("Rule fact can't be null");
			throw new MarsException("Rule fact can't be null");
		}
		
		MarsVO marsVO = null;
		RulesResponse response = null;
		Comments comments = null;
		Map<String, List<String>> ruleFiredMap = null;
		Map<String, Object> actionMetaData = null;
		
		List<Object> objects = ruleFact.getFacts();
		Mercury mercuryMessage = (Mercury) map.get(RulesActionConstants.MERCURY_OBJ);
		
		for(Object obj : objects) {
			if (obj instanceof MarsVO) {
				marsVO = (MarsVO) obj;
			}
			else if (obj instanceof RulesResponse) {
				response = (RulesResponse) obj;
			}
		}
		
		if (response != null) {
			ruleFiredMap = response.getRulesFiredMap();
		}
		
		if (marsVO != null) {
			comments = new Comments();
			comments.setComment(marsVO.getOrderComments());
			if (mercuryMessage != null) {
				mercuryMessage.setViewQueue(marsVO.getViewQueue());
			}
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Rules Output : " + marsVO);
		}
		
		map.put(RulesActionConstants.MERCURY_OBJ, mercuryMessage);
		map.put(RulesActionConstants.COMMENTS_OBJ, comments);
		
		try {
			LOGGER.info("Processing the Mercury Message action codes returned by Rule Engine after applying rules");
			rulesActionHandler.handleActions(map, ruleFiredMap, actionMetaData);
		} 
		catch (RulesActionException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RulesActionException(e);
		}
		
		try {
			LOGGER.info("Processing the View Queue action codes returned by Rule Engine after applying rules");
			viewQueueRulesActionHandler.handleActions(map, ruleFiredMap, actionMetaData);
		} 
		catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
		
	}
}
