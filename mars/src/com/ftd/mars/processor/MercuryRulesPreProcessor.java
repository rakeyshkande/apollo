package com.ftd.mars.processor;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.ftd.core.domain.Customer;
import com.ftd.core.domain.Mercury;
import com.ftd.core.domain.MercuryRulesTracker;
import com.ftd.core.domain.OrderDetails;
import com.ftd.core.domain.Orders;
import com.ftd.core.domain.request.MarsVO;
import com.ftd.floristservice.exception.FloristServiceException;
import com.ftd.floristservice.service.FloristService;
import com.ftd.mars.constant.MarsConstant;
import com.ftd.mars.exception.MarsException;
import com.ftd.mars.exception.MercuryMessageTypeException;
import com.ftd.mars.util.MarsUtil;
import com.ftd.mercuryservice.exception.MercuryServiceException;
import com.ftd.mercuryservice.service.MercuryService;
import com.ftd.orderservice.exception.OrderServiceException;
import com.ftd.orderservice.service.OrderService;
import com.ftd.rulesaction.constant.RulesActionConstants;

/**
 * 
 * Gets all the objects(Mercury, OrderDetails etc) based on the ids
 * and constructs MarVO which is required for executing the rules
 * 
 * @author kdatchan
 *
 */
@Component
public class MercuryRulesPreProcessor {

	@Autowired
	@Qualifier(value="mercuryService")
	private MercuryService mercuryService;

	@Autowired
	@Qualifier(value="orderService")
	private OrderService orderService;
	
	@Autowired
	@Qualifier(value="floristService")
	private FloristService floristService;
	
	private static final Logger LOGGER = Logger.getLogger(MercuryRulesPreProcessor.class);
	
	/**
	 * 1. Gets all the objects(Mercury, OrderDetails, Customer etc) by calling 
	 * the new SOA services(i.e. MercuryService, OrderService).
	 * <br/>
	 * 2. Constructs MarsVO object using Mercury, OrderDetails etc. objects.
	 * <br/>
	 * 3. Puts all the objects into the map so that the objects can be re-used later.
	 * <br/>
	 * 4. Throws appropriate exceptions to the caller. 
	 * 
	 * @param mercuryId
	 * @return map
	 * @throws MarsException
	 * @throws OrderServiceException
	 * @throws MercuryServiceException
	 * @throws MercuryMessageTypeException 
	 */
	public Map<String, Object> preProcess(String mercuryId) throws MarsException, OrderServiceException, MercuryServiceException, MercuryMessageTypeException {
		String timeZone = null;
		boolean orderStatus;
		String phoenixEligible = null;
		boolean citySearchOrder = false;
		Mercury mercuryMessage = null;
		OrderDetails orderDetails = null;
		Mercury associatedMessage = null;
		Orders orders = null;
		Customer recipient = null;
		Customer customer = null;
		Map<String, Object> codifiedProducts = null;
		MercuryRulesTracker mercuryRulesTracker = null;
		Map<String, Object> map = new HashMap<String, Object>();
		String messageTypesToSkip=null;
		// Get the mercury object using the mercury id
		mercuryMessage = mercuryService.getMercuryById(mercuryId);
		
		if (mercuryMessage == null) {
			LOGGER.error("Mercury message detail is required and can't be null. Exiting the process");
			throw new MarsException("Mercury message detail is required and can't be null. Exiting the process");
		}
		//Here we are going to ignoring the MARS Process for some message types, as per the configuration in global param.
		messageTypesToSkip =MarsUtil.getMessageTypesToSkip();
		if(mercuryMessage.getMessageType()!=null && messageTypesToSkip !=null && messageTypesToSkip.trim().indexOf(mercuryMessage.getMessageType().trim()) >= 0){
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(mercuryMessage.getMessageType() +" Message Type is skipped by mars rules processor");
			}
			throw new MercuryMessageTypeException(mercuryMessage.getMessageType()+" - message type is not processed by MARS Rules Processor.");
		}
		
		// Get the associated mercury object(i.e. FTD message) using the mercury order number
		associatedMessage = mercuryService.getMercuryByOrderNumber(mercuryMessage.getMercuryOrderNumber(), mercuryMessage.getReferenceNumber());
		
		if (associatedMessage == null) {
			LOGGER.error("Associated Mercury message is required and can't be null. Exiting the process");
			throw new MarsException("Associated Mercury message is required and can't be null. Exiting the process");
		}
		
		// Get the order details object using the reference number of the associated mercury object. 
		// mercury.mercury.reference_number = clean.order_details.order_detail_id
		orderDetails = orderService.getOrderDetailById(Long.valueOf(associatedMessage.getReferenceNumber()));
		
		if (orderDetails == null) {
			LOGGER.error("Order details object is required and can't be null. Exiting the process");
			throw new MarsException("Order details object is required and can't be null. Exiting the process");
		}
		
		// Order using the order guid
		orders = orderService.getOrderByGuid(orderDetails.getOrderGuid());
		if (orders == null) {
			LOGGER.error("Orders object is required and can't be null. Exiting the process");
			throw new MarsException("Orders object is required and can't be null. Exiting the process");
		}

		recipient = orderService.getCustomerById(orderDetails.getRecipientId());
		if (recipient == null) {
			LOGGER.error("Recipient details is required and can't be null. Exiting the process");
			throw new MarsException("Recipient details is required and can't be null. Exiting the process");
		}

		customer = orderService.getCustomerById(orders.getCustomerId());
		if (customer == null) {
			LOGGER.error("Customer details is required and can't be null. Exiting the process");
			throw new MarsException("Customer details is required and can't be null. Exiting the process");
		}
		
		try {
			codifiedProducts = floristService.getCodifiedProductsNMinPrice(orderDetails.getProductId(), mercuryMessage.getSendingFlorist());
		} 
		catch (FloristServiceException e) {
			LOGGER.error("Unable to get the codified products");
		}
								
		mercuryRulesTracker = mercuryService.getMercuryRulesTracker(mercuryMessage.getMercuryId(), orderDetails.getOrderDetailId());
		
		timeZone = orderService.getTimeZone(recipient.getState());
		orderStatus = mercuryService.isCancelledOrRejected(mercuryMessage);
		phoenixEligible = orderService.getPhoenixEligible(String.valueOf(orderDetails.getOrderDetailId()), mercuryMessage.getMessageType(), mercuryMessage.getMercuryOrderNumber());
		try{
			citySearchOrder = floristService.isCitySearchedOrder(mercuryMessage.getMercuryOrderNumber());
		}catch (FloristServiceException e){
			LOGGER.error("Unable to get order florist used for the message");
		}
		
		
		LOGGER.info("CitySearchOrder : "+citySearchOrder);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Preparing the input request object for the Rule engine");
		}
		// Prepare/construct the MarsVO object using the other objects.
		MarsVO marsVO = MarsUtil.getMarsVO(mercuryMessage, associatedMessage, orderDetails, orders, recipient, customer, timeZone, orderStatus, phoenixEligible, citySearchOrder, (mercuryRulesTracker != null ? mercuryRulesTracker.getMarsOrderBounceCount() : 0));
		LOGGER.info("MARSVO : "+marsVO);
		
		if (codifiedProducts != null) {
			marsVO.setCodifiedId(codifiedProducts.get("CODIFICATION_ID") != null ? codifiedProducts.get("CODIFICATION_ID").toString() : null);
			if (codifiedProducts.get("CODIFIED_MINIMUM") != null) {
				try {
					marsVO.setCodifiedMinimum(new BigDecimal(codifiedProducts.get("CODIFIED_MINIMUM") != null ? codifiedProducts.get("CODIFIED_MINIMUM").toString() : "0"));	
				}
				catch (Exception e) {
					LOGGER.error("Suppressing the exception, so that request processing is not affected");
				}
					
			}
		}
		
		// Send a system message if View Queue Holiday field is empty or null
		if (marsVO != null && (marsVO.getViewQueueHolidayDate() == null && (marsVO.getViewQueueControl().equalsIgnoreCase(MarsConstant.Y_STRING) && marsVO.getViewQueue().equalsIgnoreCase(MarsConstant.Y_STRING)))) {
			try {
				String message = "Unable to get the View Queue Holiday Date from the global control. Param Name: " + MarsConstant.VIEW_QUEUE_HOLIDAY_DATE + ". Please update with a valid value.";
				MarsUtil.sendSystemMessageWrapper(message, MarsConstant.NOPAGE_SOURCE, MarsConstant.ERROR);
			}
			catch (Exception e) {
				LOGGER.error("Unable to send system message. Suppressing the exception");
			}
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Rules Input : " + marsVO);
		}
		
		LOGGER.info("MARSVO : "+marsVO);
		
		map.put(RulesActionConstants.MERCURY_OBJ, mercuryMessage);
		map.put(RulesActionConstants.ORDER_DETAILS_OBJ, orderDetails);
		map.put(RulesActionConstants.ASSOCIATED_MERCURY_OBJ, associatedMessage);
		map.put(RulesActionConstants.MARS_OBJ, marsVO);
		map.put(RulesActionConstants.MERCURY_RULES_TRACKER_OBJ, mercuryRulesTracker);
		
		return map; 
	}
}
