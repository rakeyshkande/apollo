package com.ftd.mars.cache;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.ftd.rulesexecutor.engine.RuleEngine;

/**
 * Singleton class to persist the Rule engine object into cache map
 * 
 * @author kdatchan
 *
 */
public class RuleEngineCache {
		private static RuleEngineCache cache = new RuleEngineCache();
		private Map<String, RuleEngine> ruleEngineCacheMap = Collections.synchronizedMap(new HashMap<String, RuleEngine>());
		
		private RuleEngineCache(){
		}
		
		public static RuleEngineCache getInstance(){
			return cache;
		}
		
		public RuleEngine getRuleEngine(String mavenVersion){
			return ruleEngineCacheMap.get(mavenVersion);
		}
		
		public synchronized void setRuleEngine(String mavenVersion, RuleEngine ruleEngine){
			ruleEngineCacheMap.put(mavenVersion, ruleEngine);
		}

}
