package com.ftd.op.mercury.mdb;

import com.ftd.op.common.framework.util.CommonUtils;
import com.ftd.op.common.to.ResultTO;
import com.ftd.op.mercury.bo.MercuryHandler;
import com.ftd.op.mercury.to.BaseMercuryMessageTO;
import com.ftd.osp.utilities.plugins.Logger;

import java.lang.reflect.Method;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.EJBException;
import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;

import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;


public class AdHocInboundMDBean implements MessageDrivenBean, MessageListener {
    private Logger logger = new Logger(AdHocInboundMDBean.class.getName());
    private MessageDrivenContext context;

    public void ejbCreate() {
    }

    public void onMessage(Message msg) {
        logger.debug("onMessage: message received.");

        try {
            logger.debug("onMessage: JMSMessageID: " + msg.getJMSMessageID());
            logger.debug("onMessage: JMSTimestamp: " + msg.getJMSTimestamp());
            logger.debug("onMessage: JMSExpiration: " + msg.getJMSExpiration());

            //                logger.debug("onMessage: JMSDeliveryMode: " +
            //                    msg.getJMSDeliveryMode());
            //                logger.debug("onMessage: Persistent: " + DeliveryMode.PERSISTENT);
            logger.debug("onMessage: JMSRedelivered: " +
                msg.getJMSRedelivered());

            BaseMercuryMessageTO messageTO = null;

            if (msg instanceof MapMessage) {
                logger.debug("onMessage: MAP message received.");

                MapMessage mapMessage = (MapMessage) msg;

                String messageType = mapMessage.getString("MessageType");

                if ((mapMessage != null) && (messageType != null)) {
                    // Obtain the appropriate value transfer object.
                    messageTO = MercuryHandler.getTOInstance(messageType);

                    // Populate the TO.
                    Enumeration msgKeyEnum = mapMessage.getMapNames();

                    while (msgKeyEnum.hasMoreElements()) {
                        String key = (String) msgKeyEnum.nextElement();

                        Method method = null;

                        boolean isDoubleTypeInput = false;

                        // Attempt method signatures having input parameters of String or Double.
                        try {
                            method = messageTO.getClass().getMethod("set" +
                                    key, new Class[] { String.class });
                        } catch (NoSuchMethodException e) {
                            isDoubleTypeInput = true;
                            method = messageTO.getClass().getMethod("set" +
                                    key, new Class[] { Double.class });
                        }

                        String rawInputValue = (String) mapMessage.getObject(key);
                        Object inputValue = null;

                        if (isDoubleTypeInput) {
                            inputValue = new Double(rawInputValue);
                        } else {
                            inputValue = rawInputValue;
                        }

                        method.invoke(messageTO, new Object[] { inputValue });
                    }
                } else {
                    throw new EJBException(
                        "onMessage: MessageType value can not be null.");
                }
            } else if (msg instanceof TextMessage) {
                logger.debug("onMessage: TEXT message received.");

                // Reformat the vertical-bar delimited payload to a Map.
                TextMessage textMsg = (TextMessage) msg;
                String payload = textMsg.getText();
                Map mapMessage = MercuryHandler.composePayloadMap(payload);

                String messageType = (String) mapMessage.get("messageType");

                if ((mapMessage != null) && (messageType != null)) {
                    // Obtain the appropriate value transfer object.
                    messageTO = MercuryHandler.getTOInstance(messageType);

                    // Populate the TO.
                    for (Iterator msgKeyEnum = mapMessage.keySet().iterator();
                            msgKeyEnum.hasNext();) {
                        String key = (String) msgKeyEnum.next();

                        Method method = null;

                        boolean isDoubleTypeInput = false;

                        // Attempt method signatures having input parameters of String or Double.
                        try {
                            method = messageTO.getClass().getMethod("set" +
                                    key, new Class[] { String.class });
                        } catch (NoSuchMethodException e) {
                            isDoubleTypeInput = true;
                            method = messageTO.getClass().getMethod("set" +
                                    key, new Class[] { Double.class });
                        }

                        String rawInputValue = (String) mapMessage.get(key);
                        Object inputValue = null;

                        if (isDoubleTypeInput) {
                            inputValue = new Double(rawInputValue);
                        } else {
                            inputValue = rawInputValue;
                        }

                        method.invoke(messageTO, new Object[] { inputValue });
                    }
                }
            } else {
                throw new EJBException(
                    "onMessage: Message class is unrecognized: " +
                    msg.getClass().getName());
            }

            // Invoke the Mercury API.
            ResultTO result = MercuryHandler.process(messageTO);

            if (!result.isSuccess()) {
                throw new EJBException("onMessage: Failed to use Mercury API: " +
                    result.getErrorString());
            }
        } catch (Throwable e) {
            logger.error("onMessage: Failed.", e);

            // Handle the error.
            try {
                CommonUtils.sendSystemMessage("onMessage: Failed." +
                    e.getMessage());
            } catch (Throwable th) {
                logger.error("onMessage: Could not send system message: ", th);
            }
        }
    }

    public void ejbRemove() {
    }

    public void setMessageDrivenContext(MessageDrivenContext ctx) {
        this.context = ctx;
    }
}
