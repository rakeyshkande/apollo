This project is not deployed/in use.
The source files have been merged into the order_processing project.

mercury-async\src\com\ftd\op\mercury\bo\MercuryHandler.java
mercury-async\src\com\ftd\op\mercury\mdb\AdHocInboundMDBean.java

Are now maintained in:
order_processing\src\com\ftd\op\mercury\bo\MercuryHandler.java
order_processing\src\com\ftd\op\mercury\mdb\AdHocInboundMDBean.java


Notes (6/28/2011 after JBOSS Migration):
�	The project generates a single ear file: mercuryasync.ear (no libraries generated)
�	The mercuryasync.ear is not deployed anywhere
	o	It is not listed on the Production Deployment Wiki Page (http://tungsten.ftdi.com/ftd/support/appversion/)
	o	It is not deployed in Oracle in consumer-test (copper1, copper2, tin1, tin2) or consumer-qa (lead1, lead2, lead3, lead4)
	o	There are no deployment configurations for this in the deploy_tool configuration files.
	o	There is no reference to build this project in the master: build.xml file, so, the project is not being built.
�	Code analysis indicates the following:
	o	The application implements/bundles the following JMS MDB: com.ftd.op.mercury.mdb.AdHocInboundMDBean mapped to JMSAQ/Queues/MERCURY_ASYNC
	o	This mdb and JMS mapping is currently implemented/bundled by order_processing


