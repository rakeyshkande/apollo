package com.ftd.newsletter.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import com.ftd.newsletterevents.client.novator.vo.ResponseType;
import com.ftd.newsletterevents.core.ErrorService;
import com.ftd.osp.utilities.plugins.Logger;

public class SocketTest {
	
	Logger logger = new Logger(SocketTest.class.getName());
	
	private String email="newslettertest@ftdi.com";
	
	/**
	 * Susbscribe = 'S' , Unsubscribe = 'U'
	 */
	private String SubUnsub = "S"; 

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		SocketTest test = new SocketTest();
		test.send();

	}
	
	private String send() {
		
        Socket socket = null;
        PrintWriter printer = null;
        BufferedReader reader = null;
		
		//String input = "News      U20110427024005  testingmails3@yahoo.in                              ";
		
        //do not alter this string.  it is the fixed length input for Newsletter-novator
		String input = "News      "+SubUnsub+"20110428021003  "+email+"                                     ";
		
		try {
			socket = new Socket("brass2.ftdi.com", 51000);
			//socket = new Socket("localhost", 51000);
			printer = new PrintWriter(socket.getOutputStream(), true);

			// Prepare a reader for the reply.
			reader = new BufferedReader(new InputStreamReader(
			            socket.getInputStream()));

			// Append a new line character and send.
			printer.print(input + "\r\n");
			printer.flush();

			try {
			    String reply = reader.readLine();

			    if ((reply == null) ||
			            !reply.equals(ResponseType.ACCEPTED.toString())) {
			    	System.out.println("something is wrong with the response: " + reply);
			    }

			    logger.info("SENT: " + input);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
