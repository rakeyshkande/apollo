package com.ftd.newsletterevents.client.novator;

import com.ftd.newsletterevents.bo.ejb.NewsletterPubEJB;
import com.ftd.newsletterevents.bo.ejb.NewsletterPubEJBHome;
import com.ftd.newsletterevents.client.novator.ejb.SocketOutboundEJB;
import com.ftd.newsletterevents.client.novator.ejb.SocketOutboundEJBHome;
import com.ftd.newsletterevents.client.novator.exception.ParseExceptionInbound;
import com.ftd.newsletterevents.client.novator.vo.CustomerEventVO;
import com.ftd.newsletterevents.client.novator.vo.ResponseType;
import com.ftd.newsletterevents.core.ErrorService;

import com.ftd.osp.utilities.plugins.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Serializable;

import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;

import javax.naming.Context;
import javax.naming.NamingException;

import javax.rmi.PortableRemoteObject;


public class SocketInbound {
    protected static Logger logger = new Logger(SocketInbound.class.getName());

    public void activate(final Integer port, final Integer portTimeout,
        Context jndiContext) {
        // Initialize this listener for Novator inbound socket messages.
        if ((port == null) || (portTimeout == null)) {
            logger.error(
                "postDeploy: FTD inbound socket initialization failed. Please specify a listen port and timeout in OC4J server.xml.");
        }

        ListenerThread listener = new ListenerThread(port.intValue(),
                portTimeout.intValue(), jndiContext);

        // Designate this as a daemon thread since the JVM will only shutdown
        // if all active threads are daemon.
        // This precludes the need for a Runtime.addShutdownHook().
        listener.setDaemon(true);

        try {
            // Bind the thread to JNDI so that the container does not terminate it.
            jndiContext.rebind("ftd-novator-inbound-server", listener);

            listener.start();
        } catch (NamingException e) {
            logger.error("activate: Could not bind FTD Novator inbound server to JNDI.",
                e);
        } finally {
            try {
                jndiContext.close();
            } catch (Exception e) {
                logger.error("activate: JNDI clean-up failed.", e);
            }
        }
    }
}


class ListenerThread extends Thread implements Serializable {
    protected static Logger logger = new Logger(ListenerThread.class.getName());
    private NewsletterPubEJB npEjb = null;
    private SocketOutboundEJB soEjb = null;
    private int port;
    private int portTimeout;

    ListenerThread(int port, int portTimeout, Context jndiContext) {
        this.port = port;
        this.portTimeout = portTimeout;

        try {
            // Create a RMI context to obtain an EJB co-located in
            // a separate ear.
            //            Context rmiContext = Main.getRmiContext("newsletter-pub");
            //            try {
            NewsletterPubEJBHome home = (NewsletterPubEJBHome) PortableRemoteObject.narrow(jndiContext.lookup(
                        "NewsletterPubEJB"), NewsletterPubEJBHome.class);

            // create an EJB instance 
            npEjb = home.create();
            
            // Obtain the publisher since we need to reflect the exact message back to
            // the sender.
            SocketOutboundEJBHome outboundHome = (SocketOutboundEJBHome) PortableRemoteObject.narrow(jndiContext.lookup(
                        "SocketOutboundEJB"), SocketOutboundEJBHome.class);

            // create an EJB instance 
            soEjb = outboundHome.create();

            //            } 
            //            finally {
            //                try {
            //                    rmiContext.close();
            //                } catch (Exception e) {
            //                    logger.error("init: RMI Initial Context clean-up failed.", e);
            //                }
            //            }
        } catch (Throwable e) {
            logger.error("init: Failed to obtain EJB.", e);
        }
    }

    public void run() {
        try {
            //                            final SSLServerSocketFactory factory = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();
            //                            final SSLServerSocket sslSocket = (SSLServerSocket) factory.createServerSocket(Integer.parseInt(
            //                                        _LISTEN_PORT));
            final ServerSocket socket = new ServerSocket(port);

            // Set a timeout to designate that a client request has not been received.
            socket.setSoTimeout(portTimeout);
            logger.info(
                "postDeploy: FTD Novator socket server has been started on port " +
                port + " with listen timeout of " + portTimeout +
                " milliseconds.");

            while (true) {
                try {
                    new SocketHandler(socket.accept(), npEjb, soEjb).start();
                } catch (SocketTimeoutException e) {
                    // This is thrown at timeout.
                    // Check a variable to continue.
                    String msg =
                        "FTD Novator listen socket timed out because no request received in " +
                        (portTimeout / 60000) + " minutes.";
                    logger.warn(msg);

                    try {
                        ErrorService.process("WARNING",
                            new RuntimeException(msg));
                    } catch (Throwable t) {
                        logger.error("run: Failed to record timeout warning. Proceeding to re-establish socket listener.",
                            t);
                    }

                    // Continue with the loop.
                    continue;
                }
            }
        } catch (Throwable e) {
            logger.error("run: FTD Novator socket FAILURE on port " + port, e);
        }
    }
}


class SocketHandler extends Thread {
    Logger logger = new Logger(SocketHandler.class.getName());
    Socket incoming;
    NewsletterPubEJB npEjb;
    SocketOutboundEJB soEjb;

    SocketHandler(Socket incoming, NewsletterPubEJB npEjb, SocketOutboundEJB soEjb) {
        this.incoming = incoming;
        this.npEjb = npEjb;
        this.soEjb = soEjb;
    }

    public void run() {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                        incoming.getInputStream()));
            PrintWriter printer = new PrintWriter(incoming.getOutputStream(),
                    true);

            // A message is a single line; i.e. only a single line is received at a time.
            String str = reader.readLine();

            try {
                if (str != null) {
                    CustomerEventVO ceVO = Translator.unmarshall(str);

                    // If the payload is translated successfully, send a response.
                    printer.print(ResponseType.ACCEPTED + "\r\n");
                    logger.info("ACCEPTED: " + str);

                    // Publish the message to internal systems.
                    npEjb.process(ceVO.getEmailAddress(), ceVO.getCompanyId(),
                        ceVO.getStatus(), ceVO.getOperatorId(),
                        ceVO.getOriginTimestamp());
                    
                    // Mirror the message right back to the sender.
                    soEjb.send(str);
                }
            } catch (ParseExceptionInbound t) {
                // Send a fail response.
                printer.print(ResponseType.REJECTED + "\r\n");
                logger.info("REJECTED: " + t.getMessage() + " --> " + str);
                ErrorService.process(ErrorService.ERROR_TYPE_PUBLISH_FAILURE,
                    t, str);
            } catch (Throwable t) {
                logger.error("run: Could not process Novator payload: " + str, t);
                ErrorService.process(ErrorService.ERROR_TYPE_PUBLISH_FAILURE,
                    t, str);
            } finally {
                printer.close();

                try {
                    reader.close();
                } catch (IOException e) {
                    logger.error("run: Socket clean-up failed.", e);
                }
            }
        } catch (IOException e) {
            logger.error("run: Could not read client socket payload.", e);
            ErrorService.process(ErrorService.ERROR_TYPE_PUBLISH_FAILURE, e);
        } finally {
            try {
                incoming.close();
            } catch (IOException e) {
                logger.error("run: Socket clean-up failed.", e);
            }
        }
    }
}
