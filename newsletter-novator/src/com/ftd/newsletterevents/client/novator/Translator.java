package com.ftd.newsletterevents.client.novator;

import com.ftd.newsletterevents.client.novator.exception.ParseExceptionInbound;
import com.ftd.newsletterevents.client.novator.vo.CustomerEventVO;
import com.ftd.newsletterevents.vo.INewsletterEvent;
import com.ftd.newsletterevents.vo.StatusType;

import com.ftd.osp.utilities.plugins.Logger;

import java.util.Calendar;
import java.util.Date;


public class Translator {
    protected static Logger logger = new Logger(Translator.class.getName());

    private Translator() {
    }

    private static String padString(String s, int n, char c, boolean paddingLeft) {
        StringBuffer str = new StringBuffer(s);
        int strLength = str.length();

        if ((n > 0) && (n > strLength)) {
            for (int i = 0; i <= n; i++) {
                if (paddingLeft) {
                    if (i < (n - strLength)) {
                        str.insert(0, c);
                    }
                } else {
                    if (i > strLength) {
                        str.append(c);
                    }
                }
            }
        }

        return str.toString();
    }

    public static String marshall(INewsletterEvent neVO) {
        StringBuffer sb = new StringBuffer(218);
        sb.setLength(218);

        // Static line prefix.
        sb.insert(0, padString("NEWS", 10, ' ', false));

        // Subscribe/Unsubscribe
        sb.insert(10, neVO.getStatus());

        // Process the timestamp.
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(neVO.getOriginTimestamp().longValue());

        // 4-digit year
        sb.insert(11, Integer.toString(cal.get(Calendar.YEAR)));

        // month
        sb.insert(15,
            padString(Integer.toString(cal.get(Calendar.MONTH) + 1), 2, '0',
                true));

        // day
        sb.insert(17,
            padString(Integer.toString(cal.get(Calendar.DAY_OF_MONTH)), 2, '0',
                true));

        // hour
        sb.insert(19,
            padString(Integer.toString(cal.get(Calendar.HOUR_OF_DAY)), 2, '0',
                true));

        // minute
        sb.insert(21,
            padString(Integer.toString(cal.get(Calendar.MINUTE)), 2, '0', true));

        // second
        sb.insert(23,
            padString(Integer.toString(cal.get(Calendar.SECOND)), 2, '0', true));

        // source
        sb.insert(25, "OP");

        // email
        sb.insert(27, padString(neVO.getEmailAddress(), 50, ' ', false));

        // co-brand code
        sb.insert(207, padString(" ", 10, ' ', false));
        
        // co-brand action
        sb.insert(217, neVO.getStatus());
        
        // Finally, trim the message to the appropriate size.
        sb.setLength(218);
        
        return sb.toString();
    }

    public static CustomerEventVO unmarshall(String msg)
        throws ParseExceptionInbound {
        // Verify the string length.
        if (msg == null) {
            throw new ParseExceptionInbound(
                "unmarshall: Input string can not be null.");
        }

        try {
            // Parse the String into a Value Object.
            String email = msg.substring(27, 77).trim();

            if ((email == null) || (email.length() == 0)) {
                throw new ParseExceptionInbound(
                    "unmarshall: Invalid email value: " + email);
            }

            String operatorId = msg.substring(25, 27).trim();

            // Determine the subscribe status, check both the action
            // and co-brand action metrics.
            String action = null;
            String companyId = "FTD";

            // Since the Subscribe and unsubscribe status can be located
            // in two places within the payload, check both places.
            action = msg.substring(10, 11).trim();

            if ((action == null) ||
                    (!action.equals(StatusType.SUBSCRIBED.toString()) &&
                    !action.equals(StatusType.UNSUBSCRIBED.toString()))) {
                logger.debug("unmarshall: Input specifies a co-brand.");
                //companyId = msg.substring(207, 217).trim();
                action = msg.substring(217, 218).trim();

                // Perform a second verification to ensure that a proper un/subscribe status
                // was specified.
                if ((action == null) ||
                        (!action.equals(StatusType.SUBSCRIBED.toString()) &&
                        !action.equals(StatusType.UNSUBSCRIBED.toString()))) {
                    throw new ParseExceptionInbound(
                        "unmarshall: Invalid action value: " + action);
                }
            } else {
                logger.debug("unmarshall: Input does NOT specify a co-brand.");

                // Default to FTD if no co-brand is specified.
                companyId = "FTD";
            }

            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.YEAR, Integer.parseInt(msg.substring(11, 15)));
            cal.set(Calendar.MONTH, Integer.parseInt(msg.substring(15, 17)));
            cal.set(Calendar.DAY_OF_MONTH,
                Integer.parseInt(msg.substring(17, 19)));
            cal.set(Calendar.HOUR_OF_DAY,
                Integer.parseInt(msg.substring(19, 21)));
            cal.set(Calendar.MINUTE, Integer.parseInt(msg.substring(21, 23)));
            cal.set(Calendar.SECOND, Integer.parseInt(msg.substring(23, 25)));

            return new CustomerEventVO(email, companyId,
                action.equals(StatusType.SUBSCRIBED.toString())
                ? StatusType.SUBSCRIBED : StatusType.UNSUBSCRIBED, operatorId,
                new Date(cal.getTimeInMillis()));
        } catch (ParseExceptionInbound e) {
            throw e;
        } catch (Throwable e) {
            throw new ParseExceptionInbound("unmarshall: " + e.getMessage());
        }
    }
}
