package com.ftd.newsletterevents.client.novator.vo;

import com.ftd.newsletterevents.vo.StatusType;

import java.util.Date;


public class CustomerEventVO {
    private String email;
    private String companyId;
    private StatusType status;
    private String operatorId;
    private Date originTimestamp;

    public CustomerEventVO(String email, String companyId, StatusType status,
        String operatorId, Date originTimestamp) {
        this.email = email;
        this.companyId = companyId;
        this.status = status;
        this.operatorId = operatorId;
        this.originTimestamp = originTimestamp;
    }

    public String getEmailAddress() {
        return email;
    }

    public String getCompanyId() {
        return companyId;
    }

    public StatusType getStatus() {
        return status;
    }

    public String getOperatorId() {
        return operatorId;
    }

    public Date getOriginTimestamp() {
        return originTimestamp;
    }
}
