package com.ftd.newsletterevents.client.novator.vo;

public final class ResponseType {
    public static final ResponseType ACCEPTED = new ResponseType("CON");
    public static final ResponseType REJECTED = new ResponseType("DEN");
    private String type;

    private ResponseType(String type) {
        this.type = type;
    }

    public String toString() {
        return type;
    }

    public int hashCode() {
        return type.hashCode();
    }

    public boolean equals(Object that) {
        if (that instanceof ResponseType) {
            return this.toString().equals(that.toString());
        }

        return false;
    }
}