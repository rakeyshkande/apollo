package com.ftd.newsletterevents.client.novator.ejb;
import javax.ejb.EJBLocalHome;
import javax.ejb.CreateException;

public interface SocketOutboundEJBLocalHome extends EJBLocalHome 
{
  SocketOutboundEJBLocal create() throws CreateException;
}