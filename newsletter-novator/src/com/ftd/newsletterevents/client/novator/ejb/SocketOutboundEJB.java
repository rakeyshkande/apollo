package com.ftd.newsletterevents.client.novator.ejb;
import javax.ejb.EJBObject;
import java.rmi.RemoteException;

public interface SocketOutboundEJB extends EJBObject 
{
  void send(String msg) throws RemoteException;
}