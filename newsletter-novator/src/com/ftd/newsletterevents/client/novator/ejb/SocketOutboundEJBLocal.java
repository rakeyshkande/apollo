package com.ftd.newsletterevents.client.novator.ejb;
import javax.ejb.EJBLocalObject;

public interface SocketOutboundEJBLocal extends EJBLocalObject 
{
  void send(String msg);
}