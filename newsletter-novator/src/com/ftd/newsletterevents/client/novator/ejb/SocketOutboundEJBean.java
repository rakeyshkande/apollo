package com.ftd.newsletterevents.client.novator.ejb;

import com.ftd.newsletterevents.client.novator.vo.ResponseType;
import com.ftd.newsletterevents.core.ErrorService;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;

import java.net.Socket;

import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;

import javax.naming.InitialContext;


public class SocketOutboundEJBean implements SessionBean {
    protected static Logger logger = new Logger(SocketOutboundEJBean.class.getName());
    private String _OUTBOUND_SERVER;
    private int _OUTBOUND_SERVER_PORT;
    private final static String NN_CONTEXT = "NEWSLETTER_CONFIG";

    //    private BufferedReader reader = null;
    private SessionContext sessionContext;

    protected void init() {
        // Initialize instance variables to support socket communication.
        InitialContext jndiContext = null;

        try {
            try {
                jndiContext = new InitialContext();

                ConfigurationUtil cu = ConfigurationUtil.getInstance();
                
                _OUTBOUND_SERVER = cu.getFrpGlobalParm(NN_CONTEXT,"novator_server");
                _OUTBOUND_SERVER_PORT = new Integer(cu.getFrpGlobalParm(NN_CONTEXT,"novator_server_port"));

                if ((_OUTBOUND_SERVER_PORT == 0) || (_OUTBOUND_SERVER == null)) {
                    logger.error(
                        "init: FTD Novator socket initialization failed. Please specify a Novator server destination and port.");
                }
            } catch (Exception e) {
                logger.error("init: Failed to initialize Novator socket connection.",
                    e);
                throw new EJBException(e);
            }
        } finally {
            if (jndiContext != null) {
                try {
                    jndiContext.close();
                } catch (Throwable e) {
                    logger.error("ejbCreate: JNDI context clean-up logic failed.",
                        e);
                }
            }
        }
    }

    public void send(String msg) {
        send(msg, 1);
    }

    private void send(String msg, int attemptCount) {
        Socket socket = null;
        PrintWriter printer = null;
        BufferedReader reader = null;

        try {
            //            SSLSocketFactory factory = (SSLSocketFactory) SSLSocketFactory.getDefault();
            //
            //            sslSocket = (SSLSocket) factory.createSocket(_OUTBOUND_SERVER,
            //                    _OUTBOUND_SERVER_PORT);
            //            logger.debug(
            //                "send: Successfully connected to Novator socket server " +
            //                _OUTBOUND_SERVER + ":" + _OUTBOUND_SERVER_PORT);
            socket = new Socket(_OUTBOUND_SERVER, _OUTBOUND_SERVER_PORT);
            printer = new PrintWriter(socket.getOutputStream(), true);

            // Prepare a reader for the reply.
            reader = new BufferedReader(new InputStreamReader(
                        socket.getInputStream()));

            // Append a new line character and send.
            printer.print(msg + "\r\n");
            printer.flush();

            try {
                String reply = reader.readLine();

                if ((reply == null) ||
                        !reply.equals(ResponseType.ACCEPTED.toString())) {
                    ErrorService.process("",
                        new Exception("send: payload rejected by Novator: " +
                            reply), msg);
                }

                logger.info("SENT: " + msg);
            } catch (Exception e) {
                logger.warn("send: Could not read reply on attempt " +
                    attemptCount + " for payload: " + msg);

                if (attemptCount >= 3) {
                    throw e;
                } else {
                    // Attempt another send.
                    int nextAttempt = attemptCount + 1;
                    send(msg, nextAttempt);
                }
            }
        } catch (Exception e) {
            String err = "send: Failed to send to Novator socket server: " +
                msg;
            logger.error(err, e);
            ErrorService.process(err, e);
            throw new EJBException(e);
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (Exception e) {
                logger.error("send: Socket reader clean-up failed.", e);
            }

            if (printer != null) {
                printer.flush();
                printer.close();
            }

            try {
                if (socket != null) {
                    socket.close();
                }
            } catch (IOException e) {
                logger.error("send: Socket clean-up failed.", e);
            }
        }
    }

    public void ejbCreate() {
        init();
    }

    public void ejbActivate() {
    }

    public void ejbPassivate() {
    }

    public void ejbRemove() {
    }

    public void setSessionContext(SessionContext ctx) {
        this.sessionContext = ctx;
    }
}
