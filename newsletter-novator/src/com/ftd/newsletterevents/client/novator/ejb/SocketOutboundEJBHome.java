package com.ftd.newsletterevents.client.novator.ejb;
import javax.ejb.EJBHome;
import java.rmi.RemoteException;
import javax.ejb.CreateException;

public interface SocketOutboundEJBHome extends EJBHome 
{
  SocketOutboundEJB create() throws RemoteException, CreateException;
}