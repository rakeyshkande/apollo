package com.ftd.newsletterevents.client.novator;

import com.ftd.newsletterevents.bo.IEventHandler;
import com.ftd.newsletterevents.client.novator.ejb.SocketOutboundEJB;
import com.ftd.newsletterevents.client.novator.ejb.SocketOutboundEJBHome;
import com.ftd.newsletterevents.vo.INewsletterEvent;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import javax.naming.InitialContext;

import javax.rmi.PortableRemoteObject;


public class HandlerOutbound extends UnicastRemoteObject
    implements IEventHandler {
    public HandlerOutbound() throws RemoteException {
        super(); // sets up networking
    }

    public void processMessage(INewsletterEvent neVO) throws RemoteException {

        // Obtain a reference to the outbound EJB.
        InitialContext jndiContext = null;

        try {
            try {
                jndiContext = new InitialContext();

                SocketOutboundEJBHome home = (SocketOutboundEJBHome) PortableRemoteObject.narrow(jndiContext.lookup(
                            "SocketOutboundEJB"), SocketOutboundEJBHome.class);

                SocketOutboundEJB soEjb = home.create();

                try {
                    // Parse the value object and send.  
                    soEjb.send(Translator.marshall(neVO));
                } catch (Throwable e) {
                    throw new RemoteException("processMessage: outbound send failed.",
                        e);
                }
            } catch (Throwable e) {
                throw new RemoteException("HandlerOutbound: SocketOutboundEJB failed.",
                    e);
            }
        } finally {
            if (jndiContext != null) {
                try {
                    jndiContext.close();
                } catch (Throwable e) {
                    throw new RemoteException("HandlerOutbound: JNDI context clean-up logic failed.",
                        e);
                }
            }
        }
    }
}
