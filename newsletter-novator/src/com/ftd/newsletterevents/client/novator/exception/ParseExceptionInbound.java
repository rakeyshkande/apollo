package com.ftd.newsletterevents.client.novator.exception;

public class ParseExceptionInbound extends Exception {
    public ParseExceptionInbound(String message) {
        super(message);
    }

    public ParseExceptionInbound(String message, Throwable t) {
        super(message, t);
    }
}
