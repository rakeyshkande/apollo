/**
 * 
 */
package com.ftd.newsletterevents;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Properties;

import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.xml.sax.SAXException;

import com.ftd.newsletterevents.client.novator.HandlerOutbound;
import com.ftd.newsletterevents.client.novator.SocketInbound;
import com.ftd.osp.utilities.j2ee.ResourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

/**
 * @author cjohnson
 *
 */
public class InitServlet extends HttpServlet {
	
	static Logger logger = new Logger(InitServlet.class.getName());
	
	private static final String configFile = "novator.properties";
	private static Document config;
	private static HandlerOutbound eventHandler;
	
	@Override
	public void init() throws ServletException {
		logger.info("Initializing Novator Listener Servlet");

		super.init();
		Properties props = null;
		try {
			props = readConfigs();
		} catch (Exception e1) {
			logger.error("Unable to read configuration file: " + configFile);
			e1.printStackTrace();
		}
		
        try {
            InitialContext jndiContext = new InitialContext();

            // Bind a Remote object for invocation by the MDB for
            // outbound messages.            
            eventHandler = new HandlerOutbound();
            
            
            String alias = props.getProperty("ieventhandler-bind-name");
//            String alias = (String) jndiContext.lookup(
//                    "java:comp/env/ieventhandler-bind-name");
            jndiContext.rebind(alias, eventHandler);
            logger.info(
                "main: Successfully bound IEventHandler implementation to alias: " +
                alias);

            // Initialize instance variables to support socket communication for
            Integer port = new Integer(props.getProperty("inbound-server-port"));
            // inbound messages.
//            Integer port = (Integer) jndiContext.lookup(
//                    "java:comp/env/inbound-server-port");
            Integer portTimeout = new Integer(props.getProperty("inbound-server-port-timeout"));
//            Integer portTimeout = (Integer) jndiContext.lookup(
//                    "java:comp/env/inbound-server-port-timeout");

            SocketInbound socketServer = new SocketInbound();
            socketServer.activate(port, portTimeout, jndiContext);
        } catch (Throwable e) {
            logger.error("init: Failed to initialize FTD inbound socket server.",
                e);
        	e.printStackTrace();
        }
	}

	private synchronized static Properties readConfigs() throws Exception {
		
		URL url = ResourceUtil.getInstance().getResource(configFile);
		if (url != null) {
			Properties props = new Properties();
			props.load(ResourceUtil.getInstance().getResourceAsStream(configFile));
			return props;
		}
		
		
		
		return null;
	}

	@Override
	public void destroy() {
		System.out.println("init servlet destroyed");
		// TODO Auto-generated method stub
		super.destroy();
	}



}
