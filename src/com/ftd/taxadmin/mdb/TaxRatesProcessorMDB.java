package com.ftd.taxadmin.mdb;

import javax.ejb.EJBException;
import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.taxadmin.bo.TaxRatesProcessorBO;
import com.ftd.taxadmin.dao.TaxAdminDAO;

/**
 * @author smeka
 * 
 */
@SuppressWarnings("serial")
public class TaxRatesProcessorMDB implements MessageDrivenBean, MessageListener { 
	
	private static Logger logger = new Logger(TaxRatesProcessorMDB.class.getName());
	
	public static final String LOAD_NEW_TAX_RATES = "loadTaxRates";	
	public static final String DOWNLOAD_TAX_RATES = "downloadTaxRates";

	@Override
	public void onMessage(Message msg) { 
		
		TextMessage textMessage = (TextMessage) msg;
		String payloadTxt = null;
		
		try {
			payloadTxt = textMessage.getText();
		} catch (JMSException e) {
			logger.error("Unable to identify the text message," , e);
			return;
		}
		
		if(logger.isDebugEnabled()) {
			logger.debug("request triggered for action on tax rates, " + payloadTxt);
		}
		TaxAdminDAO taxAdminDAO = null;
		TaxRatesProcessorBO taxRatesProcessorBO = null;
		String requestType = payloadTxt;
		String fileName = null;
		
		try {
			taxAdminDAO = new TaxAdminDAO();
			taxRatesProcessorBO = new TaxRatesProcessorBO(taxAdminDAO);
			
			if(payloadTxt != null && payloadTxt.indexOf(TaxRatesProcessorBO.COMMA_SEPERATOR) > 0) {
				requestType = payloadTxt.split(TaxRatesProcessorBO.COMMA_SEPERATOR)[0];
				fileName = payloadTxt.split(TaxRatesProcessorBO.COMMA_SEPERATOR)[1];
			}
			
			if(LOAD_NEW_TAX_RATES.equalsIgnoreCase(requestType)) {
				taxRatesProcessorBO.loadNewTaxRates(fileName);			
			} else if(DOWNLOAD_TAX_RATES.equalsIgnoreCase(requestType)) {
				taxRatesProcessorBO.downloadTaxRates();
			} else {
				logger.info("Invalid request to TaxRatesUpdateMDB, " + requestType);
			}
			
		} catch (Exception e) {
			logger.error("Unable to process the TaxRatesProcessorMDB request for " + requestType);
			logger.error(e);
		} finally {			
			if(taxAdminDAO != null) {
				taxAdminDAO.closeConnection();
			}			
		}	
	}

	@Override
	public void ejbRemove() throws EJBException {}

	@Override
	public void setMessageDrivenContext(MessageDrivenContext ctxt) throws EJBException {
		 
	}
	
	public void ejbCreate() {	}


}
