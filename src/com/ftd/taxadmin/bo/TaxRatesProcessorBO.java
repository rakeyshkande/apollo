package com.ftd.taxadmin.bo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.csvreader.CsvReader;
import com.ftd.ftdutilities.CalculateTaxUtil;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.taxadmin.dao.TaxAdminDAO;
import com.ftd.taxadmin.util.FTPUtil;

/**
 * @author smeka
 *
 */
public class TaxRatesProcessorBO {
	
	private static Logger logger = new Logger(TaxRatesProcessorBO.class.getName());

	private static final String FTD_TAX_FTP_SERVER = "FTD_TAX_FTP_SERVER";
	private static final String FTP_SERVER_USERNAME = "FTP_SERVER_USERNAME";
	private static final String FTP_SERVER_PWD = "FTP_SERVER_PWD";
	private static final String FTP_TAX_RATES_DIR = "FTP_TAX_RATES_DIR";	
	private static final String TAX_RATES_REMOTE_FILE_NAME = "TAX_RATES_REMOTE_FILE_NAME";
	private static final String LOCAL_TAX_RATES_DIR = "LOCAL_TAX_RATES_DIR";	
	private static final String TAX_RATES_LOCAL_FILE_PREFIX = "TAX_RATES_LOCAL_FILE_PREFIX"; 

	private static final String PAGE_SOURCE = "TAX_SERVICE_PAGE";	
	private static final String NOPAGE_SOURCE = "TAX_SERVICE_NO_PAGE";
	private static final String TAX_RATES_MSG_SUBJ = "Tax Rates Updates";
	private static final String ERROR_ALERT = "ERROR";
	
	private static final String FILESEPERATOR = "/";
	public static final String COMMA_SEPERATOR = ",";
	private static final String UNDERSCORE = "_";
	
	private TaxAdminDAO taxAdminDAO;
	
	
	/**
	 * @param taxAdminDAO
	 */
	public TaxRatesProcessorBO(TaxAdminDAO taxAdminDAO) {		
		this.taxAdminDAO = taxAdminDAO;
	}
	
	
	/**
	 * Downloads the vertex file from remote FTP server and saves it to local server.
	 */
	public void downloadTaxRates() {
		long startTime = System.currentTimeMillis();
		if(logger.isDebugEnabled()) {
			logger.debug("Downloading the vertex tax rates file started @: "+ startTime);
		} 
		StringBuffer message = null;
		
		FTPUtil ftpUtil = new FTPUtil();
		try {
			
			ConfigurationUtil cacheUtil = ConfigurationUtil.getInstance();	
			
			// Step 1: get the remote FTP file (vertex tax rates)
			loginToFTPServer(cacheUtil, ftpUtil);
			String remoteTaxRatesLoc = cacheUtil.getFrpGlobalParm(CalculateTaxUtil.TAX_SERVICE_CONTEXT, FTP_TAX_RATES_DIR);	 
			String remoteFileName = cacheUtil.getFrpGlobalParm(CalculateTaxUtil.TAX_SERVICE_CONTEXT, TAX_RATES_REMOTE_FILE_NAME);
			logger.info("retrieve the file," + remoteFileName + " from location " + remoteTaxRatesLoc);
			byte[] taxRatesFileData = ftpUtil.getFileData(remoteFileName, remoteTaxRatesLoc);
			
			
			// Step 2: Check if a new file exists on remote server.
			if(taxRatesFileData == null || taxRatesFileData.length == 0) {
				message = new StringBuffer("No updates found for tax rates.");
				sendSystemAlert(message, NOPAGE_SOURCE, ERROR_ALERT);
				return;
			}
			
			// Step3: Save the file to local server temporarily
			File localTaxRatesFile = getLocalTaxRatesFile(cacheUtil, remoteFileName);
			localTaxRatesFile.createNewFile();
			OutputStream os = new FileOutputStream(localTaxRatesFile);
			os.write(taxRatesFileData);
			os.close();
			
			// Step4: Read the file stream and save to DB.
			boolean  isFileSaved = saveNewTaxRatesFile(localTaxRatesFile.getAbsolutePath());
			if(!isFileSaved) {
				message = new StringBuffer("Unable to save the new tax rates to DB.");
				sendSystemAlert(message, NOPAGE_SOURCE, ERROR_ALERT);
				return;
			}
		
			// Step 4: File saved successfully, load data to DB and delete the file on remote server
			if(loadNewTaxRates(localTaxRatesFile.getAbsolutePath())) {	
				logger.info("Data loaded Successfully, deleting the remote file....");
				ftpUtil.deleteFile(remoteFileName);				
			}
			
			long endTime = System.currentTimeMillis();
			if(logger.isDebugEnabled()) {
				logger.debug("Downloading the vertex tax rates file completed @: "+ endTime);
			}
			
			logger.info("Downloading and loading new tax rates took " + (endTime - startTime) + " milli seconds.");
			
		} catch (Exception e) {			
			logger.error(e);
			message = new StringBuffer().append("Unable to process taxrate updates, ").append(e.getMessage());
			sendSystemAlert(message, PAGE_SOURCE, ERROR_ALERT);
			
		} finally {
			
			try {
				ftpUtil.logout();
			} catch (Exception e1) {
				logger.error("Unable to log out the ftp server, " + e1.getMessage());
			}						
		}
		
	}


	/**
	 * Load the tax rates to DB.
	 * Drop the current staged data, and recreate the data into tax.vertex_staging
	 * Insert/Update the tax rates to master table tax.tax_rates
	 * 
	 * @param taxRatesFile  
	 * @return
	 */
	public boolean loadNewTaxRates(String taxRatesFile) {		
		long startTime = System.currentTimeMillis();
		boolean isReloaded = false;
		
		if(logger.isDebugEnabled()) {
			logger.debug("Loading the new vertex tax rates to DB started @: "+ startTime);
		}
		
		if(StringUtils.isEmpty(taxRatesFile)) {
			logger.error("Mandatory message text/ file name  missing to laod the tax rates, cannot load the tax rates");
			return isReloaded;
		}
		
		StringBuffer message = null;
				
		try {			
			//delete master data
			boolean masterDataDeleted = taxAdminDAO.deleteAllMasterTaxRates();
			if(!masterDataDeleted) {
				throw new Exception("Unable to delete the old master data, verify!!"); 
			}			
			//reload master data
			isReloaded = insertNewTaxRates(taxRatesFile);
			if(!isReloaded) {
				throw new Exception("Unable to delete the old master data, verify!!");
			}
			
			File localTempFile = new File(taxRatesFile);
			if(localTempFile.exists()) {
				logger.info("Data loaded Successfully, deleting the temp file on app server....");	
				localTempFile.delete();
			}
			
			long endTime = System.currentTimeMillis();
			if(logger.isDebugEnabled()) {
				logger.debug("Loading the new vertex tax rates to DB completed @: "+ endTime);
			}			
			logger.info("loading tax rates completed in " + (endTime - startTime) + " milli seconds.");	
			
		} catch (Exception e) {
			logger.error(e);
			message = new StringBuffer().append("Error caught: ").append(e.getMessage());
			sendSystemAlert(message, PAGE_SOURCE, ERROR_ALERT); 
		} 	
		
		return isReloaded;
	}

		
	/** Get the server login details from config params and login.
	 * @param cacheUtil
	 * @param ftpUtil
	 * @throws Exception
	 */
	private void loginToFTPServer(ConfigurationUtil cacheUtil, FTPUtil ftpUtil) throws Exception {
		String ftpServer = null;
		String ftpUsername = null;
		String ftpPwd = null;
		
		ftpServer = cacheUtil.getFrpGlobalParm(CalculateTaxUtil.TAX_SERVICE_CONTEXT, FTD_TAX_FTP_SERVER);
		ftpUsername = cacheUtil.getSecureProperty(CalculateTaxUtil.TAX_SERVICE_CONTEXT, FTP_SERVER_USERNAME);
		ftpPwd = cacheUtil.getSecureProperty(CalculateTaxUtil.TAX_SERVICE_CONTEXT, FTP_SERVER_PWD);
		
		ftpUtil.login(ftpServer, ftpUsername, ftpPwd); 
		logger.info("logged into FTP server successfully");
	}
	
	

 	/** Get Local Temporary tax rates file.
 	 * @param cacheUtil
 	 * @param remoteFileName
 	 * @return
 	 * @throws Exception
 	 */
 	private File getLocalTaxRatesFile(ConfigurationUtil cacheUtil, String remoteFileName) throws Exception {
		String localTaxRatesDir = null; 
		String localFilePrefix = null;
		StringBuffer localFileName = null;
		
		localTaxRatesDir = cacheUtil.getFrpGlobalParm(CalculateTaxUtil.TAX_SERVICE_CONTEXT, LOCAL_TAX_RATES_DIR);		
		File localFileDirectory = new File(localTaxRatesDir);
		if (!localFileDirectory.exists()) {
			localFileDirectory.mkdirs();
		}

		localFilePrefix = cacheUtil.getFrpGlobalParm(CalculateTaxUtil.TAX_SERVICE_CONTEXT, TAX_RATES_LOCAL_FILE_PREFIX);
		localFileName = new StringBuffer(localTaxRatesDir).append(FILESEPERATOR).append(localFilePrefix).append(UNDERSCORE).append(remoteFileName);			
		File localTaxRatesFile = new File(localFileName.toString());
		
		return localTaxRatesFile;
	}
	
	/** Read the temporary file and insert new tax rates row by row to DB 
	 * @param taxRatesFile
	 * @return
	 * @throws Exception
	 */
	private boolean insertNewTaxRates(String taxRatesFile) throws Exception {
		if(logger.isDebugEnabled()) {
			logger.debug("************loadNewTaxRates()***************");
		}
		CsvReader reader = null;
		try {
			reader = new CsvReader(taxRatesFile);
			reader.setSkipEmptyRecords(true);	
			reader.getValues();
			
			Map<String, String> inputParams = new HashMap<String, String>();			
			Map<String, String> outputs = null; 
			
			while (reader.readRecord()) { 
				inputParams.clear();
				outputs = taxAdminDAO.insertTaxRate(reader, inputParams);
				if (TaxAdminDAO.NO.equalsIgnoreCase(outputs.get(TaxAdminDAO.OUT_STATUS))) { 
					StringBuffer message = new StringBuffer("unable to insert the tax rates, Error caught: ").append(outputs.get(TaxAdminDAO.OUT_MESSAGE));
					throw new SQLException(message.toString());
				}
			}
		} catch (Exception e) {
			logger.error("Error caught parsing the tax rates file");
			throw e;
		} finally {
			if (reader != null) {
				reader.close();
			}
		}
		return true;
	}
	
	/**
	 * @param message
	 * @param messageSource
	 * @param type 
	 */
	private void sendSystemAlert(StringBuffer message, String messageSource, String type) {		
		if(message == null || message.toString().length() == 0) {
			return;
		}
		//build system VO
		SystemMessengerVO sysMessage = new SystemMessengerVO();
		sysMessage.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
		sysMessage.setSource(messageSource);
		sysMessage.setType(type);
		sysMessage.setMessage(message.toString());
		sysMessage.setSubject(TAX_RATES_MSG_SUBJ);
		taxAdminDAO.sendSystemAlert(sysMessage);			
	}
	
	/** Archive the new tax rates file.
	 * @param taxRatesFilePath
	 * @param taxAdminDAO
	 * @return
	 * @throws Exception
	 */
	private boolean saveNewTaxRatesFile(String taxRatesFilePath) throws Exception {
		if(logger.isDebugEnabled()) {
			logger.debug("************getCSVFileAsStream***************");
		}		
		StringBuilder taxRatesFileData;
		try {
			taxRatesFileData = getCSVFileAsStream(taxRatesFilePath);
			
			if(taxRatesFileData == null || taxRatesFileData.length() == 0) {
				throw new Exception("Unable to read the taxrates from temporary location or no file exists" );
			}
			return taxAdminDAO.saveNewTaxRatesFile(taxRatesFileData);
		} catch (Exception e) {
			throw new Exception("Unable to read the taxrates from temporary location ", e );
		} finally {
			taxRatesFileData  = null;
		}
	}
	
	/**Reads the file from local server path and converts file data to string
	 * @param localFilePath
	 * @return
	 * @throws Exception
	 */
	private StringBuilder getCSVFileAsStream(String localFilePath) throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("************getCSVFileAsStream***************");
		}
		File taxRatesFile = null;
		FileInputStream fis = null;
		StringBuilder sb = null;

		try {

			taxRatesFile = new File(localFilePath);
			long length = taxRatesFile.length();

			fis = new FileInputStream(taxRatesFile);
			sb = new StringBuilder();

			do {
				byte[] bytes = new byte[(int) length];

				// Read in the bytes
				int offset = 0;
				int numRead = 0;
				while (offset < bytes.length && (numRead = fis.read(bytes, offset, bytes.length - offset)) >= 0) {
					offset += numRead;
				}

				// Ensure all the bytes have been read in
				if (offset < bytes.length) {
					throw new IOException("Could not completely read file " + localFilePath);
				}

				// Add to the string builder
				sb.append(new String(bytes));
				length = length - offset;

			} while (length > 0);

		} catch (Exception e) {
			throw e;
		} finally {
			if (fis != null)
				fis.close();
		}
		return sb;
	}

}
