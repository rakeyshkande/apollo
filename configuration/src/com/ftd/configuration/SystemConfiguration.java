package com.ftd.configuration;

import java.util.Properties;
import java.io.IOException;
import java.io.FileInputStream;

/**
 * Class which is responsible for keeping of
 */
public class SystemConfiguration {

    /**
     * Property has to be defined as Java system option
     * for example java -Dftd.configuration.file=myconfig.properties
     */
    public static String systemParameterName = "ftd.configuration.file";

    private static SystemConfiguration instance = new SystemConfiguration();
    private Properties properties = new Properties();

    public static SystemConfiguration getInstance() throws IOException {
        instance.loadData();
        return instance;
    }

    private SystemConfiguration() {}

    private void loadData() throws IOException {
        properties.clear();
        properties.load(new FileInputStream(System.getProperty(systemParameterName)));
    }

    public String getProperty(String key) {
        return properties.getProperty(key);
    }

}
