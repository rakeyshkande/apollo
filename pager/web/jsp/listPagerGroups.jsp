<%@ include file="include.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>Pager Groups</title>
    
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    
  </head>
  
  <body>
	<div id="header">
	<ul id="primary">
	  <li><a href="home.do">Home</a></li>
	  <li><a href="listRules.do">Rules</a></li>
	  <li><a href="listPagers.do">Pagers</a></li>
	  <li><span>Pager Groups</span></li>
	</ul>
	</div>
		<div id="main">
		<div id="contents">
		<table class="borderTable" >
		<tr>
			<td>
				<a href="./createPagerGroup.do">Create New Pager Group</a>
			</td>
		</tr>
		<tr>
			<td>&nbsp</td>
		</tr>
		<tr bgcolor="#A0A0A0">
			<td>Name</td>
			<td>&nbsp</td>
		</tr>
		<c:forEach items="${model.pagerGroups}" var="pagerGroup" varStatus="loop">
            <c:set var="rowclass" value="odd" />
            <c:if test="${((loop.index % 2) == 0)}" >
              <c:set var="rowclass" value="even" />
            </c:if>
        
			<tr class="<c:out value='${rowclass}' />">
				<td><a href="./editPagerGroup.do?id=<c:out value="${pagerGroup[0].id}"/>"><c:out value="${pagerGroup[0].name}"/></a></td>
				<c:choose>
				<c:when test="${pagerGroup[1] == 0}">
					<td><a href="./deletePagerGroup.do?id=<c:out value="${pagerGroup[0].id}"/>">Delete</a></td>
				</c:when>
				<c:otherwise>
					<td>In Use</td>
				</c:otherwise>
				</c:choose>
			</tr>
		</c:forEach>
		<tr>
			<td>&nbsp</td>
		</tr>
		<tr>
			<td>
			<c:forEach items="${model.pageNavigation}" var="link">
				<c:choose>
				<c:when test="${link[2] == true}">
					<a href="./listPagerGroups.do?page=<c:out value="${link[1]}"/>"><c:out value="${link[0]}"/></a>&nbsp
				</c:when>
				<c:otherwise>
					<c:out value="${link[0]}"/>&nbsp
				</c:otherwise>
				</c:choose>
			</c:forEach>
			</td>
		</tr>
		</table>
		</div>
	</div>
  </body>
</html>