<%@ include file="include.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>Create Pager Rule</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">

	</head>
	<body>
	<div id="header">
	<ul id="primary">
	  <li><a href="home.do">Home</a></li>
	  <li><span>Rules</span></li>
	  <li><a href="listPagers.do">Pagers</a></li>
	  <li><a href="listPagerGroups.do">Pager Groups</a></li>
	</ul>
	</div>
		<div id="main">
		<div id="contents">
		<%@ include file="ruleForm.jspf" %>
		</div>
		</div>
	</body>
</html>
