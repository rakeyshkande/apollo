<%@ include file="include.jsp"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>Delete Contact</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">

	</head>
	<body>
	<div id="header">
	<ul id="primary">
	  <li><a href="home.do">Home</a></li>
	  <li><span>Rules</span></li>
	  <li><a href="listPagers.do">Pagers</a></li>
	  <li><a href="listPagerGroups.do">Pager Groups</a></li>
	</ul>
	</div>
		<div id="main">
		<div id="contents">
		<FORM name="contactForm" method="POST">
			<INPUT type="hidden" name="_contactIndex" value="<c:out value='${contactIndex}' />"/>
			<TABLE>
				<TR>
					<TD width="20%">
						Priority
					</TD>
					<TD width="80%">
						<INPUT class="" type="text" name="contact.priority" value="<c:out value='${contact.priority}'/>" disabled="disabled"/>
					</TD>
				</TR>
				<TR>
					<TD width="20%">
						Minutes Until Escalation
					</TD>
					<TD width="80%">
						<INPUT class="" type="text" name="contact.escalationMinutes" value="<c:out value='${contact.escalationMinutes}'/>" disabled="disabled"/>
					</TD>
				</TR>
				<tr>
					<td>
						Pager Name
					</td>
					<td>
						<INPUT class="" type="text" name="contact.name" value="<c:out value='${contact.name}'/>" disabled="disabled"/>
					</td>
				</tr>
			</TABLE>
			<spring:hasBindErrors name="ruleCommand">
				<span class="error">Please fix all errors.</span>
			</spring:hasBindErrors>
			<br>
			<INPUT type="hidden" name="_delegateAction" value="cancel">
			<INPUT type="submit" name="_target0" value="Delete" onclick="document.contactForm._delegateAction.value='delete'"/>
			<INPUT type="submit" name="_target0" value="Cancel" />
		</FORM>
		</div>
		</div>
	</body>
</html>
