<%@ include file="include.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>Delete Pager</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">

	</head>
	<body>
	<div id="header">
	<ul id="primary">
	  <li><a href="home.do">Home</a></li>
	  <li><a href="listRules.do">Rules</a></li>
	  <li><span>Pagers</span></li>
	  <li><a href="listPagerGroups.do">Pager Groups</a></li>
	</ul>
	</div>
		<div id="main">
		<div id="contents">
		<FORM method="POST">
			<%@ include file="pagerForm.jspf" %>
			<spring:hasBindErrors name="pagerCommand">
				<span class="error">Please fix all errors.</span>
			</spring:hasBindErrors><br>
			<INPUT type="submit" name="Submit" value="Delete"/>
			<INPUT type="submit" name="Cancel" value="Cancel"/>
		</FORM>
		</div>
		</div>
	</body>
</html>
