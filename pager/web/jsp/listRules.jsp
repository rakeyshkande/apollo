<%@ include file="include.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>Rules</title>
    
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    
	<link rel="stylesheet" type="text/css" href="css/ftd.css">

  </head>
  
  <body>
	<div id="header">
	<ul id="primary">
	  <li><a href="home.do">Home</a></li>
	  <li><span>Rules</span></li>
	  <li><a href="listPagers.do">Pagers</a></li>
	  <li><a href="listPagerGroups.do">Pager Groups</a></li>
	</ul>
	</div>
		<div id="main">
		<div id="contents">
		<table class="borderTable" >
		<tr>
			<td>
				<a href="./createRule.do">Create New Rule</a>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr bgcolor="#A0A0A0">
			<td>Rule Name</td>
			<td>Active</td>
			<td>Priority</td>
			<td>On Hold</td>
			<td>On Hold Until</td>
		</tr>
		<c:forEach items="${model.rules}" var="rule" varStatus="loop">
            <c:set var="rowclass" value="odd" />
            <c:if test="${((loop.index % 2) == 0)}" >
              <c:set var="rowclass" value="even" />
            </c:if>
            
			<tr class="<c:out value='${rowclass}' />" >
				<td><a href="./editRule.do?id=<c:out value='${rule.id}' />"><c:out value="${rule.name}"/></a></td>
				<td>
				<c:if test="${rule.active == true}">
					<IMG src="images/tick.gif" border="0">
				</c:if>
				&nbsp;
				</td>
				<td><c:out value="${rule.salience}" />
				</td>
				<td>
				<c:if test="${rule.onHold == true}">
					<IMG src="images/tick.gif">
				</c:if>
				&nbsp;
				</td>
				<td>
					<fmt:formatDate value="${rule.holdUntil}" type="both"/>
				</td>
			</tr>
		</c:forEach>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>
			<c:forEach items="${model.pageNavigation}" var="link">
				<c:choose>
				<c:when test="${link[2] == true}">
					<a href="./listRules.do?page=<c:out value="${link[1]}"/>"><c:out value="${link[0]}"/></a>&nbsp;
				</c:when>
				<c:otherwise>
					<c:out value="${link[0]}"/>&nbsp;
				</c:otherwise>
				</c:choose>
			</c:forEach>
			</td>
		</tr>
		</table>
		</div>
	</div>
  </body>
</html>
