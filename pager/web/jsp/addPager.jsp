<%@ include file="include.jsp"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>Add Pager</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">

	</head>
	<body>
	<div id="header">
	<ul id="primary">
	  <li><a href="home.do">Home</a></li>
	  <li><a href="listRules.do">Rules</a></li>
	  <li><a href="listPagers.do">Pagers</a></li>
	  <li><span>Pager Groups</span></li>
	</ul>
	</div>
		<div id="main">
		<div id="contents">
		<FORM method="POST" name="pagerForm">
			<TABLE>
				<tr>
					<td>
					<spring:bind path="pagerGroupCommand.newPager">
						<select id="<c:out value='${status.expression}' />" name="<c:out value='${status.expression}' />" style="width: 150px">
							<c:forEach items="${pagers}" var="pagerVar">
								<option value="<c:out value='${pagerVar.id}' />">
									 <c:out value="${pagerVar.name}" />
								</option>
							</c:forEach>
						</select>
						<span class="error"><c:out value="${status.errorMessage}" /></span>
					</spring:bind>
					</td>
				</tr>
			</TABLE>
			<spring:hasBindErrors name="pagerGroupCommand">
				<span class="error">Please fix all errors.</span>
			</spring:hasBindErrors>
			<br>
			<INPUT type="hidden" name="_delegateAction" value="cancel">
			<INPUT type="submit" name="_target0" value="Save" onclick="document.pagerForm._delegateAction.value='save'"/>
			<INPUT type="submit" name="_target0" value="Cancel" />
		</FORM>
		</div>
		</div>
	</body>
</html>
