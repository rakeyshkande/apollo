<%@ include file="include.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>Create Pager Group</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">

	</head>
	<body>
	<div id="header">
	<ul id="primary">
	  <li><a href="home.do">Home</a></li>
	  <li><a href="listRules.do">Rules</a></li>
	  <li><a href="listPagers.do">Pagers</a></li>
	  <li><span>Pager Groups</span></li>
	</ul>
	</div>
		<div id="main">
		<div id="contents">
		<FORM name="pagerGroupForm" method="POST">
			<%@ include file="pagerGroupForm.jspf" %>
			<spring:hasBindErrors name="pagerGroupCommand">
				<span class="error">Please fix all errors.</span>
			</spring:hasBindErrors><br>
			<INPUT type="submit" name="_finish" value="Save"/>
			<INPUT type="submit" name="_cancel" value="Cancel"/>
		</FORM>
		</div>
		</div>
	</body>
</html>
