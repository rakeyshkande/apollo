<%@ include file="include.jsp"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>Remove Pager</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">

	</head>
	<body>
	<div id="header">
	<ul id="primary">
	  <li><a href="home.do">Home</a></li>
	  <li><a href="listRules.do">Rules</a></li>
	  <li><a href="listPagers.do">Pagers</a></li>
	  <li><span>Pager Groups</span></li>
	</ul>
	</div>
		<div id="main">
		<div id="contents">
		<FORM name="pagerForm" method="POST">
			<INPUT type="hidden" name="_pagerIndex" value="<c:out value='${pagerIndex}' />"/>
			<TABLE>
				<tr>
					<td>
						Pager Name
					</td>
					<td>
						<INPUT class="" type="text" name="pager.name" value="<c:out value='${pager.name}'/>" disabled="disabled"/>
					</td>
				</tr>
				<tr>
					<td>
						Pager Email
					</td>
					<td>
						<INPUT class="" type="text" name="pager.email" value="<c:out value='${pager.email}'/>" disabled="disabled"/>
					</td>
				</tr>
			</TABLE>
			<spring:hasBindErrors name="pagerGroupCommand">
				<span class="error">Please fix all errors.</span>
			</spring:hasBindErrors>
			<br>
			<INPUT type="hidden" name="_delegateAction" value="cancel">
			<INPUT type="submit" name="_target0" value="Remove" onclick="document.pagerForm._delegateAction.value='delete'"/>
			<INPUT type="submit" name="_target0" value="Cancel" />
		</FORM>
		</div>
		</div>
	</body>
</html>
