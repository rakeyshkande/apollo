<%@ include file="include.jsp"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>Create Contact</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">

		<script language=javascript>
			function showPager() {
				var divPager = document.getElementById('divPager');
				divPager.style.display = 'block';
				var divPagerGroup = document.getElementById('divPagerGroup');
				divPagerGroup.style.display = 'none';

				var pagerGroup = document.getElementById('newContact.pagerGroup');
				pagerGroup.selectedIndex = 0;
			}
			
			function showPagerGroup() {
				var divPager = document.getElementById('divPager');
				divPager.style.display = 'none';
				var divPagerGroup = document.getElementById('divPagerGroup');
				divPagerGroup.style.display = 'block';

				var pager = document.getElementById('newContact.pager');
				pager.selectedIndex = 0;
			}
		</script>
	</head>
	<body>
	<div id="header">
	<ul id="primary">
	  <li><a href="home.do">Home</a></li>
	  <li><span>Rules</span></li>
	  <li><a href="listPagers.do">Pagers</a></li>
	  <li><a href="listPagerGroups.do">Pager Groups</a></li>
	</ul>
	</div>
		<div id="main">
		<div id="contents">
		<FORM method="POST" name="contactForm">
			<TABLE>
				<TR>
					<TD width="20%">
						Priority
					</TD>
					<TD width="80%">
						<spring:bind path="ruleCommand.newContact.priority">
							<INPUT class="" type="text" name="<c:out value='${status.expression}'/>" value="<c:out value='${status.value}'/>"/>
							<c:if test="${status.error}">
								<span class="error"><c:out value="${status.errorMessage}" /></span>
							</c:if>
						</spring:bind>
					</TD>
				</TR>
				<TR>
					<TD width="20%">
						Minutes Until Escalation
					</TD>
					<TD width="80%">
						<spring:bind path="ruleCommand.newContact.escalationMinutes">
							<INPUT class="" type="text" name="<c:out value='${status.expression}'/>" value="<c:out value='${status.value}'/>"/>
							<c:if test="${status.error}">
								<span class="error"><c:out value="${status.errorMessage}" /></span>
							</c:if>
						</spring:bind>
					</TD>
				</TR>
				<tr>
					<td>
						<table>
						<spring:bind path="ruleCommand.pagerType">
						<tr><td><input type="radio" name="<c:out value='${status.expression}'/>" value="pager" checked onclick="showPager()">Pager</td></tr>
						<tr><td><input type="radio" name="<c:out value='${status.expression}'/>" value="pagerGroup" onclick="showPagerGroup()">Pager Group</td></tr>
						</spring:bind>
						</table>
					</td>
					<td>
					<table>
					<tr>
						<td>
						<spring:bind path="ruleCommand.newContact.pager">
							<div id="divPager">
							<select id="<c:out value='${status.expression}' />" name="<c:out value='${status.expression}' />" style="width: 150px">
								<option selected value="none" ></option>
								<c:forEach items="${pagers}" var="pagerVar">
									<option value="<c:out value='${pagerVar.id}' />">
										 <c:out value='${pagerVar.name}' />
									</option>
								</c:forEach>
							</select>
							</div>
							<span class="error"><c:out value='${status.errorMessage}' /></span>
						</spring:bind>
						<spring:bind path="ruleCommand.newContact.pagerGroup">
							<div id="divPagerGroup" style="display: none">
							<select id="<c:out value='${status.expression}' />" name="<c:out value='${status.expression}' />" style="width: 150px">
								<option selected value="none" ></option>
								<c:forEach items="${pagerGroups}" var="pagerGroupVar">
									<option value="<c:out value='${pagerGroupVar.id}' />">
										 <c:out value='${pagerGroupVar.name}' />
									</option>
								</c:forEach>
							</select>
							</div>
							<span class="error"><c:out value='${status.errorMessage}' /></span>
						</spring:bind>
						</td>
					</tr>
					</table>
					</td>
				<tr>
			</TABLE>
			<spring:hasBindErrors name="ruleCommand">
				<span class="error">Please fix all errors.</span>
			</spring:hasBindErrors>
			<br>
			<INPUT type="hidden" name="_delegateAction" value="cancel">
			<INPUT type="submit" name="_target0" value="Save" onclick="document.contactForm._delegateAction.value='save'"/>
			<INPUT type="submit" name="_target0" value="Cancel" />
		</FORM>
		</div>
		</div>
	</body>
</html>
