<%@ include file="include.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>Pagers</title>
    
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    
  </head>
  
  <body>
	<div id="header">
	<ul id="primary">
	  <li><a href="home.do">Home</a></li>
	  <li><a href="listRules.do">Rules</a></li>
	  <li><span>Pagers</span></li>
	  <li><a href="listPagerGroups.do">Pager Groups</a></li>
	</ul>
	</div>
		<div id="main">
		<div id="contents">
		<table class="borderTable" >
		<tr>
			<td>
				<a href="./createPager.do">Create New Pager</a>
			</td>
		</tr>
		<tr>
			<td>&nbsp</td>
		</tr>
		<tr bgcolor="#A0A0A0">
			<td>Name</td>
			<td>Email Address</td>
			<td>&nbsp</td>
		</tr>
		<c:forEach items="${model.pagers}" var="pager" varStatus="loop">
            <c:set var="rowclass" value="odd" />
            <c:if test="${((loop.index % 2) == 0)}" >
              <c:set var="rowclass" value="even" />
            </c:if>
			<tr class="<c:out value='${rowclass}' />">
				<td><a href="./editPager.do?id=<c:out value="${pager[0].id}"/>"><c:out value="${pager[0].name}"/></a></td>
				<td>
					<c:out value="${pager[0].email}" />
				</td>
				<c:choose>
					<c:when test="${pager[1] == 0 && pager[2] == 0}">
						<td><a href="./deletePager.do?id=<c:out value="${pager[0].id}"/>">Delete</a></td>
					</c:when>
					<c:otherwise>
						<td>In Use</td>
					</c:otherwise>
				</c:choose>
			</tr>
		</c:forEach>
		<tr>
			<td>&nbsp</td>
		</tr>
		<tr>
			<td>
			<c:forEach items="${model.pageNavigation}" var="link">
				<c:choose>
				<c:when test="${link[2] == true}">
					<a href="./listPagers.do?page=<c:out value="${link[1]}"/>"><c:out value="${link[0]}"/></a>&nbsp
				</c:when>
				<c:otherwise>
					<c:out value="${link[0]}"/>&nbsp
				</c:otherwise>
				</c:choose>
			</c:forEach>
			</td>
		</tr>
		</table>
		</div>
	</div>
  </body>
</html>
