package com.ftd.mail;

import java.util.Map;
import java.util.Properties;

import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class POP3Receiver implements MailReceiver {

    private static final Log log = LogFactory.getLog(POP3Receiver.class);

    private String host;
    private String user;
    private String password;

    public POP3Receiver()
    {
        Pop3Config config = Pop3Config.getInstance();
        host = config.getHost();
        user = config.getUser();
        password = config.getPassword();
    }
    /*
     * (non-Javadoc)
     * 
     * @see com.ftd.mail.MailReceiver#getMessages()
     */
    public void loadMessageSubjects(Map result) {

        Folder folder = null;
        Store store = null;

        try {
            store = openStore();
            folder = openInboxFolder(store);
            try {
                Message[] messages = folder.getMessages();
                for (int i = 0; i < messages.length; i++) {
                    Message message = messages[i];
                    try {
                        result.put(new Long(message.getMessageNumber()),
                                message.getSubject());
                    } catch (MessagingException e) {
                        log.error("Exception reading subject of message.", e);
                        throw new RuntimeException(
                                "Exception reading subject of message.", e);
                    }
                }

            } catch (MessagingException e) {
                log.error("Exception getting mail messages.", e);
                throw new RuntimeException("Exception getting mail messages", e);
            }
        } finally {
            try {
                if (folder != null)
                    folder.close(false);
                if (store != null)
                    store.close();
            } catch (Exception e) {
                log.info("Exception closing pop3 service.", e);
            }
        }
    }

    public void markMessageRead(Long mailId) {
        Folder folder = null;
        Store store = null;

        try {
            store = openStore();
            folder = openInboxFolder(store);
            try {
                Message message = folder.getMessage(mailId.intValue());
                message.setFlag(Flags.Flag.DELETED, true);
            } catch (MessagingException e) {
                log.error("Exception getting mail message.", e);
                throw new RuntimeException("Exception getting mail message", e);
            }
        } finally {
            try {
                if (folder != null)
                    // true tells server to expunge the folder
                    folder.close(true);
                if (store != null)
                    store.close();
            } catch (Exception e) {
                log.info("Exception closing pop3 service.", e);
            }
        }
    }
    
    /**
     * @return
     */
    private Folder openInboxFolder(Store store) {
        Folder folder = null;

        try {
            folder = store.getDefaultFolder();
            if (folder == null) {
                log.error("No default folder");
                throw new RuntimeException("No default folder");
            }
        } catch (MessagingException e) {
            log.error("Exception getting pop3 default folder.", e);
            throw new RuntimeException(
                    "Exception getting pop3 default folder.", e);
        }

        try {
            folder = folder.getFolder("INBOX");
            if (folder == null) {
                log.error("No INBOX folder");
                throw new RuntimeException("No INBOX folder");
            }
        } catch (MessagingException e) {
            log.error("Exception getting pop3 INBOX folder.", e);
            throw new RuntimeException("Exception getting pop3 INBOX folder.",
                    e);
        }

        try {
            folder.open(Folder.READ_WRITE);
        } catch (MessagingException e) {
            log.error("Exception opening pop3 INBOX folder.", e);
            throw new RuntimeException("Exception opening pop3 INBOX folder.",
                    e);
        }
        return folder;
    }

    /**
     * @param store
     * @return
     */
    private Store openStore() {
        
        Store store = null;
        
        Properties props = System.getProperties();
        Session session = Session.getDefaultInstance(props, null);

        if(log.isDebugEnabled()) {
            session.setDebug(true);
        }
        
        try {
            store = session.getStore("pop3");
        } catch (NoSuchProviderException e) {
            log.error("No pop3 provider configured.", e);
            throw new RuntimeException("No pop3 provider configured.", e);
        }

        try {
            store.connect(host, user, password);
        } catch (MessagingException e) {
            log.error("Exception connecting to pop3 service.", e);
            throw new RuntimeException("Exception connecting to pop3 service.",
                    e);
        }
        return store;
    }

}
