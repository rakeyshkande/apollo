package com.ftd.mail;

import com.ftd.osp.utilities.ConfigurationUtil;

public class Pop3Config
{
    private static Pop3Config instance = null;
    private final static String CONTEXT = "PAGER_CONFIG";
    
    private String host;
    private String user;
    private String password;

    private Pop3Config()
    {
    }
    
    public static Pop3Config getInstance()
    {
        if (instance == null)
        {
            Pop3Config newInstance = new Pop3Config();
            newInstance.init();
            instance = newInstance;
        }
        return instance;
    }
    
    private void init()
    {
        try
        {
            ConfigurationUtil cu = ConfigurationUtil.getInstance();
            host = cu.getFrpGlobalParm(CONTEXT,"pop3_host");
            user = cu.getFrpGlobalParm(CONTEXT,"pop3_user");
            password = cu.getFrpGlobalParm(CONTEXT,"pop3_password");
            
        }
        catch (Exception e)
        {
            e.printStackTrace();
            host="localhost";
            user="blah";
            password="blah";
        }
    }


    public String getHost()
    {
        return host;
    }

    public String getUser()
    {
        return user;
    }

    public String getPassword()
    {
        return password;
    }
}
