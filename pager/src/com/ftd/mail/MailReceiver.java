package com.ftd.mail;

import java.util.Map;

public interface MailReceiver {

    public void loadMessageSubjects(Map result);

    public void markMessageRead(Long mailId);

}