package com.ftd.web;

import java.sql.Connection;

import java.util.Collection;
import java.util.LinkedList;

import javax.servlet.http.HttpServletRequest;

public class Navigation
{

    private Pagination pagination;
    private int page = 1;
    private int pageSize = 1;
    private int pageCount = 1;

    public void setPagination(Pagination pagination)
    {
        this.pagination = pagination;
    }

    public void setPageSize(int pageSize)
    {
        this.pageSize = pageSize;
    }

    public void handleRequest(HttpServletRequest request, Connection con) throws Exception
    {
        final String path = pagination.getClass().getName() + ".pageCount";

        String pageParam = (String)request.getParameter("page");
        Integer pageCountAttribute = (Integer)request.getSession().getAttribute(path);
        if (pageParam == null || pageCountAttribute == null)
        {
            page = 1;
            int count = pagination.getRecordCount(con);
            pageCount = (count / pageSize) + (count % pageSize > 0 ? 1 : 0);
            request.getSession().setAttribute(path, new Integer(pageCount));
        } else
        {
            page = Integer.valueOf(pageParam).intValue();
            pageCount = pageCountAttribute.intValue();
        }
    }

    public Collection getPageNavigation() throws Exception
    {
        LinkedList pageNavigation = new LinkedList();
        if (pageCount > 1)
        {
            for (int i = 1; i <= pageCount; i++)
            {
                pageNavigation.addLast(new Object[]
                        { String.valueOf(i), new Integer(i), new Boolean(i != page) });
            }
            if (page == 1 && pageCount > 1)
            {
                pageNavigation.addFirst(new Object[]
                        { new String("Prev"), new Integer(page - 1), Boolean.FALSE });
                pageNavigation.addLast(new Object[]
                        { new String("Next"), new Integer(page + 1), Boolean.TRUE });
            } else if (page > 1 && page < pageCount)
            {
                pageNavigation.addFirst(new Object[]
                        { new String("Prev"), new Integer(page - 1), Boolean.TRUE });
                pageNavigation.addLast(new Object[]
                        { new String("Next"), new Integer(page + 1), Boolean.TRUE });
            } else if (page > 1 && page == pageCount)
            {
                pageNavigation.addFirst(new Object[]
                        { new String("Prev"), new Integer(page - 1), Boolean.TRUE });
                pageNavigation.addLast(new Object[]
                        { new String("Next"), new Integer(page + 1), Boolean.FALSE });
            }
        }
        return pageNavigation;
    }

    public Collection getCurrentPageResults(Connection con) throws Exception
    {
        return pagination.findByPage(con, page - 1, pageSize);
    }

}
