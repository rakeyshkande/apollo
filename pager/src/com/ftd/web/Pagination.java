package com.ftd.web;

import java.sql.Connection;

import java.util.Collection;

public interface Pagination 
{
    public int getRecordCount(Connection con) throws Exception;
    public Collection findByPage(Connection con, int page, int size) throws Exception;
}
