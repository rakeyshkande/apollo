package com.ftd.pager.util;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;

import java.sql.Connection;

public class ConnectionHelper
{
    private final static String PROPERTY_FILE   = "pager_config.xml";
    private final static String DATASOURCE_NAME = "DATASOURCE_NAME";
    
    public ConnectionHelper()
    {
    }
    
    public static Connection getConnection() throws Exception
    {
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        String datasource = configUtil.getProperty(PROPERTY_FILE, DATASOURCE_NAME);
        Connection con = DataSourceUtil.getInstance().getConnection(datasource);
        return con;
    }
}
