package com.ftd.pager.service.impl;

import com.ftd.pager.model.SystemMessageVO;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ftd.pager.dao.SystemMessageDAO;
import com.ftd.pager.model.ContactVO;
import com.ftd.pager.model.RuleVO;
import com.ftd.pager.service.SystemMessageService;

import java.sql.Connection;

public class SystemMessageServiceImpl implements SystemMessageService 
{

    private static final Log log = LogFactory.getLog(SystemMessageServiceImpl.class);
    
    public List findUnreadMessages(Connection con) throws Exception
    {
        SystemMessageDAO systemMessageDAO = new SystemMessageDAO(con);
        return systemMessageDAO.findUnreadMessages();
    }

    public SystemMessageVO findUnreadMessagesById(Connection con, Integer id) throws Exception
    {
        SystemMessageDAO systemMessageDAO = new SystemMessageDAO(con);
        return systemMessageDAO.findUnreadMessageById(id);
    }
    
    public void updateMessageReadById(Connection con, Long id) throws Exception
    {
        SystemMessageDAO systemMessageDAO = new SystemMessageDAO(con);
        systemMessageDAO.updateMessageReadById(id);
    }

    public void processEscalation(Connection con, RuleVO rule, SystemMessageVO systemMessage) throws Exception
    {
        SystemMessageDAO systemMessageDAO = new SystemMessageDAO(con);
        Long id = (Long) systemMessage.getSystemMessageId();
        Integer level = (Integer) systemMessage.getEscalationLevel();

        ContactVO contact = null;
        if(level == null) 
        {
            contact = rule.getContactByFirstPriority();
        }
        else 
        {
          contact = rule.getContactByPriority(level);
        }

        if( contact != null ) 
        {
        
            ContactVO nextContact = rule.getNextContact(contact);
            // if this can be escalated...
            if( nextContact != null) 
            {
                systemMessageDAO.updateMessageEscalationById(id, nextContact.getPriority(), nextContact.getNextEscalationTime());
            }
            else 
            {
                systemMessageDAO.updateMessageReadById(id);
            }
        }
        else 
        {
            log.error("No contact at level " + level + " for rule " + rule.getId());
            throw new RuntimeException("No contact at level " + level + " for rule " + rule.getId());
        }
    }
}
