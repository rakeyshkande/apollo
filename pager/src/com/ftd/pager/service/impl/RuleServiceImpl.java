package com.ftd.pager.service.impl;

import com.ftd.pager.dao.ContactDAO;

import java.util.Collection;
import java.util.List;

import com.ftd.pager.dao.RuleDAO;
import com.ftd.pager.model.ContactVO;
import com.ftd.pager.model.RuleVO;
import com.ftd.pager.service.RuleService;

import java.sql.Connection;

public class RuleServiceImpl implements RuleService
{
    public Collection findByPage(Connection con, int page, int size) throws Exception
    {
        RuleDAO ruleDAO = new RuleDAO(con);
        return ruleDAO.findByPage(page, size);
    }

    public RuleVO findById(Connection con, Long i) throws Exception
    {
        RuleDAO ruleDAO = new RuleDAO(con);
        return ruleDAO.findById(i);
    }

    public void save(Connection con, RuleVO rule) throws Exception
    {
        RuleDAO ruleDAO = new RuleDAO(con);
        ruleDAO.save(rule);
    }

    public RuleVO findByIdInitContacts(Connection con, Long id) throws Exception
    {
        RuleDAO ruleDAO = new RuleDAO(con);
        return ruleDAO.findByIdInitContacts(id);
    }

    public List findExpiredOnHold(Connection con) throws Exception
    {
        RuleDAO ruleDAO = new RuleDAO(con);
        return ruleDAO.findExpiredOnHold();
    }

    public List findActive(Connection con) throws Exception
    {
        RuleDAO ruleDAO = new RuleDAO(con);
        return ruleDAO.findActive();
    }

    public int getRecordCount(Connection con) throws Exception
    {
        RuleDAO ruleDAO = new RuleDAO(con);
        return ruleDAO.getCount();
    }

    public void removeContact(Connection con, RuleVO rule, ContactVO contact) throws Exception
    {
        ContactDAO contactDAO = new ContactDAO(con);
        contactDAO.delete(contact);
        rule.removeContact(contact);
    }

    public void addContact(Connection con, ContactVO contact) throws Exception
    {
        ContactDAO contactDAO = new ContactDAO(con);
        contactDAO.save(contact);
    }
    
}
