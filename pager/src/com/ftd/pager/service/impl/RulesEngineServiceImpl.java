package com.ftd.pager.service.impl;

import com.ftd.pager.model.SystemMessageVO;
import com.ftd.pager.util.ConnectionHelper;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.drools.FactException;
import org.drools.IntegrationException;
import org.drools.RuleBase;
import org.drools.WorkingMemory;
import org.drools.conflict.SalienceConflictResolver;
import org.drools.event.DebugWorkingMemoryEventListener;
import org.drools.io.RuleBaseLoader;
import org.drools.io.RuleSetLoader;

import org.xml.sax.SAXException;

import com.ftd.pager.model.RuleVO;
import com.ftd.pager.service.RuleService;
import com.ftd.pager.service.RulesEngineService;
import com.ftd.pager.service.SystemMessageMailService;
import com.ftd.pager.service.SystemMessageService;

import java.sql.Connection;

import org.drools.event.ActivationCancelledEvent;
import org.drools.event.ActivationCreatedEvent;
import org.drools.event.ActivationFiredEvent;
import org.drools.event.ConditionTestedEvent;
import org.drools.event.ObjectAssertedEvent;
import org.drools.event.ObjectModifiedEvent;
import org.drools.event.ObjectRetractedEvent;
import org.drools.event.WorkingMemoryEventListener;

public class RulesEngineServiceImpl implements RulesEngineService
{
    private static final Log log = LogFactory.getLog(RulesEngineServiceImpl.class);

    private RuleBase ruleBase;
    private boolean noRules;

    private RuleService ruleService;
    private SystemMessageService systemMessageService;
    private SystemMessageMailService mailService;

    public void setSystemMessageService(SystemMessageService systemMessageService)
    {
        this.systemMessageService = systemMessageService;
    }

    public void setRuleService(RuleService ruleService)
    {
        this.ruleService = ruleService;
    }

    public void setMailService(SystemMessageMailService mailService)
    {
        this.mailService = mailService;
    }

    public synchronized void init() throws IntegrationException, SAXException, 
                                           IOException, Exception
    {
        RuleBaseLoader loader = new RuleBaseLoader(SalienceConflictResolver.getInstance());
        RuleSetLoader setLoader = new RuleSetLoader();
        List rules = new ArrayList();
        Connection con = null;
        try
        {
            con = ConnectionHelper.getConnection();
            rules = ruleService.findActive(con);
        }
        finally
        {
            try
            {
                if (con != null)
                {
                    con.close();
                }
                
            }
            catch (Exception e)
            {
                log.error("Error closing connection",e);
            }
        }

        if (rules.size() > 0)
        {
            noRules = false;
        }
        else
        {
            log.error("There are no rules for the rules engine to run!");
            noRules = true;
        }
        
        for (Iterator it = rules.iterator(); it.hasNext(); )
        {
            RuleVO rule = (RuleVO)it.next();
            StringReader reader = new StringReader(getRuleBody(rule));
            setLoader.addFromReader(reader);
        }
        loader.addFromRuleSetLoader(setLoader);
        ruleBase = loader.buildRuleBase();
    }

    public boolean validateRule(RuleVO rule)
    {
        boolean rc = false;
        try
        {
            RuleBaseLoader loader = new RuleBaseLoader();
            RuleSetLoader setLoader = new RuleSetLoader();

            StringReader reader = new StringReader(getRuleBody(rule));
            setLoader.addFromReader(reader);
            loader.addFromRuleSetLoader(setLoader);
            loader.buildRuleBase();
            rc = true;
        } catch (Exception e)
        {
            log.debug("Invalid rule: " + rule, e);
        }
        return rc;
    }

    private String getRuleBody(RuleVO rule) throws IOException
    {
        InputStream in = 
            getClass().getResource("/template.java.drl.xml").openStream();
        try
        {
            String body = IOUtils.toString(in);

            String id = rule.getId() != null ? rule.getId().toString() : "0";
            body = StringUtils.replace(body, "%%RULE_ID%%", StringEscapeUtils.escapeXml(id));
            body = StringUtils.replace(body, "%%RULE_NAME%%", StringEscapeUtils.escapeXml(rule.getName()));
            body = StringUtils.replace(body, "%%CONDITION%%", StringEscapeUtils.escapeXml(rule.getCondition()));
            body = StringUtils.replace(body, "%%CONSEQUENCE%%", StringEscapeUtils.escapeXml(rule.getConsequence()));
            body = StringUtils.replace(body, "%%SALIENCE%%", StringEscapeUtils.escapeXml(String.valueOf(rule.getSalience())));

            return body;
        } finally
        {
            if (in != null)
                IOUtils.closeQuietly(in);
        }
    }

    public synchronized void processSystemMessage(Connection con, SystemMessageVO systemMessage)
    {
        if (!noRules)
        {
            WorkingMemory workingMemory = ruleBase.newWorkingMemory();
            if (log.isDebugEnabled())
            {
                workingMemory.addEventListener(new FTDDebugWorkingMemoryEventListener());
            }
            try
            {
                workingMemory.setApplicationData("log", log);
                workingMemory.setApplicationData("ruleService", ruleService);
                workingMemory.setApplicationData("rulesEngineService", this);
                workingMemory.setApplicationData("systemMessageService", systemMessageService);
                workingMemory.setApplicationData("mailService", mailService);
                workingMemory.setApplicationData("con", con);
                workingMemory.assertObject(systemMessage);
                workingMemory.fireAllRules();
            } catch (FactException e)
            {
                log.error("Error processing rules for message id: " + systemMessage.getSystemMessageId(), e);
                throw new RuntimeException(e);
            }
        } else
        {
            // No rules to run!!    
        }
    }


    private class FTDDebugWorkingMemoryEventListener implements WorkingMemoryEventListener  
    {  
      public FTDDebugWorkingMemoryEventListener()  
      {  
          // intentionally left blank  
      }  
   
      public void objectAsserted(ObjectAssertedEvent event)  
      {  
          log.debug( event );  
      }  
      public void objectModified(ObjectModifiedEvent event)  
      {  
          log.debug( event );  
      }  
   
      public void objectRetracted(ObjectRetractedEvent event)  
      {  
          log.debug( event );  
      }  
   
      public void conditionTested(ConditionTestedEvent event)  
      {  
          log.debug( event );  
      }  
   
      public void activationCreated(ActivationCreatedEvent event)  
      {  
          log.debug( event );  
      }  
   
      public void activationCancelled(ActivationCancelledEvent event)  
      {  
          log.debug( event );  
      }  
   
      public void activationFired(ActivationFiredEvent event)  
      {  
          log.debug( event );  
      }  
    }  
    
}
