package com.ftd.pager.service.impl;

import com.ftd.pager.model.SystemMessageVO;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;

import com.ftd.mail.MailReceiver;
import com.ftd.pager.model.ContactVO;
import com.ftd.pager.model.PagerVO;
import com.ftd.pager.model.RuleVO;
import com.ftd.pager.service.SystemMessageMailService;
import com.ftd.spring.SystemMessageTemplate;

public class SystemMessageMailServiceImpl implements SystemMessageMailService
{

    private static final Log log = LogFactory.getLog(SystemMessageMailServiceImpl.class);

    private MailSender mailSender;
    private MailReceiver mailReceiver;
    private SystemMessageTemplate messageTemplate;

    public void setMailSender(MailSender mailSender)
    {
        this.mailSender = mailSender;
    }

    public void setMailReceiver(MailReceiver mailReceiver)
    {
        this.mailReceiver = mailReceiver;
    }

    public void setMessageTemplate(SystemMessageTemplate messageTemplate)
    {
        this.messageTemplate = messageTemplate;
    }

    public void sendEmail(RuleVO rule, SystemMessageVO systemMessage)
    {
        Integer level = systemMessage.getEscalationLevel();
        ContactVO contact = null;
        
        if (level == null)
        {
            contact = rule.getContactByFirstPriority();
        } else
        {
            contact = rule.getContactByPriority(level);
        }

        if (contact != null)
        {

            ContactVO nextContact = rule.getNextContact(contact);
            if (contact.getPager() != null)
            {
                sendEmail(rule, systemMessage, contact.getPager(), nextContact);
            } else if (contact.getPagerGroup() != null && contact.getPagerGroup().getPagers() != null)
            {
                for (Iterator pagers = contact.getPagerGroup().getPagers().iterator(); pagers.hasNext(); )
                {
                    PagerVO pager = (PagerVO)pagers.next();
                    sendEmail(rule, systemMessage, pager, nextContact);
                }
            }
        } else
        {
            log.error("No contact at level " + level + " for rule " + rule.getId());
            throw new RuntimeException("No contact at level " + level + " for rule " + rule.getId());
        }
    }

    /**
     * @param rule
     * @param systemMessage
     * @param pager
     */
    private void sendEmail(RuleVO rule, SystemMessageVO systemMessage, PagerVO pager, ContactVO nextContact)
    {
        log.info("Sending email for " + rule.getName() + " " + systemMessage.getSystemMessageId() + " " + pager.getName());

        String nextContactName = nextContact != null ? nextContact.getName() : "";
        String escalationTime = nextContact != null ? nextContact.getNextEscalationTime().toString() : "";

        try
        {
            SystemMessageTemplate msg = 
                new SystemMessageTemplate(messageTemplate, pager, nextContactName, escalationTime, systemMessage);
            log.info("Sending email for rule: " + rule.getId() + " and message: " + systemMessage.getSystemMessageId());
            mailSender.send(msg);
        } catch (MailException e)
        {
            log.error("Failed to send email for rule: " + rule.getId() + " and message: " + 
                      systemMessage.getSystemMessageId(), e);
            // throw as a runtime exception to rollback any tx
            throw new RuntimeException("Failed to send email for rule: " + rule.getId() + " and message: " + 
                                       systemMessage.getSystemMessageId(), e);
        }
    }

    public Map findUnreadMessages()
    {
        Map result = new HashMap();

        try
        {
            mailReceiver.loadMessageSubjects(result);
        }
        catch (Exception e)
        {
            // Tons of exceptions in development, so reducing the spam of this
            log.error("Failed to properly connect to email " + e.getMessage());
            result = new HashMap();
        }

        // filter for only the system messsage replies
        for (Iterator it = result.keySet().iterator(); it.hasNext(); )
        {
            Long id = (Long)it.next();
            String subject = (String)result.get(id);
            int index = StringUtils.lastIndexOf(subject, messageTemplate.getSubject());
            if (index > 0)
            {
                // Just take the system message id
                String systemMessageId = StringUtils.substring(subject, index + messageTemplate.getSubject().length() + 1);
                result.put(id, new Long(systemMessageId));
            } else
            {
                it.remove();
            }
        }

        return result;
    }

    public void markMessageRead(Long mailId)
    {
        mailReceiver.markMessageRead(mailId);
    }

}
