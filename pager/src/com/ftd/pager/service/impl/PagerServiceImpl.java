package com.ftd.pager.service.impl;

import java.util.Collection;

import com.ftd.pager.dao.PagerDAO;
import com.ftd.pager.model.PagerVO;
import com.ftd.pager.service.PagerService;

import java.sql.Connection;

public class PagerServiceImpl implements PagerService
{
    public Collection findByPage(Connection con, int page, int size) throws Exception
    {
        PagerDAO pagerDAO = new PagerDAO(con);
        return pagerDAO.findByPageWithContactAndGroupCount(page, size);
    }

    public PagerVO findById(Connection con, Long i) throws Exception
    {
        PagerDAO pagerDAO = new PagerDAO(con);
        return pagerDAO.findById(i);
    }

    public void save(Connection con, PagerVO pager) throws Exception
    {
        PagerDAO pagerDAO = new PagerDAO(con);
        pagerDAO.save(pager);
    }

    public void delete(Connection con, PagerVO pager) throws Exception
    {
        PagerDAO pagerDAO = new PagerDAO(con);
        pagerDAO.delete(pager);
    }

    public Collection findAll(Connection con) throws Exception
    {
        PagerDAO pagerDAO = new PagerDAO(con);
        return pagerDAO.findAll();
    }

    public int getRecordCount(Connection con) throws Exception
    {
        PagerDAO pagerDAO = new PagerDAO(con);
        return pagerDAO.getCount();
    }
}
