package com.ftd.pager.service.impl;

import java.util.Collection;

import com.ftd.pager.dao.PagerGroupDAO;
import com.ftd.pager.model.PagerGroupVO;
import com.ftd.pager.model.PagerVO;
import com.ftd.pager.service.PagerGroupService;

import java.sql.Connection;

public class PagerGroupServiceImpl implements PagerGroupService
{
    public Collection findByPage(Connection con, int page, int size) throws Exception
    {
        PagerGroupDAO pagerGroupDAO = new PagerGroupDAO(con);
        return pagerGroupDAO.findByPageWithContactCount(page, size);
    }

    public PagerGroupVO findById(Connection con, Long i) throws Exception
    {
        PagerGroupDAO pagerGroupDAO = new PagerGroupDAO(con);
        return pagerGroupDAO.findById(i);
    }

    public void save(Connection con, PagerGroupVO pagerGroup) throws Exception
    {
        PagerGroupDAO pagerGroupDAO = new PagerGroupDAO(con);
        pagerGroupDAO.save(pagerGroup);
    }

    public void delete(Connection con, PagerGroupVO pagerGroup) throws Exception
    {
        PagerGroupDAO pagerGroupDAO = new PagerGroupDAO(con);
        pagerGroupDAO.delete(pagerGroup);
    }

    public Collection findAll(Connection con) throws Exception
    {
        PagerGroupDAO pagerGroupDAO = new PagerGroupDAO(con);
        return pagerGroupDAO.findAll();
    }

    public PagerGroupVO findByIdInitPagers(Connection con, Long i) throws Exception
    {
        PagerGroupDAO pagerGroupDAO = new PagerGroupDAO(con);
        return pagerGroupDAO.findByIdInitPagers(i);
    }

    public int getRecordCount(Connection con) throws Exception
    {
        PagerGroupDAO pagerGroupDAO = new PagerGroupDAO(con);
        return pagerGroupDAO.getCount();
    }

    public void addPagerToGroup(Connection con, PagerGroupVO pagerGroup, PagerVO pager) 
            throws Exception
    {
        PagerGroupDAO pagerGroupDAO = new PagerGroupDAO(con);
        pagerGroupDAO.addPager(pagerGroup,pager);
    }

    public void removePagerFromGroup(Connection con, PagerGroupVO pagerGroup, PagerVO pager) 
            throws Exception
    {
        PagerGroupDAO pagerGroupDAO = new PagerGroupDAO(con);
        pagerGroupDAO.removePager(pagerGroup,pager);
    }
}
