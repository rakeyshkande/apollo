package com.ftd.pager.service;

import com.ftd.pager.model.SystemMessageVO;
import java.io.IOException;
import java.util.Map;

import org.drools.IntegrationException;
import org.xml.sax.SAXException;

import com.ftd.pager.model.RuleVO;

import java.sql.Connection;

public interface RulesEngineService 
{
    public void init() throws IntegrationException, SAXException, IOException, Exception;
    public boolean validateRule(RuleVO rule) throws Exception;    
    public void processSystemMessage(Connection con,SystemMessageVO systemMessage) throws Exception;
}
