package com.ftd.pager.service;

import java.util.Collection;

import com.ftd.pager.model.PagerGroupVO;
import com.ftd.pager.model.PagerVO;
import com.ftd.web.Pagination;

import java.sql.Connection;

public interface PagerGroupService extends Pagination 
{
    public PagerGroupVO findById(Connection con,Long i) throws Exception;
    public void save(Connection con,PagerGroupVO pagerGroup) throws Exception;    
    public void delete(Connection con,PagerGroupVO pagerGroup) throws Exception;
    public Collection findAll(Connection con) throws Exception;
    public PagerGroupVO findByIdInitPagers(Connection con,Long i) throws Exception;
    public void addPagerToGroup(Connection con,PagerGroupVO pagerGroup, PagerVO pager) throws Exception;    
    public void removePagerFromGroup(Connection con,PagerGroupVO pagerGroup, PagerVO pager) throws Exception;    
}
 
