package com.ftd.pager.service;

import com.ftd.pager.model.SystemMessageVO;
import java.util.Map;

import com.ftd.pager.model.RuleVO;

public interface SystemMessageMailService {
    public void sendEmail(RuleVO rule, SystemMessageVO systemMessage);
    public Map findUnreadMessages();
    public void markMessageRead(Long mailId);
}
