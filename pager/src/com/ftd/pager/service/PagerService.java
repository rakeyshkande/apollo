package com.ftd.pager.service;

import java.util.Collection;

import com.ftd.pager.model.PagerVO;
import com.ftd.web.Pagination;

import java.sql.Connection;

public interface PagerService extends Pagination{
    public PagerVO findById(Connection con,Long i) throws Exception;
    public void save(Connection con,PagerVO pager) throws Exception;    
    public void delete(Connection con,PagerVO pager) throws Exception;
    public Collection findAll(Connection con) throws Exception;
}
