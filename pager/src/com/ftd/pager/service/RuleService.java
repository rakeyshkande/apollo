package com.ftd.pager.service;

import com.ftd.pager.model.ContactVO;

import java.util.List;

import com.ftd.pager.model.RuleVO;
import com.ftd.web.Pagination;

import java.sql.Connection;

public interface RuleService extends Pagination {
    public RuleVO findById(Connection con, Long i) throws Exception;
    public void save(Connection con, RuleVO rule) throws Exception;
    public RuleVO findByIdInitContacts(Connection con, Long id) throws Exception;
    public List findExpiredOnHold(Connection con) throws Exception;
    public List findActive(Connection con) throws Exception;
    public void removeContact(Connection con, RuleVO rule, ContactVO contact) throws Exception;
    public void addContact(Connection con, ContactVO contact) throws Exception;
}
