package com.ftd.pager.service;

import com.ftd.pager.model.SystemMessageVO;
import java.util.List;
import java.util.Map;

import com.ftd.pager.model.RuleVO;

import java.sql.Connection;

public interface SystemMessageService 
{
    public List findUnreadMessages(Connection con) throws Exception;
    public SystemMessageVO findUnreadMessagesById(Connection con, Integer id) throws Exception;
    public void updateMessageReadById(Connection con, Long id) throws Exception;
    public void processEscalation(Connection con, RuleVO rule, SystemMessageVO systemMessage) throws Exception;
}
