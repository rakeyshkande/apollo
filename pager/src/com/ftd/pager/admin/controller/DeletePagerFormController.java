package com.ftd.pager.admin.controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.ftd.pager.admin.command.PagerCommand;
import com.ftd.pager.model.PagerVO;
import com.ftd.pager.service.PagerService;

import com.ftd.pager.util.ConnectionHelper;

import java.sql.Connection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class DeletePagerFormController extends CancellableFormController
{
    private static final Log log = LogFactory.getLog(DeletePagerFormController.class);


    protected PagerService pagerService;

    public void setPagerService(PagerService pagerService)
    {
        this.pagerService = pagerService;
    }

    public ModelAndView onSubmit(Object command) throws Exception
    {
        PagerCommand cmd = (PagerCommand)command;
        PagerVO pager = cmd.getPager();
        Connection con = null;

        try
        {
            con = ConnectionHelper.getConnection();
            pagerService.delete(con, pager);
        } finally
        {
            try
            {
                if (con != null)
                {
                    con.close();
                }

            } catch (Exception e)
            {
                log.error("Error closing connection", e);
            }
        }

        return new ModelAndView(new RedirectView(getSuccessView()));
    }

    public ModelAndView onCancel(Object command) throws ServletException
    {
        return new ModelAndView(new RedirectView(getCancelView()));
    }

    protected Object formBackingObject(HttpServletRequest request) throws Exception
    {
        Connection con = null;

        try
        {
            con = ConnectionHelper.getConnection();
            String id = (String)request.getParameter("id");
            if (id != null)
            {
                return new PagerCommand(pagerService.findById(con, Long.valueOf(id)), true);
            } else
            {
                return new PagerCommand(new PagerVO(), true);
            }
        } finally
        {
            try
            {
                if (con != null)
                {
                    con.close();
                }

            } catch (Exception e)
            {
                log.error("Error closing connection", e);
            }
        }
    }
}
