package com.ftd.pager.admin.controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.ftd.pager.admin.command.PagerGroupCommand;
import com.ftd.pager.model.PagerGroupVO;
import com.ftd.pager.service.PagerGroupService;

import com.ftd.pager.util.ConnectionHelper;

import java.sql.Connection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class DeletePagerGroupFormController extends CancellableFormController
{

    private static final Log log = LogFactory.getLog(DeletePagerGroupFormController.class);
    protected PagerGroupService pagerGroupService;

    public void setPagerGroupService(PagerGroupService pagerGroupService)
    {
        this.pagerGroupService = pagerGroupService;
    }

    public ModelAndView onSubmit(Object command) throws Exception
    {
        PagerGroupCommand cmd = (PagerGroupCommand)command;
        PagerGroupVO pagerGroup = cmd.getPagerGroup();
        Connection con = null;
        
        try
        {
            con = ConnectionHelper.getConnection();
            pagerGroupService.delete(con,pagerGroup);
        }
        finally
        {
            try
            {
                if (con != null)
                {
                    con.close();
                }
                
            }
            catch (Exception e)
            {
                log.error("Error closing connection",e);
            }
        }

        return new ModelAndView(new RedirectView(getSuccessView()));
    }

    public ModelAndView onCancel(Object command) throws ServletException
    {
        return new ModelAndView(new RedirectView(getCancelView()));
    }

    protected Object formBackingObject(HttpServletRequest request) throws Exception
    {
        Connection con = null;
        
        try
        {
            con = ConnectionHelper.getConnection();
            String id = (String)request.getParameter("id");
            if (id != null)
            {
                return new PagerGroupCommand(pagerGroupService.findByIdInitPagers(con,Long.valueOf(id)), true);
            } else
            {
                return new PagerGroupCommand(new PagerGroupVO(), true);
            }
        }
        finally
        {
            try
            {
                if (con != null)
                {
                    con.close();
                }
                
            }
            catch (Exception e)
            {
                log.error("Error closing connection",e);
            }
        }
    }
}
