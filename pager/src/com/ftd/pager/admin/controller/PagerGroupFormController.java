package com.ftd.pager.admin.controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.ftd.pager.admin.command.PagerGroupCommand;
import com.ftd.pager.model.PagerGroupVO;
import com.ftd.pager.service.PagerGroupService;
import com.ftd.pager.util.ConnectionHelper;
import com.ftd.spring.DelegatingWizardFormController;

import java.sql.Connection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class PagerGroupFormController extends DelegatingWizardFormController
{
    private static final Log log = LogFactory.getLog(PagerGroupFormController.class);

    protected PagerGroupService pagerGroupService;

    public void setPagerGroupService(PagerGroupService pagerGroupService)
    {
        this.pagerGroupService = pagerGroupService;
    }

    protected ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object command, 
                                         BindException errors) throws Exception
    {
        PagerGroupCommand cmd = (PagerGroupCommand)command;
        PagerGroupVO pagerGroup = cmd.getPagerGroup();
        Connection con = null;
        
        try
        {
            con = ConnectionHelper.getConnection();
            pagerGroupService.save(con,pagerGroup);
        }
        finally
        {
            try
            {
                if (con != null)
                {
                    con.close();
                }
                
            }
            catch (Exception e)
            {
                log.error("Error closing connection",e);
            }
        }

        return new ModelAndView(new RedirectView(getFinishView()));
    }

    public ModelAndView onCancel(Object command) throws ServletException
    {
        return new ModelAndView(new RedirectView(getCancelView()));
    }

    protected Object formBackingObject(HttpServletRequest request) throws Exception
    {
        Connection con = null;
        
        try
        {
            con = ConnectionHelper.getConnection();
            String id = (String)request.getParameter("id");
            if (id != null)
            {
                return new PagerGroupCommand(pagerGroupService.findByIdInitPagers(con,Long.valueOf(id)), false);
            } else
            {
                return new PagerGroupCommand(false);
            }
        }
        finally
        {
            try
            {
                if (con != null)
                {
                    con.close();
                }
                
            }
            catch (Exception e)
            {
                log.error("Error closing connection",e);
            }
        }
    }

    protected ModelAndView processCancel(HttpServletRequest request, HttpServletResponse response, Object command, 
                                         BindException errors) throws Exception
    {
        return new ModelAndView(new RedirectView(getCancelView()));
    }
}
