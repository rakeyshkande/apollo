package com.ftd.pager.admin.controller;

import java.beans.PropertyEditorSupport;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestDataBinder;

import com.ftd.pager.admin.command.RuleCommand;
import com.ftd.pager.model.PagerVO;
import com.ftd.pager.model.PagerGroupVO;
import com.ftd.pager.service.PagerGroupService;
import com.ftd.pager.service.PagerService;
import com.ftd.pager.service.RuleService;
import com.ftd.pager.util.ConnectionHelper;
import com.ftd.spring.WizardFormControllerDelegate;

import java.sql.Connection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class RuleFormControllerCreateContactDelegate extends WizardFormControllerDelegate
{

    private static final Log log = LogFactory.getLog(RuleFormControllerCreateContactDelegate.class);
    
    protected RuleService ruleService;
    protected PagerService pagerService;
    protected PagerGroupService pagerGroupService;

    public void setRuleService(RuleService ruleService)
    {
        this.ruleService = ruleService;
    }

    public void setPagerService(PagerService pagerService)
    {
        this.pagerService = pagerService;
    }

    public void setPagerGroupService(PagerGroupService pagerGroupService)
    {
        this.pagerGroupService = pagerGroupService;
    }

    public boolean suppressBinding(HttpServletRequest request)
    {
        return request.getParameter("_delegateAction").equalsIgnoreCase("cancel");
    }

    public boolean suppressValidation(HttpServletRequest request)
    {
        return request.getParameter("_delegateAction").equalsIgnoreCase("cancel");
    }

    public void onBindAndValidate(HttpServletRequest request, Object command, BindException errors, int page) throws Exception
    {
        if (!request.getParameter("_delegateAction").equalsIgnoreCase("cancel"))
        {
            getValidator().validate(command, errors);

            if (errors.getErrorCount() == 0)
            {
                RuleCommand cmd = (RuleCommand)command;
                Connection con = null;
                try
                {
                    con = ConnectionHelper.getConnection();
                    cmd.bindNewContact(con,ruleService);
                    
                }
                finally
                {
                    try
                    {
                        if (con != null)
                        {
                            con.close();
                        }
                    }
                    catch (Exception e)
                    {
                        log.error("Error closing connection",e);
                    }
                }

            }
        }
    }

    protected Map referenceData(HttpServletRequest request, Object command, Errors errors, int page) throws Exception
    {
        HashMap map = new HashMap();
        Connection con = null;
        
        try
        {
            con = ConnectionHelper.getConnection();
            map.put("pagers", pagerService.findAll(con));
            map.put("pagerGroups", pagerGroupService.findAll(con));
        }
        finally
        {
            try
            {
                if (con != null)
                {
                    con.close();
                }
                
            }
            catch (Exception e)
            {
                log.error("Error closing connection",e);
            }
        }
        return map;
    }

    protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder)
    {
        binder.registerCustomEditor(PagerVO.class, "newContact.pager", new PropertyEditorSupport()
                {

                    public void setAsText(String text) throws IllegalArgumentException
                    {
                        Connection con = null;
                        Object target = null;
                        if (text != null && StringUtils.isNumeric(text))
                        {
                            Long id = new Long((String)text);
                            try
                            {
                                con = ConnectionHelper.getConnection();
                                target = pagerService.findById(con,id);
                            } catch (Exception e)
                            {
                                target = null;
                            }
                            finally
                            {
                                try
                                {
                                    if (con != null)
                                    {
                                        con.close();
                                    }
                                    
                                }
                                catch (Exception e)
                                {
                                    log.error("Error closing connection",e);
                                }
                            }
                        }
                        super.setValue(target);
                    }
                });
        binder.registerCustomEditor(PagerGroupVO.class, "newContact.pagerGroup", new PropertyEditorSupport()
                {

                    public void setAsText(String text) throws IllegalArgumentException
                    {
                        Connection con = null;
                        Object target = null;
                        if (text != null && StringUtils.isNumeric(text))
                        {
                            Long id = new Long((String)text);
                            try
                            {
                                con = ConnectionHelper.getConnection();
                                target = pagerGroupService.findById(con,id);
                            } catch (Exception e)
                            {
                                target = null;
                            }
                            finally
                            {
                                try
                                {
                                    if (con != null)
                                    {
                                        con.close();
                                    }
                                    
                                }
                                catch (Exception e)
                                {
                                    log.error("Error closing connection",e);
                                }
                            }
                        }
                        super.setValue(target);
                    }
                });
    }
}
