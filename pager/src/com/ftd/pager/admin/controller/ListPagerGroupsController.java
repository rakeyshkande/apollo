package com.ftd.pager.admin.controller;

import com.ftd.pager.job.CheckReplyJob;
import com.ftd.pager.util.ConnectionHelper;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.ftd.web.Navigation;

import java.sql.Connection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ListPagerGroupsController implements Controller
{

    private Navigation navigation;
    private static final Log log = LogFactory.getLog(ListPagerGroupsController.class);

    public void setNavigation(Navigation navigation)
    {
        this.navigation = navigation;
    }

    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
    {

        HashMap model = new HashMap();

        Connection con = null;
        
        try
        {
            con = ConnectionHelper.getConnection();
            
            navigation.handleRequest(request, con);

            model.put("pageNavigation", navigation.getPageNavigation());
            model.put("pagerGroups", navigation.getCurrentPageResults(con));
        }
        finally
        {
            try
            {
                if (con != null)
                {
                    con.close();
                }
                
            }
            catch (Exception e)
            {
                log.error("Error closing connection",e);
            }
        }

        return new ModelAndView("listPagerGroups", "model", model);
    }
}
