package com.ftd.pager.admin.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.validation.BindException;
import org.springframework.validation.Errors;

import com.ftd.pager.admin.command.PagerGroupCommand;
import com.ftd.pager.model.PagerVO;
import com.ftd.pager.service.PagerGroupService;
import com.ftd.pager.util.ConnectionHelper;
import com.ftd.spring.WizardFormControllerDelegate;

import java.sql.Connection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class PagerGroupFormControllerRemovePagerDelegate extends WizardFormControllerDelegate
{
    private static final Log log = LogFactory.getLog(PagerGroupFormControllerRemovePagerDelegate.class);

    protected PagerGroupService pagerGroupService;

    public void setPagerGroupService(PagerGroupService pagerGroupService)
    {
        this.pagerGroupService = pagerGroupService;
    }

    public boolean suppressBinding(HttpServletRequest request)
    {
        return request.getParameter("_delegateAction").equalsIgnoreCase("cancel");
    }

    public boolean suppressValidation(HttpServletRequest request)
    {
        return request.getParameter("_delegateAction").equalsIgnoreCase("cancel");
    }

    public void onBindAndValidate(HttpServletRequest request, Object command, BindException errors, int page) throws Exception
    {
        if (!request.getParameter("_delegateAction").equalsIgnoreCase("cancel"))
        {

            if (errors.getErrorCount() == 0)
            {
                Connection con = null;
                
                try
                {
                    con = ConnectionHelper.getConnection();
                    PagerGroupCommand cmd = (PagerGroupCommand)command;
                    String id = (String)request.getParameter("_pagerIndex");
                    PagerVO pager = (PagerVO) cmd.getPagerGroup().getPagerByIndex(new Integer(id));
                    cmd.setNewPager(pager);
                    cmd.removePager(con, pagerGroupService);
                }
                finally
                {
                    try
                    {
                        if (con != null)
                        {
                            con.close();
                        }
                        
                    }
                    catch (Exception e)
                    {
                        log.error("Error closing connection",e);
                    }
                }
                //cmd.getPagerGroup().removePagerByIndex(new Integer(id));
            }
        }
    }

    protected Map referenceData(HttpServletRequest request, Object command, Errors errors, int page) throws Exception
    {
        Map map = null;
        String index = (String)request.getParameter("_pagerIndex");
        if (index != null)
        {
            map = new HashMap();
            PagerGroupCommand cmd = (PagerGroupCommand)command;
            map.put("pager", cmd.getPagerGroup().getPagerByIndex(new Integer(index)));
            map.put("pagerIndex", index);
        }
        return map;
    }


}
