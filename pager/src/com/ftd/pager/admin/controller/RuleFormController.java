package com.ftd.pager.admin.controller;

import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringEscapeUtils;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.ftd.pager.admin.command.RuleCommand;
import com.ftd.pager.model.RuleVO;
import com.ftd.pager.service.RuleService;
import com.ftd.pager.service.RulesEngineService;
import com.ftd.pager.util.ConnectionHelper;
import com.ftd.spring.DelegatingWizardFormController;

import java.sql.Connection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class RuleFormController extends DelegatingWizardFormController
{

    private static final Log log = LogFactory.getLog(RuleFormController.class);
    protected RuleService ruleService;
    protected RulesEngineService rulesEngineService;

    public void setRuleService(RuleService ruleService)
    {
        this.ruleService = ruleService;
    }

    public void setRulesEngineService(RulesEngineService rulesEngineService)
    {
        this.rulesEngineService = rulesEngineService;
    }

    protected Object formBackingObject(HttpServletRequest request) throws Exception
    {
        Connection con = null;
        
        try
        {
            con = ConnectionHelper.getConnection();
            String id = (String)request.getParameter("id");
            if (id != null)
            {
                RuleVO rule = ruleService.findByIdInitContacts(con,Long.valueOf(id));
                rule.setCondition(StringEscapeUtils.unescapeXml(rule.getCondition()));
                rule.setConsequence(StringEscapeUtils.unescapeXml(rule.getConsequence()));
                return new RuleCommand(rule);
            }
        }
        finally
        {
            try
            {
                if (con != null)
                {
                    con.close();
                }
                
            }
            catch (Exception e)
            {
                log.error("Error closing connection",e);
            }
        }
        return new RuleCommand();
    }

    protected ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object command, 
                                         BindException errors) throws Exception
    {
        Connection con = null;
        
        try
        {
            con = ConnectionHelper.getConnection();
            RuleCommand cmd = (RuleCommand)command;
            RuleVO rule = cmd.getRule();
            if (rule.getOnHold() == true)
            {
                Calendar now = Calendar.getInstance();
                now.add(Calendar.HOUR_OF_DAY, 1);
                rule.setHoldUntil(now.getTime());
            } else  
            {
                rule.setHoldUntil(null);
            }
            ruleService.save(con,rule);
            rulesEngineService.init();
        }
        finally
        {
            try
            {
                if (con != null)
                {
                    con.close();
                }
                
            }
            catch (Exception e)
            {
                log.error("Error closing connection",e);
            }
        }


        return new ModelAndView(new RedirectView(getFinishView()));
    }

    protected ModelAndView processCancel(HttpServletRequest request, HttpServletResponse response, Object command, 
                                         BindException errors) throws Exception
    {
        return new ModelAndView(new RedirectView(getCancelView()));
    }
}
