package com.ftd.pager.admin.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.validation.BindException;
import org.springframework.validation.Errors;

import com.ftd.pager.admin.command.RuleCommand;
import com.ftd.pager.model.ContactVO;
import com.ftd.pager.service.RuleService;
import com.ftd.pager.util.ConnectionHelper;
import com.ftd.spring.WizardFormControllerDelegate;

import java.sql.Connection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class RuleFormControllerDeleteContactDelegate extends WizardFormControllerDelegate
{
    private static final Log log = LogFactory.getLog(RuleFormControllerDeleteContactDelegate.class);

    protected RuleService ruleService;

    public void setRuleService(RuleService ruleService)
    {
        this.ruleService = ruleService;
    }

    public boolean suppressBinding(HttpServletRequest request)
    {
        return request.getParameter("_delegateAction").equalsIgnoreCase("cancel");
    }

    public boolean suppressValidation(HttpServletRequest request)
    {
        return request.getParameter("_delegateAction").equalsIgnoreCase("cancel");
    }

    public void onBindAndValidate(HttpServletRequest request, Object command, BindException errors, int page) throws Exception
    {
        if (!request.getParameter("_delegateAction").equalsIgnoreCase("cancel"))
        {

            if (errors.getErrorCount() == 0)
            {
                RuleCommand cmd = (RuleCommand)command;
                String id = (String)request.getParameter("_contactIndex");
                Connection con = null;
                try
                {
                    con = ConnectionHelper.getConnection();
                    ContactVO contact = (ContactVO) cmd.getRule().getContactByIndex(new Integer(id));
                    cmd.setNewContact(contact);
                    cmd.removeContact(con, ruleService);
                }
                finally
                {
                    try
                    {
                        if (con != null)
                        {
                            con.close();
                        }
                        
                    }
                    catch (Exception e)
                    {
                        log.error("Error closing connection",e);
                    }
                }
                //cmd.getRule().removeContactByIndex(new Integer(id));
            }
        }
    }

    protected Map referenceData(HttpServletRequest request, Object command, Errors errors, int page) throws Exception
    {
        Map map = null;
        String index = (String)request.getParameter("_contactIndex");
        if (index != null)
        {
            map = new HashMap();
            RuleCommand cmd = (RuleCommand)command;
            map.put("contact", cmd.getRule().getContactByIndex(new Integer(index)));
            map.put("contactIndex", index);
        }
        return map;
    }


}
