package com.ftd.pager.admin.controller;

import com.ftd.pager.util.ConnectionHelper;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.ftd.web.Navigation;

import java.sql.Connection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ListRulesController implements Controller
{

    private static final Log log = LogFactory.getLog(ListRulesController.class);
    private Navigation navigation;

    public void setNavigation(Navigation navigation)
    {
        this.navigation = navigation;
    }

    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Connection con = null;
        
        try
        {
            con = ConnectionHelper.getConnection();

            HashMap model = new HashMap();

            navigation.handleRequest(request, con);

            model.put("pageNavigation", navigation.getPageNavigation());
            model.put("rules", navigation.getCurrentPageResults(con));

            return new ModelAndView("listRules", "model", model);
        }
        finally
        {
            try
            {
                if (con != null)
                {
                    con.close();
                }
                
            }
            catch (Exception e)
            {
                log.error("Error closing connection",e);
            }
        }
    }
}
