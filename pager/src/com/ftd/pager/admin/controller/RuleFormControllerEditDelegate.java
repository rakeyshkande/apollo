package com.ftd.pager.admin.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.validation.BindException;

import com.ftd.spring.WizardFormControllerDelegate;

public class RuleFormControllerEditDelegate extends WizardFormControllerDelegate {

    public boolean suppressBinding(HttpServletRequest request) {
        return getController().isCancelRequest(request);
    }

    public boolean suppressValidation(HttpServletRequest request) {
        return getController().isCancelRequest(request);
    }

    public void onBindAndValidate(HttpServletRequest request, Object command, BindException errors, int page) throws Exception {
        if(getController().isFinishRequest(request)) {
            getValidator().validate(command, errors);
        }
    }
}
