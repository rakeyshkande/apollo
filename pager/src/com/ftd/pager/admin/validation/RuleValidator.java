package com.ftd.pager.admin.validation;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.ftd.pager.admin.command.RuleCommand;
import com.ftd.pager.service.RuleService;
import com.ftd.pager.service.RulesEngineService;

public class RuleValidator implements Validator
{

    protected RuleService ruleService;
    protected RulesEngineService rulesEngineService;

    public void setRuleService(RuleService ruleService)
    {
        this.ruleService = ruleService;
    }

    public boolean supports(Class clazz)
    {
        return RuleCommand.class.isAssignableFrom(clazz);
    }

    public void setRulesEngineService(RulesEngineService rulesEngineService)
    {
        this.rulesEngineService = rulesEngineService;
    }

    public void validate(Object obj, Errors errors)
    {
        if (obj != null)
        {
            try
            {
                RuleCommand cmd = (RuleCommand)obj;
                if (cmd.getRule() != null)
                {
                    if (cmd.getRule().getName() == null || 
                        cmd.getRule().getName().length() == 0)
                    {
                        errors.rejectValue("rule.name", 
                                           "error.name-not-specified", null, 
                                           "Value required.");
                    }

                    if (cmd.getRule() != null && 
                        rulesEngineService.validateRule(cmd.getRule()) == 
                        false)
                    {
                        errors.rejectValue("rule.condition", 
                                           "error.invalid-rule", null, 
                                           "Invalid rule.");
                        errors.rejectValue("rule.consequence", 
                                           "error.invalid-rule", null, 
                                           "Invalid rule.");
                    }
                } else
                {
                    errors.reject("Must enter values into form.");
                }
            } catch (Exception e)
            {
                errors.reject("Exception: " + e.getMessage());
            }
        } else
        {
            errors.reject("Must enter values into form.");
        }
    }
}
