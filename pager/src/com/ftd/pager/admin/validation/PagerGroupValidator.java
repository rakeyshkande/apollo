package com.ftd.pager.admin.validation;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.ftd.pager.admin.command.PagerGroupCommand;

public class PagerGroupValidator implements Validator {

    public boolean supports(Class clazz) {
        return PagerGroupCommand.class.isAssignableFrom(clazz);
    }

    public void validate(Object obj, Errors errors) {
        if(obj != null) {
            PagerGroupCommand cmd = (PagerGroupCommand)obj;
            if( cmd.getPagerGroup() != null ) {
                if(cmd.getPagerGroup().getName() == null || cmd.getPagerGroup().getName().length() == 0) {
                    errors.rejectValue("pagerGroup.name", "error.name-not-specified", null, "Value required.");
                }
            }
            else {
                errors.reject("Must enter values into form.");
            }
        }
        else {
            errors.reject("Must enter values into form.");
        }
    }
}
