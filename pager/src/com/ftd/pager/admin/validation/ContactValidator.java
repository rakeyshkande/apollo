package com.ftd.pager.admin.validation;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.ftd.pager.admin.command.RuleCommand;

public class ContactValidator implements Validator {

    public boolean supports(Class clazz) {
        return RuleCommand.class.isAssignableFrom(clazz);
    }

    public void validate(Object obj, Errors errors) {
        if(obj != null) {
            RuleCommand cmd = (RuleCommand)obj;
            if( cmd.getNewContact() != null ) {
                if(cmd.getNewContact().getPager() == null && cmd.getNewContact().getPagerGroup() == null) {
                    if(cmd.getPagerType().equals("pager")) {
                        errors.rejectValue("newContact.pager", "error.pager-not-specified", null, "Value required.");
                    }
                    else if(cmd.getPagerType().equals("pagerGroup")) {
                        errors.rejectValue("newContact.pagerGroup", "error.pagerGroup-not-specified", null, "Value required.");
                    }
                }
            }
            else {
                errors.reject("Must enter values into form.");
            }
        }
        else {
            errors.reject("Must enter values into form.");
        }
    }
}
