package com.ftd.pager.admin.validation;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.ftd.pager.admin.command.PagerCommand;

public class PagerValidator implements Validator {

    public boolean supports(Class clazz) {
        return PagerCommand.class.isAssignableFrom(clazz);
    }

    public void validate(Object obj, Errors errors) {
        if(obj != null) {
            PagerCommand cmd = (PagerCommand)obj;
            if( cmd.getPager() != null ) {
                if(cmd.getPager().getName() == null || cmd.getPager().getName().length() == 0) {
                    errors.rejectValue("pager.name", "error.name-not-specified", null, "Value required.");
                }
                if(cmd.getPager().getEmail() == null || cmd.getPager().getEmail().length() == 0) {
                    errors.rejectValue("pager.email", "error.email-not-specified", null, "Value required.");
                }
            }
            else {
                errors.reject("Must enter values into form.");
            }
        }
        else {
            errors.reject("Must enter values into form.");
        }
    }
}
