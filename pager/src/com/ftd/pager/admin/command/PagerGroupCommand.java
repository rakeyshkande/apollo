package com.ftd.pager.admin.command;

import com.ftd.pager.model.PagerVO;
import com.ftd.pager.model.PagerGroupVO;

import com.ftd.pager.service.PagerGroupService;

import java.sql.Connection;

public class PagerGroupCommand
{

    PagerGroupVO pagerGroup = new PagerGroupVO();
    PagerVO newPager = new PagerVO();
    boolean disabled;

    public PagerGroupCommand(boolean disabled)
    {
        this.disabled = disabled;
    }

    public PagerGroupCommand(PagerGroupVO pagerGroup, boolean disabled)
    {
        this.pagerGroup = pagerGroup;
        this.disabled = disabled;
    }

    /**
     * @return Returns the pagerGroup.
     */
    public PagerGroupVO getPagerGroup()
    {
        return pagerGroup;
    }

    public PagerVO getNewPager()
    {
        return newPager;
    }

    public void setNewPager(PagerVO pager)
    {
        this.newPager = pager;
    }

    public void bindNewPager(Connection con, PagerGroupService pagerGroupService) throws Exception
    {
        pagerGroupService.addPagerToGroup(con, pagerGroup, newPager);
        this.newPager = new PagerVO();
    }

    public void removePager(Connection con, PagerGroupService pagerGroupService) throws Exception
    {
        pagerGroupService.removePagerFromGroup(con, pagerGroup, newPager);
        this.newPager = new PagerVO();
    }

    /**
     * @return Returns the disabled.
     */
    public boolean isDisabled()
    {
        return disabled;
    }
}
