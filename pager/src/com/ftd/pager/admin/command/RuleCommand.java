package com.ftd.pager.admin.command;

import com.ftd.pager.model.ContactVO;
import com.ftd.pager.model.RuleVO;

import com.ftd.pager.service.RuleService;

import com.ftd.pager.util.ConnectionHelper;

import java.sql.Connection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class RuleCommand
{
    private static final Log log = LogFactory.getLog(RuleCommand.class);

    RuleVO rule = new RuleVO();
    ContactVO newContact = new ContactVO();

    String pagerType;

    public RuleCommand()
    {

    }

    public RuleCommand(RuleVO rule)
    {
        this.rule = rule;
    }

    public RuleVO getRule()
    {
        return rule;
    }

    public ContactVO getNewContact()
    {
        return newContact;
    }

    public void setNewContact(ContactVO newContact)
    {
        this.newContact = newContact;
    }

    public void bindNewContact(Connection con, RuleService ruleService) throws Exception
    {
        newContact.setRule(getRule());
        getRule().getContacts().put(newContact.getPriority(), newContact);
        ruleService.addContact(con, newContact);

        this.newContact = new ContactVO();
    }

    public String getPagerType()
    {
        return pagerType;
    }

    public void setPagerType(String pagerType)
    {
        this.pagerType = pagerType;
    }
    
    public void removeContact(Connection con, RuleService ruleService) throws Exception
    {
        ruleService.removeContact(con, getRule(), getNewContact());
        this.newContact = new ContactVO();
         
    }
}
