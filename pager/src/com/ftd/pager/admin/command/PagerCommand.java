package com.ftd.pager.admin.command;

import com.ftd.pager.model.PagerVO;

public class PagerCommand {

    PagerVO pager = new PagerVO();
    boolean disabled;
    
    public PagerCommand( boolean disabled ) {
        this.disabled = disabled;
    }

    public PagerCommand( PagerVO pager, boolean disabled ) {
        this.pager = pager;
        this.disabled = disabled;
    }

    /**
     * @return Returns the pager.
     */
    public PagerVO getPager() {
        return pager;
    }

    /**
     * @return Returns the disabled.
     */
    public boolean isDisabled() {
        return disabled;
    }
}
