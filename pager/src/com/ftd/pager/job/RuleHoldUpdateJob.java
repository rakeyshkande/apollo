package com.ftd.pager.job;

import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ftd.pager.model.RuleVO;
import com.ftd.pager.service.RuleService;
import com.ftd.pager.util.ConnectionHelper;

import java.sql.Connection;

public class RuleHoldUpdateJob implements Runnable
{

    private static final Log log = LogFactory.getLog(RuleHoldUpdateJob.class);

    RuleService ruleService;

    public void setRuleService(RuleService ruleService)
    {
        this.ruleService = ruleService;
    }

    public void run()
    {
        Connection con = null;
        try 
        {
            con = ConnectionHelper.getConnection();
            List rules = ruleService.findExpiredOnHold(con);
            if (rules.size() > 0)
            {
                for (Iterator it = rules.iterator(); it.hasNext(); )
                {
                    RuleVO rule = (RuleVO)it.next();
                    rule.setOnHold(false);
                    rule.setHoldUntil(null);
                    ruleService.save(con, rule);
                }
            }
        } catch (Exception e)
        {
            log.error("Caught exception while running RuleHoldUpdateJob", e);
        }
        finally
        {
            try
            {
                if (con != null)
                {
                    con.close();
                }
                
            }
            catch (Exception e)
            {
                log.error("Error closing connection",e);
            }
        }
    }
}
