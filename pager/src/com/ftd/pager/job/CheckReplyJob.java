package com.ftd.pager.job;

import java.util.Iterator;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ftd.pager.service.SystemMessageMailService;
import com.ftd.pager.service.SystemMessageService;
import com.ftd.pager.util.ConnectionHelper;

import java.sql.Connection;

public class CheckReplyJob implements Runnable
{

    private static final Log log = LogFactory.getLog(CheckReplyJob.class);

    SystemMessageService systemMessageService;
    SystemMessageMailService mailService;

    public void setSystemMessageService(SystemMessageService systemMessageService)
    {
        this.systemMessageService = systemMessageService;
    }

    public void setMailService(SystemMessageMailService mailService)
    {
        this.mailService = mailService;
    }

    public void run()
    {
        Connection con = null;
        try 
        {
            con = ConnectionHelper.getConnection();

            Map unread = mailService.findUnreadMessages();
            for (Iterator it = unread.keySet().iterator(); it.hasNext(); )
            {
                Long mailId = (Long)it.next();
                Long systemMessageId = (Long)unread.get(mailId);
                processMessage(con, mailId, systemMessageId);
            }
        } catch (Exception e)
        {
            log.error("Caught exception while running CheckReplyJob", e);
        }
        finally
        {
            try
            {
                if (con != null)
                {
                    con.close();
                }
                
            }
            catch (Exception e)
            {
                log.error("Error closing connection",e);
            }
        }
    }

    /**
     * @param mailId
     * @param systemMessageId
     *
     * Managed by Spring transaction. If update fails, mail message will not be marked read
     * and tx will rollback
     *
     */
    private void processMessage(Connection con, Long mailId, Long systemMessageId) throws Exception
    {
        systemMessageService.updateMessageReadById(con, systemMessageId);
        mailService.markMessageRead(mailId);
    }

}
