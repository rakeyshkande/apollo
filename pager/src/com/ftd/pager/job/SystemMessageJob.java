package com.ftd.pager.job;

import com.ftd.pager.model.SystemMessageVO;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ftd.pager.service.RulesEngineService;
import com.ftd.pager.service.SystemMessageService;

import com.ftd.pager.util.ConnectionHelper;

import java.sql.Connection;

public class SystemMessageJob implements Runnable 
{
    private static final Log log = LogFactory.getLog(SystemMessageJob.class);

    SystemMessageService systemMessageService;
    RulesEngineService rulesEngineService;
    
    public void setSystemMessageService(SystemMessageService systemMessageService) 
    {
        this.systemMessageService = systemMessageService;
    }

    public void setRulesEngineService(RulesEngineService rulesEngineService) 
    {
        this.rulesEngineService = rulesEngineService;
    }

    public void run() 
    {
        // run query, rules, emails, update
        Connection con = null;
        try 
        {
            con = ConnectionHelper.getConnection();
        
            List messageIds = systemMessageService.findUnreadMessages(con);
            for(Iterator it = messageIds.iterator(); it.hasNext();) 
            {
                SystemMessageVO systemMessage = (SystemMessageVO) it.next();
                processSystemMessage(con, systemMessage);
            }
        }
        catch(Exception e) 
        {
            log.error("Caught exception while running SystemMessageJob", e);
        }
        finally
        {
            try
            {
                if (con != null)
                {
                    con.close();
                }
                
            }
            catch (Exception e)
            {
                log.error("Error closing connection",e);
            }
        }
    }

    /**
     * @param systemMessage
     * 
     * Managed by Spring transaction. If rule update or send mail fails, system
     * message update will roll back.
     * 
     */
    private void processSystemMessage(Connection con, SystemMessageVO systemMessage)
    {
        try
        {
            rulesEngineService.processSystemMessage(con,systemMessage);
        }
        catch (Exception e)
        {
            log.fatal("Fatal error processing system message",e);
        }
    }

}
