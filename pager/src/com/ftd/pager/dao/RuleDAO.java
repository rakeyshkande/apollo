package com.ftd.pager.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.util.Collection;
import java.util.List;

import com.ftd.pager.model.RuleVO;

import java.math.BigDecimal;
import java.sql.Connection;

import java.util.ArrayList;
import java.util.Map;

import org.apache.commons.lang.StringUtils;


/**
 * Data access object (DAO) for domain model class Rule.
 * @see com.ftd.pager.model.RuleVO
 */
public class RuleDAO extends BaseDAO
{
    private final static String IN_RULE_ID       = "IN_RULE_ID";
    private final static String IN_NAME          = "IN_NAME";
    private final static String IN_CONDITION     = "IN_CONDITION";
    private final static String IN_CONSEQUENCE   = "IN_CONSEQUENCE";
    private final static String IN_SALIENCE      = "IN_SALIENCE";
    private final static String IN_ACTIVE        = "IN_ACTIVE";
    private final static String IN_ON_HOLD       = "IN_ON_HOLD";
    private final static String IN_HOLD_UNTIL    = "IN_HOLD_UNTIL";

    private final static String OUT_RULE_ID          = "OUT_RULE_ID";
    private final static String COLUMN_RULE_ID       = "RULE_ID";
    private final static String COLUMN_NAME          = "NAME";
    private final static String COLUMN_CONDITION     = "CONDITION";
    private final static String COLUMN_CONSEQUENCE   = "CONSEQUENCE";
    private final static String COLUMN_SALIENCE      = "SALIENCE";
    private final static String COLUMN_ACTIVE        = "ACTIVE";
    private final static String COLUMN_ON_HOLD       = "ON_HOLD";
    private final static String COLUMN_HOLD_UNTIL    = "HOLD_UNTIL";

    private final static String STMT_DELETE_RULE            = "DELETE_RULE";
    private final static String STMT_INSERT_RULE            = "INSERT_RULE";
    private final static String STMT_UPDATE_RULE            = "UPDATE_RULE";
    private final static String STMT_FIND_RULE_BY_ID        = "FIND_RULE_BY_ID";
    private final static String STMT_FIND_RULE_ACTIVE       = "FIND_RULE_ACTIVE";
    private final static String STMT_FIND_RULE_EXPIRE_HOLD  = "FIND_RULE_EXPIRE_HOLD";
    private final static String STMT_FIND_RULE_COUNT        = "FIND_RULE_COUNT";
    private final static String STMT_FIND_RULE_BY_PAGE      = "FIND_RULE_BY_PAGE";
   
    private static Logger       logger  = new Logger("com.ftd.pager.dao.RuleDAO");

    public RuleDAO(Connection con)
    {
        super(con);
    }

    public void save(RuleVO rule) throws Exception
    {
        if (rule.getId() != null)
        {
            update(rule);
        }
        else
        {
            insert(rule);
        }
    }
        
    public void insert(RuleVO rule) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(getConnection());
        dataRequest.setStatementID(STMT_INSERT_RULE);
        dataRequest.addInputParam(IN_NAME, rule.getName());
        dataRequest.addInputParam(IN_CONDITION, rule.getCondition());
        dataRequest.addInputParam(IN_CONSEQUENCE, rule.getConsequence());
        dataRequest.addInputParam(IN_SALIENCE, new Integer(rule.getSalience()));
        dataRequest.addInputParam(IN_ACTIVE, boolToYes(rule.getActive()));
        dataRequest.addInputParam(IN_ON_HOLD, boolToYes(rule.getOnHold()));
        dataRequest.addInputParam(IN_HOLD_UNTIL, rule.getHoldUntilSqlDate());
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get(STATUS_PARAM);
        if( StringUtils.equals(status, "N") )
        {
            String message = (String) outputs.get(MESSAGE_PARAM);
            throw new Exception(message);
        }
	BigDecimal rule_id = (BigDecimal) outputs.get(OUT_RULE_ID);
        Long rule_idLong = new Long(rule_id.longValue());
        rule.setId(rule_idLong);
        
        logger.debug("Insert Pager Successful");
        }

    public void update(RuleVO rule) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(getConnection());
        dataRequest.setStatementID(STMT_UPDATE_RULE);
        dataRequest.addInputParam(IN_RULE_ID, rule.getId());
        dataRequest.addInputParam(IN_NAME, rule.getName());
        dataRequest.addInputParam(IN_CONDITION, rule.getCondition());
        dataRequest.addInputParam(IN_CONSEQUENCE, rule.getConsequence());
        dataRequest.addInputParam(IN_SALIENCE, new Integer(rule.getSalience()));
        dataRequest.addInputParam(IN_ACTIVE, boolToYes(rule.getActive()));
        dataRequest.addInputParam(IN_ON_HOLD, boolToYes(rule.getOnHold()));
        dataRequest.addInputParam(IN_HOLD_UNTIL, rule.getHoldUntilSqlDate());
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get(STATUS_PARAM);
        if( StringUtils.equals(status, "N") )
        {
            String message = (String) outputs.get(MESSAGE_PARAM);
            throw new Exception(message);
        }
        
        logger.debug("Insert Pager Successful");
    }
    
	public void delete(RuleVO rule) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(getConnection());
        dataRequest.setStatementID(STMT_DELETE_RULE);
        dataRequest.addInputParam(IN_RULE_ID, rule.getId());
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get(STATUS_PARAM);
        if( StringUtils.equals(status, "N") )
        {
            String message = (String) outputs.get(MESSAGE_PARAM);
            throw new Exception(message);
        }
        logger.debug("Delete Rule Successful");
    }
    
    public RuleVO findById(Long id) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(getConnection());
        dataRequest.setStatementID(STMT_FIND_RULE_BY_ID);
        dataRequest.addInputParam(IN_RULE_ID, id);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        List rules = buildRuleVOFromResultSet(rs);
        // Can only be one, unique key
        RuleVO rule = null;
        if (rules.size() > 0)
        {
            rule = (RuleVO) rules.get(0);    
        }
        
        logger.debug("FindById Successful");
        return rule;
    }
    
    public Collection findByPage(int page, int size) throws Exception
    {
        int start_count = page * size + 1;
        int end_count = start_count + size - 1;
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(getConnection());
        dataRequest.setStatementID(STMT_FIND_RULE_BY_PAGE);
        dataRequest.addInputParam(IN_START_ROW, new Integer(start_count));
        dataRequest.addInputParam(IN_END_ROW, new Integer(end_count));
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        List rules = buildRuleVOFromResultSet(rs);

        logger.debug("FindByPage Successful");
        return rules;
    }

    public RuleVO findByIdInitContacts(Long id) throws Exception
    {
        RuleVO rule = findById(id);
        ContactDAO contactDAO = new ContactDAO(con);
        List contacts = contactDAO.findByRuleId(rule.getId());
        rule.setContacts(contacts);

        return rule;
    }

    public List findActive() throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(getConnection());
        dataRequest.setStatementID(STMT_FIND_RULE_ACTIVE);
        dataRequest.addInputParam(IN_ACTIVE, boolToYes(Boolean.TRUE));
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        List rules = buildRuleVOFromResultSet(rs);
        
        logger.debug("FindExpiredOnHold Successful");
        return rules;
    }

    public List findExpiredOnHold() throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(getConnection());
        dataRequest.setStatementID(STMT_FIND_RULE_EXPIRE_HOLD);
        dataRequest.addInputParam(IN_ON_HOLD, boolToYes(Boolean.TRUE));
        dataRequest.addInputParam(IN_HOLD_UNTIL, new java.sql.Date(new java.util.Date().getTime()));
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        List rules = buildRuleVOFromResultSet(rs);
        
        logger.debug("FindExpiredOnHold Successful");
        return rules;
    }

    public int getCount() throws Exception
    {
        return getCount(STMT_FIND_RULE_COUNT);
    }
    
    private List buildRuleVOFromResultSet(CachedResultSet rs)
    {
        List rules = new ArrayList();
        
        while (rs.next())
        {
            RuleVO rule = new RuleVO();
            rule.setId(new Long(rs.getLong(COLUMN_RULE_ID)));
            rule.setName(rs.getString(COLUMN_NAME));
            rule.setCondition(rs.getString(COLUMN_CONDITION));
            rule.setConsequence(rs.getString(COLUMN_CONSEQUENCE));
            rule.setSalience(rs.getInt(COLUMN_SALIENCE));
            rule.setActive(isYes(rs.getString(COLUMN_ACTIVE)));
            rule.setOnHold(isYes(rs.getString(COLUMN_ON_HOLD)));
            rule.setHoldUntil(rs.getDate(COLUMN_HOLD_UNTIL));
            
            rules.add(rule);
        }
    
        return rules;
    }

}
