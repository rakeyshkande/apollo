package com.ftd.pager.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;

import com.ftd.osp.utilities.plugins.Logger;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;

import com.ftd.pager.model.PagerGroupVO;

import com.ftd.pager.model.PagerVO;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.Map;

import org.apache.commons.lang.StringUtils;


/**
 * Data access object (DAO) for domain model class PagerGroup.
 * @see com.ftd.pager.model.PagerGroupVO
 * @author MyEclipse - Hibernate Tools
 */
public class PagerGroupDAO extends BaseDAO 
{
    private final static String IN_PAGER_GROUP_ID     = "IN_PAGER_GROUP_ID";
    private final static String IN_PAGER_ID           = "IN_PAGER_ID";
    private final static String IN_NAME               = "IN_NAME";

    private final static String OUT_PAGER_GROUP_ID    = "OUT_PAGER_GROUP_ID";
    private final static String COLUMN_PAGER_GROUP_ID = "PAGER_GROUP_ID";
    private final static String COLUMN_NAME           = "NAME";
    private final static String COLUMN_COUNT          = "COUNT";

    private final static String STMT_INSERT_PAGER_GROUP       = "INSERT_PAGER_GROUP";
    private final static String STMT_UPDATE_PAGER_GROUP       = "UPDATE_PAGER_GROUP";
    private final static String STMT_DELETE_PAGER_GROUP       = "DELETE_PAGER_GROUP";
    private final static String STMT_FIND_PAGER_GROUP_BY_ID   = "FIND_PAGER_GROUP_BY_ID";
    private final static String STMT_FIND_PAGER_GROUP_ALL     = "FIND_PAGER_GROUP_ALL";
    private final static String STMT_FIND_PAGER_GROUP_BY_PAGE = "FIND_PAGER_GROUP_BY_PAGE";
    private final static String STMT_ADD_PAGER_TO_GROUP       = "ADD_PAGER_TO_GROUP";
    private final static String STMT_REMOVE_PAGER_FROM_GROUP  = "REMOVE_PAGER_FROM_GROUP";


    private static Logger       logger  = new Logger("com.ftd.pager.dao.PagerGroupDAO");

    public PagerGroupDAO(Connection con)
    {
        super(con);
    }


    public void save(PagerGroupVO pagerGroup) throws Exception
    {
        if (pagerGroup.getId() != null)
        {
            update(pagerGroup);
        }
        else
        {
            insert(pagerGroup);
        }
    }
        
    public void insert(PagerGroupVO pagerGroup) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(getConnection());
        dataRequest.setStatementID(STMT_INSERT_PAGER_GROUP);
        dataRequest.addInputParam(IN_NAME, pagerGroup.getName());
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get(STATUS_PARAM);
        if( StringUtils.equals(status, "N") )
        {
            String message = (String) outputs.get(MESSAGE_PARAM);
            throw new Exception(message);
        }
        BigDecimal pagerGroup_id = (BigDecimal) outputs.get(OUT_PAGER_GROUP_ID);
        pagerGroup.setId(new Long(pagerGroup_id.longValue()));
        
        logger.debug("Insert PagerGroup Successful");
        }

    public void update(PagerGroupVO pagerGroup) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(getConnection());
        dataRequest.setStatementID(STMT_UPDATE_PAGER_GROUP);
        dataRequest.addInputParam(IN_PAGER_GROUP_ID, pagerGroup.getId());
        dataRequest.addInputParam(IN_NAME, pagerGroup.getName());
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get(STATUS_PARAM);
        if( StringUtils.equals(status, "N") )
        {
            String message = (String) outputs.get(MESSAGE_PARAM);
            throw new Exception(message);
        }
        
        logger.debug("Insert Pager Successful");
    }
    
    public void delete(PagerGroupVO pagerGroup) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(getConnection());
        dataRequest.setStatementID(STMT_DELETE_PAGER_GROUP);
        dataRequest.addInputParam(IN_PAGER_GROUP_ID, pagerGroup.getId());
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get(STATUS_PARAM);
        if( StringUtils.equals(status, "N") )
        {
            String message = (String) outputs.get(MESSAGE_PARAM);
            throw new Exception(message);
        }
        logger.debug("Delete PagerGroup Successful");
    }
    
    public PagerGroupVO findById( Long id) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(getConnection());
        dataRequest.setStatementID(STMT_FIND_PAGER_GROUP_BY_ID);
        dataRequest.addInputParam(IN_PAGER_GROUP_ID, id);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        List pagerGroups = buildPagerGroupVOFromResultSet(rs);
        // Can only be one, unique key
        PagerGroupVO pagerGroup = null;
        if (pagerGroups.size() > 0)
        {
            pagerGroup = (PagerGroupVO) pagerGroups.get(0);    
        }
        
        logger.debug("FindById Successful");
        return pagerGroup;
    }

    public Collection findByPageWithContactCount(int page, int size) throws Exception
    {
        int start_count = page * size + 1;
        int end_count = start_count + size - 1;
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(getConnection());
        dataRequest.setStatementID(STMT_FIND_PAGER_GROUP_BY_PAGE);
        dataRequest.addInputParam(IN_START_ROW, new Integer(start_count));
        dataRequest.addInputParam(IN_END_ROW, new Integer(end_count));
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        List pagerGroups = buildPagerGroupVOFromResultSetWithCount(rs);

        logger.debug("FindByPageWithContactCount Successful");
        return pagerGroups;

    }

    public Collection findAll() throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(getConnection());
        dataRequest.setStatementID(STMT_FIND_PAGER_GROUP_ALL);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        List pagerGroups = buildPagerGroupVOFromResultSet(rs);

        logger.debug("FindAll Successful");
        return pagerGroups;
    }

    public PagerGroupVO findByIdInitPagers(Long i) throws Exception
    {
        PagerGroupVO group = findById(i);
        PagerDAO pagerDao = new PagerDAO(con);
        List pagers = pagerDao.findByPagerGroupId(group.getId());
        group.setPagers(pagers);
        
        return group;
    }

    public int getCount() throws Exception
    {
        return getCount("FIND_PAGER_GROUP_COUNT");
    }
    
    public void addPager(PagerGroupVO pagerGroup, PagerVO pager) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(getConnection());
        dataRequest.setStatementID(STMT_ADD_PAGER_TO_GROUP);
        dataRequest.addInputParam(IN_PAGER_GROUP_ID, pagerGroup.getId());
        dataRequest.addInputParam(IN_PAGER_ID, pager.getId());
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get(STATUS_PARAM);
        if( StringUtils.equals(status, "N") )
        {
            String message = (String) outputs.get(MESSAGE_PARAM);
            throw new Exception(message);
        }
        
        pagerGroup.addPager(pager);
        
        logger.debug("Add Pager Successful");
    }

    public void removePager(PagerGroupVO pagerGroup, PagerVO pager) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(getConnection());
        dataRequest.setStatementID(STMT_REMOVE_PAGER_FROM_GROUP);
        dataRequest.addInputParam(IN_PAGER_GROUP_ID, pagerGroup.getId());
        dataRequest.addInputParam(IN_PAGER_ID, pager.getId());
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get(STATUS_PARAM);
        if( StringUtils.equals(status, "N") )
        {
            String message = (String) outputs.get(MESSAGE_PARAM);
            throw new Exception(message);
        }
        
        pagerGroup.removePager(pager);
        
        logger.debug("Remove Pager Successful");
    }

    private List buildPagerGroupVOFromResultSet(CachedResultSet rs)
    {
        List pagerGroups = new ArrayList();
        
        while (rs.next())
        {
            PagerGroupVO pagerGroup = new PagerGroupVO();
            pagerGroup.setId(new Long(rs.getLong(COLUMN_PAGER_GROUP_ID)));
            pagerGroup.setName(rs.getString(COLUMN_NAME));
            
            pagerGroups.add(pagerGroup);
        }
    
        return pagerGroups;
    }

    private List buildPagerGroupVOFromResultSetWithCount(CachedResultSet rs)
    {
        List pagerGroups = new ArrayList();
        
        while (rs.next())
        {
            List pagerGroupList = new ArrayList();
            PagerGroupVO pagerGroup = new PagerGroupVO();
            pagerGroup.setId(new Long(rs.getLong(COLUMN_PAGER_GROUP_ID)));
            pagerGroup.setName(rs.getString(COLUMN_NAME));
            
            pagerGroupList.add(pagerGroup);
            pagerGroupList.add(new Long(rs.getLong(COLUMN_COUNT)));
            pagerGroups.add(pagerGroupList);
        }
    
        return pagerGroups;
    }
}