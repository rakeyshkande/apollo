package com.ftd.pager.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;

import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.pager.model.PagerVO;

import java.util.ArrayList;

/**
 * Data access object (DAO) for domain model class Pager.
 */
public class PagerDAO extends BaseDAO
{
    private final static String IN_PAGER_ID      = "IN_PAGER_ID";
    private final static String IN_PAGER_GROUP_ID= "IN_PAGER_GROUP_ID";
    private final static String IN_NAME          = "IN_NAME";
    private final static String IN_EMAIL         = "IN_EMAIL";
    private final static String OUT_PAGER_ID     = "OUT_PAGER_ID";

    private final static String COLUMN_PAGER_ID      = "PAGER_ID";
    private final static String COLUMN_NAME          = "NAME";
    private final static String COLUMN_EMAIL         = "EMAIL";
    private final static String COLUMN_CONTACT_COUNT = "CONTACT_COUNT";
    private final static String COLUMN_GROUP_COUNT   = "GROUP_COUNT";;


    private final static String STMT_INSERT_PAGER       = "INSERT_PAGER";
    private final static String STMT_UPDATE_PAGER       = "UPDATE_PAGER";
    private final static String STMT_DELETE_PAGER       = "DELETE_PAGER";
    private final static String STMT_FIND_PAGER_BY_ID   = "FIND_PAGER_BY_ID";
    private final static String STMT_FIND_PAGER_ALL     = "FIND_PAGER_ALL";
    private final static String STMT_FIND_PAGER_BY_PAGE = "FIND_PAGER_BY_PAGE";
    private final static String STMT_FIND_PAGER_BY_GROUP= "FIND_PAGER_BY_GROUP";


    private static Logger       logger  = new Logger("com.ftd.pager.dao.PagerDAO");

	public PagerDAO(Connection con)
    {
        super(con);
    }
    
    public void save(PagerVO pager) throws Exception
    {
        if (pager.getId() != null)
        {
            update(pager);
        }
        else
        {
            insert(pager);
        }
    }
    
    public void insert(PagerVO pager) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(getConnection());
        dataRequest.setStatementID(STMT_INSERT_PAGER);
        dataRequest.addInputParam(IN_NAME, pager.getName());
        dataRequest.addInputParam(IN_EMAIL, pager.getEmail());
    
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
     
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
    
        String status = (String) outputs.get(STATUS_PARAM);
        if( StringUtils.equals(status, "N") )
        {
            String message = (String) outputs.get(MESSAGE_PARAM);
            throw new Exception(message);
        }
        BigDecimal pager_id = (BigDecimal) outputs.get(OUT_PAGER_ID);
        pager.setId(new Long(pager_id.longValue()));
        
        logger.debug("Insert Pager Successful");
    }

    public void update(PagerVO pager) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(getConnection());
        dataRequest.setStatementID(STMT_UPDATE_PAGER);
        dataRequest.addInputParam(IN_PAGER_ID, pager.getId());
        dataRequest.addInputParam(IN_NAME, pager.getName());
        dataRequest.addInputParam(IN_EMAIL, pager.getEmail());
    
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
     
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
    
        String status = (String) outputs.get(STATUS_PARAM);
        if( StringUtils.equals(status, "N") )
        {
            String message = (String) outputs.get(MESSAGE_PARAM);
            throw new Exception(message);
        }
        
        logger.debug("Insert Pager Successful");
    }
    
	public void delete(PagerVO pager) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(getConnection());
        dataRequest.setStatementID(STMT_DELETE_PAGER);
        dataRequest.addInputParam(IN_PAGER_ID, pager.getId());
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get(STATUS_PARAM);
        if( StringUtils.equals(status, "N") )
        {
            String message = (String) outputs.get(MESSAGE_PARAM);
            throw new Exception(message);
        }
        logger.debug("Delete Pager Successful");
    }
    
    public PagerVO findById(Long pagerId) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(getConnection());
        dataRequest.setStatementID(STMT_FIND_PAGER_BY_ID);
        dataRequest.addInputParam(IN_PAGER_ID, pagerId);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        List pagers = buildPagerVOFromResultSet(rs);
        // Can only be one, unique key
        PagerVO pager = null;
        if (pagers.size() > 0)
        {
            pager = (PagerVO) pagers.get(0);    
        }
        
        logger.debug("FindById Successful");
        return pager;
    }

    public List findByPagerGroupId(Long pagerGroupId) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(getConnection());
        dataRequest.setStatementID(STMT_FIND_PAGER_BY_GROUP);
        dataRequest.addInputParam(IN_PAGER_GROUP_ID, pagerGroupId);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        CachedResultSet rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
        List pagers = buildPagerVOFromResultSet(rs);

        logger.debug("FindByPagerGroupId Successful");
        return pagers;
    }

    public Collection findByPageWithContactAndGroupCount(int page, int size) throws Exception
    {
        int start_count = page * size + 1;
        int end_count = start_count + size - 1;
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(getConnection());
        dataRequest.setStatementID(STMT_FIND_PAGER_BY_PAGE);
        dataRequest.addInputParam(IN_START_ROW, new Integer(start_count));
        dataRequest.addInputParam(IN_END_ROW, new Integer(end_count));
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        List pagers = buildPagerVOFromResultSetWithCount(rs);

        logger.debug("FindByPage Successful");
        return pagers;

    }

    public Collection findAll() throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(getConnection());
        dataRequest.setStatementID(STMT_FIND_PAGER_ALL);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        List pagers = buildPagerVOFromResultSet(rs);

        logger.debug("FindAll Successful");
        return pagers;
    }

    public int getCount() throws Exception
    {
        return getCount("FIND_PAGER_COUNT");
    }
    
    private List buildPagerVOFromResultSet(CachedResultSet rs)
    {
        List pagers = new ArrayList();
        
        while (rs.next())
        {
            PagerVO pager = new PagerVO();
            pager.setId(new Long(rs.getLong(COLUMN_PAGER_ID)));
            pager.setName(rs.getString(COLUMN_NAME));
            pager.setEmail(rs.getString(COLUMN_EMAIL));
            
            pagers.add(pager);
        }
    
        return pagers;
    }

    private List buildPagerVOFromResultSetWithCount(CachedResultSet rs)
    {
        List pagers = new ArrayList();
        
        while (rs.next())
        {
            List pagerList = new ArrayList();
            PagerVO pager = new PagerVO();
            pager.setId(new Long(rs.getLong(COLUMN_PAGER_ID)));
            pager.setName(rs.getString(COLUMN_NAME));
            pager.setEmail(rs.getString(COLUMN_EMAIL));
            
            pagerList.add(pager);
            pagerList.add(new Long(rs.getLong(COLUMN_CONTACT_COUNT)));
            pagerList.add(new Long(rs.getLong(COLUMN_GROUP_COUNT)));
            pagers.add(pagerList);
        }
    
        return pagers;
    }    
}