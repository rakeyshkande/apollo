package com.ftd.pager.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.pager.dao.BaseDAO;

import java.math.BigDecimal;
import java.util.List;

import com.ftd.pager.model.ContactVO;

import com.ftd.pager.model.PagerGroupVO;

import com.ftd.pager.model.PagerVO;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.Map;

import org.apache.commons.lang.StringUtils;


/**
 * Data access object (DAO) for domain model class Contact.
 * @see com.ftd.pager.model.ContactVO
 */
public class ContactDAO extends BaseDAO
{
    private final static String IN_CONTACT_ID          = "IN_CONTACT_ID";
    private final static String IN_RULE_ID             = "IN_RULE_ID";
    private final static String IN_PAGER_GROUP_ID      = "IN_PAGER_GROUP_ID";
    private final static String IN_PAGER_ID            = "IN_PAGER_ID";
    private final static String IN_PRIORITY            = "IN_PRIORITY";
    private final static String IN_ESCALATION_MINUTES  = "IN_ESCALATION_MINUTES";
    private final static String OUT_CONTACT_ID         = "OUT_CONTACT_ID";
    private final static String COLUMN_CONTACT_ID          = "CONTACT_ID";
    private final static String COLUMN_RULE_ID             = "RULE_ID";
    private final static String COLUMN_PAGER_GROUP_ID      = "PAGER_GROUP_ID";
    private final static String COLUMN_PAGER_ID            = "PAGER_ID";
    private final static String COLUMN_PRIORITY            = "PRIORITY";
    private final static String COLUMN_ESCALATION_MINUTES  = "ESCALATION_MINUTES";

    private final static String STMT_INSERT_CONTACT          = "INSERT_CONTACT";
    private final static String STMT_UPDATE_CONTACT          = "UPDATE_CONTACT";
    private final static String STMT_DELETE_CONTACT          = "DELETE_CONTACT";
    private final static String STMT_FIND_CONTACT_BY_ID      = "FIND_CONTACT_BY_ID";
    private final static String STMT_FIND_CONTACT_BY_RULE_ID = "FIND_CONTACT_BY_RULE_ID";

    private static Logger       logger  = new Logger("com.ftd.contact.dao.ContactDAO");

    public ContactDAO(Connection con)
    {
        super(con);
    }
    
    
    public void save(ContactVO contact) throws Exception
    {
        if (contact.getId() != null)
        {
            update(contact);
        }
        else
        {
            insert(contact);
        }
    }
    
    public void insert(ContactVO contact) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(getConnection());
        dataRequest.setStatementID(STMT_INSERT_CONTACT);
        dataRequest.addInputParam(IN_ESCALATION_MINUTES, contact.getEscalationMinutes());
        if (contact.getPagerGroup() != null)
        {
            dataRequest.addInputParam(IN_PAGER_GROUP_ID, contact.getPagerGroup().getId()+"");
        }
        else 
        {
            dataRequest.addInputParam(IN_PAGER_GROUP_ID, null);
        }
        if (contact.getPager() != null)
        {
            dataRequest.addInputParam(IN_PAGER_ID, contact.getPager().getId()+"");
        }
        else 
        {
            dataRequest.addInputParam(IN_PAGER_ID, null);
        }
        dataRequest.addInputParam(IN_PRIORITY, contact.getPriority());
        dataRequest.addInputParam(IN_RULE_ID, contact.getRule().getId());
    
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
     
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
    
        String status = (String) outputs.get(STATUS_PARAM);
        if( StringUtils.equals(status, "N") )
        {
            String message = (String) outputs.get(MESSAGE_PARAM);
            throw new Exception(message);
        }
        BigDecimal contact_id = (BigDecimal) outputs.get(OUT_CONTACT_ID);
        contact.setId(new Long(contact_id.longValue()));
        
        logger.debug("Insert contact Successful");
    }

    public void update(ContactVO contact) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(getConnection());
        dataRequest.setStatementID(STMT_UPDATE_CONTACT);
        dataRequest.addInputParam(IN_CONTACT_ID, contact.getId());
        dataRequest.addInputParam(IN_ESCALATION_MINUTES, contact.getEscalationMinutes());
        if (contact.getPagerGroup() != null)
        {
            dataRequest.addInputParam(IN_PAGER_GROUP_ID, contact.getPagerGroup().getId()+"");
        }
        else 
        {
            dataRequest.addInputParam(IN_PAGER_GROUP_ID, null);
        }
        if (contact.getPager() != null)
        {
            dataRequest.addInputParam(IN_PAGER_ID, contact.getPager().getId()+"");
        }
        else 
        {
            dataRequest.addInputParam(IN_PAGER_ID, null);
        }
        dataRequest.addInputParam(IN_PRIORITY, contact.getPriority());
        dataRequest.addInputParam(IN_RULE_ID, contact.getRule().getId());
    
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
     
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
    
        String status = (String) outputs.get(STATUS_PARAM);
        if( StringUtils.equals(status, "N") )
        {
            String message = (String) outputs.get(MESSAGE_PARAM);
            throw new Exception(message);
        }
        
        logger.debug("Insert contact Successful");
    }
    
    public void delete(ContactVO contact) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(getConnection());
        dataRequest.setStatementID(STMT_DELETE_CONTACT);
        dataRequest.addInputParam(IN_CONTACT_ID, contact.getId());
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get(STATUS_PARAM);
        if( StringUtils.equals(status, "N") )
        {
            String message = (String) outputs.get(MESSAGE_PARAM);
            throw new Exception(message);
        }
        logger.debug("Delete contact Successful");
    }
    
    public ContactVO findById(Long contactId) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(getConnection());
        dataRequest.setStatementID(STMT_FIND_CONTACT_BY_ID);
        dataRequest.addInputParam(IN_CONTACT_ID, contactId);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        List contacts = buildContactVOFromResultSet(rs);
        // Can only be one, unique key
        ContactVO contact = null;
        if (contacts.size() > 0)
        {
            contact = (ContactVO) contacts.get(0);    
        }
        
        logger.debug("FindById Successful");
        return contact;
    }

    public List findByRuleId(Long ruleId) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(getConnection());
        dataRequest.setStatementID(STMT_FIND_CONTACT_BY_RULE_ID);
        dataRequest.addInputParam(IN_RULE_ID, ruleId);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        List contacts = buildContactVOFromResultSet(rs);
        
        logger.debug("FindByRuleId Successful");
        return contacts;
    }

    
    private List buildContactVOFromResultSet(CachedResultSet rs) throws Exception
    {
        List contacts = new ArrayList();
        
        while (rs.next())
        {
            ContactVO contact = new ContactVO();
            contact.setId(new Long(rs.getLong(COLUMN_CONTACT_ID)));
            contact.setEscalationMinutes(new Integer(rs.getInt(COLUMN_ESCALATION_MINUTES)));
            
            long pagerGroupId = rs.getLong(COLUMN_PAGER_GROUP_ID);
            if (pagerGroupId != 0)
            {
                PagerGroupDAO pagerGroupDAO = new PagerGroupDAO(con);
                PagerGroupVO pagerGroup = pagerGroupDAO.findByIdInitPagers(new Long(pagerGroupId));
                contact.setPagerGroup(pagerGroup);
            }
            long pagerId = rs.getLong(COLUMN_PAGER_ID);
            if (pagerId != 0)
            {
                PagerDAO pagerDAO = new PagerDAO(con);
                PagerVO pager = pagerDAO.findById(new Long(pagerId));
                contact.setPager(pager);
            }
            
            contact.setPriority(new Integer(rs.getInt(COLUMN_PRIORITY)));
//            contact.setRuleId(rs.getLong(COLUMN_RULE_ID));

            contacts.add(contact);
        }
    
        return contacts;
    }
    
}