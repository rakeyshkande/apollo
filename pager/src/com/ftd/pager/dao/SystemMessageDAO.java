package com.ftd.pager.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pager.model.SystemMessageVO;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

public class SystemMessageDAO extends BaseDAO
{
    private final static String STMT_FIND_UNREAD_MESSAGES = "VIEW_SYSTEM_MSGS_UNREAD_ESCL";
    private final static String STMT_FIND_MESSAGE_BY_ID   = "VIEW_SYSTEM_MESSAGES_BY_ID";
    private final static String STMT_UPDATE_READ_ID       = "UPDATE_SYSTEM_MESSAGES_READ_ID";
    private final static String STMT_UPDATE_ESCALATION_ID = "UPDATE_SYSTEM_MESSAGES_ESCL_ID";
    
    private final static String COLUMN_SYSTEM_MESSAGE_ID  = "SYSTEM_MESSAGE_ID";
    private final static String COLUMN_SOURCE             = "SOURCE";
    private final static String COLUMN_TYPE               = "TYPE";
    private final static String COLUMN_READ               = "READ";
    private final static String COLUMN_TIMESTAMP          = "TIMESTAMP";
    private final static String COLUMN_COMPUTER           = "COMPUTER";
    private final static String COLUMN_EMAIL_SUBJECT      = "EMAIL_SUBJECT";
    private final static String COLUMN_ESCALATION_LEVEL   = "ESCALATION_LEVEL";
    private final static String COLUMN_ESCALATION_TIME    = "ESCALATION_TIME";
    private final static String COLUMN_MESSAGE            = "MESSAGE";

    private final static String IN_SYSTEM_MESSAGE_ID      = "IN_SYSTEM_MESSAGE_ID";
    private final static String IN_READ                   = "IN_READ_FLAG";
    private final static String IN_ESCALATION_LEVEL       = "IN_ESCALATION_LEVEL";
    private final static String IN_ESCALATION_TIME        = "IN_ESCALATION_TIME";

    private static Logger       logger  = new Logger("com.ftd.pager.dao.SystemMessageDAO");

    public SystemMessageDAO(Connection con)
    {
        super(con);
    }
    
    public List findUnreadMessages() throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(getConnection());
        dataRequest.setStatementID(STMT_FIND_UNREAD_MESSAGES);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        List messages = buildSystemMessageVOFromResultSet(rs);
        
        logger.debug("FindUnreadMessages Successful");
        return messages;
    }

    public SystemMessageVO findUnreadMessageById(Integer id) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(getConnection());
        dataRequest.setStatementID(STMT_FIND_MESSAGE_BY_ID);
        dataRequest.addInputParam(IN_SYSTEM_MESSAGE_ID, id);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        List messages = buildSystemMessageVOFromResultSet(rs);
        // First one only
        SystemMessageVO messageVO = null;
        if (messages.size() > 0)
        {
            messageVO = (SystemMessageVO) messages.get(0);
        }
        
        logger.debug("FindUnreadMessagesById Successful");
        return messageVO;

    }

    public void updateMessageReadById(Long id) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(getConnection());
        dataRequest.setStatementID(STMT_UPDATE_READ_ID);
        dataRequest.addInputParam(IN_SYSTEM_MESSAGE_ID, id);
        dataRequest.addInputParam(IN_READ, "Y");
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get(STATUS_PARAM);
        if( StringUtils.equals(status, "N") )
        {
            String message = (String) outputs.get(MESSAGE_PARAM);
            throw new Exception(message);
        }
        
        logger.debug("updateMessageReadById Successful");
    }


    public void updateMessageEscalationById(Long id, Integer level, Date date) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(getConnection());
        dataRequest.setStatementID(STMT_UPDATE_ESCALATION_ID);
        dataRequest.addInputParam(IN_SYSTEM_MESSAGE_ID, id);
        dataRequest.addInputParam(IN_ESCALATION_LEVEL, level);
        java.sql.Timestamp sqlDate = null;
        if (date != null)
        {
             
            sqlDate = new java.sql.Timestamp(date.getTime());
        }
        dataRequest.addInputParam(IN_ESCALATION_TIME, sqlDate);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        String status = (String) outputs.get(STATUS_PARAM);
        if( StringUtils.equals(status, "N") )
        {
            String message = (String) outputs.get(MESSAGE_PARAM);
            throw new Exception(message);
        }
        
        logger.debug("updateMessageReadById Successful");
    }
    
    private List buildSystemMessageVOFromResultSet(CachedResultSet rs)
    {
        List messages = new ArrayList();
        
        while (rs.next())
        {
            SystemMessageVO message = new SystemMessageVO();
            message.setSystemMessageId(new Long(rs.getLong(COLUMN_SYSTEM_MESSAGE_ID)));
            message.setSource(rs.getString(COLUMN_SOURCE));
            message.setType(rs.getString(COLUMN_TYPE));
            message.setRead(rs.getString(COLUMN_READ));
            message.setTimestamp(rs.getTimestamp(COLUMN_TIMESTAMP));
            message.setComputer(rs.getString(COLUMN_COMPUTER));
            message.setEmailSubject(rs.getString(COLUMN_EMAIL_SUBJECT));
            message.setEscalationLevel(new Integer(rs.getInt(COLUMN_ESCALATION_LEVEL)));
            message.setEscalationTime(rs.getDate(COLUMN_ESCALATION_TIME));
            message.setMessage(rs.getString(COLUMN_MESSAGE));
            
            messages.add(message);
        }
        return messages;
    }
    

}
