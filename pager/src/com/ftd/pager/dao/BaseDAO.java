package com.ftd.pager.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import java.math.BigDecimal;

import java.sql.Connection;

import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

public abstract class BaseDAO
{
    /*********************************************************************************************
    //custom parameters returned from database procedure
    *********************************************************************************************/
    protected final static String IN_START_ROW          = "IN_START_ROW";
    protected final static String IN_END_ROW            = "IN_END_ROW";


    protected static final String STATUS_PARAM     = "OUT_STATUS";
    protected static final String MESSAGE_PARAM    = "OUT_MESSAGE";

    protected final static String OUT_COUNT        = "OUT_COUNT";

    protected final static String  YES = "Y";
    protected final static String  NO  = "N";
    
    protected Connection con;

    protected BaseDAO(Connection con)
    {
        this.con = con;
    }

    public int getCount(String statementId) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(getConnection());
        dataRequest.setStatementID(statementId);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get(STATUS_PARAM);
        if( StringUtils.equals(status, "N") )
        {
            String message = (String) outputs.get(MESSAGE_PARAM);
            throw new Exception(message);
        }
        BigDecimal count = (BigDecimal) outputs.get(OUT_COUNT);
        
        return count.intValue();
    }
    
    
    protected boolean isYes(String yesNoField)
    {
        if (yesNoField == null)
        {
            return false;
        }
        
        if (YES.equalsIgnoreCase(yesNoField))
        {
            return true;
        }
        
        return false;
    }
    
    protected String boolToYes(Boolean value)
    {
        return boolToYes(value.booleanValue());
    }
    protected String boolToYes(boolean value)
    {
        if (value)
        {
            return YES;
        }
        
        return NO;
    }
   
    protected Connection getConnection()
    {
        return con;
    }
}