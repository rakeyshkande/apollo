package com.ftd.pager.model;
import java.util.Date;

public class SystemMessageVO 
{
    private String source;
    private String type;
    private String message;
    private String read;
    private Date   timestamp;
    private Long   systemMessageId;
    private String computer;
    private String emailSubject;
    private Integer escalationLevel;
    private Date   escalationTime;
    
    public SystemMessageVO()
    {
    }


    public void setSource(String source)
    {
        this.source = source;
    }


    public String getSource()
    {
        return source;
    }


    public void setType(String type)
    {
        this.type = type;
    }


    public String getType()
    {
        return type;
    }


    public void setMessage(String message)
    {
        this.message = message;
    }


    public String getMessage()
    {
        return message;
    }


    public void setRead(String read)
    {
        this.read = read;
    }


    public String getRead()
    {
        return read;
    }


    public void setTimestamp(Date timestamp)
    {
        this.timestamp = timestamp;
    }


    public Date getTimestamp()
    {
        return timestamp;
    }


    public void setSystemMessageId(Long systemMessageId)
    {
        this.systemMessageId = systemMessageId;
    }


    public Long getSystemMessageId()
    {
        return systemMessageId;
    }


    public void setComputer(String computer)
    {
        this.computer = computer;
    }


    public String getComputer()
    {
        return computer;
    }


    public void setEmailSubject(String emailSubject)
    {
        this.emailSubject = emailSubject;
    }


    public String getEmailSubject()
    {
        return emailSubject;
    }


    public void setEscalationLevel(Integer escalationLevel)
    {
        this.escalationLevel = escalationLevel;
    }


    public Integer getEscalationLevel()
    {
        return escalationLevel;
    }


    public void setEscalationTime(Date escalationTime)
    {
        this.escalationTime = escalationTime;
    }


    public Date getEscalationTime()
    {
        return escalationTime;
    }
    
    
}