package com.ftd.pager.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

public class RuleVO
{
    private Long id;
    private String name;
    private String condition;
    private String consequence;
    private int salience;
    private boolean active;
    private boolean onHold;
    private Date holdUntil;
    private Map contacts = new HashMap();

    // Constructors

    /** default constructor */
    public RuleVO()
    {
    }

    // Property accessors

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getCondition()
    {
        return this.condition;
    }

    public void setCondition(String rule)
    {
        this.condition = rule;
    }

    public boolean getActive()
    {
        return this.active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public boolean getOnHold()
    {
        return this.onHold;
    }

    public void setOnHold(boolean onHold)
    {
        this.onHold = onHold;
    }

    public Date getHoldUntil()
    {
        return this.holdUntil;
    }

    public java.sql.Date getHoldUntilSqlDate()
    {
        java.sql.Date sqlDate = null;
        if (holdUntil != null)
        {
            sqlDate = new java.sql.Date(holdUntil.getTime());
        }
        return sqlDate;
    }

    public void setHoldUntil(Date holdUntil)
    {
        this.holdUntil = holdUntil;
    }

    public void setContacts(Map contacts)
    {
        this.contacts = contacts;
    }

    public void setContacts(List contactList)
    {
        contacts = new HashMap();
        for (int i=0; i < contactList.size(); i++)
        {
            ContactVO contact = (ContactVO) contactList.get(i);
            contacts.put(contact.getPriority(), contact);            
        }
    }

    public Map getContacts()
    {
        return contacts;
    }

    public Collection getContactList()
    {
        List contactList = new ArrayList(contacts.values());
        Collections.sort(contactList, new Comparator()
                {

                    public int compare(Object o1, Object o2)
                    {
                        ContactVO left = (ContactVO)o1;
                        ContactVO right = (ContactVO)o2;
                        return left.getPriority().intValue() - right.getPriority().intValue();
                    }

                });
        return contactList;
    }

    public String getConsequence()
    {
        return consequence;
    }

    public void setConsequence(String consequence)
    {
        this.consequence = consequence;
    }

    public int getSalience()
    {
        return salience;
    }

    public void setSalience(int salience)
    {
        this.salience = salience;
    }

    public ContactVO getContactByFirstPriority()
    {
        SortedSet keys = new TreeSet(contacts.keySet());
        if (!keys.isEmpty())
        {
            return (ContactVO)contacts.get(keys.first());
        } else
        {
            return null;
        }
    }

    public ContactVO getNextContact(ContactVO contact)
    {
        SortedSet keys = new TreeSet(contacts.keySet());
        Integer next = new Integer(contact.getPriority().intValue() + 1);
        SortedSet higher = keys.tailSet(next);
        if (!higher.isEmpty())
        {
            return (ContactVO)contacts.get(higher.first());
        } else
        {
            return null;
        }
    }

    public void removeContactByIndex(Integer index)
    {
        ContactVO contact = (ContactVO)getContactList().toArray()[index.intValue()];
        if (contact != null)
        {
            contacts.remove(contact.getPriority());
        }
    }

    public void removeContact(ContactVO contact)
    {
        contacts.remove(contact.getPriority());
    }

    public Object getContactByIndex(Integer index)
    {
        return (ContactVO)getContactList().toArray()[index.intValue()];
    }

    public ContactVO getContactByPriority(Integer level)
    {
        return (ContactVO)contacts.get(level);
    }
}
