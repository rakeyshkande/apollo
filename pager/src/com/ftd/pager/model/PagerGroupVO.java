package com.ftd.pager.model;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;


public class PagerGroupVO
{

    // Fields    

    private Long id;
    private String name;
    private Set pagers = new HashSet();

    // Constructors

    /** default constructor */
    public PagerGroupVO()
    {
    }

    // Property accessors

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Set getPagers()
    {
        return pagers;
    }

    public Object getPagerByIndex(Integer index)
    {
        return pagers.toArray()[index.intValue()];
    }

    public void removePagerByIndex(Integer index)
    {
        PagerVO pager = (PagerVO)pagers.toArray()[index.intValue()];
        pagers.remove(pager);
    }

    public void setPagers(Collection pagerList)
    {
        pagers = new LinkedHashSet(pagerList);
    }

    public void removePager(PagerVO pager)
    {
        pagers.remove(pager);
    }

    public void addPager(PagerVO pager)
    {
        pagers.add(pager);
    }
}
