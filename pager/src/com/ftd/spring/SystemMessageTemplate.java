package com.ftd.spring;

import com.ftd.pager.model.SystemMessageVO;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import org.springframework.mail.SimpleMailMessage;

import com.ftd.pager.model.PagerVO;

public class SystemMessageTemplate extends SimpleMailMessage
{
    private String template;

    public void setTemplate(String template)
    {
        this.template = template;
    }

    public SystemMessageTemplate()
    {
        super();
    }

    public SystemMessageTemplate(SystemMessageTemplate msg, PagerVO pager, 
                                 String nextContactName, String escalationTime, 
                                 SystemMessageVO systemMessage)
    {
        super(msg);
        this.template = msg.template;

        String body = template;
        body = StringUtils.replace(body, "%%SYSTEM_MESSAGE_ID%%", systemMessage.getSystemMessageId().toString());
        body = StringUtils.replace(body, "%%COMPUTER%%", systemMessage.getComputer());
        body = StringUtils.replace(body, "%%EMAIL_SUBJECT%%", systemMessage.getEmailSubject());
        body = StringUtils.replace(body, "%%ESCALATION_LEVEL%%", systemMessage.getEscalationLevel().toString());
        if (systemMessage.getEscalationTime() != null)
        {
            body = StringUtils.replace(body, "%%ESCALATION_TIME%%", systemMessage.getEscalationTime().toString());
        }
        body = StringUtils.replace(body, "%%MESSAGE%%", systemMessage.getMessage());
        body = StringUtils.replace(body, "%%READ%%", systemMessage.getRead());
        body = StringUtils.replace(body, "%%SOURCE%%", systemMessage.getSource());
        body = StringUtils.replace(body, "%%TIMESTAMP%%", systemMessage.getTimestamp().toString());
        body = StringUtils.replace(body, "%%TYPE%%", systemMessage.getType());

        body = StringUtils.replace(body, "%%NEXT_CONTACT_NAME%%", nextContactName);
        body = StringUtils.replace(body, "%%NEXT_ESCALATION_TIME%%", escalationTime);

        setText(body);
        setSubject(getSubject() + " " + systemMessage.getSystemMessageId());
        setTo(pager.getName() + " <" + pager.getEmail() + ">");
    }
}
