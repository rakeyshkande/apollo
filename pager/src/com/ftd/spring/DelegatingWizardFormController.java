package com.ftd.spring;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;

public abstract class DelegatingWizardFormController extends
        AbstractWizardFormController {

    WizardFormControllerDelegate[] delegates;
    
    protected String finishView;
    protected String cancelView;

    public String getCancelView() {
        return cancelView;
    }
    
    public void setCancelView(String cancelView) {
        this.cancelView = cancelView;
    }
    
    public String getFinishView() {
        return finishView;
    }
    
    public void setFinishView(String successView) {
        this.finishView = successView;
    }

    public void setDelegates(WizardFormControllerDelegate[] delegates) {
        this.delegates = delegates;
        
        String[] pages = new String[delegates.length];
        Validator[] validators = new Validator[delegates.length];
        
        for(int i = 0; i < delegates.length; i++ ) {
            pages[i] = delegates[i].getPage();
            validators[i] = delegates[i].getValidator();
        }
        setPages(pages);
        setValidators(validators);
    }
    
    public WizardFormControllerDelegate[] getDelegates() {
        return this.delegates;
    }
    
    protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors, int page) throws Exception {
        WizardFormControllerDelegate delegate = getDelegates()[page];
        if(delegate != null) {            
            delegate.onBindAndValidate(request, command, errors, page);
        }
    }

    
    protected Map referenceData(HttpServletRequest request, Object command, Errors errors, int page) throws Exception {
        Map map = null;
        WizardFormControllerDelegate delegate = getDelegates()[page];
        if(delegate != null) {
            map =delegate.referenceData(request, command, errors, page);
        }
        return map;
    }
    
    protected boolean suppressBinding(HttpServletRequest request) {
        int page = getCurrentPage(request);
        WizardFormControllerDelegate delegate = getDelegates()[page];
        if( delegate != null)
            return delegate.suppressBinding(request);
        else
            return super.suppressBinding(request);
    }

    protected boolean suppressValidation(HttpServletRequest request) {
        int page = getCurrentPage(request);
        WizardFormControllerDelegate delegate = getDelegates()[page];
        if( delegate != null)
            return delegate.suppressValidation(request);
        else
            return super.suppressValidation(request);
    }

    protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) {
        try {
            int currentPage = getCurrentPage(request);
            int targetPage = getTargetPage(request, currentPage);

            WizardFormControllerDelegate currentDelegate = getDelegates()[currentPage];
            if( currentDelegate != null)
                currentDelegate.initBinder(request, binder);
            
            WizardFormControllerDelegate targetDelegate = getDelegates()[targetPage];
            if( targetDelegate != null)
                targetDelegate.initBinder(request, binder);
        }
        catch(IllegalStateException ex) {
            // eat this
        }
    }

    public boolean isCancelRequest(HttpServletRequest request) {
        return super.isCancelRequest(request);
    }

    public boolean isFinishRequest(HttpServletRequest request) {
        return super.isFinishRequest(request);
    }

    public boolean isFormSubmission(HttpServletRequest request) {
        return super.isFormSubmission(request);
    }
    
    
}
