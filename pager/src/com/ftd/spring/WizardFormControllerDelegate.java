package com.ftd.spring;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.ServletRequestDataBinder;

public class WizardFormControllerDelegate {

    String page;
    Validator validator;
    DelegatingWizardFormController controller;

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public Validator getValidator() {
        return validator;
    }

    public void setValidator(Validator validator) {
        this.validator = validator;
    }

    public DelegatingWizardFormController getController() {
        return controller;
    }

    public void setController(DelegatingWizardFormController controller) {
        this.controller = controller;
    }

    protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors, int page) throws Exception {
    }
    
    protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) {
    }

    protected Map referenceData(HttpServletRequest request, Object command, Errors errors, int page) throws Exception {
        return null;
    }

    public boolean suppressValidation(HttpServletRequest request) {
        return false;
    }

    public boolean suppressBinding(HttpServletRequest request) {
        return false;
    }
}
