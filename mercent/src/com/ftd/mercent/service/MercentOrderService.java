/**
 * 
 */
package com.ftd.mercent.service;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.ftd.mercent.bo.MercentOrderReportBO;
import com.ftd.mercent.bo.MercentOrderBO;
import com.ftd.mercent.exceptions.MercentException;
import com.ftd.mercent.utilities.MercentUtils;
import com.ftd.mercent.vo.MercentOrderFeed;

/**
 * @author skatam
 *
 */
public class MercentOrderService
{

	public void pullOrdersFromMercent()
	{
		Connection conn = null;
		try {
			conn = MercentUtils.getConnection();
			MercentOrderReportBO mercentOrderReportBO = new MercentOrderReportBO(conn);
			List<MercentOrderFeed> mercentOrderFeeds = mercentOrderReportBO.getOrdersFromMercentFTPServer();
			for (MercentOrderFeed mercentOrderFeed : mercentOrderFeeds) 
			{
				mercentOrderReportBO.process(mercentOrderFeed);
			}
		} 
		catch (MercentException e) 
		{
			throw e;
		}
		finally{
			try {
				conn.close();
			} catch (SQLException e) { }
		}		
	}
	
	public void processMercentOrderFeed(MercentOrderFeed mercentOrderFeed) 
	{
		Connection conn = null;
		try {
			conn = MercentUtils.getConnection();
			MercentOrderReportBO mercentOrderReportBO = new MercentOrderReportBO(conn);
			mercentOrderReportBO.process(mercentOrderFeed);
		} 
		catch (MercentException e) {
			throw e;
		}
		finally{
			try {
				conn.close();
			} catch (SQLException e) { }
		}
		
	}
	
	public void processMercentOrder(String mercentOrderNumber)
	{		
		Connection conn = null;
		try {
			conn = MercentUtils.getConnection();
			MercentOrderBO orderProcessor = new MercentOrderBO(conn);
			orderProcessor.process(mercentOrderNumber);	
		} 
		catch (MercentException e) {
			throw e;
		}
		finally
		{
			try {
				conn.close();
			} catch (SQLException e) { }
		}
			
	}

	public void processOGErrorOrders() {		
		Connection conn = null;
		try {
			conn = MercentUtils.getConnection();
			MercentOrderBO orderProcessor = new MercentOrderBO(conn);
			orderProcessor.processOGErrorOrders();
		} 
		catch (MercentException e) {
			throw e;
		}
		finally
		{
			try {
				conn.close();
			} catch (SQLException e) { }
		}
			
	}
	
}
