package com.ftd.mercent.mdb;

import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import com.ftd.mercent.bo.MercentFulfilledOrderBO;
import com.ftd.mercent.bo.MercentOrderAcknowledgementFeedBO;
import com.ftd.mercent.bo.MercentOrderAdjustmentFeedBO;
import com.ftd.mercent.bo.MercentOrderFulfillmentFeedBO;
import com.ftd.mercent.bo.MercentProductFeedBO;
import com.ftd.mercent.service.MercentOrderService;
import com.ftd.mercent.utilities.MercentConstants;
import com.ftd.osp.utilities.plugins.Logger;

public class MercentMDB implements MessageDrivenBean, MessageListener 
{
	private static final long serialVersionUID = 1L;
	private MessageDrivenContext context;
	private Logger logger;
	private static String logContext = "com.ftd.mercent.mdb.MercentMDB";

	public void ejbCreate() {}

	public void ejbRemove() {}

	public void setMessageDrivenContext(MessageDrivenContext ctx) 
	{
		this.context = ctx;
		logger = new Logger(logContext);
		logger.debug("MessageDrivenContext initialized :"+this.context);
	}

	public void onMessage(Message msg) 
	{		
		if(logger.isDebugEnabled()) {
			logger.debug("**********MercentMDB.onMessage()***************");
		}
		
		try 
		{
			TextMessage textMessage = (TextMessage) msg;
			String msgText = textMessage.getText();
			String action = msg.getJMSCorrelationID();
			logger.info("action: " + action);
			logger.info("msgText: " + msgText);

			if (action == null || action.equals("")) 
			{
				logger.error("Invalid action");
			} else if (action.equalsIgnoreCase(MercentConstants.PULL_ORDERS_FROM_MERCENT))
			{
				logger.debug("Pulling Orders from Mercent...");
				MercentOrderService orderService = new MercentOrderService();
				orderService.pullOrdersFromMercent();
			}
			else if (action.equalsIgnoreCase(MercentConstants.PROCESS_INBOUND_ORDER)) 
			{
				MercentOrderService orderService = new MercentOrderService();
				orderService.processMercentOrder(msgText);
			} 
			else if (action.equalsIgnoreCase(MercentConstants.PROCESS_OG_ERROR_ORDERS)) 
			{
				MercentOrderService orderService = new MercentOrderService();
				orderService.processOGErrorOrders();
			}
			else if (action.equalsIgnoreCase(MercentConstants.PROCESS_PRODUCT_FEED))
			{
				MercentProductFeedBO mercentProductFeedBO = new MercentProductFeedBO();
				mercentProductFeedBO.createMercentFeed();
			}
			else if (action.equalsIgnoreCase(MercentConstants.PROCESS_MRCNT_ORD_ACK_FEED)) {
				MercentOrderAcknowledgementFeedBO ordAckFeedBO = new MercentOrderAcknowledgementFeedBO();
				ordAckFeedBO.createMercentFeed();
				
			} else if (action.equalsIgnoreCase(MercentConstants.PROCESS_MRCNT_ORD_ADJ_FEED)) {
				MercentOrderAdjustmentFeedBO ordAdjFeedBO = new MercentOrderAdjustmentFeedBO();
				ordAdjFeedBO.createMercentFeed();
				
			} else if (action.equalsIgnoreCase(MercentConstants.PROCESS_MRCNT_ORD_FULFILL_FEED)) {
				MercentOrderFulfillmentFeedBO orderFulfillmentFeedBO = new MercentOrderFulfillmentFeedBO();
				orderFulfillmentFeedBO.createMercentFeed();
				
			} else if (action.equalsIgnoreCase(MercentConstants.UPLOAD_ACK_FEED)) {
				MercentOrderAcknowledgementFeedBO ordAckFeedBO = new MercentOrderAcknowledgementFeedBO();
				logger.info("Request received to save feeds of type: " + msgText + " to FTD ftp server");				
				ordAckFeedBO.saveMercentFeed(MercentConstants.MRCNT_ORDER_ACK_FEED);
				
			} else if (action.equalsIgnoreCase(MercentConstants.UPLOAD_ADJ_FEED)) {
				MercentOrderAdjustmentFeedBO ordAdjFeedBO = new MercentOrderAdjustmentFeedBO();
				logger.info("Request received to save feeds of type: " + msgText + " to FTD ftp server");				
				ordAdjFeedBO.saveMercentFeed(MercentConstants.MRCNT_ORDER_ADJ_FEED);
				
			} else if (action.equalsIgnoreCase(MercentConstants.UPLOAD_FULFILL_FEED)) {
				MercentOrderFulfillmentFeedBO orderFulfillmentFeedBO = new MercentOrderFulfillmentFeedBO();
				logger.info("Request received to save feeds of type: " + msgText + " to FTD ftp server");				
				orderFulfillmentFeedBO.saveMercentFeed(MercentConstants.MRCNT_ORDER_FULFILL_FEED);
				
			} else if (action.equalsIgnoreCase(MercentConstants.UPLOAD_PRODUCT_FEED)) {
				MercentProductFeedBO mercentProductFeedBO = new MercentProductFeedBO();
				logger.info("Request received to save feeds of type: " + msgText + " to FTD ftp server");				
				mercentProductFeedBO.saveMercentFeed(MercentConstants.PRODUCT_FEED);
			}
			else if (action.equalsIgnoreCase(MercentConstants.PROCESS_REFUND_FEED)) {
				MercentOrderAdjustmentFeedBO orderAdjustmentFeedBO = new MercentOrderAdjustmentFeedBO();
				logger.info("Request received to process refunds: " + msgText);				
				orderAdjustmentFeedBO.processRefundFeed();
			}
			else if (action.equalsIgnoreCase(MercentConstants.INSERT_FLORIST_FULFILLED_ORDERS)) {
				MercentFulfilledOrderBO fulfilledOrderBO = new MercentFulfilledOrderBO();
				logger.info("Request received to insert Florist fulfilled Orders: " + msgText);				
				fulfilledOrderBO.processFloristFulfilledOrders();
				fulfilledOrderBO.processAutomatedDeliveryEmail();
			}
			else 
			{
				logger.error("Invalid action: " + action);
			}

			logger.info("Finished");
		} 
		catch (Exception e) 
		{
			logger.error(e);
		} catch (Throwable t) 
		{
			logger.error("Thrown error: " + t);
		}
	}

}