package com.ftd.mercent.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ftd.mercent.service.MercentOrderService;
import com.ftd.mercent.utilities.MercentConstants;
import com.ftd.mercent.utilities.MercentUtils;
import com.ftd.mercent.vo.MercentOrderFeed;
import com.ftd.mercent.vo.mercent.MercentFeed;
import com.ftd.osp.utilities.HttpServletResponseUtil;
import com.ftd.osp.utilities.plugins.Logger;

public class MercentOrderGatherer extends HttpServlet
{ 
  
	private static final long serialVersionUID = 1L;
	/**
    * Logger instance
    */
    private Logger logger = new Logger(MercentOrderGatherer.class.getName());
    
    public void init(ServletConfig config) throws ServletException
    {
      super.init(config);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
      PrintWriter out = response.getWriter();
      out.println("The Mercent Order Gatherer!");
      out.close();
      
    }

  /**
   *
   *  DoPost method receives and process the xml file
   *
   * @param request HttpServletRequest
   * @param response HttpServletResponse
   * @exception ServletException
   * @exception IOException
   */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
      logger.info("BEGIN TRANSACTION");

      String orderXML = request.getParameter("Order");
      logger.info(orderXML);

      //check to see if anything was received in the transmission
      if(orderXML == null || orderXML.equals(""))
      {
        this.returnError(response, MercentConstants.EMPTY_XML_ERROR);
        return;
      }
      
      try {
          MercentOrderService mercentOrderService = new MercentOrderService();
          
          MercentFeed mercentFeed = 
        	  	(MercentFeed) MercentUtils.marshallAsObject(orderXML, MercentFeed.class);
          MercentOrderFeed mercentOrderFeed = new MercentOrderFeed(mercentFeed);
          mercentOrderService.processMercentOrderFeed(mercentOrderFeed );
         
          this.returnSuccess(response);
          
      } catch (Exception e) {
    	  logger.error(e);
    	  this.returnError(response, "Couldn't process Order XML");
      } 

    }
 
    /**
    *
    * Set and return success code
    *
    * @param response HttpServletResponse
    */
   private void returnSuccess(HttpServletResponse response) {
       try {
           HttpServletResponseUtil.sendError(response, MercentConstants.SUCCESS_CODE, "Success");
       } catch(Exception ex) {
           logger.error(ex.toString());
       }
   }

   /**
    *
    * Set and return error code
    *
    * @param response HttpServletResponse
    */
   private void returnError(HttpServletResponse response, String errorReason) {
       try {
           HttpServletResponseUtil.sendError(response, MercentConstants.ERROR_CODE, errorReason);
       } catch(Exception ex) {
           logger.error("XML file transmission failed: " + ex.toString());
       }
   }

}