/**
 * 
 */
package com.ftd.mercent.exceptions;

/**
 * @author skatam
 *
 */
public class MercentException extends RuntimeException
{
	private static final long serialVersionUID = 1L;

	public MercentException() {
		super();
		
	}

	public MercentException(String message, Throwable cause) {
		super(message, cause);
		
	}

	public MercentException(String message) {
		super(message);
		
	}

	public MercentException(Throwable cause) {
		super(cause);
		
	}
	
}
