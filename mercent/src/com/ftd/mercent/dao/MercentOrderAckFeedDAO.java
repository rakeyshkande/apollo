/**
 * 
 */
package com.ftd.mercent.dao;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.ftd.mercent.exceptions.MercentException;
import com.ftd.mercent.utilities.MercentConstants;
import com.ftd.mercent.vo.OrderAckFeedVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * @author smeka
 *
 */
public class MercentOrderAckFeedDAO extends MercentFeedDAO {
	
	private Logger logger = new Logger("com.ftd.mercent.dao.MercentOrderAckFeedDAO");

	public MercentOrderAckFeedDAO(Connection conn) {
		this.conn = conn;
	}
	
	/** Gets the acknowledgment feed data for given status.
	 * @param feedStatus
	 * @throws MercentException 
	 */
	@Override
	public List<OrderAckFeedVO> getMercentFeedData(String feedStatus) {
		if(logger.isDebugEnabled()) {
			logger.debug("********* Entering getMercentFeedData() *********");
		}
		
		try {
			List<OrderAckFeedVO> feedList = new ArrayList<OrderAckFeedVO>();

			DataRequest dataRequest = new DataRequest();
			dataRequest.setConnection(this.conn);
			dataRequest.setStatementID(GET_MRCNT_ORD_ACK_FEED_BY_STATUS_STMT);

			HashMap<String, Object> inputParams = new HashMap<String, Object>();
			inputParams.put(IN_FEED_STATUS, feedStatus);
			dataRequest.setInputParams(inputParams);

			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			CachedResultSet crs = (CachedResultSet) dataAccessUtil
					.execute(dataRequest);
			while (crs.next()) {
				OrderAckFeedVO feedDetail = new OrderAckFeedVO();			
				feedDetail.setFeedType(MercentConstants.MRCNT_ORDER_ACK_FEED);
				feedDetail.setOrderAckId(crs.getString(MRCNT_ORDER_ACKNOWLEDGEMENT_ID));
				feedDetail.setChannelOrderID(crs.getString(CHANNEL_ORDER_ID));
				feedDetail.setChannelName(crs.getString(CHANNEL_NAME));
				feedDetail.setStatusCode(crs.getString(STATUS_CODE));
				feedDetail.setCancelReason(crs.getString(CANCEL_REASON));
				feedDetail.setMerchantOrderID(crs.getString(MASTER_ORDER_NUMBER));	
				feedDetail.setMerchantOrderItemId(crs.getString(CONFIRMATION_NUMBER));
				feedDetail.setChannelOrderItemId(crs.getString(MRCNT_CHANNEL_ORDER_ITEM_ID));
				feedList.add(feedDetail);
			}
			return feedList;
			
		} catch(Exception e) {
			throw new MercentException(e.getMessage());
		}
	}
}
