package com.ftd.mercent.dao;

import java.math.BigDecimal;
import java.sql.Clob;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.Marshaller;

import org.apache.commons.lang.StringUtils;

import com.ftd.mercent.exceptions.MercentException;
import com.ftd.mercent.utilities.MercentConstants;
import com.ftd.mercent.utilities.MercentUtils;
import com.ftd.mercent.vo.MercentChannelMappingVO;
import com.ftd.mercent.vo.MercentOrderDetailVO;
import com.ftd.mercent.vo.ftd.FTDOrder;
import com.ftd.mercent.vo.mercent.MercentFeed;
import com.ftd.mercent.vo.mercent.Order;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.StateMasterVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

public class MercentOrderFeedDAO 
{
	private Logger logger = new Logger("com.ftd.mercent.dao.MercentOrderFeedDAO");
	private Connection conn;

	public MercentOrderFeedDAO(Connection conn) {
		this.conn = conn;
	}

	@SuppressWarnings("unchecked")
	public String insertMercentOrderReport(MercentFeed mercentFeed) 
	{
		logger.debug("Saving MercentOrderFeed XML into DB..");
		
		Map<String, Object> props = new HashMap<String, Object>();
		props.put(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		props.put(Marshaller.JAXB_NO_NAMESPACE_SCHEMA_LOCATION,"Mercent_Standard_Order.xsd");
		
		String orderReport = MercentUtils.marshallAsXML(mercentFeed, props);

		logger.debug("OrderReport XML :\n"+orderReport);
		
		Map<String,Object> inputParams = new HashMap<String,Object>();
		
		inputParams.put("IN_ORDER_REPORT", orderReport);

		DataRequest request = new DataRequest();
		request.reset();
		request.setConnection(conn);
		request.setInputParams(inputParams);
		request.setStatementID("INSERT_MRCNT_ORDER_REPORT");
		
		String orderReportId = null;
		
		Map<String,Object> result;
		try 
		{
			DataAccessUtil dau = DataAccessUtil.getInstance();
			result = (Map<String,Object>) dau.execute(request);
			String status = (String) result.get("OUT_STATUS");
			logger.info("status: " + status);
			if (status == null || status.equalsIgnoreCase("N")) 
			{
				String message = (String) result.get("OUT_MESSAGE");
				throw new MercentException(message);
			}
			orderReportId = (String) result.get("OUT_ORDER_REPORT_ID");
			logger.debug("OrderReport Id :"+orderReportId);
		} catch (Exception e) {
			throw new MercentException(e);
		}
		return orderReportId;
	}
	
	@SuppressWarnings("unchecked")
	public String insertMercentOrder(String reportId, Order mercentOrder)
	{
		logger.debug("insertMercentOrder()");
		MercentChannelMappingVO channelMappingVO = getMercentChannelMapping(mercentOrder.getChannelName().toUpperCase());
		String monPrefix = channelMappingVO.getMasterOrderNoPrefix();
		logger.debug("MasterOrderNumber Prefix :"+monPrefix);
		String channelOrderID = mercentOrder.getChannelOrderID();
		String mercentOrderNumber = monPrefix+"_"+channelOrderID;
		String masterOrderNumber = monPrefix + StringUtils.replaceChars(channelOrderID,"-", "");
		
		String mercentOrderString =MercentUtils.marshallAsXML(mercentOrder, null); 
		String orderStatus = "RECEIVED";

	    DataRequest request = new DataRequest();
	    request.reset();
	    request.setConnection(conn);
	    HashMap inputParams = new HashMap();
	    inputParams.put("IN_MRCNT_ORDER_NUMBER", mercentOrderNumber);
	    inputParams.put("IN_CHANNEL_NAME", mercentOrder.getChannelName());
	    inputParams.put("IN_CHANNEL_ORDER_ID", mercentOrder.getChannelOrderID());	    
	    inputParams.put("IN_MASTER_ORDER_NUMBER", masterOrderNumber);
	    inputParams.put("IN_MRCNT_ORDER_REPORT_ID", reportId);
	    inputParams.put("IN_MRCNT_XML", mercentOrderString);
	    inputParams.put("IN_FTD_XML", null);
	    Date date = mercentOrder.getOrderDate().toGregorianCalendar().getTime();
	    java.sql.Date sqlDate = new java.sql.Date(date.getTime());
	    inputParams.put("IN_ORDER_DATE", sqlDate);
	    inputParams.put("IN_ORDER_STATUS", orderStatus);
	    inputParams.put("IN_CURRENCY_CODE", MercentConstants.MERCENT_CURRENCY_CODE_USD);
	    
	    request.setInputParams(inputParams);
	    request.setStatementID("INSERT_MRCNT_ORDER");


	    Map<String,Object> result = null;
	    try 
		{
	    	DataAccessUtil dau = DataAccessUtil.getInstance();
			result = (Map<String,Object>) dau.execute(request);
			
		    String status = (String) result.get("OUT_STATUS");
		    logger.info("status: " + status);
		    if (status == null || status.equalsIgnoreCase("N"))
		    {
		      String message = (String) result.get("OUT_MESSAGE");
		      throw new Exception(message);
		    }
		} 
	    catch (Exception e) 
	    {
	    	throw new MercentException(e);
		}
	    return mercentOrderNumber;
	  }

	public Order getMercentOrder(String mercentOrderNumber) 
	{
		logger.debug("Get Mercent Order :"+mercentOrderNumber);
		
	      DataRequest request = new DataRequest();
	      request.reset();
	      request.setConnection(conn);
	      HashMap inputParams = new HashMap();
	      inputParams.put("IN_MRCNT_ORDER_NUMBER", mercentOrderNumber);        
	      request.setInputParams(inputParams);
	      request.setStatementID("GET_MRCNT_ORDER_XML");

	      String mercentXML = null;
	      try 
	      {
				CachedResultSet results = (CachedResultSet) DataAccessUtil.getInstance().execute(request);
				mercentXML = "";
				if(results != null && results.next()) 
				{
					Clob xmlCLOB = results.getClob("MRCNT_XML");
					mercentXML = xmlCLOB.getSubString(1L, (int) xmlCLOB.length());
				}
	      } catch (Exception e) {
	    	  throw new MercentException(e);
	      }
	      logger.info("mercentXML: " + mercentXML);
	      if(mercentXML != null){
	    	  Order order = (Order) MercentUtils.marshallAsObject(mercentXML, Order.class);
	    	  return order;
	      }
	      
		return null;
	}

	public void saveFTDOrderXML(String mercentOrderNumber, FTDOrder ftdOrder) 
	{
		logger.debug("Updating Mercent Order :"+mercentOrderNumber+" to store FTDOrder XML");
		
		String ftd_xml = MercentUtils.marshallAsXML(ftdOrder, null);
		HashMap inputParams = new HashMap();
	    inputParams.put("IN_MRCNT_ORDER_NUMBER", mercentOrderNumber);
	    inputParams.put("IN_FTD_XML", ftd_xml);
	    inputParams.put("IN_ORDER_STATUS", "TRANSFORMED");
	
	    DataRequest request = new DataRequest();
	    request.reset();
	    request.setConnection(conn);
	    request.setInputParams(inputParams);
	    request.setStatementID("SAVE_FTD_ORDER_XML");
	
	    try {
			DataAccessUtil dau = DataAccessUtil.getInstance();

			Map result = (Map) dau.execute(request);

			String status = (String) result.get("OUT_STATUS");
			logger.info("status: " + status);
			if (status == null || status.equalsIgnoreCase("N"))
			{
			  String message = (String) result.get("OUT_MESSAGE");
			  throw new Exception(message);
			}
		}catch (Exception e) {
			throw new MercentException(e);
		}
	}
	
	public void insertMercentOrderDetails(String mercentOrderNumber, MercentOrderDetailVO orderDetailVO) 
	{
		Map<String,Object> inputParams = new HashMap<String,Object>();
	    inputParams.put("IN_MRCNT_ORDER_ITEM_NUMBER", orderDetailVO.getMercentItemNumber());
	    inputParams.put("IN_MRCNT_ORDER_NUMBER", orderDetailVO.getMercentOrderNumber());
	    inputParams.put("IN_MRCNT_CHANNEL_ORDER_ITEMID", orderDetailVO.getMercentChannelOrderItemId());
	    inputParams.put("IN_CONFIRMATION_NUMBER", orderDetailVO.getConfirmationNumber());
	    inputParams.put("IN_MERCENT_PRODUCT_ID", orderDetailVO.getMercentProductId());
	    inputParams.put("IN_PRINCIPAL_AMT", orderDetailVO.getPrincipalAmount());
	    inputParams.put("IN_SHIPPING_AMT", orderDetailVO.getShippingAmount());
	    inputParams.put("IN_TAX_AMT", orderDetailVO.getTaxAmount());
	    inputParams.put("IN_SHIPPING_TAX_AMT", orderDetailVO.getShippingTaxAmount());
	
	    DataRequest request = new DataRequest();
	    request.reset();
	    request.setConnection(conn);
	    request.setInputParams(inputParams);
	    request.setStatementID("SAVE_MRCNT_ORDER_DETAILS");
	
	    try {
			DataAccessUtil dau = DataAccessUtil.getInstance();

			Map result = (Map) dau.execute(request);

			String status = (String) result.get("OUT_STATUS");
			logger.info("saveMercentOrderDetailsInfo status: " + status);
			if (status == null || status.equalsIgnoreCase("N"))
			{
				StringBuilder message = new StringBuilder("Unable to insert MercentOrderDetails into MERCENT.MRCNT_ORDER_DETAIL"); 
				message.append((String) result.get("OUT_MESSAGE"));
				throw new Exception(message.toString());
			}
		} catch (Exception e) {
			throw new MercentException(e);
		}
	}
	
	public String getConfirmationNumber(String confPrefix)
	{
		logger.debug("getConfirmationNumber()");
		String confNumber = "";
		try {
			DataRequest request = new DataRequest();
			request.reset();
			request.setConnection(conn);
			request.setStatementID("GET_ORDER_CONFIRMATION_NUMBER");

			CachedResultSet results = 
				(CachedResultSet) DataAccessUtil.getInstance().execute(request);
			
			while (results != null && results.next()) {
				confNumber = confPrefix+results.getString("confirmation_number");
			}
		}catch (Exception e) {
			e.printStackTrace();
			throw new MercentException(e);
		}
		logger.info("confNumber: " + confNumber);
		return confNumber;
	}

	public String getMercentBuyerSequence() 
	{
		String sequence = null;
		logger.debug("getMercentBuyerSequence()");
		
		try {
			DataRequest request = new DataRequest();
			request.reset();
			request.setConnection(conn);		
			HashMap inputParams = new HashMap();
			request.setInputParams(inputParams);
			request.setStatementID("GET_AMAZON_BUYER_SEQUENCE");
			DataAccessUtil dau = DataAccessUtil.getInstance();
			Map result = (Map) dau.execute(request);

			sequence = (String) result.get("OUT_BUYER_SEQUENCE");
			String status = (String) result.get("OUT_STATUS");
			logger.info(" ****BuyerSquence StoreProc Status: " + status);
			if (status == null || status.equalsIgnoreCase("N"))
			{
				String message = (String) result.get("OUT_MESSAGE");
				throw new Exception(message);
			}
		}  catch (Exception e) {
			throw new MercentException(e);
		}					
		return sequence;
	}
	
	public Map getIOTWSourceCode(String sourceCode, String itemSku, String dateReceived)
	{
		HashMap iotwMap = new HashMap();
		logger.debug("getIOTWSourceCode()");

	    String iotwSourceCode;
		BigDecimal discount;
		String discountType;
		try {
			DataRequest request = new DataRequest();
			  request.reset();
			  request.setConnection(conn);
			  HashMap inputParams = new HashMap();
			  inputParams.put("IN_SOURCE_CODE", sourceCode);
			  inputParams.put("IN_PRODUCT_ID", itemSku);
			  inputParams.put("IN_TIME_RECEIVED", dateReceived);
			  request.setInputParams(inputParams);
			  request.setStatementID("GET_IOTW_SOURCE_CODE");

			  CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(request);
			  
			  
			  iotwSourceCode = sourceCode;
			  discount = new BigDecimal("0");
			  discountType = "";
			  while ( results != null && results.next() ) 
			  {
				  iotwSourceCode = results.getString("IOTW_SOURCE");
				  discount = results.getBigDecimal("DISCOUNT");
				  discountType = results.getString("DISCOUNT_TYPE");
			  }
			  iotwMap.put("iotwSourceCode", iotwSourceCode);
		      iotwMap.put("discount", discount);
		      iotwMap.put("discountType", discountType);
		      logger.info("iotwSourceCode: " + iotwSourceCode);
		      
		} catch (Exception e) {
			throw new MercentException(e);
		}
	      
		return iotwMap;
	}
	
	public Map getPdbPriceData(String sku)
	{
		HashMap pdbPriceMap = new HashMap();
		  logger.debug("getPdbPriceData()");

	      try {
			DataRequest request = new DataRequest();
			  request.reset();
			  request.setConnection(conn);
			  HashMap inputParams = new HashMap();
			  inputParams.put("IN_PRODUCT_ID", sku);        
			  request.setInputParams(inputParams);
			  request.setStatementID("GET_PDB_DATA");

			  CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(request);
			  
			  while ( results != null && results.next() ) 
			  {
				  pdbPriceMap.put("standardPrice", results.getBigDecimal("STANDARD_PRICE"));
				  pdbPriceMap.put("deluxePrice", results.getBigDecimal("DELUXE_PRICE"));
				  pdbPriceMap.put("premiumPrice", results.getBigDecimal("PREMIUM_PRICE"));
				  pdbPriceMap.put("shipMethodFlorist", results.getString("SHIP_METHOD_FLORIST"));
				  pdbPriceMap.put("shipMethodCarrier", results.getString("SHIP_METHOD_CARRIER"));
			  }
		} catch (Exception e) {
			throw new MercentException(e);
		} 
	      
	      return pdbPriceMap;
	}
	
	public Map getProductByTimestamp(String productId, Date priceDateTime)
	{		
		HashMap productPriceMap = new HashMap();
        // Query the historical product data.
		logger.debug("getProductByTimestamp()");

		try 
		{
			DataRequest request = new DataRequest();
			request.setConnection(conn);
			request.setStatementID("GET_PRODUCT_DETAILS_BY_TIMESTAMP");
			request.addInputParam("IN_PRODUCT_ID", productId);
			request.addInputParam("IN_DATE_TIME", new java.sql.Timestamp(priceDateTime.getTime()));
			DataAccessUtil dau = DataAccessUtil.getInstance();
			CachedResultSet rs = (CachedResultSet)dau.execute(request);
			rs.reset();
			request.reset();
			
			// Only one record (or none) should return.
			if (rs.next()) 
			{
			    // Record the historical standard price.
				BigDecimal standardPrice = rs.getBigDecimal("standard_price");
				productPriceMap.put("standardPrice", standardPrice);
			    // Historical premium and deluxe prices.
				BigDecimal deluxePrice = rs.getBigDecimal("deluxe_price");
				productPriceMap.put("deluxePrice", deluxePrice);
				BigDecimal premiumPrice = rs.getBigDecimal("premium_price");
				productPriceMap.put("premiumPrice", premiumPrice);                        
			}
		} catch (Exception e) {
			throw new MercentException(e);
		} 
        
        return productPriceMap;
	}
	
	public StateMasterVO getStateByName(String state) {
		logger.debug("getStateByName(" + state + ")");
		StateMasterVO stateVO = new StateMasterVO();

	    try {
			DataRequest request = new DataRequest();
			request.reset();
			request.setConnection(conn);
			HashMap inputParams = new HashMap();
			inputParams.put("IN_STATE_NAME", state);        
			request.setInputParams(inputParams);
			request.setStatementID("GET_STATE_BY_NAME");

			CachedResultSet rs = (CachedResultSet)DataAccessUtil.getInstance().execute(request);
			while (rs.next()) {
				stateVO = new StateMasterVO();
				stateVO.setStateMasterId(rs.getString("state_master_id"));
				stateVO.setStateName(rs.getString("state_name"));
				stateVO.setCountryCode(rs.getString("country_code"));
			}
		} catch (Exception e) {
			throw new MercentException(e);
		} 
		return stateVO;
	}
	
	public void updateOrderStatus(String mercentOrderNumber, String orderStatus)
	{
		logger.debug("updateOrderStatus()");
		
	    try {
			HashMap inputParams = new HashMap();
			inputParams.put("IN_MRCNT_ORDER_NUMBER", mercentOrderNumber);
			inputParams.put("IN_ORDER_STATUS", orderStatus);

			DataRequest request = new DataRequest();
			request.reset();
			request.setConnection(conn);
			request.setInputParams(inputParams);
			request.setStatementID("UPDATE_MRCNT_ORDER_STATUS");

			DataAccessUtil dau = DataAccessUtil.getInstance();

			Map result = (Map) dau.execute(request);

			String status = (String) result.get("OUT_STATUS");
			logger.info("status: " + status);
			if (status == null || status.equalsIgnoreCase("N"))
			{
			  String message = (String) result.get("OUT_MESSAGE");
			  throw new MercentException(message);
			}
		}  catch (Exception e) {
			throw new MercentException(e);
		}
		
	}

	public boolean isDuplicateOrder(Order order) 
	{
		boolean duplicate = true;
		try {
			HashMap inputParams = new HashMap();
			inputParams.put("IN_CHANNEL_NAME", order.getChannelName());
			inputParams.put("IN_CHANNEL_ORDER_ID", order.getChannelOrderID());

			DataRequest request = new DataRequest();
			request.reset();
			request.setConnection(conn);
			request.setInputParams(inputParams);
			request.setStatementID("CHECK_MRCNT_ORDER_EXISTS");

			DataAccessUtil dau = DataAccessUtil.getInstance();

			Map result = (Map) dau.execute(request);

			String status = (String) result.get("OUT_STATUS");
			logger.info("status: " + status);
			if (status == null || status.equalsIgnoreCase("N"))
			{
			  String message = (String) result.get("OUT_MESSAGE");
			  throw new MercentException(message);
			}else{
				String exists = (String) result.get("OUT_EXISTS");
				duplicate = "Y".equalsIgnoreCase(exists);
			}
		}  catch (Exception e) {
			throw new MercentException(e);
		}
		return duplicate;
	}
	
	public boolean isDuplicateOrderItem(String mrcntOrderItemNumber)
	{
		boolean duplicate = true;
		try {
			HashMap inputParams = new HashMap();
			inputParams.put("IN_MRCNT_ORDER_ITEM_NUMBER", mrcntOrderItemNumber);
			DataRequest request = new DataRequest();
			request.reset();
			request.setConnection(conn);
			request.setInputParams(inputParams);
			request.setStatementID("CHECK_MRCNT_ORDER_ITEM_EXISTS");

			DataAccessUtil dau = DataAccessUtil.getInstance();

			Map result = (Map) dau.execute(request);

			String status = (String) result.get("OUT_STATUS");
			logger.info("isDuplicateOrderItem status: " + status);
			if (status == null || status.equalsIgnoreCase("N"))
			{
			  String message = (String) result.get("OUT_MESSAGE");
			  throw new MercentException(message);
			}else{
				String exists = (String) result.get("OUT_EXISTS");
				duplicate = "Y".equalsIgnoreCase(exists);
			}
		}  catch (Exception e) {
			throw new MercentException(e);
		}
		return duplicate;
	}
	public boolean isValidMercentChannelName(String channelName) 
	{
		if(channelName==null || channelName.trim().length()==0){
			return false;
		}
		MercentChannelMappingVO channelMappingVO = getMercentChannelMapping(channelName);
		return channelMappingVO!=null && 
				channelMappingVO.getFtdOriginMapping()!=null && 
				channelMappingVO.getFtdOriginMapping().trim().length() > 0;
	}
	
	public MercentChannelMappingVO getMercentChannelMapping(String channelName)
	{
		MercentChannelMappingVO channelMappingVO=new MercentChannelMappingVO();
		
		 try {
			 	if(channelName != null){
			 		channelName = channelName.trim().toUpperCase();
			 	}
				DataRequest request = new DataRequest();
				request.reset();
				request.setConnection(conn);
				HashMap inputParams = new HashMap();
				inputParams.put("IN_CHANNEL_NAME", channelName);
				request.setInputParams(inputParams);
				request.setStatementID("GET_MRCNT_CHANNEL_MAPPING");

				Map result = (Map)DataAccessUtil.getInstance().execute(request);
				
				if(result != null) 
				{
					String status = (String) result.get("OUT_STATUS");
					if(!"Y".equalsIgnoreCase(status))
					{
						String message = (String) result.get("OUT_MESSAGE");
						throw new MercentException(message);
					}
					CachedResultSet crs = (CachedResultSet) result.get("OUT_CUR");
					if(crs.next())
					{
						channelMappingVO.setChannelName(crs.getString("CHANNEL_NAME"));
						channelMappingVO.setChannelImage(crs.getString("CHANNEL_IMAGE"));
						channelMappingVO.setFtdOriginMapping(crs.getString("FTD_ORIGIN_MAPPING"));
						channelMappingVO.setMasterOrderNoPrefix(crs.getString("MASTER_ORDER_NO_PREFIX"));
						channelMappingVO.setConfNumberPrefix(crs.getString("CONF_NUMBER_PREFIX"));
						channelMappingVO.setDefSrcCode(crs.getString("DEF_SRC_CODE"));
						channelMappingVO.setDefRecalcSrcCode(crs.getString("DEF_RECALC_SRC_CODE"));
					}
				}
			} catch (Exception e) {
				throw new MercentException(e);
			} 
		
		return channelMappingVO;
	}
	
	public void insertMercentOrderAcknowledgement(String confirmationNumber, 
													String orderStatus,
													String cancelReason) 
	{
		Map<String,Object> inputParams = new HashMap<String,Object>();
	    inputParams.put("IN_CONFIRMATION_NUMBER", confirmationNumber);
	    inputParams.put("IN_STATUS", orderStatus);
	    inputParams.put("IN_CANCEL_REASON", cancelReason);
	
	    DataRequest request = new DataRequest();
	    request.reset();
	    request.setConnection(conn);
	    request.setInputParams(inputParams);
	    request.setStatementID("INSERT_MERCENT_ACKNOWLEDGEMENT");
	
	    try {
			DataAccessUtil dau = DataAccessUtil.getInstance();

			Map result = (Map) dau.execute(request);

			String status = (String) result.get("OUT_STATUS");
			logger.info("insertMercentOrderAcknowledgement status: " + status);
			if (status == null || status.equalsIgnoreCase("N"))
			{
				StringBuilder message = new StringBuilder("Unable to insert MercentOrderAcknowledgement into MRCNT_ORDER_ACKNOWLEDGEMENT"); 
				message.append((String) result.get("OUT_MESSAGE"));
				throw new Exception(message.toString());
			}
		} catch (Exception e) {
			throw new MercentException(e);
		}
	}

	public Map<String, FTDOrder> getFTDOrderXmlsByStatus(String orderStatus) 
	{
		logger.debug("getFTDOrderXmlsByStatus()");
		
		Map<String, FTDOrder> ordersMap = new HashMap<String, FTDOrder>();
		
	      DataRequest request = new DataRequest();
	      request.reset();
	      request.setConnection(conn);
	      HashMap inputParams = new HashMap();
	      inputParams.put("IN_ORDER_STATUS", orderStatus);
	      request.setInputParams(inputParams);
	      request.setStatementID("GET_FTD_ORDER_XMLS_BY_STATUS");

	      try 
	      {
	    	  CachedResultSet crs = (CachedResultSet)DataAccessUtil.getInstance().execute(request);
				
				while(crs.next())
				{
					String mercentOrderNumber = crs.getString("MRCNT_ORDER_NUMBER");
					FTDOrder ftdOrder = null;
					Clob xmlCLOB = crs.getClob("FTD_XML");
					String ftdXML = xmlCLOB.getSubString(1L, (int) xmlCLOB.length());
				    if(ftdXML != null){
				    	ftdOrder = (FTDOrder) MercentUtils.marshallAsObject(ftdXML, FTDOrder.class);
				    }
				    ordersMap.put(mercentOrderNumber, ftdOrder);
				}
				
	      } catch (Exception e) {
	    	  throw new MercentException(e);
	      }	      
		return ordersMap;
	}
}