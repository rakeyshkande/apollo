/**
 * 
 */
package com.ftd.mercent.dao;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ftd.mercent.exceptions.MercentException;
import com.ftd.mercent.utilities.MercentConstants;
import com.ftd.mercent.utilities.MercentUtils;
import com.ftd.mercent.vo.OrderFulfillmentFeedVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * @author smeka
 *
 */
public class MercentOrderFulfillmentDAO extends MercentFeedDAO {
	private Logger logger = new Logger("com.ftd.mercent.dao.MercentOrderFulfillmentDAO");

	public MercentOrderFulfillmentDAO(Connection conn) {
		this.conn = conn;
	}
	
	/** Gets the fulfillment feed data for given status.
	 * @param feedStatus
	 * @throws MercentException 
	 */
	public List<OrderFulfillmentFeedVO> getMercentFeedData(String feedStatus) {
		if(logger.isDebugEnabled()) {
			logger.debug("********* Entering getMercentFeedData() *********");
		}
		
		try {
			List<OrderFulfillmentFeedVO> feedList = new ArrayList<OrderFulfillmentFeedVO>();

			DataRequest dataRequest = new DataRequest();
			dataRequest.setConnection(this.conn);
			dataRequest.setStatementID(GET_MRCNT_ORD_FULFILL_FEED_BY_STATUS_STMT);

			Map<String, Object> inputParams = new HashMap<String, Object>();
			inputParams.put(IN_FEED_STATUS, feedStatus);
			dataRequest.setInputParams(inputParams);

			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			CachedResultSet crs = (CachedResultSet) dataAccessUtil
					.execute(dataRequest);
			while (crs.next()) {
				OrderFulfillmentFeedVO feedDetail = new OrderFulfillmentFeedVO();
				
				feedDetail.setFeedType(MercentConstants.MRCNT_ORDER_FULFILL_FEED);
		    	feedDetail.setOrderFulfillmentId(crs.getString(MRCNT_ORDER_FULFILLMENT_ID));
		    	
		    	feedDetail.setMerchantOrderID(crs.getString(MASTER_ORDER_NUMBER));
		    	feedDetail.setChannelOrderID(crs.getString(CHANNEL_ORDER_ID));
		    	feedDetail.setChannelName(crs.getString(CHANNEL_NAME));
		    	
		    	feedDetail.setShippingMethod(crs.getString(SHIPPING_METHOD));
				feedDetail.setCarrier(crs.getString(CARRIER_NAME));
				feedDetail.setShippingTrackingNumber(crs.getString(TRACKING_NUMBER));
				
				feedDetail.setChannelOrderItemId(crs.getString(MRCNT_CHANNEL_ORDER_ITEM_ID));
		    	feedDetail.setItemSKU(crs.getString(ITEM_SKU));
		    	feedDetail.setMerchantOrderItemId(crs.getString(CONFIRMATION_NUMBER));
				
		    	
		    	if(crs.getString(FULFILLMENT_DATE) != null) {
		    	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-dd-MM hh:mm:ss");	//2013-27-03 11:19:31	    	   
		    	    Calendar c  = Calendar.getInstance();
		    	    c.setTime(sdf.parse(crs.getString(FULFILLMENT_DATE)));
		    		feedDetail.setFulfillmentDate(MercentUtils.toXMLGregorianCalendar(c.getTime()));
		    	}
				feedList.add(feedDetail);
			}
			return feedList;
			
		} catch(Exception e) {
			throw new MercentException(e.getMessage());
		}
	}
	
	public void insertFloristFulfilledOrders() throws Exception {
		DataRequest request = new DataRequest();
		request.reset();
		request.setConnection(this.conn);
		request.setStatementID("INSERT_FLORIST_FULFILLED_ORDERS");
		DataAccessUtil dau = DataAccessUtil.getInstance();
		Map result = (Map) dau.execute(request);

		String status = (String) result.get("OUT_STATUS");
		logger.info("status: " + status);
		if (status == null || status.equalsIgnoreCase("N"))
		{
			String message = (String) result.get("OUT_MESSAGE");
			throw new Exception(message);
		}
	}
	
	/** Get the mercent orders delivered for which 
	 * 		the partner has SEND_CONFIRMATION_FLAG as Y, 
	 * 		and the thank you email is not already sent. 
	 * @return
	 * @throws Exception
	 */
	public Map<String, List<String>> getOrdersDeliveredToday() throws Exception {
		if(logger.isDebugEnabled()) {
			logger.debug("********* Entering getOrderDeliveredToday() *********");
		}
		DataRequest request = new DataRequest();
		request.reset();
		request.setConnection(this.conn);
		request.setStatementID("GET_ORDERS_DELIVERED_TODAY");
		DataAccessUtil dau = DataAccessUtil.getInstance();	
		
		@SuppressWarnings("unchecked")
		Map<String, Object> result = (Map<String, Object>) dau.execute(request);

		String status = (String) result.get("OUT_STATUS");
		logger.info("getOrderDeliveredToday status: " + status);
		if (status == null || status.equalsIgnoreCase("N")) {
			throw new Exception((String) result.get("OUT_MESSAGE"));
		}
		
		CachedResultSet crs = (CachedResultSet) result.get("OUT_ORDERS_CUR");
		Map<String, List<String>> deliveredOrders = new HashMap<String, List<String>>();
		while(crs.next()) {			
			if(deliveredOrders.get(crs.getString("order_guid")) == null) {
				deliveredOrders.put(crs.getString("order_guid"), new ArrayList<String>());
			}			
			deliveredOrders.get(crs.getString("order_guid")).add(crs.getString("mrcnt_order_item_number"));			
		}		
		logger.debug("Mercent order count delivered today: " + deliveredOrders.size());
		return deliveredOrders;
	}

	/** Update the mercent order detail to set the confirmation_email_sent flag
	 * @param ordersSuccess
	 * @param status
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public void updateConfirmationStatus(String orderItemNumber, String status) throws Exception {
		if(logger.isDebugEnabled()) {
			logger.debug("********* Entering updateConfirmationStatus() *********");
		}
		
		DataRequest request = new DataRequest();
		request.reset();
		request.setConnection(this.conn);
		request.setStatementID("UPDATE_ORDER_CONF_EMAIL_STATUS");		
		
		Map<String, Object> inputParams = new HashMap<String, Object>();
		inputParams.put("IN_STATUS", status);
		inputParams.put("IN_ORDER_ITEM_NUMBER", orderItemNumber);
		request.setInputParams(inputParams);
		
		DataAccessUtil dau = DataAccessUtil.getInstance();
		Map result = (Map) dau.execute(request);
		
		String outStatus = (String) result.get("OUT_STATUS");		
		if (outStatus == null || outStatus.equalsIgnoreCase("N")) {
			throw new Exception("Unable to update the confirmation email status for the order: " + orderItemNumber);
		}
		logger.debug("Succesfully updated status for the order: " + orderItemNumber);
	}	
}

