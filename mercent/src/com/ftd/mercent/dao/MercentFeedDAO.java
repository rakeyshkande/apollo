package com.ftd.mercent.dao;

import java.sql.Clob;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.ftd.mercent.exceptions.MercentException;
import com.ftd.mercent.utilities.MercentConstants;
import com.ftd.mercent.vo.FTPServerVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

public class MercentFeedDAO {

	protected Connection conn;

	private Logger logger = new Logger("com.ftd.mercent.dao.MercentDAO");

	protected static final String GET_MRCNT_ORD_FULFILL_FEED_BY_STATUS_STMT = "GET_MRCNT_ORD_FULFILL_FEED_BY_STATUS";
	protected static final String GET_MRCNT_ORD_ACK_FEED_BY_STATUS_STMT = "GET_MRCNT_ORD_ACK_FEED_BY_STATUS";
	protected static final String GET_MRCNT_ORD_ADJ_FEED_BY_STATUS_STMT = "GET_MRCNT_ORD_ADJ_FEED_BY_STATUS";
	protected static final String SAVE_FEED_MASTER_STMT = "SAVE_FEED_MASTER";	
	protected static final String SAVE_MRCNT_FEED_STATUS_STMT = "SAVE_MRCNT_FEED_STATUS";	
	protected static final String UPDATE_MRCNT_FEED_ID_STMT = "UPDATE_MRCNT_FEED_ID";
	protected static final String GET_MRCNT_FEEDS_BY_STATUS_STMT = "GET_MRCNT_FEEDS_BY_STATUS";
	protected static final String GET_REFUNDED_ORDER_ITEMS = "GET_REFUNDED_ORDER_ITEMS";

	protected static final String MRCNT_ORDER_ITEM_NUMBER = "MRCNT_ORDER_ITEM_NUMBER";
	protected static final String MRCNT_ORDER_NUMBER = "MRCNT_ORDER_NUMBER";
	protected static final String CONFIRMATION_NUMBER = "CONFIRMATION_NUMBER";
	protected static final String MASTER_ORDER_NUMBER = "MASTER_ORDER_NUMBER";
	protected static final String CHANNEL_ORDER_ID = "CHANNEL_ORDER_ID";
	protected static final String MRCNT_CHANNEL_ORDER_ITEM_ID = "MRCNT_CHANNEL_ORDER_ITEMID";
	protected static final String CHANNEL_NAME = "CHANNEL_NAME";	
	protected static final String MRCNT_ORDER_ACKNOWLEDGEMENT_ID = "MRCNT_ORDER_ACKNOWLEDGEMENT_ID";	
	protected static final String STATUS_CODE = "STATUS_CODE";
	protected static final String CANCEL_REASON = "CANCEL_REASON";		
	protected static final String MRCNT_ORDER_ADJUSTMENT_ID = "MRCNT_ORDER_ADJUSTMENT_ID";
	protected static final String ADJUSTMENT_REASON = "ADJUSTMENT_REASON";
	protected static final String PRINCIPAL_AMT = "PRINCIPAL_AMT";
	protected static final String SHIPPING_AMT = "SHIPPING_AMT";
	protected static final String TAX_AMT = "TAX_AMT";
	protected static final String SHIPPING_TAX_AMT = "SHIPPING_TAX_AMT";
	protected static final String ITEM_SKU = "MERCENT_PRODUCT_ID";
	protected static final String QUANTITY = "QUANTITY";
	protected static final String SHIPPING_METHOD = "SHIPPING_METHOD";
	protected static final String CARRIER_NAME = "CARRIER_NAME";
	protected static final String TRACKING_NUMBER = "TRACKING_NUMBER";
	protected static final String FULFILLMENT_DATE = "FULFILLMENT_DATE";
	protected static final String MRCNT_ORDER_FULFILLMENT_ID = "MRCNT_ORDER_FULFILLMENT_ID";
	public static final String EXTERNAL_ORDER_NUMBER = "EXTERNAL_ORDER_NUMBER";
	public static final String IS_FULLREFUND = "IS_FULLREFUND";

	protected static final String IN_FEED_XML = "IN_MRC_XML";
	protected static final String IN_FEED_ID = "IN_FEED_ID";
	protected static final String IN_FEED_TYPE = "IN_FEED_TYPE";
	protected static final String IN_FEED_STATUS = "IN_FEED_STATUS";
	protected static final String IN_SERVER = "IN_SERVER";
	protected static final String IN_FILENAME = "IN_FILENAME";
	protected static final String IN_MRCNT_ORDER_ITEM_ID = "IN_MRCNT_ORDER_ITEM_ID";
	protected static final String IN_UPDATED_BY = "IN_UPDATED_BY";
	protected static final String IN_FEED_DATA_ID = "IN_FEED_DATA_ID";
	protected static final String IN_FTP_SERVER = "IN_SERVER";
	protected static final String IN_RMT_FILE_NAME = "IN_FILENAME";
	protected static final String IN_MASTER_ORDER_NUMBER = "IN_MASTER_ORDER_NUMBER";
	protected static final String UNIQUE_CONSTRAINT_EXCEPTION = "UNIQUE_CONSTRAINT_EXCEPTION";

	protected static final String OUT_STATUS = "OUT_STATUS";
	protected static final String OUT_MESSAGE = "OUT_MESSAGE";
	protected static final String OUT_FEED_ID = "OUT_FEED_ID";
	protected static final String OUT_FEED_XML  = "FEED_XML";

	public MercentFeedDAO(Connection conn) {
		this.conn = conn;
	}

	public MercentFeedDAO() {

	}	

	/** Gets the feed data for given status.
	 * @param status
	 * @throws Exception 
	 */
	public List<?> getMercentFeedData(String newFeedStatus) {
		return null;
	}


	/** Updates the feed Id for given list of feed data.
	 * Feed data can be order acknowledgment, adjustment, fulfillment. 
	 * @param feedData
	 * @param feedId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public boolean updateMercentFeedId(List<String> mercentFeedDataIds, String feedId, String feedType, String feedStatus) {
		if(logger.isDebugEnabled()) {
			logger.debug("********* Entering updateMercentFeedId() *********");
		}

		try {
			for (String mercentFeedDataId : mercentFeedDataIds) {
				HashMap<String,Object> inputParams = new HashMap<String,Object>();

				inputParams.put(IN_FEED_ID, feedId);
				inputParams.put(IN_FEED_TYPE, feedType);
				inputParams.put(IN_UPDATED_BY, "SYS");
				inputParams.put(IN_FEED_STATUS, feedStatus);

				// The input parameter IN_FEED_DATA_ID is either order acknowledgment id, order adjustment id or order fulfillment id.
				if(logger.isDebugEnabled()) {
					logger.debug("Updating feed Id for mercent order feed data id: " + mercentFeedDataId + " of feed Type : " + feedType);
				}
				inputParams.put(IN_FEED_DATA_ID, mercentFeedDataId);

				DataRequest dataRequest = new DataRequest();
				dataRequest.setConnection(this.conn);
				dataRequest.setStatementID(UPDATE_MRCNT_FEED_ID_STMT);
				dataRequest.setInputParams(inputParams);

				DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

				Map<String, Object> outputs = (Map<String, Object>) dataAccessUtil.execute(dataRequest);

				String status = (String) outputs.get(OUT_STATUS);
				if (status != null && status.equalsIgnoreCase("N")) {
					String message = (String) outputs.get(OUT_MESSAGE);
					throw new SQLException(message);
				}				
			}

			return true;

		} catch(Exception e) {
			throw new MercentException(e.getMessage());
		}
	}

	/** inserts new feed to MRCNT_FEED_MASTER for a given feed type.
	 * @param mercentFeed
	 * @param feedType
	 * @return
	 * @throws MercentException
	 */
	@SuppressWarnings("unchecked")
	public String insertMercentFeed(String mercentFeedXML, String feedType) {
		if(logger.isDebugEnabled()) {
			logger.debug("********* Entering insertMercentFeed() *********");
		}

		try {
			HashMap<String,Object> inputParams = new HashMap<String,Object>();		    

			inputParams.put(IN_FEED_XML, mercentFeedXML);
			inputParams.put(IN_FEED_TYPE, feedType);
			inputParams.put(IN_FEED_STATUS, MercentConstants.NEW_FEED_STATUS);
			DataRequest dataRequest = new DataRequest();
			dataRequest.setConnection(this.conn);
			dataRequest.setStatementID(SAVE_FEED_MASTER_STMT);
			dataRequest.setInputParams(inputParams);

			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

			Map<String, Object> outputs = (Map<String, Object>) dataAccessUtil.execute(dataRequest);

			String status = (String) outputs.get(OUT_STATUS);
			if (status != null && status.equalsIgnoreCase("N")) {
				String message = (String) outputs.get(OUT_MESSAGE);
				throw new SQLException(message);
			}

			return (String) outputs.get(OUT_FEED_ID);

		} catch(Exception e) {
			throw new MercentException(e.getMessage());
		}
	}

	/** Gets the Mercent feeds saved to mrcnt_feed_master table
	 * @param newFeedStatus
	 * @param feedType
	 * @return
	 */
	public Map<String,Clob> getMercentFeedsByStatus(String feedStatus, String feedType) {

		if(logger.isDebugEnabled()) {
			logger.debug("********* Entering getMercentFeedsByStatus() *********");
		}

		try {
			Map<String,Clob> mercentFeeds = new HashMap<String,Clob>();

			DataRequest dataRequest = new DataRequest();
			dataRequest.setConnection(this.conn);
			dataRequest.setStatementID(GET_MRCNT_FEEDS_BY_STATUS_STMT);

			HashMap<String, Object> inputParams = new HashMap<String, Object>();
			inputParams.put(IN_FEED_STATUS, feedStatus);
			inputParams.put(IN_FEED_TYPE, feedType);
			dataRequest.setInputParams(inputParams);

			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			CachedResultSet crs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

			while (crs.next()) {	
				mercentFeeds.put(crs.getString("FEED_ID"),crs.getClob("FEED_XML"));				
			}
			return mercentFeeds;

		} catch(Exception e) {
			throw new MercentException(e.getMessage());
		}		
	}

	/** Updates the feed status for given feedId in Feed master table
	 * @param feedId
	 * @param feedStatus
	 * @param feedType
	 * @param serverVO
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public boolean updateMercentFeedStatus(String feedId, String feedStatus, String feedType, FTPServerVO serverVO) {
		try {

			HashMap<String,Object> inputParams = new HashMap<String,Object>();

			inputParams.put(IN_FEED_ID, feedId);
			inputParams.put(IN_FEED_TYPE, feedType);
			inputParams.put(IN_FEED_STATUS, feedStatus);
			if (serverVO!=null){
				inputParams.put(IN_FTP_SERVER, serverVO.getFtpServerLocation());
				inputParams.put(IN_RMT_FILE_NAME, serverVO.getFeedFileName());
			}
			else{
				inputParams.put(IN_FTP_SERVER, null);
				inputParams.put(IN_RMT_FILE_NAME, null);
			}
			if(logger.isDebugEnabled()) {
				logger.debug("Updating feed status to " + feedStatus + " feed Id: " + feedId + " of feed Type : " + feedType);
			}

			DataRequest dataRequest = new DataRequest();
			dataRequest.setConnection(this.conn);
			dataRequest.setStatementID(SAVE_MRCNT_FEED_STATUS_STMT);
			dataRequest.setInputParams(inputParams);

			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

			Map<String, Object> outputs = (Map<String, Object>) dataAccessUtil.execute(dataRequest);

			String status = (String) outputs.get(OUT_STATUS);
			if (status != null && status.equalsIgnoreCase("N")) {
				String message = (String) outputs.get(OUT_MESSAGE);
				//we should expect this message only for PTN_MERCENT.MRCNT_FEED_MASTER.FILENAME column
				if (message.equals(UNIQUE_CONSTRAINT_EXCEPTION)){
					return false;
				}
				else 
					throw new SQLException(message);
			}
			return true;

		} catch(Exception e) {
			throw new MercentException(e.getMessage());
		}
	}

}
