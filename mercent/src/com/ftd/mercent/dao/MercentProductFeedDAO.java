package com.ftd.mercent.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import com.ftd.mercent.vo.MercentProductAttributesVO;
import com.ftd.mercent.vo.MercentProductFeedDetailVO;
import com.ftd.mercent.vo.MercentProductFeedVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;


/**
 * @author sbring
 *
 */
public class MercentProductFeedDAO {

	private Logger logger = new Logger("com.ftd.mercent.bo.MercentProductFeedDAO");

	private Connection conn;
	public MercentProductFeedDAO(Connection conn) {
		this.conn = conn;
	}

	public MercentProductFeedVO getProductFeedData(int productsPerFeed) throws Exception
	{
		logger.debug("********getProductFeedData() ******");
		DataRequest dataRequest = new DataRequest();
		dataRequest.setConnection(this.conn);
		dataRequest.setStatementID("GET_NEW_PRODUCT_FEED_DATA");
		
		HashMap<String,Object> inputParams = new HashMap<String,Object>();
		inputParams.put("IN_PRODUCTS_PER_FEED", productsPerFeed);
		dataRequest.setInputParams(inputParams);
		
		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
		CachedResultSet crs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
		MercentProductFeedVO feed = new MercentProductFeedVO();
		while(crs.next())
		{
			MercentProductFeedDetailVO productFeedDetail = new MercentProductFeedDetailVO();
			productFeedDetail.setProductId(crs.getString("PRODUCT_ID"));
			productFeedDetail.setNovatorId(crs.getString("NOVATOR_ID"));
			productFeedDetail.setProductFeedId(crs.getString("MRCNT_PRODUCT_FEED_ID"));
			productFeedDetail.setTitle(crs.getString("PRODUCT_NAME"));
			productFeedDetail.setDescprition(crs.getString("LONG_DESCRIPTION"));
			productFeedDetail.setStandardPrice(crs.getBigDecimal("STANDARD_PRICE"));
			productFeedDetail.setDeluxePrice(crs.getBigDecimal("DELUXE_PRICE"));
			productFeedDetail.setPremiumPrice(crs.getBigDecimal("PREMIUM_PRICE"));
			productFeedDetail.setNoTaxCode(crs.getString("NO_TAX_FLAG"));
			productFeedDetail.setAvailabilityStatus(crs.getString("STATUS"));
			productFeedDetail.setExceptionEndDate(crs.getDate("EXCEPTION_END_DATE"));
			productFeedDetail.setExceptionStartDate(crs.getDate("EXCEPTION_START_DATE"));
			feed.addFeedDetail(productFeedDetail);
		}
		return feed;
	}

	public MercentProductAttributesVO getProductAttributes(String mrcntproductFeedId,String sku) throws Exception
	{
		logger.debug("Get Attributes for ProductId :"+sku);
		MercentProductAttributesVO productAttributes = new MercentProductAttributesVO();

		DataRequest dataRequest = new DataRequest();
		dataRequest.setConnection(this.conn);
		dataRequest.setStatementID("GET_ATTRIBUTES_BY_PRODUCT");

		HashMap<String,Object> inputParams = new HashMap<String,Object>();
		inputParams.put("IN_PRODUCT_ID", sku);
		inputParams.put("IN_MRCNT_PRODUCT_FEED_ID", mrcntproductFeedId);
		dataRequest.setInputParams(inputParams);

		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
		Map outMap = (Map) dataAccessUtil.execute(dataRequest);

		CachedResultSet BULLET_POINTS_CUR = (CachedResultSet) outMap.get("OUT_BULLET_POINTS_CUR");
		CachedResultSet SEARCH_TERMS_CUR = (CachedResultSet) outMap.get("OUT_SEARCH_TERMS_CUR");
		CachedResultSet INTENDED_USE_CUR = (CachedResultSet) outMap.get("OUT_INTENDED_USE_CUR");
		CachedResultSet TARGET_AUDIENCE_CUR = (CachedResultSet) outMap.get("OUT_TARGET_AUDIENCE_CUR");
		CachedResultSet OTHER_ATTRIBUTES_CUR = (CachedResultSet) outMap.get("OUT_OTHER_ATTRIBUTES_CUR");
		CachedResultSet SUBJECT_MATTER_CUR = (CachedResultSet) outMap.get("OUT_SUBJECT_MATTER_CUR");

		while(BULLET_POINTS_CUR.next())
		{
			productAttributes.getBulletPoint().add(BULLET_POINTS_CUR.getString("product_attribute_value"));
		}
		while(SEARCH_TERMS_CUR.next())
		{
			productAttributes.getSearchTerms().add(SEARCH_TERMS_CUR.getString("product_attribute_value"));
		}
		while(INTENDED_USE_CUR.next())
		{
			productAttributes.getIntendedUse().add(INTENDED_USE_CUR.getString("product_attribute_value"));
		}
		while(TARGET_AUDIENCE_CUR.next())
		{
			productAttributes.getMrcTargetAudience().add(TARGET_AUDIENCE_CUR.getString("product_attribute_value"));
		}
		while(OTHER_ATTRIBUTES_CUR.next())
		{
			productAttributes.getMrcOtherAttributes().add(OTHER_ATTRIBUTES_CUR.getString("product_attribute_value"));
		}
		while(SUBJECT_MATTER_CUR.next())
		{
			productAttributes.getMrcSubjectMatter().add(SUBJECT_MATTER_CUR.getString("product_attribute_value"));
		}

		return productAttributes;
	}

	public void saveFeedStatus(MercentProductFeedDetailVO pvo, String feedId) throws Exception{

		logger.debug("saving the Feed Status for Mercent product feed ");
		HashMap<String,Object> inputParams = new HashMap<String,Object>();
		inputParams.put("IN_MRCNT_PRODUCT_FEED_ID", pvo.getProductFeedId());
		inputParams.put("IN_PRODUCT_ID", pvo.getProductId());
		inputParams.put("IN_FEED_STATUS", pvo.getFeedStatus());
		inputParams.put("IN_FEED_ID", feedId);
		inputParams.put("IN_UPDATED_BY", "SYS");

		DataRequest dataRequest = new DataRequest();
		dataRequest.setConnection(this.conn);
		dataRequest.setStatementID("SAVE_PRODUCT_FEED_STATUS");
		dataRequest.setInputParams(inputParams);

		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
		Map outputs = (Map) dataAccessUtil.execute(dataRequest);

		String status = (String) outputs.get("OUT_STATUS");
		if(status != null && status.equalsIgnoreCase("N"))
		{
			String message = (String) outputs.get("OUT_MESSAGE");
			throw new SQLException(message);
		}
	}

	public String saveFeedToFeedMaster(String feedContent, String feedType, String feedStatus) throws Exception{
		logger.debug("saving the Feed Status to Feed Master table ");
		HashMap<String,Object> inputParams = new HashMap<String,Object>();
		inputParams.put("IN_MRC_XML", feedContent);
		inputParams.put("IN_FEED_TYPE",feedType);
		inputParams.put("IN_FEED_STATUS", feedStatus);
		DataRequest dataRequest = new DataRequest();
		dataRequest.setConnection(this.conn);
		dataRequest.setStatementID("SAVE_FEED_MASTER");
		dataRequest.setInputParams(inputParams);

		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
		Map outputs = (Map) dataAccessUtil.execute(dataRequest);

		String status = (String) outputs.get("OUT_STATUS");
		String feedId = (String) outputs.get("OUT_FEED_ID");
		if(status != null && status.equalsIgnoreCase("N"))
		{
			String message = (String) outputs.get("OUT_MESSAGE");
			throw new SQLException(message);
		}
		return feedId;
	}

	public String getProductCompanyMapping(String productId) throws Exception {
		logger.debug("********getProductCompanyMapping() ******");
		HashMap<String,Object> inputParams = new HashMap<String,Object>();
		inputParams.put("IN_PRODUCT_ID", productId);
		DataRequest dataRequest = new DataRequest();
		dataRequest.setConnection(this.conn);
		dataRequest.setStatementID("GET_PRODUCT_COMPANY_MAPPING");
		dataRequest.setInputParams(inputParams);

		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
		String mappingExists = (String) dataAccessUtil.execute(dataRequest);
		return mappingExists;
	}

	public Integer getProductFeedCount() throws Exception{
		logger.debug("********getProductFeedCount() ******");
		DataRequest dataRequest = new DataRequest();
		dataRequest.setConnection(this.conn);
		dataRequest.setStatementID("GET_PRODUCT_FEED_COUNT");

		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
		BigDecimal productCount = (BigDecimal) dataAccessUtil.execute(dataRequest);
		return productCount.intValueExact();
	}

	public String getMerchantCategory(String productId) throws Exception{
		logger.debug("********getMerchantCategory()******");
		HashMap<String,Object> inputParams = new HashMap<String,Object>();
		inputParams.put("IN_PRODUCT_ID", productId);
		DataRequest dataRequest = new DataRequest();
		dataRequest.setConnection(this.conn);
		dataRequest.setInputParams(inputParams);
		dataRequest.setStatementID("GET_ITEM_TYPE");
		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
		return (String)dataAccessUtil.execute(dataRequest);
	}
}
