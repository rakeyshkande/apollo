/**
 * 
 */
package com.ftd.mercent.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ftd.mercent.exceptions.MercentException;
import com.ftd.mercent.utilities.MercentConstants;
import com.ftd.mercent.vo.ItemAdjFeedVO;
import com.ftd.mercent.vo.OrderAdjFeedVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * @author smeka
 *
 */
public class MercentOrderAdjFeedDAO extends MercentFeedDAO {
	
	private Logger logger = new Logger("com.ftd.mercent.dao.MercentOrderAdjFeedDAO");

	public MercentOrderAdjFeedDAO(Connection conn) {
		this.conn = conn;
	}
	
	/** Gets the adjustment feed data for given status.
	 * @param feedStatus
	 * @throws MercentException 
	 */
	public List<OrderAdjFeedVO> getMercentFeedData(String feedStatus) {
		if(logger.isDebugEnabled()) {
			logger.debug("********* Entering getMercentFeedData() *********");
		}
		
		try {
			DataRequest dataRequest = new DataRequest();
			dataRequest.setConnection(this.conn);
			dataRequest.setStatementID(GET_MRCNT_ORD_ADJ_FEED_BY_STATUS_STMT);

			HashMap<String, Object> inputParams = new HashMap<String, Object>();
			inputParams.put(IN_FEED_STATUS, feedStatus);
			dataRequest.setInputParams(inputParams);

			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			CachedResultSet crs = (CachedResultSet) dataAccessUtil
					.execute(dataRequest);
			
			Map<String,OrderAdjFeedVO> orderItemAdjFeeds = new HashMap<String, OrderAdjFeedVO>();
			
			while (crs.next()) {				
				
				OrderAdjFeedVO feedDetail;				
				if(orderItemAdjFeeds.get(crs.getString(CONFIRMATION_NUMBER)) == null) {
					feedDetail = new OrderAdjFeedVO();				
			    	feedDetail.setFeedType(MercentConstants.MRCNT_ORDER_ADJ_FEED);			    		    	
			    	feedDetail.setMerchantOrderID(crs.getString(MASTER_ORDER_NUMBER));
			    	feedDetail.setChannelOrderID(crs.getString(CHANNEL_ORDER_ID));	    	
			    	feedDetail.setMerchantOrderItemId(crs.getString(CONFIRMATION_NUMBER));
			    	feedDetail.setChannelOrderItemId(crs.getString(MRCNT_CHANNEL_ORDER_ITEM_ID));	    	
			    	feedDetail.setChannelName(crs.getString("CHANNEL_NAME"));	
			    	feedDetail.setNoOfAdjs(crs.getInt("NO_OF_ADJUSTMENTS"));
			    	feedDetail.setOrderFullyRefudned("N".equals(crs.getString("IS_ORDER_FULL_REFUND")) ? false : true);
			    	feedDetail.setAdjAlreadySent(crs.getInt("IS_PARTIALLY_SENT") > 0 ? true : false);
			    	orderItemAdjFeeds.put(crs.getString(CONFIRMATION_NUMBER), feedDetail);
				} else {
					feedDetail = orderItemAdjFeeds.get(crs.getString(CONFIRMATION_NUMBER));	
				}
				
		    	ItemAdjFeedVO itemAdjFeedVO = new ItemAdjFeedVO();
		    	itemAdjFeedVO.setOrderAdjId(crs.getString(MRCNT_ORDER_ADJUSTMENT_ID));	
		    	itemAdjFeedVO.setAdjustmentReason(crs.getString(ADJUSTMENT_REASON));		    	
		    	itemAdjFeedVO.setItemSKU(crs.getString(ITEM_SKU));		    	
		    	itemAdjFeedVO.setItemPrincipalAmount(new BigDecimal(crs.getString(PRINCIPAL_AMT)));
		    	itemAdjFeedVO.setItemTaxAmount(new BigDecimal(crs.getString(TAX_AMT)));				
		    	itemAdjFeedVO.setShippingPrincipalAmount(new BigDecimal(crs.getString(SHIPPING_AMT)));
		    	itemAdjFeedVO.setShippingTaxAmount(new BigDecimal(crs.getString(SHIPPING_TAX_AMT)));	
		    	itemAdjFeedVO.setItemRefunded("N".equals(crs.getString("IS_ITEM_FULLREFUND")) ? false : true);
		    	feedDetail.getItemAdjFeeds().add(itemAdjFeedVO);
			}
					
			return new ArrayList<OrderAdjFeedVO>(orderItemAdjFeeds.values());
			
		} catch(Exception e) {
			throw new MercentException(e.getMessage());
		}
	}
	
	
	@SuppressWarnings("rawtypes")
	public void processRefunds() throws Exception
	{
		logger.debug("Entered RefundFeedDAO");
		DataRequest dataRequest = new DataRequest();
		dataRequest.setConnection(this.conn);
		HashMap<String,Object> inputParams = new HashMap<String,Object>();
	    inputParams.put("IN_USER_NAME", "SYS");
		dataRequest.setStatementID("PROCESS_MERCENT_REFUNDS");
		dataRequest.setInputParams(inputParams);
		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
		Map outputs = (Map) dataAccessUtil.execute(dataRequest);
		  
		String status = (String) outputs.get("OUT_STATUS");
		if(status != null && status.equalsIgnoreCase("N"))
		{
			String message = (String) outputs.get("OUT_MESSAGE");
		    throw new SQLException(message);
		}
		logger.debug("End RefundFeedDAO");
	}
}

