package com.ftd.mercent.vo;

import java.math.BigDecimal;

public class ItemAdjFeedVO {
	private String adjustmentReason;	
	private String itemSKU;
	private String itemAdjustmentReason;
	private BigDecimal itemPrincipalAmount;
	private BigDecimal itemTaxAmount;
	private BigDecimal shippingPrincipalAmount;
	private BigDecimal shippingTaxAmount;
	private String orderAdjId;
	private boolean isItemRefunded;
	
	
	public String getAdjustmentReason() {
		return adjustmentReason;
	}
	public void setAdjustmentReason(String adjustmentReason) {
		this.adjustmentReason = adjustmentReason;
	}
	public String getItemSKU() {
		return itemSKU;
	}
	public void setItemSKU(String itemSKU) {
		this.itemSKU = itemSKU;
	}
	public String getItemAdjustmentReason() {
		return itemAdjustmentReason;
	}
	public void setItemAdjustmentReason(String itemAdjustmentReason) {
		this.itemAdjustmentReason = itemAdjustmentReason;
	}
	public BigDecimal getItemPrincipalAmount() {
		return itemPrincipalAmount;
	}
	public void setItemPrincipalAmount(BigDecimal itemPrincipalAmount) {
		this.itemPrincipalAmount = itemPrincipalAmount;
	}
	public BigDecimal getItemTaxAmount() {
		return itemTaxAmount;
	}
	public void setItemTaxAmount(BigDecimal itemTaxAmount) {
		this.itemTaxAmount = itemTaxAmount;
	}
	public BigDecimal getShippingPrincipalAmount() {
		return shippingPrincipalAmount;
	}
	public void setShippingPrincipalAmount(BigDecimal shippingPrincipalAmount) {
		this.shippingPrincipalAmount = shippingPrincipalAmount;
	}
	public BigDecimal getShippingTaxAmount() {
		return shippingTaxAmount;
	}
	public void setShippingTaxAmount(BigDecimal shippingTaxAmount) {
		this.shippingTaxAmount = shippingTaxAmount;
	}	
	public String getOrderAdjId() {
		return orderAdjId;
	}
	public void setOrderAdjId(String orderAdjId) {
		this.orderAdjId = orderAdjId;
	}
	public boolean isItemRefunded() {
		return isItemRefunded;
	}
	public void setItemRefunded(boolean isItemRefunded) {
		this.isItemRefunded = isItemRefunded;
	}
	
}
