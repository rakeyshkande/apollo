package com.ftd.mercent.vo;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/**
 * @author sbring
 *
 */

public class MercentProductFeedDetailVO extends MercentBaseFeedDetailVO{

	private String productFeedId;
	private String productId;
	private String novatorId;
	private String title;
	private String descprition;
	
	private BigDecimal standardPrice;
	private BigDecimal deluxePrice;
	private BigDecimal premiumPrice;
	
	private String noTaxCode;
	private String availabilityStatus;
	private Date exceptionStartDate;
	private Date exceptionEndDate;

	public void setProductFeedId(String productFeedId) {
		this.productFeedId = productFeedId;
	}

	public String getProductFeedId() {
		return productFeedId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductId() {
		return productId;
	}

	public void setNovatorId(String novatorId) {
		this.novatorId = novatorId;
	}

	public String getNovatorId() {
		return novatorId;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

	public void setDescprition(String descprition) {
		this.descprition = descprition;
	}

	public String getDescprition() {
		return descprition;
	}

	public void setStandardPrice(BigDecimal standardPrice) {
		this.standardPrice = standardPrice;
	}

	public BigDecimal getStandardPrice() {
		return standardPrice;
	}

	public void setDeluxePrice(BigDecimal deluxePrice) {
		this.deluxePrice = deluxePrice;
	}

	public BigDecimal getDeluxePrice() {
		return deluxePrice;
	}

	public void setPremiumPrice(BigDecimal premiumPrice) {
		this.premiumPrice = premiumPrice;
	}

	public BigDecimal getPremiumPrice() {
		return premiumPrice;
	}

	public void setNoTaxCode(String noTaxCode) {
		this.noTaxCode = noTaxCode;
	}

	public String getNoTaxCode() {
		return noTaxCode;
	}

	public void setAvailabilityStatus(String availabilityStatus) {
		this.availabilityStatus = availabilityStatus;
	}

	public String getAvailabilityStatus() {
		return availabilityStatus;
	}

	public void setExceptionStartDate(Date exceptionStartDate) {
		this.exceptionStartDate = exceptionStartDate;
	}

	public Date getExceptionStartDate() {
		return exceptionStartDate;
	}

	public void setExceptionEndDate(Date exceptionEndDate) {
		this.exceptionEndDate = exceptionEndDate;
	}

	public Date getExceptionEndDate() {
		return exceptionEndDate;
	}
	
	
}
