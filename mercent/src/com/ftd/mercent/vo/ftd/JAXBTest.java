package com.ftd.mercent.vo.ftd;

import java.io.File;
import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class JAXBTest {

	public static void main(String arg[])
	{
		try 
		{
			JAXBContext context = JAXBContext.newInstance(FTDOrder.class);
			Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

			Unmarshaller unmarshaller = context.createUnmarshaller();
			FTDOrder order=new FTDOrder();
			
			Header header=new Header();
			List<Item> items=new ArrayList<Item>();
			Item item= new Item();
			List<Tax> taxes=new ArrayList<Tax>();
			Tax tax=new Tax();
			AddressVerification addressVerification=new AddressVerification();
			AvsAddressScores avs_address_scores=new AvsAddressScores();
			
			header.setMasterOrderNumber("M1222831999");
			header.setDnisId("1111");
			header.setSourceCode("7001");
			header.setOrderAmount("37.27");
			header.setBuyerDaytimePhone(""+new BigDecimal(1231231234));
			header.setCcNumber("1234567891234567");
			
			item.setOccassion("3");
			item.setOrderNumber("C12228319999");
			item.setItemSourceCode("7001");
			item.setDeliveryDate(""+new Date());
			item.setProductid(""+3072);
			item.setRecipPhone(""+new BigDecimal(1231231234));
			tax.setAmount(1.77);
			taxes.add(tax);
			item.setTaxes(taxes);
			
			addressVerification.setRecipAvsPerformed("Y");
			addressVerification.setRecipAddressVerificationResult("FAIL");	
			addressVerification.setAvsStreetAddress("Woodcreek Dr");
			addressVerification.setAvsLongitude(new BigDecimal(-88.03585815429688));
			addressVerification.setAvsLatitude(new BigDecimal(41.8285026550293));
			avs_address_scores.setAvsScoreMediumConfidence(50);
			avs_address_scores.setAvsScoreAddressLineDifferent(50);
			avs_address_scores.setAvsScoreLowConfidence(0);
			addressVerification.setAvsAddressScores(avs_address_scores);
			
			item.setAddressVerification(addressVerification);
			
			items.add(item);
			
			order.setHeader(header);
			order.setItems(items);
			
			StringWriter sw = new StringWriter();
			marshaller.marshal(order, sw);
			System.out.println(sw.toString());
			
			String ftdOrderXmlFile = "E:\\Projects\\Desktop\\apollo\\order.xml";
			FTDOrder ftdOrder = (FTDOrder) unmarshaller.unmarshal(new File(ftdOrderXmlFile));
			List<Item> items2 = ftdOrder.getItems();
			for (Item item2 : items2) {
				System.err.println("----------->"+item2.getOrderNumber());
				
			}
			FTDOrder unmarshal = (FTDOrder) unmarshaller.unmarshal(new StringReader(sw.toString()));
			System.out.println(unmarshal.getItems().get(0).getOrderNumber());
			System.out.println(unmarshal.getItems().get(0).getTaxes().get(0).getAmount());
			
		} catch (JAXBException e) {
			
			e.printStackTrace();
		} finally {
		}
	}
	
	
}
