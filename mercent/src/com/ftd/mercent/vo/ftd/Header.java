package com.ftd.mercent.vo.ftd;

import java.math.BigDecimal;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class Header {

	@XmlElement(name="master-order-number")
	private String masterOrderNumber;
	@XmlElement(name="dnis-id")
	private String dnisId;
	@XmlElement(name="source-code")
	private String sourceCode;
	@XmlElement(name="origin")
	private String origin="";
	@XmlElement(name="transaction-date")
	private String transactionDate;
	@XmlElement(name="socket-timestamp")
	private String socketTimestamp;
	@XmlElement(name="order-count")
	private String orderCount;
	@XmlElement(name="order-amount")
	private String orderAmount;
	@XmlElement(name="order-created-by-id")
	private String orderCreatedById;
	@XmlElement(name="fraud-flag")
	private String fraudFlag;
	@XmlElement(name="fraud-id")
	private BigDecimal fraudId;
	@XmlElement(name="fraud-comments")
	private String fraudComments;
	@XmlElement(name="language-id")
	private String languageId;
	@XmlElement(name="buyer-first-name")
	private String buyerFirstName;
	@XmlElement(name="buyer-last-name")
	private String buyerLastName;
	@XmlElement(name="buyer-address1")
	private String buyerAddress1;
	@XmlElement(name="buyer-city")
	private String	buyerCity;
	@XmlElement(name="buyer-state")
	private String buyerState;
	@XmlElement(name="buyer-postal-code")
	private String buyerPostalCode;
	@XmlElement(name="buyer-country")
	private String buyerCountry;
	@XmlElement(name="buyer-primary-phone")
	private String buyerDaytimePhone;
	@XmlElement(name="buyer-primary-phone-ext")
	private BigDecimal buyerWorkExt;
	@XmlElement(name="buyer-secondary-phone")
	private BigDecimal buyerEveningPhone;
	@XmlElement(name="buyer-evening-ext")
	private BigDecimal buyerEveningExt;
	@XmlElement(name="buyer-signed-in")
	private String buyerSignedIn;
	@XmlElement(name="buyer-email-address")
	private String buyerEmailAddress;
	@XmlElement(name="news-letter-flag")
	private String newsLetterFlag;
	@XmlElement(name="cc-number")
	private String ccNumber;
	@XmlElement(name="cc-type")
	private String ccType;
	@XmlElement(name="cc-exp-date")
	private Date ccExpDate;
	@XmlElement(name="cc-approval-amt")
	private double ccApprovalAmt;
	@XmlElement(name="cc-approval-code")
	private String ccApprovalCode;
	@XmlElement(name="cc-approval-verbage")
	private String 	ccApprovalVerbage;
	@XmlElement(name="cc-approval-action-code")
	private int ccApprovalActionCode;
	@XmlElement(name="cc-avs-result")
	private String ccAvsResult;
	@XmlElement(name="cc-acq-data")
	private String ccAcqData;
	@XmlElement(name="aafes-ticket-number")
	private BigDecimal aafesTicketNumber;
	@XmlElement(name="csc-validated-flag")
	private String cscValidatedFlag;
	@XmlElement(name="csc-response-code")
	private String cscResponseCode;
	@XmlElement(name="csc-failure-count")
	private int cscFailureCount;
	
	@XmlElement(name="mercent-order-number")
	private String mercentOrderNumber;
	@XmlElement(name="discount-total")
	private String discountTotal;
	
	
	public String getDiscountTotal() {
		return discountTotal;
	}
	public void setDiscountTotal(String discountTotal) {
		this.discountTotal = discountTotal;
	}
	
	public String getMercentOrderNumber() {
		return mercentOrderNumber;
	}
	public void setMercentOrderNumber(String mercentOrderNumber) {
		this.mercentOrderNumber = mercentOrderNumber;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	public String getSocketTimestamp() {
		return socketTimestamp;
	}
	public void setSocketTimestamp(String socketTimestamp) {
		this.socketTimestamp = socketTimestamp;
	}
	public String getMasterOrderNumber() {
		return masterOrderNumber;
	}
	public void setMasterOrderNumber(String masterOrderNumber) {
		this.masterOrderNumber = masterOrderNumber;
	}
	public String getDnisId() {
		return dnisId;
	}
	public void setDnisId(String dnisId) {
		this.dnisId = dnisId;
	}
	public String getSourceCode() {
		return sourceCode;
	}
	public void setSourceCode(String sourceCode) {
		this.sourceCode = sourceCode;
	}
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public String getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}
	public String getOrderCount() {
		return orderCount;
	}
	public void setOrderCount(String orderCount) {
		this.orderCount = orderCount;
	}
	public String getOrderAmount() {
		return orderAmount;
	}
	public void setOrderAmount(String orderAmount) {
		this.orderAmount = orderAmount;
	}
	public String getOrderCreatedById() {
		return orderCreatedById;
	}
	public void setOrderCreatedById(String orderCreatedById) {
		this.orderCreatedById = orderCreatedById;
	}
	public String getFraudFlag() {
		return fraudFlag;
	}
	public void setFraudFlag(String fraudFlag) {
		this.fraudFlag = fraudFlag;
	}
	public BigDecimal getFraudId() {
		return fraudId;
	}
	public void setFraudId(BigDecimal fraudId) {
		this.fraudId = fraudId;
	}
	public String getFraudComments() {
		return fraudComments;
	}
	public void setFraudComments(String fraudComments) {
		this.fraudComments = fraudComments;
	}
	public String getLanguageId() {
		return languageId;
	}
	public void setLanguageId(String languageId) {
		this.languageId = languageId;
	}
	public String getBuyerFirstName() {
		return buyerFirstName;
	}
	public void setBuyerFirstName(String buyerFirstName) {
		this.buyerFirstName = buyerFirstName;
	}
	public String getBuyerLastName() {
		return buyerLastName;
	}
	public void setBuyerLastName(String buyerLastName) {
		this.buyerLastName = buyerLastName;
	}
	public String getBuyerAddress1() {
		return buyerAddress1;
	}
	public void setBuyerAddress1(String buyerAddress1) {
		this.buyerAddress1 = buyerAddress1;
	}
	public String getBuyerCity() {
		return buyerCity;
	}
	public void setBuyerCity(String buyerCity) {
		this.buyerCity = buyerCity;
	}
	public String getBuyerState() {
		return buyerState;
	}
	public void setBuyerState(String buyerState) {
		this.buyerState = buyerState;
	}
	public String getBuyerPostalCode() {
		return buyerPostalCode;
	}
	public void setBuyerPostalCode(String buyerPostalCode) {
		this.buyerPostalCode = buyerPostalCode;
	}
	public String getBuyerCountry() {
		return buyerCountry;
	}
	public void setBuyerCountry(String buyerCountry) {
		this.buyerCountry = buyerCountry;
	}
	public String getBuyerDaytimePhone() {
		return buyerDaytimePhone;
	}
	public void setBuyerDaytimePhone(String buyerDaytimePhone) {
		this.buyerDaytimePhone = buyerDaytimePhone;
	}
	public BigDecimal getBuyerWorkExt() {
		return buyerWorkExt;
	}
	public void setBuyerWorkExt(BigDecimal buyerWorkExt) {
		this.buyerWorkExt = buyerWorkExt;
	}
	public BigDecimal getBuyerEveningPhone() {
		return buyerEveningPhone;
	}
	public void setBuyerEveningPhone(BigDecimal buyerEveningPhone) {
		this.buyerEveningPhone = buyerEveningPhone;
	}
	public BigDecimal getBuyerEveningExt() {
		return buyerEveningExt;
	}
	public void setBuyerEveningExt(BigDecimal buyerEveningExt) {
		this.buyerEveningExt = buyerEveningExt;
	}
	public String getBuyerSignedIn() {
		return buyerSignedIn;
	}
	public void setBuyerSignedIn(String buyerSignedIn) {
		this.buyerSignedIn = buyerSignedIn;
	}
	public String getBuyerEmailAddress() {
		return buyerEmailAddress;
	}
	public void setBuyerEmailAddress(String buyerEmailAddress) {
		this.buyerEmailAddress = buyerEmailAddress;
	}
	public String getNewsLetterFlag() {
		return newsLetterFlag;
	}
	public void setNewsLetterFlag(String newsLetterFlag) {
		this.newsLetterFlag = newsLetterFlag;
	}
	public String getCcNumber() {
		return ccNumber;
	}
	public void setCcNumber(String ccNumber) {
		this.ccNumber = ccNumber;
	}
	public String getCcType() {
		return ccType;
	}
	public void setCcType(String ccType) {
		this.ccType = ccType;
	}
	public Date getCcExpDate() {
		return ccExpDate;
	}
	public void setCcExpDate(Date ccExpDate) {
		this.ccExpDate = ccExpDate;
	}
	public double getCcApprovalAmt() {
		return ccApprovalAmt;
	}
	public void setCcApprovalAmt(double ccApprovalAmt) {
		this.ccApprovalAmt = ccApprovalAmt;
	}
	public String getCcApprovalCode() {
		return ccApprovalCode;
	}
	public void setCcApprovalCode(String ccApprovalCode) {
		this.ccApprovalCode = ccApprovalCode;
	}
	public String getCcApprovalVerbage() {
		return ccApprovalVerbage;
	}
	public void setCcApprovalVerbage(String ccApprovalVerbage) {
		this.ccApprovalVerbage = ccApprovalVerbage;
	}
	public int getCcApprovalActionCode() {
		return ccApprovalActionCode;
	}
	public void setCcApprovalActionCode(int ccApprovalActionCode) {
		this.ccApprovalActionCode = ccApprovalActionCode;
	}
	public String getCcAvsResult() {
		return ccAvsResult;
	}
	public void setCcAvsResult(String ccAvsResult) {
		this.ccAvsResult = ccAvsResult;
	}
	public String getCcAcqData() {
		return ccAcqData;
	}
	public void setCcAcqData(String ccAcqData) {
		this.ccAcqData = ccAcqData;
	}
	public BigDecimal getAafesTicketNumber() {
		return aafesTicketNumber;
	}
	public void setAafesTicketNumber(BigDecimal aafesTicketNumber) {
		this.aafesTicketNumber = aafesTicketNumber;
	}
	public String getCscValidatedFlag() {
		return cscValidatedFlag;
	}
	public void setCscValidatedFlag(String cscValidatedFlag) {
		this.cscValidatedFlag = cscValidatedFlag;
	}
	public String getCscResponseCode() {
		return cscResponseCode;
	}
	public void setCscResponseCode(String cscResponseCode) {
		this.cscResponseCode = cscResponseCode;
	}
	public int getCscFailureCount() {
		return cscFailureCount;
	}
	public void setCscFailureCount(int cscFailureCount) {
		this.cscFailureCount = cscFailureCount;
	}
			
}
