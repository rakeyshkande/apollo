/**
 * 
 */
package com.ftd.mercent.vo;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.ftd.mercent.exceptions.MercentException;
import com.ftd.mercent.vo.mercent.MercentFeed;


/**
 * @author skatam
 *
 */
public class MercentOrderFeed
{
	private MercentFeed mercentFeed;
	
	public MercentOrderFeed() {
	}
	public MercentOrderFeed(MercentFeed mercentFeed) {
		this.mercentFeed = mercentFeed;
	}
	public MercentFeed getMercentFeed() {
		return mercentFeed;
	}
	public String getAsXmlString() 
	{
    	String xml = "";
    	try {
			JAXBContext jaxbContext = JAXBContext.newInstance(MercentFeed.class);
			Marshaller marshaller = jaxbContext.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			marshaller.setProperty(Marshaller.JAXB_NO_NAMESPACE_SCHEMA_LOCATION,"Mercent_Standard_Order.xsd");
			
			StringWriter sw = new StringWriter();
			
			marshaller.marshal(this.mercentFeed, sw);
			xml = sw.toString();
			
		} catch (JAXBException e) {
			throw new MercentException(e);
		}
		return xml;
	}

}
