package com.ftd.mercent.vo;

/**
 * @author smeka
 *
 */
public class OrderAckFeedVO extends MercentBaseFeedDetailVO {	
	private String statusCode;
	private String cancelReason;
	private String orderAckId;
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getCancelReason() {
		return cancelReason;
	}
	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}
	public String getOrderAckId() {
		return orderAckId;
	}
	public void setOrderAckId(String orderAckId) {
		this.orderAckId = orderAckId;
	}			
}
