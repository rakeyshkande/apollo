package com.ftd.mercent.vo;


/**
 * @author smeka
 * 
 */
public class FTPServerVO {
	private String ftpServerLocation;
	private String ftpServerDirectory;
	private String ftpServerLogon;
	private String ftpServerPwd;
	private String feedFileName;
	private String mercentFeedDirectory;
	public String getFtpServerLocation() {
		return ftpServerLocation;
	}
	public void setFtpServerLocation(String ftpServerLocation) {
		this.ftpServerLocation = ftpServerLocation;
	}
	public String getFtpServerDirectory() {
		return ftpServerDirectory;
	}
	public void setFtpServerDirectory(String ftpServerDirectory) {
		this.ftpServerDirectory = ftpServerDirectory;
	}
	public String getFtpServerLogon() {
		return ftpServerLogon;
	}
	public void setFtpServerLogon(String ftpServerLogon) {
		this.ftpServerLogon = ftpServerLogon;
	}
	public String getFtpServerPwd() {
		return ftpServerPwd;
	}
	public void setFtpServerPwd(String ftpServerPwd) {
		this.ftpServerPwd = ftpServerPwd;
	}
	public String getFeedFileName() {
		return feedFileName;
	}
	public void setFeedFileName(String feedFileName) {
		this.feedFileName = feedFileName;
	}
	public String getMercentFeedDirectory() {
		return mercentFeedDirectory;
	}
	public void setMercentFeedDirectory(String mercentFeedDirectory) {
		this.mercentFeedDirectory = mercentFeedDirectory;
	}
	
	
}
