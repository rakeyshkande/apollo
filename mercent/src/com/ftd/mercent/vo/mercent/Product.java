package com.ftd.mercent.vo.mercent;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.NormalizedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder={
	"uniqueIdentifiers",
	"basicInformation",
	"descriptiveInformation",
	"imageInformation",
	"pricingInformation",
	"rankAndRating",
	"productRelationships",
	"availabilityInformation",
	"shippingInformation",
	"channelSpecificInformation",
	"sku"	
})
@XmlRootElement(name = "Product")
public class Product 
{
    @XmlElement(name = "UniqueIdentifiers")
    protected UniqueIdentifiers uniqueIdentifiers;
    @XmlElement(name = "BasicInformation")
    protected BasicInformation basicInformation;
    @XmlElement(name = "DescriptiveInformation")
    protected DescriptiveInformation descriptiveInformation;
    @XmlElement(name = "ImageInformation")
    protected ImageInformation imageInformation;
    @XmlElement(name = "PricingInformation")
    protected PricingInformation pricingInformation;
    @XmlElement(name = "RankAndRating")
    protected RankAndRating rankAndRating;
    @XmlElement(name = "ProductRelationships")
    protected ProductRelationships productRelationships;
    @XmlElement(name = "AvailabilityInformation")
    protected AvailabilityInformation availabilityInformation;
    @XmlElement(name = "ShippingInformation")
    protected ShippingInformation shippingInformation;
    @XmlElement(name = "ChannelSpecificInformation")
    protected ChannelSpecificInformation channelSpecificInformation;
    @XmlAttribute(name = "SKU", required = true)
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    protected String sku;

    /**
     * Gets the value of the uniqueIdentifiers property.
     * 
     * @return
     *     possible object is
     *     {@link UniqueIdentifiers }
     *     
     */
    public UniqueIdentifiers getUniqueIdentifiers() {
        return uniqueIdentifiers;
    }

    /**
     * Sets the value of the uniqueIdentifiers property.
     * 
     * @param value
     *     allowed object is
     *     {@link UniqueIdentifiers }
     *     
     */
    public void setUniqueIdentifiers(UniqueIdentifiers value) {
        this.uniqueIdentifiers = value;
    }

    /**
     * Gets the value of the basicInformation property.
     * 
     * @return
     *     possible object is
     *     {@link BasicInformation }
     *     
     */
    public BasicInformation getBasicInformation() {
        return basicInformation;
    }

    /**
     * Sets the value of the basicInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link BasicInformation }
     *     
     */
    public void setBasicInformation(BasicInformation value) {
        this.basicInformation = value;
    }

    /**
     * Gets the value of the descriptiveInformation property.
     * 
     * @return
     *     possible object is
     *     {@link DescriptiveInformation }
     *     
     */
    public DescriptiveInformation getDescriptiveInformation() {
        return descriptiveInformation;
    }

    /**
     * Sets the value of the descriptiveInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link DescriptiveInformation }
     *     
     */
    public void setDescriptiveInformation(DescriptiveInformation value) {
        this.descriptiveInformation = value;
    }

    /**
     * Gets the value of the imageInformation property.
     * 
     * @return
     *     possible object is
     *     {@link ImageInformation }
     *     
     */
    public ImageInformation getImageInformation() {
        return imageInformation;
    }

    /**
     * Sets the value of the imageInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link ImageInformation }
     *     
     */
    public void setImageInformation(ImageInformation value) {
        this.imageInformation = value;
    }

    /**
     * Gets the value of the pricingInformation property.
     * 
     * @return
     *     possible object is
     *     {@link PricingInformation }
     *     
     */
    public PricingInformation getPricingInformation() {
        return pricingInformation;
    }

    /**
     * Sets the value of the pricingInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link PricingInformation }
     *     
     */
    public void setPricingInformation(PricingInformation value) {
        this.pricingInformation = value;
    }

    /**
     * Gets the value of the rankAndRating property.
     * 
     * @return
     *     possible object is
     *     {@link RankAndRating }
     *     
     */
    public RankAndRating getRankAndRating() {
        return rankAndRating;
    }

    /**
     * Sets the value of the rankAndRating property.
     * 
     * @param value
     *     allowed object is
     *     {@link RankAndRating }
     *     
     */
    public void setRankAndRating(RankAndRating value) {
        this.rankAndRating = value;
    }

    /**
     * Gets the value of the productRelationships property.
     * 
     * @return
     *     possible object is
     *     {@link ProductRelationships }
     *     
     */
    public ProductRelationships getProductRelationships() {
        return productRelationships;
    }

    /**
     * Sets the value of the productRelationships property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductRelationships }
     *     
     */
    public void setProductRelationships(ProductRelationships value) {
        this.productRelationships = value;
    }

    /**
     * Gets the value of the availabilityInformation property.
     * 
     * @return
     *     possible object is
     *     {@link AvailabilityInformation }
     *     
     */
    public AvailabilityInformation getAvailabilityInformation() {
        return availabilityInformation;
    }

    /**
     * Sets the value of the availabilityInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link AvailabilityInformation }
     *     
     */
    public void setAvailabilityInformation(AvailabilityInformation value) {
        this.availabilityInformation = value;
    }

    /**
     * Gets the value of the shippingInformation property.
     * 
     * @return
     *     possible object is
     *     {@link ShippingInformation }
     *     
     */
    public ShippingInformation getShippingInformation() {
        return shippingInformation;
    }

    /**
     * Sets the value of the shippingInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShippingInformation }
     *     
     */
    public void setShippingInformation(ShippingInformation value) {
        this.shippingInformation = value;
    }

    /**
     * Gets the value of the channelSpecificInformation property.
     * 
     * @return
     *     possible object is
     *     {@link ChannelSpecificInformation }
     *     
     */
    public ChannelSpecificInformation getChannelSpecificInformation() {
        return channelSpecificInformation;
    }

    /**
     * Sets the value of the channelSpecificInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChannelSpecificInformation }
     *     
     */
    public void setChannelSpecificInformation(ChannelSpecificInformation value) {
        this.channelSpecificInformation = value;
    }

    /**
     * Gets the value of the sku property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSKU() {
        return sku;
    }

    /**
     * Sets the value of the sku property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSKU(String value) {
        this.sku = value;
    }

}
