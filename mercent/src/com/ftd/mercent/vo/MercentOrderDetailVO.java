/**
 * 
 */
package com.ftd.mercent.vo;

import java.math.BigDecimal;

public class MercentOrderDetailVO 
{
	private String mercentItemNumber;
	private String mercentOrderNumber;
	private String mercentChannelOrderItemId;
	private String confirmationNumber;
	private String mercentProductId;
	private BigDecimal principalAmount;
	private BigDecimal shippingAmount;
	private BigDecimal taxAmount;
	private BigDecimal shippingTaxAmount;
	
	public String getMercentItemNumber() {
		return mercentItemNumber;
	}
	public void setMercentItemNumber(String mercentItemNumber) {
		this.mercentItemNumber = mercentItemNumber;
	}
	public String getMercentOrderNumber() {
		return mercentOrderNumber;
	}
	public void setMercentOrderNumber(String mercentOrderNumber) {
		this.mercentOrderNumber = mercentOrderNumber;
	}
	public String getMercentChannelOrderItemId() {
		return mercentChannelOrderItemId;
	}
	public void setMercentChannelOrderItemId(String mercentChannelOrderItemId) {
		this.mercentChannelOrderItemId = mercentChannelOrderItemId;
	}
	public String getConfirmationNumber() {
		return confirmationNumber;
	}
	public void setConfirmationNumber(String confirmationNumber) {
		this.confirmationNumber = confirmationNumber;
	}
	public String getMercentProductId() {
		return mercentProductId;
	}
	public void setMercentProductId(String mercentProductId) {
		this.mercentProductId = mercentProductId;
	}
	public BigDecimal getPrincipalAmount() {
		return principalAmount;
	}
	public void setPrincipalAmount(BigDecimal principalAmount) {
		this.principalAmount = principalAmount;
	}
	public BigDecimal getShippingAmount() {
		return shippingAmount;
	}
	public void setShippingAmount(BigDecimal shippingAmount) {
		this.shippingAmount = shippingAmount;
	}
	public BigDecimal getTaxAmount() {
		return taxAmount;
	}
	public void setTaxAmount(BigDecimal taxAmount) {
		this.taxAmount = taxAmount;
	}
	public BigDecimal getShippingTaxAmount() {
		return shippingTaxAmount;
	}
	public void setShippingTaxAmount(BigDecimal shippingTaxAmount) {
		this.shippingTaxAmount = shippingTaxAmount;
	}

	
	
}
