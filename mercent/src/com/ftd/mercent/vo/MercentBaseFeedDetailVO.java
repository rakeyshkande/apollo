/**
 * 
 */
package com.ftd.mercent.vo;

/**
 * @author sbring
 *
 */
public class MercentBaseFeedDetailVO {
	private String feedId;
	private String feedType;
	private String productId;
	private String feedStatus;
	private String server;
	private String filename;
	private String transactionId;
	private String createdBy;
	private String updatedBy;
	
	private String channelOrderID;
	private String merchantOrderID;
	private String channelOrderItemId;
	private String merchantOrderItemId;
	private String channelName;

	public String getFeedId() {
		return feedId;
	}
	public void setFeedId(String feedId) {
		this.feedId = feedId;
	}
	public String getFeedType() {
		return feedType;
	}
	public void setFeedType(String feedType) {
		this.feedType = feedType;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getFeedStatus() {
		return feedStatus;
	}
	public void setFeedStatus(String feedStatus) {
		this.feedStatus = feedStatus;
	}
	public String getServer() {
		return server;
	}
	public void setServer(String server) {
		this.server = server;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public String getChannelOrderID() {
		return channelOrderID;
	}
	public void setChannelOrderID(String channelOrderID) {
		this.channelOrderID = channelOrderID;
	}
	public String getMerchantOrderID() {
		return merchantOrderID;
	}
	public void setMerchantOrderID(String merchantOrderID) {
		this.merchantOrderID = merchantOrderID;
	}
	public String getChannelOrderItemId() {
		return channelOrderItemId;
	}
	public void setChannelOrderItemId(String channelOrderItemId) {
		this.channelOrderItemId = channelOrderItemId;
	}
	public String getChannelName() {
		return channelName;
	}
	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}
	public String getMerchantOrderItemId() {
		return merchantOrderItemId;
	}
	public void setMerchantOrderItemId(String merchantOrderItemId) {
		this.merchantOrderItemId = merchantOrderItemId;
	}	
}
