package com.ftd.mercent.vo;

import javax.xml.datatype.XMLGregorianCalendar;

/**
 * @author smeka
 *
 */
public class OrderFulfillmentFeedVO extends MercentBaseFeedDetailVO {	
	private String orderFulfillmentId;
	private XMLGregorianCalendar fulfillmentDate;	
	private String shippingTrackingNumber;
	private String shippingMethod;
	private String itemSKU;
	private int quantity;
	private String carrier;
	
	public String getOrderFulfillmentId() {
		return orderFulfillmentId;
	}
	public void setOrderFulfillmentId(String orderFulfillmentId) {
		this.orderFulfillmentId = orderFulfillmentId;
	}
	public XMLGregorianCalendar getFulfillmentDate() {
		return fulfillmentDate;
	}
	public void setFulfillmentDate(XMLGregorianCalendar fulfillmentDate) {
		this.fulfillmentDate = fulfillmentDate;
	}
	public String getCarrier() {
		return carrier;
	}
	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}
	public String getShippingTrackingNumber() {
		return shippingTrackingNumber;
	}
	public void setShippingTrackingNumber(String shippingTrackingNumber) {
		this.shippingTrackingNumber = shippingTrackingNumber;
	}
	public String getShippingMethod() {
		return shippingMethod;
	}
	public void setShippingMethod(String shippingMethod) {
		this.shippingMethod = shippingMethod;
	}
	public String getItemSKU() {
		return itemSKU;
	}
	public void setItemSKU(String itemSKU) {
		this.itemSKU = itemSKU;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}	
}
