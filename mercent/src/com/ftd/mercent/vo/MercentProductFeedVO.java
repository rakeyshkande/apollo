package com.ftd.mercent.vo;

import java.util.ArrayList;
import java.util.List;


/**
 * @author sbring
 *
 */
public class MercentProductFeedVO {
	private List<MercentProductFeedDetailVO> feedDetails = new ArrayList<MercentProductFeedDetailVO>();

	public List<MercentProductFeedDetailVO> getFeedDetails() {
		if(feedDetails == null){
			this.feedDetails = new ArrayList<MercentProductFeedDetailVO>();
		}
		return feedDetails;
	}

	public void setFeedDetails(List<MercentProductFeedDetailVO> feedDetails) {
		this.feedDetails = feedDetails;
	}
	public void addFeedDetail(MercentProductFeedDetailVO feedDetail) {
		this.feedDetails.add(feedDetail);
	}
}
