package com.ftd.mercent.vo;

public class MercentChannelMappingVO {
	private String channelName;
	private String ftdOriginMapping;
	private String masterOrderNoPrefix;
	private String channelImage;
	private String confNumberPrefix;
	private String defSrcCode;
	private String defRecalcSrcCode;
	
	public String getConfNumberPrefix() {
		return confNumberPrefix;
	}
	public void setConfNumberPrefix(String confNumberPrefix) {
		this.confNumberPrefix = confNumberPrefix;
	}
	public String getChannelName() {
		return channelName;
	}
	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}
	public String getFtdOriginMapping() {
		return ftdOriginMapping;
	}
	public void setFtdOriginMapping(String ftdOriginMapping) {
		this.ftdOriginMapping = ftdOriginMapping;
	}
	public String getMasterOrderNoPrefix() {
		return masterOrderNoPrefix;
	}
	public void setMasterOrderNoPrefix(String masterOrderNoPrefix) {
		this.masterOrderNoPrefix = masterOrderNoPrefix;
	}
	public String getChannelImage() {
		return channelImage;
	}
	public void setChannelImage(String channelImage) {
		this.channelImage = channelImage;
	}
	public String getDefSrcCode() {
		return defSrcCode;
	}
	public void setDefSrcCode(String defSrcCode) {
		this.defSrcCode = defSrcCode;
	}
	public String getDefRecalcSrcCode() {
		return defRecalcSrcCode;
	}
	public void setDefRecalcSrcCode(String defRecalcSrcCode) {
		this.defRecalcSrcCode = defRecalcSrcCode;
	}
	
	
}
