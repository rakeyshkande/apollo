package com.ftd.mercent.vo;

import java.util.ArrayList;
import java.util.List;

public class MercentProductAttributesVO {

	protected List<String> bulletPoint;
	protected List<String> intendedUse;
	protected List<String> searchTerms;
	protected List<String> mrcTargetAudience;
	protected List<String> mrcSubjectMatter;
	protected List<String> mrcOtherAttributes;

	public List<String> getBulletPoint() {
		if (bulletPoint == null) {
			bulletPoint = new ArrayList<String>();
		}
		return this.bulletPoint;
	}
	
	public List<String> getIntendedUse() {
		if (intendedUse == null) {
			intendedUse = new ArrayList<String>();
		}
		return this.intendedUse;
	}

	public List<String> getSearchTerms() {
		if (searchTerms == null) {
			searchTerms = new ArrayList<String>();
		}
		return this.searchTerms;
	}

	public List<String> getMrcTargetAudience() {
		if (mrcTargetAudience == null) {
			mrcTargetAudience = new ArrayList<String>();
		}
		return this.mrcTargetAudience;
	}

	public List<String> getMrcSubjectMatter() {
		if (mrcSubjectMatter == null) {
			mrcSubjectMatter = new ArrayList<String>();
		}
		return this.mrcSubjectMatter;
	}

	public List<String> getMrcOtherAttributes() {
		if (mrcOtherAttributes == null) {
			mrcOtherAttributes = new ArrayList<String>();
		}
		return this.mrcOtherAttributes;
	}

}
