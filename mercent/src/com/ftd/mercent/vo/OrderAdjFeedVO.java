package com.ftd.mercent.vo;

import java.util.ArrayList;
import java.util.List;

public class OrderAdjFeedVO extends MercentBaseFeedDetailVO {
			
	private int quantity;
	private int noOfAdjs;	
	private boolean isOrderFullyRefudned;
	private boolean isAdjAlreadySent;
	
	private List<ItemAdjFeedVO> itemAdjFeeds;
	
	public int getNoOfAdjs() {
		return noOfAdjs;
	}
	public void setNoOfAdjs(int noOfAdjs) {
		this.noOfAdjs = noOfAdjs;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public List<ItemAdjFeedVO> getItemAdjFeeds() {
		if(itemAdjFeeds == null) {
			itemAdjFeeds = new ArrayList<ItemAdjFeedVO>();
		}
		return itemAdjFeeds;
	}
	public void setItemAdjFeeds(List<ItemAdjFeedVO> itemAdjFeeds) {
		this.itemAdjFeeds = itemAdjFeeds;
	}
	public boolean isOrderFullyRefudned() {
		return isOrderFullyRefudned;
	}
	public void setOrderFullyRefudned(boolean isOrderFullyRefudned) {
		this.isOrderFullyRefudned = isOrderFullyRefudned;
	}
	public boolean isAdjAlreadySent() {
		return isAdjAlreadySent;
	}
	public void setAdjAlreadySent(boolean isAdjAlreadySent) {
		this.isAdjAlreadySent = isAdjAlreadySent;
	}
	
}
