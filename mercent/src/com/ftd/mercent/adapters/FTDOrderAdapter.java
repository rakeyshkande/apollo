/**
 * 
 */
package com.ftd.mercent.adapters;

import java.math.BigDecimal;
import java.sql.Connection;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang.StringUtils;

import com.ftd.mercent.dao.MercentOrderFeedDAO;
import com.ftd.mercent.exceptions.MercentException;
import com.ftd.mercent.utilities.MercentConstants;
import com.ftd.mercent.utilities.MercentUtils;
import com.ftd.mercent.vo.MercentChannelMappingVO;
import com.ftd.mercent.vo.ftd.FTDOrder;
import com.ftd.mercent.vo.ftd.Header;
import com.ftd.mercent.vo.ftd.Item;
import com.ftd.mercent.vo.mercent.Name;
import com.ftd.mercent.vo.mercent.Order;
import com.ftd.mercent.vo.mercent.Order.BillingData;
import com.ftd.mercent.vo.mercent.OrderItem;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.CacheUtil;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.StateMasterVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pas.client.service.PASRequestServiceFactory;
import com.ftd.pas.common.service.PASRequestService;
import com.ftd.pas.common.vo.ProductAvailVO;
import com.ftd.pas.server.ProductAvailabilityBO;

/**
 * @author skatam
 *
 */
public class FTDOrderAdapter 
{
	private Logger logger = new Logger("com.ftd.mercent.adapters.FTDOrderAdapter");
	private NumberFormat fmt = NumberFormat.getNumberInstance();
	
	private Connection conn = null;
	
	public FTDOrderAdapter(Connection conn) {
		this.conn = conn;
	}
	
	public FTDOrder getAsFTDOrder(Order mercentOrder) 
	{
		FTDOrder ftdOrder = null;
		ftdOrder = this.transformToFTDOrder(mercentOrder);
		return ftdOrder;
	}
	
	private FTDOrder transformToFTDOrder(Order mercentOrder)
	{
		FTDOrder ftdOrder = new FTDOrder();
		
		Header header = new Header();
		MercentOrderFeedDAO orderFeedDAO = new MercentOrderFeedDAO(conn);
		MercentChannelMappingVO channelMappingVO;
		try 
		{
			Date dt = null;
			if(mercentOrder.getOrderDate() != null){
				dt = MercentUtils.fromXMLGregorianCalendar(mercentOrder.getOrderDate()).getTime();
			} else{
				dt = new Date();
			}
			
			java.sql.Date orderDate = new java.sql.Date(dt.getTime());
			
	        BigDecimal dDiscount = new BigDecimal("0");
	        String discountType = "";
	        String mercentOrderNumber = mercentOrder.getChannelOrderID();
	    
	      //Origin
	      String channelName = mercentOrder.getChannelName().toUpperCase();
	      channelMappingVO=orderFeedDAO.getMercentChannelMapping(channelName);
	      header.setOrigin(channelMappingVO.getFtdOriginMapping());
	      String sourceCode = channelMappingVO.getDefSrcCode();
	      logger.debug("Source code from ChannelMapping :" +sourceCode);
			
	      //Generate the master order number
	      String masterOrderNumber = channelMappingVO.getMasterOrderNoPrefix()+StringUtils.replaceChars(mercentOrder.getChannelOrderID(),"-","");
	      header.setMercentOrderNumber(mercentOrderNumber);
	      header.setMasterOrderNumber(masterOrderNumber);
	      header.setSourceCode(sourceCode);
	      
	    //Order count
	    int orderCounter=0;
	      
	    SimpleDateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
	    header.setTransactionDate(format.format(orderDate));
	        
	    //Socket Timestamp
	    header.setSocketTimestamp(format.format(new java.util.Date()));
	  
	    //set buyer details
	    this.setBuyerDetails(mercentOrder, header);
	   
        //News Letter Flag
        String newsLetterFlag = MercentUtils.getMercentConfigProperty("news_letter_flag");
        header.setNewsLetterFlag(newsLetterFlag);
        
        //Payment Type
        String ccType = MercentUtils.getMercentConfigProperty("cc_type");
        header.setCcType(ccType);
        
        ftdOrder.setHeader(header);
        
        //Now do the order items
        BigDecimal totalComputedPrice = new BigDecimal("0");
        BigDecimal totalDiscount = new BigDecimal("0");
        BigDecimal totalShipping = new BigDecimal("0");
        BigDecimal totalTax = new BigDecimal("0");
        BigDecimal totalShippingTax = new BigDecimal("0");
        
        List<OrderItem> orderItems = mercentOrder.getOrderItem();
        for (OrderItem orderItem : orderItems) 
        {
        	Item ftdOrderItem = new Item();
        	//Now save the MercentOrderItemCode for use later on down the read
            String channelOrderItemID = orderItem.getChannelOrderItemID();
            if( channelOrderItemID==null) 
            {
              throw new MercentException("No Mercent order item code can be found for this item");  
            }
            ftdOrderItem.setMercentOrderItemCode(channelOrderItemID);
            
            //IOTW source code
            
            String sku = orderItem.getSKU();
            String skuDeluxePremium = sku;
            String mercentSku = sku;
            
            if(sku.indexOf(MercentConstants.CATALOG_SUFFIX_DELUXE) >= 0 
            		|| sku.indexOf(MercentConstants.CATALOG_SUFFIX_PREMIUM) >= 0){
          	  sku = sku.substring(0, 4);
            }
            String dateReceived = new SimpleDateFormat("dd-MMM-yy").format(orderDate);
            Map iotwMap = orderFeedDAO.getIOTWSourceCode(sourceCode, sku, dateReceived);
            
            sourceCode = (String) iotwMap.get("iotwSourceCode");
            dDiscount = (BigDecimal) iotwMap.get("discount");
            discountType = (String) iotwMap.get("discountType");
            logger.debug("sourceCode: " + sourceCode + " discountType: " + discountType +" discount: " + dDiscount);
            //End IOTW source code
            
            //Source Code
            ftdOrderItem.setItemSourceCode(sourceCode);
            
            //look up the price in FTD_APPS.PRODUCT_MASTER
            logger.debug("Getting pdb data");
            BigDecimal pdb_price = new BigDecimal("0");
            Map pdbPriceMap = orderFeedDAO.getPdbPriceData(sku);
            String shipMethodFlorist = (String) pdbPriceMap.get("shipMethodFlorist");
            if (shipMethodFlorist == null) shipMethodFlorist = "N";
            String shipMethodCarrier = (String) pdbPriceMap.get("shipMethodCarrier");
            if (shipMethodCarrier == null) shipMethodCarrier = "N";
            
            // get pdb_price
            pdb_price=getPdbPrice(pdbPriceMap, sku,skuDeluxePremium,orderDate);
            logger.debug("##pdb_price: "+pdb_price);
            
            
            BigDecimal itemComputedPrice = new BigDecimal("0");
            BigDecimal itemDiscount = new BigDecimal("0");
            BigDecimal itemPrincipal = new BigDecimal("0");
            BigDecimal itemShipping = new BigDecimal("0");
            BigDecimal itemTax = new BigDecimal("0");
            BigDecimal itemShippingTax = new BigDecimal("0");
            BigDecimal commission = new BigDecimal("0");
            
            //Get Delivery date
            String shipMethod = "";
            Date deliveryDate = this.getMercentDeliveryDate(orderItem);
            if(deliveryDate != null)
            {
              String zipCode = mercentOrder.getFulfillmentData().getFulfillmentAddress().getPostalCode();
          	  shipMethod = getShipMethod(conn, sku, deliveryDate, zipCode, shipMethodFlorist, shipMethodCarrier);
          	  ftdOrderItem.setDeliveryDate(new SimpleDateFormat("MM/dd/yyyy").format(deliveryDate));
            }
            else
            {
				String zipCode = mercentOrder.getFulfillmentData().getFulfillmentAddress().getPostalCode();
				if (zipCode.length() > 5) {
					zipCode = zipCode.substring(0, 5);
				}
				Date nextAvailableDate = getNextAvailableDate(conn, sku,
						zipCode, 30, shipMethodFlorist, shipMethodCarrier);
				if (nextAvailableDate != null) {
					shipMethod = getShipMethod(conn, sku,
							nextAvailableDate, zipCode, shipMethodFlorist,
							shipMethodCarrier);
				}

				logger.info("Delivery date from pas service: "+ nextAvailableDate);
				if (nextAvailableDate != null) {
					ftdOrderItem.setDeliveryDate(
							new SimpleDateFormat("MM/dd/yyyy").format(nextAvailableDate));
				} else {
					ftdOrderItem.setDeliveryDate(null);
				}
            }
            
          //shipping-method
            logger.info("shipMethod: " + shipMethod + " shipMethodFlorist: " + shipMethodFlorist +
          		  " shipMethodCarrier: " + shipMethodCarrier);
            if (shipMethod != null) {
            		ftdOrderItem.setShippingMethod(shipMethod);
            } else {
	              ftdOrderItem.setShippingMethod("");
            }
            
            //Get the dollar amounts of the four price elements
            if(orderItem.getItemPrice() != null)
            {
            	itemPrincipal = getSafeBigDecimal(orderItem.getItemPrice().getItemAmount());
                itemShipping = getSafeBigDecimal(orderItem.getItemPrice().getShippingAmount());
                itemTax = getSafeBigDecimal(orderItem.getItemPrice().getItemTax());
                itemShippingTax = getSafeBigDecimal(orderItem.getItemPrice().getShippingTax());
            }
            
            //Back out the discount if there is any
           
			boolean isDiscountApplied = false;

			itemDiscount = this.calculateItemDiscount(discountType, dDiscount,pdb_price, sourceCode);
			if (itemDiscount.compareTo(BigDecimal.ZERO)!=0.0)  {
				itemComputedPrice = itemPrincipal.add(itemDiscount);
				isDiscountApplied = true;
			}
			else
			{
				itemComputedPrice = itemPrincipal;
			}
			logger.debug("itemComputedPrice: " + itemComputedPrice);
            
          //discount computation price does not equal pdb price then take mercent price
            if( isDiscountApplied && itemComputedPrice.compareTo(pdb_price)!=0 ){
              itemComputedPrice = itemPrincipal;
              itemDiscount=new BigDecimal("0");
              logger.debug("Prices do not match, setting discount to 0");
            }
            //Increment the totals
            totalComputedPrice = totalComputedPrice.add(itemComputedPrice);
            //totalComputedPrice+=itemPrincipal;
            totalDiscount = totalDiscount.add(itemDiscount);
            totalShipping = totalShipping.add(itemShipping);
            totalTax = totalTax.add(itemTax);
            totalShippingTax = totalShippingTax.add(itemShippingTax);
            
          //Add the item amounts
            ftdOrderItem.setOrderTotal(fmt.format((itemComputedPrice.subtract(itemDiscount)).add(itemShipping).add(itemTax).add(itemShippingTax)));
            ftdOrderItem.setTaxAmount(fmt.format(itemTax));
            ftdOrderItem.setShippingTaxAmount(fmt.format(itemShippingTax));
            ftdOrderItem.setServiceFee(fmt.format(itemShipping));
            
            
             //Product id (always send the novator id)
            ftdOrderItem.setProductid(sku);
            ftdOrderItem.setMercentProductid(mercentSku);
            
            
            //Quantity (will be validated at the validator)
            ftdOrderItem.setQuantity(String.valueOf(orderItem.getQuantity()));
            
          //Commission
            String commissionStr = null;
            if(orderItem.getItemFee() != null && orderItem.getItemFee().getItemCommission() != null)
            {
	            commissionStr = orderItem.getItemFee().getItemCommission().toString();
	            if( commissionStr.length()>0 ) {
	              //Comes in as a negative amount, change to positive
	              commission = new BigDecimal(commissionStr).multiply(new BigDecimal(-1));
	            }
	            ftdOrderItem.setCommission(fmt.format(commission));
            }
          
            //Product price
            ftdOrderItem.setProductPrice(fmt.format(itemComputedPrice));
            logger.debug("XML product-price:" + fmt.format(itemComputedPrice));
  	       
            this.setFulfillmentData(mercentOrder, ftdOrderItem);
	        
	        ftdOrderItem.setShipToType(MercentUtils.getMercentConfigProperty("ship_to_type"));
	        ftdOrderItem.setOccassion(MercentUtils.getMercentConfigProperty("occasion"));
	        if(orderItem.getGiftWrapData() != null)
	        {
	        	ftdOrderItem.setCardMessagespecialInstructions(orderItem.getGiftWrapData().getGiftWrapMessage());
	        }
	        ftdOrderItem.setFolIndicator(MercentUtils.getMercentConfigProperty("fol_indicator"));
	        ftdOrderItem.setSundayDeliveryFlag(MercentUtils.getMercentConfigProperty("sunday_delivery_flag"));
	        ftdOrderItem.setSenderReleaseFlag(MercentUtils.getMercentConfigProperty("sender_release_flag"));
	        ftdOrderItem.setProductSubstitutionAcknowledgement(MercentUtils.getMercentConfigProperty("product_sub_ack"));
	        
            //PDB price
	        ftdOrderItem.setPdbPrice(fmt.format(pdb_price));
            logger.debug("XML pdb-price: " + fmt.format(pdb_price));
            
            //Transaction
            ftdOrderItem.setTransaction(fmt.format(itemPrincipal.add(itemShipping).add(itemTax).add(itemShippingTax)));
            
            //Wholesale
            ftdOrderItem.setWholesale(fmt.format( itemPrincipal.add(itemShipping).add(itemTax).add(itemShippingTax).subtract(commission) ));            

            //Item Discount
            if( itemDiscount.compareTo(BigDecimal.ZERO) >0 )
            {
              //Item discount amount
            	ftdOrderItem.setDiscountAmount(fmt.format(itemDiscount));
	            logger.debug("XML discount-amount: " + fmt.format(itemDiscount));
            
              //Item Discount Type
	          ftdOrderItem.setDiscountType(discountType);
	            logger.debug("XML discount-type: " + discountType);
              
              //Discounted product price
	            ftdOrderItem.setDiscountedProductPrice(fmt.format(itemPrincipal));
	            logger.debug("XML discounted-product-price: " + fmt.format(itemPrincipal));
            }
	        
            //Mercent principal amount
            ftdOrderItem.setMercentPrincipalAmount(fmt.format(itemPrincipal));
            
            ftdOrder.getItems().add(ftdOrderItem);
            
            orderCounter++;
	        
	        
		} //End Item loop
        
        
        //Add the order count in
        header.setOrderCount(String.valueOf(orderCounter));
        
        //Add the order amount
        header.setOrderAmount(fmt.format( (totalComputedPrice.subtract(totalDiscount)).add(totalShipping).add(totalTax).add(totalShippingTax) ));
        
        //Add the discount total
        if( totalDiscount.compareTo(BigDecimal.ZERO) >0 )
        {
        	header.setDiscountTotal(fmt.format(totalDiscount));
        }
        
        //Finally put in the confirmation numbers
        //We hold off on doing this till now to prevent gaps in sequence numbers because of errors
        this.setConformationNumber(ftdOrder, channelMappingVO);
        
	      
		}
		catch (Exception e) 
		{
			throw new MercentException(e);
		}
		return ftdOrder;
	}
	
	
	private void setBuyerDetails(Order mercentOrder,Header header)
	{
		MercentOrderFeedDAO orderFeedDAO = new MercentOrderFeedDAO(conn);
		ConfigurationUtil cu;
		try 
		{
			BillingData billingData = mercentOrder.getBillingData();
			cu = ConfigurationUtil.getInstance();
			String mercentOrderNumber = mercentOrder.getChannelOrderID();
			
			Name buyerNameData = billingData.getName();
			String buyerFirstName = buyerNameData.getFirstName();
			String buyerLastName = buyerNameData.getLastName();
			if(StringUtils.isBlank(buyerFirstName)|| StringUtils.isBlank(buyerLastName))
			{
				String buyerName = buyerNameData.getFullName();
				if (buyerName == null)
					buyerName = "";
				// Buyer First Name
				buyerFirstName = StringUtils.substringBefore(buyerName, " ");
				if (buyerFirstName == null)
					buyerFirstName = "";
				if (buyerFirstName == "" || buyerFirstName.isEmpty()) {
					buyerFirstName = "NA";
				}
				if (buyerFirstName.length() > 20) {
					MercentUtils.sendNoPageSystemMessage("Mercent Order Number: "
							+ mercentOrderNumber + "\nBuyer First Name too long: "
							+ buyerFirstName);
					buyerFirstName = buyerFirstName.substring(0, 20);
				}
				
				// Buyer Last Name is updated with the system date
				//Calendar cal = Calendar.getInstance();
				//DateFormat dateFormat = new SimpleDateFormat("MMddyyyy");
				//String currentDate = dateFormat.format(cal.getTime());
				buyerLastName = StringUtils.substringAfter(buyerName, " ");
				if (buyerLastName == null)
					buyerLastName = "";
				if (buyerLastName == "" || buyerLastName.isEmpty()) {
					buyerLastName = "NA";
				}
				//TODO; Do we need to append Current Date?
				//buyerLastName = buyerLastName.concat("-");
				//buyerLastName = buyerLastName.concat(currentDate);
				logger.info(" *****buyerLastName: " + buyerLastName);
				if (buyerLastName != null && buyerLastName.length() > 20) {
					MercentUtils.sendNoPageSystemMessage("Mercent Order Number: "
							+ mercentOrderNumber + "\nBuyer Last Name too long: "
							+ buyerLastName);
					buyerLastName = buyerLastName.substring(0, 20);
				}
			}
			
			header.setBuyerFirstName(buyerFirstName);
			header.setBuyerLastName(buyerLastName);

			// Buyer Daytime Phone
			String buyerPhoneNumber = billingData.getPhoneNumber();
			if (buyerPhoneNumber == null)
				buyerPhoneNumber = "";
			buyerPhoneNumber = MercentUtils.removeAllSpecialChars(buyerPhoneNumber);
			if (buyerPhoneNumber.length() > 10) {
				MercentUtils.sendNoPageSystemMessage("Mercent Order Number: "
						+ mercentOrderNumber
						+ "\nBuyer Phone Number too long: " + buyerPhoneNumber);
				buyerPhoneNumber = buyerPhoneNumber.substring(0, 10);
			} else if (buyerPhoneNumber.length() < 10) {
				buyerPhoneNumber = "0000000000" + buyerPhoneNumber;
				buyerPhoneNumber = buyerPhoneNumber.substring(buyerPhoneNumber.length() - 10);
				logger.debug("buyerPhoneNumber too short, changed to: "
						+ buyerPhoneNumber);
			}
			header.setBuyerDaytimePhone(buyerPhoneNumber);

			// Buyer Address 1 is updated with the sequence number from
			// database.
			String buyerSequence = orderFeedDAO.getMercentBuyerSequence();
			StringBuffer buyerAddress1 = new StringBuffer(buyerSequence);
			buyerAddress1.append("-");
			buyerAddress1.append(cu.getContentWithFilter(conn, "MERCENT", "DEFAULT_BUYER_ADDRESS", null, null));
			logger.info(" ***** buyerAddress1: " + buyerAddress1.toString());
			header.setBuyerAddress1(buyerAddress1.toString());

			// Buyer City
			String buyerCity = cu.getContentWithFilter(conn, "MERCENT", "DEFAULT_BUYER_CITY", null, null);
			header.setBuyerCity(buyerCity);

			// Buyer State
			String buyerState = cu.getContentWithFilter(conn, "MERCENT", "DEFAULT_BUYER_STATE", null, null);
			header.setBuyerState(buyerState);

			// Buyer Postal
			String buyerPostalCode = cu.getContentWithFilter(conn, "MERCENT", "DEFAULT_BUYER_POSTAL_CODE", null, null);
			header.setBuyerPostalCode(buyerPostalCode);

			// Buyer Country
			String buyerCountry = cu.getContentWithFilter(conn, "MERCENT", "DEFAULT_BUYER_COUNTRY", null, null);
			header.setBuyerCountry(buyerCountry);
			
			// Buyer Email
			String buyerEmailAddress = billingData.getEmailAddress();
			header.setBuyerEmailAddress(buyerEmailAddress);

		} catch (Exception e) {
			throw new MercentException(e.getCause());
		}
		
	}
	
	
	
	private void setFulfillmentData(Order mercentOrder,Item ftdOrderItem)
	{
		String mercentOrderNumber = mercentOrder.getChannelOrderID();
		MercentOrderFeedDAO orderFeedDAO = new MercentOrderFeedDAO(conn);
		
		String recipientFirstName = mercentOrder.getFulfillmentData().getName().getFirstName();
		String recipientLastName = mercentOrder.getFulfillmentData().getName().getLastName();
		if(StringUtils.isBlank(recipientFirstName) || StringUtils.isBlank(recipientLastName))
		{
			String recipientName = mercentOrder.getFulfillmentData().getName().getFullName();
	        if (recipientName == null) recipientName = "";
	         	recipientFirstName = StringUtils.substringBefore(recipientName," ");
	        if (recipientFirstName == null) recipientFirstName = "";
	        if (recipientFirstName == "" || recipientFirstName.isEmpty()){ recipientFirstName = "NA"; }
	        if (recipientFirstName.length() > 20) {
             MercentUtils.sendNoPageSystemMessage("Mercent Order Number: " + mercentOrderNumber + 
             		"\nRecipient First Name too long: " + recipientFirstName);
             recipientFirstName = recipientFirstName.substring(0, 20);
	        }
	        recipientLastName = StringUtils.substringAfter(recipientName," ");
	        if (recipientLastName == null) recipientLastName = "";
	        if (recipientLastName == "" || recipientLastName.isEmpty()){ recipientLastName = "NA"; }
	        if (recipientLastName.length() > 20) 
	        {
	        	MercentUtils.sendNoPageSystemMessage("Mercent Order Number: " + mercentOrderNumber + 
             		"\nRecipient Last Name too long: " + recipientLastName);
             recipientLastName = recipientLastName.substring(0, 20);
	        }
		}
		
		
	        //Recipient First Name
	        ftdOrderItem.setRecipFirstName(recipientFirstName);
	        //Recipient Last Name
	        ftdOrderItem.setRecipLastName(recipientLastName);
         
	      //Recipient Address 1
	        String addressOne = mercentOrder.getFulfillmentData().getFulfillmentAddress().getAddress1(); 
	        String addressTwo = mercentOrder.getFulfillmentData().getFulfillmentAddress().getAddress2(); 
	        String addressThree = mercentOrder.getFulfillmentData().getFulfillmentAddress().getAddress3(); 
	        String newAddressOne = addressOne;
	        if (newAddressOne == null) newAddressOne = "";
	        String newAddressTwo = addressTwo;
	        if (newAddressTwo == null) newAddressTwo = "";
	        if (addressThree != null) {
	        	newAddressTwo = newAddressTwo.concat(" "+addressThree);
	        }
	        if (newAddressOne.length() > 45) {
	        	String tempAddress = newAddressOne.substring(45, newAddressOne.length());
	        	if (newAddressTwo.length() > 0) tempAddress = tempAddress.concat("; ");
	        	newAddressTwo = tempAddress.concat(newAddressTwo);
	        	newAddressOne = newAddressOne.substring(0, 45);
	        }
	        if (newAddressTwo.length() > 45) {
	        	MercentUtils.sendNoPageSystemMessage("Mercent Order Number: " + mercentOrderNumber + 
             		"\nRecipient Address too long: " + newAddressOne + "\n" + newAddressTwo);
	        	newAddressTwo = newAddressTwo.substring(0, 45);
	        }
	        ftdOrderItem.setRecipAddress1(newAddressOne);
	        ftdOrderItem.setRecipAddress2(newAddressTwo);
	      
	        //Recipient City
	        String recipientCity = mercentOrder.getFulfillmentData().getFulfillmentAddress().getCity(); 
	        if (recipientCity == null) recipientCity = "";
	        if (recipientCity.length() > 30) {
	        	MercentUtils.sendNoPageSystemMessage("Mercent Order Number: " + mercentOrderNumber + 
             		"\nRecipient City too long: " + recipientCity);
             recipientCity = recipientCity.substring(0, 30);
	        }
	        ftdOrderItem.setRecipCity(recipientCity);
	        
	        //Recipient State
	        String recipientState = mercentOrder.getFulfillmentData().getFulfillmentAddress().getStateOrRegion(); 
	        if (recipientState == null) recipientState = "";
	        CacheUtil cacheUtil = CacheUtil.getInstance();
	        logger.info("checking recipientState: " + recipientState);
	        StateMasterVO stateVO=null;
			try {
				stateVO = cacheUtil.getStateById(recipientState);
			} catch (Exception e) {
				throw new MercentException(e.getCause());
			}
	        if (stateVO == null) {
	        	logger.info("Not in cache, checking by name");
	        	stateVO = orderFeedDAO.getStateByName(recipientState);
	        	if (stateVO != null && stateVO.getStateMasterId() != null) {
	        		logger.debug("found recipientState: " + stateVO.getStateMasterId());
	        		recipientState = stateVO.getStateMasterId();
	        	}
	        }
	        if (recipientState.length() > 10) {
	        	MercentUtils.sendNoPageSystemMessage("Mercent Order Number: " + mercentOrderNumber + 
             		"\nRecipient State too long: " + recipientState);
             recipientState = recipientState.substring(0, 10);
	        }
	        ftdOrderItem.setRecipState(recipientState);
	        
	        //Recipient Postal
	        String recipientZip = mercentOrder.getFulfillmentData().getFulfillmentAddress().getPostalCode(); 
	        if (recipientZip == null) recipientZip = "";
	        recipientZip = recipientZip.replaceAll(" ", "");
	        recipientZip = recipientZip.replaceAll("-", "");
	        if (recipientZip.length() > 9) {
	        	MercentUtils.sendNoPageSystemMessage("Mercent Order Number: " + mercentOrderNumber + 
             		"\nRecipient Zip too long: " + recipientZip);
             recipientZip = recipientZip.substring(0, 9);
	        }
	        ftdOrderItem.setRecipPostalCode(recipientZip);
	        
	        //Recipient Country
	        String recipientCountry = mercentOrder.getFulfillmentData().getFulfillmentAddress().getCountryCode();
	        if (recipientCountry == null) recipientCountry = "";
	        ftdOrderItem.setRecipCountry(recipientCountry);
	        
	        
	        //Recipient Daytime Phone
	        String recipientPhone = mercentOrder.getFulfillmentData().getPhoneNumber();
	        if (recipientPhone == null) recipientPhone = "";
	        recipientPhone = MercentUtils.removeAllSpecialChars(recipientPhone);
	        if (recipientPhone.length() > 10) {
	        	MercentUtils.sendNoPageSystemMessage("Mercent Order Number: " + mercentOrderNumber + 
             		"\nRecipient Phone too long: " + recipientPhone);
             recipientPhone = recipientPhone.substring(0, 10);
	        } else if (recipientPhone.length() < 10) {
	        	recipientPhone = "0000000000" + recipientPhone;
	        	recipientPhone = recipientPhone.substring(recipientPhone.length()-10);
	        	logger.debug("recipientPhone too short, changed to: " + recipientPhone);
	        }
	        ftdOrderItem.setRecipPhone(recipientPhone);
	        
	}
	private BigDecimal getPdbPrice(Map pdbPriceMap,String sku,String skuDeluxePremium,java.sql.Date orderDate) throws ParseException
	{
		BigDecimal pdb_price=new BigDecimal(0);  
		MercentOrderFeedDAO orderFeedDAO = new MercentOrderFeedDAO(conn);
		SimpleDateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
		
		if(skuDeluxePremium.indexOf(MercentConstants.CATALOG_SUFFIX_DELUXE) >= 0){
	              if(pdbPriceMap.get("deluxePrice")!=null){
	            	  		pdb_price = (BigDecimal) pdbPriceMap.get("deluxePrice");
	            	  }
	          }
	          else if(skuDeluxePremium.indexOf(MercentConstants.CATALOG_SUFFIX_PREMIUM) >= 0){
	           	  if(pdbPriceMap.get("premiumPrice")!=null){
	           		  		pdb_price = (BigDecimal) pdbPriceMap.get("premiumPrice");
	            	  }
	          }
	          else{
	           	  if(pdbPriceMap.get("standardPrice")!=null){
	           		  		pdb_price = (BigDecimal) pdbPriceMap.get("standardPrice");
	            	  }
	          }
         
         if(pdb_price.compareTo(BigDecimal.ZERO) ==0){ //get historical pdb data
	              Map productPriceMap = orderFeedDAO.getProductByTimestamp(sku, format.parse(format.format(orderDate)));
	              if(skuDeluxePremium.indexOf(MercentConstants.CATALOG_SUFFIX_DELUXE) >= 0){
	            	  if(productPriceMap.get("deluxePrice")!=null){
	            		  pdb_price = (BigDecimal) productPriceMap.get("deluxePrice");
	            	  }
	              }
	              else if(skuDeluxePremium.indexOf(MercentConstants.CATALOG_SUFFIX_PREMIUM) >= 0){
	            	  if(productPriceMap.get("premiumPrice")!=null){
	            		  pdb_price = (BigDecimal) productPriceMap.get("premiumPrice");
	            	  }
	              }
	              else{
	            	  if(productPriceMap.get("standardPrice")!=null){
	            		  pdb_price = (BigDecimal) productPriceMap.get("standardPrice");
	            	  }
	              }
         }
		
		return pdb_price;
	}
	
	private BigDecimal calculateItemDiscount(String discountType, 
											BigDecimal dDiscount,
											BigDecimal pdb_price,
											String sourceCode)
	{
		
		
		BigDecimal itemDiscount=new BigDecimal(0);
		
		if( dDiscount.compareTo(BigDecimal.ZERO)==0.0 ) 
         {
           itemDiscount= new BigDecimal("0");
         }
         else
         {
           if( StringUtils.equals("P",discountType) ) //Percentage discount
           {
             //Compute the undiscounted price.  What the next line does:
             //1.  Subtract the discount from 100
             //2.  Divide the pricicipal by the result of #1 by 100 for the undiscounted percentage 
             //3.  Multiply the amount of the principal by the undiscounted percentage giving you total price unrounded
             //4.  Multiply it by 100 to get the 1/100 position to the right of the decimal point
             //5.  Round off to the nearest decimal (aka 5/4 rounding)
             //6.  Divide the result of #5 by 100
             
             //Need to do this to get rid of the trailing non-significant digits
           	BigDecimal hundreths = new BigDecimal(0.01);
           	BigDecimal productPrice = pdb_price.setScale(2, BigDecimal.ROUND_DOWN);
           	BigDecimal discountPrice = productPrice.multiply(dDiscount).multiply(hundreths);
           	discountPrice = discountPrice.setScale(2, BigDecimal.ROUND_DOWN);
           	
             
             itemDiscount = discountPrice;
             logger.debug("##for discount type P - - itemDiscount: "+itemDiscount);
          //   isDiscountApplied = true;
           }
           else if( StringUtils.equals("D",discountType) ) //Dollar discount
           {
             itemDiscount = dDiscount;
             
             logger.debug("##for discount type D -  - itemDiscount: "+itemDiscount);
           //  isDiscountApplied = true;
           }
           else 
           {
             throw new MercentException("Mercent source code " + sourceCode + "is configured with an unsupported discount type of " + discountType );
           }
         }
		
		return itemDiscount;
	}
	
	private void setConformationNumber(FTDOrder ftdOrder,MercentChannelMappingVO channelMappingVO)
	{
		MercentOrderFeedDAO orderFeedDAO = new MercentOrderFeedDAO(conn);
		
		List<Item> items = ftdOrder.getItems();
        logger.debug("No of Items in FTD Order XML: "+items.size());
        for (Item item : items) 
        {
        	logger.debug("SKU: "+item.getProductid());
        	String confNumber = orderFeedDAO.getConfirmationNumber(channelMappingVO.getConfNumberPrefix());
        	item.setOrderNumber(confNumber);
		}
	}
	
	
	private Date getNextAvailableDate(Connection conn, 
										String itemSku, 
										String zipCode, 
										int days,
										String shipMethodFlorist, 
										String shipMethodCarrier)
	{
      Date nextAvailableDate = null;
      try {
          ProductAvailabilityBO paBO = new ProductAvailabilityBO();
	      ProductAvailVO[] paVOs = paBO.getProductAvailableDates(conn, itemSku, zipCode,null, days, null);
	      for (ProductAvailVO paVO : paVOs) {
	    	  if (paVO.getIsAvailable()) {
	    		  
    		      Calendar availableDate = paVO.getFloristCutoffDate();
                  if (availableDate != null && shipMethodFlorist.equalsIgnoreCase("Y")) {	  
    	    	      Calendar todayDate = Calendar.getInstance();
    		          //Don't allow same-day deliveries
    		          if (availableDate.after(todayDate)) {
                          if (availableDate.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
            	   	          if ( (ConfigurationUtil.getInstance().getFrpGlobalParm(MercentConstants.MERCENT_GLOBAL_CONTEXT, "SAT_DELIVERY_ALLOWED_FLORAL")).equalsIgnoreCase("N") ) {
              	                  continue;
        		              }
            	   	      //Don't allow Sunday delivery if SUN_DELIVERY_ALLOWED_FLORAL = 'N'
                          } else if (availableDate.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
                        	  if ( (ConfigurationUtil.getInstance().getFrpGlobalParm(MercentConstants.MERCENT_GLOBAL_CONTEXT, "SUN_DELIVERY_ALLOWED_FLORAL")).equalsIgnoreCase("N") ) {
              	                  continue;
        		              }
                          }
            	          nextAvailableDate = availableDate.getTime();
            	          break;
    	    	      } else {
    		        	  logger.info("Skipping same-day delivery");
    		          }
                  } else {
                	  if (shipMethodCarrier.equalsIgnoreCase("Y")) {
                		  Calendar deliveryDate = paVO.getDeliveryDate();
                	      Calendar shipND = paVO.getShipDateND();
                	      if (shipND != null) {
                	          nextAvailableDate = deliveryDate.getTime();
                   	    	  break;  
                	      } else {
                    	      Calendar ship2D = paVO.getShipDate2D();
                    	      if (ship2D != null) {
                    	          nextAvailableDate = deliveryDate.getTime();
                       	    	  break;
                	          } else {
                    	          Calendar shipGR = paVO.getShipDateGR();
                    	          if (shipGR != null) {
                    	              nextAvailableDate = deliveryDate.getTime();
                       	    	      break;  
                	              } else {
                    	              Calendar shipSA = paVO.getShipDateSA();
                    	              if (shipSA != null) {
            	                          if ( (ConfigurationUtil.getInstance().getFrpGlobalParm(MercentConstants.MERCENT_GLOBAL_CONTEXT, 
            	                        		  "SAT_DELIVERY_ALLOWED_DROPSHIP")).equalsIgnoreCase("N") ) {
          	                                  continue;
          	                              }        	              
                   	                      nextAvailableDate = deliveryDate.getTime();
                       	    	          break;
                    	              }
                	              }
                	          }
                	      }
            	      }
                  }
	    	  }
          }
      }
      catch(Exception e) {    	  
    	  logger.error("getNextAvailableDate Exception: " + e);
      }
	  return nextAvailableDate;
	}
	
	private String getShipMethod(Connection conn, String itemSku, Date deliveryDate, String zipCode,
			String shipMethodFlorist, String shipMethodCarrier) throws Exception {
		String shipMethod = null;
		boolean isShipMethodSet = false;
        SimpleDateFormat sdfTime = new SimpleDateFormat("yyyyMMddHHmm");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		try {  
			ProductAvailabilityBO paBO = new  ProductAvailabilityBO();
	      
			if(zipCode.length() > 5) {
				logger.info("Zip Code length is too long, using substring of 5 digits in - " + zipCode);
				zipCode = zipCode.substring(0,5);
			}
	      
		    ProductAvailVO paVO = paBO.getProductAvailability(conn,itemSku, deliveryDate, zipCode, null,null);
		    if (paVO.getIsAvailable()) {
    		    Calendar floristDate = paVO.getFloristCutoffDate();
		  
    		    if (shipMethodFlorist.equalsIgnoreCase("Y")) {
                    if (floristDate != null) {
        	            shipMethod = "SD";
        	            isShipMethodSet = true;
                    }
                }
    		    
    		    if (shipMethodCarrier.equalsIgnoreCase("Y")) {
                    Date today = new Date();
                    String todayString = sdfTime.format(today);
                    logger.debug("today: " + todayString);

                    if(paVO.getShipDateGR() != null && !isShipMethodSet){
                        Date shipDateGR = paVO.getShipDateGR().getTime();
                        String cutoffGR = paVO.getShipDateGRCutoff();
                        String cutoffString = sdf.format(shipDateGR) + cutoffGR;
                        logger.debug("GR cutoff: " + cutoffString);
                        if (todayString.compareTo(cutoffString) <= 0) {
            	            shipMethod = "GR";
            	            isShipMethodSet = true;
                        }
                    }
                    if(paVO.getShipDate2D() != null && !isShipMethodSet){
                        Date shipDate2D = paVO.getShipDate2D().getTime();
                        String cutoff2D = paVO.getShipDate2DCutoff();
                        String cutoffString = sdf.format(shipDate2D) + cutoff2D;
                        logger.debug("2F cutoff: " + cutoffString);
                        if (todayString.compareTo(cutoffString) <= 0) {
            	            shipMethod = "2F";
            	            isShipMethodSet = true;
                        }
                    }
                    if(paVO.getShipDateND() != null && !isShipMethodSet){
                        Date shipDateND = paVO.getShipDateND().getTime();
                        String cutoffND = paVO.getShipDateNDCutoff();
                        String cutoffString = sdf.format(shipDateND) + cutoffND;
                        logger.debug("ND cutoff: " + cutoffString);
                        if (todayString.compareTo(cutoffString) <= 0) {
            	            shipMethod = "ND";
            	            isShipMethodSet = true;
                        }
                    }
                    if(paVO.getShipDateSA() != null && !isShipMethodSet){
                        Date shipDateSA = paVO.getShipDateSA().getTime();
                        String cutoffSA = paVO.getShipDateSACutoff();
                        String cutoffString = sdf.format(shipDateSA) + cutoffSA;
                        logger.debug("SA cutoff: " + cutoffString);
                        if (todayString.compareTo(cutoffString) <= 0) {
            	            shipMethod = "SA";
            	            isShipMethodSet = true;
                        }
                    }
                    
                    if(paVO.getShipDateSU() != null && !isShipMethodSet){
                        Date shipDateSU = paVO.getShipDateSU().getTime();
                        String cutoffSU = paVO.getShipDateSUCutoff();
                        String cutoffString = sdf.format(shipDateSU) + cutoffSU;
                        logger.debug("SU cutoff: " + cutoffString);
                        if (todayString.compareTo(cutoffString) <= 0) {
            	            shipMethod = "SU";
            	            isShipMethodSet = true;
                        }
                    }
    		    }
		    }
		} catch(Exception e) {
	    	logger.error("getShipMethod Exception: " + e);
	    }
		return shipMethod;
	}
	
	public static BigDecimal getSafeBigDecimal(BigDecimal val)
	{
		if(val == null){
			return new BigDecimal("0");
		}
		return val;
	}
	public static BigDecimal getAsBigDecimal(String val)
	{
		if(val != null && val.trim().length()> 0)
		{
			try {
				BigDecimal bd = new BigDecimal(val);
				return bd;
			} catch (Exception e) {
				return new BigDecimal("0");
			}
		}
		return new BigDecimal("0");
	}
	
	private Date getMercentDeliveryDate(OrderItem orderItem) throws Exception 
	{
		Date deliveryDate = null;
		XMLGregorianCalendar deliveryStartDate = orderItem.getDeliveryStartDate();
		if(deliveryStartDate != null)
		{
			System.out.println("Given Delivery Date: "+deliveryStartDate.toString());
			Calendar deliveryCal = MercentUtils.fromXMLGregorianCalendar(deliveryStartDate);
			/*Calendar deliveryCal = Calendar.getInstance();
			deliveryCal.clear();
	        deliveryCal.set(Calendar.DATE, deliveryStartDate.getDay());
	        deliveryCal.set(Calendar.MONTH, deliveryStartDate.getMonth()-1);
	        deliveryCal.set(Calendar.YEAR, deliveryStartDate.getYear());
	        */       
	        deliveryDate = deliveryCal.getTime();
		}		
		logger.debug("Delivery Date from Mercent: "+deliveryDate);
		return deliveryDate;		
	}

}
