/**
 * 
 */
package com.ftd.mercent.bo;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.ftd.mercent.dao.MercentFeedDAO;
import com.ftd.mercent.dao.MercentOrderFulfillmentDAO;
import com.ftd.mercent.utilities.MercentConstants;
import com.ftd.mercent.utilities.MercentUtils;
import com.ftd.mercent.vo.OrderFulfillmentFeedVO;
import com.ftd.mercent.vo.mercent.MercentFeed;
import com.ftd.mercent.vo.mercent.OrderFulfillment;
import com.ftd.mercent.vo.mercent.OrderFulfillment.FulfillmentData;
import com.ftd.mercent.vo.mercent.OrderFulfillment.Item;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * @author smeka
 *
 */
public class MercentOrderFulfillmentFeedBO extends MercentFeedBO {
	
	private static Logger logger = new Logger("com.ftd.mercent.bo.MercentOrderFulfillmentFeedBO");

	/**
	 * creates the fulfillment feeds and saves to MRCNT_FEED_MASTER
	 */
	@SuppressWarnings("unchecked")
	public void createMercentFeed() {
		
		if(logger.isDebugEnabled()) {
			logger.debug("********* Entering createMercentOrderFulfillFeed() *********");
		}
		
		Connection conn = null;
		MercentFeedDAO feedDAO = null;
		StringBuffer systemMessage = new StringBuffer();
		
		try {			
			conn = MercentUtils.getConnection();
			feedDAO = new MercentOrderFulfillmentDAO(conn);
			List<OrderFulfillmentFeedVO> orderFulfillments = 
				(List<OrderFulfillmentFeedVO>) feedDAO.getMercentFeedData(MercentConstants.NEW_FEED_STATUS);
			
			if(orderFulfillments == null || orderFulfillments.isEmpty()){
	    		logger.info("No new mercent Order Fulfillments Feed data found");
	    		return;
	    	}		
			
			String feedId = createNewMercentFeed(this.buildOrderFulfillFeed(orderFulfillments), MercentConstants.MRCNT_ORDER_FULFILL_FEED,conn);				
			if(feedId !=null && !StringUtils.isEmpty(feedId)) {
				logger.info("Succesfully created feed with Id: " + feedId);
				// get the list of primary feed ids/ order fulfillment Ids to be updated.
				List<String> mercentFeedDataIds = new ArrayList<String>(orderFulfillments.size());
				for (OrderFulfillmentFeedVO orderFulfillmentFeedVO : orderFulfillments) {
					mercentFeedDataIds.add(orderFulfillmentFeedVO.getOrderFulfillmentId());
				}
				
				if(!feedDAO.updateMercentFeedId(mercentFeedDataIds, feedId, MercentConstants.MRCNT_ORDER_FULFILL_FEED, MercentConstants.SENT_FEED_STATUS)) {
					systemMessage.append("Unable to update the feed Id for the mercent fulfillment orders. rolling back transaction.");
				}
				
				return;
			}
			systemMessage.append("Unable to insert the mercent feed for the mercent fulfillment orders. rolling back transaction.");
		
		} catch (Exception e) {
			logger.error(" Error caught creating mercent order fulfillment feeds. sending system message. " + e);
			e.printStackTrace();
			systemMessage.append("Error caught creating mercent fulfillment feeds." + e.getMessage());
		}finally {
			
			if(!systemMessage.toString().isEmpty()) {
				// add code to send system messages
			}
			
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
		if(logger.isDebugEnabled()) {
			logger.debug("********* Exiting createMercentOrderFulfillFeed() *********");
		}		
	}
	
	/**
	 * @param fulfillFeedData
	 * @return
	 */
	private MercentFeed buildOrderFulfillFeed(List<OrderFulfillmentFeedVO> fulfillFeedList) {
		MercentFeed mercentFeed = new MercentFeed();
		Map<String, List<OrderFulfillmentFeedVO>> orderFullfillMap = new HashMap<String, List<OrderFulfillmentFeedVO>>();
		
		for (OrderFulfillmentFeedVO feedDetail : fulfillFeedList) {
   		if(!orderFullfillMap.containsKey(feedDetail.getMerchantOrderID())){
   			orderFullfillMap.put(feedDetail.getMerchantOrderID(), new ArrayList<OrderFulfillmentFeedVO>());
   		}
   		orderFullfillMap.get(feedDetail.getMerchantOrderID()).add(feedDetail);
		}
		
		Set<String> keySet = orderFullfillMap.keySet();
		
		for (String key : keySet) {
		
			List<OrderFulfillmentFeedVO> feedDetailList = orderFullfillMap.get(key);
			
			for (OrderFulfillmentFeedVO fulfillFedd : feedDetailList) {		
				
				OrderFulfillment orderFulfillment = new OrderFulfillment();
				orderFulfillment.setChannelOrderID(fulfillFedd.getChannelOrderID());
				orderFulfillment.setChannelName(fulfillFedd.getChannelName());
			
				FulfillmentData fulfillmentData = new FulfillmentData();
				fulfillmentData.setCarrier(fulfillFedd.getCarrier());

				if (fulfillFedd.getCarrier().equalsIgnoreCase("Florist")) {
						fulfillmentData.setCarrier("FTD Florist");
				} else {
					fulfillmentData.setCarrier(fulfillFedd.getCarrier());
				}			
			
				fulfillmentData.setShippingMethod(fulfillFedd.getShippingMethod());
				fulfillmentData.setShippingTrackingNumber(fulfillFedd.getShippingTrackingNumber());			
				orderFulfillment.setFulfillmentData(fulfillmentData);	
			
				orderFulfillment.setFulfillmentDate(fulfillFedd.getFulfillmentDate());			
			
				Item item = new Item();
				item.setChannelOrderItemID(fulfillFedd.getChannelOrderItemId());
				item.setQuantity(MercentConstants.NUMBER_OF_ITEMS);
				
				orderFulfillment.getItem().add(item);	
				mercentFeed.getOrderFulfillment().add(orderFulfillment);
			}				
			
		}
		
		return mercentFeed;
	}
}
