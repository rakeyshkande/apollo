package com.ftd.mercent.bo;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.Marshaller;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.mercent.dao.MercentProductFeedDAO;
import com.ftd.mercent.utilities.MercentConstants;
import com.ftd.mercent.utilities.MercentUtils;
import com.ftd.mercent.vo.MercentProductAttributesVO;
import com.ftd.mercent.vo.MercentProductFeedDetailVO;
import com.ftd.mercent.vo.MercentProductFeedVO;
import com.ftd.mercent.vo.mercent.Amazon;
import com.ftd.mercent.vo.mercent.AvailabilityInformation;
import com.ftd.mercent.vo.mercent.BasicInformation;
import com.ftd.mercent.vo.mercent.ChannelSpecificInformation;
import com.ftd.mercent.vo.mercent.ConditionType;
import com.ftd.mercent.vo.mercent.DescriptiveInformation;
import com.ftd.mercent.vo.mercent.ImageInformation;
import com.ftd.mercent.vo.mercent.MercentFeed;
import com.ftd.mercent.vo.mercent.Product;
import com.ftd.mercent.vo.mercent.PricingInformation;
import com.ftd.mercent.vo.mercent.ProductRelationships;
import com.ftd.mercent.vo.mercent.ProductRelationships.Variation;
import com.ftd.mercent.vo.mercent.ShippingInformation;
import com.ftd.mercent.vo.mercent.TaxCodeType;
import com.ftd.mercent.vo.mercent.UniqueIdentifiers;

/**
 * @author sbring
 *
 */
public class MercentProductFeedBO extends MercentFeedBO{

	private Logger logger = new Logger("com.ftd.mercent.bo.MercentProductFeedBO");

	public MercentProductFeedBO() {

	}

	/**
	 * Processes the Product Feed and sends it to Mercent.
	 * 
	 * @return
	 * @throws Exception
	 */
	public void createMercentFeed(){
		logger.debug("processProductFeed");
		Connection conn = null;
		StringBuffer systemMessage = new StringBuffer();
		try{
			conn = MercentUtils.getConnection();
			MercentProductFeedDAO productFeedDAO = new MercentProductFeedDAO(conn);
			int productsPerFeed = Integer.parseInt(MercentUtils.getFrpGlobalParm
					(MercentConstants.MERCENT_GLOBAL_CONTEXT, MercentConstants.MERCENT_PRODUCTS_IN_FEED));
			int totalNumberOfProducts = productFeedDAO.getProductFeedCount();
			for (int i=0 ; i<Math.ceil((double)totalNumberOfProducts/productsPerFeed); i++){
				MercentProductFeedVO productFeedData = productFeedDAO.getProductFeedData(productsPerFeed);
				if(productFeedData.getFeedDetails() ==null || productFeedData.getFeedDetails().isEmpty()){
					logger.debug("There are no product feed updates to send to Mercent");
					return;
				}
				MercentFeed mercentProductFeed = buildMercentProductFeed(productFeedData,conn);
				if (mercentProductFeed.getProduct().isEmpty()){
					logger.debug("There are no product feed updates to send to Mercent");
					return;
				}
				String feedId = saveProductFeedToFeedMaster(mercentProductFeed,productFeedDAO);
				List<MercentProductFeedDetailVO> feedDetails = productFeedData.getFeedDetails();
				for (MercentProductFeedDetailVO productFeedVo : feedDetails) 
				{
					productFeedVo.setFeedStatus(MercentConstants.FEED_STATUS_SENT);
					productFeedDAO.saveFeedStatus(productFeedVo,feedId);
				}
			}
		}
		catch (Exception e){
			logger.error(" Error caught creating mercent product feed. sending system message. " + e);
			e.printStackTrace();
			systemMessage.append("Error caught creating mercent Product feed." + e.getMessage());
		}
		finally
		{
			if(conn!= null){ try { conn.close(); } catch (SQLException e) {} }
		}
	}

	private MercentFeed buildMercentProductFeed(MercentProductFeedVO productFeedData, Connection conn) throws Exception{
		logger.debug("buildMercentProductFeed()");
		MercentFeed productFeed = new MercentFeed();
		List<MercentProductFeedDetailVO> productFeedDetails = productFeedData.getFeedDetails();
		for (MercentProductFeedDetailVO productFeedDetail : productFeedDetails){
			MercentProductFeedDAO productFeedDAO = new MercentProductFeedDAO(conn);
			MercentProductAttributesVO productAttributesVO =  productFeedDAO.getProductAttributes(productFeedDetail.getProductFeedId(),productFeedDetail.getProductId());
			String isProductCompanyMapExists = productFeedDAO.getProductCompanyMapping(productFeedDetail.getProductId());
			String itemType = productFeedDAO.getMerchantCategory(productFeedDetail.getProductId());
			for (int i = 1 ; i <= MercentConstants.NUMBER_OF_PRODUCT_VARIANTS ; i++){
				if (i==1 || (i == 2 && productFeedDetail.getDeluxePrice()!=null && !(BigDecimal.ZERO.compareTo(productFeedDetail.getDeluxePrice())==0)
						|| (i ==3 && productFeedDetail.getPremiumPrice()!=null && !(BigDecimal.ZERO.compareTo(productFeedDetail.getPremiumPrice())==0)))){
					Product mercentProduct = new Product();
					mercentProduct.setSKU(setProductVariantSKU(i,productFeedDetail.getProductId()));

					BasicInformation basicInformation = new BasicInformation();
					basicInformation.setBrand(MercentConstants.BRAND);
					basicInformation.setCondition(ConditionType.NEW);
					basicInformation.setDescription(productFeedDetail.getDescprition());
					basicInformation.setFullHTMLDescription(productFeedDetail.getDescprition());
					basicInformation.setProductURL(buildProductURL(productFeedDetail.getNovatorId()));
					basicInformation.setManufacturer(MercentConstants.MANUFACTURER);
					basicInformation.setTitle(productFeedDetail.getTitle());

					List<String> bulletPoints = productAttributesVO.getBulletPoint();					
					for (String bulletPoint : bulletPoints) {
						basicInformation.getFeature().add(bulletPoint);
					}
					
					mercentProduct.setBasicInformation(basicInformation);


					DescriptiveInformation descriptiveInformation =  new DescriptiveInformation();
					descriptiveInformation.setNumberOfItems(MercentConstants.NUMBER_OF_ITEMS);
					DescriptiveInformation.CustomFields customFields = new DescriptiveInformation.CustomFields();
					
					if (productFeedDetail.getExceptionStartDate()!=null){
						DescriptiveInformation.CustomFields.CustomField customField1 = new DescriptiveInformation.CustomFields.CustomField();
						customField1.setName(MercentConstants.CUSTOM_FIELD_EXCEPTION_START_DATE);
						customField1.setValue(MercentUtils.sqlDate2MrcDateString(productFeedDetail.getExceptionStartDate()));
						customFields.getCustomField().add(customField1);
					}
					
					if (productFeedDetail.getExceptionEndDate()!=null){
						DescriptiveInformation.CustomFields.CustomField customField2 = new DescriptiveInformation.CustomFields.CustomField();
						customField2.setName(MercentConstants.CUSTOM_FIELD_EXCEPTION_END_DATE);
						customField2.setValue(MercentUtils.sqlDate2MrcDateString(productFeedDetail.getExceptionEndDate()));
						customFields.getCustomField().add(customField2);
						descriptiveInformation.setCustomFields(customFields);
					}

					List<String> intendedUse = productAttributesVO.getIntendedUse();
					for (String productUsedFor : intendedUse){
						descriptiveInformation.getProductUsedFor().add(productUsedFor);
					}

					List<String> searchTerms = productAttributesVO.getSearchTerms();
					for (String searchTerm : searchTerms){
						descriptiveInformation.getSearchTerm().add(searchTerm);
					}

					mercentProduct.setDescriptiveInformation(descriptiveInformation);


					ImageInformation imageInformation = new ImageInformation();
					imageInformation.setMainImageURL(buildImageURL(productFeedDetail.getNovatorId(),i));
					mercentProduct.setImageInformation(imageInformation);


					PricingInformation pricingInformation = new PricingInformation();
					pricingInformation.setStandardPrice(setProductVariantPrice(i,productFeedDetail));
					pricingInformation.setTaxCode(getTaxCode(productFeedDetail.getNoTaxCode()));
					mercentProduct.setPricingInformation(pricingInformation);


					ProductRelationships productRelationships = new ProductRelationships();
					if (itemType!=null && !itemType.isEmpty()){
						productRelationships.setMerchantCategory(itemType);
					}
					else
					{
						productRelationships.setMerchantCategory(MercentUtils.getFrpGlobalParm
								(MercentConstants.MERCENT_GLOBAL_CONTEXT, MercentConstants.MERCENT_CATEGORY));
					}
					mercentProduct.setProductRelationships(productRelationships);


					AvailabilityInformation availabilityInformation = new AvailabilityInformation();
					if (productFeedDetail.getAvailabilityStatus().equalsIgnoreCase("A") && isProductCompanyMapExists.equalsIgnoreCase("Y")){
						availabilityInformation.setQuantity(Integer.parseInt(MercentUtils.getFrpGlobalParm
								(MercentConstants.MERCENT_GLOBAL_CONTEXT, MercentConstants.MERCENT_QUANTITY)));
					}
					else
					{
						availabilityInformation.setQuantity(0);
					}
					availabilityInformation.setAvailabilityCode(setAvailabilityStatus(productFeedDetail.getAvailabilityStatus(),isProductCompanyMapExists));

					availabilityInformation.setFulfillmentLatency(new Integer(Integer.parseInt(MercentUtils.getFrpGlobalParm
							(MercentConstants.MERCENT_GLOBAL_CONTEXT, MercentConstants.MERCENT_FULFILLMENT_LATENCY))));
					mercentProduct.setAvailabilityInformation(availabilityInformation);


					ShippingInformation shippingInformation = new ShippingInformation();
					shippingInformation.setShipping2Day(new BigDecimal(MercentUtils.getFrpGlobalParm
							(MercentConstants.MERCENT_GLOBAL_CONTEXT, MercentConstants.MERCENT_SHIPPING_2_DAY)));
					shippingInformation.setShippingOvernight(new BigDecimal(MercentUtils.getFrpGlobalParm
							(MercentConstants.MERCENT_GLOBAL_CONTEXT, MercentConstants.MERCENT_SHIPPING_OVERNIGHT)));
					shippingInformation.setShippingServiceAdditionalCost(new BigDecimal(MercentUtils.getFrpGlobalParm
							(MercentConstants.MERCENT_GLOBAL_CONTEXT, MercentConstants.MERCENT_SHIPPING_ADDITIONAL_COST)));
					shippingInformation.setShippingStandard(new BigDecimal(MercentUtils.getFrpGlobalParm
							(MercentConstants.MERCENT_GLOBAL_CONTEXT, MercentConstants.MERCENT_SHIPPING_STANDARD)));
					mercentProduct.setShippingInformation(shippingInformation);
					
					Amazon amazon = new Amazon();
					List<String> amazonIntendedUse = productAttributesVO.getIntendedUse();
					for (String usedFor : amazonIntendedUse){
						amazon.getUsedFor().add(usedFor);
					}
					
					List<String> targetAudiences = productAttributesVO.getMrcTargetAudience();
					for (String targetAudience : targetAudiences){
						amazon.getTargetAudience().add(targetAudience);
					}
					
					List<String> subjectMatters = productAttributesVO.getMrcSubjectMatter();
					for (String subjectMatter : subjectMatters){
						amazon.getSubjectContent().add(subjectMatter);
					}
					
					List<String> otherAttributes = productAttributesVO.getMrcOtherAttributes();
					for (String otherAttribute : otherAttributes){
						amazon.getOtherAttribute().add(otherAttribute);
					}
					
					if (itemType!=null && !itemType.isEmpty()){
						amazon.setItemType(itemType);
					}
					else {
						amazon.setItemType(MercentUtils.getFrpGlobalParm
								(MercentConstants.MERCENT_GLOBAL_CONTEXT, MercentConstants.MERCENT_CATEGORY));
					}
					ChannelSpecificInformation channelSpecificInformation = new ChannelSpecificInformation();
					channelSpecificInformation.setAmazon(amazon);
					mercentProduct.setChannelSpecificInformation(channelSpecificInformation);

					productFeed.getProduct().add(mercentProduct);
				}
			}
		}
		return productFeed;
	}

	private String setAvailabilityStatus(String availabilityStatus, String isProductCompanyMapExists) {
		String availabilityCode = null;
		if (availabilityStatus.equalsIgnoreCase("A") && isProductCompanyMapExists.equalsIgnoreCase("Y"))
		{
			availabilityCode = MercentConstants.AVAILABILITY_CODE_INSTOCK;
		}
		else {
			availabilityCode = MercentConstants.AVAILABILITY_CODE_OUTOFSTOCK;
		}
		return availabilityCode;
	}

	private TaxCodeType getTaxCode(String noTaxCode) {
		TaxCodeType taxCodeType = null;
		if (noTaxCode!=null && noTaxCode.equalsIgnoreCase("Y")){
			taxCodeType = TaxCodeType.fromValue(MercentConstants.ALWAYS_NON_TAXABLE);
		}
		else {
			taxCodeType = TaxCodeType.fromValue(MercentConstants.ALWAYS_TAXABLE);
		}
		return taxCodeType;
	}

	private BigDecimal setProductVariantPrice(int i,
			MercentProductFeedDetailVO productFeedDetail) {
		BigDecimal productPrice= new BigDecimal(0);
		switch (i) {
		case 1:  productPrice = productFeedDetail.getStandardPrice();
		break;
		case 2:  productPrice = productFeedDetail.getDeluxePrice();
		break;
		case 3:  productPrice = productFeedDetail.getPremiumPrice();
		break;
		}
		return productPrice;
	}

	private String setProductVariantSKU(int i, String productId) {
		String sku = null;
		switch (i) {
		case 1:  sku = productId + MercentConstants.CATALOG_SUFFIX_STANDARD;
		break;
		case 2:  sku = productId + MercentConstants.CATALOG_SUFFIX_DELUXE;
		break;
		case 3:  sku = productId + MercentConstants.CATALOG_SUFFIX_PREMIUM;
		break;
		}
		return sku;
	}

	private String buildImageURL(String novatorId, int i) {
		String imageSize = MercentUtils.getFrpGlobalParm(MercentConstants.MERCENT_GLOBAL_CONTEXT, MercentConstants.MERCENT_IMAGE_SIZE);
		String imageUrl = MercentUtils.getFrpGlobalParm(MercentConstants.MERCENT_GLOBAL_CONTEXT, MercentConstants.MERCENT_IMAGE_URL);
		String imageFormat = MercentUtils.getFrpGlobalParm(MercentConstants.MERCENT_GLOBAL_CONTEXT, MercentConstants.MERCENT_IMAGE_FORMAT);
		String mainImageUrl = null;
		switch (i) {
		case 1:  mainImageUrl = imageUrl + novatorId + "_" + imageSize + imageFormat;
		break;
		case 2:  mainImageUrl = imageUrl + novatorId +"_" + imageSize + MercentConstants.CATALOG_SUFFIX_DELUXE + imageFormat;
		break;
		case 3:  mainImageUrl = imageUrl + novatorId + "_" +  imageSize + MercentConstants.CATALOG_SUFFIX_PREMIUM + imageFormat;
		break;
		}
		return mainImageUrl;
	}
	private String buildProductURL(String novatorId) {
		String productUrl = MercentUtils.getFrpGlobalParm(MercentConstants.MERCENT_GLOBAL_CONTEXT, MercentConstants.MERCENT_PRODUCT_URL);
		return productUrl + novatorId;
	}

	public String saveProductFeedToFeedMaster(MercentFeed mercentFeed, MercentProductFeedDAO productFeedDAO) throws Exception {
		Map<String, Object> props = new HashMap<String, Object>();
		props.put(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		String feedContent = MercentUtils.marshallAsXML(mercentFeed, props); 
		String feedId = productFeedDAO.saveFeedToFeedMaster(feedContent,MercentConstants.PRODUCT_FEED,MercentConstants.FEED_STATUS_NEW);
		return feedId;
	}
}
