/**
 * 
 */
package com.ftd.mercent.bo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.xml.bind.Marshaller;
import org.apache.commons.lang.StringUtils;
import com.ftd.mercent.dao.MercentFeedDAO;
import com.ftd.mercent.exceptions.MercentException;
import com.ftd.mercent.utilities.FtpUtil;
import com.ftd.mercent.utilities.MercentConstants;
import com.ftd.mercent.utilities.MercentUtils;
import com.ftd.mercent.vo.FTPServerVO;
import com.ftd.mercent.vo.mercent.MercentFeed;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * @author smeka
 *
 */
public abstract class MercentFeedBO {

	private static Logger logger = new Logger("com.ftd.mercent.bo.MercentFeedBO");
	private final static int MAX_FILENAME_RETRY_COUNT = 1000;

	public abstract void createMercentFeed();


	/** Gets the mercent feeds with status 'NEW' for a given 
	 * feed type and saves it to remote server.
	 * updates the feed status of each feed saved on to remote server.
	 * @param feedType
	 */
	public void saveMercentFeed(String feedType) {
		if(logger.isDebugEnabled()) {
			logger.debug("********* Entering saveMercentFeed() *********");			
		}

		Connection conn = null;
		MercentFeedDAO feedDAO = null;
		String systemMessage = null;
		try {			
			conn = MercentUtils.getConnection();
			feedDAO = new MercentFeedDAO(conn);

			FTPServerVO serverVO = MercentUtils.populateFTPServerDetails();

			if(!StringUtils.isEmpty(serverVO.getFtpServerLocation()) && 
					!StringUtils.isEmpty(serverVO.getFtpServerLogon()) && 
					!StringUtils.isEmpty(serverVO.getFtpServerPwd())) 
			{

				Map<String,Clob> mercentFeeds = feedDAO.getMercentFeedsByStatus(MercentConstants.NEW_FEED_STATUS, feedType);

				if(mercentFeeds == null || mercentFeeds.isEmpty()){
					logger.info("No new mercent feeds data found to be sent to remote server");
					return;
				}

				Set<String> feedIds = mercentFeeds.keySet();
				// increment_second_val is the value that is added to the filename timestamp(seconds) to generate a unique filename
				int increment_second_val = 0;
				for (String feedId : feedIds) {
					String fileName = MercentUtils.deriveRemoteFeedFileName(null, feedType,increment_second_val);
					serverVO.setFeedFileName(fileName);
					boolean statusUpdated = feedDAO.updateMercentFeedStatus(feedId, MercentConstants.SENT_FEED_STATUS, feedType, serverVO);
					// retry_count is the number of retries that we make to generate a unique filename, avoid infinite loop
					int retry_count = 1;
					
					while (!statusUpdated){
						increment_second_val = increment_second_val+1;
						//we are trying to fetch Unique fileName to address duplicate fileName issue.
						fileName = MercentUtils.deriveRemoteFeedFileName(null, feedType,increment_second_val);
						serverVO.setFeedFileName(fileName);
						statusUpdated = feedDAO.updateMercentFeedStatus(feedId, MercentConstants.SENT_FEED_STATUS, feedType, serverVO);
						logger.debug("**************************************************");
						logger.debug("Global Count is: "+ increment_second_val +"Local Count is: "+ retry_count);
						retry_count++;
						if(retry_count >= MAX_FILENAME_RETRY_COUNT){
							logger.debug("statusUpdated: " + statusUpdated + " and the count is : " + retry_count );
							logger.error("Unable to create the fileName for feedId: "+ feedId + " fileName: " + fileName +" as it exceeds max try count");
							break;
						}
					}

					if(statusUpdated){
						boolean fileSent = this.ftpFile(convertClobToString(mercentFeeds.get(feedId)), feedType, serverVO);
						if(fileSent) {
							logger.info("Succesfully saved the file " + serverVO.getFeedFileName() 
									+ "  to remote server and updated the status to 'SENT' in Apollo MRCNT_FEED_MASTER for feedId: " + feedId);
						} else {
							feedDAO.updateMercentFeedStatus(feedId, MercentConstants.NEW_FEED_STATUS, feedType, null);
							logger.info("updated the status to 'NEW' in Apollo MRCNT_FEED_MASTER for feedId: " + feedId + 
									" as the file with filename: " + fileName + "is not succesfully uploaded to Server");
							systemMessage = "Unable to save the feed file to remote server for feedId " + feedId;
							logger.error(systemMessage);
						}
					}
				}
			} else {
				systemMessage = "Insufficient/Invalid remote FTP server details to save the feeds to remote server";
				logger.error(systemMessage);
			}

		} catch (Exception e) {
			systemMessage = "Exception caught and unable to save the feed to remote server for feedType: " + feedType;
			logger.error(systemMessage);
			logger.error(e.getMessage());
		} finally {
			if(systemMessage!=null) {
				MercentUtils.sendNoPageSystemMessage(systemMessage);
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
	}

	private String convertClobToString(Clob clob) {
		StringBuilder sb = new StringBuilder();
		try {
			Reader reader = clob.getCharacterStream();
			BufferedReader br = new BufferedReader(reader);
			String line;
			while(null != (line = br.readLine())) {
				sb.append(line);
			}
			br.close();
		} catch (SQLException e) {
			logger.error("SQl Exception occured while converting clob to String" + e);
		} catch (IOException e) {
			logger.error("Exception occured while converting clob to String" + e);
		}
		return sb.toString();
	}


	/** Ftp the file to the remote server.
	 * @param mercentFeed
	 * @param feedType
	 * @return
	 * @throws Exception
	 */
	private boolean ftpFile(String mercentFeed, String feedType, FTPServerVO serverVO) {		
		FtpUtil ftpUtil = new FtpUtil();
		try {			
			ftpUtil.login(serverVO.getFtpServerLocation(), serverVO.getFtpServerLogon(), serverVO.getFtpServerPwd());

			int count = 0;
			boolean isFileSent = false;

			while (!isFileSent && count < MercentConstants.RETRY_COUNT) {
				try {
					ftpUtil.putFile(mercentFeed.getBytes(), serverVO.getFeedFileName(), serverVO.getFtpServerDirectory(), false);	
					isFileSent = true;
				} catch (Exception e) {
					logger.error("Unable to save the file to remote FTP server: attempt - " + count++ + " Error caught: " + e.getMessage());				
				}
			}

			return isFileSent;

		} catch(Exception e) {
			throw new MercentException("Unable to save the file to remote FTP server");
		} finally {
			try{
				ftpUtil.logout();
			} catch(Exception e){
				logger.error(e.getMessage());
			}
		}
	}

	/**
	 * @param mercentFeed
	 * @param feedType
	 * @return
	 * @throws Exception 
	 */
	public String createNewMercentFeed(MercentFeed mercentFeed, String feedType,Connection conn) {
		if(logger.isDebugEnabled()) {
			logger.debug("********* Entering createMercentFeed() *********");			
		}
		try {

			Map<String, Object> props = new HashMap<String, Object>();
			props.put(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

			String feedContent = MercentUtils.marshallAsXML(mercentFeed, props);	

			if(logger.isDebugEnabled()) {
				logger.debug("Mercent Feed XML : " + feedContent);
			}

			MercentFeedDAO feedDAO = new MercentFeedDAO(conn);

			return feedDAO.insertMercentFeed(feedContent, feedType);	

		} catch(Exception e){
			throw new MercentException("Unable to save the feed to apollo for feedType: " + feedType);			
		} 
	}
}
