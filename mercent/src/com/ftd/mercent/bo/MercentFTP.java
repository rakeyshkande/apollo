/**
 * 
 */
package com.ftd.mercent.bo;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import com.ftd.mercent.exceptions.MercentException;
import com.ftd.mercent.utilities.FtpUtil;
import com.ftd.mercent.utilities.MercentConstants;
import com.ftd.mercent.utilities.MercentUtils;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * @author skatam
 *
 */
public class MercentFTP 
{
	private Logger logger = new Logger("com.ftd.mercent.bo.MercentFTP");
	
	public List<File> getOrderFeedsFromMercent() 
	{
		
		List<File> respFiles = new ArrayList<File>();
		
		try 
		{
			FtpUtil ftpUtil = new FtpUtil();
						
			String ftpServer = MercentUtils.getMercentFrpGlobalParm("LOCAL_FTP_SERVER");
			String ftpUsername = MercentUtils.getMercentSecureGlobalParm("LOCAL_FTP_SERVER_USERNAME");
			String ftpPassword = MercentUtils.getMercentSecureGlobalParm("LOCAL_FTP_SERVER_PWD");
			String ftpOrderReportsLocation = MercentUtils.getMercentFrpGlobalParm("LOCAL_FTP_ORDER_FEEDS_DIR");
			String ftpOrderReportsArchiveLocation = MercentUtils.getMercentFrpGlobalParm("LOCAL_FTP_ORDER_FEEDS_ARCHIVE_DIR");
			 
			String localFeedDir = MercentUtils.getMercentFrpGlobalParm("LOCAL_ORDER_FEEDS_DIR");
			
			ftpUtil.login(ftpServer, ftpUsername, ftpPassword);
			
			
	      String[] allFiles = ftpUtil.getDirectoryList(ftpOrderReportsLocation,
	    		  						MercentConstants.ORDER_REPORT_FILE_EXTN);
	      if(allFiles == null || allFiles.length == 0)
	      {
	    	  logger.info("No Order Report Xml files found on Mercent FTP server");
	      }
	      else
	      {
	    	  for (int x=0; x < allFiles.length; x++) 
		      {
	    		  String filePath = allFiles[x];
		          logger.debug("Processing Remote OrderReport Feed: "+filePath);
		          String fileName = filePath;
		          if(filePath.contains("/")){
		        	  fileName = filePath.substring(filePath.lastIndexOf("/")+1);
		          }
		          logger.debug("Filename :"+fileName);
		          byte[] fileData = ftpUtil.getFileData(fileName, ftpOrderReportsLocation);
		          
		          try {
		        	  File localFeedDirectory = new File(localFeedDir);
		        	  if(!localFeedDirectory.exists())
		        	  {
		        		  localFeedDirectory.mkdirs();
		        	  }
		        	  File localFile = new File(localFeedDir + System.currentTimeMillis()+"_"+ fileName);
					  if(!localFile.exists()) {
						  localFile.createNewFile();
					  }
					  OutputStream os = new FileOutputStream(localFile);
					  os.write(fileData);
					  os.close();
					  logger.debug("Saved to Local Directory: "+localFile.getAbsolutePath());
					  respFiles.add(localFile);
					  
					  // Rename
			          String archiveFileName = ftpOrderReportsArchiveLocation + 
			          								System.currentTimeMillis()+"_"+fileName;
			          ftpUtil.renameFile(filePath, archiveFileName);
			          logger.debug("Archived file on remote server to: " + archiveFileName);
			          
				} catch (Exception e) {
					logger.error("Unable to save feed on local server. Error: "+e.getMessage());
					logger.error(e);
					MercentUtils.sendPageSystemMessage("Unable to process OrderReport Feed :"+filePath);
				}		          
		      }
	      }
	      ftpUtil.logout();

		} catch (Exception e) {
			logger.error(e);
			throw new MercentException(e);
		}
		
		return respFiles;
	}
}
