package com.ftd.mercent.bo;

import java.io.File;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import com.ftd.mercent.dao.MercentOrderFeedDAO;
import com.ftd.mercent.exceptions.MercentException;
import com.ftd.mercent.utilities.MercentConstants;
import com.ftd.mercent.utilities.MercentUtils;
import com.ftd.mercent.vo.MercentOrderFeed;
import com.ftd.mercent.vo.mercent.MercentFeed;
import com.ftd.mercent.vo.mercent.Order;
import com.ftd.osp.utilities.plugins.Logger;

public class MercentOrderReportBO 
{
  
    private Logger logger = new Logger("com.ftd.mercent.bo.MercentOrderReportBO");
    
    private MercentOrderFeedDAO orderFeedDAO = null;
    
    public MercentOrderReportBO(Connection conn) {
    	this.orderFeedDAO = new MercentOrderFeedDAO(conn);
	}

	public void process(MercentOrderFeed mercentOrderFeed) 
    {
		//MercentOrderFeed mercentOrderFeed = this.getOrdersFromMercent();
    	if(mercentOrderFeed == null || mercentOrderFeed.getMercentFeed()==null 
    		|| mercentOrderFeed.getMercentFeed().getOrder() == null
    		|| mercentOrderFeed.getMercentFeed().getOrder().isEmpty())
    	{
    		logger.debug("No Order Feed received from Mercent.");
    		return;
    	}
    	this.saveMercentOrderFeed(mercentOrderFeed);
    	
	}
    
    public List<MercentOrderFeed> getOrdersFromMercentFTPServer()
	{
    	logger.debug("Pulling Orders from Mercent...");
    	List<MercentOrderFeed> feeds = new ArrayList<MercentOrderFeed>();
		try {
			MercentFTP mercentFTP = new MercentFTP();
			List<File> orderFeedFiles = mercentFTP.getOrderFeedsFromMercent();
			for (File orderFeedFile : orderFeedFiles) 
			{
				if(orderFeedFile != null)
				{
					MercentFeed mercentFeed = 
						(MercentFeed) MercentUtils.marshallAsObject(orderFeedFile, MercentFeed.class);
					MercentOrderFeed feed = new MercentOrderFeed(mercentFeed);
					feeds.add(feed);
					this.archiveLocalOrderReportFeed(orderFeedFile);
				}
			}
		}
		catch (Exception e) 
		{
			throw new MercentException(e);
		}
	    return feeds;
		
	}
	
    private void archiveLocalOrderReportFeed(File orderFeedFile) 
    {
    	String localFeedArchiveDir = MercentUtils.getMercentFrpGlobalParm(MercentConstants.LOCAL_ORDER_FEEDS_ARCHIVE_DIR);
		File dir = new File(localFeedArchiveDir);
    	if(!dir.exists())
    	{
    		dir.mkdirs();
    	}
    	String archiveFileName = localFeedArchiveDir + orderFeedFile.getName();
    	File archiveFile = new File(archiveFileName);
		orderFeedFile.renameTo(archiveFile);
		logger.debug("Archived file on local server to: "+archiveFile.getAbsolutePath());
	}

	private void saveMercentOrderFeed(MercentOrderFeed mercentOrderFeed) 
	{    	
    	//SAVE ORDER_REPORT into MRCNT_ORDER_REPORT table.
    	String orderReportId = orderFeedDAO.insertMercentOrderReport(mercentOrderFeed.getMercentFeed());
    	List<Order> orders = mercentOrderFeed.getMercentFeed().getOrder();
    	for (Order order : orders) 
    	{
    		boolean validMercentChannelName = orderFeedDAO.isValidMercentChannelName(order.getChannelName());
    		if(!validMercentChannelName)
    		{
    			String msg = "Received Mercent Order with Invalid ChannelName :"+order.getChannelName()+
    							" in OrderReport with ID :"+orderReportId+". Not processing this Order.";
    			logger.error(msg);
    			MercentUtils.sendNoPageSystemMessage(msg);
    		}
    		else
    		{
    			boolean duplicateOrder = orderFeedDAO.isDuplicateOrder(order);
        		if(duplicateOrder)
        		{
        			String msg = "Received a duplicate Mercent Order. Channel:"
    					+order.getChannelName()+", ChannelOrderId: "+order.getChannelOrderID()
    					+" in OrderReport with ID :"+orderReportId;
        			logger.error(msg);
        			MercentUtils.sendNoPageSystemMessage(msg);
        		}
        		else
        		{
        			String mercentOrderNumber = this.saveMercentOrder(orderReportId, order);
            		this.triggerMercentOrdersProcessing(mercentOrderNumber);
        		}
    		}    		
		}
	}
   
	private String saveMercentOrder(String orderReportId, Order order) 
    {
		logger.debug("Saving Order:"+order.getChannelOrderID());
    	//Save ORDERS into MRCNT_ORDER table.
    	String mercentOrderNumber = orderFeedDAO.insertMercentOrder(orderReportId, order);
    	logger.debug("mercentOrderNumber : "+mercentOrderNumber);
    	return mercentOrderNumber;
	}
        
	
	private void triggerMercentOrdersProcessing(String mercentOrderNumber) 
	{
		logger.debug("Sending Mercent Order ID :"+mercentOrderNumber+" to JMS queue.");
		
		boolean success = MercentUtils.sendJMSMessage(MercentConstants.JMS_PIPELINE_FOR_EM_MERCENT, 
												MercentConstants.PROCESS_INBOUND_ORDER, 
												mercentOrderNumber);

	      if (!success)
	      {
	        throw new MercentException("Unable to send JMS message for mercent order number = " 
	        							+ mercentOrderNumber);
	      }else{
	    	  logger.debug("Sent successfully");
	      }
	}

}