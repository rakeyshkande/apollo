/**
 * 
 */
package com.ftd.mercent.bo;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.Marshaller;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;

import com.ftd.mercent.adapters.FTDOrderAdapter;
import com.ftd.mercent.dao.MercentOrderFeedDAO;
import com.ftd.mercent.utilities.MercentConstants;
import com.ftd.mercent.utilities.MercentUtils;
import com.ftd.mercent.vo.MercentChannelMappingVO;
import com.ftd.mercent.vo.MercentOrderDetailVO;
import com.ftd.mercent.vo.ftd.FTDOrder;
import com.ftd.mercent.vo.ftd.Item;
import com.ftd.mercent.vo.mercent.Order;
import com.ftd.mercent.vo.mercent.OrderItem;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * @author skatam
 *
 */
public class MercentOrderBO 
{
	private Logger logger = new Logger("com.ftd.mercent.bo.MercentOrderBO");
	
	private Connection conn;
	private MercentOrderFeedDAO orderFeedDAO;
	
	public MercentOrderBO(Connection conn) {
		this.conn = conn;
		this.orderFeedDAO = new MercentOrderFeedDAO(conn);
	}

	public void process(String mercentOrderNumber) 
	{
		Order order = orderFeedDAO.getMercentOrder(mercentOrderNumber);
		if(order == null){
			logger.error("No Mercent Order found with mercentOrderNumber: "+mercentOrderNumber);
			return;
		}
		String channelName = order.getChannelName();
		MercentChannelMappingVO channelMappingVO = orderFeedDAO.getMercentChannelMapping(channelName);
		boolean validMercentChannelName = (channelMappingVO!=null && 
											channelMappingVO.getFtdOriginMapping()!=null && 
											channelMappingVO.getFtdOriginMapping().trim().length() > 0);
		
		logger.debug("validMercentChannelName :"+validMercentChannelName);
		if(!validMercentChannelName)
		{
			String msg = "Received Mercent Order with Invalid ChannelName :"+channelName+". Not processing this Order.";
			logger.error(msg);
			MercentUtils.sendNoPageSystemMessage(msg);
			return;
		}
		String origin = channelMappingVO.getFtdOriginMapping();
		List<String> duplicateOrderItems = this.checkForDuplicateOrderItems(order, origin);
		if(duplicateOrderItems!= null && !duplicateOrderItems.isEmpty())
		{
			String msg = "Received duplicate Mercent Order Item(s) :"+duplicateOrderItems+". Not processing this Order.";
			logger.error(msg);
			MercentUtils.sendNoPageSystemMessage(msg);
		}
		else
		{
			FTDOrder ftdOrder = this.buildFTDOrder(mercentOrderNumber, order);
			if(ftdOrder != null)
			{
				orderFeedDAO.saveFTDOrderXML(mercentOrderNumber, ftdOrder);
				boolean success = false;
				try {
					success = this.saveMercentOrderDetails(mercentOrderNumber, ftdOrder);
				} catch (Exception e) {
					String msg = "Unable to save Mercent Order Item details. Error :"+e.getMessage();
					logger.error(msg);
					MercentUtils.sendNoPageSystemMessage(msg);
				}
				if(success)
				{
					this.sendToOG(mercentOrderNumber, ftdOrder);
				}
			}
			
		}	
		
	}
	
	private void sendToOG(String mercentOrderNumber, FTDOrder ftdOrder)
	{
		boolean sentToOG = this.sendFTDOrderToOrderGatherer(ftdOrder, mercentOrderNumber);
		if(sentToOG)
		{
			List<Item> items = ftdOrder.getItems();
			for (Item item : items) 
			{
				String cancelReason = null;
				String confirmationNumber = item.getOrderNumber();
				String orderStatus = "Success";
				orderFeedDAO.insertMercentOrderAcknowledgement(confirmationNumber , orderStatus , cancelReason );
			}
		}
	}
	private boolean saveMercentOrderDetails(String mercentOrderNumber,FTDOrder ftdOrder) 
	{
		List<Item> items = ftdOrder.getItems();
		int itemCount = items.size();
		for (int i = 0; i < itemCount; i++) 
		{
			Item item = items.get(i);
			 
			MercentOrderDetailVO orderDetailVO = new MercentOrderDetailVO();
			orderDetailVO.setMercentItemNumber(ftdOrder.getHeader().getOrigin()+"_"+item.getMercentOrderItemCode());
			orderDetailVO.setMercentChannelOrderItemId(item.getMercentOrderItemCode());
			orderDetailVO.setMercentOrderNumber(mercentOrderNumber);
			orderDetailVO.setConfirmationNumber(item.getOrderNumber());
			orderDetailVO.setMercentProductId(item.getMercentProductid());
			
			String principal = item.getMercentPrincipalAmount();
			 String shipping = item.getServiceFee();
			 String tax = item.getTaxAmount();
			 String shippingTax = item.getShippingTaxAmount();
           
            logger.info(principal + " " + shipping + " " + tax + " " + shippingTax);
            
			if (principal == null || principal.equals("")) {
				orderDetailVO.setPrincipalAmount(new BigDecimal("0"));
              } else {
            	  orderDetailVO.setPrincipalAmount(new BigDecimal(principal));
              }
              if (shipping == null || shipping.equals("")) {
            	  orderDetailVO.setShippingAmount(new BigDecimal("0"));
              } else {
            	  orderDetailVO.setShippingAmount(new BigDecimal(shipping));
              }
              if (tax == null || tax.equals("")) {
            	  orderDetailVO.setTaxAmount(new BigDecimal("0"));
              } else {
            	  orderDetailVO.setTaxAmount(new BigDecimal(tax));
              }
              if (shippingTax == null || shippingTax.equals("")) {
            	  orderDetailVO.setShippingTaxAmount(new BigDecimal("0"));
              } else {
            	  orderDetailVO.setShippingTaxAmount(new BigDecimal(shippingTax));
              }
			
			orderFeedDAO.insertMercentOrderDetails(mercentOrderNumber, orderDetailVO);
		}
		return true;
	}
	
	private List<String> checkForDuplicateOrderItems(Order order, String origin)
	{
		List<String> duplicateOrderItemIds = new ArrayList<String>();
		List<OrderItem> items = order.getOrderItem();
		//List<Item> items = ftdOrder.getItems();
		int itemCount = items.size();
		//First check for duplicate OrderItemID in the same Order
		Set<String> orderItemIds = new HashSet<String>();
		for (int i = 0; i < itemCount; i++) 
		{
			OrderItem item = items.get(i);
			boolean added = orderItemIds.add(origin+"_"+item.getChannelOrderItemID());
			if(!added){
				duplicateOrderItemIds.add(origin+"_"+item.getChannelOrderItemID());
			}
		}
		if(!duplicateOrderItemIds.isEmpty()){
			return duplicateOrderItemIds;
		}
		for (int i = 0; i < itemCount; i++) 
		{
			OrderItem item = items.get(i);
			if(orderFeedDAO.isDuplicateOrderItem(origin+"_"+item.getChannelOrderItemID())){
				duplicateOrderItemIds.add(origin+"_"+item.getChannelOrderItemID());
			}
		}
		return duplicateOrderItemIds;
	}
	
	private FTDOrder buildFTDOrder(String mercentOrderNumber, Order mercentOrder) 
	{
		logger.debug("Building FTDOrder for Mercent Order:"+mercentOrderNumber);
		FTDOrderAdapter ftdOrderAdapter = new FTDOrderAdapter(conn);
		FTDOrder ftdOrder = null;
		try {
			ftdOrder = ftdOrderAdapter.getAsFTDOrder(mercentOrder);
		} catch (Exception e) {
			logger.error(e);
			String msg = "Error in converting Mercent Order XML to FTD Order XML. MercentOrderNumber: "+mercentOrderNumber;
			logger.error(msg);
			MercentUtils.sendNoPageSystemMessage(msg);
		}	
		return ftdOrder;
	}
	
	private boolean sendFTDOrderToOrderGatherer(FTDOrder ftdOrder, String mercentOrderNumber) 
	{
		logger.debug("Sending FTD Order :"+ftdOrder+" to FTD OrderGatherer.");
		
		boolean success = false;

        try {

            ConfigurationUtil cu = ConfigurationUtil.getInstance();
            String orderGathererURL = cu.getFrpGlobalParm(MercentConstants.MERCENT_GLOBAL_CONTEXT,
            		MercentConstants.MERCENT_ORDER_GATHERER_URL);
            
            logger.debug("orderGathererURL: " + orderGathererURL);

            Map<String, Object> props = new HashMap<String, Object>();
    		props.put(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
    		
            String strXml = MercentUtils.marshallAsXML(ftdOrder, props);
            logger.debug("Transformed FTD OrderXML being send to OG:\n"+strXml);
            String response=null;
            int result=-1;
            
            PostMethod post = new PostMethod(orderGathererURL);
            NameValuePair nvPair = null;
            NameValuePair[] nvPairArray = new NameValuePair[1];
            String name, value;
            
            name = "Order";
            value = strXml;
            nvPair = new NameValuePair(name, value);
            nvPairArray[0] = nvPair;

            post.setRequestBody(nvPairArray);
            
            HttpClient httpclient = new HttpClient();
            
            // Execute request
            try {
                result = httpclient.executeMethod(post);
                response = post.getResponseBodyAsString();
            } 
            catch (Exception e) {
				e.printStackTrace();
			}
            finally {
                // Release current connection to the connection pool once you are done
                post.releaseConnection();
            }
            String message = "Http Response Code is " + result;
            logger.debug(message);
            logger.error(response);
            /*if (result == 200) {
                success = true;
                System.out.println(success);
            }*/
            if( result==200 )
            {
            	success = true;
                updateOrderStatus(conn,mercentOrderNumber,MercentConstants.ORDER_STATUS_DONE);
            }
            else
            {   
                //if( result==600 ) 
                {
                	success = false;
                	//log the error and send email to IT and marketing
                    logger.error(response);
                    updateOrderStatus(conn,mercentOrderNumber,MercentConstants.ORDER_STATUS_GATHERER_ERROR);
                   
                    //send no-page email
                    MercentUtils.sendNoPageSystemMessage("Mercent-Order-Number: "+mercentOrderNumber+"\n ERROR-Details: "+response);
                }
                
             }
        } catch (Exception e) {
            logger.error("",e);
            success = false;
        }
        return success;
	}

	private void updateOrderStatus(Connection conn, 
									String mercentOrderNumber, 
									String orderStatus) throws Exception 
	{
		orderFeedDAO.updateOrderStatus(mercentOrderNumber, orderStatus);
	}

	public void processOGErrorOrders() 
	{
		logger.debug("Processing OG Error Orders.");
		Map<String, FTDOrder> ordersMap = 
			orderFeedDAO.getFTDOrderXmlsByStatus(MercentConstants.ORDER_STATUS_GATHERER_ERROR);
		Set<String> keys = ordersMap.keySet();
		for (String mercentOrderNumber : keys) 
		{
			logger.debug("Resending FTD Order for MercentOrderNumber :"+mercentOrderNumber);
			FTDOrder ftdOrder = ordersMap.get(mercentOrderNumber);
			this.sendToOG(mercentOrderNumber, ftdOrder);
		}		
	}
	
}
