/**
 * 
 */
package com.ftd.mercent.bo;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.ftd.mercent.dao.MercentFeedDAO;
import com.ftd.mercent.dao.MercentOrderAdjFeedDAO;
import com.ftd.mercent.utilities.MercentConstants;
import com.ftd.mercent.utilities.MercentUtils;
import com.ftd.mercent.vo.ItemAdjFeedVO;
import com.ftd.mercent.vo.OrderAdjFeedVO;
import com.ftd.mercent.vo.mercent.AdjustmentReasonType;
import com.ftd.mercent.vo.mercent.MercentFeed;
import com.ftd.mercent.vo.mercent.OrderAdjustment;
import com.ftd.mercent.vo.mercent.OrderAdjustment.OrderItemAdjustment;
import com.ftd.mercent.vo.mercent.OrderAdjustment.OrderItemAdjustment.ItemPriceAdjustments;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * @author smeka
 *
 */
public class MercentOrderAdjustmentFeedBO extends MercentFeedBO {

	private static Logger logger = new Logger("com.ftd.mercent.bo.MercentOrderAdjustmentFeedBO");

	/**
	 * creates the adjustment feeds and saves to MRCNT_FEED_MASTER
	 */
	@SuppressWarnings({ "unchecked"})
	public void createMercentFeed() {
		if(logger.isDebugEnabled()) {
			logger.debug("********* Entering createMercentOrderAdjFeed() *********");
		}
		Connection conn = null;
		MercentFeedDAO feedDAO = null;
		StringBuffer systemMessage = new StringBuffer();

		try {			
			conn = MercentUtils.getConnection();
			feedDAO = new MercentOrderAdjFeedDAO(conn);

			List<OrderAdjFeedVO> orderAdjustments = 
				(List<OrderAdjFeedVO>) feedDAO.getMercentFeedData(MercentConstants.NEW_FEED_STATUS);

			if(orderAdjustments == null || orderAdjustments.isEmpty()){
				logger.info("No new mercent Order Adjustments Feed data found");
				return;
			}		

			String feedId = createNewMercentFeed(this.buildOrderAdjMercentFeed(orderAdjustments), MercentConstants.MRCNT_ORDER_ADJ_FEED,conn);	

			if(feedId!= null && !StringUtils.isEmpty(feedId)) {
				logger.info("Succesfully created feed with Id: " + feedId);
				// get the list of primary feed ids/ order adjustments Ids to be updated.
				List<String> mercentFeedDataIds = new ArrayList<String>();
				for (OrderAdjFeedVO orderAdjFeedVO : orderAdjustments) {					
					for (ItemAdjFeedVO itemAdjustVO : orderAdjFeedVO.getItemAdjFeeds()) {
						mercentFeedDataIds.add(itemAdjustVO.getOrderAdjId());
					}					
				}
				if(!feedDAO.updateMercentFeedId(mercentFeedDataIds, feedId, MercentConstants.MRCNT_ORDER_ADJ_FEED,MercentConstants.SENT_FEED_STATUS)) {
					systemMessage.append("Unable to update the feed Id for the mercent adjustment orders. rolling back transaction.");
				}
				return;
			}
			systemMessage.append("Unable to insert the mercent feed for the mercent adjustment orders. rolling back transaction.");

		} catch (Exception e) {
			logger.error(" Error caught creating mercent order adjustments feeds. sending system message. " + e);
			e.printStackTrace();
			systemMessage.append("Error caught creating mercent adjustment feeds." + e.getMessage());
		} finally {

			if(!systemMessage.toString().isEmpty()) {
				// add code to send system messages
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
		if(logger.isDebugEnabled()) {
			logger.debug("********* Exiting createMercentOrderAdjFeed() *********");
		}		
	}

	/** Constructs the order adjustment feed.
	 * Case 1: 
	 * 	If order is fully refunded and no multiple refunds on at-least one order item - include <refundFull> 
	 * 	Can be checked for cancellation/removed orders/full shopping cart refund
	 * Case 2: (Multiple and single item cart)
	 * 	If order is fully refunded but has had a partial refund - don't include <refundFull>
	 * Case 3: (Only Multiple item cart)
	 *  If order is fully refunded but at different items at different times. 
	 *  Suppose item1, item2 fully refunded and feed already sent. 3rd item's full refund will make the complete order as refunded order.
	 *  Don't include <refundFull> when sending feed for 3rd item.
	 * 
	 * @param orderAdjFeedData
	 * @return
	 * @throws Exception
	 */
	private MercentFeed buildOrderAdjMercentFeed(List<OrderAdjFeedVO> orderAdjFeedData) throws Exception {
		
		Map<String, List<OrderAdjFeedVO>> orderAdjMap = new HashMap<String, List<OrderAdjFeedVO>>();
		for (OrderAdjFeedVO feedDetail : orderAdjFeedData) {
			if (!orderAdjMap.containsKey(feedDetail.getMerchantOrderID())) {
				orderAdjMap.put(feedDetail.getMerchantOrderID(),
						new ArrayList<OrderAdjFeedVO>());
			}
			orderAdjMap.get(feedDetail.getMerchantOrderID()).add(feedDetail);
		}
		
		MercentFeed mercentFeed = new MercentFeed();
		Set<String> merchantOrderIds = orderAdjMap.keySet();
		
		for (String merchantOrderId : merchantOrderIds) {
			List<OrderAdjFeedVO> feedDetailList = orderAdjMap.get(merchantOrderId);

			
			// common details, so get from any one feed detail.
			OrderAdjFeedVO orderAdjFeedVO = feedDetailList.get(0);
			OrderAdjustment orderAdjustment = new OrderAdjustment();
			orderAdjustment.setChannelOrderID(orderAdjFeedVO.getChannelOrderID());
			orderAdjustment.setChannelName(orderAdjFeedVO.getChannelName());
			List<ItemAdjFeedVO> itemAdjFeeds = orderAdjFeedVO.getItemAdjFeeds();
						
			//Case 1: If order is fully refunded
			if(orderAdjFeedVO.isOrderFullyRefudned()) {
				
				AdjustmentReasonType reasonType = AdjustmentReasonType.fromValue(MercentConstants.DEFAULT_ADJ_REASON);
				if(itemAdjFeeds != null && itemAdjFeeds.get(0) != null && itemAdjFeeds.get(0).getAdjustmentReason() != null) {
					reasonType = AdjustmentReasonType.fromValue(itemAdjFeeds.get(0).getAdjustmentReason());
				}
				
				// Check if no item in the order is adjusted more than once
				// NOTE: Say, if one item in the order is previously adjusted and an adjustment is already sent,
				// now if the second order item refund makes order as fully refund, the ordersFullyRefunded List will not contain the order
				// and thus we will not include <refundFull>
				// Check if all the items in cart are adjusted with same adjustment reason
				if(!isAdjustedMoreThanOnce(feedDetailList) && !orderAdjFeedVO.isAdjAlreadySent() && adjustedWithSameReason(feedDetailList, reasonType)) {
					if (logger.isDebugEnabled()) {
						logger.debug("Order fully refunded and all items adjusted only once: " + merchantOrderId);
					}
					
					orderAdjustment.setAdjustmentReason(reasonType);
					orderAdjustment.setRefundFull(true);					
					mercentFeed.getOrderAdjustment().add(orderAdjustment);				
					continue;
				}				
			}
			
			// Case 2: Order is not fully refunded

			for (OrderAdjFeedVO adjFeedVO : feedDetailList) {
				
				boolean quantityAdded = false;
				
				for (ItemAdjFeedVO itemAdjVO : adjFeedVO.getItemAdjFeeds()) {				
					
					OrderItemAdjustment itemAdjustment = new OrderItemAdjustment();
					itemAdjustment.setChannelOrderItemID(adjFeedVO.getChannelOrderItemId());
					itemAdjustment.setSKU(itemAdjVO.getItemSKU());
					
					AdjustmentReasonType reasonType = AdjustmentReasonType.fromValue(MercentConstants.DEFAULT_ADJ_REASON);
					if(itemAdjVO != null && itemAdjVO.getAdjustmentReason() != null) {
						reasonType = AdjustmentReasonType.fromValue(itemAdjVO.getAdjustmentReason());
					}
					itemAdjustment.setAdjustmentReason(reasonType);
					
					// Quantity will only be added for fully refunded items.					
					if(itemAdjVO.isItemRefunded()) {						
						// Case 2a) If order item is fully refunded, but has previously been adjusted, and only one adjustment to be sent.
						// Case 2b) If order item is fully refunded,  but has previously been adjusted, add quantity to only one adjustment.
						if(!quantityAdded) {
							itemAdjustment.setQuantity(BigInteger.valueOf(MercentConstants.NUMBER_OF_ITEMS));
							quantityAdded = true;;
						}						
					} 
					
					// irrespective of refund status(full/partial) add item adjustment, when refunds applied more than once.
					ItemPriceAdjustments itemPriceAdjs = new ItemPriceAdjustments();
					if (itemAdjVO.getItemPrincipalAmount() != null && itemAdjVO.getItemPrincipalAmount().doubleValue() >= 0) {
						itemPriceAdjs.setItemPrincipalAmount(itemAdjVO.getItemPrincipalAmount());
					}
					if (itemAdjVO.getItemTaxAmount() != null) {
						itemPriceAdjs.setItemTaxAmount(itemAdjVO.getItemTaxAmount());
					}
					if (itemAdjVO.getShippingPrincipalAmount() != null && itemAdjVO.getShippingPrincipalAmount().doubleValue() >= 0) {
						itemPriceAdjs.setShippingPrincipalAmount(itemAdjVO.getShippingPrincipalAmount());
					}
					if (itemAdjVO.getShippingTaxAmount() != null ) {
						itemPriceAdjs.setShippingTaxAmount(itemAdjVO.getShippingTaxAmount());
					}			
					
					itemAdjustment.setItemPriceAdjustments(itemPriceAdjs);
					orderAdjustment.getOrderItemAdjustment().add(itemAdjustment);
				}
			}
			mercentFeed.getOrderAdjustment().add(orderAdjustment);			
		}
		return mercentFeed;
	}

	/** Check if all the items are refunded with same reason.
	 * @param feedDetailList
	 * @param reasonType 
	 * @return
	 */
	private boolean adjustedWithSameReason(List<OrderAdjFeedVO> feedDetailList, AdjustmentReasonType reasonType) {
		
		for (OrderAdjFeedVO orderAdjFeedVO : feedDetailList) {
			List<ItemAdjFeedVO> itemAdjFeeds = orderAdjFeedVO.getItemAdjFeeds();
			// There will be only one Item Adjustment record.
			for (ItemAdjFeedVO itemAdjFeedVO : itemAdjFeeds) {
				if(itemAdjFeedVO.getAdjustmentReason() != null && !reasonType.value().equalsIgnoreCase(itemAdjFeedVO.getAdjustmentReason())) {
					return false;
				}
			}
		}
		return true;
	}

	/** Check if the no. of adjustments on each order item is not more than one
	 * @param feedDetailList
	 * @return
	 */
	private boolean isAdjustedMoreThanOnce(List<OrderAdjFeedVO> feedDetailList) {
		for (OrderAdjFeedVO orderAdjFeedVO : feedDetailList) {
			if(orderAdjFeedVO.getNoOfAdjs() > 1) {
				return true;
			}
		}
		return false;
	}


	public void processRefundFeed() throws Exception
	{
		Connection conn = null;	
		try
		{
			logger.debug("processRefundsFeed()");
			conn = MercentUtils.getConnection();
			MercentOrderAdjFeedDAO orderAdjFeedDAO  = new MercentOrderAdjFeedDAO(conn);
			orderAdjFeedDAO.processRefunds();
			logger.debug("End Refund BO");
		}
		catch (Exception e) {
			logger.error("Error occured while calling processRefundFeed method from MercentFeedDAO", e);
			throw e;

		}
		finally
		{
			if(conn!= null)
			{ 
				try 
				{ 
					conn.close(); 
				} 
				catch (SQLException e) 
				{
					logger.error("Error occured while closing the connection", e);
				} 
			}
		}
	}

}
