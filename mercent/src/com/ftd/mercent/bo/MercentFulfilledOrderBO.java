package com.ftd.mercent.bo;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.ftd.mercent.dao.MercentOrderFulfillmentDAO;
import com.ftd.mercent.exceptions.MercentException;
import com.ftd.mercent.utilities.MercentUtils;
import com.ftd.messagegenerator.StockMessageGenerator;
import com.ftd.messagegenerator.vo.PointOfContactVO;
import com.ftd.osp.utilities.email.BuildEmailConstants;
import com.ftd.osp.utilities.order.ScrubMapperDAO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * @author sbring
 *
 */

public class MercentFulfilledOrderBO {

	private Logger logger = new Logger("com.ftd.mercent.bo.MercentFulfilledOrderBO");

	public MercentFulfilledOrderBO() {

	}

	public void processFloristFulfilledOrders() throws Exception{
		logger.info("processFloristFulfilledOrders");
		Connection conn = null;
		try
		{
			conn = MercentUtils.getConnection();
			MercentOrderFulfillmentDAO mercentOrderFulfillmentDAO = new MercentOrderFulfillmentDAO(conn);
			mercentOrderFulfillmentDAO.insertFloristFulfilledOrders();
		}
		catch (Exception e) 
		{
			logger.error(e);
			throw new MercentException(e.getMessage());
		}
		finally
		{
			if(conn!= null){ try { conn.close(); } catch (SQLException e) {} }
		}
	}

	/**
	 * Process automated delivery email for the mercent orders that have current date as delivery date.
	 */
	public void processAutomatedDeliveryEmail() {
		logger.info("processAutomatedDeliveryEmail");
		Connection conn = null;
		try {
			conn = MercentUtils.getConnection();
			MercentOrderFulfillmentDAO mercentOrderFulfillmentDAO = new MercentOrderFulfillmentDAO(conn);
			
			// Get the mercent orders of the partner for whcih the send confirmation email flag is on and orders that are delivered today.
			Map<String, List<String>> ordersDelivered = mercentOrderFulfillmentDAO.getOrdersDeliveredToday();
									
            if(ordersDelivered != null) {                      
				for (String orderGUID : ordersDelivered.keySet()) {
					try {
						ScrubMapperDAO scrubMapperDao = new ScrubMapperDAO(conn);
						OrderVO orderVO = scrubMapperDao.mapOrderFromDB(orderGUID);
						if (orderVO != null) {
				            PointOfContactVO pocVO = new PointOfContactVO();
				            pocVO.setOrderGuid(orderGUID);
				            pocVO.setSourceCode(orderVO.getSourceCode());
				            pocVO.setCompanyId(orderVO.getCompanyId());
				            pocVO.setMasterOrderNumber(orderVO.getMasterOrderNumber());
				            pocVO.setCommentType("Email");
				            pocVO.setPointOfContactType("Email");
				            pocVO.setEmailType(BuildEmailConstants.CONFIRMATION_ACTION);
				            
			            	StockMessageGenerator msgGenerator = new StockMessageGenerator(conn);
			            	String recordAttributesXML = msgGenerator.generateRecordAttributeXMLFromOrderGuid(orderGUID);
			            	pocVO.setRecordAttributesXML(recordAttributesXML);
			            	
			            	msgGenerator.insertMessage(pocVO);
			            	msgGenerator.sendMessage(pocVO);

			                for (String orderItem : ordersDelivered.get(orderGUID)) {
			                	mercentOrderFulfillmentDAO.updateConfirmationStatus(orderItem, "Y");	  
							}	               	              
						} else {
							logger.error("Null OrderVO returned for order guid: " + orderGUID);
						}

					} catch(Exception e) {
						String message = "Automated email processing failed for orders: " + ordersDelivered.get(orderGUID) + ", " + e.getMessage();
						logger.error(message + ", " + e.getMessage());
						MercentUtils.sendNoPageSystemMessage(message);
						message = null;
					}               
				}	
            }
		} catch (Exception e) {
			logger.error(e);
			throw new MercentException(e.getMessage());
		} finally {
			if(conn!= null){ try { conn.close(); } catch (SQLException e) {} }
		}		
	}
}
