/**
 * 
 */
package com.ftd.mercent.bo;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.ftd.mercent.dao.MercentFeedDAO;
import com.ftd.mercent.dao.MercentOrderAckFeedDAO;
import com.ftd.mercent.utilities.MercentConstants;
import com.ftd.mercent.utilities.MercentUtils;
import com.ftd.mercent.vo.OrderAckFeedVO;
import com.ftd.mercent.vo.mercent.CancelReasonType;
import com.ftd.mercent.vo.mercent.MercentFeed;
import com.ftd.mercent.vo.mercent.OrderAcknowledgement;
import com.ftd.mercent.vo.mercent.OrderAcknowledgement.Item;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * @author smeka
 *
 */
public class MercentOrderAcknowledgementFeedBO extends MercentFeedBO {

	private static Logger logger = new Logger("com.ftd.mercent.bo.MercentOrderAcknowledgementFeedBO");

	/**
	 *  creates the acknowledgment feeds and saves to MRCNT_FEED_MASTER
	 *  Updates the feed Id for all the orders for which the feed is prepared.
	 */
	@SuppressWarnings("unchecked")
	public void createMercentFeed() {
		if(logger.isDebugEnabled()) {
			logger.debug("********* Entering createMercentOrderAckFeed() *********");
		}

		Connection conn = null;
		MercentFeedDAO feedDAO = null;	
		StringBuffer systemMessage = new StringBuffer();

		try {			
			conn = MercentUtils.getConnection();
			feedDAO = new MercentOrderAckFeedDAO(conn);

			List<OrderAckFeedVO> orderAcknowledgements = 
				(List<OrderAckFeedVO>) feedDAO.getMercentFeedData(MercentConstants.NEW_FEED_STATUS);

			if(orderAcknowledgements == null || orderAcknowledgements.isEmpty()){
				logger.info("No new mercent Order Acknowledgement Feed data found");
				return;
			}		

			String feedId = createNewMercentFeed(this.buildOrderAckMercentFeed(orderAcknowledgements), MercentConstants.MRCNT_ORDER_ACK_FEED,conn);	

			if(feedId!= null && !StringUtils.isEmpty(feedId)) {
				logger.info("Succesfully created feed with Id: " + feedId);
				List<String> mercentFeedDataIds = new ArrayList<String>(orderAcknowledgements.size());
				for (OrderAckFeedVO orderAckFeedVO : orderAcknowledgements) {
					mercentFeedDataIds.add(orderAckFeedVO.getOrderAckId());
				}

				if(!feedDAO.updateMercentFeedId(mercentFeedDataIds, feedId, MercentConstants.MRCNT_ORDER_ACK_FEED, MercentConstants.SENT_FEED_STATUS)) {
					systemMessage.append("Unable to update the feed Id for the mercent acknowlegment orders. rolling back transaction.");
				}
				return;
			}

			systemMessage.append("Unable to insert the mercent feed for the mercent acknowlegment orders. rolling back transaction.");
		} catch (Exception e) {
			logger.error("Error caught creating mercent acknowledgment feeds. sending system message. " + e);
			e.printStackTrace();
			systemMessage.append("Error caught creating mercent acknowledgment feeds." + e.getMessage());
		}finally {

			if(!systemMessage.toString().isEmpty()) {
				// add code to send system messages
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		if(logger.isDebugEnabled()) {
			logger.debug("********* Exiting createMercentOrderAckFeed() *********");
		}
	}


	/** Creates Order acknowledgment feed
	 * @param orderAcknowledgements
	 * @return
	 */
	private MercentFeed buildOrderAckMercentFeed(List<OrderAckFeedVO> orderAcknowledgements) {
		MercentFeed mercentFeed = new MercentFeed();
		Map<String, List<OrderAckFeedVO>> orderAckMap = new HashMap<String, List<OrderAckFeedVO>>();
		for (OrderAckFeedVO feedDetail : orderAcknowledgements) {
			if(!orderAckMap.containsKey(feedDetail.getMerchantOrderID())){
				orderAckMap.put(feedDetail.getMerchantOrderID(), new ArrayList<OrderAckFeedVO>());
			}
			orderAckMap.get(feedDetail.getMerchantOrderID()).add(feedDetail);
		}

		Set<String> keySet = orderAckMap.keySet();
		for (String key : keySet) {

			List<OrderAckFeedVO> feedDetailList = orderAckMap.get(key);
			OrderAckFeedVO orderAckFeedVO = feedDetailList.get(0); 	

			OrderAcknowledgement orderAcknowledgement = new OrderAcknowledgement();
			orderAcknowledgement.setChannelOrderID(orderAckFeedVO.getChannelOrderID());
			orderAcknowledgement.setMerchantOrderID(orderAckFeedVO.getMerchantOrderID());
			orderAcknowledgement.setChannelName(orderAckFeedVO.getChannelName());
			orderAcknowledgement.setStatusCode(orderAckFeedVO.getStatusCode());
			if (orderAckFeedVO.getCancelReason()!=null){
				orderAcknowledgement.setCancelReason(CancelReasonType.fromValue(orderAckFeedVO.getCancelReason()));
			}

			for (OrderAckFeedVO ackFedd : feedDetailList){
				Item item = new Item();
				item.setChannelOrderItemID(ackFedd.getChannelOrderItemId());
				item.setMerchantOrderItemID(ackFedd.getMerchantOrderItemId());
				orderAcknowledgement.getItem().add(item);
			}
			mercentFeed.getOrderAcknowledgement().add(orderAcknowledgement);
		}		
		return mercentFeed;
	}

}
