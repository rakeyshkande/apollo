package com.ftd.mercent.utilities;

public class MercentConstants
{ 
    private MercentConstants() {}

    public final static String PULL_ORDERS_FROM_MERCENT = "PULL_ORDERS_FROM_MERCENT";
    public final static String PROCESS_INBOUND_ORDER = "PROCESS_INBOUND_ORDER";
    public static final String PROCESS_PRODUCT_FEED = "PRODUCT_FEED";  
    public static final String UPLOAD_PRODUCT_FEED = "UPLOAD_PRODUCT_FEED";
    public static final String INSERT_FLORIST_FULFILLED_ORDERS = "FLORIST_FULFILLED_ORDERS";
    public static final String PROCESS_REFUND_FEED = "PROCESS_REFUNDS";
    public final static String PROCESS_MRCNT_ORD_ACK_FEED = "ORDER-ACK-FEED";	
	public final static String PROCESS_MRCNT_ORD_ADJ_FEED = "ORDER-ADJ-FEED";	
	public final static String PROCESS_MRCNT_ORD_FULFILL_FEED = "ORDER-FULFILL-FEED";
    public final static String UPLOAD_ACK_FEED = "UPLOAD-ACK-FEED";	
	public final static String UPLOAD_ADJ_FEED = "UPLOAD-ADJ-FEED";	
	public final static String UPLOAD_FULFILL_FEED = "UPLOAD-FULLFILL-FEED";
	
	
    public final static String PROPERTY_FILE = "mercent_config.xml";
    public final static String DATASOURCE_NAME = "DATASOURCE";
    
    // System messaging
    public static final String SM_PAGE_SOURCE = "MERCENT_PAGE";
    public static final String SM_NOPAGE_SOURCE = "MERCENT_NOPAGE";
    public static final String SM_PAGE_SUBJECT = "Mercent Message";
    public static final String SM_NOPAGE_SUBJECT = "NOPAGE Mercent Message";
    public static final String SM_TYPE = "System Exception";
    
    
    // Global parameters
    public static final String MERCENT_GLOBAL_CONTEXT       = "MERCENT_CONFIG";
    public static final String MERCENT_DEFAULT_SOURCE_CODE  = "default_source_code";
    public static final String MERCENT_CATEGORY 			= "category";
    public static final String MERCENT_QUANTITY				= "quantity";
    public static final String MERCENT_FULFILLMENT_LATENCY	= "fulfillment_latency";
    public static final String MERCENT_PRODUCT_URL			= "product_url";
    public static final String MERCENT_IMAGE_SIZE			= "image_size";
    public static final String MERCENT_IMAGE_URL			= "image_url";
    public static final String MERCENT_IMAGE_FORMAT			= "image_format";
    public static final String MERCENT_SHIPPING_STANDARD	= "shipping_standard";
    public static final String MERCENT_SHIPPING_2_DAY		= "shipping_2_day";
    public static final String MERCENT_SHIPPING_OVERNIGHT	= "shipping_overnight";
    public static final String MERCENT_SHIPPING_ADDITIONAL_COST = "shipping_service_additional_cost";
    public static final String MERCENT_PRODUCTS_IN_FEED		= "max_products_in_feed";
    
    // Product Feed
	public static final String BRAND 								= "FTD";
	public static final String MANUFACTURER 						= "FTD";
	public static final int NUMBER_OF_ITEMS							= 1;
	public static final int NUMBER_OF_PRODUCT_VARIANTS 				= 3;
    public static final String ALWAYS_TAXABLE 						= "AlwaysTaxable";
    public static final String ALWAYS_NON_TAXABLE					= "AlwaysNonTaxable";
    public static final String AVAILABILITY_CODE_INSTOCK			= "InStock";
    public static final String AVAILABILITY_CODE_OUTOFSTOCK 		= "OutOfStock";
    public static final String CUSTOM_FIELD_EXCEPTION_START_DATE 	= "ExceptionStartDate";
    public static final String CUSTOM_FIELD_EXCEPTION_END_DATE 		= "ExceptionEndDate";
    public static final String FEED_STATUS_NEW 						= "NEW";
    public static final String FEED_STATUS_SENT 					= "SENT";
    public static final String PRODUCT_FEED							= "PRODUCT_FEED";
    public static final String MRCNT_PRODUCT_FEED_PREFIX 			= "MercentProductFeed_";
    
    public final static int ERROR_CODE = 600;
    public final static int SUCCESS_CODE = 200;
    public final static String EMPTY_XML_ERROR = "Received empty xml transmission.";

    //JMS
    public final static String JMS_PIPELINE_FOR_EM_MERCENT = "SUCCESS";
    
    
    public static final String 	CATALOG_SUFFIX_STANDARD    = "";
    public static final String 	CATALOG_SUFFIX_DELUXE    = "_deluxe";
    public static final String 	CATALOG_SUFFIX_PREMIUM    = "_premium";
    
    
    public static final String MERCENT_ORDER_GATHERER_URL        = "ORDER_GATHERER_URL";
    
    public final static String DEFAULT_COMPANY_ID = "FTD";
    public final static String ORDER_STATUS_RECEIVED = "RECEIVED";
    public static final String ORDER_STATUS_TRANSFORMED = "TRANSFORMED";
    public final static String ORDER_STATUS_DONE = "DONE";
    public final static String ORDER_STATUS_GATHERER_ERROR = "GATHERER ERROR";

    public static final boolean NEEDS_PROXY_CONFIG = true;
    public static final String 	PROXY_HOST    = "squid.hyd.int.untd.com";
    public static final int 	PROXY_PORT    = 3128;
    
    public static final String MERCENT_CURRENCY_CODE_USD = "USD";
    

	//Feed correlation actions
    public final static String MRCNT_ORDER_ACK_FEED = "ACKNOWLEDGMENT_FEED";	
	public final static String MRCNT_ORDER_ADJ_FEED = "ADJUSTMENT_FEED";	
	public final static String MRCNT_ORDER_FULFILL_FEED = "FULLFILLMENT_FEED";	
	
	// Possible feed status.
	public static final String NEW_FEED_STATUS = "NEW";	
	public static final String SENT_FEED_STATUS = "SENT";	
	
	// mercent id + feed becomes prefix of the file name of the feed xml/txt.
	public static final String MRCNT_ID_KEY = "mercent_id";
	public static final String MRCNT_ORD_ACK_PREFIX = "OrderAcknowledgement_";	
	public static final String MRCNT_ORD_FUL_PREFIX = "OrderFulfillment_";
	public static final String MRCNT_ORD_ADJ_PREFIX = "OrderAdjustment_";
	
	// constants to FTP the feed to remote server
	public static final String LOCAL_FTP_SERVER_USERNAME = "LOCAL_FTP_SERVER_USERNAME";	
	public static final String LOCAL_FTP_SERVER_PWD = "LOCAL_FTP_SERVER_PWD";	
	public static final String LOCAL_FTP_FEEDS_DIR = "LOCAL_FTP_FEEDS_DIR";	
	public static final String LOCAL_FTP_SERVER = "LOCAL_FTP_SERVER";
	public static final String LOCAL_FTP_ORDER_FEEDS_DIR = "LOCAL_FTP_ORDER_FEEDS_DIR";
	public static final String LOCAL_FTP_ORDER_FEEDS_ARCHIVE_DIR = "LOCAL_FTP_ORDER_FEEDS_ARCHIVE_DIR";
	public static final String LOCAL_ORDER_FEEDS_DIR = "LOCAL_ORDER_FEEDS_DIR";
	public static final String LOCAL_ORDER_FEEDS_ARCHIVE_DIR = "LOCAL_ORDER_FEEDS_ARCHIVE_DIR";
	
	public static final int RETRY_COUNT = 3;
	
	public static final String ORDER_REPORT_FILE_EXTN = ".xml";
	public static final String MRCNT_SECURE_CONTEXT   = "mercent";
	public static final String PROCESS_OG_ERROR_ORDERS = "PROCESS_OG_ERROR_ORDERS";
	
	public static final String DEFAULT_ADJ_REASON = "GeneralAdjustment";

}