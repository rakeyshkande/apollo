package com.ftd.mercent.utilities;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;
import java.util.Set;

import javax.naming.InitialContext;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang.StringUtils;

import com.ftd.mercent.exceptions.MercentException;
import com.ftd.mercent.vo.FTPServerVO;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.CacheUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.vo.MessageToken;

public class MercentUtils
{ 
  
    public static final String MERCENT_CONFIG_XML = "mercent_config.xml";
    
	/**
    * Logger instance
    */
    private static Logger logger = new Logger("com.ftd.mercent.common.MercentUtils");
    private MercentUtils() 
    {
	}
    
    public static boolean sendJMSMessage(String status, String corrId, String message)
    {

      boolean success = true;

      try
      {
        MessageToken messageToken = new MessageToken();
        messageToken.setStatus(status);
        messageToken.setJMSCorrelationID(corrId);
        messageToken.setMessage(message);
        Dispatcher dispatcher = Dispatcher.getInstance();
        dispatcher.dispatchTextMessage(new InitialContext(), messageToken);
      }
      catch (Exception e)
      {
        logger.error(e);
        success = false;
      }

      return success;

    }

    /**
     * Get a new database connection.
     * 
     * @return
     * @throws Exception
     */
    public static Connection getConnection()
	{
			Connection conn = null;
			
			try {
				ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
				String datasource = configUtil.
						getPropertyNew(MercentConstants.PROPERTY_FILE,MercentConstants.DATASOURCE_NAME);
				conn = DataSourceUtil.getInstance().getConnection(datasource);
			}  catch (Exception e) {
				throw new MercentException(e);
			}
		        
			return conn;
	}

    
    
    public static String getFrpGlobalParm(String context, String name) //throws CacheException, Exception
    {
      CacheUtil cacheUtil = CacheUtil.getInstance();
      try {
		return cacheUtil.getGlobalParm(context, name);
		} catch (Exception e) {
			logger.error(e);
			throw new RuntimeException(e);
		}
    }

    public static String getMercentFrpGlobalParm(String name) //throws CacheException, Exception
    {
		return getFrpGlobalParm(MercentConstants.MERCENT_GLOBAL_CONTEXT, name);
    }
    
    public static String getSecureGlobalParm(String context, String name) //throws CacheException, Exception
    {
      try {
		ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
		  return configUtil.getSecureProperty(context, name);
	   } catch (Exception e) {
		   logger.error(e);
			throw new RuntimeException(e);
	   }
    }

    public static String getMercentSecureGlobalParm(String name) //throws CacheException, Exception
    {
    	return getSecureGlobalParm(MercentConstants.MRCNT_SECURE_CONTEXT, name);
    }
    
    public static void sendPageSystemMessage(String logMessage)
    {
      Connection conn = null;
      try
      {
        conn = getConnection();
        String appSource = MercentConstants.SM_PAGE_SOURCE;
        String errorType = MercentConstants.SM_TYPE;
        String subject = MercentConstants.SM_PAGE_SUBJECT;
        int pageLevel = SystemMessengerVO.LEVEL_PRODUCTION;

        SystemMessengerVO systemMessengerVO = new SystemMessengerVO();
        systemMessengerVO.setLevel(pageLevel);
        systemMessengerVO.setSource(appSource);
        systemMessengerVO.setType(errorType);
        systemMessengerVO.setSubject(subject);
        systemMessengerVO.setMessage(logMessage);
        String result = SystemMessenger.getInstance().send(systemMessengerVO, conn, false);
        logger.debug(result);
      }
      catch (Exception ex)
      {
        // Do not attempt to send system message it requires obtaining a
        // connection
        // and may end up in an infinite loop.
        logger.error(ex);
      }
      finally
      {
        if (conn != null)
        {
          try
          {
            conn.close();
          }
          catch (Exception e)
          {
            logger.error("Unable to close connection: " + e);
          }
        }
      }

    }

    public static void sendNoPageSystemMessage(String logMessage)
    {
      Connection conn = null;
      try
      {
        conn = getConnection();
        String appSource = MercentConstants.SM_PAGE_SOURCE;
        String errorType = MercentConstants.SM_TYPE;
        String subject = MercentConstants.SM_NOPAGE_SUBJECT;
        int pageLevel = SystemMessengerVO.LEVEL_PRODUCTION;

        SystemMessengerVO systemMessengerVO = new SystemMessengerVO();
        systemMessengerVO.setLevel(pageLevel);
        systemMessengerVO.setSource(appSource);
        systemMessengerVO.setType(errorType);
        systemMessengerVO.setSubject(subject);
        systemMessengerVO.setMessage(logMessage);
        String result = SystemMessenger.getInstance().send(systemMessengerVO, conn, false);
        logger.debug(result);
      }
      catch (Exception ex)
      {
        // Do not attempt to send system message it requires obtaining a
        // connection
        // and may end up in an infinite loop.
        logger.error(ex);
      }
      finally
      {
        if (conn != null)
        {
          try
          {
            conn.close();
          }
          catch (Exception e)
          {
            logger.error("Unable to close connection: " + e);
          }
        }
      }

    }
    
    
	
	/**
     * Get the name of the server that the PI application is running on
     * @return String name of local host or blank if it can't be determined.
     */
    public static String getLocalHostName() 
    {
        String retVal = "";
        try {
            InetAddress addr = InetAddress.getLocalHost();
            retVal = addr.getHostName();
        } catch (UnknownHostException e) {
            retVal = "Unknown";
        }
        return retVal;
    }
    
    public static String getMercentLocalFeedDirectory() {
		return getMercentFrpGlobalParm("LOCAL_FEED_DIRECTORY");
	}
    
    public static String getMercentLocalFeedArchiveDirectory() {
			return getMercentFrpGlobalParm("ARCHIVE_FEED_DIRECTORY");
	}
    
	public static Calendar fromXMLGregorianCalendar(XMLGregorianCalendar xc) //throws DatatypeConfigurationException 
	{
	 Calendar c = Calendar.getInstance();
	 c.setTimeInMillis(xc.toGregorianCalendar().getTimeInMillis());
	 return c;
	}

	public static XMLGregorianCalendar toXMLGregorianCalendar(Date date) //throws DatatypeConfigurationException 
	{
	 GregorianCalendar gc = new GregorianCalendar();
	 gc.setTimeInMillis(date.getTime());
	 XMLGregorianCalendar xc;
	try {
		xc = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
	} catch (DatatypeConfigurationException e) {
		logger.error(e);
		throw new RuntimeException(e.getMessage());
	}
	 return xc;
	}

	public static String deriveFeedName(String feedNamePrefix) {
		String directory = getMercentLocalFeedDirectory();
		return directory + feedNamePrefix + System.currentTimeMillis()+".xml";
	}
	
	/**
	   * Converts an ISO date string to a java.util.Date object
	   * The date formatter in jdk 1.4 interprets the timezone 'Z' formatter as
	   * -hh:mm.  The ISO standared which Mercent uses sends the timezone over as
	   * -hhmm.
	   * @param azDateString string to parse
	   * @return converted date object
	   * @throws java.text.ParseException
	   */
	     public static java.sql.Date mrcDateString2Date(String azDateString) throws ParseException
	     {
	        String strDate = StringUtils.substringBeforeLast(azDateString,":") + StringUtils.substringAfterLast(azDateString,":");
	        
	        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
	        java.util.Date date = format.parse(strDate);
	        return new java.sql.Date(date.getTime());
	     }
	    
	  /**
	   * Converts a java.sql.Date to an ISO date string.
	   * The date formatter in jdk 1.4 interprets the timezone 'Z' formatter as
	   * -hhmm.  The ISO standared which Mercent uses sends the timezone over as
	   * -hh:mm.
	   * @param date date to parse
	   * @return converted string object
	   * @throws java.text.ParseException
	   */
	    public static String sqlDate2MrcDateString(java.sql.Date date) throws ParseException
	    {
	      String retval=null;
	      SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
	      if( format!=null && date!=null)
	      {
	      	java.util.Date testDate = new java.util.Date(date.getTime());
	      	if( testDate!=null )
	      	{
				retval = format.format(testDate);
	      	}
	      }
	      
	      return retval;
	    }
	    
	  /**
	   * Tests a string for boolean true.  Valid true strings are:
	   * "T", "TRUE", "Y", "YES", and "1".  Passed string of null returns false;
	   * Evaluation ignores case.
	   * @param strBool string to evaluate
	   * @return evaluation results
	   */
	    public static boolean isTrue( String strBool ) 
	    {
	      if( strBool==null )
	        return false;
	      
	      if( strBool.equalsIgnoreCase("T") || strBool.equalsIgnoreCase("TRUE") ||
	          strBool.equalsIgnoreCase("Y") || strBool.equalsIgnoreCase("YES") ||
	          strBool.equalsIgnoreCase("1") )
	        return true;
	        
	      return false;
	    }

	    public static String removeAllSpecialChars(String inStr)
	    {
	        StringBuffer sb = new StringBuffer();
	        for(int i = 0; i < inStr.length(); i++)
	        {
	            if(Character.isLetterOrDigit(inStr.charAt(i)))
	            {
	                sb.append(inStr.charAt(i));
	            }
	        }

	        return sb.toString();
	    }
	    
	    public static String marshallAsXML(Object object, Map<String, Object> props) 
	    {
	    	String xml = "";
	    	try 
	    	{
				JAXBContext jaxbContext = JAXBContext.newInstance(object.getClass());
				Marshaller marshaller = jaxbContext.createMarshaller();
				if(props!= null && props.size()>0)
				{
					Set<String> keySet = props.keySet();
					for (String key : keySet) 
					{
						marshaller.setProperty(key, props.get(key));
						
					}
				}
				//marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
				//marshaller.setProperty(Marshaller.JAXB_NO_NAMESPACE_SCHEMA_LOCATION,"Mercent_Standard_Order.xsd");
				
				StringWriter sw = new StringWriter();
				
				marshaller.marshal(object, sw);
				xml = sw.toString();
				
			} catch (JAXBException e) {
				throw new MercentException(e);
			}
			return xml;
		
		}

		public static Object marshallAsObject(String mercentXML, Class<?> clazz) 
		{
			Object object = null;
			try 
	    	{
				JAXBContext jaxbContext = JAXBContext.newInstance(clazz);
				Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
				
				StringReader sw = new StringReader(mercentXML);
				object = unmarshaller.unmarshal(sw);
				
			} catch (JAXBException e) {
				throw new MercentException(e);
			}
			return object;
		}
		
		public static Object marshallAsObject(File mercentXMLFile, Class<?> clazz) 
		{
			Object object = null;
			try 
	    	{
				JAXBContext jaxbContext = JAXBContext.newInstance(clazz);
				Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
				
				object = unmarshaller.unmarshal(mercentXMLFile);
				
			} catch (JAXBException e) {
				throw new MercentException(e);
			}
			return object;
		}
		
		public static Object marshallAsObject(byte[] data, Class<?> clazz) 
		{
			Object object = null;
			try 
	    	{
				JAXBContext jaxbContext = JAXBContext.newInstance(clazz);
				Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
				ByteArrayInputStream bais = new ByteArrayInputStream(data);
				object = unmarshaller.unmarshal(bais);
				bais.close();
			} catch (JAXBException e) {
				throw new MercentException(e);
			} catch (IOException e) {
				throw new MercentException(e);
			}
			return object;
		}
	public static String getFeedPrefix(String feedType) {
		if(MercentConstants.MRCNT_ORDER_ACK_FEED.equals(feedType)) {
			return MercentConstants.MRCNT_ORD_ACK_PREFIX;
		}
		
		if(MercentConstants.MRCNT_ORDER_ADJ_FEED.equals(feedType)) {
			return MercentConstants.MRCNT_ORD_ADJ_PREFIX;
		}
		
		if(MercentConstants.MRCNT_ORDER_FULFILL_FEED.equals(feedType)) {
			return MercentConstants.MRCNT_ORD_FUL_PREFIX;
		}
		if(MercentConstants.PRODUCT_FEED.equals(feedType)){
			return MercentConstants.MRCNT_PRODUCT_FEED_PREFIX;
		}
		return null;
	}

	public static String deriveRemoteFeedFileName(String feedPrefix, String feedType, int count) {
		if(feedPrefix == null) {
			feedPrefix = getFeedPrefix(feedType);
		}
		StringBuffer fileName = getFeedFile(feedPrefix,count);		
		return fileName.toString();
	}
	
	public static StringBuffer getFeedFile(String feedPrefix, int count) {
		StringBuffer fileName = new StringBuffer();	
		
		//append mercentId
		//fileName.append(getMercentId());	
		
		//append feed type
		fileName.append(feedPrefix);
		
		// append current time in yyyyMMddHHmmss format.
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		cal.add(Calendar.SECOND, count);		 
		fileName.append(sdf.format(cal.getTime()));	
		
		//append type of file
		fileName.append(".xml");		
		return fileName;
	}
	
    public static String getMercentId() {
		return getMercentFrpGlobalParm(MercentConstants.MRCNT_ID_KEY);
	}
    
    public static String getMercentRemoteFeedDirectory() {
		return getMercentFrpGlobalParm(MercentConstants.LOCAL_FTP_FEEDS_DIR);
	}
    
	public static FTPServerVO populateFTPServerDetails() {
		FTPServerVO serverVO = null;
		try {
			serverVO = new FTPServerVO();
			serverVO.setFtpServerLocation(getMercentFrpGlobalParm(MercentConstants.LOCAL_FTP_SERVER));
			serverVO.setFtpServerLogon(getMercentSecureGlobalParm(MercentConstants.LOCAL_FTP_SERVER_USERNAME));
			serverVO.setFtpServerPwd(getMercentSecureGlobalParm(MercentConstants.LOCAL_FTP_SERVER_PWD));
			serverVO.setFtpServerDirectory(getMercentRemoteFeedDirectory());
			return serverVO;
		} catch (Exception e) {
			logger.error("Errror caught populating FTP server details.");
			throw new MercentException(e.getMessage());
		}
		
	}
	    
		public static String getMercentConfigProperty(String propertyName)
		{
			try {
				return ConfigurationUtil.getInstance().getProperty(MERCENT_CONFIG_XML, propertyName);
			} catch (Exception e) {
				throw new RuntimeException(e.getMessage());
			}
		}

}