package com.ftd.rbs.server.bo;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import org.w3c.dom.Document;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.rbs.common.RecordableBouquetServiceConstants;
import com.ftd.rbs.exception.DuplicatePersonalGreetingIdException;
import com.ftd.rbs.exception.RecordableBouquetServiceException;
import com.ftd.rbs.server.dao.RecordableBouquetServiceDAO;


/**
 * Business Object for the Recordable Bouquet Service.  
 */
public class RecordableBouquetServiceBO
{
    protected Logger logger = new Logger(this.getClass().getName());

    public RecordableBouquetServiceBO()
    {
    }

    /**
     * Generate a Personal Greeting Id.
     * @param conn Database connection
     * @return Personal Greeting Id
     * @throws Exception
     */
    public String generatePersonalGreetingId(Connection conn)
        throws Exception
    {
        ConfigurationUtil cu = ConfigurationUtil.getInstance();
        RecordableBouquetServiceDAO rbsDAO = null;
        String personalGreetingSequence = null;
        int personalGreetingId = 0;
        int leadingDigit = 0;
        int personalGreetingIdLength = 0;

        // Obtain leading digit for the Personal Greeting Id.  The leading digit is specific to each environment.
        try
        {
            leadingDigit = 
                    Integer.parseInt(cu.getFrpGlobalParm(RecordableBouquetServiceConstants.RECORDABLE_BOUQUET_SERVICE_CONFIG_CONTEXT, 
                                                         RecordableBouquetServiceConstants.LEADING_PERSONAL_GREETING_ID_DIGIT));
        }
        catch (Exception e)
        {
            new RecordableBouquetServiceException("Please specify a valid leading digit for the Personal Greeting Id.", e);
        }

        // Create the base value of the Personal Greeting Id given the leading digit and length
        personalGreetingIdLength = RecordableBouquetServiceConstants.VOICE_EXPRESS_PERSONAL_GREETING_ID_LENGTH;
        personalGreetingId += leadingDigit;
        for (int i = 0; i < personalGreetingIdLength - 1; i++)
        {
            personalGreetingId = personalGreetingId * 10;
        }

        // Obtain the next Personal Greeting Id sequence number
        rbsDAO = new RecordableBouquetServiceDAO();
        personalGreetingSequence = rbsDAO.getNextPersonalGreetingSequence(conn);

        if (logger.isDebugEnabled())
        {
            logger.debug("Next sequence number is " + personalGreetingSequence);
        }

        // If Personal Greeting Id sequence is larger than the max length then truncate
        if (personalGreetingSequence.length() > personalGreetingIdLength - 1)
        {
            personalGreetingSequence = 
                    personalGreetingSequence.substring(personalGreetingSequence.length() - (personalGreetingIdLength - 1));
        }
        personalGreetingId += Integer.parseInt(personalGreetingSequence);

        if (logger.isDebugEnabled())
        {
            logger.debug("Generated personal greeting id " + personalGreetingId);
        }

        return String.valueOf(personalGreetingId);
    }

    /**
     * Transmit a Personal Greeting Id to Voice Express.
     * @param personalGreetingId Personal Greeting Id
     * @param deliveryDate Delivery date of the order
     * @param orderNumber Order number which is not required
     * @throws RecordableBouquetServiceException
     * @throws DuplicatePersonalGreetingIdException
     */
    public void sendVoiceExpressGetRequest(String personalGreetingId, Date deliveryDate, String orderNumber)
        throws DuplicatePersonalGreetingIdException, Exception
    {
        ConfigurationUtil cu = ConfigurationUtil.getInstance();

        // Format the delivery date
        SimpleDateFormat dateFormat = new SimpleDateFormat(RecordableBouquetServiceConstants.VOICE_EXPRESS_DELIVERY_DATE_FORMAT);
        String formattedDeliveryDate = dateFormat.format(deliveryDate);

        // Obtain the FTD Voice Express customer code
        String ftdCustomerCode = 
            cu.getFrpGlobalParm(RecordableBouquetServiceConstants.RECORDABLE_BOUQUET_SERVICE_CONFIG_CONTEXT, 
                                RecordableBouquetServiceConstants.VOICE_EXPRESS_CUSTOMER_CODE);
        
        if(ftdCustomerCode == null || ftdCustomerCode.length() == 0)
        {
            new RecordableBouquetServiceException("Please specify a valid FTD Voice Express customer code.");
        }

        // Obtain the Voice Express URL
        String endpoint = 
            cu.getFrpGlobalParm(RecordableBouquetServiceConstants.RECORDABLE_BOUQUET_SERVICE_CONFIG_CONTEXT, 
                                RecordableBouquetServiceConstants.VOICE_EXPRESS_SERVICE_URL);

        if(endpoint == null || endpoint.length() == 0)
        {
            new RecordableBouquetServiceException("Please specify a valid Voice Express URL.");
        }

        HashMap requestParameters = new HashMap();
        String response = null;

        // Build parameters
        requestParameters.put(RecordableBouquetServiceConstants.VOICE_EXPRESS_CUSTOMER_CODE_PARAMETER, ftdCustomerCode);
        requestParameters.put(RecordableBouquetServiceConstants.VOICE_EXPRESS_ORDER_NUMBER_PARAMETER, personalGreetingId);
        requestParameters.put(RecordableBouquetServiceConstants.VOICE_EXPRESS_DELIVERY_DATE_PARAMETER, formattedDeliveryDate);
        requestParameters.put(RecordableBouquetServiceConstants.VOICE_EXPRESS_REFERENCE_NUMBER_PARAMETER, RecordableBouquetServiceConstants.VOICE_EXPRESS_DUMMY_ORDER_NUMBER);

        // Send data
        int i = 0;
        String key = null;
        for (Iterator<String> it = requestParameters.keySet().iterator(); it.hasNext(); i++)
        {
            key = it.next();
            if (i == 0)
            {
                endpoint += "?";
            }
            else
            {
                endpoint += "&";
            }
            endpoint += key + "=" + requestParameters.get(key);
        }

        if (logger.isDebugEnabled())
        {
            logger.debug("Voice Express URL with parameters:" + endpoint);
        }

        URL url = new URL(endpoint);
        URLConnection conn = url.openConnection();

        // Obtain the response
        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        StringBuffer sb = new StringBuffer();
        String line;
        while ((line = rd.readLine()) != null)
        {
            sb.append(line);
        }
        rd.close();
        response = sb.toString();

        if (logger.isDebugEnabled())
        {
            logger.debug("Voice Express response:" + response);
        }

        Document doc = JAXPUtil.parseDocument(response);
        String result = 
            JAXPUtil.selectSingleNodeText(doc, "/vexSetMessage/result", "VEX", "http://www.voice-express.net/talkingGift/v1.0/");

        if (result.equalsIgnoreCase(RecordableBouquetServiceConstants.VOICE_EXPRESS_DUPLICATE_PERSONAL_GREETING_ID_RESULT))
        {
            logger.warn("Generated Personal Greeting Id " + personalGreetingId + " is already in use.");
            throw new DuplicatePersonalGreetingIdException("Personal Greeting Id " + personalGreetingId + 
                                                           " is already in use.");
        }
        else if (!result.equalsIgnoreCase(RecordableBouquetServiceConstants.VOICE_EXPRESS_SUCCESS_RESULT))
        {
            throw new RecordableBouquetServiceException("Unable to interpret response from Voice Express.  Response:" + 
                                                        response);
        }
    }
}
