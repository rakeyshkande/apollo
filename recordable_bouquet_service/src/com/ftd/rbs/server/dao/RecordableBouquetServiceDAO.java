package com.ftd.rbs.server.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.HashMap;


/**
 * Data Access Object for the Recordable Bouquet Service.  
 */
public class RecordableBouquetServiceDAO
{
    protected Logger logger = new Logger(this.getClass().getName());

    public RecordableBouquetServiceDAO()
    {
    }

    /**
     * Obtain the next Personal Greeting Id sequence.
     * @param conn Database connection
     * @return Personal Greeting Id sequence number
     * @throws Exception
     */
    public String getNextPersonalGreetingSequence(Connection conn)
        throws Exception
    {
        String sequence = null;

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_TABLE", "personal_greeting");

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("KEYGEN");
        dataRequest.setInputParams(inputParms);

        sequence = (String) DataAccessUtil.getInstance().execute(dataRequest);

        return sequence;
    }

}
