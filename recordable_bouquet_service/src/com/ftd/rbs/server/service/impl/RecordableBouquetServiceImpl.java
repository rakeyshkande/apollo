package com.ftd.rbs.server.service.impl;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.rbs.common.RecordableBouquetServiceConstants;
import com.ftd.rbs.exception.DuplicatePersonalGreetingIdException;
import com.ftd.rbs.exception.RecordableBouquetServiceException;
import com.ftd.rbs.server.bo.RecordableBouquetServiceBO;

import java.rmi.RemoteException;

import java.sql.Connection;

import java.util.Calendar;

/**
 * Server version of the RecordableBouquetService implementation.
 */
public class RecordableBouquetServiceImpl
    implements com.ftd.rbs.webservices.RecordableBouquetService
{
    protected Logger logger = new Logger(this.getClass().getName());

    /**
     * Generate and transmit a Personal Greeting Id to Voice Express.
     * @param deliveryDate Delivery date of the order
     * @param orderNumber Order number which is not required
     * @return Personal Greeting Id
     * @throws RecordableBouquetServiceException
     * @throws DuplicatePersonalGreetingIdException
     */
    public String generateAndTransmitPersonalGreetingId(Calendar deliveryDate, String orderNumber)
        throws RemoteException, RecordableBouquetServiceException, DuplicatePersonalGreetingIdException
    {
        Connection conn = null;

        try
        {
            ConfigurationUtil cu = ConfigurationUtil.getInstance();
            RecordableBouquetServiceBO rbsBO = new RecordableBouquetServiceBO();
            String personalGreetingId = null;
            boolean transmitted = false;
            int maxAttempts = 0;            
            
            // Obtain the maximum number of attempts to generate and validate a Personal Greeting Id.
            try
            {
                maxAttempts = 
                        Integer.parseInt(cu.getFrpGlobalParm(RecordableBouquetServiceConstants.RECORDABLE_BOUQUET_SERVICE_CONFIG_CONTEXT, 
                                                             RecordableBouquetServiceConstants.MAX_ATTEMPTS_TO_VALIDATE_PERSONAL_GREETING_ID));
            }
            catch (Exception e)
            {
                new RecordableBouquetServiceException("Please specify a valid number of max attempts.", e);
            }

            conn = getNewConnection();

            // Attempt to generate a Personal Greeting Id and transmit to Voice Express
            for (int attempts = 0; !transmitted && attempts < maxAttempts; attempts++)
            {
                personalGreetingId = rbsBO.generatePersonalGreetingId(conn);

                try
                {
                    rbsBO.sendVoiceExpressGetRequest(personalGreetingId, deliveryDate.getTime(), orderNumber);
                    transmitted = true;
                }
                catch (Exception e)
                {
                    transmitted = false;
                }
            }

            if (!transmitted)
            {
                String msg = 
                    "Could not validate a Personal Greeting Id after " + maxAttempts + " tries.  Check the Recordable Bouquet Service log for errors.";
                SystemMessenger sm = SystemMessenger.getInstance();
                SystemMessengerVO smVO = new SystemMessengerVO();
                smVO.setMessage(msg);
                smVO.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
                smVO.setSubject("Recordable Bouquet Service - Max attempts reached");
                smVO.setSource("Recordable Bouquet Service");
                smVO.setType("ERROR");
                sm.send(smVO, conn, false);
                throw new RecordableBouquetServiceException(msg);
            }

            return personalGreetingId;
        }
        catch (DuplicatePersonalGreetingIdException dpgie)
        {
            logger.error(dpgie);
            throw dpgie;
        }
        catch (RecordableBouquetServiceException rbse)
        {
            logger.error(rbse);
            throw rbse;
        }
        catch (Throwable e)
        {
            logger.error(e);
            throw new RecordableBouquetServiceException(e);
        }
        finally
        {
            try
            {
                conn.close();
            }
            catch (Exception e)
            {
                logger.error("Error closing connection", e);
            }
        }
    }

    /**
     * !!Reserved for future use!!! Generate a Personal Greeting Id.
     * @return Personal Greeting Id
     * @throws RecordableBouquetServiceException
     */
    public String generatePersonalGreetingId()
        throws RemoteException, RecordableBouquetServiceException
    {
        throw new RecordableBouquetServiceException("Reserved for future use");
    }

    /**
     * !!Reserved for future use!!! Transmit a Personal Greeting Id to Voice Express.  If the Personal
     * Greeting Id already exists a DuplicatePersonalGreetingIdException
     * is thrown.
     * @param personalGreetingId Personal Greeting Id
     * @param deliveryDate Delivery date of the order
     * @param orderNumber Order number
     * @throws RecordableBouquetServiceException
     * @throws DuplicatePersonalGreetingIdException
     */
    public void transmitPersonalGreetingId(String personalGreetingId, Calendar deliveryDate, String orderNumber)
        throws RemoteException, RecordableBouquetServiceException, DuplicatePersonalGreetingIdException
    {
        throw new RecordableBouquetServiceException("Reserved for future use");
    }

    /**
     * Get a new database connection.
     * @return Database connection
     * @throws Exception
     */
    protected Connection getNewConnection()
        throws Exception
    {
        ConfigurationUtil cu = ConfigurationUtil.getInstance();
        String datasource = 
            cu.getProperty(RecordableBouquetServiceConstants.PROPERTY_FILE, RecordableBouquetServiceConstants.DATASOURCE_NAME);

        Connection conn = DataSourceUtil.getInstance().getConnection(datasource);

        return conn;
    }
}
