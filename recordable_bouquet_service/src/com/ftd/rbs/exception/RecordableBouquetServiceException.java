package com.ftd.rbs.exception;

/**
 * Exception representing an error with the Recordable Bouquet Service
 */
public class RecordableBouquetServiceException
    extends Exception
{
    public RecordableBouquetServiceException(Throwable cause)
    {
        super(cause);
    }

    public RecordableBouquetServiceException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public RecordableBouquetServiceException(String message)
    {
        super(message);
    }

    public RecordableBouquetServiceException()
    {
    }
}
