package com.ftd.rbs.exception;

/**
 * Exception representing a duplicate Personal Greeting Id
 */
public class DuplicatePersonalGreetingIdException
    extends Exception
{
    public DuplicatePersonalGreetingIdException(Throwable cause)
    {
        super(cause);
    }

    public DuplicatePersonalGreetingIdException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public DuplicatePersonalGreetingIdException(String message)
    {
        super(message);
    }

    public DuplicatePersonalGreetingIdException()
    {
    }
}
