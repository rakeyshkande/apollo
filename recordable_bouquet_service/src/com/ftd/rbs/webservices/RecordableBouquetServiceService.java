/**
 * RecordableBouquetServiceService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ftd.rbs.webservices;

public interface RecordableBouquetServiceService extends javax.xml.rpc.Service {
    public java.lang.String getRecordableBouquetServiceAddress();

    public com.ftd.rbs.webservices.RecordableBouquetService getRecordableBouquetService() throws javax.xml.rpc.ServiceException;

    public com.ftd.rbs.webservices.RecordableBouquetService getRecordableBouquetService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
