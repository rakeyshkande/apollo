/**
 * RecordableBouquetService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ftd.rbs.webservices;

public interface RecordableBouquetService extends java.rmi.Remote {
    public java.lang.String generateAndTransmitPersonalGreetingId(java.util.Calendar in0, java.lang.String in1) throws java.rmi.RemoteException, com.ftd.rbs.exception.DuplicatePersonalGreetingIdException, com.ftd.rbs.exception.RecordableBouquetServiceException;
    public java.lang.String generatePersonalGreetingId() throws java.rmi.RemoteException, com.ftd.rbs.exception.RecordableBouquetServiceException;
    public void transmitPersonalGreetingId(java.lang.String in0, java.util.Calendar in1, java.lang.String in2) throws java.rmi.RemoteException, com.ftd.rbs.exception.DuplicatePersonalGreetingIdException, com.ftd.rbs.exception.RecordableBouquetServiceException;
}
