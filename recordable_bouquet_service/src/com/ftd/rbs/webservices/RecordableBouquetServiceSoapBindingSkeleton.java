/**
 * RecordableBouquetServiceSoapBindingSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ftd.rbs.webservices;

public class RecordableBouquetServiceSoapBindingSkeleton implements com.ftd.rbs.webservices.RecordableBouquetService, org.apache.axis.wsdl.Skeleton {
    private com.ftd.rbs.webservices.RecordableBouquetService impl;
    private static java.util.Map _myOperations = new java.util.Hashtable();
    private static java.util.Collection _myOperationsList = new java.util.ArrayList();

    /**
    * Returns List of OperationDesc objects with this name
    */
    public static java.util.List getOperationDescByName(java.lang.String methodName) {
        return (java.util.List)_myOperations.get(methodName);
    }

    /**
    * Returns Collection of OperationDescs
    */
    public static java.util.Collection getOperationDescs() {
        return _myOperationsList;
    }

    static {
        org.apache.axis.description.OperationDesc _oper;
        org.apache.axis.description.FaultDesc _fault;
        org.apache.axis.description.ParameterDesc [] _params;
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in0"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"), java.util.Calendar.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in1"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("generateAndTransmitPersonalGreetingId", _params, new javax.xml.namespace.QName("", "generateAndTransmitPersonalGreetingIdReturn"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:recordableBouquetService", "generateAndTransmitPersonalGreetingId"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("generateAndTransmitPersonalGreetingId") == null) {
            _myOperations.put("generateAndTransmitPersonalGreetingId", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("generateAndTransmitPersonalGreetingId")).add(_oper);
        _fault = new org.apache.axis.description.FaultDesc();
        _fault.setName("DuplicatePersonalGreetingIdException");
        _fault.setQName(new javax.xml.namespace.QName("urn:recordableBouquetService", "fault"));
        _fault.setClassName("com.ftd.rbs.exception.DuplicatePersonalGreetingIdException");
        _fault.setXmlType(new javax.xml.namespace.QName("http://exception.rbs.ftd.com", "DuplicatePersonalGreetingIdException"));
        _oper.addFault(_fault);
        _fault = new org.apache.axis.description.FaultDesc();
        _fault.setName("RecordableBouquetServiceException");
        _fault.setQName(new javax.xml.namespace.QName("urn:recordableBouquetService", "fault"));
        _fault.setClassName("com.ftd.rbs.exception.RecordableBouquetServiceException");
        _fault.setXmlType(new javax.xml.namespace.QName("http://exception.rbs.ftd.com", "RecordableBouquetServiceException"));
        _oper.addFault(_fault);
        _params = new org.apache.axis.description.ParameterDesc [] {
        };
        _oper = new org.apache.axis.description.OperationDesc("generatePersonalGreetingId", _params, new javax.xml.namespace.QName("", "generatePersonalGreetingIdReturn"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:recordableBouquetService", "generatePersonalGreetingId"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("generatePersonalGreetingId") == null) {
            _myOperations.put("generatePersonalGreetingId", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("generatePersonalGreetingId")).add(_oper);
        _fault = new org.apache.axis.description.FaultDesc();
        _fault.setName("RecordableBouquetServiceException");
        _fault.setQName(new javax.xml.namespace.QName("urn:recordableBouquetService", "fault"));
        _fault.setClassName("com.ftd.rbs.exception.RecordableBouquetServiceException");
        _fault.setXmlType(new javax.xml.namespace.QName("http://exception.rbs.ftd.com", "RecordableBouquetServiceException"));
        _oper.addFault(_fault);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in0"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in1"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"), java.util.Calendar.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in2"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("transmitPersonalGreetingId", _params, null);
        _oper.setElementQName(new javax.xml.namespace.QName("urn:recordableBouquetService", "transmitPersonalGreetingId"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("transmitPersonalGreetingId") == null) {
            _myOperations.put("transmitPersonalGreetingId", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("transmitPersonalGreetingId")).add(_oper);
        _fault = new org.apache.axis.description.FaultDesc();
        _fault.setName("DuplicatePersonalGreetingIdException");
        _fault.setQName(new javax.xml.namespace.QName("urn:recordableBouquetService", "fault"));
        _fault.setClassName("com.ftd.rbs.exception.DuplicatePersonalGreetingIdException");
        _fault.setXmlType(new javax.xml.namespace.QName("http://exception.rbs.ftd.com", "DuplicatePersonalGreetingIdException"));
        _oper.addFault(_fault);
        _fault = new org.apache.axis.description.FaultDesc();
        _fault.setName("RecordableBouquetServiceException");
        _fault.setQName(new javax.xml.namespace.QName("urn:recordableBouquetService", "fault"));
        _fault.setClassName("com.ftd.rbs.exception.RecordableBouquetServiceException");
        _fault.setXmlType(new javax.xml.namespace.QName("http://exception.rbs.ftd.com", "RecordableBouquetServiceException"));
        _oper.addFault(_fault);
    }

    public RecordableBouquetServiceSoapBindingSkeleton() {
        this.impl = new com.ftd.rbs.server.service.impl.RecordableBouquetServiceImpl();
    }

    public RecordableBouquetServiceSoapBindingSkeleton(com.ftd.rbs.webservices.RecordableBouquetService impl) {
        this.impl = impl;
    }
    public java.lang.String generateAndTransmitPersonalGreetingId(java.util.Calendar in0, java.lang.String in1) throws java.rmi.RemoteException, com.ftd.rbs.exception.DuplicatePersonalGreetingIdException, com.ftd.rbs.exception.RecordableBouquetServiceException
    {
        java.lang.String ret = impl.generateAndTransmitPersonalGreetingId(in0, in1);
        return ret;
    }

    public java.lang.String generatePersonalGreetingId() throws java.rmi.RemoteException, com.ftd.rbs.exception.RecordableBouquetServiceException
    {
        java.lang.String ret = impl.generatePersonalGreetingId();
        return ret;
    }

    public void transmitPersonalGreetingId(java.lang.String in0, java.util.Calendar in1, java.lang.String in2) throws java.rmi.RemoteException, com.ftd.rbs.exception.DuplicatePersonalGreetingIdException, com.ftd.rbs.exception.RecordableBouquetServiceException
    {
        impl.transmitPersonalGreetingId(in0, in1, in2);
    }

}
