/**
 * RecordableBouquetServiceServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ftd.rbs.webservices;

public class RecordableBouquetServiceServiceLocator extends org.apache.axis.client.Service implements com.ftd.rbs.webservices.RecordableBouquetServiceService {

    public RecordableBouquetServiceServiceLocator() {
    }


    public RecordableBouquetServiceServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public RecordableBouquetServiceServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for RecordableBouquetService
    private java.lang.String RecordableBouquetService_address = "http://consumer-dev.ftdi.com/rbs/services/RecordableBouquetService";

    public java.lang.String getRecordableBouquetServiceAddress() {
        return RecordableBouquetService_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String RecordableBouquetServiceWSDDServiceName = "RecordableBouquetService";

    public java.lang.String getRecordableBouquetServiceWSDDServiceName() {
        return RecordableBouquetServiceWSDDServiceName;
    }

    public void setRecordableBouquetServiceWSDDServiceName(java.lang.String name) {
        RecordableBouquetServiceWSDDServiceName = name;
    }

    public com.ftd.rbs.webservices.RecordableBouquetService getRecordableBouquetService() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(RecordableBouquetService_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getRecordableBouquetService(endpoint);
    }

    public com.ftd.rbs.webservices.RecordableBouquetService getRecordableBouquetService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.ftd.rbs.webservices.RecordableBouquetServiceSoapBindingStub _stub = new com.ftd.rbs.webservices.RecordableBouquetServiceSoapBindingStub(portAddress, this);
            _stub.setPortName(getRecordableBouquetServiceWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setRecordableBouquetServiceEndpointAddress(java.lang.String address) {
        RecordableBouquetService_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.ftd.rbs.webservices.RecordableBouquetService.class.isAssignableFrom(serviceEndpointInterface)) {
                com.ftd.rbs.webservices.RecordableBouquetServiceSoapBindingStub _stub = new com.ftd.rbs.webservices.RecordableBouquetServiceSoapBindingStub(new java.net.URL(RecordableBouquetService_address), this);
                _stub.setPortName(getRecordableBouquetServiceWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("RecordableBouquetService".equals(inputPortName)) {
            return getRecordableBouquetService();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("urn:recordableBouquetService", "RecordableBouquetServiceService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("urn:recordableBouquetService", "RecordableBouquetService"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("RecordableBouquetService".equals(portName)) {
            setRecordableBouquetServiceEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
