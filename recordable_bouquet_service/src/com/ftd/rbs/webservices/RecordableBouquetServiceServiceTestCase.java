/**
 * RecordableBouquetServiceServiceTestCase.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ftd.rbs.webservices;

import org.apache.axis.AxisFault;

import com.ftd.osp.utilities.plugins.Logger;

public class RecordableBouquetServiceServiceTestCase extends junit.framework.TestCase {
	
	private Logger logger = new Logger(this.getClass().getName());
	
    public RecordableBouquetServiceServiceTestCase(java.lang.String name) {
        super(name);
    }

    public void testRecordableBouquetServiceWSDL() throws Exception {
        javax.xml.rpc.ServiceFactory serviceFactory = javax.xml.rpc.ServiceFactory.newInstance();
        java.net.URL url = new java.net.URL(new com.ftd.rbs.webservices.RecordableBouquetServiceServiceLocator().getRecordableBouquetServiceAddress() + "?WSDL");
        javax.xml.rpc.Service service = serviceFactory.createService(url, new com.ftd.rbs.webservices.RecordableBouquetServiceServiceLocator().getServiceName());
        assertTrue(service != null);
    }

    public void test1RecordableBouquetServiceGenerateAndTransmitPersonalGreetingId() throws Exception {
        com.ftd.rbs.webservices.RecordableBouquetServiceSoapBindingStub binding;
        try {
            binding = (com.ftd.rbs.webservices.RecordableBouquetServiceSoapBindingStub)
                          new com.ftd.rbs.webservices.RecordableBouquetServiceServiceLocator().getRecordableBouquetService();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            java.lang.String value = null;
            value = binding.generateAndTransmitPersonalGreetingId(java.util.Calendar.getInstance(), new java.lang.String());
        }
        catch (com.ftd.rbs.exception.DuplicatePersonalGreetingIdException e1) {
            throw new junit.framework.AssertionFailedError("DuplicatePersonalGreetingIdException Exception caught: " + e1);
        }
        catch (com.ftd.rbs.exception.RecordableBouquetServiceException e2) {
            throw new junit.framework.AssertionFailedError("RecordableBouquetServiceException Exception caught: " + e2);
        }
            // TBD - validate results
    }

    
    public void test2RecordableBouquetServiceGeneratePersonalGreetingId() throws Exception {
        com.ftd.rbs.webservices.RecordableBouquetServiceSoapBindingStub binding;
        try {
            binding = (com.ftd.rbs.webservices.RecordableBouquetServiceSoapBindingStub)
                          new com.ftd.rbs.webservices.RecordableBouquetServiceServiceLocator().getRecordableBouquetService();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            java.lang.String value = null;
            value = binding.generatePersonalGreetingId();
        }
        catch (com.ftd.rbs.exception.RecordableBouquetServiceException e1) {
            //throw new junit.framework.AssertionFailedError("RecordableBouquetServiceException Exception caught: " + e1);
        	logger.info("call to generatePersonalGreetingId failed because it is not implemented yet");
        	
        } catch (Exception e) {
        	logger.info("call to generatePersonalGreetingId failed because it is not implemented yet");
        }
            // TBD - validate results
    }

    public void test3RecordableBouquetServiceTransmitPersonalGreetingId() throws Exception {
        com.ftd.rbs.webservices.RecordableBouquetServiceSoapBindingStub binding;
        try {
            binding = (com.ftd.rbs.webservices.RecordableBouquetServiceSoapBindingStub)
                          new com.ftd.rbs.webservices.RecordableBouquetServiceServiceLocator().getRecordableBouquetService();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            binding.transmitPersonalGreetingId(new java.lang.String(), java.util.Calendar.getInstance(), new java.lang.String());
        }
        catch (com.ftd.rbs.exception.DuplicatePersonalGreetingIdException e1) {
            throw new junit.framework.AssertionFailedError("DuplicatePersonalGreetingIdException Exception caught: " + e1);
        }
        catch (com.ftd.rbs.exception.RecordableBouquetServiceException e2) {
            //throw new junit.framework.AssertionFailedError("RecordableBouquetServiceException Exception caught: " + e2);
        	logger.info("call to transmitPersonalGreetingId failed because it is not implemented yet");
        } catch (Exception e) {
        	logger.info("call to transmitPersonalGreetingId failed because it is not implemented yet");
        }
            // TBD - validate results
    }

}
