package com.ftd.rbs.client.service.impl;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.rbs.common.RecordableBouquetServiceConstants;
import com.ftd.rbs.common.service.RecordableBouquetService;
import com.ftd.rbs.exception.DuplicatePersonalGreetingIdException;
import com.ftd.rbs.exception.RecordableBouquetServiceException;
import com.ftd.rbs.webservices.RecordableBouquetServiceServiceLocator;
import com.ftd.rbs.webservices.RecordableBouquetServiceSoapBindingStub;

import java.util.Calendar;
import java.util.Date;


/**
 * Remote version of the RecordableBouquetService implementation.
 */
public class RecordableBouquetServiceRemoteImpl
    implements RecordableBouquetService
{
    protected Logger logger = new Logger(this.getClass().getName());

    protected String serverAddress;

    public RecordableBouquetServiceRemoteImpl()
    {
    }

    /**
     * Generate and transmit a Personal Greeting Id to Voice Express.
     * @param deliveryDate Delivery date of the order
     * @param orderNumber Order number which is not required
     * @return Personal Greeting Id
     * @throws RecordableBouquetServiceException
     * @throws DuplicatePersonalGreetingIdException
     */
    public String generateAndTransmitPersonalGreetingId(Date deliveryDate, String orderNumber)
        throws RecordableBouquetServiceException, DuplicatePersonalGreetingIdException
    {
        RecordableBouquetServiceSoapBindingStub binding = null;
        String personalGreetingId = null;
        try
        {
            // Convert Date to a Calendar for the Web Service
            Calendar deliveryDateCalendar = Calendar.getInstance();
            deliveryDateCalendar.setTime(deliveryDate);

            binding = getSoapBinding();
            personalGreetingId = binding.generateAndTransmitPersonalGreetingId(deliveryDateCalendar, orderNumber);
        }
        catch (DuplicatePersonalGreetingIdException dpgie)
        {
            throw dpgie;
        }
        catch (RecordableBouquetServiceException rbse)
        {
            throw rbse;
        }
        catch (Throwable t)
        {
            String msg = "Error processing Recordable Bouquet Service:generateAndTransmitPersonalGreetingId";
            logger.error(msg, t);
            throw new RecordableBouquetServiceException(msg, t);
        }
        
        return personalGreetingId;
    }

    /**
     * Generate a Personal Greeting Id.
     * @return Personal Greeting Id
     * @throws RecordableBouquetServiceException
     */
    public String generatePersonalGreetingId()
        throws RecordableBouquetServiceException
    {
        RecordableBouquetServiceSoapBindingStub binding = null;
        String personalGreetingId = null;
        try
        {
            binding = getSoapBinding();
            personalGreetingId = binding.generatePersonalGreetingId();
        }
        catch (RecordableBouquetServiceException rbse)
        {
            throw rbse;
        }
        catch (Throwable t)
        {
            String msg = "Error processing Recordable Bouquet Service:generatePersonalGreetingId";
            logger.error(msg, t);
            throw new RecordableBouquetServiceException(msg, t);
        }
        
        return personalGreetingId;
    }

    /**
     * Transmit a Personal Greeting Id to Voice Express.  If the Personal
     * Greeting Id already exists a DuplicatePersonalGreetingIdException
     * is thrown.
     * @param personalGreetingId Personal Greeting Id
     * @param deliveryDate Delivery date of the order
     * @param orderNumber Order number
     * @throws RecordableBouquetServiceException
     * @throws DuplicatePersonalGreetingIdException
     */
    public void transmitPersonalGreetingId(String personalGreetingId, Date deliveryDate, String orderNumber)
        throws RecordableBouquetServiceException, DuplicatePersonalGreetingIdException
    {
        RecordableBouquetServiceSoapBindingStub binding = null;
        try
        {
            // Convert Date to a Calendar for the Web Service
            Calendar deliveryDateCalendar = Calendar.getInstance();
            deliveryDateCalendar.setTime(deliveryDate);

            binding = getSoapBinding();
            binding.transmitPersonalGreetingId(personalGreetingId, deliveryDateCalendar, orderNumber);
        }
        catch (DuplicatePersonalGreetingIdException dpgie)
        {
            throw dpgie;
        }
        catch (RecordableBouquetServiceException rbse)
        {
            throw rbse;
        }
        catch (Throwable t)
        {
            String msg = "Error processing Recordable Bouquet Service:transmitPersonalGreetingId";
            logger.error(msg, t);
            throw new RecordableBouquetServiceException(msg, t);
        }
    }

    /**
     * Get the soap binding.  Setup the remote connection to be the proper Recordable Bouquet Service server.
     * @return Binding stub
     * @throws Exception
     */
    protected RecordableBouquetServiceSoapBindingStub getSoapBinding()
        throws Exception
    {
        RecordableBouquetServiceSoapBindingStub binding = null;
        RecordableBouquetServiceServiceLocator locator = null;
        String serverAddress = getServerAddress();

        locator = new RecordableBouquetServiceServiceLocator();
        locator.setRecordableBouquetServiceEndpointAddress(serverAddress);

        binding = (RecordableBouquetServiceSoapBindingStub) locator.getRecordableBouquetService();

        return binding;
    }

    /**
     * Lazy initialization of the server address.  The server address is retrieved
     * from global_parms.
     * @return Server address
     */
    protected String getServerAddress()
        throws Exception
    {
        if (serverAddress == null)
        {
            ConfigurationUtil cu = ConfigurationUtil.getInstance();
            serverAddress = 
                    cu.getFrpGlobalParm(RecordableBouquetServiceConstants.RECORDABLE_BOUQUET_SERVICE_CONFIG_CONTEXT, 
                                        RecordableBouquetServiceConstants.RECORDABLE_BOUQUET_SERVICE_URL);
        }

        return serverAddress;
    }

    /**
     * Method for performing unit tests to a specific address.
     * @param serverAddress Server address
     */
    public void setServerAddress(String serverAddress)
    {
        this.serverAddress = serverAddress;
    }
}
