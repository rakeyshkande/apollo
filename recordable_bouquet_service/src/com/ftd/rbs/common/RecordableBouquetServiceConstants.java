package com.ftd.rbs.common;

/**
 * RecordableBouquetService constants.
 */
public interface RecordableBouquetServiceConstants
{
    // Config file
    public static final String PROPERTY_FILE = "recordable-bouquet-service-config.xml";
    public static final String DATASOURCE_NAME = "DATASOURCE";
    
    // Global parms
    public static final String RECORDABLE_BOUQUET_SERVICE_CONFIG_CONTEXT = "RECORDABLE_BOUQUET_SERVICE_CONFIG";
    public static final String RECORDABLE_BOUQUET_SERVICE_URL = "RECORDABLE_BOUQUET_SERVICE_URL";
    public static final String VOICE_EXPRESS_SERVICE_URL = "VOICE_EXPRESS_SERVICE_URL";
    public static final String VOICE_EXPRESS_CUSTOMER_CODE = "VOICE_EXPRESS_CUSTOMER_CODE";
    public static final String LEADING_PERSONAL_GREETING_ID_DIGIT = "LEADING_PERSONAL_GREETING_ID_DIGIT";
    public static final String MAX_ATTEMPTS_TO_VALIDATE_PERSONAL_GREETING_ID = "MAX_ATTEMPTS_TO_VALIDATE_PERSONAL_GREETING_ID";

    // Voice Express Request
    public static final int VOICE_EXPRESS_PERSONAL_GREETING_ID_LENGTH = 8;
    public static final String VOICE_EXPRESS_DELIVERY_DATE_FORMAT = "MMddyyyy";
    public static final String VOICE_EXPRESS_CUSTOMER_CODE_PARAMETER = "customerCode";
    public static final String VOICE_EXPRESS_ORDER_NUMBER_PARAMETER = "orderNumber";
    public static final String VOICE_EXPRESS_DELIVERY_DATE_PARAMETER = "shippingDate";
    public static final String VOICE_EXPRESS_REFERENCE_NUMBER_PARAMETER = "referenceNumber";
    public static final String VOICE_EXPRESS_DUMMY_ORDER_NUMBER = "C0000000";

    // Voice Express Request
    public static final String VOICE_EXPRESS_SUCCESS_RESULT = "VEX_OK";
    public static final String VOICE_EXPRESS_DUPLICATE_PERSONAL_GREETING_ID_RESULT = "VEX_ORDER_NUMBER_IN_USE";
}
