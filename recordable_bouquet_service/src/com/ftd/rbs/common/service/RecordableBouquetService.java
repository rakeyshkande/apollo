package com.ftd.rbs.common.service;

import com.ftd.rbs.exception.DuplicatePersonalGreetingIdException;
import com.ftd.rbs.exception.RecordableBouquetServiceException;

import java.util.Date;


/**
 * Recordable Bouquet Service interface.
 */
public interface RecordableBouquetService
{
    public String generateAndTransmitPersonalGreetingId(Date deliveryDate, String orderNumber)
        throws RecordableBouquetServiceException, DuplicatePersonalGreetingIdException;

    public String generatePersonalGreetingId()
        throws RecordableBouquetServiceException;

    public void transmitPersonalGreetingId(String personalGreetingId, Date deliveryDate, String orderNumber)
        throws RecordableBouquetServiceException, DuplicatePersonalGreetingIdException;
}
