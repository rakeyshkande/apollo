@ECHO OFF
ECHO setting axis classpath
CALL setaxiscp.bat
ECHO Creating java classes from WSDL
java -cp %AXISCP% org.apache.axis.wsdl.WSDL2Java -o ..\src -d Session -s -S true -N"urn:recordableBouquetService" "com.ftd.rbs.webservices" -c "com.ftd.rbs.server.service.impl.RecordableBouquetServiceImpl" -t "recordable_bouquet_service.wsdl"
