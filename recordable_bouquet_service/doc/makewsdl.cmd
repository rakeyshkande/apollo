ECHO setting axis classpath
CALL setaxiscp.bat

ECHO Creating WSDL from interface
CALL java2wsdl.bat -o recordable_bouquet_service.wsdl -l "http://zinc3:8080/rbs/RecordableBouquetService" -n "urn:recordableBouquetService" -p"com.ftd.rbs.webservices"="urn:recordableBouquetService" com.ftd.rbs.common.service.RecordableBouquetService