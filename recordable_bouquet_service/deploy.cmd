@echo off

if not exist dist (
    @echo .
    @echo .
    @echo . Build is required
    @echo .
    @echo . Use "build" first
    @echo .
) else (
    ant -file deploy-build.xml
)
