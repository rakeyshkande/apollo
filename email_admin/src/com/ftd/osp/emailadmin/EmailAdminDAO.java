package com.ftd.osp.emailadmin;

import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.SourceMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.*;
//import com.ftd.osp.utilities.cacheMgr.handlers.*;
//import com.ftd.osp.utilities.cacheMgr.*;
import com.ftd.osp.emailadmin.vo.EmailProgramVO;
import com.ftd.osp.emailadmin.vo.EmailSectionContentVO;
import com.ftd.osp.emailadmin.vo.EmailSectionsVO;
import com.ftd.osp.emailadmin.servlet.EmailAdminConstants;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import java.io.IOException;
import java.sql.Connection;
import javax.servlet.http.HttpServletRequest;

/**
 * Data access class for administrating email confirmation
 *
 * @author Doug Johnson
 */
public class EmailAdminDAO
{
    private Logger logger;
    private final static String EMAIL_ADMIN_CONFIG_FILE = "emailadmin-config.xml";
    private static final String GET_SOURCE_CODE_BY_ID_STATEMENT = "GET_SOURCE_CODE_RECORD";

    //procedures
    private static final String INSERT_PROGRAM = "FRP.INSERT_PROGRAM_MAPPING";
    private static final String INSERT_EMAIL_SECTIONS = "FRP.INSERT_EMAIL_SECTIONS";
    private static final String INSERT_EMAIL_SECTION_CONTENT = "FRP.INSERT_EMAIL_SECTION_CONTENT";
    private static final String SEARCH_SECTION_BY_TYPE = "FRP.SEARCH_SECTIONS_BY_TYPE";
    private static final String VIEW_SECTION_BY_TYPE = "FRP.VIEW_SECTIONS_BY_TYPE";
    private static final String SEARCH_PROGRAM_BY_SOURCE = "FRP.SEARCH_PROGRAM_BY_SOURCE";
    private static final String SEARCH_PROGRAM_BY_NAME = "FRP.SEARCH_PROGRAM_BY_NAME";
    private static final String VIEW_SECTION_BY_NAME = "FRP.VIEW_SECTION_BY_NAME";
    private static final String VIEW_SECTION = "FRP.VIEW_SECTION";
    private static final String VIEW_CONTENT_SECTION_BY_NAME = "FRP.VIEW_CONTENT_SECTION_BY_NAME";
    private static final String DELETE_EMAIL_SECTION = "FRP.DELETE_EMAIL_SECTIONS";
    private static final String VIEW_SOURCES = "FRP.VIEW_SOURCES";
    private static final String UPDATE_EMAIL_SECTIONS = "FRP.UPDATE_EMAIL_SECTIONS";
    private static final String UPDATE_EMAIL_SECTION_CONTENT = "FRP.UPDATE_EMAIL_SECTION_CONTENT";
    private static final String DELETE_EMAIL_SECTION_CONTENT = "FRP.DELETE_EMAIL_SECTION_CONTENT";
    private static final String UPDATE_CUSTOM_PROGRAM_SOURCE = "FRP.UPDATE_CUSTOM_PROGRAM_SOURCE";
    private static final String RESET_CUSTOM_PROGRAM_SOURCE = "FRP.RESET_CUSTOM_PROGRAM_SOURCE";
    private static final String INSERT_PROGRAM_SECTION_MAPPING = "FRP.INSERT_PROGRAM_SECTION_MAPPING";
    private static final String VIEW_PROGRAM = "FRP.VIEW_PROGRAM";
    private static final String VIEW_CURRENT_SECTION_MAPPING = "FRP.VIEW_CURRENT_SECTION_MAPPING";
    private static final String VIEW_SOURCES_BY_PROGRAM_ID = "FRP.VIEW_SOURCES_BY_PROGRAM_ID";
    private static final String UPDATE_PROGRAM_MAPPING = "FRP.UPDATE_PROGRAM_MAPPING";
    private static final String DELETE_PROGRAM_SECTION_MAPPING = "FRP.DELETE_PROGRAM_SECTION_MAPPING";
    private static final String DELETE_PROGRAM_MAPPING = "FRP.DELETE_PROGRAM_MAPPING";
    private static final String VIEW_SECTIONS = "FRP.VIEW_SECTIONS";
    private static final String SECTION_NAME_EXISTS = "FRP.SECTION_NAME_EXISTS";
    private static final String PROGRAM_NAME_EXISTS = "FRP.PROGRAM_NAME_EXISTS";
    private static final String UPDATE_DEFAULT_PROGRAM_SOURCE = "FRP.UPDATE_DEFAULT_PROGRAM_SOURCE";
    private static final String CUSTOM_PROGRAM_OVERLAP = "FRP.CUSTOM_PROGRAM_OVERLAP";

    //ref cursors returned from database procedure
    private static final String EMAIL_SECTIONS_CURSOR = "RegisterOutParameterSectionscur";
    private static final String EMAIL_SECTION_CONTENT_CURSOR = "RegisterOutParameterSectionContentscur";
    private static final String EMAIL_SOURCE_CURSOR = "RegisterOutParameterSourcecur";
    private static final String EMAIL_PROGRAM = "RegisterOutParameterProgramcur";

    //custom parameters returned from database procedure
    private static final String STATUS_PARAM = "RegisterOutParameterStatus";
    private static final String MESSAGE_PARAM = "RegisterOutParameterMessage";

    private String datasource;
    private String initialConextLocation;
    private String status;
    private String message;
    DataRequest dataRequest;

    //message content types
    private final static String SUBJECT = "Subject";
    private final static String HEADER = "Header";
    private final static String GUARANTEE = "Guarantee";
    private final static String MARKETING = "Marketing";
    private final static String CONTACT = "Contact";

  /**
   * EmailAdminDAO cunstructor
   * @param connection The Connection that will be used to access the email info
   *
   */
  public EmailAdminDAO()
  {
      this.logger = new Logger("com.ftd.osp.emailadmin.EmailAdminDAO");
  }

  /**
   * Insert a record into EMAIL_SECTION table and EMAIL_SECTION_CONTENT table.
   * as one transaction.
   * @param EmailSectionsVO
   * @param EmailSectionContentVO
   * @return true/false
   * @throws Exception
   */
  public void insertEmailSection(EmailSectionsVO emailSection,EmailSectionContentVO emailSectionContent) throws Exception
  {
      dataRequest = DataRequestHelper.getInstance().getDataRequest();
      Connection con = dataRequest.getConnection();

      try
      {
          dataRequest.reset();

          HashMap sectionMap = new HashMap();
          long sectionId = 0;
          sectionMap = new HashMap();

          sectionMap.put("IN_UPDATED_BY",emailSection.getUpdatedBy());
          sectionMap.put("IN_SECTION_TYPE",emailSection.getSectionType());
          sectionMap.put("IN_SECTION_NAME",emailSection.getSectionName());
          sectionMap.put("IN_DESCRIPTION",emailSection.getSectionDescription());
          sectionMap.put("IN_START_DATE",getSQLDate(emailSection.getEffectiveStartDate()));
          sectionMap.put("IN_END_DATE",getSQLDate(emailSection.getEffectiveEndDate()));
          sectionMap.put("IN_CONTENT_TYPE",emailSection.getContentType());
          sectionMap.put("IN_DEFAULT_SECTION",emailSection.getSectionDefault());
          sectionMap.put("RegisterOutParameterSectionId",new Long(0));
          sectionMap.put(STATUS_PARAM,"");
          sectionMap.put(MESSAGE_PARAM,"");

          logger.debug("Setting IN_UPDATED_BY to: " + emailSection.getUpdatedBy());
          logger.debug("Setting IN_SECTION_ID to: " + emailSection.getSectionId());
          logger.debug("Setting IN_SECTION_TYPE to: " + emailSection.getSectionType());
          logger.debug("Setting IN_SECTION_NAME to: " + emailSection.getSectionName());
          logger.debug("Setting IN_DESCRIPTION to: " + emailSection.getSectionDescription());
          logger.debug("Setting IN_START_DATE to: " + emailSection.getEffectiveStartDate());
          logger.debug("Setting IN_END_DATE to: " + emailSection.getEffectiveEndDate());
          logger.debug("Setting IN_CONTENT_TYPE to: " + emailSection.getContentType());
          logger.debug("Setting IN_DEFAULT_SECTION to: " + emailSection.getSectionDefault());


          dataRequest.setStatementID(INSERT_EMAIL_SECTIONS);
          dataRequest.setInputParams(sectionMap);

          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

          Map outputs = (Map) dataAccessUtil.execute(dataRequest);

          status = (String) outputs.get(STATUS_PARAM);
          logger.debug("Status is:" + status);

          if(status.equals("N"))
          {
              message = (String) outputs.get(MESSAGE_PARAM);
              logger.debug("Message is:" + message);
              throw new Exception(message);
          }

          sectionId = Long.parseLong(outputs.get("RegisterOutParameterSectionId").toString());
          emailSection.setSectionId(sectionId);
          emailSectionContent.setSectionId(sectionId);
          insertEmailSectionContent(emailSectionContent);
      }
      catch(Exception ex)
      {
          logger.error(ex);
          throw(ex);
      }
      finally
      {
         con.close();
      }
  }

  /**
   * Insert a record into EMAIL_SECTION_CONTENT table. Uses the same conection as
   * insertEmailSection.
   *
   * @param EmailSectionContentVO
   * @throws Exception
   */
  public void insertEmailSectionContent(EmailSectionContentVO emailSectionContent) throws Exception
  {
      //dataRequest = DataRequestHelper.getInstance().getDataRequest();

      try
      {
          dataRequest.reset();

          HashMap sectionContentMap = new HashMap();

          //section content may be split
          String content[] = emailSectionContent.getContent();
          for (int i = 0; i < content.length; i++)
          {
              String text = content[i];

              sectionContentMap = new HashMap();

              sectionContentMap.put("IN_SECTION_ID",new Long(emailSectionContent.getSectionId()));
              sectionContentMap.put("IN_CONTENT_SECTION_ORDER_ID",new Long(i));
              sectionContentMap.put("IN_CONTENT",text.trim());
              sectionContentMap.put(STATUS_PARAM,"");
              sectionContentMap.put(MESSAGE_PARAM,"");
              logger.debug("setting IN_SECTION_ID to:" + emailSectionContent.getSectionId());
              logger.debug("setting IN_CONTENT_SECTION_ORDER_ID to:" + i);
              logger.debug("setting IN_CONTENT to:" + text.trim());

              dataRequest.reset();
              dataRequest.setStatementID(INSERT_EMAIL_SECTION_CONTENT);
              dataRequest.setInputParams(sectionContentMap);

              DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

              Map outputs = (Map) dataAccessUtil.execute(dataRequest);

              status = (String) outputs.get(STATUS_PARAM);
              logger.debug("Status is:" + status);

              if(status.equals("N"))
              {
                  message = (String) outputs.get(MESSAGE_PARAM);
                  logger.debug("Message is:" + message);
                  throw new Exception(message);
              }
          }
      }
      catch(Exception ex)
      {
          logger.error(ex);
          throw(ex);
      }
      finally
      {
          //dataRequest.getConnection().close();
      }
  }

  /**
   * Update a record in EMAIL_SECTION table
   *
   * @param EmailSectionsVO
   * @throws Exception
   */
  public void updateEmailSection(EmailSectionsVO emailSection,EmailSectionContentVO emailSectionContent) throws Exception
  {
      dataRequest = DataRequestHelper.getInstance().getDataRequest();
      Connection con = dataRequest.getConnection();
      try
      {
          dataRequest.reset();
          HashMap sectionMap = new HashMap();
          sectionMap = new HashMap();

          sectionMap.put("IN_UPDATED_BY",emailSection.getUpdatedBy());
          sectionMap.put("IN_SECTION_ID",new Long(emailSection.getSectionId()));
          sectionMap.put("IN_SECTION_TYPE",emailSection.getSectionType());
          sectionMap.put("IN_SECTION_NAME",emailSection.getSectionName());
          sectionMap.put("IN_DESCRIPTION",emailSection.getSectionDescription());
          sectionMap.put("IN_START_DATE",getSQLDate(emailSection.getEffectiveStartDate()));
          sectionMap.put("IN_END_DATE",getSQLDate(emailSection.getEffectiveEndDate()));
          sectionMap.put("IN_CONTENT_TYPE",emailSection.getContentType());
          sectionMap.put("IN_DEFAULT_SECTION",emailSection.getSectionDefault());
          sectionMap.put(STATUS_PARAM,"");
          sectionMap.put(MESSAGE_PARAM,"");

          logger.debug("Setting IN_UPDATED_BY to: " + emailSection.getUpdatedBy());
          logger.debug("Setting IN_SECTION_ID to: " + emailSection.getSectionId());
          logger.debug("Setting IN_SECTION_TYPE to: " + emailSection.getSectionType());
          logger.debug("Setting IN_SECTION_NAME to: " + emailSection.getSectionName());
          logger.debug("Setting IN_DESCRIPTION to: " + emailSection.getSectionDescription());
          logger.debug("Setting IN_START_DATE to: " + emailSection.getEffectiveStartDate());
          logger.debug("Setting IN_END_DATE to: " + emailSection.getEffectiveEndDate());
          logger.debug("Setting IN_CONTENT_TYPE to: " + emailSection.getContentType());
          logger.debug("Setting IN_DEFAULT_SECTION to: " + emailSection.getSectionDefault());


          dataRequest.setStatementID(UPDATE_EMAIL_SECTIONS);
          dataRequest.setInputParams(sectionMap);

          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

          Map outputs = (Map) dataAccessUtil.execute(dataRequest);

          status = (String) outputs.get(STATUS_PARAM);
          logger.debug("Status is:" + status);

          if(status.equals("N"))
          {
              message = (String) outputs.get(MESSAGE_PARAM);
              logger.debug("Message is:" + message);
              throw new Exception(message);
          }
          updateEmailSectionContent(emailSectionContent);
      }
      catch(Exception ex)
      {
          logger.error(ex);
          throw(ex);
      }
      finally
      {
          con.close();
      }
  }

  /**
   * Update a complete section record in EMAIL_SECTION_CONTENT table. Uses the same
   * connection as updateEmailSection.
   *
   * @param EmailSectionContentVO
   * @throws Exception
   */
  public void updateEmailSectionContent(EmailSectionContentVO emailSectionContent) throws Exception
  {
      //since size of content text can vary up or down in size, a delete then reinsert is needed.

      try
      {
          //Delete section content first
          deleteEmailSectionContent(emailSectionContent.getSectionId());

          //insert new updated section
          insertEmailSectionContent(emailSectionContent);
      }
      catch(Exception ex)
      {
          logger.error(ex);
          throw(ex);
      }

  }

  /**
   * Retrieve listing of sections by section type and wildcard search from EMAIL_SECTIONS table
   *
   * @param String section type
   * @param String search string
   * @throws Exception
   * @return Document
   */
  public Document searchSectionOption(String sectionType,String searchString) throws Exception
  {
      Document doc = null;
      dataRequest = DataRequestHelper.getInstance().getDataRequest();

      try {
          dataRequest.reset();

          dataRequest.setStatementID(SEARCH_SECTION_BY_TYPE);
          dataRequest.addInputParam("IN_SECTION_TYPE", sectionType);
          dataRequest.addInputParam("IN_SECTION_NAME", searchString == null? "" : searchString);

          logger.debug("Setting IN_SECTION_TYPE to:" + sectionType);
          logger.debug("Setting IN_SECTION_NAME to:" + searchString);
          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

          doc = (Document)dataAccessUtil.execute(dataRequest);
          return doc;
      }
      catch(Exception ex)
      {
          logger.error(ex);
          throw(ex);
      }
      finally
      {
         dataRequest.getConnection().close();
      }
  }

  /**
   * Retrieve listing of sections by section type from EMAIL_SECTIONS table
   *
   * @param String section type
   * @throws Exception
   * @return Document
   */
  public Document getSectionsByType(String sectionType) throws Exception
  {
      Document doc = null;
      dataRequest = DataRequestHelper.getInstance().getDataRequest();

      try {
          dataRequest.reset();

          dataRequest.setStatementID(VIEW_SECTION_BY_TYPE);
          dataRequest.addInputParam("IN_SECTION_TYPE", sectionType);
          logger.debug("Setting IN_SECTION_TYPE to:" + sectionType);

          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

          doc = (Document)dataAccessUtil.execute(dataRequest);
          return doc;
      }
      catch(Exception ex)
      {
          logger.error(ex);
          throw(ex);
      }
      finally
      {
          dataRequest.getConnection().close();
      }
  }

  /**
   * Retrieve listing of programs by source code from PROGRAM_MAPPING and PROGRAM_TO_SOURCE_MAPPING tables
   *
   * @param String source code
   * @throws Exception
   * @return Document
   */
  public Document searchProgramBySource(String sourceCode) throws Exception
  {
      Document doc = null;
      dataRequest = DataRequestHelper.getInstance().getDataRequest();

      try {
          dataRequest.reset();

          dataRequest.setStatementID(SEARCH_PROGRAM_BY_SOURCE);
          dataRequest.addInputParam("IN_SOURCE", sourceCode);
          logger.debug("Setting IN_SOURCE to:" + sourceCode);

          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

          doc = (Document)dataAccessUtil.execute(dataRequest);
          return doc;
      }
      catch(Exception ex)
      {
          logger.error(ex);
          throw(ex);
      }
      finally
      {
          dataRequest.getConnection().close();
      }
  }

  /**
   * Retrieve program by program name from PROGRAM_MAPPING table
   *
   * @param String program name
   * @throws Exception
   * @return Document
   */
  public Document searchProgramByName(String programName) throws Exception
  {
      Document doc = null;
      dataRequest = DataRequestHelper.getInstance().getDataRequest();

      try {
          dataRequest.reset();

          dataRequest.setStatementID(SEARCH_PROGRAM_BY_NAME);
          dataRequest.addInputParam("IN_PROGRAM_NAME", programName == null? "" : programName.toUpperCase());
          logger.debug("Setting IN_PROGRAM_NAME to:" + programName);
          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

          doc = (Document)dataAccessUtil.execute(dataRequest);
          return doc;
      }
      catch(Exception ex)
      {
          logger.error(ex);
          throw(ex);
      }
      finally
      {
          dataRequest.getConnection().close();
      }
  }

  /**
   * Retrieve source codes by custom flag from PROGRAM_TO_SOURCE_MAPPING table
   *
   * @param String custom flag
   * @throws Exception
   * @return Document
   */
  public Document getSources(String sourceFlag, String programId) throws Exception
  {
      Document doc = null;
      dataRequest = DataRequestHelper.getInstance().getDataRequest();


      try {
          dataRequest.reset();

          dataRequest.setStatementID(VIEW_SOURCES);
          dataRequest.addInputParam("IN_ALL_FLAG", sourceFlag);
          dataRequest.addInputParam("IN_PROGRAM_ID", programId);
          logger.debug("Setting in_all_flag to:" + sourceFlag);
          logger.debug("Setting in_program_id to:" + programId);
          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

          doc = (Document)dataAccessUtil.execute(dataRequest);
          return doc;
      }
      catch(Exception ex)
      {
          logger.error(ex);
          throw(ex);
      }
      finally
      {
          dataRequest.getConnection().close();
      }
  }

  /**
   * Retrieve complete sections by section type and section name from EMAIL_SECTIONS and EMAIL_SECTION_CONTENT table
   *
   * @param String section type
   * @param String section name
   * @throws Exception
   * @return Document
   */
  public Document getSectionByName(String sectionType,String sectionName) throws Exception
  {
      Document section = null;
      dataRequest = DataRequestHelper.getInstance().getDataRequest();

      try {
          dataRequest.reset();

          dataRequest.setStatementID(VIEW_SECTION_BY_NAME);
          dataRequest.addInputParam("IN_SECTION_TYPE", sectionType);
          dataRequest.addInputParam("IN_SECTION_NAME", sectionName);
          logger.debug("Setting IN_SECTION_TYPE to:" + sectionType);
          logger.debug("Setting IN_SECTION_NAME to:" + sectionName);
          Map sectionMap = null;

          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
          sectionMap = (Map)dataAccessUtil.execute(dataRequest);

          Collection emailSections = new ArrayList();

          Element tagElement = null;

          section = DOMUtil.getDefaultDocument();
          Element sectionElement = section.createElement("SECTION");
          section.appendChild(sectionElement);

          if(!sectionMap.isEmpty())
          {
                Collection allSectionContents = getSectionContents(sectionMap);

                CachedResultSet rs = new CachedResultSet();
                rs = (CachedResultSet) sectionMap.get(EMAIL_SECTIONS_CURSOR);

                boolean firstRow = true;

                while(rs != null && rs.next())
                {
                    long sectionId = new Long(rs.getObject(1).toString()).longValue();
                    String contentType = rs.getObject(2).toString();

                    if(firstRow)
                    {
                      sectionElement.appendChild(this.createTextNode("section_id", (String) rs.getObject(1).toString(), section, tagElement));
                      sectionElement.appendChild(this.createTextNode("start_date", (String) rs.getObject(3), section, tagElement));
                      sectionElement.appendChild(this.createTextNode("end_date", (String) rs.getObject(4), section, tagElement));
                      sectionElement.appendChild(this.createTextNode("section_default", (String) rs.getObject(5), section, tagElement));
                      sectionElement.appendChild(this.createTextNode("section_type", sectionType, section, tagElement));
                      sectionElement.appendChild(this.createTextNode("description", (String) rs.getObject(7), section, tagElement));
                      sectionElement.appendChild(this.createTextNode("created_on", (String) rs.getObject(8), section, tagElement));
                      sectionElement.appendChild(this.createTextNode("updated_on", (String) rs.getObject(9), section, tagElement));
                      sectionElement.appendChild(this.createTextNode("updated_by", (String) rs.getObject(10), section, tagElement));
                      sectionElement.appendChild(this.createTextNode("section_name", (String) rs.getObject(11), section, tagElement));

                      firstRow = false;
                    }

                    //get section content
                    Collection sectionContents = new ArrayList();
                    Iterator iterator = allSectionContents.iterator();

                    EmailSectionContentVO sectionContent = null;
                    String[] sectionContentBuffer = new String[1];

                    for (int i = 0; iterator.hasNext(); i++)
                    {
                       sectionContent = new EmailSectionContentVO();
                       sectionContent = (EmailSectionContentVO)iterator.next();

                       if(sectionContent.getSectionId() == sectionId)
                       {
                            sectionContentBuffer = sectionContent.getContent();
                       }
                    }

                    if(contentType.equalsIgnoreCase("text"))
                    {
                        Element sectionTextElement = section.createElement("SECTION_CONTENT_TEXT");
                        sectionElement.appendChild(sectionTextElement);

                        sectionTextElement.appendChild(this.createTextNode("section_id", new Long(sectionId).toString(), section, tagElement));

                        if(sectionType.equalsIgnoreCase("Subject"))
                        {
                             String seperator = "~";
                             String fromAddress = "";
                             if(sectionContentBuffer[0] == null) {
                                throw new Exception("Data error: Invalid Subject.");
                             }
                             String temp = sectionContentBuffer[0].toString();
                             int seperatorPosition = temp.indexOf(seperator);
                             fromAddress = temp.substring(0,seperatorPosition);

                             sectionTextElement.appendChild(this.createTextNode("from_address", fromAddress, section, tagElement));
                             sectionTextElement.appendChild(this.createTextNode("text_version", temp.substring(seperatorPosition + 1), section, tagElement));
                        }
                        else
                        {
                          if (sectionContentBuffer != null && sectionContentBuffer.length > 0 && sectionContentBuffer[0] != null) {
                            sectionTextElement.appendChild(this.createTextNode("text_version", sectionContentBuffer[0].toString(), section, tagElement));
                          }
                        }
                    }
                    else
                    {
                        //html version
                        Element sectionHtmlElement = section.createElement("SECTION_CONTENT_HTML");
                        sectionElement.appendChild(sectionHtmlElement);

                        sectionHtmlElement.appendChild(this.createTextNode("section_id", new Long(sectionId).toString(), section, tagElement));
                        if (sectionContentBuffer != null && sectionContentBuffer.length > 0 && sectionContentBuffer[0] != null) {
                            sectionHtmlElement.appendChild(this.createTextNode("html_version", sectionContentBuffer[0].toString(), section, tagElement));
                        }
                    }
                }
          }
          return section;
      }
      catch(Exception ex)
      {
          logger.error(ex);
          throw(ex);
      }
      finally
      {
          dataRequest.getConnection().close();
      }
  }

  /**
   * Retrieve email section name by section id from EMAIL_SECTIONS table
   *
   * @param int section id
   * @throws Exception
   * @return String section name
   */
  public String getSectionNameById(long sectionId) throws Exception
  {
      Map outputMap = null;
      String sectionName = "";
      dataRequest = DataRequestHelper.getInstance().getDataRequest();

      try {
          dataRequest.reset();

          dataRequest.setStatementID(VIEW_SECTION);
          dataRequest.addInputParam("IN_SECTION_ID", new Long(sectionId));
          logger.debug("Setting IN_SECTION_ID to:" + sectionId);
          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
          outputMap = (Map)dataAccessUtil.execute(dataRequest);

          if(!outputMap.isEmpty())
          {
                CachedResultSet rs = (CachedResultSet) outputMap.get(EMAIL_SECTIONS_CURSOR);

                if(rs != null && rs.next())
                {
                    sectionName = (String)rs.getObject(11);
                } else {
                    throw new Exception("Section result set is empty.");
                }
          } else {
              throw new Exception("VIEW_SECTION by id returned null.");
          }

          return sectionName;
      }
      catch(Exception ex)
      {
          logger.error(ex);
          throw(ex);
      }
      finally
      {
          dataRequest.getConnection().close();
      }
  }

  /**
   * Retrieve section content by concatenating the chunks of data returned as result set
   * in the map.
   * @param SectionMap
   * @return Collection
   * @throws Exception
   */
  private Collection getSectionContents(Map sectionMap) throws Exception
  {
        Collection sectionContents  = new ArrayList();

        try {

            EmailSectionContentVO sectionContent = null;

            CachedResultSet rs = new CachedResultSet();
            rs = (CachedResultSet) sectionMap.get(EMAIL_SECTION_CONTENT_CURSOR);

            long sectionId = 0;
            long holdsectionId = 0;
            boolean firstTime = true;
            StringBuffer content = new StringBuffer();

            int rowCount = rs.getRowCount();
            int count = 0;
            String hold[] = null;
            while(rs != null && rs.next())
            {
                count++;
                sectionId = new Long(rs.getObject(1).toString()).longValue();
                if(firstTime)
                {
                    holdsectionId = sectionId;
                    sectionContent = new EmailSectionContentVO();
                    sectionContent.setSectionId(sectionId);
                    content.append((String)rs.getObject(3));
                    firstTime = false;

                    //only 1 row for 1st content type or subject
                    if(rowCount == 1 || rowCount == 2)
                    {
                        hold = new String[1];
                        hold[0] = content.toString();
                        sectionContent.setContent(hold);
                        sectionContents.add(sectionContent);

                        sectionContent = new EmailSectionContentVO();
                        content = new StringBuffer();
                    }
                }
                else if (holdsectionId == sectionId)
                {
                    //multiple records for a section
                    content.append((String)rs.getObject(3));

                    //last record
                    if(count == rowCount)
                    {
                        hold = new String[1];
                        hold[0] = content.toString();
                        sectionContent.setSectionId(sectionId);
                        sectionContent.setContent(hold);
                        sectionContents.add(sectionContent);
                    }
                }
                else
                {
                  hold = new String[1];

                  if(rowCount == 2)
                  {
                      content.append((String)rs.getObject(3));
                      hold[0] = content.toString();
                  }
                  else
                  {
                      hold[0] = content.toString();
                  }

                  sectionContent.setContent(hold);
                  sectionContents.add(sectionContent);

                  //new content id
                  holdsectionId = sectionId;
                  sectionContent = new EmailSectionContentVO();
                  sectionContent.setSectionId(sectionId);
                  content = new StringBuffer();
                  content.append((String)rs.getObject(3));
                  if(count == rowCount)
                  {
                      hold = new String[1];
                      hold[0] = content.toString();
                      sectionContent.setContent(hold);
                      sectionContents.add(sectionContent);
                  }
                }
            }
        }
        catch(Exception ex)
        {
            logger.error(ex);
            throw(ex);
        }

        return sectionContents;
  }

  /**
   * Retrieve complete sections by section type and section name from EMAIL_SECTIONS and EMAIL_SECTION_CONTENT table
   *
   * @param String section type
   * @param String section name
   * @throws Exception
   * @return Document
   */
  public Document getSectionContentByName(String sectionType,String sectionName) throws Exception
  {
      Document section = null;
      dataRequest = DataRequestHelper.getInstance().getDataRequest();

      try {
          dataRequest.reset();

          dataRequest.setStatementID(VIEW_SECTION_BY_NAME);
          dataRequest.addInputParam("IN_SECTION_TYPE", sectionType);
          dataRequest.addInputParam("IN_SECTION_NAME", sectionName);
          logger.debug("Setting IN_SECTION_TYPE to:" + sectionType);
          logger.debug("Setting IN_SECTION_NAME to:" + sectionName);
          Map sectionMap = null;

          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
          sectionMap = (Map)dataAccessUtil.execute(dataRequest);

          Element tagElement = null;

          section = DOMUtil.getDefaultDocument();
          Element sectionElement = section.createElement("SECTION");
          section.appendChild(sectionElement);

          if(!sectionMap.isEmpty())
          {
                Collection allSectionContents = getSectionContents(sectionMap);

                CachedResultSet rs = new CachedResultSet();
                rs = (CachedResultSet) sectionMap.get(EMAIL_SECTIONS_CURSOR);

                boolean firstRow = true;

                while(rs != null && rs.next())
                {
                    long sectionId = new Long(rs.getObject(1).toString()).longValue();
                    String contentType = rs.getObject(2).toString();

                    //get section content
                    Collection sectionContents = new ArrayList();
                    Iterator iterator = allSectionContents.iterator();

                    EmailSectionContentVO sectionContent = null;
                    StringBuffer sectionContentBuffer = new StringBuffer();

                    for (int i = 0; iterator.hasNext(); i++)
                    {
                       sectionContent = new EmailSectionContentVO();
                       sectionContent = (EmailSectionContentVO)iterator.next();

                       if(sectionContent.getSectionId() == sectionId)
                       {
                            String temp[] = null;
                            temp = sectionContent.getContent();

                            sectionContentBuffer.append(temp[0]);
                       }
                    }

                    if(contentType.equalsIgnoreCase("text"))
                    {
                        Element sectionTextElement = section.createElement("SECTION_CONTENT_TEXT");
                        sectionElement.appendChild(sectionTextElement);

                        sectionTextElement.appendChild(this.createTextNode("section_id", new Long(sectionId).toString(), section, tagElement));

                        if(sectionType.equalsIgnoreCase("Subject"))
                        {
                             String seperator = "~";
                             String fromAddress = "";
                             String temp = sectionContentBuffer.toString();
                             int seperatorPosition = temp.indexOf(seperator);
                             if (seperatorPosition == -1) {
                                throw new Exception("Invalid subject. Please ask your administrator to fix the data.");
                             }
                             fromAddress = temp.substring(0,seperatorPosition);
                             sectionTextElement.appendChild(this.createTextNode("from_address", fromAddress, section, tagElement));
                             sectionTextElement.appendChild(this.createTextNode("text_version", temp.substring(seperatorPosition + 1), section, tagElement));
                        }
                        else
                        {
                            sectionTextElement.appendChild(this.createTextNode("text_version", sectionContentBuffer.toString(), section, tagElement));
                        }
                    }
                    else
                    {
                        //html version
                        Element sectionHtmlElement = section.createElement("SECTION_CONTENT_HTML");
                        sectionElement.appendChild(sectionHtmlElement);

                        sectionHtmlElement.appendChild(this.createTextNode("section_id", new Long(sectionId).toString(), section, tagElement));
                        sectionHtmlElement.appendChild(this.createTextNode("html_version", sectionContentBuffer.toString(), section, tagElement));
                    }

                }
          }
          return section;
      }
      catch(Exception ex)
      {
          logger.error(ex);
          throw(ex);
      }
      finally
      {
          dataRequest.getConnection().close();
      }
  }

  /**
   * Delete an email section record by section id from EMAIL_SECTIONS table
   *
   * @param int section id
   * @throws Exception
   */
  public void deleteEmailSection(long sectionId) throws Exception
  {
      dataRequest = DataRequestHelper.getInstance().getDataRequest();

      try {
          dataRequest.reset();

          HashMap sectionMap = new HashMap();
          sectionMap = new HashMap();

          sectionMap.put("IN_SECTION_ID",new Long(sectionId));
          sectionMap.put(STATUS_PARAM,"");
          sectionMap.put(MESSAGE_PARAM,"");
          logger.debug("Setting IN_SECTION_ID to:" + sectionId);

          dataRequest.setStatementID(DELETE_EMAIL_SECTION);
          dataRequest.setInputParams(sectionMap);

          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

          Map outputs = (Map) dataAccessUtil.execute(dataRequest);

          status = (String) outputs.get(STATUS_PARAM);
          logger.debug("Status is:" + status);

          if(status.equals("N"))
          {
              message = (String) outputs.get(MESSAGE_PARAM);
              logger.debug("Message is:" + message);
              throw new Exception(message);
          }

      }
      catch(Exception ex)
      {
          logger.error(ex);
          throw(ex);
      }
      finally
      {
          dataRequest.getConnection().close();
      }
  }

  /**
   * Retrieve email program from PROGRAM_MAPPING table
   *
   * @param EmailProgramVO
   * @throws Exception
   * @return int program id
   */
  public long insertEmailProgram(EmailProgramVO emailProgram) throws Exception
  {
      dataRequest = DataRequestHelper.getInstance().getDataRequest();

      try
      {
          dataRequest.reset();

          HashMap programMap = new HashMap();

          programMap.put("IN_UPDATED_BY",emailProgram.getUpdatedBy());
          programMap.put("IN_PROGRAM_NAME",emailProgram.getProgramName());
          programMap.put("IN_DESCRIPTION",emailProgram.getProgramDescription());
          programMap.put("IN_START_DATE",getSQLDate(emailProgram.getEffectiveStartDate()));
          programMap.put("IN_END_DATE",getSQLDate(emailProgram.getEffectiveEndDate()));
          programMap.put("IN_DEFAULT_SECTION",emailProgram.getProgramDefault());
          programMap.put("RegisterOutParameterProgramId",new Long(0));
          programMap.put(STATUS_PARAM,"");
          programMap.put(MESSAGE_PARAM,"");
          logger.debug("Setting IN_UPDATED_BY to:" + emailProgram.getUpdatedBy());
          logger.debug("Setting IN_PROGRAM_NAME to:" + emailProgram.getProgramName());
          logger.debug("Setting IN_DESCRIPTION to:" + emailProgram.getProgramDescription());
          logger.debug("Setting IN_START_DATE to:" + emailProgram.getEffectiveEndDate());
          logger.debug("Setting IN_END_DATE to:" + emailProgram.getUpdatedBy());
          logger.debug("Setting IN_DEFAULT_SECTION to:" + emailProgram.getProgramDefault());

          dataRequest.setStatementID(INSERT_PROGRAM);
          dataRequest.setInputParams(programMap);

          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

          long programId = 0;

          Map outputs = (Map) dataAccessUtil.execute(dataRequest);

          status = (String) outputs.get(STATUS_PARAM);
          logger.debug("Status is:" + status);

          if(status.equals("N"))
          {
              message = (String) outputs.get(MESSAGE_PARAM);
              logger.debug("Message is:" + message);
              throw new Exception(message);
          }
          else
          {
              programId = Long.parseLong(outputs.get("RegisterOutParameterProgramId").toString());
              emailProgram.setProgramId(programId);
          }

          insertProgramSections(dataRequest,emailProgram);
          return emailProgram.getProgramId();
      }
      catch(Exception ex)
      {
          logger.error(ex);
          throw(ex);
      }
      finally
      {
          dataRequest.getConnection().close();
      }
  }

  /**
   * Delete email section content from EMAIL_SECTION_CONTENT table.
   */
  private void deleteEmailSectionContent(long sectionId) throws Exception
  {
      //dataRequest = DataRequestHelper.getInstance().getDataRequest();

      try
      {
        dataRequest.reset();

        HashMap sectionMap = new HashMap();
        sectionMap = new HashMap();

        sectionMap.put("IN_SECTION_ID",new Long(sectionId));
        sectionMap.put(STATUS_PARAM,"");
        sectionMap.put(MESSAGE_PARAM,"");
        logger.debug("Setting IN_SECTION_ID to:" + sectionId);

        dataRequest.setStatementID(DELETE_EMAIL_SECTION_CONTENT);
        dataRequest.setInputParams(sectionMap);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        Map outputs = (Map) dataAccessUtil.execute(dataRequest);

        status = (String) outputs.get(STATUS_PARAM);
        logger.debug("Status is:" + status);

        if(status.equals("N"))
        {
            message = (String) outputs.get(MESSAGE_PARAM);
            logger.debug("Message is:" + message);

            throw new Exception(message);
        }

      }
      catch(Exception ex)
      {
          logger.error(ex);
          throw(ex);
      }
  }

  /**
   * Updates source codes related to a program. EmailProgramVO object has
   * a String array of source codes that need to be associated to the program.
   * The procedure is used when a new program is created, a custom program is
   * update to be a custom program, or a default program is updated. If the program
   * is custom, first reset source codes related to the program and then update
   * the new selected source codes. Calls UPDATE_DEFAULT_PROGRAM_MAPPING or
   * UPDATE_CUSTOM_PROGRAM_MAPPING depending on if the program is custom or default.
   *
   * @param EmailProgramVO the program whose source codes need to be updated.
   * @throw Exception
   */
  public void updateProgramSourceCodes(EmailProgramVO program) throws Exception
  {
      dataRequest = DataRequestHelper.getInstance().getDataRequest();
      try
      {
        dataRequest.reset();
        HashMap programMap = new HashMap();

        programMap.put("IN_PROGRAM_ID",new Long(program.getProgramId()));
        programMap.put(STATUS_PARAM,"");
        programMap.put(MESSAGE_PARAM,"");
        logger.debug("Setting IN_PROGRAM_ID to:" + program.getProgramId());
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = null;

        if ("N".equals(program.getProgramDefault()))
        {
            // reset_custom_program_source and update_custom_program_source if custom program.
            dataRequest.setStatementID(RESET_CUSTOM_PROGRAM_SOURCE);
            dataRequest.setInputParams(programMap);

            outputs = (Map) dataAccessUtil.execute(dataRequest);

            //ignore status coming back for existing source codes
            status = (String) outputs.get(STATUS_PARAM);
            logger.debug("Status is:" + status);

            if(status.equals("N"))
            {
                  message = (String) outputs.get(MESSAGE_PARAM);
                  logger.debug("Message is:" + message);
                  throw new Exception(message);
            }
        }
        //set selected source codes to program
        String sourceCodes[] = program.getProgramSourceCodes();
        String statementId = UPDATE_CUSTOM_PROGRAM_SOURCE;
        if ("Y".equals(program.getProgramDefault())) {
            statementId = UPDATE_DEFAULT_PROGRAM_SOURCE;
        }

        for(int i = 0; i < sourceCodes.length; i++)
        {
            programMap = new HashMap();

            programMap.put("IN_SOURCE_CODE",sourceCodes[i]);
            programMap.put("IN_PROGRAM_ID",new Long(program.getProgramId()));
            programMap.put(STATUS_PARAM,"");
            programMap.put(MESSAGE_PARAM,"");
            logger.debug("Setting IN_SOURCE_CODE to:" + sourceCodes[i]);
            logger.debug("Setting IN_PROGRAM_ID to:" + program.getProgramId());

            dataRequest.reset();
            dataRequest.setStatementID(statementId);
            dataRequest.setInputParams(programMap);

            dataAccessUtil = DataAccessUtil.getInstance();

            outputs = (Map) dataAccessUtil.execute(dataRequest);

            //ignore status coming back for existing source codes
            status = (String) outputs.get(STATUS_PARAM);
            logger.debug("Status is:" + status);

            if(status.equals("N"))
            {
                message = (String) outputs.get(MESSAGE_PARAM);
                logger.debug("Message is:" + message);
                throw new Exception(message);
            }

        }
      }
      catch(Exception ex)
      {
          logger.error(ex);
          throw(ex);
      }
      finally
      {
          dataRequest.getConnection().close();
      }
  }

  /**
   * Inserts program sections. This procedure is used when create a new program
   *
   * @param DataRequest
   * @param EmailProgramVO
   * @throws Exception
   */
  private void insertProgramSections(DataRequest dataRequest,EmailProgramVO program) throws Exception
  {
      try
      {
        dataRequest.reset();
        HashMap programMap = new HashMap();

        String sections[] = program.getSections();

        for(int i = 0; i < sections.length; i++)
        {
            programMap = new HashMap();

            programMap.put("IN_PROGRAM_ID",new Long(program.getProgramId()));
            programMap.put("IN_SECTION_ID",new Long(sections[i]));
            programMap.put(STATUS_PARAM,"");
            programMap.put(MESSAGE_PARAM,"");
            logger.debug("Setting IN_PROGRAM_ID to:" + program.getProgramId());
            logger.debug("Setting IN_SECTION_ID to:" + sections[i]);

            dataRequest.reset();
            dataRequest.setStatementID(INSERT_PROGRAM_SECTION_MAPPING);
            dataRequest.setInputParams(programMap);

            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

            Map outputs = (Map) dataAccessUtil.execute(dataRequest);

            status = (String) outputs.get(STATUS_PARAM);
            logger.debug("Status is:" + status);

            if(status.equals("N"))
            {
                message = (String) outputs.get(MESSAGE_PARAM);
                logger.debug("Message is:" + message);
                throw new Exception(message);
            }
          }
      }
      catch(Exception ex)
      {
          logger.error(ex);
          throw(ex);
      }
      finally
      {
          dataRequest.getConnection().close();
      }
  }

  /**
   * Inserts program sections. This procedure is used when update a program. A program
   * can have multiple sections of the same type as long as their start dates are
   * different. To ensure the success of insert, the procedure first delete the program
   * section mapping and then inserts the mapping. This is because whether the section
   * of a particular type has been updated is transparent to the procedure. If such
   * reset is not done first, the insert will fail if the section of that type is not
   * being updated. This way no checking is necessary to only update the sections
   * that have changed. Just update all sections.
   *
   * @param DataRequest
   * @param EmailProgramVO
   * @throws Exception
   */
  private void insertProgramSectionsForUpdate(DataRequest dataRequest,EmailProgramVO program) throws Exception
  {
      try {
      dataRequest.reset();

      HashMap programMap = new HashMap();

      String sections[] = program.getSections();

      for(int i = 0; i < sections.length; i++)
      {
          //delete section mapping 1st then reinsert
          programMap = new HashMap();

          programMap.put("IN_PROGRAM_ID",new Long(program.getProgramId()));
          programMap.put("IN_SECTION_ID",new Long(sections[i]));
          programMap.put(STATUS_PARAM,"");
          programMap.put(MESSAGE_PARAM,"");

          logger.debug("Setting IN_PROGRAM_ID to:" + program.getProgramId());
          logger.debug("Setting IN_SECTION_ID to:" + sections[i]);

          dataRequest.reset();
          dataRequest.setStatementID(DELETE_PROGRAM_SECTION_MAPPING);
          dataRequest.setInputParams(programMap);

          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

          Map outputs = (Map) dataAccessUtil.execute(dataRequest);

          programMap = new HashMap();

          programMap.put("IN_PROGRAM_ID",new Long(program.getProgramId()));
          programMap.put("IN_SECTION_ID",new Long(sections[i]));
          programMap.put(STATUS_PARAM,"");
          programMap.put(MESSAGE_PARAM,"");
          logger.debug("Setting IN_PROGRAM_ID to:" + program.getProgramId());
          logger.debug("Setting IN_SECTION_ID to:" + sections[i]);

          dataRequest.reset();
          dataRequest.setStatementID(INSERT_PROGRAM_SECTION_MAPPING);
          dataRequest.setInputParams(programMap);

          dataAccessUtil = DataAccessUtil.getInstance();
          outputs = (Map) dataAccessUtil.execute(dataRequest);

          status = (String) outputs.get(STATUS_PARAM);
          logger.debug("Status is:" + status);

          if(status.equals("N"))
          {
              message = (String) outputs.get(MESSAGE_PARAM);
              logger.debug("Message is:" + message);
              throw new Exception(message);
          }
        }
      }
      catch(Exception ex)
      {
          logger.error(ex);
          throw(ex);
      }
  }

  /**
   * Retrieve program from PROGRAM_MAPPING table
   *
   * @param int program id
   * @throws Exception
   * @return EmailProgramVO
   */
  public EmailProgramVO getEmailProgram(long programId) throws Exception
  {
      EmailProgramVO emailProgram = new EmailProgramVO();
      dataRequest = DataRequestHelper.getInstance().getDataRequest();

      try{

        dataRequest.reset();

        dataRequest.setStatementID(VIEW_PROGRAM);
        dataRequest.addInputParam("IN_PROGRAM_ID", new Long(programId));
        logger.debug("Setting IN_PROGRAM_ID to:" + programId);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        CachedResultSet rs = new CachedResultSet();
        rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        while(rs != null && rs.next())
        {
            emailProgram.setProgramId(programId);
            emailProgram.setProgramDescription((String)rs.getObject(2));
            emailProgram.setEffectiveStartDate(rs.getObject(3).toString());

            Object effectiveEndDate = rs.getObject(4);
            if(effectiveEndDate != null)
                emailProgram.setEffectiveEndDate(effectiveEndDate.toString());
            emailProgram.setProgramDefault((String)rs.getObject(5));
            emailProgram.setCreatedOn((String)rs.getObject(6).toString());
            emailProgram.setUpdatedOn((String)rs.getObject(7).toString());
            emailProgram.setUpdatedBy((String)rs.getObject(8));
            emailProgram.setProgramName((String)rs.getObject(9));
        }
        return emailProgram;
      }
      catch(Exception ex)
      {
          logger.error(ex);
          throw(ex);
      }
      finally
      {
          dataRequest.getConnection().close();
      }
  }

  /**
   * Retrieve current program sections by program id and date
   *
   * @param int program id
   * @param String program date
   * @throws Exception
   * @return Collection
   */
  public Collection getCurrentProgramSections(long programId,String programDate) throws Exception
  {
      Collection programSections  = new ArrayList();
      dataRequest = DataRequestHelper.getInstance().getDataRequest();

      try {
        EmailSectionsVO programSection = null;

        EmailSectionsVO emailSections = new EmailSectionsVO();

        dataRequest.reset();
        dataRequest.setStatementID(VIEW_CURRENT_SECTION_MAPPING);
        dataRequest.addInputParam("IN_PROGRAM_ID", new Long(programId));
        dataRequest.addInputParam("IN_DATE",getSQLDate(programDate));
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        logger.debug("Setting IN_PROGRAM_ID to:" + programId);
        logger.debug("Setting IN_DATE to:" + programDate);

        CachedResultSet rs = new CachedResultSet();
        rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        while(rs != null && rs.next())
        {
            programSection = new EmailSectionsVO();

            programSection.setProgramId(programId);
            programSection.setSectionType((String)rs.getObject(1));
            programSection.setSectionName((String)rs.getObject(2));

            programSections.add(programSection);
        }
        return programSections;
      }
      catch(Exception ex)
      {
          logger.error(ex);
          throw(ex);
      }
      finally
      {
        dataRequest.getConnection().close();
      }
  }

  /**
   * Retrieve current program source codes by program
   *
   * @param int program id
   * @throws Exception
   * @return String
   */
  public String getCurrentProgramSources(long programId) throws Exception
  {
      StringBuffer sources = new StringBuffer();
      dataRequest = DataRequestHelper.getInstance().getDataRequest();

      try{
        dataRequest.reset();

        dataRequest.setStatementID(VIEW_SOURCES_BY_PROGRAM_ID);
        dataRequest.addInputParam("IN_PROGRAM_ID", new Long(programId));
        logger.debug("Setting IN_PROGRAM_ID to:" + programId);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        int recordCount = rs.getRowCount();
        int count = 0;
        while(rs != null && rs.next())
        {
            count++;
            if(count == recordCount)
            {
                sources.append((String)rs.getObject(1));
            }
            else
            {
                sources.append((String)rs.getObject(1) + "~");
            }
        }
        return sources.toString();
      }
      catch(Exception ex)
      {
          logger.error(ex);
          throw(ex);
      }
      finally
      {
          dataRequest.getConnection().close();
      }
  }

  /**
   * Update program record in table PROGRAM_MAPPING
   *
   * @param EmailProgramVO
   * @throws Exception
   */
  public void updateEmailProgram(EmailProgramVO emailProgram) throws Exception
  {
      dataRequest = DataRequestHelper.getInstance().getDataRequest();
      boolean returnStatus = true;
      try
      {
          dataRequest.reset();

          HashMap programMap = new HashMap();

          programMap.put("IN_UPDATED_BY",emailProgram.getUpdatedBy());
          programMap.put("IN_PROGRAM_ID",new Long(emailProgram.getProgramId()));
          programMap.put("IN_PROGRAM_NAME",emailProgram.getProgramName());
          programMap.put("IN_DESCRIPTION",emailProgram.getProgramDescription());
          programMap.put("IN_START_DATE",getSQLDate(emailProgram.getEffectiveStartDate()));
          programMap.put("IN_END_DATE",getSQLDate(emailProgram.getEffectiveEndDate()));
          programMap.put("IN_DEFAULT_PROGRAM",emailProgram.getProgramDefault());
          programMap.put(STATUS_PARAM,"");
          programMap.put(MESSAGE_PARAM,"");
          logger.debug("Setting IN_UPDATED_BY to:" + emailProgram.getUpdatedBy());
          logger.debug("Setting IN_PROGRAM_ID to:" + emailProgram.getProgramId());
          logger.debug("Setting IN_PROGRAM_NAME to:" + emailProgram.getProgramName());
          logger.debug("Setting IN_DESCRIPTION to:" + emailProgram.getProgramDescription());
          logger.debug("Setting IN_START_DATE to:" + emailProgram.getEffectiveStartDate());
          logger.debug("Setting IN_END_DATE to:" + emailProgram.getEffectiveEndDate());
          logger.debug("Setting IN_DEFAULT_PROGRAM to:" + emailProgram.getProgramDefault());
          dataRequest.setStatementID(UPDATE_PROGRAM_MAPPING);
          dataRequest.setInputParams(programMap);

          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

          Map outputs = (Map) dataAccessUtil.execute(dataRequest);

          status = (String) outputs.get(STATUS_PARAM);
          logger.debug("Status is:" + status);

          if(status.equals("N"))
          {
              message = (String) outputs.get(MESSAGE_PARAM);
              logger.debug("Message is:" + message);
              throw new Exception(message);
          }

          insertProgramSectionsForUpdate(dataRequest,emailProgram);
      }
      catch(Exception ex)
      {
          logger.error(ex);
          throw(ex);
      }
      finally
      {
          dataRequest.getConnection().close();
      }
  }

  /**
   * Delete program record from table PROGRAM_MAPPING
   *
   * @param int program id
   * @throws Exception
   */
  public void deleteEmailProgram(long programId) throws Exception
  {
      dataRequest = DataRequestHelper.getInstance().getDataRequest();

      try
      {
          dataRequest.reset();

          HashMap programMap = new HashMap();

          programMap.put("IN_PROGRAM_ID",new Long(programId));
          programMap.put(STATUS_PARAM,"");
          programMap.put(MESSAGE_PARAM,"");
          logger.debug("Setting IN_PROGRAM_ID to:" + programId);

          dataRequest.setStatementID(DELETE_PROGRAM_MAPPING);
          dataRequest.setInputParams(programMap);

          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

          Map outputs = (Map) dataAccessUtil.execute(dataRequest);

          status = (String) outputs.get(STATUS_PARAM);
          logger.debug("Status is:" + status);
          if(status.equals("N"))
          {
              message = (String) outputs.get(MESSAGE_PARAM);
              logger.debug("Message is:" + message);
              throw new Exception(message);
          }
      }
      catch(Exception ex)
      {
          logger.error(ex);
          throw(ex);
      }
      finally
      {
          dataRequest.getConnection().close();
      }
  }

  /**
   * Retrieve related sections for a given program
   *
   * @param  int program id
   * @throws Exception
   * @return Document
   */
  public Document getRelatedProgramSections(long programId) throws Exception
  {
      Document doc = null;
      dataRequest = DataRequestHelper.getInstance().getDataRequest();

      try{
      String sectionName = null;

      // Search
      dataRequest.reset();

      dataRequest.setStatementID(VIEW_SECTIONS);
      dataRequest.addInputParam("IN_PROGRAM_ID", new Long(programId));
      dataRequest.addInputParam("IN_SECTION_NAME",sectionName);
      dataRequest.addInputParam("IN_DATE",getSQLDate(null));
      logger.debug("Setting IN_PROGRAM_ID to:" + programId);
      logger.debug("Setting IN_SECTION_NAME to:" + sectionName);
      logger.debug("Setting IN_DATE to: null");

      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

      doc = (Document)dataAccessUtil.execute(dataRequest);
      return doc;

      }
      catch(Exception ex)
      {
          logger.error(ex);
          throw(ex);
      }
      finally
      {
          dataRequest.getConnection().close();
      }
  }

  /**
   * Description: Takes in a string Date, checks for valid formatting
   * 				in the form of mm/dd/yyyy and converts it
   * 				to a SQL date of yyyy-mm-dd.
   *
   * @param String date in string format
   * @return java.sql.Date
   *
   * Note..copied from OE project
   */
   private java.sql.Date getSQLDate(String strDate) throws Exception
   {
       java.sql.Date sqlDate = null;
        String inDateFormat = "";
        int dateLength = 0;
        int firstSep = 0;
        int lastSep = 0;

      try{
        if ((strDate != null) && (!strDate.trim().equals(""))) {

		    // set input date format
		    dateLength = strDate.length();
		    if ( dateLength > 10) {
			    firstSep = strDate.indexOf("/");
          if(firstSep == 1)
          {
            inDateFormat = "M/dd/yyyy hh:mm:ss";
          }
          else if (firstSep == 2)
          {
            inDateFormat = "MM/dd/yyyy hh:mm:ss";
          }
          else
          {
            inDateFormat = "yyyy-MM-dd hh:mm:ss";
          }

		    } else {
			    firstSep = strDate.indexOf("/");
			    lastSep = strDate.lastIndexOf("/");

			    switch ( dateLength ) {
				    case 10:
			    		inDateFormat = "MM/dd/yyyy";
				    	break;
				    case 9:
				    	if ( firstSep == 1 ) {
					    	inDateFormat = "M/dd/yyyy";
				    	} else {
					    	inDateFormat = "MM/d/yyyy";
			    		}
				    	break;
				    case 8:
				    	if ( firstSep == 1 ) {
					    	inDateFormat = "M/d/yyyy";
				    	} else {
					    	inDateFormat = "MM/dd/yy";
			    		}
				    	break;
				    case 7:
				    	if ( firstSep == 1 ) {
					    	inDateFormat = "M/dd/yy";
				    	} else {
					    	inDateFormat = "MM/d/yy";
			    		}
				    	break;
				    case 6:
				    	inDateFormat = "M/d/yy";
				    	break;
				    default:
				    	break;
			    }
		    }
            //SimpleDateFormat sdfInput = new SimpleDateFormat( "MM/dd/yyyy" );
            SimpleDateFormat sdfInput = new SimpleDateFormat(inDateFormat);
            SimpleDateFormat sdfOutput = new SimpleDateFormat ( "yyyy-MM-dd" );

            java.util.Date date = sdfInput.parse( strDate );
            String outDateString = sdfOutput.format( date );

          // now that we have no errors, use the string to make a SQL date
          sqlDate = sqlDate.valueOf(outDateString);
        }
        return sqlDate;
        }
        catch(Exception ex)
        {
            logger.error(ex);
            throw(ex);
        }
    }

    /**
     * Creates text nodes for building XML document trees containing data that is displayed to user.
     *
     * @param String text node name
     * @param String text node value
     * @param Document document that creates the element
     * @param Element tagElement element to create the text node on
     */
    private Element createTextNode(String elementName, String elementValue, Document document, Element tagElement) throws Exception
    {
        try {
            tagElement = document.createElement(elementName);
            if(elementValue != null)
            {
                tagElement.appendChild(document.createTextNode(elementValue));
            }
            return tagElement;
        }
        catch(Exception ex)
        {
            logger.error(ex);
            throw(ex);
        }

    }

  /**
   * Checks if section name exists. The procedure is used when
   * a section is created as part of the data validation. Section
   * names are unique in the system.
   *
   * @param String section name
   * @return boolean true if section name exists, false otherwise
   * @throws Exception
   */
  public boolean sectionNameExists(String sectionName) throws Exception {
      dataRequest = DataRequestHelper.getInstance().getDataRequest();

      try{
          dataRequest.reset();

          dataRequest.setStatementID(SECTION_NAME_EXISTS);
          dataRequest.addInputParam("IN_SECTION_NAME", sectionName);
          logger.debug("Setting IN_SECTION_NAME to:" + sectionName);
          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

          status = (String)dataAccessUtil.execute(dataRequest);
          logger.debug("Status is:" + status);
          return "Y".equals(status);
      } catch (Exception e) {
          logger.error(e);
          throw e;
      } finally {
          dataRequest.getConnection().close();
      }
  }

  /**
   * Checks if program name exists. The procedure is used when
   * a program is created as part of the data validation. Program
   * names are unique in the system.
   *
   * @param String program name
   * @return boolean true if program name exists, false otherwise
   * @throws Exception
   */
  public boolean programNameExists(String programName) throws Exception {
      dataRequest = DataRequestHelper.getInstance().getDataRequest();

      try{
          dataRequest.reset();

          dataRequest.setStatementID(PROGRAM_NAME_EXISTS);
          dataRequest.addInputParam("IN_PROGRAM_NAME", programName);
          logger.debug("Setting IN_PROGRAM_NAME to:" + programName);
          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

          status = (String)dataAccessUtil.execute(dataRequest);
          logger.debug("Status is:" + status);
          return "Y".equals(status);
      } catch (Exception e) {
          logger.error(e);
          throw e;
      } finally {
          dataRequest.getConnection().close();
      }
  }

  /**
   * Checks if any selected section has the same start date as an existing section of the same type.
   * This check is necessary when updating a program. Only the first violating section name of
   * each type is returned.
   *
   * @param int program id
   * @param HttpServletRequest request
   * @return String the section names that have the same start date separated by comma,
   * empty string if no such section is found.
   * @throws Exception
   */
  public String getDuplicateStartDateError(long programId, HttpServletRequest request) throws Exception {
      // Only test text version since different versions have the same start date.
      String selectedSubjectId = request.getParameter("selSubjectId");
      String selectedHeaderId = request.getParameter("selHeaderIdText");
      String selectedGuaranteeId = request.getParameter("selGuaranteeIdText");
      String selectedMarketingId = request.getParameter("selMarketingIdText");
      String selectedContactId = request.getParameter("selContactIdText");
      String errorString = "";

      if (sectionStartDateClashes(selectedSubjectId, programId, SUBJECT)) {
          errorString = SUBJECT;
      }
      if (sectionStartDateClashes(selectedHeaderId, programId, HEADER)) {
          errorString = errorString == ""? "" : (errorString + ",");
          errorString = errorString + HEADER;
      }
      if (sectionStartDateClashes(selectedGuaranteeId, programId, GUARANTEE)) {
          errorString = errorString == ""? "" : (errorString + ",");
          errorString = errorString + GUARANTEE;
      }
      if (sectionStartDateClashes(selectedMarketingId, programId, MARKETING)) {
          errorString = errorString == ""? "" : (errorString + ",");
          errorString = errorString +  MARKETING;
      }
      if (sectionStartDateClashes(selectedContactId, programId, CONTACT)) {
          errorString = errorString == ""? "" : (errorString + ",");
          errorString = errorString +  CONTACT;
      }

      return errorString;
  }

  /**
   * Tests if the particular section has the same start date with one of the existing sections
   * of the type.
   *
   * @param String section id the section id of the selected section
   * @param int program id the program id being updated
   * @param String sectionType section type. One of Subject, Header, Guarantee, Marketing, Contact.
   * @return boolean true if the section has the same start date with one of the existing
   * sections of the type. False otherwise.
   * @throw Exception
   *
   */
  private boolean sectionStartDateClashes(String sectionId, long programId, String sectionType) throws Exception
  {
    try {
      Document sections = null;
      Map sectionMap = null;
      String addSectionStartDate = "";
      String addSectionName = "";

      dataRequest = DataRequestHelper.getInstance().getDataRequest();
      dataRequest.reset();

      // Retrieve section to add.
      dataRequest.setStatementID(VIEW_SECTION);
      dataRequest.addInputParam("IN_SECTION_ID", new Long(sectionId));
      logger.debug("Setting sectionId to " + sectionId);
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      sectionMap = (Map)dataAccessUtil.execute(dataRequest);

      if(!sectionMap.isEmpty())
      {
          CachedResultSet rs = (CachedResultSet) sectionMap.get(EMAIL_SECTIONS_CURSOR);

          if(rs != null && rs.next())
          {
              addSectionStartDate = (String)rs.getObject(3);
              addSectionName = (String)rs.getObject(11);
          }
      }

      // Retrieve sections of this type related to this program.
      dataRequest.reset();
      dataRequest.setStatementID(VIEW_SECTIONS);
      dataRequest.addInputParam("IN_PROGRAM_ID", new Long(programId));
      dataRequest.addInputParam("IN_SECTION_NAME", sectionType); // section type
      dataRequest.addInputParam("IN_DATE", null);
      sections = (Document)dataAccessUtil.execute(dataRequest);

      NodeList sectionList = sections.getElementsByTagName("SECTION");
      String start_date = "";
      String section_name = "";

      for(int i = 0; i < sectionList.getLength(); i++ ){
          start_date = (((Element)sectionList.item(i)).getElementsByTagName("start_date")).item(0).getFirstChild().getNodeValue();
          section_name = (((Element)sectionList.item(i)).getElementsByTagName("section_name")).item(0).getFirstChild().getNodeValue();

          if(addSectionStartDate.equals(start_date) && !section_name.equals(addSectionName)) {
              return true;
          }
      }
      return false;
    } catch (Exception e) {
      throw e;
    } finally {
      dataRequest.getConnection().close();
    }
  }

  /**
   * Checks if any selected sections have effective end dates. The procedure is called
   * when creating a default program, updating custom program to default, or updating
   * a default program.
   *
   * @param HttpServletRequest request
   * @return String concatenation of section names that have end date, empty string otherwise.
   * @throw Exception
   */
  public String getNonDefaultSectionError(HttpServletRequest request) throws Exception {
      // Only test text version since different versions have the same end date.
      String selectedSubjectId = request.getParameter("selSubjectId");
      String selectedHeaderId = request.getParameter("selHeaderIdText");
      String selectedGuaranteeId = request.getParameter("selGuaranteeIdText");
      String selectedMarketingId = request.getParameter("selMarketingIdText");
      String selectedContactId = request.getParameter("selContactIdText");
      String errorString = null;
      String formAction = request.getQueryString();
      String programId = request.getParameter("programId");

      String errorSubjectName = getNonDefaultSectionName(selectedSubjectId, programId, formAction, SUBJECT);
      String errorHeaderName = getNonDefaultSectionName(selectedHeaderId, programId, formAction, HEADER);
      String errorGuaranteeName = getNonDefaultSectionName(selectedGuaranteeId, programId, formAction, GUARANTEE);
      String errorMarketingName = getNonDefaultSectionName(selectedMarketingId, programId, formAction, MARKETING);
      String errorContactName = getNonDefaultSectionName(selectedContactId, programId, formAction, CONTACT);

      errorString = errorSubjectName == null? "" : errorSubjectName;
      errorString = errorHeaderName == null? errorString : errorString + "," + errorHeaderName;
      errorString = errorGuaranteeName == null? errorString : errorString + "," + errorGuaranteeName;
      errorString = errorMarketingName == null? errorString : errorString + "," + errorMarketingName;
      errorString = errorContactName == null? errorString : errorString + "," + errorContactName;

      // remove leading comma if necessary
      if(errorString.startsWith(",")) {
          errorString = errorString.substring(1);
      }
      return errorString;
  }

  /**
   * Returns the name of the section if it has effective end date given section id,
   * null if section does not have effective end date. Only return one violating name for each
   * type.
   *
   * @param String sectionId
   * @param String programId
   * @param String formAction
   * @param String sectionType
   * @return String concetenation of section names that have end dates, null otherwise.
   */
  private String getNonDefaultSectionName(String sectionId, String programId, String formAction, String sectionType) throws Exception {
      Document sections = null;
      Map sectionMap = null;
      String addSectionEndDate = "";
      String addSectionName = "";

    try {
      dataRequest = DataRequestHelper.getInstance().getDataRequest();
      dataRequest.reset();

      // Retrieve section to add.
      dataRequest.setStatementID(VIEW_SECTION);
      dataRequest.addInputParam("IN_SECTION_ID", new Long(sectionId));
      logger.debug("Setting sectionId to " + sectionId);
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      sectionMap = (Map)dataAccessUtil.execute(dataRequest);

      if(!sectionMap.isEmpty())
      {
          CachedResultSet rs = (CachedResultSet) sectionMap.get(EMAIL_SECTIONS_CURSOR);

          if(rs != null && rs.next())
          {
              addSectionEndDate = (String)rs.getObject(4);
              addSectionName = (String)rs.getObject(11);
          }
      }

      // If update program, also check other sections of this type related to this program.
      if((EmailAdminConstants.UPDATE).equals(formAction)
          || EmailAdminConstants.UPDATE_PREVIEW.equals(formAction)) {
          dataRequest.reset();
          dataRequest.setStatementID(VIEW_SECTIONS);
          dataRequest.addInputParam("IN_PROGRAM_ID", new Long(programId));
          dataRequest.addInputParam("IN_SECTION_NAME", sectionType); // section type
          dataRequest.addInputParam("IN_DATE", null);
          logger.debug("Setting IN_PROGRAM_ID to:" + programId);
          logger.debug("Setting IN_SECTION_NAME to:" + sectionType);
          logger.debug("Setting IN_DATE to null");
          sections = (Document)dataAccessUtil.execute(dataRequest);
          NodeList sectionList = sections.getElementsByTagName("SECTION");
          String end_date = "";
          String section_name = "";
          Element endDateElem = null;

          for(int i = 0; i < sectionList.getLength(); i++ ) {
              endDateElem = (Element)((Element)sectionList.item(i)).getElementsByTagName("end_date").item(0);
              if(endDateElem != null & endDateElem.getFirstChild() != null) {
                  end_date = endDateElem.getFirstChild().getNodeValue();
              }
              section_name = (((Element)sectionList.item(i)).getElementsByTagName("section_name")).item(0).getFirstChild().getNodeValue();

              if(end_date != null && end_date.length() > 0) {
                  return section_name;
              }

          }
      }

      if (addSectionEndDate != null && addSectionEndDate.length() > 0) {
          return addSectionName;
      }
      return null;
    } catch (Exception e) {
      throw e;
    } finally {
      dataRequest.getConnection().close();
    }
  }

  /**
   * Check if selected source codes can add this custom program without
   * custom program date overlap. Used when create or update a custom program.
   *
   * @param EamilProgramVO emailProgram
   * @String returns a string containing all the source codes in error.  Empty string
   *   is returned if there are no errors.
   * @throw Exception
   */
  public String programDateOverlap(EmailProgramVO emailProgram) throws Exception {
      dataRequest = DataRequestHelper.getInstance().getDataRequest();
      String sourceCodesInError = "";

      try{
          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

          long programId = emailProgram.getProgramId();
          String[] sources = emailProgram.getProgramSourceCodes();
          String result = null;

          for(int i = 0; i<sources.length; i++) {
              dataRequest.reset();
              dataRequest.setStatementID(CUSTOM_PROGRAM_OVERLAP);
              dataRequest.addInputParam("IN_SOURCE_CODE", sources[i]);
              dataRequest.addInputParam("IN_PROGRAM_ID", new Long(programId));
              logger.debug("Setting IN_SOURCE_CODE to:" + sources[i]);
              logger.debug("Setting IN_PROGRAM_ID to:" + programId);

              result = (String)dataAccessUtil.execute(dataRequest);
              if("Y".equals(result)) {
                  if(sourceCodesInError.length() > 0)
                  {
                    sourceCodesInError = sourceCodesInError + ",";
                  }
                  sourceCodesInError = sourceCodesInError + sources[i];
              }
          }
          return sourceCodesInError;
      } catch (Exception e) {
          logger.error(e);
          throw e;
      } finally {
          dataRequest.getConnection().close();
      }
  }
/*
  public static void main(String args[]) throws IOException
  {
      Map emailMap = null;
      try {
        Connection connection = DataRequestHelper.getInstance().getDBConnection();

        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);

        dataRequest.setStatementID("FRP.VIEW_CURRENT_EMAIL_SECTIONS");
        dataRequest.addInputParam("IN_SOURCE_CODE", "350");

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        emailMap = (Map)dataAccessUtil.execute(dataRequest);

        String status = (String) emailMap.get(STATUS_PARAM);
System.out.println(status);
      } catch (Exception e) {
          e.printStackTrace();
      }
  }
  */


 public boolean sourceCodeExists(String sourceCode)
 {

        boolean found = false;

      try{

        SourceMasterHandler sourceCodeHandler = (SourceMasterHandler)CacheManager.getInstance().getHandler("CACHE_NAME_SOURCE_MASTER");
        if(sourceCodeHandler != null)
        {
            SourceMasterVO sourceVO = sourceCodeHandler.getSourceCodeById(sourceCode);
            if(sourceVO != null)
            {
                found = true;
            }
        }
        else
        {
            Map paramMap = new HashMap();
            paramMap.put("IN_SOURCE_CODE",sourceCode);
            DataRequest dataRequest = DataRequestHelper.getInstance().getDataRequest();

            try
            {
              dataRequest.setStatementID(GET_SOURCE_CODE_BY_ID_STATEMENT);
              dataRequest.setInputParams(paramMap);
              Map resultMap = null;
              DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
              CachedResultSet rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
              if(rs != null && rs.next())
              {
                found = true;
              }
            }
            //close connection
            finally
            {
							try
							{
	              dataRequest.getConnection().close();
							}
							catch(Exception e)
							{}

            }

        }

      }
      catch(Exception e)
      {
        logger.error(e);
      }


        return found;

 }



}
