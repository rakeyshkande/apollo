
package com.ftd.osp.emailadmin.vo;

import java.io.Serializable;

import java.lang.reflect.Field;

import java.util.List;

/**
 * Email Program Sections Value Object that maps to EMAIL_SECTIONS database table
 * 
 * @author Doug Johnson
 */

public class EmailSectionsVO implements Serializable
{
  private List emailSectionContent;
  private long sectionId;
  private long programId;
  private String contentType;
  private String effectiveStartDate;
  private String effectiveEndDate;
  private String sectionDefault;
  private String sectionType;
  private String sectionName;
  private String sectionDescription;
  private List sectionContent;
  private String fromAddress;
  private String updatedBy;
  private String createdOn;
  private String updatedOn;

  public EmailSectionsVO()
  {
  }

  public String toString()
    {
        String lineSep = System.getProperty("line.separator");
        StringBuffer sb = new StringBuffer();
        StringBuffer vectorsSb = new StringBuffer();
        // Get this Class
        Class thisClass = this.getClass();
        sb.append("Object Type = ").append(thisClass.getName()).append(lineSep);
        
        // Use reflection to loop through all the fields
        Field[] fields = thisClass.getDeclaredFields();
        for(int i = 0; i < fields.length; i++)
        {
            try
            {
                if(fields[i].getType().equals(Class.forName("java.lang.String")))
                {
                    sb.append(fields[i].getName()).append(" = ").append(fields[i].get(this)).append(lineSep);
                }
                else if(fields[i].getType().equals(Class.forName("java.util.List")))
                {
                    List vect = (List)fields[i].get(this);
                    if(vect != null)
                    {
                        for (int j = 0; j < vect.size(); j++) 
                        {
                            vectorsSb.append(vect.get(j).toString());
                        }
                    }
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }

        sb.append(vectorsSb);
        
        return sb.toString();
    }

  public List getEmailSectionContent()
  {
    return emailSectionContent;
  }

  public void setEmailSectionContent(List newEmailSectionContent)
  {
    emailSectionContent = newEmailSectionContent;
  }

  public long getSectionId()
  {
    return sectionId;
  }

  public void setSectionId(long newSectionId)
  {
    sectionId = newSectionId;
  }

  public long getProgramId()
  {
    return programId;
  }

  public void setProgramId(long newProgramId)
  {
    programId = newProgramId;
  }

  public String getContentType()
  {
    return contentType;
  }

  public void setContentType(String newContentType)
  {
    contentType = newContentType;
  }

  public String getEffectiveStartDate()
  {
    return effectiveStartDate;
  }

  public void setEffectiveStartDate(String newEffectiveStartDate)
  {
    effectiveStartDate = newEffectiveStartDate;
  }

  public String getEffectiveEndDate()
  {
    return effectiveEndDate;
  }

  public void setEffectiveEndDate(String newEffectiveEndDate)
  {
    effectiveEndDate = newEffectiveEndDate;
  }

  public String getSectionDefault()
  {
    return sectionDefault;
  }

  public void setSectionDefault(String newSectionDefault)
  {
    sectionDefault = newSectionDefault;
  }

  public String getSectionType()
  {
    return sectionType;
  }

  public void setSectionType(String newSectionType)
  {
    sectionType = newSectionType;
  }

  public String getSectionName()
  {
    return sectionName;
  }

  public void setSectionName(String newSectionName)
  {
    sectionName = newSectionName;
  }

  public String getSectionDescription()
  {
    return sectionDescription;
  }

  public void setSectionDescription(String newSectionDescription)
  {
    sectionDescription = newSectionDescription;
  }



  public List getSectionContent()
  {
    return sectionContent;
  }

  public void setSectionContent(List newSectionContent)
  {
    sectionContent = newSectionContent;
  }

  public String getFromAddress()
  {
    return fromAddress;
  }

  public void setFromAddress(String newFromAddress)
  {
    fromAddress = newFromAddress;
  }

  public String getUpdatedBy()
  {
    return updatedBy;
  }

  public void setUpdatedBy(String newUpdatedBy)
  {
    updatedBy = newUpdatedBy;
  }

  public String getCreatedOn()
  {
    return createdOn;
  }

  public void setCreatedOn(String newCreatedOn)
  {
    createdOn = newCreatedOn;
  }

  public String getUpdatedOn()
  {
    return updatedOn;
  }

  public void setUpdatedOn(String newUpdatedOn)
  {
    updatedOn = newUpdatedOn;
  }
}