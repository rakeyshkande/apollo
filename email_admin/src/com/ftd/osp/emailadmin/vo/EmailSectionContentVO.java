package com.ftd.osp.emailadmin.vo;

import java.io.Serializable;

import java.lang.reflect.Field;

import java.util.List;
import com.ftd.osp.utilities.xml.*;

/**
 * Email Program Section Content Value Object that maps data from EMAIL_SECTIONS AND 
 * EMAIL_SECTION_CONTENT database tables
 * 
 * @author Doug Johnson
 */

public class EmailSectionContentVO implements Serializable  
{
  private long sectionId;
  private long contentSectionOrderId;
  private String[] content;
  private String contentType;

  public EmailSectionContentVO()
  {
  }

  public String toString()
    {
        String lineSep = System.getProperty("line.separator");
        StringBuffer sb = new StringBuffer();
        StringBuffer vectorsSb = new StringBuffer();
        // Get this Class
        Class thisClass = this.getClass();
        sb.append("Object Type = ").append(thisClass.getName()).append(lineSep);
        
        // Use reflection to loop through all the fields
        Field[] fields = thisClass.getDeclaredFields();
        for(int i = 0; i < fields.length; i++)
        {
            try
            {
                if(fields[i].getType().equals(Class.forName("java.lang.String")))
                {
                    sb.append(fields[i].getName()).append(" = ").append(fields[i].get(this)).append(lineSep);
                }
                else if(fields[i].getType().equals(Class.forName("java.util.List")))
                {
                    List vect = (List)fields[i].get(this);
                    if(vect != null)
                    {
                        for (int j = 0; j < vect.size(); j++) 
                        {
                            vectorsSb.append(vect.get(j).toString());
                        }
                    }
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }

        sb.append(vectorsSb);
        
        return sb.toString();
    }

  public long getSectionId()
  {
    return sectionId;
  }

  public void setSectionId(long newSectionId)
  {
    sectionId = newSectionId;
  }

  public long getContentSectionOrderId()
  {
    return contentSectionOrderId;
  }

  public void setContentSectionOrderId(long newContentSectionOrderId)
  {
    contentSectionOrderId = newContentSectionOrderId;
  }

  public String[] getContent()
  {
    return content;
  }

  public void setContent(String[] newContent)
  {
/*
    if (newContent != null)  {
        content = new String[newContent.length];
    }
    
    //encode special chars to xml safe format
    for (int i = 0;i < newContent.length; i++)  {
      content[i] = DOMUtil.encodeChars(newContent[i]);
    }
*/
    content = newContent;
  }

  public String getContentType()
  {
    return contentType;
  }

  public void setContentType(String newContentType)
  {
    contentType = newContentType;
  }
}