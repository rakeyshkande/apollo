package com.ftd.osp.emailadmin.vo;

import java.io.Serializable;

import java.lang.reflect.Field;

import java.util.List;

/**
 * Email Program Value Object that maps to PROGRAM_MAPPING database table
 * 
 * @author Doug Johnson
 */
public class EmailProgramVO implements Serializable
{
  private long programId;
  private String programDescription;
  private String[] sourceCodes;
  private String[] sections;
  private String programName;
  private String effectiveStartDate;
  private String effectiveEndDate;
  private String updatedBy;
  private String updatedOn;
  private String programDefault;
  private String createdOn;

  public EmailProgramVO()
  {
  }

  public String toString()
    {
        String lineSep = System.getProperty("line.separator");
        StringBuffer sb = new StringBuffer();
        StringBuffer vectorsSb = new StringBuffer();
        // Get this Class
        Class thisClass = this.getClass();
        sb.append("Object Type = ").append(thisClass.getName()).append(lineSep);
        
        // Use reflection to loop through all the fields
        Field[] fields = thisClass.getDeclaredFields();
        for(int i = 0; i < fields.length; i++)
        {
            try
            {
                if(fields[i].getType().equals(Class.forName("java.lang.String")))
                {
                    sb.append(fields[i].getName()).append(" = ").append(fields[i].get(this)).append(lineSep);
                }
                else if(fields[i].getType().equals(Class.forName("java.util.List")))
                {
                    List vect = (List)fields[i].get(this);
                    if(vect != null)
                    {
                        for (int j = 0; j < vect.size(); j++) 
                        {
                            vectorsSb.append(vect.get(j).toString());
                        }
                    }
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }

        sb.append(vectorsSb);
        
        return sb.toString();
    }

  public long getProgramId()
  {
    return programId;
  }

  public void setProgramId(long newProgramId)
  {
    programId = newProgramId;
  }

  public String getProgramDescription()
  {
    return programDescription;
  }

  public void setProgramDescription(String newProgramDescription)
  {
    programDescription = newProgramDescription;
  }

  public String[] getProgramSourceCodes()
  {
    return sourceCodes;
  }

  public void setSourceCodes(String[] newSourceCodes)
  {
    sourceCodes = newSourceCodes;
  }

  public String[] getSections()
  {
    return sections;
  }

  public void setSections(String[] newSections)
  {
    sections = newSections;
  }

  public String getProgramName()
  {
    return programName;
  }

  public void setProgramName(String newProgramName)
  {
    programName = newProgramName;
  }

  public String getEffectiveStartDate()
  {
    return effectiveStartDate;
  }

  public void setEffectiveStartDate(String newEffectiveStartDate)
  {
    effectiveStartDate = newEffectiveStartDate;
  }

  public String getEffectiveEndDate()
  {
    return effectiveEndDate;
  }

  public void setEffectiveEndDate(String newEffectiveEndDate)
  {
    effectiveEndDate = newEffectiveEndDate;
  }

  public String getUpdatedBy()
  {
    return updatedBy;
  }

  public void setUpdatedBy(String newUpdatedBy)
  {
    updatedBy = newUpdatedBy;
  }

  public String getUpdatedOn()
  {
    return updatedOn;
  }

  public void setUpdatedOn(String newUpdatedOn)
  {
    updatedOn = newUpdatedOn;
  }

  public String getProgramDefault()
  {
    return programDefault;
  }

  public void setProgramDefault(String newProgramDefault)
  {
    programDefault = newProgramDefault;
  }

  public String getCreatedOn()
  {
    return createdOn;
  }

  public void setCreatedOn(String newCreatedOn)
  {
    createdOn = newCreatedOn;
  }
}