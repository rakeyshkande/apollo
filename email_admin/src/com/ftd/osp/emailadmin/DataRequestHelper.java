package com.ftd.osp.emailadmin;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;

import java.sql.Connection;

/**
 * Database access class for establishing connection to database
 * Used for stored procedure call statements
 * 
 * @author Doug Johnson
 */

public class DataRequestHelper 
{
    private static DataRequestHelper DATA_REQUEST_HELPER;
    private final static String EMAIL_ADMIN_CONFIG_FILE = "emailadmin-config.xml";
    private static String DATASOURCE = "";

    public static synchronized DataRequestHelper getInstance() throws Exception
    {
        // Load configuration properties
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance(); 
        DATASOURCE  = configUtil.getProperty(EMAIL_ADMIN_CONFIG_FILE, "datasource");

        if (DATA_REQUEST_HELPER == null)
        {
            DATA_REQUEST_HELPER = new DataRequestHelper();
            return DATA_REQUEST_HELPER;
        }
        else 
        {
            return DATA_REQUEST_HELPER;
        }
    }

    /**
     * Builds data request for stored procedure call
     * @param none
     * @exception Exception if any errors are encountered
     * @return DataRequest
     */
    public DataRequest getDataRequest() throws Exception
    {
        Connection connection =  DataSourceUtil.getInstance().getConnection(DATASOURCE);
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);

        return dataRequest;
    }

    /**
     * Gets a database connection from the pool.  Used for stored procedure calls
     * @param none
     * @exception Exception if any errors are encountered
     * @return DataRequest
     */
    public Connection getDBConnection() throws Exception
    {
        Connection connection =  DataSourceUtil.getInstance().getConnection(DATASOURCE);
        return connection;
    }    
}


  