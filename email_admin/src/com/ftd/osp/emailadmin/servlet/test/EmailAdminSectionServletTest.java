package com.ftd.osp.emailadmin.servlet.test;

import com.ftd.osp.emailadmin.servlet.*;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.w3c.dom.Document;


public class EmailAdminSectionServletTest  extends TestCase
{
    private static EmailAdminSectionServlet emailAdminSectionServlet;
    private String FAILURE = "Servlet Failed";
    
    /** 
     * Create a constructor that take a String parameter and passes it 
     * to the super class 
     **/ 
    public EmailAdminSectionServletTest(String name) { 
        super(name); 
        try
        {

        }
        catch(Exception e)
        {
          e.printStackTrace();
        }

    }

 protected void setUp()
    { 
       
    }   

   /** 
    * Test EmailAdminSectionServlet with valid url parameters
    * Test is successful if response is not equal to FAILURE
    **/ 
    public void testExit() throws Exception{ 
        Map map = new HashMap();

        String URL = "http://192.168.110.248:8008/emailadmin/EmailAdminSectionServlet?exit";

        String response = send(URL,map);          
        System.out.println("testing valid URL");
        System.out.println("response: " + response);
        //Validate response
        //Check if the result is the same as expected
        assertTrue(!response.equals(FAILURE));
    } 
   
   /** 
    * Test EmailAdminSectionServlet with valid url parameters
    * Test is successful if response is not equal to FAILURE
    **/ 
    public void testNewSection() throws Exception{ 
        Map map = new HashMap();

        String URL = "http://192.168.110.248:8008/emailadmin/EmailAdminSectionServlet?create";

        map.put("sectionType","Subject");
        map.put("sectionName","Test Junit");
        map.put("sectionDescription","Test");
        map.put("effectiveStartDate","12/10/03");
        map.put("effectiveEndDate","12/31/03");
        map.put("FromAddr","csr@ftdi.com");
        map.put("selDefault","N");
        map.put("textContent","This is a test from junit");
        map.put("htmlContent","Test");
        
        String response = send(URL,map);          
        System.out.println("testing valid URL");
        System.out.println("response: " + response);
        //Validate response
        //Check if the result is the same as expected
        assertTrue(!response.equals(FAILURE));
    } 

   /** 
    * Test EmailAdminSectionServlet with valid url parameters
    * Test is successful if response is not equal to FAILURE
    **/ 
    public void testUpdateSection() throws Exception{ 
        Map map = new HashMap();

        String URL = "http://192.168.110.248:8008/emailadmin/EmailAdminSectionServlet?update";

        map.put("sectionType","Subject");
        map.put("sectionName","Test Junit");
        map.put("sectionDescription","Test Update");
        map.put("effectiveStartDate","12/10/03");
        map.put("effectiveEndDate","12/31/03");
        map.put("FromAddr","csr@ftdi.com");
        map.put("selDefault","N");
        map.put("textContent","This is a test from junit");
        map.put("htmlContent","Test");
        map.put("textContentId","1000126");
        
        String response = send(URL,map);          
        System.out.println("testing valid URL");
        System.out.println("response: " + response);
        //Validate response
        //Check if the result is the same as expected
        assertTrue(!response.equals(FAILURE));
    } 

    /** 
    * Test EmailAdminSectionServlet with valid url parameters
    * Test is successful if response is not equal to FAILURE
    **/ 
    public void testViewSection() throws Exception{ 
        Map map = new HashMap();

        String URL = "http://192.168.110.248:8008/emailadmin/EmailAdminSectionServlet?viewupdate";

        map.put("sectionType","Subject");
        map.put("sectionName","Test Junit");
        
        String response = send(URL,map);          
        System.out.println("testing valid URL");
        System.out.println("response: " + response);
        //Validate response
        //Check if the result is the same as expected
        assertTrue(!response.equals(FAILURE));
    }    

   /** 
    * Test EmailAdminSectionServlet with valid url parameters
    * Test is successful if response is not equal to FAILURE
    **/ 
    public void testCopySection() throws Exception{ 
        Map map = new HashMap();

        String URL = "http://192.168.110.248:8008/emailadmin/EmailAdminSectionServlet?copy";

        map.put("sectionType","Subject");
        map.put("sectionName","Test Junit Copy");
        map.put("sectionDescription","Test Copy");
        map.put("effectiveStartDate","12/10/03");
        map.put("effectiveEndDate","12/31/03");
        map.put("FromAddr","csr@ftdi.com");
        map.put("selDefault","N");
        map.put("textContent","This is a test from junit");
        map.put("htmlContent","Test");

        
        String response = send(URL,map);          
        System.out.println("testing valid URL");
        System.out.println("response: " + response);
        //Validate response
        //Check if the result is the same as expected
        assertTrue(!response.equals(FAILURE));
    } 

   /** 
    * Test EmailAdminSectionServlet with valid url parameters
    * Test is successful if response is not equal to FAILURE
    **/ 
    public void testDeleteSection() throws Exception{ 
        Map map = new HashMap();

        String URL = "http://192.168.110.248:8008/emailadmin/EmailAdminSectionServlet?delete";

        map.put("sectionType","Subject");
        map.put("sectionName","Test Junit Copy");
        map.put("sectionId","1000126");

        
        String response = send(URL,map);          
        System.out.println("testing valid URL");
        System.out.println("response: " + response);
        //Validate response
        //Check if the result is the same as expected
        assertTrue(!response.equals(FAILURE));
    }
    
  /**
   * Get connection to URL. Construct request and get back a response.
   */
	private synchronized String send(String urlString, Map params)
        throws Exception{

        URLConnection con = null;
        boolean firstx      = true;

        try {
            URL url = new URL(urlString);

            //create connection to URL
            con = url.openConnection();
            con.setDoOutput(true);

            //Send data
            OutputStream out = con.getOutputStream();
            Set entries = params.entrySet();
            Iterator iter = entries.iterator();

            StringBuffer paramString = new StringBuffer();
            while(iter.hasNext()) {
                if (firstx)
                {
                  firstx = false;
                }
                else
                {
                    paramString.append('&');
                }

                Map.Entry entry = (Map.Entry)iter.next();
                String key = (String)entry.getKey();
                String value = (String)entry.getValue();

                paramString.append(key);
                paramString.append('=');
                paramString.append(URLEncoder.encode(value));
            }
            System.out.print(paramString);
            out.write(paramString.toString().getBytes());
            out.flush();
            out.close();

            InputStream is = con.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line=null;
            StringBuffer response = new StringBuffer("");
            while( (line = br.readLine())!=null)
            {
                response.append(line);
            }

            return response.toString();

        }
         catch (java.net.MalformedURLException e)  {
                e.printStackTrace();
                throw e;
        }
         catch (java.io.IOException e)  {
                e.printStackTrace();
                throw e;
        }
        finally{
            con = null;
        }

    }//end, send
    
    public static TestSuite suite()
    {
      TestSuite suite = new TestSuite();
      try {

      suite.addTest(new EmailAdminSectionServletTest("testNewSection"));
      suite.addTest(new EmailAdminSectionServletTest("testUpdateSection"));
      suite.addTest(new EmailAdminSectionServletTest("testViewSection"));
      suite.addTest(new EmailAdminSectionServletTest("testCopySection"));
      suite.addTest(new EmailAdminSectionServletTest("testDeleteSection"));
      suite.addTest(new EmailAdminSectionServletTest("testExit"));

      } catch (Exception e) {
          e.printStackTrace();
      }

      return suite;
    } 

    public static void main(String args[])
{
  junit.textui.TestRunner.run( suite() );
}

}