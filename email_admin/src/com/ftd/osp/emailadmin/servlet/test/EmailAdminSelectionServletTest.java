package com.ftd.osp.emailadmin.servlet.test;

import com.ftd.osp.emailadmin.servlet.*;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.w3c.dom.Document;


public class EmailAdminSelectionServletTest  extends TestCase
{
    private static EmailAdminSelectionServlet emailAdminSelectionServlet;
    private String FAILURE = "Servlet Failed";
    
    /** 
     * Create a constructor that take a String parameter and passes it 
     * to the super class 
     **/ 
    public EmailAdminSelectionServletTest(String name) { 
        super(name); 
        try
        {

        }
        catch(Exception e)
        {
          e.printStackTrace();
        }

    }

 protected void setUp()
    { 
       
    }   

   /** 
    * Test EmailAdminSelectionServlet with valid url parameters
    * Test is successful if response is not equal to FAILURE
    **/ 
    public void testExit() throws Exception{ 
        Map map = new HashMap();

        String URL = "http://192.168.110.248:8008/emailadmin/EmailAdminSelectionServlet?exit";

        String response = send(URL,map);          
        System.out.println("testing valid URL");
        System.out.println("response: " + response);
        //Validate response
        //Check if the result is the same as expected
        assertTrue(!response.equals(FAILURE));
    } 
   
   /** 
    * Test EmailAdminSelectionServlet with valid url parameters
    * Test is successful if response is not equal to FAILURE
    **/ 
    public void testNewProgram() throws Exception{ 
        Map map = new HashMap();

        String URL = "http://192.168.110.248:8008/emailadmin/EmailAdminSelectionServlet?new";

        map.put("option","Program");
        
        String response = send(URL,map);          
        System.out.println("testing valid URL");
        System.out.println("response: " + response);
        //Validate response
        //Check if the result is the same as expected
        assertTrue(!response.equals(FAILURE));
    } 

   /** 
    * Test EmailAdminSelectionServlet with valid url parameters
    * Test is successful if response is not equal to FAILURE
    **/ 
    public void testNewSection() throws Exception{ 
        Map map = new HashMap();

        String URL = "http://192.168.110.248:8008/emailadmin/EmailAdminSelectionServlet?new";

        //since all sections use same code, test only 1 type of section
        map.put("option","Subject");
        
        String response = send(URL,map);          
        System.out.println("testing valid URL");
        System.out.println("response: " + response);
        //Validate response
        //Check if the result is the same as expected
        assertTrue(!response.equals(FAILURE));
    } 

   /** 
    * Test EmailAdminSelectionServlet with valid url parameters
    * Test is successful if response is not equal to FAILURE
    **/ 
    public void testSearchProgram() throws Exception{ 
        Map map = new HashMap();

        String URL = "http://192.168.110.248:8008/emailadmin/EmailAdminSelectionServlet?search";

        //return all programs by no search criteria
        map.put("option","Program");
        map.put("searchName","");
        
        String response = send(URL,map);          
        System.out.println("testing valid URL");
        System.out.println("response: " + response);
        //Validate response
        //Check if the result is the same as expected
        assertTrue(!response.equals(FAILURE));
    } 

   /** 
    * Test EmailAdminSelectionServlet with valid url parameters
    * Test is successful if response is not equal to FAILURE
    **/ 
    public void testSearchSection() throws Exception{ 
        Map map = new HashMap();

        String URL = "http://192.168.110.248:8008/emailadmin/EmailAdminSelectionServlet?search";

        //return all subject sections by no search criteria
        map.put("option","Subject");
        map.put("searchName","");
        
        String response = send(URL,map);          
        System.out.println("testing valid URL");
        System.out.println("response: " + response);
        //Validate response
        //Check if the result is the same as expected
        assertTrue(!response.equals(FAILURE));
    } 

   /** 
    * Test EmailAdminSelectionServlet with valid url parameters
    * Test is successful if response is not equal to FAILURE
    **/ 
    public void testSearchSource() throws Exception{ 
        Map map = new HashMap();

        String URL = "http://192.168.110.248:8008/emailadmin/EmailAdminSelectionServlet?search";

        map.put("option","Source Code");
        map.put("searchName","CO");
        
        String response = send(URL,map);          
        System.out.println("testing valid URL");
        System.out.println("response: " + response);
        //Validate response
        //Check if the result is the same as expected
        assertTrue(!response.equals(FAILURE));
    } 
    
  /**
   * Get connection to URL. Construct request and get back a response.
   */
	private synchronized String send(String urlString, Map params)
        throws Exception{

        URLConnection con = null;
        boolean firstx      = true;

        try {
            URL url = new URL(urlString);

            //create connection to URL
            con = url.openConnection();
            con.setDoOutput(true);

            //Send data
            OutputStream out = con.getOutputStream();
            Set entries = params.entrySet();
            Iterator iter = entries.iterator();

            StringBuffer paramString = new StringBuffer();
            while(iter.hasNext()) {
                if (firstx)
                {
                  firstx = false;
                }
                else
                {
                    paramString.append('&');
                }

                Map.Entry entry = (Map.Entry)iter.next();
                String key = (String)entry.getKey();
                String value = (String)entry.getValue();

                paramString.append(key);
                paramString.append('=');
                paramString.append(URLEncoder.encode(value));
            }
            System.out.print(paramString);
            out.write(paramString.toString().getBytes());
            out.flush();
            out.close();

            InputStream is = con.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line=null;
            StringBuffer response = new StringBuffer("");
            while( (line = br.readLine())!=null)
            {
                response.append(line);
            }

            return response.toString();

        }
         catch (java.net.MalformedURLException e)  {
                e.printStackTrace();
                throw e;
        }
         catch (java.io.IOException e)  {
                e.printStackTrace();
                throw e;
        }
        finally{
            con = null;
        }

    }//end, send
    
    public static TestSuite suite()
    {
      TestSuite suite = new TestSuite();
      try {
      
      suite.addTest(new EmailAdminSelectionServletTest("testNewProgram"));
      suite.addTest(new EmailAdminSelectionServletTest("testNewSection"));
      suite.addTest(new EmailAdminSelectionServletTest("testSearchProgram"));
      suite.addTest(new EmailAdminSelectionServletTest("testSearchSection"));
      suite.addTest(new EmailAdminSelectionServletTest("testSearchSource"));
      suite.addTest(new EmailAdminSelectionServletTest("testExit"));

      } catch (Exception e) {
          e.printStackTrace();
      }

      return suite;
    } 

    public static void main(String args[])
{
  junit.textui.TestRunner.run( suite() );
}

}