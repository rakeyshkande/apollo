package com.ftd.osp.emailadmin.servlet.test;

import com.ftd.osp.emailadmin.servlet.*;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.w3c.dom.Document;


public class EmailAdminProgramServletTest  extends TestCase
{
    private static EmailAdminProgramServlet emailAdminProgramServlet;
    private String FAILURE = "Servlet Failed";
    
    /** 
     * Create a constructor that take a String parameter and passes it 
     * to the super class 
     **/ 
    public EmailAdminProgramServletTest(String name) { 
        super(name); 
        try
        {

        }
        catch(Exception e)
        {
          e.printStackTrace();
        }

    }

 protected void setUp()
    { 
       
    }   

   /** 
    * Test EmailAdminProgramServlet with valid url parameters
    * Test is successful if response is not equal to FAILURE
    **/ 
    public void testExit() throws Exception{ 
        Map map = new HashMap();

        String URL = "http://192.168.110.248:8008/emailadmin/EmailAdminProgramServlet?exit";

        String response = send(URL,map);          
        System.out.println("testing valid URL");
        System.out.println("response: " + response);
        //Validate response
        //Check if the result is the same as expected
        assertTrue(!response.equals(FAILURE));
    } 
   
   /** 
    * Test EmailAdminProgramServlet with valid url parameters
    * Test is successful if response is not equal to FAILURE
    **/ 
    public void testNewProgram() throws Exception{ 
        Map map = new HashMap();

        String URL = "http://192.168.110.248:8008/emailadmin/EmailAdminProgramServlet?create";

        map.put("programName","Test Junit 2");
        map.put("programDescription","Test from JUnit");
        map.put("effectiveStartDate","12/10/03");
        map.put("effectiveEndDate","2/3/04");
        map.put("selDefault","N");

        map.put("selSubjectId","1000186");
        map.put("selHeaderIdText","1000159");
        map.put("selHeaderIdHtml","1000160");
        map.put("selGuaranteeIdText","1000033");
        //map.put("selGuaranteeIdHtml","100034");
        map.put("selMarketingIdText","1000195");
        map.put("selMarketingIdHtml","1000196");
        map.put("selContactIdText","1000374");
        map.put("selContactIdHtml","1000375");

        map.put("selSourceCodes","350");
        
        String response = send(URL,map);          
        System.out.println("testing valid URL");
        System.out.println("response: " + response);
        //Validate response
        //Check if the result is the same as expected
        assertTrue(!response.equals(FAILURE));
    } 

   /** 
    * Test EmailAdminProgramServlet with valid url parameters
    * Test is successful if response is not equal to FAILURE
    **/ 
    public void testUpdateProgram() throws Exception{ 
        Map map = new HashMap();

        String URL = "http://192.168.110.248:8008/emailadmin/EmailAdminProgramServlet?update";

        map.put("programName","Test Junit 2");
        map.put("programDescription","Test Update");
        map.put("effectiveStartDate","12/10/03");
        map.put("effectiveEndDate","12/31/03");
        map.put("selDefault","N");

        map.put("selSubjectId","1000186");
        map.put("selHeaderIdText","1000159");
        map.put("selHeaderIdHtml","1000160");
        map.put("selGuaranteeIdText","1000033");
        //map.put("selGuaranteeIdHtml","100034");
        map.put("selMarketingIdText","1000195");
        map.put("selMarketingIdHtml","1000196");
        map.put("selContactIdText","1000374");
        map.put("selContactIdHtml","1000374");

        map.put("selSourceCodes","350");
        map.put("unselSourceCodes","");
        map.put("programId","100188");
        
        String response = send(URL,map);          
        System.out.println("testing valid URL");
        System.out.println("response: " + response);
        //Validate response
        //Check if the result is the same as expected
        assertTrue(!response.equals(FAILURE));
    } 

    /** 
    * Test EmailAdminProgramServlet with valid url parameters
    * Test is successful if response is not equal to FAILURE
    **/ 
    public void testViewProgram() throws Exception{ 
        Map map = new HashMap();

        String URL = "http://192.168.110.248:8008/emailadmin/EmailAdminProgramServlet?viewupdate";

        map.put("programId","100188");
        
        String response = send(URL,map);          
        System.out.println("testing valid URL");
        System.out.println("response: " + response);
        //Validate response
        //Check if the result is the same as expected
        assertTrue(!response.equals(FAILURE));
    }    

   /** 
    * Test EmailAdminProgramServlet with valid url parameters
    * Test is successful if response is not equal to FAILURE
    **/ 
    public void testCopyProgram() throws Exception{ 
        Map map = new HashMap();

        String URL = "http://192.168.110.248:8008/emailadmin/EmailAdminProgramServlet?copy";

        map.put("programId","100188");
        
        String response = send(URL,map);          
        System.out.println("testing valid URL");
        System.out.println("response: " + response);
        //Validate response
        //Check if the result is the same as expected
        assertTrue(!response.equals(FAILURE));
    } 

   /** 
    * Test EmailAdminProgramServlet with valid url parameters
    * Test is successful if response is not equal to FAILURE
    **/ 
    public void testDeleteProgram() throws Exception{ 
        Map map = new HashMap();

        String URL = "http://192.168.110.248:8008/emailadmin/EmailAdminProgramServlet?delete";

        map.put("programId","100188");
         
        String response = send(URL,map);          
        System.out.println("testing valid URL");
        System.out.println("response: " + response);
        //Validate response
        //Check if the result is the same as expected
        assertTrue(!response.equals(FAILURE));
    }
    
  /**
   * Get connection to URL. Construct request and get back a response.
   */
	private synchronized String send(String urlString, Map params)
        throws Exception{

        URLConnection con = null;
        boolean firstx      = true;

        try {
            URL url = new URL(urlString);

            //create connection to URL
            con = url.openConnection();
            con.setDoOutput(true);

            //Send data
            OutputStream out = con.getOutputStream();
            Set entries = params.entrySet();
            Iterator iter = entries.iterator();

            StringBuffer paramString = new StringBuffer();
            while(iter.hasNext()) {
                if (firstx)
                {
                  firstx = false;
                }
                else
                {
                    paramString.append('&');
                }

                Map.Entry entry = (Map.Entry)iter.next();
                String key = (String)entry.getKey();
                String value = (String)entry.getValue();

                paramString.append(key);
                paramString.append('=');
                paramString.append(URLEncoder.encode(value));
            }
            System.out.print(paramString);
            out.write(paramString.toString().getBytes());
            out.flush();
            out.close();

            InputStream is = con.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line=null;
            StringBuffer response = new StringBuffer("");
            while( (line = br.readLine())!=null)
            {
                response.append(line);
            }

            return response.toString();

        }
         catch (java.net.MalformedURLException e)  {
                e.printStackTrace();
                throw e;
        }
         catch (java.io.IOException e)  {
                e.printStackTrace();
                throw e;
        }
        finally{
            con = null;
        }

    }//end, send
    
    public static TestSuite suite()
    {
      TestSuite suite = new TestSuite();
      try {

      suite.addTest(new EmailAdminProgramServletTest("testNewProgram"));
      suite.addTest(new EmailAdminProgramServletTest("testUpdateProgram"));
      suite.addTest(new EmailAdminProgramServletTest("testViewProgram"));
      suite.addTest(new EmailAdminProgramServletTest("testCopyProgram"));
      suite.addTest(new EmailAdminProgramServletTest("testDeleteProgram"));
      suite.addTest(new EmailAdminProgramServletTest("testExit"));

      } catch (Exception e) {
          e.printStackTrace();
      }

      return suite;
    } 

    public static void main(String args[])
{
  junit.textui.TestRunner.run( suite() );
}

}