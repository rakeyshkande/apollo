package com.ftd.osp.emailadmin.servlet.test;

import com.ftd.osp.emailadmin.servlet.*;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.w3c.dom.Document;


public class EmailAdminPopupProgramServletTest  extends TestCase
{
    private static EmailAdminPopupProgramServlet emailAdminPopupPgoramServlet;
    private String FAILURE = "Servlet Failed";
    
    /** 
     * Create a constructor that take a String parameter and passes it 
     * to the super class 
     **/ 
    public EmailAdminPopupProgramServletTest(String name) { 
        super(name); 
        try
        {

        }
        catch(Exception e)
        {
          e.printStackTrace();
        }

    }

 protected void setUp()
    { 
       
    }   
   
   /** 
    * Test EmailAdminPopupProgramervlet with valid url parameters
    * Test is successful if response is not equal to FAILURE
    **/ 
    public void testPopupProgramServlet() throws Exception{ 
        Map map = new HashMap();

        String URL = "http://192.168.110.248:8008/emailadmin/EmailAdminPopupProgramServlet";

        map.put("programId","1");
        map.put("programName","TEST PROGRAM");
                
        String response = send(URL,map);          
        System.out.println("testing valid URL");
        System.out.println("response: " + response);
        //Validate response
        //Check if the result is the same as expected
        assertTrue(!response.equals(FAILURE));
    } 

  /** 
    * Test EmailAdminPopupProgramServlet with invalid url parameters
    * Test is successful if response is not equal to FAILURE
    **/ 
    public void testPopupProgramServletFail() throws Exception{ 
        Map map = new HashMap();
        String URL = "http://192.168.110.248:8008/emailadmin/EmailAdminPopupProgramServlet";

        map.put("programId","0");
        map.put("programName","NOT FOUND");
        
        String response = send(URL,map);          
        System.out.println("testing invalid URL");
        System.out.println("response: " + response);
        //Validate response
        //Check if the result is the same as expected
        assertTrue(!response.equals(FAILURE));
    } 
 
  /**
   * Get connection to URL. Construct request and get back a response.
   */
	private synchronized String send(String urlString, Map params)
        throws Exception{

        URLConnection con = null;
        boolean firstx      = true;

        try {
            URL url = new URL(urlString);

            //create connection to URL
            con = url.openConnection();
            con.setDoOutput(true);

            //Send data
            OutputStream out = con.getOutputStream();
            Set entries = params.entrySet();
            Iterator iter = entries.iterator();

            StringBuffer paramString = new StringBuffer();
            while(iter.hasNext()) {
                if (firstx)
                {
                  firstx = false;
                }
                else
                {
                    paramString.append('&');
                }

                Map.Entry entry = (Map.Entry)iter.next();
                String key = (String)entry.getKey();
                String value = (String)entry.getValue();

                paramString.append(key);
                paramString.append('=');
                paramString.append(URLEncoder.encode(value));
            }

            out.write(paramString.toString().getBytes());
            out.flush();
            out.close();

            InputStream is = con.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line=null;
            StringBuffer response = new StringBuffer("");
            while( (line = br.readLine())!=null)
            {
                response.append(line);
            }

            return response.toString();

        }
         catch (java.net.MalformedURLException e)  {
                e.printStackTrace();
                throw e;
        }
         catch (java.io.IOException e)  {
                e.printStackTrace();
                throw e;
        }
        finally{
            con = null;
        }

    }//end, send
    
    public static TestSuite suite()
    {
      TestSuite suite = new TestSuite();
      try {
      
      suite.addTest(new EmailAdminPopupProgramServletTest("testPopupProgramServlet"));
      suite.addTest(new EmailAdminPopupProgramServletTest("testPopupProgramServletFail"));

      } catch (Exception e) {
          e.printStackTrace();
      }

      return suite;
    } 

    public static void main(String args[])
{
  junit.textui.TestRunner.run( suite() );
}

}