package com.ftd.osp.emailadmin.servlet;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;
import com.ftd.security.exceptions.ExpiredIdentityException;
import com.ftd.security.exceptions.ExpiredSessionException;
import com.ftd.security.exceptions.InvalidSessionException;

import java.io.File;
import java.io.IOException;

import java.sql.SQLException;

import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import org.xml.sax.SAXException;

/**
 * Collection of methods used by the Servlets.
 * 
 * @author Doug Johnson
 */
public class ServletHelper  implements EmailAdminConstants {
    private static Logger logger = new Logger("com.ftd.osp.emailadmin.servlet.ServletHelper");

    /**
     * Indicates whether or not the security token and security context are valid.
     *
     * @param request the http request object
     * @exception ExpiredIdentityException
     * @exception ExpiredSessionException
     * @exception InvalidSessionException
     * @exception SAXException
     * @exception ParserConfigurationException
     * @exception IOException
     * @exception SQLException
     * @exception Exception
     * @return whether or not the security token and the security context are valid
     */
    public static boolean isValidToken(HttpServletRequest request)
      throws ExpiredIdentityException,
             ExpiredSessionException,
             InvalidSessionException,
             SAXException,
             ParserConfigurationException,
             IOException,
             SQLException,
             Exception{

      if(SECURITY_OFF)
      {
        return true;
      }

      String securityContext = request.getParameter(HTML_PARAM_SECURITY_CONTEXT); 
      logger.debug(securityContext);

      String securityToken = 
      request.getParameter(HTML_PARAM_SECURITY_TOKEN);
      if (securityToken == null || (securityToken.equals("")))  {
          logger.debug("No Security Token found");
          return false;
      }
       logger.debug(securityToken);

      logger.debug("authenticating security token");
      SecurityManager securityManager = SecurityManager.getInstance();
      return securityManager.authenticateSecurityToken(securityContext, getUnitId(), securityToken);
  }

  /**
   * Returns the path to exit Email confirmation and returns to the menu.
   * 
   * @param HttpServletRequest
   * @return String full exit path
   * @throw Exception
   */
  public static String getExitPath(HttpServletRequest request) throws Exception {
      return getExitPage() + getSecurityParams(request);
  }

  /**
   * Returns the page for Operations menu.
   * 
   * @return String exit page
   * @throw Exception
   */
  private static String getExitPage() throws Exception {
      ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
      return configUtil.getFrpGlobalParm(EMAIL_ADMIN_CONFIG_CONTEXT, EXIT_PAGE);
  }

  /**
   * Returns the parameters needed for security as a string.
   * 
   * @param HttpServletRequest
   * @return String security parameters
   * @throw Exception
   */
  private static String getSecurityParams(HttpServletRequest request) throws Exception {
      ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
      String securityAppContext = configUtil.getProperty(EMAIL_ADMIN_CONFIG_FILE, SEC_APP_CONTEXT);
      String securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN);
      String securityContext = request.getParameter(HTML_PARAM_SECURITY_CONTEXT);
 
      return "&securitytoken=" + securityToken + "&context=" + securityContext + "&applicationcontext=" + securityAppContext;
  }

  /**
   * Returns the unit id of this app. Retrieved from configuration file.
   * 
   * @return String unit id
   * @throw Exception
   */
  public static String getUnitId() throws Exception {
      ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
      return configUtil.getProperty(EMAIL_ADMIN_CONFIG_FILE, UNIT_ID);
  }  
  
  /**
    * Returns the identity logged in.
    * 
    * @return String identity id that is logged into the system
    * @throw Exception
    */
  public static String getMyId(String securityToken) throws Exception {

     if(SECURITY_OFF)
     {
       return "TEST";
     }

      SecurityManager secMgr = SecurityManager.getInstance();
      UserInfo myInfo = secMgr.getUserInfo(securityToken);
      if (myInfo != null) {
          return myInfo.getUserID();
      }
      return "";
  }  
  
 /**
  * Displays an apology page. Addes error message based on the Exception
  * 
  * @param request http request
  * @param response http response
  * @param File stylesteet
  * @param Exception the exception being thrown when the error occurs
  * @throws ServletException
  * @throws IOException
  */ 
  public static void sendErrorPage(HttpServletRequest request, 
                                    HttpServletResponse response, 
                                    File stylesheet,
                                    Exception ex) 
                throws ServletException, IOException
  {
    try{
        TraxUtil traxUtil = TraxUtil.getInstance();
        String securitytoken = request.getParameter(EmailAdminConstants.HTML_PARAM_SECURITY_TOKEN);
        String context = request.getParameter(EmailAdminConstants.HTML_PARAM_SECURITY_CONTEXT);

        Document doc = getDocumentWithAttribute("ERROR", "message", ex.getMessage());
        HashMap parameters = new HashMap();
        String isPopup = (String)request.getAttribute("isPopup");

        // Flag to close popup.
        parameters.put("isPopup", isPopup == null? "" : isPopup);
        parameters.put("securitytoken", securitytoken == null? "" : securitytoken);
        parameters.put("context", context == null? "" : context);
        traxUtil.transform(request, response, doc, stylesheet, parameters);

        response.flushBuffer();
    } catch(Exception e) {
        logger.error(e);
        throw new ServletException(e.getMessage());
    }
  }   

  /**
   * Returns a new Document with root element tagged as the input element name 
   * and attribute.
   * @param tag name of root element to be created.
   * @returns Document the created document
   * @throws SecurityAdminException
   */
  public static Document getDocumentWithAttribute(String tag, String attrName, String attrValue) 
    throws Exception
  {
    Document doc = null;
    try {
      doc = DOMUtil.getDefaultDocument();
      Element rootElem = (Element)doc.createElement(tag);
      if (attrValue != null) {
          rootElem.setAttribute(attrName, attrValue);
      }
      doc.appendChild(rootElem);
    } catch (Exception ex) {
      logger.error(ex);
      throw ex;
    }
    return doc;
  }

  /**
   * Saves the attribute to session.
   * 
   * @param HttpServletRequest the request to get session
   * @param String session variable key
   * @param String session variable value
   * @throw Exception
   */
  public static void saveSessionAttribute
    (HttpServletRequest request, String key, String value) throws Exception {
      HttpSession session = request.getSession(true);
      session.setAttribute(key, value);
  }

  /**
   * Saves the attribute to session.
   * 
   * @param HttpServletRequest the request to get session
   * @param String session variable key
   * @return String session variable value
   * @throw Exception
   */
  public static String retrieveSessionAttribute
    (HttpServletRequest request, String key) throws Exception {
      HttpSession session = request.getSession(true);
      return (String)session.getAttribute(key);
  }  

  /**
   * Checks if user is authorized to access this app. Get security token
   * from request and call SecurityManager assertPermission using the 
   * permission and resource defined for this applications: permission = View,
   * resource = ViewEmailConfirmation.
   * 
   * @param HttpServletRequest the request to get security token
   * @return boolean true if authorized, false otherwise
   * @throw Exception
   */
  public static boolean isAuthorized(HttpServletRequest request) throws Exception {

      if(SECURITY_OFF)
      {
        return true;
      }
      String securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN);
      String securityContext = request.getParameter(HTML_PARAM_SECURITY_CONTEXT);
 
      SecurityManager securityManager = SecurityManager.getInstance();
      return securityManager.assertPermission(securityContext, securityToken,
          EmailAdminConstants.EMAIL_CONFIRMATION_RESOURCE, EmailAdminConstants.EMAIL_CONFIRMATION_PERMISSION);
  }
}

