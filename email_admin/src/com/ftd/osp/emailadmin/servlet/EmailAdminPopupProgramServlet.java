package com.ftd.osp.emailadmin.servlet;

import com.ftd.osp.emailadmin.EmailAdminDAO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.security.exceptions.ExpiredIdentityException;
import com.ftd.security.exceptions.ExpiredSessionException;
import com.ftd.security.exceptions.InvalidSessionException;

import java.io.IOException;

import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;

/**
 * Controller servlet that is responsible for displaying the email sections
 * related to the email program.
 * 
 * @author Doug Johnson
 */
public class EmailAdminPopupProgramServlet extends EmailAdminBaseServlet
{
    private Logger logger;

    public void init(ServletConfig config) throws ServletException
    {
      super.init(config);
      this.logger = new Logger("com.ftd.osp.emailadmin.servlet.EmailAdminProgramServlet");
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(EmailAdminConstants.CONTENT_TYPE);
        this.searchProgramSections(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(EmailAdminConstants.CONTENT_TYPE);
        this.searchProgramSections(request, response);
    }

    /**
     * Retrieves all sections related to a program.
     * 
     * @param HttpServletRequest
     * @param HttpServletResponse
     * @throw Exception
     */
    private void searchProgramSections(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
      logger.debug("Entering EmailAdminPopupProgramServlet searchProgramSections...");
      try
      {
          //if (SECURITY_OFF || ServletHelper.isValidToken(request))
          if(true)
          {
              response.setContentType(EmailAdminConstants.CONTENT_TYPE);
              ServletContext context = this.getServletContext();

              String securityContext = null;
              String securityToken = null;

              securityContext = request.getParameter(HTML_PARAM_SECURITY_CONTEXT);
              securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN);

              String path = context.getRealPath("/xsl") + java.io.File.separator ;
              String page = "";

              Document responseDocument = DOMUtil.getDocument();
              String sprogramId = request.getParameter("programId");
              long programId = 0;
              if(sprogramId != null)
                  programId = new Long(sprogramId).longValue();

              String programName = request.getParameter("programName");
              HashMap pageData = new HashMap();
              pageData.put(HTML_PARAM_SECURITY_CONTEXT,securityContext);
              pageData.put(HTML_PARAM_SECURITY_TOKEN,securityToken);
              pageData.put("programName", programName);
              DOMUtil.addSection(responseDocument, "pageData", "data", pageData,true);

              EmailAdminDAO emailAdminDao = new EmailAdminDAO();
              Document sections = emailAdminDao.getRelatedProgramSections(programId);
              DOMUtil.addSection(responseDocument,sections.getElementsByTagName("SECTIONS"));

              page = path + RELATED_SECTIONS_PAGE;

              pageRedirect(request,response,responseDocument,page);
          }
      }
      catch(ExpiredIdentityException e )
      {
          logger.error(e);
          ServletHelper.sendErrorPage(request, response, errorStylesheet, e);
      }
      catch(ExpiredSessionException e )
      {
          logger.error(e);
          ServletHelper.sendErrorPage(request, response, errorStylesheet, e);
      }
      catch(InvalidSessionException e )
      {
          logger.error(e);
          ServletHelper.sendErrorPage(request, response, errorStylesheet, e);
      }
      catch(Exception e)
      {
          logger.error(e);
          ServletHelper.sendErrorPage(request, response, errorStylesheet, e);
      }
  }
}
