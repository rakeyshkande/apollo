package com.ftd.osp.emailadmin.servlet;

public interface EmailAdminConstants
{
  public static final String CONTENT_TYPE = "text/html; charset=UTF-8";
  public final static String XML_HEADER = "<?xml version='1.0' encoding='UTF-8'?><!-- the root element -->";
  public final static String EMAIL_ADMIN_CONFIG_FILE = "emailadmin-config.xml";
  public final static String EMAIL_ADMIN_CONFIG_CONTEXT = "EMAIL_ADMIN_CONFIG";

  //pages
  public final static String ERROR_PAGE = "/xsl/Error.xsl";
  public final static String MENU_PAGE = "OperationsMenu.html";
  public final static String START_PAGE = "EmailConfirmation.xsl";
  public final static String CREATE_PROGRAM_PAGE = "CreateProgram.xsl";
  public final static String CREATE_SECTION_PAGE = "CreateSection.xsl";
  public final static String SEARCH_SECTION_RESULTS_PAGE = "SearchSectionResults.xsl";
  public final static String SEARCH_PROGRAM_RESULTS_PAGE = "SearchProgramResults.xsl";
  public final static String UPDATE_SECTION_PAGE = "UpdateSection.xsl";
  public final static String RELATED_SECTIONS_PAGE = "RelatedSections.xsl";
  public final static String SOURCE_CODE_LOOKUP = "sourceCodeLookup.xsl"; // starts with a lower case alphabet
  public final static String PREVIEW_EMAIL = "PreviewEmail.xsl";

  //From address servers
  public final static String FTDI_ADDRESS = "confirmation@ftd.com";
  public final static String SFBM_ADDRESS = "confirmation@sfmusicbox.com";
  public final static String GIFTSENSE_ADDRESS = "confirmation@giftsense.com";
  public final static String BUTTERFIELDBLOOMS_ADDRESS = "confirmation@butterfieldblooms.com";
  public final static String FUSA_ADDRESS = "confirmation@flowersusa.com";
  public final static String FDIRECT_ADDRESS = "confirmation@flowersdirect.com";
  public final static String FLORIST_ADDRESS = "confirmation@florist.com";
  public final static String FTDCA_ADDRESS = "confirmation@ftd.ca";
  public final static String ROSES_ADDRESS = "confirmation@roses.com";

  //Page actions
  public final static String EXIT = "exit";
  public final static String CREATE = "create";
  public final static String UPDATE = "update";
  public final static String DELETE = "delete";
  public final static String COPY = "copy";
  public final static String VIEW_FOR_UPDATE = "viewupdate";
  public final static String BACK = "back";

  //Preview actions
  public final static String ONLY_PREVIEW = "preview";
  public final static String UPDATE_PREVIEW = "updatepreview";
  public final static String COPY_PREVIEW = "copypreview";
  public final static String CREATE_PREVIEW = "createpreview";

  //message content types
  public final static String SUBJECT = "Subject";
  public final static String HEADER = "Header";
  public final static String GUARANTEE = "Guarantee";
  public final static String MARKETING = "Marketing";
  public final static String CONTACT = "Contact";

  //security
  public final static String APPLICATION_CONTEXT_PROPERTY = "secadmin";
  public final static String HTML_PARAM_SECURITY_CONTEXT = "context";
  public final static String HTML_PARAM_SECURITY_TOKEN =  "securitytoken";

  public static final boolean SECURITY_OFF = false; //true indicates security is not being enforced
  public static final String MENU_ACTION = "menuAction";
  public static final String EXIT_PAGE = "exitPage";
  public static final String APP_CONTEXT = "appContext";
  public static final String SEC_APP_CONTEXT = "secAppContext";
  public static final String UNIT_ID = "unitID";

  public static final String EMAIL_CONFIRMATION_RESOURCE = "ViewEmailConfirmation";
  public static final String EMAIL_CONFIRMATION_PERMISSION = "View";

  // Error message to display if user is not authorized.
  public static final String NOT_AUTHORIZED = "Not authorized to perform action.";
}