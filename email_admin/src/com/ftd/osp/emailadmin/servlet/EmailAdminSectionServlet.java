package com.ftd.osp.emailadmin.servlet;

import java.util.StringTokenizer;
import com.ftd.osp.emailadmin.EmailAdminDAO;
import com.ftd.osp.emailadmin.vo.EmailSectionContentVO;
import com.ftd.osp.emailadmin.vo.EmailSectionsVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.security.exceptions.ExpiredIdentityException;
import com.ftd.security.exceptions.ExpiredSessionException;
import com.ftd.security.exceptions.InvalidSessionException;

import java.io.IOException;

import java.math.BigDecimal;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.StringTokenizer;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Handler that processes requests related to email section.
 * 
 * @author Doug Johnson
 */
public class EmailAdminSectionServlet extends EmailAdminBaseServlet
{
  private Logger logger;


  public void init(ServletConfig config) throws ServletException
  {
    super.init(config);
    this.logger = new Logger("com.ftd.osp.emailadmin.servlet.EmailAdminSectionServlet");
  }

  /** Receives servlet get requests.  Not Used. */
  public void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException
  {
    logger.debug("Entering EmailAdminSectionServlet doGet...");
    doPost(request, response);
  }

  /**
   * Servlet doPost. Dispatches all requests related to section. The request action
   * is determined by the query string. Query String can take one of the following values:
   * - exit: User is on 'Create <Section>' or 'Update <Section>' page and clicks on 
   *         'Email Confirmation' link.
   * - create: User is on 'Create <Section>' page and hits 'Save' button.
   * - update: User is on 'Update <Section>' page and hits 'Save' button.
   * - createSelectExistingSection: User is on 'Create <Section>' page and reselects a section.
   * - updateSelectExistingSection: User is on 'Update <Section>' page and reselects a section.
   * - viewupdate: User is on search section results page and hits 'Update' button;
   *          or user has successfully created a section and should be dispatched to 'Update Section'.
   * - delete: User is on search section results page and hits Delete button.
   * - copy: User is on search section results page and hits Copy button.
   * 
   * @param HttpServletRequest
   * @param HttpServletResponse
   * @throw ServletException
   * @throw IOException
   */
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    logger.debug("Entering EmailAdminSectionServlet doPost...");

    logger.debug("Setting the character encoding of the request object to UTF-8");
    request.setCharacterEncoding("utf-8");

    
    String page = "";
    String securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN);
    
    try
    {
        response.setContentType(EmailAdminConstants.CONTENT_TYPE);
        ServletContext context = this.getServletContext();

        if (SECURITY_OFF || ServletHelper.isValidToken(request))
        {
          if(ServletHelper.isAuthorized(request)) {
            String formAction = request.getQueryString();
            logger.debug("formAction is:" + formAction);
            logger.debug("securitytoken is:" + securityToken);

            if(formAction.equals(EXIT))
            {
                exitSection(request, response);
                //page = path + START_PAGE;
            }
            else if(formAction.equals(CREATE) || formAction.equals(UPDATE))
            {
                createOrUpdateSection(request, response);
             }
             else if(formAction.equals(CREATE_SELECT_SECTION) || formAction.equals(UPDATE_SELECT_SECTION))
             {
                selectCreateOrUpdateSection(request, response);
             }
             else if(formAction.equals(VIEW_FOR_UPDATE))
             {
                  viewForUpdateSection(request, response);
             }
             else if(formAction.equals(DELETE))
             {
                  deleteSection(request, response);
             }
             else if(formAction.equals(COPY))
             {
                  copySection(request, response);
             }
            } else {
              // not authorized
              ServletHelper.sendErrorPage
                (request, response, errorStylesheet, new Exception(NOT_AUTHORIZED));
            }       
          } else {
              // Invalid security token
              page = ServletHelper.getExitPath(request);
              response.sendRedirect(page);          
          } 
      }
      catch(ExpiredIdentityException e )
      {
          logger.error(e);
          try {
              page = ServletHelper.getExitPath(request);
          } catch (Exception ex) {
              logger.error(ex);
          }
          response.sendRedirect(page);
      }
      catch(ExpiredSessionException e )
      {
          logger.error(e);
          try {
              page = ServletHelper.getExitPath(request);
          } catch (Exception ex) {
              logger.error(ex);
          }
          response.sendRedirect(page);
      }
      catch(InvalidSessionException e )
      {
          logger.error(e);
          try {
              page = ServletHelper.getExitPath(request);
          } catch (Exception ex) {
              logger.error(ex);
          }
          response.sendRedirect(page);
      }
      catch(Exception e)
      {
          logger.error(e);
          ServletHelper.sendErrorPage(request, response, errorStylesheet, e);
      }
  }

  /**
   * Handles create and update section. The procedure does the following:
   * 1. If create section, check if section name exists. Sends back user input
   *    data and error mesage is true.
   * 2. Create EmailSectionVO off the request.
   * 3. Create or update section depending on what the request is.
   * 4. Dispatches to 'Update <Section>' page.
   * 
   * Section type Subject and Guarantee have only text format; Header, Marketing
   * and Contact have both text and html types.
   */
  private void createOrUpdateSection(HttpServletRequest request, HttpServletResponse response) throws Exception{
        logger.debug("Entering createOrUpdateSection() ");
        String formAction = "";
        formAction = request.getQueryString();

        String path = getXslPath();
        HashMap pageData = new HashMap();
        String securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN);
        String securityContext = request.getParameter(HTML_PARAM_SECURITY_CONTEXT);
        pageData.put(HTML_PARAM_SECURITY_CONTEXT, securityContext);
        pageData.put(HTML_PARAM_SECURITY_TOKEN, securityToken);       
        
        Document responseDocument = DOMUtil.getDocument();


        String sectionType = request.getParameter("sectionType");
 

        String searchString = request.getParameter("searchString");
        EmailAdminDAO emailAdminDao = new EmailAdminDAO();

        String sectionName = request.getParameter("sectionName");  
        String page = ""; 

        // check if section name exists.
        if(formAction.equals(CREATE) && emailAdminDao.sectionNameExists(sectionName)) {
            String selectedSection = request.getParameter("ExistingSection");
            String selectedType = request.getParameter("selectedType");
            logger.debug("selectedType = " + selectedType);
            
            //if(!selectedSection.equals("-1"))
            //{
               pageData.put("subjectRow","AddressRow('" + sectionType + "')");
               pageData.put("sectionType",sectionType);
               if(selectedType.equals(""))
               {
                    selectedType = "Text";
               }

               // This decides whether to show 'text', 'html', or 'both'.
               pageData.put("showBoxes","loadview('" + selectedType + "')");
               pageData.put("selectedSection",selectedSection);

            //}   
            page = path + CREATE_SECTION_PAGE;
            pageData.put("displayType",selectedType);
            pageData.put("pageTitle", "FTD - Create " + sectionType);
            pageData.put("headerName", "Create " +  sectionType);
            pageData.put("createSection", "createSection('" + sectionType + "');");
  
            // build ERROR section of tree.     
            Document errorSection = ServletHelper.getDocumentWithAttribute("ERROR", "message", "Section Name Exists");
            responseDocument = (Document)rebuildCreateOrUpdateSectionDoc(request, emailAdminDao);
            DOMUtil.addSection(responseDocument, "pageData", "data", pageData,true);
            DOMUtil.addSection(responseDocument,errorSection.getElementsByTagName("ERROR"));

            if(selectedSection.equals("-1") || selectedSection.equals("-2")) {
                // remember the content.
                Element sectionElem = responseDocument.createElement("SECTION");
                Element sectionContentHtmlElem = responseDocument.createElement("SECTION_CONTENT_HTML");
                Element tagElement = null;
                sectionContentHtmlElem.appendChild(this.createTextNode("html_version", request.getParameter("htmlContent"), responseDocument, tagElement));
                sectionElem.appendChild(sectionContentHtmlElem);
            
                Element sectionContentTextElem = responseDocument.createElement("SECTION_CONTENT_TEXT");
                tagElement = null;
                sectionContentTextElem.appendChild(this.createTextNode("text_version", request.getParameter("textContent"), responseDocument, tagElement));
                sectionElem.appendChild(sectionContentTextElem);
                responseDocument.getDocumentElement().appendChild(sectionElem);       
            }
        } else {
            java.util.Date currentDate = new java.util.Date();
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            String scurrentDate = sdf.format(currentDate);
            String secToken = getSecurityToken(request);
            String updatedBy = ServletHelper.getMyId(secToken);
            String sectionDescription = request.getParameter("sectionDescription");

            //create section
            EmailSectionsVO emailSection = new EmailSectionsVO();

            emailSection.setUpdatedBy(updatedBy);
            emailSection.setSectionType(sectionType);
            emailSection.setSectionName(request.getParameter("sectionName"));
            emailSection.setSectionDescription(sectionDescription==null? "" : sectionDescription);
            emailSection.setEffectiveStartDate(request.getParameter("effectiveStartDate"));

            String effectiveEndDate = request.getParameter("effectiveEndDate");
            String sectionDefault = request.getParameter("selDefaultObj");
            if(sectionDefault != null && sectionDefault.equals("on")) {
                 sectionDefault = "Y";
            } else {
                 sectionDefault = "N";
            }

            if(sectionDefault.equals("N")) {
                 if(effectiveEndDate == null || effectiveEndDate.equals(""))
                 {
                      emailSection.setEffectiveEndDate(scurrentDate);
                 }
                 else
                 { 
                      emailSection.setEffectiveEndDate(effectiveEndDate);
                 }                    
             } else {
                 //I commented out this line because default section can now have enddates, emueller 4/12/04
                 //emailSection.setEffectiveEndDate(null);
                 emailSection.setEffectiveEndDate(effectiveEndDate);
                 
             }
                
             emailSection.setFromAddress(request.getParameter("FromAddr"));
             emailSection.setSectionDefault(sectionDefault);


             //check if using an existing section
             String textContent = "";
             String htmlContent = "";

             //use what is entered on screen and save
             textContent = request.getParameter("textContent");
             htmlContent = request.getParameter("htmlContent");
             logger.debug("Character Encoding of Http Request is " + request.getCharacterEncoding());
             logger.debug("Text Content :: " + textContent);
             logger.debug("HTML Content :: " + htmlContent);
             
             if(htmlContent.equals(""))
             {                  
                htmlContent = textContent;
                htmlContent = replaceBreaks(htmlContent);               
             }


             Collection sectionContents = new ArrayList();
             EmailSectionContentVO sectionContent = null;

             //build text version
             sectionContent = new EmailSectionContentVO();
             sectionContent.setContentType("text");
             sectionContent.setContentSectionOrderId(1);
             if(formAction.equals(UPDATE))
             {
                  sectionContent.setSectionId(new Long(request.getParameter("textContentId")).intValue());
             }

             //store from email address if subject
             if(sectionType.equals("Subject"))
             {
                  sectionContent.setContent(splitContent(emailSection.getFromAddress() + "~" + textContent));
             }
             else
             {
                  sectionContent.setContent(splitContent(textContent));
             }

             sectionContents.add(sectionContent);

// EMAIL_CONFIRMATION_BRANCH : If statement used to check for Guarentee, now it checks for subject
             //Only Subject does not have an HTML version.
             if(!sectionType.equals("Subject")  )
             {
                  //build html version
                  sectionContent = new EmailSectionContentVO();
                  sectionContent.setContentType("html");
                  sectionContent.setContentSectionOrderId(1);
                  sectionContent.setContent(splitContent(htmlContent));
                  sectionContents.add(sectionContent);
                  if(formAction.equals(UPDATE))
                  {
                       String htmlContentValue = request.getParameter("htmlContentId").toString();
                       long htmlContentId = -1;
                       if(htmlContentValue != null && htmlContentValue.length() > 0)
                       {
                          htmlContentId = new Long(htmlContentValue).longValue();                     
                       }
                       sectionContent.setSectionId(htmlContentId);
                  }
               }

               //build any future versions

               Iterator iterator = sectionContents.iterator();
               EmailSectionContentVO emailSectionContent = null;

               //for each content build 1 to 1 records
               for (int i = 0; iterator.hasNext(); i++)
               {
                   emailSectionContent = new EmailSectionContentVO();
                   emailSectionContent = (EmailSectionContentVO)iterator.next();

                   emailSection.setContentType(emailSectionContent.getContentType());

                   if(formAction.equals(CREATE) || emailSectionContent.getSectionId() < 0)
                   {
                        emailAdminDao.insertEmailSection(emailSection, emailSectionContent);
                    }
                    else if(formAction.equals(UPDATE))
                    {
                         emailSection.setSectionId(emailSectionContent.getSectionId());
                         emailAdminDao.updateEmailSection(emailSection, emailSectionContent);
                     }                                       
                }
                // back to update section screen
              String selectedType = "Both";
              if(sectionType.equals("Subject"))
              {
                selectedType = "Text";
              }
                
              logger.debug("selectedType = " + selectedType);            

               if(selectedType.equals(""))
               {
                    selectedType = "Text";
               }

                page = path + UPDATE_SECTION_PAGE;

                pageData.put("searchString",searchString);
                pageData.put("pageTitle", "FTD - Update " + sectionType);
                pageData.put("headerName", "Update " +  sectionType);
                pageData.put("displayType",selectedType);
                pageData.put("sectionType",sectionType);
                pageData.put("updateSection", "updateSection('" + sectionType + "');");
                pageData.put("subjectRow","AddressRow('" + sectionType + "')");
                pageData.put("showBoxes","loadview('Text')");
                  
                DOMUtil.addSection(responseDocument, "pageData", "data", pageData,true);

                responseDocument = buildAddresses(responseDocument);

                Document existingSections = emailAdminDao.getSectionByName(sectionType,sectionName);
                //Document existingSections = emailAdminDao.getSectionByName(sectionType,null);
                DOMUtil.addSection(responseDocument,existingSections.getElementsByTagName("SECTION"));

                Document sections = emailAdminDao.getSectionsByType(sectionType);
                DOMUtil.addSection(responseDocument,sections.getElementsByTagName("SECTIONS"));           
           }
           pageRedirect(request,response,responseDocument,page);
  }

  /**
   * Displays Copy Section page. Retrieves all data element off the source section except
   * for section name, section description, effective start date, effective end date.
   * 
   * @param HttpServletRequest
   * @param HttpServletResponse
   * @throw Exception
   */
  private void copySection(HttpServletRequest request, HttpServletResponse response) throws Exception{
        String formAction = "";
        formAction = request.getQueryString();

        String path = getXslPath();

        Document responseDocument = DOMUtil.getDocument();
        String sectionType = request.getParameter("sectionType");
        String searchString = null;
        EmailAdminDAO emailAdminDao = new EmailAdminDAO();

        String sectionName = request.getParameter("sectionName");  
        String page = path + CREATE_SECTION_PAGE;

        HashMap pageData = new HashMap();
        pageData.put(HTML_PARAM_SECURITY_CONTEXT,request.getParameter(HTML_PARAM_SECURITY_CONTEXT));
        pageData.put(HTML_PARAM_SECURITY_TOKEN,request.getParameter(HTML_PARAM_SECURITY_TOKEN));

        pageData.put("pageTitle", "FTD - Copy " + sectionType);
        pageData.put("headerName", "Copy " +  sectionType);
        pageData.put("createSection", "createSection('" + sectionType + "');");
        pageData.put("subjectRow","AddressRow('" + sectionType + "')");
        pageData.put("sectionType",sectionType);
        pageData.put("firstCopyRequest", "true");
        pageData.put("selectedSection", sectionName);

        String showbox = "Both";
        if(sectionType.equals("Subject"))
        {
          showbox = "Text";
        }
        
        pageData.put("showBoxes","loadview('"+showbox+"')");
        // send the search string for back link.
        searchString = ServletHelper.retrieveSessionAttribute(request, "searchString");
        pageData.put("searchString", searchString == null? "" : searchString);

        DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);

        //get existing sections by section type
        Document sections = emailAdminDao.searchSectionOption(sectionType,null); //use empty search string.
        DOMUtil.addSection(responseDocument,sections.getElementsByTagName("SECTIONS"));

        Element tagElement = null;
        responseDocument = buildAddresses(responseDocument);

        Document existingSections = emailAdminDao.getSectionByName(sectionType,sectionName);
        DOMUtil.addSection(responseDocument,existingSections.getElementsByTagName("SECTION"));
        pageRedirect(request,response,responseDocument,page);
  }
 
  /**
   * Handles remove section. Sections cannot be removed if it's not expired
   * or it's associated to an effective program
   * 
   * @param HttpServletRequest
   * @param HttpServletResponse
   * @throw Exception
   */
  private void deleteSection(HttpServletRequest request, HttpServletResponse response) throws Exception{
        String formAction = "";
        formAction = request.getQueryString();

        String path = getXslPath();

        Document responseDocument = DOMUtil.getDocument();
        String sectionType = request.getParameter("sectionType");
        
        String searchString = request.getParameter("searchString");
        EmailAdminDAO emailAdminDao = new EmailAdminDAO();

        String sectionName = request.getParameter("sectionName");  
        String page = path + SEARCH_SECTION_RESULTS_PAGE;
        String cannotDeleteSections = "";

        String selectedSections = "";
        selectedSections = request.getParameter("selectedSections");

        StringTokenizer st = new StringTokenizer(selectedSections,"~");

        while (st.hasMoreTokens())
        {
              long sectionId = new Long(st.nextToken()).longValue();
              try {
                  emailAdminDao.deleteEmailSection(sectionId);
              } catch (Exception e) {
                  if("CANNOT DELETE THE SPECIFIED SECTION".equalsIgnoreCase(e.getMessage())) {
                      // Find section name from section id.
                      cannotDeleteSections += "," + emailAdminDao.getSectionNameById(sectionId);
                  } else {
                      throw e;
                  }
              }
        }

        // Check if there are any section that cannot be deleted. If yes, remove the leading ','.
        if (cannotDeleteSections != null && cannotDeleteSections != "") {
            cannotDeleteSections = "Cannot Delete section(s) with active Email Confirmation Message or Source Code: " + cannotDeleteSections.substring(1);
        }
        //custom message from database
        page = path + SEARCH_SECTION_RESULTS_PAGE;

        HashMap pageData = new HashMap();
        String securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN);
        String securityContext = request.getParameter(HTML_PARAM_SECURITY_CONTEXT);
               
        Document sections = null;
        pageData.put(HTML_PARAM_SECURITY_CONTEXT,securityContext);
        pageData.put(HTML_PARAM_SECURITY_TOKEN,securityToken);
        pageData.put("pageTitle", "FTD - " + sectionType + " Search Results");
        pageData.put("headerName", "Section Search Results");
        pageData.put("SectionType", sectionType);
        pageData.put("cannotDeleteSections", cannotDeleteSections);
                  
        sections = emailAdminDao.searchSectionOption(sectionType,searchString);
        DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);
        DOMUtil.addSection(responseDocument,sections.getElementsByTagName("SECTIONS"));

        pageRedirect(request,response,responseDocument,page);
  } 

  /**
   * Handles viewForUpdate section.
   * 
   * @param HttpServletRequest
   * @param HttpServletResponse
   * @throw Exception
   */
  private void viewForUpdateSection(HttpServletRequest request, HttpServletResponse response) throws Exception{
        String formAction = "";
        formAction = request.getQueryString();

        String path = getXslPath();

        Document responseDocument = DOMUtil.getDocument();
        String sectionType = request.getParameter("sectionType");
        String searchString = request.getParameter("searchString");
        EmailAdminDAO emailAdminDao = new EmailAdminDAO();

        String sectionName = request.getParameter("sectionName");  
        String page = path +  UPDATE_SECTION_PAGE;
        String selectedType = request.getParameter("selectedType");
        logger.debug("selectedType = " + selectedType);            

        HashMap pageData = new HashMap();
        pageData.put(HTML_PARAM_SECURITY_CONTEXT,request.getParameter(HTML_PARAM_SECURITY_CONTEXT));
        pageData.put(HTML_PARAM_SECURITY_TOKEN,request.getParameter(HTML_PARAM_SECURITY_TOKEN));

        pageData.put("pageTitle", "FTD - Update " + sectionType);
        pageData.put("headerName", "Update " +  sectionType);
        pageData.put("displayType",selectedType);
        pageData.put("sectionType",sectionType);
        pageData.put("updateSection", "updateSection('" + sectionType + "');");
        pageData.put("subjectRow","AddressRow('" + sectionType + "')");
        pageData.put("showBoxes","loadview('Text')");
        // send the search string for back link.

        pageData.put("searchString", searchString == null? "" : searchString);                 
        DOMUtil.addSection(responseDocument, "pageData", "data", pageData,true);

        responseDocument = buildAddresses(responseDocument);

        Document existingSections = emailAdminDao.getSectionByName(sectionType,sectionName);
        DOMUtil.addSection(responseDocument,existingSections.getElementsByTagName("SECTION"));

        Document sections = emailAdminDao.getSectionsByType(sectionType);
        DOMUtil.addSection(responseDocument,sections.getElementsByTagName("SECTIONS"));
        
        pageRedirect(request,response,responseDocument,page);
  }

  /**
   * Handles refresh selection of section. This procedure is called when user
   * changes the section selection.
   * 
   * @param HttpServletRequest
   * @param HttpServletResponse
   * @throw Exception
   */
  private void selectCreateOrUpdateSection(HttpServletRequest request, HttpServletResponse response) throws Exception{
        logger.debug("Entering selectCreateOrUpdateSection() ");
        String formAction = "";
        formAction = request.getQueryString();
        //Document responseDocument = DOMUtil.getDocument();
        EmailAdminDAO emailAdminDao = new EmailAdminDAO();
        String path = getXslPath();
        String page = path + CREATE_SECTION_PAGE;

        //check if using an existing section
        String textContent = "";
        String htmlContent = "";
        String selectedSection = request.getParameter("ExistingSection");
        String selectedFromAddress = request.getParameter("FromAddr");
        String sectionType = request.getParameter("sectionType");
        String headerAction = request.getParameter("headerAction");

        //if(!selectedSection.equals("-1"))
        //{
            HashMap pageData = new HashMap();

            pageData.put(HTML_PARAM_SECURITY_CONTEXT,request.getParameter(HTML_PARAM_SECURITY_CONTEXT));
            pageData.put(HTML_PARAM_SECURITY_TOKEN,request.getParameter(HTML_PARAM_SECURITY_TOKEN));

            pageData.put("sectionType",sectionType);
            pageData.put("subjectRow","AddressRow('" + sectionType + "')");
 
            String selectedType = request.getParameter("selectedType");
            logger.debug("selectedType = " + selectedType);
            if(selectedType.equals(""))
            {
                  selectedType = "Both";
            }

             pageData.put("showBoxes","loadview('" + selectedType + "')");
             pageData.put("displayType",selectedType);
             pageData.put("selectedSection",selectedSection);

             if (headerAction != null && headerAction.equals("Copy")) {
                  page = path + CREATE_SECTION_PAGE;
                  pageData.put("pageTitle", "FTD - Copy " + sectionType);
                  pageData.put("headerName", "Copy " +  sectionType);
                  pageData.put("createSection", "createSection('" + sectionType + "');");
             }
             else if(formAction.equals(CREATE_SELECT_SECTION))
             {
                  page = path + CREATE_SECTION_PAGE;
                  pageData.put("pageTitle", "FTD - Create " + sectionType);
                  pageData.put("headerName", "Create " +  sectionType);
                  pageData.put("createSection", "createSection('" + sectionType + "');");
              } 
              else
              {
                  page = path + UPDATE_SECTION_PAGE;
                  pageData.put("pageTitle", "FTD - Update " + sectionType);
                  pageData.put("headerName", "Update " +  sectionType);
                  pageData.put("updateSection", "updateSection('" + sectionType + "');");
              }
              Document responseDocument = (Document)rebuildCreateOrUpdateSectionDoc(request, emailAdminDao);

              DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);       
              pageRedirect(request,response,responseDocument,page);
          //}
  }

  /**
   * Build the document when create section errors out or selection changes.
   * 
   * @param HttpServletRequest
   * @param EmailAdminDAO
   * @throw Exception
   */
  private Document rebuildCreateOrUpdateSectionDoc(HttpServletRequest request, EmailAdminDAO emailAdminDao) throws Exception {
      String sectionType = request.getParameter("sectionType");
      String selectedSection = request.getParameter("ExistingSection");
     
      Document responseDocument = DOMUtil.getDocument();
      //get existing sections by section type
      Document sections = emailAdminDao.getSectionsByType(sectionType);
      DOMUtil.addSection(responseDocument,sections.getElementsByTagName("SECTIONS"));

      Element tagElement = null;

      responseDocument = buildAddresses(responseDocument);

      //populate with selected section and send back to screen
      Document sectionContent = emailAdminDao.getSectionContentByName(sectionType,selectedSection);

      DOMUtil.addSection(responseDocument,sectionContent.getElementsByTagName("SECTION"));


      Document newSection = DOMUtil.getDefaultDocument();
      Element sectionElement = newSection.createElement("SECTION");
      newSection.appendChild(sectionElement);

      sectionElement.appendChild(this.createTextNode("sectionType", sectionType, newSection, tagElement));
      sectionElement.appendChild(this.createTextNode("section_name", request.getParameter("sectionName"), newSection, tagElement));
      sectionElement.appendChild(this.createTextNode("description", request.getParameter("sectionDescription"), newSection, tagElement));
      sectionElement.appendChild(this.createTextNode("start_date", request.getParameter("effectiveStartDate"), newSection, tagElement));
      sectionElement.appendChild(this.createTextNode("end_date", request.getParameter("effectiveEndDate"), newSection, tagElement));
      sectionElement.appendChild(this.createTextNode("section_default", request.getParameter("selDefault"), newSection, tagElement));

      DOMUtil.addSection(responseDocument,newSection.getElementsByTagName("SECTION"));
      return responseDocument;   
  }
  
  /**
   * Displays the Email Confirmation home page.
   * 
   * @param HttpServletRequest
   * @param HttpServletResponse
   * @throw Exception
   */
  private void exitSection(HttpServletRequest request, HttpServletResponse response) throws Exception{
        Document responseDocument = DOMUtil.getDocument();
        String path = getXslPath();
        String page = path + START_PAGE;

        HashMap pageData = new HashMap();
        pageData.put(HTML_PARAM_SECURITY_TOKEN, request.getParameter(HTML_PARAM_SECURITY_TOKEN));
        pageData.put(HTML_PARAM_SECURITY_CONTEXT, request.getParameter(HTML_PARAM_SECURITY_CONTEXT)); 
        DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);
        pageRedirect(request,response,responseDocument,page);
  }

  /**
   * Split section content string into chunks of String array, each String with maximum
   * 2000 characters.
   * 
   * @param String input section content
   * @return String[] array of strings with maximum 2000 characters.
   * @throw Exception
   */
   private String[] splitContent(String contentText) throws Exception
   {
        //process email sections into subsets of char(2000) chunks for database storage
        BigDecimal messageLength = new BigDecimal(new Long(contentText.length()).toString());
        BigDecimal diff = new BigDecimal("0");
        BigDecimal chunk = new BigDecimal("2000");
        diff = messageLength.divide(chunk,BigDecimal.ROUND_UP);

        String emailContent[] = new String[diff.intValue()];

        char[] cbuf = new char[2000];

        int startpos = 0;
        int endpos = 2000;

        BigDecimal startValue = new BigDecimal("0");
        BigDecimal endValue = new BigDecimal("2000");
        BigDecimal incValue = new BigDecimal("2000");

        for(int i=0; i < emailContent.length;i++)
        {
              if(i == (emailContent.length - 1))
             {

                contentText.getChars(startpos,contentText.length(),cbuf,0);
             }
             else
             {
                contentText.getChars(startpos,endpos,cbuf,0);
             }

             emailContent[i] = new String(cbuf);

             BigDecimal d2 = startValue.add(incValue);
             startValue = d2;
             startpos = d2.intValue();

             d2 = endValue.add(incValue);
             endValue = d2;
             endpos = d2.intValue();
             cbuf = new char[2000];
        }

        // REMOVE LATER ;~
        for (int i = 0; i < emailContent.length; i++)  {
          logger.debug(emailContent[i]);
        }
        
        return emailContent;
   }

  /**
  * Returns real xsl path.
  * 
  * @return String real xsl path.
  * @throw Exception
  */
  private String getXslPath() throws Exception{
      ServletContext context = this.getServletContext();
      return context.getRealPath("/xsl") + java.io.File.separator;   
  }

  /*
   * Replace carriage returns with html break
   */
  private String replaceBreaks(String orig)
  {
    String workString = "";
    String finalString = "";

    StringTokenizer tokenizer1 = new StringTokenizer(orig, "\r\n");

    while (tokenizer1.hasMoreTokens()) {    
      if(finalString.length() > 0)
      {
        finalString = finalString + "<BR>";
      }
       finalString = finalString + tokenizer1.nextToken();    // process the line
    }

    //if finalstring is empty then no carriage returns exists
    if(finalString.length() <=0)
    {
      finalString = orig;
    }

    
    return finalString;
  }
 }





