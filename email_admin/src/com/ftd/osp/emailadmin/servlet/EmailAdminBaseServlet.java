package com.ftd.osp.emailadmin.servlet;
import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.OutputKeys;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.ftd.osp.emailadmin.DataRequestHelper;
import com.ftd.osp.emailadmin.EmailAdminDAO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.order.MercentOrderPrefixes;
import com.ftd.osp.utilities.order.vo.MercentChannelMappingVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

/**
 * Base servlet.
 * 
 * @author Doug Johnson
 */
public class EmailAdminBaseServlet extends HttpServlet implements EmailAdminConstants
{
  private Logger logger;
  protected final static String CREATE_SELECT_SECTION = "createSelectExistingSection";
  protected final static String UPDATE_SELECT_SECTION = "updateSelectExistingSection";
  protected final static String COPY_SELECT_SECTION = "copySelectExistingSection";
  private final static String LOGGER_CATEGORY = "com.ftd.osp.emailadmin.servlet.EmailAdminBaseServlet";
  private final static String _EMAIL_MAILBOX_USER = "_mailbox_monitor_USERNAME";
  protected static File errorStylesheet;

  public void init(ServletConfig config) throws ServletException {
      try {
          super.init(config);
          logger = new Logger(LOGGER_CATEGORY);
          errorStylesheet = new File(getServletContext().getRealPath(ERROR_PAGE));
      } catch (ServletException e) {
          logger.error(e);
          throw e;
      } catch (Exception e) {
          throw new ServletException("\nException caught in base servlet init");
      }
  }

  /**
   * Builds the subtree to display the from address in the subject section
   * and add the subtree to the given document. The from addresses are not
   * stored in the DB. So whenever there's a change to from address, this
   * procedure needs to be updated.
   * 
   * @param Document responseDocument the original document
   * @return Document the resulting document.
   * @throw Exception
   */
  protected Document buildAddresses(Document responseDocument) throws Exception
  {
        Element tagElement = null;
        Document addresses = DOMUtil.getDefaultDocument();
        Element addressesElement = addresses.createElement("ADDRESSES");
        addresses.appendChild(addressesElement);
        
        Element addressElement = addresses.createElement("ADDRESS");
        addressesElement.appendChild(addressElement);
        addressElement.appendChild(this.createTextNode("FROM_ADDDRESS_ID", FTDI_ADDRESS, addresses, tagElement));
        addressElement.appendChild(this.createTextNode("FROM_ADDRESS", FTDI_ADDRESS, addresses, tagElement));
       
        addressElement = addresses.createElement("ADDRESS");
        addressesElement.appendChild(addressElement);
        addressElement.appendChild(this.createTextNode("FROM_ADDDRESS_ID", SFBM_ADDRESS, addresses, tagElement));
        addressElement.appendChild(this.createTextNode("FROM_ADDRESS", SFBM_ADDRESS, addresses, tagElement));
       
        addressElement = addresses.createElement("ADDRESS");
        addressesElement.appendChild(addressElement);
        addressElement.appendChild(this.createTextNode("FROM_ADDDRESS_ID", GIFTSENSE_ADDRESS, addresses, tagElement));
        addressElement.appendChild(this.createTextNode("FROM_ADDRESS", GIFTSENSE_ADDRESS, addresses, tagElement));
       
        addressElement = addresses.createElement("ADDRESS");
        addressesElement.appendChild(addressElement);
        addressElement.appendChild(this.createTextNode("FROM_ADDDRESS_ID", BUTTERFIELDBLOOMS_ADDRESS, addresses, tagElement));
        addressElement.appendChild(this.createTextNode("FROM_ADDRESS", BUTTERFIELDBLOOMS_ADDRESS, addresses, tagElement));   
        
        addressElement = addresses.createElement("ADDRESS");  
        addressesElement.appendChild(addressElement);
        addressElement.appendChild(this.createTextNode("FROM_ADDDRESS_ID", FUSA_ADDRESS, addresses, tagElement));
        addressElement.appendChild(this.createTextNode("FROM_ADDRESS", FUSA_ADDRESS, addresses, tagElement));
        
        addressElement = addresses.createElement("ADDRESS");
        addressesElement.appendChild(addressElement);
        addressElement.appendChild(this.createTextNode("FROM_ADDDRESS_ID", FDIRECT_ADDRESS, addresses, tagElement));
        addressElement.appendChild(this.createTextNode("FROM_ADDRESS", FDIRECT_ADDRESS, addresses, tagElement));
        
        addressElement = addresses.createElement("ADDRESS");
        addressesElement.appendChild(addressElement);
        addressElement.appendChild(this.createTextNode("FROM_ADDDRESS_ID", FLORIST_ADDRESS, addresses, tagElement));
        addressElement.appendChild(this.createTextNode("FROM_ADDRESS", FLORIST_ADDRESS, addresses, tagElement));   
        
        addressElement = addresses.createElement("ADDRESS");
        addressesElement.appendChild(addressElement);
        addressElement.appendChild(this.createTextNode("FROM_ADDDRESS_ID", FTDCA_ADDRESS, addresses, tagElement));
        addressElement.appendChild(this.createTextNode("FROM_ADDRESS", FTDCA_ADDRESS, addresses, tagElement));   

        addressElement = addresses.createElement("ADDRESS");
        addressesElement.appendChild(addressElement);
        addressElement.appendChild(this.createTextNode("FROM_ADDDRESS_ID", ROSES_ADDRESS, addresses, tagElement));
        addressElement.appendChild(this.createTextNode("FROM_ADDRESS", ROSES_ADDRESS, addresses, tagElement));   
        
		Connection conn = null;
		try {
			conn = DataRequestHelper.getInstance().getDBConnection();
			MercentOrderPrefixes mop = new MercentOrderPrefixes(conn);
			List<MercentChannelMappingVO> mercentPartnerDetail = mop.getMercentChannelDetails();
			
			StringBuffer fromEmailAddress = null;
			String domain = ConfigurationUtil.getInstance().getFrpGlobalParm("EMAIL_ADMIN_CONFIG", "MERCENT_CUSTSERV_EMAIL_DOMAIN");
			final String mercent = "MERCENT ";
			StringBuffer mercentConfigParam = new StringBuffer();
			String secureParamValue = null;
			for (MercentChannelMappingVO mercentChannelMappingVO : mercentPartnerDetail) {
				if ("Y".equals(mercentChannelMappingVO.getSendConfirmationEmail()) && mercentChannelMappingVO.getChannelName() != null) {
					
					mercentConfigParam.replace(0, mercentConfigParam.length(), mercent);					
					mercentConfigParam.append(mercentChannelMappingVO.getChannelName()).append(_EMAIL_MAILBOX_USER);
					
					if(logger.isDebugEnabled()) {
						logger.debug("mercentConfigParam: " + mercentConfigParam);
					}
					
					if(mercentConfigParam != null && mercentConfigParam.length() > 0) {
						
						secureParamValue = ConfigurationUtil.getInstance().getSecureProperty("email_request_processing", mercentConfigParam.toString());
						
						if(logger.isDebugEnabled()) {
							logger.debug("mercentConfigParam value: " + secureParamValue);
						}
						
						if(secureParamValue != null && secureParamValue.length() > 0) {
							fromEmailAddress = new StringBuffer(secureParamValue).append("@").append(domain);
							
							if(fromEmailAddress != null && fromEmailAddress.length() > 0) {
								
								if(logger.isDebugEnabled() && fromEmailAddress != null) {
									logger.debug("From Email Address: " + fromEmailAddress.toString());
								}
								
								addressElement = addresses.createElement("ADDRESS");
								addressesElement.appendChild(addressElement);
								addressElement.appendChild(this.createTextNode("FROM_ADDDRESS_ID", fromEmailAddress.toString(), addresses, tagElement));
								addressElement.appendChild(this.createTextNode("FROM_ADDRESS", fromEmailAddress.toString(), addresses, tagElement));
							}
						}
					}
				}
			}
		} catch (Exception e) {
			logger.error("Unable to add Partner Address : ", e);
		} finally {
			try {
				if(conn != null && !conn.isClosed()) {
					conn.close();				
				}
			} catch (SQLException e) {
				logger.error("buildAddresses - unable to close the connection.", e);
			}
		}

        DOMUtil.addSection(responseDocument,addresses.getElementsByTagName("ADDRESSES"));
        
        return responseDocument;
  }

/**
   * Builds the subtree to display all the available sections of all types
   * and add the subtree to the given document. 
   * 
   * @param EmailAdminDAO
   * @param Document the given document
   * @return Document the resulting document.
   * @throw Exception
   */
  protected Document buildAllSectionDropDowns(EmailAdminDAO emailAdminDao,Document responseDocument) throws Exception
  {
        Document sections = null;
   
        //////////////
        //get exisiting subject sections
        Document subjects = DOMUtil.getDefaultDocument();
        Element subjectElement = subjects.createElement(SUBJECT);
        subjects.appendChild(subjectElement);
        sections = emailAdminDao.getSectionsByType(SUBJECT);
        DOMUtil.addSection(subjects,sections.getElementsByTagName("SECTIONS"));
        DOMUtil.addSection(responseDocument,subjects.getElementsByTagName(SUBJECT));
        //////////////

        //////////////
        //get exisiting header sections
        Document headers = DOMUtil.getDefaultDocument();
        Element headerElement = headers.createElement(HEADER);
        headers.appendChild(headerElement);
        sections = emailAdminDao.getSectionsByType(HEADER);
        DOMUtil.addSection(headers,sections.getElementsByTagName("SECTIONS"));
        DOMUtil.addSection(responseDocument,headers.getElementsByTagName(HEADER));
        //////////////

        //////////////
        //get exisiting guarantee sections
        Document guarantee = DOMUtil.getDefaultDocument();
        Element guaranteeElement = guarantee.createElement(GUARANTEE);
        guarantee.appendChild(guaranteeElement);
        sections = emailAdminDao.getSectionsByType(GUARANTEE);
        DOMUtil.addSection(guarantee,sections.getElementsByTagName("SECTIONS"));
        DOMUtil.addSection(responseDocument,guarantee.getElementsByTagName(GUARANTEE));
        //////////////

        //////////////
        //get exisiting marketing sections
        Document marketing = DOMUtil.getDefaultDocument();
        Element marketingElement = marketing.createElement(MARKETING);
        marketing.appendChild(marketingElement);
        sections = emailAdminDao.getSectionsByType(MARKETING);
        DOMUtil.addSection(marketing,sections.getElementsByTagName("SECTIONS"));
        DOMUtil.addSection(responseDocument,marketing.getElementsByTagName(MARKETING));
        //////////////

        //////////////
        //get exisiting contact us sections
        Document contactUs = DOMUtil.getDefaultDocument();
        Element contactUsElement = contactUs.createElement(CONTACT);
        contactUs.appendChild(contactUsElement);
        sections = emailAdminDao.getSectionsByType(CONTACT);
        DOMUtil.addSection(contactUs,sections.getElementsByTagName("SECTIONS"));
        DOMUtil.addSection(responseDocument,contactUs.getElementsByTagName(CONTACT));
        //////////////            

        return responseDocument;
  }

  /**
   * Transforms the page and sends back response.
   * 
   * @param HttpServletRequest 
   * @param HttpServletResponse
   * @pageDate Document that carries response data
   * @page XSL file to be transformed.
   * @throw Exception
   */
  protected void pageRedirect(HttpServletRequest request, HttpServletResponse response, Document pageData, String page) throws Exception
  {
    //parameters not needed for XML to XSL transformation, use empty object
    Map parameters = new HashMap();

    //build page
    File styleSheet = new File(page);
    TraxUtil traxUtil = TraxUtil.getInstance(); 
    
    /*
    String docString = DOMUtil.convertToString(pageData);
    pageData = DOMUtil.getDocument(new ByteArrayInputStream(docString.getBytes("UTF-8")));
    */
    
    parameters.put(OutputKeys.ENCODING, "UTF-8");    
    traxUtil.transform(request,response,pageData,styleSheet,parameters); 
  }

  /**
   * Create a TextNode for the given document as child of the give element.
   * 
   * @param String elementName name of the node to create
   * @param String elementValue value of the text node
   * @param Document document that creates the node.
   * @param Element parent of the text node to create.
   */
  protected Element createTextNode(String elementName, String elementValue, Document document, Element tagElement) throws Exception
  {
      tagElement = document.createElement(elementName);
      if(elementValue != null)
      {
          tagElement.appendChild(document.createTextNode(elementValue));
      }

      return tagElement;
  } 


  /**
   * Returns security token.
   */
  protected String getSecurityToken(HttpServletRequest request) throws Exception{
      String  securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN);
      return securityToken;
  }  
  
  protected HashMap getSecurityData(HttpServletRequest request) throws Exception {
      HashMap securityData = new HashMap();
      String  securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN);
      String context = request.getParameter(HTML_PARAM_SECURITY_CONTEXT);
      securityToken = securityToken == null? "" : securityToken;
      context = context == null? "" : context;
      securityData.put(HTML_PARAM_SECURITY_TOKEN, securityToken);
      securityData.put(HTML_PARAM_SECURITY_CONTEXT, context);
      
      return securityData;
  }

}
   
