package com.ftd.osp.emailadmin.servlet;

import com.ftd.messagegenerator.bo.OrderEmailGeneratorBO;
import com.ftd.osp.emailadmin.DataRequestHelper;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.security.exceptions.ExpiredIdentityException;
import com.ftd.security.exceptions.ExpiredSessionException;
import com.ftd.security.exceptions.InvalidSessionException;
import com.ftd.osp.utilities.email.*;

import java.io.IOException;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;

import org.w3c.dom.Element;

/**
 * Servlet that handles preview email.
 * 
 * @author Doug Johnson
 */
public class PreviewEmailServlet extends EmailAdminBaseServlet
{
    private Logger logger;
    //private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    private static String TEST_ORDER = "";

    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        this.logger = new Logger("com.ftd.osp.emailadmin.servlet.PreviewEmailServlet");
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
      logger.debug("Entering PreviewEmailServlet doGet...");
      try {
        response.setContentType(EmailAdminConstants.CONTENT_TYPE);
        this.loadPreviewEmail(request, response);
      } catch (Exception e) {
          logger.error(e);
          ServletHelper.sendErrorPage(request, response, errorStylesheet, e);
      }
    }

  /**
   * Calls loadPreviewEmail to send email preview message.
   * 
   * @param HttpServletRequest
   * @param HttpServletResponse
   * @throw ServletException
   * @throw IOException
   */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
      logger.debug("Entering PreviewEmailServlet doPost...");
      try {
        response.setContentType(EmailAdminConstants.CONTENT_TYPE);
        this.loadPreviewEmail(request, response);
      } catch (Exception e) {
          logger.error(e);
          ServletHelper.sendErrorPage(request, response, errorStylesheet, e);
      }      
    }

  /**
   * Sends email preview using the program defined.
   * 
   * @param HttpServletRequest
   * @param HttpServletResponse
   * @throw ServletException
   * @throw IOException
   */
    private void loadPreviewEmail(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, Exception
    {
       logger.debug("Entering loadPreviewEmail...");
       Connection con = null;    
       String page = "";
       String securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN);

       try {

        // Gets the order GUID from configration file. This determines
        // the order details included in the preview email.
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
		TEST_ORDER = ConfigurationUtil.getInstance().getFrpGlobalParm("EMAIL_ADMIN_CONFIG", "PREVIEW_ORDER_GUID");

        String securityContext = null;

        boolean messageSent = true;
        String formAction = "";
        formAction = request.getParameter("actionMethod");
        String programId = request.getParameter("programId");       

        // The preview, update preview, create preview and copy preview in
        // program servlet is responsible for saving the program and
        // renders the preview page. When user hits the 'Send' button on the
        // preview popup, the query string value is 'preview' regardless
        // of which page the preview is invoked from.
            if(formAction != null && formAction.equals(ONLY_PREVIEW))
            {
                String runDate = "";
                runDate = request.getParameter("runDate");

                String toAddresses = "";
                toAddresses = request.getParameter("toAddresses");

                List addressArray = getAddressArray(toAddresses);

                String comments = "";
                comments = request.getParameter("additionalComments");
                comments = comments == null? "" : (comments + "\n\n");
                
                // Send a message for each to address.
                for (int i=0; i<addressArray.size(); i++) {  
                    con = DataRequestHelper.getInstance().getDBConnection();
                    OrderEmailGeneratorBO buildOrderEmail = new OrderEmailGeneratorBO(con);
                    //buildOrderEmail.generateOrderEmail(new Integer(programId).intValue(),runDate,TEST_ORDER,toAddresses);
                    buildOrderEmail.generatePreviewEmail(new Long(programId).longValue(),runDate,TEST_ORDER,(String)addressArray.get(i),comments);

                    if (con != null) {
                        con.close();
                    }
                }
            }

            ServletContext context = this.getServletContext();
            String path = context.getRealPath("/xsl") + java.io.File.separator ;
            page = path + PREVIEW_EMAIL;

            // Create the initial document
            Document responseDocument =  DOMUtil.getDocument();
            Element tagElement = null;            
            Document newProgram = DOMUtil.getDefaultDocument();
            Element programElement = newProgram.createElement("PROGRAM");
            newProgram.appendChild(programElement);
            programElement.appendChild(this.createTextNode("program_id", new Long(programId).toString(), newProgram, tagElement));
            DOMUtil.addSection(responseDocument,newProgram.getElementsByTagName("PROGRAM"));            


            // Message sent. Close window. 
            responseDocument.getDocumentElement().setAttribute("messageSentFlag", "true");

            pageRedirect(request,response,responseDocument,page);
      }
      catch(ExpiredIdentityException e )
      {
          logger.error(e);
          // The request is sent from a popup page. Flag to close window when error occurs.
          request.setAttribute("isPopup", "true");
          ServletHelper.sendErrorPage(request, response, errorStylesheet, e);
      }
      catch(ExpiredSessionException e )
      {
          logger.error(e);
          request.setAttribute("isPopup", "true");
          ServletHelper.sendErrorPage(request, response, errorStylesheet, e);
      }
      catch(InvalidSessionException e )
      {
          logger.error(e);
          request.setAttribute("isPopup", "true");
          ServletHelper.sendErrorPage(request, response, errorStylesheet, e);
      }
      catch(Exception e)
      {
          logger.error(e);
          request.setAttribute("isPopup", "true");
          ServletHelper.sendErrorPage(request, response, errorStylesheet, e);
      }
      finally {
          if (con != null) {
              con.close();
          }
      }
    }

    /**
     * Tokenizes semi-colon delimited addresses to a list of addresses.
     * @param String addresses semi-colon delimited addresses
     * @return List address list
     * @throw Exception
     */
    private List getAddressArray(String addresses) throws Exception {
        List addressList = new ArrayList();
        StringTokenizer st = new StringTokenizer(addresses, ";");
        while(st.hasMoreTokens()) {
            addressList.add(st.nextToken());
        }
        return addressList;
    }
    
}
