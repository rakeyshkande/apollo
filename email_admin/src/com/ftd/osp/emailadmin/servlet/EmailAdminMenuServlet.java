package com.ftd.osp.emailadmin.servlet;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.security.exceptions.ExpiredIdentityException;
import com.ftd.security.exceptions.ExpiredSessionException;
import com.ftd.security.exceptions.InvalidSessionException;

import java.io.IOException;

import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;

/**
 * Controller servlet that is responsible for transforming the appropriate page.
 * 
 * @author Doug Johnson
 */
public class EmailAdminMenuServlet extends EmailAdminBaseServlet
{
  private Logger logger;

  public void init(ServletConfig config) throws ServletException
  {
    super.init(config);
    this.logger = new Logger("com.ftd.osp.emailadmin.servlet.EmailAdminMenuServlet");
  }

  /** Receives servlet get requests.  Not Used. */
  public void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException
  {
      doPost(request, response);
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
      logger.debug("Entering EmailAdminMenu doPost...");
      String page = "";
      String securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN);
      String securityContext = request.getParameter(HTML_PARAM_SECURITY_CONTEXT);
       
      try
      {
          response.setContentType(EmailAdminConstants.CONTENT_TYPE);
          ServletContext context = this.getServletContext();

          if (SECURITY_OFF || ServletHelper.isValidToken(request))
          {
            if(ServletHelper.isAuthorized(request)) {
              Document responseDocument = DOMUtil.getDocument();
              HashMap pageData = new HashMap();
              pageData.put(HTML_PARAM_SECURITY_TOKEN, securityToken);
              pageData.put(HTML_PARAM_SECURITY_CONTEXT, securityContext);
                 
              DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);

              String menuAction = request.getParameter(MENU_ACTION);

              if(menuAction == null || menuAction.equals("exit")) {
                  page = ServletHelper.getExitPath(request);
                  response.sendRedirect(page);
              } else {
                  page = context.getRealPath("/xsl") + java.io.File.separator + menuAction + ".xsl";
                  pageRedirect(request,response,responseDocument,page);
              }            
            } else {
              // not authorized
              ServletHelper.sendErrorPage
                (request, response, errorStylesheet, new Exception(NOT_AUTHORIZED));
            }
          } else {
              // Invalid security token
              page = ServletHelper.getExitPath(request);
              response.sendRedirect(page);          
          }          
      }
      catch(ExpiredIdentityException e )
      {
          logger.error(e);
          try {
              page = ServletHelper.getExitPath(request);
          } catch (Exception ex) {
              logger.error(ex);
          }
          response.sendRedirect(page);
      }
      catch(ExpiredSessionException e )
      {
          logger.error(e);
          try {
              page = ServletHelper.getExitPath(request);
          } catch (Exception ex) {
              logger.error(ex);
          }
          response.sendRedirect(page);
      }
      catch(InvalidSessionException e )
      {
          //logon error
          logger.error(e);
          try {
              page = ServletHelper.getExitPath(request);
          } catch (Exception ex) {
              logger.error(ex);
          }
          response.sendRedirect(page);
      }
      catch(Exception e)
      {
          logger.error(e);
          ServletHelper.sendErrorPage(request, response, errorStylesheet, e);
      }
  }
}
