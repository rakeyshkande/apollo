package com.ftd.osp.emailadmin.servlet;
import com.ftd.osp.emailadmin.EmailAdminDAO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.security.exceptions.ExpiredIdentityException;
import com.ftd.security.exceptions.ExpiredSessionException;
import com.ftd.security.exceptions.InvalidSessionException;

import java.io.IOException;
import java.io.PrintWriter;

import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;

import org.w3c.dom.Element;

/**
 * Controller servlet which determines which page to go to from Email Confirmation
 * Home page.
 * 
 * @author Doug Johnson
 */
public class EmailAdminSelectionServlet extends EmailAdminBaseServlet
{
  private Logger logger;

  private final static String EXIT = "exit";
  private final static String NEW = "new";
  private final static String PROGRAM = "Program";
  private final static String SEARCH = "search";
  private final static String SOURCE_CODE = "Source Code";

  public void init(ServletConfig config) throws ServletException
  {
    super.init(config);
    this.logger = new Logger("com.ftd.osp.emailadmin.servlet.EmailAdminSelectionServlet");
  }

  /** Receives servlet get requests.  Not Used. */
  public void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException
  {
      response.setContentType(EmailAdminConstants.CONTENT_TYPE);
      PrintWriter out = response.getWriter();
      out.println("<html>");
      out.println("<head><title>Servlet</title></head>");
      out.println("<body>");
      out.println("<p>The " + this.toString() +
              " servlet has received a GET. This is the reply.</p>");
      out.println("</body></html>");
      out.close();
  }

  /**
   * Handles request sent with Post method. Dispatches to appropriate page from
   * the Email Confirmation home page.
   * 
   * @param HttpServletRequest
   * @param HttpServletResponse
   * @throw ServletException
   * @throw IOException
   */
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
   logger.debug("Entering EmailAdminSelection doPost...");
   String securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN);
   String securityContext = request.getParameter(HTML_PARAM_SECURITY_CONTEXT);
   String page = "";
   try
   {
      response.setContentType(EmailAdminConstants.CONTENT_TYPE);
      ServletContext context = this.getServletContext();
      
    
      if (SECURITY_OFF || ServletHelper.isValidToken(request))
      {
        if(ServletHelper.isAuthorized(request)) {
          HashMap pageData = new HashMap();
          pageData.put(HTML_PARAM_SECURITY_TOKEN,securityToken);
          pageData.put(HTML_PARAM_SECURITY_CONTEXT, securityContext);
      
          String formAction = "";
          formAction = request.getQueryString();

          String option = "";
          option = request.getParameter("option");
          pageData.put("option", option);
          
          String path = context.getRealPath("/xsl") + java.io.File.separator ;

          Element tagElement = null;

          // Create the initial document
          Document responseDocument = DOMUtil.getDocument();
          EmailAdminDAO emailAdminDao = new EmailAdminDAO();


          if(formAction.equals(EXIT))
          {
              page = path + MENU_PAGE;
              DOMUtil.addSection(responseDocument, "pageData", "data", pageData,true);
          }
          else if(formAction.equals(NEW))
          {
              if(option.equals(PROGRAM))
              {
                  page = path + CREATE_PROGRAM_PAGE;

                  //pageData.put(HTML_PARAM_SECURITY_CONTEXT,securityContext);
                  pageData.put(HTML_PARAM_SECURITY_TOKEN,securityToken);
                  pageData.put("pageTitle", "FTD - Create Program");
                  pageData.put("headerName", "Create Program");
                  pageData.put("mode", "'create'");
                  pageData.put("previewmode", "'createpreview'");
                  pageData.put("pagemode", "create");

                  DOMUtil.addSection(responseDocument, "pageData", "data", pageData,true);

                  String searchString = null;
                  // When user chooses new program, by default they are creating a custom program.
                  // Get all sources, providing no program id.
                  String sourceFlag = "Y";
                  Document sources = emailAdminDao.getSources(sourceFlag, null);
                  DOMUtil.addSection(responseDocument,sources.getElementsByTagName("SOURCES"));

                  //wrap all section types to traverse
                  responseDocument = buildAllSectionDropDowns(emailAdminDao,responseDocument);
              }
              else //section
              {
                  page = path + CREATE_SECTION_PAGE;

                  //pageData.put(HTML_PARAM_SECURITY_CONTEXT,securityContext);
                  pageData.put(HTML_PARAM_SECURITY_TOKEN,securityToken);
                  pageData.put("pageTitle", "FTD - Create " + option);
                  pageData.put("headerName", "Create " +  option);
                  pageData.put("createSection", "createSection('" + option + "');");
                  pageData.put("subjectRow","AddressRow('" + option + "')");

                  if(option.equals("Subject")){
                      pageData.put("showBoxes","loadview('Text')");
                  }else
                  {
                    pageData.put("showBoxes","loadview('Both')");
                  }
                  
                  pageData.put("sectionType",option);

                  DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);

                  //get existing sections by section type
                  String searchString = null;
                  Document sections = emailAdminDao.getSectionsByType(option);
                  DOMUtil.addSection(responseDocument,sections.getElementsByTagName("SECTIONS"));

                  responseDocument = buildAddresses(responseDocument);

                  Document newSection = DOMUtil.getDefaultDocument();
                  Element sectionElement = newSection.createElement("SECTION");
                  newSection.appendChild(sectionElement);
                  sectionElement.appendChild(this.createTextNode("sectionType", option, newSection, tagElement));
                  DOMUtil.addSection(responseDocument,newSection.getElementsByTagName("SECTION"));
              }
          }
          else if(formAction.equals(SEARCH))
          {
              //pageData.put(HTML_PARAM_SECURITY_CONTEXT,securityContext);
              pageData.put(HTML_PARAM_SECURITY_TOKEN,securityToken);
              pageData.put("pageTitle", "FTD - " + option + " Search Results");
              Document sections = null;
              String searchString = request.getParameter("searchString");
              String searchStringx = request.getParameter("searchName");

              if(option.equals(SOURCE_CODE))
              {
                  page = path + SEARCH_PROGRAM_RESULTS_PAGE;

                  pageData.put("headerName", "Program for Source Code " + searchString);
                  pageData.put("SectionType", option);
                  pageData.put("searchString", searchString);
                  sections = emailAdminDao.searchProgramBySource(searchString);
              }
              else if(option.equals(PROGRAM))
              {
                  page = path + SEARCH_PROGRAM_RESULTS_PAGE;

                  pageData.put("headerName", option + " Search Results");
                  pageData.put("SectionType", option);
                  pageData.put("searchString", searchString);
                  sections = emailAdminDao.searchProgramByName(searchString);
              }
              else if(!option.equals(SOURCE_CODE) && !option.equals(PROGRAM))
              {
                  page = path + SEARCH_SECTION_RESULTS_PAGE;

                  pageData.put("headerName", option + " Search Results");
                  pageData.put("SectionType", option);
                  pageData.put("searchString", searchString);
                  sections = emailAdminDao.searchSectionOption(option,searchString);
              }

              DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);
              DOMUtil.addSection(responseDocument,sections.getElementsByTagName("SECTIONS"));
          }
          else if(formAction.equals(EXIT))
          {
              page = path + MENU_PAGE;

              pageData.put("exit", "exit");
              DOMUtil.addSection(responseDocument, "pageData", "data", pageData,true);
          }

          pageRedirect(request,response,responseDocument,page);
          } else {
              // not authorized
              ServletHelper.sendErrorPage
                (request, response, errorStylesheet, new Exception(NOT_AUTHORIZED));
          } 
          
      } else {
          // Invalid security token
          page = ServletHelper.getExitPath(request);
          response.sendRedirect(page);          
      }
    }
    catch(ExpiredIdentityException e )
    {
          logger.error(e);
          try {
              page = ServletHelper.getExitPath(request);
          } catch (Exception ex) {
              logger.error(ex);
          }
          response.sendRedirect(page);
    }
    catch(ExpiredSessionException e )
    {
          logger.error(e);
          try {
              page = ServletHelper.getExitPath(request);
          } catch (Exception ex) {
              logger.error(ex);
          }
          response.sendRedirect(page);
    }
    catch(InvalidSessionException e )
    {
          logger.error(e);
          try {
              page = ServletHelper.getExitPath(request);
          } catch (Exception ex) {
              logger.error(ex);
          }
          response.sendRedirect(page);
    }
    catch(Exception e)
    {
        logger.error(e);
        ServletHelper.sendErrorPage(request, response, errorStylesheet, e);
    }
  }
}






