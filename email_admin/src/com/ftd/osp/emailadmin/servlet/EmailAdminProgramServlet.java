
package com.ftd.osp.emailadmin.servlet;
import com.ftd.osp.emailadmin.EmailAdminDAO;
import com.ftd.osp.emailadmin.vo.EmailProgramVO;
import com.ftd.osp.emailadmin.vo.EmailSectionsVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.security.exceptions.ExpiredIdentityException;
import com.ftd.security.exceptions.ExpiredSessionException;
import com.ftd.security.exceptions.InvalidSessionException;
import java.util.List;
import java.io.*;

import java.text.SimpleDateFormat;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.StringTokenizer;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

/**
 * Handler that processes requests related to email program.
 * 
 * @author Doug Johnson
 */
public class EmailAdminProgramServlet extends EmailAdminBaseServlet
{
  private Logger logger;
  private static String ERROR_PROGRAM_NAME_EXISTS = "Program name exists.";
  //private static String ERROR_SECTION_HAS_END_DATE = "The following sections have effective end date: ";
  private static String ERROR_PROGRAM_DATE_OVERLAP = "Source codes cannot be updated because of program date overlap. Source code(s) in error ";
  private static String ERROR_PROGRAM_HAS_SOURCE = "The following program(s) cannot be deleted because they have associated source codes: ";
  private static String ERROR_SECTION_START_DATE_CLASH = "Program section start date clashes: ";
  private static String ERROR_INVALID_SOURCE_CODE = "The following source code(s) are invalid:";
  
  /**
   * Servlet initialization.
   * 
   * @param ServletConfig 
   * @throw Exception
   */
  public void init(ServletConfig config) throws ServletException
  {
    super.init(config);
    this.logger = new Logger("com.ftd.osp.emailadmin.servlet.EmailAdminProgramServlet");
  }

  /** Receives servlet get requests.  Not Used. */
  public void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException
  {
      this.processProgram(request,response);
  }

  /**
   * Servelt doPost.
   * 
   * @param HttpServletRequest request
   * @param HttpServletResponse response
   * @throw ServletException
   * @throw IOException
   */
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
      this.processProgram(request,response);
  }

  /**
   * Handles all requests related to program. The request action is determined by two parameters.
   * First check the query string. Query String can take one of the following values:
   * - create: the user has filled in data on the 'Create Program' page and hit 'Save'. The processing 
   *         should create the program.
   * - update: the user is on the 'Update Program' page and hit 'Save'. The processing should update
   *         the program.
   * - copy: the user is on the search program result page, selected a program, and hit 'Copy'.
   *         The processing should display the 'Copy Program' page. Sections related to the source program
   *         should be displayed but other program information should not be pre-populated to the page.
   * - createSelectExistingSection: the user is on the 'Create Program' page and has selected
   *         a new section from the 'Existing Selections' drop down select box, or the user has checked
   *         or unchecked the default flag. The processing should keep
   *         all user data and refresh to display the selected section for that section type.
   * - updateSelectExistingSection: the user is on the 'Update Program' page and has selected
   *         a new section.
   * - copySelectExistingSection: the user is on the 'Copy Program' page and has selected a new section
   *         or has changed the default flag.
   * - viewupdate: user is on the search program result page and chooses to 'Update' a program; user has
   *         successfully created/updated/copied a program; user has selected 'Save/Preview' on 
   *         'Create Program','Update Program', or 'Copy Program', and the processing has 
   *         successfully completed.
   * - delete: user is on the search program result page and has chosen to delete some programs.
   * - exit: user is on CreateProgram.xsl (this includes 'Create Program', 'Update Program', and 
   *         'Copy Program') and hit the link 'Email confirmation'.
   * 
   * If the query string is none of the above, then check parameter 'actionMethod'
   * - preview: user is on the search program result page and has chosen to preview the program.
   * - updatepreview: user is on the 'Update Program' page and has chosen 'Save/Preview'
   * - copypreview: user is on the 'Copy Program' page and has chosen 'Save/Preview'
   * - createpreview: user is on the 'Create Programj' page and has chosen 'Save/Preview'
   * 
   * When the program is actually copied (when user clicks on 'Save' on 'Copy Program' page
   * 
   * @param HttpServletRequest
   * @param HttpServletResponse
   * @throw ServletException
   * @throw IOException
   */
  private void processProgram(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    logger.debug("Entering EmailAdminProgram processProgram...");
    String securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN);
    String securityContext = request.getParameter(HTML_PARAM_SECURITY_CONTEXT);
    String page = "";
    String path = "";
    long programId = 0;
    Element tagElement = null;
    Document availableSourcesSection = null;
    String formAction = request.getQueryString(); 
    //used for preview program
    String formActionPrev = request.getParameter("actionMethod");   


    
    try
    {
        if (SECURITY_OFF || ServletHelper.isValidToken(request))
        {
          if(ServletHelper.isAuthorized(request)) {
            response.setContentType(EmailAdminConstants.CONTENT_TYPE);
            ServletContext context = this.getServletContext();    

            path = context.getRealPath("/xsl") + java.io.File.separator ;

            Document responseDocument = DOMUtil.getDocument();
            String programDefault = request.getParameter("selDefault");
            String sprogramId = request.getParameter("programId");
            programId = 0;
            if(sprogramId != null && !sprogramId.equals("")) {
                programId = new Long(sprogramId).longValue();
            }

            String sectionType = request.getParameter("sectionType");
            String searchString = request.getParameter("searchString");
            EmailAdminDAO emailAdminDao = new EmailAdminDAO();

            HashMap pageData = new HashMap();
            pageData.put("searchString",searchString);
            pageData.put(HTML_PARAM_SECURITY_TOKEN,securityToken);
            pageData.put(HTML_PARAM_SECURITY_CONTEXT, securityContext);
            logger.debug("formAction is:" + formAction);
            logger.debug("formActionPrev is:" + formActionPrev);
            logger.debug("programId is:" + programId);
            logger.debug("sectionType is:" + sectionType);
            logger.debug("securityToken is:" + securityToken);
            logger.debug("context is:" + securityContext);
            
            if(formAction.equals(EXIT))
            {
                page = path + START_PAGE;
                DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);
            }
            /*
            else if(formAction.equals(BACK))
            {
                  page = path + SEARCH_PROGRAM_RESULTS_PAGE;
                  searchString = request.getParameter("searchString");

                  Document sections = null;
                  
                  pageData.put("headerName", "Program Search Results");
                  pageData.put("SectionType", "Program");
                  pageData.put("SearchString", searchString);
                  sections = emailAdminDao.searchProgramByName(searchString);
                  
                  DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);
                  DOMUtil.addSection(responseDocument,sections.getElementsByTagName("SECTIONS"));
            }
            */
            else if(formAction.equals(CREATE))
            {
                 //create program
                page = path + CREATE_PROGRAM_PAGE;
                String nonDefaultSectionError = emailAdminDao.getNonDefaultSectionError(request);

                // Check if program name exists. Progam name must be unique.
                if (emailAdminDao.programNameExists(request.getParameter("programName"))) {
                    responseDocument = refreshSelectSection(request, responseDocument, emailAdminDao);
                    Document errorSection = ServletHelper.getDocumentWithAttribute("ERROR", "message", ERROR_PROGRAM_NAME_EXISTS);
                    DOMUtil.addSection(responseDocument,errorSection.getElementsByTagName("ERROR"));
                } 
                // Check if all selected sections are default. Default program can only have default sections.
    //THIS CHECK IS BEING REMOVED PER JULIE EGGERT.  THIS CHECK DETERMINES IF ANY OF THE
    //ATTACHED SECTIONS CONTAIN AN END DATE.  JULIE REQUESTED THAT THIS CHECK NO LONGER
    //BE DONE BECAUSE SECTION CAN HAVE END DATES EVEN IF THE PROGRAM IS A DEFAULT PROGRAM.
    //EMUELLER 4/9/04
    //            else if("Y".equals(programDefault) && nonDefaultSectionError.length() > 0) {
    //                responseDocument = refreshSelectSection(request, responseDocument, emailAdminDao);
    //                Document errorSection = ServletHelper.getDocumentWithAttribute("ERROR", "message", ERROR_SECTION_HAS_END_DATE + nonDefaultSectionError);
    //                DOMUtil.addSection(responseDocument,errorSection.getElementsByTagName("ERROR"));
    //            }
                else {
                    //Create program regardless of source codes being valid or not.
                    EmailProgramVO emailProgram = insertProgram(request,emailAdminDao);
                    programId = emailProgram.getProgramId();     

                    //Update source code program mapping.
                    //Check if source code program dates overlap. A source code can only have one effective
                    //custom program at a given time.
                    String invalidSourceCodes = checkForInvalidSourceCodes(emailProgram.getProgramSourceCodes());
                    if(invalidSourceCodes.length() > 0)
                    {
                      //Send error
                      String msg = ERROR_INVALID_SOURCE_CODE + " " + invalidSourceCodes;
                      Document errorSection = ServletHelper.getDocumentWithAttribute("ERROR", "message", msg);
                      DOMUtil.addSection(responseDocument,errorSection.getElementsByTagName("ERROR"));                  
                    }                    
                    else {
                          String sourceCodesInError = emailAdminDao.programDateOverlap(emailProgram);
                          if("N".equals(emailProgram.getProgramDefault()) && sourceCodesInError.length() > 0) {
                              // Send back warning.
                              String msg = ERROR_PROGRAM_DATE_OVERLAP + sourceCodesInError;
                              Document errorSection = ServletHelper.getDocumentWithAttribute("ERROR", "message", msg);
                              DOMUtil.addSection(responseDocument,errorSection.getElementsByTagName("ERROR"));                  
                          } else {
                              // Do the update.
                              emailAdminDao.updateProgramSourceCodes(emailProgram);
                          }
                    }
                    responseDocument = viewProgram(programId, request, responseDocument, emailAdminDao);
                    DOMUtil.print(responseDocument, System.out);
                }
            }
            else if(formAction.equals(CREATE_SELECT_SECTION) || formAction.equals(UPDATE_SELECT_SECTION)
                    || formAction.equals(COPY_SELECT_SECTION))
            {
                page = path + CREATE_PROGRAM_PAGE;     
                responseDocument = refreshSelectSection(request, responseDocument, emailAdminDao);              
             }
             else if(formAction.equals(VIEW_FOR_UPDATE) || formAction.equals(COPY))
             {
                page = path + CREATE_PROGRAM_PAGE;
                responseDocument = viewProgram(programId, request, responseDocument, emailAdminDao);
             }
             else if(formAction.equals(UPDATE))
             {
                //update program
                page = path + CREATE_PROGRAM_PAGE;
                String nonDefaultSectionError = emailAdminDao.getNonDefaultSectionError(request);
                
                //Check if any new section has same start date as an existing section for the program.
                //A new section cannot have the same start date of any existing sections of the same type.
                String sectionStartDateError = emailAdminDao.getDuplicateStartDateError(programId, request);
                if (sectionStartDateError != null && sectionStartDateError.length() > 0 ) {
                    responseDocument = refreshSelectSection(request, responseDocument, emailAdminDao);
                    Document errorSection = ServletHelper.getDocumentWithAttribute
                      ("ERROR", "message", ERROR_SECTION_START_DATE_CLASH + sectionStartDateError);
                    DOMUtil.addSection(responseDocument,errorSection.getElementsByTagName("ERROR"));          
                }
                //Check if all related sections are default. Default program can only have default sections.
           //REMOVING EFFECTIVE END DATE VALIDATION, EMUELLER, 4/12/04
           //     else if("Y".equals(programDefault) && nonDefaultSectionError.length() > 0) {
           //         responseDocument = refreshSelectSection(request, responseDocument, emailAdminDao);
           //         Document errorSection = ServletHelper.getDocumentWithAttribute("ERROR", "message", ERROR_SECTION_HAS_END_DATE + nonDefaultSectionError);
           //         DOMUtil.addSection(responseDocument,errorSection.getElementsByTagName("ERROR"));
           //     }
                else {
                    // Update program regardless of source code validation results.
                    EmailProgramVO emailProgram = updateProgram(request,emailAdminDao);
                    
                    //Update source code program mapping.
                    //Check if source code program dates overlap. A source code can only have one effective
                    //custom program at a given time.
                    String invalidSourceCodes = checkForInvalidSourceCodes(emailProgram.getProgramSourceCodes());
                    if(invalidSourceCodes.length() > 0)
                    {
                      //Send error
                      String msg = ERROR_INVALID_SOURCE_CODE + " " + invalidSourceCodes;
                      Document errorSection = ServletHelper.getDocumentWithAttribute("ERROR", "message", msg);
                      DOMUtil.addSection(responseDocument,errorSection.getElementsByTagName("ERROR"));                  
                    }                    
                    else {
                          String sourceCodesInError = emailAdminDao.programDateOverlap(emailProgram);                    
                          if("N".equals(emailProgram.getProgramDefault()) && sourceCodesInError.length() > 0) {
                              // Send back warning.
                              Document errorSection = ServletHelper.getDocumentWithAttribute("ERROR", "message", ERROR_PROGRAM_DATE_OVERLAP + sourceCodesInError);
                              DOMUtil.addSection(responseDocument,errorSection.getElementsByTagName("ERROR"));                  
                          } else {
                              // Do the update.
                              emailAdminDao.updateProgramSourceCodes(emailProgram);
                          }    
                    }
                    responseDocument = viewProgram(programId, request, responseDocument, emailAdminDao);
                }
             }
             // Delete if possible. Return the program name for the ones that cannot be deleted.
             else if(formAction.equals(DELETE))
             {
                //delete program
                page = path + START_PAGE;
                String cannotDeletePrograms = "";
                String selectedPrograms = "";
                selectedPrograms = request.getParameter("selectedPrograms");

                StringTokenizer st = new StringTokenizer(selectedPrograms,"~");
                while (st.hasMoreTokens())
                {
                      programId = new Long(st.nextToken()).longValue();
                      try {
                          emailAdminDao.deleteEmailProgram(programId);
                      } catch (Exception e) {
                          if("PROGRAM ASSOCIATED TO A SOURCE".equalsIgnoreCase(e.getMessage())) {
                              // Find program name from progrm id.
                              EmailProgramVO emailProgram = emailAdminDao.getEmailProgram(programId);
                              if (emailProgram == null) {
                                  throw new Exception("Internal Error: No program with id " + programId);
                              }
                              cannotDeletePrograms += "," + emailProgram.getProgramName();
                          } else {
                              throw e;
                          }
                      }
                }

                // Check if there are any program that cannot be deleted. If yes, remove the leading ','.
                if (cannotDeletePrograms != null && cannotDeletePrograms != "") {
                    cannotDeletePrograms = ERROR_PROGRAM_HAS_SOURCE + cannotDeletePrograms.substring(1);
                }

                //custom message from database
                page = path + SEARCH_PROGRAM_RESULTS_PAGE;
                searchString = request.getParameter("searchString");

                Document sections = null;
                pageData.put("headerName", "Program Search Results");
                pageData.put("SectionType", sectionType);
                pageData.put("cannotDeletePrograms", cannotDeletePrograms);

                String option = request.getParameter("option");

                if(option.equalsIgnoreCase("Source Code"))
                {
                      sections = emailAdminDao.searchProgramBySource(searchString.toUpperCase());                  
                }
                else
                {
                      sections = emailAdminDao.searchProgramByName(searchString.toUpperCase());                  
                }

                DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);
                DOMUtil.addSection(responseDocument,sections.getElementsByTagName("SECTIONS"));
            }
            else if(formActionPrev != null && formActionPrev.equals(ONLY_PREVIEW))
            {
                page = path + PREVIEW_EMAIL;
                Document newProgram = DOMUtil.getDefaultDocument();
                Element programElement = newProgram.createElement("PROGRAM");
                newProgram.appendChild(programElement);
                programElement.appendChild(this.createTextNode("program_id", new Long(programId).toString(), newProgram, tagElement));
                //pageData.put("previewmode", "'preview'");
                DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);
                DOMUtil.addSection(responseDocument,newProgram.getElementsByTagName("PROGRAM"));
            }
            else if(formActionPrev != null && formActionPrev.equals(UPDATE_PREVIEW))
            {
                page = path + PREVIEW_EMAIL;     
                String nonDefaultSectionError = emailAdminDao.getNonDefaultSectionError(request);
                    
                //Check if any new section has duplicate start date as an existing section for the program.
                String sectionStartDateError = emailAdminDao.getDuplicateStartDateError(programId, request);
                if (sectionStartDateError != null && sectionStartDateError.length() > 0 ) {
                    // Flag window to close.
                    request.setAttribute("isPopup", "true");
                    ServletHelper.sendErrorPage(request, response, errorStylesheet, new Exception(ERROR_SECTION_START_DATE_CLASH + sectionStartDateError));
                }
          //REMOVING EFFECTIVE END DATE VALIDATION, EMUELLER, 4/12/04
          //      else if("Y".equals(programDefault) && nonDefaultSectionError.length() > 0) {
          //          // Flag window to close.
          //          request.setAttribute("isPopup", "true");
          //          ServletHelper.sendErrorPage(request, response, errorStylesheet, new Exception(ERROR_SECTION_HAS_END_DATE + nonDefaultSectionError));                    
          //      }       
                else {               
                    EmailProgramVO emailProgram = updateProgram(request,emailAdminDao);

                    //Update source code program mapping.
                    String sourceCodesInError = emailAdminDao.programDateOverlap(emailProgram);                    
                    if("N".equals(emailProgram.getProgramDefault()) && sourceCodesInError.length() > 0) {
                        Document errorSection = ServletHelper.getDocumentWithAttribute("ERROR", "message", ERROR_PROGRAM_DATE_OVERLAP + sourceCodesInError);
                        DOMUtil.addSection(responseDocument,errorSection.getElementsByTagName("ERROR")); 
                    } else {
                        
                        // Do the update.
                        emailAdminDao.updateProgramSourceCodes(emailProgram);
                    }
                    
                    Document newProgram = DOMUtil.getDefaultDocument();
                    Element programElement = newProgram.createElement("PROGRAM");
                    newProgram.appendChild(programElement);
                    programElement.appendChild(this.createTextNode("program_id", new Long(programId).toString(), newProgram, tagElement));
                    pageData.put("previewmode", "'updatepreview'");
                    DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);
                    DOMUtil.addSection(responseDocument,newProgram.getElementsByTagName("PROGRAM"));
                    
                }
            }
            else if(formActionPrev != null && (formActionPrev.equals(COPY_PREVIEW) || formActionPrev.equals(CREATE_PREVIEW)))
            {
                // Response will be displayed in a popup window.
                page = path + PREVIEW_EMAIL;
                String nonDefaultSectionError = emailAdminDao.getNonDefaultSectionError(request);

                // Check if program name exists. Progam name must be unique.
                if (emailAdminDao.programNameExists(request.getParameter("programName"))) {
                    // Flag window to close.
                    request.setAttribute("isPopup", "true");
                    ServletHelper.sendErrorPage(request, response, errorStylesheet, new Exception(ERROR_PROGRAM_NAME_EXISTS));                    
                }                 
                //REMOVING EFFECTIVE END DATE VALIDATION, EMUELLER, 4/12/04
                //else if("Y".equals(programDefault) && nonDefaultSectionError.length() > 0) {
                //    // Flag window to close.
                //    request.setAttribute("isPopup", "true");
                //    ServletHelper.sendErrorPage(request, response, errorStylesheet, new Exception(ERROR_SECTION_HAS_END_DATE + nonDefaultSectionError));                    
                //}          
                else {
                    EmailProgramVO emailProgram = insertProgram(request,emailAdminDao);
                    programId = emailProgram.getProgramId();                

                    //Update source code program mapping.
                    String sourceCodesInError = emailAdminDao.programDateOverlap(emailProgram);                    
                    if("N".equals(emailProgram.getProgramDefault()) && sourceCodesInError.length() > 0) {
                        Document errorSection = ServletHelper.getDocumentWithAttribute("ERROR", "message", ERROR_PROGRAM_DATE_OVERLAP + sourceCodesInError);
                        DOMUtil.addSection(responseDocument,errorSection.getElementsByTagName("ERROR")); 
                    } else {
                        // Do the update.
                        emailAdminDao.updateProgramSourceCodes(emailProgram);
                    }
                                        
                    Document newProgram = DOMUtil.getDefaultDocument();
                    Element programElement = newProgram.createElement("PROGRAM");
                    newProgram.appendChild(programElement);
                    programElement.appendChild(this.createTextNode("program_id", new Long(programId).toString(), newProgram, tagElement));
                    DOMUtil.addSection(responseDocument,newProgram.getElementsByTagName("PROGRAM"));
                    //pageData.put("previewmode", "'" + formActionPrev + "'");
                    DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);
                }
            }
            if (!response.isCommitted()) {
                pageRedirect(request,response,responseDocument,page);
            }
        } else {
            // not authorized
            if(formActionPrev != null && (formActionPrev.equals(COPY_PREVIEW) || formActionPrev.equals(CREATE_PREVIEW))) {
                // send error page to popup.
                request.setAttribute("isPopup", "true");
            }
            ServletHelper.sendErrorPage
              (request, response, errorStylesheet, new Exception(NOT_AUTHORIZED));
        }
            
      } else {
          // Invalid security token
          if(formActionPrev != null && (formActionPrev.equals(COPY_PREVIEW) || formActionPrev.equals(CREATE_PREVIEW))) {
              // send error page to popup.
              request.setAttribute("isPopup", "true");
              ServletHelper.sendErrorPage(request, response, errorStylesheet, new Exception("Invalid Security Token"));
          } else {          
              page = ServletHelper.getExitPath(request);
              response.sendRedirect(page);
          }
      }
    }
    catch(ExpiredIdentityException e )
    {
          logger.error(e);
          if(formActionPrev != null && (formActionPrev.equals(COPY_PREVIEW) || formActionPrev.equals(CREATE_PREVIEW))) {
              // send error page to popup.
              request.setAttribute("isPopup", "true");
              ServletHelper.sendErrorPage(request, response, errorStylesheet, e);
          } else {
              try {
                  page = ServletHelper.getExitPath(request);
              } catch (Exception ex) {
                  logger.error(ex);
              }
              response.sendRedirect(page);
          }
    }
    catch(ExpiredSessionException e )
    {
          logger.error(e);
          if(formActionPrev != null && (formActionPrev.equals(COPY_PREVIEW) || formActionPrev.equals(CREATE_PREVIEW))) {
              // send error page to popup.
              request.setAttribute("isPopup", "true");
              ServletHelper.sendErrorPage(request, response, errorStylesheet, e);
          } else {
              try {
                  page = ServletHelper.getExitPath(request);
              } catch (Exception ex) {
                  logger.error(ex);
              }
              response.sendRedirect(page);
          }
    }
    catch(InvalidSessionException e )
    {
          logger.error(e);
          if(formActionPrev != null && (formActionPrev.equals(COPY_PREVIEW) || formActionPrev.equals(CREATE_PREVIEW))) {
              // send error page to popup.
              request.setAttribute("isPopup", "true");
              ServletHelper.sendErrorPage(request, response, errorStylesheet, e);
          } else {
              try {
                  page = ServletHelper.getExitPath(request);
              } catch (Exception ex) {
                  logger.error(ex);
              }
              response.sendRedirect(page);
          }
    }
    catch(Exception e)
    {
        logger.error(e);
        if(formActionPrev != null && (formActionPrev.equals(COPY_PREVIEW) || formActionPrev.equals(CREATE_PREVIEW))) {
            // send error page to popup.
            request.setAttribute("isPopup", "true");
        }
            
        ServletHelper.sendErrorPage(request, response, errorStylesheet, e);
    }
  }

  /**
   * Inserts a program. The procedure is called when user requests to create program.
   * The procedure first gets data from the request and creates an EmailProgramVO object.
   * The data fields include: program name, description (optional), effective start date, 
   * effective end date (optional), one section of each section type, source codes (optional for
   * custom program). Then it calls the DAO procedure to persist the program in database.
   * The DAO updates the PROGRAM_MAPPING table to insert a program and PROGRAM_SECTION_MAPPING
   * table to insert a record for each section. Note that the PROGRAM_TO_SOURCE_MAPPING table
   * is not updated at this stage.
   * 
   * @param HttpServletRequest the request from which EmailProgramVO data is retrieved.
   * @param EmailAdminDAO the DAO that is responsible for inserting the program.
   * @return EmailProgramVO the value object created from request.
   * @throw Exception
   */
  private EmailProgramVO insertProgram(HttpServletRequest request, EmailAdminDAO emailAdminDao) throws Exception
  {
      logger.debug("Entering insertProgram...");
      EmailProgramVO emailProgram;
      StringTokenizer st;
      java.util.Date currentDate = new java.util.Date();
      SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
      String scurrentDate = sdf.format(currentDate);
      String secToken = getSecurityToken(request);
      String updatedBy = ServletHelper.getMyId(secToken);
      String programDefault = request.getParameter("selDefault"); // program default flag
      
      emailProgram = new EmailProgramVO();
      emailProgram.setUpdatedBy(updatedBy);

      // Set program name.
      emailProgram.setProgramName(request.getParameter("programName"));

      // Set program description. Optional.
      emailProgram.setProgramDescription(request.getParameter("programDescription"));

      // Set program effective start date.
      emailProgram.setEffectiveStartDate(request.getParameter("effectiveStartDate"));   
      
      String effectiveEndDate = request.getParameter("effectiveEndDate");

      // According to specs, if program is default, program cannot have end date.
      // Program will never expire. In PROGRAM_MAPPING table, end_date column will not be populated.
      if(programDefault != null && programDefault.equals("Y"))
      {
          effectiveEndDate = null;
      }
      else
      {
          // Program is custom. If user entered effective end date, that is the end date for program.
          // If user has not entered effective end date, effective end date is current date - program
          // is expired upon creation.
          if(effectiveEndDate == null || effectiveEndDate.equals("")) {
              effectiveEndDate = scurrentDate;
          }
      }

      // Set program end date.
      emailProgram.setEffectiveEndDate(effectiveEndDate);

      // Set program default flag.
      emailProgram.setProgramDefault(programDefault);

      //////////////////////////////////////////////////////////////
      //sections to program
      // Subject section and gurantee section have only text format and no html format.
      String[] selectedSections = new String[9];
      selectedSections[0] = request.getParameter("selSubjectId");
      selectedSections[1] = request.getParameter("selHeaderIdText");
      selectedSections[2] = request.getParameter("selHeaderIdHtml");
      selectedSections[3] = request.getParameter("selGuaranteeIdText");
      selectedSections[4] = request.getParameter("selGuaranteeIdHtml");
      selectedSections[5] = request.getParameter("selMarketingIdText");
      selectedSections[6] = request.getParameter("selMarketingIdHtml");
      selectedSections[7] = request.getParameter("selContactIdText");
      selectedSections[8] = request.getParameter("selContactIdHtml");

      // Set email sections.
      emailProgram.setSections(selectedSections);

      // Retrieve selected source codes. Source codes are set as one parameter (selSourceCodes), separated by '~'.
      String selSourceCodes = request.getParameter("selSourceCodes");

      // Tokenize the string and put the selected source codes in a String array.
      st = new StringTokenizer(selSourceCodes,"~");
      String[] selectedSources = new String[st.countTokens()];
      int i = 0;
      while (st.hasMoreTokens())
      {
          selectedSources[i] = st.nextToken();
          i++;
      }

      // Set source code string array on program object.
      emailProgram.setSourceCodes(selectedSources);

      // The DAO procedure to insert the program object returns the program id.
      // Set the program id on program object and return the program object
      // (to prepare for updating the source codes).
      emailProgram.setProgramId(emailAdminDao.insertEmailProgram(emailProgram));
      return emailProgram;
  }

  /**
   * Updates a program. The procedure is called when user clicks on 'Save'
   * or 'Save/Preview' on the 'Update Program' page.
   * The procedure first gets data from the request and creates an EmailProgramVO object.
   * The data fields include: program name, description (optional), effective start date, 
   * effective end date (optional), one section of each section type, source codes (optional for
   * custom program). Then it calls the DAO procedure to update the program in database.
   * The DAO updates program information in the PROGRAM_MAPPING table. It also inserts
   * any new section in PROGRAM_SECTION_MAPPING table while keeping the originals. Note
   * that the PROGRAM_TO_SOURCE_MAPPING table is not updated at this stage.
   * 
   * @param HttpServletRequest the request from which EmailProgramVO data is retrieved.
   * @param EmailAdminDAO the DAO that is responsible for inserting the program.
   * @return EmailProgramVO the value object created from request.
   * @throw Exception
   */
  private EmailProgramVO updateProgram(HttpServletRequest request, EmailAdminDAO emailAdminDao) throws Exception
  {
      logger.debug("Entering updateProgram...");

      // Get program id.
      long programId = new Long(request.getParameter("programId")).intValue();

      // Get current date.
      java.util.Date currentDate = new java.util.Date();
      SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
      String scurrentDate = sdf.format(currentDate);  
      String updatedBy = ServletHelper.getMyId(getSecurityToken(request));

      // Create EmailProgramVO value object.
      EmailProgramVO emailProgram = new EmailProgramVO();

      // Set program id.
      emailProgram.setProgramId(programId);

      // Set updated by.
      emailProgram.setUpdatedBy(updatedBy);

      // Set program name. This should not have changed because updating program name is not allowed.
      emailProgram.setProgramName(request.getParameter("programName"));

      // Set program description. Optional field.
      emailProgram.setProgramDescription(request.getParameter("programDescription"));

      // Set effective start date.
      emailProgram.setEffectiveStartDate(request.getParameter("effectiveStartDate"));

      // Get effective end date from request.
      String effectiveEndDate = request.getParameter("effectiveEndDate");

      // Get program default flag.
      String programDefault = request.getParameter("selDefault");

      // If default program, end date is null. end_date column in PROGRAM_MAPPING table will be empty.
      if(programDefault != null && programDefault.equals("Y"))
      {
          effectiveEndDate = null;
      }
      else
      {
          // If end date is entered, use the input end date.
          // Otherwise end date is current date - program is expired upon update.
          if(effectiveEndDate == null || effectiveEndDate.equals("")) {
              effectiveEndDate = scurrentDate;
          }               
      }

      // Set effective end date.
      emailProgram.setEffectiveEndDate(effectiveEndDate);

      // Set program default flag.
      emailProgram.setProgramDefault(programDefault);

      //////////////////////////////////////////////////////////////
      // get sections to program
      // Subject and gurantee sections have only text format and no html format.
      String[] selectedSections = new String[9];
      selectedSections[0] = request.getParameter("selSubjectId");
      selectedSections[1] = request.getParameter("selHeaderIdText");
      selectedSections[2] = request.getParameter("selHeaderIdHtml");
      selectedSections[3] = request.getParameter("selGuaranteeIdText");
      selectedSections[4] = request.getParameter("selGuaranteeIdHtml");
      selectedSections[5] = request.getParameter("selMarketingIdText");
      selectedSections[6] = request.getParameter("selMarketingIdHtml");
      selectedSections[7] = request.getParameter("selContactIdText");
      selectedSections[8] = request.getParameter("selContactIdHtml");

      // Set program section as a String array on program value object.
      emailProgram.setSections(selectedSections);

      //////////////////////////////////////////////////////////////
      //Retrieve selected source codes. Source codes are set as one parameter (selSourceCodes), separated by '~'.
      String selSourceCodes = request.getParameter("selSourceCodes");

      // Tokenize selected source codes and put them in a string array.
      StringTokenizer st = new StringTokenizer(selSourceCodes,"~");
      String[] selectedSources = new String[st.countTokens()];
      int j = 0;
      while (st.hasMoreTokens())
      {
          selectedSources[j] = st.nextToken();
          j++;
      }

      // Set the string array of source codes on email program value object.
      emailProgram.setSourceCodes(selectedSources);

      // Update the email program. This updates the PROGRAM_MAPPING table and PROGRAM_SECTION_MAPPING table.
      emailAdminDao.updateEmailProgram(emailProgram);
      return emailProgram;
  }

  /**
   * Displays view program page. From this page user can update the program.
   * The formAction modes are: CREATE (after create), VIEW_FOR_UPDATE, COPY (view to copy), UPDATE.
   * 
   * @param int program id
   * @param Document document to add section to.
   * @param EmailAdminDAO the data access object
   */
  public Document viewProgram(long programId, HttpServletRequest request, Document responseDocument, EmailAdminDAO emailAdminDao) throws Exception {
      logger.debug("Entering viewProgram...");
      String formAction = request.getQueryString();
      String sectionType = request.getParameter("sectionType");
      String searchString = request.getParameter("searchString");
      String option = request.getParameter("option");
      Document availableSourcesSection = null;

      //open section update page
      java.util.Date currentDate = new java.util.Date();
      SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
      String scurrentDate = sdf.format(currentDate);      
      String securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN);
      String securityContext = request.getParameter(HTML_PARAM_SECURITY_CONTEXT);
      
      HashMap pageData = new HashMap();
      pageData.put(HTML_PARAM_SECURITY_TOKEN,securityToken);
      pageData.put(HTML_PARAM_SECURITY_CONTEXT, securityContext);

      pageData.put("searchString",searchString);
      pageData.put("option", option);
      
      // The following data elements are needed on the XSL page because create program,
      // copy program, and update program share one XSL file.
      if(formAction.equals(COPY))
      {
          pageData.put("pageTitle", "FTD - Copy Program");
          pageData.put("headerName", "Copy Program");
          pageData.put("mode", "'create'");
          pageData.put("previewmode", "'copypreview'");
          pageData.put("pagemode", "copy");
      }
      else
      {
          pageData.put("pageTitle", "FTD - Update Program");
          pageData.put("headerName", "Update Program");
          pageData.put("mode", "'update'");
          pageData.put("previewmode", "'updatepreview'");
          pageData.put("pagemode", "update");
      }

      DOMUtil.addSection(responseDocument, "pageData", "data", pageData,true);

      // Retrieves all sections for all section types.
      responseDocument = buildAllSectionDropDowns(emailAdminDao,responseDocument);

      // Retrieves EmalProgramVO from program id.
      EmailProgramVO program = new EmailProgramVO();
      program = emailAdminDao.getEmailProgram(programId);

      Element tagElement = null;

      Document newProgram = DOMUtil.getDefaultDocument();
      Element programElement = newProgram.createElement("PROGRAM");
      newProgram.appendChild(programElement);

      //clear out fields for copy. Copy program only shows related sections from the source program.
      if(formAction.equals(COPY))
      {
          program.setProgramName("");
          program.setProgramDescription("");
          program.setEffectiveStartDate("");
          program.setEffectiveEndDate("");
          program.setProgramDefault("N");
      }

      // Build program data based on the object read from database.
      programElement.appendChild(this.createTextNode("program_id", String.valueOf(programId), newProgram, tagElement));
      programElement.appendChild(this.createTextNode("program_name", program.getProgramName(), newProgram, tagElement));
      programElement.appendChild(this.createTextNode("description", program.getProgramDescription(), newProgram, tagElement));
      programElement.appendChild(this.createTextNode("start_date", program.getEffectiveStartDate(), newProgram, tagElement));
      programElement.appendChild(this.createTextNode("end_date", program.getEffectiveEndDate(), newProgram, tagElement));
      programElement.appendChild(this.createTextNode("default", program.getProgramDefault(), newProgram, tagElement));
      programElement.appendChild(this.createTextNode("program_default", program.getProgramDefault(), newProgram, tagElement));
      //dynamic loop based on program section
      Document section = null;
      Document selSection = null;
      Element selSectionElement = null;

      //if program enddate is less than current date show content for last day of program
      if(program.getEffectiveEndDate() != null && !program.getEffectiveEndDate().equals(""))
      {
          java.util.Date effectiveEndDate =  new java.util.Date(program.getEffectiveEndDate());
          if(effectiveEndDate.before(currentDate)) {
              scurrentDate = program.getEffectiveEndDate();
          }
      }

      // Determine which section in each section type should be pre-selected when the program
      // is first displayed by getting sections that the program is currently related to. 
      // There's one or zero of such section for each section type. If the program is not
      // expired, the section that has the latest start date will be displayed for that section
      // type. No section will be preselected if all sections of that type for the program
      // have expired. If the program is expired, sections related to the program as of
      // program expire date will be preselected.
          Collection programSections = emailAdminDao.getCurrentProgramSections(programId,scurrentDate);
          EmailSectionsVO programSection = null;

          Iterator iterator = programSections.iterator();
          for (int i = 0; iterator.hasNext(); i++)
          {
              programSection = new EmailSectionsVO();
              programSection = (EmailSectionsVO)iterator.next();

              sectionType = programSection.getSectionType();
              String sectionName = programSection.getSectionName();

              section = emailAdminDao.getSectionByName(sectionType,sectionName);

              selSection = DOMUtil.getDefaultDocument();
              selSectionElement = selSection.createElement("SELECTED_" + sectionType.toUpperCase());
              selSection.appendChild(selSectionElement);

              DOMUtil.addSection(selSection,section.getElementsByTagName("SECTION"));
              DOMUtil.addSection(responseDocument,selSection.getElementsByTagName("SELECTED_" + sectionType.toUpperCase()));

              programElement.appendChild(this.createTextNode("selected_" + sectionType.toLowerCase(),sectionName,newProgram,tagElement));
          }
   
          //////////////////////////////////////////////////////////////
          //build source codes tied to program
          String selSourceCodes = "";

          // Clear out fields for copy. When 'Copy Program' page is displayed, only populate program section info
          // and not program info.
          if(!formAction.equals(COPY)) 
          {
                selSourceCodes = request.getParameter("selSourceCodes");
                if(selSourceCodes == null || selSourceCodes.length() <=0 ){
                  selSourceCodes = emailAdminDao.getCurrentProgramSources(programId);
                }
          }
                  
          StringTokenizer st = new StringTokenizer(selSourceCodes,"~");

          Document selectedSources = DOMUtil.getDefaultDocument();
          Element selSourcesElement = selectedSources.createElement("SELECTED_SOURCE_CODES");
          selectedSources.appendChild(selSourcesElement);

          // In the case of view_for_update, selected_source_codes are the same as original_source_codes.
          // Remember original source codes because default program cannot dissociated source codes.
          // So when update a default program, the XSL checks the selected source codes against this
          // list to see if any source codes have been removed from the list. Error if so.
          Document originalSources = DOMUtil.getDefaultDocument();
          Element oriSourcesElement = originalSources.createElement("ORIGINAL_SOURCE_CODES");
          originalSources.appendChild(oriSourcesElement);          

          while (st.hasMoreTokens())
          {
              Element sourceCodeElement2 = selectedSources.createElement("SOURCE_CODE");
              selSourcesElement.appendChild(sourceCodeElement2);
              String sourceCode = st.nextToken();
              sourceCodeElement2.appendChild(this.createTextNode("VALUE", sourceCode, selectedSources, tagElement));
          }

          // It seems that having SELECTED_SOURCE_CODES subtree and ORIGINAL_SOURCE_CODES subtree
          // is a repetition because they are the same. But remember create program and update
          // program are sharing one xsl file. In this case these two subtrees are the same. 
          // Not the case for refreshSelectSection where 'SELECTED_SOURCE_CODES' is created
          // from the request data. But the trees are structured the same so that the XSL
          // can save some if statements.
          st = new StringTokenizer(selSourceCodes,"~");
          while (st.hasMoreTokens())
          {
              Element sourceCodeElement3 = originalSources.createElement("SOURCE_CODE");
              oriSourcesElement.appendChild(sourceCodeElement3);
              String sourceCode = st.nextToken();
              sourceCodeElement3.appendChild(this.createTextNode("VALUE", sourceCode, originalSources, tagElement));
              
          }          

          // Add SELECTED_SOURCE_CODES and ORIGINAL_SOURCE_CODES subtrees to response document.    
          DOMUtil.addSection(responseDocument,selectedSources.getElementsByTagName("SELECTED_SOURCE_CODES"));
          DOMUtil.addSection(responseDocument,originalSources.getElementsByTagName("ORIGINAL_SOURCE_CODES"));

          // Add program section to response document.
          DOMUtil.addSection(responseDocument,newProgram.getElementsByTagName("PROGRAM"));

          // Get availale source codes.
          // If create default program, available sources are all sources without a default program.
          // If create custom program, available sourcess are all sources.
          // If update program, avalable sources are all sources excluding sources related to this program.

          //No longer sending list of source codes to page
          /* if(formAction.equals(COPY)) {
              // Get all sources. Copy creates a custom program by default.
              availableSourcesSection = emailAdminDao.getSources("Y", null);
          } else {
              // CREATE, VIEW_FOR_UPDATE, UPDATE 
              // When calling this procedure, CREATE has created a program, thus is the same as UPDATE.
              availableSourcesSection = emailAdminDao.getSources("N", String.valueOf(programId));
          }
          */
          
          if(availableSourcesSection != null) {
              // Add available source code subtree to response document.
              DOMUtil.addSection(responseDocument,availableSourcesSection.getElementsByTagName("SOURCES"));
          }  
          
          return responseDocument;
      }

  /**
   * In brief, this procedure capture what the data is on the screen and refreshes selection 
   * based on user requests. The scenario when this program is called include: 
   * 1. User is on 'Create Program', 'Copy Program', or 'Update Program' page and 
   *    selects a different section from any section type.
   * 2. User is on 'Create Program' or 'Copy Program' page and checks/unchecks default program
   *    checkbox.
   * 3. User has requested one of the following action but the input data is not valid.
   *    and the user is sent back the unprocessed data.
   * The formAction/actionMethod modes are: UPDATE, UPDATE_PREVIEW, UPDATE_SELECT_SECTION, 
   * CREATE, CREATE_PREVEIEW, COPY_PREVIEW, CREATE_SELECT_SECTION,COPY_SELECT_SECTION
   *  
   *  @param HttpServletRequest
   *  @param Document document to add the subtree to
   *  @param EmailAdminDAO data access object
   *  @return Document the resulting document
   *  @throw Exception
   */
  public Document refreshSelectSection(HttpServletRequest request, Document responseDocument, EmailAdminDAO emailAdminDao) throws Exception {
      logger.debug("Entering refreshSelectSection...");
      String securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN);
      String securityContext = request.getParameter(HTML_PARAM_SECURITY_CONTEXT);
      HashMap pageData = new HashMap();
      pageData.put(HTML_PARAM_SECURITY_TOKEN,securityToken);
      pageData.put(HTML_PARAM_SECURITY_CONTEXT, securityContext);
      
      String formAction = request.getQueryString();
      String formActionPrev = request.getParameter("actionMethod"); //CREATE_PREVIEW, UPDATE_PREVIEW, COPY_PREVIEW
      String sectionType = request.getParameter("sectionType"); // The section type that has been refreshed.
      String sprogramId = request.getParameter("programId");
      long programId = 0;
      Document availableSourcesSection = null;

      // The following is needed because there's only one XSL file shared by
      // create program, copy program and update program.
      if(formAction.equals(COPY_SELECT_SECTION) || "copy".equals(request.getParameter("pagemode")) || COPY_PREVIEW.equals(formActionPrev))
      {
          pageData.put("pageTitle", "FTD - Copy Program");
          pageData.put("headerName", "Copy Program");
          pageData.put("mode", "'create'");
          //pageData.put("previewmode", "'createpreview'");
          pageData.put("previewmode", "'copypreview'");
          pageData.put("pagemode", "copy");
      } else if(formAction.equals(CREATE_SELECT_SECTION) 
            || formAction.equals(CREATE) 
            || CREATE_PREVIEW.equals(formActionPrev))
      {
          pageData.put("pageTitle", "FTD - Create Program");
          pageData.put("headerName", "Create Program");
          pageData.put("mode", "'create'");
          pageData.put("previewmode", "'createpreview'");
          pageData.put("pagemode", "create");
      } else
      {
          pageData.put("pageTitle", "FTD - Update Program");
          pageData.put("headerName", "Update Program");
          pageData.put("mode", "'update'");
          pageData.put("previewmode", "'updatepreview'");
          pageData.put("pagemode", "update");
      }

      // Remember which tab is selected when the request was sent.
      // The same tab needs to be pre-selected when page is loaded.
      String selectedTab = request.getParameter("selectedTab");
      if (selectedTab == null || selectedTab.length() == 0) {
          // Default the selected tab to the first (subject tab).
          selectedTab = "Subject";
      }
      pageData.put("selectedTab", "showtab('','" + selectedTab + "');");
      
      DOMUtil.addSection(responseDocument, "pageData", "data", pageData,true);

      //check if using an existing section
      String selectedSection = request.getParameter("ExistingSection_" + sectionType);

      //get all sections of all section types.
      responseDocument = buildAllSectionDropDowns(emailAdminDao,responseDocument);

      Document newProgram = DOMUtil.getDefaultDocument();
      Element programElement = newProgram.createElement("PROGRAM");
      newProgram.appendChild(programElement);
      Element tagElement = null;

      // Build program data based on request.
          programElement.appendChild(this.createTextNode("program_id", sprogramId, newProgram, tagElement));
          programElement.appendChild(this.createTextNode("program_name", request.getParameter("programName"), newProgram, tagElement));
          programElement.appendChild(this.createTextNode("description", request.getParameter("programDescription"), newProgram, tagElement));
          programElement.appendChild(this.createTextNode("start_date", request.getParameter("effectiveStartDate"), newProgram, tagElement));
          programElement.appendChild(this.createTextNode("end_date", request.getParameter("effectiveEndDate"), newProgram, tagElement));
          programElement.appendChild(this.createTextNode("default", request.getParameter("selDefault"), newProgram, tagElement));

          // The program could only have existed if the the request is an update like request.
          if(formAction.equals(UPDATE) || UPDATE_PREVIEW.equals(formActionPrev) || formAction.equals(UPDATE_SELECT_SECTION)) {
              // retrieve default key.
              programId = new Long(sprogramId).longValue();
              EmailProgramVO program = new EmailProgramVO();
              program = emailAdminDao.getEmailProgram(programId);
              programElement.appendChild(this.createTextNode("program_default", program.getProgramDefault(), newProgram, tagElement));
          }

          Document section = null;
          Document selSection = null;
          Element selSectionElement = null;

          if(sectionType.equals(SUBJECT))
          {
              //populate with selected section and send back to screen
              selSection = DOMUtil.getDefaultDocument();
              selSectionElement = selSection.createElement("SELECTED_SUBJECT");
              selSection.appendChild(selSectionElement);

              section = emailAdminDao.getSectionByName(sectionType,selectedSection);

              DOMUtil.addSection(selSection,section.getElementsByTagName("SECTION"));
              DOMUtil.addSection(responseDocument,selSection.getElementsByTagName("SELECTED_SUBJECT"));

              programElement.appendChild(this.createTextNode("selected_subject",selectedSection,newProgram,tagElement));
          }
          else
          {
              selSection = DOMUtil.getDefaultDocument();
              selSectionElement = selSection.createElement("SELECTED_SUBJECT");
              selSection.appendChild(selSectionElement);

              section = emailAdminDao.getSectionByName(SUBJECT,request.getParameter("selSubject"));
              DOMUtil.addSection(selSection,section.getElementsByTagName("SECTION"));
              DOMUtil.addSection(responseDocument,selSection.getElementsByTagName("SELECTED_SUBJECT"));

              programElement.appendChild(this.createTextNode("selected_subject",request.getParameter("selSubject"),newProgram,tagElement));
          }

          if(sectionType.equals(HEADER))
          {
              selSection = DOMUtil.getDefaultDocument();
              selSectionElement = selSection.createElement("SELECTED_HEADER");
              selSection.appendChild(selSectionElement);

              section = emailAdminDao.getSectionByName(sectionType,selectedSection);

              DOMUtil.addSection(selSection,section.getElementsByTagName("SECTION"));
              DOMUtil.addSection(responseDocument,selSection.getElementsByTagName("SELECTED_HEADER"));

              programElement.appendChild(this.createTextNode("selected_header",selectedSection,newProgram,tagElement));
          }
          else
          {
              selSection = DOMUtil.getDefaultDocument();
              selSectionElement = selSection.createElement("SELECTED_HEADER");
              selSection.appendChild(selSectionElement);

              section = emailAdminDao.getSectionByName(HEADER,request.getParameter("selHeader"));

              DOMUtil.addSection(selSection,section.getElementsByTagName("SECTION"));
              DOMUtil.addSection(responseDocument,selSection.getElementsByTagName("SELECTED_HEADER"));

              programElement.appendChild(this.createTextNode("selected_header",request.getParameter("selHeader"),newProgram,tagElement));
          }

          if(sectionType.equals(GUARANTEE))
          {
              selSection = DOMUtil.getDefaultDocument();
              selSectionElement = selSection.createElement("SELECTED_GUARANTEE");
              selSection.appendChild(selSectionElement);

              section = emailAdminDao.getSectionByName(sectionType,selectedSection);

              DOMUtil.addSection(selSection,section.getElementsByTagName("SECTION"));
              DOMUtil.addSection(responseDocument,selSection.getElementsByTagName("SELECTED_GUARANTEE"));

              programElement.appendChild(this.createTextNode("selected_guarantee",selectedSection,newProgram,tagElement));
          }
          else
          {
              selSection = DOMUtil.getDefaultDocument();
              selSectionElement = selSection.createElement("SELECTED_GUARANTEE");
              selSection.appendChild(selSectionElement);

              section = emailAdminDao.getSectionByName(GUARANTEE,request.getParameter("selGuarantee"));

              DOMUtil.addSection(selSection,section.getElementsByTagName("SECTION"));
              DOMUtil.addSection(responseDocument,selSection.getElementsByTagName("SELECTED_GUARANTEE"));

              programElement.appendChild(this.createTextNode("selected_guarantee",request.getParameter("selGuarantee"),newProgram,tagElement));
          }

          if(sectionType.equals(MARKETING))
          {
              selSection = DOMUtil.getDefaultDocument();
              selSectionElement = selSection.createElement("SELECTED_MARKETING");
              selSection.appendChild(selSectionElement);

              section = emailAdminDao.getSectionByName(sectionType,selectedSection);

              DOMUtil.addSection(selSection,section.getElementsByTagName("SECTION"));
              DOMUtil.addSection(responseDocument,selSection.getElementsByTagName("SELECTED_MARKETING"));

              programElement.appendChild(this.createTextNode("selected_marketing",selectedSection,newProgram,tagElement));
          }
          else
          {
              selSection = DOMUtil.getDefaultDocument();
              selSectionElement = selSection.createElement("SELECTED_MARKETING");
              selSection.appendChild(selSectionElement);

              section = emailAdminDao.getSectionByName(MARKETING,request.getParameter("selMarketing"));

              DOMUtil.addSection(selSection,section.getElementsByTagName("SECTION"));
              DOMUtil.addSection(responseDocument,selSection.getElementsByTagName("SELECTED_MARKETING"));

              programElement.appendChild(this.createTextNode("selected_marketing",request.getParameter("selMarketing"),newProgram,tagElement));
          }

          if(sectionType.equals(CONTACT))
          {
              selSection = DOMUtil.getDefaultDocument();
              selSectionElement = selSection.createElement("SELECTED_CONTACT");
              selSection.appendChild(selSectionElement);

              section = emailAdminDao.getSectionByName(sectionType,selectedSection);

              DOMUtil.addSection(selSection,section.getElementsByTagName("SECTION"));
              DOMUtil.addSection(responseDocument,selSection.getElementsByTagName("SELECTED_CONTACT"));

              programElement.appendChild(this.createTextNode("selected_contact",selectedSection,newProgram,tagElement));
          }
          else
          {
              selSection = DOMUtil.getDefaultDocument();
              selSectionElement = selSection.createElement("SELECTED_CONTACT");
              selSection.appendChild(selSectionElement);

              section = emailAdminDao.getSectionByName(CONTACT,request.getParameter("selContact"));

              DOMUtil.addSection(selSection,section.getElementsByTagName("SECTION"));
              DOMUtil.addSection(responseDocument,selSection.getElementsByTagName("SELECTED_CONTACT"));

              programElement.appendChild(this.createTextNode("selected_contact",request.getParameter("selContact"),newProgram,tagElement));
          }

          DOMUtil.addSection(responseDocument,newProgram.getElementsByTagName("PROGRAM"));
        
          //////////////////////////////////////////////////////////////
          //keep track of source codes selected for new program
          String selSourceCodes = request.getParameter("selSourceCodes");

          // Tokenize the source codes.
          StringTokenizer st = new StringTokenizer(selSourceCodes,"~");

          // Add each source code to SELECTED_SOURCE_CODES/SOURCE_CODE/VALUE text node
          Document selectedSources = DOMUtil.getDefaultDocument();
          Element selSourcesElement = selectedSources.createElement("SELECTED_SOURCE_CODES");
          selectedSources.appendChild(selSourcesElement);

          while (st.hasMoreTokens())
          {
              Element sourceCodeElement2 = selectedSources.createElement("SOURCE_CODE");
              selSourcesElement.appendChild(sourceCodeElement2);
              sourceCodeElement2.appendChild(this.createTextNode("VALUE", st.nextToken(), selectedSources, tagElement));
          }

          // Remember original source codes because default program cannot dissociated source codes.
          String oriSourceCodes = emailAdminDao.getCurrentProgramSources(programId);
          st = new StringTokenizer(oriSourceCodes,"~");

          // Tokenize orginal source codes and add them to ORIGINAL_SOURCE_CODES/SOURCE_CODE/VALUE text node
          Document originalSources = DOMUtil.getDefaultDocument();
          Element oriSourcesElement = originalSources.createElement("ORIGINAL_SOURCE_CODES");
          originalSources.appendChild(oriSourcesElement);           
                  
          while (st.hasMoreTokens())
          {
              Element sourceCodeElement3 = originalSources.createElement("SOURCE_CODE");
              oriSourcesElement.appendChild(sourceCodeElement3);
              String sourceCode = st.nextToken();
              sourceCodeElement3.appendChild(this.createTextNode("VALUE", sourceCode, originalSources, tagElement));
              
          }          

          // Add SELECTED_SOURCE_CODES and ORIGINAL_SOURCE_CODES subtree to the response document.
          DOMUtil.addSection(responseDocument,selectedSources.getElementsByTagName("SELECTED_SOURCE_CODES"));
          DOMUtil.addSection(responseDocument,originalSources.getElementsByTagName("ORIGINAL_SOURCE_CODES"));

          // If this is a refresh called by changing the default flag, get available source codes from db.
          // Otherwise get the available source codes from the screen.
          if ("Y".equals(request.getParameter("changeDefault"))) {
              // Get available source codes from database.
              availableSourcesSection = getAvailableSourceCodesFromDB(request, emailAdminDao);                      
          } else {
              availableSourcesSection = getAvailableSourceCodesFromScreen(request);
          }

          // Add available source code subtree to the response document.
          if(availableSourcesSection != null) {
              DOMUtil.addSection(responseDocument,availableSourcesSection.getElementsByTagName("SOURCES"));
          }  
   
      return responseDocument;                
  }

  /**
   * Get available source codes from db.
   * If update program, returned sources include all but the source codes related to the program. 
   * If create default program, return all source codes without a default program. 
   * If create custom program, return all source codes.
   * 
   * @param HttpServletRequest
   * @param EmailAdminDAO
   * @return Document
   * @throw Exception
   */
  private Document getAvailableSourceCodesFromDB 
  (HttpServletRequest request, EmailAdminDAO emailAdminDao) throws Exception {
          Document availableSourcesSection = null;
          String formAction = request.getQueryString();
          String formActionPrev = request.getParameter("actionMethod"); //CREATE_PREVIEW, UPDATE_PREVIEW, COPY_PREVIEW
          // Get availale source codes.          
          if(formAction.equals(UPDATE) 
          || UPDATE_PREVIEW.equals(formActionPrev) 
          || formAction.equals(UPDATE_SELECT_SECTION)) {
              // Get sources not related to this program.
              int programId = new Long(request.getParameter("programId")).intValue();
              
              availableSourcesSection = emailAdminDao.getSources("N", String.valueOf(programId));
          } else {
              // Get all sources.
              String defaultProgram = request.getParameter("selDefault");
              String allSourceFlag = "";
              if("Y".equals(defaultProgram)) {
                  // View all sources that don't have a default program, not providing program id.
                  allSourceFlag = "N";
                  
              } else {
                  // View all sources, not providing program id.
                  allSourceFlag = "Y";
              }
              availableSourcesSection = emailAdminDao.getSources(allSourceFlag, null);
          }

          // Remove the sources that are selected by user from available sources
          String selSourceCodes = request.getParameter("selSourceCodes");
          StringTokenizer st = new StringTokenizer(selSourceCodes,"~");
          NodeList availableSourceList = availableSourcesSection.getElementsByTagName("source_code");
          Element sourceElem = null;
          Text sourceText = null;
          while (st.hasMoreTokens())
          {
              String selectedSource = st.nextToken();
              for(int i = 0; i< availableSourceList.getLength(); i++) {
                  sourceElem = (Element)availableSourceList.item(i);
                  sourceText = (Text)sourceElem.getFirstChild();
                  if(selectedSource.equals(sourceText.getNodeValue())) {
                      sourceElem.getParentNode().getParentNode().removeChild(sourceElem.getParentNode());
                  }
              }
          }
          return availableSourcesSection;
  }

  /**
   * Get available source codes from screen.
   * 
   * @param HttpServletRequest
   * @param EmailAdminDAO
   * @return Document
   * @throw Exception
   */
  private Document getAvailableSourceCodesFromScreen 
  (HttpServletRequest request) throws Exception { 
      
      Element tagElement = null;

      // Get the source code string, '~' delimited.
      String unselSourceCodes = request.getParameter("unselSourceCodes");
      StringTokenizer st = new StringTokenizer(unselSourceCodes,"~");

      // Tokenize the source code string and add source codes to SOURCES/SOURCE/source_code text node.
      Document unselectedSources = DOMUtil.getDefaultDocument();
      Element unselSourcesElement = unselectedSources.createElement("SOURCES");
      unselectedSources.appendChild(unselSourcesElement);

      while (st.hasMoreTokens())
      {
          Element sourceCodeElement2 = unselectedSources.createElement("SOURCE");
          unselSourcesElement.appendChild(sourceCodeElement2);
          sourceCodeElement2.appendChild(this.createTextNode("source_code", st.nextToken(), unselectedSources, tagElement));
      }   

      return unselectedSources;
  }

  private String checkForInvalidSourceCodes(String sourceCodes[])
  {
    String invalidCodes = "";

    EmailAdminDAO emailAdminDAO = new EmailAdminDAO();

    for(int i=0; i < sourceCodes.length;i++)
    {
      String sourceCode = sourceCodes[i];
      if(!emailAdminDAO.sourceCodeExists(sourceCode))
      {
        if(invalidCodes.length() > 0) {
          invalidCodes = invalidCodes + ",";
        }
        invalidCodes = invalidCodes + sourceCode;
      }
    }

    return invalidCodes;
  }

  
}
