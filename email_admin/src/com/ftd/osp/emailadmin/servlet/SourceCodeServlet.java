package com.ftd.osp.emailadmin.servlet;

import com.ftd.osp.emailadmin.DataRequestHelper;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.exceptions.ExpiredIdentityException;
import com.ftd.security.exceptions.ExpiredSessionException;
import com.ftd.security.exceptions.InvalidSessionException;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;

import java.util.HashMap;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;

public class SourceCodeServlet extends EmailAdminBaseServlet
{
    private Logger logger;
    //private static final String CONTENT_TYPE = "text/html; charset=windows-1252";

    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        this.logger = new Logger("com.ftd.osp.emailadmin.servlet.SourceCodeServlet");
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(EmailAdminConstants.CONTENT_TYPE);
        this.loadSourceCodeSearch(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(EmailAdminConstants.CONTENT_TYPE);
        this.loadSourceCodeSearch(request, response);
    }

    private void loadSourceCodeSearch(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        DataRequest dataRequest = null;
        String page = "";

        try
        {
            String securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN);

            if (SECURITY_OFF || ServletHelper.isValidToken(request))
            {
                String sourceCodeInput = request.getParameter("sourceCodeInput").toUpperCase();

                // Create the initial document
                Document responseDocument = DOMUtil.getDocument();

                // Initialize data request
                dataRequest = DataRequestHelper.getInstance().getDataRequest();
                dataRequest.reset();

                // Load source code list from database
                dataRequest.setStatementID("GET_SOURCECODELIST_BY_VALUE");

                dataRequest.addInputParam("source_code", sourceCodeInput);
                dataRequest.addInputParam("description", sourceCodeInput);
                dataRequest.addInputParam("date_flag", request.getParameter("dateFlag").toUpperCase());

                if(!sourceCodeInput.equals(""))
                {
                  DOMUtil.addSection(responseDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes());
                }
                
                ServletContext context = this.getServletContext();
                String path = "/xsl" + java.io.File.separator + SOURCE_CODE_LOOKUP;
                File stylesheet = new File(context.getRealPath(path));
          
                //File stylesheet = new File(path);
                HashMap pageData = super.getSecurityData(request);

                TraxUtil traxUtil = TraxUtil.getInstance();
         
                traxUtil.transform(request, response, responseDocument, stylesheet, pageData);
              } else {
                  logger.error("Invalid security token!");
              }
          }
          catch(ExpiredIdentityException e )
          {
              logger.error(e);
              try {
                  page = ServletHelper.getExitPath(request);
              } catch (Exception ex) {
                  logger.error(ex);
              }
              response.sendRedirect(page);
          }
          catch(ExpiredSessionException e )
          {
              logger.error(e);
              try {
                  page = ServletHelper.getExitPath(request);
              } catch (Exception ex) {
                  logger.error(ex);
              }
              response.sendRedirect(page);

          }
          catch(InvalidSessionException e )
          {
              logger.error(e);
              try {
                  page = ServletHelper.getExitPath(request);
              } catch (Exception ex) {
                  logger.error(ex);
              }
              response.sendRedirect(page);

          }
          catch(Exception e)
          {
              logger.error(e);
              request.setAttribute("isPopup", "true");
              ServletHelper.sendErrorPage(request, response, errorStylesheet, e);
          }
          finally
          {
              try
              {
                  Connection con;
                  if (dataRequest != null && (con = dataRequest.getConnection()) != null) {
                      con.close();
                  }
              }
              catch(Exception exd)
              {
                  logger.error(exd);
              }
              
          }
    }
}
