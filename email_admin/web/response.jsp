<html>
<head>
  <title>FTD - Spell Check</title>
  <link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
  <script type="text/javascript" src="../js/copyright.js"></script>

<script>
    <%
        String textValue = (String)request.getAttribute("text");
        String originalValue = (String)request.getAttribute("original");
        String correctionsValue = (String)request.getAttribute("corrections");
        out.println("var text = " + "\"" + textValue + "\";");
        out.println("var original = " + "\"" + originalValue + "\";" );
        out.println("var corrections = " + correctionsValue + ";");
    %>
    var current = 0;
    var ignore = new Object();
    var formatted = false;
</script>

<script>
    function onChange()
    {
        if (current < corrections.length ) 
        {
            changeWord(current);
            nextWord();
        }
    }

    function onChangeAll()
    {
        if (current < corrections.length) 
        {
            var currentWord = text.substring(corrections[current].start, corrections[current].end);
            changeWord(current);
            for (var i = current + 1; i < corrections.length; i++) 
            {
                if (!ignore[i] && text.substring(corrections[i].start, corrections[i].end) == currentWord) 
                {
                    changeWord(i);
                    ignore[i] = true;
                }
            }
            nextWord();
        }
    }
    
    function onIgnore()
    {
        if (current < corrections.length) 
        {
            nextWord();
        }
    }

    function onIgnoreAll()
    {
        if (current < corrections.length) 
        {
            var currentWord = text.substring(corrections[current].start, corrections[current].end);
            for (var i = current + 1; i < corrections.length; i++) 
            {
            if (!ignore[i] && text.substring(corrections[i].start, corrections[i].end) == currentWord) 
                {
                    ignore[i] = true;
                }
            }
            nextWord();
        }
    }

  
    function onChangeSuggestions()
    {
        var suggestion = suggestions.options[suggestions.selectedIndex].text;
        if (suggestion != "no suggestions") 
        {
            word.value = suggestion;
        }
    }

    function onKeyPressWord()
    {
        if (event.keyCode == 13) 
        {
            onChange();
        }
    }

    function nextWord()
    {
        while (current++ < corrections.length && ignore[current]);
        update();
        if (current >= corrections.length) 
        {
            changeButton.disabled = true;
            changeAllButton.disabled = true;
            ignoreButton.disabled = true;
            ignoreAllButton.disabled = true;
            completed();
        }
    }

    function completed() 
    {
        alert("Spell check complete.");
        var parsedText = text.replace(/ <br> /g, "\n");
        opener.document.forms[0].<%=request.getAttribute("callerName")%>.value = parsedText;

        //var editor = opener.editor;
        //editor.setHTML(parsedText);

        self.close();
    }
    
    function update()
    {
        if (current < corrections.length) 
        {
            var html = "";
            html += '<head>';
            html += '</head>';
            html += '<body>';
            html += text.substring(0, corrections[current].start);
            html += '<span id="highlight" style="font-weight:bold;color:red">';
            html += text.substring(corrections[current].start, corrections[current].end);
            html += '</span>';
            html += text.substring(corrections[current].end, text.length);
            html += '</body>';
            preview.document.open();
            preview.document.write(html);
            preview.document.close();
            preview.highlight.scrollIntoView();

            suggestions.options.length = 0;
            var n = corrections[current].suggestions.length;

            if (n == 0) 
            {
                word.value = text.substring(corrections[current].start, corrections[current].end);
                suggestions.options[0] = new Option("no suggestions");
            }
            else 
            {
                word.value = corrections[current].suggestions[0];
                for (var i = 0; i < n; i++) 
                {
                    suggestions.options[i] = new Option(corrections[current].suggestions[i]);
                }
                suggestions.selectedIndex = 0;
            }
            
            word.select();
        }
        else 
        {
            var html = "";
            html += '<head>';
            html += '</head>';
            html += '<body marginwidth=4 marginheight=4 topmargin=4 leftmargin=4>';
            html += text;
            html += '</body>';
            preview.document.open();
            preview.document.write(html);
            preview.document.close();
            word.value = "";
            suggestions.options.length = 0;
            suggestions.options[0] = new Option("no suggestions");
        }
    }

    function changeWord(index)
    {
        var newText = "";
        newText += text.substring(0, corrections[index].start);
        newText += word.value;
        newText += text.substring(corrections[index].end, text.length);
        adjustOffsets(word.value.length - text.substring(corrections[index].start, corrections[index].end).length, index + 1);
        text = newText;
    }

    function adjustOffsets(delta, start)
    {
        for (i = start; i < corrections.length; i++) 
        {
            corrections[i].start += delta;
            corrections[i].end += delta;
        }
    }
    
</script>
</head>
<body onload="javascript:OnLoad();">

<script>
	function OnLoad()
	{
        update();

        if (corrections.length == 0)
            completed();
	}
</script>

  <!-- Header -->
  <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td width="20%"></td>
      <td width="60%" align="center" colspan="1" class="header">Spell Check</td>
      <td width="20%"></td>
    </tr>
    <tr>
      <td colspan="3"><hr/></td>
    </tr>
  </table>

  <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
    <tr>
      <td>
        <table width="100%" border="0" cellpadding="2" cellspacing="2">
          <tr>
            <td align="center" colspan="2">
              <iframe id=preview width=400 height=100></iframe>
            </td>
          </tr>
          <tr align="center">
            <td valign=top>Change to:<br>
              <input id=word style="width:16em" size=5 onkeypress="onKeyPressWord()"> 
            </td>
            <td valign=top>Suggestions:<br>
              <select id=suggestions style="WIDTH: 16em" size=5 onchange="onChangeSuggestions()"></select>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>

  <!-- Action Buttons -->
  <table width="98%" border="0" align="center" cellpadding="2" cellspacing="2">
    <tr>
      <td nowrap colspan="2" align="center">
        <input id=completedButton class=abutton type=button value="Done" onclick="completed()">
        <input id=changeButton class=abutton type=button value=Change onclick="onChange()"> 
        <input id=changeAllButton class=fbutton type=button value="Change All" onclick="onChangeAll()"> 
        <input id=ignoreButton class=fbutton type=button value=Ignore onclick="onIgnore()"> 
        <input id=ignoreAllButton class=fbutton type=button value="Ignore All" onclick="onIgnoreAll()"> 
      </td>
    </tr>
  </table>

  <!-- Footer -->
	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr><td>&nbsp;</td></tr>
    <tr>
    	<td class="disclaimer"></td><script>showCopyright();</script>  
    </tr>
	</table>

</body>
</html>