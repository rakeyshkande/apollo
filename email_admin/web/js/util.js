function getStyleObject(fieldName)
{
    if (document.getElementById && document.getElementById(fieldName))
        return document.getElementById(fieldName).style;
}

function digitOnlyListener()
{
    var input = event.keyCode;
    if (input < 48 || input > 57){
        event.returnValue = false;
    }
}

/*
    The limitTextarea will squelch any characters that exceed the limit value included in the method call.
    textarea - the textarea object
    limit - the number of characters allowed
*/
function limitTextarea(textarea, limit)
{
    if (textarea.value.length >= limit)
        event.returnValue = false;
}

/*
    The following function changes the control's style sheet to 'errorField'.
    elements - an Array of control names on the form
*/
function setErrorFields(elements)
{
    var element;
    for (var i = 0; i < elements.length; i++){
        element = document.getElementById(elements[i]);
        if (element != null){
            element.className = "errorField";
        }
    }
}
function setErrorField(element)
{
    var element = document.getElementById(element);
    if (element != null){
        element.className = "errorField";
    }
}

/*
    The following functions attached the 'onfocus' and 'onblur' events the the input
    elements.  When a control on the page gains focus, the control's border changes to red.
    When focus is lost, the control's border changes back to default.

    elements - an Array of control names on the form
    element - a control name on the form
*/
function addDefaultListenersArray(elements)
{
    var element;
    for (var i = 0; i < elements.length; i++){
        element = document.getElementById(elements[i]);
        if (element != null)
            addDefaultListenersSingle(elements[i]);
    }
}
function addDefaultListenersSingle(element)
{
    document.getElementById(element).attachEvent("onfocus", fieldFocus);
    document.getElementById(element).attachEvent("onblur", fieldBlur);
}
function fieldFocus()
{
    var element = event.srcElement;
    element.style.borderWidth = 2;
    element.style.borderColor = "red";
}
function fieldBlur()
{
    var element = event.srcElement;
    element.style.borderWidth = 2;
    element.style.borderColor = document.body.style.backgroundColor;
}

/*
    The following functions change the cursor UI when an image triggers a 'mouseover' event.

    elements - an Array of control names on the form
*/
function addImageCursorListener(elements)
{
    var element;
    for (var i = 0; i < elements.length; i++){
        element = document.getElementById(elements[i]);
        if (element != null){
            element.attachEvent("onmouseover", imageOver);
            element.attachEvent("onmouseout", imageOut);
        }
    }
}
function imageOver()
{
    document.forms[0].style.cursor = "hand";
}
function imageOut()
{
    document.forms[0].style.cursor = "default";
}

/*
    The following functions are used to dis/enable fields.

    elements - an Array of control names on the form
    access - boolean value used to set the disabled property
*/
function disableFields(elements)
{
    setFieldsAccess(elements, true)
}
function enableFields(elements)
{
    setFieldsAccess(elements, false)
}
function setFieldsAccess(elements, access)
{
    var element;
    for (var i = 0; i < elements.length; i++){
        element = document.getElementById(elements[i]);
        if (element != null){
            setFieldAccess(element, access)
        }
    }
}
function setFieldAccess(element, access)
{
    element.disabled = access;
}

        var dtCh= "/";
        var minYear=00;
        var maxYear=99;

            function isInteger(s){
              var i;
                for (i = 0; i < s.length; i++){   
                    // Check that current character is number.
                    var c = s.charAt(i);
                    if (((c < "0") || (c > "9"))) return false;
                }
                // All characters are numbers.
                return true;
            }

            function stripCharsInBag(s, bag){
              var i;
                var returnString = "";
                // Search through string's characters one by one.
                // If character is not in bag, append to returnString.
                for (i = 0; i < s.length; i++){   
                    var c = s.charAt(i);
                    if (bag.indexOf(c) == -1) returnString += c;
                }
                return returnString;
            }

            function daysInFebruary (year){
              // February has 29 days in any year evenly divisible by four,
                // EXCEPT for centurial years which are not also divisible by 400.
                return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
            }
            function DaysArray(n) {
              for (var i = 1; i <= n; i++) {
                this[i] = 31
                if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
                if (i==2) {this[i] = 29}
               } 
               return this
            }

            function isDate(dtStr){
              var daysInMonth = DaysArray(12)
              var pos1=dtStr.indexOf(dtCh)
              var pos2=dtStr.indexOf(dtCh,pos1+1)
              var strMonth=dtStr.substring(0,pos1)
              var strDay=dtStr.substring(pos1+1,pos2)
              var strYear=dtStr.substring(pos2+1)
              strYr=strYear
              if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
              if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
              for (var i = 1; i <= 3; i++) {
                if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
              }
              month=parseInt(strMonth)
              day=parseInt(strDay)
              year=parseInt(strYr)
              if (pos1==-1 || pos2==-1){
                alert("The date format should be : mm/dd/yyyy")
                return false
              }
              if (strMonth.length<1 || month<1 || month>12){
                alert("Please enter a valid month")
                return false
              }
              if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
                alert("Please enter a valid day")
                return false
              }
              if (strYear.length < 2 || year==0){
                alert("Please enter a valid 2 digit year between "+minYear+" and "+maxYear)
                return false
              }
              if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
                alert("Please enter a valid date")
                return false
              }
            return true
            }

           function checkDate(newDate)
           {
            var dt=newDate;
            if (isDate(dt.value)==false){
              return false
            }
              return true
           }
           

/*
  The following methods are used to display a wait message to the user
  while an action is being executed.  The following coding convention should
  be used for the wait div's table structure:

  waitDiv - id of the wait div
  waitMessage - id of the TD which will contain the message
  waitTD - id of the TD which will contain the dots (...)

  Parameters
  conent - The div id containing the content to be hidden
  wait - The div id containing the wait message
  message - Optional, if provided the message to be displayed, otherwise, "Processing"
            will be the message
*/
function showWaitMessage(content, wait, message){
   var content = document.getElementById(content);
   //var height = content.offsetHeight;
   var waitDiv = document.getElementById(wait + "Div").style;
   var waitMessage = document.getElementById(wait + "Message");
   _waitTD = document.getElementById(wait + "TD");

   content.style.display = "none";
   waitDiv.display = "block";
   waitDiv.height = 40;
   waitMessage.innerHTML = (message) ? message : "Processing";
   clearWaitMessage();
   updateWaitMessage();
}

var _waitTD = "";
function clearWaitMessage(){
   _waitTD.innerHTML = "";
   setTimeout("clearWaitMessage()", 5000);
}
function updateWaitMessage(){
   _waitTD.innerHTML += " . ";
   setTimeout("updateWaitMessage()", 1000);
}

/*
   Calls helper methods to limit navigation options.
*/
function setNavigationHandlers()
{
   document.oncontextmenu = contextMenuHandler;
   document.onkeydown = backKeyHandler;
}

/*
   Disables the right button mouse click.
*/
function contextMenuHandler()
{
   return false;
}

/*
   Disables the various navigation keyboard commands.
*/
function backKeyHandler()
{
   // backspace
   if (window.event && window.event.keyCode == 8){
      var formElement = false;

      for(i = 0; i < document.forms[0].elements.length; i++){
         if(document.forms[0].elements[i].name == document.activeElement.name){
            formElement = true;
            break;
         }
      }

      if(formElement == false || (document.activeElement.type != "text" && document.activeElement.type != "textarea" && document.activeElement.type != "password")){
         window.event.cancelBubble = true;
         window.event.returnValue = false;
         return false;
      }
   }

   // F5 and F11
   if (window.event.keyCode == 122 || window.event.keyCode == 116){
      window.event.keyCode = 0;
      event.returnValue = false;
      return false;
   }

   // Atl + Left Arrow
   if(window.event.altiLeft){
      window.event.returnValue = false;
      return false;
   }

   // Atl + Right Arrow
   if(window.event.altKey){
      window.event.returnValue = false;
      return false;
   }
}