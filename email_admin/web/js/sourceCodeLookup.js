/*
 * Constructor
 */
SourceCodePopup = function() {
   this.focusObj = null;
};


/*
 * Class functions
 */
SourceCodePopup.setup = function(params) {
   window.popup = Popup.setup(params, new SourceCodePopup());
   window.popup.create();
   window.popup.showAtElement(params.displayArea);
};

SourceCodePopup.enterSearchSourceCode = function(ev) {
   if (ev.keyCode == 13)
      SourceCodePopup.searchSourceCode();
};

SourceCodePopup.searchSourceCode = function(ev) {
   var sourceCode = document.getElementById("sourceCodeInput");
   var sourceCodeValue = stripWhitespace(sourceCode.value);
   if (sourceCodeValue == "" || sourceCodeValue.length < 2) {
      alert("Please correct the marked fields");
      sourceCode.focus();
      sourceCode.style.backgroundColor = "pink";
   }
   else {
      SourceCodePopup.openSourceCodePopup();
   }
};

SourceCodePopup.openSourceCodePopup = function() {
   var url_source = "SourceCodeServlet?dateFlag=Y&sourceCodeInput=" + document.all("sourceCodeInput").value;
   var modal_dim = "dialogWidth:800px; dialogHeight:500px; center:yes; status=0";
   var ret = window.showModalDialog(url_source,"", modal_dim);
   Popup.hide();

   var form = document.forms[0];
   if (ret && ret != null && ret[0]){
      form.source_code.value = ret[0];
      form.source_code_description.value = ret[1];
      form.partner_id.value = ret[2];
      form.company_id.value = ret[3];
      SourceCodePopup.submitSourceCodeChange();
   }
};

SourceCodePopup.submitSourceCodeChange = function () {
   document.forms[0].action = "SourceCodeServlet";
   document.forms[0].submit();
};


/*
 * Member Functions
 */
SourceCodePopup.prototype.renderContent = function(div) {
   var table = Popup.createElement("table");
   table.setAttribute("cellSpacing", "2");
   table.setAttribute("cellPadding", "2");
   div.appendChild(table);

   var thead, tbody, tfoot, row, cell, input, image;
   thead = Popup.createElement("thead", table);
   row = Popup.createElement("tr", thead);

   cell = Popup.createElement("td", row);
   cell.setAttribute("className", "PopupHeader");
   cell.setAttribute("colSpan", "2");
   cell.style.cursor = "move";
   cell.popup = window.popup;
   cell.appendChild(document.createTextNode("Source Code Lookup"));
   Popup.addDragListener(cell);

   tbody = Popup.createElement("tbody", table);
   row = Popup.createElement("tr", tbody);
   cell = Popup.createElement("td", row);

   row = Popup.createElement("tr", tbody);
   cell = Popup.createElement("td", row);
   cell.setAttribute("className", "label");
   cell.appendChild(document.createTextNode("Source Code or Description:"));

   cell = Popup.createElement("td", row);
   input = Popup.createElement("input", cell);
   this.focusObj = input;
   input.setAttribute("id", "sourceCodeInput");
   input.setAttribute("tabIndex", "85");
   input.setAttribute("TYPE", "text");
   Popup.addEvent(input, "blur", fieldBlur);
   Popup.addEvent(input, "focus", fieldFocus);
   Popup.addEvent(input, "keydown", SourceCodePopup.enterSearchSourceCode);

   tfoot = Popup.createElement("tfoot", table);
   row = Popup.createElement("tr", tfoot);
   cell = Popup.createElement("td", row);
   cell.setAttribute("colSpan", "2");
   cell.setAttribute("align", "right");

   image = Popup.createElement("img", cell);
   image.setAttribute("alt", "Search");
   image.setAttribute("id", "sourceCodeSearch");
   image.setAttribute("src", "images/button_search.gif");
   image.setAttribute("tabIndex", "86");
   Popup.addEvent(image, "click", SourceCodePopup.searchSourceCode);
   Popup.addEvent(image, "keydown", SourceCodePopup.enterSearchSourceCode);

   image = Popup.createElement("img", cell);
   image.setAttribute("alt", "Close screen");
   image.setAttribute("id", "sourceCodeClose");
   image.setAttribute("src", "images/button_close.gif");
   image.setAttribute("tabIndex", "87");
   Popup.addEvent(image, "click", Popup.hide);
   Popup.addEvent(image, "keydown", Popup.pressHide);
};

SourceCodePopup.prototype.setFocus = function () {
   if (this.focusObj) {
      this.focusObj.focus();
   }
};

SourceCodePopup.prototype.setValues = function (params) {
   document.all("sourceCodeInput").value = document.forms[0].source_code.value;
};