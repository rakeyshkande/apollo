<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:import href="footer.xsl"/>
  <xsl:output method="html" indent="yes"/>
  <xsl:variable name="headerName" select="'Create Program'"/>
  <xsl:template match="/root">
<html>

<head>
      <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
      <link rel="stylesheet" type="text/css" href="css/calendar.css"/>
      <script type="text/javascript" src="js/util.js"/>
      <script type="text/javascript" src="js/calendar.js"></script>
      <title><xsl:value-of select="pageData/data[name='pageTitle']/value"/></title>

      <script type="text/javascript" language="JavaScript">
      var isDefault = '<xsl:value-of select="PROGRAM/program_default"/>';
      
      var pageMode = '<xsl:value-of select="pageData/data[name='pagemode']/value"/>';
      var searchString = '<xsl:value-of select="pageData/data[name='searchString']/value"/>'; 	
 	var originalSourceCodes = new Array(  <xsl:for-each select="ORIGINAL_SOURCE_CODES/SOURCE_CODE">
                                      <xsl:choose>
                                        <xsl:when test="position()=last()">"<xsl:value-of select="VALUE"/>"</xsl:when>
                                        <xsl:otherwise>"<xsl:value-of select="VALUE"/>", </xsl:otherwise>
                                      </xsl:choose>
                                    </xsl:for-each>);
	  var securitytoken_js = '<xsl:value-of select="pageData/data[name='securitytoken']/value"/>';
	  var securitycontext_js = '<xsl:value-of select="pageData/data[name='context']/value"/>';
      <![CDATA[
        var sortitems = 1;  // Automatically sort items within lists? (1 or 0)
        var __currenttab;

	function trim(str)
	{
	   return str.replace(/^\s*|\s*$/g,"");
	}



        function tabmouseout(tab)
        {
          if(tab.className != 'here')
          {
            tab.className = 'button';
          }
        }

        function tabmouseover(tab)
        {
          if(tab.className != 'here')
          {
            tab.className = 'hover';
          }
        }

        function showtab(tab,sectionType)
        {
          tab = document.getElementById(sectionType);

          if(__currenttab)
          __currenttab.className = 'button';
          tab.className = 'here';
          __currenttab = tab;
          showtabcontent(tab.id);

          var form = document.forms[0];
          form.sectionType.value = sectionType;
          form.selectedTab.value = tab.id;
        }

        var currentcontent;
        function showtabcontent(tabid)
        {
          var content;
          var contentid = tabid + 'DIV';
          content = document.getElementById(contentid);
          if(currentcontent)
          currentcontent.className = 'hidden';
          content.className = 'visible';
          //content.focus();
          currentcontent = content;
        }

        function initialize(tabid)
        {
          __currenttab = document.getElementById(tabid);
          __currenttab.className = 'here';
          currentcontent = document.getElementById(tabid + 'DIV');
          currentcontent.className = 'visible';
        }

        function exitForm()
        {
            var action = "exit";
            var form = document.forms[0];
            form.action = "EmailAdminProgramServlet?" + action;
            form.submit();
        }

		function exit() {
            var form = document.forms[0];
            form.action = "EmailAdminMenuServlet?menuAction=exit"
            				+ "&securitytoken=" + securitytoken_js
            				+ "&context=" + securitycontext_js;
            form.submit();
		}

        function checkFields(form)
        {

            var defaultSectionCB = document.forms[0].selDefaultObj;

            if(form.programName.value == "")
            {
                alert('Program Name required');
                return false;
            }

            if(form.effectiveStartDate.value == "")
            {
                alert('Effective Start Date required');
                return false;
            }




		
	    //if default is not checked
	    if(!defaultSectionCB.checked){
	            if(form.effectiveEndDate.value == "")
        	    {
                	alert('Effective End Date required');
	                return false;
        	    }
		}

            var pickList = document.getElementById("PickList");
            var pickOptions = pickList.options;
            
            //per defect #272 The check source code is being removed
            //if(pickOptions.length <= 0)
            //{
            //    alert('Source code is required');
            //    return false;
            //}

            if(form.selSubject.value == "")
            {
                alert('Subject section content must be selected');
                return false;
            }

            if(form.selHeader.value == "")
            {
                alert('Header section content must be selected');
                return false;
            }

            if(form.selGuarantee.value == "")
            {
                alert('Guarantee section content must be selected');
                return false;
            }

            if(form.selMarketing.value == "")
            {
                alert('Marketing section content must be selected');
                return false;
            }

            if(form.selContact.value == "")
            {
                alert('Contact Us section content must be selected');
                return false;
            }

            if(sectionMissing()) {
				return false;
            }

            if(form.selDefault.value == "Y" && form.selSourceCodes.value == "") {
				alert("Source code is required for default program.");
				return false;
            }

            var endDtString = document.forms[0].effectiveEndDate.value;


			if (endDtString.length > 0 && !endDateGreaterThanStartDate()) {
 				alert("Effective End Date cannot be smaller than Effective Start Date.");
   				return false;
    	  	}
        	//else if(defaultSectionCB.checked && endDtString.length > 0) {
        	else if(form.selDefault.value == "Y" && endDtString.length > 0) {
			//Commented out per julie, 4/9/03 emueller
      			//alert("Default program cannot have Effective End Date.");
			//	return false;
 	  		}

 	  		// If program originally is custom and have source codes related to them, don't all
 	  		// to be updated to default.
 	  		if(isDefault != "Y" && originalSourceCodes.length > 0 && defaultSectionCB.checked) {
 	  			alert("Cannot update program to be default because it has associated source codes.");
 	  			return false;
 	  		}

 	  		// If updating a default program, do not allow source codes to move from right box to left.
 	  		// Because a custom program can be updated to a default program, this test is
 	  		// need only for a original default program.
 	  		//if(pageMode == "update" && form.selDefault.value == "Y" && !keepOriginalSources()) {
 	  		if(pageMode == "update" && isDefault == "Y" && !keepOriginalSources()) {
 	  			//alert("Cannot remove source code association from default program.");
 	  			return false;
 	  		}

            return true;
        }

		function sectionMissing() {
			if(document.forms[0].ExistingSection_Subject.value == "-1") {
				alert("Subject section is required.");
				return true;
			} else if (document.forms[0].ExistingSection_Header.value == "-1") {
				alert("Header section is required.");
				return true;
			} else if (document.forms[0].ExistingSection_Guarantee.value == "-1") {
				alert("Guarantee section is required.");
				return true;
			} else if (document.forms[0].ExistingSection_Marketing.value == "-1") {
				alert("Marketing section is required.");
				return true;
			} else if (document.forms[0].ExistingSection_Contact.value == "-1") {
				alert("Contact section is required.");
				return true;
			}
			return false;
		}

		function getDateFromString(dtStr) {

			var dtCh= "/";

			var pos1=dtStr.indexOf(dtCh);
			var pos2=dtStr.indexOf(dtCh,pos1+1);
			var strMonth=dtStr.substring(0,pos1);
			var strDay=dtStr.substring(pos1+1,pos2);
			var strYear=dtStr.substring(pos2+1);

			return new Date(strYear, strMonth, strDay);
		}

		function aGreaterThanB(a, b) {
			return a.getTime() >= b.getTime();
  		}

		function endDateGreaterThanStartDate() {
			var startDtString = document.forms[0].effectiveStartDate.value;
			var endDtString = document.forms[0].effectiveEndDate.value;
			var startDt = getDateFromString(startDtString);
			var endDt = getDateFromString(endDtString);

			return aGreaterThanB(endDt, startDt);
		}

		function keepOriginalSources() {
			//Check if the souce codes in the right box has at least every source in the original array.

			for (var i=0; i<originalSourceCodes.length; i++) {
				if (!valueInPickList(originalSourceCodes[i])) {
					alert("Cannot remove source code from it's default program: " + originalSourceCodes[i]);
					return false;
				}
			}

            return true;

		}

		function valueInPickList(value) {

            var pickList = document.getElementById("PickList");
            var pickOptions = pickList.options;
            var pickOLength = pickOptions.length;
            var count = 0;

            while (count != pickOLength)
            {
                count++;
                var tempValue = pickOptions[count - 1].value;

                if(tempValue == value) {
					return true;
                }
            }
			return false;
		}

		function goSearch()
  		{
   			var form = document.forms[0];
     		form.action = "EmailAdminSelectionServlet?search";
      		form.submit();
        }

        function submitForm(action)
        {

            //check that all sections are selected
            var form = document.forms[0];

            storeSourceCodeList();

            //store unselected source codes - used for update
            //storeUnselectedSourceCodes();

            //store selected source codes
            storeSelectedSourceCodes();

            //set default checkbox
            setDefault();

            var defaultSectionCB = document.forms[0].selDefaultObj;

			// set default section value
   			//setDefault(defaultSectionCB);

            if(checkFields(form))
            {

            	showWaitMessage("hideContent", "wait", "Processing");
            }
            else
            {
                return false;
            }

       
 		form.action = "EmailAdminProgramServlet?" + action;
            form.submit();
        }

        function previewEmail(previewmode)
        {
           var form = document.forms[0];

           //store unselected source codes - used for update
           //storeUnselectedSourceCodes();

           //store selected source codes
           storeSelectedSourceCodes();

		   setDefault();

           if(!checkFields(form))
           {
               return false;
           }

           var myObject = new Object();

           myObject.programId = form.programId.value;

           var sFeatures="dialogHeight: " + 400 + "px;dialogWidth: " + 800 + "px;";


           //"&unselSourceCodes=" + form.unselSourceCodes.value +

		   // Instead of getting unselSourceCodes from request, get from DB.
           var url_source="EmailAdminProgramServlet?" + "actionMethod=" + previewmode +
           "&programId=" + form.programId.value +
           "&programName=" + form.programName.value +
           "&programDescription=" + form.programDescription.value +
           "&effectiveStartDate=" + form.effectiveStartDate.value +
           "&effectiveEndDate=" + form.effectiveEndDate.value +
           "&selDefault=" + form.selDefault.value +
           "&selSubjectId=" + form.selSubjectId.value +
           "&selHeaderIdText=" + form.selHeaderIdText.value +
           "&selHeaderIdHtml=" + form.selHeaderIdHtml.value +
           "&selGuaranteeIdText=" + form.selGuaranteeIdText.value +
           "&selGuaranteeIdHtml=" + form.selGuaranteeIdHtml.value +
           "&selMarketingIdText=" + form.selMarketingIdText.value +
           "&selMarketingIdHtml=" + form.selMarketingIdHtml.value +
           "&selContactIdText=" + form.selContactIdText.value +
           "&selContactIdHtml=" + form.selContactIdHtml.value  +
           "&selSourceCodes=" + form.selSourceCodes.value +
           "&sectionType=" + form.sectionType.value +
           "&securitytoken=" + securitytoken_js +
           "&context=" + securitycontext_js;

           var test = window.showModalDialog(url_source, myObject, sFeatures);
			//var test = window.open(url_source, 'Detail', 'width=500,height=300,scrollbars=yes,addressbar=yes,status=yes');

		   // Redirect to Update Program page.
		   if(typeof test != 'undefined'){

			   form.programId.value = test;
	   		   form.action = "EmailAdminProgramServlet?viewupdate" ;
	           form.submit();
		   }

        }

        function storeSelectedSourceCodes()
        {
            var form = document.forms[0];

            //store selected source codes
            var selSourceCodes = "";
            var pickList = document.getElementById("PickList");
            var pickOptions = pickList.options;
            var pickOLength = pickOptions.length;
            var count = 0;

            while (count != pickOLength)
            {
                count++;
                var tempValue = pickOptions[count - 1].value;

                if(count == pickOLength)
                {
                    selSourceCodes = selSourceCodes + tempValue;
                }
                else
                {
                    selSourceCodes = selSourceCodes + tempValue + "~";
                }
            }

            form.selSourceCodes.value = selSourceCodes;
        }

        function storeUnselectedSourceCodes()
        {
            var form = document.forms[0];

            //store selected source codes
            var unselSourceCodes = "";
            var selectList = document.getElementById("SelectList");
            var selectOptions = selectList.options;
            var selectOLength = selectOptions.length;
            var count = 0;

            while (count != selectOLength)
            {
                count++;
                var tempValue = selectOptions[count - 1].value;

                if(count == selectOLength)
                {
                    unselSourceCodes = unselSourceCodes + tempValue;
                }
                else
                {
                    unselSourceCodes = unselSourceCodes + tempValue + "~";
                }
            }

            form.unselSourceCodes.value = unselSourceCodes;
        }

        function storeSourceCodeList()
        {
            var form = document.forms[0];

            //store source code list
            var sourceCodes = "";
            var selectList = document.getElementById("PickList");
            var selectOptions = selectList.options;
            var selectOLength = selectOptions.length;
            var count = 0;

            while (count != selectOLength)
            {
                count++;
                var tempValue = selectOptions[count - 1].value;

                if(count == selectOLength)
                {
                    sourceCodes = sourceCodes + tempValue;
                }
                else
                {
                    sourceCodes = sourceCodes + tempValue + "~";
                }
            }

            form.sourceCodeList.value = sourceCodes;
        }

	function setDefault()
        {

            var form = document.forms[0];

            if (form.selDefaultObj.checked) {
                form.selDefault.value = "Y";
            } else {
                form.selDefault.value = "N";
            }
            // Default program cannot be updated to non-default.
            if(isDefault == "Y") {
				form.selDefault.value = "Y";
            }
        }
        function relatedSections()
        {
            var form = document.forms[0];
           var myObject = new Object();
           myObject.oldtext = "test";
           var sFeatures="dialogHeight: " + 400 + "px;dialogWidth: " + 800 + "px;";

           var url_source="EmailAdminPopupProgramServlet?programId=" + form.programId.value + "&programName=" + form.programName.value;

           window.showModalDialog(url_source, myObject, sFeatures);
        }

        function init(){
          setNavigationHandlers();
          initIt();
        }

        // Control flags for list selection and sort sequence
        // Sequence is on option value (first 2 chars - can be stripped off in form processing)
        // It is assumed that the select list is in sort sequence initially

        var singleSelect = true;  // Allows an item to be selected once only
        var sortSelect = true;  // Only effective if above flag set to true
        var sortPick = true;  // Will order the picklist in sort sequence

        // Initialise - invoked on load
        function initIt() {
          var selectList = document.getElementById("SelectList");
          var pickList = document.getElementById("PickList");
          var pickOptions = pickList.options;
          //pickOptions[0] = null;  // Remove initial entry from picklist (was only used to set default width)
        }

        // Adds a selected item into the picklist

// -------------------------------------------------------------------
// sortSelect(select_object)
//   Pass this function a SELECT object and the options will be sorted
//   by their text (display) values
// -------------------------------------------------------------------
function sortSelectBox(obj) {

	var o = new Array();
	if (obj.options==null) { return; }
	for (var i=0; i<obj.options.length; i++) {
		o[o.length] = new Option( obj.options[i].text, obj.options[i].value, obj.options[i].defaultSelected, obj.options[i].selected) ;
		}
	if (o.length==0) { return; }
	o = o.sort( 
		function(a,b) { 

			if ((a.text+"") < (b.text+"")) { return -1; }
			if ((a.text+"") > (b.text+"")) { return 1; }
			return 0;
			} 
		);


	for (var i=0; i<o.length; i++) {
		obj.options[i] = new Option(o[i].text, o[i].value, o[i].defaultSelected, o[i].selected);
		}
	}


        function addIt() {
          var sourceEntryBox = document.getElementById("sourceCodeEntry");
          var sourceString = sourceEntryBox.value;
          var pickOptions = document.getElementById("PickList");


 	var currentValue = "";
	for(i=0; i<sourceString.length; i++){

		if(sourceString.charAt(i) == ","){
			if(currentValue != ""){
				    var pickOLength = pickOptions.length;
				    pickOptions[pickOLength] = new Option(currentValue);
				    pickOptions[pickOLength].value = currentValue;				
				    currentValue = "";	
			}
		}
		else{
			currentValue = currentValue + sourceString.charAt(i);
			currentValue = trim(currentValue);
		}	
	}
	if(currentValue != ""){
            var pickOLength = pickOptions.length;
            pickOptions[pickOLength] = new Option(currentValue);
            pickOptions[pickOLength].value = currentValue;
	}

	sourceEntryBox.value = "";
  	

  	sortSelectBox(pickOptions);

	  
	  //remove duplicate items
         var i = 0;
	 var lastValue = "";
	 pickOLength = pickOptions.length;
          while (pickOLength > 0 && i < pickOLength) {          	
		tempValue = pickOptions[i].value;
		if(tempValue == lastValue){
			pickOptions[i] = null;
		}else{
		  lastValue = tempValue;
		  i++;
		  }
		pickOLength = pickOptions.length;		
	     }
	      

	      
	  
        }

        // Deletes an item from the picklist
        function delIt() {

          var sourceEntryBox = document.getElementById("sourceCodeEntry");
          var sourceString = sourceEntryBox.value;

          var pickList = document.getElementById("PickList");
          var pickIndex = pickList.selectedIndex;
          var pickOptions = pickList.options;

          while (pickIndex > -1) {


	    if(sourceString.length > 0){
	        sourceString = sourceString + ",";
	        }
            sourceString = sourceString + pickList[pickIndex].value;
           
            pickOptions[pickIndex] = null;
            pickIndex = pickList.selectedIndex;
          }
          
         sourceEntryBox.value = sourceString;

        }


        function changeSection(action)
        {
			showWaitMessage("hideContent", "wait", "Reloading section");

            storeSourceCodeList();

            storeSelectedSourceCodes();

            //storeUnselectedSourceCodes();
            setDefault();
            


            var form = document.forms[0];
            form.action = "EmailAdminProgramServlet?" + action + "SelectExistingSection"
            //showWaitMessage("content", "wait", "Loading");
            form.submit();
        }

        function changeDefaultFlag(action)
        {
			showWaitMessage("hideContent", "wait", "Reloading source codes");
            storeSourceCodeList();
            storeSelectedSourceCodes();
            //storeUnselectedSourceCodes();
            setDefault();


            var form = document.forms[0];
            form.changeDefault.value = "Y";
            form.action = "EmailAdminProgramServlet?" + action + "SelectExistingSection"
            form.submit();
        }

      ]]>
	</script>
</head>

<body marginheight="0" marginwidth="0" leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" bgcolor="#FFFFFF" onload="javascript:init();">
<form name="formname" method="post" action="">
<input type="hidden" name="securitytoken" value="{pageData/data[name='securitytoken']/value}"/>
<input type="hidden" name="context" value="{pageData/data[name='context']/value}"/>
<input type="hidden" name="programId" value="{PROGRAM/program_id}"/>
<input type="hidden" name="sectionType" value="Subject"/>
<input type="hidden" name="selDefault" value=""/>
<input type="hidden" name="selSubject" value="{PROGRAM/selected_subject}"/>
<input type="hidden" name="selHeader" value="{PROGRAM/selected_header}"/>
<input type="hidden" name="selGuarantee" value="{PROGRAM/selected_guarantee}"/>
<input type="hidden" name="selMarketing" value="{PROGRAM/selected_marketing}"/>
<input type="hidden" name="selContact" value="{PROGRAM/selected_contact}"/>
<input type="hidden" name="selSubjectId" value="{SELECTED_SUBJECT/SECTION/SECTION_CONTENT_TEXT/section_id}"/>
<input type="hidden" name="selHeaderIdText" value="{SELECTED_HEADER/SECTION/SECTION_CONTENT_TEXT/section_id}"/>
<input type="hidden" name="selHeaderIdHtml" value="{SELECTED_HEADER/SECTION/SECTION_CONTENT_HTML/section_id}"/>
<input type="hidden" name="selGuaranteeIdText" value="{SELECTED_GUARANTEE/SECTION/SECTION_CONTENT_TEXT/section_id}"/>
<input type="hidden" name="selGuaranteeIdHtml" value="{SELECTED_GUARANTEE/SECTION/SECTION_CONTENT_HTML/section_id}"/>
<input type="hidden" name="selMarketingIdText" value="{SELECTED_MARKETING/SECTION/SECTION_CONTENT_TEXT/section_id}"/>
<input type="hidden" name="selMarketingIdHtml" value="{SELECTED_MARKETING/SECTION/SECTION_CONTENT_HTML/section_id}"/>
<input type="hidden" name="selContactIdText" value="{SELECTED_CONTACT/SECTION/SECTION_CONTENT_TEXT/section_id}"/>
<input type="hidden" name="selContactIdHtml" value="{SELECTED_CONTACT/SECTION/SECTION_CONTENT_HTML/section_id}"/>
<input type="hidden" name="selectedTab" value=""/>
<input type="hidden" name="selSourceCodes" value=""/>
<input type="hidden" name="unselSourceCodes" value=""/>
<input type="hidden" name="sourceCodeList" value=""/>
<!-- back link uses this param to go to search page -->
<input type="hidden" name="option" value="{pageData/data[name='option']/value}"/>
<input type="hidden" name="searchString" value="{pageData/data[name='searchString']/value}"/>
<input type="hidden" name="pagemode" value="{pageData/data[name='pagemode']/value}"/>
<input type="hidden" name="changeDefault" value=""/>
<xsl:variable name="headerName" select="pageData/data[name='headerName']/value"/>


<table width="98%" border="0" align="center" cellpadding="0" cellspacing="2">
    <tr>
        <td width="20%" align="left"><img border="0" height="60" src="images/wwwftdcom.gif" width="135"/></td>
        <td width="60%" align="center" colspan="1" class="header"><xsl:value-of select="pageData/data[name='headerName']/value"/></td>
        <td width="20%" align="right" class="label">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="3"><hr/></td>
    </tr>
	<tr>
		<td colspan="3"><font color="red"><xsl:value-of select="ERROR/@message"/></font></td>
	</tr>
</table>

<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td><a href="#" onclick="exitForm();" TABINDEX="-1">Email Maintenance</a>

        <xsl:if test="starts-with($headerName, 'Copy') or starts-with($headerName, 'Update')">
        &nbsp;&nbsp;&nbsp;&nbsp;<a href="#" onclick="javascript:goSearch();" TABINDEX="-1">Back</a>
        </xsl:if>
        </td>
		<td>
			<div align="right">
			  <input type="button" name="Exit" Value="Exit" onclick="exit();"  TABINDEX="12"/>
		      </div></td>
    </tr>
</table>

<!-- Content to hide once search begins -->
<div id="hideContent" style="display:block">
<table width="98%" align="center" cellpadding="1" cellspacing="1">
	<tr>
		<td class="tblheader" colspan="1">Program Information</td>
	</tr>
    <tr>
		<td>
		<table border="0" width="98%" cellpadding="1" cellspacing="1">
		<tr>
			<td width="11%" class="label">Name:</td>
			<td width="25%" >
			<xsl:choose>
				<xsl:when test="starts-with($headerName, 'Update')">
					<input name="programName" type="text" size="30" maxlength="30" value="{PROGRAM/program_name}" />
					<!--input name="programName" type="hidden" value="{PROGRAM/program_name}"/-->
				</xsl:when>
				<xsl:otherwise>
					<input name="programName" type="text" size="30" maxlength="30" value="{PROGRAM/program_name}" TABINDEX="1"/>
				</xsl:otherwise>
			</xsl:choose>
			</td>
			<td width="10%" class="label">Description:</td>
			<td width="40%" ><input type="text" name="programDescription" size="60" maxlength="100" value="{PROGRAM/description}"  TABINDEX="2"/></td>
			<td width="22%" >
        <xsl:choose>
          <xsl:when test="pageData/data[name='pagemode']/value = 'create'">
          <!-- When create program, refresh available source list when checkbox is checked/unchecked. -->
          <!-- If checked, return sources with no default program (minus selected); otherwise return all -->
                <xsl:choose>
                  <xsl:when test="PROGRAM[default='Y']">
         				<input type="checkbox" name="selDefaultObj" checked="true" TABINDEX="-1" onclick="changeDefaultFlag({pageData/data[name='mode']/value})"/>
                  </xsl:when>
                  <xsl:otherwise>
                      <input type="checkbox" name="selDefaultObj" TABINDEX="-1" onclick="changeDefaultFlag({pageData/data[name='mode']/value})"/>
                  </xsl:otherwise>
                </xsl:choose>
          </xsl:when>
          <xsl:otherwise>
                <xsl:choose>
                  <xsl:when test="PROGRAM[default='Y']">
                  	  <xsl:choose>
                  		<xsl:when test="pageData/data[name='pagemode']/value = 'update' and PROGRAM[program_default='Y']">
                  			<!-- Do not allow default program to be updated to non-default -->
                      		<input type="checkbox" name="selDefaultObj" checked="true" readonly="true" TABINDEX="-1"/>
                      		<!--input type="hidden" name="selDefaultObj" value="on"/-->
						</xsl:when>
						<xsl:otherwise>
							<input type="checkbox" name="selDefaultObj" checked="true"  TABINDEX="-1"/>
						</xsl:otherwise>
                      </xsl:choose>
                  </xsl:when>
                  <xsl:otherwise>
                      <input type="checkbox" name="selDefaultObj" TABINDEX="-1"/>
                  </xsl:otherwise>
                </xsl:choose>
          </xsl:otherwise>
       </xsl:choose>


        &nbsp;<span class="label">Default</span>
    </td>
    </tr>
  		<tr>
			<td class="label">Effective Start:</td>
			<td>
          <input name="effectiveStartDate" type="text" maxlength="10" value="{PROGRAM/start_date}"  TABINDEX="3" OnChange="checkDate(this);"/>&nbsp;&nbsp;
          <!--input name="effectiveStartDate" type="text" maxlength="10"/-->&nbsp;&nbsp;
          <img id="startDate" src="images/calendar.gif" width="20" height="20" align="ABSMIDDLE"/>
      </td>
	  <td class="label">Effective End:</td>
	  <td colspan="2">
          <input name="effectiveEndDate" type="text" maxlength="10" value="{PROGRAM/end_date}"  TABINDEX="4" OnChange="checkDate(this);"/>&nbsp;&nbsp;
          <img id="endDate" src="images/calendar.gif" width="20" height="20" align="ABSMIDDLE"/>
		  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <xsl:choose>
            <xsl:when test="pageData/data[name='headerName']/value ='Update Program'">
              <a href="#" onclick="relatedSections();" TABINDEX="-1"><font size="-2"><b>Related Sections</b></font></a>
            </xsl:when>
          </xsl:choose>
      </td>

	   	</tr>
		</table>
		<table border="0" width="98%" cellpadding="1" cellspacing="1">
			<tr>
				<td width="10%" class="label">Source Codes:</td>
				<td width="98">
          <table>
          <tr>
          <td>
<textarea name="sourceCodeEntry" cols="20" rows="6" TABINDEX="6"></textarea>
          </td>
          <td>
            <img src="images/nextItem.gif" ONCLICK="addIt();" />
            <BR/>
            <img src="images/backItem.gif" ONCLICK="delIt();" />
          </td>
          <td>
            <select name="PickList" id="PickList" size="6"  TABINDEX="7" multiple="multiple">

	            <xsl:for-each select="SELECTED_SOURCE_CODES/SOURCE_CODE">
	                <OPTION VALUE="{VALUE}" ><xsl:value-of select="VALUE"/></OPTION>
	            </xsl:for-each>

            </select>
          </td>
          </tr>
          </table>
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>
<div align="left">
	<UL id="tabs">
	<button id='Subject' onmouseover="tabmouseover(this)" onmouseout="tabmouseout(this)" onclick="showtab(this,'Subject')">
	<div align="center"><span class="label">Subject</span></div>
	</button>
	<button id='Header' onmouseover="tabmouseover(this)" onmouseout="tabmouseout(this)" onclick="showtab(this,'Header')">
	<div align="center"><span class="label">Header</span></div>
	</button>
	<button id='Guarantee' onmouseover="tabmouseover(this)" onmouseout="tabmouseout(this)" onclick="showtab(this,'Guarantee')">
	<div align="center"><span class="label">Guarantee</span></div>
	</button>
	<button id='Marketing' onmouseover="tabmouseover(this)" onmouseout="tabmouseout(this)" onclick="showtab(this,'Marketing')">
	<div align="center"><span class="label">Marketing</span></div>
	</button>
	<button id='Contact' onmouseover="tabmouseover(this)" onmouseout="tabmouseout(this)" onclick="showtab(this,'Contact')">
	<div align="center"><span class="label">ContactUs</span></div>
	</button>
	</UL>
</div>

	<UL id="content">
		<div id="SubjectDIV" align="center">
		<br/><br/><br/>
		<xsl:if test="string-length(ERROR/@message)>0">
		<br/>
		</xsl:if>
		<table border="3" width="98%" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
		<tr>
		<td>
		<table width="98%" align="center" cellpadding="1" cellspacing="1">
			<tr>
				<td>
					<table width="98%" border="0" cellpadding="1" cellspacing="1">
					<tr>
			 			<td width="20%" class="label">Select Existing:
						<xsl:variable name="selectedSubject" select="PROGRAM/selected_subject"/>
    				  	<select name="ExistingSection_Subject" onChange="changeSection({pageData/data[name='mode']/value})" TABINDEX="7">
                 			<option value="-1">Select Section</option>
                  			<xsl:for-each select="Subject/SECTIONS/SECTION">
                  				<xsl:choose>
                  				<xsl:when test="section_name=$selectedSubject">
                  					<option value="{section_name}" selected="true"><xsl:value-of select="section_name"/></option>
                  				</xsl:when>
                  				<xsl:otherwise>
                  					<option value="{section_name}"><xsl:value-of select="section_name"/></option>
                  				</xsl:otherwise>
                  				</xsl:choose>
                  			</xsl:for-each>
              			</select></td>
					</tr>
				 </table>
				</td>
			</tr>
			<tr>
					<td>
							<table width="98%" border="0" cellpadding="1" cellspacing="1">
							<tr>
									<td width="10%" class="label">From Address:</td>
									<!--td width="88%" align="left"><input type="text" name="FromAddr" readonly="true" size="20" value="{SELECTED_SUBJECT/SECTION/SECTION_CONTENT_TEXT/from_address}" TABINDEX="8"/></td-->
									<td width="88%" align="left"><input type="text" name="FromAddr" size="20" value="{SELECTED_SUBJECT/SECTION/SECTION_CONTENT_TEXT/from_address}" TABINDEX="8"/></td>
							</tr>
							</table>
					</td>
			</tr>
			<tr>
				<td>
					<table width="98%" border="0" cellpadding="1" cellspacing="1">
						<tr>
							<td><span class="label">Type:&nbsp;&nbsp;</span>text</td>
							<td align="left"><span class="label">Created Date:&nbsp;&nbsp;</span><xsl:value-of select="SELECTED_SUBJECT/SECTION/created_on"/></td>
							<td align="left"><span class="label">Effective Start:&nbsp;&nbsp;</span><xsl:value-of select="SELECTED_SUBJECT/SECTION/start_date"/></td>
							<td width="25%"><span class="label">Effective End:&nbsp;&nbsp;</span><xsl:value-of select="SELECTED_SUBJECT/SECTION/end_date"/></td>
							<td width="25%"><span class="label">Description:&nbsp;&nbsp;</span><xsl:value-of select="SELECTED_SUBJECT/SECTION/description"/></td>
						</tr>
				 	 </table>
				</td>
			</tr>
			<tr>
				<td>
					  <table width="98%" border="0" cellpadding="1" cellspacing="1">
						<tr>
							<!--td width="5%"><textarea rows="6" cols="80" readonly="true"><xsl:value-of select="SELECTED_SUBJECT/SECTION/SECTION_CONTENT_TEXT/text_version" TABINDEX="9"/></textarea></td-->
							<td width="5%"><textarea readonly="true" rows="6" cols="80" TABINDEX="9"><xsl:value-of select="SELECTED_SUBJECT/SECTION/SECTION_CONTENT_TEXT/text_version" disable-output-escaping="no" /></textarea></td>
						</tr>
					</table>
				  </td>
			</tr>
			</table>
			</td>
			</tr>
		</table>
    <table  width="98%" align="center"  border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td width="54%" align="right">
				<input type="button" name="Save" Value="Save" onclick="submitForm({pageData/data[name='mode']/value})" TABINDEX="10"/>
        <input type="button" name="Preview" Value="Save / Preview" onclick="previewEmail({pageData/data[name='previewmode']/value});" TABINDEX="11"/>
			</td>
			<td width="46%" align="right"><input type="button" name="Exit2" Value="Exit" onclick="exitForm();" TABINDEX="13"/></td>
		</tr>
    </table>
              <!-- Footer Template -->
        <xsl:call-template name="footer"/>
		</div>
    <div id="HeaderDIV" align="center">
		<br/><br/><br/>
		<xsl:if test="string-length(ERROR/@message)>0">
			<br/>
		</xsl:if>
		<table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
		<tr>
		<td>
		<table width="98%" align="center" cellpadding="1" cellspacing="1">
		<tr>
				<td>
					<table width="98%" border="0" cellpadding="1" cellspacing="1">
					<tr>
			 			<td width="20%" class="label">Select Existing:
							<xsl:variable name="selectedHeader" select="PROGRAM/selected_header"/>
    				  		<select name="ExistingSection_Header" onChange="changeSection({pageData/data[name='mode']/value})" TABINDEX="7">
                 				<option value="-1">Select Section</option>
                  				<xsl:for-each select="Header/SECTIONS/SECTION">
                  					<xsl:choose>
                  						<xsl:when test="section_name=$selectedHeader">
                    						<option value="{section_name}" selected="true"><xsl:value-of select="section_name"/></option>
                    					</xsl:when>
                    					<xsl:otherwise>
                    						<option value="{section_name}"><xsl:value-of select="section_name"/></option>
                  						</xsl:otherwise>
                  					</xsl:choose>
                  				</xsl:for-each>
              				</select></td>
					</tr>
				 </table>
				</td>
			</tr>
			<tr>
				<td>
					<table width="98%" border="0" cellpadding="1" cellspacing="1">
						<tr>
							<td><span class="label">Type:&nbsp;&nbsp;</span>text</td>
							<td align="left"><span class="label">Created Date:&nbsp;&nbsp;</span><xsl:value-of select="SELECTED_HEADER/SECTION/created_on"/></td>
							<td align="left"><span class="label">Effective Start:&nbsp;&nbsp;</span><xsl:value-of select="SELECTED_HEADER/SECTION/start_date"/></td>
							<td width="25%"><span class="label">Effective End:&nbsp;&nbsp;</span><xsl:value-of select="SELECTED_HEADER/SECTION/end_date"/></td>
							<td width="25%"><span class="label">Description:&nbsp;&nbsp;</span><xsl:value-of select="SELECTED_HEADER/SECTION/description"/></td>
						</tr>
				 	 </table>
				</td>
			</tr>
			<tr>
				<td>
					  <table width="98%" border="0" cellpadding="1" cellspacing="1">
						<tr>
							<!--td width="5%"><textarea rows="5" cols="80" readonly="true"><xsl:value-of select="SELECTED_HEADER/SECTION/SECTION_CONTENT_TEXT/text_version" TABINDEX="9"/></textarea></td-->
							<td width="5%"><textarea  readonly="true" rows="5" cols="80" TABINDEX="9"><xsl:value-of select="SELECTED_HEADER/SECTION/SECTION_CONTENT_TEXT/text_version" disable-output-escaping="no" /></textarea></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table width="98%" border="0" cellpadding="1" cellspacing="1">
						<tr>
							<td><span class="label">Type:&nbsp;&nbsp;</span>html</td>
							<td align="left"><span class="label">Created Date:&nbsp;&nbsp;</span><xsl:value-of select="SELECTED_HEADER/SECTION/created_on"/></td>
							<td align="left"><span class="label">Effective Start:&nbsp;&nbsp;</span><xsl:value-of select="SELECTED_HEADER/SECTION/start_date"/></td>
							<td width="25%"><span class="label">Effective End:&nbsp;&nbsp;</span><xsl:value-of select="SELECTED_HEADER/SECTION/end_date"/></td>
							<td width="25%"><span class="label">Description:&nbsp;&nbsp;</span><xsl:value-of select="SELECTED_HEADER/SECTION/description"/></td>
						</tr>
				 	 </table>
				</td>
			</tr>
			<tr>
				<td>
					  <table width="98%" border="0" cellpadding="1" cellspacing="1">
						<tr>
							<!--td width="5%"><textarea rows="5" cols="80" readonly="true"><xsl:value-of select="SELECTED_HEADER/SECTION/SECTION_CONTENT_HTML/html_version" TABINDEX="9"/></textarea></td-->
							<td width="5%"><textarea  readonly="true" rows="5" cols="80" TABINDEX="9"><xsl:value-of select="SELECTED_HEADER/SECTION/SECTION_CONTENT_HTML/html_version" disable-output-escaping="no" /></textarea></td>
						</tr>
					</table>
				</td>
			</tr>
			</table>
		</td>
		</tr>
		</table>
    <table  width="98%" align="center"  border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td width="54%" align="right">
				<input type="button" name="Save" Value="Save" onclick="submitForm({pageData/data[name='mode']/value})" TABINDEX="10"/>
				<input type="button" name="Preview" Value="Save / Preview" onclick="previewEmail({pageData/data[name='previewmode']/value});" TABINDEX="11"/>
			</td>
			<td width="46%" align="right"><input type="button" name="Exit3" Value="Exit" onclick="exitForm();" TABINDEX="13"/></td>
		</tr>
    </table>
              <!-- Footer Template -->
        <xsl:call-template name="footer"/>
		</div>
    <div id="GuaranteeDIV" align="center">
		<br/><br/><br/>
		<xsl:if test="string-length(ERROR/@message)>0">
			<br/>
		</xsl:if>
		<table border="3" width="98%" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
		<tr>
		<td>
		<table width="98%" align="center" cellpadding="1" cellspacing="1">
			<tr>
				<td>
					<table width="98%" border="0" cellpadding="1" cellspacing="1">
          				<tr>
						 	<td width="20%" class="label">Select Existing:
						 	<xsl:variable name="selectedGuarantee" select="PROGRAM/selected_guarantee"/>
    				  		<select name="ExistingSection_Guarantee" onChange="changeSection({pageData/data[name='mode']/value})" TABINDEX="7">
                 				<option value="-1">Select Section</option>
      							<xsl:for-each select="Guarantee/SECTIONS/SECTION">
      								<xsl:choose>
      								<xsl:when test="section_name=$selectedGuarantee">
        								<option value="{section_name}" selected="true"><xsl:value-of select="section_name"/></option>
                  					</xsl:when>
                  					<xsl:otherwise>
                  						<option value="{section_name}"><xsl:value-of select="section_name"/></option>
                  					</xsl:otherwise>
                  					</xsl:choose>
                  				</xsl:for-each>
              				</select></td>
						</tr>
				 	</table>
				</td>
			</tr>
			<tr>
				<td>
					<table width="98%" border="0" cellpadding="1" cellspacing="1">
						<tr>
							<td><span class="label">Type:&nbsp;&nbsp;</span>text</td>
							<td align="left"><span class="label">Created Date:&nbsp;&nbsp;</span><xsl:value-of select="SELECTED_GUARANTEE/SECTION/created_on"/></td>
							<td align="left"><span class="label">Effective Start:&nbsp;&nbsp;</span><xsl:value-of select="SELECTED_GUARANTEE/SECTION/start_date"/></td>
							<td width="25%"><span class="label">Effective End:&nbsp;&nbsp;</span><xsl:value-of select="SELECTED_GUARANTEE/SECTION/end_date"/></td>
							<td width="25%"><span class="label">Description:&nbsp;&nbsp;</span><xsl:value-of select="SELECTED_GUARANTEE/SECTION/description"/></td>
						</tr>
				 	 </table>
				</td>
			</tr>
			<tr>
				<td>
					  <table width="98%" border="0" cellpadding="1" cellspacing="1">
						<tr>
							<!--td width="5%"><textarea rows="5" cols="80" readonly="true"><xsl:value-of select="SELECTED_GUARANTEE/SECTION/SECTION_CONTENT_HTML/html_version" TABINDEX="9"/></textarea></td-->
							<td width="5%"><textarea  readonly="true" rows="5" cols="80" TABINDEX="9"><xsl:value-of select="SELECTED_GUARANTEE/SECTION/SECTION_CONTENT_TEXT/text_version" disable-output-escaping="no" /></textarea></td>
						</tr>
					</table>
				  </td>
			</tr>
			<tr>
				<td>
					<table width="98%" border="0" cellpadding="1" cellspacing="1">
						<tr>
							<td><span class="label">Type:&nbsp;&nbsp;</span>html</td>
							<td align="left"><span class="label">Created Date:&nbsp;&nbsp;</span><xsl:value-of select="SELECTED_GUARANTEE/SECTION/created_on"/></td>
							<td align="left"><span class="label">Effective Start:&nbsp;&nbsp;</span><xsl:value-of select="SELECTED_GUARANTEE/SECTION/start_date"/></td>
							<td width="25%"><span class="label">Effective End:&nbsp;&nbsp;</span><xsl:value-of select="SELECTED_GUARANTEE/SECTION/end_date"/></td>
							<td width="25%"><span class="label">Description:&nbsp;&nbsp;</span><xsl:value-of select="SELECTED_GUARANTEE/SECTION/description"/></td>
						</tr>
				 	 </table>
				</td>
			</tr>
			<tr>
				<td>
					  <table width="98%" border="0" cellpadding="1" cellspacing="1">
						<tr>
							<!--td width="5%"><textarea rows="5" cols="80" readonly="true"><xsl:value-of select="SELECTED_GUARANTEE/SECTION/SECTION_CONTENT_HTML/html_version" TABINDEX="9"/></textarea></td-->
							<td width="5%"><textarea  readonly="true" rows="5" cols="80" TABINDEX="9"><xsl:value-of select="SELECTED_GUARANTEE/SECTION/SECTION_CONTENT_HTML/html_version" disable-output-escaping="no" /></textarea></td>
						</tr>
					</table>
				  </td>
			</tr>			
			</table>
			</td>
			</tr>
		</table>
    <table  width="98%" align="center"  border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td width="54%" align="right">
				<input type="button" name="Save" Value="Save" onclick="submitForm({pageData/data[name='mode']/value})" TABINDEX="10"/>
        <input type="button" name="Preview" Value="Save / Preview" onclick="previewEmail({pageData/data[name='previewmode']/value});" TABINDEX="11"/>
			</td>
			<td width="46%" align="right"><input type="button" name="Exit4" Value="Exit" onclick="exitForm();" TABINDEX="13"/></td>
		</tr>
    </table>
          <!-- Footer Template -->
        <xsl:call-template name="footer"/>
		</div>

		<div id="MarketingDIV" align="center">
		<br/><br/><br/>
		<xsl:if test="string-length(ERROR/@message)>0">
			<br/>
		</xsl:if>
		<table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
		<tr>
		<td>
		<table width="98%" align="center" cellpadding="1" cellspacing="1">
		<tr>
				<td>
					<table width="98%" border="0" cellpadding="1" cellspacing="1">
						<tr>
			 				<td width="20%" class="label">Select Existing:
						 		<xsl:variable name="selectedMarketing" select="PROGRAM/selected_marketing"/>
    				  			<select name="ExistingSection_Marketing" onChange="changeSection({pageData/data[name='mode']/value})" TABINDEX="7">
     								<option value="-1">Select Section</option>
                  					<xsl:for-each select="Marketing/SECTIONS/SECTION">
                  						<xsl:choose>
                  							<xsl:when test="section_name=$selectedMarketing">
                  								<option value="{section_name}" selected="true"><xsl:value-of select="section_name"/></option>
                  							</xsl:when>
                  							<xsl:otherwise>
                  								<option value="{section_name}"><xsl:value-of select="section_name"/></option>
                  							</xsl:otherwise>
                  						</xsl:choose>

                  					</xsl:for-each>
             	 				</select></td>
						</tr>
				 	</table>
				</td>
			</tr>
			<tr>
				<td>
					<table width="98%" border="0" cellpadding="1" cellspacing="1">
						<tr>
							<td><span class="label">Type:&nbsp;&nbsp;</span>text</td>
							<td align="left"><span class="label">Created Date:&nbsp;&nbsp;</span><xsl:value-of select="SELECTED_MARKETING/SECTION/created_on"/></td>
							<td align="left"><span class="label">Effective Start:&nbsp;&nbsp;</span><xsl:value-of select="SELECTED_MARKETING/SECTION/start_date"/></td>
							<td width="25%"><span class="label">Effective End:&nbsp;&nbsp;</span><xsl:value-of select="SELECTED_MARKETING/SECTION/end_date"/></td>
							<td width="25%"><span class="label">Description:&nbsp;&nbsp;</span><xsl:value-of select="SELECTED_MARKETING/SECTION/description"/></td>
						</tr>
				 	 </table>
				</td>
			</tr>
			<tr>
				<td>
					  <table width="98%" border="0" cellpadding="1" cellspacing="1">
						<tr>
							<!--td width="5%"><textarea rows="5" cols="80" readonly="true"><xsl:value-of select="SELECTED_MARKETING/SECTION/SECTION_CONTENT_TEXT/text_version" TABINDEX="9"/></textarea></td-->
							<td width="5%"><textarea  readonly="true" rows="5" cols="80" TABINDEX="9"><xsl:value-of select="SELECTED_MARKETING/SECTION/SECTION_CONTENT_TEXT/text_version" disable-output-escaping="no" /></textarea></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table width="98%" border="0" cellpadding="1" cellspacing="1">
						<tr>
							<td><span class="label">Type:&nbsp;&nbsp;</span>html</td>
							<td align="left"><span class="label">Created Date:&nbsp;&nbsp;</span><xsl:value-of select="SELECTED_MARKETING/SECTION/created_on"/></td>
							<td align="left"><span class="label">Effective Start:&nbsp;&nbsp;</span><xsl:value-of select="SELECTED_MARKETING/SECTION/start_date"/></td>
							<td width="25%"><span class="label">Effective End:&nbsp;&nbsp;</span><xsl:value-of select="SELECTED_MARKETING/SECTION/end_date"/></td>
							<td width="25%"><span class="label">Description:&nbsp;&nbsp;</span><xsl:value-of select="SELECTED_MARKETING/SECTION/description"/></td>
						</tr>
				 	 </table>
				</td>
			</tr>
			<tr>
				<td>
					  <table width="98%" border="0" cellpadding="1" cellspacing="1">
						<tr>
							<!--td width="5%"><textarea rows="5" cols="80" readonly="true"><xsl:value-of select="SELECTED_MARKETING/SECTION/SECTION_CONTENT_HTML/html_version" TABINDEX="9"/></textarea></td-->
							<td width="5%"><textarea  readonly="true" rows="5" cols="80" TABINDEX="9"><xsl:value-of select="SELECTED_MARKETING/SECTION/SECTION_CONTENT_HTML/html_version" disable-output-escaping="no" /></textarea></td>
						</tr>
					</table>
				</td>
			</tr>
			</table>
		</td>
		</tr>
		</table>
    <table  width="98%" align="center"  border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td width="54%" align="right">
          <input type="button" name="Save" Value="Save" onclick="submitForm({pageData/data[name='mode']/value})" TABINDEX="10"/>
          <input type="button" name="Preview" Value="Save / Preview" onclick="previewEmail({pageData/data[name='previewmode']/value});" TABINDEX="11"/>
			</td>
			<td width="46%" align="right"><input type="button" name="Exit3" Value="Exit" onclick="exitForm();" TABINDEX="13"/></td>
		</tr>
    </table>
              <!-- Footer Template -->
        <xsl:call-template name="footer"/>
		</div>

		<div id="ContactDIV" align="center">
		<br/><br/><br/>
		<xsl:if test="string-length(ERROR/@message)>0">
			<br/>
		</xsl:if>
		<table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
		<tr>
		<td>
		<table width="98%" align="center" cellpadding="1" cellspacing="1">
		<tr>
				<td>
					<table width="98%" border="0" cellpadding="1" cellspacing="1">
						<tr>
				 			<td width="20%" class="label">Select Existing:
				 				<xsl:variable name="selectedContact" select="PROGRAM/selected_contact"/>
	    				  		<select name="ExistingSection_Contact" onChange="changeSection({pageData/data[name='mode']/value})" TABINDEX="7">
	                 				<option value="-1">Select Section</option>
	                  				<xsl:for-each select="Contact/SECTIONS/SECTION">
	                  					<xsl:choose>
	                  						<xsl:when test="section_name=$selectedContact">
	                  							<option value="{section_name}" selected="true"><xsl:value-of select="section_name"/></option>
	                  						</xsl:when>
	                  						<xsl:otherwise>
	                  							<option value="{section_name}"><xsl:value-of select="section_name"/></option>
	                  						</xsl:otherwise>
	                  					</xsl:choose>

	                  				</xsl:for-each>
	              				</select>
	        				</td>
						</tr>
				 	</table>
				</td>
			</tr>
			<tr>
				<td>
					<table width="98%" border="0" cellpadding="1" cellspacing="1">
						<tr>
							<td><span class="label">Type:&nbsp;&nbsp;</span>text</td>
							<td align="left"><span class="label">Created Date:&nbsp;&nbsp;</span><xsl:value-of select="SELECTED_CONTACT/SECTION/created_on"/></td>
							<td align="left"><span class="label">Effective Start:&nbsp;&nbsp;</span><xsl:value-of select="SELECTED_CONTACT/SECTION/start_date"/></td>
							<td width="25%"><span class="label">Effective End:&nbsp;&nbsp;</span><xsl:value-of select="SELECTED_CONTACT/SECTION/end_date"/></td>
							<td width="25%"><span class="label">Description:&nbsp;&nbsp;</span><xsl:value-of select="SELECTED_CONTACT/SECTION/description"/></td>
						</tr>
				 	 </table>
				</td>
			</tr>
			<tr>
				<td>
					  <table width="98%" border="0" cellpadding="1" cellspacing="1">
						<tr>
							<!--td width="5%"><textarea rows="5" cols="80" readonly="true"><xsl:value-of select="SELECTED_CONTACT/SECTION/SECTION_CONTENT_TEXT/text_version" TABINDEX="9"/></textarea></td-->
							<td width="5%"><textarea  readonly="true" rows="5" cols="80" TABINDEX="9"><xsl:value-of select="SELECTED_CONTACT/SECTION/SECTION_CONTENT_TEXT/text_version" disable-output-escaping="no" /></textarea></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table width="98%" border="0" cellpadding="1" cellspacing="1">
						<tr>
							<td><span class="label">Type:&nbsp;&nbsp;</span>html</td>
							<td align="left"><span class="label">Created Date:&nbsp;&nbsp;</span><xsl:value-of select="SELECTED_CONTACT/SECTION/created_on"/></td>
							<td align="left"><span class="label">Effective Start:&nbsp;&nbsp;</span><xsl:value-of select="SELECTED_CONTACT/SECTION/start_date"/></td>
							<td width="25%"><span class="label">Effective End:&nbsp;&nbsp;</span><xsl:value-of select="SELECTED_CONTACT/SECTION/end_date"/></td>
							<td width="25%"><span class="label">Description:&nbsp;&nbsp;</span><xsl:value-of select="SELECTED_CONTACT/SECTION/description"/></td>
						</tr>
				 	 </table>
				</td>
			</tr>
			<tr>
				<td>
					  <table width="98%" border="0" cellpadding="1" cellspacing="1">
						<tr>
							<!--td width="5%"><textarea rows="5" cols="80" readonly="true"><xsl:value-of select="SELECTED_CONTACT/SECTION/SECTION_CONTENT_HTML/html_version" TABINDEX="9"/></textarea></td-->
							<td width="5%"><textarea  readonly="true" rows="5" cols="80" TABINDEX="9"><xsl:value-of select="SELECTED_CONTACT/SECTION/SECTION_CONTENT_HTML/html_version" disable-output-escaping="no" /></textarea></td>
						</tr>
					</table>
				</td>
			</tr>
			</table>
		</td>
		</tr>
		</table>
    <table  width="98%" align="center"  border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td width="54%" align="right">
				<input type="button" name="Save" Value="Save" onclick="submitForm({pageData/data[name='mode']/value})" TABINDEX="10"/>
				<input type="button" name="Preview" Value="Save / Preview" onclick="previewEmail({pageData/data[name='previewmode']/value});" TABINDEX="11"/>
			</td>
			<td width="46%" align="right"><input type="button" name="Exit3" Value="Exit" onclick="exitForm();" TABINDEX="13"/></td>
		</tr>
    </table>
          <!-- Footer Template -->
        <xsl:call-template name="footer"/>
		</div>
  </UL>
  </div>
</form>

  <!-- Processing message div -->
  <div id="waitDiv" style="display:none">
    <table id="waitTable" width="98%" border="3" height="100%" align="center" cellpadding="30" cellspacing="1" bordercolor="#006699">
      <tr>
        <td width="100%">
          <table width="100%" cellspacing="0" cellpadding="0">
            <tr>
              <td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
              <td id="waitTD"  width="50%" class="waitMessage"></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>


<script>initialize('Subject');</script>
        <script type="text/javascript">
  Calendar.setup(
    {
      inputField  : "effectiveStartDate",  // ID of the input field
      ifFormat    : "mm/dd/y",  // the date format
      button      : "startDate"  // ID of the button
    }
  );
</script>
<script type="text/javascript">
  Calendar.setup(
    {
      inputField  : "effectiveEndDate",  // ID of the input field
      ifFormat    : "mm/dd/y",  // the date format
      button      : "endDate"  // ID of the button
    }
  );
</script>
<script><xsl:value-of select="pageData/data[name='selectedTab']/value"/></script>
</body>
</html>


  </xsl:template>
</xsl:stylesheet>