<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:import href="footer.xsl"/>
  <xsl:output method="html" indent="yes"/>
  <xsl:template match="/root">

    <html>
      <head>
        <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
        <script type="text/javascript" language="javascript" src="js/util.js"/>
        <title><xsl:value-of select="pageData/data[name='pageTitle']/value"/></title>

       <script type="text/javascript" language="JavaScript">
	  var securitytoken_js = '<xsl:value-of select="pageData/data[name='securitytoken']/value"/>';
	  var securitycontext_js = '<xsl:value-of select="pageData/data[name='context']/value"/>';
          <![CDATA[

              nbrOfSelected = 0;
			  function init () {
				setNavigationHandlers();
		  	  }
              function exitForm(action)
              {
                 form.action = "EmailAdminProgramServlet?" + action;
                 form.submit();
              }

              function exit()
              {
              	var form = document.forms[0];
              	form.action = "EmailAdminMenuServlet?menuAction=exit";
              	form.submit();
              }


	      function clickUpdate(programId){
	        var action = "viewupdate";
	      	form.programId.value = programId;
                form.action = "EmailAdminProgramServlet?" + action;
                form.submit();	      	
	      }


              function submitForm(action)
              {
                 var form = document.forms[0];

                  var count = 0;
                  var checkboxes = document.getElementsByTagName("input");
                  for (var i = 0; i < checkboxes.length; i++){
                    if (checkboxes[i].type == "checkbox" && checkboxes[i].checked)
                    {
                          count = count + 1;
                    }
                  }

                 if(count == 0)
                 {
                     alert('Please select a program');
                     return false;
                 }

                 if(action == "copy")
                 {
                     var programSelected = 0;
                     var checkboxes = document.getElementsByTagName("input");
                      for (var i = 0; i < checkboxes.length; i++){
                        if (checkboxes[i].type == "checkbox" && checkboxes[i].checked)
                        {
                              form.programId.value = checkboxes[i].name;
                              programSelected = programSelected + 1;
                        }
                      }

                      if(programSelected > 1)
                      {
                          alert('Please select only one program to Copy');
                          return false;
                      }
                 }

                 if(action == "viewupdate")
                 {
                     var programSelected = 0;
                     var checkboxes = document.getElementsByTagName("input");
                      for (var i = 0; i < checkboxes.length; i++){
                        if (checkboxes[i].type == "checkbox" && checkboxes[i].checked)
                        {
                              form.programId.value = checkboxes[i].name;
                              programSelected = programSelected + 1;
                        }
                      }

                      if(programSelected > 1)
                      {
                          alert('Please select only one program to Update');
                          return false;
                      }
                 }

                 if(action == "delete")
                 {  
                    var programSelected = "";
                    nbrOfSelected = 0;
                    var checkboxes = document.getElementsByTagName("input");
                    for (var i = 0; i < checkboxes.length; i++){
                       if (checkboxes[i].type == "checkbox" && checkboxes[i].checked)
                       {
                            nbrOfSelected = nbrOfSelected + 1;
                            programSelected = programSelected + checkboxes[i].name + "~";
                       }
                    }

                    form.selectedPrograms.value = programSelected;
                    var response = confirm('Press OK to Confirm deleting ' + nbrOfSelected  + ' programs');
                    if (response) 
                    {
                        //proceed
                    }
                    else
                    {
                        return false;
                    }
                }     
  
                 form.action = "EmailAdminProgramServlet?" + action;
                 form.submit();
              }
/*
              function selectProgram(programId,selectedProgram)
              {
                  var form = document.forms[0];

                 // if(selectedProgram.checked == true)
                  ///{
                  //    form.programId.value = programId;
                //  }
              }
*/
              function previewEmail()
              {
                 var form = document.forms[0];        
                 var myObject = new Object();

                var count = 0;
                  var checkboxes = document.getElementsByTagName("input");
                  for (var i = 0; i < checkboxes.length; i++){
                    if (checkboxes[i].type == "checkbox" && checkboxes[i].checked)
                    {
                          count = count + 1;
                    }
                  }

                 if(count == 0)
                 {
                     alert('Please select a program');
                     return false;
                 }
                 
                    var programSelected = 0;
                     var checkboxes = document.getElementsByTagName("input");
                      for (var i = 0; i < checkboxes.length; i++){
                        if (checkboxes[i].type == "checkbox" && checkboxes[i].checked)
                        {
                              form.programId.value = checkboxes[i].name;
                              programSelected = programSelected + 1;
                        }
                      }

                      if(programSelected > 1)
                      {
                          alert('Please select only one program to Preview');
                          return false;
                      }


                 myObject.programId = form.programId.value;

                 var sFeatures="dialogHeight: " + 400 + "px;dialogWidth: " + 800 + "px;";
                 var url_source="EmailAdminProgramServlet?" + "actionMethod=" + "preview" +
                 "&programId=" + form.programId.value + "&context=" + securitycontext_js +
                 "&securitytoken=" + securitytoken_js;
 
                 var test = window.showModalDialog(url_source, myObject, sFeatures);
                 //var test = window.open(url_source, 'Detail', 'width=500,height=300,scrollbars=yes,addressbar=yes');
              }     

              ]]>
        </script>        
       
      </head>
      <body onload="javascript:init();">
        <center>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="2">
    <tr>
        <td width="20%" align="left"><img border="0" height="60" src="images/wwwftdcom.gif" width="135"/></td>
        <td width="60%" align="center" colspan="1" class="header"><xsl:value-of select="pageData/data[name='headerName']/value"/></td>
        <td width="20%" align="left">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="3"><hr/></td>
    </tr>
</table>
<form name="form" method="post" action="">
<input type="hidden" name="securitytoken" value="{pageData/data[name='securitytoken']/value}"/>
<input type="hidden" name="context" value="{pageData/data[name='context']/value}"/>
<input type="hidden" name="programId" value=""/>
<input type="hidden" name="option" value="{pageData/data[name='option']/value}"/>
<input type="hidden" name="searchString" value="{pageData/data[name='searchString']/value}"/>
<input type="hidden" name="selectedPrograms" value=""/>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="2"><font color="red"><xsl:value-of select="pageData/data[name='cannotDeletePrograms']/value"/></font></td>
	</tr>
    <tr>
        <td width="20%"><a href="#" onclick="exitForm('exit');">Email Maintenance</a></td>
   	  <td width="60%" align="center">
					<input type="button" name="Copy" Value="Copy" onclick="submitForm('copy');" TABINDEX="2"/>
          <input type="button" name="Delete" Value="Delete" onclick="submitForm('delete');" TABINDEX="4"/>
          <input type="button" name="Preview" Value="Preview" onclick="previewEmail();" TABINDEX="5"/>
			</td>
		<td width="20%">
			<div align="right">
			  <input type="button" name="Exit" Value="Exit" onclick="exit();" TABINDEX="6"/>
		      </div></td>
    </tr>
</table>

<table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
	<tr>
		<td>
			<table width="98%" border="0" cellpadding="0" cellspacing="0">  
					<tr>
						<td width="5%" class="tbltextheader2 style1">Select</td>
						<td width="11%" class="tbltextheader2 style1">Program</td>
						<td width="16%" class="tbltextheader2 style1">Created Date</td>
						<td width="33%" class="tbltextheader2 style1">Description</td>
						<td width="12%" class="tbltextheader2 style1">Last Modified By</td>
						<td width="15%" class="tbltextheader2 style1">Last Modified Date</td>
					</tr>
  				 	<xsl:for-each select="SECTIONS/SECTION">
					<tr>
						<td align="center"><input type="checkbox" name="{program_id}" TABINDEX="1"/></td>
						<!--td align="center"><input type="checkbox" name="{program_id}" onclick="selectProgram('{program_id}',this);" TABINDEX="1"/></td-->
           				<td class="style1"><a href="#" onclick='clickUpdate("{program_id}");'><xsl:value-of select="program_name"/></a></td>
						<td align="center"><span class="style1"><xsl:value-of select="created_on"/></span></td>
						<td><span class="style1"><xsl:value-of select="description"/></span></td>
						<td align="center"><span class="style1"><xsl:value-of select="updated_by"/></span></td>
						<td align="center"><span class="style1"><xsl:value-of select="updated_on"/></span></td>
					</tr>
				 	</xsl:for-each>
					
		    </table>
		</td>
	</tr>
</table>

<table width="98%" border="0" cellpadding="1" cellspacing="1">
    <tr>
    	<td width="20%"></td>
   	  <td width="60%" align="center">
					<input type="button" name="Copy" Value="Copy" onclick="submitForm('copy');" TABINDEX="2"/>

          <input type="button" name="Delete" Value="Delete" onclick="submitForm('delete');" TABINDEX="4"/>
          <input type="button" name="Preview" Value="Preview" onclick="previewEmail();" TABINDEX="5"/>
			</td>
 			<td width="20%" align="right">
					<input type="button" name="Exit2" Value="Exit" onclick="exit();" TABINDEX="7"/>
			</td>
	</tr>
  <!--tr>
      <td align="left" class="errorMessage"><xsl:value-of select="pageData/data[name='ErrorMessage']/value"/></td>
  </tr-->
</table>
        <!-- Footer Template -->
        <xsl:call-template name="footer"/>
</form>
</center>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>