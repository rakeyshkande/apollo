<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:import href="footer.xsl"/>
  <xsl:output method="html" indent="yes"/>
  <xsl:param name="securitytoken"/>
  <xsl:param name="context"/>
  <xsl:template match="/root">

    <html>
      <head>
        <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
        <title>Related Sections for Program Name</title>
       
       <script type="text/javascript" language="JavaScript">
          <![CDATA[
              
              function exitForm()
              {
                  window.close();
              }

              ]]>
        </script>        
       
      </head>
      <body>
        <center>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="2">
    <tr>
        <td width="20%" align="left"><img border="0" height="60" src="images/wwwftdcom.gif" width="135"/></td>
        <td width="60%" align="center" colspan="1" class="header">Related Sections for&nbsp;<xsl:value-of select="pageData/data[name='programName']/value"/> </td>
        <td width="20%" align="left">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="3"><hr/></td>
    </tr>
</table>
<form name="form" method="post" action="">
<input type="hidden" name="securitytoken" value="{$securitytoken}"/>
<input type="hidden" name="context" value="{$context}"/>
<table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
	<tr>
		<td>
			<table width="98%" border="0" cellpadding="0" cellspacing="0">  
					<tr>
						<td width="10%" class="tbltextheader2 style1">Section</td>
						<td width="12%" class="tbltextheader2 style1">Name</td>
						<td width="16%" class="tbltextheader2 style1">Description</td>
						<td width="8%" class="tbltextheader2 style1">Effective Start</td>
						<td width="8%" class="tbltextheader2 style1">Effective End</td>						
					</tr>
         <xsl:for-each select="SECTIONS/SECTION">
					<tr>
            <td class="style1"><xsl:value-of select="section_type"/></td>
						<td class="style1"><xsl:value-of select="section_name"/></td>
            <td class="style1"><xsl:value-of select="description"/></td>
            <td class="style1"><xsl:value-of select="start_date"/></td>
            <td class="style1"><xsl:value-of select="end_date"/></td>
          </tr>
				 </xsl:for-each>
					
		    </table>
		</td>
	</tr>
</table>

<table width="98%" border="0" cellpadding="1" cellspacing="1">
    <tr>
 			<td width="43%" align="center">
					<input type="button" name="Exit2" Value="Close" onclick="exitForm();"/>
			</td>
	</tr>
</table>
        <!-- Footer Template -->
        <xsl:call-template name="footer"/>
</form>
</center>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
