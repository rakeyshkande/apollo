<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:import href="footer.xsl"/>
  <xsl:output method="html" indent="yes" encoding="UTF-8"/>
  <xsl:template match="/root">

    <html>
      <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" /> 
        <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
        <link rel="stylesheet" type="text/css" href="css/calendar.css"/>

        <script type="text/javascript" src="js/util.js"/>
        <script type="text/javascript" src="js/calendar.js"></script>

        <title><xsl:value-of select="pageData/data[name='pageTitle']/value"/></title>
        <style>
        td
        {
        }
        .visibleObj
        {
        display:inline;
        }
        .hiddenObj
        {
        display:none;
        }
        </style>
       <script type="text/javascript" language="JavaScript">
          <![CDATA[
              var oldtext = "";
              var removeControlChars = "";
              var tt = null;
                            
              function init () {
                setNavigationHandlers();
                loadview(document.forms[0].selectedType.value);
              }
              
              function setDefault(checkbox)
              {
                  var form = document.forms[0];
                  if (checkbox.checked) {
                      form.selDefault.value = "Y";
                  } else {
                      form.selDefault.value = "N";
                  }
              }
        
              function ShowRow(rowID)
              {
                var e = document.all(rowID);
                e.className = "visibleObj";
              }

              function HideRow(rowID)
              {
                var e = document.all(rowID);
                e.className = "hiddenObj";
              }

              function AddressRow(selectedOption)
              {
                if(selectedOption == "Subject")
                {
                    ShowRow('AddressRow');
                }
                else
                {
                    HideRow('AddressRow');
                }
              }

              function loadview(selected)
              {
                  if(selected == "Text" || selected == "text")
                  {
                    HideRow('TextRow');
                    HideRow('HTMLRow');
                    ShowRow('TextRow');
                  }

                  if(selected == "HTML" || selected == "html")
                  {
                    HideRow('TextRow');
                    HideRow('HTMLRow');
                    ShowRow('HTMLRow');
                  }

                  if(selected == "Both")
                  {
                    HideRow('TextRow');
                    HideRow('HTMLRow');
                    ShowRow('TextRow');
                    ShowRow('HTMLRow');
                  }
              }
              
              function showTextType(selectTextType)
              {
                var form = document.forms[0];
                var selected = selectTextType[selectTextType.selectedIndex].value;

                form.selectedType.value = selected;

                HideRow('TextRow');
                HideRow('HTMLRow');

                if(selected == "Both")
                {
                  ShowRow('TextRow');
                  ShowRow('HTMLRow');
                }
                else
                {
                  ShowRow(selected + 'Row');
                }
               }

              function updateSection(sectionType)
              {
                  var form = document.forms[0];
                  form.textContent.value = form.textContentDisabled.value;
                  form.htmlContent.value = form.htmlContentDisabled.value;
                  
                  if(form.sectionName.value == "")
                  {
                      alert('Section name is required');
                      return false;
                  }

                  if(form.sectionName.value == "")
                  {
                      alert('Section name is required');
                      return false;
                  }


/*
                  if(form.sectionDescription.value == "")
                  {
                      alert('Section description is required');
                      return false;
                  }
*/

                  if(form.effectiveStartDate.value == "")
                  {
                      alert('Effective start date is required');
                      return false;
                  }

		    if(form.effectiveEndDate.value == "")
		    {
			alert('Effective End Date required');
			return false;
		    }                  

		var thetext = document.getElementById("textContent").value;
		if(thetext == '')
		{
			alert('The text area cannot be empty');
			return false;
		}


                  if(sectionType == "Subject")
                  {
                      var addr = document.getElementById("FromAddrSel");
                      var selected = addr[addr.selectedIndex].value;
                      if(selected == -1)
                      {
                          alert('From Address is required when saving a Subject Section');
                          return false;
                      }
                  }
                  form.sectionType.value = sectionType;
                  var endDtString = document.forms[0].effectiveEndDate.value;
                  var defaultSectionCB = document.forms[0].selDefaultObj;

                  // set default section value
                  setDefault(defaultSectionCB);

                  if (endDtString.length > 0 && !endDateGreaterThanStartDate()) {
                  		alert("Effective End Date cannot be smaller than Effective Start Date.");
                  		return false;
             	  }


				  	  submitForm("update");


              }   

              function exit()
              {
              	var form = document.forms[0];
              	form.action = "EmailAdminMenuServlet?menuAction=exit";
              	form.submit();
              }

              function goSearch()
              {
              	var form = document.forms[0];
              	form.action = "EmailAdminSelectionServlet?search";
              	form.submit();
              }
              
              function changeSelection() {
           		  // set default section value
           		  var defaultSectionCB = document.forms[0].selDefaultObj;
                  setDefault(defaultSectionCB);
                  submitForm("updateSelectExistingSection");
              }
              
              function submitForm(action)
              {
                  var form = document.forms[0];
                  form.action = "EmailAdminSectionServlet?" + action;
                  //var selected = form.display[form.display.selectedIndex].value;
                  //form.selectedType.value = selected;
                  form.submit();
              }
/*
              function init(){
                addImageCursorListener(images);
              }
*/
             function openEdit(editType)
              {
                 //var newText = "";
                 tt = document.getElementById(editType);
                 //var myObject = new Object();
                 //myObject.oldtext = tt.value; 
                 //var sFeatures="dialogHeight: " + 500 + "px;dialogWidth: " + 800 + "px;";
                 //newText = window.showModalDialog("xsl/editText.html", myObject, sFeatures);             
                 //tt.value = newText;
                 oldtext = tt.value;

                 var sectionType = document.getElementById("SectionType").value;                
                 removeControlChars = "N";
                 if(sectionType == "Subject"){
                 	removeControlChars = "Y";
                 }

                 window.open("editText.html","","toolbar=no,resizable=yes,scrollbars=yes,width=750,height=510");
		
              }
                
              function openTextEdit(editType)
              {
                 tt = document.getElementById(editType);
                 oldtext = tt.value;                 

                 var sectionType = document.getElementById("SectionType").value;                
                 removeControlChars = "N";
                 if(sectionType == "Subject"){
                 	removeControlChars = "Y";
                 }
                 	
                 window.open("editTextAscii.html","","toolbar=no,resizable=yes,scrollbars=yes,width=750,height=510");
              }
              
			  function getDateFromString(dtStr) {

					var dtCh= "/";

					var pos1=dtStr.indexOf(dtCh);
					var pos2=dtStr.indexOf(dtCh,pos1+1);
					var strMonth=dtStr.substring(0,pos1);
					var strDay=dtStr.substring(pos1+1,pos2);
					var strYear=dtStr.substring(pos2+1);

					return new Date(strYear, strMonth, strDay);
			  }

			  function aGreaterThanB(a, b) {
					return a.getTime() >= b.getTime();
			  }

			  function endDateGreaterThanStartDate() {
					var startDtString = document.forms[0].effectiveStartDate.value;
					var endDtString = document.forms[0].effectiveEndDate.value;
					var startDt = getDateFromString(startDtString);
					var endDt = getDateFromString(endDtString);

					return aGreaterThanB(endDt, startDt);
			  }
              ]]>
        </script>        
       
      </head>
      <body onload="javascript:init();">
        <center>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="2">
    <tr>
        <td width="20%" align="left"><img border="0" height="60" src="images/wwwftdcom.gif" width="135"/></td>
        <td width="60%" align="center" colspan="1" class="header"><xsl:value-of select="pageData/data[name='headerName']/value"/></td>
        <td width="20%" align="left">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="3"><hr/></td>
    </tr>
</table>
<!--form name="formname" method="post" action="EmailAdminSectionServlet?updateSelectExistingSection"-->
<form name="formname" method="post">
<input type="hidden" name="securitytoken" value="{pageData/data[name='securitytoken']/value}"/>
<input type="hidden" name="context" value="{pageData/data[name='context']/value}"/>
<input type="hidden" name="sectionType" value="{pageData/data[name='sectionType']/value}"/>
<input type="hidden" name="createdOn" value="{SECTION/created_on}"/>
<input type="hidden" name="updatedOn" value="{SECTION/updated_on}"/>
<input type="hidden" name="updatedBy" value="{SECTION/updated_by}"/>
<input type="hidden" name="textContentId" value="{SECTION/SECTION_CONTENT_TEXT/section_id}"/>
<input type="hidden" name="htmlContentId" value="{SECTION/SECTION_CONTENT_HTML/section_id}"/>
<input type="hidden" name="selDefault" value=""/>
<input type="hidden" name="selectedType" value="{pageData/data[name='displayType']/value}"/>
<!-- back link uses this param to go to search page -->
<input type="hidden" name="option" value="{pageData/data[name='sectionType']/value}"/>
<input type="hidden" name="searchString" value="{pageData/data[name='searchString']/value}"/>

<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td><a href="#" onclick="submitForm('exit');" TABINDEX="-1">Email Maintenance</a>
			&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" onclick="javascript:goSearch();" TABINDEX="-1">Back</a>
        </td>
		<td>
			<div align="right">
			  <input type="button" name="Exit" Value="Exit" onclick="exit();" TABINDEX="11"/>
		      </div></td>
    </tr>
</table>
<table align="center" width="98%" border="0" cellpadding="2" cellspacing="2">
	<tr>
		<td colspan="5" class="tblheader">Section Information</td>
	</tr>
	<tr>
		<td width="14%" class="label">Name:</td>
		<td width="24%"><input type="text" name="sectionNameDisabled" size="30" maxlength="30" value="{SECTION/section_name}" disabled="true" TABINDEX="1"/></td>
		<input type="hidden" name="sectionName" value="{SECTION/section_name}"/>
		<td width="13%" class="label">Description:</td>
		<td width="49%">
			<input name="sectionDescription" type="text" size="60" maxlength="100"  value="{SECTION/description}" TABINDEX="2"/>
		</td>
  	<td width="11%" >
        <xsl:choose>
          <xsl:when test="SECTION[section_default='Y']">
              <input type="checkbox" name="selDefaultObj" checked="true" TABINDEX="-1"/>
          </xsl:when>
          <xsl:otherwise>
              <input type="checkbox" name="selDefaultObj" TABINDEX="-1"/>
          </xsl:otherwise>
        </xsl:choose>
    &nbsp;<span class="label">Default</span></td>    
   </tr>
	<tr>
		<td width="14%" class="label">Effective Starting:</td>
		<td width="24%">
			<input name="effectiveStartDate" type="text" maxlength="10" value="{SECTION/start_date}" TABINDEX="4" OnChange="checkDate(this);"/>
			<img id="startDate" src="images/calendar.gif" width="24" height="22" align="ABSMIDDLE"/>
		</td>
		<td width="13%" class="label">Effective Ending:</td>
		<td width="49%">
			<input name="effectiveEndDate" type="text" maxlength="10" value="{SECTION/end_date}" TABINDEX="5" OnChange="checkDate(this);"/>
			<img id="endDate" src="images/calendar.gif" width="24" height="22" align="ABSMIDDLE"/>
		</td>
	</tr>

</table>

<table border="3" width="98%" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
<tr>
	<td>
		<table width="98%" cellpadding="2" cellspacing="2">
    		<!--tr>
 						<td width="20%" class="label">Select Existing:
    				  
				  <select name="ExistingSection" onChange="javascript:changeSelection()" TABINDEX="7">
				 <xsl:variable name="selectedSection" select="pageData/data[name='selectedSection']/value"/>
				 <xsl:choose>
				 	<xsl:when test="$selectedSection='-1'">
                 		<option value="-1" selected="true">Select Section</option>
                 	</xsl:when>
                 	<xsl:otherwise>
                 		<option value="-1">Select Section</option>
                 	</xsl:otherwise>
                 </xsl:choose>
                 <xsl:choose>
                 	<xsl:when test="$selectedSection='-2'">
                 		<option value="-2" selected="true">Create New Section</option>
               		</xsl:when>
               		<xsl:otherwise>
               			<option value="-2">Create New Section</option>
               		</xsl:otherwise>
                 </xsl:choose>
                 <xsl:for-each select="SECTIONS/SECTION">
                 	<xsl:choose>
                 	  <xsl:when test="section_name=$selectedSection">
                        <option value="{section_name}" selected="true"><xsl:value-of select="section_name"/></option>
                      </xsl:when>
                      <xsl:otherwise>
						<option value="{section_name}"><xsl:value-of select="section_name"/></option>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:for-each>

                 
              </select></td>
        </tr-->
        <tr id="AddressRow">
            <td width="20%" class="label">From Address:
               <select name="FromAddr" id="FromAddrSel" TABINDEX="8">
                <option value="-1">Select Address</option>
                <xsl:variable name="sectionContentFromAddress" select="SECTION/SECTION_CONTENT_TEXT/from_address"/>
                <xsl:for-each select="ADDRESSES/ADDRESS">
                	<xsl:variable name="id" select="FROM_ADDDRESS_ID"/>
                    <xsl:choose>
                      <!--xsl:when test="SECTION/SECTION_CONTENT_TEXT[from_address=$id]"-->
                   	  <xsl:when test="$sectionContentFromAddress=$id">
                        <option value="{FROM_ADDRESS}" selected="true"><xsl:value-of select="FROM_ADDRESS"/></option>
                      </xsl:when>
                      <xsl:otherwise>
                        <option value="{FROM_ADDRESS}"><xsl:value-of select="FROM_ADDRESS"/></option>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:for-each>
                </select>
            </td>
        </tr>
        <tr id="TextRow">
          <td>
          <table>
                <tr>
                  <td colspan="4" class="tblheader">Text Version</td>
                </tr>
                <tr>
                  <td width="547" align="left">
                    <table border="1"><tr><td>
                    <textarea id="textarea" name="textContentDisabled" cols="85" rows="6" readonly="true"><xsl:value-of select="SECTION/SECTION_CONTENT_TEXT/text_version" disable-output-escaping="no" /></textarea>
                    <input type="hidden" name="textContent" value=""/>
                    </td></tr></table>
                  </td>
                  <td width="401"><input type="button" name="edittext" value="Edit" onclick="openTextEdit('textarea');" TABINDEX="9"/></td>
                </tr>
          </table>
          </td>
        </tr>
        <tr id="HTMLRow">
          <td>
          <table>
              <tr>
                <td colspan="4" class="tblheader">HTML Version</td>
              </tr>
               <tr>
                  <td width="547" align="left">
                  <table border="1"><tr><td>
                  <textarea id="htmlarea" name="htmlContentDisabled" cols="85" rows="6" readonly="true"><xsl:value-of select="SECTION/SECTION_CONTENT_HTML/html_version"  disable-output-escaping="no" /></textarea>
                  <input type="hidden" name="htmlContent" value=""/>
                  </td></tr></table>
                  </td>
                  <td width="401"><input type="button" name="edithtml" value="Edit" onclick="openEdit('htmlarea');"/></td>
              </tr>
          </table>
          </td>
        </tr>
		</table>
    </td>
    </tr>
</table>
<table width="98%" border="0" cellpadding="2" cellspacing="2">
    <tr>
        <td width="50%" align="right">
            <input type="button" name="update" value="Save" onclick="{pageData/data[name='updateSection']/value}" TABINDEX="10"/>
        </td>
		<td width="46%" align="right"><input type="button" name="Exit2" Value="Exit" onclick="exit();" TABINDEX="12"/></td>
	   </tr>
</table>
        <!-- Footer Template -->
        <xsl:call-template name="footer"/>
</form>
</center>
<script><xsl:value-of select="pageData/data[name='subjectRow']/value"/></script>
<script><xsl:value-of select="pageData/data[name='showBoxes']/value"/></script>
<script type="text/javascript">
  Calendar.setup(
    {
      inputField  : "effectiveStartDate",  // ID of the input field
      ifFormat    : "mm/dd/y",  // the date format
      button      : "startDate"  // ID of the button
    }
  );
</script>
<script type="text/javascript">
  Calendar.setup(
    {
      inputField  : "effectiveEndDate",  // ID of the input field
      ifFormat    : "mm/dd/y",  // the date format
      button      : "endDate"  // ID of the button
    }
  );
</script>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>