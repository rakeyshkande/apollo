<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>
<!--   <?xml version='1.0' encoding='windows-1252'?> -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:import href="footer.xsl"/>
  <xsl:output method="html" indent="no"/>
  <xsl:output indent="yes"/>
  <xsl:variable name="headerName" select="'Email Confirmation'"/>
  
  <xsl:template match="/root">
  
    <html>
      <head>
        <title>FTD - Email Confirmation</title>
        <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
        <script type="text/javascript" language="javascript" src="js/sourceCodeLookup.js"/>
        <script type="text/javascript" language="javascript" src="js/popup.js"/>
        <script type="text/javascript" language="javascript" src="js/util.js"/>
        <script type="text/javascript" language="javascript" src="js/FormChek.js"/>
        <script type="text/javascript" language="JavaScript">
        var securitytoken_js = '<xsl:value-of select="pageData/data[name='securitytoken']/value"/>';
        var securitycontext_js = '<xsl:value-of select="pageData/data[name='context']/value"/>';
          <![CDATA[
				  function init () {
					setNavigationHandlers();
				  }
				  
                  function exit()
                  {

                      var form = document.forms[0];
                      form.action = "EmailAdminMenuServlet?menuAction=exit";
                      form.submit();
                  }

                  function submitForm(action)
                  {
                      var form = document.forms[0];
                      var selected_item = form.sections[form.sections.selectedIndex].value;

                      if(selected_item == "-1")
                      {
                      	if(action == "new") {
                        	alert('Please specify either a Program or Section to create.');
                        } else {
							alert('No Search Criteria selected, please specify.');
                        }
                        return false;
                      }
                      else
                      {
                        if(action == "new" && selected_item == "Source Code")
                        {
                            alert('Source Code not valid for New function');
                            return false;
                        }
                      }
                      form.option.value = selected_item;
    
                      form.action = "EmailAdminSelectionServlet?" + action;
                      form.submit();
                  }
                  
                  function doSourceCodeLookup()
                  {
                      lookup();
                      //SourceCodePopup.setup({displayArea:'sourceCodeDisplay'});
                  }

                  function lookup()
                  {
                     var form = document.forms[0];
                    
                     form.sourceCodeInput.value = form.searchString.value;
                     form.dateFlag.value = "Y";
            
                     var myObject = new Object();
                     var sFeatures="dialogHeight: " + 400 + "px;dialogWidth: " + 800 + "px;";
                     var url_source="SourceCodeServlet?"
                     				+ "sourceCodeInput="
                     				+ form.searchString.value
                     				+ "&dateFlag=" + "Y"
                     				+ "&securitytoken=" + securitytoken_js
                     				+ "&context=" + securitycontext_js;
                     var newSourceCode = window.showModalDialog(url_source, myObject, sFeatures);    

                     form.searchString.value = newSourceCode;
                  }

              ]]>
        </script>
      </head>
      <body onload="javascript:init();">
        <center>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="2">
    <tr>
        <td width="20%" align="left"><img border="0" height="60" src="images/wwwftdcom.gif" width="135"/></td>
        <td width="60%" align="center" colspan="1" class="header">Email Confirmation</td>
        <td width="20%" align="left">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="3"><hr/></td>
    </tr>
</table>
<form name="form" method="post" action="">
<input type="hidden" name="securitytoken" value="{pageData/data[name='securitytoken']/value}"/>
<input type="hidden" name="context" value="{pageData/data[name='context']/value}"/>
<input type="hidden" name="option" value=""/>
<input type="hidden" name="sourceCodeInput" value=""/>
<input type="hidden" name="dateFlag" value="Y"/>

<table width="98%" align="center" cellpadding="1" celspacing="1">
<tr>
		<td align="right">
				<input type="button" name="Exit" Value="Exit" onClick="exit();" TABINDEX="6"></input>
		</td>
</tr>
</table>
<table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
    <tr>
        <td>
            <!-- Search -->
            <table width="100%" border="0" cellpadding="2" cellspacing="2">
								<tr>
                    <td width="11%" class="label">Option:</td>
                    <td width="29%">
                        <select name="sections" TABINDEX="1">
                            <option value="-1">Select One</option>
														<option value="Program">Program</option>								
                            <option value="Subject">Subject</option>
                            <option value="Header">Header</option>
                            <option value="Guarantee">Guarantee</option>
                            <option value="Marketing">Marketing</option>
                            <option value="Contact">Contact</option>
														<option value="Source Code">Source Code</option>
                        </select>
                    </td>
                    <td width="8%" class="label">&nbsp;</td>
                    <td width="52%">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td width="15%" class="label">Name / Source Code:</td>
                    <td width="59%" class="label">
												<input name="searchString" type="text" size="30" value="" TABINDEX="2"/>
                
                
                  <span id="sourceCodeDisplay">
                    
                  &nbsp;-&nbsp;<a id="sourceCodeLink" class="link" href="javascript:doSourceCodeLookup();" TABINDEX="3">Lookup Source Code</a>
                  <input type="hidden" name="source_code" value=""/>
                  <input type="hidden" name="source_code_description" value=""/>
                  </span>
                </td>


                </tr>
            </table>
        </td>
    </tr>
</table>

<table width="98%" border="0" cellpadding="1" cellspacing="1">
    <tr>
        <td width="45%" align="right">
						<input name="New" type="button" id="New" value="New" onClick="submitForm('new');" TABINDEX="4"/>
				</td>
				<td width="10%" align="right">
						<input name="search" type="button"  value="Search" onclick="submitForm('search');" TABINDEX="5"/>				
				</td>
				<td align="right">
						<input type="button" name="Exit2" Value="Exit" onClick="exit();" TABINDEX="7"/>
				</td>
    </tr>
</table>

</form>
</center>
        <!-- Footer Template -->
        <xsl:call-template name="footer"/>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>