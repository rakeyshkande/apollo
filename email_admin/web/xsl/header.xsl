<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template name="header">
<xsl:param name="headerName"/>

    <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td width="20%" align="left"><img border="0" height="60" src="images/wwwftdcom.gif" width="135"/></td>
            <td width="60%" align="center" colspan="1" class="header"><xsl:value-of select="$headerName"/></td>
            <td width="20%" align="left">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3"><hr/></td>
        </tr>
    </table>

</xsl:template>
</xsl:stylesheet>