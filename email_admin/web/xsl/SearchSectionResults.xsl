<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:import href="footer.xsl"/>
  <xsl:output method="html" indent="yes"/>
  <xsl:template match="/root">

    <html>
      <head>
        <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
        <script type="text/javascript" language="javascript" src="js/util.js"/>
        <title><xsl:value-of select="pageData/data[name='pageTitle']/value"/></title>
       
       <script type="text/javascript" language="JavaScript">
          <![CDATA[

              nbrOfSelected = 0;
              
			  function init () {
				setNavigationHandlers();
		  	  }
              function exitForm(action)
              {
                 form.action = "EmailAdminProgramServlet?" + action;
                 form.submit();
              }


	      function clickUpdate(sectionName,selectedType){
	        var action = "viewupdate";
                form.sectionName.value = sectionName;
                
                
                
                //emueller, 5/13/04..This will always be Both now..existing code is left in case it is decided to go back to the original way
                //form.selectedType.value = selectedType;
                if(form.sectionType.value == "Subject"){
                	form.selectedType.value = "Text";
                	}
                else{                
                	form.selectedType.value = "Both";
                	}
                
                
                form.action = "EmailAdminSectionServlet?" + action;                
                form.submit();	      	
	      }

              function exit()
              {
              	var form = document.forms[0];
              	form.action = "EmailAdminMenuServlet?menuAction=exit";
              	form.submit();
              }

              function submitForm(action)
              {
                 var form = document.forms[0];

                 var count = 0;
                  var checkboxes = document.getElementsByTagName("input");
                  for (var i = 0; i < checkboxes.length; i++){
                    if (checkboxes[i].type == "checkbox" && checkboxes[i].checked)
                    {
                          count = count + 1;
                    }
                  }

                 if(count == 0)
                 {
                     alert('Please select a section');
                     return false;
                 }

                 if(action == "copy")
                 {
                     var sectionSelected = 0;
                     var checkboxes = document.getElementsByTagName("input");                    
                     
                     
                      for (var i = 0; i < checkboxes.length; i++){
                        if (checkboxes[i].type == "checkbox" && checkboxes[i].checked)
                        {
                              form.sectionName.value = checkboxes[i].name;
                              sectionSelected = sectionSelected + 1;
                              
				if(form.sectionType.value == "Subject"){
					form.selectedType.value = "Text";
					}
				else{                
					form.selectedType.value = "Both";
				}                              
                              
                        }
                      }

                      if(sectionSelected > 1)
                      {
                          alert('Please select only one section to Copy');
                          return false;
                      }
                 }

                 if(action == "viewupdate")
                 {
                     var sectionSelected = 0;
                     var checkboxes = document.getElementsByTagName("input");
                      for (var i = 0; i < checkboxes.length; i++){
                        if (checkboxes[i].type == "checkbox" && checkboxes[i].checked)
                        {
                              form.sectionName.value = checkboxes[i].name;
                              sectionSelected = sectionSelected + 1;
                              form.selectedType.value = checkboxes[i].value;
                        }
                      }

                      if(sectionSelected > 1)
                      {
                          alert('Please select only one section to Update');
                          return false;
                      }
                 }
                 
                 if(action == "delete")
                 {  
                     var sectionSelected = "";
                     nbrOfSelected = 0;
                     var checkboxes = document.getElementsByTagName("input");
                      for (var i = 0; i < checkboxes.length; i++){
                        if (checkboxes[i].type == "checkbox" && checkboxes[i].checked)
                        {
                              nbrOfSelected = nbrOfSelected + 1;
                              sectionSelected = sectionSelected + checkboxes[i].id + "~";
                        }
                      }

                    form.selectedSections.value = sectionSelected;
                    var sectionType = form.sectionType.value;
                    var response = confirm('Press OK to Confirm deleting ' + nbrOfSelected  + ' ' + sectionType + ' sections');
                    if (response) 
                    {
                        //proceed
                    }
                    else
                    {
                        return false;
                    }
                 }
                 
                 form.action = "EmailAdminSectionServlet?" + action;
                 form.submit();
              }
/*
              function selectSection(sectionName,sectionId)
              {
                  var form = document.forms[0];
                  //form.sectionName.value = sectionName;
                 // form.sectionId.value = sectionId;
              }
              */
              ]]>
        </script>        
       
      </head>
      <body onload="javascript:init();">
        <center>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="2">
    <tr>
        <td width="20%" align="left"><img border="0" height="60" src="images/wwwftdcom.gif" width="135"/></td>
        <td width="60%" align="center" colspan="1" class="header"><xsl:value-of select="pageData/data[name='headerName']/value"/></td>
        <td width="20%" align="left">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="3"><hr/></td>
    </tr>
</table>
<form name="form" method="post" action="">
<!-- Required for proper rendering of the view update page -->
<input type="hidden" name="selectedType" />

<input type="hidden" name="securitytoken" value="{pageData/data[name='securitytoken']/value}"/>
<input type="hidden" name="context" value="{pageData/data[name='context']/value}"/>
<input type="hidden" name="sectionName" value=""/>
<input type="hidden" name="sectionType" value="{pageData/data[name='SectionType']/value}"/>
<input type="hidden" name="sectionId" value=""/>
<input type="hidden" name="searchString" value="{pageData/data[name='searchString']/value}"/>
<input type="hidden" name="option" value="{pageData/data[name='option']/value}"/>
<input type="hidden" name="selectedSections" value=""/>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="2"><font color="red"><xsl:value-of select="pageData/data[name='cannotDeleteSections']/value"/></font></td>
	</tr>
    	<tr>
        	<td width="20%"><a href="#" onclick="exitForm('exit');">Email Maintenance</a></td>
   	  	<td width="60%" align="center">
					<input type="button" name="Copy" Value="Copy" onclick="submitForm('copy');" TABINDEX="2"/>
					<input type="button" name="Delete" Value="Delete" onclick="submitForm('delete');" TABINDEX="4"/>
		</td>
		<td width="20%">	<div align="right">
			<input type="button" name="Exit" Value="Exit" onclick="exit();" TABINDEX="5"/>
		    	</div>
		</td>
    	</tr>
</table>

<table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
	<tr>
		<td>
			<table width="98%" border="0" cellpadding="0" cellspacing="0">  
					<tr>
						<td width="5%" class="tbltextheader2 style1">Select</td>
						<td width="11%" class="tbltextheader2 style1"><xsl:value-of select="pageData/data[name='SectionType']/value"/></td>
						<td width="16%" class="tbltextheader2 style1">Effective Date</td>
						<td width="33%" class="tbltextheader2 style1">Description</td>
						<td width="8%" class="tbltextheader2 style1"></td>						
						<td width="12%" class="tbltextheader2 style1">Last Modified By</td>
						<td width="15%" class="tbltextheader2 style1">Last Modified Date</td>
					</tr>
         <xsl:for-each select="SECTIONS/SECTION">
					<tr>
						<!--td align="center"><input type="checkbox" name="{section_name}" id="{section_id}" onclick="selectSection('{section_name}','{section_id}');" TABINDEX="1"/></td-->
						<td align="center"><input type="checkbox" name="{section_name}" id="{section_id}" value="{content_type}" TABINDEX="1"/></td>
           	<td class="style1"><a href="#" onclick='clickUpdate("{section_name}","{content_type}");'><xsl:value-of select="section_name"/></a></td>
						<td align="center"><span class="style1"><xsl:value-of select="start_date"/> - <xsl:value-of select="end_date"/></span></td>
						<td><span class="style1"><xsl:value-of select="description"/></span></td>
						<td align="center"><span class="style1"></span></td>						
						<td align="center"><span class="style1"><xsl:value-of select="updated_by"/></span></td>
						<td align="center"><span class="style1"><xsl:value-of select="updated_on"/></span></td>
					</tr>
				 </xsl:for-each>
					
		    </table>
		</td>
	</tr>
</table>

<table width="98%" border="0" cellpadding="1" cellspacing="1">
    <tr>
    	<td width="20%">&nbsp;</td>
   	  <td align="center" width="60%">
					<input type="button" name="Copy" Value="Copy" onclick="submitForm('copy');" TABINDEX="2"/>
					<input type="button" name="Delete" Value="Delete" onclick="submitForm('delete');" TABINDEX="4"/>
			</td>
 			<td width="20%" align="right">
					<input type="button" name="Exit2" Value="Exit" onclick="exit();" TABINDEX="6"/>
			</td>
	</tr>
  <tr>
      <td align="left" class="errorMessage"><xsl:value-of select="pageData/data[name='ErrorMessage']/value"/></td>
  </tr>
</table>
        <!-- Footer Template -->
        <xsl:call-template name="footer"/>
</form>
</center>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>