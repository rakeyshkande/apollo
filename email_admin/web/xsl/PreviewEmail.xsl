<!DOCTYPE xsl:stylesheet [
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:import href="footer.xsl"/>
<xsl:output method="html" indent="no"/>
<xsl:output indent="yes"/>
<xsl:param name="securitytoken"/>
<xsl:param name="context"/>
<xsl:template match="/root">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=windows-1252"/>
  <title>FTD - Preview Email</title>
  <link rel="stylesheet" type="text/css" href="css/ftd.css"/>     
  <link rel="stylesheet" type="text/css" href="css/calendar.css"/>
  <script type="text/javascript" src="js/date.js"></script>
  <script type="text/javascript" src="js/util.js"></script>
  <script type="text/javascript" src="js/calendar.js"></script>

      
      <script type="text/javascript" language="JavaScript">
      var closeWindow_js = '<xsl:value-of select="@messageSentFlag"/>';
      var programId_js = '<xsl:value-of select="PROGRAM/program_id"/>';
      <![CDATA[
      function init()
      {
          var form = document.forms[0];
          form.programId.value = programId_js;
	      if (closeWindow_js == "true"){
	        returnToParent();
	      }

          var oMyObject = window.dialogArguments;
          //var programId = oMyObject.programId;

          window.name = "VIEW_PREVIEW_EMAIL";
          window.focus();
      }
      
    function returnToParent() {
      closeWindow();
    }
        
      function sendForm()
        {
            var form = document.forms[0];

            if(form.runDate.value == "")
            {
                alert("Requested date must be entered.");
                return false;
            }

			if(!isDate(form.runDate.value)){
				return false;
			}

            if(form.toAddresses.value == "")
            {
                alert("To must be entered.");
                return false;
            }
            
            form.target = window.name;
            var runDate = form.runDate.value;
            var toAddresses = form.toAddresses.value
            var url_source = "PreviewEmailServlet?" + "actionMethod=" +  "preview" + "&runDate=" + runDate + "&toAddresses=" + toAddresses; 
            form.action = url_source;
            form.submit();
       }

        function closeWindow()
        {
        	var val = document.forms[0].programId.value;
        	window.returnValue = val;
         	window.close();
        }

      ]]>
	</script>     
       
</head>
<body onLoad="javascript:init();">
<form name="AssociatePopup" method="get" action="">
<input type="hidden" name="securitytoken" value="{$securitytoken}"/>
<input type="hidden" name="context" value="{$context}"/>
<input type="hidden" name="programId" value="{PROGRAM/program_id}"/>
<input type="hidden" name="actionMethod" value="preview"/>
<center>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="2">
    <tr>
        <td width="20%" align="left"><img border="0" height="60" src="images/wwwftdcom.gif" width="130"/></td>
        <td width="60%" align="center" colspan="1" class="header">Preview Email </td>
        <td width="20%" align="left"></td>
    </tr>
	<tr>
		<td colspan="5" align="center"><font color="red"><xsl:value-of select="ERROR/@message"/></font></td>
	</tr>
</table>
<table width="63%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">

    <tr>
        <td>
            <table width="100%" border="0" cellpadding="2" cellspacing="2">
						  <tr> 
									 <td width="22%" class="label">Requested Date:</td>
									 <td width="78%">
                       <input type="text" name="runDate" maxlength="10"/>&nbsp;&nbsp;
									 		 <img id="runDateButton" src="images/calendar.gif" width="20" height="20" align="ABSMIDDLE"/>
									 </td>
							</tr>
            	<tr>
                   <td width="22%" class="label">To:</td>
									 <td width="78%">
                      <input type="text" name="toAddresses" size="40"/>
                   </td>
              </tr>
              <tr>
                   <td width="22%" class="label">&nbsp;</td>
                   <td width="78%">(seperate with ; for multiple recipients)</td>
              </tr>
              <tr>
                   <td width="22%" class="label">Additional Comments:</td>
                   <td width="78%"><textarea name="additionalComments" rows="4" cols="42"></textarea></td>
              </tr>
            </table>
            
        </td>
    </tr>
</table>
<table width="98%" border="0" cellpadding="2" cellspacing="2">
    <tr>
        <td width="58%" align="right">
            <input name="sendEmail" type="button" value="Send" onclick="sendForm()"/>
            <input name="closebutton" type="button" value="Close" onclick="closeWindow();"/>
        </td>
    		<td width="42%" align="right"></td>
    </tr>
</table>
</center>
        <!-- Footer Template -->
        <xsl:call-template name="footer"/>
</form>
<script type="text/javascript">
  Calendar.setup(
    {
      inputField  : "runDate",  // ID of the input field
      ifFormat    : "mm/dd/y",  // the date format
      button      : "runDateButton"  // ID of the button
    }
  );
</script>
</body>
</html>
 </xsl:template>
</xsl:stylesheet>