<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:output method="html" indent="yes"/>

<xsl:param name="securitytoken"/>
<xsl:param name="context"/>
<xsl:param name="isPopup"/>

<xsl:template match="/">
<!--xsl:variable name="error_id" select="ERRORS/ERROR/@code"/-->
  <html>
  <head>
    <title>FTD - Error</title>
    <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
    <script type="text/javascript" language="javascript" src="js/util.js"/>
       <script type="text/javascript" language="JavaScript">
          <![CDATA[
		    function init () {
				setNavigationHandlers();
		    }
          	function goHome(){
                  var form = document.forms[0];
                  form.action = "EmailAdminMenuServlet?menuAction=EmailConfirmation";
                  form.submit();
          	}
	        function closeWindow()
	        {
	             window.close();
	        }
              ]]>
        </script>
  </head>
  <body onload="javascript:init();">
  
    <form name="form" method="post">
    <input type="hidden" name="adminAction" value="view"/>
    <!-- need this field for logout from error page -->
    <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
    <input type="hidden" name="context" value="{$context}"/>

     <xsl:call-template name="header">
       <xsl:with-param name="headerName" select="'Error'"/>
     </xsl:call-template>
    <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" bordercolor="#006699">
        <tr>
            <td>
        	<table width="100%" border="3" cellpadding="1" cellspacing="1" align="center" bordercolor="#006699">
                    <tr>
                        <td>
                        	<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
                        	<tr><td>
                        	<xsl:choose>
	              	         	<xsl:when test="$isPopup != 'true'">
									Your request cannot be processed due to an internal error:
								</xsl:when>
								<xsl:otherwise>
									Your request cannot be processed. Please correct the following error:
								</xsl:otherwise>
							</xsl:choose>
                        	</td></tr>
                        	
              	         	<tr><td><font color="red"><xsl:value-of select="ERROR/@message"/></font></td></tr>
              	         	<tr><td><xsl:choose>
	              	         	<xsl:when test="$isPopup != 'true'">
									You can click on the link below to start over.
								</xsl:when>
								<xsl:otherwise>
									You can click on the link below to close window.
								</xsl:otherwise>
							</xsl:choose></td></tr>
							<tr><td>&nbsp;</td></tr>
							</table>
						</td>
					</tr>

                </table>
            </td>
        </tr>
		<tr>
			<td colspan="3" align="left" class="LinkBlue">
			<xsl:choose>
  				<xsl:when test="$isPopup != 'true'">
					<a href="javascript:goHome()">Email Confirmation</a>
				</xsl:when>
				<xsl:otherwise>
					<!--a href="javascript:closeWindow()">Close Window</a-->
					<input type="button" value="Close" onclick="javascript:closeWindow()"/>
				</xsl:otherwise>
			</xsl:choose>
			</td>
  		</tr>
    </table>  
    </form>
    <xsl:call-template name="footer"/>
  </body>
  </html>
</xsl:template>

</xsl:stylesheet>