<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="text"/>
  <xsl:param name="orderDetailId"/>
  <xsl:param name="itemNumbers"/>
  <xsl:variable name="v-delimiter" select="'~~'"/>
  <xsl:variable name="v-space" select="' '"/>
  <xsl:variable name="v-empty" select="''"/>
  <xsl:variable name="v-special-characters" select="'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ`~!@#$%^&amp;*()_-+={}[]:;&quot;&lt;&gt;,.?/|\'"/>

  <xsl:template match="/">
    <xsl:value-of select="$orderDetailId"/>
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/item-source-code"/>
    <!-- 1 Source Code -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/header/buyer-first-name"/>
    <!-- 2 Bill To First Name -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/header/buyer-last-name"/>
    <!-- 3 Bill To Last Name -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/header/buyer-address1"/>
    <!-- 4 Bill To Address Line One -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/header/buyer-address2"/>
    <!-- 5 Bill To Address Line Two -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/header/buyer-city"/>
    <!-- 6 Bill To City -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/header/buyer-state"/>
    <!-- 7 Bill To State -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/header/buyer-postal-code"/>
    <!-- 8 Bill To Zip -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:call-template name="TRANSFORM-PHONE-NUMBERS">
      <xsl:with-param name="p-phone-number" select="/order/header/buyer-primary-phone" />
    </xsl:call-template>
     <!--xsl:value-of select="/order/header/buyer-daytime-phone"/-->
    <!-- 9 Bill To Home Phone -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:call-template name="TRANSFORM-PHONE-NUMBERS">
      <xsl:with-param name="p-phone-number" select="/order/header/buyer-secondary-phone" />
    </xsl:call-template>
    <!--xsl:value-of select="/order/header/buyer-evening-phone"/-->
    <!-- 10 Bill To Work Phone -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:call-template name="TRANSFORM-PHONE-NUMBERS">
      <xsl:with-param name="p-phone-number" select="/order/header/buyer-fax" />
    </xsl:call-template>
    <!--xsl:value-of select="/order/header/buyer-fax"/-->
    <!-- 11 Bill To Fax Number -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/header/buyer-email-address"/>
    <!-- 12 Bill to Email -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/header/buyer-country"/>
    <!-- 13 Bill to Country -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:call-template name="transform-credit-card-type">
      <xsl:with-param name="credit-card-type" select="/order/header/cc-type"/>
    </xsl:call-template>
    <!-- 14 CC Type -->
    <xsl:value-of select="/order/header/cc-number"/>
    <!-- 15 CC Number -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/header/cc-exp-date"/>
    <!-- 16 CC Expiration Date -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/header/cc-approval-code"/>
    <!-- 17 CC Approval Code -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/header/cc-avs-result"/>
    <!-- 18 CC AVS Result -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/header/cc-approval-amt"/>
    <!-- 19 CC Approval Amount -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/header/cc-acq-data"/>
    <!-- 20 ACQ Reference Data -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/header/cc-approval-verbage"/>
    <!-- 21 Approval Verbiage -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/header/cc-approval-action-code"/>
    <!-- 22 Approval Action Code -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/tax-amount"/>
    <!-- 23 Tax Amount -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/order-total"/>
    <!-- 24 Order Total -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:for-each select="/order/header/co-brands/co-brand">
      <!-- 25 Co-Branded Info -->
      <xsl:value-of select="name"/>
      <xsl:text>:</xsl:text>
      <xsl:value-of select="data"/>
      <xsl:text>,</xsl:text>
    </xsl:for-each>
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/header/transaction-date"/>
    <!-- 26 Transaction Date -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="count(/order/header/gift-certificates/gift-certificate)"/>
    <!-- 27 Gift Certificate Count -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/header/gift-certificates/gift-certificate/number"/>
    <!-- 28 Gift Certificate Detail -->
    <xsl:text>:</xsl:text>
    <xsl:value-of select="/order/header/gift-certificates/gift-certificate/amount"/>
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/header/news-letter-flag"/>
    <!-- 29 Newsletter Flag -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/recip-first-name"/>
    <!-- 30 Ship To First Name -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/recip-last-name"/>
    <!-- 31 Ship To Last Name -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/recip-address1"/>
    <xsl:text>$</xsl:text>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/recip-address2"/>
    <!-- 32 Ship To Address One = Ship To Address One + '$' + Ship To Address Two-->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="$v-space"/>
    <!-- 33 Ship To Address Two -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/recip-city"/>
    <!-- 34 Ship To City -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/recip-state"/>
    <!-- 35 Ship To State -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/recip-postal-code"/>
    <!-- 36 Zip Code -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/recip-country"/>
    <!-- 37 Country -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/recip-international"/>
    <!-- 38 International Flag -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/qms-address1"/>
    <!-- 39 QMS Address One -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/qms-address2"/>
    <!-- 40 QMS Address Two -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/qms-city"/>
    <!-- 41 QMS City -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/qms-state"/>
    <!-- 42 QMS State -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/qms-postal-code"/>
    <!-- 43 QMS Zip -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/qms-result-code"/>
    <!-- 44 QMS Result Code -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/qms-override-flag"/>
    <!-- 45 QMS Override Flag -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/qms-firm-name"/>
    <!-- 46 QMS Firm Name -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/qms-latitude"/>
    <!-- 47 QMS Latitude -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/qms-longitude"/>
    <!-- 48 QMS Longitude -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/qms-usps-range-record-type"/>
    <!-- 49 QMS USPS Range Record Type -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:call-template name="TRANSFORM-PHONE-NUMBERS">
      <xsl:with-param name="p-phone-number" select="/order/items/item[order-number = $orderDetailId]/recip-phone" />
    </xsl:call-template>
    <!--xsl:value-of select="/order/items/item[order-number = $orderDetailId]/recip-phone"/-->
    <!-- 50 Ship To Phone -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/recip-phone-ext"/>
    <!-- 51 Ship To Extension -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/ship-to-type"/>
    <!-- 52 Ship To Type -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/ship-to-type-name"/>
    <!-- 53 Ship To Type Name -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/ship-to-type-info"/>
    <!-- 54 Ship To Type Info -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/header/occassion"/>
    <!-- 55 Occasion -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/productid"/>
    <!-- 56 Item Number -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/product-price"/>
    <!-- 57 Item Price -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/first-color-choice"/>
    <!-- 58 Color/Size -->
    <xsl:text>:</xsl:text>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/second-color-choice"/>
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="count(/order/items/item[order-number = $orderDetailId]/add-ons/add-on)"/>
    <!-- 59 Add-On Count -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:for-each
                  select="/order/items/item[order-number = $orderDetailId]/add-ons/add-on">
      <!-- 60 Add-On Detail -->
      <xsl:value-of select="id"/>
      <xsl:text>:</xsl:text>
      <xsl:value-of select="quantity"/>
      <xsl:text>,</xsl:text>
    </xsl:for-each>
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/service-fee"/>
    <!-- 61 Service Fee -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/extra-shipping-fee"/>
    <!-- 62 Extra Shipping Fee -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/delivery-date"/>
    <!-- 63 Delivery Date -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/card-message"/>
    <!-- 64 Card Message -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/card-signature"/>
    <!-- 65 Card Signature -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/special-instructions"/>
    <!-- 66 Special Instructions -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/shipping-method"/>
    <!-- 67 Shipping method -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/drop-ship-charges"/>
    <!-- 68 Drop Ship Charges -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/header/buyer-primary-phone-ext"/>
    <!-- 69 Bill To Work Extension -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/second-delivery-date"/>
    <!-- 70 Second Delivery Date -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:choose>
      <xsl:when
                test="/root/orderDetails/orderDetailsData[@order_detail_id = $orderDetailId]/@last_minute_number = ''">
        <!-- 71 Last Minute Gift Flag -->
        <xsl:text>N</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>Y</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/fol-indicator"/>
    <!-- 72 FOL Indicator -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="$v-space"/>
    <!-- 73 Socket Date/Time Stamp -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/lmg-email-address"/>
    <!-- 74 Last Minute Gift Email Address -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/lmg-email-signature"/>
    <!-- 75 Last Minute Gift Email Signature -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/header/master-order-number"/>
    <xsl:text>/</xsl:text>
    <xsl:value-of select="$itemNumbers"/>
    <!-- 76 Master Order Number -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="$v-space"/>
    <!-- 77 Not Used -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/sender-release-flag"/>
    <!-- 78 Sender Release Flag -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/ariba-unspsc-code"/>
    <!-- 79 Ariba UNSPSC Code -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="$v-empty"/>
    <!-- 80 Item Description -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/header/ariba-payload"/>
    <!-- 81 Ariba Payload -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/header/ariba-buyer-cookie"/>
    <!-- 82 Ariba Buyer Cookie -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/header/ariba-asn-buyer-number"/>
    <!-- 83 Ariba ASN Buyer Number -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="$v-empty"/>
    <!-- 84 Order Message URL -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/ariba-po-number"/>
    <!-- 85 Ariba PO Number -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/ariba-cost-center"/>
    <!-- 86 Ariba Cost Center -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="$v-empty"/>
    <!-- 87 User ID -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="$v-empty"/>
    <!-- 88 Call Time -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="$v-empty"/>
    <!-- 89 DNIS -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="$v-empty"/>
    <!-- 90 Yellow Pages Code -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/header/contact-first-name"/>
    <xsl:value-of select="$v-space"/>
    <xsl:value-of select="/order/header/contact-last-name"/>
    <xsl:value-of select="$v-space"/>
    <xsl:call-template name="TRANSFORM-PHONE-NUMBERS">
      <xsl:with-param name="p-phone-number" select="/order/header/contact-phone" />
    </xsl:call-template>
    <!--xsl:value-of select="/order/header/contact-phone"/-->
    <xsl:value-of select="$v-space"/>
    <xsl:value-of select="/order/header/contact-ext"/>
    <xsl:value-of select="$v-space"/>
    <xsl:value-of select="/order/header/contact-email-address"/>
    <!-- 91 Order Comments -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="$v-empty"/>
    <!-- 92 Contact Information -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/header/origin"/>
    <!-- 93 Origin -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="$v-empty"/>
    <!-- 94 Florist Number -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/ariba-ams-project-code"/>
    <!-- 95 AMS Project code -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/military-star-ticket-number"/>
    <!-- 96 Military Star Ticket Number -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/novator-sender-release-flag"/>
    <!-- 97 Novator Sender Release Flag -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/product-substitution-acknowledgement"/>
    <!-- 98 Product substitution acknowledgement -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/retail-variable-price"/>
    <!-- 99 Retail Variable Price -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/header/cobrand_cc_code"/>
    <!-- 100 CoBrand Credit Card Code -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/hp_sequence"/>
    <!-- 101 Sequential Order Number -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/header/buyer_id"/>
    <!-- 102 Buyer Customer Number -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/recip_id"/>
    <!-- 103 Recipient Customer Number -->
    <xsl:value-of select="$v-delimiter"/>
    <!-- 104 Novator Fraud Fields -->
    <xsl:if test="/order/header/fraud_codes/fraud_code != ''">
    <xsl:text>Y</xsl:text>
    <xsl:for-each select="/order/header/fraud_codes/fraud_code">
      <xsl:text>,</xsl:text>
      <xsl:value-of select="."/>
    </xsl:for-each>
    <xsl:value-of select="$v-delimiter"/>
    </xsl:if>
    <!-- 105 Fraud Comments -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:for-each select="/order/header/fraud_comments/fraud_comment">
      <xsl:value-of select="fraud_comments"/>
      <xsl:value-of select="$v-space"/>
    </xsl:for-each>
    <!-- 106 Order Source Code -->
    <xsl:value-of select="/order/header/source-code"/>
    <xsl:value-of select="$v-delimiter"/>
    <!-- 107 Item of the week flag -->
    <xsl:value-of select="/order/items/item[order-number = $orderDetailId]/item-of-the-week"/>
    <xsl:value-of select="$v-delimiter"/>
    <!--108 pdb price-->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number=$orderDetailId]/pdb-price"/>
    <!--109 transaction-->
     <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number=$orderDetailId]/transaction"/> 
    <!--110 wholesale-->
     <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number=$orderDetailId]/wholesale"/> 
    <!--111 commission-->
     <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number=$orderDetailId]/commission"/> 
    <!--112 Loss prevention Indicator -->
    <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="$v-empty"/>
    <!--113 shipping tax-->
     <xsl:value-of select="$v-delimiter"/>
    <xsl:value-of select="/order/items/item[order-number=$orderDetailId]/shipping-tax"/> 
  </xsl:template>
  <xsl:template name="transform-credit-card-type">
    <xsl:param name="credit-card-type"/>
    <xsl:choose>
      <!-- CC Type -->
      <xsl:when test="contains($credit-card-type, 'VI')">
        <xsl:value-of select="'Visa'" />
      </xsl:when>
      <xsl:when test="contains($credit-card-type, 'MC')">
        <xsl:value-of select="'Master'"/>
      </xsl:when>
      <xsl:when test="contains($credit-card-type, 'AX')">
        <xsl:value-of select="'Amex'" />
      </xsl:when>
      <xsl:when test="contains($credit-card-type, 'DI')">
        <xsl:value-of select="'Discover'"/>
      </xsl:when>
      <xsl:when test="contains($credit-card-type, 'DC')">
        <xsl:value-of select="'Diners'"/>
      </xsl:when>
      <xsl:when test="contains($credit-card-type, 'CB')">
        <xsl:value-of select="'Carte'"/>
      </xsl:when>
      <xsl:otherwise>
      	<xsl:value-of select="$credit-card-type"/>
      </xsl:otherwise>
    </xsl:choose>
    <!-- Add Delimiter -->
    <xsl:value-of select="$v-delimiter"/>
  </xsl:template>

  <xsl:template name="TRANSFORM-PHONE-NUMBERS" >
      <xsl:param name="p-phone-number" />
      <xsl:value-of select="translate($p-phone-number, $v-special-characters, '')"/>
  </xsl:template>

</xsl:stylesheet>