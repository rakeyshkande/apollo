package com.ftd.osp.test.ordergatherer;

import com.ftd.ftdutilities.GeneralConstants;
import com.ftd.osp.ordergatherer.OrderGatherer;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.order.vo.AVSAddressScoreVO;
import com.ftd.osp.utilities.order.vo.AVSAddressVO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.JAXPUtil;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Random;

import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ftd.osp.utilities.order.ScrubMapperDAO;
import com.ftd.osp.utilities.order.OrderXAO;

import java.io.PrintWriter;
import java.io.StringWriter;

import java.sql.Connection;
import java.sql.DriverManager;

import javax.xml.xpath.XPathExpressionException;

public class OrderGathererTest  extends TestCase
{
    private static String URL = "";
    private static OrderGatherer orderGatherer;
    private String orderXMLString = "";
    private Document orderXMLDocument = null;
    private OrderVO order = null;
    private ConfigurationUtil configUtil = null;
    private String SUCCESS = "<HTML><HEAD><TITLE>200 OK</TITLE></HEAD><BODY><H1>200 OK</H1>Success</BODY></HTML>";
    private String FAILURE = "java.io.IOException: Server returned HTTP response code: 600";
    private final static String GATHERER_CONFIG_FILE = "gatherer_config.xml";
    private final static String BUYER_EMAIL     = "@b.com";
    private final static String BUYER_EMAIL_PFX = "test";
    
    // These arrays should each contain same number of elements
    private final static String[] LNAMES = {"Salami", "Bologna", "Pastrami", "Ham", "Roastbeef", 
                                            "Turkey", "Bacon", "Lettuce", "Tomato", "Mayonnaise", 
                                            "Onion", "Mustard", "Meatball", "Tabasco", "Wasabi", 
                                            "Salsa", "Worcestershire", "Horseradish", "Croissant", "Sauerkraut",
                                            "Cheesecake", "Tiramisu", "Pepperoni", "Pierogi", "Keylime",
                                            "Pickles", "Potatosoup", "Souffle", "Tortilla", "Mozzarella"};
    private final static String[] FNAMES = {"Todd", "Neil", "Buddy", "Roger", "Tony",
                                            "Matilda", "Maggie", "Anna", "Jo", "Julia",
                                            "Pat", "Leslie", "Ian", "Hector", "Sasha",
                                            "Timmy", "Eric", "Silvia", "John", "Mary",
                                            "Bo", "Ma", "Pops", "Wilma", "Peter",
                                            "Stosh", "Lisa", "Betty", "Carol", "Victoria"};
    private final static String[] MNAMES = {" A.", " B.", " C.", " D.", " E.",
                                            " F.", " G.", " H.", " I.", "",
                                            " K.", " L.", " M.", " N.", " O.",
                                            " P.", " Q.", " R.", " S.", "",
                                            " J.", " T.", " U.", " V.", "",
                                            " W.", " X.", " Y.", " Z.", ""};
    
  //public static final String DB_CONNECTION_STRING = "jdbc:oracle:thin:@stheno-dev.ftdi.com:1521:DEV5";
  //public static final String DB_USERNAME = "osp";
  //public static final String DB_PASSWORD = "osp";
  
   private Connection connection;


    
    /** 
     * Create a constructor that take a String parameter and passes it 
     * to the super class 
     **/ 
    public OrderGathererTest(String name) { 
        super(name); 
        try
        {
          configUtil = ConfigurationUtil.getInstance();
          URL = configUtil.getProperty(GATHERER_CONFIG_FILE, "url");
          URL="http://localhost/OG/servlet/OrderGatherer";////// CHAS TEST URL
          //URL="http://iron2.ftdi.com:8712/OG/servlet/OrderGatherer";////// IRON2 TEST URL
          Connection connection = this.connection;
           
        }
        catch(Exception e)
        {
          e.printStackTrace();
        }

    }

 protected void setUp()
    { 
       
    }   

    /** 
     * Test Order Gatherer with multiple orders from one or more order XML templates.  
     * Certain tags will be replaced within the templates (e.g., master order number)
     * for each order.  See "multiOrder_..." properties in GATHERER_CONFIG_FILE for 
     * configurable options.  Note that if multiple templates are used the filenames
     * must be numbered (e.g., filename.1.xml, filename.2.xml, etc.) 
     **/ 
    public void testMultipleOrders() throws Exception{ 
        System.out.println("Testing multiple orders");
        //setUp(configUtil.getProperty(GATHERER_CONFIG_FILE, "multiOrder_template"));
        Document orderTemplateDoc = null;
        int arrIndx1, arrIndx2, arrIndx3;
        int cntOrder;
        int cntTemp = 0;
        int templateNum = 0;
        String orderTemplateFilename;
        
        // Get configuration properties
        //
        int orderNum        = Integer.parseInt(configUtil.getProperty(GATHERER_CONFIG_FILE, "multiOrder_orderStartNum"));
        int masterNum       = Integer.parseInt(configUtil.getProperty(GATHERER_CONFIG_FILE, "multiOrder_masterStartNum"));
        String orderPrefix  = configUtil.getProperty(GATHERER_CONFIG_FILE, "multiOrder_orderPrefix");
        String masterPrefix = configUtil.getProperty(GATHERER_CONFIG_FILE, "multiOrder_masterPrefix");
        String createdDate  = configUtil.getProperty(GATHERER_CONFIG_FILE, "multiOrder_createdDate");
        String deliveryDate = configUtil.getProperty(GATHERER_CONFIG_FILE, "multiOrder_deliveryDate");
        int numOrders       = Integer.parseInt(configUtil.getProperty(GATHERER_CONFIG_FILE, "multiOrder_numOrders"));
        int numTemplates    = Integer.parseInt(configUtil.getProperty(GATHERER_CONFIG_FILE, "multiOrder_numTemplates"));
        int numOrdPerTemp = numOrders / numTemplates;

        // Loop and send specified number of orders
        //
        for (cntOrder=0; cntOrder < numOrders; cntOrder++) {
        
            // Open order XML template if necessary
            //
            if (cntTemp-- <= 0) {
              cntTemp = numOrdPerTemp;
              templateNum++;
              if (templateNum > numTemplates) {templateNum = 1;}
              orderTemplateFilename = configUtil.getProperty(GATHERER_CONFIG_FILE, "multiOrder_template") + templateNum + ".xml";
              orderTemplateDoc = DOMUtil.getDocument(new File(orderTemplateFilename));
            }
            
            // Replace fields in order XML template appropriately
            //
            Node xmlNode = DOMUtil.selectSingleNode(orderTemplateDoc, "order/header/master-order-number/text()"); 
            xmlNode.setNodeValue(masterPrefix + masterNum++);

            xmlNode = DOMUtil.selectSingleNode(orderTemplateDoc, "order/items/item/order-number/text()"); 
            xmlNode.setNodeValue(orderPrefix + orderNum++);

            xmlNode = DOMUtil.selectSingleNode(orderTemplateDoc, "order/header/transaction-date/text()"); 
            xmlNode.setNodeValue(createdDate);

            xmlNode = DOMUtil.selectSingleNode(orderTemplateDoc, "order/items/item/delivery-date/text()"); 
            xmlNode.setNodeValue(deliveryDate);

            //String format = "%010d\n";
            arrIndx1 = new Random().nextInt(LNAMES.length);  // Random index to names array
            arrIndx2 = new Random().nextInt(LNAMES.length); 
            arrIndx3 = new Random().nextInt(LNAMES.length); 
            xmlNode = DOMUtil.selectSingleNode(orderTemplateDoc, "order/header/buyer-first-name/text()"); 
            xmlNode.setNodeValue(FNAMES[arrIndx1] + MNAMES[arrIndx2]);
            xmlNode = DOMUtil.selectSingleNode(orderTemplateDoc, "order/header/buyer-last-name/text()"); 
            xmlNode.setNodeValue(LNAMES[arrIndx3]);
            xmlNode = DOMUtil.selectSingleNode(orderTemplateDoc, "order/header/buyer-email-address/text()"); 
            xmlNode.setNodeValue(BUYER_EMAIL_PFX + (orderNum - 1) + BUYER_EMAIL);
            
            arrIndx1 = new Random().nextInt(LNAMES.length);  // Random index to names array
            arrIndx2 = new Random().nextInt(LNAMES.length); 
            arrIndx3 = new Random().nextInt(LNAMES.length); 
            xmlNode = DOMUtil.selectSingleNode(orderTemplateDoc, "order/items/item/recip-first-name/text()"); 
            xmlNode.setNodeValue(FNAMES[arrIndx3] + MNAMES[arrIndx2]);
            xmlNode = DOMUtil.selectSingleNode(orderTemplateDoc, "order/items/item/recip-last-name/text()"); 
            xmlNode.setNodeValue(LNAMES[arrIndx1]);

            // Send to OrderGatherer
            //
            StringWriter s = new StringWriter();
            DOMUtil.print(orderTemplateDoc, new PrintWriter(s));
            String orderTemplateStr = s.toString();
            Map map = new HashMap();
            map.put("Order", orderTemplateStr);
            System.out.println("xml: " + orderTemplateStr); 
            String response = send(URL,map);          
            System.out.println("response: " + response);
            // Validate response
            // Check if the result is the same as expected
            assertEquals("testMultipleOrders result", SUCCESS,response);
        }
    }


    /** 
     * Companion method for testMultipleOrders.  It will send email messages to email-request-processing
     * for the orders submitted from a prior call to testMultipleOrders.
     * Like testMultipleOrders, one or more email XML templates may be used.  
     * See "multiOrder_..." properties in GATHERER_CONFIG_FILE for 
     * configurable options.  Note that if multiple templates are used the filenames
     * must be numbered (e.g., filename.1.xml, filename.2.xml, etc.) 
     **/ 
    public void testMultipleEmails() throws Exception{ 
        System.out.println("Testing multiple emails");
        Document emailTemplateDoc = null;
        int cntOrder;
        int cntTemp = 0;
        int templateNum = 0;
        String emailTemplateFilename;
        
        // Get configuration properties
        //
        int orderNum        = Integer.parseInt(configUtil.getProperty(GATHERER_CONFIG_FILE, "multiOrder_orderStartNum"));
        String orderPrefix  = configUtil.getProperty(GATHERER_CONFIG_FILE, "multiOrder_orderPrefix");
        String emailPrefix  = configUtil.getProperty(GATHERER_CONFIG_FILE, "multiOrder_emailSeqPrefix");
        int numOrders       = Integer.parseInt(configUtil.getProperty(GATHERER_CONFIG_FILE, "multiOrder_numEmails"));
        int numTemplates    = Integer.parseInt(configUtil.getProperty(GATHERER_CONFIG_FILE, "multiOrder_numEmailTemplates"));
        int numOrdPerTemp = numOrders / numTemplates;

        // Loop and send specified number of orders
        //
        for (cntOrder=0; cntOrder < numOrders; cntOrder++) {
        
            // Open email XML template if necessary
            //
            if (cntTemp-- <= 0) {
              cntTemp = numOrdPerTemp;
              templateNum++;
              if (templateNum > numTemplates) {templateNum = 1;}
              emailTemplateFilename = configUtil.getProperty(GATHERER_CONFIG_FILE, "multiOrder_emailTemplate") + templateNum + ".xml";
              emailTemplateDoc = DOMUtil.getDocument(new File(emailTemplateFilename));
            }
            
            // Replace fields in email XML template appropriately
            //
            Node xmlNode = DOMUtil.selectSingleNode(emailTemplateDoc, "email-request/order-number/text()"); 
            xmlNode.setNodeValue(orderPrefix + orderNum);

            xmlNode = DOMUtil.selectSingleNode(emailTemplateDoc, "email-request/billing-email/text()"); 
            xmlNode.setNodeValue(BUYER_EMAIL_PFX + orderNum + BUYER_EMAIL);
            
            xmlNode = DOMUtil.selectSingleNode(emailTemplateDoc, "email-request/server-id/text()"); 
            xmlNode.setNodeValue(emailPrefix);
            xmlNode = DOMUtil.selectSingleNode(emailTemplateDoc, "email-request/sequence-number/text()"); 
            xmlNode.setNodeValue(emailPrefix + "-" + orderNum);
            orderNum++;

            // Send to email-request-processing
            //
            StringWriter s = new StringWriter();
            DOMUtil.print(emailTemplateDoc, new PrintWriter(s));
            String orderTemplateStr = s.toString();
            Map map = new HashMap();
            map.put("EmailRequest", orderTemplateStr);
            System.out.println("xml: " + orderTemplateStr); 
            String urlEmail = configUtil.getProperty(GATHERER_CONFIG_FILE, "urlEmail");
            String response = send(urlEmail,map);          
            System.out.println("response: " + response);
            // Validate response
            // Check if the result is the same as expected
            assertEquals("testMultipleEmails result", SUCCESS,response);
        }
    }

    
   /** 
    * Test Order Gatherer with a valid order.  A 200 response
    * indicates the order was a valid order.  
    **/ 
    public void testGoodOrder() throws Exception{ 
          System.out.println("testing good order");
          setUp(configUtil.getProperty(GATHERER_CONFIG_FILE, "good_order"));
          Map map = new HashMap();
          map.put("Order",this.orderXMLString);
          String response = send(URL,map);          
          System.out.println("response: " + response);
          //Validate response
          //Check if the result is the same as expected
          assertEquals("testGoodOrder result", SUCCESS,response);
 } 

  /** 
    * Test Order Gatherer with a duplicate order.  A 200 
    * response indicates the order was a duplicate order.
    **/ 
    public void testDuplicateOrder() throws Exception{ 
          System.out.println("testing duplicate order");
          setUp(configUtil.getProperty(GATHERER_CONFIG_FILE, "duplicate_order"));
          Map map = new HashMap();
          map.put("Order",this.orderXMLString);
          String response = send(URL,map);          
          System.out.println(response);

          //Validate response
          //Check if the result is the same as expected
          assertEquals("testDuplicateOrder result", SUCCESS,response);
       
  } 

  /** 
    * Test Order Gatherer with an order with total
    * order count wrong.  A 600 response
    * indicates the order is an invalid order.
    **/ 
    public void testOrderCountWrong() throws IOException, Exception{ 
          System.out.println("testing order count wrong");
          setUp(configUtil.getProperty(GATHERER_CONFIG_FILE, "order_count_wrong"));
          Map map = new HashMap();
          map.put("Order",this.orderXMLString);
          try
          {
            String response = send(URL,map);
          }
          catch (IOException e)
          {
             String result = e.toString();
             result = result.substring(0,60);
             System.out.println("result: " + result);
             assertEquals("testOrderCountWrong result", FAILURE,result);
          }
  }

  

  /** 
    * Test Order Gatherer with an order that has
    * an invalid order prefix.  A 600 response
    * indicates the order is an invalid order.
    **/ 
    public void testInvalidOrderPrefix() throws Exception{
          System.out.println("testing invalid order prefix");
          setUp(configUtil.getProperty(GATHERER_CONFIG_FILE, "invalid_order_prefix"));
          Map map = new HashMap();
          map.put("Order",this.orderXMLString);
          try
          {
            String response = send(URL,map);
          }
          catch (IOException e)
          {
             String result = e.toString();
             result = result.substring(0,60);
             assertEquals("testOrderCountWrong result", FAILURE,result);
          }
  } 

  /** 
    * Test Order Gatherer with a test credit card number
    * and a test buyer email address.  A 200 
    * response indicates the order was a test order.
    **/ 
    public void testCreditCardUsed() throws Exception{ 
          System.out.println("testing if test credit card was used");
          setUp(configUtil.getProperty(GATHERER_CONFIG_FILE, "test_credit_card"));
          Map map = new HashMap();
          map.put("Order",this.orderXMLString);
          String response = send(URL,map);          
          System.out.println(response);

          //Validate response
          //Check if the result is the same as expected
          assertEquals("testCreditCardUsed result", SUCCESS,response);
       
  } 
  
  public void testGoodOrderHP() throws Exception 
  {
          System.out.println("testing Good order to HP");
          setUp(configUtil.getProperty(GATHERER_CONFIG_FILE, "test_good_order_hp"));
          Map map = new HashMap();
          map.put("Order",this.orderXMLString);
          String response = send(URL,map);          
          System.out.println(response);

          //Validate response
          //Check if the result is the same as expected
          assertEquals("testGoodOrder result", SUCCESS,response);
      
  }
  public void testGoodOrderHP2() throws Exception 
  {
          System.out.println("testing Good order to HP2");
           
            ScrubMapperDAO scrubMapperDAO = new ScrubMapperDAO(connection);

            String guid = "FTD_GUID_-3611505980718788907068338282901313396651018010004160-6562644530-6055101106521022421-17921378070-122809908605411858380-1771112660251-52581962368121630732173-119370388474-399152074214";
            OrderVO order = new OrderVO();
            order = scrubMapperDAO.mapOrderFromDB(guid);
            System.out.println(order.toString());
            OrderXAO orderXAO = new OrderXAO();
            Document document = orderXAO.generateOrderXML(order);
            System.out.println(document.toString());            
            //scrubMapperDAO.mapOrderToDB(order);
            
          
          //setUp(configUtil.getProperty(GATHERER_CONFIG_FILE, "test_good_order_hp"));
          Map map = new HashMap();
          map.put("Order",document.toString());
          String response = send(URL,map);          
          System.out.println(response);

          //Validate response
          //Check if the result is the same as expected
          assertEquals("testGoodOrder result", SUCCESS,response);
      
  }

  /**
   * Overloaded method for setting up initialization variables 
   * @param fileName String name of the file with the xml
   * to test
   */
  private void setUp(String fileName)
    { 
      try
      {
        System.out.println("getting xml");
        String record = "";
       
        FileReader fr     = new FileReader(fileName);
        
        BufferedReader br = new BufferedReader(fr);
        StringBuffer sb = new StringBuffer();
        while ((record = br.readLine()) != null) {
           sb.append(record);
        }
        System.out.println("got xml");
        this.orderXMLString = sb.toString();
        //System.out.println("orderXMLString: " + orderXMLString);
                    
      }
      catch(Exception ex)
      {
        ex.printStackTrace();
      }
    
    }   

  /**
   * Get connection to URL. Construct request and get back a response.
   */
	private synchronized String send(String urlString, Map params)
        throws Exception{

        URLConnection con = null;
        boolean firstx      = true;

        try {
            URL url = new URL(urlString);

            //create connection to URL
            con = url.openConnection();
            con.setDoOutput(true);

            //Send data
            OutputStream out = con.getOutputStream();
            Set entries = params.entrySet();
            Iterator iter = entries.iterator();

            StringBuffer paramString = new StringBuffer();
            while(iter.hasNext()) {
                if (firstx)
                {
                  firstx = false;
                }
                else
                {
                    paramString.append('&');
                }

                Map.Entry entry = (Map.Entry)iter.next();
                String key = (String)entry.getKey();
                String value = (String)entry.getValue();

                paramString.append(key);
                paramString.append('=');
                paramString.append(URLEncoder.encode(value));
            }

            out.write(paramString.toString().getBytes());
            out.flush();
            out.close();

            InputStream is = con.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line=null;
            StringBuffer response = new StringBuffer("");
            while( (line = br.readLine())!=null)
            {
                response.append(line);
            }

            return response.toString();

        }
         catch (java.net.MalformedURLException e)  {
                e.printStackTrace();
                throw e;
        }
         catch (java.io.IOException e)  {
                e.printStackTrace();
                throw e;
        }
        finally{
            con = null;
        }

    }//end, send
    
    public static TestSuite suite()
    {
      TestSuite suite = new TestSuite();
      try {
      
        suite.addTest(new OrderGathererTest("testMultipleOrders"));
        //suite.addTest(new OrderGathererTest("testMultipleEmails"));
         //suite.addTest(new OrderGathererTest("testDuplicateOrder"));
         // suite.addTest(new OrderGathererTest("testOrderCountWrong"));
          // suite.addTest(new OrderGathererTest("testGoodOrderHP"));
          //suite.addTest(new OrderGathererTest("testInvalidOrderPrefix"));
        //suite.addTest(new OrderGathererTest("testCreditCardUsed"));
         //   suite.addTest(new OrderGathererTest("testGoodOrderHP2"));      
      } catch (Exception e) {
          e.printStackTrace();
      }

      return suite;
    } 

    public static void main(String args[])
{
  junit.textui.TestRunner.run( suite() );
}

}
