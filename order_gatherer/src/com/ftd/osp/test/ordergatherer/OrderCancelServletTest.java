package com.ftd.osp.test.ordergatherer;


import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.order.OrderXAO;
import com.ftd.osp.utilities.order.ScrubMapperDAO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import junit.framework.TestCase;
import java.sql.Connection;
import junit.framework.TestSuite;
import org.w3c.dom.Document;
import java.util.*;


public class OrderCancelServletTest extends TestCase
{

  private ConfigurationUtil configUtil = null;
  private final static String GATHERER_CONFIG_FILE = "gatherer_config.xml";
  public final static String ORDER_SCRUB = "ORDER SCRUB";
  private String URL = "";
  private String SUCCESS = "<HTML><HEAD><TITLE>200 OK</TITLE></HEAD><BODY><H1>200 OK</H1>Success</BODY></HTML>";
  private String FAILURE = "java.io.IOException: Server returned HTTP response code: 600";
  
  private Connection connection = null;
  private OrderVO order = null;
  private String guid = "FTD_GUID_10009707520-5442346650-3130211280-12670633820696051727013573911400-18900551150-16462513241-21126591660-30179335703422522630-2124059292251-200047780468-134540777010050934113235-322208165240";
  
  /**
   * To get this test class to work you must have OG running
   * A simple way to do this is to run the novator_test.html
   * which will open a browser window. 
   * After that you can minimize the window and debug this class.
   * @param name
   */
  public OrderCancelServletTest(String name)
  {
    super(name);
    try 
    {
       configUtil = ConfigurationUtil.getInstance();
       //URL used to send a request to the OrderCancelServlet
       URL = "http://192.168.110.236:8989/order_gatherer-order_gatherer-context-root/OrderCancelServlet";
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }
  
  protected void setUp()
  {
      try
      {
        connection = getConnection();
        ScrubMapperDAO dao = new ScrubMapperDAO(connection);
        order = dao.mapOrderFromDB( guid );
      }
      catch(Exception e)
      {
        e.printStackTrace();
      }
  }
  
  public void testCancelOrder()throws Exception
  {
   
      System.out.println("testing OrderCancelServlet...");
      
      Map map = new HashMap();
      map.put("masterOrderNumber",order.getMasterOrderNumber());
      System.out.println(URL);
      
      String response = send(URL,map);          
      assertEquals("test OrderCancel", SUCCESS, response);
  }
  
  public void testOrderShipped() throws Exception
  {
      System.out.println("testing OrderCancelServlet...Order Shipped");
      
      Map map = new HashMap();
      map.put("masterOrderNumber",order.getMasterOrderNumber());
      System.out.println(URL);
      
      String response = send(URL,map);          
      assertEquals("test OrderCancel", SUCCESS, response);
  }
  
  
  
   /**
   * Get connection to URL. Construct request and get back a response.
   */
	private synchronized String send(String urlString, Map params)
        throws Exception{

        URLConnection con = null;
        boolean firstx      = true;

        try {
            URL url = new URL(urlString);

            //create connection to URL
            con = url.openConnection();
            con.setDoOutput(true);

            //Send data
            OutputStream out = con.getOutputStream();
            Set entries = params.entrySet();
            Iterator iter = entries.iterator();

            StringBuffer paramString = new StringBuffer();
            while(iter.hasNext()) {
                if (firstx)
                {
                  firstx = false;
                }
                else
                {
                    paramString.append('&');
                }
            
                Map.Entry entry = (Map.Entry)iter.next();
                String key = (String)entry.getKey();
                String value = (String)entry.getValue();
           
                paramString.append(key);
                paramString.append('=');
                paramString.append(URLEncoder.encode(value));
            }
      System.out.println(paramString.toString());
            out.write(paramString.toString().getBytes());
            out.flush();
            out.close();

            InputStream is = con.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line=null;
            StringBuffer response = new StringBuffer("");
            while( (line = br.readLine())!=null)
            {
                response.append(line);
            }

            return response.toString();

        }
         catch (java.net.MalformedURLException e)  {
                e.printStackTrace();
                throw e;
        }
         catch (java.io.IOException e)  {
                e.printStackTrace();
                throw e;
        }
        finally{
            con = null;
        }

    }//end, send
  
  
  
     //method for retrieving a connection to the database
    public static Connection getConnection() throws Exception {
            String driver_ = "oracle.jdbc.driver.OracleDriver";
            String database_ ="jdbc:oracle:thin:@stheno-dev.ftdi.com:1521:DEV7";
            String user_ = "emueller";
            String password_ = "emueller";
            Class.forName(driver_);
            Connection conn = DriverManager.getConnection(database_,user_, password_);
            conn.setAutoCommit(true);
            return conn;
  }//end getConnection()
  
  

   public static TestSuite suite()
    {
      TestSuite suite = new TestSuite();
      try {
          suite.addTest(new OrderCancelServletTest("testCancelOrder"));
         // suite.addTest(new OrderCancelServletTest("testOrderShipped"));
      } catch (Exception e) {
          e.printStackTrace();
      }

      return suite;
    } 

    public static void main(String args[])
    {
      junit.textui.TestRunner.run( suite() );
    }
  
}