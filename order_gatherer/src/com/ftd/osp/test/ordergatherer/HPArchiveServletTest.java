package com.ftd.osp.test.ordergatherer;

import com.ftd.osp.ordergatherer.HPArchiveServlet;
import com.ftd.osp.utilities.ConfigurationUtil;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.w3c.dom.Document;


public class HPArchiveServletTest  extends TestCase
{
    private static String URL = "";
    private static HPArchiveServlet hpArchiveServlet;
    private ConfigurationUtil configUtil = null;
    private String FAILURE = "Transmission to HP Failed";
    private final static String GATHERER_CONFIG_FILE = "gatherer_config.xml";

    
    /** 
     * Create a constructor that take a String parameter and passes it 
     * to the super class 
     **/ 
    public HPArchiveServletTest(String name) { 
        super(name); 
        try
        {
          configUtil = ConfigurationUtil.getInstance();
          URL = configUtil.getProperty(GATHERER_CONFIG_FILE, "hp_url");

        }
        catch(Exception e)
        {
          e.printStackTrace();
        }

    }

 protected void setUp()
    { 
       
    }   
   
   /** 
    * Test HPArchiveServlet with a valid master order number
    * and a trans type of HP_STRING.  Test is successful
    * if response is not equal to FAILURE
    **/ 
    public void testGetHPString() throws Exception{ 
        System.out.println("testing get hp string");
        Map map = new HashMap();
        map.put("masterOrderNumber",configUtil.getProperty(GATHERER_CONFIG_FILE, "hp_master_order_number"));
        map.put("transType",configUtil.getProperty(GATHERER_CONFIG_FILE, "hp_trans_type"));
        String response = send(URL,map);          
        System.out.println("response: " + response);
        //Validate response
        //Check if the result is the same as expected
        assertTrue(!response.equals(FAILURE));
    } 

    /** 
    * Test HPArchiveServlet with an invalid master order number
    * and a trans type of HP_STRING.  Test is successful
    * if response is equal to FAILURE
    **/ 
    public void testInvalidMasterOrderNumber() throws Exception{ 
        System.out.println("testing invalid master order number");
        Map map = new HashMap();
        map.put("masterOrderNumber",configUtil.getProperty(GATHERER_CONFIG_FILE, "invalid_hp_master_order_number"));
        map.put("transType",configUtil.getProperty(GATHERER_CONFIG_FILE, "hp_trans_type"));
        String response = send(URL,map);          
        System.out.println("response: " + response);
        //Validate response
        //Check if the result is the same as expected
        assertTrue(response.equals(FAILURE));
    }

    /** 
    * Test HPArchiveServlet with a valid master order number
    * and a trans type of XML_STRING.  Test is successful
    * if response is not equal to FAILURE
    **/ 
    public void testGetXMLString() throws Exception{ 
        System.out.println("testing get xml string");
        Map map = new HashMap();
        map.put("masterOrderNumber",configUtil.getProperty(GATHERER_CONFIG_FILE, "hp_master_order_number"));
        map.put("transType",configUtil.getProperty(GATHERER_CONFIG_FILE, "xml_trans_type"));
        String response = send(URL,map);          
        System.out.println("response: " + response);
        //Validate response
        //Check if the result is the same as expected
        assertTrue(!response.equals(FAILURE));
    } 

 

  /**
   * Get connection to URL. Construct request and get back a response.
   */
	private synchronized String send(String urlString, Map params)
        throws Exception{

        URLConnection con = null;
        boolean firstx      = true;

        try {
            URL url = new URL(urlString);

            //create connection to URL
            con = url.openConnection();
            con.setDoOutput(true);

            //Send data
            OutputStream out = con.getOutputStream();
            Set entries = params.entrySet();
            Iterator iter = entries.iterator();

            StringBuffer paramString = new StringBuffer();
            while(iter.hasNext()) {
                if (firstx)
                {
                  firstx = false;
                }
                else
                {
                    paramString.append('&');
                }

                Map.Entry entry = (Map.Entry)iter.next();
                String key = (String)entry.getKey();
                String value = (String)entry.getValue();

                paramString.append(key);
                paramString.append('=');
                paramString.append(URLEncoder.encode(value));
            }

            out.write(paramString.toString().getBytes());
            out.flush();
            out.close();

            InputStream is = con.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line=null;
            StringBuffer response = new StringBuffer("");
            while( (line = br.readLine())!=null)
            {
                response.append(line);
            }

            return response.toString();

        }
         catch (java.net.MalformedURLException e)  {
                e.printStackTrace();
                throw e;
        }
         catch (java.io.IOException e)  {
                e.printStackTrace();
                throw e;
        }
        finally{
            con = null;
        }

    }//end, send
    
    public static TestSuite suite()
    {
      TestSuite suite = new TestSuite();
      try {
      
      suite.addTest(new HPArchiveServletTest("testGetHPString"));
      suite.addTest(new HPArchiveServletTest("testInvalidMasterOrderNumber"));
      suite.addTest(new HPArchiveServletTest("testGetXMLString"));

      } catch (Exception e) {
          e.printStackTrace();
      }

      return suite;
    } 

    public static void main(String args[])
{
  junit.textui.TestRunner.run( suite() );
}

}