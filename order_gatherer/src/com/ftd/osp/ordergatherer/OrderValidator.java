package com.ftd.osp.ordergatherer;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.PartnerUtility;
import com.ftd.osp.utilities.order.MercentOrderPrefixes;
import com.ftd.osp.utilities.order.vo.BuyerEmailsVO;
import com.ftd.osp.utilities.order.vo.BuyerVO;
import com.ftd.osp.utilities.order.vo.CreditCardsVO;
import com.ftd.osp.utilities.order.vo.MercentChannelMappingVO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.PaymentsVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.PartnerMappingVO;




/**
 * This class takes the order value object that was assembled
 * from the xml transmission file and does a couple of
 * preliminary validation checks.
 *
 * @author Rose Lazuk
 */
public class OrderValidator 
{
   public static final int TEST_ORDER = 2;
   public static final int VALID_ORDER = 0;
   
   private Logger logger;
   private ConfigurationUtil configUtil;
   
   
   public OrderValidator() throws Exception
   {
    logger = new Logger("com.ftd.osp.ordergatherer.OrderValidator"); 
    configUtil = ConfigurationUtil.getInstance();
   }

  /**
   * 
   * @param order Order this is the value object that holds the order
   * @exception OrderValidationException this is thrown if line number
   *            doesn't equal total number of line items sent in xml file
   *            or if there is an invalid order prefix number or if 
   *            a test credit card number and test email address are used.
   */
  public int validate(OrderVO order, Connection conn) throws Exception, OrderValidationException
  {
   boolean success = false;
   OrderDetailsVO orderDetails = null;

   //check to see if a test credit card number and test email
   //address was used.  If so, the order needs to be thrown out
   //so no further processing is done on it.
   boolean isTestCreditCard = this.checkForTestCC(order);
   boolean isTestEmailAddress = this.checkForTestEmailAddress(order);

   if(isTestCreditCard && isTestEmailAddress)
   {
     //throw new TestOrderException();
     return TEST_ORDER;
   }
    
   if (isCalyxOrder(order)) {
       return VALID_ORDER;
   }
   
   //check line # against total number of items received for this order
   List orderDetailsList = order.getOrderDetail();
   int itemCount = orderDetailsList.size();
   try
   {
      if (itemCount != Integer.parseInt(order.getProductsTotal()))
        throw new OrderValidationException( OrderGatherConstants.ORDER_COUNT_ERROR );
   }
   catch(Exception e)
   {
      throw new OrderValidationException( OrderGatherConstants.ORDER_COUNT_ERROR );  
   }
   //check order number for valid prefixes
   
   //load list with valid prefixes from global parms    
   String prefixes_list=configUtil.getFrpGlobalParm(OrderGatherConstants.GATHERER_CONFIG_CONTEXT, OrderGatherConstants.VALID_ORDER_PREFIXES);
   String[] prefixes = prefixes_list.split(",");  
   
   for (int i=0; i<orderDetailsList.size(); i++)
   {
      orderDetails = (OrderDetailsVO)orderDetailsList.get(i);
      String orderNumber = orderDetails.getExternalOrderNumber();
      success = false; 
      String prefix = null;
      
      for (int j=0; j<prefixes.length; j++)
      {
        prefix = prefixes[j];

        if (orderNumber.startsWith(prefix))
        {
          //get the length of the valid prefix
          //then go against the orderNumber and check to see if
          //the next character is a digit
          int prefixLength = prefix.length();
          char n = orderNumber.charAt(prefixLength);
          Character nextCharacter = new Character(n);

          //if prefix starts with "F"
          //Checking for "F?" is done with the array of prefixes check above
          //Add new "F?" prefixes in the global parms configuration 
          //next character is an alpha character
          if(prefix.startsWith("F"))
          {
            if(nextCharacter.isDigit(n))
            {
              throw new OrderValidationException( OrderGatherConstants.INVALID_ORDER_NUMBER_ERROR + orderNumber);
            }
            
            prefixLength++;
            n = orderNumber.charAt(prefixLength);
            nextCharacter = new Character(n);
            if(!nextCharacter.isDigit(n))
            {
              throw new OrderValidationException(OrderGatherConstants.INVALID_ORDER_NUMBER_ERROR+ orderNumber);
            }
          }
          /*
          else
          {
            //prefix equals "AN", then the next character can be
            //either alpha or numeric
            if(!prefix.equals("AN") && !nextCharacter.isDigit(n))
            {
              throw new OrderValidationException("The following Order Number is Invalid: " + orderNumber);
            }
          }
          */
          success = true;
          break;
        }//end inner for loop
        
      }//end outer for loop
 
		// #17863 - For Partner orders, the order number starts with partner
		// origins which are defined in partner mapping table in PTN_PI schema.
		if (!success) {
			List<PartnerMappingVO> partners = new PartnerUtility().getAllPartnersMappingInfo();
			if (partners != null) {
				for (PartnerMappingVO partnerMappingVO : partners) {
					if (orderNumber.startsWith(partnerMappingVO.getFtdOriginMapping())) {
						success = true;
						break;
					}
				}
			}
		}
      
      // For mercent orders,the order number can start with channel origins i.e., MA/ME defined in channel mapping table in mercent schema.
      if(!success){
    	  MercentOrderPrefixes mrcnt = new MercentOrderPrefixes(conn);
    	  List<MercentChannelMappingVO> channelMappingList = mrcnt.getMercentChannelDetails();
    	  for(MercentChannelMappingVO vo : channelMappingList) {
    		  if(orderNumber.startsWith(vo.getFtdOriginMapping())){
    			  success = true;
    			  break;
    		  }
    	  }
      }
      
      //check to see if an order number passed the pre-validation 
      if(!success)
        throw new OrderValidationException(OrderGatherConstants.INVALID_PREFIX_ERROR + orderNumber);
     
   }
    return VALID_ORDER;
    
  }

  /**
   * 
   * @param order OrderVO contains entire order
   * @return boolean true if test credit card number was found or
   * false, if test credit card number wasn't found.
   */
  private boolean checkForTestCC(OrderVO order) throws Exception
  {
     //check for test credit card number and test
     //email address.  If these two pieces of
     //information are found, then throHashMap orderCreditCard = null;
     Collection payments = order.getPayments();            
     Collection creditCards = null;
     PaymentsVO payment = null;
     CreditCardsVO creditCard = null;
     Iterator iterator = null;
     Iterator paymentsIterator = null;
     Iterator creditCardsIterator = null;
     boolean isTestCC = false;
     String ccNumber = null;
                     
     iterator = payments.iterator();
                                
    for (int i = 0; iterator.hasNext(); i++) 
    {
        payment = (PaymentsVO)iterator.next();

        creditCards = payment.getCreditCards();
        if(creditCards != null)
        {
            creditCardsIterator = creditCards.iterator();

            for (int x = 0; creditCardsIterator.hasNext(); x++) 
            {
                creditCard = (CreditCardsVO)creditCardsIterator.next();
                ccNumber = creditCard.getCCNumber();
                if(ccNumber != null && ccNumber.equals(configUtil.getProperty(OrderGatherConstants.GATHERER_CONFIG_FILE, "test_cc_number")))
                {
                    isTestCC = true;
                }
            }
        }//end if
     }//end for loop
     return isTestCC;
  }

  /**
   * 
   * @param order OrderVO contains entire order
   * @return boolean true if test email address was found, or false
   * if test email address wasn't found
   */
  private boolean checkForTestEmailAddress(OrderVO order) throws Exception
  {
    boolean isTestEmailAddress = false;

    Collection buyers = order.getBuyer();            
    Collection buyerEmails = null;
    BuyerVO buyer = null;
    BuyerEmailsVO buyerEmail = null;
    Iterator iterator = null;
    Iterator buyerEmailsIterator = null;
                    
    iterator = buyers.iterator();
                                
    for (int i = 0; iterator.hasNext(); i++) 
    {
        buyer = (BuyerVO)iterator.next();

        buyerEmails = buyer.getBuyerEmails();
        buyerEmailsIterator = buyerEmails.iterator();

        for (int x = 0; buyerEmailsIterator.hasNext(); x++) 
        {
            buyerEmail = (BuyerEmailsVO)buyerEmailsIterator.next();
            if(buyerEmail.getEmail().equals(configUtil.getProperty(OrderGatherConstants.GATHERER_CONFIG_FILE, "test_email_address")))
              isTestEmailAddress = true;
        }
    }
    
    return isTestEmailAddress;
    
  }

  private boolean isCalyxOrder(OrderVO order) throws Exception {
      boolean isCalyxOrderCheck = false;

      Map map = configUtil.getProperties(OrderGatherConstants.GATHERER_CONFIG_FILE);
      Iterator it = map.keySet().iterator();
      logger.info("origin: " + order.getOrderOrigin());

      while(it.hasNext()) {
         String key = (String)it.next();
         String value = (String)map.get(key);

         if(key.endsWith("_gcp")) {
             if (value != null && value.equalsIgnoreCase(order.getOrderOrigin())) {
                 logger.info("Calyx order: " + value);
                 isCalyxOrderCheck = true;
                 break;
             }
         }
      }

      return isCalyxOrderCheck;
  }
}  