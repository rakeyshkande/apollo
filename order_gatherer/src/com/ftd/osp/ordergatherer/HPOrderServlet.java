package com.ftd.osp.ordergatherer;

import com.ftd.osp.utilities.HttpServletResponseUtil;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet which executes the HPOrderSender which sends
 * the xml received in the transmission to the HP.
 * 
 * @author Rose Lazuk
 */

public class HPOrderServlet extends HttpServlet 
{
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    private Logger logger;
    private static final int ERROR_CODE = 600;
    private static final int SUCCESS_CODE = 200;
    
    public void init(ServletConfig config) throws ServletException
    {
       logger = new Logger("com.ftd.osp.ordergatherer.HPOrderServlet");
       super.init(config);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String orderXMLString = request.getParameter("Order");
        File xslFile = null;
        boolean success = true;
        
        try
        {
            xslFile = new File(getServletContext().getRealPath("//novatorToHp.xsl"));       

            HPOrderSender orderSender = new HPOrderSender();
            boolean orderSent = orderSender.sendString(orderXMLString, xslFile);
            if(!orderSent)
            {
              logger.error("Error sending Order to the HP");
              success = false;
              returnError(response);
            }
        }
        catch(Exception e)
        {
            logger.error(e.toString());
            returnError(response);
        }

        if(success)
          returnSuccess(response);
    }

     /**
   *
   * Set and return success code
   *
   * @param response HttpServletResponse
   */
  private void returnSuccess(HttpServletResponse response)
  {
    //200

    try
    {
        HttpServletResponseUtil.sendError(response, SUCCESS_CODE, "Success");
    }
    catch(Exception ex)
    {
      logger.error(ex.toString());
    }
  }

  /**
   *
   * Set and return error code
   *
   * @param response HttpServletResponse
   */
  private void returnError(HttpServletResponse response)
  {
    //600

    try
    {
      HttpServletResponseUtil.sendError(response, ERROR_CODE, "Error");
    }
    catch(Exception ex)
    {
        logger.error("XML file transmission failed: " + ex.toString());
    }
  }
}