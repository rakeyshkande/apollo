package com.ftd.osp.ordergatherer;

import com.ftd.osp.utilities.plugins.Logger;


import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Iterator;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet which executes the HPOrderSender which sends the archived transmission
 * to the HP.
 * 
 * @author Brian Munter
 */

public class HPArchiveServlet extends HttpServlet 
{
    private Logger logger;
        
    public void init(ServletConfig config) throws ServletException
    {
        logger = new Logger("com.ftd.osp.ordergatherer.HPArchiveServlet");
        super.init(config);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    { 
      
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String CONTENT_TYPE = "text/html";
        String masterOrderNumber = null;
        String transType = null;
        Collection hpStrings = null;
        String hpString = null;
        File xslFile = new File(getServletContext().getRealPath("//novatorToHp.xsl"));;
        OrderGatherUtils ogUtils = new OrderGatherUtils();
        PrintWriter out = response.getWriter();
        
        try
        {
            masterOrderNumber = request.getParameter("masterOrderNumber");
            transType = request.getParameter("transType");

            if(masterOrderNumber == null || masterOrderNumber.equals(""))
            {
              out.println("PLEASE SUPPY VALID 'masterOrderNumber' PARAMETER TO YOUR QUERY"); 
              out.close();
              return;
            }
            if(transType == null || transType.equals(""))
            {
              out.println("PLEASE SUPPY VALID 'transType' PARAMETER TO YOUR QUERY"); 
              out.close();
              return;
            }
            
            if(transType.equals(OrderGatherConstants.XML_STRING))
            {
              hpString = ogUtils.getXMLFromArchive(masterOrderNumber);
              CONTENT_TYPE = "text/xml";
              response.setContentType(CONTENT_TYPE);
              out.println(hpString);
            }
            else if(transType.equals(OrderGatherConstants.HP_STRING))
            {
              hpStrings = ogUtils.getHPStringList(masterOrderNumber, transType, xslFile);
              for(Iterator iter = hpStrings.iterator(); iter.hasNext();)
              {  
                hpString = (String) iter.next();
                hpString = hpString + System.getProperty("line.separator");
                out.println(hpString);
              }
            }
            else
            {
              out.println("PLEASE SUPPY VALID 'transType' PARAMETER TO YOUR QUERY"); 
            }
            out.close();
        }
        catch(Exception e)
        {
            out.println("NO SUCH ORDER OR SYSTEM ERROR"); 
            logger.error(e.toString());
            out.close();
        }
    }
}