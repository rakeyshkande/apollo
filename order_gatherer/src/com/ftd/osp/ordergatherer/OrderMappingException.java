package com.ftd.osp.ordergatherer;

import com.ftd.osp.utilities.plugins.Logger;

/**
 * This class is used to log an Order Mapping Exception. 
 *
 * @author Rose Lazuk
 */

public class OrderMappingException extends Exception
{

  private Logger logger;

  /**
   * Generic Order Mapping Exception Message 
   */
  public OrderMappingException()
  {
    super();
  }

  /**
   * Detailed Order Mapping Exception Message
   * 
   * @param orderMappingExceptionMsg String
   */
   public OrderMappingException(String orderMappingExceptionMsg)
  {
    super(orderMappingExceptionMsg);
  }

  /**
   * Detailed Order Mapping Exception Message
   * 
   * @param orderMappingExceptionMsg Exception
   */
   public OrderMappingException(Exception orderMappingExceptionMsg)
  {
    super(orderMappingExceptionMsg.toString());
  }
}