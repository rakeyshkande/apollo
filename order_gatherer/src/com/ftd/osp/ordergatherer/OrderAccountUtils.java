package com.ftd.osp.ordergatherer;


import com.ftd.osp.ordergatherer.dao.OrderGathererDAO;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;

import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.osp.utilities.vo.ProductVO;

import java.sql.Connection;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.List;


/**
 * Provides utility methods for working with Order Accounts.
 */
public class OrderAccountUtils
{
  private Logger logger = new Logger(OrderAccountUtils.class.getName());

  

  public OrderAccountUtils()
  {
  }

  

  /**
   * Checks to see if the buyer is signed in with a valid email address
   * @param orderVO
   * @return
   */
  private boolean checkAccountCanBeCreated(OrderVO orderVO)
  {
    if (!orderVO.isBuyerSignedIn())
    {
      logger.debug("Buyer is not signed in, no account to create");
      return false;
    }

    if (!validateBuyerEmail(orderVO.getBuyerEmailAddress()))
    {
      logger.debug("Buyer email address is not valid, skipping account creation.");
      return false;
    }

    return true;
  }

  /**
   * The Service Start Date is the Date of the Order. If this value is not specified/invalid, use the current system date
   *
   * @param orderVO
   * @return
   */
  private Calendar getServiceStartDate(OrderVO orderVO)
  {
    Calendar orderCalendar = Calendar.getInstance();

    try
    {
      Date orderDate = orderVO.getOrderDateTime();
      orderCalendar.setTime(orderDate);
    }
    catch (Exception e)
    {
      logger.warn("Exception parsing Order Date, using current date: " + orderVO.getOrderDate() + ":" + e.getMessage(), e);
    }

    // Set to Midnight of the Order/Current Date
    orderCalendar.set(Calendar.MILLISECOND, 00);    
    orderCalendar.set(Calendar.HOUR, 00);
    orderCalendar.set(Calendar.MINUTE, 00);
    orderCalendar.set(Calendar.SECOND, 00);

    return orderCalendar;
  }


  /**
   * Returns the User ID for db logging. If the CSR ID from the order is null or empty
   * returns the OrderGatherConstants.USER_ID value
   * @param csrId The CSR Id from the Order.
   * @return
   */
  private String getUserId(String csrId)
  {
    if (csrId == null || csrId.trim().length() == 0)
    {
      return OrderGatherConstants.USER_ID;
    }

    return csrId;
  }

  /**
   * Checks for a buyer email to be valid. This utilizes a simple/lenient email check
   * of *@* (I.E. Validate there is text in the email and at least 1 @ symbol with characters preceding
   * and following it)
   *
   * This is a lenient email check for minimal email validation only.
   * This should always pass as we only invoke it after checking buyer signed in, which would be false
   * on a bad email anyway.
   *
   * @param emailAddress
   * @return
   */
  private boolean validateBuyerEmail(String emailAddress)
  {
    // Check for empty email
    if (emailAddress == null || emailAddress.trim().length() == 0)
    {
      return false;
    }

    // Check that there is *@*
    String[] parts = emailAddress.trim().split("@", 2);
    if (parts.length < 2)
    {
      return false;
    }

    if (parts[0].trim().length() > 0 && parts[1].trim().length() > 0)
    {
      return true;
    }

    return false;
  }

  /**
   * Checks the product type and subtype for free shipping
   * @param productType
   * @param productSubType
   * @return
   */
  private boolean isProductTypeFreeShipping(String productType, String productSubType)
  {
    if ("SERVICES".equalsIgnoreCase(productType) && "FREESHIP".equalsIgnoreCase(productSubType))
    {
      return true;
    }

    return false;
  }


  
}
