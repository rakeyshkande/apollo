package com.ftd.osp.ordergatherer;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.naming.InitialContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.ftdutilities.GeneralConstants;
import com.ftd.osp.ordergatherer.dao.OrderGathererDAO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.HttpServletResponseUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.stats.OrderDetailStates;
import com.ftd.osp.utilities.stats.StatsUtil;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.JAXPUtil;


/**
 * This class receives and processes the xml transmission
 * which contains the order.
 *
 * @author Rose Lazuk
 */
public class OrderGatherer extends HttpServlet
{
  private Logger logger;
  private static final String ES_1002 = "ES1002";

  public void init(ServletConfig config) throws ServletException
  {
    logger = new Logger("com.ftd.osp.ordergatherer.OrderGatherer");
    super.init(config);
  }

  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    PrintWriter out = response.getWriter();
    out.println("The Order Gatherer!");
    out.close();
    
  }

/**
 *
 *  DoPost method receives and process the xml file
 *
 * @param request HttpServletRequest
 * @param response HttpServletResponse
 * @exception ServletException
 * @exception IOException
 */
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
  {
    logger.debug("BEGIN TRANSACTION");
    Long startPostTime = System.currentTimeMillis();
    
    String orderXML = null;
    OrderVO order = null;
    OrderPreprocessor orderPreprocessor = null;
    OrderDBMapper orderDBMapper = null;
    OrderValidator orderValidator = null;
    OrderArchive orderArchive = null;
    Connection connection = null;
    OrderGatherUtils ogUtils = new OrderGatherUtils();

    orderXML = request.getParameter("Order"); 

    //check to see if anything was received in the transmission
    if(orderXML == null || orderXML.equals(""))
    {
      this.returnError(response, OrderGatherConstants.EMPTY_XML_ERROR);
      return;
    }
    
    try 
    {
      orderXML = ogUtils.decryptOrderXML(orderXML);
    } catch (Exception e)
    {
      this.returnError(response, e.toString());
      return;      
    }
    
    orderXML = truncateAribaOversizedFields(orderXML,ogUtils);
    
	
    //if sendOrderToHP flag is true then send order directly to the hp
    if(this.sendToHP())
    {
        RequestDispatcher rd = getServletContext().getRequestDispatcher("/OrderSenderServlet");
        rd.forward(request, response);
        return;
    }

    //processing starts when sending the order through the phase 2 system
    /******************************************************************************/
    InitialContext context = null;
    UserTransaction userTransaction = null;
    
    try
    {
        int transactionTimeout = Integer.parseInt((String)ConfigurationUtil.getInstance().getProperty(OrderGatherConstants.GATHERER_CONFIG_FILE, "transaction_timeout_in_seconds"));
        
        // get the initial context
        context = new InitialContext();

        // Retrieve the UserTransaction object
        String jndi_usertransaction_entry = ConfigurationUtil.getInstance().getProperty(OrderGatherConstants.GATHERER_CONFIG_FILE, "jndi_usertransaction_entry");
        userTransaction = (UserTransaction)  context.lookup(jndi_usertransaction_entry);

        logger.debug("User transaction obtained");
        userTransaction.setTransactionTimeout(transactionTimeout);
        logger.debug("Transaction timeout set at " + transactionTimeout + " seconds");


        // Start the transaction with the begin method
        userTransaction.begin();
        
        // enlist the database connection
        connection =  DataSourceUtil.getInstance().getConnection("ORDER SCRUB");

        orderPreprocessor = new OrderPreprocessor(orderXML, connection);
        order = orderPreprocessor.createOrder();
        orderValidator = new OrderValidator();
        int status = orderValidator.validate(order, connection);

        if (status == OrderValidator.TEST_ORDER && !order.isOeOrder())
        {
            assignTestOrderStatus(order);
        }

        if (!order.isOeOrder())
        {
            order.setCsrId(OrderGatherConstants.USER_ID);
        }
        
        //set the avs_performed_origin flag
        setAVSPerformedOrigin(order, connection);

        orderDBMapper = new OrderDBMapper(connection);
        orderDBMapper.map(order);

        if (status == OrderValidator.VALID_ORDER) {
            recordStats(order, OrderDetailStates.NEW, connection);            
        }
                
        orderArchive = new OrderArchive();
        orderArchive.archive(orderXML, order.getMasterOrderNumber(), connection);
        
        // Configure the Account for the Order
      
        
        MessageToken token = null;


        if (status == OrderValidator.TEST_ORDER && !order.isOeOrder()) 
        {
            // TEST and not JOE/M# order
            OrderDetailsVO orderDetails = null;
            if(order.getOrderDetail() != null && order.getOrderDetail().size() > 0)
            {
                for(int i = 0; i < order.getOrderDetail().size(); i++)
                {
                    orderDetails = (OrderDetailsVO) order.getOrderDetail().get(i);
                    
                    token = new MessageToken();
                    token.setStatus(OrderGatherConstants.JMS_MSG_STAMP_TEST_0RDER);
                    token.setMessage(order.getGUID() + "/" + orderDetails.getLineNumber());

                    this.dispatchOrder(context, token);
                }
            }
        }
        // Not Test non-JOE/M# order
        else if(status == OrderValidator.VALID_ORDER)
        {
            token = new MessageToken();
            //check to see if we need to bypass QMS Validation.  If we do then send directly
            //to Order Validator. If there are any unexpected errors send to QMS and send a system message.            
            try
            {
                // If AVS was already performed for "web" orders (i.e. not OeOrders) they can also skip QMS
                boolean avsPerformedOnNotOeOrder = wasAVSPerformed(order);

                OrderGathererDAO ogDAO = new OrderGathererDAO(connection);
                if(order.isOeOrder() || avsPerformedOnNotOeOrder)
                {
                    token.setStatus(ES_1002); 
                }
                else // send to QMS
                {
                    token.setStatus(OrderGatherConstants.JMS_MSG_STAMP);
                }
            }
            catch (Exception e)
            {
                token.setStatus(OrderGatherConstants.JMS_MSG_STAMP);
                ogUtils.sendMessage("Unexpected error encountered: " + e.getMessage() + ". Routing through QMS. Master Order number=" + order.getMasterOrderNumber());
            }
            token.setMessage(order.getGUID());

          this.dispatchOrder(context, token);
        }

        //Assuming everything went well, commit the transaction.
        userTransaction.commit();

        // return the success code
        this.returnSuccess(response);

        logger.debug("User transaction committed");

        logger.debug("Checking view_order");
        
/******************************************************************************/
      }
      catch(TestOrderException toe)
      {
        logger.error(toe.toString());
        ogUtils.sendMessage("Test order received.Master Order number=" + order.getMasterOrderNumber());
        rollback(userTransaction);
        returnSuccess(response);
      }
      catch(DuplicateOrderException doe)
      {
        logger.error(OrderGatherConstants.DUPLICATE_RECORD_ERROR_MESSAGE + "Master Order Number=" + order.getMasterOrderNumber());
        logger.error(doe.toString());
        ogUtils.sendMessage(OrderGatherConstants.DUPLICATE_RECORD_ERROR_MESSAGE + "Master Order Number=" + order.getMasterOrderNumber());
        rollback(userTransaction);
        returnSuccess(response);
      }
      catch(Throwable t)
      {
        logger.error(t.toString());
        rollback(userTransaction); 
        returnError(response, t.toString());
      }
      finally 
      {
        try  
        {
          // close application connections
          if (connection != null && !connection.isClosed())  
          {
            connection.close();
          }
          // close initial context
          if(context != null)
          {
            context.close();
          }
        } 
        catch (Exception ex)  
        {
          logger.error(ex);
        }
      }
    
    logger.debug("Total time to process: " + (System.currentTimeMillis() - startPostTime));
    logger.debug("END TRANSACTION");
  }

	

	

private String truncateAribaOversizedFields(String orderXML,OrderGatherUtils ogUtils){
	
	    String masterOrder = "";
	    String Fieldstruncated = "";
	    
	    try {
			Document config;
			config = DOMUtil.getDocument(orderXML);
			NodeList origin=config.getElementsByTagName("origin");
			
			String originText= origin.item(0).getTextContent();
			if("ARI".equalsIgnoreCase(originText)){
				Map<String,String> truncateFields = ogUtils.loadAribaFieldtruncateMap();
				for(String key :truncateFields.keySet()){
					NodeList tagtrunc =config.getElementsByTagName(key);
					for(int i=0;i<tagtrunc.getLength();i++){
						String text = tagtrunc.item(i).getTextContent();
						int value = Integer.parseInt(truncateFields.get(key)) ;
						if(text.length()>value){
							text = text.substring(0,value);
						    tagtrunc.item(i).setTextContent(text);
						    Fieldstruncated+= key+"\n";
						}
						if(key.equalsIgnoreCase("master-order-number")){
								    masterOrder = text;
						}
					}
				}
				orderXML = JAXPUtil.toString(config);
			}
		}catch (Exception exception)
	    {
	    	logger.error("Erro while truncating fields :"+exception.getMessage());
	    	ogUtils.sendMessage("Getting error while truncating ariba oversized fields for the received Order :" + masterOrder); 
	    }
	    if(!"".equalsIgnoreCase(Fieldstruncated) && Fieldstruncated.length()>0){
	    	logger.info("Below list of oversized fields truncated : \n"+Fieldstruncated);
	    	ogUtils.sendMessage("Below list of oversized fields truncated for the ariba Order: "+ masterOrder +"\n "+Fieldstruncated); 
	    }
	return orderXML;
}

/**
   *  set the AVS Performed Origin based on of this is an oeOrder or a website order
   * @param order
   */
  private void setAVSPerformedOrigin(OrderVO order, Connection conn) {
	try {
		String avsPerformedOrigin = "";

		if (order != null && order.isOeOrder()) {
			avsPerformedOrigin = GeneralConstants.AVS_PERFORMED_APOLLO;
		} else {

			avsPerformedOrigin = GeneralConstants.AVS_PERFORMED_WEBSITE;
		}
		
		if (order.getOrderDetail() != null) {
			for (OrderDetailsVO od : (List<OrderDetailsVO>)order.getOrderDetail()) {
				if (od.getAvsAddress() != null) {
					od.getAvsAddress().setAvsPerformedOrigin(avsPerformedOrigin);
				}
			}
		}
	} catch (Throwable e) {
		try {
			//send a system message
			SystemMessengerVO message = new SystemMessengerVO();
			message.setMessage("setAVSPerformedOrigin failed for master order# " + order.getMasterOrderNumber() + ". " + e.getMessage());
			message.setSource("OrderGatherer");
			message.setSubject("setAVSPerformedOrigin failed (" +order.getMasterOrderNumber() +")");
			message.setLevel(SystemMessengerVO.LEVEL_DEBUG);
			SystemMessenger.getInstance().send(message, conn);
			logger.error("Could not set the AVS Performed Origin because of " + e.getMessage());
			e.printStackTrace();
			//try to default avs_performed_origin to Apollo
			if (order.getOrderDetail() != null) {
				for (OrderDetailsVO od : (List<OrderDetailsVO>)order.getOrderDetail()) {
					if (od.getAvsAddress() != null) {
						od.getAvsAddress().setAvsPerformedOrigin(GeneralConstants.AVS_PERFORMED_APOLLO);
					}
				}
			}
		} catch (Throwable ee) { 
			/** we did the best we could **/
			logger.error(ee.getMessage() + " while trying to recover from the previous error");
		}
	}
	
  }

// Determine if address verification was performed on an order - requires that all order items had verification performed
  // OeOrders will return false since they are not non-OE orders.
  private boolean wasAVSPerformed(OrderVO order)
  {
      boolean avsPerformedOnNotOeOrder = true;
      if (!order.isOeOrder())
      {
          if(order.getOrderDetail() != null && order.getOrderDetail().size() > 0)
          {
              OrderDetailsVO orderDetails = null;
              // set flag based on intersection of all addressed having AVS performed
              for(int i = 0; i < order.getOrderDetail().size() && avsPerformedOnNotOeOrder; i++)
              {
                  orderDetails = (OrderDetailsVO) order.getOrderDetail().get(i);
                  if (orderDetails.getAvsAddress() != null)
                  {
                      avsPerformedOnNotOeOrder = avsPerformedOnNotOeOrder && (GeneralConstants.YES.equals(orderDetails.getAvsAddress().getAvsPerformed()));
                  }
                  else
                  {
                      avsPerformedOnNotOeOrder = false;
                  }
              }
          }  
      }
      else
      {
          avsPerformedOnNotOeOrder = false;
      }
      return avsPerformedOnNotOeOrder;
  }

  /**
   *
   * Set and return success code
   *
   * @param response HttpServletResponse
   */
  private void returnSuccess(HttpServletResponse response)
  {
    try
    {
        HttpServletResponseUtil.sendError(response, OrderGatherConstants.SUCCESS_CODE, "Success");
    }
    catch(Exception ex)
    {
      logger.error(ex.toString());
    }
  }

  /**
   *
   * Set and return error code
   *
   * @param response HttpServletResponse
   */
  private void returnError(HttpServletResponse response, String errorReason)
  {
    try
    {
        HttpServletResponseUtil.sendError(response, OrderGatherConstants.ERROR_CODE, errorReason);
    }
    catch(Exception ex)
    {
        logger.error("XML file transmission failed: " + ex.toString());
    }
  }

  /**
   *
   * Get value from frp global parms to check whether
   * or not to send an order directly to the HP or to send an
   * order through scrub
   *
   * @param connection Connection to the database
   * @return boolean true if order is to be sent directly
   * to the hp, or false if the order is to go through
   * scrub
   */
  private boolean sendToHP()
  {
      try 
      {
          Map map = ConfigurationUtil.getInstance().getProperties(OrderGatherConstants.GATHERER_CONFIG_FILE);
          String sendParm = (String)map.get("send_to_hp");

          if(sendParm == null || sendParm.equals(""))
          {
             logger.info("'send_to_hp' parameter not set in gathererConfig. Using default: [false]");
             return false;
          }

          if(sendParm.equals("true"))
          {
            return true;
          }
      } 
      catch (Exception ex) 
      {
        logger.info("'send_to_hp' parameter not set in gathererConfig. Using default: [false]");
        logger.error(ex);
      }
            
      return false;
  }

    private void recordStats(OrderVO order, int state, Connection connection) throws Exception
    {
        OrderDetailsVO item = null;
        Iterator it = order.getOrderDetail().iterator();
        StatsUtil statsUtil = new StatsUtil();
        while(it.hasNext())
        {
            item = (OrderDetailsVO)it.next();
            statsUtil.setItem(item);
            statsUtil.setOrder(order);
            statsUtil.setState(state);
            statsUtil.insert(connection);
        }
    }

  private void rollback(UserTransaction userTransaction)
  {
      try  
      {
          if (userTransaction != null)  
          {
            // rollback the user transaction
            userTransaction.rollback();

            logger.debug("User transaction rolled back");
          }
      } 
      catch (Exception ex)  
      {
          logger.error(ex);
      } 
  }

  /**
   * 
   */
   private void assignTestOrderStatus(OrderVO order)
   {
     order.setStatus(OrderGatherConstants.TEST_0RDER_STATUS);
     OrderDetailsVO orderDetailsVO = null;
     
     for (Iterator iter = order.getOrderDetail().iterator(); iter.hasNext(); ) 
     {
        orderDetailsVO = (OrderDetailsVO) iter.next();
        orderDetailsVO.setStatus(OrderGatherConstants.TEST_0RDER_STATUS);
     }
     
   }

   private void dispatchOrder(InitialContext context, MessageToken token) throws Exception
   {
        logger.debug("MESSAGE : " + (String)token.getMessage());
        logger.debug("STATUS : " + (String)token.getStatus());
   
        if(OrderGatherConstants.SEND_JMS_MESSAGE)
        {
            logger.debug("DISPATCHING MESSAGE FROM ORDER GATHERER " + (String)token.getMessage());
            Dispatcher dispatcher = Dispatcher.getInstance();

            // enlist the JMS transaction, with the other application transaction
           dispatcher.dispatch(context, token); 
        }
    }
  
    
    
}