package com.ftd.osp.ordergatherer;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.*;
import com.ftd.osp.utilities.order.ScrubMapperDAO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.sql.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;

import org.w3c.dom.NodeList;
import org.w3c.dom.Document;


/**
 * This class loads the xml received in the transmission and
 * transforms it into a standard HP string and then sends it
 * to the HP socket listener.
 *
 * @author Rose Lazuk
 */

public class HPOrderSender
{
    private Logger logger = null;

    /**
     * The Constructor for HPOrderSender
     */
    public HPOrderSender() throws Exception
    {
        logger = new Logger("com.ftd.osp.ordergatherer.HPOrderSender");
    }

    /**
     * Method which takes the xml received in the transmission and
     * converts it to a hp string.
     * 
     * @param orderXMLString the xml order received in the transmission
     * @param xslFile the name of the xsl file
     * @return boolean return true if order was sent to
     * hp successfully, else return false
     * @exception throws exception if there is an error
     */
    public boolean sendString(String orderXMLString, File xslFile) throws Exception
    {
        List updatedTransmissions = new ArrayList();
        List transmissions = null;
        String updatedTransmission = null;
        OrderGatherUtils ogUtils = new OrderGatherUtils();

        try
        {
           DocumentBuilder db = (DocumentBuilder)DOMUtil.getDocumentBuilder();
           Document orderXMLDocument = DOMUtil.getDocument(db, orderXMLString);

           //get the total number of items associated with this order
           //a '/' plus this number needs to be appended to the master-order-number
           //this format is needed for the hp
           NodeList nl = DOMUtil.selectNodes(orderXMLDocument, "/order/items/item");
           int itemNumbers = nl.getLength();
           ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
           String transType = configUtil.getProperty(OrderGatherConstants.GATHERER_CONFIG_FILE, "trans_type_hp");
           transmissions = ogUtils.transformTransmission(orderXMLDocument, transType, xslFile, itemNumbers);

           //before an order gets sent to the hp, we need to pre-append
           //the total length of the file followed by two ~~'s
           String fileLength = "";
           for(Iterator iter = transmissions.iterator(); iter.hasNext();){
             updatedTransmission = (String)iter.next();
             //get the length of the updated xml transmission
             int updatedTransmissionLength = updatedTransmission.trim().length();

             //get the length of the number set to updatedTransmissionLength
             //and add 2 ~~'s to that length
             fileLength = fileLength.valueOf(updatedTransmissionLength) + "~~";
             char[] fileLengthArray = fileLength.toCharArray();

             //add the updatedTransmissionLength to the
             //fileLengthArray size to come up with the total
             //length of the updatedTransmission
             int transLength = fileLengthArray.length + updatedTransmissionLength;

             //pre-append the total length of the transmission to the
             //order followed by two ~~'s
             updatedTransmission = transLength + "~~" + updatedTransmission;
             updatedTransmissions.add(updatedTransmission);
        }

        }
        catch(Exception e)
        {
            logger.error(e);
            throw new Exception(e.toString());
        }

            // Send modified transmission to HP
            if(!this.sendToHPSocket(updatedTransmissions))
            {
                return false;
            }

        return true;
    }

 
    /**
     * Sends the HP string to the HP socket listener.
     * @param hpstring the hp string
     * @exception Exception
     * @return a boolean indicating the status of the transmission
     */
    private boolean sendToHPSocket(List hpStrings) throws Exception
    {
        boolean result = false;

        if ( hpStrings == null)
        {
            logger.error("HP String is null");
            return result;
        }

        String tmpResultStr = "";
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        String hpIpAddress = configUtil.getProperty(OrderGatherConstants.GATHERER_CONFIG_FILE, "hpIpAddress");
        int port = Integer.valueOf(configUtil.getFrpGlobalParm(OrderGatherConstants.GATHERER_CONFIG_CONTEXT, "hpPort")).intValue();

        SocketClient client = null;
        logger.debug("Connecting to " + hpIpAddress + " at port " + port);

        try
        {
            client = new SocketClient(hpIpAddress, port);
            String hpString = null;

            //loop through the list of hp strings and send them
            //one order item at a time
            for(Iterator iter = hpStrings.iterator(); iter.hasNext();){
              hpString = (String) iter.next();
              logger.debug("Sending via socket...");
              logger.debug(hpString);

              client.open();
              tmpResultStr = client.send(hpString);
              client.close();
              if(tmpResultStr.trim().endsWith(OrderGatherConstants.HP_SUCCESS_STRING))
              {
                  result = true;
              }
              
              logger.debug("Send Status :: " + tmpResultStr);
              logger.debug("...Sending complete");
		    }
        }
        catch ( Exception e )
        {
            logger.error(e);
        }
        finally
        {
            if ( client != null )
            {
                try
                {
                    client.close();
                }
                catch ( Exception e )
                {
                    logger.error(e);
                }
            }
        }

        return result;
    }

}