package com.ftd.osp.ordergatherer;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

/**
 * This class takes the orginal xml that is sent over
 * and stores it in the database.
 *
 * @author Rose Lazuk
 */

public class OrderArchive 
{
  
  private Logger logger;

  public OrderArchive() 
  {
    logger = new Logger("com.ftd.osp.ordergatherer.OrderArchive");
  }

  /**
   * Archive the order to the database
   * 
   * @param orderXML String
   * @param masterOrderNumber String
   */
  public void archive(String orderXML, String masterOrderNumber, Connection connection) 
  {
    try {
       DataRequest dataRequest = new DataRequest();
       HashMap inParms = new HashMap();
       
       inParms.put("IN_MASTER_ORDER_NUMBER", masterOrderNumber);
       inParms.put("IN_ORDER_XML",orderXML);
       
       dataRequest.setConnection(connection);
       dataRequest.setInputParams(inParms);
       dataRequest.setStatementID("SCRUB.INSERT_ORDER_XML_ARCHIVE");
       
       DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
       Map outputs = (Map)dataAccessUtil.execute(dataRequest);
       
       String status = (String)outputs.get("RegisterOutParameterStatus");
       
       if(status.equalsIgnoreCase("N")) {
          String message = (String)outputs.get("RegisterOutParameterMessage");
          logger.error("OrderArchive: Error inserting order xml into DB for order:" + masterOrderNumber 
            + ". Message:" + message);
       } else{
          logger.info("OrderArchive: Success archiving order xml for order:" + masterOrderNumber);
       }
    }catch(Exception e){
       logger.error(e);
       logger.error("OrderArchive: Exception while attempting to archive order xml for order:" + masterOrderNumber);
    } 
  }
}