package com.ftd.osp.ordergatherer;

/**
 * This class is used to log an Order Archive Exception. 
 *
 * @author Rose Lazuk
 */

public class OrderArchiveException extends Exception 
{
  /**
   * General Order Archive Exception Message
   */ 
  public OrderArchiveException()
  {
   
  }

   /**
   * Detailed Order Archive Exception Message
   * 
   * @param orderArchiveExceptionMsg String
   */
   public OrderArchiveException(String orderArchiveExceptionMsg)
  {
    super(orderArchiveExceptionMsg);
  }

  /**
   * Detailed Order Archive Exception Message
   * 
   * @param orderArchiveMessage Excpetion
   */
  public OrderArchiveException(Exception orderArchiveMessage)
  {
    super(orderArchiveMessage.toString());
  }

  
}