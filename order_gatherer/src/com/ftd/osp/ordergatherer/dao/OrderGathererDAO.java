package com.ftd.osp.ordergatherer.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.ProductVO;

/**
 * OrderGathererDAO
 *
 * 
 */


public class OrderGathererDAO
{
    private Connection connection;
    private static Logger logger  = new Logger("com.ftd.ordergatherer.dao.OrderGathererDAO");
    private static final String NAME_DELIMITER = ",";

  /*
   * Constructor
   * @param connection Connection
   */
  public OrderGathererDAO(Connection connection)
  {
    this.connection = connection;

  }

  /**
   * Method to retrieve the customer's credit card from the pop schema
   * @param masterOrderNumber - master order number
   * @return String credit card number
   *
   */

   public String getCSPCreditCardNumber(String masterOrderNumber) throws Exception
   {
        Map paramMap = new HashMap();     

        boolean exists = false;

        paramMap.put("IN_MASTER_ORDER_NUMBER",masterOrderNumber);

        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("GET_CSP_CC_NUMBER");
        dataRequest.setInputParams(paramMap);

           
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        CachedResultSet rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);       
        String ccNumber = null;
        if(rs != null && rs.next())
        {
          ccNumber = (String)rs.getObject("OUT_CC_NUMBER");
        }   

        return ccNumber;
   }
 
    /**
     * Method to retrieve the redemption rate by source code
     * @param sourceCode
     * @return String credit card number
     *
     */
     public BigDecimal getRdmptnRateBySource(String sourceCode) throws Exception
     {
          Map paramMap = new HashMap();     

          paramMap.put("IN_SOURCE_CODE",sourceCode);

          DataRequest dataRequest = new DataRequest();
          dataRequest.setConnection(connection);
          dataRequest.setStatementID("GET_RDMPTN_RATE_BY_SOURCE");
          dataRequest.setInputParams(paramMap);

             
          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
          CachedResultSet rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);       
          BigDecimal rdmptnRate = null;
          if(rs != null && rs.next())
          {
            rdmptnRate = (BigDecimal)rs.getObject("MP_REDEMPTION_RATE_AMT");
          }   

          return rdmptnRate;
     }    
     
     
    /**
     * Returns the Company Id for a Source Code
     * @param sourceCode
     * @return The Company Id or Null
     */
    public String getCompanyIdForSourceCode(String sourceCode) throws Exception
    {
      Map paramMap = new HashMap();
      paramMap.put("IN_SOURCE_CODE", sourceCode);
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(connection);
      dataRequest.setStatementID("GET_SOURCE_CODE_RECORD"); // This is in the utilities-statement file
      dataRequest.setInputParams(paramMap);
  
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
      if (rs == null || !rs.next())
      {
        return null;
      }
  
      return (String) rs.getObject("COMPANY_ID");
    }

    /**
     * Returns a populated ProductVO by its Product Id. 
     * Populates: productType, productSubType, serviceDuration
     * @param productId
     * @return
     * @throws Exception
     */
    private ProductVO getProductById(String productId) throws Exception
    {
      Map paramMap = new HashMap();
  
      paramMap.put("IN_PRODUCT_ID", productId);
  
      // Get the order header infomration
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(connection);
      dataRequest.setStatementID("GET_PRODUCT_BY_ID"); // This is in the utilities-statement file
      dataRequest.setInputParams(paramMap);

      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

      if (rs == null || !rs.next())
      {
        return null;
      }

      ProductVO productVO = new ProductVO();

      productVO.setProductId(productId);
      productVO.setProductType((String) rs.getObject("productType"));
      productVO.setProductSubType((String) rs.getObject("productSubType"));

      BigDecimal duration = (BigDecimal) rs.getObject("serviceDuration");
      
      if(duration != null)
      {
        productVO.setServiceDuration(new Integer(duration.intValue()));
      }

      return productVO;
    }
    
    
    
   /**
   * Retrieves the product by its Novator ID
   * Looks up the Product Id by its Novator Id then invokes {@link #getProductById(String)}
   * @param novatorId
   * @return 
   * @throws Exception
   */
    private ProductVO getProductByNovatorId(String novatorId) throws Exception
    {
      Map paramMap = new HashMap();
  
      paramMap.put("IN_NOVATOR_ID", novatorId);
  
      // Get the order header infomration
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(connection);
      dataRequest.setStatementID("OG_GET_PRODUCT_BY_NOVATOR_ID"); // This is in the utilities-statement file
      dataRequest.setInputParams(paramMap);

      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

      if (rs == null || !rs.next())
      {
        return null;
      }      
      
      String productId = rs.getString("product_id");
      if(productId != null)
      {
        return getProductById(productId);
      }
      
      return null;      
    }
    
   /**
   * Returns a populated ProductVO by its Id. 
   * Checks for the product using its Novator Id. If not found,
   * attempts to search for the Product using its Product Id instead.
   * Populates: productType, productSubType, serviceDuration
   * Invokes {@link #getProductByNovatorId(String)}
   * Invokes {@link #getProductById(String)}
   * 
   * @param productId
   * @return
   * @throws Exception
   */
    public ProductVO findProductById(String productId) throws Exception
    {
      if(logger.isDebugEnabled())
        logger.debug("Retrieving Product by Novator Id: " + productId);
      
      ProductVO retVal = getProductByNovatorId(productId);
      
      if(retVal == null) 
      {
        if(logger.isDebugEnabled())
          logger.debug("Product Not Found by Novator Id, searching by Product Id: " + productId);
        
        retVal = getProductById(productId);
      }
      
      return retVal;
    }
    
}
