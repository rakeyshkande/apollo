package com.ftd.osp.ordergatherer;

/**
 * This class is used to log an Order Validation Exception. 
 *
 * @author Rose Lazuk
 */

public class OrderValidationException extends Exception
{

  /**
   * Generic Order Validation Exception Message
   */
  public OrderValidationException()
  {
    super();
  
  }

  /**
   * Detailed Order Validator Exception Message
   *
   * @param OrderValidatorMessage String
   */
  public OrderValidationException(String OrderValidatorMessage)
  {
    super(OrderValidatorMessage);
  
  }
}