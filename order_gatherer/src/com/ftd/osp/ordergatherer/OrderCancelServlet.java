package com.ftd.osp.ordergatherer;


import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.HttpServletResponseUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.order.ScrubMapperDAO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;

import java.io.IOException;

import java.sql.Connection;

import java.util.*;

import javax.naming.InitialContext;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class is responsible for handling
 * cancellation requests sent by POP.
 * 
 * @author Carl Jenkins
 */

public class OrderCancelServlet extends HttpServlet
{
 
  //static variables
  public static final String MASTER_ORDER_NUMBER = "masterOrderNumber";
  public static final String STATUS_PARAM = "RegisterOutParameterStatus";
  public static final String MESSAGE_PARAM = "RegisterOutParameterMessage";
  public static final String ORDER_CANCEL = "OrderCancelServlet";
  public static final String PRODUCT_SHIPPED = "PS";
 

  //stored procs
  public static final String SCRUB_ORDERS_QUERY_PKG_GET_GUID = "SCRUB.ORDERS_QUERY_PKG.get_guid_from_mon";
  public static final String POP_GET_SHIPPED_ITEMS = "POP.GET_SHIPPED_BY_MASTER_ORDER";
  public static final String POP_INSERT_SCRUB_MESSAGES = "POP.INSERT_SCRUB_MESSAGES";
  
  private static String msg_status_dispatch = "ES1004"; 
  private Logger logger;
  
   public void init(ServletConfig config) throws ServletException
  {
    logger = new Logger("com.ftd.osp.ordergatherer.OrderCancelServlet");
    super.init(config);
  }
  
  
  public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException
  {
    doPost(request,response);
    
  }//end doGet
 
 /**
   * Performs the main logic of this class by mapping an
   * order from a guid and finding the shipping status of the order.
   * Based on the shipping status a cancellation request might be sent to the HP
   * or an update to the SCRUB_MESSAGES table is performed.
   * @param request
   * @param response
   * @throws java.io.IOException
   * @throws javax.servlet.ServletException
   */
  public void doPost(HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException
  {
        Connection con = null;
        OrderVO order = null;
        String guid = null;
        String orderNumber = 
           request.getParameter(MASTER_ORDER_NUMBER);
           
      
        try {
              con = DataSourceUtil.getInstance().getConnection("ORDER SCRUB");
              ScrubMapperDAO dao = new ScrubMapperDAO(con);
              if( orderNumber != null && !orderNumber.equals("") )
              {      
               guid = retrieveOrderGuid(orderNumber);
                order = dao.mapOrderFromDB( guid );  
              }
              else
              {
                String message = "The Order Number passed to OrderCancelServlet is NULL";
                logger.error(message);
                throw new Exception(message);
              }
              String shipped_status = 
                    findShippingStatus(orderNumber);
              String externalOrderNumber = 
                    retrieveItemLevelOrderNumber(order);
                  
              //If the status returned is N the order
              //has not been shipped and we need to send
              //a transmission to the HP to cancel the order
              if ( shipped_status.equalsIgnoreCase("N"))
              {
                handleProductCancellation(order, guid, con);
              }
               
              //if the status returned is Y the order
              //has been shipped and we need to update   
              //POP.SCRUB_MESSAGES with a value of PS in the 
              //DATA column.
              if( shipped_status.equalsIgnoreCase("Y"))
              {
                handleProductShipped(order,externalOrderNumber);
              }
              //if everything works out send 200 OK
              this.returnSuccess(response);
        }
        catch(Exception e)
        {
        logger.error(ORDER_CANCEL + ":" + "Unable to process cancellation" + e);
        logger.error(e);
        this.returnError(response, e.toString());
        }
        finally
        {   
            try 
            {
              if(con != null)
              {
                con.close();
              }  
            }
            catch(Exception e)
            {
              logger.error("Error closing connection in OrderCancelServlet");
            }
        }
  }//end doPost
 
 
 
 /**
   * Retrieve a GUID from SCRUB based on the Master Order Number 
   * @param orderNumber
   * @return 
   * @throws java.lang.Exception
   */
  private String retrieveOrderGuid(String orderNumber)throws Exception
  {
    String guid = "";
    Connection con = null;
    try
    {
         con =
            DataSourceUtil.getInstance().getConnection("ORDER SCRUB");
        
        DataRequest dataRequest = new DataRequest();
        HashMap messageMap = new HashMap();
        
        messageMap.put("IN_MASTER_ORDER_NUMBER", orderNumber);
        dataRequest.setConnection(con);
        dataRequest.setStatementID(SCRUB_ORDERS_QUERY_PKG_GET_GUID);
        dataRequest.setInputParams(messageMap);
   
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        guid = (String)dataAccessUtil.execute(dataRequest);
        
        if(guid == null)
        {
          String message = "Order Number: " + orderNumber + " " + "is not an order in SCRUB";
          logger.error(message);
          throw new Exception(message);
        }
    }
    catch(Exception e)
    {
      logger.debug("Unable to retrieve guid based on current master order number: " + orderNumber); 
      throw new Exception(e.toString());
    }
    finally
    {
      if(con != null)
        con.close();
    }
    
    return guid; 
  }
 
  
  /**
   * Returns the shipping column from the POP.ITEMS_SHIPPED table.
   * @param orderNumber
   * @return 
   * @throws java.lang.Exception
   */
  private String findShippingStatus(String orderNumber) throws Exception
  {
    String status = "";
     Connection con = null;
    try
    {
        con =
          DataSourceUtil.getInstance().getConnection("ORDER SCRUB");
        DataRequest dataRequest = new DataRequest();
        HashMap messageMap = new HashMap();
        
        messageMap.put("IN_MASTER_ORDER_NUMBER", orderNumber);
        dataRequest.setConnection(con);
        dataRequest.setStatementID(POP_GET_SHIPPED_ITEMS);
        dataRequest.setInputParams(messageMap);
      
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        CachedResultSet results = (CachedResultSet)dataAccessUtil.execute(dataRequest);
        //Currently Wal-Mart only has one item on each order
        if( results.getRowCount() > 1 ) 
        { 
          logger.error(ORDER_CANCEL + "Error retrieving 'shipped status' from ITEMS_SHIPPED");
          throw new Exception("More than one Master Order Number " + 
                "found when looking up shipped status: " + results.getRowCount());
        }
        
        while(results.next())
        {
          status = (String)results.getObject(5);
        }
        //if no rows are found in the ITEMS_SHIPPED table 
        //we need to treat this the same as if the item has not been shipped yet.
        if( results.getRowCount() == 0 ) 
        {
            status = "N";
            logger.error(ORDER_CANCEL + ":" + 
                "Number of rows found for shipping status: " + results.getRowCount());
        }
    }
    finally{
      if(con != null)
      con.close();
    }
      
    return status;  
  }
 
 
  /**
   * Updates the SCRUB_MESSAGES table with a value of PS
   * if the product has already been shipped and can not be 
   * cancelled
   * @param order
   * @param externalOrderNumber
   * @throws java.lang.Exception
   */
   private void handleProductShipped(OrderVO order, String externalOrderNumber) throws Exception
   {
    Connection con = null;
    try {
       String partnerId = null;
       
       ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
       con =
            DataSourceUtil.getInstance().getConnection("ORDER SCRUB");
            
        partnerId = 
              configUtil.getProperty(OrderGatherConstants.GATHERER_CONFIG_FILE, "walmart_origin_internet");
       logger.error("PartnerId : " + partnerId);
        
       DataRequest dataRequest = new DataRequest();
       HashMap inParms = new HashMap();
       
       inParms.put("IN_PARTNER_ID", partnerId.toUpperCase());
       inParms.put("IN_MESSAGE_TYPE","STATUS");
       inParms.put("IN_MASTER_ORDER_NUMBER",order.getMasterOrderNumber());
       inParms.put("IN_EXTERNAL_ORDER_NUMBER", externalOrderNumber);
       inParms.put("IN_DATA", PRODUCT_SHIPPED); //set Data column to PS
       inParms.put("IN_CREATED_BY",ORDER_CANCEL);
       inParms.put("IN_STATUS","");
       inParms.put("IN_ERROR_DESCRIPTION","");
       
       dataRequest.setConnection(con);
       dataRequest.setInputParams(inParms);
       dataRequest.setStatementID(POP_INSERT_SCRUB_MESSAGES);
       
       DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
       Map outputs = (Map)dataAccessUtil.execute(dataRequest);
       
       String status = (String)outputs.get(STATUS_PARAM);
       
       if(status.equalsIgnoreCase("N"))
       {
         String message = 
              (String)outputs.get(MESSAGE_PARAM);
          logger.error("OrderCancelServlet: Error inserting into POP.SCRUB MESSSAGES");
          throw new Exception(message.toString());
       }   
       else
       {
         logger.error(ORDER_CANCEL + "SCRUB_MESSAGES insert successful");
       }
    }
    catch(Exception e)
    {
      logger.error("OrderCancelServlet: unable to update SCRUB_MESSAGES");
    }
    finally{
      if(con != null)
      con.close();
    }
   }
   
   
   /**
   * Used to find the external order number
   * on an order. Will just return first (one) order found.
   * @param order
   * @return 
   */
   private String retrieveItemLevelOrderNumber(OrderVO order)
   {
     Collection items = order.getOrderDetail();
     OrderDetailsVO details = null;
     Iterator iter = items.iterator();
     String  externalOrderNumber = "";
     //For WalMart this will only ever return 1 item on the order
     while( iter.hasNext() )
     {
       details = 
            (OrderDetailsVO)iter.next();
       externalOrderNumber = 
            details.getExternalOrderNumber();
     }
     logger.error("External Order Number: " + externalOrderNumber);
     return externalOrderNumber;
   }
 
 
 /**
   * This method will send a cancellation request to the Dispatcher. 
   * @param order
   * @param guid
   * @param con
   * @throws java.lang.Exception
   */
  private void handleProductCancellation(OrderVO order, String guid, Connection con) throws Exception
  {
      ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
      InitialContext context = null;       
        // get the initial context
      context = new InitialContext();

      List items = order.getOrderDetail();
      MessageToken mt = null;
      Iterator it = items.iterator();
      OrderDetailsVO item = null;
      //attach the partnerId WLMTI to the guid.
      //This gives ups a way to know that the order is a wlmti order
      //and that we need to send a cancel request to the HP
      while(it.hasNext())
      {
        item = (OrderDetailsVO)it.next();
        mt = new MessageToken();
        
        mt.setStatus(this.msg_status_dispatch);
        mt.setMessage(item.getGuid() + "/" + item.getLineNumber() + 
              configUtil.getProperty(OrderGatherConstants.GATHERER_CONFIG_FILE, "walmart_origin_internet"));
      }
      
     logger.error("MESSAGE : " + (String)mt.getMessage());
     logger.error("STATUS : " + (String)mt.getStatus());

     Dispatcher dispatcher = Dispatcher.getInstance();
     dispatcher.dispatch(context, mt); //dispatch the token to the HP
    
  }//end handleProductCancellation
 

 
  private void returnSuccess(HttpServletResponse response)
  {
    try
    {
        HttpServletResponseUtil.sendError(response, OrderGatherConstants.SUCCESS_CODE, "Success");
    }
    catch(Exception ex)
    {
      logger.error(ex.toString());
    }
  }
 
 
 
 private void returnError(HttpServletResponse response, String errorReason)
  {
    try
    {
        HttpServletResponseUtil.sendError(response, OrderGatherConstants.ERROR_CODE, errorReason);
    }
    catch(Exception ex)
    {
        logger.error("XML file transmission failed: " + ex.toString());
    }
  }
}//end OrderCancelServlet