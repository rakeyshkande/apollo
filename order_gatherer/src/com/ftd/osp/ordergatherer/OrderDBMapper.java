package com.ftd.osp.ordergatherer;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.ftd.ftdutilities.FTDCAMSUtils;
import com.ftd.osp.utilities.PartnerUtility;
import com.ftd.osp.utilities.order.ScrubMapperDAO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MembershipDetailsVO;

/**
 * This class takes the order value object that was
 * created and calls the data access layer to map the order
 * to the database.
 *
 * @author Rose Lazuk
 */

public class OrderDBMapper 
{
  private Connection connection;
  private Logger logger;
  
  public OrderDBMapper(Connection connection) 
  {
    super();
    this.connection = connection;
    logger = new Logger("com.ftd.osp.ordergatherer.OrderDBMapper");

  }

  /**
   * Map the order to the database
   * 
   * @param order OrderVO
   * @exception OrderMappingException
   */

  public void map(OrderVO order) throws OrderMappingException, Exception {     
    try {
		ScrubMapperDAO scrubMapperDAO = new ScrubMapperDAO(connection);
		// #18259 - for partner orders set the PC member ship details. This happens at recalculation for other orders.
		if(logger.isDebugEnabled()) {
			logger.debug("map(), order origin - " + order.getOrderOrigin());
		}
		/*try{
			if (new PartnerUtility().isPartnerOrder(order.getOrderOrigin(),order.getSourceCode(), connection)) {
				//setPCMembershipDetail(order);
			}
		} catch (Exception e) {
			logger.error("Unable to set the Pc member detail into order item: " + e.getMessage());
		}*/
		if(!scrubMapperDAO.mapOrderToDB(order))
			throw new OrderMappingException("Error mapping order to scrub tables");
    } catch(Exception e) {
        String errorMessage = e.toString();
        int pos = errorMessage.indexOf("DUPLICATE MASTER ORDER NUMBER");
        if(pos > 0) {
            throw new DuplicateOrderException();
        } else {
            throw new OrderMappingException(e);         
        }
    }
  }
    /**
	 * @param order
	 *
	private void setPCMembershipDetail(OrderVO order) {		
		try{
		logger.info("*******************setPCMembershipDetail()*********************");
		Map<String, MembershipDetailsVO> membershipMap = new HashMap<String, MembershipDetailsVO>();
		if (order != null && StringUtils.isNotEmpty(order.getBuyerEmailAddress())) {
			List<String> emailList = new ArrayList<String>();
			emailList.add(order.getBuyerEmailAddress());
			membershipMap = FTDCAMSUtils.getMembershipData(emailList, order.getOrderDateTime());
		}
				
		MembershipDetailsVO pcMemDetails = null;
		if (membershipMap != null && membershipMap.containsKey(FTDCAMSUtils.PREMIERCIRCLE_MEMBER_TYPE)) {
			if(logger.isDebugEnabled()) {
				logger.debug("Retrieved non null membership data from CAMS for " + order.getGUID());
			}
			pcMemDetails = membershipMap.get(FTDCAMSUtils.PREMIERCIRCLE_MEMBER_TYPE);			
		}
		
		List<OrderDetailsVO> lineItems = order.getOrderDetail();
		for (int i = 0; i < lineItems.size(); i++) {
			logger.info("order.isAllowPCCheckUpdateOrder() : " + order.isAllowPCCheckUpdateOrder());
			OrderDetailsVO detailVO = (OrderDetailsVO) lineItems.get(i);
			if (pcMemDetails != null && "Y".equalsIgnoreCase(pcMemDetails.getStatus())) {
				detailVO.setPcGroupId(pcMemDetails.getGroupId());
				detailVO.setPcMembershipId(pcMemDetails.getMembershipId());
				detailVO.setPcFlag(pcMemDetails.getStatus());
			} else {
				detailVO.setPcFlag("N");
			}
		} 
		} catch (Exception e) {
			logger.error("setPCMembershipDetail() - Unable to set the Pc member detail into order item: " + e.getMessage());
		}		
	}*/
	
 }