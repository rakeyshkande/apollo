package com.ftd.osp.ordergatherer;


import com.ftd.osp.utilities.crypto.PGPEncryption;
import com.ftd.osp.utilities.j2ee.*;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import oracle.sql.CLOB;

import org.w3c.dom.NodeList;

import java.io.*;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class OrderGatherUtils 
{
  private Logger logger;
  private static  Map<String,String> truncateFields;
  
  public OrderGatherUtils()
  {
    logger = new Logger("com.ftd.osp.ordergatherer.OrderGatherer");
  }

      /**
     * Retrieves the xml stored in the order_archive table in Scrub
     * based on the masterOrderNumber.  Converts to hp string and returns
     * a list of hp strings to be returned to the hp.
     * @param masterOrderNumber 
     * @param transType the transmission type
     * @param xslFile name of xsl file
     * @return the list of strings sent in the transmission to the HP
     * @exception throws exception if there is an error
     */
    public List getHPStringList(String masterOrderNumber, String transType, File xslFile) throws Exception
    {
        List updatedTransmissions = new ArrayList();

        List transmissions = null;
        String updatedTransmission = null;
 
        try
        {
            Document originalTransmission = DOMUtil.getDocument(getXMLFromArchive(masterOrderNumber));
            //XMLDocument originalTransmission = (XMLDocument)DOMUtil.getDocument(new File(filename));
            NodeList nl = DOMUtil.selectNodes(originalTransmission, "/order/items/item");
            int itemNumbers = nl.getLength();
            // Transform xml based on transmission type
            transmissions = this.transformTransmission(originalTransmission, transType, xslFile, itemNumbers);
            if(transType.equals("HP_STRING"))
            {
              //before an order gets sent to the hp, we need to pre-append
              //the total length of the file followed by two ~~'s
              String fileLength = "";
              for(Iterator iter = transmissions.iterator(); iter.hasNext();){
               updatedTransmission = (String)iter.next();
               //get the length of the updated xml transmission
               int updatedTransmissionLength = updatedTransmission.trim().length();

               //get the length of the number set to updatedTransmissionLength
               //and add 2 ~~'s to that length
               fileLength = fileLength.valueOf(updatedTransmissionLength) + "~~";
               char[] fileLengthArray = fileLength.toCharArray();

               //add the updatedTransmissionLength to the
               //fileLengthArray size to come up with the total
               //length of the updatedTransmission
               int transLength = fileLengthArray.length + updatedTransmissionLength;

               //pre-append the total length of the transmission to the
               //order followed by two ~~'s
               updatedTransmission = transLength + "~~" + updatedTransmission;
               updatedTransmissions.add(updatedTransmission);
              }//end for loop
           }//end if
           else
           {
              updatedTransmissions.add(transmissions);
           }
        }//end try
        catch(Exception e)
        {
            logger.error(e.toString());
            throw new Exception(e.toString());
        }

        return updatedTransmissions;
    }   


    /**
     * Method which returns the archived xml 
     * 
     * @param masterOrderNumber 
     * @return the string sent in the transmission to the HP
     * @exception throws exception if there is an error
     */
    public String getXMLFromArchive(String masterOrderNumber) throws Exception 
    {
      String orderXML = "";
      Connection connection =  null;
      
      try {
         connection =  DataSourceUtil.getInstance().getConnection("ORDER SCRUB");
         
         DataRequest dataRequest = new DataRequest();
         HashMap inParms = new HashMap();
         
         inParms.put("IN_MASTER_ORDER_NUMBER", masterOrderNumber);         
         dataRequest.setConnection(connection);
         dataRequest.setInputParams(inParms);
         dataRequest.setStatementID("SCRUB.VIEW_ORDER_XML_ARCHIVE");
         
         DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();         
         CachedResultSet results = (CachedResultSet)dataAccessUtil.execute(dataRequest);
         
         while(results != null && results.next()) {
           CLOB xmlCLOB = (CLOB) results.getObject(2);
           orderXML = xmlCLOB.getSubString(1L, (int)xmlCLOB.length());
         }         
      }catch(Exception e){
        logger.error("OrderGatherUtils.getXMLFromArchive: could not retreive order xml for order" + masterOrderNumber);
        e.printStackTrace();
        throw e;
      } finally 
      {
        if (connection != null) 
        {
          try {
            connection.close();
          } catch (Exception e){}
        }
      }
      
      return orderXML;
    }
    
      /**
     * Private method which transforms the loaded transmission
     * into a HP string
     * @param xmlTransmission the original xml transmission
     * @param transmissionType the type of HP string to convert to
     * @param xslFile the name of the xsl file
     * @param int the number of line items in the order
     * @exception Exception
     * @return a List representing the transformed transmission
     */
    public List transformTransmission(Document xmlTransmission, String transmissionType, File xslFile, int itemNumbers) throws Exception
    {

      List hpStrings = new ArrayList();

      if(transmissionType != null && transmissionType.equals("HP_STRING"))
      {
          HashMap params = null;
          Node itemNode = null;
          NodeList itemNodeList = DOMUtil.selectNodes(xmlTransmission, "/order/items/item");

          if(itemNodeList != null)
          {
              TraxUtil traxUtil = TraxUtil.getInstance();

              for(int i=0; i < itemNodeList.getLength(); i++)
              {
                  itemNode = itemNodeList.item(i);

                  params = new HashMap();
                  params.put("itemNumbers", String.valueOf(itemNumbers));
                  params.put("orderDetailId", DOMUtil.selectSingleNode(itemNode, "order-number/text()").getNodeValue());
                  hpStrings.add(traxUtil.transform(xmlTransmission, xslFile, params));
              }
          }
      }
      else
      {
          // Output the string as xml
          StringWriter sw = new StringWriter();
          PrintWriter pw = new PrintWriter(sw);
          DOMUtil.print(xmlTransmission, pw);
          hpStrings.add(sw.toString());
      }

      return hpStrings;
    }

     /**
     * Private method which transforms the loaded transmission into a HP string 
     * based on the type passed into the method.
     * @param xmlTransmission the original archived transmission
     * @exception Exception
     * @return a string representing the transformed transmission
     */
    private String transformToXML(Document xmlTransmission) throws Exception
    {    
        String hpString = "";

        // Output the string as xml
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        DOMUtil.print(xmlTransmission, pw);
        hpString = sw.toString();
        
        return hpString;
    }
   
        
    /*
    * This method sends a message to the System Messenger.
    *
    * @param String message
    * @returns String message id
    */
    public void sendMessage(String message)
    {
      try
      {
      //build system vo
        // SystemMessage closes connectino passed in. Therefore, create a new connection.
        Connection connection =  DataSourceUtil.getInstance().getConnection("ORDER SCRUB");
        SystemMessengerVO sysMessage = new SystemMessengerVO();
        sysMessage.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
        sysMessage.setSource(OrderGatherConstants.MSG_SOURCE);
        sysMessage.setType("ERROR");
        sysMessage.setMessage(message);
        SystemMessenger.getInstance().send(sysMessage, connection);
      }
      catch(Throwable t)
      {
        logger.error("Sending system message failed. Message=" + message);
        logger.error(t);
      }
    }         
    
    
  /**
   * Decrypts the Passed in Order XML if it is encrypted. If not, returns the original string.
   * Otherwise, decryptes the Encrypted information, and returns the decrypted data.
   * @param orderXML
   * @return
   * @throws Exception
   */
    public String decryptOrderXML(String orderXML) throws Exception
    {
      String keyName = "";
      String encryptedData = "";
      
      /* Only enable locally for dev. 
      if(logger.isDebugEnabled()) 
      {
        logger.debug("Received: " + orderXML);
      }
      */
      
      try 
      {
        Document orderXMLDocument = DOMUtil.getDocument(orderXML);
        keyName = orderXMLDocument.getDocumentElement().getAttribute("key-name");
        
        if(keyName != null && keyName.length() > 0) 
        {
          encryptedData = DOMUtil.getNodeValue(orderXMLDocument.getDocumentElement());
        }        
      } catch (Exception e) 
      {
        // Error parsing the DOM, return original string, for default handling.
        keyName = "";
        encryptedData = "";      
      }
      
      if(keyName == null || keyName.length() == 0)
      {
        return orderXML; // Return original String
      }
            
      
      Connection connection = null;      
      
      try 
      {
        connection = DataSourceUtil.getInstance().getConnection("ORDER SCRUB");
        byte[] decryptedData = new PGPEncryption().decryptStream(new ByteArrayInputStream(encryptedData.getBytes("utf-8")), keyName, connection);
        String orderData = new String(decryptedData, "utf-8");
        
        return orderData;
      } catch (Exception e) 
      {
        logger.error("Failed to decrypt Order with Key code: " + keyName, e);
        sendMessage(throwableToString("Failed to decrypt Order with Key code: " + keyName + ";", e));
        throw e;
      } finally 
      {
        if(connection != null)
        {
          try 
          {
            connection.close();
          } catch (Exception ex)
          {  
          }
        }
      }
    }
    
    
   /**
   * Formats a throwable into a String, usefull for sending to exception to the System Message
   * @param header
   * @param t
   * @return
   */
    public String throwableToString(String header, Throwable t)
    {
      StringBuffer retVal = new StringBuffer(header);
      retVal.append(t.toString());
      
      if(t.getCause() != null) 
      {
        retVal.append(throwableToString(";", t.getCause()));
      }
      
      return retVal.toString();
    }
    
    
    public Map<String,String> loadAribaFieldtruncateMap(){
 	   
 	   if(null==truncateFields || truncateFields.size()<=0){
 	   truncateFields = new HashMap<String,String>();
 	   truncateFields.put("buyer-first-name","25");
 	   truncateFields.put("buyer-last-name", "25");
 	   truncateFields.put("buyer-address1", "45");
 	   truncateFields.put("buyer-address2","45");
 	   truncateFields.put("buyer-city", "30");
 	   truncateFields.put("buyer-state", "10");
 	   truncateFields.put("buyer-postal-code", "10");
 	   truncateFields.put("buyer-country","3");
 	   truncateFields.put("buyer-primary-phone", "20");
 	   truncateFields.put("buyer-daytime-phone", "20");
 	   truncateFields.put("buyer-secondary-phone","20");
 	   truncateFields.put("buyer-evening-phone", "20");
 	   truncateFields.put("buyer-primary-phone-ext", "10");
 	   truncateFields.put("buyer-work-ext", "10");
 	   truncateFields.put("buyer-fax","20");
 	   truncateFields.put("buyer-email-address", "55");
 	   
 	   truncateFields.put("recip-first-name", "25");
 	   truncateFields.put("recip-last-name","25");
 	   truncateFields.put("recip-address1","45");
 	   truncateFields.put("recip-address2", "45");
 	   truncateFields.put("recip-city", "30");
 	   truncateFields.put("recip-state", "10");
 	   truncateFields.put("recip-postal-code","10");
 	   truncateFields.put("recip-country", "3");
 	   truncateFields.put("recip-phone","20");
 	   truncateFields.put("recip-phone-ext", "10");
 	   
 	   truncateFields.put("ariba-buyer-cookie", "20"); 	   
 	   truncateFields.put("order-number","20");
 	   truncateFields.put("special-instructions", "300");
 	   truncateFields.put("card-message", "230");
 	   truncateFields.put("occassion", "3");
 	   truncateFields.put("productid", "10");
 	   truncateFields.put("ariba-unspsc-code","20");
 	   truncateFields.put("ariba-po-number","20");
 	   truncateFields.put("master-order-number","32");
 	  
 	   
 	   }
 	   return truncateFields;
 	   
    }
}