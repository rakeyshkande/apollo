package com.ftd.osp.ordergatherer;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.xpath.XPathExpressionException;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ftd.ftdutilities.GeneralConstants;
import com.ftd.osp.ordergatherer.dao.OrderGathererDAO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.FTDFuelSurchargeUtilities;
import com.ftd.osp.utilities.constants.PaymentExtensionConstants;
import com.ftd.osp.utilities.order.OrderIdGenerator;
import com.ftd.osp.utilities.order.vo.AVSAddressScoreVO;
import com.ftd.osp.utilities.order.vo.AVSAddressVO;
import com.ftd.osp.utilities.order.vo.AddOnsVO;
import com.ftd.osp.utilities.order.vo.BuyerAddressesVO;
import com.ftd.osp.utilities.order.vo.BuyerEmailsVO;
import com.ftd.osp.utilities.order.vo.BuyerPhonesVO;
import com.ftd.osp.utilities.order.vo.BuyerVO;
import com.ftd.osp.utilities.order.vo.CoBrandVO;
import com.ftd.osp.utilities.order.vo.CreditCardsVO;
import com.ftd.osp.utilities.order.vo.FraudCommentsVO;
import com.ftd.osp.utilities.order.vo.ItemTaxVO;
import com.ftd.osp.utilities.order.vo.OrderContactInfoVO;
import com.ftd.osp.utilities.order.vo.OrderDetailExtensionsVO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderExtensionsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.PaymentsVO;
import com.ftd.osp.utilities.order.vo.RecipientAddressesVO;
import com.ftd.osp.utilities.order.vo.RecipientPhonesVO;
import com.ftd.osp.utilities.order.vo.RecipientsVO;
import com.ftd.osp.utilities.order.vo.TaxVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.SurchargeVO;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.handlers.ProductMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ProductMasterVO;


/**
 * This class takes the fields that were sent in the xml transmission
 * and populates the value objects associated with an order.
 *
 * @author Rose Lazuk
 */

public class OrderPreprocessor 
{
  private Document orderXMLDocument = null;
  private Document orderXMLCDATADocument = null;
  private OrderVO order = null;
  private String orderXMLString = null;
  private String orderId = null;
  private String origin = null;
  private String masterOrderNumber = null;
  private long timeStamp = 0; 
  private Logger logger;
  private ConfigurationUtil configUtil;
  private String og_origin = null; 
  private Connection connection;
  private boolean isRosesDotCom;
  private String rosesDotComAddonTotal;
  boolean newWebsiteOrder = false;

  //PRODUCT TYPES
  private String PRODUCT_TYPE_FRESHCUT = "FRESHCUT";
  private String PRODUCT_TYPE_SAMEDAY_FRESHCUT = "SAMEDAYFRESHCUT";
  private String PRODUCT_TYPE_FLORAL = "FLORAL";
  private String PRODUCT_TYPE_SPECIALTY_GIFT = "SPECIALTYGIFT";
  // TMI
  private String PRODUCT_TYPE_SPECIALITY_GIFT = "SPECIALITYGIFT";

  /**
   * Constructor for OrderPreprocessor
   * @param orderXMLString String contains order that was sent in
   * xml transmission
   */
  public OrderPreprocessor(String orderXMLString, Connection connection) throws Exception
  {
    this.orderXMLString = orderXMLString;
    this.order = new OrderVO();
    this.orderId = OrderIdGenerator.getInstance().getOrderId(connection);
    this.timeStamp = System.currentTimeMillis();
    configUtil = ConfigurationUtil.getInstance();
    this.og_origin = configUtil.getProperty(OrderGatherConstants.GATHERER_CONFIG_FILE,"origin");
    this.connection = connection;
    this.logger = new Logger("com.ftd.osp.ordergatherer.OrderPreprocessor");
    this.isRosesDotCom = false;
    this.rosesDotComAddonTotal = null;
  }

/**
 * Create an order
 * 
 * @exception Exception 
 * @return Order 
 */
  public OrderVO createOrder() throws Exception
  {
    try
    {
        orderXMLDocument = DOMUtil.getDocument(orderXMLString);
        DocumentBuilder docBuilder = DOMUtil.getDocumentBuilder(false, true, true, true, false, false);
        orderXMLCDATADocument = DOMUtil.getDocument(docBuilder, orderXMLString);
    }
    catch(Exception ex)
    {
        logger.error("Unable to convert xml string to dom.  ");
        throw new Exception(ex.toString()); 
    }
   
    
    order.setGUID(this.orderId);
    order.setTimeStamp(this.timeStamp);
    this.mapXMLtoOrder(order);
    
    return this.order;
  }

/**
 * Map the xml file that was sent in the transmission to the
 * Order Value Object
 * 
 * @param order OrderVO value object that holds the whole order
 */
  private void mapXMLtoOrder(OrderVO order) 
  {
    Node xmlNode = null;
    String nodeValue = "";
    
    try
    {
      Document orderXMLDoc = orderXMLDocument;
      xmlNode = DOMUtil.selectSingleNode(orderXMLDoc, "/order/header");
    
      nodeValue = this.getSingleNode(xmlNode, "master-order-number/text()");
      this.masterOrderNumber = nodeValue;
      order.setMasterOrderNumber(nodeValue);
         
      /* Determine if this is an OE order
       * Master order numbers starting with "M + a digit" are also created 
       * by WebOE and Modify Order but are not routed through Order Gatherer.
       */ 
      String regex = "M\\d";
      Pattern p = Pattern.compile(regex);

      if(logger.isDebugEnabled())
      {
          logger.debug("Regular expression: " + regex);
          logger.debug("Master order number: " + order.getMasterOrderNumber());
      }

      Matcher m = p.matcher(order.getMasterOrderNumber());
      order.setOeOrder(m.lookingAt());

      if(logger.isDebugEnabled())
      {
          logger.debug(order.getMasterOrderNumber() + " is an OE order: " + order.isOeOrder());
      }
      
      nodeValue = this.getSingleNode(xmlNode, "/order/header/buyer-signed-in/text()");
      if(nodeValue != null && nodeValue.length() != 0)
      {
          order.setBuyerSignedIn(nodeValue);
      }
      else
          order.setBuyerSignedIn("N");

      nodeValue = this.getSingleNode(xmlNode, "/order/header/origin/text()");
      order.setOrderOrigin(nodeValue);
      if (nodeValue.equalsIgnoreCase(configUtil.getProperty(OrderGatherConstants.GATHERER_CONFIG_FILE, "roses_origin_internet"))) {
          this.isRosesDotCom = true;
          if (logger.isDebugEnabled()) {
              logger.debug("Roses.com order being processed");
          }
      }
      origin = nodeValue.toLowerCase();
      
      nodeValue = this.getSingleNode(xmlNode, "/order/header/order-channel/text()");
      if (!StringUtils.isEmpty(nodeValue)) {
          // this is an order from the new website
          logger.info("Order channel: " + nodeValue);
          newWebsiteOrder = true;
      }
      
      nodeValue = this.sourceCodePreprocessor(xmlNode, "/order/header/source-code/text()", order.getOrderOrigin());
      order.setSourceCode(nodeValue);
       
      // Only exists for OE orders
      nodeValue = this.getSingleNode(xmlNode, "/order/header/order-created-by-id/text()");
      order.setCsrId(nodeValue);
      
      // Only exists for OE orders
      nodeValue = this.getSingleNode(xmlNode, "/order/header/buyer-has-free-shipping/text()");
      order.setOeBuyerHasFreeShipping(nodeValue);
      
      nodeValue = this.getSingleNode(xmlNode, "/order/header/discount-total/text()");
      order.setDiscountTotal(nodeValue);
      
      nodeValue = this.getSingleNode(xmlNode, "/order/header/dnis-id/text()");
      order.setDnisCode(nodeValue);

      nodeValue = this.getSingleNode(xmlNode, "/order/header/order-count/text()");
      order.setProductsTotal(nodeValue);

      nodeValue = this.getSingleNode(xmlNode, "/order/header/order-amount/text()");
      order.setOrderTotal(nodeValue);

      nodeValue = this.getSingleNode(xmlNode, "/order/header/transaction-date/text()");
      order.setOrderDate(nodeValue);

      nodeValue = this.getSingleNode(xmlNode, "/order/header/socket-timestamp/text()");
      order.setSocketTimestamp(nodeValue);

      List buyer = this.getBuyer(xmlNode);
      order.setBuyer(buyer);
      order.setBuyerEmailAddress(getBuyerEmailAddress(xmlNode));
      
      nodeValue = this.getSingleNode(xmlNode, "/order/header/ariba-buyer-cookie/text()");
      order.setAribaBuyerCookie(nodeValue);
   
      nodeValue = this.getSingleNode(xmlNode, "/order/header/ariba-asn-buyer-number/text()");
      order.setAribaBuyerAsnNumber(nodeValue);

      nodeValue = this.getSingleNode(xmlNode, "/order/header/ariba-payload/text()");
      order.setAribaPayload(nodeValue);
      
      nodeValue = this.getSingleNode(xmlNode, "/order/header/alt-pay-method/redemption-rate/text()");
      order.setMpRedemptionRateAmt(nodeValue);

      nodeValue = this.getSingleNode(xmlNode, "/order/header/co-brand-credit-card-code/text()");
      //default to "N" if no flag comes across in the
      //xml transmission file
      if(nodeValue.equals(""))
        nodeValue = "N";
      order.setCoBrandCreditCardCode(nodeValue);

      nodeValue = this.getSingleNode(xmlNode, "/order/header/loss-prevention-indicator/text()");
      order.setLossPreventionIndicator(nodeValue);
      
      // Only exists for OE orders
      nodeValue = this.getSingleNode(xmlNode, "/order/header/fraud-id/text()");
      if(nodeValue != null && !"".equals(nodeValue))
      {
        FraudCommentsVO fraudComments = new FraudCommentsVO();
        fraudComments.setFraudID(nodeValue);
        nodeValue = this.getSingleNode(xmlNode, "/order/header/fraud-comments/text()");
        fraudComments.setCommentText(nodeValue);

        order.setFraudFlag("Y");
        order.setFraudComments(new ArrayList());
        order.getFraudComments().add(fraudComments);
      }

      List <OrderExtensionsVO> orderExtensions = this.getOrderExtensions(xmlNode);
      order.setOrderExtensions(orderExtensions);

      List payments = this.getPayments(xmlNode);
      order.setPayments(payments);

      List lCoBrands = this.getCoBrands(xmlNode);
      order.setCoBrand(lCoBrands);
    
      List lOrderDetails = this.getOrderDetails(xmlNode, order.getSourceCode());
      order.setOrderDetail(lOrderDetails);
      
      order.setAddOnAmountTotal(calculateAddOnTotal(lOrderDetails));

      order.setAddOnDiscountAmount(calculateAddOnDiscountTotal(lOrderDetails));

      List novatorFraudCodes = getNovatorFraudCodes(xmlNode);
      order.setFraudCodes(novatorFraudCodes);

      // Taken care of in massager now  03/17/04
      //if(novatorFraudCodes.size() > 0)
      //order.setFraudFlag("Y");

      //add bulk order specific fields to value objects
      if(origin.equals("bulk"))
      {
        List lOrderContactInfo = this.getOrderContactInfo(xmlNode);
        order.setOrderContactInfo(lOrderContactInfo);
      }

      order.setStatus(OrderGatherConstants.STATUS);
      //parse and store the language-id value
      nodeValue = this.getSingleNode(xmlNode, "/order/header/language-id/text()");
      if(nodeValue != null && nodeValue.trim().length() > 0){
    	  order.setLanguageId(nodeValue);
      }
      
      nodeValue = this.getSingleNode(xmlNode, "/order/header/joint-cart-indicator/text()");
      order.setJointCartIndicator(nodeValue);
      
    }
    catch(Exception ex)
    {
      logger.error("Error mapping the xml to the order value object: " + ex.toString()); 
    }
  
  }


  private List getNovatorFraudCodes(Node xmlNode) 
  {
      List novatorFraudCodes = new ArrayList();
      NodeList fraudCodes = null;

      try
      {
          fraudCodes = DOMUtil.selectNodes(xmlNode, "/order/header/fraud_codes/fraud_code");
          for(int i = 0; i < fraudCodes.getLength(); i++)
          {
            novatorFraudCodes.add(fraudCodes.item(i).getFirstChild().getNodeValue());
          }
      }
      catch(Exception e){
          logger.error("Error retrieving fraud codes: " + e);
      }
      
      return novatorFraudCodes;
  }


  /**
  * Get buyer information that's associated with the order.
  * 
  * @param xmlNode XMLNode 
  * @return List of buyer value objects
  */
 private List getBuyer(Node xmlNode)
 {
    List lBuyer = new ArrayList();
    BuyerVO buyer = new BuyerVO();
    String nodeValue = "";
   
    try{

      nodeValue = this.getSingleNode(xmlNode, "/order/header/buyer-first-name/text()");
      buyer.setFirstName(nodeValue);
      
      nodeValue = this.getSingleNode(xmlNode, "/order/header/buyer-last-name/text()");
      buyer.setLastName(nodeValue);

      List buyerAddress = this.getBuyerAddress(xmlNode);
      buyer.setBuyerAddresses(buyerAddress);

      List buyerPhones = this.getBuyerPhones(xmlNode);
      buyer.setBuyerPhones(buyerPhones);

      List buyerEmail = this.getBuyerEmail(xmlNode);
      buyer.setBuyerEmails(buyerEmail);

      buyer.setStatus(OrderGatherConstants.STATUS);

      lBuyer.add(buyer);
      
     }
     catch(Exception ex)
     {
        logger.error("Error mapping the xml to the Buyer value object: " + ex.toString()); 
     }
   
     return lBuyer;
    
 }

 /**
  * Get buyer address information associated with the order.
  * 
  * @param xmlNode XMLNode 
  * @return List of buyer address value objects
  */
 private List getBuyerAddress(Node xmlNode)
 {
    List lBuyerAddress = new ArrayList();
    BuyerAddressesVO buyerAddress = new BuyerAddressesVO();
    String nodeValue = "";
    
    try{

      nodeValue = this.getSingleNode(xmlNode, "/order/header/buyer-address1/text()");
      buyerAddress.setAddressLine1(nodeValue);
      
      nodeValue = this.getSingleNode(xmlNode, "/order/header/buyer-address2/text()");
      buyerAddress.setAddressLine2(nodeValue);
      
      nodeValue = this.getSingleNode(xmlNode, "/order/header/buyer-business-name/text()");
      buyerAddress.setAddressEtc(nodeValue);
      
      nodeValue = this.getSingleNode(xmlNode, "/order/header/buyer-city/text()");
      buyerAddress.setCity(nodeValue);
      
      nodeValue = this.getSingleNode(xmlNode, "/order/header/buyer-state/text()");
      buyerAddress.setStateProv(nodeValue);
      
      nodeValue = this.getSingleNode(xmlNode, "/order/header/buyer-postal-code/text()");
      buyerAddress.setPostalCode(nodeValue);
      
      nodeValue = this.getSingleNode(xmlNode, "/order/header/buyer-country/text()");
      buyerAddress.setCountry(nodeValue);

      //add bulk order specific fields to value objects
      if(origin.equals("bulk"))
      {
        nodeValue = this.getSingleNode(xmlNode, "/order/header/buyer-business/text()");
        if(!nodeValue.equals(""))
          {
            buyerAddress.setAddressEtc(nodeValue);
            //set address type to 'Business' if origin = bulk
            //and buyer-business is not null
            buyerAddress.setAddressType("Business");
          }
          else
          {
              //for all other cases, set address type to 'Home'
              buyerAddress.setAddressType("Home"); 
          }
      }
      else
      {
          //for all other cases, set address type to 'Home'
          buyerAddress.setAddressType("Home"); 
      }

      lBuyerAddress.add(buyerAddress);
      
     }
     catch(Exception ex)
     {
        logger.error("Error mapping the xml to the buyerAddress value object: " + ex.toString()); 
     }
   
     return lBuyerAddress;
    
 }

/**
  * Get buyer phone information associated with the order.
  * 
  * @param xmlNode XMLNode 
  * @return List of buyer phone value objects
  */
 private List getBuyerPhones(Node xmlNode)
 {
    List lBuyerPhones = new ArrayList();
    BuyerPhonesVO buyerPhones = null;
    String nodeValue = "";
    String phone = "";
    String ext = "";
    String smsOptIn = "";
    String phoneNumberType = "";
    
     try{

      buyerPhones = new BuyerPhonesVO();
      
      //set work phone number
      phone = this.getSingleNode(xmlNode, "/order/header/buyer-primary-phone/text()");
      if(phone==null || phone == ""){
    	  phone = this.getSingleNode(xmlNode, "/order/header/buyer-daytime-phone/text()");
      }
      
      if(!phone.equals(""))
      {
        buyerPhones.setPhoneNumber(phone);
      }

      ext = this.getSingleNode(xmlNode, "/order/header/buyer-primary-phone-ext/text()");
      if(ext==null || ext == ""){
    	  ext = this.getSingleNode(xmlNode, "/order/header/buyer-work-ext/text()");
      }
      if(!ext.equals(""))
      {
        buyerPhones.setExtension(ext);
      }
      
      phoneNumberType = this.getSingleNode(xmlNode, "/order/header/buyer-primary-phone-type/text()");
      if(!phone.equals(""))
      {
        buyerPhones.setPhoneNumberType(phoneNumberType);
      }
      
      smsOptIn = this.getSingleNode(xmlNode, "/order/header/buyer-primary-sms-optin-status/text()");
      if(!phone.equals(""))
      {
        buyerPhones.setSmsOptIn(smsOptIn);
      }

      if(!phone.equals("") || !ext.equals(""))
      {
        buyerPhones.setPhoneType(configUtil.getProperty(OrderGatherConstants.GATHERER_CONFIG_FILE, "work"));
        lBuyerPhones.add(buyerPhones);  
      }   
           
      //set home phone number
      buyerPhones = new BuyerPhonesVO();

      phone = this.getSingleNode(xmlNode, "/order/header/buyer-secondary-phone/text()");
      if(phone==null || phone == ""){
    	  phone = this.getSingleNode(xmlNode, "/order/header/buyer-evening-phone/text()");
      }
      
      
      if(!phone.equals(""))
      {
        buyerPhones.setPhoneNumber(phone);
        
      }
            
      ext = this.getSingleNode(xmlNode, "/order/header/buyer-secondary-phone-ext/text()");
      if(ext != null && !ext.equals(""))
      {
        buyerPhones.setExtension(ext);
      }
      
      phoneNumberType = this.getSingleNode(xmlNode, "/order/header/buyer-secondary-phone-type/text()");
      if(!phone.equals(""))
      {
        buyerPhones.setPhoneNumberType(phoneNumberType);
      }
      
      smsOptIn = this.getSingleNode(xmlNode, "/order/header/buyer-secondary-sms-optin-status/text()");
      if(!phone.equals(""))
      {
        buyerPhones.setSmsOptIn(smsOptIn);
      }
      
      if(!phone.equals("") || !ext.equals(""))
      {
    	  buyerPhones.setPhoneType(configUtil.getProperty(OrderGatherConstants.GATHERER_CONFIG_FILE, "home"));
          lBuyerPhones.add(buyerPhones);  
      }  

      //set fax number
      buyerPhones = new BuyerPhonesVO();

      nodeValue = this.getSingleNode(xmlNode, "/order/header/buyer-fax/text()");
      if(!nodeValue.equals(""))
      {
        buyerPhones.setPhoneNumber(nodeValue);
        buyerPhones.setPhoneType(configUtil.getProperty(OrderGatherConstants.GATHERER_CONFIG_FILE, "fax"));
        lBuyerPhones.add(buyerPhones);
      }

     }
     catch(Exception ex)
     {
        logger.error("Error mapping the xml to the buyerPhones value object: " + ex.toString()); 
     }
   
     return lBuyerPhones;
 }

 /**
  * Get buyer email information associated with the order.
  * 
  * @param xmlNode XMLNode 
  * @return List of buyer email value objects
  */
 private List getBuyerEmail(Node xmlNode)
 {
    List lBuyerEmail = new ArrayList();
    BuyerEmailsVO buyerEmail = new BuyerEmailsVO();
    String nodeValue = "";
    
    try{

      nodeValue = this.getBuyerEmailAddress(xmlNode);
      buyerEmail.setEmail(nodeValue);

      if(!nodeValue.equals(""))
        buyerEmail.setPrimary("Y");
        
      nodeValue = this.getSingleNode(xmlNode, "/order/header/news-letter-flag/text()");
      buyerEmail.setNewsletter(nodeValue);
                  
      lBuyerEmail.add(buyerEmail);
      
     }
     catch(Exception ex)
     {
        logger.error("Error mapping the xml to the buyerEmail value object: " + ex.toString()); 
     }
   
     return lBuyerEmail;
 }
 
 
  /**
   * Returns the Buyer Email Address as a String. 
   * Returns null if the Address is not set or is empty. 
   * Unlike the getBuyerEmail method, this function just returns a string.
   * @param xmlNode
   */
  private String getBuyerEmailAddress(Node xmlNode)
  {
    String nodeValue = "";
    
    try{

      nodeValue = this.getSingleNode(xmlNode, "/order/header/buyer-email-address/text()");
      
      if(nodeValue == null || nodeValue.trim().length() == 0)
      {
        return null;
      }
      logger.debug("getBuyerEmailAddress: " + nodeValue.trim());
     
      return nodeValue.trim();      
     }
     catch(Exception ex)
     {
        logger.error("Error retrieving the Buyer Email: " + ex.toString()); 
     }  
     
     return null;
  }

 /**
  * Get credit card information associated with the order.
  * 
  * @param xmlNode XMLNode 
  * @return List of credit card value objects
  */
 private List getCreditCard(Node xmlNode)
 {
    List lCreditCard = new ArrayList();
    CreditCardsVO creditCard = null;
    String nodeValue = "";
    boolean haveCreditCardInfo = false;
    
    try{
      creditCard = new CreditCardsVO();
      nodeValue = this.getSingleNode(xmlNode, "/order/header/cc-type/text()");
      logger.info("nodeValue: " + nodeValue);
        
      //if origin equals fox or csp or mobile
      //get value of corresponding credit card type
      //from the gatherer_config file and 
      //set credit card type to that value.
      //Novator and csp send card_id in the transmission.
      if(!nodeValue.equals("")) { 
          if (origin.toLowerCase().equals(this.og_origin) || 
              origin.equalsIgnoreCase(configUtil.getProperty(OrderGatherConstants.GATHERER_CONFIG_FILE, "csp_origin_internet")) ||
              origin.equalsIgnoreCase(configUtil.getProperty(OrderGatherConstants.GATHERER_CONFIG_FILE, "mob_origin_internet")) ||
              origin.equalsIgnoreCase(configUtil.getProperty(OrderGatherConstants.GATHERER_CONFIG_FILE, "tablet_origin_internet"))  ||
              origin.equalsIgnoreCase(configUtil.getProperty(OrderGatherConstants.GATHERER_CONFIG_FILE, "roses_origin_internet")) ||
              origin.equalsIgnoreCase(configUtil.getProperty(OrderGatherConstants.GATHERER_CONFIG_FILE, "mobile_origin_gcp")) ||
              origin.equalsIgnoreCase(configUtil.getProperty(OrderGatherConstants.GATHERER_CONFIG_FILE, "tablet_origin_gcp")) ||
              origin.equalsIgnoreCase(configUtil.getProperty(OrderGatherConstants.GATHERER_CONFIG_FILE, "web_origin_gcp"))
              )
          {
              //Check to see if credit card type passed over
              //matches one that's stored in the gatherer_config_file.
              //If it finds a match, then use the value from the config
              //file to set the credit card type.  If a match is not found
              //then just set credit card type equal to the value that
              //was sent in the xml transmission file.
              logger.info("Converting credit card");
              String creditCardType = configUtil.getProperty(OrderGatherConstants.GATHERER_CONFIG_FILE, nodeValue);
              if(creditCardType != null && !creditCardType.equals(""))
                  creditCard.setCCType(creditCardType);
              else
                  creditCard.setCCType(nodeValue);
              haveCreditCardInfo = true;
          } else {
              creditCard.setCCType(nodeValue);
              haveCreditCardInfo = true;
          }
      }
      logger.info("ccType: " + creditCard.getCCType());
             
      //if origin equals csp, retrieve the credit card value from the pop schema
      if(!nodeValue.equals("") && nodeValue.startsWith("*"))
      {
          OrderGathererDAO ogDAO = new OrderGathererDAO(this.connection);
          //call stored proc divya created
          creditCard.setCCNumber(ogDAO.getCSPCreditCardNumber(this.masterOrderNumber));
          haveCreditCardInfo = true; 
      }
      else
      {
          nodeValue = this.getSingleNode(xmlNode, "/order/header/cc-number/text()");
          if(!nodeValue.equals(""))
          {
            creditCard.setCCNumber(nodeValue);
            haveCreditCardInfo = true;
          }
      }
      nodeValue = this.getSingleNode(xmlNode, "/order/header/cc-exp-date/text()");
      if(!nodeValue.equals(""))
      {
        creditCard.setCCExpiration(nodeValue);
        haveCreditCardInfo = true;
      }
      nodeValue = this.getSingleNode(xmlNode, "/order/header/cc-encrypted/text()");
      if (!nodeValue.equals("")) {
          creditCard.setCcEncrypted(nodeValue);
      }

      if(haveCreditCardInfo)
        lCreditCard.add(creditCard);
      
     }
     catch(Exception ex)
     {
        logger.error("Error mapping the xml to the credit card value object: " + ex.toString()); 
     }
   
     return lCreditCard;
 }

 /**
  * Get payment information associated with the order.
  * 
  * @param xmlNode XMLNode 
  * @return List of payment value objects
  */
    private List getPayments(Node xmlNode) throws Exception
    {
        List lPayment = new ArrayList();
        PaymentsVO payment = null;
        CreditCardsVO creditCardVo = null;
        String nodeValue = "";
        String altPayPaymentType = null;
        NodeList numberNodes = null;
        NodeList amountNodes = null;
        NodeList ptypeNodes = null;
        NodeList authidNodes = null;
        NodeList gcNodeList = null;
        NodeList pinNodes = null;
        NodeList cardSpecificGroupNodes = null;
        Node itemNode = null;
        Node paymentTypeNode = null;
        boolean haveCreditCardInfo = false;
        boolean haveCreditCardAuthInfo = false;
        boolean haveGiftCertificateInfo = false;
        boolean isBillMeLater = false;
        String cardinalVerifiedFlag = "";
        OrderGatherUtils ogUtils = new OrderGatherUtils();
        OrderGathererDAO ogDao = new OrderGathererDAO(this.connection);
    
        try
        {
            //set credit card information
            //check to see if there is any information coming for
            //credit cards, gift certificates or aafes.
            payment = new PaymentsVO();
            List creditCard = new ArrayList();
            
            //check order number for valid prefixes
            Iterator it = null;
            String key = null;
            String value = null;
            //load list with alt pay method partners from gatherer_config file
            ArrayList altPayPartners = new ArrayList();
            Map map = configUtil.getProperties(OrderGatherConstants.GATHERER_CONFIG_FILE);
            it = map.keySet().iterator();

            while(it.hasNext())
            {
               key = (String)it.next();
               value = (String)map.get(key);

               if(key.startsWith("alt_pay_method_"))
               {
                   altPayPartners.add(value);
               }
            }
       
            // First check for invoice and alternative (PayPal, BillMeLater) payments    
            nodeValue = this.getSingleNode(xmlNode, "/order/header/cc-type/text()");
            altPayPaymentType = this.getSingleNode(xmlNode, "/order/header/alt-pay-method/payment-type/text()");
                        
            if(nodeValue != null && (nodeValue.equalsIgnoreCase("IN") || nodeValue.equalsIgnoreCase("PC")))
            {
                payment = new PaymentsVO();
                payment.setPaymentsType(configUtil.getProperty(OrderGatherConstants.GATHERER_CONFIG_FILE, "invoice"));
                lPayment.add(payment);
            }
            else if (altPayPaymentType != null && !altPayPaymentType.equals(""))
            {
                if(altPayPaymentType.equalsIgnoreCase("BM")) {
                    isBillMeLater = true; 
                } else if (altPayPaymentType.equals("UA")) {
                    // Monitor redemption rate. If order redemption rate differs from current rate, send system message.
                    String orderRdmptnRateStr = null;
                    BigDecimal currentRdmptnRate = null;
                    BigDecimal orderRdmptnRate = null;
                    try {
                        orderRdmptnRateStr = this.getSingleNode(xmlNode, "/order/header/alt-pay-method/redemption-rate/text()");
                        orderRdmptnRate = new BigDecimal(orderRdmptnRateStr);
                        currentRdmptnRate = ogDao.getRdmptnRateBySource(order.getSourceCode());
                    } catch (Exception e) {
                        ogUtils.sendMessage("Received invalid redemption rate in Order Gatherer: " + this.getSingleNode(xmlNode, "master-order-number/text()"));
                    }
                    try {
                        if(orderRdmptnRate.compareTo(currentRdmptnRate) != 0) {
                            ogUtils.sendMessage("Received different redemption rate in Order Gatherer: " + this.getSingleNode(xmlNode, "master-order-number/text()"));
                        }
                    } catch (Exception e) {
                        ogUtils.sendMessage("UA redemption rate monitor failed: " + this.getSingleNode(xmlNode, "master-order-number/text()"));
                    }
                }
                String altPayPartner = null;
                for (int i=0; i<altPayPartners.size(); i++)
                {
                  altPayPartner = altPayPartners.get(i).toString();

                  if (altPayPaymentType.equalsIgnoreCase(altPayPartner))
                  {
                    //if this is an alternate payment type partner, set retrieve alt pay info and set value object values
                     payment.setPaymentsType(this.getSingleNode(xmlNode, "/order/header/alt-pay-method/payment-type/text()"));
                     payment.setApAccount(this.getSingleNode(xmlNode, "/order/header/alt-pay-method/account-number/text()"));
                     payment.setApAuth(this.getSingleNode(xmlNode, "/order/header/alt-pay-method/auth-id/text()"));
                     payment.setAcqReferenceNumber(this.getSingleNode(xmlNode, "/order/header/alt-pay-method/order-number/text()"));
                     payment.setAmount(this.getSingleNode(xmlNode, "/order/header/alt-pay-method/approval-amount/text()"));
                     payment.setOrigAuthAmount(this.getSingleNode(xmlNode, "/order/header/alt-pay-method/approval-amount/text()"));
                     payment.setOrigMilesPointsAmt(this.getSingleNode(xmlNode, "/order/header/alt-pay-method/miles-points/text()"));
                     payment.setMilesPointsAmt(this.getSingleNode(xmlNode, "/order/header/alt-pay-method/miles-points/text()"));
                     lPayment.add(payment);
                  }
                }
            }
            else
            {
                creditCard = this.getCreditCard(xmlNode);
                payment.setCreditCards(creditCard);                
            }

            if(creditCard != null && creditCard.size() != 0)
            {
                haveCreditCardInfo = true;
        
                if (!nodeValue.equals("")) {
                    if((origin.toLowerCase().equals(this.og_origin) || 
                        origin.equalsIgnoreCase(configUtil.getProperty(OrderGatherConstants.GATHERER_CONFIG_FILE, "csp_origin_internet")) ||
                        origin.equalsIgnoreCase(configUtil.getProperty(OrderGatherConstants.GATHERER_CONFIG_FILE, "mob_origin_internet")) ||
                        origin.equalsIgnoreCase(configUtil.getProperty(OrderGatherConstants.GATHERER_CONFIG_FILE, "tablet_origin_internet"))  ||
                        origin.equalsIgnoreCase(configUtil.getProperty(OrderGatherConstants.GATHERER_CONFIG_FILE, "roses_origin_internet")) ||
                        origin.equalsIgnoreCase(configUtil.getProperty(OrderGatherConstants.GATHERER_CONFIG_FILE, "mobile_origin_gcp")) ||
                        origin.equalsIgnoreCase(configUtil.getProperty(OrderGatherConstants.GATHERER_CONFIG_FILE, "tablet_origin_gcp")) ||
                        origin.equalsIgnoreCase(configUtil.getProperty(OrderGatherConstants.GATHERER_CONFIG_FILE, "web_origin_gcp"))
                        )
                    )
                    {
                        payment.setPaymentsType(configUtil.getProperty(OrderGatherConstants.GATHERER_CONFIG_FILE, nodeValue));
                        haveCreditCardInfo = true;
                    } else {
                        payment.setPaymentsType(nodeValue);
                        haveCreditCardInfo = true;
                    }
                }
            }
            
            nodeValue = this.getSingleNode(xmlNode, "/order/header/token-id/text()");
            if(!nodeValue.equals(""))
            {
                payment.setTokenId(nodeValue);
                haveCreditCardInfo = true;
            }

            nodeValue = this.getSingleNode(xmlNode, "/order/header/authorization-transaction-id/text()");
            if(!nodeValue.equals(""))
            {
                payment.setAuthTransactionId(nodeValue);
            }

            nodeValue = this.getSingleNode(xmlNode, "/order/header/cc-approval-code/text()");
            if(!nodeValue.equals(""))
            {
                payment.setAuthNumber(nodeValue);
                haveCreditCardAuthInfo = true;
            }

            nodeValue = this.getSingleNode(xmlNode, "/order/header/cc-approval-amt/text()");
            if(!nodeValue.equals(""))
            {
                payment.setAmount(nodeValue);
                haveCreditCardAuthInfo = true;
            }

            nodeValue = this.getSingleNode(xmlNode, "/order/header/cc-approval-verbage/text()");
            if(!nodeValue.equals(""))
            {
                payment.setAuthResult(nodeValue);
                haveCreditCardAuthInfo = true;
            }

            nodeValue = this.getSingleNode(xmlNode, "/order/header/cc-avs-result/text()");
            if(!nodeValue.equals(""))
            {
                payment.setAvsCode(nodeValue);
                haveCreditCardAuthInfo = true;
            }
      
            nodeValue = this.getSingleNode(xmlNode, "/order/header/cc-acq-data/text()");
            if( !nodeValue.equals("") && !isBillMeLater)
            {
                payment.setAcqReferenceNumber(nodeValue);
                haveCreditCardAuthInfo = true;
            }     

            nodeValue = this.getSingleNode(xmlNode, "/order/header/aafes-ticket-number/text()");
            if(!nodeValue.equals(""))  
            {
                payment.setAafesTicket(nodeValue);
                haveCreditCardAuthInfo = true;
            }
            
            nodeValue = this.getSingleNode(xmlNode, "/order/header/csc-response-code/text()");
            if(!nodeValue.equals(""))
            {
                payment.setCscResponseCode(nodeValue);
                haveCreditCardAuthInfo = true;
            }

            nodeValue = this.getSingleNode(xmlNode, "/order/header/csc-validated-flag/text()");
            if(!nodeValue.equals(""))
            {
                payment.setCscValidatedFlag(nodeValue);
            }

            nodeValue = this.getSingleNode(xmlNode, "/order/header/csc-failure-count/text()");
            if(!nodeValue.equals(""))
            {
                try 
                {
                    payment.setCscFailureCount(Integer.parseInt(nodeValue));
                } 
                catch (Exception e)
                {
                    logger.error("Invalid CSC failure count: " + nodeValue);
                    payment.setCscFailureCount(0);
                }
            }
            else
            {
                payment.setCscFailureCount(0);
            }

            nodeValue = this.getSingleNode(xmlNode, "/order/header/wallet-indicator/text()");
            if(!nodeValue.equals(""))
            {
                payment.setWalletIndicator(nodeValue);
            }
            
            nodeValue = this.getSingleNode(xmlNode, "/order/header/authorization-route/text()");
            if(!nodeValue.equals("")){
                payment.setRoute(nodeValue);
            }
            
            //User story 2699 STORE THE CARDINAL DATA ELEMENTS
            
            StringBuffer missingCardinalTokens = new StringBuffer();
            
            String cavv = this.getSingleNode(xmlNode, "/order/header/cardinal-commerce/cavv/text()");
            String xid =  this.getSingleNode(xmlNode, "/order/header/cardinal-commerce/xid/text()");
            String eci =  this.getSingleNode(xmlNode, "/order/header/cardinal-commerce/eci/text()");
            String ucaf =  this.getSingleNode(xmlNode, "/order/header/cardinal-commerce/ucaf/text()");
            
            String ccType = this.getSingleNode(xmlNode, "/order/header/cc-type/text()");            
            payment.setCardinalVerifiedFlag("N");
            
            // If the Route is null, Payment Authorize, Settled and Refunded as existing.
            nodeValue = this.getSingleNode(xmlNode, "/order/header/is-cardinal-verified/text()");
            payment.setCardinalVerifiedFlag(nodeValue);
            
            if("Y".equalsIgnoreCase(nodeValue)){
	            if(StringUtils.isEmpty(cavv)){
	            	missingCardinalTokens.append("cavv, ");
	            }
	            if(StringUtils.isEmpty(xid)){
	            	missingCardinalTokens.append("xid, ");
	            }
	            if(StringUtils.isEmpty(eci)){
	            	missingCardinalTokens.append("eci, ");
	            }
	            
	            if( "Master".equalsIgnoreCase(ccType) && StringUtils.isEmpty(ucaf) ){
	            	missingCardinalTokens.append("ucaf, ");
	            }
	            
	            if(!StringUtils.isEmpty(missingCardinalTokens.toString())){
	            	payment.setCardinalVerifiedFlag("N");
	            	int index = missingCardinalTokens.toString().lastIndexOf(",");
	            	String tokens = missingCardinalTokens.toString().substring(0, index);
	            	ogUtils.sendMessage("Missing Cardinal Commerce element data for the Master Order Number:: "+ masterOrderNumber + " and Missing Tokens :: "+tokens);
	            }
            }
            
            logger.info("cardinal flag received: "+payment.getCardinalVerifiedFlag());
            
            //End 2699

            //defect#16 BAMS integration
            nodeValue = this.getSingleNode(xmlNode, "/order/header/cc-auth-provider/text()");
            if (StringUtils.isEmpty(nodeValue) && payment.getCardinalVerifiedFlag() != null &&
                    payment.getCardinalVerifiedFlag().equalsIgnoreCase("Y") &&
                    payment.getTokenId() != null) {
                logger.info("Setting auth provider to BAMS because Cardinal nodes exist");
                nodeValue = "BAMS";
            }
            if(nodeValue != null && !nodeValue.equals(""))
            {
                payment.setCcAuthProvider(nodeValue);
                if("BAMS".equalsIgnoreCase(nodeValue)){
                	Map<String,Object> paymentExtMap = new HashMap<String,Object>();
                	nodeValue = this.getSingleNode(xmlNode, "/order/header/transmission-date-time/text()");
                	if(nodeValue != null && !nodeValue.equals("")){
                		paymentExtMap.put("TrnmsnDateTime", nodeValue);
                	}
                	nodeValue = this.getSingleNode(xmlNode, "/order/header/cc-system-trace-audit-number/text()");
                	if(nodeValue != null && !nodeValue.equals("")){
                		paymentExtMap.put("STAN", nodeValue);
                	}
                	nodeValue = this.getSingleNode(xmlNode, "/order/header/cc-ref-num/text()");
                	if(nodeValue != null && !nodeValue.equals("")){
                		paymentExtMap.put("RefNum", nodeValue);
                	}
                	nodeValue = this.getSingleNode(xmlNode, "/order/header/cardholder-activated-terminal/text()");
                	if(nodeValue != null && !nodeValue.equals("")){
                		paymentExtMap.put("TermCatCode", nodeValue);
                	}                	
            		nodeValue = this.getSingleNode(xmlNode, "/order/header/visa-pos-condition-code/text()");
            		if(nodeValue != null && !nodeValue.equals("")){
                		paymentExtMap.put("POSCondCode", nodeValue);
                	}
                	nodeValue = this.getSingleNode(xmlNode, "/order/header/cc-approval-amt/text()");
                	if(nodeValue != null && !nodeValue.equals("")){
                		paymentExtMap.put("TxnAmt", nodeValue);
                	}
                	nodeValue = this.getSingleNode(xmlNode, "/order/header/cc-trans-crncy/text()");
                	if(nodeValue != null && !nodeValue.equals("")){
                		paymentExtMap.put("TxnCrncy", nodeValue);
                	}                	
            		nodeValue = this.getSingleNode(xmlNode, "/order/header/cc-response-code/text()");
            		if(nodeValue != null && !nodeValue.equals("")){
                		paymentExtMap.put("RespCode", nodeValue);
                	}
            		
            		//User Story 2699
            		
            		if(cavv != null && !cavv.equals("")){
                		paymentExtMap.put("Cavv", cavv);
                	}
            		
            		if(xid != null && !xid.equals("")){
                		paymentExtMap.put("Xid", xid);
                	}
            		
            		if(eci != null && !eci.equals("")){
                		paymentExtMap.put("Eci", eci);
                	}
            		
            		//End User Story 2699
            		
            		if(ucaf != null && !ucaf.equals("")){
                		paymentExtMap.put("Ucaf", ucaf);
                	}
            		
                	cardSpecificGroupNodes = DOMUtil.selectNodes(xmlNode, "/order/header/card-specific-group/card-specific-detail");
                	if(cardSpecificGroupNodes!=null && cardSpecificGroupNodes.getLength()!=0){
                		Map<String,String> cardSpecificDetailsMap = new HashMap<String,String>();
                		for(int i=0, length=cardSpecificGroupNodes.getLength(); i<length; i++){
                			Node node = cardSpecificGroupNodes.item(i);
                			cardSpecificDetailsMap.put(node.getAttributes().getNamedItem("name").getNodeValue(), node.getTextContent());
                    	}
                		paymentExtMap.put(PaymentExtensionConstants.CARD_SPECIFIC_DETAIL,cardSpecificDetailsMap);
                	}
                	payment.getPaymentExtMap().putAll(paymentExtMap);                	                	
                    logger.info("paymentExtMap.size(): " + paymentExtMap.size());
                }               
            }
            
            
            //Added nullpointer check to fix defect 18523
            if(payment != null && payment.getPaymentType()!= null && !payment.getPaymentType().equals("IN")) //fix for 17917
            {
            	if( haveCreditCardInfo || haveCreditCardAuthInfo )
            		lPayment.add(payment);
            } else if (payment.getTokenId() != null) {
                lPayment.add(payment);
            }

            //set aafes ticket information
            /*
            payment = new PaymentsVO();

            nodeValue = this.getSingleNode(xmlNode, "/order/header/aafes-ticket-number/text()");
            if(!nodeValue.equals(""))
            {
                payment.setAafesTicket(nodeValue);
                payment.setPaymentsType(configUtil.getProperty(OrderGatherConstants.GATHERER_CONFIG_FILE, "aafesTicket"));
                  
                lPayment.add(payment);
        
                haveAafesInfo = true;
            }
            */

             nodeValue = this.getSingleNode(xmlNode, "/order/header/no-charge/type/text()");
             // If no charge payment
             if(nodeValue != null && !"".equals(nodeValue))
             {
                 payment = new PaymentsVO();
                 payment.setPaymentsType(configUtil.getProperty(OrderGatherConstants.GATHERER_CONFIG_FILE, "no_charge"));
                 payment.setNcType(nodeValue);
                 payment.setNcReason(this.getSingleNode(xmlNode, "/order/header/no-charge/reason/text()"));
                 payment.setNcApprovalCode(this.getSingleNode(xmlNode, "/order/header/no-charge/approval-id/text()"));
                 payment.setNcOrderReference(this.getSingleNode(xmlNode, "/order/header/no-charge/order-reference/text()"));
                 payment.setAmount(this.getSingleNode(xmlNode, "/order/header/no-charge/amount/text()"));
                 lPayment.add(payment);
             }

            // Set gift certificate/card information
            // NOTE this logic assumes there is only one gift certificate/card (as specified in requirements).  If multiple
            // are ever allowed, this will need to be modified.
            //
            itemNode =  DOMUtil.selectSingleNode(xmlNode, "/order/header/gift-certificates/gift-certificate/number/text()");
            paymentTypeNode =  DOMUtil.selectSingleNode(xmlNode, "/order/header/gift-certificates/gift-certificate/payment-type/text()");
            if(itemNode != null || paymentTypeNode != null)
            {
            	gcNodeList  = DOMUtil.selectNodes(xmlNode, "/order/header/gift-certificates/gift-certificate");
                numberNodes = DOMUtil.selectNodes(xmlNode, "/order/header/gift-certificates/gift-certificate/number/text()");
                amountNodes = DOMUtil.selectNodes(xmlNode, "/order/header/gift-certificates/gift-certificate/amount/text()");
            	ptypeNodes  = DOMUtil.selectNodes(xmlNode, "/order/header/gift-certificates/gift-certificate/payment-type/text()");
            	authidNodes = DOMUtil.selectNodes(xmlNode, "/order/header/gift-certificates/gift-certificate/transaction-id/text()");
            	pinNodes 	= DOMUtil.selectNodes(xmlNode, "/order/header/gift-certificates/gift-certificate/pin/text()");
            	
            	String gdType = configUtil.getProperty(OrderGatherConstants.GATHERER_CONFIG_FILE, "giftCard");
                String payType = null;
                String authId = null;
          
                for(int i=0; i<gcNodeList.getLength(); i++)
                {
                    payment = new PaymentsVO();
                    payment.setAmount(amountNodes.item(i).getNodeValue());
                	
                    if (ptypeNodes != null && ptypeNodes.item(i) != null) {
                    	payType = ptypeNodes.item(i).getNodeValue();
                    }
                	
                    if (authidNodes != null && authidNodes.item(i) != null) {
                    	authId = authidNodes.item(i).getNodeValue();
                    }

                    
                    
                	// Gift Card Payment
                    //
                    if (payType != null && payType.equalsIgnoreCase(gdType)) {
                    	if(numberNodes != null && numberNodes.item(i) != null)
                    	{
                    		payment.setGiftCertificateId(getMaskedGiftCardNumber(numberNodes.item(i).getNodeValue()));
                    	}
                    	payment.setPaymentsType(payType);
                    	payment.setOrigAuthAmount(payment.getAmount());
                    	if (authId != null) {
                    		payment.setAuthResult(configUtil.getProperty(OrderGatherConstants.GATHERER_CONFIG_FILE, "authApproved"));
                    		payment.setAuthNumber(authId);
                    		payment.setApAuth(authId);
                    		payment.setCscValidatedFlag("Y");
                    	}
                    	creditCardVo = new CreditCardsVO();
                    	creditCardVo.setCCType(gdType);
                    	if(numberNodes != null && numberNodes.item(i) != null)
                    	{
                    		creditCardVo.setCCNumber(numberNodes.item(i).getNodeValue());
                    	}
                    	if(pinNodes != null && pinNodes.item(i) != null)
                    	{	
                    		creditCardVo.setPin(pinNodes.item(i).getNodeValue());
                    	}
                    	List<CreditCardsVO> ccList = new ArrayList<CreditCardsVO>();
                        ccList.add(creditCardVo);
                        payment.setCreditCards(ccList);
                    	
                  	// Gift Certificate
                    //	
                    }
                    else if ((payType == null || payType.equals(""))) {
                    	if(numberNodes != null && numberNodes.item(i) != null)
                    	{
                    		payment.setGiftCertificateId(numberNodes.item(i).getNodeValue());
                    	}
                        payment.setPaymentsType(configUtil.getProperty(OrderGatherConstants.GATHERER_CONFIG_FILE, "giftCertificate"));
                        haveGiftCertificateInfo = true;
                    }
                    
                    lPayment.add(payment);
                }
            }       
        }
        catch(Exception ex)
        {
            logger.error("Error mapping the xml to the payment value object: " + ex.toString());
            ex.printStackTrace();  // GPS 
        }
   
        return lPayment;
    }

private String getMaskedGiftCardNumber(String gcNum) {
	String retVal = "";
	int maskLength = 0;
	int strLength = 0;	
	if (gcNum != null) {
		if (gcNum.length() >= 4) {
			strLength = gcNum.length();
			maskLength = gcNum.length() - 4;
			String lastFour = gcNum.substring(maskLength);
			retVal = StringUtils.leftPad(lastFour, strLength, "*");
		} else {
			retVal = gcNum;
		}
	}
	return retVal;
}
    
 /**
 * Get the value from the xmlNode 
 * 
 * @param xmlNode XMLNode
 * @param nodeName String
 * @return String
 */
private String getSingleNode(Node xmlNode, String nodeName)
{
    Node itemNode = null;
    String nodeValue = "";
    
    try
    {
      itemNode =  DOMUtil.selectSingleNode(xmlNode, nodeName);
      if(itemNode != null)
      {
        itemNode = DOMUtil.selectSingleNode(xmlNode, nodeName);
        nodeValue = itemNode.getNodeValue();
      } 

    }
    catch(Exception ex)
    {
      logger.error("Error getting the node.  Node may not exist in xml file. " + ex.toString()); 
    }
 
  return nodeValue; 
}

 /**
   * Get the cobrands associated with an order
   * 
   * @param xmlNode XMLNode  
   * @return List of CoBrand value objects
   */
  private List getCoBrands(Node xmlNode)
  {
    CoBrandVO coBrand = null;
    List lCoBrands = new ArrayList();
    NodeList name = null;
    NodeList data = null;
    Node itemNode = null;

    try
     {
       itemNode =  DOMUtil.selectSingleNode(xmlNode, "/order/header/co-brands/co-brand/name/text()");
       if(itemNode != null)
       {
          name = DOMUtil.selectNodes(xmlNode, "/order/header/co-brands/co-brand/name/text()");
          data = DOMUtil.selectNodes(xmlNode, "/order/header/co-brands/co-brand/data/text()");
        
          for(int i=0; i<name.getLength(); i++)
          {
            coBrand = new CoBrandVO();
            coBrand.setInfoName(name.item(i).getNodeValue());
            coBrand.setInfoData(data.item(i).getNodeValue()); 
            lCoBrands.add(coBrand);
          }
       }
     }
     catch(Exception ex)
     {
        logger.error("Error mapping the xml to the CoBrand value object: " + ex.toString()); 
     }
   
     return lCoBrands;
  }

    /**
    * Get any Order Extensions that are associated with the order details.
    * 
    * @param xmlNode XMLNode 
    * @return List of order extensions value objects
    */
  private List <OrderExtensionsVO> getOrderExtensions(Node xmlNode)
  {
    OrderExtensionsVO orderExtensionsVO = null;
    List <OrderExtensionsVO> orderExtensions  = new ArrayList <OrderExtensionsVO> ();
    NodeList name = null;
    NodeList value = null;
    Node itemNode = null;

    try
     {
       itemNode =  DOMUtil.selectSingleNode(xmlNode, "order-extensions/extension/name/text()");
       if(itemNode != null)
       {
          name = DOMUtil.selectNodes(xmlNode, "order-extensions/extension/name/text()");
          value = DOMUtil.selectNodes(xmlNode, "order-extensions/extension/value/text()");
          
          for(int i=0; i<name.getLength(); i++)
          {
            orderExtensionsVO = new OrderExtensionsVO();
            orderExtensionsVO.setInfoName(name.item(i).getNodeValue());
            orderExtensionsVO.setInfoValue(value.item(i).getNodeValue()); 
            orderExtensions.add(orderExtensionsVO);
          }
       }
     }
     catch(Exception ex)
     {
        logger.error("Error mapping the xml to the OrderExtensions value object: " + ex.toString()); 
     }
   
     return orderExtensions;
  }

	/** Get any Order Detail Extensions that are associated with the order
	 * details. 
	 * @param xmlNode XMLNode
	 * @return List of order detail extensions value objects
	 */
	private List<OrderDetailExtensionsVO> getOrderDetailExtensions(Node xmlNode) { 
		List<OrderDetailExtensionsVO> lext = new ArrayList<OrderDetailExtensionsVO>();		
		try {
			Document newDoc = DOMUtil.getDefaultDocument();
			Node node = newDoc.importNode(xmlNode, true);
			newDoc.appendChild(node);

			newDoc.getDocumentElement().normalize();			
			NodeList nList = newDoc.getElementsByTagName("item-extensions");
			if(nList != null && nList.getLength() > 0 ) {
				nList = newDoc.getElementsByTagName("extension");
			}			
			
			for (int i = 0; i < nList.getLength(); i++) {
				Node nNode = nList.item(i);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					OrderDetailExtensionsVO ext = new OrderDetailExtensionsVO();
					Element eElement = (Element) nNode;
					ext = new OrderDetailExtensionsVO();
					ext.setInfoName(eElement.getElementsByTagName("name").item(0).getTextContent());
					ext.setInfoData(eElement.getElementsByTagName("data").item(0) == null ? "" : eElement.getElementsByTagName("data").item(0).getTextContent());
					lext.add(ext);
				}
			}

		} catch (Exception ex) {
			logger.error("Error mapping the xml to the OrderDetailExtensions value object: " + ex.toString());
		}
		return lext;
	}
  
    /**
     * Returns personalization data as xml
     * Information in XML file is of the format:
     *   <item>
     *     <personalizations>
     *        <personalization>
     *            <name>field name</name>
     *            <data>field data</data>
     *        <personalization>
     *     <personalizations>
     *   </item>
     * CDATA sections are not preserved as part of the existing
     * order xml parsing.  A second document was created
     * to preserve the personalization CDATA sections.
     * The second document could have been processed
     * at the same time as the original but instead is iterated
     * through to avoid risk regarding DocumentBuilder differences.  
     * This processing only occurs if personalization information exists.
     * 
     * @param xmlNode The XML node containing the <item> information.
     * @return a string representing item personalization xml.
     */
    private String getOrderDetailPersonalizations(Node xmlNode) {            
        String personalizations = null;
        String currentOrder = null;
        String order = null;
        Node node = null;
        NodeList nl = null;
        boolean found = false;
              
        try {
            // If no personalizations exist skip processing
            nl = DOMUtil.selectNodes(xmlNode, "personalizations/personalization");
            if(nl.getLength() == 0)
                return null;
            
            // Obtain current order number
            currentOrder = this.getSingleNode(xmlNode, "order-number/text()");
            
            // Obtain items containing preserved CDATA sections
            nl = DOMUtil.selectNodes(orderXMLCDATADocument, "/order/items/item");
            for(int i = 0; i < nl.getLength() && !found; i++)
            {
                node = nl.item(i);
                order = this.getSingleNode(node, "order-number/text()");

                if(logger.isDebugEnabled())
                {
                    logger.debug("Current order is " + currentOrder);
                    logger.debug("Order is " + order);
                }

                // If order equals the current order process the personalization
                if(order.equals(currentOrder))
                {
                    found = true;
                    nl = DOMUtil.selectNodes(node, "personalizations");
                    // If more than one personalizations node exists log an error
                    if(nl.getLength() > 1) 
                    {
                        logger.error("Multiple personalizations exist.  Only one personalization node can exist.  Please check the XML received.");
                    }
                    else if(nl.getLength() > 0) 
                    {
                        StringWriter sw = new StringWriter();
                        node = nl.item(0);
                        DOMUtil.print(node, sw);
                        personalizations = sw.toString();
                        
                        if(logger.isDebugEnabled())
                            logger.debug("Personalizations: " + personalizations);
                    }
                }
            }
        }
        catch(Exception ex) {
            logger.error("Error obtaining personalizations: " + ex.toString()); 
        }
         
        return personalizations;
    }
    
  /**
  * Get Add Ons that are associated with the order details.
  * 
  * @param xmlNode XMLNode 
  * @return List of add on value objects
  */
 private List getAddOns(Node xmlNode)
 {
    AddOnsVO addOn = null;
    List lAddOns = new ArrayList();
    NodeList id = null;
    NodeList quantity = null;
    NodeList text = null;
    NodeList price = null;
    NodeList discountAmount = null;
    NodeList addOns = null;
    Node addOnNode = null;
    BigDecimal addonTotal = new BigDecimal(0);
    
    try
     {
         addOns = DOMUtil.selectNodes(xmlNode, "add-ons/add-on");
         for(int i=0; i<addOns.getLength(); i++)
         {
            addOnNode = addOns.item(i);
            
            id = DOMUtil.selectNodes(addOnNode, "id/text()");
            quantity = DOMUtil.selectNodes(addOnNode, "quantity/text()");
            text = DOMUtil.selectNodes(addOnNode, "text/text()");
            price = DOMUtil.selectNodes(addOnNode, "price/text()");
            discountAmount = DOMUtil.selectNodes(addOnNode, "discount-amount/text()");
            
            if(id.getLength() > 0)
            {
                addOn = new AddOnsVO();                
                addOn.setAddOnCode(id.item(0).getNodeValue());

                if(quantity.getLength() > 0)
                {
                     addOn.setAddOnQuantity(quantity.item(0).getNodeValue());
                }
                if(text.getLength() > 0)
                {
                  addOn.setText(text.item(0).getNodeValue());
                }
                if(price.getLength() > 0) 
                {
                  String value = price.item(0).getNodeValue();
                  addOn.setPrice(value);

                  // Total up addon prices for Roses.com.
                  // We need to do this since recalculate is not used on Roses.com orders. 
                  if (this.isRosesDotCom) {
                      try {
                          value = (value != null && value.trim().length() > 0)? value: "0";
                          BigDecimal addonPrice = new BigDecimal(value);
                          addonTotal = addonTotal.add(addonPrice);
                      } catch (NumberFormatException e) {
                          logger.error("Addon price for Roses.com order not a number: " + value);
                      }
                  }
                }
                if (discountAmount.getLength() > 0)
                {
                  addOn.setDiscountAmount(discountAmount.item(0).getNodeValue());
                }
                lAddOns.add(addOn);
            }
         }
         if (this.isRosesDotCom) {
             this.rosesDotComAddonTotal = addonTotal.setScale(2, 5).toString();             
         }
     }
     catch(Exception ex)
     {
        logger.error("Error mapping the xml to the AddOn value object: " + ex.toString()); 
     }
   
     return lAddOns;
    
 }

 /**
  * Get Recipients that are associated with the 
  * order details.
  * 
  * @param xmlNode XMLNode 
  * @return List of recipients value objects
  */
 private List getRecipients(Node xmlNode)
 {
    List lRecipients = new ArrayList();
    RecipientsVO recipients = null;
    String nodeValue = "";

    try
     {
        recipients = new RecipientsVO();

        nodeValue = this.getSingleNode(xmlNode, "recip-first-name/text()");
        recipients.setFirstName(nodeValue); 

        nodeValue = this.getSingleNode(xmlNode, "recip-last-name/text()");
        recipients.setLastName(nodeValue);

        List lRecipientsAddresses = this.getRecipientAddresses(xmlNode);
        recipients.setRecipientAddresses(lRecipientsAddresses);

        List lRecipientPhones = this.getRecipientPhone(xmlNode);
        recipients.setRecipientPhones(lRecipientPhones);

        recipients.setStatus(OrderGatherConstants.STATUS);

        lRecipients.add(recipients);
     }
     catch(Exception ex)
     {
        logger.error("Error mapping the xml to the Recipients value object: " + ex.toString()); 
     }
   
     return lRecipients;
 }

 /**
  * Get Recipient Addresses that are associated with the 
  * order details.
  * 
  * @param xmlNode XMLNode 
  * @return List of recipient addresses value objects
  */
 private List getRecipientAddresses(Node xmlNode)
 {
    List lRecipientAddresses = new ArrayList();
    RecipientAddressesVO recipientAddresses = null;
    String nodeValue = "";

    try
     {
        recipientAddresses = new RecipientAddressesVO(); 
       
        nodeValue = this.getSingleNode(xmlNode, "recip-address1/text()");
        recipientAddresses.setAddressLine1(nodeValue);

        nodeValue = this.getSingleNode(xmlNode, "recip-address2/text()");
        recipientAddresses.setAddressLine2(nodeValue);

        nodeValue = this.getSingleNode(xmlNode, "recip-city/text()");
        recipientAddresses.setCity(nodeValue);

        nodeValue = this.getSingleNode(xmlNode, "recip-state/text()");
        recipientAddresses.setStateProvince(nodeValue);

        nodeValue = this.getSingleNode(xmlNode, "recip-postal-code/text()");
        recipientAddresses.setPostalCode(nodeValue);

        nodeValue = this.getSingleNode(xmlNode, "recip-country/text()");
        recipientAddresses.setCountry(nodeValue);

        nodeValue = this.getSingleNode(xmlNode, "recip-international/text()");
        recipientAddresses.setInternational(nodeValue);

        nodeValue = this.getSingleNode(xmlNode, "ship-to-type/text()");
        recipientAddresses.setAddressType(nodeValue);

        nodeValue = this.getSingleNode(xmlNode, "ship-to-type-name/text()");
        recipientAddresses.setName(nodeValue);

        nodeValue = this.getSingleNode(xmlNode, "ship-to-type-info/text()");
        recipientAddresses.setInfo(nodeValue); 

        lRecipientAddresses.add(recipientAddresses);
                
     }
     catch(Exception ex)
     {
        logger.error("Error mapping the xml to the Recipient Address value object: " + ex.toString()); 
     }
   
     return lRecipientAddresses;
    
 }
 

 private AVSAddressVO getAvsAddress(Node xmlNode)
 {
     AVSAddressVO avsAddress = null;
     try
     {
         Element avsNode = (Element)xmlNode;
         // determine if there is an avs address to populate
         Element el  = JAXPUtil.selectSingleNode((Element)xmlNode, GeneralConstants.XML_TAG_ADDRESS_VERIFICATION);
         if (el == null)
             return null;
         
         avsAddress = new AVSAddressVO(); 

         avsAddress.setAvsPerformed(getChldElementValue(avsNode, GeneralConstants.XML_TAG_ADDRESS_VERIFICATION, GeneralConstants.XML_TAG_RECIP_ADDRESS_VERIFIED));
         // guard against empty nodes, etc.
         if (avsAddress.getAvsPerformed() == null)
             return null;
         
         avsAddress.setAddress(getChldElementValue(avsNode, GeneralConstants.XML_TAG_ADDRESS_VERIFICATION, GeneralConstants.XML_TAG_AVS_STREET_ADDRESS));
         avsAddress.setCity(getChldElementValue(avsNode, GeneralConstants.XML_TAG_ADDRESS_VERIFICATION, GeneralConstants.XML_TAG_AVS_CITY));
         avsAddress.setStateProvince(getChldElementValue(avsNode, GeneralConstants.XML_TAG_ADDRESS_VERIFICATION, GeneralConstants.XML_TAG_AVS_STATE));
         avsAddress.setPostalCode(getChldElementValue(avsNode, GeneralConstants.XML_TAG_ADDRESS_VERIFICATION, GeneralConstants.XML_TAG_AVS_POSTAL_CODE));
         avsAddress.setLatitude(getChldElementValue(avsNode, GeneralConstants.XML_TAG_ADDRESS_VERIFICATION, GeneralConstants.XML_TAG_AVS_LATITUDE));
         avsAddress.setLongitude(getChldElementValue(avsNode, GeneralConstants.XML_TAG_ADDRESS_VERIFICATION, GeneralConstants.XML_TAG_AVS_LONGITUDE));
         avsAddress.setCountry(getChldElementValue(avsNode, GeneralConstants.XML_TAG_ADDRESS_VERIFICATION, GeneralConstants.XML_TAG_AVS_COUNTRY));
         avsAddress.setEntityType(getChldElementValue(avsNode, GeneralConstants.XML_TAG_ADDRESS_VERIFICATION, GeneralConstants.XML_TAG_AVS_ENTITY_TYPE));
         avsAddress.setOverrideFlag(getChldElementValue(avsNode, GeneralConstants.XML_TAG_ADDRESS_VERIFICATION, GeneralConstants.XML_TAG_RECIP_ADDRESS_VERIFICATION_BYPASSED));
         avsAddress.setResult(getChldElementValue(avsNode, GeneralConstants.XML_TAG_ADDRESS_VERIFICATION, GeneralConstants.XML_TAG_RECIP_ADDRESS_VERIFICATION_RESULT));

         List <AVSAddressScoreVO> scores;
         scores = this.getAVSAddressScores(avsNode);
         avsAddress.setScores(scores);
     }
     catch(Exception ex)
     {
         logger.error("Error mapping the xml to the AVS Address value object: " + ex.toString()); 
     }

     return avsAddress;
 }
 
 private String getChldElementValue(Element element, String parentTag, String childTag) throws Exception
 {
     Element el = JAXPUtil.selectSingleNode(element, parentTag + "/" + childTag);
     if (el != null)
         return el.getTextContent();
     else
         return null;
 }

 private List<AVSAddressScoreVO> getAVSAddressScores(Node xmlNode) throws Exception
 {    
     List<AVSAddressScoreVO> scores = new ArrayList<AVSAddressScoreVO>();

     Node scoreNode = null;

     scoreNode = JAXPUtil.selectSingleNode((Element)xmlNode, GeneralConstants.XML_TAG_ADDRESS_VERIFICATION + "/" + GeneralConstants.XML_TAG_AVS_ADDRESS_SCORES);

     if(scoreNode == null)
         return null;

     NodeList nodes = scoreNode.getChildNodes();
     for (int x = 0; x < nodes.getLength(); x++)
     {
         Node score = nodes.item(x);
         if (score.getTextContent() != null && !score.getTextContent().trim().isEmpty())
         {
             addScore(scores, scoreNode, score.getNodeName(), score.getTextContent());
         }
     }
     
     if (scores.size() == 0)
         return null;

     return scores;
}

 private void addScore(List<AVSAddressScoreVO> scores, Node scoreNode, String nodeName, String value)
 {
     AVSAddressScoreVO score;
     score = new AVSAddressScoreVO();
     score.setReason(nodeName);
     score.setScore(value);
     scores.add(score);
 }

/**
  * Get Recipient Phones that are associated with the 
  * order details.
  * 
  * @param xmlNode XMLNode 
  * @return List of recipient phone value objects
  */
 private List getRecipientPhone(Node xmlNode)
 {
    List lRecipientPhone = new ArrayList();
    RecipientPhonesVO recipientPhones = null;
    String phone = "";
    String ext = "";

    try
     {
        recipientPhones = new RecipientPhonesVO(); 

        recipientPhones.setPhoneType(configUtil.getProperty(OrderGatherConstants.GATHERER_CONFIG_FILE, "work"));

        phone = this.getSingleNode(xmlNode, "recip-phone/text()");
        if(!phone.equals(""))
          recipientPhones.setPhoneNumber(phone);

        ext = this.getSingleNode(xmlNode, "recip-phone-ext/text()");
        if(!ext.equals(""))
          recipientPhones.setExtension(ext); 

        if(!phone.equals("") || !ext.equals(""))
          lRecipientPhone.add(recipientPhones);        
     }
     catch(Exception ex)
     {
        logger.error("Error mapping the xml to the Recipient Phone value object: " + ex.toString()); 
     }
   
     return lRecipientPhone;
    
 }

 /**
 * Populate and return List of Order Detail Value Objects that 
 * are associated with the order.
 * @param xmlNode XMLNode 
 * @return List
 */
private List getOrderDetails(Node xmlNode, String orderSourceCode)
{
     OrderDetailsVO orderDetail = null;
     List lOrderDetails = new ArrayList();
     String nodeValue = "";
     
     try{
          
          Document orderXMLDoc = orderXMLDocument;
          NodeList nl = DOMUtil.selectNodes(orderXMLDoc, "/order/items/item");
             
          for(int i=0; i<nl.getLength(); i++) 
          {
            orderDetail = new OrderDetailsVO();
           
            xmlNode = (Node) nl.item(i);

            String lineNumber = "";
            
            orderDetail.setLineNumber(lineNumber.valueOf(i+1));
            
            nodeValue = this.getSingleNode(xmlNode, "auto-renew/text()");
            
            if(nodeValue != null && nodeValue.length() != 0)
            {
              orderDetail.setAutoRenew(nodeValue);
            }
            else
              orderDetail.setAutoRenew("N");
              
            nodeValue = this.getSingleNode(xmlNode, "programs/free-shipping/text()");
            if(nodeValue != null && nodeValue.length() != 0)
            {
              orderDetail.setFreeShipping(nodeValue);
            }   
            else
              orderDetail.setFreeShipping("N");
            
            nodeValue = this.getSingleNode(xmlNode, "order-number/text()");
            orderDetail.setExternalOrderNumber(nodeValue); 

            nodeValue = this.sourceCodePreprocessor(xmlNode, "item-source-code/text()", order.getOrderOrigin());
            orderDetail.setSourceCode(nodeValue);
            String sourceCode = nodeValue;

            nodeValue = this.getSingleNode(xmlNode, "item-of-the-week-flag/text()");
            orderDetail.setItemOfTheWeekFlag(nodeValue);

            // Only exists for OE orders
            nodeValue = this.getSingleNode(xmlNode, "add-on-amount/text()");
            orderDetail.setAddOnAmount(nodeValue);

            nodeValue = this.getSingleNode(xmlNode, "order-total/text()");
            orderDetail.setExternalOrderTotal(nodeValue); 

            nodeValue = this.getSingleNode(xmlNode, "product-type/text()");
            orderDetail.setProductType(nodeValue);
            
            nodeValue = this.getSingleNode(xmlNode, "shipping-method/text()");
            orderDetail.setShipMethod(nodeValue);
            String shipMethod = nodeValue;

            BigDecimal extraShippingFee = new BigDecimal("0");
            BigDecimal dropShipCharges = new BigDecimal("0");
            BigDecimal shippingFeeAmount = new BigDecimal("0");
            BigDecimal serviceFeeAmount = new BigDecimal("0");
            BigDecimal additionalCharges = new BigDecimal("0");
            
            nodeValue = this.getSingleNode(xmlNode, "fulfillment-channel/text()");
            String fulfillmentChannel = nodeValue;
                        
            nodeValue = this.getSingleNode(xmlNode, "service-fee/text()");
            if (!StringUtils.isEmpty(nodeValue)) {
                serviceFeeAmount = new BigDecimal(nodeValue);
            }
            
            nodeValue = this.getSingleNode(xmlNode, "extra-shipping-fee/text()");
            if (!StringUtils.isEmpty(nodeValue)) {
              extraShippingFee = new BigDecimal(nodeValue);
            }
            nodeValue = this.getSingleNode(xmlNode, "drop-ship-charges/text()");
            if (!StringUtils.isEmpty(nodeValue)) {
              dropShipCharges = new BigDecimal(nodeValue);
            }
            shippingFeeAmount = extraShippingFee.add(dropShipCharges);

            nodeValue = this.getSingleNode(xmlNode, "product-type/text()");
            String productType = "";
            if (!StringUtils.isEmpty(nodeValue)) {
                productType = nodeValue.toUpperCase();
            }

            nodeValue = this.getSingleNode(xmlNode, "fuel-surcharge/text()");
            if (!StringUtils.isEmpty(nodeValue)) {
                // this will only come from the new website
                // we need to get fuel surcharge details
                SurchargeVO surchargeVO = null;
                try {
                    Calendar cal = Calendar.getInstance();
                    surchargeVO = FTDFuelSurchargeUtilities.getSurchargeVO(cal, sourceCode, connection);
                } catch (Exception e) {
                    logger.error("Could not get FuelSurchargeVO: " + e);
                    surchargeVO = new SurchargeVO();
                }
                orderDetail.setFuelSurcharge(nodeValue);
                BigDecimal fuelSurcharge = new BigDecimal(nodeValue);
                additionalCharges = additionalCharges.add(fuelSurcharge);
                orderDetail.setApplySurchargeCode("ON");
                orderDetail.setFuelSurchargeDescription(surchargeVO.getSurchargeDescription());
                orderDetail.setDisplaySurcharge(surchargeVO.getDisplaySurcharge());
                String sendToFloristFlag = surchargeVO.getSendSurchargeToFlorist();
                orderDetail.setSendSurchargeToFlorist(sendToFloristFlag);
                
                logger.info(productType + " " + shipMethod + " " + sendToFloristFlag + " " + orderDetail.getFuelSurcharge());
                if (productType.equals(PRODUCT_TYPE_FLORAL)) {
                    if (sendToFloristFlag.equals("Y")) {
                        shippingFeeAmount = shippingFeeAmount.add(fuelSurcharge);
                        logger.info("fuel surcharge added to shipping fee");
                   
                    } else {
                        serviceFeeAmount = serviceFeeAmount.add(fuelSurcharge);
                        logger.info("fuel surcharge added to service fee");
                    }
                } else if (productType.equals(PRODUCT_TYPE_SPECIALTY_GIFT) ||
                        productType.equals(PRODUCT_TYPE_SPECIALITY_GIFT)) {
                    shippingFeeAmount = shippingFeeAmount.add(fuelSurcharge);
                    logger.info("fuel surcharge added to shipping fee");
                } else if (productType.equals(PRODUCT_TYPE_FRESHCUT)) {
                    serviceFeeAmount = serviceFeeAmount.add(fuelSurcharge);
                    logger.info("fuel surcharge added to service fee");
                } else if (productType.equals(PRODUCT_TYPE_SAMEDAY_FRESHCUT)) {
                    if (shipMethod == null || shipMethod.equals("") || shipMethod.equalsIgnoreCase("SD")) {
                        if (sendToFloristFlag.equals("Y")) {
                            shippingFeeAmount = shippingFeeAmount.add(fuelSurcharge);
                            logger.info("fuel surcharge added to shipping fee");
                        } else {
                            serviceFeeAmount = serviceFeeAmount.add(fuelSurcharge);
                            logger.info("fuel surcharge added to service fee");
                        }
                    } else {
                        serviceFeeAmount = serviceFeeAmount.add(fuelSurcharge);
                        logger.info("fuel surcharge added to service fee");
                    }
                }
            }

            nodeValue = this.getSingleNode(xmlNode, "morning-delivery-fee/text()");
            orderDetail.setMorningDeliveryFee(nodeValue);           
            if (!StringUtils.isEmpty(nodeValue)) {
                orderDetail.setOriginalOrderHasMDF("Y");
                if (newWebsiteOrder) {
                    if (productType.equals(PRODUCT_TYPE_FRESHCUT) || productType.equals(PRODUCT_TYPE_SAMEDAY_FRESHCUT)) {
                        serviceFeeAmount = serviceFeeAmount.add(new BigDecimal(nodeValue));
                    } else {
                        shippingFeeAmount = shippingFeeAmount.add(new BigDecimal(nodeValue));
                    }
                    additionalCharges = additionalCharges.add(new BigDecimal(nodeValue));
                }
                logger.info("Morning delivery fee: " + nodeValue);
            }
            
            nodeValue = this.getSingleNode(xmlNode, "late-cutoff-fee/text()");
            orderDetail.setLatecutoffCharge(nodeValue); 
            if (!StringUtils.isEmpty(nodeValue)) {
                orderDetail.setOriginalOrderHasLCF("Y");
                if (newWebsiteOrder) {
                    if (productType.equals(PRODUCT_TYPE_FRESHCUT) || productType.equals(PRODUCT_TYPE_SAMEDAY_FRESHCUT)) {
                        serviceFeeAmount = serviceFeeAmount.add(new BigDecimal(nodeValue));
                    } else {
                        shippingFeeAmount = shippingFeeAmount.add(new BigDecimal(nodeValue));
                    }
                    additionalCharges = additionalCharges.add(new BigDecimal(nodeValue));
                }
                logger.info("Late cutoff fee: " + nodeValue);
            }

            nodeValue = this.getSingleNode(xmlNode, "same-day-upcharge/text()");
            if (!StringUtils.isEmpty(nodeValue)) {
                orderDetail.setSameDayUpcharge(nodeValue);
                if (newWebsiteOrder) {
                    serviceFeeAmount = serviceFeeAmount.add(new BigDecimal(nodeValue));
                    additionalCharges = additionalCharges.add(new BigDecimal(nodeValue));
                }
                logger.info("Same day upcharge: " + nodeValue);
            }
            
            nodeValue = this.getSingleNode(xmlNode, "original-order-has-sdu/text()");
            orderDetail.setOriginalOrderHasSDU(nodeValue);
            
            nodeValue = this.getSingleNode(xmlNode, "alaska-hawaii-fee/text()");
            if (!StringUtils.isEmpty(nodeValue)) {
                orderDetail.setAlaskaHawaiiSurcharge(nodeValue);
                if (newWebsiteOrder) {
                    if (productType.equals(PRODUCT_TYPE_FRESHCUT) || productType.equals(PRODUCT_TYPE_SAMEDAY_FRESHCUT)) {
                        serviceFeeAmount = serviceFeeAmount.add(new BigDecimal(nodeValue));
                    } else {
                        shippingFeeAmount = shippingFeeAmount.add(new BigDecimal(nodeValue));
                    }
                    additionalCharges = additionalCharges.add(new BigDecimal(nodeValue));
                }
                logger.info("Alaska/Hawaii Fee: " + nodeValue);
            }
            
            nodeValue = this.getSingleNode(xmlNode, "vendor-sat-upcharge/text()");
            if (!StringUtils.isEmpty(nodeValue)) {
                orderDetail.setSaturdayUpcharge(nodeValue);
                if (newWebsiteOrder) {
                    serviceFeeAmount = serviceFeeAmount.add(new BigDecimal(nodeValue));
                    additionalCharges = additionalCharges.add(new BigDecimal(nodeValue));
                }
                logger.info("Saturday upcharge: " + nodeValue);
            }
            
            nodeValue = this.getSingleNode(xmlNode, "vendor-sun-upcharge/text()");
            if (!StringUtils.isEmpty(nodeValue)) {
                orderDetail.setSundayUpcharge(nodeValue);
                if (newWebsiteOrder) {
                    serviceFeeAmount = serviceFeeAmount.add(new BigDecimal(nodeValue));
                    additionalCharges = additionalCharges.add(new BigDecimal(nodeValue));
                }
                logger.info("Sunday upcharge: " + nodeValue);
            }
            
            nodeValue = this.getSingleNode(xmlNode, "vendor-mon-upcharge/text()");
            if (!StringUtils.isEmpty(nodeValue)) {
                orderDetail.setMondayUpcharge(nodeValue);
                if (newWebsiteOrder) {
                    serviceFeeAmount = serviceFeeAmount.add(new BigDecimal(nodeValue));
                    additionalCharges = additionalCharges.add(new BigDecimal(nodeValue));
                }
                logger.info("Monday upcharge: " + nodeValue);
            }
            
            orderDetail.setServiceFeeAmount(serviceFeeAmount.toString()); 
            orderDetail.setShippingFeeAmount(shippingFeeAmount.toString());
            
            if (newWebsiteOrder) {
                // this is an order from the new website
                logger.info("** New website order: " + fulfillmentChannel + " " + productType);
                logger.info("serviceFee: " + serviceFeeAmount.toString());
                logger.info("shippingFee: " + shippingFeeAmount.toString());
                logger.info("additionalCharges: " + additionalCharges.toString());

                orderDetail.setOriginalServiceFeeAmount(serviceFeeAmount.toString());
                orderDetail.setOriginalShippingFeeAmount(shippingFeeAmount.toString());
                
                BigDecimal newServiceFee = serviceFeeAmount.subtract(additionalCharges);
                if(newServiceFee.floatValue() < 0) {
                    newServiceFee = new BigDecimal("0");            
                }
                BigDecimal newShippingFee = shippingFeeAmount.subtract(additionalCharges);
                if(newShippingFee.floatValue() < 0) {
                    newShippingFee = new BigDecimal("0");            
                }
                logger.info("newServiceFee: " + newServiceFee.toString());
                logger.info("newShippingFee: " + newShippingFee.toString());
                
                if (fulfillmentChannel.equalsIgnoreCase("florist")) {
                    String deliveryType = this.getSingleNode(xmlNode, "delivery-type/text()");
                    if (deliveryType != null && deliveryType.equalsIgnoreCase("Domestic")) {
                        orderDetail.setDomesticFloristServiceCharge(newServiceFee.toString());
                    } else {
                        orderDetail.setInternationalFloristServiceCharge(newServiceFee.toString());
                    }
                } else {
                    if (productType.equals(PRODUCT_TYPE_FRESHCUT) || productType.equals(PRODUCT_TYPE_SAMEDAY_FRESHCUT)) {
                        orderDetail.setVendorCharge(newServiceFee.toString());
                    } else {
                        orderDetail.setShippingCost(newShippingFee.toString());
                    }
                }
                
                nodeValue = this.getSingleNode(xmlNode, "service-fee-saved/text()");
                if (!StringUtils.isEmpty(nodeValue)) {
                    orderDetail.setServiceFeeAmountSavings(nodeValue);
                }

                nodeValue = this.getSingleNode(xmlNode, "shipping-fee-saved/text()");
                if (!StringUtils.isEmpty(nodeValue)) {
                    orderDetail.setShippingFeeAmountSavings(nodeValue);
                }
            
                if (orderDetail.getSourceCode() != null && !orderDetail.getSourceCode().equals(orderSourceCode)) {
                    orderDetail.setItemOfTheWeekFlag("Y");
                }
            }

            nodeValue = this.getSingleNode(xmlNode, "time-of-service/text()");
            if (nodeValue != null) { 
                orderDetail.setTimeOfService(nodeValue);
            }
            
            nodeValue = this.getSingleNode(xmlNode, "productid/text()");
            orderDetail.setProductId(nodeValue); 
            
            nodeValue = this.getSingleNode(xmlNode, "website-id/text()");
            orderDetail.setNovatorId(nodeValue);
            logger.info("Product id: " + orderDetail.getProductId() +
                    " Website id: " + orderDetail.getNovatorId());

            nodeValue = this.getSingleNode(xmlNode, "product-price/text()");
            orderDetail.setProductsAmount(nodeValue); 

            nodeValue = this.getSingleNode(xmlNode, "first-color-choice/text()");
            orderDetail.setColorFirstChoice(nodeValue); 

            nodeValue = this.getSingleNode(xmlNode, "second-color-choice/text()");
            orderDetail.setColorSecondChoice(nodeValue); 

            nodeValue = this.getSingleNode(xmlNode, "size/text()");
            orderDetail.setSizeChoice(nodeValue); 
            
            if(order.isOeOrder()){
            	nodeValue = this.getSingleNode(xmlNode, "size-indicator/text()");
                orderDetail.setSizeChoice(nodeValue); 
            }

            nodeValue = this.getOrderDetailPersonalizations(xmlNode);
            orderDetail.setPersonalizationData(nodeValue);

            List lAddOns = this.getAddOns(xmlNode); 
            orderDetail.setAddOns(lAddOns);
            if (this.isRosesDotCom) {
                orderDetail.setAddOnAmount(this.rosesDotComAddonTotal);
                if (logger.isDebugEnabled()) {
                    logger.debug("Addon amount for Roses.com is: " + this.rosesDotComAddonTotal);
                }
            }
            
            List lExtensions = this.getOrderDetailExtensions(xmlNode);
            orderDetail.setOrderDetailExtensions(lExtensions);
            
            List lRecipients = this.getRecipients(xmlNode);
            orderDetail.setRecipients(lRecipients);

            ItemTaxVO itemTaxVO = this.getTaxes(xmlNode);
            orderDetail.setItemTaxVO(itemTaxVO);
            
            /* emueller 10/14/05
             * During PhaseIII we found out that Amazon.com is sending over
             * the xml with this tag spelled 'occasion', but the gatherer
             * exepects 'occassion'.  Since we are not changing Amazon as 
             * part of PhaseIII a change is being put here to accept both
             * spellings.
             */
            nodeValue = this.getSingleNode(xmlNode, "occassion/text()");
            if(nodeValue == null || nodeValue.length() == 0)
            {
                nodeValue = this.getSingleNode(xmlNode, "occasion/text()");
            }            
            orderDetail.setOccassionId(nodeValue);

            nodeValue = this.getSingleNode(xmlNode, "delivery-date/text()");
            orderDetail.setDeliveryDate(nodeValue);

            nodeValue = this.getSingleNode(xmlNode, "second-delivery-date/text()");
            orderDetail.setDeliveryDateRangeEnd(nodeValue);

            // Only exists for OE orders
            nodeValue = this.getSingleNode(xmlNode, "order-comments/text()");
            orderDetail.setOrderComments(nodeValue);

            nodeValue = this.getSingleNode(xmlNode, "card-message/text()");
            orderDetail.setCardMessage(nodeValue);

            nodeValue = this.getSingleNode(xmlNode, "card-signature/text()");
            orderDetail.setCardSignature(nodeValue);

            // Add add on text to special instructions
            StringBuffer specialInstructions = new StringBuffer();
            AddOnsVO aoVO = null;
            for(Iterator<AddOnsVO> it = lAddOns.iterator(); it.hasNext();) 
            {
                aoVO = it.next();
                if(aoVO.getText() != null && !"".equals(aoVO.getText()))
                {
                    // If a funeral banner exists
                    if(aoVO.getAddOnCode().equalsIgnoreCase(OrderGatherConstants.ADD_ON_FUNERAL_BANNER_CODE))
                    {
                        specialInstructions.append("BANNER TEXT(");
                        specialInstructions.append(aoVO.getText());
                        specialInstructions.append(") ");
                    }
                    else 
                    {
                        specialInstructions.append(aoVO.getText());
                        specialInstructions.append(" ");
                    }
                }
            }
            // Add special instructions
            nodeValue = this.getSingleNode(xmlNode, "special-instructions/text()");
            if(nodeValue != null && !"".equals(nodeValue)) 
            {
                specialInstructions.append(nodeValue); 
            }
            
            orderDetail.setSpecialInstructions(specialInstructions.toString());
            
            nodeValue = this.getSingleNode(xmlNode, "legacy_id/text()");
            if(nodeValue != null && !"".equals(nodeValue)) 
            {
            	orderDetail.setLegacyId(nodeValue);
            }
            
            nodeValue = this.getSingleNode(xmlNode, "lmg-email-address/text()");
            orderDetail.setLastMinuteGiftEmail(nodeValue);

            nodeValue = this.getSingleNode(xmlNode, "lmg-email-signature/text()");
            orderDetail.setLastMinuteGiftSignature(nodeValue);

            nodeValue = this.getSingleNode(xmlNode, "sender-release-flag/text()");
            orderDetail.setSenderInfoRelease(nodeValue);

            nodeValue = this.getSingleNode(xmlNode, "ariba-po-number/text()");
            orderDetail.setAribaPoNumber(nodeValue);

            nodeValue = this.getSingleNode(xmlNode, "ariba-cost-center/text()");
            orderDetail.setAribaCostCenter(nodeValue);

            nodeValue = this.getSingleNode(xmlNode, "ariba-ams-project-code/text()");
            orderDetail.setAribaAmsProjectCode(nodeValue);

            nodeValue = this.getSingleNode(xmlNode, "ariba-unspsc-code/text()");
            orderDetail.setAribaUnspscCode(nodeValue);
		
	    // Default substitution ack to Yes
            nodeValue = this.getSingleNode(xmlNode, "product-substitution-acknowledgement/text()");
            orderDetail.setSubstituteAcknowledgement(nodeValue == null || nodeValue.equals("")?"Y":nodeValue);
            
           nodeValue = this.getSingleNode(xmlNode, "partner-cost/text()");
           orderDetail.setPartnerCost(nodeValue);

            //  Used for OE and bulk orders
            nodeValue = this.getSingleNode(xmlNode, "florist/text()");
            orderDetail.setFloristNumber(nodeValue);

            orderDetail.setStatus(OrderGatherConstants.ITEM_STATUS);
            
            nodeValue = this.getSingleNode( xmlNode, "commission/text()" );
            orderDetail.setCommission( nodeValue );
            nodeValue = this.getSingleNode( xmlNode, "wholesale/text()" );
            orderDetail.setWholesale(nodeValue);
            nodeValue = this.getSingleNode( xmlNode, "transaction/text()" );
            orderDetail.setTransaction( nodeValue );
            nodeValue = this.getSingleNode( xmlNode, "pdb-price/text()" );
            orderDetail.setPdbPrice( nodeValue );
            
            nodeValue = this.getSingleNode( xmlNode, "quantity/text()" );
            orderDetail.setQuantity( nodeValue );
            nodeValue = this.getSingleNode( xmlNode, "discount-amount/text()" );
            orderDetail.setDiscountAmount( nodeValue );
            nodeValue = this.getSingleNode( xmlNode, "discount-type/text()" );
            orderDetail.setDiscountType( nodeValue );
            nodeValue = this.getSingleNode( xmlNode, "discounted-product-price/text()" );
            orderDetail.setDiscountedProductPrice( nodeValue );

            // Recalculate is never called downstream in the pipeline for Roses.com orders 
            // so we must override the product price with any discounted price here.
            // Clear discountAmount otherwise COM will display totals incorrectly.
            // Basically for FF, we are taking orders as is - so we don't care what discount was.
            //
            if (this.isRosesDotCom) {
                orderDetail.setDiscountAmount(null);
                if (orderDetail.getDiscountedProductPrice() != null && !orderDetail.getDiscountedProductPrice().equals("")) {
                    orderDetail.setProductsAmount(orderDetail.getDiscountedProductPrice());
                } else {
                    nodeValue = this.getSingleNode( xmlNode, "retail-variable-price/text()" );
                    if (nodeValue != null) {
                        orderDetail.setProductsAmount(nodeValue);
                        orderDetail.setDiscountedProductPrice(nodeValue);
                    }
                }
                logger.info("productsAmount: " + orderDetail.getProductsAmount());
            }

            nodeValue = this.getSingleNode( xmlNode, "miles-points/text()" );
            orderDetail.setMilesPoints( nodeValue );

            orderDetail.setPriceOverrideFlag(configUtil.getProperty(OrderGatherConstants.GATHERER_CONFIG_FILE, "price_override_flag_default"));

            nodeValue = this.getSingleNode( xmlNode, "personal-greeting-id/text()" );
            orderDetail.setPersonalGreetingId( nodeValue );
            
            nodeValue = this.getSingleNode( xmlNode, "bin-source-changed/text()" );
            if (nodeValue == null || nodeValue.equals("")) nodeValue = "N";
            orderDetail.setBinSourceChangedFlag(nodeValue);

            AVSAddressVO avsAddress = this.getAvsAddress(xmlNode);
            orderDetail.setAvsAddress(avsAddress);
            
            nodeValue = getSingleNode(xmlNode, "add-on-discount-amount/text()");
            orderDetail.setAddOnDiscountAmount(nodeValue);

            if (newWebsiteOrder) {
                ProductMasterHandler pmh = (ProductMasterHandler) CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_PRODUCT_MASTER);
                ProductMasterVO pmVO = pmh.getProductByWebProductId(orderDetail.getProductId());
                if (pmVO != null) {
                    orderDetail.setPdbPrice(pmVO.getStandardPrice().toString());
                }
                logger.info("PDB price set to " + orderDetail.getPdbPrice());
                if (StringUtils.isEmpty(orderDetail.getCardMessage())) {
                    String cardMessage = configUtil.getFrpGlobalParm("FTDAPPS_PARMS", "CALYX_DEFAULT_CARD_MESSAGE");
                    orderDetail.setCardMessage(cardMessage);
                }
            }

            lOrderDetails.add(orderDetail);
         
          }
       }
     catch(Exception ex)
     {
        logger.error("Error mapping xml to Order Detail Value object: " + ex.toString()); 
        StringWriter sw = new StringWriter();
        ex.printStackTrace(new PrintWriter(sw));
        logger.error(sw.toString());
     }
     
return lOrderDetails;
}

	private ItemTaxVO getTaxes(Node xmlNode) {
		logger.debug("inside getTaxes");
		
		ItemTaxVO itemTaxVO = null;
				
		try {
					
			itemTaxVO = new ItemTaxVO();
			
			//if tag tax-service-performed is not present - It is OLD XML
			String taxServicePerformed = this.getSingleNode(xmlNode, "taxes/tax-service-performed/text()");	
			if(logger.isDebugEnabled()) {
				logger.debug("Tax service performed - " + taxServicePerformed);
			}
			
			// saving the split from the node list taxes/tax	
			boolean isTaxServicePerformed = "Y".equalsIgnoreCase(taxServicePerformed) ? true : false;
			setTax(xmlNode, itemTaxVO, isTaxServicePerformed);
			
			// save the total tax and shipping tax
			String nodeValue = null;
			if(!isTaxServicePerformed && (StringUtils.isEmpty(itemTaxVO.getTaxAmount())
							|| new BigDecimal(itemTaxVO.getTaxAmount()).compareTo(BigDecimal.ZERO) == 0)) { // If service is down the total_tax is populated and tax-amount would be null, no need to enter this block when service is down and default rate applied.
				// IF OLD XML TAGS to process
				nodeValue = this.getSingleNode(xmlNode, "tax-amount/text()");
				if(nodeValue != null && nodeValue.length() > 0) {
					itemTaxVO.setTaxAmount(nodeValue);
				}
			} 
			

			nodeValue = this.getSingleNode( xmlNode, "shipping-tax-amount/text()" );
			itemTaxVO.setShippingTax(nodeValue);
			 
		} catch (Exception ex) {
			logger.error("Error mapping the xml to the Tax value object: " + ex.toString());
		}
		return itemTaxVO; 
	}

	private void setTax(Node xmlNode, ItemTaxVO itemTaxVO, boolean isTaxPerformed) throws XPathExpressionException {
		
		NodeList taxesNodes = null; 
		Node taxNode = null;
		NodeList totalTaxNodes = null; 
		NodeList taxName = null;
		NodeList taxRate = null;
		NodeList taxAmount = null;
		NodeList taxDesc = null;
		
		taxesNodes = DOMUtil.selectNodes(xmlNode, "taxes/tax");	
		totalTaxNodes = DOMUtil.selectNodes(xmlNode, "taxes/total_tax");
		String oldTotalTax = this.getSingleNode(xmlNode, "tax-amount/text()");
      
		
		boolean saveSplit = true;
		// Do not save the split when tax service down and default tax rate is applied.
		if(!isTaxPerformed && (totalTaxNodes != null && totalTaxNodes.getLength() == 1) && StringUtils.isEmpty(oldTotalTax)) {
			saveSplit = false;
		}

		if(taxesNodes != null && taxesNodes.getLength() > 0 && saveSplit) {
			for (int i = 0; i < taxesNodes.getLength(); i++) {
				taxNode = taxesNodes.item(i);
				TaxVO taxVO = new TaxVO(); 
				
				taxDesc = DOMUtil.selectNodes(taxNode, "description/text()");
				taxRate = DOMUtil.selectNodes(taxNode, "rate/text()");
				taxAmount = DOMUtil.selectNodes(taxNode, "amount/text()");
				taxName = DOMUtil.selectNodes(taxNode, "name/text()"); // old order XML

				if(taxName != null && taxName.getLength() > 0) { // it type does not present name should have been there for old XML.
					taxVO.setName(taxName.item(0).getNodeValue());
				}  
				
				if(taxDesc != null && taxDesc.getLength() > 0) {
					taxVO.setDescription(taxDesc.item(0).getNodeValue());
					if(StringUtils.isEmpty(taxVO.getName())) {
						taxVO.setName(taxDesc.item(0).getNodeValue()); // for new order XML, save name same as description for display
					}
				}
				
				if (taxAmount != null && taxAmount.getLength() > 0) {
					taxVO.setAmount(new BigDecimal(taxAmount.item(0).getNodeValue()));
				}
				
				if (taxDesc != null && taxDesc.getLength() > 0) {
					taxVO.setDescription(taxDesc.item(0).getNodeValue());
				}
				
				if (taxRate != null && taxRate.getLength() > 0) {
					taxVO.setRate(new BigDecimal(taxRate.item(0).getNodeValue()));
				}

				itemTaxVO.getTaxSplit().add(taxVO);
			}
		}
		
		// Add total taxes		
		if(totalTaxNodes != null && totalTaxNodes.getLength() == 1) {
			
			taxNode = totalTaxNodes.item(0);				
			taxAmount = DOMUtil.selectNodes(taxNode, "amount/text()");
			taxDesc = DOMUtil.selectNodes(taxNode, "description/text()");
			taxRate = DOMUtil.selectNodes(taxNode, "rate/text()");
			TaxVO taxVO = new TaxVO();
			
			if (taxAmount != null && taxAmount.getLength() > 0) {
				itemTaxVO.setTaxAmount(taxAmount.item(0).getNodeValue());
				taxVO.setAmount(new BigDecimal(taxAmount.item(0).getNodeValue()));
			}
			
			if (taxDesc != null && taxDesc.getLength() > 0) {
				itemTaxVO.setTotalTaxDescription(taxDesc.item(0).getNodeValue());
				taxVO.setDescription(taxDesc.item(0).getNodeValue());
				taxVO.setName(taxDesc.item(0).getNodeValue());
			}
			
			if (taxRate != null && taxRate.getLength() > 0) {
				itemTaxVO.setTotalTaxrate(new BigDecimal(taxRate.item(0).getNodeValue()));
				taxVO.setRate(new BigDecimal(taxRate.item(0).getNodeValue()));
			}	
			
			
			if(isTaxPerformed && itemTaxVO.getTaxSplit() != null && itemTaxVO.getTaxSplit().size() == 0) { // this is for FTD-CA surcharge
				itemTaxVO.getTaxSplit().add(taxVO);
			}
			
		}

	}

/**
  * Get contact information that's associated with the order
  * for bulk orders only.
  * 
  * @param xmlNode XMLNode 
  * @return List of order contact info value objects
  */
 private List getOrderContactInfo(Node xmlNode)
 {
    List lOrderContactInfo = new ArrayList();
    OrderContactInfoVO orderContactInfo = new OrderContactInfoVO();
    String nodeValue = "";
   
    try{

        nodeValue = this.getSingleNode(xmlNode, "/order/header/contact-first-name/text()");
        orderContactInfo.setFirstName(nodeValue);

        nodeValue = this.getSingleNode(xmlNode, "/order/header/contact-last-name/text()");
        orderContactInfo.setLastName(nodeValue);

        nodeValue = this.getSingleNode(xmlNode, "/order/header/contact-phone/text()");
        orderContactInfo.setPhone(nodeValue);

        nodeValue = this.getSingleNode(xmlNode, "/order/header/contact-ext/text()");
        orderContactInfo.setExt(nodeValue);

        nodeValue = this.getSingleNode(xmlNode, "/order/header/contact-email-address/text()");
        orderContactInfo.setEmail(nodeValue);

        lOrderContactInfo.add(orderContactInfo);
      
     }
     catch(Exception ex)
     {
        logger.error("Error mapping the xml to the Order Contact Info value object: " + ex.toString()); 
     }
   
     return lOrderContactInfo;
    
 }
 
 /**
  * Get the tax information associated with the order
  * 
  * Example Tax XML:
  * <taxes>
  * <tax>
  * <name>qst-tax</name>
  * <amount>5.89</amount>
  * </tax>
  * </taxes>
  * 
  * @param xmlNode XMLNode 
  * @return List of TaxVOs
  */
 /*private List getOrderTaxes(Node xmlNode)
 {
	 logger.debug("inside getOrderTaxes");
	 TaxVO taxVO = null;
	 List lOrderTaxes = new ArrayList();
	 NodeList taxes = null;
	 NodeList name = null;
	 NodeList amount = null;
	 Node taxNode = null;
	 try{
		 	taxes = DOMUtil.selectNodes(xmlNode, "taxes/tax");
		 	for (int i =0; i<taxes.getLength(); i++){
		 		taxNode = taxes.item(i);
		 		taxVO = new TaxVO();
		 		
		 		name = DOMUtil.selectNodes(taxNode, "name/text()");		 		
		 		amount = DOMUtil.selectNodes(taxNode, "amount/text()");

		 		if(name != null && name.getLength() > 0)
		 		{
		 			taxVO.setName(name.item(0).getNodeValue());
		 		}
		 		if(amount != null && amount.getLength() > 0)
		 		{
		 			taxVO.setAmount(new BigDecimal(amount.item(0).getNodeValue()));
		 		}
		 		lOrderTaxes.add(taxVO);
		 	}
	 }
	 catch(Exception ex){
		 logger.error("Error mapping the xml to the Tax value object: " + ex.toString());
	 }
	 return lOrderTaxes;
 }*/

/**
 * Override source code value for specific origin if necessary.
 * 
 * This private method was introduced in support of the first release of Roses.com.com.
 * Roses.com eventually will map a voucher number to source code (through DB lookup)
 * but for the first release it's simply mapped from a global_parm.  
 * 
 * @param xmlNode XML document
 * @param nodeName Node in XML document containing source code
 * @param origin Order Origin
 * @return
 */
private String sourceCodePreprocessor(Node xmlNode, String sourceNodeName, String origin) throws Exception {
    String sourceCode = null;
    
    // Only map Roses.com web orders
    logger.info("origin: " + origin);
    ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
    if (origin.equalsIgnoreCase(configUtil.getProperty(OrderGatherConstants.GATHERER_CONFIG_FILE, "roses_origin_internet"))) {
        sourceCode = configUtil.getFrpGlobalParm("FTDAPPS_PARMS", "ROSES_DEFAULT_SOURCE_CODE");
        logger.info("Using Roses.com default source code (" + sourceCode + ") for order");
        
    // Otherwise just get from XML doc
    } else {
        sourceCode = this.getSingleNode(xmlNode, sourceNodeName);
    }
    return sourceCode;
}


	public String calculateAddOnTotal(List<OrderDetailsVO> orderDetails)
	{
	  BigDecimal addOnTotal = new BigDecimal("0.00");
	  if ((orderDetails != null) && (!orderDetails.isEmpty())) {
	    for (OrderDetailsVO orderDetail : orderDetails) {
	      if ((orderDetails != null) && (orderDetail.getAddOns() != null) && (!orderDetail.getAddOns().isEmpty())) {
	        for (AddOnsVO addOnVO : (List<AddOnsVO>) orderDetail.getAddOns()) {
	          if ((addOnVO.getPrice() != null) && (addOnVO.getAddOnQuantity() != null)) {
	            BigDecimal temp = new BigDecimal(addOnVO.getPrice());
	            temp = temp.multiply(new BigDecimal(addOnVO.getAddOnQuantity()));
	            addOnTotal = addOnTotal.add(temp);
	          }
	        }
	      }
	    }
	  }
	  return String.valueOf(addOnTotal);
	}
	
	public String calculateAddOnDiscountTotal(List<OrderDetailsVO> orderDetails)
	{
	  BigDecimal addOnDiscountTotal = new BigDecimal("0.00");
	
	  if ((orderDetails != null) && (!orderDetails.isEmpty())) {
	    for (OrderDetailsVO orderDetail : orderDetails) {
	      if ((orderDetail != null) && (orderDetail.getAddOns() != null) && (!orderDetail.getAddOns().isEmpty())) {
	        for (AddOnsVO addOnVO : (List<AddOnsVO>) orderDetail.getAddOns()) {
	          if (addOnVO.getDiscountAmount() != null) {
	            BigDecimal temp = new BigDecimal(addOnVO.getDiscountAmount());
	            temp = temp.multiply(new BigDecimal(addOnVO.getAddOnQuantity()));
	            addOnDiscountTotal = addOnDiscountTotal.add(temp);
	          }
	        }
	      }
	    }
	  }
	  return String.valueOf(addOnDiscountTotal);
	}
}
