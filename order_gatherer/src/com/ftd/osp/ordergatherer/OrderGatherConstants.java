package com.ftd.osp.ordergatherer;

public class OrderGatherConstants
{
  public final static String GATHERER_CONFIG_FILE = "gatherer_config.xml";
  public final static String GATHERER_CONFIG_CONTEXT = "ORDER_GATHERER_CONFIG";
  public final static String XML_STRING = "XML_STRING";
  public final static String HP_STRING = "HP_STRING";
  public final static String HP_SUCCESS_STRING = "SUCCESS";
  public final static String USER_ID = "_SYSTEM__";
  public final static String JMS_MSG_STAMP = "ES1001";
  public final static String JMS_MSG_STAMP_TEST_0RDER = "ES1005";
  public final static String TEST_0RDER_STATUS = "9999";
  public final static int ERROR_CODE = 600;
  public final static int SUCCESS_CODE = 200;
  public final static String DUPLICATE_RECORD_ERROR_MESSAGE = "DUPLICATE MASTER ORDER NUMBER";
  public final static String TEST_ORDER_FOUND_ERROR = "com.ftd.osp.ordergatherer.OrderValidationException: TEST ORDER FOUND ERROR";
  public final static String GET_GLOBAL_PARAM = "GET_GLOBAL_PARAM_OG";
  public final static String MSG_TYPE_EXCEPTION = "EXCEPTION";
  public final static String MSG_SOURCE = "ORDER GATHERER";
  public final static String STATUS = "1001";
  public final static String ITEM_STATUS = "2001";
  public final static boolean SEND_JMS_MESSAGE = true;
  public final static String REASON = "reason";
  public final static String EMPTY_XML_ERROR = "Received empty xml transmission.";
  public final static String ORDER_COUNT_ERROR = "Order count does not equal total number of items received in XML file.";
  public final static String INVALID_ORDER_NUMBER_ERROR = "The following Order Number is Invalid.";
  public final static String VALID_ORDER_PREFIXES = "VALID_ORDER_PREFIXES";
  public final static String INVALID_PREFIX_ERROR = "The following Order Number is Invalid, it does not contain one of the predefined prefixes:";
  public final static String ADD_ON_FUNERAL_BANNER_CODE = "F";  

}

