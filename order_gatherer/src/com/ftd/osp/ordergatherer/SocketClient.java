package com.ftd.osp.ordergatherer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import java.net.Socket;

/**
 * Helper class for socket transmissions.
 *
 * @author Brian Munter
 */

public class SocketClient 
{
    private Socket socket;
    private String serverName;
    private int portNumber;
    private OutputStreamWriter writer;
    private BufferedReader reader;
    private static final char DELIMITER = '\n';

    private SocketClient() 
    {
    }
    
    public SocketClient(String serverName, int portNumber) throws IOException
    {
        this.serverName = serverName;
        this.portNumber = portNumber;
    }

    public void open() throws IOException 
    {
        socket = new Socket(serverName, portNumber);
        writer = new OutputStreamWriter(socket.getOutputStream());
        reader = new BufferedReader(new InputStreamReader( socket.getInputStream()));            
    }

    public String send(String message) throws IOException 
    {
        char[] chars = message.toCharArray();
        writer.write( chars, 0, chars.length );
        writer.write( DELIMITER );
        writer.flush();
        
        StringBuffer sb = new StringBuffer();
        int i;
        while ( (i=reader.read()) != -1 ) 
        {
            if ( ( (char)i )!= DELIMITER )
                sb.append( (char)i );
            else
                break;
        }

        return sb.toString();
    }
    
    public void close() throws IOException
    {   
        if (writer != null)
            writer.close();
        if (reader != null)
            reader.close();
        if (socket != null)
            socket.close();
    }
}