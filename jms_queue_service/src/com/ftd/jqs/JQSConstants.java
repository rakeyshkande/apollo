package com.ftd.jqs;

public interface JQSConstants
{
    public final static String PROPERTY_FILE = "jqs-config.xml";
    public final static String DATASOURCE_NAME = "DATASOURCE";

    public final static String JQS_CONFIG_CONTEXT      = "JQS_CONFIG";
    
}
