package com.ftd.jqs.bo;

import com.ftd.jqs.JQSConstants;
import com.ftd.jqs.dao.JQSDAO;
import com.ftd.jqs.util.SystemMessage;
import com.ftd.jqs.vo.JMSMessageHoldVO;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.osp.utilities.vo.MessageToken;

import java.sql.Connection;

import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;

public class JMSHoldBO
{
    protected Logger logger = new Logger(this.getClass().getName());

    private JQSDAO dao;
    private ConfigurationUtil cu;

    public JMSHoldBO()
    {
    }
    
    
    /**
     * Process a group of messages in the JMS Hold table and insert
     * them into the appropriate JMS Queue.
     */
    public void processHoldMessages()
    {
        Connection conn = null;

        try
        {

            String datasource = getConfigurationUtil().getProperty(JQSConstants.PROPERTY_FILE, JQSConstants.DATASOURCE_NAME);
            conn = DataSourceUtil.getInstance().getConnection(datasource);
            
            // Get the list of messages to process
            List<JMSMessageHoldVO> messages = getDAO().getMessages(conn,100);
            // Process each message
            if (messages.size() > 0)
            {
                logger.debug("Processing " + messages.size() + " JMS Hold messages");
            }

            for (int i=0 ;i < messages.size() ; i++) 
            {
                processMessage(conn,messages.get(i));
            }

            // Commit it all
            conn.commit();            
            if (messages.size() > 0)
            {
                logger.debug("Complete");
            }
        }
        catch (Throwable t)
        {
            logger.error("Exception processing JMS Message Hold",t);
            SystemMessage.sendSystemMessage(conn,"JQS Command Failed",t.getMessage());
        }
        finally
        {
            //close the connection
            try
            {
                conn.close();
            } catch (Exception e)
            {
                logger.error("Exception processing",e);
            }
            
        }
        
    }

    /**
     * Get the JQS Query DAO object.  This object is lazily initialized.
     * @return
     */
    protected JQSDAO getDAO()
    {
        if (dao == null)
        {
            dao = new JQSDAO();
        }
        return dao;
    }

    /**
     * Get the configurationUtil instance.  This is lazily initialized.
     * @return
     * @throws Exception
     */
    protected ConfigurationUtil getConfigurationUtil() throws Exception
    {
        if (cu == null)
        {
            cu = ConfigurationUtil.getInstance();
        }
        return cu;
    }

    /**
     * Process a held message.  Put the message on the JMS queue and delete
     * it from the database.
     * 
     * @param conn
     * @param vo
     */
    protected void processMessage(Connection conn, JMSMessageHoldVO vo) throws Exception
    {
        // Insert into JMS
        sendJMSMessage(vo.getPayload(), vo.getCorrelationId(), vo.getJmsQueueName());
        
        // Delete the message
        getDAO().deleteJMSMessageHold(conn, vo.getJmsMessageHoldId());
    }
    
    
    protected void sendJMSMessage(String payload, String correlationId, String jmsQueueName) throws Exception
    {
        MessageToken token = new MessageToken();
        Context context = new InitialContext();
        token.setMessage(payload);
        token.setStatus(jmsQueueName);
        token.setJMSCorrelationID(correlationId);
        Dispatcher dispatcher = Dispatcher.getInstance();
        dispatcher.dispatchTextMessage(context, token);
    }

}
