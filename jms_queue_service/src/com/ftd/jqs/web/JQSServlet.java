package com.ftd.jqs.web;

import com.ftd.jqs.util.ProcessJMSHoldTimerTask;

import java.util.Timer;

import javax.servlet.http.HttpServlet;

public class JQSServlet extends HttpServlet
{
    private static ProcessJMSHoldTimerTask processJMSHoldTimerTask = null;
    private static int TIMER_MILLISECOND_INTERVAL = 1000;
    
    public JQSServlet()
    {
        timerInit();
    }
    
    public static synchronized void timerInit()
    {
        if (processJMSHoldTimerTask == null)
        {
            Timer timer = new Timer("JMSHoldTimer", true);
            
            processJMSHoldTimerTask = new ProcessJMSHoldTimerTask();
            timer.scheduleAtFixedRate(processJMSHoldTimerTask,5000,TIMER_MILLISECOND_INTERVAL);
        }
    }
    
    public void finalize()
    {
        processJMSHoldTimerTask.cancel();
        processJMSHoldTimerTask = null;
    }
    
    
}
