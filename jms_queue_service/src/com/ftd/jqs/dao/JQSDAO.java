package com.ftd.jqs.dao;

import com.ftd.jqs.vo.JMSMessageHoldVO;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;

import java.math.BigDecimal;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JQSDAO
{
    public JQSDAO()
    {
    }
    
    
    public List<JMSMessageHoldVO> getMessages(Connection conn, int sizeLimit) throws Exception
    {
        JMSMessageHoldVO vo;
        List<JMSMessageHoldVO> messages = new ArrayList<JMSMessageHoldVO>();

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_MAX_NUMBER", new BigDecimal(sizeLimit));

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_JMS_MESSAGE_HOLD_LIST");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        while ( results != null && results.next() ) 
        {
            vo = new JMSMessageHoldVO();

            vo.setJmsMessageHoldId(results.getLong("jms_message_hold_id"));
            vo.setCorrelationId(results.getString("correlation_id"));
            vo.setJmsQueueName(results.getString("jms_queue_id"));
            vo.setPayload(results.getString("payload_txt"));
            
            messages.add(vo);
        }

        return messages;
           
    }
    
    public void deleteJMSMessageHold(Connection conn, Long messageId) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("DELETE_JMS_MESSAGE_HOLD");

        HashMap inputParams = new HashMap();
        inputParams.put("IN_JMS_MESSAGE_HOLD_ID", new BigDecimal(messageId));
        dataRequest.setInputParams(inputParams);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map)dataAccessUtil.execute(dataRequest);

        String status = (String)outputs.get("OUT_STATUS");
        if (status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String)outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
    }
    
}
