package com.ftd.jqs.util;

import com.ftd.jqs.bo.JMSHoldBO;

import java.util.TimerTask;

public class ProcessJMSHoldTimerTask extends TimerTask
{
    JMSHoldBO bo;
    
    public ProcessJMSHoldTimerTask()
    {
        bo = new JMSHoldBO();
    }

    public void run()
    {
        bo.processHoldMessages();
    }
    
    
}
