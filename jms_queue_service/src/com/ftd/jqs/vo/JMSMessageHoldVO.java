package com.ftd.jqs.vo;

public class JMSMessageHoldVO
{
    private Long   jmsMessageHoldId;
    private String jmsQueueName;
    private String correlationId;
    private String payload;
        
    public JMSMessageHoldVO()
    {
    }


    public void setJmsQueueName(String jmsQueueName)
    {
        this.jmsQueueName = jmsQueueName;
    }

    public String getJmsQueueName()
    {
        return jmsQueueName;
    }

    public void setCorrelationId(String correlationId)
    {
        this.correlationId = correlationId;
    }

    public String getCorrelationId()
    {
        return correlationId;
    }

    public void setPayload(String payload)
    {
        this.payload = payload;
    }

    public String getPayload()
    {
        return payload;
    }

    public void setJmsMessageHoldId(Long jmsMessageHoldId)
    {
        this.jmsMessageHoldId = jmsMessageHoldId;
    }

    public Long getJmsMessageHoldId()
    {
        return jmsMessageHoldId;
    }
}
