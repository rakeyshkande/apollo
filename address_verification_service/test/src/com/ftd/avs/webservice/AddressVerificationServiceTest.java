package com.ftd.avs.webservice;

import java.net.URL;
import java.util.Iterator;

import javax.xml.namespace.QName;

import net.virtualearth.dev.webservices.v1.common.ArrayOfGeocodeResult;
import net.virtualearth.dev.webservices.v1.common.Confidence;
import net.virtualearth.dev.webservices.v1.common.Credentials;
import net.virtualearth.dev.webservices.v1.common.GeocodeResult;
import net.virtualearth.dev.webservices.v1.geocode.ArrayOfFilterBase;
import net.virtualearth.dev.webservices.v1.geocode.ConfidenceFilter;
import net.virtualearth.dev.webservices.v1.geocode.GeocodeOptions;
import net.virtualearth.dev.webservices.v1.geocode.GeocodeRequest;
import net.virtualearth.dev.webservices.v1.geocode.GeocodeResponse;
import net.virtualearth.dev.webservices.v1.geocode.contracts.IGeocodeService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.tempuri.VirtualEarthGeocodingService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:**/spring-beans.xml"})
public class AddressVerificationServiceTest {
	
	/**
	 * This webservice client factory instance is injected via spring from spring-beans.xml
	 */
	//@Resource
	//WebServiceClientFactory webServiceClientFactory;
	
	@Test
	public void testWebServiceCall() throws Exception{
		
		/*if (webServiceClientFactory == null) {
			System.out.println("ws client factory is null");
		}*/
		
		
		
		/**
		 * To use the bing-melissadata wsdl, uncomment the following 3 lines, and comment out the 3 lines after that.
		 */
		URL geocodeServiceURL = new URL("http://esggeocodingservice.cloudapp.net/VirtualEarthGeocodingService.svc?wsdl");
		VirtualEarthGeocodingService service = new VirtualEarthGeocodingService(geocodeServiceURL);

//		URL geocodeServiceURL = new URL("http://dev.virtualearth.net/webservices/v1/metadata/geocodeservice/geocodeservice.wsdl");
//		GeocodeService service = new GeocodeService(geocodeServiceURL);
        Iterator iter = service.getPorts();
        while(iter.hasNext())
        {
          QName testQ = (QName)iter.next();
          service.addPort(testQ, "http://schemas.xmlsoap.org/soap/", "http://dev.virtualearth.net/webservices/v1/geocodeservice/GeocodeService.svc");///partnerUpdateMilesURL.toString());
        }
		IGeocodeService geocodeService = service.getNonSSL();

		
		GeocodeRequest req  = new GeocodeRequest();
		GeocodeResponse resp = new GeocodeResponse();		
		
		Credentials creds = new Credentials();
		creds.setApplicationId("Ai6XcCY8bXWz8MbaDeC5iCpvQEEF2KuGC6g6tKqJbVrMR5McVOmrcpHHbs5cN7VV");
		creds.setToken(null);
		
		GeocodeOptions options = new GeocodeOptions();
		//options.set
		ConfidenceFilter conf = new ConfidenceFilter();
		conf.setMinimumConfidence(Confidence.MEDIUM);
		ArrayOfFilterBase aofb = new ArrayOfFilterBase();
		aofb.getFilterBase().add(conf);
		options.setFilters(aofb);

		//use some random address.  I'll just pull one out of a hat
		//req.setQuery("3113 Woodcreek Dr, Downers Grove,IL 60515");
		req.setQuery("1 loop road spring field il 62701");
		req.setCredentials(creds);
		req.setOptions(options);
		
		try {
			resp = geocodeService.geocode(req);

			ArrayOfGeocodeResult results = resp.getResults();
			System.out.println(results.getGeocodeResult().size() + " results returned");
			for (GeocodeResult result : results.getGeocodeResult()) {
				System.out.println(result.getAddress().getAddressLine());
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		

	}

}
