package com.ftd.avs.webservice;

import static com.ftd.avs.common.constant.AddressVerificationServiceConstants.AVS_CONFIG;
import static junit.framework.Assert.assertTrue;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.isA;
import static org.easymock.EasyMock.replay;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;
import net.virtualearth.dev.webservices.v1.common.Address;
import net.virtualearth.dev.webservices.v1.common.ArrayOfGeocodeLocation;
import net.virtualearth.dev.webservices.v1.common.ArrayOfGeocodeResult;
import net.virtualearth.dev.webservices.v1.common.Confidence;
import net.virtualearth.dev.webservices.v1.common.GeocodeLocation;
import net.virtualearth.dev.webservices.v1.common.GeocodeResult;
import net.virtualearth.dev.webservices.v1.common.ResponseSummary;
import net.virtualearth.dev.webservices.v1.geocode.GeocodeRequest;
import net.virtualearth.dev.webservices.v1.geocode.GeocodeResponse;
import net.virtualearth.dev.webservices.v1.geocode.contracts.IGeocodeService;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

import com.ftd.avs.common.constant.AddressVerificationServiceConstants;
import com.ftd.avs.common.dao.AVSDAO;
import com.ftd.avs.common.util.CommonUtil;
import com.ftd.avs.common.util.WebServiceClientFactory;
import com.ftd.avs.webservice.vo.v1.DevaluationReason;
import com.ftd.avs.webservice.vo.v1.VerificationRequest;
import com.ftd.avs.webservice.vo.v1.VerificationResponse;
import com.ftd.avs.webservice.vo.v1.VerifiedAddress;
import com.ftd.ftdutilities.GeneralConstants;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;

/**
 * This unit test uses easyMock to exercise the logic in the address verification service.
 * It can be your best friend if you use it correctly.
 * 
 * The pattern goes like this.
 * -at a class level we create our mocks
 * -in each test method we create our test inputs
 * -execute the address verification service logic.  AVS will use the mocks that we created instead of actually
 * going to the database or to Bing.
 * -evaluate the outputs vs. expected outputs.
 * 
 * @author cjohnson
 *
 */
public class AddressVerificationServiceBingImplTest {
	
		
	private AddressVerificationServiceBingImpl AVSBingImpl;
	private Connection mockConn;
	private AVSDAO mockAVSDAO;
	private WebServiceClientFactory mockWSCF;
	private IGeocodeService imockGeoCodeService;
	private ArrayOfGeocodeResult mockAOGR;
	private ArrayOfGeocodeLocation mockAOGL;
	private CommonUtil mockCommonUtil;
	
	private GeocodeResponse geocodeResponse = new GeocodeResponse();
	private List<GeocodeResult> geocodeResults = new ArrayList<GeocodeResult>();
	private List<GeocodeLocation> geocodeLocations = new ArrayList<GeocodeLocation>();
	
	
	/**
	 * This is where we set up the mocks.  We are mocking out all of the external services and dependencies
	 * so we can focus exclusively on the logic in this service.
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {
		AVSBingImpl = new AddressVerificationServiceBingImpl();
		mockCommonUtil = createMock(CommonUtil.class);

		mockConn = EasyMock.createNiceMock(Connection.class);
		replay(mockConn);
		
		expect(mockCommonUtil.getNewConnection()).andReturn(mockConn).anyTimes();

		setupScoresAndThresholds();
		
		replay(mockCommonUtil);
		
		mockAVSDAO = createMock(AVSDAO.class);
		expect(mockAVSDAO.saveAVSRequest(isA(com.ftd.avs.webservice.vo.v1.Address.class), isA(String.class), EasyMock.gt(-1l), isA(String.class), isA(Connection.class)))
			.andReturn(new Long(999l)).anyTimes();
		replay(mockAVSDAO);
		
		imockGeoCodeService = createMock(IGeocodeService.class);
		expect(imockGeoCodeService.geocode(isA(GeocodeRequest.class)))
			.andReturn(geocodeResponse).anyTimes();
		replay(imockGeoCodeService);
		
		
		mockWSCF = createMock(WebServiceClientFactory.class);
		expect(this.mockWSCF.getGeocodeService())
			.andReturn(this.imockGeoCodeService).anyTimes();
		replay(mockWSCF);
		
		mockAOGR = createMock(ArrayOfGeocodeResult.class);
		expect(mockAOGR.getGeocodeResult())
			.andReturn(geocodeResults).anyTimes();
		replay(mockAOGR);
		
		mockAOGL = createMock(ArrayOfGeocodeLocation.class);
		expect(mockAOGL.getGeocodeLocation())
			.andReturn(geocodeLocations).anyTimes();
		replay(mockAOGL);
		
		geocodeResponse.setResults(mockAOGR);
		
		ResponseSummary summary = new ResponseSummary();
		summary.setTraceId("blahblahblah");
		
		geocodeResponse.setResponseSummary(summary);
		
		AVSBingImpl.webServiceClientFactory = mockWSCF;
		AVSBingImpl.commonUtil = mockCommonUtil;
		AVSBingImpl.dao = mockAVSDAO;
		
		

		
	}
	
	/**
	 * these are the mocks for the global parms avs uses.
	 * @throws CacheException
	 * @throws Exception
	 */
	private void setupScoresAndThresholds() throws CacheException, Exception {
		expect(this.mockCommonUtil.getFrpGlobalParm(AVS_CONFIG, GeneralConstants.AVS_SCORE_HIGH_CONFIDENCE)).andReturn("1").anyTimes();
		expect(this.mockCommonUtil.getFrpGlobalParm(AVS_CONFIG, GeneralConstants.AVS_SCORE_MEDIUM_CONFIDENCE)).andReturn("2").anyTimes();
		expect(this.mockCommonUtil.getFrpGlobalParm(AVS_CONFIG, GeneralConstants.AVS_SCORE_LOW_CONFIDENCE)).andReturn("2").anyTimes();
		expect(this.mockCommonUtil.getFrpGlobalParm(AVS_CONFIG, GeneralConstants.AVS_SCORE_CITY_DIFFERENT)).andReturn("30").anyTimes();
		expect(this.mockCommonUtil.getFrpGlobalParm(AVS_CONFIG, GeneralConstants.AVS_SCORE_ZIP_DIFFERENT)).andReturn("30").anyTimes();
		expect(this.mockCommonUtil.getFrpGlobalParm(AVS_CONFIG, GeneralConstants.AVS_SCORE_ZIPPLUSFOUR_DIFFERENT)).andReturn("10").anyTimes();
		expect(this.mockCommonUtil.getFrpGlobalParm(AVS_CONFIG, GeneralConstants.AVS_SCORE_STATE_DIFFERENT)).andReturn("25").anyTimes();
		expect(this.mockCommonUtil.getFrpGlobalParm(AVS_CONFIG, GeneralConstants.AVS_SCORE_ADDRESS_TYPE_ADDRESS)).andReturn("0").anyTimes();
		expect(this.mockCommonUtil.getFrpGlobalParm(AVS_CONFIG, GeneralConstants.AVS_SCORE_ADDRESS_LINE_DIFFERENT)).andReturn("20").anyTimes();
		expect(this.mockCommonUtil.getFrpGlobalParm(AVS_CONFIG, AddressVerificationServiceConstants.AVS_SCORE_IS_MULTIPLE)).andReturn("15").anyTimes();
		expect(this.mockCommonUtil.getFrpGlobalParm(AVS_CONFIG, AddressVerificationServiceConstants.BING_CREDENTIAL_APP_ID)).andReturn("whatever").anyTimes();
		expect(this.mockCommonUtil.getFrpGlobalParm(AVS_CONFIG, AddressVerificationServiceConstants.REAPPEND_SECONDARY_UNIT_DESIGNATORS)).andReturn("on").anyTimes();
		
	}

	@Test
	public void test() throws Exception {
		
		
		//first create input data.
		VerificationRequest req = new VerificationRequest();
		com.ftd.avs.webservice.vo.v1.Address a = new com.ftd.avs.webservice.vo.v1.Address();
		a.setStreet1("3113 Woodcreek Dr.");
		a.setCity("Downer's Grove");
		a.setZip("60515");
		a.setState("IL");
		req.setAddress(a);
		req.setClientIdentifier("123");
		
				
		//next create the output data.
		ResponseSummary summary = new ResponseSummary();
		summary.setTraceId("blahblahblah");
		
		geocodeResponse.setResponseSummary(summary);
		
		//need to set or mock out the geocoderesult list
		GeocodeResult result = new GeocodeResult();
		Address outAddress = new Address();
		outAddress.setAddressLine("3113 Woodcreek Dr");
		outAddress.setLocality("Downers Grove");
		outAddress.setPostalCode("60515");
		outAddress.setAdminDistrict("IL");
		result.setAddress(outAddress);
		result.setEntityType("Address");
		result.setLocations(mockAOGL);
		result.setConfidence(Confidence.MEDIUM);
		
		//this is the location that will get returned by the mockAOGL
		GeocodeLocation l = new GeocodeLocation();
		l.setAltitude(0.0);
		l.setLatitude(41.828567504882813);
		l.setLongitude(-88.034416198730469);
		l.setCalculationMethod("Parcel");
		geocodeLocations.add(l);
		
		geocodeResults.add(result);
		
		
		VerificationResponse resp = AVSBingImpl.verifyAddress(req);
		//finally test to make sure that we got the expected confidence score

		
		//do some verification on response data
		
		
	}
	
	@Test
	public void testDifferentStreetAddr() throws Exception{
		try{
			VerificationRequest req = new VerificationRequest();
			com.ftd.avs.webservice.vo.v1.Address a = new com.ftd.avs.webservice.vo.v1.Address();
			a.setStreet1("9011 Ballard Rd");
			a.setCity("Des Plaines");
			a.setZip("60016");
			a.setState("IL");
			req.setAddress(a);
			req.setClientIdentifier("123");
			
					
			GeocodeResult result = new GeocodeResult();
			Address outAddress = new Address();
			outAddress.setAddressLine("9011 W Ballard Rd");
			outAddress.setLocality("Downers Grove");
			outAddress.setPostalCode("60016");
			outAddress.setAdminDistrict("IL");
			result.setAddress(outAddress);
			result.setEntityType("Address");
			result.setLocations(mockAOGL);
			result.setConfidence(Confidence.MEDIUM);
			
			//this is the location that will get returned by the mockAOGL
			GeocodeLocation l = new GeocodeLocation();
			l.setAltitude(0.0);
			l.setLatitude(41.828567504882813);
			l.setLongitude(-88.034416198730469);
			l.setCalculationMethod("Parcel");
			geocodeLocations.add(l);
			
			geocodeResults.add(result);
			
			VerificationResponse res  = AVSBingImpl.verifyAddress(req);
					
			List<VerifiedAddress> verifiedAddrs = res.getVerifiedAddresses();
			boolean flag = false;
			for (VerifiedAddress verifiedAddr : verifiedAddrs) {
				if(!req.getAddress().getAddressLine().equalsIgnoreCase(verifiedAddr.getAddressLine())){
					List<DevaluationReason> deValRsns = verifiedAddr.getDevaluationReasons();
					for(DevaluationReason deValRsn : deValRsns){
						
						String key = deValRsn.getReasonKey();
						int val = deValRsn.getReasonScore();
						if(key.equalsIgnoreCase(GeneralConstants.AVS_SCORE_ADDRESS_LINE_DIFFERENT)){
							flag = true;
							Assert.assertTrue(val == 20);
						}
					}
					
				}
			}
			if(!flag)
				Assert.fail();
			//do some verification on response data
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
	@Test
	public void testDifferentCity() throws Exception{
		try{
			VerificationRequest req = new VerificationRequest();
			com.ftd.avs.webservice.vo.v1.Address a = new com.ftd.avs.webservice.vo.v1.Address();
			a.setStreet1("3113 Woodcreek Dr.");
			a.setCity("Downers Grov");
			a.setZip("60515");
			a.setState("IL");
			req.setAddress(a);
			req.setClientIdentifier("123");
			
					
			GeocodeResult result = new GeocodeResult();
			Address outAddress = new Address();
			outAddress.setAddressLine("3113 Woodcreek Dr");
			outAddress.setLocality("Downers Grove");
			outAddress.setPostalCode("60515");
			outAddress.setAdminDistrict("IL");
			result.setAddress(outAddress);
			result.setEntityType("Address");
			result.setLocations(mockAOGL);
			result.setConfidence(Confidence.MEDIUM);
			
			//this is the location that will get returned by the mockAOGL
			GeocodeLocation l = new GeocodeLocation();
			l.setAltitude(0.0);
			l.setLatitude(41.828567504882813);
			l.setLongitude(-88.034416198730469);
			l.setCalculationMethod("Parcel");
			geocodeLocations.add(l);
			
			geocodeResults.add(result);
			
			VerificationResponse res  = AVSBingImpl.verifyAddress(req);
			
			List<VerifiedAddress> verifiedAddrs = res.getVerifiedAddresses();
			boolean flag = false;
			for (VerifiedAddress verifiedAddr : verifiedAddrs) {
				if(!req.getAddress().getCity().equalsIgnoreCase(verifiedAddr.getCity())){
					List<DevaluationReason> deValRsns = verifiedAddr.getDevaluationReasons();
					for(DevaluationReason deValRsn : deValRsns){
						
						String key = deValRsn.getReasonKey();
						int val = deValRsn.getReasonScore();
						if(key.equalsIgnoreCase(GeneralConstants.AVS_SCORE_CITY_DIFFERENT)){
							Assert.assertTrue(val == 30);
							flag = true;
						}
					}
					
				}
			}
					
			if(!flag)
				Assert.fail();
			
			//do some verification on response data
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
	@Test
	public void testDifferentZip() throws Exception{
		try{
			VerificationRequest req = new VerificationRequest();
			com.ftd.avs.webservice.vo.v1.Address a = new com.ftd.avs.webservice.vo.v1.Address();
			a.setStreet1("3113 Woodcreek Dr.");
			a.setCity("Downers Grove");
			a.setZip("60516");
			a.setState("IL");
			req.setAddress(a);
			req.setClientIdentifier("123");
			
					
			GeocodeResult result = new GeocodeResult();
			Address outAddress = new Address();
			outAddress.setAddressLine("3113 Woodcreek Dr");
			outAddress.setLocality("Downers Grove");
			outAddress.setPostalCode("60515");
			outAddress.setAdminDistrict("IL");
			result.setAddress(outAddress);
			result.setEntityType("Address");
			result.setLocations(mockAOGL);
			result.setConfidence(Confidence.MEDIUM);
			
			//this is the location that will get returned by the mockAOGL
			GeocodeLocation l = new GeocodeLocation();
			l.setAltitude(0.0);
			l.setLatitude(41.828567504882813);
			l.setLongitude(-88.034416198730469);
			l.setCalculationMethod("Parcel");
			geocodeLocations.add(l);
			
			geocodeResults.add(result);
			
			VerificationResponse res  = AVSBingImpl.verifyAddress(req);
			
			List<VerifiedAddress> verifiedAddrs = res.getVerifiedAddresses();
			String[] reqZipArr = req.getAddress().getZip().split("-");
			String reqZip = "";
			if(reqZipArr.length > 1){
				reqZip = reqZipArr[0];
			}else{
				reqZip = req.getAddress().getZip();
			}
			
			boolean flag = false;
			
			for (VerifiedAddress verifiedAddr : verifiedAddrs) {
				String[] verifiedZipArr = verifiedAddr.getZip().split("-");
				String verifiedZip = "";
				if (verifiedZipArr.length > 1) {
					verifiedZip = verifiedZipArr[0];
				}else{
					verifiedZip = verifiedAddr.getZip();
				}
				if(!req.getAddress().getZip().equalsIgnoreCase(verifiedAddr.getZip())){
					List<DevaluationReason> deValRsns = verifiedAddr.getDevaluationReasons();
					for(DevaluationReason deValRsn : deValRsns){
						
						String key = deValRsn.getReasonKey();
						int val = deValRsn.getReasonScore();
						if(key.equalsIgnoreCase(GeneralConstants.AVS_SCORE_ZIP_DIFFERENT)){
							Assert.assertTrue(val == 30);
							flag = true;
						}
					}
					
				}
			}
			
			if(!flag)
				Assert.fail();
					
			
			
			//do some verification on response data
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
	@Test
	public void testDifferentZipPlus4() throws Exception{
		try{
			VerificationRequest req = new VerificationRequest();
			com.ftd.avs.webservice.vo.v1.Address a = new com.ftd.avs.webservice.vo.v1.Address();
			a.setStreet1("3113 Woodcreek Dr.");
			a.setCity("Downers Grove");
			a.setZip("60515-5413");
			a.setState("IL");
			req.setAddress(a);
			req.setClientIdentifier("123");
			
					
			GeocodeResult result = new GeocodeResult();
			Address outAddress = new Address();
			outAddress.setAddressLine("3113 Woodcreek Dr");
			outAddress.setLocality("Downers Grove");
			outAddress.setPostalCode("60515");
			outAddress.setAdminDistrict("IL");
			result.setAddress(outAddress);
			result.setEntityType("Address");
			result.setLocations(mockAOGL);
			result.setConfidence(Confidence.MEDIUM);
			
			//this is the location that will get returned by the mockAOGL
			GeocodeLocation l = new GeocodeLocation();
			l.setAltitude(0.0);
			l.setLatitude(41.828567504882813);
			l.setLongitude(-88.034416198730469);
			l.setCalculationMethod("Parcel");
			geocodeLocations.add(l);
			
			geocodeResults.add(result);
			
			VerificationResponse res  = AVSBingImpl.verifyAddress(req);
			
			List<VerifiedAddress> verifiedAddrs = res.getVerifiedAddresses();
			String[] reqZipArr = req.getAddress().getZip().split("-");
			String reqZip4 = "";
			if(reqZipArr.length > 1){
				reqZip4 = reqZipArr[1];
			}
			boolean flag = false;
			for (VerifiedAddress verifiedAddr : verifiedAddrs) {
				String[] verifiedZipArr = verifiedAddr.getZip().split("-");
				String verifiedZip4 = "";
				if (verifiedZipArr.length > 1) {
					verifiedZip4 = verifiedZipArr[1];
				}
				if(!reqZip4.equalsIgnoreCase(verifiedZip4)){
					List<DevaluationReason> deValRsns = verifiedAddr.getDevaluationReasons();
					for(DevaluationReason deValRsn : deValRsns){
						
						String key = deValRsn.getReasonKey();
						int val = deValRsn.getReasonScore();
						if(key.equalsIgnoreCase(GeneralConstants.AVS_SCORE_ZIPPLUSFOUR_DIFFERENT)){
							Assert.assertTrue(val == 10);
							flag = true;
						}
					}
				}
			}
			
			if(!flag)
				Assert.fail();
			
			//do some verification on response data
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
	@Test
	public void testDifferentState() throws Exception{
		try{
			VerificationRequest req = new VerificationRequest();
			com.ftd.avs.webservice.vo.v1.Address a = new com.ftd.avs.webservice.vo.v1.Address();
			a.setStreet1("3113 Woodcreek Dr.");
			a.setCity("Downers Grove");
			a.setZip("60516");
			a.setState("IL");
			req.setAddress(a);
			req.setClientIdentifier("123");
			
					
			GeocodeResult result = new GeocodeResult();
			Address outAddress = new Address();
			outAddress.setAddressLine("3113 Woodcreek Dr");
			outAddress.setLocality("Downers Grove");
			outAddress.setPostalCode("60515");
			outAddress.setAdminDistrict("IN");
			result.setAddress(outAddress);
			result.setEntityType("Address");
			result.setLocations(mockAOGL);
			result.setConfidence(Confidence.MEDIUM);
			
			//this is the location that will get returned by the mockAOGL
			GeocodeLocation l = new GeocodeLocation();
			l.setAltitude(0.0);
			l.setLatitude(41.828567504882813);
			l.setLongitude(-88.034416198730469);
			l.setCalculationMethod("Parcel");
			geocodeLocations.add(l);
			
			geocodeResults.add(result);
			
			VerificationResponse res  = AVSBingImpl.verifyAddress(req);
			
			List<VerifiedAddress> verifiedAddrs = res.getVerifiedAddresses();
			boolean flag = false;
			for (VerifiedAddress verifiedAddr : verifiedAddrs) {
				if(!req.getAddress().getState().equalsIgnoreCase(verifiedAddr.getState())){
					List<DevaluationReason> deValRsns = verifiedAddr.getDevaluationReasons();
					for(DevaluationReason deValRsn : deValRsns){
						
						String key = deValRsn.getReasonKey();
						int val = deValRsn.getReasonScore();
						if(key.equalsIgnoreCase(GeneralConstants.AVS_SCORE_STATE_DIFFERENT)){
							Assert.assertTrue(val == 25);
							flag = true;
						}
					}
					
				}
			}
			
			if(!flag)
				Assert.fail();
			
			
			//do some verification on response data
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
	/**
	 *  This is no longer a valid test since the 'multiple address' rule has been removed
	 *  
	 * @throws Exception
	 */
	@Deprecated
	public void testMultipleAddresses() throws Exception{
		try{
			VerificationRequest req = new VerificationRequest();
			com.ftd.avs.webservice.vo.v1.Address a = new com.ftd.avs.webservice.vo.v1.Address();
			a.setStreet1("100 ogden ave");
			a.setCity("downers groove");
			a.setZip("");
			a.setState("IL");
			req.setAddress(a);
			req.setClientIdentifier("123");
			
					
			GeocodeResult result = new GeocodeResult();
			Address outAddress = new Address();
			outAddress.setAddressLine("100 ogden ave");
			outAddress.setLocality("");
			outAddress.setPostalCode("60515");
			outAddress.setAdminDistrict("IL");
			result.setAddress(outAddress);
			result.setEntityType("Address");
			result.setLocations(mockAOGL);
			result.setConfidence(Confidence.MEDIUM);
			
			//this is the location that will get returned by the mockAOGL
			GeocodeLocation l = new GeocodeLocation();
			l.setAltitude(0.0);
			l.setLatitude(41.828567504882813);
			l.setLongitude(-88.034416198730469);
			l.setCalculationMethod("Parcel");
			geocodeLocations.add(l);
			
			geocodeResults.add(result);
			
			GeocodeResult result1 = new GeocodeResult();
			Address outAddress1 = new Address();
			outAddress1.setAddressLine("100 W Ogden Ave");
			outAddress1.setLocality("Westmont");
			outAddress1.setPostalCode("60559");
			outAddress1.setAdminDistrict("IL");
			result1.setAddress(outAddress1);
			result1.setEntityType("Address");
			result1.setLocations(mockAOGL);
			result1.setConfidence(Confidence.MEDIUM);
			
			//this is the location that will get returned by the mockAOGL
			GeocodeLocation l1 = new GeocodeLocation();
			l1.setAltitude(0.0);
			l1.setLatitude(41.828567504882813);
			l1.setLongitude(-88.034416198730469);
			l1.setCalculationMethod("Parcel");
			geocodeLocations.add(l1);
			
			geocodeResults.add(result1);
			
			GeocodeResult result2 = new GeocodeResult();
			Address outAddress2 = new Address();
			outAddress2.setAddressLine("100 N Ogden Ave");
			outAddress2.setLocality("Chicago");
			outAddress2.setPostalCode("60607");
			outAddress2.setAdminDistrict("IL");
			result2.setAddress(outAddress2);
			result2.setEntityType("Address");
			result2.setLocations(mockAOGL);
			result2.setConfidence(Confidence.MEDIUM);
			
			//this is the location that will get returned by the mockAOGL
			GeocodeLocation l2 = new GeocodeLocation();
			l2.setAltitude(0.0);
			l2.setLatitude(41.828567504882813);
			l2.setLongitude(-88.034416198730469);
			l2.setCalculationMethod("Parcel");
			geocodeLocations.add(l2);
			
			geocodeResults.add(result2);
			
			GeocodeResult result3 = new GeocodeResult();
			Address outAddress3 = new Address();
			outAddress3.setAddressLine("100 W Ogden Ave");
			outAddress3.setLocality("La Grange");
			outAddress3.setPostalCode("60525");
			outAddress3.setAdminDistrict("IL");
			result3.setAddress(outAddress3);
			result3.setEntityType("Address");
			result3.setLocations(mockAOGL);
			result3.setConfidence(Confidence.MEDIUM);
			
			//this is the location that will get returned by the mockAOGL
			GeocodeLocation l3 = new GeocodeLocation();
			l3.setAltitude(0.0);
			l3.setLatitude(41.828567504882813);
			l3.setLongitude(-88.034416198730469);
			l3.setCalculationMethod("Parcel");
			geocodeLocations.add(l3);
			
			geocodeResults.add(result3);
			
			
			VerificationResponse res  = AVSBingImpl.verifyAddress(req);
			
			List<VerifiedAddress> verifiedAddrs = res.getVerifiedAddresses();
			boolean flag = false;
			for (VerifiedAddress verifiedAddr : verifiedAddrs) {
				List<DevaluationReason> deValRsns = verifiedAddr.getDevaluationReasons();
				for(DevaluationReason deValRsn : deValRsns){
					
					String key = deValRsn.getReasonKey();
					int val = deValRsn.getReasonScore();
					if(key.equalsIgnoreCase(AddressVerificationServiceConstants.AVS_SCORE_IS_MULTIPLE)
							&& val == 15){
						Assert.assertTrue(val == 15);
						flag = true;
					}
				}
			}
			if(!flag)
				Assert.fail();
			
					
			
			
			//do some verification on response data
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
	@Test
	public void testSecDesignatorSuite() throws Exception{
		try{
			VerificationRequest req = new VerificationRequest();
			com.ftd.avs.webservice.vo.v1.Address a = new com.ftd.avs.webservice.vo.v1.Address();
			a.setStreet1("4721 EMPEROR BLVD # SUITE400");
			a.setCity("DURHAM");
			a.setZip("27703");
			a.setState("NC");
			req.setAddress(a);
			req.setClientIdentifier("123");
			
					
			GeocodeResult result = new GeocodeResult();
			Address outAddress = new Address();
			outAddress.setAddressLine("4721 EMPEROR BLVD");
			outAddress.setLocality("DURHAM");
			outAddress.setPostalCode("27703");
			outAddress.setAdminDistrict("NC");
			result.setAddress(outAddress);
			result.setEntityType("Address");
			result.setLocations(mockAOGL);
			result.setConfidence(Confidence.MEDIUM);
			
			//this is the location that will get returned by the mockAOGL
			GeocodeLocation l = new GeocodeLocation();
			l.setAltitude(0.0);
			l.setLatitude(41.828567504882813);
			l.setLongitude(-88.034416198730469);
			l.setCalculationMethod("Parcel");
			geocodeLocations.add(l);
			
			geocodeResults.add(result);
			
			VerificationResponse res  = AVSBingImpl.verifyAddress(req);
			
			List<VerifiedAddress> verifiedAddrs = res.getVerifiedAddresses();
			boolean flag = false;
			for (VerifiedAddress verifiedAddr : verifiedAddrs) {
				if(req.getAddress().getAddressLine().equalsIgnoreCase(verifiedAddr.getAddressLine())){
					Assert.assertTrue(req.getAddress().getAddressLine().equalsIgnoreCase(verifiedAddr.getAddressLine()));
					flag = true;
					
				}
			}
			
			if(!flag)
				Assert.fail();
					
			
			
			//do some verification on response data
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
	@Test
	public void testSecDesignatorSuite1() throws Exception{
		try{
			VerificationRequest req = new VerificationRequest();
			com.ftd.avs.webservice.vo.v1.Address a = new com.ftd.avs.webservice.vo.v1.Address();
			a.setStreet1("4721 EMPEROR BLVD SUITE#400");
			a.setCity("DURHAM");
			a.setZip("27703");
			a.setState("NC");
			req.setAddress(a);
			req.setClientIdentifier("123");
			
					
			GeocodeResult result = new GeocodeResult();
			Address outAddress = new Address();
			outAddress.setAddressLine("4721 EMPEROR BLVD");
			outAddress.setLocality("DURHAM");
			outAddress.setPostalCode("27703");
			outAddress.setAdminDistrict("NC");
			result.setAddress(outAddress);
			result.setEntityType("Address");
			result.setLocations(mockAOGL);
			result.setConfidence(Confidence.MEDIUM);
			
			//this is the location that will get returned by the mockAOGL
			GeocodeLocation l = new GeocodeLocation();
			l.setAltitude(0.0);
			l.setLatitude(41.828567504882813);
			l.setLongitude(-88.034416198730469);
			l.setCalculationMethod("Parcel");
			geocodeLocations.add(l);
			
			geocodeResults.add(result);
			
			VerificationResponse res  = AVSBingImpl.verifyAddress(req);
			
			List<VerifiedAddress> verifiedAddrs = res.getVerifiedAddresses();
			boolean flag = false;
			for (VerifiedAddress verifiedAddr : verifiedAddrs) {
				if(req.getAddress().getAddressLine().equalsIgnoreCase(verifiedAddr.getAddressLine())){
					Assert.assertTrue(req.getAddress().getAddressLine().equalsIgnoreCase(verifiedAddr.getAddressLine()));
					flag = true;
					
				}
			}
			
			if(!flag)
				Assert.fail();
					
			
			
			//do some verification on response data
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
	@Test
	public void testSecDesignatorBLDG() throws Exception{
		try{
			VerificationRequest req = new VerificationRequest();
			com.ftd.avs.webservice.vo.v1.Address a = new com.ftd.avs.webservice.vo.v1.Address();
			a.setStreet1("4721 EMPEROR BLVD BLDG #400");
			a.setCity("DURHAM");
			a.setZip("27703");
			a.setState("NC");
			req.setAddress(a);
			req.setClientIdentifier("123");
			
					
			GeocodeResult result = new GeocodeResult();
			Address outAddress = new Address();
			outAddress.setAddressLine("4721 EMPEROR BLVD");
			outAddress.setLocality("DURHAM");
			outAddress.setPostalCode("27703");
			outAddress.setAdminDistrict("NC");
			result.setAddress(outAddress);
			result.setEntityType("Address");
			result.setLocations(mockAOGL);
			result.setConfidence(Confidence.MEDIUM);
			
			//this is the location that will get returned by the mockAOGL
			GeocodeLocation l = new GeocodeLocation();
			l.setAltitude(0.0);
			l.setLatitude(41.828567504882813);
			l.setLongitude(-88.034416198730469);
			l.setCalculationMethod("Parcel");
			geocodeLocations.add(l);
			
			geocodeResults.add(result);
			
			VerificationResponse res  = AVSBingImpl.verifyAddress(req);
			
			List<VerifiedAddress> verifiedAddrs = res.getVerifiedAddresses();
			boolean flag = false;
			for (VerifiedAddress verifiedAddr : verifiedAddrs) {
				if(req.getAddress().getAddressLine().equalsIgnoreCase(verifiedAddr.getAddressLine())){
					Assert.assertTrue(req.getAddress().getAddressLine().equalsIgnoreCase(verifiedAddr.getAddressLine()));
					flag = true;
					
				}
			}
			
			if(!flag)
				Assert.fail();
					
			
			
			//do some verification on response data
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
	@Test
	public void testSecDesignatorApt() throws Exception{
		try{
			VerificationRequest req = new VerificationRequest();
			com.ftd.avs.webservice.vo.v1.Address a = new com.ftd.avs.webservice.vo.v1.Address();
			a.setStreet1("9011 W Ballard Rd Apt 2B");
			a.setCity("Des Plaines");
			a.setZip("60016");
			a.setState("IL");
			req.setAddress(a);
			req.setClientIdentifier("123");
			
					
			GeocodeResult result = new GeocodeResult();
			Address outAddress = new Address();
			outAddress.setAddressLine("9011 W Ballard Rd");
			outAddress.setLocality("Des Plaines");
			outAddress.setPostalCode("60016");
			outAddress.setAdminDistrict("IL");
			result.setAddress(outAddress);
			result.setEntityType("Address");
			result.setLocations(mockAOGL);
			result.setConfidence(Confidence.MEDIUM);
			
			//this is the location that will get returned by the mockAOGL
			GeocodeLocation l = new GeocodeLocation();
			l.setAltitude(0.0);
			l.setLatitude(41.828567504882813);
			l.setLongitude(-88.034416198730469);
			l.setCalculationMethod("Parcel");
			geocodeLocations.add(l);
			
			geocodeResults.add(result);
			
			VerificationResponse res  = AVSBingImpl.verifyAddress(req);
			
			List<VerifiedAddress> verifiedAddrs = res.getVerifiedAddresses();
			boolean flag = false;
			for (VerifiedAddress verifiedAddr : verifiedAddrs) {
				if(req.getAddress().getAddressLine().equalsIgnoreCase(verifiedAddr.getAddressLine())){
					Assert.assertTrue(req.getAddress().getAddressLine().equalsIgnoreCase(verifiedAddr.getAddressLine()));
					flag = true;
					
				}
			}
					
			if(!flag)
				Assert.fail();
			
			//do some verification on response data
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
	@Test
	public void testMediumConfidence() throws Exception{
		try{
			VerificationRequest req = new VerificationRequest();
			com.ftd.avs.webservice.vo.v1.Address a = new com.ftd.avs.webservice.vo.v1.Address();
			a.setStreet1("100 ogden ave");
			a.setCity("downers groove");
			a.setZip("");
			a.setState("IL");
			req.setAddress(a);
			req.setClientIdentifier("123");
			
					
			GeocodeResult result = new GeocodeResult();
			Address outAddress = new Address();
			outAddress.setAddressLine("100 ogden ave");
			outAddress.setLocality("");
			outAddress.setPostalCode("60515");
			outAddress.setAdminDistrict("IL");
			result.setAddress(outAddress);
			result.setEntityType("Address");
			result.setLocations(mockAOGL);
			result.setConfidence(Confidence.MEDIUM);
			
			//this is the location that will get returned by the mockAOGL
			GeocodeLocation l = new GeocodeLocation();
			l.setAltitude(0.0);
			l.setLatitude(41.828567504882813);
			l.setLongitude(-88.034416198730469);
			l.setCalculationMethod("Parcel");
			geocodeLocations.add(l);
			
			geocodeResults.add(result);
			
			GeocodeResult result1 = new GeocodeResult();
			Address outAddress1 = new Address();
			outAddress1.setAddressLine("100 W Ogden Ave");
			outAddress1.setLocality("Westmont");
			outAddress1.setPostalCode("60559");
			outAddress1.setAdminDistrict("IL");
			result1.setAddress(outAddress1);
			result1.setEntityType("Address");
			result1.setLocations(mockAOGL);
			result1.setConfidence(Confidence.MEDIUM);
			
			//this is the location that will get returned by the mockAOGL
			GeocodeLocation l1 = new GeocodeLocation();
			l1.setAltitude(0.0);
			l1.setLatitude(41.828567504882813);
			l1.setLongitude(-88.034416198730469);
			l1.setCalculationMethod("Parcel");
			geocodeLocations.add(l1);
			
			geocodeResults.add(result1);
			
			GeocodeResult result2 = new GeocodeResult();
			Address outAddress2 = new Address();
			outAddress2.setAddressLine("100 N Ogden Ave");
			outAddress2.setLocality("Chicago");
			outAddress2.setPostalCode("60607");
			outAddress2.setAdminDistrict("IL");
			result2.setAddress(outAddress2);
			result2.setEntityType("Address");
			result2.setLocations(mockAOGL);
			result2.setConfidence(Confidence.MEDIUM);
			
			//this is the location that will get returned by the mockAOGL
			GeocodeLocation l2 = new GeocodeLocation();
			l2.setAltitude(0.0);
			l2.setLatitude(41.828567504882813);
			l2.setLongitude(-88.034416198730469);
			l2.setCalculationMethod("Parcel");
			geocodeLocations.add(l2);
			
			geocodeResults.add(result2);
			
			GeocodeResult result3 = new GeocodeResult();
			Address outAddress3 = new Address();
			outAddress3.setAddressLine("100 W Ogden Ave");
			outAddress3.setLocality("La Grange");
			outAddress3.setPostalCode("60525");
			outAddress3.setAdminDistrict("IL");
			result3.setAddress(outAddress3);
			result3.setEntityType("Address");
			result3.setLocations(mockAOGL);
			result3.setConfidence(Confidence.MEDIUM);
			
			//this is the location that will get returned by the mockAOGL
			GeocodeLocation l3 = new GeocodeLocation();
			l3.setAltitude(0.0);
			l3.setLatitude(41.828567504882813);
			l3.setLongitude(-88.034416198730469);
			l3.setCalculationMethod("Parcel");
			geocodeLocations.add(l3);
			
			geocodeResults.add(result3);
			
			
			VerificationResponse res  = AVSBingImpl.verifyAddress(req);
			
			List<VerifiedAddress> verifiedAddrs = res.getVerifiedAddresses();
			boolean flag = false;
			for (VerifiedAddress verifiedAddr : verifiedAddrs) {
				if(verifiedAddr.getConfidence().equals("MEDIUM")){
					List<DevaluationReason> deValRsns = verifiedAddr.getDevaluationReasons();
					for(DevaluationReason deValRsn : deValRsns){
						
						String key = deValRsn.getReasonKey();
						int val = deValRsn.getReasonScore();
						if(key.equalsIgnoreCase(GeneralConstants.AVS_SCORE_MEDIUM_CONFIDENCE)){
							Assert.assertTrue(val == 2);
							flag = true;
						}
					}
					
					
				}
			}
			
			if(!flag)
				Assert.fail();
			
			//do some verification on response data
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
	
	@Test
	public void testHighConfidence() throws Exception{
		try{
			VerificationRequest req = new VerificationRequest();
			com.ftd.avs.webservice.vo.v1.Address a = new com.ftd.avs.webservice.vo.v1.Address();
			a.setStreet1("7129 LYRE LANE");
			a.setCity("Dallas");
			a.setZip("75214");
			a.setState("TX");
			req.setAddress(a);
			req.setClientIdentifier("123");
			
					
			GeocodeResult result = new GeocodeResult();
			Address outAddress = new Address();
			outAddress.setAddressLine("7129 LYRE LANE");
			outAddress.setLocality("Dallas");
			outAddress.setPostalCode("75214");
			outAddress.setAdminDistrict("TX");
			result.setAddress(outAddress);
			result.setEntityType("Address");
			result.setLocations(mockAOGL);
			result.setConfidence(Confidence.HIGH);
			
			//this is the location that will get returned by the mockAOGL
			GeocodeLocation l = new GeocodeLocation();
			l.setAltitude(0.0);
			l.setLatitude(41.828567504882813);
			l.setLongitude(-88.034416198730469);
			l.setCalculationMethod("Parcel");
			geocodeLocations.add(l);
			
			geocodeResults.add(result);
			
			VerificationResponse res  = AVSBingImpl.verifyAddress(req);
			
			List<VerifiedAddress> verifiedAddrs = res.getVerifiedAddresses();
			boolean flag = false;
			for (VerifiedAddress verifiedAddr : verifiedAddrs) {
				if(verifiedAddr.getConfidence().equals("HIGH")){
					List<DevaluationReason> deValRsns = verifiedAddr.getDevaluationReasons();
					for(DevaluationReason deValRsn : deValRsns){
						
						String key = deValRsn.getReasonKey();
						int val = deValRsn.getReasonScore();
						if(key.equalsIgnoreCase(GeneralConstants.AVS_SCORE_HIGH_CONFIDENCE)){
							Assert.assertTrue(val == 1);
							flag = true;
							
						}
					}
					
					
				}
			}
			
			if(!flag)
				Assert.fail();
					
			
			
			//do some verification on response data
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
	
	@Test 
	public void testSecDesignator1stFloor() throws Exception{
		try{
			VerificationRequest req = new VerificationRequest();
			com.ftd.avs.webservice.vo.v1.Address a = new com.ftd.avs.webservice.vo.v1.Address();
			a.setStreet1("7129 LYRE LANE 1st Floor");
			a.setCity("Dallas");
			a.setZip("75214");
			a.setState("TX");
			req.setAddress(a);
			req.setClientIdentifier("123");
			
					
			GeocodeResult result = new GeocodeResult();
			Address outAddress = new Address();
			outAddress.setAddressLine("7129 LYRE LANE");
			outAddress.setLocality("Dallas");
			outAddress.setPostalCode("75214");
			outAddress.setAdminDistrict("TX");
			result.setAddress(outAddress);
			result.setEntityType("Address");
			result.setLocations(mockAOGL);
			result.setConfidence(Confidence.HIGH);
			
			//this is the location that will get returned by the mockAOGL
			GeocodeLocation l = new GeocodeLocation();
			l.setAltitude(0.0);
			l.setLatitude(41.828567504882813);
			l.setLongitude(-88.034416198730469);
			l.setCalculationMethod("Parcel");
			geocodeLocations.add(l);
			
			geocodeResults.add(result);
			
			VerificationResponse res  = AVSBingImpl.verifyAddress(req);
			
			List<VerifiedAddress> verifiedAddrs = res.getVerifiedAddresses();
			boolean flag = false;
			for (VerifiedAddress verifiedAddr : verifiedAddrs) {
				Assert.assertEquals(req.getAddress().getAddressLine().toUpperCase(), verifiedAddr.getAddressLine().toUpperCase());
				if(req.getAddress().getAddressLine().equalsIgnoreCase(verifiedAddr.getAddressLine())){
					List<DevaluationReason> deValRsns = verifiedAddr.getDevaluationReasons();
					for(DevaluationReason deValRsn : deValRsns){
						
						String key = deValRsn.getReasonKey();
						int val = deValRsn.getReasonScore();
						if(key.equalsIgnoreCase(GeneralConstants.AVS_SCORE_ADDRESS_LINE_DIFFERENT)){
							flag = true;
							Assert.assertTrue(val == 0);
						}
					}
					
				}
			}
			if(!flag)
				Assert.fail();
					
			
			
			//do some verification on response data
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
	@Test 
	public void testSecDesignator2ndFloor() throws Exception{
		try{
			VerificationRequest req = new VerificationRequest();
			com.ftd.avs.webservice.vo.v1.Address a = new com.ftd.avs.webservice.vo.v1.Address();
			a.setStreet1("7129 LYRE LANE 2nd Floor");
			a.setCity("Dallas");
			a.setZip("75214");
			a.setState("TX");
			req.setAddress(a);
			req.setClientIdentifier("123");
			
					
			GeocodeResult result = new GeocodeResult();
			Address outAddress = new Address();
			outAddress.setAddressLine("7129 LYRE LANE");
			outAddress.setLocality("Dallas");
			outAddress.setPostalCode("75214");
			outAddress.setAdminDistrict("TX");
			result.setAddress(outAddress);
			result.setEntityType("Address");
			result.setLocations(mockAOGL);
			result.setConfidence(Confidence.HIGH);
			
			//this is the location that will get returned by the mockAOGL
			GeocodeLocation l = new GeocodeLocation();
			l.setAltitude(0.0);
			l.setLatitude(41.828567504882813);
			l.setLongitude(-88.034416198730469);
			l.setCalculationMethod("Parcel");
			geocodeLocations.add(l);
			
			geocodeResults.add(result);
			
			VerificationResponse res  = AVSBingImpl.verifyAddress(req);
			
			List<VerifiedAddress> verifiedAddrs = res.getVerifiedAddresses();
			boolean flag = false;
			for (VerifiedAddress verifiedAddr : verifiedAddrs) {
				Assert.assertEquals(req.getAddress().getAddressLine().toUpperCase(), verifiedAddr.getAddressLine().toUpperCase());
				if(req.getAddress().getAddressLine().equalsIgnoreCase(verifiedAddr.getAddressLine())){
					List<DevaluationReason> deValRsns = verifiedAddr.getDevaluationReasons();
					for(DevaluationReason deValRsn : deValRsns){
						
						String key = deValRsn.getReasonKey();
						int val = deValRsn.getReasonScore();
						if(key.equalsIgnoreCase(GeneralConstants.AVS_SCORE_ADDRESS_LINE_DIFFERENT)){
							flag = true;
							Assert.assertTrue(val == 0);
						}
					}
					
				}
			}
			if(!flag)
				Assert.fail();
					
			
			
			//do some verification on response data
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
	
	@Test 
	public void testSecDesignator3rdFloor() throws Exception{
		try{
			VerificationRequest req = new VerificationRequest();
			com.ftd.avs.webservice.vo.v1.Address a = new com.ftd.avs.webservice.vo.v1.Address();
			a.setStreet1("7129 LYRE LANE 3rd Floor");
			a.setCity("Dallas");
			a.setZip("75214");
			a.setState("TX");
			req.setAddress(a);
			req.setClientIdentifier("123");
			
					
			GeocodeResult result = new GeocodeResult();
			Address outAddress = new Address();
			outAddress.setAddressLine("7129 LYRE LANE");
			outAddress.setLocality("Dallas");
			outAddress.setPostalCode("75214");
			outAddress.setAdminDistrict("TX");
			result.setAddress(outAddress);
			result.setEntityType("Address");
			result.setLocations(mockAOGL);
			result.setConfidence(Confidence.HIGH);
			
			//this is the location that will get returned by the mockAOGL
			GeocodeLocation l = new GeocodeLocation();
			l.setAltitude(0.0);
			l.setLatitude(41.828567504882813);
			l.setLongitude(-88.034416198730469);
			l.setCalculationMethod("Parcel");
			geocodeLocations.add(l);
			
			geocodeResults.add(result);
			
			VerificationResponse res  = AVSBingImpl.verifyAddress(req);
			
			List<VerifiedAddress> verifiedAddrs = res.getVerifiedAddresses();
			boolean flag = false;
			for (VerifiedAddress verifiedAddr : verifiedAddrs) {
				Assert.assertEquals(req.getAddress().getAddressLine().toUpperCase(), verifiedAddr.getAddressLine().toUpperCase());
				if(req.getAddress().getAddressLine().equalsIgnoreCase(verifiedAddr.getAddressLine())){
					List<DevaluationReason> deValRsns = verifiedAddr.getDevaluationReasons();
					for(DevaluationReason deValRsn : deValRsns){
						
						String key = deValRsn.getReasonKey();
						int val = deValRsn.getReasonScore();
						if(key.equalsIgnoreCase(GeneralConstants.AVS_SCORE_ADDRESS_LINE_DIFFERENT)){
							flag = true;
							Assert.assertTrue(val == 0);
						}
					}
					
				}
			}
			if(!flag)
				Assert.fail();
					
			
			
			//do some verification on response data
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
	@Test 
	public void testSecDesignator4thFloor() throws Exception{
		try{
			VerificationRequest req = new VerificationRequest();
			com.ftd.avs.webservice.vo.v1.Address a = new com.ftd.avs.webservice.vo.v1.Address();
			a.setStreet1("7129 LYRE LANE 4th Floor");
			a.setCity("Dallas");
			a.setZip("75214");
			a.setState("TX");
			req.setAddress(a);
			req.setClientIdentifier("123");
			
					
			GeocodeResult result = new GeocodeResult();
			Address outAddress = new Address();
			outAddress.setAddressLine("7129 LYRE LANE");
			outAddress.setLocality("Dallas");
			outAddress.setPostalCode("75214");
			outAddress.setAdminDistrict("TX");
			result.setAddress(outAddress);
			result.setEntityType("Address");
			result.setLocations(mockAOGL);
			result.setConfidence(Confidence.HIGH);
			
			//this is the location that will get returned by the mockAOGL
			GeocodeLocation l = new GeocodeLocation();
			l.setAltitude(0.0);
			l.setLatitude(41.828567504882813);
			l.setLongitude(-88.034416198730469);
			l.setCalculationMethod("Parcel");
			geocodeLocations.add(l);
			
			geocodeResults.add(result);
			
			VerificationResponse res  = AVSBingImpl.verifyAddress(req);
			
			List<VerifiedAddress> verifiedAddrs = res.getVerifiedAddresses();
			boolean flag = false;
			for (VerifiedAddress verifiedAddr : verifiedAddrs) {
				Assert.assertEquals(req.getAddress().getAddressLine().toUpperCase(), verifiedAddr.getAddressLine().toUpperCase());
				if(req.getAddress().getAddressLine().equalsIgnoreCase(verifiedAddr.getAddressLine())){
					List<DevaluationReason> deValRsns = verifiedAddr.getDevaluationReasons();
					for(DevaluationReason deValRsn : deValRsns){
						
						String key = deValRsn.getReasonKey();
						int val = deValRsn.getReasonScore();
						if(key.equalsIgnoreCase(GeneralConstants.AVS_SCORE_ADDRESS_LINE_DIFFERENT)){
							flag = true;
							Assert.assertTrue(val == 0);
						}
					}
					
				}
			}
			if(!flag)
				Assert.fail();
					
			
			
			//do some verification on response data
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}	
	
	public void testFloorWithOtherDesignators() {
		try{
			VerificationRequest req = new VerificationRequest();
			com.ftd.avs.webservice.vo.v1.Address a = new com.ftd.avs.webservice.vo.v1.Address();
			a.setStreet1("7129 LYRE LANE 4th Floor REAR");
			a.setCity("Dallas");
			a.setZip("75214");
			a.setState("TX");
			req.setAddress(a);
			req.setClientIdentifier("123");
			
					
			GeocodeResult result = new GeocodeResult();
			Address outAddress = new Address();
			outAddress.setAddressLine("7129 LYRE LANE");
			outAddress.setLocality("Dallas");
			outAddress.setPostalCode("75214");
			outAddress.setAdminDistrict("TX");
			result.setAddress(outAddress);
			result.setEntityType("Address");
			result.setLocations(mockAOGL);
			result.setConfidence(Confidence.HIGH);
			
			//this is the location that will get returned by the mockAOGL
			GeocodeLocation l = new GeocodeLocation();
			l.setAltitude(0.0);
			l.setLatitude(41.828567504882813);
			l.setLongitude(-88.034416198730469);
			l.setCalculationMethod("Parcel");
			geocodeLocations.add(l);
			
			geocodeResults.add(result);
			
			VerificationResponse res  = AVSBingImpl.verifyAddress(req);
			
			List<VerifiedAddress> verifiedAddrs = res.getVerifiedAddresses();
			boolean flag = false;
			for (VerifiedAddress verifiedAddr : verifiedAddrs) {
				Assert.assertEquals(req.getAddress().getAddressLine().toUpperCase(), verifiedAddr.getAddressLine().toUpperCase());
				assertTrue(req.getAddress().getAddressLine().equalsIgnoreCase(verifiedAddr.getAddressLine()));
				

					
				
			}
			if(!flag)
				Assert.fail();
					
			
			
			//do some verification on response data
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testOtherSecondaryUnitDesignators() {
		
		String[] designators = {"Suit 123",
								"Slip 123",
								"Stop 123",
								"Trlr 123",
								"Front",
								"Frnt",
								"Spc 123",
								"Stop 123",
								"Rear House",
								"LOT 123",
								"Hanger 123",
								"Hngr 123",
								"Dept 123",
								"Department 123",
								"Bsmt 123",
								"Basement 123",
								"Lobby",
								"Lbby",
								"Lot 123",
								"Lowr something",
								"Ofc 123",
								"Penthouse",
								"Ph",
								"UPPR Floor",
								"REAR"};
		
		try{
			VerificationRequest req = new VerificationRequest();
			com.ftd.avs.webservice.vo.v1.Address a = new com.ftd.avs.webservice.vo.v1.Address();
			String originalStreet = "3113 Woodcreek Dr";
			a.setStreet1(originalStreet);
			a.setCity("Downers Grove");
			a.setZip("60515");
			a.setState("IL");
			req.setAddress(a);
			req.setClientIdentifier("123");
			
					
			GeocodeResult result = new GeocodeResult();
			Address outAddress = new Address();
			outAddress.setAddressLine("3113 Woodcreek Dr");
			outAddress.setLocality("Downers Grove");
			outAddress.setPostalCode("60515");
			outAddress.setAdminDistrict("IL");
			result.setAddress(outAddress);
			result.setEntityType("Address");
			result.setLocations(mockAOGL);
			result.setConfidence(Confidence.MEDIUM);
			
			//this is the location that will get returned by the mockAOGL
			GeocodeLocation l = new GeocodeLocation();
			l.setAltitude(0.0);
			l.setLatitude(41.828567504882813);
			l.setLongitude(-88.034416198730469);
			l.setCalculationMethod("Parcel");
			geocodeLocations.add(l);
			
			geocodeResults.add(result);
			
			
			//loop through the sec. unit designators and test that we recognize them all
			for (String designator : designators) {
				System.out.println( "testing designator " + designator);
				
				
				//append the current designator on to the end of the address followed by a space and some characters
				// This will be different from the output address
				//a.setStreet1(originalStreet + " " + designator + " 123");
				
				VerificationResponse res  = AVSBingImpl.verifyAddress(req);
				
				//now test that the input address and the output address are the SAME, meaning that we have successfully re-appended the secondary unit designator
				List<VerifiedAddress> verifiedAddrs = res.getVerifiedAddresses();

				for (VerifiedAddress verifiedAddr : verifiedAddrs) {
					if(!req.getAddress().getAddressLine().equalsIgnoreCase(verifiedAddr.getAddressLine())){
						Assert.fail();
					}
				}
				
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	
	@Test
	public void testRemoveTownOfFromBingResults() throws Exception {
		VerificationRequest req = new VerificationRequest();
		com.ftd.avs.webservice.vo.v1.Address a = new com.ftd.avs.webservice.vo.v1.Address();
		String originalStreet = "3113 Woodcreek Dr";
		a.setStreet1(originalStreet);
		a.setCity("Downers Grove");
		a.setZip("60515");
		a.setState("IL");
		req.setAddress(a);
		req.setClientIdentifier("123");
		
				
		GeocodeResult result = new GeocodeResult();
		Address outAddress = new Address();
		outAddress.setAddressLine("3113 Woodcreek Dr");
		//Bing returns a city that says "Town Of"
		outAddress.setLocality("Town of Downers Grove");
		outAddress.setPostalCode("60515");
		outAddress.setAdminDistrict("IL");
		result.setAddress(outAddress);
		result.setEntityType("Address");
		result.setLocations(mockAOGL);
		result.setConfidence(Confidence.MEDIUM);
		
		//this is the location that will get returned by the mockAOGL
		GeocodeLocation l = new GeocodeLocation();
		l.setAltitude(0.0);
		l.setLatitude(41.828567504882813);
		l.setLongitude(-88.034416198730469);
		l.setCalculationMethod("Parcel");
		geocodeLocations.add(l);
		
		geocodeResults.add(result);
		
		VerificationResponse res  = AVSBingImpl.verifyAddress(req);
		
		//assert that the output city is the SAME as the input city since we've stripped the "Town Of"
		Assert.assertTrue(res.getVerifiedAddresses().get(0).getCity().equals(a.getCity()));
	}
	
	
	
}

