
package com.ftd.avs.webservice;

import static com.ftd.avs.common.constant.AddressVerificationServiceConstants.AVS_CONFIG;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.jws.WebService;

import net.virtualearth.dev.webservices.v1.common.ArrayOfGeocodeLocation;
import net.virtualearth.dev.webservices.v1.common.ArrayOfGeocodeResult;
import net.virtualearth.dev.webservices.v1.common.Confidence;
import net.virtualearth.dev.webservices.v1.common.Credentials;
import net.virtualearth.dev.webservices.v1.common.GeocodeLocation;
import net.virtualearth.dev.webservices.v1.common.GeocodeResult;
import net.virtualearth.dev.webservices.v1.geocode.ArrayOfFilterBase;
import net.virtualearth.dev.webservices.v1.geocode.ConfidenceFilter;
import net.virtualearth.dev.webservices.v1.geocode.GeocodeOptions;
import net.virtualearth.dev.webservices.v1.geocode.GeocodeRequest;
import net.virtualearth.dev.webservices.v1.geocode.GeocodeResponse;
import net.virtualearth.dev.webservices.v1.geocode.contracts.IGeocodeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftd.avs.common.constant.AddressVerificationServiceConstants;
import com.ftd.avs.common.dao.AVSDAO;
import com.ftd.avs.common.util.CommonUtil;
import com.ftd.avs.common.util.WebServiceClientFactory;
import com.ftd.avs.exception.IlligalOrderTypeException;
import com.ftd.avs.webservice.vo.v1.Address;
import com.ftd.avs.webservice.vo.v1.DevaluationReason;
import com.ftd.avs.webservice.vo.v1.VerificationRequest;
import com.ftd.avs.webservice.vo.v1.VerificationResponse;
import com.ftd.avs.webservice.vo.v1.VerifiedAddress;
import com.ftd.avs.webservice.vo.v1.VerifiedAddress.AVS_VENDORS;
import com.ftd.ftdutilities.GeneralConstants;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;

/**
 * 
 * @author cjohnson
 *
 */
@Service
@WebService(endpointInterface = "com.ftd.avs.webservice.AddressVerificationService")
public class AddressVerificationServiceBingImpl implements AddressVerificationService {

	Logger logger = new Logger(AddressVerificationServiceBingImpl.class.getName());
	
	
	/**
	 * this gets injected via spring-beans.xml
	 */
	protected WebServiceClientFactory webServiceClientFactory;
	
	/**
	 * We are instantiating the connection and DAO here in case we ever want to inject them
	 * AND because it makes this class more easily testable.
	 */
	protected AVSDAO dao;
	protected CommonUtil commonUtil;

	
	/**
	 * this is the regex that will allow us to find secondary unit designators
	 * it includes the space that will preceed the secondary unit designator.
	 * We define the pattern here because there is some overhead involved with compiling it.
	 */
	private Pattern findSecondaryUnitDesignatorsPattern = Pattern.compile(" (#|APT|APARTMENT|APART|BASEMENT|BSMT|BUILDING|BLDG|DEPT|DEPARTMENT |FRONT$|FRNT|HANGER|HNGR|LOBBY|LBBY|LOT |LOWR|OFC|PENTHOUSE |PH |ROOM |REAR |REAR$|RM |SLIP |SPACE|SPC |STOP |SUITE|SUIT|STE |TRLR |UNIT |UPPR ).*");
	private Pattern findFloorsPattern = Pattern.compile(" (\\d+)(ST|ND|RD|TH) (FLOOR|FL).*");
	
	@Autowired
	public void setWebServiceClientFactory(
			WebServiceClientFactory webServiceClientFactory) {
		this.webServiceClientFactory = webServiceClientFactory;
	}

	@Override
	public VerificationResponse verifyAddress(VerificationRequest request) throws Exception{
		if(request.getOrderType() != null && !request.getOrderType().trim().equals("") 
				&& !request.getOrderType().equalsIgnoreCase(GeneralConstants.AVS_ORDER_TYPE_DROPSHIP)
				&& !request.getOrderType().equalsIgnoreCase(GeneralConstants.AVS_ORDER_TYPE_FLORIST)){
			throw new IlligalOrderTypeException("Invalid Order Type.  Expected 'F' or 'D' (for Florist or Dropship)");
		}
		if (this.commonUtil == null) {
			this.commonUtil = new CommonUtil();
		}
		
		VerificationResponse response = new VerificationResponse();
		List<VerifiedAddress> vaList = new ArrayList<VerifiedAddress>();  //the response object
		Long avsRequestId = null;  // the id of the AVS_REQUEST record when it gets created

        GeocodeResponse resp = new GeocodeResponse();       		
		GeocodeRequest req = buildRequest(request);
		
		IGeocodeService geocodeSvc = webServiceClientFactory.getGeocodeService();
        Connection conn = null;
        long ts = new Date().getTime();
        long responseTime = 0;
        try {
            conn = this.commonUtil.getNewConnection();  

            int maxretries = 1;
            maxretries = setMaxRetries(maxretries);
            
			//in 0.1% of cases we've seen resp = null here.  just retry in that case
			// three shall be the number of tries and the number of tries shall be 3.
		    int numAttempt = 1;
	        
		    while (resp.getResponseSummary() == null && numAttempt <= maxretries+1) {
                ts = new Date().getTime();
		        try {
		            // Call BING to verify address
		            resp = geocodeSvc.geocode(req);
		        } catch (javax.xml.ws.WebServiceException e) { // catching timeout
		            logger.info("Connection timed out on attempt " + numAttempt + "\nError: " + e.getMessage());
		            
		            if (numAttempt > maxretries)
		            {
		                // send system error and log failure
		                logger.error("Timed out " + numAttempt + " times. Giving up.");
		                String errorMsg = "Max timeouts (" + numAttempt + ") received while trying to contact geocode service.";
		                //sendSystemMessage(errorMsg, conn);
		                throw new Exception(errorMsg);
		            }
		        }
		        numAttempt++;
		    }			
            
            //get response time for the Bing call in ms.
		    responseTime = new Date().getTime() - ts;

            if (numAttempt > 2) {
				logger.warn("Had to retry avs request " + (numAttempt-1) +" times before it was successful");
			}
			// run some business rules to re-append secondary unit designators.
			vaList = evaluateBusinessRules(request.getOrderType(), request.getAddress(), resp);
			
			response.setTraceId(resp.getResponseSummary().getTraceId());
			response.setAvsVendor(AVS_VENDORS.BING);
			
		} catch (NumberFormatException e) {
			logger.error(e);
			throw new Exception("System error occured. Contact admin/check logs");
		} catch (CacheException e) {
			logger.error(e);
			throw new Exception("Cache error occured. Contact admin/check logs");
		} catch(IlligalOrderTypeException iot){
			iot.printStackTrace();
			throw new Exception(iot.getMessage());
		} catch (Exception e) {
			logger.error(e);
			responseTime=-1;
			sendSystemMessage(e.getMessage(),conn);
			throw new Exception("Fatal error occured. Contact admin/check logs.");
		} finally {			
			//save the request info.
			if (responseTime == 0)
		         responseTime = new Date().getTime() - ts;

			ts = new Date().getTime();
			try {

				if (this.dao == null) {
					this.dao = new AVSDAO();
				}

				String vendorResponseId = ((resp != null && resp.getResponseSummary() != null) ? resp.getResponseSummary().getTraceId() : null);
				avsRequestId = dao.saveAVSRequest(request.getAddress(), request.getClientIdentifier(), responseTime, vendorResponseId, conn );
				response.setFTDRequestId(avsRequestId);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				try {conn.close(); } catch (SQLException e) { conn = null; }
			}
			logger.info("saved record in " + (new Date().getTime()-ts) + "ms");
		}		
		
		response.setVerifiedAddresses(vaList);
		return response;
	}

    private int setMaxRetries(int defaultMaxRetries)
    {
        String retries = null;
        try
        {
            retries = this.commonUtil.getFrpGlobalParm(AVS_CONFIG, AddressVerificationServiceConstants.BING_TIMEOUT_RETRIES);
        }
        catch (Exception e) 
        {
            logger.error("Error getting BING timeout retries, using default " + defaultMaxRetries);
        }
        finally
        {            
            if (retries != null)
                defaultMaxRetries = Integer.valueOf(retries);
        }
        return defaultMaxRetries;
    }

    private GeocodeRequest buildRequest(VerificationRequest request) throws Exception
    {
        //send the verified address to Bing and get a response
		//here's the weird thing.  We just send it to Bing all as one big string
		StringBuffer sb = new StringBuffer();
		sb.append(request.getAddress().getStreet1());
		if (request.getAddress().getStreet2() != null && !request.getAddress().getStreet2().isEmpty()) {
			sb.append(" " + request.getAddress().getStreet2());
		}
		sb.append(", " + request.getAddress().getCity());
		sb.append(","+ request.getAddress().getState());
		sb.append(" "+ request.getAddress().getZip());				
		
		GeocodeRequest req  = new GeocodeRequest();
		Credentials creds = new Credentials();
		creds.setApplicationId(this.commonUtil.getFrpGlobalParm(AVS_CONFIG, AddressVerificationServiceConstants.BING_CREDENTIAL_APP_ID));
		creds.setToken(null);
		
		GeocodeOptions options = new GeocodeOptions();
		//options.set
		ConfidenceFilter conf = new ConfidenceFilter();
		conf.setMinimumConfidence(Confidence.LOW);
		ArrayOfFilterBase aofb = new ArrayOfFilterBase();
		aofb.getFilterBase().add(conf);
		options.setFilters(aofb);

		req.setQuery(sb.toString());
		req.setCredentials(creds);
		req.setOptions(options);
        return req;
    }



	/**
	 * 
	 * @param inputAddress - the input address
	 * @param resp - the Bing response
	 * @return
	 * @throws Exception 
	 * @throws CacheException 
	 * @throws NumberFormatException 
	 */
	private List<VerifiedAddress> evaluateBusinessRules(String orderType, Address inputAddress, GeocodeResponse resp) throws NumberFormatException, CacheException, Exception {
		List<VerifiedAddress> vaList = new ArrayList<VerifiedAddress>();
		//for each address in the response, put it into a list.
		ArrayOfGeocodeResult results = resp.getResults();
		int resultCount = 0;
		if(results != null && results.getGeocodeResult() != null){
			resultCount = results.getGeocodeResult().size();
		}
		for (GeocodeResult result : results.getGeocodeResult()) {
			boolean hasGeoCodes = false;
			VerifiedAddress va = new VerifiedAddress();
			//need to  put the secondary unit designators back on based on a configurable control "on" or "off" which defaults to off		
			String reappendParmStr = this.commonUtil.getFrpGlobalParm(AVS_CONFIG, AddressVerificationServiceConstants.REAPPEND_SECONDARY_UNIT_DESIGNATORS);
			Boolean reappendSecondaryUnitDesignators = (reappendParmStr != null && reappendParmStr.equalsIgnoreCase("on")? true : false);
			if (reappendSecondaryUnitDesignators) {
				va.setStreet1(reAppendSecondaryUnitDesignators(inputAddress.getAddressLine(), result.getAddress().getAddressLine()));	
			} else {
				va.setStreet1(result.getAddress().getAddressLine());
			}
			String city = removeTownOfFromCity(result.getAddress().getLocality());
			va.setCity(city);
			va.setState(result.getAddress().getAdminDistrict());
			va.setZip(result.getAddress().getPostalCode());
			va.setCountry(result.getAddress().getCountryRegion());
			ArrayOfGeocodeLocation arrayOfLocations = result.getLocations();
			/*
			 * First loop through locations list and find whether we have logitudes/latitudes for calculation method Rooftop
			 * otherwise go for Parcel calculation method. if Rooftop and Parcel are not available go for Interpolation calculation method.
			 */
			List<GeocodeLocation> locations = arrayOfLocations.getGeocodeLocation();
			if(!hasGeoCodes){
				for(GeocodeLocation location : locations){
					if(location != null && location.getCalculationMethod().equalsIgnoreCase("Rooftop")){
						if(location.getLongitude() != null && location.getLatitude() != null){
							va.setLongitutde(location.getLongitude().toString());
							va.setLatitude(location.getLatitude().toString());
							hasGeoCodes = true;
						}
	
					}
				}
			}
			if(!hasGeoCodes){
				for(GeocodeLocation location : locations){
					if(location != null && location.getCalculationMethod().equalsIgnoreCase("Parcel") && !hasGeoCodes){
						if(location.getLongitude() != null && location.getLatitude() != null){
							va.setLongitutde(location.getLongitude().toString());
							va.setLatitude(location.getLatitude().toString());
							hasGeoCodes = true;
						}
	
					}
				}
			}
			if(!hasGeoCodes){
				for(GeocodeLocation location : locations){
					if(location != null && location.getCalculationMethod().equalsIgnoreCase("Interpolation") && !hasGeoCodes){
						if(location.getLongitude() != null && location.getLatitude() != null){
							va.setLongitutde(location.getLongitude().toString());
							va.setLatitude(location.getLatitude().toString());
							hasGeoCodes = true;
						}
	
					}
				}
			}
			if(!hasGeoCodes){
				for(GeocodeLocation location : locations){
					if(location != null && !hasGeoCodes){
						if(location.getLongitude() != null && location.getLatitude() != null){
							va.setLongitutde(location.getLongitude().toString());
							va.setLatitude(location.getLatitude().toString());
							hasGeoCodes = true;
						}	
					}
				}
			}
			va.setEntityType(result.getEntityType());
			va.setConfidence(result.getConfidence().toString());			
			
			calculateConfidenceRating(orderType, inputAddress, va, result, resultCount);
			
			vaList.add(va);
		}
		
		
		return vaList;
	}
	
	private String removeTownOfFromCity(String locality) {
		return locality.replaceFirst("Town of ", "");
	}

	/**
	 * Re-append the secondary unit designators (Apt#, Suite #, etc) from the original address
	 * @param addressLine
	 * @param addressLine2
	 * @return
	 */
	private String reAppendSecondaryUnitDesignators(String originalAddressLine,
			String bingVerifiedAddressLine) {
		
		//pattern match the secondary unit designators on the original address
		Matcher m;
		String match = null;
		m = findFloorsPattern.matcher(originalAddressLine.toUpperCase());
		boolean result = m.find();
		while (result) {
			match = m.group();
			result = m.find();
		}
		
		
		//second pass to find floors
		if (match == null) {
			m = findSecondaryUnitDesignatorsPattern.matcher(originalAddressLine.toUpperCase());
			result = m.find();
			while (result) {
				match = m.group();
				result = m.find();
			}
		}

		
		if (match != null) {
			bingVerifiedAddressLine += match;
		}
		
		return bingVerifiedAddressLine;
	}

	/** Calculate a confidence rating by evaluating a series of business rules against the verified address
	 * The confidence rating will get added to the verified address
	 * 
	 * @param bingResp - the Bing verified address
	 * @param address - the input address
	 * @throws Exception 
	 * @throws CacheException 
	 * @throws NumberFormatException 
	 */
	private void calculateConfidenceRating(String orderType, Address address, VerifiedAddress va, GeocodeResult geoResult, int resultCount) throws NumberFormatException, CacheException, Exception {
		
		//initialize the score map
		Map<String, Integer> errorScoreMap = new HashMap<String, Integer>();
		errorScoreMap.put(GeneralConstants.AVS_SCORE_ADDRESS_LINE_DIFFERENT, 0);
		errorScoreMap.put(GeneralConstants.AVS_SCORE_CITY_DIFFERENT, 0);
		errorScoreMap.put(GeneralConstants.AVS_SCORE_STATE_DIFFERENT, 0);
		errorScoreMap.put(GeneralConstants.AVS_SCORE_ZIP_DIFFERENT,0);
		errorScoreMap.put(GeneralConstants.AVS_SCORE_ZIPPLUSFOUR_DIFFERENT, 0);
		errorScoreMap.put(GeneralConstants.AVS_SCORE_HIGH_CONFIDENCE,0);
		errorScoreMap.put(GeneralConstants.AVS_SCORE_MEDIUM_CONFIDENCE, 0);
		errorScoreMap.put(GeneralConstants.AVS_SCORE_LOW_CONFIDENCE, 0);
		errorScoreMap.put(GeneralConstants.AVS_SCORE_ADDRESS_TYPE_ADDRESS, 0);
		errorScoreMap.put(GeneralConstants.AVS_SCORE_ADDRESS_TYPE_OTHER, 0);
		
		
		List<DevaluationReason> devaluationReasons = new ArrayList<DevaluationReason>();
		
		Integer errorScore = 0;
		
		if (geoResult.getConfidence().equals(Confidence.HIGH)) {
			//bing confidence HIGH
			errorScoreMap.put(GeneralConstants.AVS_SCORE_HIGH_CONFIDENCE, 
					Integer.valueOf(this.commonUtil.getFrpGlobalParm(AVS_CONFIG, GeneralConstants.AVS_SCORE_HIGH_CONFIDENCE)));
		}
		else if (geoResult.getConfidence().equals(Confidence.MEDIUM)) {
			//bing confidence MEDIUM			
			errorScoreMap.put(GeneralConstants.AVS_SCORE_MEDIUM_CONFIDENCE, 
					Integer.valueOf(this.commonUtil.getFrpGlobalParm(AVS_CONFIG, GeneralConstants.AVS_SCORE_MEDIUM_CONFIDENCE)));
		}
		else if (geoResult.getConfidence().equals(Confidence.LOW)) {
			//bing confidence LOW			
			errorScoreMap.put(GeneralConstants.AVS_SCORE_LOW_CONFIDENCE, 
					Integer.valueOf(this.commonUtil.getFrpGlobalParm(AVS_CONFIG, GeneralConstants.AVS_SCORE_LOW_CONFIDENCE)));
		}
		
		//normalize the addresses to uppercase without periods
		if (!va.getAddressLine().toUpperCase().replace(".","").equals(
				address.getAddressLine().toUpperCase().replace(".",""))) {
			errorScoreMap.put(GeneralConstants.AVS_SCORE_ADDRESS_LINE_DIFFERENT, 
					Integer.valueOf(this.commonUtil.getFrpGlobalParm(AVS_CONFIG, GeneralConstants.AVS_SCORE_ADDRESS_LINE_DIFFERENT)));
		}

		if (!va.getCity().toUpperCase().equals(
				address.getCity().toUpperCase())) {
			errorScoreMap.put(GeneralConstants.AVS_SCORE_CITY_DIFFERENT, 
					Integer.valueOf(this.commonUtil.getFrpGlobalParm(AVS_CONFIG, GeneralConstants.AVS_SCORE_CITY_DIFFERENT)));
		}
		
		
		
		/**
		 * Important info about zip comparisons
		 * The zip is defined as any characters before a dash "-" character
		 * 
		 * the zipplusfour is defined as any characters after a dash "-" character.
		 * 
		 * If there is no dash, the the zipplusfour will be an empty string.
		 * 
		 */
		
		//get the zip+4 from the bing address
		String bingZipPlusFour = "";
		String bingZip = geoResult.getAddress().getPostalCode();
		String[] bingZipArr = geoResult.getAddress().getPostalCode().split("-");
		if (bingZipArr.length > 1) {
			bingZip = bingZipArr[0];
			bingZipPlusFour = bingZipArr[1];
		}
		
		//get the zip+4 from the input address
		String inputZipPlusFour = "";
		String inputZip = address.getZip();
		String[] inputZipArr = address.getZip().split("-");
		if (inputZipArr.length > 1) {
			inputZip = inputZipArr[0];
			inputZipPlusFour = inputZipArr[1];
		}
		
		//check for different zips
		if (!bingZip.equals(inputZip)) {
			errorScoreMap.put(GeneralConstants.AVS_SCORE_ZIP_DIFFERENT, 
					Integer.valueOf(this.commonUtil.getFrpGlobalParm(AVS_CONFIG, GeneralConstants.AVS_SCORE_ZIP_DIFFERENT)));
		}
		
		
		//check for different zip+4s
		if (!bingZipPlusFour.equals(inputZipPlusFour)) {
			errorScoreMap.put(GeneralConstants.AVS_SCORE_ZIPPLUSFOUR_DIFFERENT, 
					Integer.valueOf(this.commonUtil.getFrpGlobalParm(AVS_CONFIG, GeneralConstants.AVS_SCORE_ZIPPLUSFOUR_DIFFERENT)));
		}
		
		//check for different state
		if (!va.getState().toUpperCase().equals(address.getState().toUpperCase().replace(".",""))) {
			errorScoreMap.put(GeneralConstants.AVS_SCORE_STATE_DIFFERENT, 
					Integer.valueOf(this.commonUtil.getFrpGlobalParm(AVS_CONFIG, GeneralConstants.AVS_SCORE_STATE_DIFFERENT)));
		}
		
		//entity type address
		if (va.getEntityType().equals("Address")) {
			errorScoreMap.put(GeneralConstants.AVS_SCORE_ADDRESS_TYPE_ADDRESS, 
					Integer.valueOf(this.commonUtil.getFrpGlobalParm(AVS_CONFIG, GeneralConstants.AVS_SCORE_ADDRESS_TYPE_ADDRESS)));
		} else {
			errorScoreMap.put(GeneralConstants.AVS_SCORE_ADDRESS_TYPE_OTHER, 
					Integer.valueOf(this.commonUtil.getFrpGlobalParm(AVS_CONFIG, GeneralConstants.AVS_SCORE_ADDRESS_TYPE_OTHER)));
		}
		
		
		
		//put all of the reasons onto the verified address
		int totalScore = 0;
		Iterator<Map.Entry<String, Integer>> it = errorScoreMap.entrySet().iterator();
		DevaluationReason devalReason;
		while (it.hasNext()) {
			Map.Entry<String, Integer> pair = it.next();
			devalReason = new DevaluationReason();
			devalReason.setReasonKey(pair.getKey());
			devalReason.setReasonScore(pair.getValue());
			devaluationReasons.add(devalReason);
			totalScore = totalScore + pair.getValue();
		}
		va.setDevaluationReasons(devaluationReasons);
		va.setFTDConfidenceScore(totalScore);
		
		if(orderType != null && orderType.equalsIgnoreCase(GeneralConstants.AVS_ORDER_TYPE_DROPSHIP)){
			va.setConfidenceThreshold(Integer.valueOf(this.commonUtil.getFrpGlobalParm(AVS_CONFIG, GeneralConstants.AVS_SCORE_THRESHOLD_DROPSHIP)));
			if(totalScore < Integer.valueOf(this.commonUtil.getFrpGlobalParm(AVS_CONFIG, GeneralConstants.AVS_SCORE_THRESHOLD_DROPSHIP))){
				va.setStatus(GeneralConstants.AVS_VERIFIED_ADDRESS_PASS);
			}else{
				va.setStatus(GeneralConstants.AVS_VERIFIED_ADDRESS_FAIL);
			}
			
		}else if(orderType != null && orderType.equalsIgnoreCase(GeneralConstants.AVS_ORDER_TYPE_FLORIST)){
			va.setConfidenceThreshold(Integer.valueOf(this.commonUtil.getFrpGlobalParm(AVS_CONFIG, GeneralConstants.AVS_SCORE_THRESHOLD_FLORIST)));
			if(totalScore < Integer.valueOf(this.commonUtil.getFrpGlobalParm(AVS_CONFIG, GeneralConstants.AVS_SCORE_THRESHOLD_FLORIST))){
				va.setStatus(GeneralConstants.AVS_VERIFIED_ADDRESS_PASS);
			}else{
				va.setStatus(GeneralConstants.AVS_VERIFIED_ADDRESS_FAIL);
			}
		}
		
	}
	
	
    /**
     * This method sends a message to the System Messenger.
     * @param message
     * @return message id
     */
	public void sendSystemMessage(String message, Connection connection) throws Exception {
	    String messageID = "";

	    String messageSource = "AVS";

	    //build system vo
	    SystemMessengerVO sysMessage = new SystemMessengerVO();
	    sysMessage.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
	    sysMessage.setSource(messageSource);
	    sysMessage.setType("ERROR");
	    sysMessage.setMessage(message);
	    sysMessage.setSubject("AVS ERROR - Address verification failed");

	    SystemMessenger sysMessenger = SystemMessenger.getInstance();
	    // Passing the flag to not close connection.
	    messageID = sysMessenger.send(sysMessage, connection, false);

	    if (messageID == null) {
	        String msg = "Error occured while attempting to send out a system message. Msg not sent: " + message;
	        throw new Exception(msg);
	    }

	}
}
