package com.ftd.avs.webservice.vo.v1;

import java.util.List;

/**
 * This class gets used as part of the AVS service response.
 * Changing this class will change the WSDL!!!!!
 * Since you should never change a WSDL, any changes to a contract should get put
 * into a different package (like com.ftd.avs.webservice.vo.v2)
 * and then a new method should be exposed on the wsdl using the new VOs.
 * 
 * @author cjohnson
 *
 */
public class VerifiedAddress extends Address {
	
	private String displayName;
	private String confidence;
	private String entityType;
	private String latitude;
	private String longitutde;
	private Boolean isStreetDifferent;
	private Boolean isCityDifferent;
	private Boolean isStateDifferent;
	private Boolean isZipDifferent;
	private Boolean isCountryDifferent;
	private Boolean isMultiple;
	private Integer FTDConfidenceScore;
	private Integer confidenceThreshold;
	private Boolean isValid;
	private String status;
	private List<DevaluationReason> devaluationReasons;
	
	private AVS_VENDORS avsVendor;
	
	public enum AVS_VENDORS {
		G1,
		BING
	}
	
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getConfidence() {
		return confidence;
	}
	public void setConfidence(String confidence) {
		this.confidence = confidence;
	}
	public String getEntityType() {
		return entityType;
	}
	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitutde() {
		return longitutde;
	}
	public void setLongitutde(String longitutde) {
		this.longitutde = longitutde;
	}
	public void setAvsVendor(AVS_VENDORS avsVendor) {
		this.avsVendor = avsVendor;
	}
	public AVS_VENDORS getAvsVendor() {
		return avsVendor;
	}
	public Boolean getIsStreetDifferent() {
		return isStreetDifferent;
	}
	public void setIsStreetDifferent(Boolean isStreetDifferent) {
		this.isStreetDifferent = isStreetDifferent;
	}
	public Boolean getIsCityDifferent() {
		return isCityDifferent;
	}
	public void setIsCityDifferent(Boolean isCityDifferent) {
		this.isCityDifferent = isCityDifferent;
	}
	public Boolean getIsStateDifferent() {
		return isStateDifferent;
	}
	public void setIsStateDifferent(Boolean isStateDifferent) {
		this.isStateDifferent = isStateDifferent;
	}
	public Boolean getIsZipDifferent() {
		return isZipDifferent;
	}
	public void setIsZipDifferent(Boolean isZipDifferent) {
		this.isZipDifferent = isZipDifferent;
	}
	public Boolean getIsCountryDifferent() {
		return isCountryDifferent;
	}
	public void setIsCountryDifferent(Boolean isCountryDifferent) {
		this.isCountryDifferent = isCountryDifferent;
	}
	public Boolean getIsMultiple() {
		return isMultiple;
	}
	public void setIsMultiple(Boolean isMultiple) {
		this.isMultiple = isMultiple;
	}
	public Integer getFTDConfidenceScore() {
		return FTDConfidenceScore;
	}
	public void setFTDConfidenceScore(Integer fTDConfidence) {
		FTDConfidenceScore = fTDConfidence;
	}
	public Boolean getIsValid() {
		return isValid;
	}
	public void setIsValid(Boolean isValid) {
		this.isValid = isValid;
	}
	
	public void setConfidenceThreshold(Integer confidenceThreshold) {
		this.confidenceThreshold = confidenceThreshold;
	}
	public Integer getConfidenceThreshold() {
		return confidenceThreshold;
	}
	public List<DevaluationReason> getDevaluationReasons() {
		return devaluationReasons;
	}
	public void setDevaluationReasons(List<DevaluationReason> devaluationReasons) {
		this.devaluationReasons = devaluationReasons;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	


}
