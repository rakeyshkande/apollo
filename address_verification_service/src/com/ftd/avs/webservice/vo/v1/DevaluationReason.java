package com.ftd.avs.webservice.vo.v1;

public class DevaluationReason {
	private String reasonKey;
	private Integer reasonScore;
	public String getReasonKey() {
		return reasonKey;
	}
	public void setReasonKey(String reasonKey) {
		this.reasonKey = reasonKey;
	}
	public Integer getReasonScore() {
		return reasonScore;
	}
	public void setReasonScore(Integer reasonScore) {
		this.reasonScore = reasonScore;
	}
	
	
	
	

}
