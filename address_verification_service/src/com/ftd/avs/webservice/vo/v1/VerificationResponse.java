package com.ftd.avs.webservice.vo.v1;

import java.util.List;

import com.ftd.avs.webservice.vo.v1.VerifiedAddress.AVS_VENDORS;

/**
 * object to hold the top level response for the AVS service.
 * Changing this class will change the WSDL!!!!!
 * Since you should never change a WSDL, any changes to a contract should get put
 * into a different package (like com.ftd.orderservice.webservice.vo.v2)
 * 
 * @author cjohnson
 *
 */
public class VerificationResponse {
	
	private AVS_VENDORS avsVendor;
	private String traceId;
	private Long FTDRequestId;
	private List<VerifiedAddress> verifiedAddresses;
	

	public String getTraceId() {
		return traceId;
	}
	public void setTraceId(String traceId) {
		this.traceId = traceId;
	}
	public Long getFTDRequestId() {
		return FTDRequestId;
	}
	public void setFTDRequestId(Long fTDRequestId) {
		FTDRequestId = fTDRequestId;
	}
	public List<VerifiedAddress> getVerifiedAddresses() {
		return verifiedAddresses;
	}
	public void setVerifiedAddresses(List<VerifiedAddress> verifiedAddresses) {
		this.verifiedAddresses = verifiedAddresses;
	}
	public AVS_VENDORS getAvsVendor() {
		return avsVendor;
	}
	public void setAvsVendor(AVS_VENDORS avsVendor) {
		this.avsVendor = avsVendor;
	}

}
