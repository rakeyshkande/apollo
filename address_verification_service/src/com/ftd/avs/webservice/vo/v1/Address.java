package com.ftd.avs.webservice.vo.v1;

/**
 * This class gets used as part of the AVS service response.
 * Changing this class will change the WSDL!!!!!
 * Since you should never change a WSDL, any changes to a contract should get put
 * into a different package (like com.ftd.orderservice.webservice.vo.v2)
 * and then a new method should be exposed on the wsdl using the new VOs.
 * 
 * @author cjohnson
 *
 */
public class Address {
	
	private String street1;
	private String street2;
	private String city;
	private String state;
	private String zip;
	private String country;
	
	
	
	public String getAddressLine() {
		return street1 + (street2 != null && !street2.isEmpty() ? " " + street2 : "");
	}
	
	public String getStreet1() {
		return street1;
	}
	public void setStreet1(String street1) {
		this.street1 = street1;
	}
	public String getStreet2() {
		return street2;
	}
	public void setStreet2(String street2) {
		this.street2 = street2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	

	

}
