package com.ftd.avs.webservice.vo.v1;

/**
 * Top level object for the AVS Service request.
 * 
 * Changing this class will change the WSDL!!!!!
 * Since you should never change a WSDL, any changes to a contract should get put
 * into a different package (like com.ftd.avs.webservice.vo.v2)
 * 
 * @author cjohnson
 *
 */
public class VerificationRequest {
	
	private Address address;
	private String clientIdentifier;
	private String orderType;

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getClientIdentifier() {
		return clientIdentifier;
	}

	public void setClientIdentifier(String clientIdentifier) {
		this.clientIdentifier = clientIdentifier;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	
	

}
