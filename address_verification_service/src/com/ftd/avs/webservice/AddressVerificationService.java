
package com.ftd.avs.webservice;

import java.util.List;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import com.ftd.avs.webservice.vo.v1.Address;
import com.ftd.avs.webservice.vo.v1.VerificationRequest;
import com.ftd.avs.webservice.vo.v1.VerificationResponse;
import com.ftd.avs.webservice.vo.v1.VerifiedAddress;


/**
 * Web service interface for an address verification service
 * 
 * @author cjohnson
 *
 */
@WebService
public interface AddressVerificationService {
	
	/**
	 *  given some address, attempt to verify the address against some AVS implementation. 
	 * @param address - the address to be verified
	 * @return a list of suggested addresses.  May be zero to many
	 */
	@WebResult(name="verifiedAddress")
    VerificationResponse verifyAddress(@WebParam(name="verificationRequest")VerificationRequest request) throws Exception;
}
