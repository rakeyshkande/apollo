package com.ftd.avs.common.constant;


public class AddressVerificationServiceConstants
{
    public static final String APPLICATION_NAME = "ORDER_SERVICE";
    //public static final String STATS_CAT_REQUEST_RECEIVED = "REQUEST_RECEIVED";
    public static final String STATS_CAT_REQUEST_PROCESSED = "REQUEST_PROCESSED";
    public static final String STATS_CAT_TRANS_CACHE_HIT = "TRANS_CACHE_HIT";
    public static final String DB_GET_PRODUCT_INFO = "DB_GET_PRODUCT_INFO";
    public static final String DB_INSERT_AVS_REQUEST = "INSERT_AVS_REQUEST";
    public final static String PROPERTY_FILE = "avs-config.xml";
    public final static String DATASOURCE_NAME = "DATASOURCE";
    public final static String LOAD_TEST_CACHE = "DATASOURCE";
    public final static String TEST_MODE = "TEST_MODE";
    public static final String DATE_FORMAT_DD = "MM/dd/yyyy";
    public static final String DATE_FORMAT_NOW = "yyyy-MM-dd HH:mm:ss.SSS";

    // Request parameters
    public static final String REQUEST_PARM_ACTION_TYPE = "serviceType";
    // paremeter common to all partners.
    public static final String REQUEST_PARM_CLIENT_ID = "clientId";
    
    // request parameters that need to looked up in config file.
    public static final String REQUEST_PARM_SOURCE_CODE = "REQUEST_PARM_SOURCE_CODE";
    public static final String REQUEST_PARM_REQUEST_ID = "REQUEST_PARM_REQUEST_ID";
    public static final String REQUEST_PARM_WEB_PRODUCT_ID = "REQUEST_PARM_WEB_PRODUCT_ID";
    public static final String REQUEST_PARM_LOAD_LEVEL = "REQUEST_PARM_LOAD_LEVEL";
    public static final String REQUEST_PARM_CATEGORY_ID = "REQUEST_PARM_CATEGORY_ID";
    public static final String REQUEST_PARM_DELIVERY_DATE = "REQUEST_PARM_DELIVERY_DATE";
    public static final String REQUEST_PARM_ZIP_CODE = "REQUEST_PARM_ZIP_CODE";
    public static final String REQUEST_PARM_COUNTRY = "REQUEST_PARM_COUNTRY";
    public static final String REQUEST_PARM_DAYS = "REQUEST_PARM_DAYS";
    public static final String REQUEST_PARM_PRODUCT_COUNT = "REQUEST_PARM_PRODUCT_COUNT";
    public static final String REQUEST_PARM_PRODUCT_ORDER_BY = "REQUEST_PARM_PRODUCT_ORDER_BY";
    public static final String REQUEST_PARM_PRODUCT_PRICE = "REQUEST_PARM_PRODUCT_PRICE";
    
    // System messaging
    public static final String SM_PAGE_SOURCE = "ORDER_SERVICE_PAGE";
    public static final String SM_NOPAGE_SOURCE = "ORDER_SERVICE_NOPAGE";
    public static final String SM_PAGE_SUBJECT = "Order Service System Message";
    public static final String SM_NOPAGE_SUBJECT = "NOPAGE Order Service System Message";
    public static final String SM_TYPE = "System Exception";
    
    // Validation errors
    public static final String VALIDATION_ERROR_MISSING_CLIENT_ID = "VALIDATION_ERROR_MISSING_CLIENT_ID";
    public static final String VALIDATION_ERROR_INVALID_CLIENT_ID = "VALIDATION_ERROR_INVALID_CLIENT_ID";
    public static final String VALIDATION_ERROR_INVALID_ZIP_CODE = "VALIDATION_ERROR_INVALID_ZIP_CODE";
    public static final String VALIDATION_ERROR_INVALID_COUNTRY_ID = "VALIDATION_ERROR_INVALID_COUNTRY_ID";
    public static final String VALIDATION_ERROR_INVALID_DELIVERY_DATE_FORMAT = "VALIDATION_ERROR_INVALID_DELIVERY_DATE_FORMAT";
    public static final String GENERIC_ERROR_CANNOT_FULFILL_REQUEST = "GENERIC_ERROR_CANNOT_FULFILL_REQUEST";
    public static final String VALIDATION_ERROR_VENDOR_PRODUCT_NO_DELIVERY_DATES = "VALIDATION_ERROR_VENDOR_PRODUCT_NO_DELIVERY_DATES";
    public static final String VALIDATION_ERROR_FLORIST_PRODUCT_NO_DELIVERY_DATES_US = "VALIDATION_ERROR_FLORIST_PRODUCT_NO_DELIVERY_DATES_US";
    public static final String VALIDATION_ERROR_FLORIST_PRODUCT_NO_DELIVERY_DATES_CA = "VALIDATION_ERROR_FLORIST_PRODUCT_NO_DELIVERY_DATES_CA";
    public static final String VALIDATION_ERROR_PRODUCT_UNAVAILABLE = "VALIDATION_ERROR_PRODUCT_UNAVAILABLE";
    public static final String VALIDATION_ERROR_FLORIST_PRODUCT_NO_DELIVERY_DATES_INT = "VALIDATION_ERROR_FLORIST_PRODUCT_NO_DELIVERY_DATES_INT";

    public static final String VALIDATION_ERROR_INVALID_AVAILABLE_DAYS = "VALIDATION_ERROR_INVALID_AVAILABLE_DAYS";
    //public static final String VALIDATION_ERROR_INVALID_PRODUCT_COUNT = "VALIDATION_ERROR_INVALID_PRODUCT_COUNT";
    public static final String VALIDATION_ERROR_INVALID_SERVICE_REQUESTED = "VALIDATION_ERROR_INVALID_SERVICE_REQUESTED";
    public static final String VALIDATION_ERROR_INVALID_CUSTOMER_EMAIL = "VALIDATION_ERROR_INVALID_CUSTOMER_EMAIL";   
    public static final String VALIDATION_ERROR_INVALID_CUSTOMER_SIGNED_IN_FLAG= "VALIDATION_ERROR_INVALID_CUSTOMER_SIGNED_IN_FLAG";
    public static final String VALIDATION_ERROR_INVALID_FREE_SHIP_MEMBER_IN_CART_FLAG = "VALIDATION_ERROR_INVALID_FREE_SHIP_MEMBER_IN_CART_FLAG";
    
    // Request type supported:
     public static final String REQUEST_TYPE_GET_PRODUCT_BY_PRODUCT_ID = "GET_PRODUCT_BY_PRODUCT_ID";
     public static final String REQUEST_TYPE_GET_PRODUCT_BY_PRODUCT_ID_ZIP = "GET_PRODUCT_BY_PRODUCT_ID_ZIP";
     public static final String REQUEST_TYPE_GET_PRODUCT_BY_PRODUCT_ID_ZIP_DATE = "GET_PRODUCT_BY_PRODUCT_ID_ZIP_DATE";
     public static final String REQUEST_TYPE_GET_PRODUCT_BY_CATEGORY_ID = "GET_PRODUCT_BY_CATEGORY_ID";
     public static final String REQUEST_TYPE_GET_PRODUCT_BY_CATEGORY_ID_ZIP = "GET_PRODUCT_BY_CATEGORY_ID_ZIP"; 
     public static final String REQUEST_TYPE_GET_PRODUCT_BY_CATEGORY_ID_ZIP_DATE = "GET_PRODUCT_BY_CATEGORY_ID_ZIP_DATE";
     public static final String REQUEST_PARM_CUSTOMER_EMAIL = "REQUEST_PARM_CUSTOMER_EMAIL";
     public static final String REQUEST_PARM_CUSTOMER_SIGNED_IN_FLAG = "REQUEST_PARM_CUSTOMER_SIGNED_IN_FLAG";
     public static final String REQUEST_PARM_FREE_SHIP_MEMBER_IN_CART_FLAG = "REQUEST_PARM_FREE_SHIP_MEMBER_IN_CART_FLAG";
     
     public static final String COUNTRY_US = "US";
    
     public static final String CACHE_KEY_DELIM = ",";
    
     public static final String GBB_TOKEN_1 = "__PRICE2-1DIFF__";
     public static final String GBB_TOKEN_2 = "__PRICE3-1DIFF__";
     public static final String GBB_TOKEN_3 = "__PRICE3-2DIFF__";
     public static final String GBB_TOKEN_4 = "__PRICE__";
     
     public static final String CACHE_KEY_ALL = "ALL";
     
     // discounts
     public static final String IGNORE_PRICE_CODE_VALUE = "ZZ";
     //DISCOUNT TYPES
     public static final String DISCOUNT_TYPE_MILES = "M";
     public static final String DISCOUNT_TYPE_DOLLARS = "D";
     public static final String DISCOUNT_TYPE_PERCENT = "P";  
     public static final int AMOUNT_SCALE = 2;
    
     // Calcuation basis
     public static final String CALC_BASIS_FIXED = "F";
     public static final String CALC_BASIS_MERCH = "M";
     public static final String CALC_BASIS_TOTAL = "T";
     
     // default company id
     public static final String DEFAULT_COMPANY_ID = "FTD";
     
     // content for errors
     public static final String ORDER_SERVICE_CLIENT_ERROR = "ORDER_SERVICE_CLIENT_ERROR";
     public static final String ORDER_SERVICE_CONTEXT = "ORDER_SERVICE_CONTEXT";
     public static final String IOTW_MESSAGING_API = "IOTW_messaging_API";
     ////////////////////////////////////// VALUE NEEDS TO BE UPDATED/////////////////
     public static final String IOTW_MESSAGING_API_DEFAULT = "On Sale!";
    ////////////////////////////////////// VALUE NEEDS TO BE UPDATED/////////////////
     // content for image url
     public static final String ORDER_SERVICE_IMAGE_URL = "WEBSITE_STANDARD_IMAGE_URL";
     public static final String NAME_IMAGE_CATEGORY = "IMAGE_CATEGORY";
     public static final String NAME_IMAGE_CATEGORY_INTL = "IMAGE_CATEGORY_INTL";
     public static final String NAME_IMAGE_GBB = "IMAGE_GBB";
     public static final String NAME_IMAGE_ADDON = "IMAGE_ADDON";
     public static final String FILTER_GBB_GOOD = "GOOD";
     public static final String FILTER_GBB_BETTER= "BETTER";
     public static final String FILTER_GBB_BEST = "BEST";
     public static final String FILTER_GBB_TYPE_CATEGORY = "CATEGORY";
     public static final String FILTER_GBB_TYPE_PRODUCT = "PRODUCT";
     public static final String FILTER_GBB_TYPE_THUMBNAIL = "THUMBNAIL";
     
     // Global parms
     public static final String AVS_CONFIG = "AVS_CONFIG";   
     public static final String GET_OCCASION_INDEX_NAME = "GET_OCCASION_INDEX_NAME";    
     public static final String GET_PRODUCT_MAX_COUNT = "GET_PRODUCT_MAX_COUNT"; 
     public static final String CONTEXT_PDB_CONFIG = "PDB_CONFIG";
     public static final String GBB_DEFAULT_TITLE = "GBB_DEFAULT_TITLE";
     public static final String CONTEXT_FTDAPPS_PARMS = "FTDAPPS_PARMS";
     public static final String FLORAL_LABEL_STANDARD = "FLORAL_LABEL_STANDARD";
     public static final String FLORAL_LABEL_DELUXE = "FLORAL_LABEL_DELUXE";
     public static final String FLORAL_LABEL_PREMIUM = "FLORAL_LABEL_PREMIUM";
     public static final String EXCLUDED_INDEX = "EXCLUDED_INDEX";
     public static final String RETURN_ALL_ADDON_FLAG = "RETURN_ALL_ADDON_FLAG";
     public final static String ADDON_VALUES_CONTEXT = "ADDON_VALUES";
     public final static String MAX_DISPLAY_COUNT_NAME = "MAX_DISPLAY_COUNT";
     public final static String MAX_VASE_DISPLAY_COUNT_NAME = "MAX_VASE_DISPLAY_COUNT";
     public final static String INCLUDE_FUNERAL_BANNER_FLAG = "INCLUDE_FUNERAL_BANNER_FLAG";
     public final static String GENERIC_OCCASION_DISP_NAME = "GENERIC_OCCASION_DISP_NAME";
     public final static String GET_DELIVERY_DAYS_MAX_COUNT = "GET_DELIVERY_DAYS_MAX_COUNT";
     
     // GBB
     public static final String GBB_PRICE_LABEL_STANDARD = "Standard Price";
     public static final String GBB_PRICE_LABEL_DELUXE = "Deluxe Price";
     public static final String GBB_PRICE_LABEL_PREMIUM = "Premium Price";

     // Content token replacement
     public static final String CONTENT_REPLACE_TOKEN_1 = "~REPLACE_TOKEN_1~";
     public static final String CONTENT_REPLACE_SOURCE_CODE = "~sourceCode~";
     public static final String CONTENT_REPLACE_DELIVERY_DATE = "~deliveryDate~";
     public static final String BASE_URL = "__BASEURL__";
     public static final String ADDON_PRICE_TYPE = "base";
     public static final String ADDON_PRICE_UNIT = "Each";
     public static final String DATE_UNAVAILABLE = "DATE_UNAVAILABLE";
     public static final String ZIP_CODE_AND_DATE_UNAVAILABLE = "ZIP_CODE_AND_DATE_UNAVAILABLE";
     
     public static final String NAME_ADDON_OCCA_DISP_NAME = "ADDON_OCCASION_DISPLAY_NAME";
     public static final String ACTION_TYPE_ERROR = "actionTypeError";
     
     // Request Status Values
     public static final String REQUEST_SUCCESS_STATUS = "STATUS=SUCCESS";
     public static final String REQUEST_PARTIAL_SUCCESS_STATUS = "STATUS=PARTIAL_SUCCESS";
     public static final String REQUEST_FAILURE_STATUS = "STATUS=FAILURE";
     
     // Address Verification Global Parms
     public static final String AVS_BING_SERVICE_URL = "avs_bing_service_url";
     public static final String AVS_SCORE_IS_MULTIPLE = "avs_score_is_multiple";
	 public static final String BING_CREDENTIAL_APP_ID = "bing_credential_app_id";
	 public static final String REAPPEND_SECONDARY_UNIT_DESIGNATORS = "reappend_secondary_unit_designators";
     public static final String BING_TIMEOUT_RETRIES = "bing_timeout_retries";
     public static final String BING_TIMEOUT_PERIOD = "bing_timeout_period";
     
     public static final String WSDL_EXTENSION = "?wsdl";

     
}