package com.ftd.avs.common.util;

import java.io.StringWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import com.ftd.avs.common.constant.AddressVerificationServiceConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.CacheUtil;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.CompanyMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.SourceMasterVO;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.xml.DOMUtil;


public class CommonUtil {
    protected static Logger logger  = new Logger("com.ftd.orderservice.common.util.CommonUtil");
    private static final int PAD_RIGHT = 0; 
    private static final int PAD_LEFT = 1; 
    private static final int lowerBound = 1000;
    private static final int upperBound = 10000;
    
    public void sendPageSystemMessage(String logMessage) {
        Connection con = null;
        try {
             con = this.getNewConnection();
             String appSource = AddressVerificationServiceConstants.SM_PAGE_SOURCE;
             String errorType = AddressVerificationServiceConstants.SM_TYPE;
             String subject = AddressVerificationServiceConstants.SM_PAGE_SUBJECT;
             int pageLevel = SystemMessengerVO.LEVEL_PRODUCTION;

             SystemMessengerVO systemMessengerVO = new SystemMessengerVO();
             systemMessengerVO.setLevel(pageLevel);
             systemMessengerVO.setSource(appSource);
             systemMessengerVO.setType(errorType);
             systemMessengerVO.setSubject(subject);
             systemMessengerVO.setMessage(logMessage);
             String result = SystemMessenger.getInstance().send(systemMessengerVO, con, false);
         
         } catch (Exception ex) {
            // Do not attempt to send system message it requires obtaining a connection
            // and may end up in an infinite loop.
             logger.error(ex);
         } finally {
             if(con != null) {
                try {
                    con.close();
                } catch (SQLException sx) {
                    logger.error(sx);
                }
             }
         }
    
    }
    
    public void sendNoPageSystemMessage(String logMessage) {
        Connection con = null;
        try {
             con = this.getNewConnection();
             String appSource = AddressVerificationServiceConstants.SM_PAGE_SOURCE;
             String errorType = AddressVerificationServiceConstants.SM_TYPE;
             String subject = AddressVerificationServiceConstants.SM_NOPAGE_SUBJECT;
             int pageLevel = SystemMessengerVO.LEVEL_PRODUCTION;

             SystemMessengerVO systemMessengerVO = new SystemMessengerVO();
             systemMessengerVO.setLevel(pageLevel);
             systemMessengerVO.setSource(appSource);
             systemMessengerVO.setType(errorType);
             systemMessengerVO.setSubject(subject);
             systemMessengerVO.setMessage(logMessage);
             String result = SystemMessenger.getInstance().send(systemMessengerVO, con, false);
         
         } catch (Exception ex) {
            // Do not attempt to send system message it requires obtaining a connection
            // and may end up in an infinite loop.
             logger.error(ex);
         } finally {
             if(con != null) {
                try {
                    con.close();
                } catch (SQLException sx) {
                    logger.error(sx);
                }
             }
         }
    
    }
    
    /**
     * Get a new database connection.
     * @return
     * @throws Exception
     */
    public Connection getNewConnection() throws Exception
    {
        // get database connection
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        Connection conn = null;

        String datasource = configUtil.getPropertyNew(AddressVerificationServiceConstants.PROPERTY_FILE, AddressVerificationServiceConstants.DATASOURCE_NAME);
        conn = DataSourceUtil.getInstance().getConnection(datasource);

        return conn;
    }    
    
    /**
     * Get today's date formated
     * @return
     */
    public static String now() throws Exception {
      return getDateFormated(AddressVerificationServiceConstants.DATE_FORMAT_NOW, 0);
    }    

    /**
     * Get a formated date relative to today.
     * @return
     */
    public static String getDateFormated(String format, int offset) throws Exception {
      Date dt = getDate(offset);
      SimpleDateFormat sdf = new SimpleDateFormat(format);
      return sdf.format(dt);
    }  
    
    /**
     * Returns a date relative to today.
     * @param offset
     * @return
     */
    public static Date getDate(int offset) {
      Calendar cal = Calendar.getInstance();
      cal.add(Calendar.DATE, offset);
      return cal.getTime();
    }  

    /**
     * Returns a date relative to today.
     * @param offset
     * @return
     */
    public static Date getDateTruncated(int offset) throws Exception {
      Calendar cal = Calendar.getInstance();
      cal.add(Calendar.DATE, offset);
      
      SimpleDateFormat sdf = new SimpleDateFormat(AddressVerificationServiceConstants.DATE_FORMAT_DD);
      String dateString = sdf.format(cal.getTime());
      Date truncatedDate = sdf.parse(dateString);
      return truncatedDate;
    }  
    
    public static String dateToString(Date dt) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat(AddressVerificationServiceConstants.DATE_FORMAT_DD);
        return sdf.format(dt);
    }
    
    /**
     * Returns a date relative to today.
     * @param offset
     * @return
     */
    public static Calendar getCalendarTruncated(int offset) throws Exception {
      Calendar cal = Calendar.getInstance();
      Date dt = getDateTruncated(offset);
      cal.setTime(dt);
      return cal;
    }      
    /*
    public BigDecimal getGBBPriceDiff(String token, BigDecimal standard, BigDecimal deluxe, BigDecimal premium) throws Exception {
        BigDecimal priceDiff = null;
        if(OrderServiceConstants.GBB_TOKEN_1.equals(token)) {
            // deluxe price - standard price
            priceDiff = deluxe.subtract(standard);
        } else if(OrderServiceConstants.GBB_TOKEN_2.equals(token)) {
            // premium price - standard price
            priceDiff = premium.subtract(standard);
        } else if(OrderServiceConstants.GBB_TOKEN_3.equals(token)) {
            // premium price - deluxe price
            priceDiff = premium.subtract(deluxe);
        } else {
            priceDiff = new BigDecimal("0");
        }
        return priceDiff;
    }  
    */
    public String getFrpGlobalParm(String context, String name) throws CacheException, Exception {
        CacheUtil cacheUtil = CacheUtil.getInstance();
        return cacheUtil.getGlobalParm(context, name);
    }
    
    private String padTime(int time) throws Exception {
        return pad(String.valueOf(time), PAD_LEFT, "0", 2);
    }
    
    public static String pad(String value,int padSide, String padChar, int size) throws Exception 
    {
     //null check
     if(value == null)
        value = "";
       
     String padded = value;       
    
     if(value.length() > size) {
        if(padSide == PAD_RIGHT) {
          padded = value.substring(0,size);
        }
        else {
          padded = value.substring(padded.length()-size);
        }
     }
     else
     {
       for(int i=value.length();i<size;i++)
       {
         if(padSide == PAD_LEFT)
            padded = padChar + padded;
         else
            padded = padded + padChar;
       }//end for loop
     }//end else less then max size
    
     return padded;
    }    
    
    public String getImageFormat(String url) throws Exception {
        if(url != null) {
            return "image/" + url.substring(url.lastIndexOf(".") + 1);
        }
        return null;
    }
    
    public String listToDelimitedString(List<String> list, String delimiter) throws Exception {
        String str = "";
        if(list != null) {
            for(int i = 0; i < list.size(); i++) {
                str += list.get(i);
                if(i < (list.size() - 1)) {
                    str += delimiter;
                }
            }
        }
        return str;
    }
}    