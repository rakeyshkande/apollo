package com.ftd.avs.common.util;

import com.ftd.osp.utilities.cacheMgr.handlers.vo.ProductMasterVO;
import java.util.Comparator;

/**
 * Comparator interface implementation to allow sort by product index order.
 */
public class IndexOrderComparator implements Comparator {

    
    public int compare(Object obj1, Object obj2) {
        int indexOrder1 = ((ProductMasterVO)obj1).getIndexOrder(); 
        int indexOrder2 = ((ProductMasterVO)obj2).getIndexOrder();
        
        if(indexOrder1 > indexOrder2) {
            return 1;
        } else if (indexOrder1 < indexOrder2) {
            return -1;
        }
         return 0;
    }


}    