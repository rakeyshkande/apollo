package com.ftd.avs.common.util;

import static com.ftd.avs.common.constant.AddressVerificationServiceConstants.AVS_BING_SERVICE_URL;
import static com.ftd.avs.common.constant.AddressVerificationServiceConstants.AVS_CONFIG;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;

import javax.xml.namespace.QName;

import net.virtualearth.dev.webservices.v1.geocode.contracts.IGeocodeService;

import org.apache.commons.lang.StringUtils;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.tempuri.MdBingGeocodeService;

import com.ftd.avs.common.constant.AddressVerificationServiceConstants;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.j2ee.ResourceUtil;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * This class should be used to fetch web service client instances
 * 
 * @author cjohnson
 *
 */
public class WebServiceClientFactory {
	
	private static final Logger log = new Logger(WebServiceClientFactory.class.getName());
	
	private IGeocodeService iGeocodeService;
	private String geocodeServiceURLStr;
    private String timeout = null;

	
	public void setGeocodeServiceURL(String geocodeServiceURL) {
		this.geocodeServiceURLStr = geocodeServiceURL;
	}
	
	
	public IGeocodeService getGeocodeService() throws CacheException, Exception {
		
		CommonUtil commonUtil = new CommonUtil();
        String currTimeout = null;

        try
        {
            currTimeout = commonUtil.getFrpGlobalParm(AVS_CONFIG, AddressVerificationServiceConstants.BING_TIMEOUT_PERIOD);
        }
        catch (Exception e) {}
        
        // check for global parm changes to know if we need to refresh the service object
		if (
		        iGeocodeService == null
		        || !StringUtils.equals(this.geocodeServiceURLStr,commonUtil.getFrpGlobalParm(AVS_CONFIG, AVS_BING_SERVICE_URL))
		        || !StringUtils.equals(this.timeout, currTimeout)
		)
		{
			try {
				this.geocodeServiceURLStr = commonUtil.getFrpGlobalParm(AVS_CONFIG, AVS_BING_SERVICE_URL);
				log.warn("Creating Bing webservice reference for endpoint: " + this.geocodeServiceURLStr);
								
				URL geocodeServiceURL = new URL(this.geocodeServiceURLStr);
				
				//URL wsdlFile = ResourceUtil.getInstance().getResource(MdBingGeocodeService.WSDL_LOCATION.getPath());
				URL wsdlFile = new URL(this.geocodeServiceURLStr.toString() + AddressVerificationServiceConstants.WSDL_EXTENSION);
				MdBingGeocodeService service = new MdBingGeocodeService(wsdlFile);
		        Iterator iter = service.getPorts();
		        while(iter.hasNext())
		        {
		          QName testQ = (QName)iter.next();
		          service.addPort(testQ, "http://schemas.xmlsoap.org/soap/", geocodeServiceURL.toString());
		        }
				this.iGeocodeService = service.getNonSSL();
				
				// set a timeout on the service call				
				int connectionTimeout = 5000;
				int readTimeout = 5000;
   
				if (currTimeout != null)
				{
				    timeout = currTimeout;
				    connectionTimeout = Integer.valueOf(timeout);
				    readTimeout = Integer.valueOf(timeout);
				}
				else
				{
                    log.error("Error getting time out period config - using defaults");
				}
				
                Client proxy = ClientProxy.getClient(iGeocodeService);                 
                HTTPConduit conduit = (HTTPConduit) proxy.getConduit();
                 
                // HTTPClientPolicy - Properties used to configure a client-side HTTP port
                HTTPClientPolicy httpClientPolicy = new HTTPClientPolicy();                 
                httpClientPolicy.setConnectionTimeout(connectionTimeout);
                httpClientPolicy.setReceiveTimeout(readTimeout);
                log.info("BING timeout = " + readTimeout);
                 
                conduit.setClient(httpClientPolicy);
				
			} catch (MalformedURLException e) {
				log.error("Error when getting bing web service: " + e.getMessage() );
				e.printStackTrace();
			}
			catch (Exception e) {
				log.error("Error when getting bing web service with url "+this.geocodeServiceURLStr+": "+ e.getMessage() );
				e.printStackTrace();
			}
		}
		
		return iGeocodeService;
	}
	


}
