package com.ftd.avs.common.dao;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.ftd.avs.common.constant.AddressVerificationServiceConstants;
import com.ftd.avs.webservice.vo.v1.Address;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.OccasionVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.statements.ExecuteSQLStatement;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * This class handles the data access related to AVS requests and responses
 * 
 * @author cjohnson
 *
 */
public class AVSDAO {

    protected static Logger logger  = 
        new Logger(AVSDAO.class.getName());

    /**
     * 
     * @param address
     * @return
     */
	public Long saveAVSRequest(Address address, String clientID, long responseTime, String vendorResponseID, Connection conn) {
		
		Long avsRequestId = null;
		
		if(logger.isDebugEnabled()) {
			logger.debug("saveAVSRequest");
		}
		Map<String,Object> inputs = new HashMap<String, Object>();
		
		inputs.put("IN_CLIENT_IDENTIFIER", clientID);
		inputs.put("IN_RESPONSE_TIME", responseTime);
		inputs.put("IN_VENDOR_RESPONSE_ID", vendorResponseID);
		inputs.put("IN_VENDOR", "BING");
		inputs.put("IN_ADDRESS", address.getAddressLine());
		inputs.put("IN_CITY", address.getCity());
		inputs.put("IN_STATE_PROVINCE", address.getState());
		inputs.put("IN_POSTAL_CODE", address.getZip());
		
		DataRequest dataRequest = new DataRequest();
		dataRequest.setConnection(conn);
		dataRequest.setStatementID(AddressVerificationServiceConstants.DB_INSERT_AVS_REQUEST);
		try {
			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			dataRequest.setInputParams(inputs);
			dataRequest.setConnection(conn);
			Map outputs = (Map)dataAccessUtil.execute(dataRequest);
			BigDecimal avsRequestIDDecimal = (BigDecimal)outputs.get("AVS_REQUEST_ID");
			
			// yay finally got the return value from this query
			avsRequestId = new Long(avsRequestIDDecimal.longValue());
			
			

			
			
		} catch (Exception e) {
			logger.error("Error while saving AVS request info: " + e.getMessage());
			e.printStackTrace();
		}
		
		return avsRequestId;
	}
	
}
