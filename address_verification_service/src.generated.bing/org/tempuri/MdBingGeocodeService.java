package org.tempuri;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.Service;
import net.virtualearth.dev.webservices.v1.geocode.contracts.IGeocodeService;

/**
 * This class was generated by Apache CXF 2.4.2
 * 2017-12-12T13:40:24.365-06:00
 * Generated source version: 2.4.2
 * 
 */
@WebServiceClient(name = "MdBingGeocodeService", 
                  wsdlLocation = "file:wsdl/geocodeservice-bing-2017.svc.wsdl",
                  targetNamespace = "http://tempuri.org/") 
public class MdBingGeocodeService extends Service {

    public final static URL WSDL_LOCATION;

    public final static QName SERVICE = new QName("http://tempuri.org/", "MdBingGeocodeService");
    public final static QName NonSSL = new QName("http://tempuri.org/", "NonSSL");
    static {
        URL url = null;
        try {
            url = new URL("file:wsdl/geocodeservice-bing-2017.svc.wsdl");
        } catch (MalformedURLException e) {
            java.util.logging.Logger.getLogger(MdBingGeocodeService.class.getName())
                .log(java.util.logging.Level.INFO, 
                     "Can not initialize the default wsdl from {0}", "file:wsdl/geocodeservice-bing-2017.svc.wsdl");
        }
        WSDL_LOCATION = url;
    }

    public MdBingGeocodeService(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public MdBingGeocodeService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public MdBingGeocodeService() {
        super(WSDL_LOCATION, SERVICE);
    }
    

    /**
     *
     * @return
     *     returns IGeocodeService
     */
    @WebEndpoint(name = "NonSSL")
    public IGeocodeService getNonSSL() {
        return super.getPort(NonSSL, IGeocodeService.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns IGeocodeService
     */
    @WebEndpoint(name = "NonSSL")
    public IGeocodeService getNonSSL(WebServiceFeature... features) {
        return super.getPort(NonSSL, IGeocodeService.class, features);
    }

}
